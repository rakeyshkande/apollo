/*
Global variables
*/
var callingAction = "";
var CUSTOM_ACTION = "custom_action";
var SAVE_ACTION         = "save_changes";
var CANCEL_ACTION       = "cancel_update";
var FORM_EDITED_YES     = 'Y';
var FORM_EDITED_NO      = 'N';
var DELETE              = 'delete_addon';
var CANCEL_UPDATE_PAGE  = 'cancelUpdateOrder.do?';
var LOAD_DETAIL_PAGE    = 'loadProductDetail.do';
var UPDATE_ORDER_CONFIRM = "UPDATE_ORDER_CONFIRMATION";
var DISPLAY_PRODUCT_CATEGORY_BUTTON = "DisplayProductCategoryButton";
var REMOVE_ADDONS = "";
var IGNORE_ERROR = "IgnoreError";
var tabsArray = new Array("page_delivery_date", "getMaxButton", "page_ship_method", "price_standard", "price_deluxe", "price_premium", "variable_price", "color_first_choice", "color_second_choice", "balloon_checkbox", "balloon_quantity", "bear_checkbox", "bear_quantity", "funeral_checkbox", "funeral_quantity", "chocolate_checkbox", "chocolate_quantity", "card_add_on_code", "greetingCardsLink", "custComplete", "custCancel");
var validationMatrix = new Array
     (//input attribute name      error message                         validation function       additional function parameters
     );


/*
 *  Initialization
 */
function init(){
  document.oncontextmenu = contextMenuHandler;
  document.onkeydown = upcBackKeyHandler;
  setScrollingDivHeight();
  addDefaultListeners();

  setTabIndexes(tabsArray, 1);
  initErrorMessages();

  window.onresize = setScrollingDivHeight;
}

/*
 * Called when the page loads, this function displays any error messages
 * generated on load or from validation failures.
 */
function initErrorMessages() {
  if ( fieldErrors.length > 0 || okErrors.length > 0 || confirmErrors.length > 0 ) {
    var array = doDisplayServerSideValidationMessages();
    if ( array.goToPage != null && array.goToPage != "" ) {
      doCustomAction( array.goToPage );
    }
    else if ( array.jsFunction != null) {
      array.jsFunction();
    }
  }
}

/*
 * Custom action determined by server side processing.
 */
function doCustomAction(action){
  callingAction = CUSTOM_ACTION;
  document.getElementById("productDetailForm").action = action;
  doRecordLock('MODIFY_ORDER', 'check', null, "productDetailForm");
}

/*
 * Submits the custom action after retrieving the lock.
 */
function doCustomPostAction(){
  showWaitMessage("mainContent", "wait", "Retrieving");
  document.getElementById("productDetailForm").submit();
}


function setScrollingDivHeight(){
    var productDetailDiv = document.getElementById("productDetail");
    productDetailDiv.style.height = document.body.clientHeight - productDetailDiv.getBoundingClientRect().top - 75;
}

function prepareSubmit(){

  prepareDeliveryDate("productDetailForm");

  var standard = document.getElementById("price_standard");
  var deluxe = document.getElementById("price_deluxe");
  var premium = document.getElementById("price_premium");
  var variable = document.getElementById("price_variable");
  var productAmount = document.getElementById("product_amount");

  var subcode = document.getElementById('subcode_id');
  if ( subcode )
  {
    var subcodeIndexSelected = subcode.selectedIndex;
    var subcodeValue = subcode.value;

    if (subcodeIndexSelected != 0)
      document.getElementById("subcode").value = subcodeValue;
  }


  if (standard && standard.checked){
    productAmount.value = document.getElementById("standard_price").value;
    document.getElementById("size_indicator").value = "A";
  }
  else if (deluxe && deluxe.checked){
    productAmount.value = document.getElementById("deluxe_price").value;
    document.getElementById("size_indicator").value = "B";
  }
  else if (premium && premium.checked){
    productAmount.value = document.getElementById("premium_price").value;
    document.getElementById("size_indicator").value = "C";
  }
  else if (variable && variable.checked){
    productAmount.value = document.getElementById("variable_price").value;
		document.getElementById("variable_price_selected").value = "y";
    document.getElementById("size_indicator").value = "A";
  }

  if ( !isFlorist ) {
    document.getElementById("size_indicator").value = "A";
  }
}


function performAction(url){
  showWaitMessage("mainContent", "wait","Processing");
  document.getElementById('productDetailForm').action = url;
  document.getElementById('productDetailForm').submit();
}


/*
*  Validation
*/
function validate(){
  var valid = true;
	var outsideRange = false;

	var minimumPrice = 0;
	if (parseFloat(variablePriceMinStandard) < parseFloat(variablePriceMinText))
		minimumPrice = variablePriceMinStandard;
	else
		minimumPrice = variablePriceMinText;

//alert("variablePriceMinStandard = " + variablePriceMinStandard + " and variablePriceMinText = " + variablePriceMinText + " and minimumPrice = " + minimumPrice);
  var variableRadio = document.getElementById("price_variable");
  var field = document.getElementById('variable_price');
  if ( field != null )
  {
	  field.style.backgroundColor='white';
	}
  if ( variableRadio && variableRadio.checked )
  {
    if ( field != null )
    {
      valid = isValidUSDollar ( field.value );
      if ( parseFloat(field.value) < parseFloat(minimumPrice) || parseFloat(field.value) > parseFloat(variablePriceMax ) )
      {
				outsideRange = true;
				valid = false;
      }

      if( !valid )
      {
       field.focus();
       field.style.backgroundColor='pink';
      }
    }
  }

  //color1
  var color1 = document.getElementById("color_first_choice");
  if ( color1 )
  {
    color1.className = "TblText";
    if (color1.value == "")
    {
      color1.className = "ErrorField";
      color1.focus();
      valid = false;
    }
  }

  //color2
  var color2 = document.getElementById("color_second_choice");
  if ( color2 )
  {
    color2.className = "TblText";
    if (color2.value == "")
    {
      color2.className = "ErrorField";
      color2.focus();
      valid = false;
    }
  }

  //delivery dates
	var returnDate = true;
	returnDate = validateShipMethodAndDeliveryDate();
	if (returnDate == false)
	{
		valid = false;
	}


  //productSubType
  var productSubType = document.getElementById("subcode_id");
  if ( productSubType )
  {
    productSubType.className = "TblText";
    if (productSubType.value == "")
    {
      productSubType.className = "ErrorField";
      productSubType.focus();
      valid = false;
    }
  }

	if (valid == false)
	{
		if (outsideRange)
			alert("Variable price range: " + minimumPrice + " - " + variablePriceMax + ".  Please correct the marked fields");
		else
			alert("Please correct the marked fields");
	}

  return valid;
}



/*
 *  Utility Functions
 */
function updateSubstitutionIndicator()
{
   var element = document.getElementById("substitution_indicator");

   if (element.checked)
   {
   	element.value = 'Y';
   }
   else
   {
   	element.value = 'N';
   }

 }





/*
 *  Utility Functions
 */
function updateAddOnQuantity(checkbox, fieldName){
  var element = document.getElementById(fieldName);
  if (element) {
    if (checkbox.checked){
      element.disabled = false;
    } else {
      element.disabled = true;
    }
  }
}

function updateCheckbox(textfield, checkbox){
  checkbox.checked = parseInt(textfield.value) > 0;
}


/*
 *  Popups
 */
function openGreetingCardPopup(){
   var form = document.getElementById('productDetailForm');
   var greeting = document.getElementById('addonNew_optCard');

   var url_source =  "lookupGreeting.do" +
                     "?action=LookupGreeting" +
                     "&occasionInput=" + form.occasion.value +
                     "&cardIdInput=" + greeting.options[greeting.selectedIndex].value +
                     getSecurityParams(false);

  var modal_dim = "dialogWidth:800px; dialogHeight:500px; center:yes; status=0; help:no";
  var ret =  showModalDialogIFrame(url_source, "", modal_dim);
  if (ret && ret != null && ret[0] != '') {
    greeting.value = ret[0] + "---" + ret[1];  
  }
}


function doLargeImagePopup(){
   largeImage = document.getElementById("showLargeImage").style;
   scroll_top = document.body.scrollTop;

   largeImage.top = scroll_top + document.body.clientHeight/2 - 200;
   largeImage.width = 340;
   largeImage.height = 400;
   largeImage.left = 200;
   largeImage.display = "block";
   hideShowCovered(showLargeImage);
}

function closeLargeImage(){
  largeImage.display = "none";
  hideShowCovered(showLargeImage);
}



  // Called when a same day gift has no codified florists for this zip code
  function openNoCodifiedFloristHasCommonCarrier(){
    disableContinue();

    noCodifiedFloristHasCommonCarrierV = document.getElementById("noCodifiedFloristHasCommonCarrier").style;
    scroll_top = document.body.scrollTop;
    noCodifiedFloristHasCommonCarrierV.top = scroll_top+document.body.clientHeight/2-100;

    noCodifiedFloristHasCommonCarrierV.width = 290;
    noCodifiedFloristHasCommonCarrierV.height = 100;
    noCodifiedFloristHasCommonCarrierV.left = document.body.clientWidth/2 - 50;
    noCodifiedFloristHasCommonCarrierV.visibility = "visible";
  }

  function closeNoCodifiedFloristHasCommonCarrier(){
    noCodifiedFloristHasCommonCarrierV.visibility = "hidden";
    enableContinue();
  }

  function noCodifiedFloristHasCommonCarrier(){
    closeNoCodifiedFloristHasCommonCarrier();
  }

  // Called when a same day gift has no florists for this zip code
  function openNoFloristHasCommonCarrier(){
    disableContinue();

    noFloristHasCommonCarrierV = document.getElementById("noFloristHasCommonCarrier").style;
    scroll_top = document.body.scrollTop;
    noFloristHasCommonCarrierV.top = scroll_top+document.body.clientHeight/2-100;

    noFloristHasCommonCarrierV.width = 290
    noFloristHasCommonCarrierV.height = 100;
    noFloristHasCommonCarrierV.left = document.body.clientWidth/2 - 50;
    noFloristHasCommonCarrierV.visibility = "visible";
  }

  function closeNoFloristHasCommonCarrier(){
    noFloristHasCommonCarrierV.visibility = "hidden";
    enableContinue();
  }

  function noFloristHasCommonCarrier(){
    closeNoFloristHasCommonCarrier();
  }


/*
  New functions added specifically for Modify Order
*/

function resetRadio( value )
{
 var size = document.all.price.length;
 for (i=0; i<size; i++) {
   if ( document.all.price[i].value == "variable" )
  document.all.price[i].checked = true;
   }
}

function checkVariablePrice( value ) {
  if ( value != "" && document.getElementById('variable_price') != null) {
    document.getElementById("variable_price").value= "";
    document.getElementById("variable_price").style.backgroundColor = "white";
  }
}

function isValidUSDollar( dollar )
{
   if ( dollar == null || dollar == "" )
  return false;

   first = dollar.indexOf(".");

   if ( first != -1 )
   {
  dollar_length = dollar.length;

  firstSub = dollar.substr( 0, first );
  secondSub = dollar.substr( first + 1, dollar_length - first );

  if ( ( dollar_length - first ) == 1 )
    return isInteger( firstSub );
  else
    return ( isInteger( firstSub ) && isInteger( secondSub ) );

    } else {
  return isInteger( dollar );
    }

  return false;
}

/*
 Function handles the Cancle Update button
*/
function doCancelUpdateAction(){
  if ( hasFormChanged() ) {
    if ( doExitPageAction(WARNING_MSG_4) ) {
      baseDoCancelUpdateAction("productDetailForm");
    }
  }
  else {
    baseDoCancelUpdateAction("productDetailForm");
  }
}


/*
 Function handles the Complete Update button action
 Once this function completes the checkLockIFrame calls
 checkAndForward() which calls the doCopmleteOrder() function
*/
function doCompleteUpdateAction(){
  if ( validate() ) {
    //remove personal greeting id if necessary
    if ( document.getElementById('personal_greeting_flag').value == 'N' && document.getElementById('personal_greeting_id').value != null )
    {
        document.getElementById('personal_greeting_id').value = '';
    }
    showWaitMessage("mainContent", "wait", "Validating information please wait"); /* show wait message */
    doRecordLock('MODIFY_ORDER', 'check', null, "productDetailForm");  /* make sure we still have a lock on this record */
  }
}

/*
   continue to confirmation page
*/
function doOrderConfirmation(){
  prepareSubmit();
  document.getElementById('display_page').value = UPDATE_ORDER_CONFIRM;
  var toSubmit = document.getElementById('productDetailForm');
  toSubmit.action += "?action=" + SAVE_ACTION + getSecurityParams(false);
  toSubmit.submit();
}

var VALIDATION_MESSAGE = "";
function doDisplayServerSideValidationMessages() {
  var ID = 0;
  var TEXT = 1;
  var YES = 2;
  var NO = 3;
  var toReturn = new Array();

  if ( isAvailable ) {

    // highlight errors
    if ( fieldErrors != null && fieldErrors.length > 0 ) {
      validateForm(fieldErrors);
    }

    // error messages requiring an 'Ok' popup
    if ( okErrors != null && okErrors.length > 0 ) {
      // concatenate error messages
      var messages = "";
      for (var i = 0; i < okErrors.length; i++) {
        messages += okErrors[i][TEXT];
      }
      VALIDATION_MESSAGE = messages;

      urlOrId = okErrors[0][YES];
      if ( urlOrId != DISPLAY_PRODUCT_CATEGORY_BUTTON ) {
        // modal arguments
        var modalArguments = new Object();
        modalArguments.modalText = messages;

        // modal features
        var modalFeatures = 'dialogWidth:300px; dialogHeight:200px; center:yes; status=no; help=no; resizable:no; scroll:yes';
        toReturn.okErrorReturnValue = window.showModalDialog("alert.html", modalArguments, modalFeatures);
      }
    }

    // error messages requiring a 'Yes/No' popup
    if ( confirmErrors != null && confirmErrors.length > 0 ) {
      var urlOrId = "";
      for ( var i = 0; i < confirmErrors.length; i++) {
        if ( urlOrId == '' || urlOrId == confirmErrors[i][ID] ) {
          // concatenate error messages
          var messages = confirmErrors[i][TEXT];

          // modal arguments
          var modalArguments = new Object();
          modalArguments.modalText = messages;
          VALIDATION_MESSAGE = messages;

          // modal features
          var modalFeatures = 'dialogWidth:300px; dialogHeight:200px; center:yes; status=no; help=no; resizable:no; scroll:yes';
          toReturn.confirmErrorReturnValue = window.showModalDialog("confirm.html", modalArguments, modalFeatures);
          if ( toReturn.confirmErrorReturnValue ) {
            urlOrId = confirmErrors[i][YES];
          }
          else {
            urlOrId = confirmErrors[i][NO];
          }
        }
      }
    }

    // a JS function may need to be called, if so check wether or not the
    // urlOrId field contains an action or not
    if ( urlOrId == DISPLAY_PRODUCT_CATEGORY_BUTTON ) {
      toReturn.jsFunction = doShowUpdateProductCategoryDiv;
    }
    else if ( urlOrId == IGNORE_ERROR ) {
      toReturn.jsFunction = doIgnoreErrorsResubmitAction;
    }
    else {
      toReturn.goToPage = urlOrId;
    }

  } // end isAvailable

  return toReturn;
}

/*
  If there are any addons that were on the original
  order, that are not supported by the current product
  call call loadDetail.do to remove these addons
*/
function doRemoveAddonsAction(){
  performAction(LOAD_DETAIL_PAGE+"?action_type=" + DELETE + getSecurityParams(false));
}


/*
 Any time the checkLockIframe needs to be invoked
 this method will be called after the IFrame finishes loading
 From here we can call what ever method we need to in order
 to continue processing

 You must call doRecordLock(  ) in order to have this
 function invoked. Also see util.js

*/
function checkAndForward(forwardAction){
  if ( callingAction == CUSTOM_ACTION ) {
    doCustomPostAction();
  }
  else {
    doOrderConfirmation();
  }
}



/* Executes an iframe action.
 *
 * @param action  The sub-action
 */
function doIFrameAction(url, data) {
   var form = document.getElementById("productDetailForm");
   var frame = document.getElementById("updateProductDetailActions");
   frame.src = url + data;
}

var MAX_DATES_ACTION = "max_dates";
var callingAction = "";
function doGetMaxDeliveryDateAction() {
  showWaitMessage("mainContent", "wait", "Retrieving");
  callingAction = MAX_DATES_ACTION;
  var data = "?action_type=" + MAX_DATES_ACTION +
             "&recipient_state=" + document.getElementById("recipient_state").value +
             "&recipient_zip_code=" + document.getElementById("recipient_zip_code").value +
             "&recipient_country=" + document.getElementById("recipient_country").value +
             "&product_id=" + document.getElementById("product_id").value +
             "&occasion=" + document.getElementById("occasion").value +
             "&origin_id=" + document.getElementById("origin_id").value +
             "&order_detail_id=" + document.getElementById("order_detail_id").value +
             "&ship_method_carrier=" + document.getElementById("ship_method_carrier").value +
             "&ship_method_florist=" + document.getElementById("ship_method_florist").value +
             "&orig_product_id=" + document.getElementById("orig_product_id").value +
             getSecurityParams(false);

  doIFrameAction("loadProductDetail.do", data);
}

function doGetMaxDeliveryDaysPostAction(deliveryDates) {
  hideWaitMessage("mainContent", "wait");
  populateDeliveryDates(deliveryDates);
}

/* Populates the delivery date drop down using the supplied two-dimensional array.
 * This function is typically called after clicking the "Get Max" button.
 *
 * @param deliveryDates three-dimensional array of delivery date data
 */
function populateDeliveryDates(deliveryDates)
{
  var reqDate = document.getElementById("delivery_date");
  var dd = document.getElementById("delivery_date_select");
  var option;
  var selected = false;
  dd.options.length = 0;
  for (var i = 0; i < deliveryDates.length; i++){
    selected = reqDate.value == deliveryDates[i][0];
    option = new Option(deliveryDates[i][2], deliveryDates[i][0], false, selected);
    option.endDate = deliveryDates[i][1];
    dd.add(option);
  }
}

/*
 * Used to go back to the Update Product Category page.
 */
function doUpdateProductCategoryAction() {
  var toSubmit = document.getElementById("productDetailForm");
  toSubmit.action = "loadProductCategory.do";
  toSubmit.submit();
}


function doShowUpdateProductCategoryDiv() {
  document.getElementById("productDetail").style.display = "none";
  document.getElementById("custComplete").style.display = "none";
  document.getElementById("custCancel").style.display = "none";
  document.getElementById("updCompleteButton").style.display = "none";
  document.getElementById("updCancelButton").style.display = "none";
  document.getElementById("messageDiv").style.display = "block";
  document.getElementById("messageCell").innerHTML = VALIDATION_MESSAGE;
}


/*
 * Called after displaying server side validations, this function
 * resubmits the form setting a flag which will cause server
 * side validations to be skipped.
 */
function doIgnoreErrorsResubmitAction() {
  prepareSubmit();
  document.getElementById("ignore_error").value = "Y";
  document.getElementById("productDetailForm").submit();
}

/*
 *  Disables the various navigation keyboard commands.
 */
function upcBackKeyHandler() {
   // backspace
   if (window.event && window.event.keyCode == 8){
      var formElement = false;

      for(i = 0; i < document.forms['productDetailForm'].elements.length; i++){
         if(document.forms['productDetailForm'].elements[i].name == document.activeElement.name){
            formElement = true;
            break;
         }
      }

      if(formElement == false || (document.activeElement.type != "text" && document.activeElement.type != "textarea" && document.activeElement.type != "password")){
         window.event.cancelBubble = true;
         window.event.returnValue = false;
         return false;
      }
   }

   // F5 and F11
   if (window.event.keyCode == 122 || window.event.keyCode == 116){
      window.event.keyCode = 0;
      event.returnValue = false;
      return false;
   }

   // Alt + Left Arrow
   if(window.event.altLeft){
      window.event.returnValue = false;
      return false;
   }

   // Alt + Right Arrow
   if(window.event.altKey){
      window.event.returnValue = false;
      return false;
   }
}

/*
 * Handle the Update Delivery Information button event
 */
function doUpdateDeliveryInformation(){
  var url = 'loadDeliveryInfo.do';
  performAction(url);
}

/*
 * Handle the Update Product Category button event
 */
function doUpdateProductCategory(){
  var url = 'loadProductCategory.do';
  performAction(url);
}

/*******************************************************************************
*
*    performAction()
*
*******************************************************************************/
function performAction(url){
    /*
      Display a message to the user called Processing...
      this allows the user to know that the system is working
      and not stalled out.
      scroll(0,0) returns the window to the top position since
      all(most) buttons are located at the bottom of a scrolled window
    */
    scroll(0,0);
    showWaitMessage("mainContent", "wait", "Processing");

     document.forms[0].action = url;
     document.forms[0].submit();
}

