
/*************************************************************************************
* Clear all form fields
**************************************************************************************/
function init(){
          setNavigationHandlers();
          addDefaultListeners();
          document.getElementById('scMessageSystemId').focus();
}
function doClearFields(){
        clearAllFieldsBetter(document.forms[0]);
        document.theform.scNonPartner.checked=true;
        document.theform.scCustomerEmail.checked=true;
        document.getElementById('Tr_Askp_Amts').style.display="none";
        document.getElementById('Tr_Key_Word').style.display="none";
        initInputFieldStyle();
      }
      
function changeCriteriaType(){
  if(document.theform.scMessageSystemId.value =='Merc' && 
  document.theform.scQueueTypeId.value =='ASKP' && 
  document.theform.scMessageDirectionId.value =='INBOUND'){
  
    document.getElementById('Tr_Askp_Amts').style.display="block";
  }else{
    document.getElementById('Tr_Askp_Amts').style.display="none";
  }
  
  if((document.theform.scQueueTypeId.value =='ASK' || 
  document.theform.scQueueTypeId.value =='ANS' || document.theform.scQueueTypeId.value =='ASKP') &&  
  document.theform.scMessageDirectionId.value =='INBOUND' && 
  document.theform.scMessageSystemId.value =='Merc'){
  
    document.getElementById('Tr_Key_Word').style.display="block";
  }else{
    document.getElementById('Tr_Key_Word').style.display="none";
  }
}          
function validateAmountFields(textbox){
  if(!formatDollarAmount(textbox)){
    textbox.className = "ErrorField";
    textbox.focus();
    alert("Invalid ASKP Search Range.");
  }else{
    textbox.className = "input";
  }
}

function validateAmountRange(){
  this.errMsg = "";
  this.focusOnField = "";
  this.minTabIndex = 99999;
  var fromVal = document.theform.scAmountFrom.value;
  var toVal = document.theform.scAmountTo.value;
  if((fromVal != null && fromVal != '') && (toVal == null || toVal == '')){
    this.errMsg = "Please enter a valid ASKP Search Range.";
    if (document.theform.scAmountTo.tabIndex < this.minTabIndex) {
      this.minTabIndex = document.theform.scAmountTo.tabIndex;
      this.focusOnField = document.theform.scAmountTo.name;
    }
    document.theform.scAmountTo.className="ErrorField";
  }else if((fromVal == null || fromVal == '') && (toVal != null && toVal != '') ){
    this.errMsg = "Please enter a valid ASKP Search Range.";
    if (document.theform.scAmountFrom.tabIndex < this.minTabIndex) {
      this.minTabIndex = document.theform.scAmountFrom.tabIndex;
      this.focusOnField = document.theform.scAmountFrom.name;
    }
    document.theform.scAmountFrom.className="ErrorField";
  }else{
    if(parseFloat(fromVal) > parseFloat(toVal)){
      this.errMsg = "Please enter a valid ASKP Search Range.";
      if (document.theform.scAmountFrom.tabIndex < this.minTabIndex) {
         this.minTabIndex = document.theform.scAmountFrom.tabIndex;
         this.focusOnField = document.theform.scAmountFrom.name;
      }
      document.theform.scAmountFrom.className="ErrorField";
    }
  }
}

function changeKeywordCondition(drpdown){
  var str = drpdown.value;
  document.theform.scKeywordSearchCondition1.value=str;
  document.theform.scKeywordSearchCondition2.value=str;
}

function validateKeywordSearch(){
  
  this.errMsg="";
  this.focusOnField = "";
  this.minTabIndex = 99999;
  if((document.theform.scKeywordSearchText1.value == null || document.theform.scKeywordSearchText1.value == "") && 
      (document.theform.scKeywordSearchText2.value != "" || 
      document.theform.scKeywordSearchText3.value != "")){
    
      this.errMsg = this.errMsg+"Please enter text in first keyword search field.\n";
      if (document.theform.scKeywordSearchText1.tabIndex < this.minTabIndex) {
        this.minTabIndex = document.theform.scKeywordSearchText1.tabIndex;
        this.focusOnField = document.theform.scKeywordSearchText1.name;
       }
       document.theform.scKeywordSearchText1.className="ErrorField";
    
  }else if( (document.theform.scKeywordSearchText2.value == null || document.theform.scKeywordSearchText2.value == "") && 
      document.theform.scKeywordSearchText3.value != ""){
      this.errMsg = this.errMsg+"Please enter text in second Keyword Search field or leave the third Keyword Search field blank.\n";
      if (document.theform.scKeywordSearchText2.tabIndex < this.minTabIndex) {
        this.minTabIndex = document.theform.scKeywordSearchText2.tabIndex;
        this.focusOnField = document.theform.scKeywordSearchText2.name;
       }
       document.theform.scKeywordSearchText2.className="ErrorField";
  }
}




function validateInclude(){
    this.focusOnField = "";
    this.errMsg = "";
    this.minTabIdx = 99999;
    this.flag = '0';
  
  if(document.theform.scNonPartner.checked){
    this.flag='1';
  }

  a = document.theform.prodCnt.value;
  b = document.theform.partnerCnt.value;
  if(this.flag =='0'){
    for(var i = 1; i<=a; i++){
      var chkbox = document.getElementById('prodProp_' + i);
      if(chkbox.checked){
        this.flag='1';
      }
      
    }
    
  }
  
  
  if(this.flag =='0'){
    for(var j = 1; j<=b; j++){
      var checkbox = document.getElementById('prefPartner_' + j);
      if(checkbox.checked){
        this.flag='1';
      }
      
    }
  }
  
  if(this.flag == '0'){
    this.errMsg = "Please select at least one Include option.\n";
    if (document.theform.scNonPartner.tabIndex < this.minTabIndex) {
      this.minTabIndex = document.theform.scNonPartner.tabIndex;
      this.focusOnField = document.theform.scNonPartner.name;
    }
    document.theform.scNonPartner.className="ErrorField";
    for(var i = 1; i<=a; i++){
      var chkbox = document.getElementById('prodProp_' + i);
      chkbox.className="ErrorField";
    }
    
    for(var j = 1; j<=b; j++){
      var checkbox = document.getElementById('prefPartner_' + j);
      checkbox.className="ErrorField"; 
    }
  }
}

  function initInputFieldStyle() {
    document.theform.scQueueTypeId.className="input";
    document.theform.scMessageDirectionId.className="input";
    document.theform.scMessageSystemId.className="input";
    document.theform.scNonPartner.className="input";
    document.theform.scKeywordSearchText1.className="input";
    document.theform.scKeywordSearchText2.className="input";
    document.theform.scKeywordSearchText3.className="input";
    document.theform.scAmountFrom.className="input";
    document.theform.scAmountTo.className="input";
    a = document.theform.prodCnt.value;
    b = document.theform.partnerCnt.value;
    
    for(var i = 1; i<=a; i++){
      var chkbox = document.getElementById('prodProp_' + i);
      chkbox.className="input";
    
    }
    for(var j = 1; j<=b; j++){
      var checkbox = document.getElementById('prefPartner_' + j);
      checkbox.className="input";
    
    }
    
    document.getElementById('scMessageSystemId').focus();
  
  }

function doSearch() 
{
  var textErrorMsg = "";
  if (window.event)
    if (window.event.keyCode)
      if (window.event.keyCode != 13)
        return false;

  var formValidator = new FormValidator();
  if (formValidator.errorMsg != "") 
  {
    alert(formValidator.errorMsg);
    if (formValidator.focusOnField != null && formValidator.focusOnField != "")
      document.theform[formValidator.focusOnField].focus();
  } 
  else 
  {
    if(document.theform.scMessageSystemId.value == 'Merc' 
        && (document.theform.scQueueTypeId.value == 'ASK' || document.theform.scQueueTypeId.value == 'ANS' || document.theform.scQueueTypeId.value == 'ASKP')
        && document.theform.scMessageDirectionId.value == 'INBOUND' )
    {
      if(document.theform.scKeywordSearchText1.value != "")
      {
        var valKeywordText1 = new validateKeywordText(document.theform.scKeywordSearchText1);
        if(valKeywordText1.invalidStr != null && valKeywordText1.invalidStr != "")
        {
          textErrorMsg = textErrorMsg + valKeywordText1.invalidStr+' from Keyword Text1 ';
          document.theform.scKeywordSearchText1.className = "ErrorField";
        }
        document.theform.scSearchText1.value = valKeywordText1.validStr;
      }
      else
      {
        document.theform.scSearchText1.value = "";
      }

      if(document.theform.scKeywordSearchText2.value != "")
      {
        var valKeywordText2 = new validateKeywordText(document.theform.scKeywordSearchText2);
        if(valKeywordText2.invalidStr != null && valKeywordText2.invalidStr != "")
        {
          textErrorMsg = textErrorMsg + valKeywordText2.invalidStr+ ' from Keyword Text2 ';
          document.theform.scKeywordSearchText2.className = "ErrorField";
        }
        document.theform.scSearchText2.value = valKeywordText2.validStr;
      }
      else
      {
        document.theform.scSearchText2.value = "";
      }

      if(document.theform.scKeywordSearchText3.value != "")
      {
        var valKeywordText3 = new validateKeywordText(document.theform.scKeywordSearchText3);
        if(valKeywordText3.invalidStr != null && valKeywordText3.invalidStr != "")
        {
          textErrorMsg = textErrorMsg + valKeywordText3.invalidStr+' from Keyword Text3 ';
          document.theform.scKeywordSearchText3.className = "ErrorField";
        }
        document.theform.scSearchText3.value = valKeywordText3.validStr;
      }
      else
      {
        document.theform.scSearchText3.value = "";
      }

      if(textErrorMsg != null && textErrorMsg != "")
      {
        alert("Please remove disallowed special characters from Keyword Search Fields.");
        return false; 
      }
    }

    document.theform.action_type.value = "search";
    submitActionNewWindow("queueMessageSearch.do");
  }
}

function submitActionNewWindow(url) {
 
   var winName = "win" + Math.round(1000*Math.random());
   window.open('',winName,'scrollbars=0,menubar=1,toolbar=0,location=0,status=0,resizable=1'); 
   document.theform.action = url;
   document.theform.target = winName;
   document.theform.submit();
}

/*************************************************************************************
* Using the specified URL submit the form
**************************************************************************************/
      function performAction(url){
        showWaitMessage("content", "wait", "Searching");
        document.forms[0].action = url;
        document.forms[0].submit();
      }

/*************************************************************************************
* Performs the Back button functionality
**************************************************************************************/      
      function doBackAction() {
   document.theform.action = "mainMenu.do";
   document.theform.target = "_top";
   document.theform.submit();
   }

function FormValidator() {
   this.focusOnField = "";
   this.errorMsg = "";
   this.minTabIndex = 99999;
   
   initInputFieldStyle();
   
   var msg = "";
   if (document.theform.scQueueTypeId.value == "") {
       msg = msg + "Queue Type is required.\n";
       if (document.theform.scQueueTypeId.tabIndex < this.minTabIndex) {
        this.minTabIndex = document.theform.scQueueTypeId.tabIndex;
        this.focusOnField = document.theform.scQueueTypeId.name;
       }
       document.theform.scQueueTypeId.className="ErrorField";
   }
   if (document.theform.scMessageSystemId.value == "") {
       msg = msg + "Message System is required.\n";
       if (document.theform.scMessageSystemId.tabIndex < this.minTabIndex) {
        this.minTabIndex = document.theform.scMessageSystemId.tabIndex;
        this.focusOnField = document.theform.scMessageSystemId.name;
       }
       document.theform.scMessageSystemId.className="ErrorField";
   }
  var valInclude = new validateInclude();
   if(valInclude.errMsg != '' && valInclude.errMsg != null && valInclude.errMsg != 'undefined'){
      this.focusOnField = valInclude.focusOnField;
      msg = msg + valInclude.errMsg;
   }
   if(document.theform.scMessageSystemId.value == 'Merc' 
    && (document.theform.scQueueTypeId.value == 'ASK' || document.theform.scQueueTypeId.value == 'ANS' || document.theform.scQueueTypeId.value == 'ASKP')
    && document.theform.scMessageDirectionId.value == 'INBOUND' ){
     var valKeywordSearch = new validateKeywordSearch();
     if(valKeywordSearch.errMsg != "" && valKeywordSearch.errMsg != null && valKeywordSearch.errMsg != 'undefined'){
      msg = msg + valKeywordSearch.errMsg;
      this.focusOnField = valKeywordSearch.focusOnField;
     }
  }
  
  if(document.theform.scMessageSystemId.value == 'Merc' 
    && document.theform.scQueueTypeId.value == 'ASKP'
    && document.theform.scMessageDirectionId.value == 'INBOUND' ){
    var valAmountRange = new validateAmountRange();
    if(valAmountRange.errMsg != null && valAmountRange.errMsg != "" && valAmountRange.errMsg != 'undefined'){
      msg = msg + valAmountRange.errMsg;
      this.focusOnField = valAmountRange.focusOnField;
    }
  }
  this.errorMsg = msg;
  
}


function validateKeywordText(txtbox){
	var ti=txtbox.value;
	var len = ti.length;
	var str = "";
	this.invalidStr = "";
  this.validStr = "";
	if(ti != null && ti != ""){
		for(var i = 0; i < len; i++){
		  str = ti.substring(i,i+1);
		  
			if (!str.match(/^[a-zA-Z0-9 $,-.'%]+$/)) {
				  this.invalidStr = this.invalidStr + str;
			}else{
          this.validStr = this.validStr + str;
      }
			
		}
		
	}
}