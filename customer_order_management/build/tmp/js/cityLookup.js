/*
 * Constructor
 */
CityPopup = function(params) {
   this.focusObj = null;
   this.cityName = params.city;
   this.stateName = params.state;
   this.zipName = params.zip;
   this.countryName = params.country;
};

CityPopup._C = null;

/*
 * Class functions
 */
CityPopup.setup = function(params) {
   window.popup = Popup.setup(params, new CityPopup(params));
   window.popup.create();
   window.popup.showAtElement(params.displayArea);
};

CityPopup.enterCitySearch = function(ev) {
   if (ev.keyCode == 13)
      CityPopup.searchCity(ev);
};

CityPopup.searchCity = function(ev) {
   //First validate the inputs
   var check = true;

   var state = document.getElementById("stateInput");
   var city = document.getElementById("cityInput");
   var zip = document.getElementById("zipCodeInput");

   //State is required if zip is empty
   if((stripWhitespace(state.value).length == 0) && (stripWhitespace(zip.value).length == 0)) {
     if (check == true) {
       state.focus();
       check = false;
     }
     state.style.backgroundColor = 'pink';
   }

   //City is required if zip is empty
   if((stripWhitespace(city.value).length == 0) && (stripWhitespace(zip.value).length == 0)) {
     if (check == true) {
       city.focus();
       check = false;
     }
     city.style.backgroundColor = 'pink';
   }

   if (!check) {
     alert("Please correct the marked fields")
     return false;
   }

   //Now that everything is valid, open the popup after removing special characters
   var bag2 = ",/.<>?;:\|[]{}~!@#$%^&*()-_=+`" + "\\\'";
   var cityVal = stripCharsInBag(city.value, bag2);
   var stateVal = stripCharsInBag(state.value, bag2);
   var zipVal = stripCharsInBag(zip.value, bag2);
   var countryInput = document.getElementById(CityPopup._C.countryName).value;

   var form = document.forms[0];
   var url_source="lookupLocation.do?" +
   "&cityInput=" + cityVal +
   "&stateInput=" + stateVal +
   "&zipCodeInput=" + zipVal +
   "&countryInput=" + countryInput + getSecurityParams(false);

   //Open the window
   var modal_dim = "dialogWidth:800px; dialogHeight:500px; center:yes; status=0";
   var ret = showModalDialogIFrame(url_source, "", modal_dim);

   // Set the new city, state, and zip
   if (ret && ret != null && ret[0] != "")
   {
       document.getElementById(CityPopup._C.cityName).value = ret[0];
       setSelectedState(document.getElementById(CityPopup._C.stateName), ret[1]);
       document.getElementById(CityPopup._C.zipName).value = ret[2];
   }

   Popup.hide();

		//updateCustomerAccount.xsl has a onkeypress event on the <body> tag.  We need to suppress this
		//when a lookup is performed.
 		document.getElementById(CityPopup._C.zipName).focus();
 		var submitSwitch = document.getElementById('submitSwitch');
	 	if (window.event.keyCode == 13 && submitSwitch != null)
	 	{
			document.getElementById('submitSwitch').value = 'no';
		}

   CityPopup._C = null;


};


CityPopup.pressHide = function () {
  if (window.event.keyCode == 13)
      Popup.hide();
  // if tab on close button, return to first button
  if (window.event.keyCode == 9){
     var input = document.getElementById("cityInput");
      input.focus();
      return false;
  }
};


/*
 * Member Functions
 */
CityPopup.prototype.renderContent = function(div) {
   CityPopup._C = this;

   var table = Popup.createElement("table");
   table.setAttribute("cellSpacing", "2");
   table.setAttribute("cellPadding", "2");
   table.setAttribute("className", "LookupTable");
   div.appendChild(table);

   var thead, tbody, tfoot, row, cell, input, image;
   thead = Popup.createElement("thead", table);
   row = Popup.createElement("tr", thead);

   cell = Popup.createElement("td", row);
   cell.setAttribute("className", "PopupHeader");
   cell.setAttribute("colSpan", "2");
   cell.style.cursor = "move";
   cell.popup = window.popup;
   cell.appendChild(document.createTextNode("City Lookup"));
   Popup.addDragListener(cell);

   tbody = Popup.createElement("tbody", table);
   row = Popup.createElement("tr", tbody);
   cell = Popup.createElement("td", row);

   // City
   row = Popup.createElement("tr", tbody);
   cell = Popup.createElement("td", row);
   cell.setAttribute("className", "labelRight");
   cell.appendChild(document.createTextNode("City:"));

   cell = Popup.createElement("td", row);
   input = Popup.createElement("input", cell);
   this.focusObj = input;
   input.setAttribute("id", "cityInput");
   input.setAttribute("tabIndex", "96");
   input.setAttribute("TYPE", "text");
   Popup.addEvent(input, "blur", fieldBlur);
   Popup.addEvent(input, "focus", fieldFocus);

   // State
   row = Popup.createElement("tr", tbody);
   cell = Popup.createElement("td", row);
   cell.setAttribute("className", "labelRight");
   cell.appendChild(document.createTextNode("State:"));

   cell = Popup.createElement("td", row);
   input = Popup.createElement("select", cell);
   input.setAttribute("id", "stateInput");
   input.setAttribute("tabIndex", "97");

   // Zip
   row = Popup.createElement("tr", tbody);
   cell = Popup.createElement("td", row);
   cell.setAttribute("className", "labelRight");
   cell.appendChild(document.createTextNode("Zip/Postal Code:"));

   cell = Popup.createElement("td", row);
   input = Popup.createElement("input", cell);
   input.setAttribute("id", "zipCodeInput");
   input.setAttribute("tabIndex", "98");
   input.setAttribute("TYPE", "text");
   Popup.addEvent(input, "blur", fieldBlur);
   Popup.addEvent(input, "focus", fieldFocus);

   // Buttons
   tfoot = Popup.createElement("tfoot", table);
   row = Popup.createElement("tr", tfoot);
   cell = Popup.createElement("td", row);
   cell.setAttribute("colSpan", "2");
   cell.setAttribute("align", "right");

   button = Popup.createElement("button", cell);
   button.setAttribute("value", "Search");
   button.setAttribute("className", "BlueButton");
   button.setAttribute("tabIndex", "99");
   Popup.addEvent(button, "click", CityPopup.searchCity);
   Popup.addEvent(button, "keydown", CityPopup.enterCitySearch);

   button = Popup.createElement("button", cell);
   button.setAttribute("value", "Close screen");
   button.setAttribute("className", "BlueButton");
   button.setAttribute("tabIndex", "100");
   Popup.addEvent(button, "click", Popup.hide);
   Popup.addEvent(button, "keydown", CityPopup.pressHide);
};

CityPopup.prototype.setFocus = function() {
   if (this.focusObj) {
      this.focusObj.focus();
   }
};

CityPopup.prototype.setValues = function() {
   var sState = document.getElementById(CityPopup._C.stateName);
   document.getElementById("cityInput").value = document.getElementById(CityPopup._C.cityName).value;
   document.getElementById("zipCodeInput").value = document.getElementById(CityPopup._C.zipName).value;
   populateStates(CityPopup._C.countryName, "stateInput", sState[sState.selectedIndex].value);
};
