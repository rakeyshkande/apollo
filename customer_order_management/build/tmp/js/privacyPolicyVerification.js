var MIN_CV_COUNT_REQUIRED      = "1";
var MIN_OV_COUNT_REQUIRED      = "2";


//***********************************************************************************************************************
//function checkKeyPressed()
//***********************************************************************************************************************
function checkKeyPressed()
{
  if(window.event.keyCode != 13)
    return false;
  else
    processSubmit();
}


//***********************************************************************************************************************
//function init()
//***********************************************************************************************************************
function init() 
{
  setNavigationHandlers();
  addDefaultListeners();

  //check for OV.  if N, do not display the checkbox and text for "not the right order" checkbox within VE checkboxes
  if (orderValidationRequiredFlag == 'N' && document.getElementById('VENotRight') != null)
  {
    document.getElementById('td_code_VENotRight').style.display = "none";
    document.getElementById('td_desc_VENotRight').style.display = "none";
  }
}


//***********************************************************************************************************************
//function toggleClick(checkboxName)
//***********************************************************************************************************************
function toggleClick(checkboxName)
{
  var cbClicked = document.getElementById(checkboxName); 
  var cb; 

  //if something was checked
  if (cbClicked != null && cbClicked.checked)
  {
    //uncheck all other LD checkboxes
    for(var i=0;i<ldCheckboxes.length;i++)
    {
      cb = document.getElementById(ldCheckboxes[i]); 
      if (cb != null && cb.name != cbClicked.name)
        cb.checked = false; 
    }

    //uncheck all other VE checkboxes
    for(var i=0;i<veCheckboxes.length;i++)
    {
      cb = document.getElementById(veCheckboxes[i]); 
      if (cb != null && cb.name != cbClicked.name)
        cb.checked = false; 
    }

    //uncheck and disable CV checkboxes
    for(var i=0;i<cvCheckboxes.length;i++)
    {
      cb = document.getElementById(cvCheckboxes[i]); 
      if (cb != null)
      {
        cb.checked = false; 
        cb.disabled = true; 
      }
    }

    //uncheck and disable OV checkboxes
    for(var i=0;i<ovCheckboxes.length;i++)
    {
      cb = document.getElementById(ovCheckboxes[i]); 
      if (cb != null)
      {
        cb.checked = false; 
        cb.disabled = true; 
      }
    }
  }
  else
  {
    //enable CV checkboxes
    for(var i=0;i<cvCheckboxes.length;i++)
    {
      cb = document.getElementById(cvCheckboxes[i]); 
      if (cb != null)
        cb.disabled = false; 
    }

    //enable OV checkboxes
    for(var i=0;i<ovCheckboxes.length;i++)
    {
      cb = document.getElementById(ovCheckboxes[i]); 
      if (cb != null)
        cb.disabled = false; 
    }

  }

}


//***********************************************************************************************************************
//function processSubmit()
//
//  NOTE: an empty returnArray means that the user did not hit the submit button.  
//        We HAVE to populate this array on submit otherwise the code will go in an infinite loop
//
//***********************************************************************************************************************
function processSubmit()
{

  var notEnough = false; 
  var noInfo = false; 
  var remember = false; 
  var cvCount = 0; 
  var ovCount = 0; 
  var ldCount = 0; 
  var veCount = 0; 
  var found = false; 
  var message; 
  
  var cb; 
  
  var form = document.forms[0];
  var url = "";
  for(var i = 0; i < form.elements.length; i++)
  {
    if(form.elements[i].type == 'checkbox' && form.elements[i].checked)
    {
      //get the checkbox
      cb = form.elements[i];

      //for EACH checkbox checked, find out which arraylist that checkbox belongs to.  Get a count of ALL checkboxes checked for each group
      if (cb != null && cb.checked)
      {
        found = false; 
        for(var c=0;c<cvCheckboxes.length;c++)
        {
          if (cb.id == cvCheckboxes[c])
          {
            cvCount++;
            found = true; 
            break;
          }
        }
        
        if (!found)
        {
          for(var o=0;o<ovCheckboxes.length;o++)
          {
            if (cb.id == ovCheckboxes[o])
            {
              ovCount++;
              found = true; 
              break;
            }
          }
        }
                                 
        if (!found)
        {
          for(var l=0;l<ovCheckboxes.length;l++)
          {
            if (cb.id == ldCheckboxes[l])
            {
              ldCount++;
              found = true; 
              break;
            }
          }
        }
                                 
        if (!found)
        {
          for(var v=0;v<veCheckboxes.length;v++)
          {
            if (cb.id == veCheckboxes[v])
            {
              veCount++;
              found = true; 
              break;
            }
          }
        }
                                 
      }//if checked
    }//if checkbox
  }//for


  if ( (cvCount + ovCount + ldCount + veCount) == 0)
  {
    message = document.getElementById('errorNoInfoMessage1').value + '<br/>' + '<br/>' + document.getElementById('errorNoInfoMessage2').value;
    showAlertDynamicDialog('R', '(R)etry', message, 'left', '');
    return false; 
  }
  else if (
            (customerValidationRequiredFlag == 'Y' && cvCount < MIN_CV_COUNT_REQUIRED && !(ldCount > 0 || veCount > 0)) ||
            (orderValidationRequiredFlag == 'Y' && ovCount < MIN_OV_COUNT_REQUIRED && !(ldCount > 0 || veCount > 0))
          )
  {          
    message = document.getElementById('errorNotEnoughMessage1').value + '<br/>' + '<br/>' + document.getElementById('errorNotEnoughMessage2').value;
    showAlertDynamicDialog('R', '(R)etry', message, 'left', '');
    return false; 
  }
  else
  {
    if (ldCount > 0)
    {
      message = document.getElementById('rememberMessage1').value + '<br/>' + '<br/>' + document.getElementById('rememberMessage2').value;
      showAlertDynamicDialog('C', '(C)lose',  message, 'left', '');
    }
  }


  //if Validation Exception checkbox is checked, based on the checkbox checked, set which page to give control to.  This is to simulate the "back" functionality
  if (veCount >0)
  {
    var vecb; 
    for(var j=0;j<veCheckboxes.length;j++)
    {
      vecb = document.getElementById(veCheckboxes[j]); 
      if (vecb != null && vecb.checked == true) 
      {
        /******* "Caller Unable to Validate" (VENotValid) *******/
        if (vecb.name == 'VENotValid')
        {
          //If customer is already validated, give control to CAI page. This will account for both Customer Account page as well as Recipient Order page. 
          //Invokation from CAI - If customer was already validated, CAI page would have never invoked PPV.  
          //Invokation from Shopping Cart - CAI Tab is available and give control to it
          if (customerValidationRequiredFlag == 'V')
            returnArray[0] = 'cai';
          //else give control to Customer Order Search page
          else
            returnArray[0] = 'cos';
        }
        /******* "Not the Right Order" (VENotRight) *******/
        //This option inherently assumes that we are on CSCI page.  Thus, give control to Customer Account tab
        else if (vecb.name == 'VENotRight')
        {
          returnArray[0] = 'cai';
        }
        /******* "Not a Call" (VENotCall) *******/
        //This option will set bypass validation flag.  no need to transfer control to any other page
        else
        {
          returnArray[0] = 'ok';
        }

        break;
      }
    }
  }
  //else, if all is ok, return "ok" in the returnArray. 
  else
    returnArray[0] = 'ok';


  var url = "customerOrderSearch.do" + "?action=insert_privacy_policy" + getSecurityParams(false);
  document.forms[0].action = url;
  document.forms[0].submit();
  top.returnValue = returnArray;
  top.close();

}



