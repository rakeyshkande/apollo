var isRefresh=false;
    function doOnLoad(){
        isRefresh=false;
        hideWaitMessage("content", "wait");
    }
    function doRefresh(){
        if(isRefresh){
            //var time = setInterval('alert(\'Please wait while your request is processing!\')', 2000);
            //clearInterval(time);
            alert("Please wait while your request is processing!")
            return;
        }
        isRefresh=true;
        showWaitMessage("content", "wait", "Please wait while your request is processing!");
        document.status.action = "queueBatchStatus.do?action=loadData";
        document.status.submit();
    }
    
    function switchContent(){
        $('waitDiv').style.display='block';
        $('content').innerHTML=$('waitDiv').innerHTML;
        return;
    }
    
    function goBackToCustomerServiceMenuAction() {
        document.status.action = "mainMenu.do?adminAction=customerService";
        document.status.target = "_top";
        document.status.submit();
    }
    
    function goBackToDeleteAction() {
        document.status.action = "queueMessageDelete.do?action_type=load&adminAction="+document.status.adminAction.value+"&context="+document.status.context.value+"&securitytoken="+document.status.securitytoken.value;
        document.status.target = "_top";
        document.status.submit();
    }