/*
 * Constructor
 */
SourceCodePopup = function(params) {
   this.focusObj = null;
   this.sourceCodeName = params.sourceCode;
   this.sourceCodeDescription = params.sourceCodeDescription;
};
SourceCodePopup._S = null;

/*
 * Class functions
 */
SourceCodePopup.setup = function(params) {
   window.popup = Popup.setup(params, new SourceCodePopup(params));
   window.popup.create();
   window.popup.showAtElement(params.displayArea);
};

SourceCodePopup.enterSearchSourceCode = function(ev) {
   if (ev.keyCode == 13)
      SourceCodePopup.searchSourceCode(ev);
};

SourceCodePopup.pressHide = function () {
  if (window.event.keyCode == 13)
      Popup.hide();
  // if tab on close button, return to first button
  if (window.event.keyCode == 9){
     var sourceCode = document.getElementById("sourceCodeInput");
      sourceCode.focus();
      return false;
  }
};

SourceCodePopup.searchSourceCode = function(ev) {
   var sourceCode = document.getElementById("sourceCodeInput");
   var sourceCodeValue = stripWhitespace(sourceCode.value);

   if ( sourceCodeValue == "" ) {
      alert("Please correct the marked fields");
      sourceCode.focus();
      sourceCode.style.backgroundColor = "pink";
    return false;
   }

   var url_source = "lookupSourceCode.do" +
                    getSecurityParams(true) +
                    "&sourceCodeInput=" + document.getElementById("sourceCodeInput").value +
                    "&companyId=" + document.getElementById("company_id").value;

   var modal_dim = "dialogWidth:800px; dialogHeight:500px; center:yes; status=0";
   var ret = showModalDialogIFrame(url_source,"", modal_dim);

   if (ret && ret != null && ret[0]){
      document.getElementById(SourceCodePopup._S.sourceCodeName).value = ret[0];
   }
   Popup.hide();
   SourceCodePopup._S = null;
};

/*
 * Member Functions
 */
SourceCodePopup.prototype.renderContent = function(div) {
   SourceCodePopup._S = this;
   var table = Popup.createElement("table");
   table.setAttribute("cellSpacing", "2");
   table.setAttribute("cellPadding", "2");
   table.setAttribute("className", "LookupTable");
   div.appendChild(table);

   var thead, tbody, tfoot, row, cell, input, image;
   thead = Popup.createElement("thead", table);
   row = Popup.createElement("tr", thead);

   cell = Popup.createElement("td", row);
   cell.setAttribute("className", "PopupHeader");
   cell.setAttribute("colSpan", "2");
   cell.style.cursor = "move";
   cell.popup = window.popup;
   cell.appendChild(document.createTextNode("Source Code Lookup"));
   Popup.addDragListener(cell);

   tbody = Popup.createElement("tbody", table);
   row = Popup.createElement("tr", tbody);
   cell = Popup.createElement("td", row);

   row = Popup.createElement("tr", tbody);
   cell = Popup.createElement("td", row);
   cell.setAttribute("className", "label");
   cell.appendChild(document.createTextNode("Source Code or Description:"));

   cell = Popup.createElement("td", row);
   input = Popup.createElement("input", cell);
   this.focusObj = input;
   input.setAttribute("id", "sourceCodeInput");
   input.setAttribute("tabIndex", "85");
   input.setAttribute("TYPE", "text");
   Popup.addEvent(input, "blur", fieldBlur);
   Popup.addEvent(input, "focus", fieldFocus);
   Popup.addEvent(input, "keydown", SourceCodePopup.enterSearchSourceCode);

   tfoot = Popup.createElement("tfoot", table);
   row = Popup.createElement("tr", tfoot);
   cell = Popup.createElement("td", row);
   cell.setAttribute("colSpan", "2");
   cell.setAttribute("align", "right");

   button = Popup.createElement("button", cell);
   button.setAttribute("value", "Search");
   button.setAttribute("className", "BlueButton");
   button.setAttribute("tabIndex", "86");
   Popup.addEvent(button, "click", SourceCodePopup.searchSourceCode);
   Popup.addEvent(button, "keydown", SourceCodePopup.enterSearchSourceCode);

   button = Popup.createElement("button", cell);
   button.setAttribute("value", "Close screen");
   button.setAttribute("className", "BlueButton");
   button.setAttribute("tabIndex", "87");
   Popup.addEvent(button, "click", Popup.hide);
   Popup.addEvent(button, "keydown", SourceCodePopup.pressHide);
};

SourceCodePopup.prototype.setFocus = function () {
   if (this.focusObj) {
      this.focusObj.focus();
   }
};

SourceCodePopup.prototype.setValues = function (params) {
   document.getElementById("sourceCodeInput").value = document.getElementById(SourceCodePopup._S.sourceCodeName).value;
};
