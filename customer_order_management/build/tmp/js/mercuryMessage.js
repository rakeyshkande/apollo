//TODOs: fields || stub || modal || hardcoded || commented

var isRecordLocked = false;

function contextMenuHandler()
{
   //return false;
}

function doBackAction() {
	var form = document.forms[0];
	form.action = "displayCommunication.do?order_detail_id=" + form.order_detail_id.value;
	form.submit();
}

function backKeyHandler()
{
   // backspace
   if (window.event && window.event.keyCode == 8){
      var formElement = false;

      for(i = 0; i < document.forms[0].elements.length; i++){
         if(document.forms[0].elements[i].name == document.activeElement.name){
            formElement = true;
            break;
         }
      }
      var searchBox = document.getElementById('numberEntry');
      if(searchBox != null){
        if(document.getElementById('numberEntry').id == document.activeElement.id){
          formElement = true;
        }
      }

      if(formElement == false || (document.activeElement.type != "text" && document.activeElement.type != "textarea" && document.activeElement.type != "password")){
         window.event.cancelBubble = true;
         window.event.returnValue = false;
         return false;
      }
   }

   // F5 and F11
   if (window.event.keyCode == 122 || window.event.keyCode == 116){
      window.event.keyCode = 0;
      event.returnValue = false;
      return false;
   }

   // Alt + Left Arrow
   if(window.event.altLeft){
      window.event.returnValue = false;
      return false;
   }

   // Alt + Right Arrow
   if(window.event.altKey){
      window.event.returnValue = false;
      return false;
   }
}

/*
	Catch all numeric keypresses and send them to the search box.
*/
function evaluateKeyPress()
{
	var a;
	var keypress = window.event.keyCode;

	if (keypress == 48||keypress==96)
		a = 0;
	if (keypress == 49||keypress==97)
		a = 1;
	if (keypress == 50||keypress==98)
		a = 2;
	if (keypress == 51||keypress==99)
		a = 3;
	if (keypress == 52||keypress==100)
		a = 4;
	if (keypress == 53||keypress==101)
		a = 5;
	if (keypress == 54||keypress==102)
		a = 6;
	if (keypress == 55||keypress==103)
		a = 7;
	if (keypress == 56||keypress==104)
		a = 8;
	if (keypress == 57||keypress==105)
		a = 9;

	if (a >= 0 && a <= 9)
	{
		document.getElementById("numberEntry").focus();
	}
}


/*
Remove most of the elements in the page header from the tabbing sequence.
*/
function untabHeader()
{
	var printerIcon = document.getElementById("printer");
	if(printerIcon != null)
	{
		printerIcon.parentNode.tabIndex = -1;
	}

	var viewedBy = document.getElementById("currentCSRs");
	if(viewedBy != null)
	{
		viewedBy.tabIndex = -1;
	}

	var csrIframe = document.getElementById("CSRFrame");
	if(csrIframe != null)
	{
		csrIframe.tabIndex = -1;
	}

	var backButton = document.getElementById("backButtonHeader");
	if(backButton != null)
	{
		backButton.tabIndex = -1;
	}

	var inputElements = document.getElementsByTagName("input");
	var type;
	for(var i = 0; i < inputElements.length; i++)
	{
		type = inputElements[i].getAttribute("type");
		// Untab any hidden input fields on the page
		if(type == "hidden")
		{
			inputElements[i].setAttribute("tabindex", "-1");
		}
	}
}


/*
   This is a helper function to return the two security parameters
   required by the security framework.

   areOnlyParams -   is used to change the url string (? vs. &) depending if these
					 are the only parameters in the url
*/
function getSecurityParams(areOnlyParams)
{
	return ((areOnlyParams) ? "?" : "&")
			+ "securitytoken=" + document.getElementById("securitytoken").value
			+ "&context=" + document.getElementById("context").value;
}


/*
This function is executed when the csr
uses the search box. If the csr hits the 'enter' key
the associated td with the same id will be located and the customer
id is grabbed from the hidden field and a search is performed.
*/
function doEntryAction()
{
	if(window.event.keyCode != 13)
	{
		return false;
	}

	if(isWhitespace(event.srcElement.value))
	{
		window.event.returnValue = false;
		return false;
	}


	var messageNumber = stripZeros(stripWhitespace(event.srcElement.value)).toUpperCase();
	var messageNameRegex = /^\d+E*$/;
	if(messageNameRegex.test(messageNumber))
	{
		openMessage(messageNumber);
		window.event.returnValue = false;
		return false;
	}
	else
	{
		alert(messageNumber + " isn't a valid message number.");
		window.event.returnValue = false;
		return false;
	}
}

/*
Using the specified URL submit the form
*/
function performAction(url)
{
//	showWaitMessage("content", "wait", "Searching");
	document.forms[0].action = url;
  document.forms[0].submit();
}


/*
Handlers for the mercMessButtons.xsl footer buttons.
*/
function openRecipientOrder()
{
	var url = "customerOrderSearch.do?action=customer_account_search"
				+ "&order_number=" + document.getElementById("msg_external_order_number").value
				+ "&order_guid=" + document.getElementById("msg_guid").value
				+ "&customer_id=" + document.getElementById("msg_customer_id").value;

	performAction(url);
}

function openHistory()
{
	var url = "loadHistory.do?history_type=Order&page_position=1"
				+ "&order_detail_id=" + document.getElementById("order_detail_id").value;

	performAction(url);
}

function openOrderComments()
{
	var url = "loadComments.do"
				+ "?page_position=1"
				+ "&comment_type=Order"
				+ "&order_detail_id=" + document.getElementById("order_detail_id").value
				+ "&order_guid="
				+ document.getElementById("msg_guid").value
				+ "&customer_id="
				+ document.getElementById("msg_customer_id").value;

	performAction(url);
}

function openEmail()
{
	if(document.getElementById("msg_email_indicator").value == "true")
	{
		var url = "loadSendMessage.do?message_type=Email&action_type=load_titles&external_order_number="
					+ document.getElementById("msg_external_order_number").value;

		performAction(url);
	}
	else
	{
		alert("There is no email address on file for this customer.");
	}
}

function openLetters()
{
	var url = "loadSendMessage.do?message_type=Letter&action_type=load_titles&external_order_number="
				+ document.getElementById("msg_external_order_number").value;

	performAction(url);
}

function openRefund()
{
	var url = "loadRefunds.do?order_detail_id="
				+ document.getElementById("order_detail_id").value;

	performAction(url);
}

function openFloristInquiry()
{
	  var sURL = "incorrectOrderDisposition.html";
	  var objFlorist = new Object();

	  var   url = document.getElementById("load_member_data_url").value;
	        url += "action=loadFloristSearch" + getSecurityParams(false);

	  objFlorist.scrubURL = url;
	  objFlorist.orderStatus = '';
	  objFlorist.callingPage = 'customerShoppingCart';
	  //Features
	  var sFeatures="dialogHeight:500px;dialogWidth:950px;";
	    sFeatures +=  "status:1;scroll:1;resizable:0";

	  window.showModalDialog(sURL, objFlorist, sFeatures);
}

/*
Handler for the communicationScreen.xsl and floristLookup.xsl functionality of clicking on a
florist's number in the table of Mercury messages, being taken to that florist's information
page.
*/
function openFlorist(floristCode)
{
	var sURL = "incorrectOrderDisposition.html";
	  var objFlorist = new Object();

	  /* build url used to access load member data */
	   var  url = document.forms[0].load_member_data_url.value;
	         url += "action=searchFlorist" + "&sc_floristnum=" + floristCode + getSecurityParams(false);

	  objFlorist.scrubURL = url;
	  objFlorist.orderStatus = '';
	  objFlorist.callingPage = 'customerShoppingCart';

	 //Features
	           var sFeatures="dialogHeight:500px;dialogWidth:950px;";
	          sFeatures +=  "status:0;scroll:1;resizable:0";

	  window.showModalDialog(sURL, objFlorist, sFeatures);
}

/*
Handler for the "(M)ain Menu" button, as created by mercMessButtons.xsl.

function doMainMenuAction()
{
	var url = '';

	//Modal_URL
	var modalUrl = "confirm.html";

	//Modal_Arguments
	var modalArguments = new Object();
	modalArguments.modalText = 'Have you completed your work on this order?';

	//Modal_Features
	var modalFeatures = 'dialogWidth:200px; dialogHeight:150px; center:yes; status=no; help=no;';
	modalFeatures += 'resizable:no; scroll:no';

	//get a true/false value from the modal dialog.
	var ret = window.showModalDialog(modalUrl, modalArguments, modalFeatures);

	if(ret)
	{
		url = "mainMenu.do?work_complete=y" + getSecurityParams(false);
	}
	else
	{
		url = "mainMenu.do?work_complete=n" + getSecurityParams(false);
	}

	performAction(url);
}
*/

/*
Handler for the communicationScreen.xsl "(Q)Delete" button in the Mercury
message table. This button creates a popup window listing the messages in the queue, as
created by the qDelete.xsl.
*/
function openQDelete()
{

	var args = new Array();

	args.action = "displayQDelete.do";

  /*
	The action needs a list of all the messages on the communication screen. The list will
	consist of all the messages mercury ids. All the message hidden input
	fields are named "n" where n is that message''s line number. Their values are the
	concatenation of their mercury ids and their message types(mercury, venus, email or
	letter), separated by a "|" character. We use an appropriate regular expression to select
	the message hidden fields, split off just the message id portions of those fields''	values,
	and use them to make a list of ids we pass to the displayQDelete action.
  */
	var formFields = document.getElementById("communicationScreenForm").elements;
	var messageNameRegex = /^\d+E*$/;

	var messageId;

	for(var i = 0; i < formFields.length; i++)
	{
		//alert("name="+formFields[i].name+" value="+formFields[i].value);
		if(messageNameRegex.test(formFields[i].name))
		{
			//alert("name="+formFields[i].name);
			messageId = formFields[i].value.split("|");

			args[args.length] = new Array(formFields[i].name, messageId[0]);
		}
	}

	args[args.length] = new Array("order_detail_id", document.getElementById("order_detail_id").value);
	args[args.length] = new Array("securitytoken", document.getElementById("securitytoken").value);
	args[args.length] = new Array("context", document.getElementById("context").value);

	var modal_dim = "dialogWidth:570px; dialogHeight:245px; dialogLeft:125; dialogTop: 190; center:yes; status=0; help:no; scroll:yes;";
	var shouldReload = window.showModalDialog("html/waitModalDialog.html", args, modal_dim);

	if(shouldReload == "Y")
	{
		//window.location.reload(true);
		url = "displayCommunication.do?";

		//We need to refresh the communications window to reflect any deletions that were just made
		//in the qdelete popup screen, and so we must append the relevant hidden field values to the
		//url so they'll be preserved.
		var inputElements = document.getElementsByTagName("input");
		var type;

		//We'll need a regular expression matching purely numeric strings to weed out the message
		//fields.
		var numberRegex = /^\d+E*$/;
		var isNumber;
		for(var i = 0; i < inputElements.length; i++)
		{
			type = inputElements[i].getAttribute("type");
			//We don't bother sending along hidden field values which are null, and we don't want to
			//send the message hidden fields. Checking for null values is simple, and all the
			//message hidden fields have purely numeric names, unlike all the other hidden fields,
			//so we can use our simple numeric regular expression to identify them.
			if((type == "hidden") && (inputElements[i].value) && (inputElements[i].value != ""))
			{
				isNumber = numberRegex.test(inputElements[i].name)
				if(!isNumber)
				{
					url += "&" + inputElements[i].name + "=" + inputElements[i].value;
				}
			}
		}

		window.location = url;
	}
}

/*
Handler for the communicationScreen.xsl "Florist (D)ashboard" button in the Mercury
message table. This button creates a popup window providing florist data relating to
the order, as created by the floristDashboard.xsl.
*/
function openDashboard()
{
	var args = new Array();

	args.action = "displayFloristDashboard.do";

	args[0] = new Array("action_type", "florist_dashboard");
	args[1] = new Array("order_detail_id", document.getElementById("order_detail_id").value);
	args[2] = new Array("securitytoken", document.getElementById("securitytoken").value);
	args[3] = new Array("context", document.getElementById("context").value);

	var modal_dim = "dialogWidth:570px; dialogHeight:245px; dialogLeft:125; dialogTop: 190; center:yes; status=0; help:no; scroll:no";

	window.showModalDialog("html/waitModalDialog.html", args, modal_dim);
}


/*
Handler for the message type links in communicationScreen.xsl. This function accepts an
integer parameter representing the message number whose details it will attempt to bring up.
*/
function openMessage(recordNumber)
{

	var hiddenMessageField = document.getElementById(recordNumber.toString());
	if(hiddenMessageField == null)
	{
		alert("There is no message " + recordNumber + " on this page.");
		window.event.returnValue = false;
		return false;
	}
	else
	{
		document.forms[0].message_info.value = hiddenMessageField.value;
		document.forms[0].message_line_number.value = recordNumber;

		document.forms[0].submit();
	}
}




/*
This function implements the checkLockIFrame.xsl this will either display a message
to the user and set the isRecordLocked variable to false or isRecordLocked to true
*/
function checkLockProcessRequest(actionType){
  var frame = document.getElementById('checkLock');
  var sessionId=new Date().getTime();//append to url to force iframe refresh each time.
  var url = "messagingLock.do?action_type="+actionType+"&order_detail_id=" + document.forms[0].order_detail_id.value
            + getSecurityParams(false)+"&sessionId="+sessionId;
  frame.src = url;
}

/*
This function implements the checkLockIFrame.xsl this will either display a message
to the user and set the isRecordLocked variable to false or isRecordLocked to true
*/
function checkLockProcessRequest(actionType, floristSelectionLogId, zipCityFlag){
  var frame = document.getElementById('checkLock');
  var sessionId=new Date().getTime();//append to url to force iframe refresh each time.
  var url = "messagingLock.do?action_type="+actionType+"&order_detail_id=" + document.forms[0].order_detail_id.value
            + "&florist_selection_log_id="+floristSelectionLogId 
            + "&zip_city_flag="+zipCityFlag + getSecurityParams(false)+"&sessionId="+sessionId;
  frame.src = url;
}


/*
This function implements the checkLockIFrame.xsl this will either display a message
to the user and set the isRecordLocked variable to false or isRecordLocked to true
*/
function releaseLock(actionType, appName){
  var frame = document.getElementById('unlockFrame');
  var sessionId=new Date().getTime();//append to url to force iframe refresh each time.
  var url = "messagingUnLock.do?action_type="+actionType+"&order_detail_id=" + document.forms[0].order_detail_id.value
  	     +"&lock_app="+appName
            + getSecurityParams(false)+"&sessionId="+sessionId;
  frame.src = url;
  //alert(url)


}



function processRequest(actionType){
    if(actionType=='message_forward'){
         doMessageForward();
    }else if(actionType=='send_message'){
         doSendMessagingAction();
    }else if(actionType=='send_price_message'){
    	 doSendPriceMessageAction();
    }else if(actionType=='send_adjustment'){
    	doSendAdjustmentAction();
    }else if(actionType=='send_cancel'){
       doSendCancelAction();
    }else if(actionType=='update_florist'){
       doUpdateFlorist();
    }else if(actionType=='send_cancel_ftdm'){
       doSendCancelFTDMAction();

    }else if(actionType=='send_cancel_vendor'){
       doSendCancelVendorAction();

    }else if(actionType=='call_out'){
       doCalloutAction();
    }else if(actionType=='do_close'){
       doClose();
    }else{
       //alert("doSendNewFTDMessageAction "+actionType);
       doSendNewFTDMessageAction(actionType);
    }



}

function processRequest(actionType, floristSelectionLogId, zipCityFlag){
    if(actionType=='message_forward'){
         doMessageForward();
    }else if(actionType=='send_message'){
         doSendMessagingAction();
    }else if(actionType=='send_price_message'){
    	 doSendPriceMessageAction();
    }else if(actionType=='send_adjustment'){
    	doSendAdjustmentAction();
    }else if(actionType=='send_cancel'){
       doSendCancelAction();
    }else if(actionType=='update_florist'){
       doUpdateFlorist(floristSelectionLogId, zipCityFlag);
    }else if(actionType=='send_cancel_ftdm'){
       doSendCancelFTDMAction();

    }else if(actionType=='send_cancel_vendor'){
       doSendCancelVendorAction();

    }else if(actionType=='call_out'){
       doCalloutAction();
    }else if(actionType=='do_close'){
       doClose();
    }else{
       //alert("doSendNewFTDMessageAction "+actionType);
       doSendNewFTDMessageAction(actionType, floristSelectionLogId, zipCityFlag);
    }



}
/*
Handler for the "I would like to:" drop down box. This box contains a number of options
for determining precisely how a message should be forwarded. It appears in
viewMessage.xsl, viewPriceMessage.xsl, and viewFTDMessage.xsl.
*/



function doMessageForward()
{
	var selectBox = document.getElementById("would_like_to");
	var action = selectBox.options[selectBox.selectedIndex].value;
  var msgType = document.getElementById("msg_message_type").value;

	var ftdMsgId=document.getElementById("ftd_message_id");
	var sendingFloirst=document.getElementById("sending_florist_code");
	var fillingFloirst=document.getElementById("filling_florist_code");
	var floristSelectionLogId=document.getElementById("florist_selection_log_id");
	var ftdMessageStatus=document.getElementById("msg_ftd_message_status");
	var ftdCancelled=document.getElementById("ftd_can_rej_flag").value;
	var zipCityFlag=document.getElementById("zip_city_flag");
	//alert("ftdCancelled="+ftdCancelled);
  if(action == "pay_original_send_new")
	{
		if(ftdCancelled!="true"){
			//Modal_URL
			var modalUrl = "confirm.html";

			//Modal_Arguments
			var modalArguments = new Object();
			modalArguments.modalText = "Do you want to cancel the original order?";

			//Modal_Features
			var modalFeatures = 'dialogWidth:200px; dialogHeight:150px; center:yes; status=no; help=no;';
			modalFeatures += 'resizable:no; scroll:no';

			//get a true/false value from the modal dialog.
			var choseYes = window.showModalDialog(modalUrl, modalArguments, modalFeatures);

			if(choseYes)
			{
				alert("You must select 'Cancel Original Order & Send New FTD.'");
				return false;
			}
		}
	}

  if(action == "send_comp_order"
     || msgType == "Mercury" && action == "pay_original_send_new")
	{
    var invalidManagerInfo = false;
    var approvalId = document.getElementById("approval_id").value;
    if(isWhitespace(approvalId))
    {
      invalidManagerInfo = true;
    }
    var approvalPassword = document.getElementById("approval_passwd").value;
    if(isWhitespace(approvalPassword))
    {
      invalidManagerInfo = true;
    }

    if(invalidManagerInfo)
    {
      parent.document.getElementById("approval_id").style.backgroundColor='pink';
      parent.document.getElementById("approval_passwd").style.backgroundColor='pink';
      alert("Incorrect manager login and/or password.  Please correct marked fields.");
      parent.document.getElementById("approval_id").focus();
      return false;
    }
  }


  showWaitMessage("content", "wait");

	var url = "prepareMessage.do?order_detail_id="
				+ document.getElementById("order_detail_id").value
				+ "&msg_message_order_number="
				+ document.getElementById("msg_message_order_number").value
				+ "&msg_message_type="
				+ document.getElementById("msg_message_type").value
				+ "&action_type="
				+ action+ getSecurityParams(false);
	if(ftdMsgId!=null){
			url =url+"&ftd_message_id="+document.getElementById("ftd_message_id").value;
	}
	if(sendingFloirst!=null){
			url =url+"&sending_florist_code="+document.getElementById("sending_florist_code").value;
	}
	if(fillingFloirst!=null){
			url =url+"&filling_florist_code="+document.getElementById("filling_florist_code").value;
	}
	if(floristSelectionLogId!=null){
		url =url+"&florist_selection_log_id="+document.getElementById("florist_selection_log_id").value;
    }
	if(zipCityFlag!=null){
		url =url+"&zip_city_flag="+document.getElementById("zip_city_flag").value;
    }

	if(ftdMessageStatus!=null){
			url =url+"&msg_ftd_message_status="+document.getElementById("msg_ftd_message_status").value;
	}

  if(action == "send_comp_order"
     || msgType == "Mercury" && action == "pay_original_send_new")
	{
    url = url+"&manager_code="+approvalId
          +"&manager_password="+approvalPassword;
  }
  

	//alert(url);
	MessageForwardFrame.location.href = url;
}

function resetFormActionAndSubmit(forwardAction, subsequentAction, orderType, orderStatus, cancelled, cdispCancelled)
{
	//set parent window's forward action field.
	if(document.forms[0] && forwardAction != "")
	{
		document.forms[0].action = forwardAction;
	}

	//set parent window's subsequent action field, if it has one.
	if(document.forms[0].subsequent_action && subsequentAction != "")
	{
		document.forms[0].subsequent_action.value = subsequentAction;
	}

	//set parent window's msg_order_type field, if it has one.
	if(document.forms[0].msg_order_type && orderType != "")
	{
		document.forms[0].msg_order_type.value = orderType;
	}

	//set parent window's msg_order_status field, if it has one.
	if(document.forms[0].msg_order_status && orderStatus != "")
	{
		document.forms[0].msg_order_status.value = orderStatus;
	}

	//set parent window''s msg_ftd_message_cancelled field, if it has one.
	if(document.forms[0].msg_ftd_message_cancelled && cancelled != "")
	{
		document.forms[0].msg_ftd_message_cancelled.value = cancelled;
	}

	if(document.forms[0].cdisp_cancelledOrder && cdispCancelled != "")
	{
		document.forms[0].cdisp_cancelledOrder.value = cdispCancelled;
	}

	document.forms[0].submit();
}

/*
Handler for the "(Q)Delete" button in the qDelete.xsl.
*/
function doQDeleteAction()
{
	//alert("in doQDeleteAction");
	var toBeDeleted = "";
	if(qDeleteForm.messageCheckboxes != null){
		var len= qDeleteForm.messageCheckboxes.length
		if(len==null&&qDeleteForm.messageCheckboxes.checked){
			toBeDeleted=qDeleteForm.messageCheckboxes.value;
		}else if(len > 0)
		{
			//alert("in doQDeleteAction checkboxes");

			for(var i = 0; i < qDeleteForm.messageCheckboxes.length; i++)
			{
				if(qDeleteForm.messageCheckboxes[i].checked)
				{
					toBeDeleted += qDeleteForm.messageCheckboxes[i].value + ',';
				}
			}
			//remove the last ","
			toBeDeleted = toBeDeleted.slice(0, -1);

		}
	}
	//alert("toBeDeleted="+toBeDeleted);
	if(toBeDeleted != "")
	{


		var url = "deleteQueue.do?action_type=delete"
					+ "&order_detail_id=" + document.getElementById("order_detail_id").value
					+ "&queue_ids=" + toBeDeleted;


		performAction(url);
	}
	else
	{
		alert("You must select at least one message to delete.");
	}
}

/*
Handler for the "(C)lose" buttons in the qDelete screen. The qDelete modal popup window
must be closed, and the parent window, the Communication screen, must be updated, since
it''s possible that deletions made on the qDelete screen have caused some of the messages
on the Communications screen to cease to exist.
*/
function closeQDelete()
{
	top.returnValue = shouldReload;

	top.close();
}

/*
Handler for the "QDelete(A)ll" button in the qDelete.xsl. This action checks all the boxes
in the table, and then invokes the doQDeleteAction() function to have them all deleted.
*/
function checkAll()
{
	if(qDeleteForm.messageCheckboxes != null)
	{
		//alert("qDeleteForm.messageCheckboxes = "+qDeleteForm.messageCheckboxes.length);
		var msgLength=qDeleteForm.messageCheckboxes.length;
		//Check all the messages in the table
		if(msgLength!=null){//multiple checkboxes
			for(var i = 0; i < qDeleteForm.messageCheckboxes.length; i++)
			{
				if(!qDeleteForm.messageCheckboxes[i].checked)
				{
					qDeleteForm.messageCheckboxes[i].checked = true;
				}
			}
		}else{ //only one checkbox.
			qDeleteForm.messageCheckboxes.checked = true;
		}

		doQDeleteAction();
	}
}



/*
Validation Functions which are specific to Mercury Messaging.
*/
function isFloristCode(str)
{
	var floristCodeRegex = /^\s*\d{2}-\d{4}[a-zA-Z][a-zA-Z]\s*$/;

	return floristCodeRegex.test(str);
}

function checkFloristCode(elementName, errorFieldName)
{
	var code = document.getElementById(elementName).value;

	if(isWhitespace(code))
	{
		writeError(errorFieldName, "Required Field");
		return false;
	}

	if(!isFloristCode(code))
	{
		writeError(errorFieldName, "Invalid format, must be 99-9999AA");
		return false;
	}

	document.getElementById(errorFieldName).style.display = "none";
	return true;
}

function checkRecipientName(elementName, errorFieldName)
{
	var rName = document.getElementById(elementName).value;

	if(isWhitespace(rName))
	{
		writeError(errorFieldName, "Required Field");
		return false;
	}

	document.getElementById(errorFieldName).style.display = "none";
	return true;
}

function checkRecipientStreet(elementName, errorFieldName)
{
	var street = document.getElementById(elementName).value;

	if(isWhitespace(street))
	{
		writeError(errorFieldName, "Required Field");
		return false;
	}

	document.getElementById(errorFieldName).style.display = "none";
	return true;
}

function checkRecipientCity(elementName, errorFieldName)
{
	var city = document.getElementById(elementName).value;

	if(isWhitespace(city))
	{
		writeError(errorFieldName, "Required Field");
		return false;
	}

	document.getElementById(errorFieldName).style.display = "none";
	return true;
}

function checkRecipientState(elementName, errorFieldName)
{
	var state = document.getElementById(elementName).value;

	if(isWhitespace(state))
	{
		writeError(errorFieldName, "Required Field");
		return false;
	}

	document.getElementById(errorFieldName).style.display = "none";
	return true;
}

function checkRecipientZip(elementName, errorFieldName)
{
	var zip = document.getElementById(elementName).value;

	if(isWhitespace(zip))
	{
		writeError(errorFieldName, "Required Field");
		return false;
	}

	document.getElementById(errorFieldName).style.display = "none";
	return true;
}

function checkOrderHasDeliveryRestrictions()
{
	var delDate = new Date(document.forms["payOriginalNewFTDForm"].delivery_date.value);
	var delDay = delDate.getDay();
	if (delDay != null)
	{	
		
		var isFTDWOrder = document.forms["payOriginalNewFTDForm"].is_ftdw_order.value;
		
		if(document.forms["payOriginalNewFTDForm"].order_has_morning_delivery.value == 'Y' && delDay == 1 && isFTDWOrder == 'false')
		{
			return false;               				
		}
		else
		{
			return true;
		}
	}
	return true;
}

function isPhone(str)
{
	var phoneRegex = /^\s*\d{3}\/\d{3}-\d{4}\s*$/;
	return phoneRegex.test(str);
}

function checkRecipientPhone(elementName, errorFieldName)
{
	var valid=true;

	var area = document.getElementById(elementName+"_area").value;

	var prefix = document.getElementById(elementName+"_prefix").value;
	var number = document.getElementById(elementName+"_number").value;


	if(isWhitespace(area))
	{
		writeError(errorFieldName, "Area Code is required");
		valid= false;
	}

	if(isWhitespace(prefix))
	{
		writeError(errorFieldName, "Prefix is required");
		valid= false;
	}

	if(isWhitespace(number))
	{
		writeError(errorFieldName, "Number is required");
		valid= false;
	}

	if(valid){
		document.getElementById(errorFieldName).style.display = "none";
	}

	return valid;
}

function checkOrderDeliveryDate(monthName, dayName, errorFieldName)
{

	var newDelDate;
	var month = document.getElementById(monthName).value;
	var date = document.getElementById(dayName).value;

	//alert("date name="+dayName +" value="+date);

	//Check that they entered a date.
	if(isWhitespace(date))
	{
		writeError(errorFieldName, "Required Field");
		return false;
	}

	//Check that date field is one or two digits.
	var dateRegex = /^\s*\d{1,2}\s*$/;
	if(!dateRegex.test(date))
	{
		writeError(errorFieldName, "Format: ##");
		return false;
	}else{

	     date=stripZeros(date);

	}

	//Check that date is within legal range for any date
	var dateNum = parseInt(date);
	//alert("date ="+dateNum);
	var monthNum=parseInt(month);

	if(dateNum > 31 || dateNum < 1)
	{
		writeError(errorFieldName, "Invalid Date");
		return false;
	}


	/*
	Create a date object for the order based on the form values. The time is set to the
	maximum	value in case the order is for same-day delivery. This makes the delivery date
	no later than whenever this order was processed on that day. Thus, it''s possible
	to create an order whose delivery date is now, or only seconds away; the issue was
	deemed unimportant for now.
	*/
	//Check that order date <= 90 in the future.
	//Compute the deadline date: today + 90 days.
	var today = new Date();
	var deadline=new Date();
	deadline.setDate(today.getDate() + 120);
	if(monthNum>today.getMonth()){
		newDelDate=new Date(today.getFullYear(), monthNum, dateNum);

	}else if(monthNum<today.getMonth()){//assume be next year.
		newDelDate=new Date(today.getFullYear()+1, monthNum, dateNum);
	}else{
		if(dateNum<today.getDate()){
			newDelDate=new Date(today.getFullYear()+1, monthNum, dateNum);
		}else{
			newDelDate=new Date(today.getFullYear(), monthNum, dateNum);
		}
	}

	newDelDate.setUTCHours(23, 59, 59, 999);
	deadline.setUTCHours(23, 59, 59, 999);
	if(newDelDate.valueOf() > deadline.valueOf())
	{
		var errorMessage = "The delivery date cannot be more than 120 days in the future or past the current date";
		writeError(errorFieldName, errorMessage);

		return false;
	}

	document.getElementById(errorFieldName).style.display = "none";
	return true;
}

function checkOrderDeliveryDateEnd(elementName, errorFieldName)
{
	var orderDateString = document.getElementById(elementName).value.split(" ");
	var month = orderDateString[0];
	var date = stripZeros(orderDateString[1]);

	//Check that they entered a date.
	if(isWhitespace(date))
	{
		writeError(errorFieldName, "Required Field");
		return false;
	}

	//Check that date field is one or two digits.
	var dateRegex = /^\s*\d{1,2}\s*$/;
	if(!dateRegex.test(date))
	{
		writeError(errorFieldName, "Format: ##");
		return false;
	}

	//Check that date is within legal range for any date
	var dateNum = parseInt(date);

	if(dateNum > 31 || dateNum < 1)
	{
		writeError(errorFieldName, "Invalid Date");
		return false;
	}


	/*
	Create a date object for the order based on the form values. The time is set to the
	maximum	value in case the order is for same-day delivery. This makes the delivery date
	no later than whenever this order was processed on that day. Thus, it's possible
	to create an order whose delivery date is now, or only seconds away; the issue was
	deemed unimportant for now.
	*/
	var today = new Date();

	var orderDate = new Date(date + " " + month + " " + today.getFullYear());
	orderDate.setUTCHours(23, 59, 59, 999);

	/*
	There is no year associated with the order. Assuming that we don't get orders whose
	dates have expired implies that an order whose date is before today is for next year.
	*/
//	if(orderDate.valueOf() < today.valueOf())
//	{
//		orderDate.setFullYear(orderDate.getFullYear() + 1);
//	}

	//Convert the date to a string so we can parse it.
	var myDate_string = orderDate.toUTCString();

	/*
	Split the string at every space and put the values into an array so,
	using the previous example, the first element in the array is "Wed", the
	second element is "Jan", the third element is "1", etc.
	*/
	var myDate_array = myDate_string.split(" ");

	/*
	If we entered "Feb 31, 1975" in the form, the "new Date()" function
	converts the value to "Mar 3, 1975". Therefore, we compare the month
	in the array with the month we entered into the form. If they match,
	then the date is valid, otherwise, the date is NOT valid.
	*/
	if(myDate_array[2].toUpperCase() != month)
	{
		writeError(errorFieldName, "Invalid Date");
		return false;
	}
/*
	//Check that order date <= 120 in the future.
	//Compute the deadline date: today + 120 days.
	var deadline = new Date();
	deadline.setDate(today.getDate() + 120);
	deadline.setUTCHours(23, 59, 59, 999);

	if(orderDate.valueOf() > deadline.valueOf())
	{
		var errorMessage = "Delivery must be no later than " + deadline.toDateString();
		writeError(errorFieldName, errorMessage);
		return false;
	}
*/
	document.getElementById(errorFieldName).style.display = "none";
	return true;
}

function checkDateRange(startMonthNm, startDate, endMonthNm, endDate, errorFieldName)
{

	var startDateString = document.getElementById(startDate).value;

	var startMonth = document.getElementById(startMonthNm).value;

	var startDay = stripZeros(startDateString);


	var endDateString = document.getElementById(endDate).value;

	var endMonth = document.getElementById(endMonthNm).value;
	//alert("endMonth="+endMonth);

	var endDay = stripZeros(endDateString);
	//alert("endDay="+endDay);

	var today = new Date();

	var startDate = new Date();
	startDate.setMonth(startMonth);
	startDate.setDate(startDay);

	//startDate.setUTCHours(23, 59, 59, 999);
	//alert("start="+startDate);
	var endDate = new Date();
	endDate.setMonth(endMonth);
	endDate.setDate(endDay);
	//endDate.setUTCHours(23, 59, 59, 999);
	//alert("end="+endDate);
	if(endMonth < startMonth)
	{
		//set to next year
		endDate.setFullYear(today.getFullYear()+1);

	}

	if(endDate<startDate){
			writeError(errorFieldName, "End date cannot precede start date");
			return false;
	}

	document.getElementById(errorFieldName).style.display = "none";
	return true;
}


function checkDateRangeDifference(startMonthNm, startDate, endMonthNm, endDate, numberOfDays, errorFieldName, displayError)
{

	//start date - string format
	var startDateString = document.getElementById(startDate).value;
	var startMonth = document.getElementById(startMonthNm).value;
	var startDay = stripZeros(startDateString);

	//start date - date format
	var startDate = new Date();
	startDate.setMonth(startMonth);
	startDate.setDate(startDay);

	//end date - string format
	var endDateString = document.getElementById(endDate).value;
	var endMonth = document.getElementById(endMonthNm).value;
	var endDay = stripZeros(endDateString);

	//end date - date format
	var endDate = new Date();
	endDate.setMonth(endMonth);
	endDate.setDate(endDay);

	//today - date format
	var today = new Date();

	if(endMonth < startMonth)
	{
		//set to next year
		endDate.setFullYear(today.getFullYear()+1);
	}

	var milliSecondsInADay = 24*60*60*1000; // 24 hr * 60 min per hr * 60 sec per min * 1000 milli per sec
	var difInMilliSeconds = endDate - startDate;
	var difInDays = difInMilliSeconds / milliSecondsInADay;


	if(difInDays > numberOfDays)
	{
		if (displayError)
			writeError(errorFieldName, "Delivery date range cannot be more than " + numberOfDays + " days");
		return false;
	}

	document.getElementById(errorFieldName).style.display = "none";
	return true;
}



function checkSameStartEndDate(startMonthNm, startDate, endMonthNm, endDate)
{

	//start date - string format
	var startDateString = document.getElementById(startDate).value;
	var startMonth = document.getElementById(startMonthNm).value;
	var startDay = stripZeros(startDateString);

	//end date - string format
	var endDateString = document.getElementById(endDate).value;
	var endMonth = document.getElementById(endMonthNm).value;
	var endDay = stripZeros(endDateString);


	if(startMonth == endMonth && startDay == endDay)
	{
		return true;
	}
	else
	{
		return false;
	}
}


function checkValidCutoff(startMonthNm, startDate, cutoffHour, cutoffMin, errorFieldName, displayError)
{

	//start date - string format
	var startDateString = document.getElementById(startDate).value;
	var startMonth = document.getElementById(startMonthNm).value;
	var startDay = stripZeros(startDateString);

	//start date - date format
	var startDate = new Date();
	startDate.setMonth(startMonth);
	startDate.setDate(startDay);

	//today - date format
	var todayDate = new Date();

	//today date - string format
	var todayHour = todayDate.getHours();
	var todayMin = todayDate.getMinutes();


	//if start date is greater than today, all is ok
	if (startDate > todayDate)
	{
		return true;
	}
	else
	{
		//if start date is less than today, raise an error
		if (startDate < todayDate)
		{
			if (displayError)
				writeError(errorFieldName, "The delivery date cannot be more than 120 days in the future or past the current date");
			return false;
		}
		//if start date = today, check the cutoff hours
		else
		{
			if(todayHour > cutoffHour)
			{
				if (displayError)
					writeError(errorFieldName, "Cutoff Time of " + cutoffHour + ":" + cutoffMin + " has expired.  Please select a later delivery date");
				return false;
			}
			else
			{
				if (todayHour == cutoffHour)
				{
					if (todayMin > cutoffMin)
					{
						if (displayError)
							writeError(errorFieldName, "Cutoff Time of " + cutoffHour + ":" + cutoffMin + " has expired.  Please select a later delivery date");
						return false;
					}
					else
					{
						return true;
					}
				}
				else
				{
					return true;
				}
			}
		}
	}

}






function checkFirstChoice(elementName, errorFieldName)
{
	var first = document.getElementById(elementName).value;

	if(isWhitespace(first))
	{
		writeError(errorFieldName, "Required Field");
		return false;
	}

	document.getElementById(errorFieldName).style.display = "none";
	return true;
}

function checkSecondChoice(elementName, errorFieldName)
{
	var second = document.getElementById(elementName).value;

	if(isWhitespace(second))
	{
		writeError(errorFieldName, "Required Field");
		return false;
	}

	document.getElementById(errorFieldName).style.display = "none";
	return true;
}

function isPrice(str)
{
	var priceRegex = /^\s*\d{0,5}\.\d{2}\s*$/;
	if(priceRegex.test(str)){
	    var price=parseFloat(str);
	    return (price>=0.01&&price<=9999.99);
	}
}


function isADJPrice(str)
{
	var priceRegex = /^\s*\d{0,5}\.\d{2}\s*$/;
	if(priceRegex.test(str)){
	    var price=parseFloat(str);
	    return (price>=0&&price<=9999.99);
	}
}



function checkPrice(elementName, errorFieldName)
{
	var price = document.getElementById(elementName).value;
        price = price.replace(/\,/g,"");
        
	if(isWhitespace(price))
	{
		writeError(errorFieldName, "Required Field");
		return false;
	}

	if(!isPrice(price))
	{
		writeError(errorFieldName, "Invalid Format, must be 9999.99.");
		return false;
	}

	document.getElementById(errorFieldName).style.display = "none";
	return true;
}


function checkAdditionalDeliveryFee(addFee, errorFieldName)
{
	addFee = addFee.replace(/\,/g,"");
	if(!isPrice(addFee))
	{
		writeError(errorFieldName, "Invalid Format of fee provided, it must be 9999.99.");
		return false;
	}

	document.getElementById(errorFieldName).style.display = "none";
	return true;
}


function checkADJPrice(elementName, errorFieldName)
{
	var price = document.getElementById(elementName).value;
        price = price.replace(/\,/g,"");
        
	if(isWhitespace(price))
	{
		writeError(errorFieldName, "Required Field");
		return false;
	}

	if(!isADJPrice(price))
	{
		writeError(errorFieldName, "Invalid Format, must be 9999.99.");
		return false;
	}

	document.getElementById(errorFieldName).style.display = "none";
	return true;
}





function checkCardMessage(elementName, errorFieldName)
{
	var card = document.getElementById(elementName).value;

	if(isWhitespace(card))
	{
		writeError(errorFieldName, "Required Field");
		return false;
	}

	document.getElementById(errorFieldName).style.display = "none";
	return true;
}

function checkInstructions(elementName, errorFieldName)
{
	var instructions = document.getElementById(elementName).value;

	if(isWhitespace(instructions))
	{
		writeError(errorFieldName, "Required Field");
		return false;
	}

	document.getElementById(errorFieldName).style.display = "none";
	return true;
}

function checkPersonSpokeTo(elementName, errorFieldName)
{
	var name = document.getElementById(elementName).value;

	if(isWhitespace(name))
	{
		writeError(errorFieldName, "Required Field");
		return false;
	}

	document.getElementById(errorFieldName).style.display = "none";
	return true;
}

function checkSpokeTo()
{
	var name = document.getElementById("spoke_to").value;

	if(isWhitespace(name))
	{
		return false;
	}
	else
	{
		return true;
	}
}

function checkReasonCode(elementName, errorFieldName)
{
	var selectBox = document.getElementById(elementName);

	var action = selectBox.options[selectBox.selectedIndex].value;

	if(action == "0")
	{
		writeError(errorFieldName, "You must select a reason code.");
		return false;
	}

	document.getElementById(errorFieldName).style.display = "none";
	return true;
}




function isRequired(elementName, errorFieldName)
{
	var name = document.getElementById(elementName).value;

	if(isWhitespace(name))
	{
		writeError(errorFieldName, "Required Field");
		return false;
	}

	document.getElementById(errorFieldName).style.display = "none";
	return true;
}


function writeError(errorFieldName, errorMessage)
{
	var span = document.getElementById(errorFieldName);
	var newMessage = document.createTextNode("  " + errorMessage);

	if(span.childNodes)
	{
		var kids = span.childNodes;
		for(var i = 0; i < kids.length; i++)
		{
			span.removeChild(kids[i]);
		}
	}
	span.appendChild(newMessage);
	span.style.display = "inline";
}







/*
Handler for the onclick event of the various reason anchors for outgoing messages. Clicking
on the reason writes its text into the "Reason" textarea.
*/
function addReason()
{	// Get the text to be inserted.
	//alert("in addReason");
	var kids = event.srcElement.childNodes;
	var i = 0;
	//alert("kids["+i+"].nodeType="+kids[i].nodeType);
	while(kids[i].nodeType !=  3 /*Node.TEXT_NODE*/)
	{
		i++;
	}
	var statement = kids[i].data.substring(2);
        //alert("statement="+statement);
	var input = document.getElementById("message_comment");
	input.focus();
	//var comments=document.getElementById("message_comment").value;
	//document.forms[0].message_comment.value=comments+statement;

	document.selection.createRange().text += statement;
}





function newPage(pageNumber)
{
	var url = "displayCommunication.do?order_detail_id="
							+ document.forms[0].order_detail_id.value
							+ "&page_number="
							+ pageNumber
							+"&read_only_flag="+document.forms[0].read_only_flag.value;

	performAction(url);
}



/*****
Low-level validation and formatting functions.
*****/
function isEmpty(s)
{
	return ((s == null) || (s.length == 0));
}

function isDigit(c)
{
	return ((c >= "0") && (c <= "9"));
}


var defaultEmptyOK = false;

function isInteger(s)
{
	var i;

	if(isEmpty(s))
	{
		if(isInteger.arguments.length == 1)
		{
			return defaultEmptyOK;
		}
		else
		{
			return (isInteger.arguments[1] == true);
		}
	}

	// Search through string's characters one by one
	// until we find a non-numeric character.
	// When we do, return false; if we don't, return true.

	for(i = 0; i < s.length; i++)
	{
		// Check that current character is number.
		var c = s.charAt(i);

		if(!isDigit(c))
		{
			return false;
		}
	}

	// All characters are numbers.
	return true;
}

function stripCharsInBag(s, bag)
{
	var i;
	var returnString = "";

	// Search through string's characters one by one.
	// If character is not in bag, append to returnString.
	for(i = 0; i < s.length; i++)
	{
		// Check that current character isn't whitespace.
		var c = s.charAt(i);
		if (bag.indexOf(c) == -1)
		{
			returnString += c;
		}
	}

	return returnString;
}

/*
Remove any leading zeros from the search number
*/
function stripZeros(num)
{
	var num, newTerm;

	while(num.charAt(0) == "0")
	{
		newTerm = num.substring(1, num.length);
		num = newTerm;
	}

	if(num == "")
		num = "0";

	return num;
}

// whitespace characters
var whitespace = " \t\n\r";

function stripWhitespace(s)
{
	return stripCharsInBag(s, whitespace);
}

function isWhitespace(s)
{
    // Is s empty?
    if((s == null) || (s.length == 0))
    	return true;

    // Search through string's characters one by one
    // until we find a non-whitespace character.
    // When we do, return false; if we don't, return true.
    var whitespace = " \t\n\r";

    for (var i = 0; i < s.length; i++)
    {
        // Check that current character isn''t whitespace.
        var c = s.charAt(i);

        if (whitespace.indexOf(c) == -1) return false;
    }

    // All characters are whitespace.
    return true;
}



function showModalAlert(message)
{
	//Modal_URL
	modalUrl = "alert.html";

	//Modal_Arguments
	var modalArguments = new Object();
	modalArguments.modalText = message;

	//Modal_Features
	var modalFeatures = 'dialogWidth:200px; dialogHeight:150px; center:yes; status=no; help=no;';
	modalFeatures += 'resizable:no; scroll:no';

	window.showModalDialog(modalUrl, modalArguments, modalFeatures);
}


function updateCurrentUsers()
{
	var iframe = document.getElementById('CSRFrame');

	var url = "customerOrderSearch.do?action=refresh_ids"
				+ "&entity_type=ORDER_DETAILS"
				+ "&entity_id="
				+ document.getElementById('order_detail_id').value
				+ "&order_guid="
				+ document.getElementById('msg_guid').value
				+ getSecurityParams(false);

	iframe.src = url;
}

function noEnter()
{
	if(window.event && window.event.keyCode == 13)
	{
		window.event.returnValue = false;
		return false;
	}
	else
	{
		return true;
	}
}


/*
  The following methods are used to display a wait message to the user
  while an action is being executed.  The following coding convention should
  be used for the wait div''s table structure:

  waitDiv - id of the wait div
  waitMessage - id of the TD which will contain the message
  waitTD - id of the TD which will contain the dots (...)

  Parameters
  content - The div id containing the content to be hidden
  wait - The div id containing the wait message
  message - Optional, if provided the message to be displayed, otherwise, "Processing"
            will be the message
*/
var _waitTD = "";
var clearMessageTimerID = "";
var showPeriodTimerID = "";

function showWaitMessage(content, wait, message)
{
	var content = document.getElementById(content);
	var height = content.offsetHeight;
	var waitDiv = document.getElementById(wait + "Div");
	var waitMessage = document.getElementById(wait + "Message");
	_waitTD = document.getElementById(wait + "TD");

	content.style.display = "none";
	waitDiv.style.display = "block";
	waitDiv.style.height = height;
	waitMessage.innerHTML = (message) ? message : "Processing";

	clearMessageTimerID = window.setInterval("clearWaitMessage()", 5000);
	showPeriodTimerID = window.setInterval("showPeriod()", 1000);
}

function hideWaitMessage(content, wait)
{
	var content = document.getElementById(content);
	var waitDiv = document.getElementById(wait + "Div");

	content.style.display = "block";
	waitDiv.style.display = "none";

	window.clearInterval(clearMessageTimerID);
	window.clearInterval(showPeriodTimerID);
}

function clearWaitMessage()
{
	_waitTD.innerHTML = "";
}

function showPeriod()
{

  _waitTD.innerHTML += "&nbsp;.";

}

function noFlorists(error)
{
	writeError("invalidFloristCode", error);
}

function submitNewFtd(forwardAction, floristCode, floristSelectionLogId, zipCityFlag)
{
	document.forms[0].filling_florist_code.value = floristCode;
	document.forms[0].florist_selection_log_id.value = floristSelectionLogId;
	document.forms[0].zip_city_flag.value = zipCityFlag;
	document.forms[0].action = forwardAction;
	document.forms[0].submit();
}





function isExitSafe(pageText)
{
	elementClicked = event.srcElement;

	var modalUrl = "confirm.html";

	//Modal_Arguments
	var modalArguments = new Object();
	modalArguments.modalText = pageText;

	//Modal_Features
	var modalFeatures = 'dialogWidth:200px; dialogHeight:150px; center:yes; status=no;';
	modalFeatures += 'help=no; resizable:no; scroll:no';

	//get a true/false value from the modal dialog.
	return window.showModalDialog(modalUrl, modalArguments, modalFeatures);


}

function checkButtons()
{


	if(isExitSafe("Are you sure you want to leave without sending the message?")==true)
	{
		document.getElementById("unlockFrame").src="lock.do?action_type=unlock&lock_app=COMMUNICATION&lock_id="+document.getElementById("order_detail_id").value+getSecurityParams(false);

		actions[elementClicked.id]();
	}
}


function mainMenuAction()
{
  if(isExitSafe("Are you sure you want to leave without sending the message?")==true)
  {

           document.getElementById("unlockFrame").src="lock.do?action_type=unlock&lock_app=COMMUNICATION&lock_id="+document.getElementById("order_detail_id").value+getSecurityParams(false);
           doMainMenuAction();

  }
}





function doTagOrderAction(tag,detailId){
  if(tag == '2Y' || tag == '2N'){
    alert('You cannot tag this order, there are two users already tagged');
    return;
  }
  if(tag == '1Y'){
   alert('You already have this order tagged');
   return;
  }
 var url = "tagOrder.do?order_detail_id=" + detailId   +getSecurityParams(false);
 var sFeatures="dialogHeight:525px;dialogWidth:800px;";
 sFeatures +=  "status:1;scroll:0;resizable:0";
 showModalDialogIFrame(url,"" , sFeatures);
 // added
}


//The temporary solution to substitute Modify Order. Will be removed after modify order goes live.
function resetDeliveryDateUpdated(){
	alert("resetDeliveryDateUpdated ");
	if(document.forms[0].deliveryDateUpdated){
		document.forms[0].deliveryDateUpdated.value='true';
		alert("resetDeliveryDateUpdated "+document.forms[0].deliveryDateUpdated.value);
	}

}
//end of the temporary solution


	function optionClicked()
	{
		var ftdAllowed = document.forms[0].ftd_allowed.value;
                var personalized = document.forms[0].personalized.value;
                var msgType = document.getElementById("msg_message_type").value;
                var optionPicked = document.getElementById("would_like_to").value;
                var inTransit = document.forms[0].in_transit.value;

                if ((inTransit == "Y")
                    && (optionPicked == "cancel_order" || optionPicked == "cancel_original_send_new")) {
                    alert("This order is in printed or shipped status and is on or after the ship date but before the delivery date and cannot be canceled at this time.");
                }
                else if(personalized=="Y") {
                    alert("This option is not available for personalized products.");
                }
                else if(ftdAllowed=="N") {
	            	if(	optionPicked == "send_new_ftd" ||
	        				msgType == "Mercury" && optionPicked == "pay_original_send_new" ||
	        				msgType == "Mercury" && optionPicked == "send_comp_order" ||
	        				optionPicked == "callout" )
					{
					  alert("The product cannot be delivered with the current ship method for the requested delivery date.  A new FTD cannot be sent out.");
					}
					else{
					  checkLockProcessRequest('message_forward');
					}
               }
		else{
		  checkLockProcessRequest('message_forward');
		}
	}

  function onWouldLikeToChange()
  {
    var msgType = document.getElementById("msg_message_type").value;
    var action  = document.getElementById("would_like_to").value;
    if ( action == "send_comp_order"
         || msgType == "Mercury" && action == "pay_original_send_new")
    {
      document.getElementById('compOrderDIV').style.display = "block";
      parent.document.getElementById("approval_id").value = "";
      parent.document.getElementById("approval_passwd").value = "";
      parent.document.getElementById("approval_id").style.backgroundColor='white';
      parent.document.getElementById("approval_passwd").style.backgroundColor='white';
      parent.document.getElementById("approval_id").focus();
    }
    else
    {
    	var comporderdivelement = document.getElementById('compOrderDIV');
    	if (comporderdivelement != null)
    		document.getElementById('compOrderDIV').style.display = "none";
    }
  }


/*************************************************************************************
* doViewSDSTransaction() - Displays the SDS Transaction info
**************************************************************************************/
function doViewSDSTransaction(venusOrderNumber, venusId)
{
	//Modal_URL
	var modalUrl = "modalDialog.html";


	var url  = "retrieveSDSTransaction.do?action_type=test&venus_order_number=" + venusOrderNumber;
			url += "&venus_id=" + venusId + 	getSecurityParams(false);

	//Modal_Arguments
	var modalArguments = new Object();
	modalArguments.dialogURL = url;
	modalArguments.title = "SDS Transaction";


	//Modal_Features
	var modalFeatures = 'dialogWidth:750px; dialogHeight:650px; center:yes; status=no; help=no;';
	modalFeatures += 'resizable:yes; scroll:no';

	window.showModalDialog(modalUrl, modalArguments, modalFeatures);


}

function showComplaintComm()
{
        if (document.forms[0].preferred_partner.value == "Y")
        {
            document.getElementById("complaint_comm_origin_type_id_row").style.display="inline";
            document.getElementById("complaint_comm_notification_type_id_row").style.display="inline";
            document.getElementById("complaint_comm_origin_type_id").disabled=false;
            document.getElementById("complaint_comm_notification_type_id").disabled=false;
        }
}

function complaintOriginChange()
{
        if(document.getElementById("complaint_comm_origin_type_id").value == 'NC') 
        {
            document.getElementById("complaint_comm_notification_type_id").value = 'NC';
        }
}

function complaintNotificationChange()
{
        if(document.getElementById("complaint_comm_notification_type_id").value == 'NC') 
        {
            document.getElementById("complaint_comm_origin_type_id").value = 'NC';
        }
}

function checkComplaintComm(errorFieldName)
{

        var complaintCommOrigin = document.getElementById("complaint_comm_origin_type_id").value;
        var complaintCommNotification = document.getElementById("complaint_comm_notification_type_id").value;
        var valid = true;
       
        if (complaintCommOrigin == "NC" || complaintCommNotification == "NC")
        {
              if (complaintCommOrigin != "NC" || complaintCommNotification != "NC")
              {
                    writeError(errorFieldName,"When choosing not a complaint as an origin in one of the pull downs, not a complaint must also be selected in the second pull down.");
                    valid = false;
              }         
        }
        if (valid)
        {
             document.getElementById(errorFieldName).style.display = "none";
        }
        return valid;
}
function checkProductDeliverability()
{
    var state = document.getElementById("recipient_state").value;
    var productType= document.getElementById("product_type").value;
    var isWestOrder= document.getElementById("is_ftdw_order").value;
    var valid = true;
    if(((("AK" == state || "HI" == state) && "FRECUT" == productType )|| (("PR" == state || "VI" == state) && "true" == isWestOrder)) && ("Vendor" == document.forms[0].msg_order_type.value)){
    	valid = false;
    }
    return valid;	
}

