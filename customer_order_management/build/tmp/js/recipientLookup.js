/*
 * Constructor
 */
RecipientPopup = function(params) {
   this.focusObj = null;
   this.phoneName = params.phone;
   this.firstName = params.first_name;
   this.lastName = params.last_name;
   this.address1Name = params.address1;
   this.address2Name = params.address2;
   this.cityName = params.city;
   this.stateName = params.state;
   this.zipName = params.zip;
   this.countryName = params.country;
};

RecipientPopup._C = null;

/*
 * Class functions
 */
RecipientPopup.setup = function(params) {
   window.popup = Popup.setup(params, new RecipientPopup(params));
   window.popup.create();
   window.popup.showAtElement(params.displayArea);
};

RecipientPopup.enterRecipientSearch = function(ev) {
   if (ev.keyCode == 13)
      RecipientPopup.searchRecipient(ev);
};



RecipientPopup.pressHide = function () {
  if (window.event.keyCode == 13)
      Popup.hide();
  // if tab on close button, return to first button
  if (window.event.keyCode == 9){
     var input = document.getElementById("phoneInput");
      input.focus();
      return false;
  }
};

RecipientPopup.searchRecipient = function(ev) {
   //First validate the inputs
   var phone = document.getElementById("phoneInput");

   //Phone is required
   if((stripWhitespace(phone.value).length == 0) && (stripWhitespace(phone.value).length == 0)) {
       phone.focus();
       phone.style.backgroundColor = 'pink';
       alert("Please correct the marked fields");
       return false;
   }


   //Now that everything is valid, open the popup after removing special characters
   var bag2 = ",/.<>?;:\|[]{}~!@#$%^&*()-_=+` " + "\\\'";
   var phoneVal = stripCharsInBag(phone.value, bag2);

   var form = document.forms[0];
   var url_source="lookupCustomer.do?" +
   "&phoneInput=" + phoneVal +
   "&customerInput=R" +
   getSecurityParams(false);


   //Open the window
   var modal_dim = "dialogWidth:800px; dialogHeight:500px; center:yes; status=0";
   var ret = showModalDialogIFrame(url_source, "", modal_dim);


   // Set the new values
   if (ret && ret != null && ret[0] != ""){
       document.getElementById(RecipientPopup._C.firstName).value = ret[0];
       document.getElementById(RecipientPopup._C.lastName).value = ret[1];
       document.getElementById(RecipientPopup._C.address1Name).value = ret[2];
       document.getElementById(RecipientPopup._C.address2Name).value = ret[3];
	     document.getElementById(RecipientPopup._C.cityName).value = ret[4];
	     document.getElementById(RecipientPopup._C.zipName).value = ret[6];
       document.getElementById(RecipientPopup._C.countryName).value = ret[7];

			 var isUSPhone = isUSPhoneNumber(ret[8]);
			 if (isUSPhone)
	       document.getElementById(RecipientPopup._C.phoneName).value = reformatUSPhone(ret[8]);
	     else
			   document.getElementById(RecipientPopup._C.phoneName).value = ret[8];
       populateStates(RecipientPopup._C.countryName, RecipientPopup._C.stateName, ret[5]);
   }
   Popup.hide();
   RecipientPopup._C = null;
};


/*
 * Member Functions
 */
RecipientPopup.prototype.renderContent = function(div) {
   RecipientPopup._C = this;

   var table = Popup.createElement("table");
   table.setAttribute("cellSpacing", "2");
   table.setAttribute("cellPadding", "2");
   table.setAttribute("className", "LookupTable");
   div.appendChild(table);

   var thead, tbody, tfoot, row, cell, input, image;
   thead = Popup.createElement("thead", table);
   row = Popup.createElement("tr", thead);

   cell = Popup.createElement("td", row);
   cell.setAttribute("className", "PopupHeader");
   cell.setAttribute("colSpan", "2");
   cell.style.cursor = "move";
   cell.popup = window.popup;
   cell.appendChild(document.createTextNode("Recipient Lookup"));
   Popup.addDragListener(cell);

   tbody = Popup.createElement("tbody", table);
   row = Popup.createElement("tr", tbody);
   cell = Popup.createElement("td", row);

   // City
   row = Popup.createElement("tr", tbody);
   cell = Popup.createElement("td", row);
   cell.setAttribute("className", "labelRight");
   cell.appendChild(document.createTextNode("Phone:"));

   cell = Popup.createElement("td", row);
   input = Popup.createElement("input", cell);
   this.focusObj = input;
   input.setAttribute("id", "phoneInput");
   input.setAttribute("tabIndex", "96");
   input.setAttribute("TYPE", "text");
   Popup.addEvent(input, "keydown", RecipientPopup.enterRecipientSearch);
   Popup.addEvent(input, "blur", fieldBlur);
   Popup.addEvent(input, "focus", fieldFocus);

   // Buttons
   tfoot = Popup.createElement("tfoot", table);
   row = Popup.createElement("tr", tfoot);
   cell = Popup.createElement("td", row);
   cell.setAttribute("colSpan", "2");
   cell.setAttribute("align", "right");

   button = Popup.createElement("button", cell);
   button.setAttribute("value", "Search");
   button.setAttribute("className", "BlueButton");
   button.setAttribute("tabIndex", "99");
   Popup.addEvent(button, "click", RecipientPopup.searchRecipient);
   Popup.addEvent(button, "keydown", RecipientPopup.enterRecipientSearch);

   button = Popup.createElement("button", cell);
   button.setAttribute("value", "Close screen");
   button.setAttribute("className", "BlueButton");
   button.setAttribute("tabIndex", "100");
   Popup.addEvent(button, "click", Popup.hide);
   Popup.addEvent(button, "keydown", RecipientPopup.pressHide);
};

RecipientPopup.prototype.setFocus = function() {
   if (this.focusObj) {
      this.focusObj.focus();
   }
};

RecipientPopup.prototype.setValues = function() {
   document.getElementById("phoneInput").value = document.getElementById(RecipientPopup._C.phoneName).value;
};
