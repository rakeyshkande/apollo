/*
Global variables
*/
var tabsArray = new Array("page_delivery_date", "DateCalendar", "getMaxButton");

/*
 *  Initialization
 */
function init(){
  document.oncontextmenu = contextMenuHandler;
  document.onkeydown = upcBackKeyHandler;
  setScrollingDivHeight();
  addDefaultListeners();

  setTabIndexes(tabsArray, 1);
  // initErrorMessages();

  window.onresize = setScrollingDivHeight;

  if(updateValidationFail == 'n')
  {
     displayUpdateValidationMessage();
  }
}


function setScrollingDivHeight(){
    var deliveryDateDetailDiv = document.getElementById("deliveryDateDetail");
    deliveryDateDetailDiv.style.height = document.body.clientHeight - deliveryDateDetailDiv.getBoundingClientRect().top - 75;
}

/*
 Function handles the Cancle Update button
*/
function doCancelUpdateAction(){
  if ( hasFormChanged() ) {
    if ( doExitPageAction(WARNING_MSG_4) ) {
      baseDoCancelUpdateAction("deliveryDateForm");
    }
  }
  else {
    baseDoCancelUpdateAction("deliveryDateForm");
  }
}


/*
 Function handles the Complete Update button action
 Once this function completes the checkLockIFrame calls
 checkAndForward() which calls the doCopmleteOrder() function
*/
function doCompleteUpdateAction(){

    var ddSelect = document.getElementById("delivery_date_select");
    if(ddSelect != null )
    {
        if(validateDeliveryDate()) {
         // call the UpdateDeliveryDateAction

           var vendorFlag = document.getElementById("vendor_flag").value;
           if(vendorFlag == "N")
           {
            prepareFloristDeliveryDate("deliveryDateForm");
           }
           else
           {
            prepareVendorDeliveryDate("deliveryDateForm");
           }



           showWaitMessage("mainContent", "wait", "Updating");
           var data = "?recipient_state=" + document.getElementById("recipient_state").value +
                 "&recipient_zip_code=" + document.getElementById("recipient_zip_code").value +
                 "&recipient_city=" + document.getElementById("recipient_city").value +
                 "&recipient_country=" + document.getElementById("recipient_country").value +
                 "&product_id=" + document.getElementById("product_id").value +
                 "&florist_id=" + document.getElementById("florist_id").value +
                 "&occasion=" + document.getElementById("occasion").value +
                 "&origin_id=" + document.getElementById("origin_id").value +
                 "&order_detail_id=" + document.getElementById("order_detail_id").value +
                 "&external_order_number=" + document.getElementById("external_order_number").value +
                 "&vendor_flag=" + vendorFlag +
                 "&orig_product_id=" + document.getElementById("orig_product_id").value +
                 "&delivery_date=" + document.getElementById("delivery_date").value +
                 "&delivery_date_range_end=" + document.getElementById("delivery_date_range_end").value +
                 "&orig_delivery_date=" + document.getElementById("orig_delivery_date").value +
                 "&orig_delivery_date_range_end=" + document.getElementById("orig_delivery_date_range_end").value +
                 "&ship_method=" + document.getElementById("ship_method").value +
                 "&source_code=" + document.getElementById("source_code").value +
                 "&order_disp_code=" + document.getElementById("order_disp_code").value +
                 "&orig_ship_date=" + document.getElementById("orig_ship_date").value +
                 getSecurityParams(false);

          document.forms[0].action = "updateDeliveryDate.do" + data;
          document.forms[0].submit();
        }
    }else{
       alert("No additional delivery dates available for selected product, please use Update Order to select a different product.");
    }
}

 function prepareFloristDeliveryDate(formName) {
    var deliveryDate = document.getElementById("delivery_date");
    var deliveryDateRangeEnd = document.getElementById("delivery_date_range_end");
    var dd = document.getElementById("page_delivery_date");
    var option = dd[dd.selectedIndex];
    var startDate = option.value;
    var endDate = option.endDate;
    var outEndDate = ( endDate != startDate ) ?  endDate : "";

    deliveryDate.value = startDate;
    deliveryDateRangeEnd.value = outEndDate;
  }

  function prepareVendorDeliveryDate(formName) {
     var shipMethod = document.getElementById("ship_method").value

    var dd = document.getElementById(shipMethod + "_delivery_date");
    var ddSelected = dd[dd.selectedIndex];
    var date = ddSelected.value;

    document.forms[formName].delivery_date.value = date;
    document.forms[formName].delivery_date_range_end.value = "";
  }

function validateDeliveryDate()
{
    var dd = document.getElementById("delivery_date_select");
    var field = document.getElementById('delivery_date_select');
    var reqSpan = document.getElementById('required');
    field.style.backgroundColor='white';

   //compare the date selected with the current date
   if( doCheckSelection(dd) ) return;
    //check for index == 0 and empty string becuase
    //while the florist loaded dates have an empty index of 0
    //the vendor loaded dates do not
    if (dd.selectedIndex == 0 && dd[dd.selectedIndex].value == "")
    {
        field.focus();
        field.style.backgroundColor='pink';
        /*
          reqSpan is tested for null here becuase only one drop down
          on the page needs the "Required Field" error message to display.
          Other drop downs that are dispalyed will always have a value selected
          so the error message is not needed.
        */
        if(reqSpan != null){
          reqSpan.style.display = "block";
        }


        return false;
    }
    else
        return true;
}

/* Executes an iframe action.
 *
 * @param action  The sub-action
 */
function doIFrameAction(url, data) {
   var form = document.getElementById("deliveryDateForm");
   var frame = document.getElementById("updateDeliveryDateActions");
   frame.src = url + data;
}




var MAX_DATES_ACTION = "max_dates";
var callingAction = "";
function doGetMaxDeliveryDateAction() {
  showWaitMessage("mainContent", "wait", "Retrieving");
  callingAction = MAX_DATES_ACTION;
  var data = "?action_type=" + MAX_DATES_ACTION +
             "&recipient_state=" + document.getElementById("recipient_state").value +
             "&recipient_zip_code=" + document.getElementById("recipient_zip_code").value +
             "&recipient_country=" + document.getElementById("recipient_country").value +
             "&product_id=" + document.getElementById("product_id").value +
             "&occasion=" + document.getElementById("occasion").value +
             "&origin_id=" + document.getElementById("origin_id").value +
             "&vendor_flag=" + document.getElementById("vendor_flag").value +
             "&orig_product_id=" + document.getElementById("orig_product_id").value +
             "&source_code=" + document.getElementById("source_code").value +
             "&order_disp_code=" + document.getElementById("order_disp_code").value +
             "&orig_ship_date=" + document.getElementById("orig_ship_date").value +
             "&addons=" + document.getElementById("addons").value +
             getSecurityParams(false);

  doIFrameAction("loadDeliveryDate.do", data);
}

function doGetMaxDeliveryDaysPostAction(deliveryDates) {
  hideWaitMessage("mainContent", "wait");
  populateDeliveryDates(deliveryDates);
}

/* Populates the delivery date drop down using the supplied two-dimensional array.
 * This function is typically called after clicking the "Get Max" button.
 *
 * @param deliveryDates three-dimensional array of delivery date data
 */
function populateDeliveryDates(deliveryDates)
{
  var reqDate = document.getElementById("delivery_date");
  var dd = document.getElementById("delivery_date_select");
  var option;
  var selected = false;
  dd.options.length = 0;
  for (var i = 0; i < deliveryDates.length; i++){
    selected = reqDate.value == deliveryDates[i][0];
    option = new Option(deliveryDates[i][2], deliveryDates[i][0], false, selected);
    option.endDate = deliveryDates[i][1];
    dd.add(option);
  }
}


/*
 *  Disables the various navigation keyboard commands.
 */
function upcBackKeyHandler() {
   // backspace
   if (window.event && window.event.keyCode == 8){
      var formElement = false;

      for(i = 0; i < document.forms['deliveryDateForm'].elements.length; i++){
         if(document.forms['deliveryDateForm'].elements[i].name == document.activeElement.name){
            formElement = true;
            break;
         }
      }

      if(formElement == false || (document.activeElement.type != "text" && document.activeElement.type != "textarea" && document.activeElement.type != "password")){
         window.event.cancelBubble = true;
         window.event.returnValue = false;
         return false;
      }
   }

   // F5 and F11
   if (window.event.keyCode == 122 || window.event.keyCode == 116){
      window.event.keyCode = 0;
      event.returnValue = false;
      return false;
   }

   // Alt + Left Arrow
   if(window.event.altLeft){
      window.event.returnValue = false;
      return false;
   }

   // Alt + Right Arrow
   if(window.event.altKey){
      window.event.returnValue = false;
      return false;
   }
}

/*
when the drop downs are changed this checks
to make sure that the value chosen is not the same as the
current delivery date on the order.
If it is a message is dispalyed and the list is re-set.
*/
function doCheckSelection(selObj)
{
  var messageText = "The date selected is the same as the date on the order, please \n" +
                    "select another date or Cancel the date change.";
  var notify = false;

  var reqDateBeg = document.getElementById('delivery_date').value;
  var reqDateEnd = document.getElementById('delivery_date_range_end').value;

  if( reqDateEnd == null || reqDateEnd == '' )
  {
    notify =
      ( selObj[selObj.selectedIndex].value == reqDateBeg )  ? true : false;
  }
  else
  {
    notify =
      ( selObj[selObj.selectedIndex].value == reqDateBeg &&
        selObj[selObj.selectedIndex].endDate == reqDateEnd  ) ? true : false;
  }
  //if notify is ture highlight the drop down and alert the user
  if( notify )
  {
    highLight(selObj,true);
    alert(messageText);
    selObj.selectedIndex = 0;
  } else{
      highLight(selObj,false);
  }
  return notify;
}

/*
 When the the page is submited (complete update button)
 it's possible that validation fails. In this case an error message is
 created and sent the the loadDeliveryDate action again.
 If the error message exists this function will dispaly it.
*/
function displayUpdateValidationMessage()
{
     var modalArguments = new Object();
     modalArguments.modalText = updateValidationMessage;

     var modal_dim = "dialogWidth:400px; dialogHeight:150px; dialogLeft:300; dialogTop:250; center:yes; status=0; help:no; scroll:no;";
     window.showModalDialog("alert.html", modalArguments, modal_dim);


}
/*
 Takes a select object and a boolean
 if boolean is true the object is highlighted
 otherwise the highlight will be removed
*/
function highLight(obj,bol){
  obj.style.backgroundColor =
    (bol) ? 'pink' : 'white';
}