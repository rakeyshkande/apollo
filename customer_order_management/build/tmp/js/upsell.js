/*
 * (This javascript along with the upsell.xsl file were copied
 * over from the order_scrub project and chagned to work with C.O.M.
 * Actions
 */
var CANCEL_ACTION       = "cancel_update";
var tabsArray = new Array("custContinue", "custCancel");

function doExitAction(){
  window.returnValue = new Array("_EXIT_");
  doCloseAction();
}

function doCloseAction(){
  window.close();
}


/*
 Function handles the Cancle Update button
*/
function doCancelUpdateAction(){
  if ( hasFormChanged() ) {
    if ( doExitPageAction(WARNING_MSG_4) ) {
      baseDoCancelUpdateAction("upsellForm");
    }
  }
  else {
    baseDoCancelUpdateAction("upsellForm");
  }
}


/*
 Executes the Continue Update button
*/
function doContinueUpdateAction(){
   showWaitMessage("mainContent", "wait", "Processing"); /* show wait message */
   doRecordLock('MODIFY_ORDER','check',null, "upsellForm");  /* make sure we still have a lock on this record */
}

/*
 If we still have a lock on the current record
 this function will be called by checkLockIFrame.xsl
*/
function doContinueOrderProcess(){
   performAction();
}

/*

*/
function performAction(){
    document.getElementById('upsellForm').submit();
}

/*
 Any time the checkLockIframe needs to be invoked
 this method will be called after the IFrame finishes loading
 From here we can call what ever method we need to in order
 to continue processing

 You must call doRecordLock(  ) in order to have this
 function invoked. Also see util.js

*/
function checkAndForward(forwardAction){
   doContinueOrderProcess();
}



function setScrollingDivHeight(){
  var upsellDetailDiv = document.getElementById("upsellDetail");
  upsellDetailDiv.style.height = document.body.clientHeight - upsellDetailDiv.getBoundingClientRect().top - 75;
}


function JCPPopChoice(inChoice){
//JC penney popup for international change
  if (inChoice == 'Y'){
    var url = "DNISChangeServlet?source=productDetailJCP";
    document.forms[0].source.value = 'productDetailJCP';
    document.forms[0].action = url;
    document.forms[0].method = "get";
    document.forms[0].submit();
  }
}


function openJCPPopup(){
  JCPPopV = document.getElementById("JCPPop").style;
  scroll_top = document.body.scrollTop;
  JCPPopV.top = scroll_top+document.body.clientHeight/2-100;

  JCPPopV.width = 290;
  JCPPopV.height = 100;
  JCPPopV.left = document.body.clientWidth/2 - 50;
  JCPPopV.visibility = "visible";
}

function closeJCPPopup(){
  JCPPopV.visibility = "hidden";
}


function viewLargeImage(){
  largeImage = document.getElementById("showLargeImage").style;
  scroll_top = document.body.scrollTop;
  largeImage.top = scroll_top + document.body.clientHeight/2 - 200;

  largeImage.width = 340;
  largeImage.height = 400;
  largeImage.left = 200;
  largeImage.visibility = "visible";
}

function closeLargeImage(){
  largeImage.visibility= "hidden";
}


     /*  **************************************************************************
            This group of functions is used to display/hide the Specialty Gift
            only available for zip code message box.
         **************************************************************************  */
function openNoFloral(){
  // disableContinue();

   noFloralV = document.getElementById("noFloral").style;
   scroll_top = document.body.scrollTop;
   noFloralV.top = scroll_top+document.body.clientHeight/2-100;

   noFloralV.width = 290;
   noFloralV.height = 100;
   noFloralV.left = document.body.clientWidth/2 - 100;
   noFloralV.visibility = "visible";
}

function closeNoFloral(){
  // disableContinue();
   noFloral.style.visibility = "hidden";
}

  /*  **************************************************************************
         This group of functions is used to display/hide the No Product
         available for zip code message box
      **************************************************************************  */
function openNoProduct(){
  // disableContinue();

   noProductV = document.getElementById("noProduct").style;
   scroll_top = document.body.scrollTop;
   noProductV.top = scroll_top+document.body.clientHeight/2-100;

   noProductV.width = 290;
   noProductV.height = 100;
   noProductV.left=document.body.clientWidth/2 - 50;
   noProductV.visibility = "visible";
}

function closeNoProduct(){
   noProductV.visibility = "hidden";
}

function noProduct(){
   closeNoProduct();
}

    /*  **************************************************************************
            This group of functions is used to display/hide the Product Unavailable
            for zip code message box
        **************************************************************************  */
function openProductUnavailable(){
   //disableContinue();

   productUV = document.getElementById("productUnavailable").style;
   scroll_top = document.body.scrollTop;
   productUV.top = scroll_top+document.body.clientHeight/2-100;

   productUV.width = 290;
   productUV.height = 100;
   productUV.left = document.body.clientWidth/2 - 50;
   productUV.visibility = "visible";
}

function openJcpFoodItem(){
   productUV = document.getElementById("jcpFood").style;
   scroll_top = document.body.scrollTop;
   productUV.top = scroll_top+document.body.clientHeight/2-100;

   productUV.width = 290;
   productUV.height = 100;
   productUV.left = document.body.clientWidth/2 - 50;
   productUV.visibility = "visible";
}

function closeJcpFoodItem(){
   productUV.visibility = "hidden";
   document.forms[0].backButton.focus();
}

function closeProductUnavailable(){
   productUV.visibility = "hidden";
}

  /*  **************************************************************************
      This group of functions is used to display/hide the Floral Only
      for zip code message box
    **************************************************************************  */
function openOnlyFloral(){
   //disableContinue();

   onlyFloralV = document.getElementById("onlyFloral").style;
   scroll_top = document.body.scrollTop;
   onlyFloralV.top = scroll_top+document.body.clientHeight/2-100;

   onlyFloralV.width = 290;
   onlyFloralV.height = 100;
   onlyFloralV.left = document.body.clientWidth/2 - 50;
   onlyFloralV.visibility = "visible";
}

function closeOnlyFloral(){
   onlyFloralV.visibility = "hidden";
}

  /*  **************************************************************************
         This group of functions is used to display/hide the Codified Special
         for zip code message box
      **************************************************************************  */
function openCodifiedSpecial(){
   //disableContinue();

   codifiedSpecialV = document.getElementById("codifiedSpecial").style;
   scroll_top = document.body.scrollTop;
   codifiedSpecialV.top = scroll_top+document.body.clientHeight/2-100;

   codifiedSpecialV.width = 290;
   codifiedSpecialV.height = 100;
   codifiedSpecialV.left = document.body.clientWidth/2 - 50;
   codifiedSpecialV.visibility = "visible";

   document.all.submitButton.src = "../images/button_back.gif";
   backupFlag = true;
}

function closeCodifiedSpecial(){
   codifiedSpecialV.visibility = "hidden";
}

function codifiedSpecial(){
   closeCodifiedSpecial();
}

/*
 * Handle the Update Delivery Information button event
 */
function doUpdateDeliveryInfo(){
  var url = 'loadDeliveryInfo.do';
  performButtonAction(url);
}

/*
 * Handle the Update Product Category button event
 */
function doUpdateProductCategory(){
  var url = 'loadProductCategory.do';
  performButtonAction(url);
}

/*******************************************************************************
*
*    performAction()
*
*******************************************************************************/
function performButtonAction(url){
    /*
      Display a message to the user called Processing...
      this allows the user to know that the system is working
      and not stalled out.
      scroll(0,0) returns the window to the top position since
      all(most) buttons are located at the bottom of a scrolled window
    */
    scroll(0,0);
    showWaitMessage("mainContent", "wait", "Processing");

     document.forms[0].action = url;
     document.forms[0].submit();
}