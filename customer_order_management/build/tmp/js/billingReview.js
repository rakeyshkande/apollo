/*
   This js file is the main javascript file used for the billingReview.xsl screen.
   
*/
var billingValidationArray = new Array
     (//input attribute name           error message                          validation function   additional function parameters
      ["pay_type",                   "Required Field",                             isPaymentTypeSelected,     ],     
      ["credit_card_num",            "Invalid Format",                             isValidCreditCard,         ]   
     );


 var recalculateMatrix = new Array
    (//input attribute name           error message                          validation function   additional function parameters
      ["entered_merch",                "Invalid Format",                             isEnteredMerch,     ],     
      ["entered_add_on",               "Invalid Format",                             isEnteredAddOn,     ],   
      ["entered_serv_ship",            "Invalid Format",                             isEnteredServShip,  ]   
     );
/*
 Valid Payment Types
*/
var ccNames = new Array
     ( //card name                   length
       //Visa
       ["VI",                  "VISA"        ],
       //Master Card
       ["MC",                  "MASTERCARD"  ],
       //Discover
       ["DI",                  "DISCOVER"    ],
       //American Express
       ["AX",                  "AMEX"        ],
       //Diners Club
       ["DC",                  "DINERS"      ],
       //Carte Blanche
       ["CB",                  "CARTEBLANCHE"]
     );



var CANCEL = 'cancel';
var APPLY = 'apply';

var isPageValid = false;
/*
 Used to determine if we should display the
 Gift Certificate popup again or not
*/
var isGCSelected = false;
/*
 Used to help decide if the Credit Card being 
 validated is the original card or not.
*/
var isCardUpdated = false;
var isOriginalCardUsed = true;

var isMaxExceeded = false;

function doSubmitBillingReview(){

   var payment = document.getElementById('pay_type');

   if(payment.options[payment.selectedIndex].value.toString() == 'GC' && isGCSelected == false){       
       doGiftCertificate();
   }else{ 
      //validate the form fields
      var doSubmit;
	  if(!payment.disabled){
	    doSubmit =  validateForm(billingValidationArray); 
	  }else{
	     isPageValid = true;
		 doSubmit = true;
	  }
	   
      /* validate merch,addon, service/shipping if we have an 'add bill' */
       if( isPageValid )
       {
         isPageValid = validateAddBillFields();   
       }
       
      if(doSubmit && isPageValid ){             

        document.getElementById('page_action').value = "apply_billing";  

       // trimOriginalCardAsterisks(); //if it's the original credit card remove the *'s before submittion

        showWaitMessage("mainContent","wait","Validating");           
        doRecordLock('MODIFY_ORDER','check',""); 
      }
   }    

}

/*
 executed if the check lock was successful
*/
function doSubmitBillingAction(){ 
 // peform a GET request using an IFrame xsl     
  var frame = document.getElementById('billingReviewActions');
    
  var enteredMerch = document.getElementById('entered_merch')
  var enteredAddOn = document.getElementById('entered_add_on')
  var enteredServShip = document.getElementById('entered_serv_ship')
  
  if (enteredMerch != null)
    enteredMerch = enteredMerch.value;
  else
    enteredMerch = 0;

  if (enteredAddOn != null)
    enteredAddOn = enteredAddOn.value;
  else
    enteredAddOn = 0;

  if (enteredServShip != null)
    enteredServShip = enteredServShip.value;
  else
    enteredServShip = 0;
    

  var frameSrc = 'billingReview.do' 
    + '?securitytoken=' + document.getElementById('securitytoken').value
    + '&context=' + document.getElementById('context').value
    + '&page_action=' + document.getElementById('page_action').value
    + '&entered_merch=' + enteredMerch 
    + '&entered_add_on=' + enteredAddOn 
    + '&entered_serv_ship=' + enteredServShip
    + '&net_product_amount=' + document.getElementById('net_product_amount').value
    + '&net_add_on_amount=' + document.getElementById('net_add_on_amount').value
    + '&net_serv_ship_fee=' + document.getElementById('net_serv_ship_fee').value
    + '&net_service_fee=' + document.getElementById('net_service_fee').value
    + '&net_shipping_fee=' + document.getElementById('net_shipping_fee').value
    + '&net_discount_amount=' + document.getElementById('net_discount_amount').value
    + '&net_shipping_tax=' + document.getElementById('net_shipping_tax').value
    + '&net_service_fee_tax=' + document.getElementById('net_service_fee_tax').value
    + '&net_product_tax=' + document.getElementById('net_product_tax').value
    + '&net_tax=' + document.getElementById('net_tax').value
    + '&net_total=' + document.getElementById('net_total').value
    + '&orig_net_product_amount=' + document.getElementById('orig_net_product_amount').value
    + '&orig_net_add_on_amount=' + document.getElementById('orig_net_add_on_amount').value
    + '&orig_net_serv_ship_fee=' + document.getElementById('orig_net_serv_ship_fee').value
    + '&orig_net_service_fee=' + document.getElementById('orig_net_service_fee').value
    + '&orig_net_shipping_fee=' + document.getElementById('orig_net_shipping_fee').value
    + '&orig_net_discount_amount=' + document.getElementById('orig_net_discount_amount').value
    + '&orig_net_shipping_tax=' + document.getElementById('orig_net_shipping_tax').value
    + '&orig_net_shipping_tax=' + document.getElementById('orig_net_shipping_tax').value
    + '&orig_net_product_tax=' + document.getElementById('orig_net_product_tax').value
    + '&orig_net_service_fee_tax=' + document.getElementById('orig_net_service_fee_tax').value 
    + '&orig_net_tax=' + document.getElementById('orig_net_tax').value
    + '&orig_net_total=' + document.getElementById('orig_net_total').value
    + '&gcc_num=' + document.getElementById('gcc_num').value
    + '&amount_owed=' + document.getElementById('amount_owed').value
    + '&gcc_status=' + document.getElementById('gcc_status').value
    + '&gcc_amt=' + document.getElementById('gcc_amt').value
    + '&total_owed=' + document.getElementById('total').value 
    + '&total=' + document.getElementById('total').value
    /*
    + '&orig_ord_merch=' + document.getElementById('orig_ord_merch').value
    + '&orig_ord_add_on=' + document.getElementById('orig_ord_add_on').value
    + '&orig_ord_serv_ship=' + document.getElementById('orig_ord_serv_ship').value
    + '&orig_ord_tax=' + document.getElementById('orig_ord_tax').value
    + '&orig_ord_disc=' + document.getElementById('orig_ord_disc').value
    + '&orig_ord_total=' + document.getElementById('orig_ord_total').value
    + '&orig_ord_shipping_fee=' + document.getElementById('orig_ord_shipping_fee').value
    + '&orig_ord_service_fee=' + document.getElementById('orig_ord_service_fee').value
    + '&orig_ord_service_fee_tax=' + document.getElementById('orig_ord_service_fee_tax').value
    + '&orig_ord_product_tax=' + document.getElementById('orig_ord_product_tax').value
    + '&orig_ord_shipping_tax=' + document.getElementById('orig_ord_shipping_tax').value
    + '&mod_ord_merch=' + document.getElementById('mod_ord_merch').value
    + '&mod_ord_add_on=' + document.getElementById('mod_ord_add_on').value
    + '&mod_ord_serv_ship=' + document.getElementById('mod_ord_serv_ship').value
    + '&mod_ord_tax=' + document.getElementById('mod_ord_tax').value
    + '&mod_ord_disc=' + document.getElementById('mod_ord_disc').value
    + '&mod_ord_tot=' + document.getElementById('mod_ord_tot').value
    + '&mod_ord_shipping_fee=' + document.getElementById('mod_ord_shipping_fee').value
    + '&mod_ord_service_fee=' + document.getElementById('mod_ord_service_fee').value
    + '&mod_ord_service_fee_tax=' + document.getElementById('mod_ord_service_fee_tax').value
    + '&orig_ord_product_tax=' + document.getElementById('orig_ord_product_tax').value
    + '&orig_ord_shipping_tax=' + document.getElementById('orig_ord_shipping_tax').value
    */
    + '&add_bill_product_amount=' + document.getElementById('add_bill_product_amount').value
    + '&add_bill_add_on_amount=' + document.getElementById('add_bill_add_on_amount').value
    + '&add_bill_serv_ship_fee=' + document.getElementById('add_bill_serv_ship_fee').value
    + '&add_bill_discount_amount=' + document.getElementById('add_bill_discount_amount').value
    + '&add_bill_tax=' + document.getElementById('add_bill_tax').value
    + '&add_bill_order_total=' + document.getElementById('add_bill_order_total').value
    + '&add_bill_service_fee=' + document.getElementById('add_bill_service_fee').value
    + '&add_bill_shipping_fee=' + document.getElementById('add_bill_shipping_fee').value
    + '&add_bill_shipping_tax=' + document.getElementById('add_bill_shipping_tax').value
    + '&add_bill_service_fee_tax=' + document.getElementById('add_bill_service_fee_tax').value
    + '&add_bill_product_tax=' + document.getElementById('add_bill_product_tax').value
    + '&refund_product_amount=' + document.getElementById('refund_product_amount').value
    + '&refund_add_on_amount=' + document.getElementById('refund_add_on_amount').value
    + '&refund_serv_ship_fee=' + document.getElementById('refund_serv_ship_fee').value
    + '&refund_discount_amount=' + document.getElementById('refund_discount_amount').value
    + '&refund_tax=' + document.getElementById('refund_tax').value
    + '&refund_order_total=' + document.getElementById('refund_order_total').value
    + '&refund_service_fee=' + document.getElementById('refund_service_fee').value
    + '&refund_shipping_fee=' + document.getElementById('refund_shipping_fee').value
    + '&refund_shipping_tax=' + document.getElementById('refund_shipping_tax').value
    + '&refund_service_fee_tax=' + document.getElementById('refund_service_fee_tax').value
    + '&refund_product_tax=' + document.getElementById('refund_product_tax').value
    + '&mgr_password=' + document.getElementById('mgr_password').value
    + '&is_orig_cc=' + document.getElementById('is_orig_cc').value 
    + '&class_change=' + document.getElementById('class_change').value  
    + '&order_guid=' + document.getElementById('order_guid').value
    + '&order_detail_id=' + document.getElementById('order_detail_id').value 
    + '&class_change=' + document.getElementById('class_change').value 
    + '&billing_type=' + document.getElementById('billing_type').value 
    + '&manual_auth=' + document.getElementById('manual_auth').value
    + '&aafes_code_entered=' + document.getElementById('aafes_code_entered').value
    + '&' + getNameValuePair(document.getElementById('exp_date_month'))
    + '&' + getNameValuePair(document.getElementById('exp_date_year'))
    + '&' + getNameValuePair(document.getElementById('pay_type'))
    + '&' + getNameValuePair(document.getElementById('credit_card_num'));
   
   frame.src = frameSrc;
}


/*
 required method, when implementing locking
 this is called by checkLockIFrame.xsl
*/
function checkAndForward(forwardAction){
   if(forwardAction == 'refresh'){
      doRefreshAction();
   }else{
      doSubmitBillingAction();
   }
}

/*
 this function is called from an IFrame after
 the submitBillingAction is executed.
 if the status returned was successful the IFrame calls 
 this function
*/
function deliveryConfirmationAction(actionType){
 
   var url = 'updateDeliveryConfirmation.do?';
   showWaitMessage("mainContent","wait","Validating"); 

   document.getElementById('page_action').value = actionType;


   document.getElementById('billingReviewForm').action = (url + retrieveDeliveryConfirmationParms() );
  
   document.getElementById('billingReviewForm').submit();

}

function doTestReview(){
    var url = 'updateDeliveryConfirmation.do?';

   showWaitMessage("mainContent","wait","Validating"); 

   document.getElementById('page_action').value = "send_message";

   document.billingReviewForm.action = (url + retrieveDeliveryConfirmationParms() );

   document.billingReviewForm.submit();
   document.getElementById('billingReviewForm').submit();


}




/*
 Instead of posting every hidden field to the deliveryConfirmation action
 this function retrieves only the necessary fields
*/
function retrieveDeliveryConfirmationParms(){
   var queryString = '';
   
   queryString = 'action=' + document.getElementById('page_action').value;
   /*+ 
      '&start_origin=' + document.getElementById('start_origin').value +
                 '&customer_id=' + document.getElementById('customer_id').value +
                 '&external_order_number=' + document.getElementById('external_order_number').value +
                 '&master_order_number=' + document.getElementById('master_order_number').value +
                 '&order_detail_id=' + document.getElementById('order_detail_id').value +
                 '&order_guid=' + document.getElementById('order_guid').value +
                 '&msg_type=' + document.getElementById('msg_type').value +
                 '&page_florist_id=' + document.getElementById('page_florist_id').value +
                 '&florist_type=' + document.getElementById('florist_type').value + getSecurityParams(false);
    */

  return queryString;
}

/*
 called when the status from submitBillingAction
 is 'declined'
*/
function invokeDeclinePopup(reason,message){
   hideWaitMessage("mainContent", "wait");

  if( Trim(reason) == 'decline_credit' ){
     creditCardDecline();
  }else{
     alert(message);
     if( Trim(reason) == 'decline_gcc'  ){
         document.getElementById('giftCertRow').style.display = 'none';
         document.getElementById('page_action').value = "";
         document.getElementById('gcc_num').value = "";
         document.getElementById('amount_owed').value = "";
         document.getElementById('gcc_status').value = "";
         document.getElementById('gcc_amt').value = "";
         document.getElementById('total').value = "";         
     }
  }
}
/*
 display credit card decline
*/
function creditCardDecline(){
   var pay_type = document.getElementById('pay_type');
   var url_source = "ccDecline.do?" + getSecurityParams(false) + "&pay_type=" + pay_type.options[pay_type.selectedIndex].value;
   var modal_dim = "dialogWidth:765px; dialogHeight:345px; center:yes; status=0; help:no; scroll:no";

   var ret = showModalDialogIFrame(url_source, "", modal_dim);
      if(ret != null){
         if( ret['page_action'] == 'save' ){
            if( ret["type"] == "auth"){
               document.getElementById('auth_code').value = ret['auth_code'];
            }
            if( ret["type"] == "aafes"){
               document.getElementById('aafes_code').value = ret['auth_code'];
            }                
           document.getElementById('manual_auth').value = 'Y';
           doRecordLock('MODIFY_ORDER','check',"");
         }       
         if(ret["page_action"] == 'cancel' ){
            //return user to recip/order or shopping cart page or updatebilling  
            //set util.js for the functionality of this method
            baseDoCancelUpdateAction('billingReviewForm');
            
         }
         if(ret["page_action"] == 'returntobilling' ){
            //do nothing and stay on current page.
         }
     }else{
         document.getElementById('manual_auth').value = 'N';
     }
}


/*
 Check payment type for (GC) Gift Certifciate type
*/
function doGiftCertificate(){
   redeemGcCoupon();   

   if(Trim(document.getElementById('gcc_status').value.toString()) == 'valid' &&
         Trim(document.getElementById('amount_owed').value.toString()) == 'true'){
            //recalculate the order here                 
          recalculateOrder();       
   }
   if(Trim(document.getElementById('amount_owed').value.toString()) == 'false' ){
      document.getElementById('page_action').value = "apply_billing";                          
      doRecordLock('MODIFY_ORDER','check',"");
   }   
}


/*
 method calls the gift certificate 
 pop-up
*/
function redeemGcCoupon(){
   //set page_action=pop becuase the action is looking for something to be
   //there and we can not just send nothing.
   var url_source = "gcCoupon.do?page_action=pop" + 
          getSecurityParams(false) + '&total_owed=' + document.getElementById('total').value;
   var modal_dim = "dialogWidth:765px; dialogHeight:345px; center:yes; status=0; help:no; scroll:no";

   var ret = showModalDialogIFrame(url_source, "", modal_dim);
   if( ret != null) {
      if(ret["status"] != null){
         document.getElementById('page_action').value = ret["page_action"];
         document.getElementById('gcc_num').value = ret["gift_number"];    
         document.getElementById('amount_owed').value = ret["amount_owed"];
         document.getElementById('gcc_status').value = ret["status"];
         document.getElementById('gcc_amt').value = ret["amount"];
         document.getElementById('total').value = ret["total_owed"];       
    
         //if GC was selected then we can not allow the user to select this option again
         //check the status, if "valid" set flag to true that way the user can not pop the GC page again
         if(ret["status"] == 'valid' ){
            isGCSelected = true;
            resetCreditCardLists('exp_date_month','exp_date_year','credit_card_num', false);
            disableAddBillFields();
         }

         displayGCInfo(ret["gift_number"],ret["amount"]);      
      }
   }
    
}

/*
 Displays the Gift Certificate Number, and Amount
 after the Gift Certificate popup is closed
*/
function displayGCInfo(gNumber, amt){
   //make sure we have values to display
   if(gNumber != null && amt != null){
      document.getElementById('giftCertRow').style.display = 'block';
      var display = document.getElementById('giftCertCell');
      display.innerText = 'Gift Certificate#: ' + gNumber + '      ' + 'Amount: ' + '$' + amt;
   }
}


/*
 This function refreshes the page if the user clicks the 
 Refresh button 
*/
function displayRecalculationTotals(recalcForm){
/*
 Display the new values in the NET row / charged row and totoal
*/
 updateHiddenFields(recalcForm);

 updateNetDisplay(recalcForm);

 updateChrgPostDispaly(recalcForm);

 document.getElementById('total_charge_amt').innerText = recalcForm.order_total.value;
 document.getElementById('total').value = recalcForm.order_total.value;



   //check to make sure MERCH, AddOn and Serv Ship Fields are valid
   //validateAddBillFields();
}

/*
 
*/
function updateHiddenFields(form){

   //update the hidden fields w/ the recalculated values
 document.getElementById('net_product_amount').value = form.product_amount.value;
 document.getElementById('net_add_on_amount').value = form.add_on_amount.value;
 document.getElementById('net_serv_ship_fee').value = form.serv_ship_fee.value;
 document.getElementById('net_service_fee').value = form.service_fee.value;
 document.getElementById('net_shipping_fee').value = form.shipping_fee.value;
 document.getElementById('net_discount_amount').value = form.discount_amount.value;
 document.getElementById('net_shipping_tax').value = form.shipping_tax.value;
 document.getElementById('net_service_fee_tax').value = form.service_fee_tax.value;
 document.getElementById('net_product_tax').value = form.product_tax.value;
 document.getElementById('net_tax').value = form.tax.value;
 document.getElementById('net_total').value = form.order_total.value;

}

/*
 Update the screen with either labels or new input fields
*/
function updateNetDisplay(form){

  //display the values on the Add Bill or Posted of the page
 document.getElementById('netMerchTD').innerText = form.product_amount.value;
 document.getElementById('netAddOnTD').innerText = form.add_on_amount.value;
 document.getElementById('netServShipTD').innerText = form.serv_ship_fee.value;
 document.getElementById('netTaxTD').innerText = form.tax.value;
 document.getElementById('netDiscountAmtTD').innerText = form.discount_amount.value;
 document.getElementById('netTotalTD').innerText = form.order_total.value;

}



/*
 This function displays the 'Charged Add Bill' or the 'Posted Refund' fields
 Also displays text boxes if the Merch, Add-On or Serv/Ship amounts are > than 0
*/
function updateChrgPostDispaly(form){

    //display the values on the Net Row of the page
   for(var i = 0; i < form.elements.length; i++)
   {
      if( Number(Trim(form.elements[i].value.toString())) > 0 && (form.elements[i].name == 'product_amount' ||
                  form.elements[i].name == 'add_on_amount' || form.elements[i].name == 'serv_ship_fee') )
      {       
         if(form.elements[i].name == 'product_amount' )
         {  
            var ele =  document.createElement('input');          
            var td = document.getElementById('chrgPostMerchTD');
            ele.setAttribute('type','text');
            ele.setAttribute('size',4);
            ele.setAttribute('name','entered_merch');
            ele.setAttribute('id','entered_merch'); 
            ele.setAttribute('value',form.elements[i].value)

            ele.attachEvent("onfocus", fieldFocus);
            ele.attachEvent("onblur", fieldBlur);
            ele.attachEvent("onblur", isEnteredMerch);             
            td.innerText = '';
            td.appendChild(ele);
         }
         if(form.elements[i].name == 'add_on_amount' ){

            var ele =  document.createElement('input');
            var td = document.getElementById('chrgPostAddOnTD');
            ele.setAttribute('type','text');
            ele.setAttribute('size', 4);
            ele.setAttribute('name','entered_add_on');
            ele.setAttribute('id','entered_add_on'); 
            ele.setAttribute('value',form.elements[i].value)

            ele.attachEvent("onfocus", fieldFocus);
            ele.attachEvent("onblur", fieldBlur);
            ele.attachEvent("onblur", isEnteredAddOn);             
            td.innerText = '';
            td.appendChild(ele);
         }
         if( form.elements[i].name == 'serv_ship_fee' ){
            var ele =  document.createElement('input');
            var td = document.getElementById('chrgPostServShipTD');
            ele.setAttribute('type','text');
            ele.setAttribute('size',4);
            ele.setAttribute('name','entered_serv_ship');
            ele.setAttribute('id','entered_serv_ship'); 
            ele.setAttribute('value',form.elements[i].value)

            ele.attachEvent("onfocus", fieldFocus);
            ele.attachEvent("onblur", fieldBlur);
            ele.attachEvent("onblur", isEnteredServShip);             
            td.innerText = '';
            td.appendChild(ele);
         }       
      }else{
          if(form.elements[i].name == 'product_amount' ){
            document.getElementById('chrgPostMerchTD').innerText = form.product_amount.value;
         }
         if(form.elements[i].name == 'add_on_amount' ){
             document.getElementById('chrgPostAddOnTD').innerText = form.add_on_amount.value;
         }
         if( form.elements[i].name == 'serv_ship_fee' ){
            document.getElementById('chrgPostServShipTD').innerText = form.serv_ship_fee.value;
         }
         if( form.elements[i].name == 'tax' ){
             document.getElementById('chrgPostTaxTD').innerText = form.tax.value;
         }
         if(  form.elements[i].name == 'discount_amount' ){
             document.getElementById('chrgPostDiscountAmtTD').innerText = form.discount_amount.value;
         }
         if(  form.elements[i].name == 'total' ){
            document.getElementById('chrgPostTotalTD').innerText = form.order_total.value;
         }

      }  
   }
}

/*
 This function is executed when we receive a 'valid' 
 status back from the gcCoupon.xsl.
*/
function disableAddBillFields(){

   document.getElementById('entered_merch').disabled = true;
   document.getElementById('entered_add_on').disabled = true;
   document.getElementById('entered_serv_ship').disabled = true;

}
/*
 recalculate the order and update the page
*/
function recalculateOrder(){
  //TODO is this needed
  // var eve = event.srcElement.type;
   document.getElementById('page_action').value = "recalculate_order";
    // call billingReview.do w/ page_action of recalculate_order 
    // this will refresh the page and upadate the amounts
   doSubmitBillingAction();          
}

/*
 Executes when the Refresh button is clicked
 will refresh the display for the user
*/

function refreshDisplay(){
 /* before doing the refresh we need to ensure we still have a lock on the record*/
   //TODO add this back
   if(validateAddBillFields()) //before going server side validate the fields on the page
   { //before going server side validate the fields on the page
      doRecordLock("MODIFY_ORDER",'check','refresh');        
   }  
   
}

/*
 This will execute after the refreshDisplay validates the page & 
 ensures we still have a lock on the record.
 Display updated fields on the page
*/
function doRefreshAction(){

    recalculateOrder();
}


/*
 executes when the user clicks the cancel button
*/
function doCancelBillingReview(){
   if(hasFormChanged())
   { 
      if( doExitPageAction(WARNING_MSG_4) ){        
          baseDoCancelUpdateAction('billingReviewForm');
      }
   }else{      
      baseDoCancelUpdateAction('billingReviewForm');
   }
   
}

/*
 Trim original credit card
*/
function trimOriginalCardAsterisks(){

   var reg = /^[\*]+/g;
   var space = '';

   if(isOriginalCardUsed){
      var card = document.getElementById('credit_card_num').value;
      if(reg.test(card)){
          card = card.replace(reg,space);
          document.getElementById('credit_card_num').value = card;          
      }
   } 
}

/*
 Populate the Credit Card Lists
*/
function populateCreditCardInfo(pay_type, exp_date_month, exp_date_year, selectedCCType, selectedCCExp){
   var ccTypeObj = document.getElementById(pay_type); //retrieve the list obj
   if(!isWhitespace(selectedCCType)){
     for(var c = 0; c < ccTypeObj.length; c++){          
        if(ccTypeObj.options[c].value.toString() == selectedCCType.toString()){          
           ccTypeObj.selectedIndex = c; //display the option in the list
           ccTypeObj.options[c].defaultSelected = true;
           break;
        }
     }
   }
  populateExpDate(exp_date_year, exp_date_month, selectedCCExp);
}
/*
 Populate Exp Month & Year lists
*/
function populateExpDate(exp_date_year,exp_date_month,selectedCCExp){
 if(!isWhitespace(selectedCCExp)){

      var ccMonthObj = document.getElementById(exp_date_month); //get month exp list
      var ccYearObj = document.getElementById(exp_date_year);  //get year exp list

      var expArr;
        //if the exp date has a '/' in the string test for this.. otherwise extract the necessary parts
       if(selectedCCExp.indexOf("/") != -1 ){
        expArr = new String(selectedCCExp).split("/");
       }else{
         expArr = new Array();
         expArr[0] = selectedCCExp.substr(0,2);
         expArr[1] = selectedCCExp.substr(2,2);;
       }

      for(var e = 0; e < ccMonthObj.length; e++){         
         if(ccMonthObj.options[e].value == expArr[0].toString()){
            ccMonthObj.selectedIndex = e;
            ccMonthObj.options[e].defaultSelected = true;
            break;
         }
      }
      for(var y = 0; y < ccYearObj.length; y++){         
         if(ccYearObj.options[y].value == expArr[1].toString()){
            ccYearObj.selectedIndex = y;
            ccMonthObj.options[y].defaultSelected = true;
            break;
         }
      }
   }
}
/*
 The Credit Card Exp year list must always 
 hold 10 years into the future. (including the current year)
*/
function buildCCYearList(list){
   var thisYear = new Date();   
   var theList = document.getElementById(list);
   var year = thisYear.getYear().toString();
   theList.options.length = 0; //clear the list
   theList.add(new Option("","",false,false));
   for(var i = 0; i <= 10; i++){ //add 10 years to current year create new options in list   
      theList.add(new Option((thisYear.getYear() + i), (thisYear.getYear() + i).toString().substr(2,2),false,false));
   }
}

/*
 Validate Payment drop down
 Visa, MasterCard, etc...
*/
function isPaymentTypeSelected(){
   var list = document.getElementById('pay_type');
      
  if(list.selectedIndex > 0 && list.options[list.selectedIndex].value != 'NC' &&
      list.options[list.selectedIndex].value != 'MS'){
     //set isPageValid to true becuase we are not checking the 
     //Manager approval and Military Star fields
      isPageValid = true;
      return true;
  }
   if(list.options[list.selectedIndex].value == 'NC'){
      //if pay type is No Charge check for manager approval fields
     isPageValid = isMgrApproval();    
      //return true becuase an item in the list has been selected
      //but the page will only submit if the form is valid and isPageValid equals true
      return true;
   }
   if(list.options[list.selectedIndex].value == 'MS' ){
        //if pay type is No Charge check for manager approval fields
     isPageValid = isAAEFSValid();    
      //return true becuase an item in the list has been selected
      //but the page will only submit if the form is valid and isPageValid equals true
      return true;
   }
   
	
  return false;
}
/*
 If pay type is Military Star a AAEFS code must 
 be entered before we can submit the page.
*/
function isAAEFSValid(){
   var valid = false;
   var input = document.getElementById('aafes_code_entered');
   
   var cell = document.getElementById('pay_type_validation_message_cell');
   var row = document.getElementById('pay_type_validation_message_row');
   
   if(input.value == ''){
      cell.innerText = "Required Field";
      input.className = "errorField";
      row.style.display = 'block';
     return valid;
   }
   if(input.value != ""){
     cell.innerText = "";
     input.className = "";
     row.style.display = "";
     valid = true;
   }
 return valid;
}
/*
 if pay type selected is NC 
 validate the manager approval code & password
*/
function isMgrApproval(){
   var valid = false;

   var mgrApproval = document.getElementById('mgr_appr_code');
   var mgrPassword = document.getElementById('password');
   var payType = document.getElementById('pay_type');

   var cell = document.getElementById('pay_type_validation_message_cell');
   var row = document.getElementById('pay_type_validation_message_row');

   var both = ( (mgrApproval.value == '' && mgrPassword.value == '') ) ? true : false;

   if(both){
      cell.innerText = 'Required Field(s)';
      mgrApproval.className = "errorField";
      mgrPassword.className = "errorField";
      row.style.display = 'block';     
     return valid;
   }
   if(!both && mgrApproval.value == ''){     
      cell.innerText = 'Required Field';
      mgrApproval.className = 'errorField';
      mgrPassword.className = '';
      row.style.display = 'block';
       return valid;
   }
   if(!both && mgrPassword.value == ''){
      
      cell.innerText = 'Required Field';
      mgrApproval.className = '';
      mgrPassword.className = 'errorField';
      row.style.display = 'block';
       return valid;
   }
   if( mgrPassword.value != '' && mgrApproval.value != '' ){    
      cell.innerText = '';
      mgrApproval.className = '';
      mgrPassword.className = '';
      row.style.display = 'none';
      valid = true;
   }
   isPageValid = valid;
  return valid;
}

/*
 Validate credit card number
*/
function isValidCreditCard(){
   var ccField = document.getElementById('credit_card_num');
   var payType = document.getElementById('pay_type');
   var creditCardType;
 
   //if the credit card field has been updated
   if(ccField.value == ''){
      isCardUpdated = true;
      isOriginalCardUsed = false;
       /* 
            if the credit card field is empty it's a required field
            otherwise if a value is supplied it will be an invalid format 
            if the value does not match any of the credit card types
       */      
      billingValidationArray[1][1] = "Required Field";
   }
  
    /*
      If we are here it means that the credit card has been updated
      we need to add  an N value to the is_orig_cc hidden field.            
    */
   if(!isOriginalCardUsed){       
      document.getElementById('is_orig_cc').value = 'N';
   }else{
      document.getElementById('is_orig_cc').value = 'Y';
   }

   /*
     The validation for Military Star credit card was pulled from utilities.jar
     which used the checkCreditCard(String ccType, String id) of the ValidateMembership class
     For this type of credit card we do not need to validate exp dates
   */
   if( Trim(payType.options[payType.selectedIndex].value) == 'MS' ){
     var bol = (ccField.value.length == 16 && ccField.value.substr(0,4) == '6019') ? true : false;           
       return bol; // return immediately as to not continue processing the rest of this method
   }

   /*
    Validate Credit Card Field 
    If the Credit Card has been modifed then we are no longer 
    using the original credit card and we must validate the credit card
   */
   var returnVal = true;

   if( (payType.options[payType.selectedIndex].value != 'IN' ) &&
          (payType.options[payType.selectedIndex].value != 'NC')  &&
             payType.options[payType.selectedIndex].value != 'GC' &&
                payType.options[payType.selectedIndex].value != 'PC')
   { 
      //if we are checking the credit card field 
      //also check that the month and year are selected also
     isPageValid = isMonthYearSelected(); 

      //check length if card is Military Star
      if( (isCardUpdated && !isOriginalCardUsed) || hasInputChanged(payType))
      {         
         //find the appropriate credit card type to use
         for(var m = 0; m < ccNames.length; m++)
         {             
            if(Trim(ccNames[m][0].toString()) == Trim(payType.options[payType.selectedIndex].value.toString()) )
            { 
                   creditCardType = ccNames[m][1]; 
                   break;
            }          
         }
            /*
               if the type does not exist in the array display a message
               other types (PC, NC...) shouldn't matter becuase we are checking
               for those types before we even get into this part of the method
            */
            if(creditCardType == null || creditCardType == "")
            {
               alert("Invalid Payment Type.");
               return;
            }
         //make sure the credit card type and number match        
         if(!isCardMatch(creditCardType, ccField.value) )
         {
            returnVal = false;
            //if the credit card number does not match the payment type selected display the following message.
            billingValidationArray[1][1] = "The credit card number is invalid. Please correct."
         }              
      }
   }   
  return returnVal;
}

/*
 Validate the Month & Year Expiration date lists
*/
function isMonthYearSelected(){
   //retrieve a reference to the lists
   var paymentList = document.getElementById('pay_type');
   var ccMonthList = document.getElementById('exp_date_month');
   var ccYearList = document.getElementById('exp_date_year');
   //retrieve a collection of list options
   var paymentOptions = paymentList.options;
   var ccMonthListOptions = ccMonthList.options;
   var ccYearListOptions = ccYearList.options;

   var valid = false;

   if( paymentOptions[paymentList.selectedIndex].value == 'NC' ||
         paymentOptions[paymentList.selectedIndex].value == 'IN' )
   { 
      if( ccMonthList.selectedIndex != 0 && ccYearList.selectedIndex != 0 ){
         //this should never happen lists are reset automagically when these values are selected
      }

   }else{ 
      if( ccMonthList.selectedIndex == 0 && ccYearList.selectedIndex == 0 ){
         //set error field and set display to pink for both fields        
         ccMonthList.className = 'errorField';
         ccYearList.className = 'errorField';
         document.getElementById('exp_date_month_validation_message_cell').innerText = 'Required Field(s)';       
         document.getElementById('exp_date_month_validation_message_row').style.display = 'block';
         valid = false;
      }
      if( ccMonthList.selectedIndex > 0 && ccYearList.selectedIndex == 0 ){

           //set year List        
         ccYearList.className = 'errorField';
         ccMonthList.className = '';
         document.getElementById('exp_date_month_validation_message_cell').innerText = 'Required Field';       
         document.getElementById('exp_date_month_validation_message_row').style.display = 'block';
         valid = false;
      }
      if(ccMonthList.selectedIndex == 0 && ccYearList.selectedIndex > 0 ){
         //set Month list

         ccMonthList.className = 'errorField';
         ccYearList.className = '';
         document.getElementById('exp_date_month_validation_message_cell').innerText = 'Required Field';       
         document.getElementById('exp_date_month_validation_message_row').style.display = 'block';
         valid = false;
      }
      if( ccMonthList.selectedIndex != 0 && ccYearList.selectedIndex != 0){
          ccMonthList.className = '';
          ccYearList.className = '';
          document.getElementById('exp_date_month_validation_message_row').style.display = 'none';
          valid = true;

      }     
   }
   return valid;
}


/*
 If we are processing an add bill we need to make sure the
 user has entered the changed dollar amounts appropriately.              
*/
function validateAddBillFields(){
   /* 
      use the event object to determine which 
      button triggered the event. Refresh button or Submit Billing
   */
    var eve = event.srcElement;

    var m = (document.getElementById('entered_merch') != null ) ? document.getElementById('entered_merch') : null;
    var a = (document.getElementById('entered_add_on') != null) ? document.getElementById('entered_add_on'): null ;
    var ss = (document.getElementById('entered_serv_ship') != null ) ? document.getElementById('entered_serv_ship'):null ;
    
   var fields = new Array(m,a,ss);

   var allAvailable = false;
   var atLeastOneHasData = false;
   var count = 0;

   for(var i = 0; i < fields.length; i++){
      if(fields[i] == null){
         count++;
      }
      if(fields[i] != null && fields[i].value != '' ){
         atLeastOneHasData = true;  
         //validate the field if data exists
         if(fields[i].name == 'entered_merch'){ if(!isEnteredMerch()) return false; }
         if(fields[i].name == 'entered_add_on') { if(!isEnteredAddOn()) return false; }
         if(fields[i].name == 'entered_serv_ship'){ if(!isEnteredServShip()) return false; }
      }
   }
   /* if count is zero they are all available (or displayed)*/
   if(count == 0) allAvailable = true;
           
    /* 
       if all the text boxes are displayed we must have at least one box with value's in order
       to refresh the page. we also test that if one of the boxes is not displayed due to a value less than zero
       at least one box has data
    */
   if(eve.name == 'refresh'){
      if( (allAvailable && !atLeastOneHasData) || (!allAvailable && !atLeastOneHasData)){
         alert("At least one amount must be entered");
         return false;
      }
   }
   return true;
}


/*
 Validate the Merchandise field
 @param isEmptyOk used to determine if the field
        we are validating is allowed to be empty or not
*/
function isEnteredMerch(){

   var merchField = document.getElementById('entered_merch');
   var isOk = isValid(merchField);

    handleErrorDisplay(isOk,merchField);
   
   return isOk;
}

/*
 Validate the Add-On field
*/



function isEnteredAddOn(){

   var addon = document.getElementById('entered_add_on');
   var isOk = isValid(addon);

   handleErrorDisplay(isOk,addon);

   return isOk;
}

/*
 Validate the Service/Shipping field
*/
function isEnteredServShip(){

   var servShip = document.getElementById('entered_serv_ship');
   var isOk = isValid(servShip);

    handleErrorDisplay(isOk,servShip);

   return isOk;
}

/*
 Display/Remove Error message formatting
*/
function handleErrorDisplay(ok,ele){
  if(!ok){
      setErrorField(ele);
  }else{
      removeErrorField(ele);
  }
}

/*
 @param field  current textbox being processed
 Used to validate the Merch,AddOn, Service/Shipping fields
*/
function isValid(field){
   var dec = '';
   var dol = '';
   var newValue = '';
   var reg = /^(\d){0,}\.\d\d$/g;
   var toReturn = true;
   var prefix = 'max_';

  // if(isEmptyOk && field.value == '') return true; 


   if(!field.disabled && field.value != ''){
       if(field.value.indexOf(".") == -1){ 
          
          dec = field.value.substr(field.value.length - 2, field.value.length);
          dol = field.value.substr(0, field.value.length - 2);
  
          //add a decimal to the dollar amount
          document.getElementById(field.name).value = newValue.concat(dol,'.',dec);  
         toReturn = true;
       }
      
       if(  field.value.indexOf(".") != -1 ){
          if(!reg.test(field.value) ){
             toReturn = false;
           }                 
       }     
       
       if(toReturn){

          var max = Number(Trim(document.getElementById(prefix + field.name).value.toString()));

             if( Number(field.value.toString()) > (max) ){
               toReturn = false;               
               isMaxExceeded = true;
             }
       }
   } 
   //else if(field.value == '' && !isEmptyOk)
   //{
       //toReturn = false;
   //}
   return toReturn;       
}

/*
  Generic method used to set the appropriate error
  information for the merch, addOn and Service Shipping fields
*/
function  setErrorField(field){
   var name = field.name;
   var ERROR = '_error';

   var td =    document.getElementById(name + ERROR);

   if(field.value == ''){     
      td.innerText = "Required Field";
   }else{
      td.innerText = "Invalid Format";
   }
   //if the max amount allowed is entered into the field set the error message
   if(  isMaxExceeded ){ td.innerText = "Invalid Amount"; }

   field.className = "errorField";
  
  //after every call to this method ensure that this variable is reset to false
   isMaxExceeded = false;
}
/*
 Generic method in order to remove any error formatting applied
*/
function removeErrorField(field){
  
   var name = field.name;
   var ERROR = '_error';

   var td = document.getElementById(name + ERROR);
   td.innerText = "";
   document.getElementById(field.name).className = '';
}


/*
 this function can be used to allow
 only numeric key presses in text fields
*/
function doOnlyNumeric(){
    var code = event.keyCode;
  
   if(code != 46 &&(code < 48 || code > 57)){
      return false;
   }
   //if a number was entered the user is attempting to update 
   //the credit card inforamtion, set this flag for use in the isValidCreditCard() method
   isCardUpdated = true;

   //if the user has updated the credit card info at all set
  //this flag to false.
   if( event.srcElement.name == 'credit_card_num'){    
      //if info has been updated we are no longer using the original cc information
      isOriginalCardUsed = false;
   }
   return true;  
}

/*
 Disables/Enables Credit Card fields based on 
 Payment Type selections
*/
function resetCreditCardLists(selectOne, selectTwo, ccText, setFlag){
   
   //clear the page of any previous errors (does not include mo/year expiration)
   validateFormResetPrevErrors(); 
   //clear year/month expiration error fields
   validateFormResetExpMonthYearErrors();
   //clear aafes, manager approval code and manager approval id error fields
   validateFormResetApprovalFields();
   //setCreditCardMaxLength();

   //hide/display NC/MS fields
   displayNoChrgMilitaryFields();

   var disable = false;
   var selectMonth =  document.getElementById(selectOne);
   var selectYear = document.getElementById(selectTwo);
   var eve = event.srcElement.value;

   disable = (eve == 'IN' || eve == 'NC' || eve == 'GC' || eve == 'PC' || eve == 'MS') ?  true:false;
   
   if(setFlag != null && setFlag != '' ) disable = setFlag;
  
   if(disable){
      selectMonth.selectedIndex = 0;
      selectMonth.disabled = disable;
      selectYear.selectedIndex = 0;
      selectYear.disabled = disable;
      document.getElementById(ccText).value = '';
       if(eve == 'MS') disable = false;//reset disable for MS type 
      document.getElementById(ccText).disabled = disable;
   }else{
      selectMonth.disabled = disable;
      selectYear.disabled = disable;
      document.getElementById(ccText).disabled = disable;
      if(setFlag == false){
         document.getElementById('pay_type').selectedIndex = 0;
      }
   }
   //if the user already received a status of valid for a Gift Certificate do 
   //not allow them to select GC again
   if(isGCSelected && eve == "GC") {
      alert("Only one gift certificate is allowed per charge.");
      document.getElementById('pay_type').selectedIndex = 0;
   }
}

/*
 These fields could not use the generic 
 validateForm() method from the util.js file
 these had to be dealt with seperately
*/
function validateFormResetExpMonthYearErrors(){
   document.getElementById('exp_date_month_validation_message_row').style.display = 'none';
   document.getElementById('exp_date_year_validation_message_row').style.display = 'none';
   document.getElementById('exp_date_year').className = '';
   document.getElementById('exp_date_month').className = '';
}

/*
 These fields could not use the generic 
 validateForm() method from the util.js file
 these had to be dealt with seperately
*/
function validateFormResetApprovalFields(){
   var row = document.getElementById('pay_type_validation_message_row');
   var appr = document.getElementById('mgr_appr_code');
   var id = document.getElementById('password');
   var aaefs = document.getElementById('aafes_code_entered');

   if(row.style.display == 'block' || row.style.display == 'inline' ){
      appr.className = '';
      id.className = '';
      aaefs.className = '';
      row.style.display = 'none';
   }
}

/*
  hide/display required fields for 
  Military Star & No Charge payment types
*/
function displayNoChrgMilitaryFields(){
   var payType = document.getElementById('pay_type');
   if(payType.options[payType.selectedIndex].value == 'MS'){
      document.getElementById('aafesField').style.display = 'inline';
      document.getElementById('noChargeField').style.display = 'none';
   }
   if (payType.options[payType.selectedIndex].value == 'NC')
   {
      document.getElementById('noChargeField').style.display = 'inline';
      document.getElementById('aafesField').style.display = 'none';
   }
   if(payType.options[payType.selectedIndex].value != 'NC' &&
         payType.options[payType.selectedIndex].value != 'MS')
   {
      document.getElementById('aafesField').style.display = 'none';
      document.getElementById('noChargeField').style.display = 'none';
   }
}

/*
executed if there are any
errors in the iframe processes.
*/
function doCallErrorPage(){
   var url = "Error.do?";
   document.getElementById('billingReviewForm').action = url;
   document.getElementById('billingReviewForm').submit();
}