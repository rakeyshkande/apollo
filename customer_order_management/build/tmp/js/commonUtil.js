/*
 * Common warning messages.
 */
var WARNING_MSG_4						= "Are you sure you want to exit without saving your changes?";
var WARNING_MSG_7						= "Are you sure you want to refund the entire shopping cart?";

var WARNING_INT_2_DMSTC						= "Changing from a Domestic address to an international address will require you to change the product and maybe the delivery date.  Do you wish to change the product?";
var WARNING_DMSTC_2_INT						= "Changing from an International Order to a Domestic order will require you to choose a different product.  Do you wish to change the product?";
var WARNING_WLMTI_PRODUCT					= "The product cannot be changed on Wal-Mart Orders.. If you wish to continue you will have to cancel this order and replace a new order with source code 9849.";
var WARNING_WLMTI_ORDER						= "Wal-Mart orders cannot be updated."
var WARNING_AMZNI_ORDER 					= "Order is an Amazon order and cannot be updated."
var WARNING_MRCNT_ORDER 					= " orders may not be updated."
var WARNING_PARTNER_ORDER 					= " orders may not be updated.";
var WARNING_PARTNER_PAYMENT 				= " payments may not be updated.";
var WARNING_ARIBA_ORDER 					= "Ariba orders cannot be updated."
var WARNING_FRAUD 						= "Order currently in the LP queue and cannot be updated.";
var WARNING_NO_PAYMENTS 					= "Payments cannot be authorized at this time.\nPlease try again later.";
var WARNING_ORDER_IN_CREDIT_QUEUE                               = "Order must be removed from the Credit Queue before it can be updated.";
var WARNING_SFMB 						= "San Francisco Music Box orders cannot be updated.";
var WARNING_JCP 						= "JCPenney orders cannot be updated.";
var WARNING_ORDER_AUTHING 				        = "Payments cannot be authorized at this time.  Please try again later";
var WARNING_PERSONALIZATION_PRODUCT                             = "You are unable to perform this operation for a personalized order.  Please contact your manager for further instruction.";
var CALL_REASON_CODE_SCREEN_DISPLAY       = "Y";
var WARNING_FREESHIP_PRODUCT                                    = "Order for this product type cannot be updated.";
var WARNING_GD_SOURCE_CODE                = "The source code provided does not allow for a Gift Card/Certificate, please change to a source code that allows for Gift Card/Certificate purchases. Do you wish to continue with the new source code?";
var WARNING_SDFC_PRODUCT_SELECTED         = "Product is Flex Fill. To make any changes use alternate methods";
/*
   Helper function to print the user screen
*/
function printIt(){
	window.print();
}
/*
This function formats the dollar amount entered in the text field with decimal places.
*/
function formatDollarAmount(textbox){
  if(textbox.value != null && textbox.value != ""){
		if(!isNaN(textbox.value)){
      if(textbox.value.substring(0,1) == '-'){
        return false;
      }
			if(textbox.value.indexOf('.') == -1){
				textbox.value=textbox.value+'.00';
			}else{
				var startPos = textbox.value.indexOf('.');
				var decimals = textbox.value.substring(startPos*1);
				if(decimals.length == 2){
					decimals = decimals+'0';
					textbox.value = textbox.value.substring(0,startPos*1)+decimals;
				}else if(decimals.length == 1){
					decimals = decimals+'00';
					textbox.value = textbox.value.substring(0,startPos*1)+decimals;
				}else if(decimals.length > 3){
					decimals = decimals.substring(0,3);
					textbox.value = textbox.value.substring(0,startPos*1)+decimals;
				}
			}
		}else{
      return false;
		}
  }
  return true;
}

/*
This function formats the dollar amount entered in the text field with decimal places.
*/
function formatDollarAmt(textbox){
    if(textbox != null && textbox != "") {
	    if(!isNaN(textbox)) {
            if(textbox.substring(0,1) == '-') {
                return textbox;
            }
			if(textbox.indexOf('.') == -1){
				textbox = textbox + '.00';
			} else {
				var startPos = textbox.indexOf('.');
				var decimals = textbox.substring(startPos*1);
				if(decimals.length == 2){
					decimals = decimals+'0';
					textbox = textbox.substring(0,startPos*1)+decimals;
				}else if(decimals.length == 1){
					decimals = decimals+'00';
					textbox = textbox.substring(0,startPos*1)+decimals;
				}else if(decimals.length > 3){
					decimals = decimals.substring(0,3);
					textbox = textbox.substring(0,startPos*1)+decimals;
				}
			}
		}
    }
    return textbox;
}



/*
This function formats the dollar amount with decimal places and returns a String
*/
function formatAmount2DecimalPlaces(input)
{
  var output = input;
  input = input + "";
  if(input != null && input != "" )
  {
    if(input.indexOf('.') == -1)
    {
      output = input + '.00';
    }
    else
    {
      var startPos = input.indexOf('.');
      var decimals = input.substring(startPos*1);
      if(decimals.length == 2)
      {
        decimals = decimals+'0';
        output = input.substring(0,startPos*1)+decimals;
      }
      else if(decimals.length == 1)
      {
        decimals = decimals+'00';
        output = input.substring(0,startPos*1)+decimals;
      }
      else if(decimals.length > 3)
      {
        decimals = decimals.substring(0,3);
        output = input.substring(0,startPos*1)+decimals;
      }
    }
  }
  return output;
}



 /*
    This function handels the back button functionality
 function doBackAction(){
	var url = "BackButton.do" +
		    "?action=back_button" + getSecurityParams(false);
	performAction(url);
 }
 */

 /*
  This function handles the main menu functionality for all pages.
 */
function doMainMenuAction(siteNameInput, applicationContextInput){
    if (!cdisp_showWidgetIfRequired()) {
        var url = '';
        var urlPrefix = '';
        if (siteNameInput != null && applicationContextInput != null) {
          urlPrefix = "http://" + siteNameInput + applicationContextInput + "/";
        }
        url = urlPrefix + "mainMenu.do?work_complete=y" + getSecurityParams(false);
        performAction(url);
    }
}

/*
 Displays the Florist Maintenance screen for the user as
 a modal dialog window
*/
function doFloristMaintenanceAction(url){
		var modal_dim = 'dialogWidth=950px,dialogHeight=425px,dialogLeft=0px,dialogTop=90px';
	     	  modal_dim +='location=0,status=0,menubar=0,scrollbars=0,resizable=0';
    showModalDialogIFrame(url, "", modal_dim);

}

/*
	Displays the Load Memeber Data Search page
	in a modal dialog.
*/
 function doFloristInquiryAction(url){
 	var modal_dim = 'dialogWidth=950px,dialogHeight=425px,dialogLeft=0px,dialogTop=90px';
	     	  modal_dim +='location=0,status=0,menubar=0,scrollbars=1,resizable=0';
    showModalDialogIFrame(url, "", modal_dim);
}

function doLossPreventionSearchAction() {
    if (!cdisp_showWidgetIfRequired()) {
        var url = "customerOrderSearch.do?action=load_lpsearch_page";
        performAction(url);
    }
}

/***********************************************************************************
* doSearchAction()
************************************************************************************/
function doSearchAction()
{
    if (!cdisp_showWidgetIfRequired()) {
        var url = "customerOrderSearch.do?action=load_page";
	performAction(url);
    }
}

/***********************************************************************************
*doMainMenuActionNoPopup() -- take to the main menu without asking if work is complete
************************************************************************************/
function doMainMenuActionNoPopup(){
	var url = "mainMenu.do?work_complete=n" + getSecurityParams(false);
	performAction(url);
}


/***********************************************************************************
*showModalAlert() -- display a modal popup message
************************************************************************************/
function showModalAlert(message)
{
	//Modal_URL
	modalUrl = "alert.html";

	//Modal_Arguments
	var modalArguments = new Object();
	modalArguments.modalText = message;

	//Modal_Features
	var modalFeatures = 'dialogWidth:200px; dialogHeight:150px; center:yes; status=no; help=no;';
	modalFeatures += 'resizable:no; scroll:no';

	window.showModalDialog(modalUrl, modalArguments, modalFeatures);
}


/***********************************************************************************
* doExitPageAction()
************************************************************************************/
function doExitPageAction(popupText){
    return doExitPageActionSetSize(popupText, '200px', '150px');
}

/***********************************************************************************
* doExitPageAction() and allows you to set the size of the popup
************************************************************************************/
function doExitPageActionSetSize(popupText, width, height){
  //Modal_URL
  	var modalUrl = "confirm.html";

  	//Modal_Arguments
  	var modalArguments = new Object();
  	modalArguments.modalText = popupText;

  	//Modal_Features
  	var modalFeatures  =  'dialogWidth:'+width+';';
  	modalFeatures +=  ' dialogHeight:'+height+';';
  	modalFeatures +=  'center:yes; status=no; help=no;';
  	modalFeatures +=  'resizable:no; scroll:no';

  	//get a true/false value from the modal dialog.
  	var ret = window.showModalDialog(modalUrl, modalArguments, modalFeatures);

  	if(ret)
  	{
  		return true;
	}
  	else
  	{
		return false;
	}

}

function formatNumber(inputNumber) {
    inputNumber += '';
    x = inputNumber.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;

}


/***********************************************************************************
* cdisp_clearParams() - Clears Call Disposition parameters
************************************************************************************/
function cdisp_clearParams() {

    // Sets flag to signal data filter to clear Call Disposition params.
    // Also call decision_result function to clear widgets params.
    //
    var resetFlag = document.getElementById('cdisp_resetFlag');

    if (resetFlag != null) {
        resetFlag.value = 'Y';
    }
    dcsnClearParams();
    resetDataFields();
}

/***********************************************************************************
* cdisp_showWidgetIfRequired() - Displays Call Disposition Widget if necessary
************************************************************************************/
function cdisp_showWidgetIfRequired() {
    var retVal = false;
    var isEnabled  = document.getElementById('cdisp_enabledFlag');
    var isReminder = document.getElementById('cdisp_reminderFlag');

    // Call Dispo is displayed only if enabled (based on CSR permission and DNIS)
    //
    if ((isEnabled != null) && (isEnabled.value == 'Y')) {

        // Use Decision Result javascript to determine if any orders require a disposition
        // and pop-up widget if necessary
        //
        var exitAllowed = dcsnExitAllowed();

        // Yup, so pop-up
        //
        if (!exitAllowed) {
            loadCallDispMandatory();
            retVal = true;

        // Not required, so check if reminder flag is set and make sure there
        // is at least one order that was submitted or declined.  If not we want to
        // pop-up widget too.
        //
        } else if ((isReminder != null) && (isReminder.value == 'Y') && (!dcsnWereAnySubmittedOrDeclined())) {
            dcsnDeclineAllOptional();   // Do this so reminder will only pop-up once
            loadCallDispOptional();
            retVal = true;

        // Otherwise just clear Call Disposition data
        } else {
            cdisp_clearParams();
            retVal = false;
        }
    }

    return retVal;
}



/***********************************************************************************
* doConfirmPageAction() presents a modal confirmation prompt
************************************************************************************/
function doConfirmPageAction(popupText){
    return doConfirmPageActionSetSize(popupText, '200px', '150px');
}

/***********************************************************************************
* doConfirmPageActionSetSize() resents a modal confirmation prompt and allows you
* to set the size of the popup
************************************************************************************/
function doConfirmPageActionSetSize(popupText, width, height){
  //Modal_URL
  	var modalUrl = "confirm.html";

  	//Modal_Arguments
  	var modalArguments = new Object();
  	modalArguments.modalText = popupText;

  	//Modal_Features
  	var modalFeatures  =  'dialogWidth:'+width+';';
  	modalFeatures +=  ' dialogHeight:'+height+';';
  	modalFeatures +=  'center:yes; status=no; help=no;';
  	modalFeatures +=  'resizable:no; scroll:no';

  	//get a true/false value from the modal dialog.
  	var ret = window.showModalDialog(modalUrl, modalArguments, modalFeatures);

  	if(ret)
  	{
  		return true;
	}
  	else
  	{
		return false;
	}
}