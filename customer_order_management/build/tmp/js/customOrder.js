/*
 * (This javascript along with the customOrder.xsl file were copied
 * over from the order_scrub project and chagned to work with C.O.M.
 * The following javascript is used in conjunction with the C.O.M customOrder.xsl
 */

/*
 Warning messages
*/
var CANCEL_ACTION       = "cancel_update";
var CANCEL_UPDATE_PAGE  = 'cancelUpdateOrder.do?';
var CUSTOM_ACTION 			= "custom_action";
var tabsArray = new Array("radPrice", "radVariable", "variablePriceAmt", "florist_comments", "continueButton", "custCancel");

function init() {
  document.oncontextmenu = contextMenuHandler;
  document.onkeydown = coBackKeyHandler;
  addDefaultListeners();
  untabAllElements();

  initErrorMessages();
  setTabIndexes(tabsArray, 1);

  if	(	originalProductPrice > customProductPrice) 
  {
    var size = document.all.PRICE.length;
    for (i=0; i<size; i++)
    {
      if ( document.all.PRICE[i].value == "variable" )
        document.all.PRICE[i].checked = true;
    }
  }

}

/***  This function will limited to length of the gift message to 240*/
function checkFloristCommentsSize(){
  var availableFieldLength = 240;
  if(document.all.florist_comments.value.length > availableFieldLength)
  {
    document.all.florist_comments.value = document.all.florist_comments.value.substring(0,(availableFieldLength));
  }
}


function onKeyDown(){
  if (window.event.keyCode == 13)
  {
    submitForm();
  }
}


function resetRadio( value,ele ) {
  var STANDARD = "standard";
  var VARIABLE = "variable";
  var stan = document.getElementById('radPrice');
  var vari = document.getElementById('radVariable');
  var variText =  document.getElementById('variablePriceAmt');

  if (vari.value == VARIABLE && ele == VARIABLE)
  {
    vari.checked = true;
  }

}


function isValidUSDollar( dollar )
{
   if ( dollar == null || dollar == "" )
  return false;

   first = dollar.indexOf(".");

   if ( first != -1 )
   {
  dollar_length = dollar.length;

  firstSub = dollar.substr( 0, first );
  secondSub = dollar.substr( first + 1, dollar_length - first );

  if ( ( dollar_length - first ) == 1 )
    return isInteger( firstSub );
  else
    return ( isInteger( firstSub ) && isInteger( secondSub ) );

    } else {
  return isInteger( dollar );
    }

  return false;
}

/*
 * Used to go back to the Update Product Category page.
 */
function doUpdateProductCategoryAction() {
  var toSubmit = document.getElementById("coForm");
  toSubmit.action = "loadProductCategory.do";
  toSubmit.submit();
}

/*
 Function handles the Cancle Update button
*/
function doCancelUpdateAction(){
  if ( hasFormChanged() ) {
    if ( doExitPageAction(WARNING_MSG_4) ) {
      baseDoCancelUpdateAction("coForm");
    }
  }
  else {
    baseDoCancelUpdateAction("coForm");
  }
}


function validateVariablePrice(){
  var variable = document.getElementById("VARIABLE_PRICE_AMT");
  var variablePrice = parseFloat(variable.value);
  var oldProductPrice = parseFloat(document.getElementById("orig_product_amount").value);

  //Check to make sure the VARIABLE_PRICE_AMT is valid US dollar
  checked = isValidUSDollar ( variable.value );
  if ( !checked ) {
    alert("Invalid Varible price: '" + variable.value + "'");
    variable.focus();
    variable.style.backgroundColor='pink';
  }

  else if ( !(customProductPrice <= variablePrice && variablePrice <= variablePriceMax) ) {
    alert("Varible price range: " + customProductPrice + " - " + variablePriceMax);
    variable.focus();
    variable.style.backgroundColor='pink';
    checked = false;
  }
  return checked;
}

function checkVariablePrice( value ) {
  if ( value != "" ) {
    document.all.VARIABLE_PRICE_AMT.style.backgroundColor = "white";
  }
}

function validateForm() {
  checked = true;

  // price
  size = document.all.PRICE.length;
  for (i=0; i<size; i++) {
    if (document.all.PRICE[i].checked && document.all.PRICE[i].value == "variable" ) {
      checked = validateVariablePrice();
    }
  }

  // description
  document.all.florist_comments.className="TblText";
  var a1nws = stripWhitespace(document.all.florist_comments.value);

  if ( a1nws.length == 0 ){
    if ( checked ){
      document.all.florist_comments.focus();
      checked = false;
    }
    document.all.florist_comments.className="Error";
  }
  return checked;
}

function submitForm(){
  if ( validateForm() ){
    //remove personal greeting id if necessary
    if (document.getElementById('personal_greeting_flag').value == 'N' && document.getElementById('personal_greeting_id').value != null)
    {
        document.getElementById('personal_greeting_id').value = '';
    }
    doRecordLock('MODIFY_ORDER','check',"", "coForm");
  }
  else{
    alert("Please correct the marked fields.");
    return false;
  }
}

/*
 * Finish Submit
 */
function continueSubmit(){
  showWaitMessage("mainContent", "wait");
  prepareSubmit();
  document.getElementById('coForm').submit();
}

function prepareSubmit() {
  var standard = document.getElementById("radPrice");
  var variable = document.getElementById("radVariable");
  var amount = document.getElementById("product_amount");

  if ( standard.checked ) {
    amount.value = customProductPrice;
  }
  else if ( variable.checked ) {
    amount.value = document.getElementById("variablePriceAmt").value;
  }
}

/*
 * Called by checkLockIFrame.xsl
 */
function checkAndForward(forwardAction){
   continueSubmit();
}

/*
   Disables the various navigation keyboard commands.
*/
function coBackKeyHandler() {
   // backspace
   if (window.event && window.event.keyCode == 8){
      var formElement = false;

      for(i = 0; i < document.forms['coForm'].elements.length; i++){
         if(document.forms['coForm'].elements[i].name == document.activeElement.name){
            formElement = true;
            break;
         }
      }
      var searchBox = document.getElementById('numberEntry');
      if(searchBox != null){
        if(document.getElementById('numberEntry').id == document.activeElement.id){
          formElement = true;
        }
      }

      if(formElement == false || (document.activeElement.type != "text" && document.activeElement.type != "textarea" && document.activeElement.type != "password")){
         window.event.cancelBubble = true;
         window.event.returnValue = false;
         return false;
      }
   }

   // F5 and F11
   if (window.event.keyCode == 122 || window.event.keyCode == 116){
      window.event.keyCode = 0;
      event.returnValue = false;
      return false;
   }

   // Alt + Left Arrow
   if(window.event.altLeft){
      window.event.returnValue = false;
      return false;
   }

   // Alt + Right Arrow
   if(window.event.altKey){
      window.event.returnValue = false;
      return false;
   }
}


function initErrorMessages() {
  if ( fieldErrors.length > 0 || okErrors.length > 0 || confirmErrors.length > 0 )
  {
    var array = doDisplayServerSideValidationMessages();
    if ( array.goToPage != null && array.goToPage != "" )
    {
      doCustomAction( array.goToPage );
    }
    else if ( array.jsFunction != null)
    {
      array.jsFunction();
    }


  }
}


function doCustomAction(action)
{
  callingAction = CUSTOM_ACTION;
  document.getElementById("coForm").action = action;
  doRecordLock('MODIFY_ORDER', 'check', null, "coForm");
}


function doDisplayServerSideValidationMessages() {
  var ID = 0;
  var TEXT = 1;
  var YES = 2;
  var NO = 3;
  var toReturn = new Array();

  // highlight errors
  if ( fieldErrors != null && fieldErrors.length > 0 ) {
    validateForm(fieldErrors);
  }

  // error messages requiring an 'Ok' popup
  if ( okErrors != null && okErrors.length > 0 ) {
    // concatenate error messages
    var messages = "";
    for (var i = 0; i < okErrors.length; i++) {
      messages += okErrors[i][TEXT] + "<br><br>";
    }

    // modal arguments
    var modalArguments = new Object();
    modalArguments.modalText = messages;

    // modal features
    var modalFeatures = 'dialogWidth:300px; dialogHeight:200px; center:yes; status=no; help=no; resizable:no; scroll:yes';
    toReturn.okErrorReturnValue = window.showModalDialog("alert.html", modalArguments, modalFeatures);
    if ( toReturn.okErrorReturnValue ) {
      toReturn.okGoToPage = okErrors[0][YES];
    }
  }

  // error messages requiring a 'Yes/No' popup
  if ( confirmErrors != null && confirmErrors.length > 0 ) {
    var urlOrId = "";
    for ( var i = 0; i < confirmErrors.length; i++) {
      if ( urlOrId == '' || urlOrId == confirmErrors[i][ID] ) {
        // concatenate error messages
        var messages = confirmErrors[i][TEXT];

        // modal arguments
        var modalArguments = new Object();
        modalArguments.modalText = messages;

        // modal features
        var modalFeatures = 'dialogWidth:300px; dialogHeight:200px; center:yes; status=no; help=no; resizable:no; scroll:yes';
        toReturn.confirmErrorReturnValue = window.showModalDialog("confirm.html", modalArguments, modalFeatures);
        if ( toReturn.confirmErrorReturnValue ) {
          urlOrId = confirmErrors[i][YES];
        }
        else {
          urlOrId = confirmErrors[i][NO];
        }
      }
    }

    // if a JS function needs to be called, uncomment the following:
    //a JS function may need to be called, if so check wether or not the
    // urlOrId field contains an action or not
/*
    if ( urlOrId == abc )
    {
      toReturn.jsFunction = doShowUpdateProductCategoryDiv;
    }
    else
    {
*/
      toReturn.goToPage = urlOrId;
/*
    }
*/
  }

  return toReturn;
}

/*
 * Handle the Update Delivery Information button event
 */
function doUpdateDeliveryInfo(){
var url = 'loadDeliveryInfo.do';
performAction(url);
}

/*
 * Handle the Update Product Category button event
 */
function doUpdateProductCategory(){
  var url = 'loadProductCategory.do';
  performAction(url);
}

/*******************************************************************************
*
*    performAction()
*
*******************************************************************************/
function performAction(url){
    /*
      Display a message to the user called Processing...
      this allows the user to know that the system is working
      and not stalled out.
      scroll(0,0) returns the window to the top position since
      all(most) buttons are located at the bottom of a scrolled window
    */
     scroll(0,0);
   
     showWaitMessage("mainContent", "wait", "Processing");

     document.forms[0].action = url;
     document.forms[0].submit();
}

