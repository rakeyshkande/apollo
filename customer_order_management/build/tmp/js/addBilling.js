/*
 The following js file is a utility file for the addBilling.xsl page
*/
var validationMatrix = new Array
     (//input attribute name           error message                          validation function   additional function parameters
      ["pay_type",                   "Required Field",                             isPaymentTypeSelected,     ],
      ["credit_card_num",            "Invalid Format",                             isValidCreditCard,         ],
      ["merchandise_display",                "Invalid Format 9.99",                        isMerchValid,              ],
      ["add_on_display",                     "Invalid Format 9.99",                        isAddOnValid,              ],
      ["service_shipping_display",           "Invalid Format 9.99",                        isServShipValid,           ]
     );

var recalculateMatrix = new Array
     (//input attribute name           error message                          validation function   additional function parameters
      ["merchandise_display",                "Invalid Format 9.99",                        isMerchValid,              ],
      ["add_on_display",                     "Invalid Format 9.99",                        isAddOnValid,              ],
      ["service_shipping_display",           "Invalid Format 9.99",                        isServShipValid,           ]
     );

var ccLengths = new Array
     ( //card name                   length
       //Visa
       ["VI",                  "16"],
       //Master Card
       ["MC",                  "16"],
       //Discover
       ["DI",                  "15"],
       //American Express
       ["AX",                  "15"],
       //Diners Club
       ["DC",                  "14"],
       //JCPenny
       ["JP",                  "11"],
       //Military Star
       ["MS",                  "16"],
       //Other Cards
       ["others",              "16"]
     );
/*
 Valid Payment Types
*/
var ccNames = new Array
     ( //card name                   length
       //Visa
       ["VI",                  "VISA"        ],
       //Master Card
       ["MC",                  "MASTERCARD"  ],
       //Discover
       ["DI",                  "DISCOVER"    ],
       //American Express
       ["AX",                  "AMEX"        ],
       //Diners Club
       ["DC",                  "DINERS"      ],
       //Carte Blanche
       ["CB",                  "CARTEBLANCHE"]
     );



var isPageValid = false;
/*
 Used to determine if we should display the
 Gift Certificate popup again or not
*/
var isGCSelected = false;
/*
 Used to help decide if the Credit Card being
 validated is the original card or not.
*/
var isCardUpdated = false;
var isOriginalCardUsed = true;
/*
 Used to determine if the user entered in
 a charge amount in any of the three fields
 Addon, Serv/Ship, Merchendise
*/
var validChargeAmount = false;
var messageToDisplay = "";
var isAddOnValidValue = false;
var isServShipValidValue = false;
var isMerchValidValue = false;


var hasBeenRecalculated = false;
var submitted = false;
/*
 executed when the Submit button is clicked
*/
function doSubmitBilling(){

   validChargeAmount = checkValidChargeEntered();

   document.getElementById('return_action').value = "submit";

   if(!validChargeAmount){
      alert(messageToDisplay); // display message if problems w/ Charge amount fields
    return;
   }

  /*
   it's possible for the user to not mouse out of one of the text fields
   therefore a recalculation will not be performed. If this is the case and they hit the Submit button
   two events will occur w/ the first being the onblur event and then the button event.
   In this case we will recalculate the order again, but this time passing a return_action=submit
   after execution returns we check the return_action parameter and set submitted = true.
   After recalculateOrder returns and loads the recalculateOrder.xsl is transfored it will
   call the displayRecalculation(order_amt, tax_amt, return_action) mehtod.
   This method will then check for the return_action and call the completeBillingProcess() method to finish off the process
  */

  if( document.getElementById('billing_level').value != "cart" ) {
     recalculateOrder();
  } else {
     completeBillingProcess();
  }

}//end method doSubmitBilling()

/*
 Called after we return from recalculating an order and the user
 has clicked the Submit button
*/
function completeBillingProcess(){

   var payment = document.getElementById('pay_type');
   if(payment.options[payment.selectedIndex].value.toString() == 'GC' && isGCSelected == false){

      doGiftCertificate();
      return;
   }

      //validate the form
   var doSubmit = validateForm(validationMatrix);
     //if main fields and misc.are valid submit
   if( doSubmit && isPageValid ){
      document.getElementById('page_action').value = "apply_billing";

      showWaitMessage("mainContent","wait","Validating");

      if( document.getElementById('billing_level').value == "cart" ){
          doGUIDRecordLock('PAYMENTS','retrieve',orderLevel,'addBillingForm');
      }else{
          doRecordLock('PAYMENTS','retrieve',orderLevel,'addBillingForm');
      }
   } //end if( doSubmit && isPageValid )
}//completeBillingProcess()

/*
 Check Serv/Ship, AddOn, Merch Fields
 for total amounts
*/
function checkValidChargeEntered(){
   var valid = true;
   /*
     if we get to this method we know that the user has entered in some sort of valid values into the
     Serv/Ship, Addon and Merch fields. At this point we must add them to ensure they are greater than 0.00
     or if no values are entered we display a message stating so.
   */
   var merch = document.getElementById('merchandise_display').value;
   var addOn = document.getElementById('add_on_display').value;
   var servShip = document.getElementById('service_shipping_display').value;

   try{
      //charge amount must be > 0.00

      var total;

      total = Number(Trim(merch.toString())) + Number(Trim(addOn.toString())) + Number(Trim(servShip.toString()));

      if( parseFloat(total) <= 0 ){
         valid = false;
         messageToDisplay = "Charge must be greater than 0.00.";
      }
   }catch(e){
      alert("ERROR " + e);
      valid = false;
   }
   //charge amount fields can not be empty
   if(merch == "" && addOn == "" && servShip == "" ){
      valid = false;
      messageToDisplay = "Charge amount must be entered before it can be processed.";
   }
  return valid;
}

/*
 Check payment type for (GC) Gift Certifciate type
*/
function doGiftCertificate(){

   redeemGcCoupon();

   if(Trim(document.getElementById('amount_owed').value.toString()) == 'false' ){

    document.getElementById('page_action').value = "apply_billing";

    if( document.getElementById('billing_level').value == "cart" ){
          doGUIDRecordLock('PAYMENTS','retrieve',orderLevel,'addBillingForm');
    }else{
          doRecordLock('PAYMENTS','retrieve',orderLevel,'addBillingForm');
    }
  }
}
/*
 executed if the check lock was successful
*/
function doSubmitBillingAction(){
   //  Send a POST request via AJAX to submit the billing
   var url = 'addBilling.do';
	 var parameters = getFormNameValueQueryString(  document.getElementById('addBillingForm') );
	 parameters = parameters.substring(1);
	 xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
	 xmlHttp.onreadystatechange = evalSubmitBillingAction;
   xmlHttp.open("POST", url , true);
   xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
   xmlHttp.setRequestHeader("Content-length", parameters.length);
   xmlHttp.send(parameters);
}


/*
 Evaluates response from the addBilling apply billing functionality via AJAX
*/
function evalSubmitBillingAction()
{
	 if (xmlHttp.readyState == 4 || xmlHttp.readyState == "complete")
   {
      var xml = getXMLobjXML(xmlHttp.responseText);

      // If there was an error display the error page
			var isError = xml.selectSingleNode("root/is_error").text;
      if(isError == "Y")
			{
				doCallErrorPage();
				return;
			}

			var msgStatus = xml.selectSingleNode("root/status/data/message_status").text;
      var msgAction = xml.selectSingleNode("root/status/data/message_action").text;
      var msgTxt = xml.selectSingleNode("root/status/data/message_text").text;

			// If billing was successful go to the Update Billing
			// screen
			if(msgStatus == 'success')
			{
				updateBillingAction();
			}
			// If billing was not successful display the reason
			// why to the user
			else if(msgStatus == 'declined' )
			{
				invokeDeclinePopup(msgAction, msgTxt);
			}

   }
 }


/*
 Calls the addBilling recalculate order functionality via AJAX
*/
function doRecalculateOrderAction()
{
   //  Send a POST request via AJAX to recalculate the order
	 var url = 'addBilling.do';
	 var parameters = getFormNameValueQueryString(  document.getElementById('addBillingForm') );
	 parameters = parameters.substring(1);
	 xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
	 xmlHttp.onreadystatechange = evalRecalculateOrderAction;
   xmlHttp.open("POST", url , true);
   xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
   xmlHttp.setRequestHeader("Content-length", parameters.length);
   xmlHttp.send(parameters);
}


/*
 Evaluates response from the addBilling recalculate order functionality via AJAX
*/
function evalRecalculateOrderAction()
{
	if (xmlHttp.readyState == 4 || xmlHttp.readyState == "complete")
	{
		// Obtain response data
		var xml = getXMLobjXML(xmlHttp.responseText);

		// If there was an error display the error page
		var isError = xml.selectSingleNode("root/is_error").text;
		if(isError == "Y")
		{
			doCallErrorPage();
			return;
		}

		var orderAmount = xml.selectSingleNode("root/data/order_amount").text;
		var taxAmount = xml.selectSingleNode("root/data/tax_amount").text;
		var returnAction = xml.selectSingleNode("root/data/return_action").text;

		// set the various tax related values
		var tax1_name = get_tax_value(xml, "root/TAXES/TAX1/tax1_name");
		var tax1_description = get_tax_value(xml, "root/TAXES/TAX1/tax1_description");
		var tax1_rate = get_tax_value(xml, "root/TAXES/TAX1/tax1_rate");
		var tax1_amount = get_tax_value(xml, "root/TAXES/TAX1/tax1_amount");

		var tax2_name = get_tax_value(xml, "root/TAXES/TAX2/tax2_name");
		var tax2_description = get_tax_value(xml, "root/TAXES/TAX2/tax2_description");
		var tax2_rate = get_tax_value(xml, "root/TAXES/TAX2/tax2_rate");
		var tax2_amount = get_tax_value(xml, "root/TAXES/TAX2/tax2_amount");

		var tax3_name = get_tax_value(xml, "root/TAXES/TAX3/tax3_name");
		var tax3_description = get_tax_value(xml, "root/TAXES/TAX3/tax3_description");
		var tax3_rate = get_tax_value(xml, "root/TAXES/TAX3/tax3_rate");
		var tax3_amount = get_tax_value(xml, "root/TAXES/TAX3/tax3_amount");

		var tax4_name = get_tax_value(xml, "root/TAXES/TAX4/tax4_name");
		var tax4_description = get_tax_value(xml, "root/TAXES/TAX4/tax4_description");
		var tax4_rate = get_tax_value(xml, "root/TAXES/TAX4/tax4_rate");
		var tax4_amount = get_tax_value(xml, "root/TAXES/TAX4/tax4_amount");

		var tax5_name = get_tax_value(xml, "root/TAXES/TAX5/tax5_name");
		var tax5_description = get_tax_value(xml, "root/TAXES/TAX5/tax5_description");
		var tax5_rate = get_tax_value(xml, "root/TAXES/TAX5/tax5_rate");
		var tax5_amount = get_tax_value(xml, "root/TAXES/TAX5/tax5_amount");


		// Display recalculated amounts
		displayRecalculation(orderAmount, taxAmount, returnAction,
				tax1_name, tax1_description, tax1_rate, tax1_amount,
				tax2_name, tax2_description, tax2_rate, tax2_amount,
				tax3_name, tax3_description, tax3_rate, tax3_amount,
				tax4_name, tax4_description, tax4_rate, tax4_amount,
				tax5_name, tax5_description, tax5_rate, tax5_amount
			);
	}
 }

/*
 * Return the value for the given node in the given xml, handling null/non-existant
 * nodes (returns null in that case)
 */
function get_tax_value(xml, nodepath) {
	var node = xml.selectSingleNode(nodepath);
	if (node != null)
		return node.text;
	else
		return;
}


/*
 required method, when implementing locking
 this is called by checkLockIFrame.xsl
*/
function checkAndForward(forwardAction){
      doSubmitBillingAction();
}

/*
 called when the submitBillingAction
 is 'successful'
*/
function updateBillingAction(){

   //showWaitMessage("mainContent","wait","Validating");

   // ***** Make sure this is a POST request!!!!  If changed to a GET request
	 // make sure no sensitive information is sent.  The URL from the
	 // GET request is displayed within the Apache logs *****
   document.getElementById('addBillingForm').method = 'POST';
   var url = 'updateBilling.do?';
   document.getElementById('addBillingForm').action = url;
   document.getElementById('addBillingForm').submit();
}

/*
 recalculate the order and update the page
 called by doSubmitBilling()
*/
function recalculateOrder(){

   document.getElementById('page_action').value = "recalculate_order";

   var flag;
      //validate the Addon, Serv/Ship, and Merch fields
   if(validateForm(recalculateMatrix)){
            //if the fields equal zero or are empty display a message otherwise recalculate
      flag = checkValidChargeEntered();
      if(flag) doRecalculateOrderAction(); else alert(messageToDisplay); //doSubmitBillingAction();
    }
  /* }else{

         var textArray = new Array('merchandise','add_on','service_shipping');
         var isReady = false;
         var emptyCount = 0;

         for(var i = 0; i <= textArray.length; i++){
            if( document.getElementById( textArray[i] ).value == "" ){
               ++emptyCount;
            }
         }
         if(validateForm(recalculateMatrix)){
            if( emptyCount <= 3 && emptyCount > 0 ){
               isReady = checkValidChargeEntered();
            }

            if(isReady){
               doSubmitBillingAction();
            }
         }
   }*/
}

/*
 triggered when the Merch, Add-On, or Service Shipping
 fields lose focus
*/
function changeRecalculateOrder(){

   /*
      if the user tabs out of the text box
      and the there is no value entered just return
      so we do not always display an alert box
   */
   var eve = event.srcElement;
   if(eve.value == ""){
      return;
   }

   document.getElementById('page_action').value = "recalculate_order";
   //check to see if the user triggered a recalculate by tabbing out of one of the fields.
   var flag;
      //validate the Addon, Serv/Ship, and Merch fields
   if(validateForm(recalculateMatrix)){
      //if the fields equal zero or are empty display a message otherwise recalculate
      flag = checkValidChargeEntered();
      if(flag)  doRecalculateOrderAction(); else alert(messageToDisplay); //doSubmitBillingAction();
   }
}//end method changeRecalculateOrder()

/*
 called after recalculation to
 display new totals on the page and set hidden fields
*/
function displayRecalculation(order_amt, tax_amt, return_action,
		tax1_name, tax1_description, tax1_rate, tax1_amount,
		tax2_name, tax2_description, tax2_rate, tax2_amount,
		tax3_name, tax3_description, tax3_rate, tax3_amount,
		tax4_name, tax4_description, tax4_rate, tax4_amount,
		tax5_name, tax5_description, tax5_rate, tax5_amount) {

   //display the tax amount & set the hidden field to be passed back
   var tdTax = document.getElementById('tax_amount');
   document.getElementById('product_tax').value = stripCharsInBag(tax_amt,',');
   tdTax.innerText = "$" + tax_amt;

   //display the total amount & set the hidden field to be passed back
   var tdTotal = document.getElementById('total_charge_amt');
   document.getElementById('total').value = stripCharsInBag(order_amt,',');
   tdTotal.innerText = "$" +  order_amt;

   // set tax related hidden fields
   if (tax1_name != null) {
	   document.getElementById('tax1_name').value = tax1_name;
	   document.getElementById('tax1_description').value = tax1_description;
	   document.getElementById('tax1_rate').value = tax1_rate;
	   document.getElementById('tax1_amount').value = stripCharsInBag(tax1_amount,',');
   }

   if (tax2_name != null) {
	   document.getElementById('tax2_name').value = tax2_name;
	   document.getElementById('tax2_description').value = tax2_description;
	   document.getElementById('tax2_rate').value = tax2_rate;
	   document.getElementById('tax2_amount').value = stripCharsInBag(tax2_amount,',');
   }

   if (tax3_name != null) {
	   document.getElementById('tax3_name').value = tax3_name;
	   document.getElementById('tax3_description').value = tax3_description;
	   document.getElementById('tax3_rate').value = tax3_rate;
	   document.getElementById('tax3_amount').value = stripCharsInBag(tax3_amount,',');
   }

   if (tax4_name != null) {
	   document.getElementById('tax4_name').value = tax4_name;
	   document.getElementById('tax4_description').value = tax4_description;
	   document.getElementById('tax4_rate').value = tax4_rate;
	   document.getElementById('tax4_amount').value = stripCharsInBag(tax4_amount,',');
   }

   if (tax5_name != null) {
	   document.getElementById('tax5_name').value = tax5_name;
	   document.getElementById('tax5_description').value = tax5_description;
	   document.getElementById('tax5_rate').value = tax5_rate;
	   document.getElementById('tax5_amount').value = stripCharsInBag(tax5_amount,',');
   }

   hasBeenRecalculated = true;
  //check the return action if it = submit the the user clicked the submit button
  // and after performing the recalculation we call the doSubmitBilling() method again
  if(return_action == "submit"){
     submitted = true;
     completeBillingProcess();
  }


}//end method displayRecalculation(order_amt, tax_amt, return_action)


/*
 called when the status from submitBillingAction
 is 'declined'
*/
function invokeDeclinePopup(reason,message){
   /*
     we need to clear the return_action that way when we do a recalculate()
     the page will not automagically submit to the server.
   */
   document.getElementById('return_action').value = "";

   hideWaitMessage("mainContent", "wait");

  if( Trim(reason) == 'decline_credit' ){
     creditCardDecline();
    return;
  }else{
     alert(message);
     if( Trim(reason) == 'decline_gcc'  ){
         document.getElementById('giftCertRow').style.display = 'none';
         document.getElementById('action_type').value = "";
         document.getElementById('gcc_num').value = "";
         document.getElementById('amount_owed').value = "";
         document.getElementById('gcc_status').value = "";
         document.getElementById('gcc_amt').value = "";
         document.getElementById('total_owed').value = "";
     }
  }

}
/*
 display credit card decline
*/
function creditCardDecline(){
   var pay_type = document.getElementById('pay_type');
   var url_source = "ccDecline.do?" + getSecurityParams(false) + "&pay_type=" + pay_type.options[pay_type.selectedIndex].value;
   var modal_dim = "dialogWidth:765px; dialogHeight:345px; center:yes; status=0; help:no; scroll:no";

   var ret = showModalDialogIFrame(url_source, "", modal_dim);
      if(ret != null){
         if( ret['page_action'] == 'save' ){
            if( ret["type"] == "auth"){
               document.getElementById('auth_code').value = ret['auth_code'];
            }
            if( ret["type"] == "aafes"){
               document.getElementById('aafes_code').value = ret['aafes_code'];
               document.getElementById('auth_code').value = ret['auth_code'];
            }
           document.getElementById('manual_auth').value = 'Y';
           if( document.getElementById('billing_level').value == "cart" ){
              doGUIDRecordLock('PAYMENTS','retrieve',orderLevel,'addBillingForm');
           }else{
              doRecordLock('PAYMENTS','retrieve',orderLevel,'addBillingForm');
           }
         }
         if(ret["page_action"] == 'cancel' ){
            //return user to recip/order or shopping cart page or updatebilling
            doCreditCancelProcess();

         }
         if(ret["page_action"] == 'returntobilling' ){
            //do nothing and stay on current page.
         }
     }else{
         document.getElementById('manual_auth').value = 'N';
     }
}

/*
  Before returning to the shopping cart or
  recipient order page we must release the lock
*/
function doCreditCancelProcess(){
  //Before send the user back to the shopping cart we need to release the lock on the record
   showWaitMessage("mainContent","wait","Cancelling");
   doRecordLock('PAYMENTS','unlock',orderLevel,'addBillingForm');
   //complete the cancel process
   doCompleteCancelProcess();

}
/*
 Returns user to the shopping cart or the recipient order page when
 the cancel button on the credit card decline popup is executed
*/
function doCompleteCancelProcess(){

   // Defect 1240: PCI Compliance.  Clear the cc number field.
   document.getElementById('credit_card_num').value = "";

   /*
     use orderLevel variable to determine which page we cam from shopping cart or recipient order
   */
   var url = "";
   //go to shopping cart
   if( orderLevel == 'updateBilling' ){
      url = "http://" + siteName + applicationContext + "/customerOrderSearch.do?action=search" + "&in_order_number=" + document.getElementById('master_order_number').value;
   }
   //go to recipient order page
   if( orderLevel == 'updatePayment'){
      url = "http://" + siteName + applicationContext + "/customerOrderSearch.do?action=search&recipient_flag=Y&in_order_number=" + document.getElementById('external_order_number').value;
   }
   document.getElementById('addBillingForm').action = url;
   document.getElementById('addBillingForm').submit();
}

/*
 executes when the user clicks the cancel button
*/
function doCancelUpdateBilling(){
   if(hasFormChanged())
   {
      if(doExitPageAction(WARNING_MSG_4)){
         doCancelAction();
      }
   }else{
       doCancelAction();
   }
}
/*
 makes the requst to cancel and return
 the user to another page
*/
function doCancelAction(){
   // Defect 1240: PCI Compliance.  Clear the cc number field.
   document.getElementById('credit_card_num').value = "";

   document.getElementById('addBillingForm').action = 'http://' + siteName + applicationContext + '/updateBilling.do?';
   document.getElementById('page_action').value = 'search';
   document.getElementById('addBillingForm').submit();
}


/*
 method calls the gift certificate
 pop-up
*/
function redeemGcCoupon(){
   //set page_action=pop becuase the action is looking for something to be
   //there and we can not just send nothing.
   var url_source = "gcCoupon.do?page_action=pop" +
          getSecurityParams(false) + '&total_owed=' + document.getElementById('total').value;
   var modal_dim = "dialogWidth:765px; dialogHeight:345px; center:yes; status=0; help:no; scroll:no";



   var ret = showModalDialogIFrame(url_source, "", modal_dim);
    if( ret != null) {
      if(ret["status"] != null){

		     document.getElementById('action_type').value = ret["page_action"];
         document.getElementById('gcc_num').value = ret["gift_number"];
         document.getElementById('amount_owed').value = ret["amount_owed"];
         document.getElementById('gcc_status').value = ret["status"];
         document.getElementById('gcc_amt').value = ret["amount"];
         document.getElementById('total_owed').value = ret["net_owed"];
         document.getElementById('total').value = ret["net_owed"];
         document.getElementById('total_charge_amt').innerText = ret["net_owed_frmtd"];


         //if GC was selected then we can not allow the user to select this option again
         //check the status, if "valid" set flag to true that way the user can not pop the GC page again
         if(ret["status"] == 'valid' ){

            if(ret["amount_owed"] == "true" ){
               document.getElementById('pay_type').selectedIndex = 0;
               isGCSelected = true;
            }



    		    document.getElementById('merchandise_display').disabled = true;
 	    	    document.getElementById('add_on_display').disabled = true;
 			      document.getElementById('service_shipping_display').disabled = true;
 		     }

         displayGCInfo(ret["gift_number"],ret["amount"]);

        if( ret["status"] == 'valid' ){
            resetCreditCardLists('exp_date_month','exp_date_year','credit_card_num', false);
         }
      }
   }

}//end method redeemGcCoupon()

/*
 Displays the Gift Certificate Number, and Amount
 after the Gift Certificate popup is closed
*/
function displayGCInfo(gNumber, amt){
   //make sure we have values to display
   if(gNumber != null && amt != null){
      document.getElementById('giftCertRow').style.display = 'block';
      var display = document.getElementById('giftCertCell');
      display.innerText = 'Gift Certificate#: ' + gNumber + '      ' + 'Amount: ' + '$' + amt;
   }
}

/*
 Utility & Validation methods
*/
function setScrollingDivHeight(){
    var num;
    if(countOfOrderPayments == 0) {
       document.getElementById('messageContainer').style.height = "0px";
    }
}
/*
 Validate Payment drop down
 Visa, MasterCard, etc...
*/
function isPaymentTypeSelected(){
   var list = document.getElementById('pay_type');
  if(list.selectedIndex > 0 && list.options[list.selectedIndex].value != 'NC' &&
      list.options[list.selectedIndex].value != 'MS'){
     //set isPageValid to true becuase we are not checking the
     //Manager approval and Military Star fields
      isPageValid = true;
      return true;
  }
   if(list.options[list.selectedIndex].value == 'NC'){
      //if pay type is No Charge check for manager approval fields
     isPageValid = isMgrApproval();
      //return true becuase an item in the list has been selected
      //but the page will only submit if the form is valid and isPageValid equals true
      return true;
   }
   if(list.options[list.selectedIndex].value == 'MS' ){
        //if pay type is No Charge check for manager approval fields
    // isPageValid = isAAEFSValid();
      //return true becuase an item in the list has been selected
      //but the page will only submit if the form is valid and isPageValid equals true
      isPageValid = true;
      return true;
   }
  return false;
}


/*
 Validate the Month & Year Expiration date lists
*/
function isMonthYearSelected(){
   //retrieve a reference to the lists
   var paymentList = document.getElementById('pay_type');
   var ccMonthList = document.getElementById('exp_date_month');
   var ccYearList = document.getElementById('exp_date_year');
   //retrieve a collection of list options
   var paymentOptions = paymentList.options;
   var ccMonthListOptions = ccMonthList.options;
   var ccYearListOptions = ccYearList.options;

   var valid = false;

   if( paymentOptions[paymentList.selectedIndex].value == 'NC' ||
         paymentOptions[paymentList.selectedIndex].value == 'IN' )
   {
      if( ccMonthList.selectedIndex != 0 && ccYearList.selectedIndex != 0 ){
         //this should never happen lists are reset automagically when these values are selected
      }

   }else{
      if( ccMonthList.selectedIndex == 0 && ccYearList.selectedIndex == 0 ){
         //set error field and set display to pink for both fields
         ccMonthList.className = 'errorField';
         ccYearList.className = 'errorField';
         document.getElementById('exp_date_month_validation_message_cell').innerText = 'Required Field(s)';
         document.getElementById('exp_date_month_validation_message_row').style.display = 'block';
         valid = false;
      }
      if( ccMonthList.selectedIndex > 0 && ccYearList.selectedIndex == 0 ){
           //set year List
         ccYearList.className = 'errorField';
         ccMonthList.className = '';
         document.getElementById('exp_date_month_validation_message_cell').innerText = 'Required Field';
         document.getElementById('exp_date_month_validation_message_row').style.display = 'block';
         valid = false;
      }
      if(ccMonthList.selectedIndex == 0 && ccYearList.selectedIndex > 0 ){
         //set Month list
         ccMonthList.className = 'errorField';
         ccYearList.className = '';
         document.getElementById('exp_date_month_validation_message_cell').innerText = 'Required Field';
         document.getElementById('exp_date_month_validation_message_row').style.display = 'block';
         valid = false;
      }
      if( ccMonthList.selectedIndex != 0 && ccYearList.selectedIndex != 0){
          ccMonthList.className = '';
          ccYearList.className = '';
          document.getElementById('exp_date_month_validation_message_row').style.display = 'none';
          valid = true;

      }
   }
   return valid;
}

/*
 Validate credit card number
*/
function isValidCreditCard(){
   var ccField = document.getElementById('credit_card_num');
   var payType = document.getElementById('pay_type');
   var creditCardType;
   //if the credit card field has been updated
   if(ccField.value == ''){
      isCardUpdated = true;
      isOriginalCardUsed = false;
       /*
            if the credit card field is empty it's a required field
            otherwise if a value is supplied it will be an invalid format
            if the value does not match any of the credit card types
       */
      validationMatrix[1][1] = "Required Field";
   }
   /*
     It is possible for a user to copy and past a credit card number into the field
     this sadly will not trigger the event tied to the field. Thus, not updating
     the appropriate variables to determine if we are using the original credit card or not.
     Here we check the field to see if it's current value is equal to the original
   */
  /*Defect 12702. hasInputChanged(ccField) returns true even if the ccField is not changed. This is happening because ccField is set to empty by default
    * in the beginning and later it is being populated in addBilling.xsl based on few conditions.*/

   /*
   *  if( hasInputChanged(ccField) ){
      isOriginalCardUsed = false;
    }
    */
   /*cc_orig_number is the original value of the masked credit_card_number field and ccField is the value that is sent when a user clicks on Submit (B)illing button.
    */
    if(!(document.getElementById("cc_orig_number").value == ccField.value)){
 	   isOriginalCardUsed = false;
 	   }

    /*
      If we are here it means that the credit card has been updated
      we need to add  an N value to the is_orig_cc hidden field.
    */
   if(!isOriginalCardUsed){
      document.getElementById('is_orig_cc').value = 'N';
   }else{
      document.getElementById('is_orig_cc').value = 'Y';
   }

   /*
     The validation for Military Star credit card was pulled from utilities.jar
     which used the checkCreditCard(String ccType, String id) of the ValidateMembership class
     For this type of credit card we do not need to validate exp dates
   */
   if(isCardUpdated && !isOriginalCardUsed){
      if( Trim(payType.options[payType.selectedIndex].value) == 'MS' ){
         returnVal = (ccField.value.length == 16 && ccField.value.substr(0,4) == '6019') ? true : false;
         return returnVal; // return immediately as to not continue processing the rest of this method
      }
   }

   /*
    Validate Credit Card Field
    If the Credit Card has been modifed then we are no longer
    using the original credit card and we must validate the credit card
   */
   var returnVal = true;
   if( (payType.options[payType.selectedIndex].value != 'IN' ) &&
          (payType.options[payType.selectedIndex].value != 'NC')  &&
             payType.options[payType.selectedIndex].value != 'GC' &&
                payType.options[payType.selectedIndex].value != 'PC')
   {
      //if we are checking the credit card field
      //also check that the month and year are selected also
      isPageValid = isMonthYearSelected();
      //check length if card is Military Star
      if(isCardUpdated && !isOriginalCardUsed)
      {
         //find the appropriate credit card type to use
         for(var m = 0; m < ccNames.length; m++)
         {
            if(Trim(ccNames[m][0].toString()) == Trim(payType.options[payType.selectedIndex].value.toString()) )
            {
                   creditCardType = ccNames[m][1];
                   break;
            }
         }
            /*
               if the type does not exist in the array display a message
               other types (PC, NC...) shouldn't matter becuase we are checking
               for those types before we even get into this part of the method
            */
            if(creditCardType == null || creditCardType == "")
            {
               alert("Invalid Payment Type.");
               return;
            }
         //make sure the credit card type and number match
         if(!isCardMatch(creditCardType, ccField.value) )
         {
            returnVal = false;
            //if the credit card number does not match the payment type selected display the following message.
            validationMatrix[1][1] = "The credit card number is invalid. Please correct."
         }
      }
   }
  return returnVal;
}
/*
 Populate the Credit Card Lists
*/
function populateCreditCardInfo(pay_type, exp_date_month, exp_date_year, selectedCCType, selectedCCExp)
{
   var ccTypeObj = document.getElementById(pay_type); //retrieve the list obj
   if(!isWhitespace(selectedCCType) && selectedCCType.toString() != 'MS'){
     for(var c = 0; c < ccTypeObj.length; c++){
        if(ccTypeObj.options[c].value.toString() == selectedCCType.toString()){
           ccTypeObj.selectedIndex = c; //display the option in the list
           ccTypeObj.options[c].defaultSelected = true;
           break;
        }
     }
   }
   if (selectedCCType.toString() != 'MS') {
	   populateExpDate(exp_date_year, exp_date_month, selectedCCExp);
   }
}
/*
 Populate Exp Month & Year lists
*/
function populateExpDate(exp_date_year,exp_date_month,selectedCCExp){
 if(!isWhitespace(selectedCCExp)){
      var ccMonthObj = document.getElementById(exp_date_month); //get month exp list
      var ccYearObj = document.getElementById(exp_date_year);  //get year exp list
      var selectedCCExpMonth;
      var selectedCCExpYear;

       //if the exp date has a '/' in the string test for this.. otherwise extract the necessary parts
      if(selectedCCExp.indexOf("/") != -1 )
      {
        var expArr = new String(selectedCCExp).split("/");

        //get the month
        selectedCCExpMonth = expArr[0].toString();

        //calling code can pass 2013 or 13.  Get the year
        var centuryYear = expArr[1].toString();
        if (centuryYear.length == 4)
        {
          selectedCCExpYear = centuryYear.substr(2,2);
        }
        else
        {
          selectedCCExpYear = centuryYear;
        }

      }
      else
      {
        //get the month
        selectedCCExpMonth = selectedCCExp.substr(0,2);

        //calling code can pass 2013 or 13.  Get the year
        if (selectedCCExp.length == 6)
        {
          selectedCCExpYear = selectedCCExp.substr(4,2);
        }
        else
        {
          selectedCCExpYear = selectedCCExp.substr(2,2);
        }
      }


      for(var e = 0; e < ccMonthObj.length; e++){
         if(ccMonthObj.options[e].value == selectedCCExpMonth){
            ccMonthObj.selectedIndex = e;
            ccMonthObj.options[e].defaultSelected = true;
            break;
         }
      }
      for(var y = 0; y < ccYearObj.length; y++){
         if(ccYearObj.options[y].value == selectedCCExpYear){
            ccYearObj.selectedIndex = y;
            ccYearObj.options[y].defaultSelected = true;
            break;
         }
      }
   }
}
/*
 The Credit Card Exp year list must always
 hold 15 years into the future. (including the current year)
*/
function buildCCYearList(list){
   var thisYear = new Date();
   var theList = document.getElementById(list);
   var year = thisYear.getYear().toString();
   theList.options.length = 0; //clear the list
   theList.add(new Option("","",false,false));
   for(var i = 0; i <= 14; i++){ //add 15 years to current year create new options in list
      theList.add(new Option((thisYear.getYear() + i), (thisYear.getYear() + i).toString().substr(2,2),false,false));
   }
}
/*
 this function can be used to allow
 only numeric key presses in text fields
*/
function doOnlyNumeric(){
    var code = event.keyCode;

   if(code != 46 &&(code < 48 || code > 57) && code != 44){ //44 = (,) and 46 = (.)
      return false;
   }

   //if the user has updated the credit card info at all set
  //this flag to false.
   if( event.srcElement.name == 'credit_card_num'){
      //if info has been updated we are no longer using the original cc information
      isOriginalCardUsed = false;
       //if a number was entered the user is attempting to update
       //the credit card inforamtion, set this flag for use in the isValidCreditCard() method
       isCardUpdated = true;
   }
   return true;
}
/*
 The following 3 methods could probably be combined to one generic method
*/

/*
 Validate the Merchandise field
*/
function isMerchValid(){

   var merchField = document.getElementById('merchandise_display');

   var newValue = '';
   var reg = /(\d){0,}\.\d\d$/g;
   var toReturn = true;
   var bag = ",.";


   if(!merchField.disabled && merchField.value != ''){
      //clean out any user entered formatting characters
      var striped = stripCharsInBag(merchField.value , bag);

      if(striped.length > 7){
        toReturn = false;
      }else{
         toReturn = ( reg.test(moneyFormat(striped))  ) ? true:false;
         if(toReturn){
            newValue = moneyFormat(striped);
            document.getElementById('merchandise').value = stripCharsInBag(newValue,",");//take out comma in order to return to the server
            document.getElementById('merchandise_display').value = newValue;
         }
      }//end if(striped.length > 6)
   }//end   if(!merchField.disabled && merchField.value != '')

   if(toReturn){
      if(parseFloat(newValue) > 0.00) {
         isMerchValidValue = true;
      }
   }//end if(toReturn)

   return toReturn;
}
/*
  validate the Add On field
*/
function isAddOnValid(){

   var addOnField = document.getElementById('add_on_display');

   var newValue = '';
   var reg = /(\d){0,}\.\d\d$/g;
   var toReturn = true;
   var bag = ",.";

   if(!addOnField.disabled && addOnField.value != ''){
      //clean out any user entered formatting characters
      var striped = stripCharsInBag(addOnField.value , bag);

      if(striped.length > 10){
        toReturn = false;
      }else{
         toReturn = ( reg.test(moneyFormat(striped))  ) ? true:false;
         if(toReturn){
            newValue = moneyFormat(striped);
            document.getElementById('add_on').value = stripCharsInBag(newValue,",");//take out comma in order to return to the server
            document.getElementById('add_on_display').value = newValue;
         }
      }//end if(striped.length > 6)
   }//end   if(!addOnField.disabled && addOnField.value != '')
    if(toReturn){
      if(parseFloat(newValue) > 0.00) {
         isAddonValidValue = true;
      }
   }
   return toReturn;

}
/*
 validate the service/shipping fee field
*/
function isServShipValid(){

   var servShipField = document.getElementById('service_shipping_display');

   var newValue = '';
   var reg = /(\d){0,}\.\d\d$/g;
   var toReturn = true;
   var bag = ",.";

   if(!servShipField.disabled && servShipField.value != ''){
      //clean out any user entered formatting characters
      var striped = stripCharsInBag(servShipField.value , bag);

      if(striped.length > 10){
        toReturn = false;
      }else{
         toReturn = ( reg.test(moneyFormat(striped))  ) ? true:false;
         if(toReturn){
            newValue = moneyFormat(striped);
            document.getElementById('service_shipping').value = stripCharsInBag(newValue,","); //take out comma in order to return to the server
            document.getElementById('service_shipping_display').value = newValue;
         }
      }//end if(striped.length > 6)
   }//end   if(!servShipField.disabled && servShipField.value != '')
   if(toReturn){
      if(parseFloat(newValue) > 0.00) {
         isServShipValidValue = true;
      }
   }
   return toReturn;
}


/*
 return a U.S. Dollar formatted value
*/
function moneyFormat(frmt){
   //empty variable to hold newly formatted monetary value
   var newValue = "";
   //variables to hold the different monetary places
   var dec = "";
   var dol = "";
   var thou = "";
   var mill = "";
   var comma = 0;

    dec = frmt.substr(frmt.length - 2, frmt.length);//get the decimal portion of a string meaning the last two digits ( .99 )
    dol = frmt.substr(0, frmt.length - 2); //get the dollar portion of the string everything on the left side of a decimal

   //check dol for length, if it's 7 or larger. Do this in order to figure out were to put the commas
   if(dol.length >= 7 ){
      comma = (dol.length == 7 ) ? 1:2 ;
      mill = dol.substr(0,comma); //[99]999 999 99
      thou = dol.substr(mill.length, 3);//99 [999] 999.99
      dol =  dol.substr(thou.length,3); //99 999 [999].99
      //add a decimal to the dollar amount
      newValue = newValue.concat(mill, ',', thou, ',', dol,'.',dec);  //create new value 99,999,999.99 || 9,999,999.99
   }else if (dol.length >= 4 && dol.length <= 6){
      thou = dol.substr(0, dol.length - 3);
      dol =  dol.substr(thou.length,3);
      newValue = newValue.concat( thou, ',', dol,'.',dec);
   }else{
     dec = frmt.substr(frmt.length - 2, frmt.length);
     dol = frmt.substr(0, frmt.length - 2);
     newValue = newValue.concat(dol,'.',dec);
   }
  return newValue;
}




/*
 If pay type is Military Star a AAEFS code must
 be entered before we can submit the page.
*/
function isAAEFSValid(){
   var valid = false;
   var input = document.getElementById('aafes_code_entered');

   var cell = document.getElementById('pay_type_validation_message_cell');
   var row = document.getElementById('pay_type_validation_message_row');

   if(input.value == ''){
      cell.innerText = "Required Field";
      input.className = "errorField";
      row.style.display = 'block';
     return valid;
   }
   if(input.value != ""){
     cell.innerText = "";
     input.className = "";
     row.style.display = "";
     valid = true;
   }
 return valid;
}
/*
 if pay type selected is NC
 validate the manager approval code & password
*/
function isMgrApproval(){
   var valid = false;

   var mgrApproval = document.getElementById('mgr_appr_code');
   var mgrPassword = document.getElementById('password');
   var payType = document.getElementById('pay_type');

   var cell = document.getElementById('pay_type_validation_message_cell');
   var row = document.getElementById('pay_type_validation_message_row');

   var both = ( (mgrApproval.value == '' && mgrPassword.value == '') ) ? true : false;

   if(both){
      cell.innerText = 'Required Field(s)';
      mgrApproval.className = "errorField";
      mgrPassword.className = "errorField";
      row.style.display = 'block';
     return valid;
   }
   if(!both && mgrApproval.value == ''){
      cell.innerText = 'Required Field';
      mgrApproval.className = 'errorField';
      mgrPassword.className = '';
      row.style.display = 'block';
       return valid;
   }
   if(!both && mgrPassword.value == ''){

      cell.innerText = 'Required Field';
      mgrApproval.className = '';
      mgrPassword.className = 'errorField';
      row.style.display = 'block';
       return valid;
   }
   if( mgrPassword.value != '' && mgrApproval.value != '' ){
      cell.innerText = '';
      mgrApproval.className = '';
      mgrPassword.className = '';
      row.style.display = 'none';
      valid = true;
   }
   isPageValid = valid;
  return valid;
}

/*

*/
function setCreditCardMaxLength(){

   var list = document.getElementById('pay_type');

   if(list.options[list.selectedIndex].value != 'IN' &&
         list.options[list.selectedIndex].value != 'NC' )
   {
     // document.getElementById('credit_card').setAttribute('maxlength',);
      var option = list.options[list.selectedIndex].value;

      for( var i = 0; i < ccLengths.length; i++){
         if( Trim(option) == Trim(ccLengths[i][0]) ){
             document.getElementById('credit_card').setAttribute('maxlength',ccLengths[i][1]);
             alert(document.getElementById('credit_card').maxlength);
         }
      }

   }

}
/*
 Disables/Enables Credit Card fields based on
 Payment Type selections
*/
function resetCreditCardLists(selectOne, selectTwo, ccText, setFlag){

   //clear the page of any previous errors (does not include mo/year expiration)
   validateFormResetPrevErrors();
   //clear year/month expiration error fields
   validateFormResetExpMonthYearErrors();
   //clear aafes, manager approval code and manager approval id error fields
   validateFormResetApprovalFields();
   //setCreditCardMaxLength();

   //hide/display NC/MS fields
   displayNoChrgMilitaryFields();


   var disable = false;
   var selectMonth =  document.getElementById(selectOne);
   var selectYear = document.getElementById(selectTwo);
   var payment = document.getElementById('pay_type');
  // var eve = (event.srcElement != null ) ? event.srcElement : null;
   var eve = payment.options[payment.selectedIndex].value.toString();
   //if(eve != null){
     disable = (eve == 'IN' || eve == 'NC' || eve == 'GC' || eve == 'PC') ?  true:false;
   //}
   if(setFlag != null && setFlag != '' ) disable = setFlag;

   if(disable){
      selectMonth.selectedIndex = 0;
      selectMonth.disabled = disable;
      selectYear.selectedIndex = 0;
      selectYear.disabled = disable;
      document.getElementById(ccText).value = '';

   }else{
      selectMonth.disabled = disable;
      selectYear.disabled = disable;
      document.getElementById(ccText).disabled = disable;
      if(setFlag == false){
         document.getElementById('pay_type').selectedIndex = 0;
      }
   }
   //if the user already received a status of valid for a Gift Certificate do
   //not allow them to select GC again
   if(isGCSelected && eve == 'GC') {
      alert("Only one gift certificate is allowed per charge.");
      document.getElementById('pay_type').selectedIndex = 0;
   }


}

/*
 These fields could not use the generic
 validateForm() method from the util.js file
 these had to be dealt with seperately
*/
function validateFormResetExpMonthYearErrors(){
   document.getElementById('exp_date_month_validation_message_row').style.display = 'none';
   document.getElementById('exp_date_year_validation_message_row').style.display = 'none';
   document.getElementById('exp_date_year').className = '';
   document.getElementById('exp_date_month').className = '';
}

/*
 These fields could not use the generic
 validateForm() method from the util.js file
 these had to be dealt with seperately
*/
function validateFormResetApprovalFields(){
   var row = document.getElementById('pay_type_validation_message_row');
   var appr = document.getElementById('mgr_appr_code');
   var id = document.getElementById('password');
   //aafes code not needed on addBill page
   //var aaefs = document.getElementById('aafes_code_entered');

   if(row.style.display == 'block' || row.style.display == 'inline' ){
      appr.className = '';
      id.className = '';
      aaefs.className = '';
      row.style.display = 'none';
   }
}

/*
  hide/display required fields for
  Military Star & No Charge payment types
*/
function displayNoChrgMilitaryFields(){
   var payType = document.getElementById('pay_type');
   //This has been commented out and is not needed for MS on the add bill page
   if(payType.options[payType.selectedIndex].value == 'MS'){
     // document.getElementById('aafesField').style.display = 'inline';
      //document.getElementById('noChargeField').style.display = 'none';
   }
   if (payType.options[payType.selectedIndex].value == 'NC')
   {
      document.getElementById('noChargeField').style.display = 'inline';
      document.getElementById('aafesField').style.display = 'none';
   }
   if(payType.options[payType.selectedIndex].value != 'NC' &&
         payType.options[payType.selectedIndex].value != 'MS')
   {
      document.getElementById('aafesField').style.display = 'none';
      document.getElementById('noChargeField').style.display = 'none';
   }
}

/*
 Called from the applyBilling.xsl or
 recalculateOrder.xsl if there are any
 errors
*/
function doCallErrorPage(){
   var url = "Error.do?";
   document.getElementById('addBillingForm').action = url;
   document.getElementById('addBillingForm').submit();
}



/*
 Loads text into an XML Document
*/
function getXMLobjXML(xmlToLoad)
{
	var xml = new ActiveXObject("Msxml2.FreeThreadedDOMDocument");
	xml.async = false;
	xml.resolveExternals = false;
	try
	{
		xml.loadXML(xmlToLoad);
		return(xml)
	}
	catch(e)
	{
		throw(xml.parseError.reason)
	}
}