/*
 * The followig javascript is used for the C.O.M updateProductCategories.xsl
 * (This javascript along with the updateProductCategory.xsl file were copied
 * over from the order_scrub project and chagned to work with C.O.M.
 *  Global variables
 */
    var subcategoryHTML = new Array();
    var subcategoryHTMLMapping = new Array();
    var c_counter = 0;
    var catwinopen = false;
    var PAGE_URL = "";
    var CANCEL_ACTION       = "cancel_update";
    var CANCEL_UPDATE_PAGE  = 'cancelUpdateOrder.do?';
/*
 *  Initialization
 */
function init(){
  document.oncontextmenu = contextMenuHandler;
  document.onkeydown = upcBackKeyHandler;
  setScrollingDivHeight();
  addDefaultListeners();
  window.onresize = setScrollingDivHeight;
}

/*
 * The categories/products list are contained in a scrolling div element
 * this function will set the height of this element
 */
function setScrollingDivHeight(){
  var productListingDiv = document.getElementById("productListing");
  productListingDiv.style.height = document.body.clientHeight - productListingDiv.getBoundingClientRect().top ;
  
}

/*
 * Test the document for key down events
 */
document.onkeydown = function() {
  if (window.event.keyCode == 13) {
	  return false;
  }
    
}


/*
 *  Hightlights the Occassions/Products layers
 */
function doCategoryOverEvent(selection){
  selection.className = "EditProductCellHighlight";
}

/*
 *  Hightlights the Occassions/Products layers
 */
function doCategoryOutEvent(selection){
  selection.className = selection.className.defaultValue;
}

/*
 *  Actions
 */
function doCloseAction(){
  window.close();
}

function doExitAction(){
  window.returnValue = new Array("_EXIT_");
  doCloseAction();
}

function doSearchAction(event){
  if (window.event && window.event.keyCode) {
   if (window.event.keyCode != 13)
     return false;
  }

  var searchBox = document.getElementById('searchSelect');
  var searchType = searchBox.options[searchBox.selectedIndex].value;

  var search_value = document.getElementById('search_input').value;
  var param_type = '';

  if(searchType != "simpleproductsearch"){
     searchType = 'keywordsearch';
     param_type = "&searchKeyword=";
  }else{
    param_type = "&product_id=";
  }
   var url = 'search.do?action_type=' + searchType +
          param_type + document.getElementById('search_input').value ;

  performAction(url);
}



function doCategorySearchAction(index){
  document.getElementById('category_index').value = index;
  var url = "loadProductList.do?";
  performAction(url);
}


/*
 Function handles the Cancle Update button
*/
function doCancelUpdateAction(){
  if ( hasFormChanged() ) {
    if ( doExitPageAction(WARNING_MSG_4) ) {
      baseDoCancelUpdateAction("upcForm");
    }
  }
  else {
    baseDoCancelUpdateAction("upcForm");
  }
}




/*
 * Handle the Custom Order button event
 */
function doCustomOrder(){
  var url = 'loadCustomOrder.do';
  performAction(url);
}

 /*
  * Submit the page
  */
function performAction(url){
  PAGE_URL = url;
  showWaitMessage("mainContent","wait","Please wait searching");
  doRecordLock('MODIFY_ORDER','check',"", "upcForm");
}

/*
 forward user to next page
*/
function doContinueProcess(){
    document.getElementById('upcForm').action = PAGE_URL;
    document.getElementById('upcForm').submit();
}
/*
 continue to next process
*/
function checkAndForward(forwardAction){
    doContinueProcess();
}

 /*
  *  Subcat box loading, displaying, and dragging
  */
var isWinMoving = false;
var curX = 0;
var curY = 0;

function startSubCategoryHTML(_categoryName){
  subcategoryHTML[c_counter] = "";
  subcategoryHTMLMapping[c_counter] = _categoryName;
  subcategoryHTML[c_counter] = subcategoryHTML[c_counter] + '<span class="popupHeader">' + _categoryName + ' List</span><br>';
}

function addSubCategoryHTML(_name, _index){
  subcategoryHTML[c_counter] = subcategoryHTML[c_counter] + '<table cellpadding="0" cellspacing="0"><tr><td tabindex="6" onkeypress="javascript:doCategorySearchAction(' + _index + ');" onclick="javascript:doCategorySearchAction(' + _index + ');" ><u>' + _name + '</u></td></tr></table>';
}

function endSubCategoryHTML(){
  c_counter++;
}

function doShowCategory(sender){
   cen = document.body.clientWidth/2 - 155;
   subcategorywin = document.getElementById("subcategory").style;
   scroll_top = document.body.scrollTop;

   for (var i = 0; i < c_counter; i++){
     if (sender.id == subcategoryHTMLMapping[i]){
       document.getElementById("subcategorydetail").innerHTML = subcategoryHTML[i];
       break;
     }
   }

   if (curX == 0 && curX == 0){
      subcategorywin.top = scroll_top + document.body.clientHeight/2 - 155;
      subcategorywin.left = document.body.clientWidth/2 - 200/2;
   }
   else{
      subcategorywin.top = scroll_top + curY;
      subcategorywin.left = curX;
   }

     subcategorywin.height = 200;
     subcategorywin.display = "block";
     hideShowCovered(subcategory);
     catwinopen = true;
}

function getMousePosition(e){
   if (isWinMoving){
     _x = event.clientX;
     _y = event.clientY;
     curX = subcategorywin.left = _x - 15;
     curY = subcategorywin.top = _y - 10;
     hideShowCovered(subcategory);
   }
  return true;
}

function moveBtn_onmouseclick(button){
   button.style.cursor = "move";
   isWinMoving = !isWinMoving;

   if (isWinMoving)
     document.attachEvent("onmousemove", getMousePosition);
   else
     document.detachEvent("onmousemove", getMousePosition);
}

function dismisssubcatbox(){
  catwinopen = false;
  subcategorywin.display = "none";
  hideShowCovered(subcategory);
}

/*
 *  Occasion, Product, Price column changes
 */
function showByHelper(occasionFlag, productFlag){
  setOccasionColumn(occasionFlag);
  setProductColumn(productFlag);

/**
*  Not currently using price point category search functionality.
*
*  setPriceColumn(priceFlag);
*/
}

function doShowByProduct(){
  showByHelper(false, true);
}
function setProductColumn(selected){
  document.getElementById("productCol").className = (selected) ? "EditProductColHeaderSelected" : "EditProductColHeaderDeselect";
  document.getElementById("productLayer").className = (selected) ? "EditProductCellSelected" : "EditProductCellDeselect";
}

function doShowByOccasion(){
  showByHelper(true, false);
}

function setOccasionColumn(selected)  {
   document.getElementById("occasionCol").className = (selected) ? "EditProductColHeaderSelected" : "EditProductColHeaderDeselect";
   document.getElementById("occasionLayer").className = (selected) ? "EditProductCellSelected" : "EditProductCellDeselect";
}

/*
 *  Disables the various navigation keyboard commands.
 */
function upcBackKeyHandler() {
   // backspace
   if (window.event && window.event.keyCode == 8){
      var formElement = false;

      for(i = 0; i < document.forms['upcForm'].elements.length; i++){
         if(document.forms['upcForm'].elements[i].name == document.activeElement.name){
            formElement = true;
            break;
         }
      }

      if(formElement == false || (document.activeElement.type != "text" && document.activeElement.type != "textarea" && document.activeElement.type != "password")){
         window.event.cancelBubble = true;
         window.event.returnValue = false;
         return false;
      }
   }

   // F5 and F11
   if (window.event.keyCode == 122 || window.event.keyCode == 116){
      window.event.keyCode = 0;
      event.returnValue = false;
      return false;
   }

   // Alt + Left Arrow
   if(window.event.altLeft){
      window.event.returnValue = false;
      return false;
   }

   // Alt + Right Arrow
   if(window.event.altKey){
      window.event.returnValue = false;
      return false;
   }
}

/*
 * Handle the Update Delivery Information button event
 */
function doUpdateDeliveryInformation(){
  var url = 'loadDeliveryInfo.do';
  performAction(url);
}

/*
 * Handle the Update Product Category button event
 */
function doUpdateProductCategory(){
  var url = 'loadProductCategory.do';
  performAction(url);
}

/*******************************************************************************
*
*    performAction()
*
*******************************************************************************/
function performAction(url){
    /*
      Display a message to the user called Processing...
      this allows the user to know that the system is working
      and not stalled out.
      scroll(0,0) returns the window to the top position since
      all(most) buttons are located at the bottom of a scrolled window
    */
    scroll(0,0);
    showWaitMessage("mainContent", "wait", "Processing");

     document.forms[0].action = url;
     document.forms[0].submit();
}