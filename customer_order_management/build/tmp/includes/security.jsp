<%@ taglib uri = "http://java.sun.com/jstl/core" prefix = "c"%>
<%
  String securitytoken = request.getParameter("securitytoken");
  String context = request.getParameter("context");
  String adminAction = request.getParameter("adminAction");
  String urlString="queueBatchStatus.do?action=generateExcel&securitytoken="+securitytoken+"&context="+context+"&adminAction="+adminAction;
%>
<c:set var="query" scope="request"><%=urlString%></c:set>

<input type="hidden" name="securitytoken" id="securitytoken" value="<%=securitytoken%>">
<input type="hidden" name="context" id="context" value="<%=context%>">
<input type="hidden" name="adminAction" id="adminAction" value="<%=adminAction%>">