<%@ taglib uri="http://displaytag.sf.net" prefix="display" %> 
<%@ taglib uri = "http://java.sun.com/jstl/core" prefix="c"%>
<%@ page import="com.ftd.messaging.form.QueueMessageProcessingForm" %>
<jsp:include page="/includes/header.jsp" flush="true" />
<% 
QueueMessageProcessingForm qForm = (QueueMessageProcessingForm)request.getAttribute("QueueMessageProcessingForm");
int pageSize = qForm.getPageSize();
%> 

<form name="status" method="post" action="queueBatchStatus.do?action=loadData">
<jsp:include page="/includes/security.jsp" flush="true" />
<div id="content" class="content">
    <div class="buttonDiv">
        <table align="center" width="90%" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td valign="top" align="left" width="50%">
            <c:if test='${requestScope.QueueMessageProcessingForm.queueDeleteManager}'>
              <select name="user">
                  <c:choose>
                      <c:when test='${requestScope.QueueMessageProcessingForm.users == null}'>
                          <option value="None" selected >None</option>
                      </c:when>
                      <c:otherwise>
                          <c:forEach items="${requestScope.QueueMessageProcessingForm.users}" var="user">  
                              <option value="<c:out value='${user}'/>" <c:if test='${user==requestScope.QueueMessageProcessingForm.selectedUser}'>selected</c:if>><c:out value='${user}'/></option>   
                          </c:forEach>         
                      </c:otherwise>
                  </c:choose>
              </select> 
            </c:if>
            &nbsp;&nbsp;  
            <button id="refreshButton"  class="blueButton" tabindex="1" onclick="javascript:doRefresh();">Refresh</button>
          </td>
          <td width="50%" align="right">
            <button id="backButton"     class="blueButton" tabindex="2" onclick="javascript:goBackToDeleteAction();">Batch Queue Delete</button>
            <button id="mainMenuButton" class="blueButton" tabindex="3" onclick="javascript:goBackToCustomerServiceMenuAction();">Main Menu</button>
          </td>
        </tr>
      </table>
    </div>
    <div class="dataListDiv">
        <display:table export="true"  id="data" name="requestScope.QueueMessageProcessingForm.headers" requestURI="/queueBatchStatus.do" pagesize="<%=pageSize%>"  class="display sorttable altstripe sort01">
        <display:column property="createdBy" title="User" />             
        <display:column property="batchName" title="Batch" />             
        <display:column property="progress" title="Progress" />             
        <display:column property="totalSuccess" title="Total Successes" />             
        <display:column property="totalFailure" title="Total Failures" />         
        <display:column property="totalDuplicate" title="Total Duplicates" />         
        <display:column title="Link to Results Spreadsheet" >
            <span class="excelLink"><a href='<c:out value="${requestScope.query}&queueDeleteHeaderId=${data.queueDeleteHeaderId}"/>'><c:out value="${data.linkToDisplay}"/></a></span>
        </display:column> 
        </display:table>     
    </div>
</div>

<div id="waitDiv" style="display:none">
   <table id="waitTable" class="mainTable" width="98%" height="100%" align="center" cellpadding="2" cellspacing="1" >
      <tr>
        <td width="100%">
          <table class="innerTable" width="100%" height="100%" cellpadding="30" cellspacing="1">
           <tr>
              <td>
                 <table width="100%" cellspacing="0" cellpadding="0" border="0">
                     <tr>
                       <td id="waitMessage" align="right" width="50%" class="waitMessage">Processing please wait.</td>
                       <td id="waitTD"  width="50%" class="waitMessage"></td>
                     </tr>
                 </table>
               </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
</div>

</form>
<jsp:include page="/includes/footer.jsp" flush="true" />
