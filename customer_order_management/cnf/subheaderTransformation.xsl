<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" omit-xml-declaration="yes"/>
<xsl:variable name="product" select="root/PD_ORIG_PRODUCTS/PD_ORIG_PRODUCT"/>
<xsl:variable name="order" select="root/PD_ORDER_PRODUCTS/PD_ORDER_PRODUCT"/>
<xsl:template match="/root">
<subheader>
<subheader_info>
  <sh_customer_first_name><xsl:value-of select="$product/customer_first_name"/></sh_customer_first_name>
  <sh_customer_last_name><xsl:value-of select="$product/customer_last_name"/></sh_customer_last_name>
  <sh_external_order_number><xsl:value-of select="$product/external_order_number"/></sh_external_order_number>
  <sh_product_id><xsl:value-of select="$product/product_id"/></sh_product_id>
  <sh_product_name><xsl:value-of select="$product/product_name"/></sh_product_name>
  <sh_short_description><xsl:value-of select="$product/short_description"/></sh_short_description>
  <sh_color1_description><xsl:value-of select="$product/color1_description"/></sh_color1_description>
  <sh_color2_description><xsl:value-of select="$product/color2_description"/></sh_color2_description>
  <sh_product_amount><xsl:value-of select="$product/product_amount"/></sh_product_amount>
  <sh_discount_amount><xsl:value-of select="$product/discount_amount"/></sh_discount_amount>
  <sh_tax><xsl:value-of select="$product/tax"/></sh_tax>
  <sh_addon_count><xsl:value-of select="$product/addon_count"/></sh_addon_count>
  <sh_miles_points><xsl:value-of select="$product/miles_points"/></sh_miles_points>
  <sh_miles_points_posted><xsl:value-of select="$product/miles_points_posted"/></sh_miles_points_posted>
  <sh_discount_reward_type><xsl:value-of select="$product/discount_reward_type"/></sh_discount_reward_type>
  <sh_add_on_amount><xsl:value-of select="$product/add_on_amount"/></sh_add_on_amount>
  <sh_delivery_date><xsl:value-of select="$product/delivery_date"/></sh_delivery_date>
  <sh_delivery_date_range_end><xsl:value-of select="$product/delivery_date_range_end"/></sh_delivery_date_range_end>
  <sh_ship_method_desc><xsl:value-of select="$product/ship_method_desc"/></sh_ship_method_desc>
</subheader_info>
<subheader_addons>
  <xsl:for-each select="PD_ORIG_ADD_ONS/PD_ORIG_ADD_ON">
  <subheader_addon>
    <sh_add_on_description><xsl:value-of select="description" disable-output-escaping="yes"/></sh_add_on_description>
    <sh_add_on_quantity><xsl:value-of select="add_on_quantity"/></sh_add_on_quantity>
    <sh_add_on_price><xsl:value-of select="price"/></sh_add_on_price>
    <sh_add_on_code><xsl:value-of select="add_on_code"/></sh_add_on_code>
    <sh_add_on_type_description><xsl:value-of select="add_on_type_description"/></sh_add_on_type_description>
  </subheader_addon>
  </xsl:for-each>
</subheader_addons>
<subheader_taxes>
  <xsl:for-each select="PD_ORDER_TAXES/PD_ORDER_TAX">
  <subheader_tax>
    <sh_tax_description><xsl:value-of select="tax_description"/></sh_tax_description>
    <sh_tax_amount><xsl:value-of select="tax_amount"/></sh_tax_amount>
    <sh_display_order><xsl:value-of select="display_order"/></sh_display_order>
  </subheader_tax>
  </xsl:for-each>
</subheader_taxes>
</subheader>
</xsl:template>
</xsl:stylesheet>