package com.ftd.mo.bo;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.Closure;
import org.apache.commons.collections.Transformer;
import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.dao.ServicesProgramDAO;
import com.ftd.customerordermanagement.util.BasePageBuilder;
import com.ftd.customerordermanagement.util.functors.DateFormatTransformer;
import com.ftd.customerordermanagement.util.functors.NodeUpdateClosure;
import com.ftd.customerordermanagement.util.functors.PhoneFormatTransformer;
import com.ftd.customerordermanagement.util.functors.ResultSetToXmlTransformer;
import com.ftd.ftdutilities.FieldUtils;
import com.ftd.mo.dao.UpdateOrderDAO;
import com.ftd.mo.util.BreadcrumbUtil;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.order.RecalculateOrderBO;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.RecipientAddressesVO;
import com.ftd.osp.utilities.order.vo.RecipientsVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.security.SecurityManager;
import com.ftd.security.cache.vo.UserInfo;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.AccountProgramMasterVO;

/**
 * LoadDeliveryConfirmationBO builds the Document needed to display Delivery Confirmation information.
 *
 * @author Mark Moon, Software Architects Inc.
 */
public class LoadDeliveryConfirmationBO {

	
  private static final Logger logger  = new Logger( LoadDeliveryConfirmationBO.class.getName() );
  private static final String ROOT = "root";
  private static final String PRODUCTS_XPATH = "UPDATED_PRODUCTS/UPDATED_PRODUCT";
  private static final String PRODUCT_AMOUNT = ROOT + "/" + PRODUCTS_XPATH+"/product_amount/text()";
  private static final String ADD_ON_AMOUNT = ROOT + "/" + PRODUCTS_XPATH+"/add_on_amount/text()";
  private static final String SERVICE_FEE = ROOT + "/" + PRODUCTS_XPATH+"/service_fee/text()";
  private static final String SHIPPING_FEE = ROOT + "/" + PRODUCTS_XPATH+"/shipping_fee/text()";
  private static final String SHIPPING_TAX = ROOT + "/" + PRODUCTS_XPATH+"/shipping_tax/text()";
  private static final String TAX = ROOT + "/" + PRODUCTS_XPATH+"/tax/text()";
  private static final String DISCOUNT_AMOUNT = ROOT + "/" + PRODUCTS_XPATH+"/discount_amount/text()";
  private static final String DELIVERY_DATE = ROOT + "/" + PRODUCTS_XPATH+"/delivery_date/text()";
  private static final String DELIVERY_DATE_RANGE_END = ROOT + "/" + PRODUCTS_XPATH+"/delivery_date_range_end/text()";
  private static final String RECIPIENT_PHONE_NUMBER = ROOT + "/" + PRODUCTS_XPATH+"/recipient_phone_number/text()";
  private static final String RECIPIENT_CITY = ROOT + "/" + PRODUCTS_XPATH+"/recipient_city/text()";
  private static final String RECIPIENT_STATE = ROOT + "/" + PRODUCTS_XPATH+"/recipient_state/text()";
  private static final String RECIPIENT_ZIP_CODE = ROOT + "/" + PRODUCTS_XPATH+"/recipient_zip_code/text()";
  private static final String RECIPIENT_COUNTRY = ROOT + "/" + PRODUCTS_XPATH+"/recipient_country/text()";
  private static final String SERVICE_FEE_SAVED = ROOT + "/" + PRODUCTS_XPATH+"/service_fee_saved/text()";    
  private static final String SHIPPING_FEE_SAVED = ROOT + "/" + PRODUCTS_XPATH+"/shipping_fee_saved/text()";


  private static final String IN_FORMAT = "yyyy-MM-dd hh:mm:ss";
  private static final String OUT_FORMAT = "MM/dd/yyyy";

  private String messageId;
  private String imageLocation;
  private String csrId;
  private String sessionId;
  private String orderDetailId;
  private String buyerName;
  private String categoryIndex;
  private String companyId;
  private String customerId;
  private String occasionText;
  private String origProductAmount;
  private String origProductId;
  private String originId;
  private String pageNumber;
  private String pricePointId;
  private String shipMethod;
  private String requestedDeliveryDateRangeEndString;
  private String requestedDeliveryDateString;
  private BigDecimal orderTotal;
  private Document document;
  private Connection connection;
  private HttpServletRequest request;
  private String surchargeExplanation = "";


  /*
   * Default Constructor.
   */
  private LoadDeliveryConfirmationBO() {
    super();
  }


  /**
   * Initializes the DeliveryInfoBO using the supplied request and connection.
   * @param request
   * @param connection
   */
  public LoadDeliveryConfirmationBO(HttpServletRequest request, Connection connection) {
    this();
    this.request = request;
    this.connection = connection;
  }


  /**
   * Returns a Map containing all data needed by the action to perform the
   * transformation and redirect.
   * @return HashMap
   */
  public HashMap processRequest()
      throws Exception {
    HashMap toReturn = new HashMap();

    getRequestInfo();
    
    getConfigInfo();

    // generate the document used during transformation
    document = getDeliveryConfirmationInfo();

    // error xml (passed from UpdateDeliveryConfirmationBO)
    setErrorXml();

    // page data
    getPageData();
    
    // set the Document
    toReturn.put( "DELIVERY_CONFIRMATION_INFO", document );

    // set the action mapping
    toReturn.put( "FORWARD", "success" );

    return toReturn;
  }


  /*
   * Builds the Delivery Confirmation Information Document.
   * @return Document containing the the Delivery Confirmation Information
   */
  private Document getDeliveryConfirmationInfo()
      throws Exception {

    UpdateOrderDAO dao = new UpdateOrderDAO(connection);
    HashMap results = dao.getOrderDetailsUpdate(orderDetailId, COMConstants.DELIVERY_CONFIRMATION_UPDATE_TYPE, false, csrId, null);
    Document toReturn = (Document)DOMUtil.getDocument();

    // original product
    Transformer transformer = ResultSetToXmlTransformer.getInstance("ORIG_PRODUCTS", "ORIG_PRODUCT", true);
    Object transformed = transformer.transform( results.get("OUT_ORIG_PRODUCT") );
    if ( transformed != null ) {
      DOMUtil.addSection( toReturn, ((Document)transformed).getChildNodes() );
    }

    // updated addons
    transformer = ResultSetToXmlTransformer.getInstance("UPDATED_ADDONS", "UPDATED_ADDON", true);
    transformed = transformer.transform( results.get("OUT_UPD_ADDON_CUR") );
    if ( transformed != null ) {
      DOMUtil.addSection( toReturn, ((Document)transformed).getChildNodes() );
    }

    // updated product
    transformer = ResultSetToXmlTransformer.getInstance("UPDATED_PRODUCTS", "UPDATED_PRODUCT", true);
    transformed = transformer.transform( results.get("OUT_UPD_ORDER_CUR") );
    if ( transformed != null ) {
      DOMUtil.addSection( toReturn, ((Document)transformed).getChildNodes() );
    }

    // original taxes
    transformer = ResultSetToXmlTransformer.getInstance("PD_ORDER_TAXES", "PD_ORDER_TAX", true);
    transformed = transformer.transform( results.get("OUT_ORIG_TAXES_CUR") );
    if ( transformed != null ) {
      DOMUtil.addSection( toReturn, ((Document)transformed).getChildNodes() );
    }

    // updated taxes
    transformer = ResultSetToXmlTransformer.getInstance("UPDATED_TAXES", "UPDATED_TAX", true);
    transformed = transformer.transform( results.get("OUT_UPD_TAXES_CUR") );
    if ( transformed != null ) {
      DOMUtil.addSection( toReturn, ((Document)transformed).getChildNodes() );
    }

    // transform all dates
    transformer = DateFormatTransformer.getInstance(IN_FORMAT, OUT_FORMAT);
    Closure closure = NodeUpdateClosure.getInstance(transformer, toReturn);
    closure.execute( DELIVERY_DATE );
    closure.execute( DELIVERY_DATE_RANGE_END );

    String requestedDeliveryDate = getNodeValue(PRODUCTS_XPATH + "/delivery_date", (Document)transformed);
    String requestedDeliveryDateRangeEnd = getNodeValue(PRODUCTS_XPATH + "/delivery_date_range_end", (Document)transformed);
    Calendar tempCalendar = Calendar.getInstance();
  	//Define the format
    SimpleDateFormat sdfIN = new SimpleDateFormat(IN_FORMAT);
    SimpleDateFormat sdfOUT = new SimpleDateFormat("EEE MM/dd/yyyy");

    if (requestedDeliveryDate != null && !requestedDeliveryDate.equalsIgnoreCase(""))
    {
      tempCalendar.setTime(sdfIN.parse(requestedDeliveryDate));
      requestedDeliveryDate = sdfOUT.format(tempCalendar.getTime());
    }
    if (requestedDeliveryDateRangeEnd != null && !requestedDeliveryDateRangeEnd.equalsIgnoreCase(""))
    {
      tempCalendar.setTime(sdfIN.parse(requestedDeliveryDateRangeEnd));
      requestedDeliveryDateRangeEnd = sdfOUT.format(tempCalendar.getTime());
    }

    // transform phone numbers
    transformer = PhoneFormatTransformer.getInstance();
    closure = NodeUpdateClosure.getInstance(transformer, toReturn);
    closure.execute( RECIPIENT_PHONE_NUMBER );

    // get the order grand total
    OrderDetailsVO orderDetailsVO = new OrderDetailsVO();
    orderDetailsVO.setProductsAmount( DOMUtil.getNodeText(toReturn, PRODUCT_AMOUNT) );
    orderDetailsVO.setAddOnAmount( DOMUtil.getNodeText(toReturn, ADD_ON_AMOUNT) );
    orderDetailsVO.setServiceFeeAmount( DOMUtil.getNodeText(toReturn, SERVICE_FEE) );
    orderDetailsVO.setShippingFeeAmount( DOMUtil.getNodeText(toReturn, SHIPPING_FEE) );
    orderDetailsVO.setShippingTax( DOMUtil.getNodeText(toReturn, SHIPPING_TAX) );
    orderDetailsVO.setTaxAmount( DOMUtil.getNodeText(toReturn, TAX) );
    orderDetailsVO.setDiscountAmount( DOMUtil.getNodeText(toReturn, DISCOUNT_AMOUNT) );

    //issue 2331
    RecipientsVO recipient = new RecipientsVO();
    RecipientAddressesVO recipientAddr = new RecipientAddressesVO();
    recipientAddr.setCity( DOMUtil.getNodeText(toReturn, RECIPIENT_CITY) ) ;
    recipientAddr.setStateProvince( DOMUtil.getNodeText(toReturn, RECIPIENT_STATE) ) ;
    recipientAddr.setPostalCode( DOMUtil.getNodeText(toReturn, RECIPIENT_ZIP_CODE) ) ;
    recipientAddr.setCountry( DOMUtil.getNodeText(toReturn, RECIPIENT_COUNTRY) ) ;
    Collection recipients = new ArrayList();
    Collection recipientAddrs = new ArrayList();
    recipientAddrs.add(recipientAddr);
    recipient.setRecipientAddresses((List)recipientAddrs);
    recipients.add(recipient);
    orderDetailsVO.setRecipients((List)recipients);
    orderTotal = new RecalculateOrderBO().calculateOrderTotal(orderDetailsVO);

    // breadcrumbs
    DOMUtil.addSection( toReturn, new BreadcrumbUtil().getBreadcrumbXML(request).getChildNodes() );

    // subheader
    BasePageBuilder builder = new BasePageBuilder();
    DOMUtil.addSection( toReturn, builder.retrieveSubHeaderData(request).getChildNodes() );

    // message id
    CachedResultSet deliveryInfoRs = dao.getOriginalDeliveryInfo( orderDetailId );
    if ( deliveryInfoRs != null ) {
      if ( deliveryInfoRs.next() ) {
        messageId = deliveryInfoRs.getString("message_order_number");
      }
    }
    
      Document pageDataDoc = DOMUtil.getDocumentBuilder().newDocument();
      Element pageDataXML = (Element) pageDataDoc.createElement("pageData");
      pageDataDoc.appendChild(pageDataXML);
      addElement((Document)pageDataDoc,"requested_delivery_date",requestedDeliveryDate);
      addElement((Document)pageDataDoc,"requested_delivery_date_range_end",requestedDeliveryDateRangeEnd);

      logger.info(recipientAddr.getCountry() + " " + companyId);
      if (recipientAddr.getCountry() != null && recipientAddr.getCountry().equalsIgnoreCase("CA") &&
      		  companyId != null && !companyId.equalsIgnoreCase("FTDCA")) {
      	  logger.info("Get surcharge explanation");
          surchargeExplanation = ConfigurationUtil.getInstance().getContentWithFilter(this.connection,
                  "TAX", "TAX_EXPLANATION", null, null);
      }

      DOMUtil.addSection(toReturn,pageDataDoc.getChildNodes());
      this.requestedDeliveryDateString = requestedDeliveryDate;
      this.requestedDeliveryDateRangeEndString = requestedDeliveryDateRangeEnd;
      
      

    return toReturn;
  }
  
    private String convertDocToString(Document xmlDoc) throws Exception
    {
            String xmlString = "";      
          
            StringWriter s = new StringWriter();
            DOMUtil.print(xmlDoc,new PrintWriter(s));
            xmlString = s.toString();

            return xmlString;
        }        

     private void addElement(Document xml, String keyName, String value)
    {
        Element nameElement = (Element) xml.createElement(keyName);

        if(value != null && value.length() > 0 )
        {
            nameElement.appendChild(xml.createTextNode(value));
        }
        xml.getDocumentElement().appendChild(nameElement);
    }


  /*
   * Sets private data members using the request.
   */
  private void getRequestInfo() {
    orderDetailId = request.getParameter(COMConstants.ORDER_DETAIL_ID);
    sessionId = request.getParameter(COMConstants.SEC_TOKEN);
    buyerName = request.getParameter(COMConstants.BUYER_FULL_NAME);
    categoryIndex = request.getParameter(COMConstants.CATEGORY_INDEX);
    companyId = request.getParameter(COMConstants.COMPANY_ID);
    customerId = request.getParameter(COMConstants.CUSTOMER_ID);
    occasionText = request.getParameter(COMConstants.OCCASION_TEXT);
    origProductAmount = request.getParameter(COMConstants.ORIG_PRODUCT_AMOUNT);
    origProductId = request.getParameter(COMConstants.ORIG_PRODUCT_ID);
    if (!StringUtils.isEmpty(origProductId))
      origProductId = origProductId.toUpperCase();
    originId = request.getParameter(COMConstants.ORIGIN_ID);
    pageNumber = request.getParameter(COMConstants.PAGE_NUMBER);
    pricePointId = request.getParameter(COMConstants.PRICE_POINT_ID);
    shipMethod = request.getParameter(COMConstants.SHIP_METHOD);
  }


  /*
   * Retrieve the info from the configuration file.
   */
  private void getConfigInfo()
      throws Exception {

    ConfigurationUtil cu = ConfigurationUtil.getInstance();

    // image location
    imageLocation = cu.getProperty(COMConstants.PROPERTY_FILE, COMConstants.CONS_PRODUCT_IMAGE_LOCATION);

    SecurityManager sm = SecurityManager.getInstance();
    UserInfo ui = sm.getUserInfo( sessionId );
    csrId = ui.getUserID();
    
  }


  /*
   * If error messages are present in the request attributes, add them to the document.
   */
  private void setErrorXml(){
    Object errorXml = request.getAttribute("error_messages");
    if ( document != null && errorXml != null ) {
      DOMUtil.addSection(document, ((Document)errorXml).getChildNodes());
    }
  }


  /*
   * Adds the pageData element to the XML document.
   */
  private void getPageData() throws Exception {
    HashMap pageData = new HashMap();
    pageData.put(COMConstants.PD_MO_PRODUCT_IMAGE_LOCATION, imageLocation);
    pageData.put("order_total", orderTotal);
    pageData.put("message_id", messageId);
    pageData.put(COMConstants.PD_MO_BUYER_FULL_NAME, buyerName);
    pageData.put(COMConstants.PD_MO_CATEGORY_INDEX, categoryIndex);
    pageData.put(COMConstants.PD_MO_COMPANY_ID, companyId);
    pageData.put(COMConstants.PD_MO_OCCASION_TEXT, occasionText);
    pageData.put(COMConstants.PD_MO_ORIGIN_ID, originId);
    pageData.put(COMConstants.PD_MO_PAGE_NUMBER, pageNumber);
    pageData.put(COMConstants.PD_MO_ORIG_PRODUCT_AMOUNT, origProductAmount);
    pageData.put(COMConstants.PD_MO_ORIG_PRODUCT_ID, origProductId);
    pageData.put(COMConstants.PD_MO_PRICE_POINT_ID, pricePointId);
    pageData.put(COMConstants.SHIP_METHOD, shipMethod);
    pageData.put("requested_delivery_date", requestedDeliveryDateString);
    pageData.put("requested_delivery_date_range_end", requestedDeliveryDateRangeEndString);    
    pageData.put("surcharge_explanation", surchargeExplanation);
    
    addFreeShippingSavingsToPageData(pageData);

    String custom_order = "N";
    if( (String)request.getAttribute("is_custom_order") != null)
    {
      custom_order = (String)request.getAttribute("is_custom_order"); 
    }
    pageData.put("is_custom_order", custom_order);
   
      
    DOMUtil.addSection(document, "pageData", "data", pageData, true);
  }


  /**---------------------------------------------------------------------------
  *
  * getNodeValue
  *
  * Private utility method to do xpath query to get a node value.
  *
  * @params String a_xpath       - Xpath to element
  * @params Document a_DocXml - XML document
  * @returns String              - Node value (or null if not found)
  */
  private String getNodeValue(String a_xpath, Document a_docXml) throws Exception {
    String retValue = null;
    NodeList nl = DOMUtil.selectNodes(a_docXml,a_xpath);
    if (nl != null && nl.getLength() > 0){
      Text node = (Text)nl.item(0).getFirstChild();
      if(node != null) {
        retValue = node.getNodeValue();
      }
    }
    return retValue;
  }


  /**
   * Adds the Free Shipping savings to the Page data
   * @param pageData
   */
  private void addFreeShippingSavingsToPageData(HashMap pageData)
  throws Exception
  {  
    logger.info("Checking free shipping savings");
    
    String serviceFeeSaved =  DOMUtil.getNodeText(document, SERVICE_FEE_SAVED);
    String shippingFeeSaved =  DOMUtil.getNodeText(document, SHIPPING_FEE_SAVED);
    
    BigDecimal zeroVal = new BigDecimal("0.00");
    String orderFeesSavedMsg = "";
    Boolean fsSavings = null;
    
	//SN: Added the IF and ELSE block
    BigDecimal savingsVal = FieldUtils.stringToBigDecimal(serviceFeeSaved, zeroVal).add(FieldUtils.stringToBigDecimal(shippingFeeSaved, zeroVal));
	ServicesProgramDAO spDAO = new ServicesProgramDAO();
	AccountProgramMasterVO acctProgMasterVO = spDAO.getFreeShippingProgramInfo();
	
	//SN: Added the following lines
	fsSavings=spDAO.getBuyerHasFreeShipping(this.connection, orderDetailId);
	
	if(fsSavings == null){
		orderFeesSavedMsg = FieldUtils.replaceAll(COMConstants.RESPONSE_CAMS_UNREACHABLE, "{programName}", acctProgMasterVO.getDisplayName());
	    logger.info("msg: " + orderFeesSavedMsg);
	} else {
	 if(fsSavings == true){
		orderFeesSavedMsg = spDAO.getFreeShippingOrderSavingsMsg(this.connection, savingsVal.doubleValue());
	    logger.info("msg: " + orderFeesSavedMsg);
      } //SN: Add an else statement if a message needs to displayed for non-members. 	
	}
    pageData.put(COMConstants.FREESHIP_ORDER_SAVINGS_MSG, orderFeesSavedMsg);
  }
  
}
