package com.ftd.mo.bo;
import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.mo.dao.UpdateOrderDAO;
import com.ftd.customerordermanagement.dao.ViewDAO;
import com.ftd.customerordermanagement.util.BasePageBuilder;
import com.ftd.mo.util.SearchUtil;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.XMLFormat;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.security.SecurityManager;
import com.ftd.security.cache.vo.UserInfo;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import com.ftd.mo.util.BreadcrumbUtil;
import javax.servlet.ServletContext;

import java.sql.Connection;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;

import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;


import com.ftd.mo.vo.*;


public class LoadProductCategoryBO
{

/*******************************************************************************************
 * Class level variables
 *******************************************************************************************/
  private HttpServletRequest  request             = null;
  private Connection          con                 = null;
  private static Logger       logger              = new Logger("com.ftd.mo.bo.LoadProductCategoryBO");


  //The following variables are the ONLY ones that the XSL will initially send
  private String              buyerName           = null;
  private String              categoryIndex       = null;
  private String              companyId           = null;
  private String              customerId          = null;
  private String              deliveryDate        = "";
  private String              deliveryDateRange   = "";
  private String              externalOrderNumber = null;
  private String              masterOrderNumber   = null;
  private String              occasion            = null;
  private String              occasionText        = null;
  private String              orderDetailId       = null;
  private String              orderGuid           = null;
  private String              originId            = null;
  private	String							origProductAmount		= null;
  private	String							origProductId				= null;
  private String              pageNumber          = null;
  private String              pricePointId        = null;
  private String              recipientCity       = null;
  private String              recipientCountry    = null;
  private String              recipientState      = null;
  private String              recipientZipCode    = null;
  private String              rewardType          = null;
  private String              shipMethod          = null;
  private String              sourceCode          = null;

  //The following variables will be initialized in the class, and will be used in subsequent classes
  private ServletContext      context             = null;
  private String              csrId               = null;
  private boolean             domesticFlag        = true;
  private String              domesticSvcFee      = "0";
  private String              domIntFlag          = null;
  private HashMap             hashXML             = new HashMap();
  private String              imageLocation       = null;
  private String              internationalSvcFee = "0";
  private HashMap             pageData            = new HashMap();
  private String              partnerId           = null;
  private String              pricingCode         = null;
  private String              sessionId           = null;
  private String              snhId               = null;
  private SearchUtil          sUtil;

  // Standard, Deluxe, Premium labels
  private String              standardLabel       = null;
  private String              deluxeLabel         = null;
  private String              premiumLabel        = null;

/*******************************************************************************************
 * Constructor 1
 *******************************************************************************************/
  public LoadProductCategoryBO()
  {
  }


/*******************************************************************************************
 * Constructor 2
 *******************************************************************************************/
  public LoadProductCategoryBO(HttpServletRequest request, Connection con, ServletContext context)
  {
    this.request = request;
    this.con = con;
    this.context = context;
  }


/*******************************************************************************************
  * processRequest()
  ******************************************************************************************
  *
  * @param  String - action
  * @return HashMap - contains 0 - many XML documents of data, 1 HashMap each for page
  *                   details and search criteria
  * @throws
  */
  public HashMap processRequest() throws Exception
  {
    //HashMap that will be returned to the calling class
    HashMap returnHash = new HashMap();

    //Get info from the request object
    getRequestInfo(this.request);

    //Get config file info
    getConfigInfo();

    //instantiate the search util
    this.sUtil = new SearchUtil(this.con);

    //find if the recipient recipientCountry was domestic or not
    this.domesticFlag = this.sUtil.isDomestic(this.recipientCountry);
    if (this.domesticFlag)
      this.domIntFlag = COMConstants.CONS_DELIVERY_TYPE_DOMESTIC;
    else
      this.domIntFlag = COMConstants.CONS_DELIVERY_TYPE_INTERNATIONAL;


    //process the action
    processMain();

    //populate the remainder of the fields on the page data
    populatePageData();

    //populate the sub header info
    populateSubHeaderInfo();

    //retrieve all the Documents from hashXML and put them in returnHash
    //retrieve the keyset
    Set ks1 = hashXML.keySet();
    Iterator iter = ks1.iterator();
    String key = null;
    Document doc = null;

    //Iterate thru the keyset
    while(iter.hasNext())
    {
      //get the key
      key = iter.next().toString();

      //retrieve the XML document
      doc = (Document) hashXML.get(key);

      //put the XML object in the returnHash
      returnHash.put(key, (Document) doc);
    }

    //append pageData to returnHash
    returnHash.put("pageData",        (HashMap)this.pageData);

    return returnHash;
  }



/*******************************************************************************************
  * getRequestInfo(HttpServletRequest request)
  ******************************************************************************************
  * Retrieve the info from the request object
  *
  * @param  HttpServletRequest - request
  * @return none
  * @throws none
  */
  private void getRequestInfo(HttpServletRequest request) throws Exception
  {

    /****************************************************************************************
     *  Retrieve order/search specific parameters
     ***************************************************************************************/
    //retrieve the buyer_full_name
    if(request.getParameter(COMConstants.BUYER_FULL_NAME)!=null)
    {
      this.buyerName = request.getParameter(COMConstants.BUYER_FULL_NAME);
    }

    //retrieve the category index
    if(request.getParameter(COMConstants.CATEGORY_INDEX)!=null)
    {
      this.categoryIndex = request.getParameter(COMConstants.CATEGORY_INDEX);
    }

    //retrieve the company id
    if(request.getParameter(COMConstants.COMPANY_ID)!=null)
    {
      this.companyId = request.getParameter(COMConstants.COMPANY_ID);
    }

    //retrieve the customer id
    if(request.getParameter(COMConstants.CUSTOMER_ID)!=null)
    {
      this.customerId = request.getParameter(COMConstants.CUSTOMER_ID);
    }

    //retrieve the delivery date
    if(request.getParameter(COMConstants.DELIVERY_DATE)!=null)
    {
      this.deliveryDate = request.getParameter(COMConstants.DELIVERY_DATE);
    }

    //retrieve the delivery date range end
    if(request.getParameter(COMConstants.DELIVERY_DATE_RANGE_END)!=null)
    {
      this.deliveryDateRange = request.getParameter(COMConstants.DELIVERY_DATE_RANGE_END);
    }

    //retrieve the item order number
    if(request.getParameter(COMConstants.EXTERNAL_ORDER_NUMBER)!=null)
    {
      this.externalOrderNumber = request.getParameter(COMConstants.EXTERNAL_ORDER_NUMBER);
    }

    //retrieve the master order number
    if(request.getParameter(COMConstants.MASTER_ORDER_NUMBER)!=null)
    {
      this.masterOrderNumber = request.getParameter(COMConstants.MASTER_ORDER_NUMBER);
    }

    //retrieve the occasion id
    if(request.getParameter(COMConstants.OCCASION)!=null)
    {
      this.occasion = request.getParameter(COMConstants.OCCASION);
    }

    //retrieve the occasion text
    if(request.getParameter(COMConstants.OCCASION_TEXT)!=null)
    {
      this.occasionText = request.getParameter(COMConstants.OCCASION_TEXT);
    }

    //retrieve the order detail id
    if(request.getParameter(COMConstants.ORDER_DETAIL_ID)!=null)
    {
      this.orderDetailId = request.getParameter(COMConstants.ORDER_DETAIL_ID);
    }

    //retrieve the order guid
    if(request.getParameter(COMConstants.ORDER_GUID)!=null)
    {
      this.orderGuid = request.getParameter(COMConstants.ORDER_GUID);
    }

    //retrieve the origin id
    if(request.getParameter(COMConstants.ORIGIN_ID)!=null)
    {
      this.originId = request.getParameter(COMConstants.ORIGIN_ID);
    }

    //retrieve the page number
    if(request.getParameter(COMConstants.PAGE_NUMBER)!=null)
    {
      this.pageNumber = request.getParameter(COMConstants.PAGE_NUMBER);
    }

    //retrieve the price point id
    if(request.getParameter(COMConstants.PRICE_POINT_ID)!=null)
    {
      this.pricePointId = request.getParameter(COMConstants.PRICE_POINT_ID);
    }

    //retrieve the original product amount
    if(request.getParameter(COMConstants.ORIG_PRODUCT_AMOUNT)!=null)
    {
      this.origProductAmount = request.getParameter(COMConstants.ORIG_PRODUCT_AMOUNT);
    }

    //retrieve the product id
    if(request.getParameter(COMConstants.ORIG_PRODUCT_ID)!=null)
    {
      this.origProductId = (request.getParameter(COMConstants.ORIG_PRODUCT_ID)).toUpperCase();
    }

    //retrieve the city
    if(request.getParameter(COMConstants.RECIPIENT_CITY)!=null)
    {
      this.recipientCity = request.getParameter(COMConstants.RECIPIENT_CITY);
    }

    //retrieve the recipientCountry id
    if(request.getParameter(COMConstants.RECIPIENT_COUNTRY)!=null)
    {
      this.recipientCountry = request.getParameter(COMConstants.RECIPIENT_COUNTRY);
    }

    //retrieve the recipientState
    if(request.getParameter(COMConstants.RECIPIENT_STATE)!=null)
    {
      this.recipientState = request.getParameter(COMConstants.RECIPIENT_STATE);
    }

    //retrieve the postal code
    if(request.getParameter(COMConstants.RECIPIENT_ZIP_CODE)!=null)
    {
      this.recipientZipCode = request.getParameter(COMConstants.RECIPIENT_ZIP_CODE);
    }

    //retrieve the reward type
    if(request.getAttribute(COMConstants.REWARD_TYPE)!=null)
    {
      this.rewardType = request.getAttribute(COMConstants.REWARD_TYPE).toString();
    }
    if((this.rewardType == null || this.rewardType.equalsIgnoreCase("")) && request.getParameter(COMConstants.REWARD_TYPE)!=null)
    {
      this.rewardType = request.getParameter(COMConstants.REWARD_TYPE);
    }

    //retrieve the ship method
    if(request.getParameter(COMConstants.SHIP_METHOD)!=null)
    {
      this.shipMethod = request.getParameter(COMConstants.SHIP_METHOD);
    }

    //retrieve the source code
    if(request.getParameter(COMConstants.SOURCE_CODE)!=null)
    {
      this.sourceCode = request.getParameter(COMConstants.SOURCE_CODE);
    }

    /****************************************************************************************
     *  Retrieve Security Info
     ***************************************************************************************/
    this.sessionId   = request.getParameter(COMConstants.SEC_TOKEN);

  }


/*******************************************************************************************
  * getConfigInfo()
  ******************************************************************************************
  * Retrieve the info from the configuration file
  *
  * @param none
  * @return
  * @throws none
  */
  private void getConfigInfo() throws Exception
  {

    ConfigurationUtil cu = null;
    cu = ConfigurationUtil.getInstance();

    SecurityManager sm = SecurityManager.getInstance();
    UserInfo ui = sm.getUserInfo(this.sessionId);
    this.csrId = ui.getUserID();
    
    //get PRODUCT_IMAGE_LOCATION
    if (cu.getProperty(COMConstants.PROPERTY_FILE, COMConstants.CONS_PRODUCT_IMAGE_LOCATION) != null)
    {
      this.imageLocation  = cu.getProperty(COMConstants.PROPERTY_FILE, COMConstants.CONS_PRODUCT_IMAGE_LOCATION);
    }

    //get Standard, Deluxe, Premium labels
    String tempLabel = cu.getFrpGlobalParm("FTDAPPS_PARMS", "FLORAL_LABEL_STANDARD");
    if (tempLabel != null) {
        this.standardLabel = tempLabel;
    }
    tempLabel = cu.getFrpGlobalParm("FTDAPPS_PARMS", "FLORAL_LABEL_DELUXE");
    if (tempLabel != null) {
        this.deluxeLabel = tempLabel;
    }
    tempLabel = cu.getFrpGlobalParm("FTDAPPS_PARMS", "FLORAL_LABEL_PREMIUM");
    if (tempLabel != null) {
        this.premiumLabel = tempLabel;
    }

  }



/*******************************************************************************************
  * populatePageData()
  ******************************************************************************************
  * Populate the page data with the remainder of the fields
  *
  * @param none
  * @return
  * @throws none
  */
  private void populatePageData()
  {
    this.pageData.put(COMConstants.PD_MO_BUYER_FULL_NAME,         this.buyerName);
    this.pageData.put(COMConstants.PD_MO_CATEGORY_INDEX,          this.categoryIndex);
    this.pageData.put(COMConstants.PD_MO_COMPANY_ID,              this.companyId);
    this.pageData.put(COMConstants.PD_MO_CUSTOMER_ID,             this.customerId);
    this.pageData.put(COMConstants.PD_MO_DELIVERY_DATE,           this.deliveryDate);
    this.pageData.put(COMConstants.PD_MO_DELIVERY_DATE_RANGE_END, this.deliveryDateRange);
    this.pageData.put(COMConstants.PD_MO_DOMESTIC_INT_FLAG,       this.domIntFlag);
    this.pageData.put(COMConstants.PD_MO_DOMESTIC_SRVC_FEE,       this.domesticSvcFee);
    this.pageData.put(COMConstants.PD_MO_EXTERNAL_ORDER_NUMBER,   this.externalOrderNumber);
    this.pageData.put(COMConstants.PD_MO_PRODUCT_IMAGE_LOCATION,  this.imageLocation);
    this.pageData.put(COMConstants.PD_MO_INTERNATIONAL_SRVC_FEE,  this.internationalSvcFee);
    this.pageData.put(COMConstants.PD_MO_MASTER_ORDER_NUMBER,     this.masterOrderNumber);
		this.pageData.put(COMConstants.PD_MO_OCCASION,                this.occasion);
    this.pageData.put(COMConstants.PD_MO_OCCASION_TEXT,           this.occasionText);
    this.pageData.put(COMConstants.PD_MO_ORIG_PRODUCT_AMOUNT,			this.origProductAmount);
    this.pageData.put(COMConstants.PD_MO_ORIG_PRODUCT_ID,					this.origProductId);
    this.pageData.put(COMConstants.PD_MO_ORDER_DETAIL_ID,         this.orderDetailId);
    this.pageData.put(COMConstants.PD_MO_ORDER_GUID,              this.orderGuid);
    this.pageData.put(COMConstants.PD_MO_ORIGIN_ID,               this.originId);
    this.pageData.put(COMConstants.PD_MO_PAGE_NUMBER,             this.pageNumber);
    this.pageData.put(COMConstants.PD_MO_PARTNER_ID,              this.partnerId);
    this.pageData.put(COMConstants.PD_MO_PRICE_POINT_ID,          this.pricePointId);
    this.pageData.put(COMConstants.PD_MO_PRICE_POINT_ID,          this.pricePointId);
    this.pageData.put(COMConstants.PD_MO_PRICING_CODE,            this.pricingCode);
    this.pageData.put(COMConstants.PD_MO_RECIPIENT_CITY,          this.recipientCity);
    this.pageData.put(COMConstants.PD_MO_RECIPIENT_COUNTRY,       this.recipientCountry);
    this.pageData.put(COMConstants.PD_MO_RECIPIENT_STATE,         this.recipientState);
    this.pageData.put(COMConstants.PD_MO_RECIPIENT_ZIP_CODE,      this.recipientZipCode);
    this.pageData.put(COMConstants.PD_MO_REWARD_TYPE,             this.rewardType);
    this.pageData.put(COMConstants.PD_MO_SHIP_METHOD,             this.shipMethod);
    this.pageData.put(COMConstants.PD_MO_SOURCE_CODE,             this.sourceCode);
    this.pageData.put(COMConstants.PD_MO_SNH_ID,                  this.snhId);

    this.pageData.put(COMConstants.PD_MO_FLORAL_LABEL_STANDARD, this.standardLabel);
    this.pageData.put(COMConstants.PD_MO_FLORAL_LABEL_DELUXE, this.deluxeLabel);
    this.pageData.put(COMConstants.PD_MO_FLORAL_LABEL_PREMIUM, this.premiumLabel);

  }



/*******************************************************************************************
  * populateSubHeaderInfo()
  ******************************************************************************************
  * Populate the page data with the remainder of the fields
  *
  * @param none
  * @return
  * @throws none
  */
  private void populateSubHeaderInfo() throws Exception
  {
    BasePageBuilder bpb = new BasePageBuilder();

    //xml document for sub header
    Document subHeaderXML = DOMUtil.getDocument();

    //retrieve the sub header info
    subHeaderXML = bpb.retrieveSubHeaderData(this.request);

    //save the original product xml
    this.hashXML.put(COMConstants.HK_SUB_HEADER, subHeaderXML);

  }


/*******************************************************************************************
  * processBreadcrumbs()
  ******************************************************************************************
  *
  *
  * @param  none
  * @return none
  * @throws none
  */
  private void processBreadcrumbs(String breadcrumbPageName) throws Exception
  {
    HashMap optionalParms = new HashMap();
		optionalParms.put(COMConstants.BUYER_FULL_NAME,         this.buyerName);
    optionalParms.put(COMConstants.CATEGORY_INDEX,          this.categoryIndex);
    optionalParms.put(COMConstants.COMPANY_ID,              this.companyId);
    optionalParms.put(COMConstants.CUSTOMER_ID,             this.customerId);
    optionalParms.put(COMConstants.DELIVERY_DATE,           this.deliveryDate);
    optionalParms.put(COMConstants.DELIVERY_DATE_RANGE_END, this.deliveryDateRange);
    optionalParms.put(COMConstants.EXTERNAL_ORDER_NUMBER,   this.externalOrderNumber);
    optionalParms.put(COMConstants.MASTER_ORDER_NUMBER,     this.masterOrderNumber);
    optionalParms.put(COMConstants.OCCASION,     				    this.occasion);
    optionalParms.put(COMConstants.OCCASION_TEXT,           this.occasionText);
    optionalParms.put(COMConstants.ORDER_DETAIL_ID,         this.orderDetailId);
    optionalParms.put(COMConstants.ORDER_GUID,              this.orderGuid);
    optionalParms.put(COMConstants.ORIGIN_ID,               this.originId);
    optionalParms.put(COMConstants.PAGE_NUMBER,             this.pageNumber);
    optionalParms.put(COMConstants.PRICE_POINT_ID,          this.pricePointId);
    optionalParms.put(COMConstants.ORIG_PRODUCT_AMOUNT,     this.origProductAmount);
    optionalParms.put(COMConstants.ORIG_PRODUCT_ID,         this.origProductId);
    optionalParms.put(COMConstants.RECIPIENT_CITY,          this.recipientCity);
    optionalParms.put(COMConstants.RECIPIENT_COUNTRY,       this.recipientCountry);
    optionalParms.put(COMConstants.RECIPIENT_STATE,         this.recipientState);
    optionalParms.put(COMConstants.RECIPIENT_ZIP_CODE,      this.recipientZipCode);
    optionalParms.put(COMConstants.REWARD_TYPE,             this.rewardType);
    optionalParms.put(COMConstants.SHIP_METHOD,             this.shipMethod);
    optionalParms.put(COMConstants.SOURCE_CODE,             this.sourceCode);

    BreadcrumbUtil bcUtil = new BreadcrumbUtil();

//bcUtil.getBreadcrumbXML(  request,
                          //Title of breadcrumb,
                          //Name of Structs Action,
                          //Action parameter,
                          //Error display page,
                          //Flag indicating if breadcrumb should be a link, ,
                          //Optional hash of additional name/value parameters for this page
                      //)

    Document bcXML;

    if (breadcrumbPageName.equalsIgnoreCase(COMConstants.XSL_UPDATE_PRODUCT_CATEGORY))
    {
      bcXML = bcUtil.getBreadcrumbXML(this.request, COMConstants.BCT_PRODUCT_CATEGORY,
                              COMConstants.BCA_PRODUCT_CATEGORY, null, "Error",
                              "true", optionalParms, this.orderDetailId);
    }
    else
    {
      bcXML = bcUtil.getBreadcrumbXML(this.request, COMConstants.BCT_PRODUCT_LIST,
                              COMConstants.BCA_PRODUCT_LIST, null, "Error",
                              "true", optionalParms, this.orderDetailId);
    }

    this.hashXML.put("breadcrumbs",  bcXML);

  }




/*******************************************************************************************
  * processMain()
  ******************************************************************************************
  *
  * @param  none
  * @return none
  * @throws none
  */
  private void processMain() throws Exception
  {

    //get the source code info
    retrieveProductBySourceCodeInfo();

    //get the fee info
    retrieveFeeInfo();

    //retrieve the reward type
    if (this.rewardType == null || this.rewardType.equalsIgnoreCase(""))
      retrieveRewardType();

    //retrieve the order/product data from the temp tables.
    retrieveOrderData();

    if(this.domesticFlag)
    {
      //Load Super Index List
      retrieveSuperIndexInfo();

      //Load Super Index List with other date passed in
      retrieveSuperIndexWithDateInfo();

      //Load sub index list
      retrieveSubIndexInfo();

      //set the XSL parameter, which governs the page to be loaded
      this.pageData.put("XSL", COMConstants.XSL_UPDATE_PRODUCT_CATEGORY);

      //process Breadcrumb
      processBreadcrumbs(COMConstants.XSL_UPDATE_PRODUCT_CATEGORY);


    }
    else
    {
      //Load Super Index List
      retrieveSuperIndexInfo();

      //Load the product by category
      retrieveProductByCategory();

      //set the XSL parameter, which governs the page to be loaded
      this.pageData.put("XSL", COMConstants.XSL_UPDATE_PRODUCT_LIST);

      //process Breadcrumb
      processBreadcrumbs(COMConstants.XSL_UPDATE_PRODUCT_LIST);

    }


    //append addons
    Document addOnXML  = DOMUtil.getDocument();
    addOnXML = this.sUtil.appendAddons(this.request, addOnXML);
    this.hashXML.put(COMConstants.HK_ADDON_XML, addOnXML);
    
  }



/*******************************************************************************************
 * retrieveOrderData()
 *******************************************************************************************
  * This method is used to call the
  *
  */
  private void retrieveOrderData() throws Exception
  {
    //Instantiate UpdateOrderDAO
    UpdateOrderDAO uoDAO = new UpdateOrderDAO(this.con);

    //hashmap that will contain the output from the stored proc
    HashMap tempOrderInfo = new HashMap();

    //Call getOrderDetailsUpdate method in the DAO
    tempOrderInfo = uoDAO.getOrderDetailsUpdate(this.orderDetailId,
                            COMConstants.CONS_ENTITY_TYPE_PRODUCT_DETAIL, false, this.csrId, this.origProductId );

    //Create an Document and call a method that will transform the CachedResultSet from the cursor:
    //OUT_ORIG_PRODUCT into an Document.  Store this XML object in hashXML HashMap
    Document origProductXML =
        buildXML(tempOrderInfo, "OUT_ORIG_PRODUCT",     "PD_ORIG_PRODUCTS",   "PD_ORIG_PRODUCTS",   "element");
    this.hashXML.put(COMConstants.HK_ORIG_PRODUCT_XML,  origProductXML);

    //Create an Document and call a method that will transform the CachedResultSet from the cursor:
    //OUT_ORIG_ADD_ONS into an Document.  Store this XML object in hashXML HashMap
    Document origAddOnXML =
        buildXML(tempOrderInfo, "OUT_ORIG_ADD_ONS",     "PD_ORIG_ADD_ONS",    "PD_ORIG_ADD_ON",     "element");
    this.hashXML.put(COMConstants.HK_ORIG_ADDON_XML,    origAddOnXML);

    //Create an Document and call a method that will transform the CachedResultSet from the cursor:
    //OUT_UPD_ORDER_CUR into an Document.  Store this XML object in hashXML HashMap
    Document orderProductXML =
        buildXML(tempOrderInfo, "OUT_UPD_ORDER_CUR",    "PD_ORDER_PRODUCTS",   "PD_ORDER_PRODUCT",  "element");
    this.hashXML.put(COMConstants.HK_ORDER_PRODUCT_XML, orderProductXML);

    //Create an Document and call a method that will transform the CachedResultSet from the cursor:
    //OUT_UPD_ADDON_CUR into an Document.  Store this XML object in hashXML HashMap
    Document orderAddOnXML =
        buildXML(tempOrderInfo, "OUT_UPD_ADDON_CUR",    "PD_ORDER_ADDONS",    "PD_ORDER_ADDON",     "element");
    this.hashXML.put(COMConstants.HK_ORDER_ADDON_XML,   orderAddOnXML);

    //Create an Document and call a method that will transform the CachedResultSet from the cursor:
    //OUT_SHIP_COST into an Document.  Store this XML object in hashXML HashMap
    Document shipCostXML =
        buildXML(tempOrderInfo, "OUT_SHIP_COST",        "PD_SHIP_COSTS",      "PD_SHIP_COSTS",      "element");
    this.hashXML.put(COMConstants.HK_SHIP_COST_XML,     shipCostXML);

    //Create an Document and call a method that will transform the CachedResultSet from the cursor:
    //OUT_ORIG_TAXES_CUR into an Document.  Store this XML object in hashXML HashMap
    Document orderTaxXML =
        buildXML(tempOrderInfo, "OUT_ORIG_TAXES_CUR", "PD_ORDER_TAXES", "PD_ORDER_TAX",   "element");
    this.hashXML.put(COMConstants.HK_ORDER_TAX_XML, orderTaxXML);

  }


/*******************************************************************************************
 * retrieveProductBySourceCodeInfo() (COM_SOURCE_CODE_RECORD_LOOKUP)
 *******************************************************************************************
  * This method is used to call the
  *
  */
  private void retrieveProductBySourceCodeInfo() throws Exception
  {
    BasePageBuilder bpb = new BasePageBuilder();

    //Call retrieveProductBySourceCodeInfo method in the BasePageBuilder
    Document sourceCodeInfo = (Document) bpb.retrieveProductBySourceCodeInfo(this.con, this.sourceCode);

    //create an array.
    ArrayList aList = new ArrayList();

    //Initialize the array with the top and bottom nodes of the XML, and the node for which
    //the value must be retrieved. In this instance, we will retireve the value of
    //shipping code
    aList.clear();
    aList.add(0,"PRODUCTS");
    aList.add(1,"PRODUCT");
    aList.add(2,"shipping_code");
    String shippingCode = DOMUtil.getNodeValue(sourceCodeInfo, aList);
    this.snhId = shippingCode;

    //Initialize the array with the top and bottom nodes of the XML, and the node for which
    //the value must be retrieved. In this instance, we will retireve the value of
    //pricing code
    aList.clear();
    aList.add(0,"PRODUCTS");
    aList.add(1,"PRODUCT");
    aList.add(2,"pricing_code");
    String priceCode = DOMUtil.getNodeValue(sourceCodeInfo, aList);
    this.pricingCode = priceCode;

    //Initialize the array with the top and bottom nodes of the XML, and the node for which
    //the value must be retrieved. In this instance, we will retireve the value of
    //partner id
    aList.clear();
    aList.add(0,"PRODUCTS");
    aList.add(1,"PRODUCT");
    aList.add(2,"partner_id");
    String ptnrId = DOMUtil.getNodeValue(sourceCodeInfo, aList);
    this.partnerId = ptnrId;


    this.hashXML.put(COMConstants.HK_SOURCE_CODE_XML,  sourceCodeInfo);

  }


/*******************************************************************************************
 * retrieveFeeInfo() (COM_SNH_BY_ID)
 *******************************************************************************************
  * This method is used to call the
  *
  */
  private void retrieveFeeInfo() throws Exception
  {
    BasePageBuilder bpb = new BasePageBuilder();

    //Call retrieveFeeInfo method in the Base Page Builder
    Document feeInfo = (Document) bpb.retrieveFeeInfo(this.con, this.snhId, this.deliveryDate);

    //create an array.
    ArrayList aList = new ArrayList();

    //Initialize the array with the top and bottom nodes of the XML, and the node for which
    //the value must be retrieved. In this instance, we will retireve the value of
    //first order domestic
    aList.clear();
    aList.add(0,"FEES");
    aList.add(1,"FEE");
    aList.add(2,"first_order_domestic");
    String domesticFee = DOMUtil.getNodeValue(feeInfo, aList);
    this.domesticSvcFee = domesticFee;

    //Initialize the array with the top and bottom nodes of the XML, and the node for which
    //the value must be retrieved. In this instance, we will retireve the value of
    //first order international
    aList.clear();
    aList.add(0,"FEES");
    aList.add(1,"FEE");
    aList.add(2,"first_order_international");
    String internationalFee = DOMUtil.getNodeValue(feeInfo, aList);
    this.internationalSvcFee = internationalFee;

  }


/*******************************************************************************************
 * retrieveRewardType()
 *******************************************************************************************
 * get the reward type.
 *
 * @return nothing
 * @throws java.lang.Exception
 */
  private void retrieveRewardType() throws Exception
  {
    UpdateOrderDAO uoDAO = new UpdateOrderDAO(this.con);

    String newRewardType = (String) uoDAO.getDiscountRewardTypeById(this.sourceCode);

    this.rewardType = newRewardType;

  }



/*******************************************************************************************
 * retrieveSuperIndexInfo()
 *******************************************************************************************
  * This method is used to call the
  *
  */
  private void retrieveSuperIndexInfo() throws Exception
  {
    //Instantiate UpdateOrderDAO
    UpdateOrderDAO uoDAO = new UpdateOrderDAO(this.con);

    //HashMap that will contain the output from the stored proc
    HashMap superIndexResults = new HashMap();
    CachedResultSet superIndexRS;

    //getSuperIndexXML(liveFlag, occasionFlag, productsFlag, dropShipFlag, recipientCountry, sourceCode)
    if(this.domesticFlag)
      superIndexRS = (CachedResultSet) uoDAO.getSuperIndexRS("Y", "N", "Y", "N", null, this.sourceCode);
    else
      superIndexRS = (CachedResultSet) uoDAO.getSuperIndexRS("Y", "N", "N", "N", this.recipientCountry, this.sourceCode);


    //put the CachedResultSet in a hashmap
    superIndexResults.put("OUT_CUR", superIndexRS);

    //Create an Document and call a method that will transform the CachedResultSet from the cursor:
    //OUT_CUR into an Document.  Store this XML object in hashXML HashMap
    Document superIndexXML =
      buildXML(superIndexResults, "OUT_CUR", "PRODUCT_INDEXES", "INDEX", "attribute");
    //and store the XML object in a hashXML hashmap
    this.hashXML.put(COMConstants.HK_SUPER_INDEX_XML,  superIndexXML);


  }


/*******************************************************************************************
 * retrieveSuperIndexWithDateInfo()
 *******************************************************************************************
  * This method is used to call the
  *
  */
  private void retrieveSuperIndexWithDateInfo() throws Exception
  {
    //Instantiate UpdateOrderDAO
    UpdateOrderDAO uoDAO = new UpdateOrderDAO(this.con);

    //HashMap that will contain the output from the stored proce
    HashMap superIndexWithDateResults = new HashMap();
    CachedResultSet superIndexWithDateRS;

    //getSuperIndexXML(liveFlag, occasionFlag, productsFlag, dropShipFlag, recipientCountry, sourceCode)
    superIndexWithDateRS = (CachedResultSet) uoDAO.getSuperIndexRS("Y", "Y", "N", "N", null, this.sourceCode);

    //put the CachedResultSet in a hashmap
    superIndexWithDateResults.put("OUT_CUR", superIndexWithDateRS);

    //Create an Document and call a method that will transform the CachedResultSet from the cursor:
    //OUT_PHONE_CUR into an Document.  Store this XML object in hashXML HashMap
    Document superIndexWithDateXML =
      buildXML(superIndexWithDateResults, "OUT_CUR", "OCCASION_INDEXES", "INDEX", "attribute");
    //and store the XML object in a hashXML hashmap
    this.hashXML.put(COMConstants.HK_SUPER_INDEX_WITH_DATE_XML,  superIndexWithDateXML);

  }


/*******************************************************************************************
 * retrieveSubIndexInfo()
 *******************************************************************************************
  * This method is used to call the
  *
  */
  private void retrieveSubIndexInfo() throws Exception
  {
    //Instantiate ViewDAO
    UpdateOrderDAO uoDAO = new UpdateOrderDAO(this.con);

    //Document that will contain the output from the stored proce
    Document subIndexInfo = DOMUtil.getDocument();

    //getSubIndexXML(liveFlag, dropShipFlag, recipientCountry, sourceCode)
    subIndexInfo = uoDAO.getSubIndexXML("Y", "N", null, this.sourceCode);

    this.hashXML.put(COMConstants.HK_SUB_INDEX_XML,  subIndexInfo);

  }


/*******************************************************************************************
 * retrieveProductByCategory()
 *******************************************************************************************
  * This method is used to call the
  *
  */
  private void retrieveProductByCategory() throws Exception
  {
    List promotionList = new ArrayList();
    ProductFlagsVO flagsVO = new ProductFlagsVO();


    HashMap productMap = new HashMap();

    productMap.put("page_number",          this.pageNumber);

    Document superIndexXML = (Document) this.hashXML.get(COMConstants.HK_SUPER_INDEX_XML);

    // Pull the super index details for the selected recipientCountry
    Element superIndexData = null;
    String xpath = "//INDEX";
    NodeList nl = DOMUtil.selectNodes(superIndexXML,xpath);

    superIndexData = (Element) nl.item(0);
    this.categoryIndex = superIndexData.getAttribute("indexid");
    productMap.put("category_index",       this.categoryIndex);

    //clear out zipcode for internationial searchers
    String zipCode = "";

    //obtain a list of products for this recipientCountry
    Document productsXML = this.sUtil.getProductsByCategory( this.pageNumber,
                                    this.categoryIndex, zipCode,  this.domesticFlag,
                                    this.sourceCode, this.pricePointId, this.recipientCountry,
                                    this.domesticSvcFee,  this.internationalSvcFee,
                                    this.recipientState,  this.pricingCode,  this.partnerId,
                                    promotionList, this.rewardType, flagsVO, null,
                                    this.buyerName, this.origProductId, this.shipMethod,
                                    productMap, this.context, this.deliveryDate);

    this.hashXML.put(COMConstants.HK_OCCASION_LIST_XML,  productsXML);

    Set ks = productMap.keySet();
    Iterator iter = ks.iterator();
    String key;
    //Iterate thru the hashmap returned from the business object using the keyset
    while(iter.hasNext())
    {
      key = iter.next().toString();
      this.pageData.put(key, (String)productMap.get(key));
    }
    this.pageNumber = this.pageData.get("page_number")!=null?this.pageData.get("page_number").toString():this.pageNumber;


  }




/*******************************************************************************************
 * buildXML()
 *******************************************************************************************/
  private Document buildXML(HashMap outMap, String cursorName, String topName, String bottomName, String type)
        throws Exception
  {

    //Create a CachedResultSet object using the cursor name that was passed.
    CachedResultSet rs = (CachedResultSet) outMap.get(cursorName);

    Document doc =  null;

    //Create an XMLFormat, and initialize the top and bottom node.  Note that since this method
    //can be/is called by various other methods, the top and botton names MUST be passed to it.
    XMLFormat xmlFormat = new XMLFormat();
    xmlFormat.setAttribute("type", type);
    xmlFormat.setAttribute("top", topName);
    xmlFormat.setAttribute("bottom", bottomName );

    //call the DOMUtil's converToXMLDOM method
    doc = (Document) DOMUtil.convertToXMLDOM(rs, xmlFormat);

    //return the xml document.
    return doc;

  }


}