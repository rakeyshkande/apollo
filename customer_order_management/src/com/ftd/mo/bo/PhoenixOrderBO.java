package com.ftd.mo.bo;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.naming.InitialContext;

import org.apache.commons.lang.StringUtils;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.mo.dao.UpdateOrderDAO;
import com.ftd.mo.vo.DeliveryInfoVO;
import com.ftd.op.order.dao.PhoenixDAO;
import com.ftd.osp.framework.dispatcher.Dispatcher;
import com.ftd.osp.utilities.PASServiceUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.pas.core.domain.PasShipDatesAvailVO;
import com.ftd.pas.core.domain.ProductAvailVO;

public class PhoenixOrderBO {
	private static Logger logger = new Logger("com.ftd.mo.bo.PhoenixOrderBO");
	Connection conn = null;

	public PhoenixOrderBO(Connection con) {
		this.conn = con;
	}

	public DeliveryInfoVO getPhoenixOrderInfo(String orderDetailId)
			throws Exception {
		// TODO implement the method. we need to add a new method to DAO which
		// will return the DeliveryInfoVO object type. The DeliveryInfoVO should have t
		// Phoeix product ID,Delivery DATE,recipzipcode
		// UpdateOrderDAO updateOrderDAO = new UpdateOrderDAO(conn);
		PhoenixDAO phoenixDAO = new PhoenixDAO(conn);
		CachedResultSet phoenixEligibilityInfo = phoenixDAO
				.getOrderPhoenixEligibility(orderDetailId, "COM", null);

		DeliveryInfoVO vo = new DeliveryInfoVO();
		if (phoenixEligibilityInfo.next()) {
			if (phoenixEligibilityInfo.getString("V_PHOENIX_PRODUCT_ID") != null) {
				vo.setProductId(phoenixEligibilityInfo.getString(
						"V_PHOENIX_PRODUCT_ID").trim());
			}
			if (phoenixEligibilityInfo.getString("v_delivery_date") != null) {
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
				Date deliveryDate = formatter.parse(phoenixEligibilityInfo
						.getString("v_delivery_date").trim());
				Calendar cal = Calendar.getInstance();
				cal.setTime(deliveryDate);
				vo.setDeliveryDate(cal);
			}
			if (phoenixEligibilityInfo.getString("v_recip_zip_code") != null) {
				vo.setRecipientZipCode(phoenixEligibilityInfo.getString(
						"v_recip_zip_code").trim());
			}
			if (phoenixEligibilityInfo.getString("v_orig_new_prod_same") != null) {
				vo.setOrigNewProdSame(phoenixEligibilityInfo.getString(
						"v_orig_new_prod_same").trim());
			}
			if (phoenixEligibilityInfo.getString("v_bear_addon_id") != null) {
				vo.setBearAddonId(phoenixEligibilityInfo
						.getString("v_bear_addon_id"));
			}
			if (phoenixEligibilityInfo.getString("v_choc_addon_id") != null) {
				vo.setChocolateAddonId(phoenixEligibilityInfo
						.getString("v_choc_addon_id"));
			}
			if (phoenixEligibilityInfo.getString("v_source_code") != null) {
				vo.setSourceCode(phoenixEligibilityInfo
						.getString("v_source_code"));
			}
		}
		return vo;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public HashMap<String, String> getDeliveryDatesFromPAS(String productId,
			String zipcode, String addons, String sourceCode) throws Exception {

		CachedResultSet rs;
		int delivery_days_out_max = 1; // default to one
		HashMap<String, String> deliveryDates = new HashMap<String, String>(); 

		// Pull the global params for phoenix 
		UpdateOrderDAO uoDAO = new UpdateOrderDAO(conn);

		rs = (CachedResultSet) uoDAO.getGlobalParmByIdRS("PHOENIX");

		while (rs.next()) {
			String name = (String) rs.getObject("name");
			if (name.equalsIgnoreCase("DELIVERY_DAYS_OUT_MAX")) {
				String delivery_days = (String) rs.getObject("value");
				delivery_days_out_max = delivery_days_out_max
						+ new Integer(delivery_days).intValue();
				break;
			}
		}

		List addonIds = new ArrayList();
		if (addons != null && addons.length() > 0) {
			addonIds = Arrays.asList(addons.split(","));
		}

		PasShipDatesAvailVO pasShipDates = PASServiceUtil
				.getAvailableShipDates(productId, addonIds, zipcode,
						sourceCode, delivery_days_out_max);
		Set<String> calDates = PASServiceUtil.getPASShipDates(pasShipDates);

		Iterator<String> delIter = calDates.iterator();
		while (delIter.hasNext()) {
			String date = delIter.next();
			deliveryDates.put(date, date);
		}

		HashMap<String, String> deliveryDatesMap = sortHashMap(deliveryDates);
		return deliveryDatesMap;
	}

	public boolean isLiveFTD(String orderDetailId) throws Exception {

		UpdateOrderDAO uoDAO = new UpdateOrderDAO(conn);

		// Call getqueueretrieveLockXML method in the DAO
		boolean hasLiveFTD = uoDAO.hasLiveMercury(orderDetailId);

		return hasLiveFTD;
	}

	private HashMap<String, String> sortHashMap(HashMap<String, String> input) {
		Map<String, String> tempMap = new HashMap<String, String>();
		for (String wsState : input.keySet()) {
			tempMap.put(wsState, input.get(wsState));
		}

		List<String> mapKeys = new ArrayList<String>(tempMap.keySet());
		List<String> mapValues = new ArrayList<String>(tempMap.values());
		HashMap<String, String> sortedMap = new LinkedHashMap<String, String>();
		TreeSet<String> sortedSet = new TreeSet<String>(mapValues);
		Object[] sortedArray = sortedSet.toArray();
		int size = sortedArray.length;
		for (int i = 0; i < size; i++) {
			sortedMap.put(mapKeys.get(mapValues.indexOf(sortedArray[i])),
					(String) sortedArray[i]);
		}
		return sortedMap;
	}

	public boolean isPhoenixOrder(String externalOrderNumber) {
		boolean isPhoenixOrder = false;
		UpdateOrderDAO phoenixDao = new UpdateOrderDAO(conn);
		try {
			isPhoenixOrder = phoenixDao.getPhoenixOrder(externalOrderNumber);
		} catch (Exception e) {
			logger.error("Error checking if order is phoenix or not");
		}
		return isPhoenixOrder;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Map<String, String> getShipMethodNDate(String phoenixProductId,
			Date deliveryDate, String zipCode, String addons, String sourceCode)
			throws Exception {
		Map<String, String> returnMap = null;
		try {			 
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			SimpleDateFormat requestFormat = new SimpleDateFormat("MM/dd/yyyy");
			// start Testing code
			/*
			 * Calendar c = Calendar.getInstance(); // starts with today's date
			 * and time c.add(Calendar.DAY_OF_YEAR, 2); // advances day by 2
			 * deliveryDate = c.getTime();
			 */

			// end testing code
			List addonIds = new ArrayList();
			if (addons != null && addons.length() > 0) {
				addonIds = Arrays.asList(addons.split(","));
			}

			// ProductAvailVO paVO = paBO.getProductAvailability(conn,
			// phoenixProductId, deliveryDate, zipCode, addonIds,sourceCode);
			ProductAvailVO paVO = PASServiceUtil.getProductAvailShipData(
					phoenixProductId, deliveryDate, zipCode, null, null, null,
					addonIds, sourceCode);
			logger.info("available: " + paVO.isIsAvailable() + " "
					+ paVO.isIsAddOnAvailable());
			
			if (paVO.isIsAvailable() && paVO.isIsAddOnAvailable()) {
				returnMap = new HashMap<String, String>();
				if (paVO.getShipMethod() != null) {
					returnMap.put(COMConstants.SHIP_METHOD,
							paVO.getShipMethod());
					returnMap.put(COMConstants.SHIP_DATE,
							sdf.format(requestFormat.parse(paVO.getShipDate())));
				} else {
					return null;
				}
			}

		} catch (Exception e) {
			logger.error("Error occured while connecting to PAS service : "+e);
			throw new Exception("Could not find a Ship Method");
		}

		return returnMap;
	}

	public void savePhoenixOrder(String orderDetailId, Date deliveryDate,
			String cancelText, String shipMethod, String shipDate,
			String csrId, String emailTypeId, String emailBody,
			String emailSubject, String orderComments, String phoenixProductId,
			String emailTitle, String startOrigin) throws Exception {
		StringBuffer sb = new StringBuffer();

		// Check to see if Phoenix is enabled

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

		boolean orderIsPhoenixEligible = false;
		String sendToQueue = "null";
		String isOrderPhoenixEligible = null;
		String bearAddonId = null;
		String chocAddonId = null; 
		String recipZipCode = null;
		String origNewProdSame = null; 
		String phoenixProductIdList = "";

		// If Phoenix is enabled then check to see if the order is Phoenix
		// eligible
		PhoenixDAO phoenixDAO = new PhoenixDAO(conn);
		CachedResultSet phoenixEligibilityInfo = phoenixDAO
				.getOrderPhoenixEligibility(orderDetailId, "COM", null);

		if (phoenixEligibilityInfo.next()) {
			
			if (phoenixEligibilityInfo.getString("v_order_phoenix_eligible") != null)
				isOrderPhoenixEligible = phoenixEligibilityInfo.getString("v_order_phoenix_eligible").trim();
			
			if (phoenixEligibilityInfo.getString("v_bear_addon_id") != null)
				bearAddonId = phoenixEligibilityInfo.getString("v_bear_addon_id").trim();
			
			if (phoenixEligibilityInfo.getString("v_choc_addon_id") != null)
				chocAddonId = phoenixEligibilityInfo.getString("v_choc_addon_id").trim();
			
			/*if (phoenixEligibilityInfo.getString("v_delivery_date") != null)
				orderDeliveryDate = phoenixEligibilityInfo.getString("v_delivery_date").trim();*/
			
			if (phoenixEligibilityInfo.getString("v_recip_zip_code") != null)
				recipZipCode = phoenixEligibilityInfo.getString("v_recip_zip_code").trim();
			
			if (phoenixEligibilityInfo.getString("v_orig_new_prod_same") != null)
				origNewProdSame = phoenixEligibilityInfo.getString("v_orig_new_prod_same").trim();
			
			if (phoenixEligibilityInfo.getString("v_phoenix_product_id") != null) {
				phoenixProductIdList = phoenixEligibilityInfo.getString("v_phoenix_product_id").trim();
				if (!phoenixProductIdList.contains(phoenixProductId)) {
					isOrderPhoenixEligible = "N";
				}
			}
		}
		if (isOrderPhoenixEligible.equalsIgnoreCase("Y")) {
			orderIsPhoenixEligible = true;
		}

		if (orderIsPhoenixEligible) {
			// If order is Phoenix eligible then send it to the Phoenix process
			// msg format = order detail id|origin|mercury order number|phoenix
			// product id|bear addon id|chocolate addon id|order delivery
			// date|recip zip code|orig new prod same flag|cancel original FTD
			// message|mercury id|ship method|ship date
			logger.info(orderDetailId + ": Order is Phoenix eligible");

			sb.append(orderDetailId);
			sb.append("|");
			sb.append("COM");
			sb.append("|");
			sb.append("null");
			sb.append("|");
			sb.append(phoenixProductId);
			sb.append("|");
			if (StringUtils.isEmpty(bearAddonId)) {
				sb.append("null");
			} else {
				sb.append(bearAddonId);
			}
			sb.append("|");
			if (StringUtils.isEmpty(chocAddonId)) {
				sb.append("null");
			} else {
				sb.append(chocAddonId);
			}
			sb.append("|");
			if (deliveryDate == null) {
				sb.append("null");
			} else {
				sb.append(formatter.format(deliveryDate));
			}
			sb.append("|");
			sb.append(recipZipCode);
			sb.append("|");
			sb.append(origNewProdSame);
			sb.append("|");
			if (StringUtils.isEmpty(cancelText)) {
				sb.append("null");
			} else {
				sb.append(cancelText);
			}
			sb.append("|");
			sb.append("null");
			sb.append("|");
			if (!StringUtils.isEmpty(shipMethod)) {
				sb.append(shipMethod);
			} else {
				sb.append("null");
			}
			sb.append("|");
			if (!StringUtils.isEmpty(shipDate)) {
				sb.append(shipDate);
			} else {
				sb.append("null");
			}
			sb.append("|");
			sb.append(csrId);
			sb.append("|");
			if (StringUtils.isEmpty(orderComments)) {
				sb.append("null");
			} else {
				sb.append(orderComments);
			}
			sb.append("|");
			if (StringUtils.isEmpty(emailBody)) {
				sb.append("null");
			} else {
				sb.append(emailBody);
			}
			sb.append("|");
			if (StringUtils.isEmpty(emailSubject)) {
				sb.append("null");
			} else {
				sb.append(emailSubject);
			}
			sb.append("|");
			if (StringUtils.isEmpty(emailTypeId)) {
				sb.append("null");
			} else {
				sb.append(emailTypeId);
			}
			sb.append("|");
			sb.append(sendToQueue); // sendToQueue

			sb.append("|");
			sb.append(emailTitle); // emailTitle

			sb.append("|");
			if (StringUtils.isEmpty(startOrigin)) {
				sb.append("null");
			} else {
				sb.append(startOrigin);
			}
			sb.append("|");
			sb.append("null"); // bulk

			String msg = sb.toString();
			logger.info("msg: " + msg);
			Dispatcher dispatcher = Dispatcher.getInstance();
			MessageToken token = new MessageToken();
			token.setMessage(msg);
			token.setJMSCorrelationID("ProcessDetail");
			token.setStatus("PHOENIX");
			dispatcher.dispatchTextMessage(new InitialContext(), token);
		}

	}

	@SuppressWarnings("rawtypes")
	public Map getPhoenixEmailTypes() throws Exception {
		PhoenixDAO phoenixDAO = new PhoenixDAO(conn);
		return phoenixDAO.getPhoenixEmailTypes();
	}

}
