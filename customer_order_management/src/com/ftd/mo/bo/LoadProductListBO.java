package com.ftd.mo.bo;
import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.mo.dao.UpdateOrderDAO;
import com.ftd.customerordermanagement.dao.ViewDAO;
import com.ftd.customerordermanagement.util.BasePageBuilder;
import com.ftd.mo.util.SearchUtil;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.XMLFormat;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.security.SecurityManager;
import com.ftd.security.cache.vo.UserInfo;
import java.util.Enumeration;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import com.ftd.mo.util.BreadcrumbUtil;
import javax.servlet.ServletContext;

import java.sql.Connection;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;

import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;


import com.ftd.mo.vo.*;


public class LoadProductListBO
{

/*******************************************************************************************
 * Class level variables
 *******************************************************************************************/
  private HttpServletRequest  request             = null;
  private Connection          con                 = null;
  private static Logger       logger              = new Logger("com.ftd.mo.bo.LoadProductListBO");


	//request parameters required to be sent back to XSL all the time.
  private String              buyerName						= null;
  private String              categoryIndex				= null;
  private String              companyId           = null;
  private String              customerId          = null;
  private String              deliveryDate        = "";
  private String              deliveryDateRange   = "";
  private String              externalOrderNumber	= null;
  private String              masterOrderNumber		= null;
  private String              occasion  					= null;
  private String              occasionText				= null;
  private String              orderDetailId				= null;
  private String              orderGuid						= null;
  private String              originId            = null;
  private	String							origProductAmount		= null;
  private	String							origProductId				= null;
  private String              pageNumber					= null;
  private String              pricePointId				= null;
  private String              recipientCity       = null;
  private String              recipientCountry    = null;
  private String              recipientState      = null;
  private String              recipientZipCode    = null;
  private String              rewardType          = null;
  private String              shipMethod          = null;
  private String              sourceCode					= null;

	//other variables needed for this class
  private ServletContext      context             = null;
  private String              csrId               = null;
  private boolean             domesticFlag		  	= true;
  private String              domesticSvcFee			= null;
  private String              domIntFlag          = null;
  private String              imageLocation				= null;
  private String              internationalSvcFee	= null;
  private HashMap             hashXML             = new HashMap();
  private HashMap             pageData            = new HashMap();
  private String              partnerId						= null;
  private String              pricingCode					= null;
  private String              sessionId           = null;
  private String              snhId								= null;
  private String              sortType            = null;
  private SearchUtil          sUtil;
  private String              totalPages          = null;

  // Standard, Deluxe, Premium labels
  private String              standardLabel       = null;
  private String              deluxeLabel         = null;
  private String              premiumLabel        = null;


/*******************************************************************************************
 * Constructor 1
 *******************************************************************************************/
  public LoadProductListBO()
  {
  }


/*******************************************************************************************
 * Constructor 2
 *******************************************************************************************/
  public LoadProductListBO(HttpServletRequest request, Connection con, ServletContext context)
  {
    this.request = request;
    this.con = con;
    this.context = context;
  }


/*******************************************************************************************
  * processRequest()
  ******************************************************************************************
  *
  * @param  String - action
  * @return HashMap - contains 0 - many XML documents of data, 1 HashMap each for page
  *                   details and search criteria
  * @throws
  */
  public HashMap processRequest() throws Exception
  {
    //HashMap that will be returned to the calling class
    HashMap returnHash = new HashMap();

    //Get info from the request object
    getRequestInfo(this.request);

    //Get config file info
    getConfigInfo();

    //instantiate the search util
    this.sUtil = new SearchUtil(this.con);

    //find if the recipient recipientCountry was domestic or not
    this.domesticFlag = this.sUtil.isDomestic(this.recipientCountry);
    if (this.domesticFlag)
      this.domIntFlag = COMConstants.CONS_DELIVERY_TYPE_DOMESTIC;
    else
      this.domIntFlag = COMConstants.CONS_DELIVERY_TYPE_INTERNATIONAL;

    //process the action
    processMain();

    //populate the remainder of the fields on the page data
    populatePageData();

    //process Breadcrumb
    processBreadcrumbs();

    //populate the sub header info
    populateSubHeaderInfo();

    //At this point, we should have three HashMaps.
    // HashMap1 = hashXML         - hashmap that contains zero to many Documents
    // HashMap2 = pageData        - hashmap that contains String data at page level
    //
    //Combine all of the above HashMaps into one HashMap, called returnHash

    //retrieve all the Documents from hashXML and put them in returnHash
    //retrieve the keyset
    Set ks1 = hashXML.keySet();
    Iterator iter = ks1.iterator();
    String key = null;
    Document doc = null;

    //Iterate thru the keyset
    while(iter.hasNext())
    {
      //get the key
      key = iter.next().toString();

      //retrieve the XML document
      doc = (Document) hashXML.get(key);

      //put the XML object in the returnHash
      returnHash.put(key, (Document) doc);
    }

    //append pageData to returnHash
    returnHash.put("pageData",        (HashMap)this.pageData);

    return returnHash;
  }



/*******************************************************************************************
  * getRequestInfo(HttpServletRequest request)
  ******************************************************************************************
  * Retrieve the info from the request object
  *
  * @param  HttpServletRequest - request
  * @return none
  * @throws none
  */
  private void getRequestInfo(HttpServletRequest request) throws Exception
  {

    /****************************************************************************************
     *  Retrieve order/search specific parameters
     ***************************************************************************************/
    //retrieve the buyer_full_name
    if(request.getParameter(COMConstants.BUYER_FULL_NAME)!=null)
    {
      this.buyerName = request.getParameter(COMConstants.BUYER_FULL_NAME);
    }

    //retrieve the category Index
    if(request.getParameter(COMConstants.CATEGORY_INDEX)!=null)
    {
      this.categoryIndex = request.getParameter(COMConstants.CATEGORY_INDEX);
    }

    //retrieve the company id
    if(request.getParameter(COMConstants.COMPANY_ID)!=null)
    {
      this.companyId = request.getParameter(COMConstants.COMPANY_ID);
    }

    //retrieve the customer id
    if(request.getParameter(COMConstants.CUSTOMER_ID)!=null)
    {
      this.customerId = request.getParameter(COMConstants.CUSTOMER_ID);
    }

    //retrieve the delivery date
    if(request.getParameter(COMConstants.DELIVERY_DATE)!=null)
    {
      this.deliveryDate = request.getParameter(COMConstants.DELIVERY_DATE);
    }

    //retrieve the delivery date range end
    if(request.getParameter(COMConstants.DELIVERY_DATE_RANGE_END)!=null)
    {
      this.deliveryDateRange = request.getParameter(COMConstants.DELIVERY_DATE_RANGE_END);
    }

    //retrieve the item order number
    if(request.getParameter(COMConstants.EXTERNAL_ORDER_NUMBER)!=null)
    {
      this.externalOrderNumber = request.getParameter(COMConstants.EXTERNAL_ORDER_NUMBER);
    }

    //retrieve the master order number
    if(request.getParameter(COMConstants.MASTER_ORDER_NUMBER)!=null)
    {
      this.masterOrderNumber = request.getParameter(COMConstants.MASTER_ORDER_NUMBER);
    }

    //retrieve the occasion id
    if(request.getParameter(COMConstants.OCCASION)!=null)
    {
      this.occasion = request.getParameter(COMConstants.OCCASION);
    }

    //retrieve the occasion text
    if(request.getParameter(COMConstants.OCCASION_TEXT)!=null)
    {
      this.occasionText = request.getParameter(COMConstants.OCCASION_TEXT);
    }

    //retrieve the orig product amount
    if(request.getParameter(COMConstants.ORIG_PRODUCT_AMOUNT)!=null)
    {
      this.origProductAmount = request.getParameter(COMConstants.ORIG_PRODUCT_AMOUNT);
    }

    //retrieve the orig product id
    if(request.getParameter(COMConstants.ORIG_PRODUCT_ID)!=null)
    {
      this.origProductId = (request.getParameter(COMConstants.ORIG_PRODUCT_ID)).toUpperCase();
    }

    //retrieve the order detail id
    if(request.getParameter(COMConstants.ORDER_DETAIL_ID)!=null)
    {
      this.orderDetailId = request.getParameter(COMConstants.ORDER_DETAIL_ID);
    }

    //retrieve the order guid
    if(request.getParameter(COMConstants.ORDER_GUID)!=null)
    {
      this.orderGuid = request.getParameter(COMConstants.ORDER_GUID);
    }

    //retrieve the origin id
    if(request.getParameter(COMConstants.ORIGIN_ID)!=null)
    {
      this.originId = request.getParameter(COMConstants.ORIGIN_ID);
    }

    //retrieve the page number
    if(request.getParameter(COMConstants.PAGE_NUMBER)!=null)
    {
      this.pageNumber = request.getParameter(COMConstants.PAGE_NUMBER);
    }

    //retrieve the partnerId
    if(request.getParameter(COMConstants.PARTNER_ID)!=null)
    {
      this.partnerId = request.getParameter(COMConstants.PARTNER_ID);
    }

    //retrieve the price point id
    if(request.getParameter(COMConstants.PRICE_POINT_ID)!=null)
    {
      this.pricePointId = request.getParameter(COMConstants.PRICE_POINT_ID);
    }

    //retrieve the pricing Code
    if(request.getParameter(COMConstants.PRICING_CODE)!=null)
    {
      this.pricingCode = request.getParameter(COMConstants.PRICING_CODE);
    }

    //retrieve the city
    if(request.getParameter(COMConstants.RECIPIENT_CITY)!=null)
    {
      this.recipientCity = request.getParameter(COMConstants.RECIPIENT_CITY);
    }

    //retrieve the recipientCountry id
    if(request.getParameter(COMConstants.RECIPIENT_COUNTRY)!=null)
    {
      this.recipientCountry = request.getParameter(COMConstants.RECIPIENT_COUNTRY);
    }

    //retrieve the recipientState
    if(request.getParameter(COMConstants.RECIPIENT_STATE)!=null)
    {
      this.recipientState = request.getParameter(COMConstants.RECIPIENT_STATE);
    }

    //retrieve the postal code
    if(request.getParameter(COMConstants.RECIPIENT_ZIP_CODE)!=null)
    {
      this.recipientZipCode = request.getParameter(COMConstants.RECIPIENT_ZIP_CODE);
    }

    //retrieve the reward type
    if(request.getAttribute(COMConstants.REWARD_TYPE)!=null)
    {
      this.rewardType = request.getAttribute(COMConstants.REWARD_TYPE).toString();
    }
    if((this.rewardType == null || this.rewardType.equalsIgnoreCase("")) && request.getParameter(COMConstants.REWARD_TYPE)!=null)
    {
      this.rewardType = request.getParameter(COMConstants.REWARD_TYPE);
    }

    //retrieve the ship method
    if(request.getParameter(COMConstants.SHIP_METHOD)!=null)
    {
      this.shipMethod = request.getParameter(COMConstants.SHIP_METHOD);
    }

    //retrieve the snh id
    if(request.getParameter(COMConstants.SNH_ID)!=null)
    {
      this.snhId = request.getParameter(COMConstants.SNH_ID);
    }

    //retrieve the sort type
    if(request.getParameter(COMConstants.SORT_TYPE)!=null)
    {
      this.sortType = request.getParameter(COMConstants.SORT_TYPE);
    }

    //retrieve the source code
    if(request.getParameter(COMConstants.SOURCE_CODE)!=null)
    {
      this.sourceCode = request.getParameter(COMConstants.SOURCE_CODE);
    }

    /****************************************************************************************
     *  Retrieve Security Info
     ***************************************************************************************/
    this.sessionId   = request.getParameter(COMConstants.SEC_TOKEN);

  }


/*******************************************************************************************
  * getConfigInfo()
  ******************************************************************************************
  * Retrieve the info from the configuration file
  *
  * @param none
  * @return
  * @throws none
  */
  private void getConfigInfo() throws Exception
  {

    ConfigurationUtil cu = null;
    cu = ConfigurationUtil.getInstance();

    //get csr Id
    String security = (String) cu.getProperty(COMConstants.PROPERTY_FILE,COMConstants.SECURITY_IS_ON);
    boolean securityIsOn = security.equalsIgnoreCase("true") ? true : false;

   //delete this before going to production.  the following should only check on securityIsOn
    if(securityIsOn || this.sessionId != null)
    {
      SecurityManager sm = SecurityManager.getInstance();
      UserInfo ui = sm.getUserInfo(this.sessionId);
      this.csrId = ui.getUserID();
    }

    //get PRODUCT_IMAGE_LOCATION
    if (cu.getProperty(COMConstants.PROPERTY_FILE, COMConstants.CONS_PRODUCT_IMAGE_LOCATION) != null)
    {
      this.imageLocation  = cu.getProperty(COMConstants.PROPERTY_FILE, COMConstants.CONS_PRODUCT_IMAGE_LOCATION);
      this.pageData.put(COMConstants.PD_MO_PRODUCT_IMAGE_LOCATION, imageLocation);
    }

    //get Standard, Deluxe, Premium labels
    String tempLabel = cu.getFrpGlobalParm("FTDAPPS_PARMS", "FLORAL_LABEL_STANDARD");
    if (tempLabel != null) {
        this.standardLabel = tempLabel;
    }
    tempLabel = cu.getFrpGlobalParm("FTDAPPS_PARMS", "FLORAL_LABEL_DELUXE");
    if (tempLabel != null) {
        this.deluxeLabel = tempLabel;
    }
    tempLabel = cu.getFrpGlobalParm("FTDAPPS_PARMS", "FLORAL_LABEL_PREMIUM");
    if (tempLabel != null) {
        this.premiumLabel = tempLabel;
    }

  }



/*******************************************************************************************
  * populatePageData()
  ******************************************************************************************
  * Populate the page data with the remainder of the fields
  *
  * @param none
  * @return
  * @throws none
  */
  private void populatePageData()
  {

    this.pageData.put(COMConstants.PD_MO_BUYER_FULL_NAME,         this.buyerName);
    this.pageData.put(COMConstants.PD_MO_CATEGORY_INDEX,          this.categoryIndex);
    this.pageData.put(COMConstants.PD_MO_COMPANY_ID,              this.companyId);
    this.pageData.put(COMConstants.PD_MO_CUSTOMER_ID,             this.customerId);
    this.pageData.put(COMConstants.PD_MO_DELIVERY_DATE,           this.deliveryDate);
    this.pageData.put(COMConstants.PD_MO_DELIVERY_DATE_RANGE_END, this.deliveryDateRange);
    this.pageData.put(COMConstants.PD_MO_DOMESTIC_INT_FLAG,       this.domIntFlag);
    this.pageData.put(COMConstants.PD_MO_DOMESTIC_SRVC_FEE,       this.domesticSvcFee);
    this.pageData.put(COMConstants.PD_MO_EXTERNAL_ORDER_NUMBER,   this.externalOrderNumber);
    this.pageData.put(COMConstants.PD_MO_PRODUCT_IMAGE_LOCATION,  this.imageLocation);
    this.pageData.put(COMConstants.PD_MO_INTERNATIONAL_SRVC_FEE,  this.internationalSvcFee);
    this.pageData.put(COMConstants.PD_MO_MASTER_ORDER_NUMBER,     this.masterOrderNumber);
    this.pageData.put(COMConstants.PD_MO_OCCASION,                this.occasion);
    this.pageData.put(COMConstants.PD_MO_OCCASION_TEXT,           this.occasionText);
    this.pageData.put(COMConstants.PD_MO_ORDER_DETAIL_ID,         this.orderDetailId);
    this.pageData.put(COMConstants.PD_MO_ORDER_GUID,              this.orderGuid);
    this.pageData.put(COMConstants.PD_MO_ORIGIN_ID,               this.originId);
    this.pageData.put(COMConstants.PD_MO_ORIG_PRODUCT_AMOUNT,			this.origProductAmount);
    this.pageData.put(COMConstants.PD_MO_ORIG_PRODUCT_ID,					this.origProductId);
    this.pageData.put(COMConstants.PD_MO_PAGE_NUMBER,             this.pageNumber);
    this.pageData.put(COMConstants.PD_MO_PARTNER_ID,              this.partnerId);
    this.pageData.put(COMConstants.PD_MO_PRICE_POINT_ID,          this.pricePointId);
    this.pageData.put(COMConstants.PD_MO_PRICING_CODE,            this.pricingCode);
    this.pageData.put(COMConstants.PD_MO_RECIPIENT_CITY,          this.recipientCity);
    this.pageData.put(COMConstants.PD_MO_RECIPIENT_COUNTRY,       this.recipientCountry);
    this.pageData.put(COMConstants.PD_MO_RECIPIENT_STATE,         this.recipientState);
    this.pageData.put(COMConstants.PD_MO_RECIPIENT_ZIP_CODE,      this.recipientZipCode);
    this.pageData.put(COMConstants.PD_MO_REWARD_TYPE,             this.rewardType);
    this.pageData.put(COMConstants.PD_MO_SHIP_METHOD,             this.shipMethod);
    this.pageData.put(COMConstants.PD_MO_SORT_TYPE,               this.sortType);
    this.pageData.put(COMConstants.PD_MO_SOURCE_CODE,             this.sourceCode);
    this.pageData.put(COMConstants.PD_MO_TOTAL_PAGES,             this.totalPages);
    this.pageData.put(COMConstants.PD_MO_SNH_ID,                  this.snhId);

    this.pageData.put(COMConstants.PD_MO_FLORAL_LABEL_STANDARD, this.standardLabel);
    this.pageData.put(COMConstants.PD_MO_FLORAL_LABEL_DELUXE, this.deluxeLabel);
    this.pageData.put(COMConstants.PD_MO_FLORAL_LABEL_PREMIUM, this.premiumLabel);

  }


/*******************************************************************************************
  * populateSubHeaderInfo()
  ******************************************************************************************
  * Populate the page data with the remainder of the fields
  *
  * @param none
  * @return
  * @throws none
  */
  private void populateSubHeaderInfo() throws Exception
  {
    BasePageBuilder bpb = new BasePageBuilder();

    //xml document for sub header
    Document subHeaderXML = DOMUtil.getDocument();

    //retrieve the sub header info
    subHeaderXML = bpb.retrieveSubHeaderData(this.request);

    //save the original product xml
    this.hashXML.put(COMConstants.HK_SUB_HEADER, subHeaderXML);

  }


/*******************************************************************************************
  * processBreadcrumbs()
  ******************************************************************************************
  *
  *
  * @param  none
  * @return none
  * @throws none
  */
  private void processBreadcrumbs() throws Exception
  {
    HashMap optionalParms = new HashMap();
    optionalParms.put(COMConstants.BUYER_FULL_NAME,         				this.buyerName);
    optionalParms.put(COMConstants.CATEGORY_INDEX,          				this.categoryIndex);
    optionalParms.put(COMConstants.COMPANY_ID,              				this.companyId);
    optionalParms.put(COMConstants.CUSTOMER_ID,             				this.customerId);
    optionalParms.put(COMConstants.DELIVERY_DATE,           				this.deliveryDate);
    optionalParms.put(COMConstants.DELIVERY_DATE_RANGE_END, 				this.deliveryDateRange);
    optionalParms.put(COMConstants.EXTERNAL_ORDER_NUMBER,   				this.externalOrderNumber);
    optionalParms.put(COMConstants.MASTER_ORDER_NUMBER,     				this.masterOrderNumber);
    optionalParms.put(COMConstants.OCCASION,                 				this.occasion);
    optionalParms.put(COMConstants.OCCASION_TEXT,           				this.occasionText);
    optionalParms.put(COMConstants.ORIG_PRODUCT_AMOUNT,  						this.origProductAmount);
    optionalParms.put(COMConstants.ORIG_PRODUCT_ID,									this.origProductId);
    optionalParms.put(COMConstants.ORDER_DETAIL_ID,         				this.orderDetailId);
    optionalParms.put(COMConstants.ORDER_GUID,              				this.orderGuid);
    optionalParms.put(COMConstants.ORIGIN_ID,               				this.originId);
    optionalParms.put(COMConstants.PAGE_NUMBER,             				this.pageNumber);
    optionalParms.put(COMConstants.PRICE_POINT_ID,          				this.pricePointId);
    optionalParms.put(COMConstants.RECIPIENT_CITY,          				this.recipientCity);
    optionalParms.put(COMConstants.RECIPIENT_COUNTRY,       				this.recipientCountry);
    optionalParms.put(COMConstants.RECIPIENT_STATE,         				this.recipientState);
    optionalParms.put(COMConstants.RECIPIENT_ZIP_CODE,      				this.recipientZipCode);
    optionalParms.put(COMConstants.REWARD_TYPE,                     this.rewardType);
    optionalParms.put(COMConstants.SHIP_METHOD,                     this.shipMethod);
    optionalParms.put(COMConstants.SOURCE_CODE,             				this.sourceCode);

    BreadcrumbUtil bcUtil = new BreadcrumbUtil();

//bcUtil.getBreadcrumbXML(  request,
                          //Title of breadcrumb,
                          //Name of Structs Action,
                          //Action parameter,
                          //Error display page,
                          //Flag indicating if breadcrumb should be a link, ,
                          //Optional hash of additional name/value parameters for this page
                      //)

    Document bcXML = bcUtil.getBreadcrumbXML(this.request, COMConstants.BCT_PRODUCT_LIST,
                            COMConstants.BCA_PRODUCT_LIST, null, "Error",
                            "true", optionalParms, this.orderDetailId);
    this.hashXML.put("breadcrumbs",  bcXML);

  }




/*******************************************************************************************
  * processLoadPage()
  ******************************************************************************************
  * process the action type of "load".  This method implements the initial page load-up.
  *
  * @param  none
  * @return none
  * @throws none
  */
  private void processMain() throws Exception
  {

    ArrayList promotionList = new ArrayList();
    ProductFlagsVO flagsVO = new ProductFlagsVO();

    HashMap productMap = new HashMap();

    if (this.categoryIndex == null)
    {
      this.categoryIndex = "";
    }

    //get the source code info
    retrieveProductBySourceCodeInfo();

    //get the fee info
    retrieveFeeInfo();

    Document productCategoryXML = null;


    productCategoryXML = this.sUtil.getProductsByCategory(this.pageNumber, this.categoryIndex,
                            this.recipientZipCode, this.domesticFlag, this.sourceCode,
                            this.pricePointId, this.recipientCountry, this.domesticSvcFee,
                            this.internationalSvcFee, this.recipientState, this.pricingCode,
                            this.partnerId, promotionList, this.rewardType, flagsVO,
                            this.sortType, this.buyerName, this.origProductId, this.shipMethod,
                            productMap, this.context, this.deliveryDate);

    //append addons
    productCategoryXML = this.sUtil.appendAddons(this.request, productCategoryXML);

    this.hashXML.put(COMConstants.HK_PRODUCT_CATEGORY_XML,    productCategoryXML);

    this.pageData.put("XSL", COMConstants.XSL_UPDATE_PRODUCT_LIST);//

    Set ks = productMap.keySet();
    Iterator iter = ks.iterator();
    String key;
    //Iterate thru the hashmap returned from the business object using the keyset
    while(iter.hasNext())
    {
      key = iter.next().toString();
      this.pageData.put(key, (String)productMap.get(key));
    }

    this.pageNumber = this.pageData.get("page_number")!=null?this.pageData.get("page_number").toString():this.pageNumber;
    this.totalPages = this.pageData.get("total_pages")!=null?this.pageData.get("total_pages").toString():this.totalPages;

    this.hashXML.put(COMConstants.HK_CSCI_VIEWING_XML, retrieveCSRViewing());
  }


/*******************************************************************************************
 * retrieveProductBySourceCodeInfo() (COM_SOURCE_CODE_RECORD_LOOKUP)
 *******************************************************************************************
  * This method is used to call the
  *
  */
  private void retrieveProductBySourceCodeInfo() throws Exception
  {
    BasePageBuilder bpb = new BasePageBuilder();

    //Call getCSRViewed method in the DAO
    Document sourceCodeInfo = (Document) bpb.retrieveProductBySourceCodeInfo(this.con, this.sourceCode);

    //create an array.
    ArrayList aList = new ArrayList();

    //Initialize the array with the top and bottom nodes of the XML, and the node for which
    //the value must be retrieved. In this instance, we will retireve the value of
    //customer id
    aList.clear();
    aList.add(0,"PRODUCTS");
    aList.add(1,"PRODUCT");
    aList.add(2,"shipping_code");
    String shippingCode = DOMUtil.getNodeValue(sourceCodeInfo, aList);
    this.snhId = shippingCode;

    //Initialize the array with the top and bottom nodes of the XML, and the node for which
    //the value must be retrieved. In this instance, we will retireve the value of
    //customer id
    aList.clear();
    aList.add(0,"PRODUCTS");
    aList.add(1,"PRODUCT");
    aList.add(2,"pricing_code");
    String priceCode = DOMUtil.getNodeValue(sourceCodeInfo, aList);
    this.pricingCode = priceCode;

    //Initialize the array with the top and bottom nodes of the XML, and the node for which
    //the value must be retrieved. In this instance, we will retireve the value of
    //customer id
    aList.clear();
    aList.add(0,"PRODUCTS");
    aList.add(1,"PRODUCT");
    aList.add(2,"partner_id");
    String ptnrId = DOMUtil.getNodeValue(sourceCodeInfo, aList);
    this.partnerId = ptnrId;

    this.hashXML.put(COMConstants.HK_SOURCE_CODE_XML,  sourceCodeInfo);

  }


/*******************************************************************************************
 * retrieveFeeInfo() (COM_SNH_BY_ID)
 *******************************************************************************************
  * This method is used to call the
  *
  */
  private void retrieveFeeInfo() throws Exception
  {
    BasePageBuilder bpb = new BasePageBuilder();

    //Call getCSRViewed method in the DAO
    Document feeInfo = (Document) bpb.retrieveFeeInfo(this.con, this.snhId, this.deliveryDate);

    //create an array.
    ArrayList aList = new ArrayList();

    //Initialize the array with the top and bottom nodes of the XML, and the node for which
    //the value must be retrieved. In this instance, we will retireve the value of
    //customer id
    aList.clear();
    aList.add(0,"FEES");
    aList.add(1,"FEE");
    aList.add(2,"first_order_domestic");
    String domesticFee = DOMUtil.getNodeValue(feeInfo, aList);
    this.domesticSvcFee = domesticFee;

    //Initialize the array with the top and bottom nodes of the XML, and the node for which
    //the value must be retrieved. In this instance, we will retireve the value of
    //customer id
    aList.clear();
    aList.add(0,"FEES");
    aList.add(1,"FEE");
    aList.add(2,"first_order_international");
    String internationalFee = DOMUtil.getNodeValue(feeInfo, aList);
    this.internationalSvcFee = internationalFee;

  }


/*******************************************************************************************
 * buildXML()
 *******************************************************************************************/
  private Document buildXML(HashMap outMap, String cursorName, String topName, String bottomName, String type)
        throws Exception
  {

    //Create a CachedResultSet object using the cursor name that was passed.
    CachedResultSet rs = (CachedResultSet) outMap.get(cursorName);

    Document doc =  null;

    //Create an XMLFormat, and initialize the top and bottom node.  Note that since this method
    //can be/is called by various other methods, the top and botton names MUST be passed to it.
    XMLFormat xmlFormat = new XMLFormat();
    xmlFormat.setAttribute("type", type);
    xmlFormat.setAttribute("top", topName);
    xmlFormat.setAttribute("bottom", bottomName );

    //call the DOMUtil's converToXMLDOM method
    doc = (Document) DOMUtil.convertToXMLDOM(rs, xmlFormat);

    //return the xml document.
    return doc;

  }


  /*
   * Returns all CSR's currently viewing this order detail id.
   * @return Document containing all CSR's currently viewing this order
   * @throws java.lang.Exception
   */
  private Document retrieveCSRViewing()
      throws Exception {
    //Instantiate ViewDAO
    ViewDAO viewDAO = new ViewDAO(this.con);

    //Document that will contain the output from the stored proce
    Document csrViewingInfo = DOMUtil.getDocument();

    //Call getCSRViewing method in the DAO
    csrViewingInfo = viewDAO.getCSRViewing(this.sessionId, this.csrId, COMConstants.CONS_ENTITY_TYPE_ORDER_DETAILS, this.orderDetailId);

    return csrViewingInfo;
  }
}