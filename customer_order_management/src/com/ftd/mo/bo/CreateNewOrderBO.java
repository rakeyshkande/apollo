package com.ftd.mo.bo;

import java.math.BigDecimal;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.naming.InitialContext;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.UserTransaction;

import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.ftd.customerordermanagement.bo.GiftCardBO;
import com.ftd.customerordermanagement.bo.RefundBO;
import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.dao.CommentDAO;
import com.ftd.customerordermanagement.dao.LockDAO;
import com.ftd.customerordermanagement.dao.OrderDAO;
import com.ftd.customerordermanagement.dao.PaymentDAO;
import com.ftd.customerordermanagement.dao.RefundDAO;
import com.ftd.customerordermanagement.util.COMDeliveryDateUtil;
import com.ftd.customerordermanagement.vo.AddOnVO;
import com.ftd.customerordermanagement.vo.CommentsVO;
import com.ftd.customerordermanagement.vo.CreditCardVO;
import com.ftd.customerordermanagement.vo.OrderBillVO;
import com.ftd.customerordermanagement.vo.PaymentVO;
import com.ftd.customerordermanagement.vo.RefundVO;
import com.ftd.decisionresult.filter.DecisionResultDataFilter;
import com.ftd.messaging.constants.MessagingConstants;
import com.ftd.messaging.util.MessageUtil;
import com.ftd.messaging.util.MessagingServiceLocator;
import com.ftd.messaging.vo.MessageVO;
import com.ftd.mo.dao.UpdateOrderDAO;
import com.ftd.mo.exceptions.GiftCardException;
import com.ftd.mo.util.DomainObjectConverter;
import com.ftd.mo.vo.DeliveryInfoVO;
import com.ftd.op.common.to.ResultTO;
import com.ftd.op.mercury.to.ADJMessageTO;
import com.ftd.op.mercury.to.BaseMercuryMessageTO;
import com.ftd.op.mercury.to.CANMessageTO;
import com.ftd.op.venus.to.AdjustmentMessageTO;
import com.ftd.op.venus.to.CancelOrderTO;
import com.ftd.osp.framework.dispatcher.Dispatcher;
import com.ftd.osp.ordervalidator.util.DataMassager;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.GUID.GUIDGenerator;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.order.OrderIdGenerator;
import com.ftd.osp.utilities.order.RecalculateOrderBO;
import com.ftd.osp.utilities.order.ScrubMapperDAO;
import com.ftd.osp.utilities.order.vo.BuyerVO;
import com.ftd.osp.utilities.order.vo.CoBrandVO;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.OrderExtensionsVO;
import com.ftd.osp.utilities.order.vo.OrderStatus;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.order.vo.RecipientsVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;
import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.security.SecurityManager;
import com.ftd.security.cache.vo.UserInfo;

/**
 * This class is responsible for creating a new order and injecting it
 * into the scrub database for processing.
 */
public class CreateNewOrderBO 
{
  private Connection connection;
  private HttpServletRequest request;
  private static Logger logger  = new Logger("com.ftd.mo.bo.CreateNewOrderBO");
  
  protected OrderIdGenerator orderIdGenerator = null;
  
  private static final String DISPATCH_STATUS = "ES1005";  
  private static final String NEWLINE_CHAR_CODE = "\n";
  
  protected DeliveryInfoVO originalDeliveryInfoVO = new DeliveryInfoVO();
  protected DeliveryInfoVO tempDeliveryInfoVO = new DeliveryInfoVO();      
  protected OrderBillVO originalOrderBillVO = new OrderBillVO();
  protected OrderBillVO tempOrderBillVO = new OrderBillVO();  
  protected HashMap originalOrderAddOnVO = new HashMap(); //key = addon-code (eg. RC73)
  protected HashMap tempOrderAddOnVO = new HashMap(); //key = addon-code (eg. RC73)
  private List tempCommentList = new ArrayList();
  private List tempCoBrandList = new ArrayList();
  private List tempPaymentList = new ArrayList();
  private Map tempCreditCardMap = new HashMap();
  //private OrderDetailVO originalOrderDetail = null;
  private List<RefundVO> refundsIssuedList;
  private OrderVO newScrubOrder = null;
  private boolean isA40 = false;
  private boolean isB40 = false;
  
  private static final String CANCEL_MSG_COMMENT = "Please cancel this order.  Thank you.";
  private static final String ADJ_MSG_COMMENT = "Adjustment created.";
  private static final String COMMENT_DELIMITER = "<br>";
  
  private static final String RECIP_NAME ="recip_name"; 
  private static final String RECIP_BUSINESS ="recip_business"; 
  private static final String RECIP_ADDRESS ="recip_address"; 
  private static final String RECIP_DELIVERY_LOCATION ="recip_delivery_location"; 
  private static final String RECIP_CITY ="recip_city"; 
  private static final String RECIP_STATE ="recip_state"; 
  private static final String RECIP_ZIPCODE ="recip_zipcode"; 
  private static final String RECIP_COUNTRY ="recip_country"; 
  private static final String RECIP_PHONE ="recip_phone"; 
  private static final String DELIVERY_DATE ="delivery_date"; 
  private static final String CARD_MESSAGE ="card_message"; 
  private static final String SIGNATURE ="signature"; 
  private static final String SPECIAL_INSTRUCTIONS ="special_instructions"; 
  private static final String ALT_CONTACT_INFO ="alt_contact_info"; 
  private static final String PRODUCT ="product"; 
  private static final String PRODUCT_PRICE ="product_price"; 
  private static final String SHIP_TYPE_CHANGED ="ship_type_changed"; 
  private static final String COLOR_FIRST_CHOICE ="color_first_choice"; 
  private static final String COLOR_SECOND_CHOICE ="color_second_choice"; 
  private static final String SUBSTITUTION_INDICATOR ="substitution_indicator"; 
  private static final String SHIP_METHOD ="ship_method"; 
  private static final String SOURCE_CODE ="source_code"; 
  private static final String MEMBERSHIP_NAME ="membership_name"; 
  private static final String MEMBERSHIP_NUMBER="membership_number";  
  private static final String ADD_ONS="add_ons"; 
  private static final String VENDOR_TO_FLORIST="vendor_to_florist"; 
  private static final String FLORIST_TO_VENDOR="florist_to_vendor";   
  private static final String LINE_NUMBER = "1";
  private static final String PERSONAL_GREETING_ID = "personal_greeting_id"; 
  
  //external resources that we are keeping as member variables for easier testing
  protected SecurityManager securityManager = null;
  protected InitialContext initialContext = null;
  protected UpdateOrderDAO uoDAO = null;
  protected LockDAO lockDAO = null;
  protected CommentDAO commentDAO = null;
  protected RefundBO rBO = null;
  protected DomainObjectConverter converter = null;
  protected ScrubMapperDAO scrubMapper = null;
  protected DataMassager dm = null;
  protected Dispatcher dispatcher = null;
  protected PaymentDAO paymentDAO = null;
  protected GiftCardBO giftCardBO;
  protected RefundDAO refundDAO;
  protected OrderDAO orderDAO;
  
  /*
   * Default Constructor.
   */
  private CreateNewOrderBO() {
    super();
  }

  /**
   * Initializes the DeliveryInfoBO using the supplied request and connection.
   * @param request
   * @param connection
   */
  public CreateNewOrderBO(HttpServletRequest request, Connection connection) {
    this();
    this.request = request;
    this.connection = connection;
  }
  

  /**
   * Unfortunately does a bunch of things that would ideally be in separate methods:
   * - process refunds for old order
   * - authorize gift card payments for new order - other authorizations were performed other places - yuck
   * - remove holds on old order
   * - cancel old order
   * - create new order
   * - dispatch new order
   * Can you say refactor?
   * 
   * Throws exception if processing should be rolled back.
   * @throws GiftCardException
   * @throws Exception
   * @throws Throwable
   */
  public void processRequest() throws GiftCardException, Exception, Throwable {

      //Get values from the request.  
      String orderDetailId = (String)request.getParameter(COMConstants.ORDER_DETAIL_ID);
      String securityToken = (String)request.getParameter(COMConstants.SEC_TOKEN);
      String context = (String)request.getParameter(COMConstants.CONTEXT);
      String externalOrderNumber = (String)request.getParameter(COMConstants.EXTERNAL_ORDER_NUMBER);
      String orderGuid = request.getParameter(COMConstants.ORDER_GUID);

      logger.info("Orig order detail id="+orderDetailId);
      logger.info("Orig external order number="+externalOrderNumber);
      logger.info("Orig guid="+orderGuid);

      if (this.uoDAO == null) {
          uoDAO = new UpdateOrderDAO(this.connection);
      }
      if (this.paymentDAO == null) {
          paymentDAO = new PaymentDAO(this.connection);
      }
      if (this.giftCardBO == null) {
          giftCardBO = new GiftCardBO(this.connection);
      }
      if (this.refundDAO == null) {
          refundDAO = new RefundDAO(this.connection);
      }
      if (this.orderDAO == null) {
          orderDAO = new OrderDAO(this.connection);
      }

      //check to see if this order has already been modified
      //if it hasn't process as normal
      if(hasOrderBeenModified(externalOrderNumber))
      {
          return;
      }
      //Using the SecurityManager get the csr id
      if (this.securityManager == null) {
          this.securityManager = SecurityManager.getInstance();
      }
      UserInfo userInfo = this.securityManager.getUserInfo(securityToken);
      if (userInfo ==null)
      {
          throw new Exception("Csr id could not be obtained.");
      }
      String csrId = userInfo.getUserID();

      try
      { 
          // get the initial context
          if (initialContext == null) {
              initialContext = new InitialContext();
          }

          //Create comments on the modified order 
          createModifyOrderComments(externalOrderNumber, orderDetailId, orderGuid, csrId);

          //get orig and new order info
          getOrderInformation(orderDetailId, csrId);

          //send florist boolean
          boolean floristOrder = true;
          if(originalDeliveryInfoVO.getVendorFlag().equalsIgnoreCase("Y"))
          {
              logger.debug("Vendor order");
              floristOrder = false;
          }        

          //Get the new guid, master order number and external order number for the order
          if (orderIdGenerator == null) {
              orderIdGenerator = OrderIdGenerator.getInstance();
          }
          String newOrderId = orderIdGenerator.getOrderId(connection);
          String newExternalOrderNumber = createNewExternalOrder();
          String newMasterOrderNumber = createNewMasterOrder();

          logger.debug("New master order number="+newMasterOrderNumber);
          logger.debug("New external order number="+newExternalOrderNumber);
          logger.debug("New order id ="+newOrderId);        

          // verify that any gift card still has enough balance - this is intended to guarantee that the authorization 
          // of a new gift card does not fail/that it get processed gracefully
          String errorMsg = verifyGiftCardBalance(orderGuid, orderDetailId);
          if (errorMsg != null)
          {
              logger.error("Gift Card has insufficient balance or unable to determine balance.");
              throw new GiftCardException(errorMsg);
          }
          
          // after this point, no rollbacks should be performed as we are committing external transactions with the payment service (potentially)

          try {
              //Process refund
              refundOrder(orderDetailId, csrId, originalDeliveryInfoVO.getDeliveryDate(), securityToken, context);        

              //release the lock on the orig order
              if (this.lockDAO == null) {
                  this.lockDAO = new LockDAO(connection);
              }
              lockDAO.releaseLock(securityToken, csrId, orderDetailId, "MODIFY_ORDER");

              // authorize the new gift card payment if there is one
              authorizeGiftCard();

              //Insert the new order into scrub
              processOrder( newExternalOrderNumber,
                      newMasterOrderNumber,
                      newOrderId,
                      csrId,
                      orderDetailId,
                      externalOrderNumber);

              //Create comments on the original order to indicate a new order has been created
              createOriginalOrderComments(newExternalOrderNumber, orderDetailId, orderGuid, csrId);

              //remove any hold that are on the original order
              uoDAO.deleteOrderHold(orderDetailId);

              //Send a CAN message on the original order if it is needed.
              sendCAN(orderDetailId,floristOrder,csrId,originalDeliveryInfoVO.getDeliveryDate().getTime());

              //userTransaction.commit();

              //dispatch order
              MessageToken token = null;
              if (dispatcher == null) {
                  dispatcher = Dispatcher.getInstance(); 
              }    
              token = new MessageToken();
              token.setMessage(newOrderId + "/" + this.LINE_NUMBER);
              token.setStatus(DISPATCH_STATUS);
              token.setProperty("JMS_OracleDelay", new Integer(30).toString(), "int");
              dispatcher.dispatch(initialContext, token);

              // Indicate that a Call Disposition is now required (since order modified)              
              DecisionResultDataFilter.updateEntity(request, orderDetailId, true);
          }
          catch(Throwable t)
          {
              logger.error(t);
              SystemMessengerVO message = new SystemMessengerVO();
              message.setMessage("Could not create new order after processing refund/authorization. Order detail ID: " + orderDetailId + 
                      ".  Error : " + t.getMessage() + ".  ");
              message.setLevel(SystemMessengerVO.LEVEL_PRODUCTION);
              message.setSource("Update Order");
              message.setType("ERROR");
              message.setSubject("Create New Order failure");
              SystemMessenger.getInstance().send(message, connection);
              // Don't throw an exception so no rollback is triggered
          }
      }
      catch(Throwable t)
      {
          logger.error(t);

          throw t;
      }
      finally
      {
          try
          {
              initialContext.close();
          }
          catch (Exception e)
          {}
      }
  }  

  
  /**
   * Verify that a gift card used for payment still has sufficient balance for the expected charge to it. This is tricky since this could
   * be the same gift card or a new one. We will call validateGiftcard which, among other things, calculates how much of the gift card can 
   * be used on this update. If that amount < expected amount then we have an error. It normally should be the same but we will allow the
   * gift card balance to have increased for whatever random reason.  
   * @param orderGuid
   * @param orderDetailId
   * @return String error message if there was a problem or card has insufficient balance
   * @throws Exception
   */
  private String verifyGiftCardBalance(String orderGuid, String orderDetailId) throws Exception
  {
      String newGiftCardNumber = null;
      String pin = null;
      Double totalOwed = 0.0;
      for (PaymentVO payment : (ArrayList<PaymentVO>)tempPaymentList) {
          if (payment.getPaymentType().equals("GD")) {
              //authorize this gift card
              newGiftCardNumber = payment.getCardNumber();
              pin = payment.getGiftCardPin();
              totalOwed = payment.getCreditAmount();
          }
      }
      // if no gift card was used, nothing to check
      if (newGiftCardNumber == null)
      {
          return null;
      }

      // default return code/error string to an error in case validategiftcard reutrns nothing.
      String rc = giftCardBO.getErrorMessage(GiftCardBO.ERROR_MESSAGE_2_CODE.toString());

      // Validate the gift card
      Document gdResultDoc = giftCardBO.validateGiftCard(orderGuid, orderDetailId, newGiftCardNumber, pin, totalOwed.toString());

      // Obtain results of gift certificate/coupon validation
      if (gdResultDoc != null)
      {
          String XPath = "//root";
          NodeList nodeList = DOMUtil.selectNodes(gdResultDoc, XPath);
          Node node = null;
          if (nodeList != null)
          {
              for (int i = 0; i < nodeList.getLength(); i++)
              {
                  node = (Node) nodeList.item(i);
                  if (node != null)
                  {
                      String gdStatus = DOMUtil.selectSingleNode(node,"gcc_status/text()").getNodeValue();
                      if(gdStatus.equalsIgnoreCase("invalid"))
                      {
                          rc = DOMUtil.selectSingleNode(node,"message/text()").getNodeValue();
                      }
                      else
                      {
                          String gdAmount = DOMUtil.selectSingleNode(node,"amount/text()").getNodeValue(); 
                          Double gdamt = new Double(gdAmount);
                          if (gdamt.compareTo(totalOwed) >= 0)
                          {
                              rc = null;
                          }
                      }
                      break;
                  }
              }
          }
      }
      return rc;
  }

	/**
	 * refundOrder
	 * 
	 * Refunds all remaining payment amounts on the original order
	 * 
	 * @param orderDetailId
	 *            - Order detail id
	 * @param orderGuid
	 *            - Order guid
	 * @param csrId
	 *            - Csr id
	 * @param lastDeliveryDate
	 *            - Latest delivery date
	 * @return
	 * @throws java.lang.Exception
	 */
	private void refundOrder(String orderDetailId, String csrId,
			Calendar cOriginalDeliveryDate, String securityToken, String context)
			throws GiftCardException, Exception {
		// BOs
		if (this.rBO == null) {
			this.rBO = new RefundBO(connection, csrId);
		}

		// Variables
		String refundDisp = null;
		ArrayList orderDetails = new ArrayList();

		Calendar cTodaysDate = Calendar.getInstance();
		// Since the delivery date's time is 00:00:00, set today's date time to
		// 00:00:00 too
		cTodaysDate.set(Calendar.HOUR, 0);
		cTodaysDate.set(Calendar.MINUTE, 0);
		cTodaysDate.set(Calendar.SECOND, 0);
		cTodaysDate.set(Calendar.MILLISECOND, 0);
		cTodaysDate.set(Calendar.AM_PM, Calendar.AM);

		// If delivery date < current date, issue a B40 refund
		if (COMDeliveryDateUtil.getDiffInDays(cTodaysDate.getTime(),
				cOriginalDeliveryDate.getTime()) < 0) {
			this.isB40 = true;
			refundDisp = COMConstants.B40;
		}
		// If delivery date has not passed record an A40 refund
		else {
			this.isA40 = true;
			refundDisp = COMConstants.A40;
		}

		// Add order to refund
		orderDetails.add(orderDetailId);

		// Refund the order
		this.refundsIssuedList = rBO.postFullRefundMO(orderDetails, refundDisp,
				csrId, "Unbilled", securityToken, context);

		
		/**
		 * Each refund in the refundsIssuedList has a "payment" which is the Payment P record, and a 
		 * "paymentRefundID" which is the id for the Payment R record.
		 */
		logger.info("settling and refunding Gift Card payments against SVS");
		List<PaymentVO> origPayments = orderDAO.getGiftCardPayments(originalDeliveryInfoVO.getOrderGuid());
		List<PaymentVO> payments = new ArrayList<PaymentVO>();

		for (RefundVO refund : refundsIssuedList) 
		{
			// "R" is from FTD_APPS.PAYMENT_METHOD.PAYMENT_TYPE. it's the one
			// for gift card
			if (refund.getPaymentType().equals("R")  
					&& (refund.getPayment().getPaymentInd() == null || !refund.getPayment().getPaymentInd().equals("R"))) 
			{
				//sadly we cannot trust the payment credit amount that comes out of RefundBO.  
				//so we must set it here from the refund before we settle or refund
				if (refund.getPayment().getBillStatus() == null || 
						(refund.getPayment().getBillStatus() != null && refund.getPayment().getBillStatus().equals("Unbilled"))) 
				{
					// settle for the full amount of the original GD payment
					//find the original payment related to this refund so we can settle using the original balance
					for (PaymentVO origPmt : origPayments) {
						//compare the digits that aren't stars. 
						if (refund.getPayment().getCardNumber().equals(origPmt.getCardNumber()) 
								&& origPmt.getPaymentInd().equals("P")) 
						{
							//make a dummy paymentVO so we can settle it
							PaymentVO settlePmt = new PaymentVO();
							settlePmt.setCreditAmount(origPmt.getCreditAmount());
							settlePmt.setCardNumber(refund.getPayment().getCardNumber());
							settlePmt.setGiftCardPin(refund.getPayment().getGiftCardPin());
							settlePmt.setTransactionId(refund.getPayment().getTransactionId());
							
							giftCardBO.settlePayment(settlePmt);
							refund.getPayment().setBillStatus("Billed");
							refund.getPayment().setBillDate(new Date());
						}
					}

				}
 
				// refund.  set the refund payment amount based on the calculated refund amount.
				refund.getPayment().setCreditAmount(refund.getCalculatedRefundAmount());
				giftCardBO.refundPayment(refund.getPayment());
				refund.setRefundStatus("Billed");
				refund.setRefundDate(new Date());
				
                // get Payment R record by refund.getPaymentRefundID. update its billstatus & billdate, add it to payments list to get saved.
                for (PaymentVO origPmt : origPayments) {
                    if (refund.getPaymentRefundId() == origPmt.getPaymentId())
                    {
                        origPmt.setBillStatus("Billed");
                        origPmt.setBillDate(new Date());
                        origPmt.setUpdatedBy(csrId);
                        payments.add(origPmt);

                    }
                }
			}
		}

		// save all of the updated payment and refund records
		for (RefundVO refund : refundsIssuedList) 
		{
			//need the csr id and updated by regardless of if its a gift card or not
			refund.setUpdatedBy(csrId);
			refund.getPayment().setUpdatedBy(csrId);
			payments.add(refund.getPayment());
			refundDAO.updateRefund(refund);
		}
		paymentDAO.updatePayments(payments.toArray(new PaymentVO[payments.size()]));

	}

	/**
     * processOrder
     * 
     * This method creates a scrub order object, runs it through data massager,
     * persists it the database, and then dispatches it.
     * 
     * @param newExternalOrderNumber
     * @param newMasterOrderNumber
     * @param newGuid
     * @param csrId
     * @return 
     * @throws java.lang.Exception
     */
	private void processOrder(String newExternalOrderNumber,
	        String newMasterOrderNumber,
	        String newGuid,
	        String csrId,
	        String oldOrderDetailId,
	        String oldExternalOrderNumber) throws GiftCardException, Exception
	        {   	     	 
        String newOrderDetailId = null;
    
        //convert the updated clean order to a scrub order object
        Map orderData = new HashMap();
        orderData.put("originalDeliveryInfoVO",originalDeliveryInfoVO);
        orderData.put("tempDeliveryInfoVO",tempDeliveryInfoVO);
        orderData.put("originalOrderBillVO",originalOrderBillVO);
        orderData.put("tempOrderBillVO",tempOrderBillVO);
        orderData.put("originalOrderAddOnVO",originalOrderAddOnVO);
        orderData.put("tempOrderAddOnVO",tempOrderAddOnVO);
        orderData.put("tempCommentList",tempCommentList);
        orderData.put("tempCoBrandList",tempCoBrandList);
        orderData.put("tempPaymentList",tempPaymentList);
        orderData.put("tempCreditCardMap",tempCreditCardMap);
        if (this.converter == null) {
        	this.converter = new DomainObjectConverter(connection);
        }
        this.converter.setOrderData(orderData);
        
        newScrubOrder = converter.convertOrder(newExternalOrderNumber,
                newMasterOrderNumber,
                newGuid,                                                                 
                csrId);
        
                                                                         
         //Massage the order
         if (dm == null) {
        	 dm = new DataMassager();
         }
         newScrubOrder = dm.massageOrderDataNotStatic(newScrubOrder, connection);         
         
         // Set order status
         setStatusOrderValid(newScrubOrder);         
         
         //persist to scrub schema
         if (this.scrubMapper == null) {
        	 this.scrubMapper = new ScrubMapperDAO(connection);
         }
         scrubMapper.mapOrderToDB(newScrubOrder);            
            
         //obtain the order detail id         
         newOrderDetailId = uoDAO.getScrubOrderDetailId(newExternalOrderNumber);
            
         //Check for changes and add appropriate comments to order
         logChanges(oldOrderDetailId,oldExternalOrderNumber,newOrderDetailId,newExternalOrderNumber,csrId);                    
                  
    }

    private void authorizeGiftCard() throws GiftCardException, Exception
    {
        for (PaymentVO payment : (ArrayList<PaymentVO>)tempPaymentList) {
	        if (payment.getPaymentType().equals("GD")) {
	            //authorize this gift card
	            try {
	                giftCardBO.authorizePayment(payment);
	            } catch (GiftCardException ge) {
	                throw(ge);
	            } catch (Exception e) {
	                //ruh roh.  could not authorize.  don't try to fix, just throw error
	                throw new GiftCardException(giftCardBO.getErrorMessage(GiftCardBO.ERROR_MESSAGE_2_CODE.toString()));
	            }
	        }  //gift card check
	    }
    }

    /**
     * This method sets the status of order valid
     * valid
     *
    */
    private void setStatusOrderValid(OrderVO order)
    {
        // Set order status to Valid
        order.setStatus(String.valueOf(OrderStatus.VALID));
        setBuyerStatus(order, String.valueOf(OrderStatus.VALID));

        // Set Item status
        List items = order.getOrderDetail();
        MessageToken mt = null;
        Iterator it = items.iterator();
        OrderDetailsVO item = null;
        while(it.hasNext())
        {
            item = (OrderDetailsVO)it.next();

            item.setStatus(String.valueOf(OrderStatus.VALID_ITEM));
            setRecipientStatus(item, String.valueOf(OrderStatus.VALID_ITEM));
        }    
    }  
    
    private void setBuyerStatus(OrderVO order, String status)
    {
        BuyerVO buyer = null;
        if(order.getBuyer().size() > 0)
        {
            buyer = (BuyerVO)order.getBuyer().get(0);

            if(buyer != null)
            {
                buyer.setStatus(status);
            }
        }
    }

    private void setRecipientStatus(OrderDetailsVO item, String status)
    {
        RecipientsVO recipient = null;

        if(item.getRecipients().size() > 0)
        {
            recipient = (RecipientsVO)item.getRecipients().get(0);

            if(recipient != null)
            {
                recipient.setStatus(status);
            }
        }
    }       
  
  
 
     /*
      * sendCAN
      * 
      * Send a CAN on the original order if needed
      */
     private void sendCAN(String orderDetailId,boolean floristOrder, String csrId, Date deliveryDate) throws Exception
     {         
        logger.info("CAN for order detail " + orderDetailId);             
         
        ResultTO result = null;
     
        //check if a live mercury exists on the order
        CachedResultSet mercuryOrderMessageStatusInfo = uoDAO.getMercuryOrderMessageStatus(orderDetailId,null,"Y");
        String rejectSent = "N";
        String cancelSent = "N";
        String attemptedFTD = "N";
        String hasLiveFTD = "N";
        String ftdStatus = "N";
        if(mercuryOrderMessageStatusInfo.next())
        {
            if(mercuryOrderMessageStatusInfo.getString("reject_sent") != null)
              rejectSent = mercuryOrderMessageStatusInfo.getString("reject_sent").trim();
            if(mercuryOrderMessageStatusInfo.getString("cancel_sent") != null)
              cancelSent = mercuryOrderMessageStatusInfo.getString("cancel_sent").trim();
            if(mercuryOrderMessageStatusInfo.getString("attempted_ftd") != null)
              attemptedFTD = mercuryOrderMessageStatusInfo.getString("attempted_ftd").trim();
            if(mercuryOrderMessageStatusInfo.getString("has_live_ftd") != null)
              hasLiveFTD = mercuryOrderMessageStatusInfo.getString("has_live_ftd").trim();
            if(mercuryOrderMessageStatusInfo.getString("ftd_status") != null)
              ftdStatus = mercuryOrderMessageStatusInfo.getString("ftd_status").trim();
        }
        else
        {
            logger.debug("No prior message on order.  Order detail id=" + orderDetailId);
        }            

         //only send CAN for orders that have an attempted FTD associated with them
         //and if a CAN or REJ was not sent on the order 
         if(     (attemptedFTD != null && attemptedFTD.equalsIgnoreCase("Y")) &&
                 (rejectSent == null || rejectSent.equalsIgnoreCase("N")) &&
                 (cancelSent == null || cancelSent.equalsIgnoreCase("N")) )
         {

            logger.debug("Attempted, no rejects or cancels");

            //check if ADJ needs to be sent based on delivery date
            boolean adjustmentNeed = false;
            if (checkDeliveryDate(deliveryDate) == true) {
              adjustmentNeed=true;
              logger.debug("Based on date, ADJ needed.  Note:  This status may be changed for SDS orders");             
            }
            else
            {
              logger.debug("Based on date, ADJ not needed");                             
            }

             
             //check what type of order this orig one is
             if(floristOrder)
             {
                //get the mercury message associated with the FTD
                MessageVO messageVO = uoDAO.getMessageDetailLastFTD(orderDetailId,MessagingConstants.MERCURY_MESSAGE);

                //build mercury cancel TO
                CANMessageTO canTO = new CANMessageTO();
                canTO.setMercuryId(messageVO.getMessageId());
                canTO.setComments(CANCEL_MSG_COMMENT);    
                canTO.setOperator(csrId);    
                canTO.setMercuryStatus(BaseMercuryMessageTO.MERCURY_OPEN);    
                canTO.setDirection(BaseMercuryMessageTO.OUTBOUND);
                canTO.setFillingFlorist(messageVO.getFillingFloristCode());
                canTO.setSendingFlorist(messageVO.getSendingFloristCode());   
                
                if(adjustmentNeed)
                {  
                
                    ConfigurationUtil configUtil = ConfigurationUtil.getInstance();         
                    String adjReasonCode = configUtil.getProperty(COMConstants.PROPERTY_FILE,"MODIFY_ORDER_ADJ_REASON");                                           
                
                    //combinded report number = month + "-1"
                    int month = deliveryDate.getMonth() + 1;
                    String sMonth = String.valueOf(month);
                    //make the month 2 digits
                    if (sMonth.length() == 1){
                        sMonth = '0' + sMonth;
                    }
                    String combindedReportNumber = sMonth + "-1";
                
                    ADJMessageTO adjTO = new ADJMessageTO();
                    adjTO.setMercuryId(messageVO.getMessageId());
                    adjTO.setComments(ADJ_MSG_COMMENT);
                    adjTO.setCombinedReportNumber(combindedReportNumber);
                    adjTO.setOperator(csrId);
                    adjTO.setMercuryStatus("MO");
                    adjTO.setDirection(BaseMercuryMessageTO.OUTBOUND);
                    adjTO.setOldPrice(new Double(messageVO.getCurrentPrice()));
                    adjTO.setOverUnderCharge(new Double("0.00"));
                    adjTO.setAdjustmentReasonCode(adjReasonCode);
                    adjTO.setSendingFlorist(messageVO.getSendingFloristCode());
                    adjTO.setFillingFlorist(messageVO.getFillingFloristCode());    
                    
                    result=MessagingServiceLocator.getInstance().getMercuryAPI().sendADJMessage(adjTO, connection);

                    //check for errors
                     if(!result.isSuccess())
                     {
                        throw new Exception("Could not create Mercury ADJ message (order detail id="+orderDetailId+"). ERROR: "+result.getErrorString());
                     }
                     else
                     {
                         logger.debug("ADJ sent - florist");
                     }

                }
                
                
                
                //send mercury cancel
                result = MessagingServiceLocator.getInstance().getMercuryAPI().sendCANMessage(canTO, connection);
                
                //check for errors
                 if(!result.isSuccess())
                 {
                    throw new Exception("Could not create Mercury CAN message (order detail id="+orderDetailId+"). ERROR: "+result.getErrorString());
                 }                

             }
             else
             {
                //venus order
             
                //get the mercury message associated with the FTD
                MessageVO messageVO = uoDAO.getMessageDetailLastFTD(orderDetailId,MessagingConstants.VENUS_MESSAGE);            
             
                //get flag to determine if JMS should be sent out.  (used while testing)
                ConfigurationUtil configUtil = ConfigurationUtil.getInstance();         
                String cancelReason = configUtil.getProperty(COMConstants.PROPERTY_FILE,"MODIFY_ORDER_CANCEL_REASON");                           
             
                //build venus cancel TO
                CancelOrderTO canTO = new CancelOrderTO();
                canTO.setVenusId(messageVO.getMessageId());
                canTO.setComments(CANCEL_MSG_COMMENT);
                canTO.setCsr(csrId);
                canTO.setCancelReasonCode(cancelReason);

                // Test to see if an adjustment is required for SDS orders.  
                MessageUtil msgUtil = new MessageUtil(connection);
                boolean SDSShippingSystem = msgUtil.isShippingSystemSDS(messageVO);
                
                // If shipping system is SDS          
                if(SDSShippingSystem)
                {
                  // If SDS CAN requires an ADJ
                  if(msgUtil.requiresSDSAutoADJ(messageVO))
                  {
                   adjustmentNeed = true;
                   canTO.setVenusStatus(MessagingConstants.VENUS_STATUS_VERIFIED);
                   
                   if(logger.isDebugEnabled())
                    logger.debug("Based on ship date, ADJ needed for SDS order");             
                  }
                  /* If shipping system is SDS negate the ESCALATE logic run above
                   * if an auto ADJ is not required
                   */ 
                  else
                  {
                    adjustmentNeed = false;
                    if(logger.isDebugEnabled())
                     logger.debug("Based on ship date, ADJ not needed for SDS order");             
                  }
                }
                
                //if going to do an adjustment then do not transmit the cancel                
                if(adjustmentNeed)
                {
                  canTO.setTransmitCancel(false);
                }                
            
                //send venus cancel
                result= MessagingServiceLocator.getInstance().getVenusAPI().sendCancel(canTO, connection);
                
                //check for errors
                 if(!result.isSuccess())
                 {
                    throw new Exception("Could not create Venus CAN message (order detail id="+orderDetailId+"). ERROR: "+result.getErrorString());
                 }                
                
                
                //if adjustment needed
                if(adjustmentNeed)
                {
                    AdjustmentMessageTO adjTO = new AdjustmentMessageTO();
                    adjTO.setComments(ADJ_MSG_COMMENT);
                    adjTO.setOperator(csrId);
                    adjTO.setPrice(new Double(messageVO.getCurrentPrice()));
                    adjTO.setMessageType("ADJ");
                    adjTO.setOrderDetailId(orderDetailId);
                    adjTO.setVenusOrderNumber(messageVO.getMessageOrderId());
                    adjTO.setFillingVendor(messageVO.getFillingFloristCode());
                    adjTO.setOverUnderCharge(new Double("0.00"));
                    adjTO.setCancelReasonCode(canTO.getCancelReasonCode());
            
                    result = MessagingServiceLocator.getInstance().getVenusAPI().sendAdjustment(adjTO, connection);
                    
                    //check for errors
                     if(!result.isSuccess())
                     {
                        throw new Exception("Could not create ADJ message (order detail id="+orderDetailId+"). ERROR: "+result.getErrorString());
                     }                    
                     else
                     {
                         logger.debug("ADJ sent - vendor");
                     }                    
            
                }
                
                
             }




         }//end check if floral order
         
     }     
    private Double getMessageTOPrice(String priceStr) {
        Double price = new Double(0);

        if ((priceStr != null) && (priceStr.trim().length() > 0)) {
            try {
                price = (new Double(priceStr));
            } catch (NumberFormatException ne) {
                logger.error("can not parse price. ", ne);
                //don't need to throw an exception.
            }
        }

        return price;
    }     
     
     
    /*
     * This method populates the class variables used to store information
     * regarding the original and new order.
     */
    private void getOrderInformation(String orderDetailId, String csrId) throws Exception
    {
    	long ts = new Date().getTime();
        //Get updated order delivery info
        CachedResultSet rs = (CachedResultSet) uoDAO.getOriginalDeliveryInfo(orderDetailId);        
        if (!rs.next()) {
          throw new Exception("Original Delivery Info does not exist for order detail id " + orderDetailId);
        }        
        buildOriginalOrderVO(rs);    
        
        //Get hashmap of order data
        HashMap tempOrderInfo = uoDAO.getOrderDetailsUpdate(orderDetailId, "DELIVERY_CONFIRMATION", false, csrId, null);

        //original addons
        rs = (CachedResultSet) tempOrderInfo.get("OUT_ORIG_ADD_ONS");
        buildOriginalAddOnVO(rs,orderDetailId,csrId);
        
        //updated order
        rs = (CachedResultSet) tempOrderInfo.get("OUT_UPD_ORDER_CUR");
        if (!rs.next()) {
          throw new Exception("Data not found in update table for order detail id " + orderDetailId);
        }
        buildTempOrderVO(rs);
        
        //updated addons
        rs = (CachedResultSet) tempOrderInfo.get("OUT_UPD_ADDON_CUR");
        if (rs.next()) {
          buildTempAddOnVO(rs,orderDetailId,csrId);
        }
        
        //get comments
        rs = (CachedResultSet) tempOrderInfo.get("OUT_UPD_COMMENTS_CUR");
        buildTempCommentVO(rs);   
        
        //get cobrands
        rs = (CachedResultSet) tempOrderInfo.get("OUT_CO_BRAND_CUR");
        buildTempCoBrandVO(rs);   
        
        //get payment
        rs = (CachedResultSet) tempOrderInfo.get("OUT_PAYMENTS_CUR");
        buildTempPaymentVO(rs);   
        
        //get order extensions
        rs = (CachedResultSet) tempOrderInfo.get("OUT_ORDER_EXTENSIONS_CUR");
        buildTempOrderExtensions(rs);       

        logger.info("fetched order info in " + (new Date().getTime() - ts));
    }

    private void buildTempCommentVO(CachedResultSet rs)
    {
        while(rs != null && rs.next())
        {
            CommentsVO commentsVO = new CommentsVO();
            commentsVO.setComment((String)rs.getObject("comment_text"));
            commentsVO.setCommentOrigin((String)rs.getObject("comment_origin"));
            commentsVO.setCommentType((String)rs.getObject("comment_type"));
            
            tempCommentList.add(commentsVO);
        }
    }

    private void buildTempPaymentVO(CachedResultSet rs) throws Exception
    {
         while(rs != null && rs.next())
            {
                PaymentVO paymentVO = new PaymentVO();
                paymentVO.setAafesTicketNumber((String)rs.getObject("aafes_ticket_number"));
                paymentVO.setAcqRefNumber((String)rs.getObject("acq_reference_number"));
                paymentVO.setAvsCode((String)rs.getObject("avs_code"));
                paymentVO.setPaymentType((String)rs.getObject("payment_type"));
                paymentVO.setCardId(rs.getString("cc_id"));
                paymentVO.setCreditAmount(getDouble(rs.getString("credit_amount")));
                paymentVO.setAuthorization(rs.getString("auth_number"));
                paymentVO.setAuthResult(rs.getString("auth_result"));
                paymentVO.setGcCouponNumber(rs.getString("gc_coupon_number"));
                paymentVO.setNcApprovalId(rs.getString("nc_approval_identity_id"));
                paymentVO.setCscFailureCount(0);
                paymentVO.setCscResponseCode(null);
                paymentVO.setCscValidatedFlag("N");
                paymentVO.setCardNumber((String)rs.getObject("cc_number"));
                paymentVO.setGiftCardPin(rs.getString("gift_card_pin"));
                paymentVO.setOrderGuid("order_guid");
//                if (request.getParameter("gcc_num") != null && request.getParameter("gcc_num").equals(paymentVO.getCardNumber())) {
//                	paymentVO.setGiftCardPin(request.getParameter("gcc_pin"));
//                }
                paymentVO.setWalletIndicator(rs.getString("wallet_indicator"));
                tempPaymentList.add(paymentVO); 
    
                if(paymentVO.getCardId() != null && paymentVO.getCardId().length() > 0)
                {
                    CreditCardVO cardVO = new CreditCardVO();
                    cardVO.setCcExpiration((String)rs.getObject("cc_expiration"));
                    cardVO.setCcNumber((String)rs.getObject("cc_number"));
                    cardVO.setCcType((String)rs.getObject("cc_type"));
                    tempCreditCardMap.put(paymentVO.getCardId(),cardVO);                        
                }

            }            
    }
    
    private void buildTempOrderExtensions(CachedResultSet rs) throws Exception
    {
        ArrayList <OrderExtensionsVO> tempOrderExtensions = new ArrayList <OrderExtensionsVO> ();
    
        while(rs !=null && rs.next())
            {
                OrderExtensionsVO orderExtensionsVO = new OrderExtensionsVO();
                orderExtensionsVO.setInfoName((String)rs.getObject("info_name"));
                orderExtensionsVO.setInfoValue((String)rs.getObject("info_value"));            
                tempOrderExtensions.add(orderExtensionsVO);
            }
        this.tempDeliveryInfoVO.setOrderExtensions(tempOrderExtensions);
    }


    private double getDouble(String value)
    {
        if(value == null)
        {
            return 0.0;
        }
        else
        {
            return Double.parseDouble(value);
        }
    }

    private void buildTempCoBrandVO(CachedResultSet rs) throws Exception
    {
    
        while(rs !=null && rs.next())
            {
                CoBrandVO coBrandVO = new CoBrandVO();
                coBrandVO.setInfoName((String)rs.getObject("info_name"));
                coBrandVO.setInfoData((String)rs.getObject("info_data"));            
                tempCoBrandList.add(coBrandVO);
            }            
    }

  private void logChanges(String origOrderDetailId, String origExternalOrderNumber,
                            String newOrderDetailId, String newExternalOrderNumber,
                            String csrId) throws Exception
  {
      //get a map containing all the changes
      HashMap changesMap = checkForChanges();
      
      uoDAO.insertChanges(origOrderDetailId,
                        origExternalOrderNumber,
                        newOrderDetailId,
                        newExternalOrderNumber,
                        csrId,
                        changesMap);
                        
      
      
  }
  

     

  /**
   * This method returns a hash map of all the fields that were changed in the modified order.
   * @return HashMap
   * @throws java.lang.Exception
   */
  private HashMap checkForChanges() throws Exception {

    HashMap changesMap = new HashMap();

    boolean productChanged = false;

    if (valueChanged(originalDeliveryInfoVO.getRecipientFirstName(), tempDeliveryInfoVO.getRecipientFirstName()) ||
        valueChanged(originalDeliveryInfoVO.getRecipientLastName(), tempDeliveryInfoVO.getRecipientLastName())) {
      changesMap.put(RECIP_NAME,"Y");
    }

    if (valueChanged(originalDeliveryInfoVO.getRecipientBusinessName(), tempDeliveryInfoVO.getRecipientBusinessName())) {
      changesMap.put(RECIP_BUSINESS,"Y");
    }

    if (valueChanged(originalDeliveryInfoVO.getRecipientAddress1(), tempDeliveryInfoVO.getRecipientAddress1()) ||
        valueChanged(originalDeliveryInfoVO.getRecipientAddress2(), tempDeliveryInfoVO.getRecipientAddress2())) {
      changesMap.put(RECIP_ADDRESS,"Y");
    }

    if (valueChanged(originalDeliveryInfoVO.getRecipientCity(), tempDeliveryInfoVO.getRecipientCity())){
      changesMap.put(RECIP_CITY,"Y");
    }
    
    if (valueChanged(originalDeliveryInfoVO.getRecipientState(), tempDeliveryInfoVO.getRecipientState())){
      changesMap.put(RECIP_STATE,"Y");
    }
    
    if (valueChanged(originalDeliveryInfoVO.getRecipientZipCode(), tempDeliveryInfoVO.getRecipientZipCode())){
      changesMap.put(RECIP_ZIPCODE,"Y");
    }    
    
    if (valueChanged(originalDeliveryInfoVO.getRecipientCountry(), tempDeliveryInfoVO.getRecipientCountry())){
      changesMap.put(RECIP_COUNTRY,"Y");
    }            

    String origPhone = removeNonNumerics(originalDeliveryInfoVO.getRecipientPhoneNumber());
    String newPhone = removeNonNumerics(tempDeliveryInfoVO.getRecipientPhoneNumber());
    String origExt = removeNonNumerics(originalDeliveryInfoVO.getRecipientExtension());
    String newExt = removeNonNumerics(tempDeliveryInfoVO.getRecipientExtension());

    if (valueChanged(origPhone, newPhone) || valueChanged(origExt, newExt)) {
      changesMap.put(RECIP_PHONE,"Y");
    }

    if (valueChanged(originalDeliveryInfoVO.getDeliveryDate(), tempDeliveryInfoVO.getDeliveryDate())) {
      changesMap.put(DELIVERY_DATE,"Y");
    }

    if (valueChanged(originalDeliveryInfoVO.getCardMessage(), tempDeliveryInfoVO.getCardMessage())) {
      changesMap.put(CARD_MESSAGE,"Y");
    }

    if (valueChanged(originalDeliveryInfoVO.getCardSignature(), tempDeliveryInfoVO.getCardSignature())) {
      changesMap.put(SIGNATURE,"Y");
    }

    if (valueChanged(originalDeliveryInfoVO.getSpecialInstructions(), tempDeliveryInfoVO.getSpecialInstructions())) {
      changesMap.put(SPECIAL_INSTRUCTIONS,"Y");
    }

    if (isAltContactChange()) {
      changesMap.put(ALT_CONTACT_INFO,"Y");
    }

    if (valueChanged(originalDeliveryInfoVO.getProductId(), tempDeliveryInfoVO.getProductId())) {
        changesMap.put(PRODUCT,"Y");
        productChanged = true;
     }

    if (valueChanged(originalDeliveryInfoVO.getActualProductAmount(), tempOrderBillVO.getProductAmount())) {
        changesMap.put(PRODUCT_PRICE,"Y");
    }
    
    if ( valueChanged(originalDeliveryInfoVO.getPersonalGreetingId(), tempDeliveryInfoVO.getPersonalGreetingId()) ) {
       changesMap.put(PERSONAL_GREETING_ID,"Y");
    }

     //check if the ship type change
     if(valueChanged(tempDeliveryInfoVO.getVendorFlag(),originalDeliveryInfoVO.getVendorFlag()))
     {
          if (tempDeliveryInfoVO.getVendorFlag().equals("N"))
          {
            changesMap.put(VENDOR_TO_FLORIST,"Y");
          }
          else
          {
            changesMap.put(FLORIST_TO_VENDOR,"Y");
          }
     }


      //if florist delivered checks
      if (tempDeliveryInfoVO.getVendorFlag().equals("N")) {

            if (valueChanged(originalDeliveryInfoVO.getColor1(), tempDeliveryInfoVO.getColor1())) {
                  changesMap.put(COLOR_FIRST_CHOICE,"Y");
            }

            if (valueChanged(originalDeliveryInfoVO.getColor2(), tempDeliveryInfoVO.getColor2())) {
              changesMap.put(COLOR_SECOND_CHOICE,"Y");
            }

        if (valueChanged(originalDeliveryInfoVO.getSubstitutionIndicator(), tempDeliveryInfoVO.getSubstitutionIndicator())) {
          changesMap.put(SUBSTITUTION_INDICATOR,"Y");
        }

        if (addonsChanged()) {
          changesMap.put(ADD_ONS,"Y");
        }
        
      } else //else, vendor delivered
       {
        if (valueChanged(originalDeliveryInfoVO.getShipMethod(), tempDeliveryInfoVO.getShipMethod())) {
          changesMap.put(SHIP_METHOD,"Y");
        }
      } //end else, vendor

      if (valueChanged(originalDeliveryInfoVO.getSourceCode(), tempDeliveryInfoVO.getSourceCode())) {
        changesMap.put(SOURCE_CODE,"Y");
      }

      if ((valueChanged(originalDeliveryInfoVO.getMembershipFirstName(), tempDeliveryInfoVO.getMembershipFirstName())) ||
          (valueChanged(originalDeliveryInfoVO.getMembershipLastName(), tempDeliveryInfoVO.getMembershipLastName()))) {
        changesMap.put(MEMBERSHIP_NAME,"Y");
      }

      if (valueChanged(originalDeliveryInfoVO.getMembershipNumber(), tempDeliveryInfoVO.getMembershipNumber())) {
        changesMap.put(MEMBERSHIP_NUMBER,"Y");
      }

    return changesMap;
  }

  /*
   * Check if the two passed in Calendar values are the same.
   */
  private boolean valueChanged(Calendar inValue1, Calendar inValue2) {
    boolean changed = true;

    if (((inValue1 == null) && (inValue2 == null)) || ((inValue1 != null) && (inValue2 != null) && inValue1.get(Calendar.DAY_OF_YEAR) == inValue2.get(Calendar.DAY_OF_YEAR))) {
      changed = false;
    }

    return changed;
  }

  /*
   * Check if the two passed in double values are the same.
   */
  private boolean valueChanged(double inValue1, double inValue2) {
    return ((inValue1 == inValue2) ? false : true);
  }
    
    
  /*******************************************************************************************
   * buildOriginalOrderVO()
   *******************************************************************************************/
  private void buildOriginalOrderVO(CachedResultSet rs)
    throws Exception {
    //Define the format
    SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");

    /*****************************************************************************************
     * Retrieve/build the DeliveryInfoVO
     *****************************************************************************************/
    /*****************************************************************************************
     * Retrieve/build the OrderBillVO
     *****************************************************************************************/
    this.originalOrderBillVO.setAddOnAmount(getDouble(rs.getObject("add_on_amount")));
    this.originalOrderBillVO.setDiscountAmount(getDouble(rs.getObject("discount_amount")));
    this.originalOrderBillVO.setProductAmount(getDouble(rs.getObject("product_amount")));
    this.originalOrderBillVO.setServiceFee(getDouble(rs.getObject("service_fee")));

    this.originalOrderBillVO.setServiceFeeTax(getDouble(rs.getObject("service_fee_tax")));
    this.originalOrderBillVO.setShippingFee(getDouble(rs.getObject("shipping_fee")));
    this.originalOrderBillVO.setShippingTax(getDouble(rs.getObject("shipping_tax")));
    this.originalOrderBillVO.setTax(getDouble(rs.getObject("tax")));

    /*****************************************************************************************
     * Retrieve/build the OrderContactInfoVO info in the DeliveryInfoVO
     *****************************************************************************************/
    this.originalDeliveryInfoVO.setAltContactEmail((rs.getObject("alt_contact_email") != null)
      ? (String) rs.getObject("alt_contact_email") : null);
    this.originalDeliveryInfoVO.setAltContactExtension((rs.getObject("alt_contact_extension") != null)
      ? (String) rs.getObject("alt_contact_extension") : null);
    this.originalDeliveryInfoVO.setAltContactFirstName((rs.getObject("alt_contact_first_name") != null)
      ? (String) rs.getObject("alt_contact_first_name") : null);
    this.originalDeliveryInfoVO.setAltContactInfoId((rs.getObject("alt_contact_info_id") != null)
      ? rs.getObject("alt_contact_info_id").toString() : null);
    this.originalDeliveryInfoVO.setAltContactLastName((rs.getObject("alt_contact_last_name") != null)
      ? (String) rs.getObject("alt_contact_last_name") : null);
    this.originalDeliveryInfoVO.setAltContactPhoneNumber((rs.getObject("alt_contact_phone_number") != null)
      ? (String) rs.getObject("alt_contact_phone_number") : null);

    /*****************************************************************************************
     * Retrieve/build the MembershipVO info in the DeliveryInfoVO
     *****************************************************************************************/
    this.originalDeliveryInfoVO.setMembershipFirstName((rs.getObject("membership_first_name") != null)
      ? (String) rs.getObject("membership_first_name") : null);
    this.originalDeliveryInfoVO.setMembershipLastName((rs.getObject("membership_last_name") != null)
      ? (String) rs.getObject("membership_last_name") : null);
    this.originalDeliveryInfoVO.setMembershipNumber((rs.getObject("membership_number") != null)
      ? (String) rs.getObject("membership_number") : null);
    this.originalDeliveryInfoVO.setMessageOrderNumber((rs.getObject("message_order_number") != null)
      ? (String) rs.getObject("message_order_number") : null);
    this.originalDeliveryInfoVO.setFtdmIndicator((rs.getObject("message_ftdm_indicator") != null)
      ? (String) rs.getObject("message_ftdm_indicator") : null);

    /*****************************************************************************************
     * Retrieve/build the OrderDetailVO info in the DeliveryInfoVO
     *****************************************************************************************/
    this.originalDeliveryInfoVO.setActualProductAmount(getDouble(rs.getObject("actual_product_amount")));
    this.originalDeliveryInfoVO.setCardMessage((rs.getObject("card_message") != null) ? (String) rs.getObject("card_message") : null);
    this.originalDeliveryInfoVO.setCardSignature((rs.getObject("card_signature") != null)
      ? (String) rs.getObject("card_signature") : null);
    this.originalDeliveryInfoVO.setColor1((rs.getObject("color_1") != null) ? (String) rs.getObject("color_1") : null);
    this.originalDeliveryInfoVO.setColor2((rs.getObject("color_2") != null) ? (String) rs.getObject("color_2") : null);
    this.originalDeliveryInfoVO.setPersonalGreetingId((rs.getObject("personal_greeting_id") != null) ? (String) rs.getObject("personal_greeting_id") : null);
    
    /*****************************************************************************************
     * Retrieve and build Premier Circle Membership info
     *****************************************************************************************/
    this.originalDeliveryInfoVO.setPcFlag((rs.getObject("pc_flag") != null) ? (String) rs.getObject("pc_flag") : null);
    this.originalDeliveryInfoVO.setPcGroupId((rs.getObject("pc_group_id") != null) ? (String) rs.getObject("pc_group_id") : null);
    this.originalDeliveryInfoVO.setPcMembershipId((rs.getObject("pc_membership_id") != null) ? (String) rs.getObject("pc_membership_id") : null);

    //retrieve/format the delivery date
    java.sql.Date deliveryDateSql = null;

    if (rs.getObject("delivery_date") != null) {
      deliveryDateSql = (java.sql.Date) rs.getObject("delivery_date");

      //Create a Calendar Object
      Calendar cDeliveryDate = Calendar.getInstance();

      //Set the Calendar Object
      cDeliveryDate.setTime(new java.util.Date(deliveryDateSql.getTime()));
      this.originalDeliveryInfoVO.setDeliveryDate(cDeliveryDate);
    }

    //retrieve/format the delivery date range end
    java.sql.Date sDeliveryDateRangeEnd = null;

    if (rs.getObject("delivery_date_range_end") != null) {
      sDeliveryDateRangeEnd = (java.sql.Date) rs.getObject("delivery_date_range_end");

      //Create a Calendar Object
      Calendar cDeliveryDateRangeEnd = Calendar.getInstance();

      //Set the Calendar Object
      cDeliveryDateRangeEnd.setTime(new java.util.Date(sDeliveryDateRangeEnd.getTime()));
      this.originalDeliveryInfoVO.setDeliveryDateRangeEnd(cDeliveryDateRangeEnd);
    }

    //retrieve/format the delivery date range end
    java.sql.Date sEODDeliveryDate = null;

    if (rs.getObject("eod_delivery_date") != null) {
      sEODDeliveryDate = (java.sql.Date) rs.getObject("eod_delivery_date");

      //Create a Calendar Object
      Calendar cEODDeliveryDate = Calendar.getInstance();

      //Set the Calendar Object
      cEODDeliveryDate.setTime(new java.util.Date(sEODDeliveryDate.getTime()));
      this.originalDeliveryInfoVO.setEODDeliveryDate(cEODDeliveryDate);
    }

    this.originalDeliveryInfoVO.setEODDeliveryIndicator((rs.getObject("eod_delivery_indicator") != null)
      ? (String) rs.getObject("eod_delivery_indicator") : null);
    this.originalDeliveryInfoVO.setFloristId((rs.getObject("florist_id") != null) ? (String) rs.getObject("florist_id") : null);
    this.originalDeliveryInfoVO.setMilesPoints((rs.getObject("miles_points") != null)
      ? Double.parseDouble((rs.getObject("miles_points").toString())) : 0);
    this.originalDeliveryInfoVO.setOccasion((rs.getObject("occasion") != null) ? (String) rs.getObject("occasion") : null);
    this.originalDeliveryInfoVO.setOrderDetailId((rs.getObject("order_detail_id") != null)
      ? rs.getObject("order_detail_id").toString() : null);
    this.originalDeliveryInfoVO.setOrderGuid((rs.getObject("order_guid") != null) ? (String) rs.getObject("order_guid") : null);
    this.originalDeliveryInfoVO.setProductId((rs.getObject("product_id") != null) ? (String) rs.getObject("product_id") : null);
    this.originalDeliveryInfoVO.setQuantity((rs.getObject("quantity") != null)
      ? Long.parseLong((rs.getObject("quantity").toString())) : 0);
    this.originalDeliveryInfoVO.setReleaseInfoIndicator((rs.getObject("release_info_indicator") != null)
      ? (String) rs.getObject("release_info_indicator") : null);
    this.originalDeliveryInfoVO.setSecondChoiceProduct((rs.getObject("second_choice_product") != null)
      ? (String) rs.getObject("second_choice_product") : null);

    this.originalDeliveryInfoVO.setCustomerId((rs.getObject("customer_id") != null)
      ?  rs.getObject("customer_id").toString() : null);

    this.originalDeliveryInfoVO.setCustomerFirstName((rs.getObject("customer_first_name") != null)
      ? (String) rs.getObject("customer_first_name") : null);
    this.originalDeliveryInfoVO.setCustomerLastName((rs.getObject("customer_last_name") != null)
      ? (String) rs.getObject("customer_last_name") : null);
    this.originalDeliveryInfoVO.setCompanyId((rs.getObject("company_id") != null) ? (String) rs.getObject("company_id") : null);
    this.originalDeliveryInfoVO.setOccasionDescription((rs.getObject("occasion_description") != null)
      ? (String) rs.getObject("occasion_description") : null);
    this.originalDeliveryInfoVO.setOccasionCategoryIndex((rs.getObject("index_id") != null) ? rs.getObject("index_id").toString()
                                                                                            : null);
    this.originalDeliveryInfoVO.setOriginId((rs.getObject("origin_id") != null) ? (String) rs.getObject("origin_id") : null);

    //retrieve/format the ship date
    java.sql.Date sShipDate = null;

    if (rs.getObject("ship_date") != null) {
      sShipDate = (java.sql.Date) rs.getObject("ship_date");

      //Create a Calendar Object
      Calendar cShipDate = Calendar.getInstance();

      //Set the Calendar Object
      cShipDate.setTime(new java.util.Date(sShipDate.getTime()));
      this.originalDeliveryInfoVO.setShipDate(cShipDate);
    }

    this.originalDeliveryInfoVO.setShipMethod((rs.getObject("ship_method") != null) ? (String) rs.getObject("ship_method") : null);
    this.originalDeliveryInfoVO.setSizeIndicator((rs.getObject("size_indicator") != null)
      ? rs.getObject("size_indicator").toString() : null);
    this.originalDeliveryInfoVO.setSourceCode((rs.getObject("source_code") != null) ? rs.getObject("source_code").toString() : null);
    this.originalDeliveryInfoVO.setSpecialInstructions((rs.getObject("special_instructions") != null)
      ? rs.getObject("special_instructions").toString() : null);
    this.originalDeliveryInfoVO.setSubcode((rs.getObject("subcode") != null) ? (String) rs.getObject("subcode") : null);
    this.originalDeliveryInfoVO.setSubstitutionIndicator((rs.getObject("substitution_indicator") != null)
      ? (String) rs.getObject("substitution_indicator") : null);
    this.originalDeliveryInfoVO.setUpdatedBy((rs.getObject("updated_by") != null) ? (String) rs.getObject("updated_by") : null);

    //retrieve/format the ship date
    java.sql.Date sUpdatedOnDate = null;

    if (rs.getObject("updated_on") != null) {
      sUpdatedOnDate = (java.sql.Date) rs.getObject("updated_on");

      //Create a Calendar Object
      Calendar cUpdatedOnDate = Calendar.getInstance();

      //Set the Calendar Object
      cUpdatedOnDate.setTime(new java.util.Date(sUpdatedOnDate.getTime()));
      this.originalDeliveryInfoVO.setUpdatedOn(cUpdatedOnDate);
    }

    /*****************************************************************************************
     * Retrieve/build the OrderVO info in the DeliveryInfoVO
     *****************************************************************************************/
    this.originalDeliveryInfoVO.setCompanyId((rs.getObject("company_id") != null) ? (String) rs.getObject("company_id") : null);
    this.originalDeliveryInfoVO.setOriginId((rs.getObject("origin_id") != null) ? (String) rs.getObject("origin_id") : null);

    /*****************************************************************************************
     * Retrieve/build the CustomerVO info in the DeliveryInfoVO
     *****************************************************************************************/
    this.originalDeliveryInfoVO.setRecipientAddress1((rs.getObject("recipient_address_1") != null)
      ? (String) rs.getObject("recipient_address_1").toString() : null);
    this.originalDeliveryInfoVO.setRecipientAddress2((rs.getObject("recipient_address_2") != null)
      ? (String) rs.getObject("recipient_address_2").toString() : null);
    this.originalDeliveryInfoVO.setRecipientAddressType((rs.getObject("recipient_address_type") != null)
      ? (String) rs.getObject("recipient_address_type").toString() : null);
    this.originalDeliveryInfoVO.setRecipientBusinessInfo((rs.getObject("recipient_business_info") != null)
      ? (String) rs.getObject("recipient_business_info").toString() : null);
    this.originalDeliveryInfoVO.setRecipientBusinessName((rs.getObject("recipient_business_name") != null)
      ? (String) rs.getObject("recipient_business_name").toString() : null);
    this.originalDeliveryInfoVO.setRecipientCity((rs.getObject("recipient_city") != null)
      ? (String) rs.getObject("recipient_city").toString() : null);
    this.originalDeliveryInfoVO.setRecipientCountry((rs.getObject("recipient_country") != null)
      ? (String) rs.getObject("recipient_country").toString() : null);
    this.originalDeliveryInfoVO.setRecipientFirstName((rs.getObject("recipient_first_name") != null)
      ? (String) rs.getObject("recipient_first_name").toString() : null);
    this.originalDeliveryInfoVO.setRecipientId((rs.getObject("recipient_id") != null) ? rs.getObject("recipient_id").toString()
                                                                                      : null);
    this.originalDeliveryInfoVO.setRecipientLastName((rs.getObject("recipient_last_name") != null)
      ? (String) rs.getObject("recipient_last_name").toString() : null);
    this.originalDeliveryInfoVO.setRecipientState((rs.getObject("recipient_state") != null)
      ? (String) rs.getObject("recipient_state").toString() : null);
    this.originalDeliveryInfoVO.setRecipientZipCode((rs.getObject("recipient_zip_code") != null)
      ? (String) rs.getObject("recipient_zip_code").toString() : null);

    /*****************************************************************************************
     * Retrieve/build the CustomerPhoneVO info in the DeliveryInfoVO
     *****************************************************************************************/
    this.originalDeliveryInfoVO.setRecipientExtension((rs.getObject("recipient_extension") != null)
      ? (String) rs.getObject("recipient_extension").toString() : null);
    this.originalDeliveryInfoVO.setRecipientPhoneNumber((rs.getObject("recipient_phone_number") != null)
      ? (String) rs.getObject("recipient_phone_number").toString() : null);

    /*****************************************************************************************
     * Retrieve/build the remainder fields
     *****************************************************************************************/
    this.originalDeliveryInfoVO.setVendorFlag((rs.getObject("vendor_flag") != null) ? (String) rs.getObject("vendor_flag") : null);
    
    this.originalDeliveryInfoVO.setBuyerSignedInFlag((rs.getObject("buyer_signed_in_flag") != null) ? (String) rs.getObject("buyer_signed_in_flag") : null);
  }
    
    /*
     * Add passed in string to the buffer
     */
  private void addToBuffer(StringBuffer buffer, String message, String newLineChar) {
    if ((buffer != null) && (buffer.length() > 0)) {
      buffer.append(newLineChar);
    }

    buffer.append(message);
  }    
    
  private boolean valueChanged(String inValue1, String inValue2) {
    boolean changed = true;

    if (((inValue1 == null) && (inValue2 == null)) || ((inValue1 != null) && (inValue2 != null) && inValue1.equalsIgnoreCase(inValue2))) {
      changed = false;
    }

    return changed;
  }    
   
   /*
    * Remove the non-numeric chars from the passed in string
    */
  private String removeNonNumerics(String input) {
    String output = "";

    if (input == null) {
      return output;
    }

    for (int i = 0; i < input.length(); i++) {
      char n = input.charAt(i);
      Character nextCharacter = new Character(n);

      if (nextCharacter.isDigit(n)) {
        output = output + nextCharacter.toString();
      }
    }

    return output;
  }    
    
  private double getDouble(Object obj) throws Exception {
    double retValue = 0;

    try {
      retValue = (obj == null) ? 0 : Double.parseDouble(obj.toString());
    } catch (Exception e) {
      throw new Exception("Could not parse value into a double. Value=" + obj.toString());
    }

    return retValue;
  }    

  /*
   * Check if the alt contact info change
   */
  private boolean isAltContactChange() {
    boolean altContactChange = false;

    String origPhone = removeNonNumerics(originalDeliveryInfoVO.getAltContactPhoneNumber());
    String newPhone = removeNonNumerics(tempDeliveryInfoVO.getAltContactPhoneNumber());
    String origExt = removeNonNumerics(originalDeliveryInfoVO.getAltContactExtension());
    String newExt = removeNonNumerics(tempDeliveryInfoVO.getAltContactExtension());

    if (valueChanged(originalDeliveryInfoVO.getAltContactFirstName(), tempDeliveryInfoVO.getAltContactFirstName()) ||
        valueChanged(originalDeliveryInfoVO.getAltContactLastName(), tempDeliveryInfoVO.getAltContactLastName()) ||
        valueChanged(origPhone, newPhone) || valueChanged(origExt, newExt) ||
        valueChanged(originalDeliveryInfoVO.getAltContactEmail(), tempDeliveryInfoVO.getAltContactEmail())) {
      altContactChange = true;
    }

    return altContactChange;
  }
    

  /*
   * Check for changes made to the addons
   */
  private boolean addonsChanged()
    throws Exception {
    
    boolean changed = false;
    
    StringBuffer addonBuff = new StringBuffer();

    //first check for any added addons and updated existing addons
    Iterator iter = tempOrderAddOnVO.keySet().iterator();

    while (iter.hasNext()) {
      //get addon key
      String key = (String) iter.next();

      //get the temp Addon
      AddOnVO tempAddon = (AddOnVO) tempOrderAddOnVO.get(key);

      //get original Addon
      AddOnVO origAddOn = (AddOnVO) originalOrderAddOnVO.get(key);

      //if addon was added
      if (origAddOn == null) {
        changed = true;
      } else // check for quantity change
       {
        if (tempAddon.getAddOnQuantity() != origAddOn.getAddOnQuantity()) {
          changed = true;
        }
      } //end orig addon exists
    } //end addon loop

    //next check for any addons removed
    //first check for any added addons and updated existing addons
    iter = originalOrderAddOnVO.keySet().iterator();

    while (iter.hasNext()) {
      //get addon key
      String key = (String) iter.next();

      //get the temp Addon
      AddOnVO tempAddon = (AddOnVO) tempOrderAddOnVO.get(key);

      //get original Addon
      AddOnVO origAddOn = (AddOnVO) originalOrderAddOnVO.get(key);

      //if addon was removed
      if (tempAddon == null) {
        changed = true;
      }
    } //end addon loop

    return changed;
  }
    
    
  /*******************************************************************************************
   * buildTempOrderVO()
   *******************************************************************************************/
  private void buildTempOrderVO(CachedResultSet rs) throws Exception {
    //Define the format
    SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");

    /*****************************************************************************************
     * Retrieve/build the DeliveryInfoVO
     *****************************************************************************************/
    /*****************************************************************************************
     * Retrieve/build the OrderBillVO
     *****************************************************************************************/
    this.tempOrderBillVO.setAddOnAmount(getDouble(rs.getObject("add_on_amount")));
    this.tempOrderBillVO.setDiscountAmount(getDouble(rs.getObject("discount_amount")));
    this.tempOrderBillVO.setProductAmount(getDouble(rs.getObject("product_amount")));
    this.tempOrderBillVO.setServiceFee(getDouble(rs.getObject("service_fee")));
    this.tempOrderBillVO.setShippingFee(getDouble(rs.getObject("shipping_fee")));
    this.tempOrderBillVO.setShippingTax(getDouble(rs.getObject("shipping_tax")));
    this.tempOrderBillVO.setTax(getDouble(rs.getObject("tax")));

    /*****************************************************************************************
     * Retrieve/build the OrderContactInfoVO info in the DeliveryInfoVO
     *****************************************************************************************/
    this.tempDeliveryInfoVO.setAltContactEmail((rs.getObject("alt_contact_email") != null)
      ? (String) rs.getObject("alt_contact_email") : null);
    this.tempDeliveryInfoVO.setAltContactExtension((rs.getObject("alt_contact_extension") != null)
      ? (String) rs.getObject("alt_contact_extension") : null);
    this.tempDeliveryInfoVO.setAltContactFirstName((rs.getObject("alt_contact_first_name") != null)
      ? (String) rs.getObject("alt_contact_first_name") : null);
    this.tempDeliveryInfoVO.setAltContactInfoId((rs.getObject("alt_contact_info_id") != null)
      ? rs.getObject("alt_contact_info_id").toString() : null);
    this.tempDeliveryInfoVO.setAltContactLastName((rs.getObject("alt_contact_last_name") != null)
      ? (String) rs.getObject("alt_contact_last_name") : null);
    this.tempDeliveryInfoVO.setAltContactPhoneNumber((rs.getObject("alt_contact_phone_number") != null)
      ? (String) rs.getObject("alt_contact_phone_number") : null);

    /*****************************************************************************************
     * Retrieve/build the MembershipVO info in the DeliveryInfoVO
     *****************************************************************************************/
    this.tempDeliveryInfoVO.setMembershipFirstName((rs.getObject("membership_first_name") != null)
      ? (String) rs.getObject("membership_first_name") : null);
    this.tempDeliveryInfoVO.setMembershipLastName((rs.getObject("membership_last_name") != null)
      ? (String) rs.getObject("membership_last_name") : null);
    this.tempDeliveryInfoVO.setMembershipNumber((rs.getObject("membership_number") != null)
      ? (String) rs.getObject("membership_number") : null);
    
    /*****************************************************************************************
     * Retrieve and build Premier Circle Membership info
     *****************************************************************************************/
    this.tempDeliveryInfoVO.setPcFlag((rs.getObject("pc_flag") != null) ? (String) rs.getObject("pc_flag") : null);
    this.tempDeliveryInfoVO.setPcGroupId((rs.getObject("pc_group_id") != null) ? (String) rs.getObject("pc_group_id") : null);
    this.tempDeliveryInfoVO.setPcMembershipId((rs.getObject("pc_membership_id") != null) ? (String) rs.getObject("pc_membership_id") : null);

    /*****************************************************************************************
     * Retrieve/build the OrderDetailVO info in the DeliveryInfoVO
     *****************************************************************************************/
    this.tempDeliveryInfoVO.setActualProductAmount(getDouble(rs.getObject("actual_product_amount")));
    this.tempDeliveryInfoVO.setCardMessage((rs.getObject("card_message") != null) ? (String) rs.getObject("card_message") : null);
    this.tempDeliveryInfoVO.setCardSignature((rs.getObject("card_signature") != null) ? (String) rs.getObject("card_signature")
                                                                                      : null);
    this.tempDeliveryInfoVO.setColor1((rs.getObject("color_1") != null) ? (String) rs.getObject("color_1") : null);
    this.tempDeliveryInfoVO.setColor2((rs.getObject("color_2") != null) ? (String) rs.getObject("color_2") : null);
    this.tempDeliveryInfoVO.setPersonalGreetingId((rs.getObject("personal_greeting_id") != null) ? (String) rs.getObject("personal_greeting_id") : null);

    //retrieve/format the delivery date
    java.sql.Date sDeliveryDate = null;

    if (rs.getObject("delivery_date") != null) {
      sDeliveryDate = (java.sql.Date) rs.getObject("delivery_date");

      //Create a Calendar Object
      Calendar cDeliveryDate = Calendar.getInstance();

      //Set the Calendar Object
      cDeliveryDate.setTime(new java.util.Date(sDeliveryDate.getTime()));
      this.tempDeliveryInfoVO.setDeliveryDate(cDeliveryDate);
    }

    //retrieve/format the delivery date range end
    java.sql.Date sDeliveryDateRangeEnd = null;

    if (rs.getObject("delivery_date_range_end") != null) {
      sDeliveryDateRangeEnd = (java.sql.Date) rs.getObject("delivery_date_range_end");

      //Create a Calendar Object
      Calendar cDeliveryDateRangeEnd = Calendar.getInstance();

      //Set the Calendar Object
      cDeliveryDateRangeEnd.setTime(new java.util.Date(sDeliveryDateRangeEnd.getTime()));
      this.tempDeliveryInfoVO.setDeliveryDateRangeEnd(cDeliveryDateRangeEnd);
    }

    //retrieve/format the delivery date range end
    java.sql.Date sEODDeliveryDate = null;

    if (rs.getObject("eod_delivery_date") != null) {
      sEODDeliveryDate = (java.sql.Date) rs.getObject("eod_delivery_date");

      //Create a Calendar Object
      Calendar cEODDeliveryDate = Calendar.getInstance();

      //Set the Calendar Object
      cEODDeliveryDate.setTime(new java.util.Date(sEODDeliveryDate.getTime()));
      this.tempDeliveryInfoVO.setEODDeliveryDate(cEODDeliveryDate);
    }

    this.tempDeliveryInfoVO.setEODDeliveryIndicator((rs.getObject("eod_delivery_indicator") != null)
      ? (String) rs.getObject("eod_delivery_indicator") : null);
    this.tempDeliveryInfoVO.setFloristId((rs.getObject("florist_id") != null) ? (String) rs.getObject("florist_id") : null);
    this.tempDeliveryInfoVO.setMilesPoints((rs.getObject("miles_points") != null)
      ? Double.parseDouble(rs.getObject("miles_points").toString()) : 0);
    this.tempDeliveryInfoVO.setOccasion((rs.getObject("occasion") != null) ? (String) rs.getObject("occasion") : null);
    this.tempDeliveryInfoVO.setOrderDetailId((rs.getObject("order_detail_id") != null)
      ? rs.getObject("order_detail_id").toString() : null);
    this.tempDeliveryInfoVO.setOrderGuid((rs.getObject("order_guid") != null) ? (String) rs.getObject("order_guid") : null);
    this.tempDeliveryInfoVO.setProductId((rs.getObject("product_id") != null) ? (String) rs.getObject("product_id") : null);
    this.tempDeliveryInfoVO.setQuantity((rs.getObject("quantity") != null) ? Long.parseLong(rs.getObject("quantity").toString()) : 0);
    this.tempDeliveryInfoVO.setReleaseInfoIndicator((rs.getObject("release_info_indicator") != null)
      ? (String) rs.getObject("release_info_indicator") : null);
    this.tempDeliveryInfoVO.setSecondChoiceProduct((rs.getObject("second_choice_product") != null)
      ? (String) rs.getObject("second_choice_product") : null);
    this.tempDeliveryInfoVO.setNoTaxFlag((rs.getObject("no_tax_flag") != null)
        ? (String) rs.getObject("no_tax_flag") : null); 
    this.tempDeliveryInfoVO.setOriginalOrderHasSDU((rs.getObject("original_order_has_sdu") != null)
            ? (String) rs.getObject("original_order_has_sdu") : null); 

    //retrieve/format the ship date
    java.sql.Date sShipDate = null;

    if (rs.getObject("ship_date") != null) {
      sShipDate = (java.sql.Date) rs.getObject("ship_date");

      //Create a Calendar Object
      Calendar cShipDate = Calendar.getInstance();

      //Set the Calendar Object
      cShipDate.setTime(new java.util.Date(sShipDate.getTime()));
      this.tempDeliveryInfoVO.setShipDate(cShipDate);
    }

    this.tempDeliveryInfoVO.setShipMethod((rs.getObject("ship_method") != null) ? (String) rs.getObject("ship_method") : null);
    this.tempDeliveryInfoVO.setSizeIndicator((rs.getObject("size_indicator") != null) ? rs.getObject("size_indicator").toString()
                                                                                      : null);
    this.tempDeliveryInfoVO.setSourceCode((rs.getObject("source_code") != null) ? rs.getObject("source_code").toString() : null);
    this.tempDeliveryInfoVO.setSpecialInstructions((rs.getObject("special_instructions") != null)
      ? rs.getObject("special_instructions").toString() : null);
    this.tempDeliveryInfoVO.setSubcode((rs.getObject("subcode") != null) ? (String) rs.getObject("subcode") : null);
    this.tempDeliveryInfoVO.setSubstitutionIndicator((rs.getObject("substitution_indicator") != null)
      ? (String) rs.getObject("substitution_indicator") : null);
    this.tempDeliveryInfoVO.setUpdatedBy((rs.getObject("updated_by") != null) ? (String) rs.getObject("updated_by") : null);

    //retrieve/format the ship date
    java.sql.Date sUpdatedOnDate = null;

    if (rs.getObject("updated_on") != null) {
      sUpdatedOnDate = (java.sql.Date) rs.getObject("updated_on");

      //Create a Calendar Object
      Calendar cUpdatedOnDate = Calendar.getInstance();

      //Set the Calendar Object
      cUpdatedOnDate.setTime(new java.util.Date(sUpdatedOnDate.getTime()));
      this.tempDeliveryInfoVO.setUpdatedOn(cUpdatedOnDate);
    }

    /*****************************************************************************************
     * Retrieve/build the OrderVO info in the DeliveryInfoVO
     *****************************************************************************************/
    //this.tempDeliveryInfoVO.setCompanyId(rs.getObject("company_id") != null? (String) rs.getObject("company_id"):null);
    //this.tempDeliveryInfoVO.setOriginId(rs.getObject("origin_id") != null? (String) rs.getObject("origin_id"):null);
    this.tempDeliveryInfoVO.setCompanyId(originalDeliveryInfoVO.getCompanyId());
    this.tempDeliveryInfoVO.setOriginId(originalDeliveryInfoVO.getOriginId());
    /*****************************************************************************************
     * Retrieve/build the CustomerVO info in the DeliveryInfoVO
     *****************************************************************************************/
    this.tempDeliveryInfoVO.setRecipientAddress1((rs.getObject("recipient_address_1") != null)
      ? (String) rs.getObject("recipient_address_1").toString() : null);
    this.tempDeliveryInfoVO.setRecipientAddress2((rs.getObject("recipient_address_2") != null)
      ? (String) rs.getObject("recipient_address_2").toString() : null);
    this.tempDeliveryInfoVO.setRecipientAddressType((rs.getObject("recipient_address_type") != null)
      ? (String) rs.getObject("recipient_address_type").toString() : null);
    this.tempDeliveryInfoVO.setRecipientBusinessInfo((rs.getObject("recipient_business_info") != null)
      ? (String) rs.getObject("recipient_business_info").toString() : null);
    this.tempDeliveryInfoVO.setRecipientBusinessName((rs.getObject("recipient_business_name") != null)
      ? (String) rs.getObject("recipient_business_name").toString() : null);
    this.tempDeliveryInfoVO.setRecipientCity((rs.getObject("recipient_city") != null)
      ? (String) rs.getObject("recipient_city").toString() : null);
    this.tempDeliveryInfoVO.setRecipientCountry((rs.getObject("recipient_country") != null)
      ? (String) rs.getObject("recipient_country").toString() : null);
    this.tempDeliveryInfoVO.setRecipientFirstName((rs.getObject("recipient_first_name") != null)
      ? (String) rs.getObject("recipient_first_name").toString() : null);
    this.tempDeliveryInfoVO.setRecipientId((rs.getObject("recipient_id") != null) ? rs.getObject("recipient_id").toString() : null);
    this.tempDeliveryInfoVO.setRecipientLastName((rs.getObject("recipient_last_name") != null)
      ? (String) rs.getObject("recipient_last_name").toString() : null);
    this.tempDeliveryInfoVO.setRecipientState((rs.getObject("recipient_state") != null)
      ? (String) rs.getObject("recipient_state").toString() : null);
    this.tempDeliveryInfoVO.setRecipientZipCode((rs.getObject("recipient_zip_code") != null)
      ? (String) rs.getObject("recipient_zip_code").toString() : null);

    /*****************************************************************************************
     * Retrieve/build the CustomerPhoneVO info in the DeliveryInfoVO
     *****************************************************************************************/
    this.tempDeliveryInfoVO.setRecipientExtension((rs.getObject("recipient_extension") != null)
      ? (String) rs.getObject("recipient_extension").toString() : null);
    this.tempDeliveryInfoVO.setRecipientPhoneNumber((rs.getObject("recipient_phone_number") != null)
      ? (String) rs.getObject("recipient_phone_number").toString() : null);

    /*****************************************************************************************
     * Retrieve/build the remainder fields
     *****************************************************************************************/
    this.tempDeliveryInfoVO.setVendorFlag((rs.getObject("vendor_flag") != null) ? (String) rs.getObject("vendor_flag") : null);
  }    
    
    
  /*******************************************************************************************
   * buildOriginalAddOnVO()
   *******************************************************************************************/
  private void buildOriginalAddOnVO(CachedResultSet rs, String orderDetailId, String csrId)
    throws Exception {
    //multiple addons can be returned.  therefore, go thru them all
    while (rs.next()) {
      AddOnVO addOnVO = new AddOnVO();

      //set the order detail id
      addOnVO.setOrderDetailId(new Long(orderDetailId).longValue());

      //set the updated by id
      addOnVO.setUpdatedBy(csrId);

      //retrieve the add on code
      String addOnCode = null;

      if (rs.getObject("add_on_code") != null) {
        addOnCode = (String) rs.getObject("add_on_code");
      }

      addOnVO.setAddOnCode(addOnCode);

      //retrieve the add on quantity
      String addOnQty = null;
      BigDecimal bAddOnQty;
      long lAddOnQty = 0;

      if (rs.getObject("add_on_quantity") != null) {
        bAddOnQty = (BigDecimal) rs.getObject("add_on_quantity");
        addOnQty = bAddOnQty.toString();
        lAddOnQty = Long.valueOf(addOnQty).longValue();
      }

      addOnVO.setAddOnQuantity(lAddOnQty);

      String addOnTypeDesc = null;

      if (rs.getObject("add_on_type_description") != null) {
        addOnTypeDesc = (String) rs.getObject("add_on_type_description");
        addOnTypeDesc = addOnTypeDesc.toUpperCase();
      }

      addOnVO.setCreatedBy(null); //users cannot update
      addOnVO.setCreatedOn(null); //users cannot update
      addOnVO.setUpdatedOn(null); //users cannot update

      //also add it in a hashmap
      if (addOnCode != null) {
        this.originalOrderAddOnVO.put(addOnCode, addOnVO);
      }
    } //end (while rs.next())
  } 
  
  /*******************************************************************************************
   * buildTempAddOnVO()
   *******************************************************************************************/
  private void buildTempAddOnVO(CachedResultSet rs, String orderDetailId,String csrId) throws Exception {
    //multiple addons can be returned.  therefore, go thru them all
    do {
      AddOnVO addOnVO = new AddOnVO();

      //set the order detail id
      addOnVO.setOrderDetailId(new Long(orderDetailId).longValue());

      //set the updated by id
      addOnVO.setUpdatedBy(csrId);

      //retrieve the add on code
      String addOnCode = null;

      if (rs.getObject("add_on_code") != null) {
        addOnCode = (String) rs.getObject("add_on_code");
      }

      addOnVO.setAddOnCode(addOnCode);

      //retrieve the add on quantity
      String addOnQty = null;
      BigDecimal bAddOnQty;
      long lAddOnQty = 0;

      if (rs.getObject("add_on_quantity") != null) {
        bAddOnQty = (BigDecimal) rs.getObject("add_on_quantity");
        addOnQty = bAddOnQty.toString();
        lAddOnQty = Long.valueOf(addOnQty).longValue();
      }

      addOnVO.setAddOnQuantity(lAddOnQty);

      addOnVO.setCreatedBy(null); //users cannot update
      addOnVO.setCreatedOn(null); //users cannot update
      addOnVO.setUpdatedOn(null); //users cannot update

      //also add it in a hashmap
      if (addOnCode != null) {
        this.tempOrderAddOnVO.put(addOnCode, addOnVO);
      }
    } while (rs.next()); //end (while rs.next())
  }
  

  /*******************************************************************************************
   * createNewGuid()
   *******************************************************************************************/
  private String createNewGuid() throws Exception 
  {
    String newGUID = null;
    newGUID = GUIDGenerator.getInstance().getGUID();
    
    return newGUID; 

  }   

 
  /**
   * This method is used to enter comments on the new order.
   * @param externalOrderNumber
   * @param orderDetailId
   * @param orderGuid
   * @param csrId
   * @throws java.lang.Exception
   */
  private void createModifyOrderComments(String externalOrderNumber, String orderDetailId, String orderGuid, String csrId) throws Exception 
  {

    ConfigurationUtil configUtil = ConfigurationUtil.getInstance();         
    String modifyOrderDNIS = configUtil.getProperty(COMConstants.PROPERTY_FILE,"MODIFY_ORDER_DNIS");  

    //insert a record into the comments_update table

    HashMap updateResults = new HashMap();
     
    CommentsVO commentsVO = new CommentsVO();
    commentsVO.setComment("New order created due to updates on original order " + externalOrderNumber + ".");
    commentsVO.setCommentOrigin(request.getParameter(COMConstants.START_ORIGIN));
    commentsVO.setCommentType("Order");
    commentsVO.setCreatedBy(csrId);
    commentsVO.setUpdatedBy(csrId);
    commentsVO.setOrderDetailId(orderDetailId);
    commentsVO.setOrderGuid(orderGuid);
    commentsVO.setDnisId(modifyOrderDNIS);
    this.uoDAO.insertCommentsUpdate(commentsVO);
  }
  
 
  /**
   * This method is used to enter comments on the original order.
   * @param newExternalOrderNumber
   * @param orderDetailId
   * @param orderGuid
   * @param csrId
   * @throws java.lang.Exception
   */
  private void createOriginalOrderComments(String newExternalOrderNumber, String orderDetailId, String orderGuid, String csrId) throws Exception 
  {

    //insert a record into the comments table
    //Instantiate CommentDAO
    if (commentDAO == null) {
    	commentDAO = new CommentDAO(this.connection);
    }

    HashMap updateResults = new HashMap();
    
    String refundType = null;
    
    if(this.isA40)
      refundType = "A40";
    else
      refundType = "B40";
     
    CommentsVO commentsVO = new CommentsVO();
    commentsVO.setComment("CAN message issued due to Update order updates." + this.NEWLINE_CHAR_CODE + refundType + " refund issued due to Update order updates." + this.NEWLINE_CHAR_CODE + "New order " + newExternalOrderNumber + " created due to Update order updates.");
    commentsVO.setCommentOrigin(request.getParameter(COMConstants.START_ORIGIN));
    commentsVO.setCommentType("Order");
    commentsVO.setCreatedBy(csrId);
    commentsVO.setUpdatedBy(csrId);
    commentsVO.setOrderDetailId(orderDetailId);
    commentsVO.setOrderGuid(orderGuid);
    commentsVO.setCustomerId(originalDeliveryInfoVO.getCustomerId());
    commentDAO.insertComment(commentsVO);
  }  
  
  /*******************************************************************************************
   * createNewExternalOrder()
   *******************************************************************************************/
  private String createNewExternalOrder() throws Exception 
  {
    
    //Next available sequence number for the external order 
    String newExternalOrderNumber = uoDAO.getExternalOrderSeqNum();

    if (StringUtils.isNotEmpty(newExternalOrderNumber))
      newExternalOrderNumber = "C" + newExternalOrderNumber;
    else
      throw new Exception("An empty sequence number was returned while creating the External Order Number.");

    return newExternalOrderNumber;

  }   


  /*******************************************************************************************
   * createNewMasterOrder()
   *******************************************************************************************/
  private String createNewMasterOrder() throws Exception 
  {
    
    //Next available sequence number for the master order 
    String newMasterOrderNumber = uoDAO.getMasterOrderSeqNum();

    if (StringUtils.isNotEmpty(newMasterOrderNumber))
      newMasterOrderNumber = "M" + newMasterOrderNumber + "/1";
    else
      throw new Exception("An empty sequence number was returned while creating the Master Order Number.");

    return newMasterOrderNumber;

  }   


    /**
     *  check if the current date is greater than the
     *  last day of the delivery date month
     * @param deliveryDate - Date
     * @return boolean
     */
    private boolean checkDeliveryDate(Date deliveryDate)
        throws Exception {
        if (logger.isDebugEnabled()) {
            logger.debug("Entering checkDeliveryDate");
            logger.debug("deliveryDate: " + deliveryDate);
        }

        boolean dateComparision = false;

        try {
            Calendar calCurrent = Calendar.getInstance(TimeZone.getDefault());
            Date currentDate = calCurrent.getTime();

            SimpleDateFormat sdfOutput = new SimpleDateFormat(MessagingConstants.MESSAGE_DATE_FORMAT);

            if (currentDate.after(deliveryDate)) {
                Calendar calDelivery = Calendar.getInstance(TimeZone.getDefault());
                calDelivery.setTime(deliveryDate);

                int day = calDelivery.getActualMaximum(Calendar.DAY_OF_MONTH);

                Calendar calAdjustment = Calendar.getInstance(TimeZone.getDefault());
                calAdjustment.setTime(deliveryDate);
                calAdjustment.set(calDelivery.get(Calendar.YEAR),
                    calDelivery.get(Calendar.MONTH), day);

                Date adjustmentDate = calAdjustment.getTime();

                if (currentDate.after(adjustmentDate)) {
                    dateComparision = true;
                }
            }
        } finally {
            if (logger.isDebugEnabled()) {
                logger.debug("Exiting checkDeliveryDate");
            }
        }

        return dateComparision;
    }                  


	 /**
		*	Method calculates the order total.
		*
		* @param odVO - OrderDetailVO
    * @throws Exception
    */
    public String calculateOrderTotal(OrderBillVO obVO) throws Exception
    {
  		  OrderDetailsVO odsVO = new OrderDetailsVO();
  		  RecalculateOrderBO roVO = new RecalculateOrderBO();
 		
  	  	// Populate and OrderDetailsVO with amounts from OrderDetailVO
  	  	odsVO.setProductsAmount(String.valueOf(obVO.getProductAmount()));
    		odsVO.setAddOnAmount(String.valueOf(obVO.getAddOnAmount()));
    		odsVO.setServiceFeeAmount(String.valueOf(obVO.getServiceFee()));
  		  odsVO.setShippingFeeAmount(String.valueOf(obVO.getShippingFee()));
  	  	odsVO.setDiscountAmount(String.valueOf(obVO.getDiscountAmount()));
    		odsVO.setShippingTax(String.valueOf(obVO.getShippingTax()));
    		odsVO.setTaxAmount(String.valueOf(obVO.getTax()));
 		
    		// Calculate order total
    		return roVO.calculateOrderTotal(odsVO).toString();
 	}
  
  /**
   * This method checks to see if there is already a record in the mo_xref table
   * for this external order number.  Return true if there is, false if there is not.
   */
  private boolean hasOrderBeenModified(String externalOrderNumber) throws Exception
  {
    
    String orderHasBeenModified = uoDAO.hasOrderBeenModified(externalOrderNumber);
    
    boolean orderAlreadyModified = orderHasBeenModified.equalsIgnoreCase("Y") ? true : false;
      
    return orderAlreadyModified;    
  }
  
    private void rollback(UserTransaction userTransaction)
  {
      try  
      {
          if (userTransaction != null)  
          {
            // rollback the user transaction
            userTransaction.rollback();

            logger.info("User transaction rolled back");
          }
      } 
      catch (Exception ex)  
      {
          logger.error(ex);
      } 
  }

    /**
     * getter so the unit test can access the refund data after we process it.
     * @return
     */
	public List<RefundVO> getRefundsIssued() {
		return refundsIssuedList;
	}

	/**
	 * getter for the unit test to access the newly created order data after we process it.
	 * @return
	 */
	public OrderVO getNewScrubOrder() {
		return newScrubOrder;
	}
  
}   