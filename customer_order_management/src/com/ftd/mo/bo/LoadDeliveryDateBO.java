package com.ftd.mo.bo;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.dao.OrderDAO;
import com.ftd.customerordermanagement.vo.OrderBillVO;
import com.ftd.customerordermanagement.vo.OrderDetailVO;
import com.ftd.mo.dao.UpdateOrderDAO;
import com.ftd.customerordermanagement.dao.ViewDAO;
import com.ftd.customerordermanagement.util.BasePageBuilder;
import com.ftd.mo.util.BreadcrumbUtil;
import com.ftd.customerordermanagement.util.COMDeliveryDateUtil;
import com.ftd.customerordermanagement.util.DeliveryDateConfiguration;
import com.ftd.customerordermanagement.util.DeliveryDateParameters;
import com.ftd.mo.util.SearchUtil;
import com.ftd.mo.validation.ValidationConfiguration;
import com.ftd.mo.validation.ValidationLevels;
import com.ftd.mo.validation.ValidationRules;
import com.ftd.mo.validation.Validator;
import com.ftd.mo.validation.impl.DeliveryDateValidatorFactory;
import com.ftd.customerordermanagement.vo.AddOnVO;
import com.ftd.mo.vo.DeliveryInfoVO;
import com.ftd.mo.vo.ProductFlagsVO;
import com.ftd.ftdutilities.OEDeliveryDateParm;
import com.ftd.ftdutilities.OEParameters;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.XMLFormat;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.security.SecurityManager;
import com.ftd.security.cache.vo.UserInfo;

import java.io.PrintWriter;
import java.io.StringWriter;

import java.math.BigDecimal;

import java.util.List;

import java.sql.Connection;

import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;



import org.apache.commons.lang.StringUtils;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;


public class LoadDeliveryDateBO {

    /*******************************************************************************************
 * Class level variables
 *******************************************************************************************/
    private HttpServletRequest request = null;
    private Connection conn = null;
    private static Logger logger = 
        new Logger("com.ftd.mo.bo.LoadDeliveryDateBO");
        
    //request parameters required to be sent back to XSL all the time.
    private String customerId = null;
    private String deliveryDate = "";
    private String deliveryDateRange = "";
    private String externalOrderNumber = null;
    private String floristId = null;
    private String masterOrderNumber = null;
    private String occasion = null;
    private String orderDetailId = null;
    private String orderDispCode = null;
    private String orderGuid = null;
    private String originId = null;
    private String recipientCity = null;
    private String recipientCountry = null;
    private String recipientState = null;
    private String recipientZipCode = null;
    private String shipDate = null;
    private String shipMethod = null;
    private String sourceCode = null;
    private String currentDate = "";
    
    //other variables needed for this class
    private String actionType = null;
    private String csrId = null;
    private String displayFormat = null;
    private String sessionId = null;
    private HashMap hashXML = new HashMap();
    private HashMap pageData = new HashMap();
    private String productId = null;
    private String vendorFlag = null;
    private boolean wasMaxRetrieved = false;
    private String addons = null;

    //values returned from a validation failure of UpdateDeliveryDate
    private String uddMessageDisplay = null;
    private String uddSuccess        = null;


    // constants
    private static final String DATE_FORMAT = "MM/dd/yyyy";
    private static final String OCCASION_SYMPATHY_FUNERAL = "1";


    /*******************************************************************************************
 * Constructor 1
 *******************************************************************************************/
    public LoadDeliveryDateBO() {
    }


    /*******************************************************************************************
 * Constructor 2
 *******************************************************************************************/
    public LoadDeliveryDateBO(HttpServletRequest request, Connection conn) {
        this.request = request;
        this.conn = conn;
    }


    /*******************************************************************************************
  * processRequest()
  ******************************************************************************************
  *
  * @return HashMap - contains 0 - many XML documents of data, 1 HashMap each for page
  *                   details and search criteria
  * @throws
  */
    public HashMap processRequest() throws Exception {
        //HashMap that will be returned to the calling class
        HashMap returnHash = new HashMap();

        //Get info from the request object
        getRequestInfo(this.request);

        //process the action
        processMain();

        //populate the remainder of the fields on the page data
        populatePageData();


        //At this point, we should have three HashMaps.
        // HashMap1 = hashXML         - hashmap that contains zero to many Documents
        // HashMap2 = pageData        - hashmap that contains String data at page level
        //
        //Combine all of the above HashMaps into one HashMap, called returnHash

        //retrieve all the Documents from hashXML and put them in returnHash
        //retrieve the keyset
        Set ks1 = hashXML.keySet();
        Iterator iter = ks1.iterator();
        String key = null;
        Document doc = null;

        //Iterate thru the keyset
        while (iter.hasNext()) {
            //get the key
            key = iter.next().toString();

            //retrieve the XML document
            doc = (Document)hashXML.get(key);

            //put the XML object in the returnHash
            returnHash.put(key, (Document)doc);
        }

        //append pageData to returnHash
        returnHash.put("pageData", (HashMap)this.pageData);
        return returnHash;
    }


    /*******************************************************************************************
      * getRequestInfo(HttpServletRequest request)
      ******************************************************************************************
      * Retrieve the info from the request object
      *
      * @param  request - HttpServletRequest
      * @return none
      * @throws Exception
      */
    private void getRequestInfo(HttpServletRequest request) throws Exception {
        /****************************************************************************************
         *  Retrieve update delivery date specific parameters
         ***************************************************************************************/
         //retrieve the action
         if (request.getParameter(COMConstants.ACTION_TYPE) != null) {
             this.actionType = request.getParameter(COMConstants.ACTION_TYPE);
         }

         //retrieve the customer id
         if(request.getParameter(COMConstants.CUSTOMER_ID)!=null)
         {
           this.customerId = request.getParameter(COMConstants.CUSTOMER_ID);
         }

         //retrieve the delivery date
         if(request.getParameter(COMConstants.DELIVERY_DATE)!=null)
         {
           this.deliveryDate = request.getParameter(COMConstants.DELIVERY_DATE);
         }

         //retrieve the delivery date range end
         if(request.getParameter(COMConstants.DELIVERY_DATE_RANGE_END)!=null)
         {
           this.deliveryDateRange = request.getParameter(COMConstants.DELIVERY_DATE_RANGE_END);
         }

         //retrieve the external order number
         if(request.getParameter(COMConstants.EXTERNAL_ORDER_NUMBER)!=null)
         {
           this.externalOrderNumber = request.getParameter(COMConstants.EXTERNAL_ORDER_NUMBER);
         }
         
        if(request.getParameter(COMConstants.FLORIST_ID)!=null)
        {
          this.floristId = request.getParameter(COMConstants.FLORIST_ID);
        }

         //retrieve the master order number
         if(request.getParameter(COMConstants.MASTER_ORDER_NUMBER)!=null)
         {
           this.masterOrderNumber = request.getParameter(COMConstants.MASTER_ORDER_NUMBER);
         }

         //retrieve the occasion id
         if (request.getParameter(COMConstants.OCCASION) != null) {
             this.occasion = request.getParameter(COMConstants.OCCASION);
         }

         //retrieve the order detail id
         if(request.getParameter(COMConstants.ORDER_DETAIL_ID)!=null)
         {
           this.orderDetailId = request.getParameter(COMConstants.ORDER_DETAIL_ID);
         }
         
         //retrieve the order disp code
         if (request.getParameter(COMConstants.ORDER_DISP_CODE) != null) {
           this.orderDispCode = request.getParameter(COMConstants.ORDER_DISP_CODE);
         }

         //retrieve the order guid
         if(request.getParameter(COMConstants.ORDER_GUID)!=null)
         {
           this.orderGuid = request.getParameter(COMConstants.ORDER_GUID);
         }

         //retrieve the origin id
         if(request.getParameter(COMConstants.ORIGIN_ID)!=null)
         {
           this.originId = request.getParameter(COMConstants.ORIGIN_ID);
         }

         //retrieve the product id
         if (request.getParameter(COMConstants.PRODUCT_ID) != null) {
             this.productId = 
                     (request.getParameter(COMConstants.PRODUCT_ID)).toUpperCase();
         }
         
         //retrieve the recipient city
         if (request.getParameter(COMConstants.RECIPIENT_CITY) != null) {
            this.recipientCity = 
                    request.getParameter(COMConstants.RECIPIENT_CITY);
         }
         
         //retrieve the recipientCountry id
         if (request.getParameter(COMConstants.RECIPIENT_COUNTRY) != null) {
             this.recipientCountry = 
                     request.getParameter(COMConstants.RECIPIENT_COUNTRY);
         }

         //retrieve the recipientState
         if (request.getParameter(COMConstants.RECIPIENT_STATE) != null) {
             this.recipientState = 
                     request.getParameter(COMConstants.RECIPIENT_STATE);
         }

         //retrieve the postal code
         if (request.getParameter(COMConstants.RECIPIENT_ZIP_CODE) != null) {
             this.recipientZipCode = 
                     request.getParameter(COMConstants.RECIPIENT_ZIP_CODE);
         }

         //retrieve the ship method
         if(request.getParameter(COMConstants.SHIP_METHOD)!=null)
         {
           this.shipMethod = request.getParameter(COMConstants.SHIP_METHOD);
         }
         
         //retrieve the source code
         if(request.getParameter(COMConstants.SOURCE_CODE)!=null)
         {
           this.sourceCode = request.getParameter(COMConstants.SOURCE_CODE);
         }
         
         //retrieve the vendor flag
         if (request.getParameter(COMConstants.VENDOR_FLAG) != null) {
             this.vendorFlag = 
                     request.getParameter(COMConstants.VENDOR_FLAG);
         }

        if( request.getAttribute(COMConstants.PD_UDD_ERROR) != null)
        {
           Map hashMap = new HashMap();
           hashMap = (HashMap)request.getAttribute(COMConstants.PD_UDD_ERROR);
           this.uddMessageDisplay = (String) hashMap.get(COMConstants.PD_MESSAGE_DISPLAY);
           this.uddSuccess        = (String) hashMap.get("success");           
        }
         /****************************************************************************************
          *  Retrieve Security Info
          ***************************************************************************************/
         this.sessionId = request.getParameter(COMConstants.SEC_TOKEN);
         
         if (request.getParameter("addons") != null) {
        	 logger.info("addons: " + request.getParameter("addons"));
        	 this.addons = request.getParameter("addons");
         }
    }


    /*******************************************************************************************
      * getConfigInfo()
      ******************************************************************************************
      * Retrieve the info from the configuration file
      *
      * @throws Exception
      */
    private void getConfigInfo() throws Exception {

        SecurityManager sm = SecurityManager.getInstance();
        UserInfo ui = sm.getUserInfo(this.sessionId);
        this.csrId = ui.getUserID();
        
    }


    /*******************************************************************************************
      * populatePageData()
      ******************************************************************************************
      * Populate the page data with the remainder of the fields
      *
      */
    private void populatePageData() {
    
        this.pageData.put(COMConstants.PD_MO_CUSTOMER_ID,             this.customerId);
        this.pageData.put(COMConstants.PD_MO_DELIVERY_DATE,           this.deliveryDate);
        this.pageData.put(COMConstants.PD_MO_DELIVERY_DATE_RANGE_END, this.deliveryDateRange);
        this.pageData.put(COMConstants.PD_MO_EXTERNAL_ORDER_NUMBER,   this.externalOrderNumber);
        this.pageData.put(COMConstants.FLORIST_ID,                    this.floristId);
        this.pageData.put(COMConstants.PD_MO_MASTER_ORDER_NUMBER,     this.masterOrderNumber);
        this.pageData.put(COMConstants.PD_MO_OCCASION,                this.occasion);
        this.pageData.put(COMConstants.PD_MO_ORDER_DETAIL_ID,         this.orderDetailId);
        this.pageData.put(COMConstants.ORDER_DISP_CODE,               this.orderDispCode);
        this.pageData.put(COMConstants.PD_MO_ORDER_GUID,              this.orderGuid);
        this.pageData.put(COMConstants.PD_MO_ORIGIN_ID,               this.originId);
        this.pageData.put(COMConstants.PD_MO_PRODUCT_ID,              this.productId);
        this.pageData.put(COMConstants.PD_MO_RECIPIENT_COUNTRY,       this.recipientCountry);
        this.pageData.put(COMConstants.RECIPIENT_CITY,                this.recipientCity);
        this.pageData.put(COMConstants.PD_MO_RECIPIENT_STATE,         this.recipientState);
        this.pageData.put(COMConstants.PD_MO_RECIPIENT_ZIP_CODE,      this.recipientZipCode);
        this.pageData.put(COMConstants.SHIP_DATE,                     this.shipDate);
        this.pageData.put(COMConstants.PD_MO_SHIP_METHOD,             this.shipMethod);
        this.pageData.put(COMConstants.SOURCE_CODE,                   this.sourceCode);
        this.pageData.put(COMConstants.VENDOR_FLAG,                   this.vendorFlag);
        this.pageData.put(COMConstants.PD_MO_MAX_DELIVERY_DATES_RETRIEVED, this.wasMaxRetrieved?COMConstants.CONS_YES:COMConstants.CONS_NO);
        this.pageData.put("addons", addons);
        
        this.pageData.put(COMConstants.PD_MESSAGE_DISPLAY, this.uddMessageDisplay);
        this.pageData.put("success", this.uddSuccess);
     
        Calendar date = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
        this.currentDate = dateFormat.format(date.getTime());
        this.pageData.put("current_date", this.currentDate);
    }


  /*******************************************************************************************
  * processMain()
  ******************************************************************************************
  *
  * @throws Exception
  */
    private void processMain() throws Exception {

        if (this.actionType != null && 
            this.actionType.equalsIgnoreCase(COMConstants.ACTION_UDI_MAX_DATES)) {
            //determine if the product was a vendor or florist product
            determineVendorVsFloristProduct();

            //get the delivery dates
            processDeliveryDates(false);

            //set the xsl
            this.pageData.put("XSL", COMConstants.XSL_UDI_UPDATE_IFRAME);

        } else {

            //determine if the product was a vendor or florist product
            determineVendorVsFloristProduct();
            
            //get order detail information when page is first initialized
            getOrderDetailInfo();

            //get the delivery dates
            processDeliveryDates(true);

            //set the xsl
            this.pageData.put("XSL", COMConstants.XSL_UPDATE_DELIVERY_DATE);

        }
    }


 /*******************************************************************************************
 * determineVendorVsFloristOrder()
 *******************************************************************************************
  * This method is used to call the
  *
  */
    private void getOrderDetailInfo() throws Exception {

        // Instantiate OrderDAO
        OrderDAO orderDAO = new OrderDAO(this.conn);
                
        CachedResultSet orderDetailsInfo = orderDAO.getOrderDetailsInfo(orderDetailId);
        
        //populate values from the query results
        if(orderDetailsInfo.next())
        {
              //retrieve the source code
              this.sourceCode = orderDetailsInfo.getString(COMConstants.SOURCE_CODE);
              
              //retrieve the delivery date
              this.deliveryDate = orderDetailsInfo.getString(COMConstants.DELIVERY_DATE);
          
              //retrieve the delivery date range end
              this.deliveryDateRange = orderDetailsInfo.getString(COMConstants.DELIVERY_DATE_RANGE_END);
             
              //retrieve the external order number
              this.externalOrderNumber = orderDetailsInfo.getString(COMConstants.EXTERNAL_ORDER_NUMBER);
             
              //retrieve the occasion id
              this.occasion = orderDetailsInfo.getString(COMConstants.OCCASION);
              
              //retrieve the order disp code
              this.orderDispCode = orderDetailsInfo.getString("order_disp_code");
             
              //retrieve the product id
              this.productId = (orderDetailsInfo.getString(COMConstants.PRODUCT_ID));
                        
              //retrieve the recipient City 
              this.recipientCity = orderDetailsInfo.getString("city");
              
              //retrieve the recipientCountry id
              this.recipientCountry = orderDetailsInfo.getString("country");
              
              //retrieve the recipientState
              this.recipientState = orderDetailsInfo.getString("state");
              
              //retrieve the postal code
              this.recipientZipCode = orderDetailsInfo.getString("zip_code");
              
              //retrieve the ship date
              if( orderDetailsInfo.getString("ship_date") != null)
              {
                  this.shipDate = orderDetailsInfo.getString("ship_date");
                  //reformat the ship date
                  this.shipDate = shipDate.substring(5,7) + "/" + shipDate.substring(8,10) + "/" + shipDate.substring(0,4);  
              }
                                
              //retrieve the ship method
              this.shipMethod = orderDetailsInfo.getString(COMConstants.SHIP_METHOD);
                      
        }

    }
    
 /*******************************************************************************************
 * determineVendorVsFloristOrder()
 *******************************************************************************************
 * This method is used to call the
 *
 */
   private void determineVendorVsFloristProduct() throws Exception {

       if (this.vendorFlag.equalsIgnoreCase(COMConstants.CONS_YES)) {
           this.displayFormat = COMConstants.CONS_VENDOR;
       } else {
           this.displayFormat = COMConstants.CONS_FLORIST;
       }
       this.pageData.put("display_format", this.displayFormat);

   }


 /*******************************************************************************************
 * processDeliveryDates()
 *******************************************************************************************
  * This method is used to call the
  *
  */
    private void processDeliveryDates(boolean minDateFlag) throws Exception {

        Document deliveryDatesXML = DOMUtil.getDocument();

        DeliveryDateParameters ddParms = new DeliveryDateParameters();
        ddParms.setMinDaysOut(minDateFlag);
        ddParms.setRecipientState(this.recipientState);
        ddParms.setRecipientZipCode(this.recipientZipCode);
        ddParms.setRecipientCountry(this.recipientCountry);
        ddParms.setProductId(this.productId);
        ddParms.setOccasionId(this.occasion);
        ddParms.setOrderOrigin(this.originId);
        ddParms.setIsFlorist(this.displayFormat.equalsIgnoreCase(COMConstants.CONS_FLORIST) ? 
                             true : false);
        ddParms.setSourceCode(this.sourceCode);

        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
        Calendar date = Calendar.getInstance();
        if (StringUtils.isNotEmpty(this.deliveryDate)) {
            date.setTime(dateFormat.parse(this.deliveryDate));
            ddParms.setRequestedDeliveryDate(date);
        }
        if (StringUtils.isNotEmpty(this.deliveryDateRange)) {
            date.setTime(dateFormat.parse(this.deliveryDateRange));
            ddParms.setRequestedDeliveryDateRangeEnd(date);
        }
        ddParms.setAddons(this.addons);

        COMDeliveryDateUtil dateUtil = new COMDeliveryDateUtil();
        dateUtil.setParameters(ddParms);

        try {
            // do not show date ranges for sympathy/funeral orders
            DeliveryDateConfiguration deliveryDateConfig = 
                new DeliveryDateConfiguration();

            deliveryDateConfig.setDisplayDateFormat(DATE_FORMAT);
            deliveryDateConfig.setSubmitDateFormat(DATE_FORMAT);
            deliveryDateConfig.setShowDateRanges(this.occasion != null && 
                                                 this.occasion.equalsIgnoreCase(OCCASION_SYMPATHY_FUNERAL) ? 
                                                 false : true);
            deliveryDateConfig.setIncludeRequestedDeliveryDate(true);
            dateUtil.setConfiguration(deliveryDateConfig);

            deliveryDatesXML = dateUtil.getXmlDeliveryDates();
            this.wasMaxRetrieved = dateUtil.wasMaxRetrieved();

            this.hashXML.put(COMConstants.HK_DELIVERY_DATES_XML, 
                             deliveryDatesXML);
            
        } catch (Exception e) {
            logger.error(e);
            throw (e);
        }

    }

}
