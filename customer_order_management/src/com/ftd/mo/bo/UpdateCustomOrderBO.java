package com.ftd.mo.bo;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.dao.OrderDAO;
import com.ftd.customerordermanagement.vo.CommentsVO;
import com.ftd.customerordermanagement.vo.OrderBillVO;
import com.ftd.customerordermanagement.vo.OrderDetailVO;
import com.ftd.mo.dao.UpdateOrderDAO;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.order.RecalculateOrderBO;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.security.SecurityManager;
import com.ftd.security.cache.vo.UserInfo;

import java.math.BigDecimal;

import java.sql.Connection;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;



import org.apache.commons.lang.StringUtils;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class UpdateCustomOrderBO
{

/*******************************************************************************************
 * Class level variables
 *******************************************************************************************/
  private HttpServletRequest  request                 = null;
  private Connection          con                     = null;
  private static Logger       logger                  = new Logger("com.ftd.mo.bo.UpdateCustomOrderBO");


	//request parameters required to be sent back to XSL all the time.
  private String              orderDetailId        		= null;
  private String              originId                = null;
  private String              shipMethod              = null;

	//other variables needed for this class
  private String              commentId               = null;
  private String              csrId                   = null;
  private Calendar            cDeliveryDate           = null;
  private Calendar            cDeliveryDateRange      = null;
  private String              deliveryDate            = null;
  private String              deliveryDateRange       = null;
  private String              floristComments         = null;
  private HashMap             hashXML                 = new HashMap();
  private String              orderGuid               = null;
  private OrderDetailVO       origOrderDetailVO       = new OrderDetailVO();
  private OrderBillVO         origOrderBillVO         = new OrderBillVO();
  private HashMap             pageData                = new HashMap();
  private	String							productAmount				    = null;
  private String              productId               = null;
  private String              sessionId               = null;
  private String              sizeIndicator           = "A"; //default value
  private String              specialInstructions     = null;
  private String              personalGreetingId      = null;

  //constants
  private static final String POPUP_INDICATOR_OK      = "O";
  private static final String POPUP_INDICATOR_CONFIRM = "C";
  private static final String INPUT_DATE_FORMAT       = "MM/dd/yyyy";



/*******************************************************************************************
 * Constructor 1
 *******************************************************************************************/
  public UpdateCustomOrderBO()
  {
  }


/*******************************************************************************************
 * Constructor 2
 *******************************************************************************************/
  public UpdateCustomOrderBO(HttpServletRequest request, Connection con)
  {
    this.request = request;
    this.con = con;
  }


/*******************************************************************************************
  * processRequest()
  ******************************************************************************************
  *
  * @param  String - action
  * @return HashMap - contains 0 - many XML documents of data, 1 HashMap each for page
  *                   details and search criteria
  * @throws
  */
  public HashMap processRequest() throws Exception
  {
    //HashMap that will be returned to the calling class
    HashMap returnHash = new HashMap();

    //Get info from the request object
    getRequestInfo(this.request);

    //Get csr id
    getCsrId();

    //load the page
    processMain();

    //retrieve all the Documents from hashXML and put them in returnHash
    //retrieve the keyset
    Set ks1 = hashXML.keySet();
    Iterator iter = ks1.iterator();
    String key = null;
    Document doc = null;

    //Iterate thru the keyset
    while(iter.hasNext())
    {
      //get the key
      key = iter.next().toString();

      //retrieve the XML document
      doc = (Document) hashXML.get(key);

      //put the XML object in the returnHash
      returnHash.put(key, (Document) doc);
    }

    //append pageData to returnHash
    returnHash.put("pageData",        (HashMap)this.pageData);

    return returnHash;
  }



/*******************************************************************************************
  * getRequestInfo(HttpServletRequest request)
  ******************************************************************************************
  * Retrieve the info from the request object
  *
  * @param  HttpServletRequest - request
  * @return none
  * @throws none
  */
  private void getRequestInfo(HttpServletRequest request) throws Exception
  {

    /****************************************************************************************
     *  Retrieve order/search specific parameters
     ***************************************************************************************/
    //retrieve the custom order product id
    if(request.getParameter(COMConstants.PRODUCT_ID)!=null)
    {
      this.productId = (request.getParameter(COMConstants.PRODUCT_ID)).toUpperCase();
    }
    
    //retrieve the order guid
    if(request.getParameter(COMConstants.ORDER_GUID)!=null)
    {
      this.orderGuid = request.getParameter(COMConstants.ORDER_GUID);
    }
    
    //retrieve the delivery date
    if(request.getParameter(COMConstants.DELIVERY_DATE)!=null)
    {
      this.deliveryDate = request.getParameter(COMConstants.DELIVERY_DATE);
    }

    //retrieve the delivery date range end
    if(request.getParameter(COMConstants.DELIVERY_DATE_RANGE_END)!=null)
    {
      this.deliveryDateRange = request.getParameter(COMConstants.DELIVERY_DATE_RANGE_END);
    }

    //retrieve the order detail id
    if(request.getParameter(COMConstants.ORDER_DETAIL_ID)!=null)
    {
      this.orderDetailId = request.getParameter(COMConstants.ORDER_DETAIL_ID);
    }

    //retrieve the origin id
    if(request.getParameter(COMConstants.ORIGIN_ID)!=null)
    {
      this.originId = request.getParameter(COMConstants.ORIGIN_ID);
    }

    //retrieve the product amount
    if(request.getParameter(COMConstants.PRODUCT_AMOUNT)!=null)
    {
      this.productAmount = request.getParameter(COMConstants.PRODUCT_AMOUNT);
    }

    //retrieve the ship method
    if(request.getParameter(COMConstants.SHIP_METHOD)!=null)
    {
      this.shipMethod = request.getParameter(COMConstants.SHIP_METHOD);
    }

    //retrieve the size indicator
    if(request.getParameter(COMConstants.SIZE_INDICATOR)!=null)
    {
      this.sizeIndicator = request.getParameter(COMConstants.SIZE_INDICATOR);
    }

    //retrieve the florist comments
    if(request.getParameter(COMConstants.FLORIST_COMMENTS)!=null)
    {
      this.floristComments = request.getParameter(COMConstants.FLORIST_COMMENTS);
    }
    
    //retrieve the florist comments id
    if(request.getParameter(COMConstants.COMMENT_ID)!=null)
    {
      this.commentId = request.getParameter(COMConstants.COMMENT_ID);
    }
    
    //retrieve the personal greeting id
    if(request.getParameter("personal_greeting_id")!=null)
    {
      this.personalGreetingId = request.getParameter("personal_greeting_id");
    }

    /****************************************************************************************
     *  Retrieve Security Info
     ***************************************************************************************/
    this.sessionId   = request.getParameter(COMConstants.SEC_TOKEN);


    /******************************************************************************************
     * Set the calendar objects for dates
     *****************************************************************************************/
    SimpleDateFormat dateFormat = new SimpleDateFormat( INPUT_DATE_FORMAT );

    if ( StringUtils.isNotEmpty(this.deliveryDate) )
    {
      //Create a Calendar Object
      this.cDeliveryDate = Calendar.getInstance();
      //Set the Calendar Object using the date retrieved in sCreatedOn
      this.cDeliveryDate.setTime( dateFormat.parse( this.deliveryDate ) );
    }

    if ( StringUtils.isNotEmpty(this.deliveryDateRange) )
    {
      //Create a Calendar Object
      this.cDeliveryDateRange = Calendar.getInstance();
      //Set the Calendar Object using the date retrieved in sCreatedOn
      this.cDeliveryDateRange.setTime( dateFormat.parse( this.deliveryDateRange ) );
    }

  }


/*******************************************************************************************
  * getCsrId()
  ******************************************************************************************
  * Retrieve and set the csr id based on the security token
  *
  * @param none
  * @return
  * @throws none
  */
  private void getCsrId() throws Exception
  {

    //get csr Id
    SecurityManager sm = SecurityManager.getInstance();
    UserInfo ui = sm.getUserInfo(this.sessionId);
    this.csrId = ui.getUserID();
 
 }



/*******************************************************************************************
  * processMain()
  ******************************************************************************************
  *
  * @param  none
  * @return none
  * @throws none
  */
  private void processMain() throws Exception
  {
    processRetrieveOrderData();

    //reset the product amounts
    resetProductAmounts();

    //update the product related info
    processProductDetail();

    //update the product totals.
    processProductTotals();
    
    //update the florist comments
    processFloristComments();

    //set the request parm for the modifY_order_update
    setModifyFlag();

    this.pageData.put("XSL", COMConstants.XSL_LOAD_DELIVERY_CONF_ACTION);
  }


/*******************************************************************************************
 * processRetrieveOrderData()
 *******************************************************************************************
  * This method is used to call the
  *
  */
  private void processRetrieveOrderData() throws Exception
  {
    //Instantiate UpdateOrderDAO
    UpdateOrderDAO uoDAO = new UpdateOrderDAO(this.con);

    //hashmap that will contain the output from the stored proc
    HashMap orderInfo = new HashMap();

    //Call getOrderDetailsUpdate method in the DAO
    orderInfo = uoDAO.getOrderDetailsUpdate(this.orderDetailId,
                            COMConstants.CONS_ENTITY_TYPE_PRODUCT_DETAIL, false, this.csrId, this.productId );


    Set ks1 = orderInfo.keySet();
    Iterator iter = ks1.iterator();
    String key = null;

    //Create a CachedResultSet object using the cursor name that was passed.
    CachedResultSet rs = null;

    //Iterate thru the keyset
    while(iter.hasNext())
    {
      //get the key
      key = iter.next().toString();
      rs = (CachedResultSet) orderInfo.get(key);

//*****************************************************************************************
// * OUT_ORIG_PRODUCT
// *****************************************************************************************/
      if (key.equalsIgnoreCase("OUT_ORIG_PRODUCT"))
      {
        rs.next();

				/*****************************************************************************************
				 * Retrieve/build the order detail VO
				 *****************************************************************************************/

				this.origOrderDetailVO.setColor1(rs.getObject("color_1") != null? (String) rs.getObject("color_1"):null);
				this.origOrderDetailVO.setColor2(rs.getObject("color_2") != null? (String) rs.getObject("color_2"):null);
                                this.origOrderDetailVO.setPersonalGreetingId(rs.getObject("personal_greeting_id") != null? (String) rs.getObject("personal_greeting_id"):null);

				//Define the format
				SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");

				//retrieve/format the delivery date
        java.sql.Date dDeliveryDate = null;

				if (rs.getObject("delivery_date") != null)
        {
					dDeliveryDate = (java.sql.Date)rs.getObject("delivery_date");
  				//Create a Calendar Object
    			Calendar cDeliveryDate = Calendar.getInstance();
      		//Set the Calendar Object using the date retrieved in sCreatedOn
        	cDeliveryDate.setTime(new java.util.Date(dDeliveryDate.getTime()));
          this.origOrderDetailVO.setDeliveryDate(cDeliveryDate);
        }

				//retrieve/format the delivery date
				java.sql.Date dDeliveryDateRange = null;
				if (rs.getObject("delivery_date_range_end") != null)
        {
					dDeliveryDateRange = (java.sql.Date)rs.getObject("delivery_date_range_end");
  				//Create a Calendar Object
    			Calendar cDeliveryDateRange = Calendar.getInstance();
      		//Set the Calendar Object using the date retrieved in sCreatedOn
        	cDeliveryDateRange.setTime(new java.util.Date(dDeliveryDateRange.getTime()));
          this.origOrderDetailVO.setDeliveryDateRangeEnd(cDeliveryDateRange);
        }


				//retrieve/format the ship date
				java.sql.Date dShipDate = null;
				if (rs.getObject("ship_date") != null)
        {
					dShipDate = (java.sql.Date)rs.getObject("ship_date");
  				//Create a Calendar Object
    			Calendar cShipDate = Calendar.getInstance();
      		//Set the Calendar Object using the date retrieved in sCreatedOn
        	cShipDate.setTime(new java.util.Date(dShipDate.getTime()));
          this.origOrderDetailVO.setShipDate(cShipDate);
        }

				//retrieve the quantity
				String      qty = null;
				BigDecimal  bQty;
				long        lQty = 0;
				if (rs.getObject("quantity") != null)
				{
					bQty = (BigDecimal)rs.getObject("quantity");
					qty = bQty.toString();
					lQty = Long.valueOf(qty).longValue();
				}
				this.origOrderDetailVO.setQuantity(lQty);

				this.origOrderDetailVO.setOrderDetailId(this.orderDetailId);
				this.origOrderDetailVO.setProductId(rs.getObject("product_id") != null? rs.getObject("product_id").toString().toUpperCase():null);
				this.origOrderDetailVO.setShipMethod(rs.getObject("ship_method") != null? (String) rs.getObject("ship_method"):null);
				this.origOrderDetailVO.setSizeIndicator(rs.getObject("size_indicator") != null? (String) rs.getObject("size_indicator"):null);
				this.origOrderDetailVO.setSpecialInstructions(rs.getObject("special_instructions") != null? (String) rs.getObject("special_instructions"):null);
				this.origOrderDetailVO.setSubstitutionIndicator(rs.getObject("substitution_indicator") != null? (String) rs.getObject("substitution_indicator"):null);

				//retrieve the miles/points
				String      milesPoints = null;
				BigDecimal  bMilesPoints;
				double      dMilesPoints = 0;
				if (rs.getObject("miles_points") != null)
				{
					bMilesPoints = (BigDecimal)rs.getObject("miles_points");
					milesPoints = bMilesPoints.toString();
					dMilesPoints = Double.valueOf(milesPoints).doubleValue();
				}
				this.origOrderDetailVO.setMilesPoint(dMilesPoints);

      }
//*****************************************************************************************
// * OUT_UPD_ORDER_CUR
// *****************************************************************************************/
      else if (key.equalsIgnoreCase("OUT_UPD_ORDER_CUR"))
      {
        rs.next();
				this.specialInstructions = (rs.getObject("special_instructions") != null? (String) rs.getObject("special_instructions"):null);
      }
//*****************************************************************************************
// * OUT_UPD_ADDON_CUR
// *****************************************************************************************/
      else if (key.equalsIgnoreCase("OUT_UPD_ADDON_CUR"))
      {}
//*****************************************************************************************
// * OUT_ORIG_ADD_ONS
// *****************************************************************************************/
      else if (key.equalsIgnoreCase("OUT_ORIG_ADD_ONS"))
      {}
//*****************************************************************************************
// * OUT_SHIP_COST
// *****************************************************************************************/
      else if (key.equalsIgnoreCase("OUT_SHIP_COST"))
      {}


      
    } //end-while(iter.hasNext())


  }



/*******************************************************************************************
 * processProductDetail()
 *******************************************************************************************
  * This method is used to update the order
  *
  */
  private void processProductDetail() throws Exception
  {
    //Instantiate UpdateOrderDAO
    UpdateOrderDAO uoDAO = new UpdateOrderDAO(this.con);

    HashMap updateResults = new HashMap();

    updateResults = uoDAO.updateProductDetailInfo(this.orderDetailId,
                                  this.csrId,
                                  this.cDeliveryDate,
                                  this.cDeliveryDateRange,
                                  this.productId,
                                  null, //color1
                                  null, //color2
                                  null, //SubstitutionIndicator,
                                  this.specialInstructions, //special instructions
                                  this.origOrderDetailVO.getShipMethod(),
                                  this.sizeIndicator,
                                  this.origOrderDetailVO.getShipDate(),
                                  null,//subcode
                                  this.personalGreetingId); 


    String status = (String) updateResults.get(COMConstants.STATUS_PARAM);
    if( status.equalsIgnoreCase(COMConstants.CONS_NO))
    {
        String message = (String) updateResults.get(COMConstants.MESSAGE_PARAM);
        throw new Exception(message);
    }

  }



/*******************************************************************************************
 * processProductTotals()
 *******************************************************************************************
  * This method is used to update the order
  *
  */
  private void processProductTotals() throws Exception
  {
    //Instantiate UpdateOrderDAO
    UpdateOrderDAO uoDAO = new UpdateOrderDAO(this.con);

    //note that uoDAO.recalculateUpdatedPricingInfo(this.orderDetailId, this.csrId) is designed
    //to get the info from the database; this would bypass the product related changes that the CSR
    //updated on the page.  Thus, we have to create a VO ourselves instead of generating it from a
    //generic method.

    //build the order details vo
    OrderDetailsVO odVO = buildOrigOrderDetailVO();

    //build and calculate the price for the new product
    OrderVO newOrderVO = (OrderVO) recalculateNewOrderAmounts(odVO);

    //get the updated order detail vo.
    odVO = (OrderDetailsVO) newOrderVO.getOrderDetail().get(0);

    //update the prices
    uoDAO.updateODUAmounts(odVO, this.origOrderBillVO, this.orderDetailId, this.csrId);


  }

/*******************************************************************************************
 * processFloristComments()
 *******************************************************************************************
  * This method is used to insert or update the florist comments
  *
  */
  private void processFloristComments() throws Exception
  {
    //Instantiate UpdateOrderDAO
    UpdateOrderDAO uoDAO = new UpdateOrderDAO(this.con);

    HashMap updateResults = new HashMap();
     
    CommentsVO commentsVO = new CommentsVO();
    commentsVO.setCommentId(this.commentId);
    commentsVO.setComment(this.floristComments);
    commentsVO.setCommentOrigin(request.getParameter(COMConstants.START_ORIGIN));
    commentsVO.setCommentType("Florist");
    commentsVO.setCreatedBy(csrId);
    commentsVO.setUpdatedBy(csrId);
    commentsVO.setOrderDetailId(orderDetailId);
    commentsVO.setOrderGuid(this.orderGuid);

    if(this.commentId.equals(""))
    {  
      uoDAO.insertCommentsUpdate(commentsVO);
    }
    else
    {
      uoDAO.updateCommentsUpdate(commentsVO);      
    }
  }



/*******************************************************************************************
 * buildOrigOrderDetailVO()
 *******************************************************************************************/
  private OrderDetailsVO buildOrigOrderDetailVO()
        throws Exception
  {

    UpdateOrderDAO uaDAO = new UpdateOrderDAO(this.con);

    // Build an OrderDetailsVO for specified updated order detail
    OrderDetailsVO odvo = uaDAO.buildRecalculateOrderDetailsVO(this.orderDetailId);

    return odvo;

  }



/*******************************************************************************************
 * recalculateOrigOrderAmounts()
 *******************************************************************************************/
  private OrderVO recalculateOrigOrderAmounts(OrderDetailsVO odVO)
        throws Exception
  {

    OrderVO orderVO = initializeOrderVO();
    Collection orderDetails = new ArrayList();
    orderDetails.add(odVO);
    orderVO.setOrderDetail((List) orderDetails);
    RecalculateOrderBO recalcOrderVo = new RecalculateOrderBO();
    recalcOrderVo.recalculate(this.con, orderVO);

    return orderVO;
  }


/*******************************************************************************************
 * recalculateNewOrderAmounts()
 *******************************************************************************************/
  private OrderVO recalculateNewOrderAmounts(OrderDetailsVO odVO)
        throws Exception
  {
    //initialize these fields.  they will be recalculated in the recalculateVO.
    odVO.setAddOnAmount("0");
    odVO.setDiscountAmount("0");
    odVO.setExternalOrderTotal("0");
    odVO.setMilesPoints("0");
    odVO.setServiceFeeAmount("0");
    odVO.setShippingFeeAmount("0");
    odVO.setTaxAmount("0");
    odVO.setOrigSourceCode(odVO.getSourceCode());
    odVO.setOrigProductId(odVO.getProductId());
    odVO.setOrigShipMethod(odVO.getShipMethod());
    odVO.setOrigDeliveryDate(odVO.getDeliveryDate());


    //for the new product, we have to update the following fields in the OrderDetailsVO
    //before we trigger the recalculate class.
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
    String sDeliveryDate = null;
    String sDeliveryDateRange = null;
    if (StringUtils.isNotEmpty(this.deliveryDate))
    {
      Date dDeliveryDate = this.cDeliveryDate.getTime();
      sDeliveryDate = sdf.format(dDeliveryDate);
    }
    if (StringUtils.isNotEmpty(this.deliveryDateRange))
    {
      Date dDeliveryDateRange = this.cDeliveryDateRange.getTime();
      sDeliveryDateRange = sdf.format(dDeliveryDateRange);
    }

    odVO.setDeliveryDate(sDeliveryDate);
    odVO.setDeliveryDateRangeEnd(sDeliveryDateRange);

    odVO.setProductId(this.productId);
    odVO.setProductsAmount(this.productAmount);
    odVO.setSizeChoice(this.sizeIndicator);

    OrderVO orderVO = initializeOrderVO();
    Collection orderDetails = new ArrayList();
    orderDetails.add(odVO);
    orderVO.setOrderDetail((List) orderDetails);
    RecalculateOrderBO recalcOrderVo = new RecalculateOrderBO();

    //recalculate
    boolean clearODAmounts = false;
    recalcOrderVo.recalculate(this.con, orderVO, clearODAmounts);

    return orderVO;

  }


/*******************************************************************************************
 * resetProductAmounts()
 *******************************************************************************************
  * This method is used to update the order
  *
  */
  private void resetProductAmounts() throws Exception
  {
    //Instantiate UpdateOrderDAO
    UpdateOrderDAO uoDAO = new UpdateOrderDAO(this.con);

    //update the prices
    uoDAO.resetODUAmounts(this.productAmount, this.orderDetailId, this.csrId);

  }


 /*******************************************************************************************
  * setModifyFlag()
  *******************************************************************************************
  *
  * A change was detected.  Thus, update the MODIFY_ORDER_UPDATED flag in the request parameters
  *
  * @return nothing
  * @throws java.lang.Exception
  */
  private void setModifyFlag()
  {
    HashMap parameters = (HashMap) this.request.getAttribute    ( COMConstants.CONS_APP_PARAMETERS);
    parameters.put(COMConstants.MODIFY_ORDER_UPDATED,             COMConstants.CONS_YES);
    this.request.setAttribute(COMConstants.CONS_APP_PARAMETERS,   parameters);
  }




 /*******************************************************************************************
  * generateErrorNode()
  *******************************************************************************************
  *
  * Adds an error message to the error message XML doc
  *
  * @params messageText - Message text
  * @params fieldInError - Name of erred field (optional).
  * @params popupType - Type of popup window (optional).
  * @params popupConfirmation - Return page if popup confirmation selected (optional).
  * @params popupCancel - Return page if confirmation is cancelled (optional).
  */
  private void generateErrorNode(String messageText, String fieldInError,
                                   String popupType, String popupConfirmation, String popupCancel)
        throws Exception
  {

    String l_popupType = popupType;
    if (fieldInError == null && popupType == null) {
      l_popupType = POPUP_INDICATOR_OK;
    }

    // Generate top node
    Document errorMsgsXML = (Document) DOMUtil.getDefaultDocument();
    Element errMsgsTopElement = errorMsgsXML.createElement("ERROR_MESSAGES");
    errorMsgsXML.appendChild(errMsgsTopElement);

    if (messageText != null)
    {
      Element errorMsgElement = errorMsgsXML.createElement("ERROR_MESSAGE");
      errMsgsTopElement.appendChild(errorMsgElement);

      Element errMsgNode = errorMsgsXML.createElement("TEXT");
      errorMsgElement.appendChild(errMsgNode);
      errMsgNode.appendChild(errorMsgsXML.createTextNode(messageText));

      Element errFieldNode = errorMsgsXML.createElement("FIELDNAME");
      errorMsgElement.appendChild(errFieldNode);
      if (fieldInError != null)
        errFieldNode.appendChild(errorMsgsXML.createTextNode(fieldInError));

      Element popupIndicatorNode = errorMsgsXML.createElement("POPUP_INDICATOR");
      errorMsgElement.appendChild(popupIndicatorNode);
      if (l_popupType != null)
        popupIndicatorNode.appendChild(errorMsgsXML.createTextNode(l_popupType));

      Element popupGotoNode = errorMsgsXML.createElement("POPUP_GOTO_PAGE");
      errorMsgElement.appendChild(popupGotoNode);
      if (popupConfirmation != null)
        popupGotoNode.appendChild(errorMsgsXML.createTextNode(popupConfirmation));

      Element popupCancelNode = errorMsgsXML.createElement("POPUP_CANCEL_PAGE");
      errorMsgElement.appendChild(popupCancelNode);
      if (popupCancel != null)
        popupCancelNode.appendChild(errorMsgsXML.createTextNode(popupCancel));
    }

    this.hashXML.put(COMConstants.HK_ERROR_MESSAGES,  errorMsgsXML);

  }

/*******************************************************************************************
 * initializeOrderVO()
 *******************************************************************************************/
  private OrderVO initializeOrderVO() throws Exception
  {
  
    OrderVO orderVO = new OrderVO();

    OrderDAO orderDAO = new OrderDAO(this.con);

    com.ftd.customerordermanagement.vo.OrderVO comOrderVO = orderDAO.getOrder(this.orderGuid);
    
    orderVO.setCompanyId(comOrderVO.getCompanyId());
    orderVO.setOrderDate(new Date()); // use current date, this is a new order
    orderVO.setBuyerSignedIn(comOrderVO.getBuyerSignedInFlag());
    
    String customerEmailAddress = "" ;
    CachedResultSet rs = orderDAO.getOrderCustomerInfo(this.orderDetailId);
    if(rs.next())
    {
       customerEmailAddress = rs.getString("email_address");
    }
    orderVO.setBuyerEmailAddress(customerEmailAddress);

    return orderVO;
  }




}

