package com.ftd.mo.bo;

import com.ftd.customerordermanagement.dao.GcCouponDAO;
import com.ftd.security.SecurityManager;
import com.ftd.customerordermanagement.bo.CreditCardBO;
import com.ftd.customerordermanagement.bo.GcCouponBO;
import com.ftd.customerordermanagement.bo.GiftCardBO;
import com.ftd.customerordermanagement.bo.PaymentMethodBO;
import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.vo.CommonCreditCardVO;
import com.ftd.customerordermanagement.vo.GcCouponVO;
import com.ftd.mo.dao.CostCenterDAO;
import com.ftd.customerordermanagement.dao.CreditCardDAO;
import com.ftd.mo.dao.InvoiceDAO;
import com.ftd.customerordermanagement.dao.OrderDAO;
import com.ftd.customerordermanagement.dao.PaymentDAO;
import com.ftd.customerordermanagement.vo.CreditCardVO;
import com.ftd.customerordermanagement.vo.GiftCardVO;
import com.ftd.customerordermanagement.vo.PaymentVO;
import com.ftd.customerordermanagement.vo.SourceCodeVO;
import com.ftd.ftdutilities.GeneralConstants;
import com.ftd.mo.dao.UpdateOrderDAO;
import com.ftd.op.common.dao.CommonDAO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;

import com.ftd.security.exceptions.AuthenticationException;
import com.ftd.security.exceptions.ExpiredCredentialsException;
import com.ftd.security.exceptions.ExpiredIdentityException;
import com.ftd.security.exceptions.TooManyAttemptsException;
import java.io.StringReader;
import java.sql.Connection;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.naming.InitialContext;
import javax.transaction.UserTransaction;
import javax.xml.parsers.ParserConfigurationException;
//import oracle.xml.parser.v2.DOMParser;
//import oracle.xml.parser.v2.Document;
import org.xml.sax.InputSource;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
//import oracle.xml.parser.v2.XMLNode;

import org.w3c.dom.Element;

public class PaymentBO
{
  private Connection          conn                = null;
  private static Logger       logger              = new Logger("com.ftd.mo.bo.PaymentBO");
	
	// Class level constants
  private static final String DATA_ROOT_NODE = "root";
  private static final String STATUS_ROOT_NODE = "status";
	private static final String PAYMENT_XML = "payments_xml";
	private static final String LAST_CC_XML = "last_cc_xml";
	private static final String LAST_GD_XML = "last_gd_xml";
	private static final String BILLING_INFO = "billing_info";
	private static final String BILLING_INFO_OPTIONS = "billing_info_options";
	private static final String GD_SWITCH_XML = "gd_switch_xml";


  public PaymentBO(Connection conn)
  {
    this.conn = conn;
  }


 /**
	*	Method obtains data required for a Modify Order Invoice payment.
	*
	* @param sourceCode - Source Code
  * @return HashMap - XML documents required for a Modify Order Invoice Payment
  * @throws Exception
  */
  public HashMap getInvoicePayInfo(String sourceCode) throws Exception
  {
		HashMap paymentMap = new HashMap();
		InvoiceDAO iDAO = new InvoiceDAO(conn);
		
		// Check to make sure it is not an Invoice payment with a gift certificate
		// If it is, only return the payment method for gift certificate
		// any remaining amount will be applied to an invoice
		OrderDAO oDAO = new OrderDAO(conn);
		boolean biGcFlag = oDAO.getBillingInfoForCertificate(sourceCode);
		if(biGcFlag)
		{
			PaymentMethodBO pmBO = new PaymentMethodBO(conn);
			paymentMap.put(PAYMENT_XML ,pmBO.getPaymentMethod(COMConstants.GIFT_CERTIFICATE));

			// There will never be billing information for an IN requiring
			// a gift certificate so we just return the GC payment method info
			return paymentMap;
		}

		// Obtain billing info and billing info options
		Document billInfoXML = iDAO.getBillingInfo(sourceCode);
		Document billInfoOptionsXML = iDAO.getBillingInfoOptions(sourceCode);
		
		// Add billing info to payment HashMap
		paymentMap.put(BILLING_INFO, billInfoXML);
		paymentMap.put(BILLING_INFO_OPTIONS, billInfoOptionsXML);
		
		return paymentMap;
	}
	
	
 /**
	*	Method obtains data required for the Modify Order Payment Screen.
	*
	* @param orderDetailId - Order detail id
	* @param sourceCode - Source code
	* @param securitytoken - Session token
	* @param pageData - Page data passed back to front end
  * @return HashMap - XML documents required for the Modify Order Payment Screen
  * @throws Exception
  */
  public HashMap getPaymentInfo(String orderGuid, String orderDetailId,
		String sourceCode, HashMap pageData) throws Exception
  {
		// HashMap of XMLDocs which is returned
    HashMap paymentMap = null;

		// DAO's
		UpdateOrderDAO uoDAO = new UpdateOrderDAO(conn);
		OrderDAO oDAO = new OrderDAO(conn);
		
		// Obtain Source Code information
		SourceCodeVO scVO = oDAO.getSourceCodeRecord(sourceCode);
		
		// Set the partner id
		pageData.put("partner_id", scVO.getPartnerId());
		
		// Determine payment type
		// The original design obtained the payment methods from a cache.  The 
		// decision was made not to create a cache at this time.  The logic
		// below should be obtained from a cached object.  The cached object could 
		// be used for the WebOE payment and COM Add Billing screen as well.
		String payType = null;
		java.util.ArrayList paymentMethods = new com.ftd.customerordermanagement.dao.PaymentDAO(conn).getAllPaymentMethod();
		for(int i = 0; i < paymentMethods.size() && payType == null; i++)
		{
			com.ftd.customerordermanagement.vo.PaymentMethodVO pmVO = (com.ftd.customerordermanagement.vo.PaymentMethodVO)paymentMethods.get(i);
			if(pmVO.getpaymentMethodId().equalsIgnoreCase(scVO.getValidPayMethod()))
			{
				payType = pmVO.getpaymentType();
			}
		}
		
		// If an Invoice payment obtain Invoice payment info
		if(payType != null && payType.equalsIgnoreCase(COMConstants.CONS_INVOICE_PAYMENT_TYPE))
		{
			// Add payment type if Invoice
			pageData.put(COMConstants.HM_PAYMENT_TYPE, payType);
			pageData.put("payment_method", scVO.getValidPayMethod());

			paymentMap = getInvoicePayInfo(sourceCode);
		}
		// If not an Invoice payment obtain general payment information
		else
		{
			// Add payment method id as payment type to pageData only for general
			// payments
			pageData.put(COMConstants.HM_PAYMENT_TYPE, scVO.getValidPayMethod());

			paymentMap = getGenPayInfo(orderGuid, orderDetailId, sourceCode);
		}
				
		return paymentMap;
  }


 /**
	*	Method obtains general payment information required for the Modify Order 
	* Payment.
	*
	* @param orderDetailId - Order detail id
	* @param sourceCode - Source code 
	* @param securitytoken - Session id
	* @param pageData - Map of page data
  * @return HashMap - XML documents required for the Modify Order Payment Screen
  * @throws Exception
  */
  public HashMap getGenPayInfo(String orderGuid, String orderDetailId, String sourceCode) throws Exception
  {
		// HashMap of XMLDocs which is returned
    HashMap paymentMap = new HashMap();

		// BO's
		PaymentMethodBO pmBO = new PaymentMethodBO(conn);

		// DAO's
		CreditCardDAO ccDAO = new CreditCardDAO(conn);
		
		// Obtain the payment methods
		Document paymentsXML = pmBO.getPaymentMethods(sourceCode, false);
		paymentMap.put(PAYMENT_XML, paymentsXML);
		
		// Obtain last credit card used on the order
		Document lastCCXML = ccDAO.getLastCreditCard(orderDetailId);

		// Modify credit card information so only the last four digits are displayed
		if (lastCCXML != null)
		{
			String XPath = "//LAST_CREDITCARDS/LAST_CREDITCARD";
			NodeList nodeList = DOMUtil.selectNodes(lastCCXML,XPath);
			Node node = null;

			if (nodeList != null)
			{
				for (int i = 0; i < nodeList.getLength(); i++)
				{
					node = (Node) nodeList.item(i);
					if (node != null)
					{
						String creditCard = (DOMUtil.selectSingleNode(node,"cc_number/text()").getNodeValue());
						DOMUtil.selectSingleNode(node,"cc_number/text()").setNodeValue(creditCard.substring(creditCard.length()-4));
					}
				}
			}
			paymentMap.put(LAST_CC_XML, lastCCXML);
		}
		
		// get last gift card used
		Document lastGDXML = getLastGiftCardUsedDoc(conn, orderGuid);
		paymentMap.put(LAST_GD_XML, lastGDXML);
		
		// get global gift card switch setting
		Document gdGlobalSwitch = GiftCardBO.getGiftCardGlobalSwitch(conn);
		paymentMap.put(GD_SWITCH_XML, gdGlobalSwitch);

		return paymentMap;
  }

  /**
   * If a gift card was used to pay for the specified order, return the card and pin in a document under
   * LAST_GIFTCARDS/LAST_GIFTCARD/gift_card_number
   * LAST_GIFTCARDS/LAST_GIFTCARD/gift_card_pin 
   * 
   * @param conn
   * @param orderGuid
   * @return
   * @throws Exception
   */
  public Document getLastGiftCardUsedDoc(Connection conn, String orderGuid) throws Exception
  {
      Document doc = DOMUtil.getDefaultDocument();
      Element root = (Element) doc.createElement("LAST_GIFTCARDS");
      doc.appendChild(root);
      
      String lastGD=null, lastPIN=null;
      OrderDAO dao = new OrderDAO(conn);
      
      List <PaymentVO> payments = dao.getGiftCardPayments(orderGuid);
      if (payments != null && !payments.isEmpty())
      {
          lastGD = payments.get(0).getCardNumber();
          lastGD = lastGD.substring(lastGD.length()-4);
          lastPIN = payments.get(0).getGiftCardPin();
      }
      else 
      {
          return doc;
      }
      
      /**
       * add to LAST_GIFTCARDS/LAST_GIFTCARD/gift_card_[number|pin]
       */
      Element node;
      node = (Element)doc.createElement("LAST_GIFTCARD");
      root.appendChild(node);

      Element node2;
      node2 = (Element)doc.createElement("gift_card_number");
      node2.appendChild(doc.createTextNode(lastGD));
      node.appendChild(node2);
      
      node2 = (Element)doc.createElement("gift_card_pin");
      node2.appendChild(doc.createTextNode(lastPIN));
      node.appendChild(node2);

      return doc;
  }


/**
	*	Apply a credit card, no charge, or gift certificate payment.
	*
	* @param orderGuid - Order Guid
	* @param orderDetailId - Order detail id
	* @param mgrCode - Approval username
	* @param mgrPassword - Approval password
	* @param context - Security context
	* @param gcPaymentVO - Gift certificate payment
	* @param paymentVO - General payment
	* @param gcCouponVO - Gift certificate information
	* @param creditCardVO - Credit card information
	* @param commonCreditCardVO - Credit card information used for authorization
  * @return HashMap - XML documents required for the Modify Order Payment Screen
  * @throws Exception
  */
  public Document applyGenPayment(String orderGuid, String orderDetailId, 
				String mgrCode, 
				String mgrPassword, 
				String context, 
				PaymentVO gcPaymentVO, //gift card
				PaymentVO paymentVO, //??
				GcCouponVO gcCouponVO, 
				GiftCardVO giftCardVO, 
				CreditCardVO creditCardVO,
				CommonCreditCardVO commonCreditCardVO) throws Exception
  {
		// DAOs
		CreditCardDAO ccDAO = new CreditCardDAO(conn);
		PaymentDAO pDAO = new PaymentDAO(conn);
		
		// Variables
		Document statusDoc = null;
		String creditCardId = null;
		
		// If credit card exists add/update credit card information
		if(creditCardVO != null)
		{
			creditCardId = ccDAO.addCreditCardMO(creditCardVO, orderDetailId).toString();
			
			// Populate payment with the credit card id
			paymentVO.setCardId(creditCardId);
		} 
		if (giftCardVO != null) {
			
			String ccId = ccDAO.addCreditCardMO(giftCardVO.getCreditCardVO(), orderDetailId).toString();
			gcPaymentVO.setCardId(ccId);
		}
		
		// If gift certificate/coupon exists redeem the gift certificate/coupon
		if(gcCouponVO != null)
		{
			GcCouponDAO gcDAO = new GcCouponDAO(conn);
			
			String redeemStatus = gcDAO.redeemGcCoupon(gcCouponVO, orderDetailId);
			
			// If the gift certificate was already redeemed send back an invalid status
			// document
			if(!redeemStatus.equalsIgnoreCase("Redeemed"))
			{
				//will result in a rollback
				return createStatusDocument(COMConstants.STATUS_INVALID, 
					COMConstants.ACTION_DECLINE_GCCR,
					COMConstants.ACTION_DECLINE_GCCR,
					false);
			}
		}
		
		// Insert the payments
		ArrayList payList = new ArrayList();
		if(paymentVO != null) payList.add(paymentVO);
		if(gcPaymentVO != null) payList.add(gcPaymentVO); //gift card
		pDAO.addOrderPaymentsMO((PaymentVO[])payList.toArray(new PaymentVO[0]));

		
		// Create successful status document
    return createStatusDocument(COMConstants.STATUS_SUCCESS, null,
                                                      null, false);
	}


  /**
	*	Apply an invoice payment.
	*
	* @param orderGuid - Order Guid
	* @param orderDetailId - Order detail id
	* @param sourceCode - Source code
	* @param partnerId - Source code partner id
	* @param paymentVO - General payment
	* @param gcCouponVO - Gift certificate information
	* @param gcPaymentVO - Gift certificate payment
  * @return HashMap - XML documents required for the Modify Order Payment Screen
  * @throws Exception
  */
  public Document applyInvoicePayment(String orderGuid, String orderDetailId, 
	String sourceCode, String partnerId, PaymentVO paymentVO, HashMap billingInfo,
	GcCouponVO gcCouponVO, PaymentVO gcPaymentVO) throws Exception
  {
		// DAO
		CreditCardDAO ccDAO = new CreditCardDAO(conn);
		PaymentDAO pDAO = new PaymentDAO(conn);
				
		// Variables
		UserTransaction ut = null;
		Document statusDoc = null;
		
		// Validate the gift certificate
		if(gcCouponVO != null)
		{
			statusDoc = authorizeGc(gcPaymentVO, gcCouponVO);			
			if(statusDoc != null)
				return statusDoc;
		}
		
		// Authorize the cost center if billing info exists
		if(billingInfo != null)
		{
			statusDoc = validateCostCenter(sourceCode, partnerId, billingInfo);			
			if(statusDoc != null)
				return statusDoc;
		}
		
		// Create user transaction
		ut = createUserTransaction();
		ut.begin();
		
		// If gift certificate/coupon exists redeem the gift certificate/coupon
		if(gcCouponVO != null)
		{
			GcCouponDAO gcDAO = new GcCouponDAO(conn);
			
			String redeemStatus = gcDAO.redeemGcCoupon(gcCouponVO, orderDetailId);
			
			// Send back an invalid status document if the gift certificate was
			// already redeemed
			if(!redeemStatus.equalsIgnoreCase("Redeemed"))
			{
				rollback(ut);  
				return createStatusDocument(COMConstants.STATUS_INVALID, 
					COMConstants.ACTION_DECLINE_GCCR,
					COMConstants.ACTION_DECLINE_GCCR,
					false);
			}
		}
		
		// Insert the payments
		ArrayList payList = new ArrayList();
		if(paymentVO != null) payList.add(paymentVO);
		if(gcPaymentVO != null) payList.add(gcPaymentVO);
		pDAO.addOrderPaymentsMO((PaymentVO[])payList.toArray(new PaymentVO[0]));

		// Insert billing information if it exists
		// The p-card credit card is not inserted in Modify Order.  The p-card
		// credit card is applied to the payment within the Data Massager
		if(billingInfo != null)
		{
			pDAO.updateCoBrandMO(orderGuid, billingInfo);			
		}
		
		// Commit the transaction
		ut.commit();
		
		// Create successful status document
    return createStatusDocument(COMConstants.STATUS_SUCCESS, null,
                                                      null, false);
	}
	
	
/**
	*	Authorize gift certificate/coupon, credit card, or no charge payment.
	*
	* @param orderGuid - Order Guid
	* @param orderDetailId - Order detail id
	* @param mgrCode - Approval username
	* @param mgrPassword - Approval password
	* @param context - Security context
	* @param creditCardVO - Credit card information
	* @param commonCreditCardVO - Credit card information used for authorization
	* @param gcPaymentVO - Gift certificate payment
	* @param paymentVO - General payment
	* @param gcCouponVO - Gift certificate information
	* @return Document - Status document
  * @throws Exception
  */
  public Document authorize(String orderGuid, String orderDetailId, String mgrCode, 
		String mgrPassword, String context, CreditCardVO creditCardVO,
		CommonCreditCardVO commonCreditCardVO, PaymentVO gcPaymentVO, 
		PaymentVO paymentVO, GcCouponVO gcCouponVO, GiftCardVO giftCardVO) 
		throws Exception
  {
		// If coupon exists and credit card does not validate gift certificate
		if(gcCouponVO != null && commonCreditCardVO == null)
		{
			return authorizeGc(gcPaymentVO, gcCouponVO);
		}
        // If gift card exists and credit card does not validate gift certificate
        if(giftCardVO != null && commonCreditCardVO == null)
        {
            return validateGd(gcPaymentVO, giftCardVO, orderDetailId);
        }
		// If credit card exists authorize the credit card
		else if(commonCreditCardVO != null)
		{
			return authorizeCC(orderGuid, orderDetailId, creditCardVO, commonCreditCardVO, paymentVO);				
		}
		// If manager approval code exists authorize the no charge
		else if(mgrCode != null && !mgrCode.equalsIgnoreCase(""))
		{
			return authorizeNC(mgrCode, mgrPassword, context);
		}
		
		return null;
	}


  /**
	*	Authorize no charge.
	*
	* @param mgrCode - Approval username
	* @param mgrPassword - Approval password
	* @param context - Security context
	* @return Document - Status document
  * @throws Exception
  */
  private Document authorizeNC(String mgrCode, String mgrPassword,
		String context) 
		throws Exception
  {
		// Variables
	  Object token = null;
	  boolean authenticated = false;
		String unitID = null;
    
		// Obtain unit id
		unitID = ConfigurationUtil.getInstance().getProperty(COMConstants.SECURITY_FILE, COMConstants.UNIT_ID);

		try
		{
			// Authenticate no charge
			token = SecurityManager.getInstance().authenticateIdentity(context, unitID, mgrCode, mgrPassword);		  
			authenticated = true;
		}
		catch(ExpiredIdentityException e){logger.info("User was not authenticated:", e);}
		catch(ExpiredCredentialsException e){logger.info("User was not authenticated:", e);}
		catch(AuthenticationException e){logger.info("User was not authenticated:", e);}
		catch(TooManyAttemptsException e){logger.info("User was not authenticated:", e);}
			
 	  // Check authentication status and user permission status
		if(!authenticated || !SecurityManager.getInstance().assertPermission(COMConstants.RESOURCE_CONTEXT, token, 
										COMConstants.RESOURCE_NO_CHARGE, COMConstants.RESOURCE_NO_CHARGE_PERMISSION))
		{
				 return createStatusDocument(COMConstants.STATUS_INVALID, COMConstants.ACTION_DECLINE_NC, 
					COMConstants.ACTION_DECLINE_NC, false);
		}
		
		return null;
	}
	

/**
	*	Authorize credit card.
	*
	* @param orderGuid - Order guid
	* @param orderDetailId - Order detail id
	* @param creditCardVO - Credit card information
	* @param commonCreditCardVO - Credit card information used for authorization
	* @param paymentVO - Payment information
	* @return Document - Status document
  * @throws Exception
  */
  private Document authorizeCC(String orderGuid, String orderDetailId, 
		CreditCardVO creditCardVO, CommonCreditCardVO commonCreditCardVO, 
		PaymentVO paymentVO) 
		throws Exception
  {
		// DAOs
		CreditCardDAO ccDAO = new CreditCardDAO(conn);
		
		// BOs
		CreditCardBO ccBO = new CreditCardBO(conn);
		
		// Variables
		CreditCardVO ccVO = new CreditCardVO();
		
		// If no credit card number exists the last credit card was used
		// Obtain the last credit card information
		if(commonCreditCardVO.getCreditCardNumber() == null || commonCreditCardVO.getCreditCardNumber().equalsIgnoreCase(""))
		{
			ccVO = ccDAO.getLastCreditCardRS(orderDetailId);
			
			creditCardVO.setCcNumber(ccVO.getCcNumber());
			commonCreditCardVO.setCreditCardNumber(ccVO.getCcNumber());
			commonCreditCardVO.setCcId(String.valueOf(ccVO.getCcId()));
			paymentVO.setCardNumber(ccVO.getCcNumber());
		}
		
                // If client authorization is off or CSR says to skip authorization, just get out
                String ccasAuthClientActive = ConfigurationUtil.getInstance().getFrpGlobalParm(COMConstants.CCAS_CONTEXT, COMConstants.CONS_CCAS_AUTH_CLIENT_ACTIVE);
                if ("N".equalsIgnoreCase(ccasAuthClientActive) || commonCreditCardVO.isCcSkipAuth()) 
                {
                    if (commonCreditCardVO.isCcSkipAuth())  {
                      logger.info("Skipping CC authorization per CSR's choice");
                    } else {
                      logger.info("Skipping CC authorization since CCAS client authorization is inactive");
                    }
                    paymentVO.setCardId(commonCreditCardVO.getCcId());
                    paymentVO.setCardNumber(commonCreditCardVO.getCreditCardNumber());
                    return null;
                }
                
		// Authorize the credit card
		// If the credit card was not manually authorized
		if(paymentVO.getAuthOverrideFlag().equalsIgnoreCase("N"))
		{
			CommonDAO opDAO = new CommonDAO(conn);
			com.ftd.op.common.vo.CommonCreditCardVO cardinalVO = opDAO.getCardinalData(new Long(paymentVO.getPaymentId()).toString());
			if("Y".equalsIgnoreCase(paymentVO.getCardinalVerifiedFlag())){
				commonCreditCardVO.setEci(cardinalVO.getEci());
				commonCreditCardVO.setCavv(cardinalVO.getCavv());
				commonCreditCardVO.setXid(cardinalVO.getXid());
				commonCreditCardVO.setUcaf(cardinalVO.getUcaf());
			}
			else{
				commonCreditCardVO.setEci("");
				commonCreditCardVO.setCavv("");
				commonCreditCardVO.setXid("");
				commonCreditCardVO.setUcaf("");
			}
			
			commonCreditCardVO = ccBO.authorize(commonCreditCardVO, orderGuid);
		}
		else
		{
			// Return if the credit card was manually authorized
			return null;
		}
		
                // Check credit card authorization status
                String ccAuthStatus = commonCreditCardVO.getValidationStatus();
                if(ccAuthStatus == null || !ccAuthStatus.equalsIgnoreCase("A"))
                {
                    // Create invalid status document for CCAUTH errors
                    if (COMConstants.AUTH_RESULT_ERROR.equalsIgnoreCase(ccAuthStatus)) {
                        if(commonCreditCardVO.getCreditCardType().equalsIgnoreCase(COMConstants.MILITARY_STAR)) {
                            return createStatusDocument(COMConstants.STATUS_INVALID, 
                                    COMConstants.ACTION_ERROR_CREDIT_MS, COMConstants.ACTION_ERROR_CREDIT_MS,
                                    false);
                        } else {
                            return createStatusDocument(COMConstants.STATUS_INVALID, 
                                    COMConstants.ACTION_ERROR_CREDIT, COMConstants.ACTION_ERROR_CREDIT,
                                    false);
                        }
                    }
                    // Create invalid status document for CC declines
                    else if(commonCreditCardVO.getCreditCardType().equalsIgnoreCase(COMConstants.MILITARY_STAR))
                    {
                            return createStatusDocument(COMConstants.STATUS_INVALID, 
                                    COMConstants.ACTION_DECLINE_CREDIT_MS, COMConstants.ACTION_DECLINE_CREDIT_MS,
                                    false);
                    }
                    else
                    {
                            return createStatusDocument(COMConstants.STATUS_INVALID, 
                                    COMConstants.ACTION_DECLINE_CREDIT, COMConstants.ACTION_DECLINE_CREDIT,
                                    false);
                    }
		}
		else
		{
				// Set payment credit card auth information
				paymentVO.setCardId(commonCreditCardVO.getCcId());
				paymentVO.setCardNumber(commonCreditCardVO.getCreditCardNumber());
				paymentVO.setAuthResult("AP");
				paymentVO.setAuthorization(commonCreditCardVO.getApprovalCode());
				paymentVO.setAvsCode(commonCreditCardVO.getAVSIndicator());
				paymentVO.setAcqRefNumber(commonCreditCardVO.getAcquirerReferenceData());
                                
                                // Defect 3256
                                if(commonCreditCardVO.getCreditCardType().equalsIgnoreCase(COMConstants.MILITARY_STAR))
                                {
                                    paymentVO.setAafesTicketNumber(commonCreditCardVO.getAcquirerReferenceData());
                                }
		}
		
		return null;
	}


/**
	*	Authorize gift certificate/coupon payment.
	*
	* @param gcPaymentVO - Gift certificate payment
	* @param gcCouponVO - Gift certificate information
	* @return Document - Status document
  * @throws Exception
  */
  private Document authorizeGc(PaymentVO gcPaymentVO, GcCouponVO gcCouponVO) 
		throws Exception
  {
		// BOs
		GcCouponBO gccBO = new GcCouponBO(conn);
		
		// Variables
		String gcStatus = null;
		boolean amountOwed = false;
		String gcAmount = null;
		String netOwed = null;
		String message = null;
		
		// Authorize the gift certificate
		Document gcResultDoc = gccBO.validateGiftCertificateCoupon(gcCouponVO.getGcCouponNumber(),
			String.valueOf(gcPaymentVO.getCreditAmount()));
			
		// Obtain results of gift certificate/coupon validation
		if (gcResultDoc != null)
		{
			String XPath = "//root";
			NodeList nodeList = DOMUtil.selectNodes(gcResultDoc,XPath);
			Node node = null;
			if (nodeList != null)
			{
				for (int i = 0; i < nodeList.getLength(); i++)
				{
					node = (Node) nodeList.item(i);
					if (node != null)
					{
						gcStatus = DOMUtil.selectSingleNode(node,"gcc_status/text()").getNodeValue();
						if(gcStatus.equalsIgnoreCase("invalid"))
						{
							message = DOMUtil.selectSingleNode(node,"message/text()").getNodeValue();							
						}
						else
						{
							amountOwed = DOMUtil.selectSingleNode(node,"amount_owed/text()").getNodeValue().equalsIgnoreCase("true")?true:false;
							netOwed = DOMUtil.selectSingleNode(node,"net_owed/text()").getNodeValue();
							gcAmount = DOMUtil.selectSingleNode(node,"amount/text()").getNodeValue();							
						}
					}
				}
			}
		}
			
		// If the gift certificate/coupon was invalid create an invalid status doc
		if(gcStatus.equalsIgnoreCase("invalid"))
		{
			return createStatusDocument(COMConstants.STATUS_INVALID, 
				COMConstants.ACTION_DECLINE_GCC,
				message,
				false);				
		}
		// If the gift certificate/coupon was valid but there is still an amount
		// owed
		else if(gcStatus.equalsIgnoreCase("valid") && amountOwed)
		{
	    // get an conversion object customised for a particular locale
			NumberFormat nf = java.text.NumberFormat.getInstance( );
			// set whether you want commas (or locale equivalent) inserted
			nf.setGroupingUsed(true);
			// set how many places you want to the right of the decimal.
			nf.setMinimumFractionDigits(2);
			nf.setMaximumFractionDigits(2);
			// set how many places you want to the left of the decimal.
			nf.setMinimumIntegerDigits(1);
			//nf.setMaximumIntegerDigits(6);

			// Format net amount owed
			if((netOwed != null) && (!netOwed.equals(COMConstants.EMPTY_STRING)))
			{
				netOwed = nf.format(Double.valueOf(netOwed));
			}

			// Create status document
			Document statusDoc = createGcAmtOwedStatusDocument(COMConstants.STATUS_SUCCESS, 
				COMConstants.ACTION_AMOUNT_OWED,
				"",
				netOwed, gcAmount, false);		
				
			return statusDoc;
		}

		return null;
	}


  private Document validateGd(PaymentVO gcPaymentVO, GiftCardVO giftCardVO, String orderDetailId)
  throws Exception
  {
      GiftCardBO gdBO = new GiftCardBO(conn);
      
      // Variables
      String gdStatus = null;
      boolean amountOwed = false;
      String gdAmount = null;
      String netOwed = null;
      String message = null;
      
      // Validate the gift card
      Document gdResultDoc = gdBO.validateGiftCard(gcPaymentVO.getOrderGuid(), orderDetailId,
              giftCardVO.getGcCouponNumber(), giftCardVO.getPin(), String.valueOf(gcPaymentVO.getCreditAmount()));
          
      // Obtain results of gift certificate/coupon validation
      if (gdResultDoc != null)
      {
          String XPath = "//root";
          NodeList nodeList = DOMUtil.selectNodes(gdResultDoc,XPath);
          Node node = null;
          if (nodeList != null)
          {
              for (int i = 0; i < nodeList.getLength(); i++)
              {
                  node = (Node) nodeList.item(i);
                  if (node != null)
                  {
                      gdStatus = DOMUtil.selectSingleNode(node,"gcc_status/text()").getNodeValue();
                      if(gdStatus.equalsIgnoreCase("invalid"))
                      {
                          message = DOMUtil.selectSingleNode(node,"message/text()").getNodeValue();                           
                      }
                      else
                      {
                          amountOwed = DOMUtil.selectSingleNode(node,"amount_owed/text()").getNodeValue().equalsIgnoreCase("true")?true:false;
                          netOwed = DOMUtil.selectSingleNode(node,"net_owed/text()").getNodeValue();
                          gdAmount = DOMUtil.selectSingleNode(node,"amount/text()").getNodeValue();                           
                      }
                  }
              }
          }
      }
          
      // If the gift certificate/coupon was invalid create an invalid status doc
      if(gdStatus == null || gdStatus.equalsIgnoreCase("invalid"))
      {
          return createStatusDocument(COMConstants.STATUS_INVALID, COMConstants.ACTION_DECLINE_GCC,
              message, false);             
      }
      // If the gift certificate/coupon was valid but there is still an amount
      // owed
      else if(gdStatus.equalsIgnoreCase("valid") && amountOwed)
      {
      // get an conversion object customised for a particular locale
          NumberFormat nf = java.text.NumberFormat.getInstance( );
          // set whether you want commas (or locale equivalent) inserted
          nf.setGroupingUsed(true);
          // set how many places you want to the right of the decimal.
          nf.setMinimumFractionDigits(2);
          nf.setMaximumFractionDigits(2);
          // set how many places you want to the left of the decimal.
          nf.setMinimumIntegerDigits(1);
          //nf.setMaximumIntegerDigits(6);

          // Format net amount owed
          if((netOwed != null) && (!netOwed.equals(COMConstants.EMPTY_STRING)))
          {
              netOwed = nf.format(Double.valueOf(netOwed));
          }

          // Create status document
          Document statusDoc = createGcAmtOwedStatusDocument(COMConstants.STATUS_SUCCESS, 
              COMConstants.ACTION_AMOUNT_OWED, "", netOwed, gdAmount, false);      
              
          return statusDoc;
      }

      return null;
  }


/**
	*	Validates the cost center information.
	*
	* @param sourceCode - Source code
	* @param partnerId - Partner Id
	* @param billingInfo - Billing/co-brand information
	* @return Document - Status document
  * @throws Exception
  */
  public Document validateCostCenter(String sourceCode, String partnerId,
	HashMap billingInfo) throws Exception
  {
		CostCenterDAO ccDAO = null;
		StringBuffer costCenterId = null;
		
		// If the partner id is ADVO or TARGET create the cost center id
		if(partnerId != null && (partnerId.equalsIgnoreCase(COMConstants.CONS_ADVO_CODE)
		|| partnerId.equalsIgnoreCase(COMConstants.CONS_TARGET_CODE)))
		{
			ccDAO = new CostCenterDAO(conn);

			// Build CostCenterId
			// Cost center id is a conglomeration of all input fields displayed to the user
			costCenterId = new StringBuffer();
			HashMap info = null;
			String value = "value";
			for ( int i = 0; i < billingInfo.size(); i++ ) 
			{
				info = (HashMap) billingInfo.get("promptName" + i);
				if (info != null) 
				{	
					costCenterId.append((String) info.get(value));
				}
				else 
				{
					break;
				}
			}
		}
		
		// Validate cost center for ADVO
		if(partnerId != null && partnerId.equalsIgnoreCase(COMConstants.CONS_ADVO_CODE))
		{
			// If cost center does not exist return an invalid status
			if(!ccDAO.costCenterExists(costCenterId.toString(), partnerId, null))
				return createStatusDocument(COMConstants.STATUS_INVALID, 
					COMConstants.ACTION_DECLINE_ICC,
					COMConstants.ACTION_DECLINE_ICC, false);
		}
		// Validate cost center for Target
		else if (partnerId != null && partnerId.equalsIgnoreCase(COMConstants.CONS_TARGET_CODE))
		{
			// If cost center does not exist return an invalid status
			if(!ccDAO.costCenterExists(costCenterId.toString(), partnerId, sourceCode))
				return createStatusDocument(COMConstants.STATUS_INVALID, 
					COMConstants.ACTION_DECLINE_ICC,
					COMConstants.ACTION_DECLINE_ICC, false);
		}
		
		return null;
	}


/**
	*	Converts string to XML.
	*
	* @param xmlString - XML in the form of a String
	* @return Document - String as an XML document
  * @throws Exception
  */
  private Document toXML(String xmlString) throws Exception
  {
    return DOMUtil.getDocument(xmlString);
  }


/**
	*	Creates user transaction.
	*
	* @return UserTransaction - User transaction
  * @throws Exception
  */
  private UserTransaction createUserTransaction() 
		throws Exception
  {
 		// Obtain the transaction timeout
		int transactionTimeout = Integer.parseInt((String)ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, COMConstants.TRANSACTION_TIMEOUT_IN_SECONDS));

		// Obtain UserTransaction object
		String jndi_usertransaction_entry = ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, COMConstants.JNDI_USERTRANSACTION_ENTRY);

		// Obtain initial context
		InitialContext iContext = new InitialContext();
		
		// Generate UserTransaction
		UserTransaction userTransaction = (UserTransaction)  iContext.lookup(jndi_usertransaction_entry);

		// Set timeout
		userTransaction.setTransactionTimeout(transactionTimeout);
		
		return userTransaction;
 }
 
  /**
   * Create status document
   * @param String  - status
   * @param String  - action
   * @param String  - message
   * @param boolean  - error flag
   * @return Document - status
   * @throws Exception
   */
  private Document createStatusDocument(String status, String action, 
		String msg, boolean isError)
        throws Exception
  {
    // Obtain message text
		String messageText = ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, msg);
		
		// If no messageText was found it was an existing message passed from 
		// another system.
		if(messageText == null || messageText.equals(""))
			messageText = msg;

    Document returnDocument = DOMUtil.getDefaultDocument();
    Element root = (Element) returnDocument.createElement(DATA_ROOT_NODE);
    returnDocument.appendChild(root);

    Element errorNode = (Element)returnDocument.createElement("is_error");
    String error = isError?"Y":"N";
		errorNode.appendChild(returnDocument.createTextNode(error));
    root.appendChild(errorNode);
    
    Element statusElement = (Element) returnDocument.createElement(STATUS_ROOT_NODE);
    root.appendChild(statusElement);
    
    Element dataElement = (Element)returnDocument.createElement("data");  
    statusElement.appendChild(dataElement);

    Element node = (Element)returnDocument.createElement("message_status");
    node.appendChild(returnDocument.createTextNode(status));
    dataElement.appendChild(node);
   
    node = (Element)returnDocument.createElement("message_action");
    node.appendChild(returnDocument.createTextNode((action == null || action.equals("") ? "" : action)));
    dataElement.appendChild(node);

    node = (Element)returnDocument.createElement("message_text");
    node.appendChild(returnDocument.createTextNode(messageText));
    dataElement.appendChild(node);
    
    root.appendChild(statusElement);
    
    return returnDocument;  
  }


  /**
   * Create status document when an amount is still owed for a gift certificate
	 * 
   * @param status  - Status
   * @param action  - Action
   * @param msg  - Message
   * @param amountOwed  - Net amount owed
   * @param gcAmount  - Gift certificate amount
   * @param isError  - Signifies an error occurred
   * @return Document - Status document
   * @throws Exception
   */
  private Document createGcAmtOwedStatusDocument(String status, String action, 
		String msg, String amountOwed, String gcAmount, boolean isError)
        throws Exception
  {
    // Obtain message text
		String messageText = ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, msg);
		
		// If no messageText was found it was an existing message passed from 
		// another system.
		if(messageText == null || messageText.equals(""))
			messageText = msg;

    Document returnDocument = DOMUtil.getDefaultDocument();
    Element root = (Element) returnDocument.createElement(DATA_ROOT_NODE);
    returnDocument.appendChild(root);

    Element errorNode = (Element)returnDocument.createElement("is_error");
    String error = isError?"Y":"F";
		errorNode.appendChild(returnDocument.createTextNode(error));
    root.appendChild(errorNode);

    Element statusElement = (Element) returnDocument.createElement(STATUS_ROOT_NODE);
    root.appendChild(statusElement);
    
    Element dataElement = (Element)returnDocument.createElement("data");  
    statusElement.appendChild(dataElement);

    Element node = (Element)returnDocument.createElement("message_status");
    node.appendChild(returnDocument.createTextNode(status));
    dataElement.appendChild(node);
   
    node = (Element)returnDocument.createElement("message_action");
    node.appendChild(returnDocument.createTextNode(action));
    dataElement.appendChild(node);

    node = (Element)returnDocument.createElement("message_text");
    node.appendChild(returnDocument.createTextNode(messageText));
    dataElement.appendChild(node);
    
    node = (Element)returnDocument.createElement("amount_owed");
    node.appendChild(returnDocument.createTextNode(amountOwed));
    dataElement.appendChild(node);

    node = (Element)returnDocument.createElement("gc_amount");
    node.appendChild(returnDocument.createTextNode(gcAmount));
    dataElement.appendChild(node);

    root.appendChild(statusElement);
    
    return returnDocument;  
  }


 /**
  * Rollback user transaction when exceptions occur
  * @param userTransaction - user transaction  
  * @return void
  * @throws none
  */ 
  private void rollback(UserTransaction userTransaction)
  {
     try  
     {
       if (userTransaction != null)  
       {
         // rollback the user transaction
         userTransaction.rollback();
         logger.info("User transaction rolled back");
       }
     } 
     catch (Exception ex)  
     {
       logger.error(ex);
     } 
  } 
}