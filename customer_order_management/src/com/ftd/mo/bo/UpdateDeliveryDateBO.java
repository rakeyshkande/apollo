package com.ftd.mo.bo;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.dao.CommentDAO;
import com.ftd.customerordermanagement.dao.LockDAO;
import com.ftd.customerordermanagement.dao.OrderDAO;
import com.ftd.customerordermanagement.util.COMDeliveryDateUtil;
import com.ftd.customerordermanagement.vo.CommentsVO;
import com.ftd.ftdutilities.DeliveryDateUTIL;
import com.ftd.ftdutilities.OEDeliveryDateParm;
import com.ftd.messaging.constants.MessagingConstants;
import com.ftd.messaging.dao.MessageDAO;
import com.ftd.messaging.util.MessageUtil;
import com.ftd.messaging.util.MessagingHelper;
import com.ftd.messaging.util.MessagingServiceLocator;
import com.ftd.messaging.vo.FloristStatusVO;
import com.ftd.messaging.vo.MessageVO;
import com.ftd.mo.dao.UpdateOrderDAO;
import com.ftd.mo.util.ModifyOrderUTIL;
import com.ftd.mo.vo.MessageStatusVO;
import com.ftd.op.common.to.ResultTO;
import com.ftd.op.mercury.to.ADJMessageTO;
import com.ftd.op.mercury.to.BaseMercuryMessageTO;
import com.ftd.op.mercury.to.CANMessageTO;
import com.ftd.op.order.to.RWDFloristTO;
import com.ftd.op.order.to.RWDTO;
import com.ftd.op.venus.to.AdjustmentMessageTO;
import com.ftd.op.venus.to.CancelOrderTO;
import com.ftd.op.venus.to.OrderTO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.FTDServiceChargeUtility;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.ServiceChargeVO;
import com.ftd.security.SecurityManager;
import com.ftd.security.cache.vo.UserInfo;

import java.math.BigDecimal;

import java.sql.Connection;

import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import javax.naming.InitialContext;

import javax.servlet.http.HttpServletRequest;

import javax.transaction.UserTransaction;



import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;


public class UpdateDeliveryDateBO {

 /*******************************************************************************************
 * Class level variables
 *******************************************************************************************/
    private HttpServletRequest request = null;
    private Connection conn = null;
    private ActionMapping mapping = null;

    private static Logger logger = 
        new Logger("com.ftd.mo.bo.UpdateDeliveryDateBO");
        
    //request parameters required to be sent back to XSL all the time.
    private String customerId = null;
    private String deliveryDate = "";
    private String deliveryDateRange = "";
    private String externalOrderNumber = null;
    private String floristId = null;
    private String newFloristId = null;
    private String masterOrderNumber = null;
    private String origDeliveryDate = "";
    private String origDeliveryDateRange = "";
    private String occasion = null;
    private String orderDetailId = null;
    private String orderDispCode = null;
    private String orderGuid = null;
    private String originId = null;
    private String recipientCity = null;
    private String recipientCountry = null;
    private String recipientState = null;
    private String recipientZipCode = null;
    private String shipDate = null;
    private String shipMethod = null;
    private String sourceCode = null;

    private String productType = null;

    //other variables needed for this class
    private String actionType = null;
    private String csrId = null;
    private String displayFormat = null;
    private String context = null;
    private String sessionId = null;
    private HashMap hashXML = new HashMap();
    private HashMap pageData = new HashMap();
    private String productId = null;
    private String productAmount = null;
    private String serviceFee = null;
    private String vendorFlag = null;
    private boolean wasMaxRetrieved = false;
    private String origShipDate = "";
    private Calendar cDeliveryDate = null;
    private Calendar cOrigDeliveryDate = null;
    private Calendar cDeliveryDateRangeEnd = null;
    private Calendar cShipDate = null;
    private Calendar cOrigShipDate = null;
    private String selectionData = null;
    
    private String askMessageComments = "";
    // constants
    private static final String CANCEL_MSG_COMMENT = "Please cancel this order.  Thank you.";
    private static final String ADJ_MSG_COMMENT = "Adjustment created.";


    /*******************************************************************************************
 * Constructor 1
 *******************************************************************************************/
    public UpdateDeliveryDateBO() {
    }


    /*******************************************************************************************
 * Constructor 2
 *******************************************************************************************/
    public UpdateDeliveryDateBO(HttpServletRequest request, Connection conn, ActionMapping mapping) {
        this.request = request;
        this.conn = conn;
        this.mapping = mapping;
    }


    /*******************************************************************************************
  * processRequest()
  ******************************************************************************************
  *
  * @return HashMap - contains 0 - many XML documents of data, 1 HashMap each for page
  *                   details and search criteria
  * @throws
  */
    public HashMap processRequest() throws Exception, Throwable {
        //HashMap that will be returned to the calling class
        HashMap returnHash = new HashMap();

        //Get info from the request object
        getRequestInfo(this.request);

        getConfigInfo();

        try{        
            //process the action
            processMain();
        }
        catch(Throwable t)
        {
          throw t;
        }
        //populate the remainder of the fields on the page data
        populatePageData();


        //At this point, we should have three HashMaps.
        // HashMap1 = hashXML         - hashmap that contains zero to many Documents
        // HashMap2 = pageData        - hashmap that contains String data at page level
        //
        //Combine all of the above HashMaps into one HashMap, called returnHash

        //retrieve all the Documents from hashXML and put them in returnHash
        //retrieve the keyset
        Set ks1 = hashXML.keySet();
        Iterator iter = ks1.iterator();
        String key = null;
        Document doc = null;

        //Iterate thru the keyset
        while (iter.hasNext()) {
            //get the key
            key = iter.next().toString();

            //retrieve the XML document
            doc = (Document)hashXML.get(key);

            //put the XML object in the returnHash
            returnHash.put(key, (Document)doc);
        }

        //append pageData to returnHash
        returnHash.put("pageData", (HashMap)this.pageData);
        return returnHash;
    }


    /*******************************************************************************************
      * getRequestInfo(HttpServletRequest request)
      ******************************************************************************************
      * Retrieve the info from the request object
      *
      * @param  request - HttpServletRequest
      * @throws Exception
      */
    private void getRequestInfo(HttpServletRequest request) throws Exception {
        /****************************************************************************************
         *  Retrieve update delivery date specific parameters
         ***************************************************************************************/
         //retrieve the action
         if (request.getParameter(COMConstants.ACTION_TYPE) != null) {
             this.actionType = request.getParameter(COMConstants.ACTION_TYPE);
         }

         //retrieve the customer id
         if(request.getParameter(COMConstants.CUSTOMER_ID)!=null)
         {
           this.customerId = request.getParameter(COMConstants.CUSTOMER_ID);
         }

         //retrieve the delivery date
         if(request.getParameter(COMConstants.DELIVERY_DATE)!=null)
         {
           this.deliveryDate = request.getParameter(COMConstants.DELIVERY_DATE);
         }

         //retrieve the delivery date range end
         if(request.getParameter(COMConstants.DELIVERY_DATE_RANGE_END)!=null)
         {
           this.deliveryDateRange = request.getParameter(COMConstants.DELIVERY_DATE_RANGE_END);
         }

         //retrieve the external order number
         if(request.getParameter(COMConstants.EXTERNAL_ORDER_NUMBER)!=null)
         {
           this.externalOrderNumber = request.getParameter(COMConstants.EXTERNAL_ORDER_NUMBER);
         }
         
         
         if(request.getParameter(COMConstants.FLORIST_ID)!=null)
         {
            this.floristId = request.getParameter(COMConstants.FLORIST_ID);
         }

         //retrieve the master order number
         if(request.getParameter(COMConstants.MASTER_ORDER_NUMBER)!=null)
         {
           this.masterOrderNumber = request.getParameter(COMConstants.MASTER_ORDER_NUMBER);
         }
         
        //retrieve the delivery date
        if(request.getParameter("orig_delivery_date")!=null)
        {
          this.origDeliveryDate = request.getParameter("orig_delivery_date");
        }

        //retrieve the delivery date range end
        if(request.getParameter("orig_delivery_date_range_end")!=null)
        {
          this.origDeliveryDateRange = request.getParameter("orig_delivery_date_range_end");
        }

         //retrieve the occasion id
         if (request.getParameter(COMConstants.OCCASION) != null) {
             this.occasion = request.getParameter(COMConstants.OCCASION);
         }

         //retrieve the order detail id
         if(request.getParameter(COMConstants.ORDER_DETAIL_ID)!=null)
         {
           this.orderDetailId = request.getParameter(COMConstants.ORDER_DETAIL_ID);
         }
         
         //retrieve the order disp code
         if (request.getParameter(COMConstants.ORDER_DISP_CODE) != null) {
            this.orderDispCode = request.getParameter(COMConstants.ORDER_DISP_CODE);
         }

         //retrieve the order guid
         if(request.getParameter(COMConstants.ORDER_GUID)!=null)
         {
           this.orderGuid = request.getParameter(COMConstants.ORDER_GUID);
         }

         //retrieve the origin id
         if(request.getParameter(COMConstants.ORIGIN_ID)!=null)
         {
           this.originId = request.getParameter(COMConstants.ORIGIN_ID);
         }

         //retrieve the product id
         if (request.getParameter(COMConstants.PRODUCT_ID) != null) {
             this.productId = 
                     (request.getParameter(COMConstants.PRODUCT_ID)).toUpperCase();
         }
         
         //retrieve the recipient city
         if (request.getParameter(COMConstants.RECIPIENT_CITY) != null) {
            this.recipientCity = 
                   request.getParameter(COMConstants.RECIPIENT_CITY);
         }
        
         //retrieve the recipientCountry id
         if (request.getParameter(COMConstants.RECIPIENT_COUNTRY) != null) {
             this.recipientCountry = 
                     request.getParameter(COMConstants.RECIPIENT_COUNTRY);
         }

         //retrieve the recipientState
         if (request.getParameter(COMConstants.RECIPIENT_STATE) != null) {
             this.recipientState = 
                     request.getParameter(COMConstants.RECIPIENT_STATE);
         }

         //retrieve the postal code
         if (request.getParameter(COMConstants.RECIPIENT_ZIP_CODE) != null) {
             this.recipientZipCode = 
                     request.getParameter(COMConstants.RECIPIENT_ZIP_CODE);
         }

         //retrieve the ship date
         if(request.getParameter("orig_ship_date")!=null && !request.getParameter("orig_ship_date").equals(""))
         {
             this.origShipDate = request.getParameter("orig_ship_date");
         }
         
         //retrieve the ship method
         if(request.getParameter(COMConstants.SHIP_METHOD)!=null && !request.getParameter(COMConstants.SHIP_METHOD).equals(""))
         {
           this.shipMethod = request.getParameter(COMConstants.SHIP_METHOD);
         }
         
         //retrieve the source code
         if(request.getParameter(COMConstants.SOURCE_CODE)!=null)
         {
           this.sourceCode = request.getParameter(COMConstants.SOURCE_CODE);
         }
         
         //retrieve the vendor flag
         if (request.getParameter(COMConstants.VENDOR_FLAG) != null) {
             this.vendorFlag = 
                     request.getParameter(COMConstants.VENDOR_FLAG);
         }


         /****************************************************************************************
            *  Retrieve Security Info
            ***************************************************************************************/
         this.sessionId = request.getParameter(COMConstants.SEC_TOKEN);
         this.context = request.getParameter(COMConstants.CONTEXT);

    }


    /*******************************************************************************************
      * getConfigInfo()
      ******************************************************************************************
      * Retrieve the info from the configuration file
      *
      * @throws Exception
      */
    private void getConfigInfo() throws Exception {

        ConfigurationUtil cu = null;
        cu = ConfigurationUtil.getInstance();

        SecurityManager sm = SecurityManager.getInstance();
        UserInfo ui = sm.getUserInfo(this.sessionId);
        this.csrId = ui.getUserID();
        
    }


    /*******************************************************************************************
      * populatePageData()
      ******************************************************************************************
      * Populate the page data with the remainder of the fields
      *
      */
    private void populatePageData() {
    
        this.pageData.put(COMConstants.PD_MO_CUSTOMER_ID,             this.customerId);
        this.pageData.put(COMConstants.PD_MO_DELIVERY_DATE,           this.deliveryDate);
        this.pageData.put(COMConstants.PD_MO_DELIVERY_DATE_RANGE_END, this.deliveryDateRange);
        this.pageData.put(COMConstants.PD_MO_EXTERNAL_ORDER_NUMBER,   this.externalOrderNumber);
        this.pageData.put(COMConstants.PD_MO_MASTER_ORDER_NUMBER,     this.masterOrderNumber);
        this.pageData.put(COMConstants.PD_MO_OCCASION,                this.occasion);
        this.pageData.put(COMConstants.PD_MO_ORDER_DETAIL_ID,         this.orderDetailId);
        this.pageData.put(COMConstants.ORDER_DISP_CODE,               this.orderDispCode);
        this.pageData.put(COMConstants.PD_MO_ORDER_GUID,              this.orderGuid);
        this.pageData.put(COMConstants.PD_MO_ORIGIN_ID,               this.originId);
        this.pageData.put(COMConstants.PD_MO_PRODUCT_ID,              this.productId);
        this.pageData.put(COMConstants.RECIPIENT_CITY,                this.recipientCity);
        this.pageData.put(COMConstants.PD_MO_RECIPIENT_COUNTRY,       this.recipientCountry);
        this.pageData.put(COMConstants.PD_MO_RECIPIENT_STATE,         this.recipientState);
        this.pageData.put(COMConstants.PD_MO_RECIPIENT_ZIP_CODE,      this.recipientZipCode);
        this.pageData.put(COMConstants.SHIP_DATE,                     this.shipDate);
        this.pageData.put(COMConstants.PD_MO_SHIP_METHOD,             this.shipMethod);
        this.pageData.put(COMConstants.SOURCE_CODE,                   this.sourceCode);
        this.pageData.put(COMConstants.VENDOR_FLAG,                   this.vendorFlag);
        this.pageData.put(COMConstants.PD_MO_MAX_DELIVERY_DATES_RETRIEVED, this.wasMaxRetrieved?COMConstants.CONS_YES:COMConstants.CONS_NO);
    }


  /*******************************************************************************************
  * processMain()
  ******************************************************************************************
  *
  * @throws Exception
  * @throws Throwable
  */
    private void processMain() throws Exception, Throwable {

       
        boolean sendASK = false;
        boolean cancelOriginalSendNew = false;
        boolean validationPassed = true;
        RWDFloristTO floristTO = null;
        String xslName = null;
        String floristSelectionLogId = null;
             
        //determine if the product was a vendor or florist product
        determineVendorVsFloristProduct();
        
        //set calendar objects for the delivery date, date range end and ship date
        populateCalendarObjects();
        
        //check to see if the mercury is still live with this order
        //if not, return user to Update Delivery Date page and display error
        if(!isOrderStillLive())
        {
            pageData.put(COMConstants.PD_MESSAGE_DISPLAY,   COMConstants.UDD_NO_LIVE_MERCURY);
            pageData.put("success",                         COMConstants.CONS_NO);
            validationPassed= false;    
        }
        
        // perform the following for florist orders only
        if(validationPassed && vendorFlag.equalsIgnoreCase(COMConstants.CONS_NO))
        {
            getOrderBillInfo(); 
            
            boolean serviceFeeDifferent = checkServiceFeeOverrideValue(true);
           
            if(serviceFeeDifferent)
            {
                logger.debug("service fee is different");
                pageData.put(COMConstants.PD_MESSAGE_DISPLAY,   COMConstants.UDD_SERVICE_FEE_ERROR);
                pageData.put("success",                         COMConstants.CONS_NO);
                validationPassed= false;
            }
            else
            {
                boolean floristStillAvailable = checkFloristStatus();
                                            
                if(floristStillAvailable)
                {
                    sendASK = true;        
                }
                else
                {
                    floristTO=this.autoSelectFlorist();
                    // Check to see if there are anymore florists available to fulfill this order 
                    // If there is, send the order to that florist, if not, return error message to user
                     if(!floristExist(floristTO))
                     {
                        logger.debug("florist does not exist");
                        pageData.put(COMConstants.PD_MESSAGE_DISPLAY,   COMConstants.UDD_NO_FLORISTS_AVAILABLE);
                        pageData.put("success",                         COMConstants.CONS_NO);
                        validationPassed= false;
                     }
                     else
                     {  
                        this.newFloristId = floristTO.getFloristId();
                        //a new florist has been found
                        this.selectionData = floristTO.getZipCityFlag(); 
                        cancelOriginalSendNew = true;
                     }
                } 
            }
        } 
        // Check if service fee override exists for vendor FRECUT or SDFC.
        else if(validationPassed 
                && vendorFlag.equalsIgnoreCase(COMConstants.CONS_YES)
                && this.productType != null 
                && (this.productType.equals(COMConstants.CONS_PRODUCT_TYPE_FRECUT) 
                    || this.productType.equals(COMConstants.CONS_PRODUCT_TYPE_SDFC)))
        {
            getOrderBillInfo(); 
            
            boolean serviceFeeDifferent = checkServiceFeeOverrideValue(false);
            if(serviceFeeDifferent)
            {
                logger.debug("service fee is different");
                pageData.put(COMConstants.PD_MESSAGE_DISPLAY,   COMConstants.UDD_SERVICE_FEE_ERROR);
                pageData.put("success",                         COMConstants.CONS_NO);
                validationPassed= false;
            }
        }

        if (!validationPassed)
        {
           //return user to the Update Delivery Date page and display error message
           xslName = COMConstants.XSL_UDD_INIT_LOAD_DELIVERY_DATE_ACTION;
           this.pageData.put("XSL", xslName); 
        }
        if (validationPassed)
        {
              //start a user transaction
              UserTransaction userTransaction = null;
              
              InitialContext initialContext = null;
              try
              { 
                //action used to transfer execution back to the cart
              //  xslName = COMConstants.XSL_UDD_CUSTOMER_SEARCH_ACTION;                
                // get the initial context
                initialContext = new InitialContext();
              
                //get the transaction timeout
                int transactionTimeout = Integer.parseInt((String)ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, COMConstants.TRANSACTION_TIMEOUT_IN_SECONDS));
                // Retrieve the UserTransaction object
                String jndi_usertransaction_entry = ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, COMConstants.JNDI_USERTRANSACTION_ENTRY);
                userTransaction = (UserTransaction)  initialContext.lookup(jndi_usertransaction_entry);
                userTransaction.setTransactionTimeout(transactionTimeout);
                userTransaction.begin();  
                
                //update delivery date and ship date
                updateOrderDetails();
                
                //if the original florist cannot fulfill this order because of the 
                //delivery date selected, and a new florist is found that can fulfill this order
                //then we need to update the order florist used table with the original florist
                if(cancelOriginalSendNew)
                {
                    //update florist used
                    updateFloristUsedOnOrder();
                }
                
                //update order comments
                addOrderComments();
             
                //commit changes
                userTransaction.commit();
                //release lock
                releaseLock();
                }
                catch(Throwable t)
                {
                  logger.error(t);
                  rollback(userTransaction);
                  throw t;
                }
                finally
                {
                  try
                  {
                    initialContext.close();
                  }
                  catch (Exception e)
                  {
                    logger.debug(e.toString());
                  }
                }
                                        
                //if original florist can still handle the order
                //forward user on to the ASK Message screen under
                //the communication screen
                if(sendASK)
                {
                    sendASKMessageProcess();
                }
                else if(cancelOriginalSendNew)
                {
                    //cancel ftd
                    sendCAN(true);
                    //send new ftd
                    sendNewFTD(true, floristTO);
                    //return user to the recip order info screen
                    xslName = COMConstants.XSL_UDD_CUSTOMER_SEARCH_ACTION;
                    this.pageData.put( "XSL",xslName);
                }
                else if (vendorFlag.equalsIgnoreCase("Y"))
                {
                     //cancel ftd
                     sendCAN(false);
                     //send new ftd
                     sendNewFTD(false);
                    //return user to the recip order info screen
                     xslName = COMConstants.XSL_UDD_CUSTOMER_SEARCH_ACTION;
                     this.pageData.put( "XSL",xslName);
                }
                     
            }
            
     //   this.pageData.put("XSL", xslName);  

   }

   private boolean floristExist(RWDFloristTO floristTO)
      {
        
        return (floristTO!=null&&floristTO.getFloristId()!=null&&floristTO.getFloristId().trim().length()>0);
      }

    /*******************************************************************************************
    * computeShipDate()
    *******************************************************************************************
     * Calculate the ship date
     *
     */
     private String computeShipDate(String shipMethod, String requestedDeliveryDate, String recipState,
                                  String recipCountry, String productId) throws Exception
     {
       DeliveryDateUTIL ddu = new DeliveryDateUTIL();

       OEDeliveryDateParm oeParms = new OEDeliveryDateParm();

       getProductInfo(productId);

       oeParms.setProductType(productType);

       String newShipDate =  ddu.getShipDate(oeParms, shipMethod,
                             requestedDeliveryDate, recipState,
                             recipCountry, productId, this.conn);

       return newShipDate;
     }
     
    /**
      * Obtain the product type and vendor id
      */
    private void getProductInfo(String productId) throws Exception
     {
       MessageDAO messageDAO = new MessageDAO(this.conn);

       CachedResultSet crs =  messageDAO.getProductInfo(productId);
       String productType = null;
       if(crs.next())
       {
         this.productType = (String) crs.getString("productType");
       }
     }
 
  
    /**
      * Obtain florist specific information
      */
    private boolean checkFloristStatus() throws Exception
     {
          boolean floristStillAvailable = true;
          logger.info("Florist id: " + this.floristId + " delivery date: " + this.deliveryDate);
          
          UpdateOrderDAO uoDAO = new UpdateOrderDAO(this.conn);

          CachedResultSet crs =  uoDAO.getFloristInfo(this.floristId);
          String status = null;
          String sundayDelivery = null;
          if(crs.next())
          {
            status = (String) crs.getString("status");
            sundayDelivery = (String) crs.getString("sunday_delivery_flag");    
            logger.info("oldStatus: " + status);
            logger.info("oldSundayDeliveryDate: " + sundayDelivery);
          }
         
         MessageDAO dao = new MessageDAO(this.conn); 
         FloristStatusVO floristStatus = dao.getUDDFloristStatus(this.floristId, this.cDeliveryDate.getTime());
                
         if(floristStatus != null){
	         if(floristStatus.getFloristCurrentlyOpen() != null && floristStatus.getFloristCurrentlyOpen().equalsIgnoreCase("N")){
	        	 floristStillAvailable = false;
	     	 }else if(floristStatus.getOpenOnDeliveryDate() != null && floristStatus.getOpenOnDeliveryDate().equalsIgnoreCase("N")){
	     		floristStillAvailable = false;
	     	 }else if(floristStatus.getFloristBlocked() != null && floristStatus.getFloristBlocked().equalsIgnoreCase("Y")){
	     		floristStillAvailable = false;
	     	 }else if(floristStatus.getFloristSuspended() != null && floristStatus.getFloristSuspended().equalsIgnoreCase("Y")){
	     		floristStillAvailable = false;
	     	 } else {
		     	 if(floristStatus.getSundayDeliveryFlag() != null) {
			         sundayDelivery = floristStatus.getSundayDeliveryFlag();
			     }
	             //check delivery date, if delivery date is a Sunday, then check
	             //to see if the florist is codified for Sunday delivery
	             //Define the format
	             if (this.cDeliveryDate.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
	                 if(sundayDelivery.equalsIgnoreCase("N")) {
	                   floristStillAvailable = false;
	                 }
	             }
	     	 }
         }
         logger.info("floristStillAvailable: " + floristStillAvailable);
         
         return floristStillAvailable;
     }
     
    /**
      * Obtain florist specific information
      */
    private boolean checkServiceFeeOverrideValue(boolean floristDelivered) throws Exception
     {
        FTDServiceChargeUtility scUtil = new FTDServiceChargeUtility();
        Calendar now = Calendar.getInstance();
        Date dd = null;
        
        if(this.cDeliveryDate != null) {
            dd = this.cDeliveryDate.getTime();
        }
        
        ServiceChargeVO scVO = scUtil.getServiceChargeData(now.getTime(), this.sourceCode, dd, this.conn);
        logger.debug("source code:" + this.sourceCode + "; dd:" + deliveryDate);
        logger.debug("florist fee:" + scVO.getDomesticCharge().toString());
        logger.debug("international fee:" + scVO.getInternationalCharge().toString());
        logger.debug("vendor fee:" + scVO.getVendorCharge().toString());
        logger.debug("vendor Sat fee:" + scVO.getVendorSatUpcharge().toString());
        logger.debug("this.serviceFee:" + this.serviceFee);
        boolean serviceFeeDifferent = false;
        BigDecimal orderServiceFee = new BigDecimal(this.serviceFee);
        BigDecimal overrideServiceFee = null;
        
        // Only care about override service charge that is different from the current value.
        if("Y".equals(scVO.getOverrideFlag())) {
            if(floristDelivered && orderServiceFee.compareTo(scVO.getDomesticCharge()) != 0) {
                // If florist delivered and the override domestic charge is different.
                serviceFeeDifferent = true;
                logger.info("Order service fee:" + orderServiceFee.toString() + "; override service fee:" + scVO.getDomesticCharge().toString());
            } else if (!floristDelivered) {
                // Vendor delivered (FRECUT or SDFC)
                overrideServiceFee = scVO.getVendorCharge();
                // Check if delivery date is Saturday.
                if(dd != null && (this.cDeliveryDate.DAY_OF_WEEK == Calendar.SATURDAY)) {
                    overrideServiceFee = overrideServiceFee.add(scVO.getVendorSatUpcharge());
                }
                if(orderServiceFee.compareTo(overrideServiceFee) != 0) {
                    serviceFeeDifferent = true;
                    logger.info("Order service fee:" + orderServiceFee.toString() + "; override service fee:" + overrideServiceFee.toString());
                }
            }
        }
        
        return serviceFeeDifferent;
    }
    
 /*******************************************************************************************
 * determineVendorVsFloristOrder()
 *******************************************************************************************
 * This method is used to call the
 *
 */
   private void determineVendorVsFloristProduct() throws Exception {

       if (this.vendorFlag.equalsIgnoreCase(COMConstants.CONS_YES)) {
           this.displayFormat = COMConstants.CONS_VENDOR;
       } else {
           this.displayFormat = COMConstants.CONS_FLORIST;
       }
       this.pageData.put("display_format", this.displayFormat);

   }


     /*
      * Add comments to the order
      */
     private void addOrderComments() throws Exception {
       
        //insert a record into the comments table
        //Instantiate CommentDAO
        CommentDAO commentDAO = new CommentDAO(this.conn);
   
        CommentsVO commentsVO = new CommentsVO();
        
        this.askMessageComments = 
             "Delivery date changed from " + origDeliveryDate +
              ( this.origDeliveryDateRange != null && !this.origDeliveryDateRange.equals("")  ? " - " + this.origDeliveryDateRange : "" ) 
              +   " to " + deliveryDate  +
              ( this.deliveryDateRange != null && !this.deliveryDateRange.equals("")  ? " - " + this.deliveryDateRange : "" ) ;
         
        commentsVO.setComment( this.askMessageComments + " due to Update Delivery Date process.");
          
        commentsVO.setCommentOrigin(request.getParameter(COMConstants.START_ORIGIN));
        commentsVO.setCommentType("Order");
        commentsVO.setCreatedBy(csrId);
        commentsVO.setUpdatedBy(csrId);
        commentsVO.setOrderDetailId(orderDetailId);
        commentsVO.setOrderGuid(orderGuid);
        commentsVO.setCustomerId(customerId);
        commentDAO.insertComment(commentsVO); 
                
       }   

       
  /*
   * Try to find another florist to fill this order
   */
    private RWDFloristTO autoSelectFlorist() throws Exception{
       
        UpdateOrderDAO uoDAO = new UpdateOrderDAO(this.conn);
        CachedResultSet orderBillInfo = uoDAO.getOrderBill(this.orderDetailId);
               
        RWDTO rwdTO=new RWDTO();
        RWDFloristTO floristTO = new RWDFloristTO();
        try {
            rwdTO.setOrderDetailId(this.orderDetailId);
            rwdTO.setDeliveryCity(this.recipientCity);
            rwdTO.setDeliveryState(this.recipientState);
            rwdTO.setZipCode(this.recipientZipCode.toUpperCase());
            rwdTO.setReturnAll(false);
            rwdTO.setOrderValue(Double.parseDouble(this.productAmount));
            rwdTO.setProduct(this.productId);
            rwdTO.setDeliveryDate(cDeliveryDate.getTime());
            if ( StringUtils.isNotEmpty(this.deliveryDateRange) ) {
              rwdTO.setDeliveryDateEnd(cDeliveryDateRangeEnd.getTime());
            }
            rwdTO.setSourceCode(sourceCode);
            rwdTO.setRWDFlagOverride(Boolean.TRUE);
            
            floristTO = MessagingServiceLocator.getInstance().getOrderAPI().getFillingFlorist(rwdTO, conn);
            } finally {
                if (logger.isDebugEnabled()) {
                    logger.debug("Exiting autoSelectFlorist");
                }
            }    
            return floristTO;
     
    }
    
    private void getOrderBillInfo()throws Exception
    {
         UpdateOrderDAO uoDAO = new UpdateOrderDAO(this.conn);
         CachedResultSet orderBillInfo = uoDAO.getOrderBill(this.orderDetailId);
         
         if(orderBillInfo.next())
         {
             this.productAmount = orderBillInfo.getString("product_amount");
             this.serviceFee = orderBillInfo.getString("service_fee");
         }
    }
    
     /*
      * sendCAN
      * 
      * Send a CAN on the original order if needed
      */
     private void sendCAN(boolean floristOrder) throws Exception
     {
        logger.info("sendCAN for order detail " + orderDetailId); 
                      
        UpdateOrderDAO dao = new UpdateOrderDAO(conn);
        ResultTO result = null;
        
        //check if ADJ needs to be sent 
        boolean adjustmentNeed = false;
        
        //check what type of order this orig one is
        if(floristOrder)
        {
           //get the mercury message associated with the FTD
           MessageVO messageVO = dao.getMessageDetailLastFTD(orderDetailId,MessagingConstants.MERCURY_MESSAGE, "N");
           //build mercury cancel TO
           CANMessageTO canTO = new CANMessageTO();
           canTO.setMercuryId(messageVO.getMessageId());
           canTO.setComments(CANCEL_MSG_COMMENT);   
           canTO.setOperator(csrId);    
           canTO.setMercuryStatus(BaseMercuryMessageTO.MERCURY_OPEN);    
           canTO.setDirection(BaseMercuryMessageTO.OUTBOUND);
           canTO.setFillingFlorist(messageVO.getFillingFloristCode());
           canTO.setSendingFlorist(messageVO.getSendingFloristCode());   
            
           //check if ADJ needs to be sent based on original delivery date
           if (checkDeliveryDate( this.cOrigDeliveryDate.getTime()) == true) {
               adjustmentNeed=true;
               logger.debug("Based on date, ADJ needed");             
           }
           
           if(adjustmentNeed)
           { 
                ConfigurationUtil configUtil = ConfigurationUtil.getInstance();         
                String adjReasonCode = configUtil.getProperty(COMConstants.PROPERTY_FILE,"MODIFY_ORDER_ADJ_REASON");                                           
            
                //combinded report number = month + "-1"
                int month = this.cOrigDeliveryDate.get(Calendar.MONTH) + 1;
                String sMonth = String.valueOf(month);
                //make the month 2 digits
                if (sMonth.length() == 1){
                    sMonth = '0' + sMonth;
                }
                String combinedReportNumber = sMonth + "-1";
            
                ADJMessageTO adjTO = new ADJMessageTO();
                adjTO.setMercuryId(messageVO.getMessageId());
                adjTO.setComments(ADJ_MSG_COMMENT);
                adjTO.setCombinedReportNumber(combinedReportNumber);
                adjTO.setOperator(this.csrId);
                adjTO.setMercuryStatus("MO");
                adjTO.setDirection(BaseMercuryMessageTO.OUTBOUND);
                adjTO.setOldPrice(new Double(messageVO.getCurrentPrice()));
                adjTO.setOverUnderCharge(new Double("0.00"));
                adjTO.setAdjustmentReasonCode(adjReasonCode);
                adjTO.setSendingFlorist(messageVO.getSendingFloristCode());
                adjTO.setFillingFlorist(messageVO.getFillingFloristCode());    
                
                result=MessagingServiceLocator.getInstance().getMercuryAPI().sendADJMessage(adjTO, conn);

                //check for errors
                 if(!result.isSuccess())
                 {
                    throw new Exception("Could not create Mercury ADJ message (order detail id="+orderDetailId+"). ERROR: "+result.getErrorString());
                 }
                 else
                 {
                     logger.debug("ADJ sent - florist");
                 }

            }
               
            //send mercury cancel
            result = MessagingServiceLocator.getInstance().getMercuryAPI().sendCANMessage(canTO, conn);
            
            //check for errors
            if(!result.isSuccess())
            {
               throw new Exception("Could not create Mercury CAN message (order detail id="+orderDetailId+"). ERROR: "+result.getErrorString());
            }                
        }  //end floral check
        else
        {
            //venus order
      
            //get the mercury message associated with the FTD
            MessageVO messageVO = dao.getMessageDetailLastFTD(orderDetailId,MessagingConstants.VENUS_MESSAGE);
            String cancelMessage = "";
            String cancelReason = "";
         
            Calendar cTodaysDate = Calendar.getInstance();
            //Since the delivery date's time is 00:00:00, set today's date time to 00:00:00 too
            cTodaysDate.set(Calendar.HOUR, 0); 
            cTodaysDate.set(Calendar.MINUTE, 0); 
            cTodaysDate.set(Calendar.SECOND, 0); 
            cTodaysDate.set(Calendar.MILLISECOND, 0);
            cTodaysDate.set( Calendar.AM_PM, Calendar.AM );
            
            //figure out which cancel reason code should be populated in the Cancel Message
            //if order is in Shipped status set the cancel reason code for "Poor Quality/Broken Vase"
            if (orderDispCode.equalsIgnoreCase(COMConstants.ORDER_DISPOSITION_SHIPED))
            {
                cancelReason = "QUA";   
                cancelMessage = "Poor Quality/Broken Vase";
            }
            //if the order is in Processed or Printed status then set cancel reason code for "Customer/Vendor Cancelled"
            else if ( orderDispCode.equalsIgnoreCase(COMConstants.ORDER_DISPOSITION_PROCESSED) ||
                      orderDispCode.equalsIgnoreCase(COMConstants.ORDER_DISPOSITION_PRINTED) )  
            {
                cancelReason = "CCN";    
                cancelMessage = "Customer/Vendor Cancelled";
            }
            
            //build venus cancel TO
            CancelOrderTO canTO = new CancelOrderTO();
            canTO.setVenusId(messageVO.getMessageId());
            canTO.setComments(cancelMessage);
            canTO.setCsr(csrId);
            canTO.setCancelReasonCode(cancelReason);

            //check if ADJ needs to be sent
            if (new MessageUtil(conn).requiresSDSAutoADJ(messageVO)) {
                adjustmentNeed=true;
                logger.debug("ADJ required for CAN when updating Delivery Date");             
            }
            
            /* If going to do an adjustment then do not transmit the cancel and
             * set the venus status to VERIFIED so the CAN is not processed
             */ 
            if(adjustmentNeed)
            {
              canTO.setTransmitCancel(false);
              canTO.setVenusStatus(MessagingConstants.VENUS_STATUS_VERIFIED);
            }                
        
            //send venus cancel
            result= MessagingServiceLocator.getInstance().getVenusAPI().sendCancel(canTO, conn);
            
            //check for errors
            if(!result.isSuccess())
            {
               throw new Exception("Could not create Venus CAN message (order detail id="+orderDetailId+"). ERROR: "+result.getErrorString());
            }                
            
            //if adjustment needed
            if(adjustmentNeed)
            {
                AdjustmentMessageTO adjTO = new AdjustmentMessageTO();
                adjTO.setComments(ADJ_MSG_COMMENT);
                adjTO.setOperator(csrId);
                adjTO.setPrice(new Double(messageVO.getCurrentPrice()));
                adjTO.setMessageType("ADJ");
                adjTO.setOrderDetailId(orderDetailId);
                adjTO.setVenusOrderNumber(messageVO.getMessageOrderId());
                adjTO.setFillingVendor(messageVO.getFillingFloristCode());
                adjTO.setOverUnderCharge(new Double("0.00")); 
                adjTO.setCancelReasonCode(cancelReason);
        
                result = MessagingServiceLocator.getInstance().getVenusAPI().sendAdjustment(adjTO, conn);
                
                //check for errors
                 if(!result.isSuccess())
                 {
                    throw new Exception("Could not create ADJ message (order detail id="+orderDetailId+"). ERROR: "+result.getErrorString());
                 }                    
                 else
                 {
                     logger.debug("ADJ sent - vendor");
                 }                    
            }//end check if vendor order
        }
      
     } 
     
     /*
      * sendNewFTD
      * 
      * Send a new FTD message
      */
     private void sendNewFTD(boolean floristOrder) throws Exception
     {
        logger.info("sendNewFTD for order detail " + orderDetailId); 
        
        ResultTO result=new ResultTO();
         
         //check what type of order this orig one is
         if(floristOrder)
         {
            //create Mercury FTD message TO.
            com.ftd.op.mercury.to.OrderDetailKeyTO orderDetail = new com.ftd.op.mercury.to.OrderDetailKeyTO();
            orderDetail.setOrderDetailId(orderDetailId);
            result=MessagingServiceLocator.getInstance().getMercuryAPI().sendFTDMessage(orderDetail, conn);
         }
         else
         {
            //vendor order
            //create Venus FTD message TO.
            com.ftd.op.venus.to.OrderDetailKeyTO orderDetail = new com.ftd.op.venus.to.OrderDetailKeyTO();
            orderDetail.setOrderDetailId(orderDetailId);
            orderDetail.setCsr(csrId);
            result=MessagingServiceLocator.getInstance().getVenusAPI().sendOrder(orderDetail, conn);
         }
         
         if(!result.isSuccess())
         {
                logger.error("Could not store FTD message. ERROR: "+result.getErrorString());
                //need to forward user to USE page
         }else
         {

             //new ftd sent successfully
             //need to return user to recipient order information page
         }
         
     }
     
     /*
      * sendNewFTD
      * @param boolean floristOrder
      * @param 
      * Overloaded message to send a new Mercury FTD message
      */
     private void sendNewFTD(boolean floristOrder, RWDFloristTO floristTO) throws Exception
     {
        logger.info("sendNewFTD for order detail " + orderDetailId); 
        
        ResultTO result=new ResultTO();
         
         //check what type of order this orig one is
         if(floristOrder)
         {
            //create Mercury FTD message TO.
            com.ftd.op.mercury.to.OrderDetailKeyTO orderDetail = new com.ftd.op.mercury.to.OrderDetailKeyTO();
            orderDetail.setOrderDetailId(orderDetailId);
            result=MessagingServiceLocator.getInstance().getMercuryAPI().sendFTDMessage(orderDetail, conn);
            /* add floristSelectionLogId to the mercury table */
            String floristSelectionLogId = floristTO.getFloristSelectionLogId();
            logger.info("floristSelectionLOgId: " + floristSelectionLogId);
            logger.info("key: " + result.getKey());
            if(floristSelectionLogId != null && result.getKey() != null)
            {
            	MessageDAO mDAO = new MessageDAO(conn);
            	mDAO.updateFloristSelectionLogId(result.getKey(), floristSelectionLogId);
            }
         }
         
         if(!result.isSuccess())
         {
                logger.error("Could not store FTD message. ERROR: "+result.getErrorString());
                //need to forward user to USE page
         }else
         {

             //new ftd sent successfully
             //need to return user to recipient order information page
         }
         
     }
     
      /**
       *  check if the current date is greater than the
       *  last day of the delivery date month
       * @param deliveryDate - Date
       * @return boolean
       * @throws Exception
       */
      private boolean checkDeliveryDate(Date deliveryDate)
          throws Exception {
          if (logger.isDebugEnabled()) {
              logger.debug("Entering checkDeliveryDate");
              logger.debug("deliveryDate: " + deliveryDate);
          }

          boolean dateComparision = false;

          try {
              Calendar calCurrent = Calendar.getInstance(TimeZone.getDefault());
              Date currentDate = calCurrent.getTime();

              SimpleDateFormat sdfOutput = new SimpleDateFormat(MessagingConstants.MESSAGE_DATE_FORMAT);

              if (currentDate.after(deliveryDate)) {
                  Calendar calDelivery = Calendar.getInstance(TimeZone.getDefault());
                  calDelivery.setTime(deliveryDate);

                  int day = calDelivery.getActualMaximum(Calendar.DAY_OF_MONTH);

                  Calendar calAdjustment = Calendar.getInstance(TimeZone.getDefault());
                  calAdjustment.setTime(deliveryDate);
                  calAdjustment.set(calDelivery.get(Calendar.YEAR),
                      calDelivery.get(Calendar.MONTH), day);

                  Date adjustmentDate = calAdjustment.getTime();

                  if (currentDate.after(adjustmentDate)) {
                      dateComparision = true;
                  }
              }
          } finally {
              if (logger.isDebugEnabled()) {
                  logger.debug("Exiting checkDeliveryDate");
              }
          }

          return dateComparision;
      }   
       
     
    private Double getMessageTOPrice(String priceStr) {
        Double price = new Double(0);

        if ((priceStr != null) && (priceStr.trim().length() > 0)) {
            try {
                price = (new Double(priceStr));
            } catch (NumberFormatException ne) {
                logger.error("can not parse price. ", ne);
                //don't need to throw an exception.
            }
        }

        return price;
    }     
    
    private void releaseLock() throws Exception{
        LockDAO lockDAO = new LockDAO(conn);
        logger.debug("releaseLock()");
        logger.debug("sessionId = " + sessionId);
        logger.debug("csrId = " + csrId);
        logger.debug("orderDetailId = " + orderDetailId);
        
        lockDAO.releaseLock(sessionId, csrId, orderDetailId, "MODIFY_ORDER");
        
        logger.debug("lock released");
    }
    
    private void updateFloristUsedOnOrder()throws Exception
    {
            //Instantiate OrderDAO
            OrderDAO oDAO = new OrderDAO(this.conn);
            
            //update order florist used table with the original florist on the order
            oDAO.insertOrderFloristUsed(this.orderDetailId, this.floristId, this.selectionData);
    }

    private void updateOrderDetails()throws Exception
    {
        //Instantiate UpdateOrderDAO
        UpdateOrderDAO uoDAO = new UpdateOrderDAO(this.conn);
            
        //update order detail.
        uoDAO.updateUDDOrderDetails(this.orderDetailId, this.csrId, this.cDeliveryDate, this.cDeliveryDateRangeEnd, this.cShipDate, this.newFloristId);
   }
   
    private void rollback(UserTransaction userTransaction)
    {
      try  
      {
          if (userTransaction != null)  
          {
            // rollback the user transaction
            userTransaction.rollback();

            logger.info("User transaction rolled back");
          }
      } 
      catch (Exception ex)  
      {
          logger.error(ex);
      } 
    }
    
  /*
   * Set values necessary to forward user to the ASK Message screen
   */
  private void sendASKMessageProcess() throws Exception {
    HashMap attributes = new HashMap();

    String messageType = (vendorFlag != null && vendorFlag.equals("Y")) ? "Venus" : "Mercury";

    //instantiate UpdateOrderDAO
    UpdateOrderDAO uoDAO = new UpdateOrderDAO(this.conn);
    MessageVO messageVO = uoDAO.getMessageDetailLastFTD(orderDetailId, messageType, "N");
    
    StringBuffer parms = new StringBuffer();
    parms.append("?msg_message_type=" + messageType);
    parms.append("&destination=recipient_order_page");
    parms.append("&order_detail_id=" + orderDetailId);
    parms.append("&msg_message_order_number=" + messageVO.getMessageOrderId());
    parms.append("&securitytoken=" + sessionId);
    parms.append("&context=" + context);
    parms.append("&msg_external_order_number=" + externalOrderNumber);
    parms.append("&msg_guid=" + orderGuid);
    parms.append("&msg_customer_id=" + customerId);
    parms.append("&reason=" + this.askMessageComments);
    
    ActionForward actionForward = mapping.findForward("prepareAskFlorist");
    pageData.put("attributes", attributes);
    String path = actionForward.getPath() + parms;
    actionForward = new ActionForward(path, false);
    pageData.put("XSL", "forward");
    pageData.put("forward", actionForward);

    logger.debug("Path to COM page:" + path);

    //encode path
    logger.debug("Encoded path to COM page:" + path);
  
  }
  
  private void populateCalendarObjects() throws Exception
  {
      SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
      
      if (StringUtils.isNotEmpty(this.deliveryDate)) {
         cDeliveryDate = Calendar.getInstance();
         cDeliveryDate.setTime(dateFormat.parse(this.deliveryDate));
      }            
      
      if ( StringUtils.isNotEmpty(this.deliveryDateRange) ) {
        cDeliveryDateRangeEnd = Calendar.getInstance();
        cDeliveryDateRangeEnd.setTime(dateFormat.parse(this.deliveryDateRange));
      }
      
      //compute ship date for vendor orders only
      if(vendorFlag.equalsIgnoreCase("Y"))
      {
          String shpDate = computeShipDate(shipMethod, deliveryDate, recipientState, recipientCountry, productId);
          this.shipDate = shpDate;
          cShipDate = Calendar.getInstance();
          cShipDate.setTime(dateFormat.parse(shpDate));
          
          cOrigShipDate = Calendar.getInstance();
          cOrigShipDate.setTime(dateFormat.parse(this.origShipDate));
      }  
      
      if (StringUtils.isNotEmpty(this.origDeliveryDate)) {
         cOrigDeliveryDate = Calendar.getInstance();
         cOrigDeliveryDate.setTime(dateFormat.parse(this.origDeliveryDate));
      }  
      
  }
  
  private boolean isOrderStillLive() throws Exception
  {
      String messageType = null;
      if(vendorFlag.equalsIgnoreCase("Y"))
        messageType = "Venus";
      else
        messageType = "Mercury";
      ModifyOrderUTIL moUTIL = new ModifyOrderUTIL(conn);
      MessageStatusVO msVO =  moUTIL.getMessageStatus(orderDetailId, messageType, "Y");
      
      return msVO.isLiveMercury();
  }
    
    
}