package com.ftd.mo.bo;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.util.BasePageBuilder;
import com.ftd.mo.dao.UpdateOrderDAO;
import com.ftd.mo.util.BreadcrumbUtil;
import com.ftd.mo.util.SearchUtil;
import com.ftd.mo.vo.ProductFlagsVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.XMLFormat;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;


public class LoadCustomOrderBO
{

/*******************************************************************************************
 * Class level variables
 *******************************************************************************************/
  private HttpServletRequest  request             = null;
  private Connection          con                 = null;
  private static Logger       logger              = new Logger("com.ftd.mo.bo.LoadCustomOrderBO");


	//request parameters required to be sent back to XSL all the time.
  private String              buyerName						= null;
  private String              categoryIndex				= null;
  private String              companyId           = null;
  private String              customerId    			= null;
  private String              deliveryDate        = "";
  private String              deliveryDateRange   = "";
  private String              externalOrderNumber	= null;
  private String              masterOrderNumber		= null;
  private String              occasion  					= null;
  private String              occasionText				= null;
  private	String							origProductAmount		= null;
  private	String							origProductId				= null;
  private String              orderDetailId   		= null;
  private String              orderGuid						= null;
  private String              originId            = null;
  private String              pageNumber					= null;
  private String              pricePointId				= null;
  private String              recipientCity       = null;
  private String              recipientCountry    = null;
  private String              recipientState      = null;
  private String              recipientZipCode    = null;
  private String              rewardType          = null;
  private String              shipMethod          = null;
  private String              sourceCode					= null;

	//other variables needed for this class
  private String              actionType          = null;
  private boolean             addOnFound    	  	= false;
  private String              csrId               = null;
  private boolean             domesticFlag		  	= true;
  private String              domesticSvcFee			= null;
  private String              domIntFlag          = null;
  private String              imageLocation				= null;
  private String              internationalSvcFee	= null;
  private HashMap             hashXML             = new HashMap();
  private HashMap             pageData            = new HashMap();
  private String              partnerId						= null;
  private String              pricingCode					= null;
  private String              productId           = null;
  private String              sessionId           = null;
  private String              snhId								= null;
  private SearchUtil          sUtil;
  private String              productType         = null;
  private String              displayCodifiedSpecial    = null;
  private List                promotionList       = new ArrayList();
  private ProductFlagsVO      flagsVO             = new ProductFlagsVO();

  private String              floristComments     = null;
  private String              commentId           = null;
  
  private static final String POPUP_INDICATOR_OK        = "O";
  private static final String POPUP_INDICATOR_CONFIRM   = "C";
  private static final String LOAD_PRODUCT_CATEGORY     = "loadProductCategory.do";
  private static final String LOAD_CUSTOM_ORDER         = "loadCustomOrder.do?action_type=delete_addon";
  private static final String HK_UPDATE_COMMENTS_XML    = "HK_updateComments";


/*******************************************************************************************
 * Constructor 1
 *******************************************************************************************/
  public LoadCustomOrderBO()
  {
  }


/*******************************************************************************************
 * Constructor 2
 *******************************************************************************************/
  public LoadCustomOrderBO(HttpServletRequest request, Connection con)
  {
    this.request = request;
    this.con = con;
  }


/*******************************************************************************************
  * processRequest()
  ******************************************************************************************
  *
  * @return HashMap - contains 0 - many XML documents of data, 1 HashMap each for page
  *                   details and search criteria
  * @throws
  */
  public HashMap processRequest() throws Exception
  {
    //HashMap that will be returned to the calling class
    HashMap returnHash = new HashMap();

    //instantiate the search util
    this.sUtil = new SearchUtil(this.con);

    //find if the recipient recipientCountry was domestic or not
    this.domesticFlag = this.sUtil.isDomestic(this.recipientCountry);
    if (this.domesticFlag)
      this.domIntFlag = COMConstants.CONS_DELIVERY_TYPE_DOMESTIC;
    else
      this.domIntFlag = COMConstants.CONS_DELIVERY_TYPE_INTERNATIONAL;


    //Get info from the request object
    getRequestInfo(this.request);

    //Get config file info
    getConfigInfo();

    //load the page
    processMain();

    //populate the remainder of the fields on the page data
    populatePageData();

    //process breadcrumbs
    processBreadcrumbs();

    //populate the sub header info
    populateSubHeaderInfo();

    //At this point, we should have three HashMaps.
    // HashMap1 = hashXML         - hashmap that contains zero to many Documents
    // HashMap2 = pageData        - hashmap that contains String data at page level
    //
    //Combine all of the above HashMaps into one HashMap, called returnHash

    //retrieve all the Documents from hashXML and put them in returnHash
    //retrieve the keyset
    Set ks1 = hashXML.keySet();
    Iterator iter = ks1.iterator();
    String key = null;
    Document doc = null;

    //Iterate thru the keyset
    while(iter.hasNext())
    {
      //get the key
      key = iter.next().toString();

      //retrieve the XML document
      doc = (Document) hashXML.get(key);
      //put the XML object in the returnHash
      returnHash.put(key, (Document) doc);
    }

    //append pageData to returnHash
    returnHash.put("pageData",        (HashMap)this.pageData);

    return returnHash;
  }



/*******************************************************************************************
  * getRequestInfo(HttpServletRequest request)
  ******************************************************************************************
  * Retrieve the info from the request object
  *
  * @param  HttpServletRequest - request
  * @return none
  * @throws none
  */
  private void getRequestInfo(HttpServletRequest request) throws Exception
  {

    /****************************************************************************************
     *  Retrieve order/search specific parameters
     ***************************************************************************************/
    //retrieve the action
    if(request.getParameter(COMConstants.ACTION_TYPE)!=null)
    {
      this.actionType = request.getParameter(COMConstants.ACTION_TYPE);
    }

    //retrieve the buyer_full_name
    if(request.getParameter(COMConstants.BUYER_FULL_NAME)!=null)
    {
      this.buyerName = request.getParameter(COMConstants.BUYER_FULL_NAME);
    }

    //retrieve the category Index
    if(request.getParameter(COMConstants.CATEGORY_INDEX)!=null)
    {
      this.categoryIndex = request.getParameter(COMConstants.CATEGORY_INDEX);
    }

    //retrieve the company id
    if(request.getParameter(COMConstants.COMPANY_ID)!=null)
    {
      this.companyId = request.getParameter(COMConstants.COMPANY_ID);
    }

    //retrieve the customer id
    if(request.getParameter(COMConstants.CUSTOMER_ID)!=null)
    {
      this.customerId = request.getParameter(COMConstants.CUSTOMER_ID);
    }

    //retrieve the delivery date
    if(request.getParameter(COMConstants.DELIVERY_DATE)!=null)
    {
      this.deliveryDate = request.getParameter(COMConstants.DELIVERY_DATE);
    }

    //retrieve the delivery date range end
    if(request.getParameter(COMConstants.DELIVERY_DATE_RANGE_END)!=null)
    {
      this.deliveryDateRange = request.getParameter(COMConstants.DELIVERY_DATE_RANGE_END);
    }

    //retrieve the item order number
    if(request.getParameter(COMConstants.EXTERNAL_ORDER_NUMBER)!=null)
    {
      this.externalOrderNumber = request.getParameter(COMConstants.EXTERNAL_ORDER_NUMBER);
    }

    //retrieve the master order number
    if(request.getParameter(COMConstants.MASTER_ORDER_NUMBER)!=null)
    {
      this.masterOrderNumber = request.getParameter(COMConstants.MASTER_ORDER_NUMBER);
    }

    //retrieve the occasion id
    if(request.getParameter(COMConstants.OCCASION)!=null)
    {
      this.occasion = request.getParameter(COMConstants.OCCASION);
    }

    //retrieve the occasion text
    if(request.getParameter(COMConstants.OCCASION_TEXT)!=null)
    {
      this.occasionText = request.getParameter(COMConstants.OCCASION_TEXT);
    }

    //retrieve the order detail id
    if(request.getParameter(COMConstants.ORDER_DETAIL_ID)!=null)
    {
      this.orderDetailId = request.getParameter(COMConstants.ORDER_DETAIL_ID);
    }

    //retrieve the order guid
    if(request.getParameter(COMConstants.ORDER_GUID)!=null)
    {
      this.orderGuid = request.getParameter(COMConstants.ORDER_GUID);
    }

    //retrieve the origin id
    if(request.getParameter(COMConstants.ORIGIN_ID)!=null)
    {
      this.originId = request.getParameter(COMConstants.ORIGIN_ID);
    }

    //retrieve the orig product amount
    if(request.getParameter(COMConstants.ORIG_PRODUCT_AMOUNT)!=null)
    {
      this.origProductAmount = request.getParameter(COMConstants.ORIG_PRODUCT_AMOUNT);
    }

    //retrieve the orig product id
    if(request.getParameter(COMConstants.ORIG_PRODUCT_ID)!=null)
    {
      this.origProductId = (request.getParameter(COMConstants.ORIG_PRODUCT_ID)).toUpperCase();
    }

    //retrieve the page number
    if(request.getParameter(COMConstants.PAGE_NUMBER)!=null)
    {
      this.pageNumber = request.getParameter(COMConstants.PAGE_NUMBER);
    }

    //retrieve the price point id
    if(request.getParameter(COMConstants.PRICE_POINT_ID)!=null)
    {
      this.pricePointId = request.getParameter(COMConstants.PRICE_POINT_ID);
    }

    //retrieve the city
    if(request.getParameter(COMConstants.RECIPIENT_CITY)!=null)
    {
      this.recipientCity = request.getParameter(COMConstants.RECIPIENT_CITY);
    }

    //retrieve the recipientCountry id
    if(request.getParameter(COMConstants.RECIPIENT_COUNTRY)!=null)
    {
      this.recipientCountry = request.getParameter(COMConstants.RECIPIENT_COUNTRY);
    }

    //retrieve the postal code
    if(request.getParameter(COMConstants.RECIPIENT_ZIP_CODE)!=null)
    {
      this.recipientZipCode = request.getParameter(COMConstants.RECIPIENT_ZIP_CODE);
    }

    //retrieve the reward type
    if(request.getAttribute(COMConstants.REWARD_TYPE)!=null)
    {
      this.rewardType = request.getAttribute(COMConstants.REWARD_TYPE).toString();
    }
    if((this.rewardType == null || this.rewardType.equalsIgnoreCase("")) && request.getParameter(COMConstants.REWARD_TYPE)!=null)
    {
      this.rewardType = request.getParameter(COMConstants.REWARD_TYPE);
    }

    //retrieve the recipientState
    if(request.getParameter(COMConstants.RECIPIENT_STATE)!=null)
    {
      this.recipientState = request.getParameter(COMConstants.RECIPIENT_STATE);
    }

    //retrieve the ship method
    if(request.getParameter(COMConstants.SHIP_METHOD)!=null)
    {
      this.shipMethod = request.getParameter(COMConstants.SHIP_METHOD);
    }

    //retrieve the source code
    if(request.getParameter(COMConstants.SOURCE_CODE)!=null)
    {
      this.sourceCode = request.getParameter(COMConstants.SOURCE_CODE);
    }

    /****************************************************************************************
     *  Retrieve Security Info
     ***************************************************************************************/
    this.sessionId   = request.getParameter(COMConstants.SEC_TOKEN);

    /****************************************************************************************
     *  This page can also be be invoked from the UpdateCustomOrderBO if an error were
     *  found.  In this case, just retrieve the error xml, and populate the page data.
     *  Normal LoadCustomOrderBO processing will occur.
     *
     ***************************************************************************************/

    HashMap updateCustomOrderResults = (HashMap) request.getAttribute(
                                                  COMConstants.HK_UPDATE_CUSTOM_ORDER_ATTRIBUTE);
    if (updateCustomOrderResults != null && updateCustomOrderResults.size() > 0)
    {
      this.hashXML.put(COMConstants.HK_ERROR_MESSAGES,
                        (Document) updateCustomOrderResults.get(COMConstants.HK_ERROR_MESSAGES));
    }

  }


/*******************************************************************************************
  * getConfigInfo()
  ******************************************************************************************
  * Retrieve the info from the configuration file
  *
  * @param none
  * @return
  * @throws none
  */
  private void getConfigInfo() throws Exception
  {

    ConfigurationUtil cu = null;
    cu = ConfigurationUtil.getInstance();

    //get CUSTOM_ORDER_PRODUCT_ID
    if (cu.getProperty(COMConstants.PROPERTY_FILE, COMConstants.CONS_CUSTOM_ORDER_PRODUCT_ID) != null)
    {
      this.productId  = (cu.getProperty(COMConstants.PROPERTY_FILE, COMConstants.CONS_CUSTOM_ORDER_PRODUCT_ID)).toUpperCase();
    }

  }



/*******************************************************************************************
  * populatePageData()
  ******************************************************************************************
  * Populate the page data with the remainder of the fields
  *
  * @param none
  * @return
  * @throws none
  */
  private void populatePageData()
  {
    this.pageData.put(COMConstants.PD_MO_BUYER_FULL_NAME,         this.buyerName);
    this.pageData.put(COMConstants.PD_MO_CATEGORY_INDEX,          this.categoryIndex);
    this.pageData.put(COMConstants.PD_MO_COMPANY_ID,              this.companyId);
    this.pageData.put(COMConstants.PD_MO_CUSTOMER_ID,             this.customerId);
    this.pageData.put(COMConstants.PD_MO_DELIVERY_DATE,           this.deliveryDate);
    this.pageData.put(COMConstants.PD_MO_DELIVERY_DATE_RANGE_END, this.deliveryDateRange);
    this.pageData.put(COMConstants.PD_MO_DOMESTIC_SRVC_FEE,       this.domesticSvcFee);
    this.pageData.put(COMConstants.PD_MO_DOMESTIC_INT_FLAG,       this.domIntFlag);
    this.pageData.put(COMConstants.PD_MO_EXTERNAL_ORDER_NUMBER,   this.externalOrderNumber);
    this.pageData.put(COMConstants.PD_MO_INTERNATIONAL_SRVC_FEE,  this.internationalSvcFee);
    this.pageData.put(COMConstants.PD_MO_MASTER_ORDER_NUMBER,     this.masterOrderNumber);
    this.pageData.put(COMConstants.PD_MO_OCCASION,                this.occasion);
    this.pageData.put(COMConstants.PD_MO_OCCASION_TEXT,           this.occasionText);
    this.pageData.put(COMConstants.PD_MO_ORIG_PRODUCT_AMOUNT,			this.origProductAmount);
    this.pageData.put(COMConstants.PD_MO_ORIG_PRODUCT_ID,					this.origProductId);
    this.pageData.put(COMConstants.PD_MO_ORDER_DETAIL_ID,         this.orderDetailId);
    this.pageData.put(COMConstants.PD_MO_ORDER_GUID,              this.orderGuid);
    this.pageData.put(COMConstants.PD_MO_ORIGIN_ID,               this.originId);
    this.pageData.put(COMConstants.PD_MO_PAGE_NUMBER,             this.pageNumber);
    this.pageData.put(COMConstants.PD_MO_PARTNER_ID,              this.partnerId);
    this.pageData.put(COMConstants.PD_MO_PRICE_POINT_ID,          this.pricePointId);
    this.pageData.put(COMConstants.PD_MO_PRICING_CODE,            this.pricingCode);
    this.pageData.put(COMConstants.PD_MO_PRODUCT_ID,  						this.productId);
    this.pageData.put(COMConstants.PD_MO_PRODUCT_IMAGE_LOCATION,  this.imageLocation);
    this.pageData.put(COMConstants.PD_MO_RECIPIENT_CITY,          this.recipientCity);
    this.pageData.put(COMConstants.PD_MO_RECIPIENT_COUNTRY,       this.recipientCountry);
    this.pageData.put(COMConstants.PD_MO_RECIPIENT_STATE,         this.recipientState);
    this.pageData.put(COMConstants.PD_MO_RECIPIENT_ZIP_CODE,      this.recipientZipCode);
    this.pageData.put(COMConstants.PD_MO_REWARD_TYPE,             this.rewardType);
    this.pageData.put(COMConstants.PD_MO_SHIP_METHOD,             this.shipMethod);
    this.pageData.put(COMConstants.PD_MO_SOURCE_CODE,             this.sourceCode);
    this.pageData.put(COMConstants.PD_MO_SNH_ID,                  this.snhId);
    this.pageData.put(COMConstants.FLORIST_COMMENTS,              this.floristComments);
    this.pageData.put(COMConstants.COMMENT_ID,                    this.commentId);
  }


/*******************************************************************************************
  * populateSubHeaderInfo()
  ******************************************************************************************
  * Populate the page data with the remainder of the fields
  *
  * @param none
  * @return
  * @throws none
  */
  private void populateSubHeaderInfo() throws Exception
  {
    BasePageBuilder bpb = new BasePageBuilder();

    //xml document for sub header
    Document subHeaderXML = DOMUtil.getDocument();

    //retrieve the sub header info
    subHeaderXML = bpb.retrieveSubHeaderData(this.request);

    //save the original product xml
    this.hashXML.put(COMConstants.HK_SUB_HEADER, subHeaderXML);

  }


/*******************************************************************************************
  * processBreadcrumbs()
  ******************************************************************************************
  *
  *
  * @param  none
  * @return none
  * @throws none
  */
  private void processBreadcrumbs() throws Exception
  {
    HashMap optionalParms = new HashMap();
    optionalParms.put(COMConstants.ACTION_TYPE,             this.actionType);
    optionalParms.put(COMConstants.BUYER_FULL_NAME,         this.buyerName);
    optionalParms.put(COMConstants.CATEGORY_INDEX,          this.categoryIndex);
    optionalParms.put(COMConstants.COMPANY_ID,              this.companyId);
    optionalParms.put(COMConstants.CUSTOMER_ID,             this.customerId);
    optionalParms.put(COMConstants.DELIVERY_DATE,           this.deliveryDate);
    optionalParms.put(COMConstants.DELIVERY_DATE_RANGE_END, this.deliveryDateRange);
    optionalParms.put(COMConstants.EXTERNAL_ORDER_NUMBER,   this.externalOrderNumber);
    optionalParms.put(COMConstants.INTERNATIONAL_SRVC_FEE,  this.internationalSvcFee);
    optionalParms.put(COMConstants.MASTER_ORDER_NUMBER,     this.masterOrderNumber);
    optionalParms.put(COMConstants.OCCASION,                this.occasion);
    optionalParms.put(COMConstants.OCCASION_TEXT,           this.occasionText);
    optionalParms.put(COMConstants.ORIG_PRODUCT_AMOUNT,  		this.origProductAmount);
    optionalParms.put(COMConstants.ORIG_PRODUCT_ID,					this.origProductId);
    optionalParms.put(COMConstants.ORDER_DETAIL_ID,         this.orderDetailId);
    optionalParms.put(COMConstants.ORDER_GUID,              this.orderGuid);
    optionalParms.put(COMConstants.ORIGIN_ID,               this.originId);
    optionalParms.put(COMConstants.PAGE_NUMBER,             this.pageNumber);
    optionalParms.put(COMConstants.PRICE_POINT_ID,          this.pricePointId);
    optionalParms.put(COMConstants.RECIPIENT_CITY,          this.recipientCity);
    optionalParms.put(COMConstants.RECIPIENT_COUNTRY,       this.recipientCountry);
    optionalParms.put(COMConstants.RECIPIENT_STATE,         this.recipientState);
    optionalParms.put(COMConstants.RECIPIENT_ZIP_CODE,      this.recipientZipCode);
    optionalParms.put(COMConstants.REWARD_TYPE,             this.rewardType);
    optionalParms.put(COMConstants.SHIP_METHOD,             this.shipMethod);
    optionalParms.put(COMConstants.SOURCE_CODE,             this.sourceCode);

    BreadcrumbUtil bcUtil = new BreadcrumbUtil();

//bcUtil.getBreadcrumbXML(  request,
                          //Title of breadcrumb,
                          //Name of Struts Action,
                          //Action parameter,
                          //Error display page,
                          //Flag indicating if breadcrumb should be a link, ,
                          //Optional hash of additional name/value parameters for this page
                      //)

    Document bcXML = bcUtil.getBreadcrumbXML(this.request, COMConstants.BCT_CUSTOM_ORDER,
                            COMConstants.BCA_CUSTOM_ORDER, null, "Error",
                            "true", optionalParms, this.orderDetailId);
    this.hashXML.put("breadcrumbs",  bcXML);

  }




/*******************************************************************************************
  * processMain()
  ******************************************************************************************
  *
  * @param  none
  * @return none
  * @throws none
  */
  private void processMain() throws Exception
  {
    //retrieve the order info
    retrieveOrderData();

    retrieveProductBySourceCodeInfo();

    retrieveFeeInfo();

    processCustomProduct();

    this.pageData.put("XSL", COMConstants.XSL_CUSTOM_ORDER);

  }


/*******************************************************************************************
 * processCustomProduct()
 *******************************************************************************************
  * This method is used to retrieve the info for product_id = 6611
  *
  */
  private void processCustomProduct() throws Exception
  {
      List promotionList = new ArrayList();
      ProductFlagsVO flagsVO = new ProductFlagsVO();

      HashMap productMap = new HashMap();

      productMap.put("page_number",          this.pageNumber);
      productMap.put("custom_order",         COMConstants.CONS_YES);

      //clear out zipcode for internationial searchers
      String zipCode = "";

      //obtain a list of products for this recipientCountry
      Document productsXML = this.sUtil.getProductByID( this.sourceCode,  this.recipientZipCode,
              this.recipientCountry,  this.productId,  null,  this.domesticFlag,
              this.recipientState,  this.pricingCode, this.domesticSvcFee, this.internationalSvcFee,
              this.partnerId,  promotionList, this.rewardType, flagsVO,  this.occasion, this.companyId,
              this.deliveryDate, this.recipientCity, this.deliveryDate, origProductId,
              this.shipMethod, productMap);

      this.hashXML.put(COMConstants.HK_OCCASION_LIST_XML,  productsXML);

      Set ks = productMap.keySet();
      Iterator iter = ks.iterator();
      String key;
      //Iterate thru the hashmap returned from the business object using the keyset
      while(iter.hasNext())
      {
        key = iter.next().toString();
        this.pageData.put(key, (String)productMap.get(key));
      }
      this.pageNumber = this.pageData.get("page_number")!=null?this.pageData.get("page_number").toString():this.pageNumber;


  }


/*******************************************************************************************
 * retrieveProductBySourceCodeInfo() (COM_SOURCE_CODE_RECORD_LOOKUP)
 *******************************************************************************************
  * This method is used to retrieve shipping code, pricing code and partner id
  *
  */
  private void retrieveProductBySourceCodeInfo() throws Exception
  {
    BasePageBuilder bpb = new BasePageBuilder();

    Document sourceCodeInfo = (Document) bpb.retrieveProductBySourceCodeInfo(this.con, this.sourceCode);

    //create an array.
    ArrayList aList = new ArrayList();

    //Initialize the array with the top and bottom nodes of the XML, and the node for which
    //the value must be retrieved. In this instance, we will retireve the value of
    //shipping_code
    aList.clear();
    aList.add(0,"PRODUCTS");
    aList.add(1,"PRODUCT");
    aList.add(2,"shipping_code");
    String shippingCode = DOMUtil.getNodeValue(sourceCodeInfo, aList);
    this.snhId = shippingCode;

    //Initialize the array with the top and bottom nodes of the XML, and the node for which
    //the value must be retrieved. In this instance, we will retireve the value of
    //pricing_code
    aList.clear();
    aList.add(0,"PRODUCTS");
    aList.add(1,"PRODUCT");
    aList.add(2,"pricing_code");
    String priceCode = DOMUtil.getNodeValue(sourceCodeInfo, aList);
    this.pricingCode = priceCode;

    //Initialize the array with the top and bottom nodes of the XML, and the node for which
    //the value must be retrieved. In this instance, we will retireve the value of
    //partner_id
    aList.clear();
    aList.add(0,"PRODUCTS");
    aList.add(1,"PRODUCT");
    aList.add(2,"partner_id");
    String ptnrId = DOMUtil.getNodeValue(sourceCodeInfo, aList);
    this.partnerId = ptnrId;

    this.hashXML.put(COMConstants.HK_SOURCE_CODE_XML,  sourceCodeInfo);

  }


/*******************************************************************************************
 * retrieveFeeInfo() (COM_SNH_BY_ID)
 *******************************************************************************************
  * This method is used to retrieve domestic and international service fee
  *
  */
  private void retrieveFeeInfo() throws Exception
  {
    BasePageBuilder bpb = new BasePageBuilder();

    Document feeInfo = (Document) bpb.retrieveFeeInfo(this.con, this.snhId, this.deliveryDate);

    //create an array.
    ArrayList aList = new ArrayList();

    //Initialize the array with the top and bottom nodes of the XML, and the node for which
    //the value must be retrieved. In this instance, we will retireve the value of
    //first_order_domestic
    aList.clear();
    aList.add(0,"FEES");
    aList.add(1,"FEE");
    aList.add(2,"first_order_domestic");
    String domesticFee = DOMUtil.getNodeValue(feeInfo, aList);
    this.domesticSvcFee = domesticFee;

    //Initialize the array with the top and bottom nodes of the XML, and the node for which
    //the value must be retrieved. In this instance, we will retireve the value of
    //first_order_international
    aList.clear();
    aList.add(0,"FEES");
    aList.add(1,"FEE");
    aList.add(2,"first_order_international");
    String internationalFee = DOMUtil.getNodeValue(feeInfo, aList);
    this.internationalSvcFee = internationalFee;

  }



/*******************************************************************************************
 * buildXML()
 *******************************************************************************************/
  private Document buildXML(HashMap outMap, String cursorName, String topName, String bottomName, String type)
        throws Exception
  {

    //Create a CachedResultSet object using the cursor name that was passed.
    CachedResultSet rs = (CachedResultSet) outMap.get(cursorName);

    Document doc =  null;

    //Create an XMLFormat, and initialize the top and bottom node.  Note that since this method
    //can be/is called by various other methods, the top and botton names MUST be passed to it.
    XMLFormat xmlFormat = new XMLFormat();
    xmlFormat.setAttribute("type", type);
    xmlFormat.setAttribute("top", topName);
    xmlFormat.setAttribute("bottom", bottomName );

    //call the DOMUtil's converToXMLDOM method
    doc = (Document) DOMUtil.convertToXMLDOM(rs, xmlFormat);

    //return the xml document.
    return doc;

  }


/*******************************************************************************************
 * retrieveOrderData()
 *******************************************************************************************
  * This method is used to retrieve Order Data
  *
  */
  private void retrieveOrderData() throws Exception
  {
    //Instantiate UpdateOrderDAO
    UpdateOrderDAO uoDAO = new UpdateOrderDAO(this.con);

    //hashmap that will contain the output from the stored proc
    HashMap tempOrderInfo = new HashMap();

    //Call getOrderDetailsUpdate method in the DAO
    tempOrderInfo = uoDAO.getOrderDetailsUpdate(this.orderDetailId,
                            COMConstants.CONS_ENTITY_TYPE_PRODUCT_DETAIL, false, this.csrId, this.productId );


    //Create an Document and call a method that will transform the CachedResultSet from the cursor:
    //OUT_ORIG_PRODUCT into an Document.  Store this XML object in hashXML HashMap
    Document origProductXML =
        buildXML(tempOrderInfo, "OUT_ORIG_PRODUCT",     "PD_ORIG_PRODUCTS",   "PD_ORIG_PRODUCT",   "element");
    this.hashXML.put(COMConstants.HK_ORIG_PRODUCT_XML,  origProductXML);

    //Create an Document and call a method that will transform the CachedResultSet from the cursor:
    //OUT_UPD_ORDER_CUR into an Document.  Store this XML object in hashXML HashMap
    Document orderProductXML =
        buildXML(tempOrderInfo, "OUT_UPD_ORDER_CUR",    "PD_ORDER_PRODUCTS",   "PD_ORDER_PRODUCT",  "element");
    this.hashXML.put(COMConstants.HK_ORDER_PRODUCT_XML, orderProductXML);

    //Create an Document and call a method that will transform the CachedResultSet from the cursor:
    //OUT_UPD_COMMENTS_CUR into an Document.  Store this XML object in hashXML HashMap
    Document updateCommentsXML =
        buildXML(tempOrderInfo, "OUT_UPD_COMMENTS_CUR",    "PD_UPDATE_COMMENTS",   "PD_UPDATE_COMMENT",  "element");
 
     //Retrieve the florist comments and add it to page data.
     Node xmlNode = null;
     NodeList commentId = null;
     NodeList commentType = null;
     NodeList commentText = null;
     Node itemNode = null;
    
     NodeList nl = DOMUtil.selectNodes(updateCommentsXML,"/PD_UPDATE_COMMENTS/PD_UPDATE_COMMENT");
    
     for(int i=0; i<nl.getLength(); i++) 
     {
        xmlNode = (Node) nl.item(i);
    
        try
           {
             itemNode = DOMUtil.selectSingleNode(xmlNode,"comment_id/text()");
             if(itemNode != null)
             {
                commentId = DOMUtil.selectNodes(xmlNode,"comment_id/text()");
                commentType = DOMUtil.selectNodes(xmlNode,"comment_type/text()");
                commentText =  DOMUtil.selectNodes(xmlNode,"comment_text/text()");
              
                for(int j=0; j<commentId.getLength(); j++)
                {
                  if (commentType.item(j).getNodeValue().equals("Florist"))
                    {
                      this.floristComments =  commentText.item(j).getNodeValue();
                      this.commentId       =  commentId.item(j).getNodeValue();
                    }
                }
              }
             }
             catch(Exception ex)
           {
              logger.error("Error parsing xml in LoadCustomOrderBO : retrieveOrderData() method: " + ex.toString()); 
           }
    }
     
    this.hashXML.put(this.HK_UPDATE_COMMENTS_XML, updateCommentsXML);

    //Create an Document and call a method that will transform the CachedResultSet from the cursor:
    //OUT_ORIG_TAXES_CUR into an Document.  Store this XML object in hashXML HashMap
    Document orderTaxXML =
        buildXML(tempOrderInfo, "OUT_ORIG_TAXES_CUR", "PD_ORDER_TAXES", "PD_ORDER_TAX",   "element");
    this.hashXML.put(COMConstants.HK_ORDER_TAX_XML, orderTaxXML);

  }

 /*******************************************************************************************
  * generateErrorNode()
  *******************************************************************************************
  *
  * Adds an error message to the error message XML doc
  *
  * @params messageText - Message text
  * @params fieldInError - Name of erred field (optional).
  * @params popupType - Type of popup window (optional).
  * @params popupConfirmation - Return page if popup confirmation selected (optional).
  * @params popupCancel - Return page if confirmation is cancelled (optional).
  */
  private void generateErrorNode(String messageText, String fieldInError,
                                   String popupType, String popupConfirmation, String popupCancel)
        throws Exception
  {

    String l_popupType = popupType;
    if (fieldInError == null && popupType == null) {
      l_popupType = POPUP_INDICATOR_OK;
    }

    // Generate top node
    Document errorMsgsXML = (Document) DOMUtil.getDefaultDocument();
    Element errMsgsTopElement = errorMsgsXML.createElement("ERROR_MESSAGES");
    errorMsgsXML.appendChild(errMsgsTopElement);

    if (messageText != null)
    {
      Element errorMsgElement = errorMsgsXML.createElement("ERROR_MESSAGE");
      errMsgsTopElement.appendChild(errorMsgElement);

      Element errMsgNode = errorMsgsXML.createElement("TEXT");
      errorMsgElement.appendChild(errMsgNode);
      errMsgNode.appendChild(errorMsgsXML.createTextNode(messageText));

      Element errFieldNode = errorMsgsXML.createElement("FIELDNAME");
      errorMsgElement.appendChild(errFieldNode);
      if (fieldInError != null)
        errFieldNode.appendChild(errorMsgsXML.createTextNode(fieldInError));

      Element popupIndicatorNode = errorMsgsXML.createElement("POPUP_INDICATOR");
      errorMsgElement.appendChild(popupIndicatorNode);
      if (l_popupType != null)
        popupIndicatorNode.appendChild(errorMsgsXML.createTextNode(l_popupType));

      Element popupGotoNode = errorMsgsXML.createElement("POPUP_GOTO_PAGE");
      errorMsgElement.appendChild(popupGotoNode);
      if (popupConfirmation != null)
        popupGotoNode.appendChild(errorMsgsXML.createTextNode(popupConfirmation));

      Element popupCancelNode = errorMsgsXML.createElement("POPUP_CANCEL_PAGE");
      errorMsgElement.appendChild(popupCancelNode);
      if (popupCancel != null)
        popupCancelNode.appendChild(errorMsgsXML.createTextNode(popupCancel));
    }

    this.hashXML.put(COMConstants.HK_ERROR_MESSAGES,  errorMsgsXML);

  }
  
   /**
 * Get the value from the xmlNode 
 * 
 * @param xmlNode Node
 * @param nodeName String
 * @return String
 */
private String getSingleNode(Node xmlNode, String nodeName)
{
    Node itemNode = null;
    String nodeValue = "";
    
    try
    {
      itemNode =  DOMUtil.selectSingleNode(xmlNode,nodeName);
      if(itemNode != null)
      {
        itemNode = DOMUtil.selectSingleNode(xmlNode,nodeName);
        nodeValue = itemNode.getNodeValue();
      } 

    }
    catch(Exception ex)
    {
      logger.error("Error getting the node.  Node may not exist in xml file. " + ex.toString()); 
    }
 
  return nodeValue; 
}


}