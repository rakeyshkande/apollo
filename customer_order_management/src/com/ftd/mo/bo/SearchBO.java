package com.ftd.mo.bo;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.dao.ViewDAO;
import com.ftd.customerordermanagement.util.BasePageBuilder;
import com.ftd.mo.util.BreadcrumbUtil;
import com.ftd.mo.util.SearchUtil;
import com.ftd.mo.vo.ProductFlagsVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.security.SecurityManager;
import com.ftd.security.cache.vo.UserInfo;






public class SearchBO
{

/*******************************************************************************************
 * Class level variables
 *******************************************************************************************/
  private HttpServletRequest  request             = null;
  private Connection          con                 = null;
  private static Logger       logger              = new Logger("com.ftd.mo.bo.SearchBO");


	//request parameters required to be sent back to XSL all the time.
  private String              buyerName						= null;
  private String              categoryIndex				= null;
  private String              companyId           = null;
  private String              customerId          = null;
  private String              deliveryDate        = "";
  private String              deliveryDateRange   = "";
  private String              externalOrderNumber	= null;
  private String              masterOrderNumber		= null;
  private String              occasion  					= null;
  private String              occasionText				= null;
  private String              orderDetailId				= null;
  private String              orderGuid						= null;
  private String              originId            = null;
  private	String							origProductAmount		= null;
  private	String							origProductId				= null;
  private String              pageNumber					= null;
  private String              pricePointId				= null;
  private String              recipientCity       = null;
  private String              recipientCountry    = null;
  private String              recipientState      = null;
  private String              recipientZipCode    = null;
  private String              rewardType          = null;
  private String              sourceCode					= null;

	//other variables needed for this class
  private String              actionType          = null;
  private String              csrId               = null;
  private boolean             domesticFlag		  	= true;
  private String              domesticSvcFee      = "0";
  private String              domIntFlag          = null;
  private HashMap             hashXML             = new HashMap();
  private String              imageLocation				= null;
  private String              internationalSvcFee = "0";
  private String              keywordSFMB         = null;
  private String              keywordGIFT         = null;
  private String              keywordHIGH         = null;
  private String              keywords            = null;
  private String              listFlag					  = null;
  private HashMap             pageData            = new HashMap();
  private String              partnerId           = null;
  private String              pricingCode         = null;
  private String              productId           = null;
  private String              sessionId           = null;
  private String              serverIP            = null;
  private String              serverPort          = null;
  private String              shipMethod          = null;
  private String              snhId               = null;
  private SearchUtil          sUtil;
  private String              totalPages          = null;

  // Standard, Deluxe, Premium labels
  private String              standardLabel       = null;
  private String              deluxeLabel         = null;
  private String              premiumLabel        = null;


/*******************************************************************************************
 * Constructor 1
 *******************************************************************************************/
  public SearchBO()
  {
  }


/*******************************************************************************************
 * Constructor 2
 *******************************************************************************************/
  public SearchBO(HttpServletRequest request, Connection con)
  {
    this.request = request;
    this.con = con;
  }


/*******************************************************************************************
  * processRequest()
  ******************************************************************************************
  *
  * @return HashMap - contains 0 - many XML documents of data, 1 HashMap each for page
  *                   details and search criteria
  * @throws
  */
  public HashMap processRequest() throws Exception
  {
    //HashMap that will be returned to the calling class
    HashMap returnHash = new HashMap();

    //Get info from the request object
    getRequestInfo(this.request);

    //Get config file info
    getConfigInfo();

    //instantiate the search util
    this.sUtil = new SearchUtil(this.con);

    //find if the recipient recipientCountry was domestic or not
    this.domesticFlag = this.sUtil.isDomestic(this.recipientCountry);
    if (this.domesticFlag)
      this.domIntFlag = COMConstants.CONS_DELIVERY_TYPE_DOMESTIC;
    else
      this.domIntFlag = COMConstants.CONS_DELIVERY_TYPE_INTERNATIONAL;

    //process the action
    processMain();

    //populate the remainder of the fields on the page data
    populatePageData();

    //populate the sub header info
    populateSubHeaderInfo();

    //At this point, we should have three HashMaps.
    // HashMap1 = hashXML         - hashmap that contains zero to many Documents
    // HashMap2 = pageData        - hashmap that contains String data at page level
    //
    //Combine all of the above HashMaps into one HashMap, called returnHash

    //retrieve all the Documents from hashXML and put them in returnHash
    //retrieve the keyset
    Set ks1 = hashXML.keySet();
    Iterator iter = ks1.iterator();
    String key = null;
    Document doc = null;

    //Iterate thru the keyset
    while(iter.hasNext())
    {
      //get the key
      key = iter.next().toString();

      //retrieve the XML document
      doc = (Document) hashXML.get(key);

      //put the XML object in the returnHash
      returnHash.put(key, (Document) doc);
    }

    //append pageData to returnHash
    returnHash.put("pageData",        (HashMap)this.pageData);

    return returnHash;
  }



/*******************************************************************************************
  * getRequestInfo(HttpServletRequest request)
  ******************************************************************************************
  * Retrieve the info from the request object
  *
  * @param  HttpServletRequest - request
  * @return none
  * @throws none
  */
  private void getRequestInfo(HttpServletRequest request) throws Exception
  {

    /****************************************************************************************
     *  Retrieve order/search specific parameters
     ***************************************************************************************/
    //retrieve the action
    if(request.getParameter(COMConstants.ACTION_TYPE)!=null)
    {
      this.actionType = request.getParameter(COMConstants.ACTION_TYPE);
    }

    //retrieve the buyer_full_name
    if(request.getParameter(COMConstants.BUYER_FULL_NAME)!=null)
    {
      this.buyerName = request.getParameter(COMConstants.BUYER_FULL_NAME);
    }

    //retrieve the category Index
    if(request.getParameter(COMConstants.CATEGORY_INDEX)!=null)
    {
      this.categoryIndex = request.getParameter(COMConstants.CATEGORY_INDEX);
    }

    //retrieve the company id
    if(request.getParameter(COMConstants.COMPANY_ID)!=null)
    {
      this.companyId = request.getParameter(COMConstants.COMPANY_ID);
    }

    //retrieve the customer id
    if(request.getParameter(COMConstants.CUSTOMER_ID)!=null)
    {
      this.customerId = request.getParameter(COMConstants.CUSTOMER_ID);
    }

    //retrieve the delivery date
    if(request.getParameter(COMConstants.DELIVERY_DATE)!=null)
    {
      this.deliveryDate = request.getParameter(COMConstants.DELIVERY_DATE);
    }

    //retrieve the delivery date range end
    if(request.getParameter(COMConstants.DELIVERY_DATE_RANGE_END)!=null)
    {
      this.deliveryDateRange = request.getParameter(COMConstants.DELIVERY_DATE_RANGE_END);
    }

    //retrieve the item order number
    if(request.getParameter(COMConstants.EXTERNAL_ORDER_NUMBER)!=null)
    {
      this.externalOrderNumber = request.getParameter(COMConstants.EXTERNAL_ORDER_NUMBER);
    }

    //retrieve the list flag
    if(request.getParameter(COMConstants.LIST_FLAG)!=null)
    {
      this.listFlag = request.getParameter(COMConstants.LIST_FLAG);
    }

    //retrieve the master order number
    if(request.getParameter(COMConstants.MASTER_ORDER_NUMBER)!=null)
    {
      this.masterOrderNumber = request.getParameter(COMConstants.MASTER_ORDER_NUMBER);
    }

    //retrieve the occasion id
    if(request.getParameter(COMConstants.OCCASION)!=null)
    {
      this.occasion = request.getParameter(COMConstants.OCCASION);
    }

    //retrieve the occasion text
    if(request.getParameter(COMConstants.OCCASION_TEXT)!=null)
    {
      this.occasionText = request.getParameter(COMConstants.OCCASION_TEXT);
    }

    //retrieve the orig product amount
    if(request.getParameter(COMConstants.ORIG_PRODUCT_AMOUNT)!=null)
    {
      this.origProductAmount = request.getParameter(COMConstants.ORIG_PRODUCT_AMOUNT);
    }

    //retrieve the orig product id
    if(request.getParameter(COMConstants.ORIG_PRODUCT_ID)!=null)
    {
      this.origProductId = (request.getParameter(COMConstants.ORIG_PRODUCT_ID)).toUpperCase();
    }

    //retrieve the order detail id
    if(request.getParameter(COMConstants.ORDER_DETAIL_ID)!=null)
    {
      this.orderDetailId = request.getParameter(COMConstants.ORDER_DETAIL_ID);
    }

    //retrieve the order guid
    if(request.getParameter(COMConstants.ORDER_GUID)!=null)
    {
      this.orderGuid = request.getParameter(COMConstants.ORDER_GUID);
    }

    //retrieve the origin id
    if(request.getParameter(COMConstants.ORIGIN_ID)!=null)
    {
      this.originId = request.getParameter(COMConstants.ORIGIN_ID);
    }

    //retrieve the page number
    if(request.getParameter(COMConstants.PAGE_NUMBER)!=null)
    {
      this.pageNumber = request.getParameter(COMConstants.PAGE_NUMBER);
    }

    //retrieve the price point id
    if(request.getParameter(COMConstants.PRICE_POINT_ID)!=null)
    {
      this.pricePointId = request.getParameter(COMConstants.PRICE_POINT_ID);
    }

    //retrieve the product id
    if(request.getParameter(COMConstants.PRODUCT_ID)!=null)
    {
      this.productId = (request.getParameter(COMConstants.PRODUCT_ID)).toUpperCase();
    }

    //retrieve the city
    if(request.getParameter(COMConstants.RECIPIENT_CITY)!=null)
    {
      this.recipientCity = request.getParameter(COMConstants.RECIPIENT_CITY);
    }

    //retrieve the recipientCountry id
    if(request.getParameter(COMConstants.RECIPIENT_COUNTRY)!=null)
    {
      this.recipientCountry = request.getParameter(COMConstants.RECIPIENT_COUNTRY);
    }

    //retrieve the recipientState
    if(request.getParameter(COMConstants.RECIPIENT_STATE)!=null)
    {
      this.recipientState = request.getParameter(COMConstants.RECIPIENT_STATE);
    }

    //retrieve the postal code
    if(request.getParameter(COMConstants.RECIPIENT_ZIP_CODE)!=null)
    {
      this.recipientZipCode = request.getParameter(COMConstants.RECIPIENT_ZIP_CODE);
    }

    //retrieve the reward type
    if(request.getAttribute(COMConstants.REWARD_TYPE)!=null)
    {
      this.rewardType = request.getAttribute(COMConstants.REWARD_TYPE).toString();
    }
    if((this.rewardType == null || this.rewardType.equalsIgnoreCase("")) && request.getParameter(COMConstants.REWARD_TYPE)!=null)
    {
      this.rewardType = request.getParameter(COMConstants.REWARD_TYPE);
    }

    //retrieve the search keyword
    if(request.getParameter(COMConstants.SEARCH_KEYWORD)!=null)
    {
      this.keywords = request.getParameter(COMConstants.SEARCH_KEYWORD);
    }

    //retrieve the ship method
    if(request.getParameter(COMConstants.SHIP_METHOD)!=null)
    {
      this.shipMethod = request.getParameter(COMConstants.SHIP_METHOD);
    }

    //retrieve the source code
    if(request.getParameter(COMConstants.SOURCE_CODE)!=null)
    {
      this.sourceCode = request.getParameter(COMConstants.SOURCE_CODE);
    }


    /****************************************************************************************
     *  Retrieve Security Info
     ***************************************************************************************/
    this.sessionId   = request.getParameter(COMConstants.SEC_TOKEN);

  }


/*******************************************************************************************
  * getConfigInfo()
  ******************************************************************************************
  * Retrieve the info from the configuration file
  *
  * @param none
  * @return
  * @throws none
  */
  private void getConfigInfo() throws Exception
  {

    ConfigurationUtil cu = null;
    cu = ConfigurationUtil.getInstance();

    //get csr Id
    String security = (String) cu.getProperty(COMConstants.PROPERTY_FILE,COMConstants.SECURITY_IS_ON);
    boolean securityIsOn = security.equalsIgnoreCase("true") ? true : false;

   //delete this before going to production.  the following should only check on securityIsOn
    if(securityIsOn || this.sessionId != null)
    {
      SecurityManager sm = SecurityManager.getInstance();
      UserInfo ui = sm.getUserInfo(this.sessionId);
      this.csrId = ui.getUserID();
    }

    //get PRODUCT_IMAGE_LOCATION
    if (cu.getProperty(COMConstants.PROPERTY_FILE, COMConstants.CONS_PRODUCT_IMAGE_LOCATION) != null)
    {
      this.imageLocation  = cu.getProperty(COMConstants.PROPERTY_FILE, COMConstants.CONS_PRODUCT_IMAGE_LOCATION);
      this.pageData.put(COMConstants.PD_MO_PRODUCT_IMAGE_LOCATION, imageLocation);
    }


    //get server ip address for keyword search
    if (cu.getFrpGlobalParm(COMConstants.COM_CONTEXT, COMConstants.CONS_KEYWORD_SEARCH_SERVER_IP) != null)
    {
      this.serverIP  = cu.getFrpGlobalParm(COMConstants.COM_CONTEXT, COMConstants.CONS_KEYWORD_SEARCH_SERVER_IP);
    }


    //get server port for keyword search
    if (cu.getProperty(COMConstants.PROPERTY_FILE, COMConstants.CONS_KEYWORD_SEARCH_SERVER_PORT) != null)
    {
      this.serverPort  = cu.getProperty(COMConstants.PROPERTY_FILE, COMConstants.CONS_KEYWORD_SEARCH_SERVER_PORT);
    }

    //get keyword markcode for SFMB
    if (cu.getProperty(COMConstants.PROPERTY_FILE, COMConstants.CONS_KEYWORD_SEARCH_MARKCODE_SFMB) != null)
    {
      this.keywordSFMB  = cu.getProperty(COMConstants.PROPERTY_FILE, COMConstants.CONS_KEYWORD_SEARCH_MARKCODE_SFMB);
    }

    //get keyword markcode for GIGT
    if (cu.getProperty(COMConstants.PROPERTY_FILE, COMConstants.CONS_KEYWORD_SEARCH_MARKCODE_GIFT) != null)
    {
      this.keywordGIFT  = cu.getProperty(COMConstants.PROPERTY_FILE, COMConstants.CONS_KEYWORD_SEARCH_MARKCODE_GIFT);
    }

    //get keyword markcode for HIGH
    if (cu.getProperty(COMConstants.PROPERTY_FILE, COMConstants.CONS_KEYWORD_SEARCH_MARKCODE_HIGH) != null)
    {
      this.keywordHIGH  = cu.getProperty(COMConstants.PROPERTY_FILE, COMConstants.CONS_KEYWORD_SEARCH_MARKCODE_HIGH);
    }

    //get Standard, Deluxe, Premium labels
    String tempLabel = cu.getFrpGlobalParm("FTDAPPS_PARMS", "FLORAL_LABEL_STANDARD");
    if (tempLabel != null) {
        this.standardLabel = tempLabel;
    }
    tempLabel = cu.getFrpGlobalParm("FTDAPPS_PARMS", "FLORAL_LABEL_DELUXE");
    if (tempLabel != null) {
        this.deluxeLabel = tempLabel;
    }
    tempLabel = cu.getFrpGlobalParm("FTDAPPS_PARMS", "FLORAL_LABEL_PREMIUM");
    if (tempLabel != null) {
        this.premiumLabel = tempLabel;
    }


  }



/*******************************************************************************************
  * populatePageData()
  ******************************************************************************************
  * Populate the page data with the remainder of the fields
  *
  * @param none
  * @return
  * @throws none
  */
  private void populatePageData()
  {
    this.pageData.put(COMConstants.PD_MO_BUYER_FULL_NAME,         this.buyerName);
    this.pageData.put(COMConstants.PD_MO_CATEGORY_INDEX,          this.categoryIndex);
    this.pageData.put(COMConstants.PD_MO_COMPANY_ID,              this.companyId);
    this.pageData.put(COMConstants.PD_MO_CUSTOMER_ID,             this.customerId);
    this.pageData.put(COMConstants.PD_MO_DELIVERY_DATE,           this.deliveryDate);
    this.pageData.put(COMConstants.PD_MO_DELIVERY_DATE_RANGE_END, this.deliveryDateRange);
    this.pageData.put(COMConstants.PD_MO_DOMESTIC_SRVC_FEE,       this.domesticSvcFee);
    this.pageData.put(COMConstants.PD_MO_DOMESTIC_INT_FLAG,       this.domIntFlag);
    this.pageData.put(COMConstants.PD_MO_DOMESTIC_SRVC_FEE,       this.domesticSvcFee);
    this.pageData.put(COMConstants.PD_MO_EXTERNAL_ORDER_NUMBER,   this.externalOrderNumber);
    this.pageData.put(COMConstants.PD_MO_PRODUCT_IMAGE_LOCATION,  this.imageLocation);
    this.pageData.put(COMConstants.PD_MO_INTERNATIONAL_SRVC_FEE,  this.internationalSvcFee);
    this.pageData.put(COMConstants.PD_MO_MASTER_ORDER_NUMBER,     this.masterOrderNumber);
    this.pageData.put(COMConstants.PD_MO_OCCASION,                this.occasion);
    this.pageData.put(COMConstants.PD_MO_OCCASION_TEXT,           this.occasionText);
    this.pageData.put(COMConstants.PD_MO_ORIG_PRODUCT_AMOUNT,			this.origProductAmount);
    this.pageData.put(COMConstants.PD_MO_ORIG_PRODUCT_ID,					this.origProductId);
    this.pageData.put(COMConstants.PD_MO_ORDER_DETAIL_ID,         this.orderDetailId);
    this.pageData.put(COMConstants.PD_MO_ORDER_GUID,              this.orderGuid);
    this.pageData.put(COMConstants.PD_MO_ORIGIN_ID,               this.originId);
    this.pageData.put(COMConstants.PD_MO_PAGE_NUMBER,             this.pageNumber);
    this.pageData.put(COMConstants.PD_MO_PARTNER_ID,              this.partnerId);
    this.pageData.put(COMConstants.PD_MO_PRICE_POINT_ID,          this.pricePointId);
    this.pageData.put(COMConstants.PD_MO_PRODUCT_ID,  						this.productId);
    this.pageData.put(COMConstants.PD_MO_PRICING_CODE,            this.pricingCode);
    this.pageData.put(COMConstants.PD_MO_RECIPIENT_CITY,          this.recipientCity);
    this.pageData.put(COMConstants.PD_MO_RECIPIENT_COUNTRY,       this.recipientCountry);
    this.pageData.put(COMConstants.PD_MO_RECIPIENT_STATE,         this.recipientState);
    this.pageData.put(COMConstants.PD_MO_RECIPIENT_ZIP_CODE,      this.recipientZipCode);
    this.pageData.put(COMConstants.PD_MO_REWARD_TYPE,             this.rewardType);
    this.pageData.put(COMConstants.PD_MO_SHIP_METHOD,             this.shipMethod);
    this.pageData.put(COMConstants.PD_MO_SNH_ID,                  this.snhId);
    this.pageData.put(COMConstants.PD_MO_SOURCE_CODE,             this.sourceCode);
    this.pageData.put(COMConstants.PD_MO_TOTAL_PAGES,             this.totalPages);

    this.pageData.put(COMConstants.PD_MO_FLORAL_LABEL_STANDARD, this.standardLabel);
    this.pageData.put(COMConstants.PD_MO_FLORAL_LABEL_DELUXE, this.deluxeLabel);
    this.pageData.put(COMConstants.PD_MO_FLORAL_LABEL_PREMIUM, this.premiumLabel);

  }



/*******************************************************************************************
  * populateSubHeaderInfo()
  ******************************************************************************************
  * Populate the page data with the remainder of the fields
  *
  * @param none
  * @return
  * @throws none
  */
  private void populateSubHeaderInfo() throws Exception
  {
    BasePageBuilder bpb = new BasePageBuilder();

    //xml document for sub header
    Document subHeaderXML = DOMUtil.getDocument();

    //retrieve the sub header info
    subHeaderXML = bpb.retrieveSubHeaderData(this.request);

    //save the original product xml
    this.hashXML.put(COMConstants.HK_SUB_HEADER, subHeaderXML);

  }


/*******************************************************************************************
  * processBreadcrumbs()
  ******************************************************************************************
  *
  *
  * @param  none
  * @return none
  * @throws none
  */
  private void processBreadcrumbs(String breadcrumbTitle, String strutsAction) throws Exception
  {
    HashMap optionalParms = new HashMap();
    optionalParms.put(COMConstants.ACTION_TYPE,         				    this.actionType);
    optionalParms.put(COMConstants.BUYER_FULL_NAME,         				this.buyerName);
    optionalParms.put(COMConstants.CATEGORY_INDEX,          				this.categoryIndex);
    optionalParms.put(COMConstants.COMPANY_ID,              				this.companyId);
    optionalParms.put(COMConstants.CUSTOMER_ID,             				this.customerId);
    optionalParms.put(COMConstants.DELIVERY_DATE,           				this.deliveryDate);
    optionalParms.put(COMConstants.DELIVERY_DATE_RANGE_END, 				this.deliveryDateRange);
    optionalParms.put(COMConstants.EXTERNAL_ORDER_NUMBER,   				this.externalOrderNumber);
    optionalParms.put(COMConstants.LIST_FLAG,             				  this.listFlag);
    optionalParms.put(COMConstants.MASTER_ORDER_NUMBER,     				this.masterOrderNumber);
    optionalParms.put(COMConstants.OCCASION,             				    this.occasion);
    optionalParms.put(COMConstants.OCCASION_TEXT,           				this.occasionText);
    optionalParms.put(COMConstants.ORIG_PRODUCT_AMOUNT,  						this.origProductAmount);
    optionalParms.put(COMConstants.ORIG_PRODUCT_ID,									this.origProductId);
    optionalParms.put(COMConstants.ORDER_DETAIL_ID,         				this.orderDetailId);
    optionalParms.put(COMConstants.ORDER_GUID,              				this.orderGuid);
    optionalParms.put(COMConstants.ORIGIN_ID,               				this.originId);
    optionalParms.put(COMConstants.PAGE_NUMBER,             				this.pageNumber);
    optionalParms.put(COMConstants.PRICE_POINT_ID,          				this.pricePointId);
    optionalParms.put(COMConstants.PRODUCT_ID,            				  this.productId);
    optionalParms.put(COMConstants.RECIPIENT_CITY,          				this.recipientCity);
    optionalParms.put(COMConstants.RECIPIENT_COUNTRY,       				this.recipientCountry);
    optionalParms.put(COMConstants.RECIPIENT_STATE,         				this.recipientState);
    optionalParms.put(COMConstants.RECIPIENT_ZIP_CODE,      				this.recipientZipCode);
    optionalParms.put(COMConstants.REWARD_TYPE,                     this.rewardType);
    optionalParms.put(COMConstants.SEARCH_KEYWORD,           				this.keywords);
    optionalParms.put(COMConstants.SHIP_METHOD,                     this.shipMethod);
    optionalParms.put(COMConstants.SOURCE_CODE,             				this.sourceCode);

    BreadcrumbUtil bcUtil = new BreadcrumbUtil();

//bcUtil.getBreadcrumbXML(  request,
                          //Title of breadcrumb,
                          //Name of Structs Action,
                          //Action parameter,
                          //Error display page,
                          //Flag indicating if breadcrumb should be a link, ,
                          //Optional hash of additional name/value parameters for this page
                      //)

    Document bcXML = bcUtil.getBreadcrumbXML(this.request, breadcrumbTitle, strutsAction, null, "Error",
                            "true", optionalParms, this.orderDetailId);
    this.hashXML.put("breadcrumbs",  bcXML);

  }




/*******************************************************************************************
  * processMain()
  ******************************************************************************************
  *
  * @param  none
  * @return none
  * @throws none
  */
  private void processMain() throws Exception
  {

    //get the source code info
    retrieveProductBySourceCodeInfo();

    //get the fee info
    retrieveFeeInfo();

    //if the search_type = keywordsearch
    if (this.actionType.equalsIgnoreCase(COMConstants.CONS_SEARCH_KEYWORD_SEARCH))
    {
      //retrieve the keyword
      processKeywordSearch();
    }
    //else, do a product search
    else
    {
      //retrieve the product info
      processProductSearch();
    }

    this.hashXML.put(COMConstants.HK_CSCI_VIEWING_XML, retrieveCSRViewing());
  }


/*******************************************************************************************
 * processProductSearch()
 *******************************************************************************************
  * This method is used to call the
  *
  */
  private void processProductSearch() throws Exception
  {
		Enumeration enum1 = request.getParameterNames();
    String param = "";
    String value = "";
    ArrayList promotionList = new ArrayList();
    ProductFlagsVO flagsVO = new ProductFlagsVO();

    if(this.listFlag == null || this.listFlag.length() <=0)
    {
      this.listFlag = "N";
    }


    HashMap productMap = new HashMap();

    HashMap parms = new HashMap();
    while(enum1.hasMoreElements())
    {
        param = (String) enum1.nextElement();
        value = request.getParameter(param);
        parms.put(param, value);
    }

    try
    {
      HashMap args = new HashMap();
      Document xml = null;


      xml = this.sUtil.getProductByID( this.sourceCode,  this.recipientZipCode, this.recipientCountry,
            this.productId,  this.actionType,  this.domesticFlag, this.recipientState,  this.pricingCode,
            this.domesticSvcFee, this.internationalSvcFee,  this.partnerId,  promotionList,
            this.rewardType, flagsVO,  this.occasion, this.companyId,
            this.deliveryDate, this.recipientCity,  this.deliveryDate, this.origProductId,
            this.shipMethod, productMap);

      if(flagsVO.isUpsellExists())
      {

        this.pageData.put("XSL", COMConstants.XSL_UPSELL_DETAIL);

        if(flagsVO.getUpsellSkip() != null && !flagsVO.getUpsellSkip().equals("N"))
        {
          args.put("productId", flagsVO.getUpsellSkip());

          xml = this.sUtil.getProductByID( this.sourceCode,  this.recipientZipCode,
                      this.recipientCountry, flagsVO.getUpsellSkip(),  this.actionType,
                      this.domesticFlag, this.recipientState,  this.pricingCode,
                      this.domesticSvcFee, this.internationalSvcFee,  this.partnerId,
                      promotionList, this.rewardType, flagsVO,  this.occasion, this.companyId,
                      this.deliveryDate, this.recipientCity,  this.deliveryDate,
                      this.origProductId, this.shipMethod, productMap);

          this.pageData.put("XSL", COMConstants.XSL_LOAD_PRODUCT_DETAIL_ACTION);
        }
        else
        {
          //process Breadcrumb
          processBreadcrumbs(COMConstants.BCT_UPSELL_DETAIL, COMConstants.BCA_UPSELL_DETAIL);
        }

        this.hashXML.put(COMConstants.HK_UPSELL_XML, xml);

      }
      else
      {
        this.pageData.put("XSL", COMConstants.XSL_LOAD_PRODUCT_DETAIL_ACTION);

        this.hashXML.put(COMConstants.HK_PRODUCT_DETAIL_XML, xml);
      }

      Set ks = productMap.keySet();
      Iterator iter = ks.iterator();
      String key;
      //Iterate thru the hashmap returned from the business object using the keyset
      while(iter.hasNext())
      {
        key = iter.next().toString();
        this.pageData.put(key, (String)productMap.get(key));
      }
      this.pageNumber = this.pageData.get("page_number")!=null?this.pageData.get("page_number").toString():this.pageNumber;
      this.totalPages = this.pageData.get("total_pages")!=null?this.pageData.get("total_pages").toString():this.totalPages;

    }

    finally
    {
    }

  }


/*******************************************************************************************
 * processKeywordSearch()
 *******************************************************************************************
  * This method is used to call the
  *
  */
  private void processKeywordSearch() throws Exception
  {
    String searchResults;
    String productIdsNovator;

    //connect to novator, send the search, retrieve the response
    searchResults = retrieveDataFromNovator();

    //go thru the response, and get the product ids
    productIdsNovator = extractNovatorProductIds(searchResults);

    //call search util's getProductsByIds()
    if (productIdsNovator != null && !productIdsNovator.equalsIgnoreCase(""))
    {
      int numberOfProducts = retrieveKeywordProductsByIds(productIdsNovator);

      if (numberOfProducts > 1)
      {
        this.pageData.put("XSL", COMConstants.XSL_UPDATE_PRODUCT_LIST);
        //process Breadcrumb
        processBreadcrumbs(COMConstants.BCT_PRODUCT_LIST, COMConstants.BCA_KEYWORD_SEARCH_RESULTS);
      }
      else
      {
        this.pageData.put("XSL", COMConstants.XSL_LOAD_PRODUCT_DETAIL_ACTION);
        //process Breadcrumb
        processBreadcrumbs(COMConstants.BCT_PRODUCT_DETAIL, COMConstants.BCA_KEYWORD_SEARCH_RESULTS);
      }
    }
    else
    {
      this.pageData.put("XSL", COMConstants.XSL_LOAD_PRODUCT_DETAIL_ACTION);
      //process Breadcrumb
      processBreadcrumbs(COMConstants.BCT_PRODUCT_DETAIL, COMConstants.BCA_KEYWORD_SEARCH_RESULTS);
    }
  }




/*******************************************************************************************
 * retrieveProductBySourceCodeInfo() (COM_SOURCE_CODE_RECORD_LOOKUP)
 *******************************************************************************************
  * This method is used to call the
  *
  */
  private void retrieveProductBySourceCodeInfo() throws Exception
  {
    BasePageBuilder bpb = new BasePageBuilder();

    //Call getCSRViewed method in the DAO
    Document sourceCodeInfo = (Document) bpb.retrieveProductBySourceCodeInfo(this.con, this.sourceCode);

    //create an array.
    ArrayList aList = new ArrayList();

    //Initialize the array with the top and bottom nodes of the XML, and the node for which
    //the value must be retrieved. In this instance, we will retireve the value of
    //customer id
    aList.clear();
    aList.add(0,"PRODUCTS");
    aList.add(1,"PRODUCT");
    aList.add(2,"shipping_code");
    String shippingCode = DOMUtil.getNodeValue(sourceCodeInfo, aList);
    this.snhId = shippingCode;

    //Initialize the array with the top and bottom nodes of the XML, and the node for which
    //the value must be retrieved. In this instance, we will retireve the value of
    //customer id
    aList.clear();
    aList.add(0,"PRODUCTS");
    aList.add(1,"PRODUCT");
    aList.add(2,"pricing_code");
    String priceCode = DOMUtil.getNodeValue(sourceCodeInfo, aList);
    this.pricingCode = priceCode;

    //Initialize the array with the top and bottom nodes of the XML, and the node for which
    //the value must be retrieved. In this instance, we will retireve the value of
    //customer id
    aList.clear();
    aList.add(0,"PRODUCTS");
    aList.add(1,"PRODUCT");
    aList.add(2,"partner_id");
    String ptnrId = DOMUtil.getNodeValue(sourceCodeInfo, aList);
    this.partnerId = ptnrId;

  }


/*******************************************************************************************
 * retrieveFeeInfo() (COM_SNH_BY_ID)
 *******************************************************************************************
  * This method is used to call the
  *
  */
  private void retrieveFeeInfo() throws Exception
  {
    BasePageBuilder bpb = new BasePageBuilder();

    //Call getCSRViewed method in the DAO
    Document feeInfo = (Document) bpb.retrieveFeeInfo(this.con, this.snhId, this.deliveryDate);

    //create an array.
    ArrayList aList = new ArrayList();

    //Initialize the array with the top and bottom nodes of the XML, and the node for which
    //the value must be retrieved. In this instance, we will retireve the value of
    //customer id
    aList.clear();
    aList.add(0,"FEES");
    aList.add(1,"FEE");
    aList.add(2,"first_order_domestic");
    String domesticFee = DOMUtil.getNodeValue(feeInfo, aList);
    this.domesticSvcFee = domesticFee;

    //Initialize the array with the top and bottom nodes of the XML, and the node for which
    //the value must be retrieved. In this instance, we will retireve the value of
    //customer id
    aList.clear();
    aList.add(0,"FEES");
    aList.add(1,"FEE");
    aList.add(2,"first_order_international");
    String internationalFee = DOMUtil.getNodeValue(feeInfo, aList);
    this.internationalSvcFee = internationalFee;

  }



/*******************************************************************************************
 * retrieveDataFromNovator()
 *     //connect to novator, send the search, retrieve the response
    Here is an actual transmission sent to and received from Novator:

    Sent:
    -----
    <?xml version="1.0"?><keywordSearch><keywords><keyword value="roses"/></keywords></keywordSearch>

    Received:
    ---------
    <?xml version="1.0"?><!-- filename = keywordSearchResults.xml --><keywordSearch><products><product productId="FFFR"/><product productId="8012"/><product productId="F001"/>        <product productId="03BP"/><product productId="00BP"/></products></keywordSearch>CON




  ******************************************************************************************
  * Retrieve the novator ids
  *
  * @param none
  * @return
  * @throws none
  *
  * Input should look like:
  * -----------------------
  * <?xml version="1.0"?><keywordSearch><keywords><keyword value="victorian"/></keywords></keywordSearch>";
	*
  *
  * Output from Novator looks like:
  * -------------------------------
  * "<keywordSearch><products><product productId="3037"/><product productId="2943"/></products></keywordSearch>CON";
  *
  */
  private String retrieveDataFromNovator() throws Exception
  {

    Socket socket;
    OutputStreamWriter writer;
    BufferedReader reader;
    StringBuffer sbResponse = new StringBuffer();
    String sResponse = null;;

//1
    String sInputRequest;
    //format the input in a string that has xml representation
    sInputRequest = formatRequest();

    this.logger.debug( "KEYWORD SEARCH - Formatted Input = " + sInputRequest );

    //open the socket
    socket = new Socket(this.serverIP, Integer.parseInt(this.serverPort));
    writer = new OutputStreamWriter( socket.getOutputStream() );
    reader = new BufferedReader( new InputStreamReader( socket.getInputStream() ) );

    //send the message accross
    char[] chars = sInputRequest.toCharArray();
    writer.write( chars, 0, chars.length );
    writer.write(COMConstants.CONS_DELIMITER_CHAR);
    this.logger.debug( "KEYWORD SEARCH - Connection made. Sent To Novator: " + sInputRequest );
    writer.flush();

    //receive the message back
    int i;
    while ( (i=reader.read()) != -1 )
    {
      if ( ( (char)i )!= COMConstants.CONS_DELIMITER_CHAR )
        sbResponse.append( (char)i );
      else
        break;
    }

    this.logger.debug( "KEYWORD SEARCH - Received From Novator: " + sbResponse.toString() );

    //close the socket
    if ( writer != null)
      writer.close();
    if ( reader != null )
      reader.close();
    if ( socket != null )
      socket.close();

    sResponse = sbResponse.toString();
    sResponse = formatResponse(sResponse);

    this.logger.debug( "KEYWORD SEARCH - After formatting the response from Novator: " + sResponse);

    return sResponse;

  }


/*******************************************************************************************
 * retrieveKeywordProductsByIds()
 *     //call search util's getProductsByIds()
 *******************************************************************************************
  * Retrieve the
  *
  * @param none
  * @return
  * @throws none
  */
  private int retrieveKeywordProductsByIds(String productIds)
      throws Exception
  {

    ArrayList promotionList = new ArrayList();
    ProductFlagsVO flagsVO = new ProductFlagsVO();

    HashMap productMap = new HashMap();

    if (this.categoryIndex == null)
    {
      this.categoryIndex = "";
    }

    Document document = this.sUtil.getProductsForKeywordSearch(this.pageNumber, productIds,
                                    this.recipientZipCode, this.domesticFlag, this.sourceCode,
                                    this.pricePointId, this.recipientCountry, this.domesticSvcFee,
                                    this.internationalSvcFee, this.recipientState,
                                    this.pricingCode, this.partnerId, promotionList,
                                    this.rewardType, flagsVO, null, this.buyerName,
                                    this.origProductId, this.shipMethod, productMap, this.deliveryDate);

    //append addons
    document = this.sUtil.appendAddons(this.request, document);

    this.hashXML.put(COMConstants.HK_PRODUCT_DETAIL_LIST_XML, document);

    Set ks = productMap.keySet();
    Iterator iter = ks.iterator();
    String key;
    //Iterate thru the hashmap returned from the business object using the keyset
    while(iter.hasNext())
    {
      key = iter.next().toString();
      this.pageData.put(key, (String)productMap.get(key));
    }
    this.pageNumber = this.pageData.get("page_number")!=null?this.pageData.get("page_number").toString():this.pageNumber;
    this.totalPages = this.pageData.get("total_pages")!=null?this.pageData.get("total_pages").toString():this.totalPages;


    String xpath = "//PRODUCTS/PRODUCT";
    NodeList nl = DOMUtil.selectNodes(document,xpath);

    int numberOfProducts = nl.getLength();

    return numberOfProducts;

  }


 /************************************************************************************************************
  * formatRequest()
  * Input should look like:
  * -----------------------
  * <?xml version="1.0"?><keywordSearch><keywords><keyword value="victorian"/></keywords></keywordSearch>";
	*
  * Note: Initially, we tried to create an xml document, and then convert the xml document
  *       to String using the String Writer.  However, this did not work with Novator.
  *       String Writer generated the String in the format:
  *         	<keywordSearch>
  *         	  <keywords>
  *         	    <keyword value="victorian"/>
  *         	  </keywords>
  *         	</keywordSearch>
  *
  *       Novator could not parse thru this string.  Thus, it was decided to just create a string
  *       directly from the input instead of generating the xml document first.
  ************************************************************************************************************
  */
	private String formatRequest() throws Exception
	{
    StringBuffer keywordSearchString = new StringBuffer();

    keywordSearchString.append("<?xml version=\"1.0\"?>");


    //*****************************************************************************************
    //create the highest element
    //*****************************************************************************************
    keywordSearchString.append("<keywordSearch>");

    //*****************************************************************************************
    //create the markcode element
    //*****************************************************************************************
    // ** SFMB **
    if(this.companyId != null && this.companyId.equals("SFMB"))
    {
      keywordSearchString.append("<markcode>");
      keywordSearchString.append(this.keywordSFMB);
      keywordSearchString.append("</markcode>");
    }
    // ** Gift Site **
    else if(companyId != null && companyId.equals("GIFT"))
    {
      keywordSearchString.append("<markcode>");
      keywordSearchString.append(this.keywordGIFT);
      keywordSearchString.append("</markcode>");
    }
    // ** High End Site **
    else if(companyId != null && companyId.equals("HIGH"))
    {
      keywordSearchString.append("<markcode>");
      keywordSearchString.append(this.keywordHIGH);
      keywordSearchString.append("</markcode>");
    }

    //*****************************************************************************************
    //create the keywords element
    //*****************************************************************************************
		keywordSearchString.append("<keywords>");

		StringTokenizer s = new StringTokenizer(this.keywords);

		int count = 0;
		while(s.hasMoreTokens())
		{
			keywordSearchString.append("<keyword value=\"");
			String word = s.nextToken();
			keywordSearchString.append(word);
			keywordSearchString.append("\"/>");
		}
		keywordSearchString.append("</keywords>");

    //*****************************************************************************************
    //end the highest element
    //*****************************************************************************************
		keywordSearchString.append("</keywordSearch>");

    return keywordSearchString.toString();

  }



/*******************************************************************************************
 * formatResponse()
 *     //format the input keywords in an xml format
 *******************************************************************************************
 * the purpose of this method is to ensure that the string returned back from Novator is in
 * the same format as our other search string (that is, the item search string).  Once we
 * have removed/changed case of the particulars in the string, we will use the xml parser,
 * which will be generic for both the searches.
 *
 * @param none
 * @return
 * @throws none
 */
	private String formatResponse(String inputString)
	{
    BasePageBuilder bpb = new BasePageBuilder();

    inputString = bpb.replaceAll(inputString, "<keywordSearch>",  "");
    inputString = bpb.replaceAll(inputString, "</keywordSearch>", "");
    inputString = bpb.replaceAll(inputString, "<products>",       "<PRODUCTS>");
    inputString = bpb.replaceAll(inputString, "</products>",      "</PRODUCTS>");
    inputString = bpb.replaceAll(inputString, "<product",         "<PRODUCT");
    inputString = bpb.replaceAll(inputString, "productId",        "productid");


    return inputString;
  }


/*******************************************************************************************
 * extractNovatorProductIds()
 *     //go thru the response, and get the product ids
 *******************************************************************************************
  * Retrieve the
  *
  * @param none
  * @return
  * @throws none
  */
  private String extractNovatorProductIds(String searchResults) throws Exception
  {
    StringBuffer sbProductIdsNovator = new StringBuffer();


		if(searchResults.indexOf("CON") != -1             &&
       searchResults.indexOf("PRODUCTS") != -1        &&
       searchResults.indexOf("PRODUCT") != -1         )
    {
			searchResults = searchResults.substring(0, searchResults.length() - 3);

			// Parse product IDs
      Document productIdsNovatorXML = (Document) DOMUtil.getDefaultDocument();

      //generate the top node
      Element recipientStateElement = productIdsNovatorXML.createElement("PRODUCTRESULTS");
      //and append it
      productIdsNovatorXML.appendChild(recipientStateElement);

      productIdsNovatorXML.getDocumentElement().appendChild(productIdsNovatorXML.importNode(DOMUtil.getDocument(searchResults).getLastChild(), true));

      String xpath = "//PRODUCTS/PRODUCT";
      NodeList nl = DOMUtil.selectNodes(productIdsNovatorXML,xpath);

			if(nl.getLength() > 0)
			{
				// We have products
				for (int i = 0; i < nl.getLength() && i < 255; i++)
				{
					Element node = (Element)nl.item(i);
					sbProductIdsNovator.append("'");
					sbProductIdsNovator.append(node.getAttribute("productid"));
					sbProductIdsNovator.append("'");
					sbProductIdsNovator.append(",");
				}

				// Remove the last comma
				if(sbProductIdsNovator.length() > 0)
				{
					sbProductIdsNovator.deleteCharAt(sbProductIdsNovator.length()-1);
				}
			}
			else
			{
				// We do not have products
				searchResults = "";
				sbProductIdsNovator = new StringBuffer("");
			}
		}
		else
		{
			searchResults = "";
			sbProductIdsNovator = new StringBuffer("");
		}

    return sbProductIdsNovator.toString();

  }


  /*
   * Returns all CSR's currently viewing this order detail id.
   * @return Document containing all CSR's currently viewing this order
   * @throws java.lang.Exception
   */
  private Document retrieveCSRViewing()
      throws Exception {
    //Instantiate ViewDAO
    ViewDAO viewDAO = new ViewDAO(this.con);

    //Document that will contain the output from the stored proce
    Document csrViewingInfo = DOMUtil.getDocument();

    //Call getCSRViewing method in the DAO
    csrViewingInfo = viewDAO.getCSRViewing(this.sessionId, this.csrId, COMConstants.CONS_ENTITY_TYPE_ORDER_DETAILS, this.orderDetailId);

    return csrViewingInfo;
  }
}
