package com.ftd.mo.bo;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.dao.AcctTransDAO;
import com.ftd.customerordermanagement.dao.LockDAO;
import com.ftd.customerordermanagement.dao.LookupDAO;
import com.ftd.customerordermanagement.dao.OrderDAO;
import com.ftd.mo.dao.UpdateOrderDAO;
import com.ftd.customerordermanagement.util.BasePageBuilder;
import com.ftd.mo.util.SearchUtil;
import com.ftd.mo.validation.ValidationConfiguration;
import com.ftd.mo.validation.ValidationLevels;
import com.ftd.mo.validation.ValidationResult;
import com.ftd.mo.validation.ValidationRules;
import com.ftd.mo.validation.Validator;
import com.ftd.mo.validation.impl.DeliveryDateValidatorFactory;
import com.ftd.mo.validation.impl.FloristDeliveredValidator;
import com.ftd.mo.validation.impl.VendorDeliveredValidator;
import com.ftd.customerordermanagement.vo.AcctTransVO;
import com.ftd.customerordermanagement.vo.AddOnVO;
import com.ftd.customerordermanagement.vo.CommentsVO;
import com.ftd.customerordermanagement.vo.CustomerPhoneVO;
import com.ftd.customerordermanagement.vo.CustomerVO;
import com.ftd.mo.vo.DeliveryInfoVO;
import com.ftd.customerordermanagement.vo.ErrorMessageVO;
import com.ftd.customerordermanagement.vo.MembershipVO;
import com.ftd.customerordermanagement.vo.OrderBillVO;
import com.ftd.customerordermanagement.vo.OrderDetailVO;
import com.ftd.customerordermanagement.vo.OrderVO;

import com.ftd.ftdutilities.FieldUtils;

import com.ftd.messaging.constants.MessagingConstants;
import com.ftd.messaging.dao.MessageDAO;
import com.ftd.messaging.vo.FloristStatusVO;
import com.ftd.messaging.vo.MessageVO;
import com.ftd.messaging.vo.MessageOrderStatusVO;

import com.ftd.osp.framework.dispatcher.Dispatcher;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.XMLFormat;
import com.ftd.osp.utilities.order.vo.OrderContactInfoVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.osp.utilities.xml.DOMUtil;

import com.ftd.security.SecurityManager;
import com.ftd.security.cache.vo.UserInfo;

import java.util.Date;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.transaction.UserTransaction;


import org.apache.commons.lang.StringUtils;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.io.PrintWriter;
import java.io.StringWriter;

import java.math.BigDecimal;

import java.net.URL;
import java.net.URLEncoder;

import java.sql.Connection;
import java.sql.DriverManager;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;


/*
original order info:
  VIEW_ORIGINAL_ORDER_INFO - retrieves product, product amount, recipient, and customer info
  VIEW_ORDER_DETAIL_UPDATE - the OUT_ORIG_ADD_ONS cursor within this retrieves all the original addon info

update tables:
  VIEW_DELIVERY_INFO - retrieves some product info, recipient, and customer info.  Does NOT retrieve the product details and amounts
  VIEW_PRODUCT_DETAIL - only retrieves the product detail info
  VIEW_UPDATE_ORDER_INFO - retrieves product, recipient, and customer info. Does NOT retrieve product amounts.
 *
 */
public class UpdateDeliveryConfirmationBO {
  private static Logger logger = new Logger("com.ftd.mo.bo.UpdateDeliveryConfirmationBO");

  //constants
  private static final String BALLOON = "BALLOON";
  private static final String BEAR = "BEAR";
  private static final String CARD = "CARD";
  private static final String CHOCOLATE = "CHOCOLATE";
  private static final String FUNERAL = "FUNERAL";
  private static final String NEWLINE_CHAR_CODE = "\n";
  private static final String NEWLINE_CHAR_HTML = "<BR/>";

  //page actions
  private static final String UPDATE_FLORIST_ACTION = "update_florist";

  //messages displayed in popups
  private static final String SAME_FLORIST_MSG = "Selected florist is currently assigned to the order.  Do you wish to keep the order with this florist?";
  private static final String NEW_ORDER_MSG = "A new Mercury Order needs to be sent to the new ";
  private static final String TYPE_VENDOR_MSG = "vendor";
  private static final String TYPE_FLORIST_MSG = "florist";
  private static final String CONTINUE_MSG = "Do you wish to continue?";
  private static final String SEND_CAN_MSG = "A CAN message will be sent to the ";
  private static final String CHANGE_MERC_VALUE_MSG = "Do you wish to change the Mercury value on the order to the florist from ";
  private static final String ASK_SENT_MSG = "An ASK message will be sent to the florist.";
  private static final String COMP_MSG = "A complimentary FTD will be sent to the ";
  private static final String NO_CHANGES_MSG = "No changes have been made to this order.";
  private static final String CUSTOMER_REQ_CHANGE_MSG = "The customer has requested the following changes be made to their order.";
  private static final String PLEASE_CANCEL_MESSAGE = "Please cancel the order per the customer's request.";
  private static final String MANUAL_MERCURY_MESSAGE = "The order is assigned to a non-mercury florist.  Please contact the florist with changes made to this order.";

  /*******************************************************************************************
   * Class level variables
   *******************************************************************************************/
  private HttpServletRequest request = null;
  private Connection con = null;
  private ActionMapping mapping = null;

  //hold flags
  boolean efosHold = false;
  boolean gnaddHold = false;

  //request parameters required to be sent back to XSL all the time.
  private String customerId = null;
  private String externalOrderNumber = null;
  private String masterOrderNumber = null;
  private String orderDetailId = null;
  private String orderGuid = null;

  //other variables needed for this class
  private String action = "";
  private String csrId = null;
  private HashMap hashXML = new HashMap();
  private HashMap pageData = new HashMap();
  private String sessionId = null;
  private String context = null;
  private SearchUtil sUtil;

  //delivery info vo
  private DeliveryInfoVO originalDeliveryInfoVO = new DeliveryInfoVO();
  private DeliveryInfoVO tempDeliveryInfoVO = new DeliveryInfoVO();

  //order detail vo
  private OrderBillVO originalOrderBillVO = new OrderBillVO();
  private OrderBillVO tempOrderBillVO = new OrderBillVO();

  //AddOn HashMaps
  private HashMap originalOrderAddOnVO = new HashMap(); //key = addon-code (eg. RC73)
  private HashMap tempOrderAddOnVO = new HashMap(); //key = addon-code (eg. RC73)

  //additionial bills
  private List tempAddBills = new ArrayList();
  private List origAddBills = new ArrayList();

  //flag to indicate that the florist has changed
  private boolean floristChanged = false;

  /*******************************************************************************************
   * Constructor 1
   *******************************************************************************************/
  public UpdateDeliveryConfirmationBO()
  {
  }


  /*******************************************************************************************
   * Constructor 2
   *******************************************************************************************/
  public UpdateDeliveryConfirmationBO(HttpServletRequest request, Connection con, ActionMapping mapping)
  {
    this.request = request;
    this.con = con;
    this.mapping = mapping;
  }


  /*******************************************************************************************
    * processRequest()
    ******************************************************************************************
    *
    * @param  String - action
    * @return HashMap - contains 0 - many XML documents of data, 1 HashMap each for page
    *                   details and search criteria
    * @throws
    */
  public HashMap processRequest() throws Exception
  {
    //HashMap that will be returned to the calling class
    HashMap returnHash = new HashMap();

    //Get info from the request object
    getRequestInfo(this.request);

    //Get config file info
    getConfigInfo();

    //instantiate the search util
    this.sUtil = new SearchUtil(this.con);

    //process the action
    processAction(con);

    //populate the remainder of the fields on the page data
    populatePageData();

    //populate the sub header info
    populateSubHeaderInfo();

    //At this point, we should have three HashMaps.
    // HashMap1 = hashXML         - hashmap that contains zero to many Documents
    // HashMap2 = pageData        - hashmap that contains String data at page level
    //
    //Combine all of the above HashMaps into one HashMap, called returnHash

    //retrieve all the Documents from hashXML and put them in returnHash
    //retrieve the keyset
    Set ks1 = hashXML.keySet();
    Iterator iter = ks1.iterator();
    String key = null;
    Document doc = null;

    //Iterate thru the keyset
    while (iter.hasNext())
    {
      //get the key
      key = iter.next().toString();

      //retrieve the XML document
      doc = (Document) hashXML.get(key);

      //put the XML object in the returnHash
      returnHash.put(key, (Document) doc);
    }

    //append pageData to returnHash
    returnHash.put("pageData", (HashMap) this.pageData);

    return returnHash;
  }



  /*******************************************************************************************
    * getRequestInfo(HttpServletRequest request)
    ******************************************************************************************
    * Retrieve the info from the request object
    *
    * @param  HttpServletRequest - request
    * @return none
    * @throws none
    */
  private void getRequestInfo(HttpServletRequest request) throws Exception
  {

    /****************************************************************************************
     *  Retrieve order/search specific parameters
     ***************************************************************************************/

    //retrieve the action
    if (request.getParameter(COMConstants.ACTION) != null)
    {
      this.action = request.getParameter(COMConstants.ACTION);
      logger.info("Current Action = " + action);
    }

    //retrieve the customer id
    if (request.getParameter(COMConstants.CUSTOMER_ID) != null)
    {
      this.customerId = request.getParameter(COMConstants.CUSTOMER_ID);
    }

    //retrieve the item order number
    if (request.getParameter(COMConstants.EXTERNAL_ORDER_NUMBER) != null)
    {
      this.externalOrderNumber = request.getParameter(COMConstants.EXTERNAL_ORDER_NUMBER);
    }

    //retrieve the master order number
    if (request.getParameter(COMConstants.MASTER_ORDER_NUMBER) != null)
    {
      this.masterOrderNumber = request.getParameter(COMConstants.MASTER_ORDER_NUMBER);
    }

    //retrieve the order detail id
    if (request.getParameter(COMConstants.ORDER_DETAIL_ID) != null)
    {
      this.orderDetailId = request.getParameter(COMConstants.ORDER_DETAIL_ID);
      logger.info("Processing order detail " + orderDetailId);
    }

    //retrieve the order guid
    if (request.getParameter(COMConstants.ORDER_GUID) != null)
    {
      this.orderGuid = request.getParameter(COMConstants.ORDER_GUID);
    }

    /****************************************************************************************
     *  Retrieve Security Info
     ***************************************************************************************/
    this.sessionId = request.getParameter(COMConstants.SEC_TOKEN);
    this.context = request.getParameter(COMConstants.CONTEXT);
  }

  /*******************************************************************************************
    * getConfigInfo()
    ******************************************************************************************
    * Retrieve the info from the configuration file
    *
    * @param none
    * @return
    * @throws none
    */
  private void getConfigInfo() throws Exception
  {
    ConfigurationUtil cu = null;
    cu = ConfigurationUtil.getInstance();

    //get csr Id
    String security = (String) cu.getProperty(COMConstants.PROPERTY_FILE, COMConstants.SECURITY_IS_ON);
    boolean securityIsOn = security.equalsIgnoreCase("true") ? true : false;

    //delete this before going to production.  the following should only check on securityIsOn
    if (securityIsOn || (this.sessionId != null))
    {
      SecurityManager sm = SecurityManager.getInstance();
      UserInfo ui = sm.getUserInfo(this.sessionId);
      this.csrId = ui.getUserID();
    }

  }

  /*******************************************************************************************
    * populatePageData()
    ******************************************************************************************
    * Populate the page data with the remainder of the fields
    *
    * @param none
    * @return
    * @throws none
    */
  private void populatePageData()
  {
    this.pageData.put(COMConstants.PD_MO_CUSTOMER_ID, this.customerId);
    this.pageData.put(COMConstants.PD_MO_EXTERNAL_ORDER_NUMBER, this.externalOrderNumber);
    this.pageData.put(COMConstants.PD_MO_MASTER_ORDER_NUMBER, this.masterOrderNumber);
    this.pageData.put(COMConstants.PD_MO_ORDER_DETAIL_ID, this.orderDetailId);
    this.pageData.put(COMConstants.PD_MO_ORDER_GUID, this.orderGuid);
  }

  /*******************************************************************************************
    * populateSubHeaderInfo()
    ******************************************************************************************
    * Populate the page data with the remainder of the fields
    *
    * @param none
    * @return
    * @throws none
    */
  private void populateSubHeaderInfo() throws Exception
  {
    BasePageBuilder bpb = new BasePageBuilder();

    //xml document for sub header
    Document subHeaderXML = DOMUtil.getDocument();

    //retrieve the sub header info
    subHeaderXML = bpb.retrieveSubHeaderData(this.request);

    //save the original product xml
    this.hashXML.put(COMConstants.HK_SUB_HEADER, subHeaderXML);
  }

  /*******************************************************************************************
    * processAction()
    ******************************************************************************************
    * process the action type of "load".  This method implements the initial page load-up.
    *
    * @param  none
    * @return none
    * @throws none
    */
  private void processAction(Connection conn) throws Exception
  {
    //get the originial order and save in class variable
    processOriginalOrder();

    //get new order and save in class variable
    processOrginalAddOnsAndNewOrder();

    //get the add bills
    buildAdditionialBills(conn);

    //Check if order is on EFOS or GNADD hold
    OrderDAO orderDAO = new OrderDAO(con);
    String holdReason = orderDAO.getHoldReason(originalDeliveryInfoVO.getOrderDetailId());
    if(holdReason != null && holdReason.equals("GNADD"))
    {
      gnaddHold = true;
    }
    else if(holdReason != null && holdReason.equals("EFOS"))
    {
      efosHold = true;
    }


    //determine what action to execute
    if (action.equals("validate"))
    {
      processValidate(conn);
    }
    else if (action.equals("commit_changes"))
    {
      processCommitChanges();
    }
    else if (action.equals("update_florist"))
    {
      String floristId = request.getParameter("florist_id");
      processUpdateFlorist(floristId);

      //if order amounts changes go to billing review
      if (orderAmountsChanged())
      {
        gotoBillingReview(floristId);
      }
      else
      {
        processSendMessage(con, floristChanged);
      }
    }
    else if (action.equals("create_message"))
    {
      String messageType = request.getParameter(COMConstants.MSG_TYPE);
      processCreateMessage(conn, messageType);
    }
    else if (action.equals("send_message"))
    {
      String messageType = request.getParameter(COMConstants.MSG_TYPE);

      //chec if florist changed
      processSendMessage(conn, false);
    }
    else if (action.equals("roll_back"))
    {
      processRollBack();
    }
    else
    {
      throw new Exception("Unknown action:" + action);
    }
  }

  /**
   * validate action
     *
     * Validate that the info on the order is good.
   */
  private void processValidate(Connection conn) throws Exception
  {
    String enteredFloristId = request.getParameter(COMConstants.PAGE_FLORIST_ID);
    boolean floristIdEntered = false;

    if ((enteredFloristId != null) && (enteredFloristId.length() > 0))
    {
      floristIdEntered = true;
    }

    //check if live mercury
    boolean liveMercury = false;
    String vendorFloristFlag = originalDeliveryInfoVO.getVendorFlag().equals("Y") ? "Vendor" : "Mercury";
    UpdateOrderDAO updateOrderDAO = new UpdateOrderDAO(conn);

    if (updateOrderDAO.hasLiveMercury(tempDeliveryInfoVO.getOrderDetailId(), vendorFloristFlag, false))
    {
      liveMercury = true;
    }

    //check if the request indicates that validation should not be done.
    String skipValidateFlag = request.getParameter("skip_validate");

    List validationErrors = null;

    Document popupXML = null;

    //used for error processing
    ErrorMessageVO errorVO = new ErrorMessageVO();
    List errorList = new ArrayList();

    //daos
    MessageDAO messageDAO = new MessageDAO(conn);

    //only include the alt contact changes on florist delivered orders
    boolean checkForAltChanges = true;

    if (tempDeliveryInfoVO.getVendorFlag().equalsIgnoreCase("Y"))
    {
      checkForAltChanges = false;
    }

    //check if florist changed
    floristChanged = false;

    if (valueChanged(tempDeliveryInfoVO.getFloristId(), originalDeliveryInfoVO.getFloristId()))
    {
      floristChanged = true;
    }

    //check if vendor-florist flag changed
    boolean vendorChanged = false;

    if (valueChanged(tempDeliveryInfoVO.getVendorFlag(), originalDeliveryInfoVO.getVendorFlag()))
    {
      logger.debug("Vendor flag changed.");
      vendorChanged = true;
    }

    //first check if anythign changed on the order, if nothing change exit
    String changeComments = checkForChanges(false, NEWLINE_CHAR_HTML, checkForAltChanges, false);

     if (!orderAmountsChanged() && !vendorChanged && ((changeComments == null) || (changeComments.length() == 0)))
     {
      logger.debug("Comments were not updated.");

      String parms = "?action=search" + "&" + "recipient_flag=y" + "&" + "in_order_number=" + externalOrderNumber;

      boolean isClassChange = isClassificationChange(tempDeliveryInfoVO, originalDeliveryInfoVO);
      //Change comments could be null if no comments are needed, but
      //it's possible that something else changed on the order.
      if ( isClassChange || isAltContactChange() || isMembershipInfoChange()) {
        logger.debug("Classification or alt contact or membership info change.");

        //check to see if a classification change occured
        //if it did then we need to update the accounting transactions table
        if (isClassChange)
        {
            applyA40Refund(conn);
        }

        //nothing changed that needs a new message to be sent to vendor/florist
        //commit changes
        processCommitChanges();

        //go to recipient order page
        ActionForward actionForward = mapping.findForward("CustomerAccount");
        String path = actionForward.getPath() + parms;
        actionForward = new ActionForward(path, false);
        pageData.put("XSL", "forward");
        pageData.put("forward", actionForward);

        //flag error...this won't actually be displayed to user though
        errorVO.setMessage(NO_CHANGES_MSG);
        errorList.add(errorVO);
      }
      else
      {
        logger.debug("nothing changed");

        //nothing changed.  Tell user and goto recipient order
        errorVO.setMessage(NO_CHANGES_MSG);
        errorVO.setDisplayYes(false);
        errorVO.setYesAction("customerOrderSearch.do" + parms);
        errorVO.setDisplayNo(false);
        errorVO.setNoAction("");
        errorVO.setDisplayOk(true);
        errorList.add(errorVO);
        displayPopUpXML(errorList);
      }
    }
    //if florist order
    else if (tempDeliveryInfoVO.getVendorFlag().equals("N")) {
      logger.debug("florist order");

      //if a new florist code was entered
      if (floristIdEntered) {
        logger.debug("florist id was entered");
        errorList = validateFloristIdEntered(errorVO, errorList, enteredFloristId);
      } else //else, florist id not entered
       {
        //if order is on gnadd or efos hold then validation should not be done.
        //this is because the order does not have a vendor/florist assigned to it yet.
        if(efosHold || gnaddHold)
        {
            logger.debug("Validation skipped because order on EFOS or GNADD hold.");
        }
        else
        {
            //validate florist, unless the skip validate flag was passed in
            if ((skipValidateFlag != null) && skipValidateFlag.equals("Y")) {
              logger.debug("validation skipped becasuse skip flag was passed in from request");
            } else {
              validationErrors = validateDeliveryDateAndInfo(tempDeliveryInfoVO, floristChanged, liveMercury);
            }

            //if validation error occured
            if ((validationErrors != null) && (validationErrors.size() > 0)) {
              displayPopUpXML(validationErrors);
            } //end if validation errors
        }
      }
    } //else, vendor order
    else {

        //if order is on gnadd or efos hold then validation should not be done.
        //this is because the order does not have a vendor/florist assigned to it yet.
        if(efosHold || gnaddHold)
        {
            logger.debug("Validation skipped because order on EFOS or GNADD hold.");
        }
        else
        {
              //validate vendor
              if ((skipValidateFlag != null) && skipValidateFlag.equals("Y")) {
                logger.debug("validation skipped due to value skip flag being set in request");
              } else {
                validationErrors = validateDeliveryDateAndInfo(tempDeliveryInfoVO, floristChanged, liveMercury);
              }

              //if validation error occured
              if ((validationErrors != null) && (validationErrors.size() > 0)) {
                displayPopUpXML(validationErrors);
              } //end if validation errors
        }

    }

    //end if
    //This code is used for testing purposes..and way to override validation if ever needed.
    ConfigurationUtil config = ConfigurationUtil.getInstance();
    String validationOverride = (String) config.getProperty(COMConstants.PROPERTY_FILE, "VALIDATION_OVERRIDE");

    if ((validationOverride != null) && validationOverride.equals("Y")) {
      //clearing these out will cause validation to be ignored
      validationErrors = null;
      errorList = new ArrayList();

      logger.warn("VALIDATION IS BEING OVERIDDEN IN UPDATEDELIVERYCONFIRMATION.");
    }

    //if no validation errors occured
    if (((validationErrors == null) || (validationErrors.size() == 0)) && (errorList.size() == 0)) {
      //only forward on if the florist did not change, the update
      //florist method will forward to next action if there are no errors
      //if(!floristIdEntered)
      //{
      //if order amounts changed
      if (orderAmountsChanged()) {
        gotoBillingReview(enteredFloristId);
      } else {
        logger.debug("no price change, sending message");

        //process send message
        processSendMessage(conn, floristChanged);
      }

      //}
    } //end if
  }

  private void gotoBillingReview(String floristId) {
    String classChangeFlag = isClassificationChange(tempDeliveryInfoVO, originalDeliveryInfoVO) ? "Y" : "N";
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    //go to view bill
    ActionForward actionForward = mapping.findForward("billingReview");


    String path =	actionForward.getPath() + "?" +
    								COMConstants.MSG_TYPE + "=" + request.getParameter(COMConstants.MSG_TYPE) + "&" +
    								COMConstants.PAGE_FLORIST_ID + "=" + floristId + "&" +
    								COMConstants.FLORIST_TYPE + "=" + request.getParameter(COMConstants.FLORIST_TYPE) + "&" +
    								COMConstants.START_ORIGIN + "=" + request.getParameter(COMConstants.START_ORIGIN) + "&" +
    								COMConstants.CUSTOMER_ID + "=" + customerId + "&" +
    								COMConstants.EXTERNAL_ORDER_NUMBER + "=" + externalOrderNumber + "&" +
    								COMConstants.MASTER_ORDER_NUMBER + "=" + masterOrderNumber + "&" +
    								COMConstants.ORDER_DETAIL_ID + "=" + orderDetailId + "&" +
    								COMConstants.ORDER_GUID + "=" + orderGuid + "&" +
    								COMConstants.SOURCE_CODE + "=" + tempDeliveryInfoVO.getSourceCode() + "&" +
    								COMConstants.SHIP_METHOD + "=" + tempDeliveryInfoVO.getShipMethod() + "&" +
    								COMConstants.DELIVERY_DATE + "=" + sdf.format(tempDeliveryInfoVO.getDeliveryDate().getTime()) + "&" +
    								COMConstants.ORIG_SOURCE_CODE + "=" + originalDeliveryInfoVO.getSourceCode() + "&" +
    								COMConstants.BUYER_FULL_NAME + "=" + request.getParameter(COMConstants.BUYER_FULL_NAME) + "&" +
    								COMConstants.CATEGORY_INDEX + "=" + request.getParameter(COMConstants.CATEGORY_INDEX) + "&" +
    								COMConstants.COMPANY_ID + "=" + request.getParameter(COMConstants.COMPANY_ID) + "&" +
    								COMConstants.OCCASION_TEXT + "=" + request.getParameter(COMConstants.OCCASION_TEXT) + "&" +
    								COMConstants.ORIG_PRODUCT_AMOUNT + "=" + request.getParameter(COMConstants.ORIG_PRODUCT_AMOUNT) + "&" +
    								COMConstants.ORIG_PRODUCT_ID + "=" + (request.getParameter(COMConstants.ORIG_PRODUCT_ID)).toUpperCase() + "&" +
    								COMConstants.PRODUCT_ID + "=" + tempDeliveryInfoVO.getProductId().toUpperCase() + "&" +
    								COMConstants.ORIGIN_ID + "=" + request.getParameter(COMConstants.ORIGIN_ID) + "&" +
    								COMConstants.PAGE_NUMBER + "=" + request.getParameter(COMConstants.PAGE_NUMBER) + "&" +
    								COMConstants.PRICE_POINT_ID + "=" + request.getParameter(COMConstants.PRICE_POINT_ID) + "&" +
    								COMConstants.CLASS_CHANGE + "=" + classChangeFlag + "&" +
    								COMConstants.ORIG_RECIPIENT_STATE + "=" + originalDeliveryInfoVO.getRecipientState() + "&" +
    								COMConstants.RECIPIENT_STATE + "=" + tempDeliveryInfoVO.getRecipientState();
    actionForward = new ActionForward(path, false);

    logger.debug("Going to billing review. Path=" + path);

    //put forward in map
    pageData.put("XSL", "forward");
    pageData.put("forward", actionForward);
  }

  /*
   * Performs validation when a florist id was entered
   */
  private List validateFloristIdEntered(ErrorMessageVO errorVO, List errorList, String enteredFloristId)
    throws Exception {
    MessageDAO messageDAO = new MessageDAO(con);

    //if entered florist id is same as existing florist id
    if ((originalDeliveryInfoVO.getFloristId() != null) && originalDeliveryInfoVO.getFloristId().equals(enteredFloristId)) {
      logger.debug("entered florist id is same as existing id");

      //display SAME_FLORIST message
      //if yes clicked, submit to update_florist
      //if no clicked, do nothing
      errorVO.setMessage(SAME_FLORIST_MSG);
      errorVO.setDisplayYes(true);
      errorVO.setYesAction("updateDeliveryConfirmation.do?action=update_florist");
      errorVO.setDisplayNo(true);
      errorVO.setNoAction("");
      errorList.add(errorVO);
      displayPopUpXML(errorList);
    }
    //else, florist different
    else {
      logger.debug("entered florist id is different then existing id");

      //if code entered automatically
      String floristType = request.getParameter(COMConstants.FLORIST_TYPE);

      //default to auto
      if(floristType == null || floristType.length() == 0)
      {
          floristType = "auto";
      }

      if ((floristType != null) && floristType.equals("auto")) {
        logger.debug("entered florist = auto");

        //call processUpdateFlorist
        processUpdateFlorist(enteredFloristId);
      } else if ((floristType != null) && floristType.equals("man")) //else, code entered manually
       {
        logger.debug("entered florist = man");

        FloristStatusVO floristStatusVO = messageDAO.getFloristStatus(enteredFloristId);

        //if florist id is invalid
        if (floristStatusVO == null) {
          //display INVALID_FLORIST error
          errorVO.setMessage(COMConstants.INVALID_FLORIST_NUMBER);
          errorVO.setDisplayYes(false);
          errorVO.setYesAction("");
          errorVO.setDisplayNo(false);
          errorVO.setNoAction("");
          errorVO.setDisplayOk(true);
          errorList.add(errorVO);
          displayPopUpXML(errorList);
        }
        //if florist id is for a vendor
        else if (floristStatusVO.isVendor()) {
          //display FLORIST_IS_VENDOR error
          errorVO.setMessage(COMConstants.FLORIST_CODE_FLAGGED_AS_VENDOR);
          errorVO.setDisplayYes(false);
          errorVO.setYesAction("");
          errorVO.setDisplayNo(false);
          errorVO.setNoAction("");
          errorVO.setDisplayOk(true);
          errorList.add(errorVO);
          displayPopUpXML(errorList);
        }
        //if florist opt'd out
        else if (floristStatusVO.isOptout()) {
          //display OPT_OUT_ERROR
          errorVO.setMessage(COMConstants.FLORIST_IN_OPT_OUT_STATUS);
          errorVO.setDisplayYes(false);
          errorVO.setYesAction("");
          errorVO.setDisplayNo(false);
          errorVO.setNoAction("");
          errorVO.setDisplayOk(true);
          errorList.add(errorVO);
          displayPopUpXML(errorList);
        } else if (floristStatusVO.getStatus().equalsIgnoreCase("Inactive")) {
          //display INACTIVE
          errorVO.setMessage(COMConstants.FLORIST_UNAVAILABLE);
          errorVO.setDisplayYes(false);
          errorVO.setYesAction("");
          errorVO.setDisplayNo(false);
          errorVO.setNoAction("");
          errorVO.setDisplayOk(true);
          errorList.add(errorVO);
          displayPopUpXML(errorList);
        }
        //if florist suspended
        else if (floristStatusVO.getStatus().equalsIgnoreCase("Suspend")) {
          //displayed SUSPENDED_ERROR
          processUpdateFlorist(enteredFloristId);
        }
        //else, the florist is valid
        else {
          //call process UpdateFlorist
          processUpdateFlorist(enteredFloristId);
        }
      } else {
        throw new Exception("Invalid florist_type:" + floristType);
      } //end how florist id entered
    } //end, check if same florist id

    return errorList;
  }

  /*
   * Go through the error messages and display the appropiate
   * popup to the user.
   *
   * The list of error messages is looped through twice.  Once
   * to figure what what action should take place, and then again
   * to put all the errors into a list.
   *
   */
  /*private void processValidationErrors(List errorMessages)
  throws Exception
  {
      boolean actionFound = false;

      //default action is to just display the error with an OK
      String yesAction = "";
      String noAction = "";
      boolean displayYes = false;
      boolean displayNo = false;
      boolean displayOk = true;

      //certain messages require special processing, this loops checks for those error messages
      Iterator iter = errorMessages.iterator();
      while(!actionFound && iter.hasNext())
      {
          String msg = (String)iter.next();

          //TODO: Put the message constant in here

          //If codify florist for sunday delivery,but not product
          //Yes = bring to product category
          //No = Do nothing
          if(msg.equals(COMConstants.FLORIST_CANNOT_DELIVER_PRODUCT))
          {

              yesAction = "loadProductCategory.do" + getProductCategoryParms();
              noAction = "";
              displayYes = true;
              displayNo = true;
              displayOk = false;

              actionFound = true;
          }
          //If codified florist for Sunday delivery not found
          //YES = Bring back to page and show florist search box
          //NO = do nothing
          else if(msg.equals(COMConstants.SUNDAY_DELIVERY_NOT_AVAILABLE_IN_ZIP))
          {
              yesAction = "florist_lookup";
              noAction = "";
              displayYes = true;
              displayNo = true;
              displayOk = false;
              //noAction = "updateDeliveryConfirmation.do?action=validate&skip_validate=Y";
          }

      }//end while loop looking for special errors

     //put all the errors in a list
     ArrayList errorList = new ArrayList();
     iter = errorMessages.iterator();
     while(iter.hasNext())
     {
         //display florist validation message
         ErrorMessageVO errorVO = new ErrorMessageVO();
         errorVO.setMessage((String)iter.next());
         errorVO.setDisplayYes(displayYes);
         errorVO.setYesAction(yesAction);
         errorVO.setDisplayNo(displayNo);
         errorVO.setNoAction(noAction);
         errorVO.setDisplayOk(displayOk);
         errorList.add(errorVO);

     }//end while
     displayPopUpXML(errorList);


  }*/

  /*
   * Returns list of parms needed for product category page
   */
  private String getProductCategoryParms()
  {
    String parms =
    		"?buyer_full_name=" + originalDeliveryInfoVO.getCustomerFirstName() + " " + originalDeliveryInfoVO.getCustomerLastName() +
    		"&category_index=" + originalDeliveryInfoVO.getOccasionCategoryIndex() +
				"&company_id=" + originalDeliveryInfoVO.getCompanyId() +
				"&customer_id=" + customerId +
				"&delivery_date=" + tempDeliveryInfoVO.getDeliveryDate().getTime() +
				"&delivery_date_range_end=" + tempDeliveryInfoVO.getDeliveryDateRangeEnd().getTime() +
				"&external_order_number=" + externalOrderNumber +
				"&master_order_number=" + masterOrderNumber +
				"&occasion=" + tempDeliveryInfoVO.getOccasion() +
				"&occassion_text=" + originalDeliveryInfoVO.getOccasionDescription() +
				"&order_detail_id=" + tempDeliveryInfoVO.getOrderDetailId() +
				"&order_guid=" + tempDeliveryInfoVO.getOrderGuid() +
				"&origin_id=" + originalDeliveryInfoVO.getOriginId() +
				"&page_number=" + "1" +
				"&price_point_id=" + tempDeliveryInfoVO.getSizeIndicator() +
				"&orig_product_id=" + originalDeliveryInfoVO.getProductId() +
				"&recipient_city=" + tempDeliveryInfoVO.getRecipientCity() +
				"&orig_product_amount=" + originalOrderBillVO.getProductAmount() +
				"&recipient_country=" + tempDeliveryInfoVO.getRecipientCountry() +
				"&recipient_state=" + tempDeliveryInfoVO.getRecipientState() +
				"&recipient_zip_code=" + tempDeliveryInfoVO.getRecipientZipCode() +
				"&source_code=" + tempDeliveryInfoVO.getSourceCode();

    return parms;
  }

  /*
     *
     * @param msg = message to display in popup
     * @param displayYes = flag if yes button should be displayed
     * @param yesAction = action to take if yes clicked
     * @param displayNo= flag if no button should be displayed
     * @param noAction= action to take if no clicked
     * @param displayOk= flag if ok button should be displayed
     * @param okAction= action to take if ok clicked
     * @param yesAction2 = if a second alert is displayed, this is yes action for that
     * @param noAction2 = if a second alert is displayed, this is no action for that
     * @throws java.lang.Exception
     */
  private Document displayPopUpXML(List errorMessages)
    throws Exception {
    Document rootDoc = (Document) DOMUtil.getDocumentBuilder().newDocument();
    Element root = (Element) rootDoc.createElement("ERROR_MESSAGES");
    rootDoc.appendChild(root);

    Iterator iter = errorMessages.iterator();
    int i = 0;

    while (iter.hasNext()) {
      i++;

      ErrorMessageVO errorVO = (ErrorMessageVO) iter.next();

      Document errorXML = (Document) DOMUtil.getDocumentBuilder().newDocument();
      Element errorRoot = (Element) errorXML.createElement("ERROR_MESSAGE");
      errorRoot.setAttribute("id", Integer.toString(i));
      errorXML.appendChild(errorRoot);

      //add text
      addElement(errorXML, "TEXT", errorVO.getMessage());

      //determine what popup to display
      if (errorVO.isDisplayYes() && errorVO.isDisplayNo()) {
        addElement(errorXML, "POPUP_INDICATOR", "C");
      } else if (errorVO.isDisplayOk()) {
        addElement(errorXML, "POPUP_INDICATOR", "O");
      } else {
        throw new Exception("UI is not setup to display requested popup.");
      }

      //add the action to go to
      addElement(errorXML, "POPUP_GOTO_PAGE_YES", errorVO.getYesAction());
      addElement(errorXML, "POPUP_GOTO_PAGE_NO", errorVO.getNoAction());

      //add to main document
      DOMUtil.addSection(rootDoc, errorXML.getChildNodes());
    }

    //send back to page to display popup
    pageData.put("error_message", rootDoc);

    StringWriter sw = new StringWriter();
    DOMUtil.print((Document) rootDoc,new PrintWriter(sw));
    logger.debug(sw.toString());

    ActionForward actionForward = mapping.findForward("loadDeliveryConfirmation");

    //String path = actionForward.getPath() + "?securitytoken=" + sessionId + "&context=" + context + "&order_detail_id=" + orderDetailId;
    // String path = actionForward.getPath();
    //actionForward = new ActionForward(path,true);
    //put forward in map
    pageData.put("XSL", "forward");
    pageData.put("forward", actionForward);

    return rootDoc;
  }


  private boolean orderAmountsChanged() {
    boolean changed = false;

    //check if MSD (products_amount - discount_amount) changed
    if (originalOrderBillVO.getProductAmount() != tempOrderBillVO.getProductAmount()) {
      changed = true;
    }
    //check if add_on amount changed
    else if (originalOrderBillVO.getAddOnAmount() != tempOrderBillVO.getAddOnAmount()) {
      changed = true;
    }
    //check if service fee changed
    else if (originalOrderBillVO.getServiceFee() != tempOrderBillVO.getServiceFee()) {
      changed = true;
    }
    //check if shipping fee changed
    else if (originalOrderBillVO.getShippingFee() != tempOrderBillVO.getShippingFee()) {
      changed = true;
    }
    //check if tax changed (service fee tax  + shipping fee tax + tax)
    else if ((originalOrderBillVO.getServiceFeeTax() + originalOrderBillVO.getShippingTax() + originalOrderBillVO.getTax()) != (tempOrderBillVO.getServiceFeeTax() +
        tempOrderBillVO.getShippingTax() + tempOrderBillVO.getTax())) {
      changed = true;
    }
    //check if discount amount changed
    else if (originalOrderBillVO.getDiscountAmount() != tempOrderBillVO.getDiscountAmount()) {
      changed = true;
    }

    return changed;
  }

  private void processSendMessage(Connection conn, boolean floristChanged)
    throws Exception {
    logger.debug("SendMessage(conn,boolean)  FloristChanged=" + floristChanged);

    boolean checkForAltChanges = true;

    //daos
    UpdateOrderDAO updateOrderDAO = new UpdateOrderDAO(conn);

    String vendorFloristFlag = originalDeliveryInfoVO.getVendorFlag().equals("Y") ? "Vendor" : "Mercury";

    String changeMsgText = checkForChanges(false, NEWLINE_CHAR_HTML, false, false);

    //check if anything changed on the order
    if ((changeMsgText == null) || (changeMsgText.length() == 0)) {
      //save changes and redirect to recipient order page
      processCommitChanges();
    }
    else if(efosHold || gnaddHold)
    {
        //save changes and redirect to recipient order page
        processCommitChanges();
    }
    //if live mercury
    else if (updateOrderDAO.hasLiveMercury(orderDetailId, vendorFloristFlag, false)) {
      //processSendMessgeLiveMercury
      processSendMessageLiveMercury(floristChanged);
    } else {
      //processSendMessageNoMercury
      processSendMessageNoMercury(floristChanged);
    }
  }

  private void processSendMessageNoMercury(boolean floristChanged)
    throws Exception {
    ErrorMessageVO errorVO = new ErrorMessageVO();
    ArrayList errorList = new ArrayList();

    logger.debug("processSendMessageNoMercury(boolean)  FloristChanged=" + floristChanged);

    //prompt user that a new order will be sent out
    //(if yes, submit with action create_message)
    //(if no, submit with action commit_changes)
    //if delivery type is florist
    if (tempDeliveryInfoVO.getVendorFlag().equals("N")) {
      //prompt NEW_MERCURY_ORDER_MSG + "florist" + CONTINUE_MSG
      errorVO.setMessage(NEW_ORDER_MSG + TYPE_FLORIST_MSG + ".  " + CONTINUE_MSG);
      errorVO.setDisplayYes(true);
      errorVO.setYesAction("updateDeliveryConfirmation.do?action=create_message&msg_type=ftd");
      errorVO.setDisplayNo(true);
      errorVO.setNoAction("updateDeliveryConfirmation.do?action=commit_changes");
      errorVO.setDisplayOk(false);
      errorList.add(errorVO);
      displayPopUpXML(errorList);
    } else {
      //else
      //prompt NEW_MERCURY_ORDER_MSG + "venodr" + CONTINUE_MSG
      errorVO.setMessage(NEW_ORDER_MSG + TYPE_VENDOR_MSG + ".  " + CONTINUE_MSG);
      errorVO.setDisplayYes(true);
      errorVO.setYesAction("updateDeliveryConfirmation.do?action=create_message&msg_type=ftd");
      errorVO.setDisplayNo(true);
      errorVO.setNoAction("updateDeliveryConfirmation.do?action=commit_changes");
      errorVO.setDisplayOk(false);
      errorList.add(errorVO);
      displayPopUpXML(errorList);
    } //end if florist delivered
  }

  private void processSendMessageLiveMercury(boolean floristChanged)
    throws Exception {
    ErrorMessageVO errorVO = new ErrorMessageVO();
    ArrayList errorList = new ArrayList();

    logger.debug("processSendMessageLiveMercury(,boolean)  FloristChanged=" + floristChanged);

    logger.debug("Original vendor flag =" + originalDeliveryInfoVO.getVendorFlag());
    logger.debug("New vendor flag =" + tempDeliveryInfoVO.getVendorFlag());

    //if orig and new order have different delivery types
    if (!originalDeliveryInfoVO.getVendorFlag().equals(tempDeliveryInfoVO.getVendorFlag())) {
      //prompt user about sending CAN
      //(yes, submit to create_message)
      //(no, prompt user..submit to commit_changes or create_message)
      logger.debug("Ship type changed.");

      //if new type is vendor
      if (originalDeliveryInfoVO.getVendorFlag().equals("N")) {
        //prompt: CAN_SENT_MSG + 'florist' + CONTINUE_MSG
        errorVO.setMessage(SEND_CAN_MSG + TYPE_FLORIST_MSG + ".");
        errorVO.setDisplayYes(false);
        errorVO.setYesAction("updateDeliveryConfirmation.do?action=create_message&msg_type=can");
        errorVO.setDisplayNo(false);
        errorVO.setNoAction("2");
        errorVO.setDisplayOk(true);
        errorList.add(errorVO);

        ErrorMessageVO errorVOb = new ErrorMessageVO();
        errorVOb.setMessage(COMP_MSG + TYPE_FLORIST_MSG + ".  " + CONTINUE_MSG);
        errorVOb.setDisplayYes(true);
        errorVOb.setYesAction("updateDeliveryConfirmation.do?action=create_message&msg_type=ftdc");
        errorVOb.setDisplayNo(true);
        errorVOb.setNoAction("updateDeliveryConfirmation.do?action=commit_changes");
        errorVOb.setDisplayOk(false);

        errorList.add(errorVOb);
        displayPopUpXML(errorList);
      } else {
        //prompt: CAN_SENT_MSG + 'vendor' + CONTINUE_MSG
        errorVO.setMessage(SEND_CAN_MSG + TYPE_VENDOR_MSG + ".");
        errorVO.setDisplayYes(false);
        errorVO.setYesAction("updateDeliveryConfirmation.do?action=create_message&msg_type=can");
        errorVO.setDisplayNo(false);
        errorVO.setNoAction("2");
        errorVO.setDisplayOk(true);
        errorList.add(errorVO);

        ErrorMessageVO errorVOb = new ErrorMessageVO();
        errorVOb.setMessage(COMP_MSG + TYPE_VENDOR_MSG + ".  " + CONTINUE_MSG);
        errorVOb.setDisplayYes(true);
        errorVOb.setYesAction("updateDeliveryConfirmation.do?action=create_message&msg_type=ftdc");
        errorVOb.setDisplayNo(true);
        errorVOb.setNoAction("updateDeliveryConfirmation.do?action=commit_changes");
        errorVOb.setDisplayOk(false);
        errorList.add(errorVOb);
        displayPopUpXML(errorList);
      } //end if
    }
    //else, delivery types are the same
    else {
      logger.debug("Ship type did not change");

      //if florist order
      if (tempDeliveryInfoVO.getVendorFlag().equals("N")) {
        //if the florist id changed
        if (floristChanged || valueChanged(tempDeliveryInfoVO.getFloristId(), originalDeliveryInfoVO.getFloristId())) {
          logger.debug("Florist changed from " + originalDeliveryInfoVO.getFloristId() + " to " +
            tempDeliveryInfoVO.getFloristId());

          //prompt: CAN_SENT_MSG + 'vendor' + CONTINUE_MSG
          errorVO.setMessage(SEND_CAN_MSG + TYPE_FLORIST_MSG + ".");
          errorVO.setDisplayYes(false);
          errorVO.setYesAction("updateDeliveryConfirmation.do?action=create_message&msg_type=can");
          errorVO.setDisplayNo(false);
          errorVO.setNoAction("2");
          errorVO.setDisplayOk(true);
          errorList.add(errorVO);

          ErrorMessageVO errorVOb = new ErrorMessageVO();
          errorVOb.setMessage(COMP_MSG + TYPE_FLORIST_MSG + ".  " + CONTINUE_MSG);
          errorVOb.setDisplayYes(true);
          errorVOb.setYesAction("updateDeliveryConfirmation.do?action=create_message&msg_type=ftdc");
          errorVOb.setDisplayNo(true);
          errorVOb.setNoAction("updateDeliveryConfirmation.do?action=commit_changes");
          errorVOb.setDisplayOk(false);
          errorList.add(errorVOb);
          displayPopUpXML(errorList);
        } else {
          //get original total
          //Fix for defect#1826.  The stored proc that builds the originalOrderBillVO
          //adds in the amounts for the additionial bills.  So the add bill list should
          //be passed into this method.
          BigDecimal origTotal = calculateMercuryOrderAmount(originalOrderBillVO, null);
          origTotal = origTotal.setScale(2, BigDecimal.ROUND_HALF_UP);

          //get new total
          BigDecimal newTotal = calculateMercuryOrderAmount(tempOrderBillVO, tempAddBills);
          newTotal = newTotal.setScale(2, BigDecimal.ROUND_HALF_UP);

          //check if the florist is a mercury florist
          MessageDAO messageDAO = new MessageDAO(con);
          FloristStatusVO floristStatusVO = messageDAO.getFloristStatus(tempDeliveryInfoVO.getFloristId());
          MessageVO messageVO = messageDAO.getMessageForCurrentFlorist(originalDeliveryInfoVO.getMessageOrderNumber(), "Mercury", true, "OUTBOUND", true );
          String askMessage = ASK_SENT_MSG;
          String askPMessage = CHANGE_MERC_VALUE_MSG + "$" + messageVO.getCurrentPrice() + " to $" + newTotal;

          if ((floristStatusVO != null) && (floristStatusVO.getMercury() != null) &&
              floristStatusVO.getMercury().equalsIgnoreCase("M")) {
            //Mercury florist, no special text needs to be added to order
          } else {
            //Manual florist, need to add speical text
            askMessage = MANUAL_MERCURY_MESSAGE;
            askPMessage = askPMessage + MANUAL_MERCURY_MESSAGE;
          }

          //if totals are different
        BigDecimal baseTotal = new BigDecimal(messageVO.getCurrentPrice());
        baseTotal = baseTotal.setScale(2, BigDecimal.ROUND_HALF_UP);
        if (baseTotal.compareTo(newTotal) != 0) {
            //prompt: CHANGE_MERCURY_VALUE_MSG
            //(yes, submit create message askp)
            //(no, submit create message ask
            errorVO.setMessage(askPMessage);
            errorVO.setDisplayYes(true);
            errorVO.setYesAction("updateDeliveryConfirmation.do?action=create_message&msg_type=askp");
            errorVO.setDisplayNo(true);
            errorVO.setNoAction("updateDeliveryConfirmation.do?action=create_message&msg_type=ask");
            errorVO.setDisplayOk(false);
            errorList.add(errorVO);
            displayPopUpXML(errorList);
          } else //else, totals the same
           {
            //prompt: SENDING_ASK_MSG
            //ok, submit to create message)
            errorVO.setMessage(askMessage);
            errorVO.setDisplayYes(false);
            errorVO.setDisplayNo(false);
            errorVO.setNoAction("");
            errorVO.setDisplayOk(true);
            errorVO.setYesAction("updateDeliveryConfirmation.do?action=create_message&msg_type=ask");
            errorList.add(errorVO);
            displayPopUpXML(errorList);
          } //end if order totals same
        } //end if florist ids changed
      } else //else vendor order
       {
        //prompt: CAN_SENT_MSG + 'vendor' + CONTINUE_MSG
        errorVO.setMessage(SEND_CAN_MSG + TYPE_VENDOR_MSG + ".");
        errorVO.setDisplayYes(false);
        errorVO.setYesAction("updateDeliveryConfirmation.do?action=create_message&msg_type=can");
        errorVO.setDisplayNo(false);
        errorVO.setNoAction("2");
        errorVO.setDisplayOk(true);
        errorList.add(errorVO);

        ErrorMessageVO errorVOb = new ErrorMessageVO();
        errorVOb.setMessage(COMP_MSG + TYPE_VENDOR_MSG + ".  " + CONTINUE_MSG);
        errorVOb.setDisplayYes(true);
        errorVOb.setYesAction("updateDeliveryConfirmation.do?action=create_message&msg_type=ftdc");
        errorVOb.setDisplayNo(true);
        errorVOb.setNoAction("updateDeliveryConfirmation.do?action=commit_changes");
        errorVOb.setDisplayOk(false);
        errorList.add(errorVOb);
        displayPopUpXML(errorList);
      } //end, if florist delivered
    }
  }

  /*
   * Calculate the mercury order amount.
   * The mercury order amount is equal to the product amount + price of addons
   * + add bills.  Discounts, taxes, and fees are not included.
   */
  private BigDecimal calculateMercuryOrderAmount(OrderBillVO orderBillVO, List addBills) {
    BigDecimal price = new BigDecimal(0.0);

    price = price.add(new BigDecimal(orderBillVO.getProductAmount()));
    price = price.add(new BigDecimal(orderBillVO.getAddOnAmount()));

    //add bills for product and addon amounts
    if (addBills != null) {
      Iterator iter = addBills.iterator();

      while (iter.hasNext()) {
        OrderBillVO billVO = (OrderBillVO) iter.next();

        price = price.add(new BigDecimal(billVO.getProductAmount()));
        price = price.add(new BigDecimal(billVO.getAddOnAmount()));
      }
    }

    return price;
  }

  /*
   * Calculate the product price for the mercury messages.
   * The product price is equal to the product amount + price of addons
   */
  private BigDecimal calculateMercuryProductPrice(OrderBillVO orderBillVO) {
    BigDecimal price = new BigDecimal(0.0);

    price = price.add(new BigDecimal(orderBillVO.getProductAmount()));
    price = price.add(new BigDecimal(orderBillVO.getAddOnAmount()));

    return price;
  }

  /*
   * Create an outgoing message
   */
  private void processCreateMessage(Connection conn, String messageType)
    throws Exception {
    ActionForward actionForward = new ActionForward();

    HashMap attributes = new HashMap();

    //get a string containing all the changes made to order
    //New method added to separate mercury messages to the florist.  Issue 2441.
    String orderChangesWithN = checkForChangesMercury(NEWLINE_CHAR_CODE);
    String orderChangesWithHTML = checkForChanges(false, NEWLINE_CHAR_HTML, true, true);

    //add statement to the NEWLINE_CHAR_CODE version
    orderChangesWithN = CUSTOMER_REQ_CHANGE_MSG + NEWLINE_CHAR_CODE + orderChangesWithN;
    orderChangesWithN = URLEncoder.encode(orderChangesWithN);

    UpdateOrderDAO updateOrderDAO = new UpdateOrderDAO(conn);

    //save the changes as a comment on the order
    if ((orderChangesWithHTML != null) && (orderChangesWithHTML.length() > 0)) {
      CommentsVO commentsVO = new CommentsVO();
      commentsVO.setComment(orderChangesWithHTML);
      commentsVO.setCommentOrigin(request.getParameter(COMConstants.START_ORIGIN));
      commentsVO.setCommentType("Order");
      commentsVO.setCreatedBy(csrId);
      commentsVO.setUpdatedBy(csrId);
      commentsVO.setCustomerId(customerId);
      commentsVO.setOrderDetailId(orderDetailId);
      commentsVO.setOrderGuid(orderGuid);
      updateOrderDAO.insertCommentsUpdate(commentsVO);
    } else {
      logger.debug("In Create Message...and no comments to save");
    }

    //if florist delivered check if florist is suspended
    FloristStatusVO floristStatusVO = null;

    if (!tempDeliveryInfoVO.getVendorFlag().equals("Y")) {
      MessageDAO messageDAO = new MessageDAO(con);
      floristStatusVO = messageDAO.getFloristStatus(tempDeliveryInfoVO.getFloristId());
    }

    //commit the changes to the order
    updateOrderDAO.commitUpdateOrder(orderDetailId, "Y");

    //logic added 01/05/06 - issue when locks are not released when changes are made.
    //release locks
    LockDAO lockDAO = new LockDAO(con);
    logger.debug("Im in processCreateMessage");
    logger.debug("sessionId = " + sessionId);
    logger.debug("csrId = " + csrId);
    logger.debug("orderDetailId = " + orderDetailId);

    lockDAO.releaseLock(sessionId, csrId, orderDetailId, "MODIFY_ORDER");

    logger.debug("locks released");

    String origMsgType = originalDeliveryInfoVO.getVendorFlag().equals("Y") ? "Venus" : "Mercury";
    String tempMsgType = tempDeliveryInfoVO.getVendorFlag().equals("Y") ? "Venus" : "Mercury";

    //based on the messge type call the appropiate method (ask,askp,can,ftd,ftdc)
    //to send the message
    StringBuffer parms = new StringBuffer();
    parms.append("?msg_message_type=" + origMsgType);

    if (messageType == null) {
      throw new Exception("Null Message Type");
    } else if (messageType.equalsIgnoreCase("ASK")) {
      parms.append("&reason=" + orderChangesWithN);
      actionForward = mapping.findForward("prepareAskFlorist");
    } else if (messageType.equalsIgnoreCase("ASKP")) {
      parms.append("&reason=" + orderChangesWithN);

      BigDecimal orderAmount = calculateMercuryOrderAmount(tempOrderBillVO, tempAddBills);
      orderAmount = orderAmount.setScale(2, BigDecimal.ROUND_HALF_UP);

      parms.append("&new_mercury_order_amount=" + orderAmount);
      actionForward = mapping.findForward("prepareFloristPriceChange");
    } else if (messageType.equalsIgnoreCase("CAN")) {
      parms.append("&manual_can=Y");
      parms.append("&reason=" + PLEASE_CANCEL_MESSAGE);
      parms.append("&new_msg_message_type=" + tempMsgType);
      parms.append("&new_florist_id=" + tempDeliveryInfoVO.getFloristId());

      actionForward = mapping.findForward("prepareCancel");

      //set messages type
      if ((originalDeliveryInfoVO.getFtdmIndicator() != null) && originalDeliveryInfoVO.getFtdmIndicator().equalsIgnoreCase("Y")) {
        parms.append("&msg_order_type=" + MessageOrderStatusVO.FTDM);
      } else if (originalDeliveryInfoVO.getVendorFlag().equals("Y")) {
        parms.append("&msg_order_type=" + MessageOrderStatusVO.VENDOR);
      } else {
        parms.append("&msg_order_type=" + MessageOrderStatusVO.FLORIST);
      }

      //set subsequent action
      if (  ((originalDeliveryInfoVO.getFtdmIndicator() != null) &&
              originalDeliveryInfoVO.getFtdmIndicator().equalsIgnoreCase("Y"))
              ||
            ((floristStatusVO != null) && floristStatusVO.getStatus().equalsIgnoreCase("Suspend"))
              ||
            (floristStatusVO != null && (floristStatusVO.getMercury() == null || !floristStatusVO.getMercury().equalsIgnoreCase("M")))
            ) {
        parms.append("&subsequent_action=prepare_callout");
      } else {
        parms.append("&subsequent_action=prepare_new_ftd");
      }
    } else if (messageType.equalsIgnoreCase("FTD")) {
      if (floristStatusVO != null)
      {
          if(!floristStatusVO.getMercury().equalsIgnoreCase("M") || floristStatusVO.getStatus().equalsIgnoreCase("Suspend"))
          {
              actionForward = mapping.findForward("prepareCallout");
          }
          else
          {
              actionForward = mapping.findForward("prepareNewFTD");
          }
      }
      else
      {
          actionForward = mapping.findForward("prepareNewFTD");
      }



      parms.append("&new_florist_id=" + tempDeliveryInfoVO.getFloristId());

      if ((originalDeliveryInfoVO.getFtdmIndicator() != null) && originalDeliveryInfoVO.getFtdmIndicator().equalsIgnoreCase("Y")) {
        parms.append("&msg_order_type=" + MessageOrderStatusVO.FTDM);
      } else if (tempDeliveryInfoVO.getVendorFlag().equals("Y")) {
        parms.append("&msg_order_type=" + MessageOrderStatusVO.VENDOR);
      } else {
        parms.append("&msg_order_type=" + MessageOrderStatusVO.FLORIST);
      }
    } else if (messageType.equalsIgnoreCase("FTDC")) {
      if ((floristStatusVO.getMercury() != null) && floristStatusVO.getMercury().equalsIgnoreCase("M")) {
        actionForward = mapping.findForward("preparePayOriginalNewFTD");
      } else {
        actionForward = mapping.findForward("prepareCOMPCallout");
      }

      parms.append("&new_msg_message_type=" + tempMsgType);
      parms.append("&new_florist_id=" + tempDeliveryInfoVO.getFloristId());
    } else {
      throw new Exception("Invalid message type " + messageType);
    }

    //add remaining parms
    parms.append("&destination=recipient_order_page");
    parms.append("&order_detail_id=" + orderDetailId);
    parms.append("&msg_message_order_number=" + originalDeliveryInfoVO.getMessageOrderNumber());
    parms.append("&securitytoken=" + sessionId);
    parms.append("&context=" + context);
    parms.append("&msg_external_order_number=" + externalOrderNumber);
    parms.append("&msg_guid=" + orderGuid);
    parms.append("&msg_customer_id=" + customerId);

    if (tempDeliveryInfoVO.getVendorFlag().equals("N")) {
    } else {
      parms.append("&new_productId=" + tempDeliveryInfoVO.getProductId());
    }

    String path = actionForward.getPath() + parms.toString();

    logger.debug("Path to COM page:" + path);

    //encode path
    logger.debug("Encoded path to COM page:" + path);

    //redirect parameter needs to be set to false
    //otherwise we lose the datafilter information.
    actionForward = new ActionForward(path, false);

    //put attributes in map
    pageData.put("attributes", attributes);

    //put forward in map
    pageData.put("XSL", "forward");
    pageData.put("forward", actionForward);
  }


  private boolean isClassificationChange(DeliveryInfoVO infoA, DeliveryInfoVO infoB) {
    boolean changed = false;

    if (valueChanged(infoA.getProductId(), infoB.getProductId()) || valueChanged(infoA.getSourceCode(), infoB.getSourceCode()) ||
        valueChanged(infoA.getVendorFlag(), infoB.getVendorFlag())) {
      changed = true;
    }

    return changed;
  }

  /**
   * This method returns a paragraph of text describing the changes made on the order.
   * The other method of the same name does a similar thing, but returns a boolean.
   * @param checkRecipOnly - check only the recipient information
   * @param newLineDelimeter - the new line delimiter to use in the text
   * @param includeAltContact - include the Alt Contact changes
   * @param includeCommentsOnly - use in the comments and not in the messaging to the florist.
   * @return A paragraph of text describing the changes made on the order.
   * @throws java.lang.Exception
   */
  private String checkForChanges(boolean checkRecipOnly, String newLineDelimeter, boolean includeAltContact,
    boolean includeCommentsOnly) throws Exception {
    StringBuffer changesBuff = new StringBuffer();
    boolean hasChanges = false;
    boolean productChanged = false;

    //DAO used to look up code descriptions (e.g. color, addon)
    LookupDAO lookDAO = new LookupDAO(con);

    MessageDAO messageDAO = new MessageDAO(con);
    MessageVO messageVO = messageDAO.getMessageForCurrentFlorist(originalDeliveryInfoVO.getMessageOrderNumber(), "Mercury", true, "OUTBOUND", true );

    if (valueChanged(originalDeliveryInfoVO.getRecipientFirstName(), tempDeliveryInfoVO.getRecipientFirstName()) ||
        valueChanged(originalDeliveryInfoVO.getRecipientLastName(), tempDeliveryInfoVO.getRecipientLastName())) {
      addToBuffer(changesBuff,
        "Recipient's name changed from " + originalDeliveryInfoVO.getRecipientFirstName() + " " +
        originalDeliveryInfoVO.getRecipientLastName() + " to " + tempDeliveryInfoVO.getRecipientFirstName() + " " +
        tempDeliveryInfoVO.getRecipientLastName(), newLineDelimeter);
    }

    if (valueChanged(originalDeliveryInfoVO.getRecipientBusinessName(), tempDeliveryInfoVO.getRecipientBusinessName())) {
      addToBuffer(changesBuff,
        "Recipient's business name changed from " + originalDeliveryInfoVO.getRecipientBusinessName() + " to " +
        tempDeliveryInfoVO.getRecipientBusinessName(), newLineDelimeter);
    }

    if (valueChanged(originalDeliveryInfoVO.getRecipientAddress1(), tempDeliveryInfoVO.getRecipientAddress1()) ||
        valueChanged(originalDeliveryInfoVO.getRecipientAddress2(), tempDeliveryInfoVO.getRecipientAddress2())) {
      addToBuffer(changesBuff,
        "Recipient's address changed from " + originalDeliveryInfoVO.getRecipientAddress1() + " " +
        formatString(originalDeliveryInfoVO.getRecipientAddress2()) + " to " + tempDeliveryInfoVO.getRecipientAddress1() + " " +
        formatString(tempDeliveryInfoVO.getRecipientAddress2()), newLineDelimeter);
    }

    //Issue 2526
    if (valueChanged(originalDeliveryInfoVO.getRecipientAddressType(), tempDeliveryInfoVO.getRecipientAddressType()))
    {
      addToBuffer(changesBuff,
        "Recipient's Delivery Location changed from " + originalDeliveryInfoVO.getRecipientAddressType() +
        " to " + tempDeliveryInfoVO.getRecipientAddressType(), newLineDelimeter);
    }

    if (valueChanged(originalDeliveryInfoVO.getRecipientCity(), tempDeliveryInfoVO.getRecipientCity()) ||
        valueChanged(originalDeliveryInfoVO.getRecipientState(), tempDeliveryInfoVO.getRecipientState()) ||
        valueChanged(originalDeliveryInfoVO.getRecipientZipCode(), tempDeliveryInfoVO.getRecipientZipCode()) ||
        valueChanged(originalDeliveryInfoVO.getRecipientCountry(), tempDeliveryInfoVO.getRecipientCountry())) {
      addToBuffer(changesBuff,
        "Recipient's city state and zip / postal code has changed from " + originalDeliveryInfoVO.getRecipientCity() + " " +
        originalDeliveryInfoVO.getRecipientState() + " " + originalDeliveryInfoVO.getRecipientZipCode() + " to " +
        tempDeliveryInfoVO.getRecipientCity() + " " + tempDeliveryInfoVO.getRecipientZipCode() + " " +
        tempDeliveryInfoVO.getRecipientState(), newLineDelimeter);
    }

    String origPhone = removeNonNumerics(originalDeliveryInfoVO.getRecipientPhoneNumber());
    String newPhone = removeNonNumerics(tempDeliveryInfoVO.getRecipientPhoneNumber());
    String origExt = removeNonNumerics(originalDeliveryInfoVO.getRecipientExtension());
    String newExt = removeNonNumerics(tempDeliveryInfoVO.getRecipientExtension());

    if (valueChanged(origPhone, newPhone) || valueChanged(origExt, newExt)) {
      addToBuffer(changesBuff,
        "Recipient's phone number changed from " + originalDeliveryInfoVO.getRecipientPhoneNumber() + " ext " +
        formatString(originalDeliveryInfoVO.getRecipientExtension()) + " to " + tempDeliveryInfoVO.getRecipientPhoneNumber() +
        " ext " + formatString(tempDeliveryInfoVO.getRecipientExtension()), newLineDelimeter);
    }

    if (valueChanged(originalDeliveryInfoVO.getDeliveryDate(), tempDeliveryInfoVO.getDeliveryDate())) {
      addToBuffer(changesBuff, "The delivery date changed from ", newLineDelimeter);

      if (originalDeliveryInfoVO.getDeliveryDate() == null) {
        changesBuff.append("<null>");
      } else {
        changesBuff.append(FieldUtils.formatUtilDateToString(originalDeliveryInfoVO.getDeliveryDate().getTime()));
      }

      changesBuff.append(" to ");

      if (tempDeliveryInfoVO.getDeliveryDate() == null) {
        changesBuff.append("<null>");
      } else {
        changesBuff.append(FieldUtils.formatUtilDateToString(tempDeliveryInfoVO.getDeliveryDate().getTime()));
      }
    }

    if (valueChanged(originalDeliveryInfoVO.getCardMessage(), tempDeliveryInfoVO.getCardMessage()) ||
        valueChanged(originalDeliveryInfoVO.getCardSignature(), tempDeliveryInfoVO.getCardSignature())) {
      addToBuffer(changesBuff,
        "The card message and signature changed from " + formatString(originalDeliveryInfoVO.getCardMessage()) + " " +
        formatString(originalDeliveryInfoVO.getCardSignature()) + " to " + formatString(tempDeliveryInfoVO.getCardMessage()) +
        " " + formatString(tempDeliveryInfoVO.getCardSignature()), newLineDelimeter);
    }

    if (valueChanged(originalDeliveryInfoVO.getSpecialInstructions(), tempDeliveryInfoVO.getSpecialInstructions())) {
      addToBuffer(changesBuff,
        "Special delivery instructions has changed from " + formatString(originalDeliveryInfoVO.getSpecialInstructions()) + " " +
        " to " + formatString(tempDeliveryInfoVO.getSpecialInstructions()), newLineDelimeter);
    }

    //only check if this if asked to
    if (includeAltContact) {
      if (isAltContactChange()) {
        addToBuffer(changesBuff,
          "The alternate contact information changed from " + formatString(originalDeliveryInfoVO.getAltContactFirstName()) +
          " " + formatString(originalDeliveryInfoVO.getAltContactLastName()) + " " +
          formatString(originalDeliveryInfoVO.getAltContactPhoneNumber()) + " " +
          formatString(originalDeliveryInfoVO.getAltContactExtension()) + " to " + formatString(tempDeliveryInfoVO.getAltContactFirstName()) +
          " " + formatString(tempDeliveryInfoVO.getAltContactLastName()) + " " + formatString(tempDeliveryInfoVO.getAltContactPhoneNumber()) + " " +
          formatString(tempDeliveryInfoVO.getAltContactExtension()), newLineDelimeter);
      }
    }

    //only do the following if we are checking for things other then recipient
    if (!checkRecipOnly) {
      if (valueChanged(originalDeliveryInfoVO.getProductId(), tempDeliveryInfoVO.getProductId())) {
        addToBuffer(changesBuff,
          "The product changed from " + originalDeliveryInfoVO.getProductId() + " to " + tempDeliveryInfoVO.getProductId(),
          newLineDelimeter);

        productChanged = true;
      }

      if (valueChanged(originalDeliveryInfoVO.getActualProductAmount(), tempOrderBillVO.getProductAmount())) {
        addToBuffer(changesBuff,
          "The product price changed from " + originalDeliveryInfoVO.getActualProductAmount() + " to " +
          tempOrderBillVO.getProductAmount(), newLineDelimeter);
      }

     //check if the ship type change
     if(valueChanged(tempDeliveryInfoVO.getVendorFlag(),originalDeliveryInfoVO.getVendorFlag()))
     {
          if (tempDeliveryInfoVO.getVendorFlag().equals("N"))
          {
            addToBuffer(changesBuff,"Order changed from vendor to florist delivered.",newLineDelimeter);
          }
          else
          {
            addToBuffer(changesBuff,"Order changed from florist to vendor delivered.",newLineDelimeter);
          }
     }


      //if florist delivered checks
      if (tempDeliveryInfoVO.getVendorFlag().equals("N")) {
        //Only include the color change messages if this is being done for the FTD message,
        //of if the product changed.
        if (!includeCommentsOnly || productChanged) {
          //Do not include the color changes if the product was changed from a product
          //that had color choices associated with it to on that doesn't.
          if (hasColorChoices(originalDeliveryInfoVO.getColor1(), tempDeliveryInfoVO.getColor1())) {
            if (valueChanged(originalDeliveryInfoVO.getColor1(), tempDeliveryInfoVO.getColor1())) {
              addToBuffer(changesBuff,
                "The product color first choice changed from " +
                lookDAO.getColorDescriptionById(originalDeliveryInfoVO.getColor1()) + " to " +
                lookDAO.getColorDescriptionById(tempDeliveryInfoVO.getColor1()), newLineDelimeter);
            }

            if (valueChanged(originalDeliveryInfoVO.getColor2(), tempDeliveryInfoVO.getColor2())) {
              addToBuffer(changesBuff,
                "The product color second choice changed from " +
                lookDAO.getColorDescriptionById(originalDeliveryInfoVO.getColor2()) + " to " +
                lookDAO.getColorDescriptionById(tempDeliveryInfoVO.getColor2()), newLineDelimeter);
            }
          }
        }

        if (valueChanged(originalDeliveryInfoVO.getSubstitutionIndicator(), tempDeliveryInfoVO.getSubstitutionIndicator())) {
          addToBuffer(changesBuff,
            "The substitution code changed from " + originalDeliveryInfoVO.getSubstitutionIndicator() + " to " +
            tempDeliveryInfoVO.getSubstitutionIndicator(), newLineDelimeter);
        }

        String addonComment = createAddonComment(newLineDelimeter);

        if (addonComment != null) {
          addToBuffer(changesBuff, addonComment, newLineDelimeter);
        }
      } else //else, vendor delivered
       {
        if (valueChanged(originalDeliveryInfoVO.getShipMethod(), tempDeliveryInfoVO.getShipMethod())) {
          addToBuffer(changesBuff,
            "The ship method changed from " + originalDeliveryInfoVO.getShipMethod() + " to " +
            tempDeliveryInfoVO.getShipMethod(), newLineDelimeter);
        }
      } //end else, vendor
    } //end, check recip only

    //Only check for this changes if we are building order comments
    if (includeCommentsOnly) {
      if (valueChanged(originalDeliveryInfoVO.getSourceCode(), tempDeliveryInfoVO.getSourceCode())) {
        addToBuffer(changesBuff,
          "The source code changed from " + originalDeliveryInfoVO.getSourceCode() + " to " + tempDeliveryInfoVO.getSourceCode(),
          newLineDelimeter);
      }
    }

    //Only check for this changes if we are building order comments
    if (includeCommentsOnly) {
      if ((valueChanged(originalDeliveryInfoVO.getMembershipFirstName(), tempDeliveryInfoVO.getMembershipFirstName())) ||
          (valueChanged(originalDeliveryInfoVO.getMembershipLastName(), tempDeliveryInfoVO.getMembershipLastName()))) {
        addToBuffer(changesBuff,
          "The membership name changed from " + formatString(originalDeliveryInfoVO.getMembershipFirstName()) + " " +
          formatString(originalDeliveryInfoVO.getMembershipLastName()) + " to " +
          formatString(tempDeliveryInfoVO.getMembershipFirstName()) + " " +
          formatString(tempDeliveryInfoVO.getMembershipLastName()), newLineDelimeter);
      }

      if (valueChanged(originalDeliveryInfoVO.getMembershipNumber(), tempDeliveryInfoVO.getMembershipNumber())) {
        addToBuffer(changesBuff,
          "The membership number changed from " + originalDeliveryInfoVO.getMembershipNumber() + " to " +
          tempDeliveryInfoVO.getMembershipNumber(), newLineDelimeter);
      }
    }

    logger.debug("Changes made to order: " + changesBuff.toString());

    return changesBuff.toString();
  }


  /**
   * This method returns a paragraph of text describing the changes made on the order.
   * This method returns text for the mercury messages to be sent to the florist.
   * @param newLineDelimeter - the new line delimiter to use in the text
   * @return A paragraph of text describing the changes made on the order.
   * @throws java.lang.Exception
   */
  private String checkForChangesMercury(String newLineDelimeter)
    throws Exception
  {
    StringBuffer changesBuff = new StringBuffer();
    boolean hasChanges = false;
    boolean productChanged = false;

    //DAO used to look up code descriptions (e.g. color, addon)
    LookupDAO lookDAO = new LookupDAO(con);

    MessageDAO messageDAO = new MessageDAO(con);
    MessageVO messageVO = messageDAO.getMessageForCurrentFlorist(originalDeliveryInfoVO.getMessageOrderNumber(), "Mercury", true, "OUTBOUND", true );

    if (valueChanged(originalDeliveryInfoVO.getRecipientFirstName(), tempDeliveryInfoVO.getRecipientFirstName()) ||
        valueChanged(originalDeliveryInfoVO.getRecipientLastName(), tempDeliveryInfoVO.getRecipientLastName())) {
      addToBuffer(changesBuff,
        "Recipient's name changed to " + tempDeliveryInfoVO.getRecipientFirstName() + " " +
                                         tempDeliveryInfoVO.getRecipientLastName(), newLineDelimeter);
    }

    if (valueChanged(originalDeliveryInfoVO.getRecipientBusinessName(), tempDeliveryInfoVO.getRecipientBusinessName())) {
      addToBuffer(changesBuff,
        "Recipient's business name changed to " + tempDeliveryInfoVO.getRecipientBusinessName(), newLineDelimeter);
    }

    if (valueChanged(originalDeliveryInfoVO.getRecipientAddress1(), tempDeliveryInfoVO.getRecipientAddress1()) ||
        valueChanged(originalDeliveryInfoVO.getRecipientAddress2(), tempDeliveryInfoVO.getRecipientAddress2())) {
      addToBuffer(changesBuff,
        "Recipient's address changed to " + tempDeliveryInfoVO.getRecipientAddress1() + " " +
                               formatString(tempDeliveryInfoVO.getRecipientAddress2()), newLineDelimeter);
    }

    //Issue 2526
    if (valueChanged(originalDeliveryInfoVO.getRecipientAddressType(), tempDeliveryInfoVO.getRecipientAddressType()))
    {
      addToBuffer(changesBuff,
        "Recipient's Delivery Location changed to  " + tempDeliveryInfoVO.getRecipientAddressType(), newLineDelimeter);
    }


    if (valueChanged(originalDeliveryInfoVO.getRecipientCity(), tempDeliveryInfoVO.getRecipientCity()) ||
        valueChanged(originalDeliveryInfoVO.getRecipientState(), tempDeliveryInfoVO.getRecipientState()) ||
        valueChanged(originalDeliveryInfoVO.getRecipientZipCode(), tempDeliveryInfoVO.getRecipientZipCode()) ||
        valueChanged(originalDeliveryInfoVO.getRecipientCountry(), tempDeliveryInfoVO.getRecipientCountry())) {
      addToBuffer(changesBuff,
        "Recipient's city state and zip / postal code has changed to " + tempDeliveryInfoVO.getRecipientCity()
                                       + ", " + tempDeliveryInfoVO.getRecipientState() + " "
                                       + tempDeliveryInfoVO.getRecipientZipCode(), newLineDelimeter);
    }

    String origPhone = removeNonNumerics(originalDeliveryInfoVO.getRecipientPhoneNumber());
    String newPhone = removeNonNumerics(tempDeliveryInfoVO.getRecipientPhoneNumber());
    String origExt = removeNonNumerics(originalDeliveryInfoVO.getRecipientExtension());
    String newExt = removeNonNumerics(tempDeliveryInfoVO.getRecipientExtension());

    if (valueChanged(origPhone, newPhone) || valueChanged(origExt, newExt)) {
      addToBuffer(changesBuff,
        "Recipient's phone number changed to " + tempDeliveryInfoVO.getRecipientPhoneNumber() +
                          " ext " + formatString(tempDeliveryInfoVO.getRecipientExtension()), newLineDelimeter);
    }

    if (valueChanged(originalDeliveryInfoVO.getDeliveryDate(), tempDeliveryInfoVO.getDeliveryDate())) {
      addToBuffer(changesBuff, "The delivery date changed to ", newLineDelimeter);
      if (tempDeliveryInfoVO.getDeliveryDate() == null) {
        changesBuff.append("<null>");
      } else {
        changesBuff.append(FieldUtils.formatUtilDateToString(tempDeliveryInfoVO.getDeliveryDate().getTime()));
        if(tempDeliveryInfoVO.getDeliveryDateRangeEnd() != null && !tempDeliveryInfoVO.getDeliveryDateRangeEnd().equals(""))
        {
          changesBuff.append(" - " + FieldUtils.formatUtilDateToString(tempDeliveryInfoVO.getDeliveryDateRangeEnd().getTime()));
        }
      }
    }

    if (valueChanged(originalDeliveryInfoVO.getCardMessage(), tempDeliveryInfoVO.getCardMessage()) ||
        valueChanged(originalDeliveryInfoVO.getCardSignature(), tempDeliveryInfoVO.getCardSignature())) {
      addToBuffer(changesBuff,
        "Card Message And Signature changed to" + formatString(tempDeliveryInfoVO.getCardMessage()) +
                        " " + formatString(tempDeliveryInfoVO.getCardSignature()), newLineDelimeter);
    }

    if (valueChanged(originalDeliveryInfoVO.getProductId(), tempDeliveryInfoVO.getProductId())) {
        addToBuffer(changesBuff,
          "Product Code changed to " + tempDeliveryInfoVO.getProductId(),
          newLineDelimeter);

        productChanged = true;
      }

      String addonComment = createAddonCommentForMercuryMessages(newLineDelimeter);

      if ( valueChanged(originalDeliveryInfoVO.getActualProductAmount(), tempOrderBillVO.getProductAmount()) || valueChanged(originalOrderBillVO.getAddOnAmount(), tempOrderBillVO.getAddOnAmount())) {

        //get new total
        BigDecimal newTotal = calculateMercuryProductPrice(tempOrderBillVO);
        newTotal = newTotal.setScale(2, BigDecimal.ROUND_HALF_UP);

        if(addonComment != null && !addonComment.equals(""))
          addToBuffer(changesBuff,
            "Product Price changed to " + newTotal + " which includes " +
             addonComment, newLineDelimeter);
        else
          addToBuffer(changesBuff,
            "Product Price changed to " + newTotal, newLineDelimeter);
      }

      //if florist delivered checks
      if (tempDeliveryInfoVO.getVendorFlag().equals("N")) {
        //Only include the color change messages if this is being done for the FTD message,
        //of if the product changed.
        if (productChanged) {
          //Do not include the color changes if the product was changed from a product
          //that had color choices associated with it to on that doesn't.
          if (hasColorChoices(originalDeliveryInfoVO.getColor1(), tempDeliveryInfoVO.getColor1())) {
            if (valueChanged(originalDeliveryInfoVO.getColor1(), tempDeliveryInfoVO.getColor1())) {
              addToBuffer(changesBuff,
                "First Color Choice changed to " +
                lookDAO.getColorDescriptionById(tempDeliveryInfoVO.getColor1()), newLineDelimeter);
            }

            if (valueChanged(originalDeliveryInfoVO.getColor2(), tempDeliveryInfoVO.getColor2())) {
              addToBuffer(changesBuff,
                "Second Color Choice changed to " +
                lookDAO.getColorDescriptionById(tempDeliveryInfoVO.getColor2()), newLineDelimeter);
            }
          }
        }
      }

    logger.debug("Changes made to order: " + changesBuff.toString());

    return changesBuff.toString();
  }


  private String formatString(String value) {
    return (value == null) ? "" : value;
  }

  /*
   * Check if the alt contact info change
   */
  private boolean isAltContactChange() {
    boolean altContactChange = false;

    String origPhone = removeNonNumerics(originalDeliveryInfoVO.getAltContactPhoneNumber());
    String newPhone = removeNonNumerics(tempDeliveryInfoVO.getAltContactPhoneNumber());
    String origExt = removeNonNumerics(originalDeliveryInfoVO.getAltContactExtension());
    String newExt = removeNonNumerics(tempDeliveryInfoVO.getAltContactExtension());

    if (valueChanged(originalDeliveryInfoVO.getAltContactFirstName(), tempDeliveryInfoVO.getAltContactFirstName()) ||
        valueChanged(originalDeliveryInfoVO.getAltContactLastName(), tempDeliveryInfoVO.getAltContactLastName()) ||
        valueChanged(origPhone, newPhone) || valueChanged(origExt, newExt) ||
        valueChanged(originalDeliveryInfoVO.getAltContactEmail(), tempDeliveryInfoVO.getAltContactEmail())) {
      altContactChange = true;
    }

    return altContactChange;
  }

  /*
   * Check if the membership info change
   */
  private boolean isMembershipInfoChange() {
    boolean membershipInfoChange = false;
    if (valueChanged(originalDeliveryInfoVO.getMembershipNumber(), tempDeliveryInfoVO.getMembershipNumber()) ||
        valueChanged(originalDeliveryInfoVO.getMembershipFirstName(), tempDeliveryInfoVO.getMembershipFirstName()) ||
        valueChanged(originalDeliveryInfoVO.getMembershipLastName(), tempDeliveryInfoVO.getMembershipLastName()))
        {
           membershipInfoChange = true;
        }
    return membershipInfoChange;
  }


  private String createAddonComment(String newLineDelimeter)
    throws Exception {
    StringBuffer addonBuff = new StringBuffer();

    //DAO used to look up code descriptions (e.g. color, addon)
    LookupDAO lookDAO = new LookupDAO(con);

    //first check for any added addons and updated existing addons
    Iterator iter = tempOrderAddOnVO.keySet().iterator();

    while (iter.hasNext()) {
      //get addon key
      String key = (String) iter.next();

      //get the temp Addon
      AddOnVO tempAddon = (AddOnVO) tempOrderAddOnVO.get(key);

      //get original Addon
      AddOnVO origAddOn = (AddOnVO) originalOrderAddOnVO.get(key);

      //if addon was added
      if (origAddOn == null) {
        addToBuffer(addonBuff,
          "Add on " + lookDAO.getAddonDescriptionById(tempAddon.getAddOnCode()) + " with a quantity of " +
          tempAddon.getAddOnQuantity() + " added to order. ", newLineDelimeter);
      } else // check for quantity change
       {
        if (tempAddon.getAddOnQuantity() != origAddOn.getAddOnQuantity()) {
          addToBuffer(addonBuff,
            "Add on " + lookDAO.getAddonDescriptionById(tempAddon.getAddOnCode()) + " quantity changed from " +
            origAddOn.getAddOnQuantity() + " to " + tempAddon.getAddOnQuantity() + ".", newLineDelimeter);
        }
      } //end orig addon exists
    } //end addon loop

    //next check for any addons removed
    //first check for any added addons and updated existing addons
    iter = originalOrderAddOnVO.keySet().iterator();

    while (iter.hasNext()) {
      //get addon key
      String key = (String) iter.next();

      //get the temp Addon
      AddOnVO tempAddon = (AddOnVO) tempOrderAddOnVO.get(key);

      //get original Addon
      AddOnVO origAddOn = (AddOnVO) originalOrderAddOnVO.get(key);

      //if addon was removed
      if (tempAddon == null) {
        addToBuffer(addonBuff, "Add on " + lookDAO.getAddonDescriptionById(origAddOn.getAddOnCode()) + " with quantity of " +
            origAddOn.getAddOnQuantity() + " removed from order.",
          newLineDelimeter);
      }
    } //end addon loop

    return addonBuff.toString();
  }

   /* Method used to generate the add on details for mercury messages
    */
   private String createAddonCommentForMercuryMessages(String newLineDelimeter)
    throws Exception {
    StringBuffer addonBuff = new StringBuffer();

    //DAO used to look up code descriptions (e.g. color, addon)
    LookupDAO lookDAO = new LookupDAO(con);

    //check for any add ons on the updated order
    Iterator iter = tempOrderAddOnVO.keySet().iterator();

    while (iter.hasNext()) {
      //get addon key
      String key = (String) iter.next();

      //get the temp Addon
      AddOnVO tempAddon = (AddOnVO) tempOrderAddOnVO.get(key);

      if(addonBuff.length() > 0)
        {
          addToBuffer(addonBuff,
            "and " + tempAddon.getAddOnQuantity() + " " + lookDAO.getAddonDescriptionById(tempAddon.getAddOnCode()), newLineDelimeter);
        }
      else
      {
          addToBuffer(addonBuff,
            tempAddon.getAddOnQuantity() + " " + lookDAO.getAddonDescriptionById(tempAddon.getAddOnCode()), newLineDelimeter);
      }
    } //end addon loop

    //next check for any addons removed
    //first check for any added addons and updated existing addons
    iter = originalOrderAddOnVO.keySet().iterator();

    while (iter.hasNext()) {
      //get addon key
      String key = (String) iter.next();

      //get the temp Addon
      AddOnVO tempAddon = (AddOnVO) tempOrderAddOnVO.get(key);

      //get original Addon
      AddOnVO origAddOn = (AddOnVO) originalOrderAddOnVO.get(key);

      //if addon was removed
      if (tempAddon == null) {
       if(addonBuff.length() > 0)
        {
          addToBuffer(addonBuff, "and the removal of " + origAddOn.getAddOnQuantity() + " " + lookDAO.getAddonDescriptionById(origAddOn.getAddOnCode()),
            newLineDelimeter);
        }
        else
        {
          addToBuffer(addonBuff, "the removal of " + origAddOn.getAddOnQuantity() + " " + lookDAO.getAddonDescriptionById(origAddOn.getAddOnCode()),
            newLineDelimeter);
        }
      }
    } //end addon loop

   return addonBuff.toString();
  }

  private void addToBuffer(StringBuffer buffer, String message, String newLineChar) {
    if ((buffer != null) && (buffer.length() > 0)) {
      buffer.append(newLineChar);
    }

    buffer.append(message);
  }

  private boolean valueChanged(String inValue1, String inValue2) {
    boolean changed = true;

    if (((inValue1 == null) && (inValue2 == null)) || ((inValue1 != null) && (inValue2 != null) && inValue1.equalsIgnoreCase(inValue2))) {
      changed = false;
    }

    return changed;
  }

  //check to see if new product has color choices associated with it or not
  private boolean hasColorChoices(String inValue1, String inValue2) {
    boolean hasColorChoices = true;

    if (((inValue1 != null) && (inValue2 == null))) {
      hasColorChoices = false;
    }

    return hasColorChoices;
  }

  private boolean valueChanged(Calendar inValue1, Calendar inValue2) {
    boolean changed = true;

    if (((inValue1 == null) && (inValue2 == null)) || ((inValue1 != null) && (inValue2 != null) && inValue1.get(Calendar.DAY_OF_YEAR) == inValue2.get(Calendar.DAY_OF_YEAR))) {
      changed = false;
    }

    return changed;
  }

  private boolean valueChanged(double inValue1, double inValue2) {
    return ((inValue1 == inValue2) ? false : true);
  }

  private void addElement(Document xml, String keyName, String value) {
    Element nameElement = (Element) xml.createElement(keyName);

    if ((value != null) && (value.length() > 0)) {
      nameElement.appendChild(xml.createTextNode(value));
    }

    xml.getDocumentElement().appendChild(nameElement);
  }

  /*
   * Remove order from temp table, but save receipient changes.
   */
  private void processRollBack() throws Exception {
    //dao
    UpdateOrderDAO updateOrderDAO = new UpdateOrderDAO(con);

    //get string containing recipient changes
    String changes = checkForChanges(true, NEWLINE_CHAR_HTML, true, true);

    //release locks
    LockDAO lockDAO = new LockDAO(con);
    lockDAO.releaseLock(sessionId, csrId, orderDetailId, "MODIFY_ORDER");

    //insert comments to order
    if ((changes != null) && (changes.length() > 0)) {
      CommentsVO commentsVO = new CommentsVO();
      commentsVO.setComment(changes);
      commentsVO.setCommentOrigin(request.getParameter(COMConstants.START_ORIGIN));
      commentsVO.setCommentType("Order");
      commentsVO.setCreatedBy(csrId);
      commentsVO.setUpdatedBy(csrId);
      commentsVO.setCustomerId(customerId);
      commentsVO.setOrderDetailId(orderDetailId);
      commentsVO.setOrderGuid(orderGuid);
      commentsVO.setReason(request.getParameter(COMConstants.COMMENT_REASON));
      updateOrderDAO.insertCommentsUpdate(commentsVO);
    }

    //call method to rollback order
    updateOrderDAO.cancelUpdateOrder(orderDetailId, "Y");

    ActionForward actionForward = mapping.findForward("CustomerAccount");
    String path = actionForward.getPath() + "?securitytoken=" + sessionId + "&context=" + context + "&order_detail_id=" +
      orderDetailId + "&action=search" + "&in_order_number=" + orderDetailId;

    actionForward = new ActionForward(path, true);

    //put forward in map
    pageData.put("XSL", "forward");
    pageData.put("forward", actionForward);
  }

  /*
   * Get all the changes that are on an order and put them into a string.
   */
  private void processCommitChanges() throws Exception {
    //dao
    UpdateOrderDAO updateOrderDAO = new UpdateOrderDAO(con);

    //get string containing recipient changes
    String changes = checkForChanges(false, NEWLINE_CHAR_HTML, true, true);

    //release locks
    LockDAO lockDAO = new LockDAO(con);
    logger.debug("Im in processCommitChanges");
    logger.debug("sessionId = " + sessionId);
    logger.debug("csrId = " + csrId);
    logger.debug("orderDetailId = " + orderDetailId);

    lockDAO.releaseLock(sessionId, csrId, orderDetailId, "MODIFY_ORDER");

    //insert comments to order
    if ((changes != null) && (changes.length() > 0)) {
      CommentsVO commentsVO = new CommentsVO();
      commentsVO.setComment(changes);
      commentsVO.setCommentOrigin(request.getParameter(COMConstants.START_ORIGIN));
      commentsVO.setCommentType("Order");
      commentsVO.setCreatedBy(csrId);
      commentsVO.setUpdatedBy(csrId);
      commentsVO.setCustomerId(customerId);
      commentsVO.setOrderDetailId(orderDetailId);
      commentsVO.setOrderGuid(orderGuid);
      commentsVO.setReason(request.getParameter(COMConstants.COMMENT_REASON));
      updateOrderDAO.insertCommentsUpdate(commentsVO);
    }

    //call method to commit order
    updateOrderDAO.commitUpdateOrder(orderDetailId, "Y");

    //if order was on gnadd hold, then remove hold and send order back
    //to order processing.  If the order is still on gnadd then order processing
    //will place another hold on it.
    if(gnaddHold)
    {
        logger.debug("Order on gnadd hold (" + orderDetailId + ")");
        logger.debug("Order being resubmitted to order processing.");

        //delete existing hold
        OrderDAO orderDAO = new OrderDAO(con);

        try
        {
            orderDAO.deleteOrderHold(Long.parseLong(orderDetailId),"GNADD");
        }
        catch(Exception e)
        {
            //log exception, but keep processing.  The procedure will throw
            //an exception if there were no holds to delete.  We want to continue
            //processing even if there were no holds to delete.
            logger.warn(e);
        }

        //update order disposition
        orderDAO.updateOrderDisposition(Long.parseLong(orderDetailId),COMConstants.ORDER_DISPOSITION_VALIDATED,csrId);

        //request order processing to process order
        MessageToken messageToken = new MessageToken();
        messageToken.setMessage(orderDetailId);
        messageToken.setStatus("ORDER PROCESSING");
        Dispatcher dispatcher = Dispatcher.getInstance();
        dispatcher.dispatchTextMessage(new InitialContext(),messageToken);


    }


    ActionForward actionForward = mapping.findForward("CustomerAccount");
    String path = actionForward.getPath() + "?securitytoken=" + sessionId + "&context=" + context + "&order_detail_id=" +
      orderDetailId + "&action=search" + "&recipient_flag=y" + "&in_order_number=" + this.externalOrderNumber;

    actionForward = new ActionForward(path, true);

    //put forward in map
    pageData.put("XSL", "forward");
    pageData.put("forward", actionForward);
  }

  /*
   * Updates the florist id on an order and then sends order
   */
  private boolean processUpdateFlorist(String floristId)
    throws Exception {
    //call dao to update florist on order
    UpdateOrderDAO updateOrderDAO = new UpdateOrderDAO(con);
    updateOrderDAO.updateFloristIdTempTable(orderDetailId, csrId, floristId);

    //check if florist change and set class variable
    floristChanged = valueChanged(floristId, originalDeliveryInfoVO.getFloristId());


// Ali Lakhani - CONFLICT - the version on the head used to update the Temp tables.

    logger.debug("process update florst.  change flag = " + floristChanged);

    return floristChanged;
  }

  /*******************************************************************************************
   * processOriginalOrder()
   *******************************************************************************************
    * This method is used to call the
    *
    */
  private void processOriginalOrder() throws Exception {
    UpdateOrderDAO upDAO = new UpdateOrderDAO(this.con);

    CachedResultSet rs = (CachedResultSet) upDAO.getOriginalDeliveryInfo(this.orderDetailId);

    if (!rs.next()) {
      throw new Exception("Original Delivery Info does not exist for order detail id " + orderDetailId);
    }

    buildOriginalOrderVO(rs);
  }

  //done

  /*******************************************************************************************
   * processOrginalAddOnsAndNewOrder()
   *******************************************************************************************
    * This method is used to update the order
    *
    */
  private void processOrginalAddOnsAndNewOrder() throws Exception {
    //Instantiate CustomerDAO
    UpdateOrderDAO uoDAO = new UpdateOrderDAO(this.con);

    //hashmap that will contain the output from the stored proc
    HashMap tempOrderInfo = new HashMap();

    //Call getCustomerAcct method in the DAO
    tempOrderInfo = uoDAO.getOrderDetailsUpdate(this.orderDetailId, "DELIVERY_CONFIRMATION", false, this.csrId, null);

    Set ks1 = tempOrderInfo.keySet();
    Iterator iter = ks1.iterator();
    String key = null;

    List addOnVOList = new ArrayList();
    List orderBillVOList = new ArrayList();

    //Create a CachedResultSet object using the cursor name that was passed.
    CachedResultSet rs = null;

    //Iterate thru the keyset
    while (iter.hasNext()) {
      //get the key
      key = iter.next().toString();

      if (key.equalsIgnoreCase("OUT_ORIG_PRODUCT")) {
        //This cursor is not used by Delivery Confirmation
        //rs = (CachedResultSet) tempOrderInfo.get(key);
        //buildTempOrderVO(rs);
      } else if (key.equalsIgnoreCase("OUT_ORIG_ADD_ONS")) {
        rs = (CachedResultSet) tempOrderInfo.get(key);
        buildOriginalAddOnVO(rs);
      } else if (key.equalsIgnoreCase("OUT_UPD_ORDER_CUR")) {
        rs = (CachedResultSet) tempOrderInfo.get(key);

        if (!rs.next()) {
          throw new Exception("Data not found in update table for order detail id " + orderDetailId);
        }

        buildTempOrderVO(rs);
      } else if (key.equalsIgnoreCase("OUT_UPD_ADDON_CUR")) {
        rs = (CachedResultSet) tempOrderInfo.get(key);

        if (rs.next()) {
          buildTempAddOnVO(rs);
        }
      }
    }
  }

  //done

  /*******************************************************************************************
   * buildOriginalOrderVO()
   *******************************************************************************************/
  private void buildOriginalOrderVO(CachedResultSet rs)
    throws Exception {
    //Define the format
    SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");

    /*****************************************************************************************
     * Retrieve/build the DeliveryInfoVO
     *****************************************************************************************/
    /*****************************************************************************************
     * Retrieve/build the OrderBillVO
     *****************************************************************************************/
    this.originalOrderBillVO.setAddOnAmount(getDouble(rs.getObject("add_on_amount")));
    this.originalOrderBillVO.setDiscountAmount(getDouble(rs.getObject("discount_amount")));
    this.originalOrderBillVO.setProductAmount(getDouble(rs.getObject("product_amount")));
    this.originalOrderBillVO.setServiceFee(getDouble(rs.getObject("service_fee")));

    this.originalOrderBillVO.setServiceFeeTax(getDouble(rs.getObject("service_fee_tax")));
    this.originalOrderBillVO.setShippingFee(getDouble(rs.getObject("shipping_fee")));
    this.originalOrderBillVO.setShippingTax(getDouble(rs.getObject("shipping_tax")));
    this.originalOrderBillVO.setTax(getDouble(rs.getObject("tax")));

    /*****************************************************************************************
     * Retrieve/build the OrderContactInfoVO info in the DeliveryInfoVO
     *****************************************************************************************/
    this.originalDeliveryInfoVO.setAltContactEmail((rs.getObject("alt_contact_email") != null)
      ? (String) rs.getObject("alt_contact_email") : null);
    this.originalDeliveryInfoVO.setAltContactExtension((rs.getObject("alt_contact_extension") != null)
      ? (String) rs.getObject("alt_contact_extension") : null);
    this.originalDeliveryInfoVO.setAltContactFirstName((rs.getObject("alt_contact_first_name") != null)
      ? (String) rs.getObject("alt_contact_first_name") : null);
    this.originalDeliveryInfoVO.setAltContactInfoId((rs.getObject("alt_contact_info_id") != null)
      ? rs.getObject("alt_contact_info_id").toString() : null);
    this.originalDeliveryInfoVO.setAltContactLastName((rs.getObject("alt_contact_last_name") != null)
      ? (String) rs.getObject("alt_contact_last_name") : null);
    this.originalDeliveryInfoVO.setAltContactPhoneNumber((rs.getObject("alt_contact_phone_number") != null)
      ? (String) rs.getObject("alt_contact_phone_number") : null);

    /*****************************************************************************************
     * Retrieve/build the MembershipVO info in the DeliveryInfoVO
     *****************************************************************************************/
    this.originalDeliveryInfoVO.setMembershipFirstName((rs.getObject("membership_first_name") != null)
      ? (String) rs.getObject("membership_first_name") : null);
    this.originalDeliveryInfoVO.setMembershipLastName((rs.getObject("membership_last_name") != null)
      ? (String) rs.getObject("membership_last_name") : null);
    this.originalDeliveryInfoVO.setMembershipNumber((rs.getObject("membership_number") != null)
      ? (String) rs.getObject("membership_number") : null);
    this.originalDeliveryInfoVO.setMessageOrderNumber((rs.getObject("message_order_number") != null)
      ? (String) rs.getObject("message_order_number") : null);
    this.originalDeliveryInfoVO.setFtdmIndicator((rs.getObject("message_ftdm_indicator") != null)
      ? (String) rs.getObject("message_ftdm_indicator") : null);

    /*****************************************************************************************
     * Retrieve/build the OrderDetailVO info in the DeliveryInfoVO
     *****************************************************************************************/
    this.originalDeliveryInfoVO.setActualProductAmount(getDouble(rs.getObject("actual_product_amount")));
    this.originalDeliveryInfoVO.setCardMessage((rs.getObject("card_message") != null) ? (String) rs.getObject("card_message") : null);
    this.originalDeliveryInfoVO.setCardSignature((rs.getObject("card_signature") != null)
      ? (String) rs.getObject("card_signature") : null);
    this.originalDeliveryInfoVO.setColor1((rs.getObject("color_1") != null) ? (String) rs.getObject("color_1") : null);
    this.originalDeliveryInfoVO.setColor2((rs.getObject("color_2") != null) ? (String) rs.getObject("color_2") : null);

    //retrieve/format the delivery date
    java.sql.Date deliveryDateSql = null;

    if (rs.getObject("delivery_date") != null) {
      deliveryDateSql = (java.sql.Date) rs.getObject("delivery_date");

      //Create a Calendar Object
      Calendar cDeliveryDate = Calendar.getInstance();

      //Set the Calendar Object
      cDeliveryDate.setTime(new java.util.Date(deliveryDateSql.getTime()));
      this.originalDeliveryInfoVO.setDeliveryDate(cDeliveryDate);
    }

    //retrieve/format the delivery date range end
    java.sql.Date sDeliveryDateRangeEnd = null;

    if (rs.getObject("delivery_date_range_end") != null) {
      sDeliveryDateRangeEnd = (java.sql.Date) rs.getObject("delivery_date_range_end");

      //Create a Calendar Object
      Calendar cDeliveryDateRangeEnd = Calendar.getInstance();

      //Set the Calendar Object
      cDeliveryDateRangeEnd.setTime(new java.util.Date(sDeliveryDateRangeEnd.getTime()));
      this.originalDeliveryInfoVO.setDeliveryDateRangeEnd(cDeliveryDateRangeEnd);
    }

    //retrieve/format the delivery date range end
    java.sql.Date sEODDeliveryDate = null;

    if (rs.getObject("eod_delivery_date") != null) {
      sEODDeliveryDate = (java.sql.Date) rs.getObject("eod_delivery_date");

      //Create a Calendar Object
      Calendar cEODDeliveryDate = Calendar.getInstance();

      //Set the Calendar Object
      cEODDeliveryDate.setTime(new java.util.Date(sEODDeliveryDate.getTime()));
      this.originalDeliveryInfoVO.setEODDeliveryDate(cEODDeliveryDate);
    }

    this.originalDeliveryInfoVO.setEODDeliveryIndicator((rs.getObject("eod_delivery_indicator") != null)
      ? (String) rs.getObject("eod_delivery_indicator") : null);
    this.originalDeliveryInfoVO.setFloristId((rs.getObject("florist_id") != null) ? (String) rs.getObject("florist_id") : null);
    this.originalDeliveryInfoVO.setMilesPoints((rs.getObject("miles_points") != null)
      ? Double.parseDouble((rs.getObject("miles_points").toString())) : 0);
    this.originalDeliveryInfoVO.setOccasion((rs.getObject("occasion") != null) ? (String) rs.getObject("occasion") : null);
    this.originalDeliveryInfoVO.setOrderDetailId((rs.getObject("order_detail_id") != null)
      ? rs.getObject("order_detail_id").toString() : null);
    this.originalDeliveryInfoVO.setOrderGuid((rs.getObject("order_guid") != null) ? (String) rs.getObject("order_guid") : null);
    this.originalDeliveryInfoVO.setProductId((rs.getObject("product_id") != null) ? (String) rs.getObject("product_id") : null);
    this.originalDeliveryInfoVO.setQuantity((rs.getObject("quantity") != null)
      ? Long.parseLong((rs.getObject("quantity").toString())) : 0);
    this.originalDeliveryInfoVO.setReleaseInfoIndicator((rs.getObject("release_info_indicator") != null)
      ? (String) rs.getObject("release_info_indicator") : null);
    this.originalDeliveryInfoVO.setSecondChoiceProduct((rs.getObject("second_choice_product") != null)
      ? (String) rs.getObject("second_choice_product") : null);

    this.originalDeliveryInfoVO.setCustomerFirstName((rs.getObject("customer_first_name") != null)
      ? (String) rs.getObject("customer_first_name") : null);
    this.originalDeliveryInfoVO.setCustomerLastName((rs.getObject("customer_last_name") != null)
      ? (String) rs.getObject("customer_last_name") : null);
    this.originalDeliveryInfoVO.setCompanyId((rs.getObject("company_id") != null) ? (String) rs.getObject("company_id") : null);
    this.originalDeliveryInfoVO.setOccasionDescription((rs.getObject("occasion_description") != null)
      ? (String) rs.getObject("occasion_description") : null);
    this.originalDeliveryInfoVO.setOccasionCategoryIndex((rs.getObject("index_id") != null) ? rs.getObject("index_id").toString()
                                                                                            : null);
    this.originalDeliveryInfoVO.setOriginId((rs.getObject("origin_id") != null) ? (String) rs.getObject("origin_id") : null);

    //retrieve/format the ship date
    java.sql.Date sShipDate = null;

    if (rs.getObject("ship_date") != null) {
      sShipDate = (java.sql.Date) rs.getObject("ship_date");

      //Create a Calendar Object
      Calendar cShipDate = Calendar.getInstance();

      //Set the Calendar Object
      cShipDate.setTime(new java.util.Date(sShipDate.getTime()));
      this.originalDeliveryInfoVO.setShipDate(cShipDate);
    }

    this.originalDeliveryInfoVO.setShipMethod((rs.getObject("ship_method") != null) ? (String) rs.getObject("ship_method") : null);
    this.originalDeliveryInfoVO.setSizeIndicator((rs.getObject("size_indicator") != null)
      ? rs.getObject("size_indicator").toString() : null);
    this.originalDeliveryInfoVO.setSourceCode((rs.getObject("source_code") != null) ? rs.getObject("source_code").toString() : null);
    this.originalDeliveryInfoVO.setSpecialInstructions((rs.getObject("special_instructions") != null)
      ? rs.getObject("special_instructions").toString() : null);
    this.originalDeliveryInfoVO.setSubcode((rs.getObject("subcode") != null) ? (String) rs.getObject("subcode") : null);
    this.originalDeliveryInfoVO.setSubstitutionIndicator((rs.getObject("substitution_indicator") != null)
      ? (String) rs.getObject("substitution_indicator") : null);
    this.originalDeliveryInfoVO.setUpdatedBy((rs.getObject("updated_by") != null) ? (String) rs.getObject("updated_by") : null);

    //retrieve/format the ship date
    java.sql.Date sUpdatedOnDate = null;

    if (rs.getObject("updated_on") != null) {
      sUpdatedOnDate = (java.sql.Date) rs.getObject("updated_on");

      //Create a Calendar Object
      Calendar cUpdatedOnDate = Calendar.getInstance();

      //Set the Calendar Object
      cUpdatedOnDate.setTime(new java.util.Date(sUpdatedOnDate.getTime()));
      this.originalDeliveryInfoVO.setUpdatedOn(cUpdatedOnDate);
    }

    /*****************************************************************************************
     * Retrieve/build the OrderVO info in the DeliveryInfoVO
     *****************************************************************************************/
    this.originalDeliveryInfoVO.setCompanyId((rs.getObject("company_id") != null) ? (String) rs.getObject("company_id") : null);
    this.originalDeliveryInfoVO.setOriginId((rs.getObject("origin_id") != null) ? (String) rs.getObject("origin_id") : null);

    /*****************************************************************************************
     * Retrieve/build the CustomerVO info in the DeliveryInfoVO
     *****************************************************************************************/
    this.originalDeliveryInfoVO.setRecipientAddress1((rs.getObject("recipient_address_1") != null)
      ? (String) rs.getObject("recipient_address_1").toString() : null);
    this.originalDeliveryInfoVO.setRecipientAddress2((rs.getObject("recipient_address_2") != null)
      ? (String) rs.getObject("recipient_address_2").toString() : null);
    this.originalDeliveryInfoVO.setRecipientAddressType((rs.getObject("recipient_address_type") != null)
      ? (String) rs.getObject("recipient_address_type").toString() : null);
    this.originalDeliveryInfoVO.setRecipientBusinessInfo((rs.getObject("recipient_business_info") != null)
      ? (String) rs.getObject("recipient_business_info").toString() : null);
    this.originalDeliveryInfoVO.setRecipientBusinessName((rs.getObject("recipient_business_name") != null)
      ? (String) rs.getObject("recipient_business_name").toString() : null);
    this.originalDeliveryInfoVO.setRecipientCity((rs.getObject("recipient_city") != null)
      ? (String) rs.getObject("recipient_city").toString() : null);
    this.originalDeliveryInfoVO.setRecipientCountry((rs.getObject("recipient_country") != null)
      ? (String) rs.getObject("recipient_country").toString() : null);
    this.originalDeliveryInfoVO.setRecipientFirstName((rs.getObject("recipient_first_name") != null)
      ? (String) rs.getObject("recipient_first_name").toString() : null);
    this.originalDeliveryInfoVO.setRecipientId((rs.getObject("recipient_id") != null) ? rs.getObject("recipient_id").toString()
                                                                                      : null);
    this.originalDeliveryInfoVO.setRecipientLastName((rs.getObject("recipient_last_name") != null)
      ? (String) rs.getObject("recipient_last_name").toString() : null);
    this.originalDeliveryInfoVO.setRecipientState((rs.getObject("recipient_state") != null)
      ? (String) rs.getObject("recipient_state").toString() : null);
    this.originalDeliveryInfoVO.setRecipientZipCode((rs.getObject("recipient_zip_code") != null)
      ? (String) rs.getObject("recipient_zip_code").toString() : null);

    /*****************************************************************************************
     * Retrieve/build the CustomerPhoneVO info in the DeliveryInfoVO
     *****************************************************************************************/
    this.originalDeliveryInfoVO.setRecipientExtension((rs.getObject("recipient_extension") != null)
      ? (String) rs.getObject("recipient_extension").toString() : null);
    this.originalDeliveryInfoVO.setRecipientPhoneNumber((rs.getObject("recipient_phone_number") != null)
      ? (String) rs.getObject("recipient_phone_number").toString() : null);

    /*****************************************************************************************
     * Retrieve/build the remainder fields
     *****************************************************************************************/
    this.originalDeliveryInfoVO.setVendorFlag((rs.getObject("vendor_flag") != null) ? (String) rs.getObject("vendor_flag") : null);
  }

  //done

  /*******************************************************************************************
   * buildOriginalAddOnVO()
   *******************************************************************************************/
  private void buildOriginalAddOnVO(CachedResultSet rs)
    throws Exception {
    //multiple addons can be returned.  therefore, go thru them all
    while (rs.next()) {
      AddOnVO addOnVO = new AddOnVO();

      //set the order detail id
      addOnVO.setOrderDetailId(new Long(this.orderDetailId).longValue());

      //set the updated by id
      addOnVO.setUpdatedBy(this.csrId);

      //retrieve the add on code
      String addOnCode = null;

      if (rs.getObject("add_on_code") != null) {
        addOnCode = (String) rs.getObject("add_on_code");
      }

      addOnVO.setAddOnCode(addOnCode);

      //retrieve the add on quantity
      String addOnQty = null;
      BigDecimal bAddOnQty;
      long lAddOnQty = 0;

      if (rs.getObject("add_on_quantity") != null) {
        bAddOnQty = (BigDecimal) rs.getObject("add_on_quantity");
        addOnQty = bAddOnQty.toString();
        lAddOnQty = Long.valueOf(addOnQty).longValue();
      }

      addOnVO.setAddOnQuantity(lAddOnQty);

      String addOnTypeDesc = null;

      if (rs.getObject("add_on_type_description") != null) {
        addOnTypeDesc = (String) rs.getObject("add_on_type_description");
        addOnTypeDesc = addOnTypeDesc.toUpperCase();
      }

      addOnVO.setCreatedBy(null); //users cannot update
      addOnVO.setCreatedOn(null); //users cannot update
      addOnVO.setUpdatedOn(null); //users cannot update

      //also add it in a hashmap
      if (addOnCode != null) {
        this.originalOrderAddOnVO.put(addOnCode, addOnVO);
      }
    } //end (while rs.next())
  }

  //not-done

  /*******************************************************************************************
   * buildTempOrderVO()
   *******************************************************************************************/
  private void buildTempOrderVO(CachedResultSet rs) throws Exception {
    //Define the format
    SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");

    /*****************************************************************************************
     * Retrieve/build the DeliveryInfoVO
     *****************************************************************************************/
    /*****************************************************************************************
     * Retrieve/build the OrderBillVO
     *****************************************************************************************/
    this.tempOrderBillVO.setAddOnAmount(getDouble(rs.getObject("add_on_amount")));
    this.tempOrderBillVO.setDiscountAmount(getDouble(rs.getObject("discount_amount")));
    this.tempOrderBillVO.setProductAmount(getDouble(rs.getObject("product_amount")));
    this.tempOrderBillVO.setServiceFee(getDouble(rs.getObject("service_fee")));
    this.tempOrderBillVO.setShippingFee(getDouble(rs.getObject("shipping_fee")));
    this.tempOrderBillVO.setShippingTax(getDouble(rs.getObject("shipping_tax")));
    this.tempOrderBillVO.setTax(getDouble(rs.getObject("tax")));

    /*****************************************************************************************
     * Retrieve/build the OrderContactInfoVO info in the DeliveryInfoVO
     *****************************************************************************************/
    this.tempDeliveryInfoVO.setAltContactEmail((rs.getObject("alt_contact_email") != null)
      ? (String) rs.getObject("alt_contact_email") : null);
    this.tempDeliveryInfoVO.setAltContactExtension((rs.getObject("alt_contact_extension") != null)
      ? (String) rs.getObject("alt_contact_extension") : null);
    this.tempDeliveryInfoVO.setAltContactFirstName((rs.getObject("alt_contact_first_name") != null)
      ? (String) rs.getObject("alt_contact_first_name") : null);
    this.tempDeliveryInfoVO.setAltContactInfoId((rs.getObject("alt_contact_info_id") != null)
      ? rs.getObject("alt_contact_info_id").toString() : null);
    this.tempDeliveryInfoVO.setAltContactLastName((rs.getObject("alt_contact_last_name") != null)
      ? (String) rs.getObject("alt_contact_last_name") : null);
    this.tempDeliveryInfoVO.setAltContactPhoneNumber((rs.getObject("alt_contact_phone_number") != null)
      ? (String) rs.getObject("alt_contact_phone_number") : null);

    /*****************************************************************************************
     * Retrieve/build the MembershipVO info in the DeliveryInfoVO
     *****************************************************************************************/
    this.tempDeliveryInfoVO.setMembershipFirstName((rs.getObject("membership_first_name") != null)
      ? (String) rs.getObject("membership_first_name") : null);
    this.tempDeliveryInfoVO.setMembershipLastName((rs.getObject("membership_last_name") != null)
      ? (String) rs.getObject("membership_last_name") : null);
    this.tempDeliveryInfoVO.setMembershipNumber((rs.getObject("membership_number") != null)
      ? (String) rs.getObject("membership_number") : null);

    /*****************************************************************************************
     * Retrieve/build the OrderDetailVO info in the DeliveryInfoVO
     *****************************************************************************************/
    this.tempDeliveryInfoVO.setActualProductAmount(getDouble(rs.getObject("actual_product_amount")));
    this.tempDeliveryInfoVO.setCardMessage((rs.getObject("card_message") != null) ? (String) rs.getObject("card_message") : null);
    this.tempDeliveryInfoVO.setCardSignature((rs.getObject("card_signature") != null) ? (String) rs.getObject("card_signature")
                                                                                      : null);
    this.tempDeliveryInfoVO.setColor1((rs.getObject("color_1") != null) ? (String) rs.getObject("color_1") : null);
    this.tempDeliveryInfoVO.setColor2((rs.getObject("color_2") != null) ? (String) rs.getObject("color_2") : null);

    //retrieve/format the delivery date
    java.sql.Date sDeliveryDate = null;

    if (rs.getObject("delivery_date") != null) {
      sDeliveryDate = (java.sql.Date) rs.getObject("delivery_date");

      //Create a Calendar Object
      Calendar cDeliveryDate = Calendar.getInstance();

      //Set the Calendar Object
      cDeliveryDate.setTime(new java.util.Date(sDeliveryDate.getTime()));
      this.tempDeliveryInfoVO.setDeliveryDate(cDeliveryDate);
    }

    //retrieve/format the delivery date range end
    java.sql.Date sDeliveryDateRangeEnd = null;

    if (rs.getObject("delivery_date_range_end") != null) {
      sDeliveryDateRangeEnd = (java.sql.Date) rs.getObject("delivery_date_range_end");

      //Create a Calendar Object
      Calendar cDeliveryDateRangeEnd = Calendar.getInstance();

      //Set the Calendar Object
      cDeliveryDateRangeEnd.setTime(new java.util.Date(sDeliveryDateRangeEnd.getTime()));
      this.tempDeliveryInfoVO.setDeliveryDateRangeEnd(cDeliveryDateRangeEnd);
    }

    //retrieve/format the delivery date range end
    java.sql.Date sEODDeliveryDate = null;

    if (rs.getObject("eod_delivery_date") != null) {
      sEODDeliveryDate = (java.sql.Date) rs.getObject("eod_delivery_date");

      //Create a Calendar Object
      Calendar cEODDeliveryDate = Calendar.getInstance();

      //Set the Calendar Object
      cEODDeliveryDate.setTime(new java.util.Date(sEODDeliveryDate.getTime()));
      this.tempDeliveryInfoVO.setEODDeliveryDate(cEODDeliveryDate);
    }

    this.tempDeliveryInfoVO.setEODDeliveryIndicator((rs.getObject("eod_delivery_indicator") != null)
      ? (String) rs.getObject("eod_delivery_indicator") : null);
    this.tempDeliveryInfoVO.setFloristId((rs.getObject("florist_id") != null) ? (String) rs.getObject("florist_id") : null);
    this.tempDeliveryInfoVO.setMilesPoints((rs.getObject("miles_points") != null)
      ? Double.parseDouble(rs.getObject("miles_points").toString()) : 0);
    this.tempDeliveryInfoVO.setOccasion((rs.getObject("occasion") != null) ? (String) rs.getObject("occasion") : null);
    this.tempDeliveryInfoVO.setOrderDetailId((rs.getObject("order_detail_id") != null)
      ? rs.getObject("order_detail_id").toString() : null);
    this.tempDeliveryInfoVO.setOrderGuid((rs.getObject("order_guid") != null) ? (String) rs.getObject("order_guid") : null);
    this.tempDeliveryInfoVO.setProductId((rs.getObject("product_id") != null) ? (String) rs.getObject("product_id") : null);
    this.tempDeliveryInfoVO.setQuantity((rs.getObject("quantity") != null) ? Long.parseLong(rs.getObject("quantity").toString()) : 0);
    this.tempDeliveryInfoVO.setReleaseInfoIndicator((rs.getObject("release_info_indicator") != null)
      ? (String) rs.getObject("release_info_indicator") : null);
    this.tempDeliveryInfoVO.setSecondChoiceProduct((rs.getObject("second_choice_product") != null)
      ? (String) rs.getObject("second_choice_product") : null);

    //retrieve/format the ship date
    java.sql.Date sShipDate = null;

    if (rs.getObject("ship_date") != null) {
      sShipDate = (java.sql.Date) rs.getObject("ship_date");

      //Create a Calendar Object
      Calendar cShipDate = Calendar.getInstance();

      //Set the Calendar Object
      cShipDate.setTime(new java.util.Date(sShipDate.getTime()));
      this.tempDeliveryInfoVO.setShipDate(cShipDate);
    }

    this.tempDeliveryInfoVO.setShipMethod((rs.getObject("ship_method") != null) ? (String) rs.getObject("ship_method") : null);
    this.tempDeliveryInfoVO.setSizeIndicator((rs.getObject("size_indicator") != null) ? rs.getObject("size_indicator").toString()
                                                                                      : null);
    this.tempDeliveryInfoVO.setSourceCode((rs.getObject("source_code") != null) ? rs.getObject("source_code").toString() : null);
    this.tempDeliveryInfoVO.setSpecialInstructions((rs.getObject("special_instructions") != null)
      ? rs.getObject("special_instructions").toString() : null);
    this.tempDeliveryInfoVO.setSubcode((rs.getObject("subcode") != null) ? (String) rs.getObject("subcode") : null);
    this.tempDeliveryInfoVO.setSubstitutionIndicator((rs.getObject("substitution_indicator") != null)
      ? (String) rs.getObject("substitution_indicator") : null);
    this.tempDeliveryInfoVO.setUpdatedBy((rs.getObject("updated_by") != null) ? (String) rs.getObject("updated_by") : null);

    //retrieve/format the ship date
    java.sql.Date sUpdatedOnDate = null;

    if (rs.getObject("updated_on") != null) {
      sUpdatedOnDate = (java.sql.Date) rs.getObject("updated_on");

      //Create a Calendar Object
      Calendar cUpdatedOnDate = Calendar.getInstance();

      //Set the Calendar Object
      cUpdatedOnDate.setTime(new java.util.Date(sUpdatedOnDate.getTime()));
      this.tempDeliveryInfoVO.setUpdatedOn(cUpdatedOnDate);
    }

    /*****************************************************************************************
     * Retrieve/build the OrderVO info in the DeliveryInfoVO
     *****************************************************************************************/
    //this.tempDeliveryInfoVO.setCompanyId(rs.getObject("company_id") != null? (String) rs.getObject("company_id"):null);
    //this.tempDeliveryInfoVO.setOriginId(rs.getObject("origin_id") != null? (String) rs.getObject("origin_id"):null);
    this.tempDeliveryInfoVO.setCompanyId(originalDeliveryInfoVO.getCompanyId());
    this.tempDeliveryInfoVO.setOriginId(originalDeliveryInfoVO.getOriginId());
    /*****************************************************************************************
     * Retrieve/build the CustomerVO info in the DeliveryInfoVO
     *****************************************************************************************/
    this.tempDeliveryInfoVO.setRecipientAddress1((rs.getObject("recipient_address_1") != null)
      ? (String) rs.getObject("recipient_address_1").toString() : null);
    this.tempDeliveryInfoVO.setRecipientAddress2((rs.getObject("recipient_address_2") != null)
      ? (String) rs.getObject("recipient_address_2").toString() : null);
    this.tempDeliveryInfoVO.setRecipientAddressType((rs.getObject("recipient_address_type") != null)
      ? (String) rs.getObject("recipient_address_type").toString() : null);
    this.tempDeliveryInfoVO.setRecipientBusinessInfo((rs.getObject("recipient_business_info") != null)
      ? (String) rs.getObject("recipient_business_info").toString() : null);
    this.tempDeliveryInfoVO.setRecipientBusinessName((rs.getObject("recipient_business_name") != null)
      ? (String) rs.getObject("recipient_business_name").toString() : null);
    this.tempDeliveryInfoVO.setRecipientCity((rs.getObject("recipient_city") != null)
      ? (String) rs.getObject("recipient_city").toString() : null);
    this.tempDeliveryInfoVO.setRecipientCountry((rs.getObject("recipient_country") != null)
      ? (String) rs.getObject("recipient_country").toString() : null);
    this.tempDeliveryInfoVO.setRecipientFirstName((rs.getObject("recipient_first_name") != null)
      ? (String) rs.getObject("recipient_first_name").toString() : null);
    this.tempDeliveryInfoVO.setRecipientId((rs.getObject("recipient_id") != null) ? rs.getObject("recipient_id").toString() : null);
    this.tempDeliveryInfoVO.setRecipientLastName((rs.getObject("recipient_last_name") != null)
      ? (String) rs.getObject("recipient_last_name").toString() : null);
    this.tempDeliveryInfoVO.setRecipientState((rs.getObject("recipient_state") != null)
      ? (String) rs.getObject("recipient_state").toString() : null);
    this.tempDeliveryInfoVO.setRecipientZipCode((rs.getObject("recipient_zip_code") != null)
      ? (String) rs.getObject("recipient_zip_code").toString() : null);

    /*****************************************************************************************
     * Retrieve/build the CustomerPhoneVO info in the DeliveryInfoVO
     *****************************************************************************************/
    this.tempDeliveryInfoVO.setRecipientExtension((rs.getObject("recipient_extension") != null)
      ? (String) rs.getObject("recipient_extension").toString() : null);
    this.tempDeliveryInfoVO.setRecipientPhoneNumber((rs.getObject("recipient_phone_number") != null)
      ? (String) rs.getObject("recipient_phone_number").toString() : null);

    /*****************************************************************************************
     * Retrieve/build the remainder fields
     *****************************************************************************************/
    this.tempDeliveryInfoVO.setVendorFlag((rs.getObject("vendor_flag") != null) ? (String) rs.getObject("vendor_flag") : null);
  }

  /**
   *  Get the additionial bills for an order detail record
   * @param orderDetailId
   * @return
   * @throws java.lang.Exception
   */
  private void buildAdditionialBills(Connection conn) throws Exception {
    UpdateOrderDAO updateOrderDAO = new UpdateOrderDAO(conn);
    HashMap billMap = updateOrderDAO.getAdditionialBills(orderDetailId);

    CachedResultSet origRS = (CachedResultSet) billMap.get("OUT_ORIGINAL_ADD_BILLS");
    CachedResultSet tempRS = (CachedResultSet) billMap.get("OUT_UPDATE_ADD_BILLS");

    while ((origRS != null) && origRS.next()) {
      OrderBillVO billVO = new OrderBillVO();
      billVO.setAdditionalBillIndicator((String) origRS.getObject("additional_bill_indicator"));
      billVO.setAddOnAmount(getDouble(origRS.getObject("add_on_amount")));
      billVO.setCommissionAmount(getDouble(origRS.getObject("commission_amount")));
      billVO.setDiscountAmount(getDouble(origRS.getObject("discount_amount")));
      billVO.setOrderBillId(origRS.getObject("order_bill_id").toString());
      billVO.setProductAmount(getDouble(origRS.getObject("product_amount")));
      billVO.setServiceFee(getDouble(origRS.getObject("service_fee")));
      billVO.setShippingFee(getDouble(origRS.getObject("shipping_fee")));
      billVO.setShippingTax(getDouble(origRS.getObject("shipping_tax")));
      billVO.setTax(getDouble(origRS.getObject("tax")));

      origAddBills.add(billVO);
    }

    while ((tempRS != null) && tempRS.next()) {
      OrderBillVO billVO = new OrderBillVO();
      billVO.setAdditionalBillIndicator((String) origRS.getObject("additional_bill_indicator"));
      billVO.setAddOnAmount(getDouble(origRS.getObject("add_on_amount")));
      billVO.setCommissionAmount(getDouble(origRS.getObject("commission_amount")));
      billVO.setDiscountAmount(getDouble(origRS.getObject("discount_amount")));
      billVO.setOrderBillId((String) origRS.getObject("order_bill_id"));
      billVO.setProductAmount(getDouble(origRS.getObject("product_amount")));
      billVO.setServiceFee(getDouble(origRS.getObject("service_fee")));
      billVO.setShippingFee(getDouble(origRS.getObject("shipping_fee")));
      billVO.setShippingTax(getDouble(origRS.getObject("shipping_tax")));
      billVO.setTax(getDouble(origRS.getObject("tax")));

      tempAddBills.add(billVO);
    }
  }

  /*******************************************************************************************
   * buildTempAddOnVO()
   *******************************************************************************************/
  private void buildTempAddOnVO(CachedResultSet rs) throws Exception {
    //multiple addons can be returned.  therefore, go thru them all
    do {
      AddOnVO addOnVO = new AddOnVO();

      //set the order detail id
      addOnVO.setOrderDetailId(new Long(this.orderDetailId).longValue());

      //set the updated by id
      addOnVO.setUpdatedBy(this.csrId);

      //retrieve the add on code
      String addOnCode = null;

      if (rs.getObject("add_on_code") != null) {
        addOnCode = (String) rs.getObject("add_on_code");
      }

      addOnVO.setAddOnCode(addOnCode);

      //retrieve the add on id
      //String      addOnId = null;
      //BigDecimal  bAddOnId;
      //long        lAddOnId = 0;
      //if (rs.getObject("order_add_on_id") != null)
      //{
      // bAddOnId = (BigDecimal)rs.getObject("order_add_on_id");
      //  addOnId = bAddOnId.toString();
      //  lAddOnId = Long.valueOf(addOnId).longValue();
      //}
      //addOnVO.setAddOnId(lAddOnId);
      //retrieve the add on quantity
      String addOnQty = null;
      BigDecimal bAddOnQty;
      long lAddOnQty = 0;

      if (rs.getObject("add_on_quantity") != null) {
        bAddOnQty = (BigDecimal) rs.getObject("add_on_quantity");
        addOnQty = bAddOnQty.toString();
        lAddOnQty = Long.valueOf(addOnQty).longValue();
      }

      addOnVO.setAddOnQuantity(lAddOnQty);

      //String addOnTypeDesc = null;
      //if (rs.getObject("add_on_type_description") != null)
      //{
      //  addOnTypeDesc = (String)rs.getObject("add_on_type_description");
      //  addOnTypeDesc = addOnTypeDesc.toUpperCase();
      //}
      addOnVO.setCreatedBy(null); //users cannot update
      addOnVO.setCreatedOn(null); //users cannot update
      addOnVO.setUpdatedOn(null); //users cannot update

      //also add it in a hashmap
      if (addOnCode != null) {
        this.tempOrderAddOnVO.put(addOnCode, addOnVO);
      }
    } while (rs.next()); //end (while rs.next())
  }

  /*
   * Check if the passed in delivery types are of the same type (both vendor, or both florist)

   private boolean xdeliveryTypesEqual(String methodA, String methodB)  throws Exception
   {
    boolean equal = false;

     if( (isFloristDelivered(methodA) && isFloristDelivered(methodB)) ||
      (!isFloristDelivered(methodA) && !isFloristDelivered(methodB)))
      {
        equal = true;
      }

    return equal;
   }*/
  /*
   * Check is the passed in ship method is for a florist or vendor delivered item

   private boolean xisFloristDelivered(String shipMethod) throws Exception
   {
    boolean floristDelivered = false;

     if(shipMethod != null && shipMethod.equals("SD"))
     {
       floristDelivered = true;
     }
     else if((shipMethod !=null) && shipMethod.equals("ND") ||
          shipMethod.equals("2F") ||
          shipMethod.equals("GR") ||
          shipMethod.equals("SA"))
          {
            floristDelivered =  false;
          }
    else
    {
      throw new Exception("Unknown ship method:" + shipMethod);
    }


     return floristDelivered;

   }*/
  private double getDouble(Object obj) throws Exception {
    double retValue = 0;

    try {
      retValue = (obj == null) ? 0 : Double.parseDouble(obj.toString());
    } catch (Exception e) {
      throw new Exception("Could not parse value into a double. Value=" + obj.toString());
    }

    return retValue;
  }

  /*******************************************************************************************
   * buildXML()
   *******************************************************************************************/
  private Document buildXML(HashMap outMap, String cursorName, String topName, String bottomName, String type)
    throws Exception {
    //Create a CachedResultSet object using the cursor name that was passed.
    CachedResultSet rs = (CachedResultSet) outMap.get(cursorName);

    Document doc = null;

    //Create an XMLFormat, and initialize the top and bottom node.  Note that since this method
    //can be/is called by various other methods, the top and botton names MUST be passed to it.
    XMLFormat xmlFormat = new XMLFormat();
    xmlFormat.setAttribute("type", type);
    xmlFormat.setAttribute("top", topName);
    xmlFormat.setAttribute("bottom", bottomName);

    //call the DOMUtil's converToXMLDOM method
    doc = (Document) DOMUtil.convertToXMLDOM(rs, xmlFormat);

    //return the xml document.
    return doc;
  }

private ArrayList validateDeliveryDateAndInfo(DeliveryInfoVO divo,boolean floristChanged, boolean liveMercury) throws Exception {

  Validator validator;
  Map validationResults;
  boolean validationSuccess = true;  // Assume success
  ValidationConfiguration validationConfiguration = new ValidationConfiguration();
  validationConfiguration.setValidationLevel( ValidationLevels.INFO );

  ArrayList errorList = new ArrayList();

     String yesAction = "";
     String noAction = "";
     boolean displayYes = false;
     boolean displayNo = false;
     boolean displayOk = true;


    //make sure there is a delivery date on the order
    if(divo.getDeliveryDate() == null)
    {
        throw new Exception("Delivery Date is null");
    }

    //Certain validations should be skipped if the florist was not changed from the original order
    boolean doSameFloristValidation = true;
    if(!floristChanged && liveMercury)
    {
        doSameFloristValidation = false;
    }


    //check if recipient is domestic
    boolean domesticFlag = sUtil.isDomestic(tempDeliveryInfoVO.getRecipientCountry());


  //------------
  //
  // Validate delivery date
  //
  validator = DeliveryDateValidatorFactory.getInstance().getValidator(validationConfiguration, divo, this.con);
  if ( !validator.validate() ) {
	validationResults = validator.getValidationResults();

	// florist validation results
	if ( validationResults.containsKey(ValidationRules.SUNDAY_DELIVERY_NOT_ALLOWED_FOR_EXOTIC) ) {
	  errorList.add(COMConstants.SUNDAY_DELIVERY_NOT_ALLOWED_FOR_EXOTIC);
      validationSuccess = false;
	}
	if ( validationResults.containsKey(ValidationRules.SAME_DAY_EXOTIC_UNAVAILABLE) ) {
	  errorList.add(COMConstants.SAME_DAY_EXOTIC_UNAVAILABLE);
      validationSuccess = false;
	}
	if ( validationResults.containsKey(ValidationRules.SAME_DAY_DELIVERY_FOR_INTERNATIONAL_PRODUCTS) ) {
	  errorList.add(COMConstants.SAME_DAY_DELIVERY_FOR_INTERNATIONAL_PRODUCTS);
      validationSuccess = false;
	}
	if ( validationResults.containsKey(ValidationRules.PRODUCT_DELIVERY_DATE_RESTRICTIONS) ) {

        //retrieve exception dates
        UpdateOrderDAO dao = new UpdateOrderDAO(con);
        CachedResultSet crs = dao.getProductInfo(divo.getProductId());

        String message = null;
        if (crs.next()) {
          Date exceptionStartDate = crs.getDate("exceptionStartDate");
          Date exceptionEndDate = crs.getDate("exceptionEndDate");

          if(exceptionStartDate == null || exceptionEndDate == null)
          {
            logger.info("Product date restrictions not found in DB. Order Detail=" + divo.getOrderDetailId() + "Product=" + divo.getProductId());
            message = COMConstants.PRODUCT_DELIVERY_DATE_RESTRICTIONS;
          }
          else
          {
              SimpleDateFormat sdf = new SimpleDateFormat ("MM/dd/yyyy");
              String formattedExceptionStart = sdf.format(exceptionStartDate).toString();
              String formattedExceptionEnd = sdf.format(exceptionEndDate).toString();

              message = COMConstants.PRODUCT_DELIVERY_DATE_RESTRICTIONS_WITH_DATE1 +
                             formattedExceptionStart +
                             COMConstants.PRODUCT_DELIVERY_DATE_RESTRICTIONS_WITH_DATE2 +
                             formattedExceptionEnd +
                             COMConstants.PRODUCT_DELIVERY_DATE_RESTRICTIONS_WITH_DATE3;

          }

        }
        else
        {
            logger.info("Product not found in DB. Order Detail=" + divo.getOrderDetailId() + "Product=" + divo.getProductId());
            message = COMConstants.PRODUCT_DELIVERY_DATE_RESTRICTIONS;
        }

        errorList.add(message);
        validationSuccess = false;
	}

	if ( doSameFloristValidation && validationResults.containsKey(ValidationRules.ZIP_IN_GNADD) ) {
	  errorList.add(COMConstants.ZIP_IN_GNADD + COMConstants.CORRECTIVE_ACTION_CONFIRMATION);

             yesAction = "florist_lookup";
             noAction = "";
             displayYes = false;
             displayNo = false;
             displayOk = true;

             validationSuccess = false;

	}
	// vendor validation validationResults
	if ( validationResults.containsKey(ValidationRules.PRODUCT_STATE_AGRICULTURAL_RESTRICATIONS_FOR_DELIVERY_DAY_SELECTED) ) {
	  errorList.add(COMConstants.PRODUCT_STATE_AGRICULTURAL_RESTRICATIONS);
      validationSuccess = false;
	}
	// common florist and vendor validation results
	if ( validationResults.containsKey(ValidationRules.SAME_DAY_CUTOFF_EXPIRED) ) {
	  errorList.add(COMConstants.SAME_DAY_CUTOFF_EXPIRED);
      validationSuccess = false;
	}
    //make sure product can be delivered
//	if (validationResults.containsKey(ValidationRules.DELIVERY_OPTION_NOT_ALLOWED)) {
//	  errorList.add(COMConstants.PRODUCT_CANNOT_BE_DELIVERED);
//	}

//	if (validationResults.containsKey(ValidationRules.DELIVERY_SHIP_METHOD_NOT_ALLOWED)) {
//	  errorList.add(COMConstants.DELIVERY_SHIP_METHOD_NOT_ALLOWED);
//	}

  }


  //------------
  //
  // Validate vendor delivered info
  //
  if ( validationSuccess && StringUtils.equals(divo.getVendorFlag(), "Y") ) {

	VendorDeliveredValidator vendorValidator = new VendorDeliveredValidator(divo, con);
	vendorValidator.setValidationConfiguration(validationConfiguration);
	vendorValidator.validate();
	validationResults = vendorValidator.getValidationResults();

	// Check if internal error
	//
	if (validationResults.containsKey(ValidationRules.PRE_VALIDATION_ERROR)) {
	  throw new Exception("Application software error - DeliveryInfoVO not setup properly for validation");
	}

	if (validationResults.containsKey(ValidationRules.DROP_SHIP_NOT_AVAILABLE)) {
	  errorList.add(COMConstants.DROP_SHIP_NOT_AVAILABLE);
	  validationSuccess = false;
	}








	// Check if domestic order
	//
	if (validationResults.containsKey(ValidationRules.DROP_SHIP_PRODUCT_DELIVERY_RESTRICTIONS)) {
	  errorList.add(COMConstants.DROP_SHIP_PRODUCT_DELIVERY_RESTRICTIONS);
	  validationSuccess = false;
	}

	// Check if APO or PO address found
	//
	if (validationResults.containsKey(ValidationRules.RECIP_PO_NOT_ALLOWED_ADDRESS_ONE)) {
	  errorList.add(COMConstants.RECIP_PO_NOT_ALLOWED_ADDRESS_ONE);
	  validationSuccess = false;
	}
	if (validationResults.containsKey(ValidationRules.RECIP_PO_NOT_ALLOWED_ADDRESS_TWO)) {
	  errorList.add(COMConstants.RECIP_PO_NOT_ALLOWED_ADDRESS_TWO);
	  validationSuccess = false;
	}



	// Check product availability
	//
	if (validationResults.containsKey(ValidationRules.PRODUCT_UNAVAILABLE)) {
	  errorList.add(COMConstants.PRODUCT_UNAVAILABLE);
	  validationSuccess = false;
	}

	// Check if order already printed
	//
	if (validationResults.containsKey(ValidationRules.ORDER_ALREADY_PRINTED)) {
	  errorList.add(COMConstants.ORDER_ALREADY_PRINTED);
	  validationSuccess = false;

      yesAction = "updateDeliveryConfirmation.do?action=validate&skip_validate=Y";
     displayYes = false;
     displayNo = false;
     displayOk = true;
	}


  //------------
  //
  // Validate florist delivered info
  //
  } else if (validationSuccess) {

	FloristDeliveredValidator floristValidator = new FloristDeliveredValidator(divo, con);
	floristValidator.setValidationConfiguration(validationConfiguration);
	floristValidator.validate();
	validationResults = floristValidator.getValidationResults();

	// Check if internal error
	//
	if (validationResults.containsKey(ValidationRules.PRE_VALIDATION_ERROR)) {
	  throw new Exception("Application software error - DeliveryInfoVO not setup properly for validation");
	}



	// If no live Mercury order (i.e., FTD Mercury order has been cancelled or rejected)
	// then return with error
	//

	if (validationResults.containsKey(ValidationRules.NO_LIVE_MERCURY)) {
	  errorList.add(COMConstants.NO_LIVE_MERCURY + " " + COMConstants.NEW_FLORIST_REQUIRED);
	  validationSuccess = false;

        //check if there are any available florists
        if(otherFloristCanDeliver(divo,validationResults))
        {
             yesAction = "florist_lookup";
             noAction = "";
             displayYes = false;
             displayNo = false;
             displayOk = true;
        }
        else
        {
              // check for this error since it could already be in the list //
              if (!errorList.contains(COMConstants.NO_FLORISTS_AVAILABLE))
                errorList.add(COMConstants.NO_FLORISTS_AVAILABLE);
        }




    //check if florist is assigned to order
  } else if(divo.getFloristId() == null || divo.getFloristId().length() == 0)
    {
        validationSuccess = false;

        errorList.add(COMConstants.NO_FLORIST_ENTERED + " " + COMConstants.NEW_FLORIST_REQUIRED);

         yesAction = "florist_lookup";
         noAction = "";
         displayYes = false;
         displayNo = false;
         displayOk = true;
    }



    //get the product type
    UpdateOrderDAO updateOrderDAO = new UpdateOrderDAO(con);
    CachedResultSet crs = updateOrderDAO.getProductInfo(tempDeliveryInfoVO.getProductId());
    String productType = null;
    if (crs.next()) {
      productType = crs.getString("productType");
    }

    //if orig delivery method is florist delivered and current product is same day gift
    if( (originalDeliveryInfoVO.getVendorFlag() == null || originalDeliveryInfoVO.getVendorFlag().equals("N"))
        && productType != null && (productType.equals("SDG") || productType.equals("SDFC")))
        {
            //if new delivery method is still florist delivered
            if(tempDeliveryInfoVO.getVendorFlag() == null || tempDeliveryInfoVO.getVendorFlag().equals("N"))
            {
                //check if florist can delivery
                if (validationSuccess && validationResults.containsKey(ValidationRules.FLORIST_DELIVERY_NOT_AVAILABLE)) {

                    //florist cannot deliver

                    //if a live mercury does not exist
                    if(validationResults.containsKey(ValidationRules.NO_LIVE_MERCURY))
                    {
                          errorList.add(COMConstants.CURRENT_DELIVERY_METHOD_NO_LONGER_AVAILABLE);
                          validationSuccess = false;
                    }
                    else
                    {
                         //has live mercury
                         //skip validation
                    }

                }
                else
                {
                    //florist can still deliver, no error message needed
                }
            }
        }
        else
        {
            //validate florist can deliver product
            if (validationSuccess && validationResults.containsKey(ValidationRules.FLORIST_DELIVERY_NOT_AVAILABLE)) {
              errorList.add(COMConstants.FLORIST_DELIVERY_NOT_AVAILABLE);
              validationSuccess = false;
            }
        }



	// If invalid florist there's no point in continuing so bail out
	//
	if (validationSuccess && validationResults.containsKey(ValidationRules.INVALID_FLORIST_NUMBER)) {
	  errorList.add(COMConstants.INVALID_FLORIST_NUMBER);
	  validationSuccess = false;
	}

  // If international order and no florist, then bail
  //
  if (validationSuccess && !domesticFlag &&
      !validationResults.containsKey(ValidationRules.OK_FOR_ZIP_OR_CITY)) {
	  errorList.add(COMConstants.INTERNATIONAL_ORDER_NOT_SUPPORTED);
	  validationSuccess = false;
  }

	// Check if florist invalid, suspended or blocked
	if (validationSuccess &&
		(!validationResults.containsKey(ValidationRules.FLORIST_ACTIVE) ||
		validationResults.containsKey(ValidationRules.FLORIST_SUSPENDED))) {

        //check if this validation is a show stopper
        if(doSameFloristValidation)
        {
          //display an error message and do let user continue
          if(otherFloristCanDeliver(divo,validationResults))
          {
              errorList.add(COMConstants.FLORIST_UNAVAILABLE);
              validationSuccess = false;
          }
          else
          {
              // check for this error since it could already be in the list //
              if (!errorList.contains(COMConstants.NO_FLORISTS_AVAILABLE))
                errorList.add(COMConstants.NO_FLORISTS_AVAILABLE);
              validationSuccess = false;
          }
        }//end do same florist validation
        else
        {
            //display an error message, but let user continue
             errorList.add(COMConstants.NO_FLORISTS_AVAILABLE + "  " + COMConstants.KEEP_ORDER_WITH_FLORIST);
             validationSuccess = false;
             yesAction = "updateDeliveryConfirmation.do?action=validate&skip_validate=Y";
             noAction = "florist_lookup";
             displayYes = true;
             displayNo = true;
             displayOk = false;
        }
	}




	// Check if florist covers zip
	//
	if (validationSuccess && !validationResults.containsKey(ValidationRules.OK_FOR_ZIP_OR_CITY)) {

	  validationSuccess = false;

          if(!doSameFloristValidation)
          {
             if(otherFloristCanDeliver(divo,validationResults))
             {
                //If we get here it means we should allow the user to continue with
                //the current florist if they want.
                 errorList.add(COMConstants.FLORIST_CANT_DELIVER_TO_ZIP_OR_CITY + "  " + COMConstants.KEEP_ORDER_WITH_FLORIST);
                 validationSuccess = false;
                 yesAction = "updateDeliveryConfirmation.do?action=validate&skip_validate=Y";
                 noAction = "florist_lookup";
                 displayYes = true;
                 displayNo = true;
                 displayOk = false;
             }
             else
             {
                //No other florists are available.
                 errorList.add(COMConstants.NO_FLORISTS_AVAILABLE + "  " + COMConstants.KEEP_ORDER_WITH_FLORIST);
                 validationSuccess = false;
                 yesAction = "updateDeliveryConfirmation.do?action=validate&skip_validate=Y";
                 noAction = "";
                 displayYes = true;
                 displayNo = true;
                 displayOk = false;
             }

          }
          else if(otherFloristCanDeliver(divo,validationResults))
          {
        	  errorList.add(COMConstants.FLORIST_CANT_DELIVER_TO_ZIP_OR_CITY + " " + CONTINUE_MSG);

              yesAction = "florist_lookup";
              noAction = "";
              displayYes = true;
              displayNo = true;
              displayOk = false;

          }
          else
          {
              // check for this error since it could already be in the list //
              if (!errorList.contains(COMConstants.NO_FLORISTS_AVAILABLE))
                errorList.add(COMConstants.NO_FLORISTS_AVAILABLE);
          }



	}

	if (validationSuccess) {
	  // Check Sunday delivery flags if delivery is for Sunday

	  if (divo.getDeliveryDate().get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY &&
		  !validationResults.containsKey(ValidationRules.FLORIST_OK_FOR_SUNDAY)) {

//Ali Lakhani - CONFLICT
		validationSuccess = false;

         if(!doSameFloristValidation)
         {
        		errorList.add(COMConstants.FLORIST_DOESNT_DELIVER_ON_SUNDAY + "  " + COMConstants.KEEP_ORDER_WITH_FLORIST);

                yesAction = "updateDeliveryConfirmation.do?action=validate&skip_validate=Y";
                noAction = "florist_lookup";
                displayYes = true;
                displayNo = true;
                displayOk = false;
                validationSuccess = false;
         }
         else if(otherFloristCanDeliver(divo,validationResults))
          {
        		errorList.add(COMConstants.FLORIST_DOESNT_DELIVER_ON_SUNDAY);
                validationSuccess = false;


          }
          else
          {
              // check for this error since it could already be in the list //
              if (!errorList.contains(COMConstants.NO_FLORISTS_AVAILABLE))
                errorList.add(COMConstants.NO_FLORISTS_AVAILABLE);
          }



	  }

	  // Check codification flag
	  //
	  if (!validationResults.containsKey(ValidationRules.OK_FOR_CODIFICATION)) {

		validationSuccess = false;


          if(!doSameFloristValidation)
          {
            //If we get here it means we should allow the user to continue with
            //the current florist if they want.
             errorList.add(COMConstants.ASSIGNED_FLORIST_CANNOT_DELIVER_PRODUCT + "  " + COMConstants.KEEP_ORDER_WITH_FLORIST);
             validationSuccess = false;
             yesAction = "updateDeliveryConfirmation.do?action=validate&skip_validate=Y";
             noAction = "florist_lookup";
             displayYes = true;
             displayNo = true;
             displayOk = false;

          }
          else if(otherFloristCanDeliver(divo,validationResults))
          {
            errorList.add(COMConstants.ASSIGNED_FLORIST_CANNOT_DELIVER_PRODUCT + " " + CONTINUE_MSG);
             yesAction = "florist_lookup";
             noAction = "";
             displayYes = true;
             displayNo = true;
             displayOk = false;

          }
          else
          {
              // check for this error since it could already be in the list //
              if (!errorList.contains(COMConstants.NO_FLORISTS_AVAILABLE))
                errorList.add(COMConstants.NO_FLORISTS_AVAILABLE);
          }



	  }
	}
  }

    //put all the errors in a list
    ArrayList errorVOList = new ArrayList();
    Iterator iter = errorList.iterator();
    while(iter.hasNext())
    {
        //display florist validation message
        ErrorMessageVO errorVO = new ErrorMessageVO();
        errorVO.setMessage((String)iter.next());
        errorVO.setDisplayYes(displayYes);
        errorVO.setYesAction(yesAction);
        errorVO.setDisplayNo(displayNo);
        errorVO.setNoAction(noAction);
        errorVO.setDisplayOk(displayOk);
        errorVOList.add(errorVO);

    }//end while


  return errorVOList;
}

  private boolean otherFloristCanDeliver(DeliveryInfoVO divo, Map validationResults) {
    boolean otherFloristsAvaiable = true;

    // First determine if sunday delivery
    //
    boolean sundayFlag = false;
    Calendar newDate = divo.getDeliveryDate();

    if (newDate != null) {
      int newDayOfWeek = newDate.get(Calendar.DAY_OF_WEEK);

      if (newDayOfWeek == Calendar.SUNDAY) {
        sundayFlag = true;
      }
    }

    // Check if any other florists active and can handle zip or city
    //
    if (!validationResults.containsKey(ValidationRules.OTHER_FLORISTS_OK_FOR_ZIP_OR_CITY) ||
        !validationResults.containsKey(ValidationRules.OTHER_FLORISTS_ACTIVE)) {
      otherFloristsAvaiable = false;
    }

    if (otherFloristsAvaiable) {
      // Check if any other florists are codified for product.  Note that OK_FOR_CODIFIECATION
      // is set only if there are NO codification blocks OR product is NOT codified.
      //
      if (!validationResults.containsKey(ValidationRules.OTHER_FLORISTS_OK_FOR_CODIFICATION)) {
        otherFloristsAvaiable = false;
      }

      // Check if any other florists allow sunday delivery (if necessary)
      //
      if (sundayFlag) {
        if (!validationResults.containsKey(ValidationRules.OTHER_FLORISTS_OK_FOR_SUNDAY)) {
          otherFloristsAvaiable = false;
        }
      }
    }

    if (otherFloristsAvaiable) {
      // If product is codified AND sunday delivery requested, need to ensure
      // a florist can do both.  Note we need to do this since above checks
      // only determine if florists support codified product OR sunday delivery.
      //
      if (sundayFlag && validationResults.containsKey(ValidationRules.PRODUCT_IS_CODIFIED)) {
        if (!validationResults.containsKey(ValidationRules.OTHER_FLORISTS_OK_FOR_CODIFICATION_AND_SUN)) {
          otherFloristsAvaiable = false;
        }
      }
    }

    return otherFloristsAvaiable;
  }

  private String removeNonNumerics(String input) {
    String output = "";

    if (input == null) {
      return output;
    }

    for (int i = 0; i < input.length(); i++) {
      char n = input.charAt(i);
      Character nextCharacter = new Character(n);

      if (nextCharacter.isDigit(n)) {
        output = output + nextCharacter.toString();
      }
    }

    return output;
  }

  private void applyA40Refund(Connection conn) throws NamingException, Exception {

    UpdateOrderDAO updateOrderDAO = new UpdateOrderDAO(conn);

    AcctTransVO refundAtVO = new AcctTransVO();
    AcctTransVO newOrderAtVO = new AcctTransVO();

    UserTransaction userTransaction = null;
    // get the initial context
    InitialContext iContext = new InitialContext();

    try
    {
      //get the transaction timeout
      int transactionTimeout = Integer.parseInt((String)ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, COMConstants.TRANSACTION_TIMEOUT_IN_SECONDS));
      // Retrieve the UserTransaction object
      String jndi_usertransaction_entry = ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, COMConstants.JNDI_USERTRANSACTION_ENTRY);
      userTransaction = (UserTransaction)  iContext.lookup(jndi_usertransaction_entry);
      logger.debug("User transaction obtained");
      userTransaction.setTransactionTimeout(transactionTimeout);
      logger.debug("Transaction timeout set at " + transactionTimeout + " seconds");

      // Start the transaction with the begin method
      userTransaction.begin();
      int status = userTransaction.getStatus();
      logger.debug("userTransStatus = " + status);

			HashMap lastPaymentMap = updateOrderDAO.getLastPaymentUsed(orderDetailId);
			String lastPaymentMethod = (String)lastPaymentMap.get(COMConstants.HM_PAYMENT_TYPE);

      //Populate a refundAtVO
      //apply an A40 refund
      refundAtVO.setProductAmount(originalOrderBillVO.getProductAmount());
      refundAtVO.setAddOnAmount(originalOrderBillVO.getAddOnAmount());
      refundAtVO.setShippingFee(originalOrderBillVO.getShippingFee());
      refundAtVO.setServiceFee(originalOrderBillVO.getServiceFee());
      refundAtVO.setDiscountAmount(originalOrderBillVO.getDiscountAmount());
      refundAtVO.setShippingTax(originalOrderBillVO.getShippingTax());
      refundAtVO.setServiceFeeTax(originalOrderBillVO.getServiceFeeTax());
      refundAtVO.setTax(originalOrderBillVO.getTax());

      /* These fields are only used for Wal-Mart and Amazon orders
       * which we set to zero since they cannot be modified
       */
      refundAtVO.setCommissionAmount(0.0);
      refundAtVO.setWholesaleAmount(0.0);
      refundAtVO.setAdminFee(0.0);
      refundAtVO.setWholesaleServiceFee(0.0);

      refundAtVO.setDeliveryDate(originalDeliveryInfoVO.getDeliveryDate().getTime());
      refundAtVO.setProductId(originalDeliveryInfoVO.getProductId());
      refundAtVO.setSourceCode(originalDeliveryInfoVO.getSourceCode());
      refundAtVO.setShipMethod(originalDeliveryInfoVO.getShipMethod());
      refundAtVO.setTranType("Refund");
      refundAtVO.setTranDate(new Date());
      refundAtVO.setOrderDetailId(orderDetailId);
      //carlo creating stored proc to return to me
      //the last payment method on the order
      refundAtVO.setPaymentType(lastPaymentMethod);
      refundAtVO.setRefundDispCode("A40");
      refundAtVO.setOrderBillId(originalOrderBillVO.getOrderBillId());

      refundAtVO.setNewOrderSeq("N");

      //Populate a newOrderAtVO
      //apply a new order transaction
      newOrderAtVO.setProductAmount(tempOrderBillVO.getProductAmount());
      newOrderAtVO.setAddOnAmount(tempOrderBillVO.getAddOnAmount());
      newOrderAtVO.setShippingFee(tempOrderBillVO.getShippingFee());
      newOrderAtVO.setServiceFee(tempOrderBillVO.getServiceFee());
      newOrderAtVO.setDiscountAmount(tempOrderBillVO.getDiscountAmount());
      newOrderAtVO.setShippingTax(tempOrderBillVO.getShippingTax());
      newOrderAtVO.setServiceFeeTax(tempOrderBillVO.getServiceFeeTax());
      newOrderAtVO.setTax(tempOrderBillVO.getTax());

      /* These fields are only used for Wal-Mart and Amazon orders
       * which we set to zero since they cannot be modified
       */
      newOrderAtVO.setCommissionAmount(0.0);
      newOrderAtVO.setWholesaleAmount(0.0);
      newOrderAtVO.setAdminFee(0.0);
      newOrderAtVO.setWholesaleServiceFee(0.0);

      newOrderAtVO.setDeliveryDate(tempDeliveryInfoVO.getDeliveryDate().getTime());
      newOrderAtVO.setProductId(tempDeliveryInfoVO.getProductId());
      newOrderAtVO.setSourceCode(tempDeliveryInfoVO.getSourceCode());
      newOrderAtVO.setShipMethod(tempDeliveryInfoVO.getShipMethod());

      newOrderAtVO.setTranType("Order");
      newOrderAtVO.setTranDate(new Date());
      newOrderAtVO.setOrderDetailId(orderDetailId);
      //carlo creating stored proc to return to me
      //the last payment method on the order
      newOrderAtVO.setPaymentType(lastPaymentMethod);
      newOrderAtVO.setRefundDispCode("");
      newOrderAtVO.setOrderBillId(originalOrderBillVO.getOrderBillId());

      refundAtVO.setNewOrderSeq("Y");

      //insert the accounting transactions
      AcctTransDAO atDAO = new AcctTransDAO(conn);
      atDAO.insertAcctTransMO(refundAtVO);
      atDAO.insertAcctTransMO(newOrderAtVO);

      //Assuming everything went well, commit the transaction.
      logger.debug("userTransaction = " + userTransaction);
      logger.debug("userTransaction getClass = " + userTransaction.getClass());
      logger.debug("userTransaction getStatus = " + userTransaction.getStatus());

      userTransaction.commit();
      }
      catch(Throwable tr)
      {
        logger.error(tr);
        rollback(userTransaction);
      }
            finally
        {
          try
          {
            // close initial context
            if(iContext != null)
            {
              iContext.close();
            }
          }
          catch (Exception ex)
          {
            logger.error(ex);
          }
        }

      logger.debug("END TRANSACTION");

}//end applyA40Refund

 /**
  * Rollback user transaction when exceptions occur
  * @param userTransaction - user transaction
  * @return void
  * @throws none
  */

  private void rollback(UserTransaction userTransaction)
  {
     try
     {
       if (userTransaction != null)
       {
         // rollback the user transaction
         userTransaction.rollback();
         logger.info("User transaction rolled back");
       }
     }
     catch (Exception ex)
     {
       logger.error(ex);
     }
  }




}
