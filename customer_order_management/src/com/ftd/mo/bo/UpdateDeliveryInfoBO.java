package com.ftd.mo.bo;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.Closure;
import org.apache.commons.collections.Transformer;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.dao.LockDAO;
import com.ftd.customerordermanagement.dao.OrderDAO;
import com.ftd.customerordermanagement.util.BasePageBuilder;
import com.ftd.customerordermanagement.util.functors.DateFormatTransformer;
import com.ftd.customerordermanagement.util.functors.NodeUpdateClosure;
import com.ftd.customerordermanagement.util.functors.ResultSetToXmlTransformer;
import com.ftd.customerordermanagement.vo.OrderBillVO;
import com.ftd.ftdutilities.FTDCommonUtils;
import com.ftd.ftdutilities.ValidateMembership;
import com.ftd.mo.dao.UpdateOrderDAO;
import com.ftd.mo.util.BreadcrumbUtil;
import com.ftd.mo.vo.DeliveryInfoVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.handlers.AddressTypeHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.SourceMasterHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.PartnerVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.SourceMasterVO;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.j2ee.EJBServiceLocator;
import com.ftd.osp.utilities.order.vo.AddressTypeVO;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.partnerreward.core.ejb.RewardServiceEJB;
import com.ftd.partnerreward.core.ejb.RewardServiceEJBHome;
import com.ftd.security.SecurityManager;
import com.ftd.security.cache.vo.UserInfo;


public class UpdateDeliveryInfoBO
{
  private static final String DATE_FORMAT = "MM/dd/yyyy";
  private static final String DATE_FORMAT_WITH_DAY = "EEE MM/dd/yyyy";
  private static final String IN_FORMAT = "yyyy-MM-dd hh:mm:ss";

  //returned back from VIEW_ORDER_DETAIL_UPDATE
  private static final String OUT_ORIG_PRODUCT    = "OUT_ORIG_PRODUCT";
  private static final String OUT_ORIG_ADD_ONS    = "OUT_ORIG_ADD_ONS";
  private static final String OUT_UPD_ORDER_CUR   = "OUT_UPD_ORDER_CUR";
  private static final String OUT_UPD_ADDON_CUR   = "OUT_UPD_ADDON_CUR"; //not used in this class yet
  private static final String OUT_SHIP_COST       = "OUT_SHIP_COST";
  private static final String OUT_ORIG_TAXES_CUR  = "OUT_ORIG_TAXES_CUR";

  //formatted to be shown on the frontend
  private static final String PD_ORIG_PRODUCTS    = "PD_ORIG_PRODUCTS"; //OUT_ORIG_PRODUCT
  private static final String PD_ORIG_PRODUCT     = "PD_ORIG_PRODUCT";  //OUT_ORIG_PRODUCT
  private static final String PD_ORIG_ADD_ONS     = "PD_ORIG_ADD_ONS";  //OUT_ORIG_ADD_ONS
  private static final String PD_ORIG_ADD_ON      = "PD_ORIG_ADD_ON";   //OUT_ORIG_ADD_ONS
  private static final String PD_ORDER_PRODUCTS   = "PD_ORDER_PRODUCTS";//OUT_UPD_ORDER_CUR
  private static final String PD_ORDER_PRODUCT    = "PD_ORDER_PRODUCT"; //OUT_UPD_ORDER_CUR
  private static final String PD_ORDER_ADDONS     = "PD_ORDER_ADDONS";  //OUT_UPD_ADDON_CUR
  private static final String PD_ORDER_ADDON      = "PD_ORDER_ADDON";   //OUT_UPD_ADDON_CUR
  private static final String PD_SHIP_COSTS       = "PD_SHIP_COSTS";    //OUT_SHIP_COSTS
  private static final String PD_SHIP_COST        = "PD_SHIP_COST";     //OUT_SHIP_COSTS
  private static final String PD_ORDER_TAXES      = "PD_ORDER_TAXES";   //OUT_ORIG_TAXES_CUR
  private static final String PD_ORDER_TAX        = "PD_ORDER_TAX";     //OUT_ORIG_TAXES_CUR

  //XPATHs
  private static final String ORDER_PRODUCTS_XPATH = PD_ORDER_PRODUCTS + "/" + PD_ORDER_PRODUCT + "/";
  private static final String ORIG_PRODUCTS_XPATH  = PD_ORIG_PRODUCTS  + "/" + PD_ORIG_PRODUCT + "/";


  private static final String BREADCRUMB_TITLE_DELIVERY_INFO = "Delivery Information";
  private static final String POPUP_INDICATOR_OK      = "O";
  private static final String POPUP_INDICATOR_CONFIRM = "C";
  private static final String SOURCE_CODE_HANDLER_NAME  = "CACHE_NAME_SOURCE_MASTER";
  private static final String BILLING_INFO_HANDLER_NAME = "BILLING_INFO";
  private static final String COST_CENTER_HANDLER_NAME  = "COST_CENTER";
  private static final String VALID_PAY_METHOD_GIFT_CERT = "NC";
  private static final String CC_TYPE_GIFT_CERT = "GC";
  private static final String BILLING_GIFT_CERTIFICATE = "CERTIFICATE#";
  private static final String PROGRAM_TYPE_DEFAULT = "Default";
  private static final String ORIGIN_AMAZON = "AMZNI";
  private static final String MARKETING_GROUP_AMAZON = "AMAZON";
  private static final String OCCASION_SYMPATHY_FUNERAL = "1";
  private static final String REWARDS_MILES_INDICATOR = "Miles";

  //??? These should be obtained via struts mapping instead
  private static final String LOAD_DELIVERY_INFO            = "loadDeliveryInfo.do";
  private static final String UPDATE_ORDER_PRODUCT_CATEGORY = "loadProductCategory.do";
  private static final String UPDATE_DELIVERY_CONFIRMATION  = "loadDeliveryConfirmation.do";
  private static final String CUSTOMER_ORDER_SEARCH         = "customerOrderSearch.do";


 /**---------------------------------------------------------------------------
  *
  * Class level variables
  *
  */
  private HttpServletRequest  request           = null;
  private Connection          con               = null;
  private static Logger       logger            = new Logger("com.ftd.mo.bo.UpdateDeliveryInfoBO");

  private boolean             wasMaxRetrieved   = false;
  private String              nextGlobalAvailableDeliveryDate = "";
  private String              nextAvailableDeliveryDate = "";
  private String              occasion          = null;
  private String              req_action        = "";
  private String              req_errorPage     = null;
  private String              req_orderDetailId = null;
  private String              req_skipFloristValidation = null;
  private String              req_startNewUpdate = "";
  private String              req_externalOrderNumber = null;
  private String              req_membershipRequired = null;
  private String              df_displayPage    = null;
  private String              exceptionStartDate        = null;
  private String              exceptionEndDate          = null;
  //In some cases when we get an error, we display a popup on the UDI page where the user has the
  //ability to accept the error.  In other cases, we display an OK popup or just display the error
  //on the page.  In former situation, we can update the temp tables.  In latter, we shouldn't,
  //because this will cause fatal errors later. This flag will determine if the update is allowed or not.
  private boolean             updateTempTableAllowed   = true;

  private String              sessionId         = null;
  private String              csrId             = null;
  private HashMap             hashXML           = new HashMap();
  private HashMap             pageData          = new HashMap();

  private DeliveryInfoVO      divoOrig          = new DeliveryInfoVO();
  private DeliveryInfoVO      divoRequest       = new DeliveryInfoVO();
  private DeliveryInfoVO      divoTempTable     = new DeliveryInfoVO(); //will contain info from temp tables

  private Document         errorMsgsXML      = null;

  private static String initialContextStr = null;
  private static String ejbProviderUrl = null;


 /**---------------------------------------------------------------------------
  *
  * Constructors
  *
  */
//  public UpdateDeliveryInfoBO() {
//  }

  public UpdateDeliveryInfoBO(Connection con)
  {
    this.con = con;
  }

  public UpdateDeliveryInfoBO(HttpServletRequest request, Connection con)
  {
    this.request = request;
    this.con = con;
  }


 /**---------------------------------------------------------------------------
  *
  * processRequest()
  *
  * Used by the Update Delivery Info Actions to generate the page details
  * and perform validation/updates.  Any display data is passed back to the
  * calling class in a HashMap.
  *
  * @return HashMap - contains 0 - many XML documents of data, 1 HashMap each for page
  *                   details and search criteria
  * @throws
  */
  public HashMap processRequest() throws Exception
  {
    boolean actionSuccessful = true;
    boolean needCacheData = true;
    boolean needSubHeaderFromRequest = true;

    String strutsForwardName        = COMConstants.XSL_UDI_DISPLAY;
    String strutsForwardNameOnError = null;
    OrderDetailsVO odvo = null;

    // HashMap that will be returned to the calling class
    HashMap returnHash = new HashMap();

    // Get info from the request object
    getRequestInfo(this.request);
    if (logger.isDebugEnabled())
      logger.debug("UpdateDeliveryInfoBO processing request for orderDetailId=" + this.req_orderDetailId);

    // Get config file info
    getConfigInfo();


    /**********************************************************************************************
    // Process based on action
    **********************************************************************************************/
    UpdateOrderDAO uoDao = new UpdateOrderDAO(this.con);

    // Get submitted request data and original data, then check if any changes.
    // Note that through checkForChanges(), divoRequest will contain flags for
    // certain fields indicating if they were changed.
    buildDeliveryInfoVOFromRequest();

    //retrieve the divoOrig from the Original tables
    this.divoOrig = uoDao.getOriginalDeliveryInfoVO(this.req_orderDetailId);

    //build the divo from the temp tables.
    buildDivoTempFromTempTables();

    boolean changeDetected = checkForChanges(this.divoRequest, this.divoOrig);

    boolean updatedChangeDetected = checkForChanges(this.divoTempTable, this.divoOrig);

    //Check if order is on EFOS or GNADD hold
    OrderDAO orderDAO = new OrderDAO(con);
    String holdReason = orderDAO.getHoldReason(divoOrig.getOrderDetailId());
    boolean efosHold = false;
    boolean gnaddHold = false;
    if(holdReason != null && holdReason.equals("GNADD"))
        gnaddHold = true;
    else if(holdReason != null && holdReason.equals("EFOS"))
        efosHold = true;

    if (changeDetected)
      setModifyFlag();


    /*******************************************
     * Validate Source Code/Membership changes
     *******************************************/
    // If source code changed, validate it then recalculate price
    if  ( this.divoRequest.getChangesToSourceCode() ||
          this.divoTempTable.getChangesToSourceCode()
        )
    {
      actionSuccessful = validateSourceCode();
    }


    /*******************************************
     * Retrieve Reward Type
     *******************************************/
    //this method will pass the reward_type as a request attribute based on the source_code.
    if (actionSuccessful)
      retrieveRewardType();


    /**********************************************
     * If validation successful save changes to DB
     *********************************************/
    if  ( actionSuccessful    &&
          (changeDetected || updatedChangeDetected)
        )
    {
      updateDeliveryInfo(this.divoRequest, odvo);
      // We'll include an empty Error XML to indicate success
      addToErrorMsgsXML(null);
      needCacheData = false;
    }

    // If unsuccessful we need to convert form request variables back to XML
    if (actionSuccessful == false)
    {
      //If we are re-displaying the info, we need the info.  Without calling this method, we will
      //not have the Order/Product Data.
      getDisplayableDeliveryInfo(false, this.divoRequest);  // Just display current Updated Delivery Info

      // Save informaiton even on error, because sometimes we're allowed to continue.
      // Nothing should get through, but if is does, it will get caught be future validation
      if (this.updateTempTableAllowed)
        updateDeliveryInfo(this.divoRequest, odvo);
      this.hashXML.put(COMConstants.HK_UDI_DELIVERY_INFO_XML, this.divoRequest.toXMLforFrontEnd());
      needSubHeaderFromRequest = false;


      //Ali Lakhani... moved it in the (actionSuccessful == false) if.
      /********************************************************************************************
      // Build any XML needed for front end and set forward page
      ********************************************************************************************/

      // Get cached info
      if (needCacheData)
      {
        processStates();
        processCountries();
        processAddrTypes();
        processGiftMessages();
        processBreadcrumbs();
      }

    }

    // Set forward page variables for this action
    strutsForwardNameOnError = COMConstants.XSL_UDI_REDISPLAY_WITH_ERROR;
    strutsForwardName = COMConstants.XSL_UDI_SUCCESS_SAVE_AND_CONTINUE;


    // Ensure subheader (i.e., the original product order info) gets propagated.
    // Note we don't call this for default action since that's when this info is
    // originally obtained and placed in hashXML directly.
    if (needSubHeaderFromRequest)
      processSubHeader();

    // If any problems set forward page accordingly (via XSL pageData param).
    // Note that any error messages have already been put in an error XML doc
    // via prior calls to addToErrorMsgsXML.
    if (actionSuccessful == false)
    {
      if (strutsForwardNameOnError != null)
        strutsForwardName = strutsForwardNameOnError;
      else if ((this.req_errorPage != null) && (this.req_errorPage.length() > 0))
        strutsForwardName = this.req_errorPage;
      else
        strutsForwardName = COMConstants.XSL_FAILURE;
    }
    this.pageData.put("XSL", strutsForwardName);


    // Retrieve all the Documents from hashXML and put them in returnHash.
    // Also add pageData to returnHash.
    Set ks1 = hashXML.keySet();
    Iterator iter = ks1.iterator();
    String key = null;
    Document doc = null;
    while(iter.hasNext())
    {
      key = iter.next().toString();
      doc = (Document) hashXML.get(key);
      returnHash.put(key, (Document) doc);
    }
    returnHash.put("pageData", (HashMap)this.pageData);

    return returnHash;
  }


 /**---------------------------------------------------------------------------
  *
  * getRequestInfo(HttpServletRequest request)
  *
  * Retrieve the info from the request object
  *
  * @param  HttpServletRequest - request
  * @return none
  * @throws none
  */
  private void getRequestInfo(HttpServletRequest request) throws Exception
  {
    // Get action
    if (request.getParameter(COMConstants.ACTION)!=null)
      this.req_action = request.getParameter(COMConstants.ACTION);

    // Get Order Detail ID (of order to get delivery info for)
    if(request.getParameter(COMConstants.ORDER_DETAIL_ID)!=null) {
      this.req_orderDetailId = request.getParameter(COMConstants.ORDER_DETAIL_ID);
    }

    // Get page that triggered this request.  This is saved via data filter so when update
    // process is all done this value can be used to determine where to go back to.
    // We get local copy since we need it for situations like when update is cancelled.
    if(request.getParameter(COMConstants.UO_DISPLAY_PAGE)!=null)
      this.df_displayPage = request.getParameter(COMConstants.UO_DISPLAY_PAGE);
    else if (logger.isDebugEnabled())
        logger.warn("DeliveryInfoBO was not passed a uo_display_page parameter. Action was: " + this.req_action);

    // Get flag indicating if florist validation is necessary.  If (for example) current order can't
    // be fulfilled by assigned florist and user indicated this was ok, this will be
    // set to 'y'.  We'll end up setting flag in data filter so later in the order process
    // the florist lookup popup can be displayed.
    if (request.getParameter(COMConstants.UDI_SKIP_FLORIST_VALIDATION) != null)
      this.req_skipFloristValidation = request.getParameter(COMConstants.UDI_SKIP_FLORIST_VALIDATION);

    this.req_startNewUpdate = request.getParameter(COMConstants.UDI_START_NEW_UPDATE);
    if (this.req_startNewUpdate == null)
      this.req_startNewUpdate = "N";

    // Get page to go back to in event of an error
    if(request.getParameter(COMConstants.ERROR_DISPLAY_PAGE)!=null)
      this.req_errorPage = request.getParameter(COMConstants.ERROR_DISPLAY_PAGE);
    else
      this.req_errorPage = COMConstants.CONS_SYSTEM_ERROR;

    // Get external order number in case we need to goto CUSTOMER_ORDER_SEARCH page
    if (request.getParameter(COMConstants.EXTERNAL_ORDER_NUMBER) != null)
      this.req_externalOrderNumber = request.getParameter(COMConstants.EXTERNAL_ORDER_NUMBER);

    //retrieve the exception start date
    if(request.getParameter(COMConstants.EXCEPTION_START_DATE)!=null)
      this.exceptionStartDate = request.getParameter(COMConstants.EXCEPTION_START_DATE);

    //retrieve the exception end date
    if(request.getParameter(COMConstants.EXCEPTION_END_DATE)!=null)
      this.exceptionEndDate = request.getParameter(COMConstants.EXCEPTION_END_DATE);

    // Get the occasion
    if (request.getParameter(COMConstants.OCCASION) != null)
      this.occasion = request.getParameter(COMConstants.OCCASION);


    // Get external membership required
    if (request.getParameter("membership_required") != null)
      this.req_membershipRequired = request.getParameter("membership_required");


    // Retrieve Security Info
    this.sessionId   = request.getParameter(COMConstants.SEC_TOKEN);

  }


 /**---------------------------------------------------------------------------
  *
  * getConfigInfo()
  *
  * Retrieve the info from the configuration file
  *
  * @param none
  * @return
  * @throws none
  */
  private void getConfigInfo() throws Exception
  {

    ConfigurationUtil cu = null;
    cu = ConfigurationUtil.getInstance();

    //get csr Id
    String security = (String) cu.getProperty(COMConstants.PROPERTY_FILE,COMConstants.SECURITY_IS_ON);
    boolean securityIsOn = security.equalsIgnoreCase("true") ? true : false;

   //delete this before going to production.  the following should only check on securityIsOn
    if(securityIsOn || this.sessionId != null)
    {
      SecurityManager sm = SecurityManager.getInstance();
      UserInfo ui = sm.getUserInfo(this.sessionId);
      this.csrId = ui.getUserID();
    }

  }


 /**---------------------------------------------------------------------------
  *
  * buildDeliveryInfoVOFromRequest()
  *
  * Creates a DeliveryInfoVO from variables passed in form request
  *
  */
  private void buildDeliveryInfoVOFromRequest()
        throws Exception
  {
    Calendar cUpdatedOn = null;
    Calendar cDeliveryDate = null;
    Calendar cDeliveryDateRangeEnd = null;
    Calendar cShipDate = null;
    Calendar cExceptionStartDate = null;
    Calendar cExceptionEndDate = null;
    SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
    String ldate;

    // Convert dates to Calendar's first
    ldate = this.request.getParameter(COMConstants.UDI_UPDATED_ON);
    if ((ldate != null) && (ldate.trim().length() > 0))
    {
      cUpdatedOn = Calendar.getInstance();
      cUpdatedOn.setTime(dateFormat.parse(ldate));
    }
    ldate = this.request.getParameter(COMConstants.UDI_DELIVERY_DATE);
    if ((ldate != null) && (ldate.trim().length() > 0))
    {
      cDeliveryDate = Calendar.getInstance();
      cDeliveryDate.setTime(dateFormat.parse(ldate));
    }
    ldate = this.request.getParameter(COMConstants.UDI_DELIVERY_DATE_RANGE_END);
    if ((ldate != null) && (ldate.trim().length() > 0))
    {
      cDeliveryDateRangeEnd = Calendar.getInstance();
      cDeliveryDateRangeEnd.setTime(dateFormat.parse(ldate));
    }
    ldate = this.request.getParameter(COMConstants.UDI_SHIP_DATE);
    if ((ldate != null) && (ldate.trim().length() > 0))
    {
      cShipDate = Calendar.getInstance();
      cShipDate.setTime(dateFormat.parse(ldate));
    }
    ldate = this.request.getParameter(COMConstants.EXCEPTION_START_DATE);
    if ((ldate != null) && (ldate.trim().length() > 0))
    {
      cExceptionStartDate = Calendar.getInstance();
      cExceptionStartDate.setTime(dateFormat.parse(ldate));
    }
    ldate = this.request.getParameter(COMConstants.EXCEPTION_END_DATE);
    if ((ldate != null) && (ldate.trim().length() > 0))
    {
      cExceptionEndDate = Calendar.getInstance();
      cExceptionEndDate.setTime(dateFormat.parse(ldate));
    }

    // Now put everything in VO
    this.divoRequest.setAltContactEmail((String) this.request.getParameter(COMConstants.UDI_ALT_CONTACT_EMAIL));
    this.divoRequest.setAltContactExtension((String) this.request.getParameter(COMConstants.UDI_ALT_CONTACT_EXTENSION));
    this.divoRequest.setAltContactInfoId((String) this.request.getParameter(COMConstants.UDI_ALT_CONTACT_INFO_ID));
    this.divoRequest.setAltContactLastName((String) this.request.getParameter(COMConstants.UDI_ALT_CONTACT_LAST_NAME));
    this.divoRequest.setAltContactPhoneNumber((String) this.request.getParameter(COMConstants.UDI_ALT_CONTACT_PHONE_NUMBER));
    this.divoRequest.setCanChangeOrderSource((String) this.request.getParameter(COMConstants.UDI_CAN_CHANGE_ORDER_SOURCE));
    this.divoRequest.setCardMessage((String) this.request.getParameter(COMConstants.UDI_CARD_MESSAGE));
    this.divoRequest.setCardSignature((String) this.request.getParameter(COMConstants.UDI_CARD_SIGNATURE));
    this.divoRequest.setCompanyId( (String) this.request.getParameter(COMConstants.COMPANY_ID) );
    this.divoRequest.setCustomerCity((String) this.request.getParameter(COMConstants.UDI_CUSTOMER_CITY));
    this.divoRequest.setCustomerCountry((String) this.request.getParameter(COMConstants.UDI_CUSTOMER_COUNTRY));
    this.divoRequest.setCustomerFirstName((String) this.request.getParameter(COMConstants.UDI_CUSTOMER_FIRST_NAME));
    this.divoRequest.setCustomerId((String) this.request.getParameter(COMConstants.CUSTOMER_ID));
    this.divoRequest.setCustomerLastName((String) this.request.getParameter(COMConstants.UDI_CUSTOMER_LAST_NAME));
    this.divoRequest.setCustomerState((String) this.request.getParameter(COMConstants.UDI_CUSTOMER_STATE));
    this.divoRequest.setCustomerZipCode((String) this.request.getParameter(COMConstants.UDI_CUSTOMER_ZIP_CODE));
    this.divoRequest.setDeliveryDate(cDeliveryDate);
    this.divoRequest.setDeliveryDateRangeEnd(cDeliveryDateRangeEnd);
    this.divoRequest.setEODDeliveryIndicator( (String)this.request.getParameter("eod_delivery_indicator") );
    this.divoRequest.setExceptionCode((String)this.request.getParameter(COMConstants.EXCEPTION_CODE) );
    this.divoRequest.setExceptionStartDate(cExceptionStartDate);
    this.divoRequest.setExceptionEndDate(cExceptionEndDate);
    this.divoRequest.setFloristId((String) this.request.getParameter(COMConstants.UDI_FLORIST_ID));
    this.divoRequest.setMasterOrderNumber((String) this.request.getParameter(COMConstants.UDI_MASTER_ORDER_NUMBER));
    this.divoRequest.setMembershipFirstName((String) this.request.getParameter(COMConstants.UDI_MEMBERSHIP_FIRST_NAME));
    this.divoRequest.setMembershipLastName((String) this.request.getParameter(COMConstants.UDI_MEMBERSHIP_LAST_NAME));
    this.divoRequest.setMembershipNumber((String) this.request.getParameter(COMConstants.UDI_MEMBERSHIP_NUMBER));
    this.divoRequest.setMembershipRequired((String) this.request.getParameter(COMConstants.UDI_MEMBERSHIP_REQUIRED));
    this.divoRequest.setOccasion((String) this.request.getParameter(COMConstants.UDI_OCCASION));
    this.divoRequest.setOccasionCategoryIndex((String) this.request.getParameter(COMConstants.PD_MO_CATEGORY_INDEX));
    this.divoRequest.setOccasionDescription((String) this.request.getParameter(COMConstants.UDI_OCCASION_DESCRIPTION));
    this.divoRequest.setOrderDetailId((String) this.request.getParameter(COMConstants.UDI_ORDER_DETAIL_ID));
    this.divoRequest.setOrderGuid( (String) this.request.getParameter(COMConstants.ORDER_GUID) );
    this.divoRequest.setOriginId((String) this.request.getParameter(COMConstants.UDI_ORIGIN_ID));
    this.divoRequest.setProductAmount( (String) this.request.getParameter("orig_product_amount") );
    this.divoRequest.setProductId((String) this.request.getParameter(COMConstants.UDI_PRODUCT_ID));
    this.divoRequest.setRecipientAddress1((String) this.request.getParameter(COMConstants.UDI_RECIPIENT_ADDRESS_1));
    this.divoRequest.setRecipientAddress2((String) this.request.getParameter(COMConstants.UDI_RECIPIENT_ADDRESS_2));
    this.divoRequest.setRecipientAddressType((String) this.request.getParameter(COMConstants.UDI_RECIPIENT_ADDRESS_TYPE));
    this.divoRequest.setRecipientBusinessInfo((String) this.request.getParameter(COMConstants.UDI_RECIPIENT_BUSINESS_INFO));
    this.divoRequest.setRecipientBusinessName((String) this.request.getParameter(COMConstants.UDI_RECIPIENT_BUSINESS_NAME));
    this.divoRequest.setRecipientCity((String) this.request.getParameter(COMConstants.UDI_RECIPIENT_CITY));
    this.divoRequest.setRecipientCountry((String) this.request.getParameter(COMConstants.UDI_RECIPIENT_COUNTRY));
    this.divoRequest.setRecipientExtension((String) this.request.getParameter(COMConstants.UDI_RECIPIENT_EXTENSION));
    this.divoRequest.setRecipientFirstName((String) this.request.getParameter(COMConstants.UDI_RECIPIENT_FIRST_NAME));
    this.divoRequest.setRecipientId((String) this.request.getParameter(COMConstants.UDI_RECIPIENT_ID));
    this.divoRequest.setRecipientLastName((String) this.request.getParameter(COMConstants.UDI_RECIPIENT_LAST_NAME));
    this.divoRequest.setRecipientPhoneNumber((String) this.request.getParameter(COMConstants.UDI_RECIPIENT_PHONE_NUMBER));
    this.divoRequest.setRecipientState((String) this.request.getParameter(COMConstants.UDI_RECIPIENT_STATE));
    this.divoRequest.setRecipientZipCode((String) this.request.getParameter(COMConstants.UDI_RECIPIENT_ZIP_CODE));
    this.divoRequest.setReleaseInfoIndicator( StringUtils.defaultString((String)this.request.getParameter(COMConstants.UDI_RELEASE_INFO_INDICATOR), "N") );
    this.divoRequest.setShipDate(cShipDate);
    this.divoRequest.setShipMethod((String) this.request.getParameter(COMConstants.UDI_SHIP_METHOD));
    this.divoRequest.setShipMethodCarrier( (String) this.request.getParameter("ship_method_carrier") );
    this.divoRequest.setShipMethodDesc((String) this.request.getParameter(COMConstants.UDI_SHIP_METHOD_DESC));
    this.divoRequest.setShipMethodFlorist( (String) this.request.getParameter("ship_method_florist") );
    this.divoRequest.setShippingFee((String) this.request.getParameter(COMConstants.UDI_SHIPPING_FEE));
    this.divoRequest.setSizeIndicator((String) this.request.getParameter("price_point_id"));
    this.divoRequest.setSourceCode((String) this.request.getParameter(COMConstants.UDI_SOURCE_CODE));
    this.divoRequest.setSourceCodeDescription((String) this.request.getParameter(COMConstants.UDI_SOURCE_CODE_DESCRIPTION));
    this.divoRequest.setSourceType((String) this.request.getParameter(COMConstants.UDI_SOURCE_TYPE));
    this.divoRequest.setSpecialInstructions((String) this.request.getParameter(COMConstants.UDI_SPECIAL_INSTRUCTIONS));
    this.divoRequest.setUpdatedBy(this.csrId);
    this.divoRequest.setUpdatedOn(cUpdatedOn);
    this.divoRequest.setVendorFlag((String) this.request.getParameter(COMConstants.UDI_VENDOR_FLAG));

    // These "Special Instruction" fields are only submitted from front end
    // when no specialInstruction field originally existed on order.  Once
    // submitted we just concatenate together and save in specialInstructions.
    String specialInst = "";
    String deliveryLocationType = this.request.getParameter(COMConstants.UDI_RECIPIENT_ADDRESS_CODE);
    AddressTypeHandler addrTypeHandler = (AddressTypeHandler) CacheManager.getInstance().getHandler("CACHE_NAME_ADDRESS");
    AddressTypeVO addressTypeVO = addrTypeHandler.getAddressTypeByCode(deliveryLocationType);
    
    if (StringUtils.isNotEmpty(this.request.getParameter(COMConstants.UDI_SPEC_ROOM_NUMBER)))
      specialInst += addressTypeVO.getRoomLabelTxt() + ": " + this.request.getParameter(COMConstants.UDI_SPEC_ROOM_NUMBER);
    
    if (StringUtils.isNotEmpty(this.request.getParameter(COMConstants.UDI_SPEC_START_TIME)) || StringUtils.isNotEmpty(this.request.getParameter(COMConstants.UDI_SPEC_END_TIME)))
    {
      specialInst += " " + addressTypeVO.getHoursLabelTxt() + ": " + this.request.getParameter(COMConstants.UDI_SPEC_START_TIME) 
                  + " - " + this.request.getParameter(COMConstants.UDI_SPEC_END_TIME);
    }

   if (StringUtils.isNotEmpty(specialInst))
      this.divoRequest.setSpecialInstructions(specialInst);


    //if the Location Type was Home or Other, ensure that the Recipient Business Name gets erased
    //Here are the codes as per FRP.ADDRESS_TYPES table
      //HOSPITAL					H	
      //FUNERAL HOME			F	
      //HOME							R	
      //BUSINESS					B	
      //OTHER							O	
      //NURSING HOME			N	
    //String deliveryLocationType = this.request.getParameter(COMConstants.UDI_RECIPIENT_ADDRESS_CODE);
    if (deliveryLocationType.equalsIgnoreCase("R") || deliveryLocationType.equalsIgnoreCase("O"))
    {
      this.divoRequest.setRecipientBusinessInfo(null);
      this.divoRequest.setRecipientBusinessName(null);
      this.divoRequest.setSpecialInstructions(null);
    }

  }


 /**---------------------------------------------------------------------------
  *
  * buildDivoTempFromTempTables()
  *
  */
  private void buildDivoTempFromTempTables() throws Exception
  {
    this.divoTempTable = (DeliveryInfoVO)this.divoOrig.clone();

    //Instantiate UpdateOrderDAO
    UpdateOrderDAO updateOrderDAO = new UpdateOrderDAO(this.con);

    //hashmap that will contain the output from the stored proc
    HashMap deliveryInfoMap = updateOrderDAO.getOrderDetailsUpdate(
                              this.req_orderDetailId,
                              COMConstants.CONS_ORDER_DETAILS_UPDATE_TYPE,
                              false, this.csrId, null);

    Set ks1 = deliveryInfoMap.keySet();
    Iterator iter = ks1.iterator();
    String key = null;
    Calendar cDeliveryDate = null;
    Calendar cDeliveryDateRangeEnd = null;
    Calendar cShipDate = null;

    //Create a CachedResultSet object using the cursor name that was passed.
    CachedResultSet rs = null;

    //Iterate thru the keyset
    while(iter.hasNext())
    {
      //get the key
      key = iter.next().toString();

      if (key.equalsIgnoreCase("OUT_UPD_ORDER_CUR"))
      {
        rs = (CachedResultSet) deliveryInfoMap.get(key);

        while(rs.next())
        {

          //reset all the dates
          if (rs.getObject("delivery_date") != null)
          {
            cDeliveryDate = Calendar.getInstance();
            cDeliveryDate.setTime((Date) rs.getObject("delivery_date"));
            this.divoTempTable.setDeliveryDate(cDeliveryDate);
          }

          if (rs.getObject("delivery_date_range_end") != null)
          {
            cDeliveryDateRangeEnd = Calendar.getInstance();
            cDeliveryDateRangeEnd.setTime((Date) rs.getObject("delivery_date_range_end"));
            this.divoTempTable.setDeliveryDateRangeEnd(cDeliveryDateRangeEnd);
          }

          if (rs.getObject("ship_date") != null)
          {
            cShipDate = Calendar.getInstance();
            cShipDate.setTime((Date) rs.getObject("ship_date"));
            this.divoTempTable.setShipDate(cShipDate);
          }

          //reset remaining updateable fields
          this.divoTempTable.setAltContactEmail(rs.getObject("alt_contact_email") != null? rs.getObject("alt_contact_email").toString():null);
          this.divoTempTable.setAltContactExtension(rs.getObject("alt_contact_extension") != null? rs.getObject("alt_contact_extension").toString():null);
          this.divoTempTable.setAltContactFirstName(rs.getObject("alt_contact_first_name") != null? rs.getObject("alt_contact_first_name").toString():null);
          this.divoTempTable.setAltContactLastName(rs.getObject("alt_contact_last_name") != null? rs.getObject("alt_contact_last_name").toString():null);
          this.divoTempTable.setAltContactInfoId(rs.getObject("alt_contact_info_id") != null? rs.getObject("alt_contact_info_id").toString():null);
          this.divoTempTable.setAltContactPhoneNumber(rs.getObject("alt_contact_phone_number") != null? rs.getObject("alt_contact_phone_number").toString():null);
          this.divoTempTable.setCardMessage(rs.getObject("card_message") != null? rs.getObject("card_message").toString():null);
          this.divoTempTable.setCardSignature(rs.getObject("card_signature") != null? rs.getObject("card_signature").toString():null);
          this.divoTempTable.setFloristId(rs.getObject("florist_id") != null? rs.getObject("florist_id").toString():null);
          this.divoTempTable.setMembershipFirstName(rs.getObject("membership_first_name") != null? rs.getObject("membership_first_name").toString():null);
          this.divoTempTable.setMembershipLastName(rs.getObject("membership_last_name") != null? rs.getObject("membership_last_name").toString():null);
          this.divoTempTable.setMembershipNumber(rs.getObject("membership_number") != null? rs.getObject("membership_number").toString():null);
          this.divoTempTable.setOccasion(rs.getObject("occasion") != null? rs.getObject("occasion").toString():null);
          this.divoTempTable.setRecipientAddress1(rs.getObject("recipient_address_1") != null? rs.getObject("recipient_address_1").toString():null);
          this.divoTempTable.setRecipientAddress2(rs.getObject("recipient_address_2") != null? rs.getObject("recipient_address_2").toString():null);
          this.divoTempTable.setRecipientAddressType(rs.getObject("recipient_address_type") != null? rs.getObject("recipient_address_type").toString():null);
          this.divoTempTable.setRecipientBusinessInfo(rs.getObject("recipient_business_info") != null? rs.getObject("recipient_business_info").toString():null);
          this.divoTempTable.setRecipientBusinessName(rs.getObject("recipient_business_name") != null? rs.getObject("recipient_business_name").toString():null);
          this.divoTempTable.setRecipientCity(rs.getObject("recipient_city") != null? rs.getObject("recipient_city").toString():null);
          this.divoTempTable.setRecipientCountry(rs.getObject("recipient_country") != null? rs.getObject("recipient_country").toString():null);
          this.divoTempTable.setRecipientExtension(rs.getObject("recipient_extension") != null? rs.getObject("recipient_extension").toString():null);
          this.divoTempTable.setRecipientFirstName(rs.getObject("recipient_first_name") != null? rs.getObject("recipient_first_name").toString():null);
          this.divoTempTable.setRecipientId(rs.getObject("recipient_id") != null? rs.getObject("recipient_id").toString():null);
          this.divoTempTable.setRecipientLastName(rs.getObject("recipient_last_name") != null? rs.getObject("recipient_last_name").toString():null);
          this.divoTempTable.setRecipientPhoneNumber(rs.getObject("recipient_phone_number") != null? rs.getObject("recipient_phone_number").toString():null);
          this.divoTempTable.setRecipientState(rs.getObject("recipient_state") != null? rs.getObject("recipient_state").toString():null);
          this.divoTempTable.setRecipientZipCode(rs.getObject("recipient_zip_code") != null? rs.getObject("recipient_zip_code").toString():null);
          this.divoTempTable.setShipMethod(rs.getObject("ship_method") != null? rs.getObject("ship_method").toString():null);
          this.divoTempTable.setSourceCode(rs.getObject("source_code") != null? rs.getObject("source_code").toString():null);
          this.divoTempTable.setSpecialInstructions(rs.getObject("special_instructions") != null? rs.getObject("special_instructions").toString():null);
          this.divoTempTable.setVendorFlag(rs.getObject("vendor_flag") != null? rs.getObject("vendor_flag").toString():null);

        } //end (while rs.next())
      }
      else if (key.equalsIgnoreCase("OUT_ORIG_ADD_ONS"))
      {}
      else if (key.equalsIgnoreCase("OUT_UPD_ADDON_CUR"))
      {}
      else if (key.equalsIgnoreCase("OUT_ORIG_PRODUCT"))
      {}
      else if (key.equalsIgnoreCase("OUT_SHIP_COST"))
      {}


    } //end-while(iter.hasNext())

  }


 /**---------------------------------------------------------------------------
  *
  * checkForChanges()
  *
  * Compares delivery info passed via form submit against what's already in DB.
  * A comment string of all changed fields is built and change flags are set
  * appropriately in the a_divoRequest object.
  *
  * @param  DeliveryInfoVO a_divoRequest  - Contains new delivery info.  Note that
  *          change flags are set in this object when differences from a_divoOrig
  *          are detected.
  * @param  DeliveryInfoVO a_divoOrig - Contains original delivery info.
  * @param  StringBuffer a_changeComments - If non-null, comments will be added
  *          to this buffer indicating which fields changed.
  * @return true if any changes detected, false otherwise.
  *          This is same as changesMade flag.
  */
  private boolean checkForChanges(DeliveryInfoVO divoFirst, DeliveryInfoVO divoSecond) throws Exception
  {
    boolean changesMade = false;

    // Name change
    if (compareStrings(divoSecond.getRecipientFirstName(), divoFirst.getRecipientFirstName()) ||
        compareStrings(divoSecond.getRecipientLastName(), divoFirst.getRecipientLastName()))
      changesMade = true;

    // Address change
    if (compareStrings(divoSecond.getRecipientAddress1(), divoFirst.getRecipientAddress1()) ||
        compareStrings(divoSecond.getRecipientAddress2(), divoFirst.getRecipientAddress2()) ||
        compareStrings(divoSecond.getRecipientCity(), divoFirst.getRecipientCity()) ||
        compareStrings(divoSecond.getRecipientState(), divoFirst.getRecipientState()) ||
        compareStrings(divoSecond.getRecipientZipCode(), divoFirst.getRecipientZipCode()) ||
        compareStrings(divoSecond.getRecipientCountry(), divoFirst.getRecipientCountry()) ||
        compareStrings(divoSecond.getRecipientAddressType(), divoFirst.getRecipientAddressType()))
    {
      if (compareStrings(divoSecond.getRecipientZipCode(), divoFirst.getRecipientZipCode()))
        divoFirst.setChangesToZip(true);

      if (compareStrings(divoSecond.getRecipientCountry(), divoFirst.getRecipientCountry()))
        divoFirst.setChangesToCountry(true);

      changesMade = true;
    }

    // Delivery date change
    SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
    if ((divoSecond.getDeliveryDate() != null) || (divoFirst.getDeliveryDate() != null))
    {
      Calendar dummyDate = Calendar.getInstance();
      dummyDate = DateUtils.truncate(dummyDate, Calendar.DATE);
      Calendar origDate = ((divoSecond.getDeliveryDate() != null) ? divoSecond.getDeliveryDate() : dummyDate);
      if (! origDate.equals(divoFirst.getDeliveryDate()))
      {
        changesMade = true;
        divoFirst.setChangesToDeliveryDate(true);
      }
      // Set flag for later validation if delivery day was changed to Sunday
      Calendar newDate = divoFirst.getDeliveryDate();
      if (newDate != null)
      {
        int newDayOfWeek = newDate.get(Calendar.DAY_OF_WEEK);

        //if ((origDate.get(Calendar.DAY_OF_WEEK) != newDayOfWeek) && (newDayOfWeek == Calendar.SUNDAY)) {
        if (newDayOfWeek == Calendar.SUNDAY)
          divoFirst.setChangedToSundayDelivery(true);
      }
    }

    // Delivery date range end change
    if ((divoSecond.getDeliveryDateRangeEnd() != null) || (divoFirst.getDeliveryDateRangeEnd() != null))
    {
      Calendar dummyDate = Calendar.getInstance();
      dummyDate = DateUtils.truncate(dummyDate, Calendar.DATE);
      Calendar origDate = ((divoSecond.getDeliveryDateRangeEnd() != null) ? divoSecond.getDeliveryDateRangeEnd() : dummyDate);
      if (! origDate.equals(divoFirst.getDeliveryDateRangeEnd()))
      {
        changesMade = true;
        divoFirst.setChangesToDeliveryDate(true);
      }
    }

    // Ship method change
    if (compareStrings(divoSecond.getShipMethod(), divoFirst.getShipMethod()))
    {
      changesMade = true;
      divoFirst.setChangesToShipMethod(true);
    }

    // Card message and signature change
    //
    if (compareStrings(divoSecond.getCardMessage(), divoFirst.getCardMessage()) ||
        compareStrings(divoSecond.getCardSignature(), divoFirst.getCardSignature()))
      changesMade = true;

    // Phone number change
    if (compareStrings(divoSecond.getRecipientPhoneNumber(), divoFirst.getRecipientPhoneNumber()) ||
        compareStrings(divoSecond.getRecipientExtension(), divoFirst.getRecipientExtension()))
      changesMade = true;

    // Alternate contact info change
    if (compareStrings(divoSecond.getAltContactLastName(), divoFirst.getAltContactLastName()) ||
        compareStrings(divoSecond.getAltContactPhoneNumber(), divoFirst.getAltContactPhoneNumber()) ||
        compareStrings(divoSecond.getAltContactEmail(), divoFirst.getAltContactEmail()) ||
        compareStrings(divoSecond.getAltContactExtension(), divoFirst.getAltContactExtension()))
      changesMade = true;

    // Recipient business name change
    if (compareStrings(divoSecond.getRecipientBusinessName(), divoFirst.getRecipientBusinessName()))
      changesMade = true;

    // Special Instructions change
    if (compareStrings(divoSecond.getSpecialInstructions(), divoFirst.getSpecialInstructions()))
      changesMade = true;

    // Source code change
    String sourceCode = divoFirst.getSourceCode();
    if (sourceCode == null || sourceCode.trim().length() == 0)
    {
      // If new source code was not submitted, just use original
      divoFirst.setSourceCode(divoSecond.getSourceCode());
    }
    else
    {
      if( compareStrings(divoSecond.getSourceCode(), divoFirst.getSourceCode()) ||
          compareStrings(divoSecond.getMembershipNumber(), divoFirst.getMembershipNumber()) ||
          this.req_membershipRequired.equalsIgnoreCase(COMConstants.CONS_YES)
        )
      {
        divoFirst.setChangesToSourceCode(true);
        changesMade = true;
      }
    }

    return changesMade;
  }


 /**---------------------------------------------------------------------------
  *
  * compareStrings()
  *
  * Utility method used only by checkForChanges to compare two strings.
  * If changes are detected a true value is returned.
  *
  */
  private boolean compareStrings(String a_orig, String a_new)
  {
    boolean returnFlag = false;

    if ((a_orig != null) || (a_new != null))
    {
      String lorig = (a_orig != null) ? a_orig: "";

      if (! lorig.equalsIgnoreCase(a_new))
        returnFlag       = true;
    }

    return returnFlag;
  }


 /*******************************************************************************************
  * setModifyFlag()
  *******************************************************************************************
  *
  * A change was detected.  Thus, update the MODIFY_ORDER_UPDATED flag in the request parameters
  *
  * @return nothing
  * @throws java.lang.Exception
  */
  private void setModifyFlag()
  {
    HashMap parameters = (HashMap) this.request.getAttribute    ( COMConstants.CONS_APP_PARAMETERS);
    parameters.put(COMConstants.MODIFY_ORDER_UPDATED,             COMConstants.CONS_YES);
    this.request.setAttribute(COMConstants.CONS_APP_PARAMETERS,   parameters);
  }



 /**----------------------------------------------------------------------------
  *
  * validateSourceCode()
  *
  * Verify current order can handle switch to a new source code.
  *
  * @param  none
  * @return true if validation success, false otherwise
  * @throws Exception
  */
  private boolean validateSourceCode() throws Exception
  {
    if(repCanAccessOrder(this.divoRequest.getSourceCode()))
    {
        String origSourceCode = this.divoOrig.getSourceCode();
        String newSourceCode  = this.divoRequest.getSourceCode();
        String orderDetailId  = this.divoRequest.getOrderDetailId();
        CachedResultSet rs = null;
        UpdateOrderDAO uoDao = new UpdateOrderDAO(this.con);
    
        // Get source code info from cache handler.  If nothing found return with error.
        SourceMasterHandler sch = (SourceMasterHandler) CacheManager.getInstance().getHandler(SOURCE_CODE_HANDLER_NAME);
        SourceMasterVO sourceVo = sch.getSourceCodeById(newSourceCode);
        if (sourceVo == null)
        {
          addToErrorMsgsXML(COMConstants.INVALID_SOURCE_CODE, "page_source_code");
          this.updateTempTableAllowed = false;
          return false;
        }
    
        // If source code is inactive return with error
        Date todaysDate = new Date();
        Date endDate = sourceVo.getEndDate();
        Date startDate = sourceVo.getStartDate();
        if ((endDate != null && todaysDate.compareTo(endDate) > 0) ||
            (todaysDate.compareTo(startDate) < 0))
        {
          addToErrorMsgsXML(COMConstants.INVALID_SOURCE_CODE, "page_source_code");
          this.updateTempTableAllowed = false;
          return false;
        }
    
        // If source code company is not same as original, return with error
        if (!sourceVo.getCompanyId().equals(this.divoOrig.getCompanyId()))
        {
          addToErrorMsgsXML(COMConstants.INVALID_SOURCE_CODE_FOR_COMPANY + sourceVo.getCompanyId(), "page_source_code");
          this.updateTempTableAllowed = false;
          return false;
        }
    
        // Check membership number
        if (!validateMembershipNumber(sourceVo.getPartnerId(), this.divoRequest.getMembershipNumber(), sourceVo.getPartnerName()))
        {
          if (StringUtils.isNotEmpty(this.divoRequest.getMembershipNumber()))
          {
            addToErrorMsgsXML("Invalid membership number", COMConstants.UDI_MEMBERSHIP_NUMBER);
            this.updateTempTableAllowed = false;
          }
          else
          {
            addToErrorMsgsXML("Source code requires membership information. " +
                              "Either enter correct info or select a source code that does not require membership info",
                              "page_source_code");
            this.updateTempTableAllowed = false;
          }
          return false;
        }
    
        if  ( StringUtils.isEmpty(sourceVo.getPartnerId()) &&
              (StringUtils.isNotEmpty(this.divoRequest.getMembershipNumber()) ||
               StringUtils.isNotEmpty(this.divoRequest.getMembershipFirstName()) ||
               StringUtils.isNotEmpty(this.divoRequest.getMembershipLastName())
              )
            )
        {
          this.divoRequest.setMembershipNumber(null);
          this.divoRequest.setMembershipFirstName(null);
          this.divoRequest.setMembershipLastName(null);
        }
        return true;
    }


    return false;
  }


 /**---------------------------------------------------------------------------
  *
  * addToErrorMsgsXML()
  *
  * Overloaded addToErrorMsgsXML for convenience
  *
  */
  private void addToErrorMsgsXML(String a_msgText)
        throws Exception
  {
    addToErrorMsgsXML(a_msgText, null, null, null, null);
  }
  private void addToErrorMsgsXML(String a_msgText, String a_fieldName)
        throws Exception
  {
    addToErrorMsgsXML(a_msgText, a_fieldName, null, null, null);
  }
  private void addToErrorMsgsXML(String a_msgText, String a_fieldName,
                                   String a_popupType, String a_popupReturn)
        throws Exception
  {
    addToErrorMsgsXML(a_msgText, a_fieldName, a_popupType, a_popupReturn, null);
  }


 /**---------------------------------------------------------------------------
  *
  * addToErrorMsgsXML()
  *
  * Adds an error message to the error message XML doc
  *
  * @params a_msgTxt - Message text
  * @params a_fieldName - Name of erred field (optional).
  * @params a_popupType - Type of popup window (optional).
  * @params a_popupReturn - Return page if popup confirmation selected (optional).
  * @params a_popupCancel - Return page if confirmation is cancelled (optional).
  *
  */
  private void addToErrorMsgsXML(String a_msgText, String a_fieldName,
                                   String a_popupType, String a_popupReturn, String a_popupCancel)
        throws Exception
  {
    Element errMsgsTopElement;

    // Note the front end expects a popup type when no fieldname is specified
    String l_popupType = a_popupType;
    if (a_fieldName == null && a_popupType == null)
      l_popupType = POPUP_INDICATOR_OK;

    if (this.errorMsgsXML == null)
    {
      // Generate top node
      this.errorMsgsXML = (Document) DOMUtil.getDefaultDocument();
      errMsgsTopElement = errorMsgsXML.createElement("ERROR_MESSAGES");
      this.errorMsgsXML.appendChild(errMsgsTopElement);
      this.hashXML.put("errormessages",  this.errorMsgsXML);
    }
    else
      errMsgsTopElement = errorMsgsXML.getDocumentElement();


    if (a_msgText != null)
    {
      Element errorMsgElement = this.errorMsgsXML.createElement("ERROR_MESSAGE");
      errMsgsTopElement.appendChild(errorMsgElement);

      Element errMsgNode = this.errorMsgsXML.createElement("TEXT");
      errorMsgElement.appendChild(errMsgNode);
      errMsgNode.appendChild(this.errorMsgsXML.createTextNode(a_msgText));

      Element errFieldNode = this.errorMsgsXML.createElement("FIELDNAME");
      errorMsgElement.appendChild(errFieldNode);
      if (a_fieldName != null)
        errFieldNode.appendChild(this.errorMsgsXML.createTextNode(a_fieldName));

      if (l_popupType != null)
      {
        Element popupIndicatorNode = this.errorMsgsXML.createElement("POPUP_INDICATOR");
        errorMsgElement.appendChild(popupIndicatorNode);
        popupIndicatorNode.appendChild(this.errorMsgsXML.createTextNode(l_popupType));
      }

      if (a_popupReturn != null)
      {
        Element popupGotoNode = this.errorMsgsXML.createElement("POPUP_GOTO_PAGE");
        errorMsgElement.appendChild(popupGotoNode);
        popupGotoNode.appendChild(this.errorMsgsXML.createTextNode(a_popupReturn));
      }

      if (a_popupCancel != null)
      {
        Element popupCancelNode = this.errorMsgsXML.createElement("POPUP_CANCEL_PAGE");
        errorMsgElement.appendChild(popupCancelNode);
        popupCancelNode.appendChild(this.errorMsgsXML.createTextNode(a_popupCancel));
      }
    }
  }



 /**----------------------------------------------------------------------------
  *
  * validateMembershipNumber()
  *
  * Verify membership number is correct for partner.
  * If no partner ID, then no need to validate.
  *
  * @params a_partnerId - Partner associated with source code
  * @params a_memberNum - Membership number to be validated
  * @return true if valid (or no validation necessary), false otherwise
  * @throws Exception
  */
  private boolean validateMembershipNumber(String partnerId, String memberNum, String partnerName) throws Exception
  {
    boolean validMembershipId = true;

    //even if a partner id (program name) is found for a source code, check if it belongs to Discover,
    //Upromise etc.  If so, do not require a membership field.
    //Note that WebOE has this hard-coded, and there are no database columns that we can drive this
    //off of.  Tim S. knows about this, and once WebOE is fixed, this needs to be updated too.
    if  ( partnerId != null                                               &&
          partnerId.length() > 0                                          &&
          !partnerId.equalsIgnoreCase(COMConstants.CONS_DISCOVER_CODE)    &&
          !partnerId.equalsIgnoreCase(COMConstants.CONS_UPROMISE_CODE)    &&
          !partnerId.equalsIgnoreCase(COMConstants.CONS_UPROMISE1_CODE)   &&
          !partnerId.equalsIgnoreCase(COMConstants.CONS_CORP15_CODE)      &&
          !partnerId.equalsIgnoreCase(COMConstants.CONS_CORP20_CODE)      &&
          !partnerId.equalsIgnoreCase(COMConstants.CONS_ADVO_CODE)
        )
    {
      validMembershipId = new ValidateMembership().validateMembershipById(memberNum, partnerId, partnerName, this.con);
      this.divoRequest.setMembershipRequired("Y");  // Set this to make sure front end displays membership fields
    }
    else
    {
      // clear out membership information //
      this.divoRequest.setMembershipNumber(null);
      this.divoRequest.setMembershipFirstName(null);
      this.divoRequest.setMembershipLastName(null);
      this.divoRequest.setMembershipRequired("N");
    }

    return validMembershipId;
  }


/*******************************************************************************************
 * retrieveRewardType()
 *******************************************************************************************
 * get the reward type.
 *
 * @return nothing
 * @throws java.lang.Exception
 */
  private void retrieveRewardType() throws Exception
  {
    UpdateOrderDAO uoDAO = new UpdateOrderDAO(this.con);

    String rewardType = (String) uoDAO.getDiscountRewardTypeById(this.divoRequest.getSourceCode());

    this.request.setAttribute(COMConstants.REWARD_TYPE, rewardType);

  }


 /**----------------------------------------------------------------------------
  *
  * updateDeliveryInfo()
  *
  * Save entered delivery information to order_detail_update table in DB.
  *
  * @param a_divo - DeliveryInfoVO containing delivery information to save to DB.
  * @param a_odvo - OrderDetailsVO containing updated pricing info to save to DB.
  * @return none
  * @throws Exception
  */
  private void updateDeliveryInfo(DeliveryInfoVO diVO, OrderDetailsVO odVO) throws Exception
  {
    UpdateOrderDAO uoDao = new UpdateOrderDAO(this.con);

    // Save delivery info to DB
    uoDao.updateOrderDetailsUpdate(diVO);

    // For contact info, if we already have contact info ID then update.
    // Otherwise insert, but only if there's data
    if (diVO.getAltContactInfoId() != null && !diVO.getAltContactInfoId().equals(""))
      uoDao.updateOrderContactUpdate(diVO);
    else if ( StringUtils.isNotEmpty(diVO.getAltContactLastName()) ||
              StringUtils.isNotEmpty(diVO.getAltContactPhoneNumber()) ||
              StringUtils.isNotEmpty(diVO.getAltContactEmail()) ||
              StringUtils.isNotEmpty(diVO.getAltContactExtension()))
    {
      uoDao.insertOrderContactUpdate(diVO);
    }

    // Save pricing info
    if (odVO != null)
    {
      OrderBillVO obVO = new OrderBillVO();
      obVO.setAddOnAmount(this.divoOrig.getActualAddOnAmount());
      obVO.setDiscountAmount(this.divoOrig.getActualDiscountAmount());
      obVO.setProductAmount(this.divoOrig.getActualProductAmount());
      obVO.setServiceFee(this.divoOrig.getActualServiceFee());
      obVO.setShippingFee(this.divoOrig.getActualShippingFee());
      obVO.setShippingTax(this.divoOrig.getActualShippingTax());
      obVO.setTax(this.divoOrig.getActualTax());

      uoDao.updateODUAmounts(odVO, obVO,  diVO.getOrderDetailId(), this.csrId);
    }

    //update the miles/points
    if (diVO.getChangesToSourceCode())
      uoDao.updateMilesPoints(diVO.getOrderDetailId(), this.csrId, new Double(diVO.getMilesPoints()).toString());

  }


 /**----------------------------------------------------------------------------
  *
  * getDisplayableDeliveryInfo()
  *
  * Gets updated delivery info (and original order info) from database
  * and places in XML document.  If a_startNewUpdate is true, then original
  * delivery info is used to start a new update.
  *
  * @param  a_startNewUpdate - true starts a new update
  * @param  a_divoRequest - DeliveryInfoVO for use only in obtaining delivery dates.
  *                      If null, then info from DB is used instead.
  * @return none
  * @throws none
  */
  private void getDisplayableDeliveryInfo(boolean startNewUpdate,
                                            DeliveryInfoVO diVO) throws Exception
  {
    boolean forceFloralDates = false;
    DeliveryInfoVO divoDateInfo = new DeliveryInfoVO();

    // Get data from DB
    UpdateOrderDAO updateOrderDAO = new UpdateOrderDAO(this.con);

    HashMap deliveryInfoMap = updateOrderDAO.getOrderDetailsUpdate(
                              this.req_orderDetailId,
                              COMConstants.CONS_ORDER_DETAILS_UPDATE_TYPE,
                              startNewUpdate, this.csrId, null);

    // Also reset data filter flag if starting anew
    if (startNewUpdate)
      updateDataFilterValue(COMConstants.FLORIST_CANT_FULFILL, COMConstants.CONS_NO);

    // Transform CachedResultSets for the three cursors (delivery info, original product info,
    // addon info) into XML.
    /***********************************************************************************************
    // OUT_CUR (containing delivery info)
    ***********************************************************************************************/
    Transformer transformer = ResultSetToXmlTransformer.getInstance(PD_ORDER_PRODUCTS, PD_ORDER_PRODUCT, true);
    Object transformed = transformer.transform( deliveryInfoMap.get(OUT_UPD_ORDER_CUR) );
    if ( transformed != null )
      this.hashXML.put(COMConstants.HK_UDI_DELIVERY_INFO_XML, (Document)transformed);


    // Convert dates to presentable format here (so front end doesn't have to)
    transformer = DateFormatTransformer.getInstance(IN_FORMAT, DATE_FORMAT);
    Closure closure = NodeUpdateClosure.getInstance(transformer, (Document)transformed);
    closure.execute(ORDER_PRODUCTS_XPATH+"updated_on/text()");
    closure.execute(ORDER_PRODUCTS_XPATH+"delivery_date/text()");
    closure.execute(ORDER_PRODUCTS_XPATH+"delivery_date_range_end/text()");
    closure.execute(ORDER_PRODUCTS_XPATH+"ship_date/text()");
    closure.execute(ORDER_PRODUCTS_XPATH+"exception_start_date/text()");
    closure.execute(ORDER_PRODUCTS_XPATH+"exception_end_date/text()");


    //Retrieve fields from PD_ORDER_PRODUCTS as required
    String occasion                 = getNodeValue(ORDER_PRODUCTS_XPATH + COMConstants.UDI_OCCASION, (Document)transformed);
    String originId                 = getNodeValue(ORDER_PRODUCTS_XPATH + COMConstants.UDI_ORIGIN_ID, (Document)transformed);
    String recipientCountry         = getNodeValue(ORDER_PRODUCTS_XPATH + COMConstants.UDI_RECIPIENT_COUNTRY, (Document)transformed);
    String recipientState           = getNodeValue(ORDER_PRODUCTS_XPATH + COMConstants.UDI_RECIPIENT_STATE, (Document)transformed);
    String recipientZip             = getNodeValue(ORDER_PRODUCTS_XPATH + COMConstants.UDI_RECIPIENT_ZIP_CODE, (Document)transformed);
    String sDeliveryDate            = getNodeValue(ORDER_PRODUCTS_XPATH + COMConstants.UDI_DELIVERY_DATE, (Document)transformed);
    String sDeliveryDateRangeEnd    = getNodeValue(ORDER_PRODUCTS_XPATH + COMConstants.UDI_DELIVERY_DATE_RANGE_END, (Document)transformed);
    String shippingKey              = getNodeValue(ORDER_PRODUCTS_XPATH + "shipping_key", (Document)transformed);
    String sourceCode               = getNodeValue(ORDER_PRODUCTS_XPATH + "source_code", (Document)transformed);


    // Get and transform delivery date list into XML.  But first we need to populate
    // a DeliveryInfoVO (note we only populate it with data DeliveryDateUTIL needs).
    SimpleDateFormat dateFormat = new SimpleDateFormat( DATE_FORMAT );
    SimpleDateFormat dayDateFormat = new SimpleDateFormat( DATE_FORMAT_WITH_DAY );
    Calendar date = Calendar.getInstance();

    //Even if a partner id (program name) is found for a source code, check if it belongs to
    //Discover, Upromise etc.  If so, do not require a membership field.
    //Note:
    // 1) WebOE has this hard-coded, and there are no database columns that we can drive this
    //    off of.  Tim S. knows about this, and once WebOE is fixed, this needs to be updated too.
    // 2) This method is called during initial page load as well as when page validation fails.
    //    The difference is that diVO.MembershipRequired will be populated in the latter.
    //    In that case, do not override
    if (diVO != null &&
        diVO.getMembershipRequired() != null)
      this.pageData.put("membership_required", diVO.getMembershipRequired());
    else
    {
      SourceMasterHandler sch = (SourceMasterHandler) CacheManager.getInstance().getHandler(SOURCE_CODE_HANDLER_NAME);
      SourceMasterVO sourceVo = sch.getSourceCodeById(sourceCode);
      String partnerId = sourceVo.getPartnerId();
      boolean membershipRequired = false;
      if( partnerId != null                                               &&
          partnerId.length() > 0                                          &&
          !partnerId.equalsIgnoreCase(COMConstants.CONS_DISCOVER_CODE)    &&
          !partnerId.equalsIgnoreCase(COMConstants.CONS_UPROMISE_CODE)    &&
          !partnerId.equalsIgnoreCase(COMConstants.CONS_UPROMISE1_CODE)   &&
          !partnerId.equalsIgnoreCase(COMConstants.CONS_CORP15_CODE)      &&
          !partnerId.equalsIgnoreCase(COMConstants.CONS_CORP20_CODE)      &&
          !partnerId.equalsIgnoreCase(COMConstants.CONS_ADVO_CODE)
        )
      {
        membershipRequired = true;
      }
      else
        membershipRequired = false;

      this.pageData.put("membership_required", membershipRequired?"Y":"N");
    }


    if (sDeliveryDate != null && !sDeliveryDate.equalsIgnoreCase(""))
    {
      //Create a Calendar Object
      Calendar cDeliveryDate = Calendar.getInstance();

      //Set the Calendar Object using the date retrieved
      cDeliveryDate.setTime(dateFormat.parse(sDeliveryDate));

      //Set the delivery date
      this.pageData.put("formatted_delivery_date", dayDateFormat.format(cDeliveryDate.getTime()));
    }


    if (sDeliveryDateRangeEnd != null && !sDeliveryDateRangeEnd.equalsIgnoreCase(""))
    {
      //Create a Calendar Object
      Calendar cDeliveryDateRangeEnd = Calendar.getInstance();

      //Set the Calendar Object using the date retrieved
      cDeliveryDateRangeEnd.setTime(dateFormat.parse(sDeliveryDateRangeEnd));

      //Set the delivery date
      this.pageData.put("formatted_delivery_date_range_end", dayDateFormat.format(cDeliveryDateRangeEnd.getTime()));
    }


    if (diVO == null)
    {
      if ( StringUtils.isNotEmpty(sDeliveryDate) )
      {
        date.setTime( dateFormat.parse( sDeliveryDate ) );
        divoDateInfo.setDeliveryDate( date );
      }
      if ( StringUtils.isNotEmpty(sDeliveryDateRangeEnd) )
      {
        date.setTime( dateFormat.parse( sDeliveryDateRangeEnd ) );
        divoDateInfo.setDeliveryDateRangeEnd( date );
      }
      divoDateInfo.setRecipientState(recipientState);
      divoDateInfo.setRecipientZipCode(recipientZip);
      divoDateInfo.setRecipientCountry(recipientCountry);
    }
    else
    {
      divoDateInfo.setDeliveryDate(diVO.getDeliveryDate());
      divoDateInfo.setDeliveryDateRangeEnd(diVO.getDeliveryDateRangeEnd());
      divoDateInfo.setRecipientState(diVO.getRecipientState());
      divoDateInfo.setRecipientZipCode(diVO.getRecipientZipCode());
      divoDateInfo.setRecipientCountry(diVO.getRecipientCountry());
    }
    divoDateInfo.setOccasion(occasion);
    divoDateInfo.setOriginId(originId);


    /***********************************************************************************************
    // OUT_ORIG_PRODUCT
    ***********************************************************************************************/
    transformer = ResultSetToXmlTransformer.getInstance(PD_ORIG_PRODUCTS, PD_ORIG_PRODUCT, true);
    transformed = transformer.transform( deliveryInfoMap.get(OUT_ORIG_PRODUCT) );
    if ( transformed != null )
      this.hashXML.put(COMConstants.HK_UDI_PRODUCT_INFO_XML, (Document)transformed);

    // Convert dates to presentable format here (so front end doesn't have to)
    transformer = DateFormatTransformer.getInstance(IN_FORMAT, DATE_FORMAT);
    closure = NodeUpdateClosure.getInstance(transformer, (Document)transformed);
    closure.execute(ORIG_PRODUCTS_XPATH+"ship_date/text()");
    closure.execute(ORIG_PRODUCTS_XPATH+"delivery_date/text()");
    closure.execute(ORIG_PRODUCTS_XPATH+"delivery_date_range_end/text()");


    // Get miles info.  If miles have not been posted, we'll need to get them
    // (otherwise they're already in the database fields being returned).
    String discountRewardType = getNodeValue(ORIG_PRODUCTS_XPATH + "discount_reward_type", (Document)transformed);
    if (REWARDS_MILES_INDICATOR.equals(discountRewardType))
    {
      String milesPointsPosted = getNodeValue(ORIG_PRODUCTS_XPATH + "miles_points_posted", (Document)transformed);
      if (milesPointsPosted == null || milesPointsPosted.equals("N"))
      {
        // Miles not posted, so get them
        String miles = getReward(this.req_orderDetailId);
        // Now put back in XML doc
        NodeList mpList = DOMUtil.selectNodes(((Document) transformed),ORIG_PRODUCTS_XPATH + "miles_points");
        if (mpList != null && mpList.getLength() > 0)
        {
          Element mpNode = (Element)mpList.item(0);
          Text node = (Text)mpNode.getFirstChild();
          if(node != null)
          {
            ArrayList paramList = new ArrayList();
            paramList.add(0,PD_ORIG_PRODUCTS);
            paramList.add(1,PD_ORIG_PRODUCT);
            paramList.add(2,"miles_points");
            // This update won't work if miles_points is originally empty, e.g., <miles_points/>
            DOMUtil.updateNodeValue((Document)transformed, paramList, miles);
          }
          else
          {
            // But this one will
            mpNode.appendChild(((Document) transformed).createTextNode(miles));
          }
        }
      }
    }


    /***********************************************************************************************
    // OUT_SHIP_COST
    ***********************************************************************************************/
    transformer = ResultSetToXmlTransformer.getInstance(PD_SHIP_COSTS, PD_SHIP_COST, true);
    transformed = transformer.transform( deliveryInfoMap.get(OUT_SHIP_COST) );
    if ( transformed != null )
      this.hashXML.put(COMConstants.HK_UDI_SHIP_COST_XML, (Document)transformed);


    /***********************************************************************************************
    // OUT_ORIG_ADD_ONS
    ***********************************************************************************************/
    transformer = ResultSetToXmlTransformer.getInstance(PD_ORIG_ADD_ONS, PD_ORIG_ADD_ON, true);
    transformed = transformer.transform( deliveryInfoMap.get(OUT_ORIG_ADD_ONS) );
    if ( transformed != null )
      this.hashXML.put(COMConstants.HK_UDI_ADDONS_XML, (Document)transformed);

    /***********************************************************************************************
    // OUT_ORIG_TAXES_CUR
    ***********************************************************************************************/
    transformer = ResultSetToXmlTransformer.getInstance(PD_ORDER_TAXES, PD_ORDER_TAX, true);
    transformed = transformer.transform( deliveryInfoMap.get(OUT_ORIG_TAXES_CUR) );
    if ( transformed != null )
      this.hashXML.put(COMConstants.HK_ORDER_TAX_XML, (Document)transformed);

    /**
     * OUT_PAYMENT_CUR
     */
    // Get data from DB
    com.ftd.customerordermanagement.dao.OrderDAO orderDAO = new com.ftd.customerordermanagement.dao.OrderDAO(this.con);
    Document doc = orderDAO.getPaymentInfo("ORDER", this.req_orderDetailId);
    this.hashXML.put(COMConstants.HK_PAYMENT_INFO_XML, doc);
  }


 /**---------------------------------------------------------------------------
  *
  * getNodeValue
  *
  * Private utility method to do xpath query to get a node value.
  *
  * @params String a_xpath       - Xpath to element
  * @params Document a_DocXml - XML document
  * @returns String              - Node value (or null if not found)
  */
  private String getNodeValue(String a_xpath, Document a_docXml) throws Exception
  {
    String retValue = null;
    NodeList nl = DOMUtil.selectNodes(a_docXml,a_xpath);
    if (nl != null && nl.getLength() > 0)
    {
      Text node = (Text)nl.item(0).getFirstChild();

      if(node != null)
        retValue = node.getNodeValue();
    }

    return retValue;
  }


 /**---------------------------------------------------------------------------
  *
  * updateDataFilterValue
  *
  * Private utility method to update a data filter value
  *
  * @params String a_variableName
  * @params String a_value
  * @returns none
  */
  private void updateDataFilterValue(String a_variableName, String a_value) throws Exception {
      HashMap dfParams = (HashMap) this.request.getAttribute(COMConstants.CONS_APP_PARAMETERS);
      if(dfParams == null)
        dfParams = new HashMap();

      dfParams.put(a_variableName, a_value);
      this.request.setAttribute(COMConstants.CONS_APP_PARAMETERS, dfParams);
  }


 /**----------------------------------------------------------------------------
  *
  * getReward()
  *
  */
  private String getReward(String a_orderDetailId) throws Exception
  {
    if (initialContextStr == null || ejbProviderUrl == null)
    {
      ConfigurationUtil config = ConfigurationUtil.getInstance();
      initialContextStr = config.getFrpGlobalParm(COMConstants.COM_CONTEXT, "INITIAL_CONTEXT_FACTORY");
      ejbProviderUrl = config.getFrpGlobalParm(COMConstants.COM_CONTEXT, "PR_EJB_PROVIDER_URL");
    }
    String reward = null;

// /*
try
{
// */
    RewardServiceEJB rewardServiceEJB = (RewardServiceEJB)EJBServiceLocator.getEJBean("RewardServiceEJB",
                                        RewardServiceEJBHome.class, initialContextStr, ejbProviderUrl, true);
    Float rewardFloat = rewardServiceEJB.getReward(a_orderDetailId);
    if (rewardFloat != null)
      reward = rewardFloat.toString();

    if (logger.isDebugEnabled())
      logger.debug("getReward - Used RewardServiceEJB to get reward: " + reward);
// /*
}
catch (Exception e)
{

}
finally
{
  if (StringUtils.isEmpty(reward))
    reward = "";
}
// */
    return reward;
  }


 /**---------------------------------------------------------------------------
  *
  * processStates() -
  *
  * @throws none
  */
  private void processStates() throws Exception
  {
    //statesXML will store the states info
    Document statesXML = DOMUtil.getDocument();

    //Instantiate the BasePageBuilder
    BasePageBuilder builder = new BasePageBuilder();

    //and retrieve the states info in an xml document
    statesXML = builder.retrieveStates(this.con, true);
    this.hashXML.put(COMConstants.HK_STATES,  statesXML);
  }


 /**---------------------------------------------------------------------------
  *
  * processCountries() -
  *
  * @throws none
  */
  private void processCountries() throws Exception
  {
    //statesXML will store the states info
    Document countriesXML = DOMUtil.getDocument();

    //Instantiate the BasePageBuilder
    BasePageBuilder builder = new BasePageBuilder();

    //and retrieve the states info in an xml document
    countriesXML = builder.retrieveCountries(this.con);
    this.hashXML.put(COMConstants.HK_COUNTRIES,  countriesXML);
  }


 /**---------------------------------------------------------------------------
  *
  * processAddrTypes() -
  *
  * @throws none
  */
  private void processAddrTypes() throws Exception
  {
    Document addrTypesXML = DOMUtil.getDocument();
    BasePageBuilder builder = new BasePageBuilder();
    addrTypesXML = builder.retrieveAddressTypes(this.con);
    this.hashXML.put(COMConstants.HK_ADDRESS_TYPES,  addrTypesXML);
  }


 /**---------------------------------------------------------------------------
  *
  * processGiftMessages() -
  *
  * @throws none
  */
  private void processGiftMessages() throws Exception
  {
    Document giftMsgXML = DOMUtil.getDocument();
    BasePageBuilder builder = new BasePageBuilder();
    giftMsgXML = builder.retrieveGiftMessages(this.con);
    this.hashXML.put(COMConstants.HK_GIFT_MESSAGES,  giftMsgXML);
  }


 /**---------------------------------------------------------------------------
  *
  * processSubHeader()
  *
  * This will handle placing subheader data (propagated in request)
  * back into the XML to continue the propagation.
  *
  * @throws none
  */
  private void processSubHeader() throws Exception
  {
    BasePageBuilder builder = new BasePageBuilder();
    this.hashXML.put(COMConstants.HK_SUB_HEADER, builder.retrieveSubHeaderData(request));
  }


 /**---------------------------------------------------------------------------
  *
  * processBreadcrumbs()
  *
  * @throws none
  */
  private void processBreadcrumbs() throws Exception
  {
    HashMap optionalParams = null;
    BreadcrumbUtil bcUtil = new BreadcrumbUtil();

    //bcUtil.getBreadcrumbXML(
                          //request,
                          //Title of breadcrumb,
                          //Name of Structs Action,
                          //Action parameter,
                          //Error display page,
                          //Flag indicating if breadcrumb should be a link, ,
                          //Optional hash of additional name/value parameters for this page
                          //order detail id
                      //)

    Document bcXML = bcUtil.getBreadcrumbXML(
                            this.request, 												//request
                            BREADCRUMB_TITLE_DELIVERY_INFO, 			//title
                            LOAD_DELIVERY_INFO,										//name
                            COMConstants.ACTION_UDI_GET_UPDATED, 	//action parm
                            "Error",															//error page
                            "true", 															//link=true; no link=false
                            optionalParams, 											//optional parms 
                            this.req_orderDetailId); 							//order detail id

    this.hashXML.put("breadcrumbs",  bcXML);
  }


 /**---------------------------------------------------------------------------
  *
  * releaseLock() - if the lock exists, and CSR cancels an action, release the Payment lock
  *
  */
  public void releaseLock() throws Exception
  {
    //Instantiate RefundDAO
    LockDAO lockDAO = new LockDAO(this.con);

    //Call releaseLock method in the DAO
    lockDAO.releaseLock(this.sessionId, this.csrId, this.req_orderDetailId, COMConstants.CONS_ENTITY_TYPE_ORDER_DETAILS);
  }
  
    /*******************************************************************************************
     * repCanAccessOrder()
     *******************************************************************************************
      * This method checks to see if the rep is trying to access a preferred partner order and whether or not
      * that rep has access to that type of partner order.
      *
      * @param1 String - sourceCode
      * @return boolean
      * @throws Exception
      */
      private boolean repCanAccessOrder(String sourceCode)
              throws Exception
      {
       
          String repCanAccessOrder = null;
          String preferredPartnerName = null;
          String preferredPartnerResource = null;
            
          PartnerVO partnerVO = FTDCommonUtils.getPreferredPartnerBySource(sourceCode);
                       
          if(partnerVO != null && partnerVO.getSourceCode().equalsIgnoreCase(sourceCode))
          {
            preferredPartnerName = partnerVO.getPartnerName();
            preferredPartnerResource = partnerVO.getPreferredProcessingResource();
          }
          
          if(  preferredPartnerName != null )
          {
            //retrieve preferred partner resource and check to see if the rep has that role
             SecurityManager securityManager = SecurityManager.getInstance();
             String context = (String) request.getParameter(COMConstants.CONTEXT);
             String token = (String) request.getParameter(COMConstants.SEC_TOKEN);
             
             if (securityManager.assertPermission(context, token, preferredPartnerResource, COMConstants.VIEW))
             {
                 repCanAccessOrder = "Y";
                 pageData.put("repCanAccessOrder", repCanAccessOrder);
             }  
             else
             {
                 repCanAccessOrder = "N";
                 pageData.put("repCanAccessOrder", repCanAccessOrder);
                 String sourceCodeRestrictionMessage = ConfigurationUtil.getInstance().getContentWithFilter(this.con, COMConstants.PREFERRED_PARTNER_CONTEXT, 
                                                    COMConstants.PREFERRED_PARTNER_SOURCE_CODE_RESTRICTION, preferredPartnerName, null);
                 generateErrorNode(sourceCodeRestrictionMessage, null, COMConstants.CONS_ERROR_POPUP_INDICATOR_OK, null, null);
                 return false;
             }
          }
        
        return true;
      }
   

       
    /*******************************************************************************************
     * generateErrorNode()
     *******************************************************************************************
     *
     * Adds an error message to the error message XML doc
     *
     * @params messageText - Message text
     * @params fieldInError - Name of erred field (optional).
     * @params popupType - Type of popup window (optional).
     * @params popupConfirmation - Return page if popup confirmation selected (optional).
     * @params popupCancel - Return page if confirmation is cancelled (optional).
     */
     private void generateErrorNode(String messageText, String fieldInError,
                                      String popupType, String popupConfirmation, String popupCancel)
           throws Exception
     {

       String l_popupType = popupType;
       if (fieldInError == null && popupType == null) {
         l_popupType = POPUP_INDICATOR_OK;
       }

       // Generate top node
       Document errorMsgsXML = (Document) DOMUtil.getDefaultDocument();
       Element errMsgsTopElement = errorMsgsXML.createElement("ERROR_MESSAGES");
       errorMsgsXML.appendChild(errMsgsTopElement);

       if (messageText != null)
       {
         Element errorMsgElement = errorMsgsXML.createElement("ERROR_MESSAGE");
         errMsgsTopElement.appendChild(errorMsgElement);

         Element errMsgNode = errorMsgsXML.createElement("TEXT");
         errorMsgElement.appendChild(errMsgNode);
         errMsgNode.appendChild(errorMsgsXML.createTextNode(messageText));

         Element errFieldNode = errorMsgsXML.createElement("FIELDNAME");
         errorMsgElement.appendChild(errFieldNode);
         if (fieldInError != null)
           errFieldNode.appendChild(errorMsgsXML.createTextNode(fieldInError));

         Element popupIndicatorNode = errorMsgsXML.createElement("POPUP_INDICATOR");
         errorMsgElement.appendChild(popupIndicatorNode);
         if (l_popupType != null)
           popupIndicatorNode.appendChild(errorMsgsXML.createTextNode(l_popupType));

         Element popupGotoNode = errorMsgsXML.createElement("POPUP_GOTO_PAGE");
         errorMsgElement.appendChild(popupGotoNode);
         if (popupConfirmation != null)
           popupGotoNode.appendChild(errorMsgsXML.createTextNode(popupConfirmation));

         Element popupCancelNode = errorMsgsXML.createElement("POPUP_CANCEL_PAGE");
         errorMsgElement.appendChild(popupCancelNode);
         if (popupCancel != null)
           popupCancelNode.appendChild(errorMsgsXML.createTextNode(popupCancel));
       }

       this.hashXML.put(COMConstants.HK_ERROR_MESSAGES,  errorMsgsXML);

     }


}