package com.ftd.mo.bo;
import java.math.BigDecimal;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.dao.BillingDAO;
import com.ftd.customerordermanagement.dao.OrderDAO;
import com.ftd.customerordermanagement.util.COMDeliveryDateUtil;
import com.ftd.customerordermanagement.util.DeliveryDateConfiguration;
import com.ftd.customerordermanagement.util.DeliveryDateParameters;
import com.ftd.customerordermanagement.vo.AddOnVO;
import com.ftd.customerordermanagement.vo.BillingVO;
import com.ftd.customerordermanagement.vo.OrderBillVO;
import com.ftd.customerordermanagement.vo.OrderDetailVO;
import com.ftd.ftdutilities.DeliveryDateUTIL;
import com.ftd.ftdutilities.OEDeliveryDateParm;
import com.ftd.mo.dao.UpdateOrderDAO;
import com.ftd.mo.validation.ValidationConfiguration;
import com.ftd.mo.validation.ValidationLevels;
import com.ftd.mo.validation.ValidationRules;
import com.ftd.mo.validation.Validator;
import com.ftd.mo.validation.impl.DeliveryDateValidatorFactory;
import com.ftd.mo.vo.DeliveryInfoVO;
import com.ftd.osp.utilities.AddOnUtility;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.order.RecalculateOrderBO;
import com.ftd.osp.utilities.order.vo.AddOnsVO;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.order.vo.RecipientAddressesVO;
import com.ftd.osp.utilities.order.vo.RecipientsVO;
import com.ftd.osp.utilities.order.vo.TaxVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.security.SecurityManager;
import com.ftd.security.cache.vo.UserInfo;
//import oracle.xml.parser.v2.DOMParser;
//
import org.apache.commons.lang.StringUtils;


public class UpdateProductDetailBO
{

/*******************************************************************************************
 * Class level variables
 *******************************************************************************************/
  private HttpServletRequest  request                   = null;
  private Connection          con                       = null;
  private static Logger       logger                    = new Logger("com.ftd.mo.bo.UpdateProductDetailBO");


  //request parameters required to be sent back to XSL all the time.
  private String              buyerName                 = null;
  private String              categoryIndex             = null;
  private String              companyId                 = null;
  private String              customerId                = null;
  private String              deliveryDate              = "";
  private String              deliveryDateRange         = "";
  private String              externalOrderNumber       = null;
  private String              masterOrderNumber         = null;
  private String              occasion                  = null;
  private String              occasionText              = null;
  private String              orderDetailId             = null;
  private String              orderGuid                 = null;
  private String              originId                  = null;
  private  String             origProductId            = null;
  private String              pricePointId              = null;
  private String              recipientCity             = null;
  private String              recipientCountry          = null;
  private String              recipientState            = null;
  private String              recipientZipCode          = null;
  private String              sourceCode                = null;


  //other variables needed for this class
  private boolean             addOnsDifferent           = false;
  private Calendar            cDeliveryDate             = null;
  private Calendar            cDeliveryDateRange        = null;
  private String              color1                    = null;
  private String              color1Description         = null;
  private String              color2                    = null;
  private String              color2Description         = null;
  private String              csrId                     = null;
  private DeliveryInfoVO      divo                      = new DeliveryInfoVO();
  private String              exceptionStartDate        = null;
  private String              exceptionEndDate          = null;
  private HashMap             hashXML                   = new HashMap();
  private String              ignoreError               = "N";
  private String              nextAvailableDeliveryDate = "";
  private String              nextGlobalAvailableDeliveryDate = "";
  private HashMap             pageData                  = new HashMap();
  private String              productId                 = null;
  private String              productAmount             = "0";
  private String              productSubType            = null;
  private String              productType               = null;
  private String              sessionId                 = null;
  private Calendar            shipDate                  = null;
  private String              shipMethod                = null;
  private String              shipMethodCarrier         = null;
  private String              shipMethodFlorist         = null;
  private String              sizeIndicator             = null;
  private String              subcode                   = null;
  private String              substitutionIndicator     = null;
  private String              variablePriceSelected     = null;
  private String              vendorFlag                = null;
  private String              zipSundayFlag             = null;
  private String              personalGreetingId        = null;

  //AddOns
  private List<AddOnVO>            requestAddOnVOList        = new ArrayList<AddOnVO>();       // array list of all the new addons
  private HashMap<String,AddOnVO>  tempTableAddOnVOHash      = new HashMap<String,AddOnVO>();  // key = addon code
  private List<AddOnVO>            tempTableAddOnVOList      = new ArrayList<AddOnVO>();       // Array list of addons in order_addons_update

  //product detail vo
  private OrderDetailVO       origOrderDetailVO         = new OrderDetailVO();
  private OrderBillVO         origOrderBillVO           = new OrderBillVO();
  private OrderDetailVO       requestOrderDetailVO      = new OrderDetailVO();
  private OrderBillVO         requestOrderBillVO        = new OrderBillVO();
  private OrderDetailVO       tempTableOrderDetailVO    = new OrderDetailVO();
  private OrderBillVO         tempTableOrderBillVO      = new OrderBillVO();


  //constants
  private static final String CONTINUE_MSG                    = "Do you wish to continue?";
  private static final String DATE_FORMAT                     = "EEE MM/dd/yyyy";
  private static final String DISPLAY_PRODUCT_CATEGORY_BUTTON = "DisplayProductCategoryButton";
  private static final String IGNORE_ERROR                    = "IgnoreError";
  private static final String INPUT_DATE_FORMAT               = "MM/dd/yyyy";
  private static final String LOAD_PRODUCT_CATEGORY           = "loadProductCategory.do";
  private static final String POPUP_INDICATOR_OK              = "O";

  // for easier testing
  protected SecurityManager sm;

/*******************************************************************************************
 * Constructor 1
 *******************************************************************************************/
  public UpdateProductDetailBO()
  {
  }


/*******************************************************************************************
 * Constructor 2
 *******************************************************************************************/
  public UpdateProductDetailBO(HttpServletRequest request, Connection con)
  {
    this.request = request;
    this.con = con;
  }


/*******************************************************************************************
  * processRequest()
  ******************************************************************************************
  *
  * @return HashMap - contains 0 - many XML documents of data, 1 HashMap each for page
  *                   details and search criteria
  * @throws
  */
  public HashMap processRequest() throws Exception
  {
    //HashMap that will be returned to the calling class
    HashMap returnHash = new HashMap();

    //Get info from the request object
    getRequestInfo(this.request);

    //Get config file info
    getConfigInfo();

    //process the action
    processMain();

    //retrieve all the Documents from hashXML and put them in returnHash
    //retrieve the keyset
    Set ks1 = hashXML.keySet();
    Iterator iter = ks1.iterator();
    String key = null;
    Document doc = null;

    //Iterate thru the keyset
    while(iter.hasNext())
    {
      //get the key
      key = iter.next().toString();

      //retrieve the XML document
      doc = (Document) hashXML.get(key);

      //put the XML object in the returnHash
      returnHash.put(key, (Document) doc);
    }

    //append pageData to returnHash
    returnHash.put("pageData",        (HashMap)this.pageData);

    return returnHash;
  }



/*******************************************************************************************
  * getRequestInfo(HttpServletRequest request)
  ******************************************************************************************
  * Retrieve the info from the request object
  *
  * @param  request
  * @return 
  * @throws Exception
  */
  private void getRequestInfo(HttpServletRequest request) throws Exception
  {

    /****************************************************************************************
     *  Retrieve order/search specific parameters
     ***************************************************************************************/
    //retrieve the buyer_full_name
    if(request.getParameter(COMConstants.BUYER_FULL_NAME)!=null)
    {
      this.buyerName = request.getParameter(COMConstants.BUYER_FULL_NAME);
    }

    //retrieve the category Index
    if(request.getParameter(COMConstants.CATEGORY_INDEX)!=null)
    {
      this.categoryIndex = request.getParameter(COMConstants.CATEGORY_INDEX);
    }

    //retrieve color1 description
    if(request.getParameter(COMConstants.COLOR_FIRST_CHOICE)!=null)
    {
      this.color1Description = request.getParameter(COMConstants.COLOR_FIRST_CHOICE).toUpperCase();
    }

    //retrieve color2 description
    if(request.getParameter(COMConstants.COLOR_SECOND_CHOICE)!=null)
    {
      this.color2Description = request.getParameter(COMConstants.COLOR_SECOND_CHOICE).toUpperCase();
    }

    //retrieve the company id
    if(request.getParameter(COMConstants.COMPANY_ID)!=null)
    {
      this.companyId = request.getParameter(COMConstants.COMPANY_ID);
    }

    //retrieve the customer id
    if(request.getParameter(COMConstants.CUSTOMER_ID)!=null)
    {
      this.customerId = request.getParameter(COMConstants.CUSTOMER_ID);
    }

    //retrieve the delivery date
    if(request.getParameter(COMConstants.DELIVERY_DATE)!=null)
    {
      this.deliveryDate = request.getParameter(COMConstants.DELIVERY_DATE);
    }

    //retrieve the delivery date range end
    if(request.getParameter(COMConstants.DELIVERY_DATE_RANGE_END)!=null)
    {
      this.deliveryDateRange = request.getParameter(COMConstants.DELIVERY_DATE_RANGE_END);
    }

    //retrieve the exception start date
    if(request.getParameter(COMConstants.EXCEPTION_START_DATE)!=null)
    {
      this.exceptionStartDate = request.getParameter(COMConstants.EXCEPTION_START_DATE);
    }

    //retrieve the exception end date
    if(request.getParameter(COMConstants.EXCEPTION_END_DATE)!=null)
    {
      this.exceptionEndDate = request.getParameter(COMConstants.EXCEPTION_END_DATE);
    }

    //retrieve the external order number
    if(request.getParameter(COMConstants.EXTERNAL_ORDER_NUMBER)!=null)
    {
      this.externalOrderNumber = request.getParameter(COMConstants.EXTERNAL_ORDER_NUMBER);
    }

    //retrieve the indicator which will help us bypass the error
    if(request.getParameter(COMConstants.IGNORE_ERROR)!=null)
    {
      this.ignoreError = request.getParameter(COMConstants.IGNORE_ERROR).toUpperCase();
    }
    if (this.ignoreError == null || this.ignoreError.equalsIgnoreCase(""))
      this.ignoreError = COMConstants.CONS_NO;

    //retrieve the master order number
    if(request.getParameter(COMConstants.MASTER_ORDER_NUMBER)!=null)
    {
      this.masterOrderNumber = request.getParameter(COMConstants.MASTER_ORDER_NUMBER);
    }

    //retrieve the occasion id
    if(request.getParameter(COMConstants.OCCASION)!=null)
    {
      this.occasion = request.getParameter(COMConstants.OCCASION);
    }

    //retrieve the occasion text
    if(request.getParameter(COMConstants.OCCASION_TEXT)!=null)
    {
      this.occasionText = request.getParameter(COMConstants.OCCASION_TEXT);
    }

    //retrieve the order detail id
    if(request.getParameter(COMConstants.ORDER_DETAIL_ID)!=null)
    {
      this.orderDetailId = request.getParameter(COMConstants.ORDER_DETAIL_ID);
    }

    //retrieve the order guid
    if(request.getParameter(COMConstants.ORDER_GUID)!=null)
    {
      this.orderGuid = request.getParameter(COMConstants.ORDER_GUID);
    }

    //retrieve the orig product id
    if(request.getParameter(COMConstants.ORIG_PRODUCT_ID)!=null)
    {
      this.origProductId = (request.getParameter(COMConstants.ORIG_PRODUCT_ID)).toUpperCase();
    }

    //retrieve the origin id
    if(request.getParameter(COMConstants.ORIGIN_ID)!=null)
    {
      this.originId = request.getParameter(COMConstants.ORIGIN_ID);
    }

    //retrieve the new product amount
    if(request.getParameter(COMConstants.PRODUCT_AMOUNT)!=null)
    {
      this.productAmount = request.getParameter(COMConstants.PRODUCT_AMOUNT);
    }

    //retrieve the price point id
    if(request.getParameter(COMConstants.PRICE_POINT_ID)!=null)
    {
      this.pricePointId = request.getParameter(COMConstants.PRICE_POINT_ID);
    }

    //retrieve the product id
    if(request.getParameter(COMConstants.PRODUCT_ID)!=null)
    {
      this.productId = (request.getParameter(COMConstants.PRODUCT_ID)).toUpperCase();
    }

    //retrieve the product sub type
    if(request.getParameter(COMConstants.PRODUCT_SUB_TYPE)!=null)
    {
      this.productSubType = (request.getParameter(COMConstants.PRODUCT_SUB_TYPE)).toUpperCase();
    }

    //retrieve the product type
    if(request.getParameter(COMConstants.PRODUCT_TYPE)!=null)
    {
      this.productType = (request.getParameter(COMConstants.PRODUCT_TYPE)).toUpperCase();
    }

    //retrieve the city
    if(request.getParameter(COMConstants.RECIPIENT_CITY)!=null)
    {
      this.recipientCity = request.getParameter(COMConstants.RECIPIENT_CITY);
    }

    //retrieve the recipientCountry id
    if(request.getParameter(COMConstants.RECIPIENT_COUNTRY)!=null)
    {
      this.recipientCountry = request.getParameter(COMConstants.RECIPIENT_COUNTRY);
    }

    //retrieve the recipientState
    if(request.getParameter(COMConstants.RECIPIENT_STATE)!=null)
    {
      this.recipientState = request.getParameter(COMConstants.RECIPIENT_STATE);
    }

    //retrieve the recipient postal code
    if(request.getParameter(COMConstants.RECIPIENT_ZIP_CODE)!=null)
    {
      this.recipientZipCode = request.getParameter(COMConstants.RECIPIENT_ZIP_CODE);
    }

    //retrieve the ship method
    if(request.getParameter(COMConstants.SHIP_METHOD)!=null)
    {
      this.shipMethod = request.getParameter(COMConstants.SHIP_METHOD);
    }

    //retrieve the ship method carrier
    if(request.getParameter(COMConstants.SHIP_METHOD_CARRIER)!=null)
    {
      this.shipMethodCarrier = request.getParameter(COMConstants.SHIP_METHOD_CARRIER);
    }

    //retrieve the ship method florist
    if(request.getParameter(COMConstants.SHIP_METHOD_FLORIST)!=null)
    {
      this.shipMethodFlorist = request.getParameter(COMConstants.SHIP_METHOD_FLORIST);
    }

    //retrieve the size indicator
    if(request.getParameter(COMConstants.SIZE_INDICATOR)!=null)
    {
      this.sizeIndicator = request.getParameter(COMConstants.SIZE_INDICATOR);
    }

    //retrieve the source code
    if(request.getParameter(COMConstants.SOURCE_CODE)!=null)
    {
      this.sourceCode = request.getParameter(COMConstants.SOURCE_CODE);
    }

    //retrieve the subcode
    if(request.getParameter(COMConstants.SUBCODE)!=null)
    {
      this.subcode = request.getParameter(COMConstants.SUBCODE);
    }

    //retrieve the substitution indicator
    if(request.getParameter(COMConstants.SUBSTITUTION_INDICATOR)!=null)
    {
      this.substitutionIndicator = request.getParameter(COMConstants.SUBSTITUTION_INDICATOR);
    }
    if (StringUtils.isEmpty(this.substitutionIndicator))
      this.substitutionIndicator = "N";
    
    //retrieve the variable price selected
    if(request.getParameter("variable_price_selected")!=null)
    {
      this.variablePriceSelected = request.getParameter("variable_price_selected");
    }

    //retrieve the zip sunday flag
    if(request.getParameter(COMConstants.ZIP_SUNDAY_FLAG)!=null)
    {
      this.zipSundayFlag = request.getParameter(COMConstants.ZIP_SUNDAY_FLAG);
    }
    if (this.zipSundayFlag == null || this.zipSundayFlag.equalsIgnoreCase(""))
      this.zipSundayFlag = COMConstants.CONS_NO;


    /****************************************************************************************
     *  Retrieve Addon info 
     ***************************************************************************************/

    Enumeration reqParams = request.getParameterNames();
    while(reqParams.hasMoreElements()) {
        String curParam = (String) reqParams.nextElement();
        if (curParam != null) {

            if (curParam.equals("addonNew_optVase")) {

                // Vase addon
                String curVase = (String) request.getParameter(curParam);
                Pattern regex = Pattern.compile("(.+)---(.+)");
                Matcher m = regex.matcher(curVase);
                if (m.find()) {
                    AddOnVO addon = new AddOnVO();
                    addon.setOrderDetailId(new Long(this.orderDetailId).longValue());
                    addon.setUpdatedBy(this.csrId);
                    addon.setAddOnCode(m.group(1));
                    addon.setAddOnQuantity(1);
                    // addon.setAddOnTypeDescription("Huh?");  
                    // addon.setPrice(m.group(2));  ???
                    this.requestAddOnVOList.add(addon);
                    logger.debug("Addon vase request parameter " + m.group(1)); 
                }

            } else if (curParam.equals("addonNew_optCard")) {

                // Card addon
                String curCard = (String) request.getParameter(curParam);
                Pattern regex = Pattern.compile("(.+)---(.+)");
                Matcher m = regex.matcher(curCard);
                if (m.find() && !(m.group(1).equals("noCard"))) {
                    AddOnVO addon = new AddOnVO();
                    addon.setOrderDetailId(new Long(this.orderDetailId).longValue());
                    addon.setUpdatedBy(this.csrId);
                    addon.setAddOnCode(m.group(1));
                    addon.setAddOnQuantity(1);
                    // addon.setAddOnTypeDescription("Huh?"); 
                    // addon.setPrice(m.group(2));  ???
                    this.requestAddOnVOList.add(addon);
                    logger.debug("Addon card request parameter " + m.group(1)); 
                }

            } else if ((curParam.indexOf("addonNew_") > -1) && (! curParam.endsWith("_quantity")) && (! curParam.endsWith("_price"))) {
            
                // Normal addons
                String curAddon = (String) request.getParameter(curParam);
                String curQuantity = (String) request.getParameter(curParam + "_quantity");
                long curQtyLong;
                // String curPrice = (String) request.getParameter(curParam + "_price");  ???
                logger.debug("Addon request parameter/quantity: " + curAddon + "/" + curQuantity); 
                try {
                    curQtyLong = Integer.parseInt(curQuantity);
                    if (curQtyLong <= 0) {
                      curQtyLong = 0;
                    }
                } catch(NumberFormatException nfe) {
                  curQtyLong = 0;
                }
                AddOnVO addon = new AddOnVO();
                addon.setOrderDetailId(new Long(this.orderDetailId).longValue());
                addon.setUpdatedBy(this.csrId);
                addon.setAddOnCode(curAddon);
                addon.setAddOnQuantity(curQtyLong);
                // addon.setPrice(curPrice);  ???
                this.requestAddOnVOList.add(addon);
            }
        }
    }


    /****************************************************************************************
     *  Retrieve Security Info
     ***************************************************************************************/
    this.sessionId   = request.getParameter(COMConstants.SEC_TOKEN);


    /******************************************************************************************
     * Set the calendar objects for dates
     *****************************************************************************************/
    SimpleDateFormat dateFormat = new SimpleDateFormat( INPUT_DATE_FORMAT );

    if ( StringUtils.isNotEmpty(this.deliveryDate) )
    {
      //Create a Calendar Object
      this.cDeliveryDate = Calendar.getInstance();
      //Set the Calendar Object using the date retrieved in sCreatedOn
      this.cDeliveryDate.setTime( dateFormat.parse( this.deliveryDate ) );
    }

    if ( StringUtils.isNotEmpty(this.deliveryDateRange) )
    {
      //Create a Calendar Object
      this.cDeliveryDateRange = Calendar.getInstance();
      //Set the Calendar Object using the date retrieved in sCreatedOn
      this.cDeliveryDateRange.setTime( dateFormat.parse( this.deliveryDateRange ) );
    }


  }


/*******************************************************************************************
  * getConfigInfo()
  ******************************************************************************************
  * Retrieve the info from the configuration file
  *
  * @param 
  * @return 
  * @throws Exception
  */
  private void getConfigInfo() throws Exception
  {

    ConfigurationUtil cu = null;
    cu = ConfigurationUtil.getInstance();

    //get csr Id
    String security = cu.getProperty(COMConstants.PROPERTY_FILE,COMConstants.SECURITY_IS_ON);
    boolean securityIsOn = security.equalsIgnoreCase("true") ? true : false;

   //delete this before going to production.  the following should only check on securityIsOn
    if(securityIsOn || this.sessionId != null)
    {
      if (sm == null)
      sm = SecurityManager.getInstance();
      UserInfo ui = sm.getUserInfo(this.sessionId);
      this.csrId = ui.getUserID();
    }

  }


/*******************************************************************************************
  * processMain()
  ******************************************************************************************
  *
  * @param  
  * @return 
  * @throws Exception
  */
  private void processMain() throws Exception
  {
    //determine the vendor flag.
    determineVendorVsFloristOrder();

    //if its a vendor order, recalculate ship date.  the ship date should be null for florist orders
    if  ( this.vendorFlag.equalsIgnoreCase(COMConstants.CONS_YES)                     &&
          this.shipMethod != null                                                     &&
          !this.shipMethod.equalsIgnoreCase("")                                       &&
          !this.shipMethod.equalsIgnoreCase(COMConstants.CONS_DELIVERY_SAME_DAY_CODE) &&
          !this.shipMethod.equalsIgnoreCase(COMConstants.CONS_DELIVERY_FLORIST_CODE)
        )
      computeShipDate();
    else
      this.shipDate = null;

    //retrieve the list of all colors, and get the color code for the description passed.
    retrieveColorInfo();

    //retrieve the order specific data and build the xml
    //get the order specific data
    processRetrieveOrderData(); //this will also build the original vo.
    buildVOFromRequest();

    boolean validationPassed = performValidate();

    //note that if the validation does not pass, the performValidate() method will set the error
    //message and the XSL variable accordingly.  No need for an else{} clause.
    if (validationPassed)
    {
      checkAddonDifference();
      processUpdateOrder();
      this.pageData.put("XSL", COMConstants.XSL_LOAD_DELIVERY_CONF_ACTION);
    }

  }


/*******************************************************************************************
 * determineVendorVsFloristOrder()
 *******************************************************************************************
  * This method is used to call the
  *
  */
  private void determineVendorVsFloristOrder()
  {
    if (this.shipMethodCarrier == null || this.shipMethodCarrier.equalsIgnoreCase(""))
      this.shipMethodCarrier = COMConstants.CONS_NO;
    if (this.shipMethodFlorist == null || this.shipMethodFlorist.equalsIgnoreCase(""))
      this.shipMethodFlorist = COMConstants.CONS_YES;

    /*
     * There could be 3 combinations:
     *
     * shipMethodCarrier = N and shipMethodFlorist = Y - florist order
     * shipMethodCarrier = Y and shipMethodFlorist = N - vendor order.  however, it could be the
     *                                                   case that while placing order in weboe,
     *                                                   the order could have gone Florist.  So,
     *                                                   have to check for the vendorFlagOriginal
     * shipMethodCarrier = Y and shipMethodFlorist = Y - could be either vendor or flosrist order
     *                                                   should be displayed as a vendor order.
     *
     */
    if (this.shipMethodCarrier.equalsIgnoreCase(COMConstants.CONS_NO) &&
        this.shipMethodFlorist.equalsIgnoreCase(COMConstants.CONS_YES))
    {
      this.vendorFlag = COMConstants.CONS_NO;
    }
    else
    {
      if (this.origProductId.equalsIgnoreCase(this.productId)               &&
          this.shipMethodFlorist.equalsIgnoreCase(COMConstants.CONS_NO)     &&
          (this.shipMethod == null || this.shipMethod.equalsIgnoreCase("") || this.shipMethod.equalsIgnoreCase("SD"))
         )
      {
        this.vendorFlag = COMConstants.CONS_NO;
      }
      else
      {
        this.vendorFlag = COMConstants.CONS_YES;
      }
    }

  }



/*******************************************************************************************
 * processRetrieveOrderData()
 *******************************************************************************************
  * This method is used to call the
  *
  */
  private void processRetrieveOrderData() throws Exception
  {
    //Instantiate CustomerDAO
    UpdateOrderDAO uoDAO = new UpdateOrderDAO(this.con);

    //hashmap that will contain the output from the stored proc
    HashMap tempOrderInfo = new HashMap();

    //Call getCustomerAcct method in the DAO
    tempOrderInfo = uoDAO.getOrderDetailsUpdate(this.orderDetailId,
                            COMConstants.CONS_ENTITY_TYPE_PRODUCT_DETAIL, false, this.csrId, this.productId );

    buildVOFromTables(tempOrderInfo);

  }


/*******************************************************************************************
 * processUpdateOrder()
 *******************************************************************************************
  * This method is used to update the order
  *
  */
  private void processUpdateOrder() throws Exception
  {

    //trip the modify_order_update flag and reset the product amounts
    setModifyFlag();
    resetProductAmounts();

    processProductDetail();

    if (this.addOnsDifferent)
      processAddOns();

    processProductTotals();

  }



/*******************************************************************************************
 * processProductDetail()
 *******************************************************************************************
  * This method is used to update the order
  *
  */
  private void processProductDetail() throws Exception
  {
    //Instantiate UpdateOrderDAO
    UpdateOrderDAO uoDAO = new UpdateOrderDAO(this.con);

    HashMap updateResults = new HashMap();

    updateResults = uoDAO.updateProductDetailInfo(this.orderDetailId,
                                  this.csrId,
                                  this.requestOrderDetailVO.getDeliveryDate(),
                                  this.requestOrderDetailVO.getDeliveryDateRangeEnd(),
                                  this.requestOrderDetailVO.getProductId(),
                                  this.requestOrderDetailVO.getColor1(),
                                  this.requestOrderDetailVO.getColor2(),
                                  this.requestOrderDetailVO.getSubstitutionIndicator(),
                                  this.requestOrderDetailVO.getSpecialInstructions(),
                                  this.requestOrderDetailVO.getShipMethod(),
                                  this.requestOrderDetailVO.getSizeIndicator(),
                                  this.shipDate,
                                  this.requestOrderDetailVO.getSubcode(),
                                  this.requestOrderDetailVO.getPersonalGreetingId());


    String status = (String) updateResults.get(COMConstants.STATUS_PARAM);
    if( status.equalsIgnoreCase(COMConstants.CONS_NO))
    {
        String message = (String) updateResults.get(COMConstants.MESSAGE_PARAM);
        throw new Exception(message);
    }

  }


/*******************************************************************************************
 * processProductTotals()
 *******************************************************************************************
  * This method is used to update the order
  *
  */
  private void processProductTotals() throws Exception
  {
    //Instantiate UpdateOrderDAO
    UpdateOrderDAO uoDAO = new UpdateOrderDAO(this.con);

    //note that uoDAO.recalculateUpdatedPricingInfo(this.orderDetailId, this.csrId) is designed
    //to get the info from the database; this would bypass the product related changes that the CSR
    //updated on the page.  Thus, we have to create a VO ourselves instead of generating it from a
    //generic method.

    //build the order details vo
    OrderDetailsVO odVO = buildRecalculateOrderDetailsVO();

    //build and calculate the price for the new product
    OrderVO newOrderVO = recalculateNewOrderAmounts(odVO);

    //get the updated order detail vo.
    odVO = (OrderDetailsVO) newOrderVO.getOrderDetail().get(0);

    //update the prices
    uoDAO.updateODUAmounts(odVO, this.tempTableOrderBillVO, this.orderDetailId, this.csrId);

    //if the miles/points on the original product/order is different than the newly calculated ones,
    //update the databse with the new miles/points
    if (odVO.getMilesPoints() != null &&
        new Double(odVO.getMilesPoints()).doubleValue() != this.tempTableOrderDetailVO.getMilesPoint())
    {
        updateMilesPoints(odVO.getMilesPoints());
    }
    
    BillingVO bVO = new BillingVO();
    bVO.setOrderDetailId(new Long(this.orderDetailId).longValue());
    logger.info("processProductTotals(): " + bVO.getOrderDetailId());
    bVO.setProductAmt(odVO.getProductsAmount() == null ? new Double("0") : new Double(odVO.getProductsAmount()));
    bVO.setAddOnAmt(odVO.getAddOnAmount() == null ? new Double("0") : new Double(odVO.getAddOnAmount()));
    bVO.setServiceFee(odVO.getServiceFeeAmount() == null ? new Double("0") : new Double(odVO.getServiceFeeAmount()));
    bVO.setShippingFee(odVO.getShippingFeeAmount() == null ? new Double("0") : new Double(odVO.getShippingFeeAmount()));
    bVO.setDiscountAmt(odVO.getDiscountAmount() == null ? new Double("0") : new Double(odVO.getDiscountAmount()));
    bVO.setShippingTax(odVO.getShippingTax() == null ? new Double("0") : new Double(odVO.getShippingTax()));
    bVO.setProductTax(odVO.getProductTax() == null ? new Double("0") : new Double(odVO.getProductTax()));
    bVO.setServiceFeeTax(odVO.getServiceFeeTax() == null ? new Double("0") : new Double(odVO.getServiceFeeTax()));
    bVO.setDiscountProdPrice(odVO.getDiscountedProductPrice() == null ? new Double("0") : new Double(odVO.getDiscountedProductPrice()));
    bVO.setDiscountType(odVO.getDiscountType());
    bVO.setAddtBillInd("N");
    bVO.setCreatedBy("SYS");
    bVO.setBillStatus("Unbilled");
    bVO.setAcctTransInd("N");

   // List taxList = odVO.getTaxVOs();
    List<TaxVO> taxList = null;//odVO.getTaxVOs();
    if(odVO != null && odVO.getItemTaxVO() != null && odVO.getItemTaxVO().getTaxSplit() != null) {
    	taxList = odVO.getItemTaxVO().getTaxSplit();
    }
    
    if(taxList != null) {
        for (int j=0; j<taxList.size(); j++) {
            TaxVO taxVO = (TaxVO) taxList.get(j);
            if (j == 0) {
      	        bVO.setTax1Name(taxVO.getName());
          	    bVO.setTax1Description(taxVO.getDescription());
        	    bVO.setTax1Rate(taxVO.getRate());
        	    bVO.setTax1Amount(taxVO.getAmount());
      	    } else if (j == 1) {
       	        bVO.setTax2Name(taxVO.getName());
          	    bVO.setTax2Description(taxVO.getDescription());
        	    bVO.setTax2Rate(taxVO.getRate());
        	    bVO.setTax2Amount(taxVO.getAmount());
      	    } else if (j == 2) {
        	    bVO.setTax3Name(taxVO.getName());
           	    bVO.setTax3Description(taxVO.getDescription());
         	    bVO.setTax3Rate(taxVO.getRate());
         	    bVO.setTax3Amount(taxVO.getAmount());
      	    } else if (j == 3) {
        	    bVO.setTax4Name(taxVO.getName());
           	    bVO.setTax4Description(taxVO.getDescription());
         	    bVO.setTax4Rate(taxVO.getRate());
         	    bVO.setTax4Amount(taxVO.getAmount());
      	    } else if (j == 4) {
        	    bVO.setTax5Name(taxVO.getName());
           	    bVO.setTax5Description(taxVO.getDescription());
         	    bVO.setTax5Rate(taxVO.getRate());
         	    bVO.setTax5Amount(taxVO.getAmount());
      	    }            
        }
    }
    
    int count = 0;
    for (TaxVO taxVO : taxList) {
    	switch (count++) {
			case 1:
				bVO.setTax1Name(taxVO.getName());
	      	    bVO.setTax1Description(taxVO.getDescription());
	    	    bVO.setTax1Rate(taxVO.getRate());
	    	    bVO.setTax1Amount(taxVO.getAmount());
				break;
			case 2:
				bVO.setTax2Name(taxVO.getName());
	      	    bVO.setTax2Description(taxVO.getDescription());
	    	    bVO.setTax2Rate(taxVO.getRate());
	    	    bVO.setTax2Amount(taxVO.getAmount());
				break;
			case 3:
				bVO.setTax3Name(taxVO.getName());
	      	    bVO.setTax3Description(taxVO.getDescription());
	    	    bVO.setTax3Rate(taxVO.getRate());
	    	    bVO.setTax3Amount(taxVO.getAmount());
				break;
			case 4:
				bVO.setTax4Name(taxVO.getName());
	      	    bVO.setTax4Description(taxVO.getDescription());
	    	    bVO.setTax4Rate(taxVO.getRate());
	    	    bVO.setTax4Amount(taxVO.getAmount());
				break;		
			default:
				break;
		}
	} 

    BillingDAO bDAO = new BillingDAO(this.con);
    bDAO.addBillingMO(bVO);
  }



/*******************************************************************************************
 * processAddOns()
 *******************************************************************************************
  * This method is used to update the order
  *
  */
  private void processAddOns() throws Exception
  {
    //Instantiate CustomerDAO
    UpdateOrderDAO uoDAO = new UpdateOrderDAO(this.con);

    HashMap results = new HashMap();
    AddOnVO newAddOnVO;
    String status;

    // Remove any existing addons for this order first
    results = uoDAO.deleteExistingAddOns(this.orderDetailId);
    status = (String) results.get(COMConstants.STATUS_PARAM);
    if (status.equalsIgnoreCase(COMConstants.CONS_NO)) {
        String message = (String) results.get(COMConstants.MESSAGE_PARAM);
        throw new Exception(message);
    }

    // Now insert addons from request
    for(int i=0; i < this.requestAddOnVOList.size(); i++) {
        newAddOnVO = (AddOnVO) this.requestAddOnVOList.get(i);
        results = uoDAO.insertNewAddOns(this.orderDetailId, this.csrId, newAddOnVO.getAddOnCode(),
                                               newAddOnVO.getAddOnQuantity());
        status = (String) results.get(COMConstants.STATUS_PARAM);
        if (status.equalsIgnoreCase(COMConstants.CONS_NO)) {
            String message = (String) results.get(COMConstants.MESSAGE_PARAM);
            throw new Exception(message);
        }
    }
  }



/*******************************************************************************************
 * computeShipDate()
 *******************************************************************************************
  * This method is used to call the
  *
  */
  private void computeShipDate() throws Exception
  {
    // If ship method changed, recalculate ship date
    //
    DeliveryDateUTIL ddu = new DeliveryDateUTIL();
    SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");

    // Note: Tried invoking other getShipDate method without oeParms, but it
    // null pointer'd because productType was null.  So, we set it here.
    OEDeliveryDateParm oeParms = new OEDeliveryDateParm();
    oeParms.setProductType(this.productType);

    Date dDeliveryDate = this.cDeliveryDate.getTime();
    String sDeliveryDate = dateFormat.format(dDeliveryDate);

    String newShipDate =  ddu.getShipDate(oeParms, this.shipMethod,
                          sDeliveryDate, this.recipientState,
                          this.recipientCountry, this.productId, this.con);

    this.shipDate = Calendar.getInstance();

    this.shipDate.setTime(dateFormat.parse(newShipDate));

  }


/*******************************************************************************************
 * buildVOFromTables()
 *******************************************************************************************/
  private void buildVOFromTables(HashMap tempOrderInfo)
        throws Exception
  {
    Set ks1 = tempOrderInfo.keySet();
    Iterator iter = ks1.iterator();
    String key = null;

    //Create a CachedResultSet object using the cursor name that was passed.
    CachedResultSet rs = null;

    //Iterate thru the keyset
    while(iter.hasNext())
    {
      //get the key
      key = iter.next().toString();
      rs = (CachedResultSet) tempOrderInfo.get(key);

//*****************************************************************************************
// * OUT_UPD_ADDON_CUR
// *****************************************************************************************/
      if (key.equalsIgnoreCase("OUT_UPD_ADDON_CUR"))
      {
        rs = (CachedResultSet) tempOrderInfo.get(key);

        while(rs.next())
        {
          AddOnVO addOnVO = new AddOnVO();

          /**retrieve and set the AddOnVO.addOnCode**/
          String addOnCode = null;
          if (rs.getObject("add_on_code") != null)
            addOnCode = (String)rs.getObject("add_on_code");
          addOnVO.setAddOnCode(addOnCode);


          /**retrieve and set the AddOnVO.addOnDescription**/
          String addOnDescription = null;
          if (rs.getObject("description") != null)
          {
            addOnDescription = (String)rs.getObject("description");
            addOnDescription = addOnDescription.toUpperCase();
          }
          addOnVO.setAddOnDescription(addOnDescription);


          /**retrieve and set the AddOnVO.addOnId**/
          String      addOnId = null;
          BigDecimal  bAddOnId;
          long        lAddOnId = 0;
          if (rs.getObject("order_add_on_id") != null)
          {
            bAddOnId = (BigDecimal)rs.getObject("order_add_on_id");
            addOnId = bAddOnId.toString();
            lAddOnId = Long.valueOf(addOnId).longValue();
          }
          addOnVO.setAddOnId(lAddOnId);


          /**retrieve and set the AddOnVO.addOnPrice**/
          String      addOnPrice = null;
          BigDecimal  bgAddOnPrice;
          double      dAddOnPrice = 0;
          if (rs.getObject("price") != null)
          {
            bgAddOnPrice = (BigDecimal)rs.getObject("price");
            addOnPrice = bgAddOnPrice.toString();
            dAddOnPrice = Double.valueOf(addOnPrice).doubleValue();
          }
          addOnVO.setAddOnPrice(dAddOnPrice);


          /**retrieve and set the AddOnVO.addOnQuantity**/
          String      addOnQty = null;
          BigDecimal  bAddOnQty;
          long        lAddOnQty = 0;
          if (rs.getObject("add_on_quantity") != null)
          {
            bAddOnQty = (BigDecimal)rs.getObject("add_on_quantity");
            addOnQty = bAddOnQty.toString();
            lAddOnQty = Long.valueOf(addOnQty).longValue();
          }
          addOnVO.setAddOnQuantity(lAddOnQty);


          /**retrieve and set the AddOnVO.addOnStatus**/
          String addOnStatus = null;
          if (rs.getObject("add_on_status") != null)
            addOnStatus = (String)rs.getObject("add_on_status");
          addOnVO.setAddOnStatus(addOnStatus);


          /**retrieve and set the AddOnVO.addOnTypeDescription**/
          String addOnTypeDesc = null;
          if (rs.getObject("add_on_type_description") != null)
          {
            addOnTypeDesc = (String)rs.getObject("add_on_type_description");
            addOnTypeDesc = addOnTypeDesc.toUpperCase();
          }
          addOnVO.setAddOnTypeDescription(addOnTypeDesc);


          /**retrieve and set the AddOnVO.orderDetailId**/
          addOnVO.setOrderDetailId(new Long(this.orderDetailId).longValue());


          /**retrieve and set the AddOnVO.updatedBy**/
          addOnVO.setUpdatedBy(this.csrId);


          /** fields that the users cannot update **/
          addOnVO.setCreatedBy(null);  //users cannot update
          addOnVO.setCreatedOn(null);  //users cannot update
          addOnVO.setUpdatedOn(null);  //users cannot update

          //add it in a hashmap
          if (addOnTypeDesc != null)
          {
            this.tempTableAddOnVOHash.put(addOnCode, addOnVO);
            this.tempTableAddOnVOList.add(addOnVO);
          }
        } //end (while rs.next())
      }

//*****************************************************************************************
// * OUT_ORIG_PRODUCT
// *****************************************************************************************/
      else if (key.equalsIgnoreCase("OUT_ORIG_PRODUCT"))
      {
        rs.next();

        /*****************************************************************************************
         * Retrieve/build the order bill VO
         *****************************************************************************************/
        //retrieve the actual addon amount
        String      sActualAddOnAmount = null;
        BigDecimal  bActualAddOnAmount;
        double      dActualAddOnAmount = 0;
        if (rs.getObject("actual_add_on_amount") != null)
        {
          bActualAddOnAmount = (BigDecimal)rs.getObject("actual_add_on_amount");
          sActualAddOnAmount = bActualAddOnAmount.toString();
          dActualAddOnAmount = Double.valueOf(sActualAddOnAmount).doubleValue();
        }
        this.origOrderBillVO.setAddOnAmount(dActualAddOnAmount);


        //retrieve the actual Discount amount
        String      sActualDiscountAmount = null;
        BigDecimal  bActualDiscountAmount;
        double      dActualDiscountAmount = 0;
        if (rs.getObject("actual_discount_amount") != null)
        {
          bActualDiscountAmount = (BigDecimal)rs.getObject("actual_discount_amount");
          sActualDiscountAmount = bActualDiscountAmount.toString();
          dActualDiscountAmount = Double.valueOf(sActualDiscountAmount).doubleValue();
        }
        this.origOrderBillVO.setDiscountAmount(dActualDiscountAmount);


        //retrieve the actual product amount
        String      sActualProductAmount = null;
        BigDecimal  bActualProductAmount;
        double      dActualProductAmount = 0;
        if (rs.getObject("actual_product_amount") != null)
        {
          bActualProductAmount = (BigDecimal)rs.getObject("actual_product_amount");
          sActualProductAmount = bActualProductAmount.toString();
          dActualProductAmount = Double.valueOf(sActualProductAmount).doubleValue();
        }
        this.origOrderBillVO.setProductAmount(dActualProductAmount);


        //retrieve the actual Service Fee amount
        String      sActualServiceFeeAmount = null;
        BigDecimal  bActualServiceFeeAmount;
        double      dActualServiceFeeAmount = 0;
        if (rs.getObject("actual_service_fee") != null)
        {
          bActualServiceFeeAmount = (BigDecimal)rs.getObject("actual_service_fee");
          sActualServiceFeeAmount = bActualServiceFeeAmount.toString();
          dActualServiceFeeAmount = Double.valueOf(sActualServiceFeeAmount).doubleValue();
        }
        this.origOrderBillVO.setServiceFee(dActualServiceFeeAmount);


        //retrieve the actual shipping fee
        String      sActualShippingFeeAmount = null;
        BigDecimal  bActualShippingFeeAmount;
        double      dActualShippingFeeAmount = 0;
        if (rs.getObject("actual_shipping_fee") != null)
        {
          bActualShippingFeeAmount = (BigDecimal)rs.getObject("actual_shipping_fee");
          sActualShippingFeeAmount = bActualShippingFeeAmount.toString();
          dActualShippingFeeAmount = Double.valueOf(sActualShippingFeeAmount).doubleValue();
        }
        this.origOrderBillVO.setShippingFee(dActualShippingFeeAmount);


        //retrieve the actual Shipping Tax amount
        String      sActualShippingTaxAmount = null;
        BigDecimal  bActualShippingTaxAmount;
        double      dActualShippingTaxAmount = 0;
        if (rs.getObject("actual_shipping_tax") != null)
        {
          bActualShippingTaxAmount = (BigDecimal)rs.getObject("actual_shipping_tax");
          sActualShippingTaxAmount = bActualShippingTaxAmount.toString();
          dActualShippingTaxAmount = Double.valueOf(sActualShippingTaxAmount).doubleValue();
        }
        this.origOrderBillVO.setShippingTax(dActualShippingTaxAmount);


        //retrieve the actual tax amount
        String      sActualTaxAmount = null;
        BigDecimal  bActualTaxAmount;
        double      dActualTaxAmount = 0;
        if (rs.getObject("actual_tax") != null)
        {
          bActualTaxAmount = (BigDecimal)rs.getObject("actual_tax");
          sActualTaxAmount = bActualTaxAmount.toString();
          dActualTaxAmount = Double.valueOf(sActualTaxAmount).doubleValue();
        }
        this.origOrderBillVO.setTax(dActualTaxAmount);


        /*****************************************************************************************
         * Retrieve/build the order detail VO
         *****************************************************************************************/

        this.origOrderDetailVO.setColor1(rs.getObject("color_1") != null? (String) rs.getObject("color_1"):null);
        this.origOrderDetailVO.setColor2(rs.getObject("color_2") != null? (String) rs.getObject("color_2"):null);
        this.origOrderDetailVO.setPersonalGreetingId(rs.getObject("personal_greeting_id") != null? (String) rs.getObject("personal_greeting_id"):null);

        //retrieve/format the delivery date
        java.sql.Date dDeliveryDate = null;

        if (rs.getObject("delivery_date") != null)
        {
          dDeliveryDate = (java.sql.Date)rs.getObject("delivery_date");
          //Create a Calendar Object
          Calendar cDeliveryDate = Calendar.getInstance();
          //Set the Calendar Object using the date retrieved in sCreatedOn
          cDeliveryDate.setTime(new java.util.Date(dDeliveryDate.getTime()));
          this.origOrderDetailVO.setDeliveryDate(cDeliveryDate);
        }


        //retrieve/format the delivery date
        java.sql.Date dDeliveryDateRange = null;
        if (rs.getObject("delivery_date_range_end") != null)
        {
          dDeliveryDateRange = (java.sql.Date)rs.getObject("delivery_date_range_end");
          //Create a Calendar Object
          Calendar cDeliveryDateRange = Calendar.getInstance();
          //Set the Calendar Object using the date retrieved in sCreatedOn
          cDeliveryDateRange.setTime(new java.util.Date(dDeliveryDateRange.getTime()));
          this.origOrderDetailVO.setDeliveryDateRangeEnd(cDeliveryDateRange);
        }


        //retrieve/format the ship date
        java.sql.Date dShipDate = null;
        if (rs.getObject("ship_date") != null)
        {
          dShipDate = (java.sql.Date)rs.getObject("ship_date");
          //Create a Calendar Object
          Calendar cShipDate = Calendar.getInstance();
          //Set the Calendar Object using the date retrieved in sCreatedOn
          cShipDate.setTime(new java.util.Date(dShipDate.getTime()));
          this.origOrderDetailVO.setShipDate(cShipDate);
        }

        //retrieve the quantity
        String      qty = null;
        BigDecimal  bQty;
        long        lQty = 0;
        if (rs.getObject("quantity") != null)
        {
          bQty = (BigDecimal)rs.getObject("quantity");
          qty = bQty.toString();
          lQty = Long.valueOf(qty).longValue();
        }
        this.origOrderDetailVO.setQuantity(lQty);

        this.origOrderDetailVO.setOrderDetailId(this.orderDetailId);
        this.origOrderDetailVO.setProductId(rs.getObject("product_id") != null? rs.getObject("product_id").toString().toUpperCase():null);
        this.origOrderDetailVO.setShipMethod(rs.getObject("ship_method") != null? (String) rs.getObject("ship_method"):"");
        this.origOrderDetailVO.setSizeIndicator(rs.getObject("size_indicator") != null? (String) rs.getObject("size_indicator"):null);
        this.origOrderDetailVO.setSpecialInstructions(rs.getObject("special_instructions") != null? (String) rs.getObject("special_instructions"):null);
        this.origOrderDetailVO.setSubstitutionIndicator(rs.getObject("substitution_indicator") != null? (String) rs.getObject("substitution_indicator"):null);

        //retrieve the miles/points
        String      milesPoints = null;
        BigDecimal  bMilesPoints;
        double      dMilesPoints = 0;
        if (rs.getObject("miles_points") != null)
        {
          bMilesPoints = (BigDecimal)rs.getObject("miles_points");
          milesPoints = bMilesPoints.toString();
          dMilesPoints = Double.valueOf(milesPoints).doubleValue();
        }
        this.origOrderDetailVO.setMilesPoint(dMilesPoints);
      }


//*****************************************************************************************
// * OUT_UPD_ORDER_CUR
// *****************************************************************************************/

      else if (key.equalsIgnoreCase("OUT_UPD_ORDER_CUR"))
      {
        rs.next();

        /*****************************************************************************************
         * Retrieve/build the order bill VO
         *****************************************************************************************/
        //retrieve the actual addon amount
        String      sAddOnAmount = null;
        BigDecimal  bAddOnAmount;
        double      dAddOnAmount = 0;
        if (rs.getObject("add_on_amount") != null)
        {
          bAddOnAmount = (BigDecimal)rs.getObject("add_on_amount");
          sAddOnAmount = bAddOnAmount.toString();
          dAddOnAmount = Double.valueOf(sAddOnAmount).doubleValue();
        }
        this.tempTableOrderBillVO.setAddOnAmount(dAddOnAmount);


        //retrieve the actual Discount amount
        String      sDiscountAmount = null;
        BigDecimal  bDiscountAmount;
        double      dDiscountAmount = 0;
        if (rs.getObject("discount_amount") != null)
        {
          bDiscountAmount = (BigDecimal)rs.getObject("discount_amount");
          sDiscountAmount = bDiscountAmount.toString();
          dDiscountAmount = Double.valueOf(sDiscountAmount).doubleValue();
        }
        this.tempTableOrderBillVO.setDiscountAmount(dDiscountAmount);


        //retrieve the actual product amount
        String      sProductAmount = null;
        BigDecimal  bProductAmount;
        double      dProductAmount = 0;
        if (rs.getObject("product_amount") != null)
        {
          bProductAmount = (BigDecimal)rs.getObject("product_amount");
          sProductAmount = bProductAmount.toString();
          dProductAmount = Double.valueOf(sProductAmount).doubleValue();
        }
        this.tempTableOrderBillVO.setProductAmount(dProductAmount);


        //retrieve the actual Service Fee amount
        String      sServiceFeeAmount = null;
        BigDecimal  bServiceFeeAmount;
        double      dServiceFeeAmount = 0;
        if (rs.getObject("service_fee") != null)
        {
          bServiceFeeAmount = (BigDecimal)rs.getObject("service_fee");
          sServiceFeeAmount = bServiceFeeAmount.toString();
          dServiceFeeAmount = Double.valueOf(sServiceFeeAmount).doubleValue();
        }
        this.tempTableOrderBillVO.setServiceFee(dServiceFeeAmount);


        //retrieve the actual shipping fee
        String      sShippingFeeAmount = null;
        BigDecimal  bShippingFeeAmount;
        double      dShippingFeeAmount = 0;
        if (rs.getObject("shipping_fee") != null)
        {
          bShippingFeeAmount = (BigDecimal)rs.getObject("shipping_fee");
          sShippingFeeAmount = bShippingFeeAmount.toString();
          dShippingFeeAmount = Double.valueOf(sShippingFeeAmount).doubleValue();
        }
        this.tempTableOrderBillVO.setShippingFee(dShippingFeeAmount);


        //retrieve the actual Shipping Tax amount
        String      sShippingTaxAmount = null;
        BigDecimal  bShippingTaxAmount;
        double      dShippingTaxAmount = 0;
        if (rs.getObject("shipping_tax") != null)
        {
          bShippingTaxAmount = (BigDecimal)rs.getObject("shipping_tax");
          sShippingTaxAmount = bShippingTaxAmount.toString();
          dShippingTaxAmount = Double.valueOf(sShippingTaxAmount).doubleValue();
        }
        this.tempTableOrderBillVO.setShippingTax(dShippingTaxAmount);


        //retrieve the actual tax amount
        String      sTaxAmount = null;
        BigDecimal  bTaxAmount;
        double      dTaxAmount = 0;
        if (rs.getObject("tax") != null)
        {
          bTaxAmount = (BigDecimal)rs.getObject("tax");
          sTaxAmount = bTaxAmount.toString();
          dTaxAmount = Double.valueOf(sTaxAmount).doubleValue();
        }
        this.tempTableOrderBillVO.setTax(dTaxAmount);


        /*****************************************************************************************
         * Retrieve/build the order detail VO
         *****************************************************************************************/

        this.tempTableOrderDetailVO.setColor1(rs.getObject("color_1") != null? (String) rs.getObject("color_1"):null);
        this.tempTableOrderDetailVO.setColor2(rs.getObject("color_2") != null? (String) rs.getObject("color_2"):null);
        this.tempTableOrderDetailVO.setPersonalGreetingId(rs.getObject("personal_greeting_id") != null? (String) rs.getObject("personal_greeting_id"):null);

        //retrieve/format the delivery date
        java.sql.Date dDeliveryDate = null;

        if (rs.getObject("delivery_date") != null)
        {
          dDeliveryDate = (java.sql.Date)rs.getObject("delivery_date");
          //Create a Calendar Object
          Calendar cDeliveryDate = Calendar.getInstance();
          //Set the Calendar Object using the date retrieved in sCreatedOn
          cDeliveryDate.setTime(new java.util.Date(dDeliveryDate.getTime()));
          this.tempTableOrderDetailVO.setDeliveryDate(cDeliveryDate);
        }


        //retrieve/format the delivery date
        java.sql.Date dDeliveryDateRange = null;
        if (rs.getObject("delivery_date_range_end") != null)
        {
          dDeliveryDateRange = (java.sql.Date)rs.getObject("delivery_date_range_end");
          //Create a Calendar Object
          Calendar cDeliveryDateRange = Calendar.getInstance();
          //Set the Calendar Object using the date retrieved in sCreatedOn
          cDeliveryDateRange.setTime(new java.util.Date(dDeliveryDateRange.getTime()));
          this.tempTableOrderDetailVO.setDeliveryDateRangeEnd(cDeliveryDateRange);
        }


        //retrieve/format the ship date
        java.sql.Date dShipDate = null;
        if (rs.getObject("ship_date") != null)
        {
          dShipDate = (java.sql.Date)rs.getObject("ship_date");
          //Create a Calendar Object
          Calendar cShipDate = Calendar.getInstance();
          //Set the Calendar Object using the date retrieved in sCreatedOn
          cShipDate.setTime(new java.util.Date(dShipDate.getTime()));
          this.tempTableOrderDetailVO.setShipDate(cShipDate);
        }

        //retrieve the quantity
        String      qty = null;
        BigDecimal  bQty;
        long        lQty = 0;
        if (rs.getObject("quantity") != null)
        {
          bQty = (BigDecimal)rs.getObject("quantity");
          qty = bQty.toString();
          lQty = Long.valueOf(qty).longValue();
        }
        this.tempTableOrderDetailVO.setQuantity(lQty);

        this.tempTableOrderDetailVO.setOrderDetailId(this.orderDetailId);
        this.tempTableOrderDetailVO.setProductId(rs.getObject("product_id") != null? rs.getObject("product_id").toString().toUpperCase():null);
        this.tempTableOrderDetailVO.setShipMethod(rs.getObject("ship_method") != null? (String) rs.getObject("ship_method"):"");
        this.tempTableOrderDetailVO.setSizeIndicator(rs.getObject("size_indicator") != null? (String) rs.getObject("size_indicator"):null);
        this.tempTableOrderDetailVO.setSpecialInstructions(rs.getObject("special_instructions") != null? (String) rs.getObject("special_instructions"):null);
        this.tempTableOrderDetailVO.setSubstitutionIndicator(rs.getObject("substitution_indicator") != null? (String) rs.getObject("substitution_indicator"):null);

        //retrieve the miles/points
        String      milesPoints = null;
        BigDecimal  bMilesPoints;
        double      dMilesPoints = 0;
        if (rs.getObject("miles_points") != null)
        {
          bMilesPoints = (BigDecimal)rs.getObject("miles_points");
          milesPoints = bMilesPoints.toString();
          dMilesPoints = Double.valueOf(milesPoints).doubleValue();
        }
        this.tempTableOrderDetailVO.setMilesPoint(dMilesPoints);
      }

//*****************************************************************************************
// * OUT_ORIG_ADD_ONS
// *****************************************************************************************/
      else if (key.equalsIgnoreCase("OUT_ORIG_ADD_ONS"))
      {}
//*****************************************************************************************
// * OUT_SHIP_COST
// *****************************************************************************************/
      else if (key.equalsIgnoreCase("OUT_SHIP_COST"))
      {}

    } //end-while(iter.hasNext())

  }




/*******************************************************************************************
 * buildVOFromRequest()
 *******************************************************************************************/
  private void buildVOFromRequest()
        throws Exception
  {

    /**********************************************************************************************
     * Build the new order detail VOs
     **********************************************************************************************/

    this.requestOrderDetailVO.setColor1(this.color1);
    this.requestOrderDetailVO.setColor1Description(this.color1Description);
    this.requestOrderDetailVO.setColor2(this.color2);
    this.requestOrderDetailVO.setColor2Description(this.color2Description);
    this.requestOrderDetailVO.setSubcode(this.subcode);
    this.requestOrderDetailVO.setDeliveryDate(this.cDeliveryDate);
    this.requestOrderDetailVO.setDeliveryDateRangeEnd(this.cDeliveryDateRange);
    this.requestOrderDetailVO.setProductId(this.productId);
    this.requestOrderDetailVO.setShipMethod(this.shipMethod);
    this.requestOrderDetailVO.setSizeIndicator(this.sizeIndicator);
    this.requestOrderDetailVO.setSpecialInstructions(this.tempTableOrderDetailVO.getSpecialInstructions());
    this.requestOrderDetailVO.setSubstitutionIndicator(this.substitutionIndicator);
    this.requestOrderDetailVO.setUpdatedBy(this.csrId);
    this.requestOrderDetailVO.setPersonalGreetingId(this.personalGreetingId);


    /**********************************************************************************************
     * Build the new order bill VOs
     **********************************************************************************************/
    String regex = "(?<=\\d),(?=\\d)";
    this.productAmount = this.productAmount.replaceAll(regex, "");
    this.requestOrderBillVO.setProductAmount(new Double(this.productAmount).doubleValue());

  }


/*******************************************************************************************
 * checkAmounts()
 *******************************************************************************************/
  private boolean checkAmounts()  throws Exception
  {
    boolean amountExceeded = false;

    //build the order details vo
    OrderDetailsVO odVO = buildRecalculateOrderDetailsVO();

    //build and calculate the price on the original order
    OrderVO origOrderVO = recalculateOrigOrderAmounts(odVO);

    //build and calculate the price for the new product
    OrderVO newOrderVO = recalculateNewOrderAmounts(odVO);

    //retrieve the original order total
    double origTotal = new Double(origOrderVO.getOrderTotal()).doubleValue();

    //retrieve the new order total
    double newTotal = new Double(newOrderVO.getOrderTotal()).doubleValue();

    //check for difference in amounts and set the flag
    if (newTotal > origTotal)
      amountExceeded = true;

    return amountExceeded;

  }


/*******************************************************************************************
 * performAddonValidate()
 *******************************************************************************************/
  private boolean performAddonValidate()  throws Exception
  {
    boolean validationPassed = true;

    // We only need to be sure there's a single vendor that allows all addons that were selected
    // (This check is needed since front end simply displays any addons for any vendors with product) 
    //
    if (this.vendorFlag.equalsIgnoreCase(COMConstants.CONS_YES)) {
        int total = this.requestAddOnVOList.size();
        if (total > 0) {
            ArrayList<String> addonIdList = new ArrayList<String>();
            AddOnUtility addonUtil = new AddOnUtility();
            AddOnVO requestAddOnVO;  
            for (int i = 0; i < total; i++) {
              requestAddOnVO = (AddOnVO) this.requestAddOnVOList.get(i);
              addonIdList.add(requestAddOnVO.getAddOnCode());
            }
            ArrayList vendorList = addonUtil.getVendorListByAddonListAndProduct(this.requestOrderDetailVO.getProductId(), addonIdList, this.con);
            if (vendorList == null || vendorList.size() < 1) { 
                generateErrorNode(COMConstants.ADDON_NO_VENDORS, null,
                                  COMConstants.CONS_ERROR_POPUP_INDICATOR_OK,
                                  null, null);
                validationPassed = false;
            }
        }
    }
    return validationPassed;
  }


/*******************************************************************************************
 * performValidate()
 *******************************************************************************************/
  private boolean performValidate()  throws Exception
  {
    boolean validationPassed = true;

    if (this.ignoreError != null && this.ignoreError.equalsIgnoreCase(COMConstants.CONS_YES))
      validationPassed = true;
    else
    {
      HashMap results = validate();

      /*******************************************************************************************
      * WM#10 - product agricultural restriction found.
      ******************************************************************************************/
      if ( results.containsKey(ValidationRules.PRODUCT_STATE_AGRICULTURAL_RESTRICATIONS_FOR_DELIVERY_DAY_SELECTED))
      {
        generateErrorNode(COMConstants.PRODUCT_STATE_AGRICULTURAL_RESTRICATIONS, null,
                          COMConstants.CONS_ERROR_POPUP_INDICATOR_OK, LOAD_PRODUCT_CATEGORY, null);
        validationPassed = false;
      }

      /*******************************************************************************************
      * WM#15 - check if the zip code is in GNADD
      ******************************************************************************************/
      if (validationPassed)
      {
        if ( results.containsKey(ValidationRules.ZIP_IN_GNADD) )
        {
          getDeliveryDates(COMDeliveryDateUtil.MAX_DAYS_OUT, this.divo);
          String message =  COMConstants.ZIP_IN_GNADD +
                            "The next available delivery date is "  +
                            this.nextGlobalAvailableDeliveryDate  +
                            ". " + CONTINUE_MSG;
          generateErrorNode(message, null,
                            COMConstants.CONS_ERROR_POPUP_INDICATOR_CONFIRM,
                            IGNORE_ERROR, null);
          validationPassed = false;
        }
      }

      /*******************************************************************************************
       * WM#40 - check if the total exceeded for an amazon order
       ******************************************************************************************/
      /** Validate Totals for Amazon Orders**/
      if (validationPassed)
      {
        boolean amountExceeded = false;
        if (this.originId.equalsIgnoreCase(COMConstants.CONS_ORIGIN_ID_AMAZON))
          amountExceeded = checkAmounts();

        if (amountExceeded)
        {
          //generateErrorNode(COMConstants.CANNOT_INCREASE_AMAZON_ORDER_TOTAL, null, null, LOAD_PRODUCT_CATEGORY, null);

          generateErrorNode(COMConstants.CANNOT_INCREASE_AMAZON_ORDER_TOTAL, null,
                            COMConstants.CONS_ERROR_POPUP_INDICATOR_OK,
                            DISPLAY_PRODUCT_CATEGORY_BUTTON, null);
          validationPassed = false;
        }
      }

      if (this.productSubType.equalsIgnoreCase(COMConstants.CONS_PRODUCT_SUBTYPE_EXOTIC))
      {
        /*******************************************************************************************
        * WM#21
        ******************************************************************************************/
        if (validationPassed)
        {
          if (results.containsKey(ValidationRules.SUNDAY_DELIVERY_NOT_ALLOWED_FOR_EXOTIC))
          {
            generateErrorNode(COMConstants.SUNDAY_DELIVERY_NOT_ALLOWED_FOR_EXOTIC, null,
                              COMConstants.CONS_ERROR_POPUP_INDICATOR_OK,
                              DISPLAY_PRODUCT_CATEGORY_BUTTON, null);
            validationPassed = false;
          }
        }

        /*******************************************************************************************
        * WM#11
        ******************************************************************************************/
        if (validationPassed)
        {
          if (results.containsKey(ValidationRules.SAME_DAY_EXOTIC_UNAVAILABLE) )
          {
            generateErrorNode(COMConstants.SAME_DAY_EXOTIC_UNAVAILABLE, null,
                              COMConstants.CONS_ERROR_POPUP_INDICATOR_OK, null, null);
            validationPassed = false;
          }
        }

      }
      else
      {
        /*******************************************************************************************
        * WM#13 - Validate for sunday delivery
        ******************************************************************************************/
        if (validationPassed)
        {
          SimpleDateFormat formatDay = new SimpleDateFormat("EEE");

          String sDeliveryDateDay = "";
          String sDeliveryDateRangeDay = "";

          if (StringUtils.isNotEmpty(this.deliveryDate))
          {
            Date dDeliveryDate = this.cDeliveryDate.getTime();
            sDeliveryDateDay = formatDay.format(dDeliveryDate);
          }
          if (StringUtils.isNotEmpty(this.deliveryDateRange))
          {
            Date dDeliveryDateRange = this.cDeliveryDateRange.getTime();
            sDeliveryDateRangeDay = formatDay.format(dDeliveryDateRange);
          }

          if  ( this.vendorFlag.equalsIgnoreCase(COMConstants.CONS_NO)      &&
                ( sDeliveryDateDay.equalsIgnoreCase("SUN")            ||
                  sDeliveryDateRangeDay.equalsIgnoreCase("SUN")
                )                                                           &&
                this.zipSundayFlag.equalsIgnoreCase(COMConstants.CONS_NO)
              )
          {
            if (this.ignoreError.equalsIgnoreCase(COMConstants.CONS_NO))
            {
              generateErrorNode(COMConstants.SUNDAY_DELIVERY_NOT_AVAILABLE_IN_ZIP + " " + CONTINUE_MSG,
                                null, COMConstants.CONS_ERROR_POPUP_INDICATOR_CONFIRM,
                                IGNORE_ERROR, null);
              validationPassed = false;
            }
          }
        }
      }

      /*******************************************************************************************
      * WM#14
      ******************************************************************************************/
      if (validationPassed)
      {
        if (results.containsKey(ValidationRules.SAME_DAY_CUTOFF_EXPIRED) )
        {
          generateErrorNode(COMConstants.SAME_DAY_CUTOFF_EXPIRED, null,
                            COMConstants.CONS_ERROR_POPUP_INDICATOR_OK, null, null);
          validationPassed = false;
        }
      }

      /*******************************************************************************************
      * WM#23
      ******************************************************************************************/
      if (validationPassed)
      {
        if ( results.containsKey(ValidationRules.PRODUCT_DELIVERY_DATE_RESTRICTIONS) )
        {
          String message = COMConstants.PRODUCT_DELIVERY_DATE_RESTRICTIONS_WITH_DATE1 +
                           this.exceptionStartDate +
                           COMConstants.PRODUCT_DELIVERY_DATE_RESTRICTIONS_WITH_DATE2 +
                           this.exceptionEndDate +
                           COMConstants.PRODUCT_DELIVERY_DATE_RESTRICTIONS_WITH_DATE3;
          generateErrorNode(message, null,
                            COMConstants.CONS_ERROR_POPUP_INDICATOR_OK, null, null);
          validationPassed = false;
        }
      }
    
      /*******************************************************************************************
      * Addon validation for vendors
      ******************************************************************************************/
      if (validationPassed) 
      {
            validationPassed = performAddonValidate();
      }


      if (!validationPassed)
      {
        this.pageData.put("XSL", COMConstants.XSL_LOAD_PRODUCT_DETAIL_ACTION);

        //if error encountered, we have to pre-populate the price displays
        String requestedSizeDescription = "standard";
        if (this.sizeIndicator.equalsIgnoreCase("a"))
        {
          if (this.variablePriceSelected.equalsIgnoreCase(COMConstants.CONS_YES))
          {
            requestedSizeDescription = "variable";
          }
          else
            requestedSizeDescription = "standard";
        }
        else if (this.sizeIndicator.equalsIgnoreCase("b"))
          requestedSizeDescription = "deluxe";
        else if (this.sizeIndicator.equalsIgnoreCase("c"))
          requestedSizeDescription = "premium";
        this.pageData.put("requested_size_description", requestedSizeDescription);


        //add the newAddonsList to the requestOrderDetailVO
        this.requestOrderDetailVO.setAddOnVOList(this.requestAddOnVOList);

        //add the requestOrderBillVO to the requestOrderDetailVO
        List requestOrderBillVOList = new ArrayList();
        requestOrderBillVOList.add(this.requestOrderBillVO);
        this.requestOrderDetailVO.setOrderBillVOList(requestOrderBillVOList);

        //convert the requestOrderDetailVO in an XML format
        //DOMParser parser = new DOMParser();
        Document newOrderDetailXML = (Document) DOMUtil.getDefaultDocument();

        //generate the top node
        Element stateElement = newOrderDetailXML.createElement("requestOrderDetailVO");
        //and append it
        newOrderDetailXML.appendChild(stateElement);

        //parser.parse(new InputSource(new StringReader(this.requestOrderDetailVO.toXML(0))));
        Document requestOrderDetail = DOMUtil.getDocument(this.requestOrderDetailVO.toXML());
        newOrderDetailXML.getDocumentElement().appendChild(newOrderDetailXML.importNode(requestOrderDetail.getFirstChild(), true));

        //add it to hashXML so as to pass it to the loadProductDetailBO
        this.hashXML.put(COMConstants.HK_UPD_NEW_ORDER_DETAIL,    newOrderDetailXML);

      }
    }

    return validationPassed;

  }


  /*
   * Performs florist and vendor delivery date validation by delegating validation to
   * the corresponding validation handler.
   *
   * @return true if the selected delivery date is valid, otherwise false
   * @throws java.lang.Exception
   */
  private HashMap validate() throws Exception 
  {

    ValidationConfiguration validationConfiguration = new ValidationConfiguration();
    validationConfiguration.setValidationLevel( ValidationLevels.WARNING );

    this.divo.setColor1(this.requestOrderDetailVO.getColor1());
    this.divo.setColor2(this.requestOrderDetailVO.getColor2());
    this.divo.setCompanyId(this.companyId);
    this.divo.setCustomerId(this.customerId);
    this.divo.setDeliveryDate(this.requestOrderDetailVO.getDeliveryDate());
    this.divo.setDeliveryDateRangeEnd(this.requestOrderDetailVO.getDeliveryDateRangeEnd());
    this.divo.setMasterOrderNumber(this.masterOrderNumber);
    this.divo.setOccasion(this.occasion);
    this.divo.setOccasionCategoryIndex(this.categoryIndex);
    this.divo.setOccasionDescription(this.occasionText);
    this.divo.setOrderDetailId(this.requestOrderDetailVO.getOrderDetailId());
    this.divo.setOrderGuid(this.orderGuid);
    this.divo.setOriginId(this.originId);
    this.divo.setProductId(this.requestOrderDetailVO.getProductId());
    this.divo.setRecipientCity(this.recipientCity);
    this.divo.setRecipientCountry(this.recipientCountry);
    this.divo.setRecipientState(this.recipientState);
    this.divo.setRecipientZipCode(this.recipientZipCode);
    this.divo.setShipDate(this.shipDate);
    this.divo.setShipMethod(this.requestOrderDetailVO.getShipMethod());
    this.divo.setSizeIndicator(this.requestOrderDetailVO.getSizeIndicator());
    this.divo.setSourceCode(this.requestOrderDetailVO.getSourceCode());
    this.divo.setSpecialInstructions(this.requestOrderDetailVO.getSpecialInstructions());
    this.divo.setSubstitutionIndicator(this.requestOrderDetailVO.getSubstitutionIndicator());
    this.divo.setUpdatedBy(this.csrId);
    this.divo.setVendorFlag(this.vendorFlag.toUpperCase());

    Validator validator = DeliveryDateValidatorFactory.getInstance().getValidator(validationConfiguration, this.divo, this.con);
    Map results = new HashMap();

    if ( !validator.validate() )
    {
      results = validator.getValidationResults();
    }

    return (HashMap)results;

  }



/*******************************************************************************************
 * buildRecalculateOrderDetailsVO()
 *******************************************************************************************/
  private OrderDetailsVO buildRecalculateOrderDetailsVO()
        throws Exception
  {

    UpdateOrderDAO uaDAO = new UpdateOrderDAO(this.con);

    // Build an OrderDetailsVO for specified updated order detail
    OrderDetailsVO odvo = uaDAO.buildRecalculateOrderDetailsVO(this.orderDetailId);

    return odvo;

  }


/*******************************************************************************************
 * recalculateOrigOrderAmounts()
 *******************************************************************************************/
  private OrderVO recalculateOrigOrderAmounts(OrderDetailsVO odVO)
        throws Exception
  {

    OrderVO orderVO = initializeOrderVO();

    Collection orderDetails = new ArrayList();
    orderDetails.add(odVO);
    orderVO.setOrderDetail((List) orderDetails);
    RecalculateOrderBO recalcOrderVo = new RecalculateOrderBO();
    recalcOrderVo.recalculate(this.con, orderVO);

    return orderVO;
  }

/*******************************************************************************************
 * initializeOrderVO()
 *******************************************************************************************/
  private OrderVO initializeOrderVO() throws Exception
  {
  
    OrderVO orderVO = new OrderVO();

    OrderDAO orderDAO = new OrderDAO(this.con);

    com.ftd.customerordermanagement.vo.OrderVO comOrderVO = orderDAO.getOrder(this.orderGuid);
    
    orderVO.setCompanyId(comOrderVO.getCompanyId());
    orderVO.setOrderDate(new Date()); // use current date, this is a new order
    orderVO.setBuyerSignedIn(comOrderVO.getBuyerSignedInFlag());
    
    String customerEmailAddress = "" ;
    CachedResultSet rs = orderDAO.getOrderCustomerInfo(this.orderDetailId);
    if(rs.next())
    {
       customerEmailAddress = rs.getString("email_address");
    }
    orderVO.setBuyerEmailAddress(customerEmailAddress);

    return orderVO;
  }

/*******************************************************************************************
 * recalculateNewOrderAmounts()
 *******************************************************************************************/
  private OrderVO recalculateNewOrderAmounts(OrderDetailsVO odVO)
        throws Exception
  {
    //initialize these fields.  they will be recalculated in the recalculateVO.
    odVO.setAddOnAmount("0");
    odVO.setDiscountAmount("0");
    odVO.setExternalOrderTotal("0");
    odVO.setMilesPoints("0");
    odVO.setServiceFeeAmount("0");
    odVO.setShippingFeeAmount("0");
    odVO.setTaxAmount("0");

    //for the new product, we have to update the following fields in the OrderDetailsVO
    //before we trigger the recalculate class.
    odVO.setColorFirstChoice(this.color1);
    odVO.setColorSecondChoice(this.color2);

    SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
    String sDeliveryDate = null;
    String sDeliveryDateRange = null;
    if (StringUtils.isNotEmpty(this.deliveryDate))
    {
      Date dDeliveryDate = this.cDeliveryDate.getTime();
      sDeliveryDate = sdf.format(dDeliveryDate);
    }
    if (StringUtils.isNotEmpty(this.deliveryDateRange))
    {
      Date dDeliveryDateRange = this.cDeliveryDateRange.getTime();
      sDeliveryDateRange = sdf.format(dDeliveryDateRange);
    }
    odVO.setDeliveryDate(sDeliveryDate);
    odVO.setDeliveryDateRangeEnd(sDeliveryDateRange);

    odVO.setProductId(this.requestOrderDetailVO.getProductId());
    odVO.setProductsAmount(this.productAmount);
    odVO.setShipMethod(this.shipMethod);
    odVO.setSizeChoice(this.sizeIndicator);
    odVO.setSpecialInstructions(this.requestOrderDetailVO.getSpecialInstructions());
    odVO.setProductSubCodeId(this.subcode);
    odVO.setSubstituteAcknowledgement(this.substitutionIndicator);
    odVO.setSourceCode(this.sourceCode);
    //odVO.setreAddressId(this.reci);
    RecipientsVO recipient = new RecipientsVO();
    RecipientAddressesVO recipientAddr = new RecipientAddressesVO();
    recipientAddr.setCity(this.recipientCity);
    recipientAddr.setStateProvince(this.recipientState);
    recipientAddr.setPostalCode(this.recipientZipCode);
    recipientAddr.setCountry(this.recipientCountry);
    Collection recipients = new ArrayList();
    Collection recipientAddrs = new ArrayList();
    recipientAddrs.add(recipientAddr);
    recipient.setRecipientAddresses((List)recipientAddrs);
    recipients.add(recipient);
    odVO.setRecipients((List)recipients);


    Collection odAddOns = new ArrayList();

// ??? Not sure why doing this loop at all - odvo should contain everything from order_addons_update anyhow

    for(int i=0; i < this.requestAddOnVOList.size(); i++) 
    {
      //note that this is the Addon VO from utilities, not from COM.
      AddOnsVO utilAddonsVO;

      //this is the Addon VO from COM
      AddOnVO comAddonVO = (AddOnVO) this.requestAddOnVOList.get(i);

      if (comAddonVO != null)
      {
        //if the quantity > 0, csr is trying to add an Addon.
        if (comAddonVO.getAddOnQuantity() > 0)
        {
          //instantiate the utilities' AddOnsVO
          utilAddonsVO = new AddOnsVO();

          //add data to it from the com's AddOnVO
          //utilAddonsVO.setAddOnId(comAddonVO.getAddOnId());  ??? this no longer contains data
          utilAddonsVO.setAddOnCode(comAddonVO.getAddOnCode());
          utilAddonsVO.setAddOnQuantity( (new Long(comAddonVO.getAddOnQuantity())).toString());

          //add the utilities VO to the collection
          odAddOns.add(utilAddonsVO);
        }
      }
    }

    //add the collection the OrderDetailsVO.
    odVO.setAddOns((List)odAddOns);

    OrderVO orderVO =initializeOrderVO();
    Collection orderDetails = new ArrayList();
    orderDetails.add(odVO);
    orderVO.setOrderDetail((List) orderDetails);
    RecalculateOrderBO recalcOrderVo = new RecalculateOrderBO();

    //recalculate
    boolean clearODAmounts = false;
    recalcOrderVo.recalculate(this.con, orderVO, clearODAmounts);

    return orderVO;

  }


/*******************************************************************************************
 * checkAddonDifference()
 *******************************************************************************************/
  private void checkAddonDifference()
        throws Exception
  {
    this.addOnsDifferent = false;
    
    // If different number of elements in request vs update tables, then they're different (duh)
    if (this.requestAddOnVOList.size() != this.tempTableAddOnVOList.size()) {
        this.addOnsDifferent = true;
        logger.debug("Addon count different between request and update tables, so flagging to re-insert addons");
        return;
    }
    
    // Compare the order addon items
    int total = this.requestAddOnVOList.size();
    AddOnVO requestAddOnVO;     //built based on the info from the Update Product Detail Page
    AddOnVO tempTableAddOnVO;  //built from the update tables

    for (int i = 0; i < total; i++)
    {
      requestAddOnVO = (AddOnVO) this.requestAddOnVOList.get(i);
      String curCode = requestAddOnVO.getAddOnCode();
      tempTableAddOnVO = this.tempTableAddOnVOHash.get(curCode);
      if ((tempTableAddOnVO == null) || (tempTableAddOnVO.getAddOnQuantity() != requestAddOnVO.getAddOnQuantity())) {
        this.addOnsDifferent = true;
        logger.debug("Addon difference found between request and update tables, so flagging to re-insert addons");
        break;
      }
    }

  }


/*******************************************************************************************
 * resetProductAmounts()
 *******************************************************************************************
  * This method is used to update the order
  *
  */
  private void resetProductAmounts() throws Exception
  {
    //Instantiate UpdateOrderDAO
    UpdateOrderDAO uoDAO = new UpdateOrderDAO(this.con);

    //update the prices
    uoDAO.resetODUAmounts(this.productAmount, this.orderDetailId, this.csrId);

  }


/*******************************************************************************************
 * updateMilesPoints()
 *******************************************************************************************
  * This method is used to update the order
  *
  */
  private void updateMilesPoints(String milesPoints) throws Exception
  {
    //Instantiate UpdateOrderDAO
    UpdateOrderDAO uoDAO = new UpdateOrderDAO(this.con);

    //update the prices
    uoDAO.updateMilesPoints(this.orderDetailId, this.csrId, milesPoints);

  }


 /*******************************************************************************************
  * setModifyFlag()
  *******************************************************************************************
  *
  * A change was detected.  Thus, update the MODIFY_ORDER_UPDATED flag in the request parameters
  *
  * @return nothing
  */
  private void setModifyFlag()
  {
    HashMap parameters = (HashMap) this.request.getAttribute    ( COMConstants.CONS_APP_PARAMETERS);
    parameters.put(COMConstants.MODIFY_ORDER_UPDATED,             COMConstants.CONS_YES);
    this.request.setAttribute(COMConstants.CONS_APP_PARAMETERS,   parameters);
  }



 /*******************************************************************************************
  * retrieveColorInfo()
  *******************************************************************************************
  *
  * @return nothing
  * @throws java.lang.Exception
  */
  private void retrieveColorInfo() throws Exception
  {
    UpdateOrderDAO uoDAO = new UpdateOrderDAO(this.con);

    Document colorXML = uoDAO.getAllColorXML();

    String xpath = "//COLOR";
    NodeList nl = DOMUtil.selectNodes(colorXML,xpath);
    HashMap colorHash = new HashMap();
    String colorId;
    String colorDescription;
    Element colorElement = null;

    if ( nl.getLength() > 0 )
    {
      for ( int i = 0; i < nl.getLength(); i++ )
      {
        colorElement = (Element) nl.item(i);

        colorDescription = colorElement.getAttribute("description");
        colorId = colorElement.getAttribute("colormasterid");

        colorHash.put(colorDescription, colorId);
      }
    }

    this.color1 = colorHash.get(this.color1Description)!=null?colorHash.get(this.color1Description).toString():null;
    this.color2 = colorHash.get(this.color2Description)!=null?colorHash.get(this.color2Description).toString():null;

  }



 /*******************************************************************************************
  * generateErrorNode()
  *******************************************************************************************
  *
  * Adds an error message to the error message XML doc
  *
  * @params messageText - Message text
  * @params fieldInError - Name of erred field (optional).
  * @params popupType - Type of popup window (optional).
  * @params popupConfirmation - Return page if popup confirmation selected (optional).
  * @params popupCancel - Return page if confirmation is cancelled (optional).
  */
  private void generateErrorNode(String messageText, String fieldInError,
                                   String popupType, String popupConfirmation, String popupCancel)
        throws Exception
  {

    String l_popupType = popupType;
    if (fieldInError == null && popupType == null) {
      l_popupType = POPUP_INDICATOR_OK;
    }

    // Generate top node
    Document errorMsgsXML = (Document) DOMUtil.getDefaultDocument();
    Element errMsgsTopElement = errorMsgsXML.createElement("ERROR_MESSAGES");
    errorMsgsXML.appendChild(errMsgsTopElement);

    if (messageText != null)
    {
      Element errorMsgElement = errorMsgsXML.createElement("ERROR_MESSAGE");
      errMsgsTopElement.appendChild(errorMsgElement);

      Element errMsgNode = errorMsgsXML.createElement("TEXT");
      errorMsgElement.appendChild(errMsgNode);
      errMsgNode.appendChild(errorMsgsXML.createTextNode(messageText));

      Element errFieldNode = errorMsgsXML.createElement("FIELDNAME");
      errorMsgElement.appendChild(errFieldNode);
      if (fieldInError != null)
        errFieldNode.appendChild(errorMsgsXML.createTextNode(fieldInError));

      Element popupIndicatorNode = errorMsgsXML.createElement("POPUP_INDICATOR");
      errorMsgElement.appendChild(popupIndicatorNode);
      if (l_popupType != null)
        popupIndicatorNode.appendChild(errorMsgsXML.createTextNode(l_popupType));

      Element popupGotoNode = errorMsgsXML.createElement("POPUP_GOTO_PAGE");
      errorMsgElement.appendChild(popupGotoNode);
      if (popupConfirmation != null)
        popupGotoNode.appendChild(errorMsgsXML.createTextNode(popupConfirmation));

      Element popupCancelNode = errorMsgsXML.createElement("POPUP_CANCEL_PAGE");
      errorMsgElement.appendChild(popupCancelNode);
      if (popupCancel != null)
        popupCancelNode.appendChild(errorMsgsXML.createTextNode(popupCancel));
    }

    this.hashXML.put(COMConstants.HK_ERROR_MESSAGES,  errorMsgsXML);

  }



 /**----------------------------------------------------------------------------
  *
  * getDeliveryDates
  *
  * Sets the XML used to populate delivery dates into the XML hash.
  *
  * @param isMin true if the min days out should be retrieved, false if the max days out should be retrieved
  * @param divo Delivery Info VO
  */
  private void getDeliveryDates(boolean isMin, DeliveryInfoVO divo)
      throws Exception
  {

    DeliveryDateParameters ddParms = new DeliveryDateParameters();
    ddParms.setMinDaysOut( isMin );
    ddParms.setRecipientState( divo.getRecipientState() );
    ddParms.setRecipientZipCode( divo.getRecipientZipCode() );
    ddParms.setRecipientCountry( divo.getRecipientCountry() );
    ddParms.setProductId( divo.getProductId() );
    ddParms.setOccasionId( divo.getOccasion() );
    ddParms.setOrderOrigin( divo.getOriginId() );
    ddParms.setIsFlorist( StringUtils.equals(divo.getVendorFlag(), "N") );
    ddParms.setRequestedDeliveryDate( divo.getDeliveryDate() );
    ddParms.setRequestedDeliveryDateRangeEnd( divo.getDeliveryDateRangeEnd() );
    ddParms.setSourceCode(divo.getSourceCode());
    if (this.logger.isDebugEnabled()) {
      logger.debug("getDeliveryDates (state,zip,country,productId,occasion,originId): " +
      divo.getRecipientState() + "," + divo.getRecipientZipCode() + "," + divo.getRecipientCountry() + "," +
      divo.getProductId() + "," + divo.getOccasion() + "," + divo.getOriginId());
    }

    COMDeliveryDateUtil dateUtil = new COMDeliveryDateUtil();
    dateUtil.setParameters( ddParms );
    OEDeliveryDateParm oeParms = dateUtil.getOEParameters(con);

    // do not show date ranges for sympathy/funeral orders
    DeliveryDateConfiguration deliveryDateConfig = new DeliveryDateConfiguration();
    deliveryDateConfig.setShowDateRanges( !StringUtils.equals(oeParms.getProductSubType(), "SYMPAT") );
    deliveryDateConfig.setIncludeRequestedDeliveryDate( true );
    deliveryDateConfig.setDisplayDateFormat( DATE_FORMAT );
    deliveryDateConfig.setSubmitDateFormat( DATE_FORMAT );
    dateUtil.setConfiguration( deliveryDateConfig );

    SimpleDateFormat formatter = new SimpleDateFormat( deliveryDateConfig.getSubmitDateFormat() );
    this.nextGlobalAvailableDeliveryDate = (oeParms.getGlobalParms().getGNADDDate() == null) ? "" : formatter.format(oeParms.getGlobalParms().getGNADDDate());

    Element nextAvailableDeliveryDateElem = (Element)DOMUtil.selectSingleNode(dateUtil.getXmlDeliveryDates(), "//deliveryDate/startDate");
    if(nextAvailableDeliveryDateElem != null)
    {
      this.nextAvailableDeliveryDate = nextAvailableDeliveryDateElem.getFirstChild().getNodeValue();
    }

  }



}