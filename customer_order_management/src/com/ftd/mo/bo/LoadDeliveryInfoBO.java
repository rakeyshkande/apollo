package com.ftd.mo.bo;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.Closure;
import org.apache.commons.collections.Transformer;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.dao.ServicesProgramDAO;
import com.ftd.customerordermanagement.util.BasePageBuilder;
import com.ftd.customerordermanagement.util.functors.DateFormatTransformer;
import com.ftd.customerordermanagement.util.functors.NodeUpdateClosure;
import com.ftd.customerordermanagement.util.functors.ResultSetToXmlTransformer;
import com.ftd.mo.dao.UpdateOrderDAO;
import com.ftd.mo.util.BreadcrumbUtil;
import com.ftd.mo.vo.DeliveryInfoVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.handlers.SourceMasterHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.AccountProgramMasterVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.SourceMasterVO;
import com.ftd.osp.utilities.j2ee.EJBServiceLocator;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.partnerreward.core.ejb.RewardServiceEJB;
import com.ftd.partnerreward.core.ejb.RewardServiceEJBHome;
import com.ftd.security.SecurityManager;
import com.ftd.security.cache.vo.UserInfo;


public class LoadDeliveryInfoBO
{
  /**********************************************************************************************
   * Constants
   **********************************************************************************************/

  private static final String DATE_FORMAT               = "MM/dd/yyyy";
  private static final String DATE_FORMAT_WITH_DAY      = "EEE MM/dd/yyyy";
  private static final String IN_FORMAT                 = "yyyy-MM-dd hh:mm:ss";

  //returned back from VIEW_ORDER_DETAIL_UPDATE
  private static final String OUT_ORIG_PRODUCT          = "OUT_ORIG_PRODUCT";
  private static final String OUT_ORIG_ADD_ONS          = "OUT_ORIG_ADD_ONS";
  private static final String OUT_UPD_ORDER_CUR         = "OUT_UPD_ORDER_CUR";
  private static final String OUT_UPD_ADDON_CUR         = "OUT_UPD_ADDON_CUR"; //not used in this class yet
  private static final String OUT_SHIP_COST             = "OUT_SHIP_COST";
  private static final String OUT_ORIG_TAXES_CUR        = "OUT_ORIG_TAXES_CUR";

  //formatted to be shown on the frontend
  private static final String PD_ORIG_PRODUCTS          = "PD_ORIG_PRODUCTS"; //OUT_ORIG_PRODUCT
  private static final String PD_ORIG_PRODUCT           = "PD_ORIG_PRODUCT";  //OUT_ORIG_PRODUCT
  private static final String PD_ORIG_ADD_ONS           = "PD_ORIG_ADD_ONS";  //OUT_ORIG_ADD_ONS
  private static final String PD_ORIG_ADD_ON            = "PD_ORIG_ADD_ON";   //OUT_ORIG_ADD_ONS
  private static final String PD_ORDER_PRODUCTS         = "PD_ORDER_PRODUCTS";//OUT_UPD_ORDER_CUR
  private static final String PD_ORDER_PRODUCT          = "PD_ORDER_PRODUCT"; //OUT_UPD_ORDER_CUR
  private static final String PD_ORDER_ADDONS           = "PD_ORDER_ADDONS";  //OUT_UPD_ADDON_CUR
  private static final String PD_ORDER_ADDON            = "PD_ORDER_ADDON";   //OUT_UPD_ADDON_CUR
  private static final String PD_SHIP_COSTS             = "PD_SHIP_COSTS";    //OUT_SHIP_COSTS
  private static final String PD_SHIP_COST              = "PD_SHIP_COST";     //OUT_SHIP_COSTS
  private static final String PD_ORDER_TAXES            = "PD_ORDER_TAXES";   //OUT_ORIG_TAXES_CUR
  private static final String PD_ORDER_TAX              = "PD_ORDER_TAX";     //OUT_ORIG_TAXES_CUR

  //XPATHs
  private static final String ORDER_PRODUCTS_XPATH      = PD_ORDER_PRODUCTS + "/" + PD_ORDER_PRODUCT + "/";
  private static final String ORIG_PRODUCTS_XPATH       = PD_ORIG_PRODUCTS  + "/" + PD_ORIG_PRODUCT + "/";

  private static final String SOURCE_CODE_HANDLER_NAME  = "CACHE_NAME_SOURCE_MASTER";
  private static final String REWARDS_MILES_INDICATOR   = "Miles";
  private static final String LOAD_DELIVERY_INFO        = "loadDeliveryInfo.do";
  private static final String BREADCRUMB_TITLE_DELIVERY_INFO  = "Delivery Information";


  /**********************************************************************************************
   * Class level variables
   **********************************************************************************************/
  private HttpServletRequest  request           				= null;
  private Connection          con               				= null;
  private static Logger       logger            				= new Logger("com.ftd.mo.bo.LoadDeliveryInfoBO");

  private String              occasion          				= null;
  private String              req_action        				= "";
  private String              req_errorPage     				= null;
  private String              req_orderDetailId 				= null;
  private String              req_skipFloristValidation = null;
  private String              req_startNewUpdate 				= "";
  private String              req_externalOrderNumber 	= null;
  private String              req_membershipRequired 		= null;
  private String              df_displayPage    				= null;

  private String              sessionId         				= null;
  private String              csrId             				= null;
  private HashMap             hashXML           				= new HashMap();
  private HashMap             pageData          				= new HashMap();

  private static String 			initialContextStr 				= null;
  private static String 			ejbProviderUrl 						= null;


 /**---------------------------------------------------------------------------
  *
  * Constructors
  *
  */
//  public LoadDeliveryInfoBO() {
//  }

  public LoadDeliveryInfoBO(Connection con)
  {
    this.con = con;
  }

  public LoadDeliveryInfoBO(HttpServletRequest request, Connection con)
  {
    this.request = request;
    this.con = con;
  }


 /**---------------------------------------------------------------------------
  *
  * processRequest()
  *
  * Used by the Update Delivery Info Actions to generate the page details
  * and perform validation/updates.  Any display data is passed back to the
  * calling class in a HashMap.
  *
  * @return HashMap - contains 0 - many XML documents of data, 1 HashMap each for page
  *                   details and search criteria
  * @throws
  */
  public HashMap processRequest() throws Exception
  {
    boolean actionSuccessful = true;
    boolean needCacheData = true;
    boolean needSubHeaderFromRequest = true;

    String strutsForwardName        = COMConstants.XSL_UDI_DISPLAY;
    String strutsForwardNameOnError = null;
    OrderDetailsVO odvo = null;

    // HashMap that will be returned to the calling class
    HashMap returnHash = new HashMap();

    // Get info from the request object
    getRequestInfo(this.request);
    logger.info("LoadDeliveryInfoBO processing request for orderDetailId=" +
                 this.req_orderDetailId);

    // Get config file info
    getConfigInfo();


    if (this.req_startNewUpdate.equalsIgnoreCase("Y"))
    {
      getDisplayableDeliveryInfo(true, null);   // Start new Updated Delivery Info process
    }
    else
    {
      getDisplayableDeliveryInfo(false, null);  // Just display current Updated Delivery Info
    }
    strutsForwardName = COMConstants.XSL_UDI_DISPLAY;
    needSubHeaderFromRequest = false;


    /********************************************************************************************
    // Build any XML needed for front end and set forward page
    ********************************************************************************************/

    // Get cached info
    if (needCacheData)
    {
      processStates();
      processCountries();
      processAddrTypes();
      processGiftMessages();
      processBreadcrumbs();
    }

    // Ensure subheader (i.e., the original product order info) gets propagated.
    // Note we don't call this for default action since that's when this info is
    // originally obtained and placed in hashXML directly.
    if (needSubHeaderFromRequest)
    {
      processSubHeader();
    }
    
    processFreeShippingServiceInfo();

    // If any problems set forward page accordingly (via XSL pageData param).
    // Note that any error messages have already been put in an error XML doc
    // via prior calls to addToErrorMsgsXML.
    if (actionSuccessful == false)
    {
      if (strutsForwardNameOnError != null)
      {
        strutsForwardName = strutsForwardNameOnError;
      }
      else if ((this.req_errorPage != null) && (this.req_errorPage.length() > 0))
      {
        strutsForwardName = this.req_errorPage;
      }
      else
      {
        strutsForwardName = COMConstants.XSL_FAILURE;
      }
    }
    this.pageData.put("XSL", strutsForwardName);


    // Populate page data
    populatePageData();


    // Retrieve all the Documents from hashXML and put them in returnHash.
    // Also add pageData to returnHash.
    Set ks1 = hashXML.keySet();
    Iterator iter = ks1.iterator();
    String key = null;
    Document doc = null;
    while(iter.hasNext())
    {
      key = iter.next().toString();
      doc = (Document) hashXML.get(key);
      returnHash.put(key, (Document) doc);
    }
    returnHash.put("pageData", (HashMap)this.pageData);

    return returnHash;
  }


 /**---------------------------------------------------------------------------
  *
  * getRequestInfo(HttpServletRequest request)
  *
  * Retrieve the info from the request object
  *
  * @param  HttpServletRequest - request
  * @return none
  * @throws none
  */
  private void getRequestInfo(HttpServletRequest request) throws Exception
  {
    // Get action
    if (request.getParameter(COMConstants.ACTION)!=null)
      this.req_action = request.getParameter(COMConstants.ACTION);

    // Get Order Detail ID (of order to get delivery info for)
    if(request.getParameter(COMConstants.ORDER_DETAIL_ID)!=null)
      this.req_orderDetailId = request.getParameter(COMConstants.ORDER_DETAIL_ID);

    // Get page that triggered this request.  This is saved via data filter so when update
    // process is all done this value can be used to determine where to go back to.
    // We get local copy since we need it for situations like when update is cancelled.
    if(request.getParameter(COMConstants.UO_DISPLAY_PAGE)!=null)
      this.df_displayPage = request.getParameter(COMConstants.UO_DISPLAY_PAGE);
    else if (logger.isDebugEnabled())
        logger.warn("DeliveryInfoBO was not passed a uo_display_page parameter. Action was: " + this.req_action);

    // Get flag indicating if florist validation is necessary.  If (for example) current order can't
    // be fulfilled by assigned florist and user indicated this was ok, this will be
    // set to 'y'.  We'll end up setting flag in data filter so later in the order process
    // the florist lookup popup can be displayed.
    if (request.getParameter(COMConstants.UDI_SKIP_FLORIST_VALIDATION) != null)
      this.req_skipFloristValidation = request.getParameter(COMConstants.UDI_SKIP_FLORIST_VALIDATION);

    this.req_startNewUpdate = request.getParameter(COMConstants.UDI_START_NEW_UPDATE);
    if (this.req_startNewUpdate == null)
      this.req_startNewUpdate = "N";

    // Get page to go back to in event of an error
    if(request.getParameter(COMConstants.ERROR_DISPLAY_PAGE)!=null)
        this.req_errorPage = request.getParameter(COMConstants.ERROR_DISPLAY_PAGE);
    else
        this.req_errorPage = COMConstants.CONS_SYSTEM_ERROR;

    // Get external order number in case we need to goto CUSTOMER_ORDER_SEARCH page
    if (request.getParameter(COMConstants.EXTERNAL_ORDER_NUMBER) != null)
      this.req_externalOrderNumber = request.getParameter(COMConstants.EXTERNAL_ORDER_NUMBER);

    // Get the occasion
    if (request.getParameter(COMConstants.OCCASION) != null)
      this.occasion = request.getParameter(COMConstants.OCCASION);

    // Get external membership required
    if (request.getParameter("membership_required") != null)
      this.req_membershipRequired = request.getParameter("membership_required");

    // Retrieve Security Info
    this.sessionId   = request.getParameter(COMConstants.SEC_TOKEN);

  }


 /**---------------------------------------------------------------------------
  *
  * getConfigInfo()
  *
  * Retrieve the info from the configuration file
  *
  * @param none
  * @return
  * @throws none
  */
  private void getConfigInfo() throws Exception
  {

    ConfigurationUtil cu = null;
    cu = ConfigurationUtil.getInstance();

    //get csr Id
    String security = (String) cu.getProperty(COMConstants.PROPERTY_FILE,COMConstants.SECURITY_IS_ON);
    boolean securityIsOn = security.equalsIgnoreCase("true") ? true : false;

   //delete this before going to production.  the following should only check on securityIsOn
    if(securityIsOn || this.sessionId != null)
    {
      SecurityManager sm = SecurityManager.getInstance();
      UserInfo ui = sm.getUserInfo(this.sessionId);
      this.csrId = ui.getUserID();
    }

  }


 /**----------------------------------------------------------------------------
  *
  * getDisplayableDeliveryInfo()
  *
  * Gets updated delivery info (and original order info) from database
  * and places in XML document.  If a_startNewUpdate is true, then original
  * delivery info is used to start a new update.
  *
  * @param  a_startNewUpdate - true starts a new update
  * @param  diVO - DeliveryInfoVO for use only in obtaining delivery dates.
  *                      If null, then info from DB is used instead.
  * @return none
  * @throws none
  */
  private void getDisplayableDeliveryInfo(boolean a_startNewUpdate,
                                            DeliveryInfoVO diVO) throws Exception
  {
    boolean forceFloralDates = false;
    DeliveryInfoVO divoDateInfo = new DeliveryInfoVO();

    // Get data from DB
    UpdateOrderDAO updateOrderDAO = new UpdateOrderDAO(this.con);

    HashMap deliveryInfoMap = updateOrderDAO.getOrderDetailsUpdate(
                              this.req_orderDetailId,
                              COMConstants.CONS_ORDER_DETAILS_UPDATE_TYPE,
                              a_startNewUpdate, this.csrId, null);

    // Also reset data filter flag if starting anew
    if (a_startNewUpdate)
      updateDataFilterValue(COMConstants.FLORIST_CANT_FULFILL, COMConstants.CONS_NO);

    //------------
    //
    // Transform CachedResultSets for the three cursors (delivery info, original product info,
    // addon info) into XML.

    /***********************************************************************************************
    // OUT_CUR (containing delivery info)
    ***********************************************************************************************/
    Transformer transformer = ResultSetToXmlTransformer.getInstance(PD_ORDER_PRODUCTS, PD_ORDER_PRODUCT, true);
    Object transformed = transformer.transform( deliveryInfoMap.get(OUT_UPD_ORDER_CUR) );
    if ( transformed != null )
      this.hashXML.put(COMConstants.HK_UDI_DELIVERY_INFO_XML, (Document)transformed);

    // Convert dates to presentable format here (so front end doesn't have to)
    transformer = DateFormatTransformer.getInstance(IN_FORMAT, DATE_FORMAT);
    Closure closure = NodeUpdateClosure.getInstance(transformer, (Document)transformed);
    closure.execute(ORDER_PRODUCTS_XPATH+"updated_on/text()");
    closure.execute(ORDER_PRODUCTS_XPATH+"delivery_date/text()");
    closure.execute(ORDER_PRODUCTS_XPATH+"delivery_date_range_end/text()");
    closure.execute(ORDER_PRODUCTS_XPATH+"ship_date/text()");
    closure.execute(ORDER_PRODUCTS_XPATH+"exception_start_date/text()");
    closure.execute(ORDER_PRODUCTS_XPATH+"exception_end_date/text()");


    //Retrieve fields from PD_ORDER_PRODUCTS as required
    String occasion                 = getNodeValue(ORDER_PRODUCTS_XPATH + COMConstants.UDI_OCCASION, (Document)transformed);
    String originId                 = getNodeValue(ORDER_PRODUCTS_XPATH + COMConstants.UDI_ORIGIN_ID, (Document)transformed);
    String recipientCountry         = getNodeValue(ORDER_PRODUCTS_XPATH + COMConstants.UDI_RECIPIENT_COUNTRY, (Document)transformed);
    String recipientState           = getNodeValue(ORDER_PRODUCTS_XPATH + COMConstants.UDI_RECIPIENT_STATE, (Document)transformed);
    String recipientZip             = getNodeValue(ORDER_PRODUCTS_XPATH + COMConstants.UDI_RECIPIENT_ZIP_CODE, (Document)transformed);
    String sDeliveryDate            = getNodeValue(ORDER_PRODUCTS_XPATH + COMConstants.UDI_DELIVERY_DATE, (Document)transformed);
    String sDeliveryDateRangeEnd    = getNodeValue(ORDER_PRODUCTS_XPATH + COMConstants.UDI_DELIVERY_DATE_RANGE_END, (Document)transformed);
    String shippingKey              = getNodeValue(ORDER_PRODUCTS_XPATH + "shipping_key", (Document)transformed);
    String sourceCode               = getNodeValue(ORDER_PRODUCTS_XPATH + "source_code", (Document)transformed);


    // Get and transform delivery date list into XML.  But first we need to populate
    // a DeliveryInfoVO (note we only populate it with data DeliveryDateUTIL needs).
    //
    SimpleDateFormat dateFormat = new SimpleDateFormat( DATE_FORMAT );
    SimpleDateFormat dayDateFormat = new SimpleDateFormat( DATE_FORMAT_WITH_DAY );
    Calendar date = Calendar.getInstance();

    //Even if a partner id (program name) is found for a source code, check if it belongs to
    //Discover, Upromise etc.  If so, do not require a membership field.
    //Note:
    // 1) WebOE has this hard-coded, and there are no database columns that we can drive this
    //    off of.  Tim S. knows about this, and once WebOE is fixed, this needs to be updated too.
    // 2) This method is called during initial page load as well as when page validation fails.
    //    The difference is that diVO.MembershipRequired will be populated in the latter.
    //    In that case, do not override
    if (diVO != null &&
        diVO.getMembershipRequired() != null)
      this.pageData.put("membership_required", diVO.getMembershipRequired());
    else
    {
      SourceMasterHandler sch = (SourceMasterHandler) CacheManager.getInstance().getHandler(SOURCE_CODE_HANDLER_NAME);
      SourceMasterVO sourceVo = sch.getSourceCodeById(sourceCode);
      String partnerId = sourceVo.getPartnerId();
      boolean membershipRequired = false;
      if( partnerId != null                                               &&
          partnerId.length() > 0                                          &&
          !partnerId.equalsIgnoreCase(COMConstants.CONS_DISCOVER_CODE)    &&
          !partnerId.equalsIgnoreCase(COMConstants.CONS_UPROMISE_CODE)    &&
          !partnerId.equalsIgnoreCase(COMConstants.CONS_UPROMISE1_CODE)   &&
          !partnerId.equalsIgnoreCase(COMConstants.CONS_CORP15_CODE)      &&
          !partnerId.equalsIgnoreCase(COMConstants.CONS_CORP20_CODE)      &&
          !partnerId.equalsIgnoreCase(COMConstants.CONS_ADVO_CODE)
        )
        membershipRequired = true;
      else
        membershipRequired = false;

      this.pageData.put("membership_required", membershipRequired?"Y":"N");
    }

    if (sDeliveryDate != null && !sDeliveryDate.equalsIgnoreCase(""))
    {
      //Create a Calendar Object
      Calendar cDeliveryDate = Calendar.getInstance();

      //Set the Calendar Object using the date retrieved
      cDeliveryDate.setTime(dateFormat.parse(sDeliveryDate));

      //Set the delivery date
      this.pageData.put("formatted_delivery_date", dayDateFormat.format(cDeliveryDate.getTime()));
    }


    if (sDeliveryDateRangeEnd != null && !sDeliveryDateRangeEnd.equalsIgnoreCase(""))
    {
      //Create a Calendar Object
      Calendar cDeliveryDateRangeEnd = Calendar.getInstance();

      //Set the Calendar Object using the date retrieved
      cDeliveryDateRangeEnd.setTime(dateFormat.parse(sDeliveryDateRangeEnd));

      //Set the delivery date
      this.pageData.put("formatted_delivery_date_range_end", dayDateFormat.format(cDeliveryDateRangeEnd.getTime()));
    }


    if (diVO == null)
    {
      divoDateInfo.setRecipientState(recipientState);
      divoDateInfo.setRecipientZipCode(recipientZip);
      divoDateInfo.setRecipientCountry(recipientCountry);
    }
    else
    {
      divoDateInfo.setDeliveryDate(diVO.getDeliveryDate());
      divoDateInfo.setDeliveryDateRangeEnd(diVO.getDeliveryDateRangeEnd());
      divoDateInfo.setRecipientState(diVO.getRecipientState());
      divoDateInfo.setRecipientZipCode(diVO.getRecipientZipCode());
      divoDateInfo.setRecipientCountry(diVO.getRecipientCountry());
    }
    divoDateInfo.setOccasion(occasion);
    divoDateInfo.setOriginId(originId);


    /***********************************************************************************************
    // OUT_ORIG_PRODUCT
    ***********************************************************************************************/
    transformer = ResultSetToXmlTransformer.getInstance(PD_ORIG_PRODUCTS, PD_ORIG_PRODUCT, true);
    transformed = transformer.transform( deliveryInfoMap.get(OUT_ORIG_PRODUCT) );
    if ( transformed != null )
      this.hashXML.put(COMConstants.HK_UDI_PRODUCT_INFO_XML, (Document)transformed);

    // Convert dates to presentable format here (so front end doesn't have to)
    transformer = DateFormatTransformer.getInstance(IN_FORMAT, DATE_FORMAT);
    closure = NodeUpdateClosure.getInstance(transformer, (Document)transformed);
    closure.execute(ORIG_PRODUCTS_XPATH+"ship_date/text()");
    closure.execute(ORIG_PRODUCTS_XPATH+"delivery_date/text()");
    closure.execute(ORIG_PRODUCTS_XPATH+"delivery_date_range_end/text()");

    /* 
     * NOT USED - BUT LEFT IN PLACE FOR REFERENCE
     * 
    // Get miles info.  If miles have not been posted, we'll need to get them
    // (otherwise they're already in the database fields being returned).
    String discountRewardType = getNodeValue(ORIG_PRODUCTS_XPATH + "discount_reward_type", (Document)transformed);
    if (REWARDS_MILES_INDICATOR.equals(discountRewardType))
    {
      String milesPointsPosted = getNodeValue(ORIG_PRODUCTS_XPATH + "miles_points_posted", (Document)transformed);
      if (milesPointsPosted == null || milesPointsPosted.equals("N"))
      {
        // Miles not posted, so get them
        String miles =  getReward(this.req_orderDetailId);
        // Now put back in XML doc
        NodeList mpList = ((Document) transformed)DOMUtil.selectNodes(,ORIG_PRODUCTS_XPATH + "miles_points");
        if (mpList != null && mpList.getLength() > 0)
        {
          Element mpNode = (Element)mpList.item(0);
          XMLText node = (XMLText)mpNode.getFirstChild();
          if(node != null)
          {
            ArrayList paramList = new ArrayList();
            paramList.add(0,PD_ORIG_PRODUCTS);
            paramList.add(1,PD_ORIG_PRODUCT);
            paramList.add(2,"miles_points");
            // This update won't work if miles_points is originally empty, e.g., <miles_points/>
            DOMUtil.updateNodeValue((Document)transformed, paramList, miles);
          }
          else
          {
            // But this one will
            mpNode.appendChild(((Document) transformed).createTextNode(miles));
          }
        }
      }
    }
    * NOT USED
    */


    /***********************************************************************************************
    // OUT_SHIP_COST
    ***********************************************************************************************/
    transformer = ResultSetToXmlTransformer.getInstance(PD_SHIP_COSTS, PD_SHIP_COST, true);
    transformed = transformer.transform( deliveryInfoMap.get(OUT_SHIP_COST) );
    if ( transformed != null )
      this.hashXML.put(COMConstants.HK_UDI_SHIP_COST_XML, (Document)transformed);


    /***********************************************************************************************
    // OUT_ORIG_ADD_ONS
    ***********************************************************************************************/
    transformer = ResultSetToXmlTransformer.getInstance(PD_ORIG_ADD_ONS, PD_ORIG_ADD_ON, true);
    transformed = transformer.transform( deliveryInfoMap.get(OUT_ORIG_ADD_ONS) );
    if ( transformed != null )
      this.hashXML.put(COMConstants.HK_UDI_ADDONS_XML, (Document)transformed);

    /***********************************************************************************************
    // OUT_ORIG_TAXES_CUR
    ***********************************************************************************************/
    transformer = ResultSetToXmlTransformer.getInstance(PD_ORDER_TAXES, PD_ORDER_TAX, true);
    transformed = transformer.transform( deliveryInfoMap.get(OUT_ORIG_TAXES_CUR) );
    if ( transformed != null )
      this.hashXML.put(COMConstants.HK_ORDER_TAX_XML, (Document)transformed);
    
    
    /**
     * OUT_PAYMENT_CUR
     */
    // Get data from DB
    com.ftd.customerordermanagement.dao.OrderDAO orderDAO = new com.ftd.customerordermanagement.dao.OrderDAO(this.con);
    Document doc = orderDAO.getPaymentInfo("ORDER", this.req_orderDetailId);
    this.hashXML.put(COMConstants.HK_PAYMENT_INFO_XML, doc);

  }


 /**---------------------------------------------------------------------------
  *
  * updateDataFilterValue
  *
  * Private utility method to update a data filter value
  *
  * @params String a_variableName
  * @params String a_value
  * @returns none
  */
  private void updateDataFilterValue(String a_variableName, String a_value) throws Exception
  {
    HashMap dfParams = (HashMap) this.request.getAttribute(COMConstants.CONS_APP_PARAMETERS);
    if(dfParams == null)
      dfParams = new HashMap();

    dfParams.put(a_variableName, a_value);
    this.request.setAttribute(COMConstants.CONS_APP_PARAMETERS, dfParams);
  }


 /**---------------------------------------------------------------------------
  *
  * getNodeValue
  *
  * Private utility method to do xpath query to get a node value.
  *
  * @params String a_xpath       - Xpath to element
  * @params Document a_DocXml - XML document
  * @returns String              - Node value (or null if not found)
  */
  private String getNodeValue(String a_xpath, Document a_docXml) throws Exception
  {
    String retValue = null;
    NodeList nl = DOMUtil.selectNodes(a_docXml,a_xpath);
    if (nl != null && nl.getLength() > 0)
    {
      Text node = (Text)nl.item(0).getFirstChild();
      if(node != null)
        retValue = node.getNodeValue();
    }

    return retValue;
  }


 /**----------------------------------------------------------------------------
  *
  * getReward()
  *
  */
  private String getReward(String a_orderDetailId) throws Exception
  {
    if (initialContextStr == null || ejbProviderUrl == null)
    {
      ConfigurationUtil config = ConfigurationUtil.getInstance();
      initialContextStr = config.getFrpGlobalParm(COMConstants.COM_CONTEXT, "INITIAL_CONTEXT_FACTORY");
      ejbProviderUrl = config.getFrpGlobalParm(COMConstants.COM_CONTEXT, "PR_EJB_PROVIDER_URL");
    }
    String reward = null;

    RewardServiceEJB rewardServiceEJB = (RewardServiceEJB)EJBServiceLocator.getEJBean("RewardServiceEJB",
                                        RewardServiceEJBHome.class, initialContextStr, ejbProviderUrl, true);
    Float rewardFloat = rewardServiceEJB.getReward(a_orderDetailId);
    if (rewardFloat != null)
      reward = rewardFloat.toString();

    if (logger.isDebugEnabled())
      logger.debug("getReward - Used RewardServiceEJB to get reward: " + reward);
    return reward;
  }


 /**---------------------------------------------------------------------------
  *
  * processStates() -
  *
  * @throws none
  */
  private void processStates() throws Exception
  {
    //statesXML will store the states info
    Document statesXML = DOMUtil.getDocument();

    //Instantiate the BasePageBuilder
    BasePageBuilder builder = new BasePageBuilder();

    //and retrieve the states info in an xml document
    statesXML = builder.retrieveStates(this.con, true);
    this.hashXML.put(COMConstants.HK_STATES,  statesXML);
  }


 /**---------------------------------------------------------------------------
  *
  * processCountries() -
  *
  * @throws none
  */
  private void processCountries() throws Exception
  {
    //statesXML will store the states info
    Document countriesXML = DOMUtil.getDocument();

    //Instantiate the BasePageBuilder
    BasePageBuilder builder = new BasePageBuilder();

    //and retrieve the states info in an xml document
    countriesXML = builder.retrieveCountries(this.con);
    this.hashXML.put(COMConstants.HK_COUNTRIES,  countriesXML);
  }


 /**---------------------------------------------------------------------------
  *
  * processAddrTypes() -
  *
  * @throws none
  */
  private void processAddrTypes() throws Exception
  {
    Document addrTypesXML = DOMUtil.getDocument();
    BasePageBuilder builder = new BasePageBuilder();
    addrTypesXML = builder.retrieveAddressTypes(this.con);
    this.hashXML.put(COMConstants.HK_ADDRESS_TYPES,  addrTypesXML);
  }


 /**---------------------------------------------------------------------------
  *
  * processGiftMessages() -
  *
  * @throws none
  */
  private void processGiftMessages() throws Exception
  {
    Document giftMsgXML = DOMUtil.getDocument();
    BasePageBuilder builder = new BasePageBuilder();
    giftMsgXML = builder.retrieveGiftMessages(this.con);
    this.hashXML.put(COMConstants.HK_GIFT_MESSAGES,  giftMsgXML);
  }


 /**---------------------------------------------------------------------------
  *
  * processSubHeader()
  *
  * This will handle placing subheader data (propagated in request)
  * back into the XML to continue the propagation.
  *
  * @throws none
  */
  private void processSubHeader() throws Exception
  {
    BasePageBuilder builder = new BasePageBuilder();
    this.hashXML.put(COMConstants.HK_SUB_HEADER, builder.retrieveSubHeaderData(request));
  }


 /**---------------------------------------------------------------------------
  *
  * processBreadcrumbs()
  *
  * @throws none
  */
  private void processBreadcrumbs() throws Exception
  {
    HashMap optionalParams = null;
    BreadcrumbUtil bcUtil = new BreadcrumbUtil();

    //bcUtil.getBreadcrumbXML(
                          //request,
                          //Title of breadcrumb,
                          //Name of Structs Action,
                          //Action parameter,
                          //Error display page,
                          //Flag indicating if breadcrumb should be a link, ,
                          //Optional hash of additional name/value parameters for this page
                          //order detail id
                      //)

    Document bcXML = bcUtil.getBreadcrumbXML(
                            this.request, 												//request
                            BREADCRUMB_TITLE_DELIVERY_INFO, 			//title
                            LOAD_DELIVERY_INFO,										//name
                            COMConstants.ACTION_UDI_GET_UPDATED, 	//action parm
                            "Error",															//error page
                            "true", 															//link=true; no link=false
                            optionalParams, 											//optional parms 
                            this.req_orderDetailId); 							//order detail id
    
    this.hashXML.put("breadcrumbs",  bcXML);
  }



 /**---------------------------------------------------------------------------
  *
  * populatePageData()
  *
  * Populate the page data with the remainder of the fields
  *
  * @param none
  * @return
  * @throws none
  */
  private void populatePageData()
  {
    //we may need to pass the df_displayPage
    //this.pageData.put(, );
  }


  /**
   * Processes the information required for Free Shipping
   */
  private void processFreeShippingServiceInfo()
    throws Exception
  {
    System.out.println(this.req_orderDetailId);
    ServicesProgramDAO servicesProgramDAO = new ServicesProgramDAO();

    //SN: Added this for UC, Setting the String value to avoid the NullPointerException.
    Boolean fsVal = servicesProgramDAO.getBuyerHasFreeShipping(con, this.req_orderDetailId);
    
    //String fsVal = (servicesProgramDAO.getBuyerHasFreeShipping(con,this.req_orderDetailId)).toString();
    
    //if(fsVal == null || fsVal.length() <= 0)
    if(fsVal != null){
	    if(fsVal)
	    {
	      // Set free Shipping Fields for Y into the Page Date  	
	      this.pageData.put(COMConstants.FREESHIP_BUYER_IS_ACTIVE_MEMBER, "N");  
	    } else {
	      // Set free Shipping Fields for N into the Page Date        
	      this.pageData.put(COMConstants.FREESHIP_BUYER_IS_ACTIVE_MEMBER, "Y");
	    }
    }
    AccountProgramMasterVO acctProgMasterVO = servicesProgramDAO.getFreeShippingProgramInfo();
    this.pageData.put(COMConstants.FREESHIP_PROGRAM_NAME, acctProgMasterVO.getDisplayName());
  }
}
