package com.ftd.mo.bo;
import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.vo.OrderBillVO;
import com.ftd.customerordermanagement.vo.OrderDetailVO;
import com.ftd.mo.dao.UpdateOrderDAO;
import com.ftd.customerordermanagement.dao.ViewDAO;
import com.ftd.customerordermanagement.util.BasePageBuilder;
import com.ftd.mo.util.BreadcrumbUtil;
import com.ftd.customerordermanagement.util.COMDeliveryDateUtil;
import com.ftd.customerordermanagement.util.DeliveryDateConfiguration;
import com.ftd.customerordermanagement.util.DeliveryDateParameters;
import com.ftd.mo.util.SearchUtil;
import com.ftd.mo.validation.ValidationConfiguration;
import com.ftd.mo.validation.ValidationLevels;
import com.ftd.mo.validation.ValidationRules;
import com.ftd.mo.validation.Validator;
import com.ftd.mo.validation.impl.DeliveryDateValidatorFactory;
import com.ftd.customerordermanagement.vo.AddOnVO;
import com.ftd.mo.vo.DeliveryInfoVO;
import com.ftd.mo.vo.ProductFlagsVO;
import com.ftd.ftdutilities.OEDeliveryDateParm;
import com.ftd.ftdutilities.OEParameters;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.XMLFormat;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.security.SecurityManager;
import com.ftd.security.cache.vo.UserInfo;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.util.List;
import java.sql.Connection;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;



import org.apache.commons.lang.StringUtils;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;




public class LoadProductDetailBO
{

/*******************************************************************************************
 * Class level variables
 *******************************************************************************************/
  private HttpServletRequest  request                 = null;
  private Connection          con                     = null;
  private static Logger       logger                  = new Logger("com.ftd.mo.bo.LoadProductDetailBO");


	//request parameters required to be sent back to XSL all the time.
  private String              buyerName						    = null;
  private String              categoryIndex				    = null;
  private String              companyId               = null;
  private String              customerId  				    = null;
  private String              deliveryDate            = "";
  private String              deliveryDateRange       = "";
  private String              externalOrderNumber	    = null;
  private String              masterOrderNumber		    = null;
  private String              occasion  					    = null;
  private String              occasionText				    = null;
  private String              orderDetailId				    = null;
  private String              orderGuid						    = null;
  private String              originId                = null;
  private	String							origProductAmount		    = null;
  private	String							origProductId				    = null;
  private String              pageNumber					    = null;
  private String              pricePointId				    = null;
  private String              recipientCity           = null;
  private String              recipientCountry        = null;
  private String              recipientState          = null;
  private String              recipientZipCode        = null;
  private String              rewardType              = null;
  private String              shipMethod              = null;
  private String              sourceCode					    = null;

	//other variables needed for this class
  private String              actionType              = null;
  private Calendar            cDeliveryDate           = null;
  private Calendar            cDeliveryDateRange      = null;
  private String              csrId                   = null;
  private String              displayFormat				    = null;
  private boolean             domesticFlag		  	    = true;
  private String              domesticSvcFee			    = null;
  private String              domIntFlag              = null;
  private boolean             globalChocAvailFlag     = false;
  private HashMap             hashXML                 = new HashMap();
  private String              imageLocation				    = null;
  private String              internationalSvcFee	    = null;
  private boolean             invokedFromSearchAction = false;
  private String              listFlag					      = null;
  private HashMap             pageData                = new HashMap();
  private String              partnerId						    = null;
  private String              pricingCode					    = null;
  private String              productId               = null;
  private boolean             productPassedErrorChecks = false;
  private String              productSubCodeId        = null;
  private String              sessionId               = null;
  private String              shipMethodCarrier       = null;
  private String              shipMethodFlorist       = null;
  private String              snhId								    = null;
  private SearchUtil          sUtil;
  private String              totalPages              = null;
  private String              upsellFlag  		  	    = null;
  private boolean             validProduct		  	    = false;
  private String              vendorFlagNewProduct    = null;
  private String              vendorFlagOriginal	    = null;
  private boolean             wasMaxRetrieved         = false;

  //hashmap of all the types of addons
  private HashMap             addOnTypesHash          = new HashMap(); //key = addon-type (eg. balloon)

  //hashmaps that will have the 'Y' or 'N' flags based on order, original product id, and new product id
  private HashMap             updateProductAddOnFlagsHash = new HashMap(); //key = addon-type (eg. balloon)


  //constants
  private static final String BALLOON                   = "BALLOON";
  private static final String BEAR                      = "BEAR";
  private static final String CARD                      = "CARD";
  private static final String CHOCOLATE                 = "CHOCOLATE";
  private static final String DATE_FORMAT               = "MM/dd/yyyy";
  private static final String FUNERAL                   = "FUNERAL BANNER";
  private static final String INPUT_DATE_FORMAT         = "MM/dd/yyyy";
  private static final String OCCASION_SYMPATHY_FUNERAL = "1";
  private static final String POPUP_INDICATOR_OK        = "O";
  private static final String POPUP_INDICATOR_CONFIRM   = "C";
  private static final String LOAD_PRODUCT_CATEGORY     = "loadProductCategory.do";
  private static final String LOAD_PRODUCT_DETAIL       = "loadProductDetail.do?action_type=delete_addon";

  // Standard, Deluxe, Premium labels
  private String              standardLabel       = null;
  private String              deluxeLabel         = null;
  private String              premiumLabel        = null;


/*******************************************************************************************
 * Constructor 1
 *******************************************************************************************/
  public LoadProductDetailBO()
  {
  }


/*******************************************************************************************
 * Constructor 2
 *******************************************************************************************/
  public LoadProductDetailBO(HttpServletRequest request, Connection con)
  {
    this.request = request;
    this.con = con;
  }


/*******************************************************************************************
  * processRequest()
  ******************************************************************************************
  *
  * @return HashMap - contains 0 - many XML documents of data, 1 HashMap each for page
  *                   details and search criteria
  * @throws
  */
  public HashMap processRequest() throws Exception
  {
    //HashMap that will be returned to the calling class
    HashMap returnHash = new HashMap();

    //Get info from the request object
    getRequestInfo(this.request);

    //Get config file info
    getConfigInfo();

    //instantiate the search util
    this.sUtil = new SearchUtil(this.con);

    //find if the recipient recipientCountry was domestic or not
    this.domesticFlag = this.sUtil.isDomestic(this.recipientCountry);
    if (this.domesticFlag)
      this.domIntFlag = COMConstants.CONS_DELIVERY_TYPE_DOMESTIC;
    else
      this.domIntFlag = COMConstants.CONS_DELIVERY_TYPE_INTERNATIONAL;

    //process the action
    processMain();

    //populate the remainder of the fields on the page data
    populatePageData();

    //process bread crumbs
    processBreadcrumbs();

    //populate sub header info
    populateSubHeaderInfo();

    //At this point, we should have three HashMaps.
    // HashMap1 = hashXML         - hashmap that contains zero to many Documents
    // HashMap2 = pageData        - hashmap that contains String data at page level
    //
    //Combine all of the above HashMaps into one HashMap, called returnHash

    //retrieve all the Documents from hashXML and put them in returnHash
    //retrieve the keyset
    Set ks1 = hashXML.keySet();
    Iterator iter = ks1.iterator();
    String key = null;
    Document doc = null;

    //Iterate thru the keyset
    while(iter.hasNext())
    {
      //get the key
      key = iter.next().toString();

      //retrieve the XML document
      doc = (Document) hashXML.get(key);

      //put the XML object in the returnHash
      returnHash.put(key, (Document) doc);
    }

    //append pageData to returnHash
    returnHash.put("pageData",        (HashMap)this.pageData);

    return returnHash;
  }



/*******************************************************************************************
  * getRequestInfo(HttpServletRequest request)
  ******************************************************************************************
  * Retrieve the info from the request object
  *
  * @param  HttpServletRequest - request
  * @return none
  * @throws none
  */
  private void getRequestInfo(HttpServletRequest request) throws Exception
  {

    /****************************************************************************************
     *  Retrieve order/search specific parameters
     ***************************************************************************************/
    //retrieve the action
    if(request.getParameter(COMConstants.ACTION_TYPE)!=null)
    {
      this.actionType = request.getParameter(COMConstants.ACTION_TYPE);
    }

    //retrieve the buyer_full_name
    if(request.getParameter(COMConstants.BUYER_FULL_NAME)!=null)
    {
      this.buyerName = request.getParameter(COMConstants.BUYER_FULL_NAME);
    }

    //retrieve the category Index
    if(request.getParameter(COMConstants.CATEGORY_INDEX)!=null)
    {
      this.categoryIndex = request.getParameter(COMConstants.CATEGORY_INDEX);
    }

    //retrieve the company id
    if(request.getParameter(COMConstants.COMPANY_ID)!=null)
    {
      this.companyId = request.getParameter(COMConstants.COMPANY_ID);
    }

    //retrieve the customer id
    if(request.getParameter(COMConstants.CUSTOMER_ID)!=null)
    {
      this.customerId = request.getParameter(COMConstants.CUSTOMER_ID);
    }

    //retrieve the delivery date
    if(request.getParameter(COMConstants.DELIVERY_DATE)!=null)
    {
      this.deliveryDate = request.getParameter(COMConstants.DELIVERY_DATE);
    }

    //retrieve the delivery date range end
    if(request.getParameter(COMConstants.DELIVERY_DATE_RANGE_END)!=null)
    {
      this.deliveryDateRange = request.getParameter(COMConstants.DELIVERY_DATE_RANGE_END);
    }

    //retrieve the external order number
    if(request.getParameter(COMConstants.EXTERNAL_ORDER_NUMBER)!=null)
    {
      this.externalOrderNumber = request.getParameter(COMConstants.EXTERNAL_ORDER_NUMBER);
    }

    //retrieve the list flag
    if(request.getParameter(COMConstants.LIST_FLAG)!=null)
    {
      this.listFlag = request.getParameter(COMConstants.LIST_FLAG);
    }

    //retrieve the master order number
    if(request.getParameter(COMConstants.MASTER_ORDER_NUMBER)!=null)
    {
      this.masterOrderNumber = request.getParameter(COMConstants.MASTER_ORDER_NUMBER);
    }

    //retrieve the occasion id
    if(request.getParameter(COMConstants.OCCASION)!=null)
    {
      this.occasion = request.getParameter(COMConstants.OCCASION);
    }

    //retrieve the occasion text
    if(request.getParameter(COMConstants.OCCASION_TEXT)!=null)
    {
      this.occasionText = request.getParameter(COMConstants.OCCASION_TEXT);
    }

    //retrieve the orig product amount
    if(request.getParameter(COMConstants.ORIG_PRODUCT_AMOUNT)!=null)
    {
      this.origProductAmount = request.getParameter(COMConstants.ORIG_PRODUCT_AMOUNT);
    }

    //retrieve the orig product id
    if(request.getParameter(COMConstants.ORIG_PRODUCT_ID)!=null)
    {
      this.origProductId = (request.getParameter(COMConstants.ORIG_PRODUCT_ID)).toUpperCase();
    }

    //retrieve the order detail id
    if(request.getParameter(COMConstants.ORDER_DETAIL_ID)!=null)
    {
      this.orderDetailId = request.getParameter(COMConstants.ORDER_DETAIL_ID);
    }

    //retrieve the order guid
    if(request.getParameter(COMConstants.ORDER_GUID)!=null)
    {
      this.orderGuid = request.getParameter(COMConstants.ORDER_GUID);
    }

    //retrieve the origin id
    if(request.getParameter(COMConstants.ORIGIN_ID)!=null)
    {
      this.originId = request.getParameter(COMConstants.ORIGIN_ID);
    }

    //retrieve the page number
    if(request.getParameter(COMConstants.PAGE_NUMBER)!=null)
    {
      this.pageNumber = request.getParameter(COMConstants.PAGE_NUMBER);
    }

    //retrieve the price point id
    if(request.getParameter(COMConstants.PRICE_POINT_ID)!=null)
    {
      this.pricePointId = request.getParameter(COMConstants.PRICE_POINT_ID);
    }

    //retrieve the product id
    if(request.getParameter(COMConstants.PRODUCT_ID)!=null)
    {
      this.productId = (request.getParameter(COMConstants.PRODUCT_ID)).toUpperCase();
    }
    //in the case that the csr went from delivery info to delivery confirmation page and hit the
    //edit product link, we could potentially have a null or "" for product id.  In that case,
    //we want to default it to the original product id.
    if (this.productId == null || this.productId.equalsIgnoreCase(""))
      this.productId = this.origProductId;


    //retrieve the city
    if(request.getParameter(COMConstants.RECIPIENT_CITY)!=null)
    {
      this.recipientCity = request.getParameter(COMConstants.RECIPIENT_CITY);
    }

    //retrieve the recipientCountry id
    if(request.getParameter(COMConstants.RECIPIENT_COUNTRY)!=null)
    {
      this.recipientCountry = request.getParameter(COMConstants.RECIPIENT_COUNTRY);
    }

    //retrieve the recipientState
    if(request.getParameter(COMConstants.RECIPIENT_STATE)!=null)
    {
      this.recipientState = request.getParameter(COMConstants.RECIPIENT_STATE);
    }

    //retrieve the postal code
    if(request.getParameter(COMConstants.RECIPIENT_ZIP_CODE)!=null)
    {
      this.recipientZipCode = request.getParameter(COMConstants.RECIPIENT_ZIP_CODE);
    }

    //retrieve the reward type
    if(request.getAttribute(COMConstants.REWARD_TYPE)!=null)
    {
      this.rewardType = request.getAttribute(COMConstants.REWARD_TYPE).toString();
    }
    if((this.rewardType == null || this.rewardType.equalsIgnoreCase("")) && request.getParameter(COMConstants.REWARD_TYPE)!=null)
    {
      this.rewardType = request.getParameter(COMConstants.REWARD_TYPE);
    }

    //retrieve the ship method
    if(request.getParameter(COMConstants.SHIP_METHOD)!=null)
    {
      this.shipMethod = request.getParameter(COMConstants.SHIP_METHOD);
    }

    //retrieve the ship method carrier
    if(request.getParameter(COMConstants.SHIP_METHOD_CARRIER)!=null)
    {
      this.shipMethodCarrier = request.getParameter(COMConstants.SHIP_METHOD_CARRIER);
    }

    //retrieve the ship method florist
    if(request.getParameter(COMConstants.SHIP_METHOD_FLORIST)!=null)
    {
      this.shipMethodFlorist = request.getParameter(COMConstants.SHIP_METHOD_FLORIST);
    }

    //retrieve the source code
    if(request.getParameter(COMConstants.SOURCE_CODE)!=null)
    {
      this.sourceCode = request.getParameter(COMConstants.SOURCE_CODE);
    }

    //retrieve the upsell flag
    if(request.getParameter(COMConstants.UPSELL_FLAG)!=null)
    {
      this.upsellFlag = request.getParameter(COMConstants.UPSELL_FLAG);
    }

    /****************************************************************************************
     *  Retrieve Security Info
     ***************************************************************************************/
    this.sessionId   = request.getParameter(COMConstants.SEC_TOKEN);

    /******************************************************************************************
     * Set the calendar objects for dates
     *****************************************************************************************/
    SimpleDateFormat dateFormat = new SimpleDateFormat( INPUT_DATE_FORMAT );

    if ( StringUtils.isNotEmpty(this.deliveryDate) )
    {
      //Create a Calendar Object
      this.cDeliveryDate = Calendar.getInstance();
      //Set the Calendar Object using the date retrieved in sCreatedOn
      this.cDeliveryDate.setTime( dateFormat.parse( this.deliveryDate ) );
    }

    if ( StringUtils.isNotEmpty(this.deliveryDateRange) )
    {
      //Create a Calendar Object
      this.cDeliveryDateRange = Calendar.getInstance();
      //Set the Calendar Object using the date retrieved in sCreatedOn
      this.cDeliveryDateRange.setTime( dateFormat.parse( this.deliveryDateRange ) );
    }



    /****************************************************************************************
     *  This page can be invoked from the productList page, the upsellDetail page or when
     *  the CSR enters in an item on the updateProductCategory page.  In the case where this
     *  page gets control from the updateProductCategory page, we have already done the
     *  product search (in SearchAction and SearchBO).  Therefore, in that case, we are
     *  passing all the relevant info in the request via "searchresults" attribute:
     *
     *        request.setAttribute("searchresults", responseHash); //in SearchAction
     *
     *  This well help us determine if we have invoke the search process or bypass it.
     ***************************************************************************************/

    HashMap searchResults = (HashMap) request.getAttribute(COMConstants.HK_SEARCH_RESULTS_ATTRIBUTE);
    if (searchResults != null && searchResults.size() > 0)
    {
      this.invokedFromSearchAction = true;

      //retrieve the keys
      Set ks = searchResults.keySet();
      Iterator i = ks.iterator();
      String key;

      //iterate thru the keys
      while(i.hasNext())
      {
        key = (String) i.next();

        //if key = page data, move it to a pageData hashmap.  else, add it to the hashXML.
        if (key.equalsIgnoreCase("pagedata"))
          this.pageData = (HashMap) searchResults.get(key);
        else
          this.hashXML.put(key, (Document)searchResults.get(key));
      }
    }


    /****************************************************************************************
     *  This page can also be be invoked from the UpdateProductDetailBO if an error were
     *  found.  In this case, just retrieve the error xml, and populate the page data.
     *  Normal LoadProductDetailBO processing will occur.
     *
     ***************************************************************************************/

    HashMap updateProductResults = (HashMap) request.getAttribute(COMConstants.HK_UPD_ATTRIBUTE);
    String requestedSizeDescription = (String) request.getAttribute("requested_size_description");
    if (updateProductResults != null && updateProductResults.size() > 0)
    {
      if (StringUtils.isNotEmpty(requestedSizeDescription))
        this.pageData.put("requested_size_description", requestedSizeDescription);
        
      this.hashXML.put(COMConstants.HK_ERROR_MESSAGES,
                        (Document) updateProductResults.get(COMConstants.HK_ERROR_MESSAGES));
      this.hashXML.put(COMConstants.HK_UPD_NEW_ORDER_DETAIL,
                        (Document) updateProductResults.get(COMConstants.HK_UPD_NEW_ORDER_DETAIL));
    }

  }


/*******************************************************************************************
  * getConfigInfo()
  ******************************************************************************************
  * Retrieve the info from the configuration file
  *
  * @param none
  * @return
  * @throws none
  */
  private void getConfigInfo() throws Exception
  {

    ConfigurationUtil cu = null;
    cu = ConfigurationUtil.getInstance();

    //get csr Id
    String security = (String) cu.getProperty(COMConstants.PROPERTY_FILE,COMConstants.SECURITY_IS_ON);
    boolean securityIsOn = security.equalsIgnoreCase("true") ? true : false;

   //delete this before going to production.  the following should only check on securityIsOn
    if(securityIsOn || this.sessionId != null)
    {
      SecurityManager sm = SecurityManager.getInstance();
      UserInfo ui = sm.getUserInfo(this.sessionId);
      this.csrId = ui.getUserID();
    }

    //get PRODUCT_IMAGE_LOCATION
    if (cu.getProperty(COMConstants.PROPERTY_FILE, COMConstants.CONS_PRODUCT_IMAGE_LOCATION) != null)
    {
      this.imageLocation  = cu.getProperty(COMConstants.PROPERTY_FILE, COMConstants.CONS_PRODUCT_IMAGE_LOCATION);
      this.pageData.put(COMConstants.PD_MO_PRODUCT_IMAGE_LOCATION, imageLocation);
    }

    //get Standard, Deluxe, Premium labels
    String tempLabel = cu.getFrpGlobalParm("FTDAPPS_PARMS", "FLORAL_LABEL_STANDARD");
    if (tempLabel != null) {
        this.standardLabel = tempLabel;
    }
    tempLabel = cu.getFrpGlobalParm("FTDAPPS_PARMS", "FLORAL_LABEL_DELUXE");
    if (tempLabel != null) {
        this.deluxeLabel = tempLabel;
    }
    tempLabel = cu.getFrpGlobalParm("FTDAPPS_PARMS", "FLORAL_LABEL_PREMIUM");
    if (tempLabel != null) {
        this.premiumLabel = tempLabel;
    }

  }



/*******************************************************************************************
  * populatePageData()
  ******************************************************************************************
  * Populate the page data with the remainder of the fields
  *
  * @param none
  * @return
  * @throws none
  */
  private void populatePageData()
  {
    this.pageData.put(COMConstants.PD_MO_BUYER_FULL_NAME,         this.buyerName);
    this.pageData.put(COMConstants.PD_MO_CATEGORY_INDEX,          this.categoryIndex);
    this.pageData.put(COMConstants.PD_MO_COMPANY_ID,              this.companyId);
    this.pageData.put(COMConstants.PD_MO_CUSTOMER_ID,             this.customerId);
    this.pageData.put(COMConstants.PD_MO_DELIVERY_DATE,           this.deliveryDate);
    this.pageData.put(COMConstants.PD_MO_DELIVERY_DATE_RANGE_END, this.deliveryDateRange);
    this.pageData.put(COMConstants.PD_MO_DOMESTIC_INT_FLAG,       this.domIntFlag);
    this.pageData.put(COMConstants.PD_MO_DOMESTIC_SRVC_FEE,       this.domesticSvcFee);
    this.pageData.put(COMConstants.PD_MO_EXTERNAL_ORDER_NUMBER,   this.externalOrderNumber);
    this.pageData.put(COMConstants.PD_MO_PRODUCT_IMAGE_LOCATION,  this.imageLocation);
    this.pageData.put(COMConstants.PD_MO_INTERNATIONAL_SRVC_FEE,  this.internationalSvcFee);
    this.pageData.put(COMConstants.PD_MO_MASTER_ORDER_NUMBER,     this.masterOrderNumber);
    this.pageData.put(COMConstants.PD_MO_OCCASION,                this.occasion);
    this.pageData.put(COMConstants.PD_MO_OCCASION_TEXT,           this.occasionText);
    this.pageData.put(COMConstants.PD_MO_ORIG_PRODUCT_AMOUNT,			this.origProductAmount);
    this.pageData.put(COMConstants.PD_MO_ORIG_PRODUCT_ID,					this.origProductId);
    this.pageData.put(COMConstants.PD_MO_ORDER_DETAIL_ID,         this.orderDetailId);
    this.pageData.put(COMConstants.PD_MO_ORDER_GUID,              this.orderGuid);
    this.pageData.put(COMConstants.PD_MO_ORIGIN_ID,               this.originId);
    this.pageData.put(COMConstants.PD_MO_PAGE_NUMBER,             this.pageNumber);
    this.pageData.put(COMConstants.PD_MO_PARTNER_ID,              this.partnerId);
    this.pageData.put(COMConstants.PD_MO_PRICE_POINT_ID,          this.pricePointId);
    this.pageData.put(COMConstants.PD_MO_PRICING_CODE,            this.pricingCode);
    this.pageData.put(COMConstants.PD_MO_PRODUCT_ID,  						this.productId);
    this.pageData.put(COMConstants.PD_MO_RECIPIENT_CITY,          this.recipientCity);
    this.pageData.put(COMConstants.PD_MO_RECIPIENT_COUNTRY,       this.recipientCountry);
    this.pageData.put(COMConstants.PD_MO_RECIPIENT_STATE,         this.recipientState);
    this.pageData.put(COMConstants.PD_MO_RECIPIENT_ZIP_CODE,      this.recipientZipCode);
    this.pageData.put(COMConstants.PD_MO_REWARD_TYPE,             this.rewardType);
    this.pageData.put(COMConstants.PD_MO_SHIP_METHOD,             this.shipMethod);
    this.pageData.put(COMConstants.PD_MO_SNH_ID,                  this.snhId);
    this.pageData.put(COMConstants.PD_MO_SOURCE_CODE,             this.sourceCode);
    this.pageData.put(COMConstants.PD_MO_TOTAL_PAGES,             this.totalPages);
    this.pageData.put(COMConstants.PD_MO_UPSELL_FLAG,             this.upsellFlag);
    this.pageData.put(COMConstants.PD_MO_MAX_DELIVERY_DATES_RETRIEVED, this.wasMaxRetrieved?COMConstants.CONS_YES:COMConstants.CONS_NO);

    this.pageData.put(COMConstants.PD_MO_FLORAL_LABEL_STANDARD, this.standardLabel);
    this.pageData.put(COMConstants.PD_MO_FLORAL_LABEL_DELUXE, this.deluxeLabel);
    this.pageData.put(COMConstants.PD_MO_FLORAL_LABEL_PREMIUM, this.premiumLabel);
    
    Calendar cal = Calendar.getInstance();
    cal.add(Calendar.DATE, 0);
    
    String DATE_FORMAT = "MM/dd/yyyy";
    SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT); 
    String dateString = sdf.format(cal.getTime());
    this.pageData.put("currentDate", dateString);
  }


/*******************************************************************************************
  * populateSubHeaderInfo()
  ******************************************************************************************
  * Populate the page data with the remainder of the fields
  *
  * @param none
  * @return
  * @throws none
  */
  private void populateSubHeaderInfo() throws Exception
  {
    BasePageBuilder bpb = new BasePageBuilder();

    //xml document for sub header
    Document subHeaderXML = DOMUtil.getDocument();

    //retrieve the sub header info
    subHeaderXML = bpb.retrieveSubHeaderData(this.request);

    //save the original product xml
    this.hashXML.put(COMConstants.HK_SUB_HEADER, subHeaderXML);

  }


/*******************************************************************************************
  * processBreadcrumbs()
  ******************************************************************************************
  *
  *
  * @param  none
  * @return none
  * @throws none
  */
  private void processBreadcrumbs() throws Exception
  {
    HashMap optionalParms = new HashMap();
    optionalParms.put(COMConstants.ACTION_TYPE,                 this.actionType);
    optionalParms.put(COMConstants.BUYER_FULL_NAME,             this.buyerName);
    optionalParms.put(COMConstants.CATEGORY_INDEX,              this.categoryIndex);
    optionalParms.put(COMConstants.COMPANY_ID,                  this.companyId);
    optionalParms.put(COMConstants.CUSTOMER_ID,                 this.customerId);
    optionalParms.put(COMConstants.DELIVERY_DATE,               this.deliveryDate);
    optionalParms.put(COMConstants.DELIVERY_DATE_RANGE_END,     this.deliveryDateRange);
    optionalParms.put(COMConstants.EXTERNAL_ORDER_NUMBER,       this.externalOrderNumber);
    optionalParms.put(COMConstants.LIST_FLAG,                   this.listFlag);
    optionalParms.put(COMConstants.MASTER_ORDER_NUMBER,         this.masterOrderNumber);
    optionalParms.put(COMConstants.OCCASION,                    this.occasion);
    optionalParms.put(COMConstants.OCCASION_TEXT,               this.occasionText);
    optionalParms.put(COMConstants.ORIG_PRODUCT_AMOUNT,  		    this.origProductAmount);
    optionalParms.put(COMConstants.ORIG_PRODUCT_ID,					    this.origProductId);
    optionalParms.put(COMConstants.ORDER_DETAIL_ID,             this.orderDetailId);
    optionalParms.put(COMConstants.ORDER_GUID,                  this.orderGuid);
    optionalParms.put(COMConstants.ORIGIN_ID,                   this.originId);
    optionalParms.put(COMConstants.PAGE_NUMBER,                 this.pageNumber);
    optionalParms.put(COMConstants.PRICE_POINT_ID,              this.pricePointId);
    optionalParms.put(COMConstants.PRODUCT_ID,                  this.productId);
    optionalParms.put(COMConstants.RECIPIENT_CITY,              this.recipientCity);
    optionalParms.put(COMConstants.RECIPIENT_COUNTRY,           this.recipientCountry);
    optionalParms.put(COMConstants.RECIPIENT_STATE,             this.recipientState);
    optionalParms.put(COMConstants.RECIPIENT_ZIP_CODE,          this.recipientZipCode);
    optionalParms.put(COMConstants.REWARD_TYPE,                 this.rewardType);
    optionalParms.put(COMConstants.SHIP_METHOD,                 this.shipMethod);
    optionalParms.put(COMConstants.SOURCE_CODE,                 this.sourceCode);

    BreadcrumbUtil bcUtil = new BreadcrumbUtil();

    //bcUtil.getBreadcrumbXML(
                          //request,
                          //Title of breadcrumb,
                          //Name of Structs Action,
                          //Action parameter,
                          //Error display page,
                          //Flag indicating if breadcrumb should be a link, ,
                          //Optional hash of additional name/value parameters for this page
                          //order detail id
                      //)

    Document bcXML = bcUtil.getBreadcrumbXML(
                            this.request,                     //request
                            COMConstants.BCT_PRODUCT_DETAIL,  //title
                            COMConstants.BCA_PRODUCT_DETAIL,  //name
                            null,                             //action parm
                            "Error",                          //error page
                            "true",                           //link=true; no link=false
                            optionalParms,                    //optional parms 
                            this.orderDetailId);              //order detail id
    this.hashXML.put("breadcrumbs",  bcXML);

  }



/*******************************************************************************************
 * retrieveOEGlobalParms() 
 *******************************************************************************************
  * This method is used to call the
  *
  */
  private void retrieveOEGlobalParms() throws Exception
  {
    //get global params
    OEParameters oeGlobalParms = this.sUtil.getGlobalParms();

    String globalChocAvail = oeGlobalParms.getMOChocolatesAvailable();
    if (globalChocAvail != null                 &&
        !globalChocAvail.equalsIgnoreCase("")   &&
        globalChocAvail.equalsIgnoreCase(COMConstants.CONS_YES))
      this.globalChocAvailFlag = true; 
      
  }



/*******************************************************************************************
  * processMain()
  ******************************************************************************************
  *
  * @param  none
  * @return none
  * @throws none
  */
  private void processMain() throws Exception
  {

    if (this.actionType != null && this.actionType.equalsIgnoreCase(COMConstants.ACTION_UDI_MAX_DATES))
    {
      //determine if the product was a vendor or florist product
      determineVendorVsFloristProduct();

      //get the delivery dates
      processDeliveryDates(false);

      //set the xsl
      this.pageData.put("XSL",    COMConstants.XSL_UDI_UPDATE_IFRAME);
    }
    else
    {
      boolean buildXMLData = false;
      boolean buildVOData = false;

      //retrieve the global parameters
      retrieveOEGlobalParms();

      //we have to build the vo as long as the action is not max dates.
      buildXMLData = false;
      buildVOData = true;
      processRetrieveOrderData(buildXMLData, buildVOData);

      //if this class was invoked 1)due to an error 2)from Update Delivery Confirmation
      if (!this.invokedFromSearchAction)
      {
        //get the source code info
        retrieveProductBySourceCodeInfo();
  
        //get the fee info
        retrieveFeeInfo();

        //invoke the product search
        processProductSearch();
      }
      
      //check if the product was found.
      checkForValidProduct();

      //check if the product generated any errors while going thru error-checks in SearchUtil
      checkForProductErrors();

      if (this.validProduct && this.productPassedErrorChecks)
      {
        //determine if the product was a vendor or florist product
        determineVendorVsFloristProduct();

        //run furhter checks specific to this customer/recipient and their info
        performValidate();

        //get the delivery dates
        processDeliveryDates(true);

        //get the source code info
        retrieveProductBySourceCodeInfo();
        
        //retrieve free add on text
        retrieveFreeAddonText();
      }

      //retrieve the order specific data and build the xml
      //get the order specific data
      buildXMLData = true;
      buildVOData = false;
      processRetrieveOrderData(buildXMLData, buildVOData);

      //set the xsl
      this.pageData.put("XSL",    COMConstants.XSL_UPDATE_PRODUCT_DETAIL);

    }//end else

  }


/*******************************************************************************************
 * processProductSearch()
 *******************************************************************************************
  * This method is used to call the
  *
  */
  private void processProductSearch() throws Exception
  {

		Enumeration enum1 = request.getParameterNames();
    String param = "";
    String value = "";
    ArrayList promotionList = new ArrayList();
    ProductFlagsVO flagsVO = new ProductFlagsVO();

    if(this.listFlag == null || this.listFlag.length() <=0)
    {
      this.listFlag = "N";
    }


    HashMap productMap = new HashMap();


    try
    {
      HashMap args = new HashMap();
      Document xml = null;


      xml = this.sUtil.getProductByID( this.sourceCode,  this.recipientZipCode,
                  this.recipientCountry, this.productId,  this.actionType,
                  this.domesticFlag, this.recipientState,  this.pricingCode,
                  this.domesticSvcFee, this.internationalSvcFee,  this.partnerId,
                  promotionList, this.rewardType, flagsVO,  this.occasion,
                  this.companyId, this.deliveryDate, this.recipientCity,
                  this.deliveryDate, this.origProductId, this.shipMethod, productMap);

      this.pageData.put("XSL", COMConstants.XSL_UPDATE_PRODUCT_DETAIL);

      this.hashXML.put(COMConstants.HK_PRODUCT_DETAIL_XML, xml);

      Set ks = productMap.keySet();
      Iterator iter = ks.iterator();
      String key;
      //Iterate thru the hashmap returned from the business object using the keyset
      while(iter.hasNext())
      {
        key = iter.next().toString();
        this.pageData.put(key, (String)productMap.get(key));
      }
      this.pageNumber = this.pageData.get("page_number")!=null?this.pageData.get("page_number").toString():this.pageNumber;
      this.totalPages = this.pageData.get("total_pages")!=null?this.pageData.get("total_pages").toString():this.totalPages;

    }

    finally
    {
    }

  }


/*******************************************************************************************
 * processRetrieveOrderData()
 *******************************************************************************************
  * This method is used to call the
  *
  */
  private void processRetrieveOrderData(boolean buildXMLData, boolean buildVOData) throws Exception
  {
    //Instantiate CustomerDAO
    UpdateOrderDAO uoDAO = new UpdateOrderDAO(this.con);

    //hashmap that will contain the output from the stored proc
    HashMap tempOrderInfo = new HashMap();

    //Call getCustomerAcct method in the DAO
    tempOrderInfo = uoDAO.getOrderDetailsUpdate(this.orderDetailId,
                            COMConstants.CONS_ENTITY_TYPE_PRODUCT_DETAIL, false, this.csrId, this.productId );

    if (buildVOData)
      buildVOFromOrderData(tempOrderInfo);
    else if (buildXMLData)
      buildXMLFromOrderData(tempOrderInfo);

  }



/*******************************************************************************************
 * checkForValidProduct()
 *******************************************************************************************
  * This method is used to call the
  *
  */
  private void checkForValidProduct() throws Exception
  {
    Document productDetail = (Document) this.hashXML.get(COMConstants.HK_PRODUCT_DETAIL_XML);

    String sProductId = null;
    String sNovatorProductId = null;
    String floralServiceCharges = null;

    if (productDetail != null)
    {
      NodeList nl = DOMUtil.selectNodes(productDetail,"//PRODUCT");

      if (nl.getLength() > 0)
      {
        this.validProduct = true;

        //note that the CSR can potentially enter in productId or a novatorId for the product.
        //We will find the novator id regardless, but need to ensure that we have the correct
        //value for the product id.
        Element element = (Element) nl.item(0);
        sProductId = element.getAttribute("productid");
        sNovatorProductId = element.getAttribute("novatorid");
        floralServiceCharges = element.getAttribute("servicecharge");
        this.pageData.put(COMConstants.PD_MO_FLORAL_SERVICE_CHARGES, floralServiceCharges);


        //at this point, if the productId entered does not match the productId retrieved, it may be because
        // 1) productId entered was a subcode, or
        // 2) productId entered was a Novator Id. 
        //If we determine it to be a productSubCode, populate the productSubCodeId field
        if  (!this.productId.equalsIgnoreCase(sProductId)         &&
             !this.productId.equalsIgnoreCase(sNovatorProductId)  
            )
          this.productSubCodeId = this.productId; 
        
        //now, change the product id to reflect the productId as it exist in our system.         
        if (!sProductId.equalsIgnoreCase(sNovatorProductId) ||
            !sProductId.equalsIgnoreCase(this.productId))
          this.productId = sProductId;

      }
      else
        this.validProduct = false;
    }
  }



/*******************************************************************************************
 * checkForProductErrors()
 *******************************************************************************************
  * This method is used to call the
  *
  */
  private void checkForProductErrors() throws Exception
  {
    String dropShipAllowed            = (String) this.pageData.get("dropShipAllowed");
    String displayCodifiedSpecial     = (String) this.pageData.get("displayCodifiedSpecial");
    String displayNoProductPopup      = (String) this.pageData.get("displayNoProductPopup");
    String displaySpecGiftPopup       = (String) this.pageData.get("displaySpecGiftPopup");
    String displayUpsellGnaddSpecial  = (String) this.pageData.get("displayUpsellGnaddSpecial");

    if (  ( StringUtils.isNotEmpty(dropShipAllowed) &&
            dropShipAllowed.equalsIgnoreCase(COMConstants.CONS_NO)
          ) ||
          ( StringUtils.isNotEmpty(displayCodifiedSpecial) &&
            displayCodifiedSpecial.equalsIgnoreCase(COMConstants.CONS_YES)
          ) ||
          ( StringUtils.isNotEmpty(displayNoProductPopup) &&
            displayNoProductPopup.equalsIgnoreCase(COMConstants.CONS_YES)
          ) ||
          ( StringUtils.isNotEmpty(displaySpecGiftPopup) &&
            displaySpecGiftPopup.equalsIgnoreCase(COMConstants.CONS_YES)
          ) ||
          ( StringUtils.isNotEmpty(displayUpsellGnaddSpecial) &&
            displayUpsellGnaddSpecial.equalsIgnoreCase(COMConstants.CONS_YES)
          )
        )
    {
      this.productPassedErrorChecks = false;
    }
    else
    {
      this.productPassedErrorChecks = true;
    }


  }



/*******************************************************************************************
 * determineVendorVsFloristOrder()
 *******************************************************************************************
  * This method is used to call the
  *
  */
  private void determineVendorVsFloristProduct() throws Exception
  {
    Document productDetail = (Document) this.hashXML.get(COMConstants.HK_PRODUCT_DETAIL_XML);

    //for max_dates, we are not getting the product data.  However, the front end will
    //pass the vendor flag
    if (productDetail != null)
    {
      NodeList nl = DOMUtil.selectNodes(productDetail,"//PRODUCT");
      Element element = (Element) nl.item(0);
      String shipMethodCarrier = element.getAttribute("shipmethodcarrier");
      String shipMethodFlorist = element.getAttribute("shipmethodflorist");

      /*
       * There could be 3 combinations:
       *
       * shipMethodCarrier = N and shipMethodFlorist = Y - florist order
       * shipMethodCarrier = Y and shipMethodFlorist = N - vendor order.  however, it could be the
       *                                                   case that while placing order in weboe,
       *                                                   the order could have gone Florist.  So,
       *                                                   have to check for the vendorFlagOriginal
       * shipMethodCarrier = Y and shipMethodFlorist = Y - could be either vendor or flosrist order
       *                                                   should be displayed as a vendor order.
       *
       */

      if (shipMethodCarrier != null && shipMethodCarrier.equalsIgnoreCase(COMConstants.CONS_NO)    &&
          shipMethodFlorist != null && shipMethodFlorist.equalsIgnoreCase(COMConstants.CONS_YES))
      {
        this.displayFormat = COMConstants.CONS_FLORIST;
        this.vendorFlagNewProduct = COMConstants.CONS_NO.toUpperCase();
        this.shipMethodCarrier = COMConstants.CONS_NO.toUpperCase();
      }
      else
      {
        this.displayFormat = COMConstants.CONS_VENDOR;
        if (this.origProductId.equalsIgnoreCase(this.productId)     &&
            this.vendorFlagOriginal.equalsIgnoreCase(COMConstants.CONS_NO)  &&
            shipMethodFlorist != null && shipMethodFlorist.equalsIgnoreCase(COMConstants.CONS_NO))
        {
          this.vendorFlagNewProduct = COMConstants.CONS_NO.toUpperCase();
        }
        else
        {
          this.vendorFlagNewProduct = COMConstants.CONS_YES.toUpperCase();
        }
      }
      this.shipMethodCarrier = shipMethodCarrier!=null?shipMethodCarrier.toUpperCase():null;
      this.shipMethodFlorist = shipMethodFlorist!=null?shipMethodFlorist.toUpperCase():null;
    }
    else
    {
      if (this.shipMethodCarrier.equalsIgnoreCase(COMConstants.CONS_NO)   &&
          this.shipMethodFlorist.equalsIgnoreCase(COMConstants.CONS_YES))
      {
        this.displayFormat = COMConstants.CONS_FLORIST;
        this.vendorFlagNewProduct = COMConstants.CONS_NO.toUpperCase();
      }
      else
      {
        this.displayFormat = COMConstants.CONS_VENDOR;
        if (this.origProductId.equalsIgnoreCase(this.productId)     &&
            this.vendorFlagOriginal.equalsIgnoreCase(COMConstants.CONS_NO)  &&
            this.shipMethodFlorist.equalsIgnoreCase(COMConstants.CONS_NO))
        {
          this.vendorFlagNewProduct = COMConstants.CONS_NO.toUpperCase();
        }
        else
        {
          this.vendorFlagNewProduct = COMConstants.CONS_YES.toUpperCase();
        }
      }
    }

    this.pageData.put("display_format", this.displayFormat);

  }



/*******************************************************************************************
 * processDeliveryDates()
 *******************************************************************************************
  * This method is used to call the
  *
  */
  private void processDeliveryDates(boolean minDateFlag) throws Exception
  {

    Document deliveryDatesXML = DOMUtil.getDocument();

    DeliveryDateParameters ddParms = new DeliveryDateParameters();
    ddParms.setMinDaysOut( minDateFlag );
    ddParms.setRecipientState( this.recipientState );
    ddParms.setRecipientZipCode( this.recipientZipCode );
    ddParms.setRecipientCountry( this.recipientCountry );
    ddParms.setProductId( this.productId );
    ddParms.setOccasionId( this.occasion );
    ddParms.setOrderOrigin( this.originId );
    ddParms.setIsFlorist( this.displayFormat.equalsIgnoreCase(COMConstants.CONS_FLORIST)?true:false );
    ddParms.setProductSubCodeId(this.productSubCodeId);
    ddParms.setSourceCode(this.sourceCode);

    SimpleDateFormat dateFormat = new SimpleDateFormat( DATE_FORMAT );
    Calendar date = Calendar.getInstance();
    if ( StringUtils.isNotEmpty(this.deliveryDate) ) {
      date.setTime( dateFormat.parse( this.deliveryDate ) );
      ddParms.setRequestedDeliveryDate( date );
    }
    if ( StringUtils.isNotEmpty(this.deliveryDateRange) ) {
      date.setTime( dateFormat.parse( this.deliveryDateRange ) );
      ddParms.setRequestedDeliveryDateRangeEnd( date );
    }

    COMDeliveryDateUtil dateUtil = new COMDeliveryDateUtil();
    dateUtil.setParameters( ddParms );

    try
    {
      // do not show date ranges for sympathy/funeral orders
      DeliveryDateConfiguration deliveryDateConfig = new DeliveryDateConfiguration();

      deliveryDateConfig.setDisplayDateFormat( DATE_FORMAT );
      deliveryDateConfig.setSubmitDateFormat( DATE_FORMAT );
      deliveryDateConfig.setShowDateRanges(this.occasion != null && this.occasion.equalsIgnoreCase(OCCASION_SYMPATHY_FUNERAL)?false:true);
      deliveryDateConfig.setIncludeRequestedDeliveryDate( true );
      dateUtil.setConfiguration( deliveryDateConfig );

      deliveryDatesXML = dateUtil.getXmlDeliveryDates();
      this.wasMaxRetrieved = dateUtil.wasMaxRetrieved();

      this.hashXML.put(COMConstants.HK_DELIVERY_DATES_XML, deliveryDatesXML);

//Issue 2134 - if the original product on the order had both vendor and floral delivery methods available
//and before going into Mod Order, if the product was maintained so that floral option is no longer
//available, we still want to display the florist delivery method and a drop down for the dates available.
//
//Thus: if orig and new product are the same, and
//      originally, we made that a florist order, and
//      florist delivery is no longer available,
//       1) display the florist method and dates
//       2) update the floristId column



    }
    catch (Exception e)
    {
      logger.error(e);
      throw(e);
    }

  }


  
  /*******************************************************************************************
   * retrieveFreeAddonText() 
   *******************************************************************************************
   * This method is used to retrieve free add on text
   *
   */
   private void retrieveFreeAddonText() throws Exception
   {
        UpdateOrderDAO uoDAO = new UpdateOrderDAO(this.con);

        //HashMap that will contain the output from the stored proce
        CachedResultSet rs;

        rs = (CachedResultSet)uoDAO.getCompAddOnInfo(this.sourceCode, this.productId.toUpperCase());

        while(rs.next())
        {
          if (rs.getObject("description")!= null && rs.getObject("description").toString().equals("Complimentary"))
          {
              if(rs.getObject("addon_text") != null) ;
              {
                pageData.put("compAddonText", rs.getObject("addon_text").toString());  
              }
          } 
        }
   }



/*******************************************************************************************
 * retrieveProductBySourceCodeInfo() (COM_SOURCE_CODE_RECORD_LOOKUP)
 *******************************************************************************************
  * This method is used to call the
  *
  */
  private void retrieveProductBySourceCodeInfo() throws Exception
  {
    BasePageBuilder bpb = new BasePageBuilder();

    //Call getCSRViewed method in the DAO
    Document sourceCodeInfo = (Document) bpb.retrieveProductBySourceCodeInfo(this.con, this.sourceCode);

    //create an array.
    ArrayList aList = new ArrayList();

    //Initialize the array with the top and bottom nodes of the XML, and the node for which
    //the value must be retrieved. In this instance, we will retireve the value of
    //shipping code
    aList.clear();
    aList.add(0,"PRODUCTS");
    aList.add(1,"PRODUCT");
    aList.add(2,"shipping_code");
    String shippingCode = DOMUtil.getNodeValue(sourceCodeInfo, aList);
    this.snhId = shippingCode;

    //Initialize the array with the top and bottom nodes of the XML, and the node for which
    //the value must be retrieved. In this instance, we will retireve the value of
    //pricing code
    aList.clear();
    aList.add(0,"PRODUCTS");
    aList.add(1,"PRODUCT");
    aList.add(2,"pricing_code");
    String priceCode = DOMUtil.getNodeValue(sourceCodeInfo, aList);
    this.pricingCode = priceCode;

    //Initialize the array with the top and bottom nodes of the XML, and the node for which
    //the value must be retrieved. In this instance, we will retireve the value of
    //partner id
    aList.clear();
    aList.add(0,"PRODUCTS");
    aList.add(1,"PRODUCT");
    aList.add(2,"partner_id");
    String ptnrId = DOMUtil.getNodeValue(sourceCodeInfo, aList);
    this.partnerId = ptnrId;
    
    // Pass the message to front end if the source code does not allow product due to source
    // product attribute restriction. Refer to table ftd_apps.product_attr_restr_source_excl.
    String productAttrExcl = bpb.loadProductAttributeExclusions(this.productId, this.sourceCode, this.con);
    logger.debug("product attribute error:" + productAttrExcl);
    if(productAttrExcl != null && productAttrExcl.length() > 0) {  
        pageData.put("product_attribute_exclusions", productAttrExcl);
    }

    this.hashXML.put(COMConstants.HK_SOURCE_CODE_XML,  sourceCodeInfo);

  }


/*******************************************************************************************
 * retrieveFeeInfo() (COM_SNH_BY_ID)
 *******************************************************************************************
  * This method is used to call the
  *
  */
  private void retrieveFeeInfo() throws Exception
  {
    BasePageBuilder bpb = new BasePageBuilder();

    //Call getCSRViewed method in the DAO
    Document feeInfo = (Document) bpb.retrieveFeeInfo(this.con, this.snhId, this.deliveryDate);

    //create an array.
    ArrayList aList = new ArrayList();

    //Initialize the array with the top and bottom nodes of the XML, and the node for which
    //the value must be retrieved. In this instance, we will retireve the value of
    //customer id
    aList.clear();
    aList.add(0,"FEES");
    aList.add(1,"FEE");
    aList.add(2,"first_order_domestic");
    String domesticFee = DOMUtil.getNodeValue(feeInfo, aList);
    this.domesticSvcFee = domesticFee;

    //Initialize the array with the top and bottom nodes of the XML, and the node for which
    //the value must be retrieved. In this instance, we will retireve the value of
    //customer id
    aList.clear();
    aList.add(0,"FEES");
    aList.add(1,"FEE");
    aList.add(2,"first_order_international");
    String internationalFee = DOMUtil.getNodeValue(feeInfo, aList);
    this.internationalSvcFee = internationalFee;

  }



/*******************************************************************************************
 * buildVOFromOrderData()
 *******************************************************************************************/
  private void buildVOFromOrderData(HashMap tempOrderInfo)
        throws Exception
  {
    Set ks1 = tempOrderInfo.keySet();
    Iterator iter = ks1.iterator();
    String key = null;

    //Create a CachedResultSet object using the cursor name that was passed.
    CachedResultSet rs = null;

    //Iterate thru the keyset
    while(iter.hasNext())
    {
      //get the key
      key = iter.next().toString();

//*****************************************************************************************
// * OUT_UPD_ADDON_CUR
// *****************************************************************************************/
      if (key.equalsIgnoreCase("OUT_UPD_ADDON_CUR"))
      {}
//*****************************************************************************************
// * OUT_ORIG_PRODUCT
// *****************************************************************************************/
      else if (key.equalsIgnoreCase("OUT_ORIG_PRODUCT"))
      {
        rs = (CachedResultSet) tempOrderInfo.get(key);
        rs.next();
        this.vendorFlagOriginal = (String)rs.getObject("vendor_flag"); 
      }
//*****************************************************************************************
// * OUT_UPD_ORDER_CUR
// *****************************************************************************************/
      else if (key.equalsIgnoreCase("OUT_UPD_ORDER_CUR"))
      {
      }
//*****************************************************************************************
// * OUT_SHIP_COST
// *****************************************************************************************/
      else if (key.equalsIgnoreCase("OUT_SHIP_COST"))
      {}
//*****************************************************************************************
// * OUT_ORIG_ADD_ONS
// *****************************************************************************************/
      else if (key.equalsIgnoreCase("OUT_ORIG_ADD_ONS"))
      {}
      

    } //end-while(iter.hasNext())

  }


/*******************************************************************************************
 * buildXMLFromOrderData()
 *******************************************************************************************/
  private void buildXMLFromOrderData(HashMap tempOrderInfo)
        throws Exception
  {
    //Create an Document and call a method that will transform the CachedResultSet from the cursor:
    //OUT_PHONE_CUR into an Document.  Store this XML object in hashXML HashMap
    Document origProductXML =
        buildXML(tempOrderInfo, "OUT_ORIG_PRODUCT",     "PD_ORIG_PRODUCTS",   "PD_ORIG_PRODUCT",    "element");
    this.hashXML.put(COMConstants.HK_ORIG_PRODUCT_XML,  origProductXML);

    //Create an Document and call a method that will transform the CachedResultSet from the cursor:
    //OUT_EMAIL_CUR into an Document.  Store this XML object in hashXML HashMap
    Document origAddOnXML =
        buildXML(tempOrderInfo, "OUT_ORIG_ADD_ONS",     "PD_ORIG_ADD_ONS",    "PD_ORIG_ADD_ON",     "element");
    this.hashXML.put(COMConstants.HK_ORIG_ADDON_XML,    origAddOnXML);

    //Create an Document and call a method that will transform the CachedResultSet from the cursor:
    //OUT_MEMBERSHIP_CUR into an Document.  Store this XML object in hashXML HashMap
    Document orderProductXML =
        buildXML(tempOrderInfo, "OUT_UPD_ORDER_CUR",    "PD_ORDER_PRODUCTS",   "PD_ORDER_PRODUCT",  "element");
    this.hashXML.put(COMConstants.HK_ORDER_PRODUCT_XML, orderProductXML);

    //Create an Document and call a method that will transform the CachedResultSet from the cursor:
    //OUT_MEMBERSHIP_CUR into an Document.  Store this XML object in hashXML HashMap
    Document orderAddOnXML =
        buildXML(tempOrderInfo, "OUT_UPD_ADDON_CUR",    "PD_ORDER_ADDONS",    "PD_ORDER_ADDON",     "element");
    this.hashXML.put(COMConstants.HK_ORDER_ADDON_XML,   orderAddOnXML);

    //Create an Document and call a method that will transform the CachedResultSet from the cursor:
    //OUT_MEMBERSHIP_CUR into an Document.  Store this XML object in hashXML HashMap
    Document shipCostXML =
        buildXML(tempOrderInfo, "OUT_SHIP_COST",        "PD_SHIP_COSTS",     "PD_SHIP_COST",        "element");
    this.hashXML.put(COMConstants.HK_SHIP_COST_XML,     shipCostXML);

    //Create an Document and call a method that will transform the CachedResultSet from the cursor:
    //OUT_ORIG_TAXES_CUR into an Document.  Store this XML object in hashXML HashMap
    Document orderTaxXML =
        buildXML(tempOrderInfo, "OUT_ORIG_TAXES_CUR", "PD_ORDER_TAXES", "PD_ORDER_TAX",   "element");
    this.hashXML.put(COMConstants.HK_ORDER_TAX_XML, orderTaxXML);


  }



/*******************************************************************************************
 * buildXML()
 *******************************************************************************************/
  private Document buildXML(HashMap outMap, String cursorName, String topName, String bottomName, String type)
        throws Exception
  {

    //Create a CachedResultSet object using the cursor name that was passed.
    CachedResultSet rs = (CachedResultSet) outMap.get(cursorName);

    Document doc =  null;

    //Create an XMLFormat, and initialize the top and bottom node.  Note that since this method
    //can be/is called by various other methods, the top and botton names MUST be passed to it.
    XMLFormat xmlFormat = new XMLFormat();
    xmlFormat.setAttribute("type", type);
    xmlFormat.setAttribute("top", topName);
    xmlFormat.setAttribute("bottom", bottomName );

    //call the DOMUtil's converToXMLDOM method
    doc = (Document) DOMUtil.convertToXMLDOM(rs, xmlFormat);

    //return the xml document.
    return doc;

  }


 /*******************************************************************************************
  * setModifyFlag()
  *******************************************************************************************
  *
  * A change was detected.  Thus, update the MODIFY_ORDER_UPDATED flag in the request parameters
  *
  * @return nothing
  * @throws java.lang.Exception
  */
  private void setModifyFlag()
  {
    HashMap parameters = (HashMap) this.request.getAttribute    ( COMConstants.CONS_APP_PARAMETERS);
    parameters.put(COMConstants.MODIFY_ORDER_UPDATED,             COMConstants.CONS_YES);
    this.request.setAttribute(COMConstants.CONS_APP_PARAMETERS,   parameters);
  }


/*******************************************************************************************
 * performValidate()
 *******************************************************************************************/
  private void performValidate()  throws Exception
  {
		boolean validationPassed = true;

    HashMap results = (HashMap) validate();

    /*******************************************************************************************
    * WM#10 - product agricultural restriction found.
    ******************************************************************************************/
    if ( results.containsKey(ValidationRules.PRODUCT_STATE_AGRICULTURAL_RESTRICATIONS_FOR_ENTIRE_WEEK) )
    {
      generateErrorNode(COMConstants.PRODUCT_STATE_AGRICULTURAL_RESTRICATIONS, null,
                        COMConstants.CONS_ERROR_POPUP_INDICATOR_OK, LOAD_PRODUCT_CATEGORY, null);
      validationPassed = false;
      this.pageData.put(COMConstants.PD_MO_AGRICULTURE_RESTRICTION_FOUND, COMConstants.CONS_YES.toUpperCase());
    }
    else if ( results.containsKey(ValidationRules.PRODUCT_STATE_AGRICULTURAL_RESTRICATIONS_FOR_DELIVERY_DAY_SELECTED) )
    {
      generateErrorNode(COMConstants.PRODUCT_STATE_AGRICULTURAL_RESTRICATIONS, null,
                        COMConstants.CONS_ERROR_POPUP_INDICATOR_OK, null, null);
      validationPassed = false;
      this.pageData.put(COMConstants.PD_MO_AGRICULTURE_RESTRICTION_FOUND, COMConstants.CONS_YES.toUpperCase());
    }
    else
    {
      this.pageData.put(COMConstants.PD_MO_AGRICULTURE_RESTRICTION_FOUND, COMConstants.CONS_NO.toUpperCase());
    }

    if (!validationPassed)
      this.pageData.put("XSL", COMConstants.XSL_LOAD_PRODUCT_DETAIL_ACTION);



  }


/*******************************************************************************************
 * validate()
 *******************************************************************************************
 *
 * Performs florist and vendor delivery date validation by delegating validation to
 * the corresponding validation handler.
 *
 * @return true if the selected delivery date is valid, otherwise false
 * @throws java.lang.Exception
 *******************************************************************************************/
  private HashMap validate() throws Exception
  {

    Map results = new HashMap();
    boolean toReturn = true;

    ValidationConfiguration validationConfiguration = new ValidationConfiguration();
    validationConfiguration.setValidationLevel( ValidationLevels.WARNING );

    DeliveryInfoVO divo = new DeliveryInfoVO();
    //divo.setColor1();
    //divo.setColor2();
    divo.setCompanyId(this.companyId);
    divo.setCustomerId(this.customerId);
    divo.setDeliveryDate(this.cDeliveryDate);
    divo.setDeliveryDateRangeEnd(this.cDeliveryDateRange);
    divo.setMasterOrderNumber(this.masterOrderNumber);
    divo.setOccasion(this.occasion);
    divo.setOccasionCategoryIndex(this.categoryIndex);
    divo.setOccasionDescription(this.occasionText);
    divo.setOrderDetailId(this.orderDetailId);
    divo.setOrderGuid(this.orderGuid);
    divo.setOriginId(this.originId);
    divo.setProductId(this.productId);
    divo.setRecipientCity(this.recipientCity);
    divo.setRecipientCountry(this.recipientCountry);
    divo.setRecipientState(this.recipientState);
    divo.setRecipientZipCode(this.recipientZipCode);

    divo.setUpdatedBy(this.csrId);
    divo.setVendorFlag(this.vendorFlagNewProduct);

    Validator validator = DeliveryDateValidatorFactory.getInstance().getValidator(validationConfiguration, divo, this.con);

    if ( !validator.validate() )
    {
      results = validator.getValidationResults();
    }

    return (HashMap)results;

  }



 /*******************************************************************************************
  * generateErrorNode()
  *******************************************************************************************
  *
  * Adds an error message to the error message XML doc
  *
  * @params messageText - Message text
  * @params fieldInError - Name of erred field (optional).
  * @params popupType - Type of popup window (optional).
  * @params popupConfirmation - Return page if popup confirmation selected (optional).
  * @params popupCancel - Return page if confirmation is cancelled (optional).
  */
  private void generateErrorNode(String messageText, String fieldInError,
                                   String popupType, String popupConfirmation, String popupCancel)
        throws Exception
  {

    String l_popupType = popupType;
    if (fieldInError == null && popupType == null) {
      l_popupType = POPUP_INDICATOR_OK;
    }

    // Generate top node
    Document errorMsgsXML = (Document) DOMUtil.getDefaultDocument();
    Element errMsgsTopElement = errorMsgsXML.createElement("ERROR_MESSAGES");
    errorMsgsXML.appendChild(errMsgsTopElement);

    if (messageText != null)
    {
      Element errorMsgElement = errorMsgsXML.createElement("ERROR_MESSAGE");
      errMsgsTopElement.appendChild(errorMsgElement);

      Element errMsgNode = errorMsgsXML.createElement("TEXT");
      errorMsgElement.appendChild(errMsgNode);
      errMsgNode.appendChild(errorMsgsXML.createTextNode(messageText));

      Element errFieldNode = errorMsgsXML.createElement("FIELDNAME");
      errorMsgElement.appendChild(errFieldNode);
      if (fieldInError != null)
        errFieldNode.appendChild(errorMsgsXML.createTextNode(fieldInError));

      Element popupIndicatorNode = errorMsgsXML.createElement("POPUP_INDICATOR");
      errorMsgElement.appendChild(popupIndicatorNode);
      if (l_popupType != null)
        popupIndicatorNode.appendChild(errorMsgsXML.createTextNode(l_popupType));

      Element popupGotoNode = errorMsgsXML.createElement("POPUP_GOTO_PAGE");
      errorMsgElement.appendChild(popupGotoNode);
      if (popupConfirmation != null)
        popupGotoNode.appendChild(errorMsgsXML.createTextNode(popupConfirmation));

      Element popupCancelNode = errorMsgsXML.createElement("POPUP_CANCEL_PAGE");
      errorMsgElement.appendChild(popupCancelNode);
      if (popupCancel != null)
        popupCancelNode.appendChild(errorMsgsXML.createTextNode(popupCancel));
    }

    this.hashXML.put(COMConstants.HK_ERROR_MESSAGES,  errorMsgsXML);

  }


}

