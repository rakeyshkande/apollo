package com.ftd.mo.util;

import com.ftd.customerordermanagement.vo.AddOnVO;
import com.ftd.customerordermanagement.vo.CommentsVO;
import com.ftd.customerordermanagement.vo.CreditCardVO;
import com.ftd.customerordermanagement.vo.CustomerPhoneVO;
import com.ftd.customerordermanagement.vo.EmailVO;
import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.dao.CustomerDAO;
import com.ftd.customerordermanagement.vo.CallLogVO;
import com.ftd.customerordermanagement.vo.CustomerVO;
import com.ftd.customerordermanagement.vo.OrderBillVO;
import com.ftd.customerordermanagement.vo.PaymentVO;
import com.ftd.mo.dao.UpdateOrderDAO;
import com.ftd.mo.vo.DeliveryInfoVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import java.sql.Connection;
import org.w3c.dom.Document;
import java.io.PrintWriter;
import java.io.StringWriter;

import com.ftd.osp.utilities.order.OrderXAO;
import com.ftd.osp.utilities.order.vo.*;

import java.util.*;
import java.text.*;
import java.math.*;

/**
 * DomainObjectConverter
 * 
 * This object uses data from the clean schema to create a scrub order object.
 * This class is modeled very closely to the WebOE DomainObjectConverter.
 */
public class DomainObjectConverter
{
    private Connection connection;

    //date formats used for creating order
    private static SimpleDateFormat orderDateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy");
    private static SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");  
    
    //objects containing clean order information
    private DeliveryInfoVO originalDeliveryInfoVO = new DeliveryInfoVO();
    private DeliveryInfoVO tempDeliveryInfoVO = new DeliveryInfoVO();      
    private OrderBillVO originalOrderBillVO = new OrderBillVO();
    private OrderBillVO tempOrderBillVO = new OrderBillVO();  
    private HashMap originalOrderAddOnVO = new HashMap(); //key = addon-code (eg. RC73)
    private HashMap tempOrderAddOnVO = new HashMap(); //key = addon-code (eg. RC73)
    private List tempCommentList = new ArrayList();
    private List tempCoBrandList = new ArrayList();
    private List tempPaymentList = new ArrayList();
    private Map tempCreditCardMap = new HashMap();    
    
    //DAOs and BOs as member variables for easier testing
    private UpdateOrderDAO dao;
    private CustomerDAO customerDAO;
    
    /**
     * 
     * @param conn Database Connection
     */
    public DomainObjectConverter(Connection conn) {
    	this.connection = conn;
    }
    
    /**
     * Constructor
     * 
     * @param conn Database connection
     * @param orderData Map order clean order data
     */
    public DomainObjectConverter(Connection conn, Map orderData)
    {
        //populate class variables
        this.connection = conn;  
        setOrderData(orderData);

    }
    
    public void setOrderData(Map orderData) {
        
        this.originalDeliveryInfoVO = (DeliveryInfoVO)orderData.get("originalDeliveryInfoVO");
        this.tempDeliveryInfoVO = (DeliveryInfoVO)orderData.get("tempDeliveryInfoVO");
        this.originalOrderBillVO = (OrderBillVO)orderData.get("originalOrderBillVO");
        this.tempOrderBillVO = (OrderBillVO)orderData.get("tempOrderBillVO");
        this.originalOrderAddOnVO = (HashMap)orderData.get("originalOrderAddOnVO");
        this.tempOrderAddOnVO = (HashMap)orderData.get("tempOrderAddOnVO");
        this.tempCommentList = (List)orderData.get("tempCommentList");
        this.tempCoBrandList = (List)orderData.get("tempCoBrandList");
        this.tempPaymentList = (List)orderData.get("tempPaymentList");
        this.tempCreditCardMap = (Map)orderData.get("tempCreditCardMap");
		
	}

	/*
     * convertOrder
     * 
     * Create an scrub order object based from data in the clean schema 
     * 
     */
     public com.ftd.osp.utilities.order.vo.OrderVO convertOrder(String newExternalOrderNumber,
                                String newMasterOrderNumber,
                                String newGuid,
                                String csrId) throws Exception
     { 
        //get flag to determine if JMS should be sent out.  (used while testing)
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();         
        String modifyOrderDNIS = configUtil.getProperty(COMConstants.PROPERTY_FILE,"MODIFY_ORDER_DNIS");  
        String modifyOrderOrigin = configUtil.getProperty(COMConstants.PROPERTY_FILE,"MODIFY_ORDER_ORIGIN");   
        SimpleDateFormat dtFormat = new SimpleDateFormat("MM/dd/yyyy");
      
        com.ftd.osp.utilities.order.vo.OrderVO scrubOrderVO = new com.ftd.osp.utilities.order.vo.OrderVO();
      
        if (dao==null) {
        	dao = new UpdateOrderDAO(connection);
        }
      
      	// Order Header
        scrubOrderVO.setDnisCode(modifyOrderDNIS);//8888
        scrubOrderVO.setMasterOrderNumber(newMasterOrderNumber);
        scrubOrderVO.setSourceCode(tempDeliveryInfoVO.getSourceCode());
        scrubOrderVO.setOrderOrigin
        ( 
          (originalDeliveryInfoVO != null && originalDeliveryInfoVO.getOriginId() != null ) 
             ? originalDeliveryInfoVO.getOriginId() : modifyOrderOrigin        
        );//FTDP
        scrubOrderVO.setCsrId(csrId);
        scrubOrderVO.setGUID(newGuid);
        scrubOrderVO.setCallTime(null);
        scrubOrderVO.setOrderDate(orderDateFormat.format(new java.util.Date()));
        scrubOrderVO.setProductsTotal("1");//total lines, always 1 in modify order   

        //Buyer information needs to be retrieved from the original order in clean.
        //Buyer information is not updated in modify order
        if (customerDAO == null) {
        	customerDAO = new CustomerDAO(connection);                
        }
        List customerList = customerDAO.getCustomerById(originalDeliveryInfoVO.getCustomerId());
        CustomerVO customerVO = null;
        if(customerList != null && customerList.size() >0)
        {
            //retrieving by the primary key, so only 1 customer should be returned
            customerVO = (CustomerVO)customerList.get(0);
        }       

        //Get buyer's phone and email information from database.
        //This is obtrained from the original order clean tables
        List phoneList = customerDAO.getCustomerPhones(Long.parseLong(originalDeliveryInfoVO.getCustomerId()));
        EmailVO emailVO = customerDAO.getCustomerEmailInfo(Long.parseLong(originalDeliveryInfoVO.getCustomerId()),tempDeliveryInfoVO.getCompanyId());
        
        //create buyer object
        BuyerVO scrubBuyer = new BuyerVO();
        scrubBuyer.setAutoHold(null);
        scrubBuyer.setBestCustomer(null);
        scrubBuyer.setFirstName(originalDeliveryInfoVO.getCustomerFirstName());
        scrubBuyer.setLastName(originalDeliveryInfoVO.getCustomerLastName());
        scrubBuyer.setCustomerId(originalDeliveryInfoVO.getCustomerId());

        // Buyer Phones
        Iterator phoneIter = phoneList.iterator();
        List scrubBuyerPhones = new ArrayList();
        //Loop through phones looking for day and evening numbers
        //Clean and Scrub do not have the same phone types
        while(phoneIter.hasNext())
        {
            CustomerPhoneVO phoneVO = (CustomerPhoneVO)phoneIter.next();
            if(phoneVO.getPhoneType().equalsIgnoreCase("Evening"))
            {
                //Get customer phone record for original order where phone type = �Evening�
                BuyerPhonesVO scrubBuyerPhone = new BuyerPhonesVO();
                scrubBuyerPhone.setExtension(phoneVO.getExtension());
                scrubBuyerPhone.setPhoneNumber(phoneVO.getPhoneNumber());
                scrubBuyerPhone.setPhoneType("HOME");   
                scrubBuyerPhone.setPhoneNumberType(phoneVO.getPhoneNumberType());
                scrubBuyerPhone.setSmsOptIn(phoneVO.getSmsOptIn());
                scrubBuyerPhones.add(scrubBuyerPhone);
            }

            if(phoneVO.getPhoneType().equalsIgnoreCase("Day"))
            {
                //Get customer phone record for original order where phone type = �Day�
                BuyerPhonesVO scrubBuyerPhone = new BuyerPhonesVO();
                scrubBuyerPhone.setExtension(phoneVO.getExtension());
                scrubBuyerPhone.setPhoneNumber(phoneVO.getPhoneNumber());
                scrubBuyerPhone.setPhoneType("WORK");   
                scrubBuyerPhone.setPhoneNumberType(phoneVO.getPhoneNumberType());
                scrubBuyerPhone.setSmsOptIn(phoneVO.getSmsOptIn());
                scrubBuyerPhones.add(scrubBuyerPhone);
            }

        }
        scrubBuyer.setBuyerPhones(scrubBuyerPhones);

        //Buyer Email
        List scrubBuyerEmails = new ArrayList();
        //check if there is an email on the order
        if(emailVO != null)
        {
            BuyerEmailsVO scrubEmailVO = new BuyerEmailsVO();
            scrubEmailVO.setEmail(emailVO.getEmailAddress());
            scrubEmailVO.setPrimary("Y");
            //The subscribe status in clean is the same as the newsletter flag in scrub        
            if(emailVO.getSubscribeStatus() !=null && emailVO.getSubscribeStatus().equalsIgnoreCase("Subscribe"))
            {
                scrubEmailVO.setNewsletter("Y");                
            }
            else
            {
                scrubEmailVO.setNewsletter("N");                
            }
            scrubBuyerEmails.add(scrubEmailVO);            
        }
        scrubBuyer.setBuyerEmails(scrubBuyerEmails);

        // Buyer Address
        BuyerAddressesVO scrubBuyerAddress = new BuyerAddressesVO();
        scrubBuyerAddress.setAddressEtc(customerVO.getBusinessName());
        scrubBuyerAddress.setAddressLine1(customerVO.getAddress1());
        scrubBuyerAddress.setAddressLine2(customerVO.getAddress2());
        scrubBuyerAddress.setAddressType(customerVO.getAddressType());
        scrubBuyerAddress.setCity(customerVO.getCity());
        scrubBuyerAddress.setCountry(customerVO.getCountry());
        scrubBuyerAddress.setCounty(customerVO.getCounty());
        scrubBuyerAddress.setPostalCode(customerVO.getZipCode());
        scrubBuyerAddress.setStateProv(customerVO.getState());
        List scrubBuyerAddressList = new ArrayList();
        scrubBuyerAddressList.add(scrubBuyerAddress);
        scrubBuyer.setBuyerAddresses(scrubBuyerAddressList);
        List buyerList = new ArrayList();
        buyerList.add(scrubBuyer);        
        scrubOrderVO.setBuyer(buyerList);

        // Memberships
        MembershipsVO scrubMembershipVO = new MembershipsVO();
        scrubMembershipVO.setFirstName(tempDeliveryInfoVO.getMembershipFirstName());
        scrubMembershipVO.setLastName(tempDeliveryInfoVO.getMembershipLastName());
        scrubMembershipVO.setMembershipIdNumber(tempDeliveryInfoVO.getMembershipNumber());
        scrubMembershipVO.setMembershipType(dao.getPartnerName(scrubOrderVO.getSourceCode())); 
        List scrubMembershipList = new ArrayList();
        scrubMembershipList.add(scrubMembershipVO);
        scrubOrderVO.setMemberships(scrubMembershipList);
            
        // Contact info
        OrderContactInfoVO scrubContactVO = new OrderContactInfoVO();
        scrubContactVO.setOrderGuid(newGuid);
        scrubContactVO.setEmail(tempDeliveryInfoVO.getAltContactEmail());    
        scrubContactVO.setExt(tempDeliveryInfoVO.getAltContactExtension());
        scrubContactVO.setFirstName(tempDeliveryInfoVO.getAltContactFirstName());
        scrubContactVO.setLastName(tempDeliveryInfoVO.getAltContactLastName());
        scrubContactVO.setPhone(tempDeliveryInfoVO.getAltContactPhoneNumber());
        List scrubContactList = new ArrayList();
        scrubContactList.add(scrubContactVO);
        scrubOrderVO.setOrderContactInfo(scrubContactList);
        
        //Order Exceptions
        scrubOrderVO.setOrderExtensions(tempDeliveryInfoVO.getOrderExtensions());

        // Items    
        OrderDetailsVO scrubOrderDetailVO = new OrderDetailsVO();
        scrubOrderDetailVO.setLineNumber("1");
        scrubOrderDetailVO.setGuid(newGuid);
        scrubOrderDetailVO.setCardMessage(tempDeliveryInfoVO.getCardMessage());
        scrubOrderDetailVO.setCardSignature(tempDeliveryInfoVO.getCardSignature());
        scrubOrderDetailVO.setColorFirstChoice(tempDeliveryInfoVO.getColor1());
        scrubOrderDetailVO.setColorSecondChoice(tempDeliveryInfoVO.getColor2());
        
        //format delivery date and delivery end date
        if(tempDeliveryInfoVO.getDeliveryDate() != null)
        {
            String deliveryDateStr = sdf.format(tempDeliveryInfoVO.getDeliveryDate().getTime());        
            scrubOrderDetailVO.setDeliveryDate(deliveryDateStr);            
        }
        if(tempDeliveryInfoVO.getDeliveryDateRangeEnd() != null)
        {
            String deliveryDateStr = sdf.format(tempDeliveryInfoVO.getDeliveryDateRangeEnd().getTime());        
            scrubOrderDetailVO.setDeliveryDateRangeEnd(deliveryDateStr);            
        }

        //set order detail informaiton
        scrubOrderDetailVO.setExternalOrderNumber(newExternalOrderNumber);
        scrubOrderDetailVO.setFloristNumber(null);
        scrubOrderDetailVO.setLastMinuteGiftEmail(null);
        scrubOrderDetailVO.setLastMinuteGiftSignature(null);
        scrubOrderDetailVO.setOccassionId(tempDeliveryInfoVO.getOccasion());
        scrubOrderDetailVO.setProductId(tempDeliveryInfoVO.getProductId());
        scrubOrderDetailVO.setSourceCode(tempDeliveryInfoVO.getSourceCode());
        scrubOrderDetailVO.setItemOfTheWeekFlag("N");
        scrubOrderDetailVO.setProductSubCodeId(tempDeliveryInfoVO.getSubcode());
        scrubOrderDetailVO.setProductsAmount(Double.toString(tempOrderBillVO.getProductAmount()));
        scrubOrderDetailVO.setQuantity("1");
        scrubOrderDetailVO.setShipMethod(tempDeliveryInfoVO.getShipMethod());
        scrubOrderDetailVO.setSubstituteAcknowledgement(tempDeliveryInfoVO.getSubstitutionIndicator());
        scrubOrderDetailVO.setSenderInfoRelease(tempDeliveryInfoVO.getReleaseInfoIndicator());
        scrubOrderDetailVO.setPersonalGreetingId(tempDeliveryInfoVO.getPersonalGreetingId());
        scrubOrderDetailVO.setNoTaxFlag(tempDeliveryInfoVO.getNoTaxFlag());
        scrubOrderDetailVO.setOrigSourceCode(tempDeliveryInfoVO.getSourceCode());
        scrubOrderDetailVO.setOrigProductId(tempDeliveryInfoVO.getProductId());
        scrubOrderDetailVO.setOrigShipMethod(tempDeliveryInfoVO.getShipMethod());
        scrubOrderDetailVO.setOrigDeliveryDate(dtFormat.format((tempDeliveryInfoVO.getDeliveryDate()).getTime()));
        scrubOrderDetailVO.setOriginalOrderHasSDU(tempDeliveryInfoVO.getOriginalOrderHasSDU());
        scrubOrderVO.setAllowPCCheckUpdateOrder(false);
        if("Y".equalsIgnoreCase(tempDeliveryInfoVO.getPcFlag())){
	        scrubOrderDetailVO.setPcFlag(tempDeliveryInfoVO.getPcFlag());
	        scrubOrderDetailVO.setPcGroupId(tempDeliveryInfoVO.getPcGroupId());
	        scrubOrderDetailVO.setPcMembershipId(tempDeliveryInfoVO.getPcMembershipId());
        }else{
        	scrubOrderDetailVO.setPcFlag(tempDeliveryInfoVO.getPcFlag());
        	scrubOrderVO.setAllowPCCheckUpdateOrder(true);
        }
        
                
        //iterate through list of comments
        Iterator commentIter = tempCommentList.iterator();
        String floristComment = " ";
        while(commentIter.hasNext())
        {
            CommentsVO commentsVO = (CommentsVO)commentIter.next();
            if(commentsVO.getCommentType().equalsIgnoreCase("Order"))
            {
                scrubOrderDetailVO.setOrderComments(commentsVO.getComment());
            }
            else if(commentsVO.getCommentType().equalsIgnoreCase("Florist"))
            {
                floristComment = commentsVO.getComment();
            }
        }

        //create the special instructions
        String institutionInfo = tempDeliveryInfoVO.getRecipientBusinessInfo();
        String specialInstructions = tempDeliveryInfoVO.getSpecialInstructions();
        if(specialInstructions == null)
        {
            specialInstructions = "";
        }
        if(institutionInfo!= null)
        {
            specialInstructions = specialInstructions + " Room Number/Ward:" + institutionInfo;
        }
        scrubOrderDetailVO.setSpecialInstructions(specialInstructions + floristComment.trim());

        // Add ons
        Set addonKeySet = tempOrderAddOnVO.keySet();
        Iterator addonIt = addonKeySet.iterator();
        List addonList = new ArrayList();
        while(addonIt.hasNext())
        {
            String addonKey = (String)addonIt.next();
            AddOnVO addonVO = (AddOnVO)tempOrderAddOnVO.get(addonKey);
        
            AddOnsVO scrubAddonVO = new AddOnsVO();
            scrubAddonVO.setAddOnCode(addonVO.getAddOnCode());
            scrubAddonVO.setAddOnQuantity(Long.toString(addonVO.getAddOnQuantity()));
            addonList.add(scrubAddonVO);
        }
        scrubOrderDetailVO.setAddOns(addonList);
        
        //Recipient is obtained from the clean _update tables
        RecipientsVO scrubRecipientsVO = new RecipientsVO();
        RecipientAddressesVO scrubRecipAddrVO = new RecipientAddressesVO();
        scrubRecipientsVO.setAutoHold(null);        
        scrubRecipientsVO.setFirstName(tempDeliveryInfoVO.getRecipientFirstName());
        scrubRecipientsVO.setLastName(tempDeliveryInfoVO.getRecipientLastName());
        scrubRecipientsVO.setCustomerId(tempDeliveryInfoVO.getRecipientId());
        scrubRecipAddrVO.setAddressLine1(tempDeliveryInfoVO.getRecipientAddress1());
        scrubRecipAddrVO.setAddressLine2(tempDeliveryInfoVO.getRecipientAddress2());
        scrubRecipAddrVO.setAddressType(tempDeliveryInfoVO.getRecipientAddressType());
        scrubRecipAddrVO.setCity(tempDeliveryInfoVO.getRecipientCity());
        scrubRecipAddrVO.setCountry(tempDeliveryInfoVO.getRecipientCountry());
        scrubRecipAddrVO.setCounty(null);        
        scrubRecipAddrVO.setInfo(tempDeliveryInfoVO.getRecipientBusinessInfo());
        scrubRecipAddrVO.setName(tempDeliveryInfoVO.getRecipientBusinessName()); 
        scrubRecipAddrVO.setPostalCode(tempDeliveryInfoVO.getRecipientZipCode());
        scrubRecipAddrVO.setStateProvince(tempDeliveryInfoVO.getRecipientState());
        scrubRecipAddrVO.setGeofindMatchCode(null);
        scrubRecipAddrVO.setLatitude(null);
        scrubRecipAddrVO.setLongitude(null);

        // Recipient phones
        List recipPhones = new ArrayList();
        RecipientPhonesVO scrubRecipPhone = new RecipientPhonesVO();
        scrubRecipPhone.setExtension(tempDeliveryInfoVO.getRecipientExtension());
        scrubRecipPhone.setPhoneNumber(tempDeliveryInfoVO.getRecipientPhoneNumber());
        scrubRecipPhone.setPhoneType("HOME");
        recipPhones.add(scrubRecipPhone);
        scrubRecipientsVO.setRecipientPhones(recipPhones);

        List scrubRecipAddrList = new ArrayList();
        scrubRecipAddrList.add(scrubRecipAddrVO);
        scrubRecipientsVO.setRecipientAddresses(scrubRecipAddrList);
        List recipientList = new ArrayList();
        recipientList.add(scrubRecipientsVO);
        scrubOrderDetailVO.setRecipients(recipientList);

        //add detail to order
        List orderDetailList = new ArrayList();
        orderDetailList.add(scrubOrderDetailVO);
        scrubOrderVO.setOrderDetail(orderDetailList);

        // Co brand
        Iterator coBrandIt = tempCoBrandList.iterator();
        List coBrandList = new ArrayList();
        while(coBrandIt.hasNext())
        {
            CoBrandVO cobrandVO = (CoBrandVO)coBrandIt.next();
        
            CoBrandVO scrubCoBrandVO = new CoBrandVO();
            scrubCoBrandVO.setInfoData(cobrandVO.getInfoData());
            scrubCoBrandVO.setInfoName(cobrandVO.getInfoName());
            
            coBrandList.add(scrubCoBrandVO);
        }        
        
        /**
        * Added membership information as part of co-brand information. This
        * was done on account of the ScrubMapperDAO, which requires membership
        * information to be sent as a part of co-brand information
        */
        if (tempDeliveryInfoVO.getMembershipFirstName() != null && 
                tempDeliveryInfoVO.getMembershipFirstName().length() > 0)
        {
              CoBrandVO scrubCoBrandVO = new CoBrandVO();
              scrubCoBrandVO.setInfoName("FNAME");
              scrubCoBrandVO.setInfoData(tempDeliveryInfoVO.getMembershipFirstName());
              coBrandList.add(scrubCoBrandVO);           
        }

        if (tempDeliveryInfoVO.getMembershipLastName() != null && 
                tempDeliveryInfoVO.getMembershipLastName().length() > 0)
        {
              CoBrandVO scrubCoBrandVO = new CoBrandVO();
              scrubCoBrandVO.setInfoName("LNAME");
              scrubCoBrandVO.setInfoData(tempDeliveryInfoVO.getMembershipLastName());
              coBrandList.add(scrubCoBrandVO);           
        }        

        if (tempDeliveryInfoVO.getMembershipNumber() != null && 
                tempDeliveryInfoVO.getMembershipNumber().length() > 0)
        {
              CoBrandVO scrubCoBrandVO = new CoBrandVO();
              scrubCoBrandVO.setInfoName(scrubMembershipVO.getMembershipType());
              scrubCoBrandVO.setInfoData(tempDeliveryInfoVO.getMembershipNumber());
              coBrandList.add(scrubCoBrandVO);           
        }        
        scrubOrderVO.setCoBrand(coBrandList);
        
        // Payments
        Iterator paymentsIter = tempPaymentList.iterator();
        List scrubPaymentList = new ArrayList();        
        while(paymentsIter.hasNext())
        {       
            PaymentVO tempPaymentVO = (PaymentVO)paymentsIter.next();
        
            PaymentsVO scrubPaymentVO = new PaymentsVO();
            scrubPaymentVO.setAafesTicket(tempPaymentVO.getAafesTicketNumber());
            scrubPaymentVO.setAcqReferenceNumber(tempPaymentVO.getAcqRefNumber());
            scrubPaymentVO.setAuthNumber(tempPaymentVO.getAuthorization());
            scrubPaymentVO.setAuthResult(tempPaymentVO.getAuthResult());
            scrubPaymentVO.setAvsCode(tempPaymentVO.getAvsCode());
            scrubPaymentVO.setPaymentsType(tempPaymentVO.getPaymentType());
            scrubPaymentVO.setPaymentMethodType(dao.getPaymentType(tempPaymentVO.getPaymentType()));
            scrubPaymentVO.setAmount(Double.toString(tempPaymentVO.getCreditAmount()));
            scrubPaymentVO.setNcApprovalCode(tempPaymentVO.getNcApprovalId());
            scrubPaymentVO.setCscFailureCount(tempPaymentVO.getCscFailureCount());
            scrubPaymentVO.setCscResponseCode(tempPaymentVO.getCscResponseCode());
            scrubPaymentVO.setCscValidatedFlag(tempPaymentVO.getCscValidatedFlag());
            scrubPaymentVO.setApAuth(tempPaymentVO.getTransactionId());

            //check if there is a credit card associated with payment
            CreditCardVO creditCardVO = (CreditCardVO)tempCreditCardMap.get(tempPaymentVO.getCardId());
    
            // Credit cards
            if(creditCardVO !=null)
            {
                CreditCardsVO scrubCreditCardVO = new CreditCardsVO();
                scrubCreditCardVO.setCCExpiration(creditCardVO.getCcExpiration());
                scrubCreditCardVO.setCCNumber(creditCardVO.getCcNumber());
                scrubCreditCardVO.setCCType(creditCardVO.getCcType());
                scrubCreditCardVO.setPin(tempPaymentVO.getGiftCardPin());
                List scrubCardList = new ArrayList();
                scrubCardList.add(scrubCreditCardVO);
                scrubPaymentVO.setCreditCards(scrubCardList);  
                scrubPaymentVO.setWalletIndicator(tempPaymentVO.getWalletIndicator());
            }

    
            // Get gift certificate if present
            if(tempPaymentVO.getGcCouponNumber() != null && tempPaymentVO.getGcCouponNumber().length() > 0)
            {
            
              BigDecimal giftCertAmount = new BigDecimal(tempPaymentVO.getCreditAmount());
              if(giftCertAmount == null)
              {
                 giftCertAmount = new BigDecimal("0");
              }           
                    
              scrubPaymentVO.setGiftCertificateId(tempPaymentVO.getGcCouponNumber());
            
              scrubPaymentVO.setPaymentsType("GC");
              scrubPaymentVO.setPaymentMethodType("G");
              scrubPaymentVO.setAmount(giftCertAmount.toString());

            }
            
            scrubPaymentList.add(scrubPaymentVO);                
            
        }        
        
        //set payment on the order
        scrubOrderVO.setPayments(scrubPaymentList);
        
        // For Free Shipping
        if(emailVO != null)
        {
          scrubOrderVO.setBuyerEmailAddress(emailVO.getEmailAddress());
        }
        
        scrubOrderVO.setBuyerSignedIn(originalDeliveryInfoVO.getBuyerSignedInFlag());
        scrubOrderVO.setCompanyId(originalDeliveryInfoVO.getCompanyId());
        
        return scrubOrderVO;
    }

	public void setDao(UpdateOrderDAO dao) {
		this.dao = dao;
	}

	public void setCustomerDAO(CustomerDAO customerDAO) {
		this.customerDAO = customerDAO;
	}    


}
