package com.ftd.mo.util;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.messaging.constants.MessagingConstants;
import com.ftd.messaging.vo.FTDMessageVO;
import com.ftd.mo.dao.UpdateOrderDAO;
import com.ftd.mo.vo.MessageStatusVO;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;

import java.util.Date;


public class ModifyOrderUTIL {
        
    private Connection conn;
    private static Logger logger = new Logger(ModifyOrderUTIL.class.getName());
    
    public ModifyOrderUTIL()
    {
    }

    public ModifyOrderUTIL(Connection connection)
    {
     this.conn = connection;   
    }
    
    /**
     *
     * 
     */
    public MessageStatusVO getMessageStatus(String orderDetailId, String messageType, String includeCompOrder) throws Exception{
       
        boolean hasLiveMercury = true;
        String attemptedFTD = null;
        String cancelSent = null;
        String compOrder = null;
        String ftdStatus = null;
        String hasLiveFTD = null;
        String rejectSent = null;
               
        // Instantiate UpdateOrderDAO
        UpdateOrderDAO uoDAO = new UpdateOrderDAO(conn);
        
        MessageStatusVO msVO = new MessageStatusVO();
        
        CachedResultSet mercuryOrderMessageStatusInfo = uoDAO.getMercuryOrderMessageStatus(orderDetailId, messageType, includeCompOrder);
        
        String ftdMsgAttemptedNotVer = uoDAO.getMercuryAttemptedStatus(orderDetailId);

        //populate mercury order message status from the mercury status query results
        if(mercuryOrderMessageStatusInfo.next())
        {
          if(mercuryOrderMessageStatusInfo.getString("reject_sent") != null)
            rejectSent = mercuryOrderMessageStatusInfo.getString("reject_sent").trim();
          if(mercuryOrderMessageStatusInfo.getString("cancel_sent") != null)
            cancelSent = mercuryOrderMessageStatusInfo.getString("cancel_sent").trim();
          if(mercuryOrderMessageStatusInfo.getString("attempted_ftd") != null)
            attemptedFTD = mercuryOrderMessageStatusInfo.getString("attempted_ftd").trim();
          if(mercuryOrderMessageStatusInfo.getString("has_live_ftd") != null)
            hasLiveFTD = mercuryOrderMessageStatusInfo.getString("has_live_ftd").trim();
          if(mercuryOrderMessageStatusInfo.getString("ftd_status") != null)
            ftdStatus = mercuryOrderMessageStatusInfo.getString("ftd_status").trim();
          if(mercuryOrderMessageStatusInfo.getString("comp_order") != null)
            compOrder = mercuryOrderMessageStatusInfo.getString("comp_order").trim();
        }
        
        if(messageType.equals("Mercury"))
        {
          if ( (hasLiveFTD != null    && hasLiveFTD.equalsIgnoreCase(COMConstants.CONS_NO) &&
                attemptedFTD != null  && attemptedFTD.equalsIgnoreCase(COMConstants.CONS_YES) &&
                rejectSent != null    && rejectSent.equalsIgnoreCase(COMConstants.CONS_NO) && 
                cancelSent != null    && cancelSent.equalsIgnoreCase(COMConstants.CONS_NO)) 
                ||
                (hasLiveFTD != null    && hasLiveFTD.equalsIgnoreCase(COMConstants.CONS_NO) &&
                attemptedFTD != null  && attemptedFTD.equalsIgnoreCase(COMConstants.CONS_NO) &&
                rejectSent != null    && rejectSent.equalsIgnoreCase(COMConstants.CONS_NO) && 
                cancelSent != null    && cancelSent.equalsIgnoreCase(COMConstants.CONS_NO) &&
                ftdMsgAttemptedNotVer != null && ftdMsgAttemptedNotVer.equalsIgnoreCase(COMConstants.CONS_YES) 
                )
                ||
                (hasLiveFTD != null    && hasLiveFTD.equalsIgnoreCase(COMConstants.CONS_NO) &&
                 attemptedFTD != null  && attemptedFTD.equalsIgnoreCase(COMConstants.CONS_NO) &&
                 rejectSent != null    && rejectSent.equalsIgnoreCase(COMConstants.CONS_NO) && 
                 cancelSent != null    && cancelSent.equalsIgnoreCase(COMConstants.CONS_NO)) 
                ||
                (hasLiveFTD != null    && hasLiveFTD.equalsIgnoreCase(COMConstants.CONS_NO) &&
                 attemptedFTD != null  && attemptedFTD.equalsIgnoreCase(COMConstants.CONS_YES) &&
                 rejectSent != null    && rejectSent.equalsIgnoreCase(COMConstants.CONS_YES) && 
                 cancelSent != null    && cancelSent.equalsIgnoreCase(COMConstants.CONS_NO)) 
                ||
                (hasLiveFTD != null    && hasLiveFTD.equalsIgnoreCase(COMConstants.CONS_NO) &&
                 attemptedFTD != null  && attemptedFTD.equalsIgnoreCase(COMConstants.CONS_YES) &&
                 rejectSent != null    && rejectSent.equalsIgnoreCase(COMConstants.CONS_NO) && 
                 cancelSent != null    && cancelSent.equalsIgnoreCase(COMConstants.CONS_YES)) 
                )
          {
            msVO.setLiveMercury(false);
          }
        }
        //check to see if the venus ftd is verified
        if(messageType.equals("Venus"))
        {
          if ( (hasLiveFTD != null    && hasLiveFTD.equalsIgnoreCase(COMConstants.CONS_YES) &&
                attemptedFTD != null  && attemptedFTD.equalsIgnoreCase(COMConstants.CONS_YES) && 
                rejectSent != null    && rejectSent.equalsIgnoreCase(COMConstants.CONS_NO) &&
                cancelSent != null    && cancelSent.equalsIgnoreCase(COMConstants.CONS_NO) && 
                ftdStatus != null     && ftdStatus.equalsIgnoreCase("MO") )
                ||
                ( hasLiveFTD != null    && hasLiveFTD.equalsIgnoreCase(COMConstants.CONS_NO) &&
                attemptedFTD != null  && attemptedFTD.equalsIgnoreCase(COMConstants.CONS_NO) && 
                rejectSent != null    && rejectSent.equalsIgnoreCase(COMConstants.CONS_NO) &&
                cancelSent != null    && cancelSent.equalsIgnoreCase(COMConstants.CONS_NO) ) 
                ||
                (hasLiveFTD != null    && hasLiveFTD.equalsIgnoreCase(COMConstants.CONS_NO) &&
                attemptedFTD != null  && attemptedFTD.equalsIgnoreCase(COMConstants.CONS_YES) &&
                rejectSent != null    && rejectSent.equalsIgnoreCase(COMConstants.CONS_YES) && 
                cancelSent != null    && cancelSent.equalsIgnoreCase(COMConstants.CONS_NO)) 
                ||
                (hasLiveFTD != null    && hasLiveFTD.equalsIgnoreCase(COMConstants.CONS_NO) &&
                attemptedFTD != null  && attemptedFTD.equalsIgnoreCase(COMConstants.CONS_YES) &&
                rejectSent != null    && rejectSent.equalsIgnoreCase(COMConstants.CONS_NO) && 
                cancelSent != null    && cancelSent.equalsIgnoreCase(COMConstants.CONS_YES)) 
              )
          {
              msVO.setLiveMercury(false);
          }
        }
        
        if(compOrder != null && compOrder.equalsIgnoreCase("C"))
        {
            msVO.setCompOrder(true);
        }
            
      return msVO; 
    }
    
    /**
     * Returns true if order is in printed status and it is after the zone
     * jump label date 
     * @param orderDetailId
     * @return CachedResultSet
     * @throws Exception
     */

    public boolean isZJOrderInTransit(String orderDetailId)
        throws Exception 
    {
        UpdateOrderDAO uoDAO = new UpdateOrderDAO(this.conn);
        FTDMessageVO messageVO =  uoDAO.getMessageDetailLastFTD(orderDetailId, "Venus", "Y");
        if(messageVO != null && messageVO.isZoneJumpFlag())
        {
            Date today = new Date();
            String sdsStatus = messageVO.getSdsStatus();
            if(sdsStatus != null) {
                if((sdsStatus.equalsIgnoreCase(COMConstants.SDS_STATUS_PRINTED)) 
                  &&  today.after(messageVO.getZoneJumpLabelDate()))
                {
                    return true;
                }
            }
        }  
      return false;
    }
    
    /**
     * Returns true if order is in printed status and it is after the zone
     * jump label date 
     * @param orderDetailId
     * @return CachedResultSet
     * @throws Exception
     */

    public boolean isZJOrderInTransit(String orderDetailId, FTDMessageVO messageVO)
        throws Exception 
    {
        if(messageVO != null && messageVO.isZoneJumpFlag())
        {
            Date today = new Date();
            String sdsStatus = messageVO.getSdsStatus();
            
            if(sdsStatus!= null) {
                if((sdsStatus.equalsIgnoreCase(COMConstants.SDS_STATUS_PRINTED)) 
                  &&  today.after(messageVO.getZoneJumpLabelDate()))
                {
                    return true;
                }
            }
        }  
      return false;
    }
    
    /**
     * Returns a ftd message vo object containing the message detail for the last
     * live ftd
     * @param orderDetailId
     * @return FTDMessageVO
     * @throws Exception
     */

    public FTDMessageVO getMessageDetail(String orderDetailId)
        throws Exception 
    {
        String messageType = "Venus";
        UpdateOrderDAO uoDAO = new UpdateOrderDAO(this.conn);
        
        return uoDAO.getMessageDetailLastFTD(orderDetailId, messageType, "Y");
    }
    
    /**
     * Returns a ftd message vo object containing the message detail for the last
     * live ftd
     * @param orderDetailId
     * @return FTDMessageVO
     * @throws Exception
     */

    public FTDMessageVO getMessageDetail(String orderDetailId, String messageType)
        throws Exception 
    {
        UpdateOrderDAO uoDAO = new UpdateOrderDAO(this.conn);
        return uoDAO.getMessageDetailLastFTD(orderDetailId, messageType, "Y");
    }
    
    /**
     * Returns true if order is in printed or shipped status and is on or after 
     * the ship date but before the delivery date
     * @param orderDetailId
     * @param shipDate
     * @param deliveryDate
     * @return boolean
     * @throws Exception
     */
     public boolean isOrderInTransit(String orderDetailId, Date shipDate, Date deliveryDate)
        throws Exception 
    {
        // if ship date is null, assume it's a florist order and return false
        if (shipDate == null){
            return false;
        }
        
        UpdateOrderDAO uoDAO = new UpdateOrderDAO(this.conn);
        FTDMessageVO messageVO =  uoDAO.getMessageDetailLastFTD(orderDetailId, "Venus", "Y");
        messageVO.getSdsStatus();
        Date today = new Date();
        String sdsStatus = messageVO.getSdsStatus();
        
        // Defect 4189 - order is not in transit if there's no SDS status.
        if(sdsStatus != null) {
            if((sdsStatus.equalsIgnoreCase(MessagingConstants.ORDER_DISP_SHIPPED) 
              || ( messageVO.getSdsStatus().equalsIgnoreCase(MessagingConstants.ORDER_DISP_PRINTED))) 
              &&  today.after(shipDate)
              &&  today.before(deliveryDate)
              )
            {
                return true;
            }
        }
      return false;
    }
    
    /**
     * Returns true if order is in printed or shipped status and is on or after 
     * the ship date but before the delivery date
     * @param shipDate
     * @param deliveryDate
     * @return boolean
     * @throws Exception
     */
     public boolean isOrderInTransit(Date shipDate, Date deliveryDate)
        throws Exception 
    {
        // if ship date is null, assume it's a florist order and return false
        if (shipDate == null){
            return false;
        }
        
        Date today = new Date();

        if(today.after(shipDate) &&  today.before(deliveryDate))
        {
            return true;
        }
      return false;
    }

}
