package com.ftd.mo.util;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.security.SecurityManager;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;
import org.apache.commons.lang.StringUtils;

public class BreadcrumbUtil  {
  private static final String BREADCRUMB_TOTAL       = "breadcrumb_total";
  private static final String BREADCRUMB_NUM         = "breadcrumb_num";
  private static final String BREADCRUMB_TITLE       = "breadcrumb_title";
  private static final String BREADCRUMB_URL         = "breadcrumb_url";
  private static final String BREADCRUMB_ACTION      = "breadcrumb_action";
  private static final String BREADCRUMB_ERROR_PAGE  = "breadcrumb_error_page";
  private static final String BREADCRUMB_ENABLE_LINK = "breadcrumb_enable_link";
  private static final String BREADCRUMB_PARAM_COUNT = "breadcrumb_param_count";
  private static final String BREADCRUMB_PARAM_NAME  = "breadcrumb_param_name";
  private static final String BREADCRUMB_PARAM_VALUE = "breadcrumb_param_value";

  public BreadcrumbUtil() {
  }


 /**---------------------------------------------------------------------------
  *
  * getBreadcrumbXML()
  *
  * Convenience (overloaded) method that just gets existing breadcrumb data
  * from request and returns resulting data as XML document.
  *
  * @param HttpServletRequest a_request
  * @throws Exception
  */
  public Document getBreadcrumbXML(HttpServletRequest a_request) throws Exception
  {
    return getBreadcrumbXML(a_request, null, null, null, null, null, null);
  }


 /**---------------------------------------------------------------------------
  *
  * getBreadcrumbXML()
  *
  * Convenience (overloaded) method that puts order_detail_id in optional param
  * list for you (since this is needed for all order update pages).
  *
  * @param HttpServletRequest a_request
  * @param String a_title - Title of breadcrumb (must be unique) for this page
  * @param String a_url - Name of Structs Action (e.g., LoadDeliveryInfo.do)
  * @param String a_action - Action parameter (e.g., get_updated_delivery_info)
  * @param String a_errorPage - Error display page
  * @param String a_enableLink - Flag indicating if breadcrumb should be a link
  * @param String a_params - Optional hash of additional name/value parameters for this page
  * @param String a_order_detail_id
  * @throws Exception
  */
  public Document getBreadcrumbXML(
    HttpServletRequest a_request, String a_title, String a_url, String a_action,
    String a_errorPage, String a_enableLink, HashMap a_params, String a_order_detail_id)
    throws Exception
  {
    HashMap lparams = a_params;

    if ((a_order_detail_id != null) && (a_order_detail_id.length() > 0)) {
      if (lparams == null) {
        lparams = new HashMap();
      }
      lparams.put(COMConstants.ORDER_DETAIL_ID, a_order_detail_id);
    }
    return getBreadcrumbXML(a_request, a_title, a_url, a_action, a_errorPage, a_enableLink, lparams);
  }


 /**---------------------------------------------------------------------------
  *
  * getBreadcrumbXML()
  *
  * Gets existing breadcrumb data from request, includes new breadcrumb item
  * (based on passed params), and returns resulting data as XML document.
  * If no title is specified, then only current breadcrumb data is returned.
  *
  * @param HttpServletRequest a_request
  * @param String a_title - Title of breadcrumb (must be unique) for this page.
  *         If null, then only current breadcrumb info is returned.
  * @param String a_url - Name of Structs Action (e.g., LoadDeliveryInfo.do)
  * @param String a_action - Action parameter (e.g., get_updated_delivery_info)
  * @param String a_errorPage - Error display page
  * @param String a_enableLink - Flag indicating if breadcrumb should be a link
  * @param String a_params - Optional hash of additional name/value parameters for this page
  * @throws Exception
  */
  public Document getBreadcrumbXML(
    HttpServletRequest a_request, String a_title, String a_url, String a_action,
    String a_errorPage, String a_enableLink, HashMap a_params)
    throws Exception
  {
    String lastPushTitle = "";
    String lastPushUrl = "";
    int totCrumbs = 0;
    int bcNum;
    String bcTitle = "";
    String bcUrl, bcAction, bcErrorPage, bcEnableLink;
    Document bcXML;
    Element bcTopElement, bcElement, bcTotalElement;
    Element numNode;
    Element titleNode, urlNode, actionNode, errorPageNode, enableLinkNode;
    Element bcParamTopElement, bcParamElement, bcParamCountElement;
    Element paramNameNode, paramValueNode;

    // Setup breadcrumbs XML doc
    //
    bcXML = (Document) DOMUtil.getDefaultDocument();
    bcTopElement = bcXML.createElement("BREADCRUMBS");
    bcXML.appendChild(bcTopElement);

    //------------
    //
    // Loop over breadcrumb info (if any) from page just submitted and convert to XML
    //
    if(a_request.getParameter(BREADCRUMB_TOTAL) != null) {
      totCrumbs = Integer.parseInt(a_request.getParameter(BREADCRUMB_TOTAL));
      for (bcNum=0; bcNum < totCrumbs; bcNum++) {
        bcTitle      = a_request.getParameter(BREADCRUMB_TITLE + "_" + bcNum);
        bcUrl        = a_request.getParameter(BREADCRUMB_URL + "_" + bcNum);
        bcAction     = a_request.getParameter(BREADCRUMB_ACTION + "_" + bcNum);
        bcErrorPage  = a_request.getParameter(BREADCRUMB_ERROR_PAGE + "_" + bcNum);
        bcEnableLink = a_request.getParameter(BREADCRUMB_ENABLE_LINK + "_" + bcNum);

        if (a_title != null && a_title.equals(bcTitle)) {

          //------
          //
          // Since breadcrumb title is same as current page, user must have
          // clicked on a breadcrumb to get back to this page, so all subsequent
          // breadcrumbs should be removed - which we accomplish by just
          // exiting this loop.
          //
          totCrumbs = bcNum;
          break;

        } else {

          //------
          //
          // Add current breadcrumb to XML doc
          //
          bcElement = bcXML.createElement("BREADCRUMB");
          bcTopElement.appendChild(bcElement);

          numNode = bcXML.createElement(BREADCRUMB_NUM);
          numNode.appendChild(bcXML.createTextNode(String.valueOf(bcNum)));
          bcElement.appendChild(numNode);
          titleNode = bcXML.createElement(BREADCRUMB_TITLE);              // Title
          titleNode.appendChild(bcXML.createTextNode(bcTitle));
          bcElement.appendChild(titleNode);
          urlNode = bcXML.createElement(BREADCRUMB_URL);                  // URL
          urlNode.appendChild(bcXML.createTextNode(bcUrl));
          bcElement.appendChild(urlNode);
          actionNode = bcXML.createElement(BREADCRUMB_ACTION);            // Action
          if (StringUtils.isNotBlank(bcAction)) {
            actionNode.appendChild(bcXML.createTextNode(bcAction));
          }
          bcElement.appendChild(actionNode);
          errorPageNode = bcXML.createElement(BREADCRUMB_ERROR_PAGE);     // Error Page
          if (StringUtils.isNotBlank(bcErrorPage)) {
            errorPageNode.appendChild(bcXML.createTextNode(bcErrorPage));
          }
          bcElement.appendChild(errorPageNode);
          enableLinkNode = bcXML.createElement(BREADCRUMB_ENABLE_LINK);   // Enable Link
          if (StringUtils.isNotBlank(bcEnableLink)) {
            enableLinkNode.appendChild(bcXML.createTextNode(bcEnableLink));
          }
          bcElement.appendChild(enableLinkNode);

          // Include any optional params for current breadcrumb
          //
          String nameOfParamTotalForCurrent = BREADCRUMB_PARAM_COUNT + "_" + bcNum;
          if (a_request.getParameter(nameOfParamTotalForCurrent) != null) {
            bcParamTopElement = bcXML.createElement("PARAMS");
            bcElement.appendChild(bcParamTopElement);
            int totParams = Integer.parseInt(a_request.getParameter(nameOfParamTotalForCurrent));
            for (int i=0; i < totParams; i++) {
              bcParamElement = bcXML.createElement("PARAM");
              paramNameNode  = bcXML.createElement(BREADCRUMB_PARAM_NAME);   // Name
              String reqValue = a_request.getParameter(BREADCRUMB_PARAM_NAME + "_" + bcNum + "_" + i);
              paramNameNode.appendChild(bcXML.createTextNode(reqValue));
              bcParamElement.appendChild(paramNameNode);
              paramValueNode = bcXML.createElement(BREADCRUMB_PARAM_VALUE);  // Value
              reqValue = a_request.getParameter(BREADCRUMB_PARAM_VALUE + "_" + bcNum + "_" + i);
              if (StringUtils.isNotBlank(reqValue)) {
                paramValueNode.appendChild(bcXML.createTextNode(reqValue));
              }
              bcParamElement.appendChild(paramValueNode);
              bcParamTopElement.appendChild(bcParamElement);
            }
            bcParamCountElement = bcXML.createElement(BREADCRUMB_PARAM_COUNT);
            bcParamTopElement.appendChild(bcParamCountElement);
            bcParamCountElement.appendChild(bcXML.createTextNode(String.valueOf(totParams)));
          } // end if (getParameter(BREADCRUMB_PARAM_COUNT)...

        } // end else
      } // end for (bcNum=0...
    }


    //------------
    //
    // Include new breadcrumb info for this page (based on passed parameters)
    //
    if (a_title != null) {
      bcElement = bcXML.createElement("BREADCRUMB");
      bcTopElement.appendChild(bcElement);
      bcNum   = totCrumbs++;
      numNode = bcXML.createElement(BREADCRUMB_NUM);
      numNode.appendChild(bcXML.createTextNode(String.valueOf(bcNum)));
      bcElement.appendChild(numNode);
      titleNode = bcXML.createElement(BREADCRUMB_TITLE);              // Title
      titleNode.appendChild(bcXML.createTextNode(a_title));
      bcElement.appendChild(titleNode);
      urlNode = bcXML.createElement(BREADCRUMB_URL);                  // URL
      urlNode.appendChild(bcXML.createTextNode( ((a_url != null)?a_url:"")  ));
      bcElement.appendChild(urlNode);
      actionNode = bcXML.createElement(BREADCRUMB_ACTION);            // Action
      if (StringUtils.isNotBlank(a_action)) {
        actionNode.appendChild(bcXML.createTextNode(a_action));
      }
      bcElement.appendChild(actionNode);
      errorPageNode = bcXML.createElement(BREADCRUMB_ERROR_PAGE);     // Error Page
      if (StringUtils.isNotBlank(a_errorPage)) {
        errorPageNode.appendChild(bcXML.createTextNode(a_errorPage));
      }
      bcElement.appendChild(errorPageNode);
      enableLinkNode = bcXML.createElement(BREADCRUMB_ENABLE_LINK);   // Enable Link
      if (StringUtils.isNotBlank(a_enableLink)) {
        enableLinkNode.appendChild(bcXML.createTextNode(a_enableLink));
      }
      bcElement.appendChild(enableLinkNode);

      // Include any optional params for this page
      //
      if (a_params != null) {
        bcParamTopElement = bcXML.createElement("PARAMS");
        bcElement.appendChild(bcParamTopElement);
        Set ks1 = a_params.keySet();
        Iterator iter = ks1.iterator();
        int paramCntr = 0;
        while(iter.hasNext()) {
          String key = iter.next().toString();
          bcParamElement = bcXML.createElement("PARAM");
          paramNameNode  = bcXML.createElement(BREADCRUMB_PARAM_NAME);   // Name
          paramNameNode.appendChild(bcXML.createTextNode(key));
          bcParamElement.appendChild(paramNameNode);
          paramValueNode = bcXML.createElement(BREADCRUMB_PARAM_VALUE);  // Value
          String value = (String) a_params.get(key);
          if (StringUtils.isNotBlank(value)) {
            paramValueNode.appendChild(bcXML.createTextNode(value));
          }
          bcParamElement.appendChild(paramValueNode);
          bcParamTopElement.appendChild(bcParamElement);
          paramCntr++;
        }
        bcParamCountElement = bcXML.createElement(BREADCRUMB_PARAM_COUNT);
        bcParamTopElement.appendChild(bcParamCountElement);
        bcParamCountElement.appendChild(bcXML.createTextNode(String.valueOf(paramCntr)));
      }
    }


    //------------
    //
    // Include number of total breadcrumbs
    //
    bcTotalElement = bcXML.createElement(BREADCRUMB_TOTAL);
    bcTopElement.appendChild(bcTotalElement);
    bcTotalElement.appendChild(bcXML.createTextNode(String.valueOf(totCrumbs)));

    return bcXML;
  }

}