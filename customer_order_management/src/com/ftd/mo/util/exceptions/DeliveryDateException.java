package com.ftd.mo.util.exceptions;

/**
 * DeliveryDateException represents any exceptions that may occur whe retrieving
 * delivery dates.
 *
 * @author Mark Moon, Software Architects Inc.
 * @version $Revision: 1.2 $
 */
public class DeliveryDateException
    extends Exception {

  public DeliveryDateException() {
    super();
  }

  public DeliveryDateException(String message) {
    super(message);
  }

  public DeliveryDateException(Throwable cause) {
    super(cause);
  }

  public DeliveryDateException(String message, Throwable cause) {
    super(message, cause);
  }
}