package com.ftd.mo.util;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.ftdutilities.DeliveryDateUTIL;
import com.ftd.ftdutilities.FieldUtils;
import com.ftd.ftdutilities.GeneralConstants;
import com.ftd.ftdutilities.OEDeliveryDate;
import com.ftd.ftdutilities.OEDeliveryDateParm;
import com.ftd.ftdutilities.OEParameters;
import com.ftd.ftdutilities.ShippingMethod;
import com.ftd.mo.dao.UpdateOrderDAO;
import com.ftd.mo.vo.ProductFlagsVO;
import com.ftd.osp.utilities.AddOnUtility;
import com.ftd.osp.utilities.FTDFuelSurchargeUtilities;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.order.vo.RecipientAddressesVO;
import com.ftd.osp.utilities.order.vo.RecipientsVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;


public class SearchUtil
{

  public static final int GET_OCCASION_description = 0;
  public static final int GET_COUNTRY_description = 1;
  private static Logger logger = new Logger("com.ftd.mo.util.SearchUtil");

  private static final int PROMOTION_BASE_POINTS =2;
  private static final int PROMOTION_UNIT_POINTS =3;
  private static final int PROMOTION_UNITS =4;
  private static final int PROMOTION_TYPE=7;

  //custom parameters returned from database procedure
  private static final String STATUS_PARAM = "RegisterOutParameterStatus";
  private static final String MESSAGE_PARAM = "RegisterOutParameterMessage";

  private String status;
  private String message;
  private Connection con = null;


  /**
  * Constructor
  */
  public SearchUtil(Connection con)
  {
      this.con = con;
  }



    /** This method takes in an XML document which contains holiday elements.
     *  For each holiday element it creates an OEDelvieryDate object which contain
     *  delivery flags.  All of the OEDeliveryDate objects are put into a map and
     *  returned.
     *  @params Document XML Document containing holidays
     *  @returns Map of OEDeliveryDate objects
     *  @throws Exception */
    public Map getHolidays(Document doc) throws Exception
    {
        SimpleDateFormat sdfInput = new SimpleDateFormat("MM/dd/yyyy");
        Map holidayMap = new HashMap();

        String xpath = "//HOLIDAY";
        NodeList nl = DOMUtil.selectNodes(doc, xpath);
        Date holidayUtilDate;
        Date currentDate = new Date();
        Element element = null;
        OEDeliveryDate holidayDate = null;

        if ( nl.getLength() > 0 )
        {
            for ( int i = 0; i < nl.getLength(); i++ )
            {
                element = (Element) nl.item(i);

                holidayDate = new OEDeliveryDate();
                holidayDate.setDeliveryDate(element.getAttribute("holidaydate"));
                holidayDate.setHolidayText(element.getAttribute("holidaydescription"));
                holidayDate.setDeliverableFlag(element.getAttribute("deliverableflag"));
                holidayDate.setShippingAllowed(element.getAttribute("shippingallowed"));
                holidayUtilDate = sdfInput.parse(holidayDate.getDeliveryDate());
                holidayMap.put(holidayDate.getDeliveryDate(), holidayDate);
            }
        }
        return holidayMap;
    }

    /**
     * This method builds the XML for a list of products or for one product.
     * This is used from the product list page and the product detail page.
     *
     * This method contains the logic to calculate prices, discounts, reward
     * miles, shipping costs, services fees, delivery methods...   This procedure also sets
     * flags (ProductFlagVO) which are used by the xsl page to determine
     * when popup messages need to be shown.
     *
     * @param isList Tells if the productList parameter is actually a list or just
     * one product
     * @param loadDeliveryDates Switch to turn off loading of delivery dates
     * @param productList An Document of product details
     * @param displayCount The number of products to display per page
     * @param argMonth The month
     * @param argYear The year
     * @param isCustomerDomesticFlag true if recip is domestic
     * @param domesticServiceFee The domestic service free
     * @param String interationialServiceFee
     * @param String sendToCustomerState State of the recipient
     * @param String sendToCustomerCountry
     * @param String pricingCode
     * @param String partnerId
     * @param List promotionList
     * @param String zipCode
     * @param ProductFlagsVO flagVO Various product flags which are used by the XSL page
     * @param String sourceCode
     * @return This method builds an Element object for the product list and
     * product detail pages.
     *
     * @author Jeff Penney
     *
     */
//********************************************************************************************
// TESTED
//********************************************************************************************
    public Element pageBuilder(boolean isList, boolean loadDeliveryDates,
        Document productList, int displayCount, String argMonth, String argYear,
          boolean isCustomerDomesticFlag, String domesticServiceFee, String internationalServiceFee,
          String sendToCustomerState, String sendToCustomerCountry,String pricingCode,
          String partnerId, List promotionList, String rewardType, String zipCode,
          ProductFlagsVO flagVO, String sourceCode, boolean customOrderFlag,
          String origProductId, String orderShipMethod)
      throws Exception
    {  
        SimpleDateFormat sdfInput = new SimpleDateFormat("MM/dd/yyyy");
        Document Document = DOMUtil.getDefaultDocument();
        Element rootElement = Document.createElement("root");

        try {


            HashMap shipMethodsMap = new HashMap();

            // Create initial node structure
            Element listElement = Document.createElement("PRODUCTLIST");
            Element productsElement = Document.createElement("PRODUCTS");
            Document.appendChild(rootElement);
            rootElement.appendChild(listElement);
            listElement.appendChild(productsElement);

            // Storage for delivery dates if product detail
            Element deliveryDatesListElement = null;
            Element shipMethodsElement = null;
            Element shipMethodElement = null;
            Element calendarListElement = null;
            Element yearListElement = null;

            // Pull out elements for the page
            Element element = null;
            NodeList nl = null;
            String xpath = null;

            //instantiate the update order dao
            UpdateOrderDAO uoDAO = new UpdateOrderDAO(this.con);

            //get global params
            OEParameters oeGlobalParms = getGlobalParms();

            // retrieve the Global Parameter information
            String globalGNADDLevel = "";

            globalGNADDLevel =  oeGlobalParms.getGNADDLevel();

            // retrieve the product information
            nl = DOMUtil.selectNodes(productList, "//PRODUCT");


            int pageSplitter = 0;

            if (nl.getLength() > 0)
            {
                double currentPrice;
                double currentDiscountAmount;

                String standardDiscountAmount = null;
                String deluxeDiscountAmount = null;
                String premiumDiscountAmount = null;

                String codifiedAvailable          = null;
                String codifiedDeliverable        = null;
                String codifiedProduct            = null;
                String codifiedSpecial            = null;
                String exceptionCode              = null;
                boolean origAndNewProductIdsSame  = false;
                String productId                  = null;
                String productType                = null;
                String stateId                    = null;
                String status                     = null;
                String shipMethodCarrier          = null;
                String shipMethodFlorist          = null;
                String shippingKey                = null;
                String standardPrice              = null;
                boolean vendorOrder               = false;
                String fuelSurcharge = null; 

                if (displayCount > 0)
                  fuelSurcharge = FTDFuelSurchargeUtilities.getFuelSurcharge(this.con);
                
                BigDecimal bdFuelSurcharge = new BigDecimal(0); 
                if (fuelSurcharge != null && !fuelSurcharge.equalsIgnoreCase(""))
                  bdFuelSurcharge = new BigDecimal(fuelSurcharge); 

                for(int i=pageSplitter; i < (pageSplitter + displayCount) && i < nl.getLength(); i++)
                {
                    element = (Element) nl.item(i);

                    codifiedAvailable     = element.getAttribute("codifiedavailable");
                    codifiedDeliverable   = element.getAttribute("codifieddeliverable");
                    codifiedProduct       = element.getAttribute("codifiedproduct");
                    codifiedSpecial       = element.getAttribute("codifiedspecial");
                    exceptionCode         = element.getAttribute("exceptioncode");
                    productId             = element.getAttribute("productid");
                    productType           = element.getAttribute("producttype");
                    stateId               = element.getAttribute("stateid");
                    status                = element.getAttribute("status");
                    shipMethodCarrier     = element.getAttribute("shipmethodcarrier");
                    shipMethodFlorist     = element.getAttribute("shipmethodflorist");
                    shippingKey           = element.getAttribute("shippingkey");
                    standardPrice         = element.getAttribute("standardprice");

                    element.setAttribute("fuel_surcharge", bdFuelSurcharge.toString());
                    
                    // Fuel surcharge is added to serviceCharge here in case it doesn't get set in any conditionals below
                    String serviceCharge = element.getAttribute("servicecharge");
                    if ((serviceCharge != null) && (!serviceCharge.equalsIgnoreCase(""))) 
                    {
                      BigDecimal serviceChargePlusFS = bdFuelSurcharge.add(new BigDecimal(serviceCharge));
                      element.setAttribute("servicecharge", serviceChargePlusFS.setScale(2, BigDecimal.ROUND_HALF_DOWN).toString());
                    }


                    /*
                     * There could be 3 combinations:
                     *
                     * shipMethodCarrier = Y and shipMethodFlorist = N - vendor order
                     * shipMethodCarrier = N and shipMethodFlorist = Y - florist order
                     * shipMethodCarrier = Y and shipMethodFlorist = Y - could be either vendor or flosrist order
                     *                     look at the ship method.
                     *
                     */

                    if (shipMethodCarrier != null && shipMethodCarrier.equalsIgnoreCase(COMConstants.CONS_YES) &&
                        shipMethodFlorist != null &&  shipMethodFlorist.equalsIgnoreCase(COMConstants.CONS_NO))
                    {
                      vendorOrder = true;
                    }
                    else if
                      (shipMethodCarrier != null && shipMethodCarrier.equalsIgnoreCase(COMConstants.CONS_NO) &&
                       shipMethodFlorist != null && shipMethodFlorist.equalsIgnoreCase(COMConstants.CONS_YES))
                    {
                      vendorOrder = false;
                    }
                    else
                    {
                      if (orderShipMethod == null || orderShipMethod.equalsIgnoreCase("") ||
                          orderShipMethod.equalsIgnoreCase("SD"))
                      {
                        vendorOrder = false;
                      }
                      else
                      {
                        vendorOrder = true;
                      }
                    }

                    if (productId.equalsIgnoreCase(origProductId))
                      origAndNewProductIdsSame = true;

                    //If ship method carrier = Y
                    if ( shipMethodCarrier != null                                                  &&
                         shipMethodCarrier.equalsIgnoreCase(COMConstants.CONS_YES)  )
                    {
                        //This method calls a stored procudure to get all the available ship
                        //methods for this product.  The method returns a hashmap of shipping
                        //objects which contain delivery types and prices.
                        HashMap shippingMethods = this.getProductShippingMethods(productId, standardPrice, shippingKey);

                        //Set the delivery types and prices in the product XML Node
                        int count = 1;
                        ShippingMethod method = null;
                        BigDecimal deliveryChargePlusFS; 

                        //is standard delivery an option?
                        if(shippingMethods.containsKey(COMConstants.CONS_DELIVERY_STANDARD))
                        {
                            method = (ShippingMethod)shippingMethods.get(COMConstants.CONS_DELIVERY_STANDARD);
                            element.setAttribute(("deliverytype" + count), COMConstants.CONS_DELIVERY_STANDARD);
                            deliveryChargePlusFS = bdFuelSurcharge.add(method.getDeliveryCharge());
                            element.setAttribute(("deliveryprice" + count), deliveryChargePlusFS.setScale(2, BigDecimal.ROUND_HALF_DOWN).toString());
                            count++;
                        }

                        //is two day an option?
                        if(shippingMethods.containsKey(COMConstants.CONS_DELIVERY_TWO_DAY))
                        {
                            method = (ShippingMethod)shippingMethods.get(COMConstants.CONS_DELIVERY_TWO_DAY);
                            element.setAttribute(("deliverytype" + count), COMConstants.CONS_DELIVERY_TWO_DAY);
                            deliveryChargePlusFS = bdFuelSurcharge.add(method.getDeliveryCharge());
                            element.setAttribute(("deliveryprice" + count), deliveryChargePlusFS.setScale(2, BigDecimal.ROUND_HALF_DOWN).toString());
                            count++;
                        }

                        //is next day an option?
                        if(shippingMethods.containsKey(COMConstants.CONS_DELIVERY_NEXT_DAY))
                        {
                            method = (ShippingMethod)shippingMethods.get(COMConstants.CONS_DELIVERY_NEXT_DAY);
                            element.setAttribute(("deliverytype" + count), COMConstants.CONS_DELIVERY_NEXT_DAY);
                            deliveryChargePlusFS = bdFuelSurcharge.add(method.getDeliveryCharge());
                            element.setAttribute(("deliveryprice" + count), deliveryChargePlusFS.setScale(2, BigDecimal.ROUND_HALF_DOWN).toString());
                            count++;
                        }

                        //is sunday delivery an option?
                        if(shippingMethods.containsKey(COMConstants.CONS_DELIVERY_SATURDAY))
                        {
                            method = (ShippingMethod)shippingMethods.get(COMConstants.CONS_DELIVERY_SATURDAY);
                            element.setAttribute(("deliverytype" + count), COMConstants.CONS_DELIVERY_SATURDAY);
                            deliveryChargePlusFS = bdFuelSurcharge.add(method.getDeliveryCharge());
                            element.setAttribute(("deliveryprice" + count), deliveryChargePlusFS.setScale(2, BigDecimal.ROUND_HALF_DOWN).toString());
                            count++;
                        }
                    }
                    // Determine if there is a special fee assoc with state and product type
                    flagVO.setSpecialFeeFlag("N");
                    if  ( sendToCustomerState != null                                               &&
                          ( sendToCustomerState.equalsIgnoreCase("AK")    ||
                            sendToCustomerState.equalsIgnoreCase("HI")
                          )
                        )
                    {
                        if ( productType != null                                                    &&
                             productType.equalsIgnoreCase(COMConstants.CONS_PRODUCT_TYPE_SPEGFT))
                        {
                            element.setAttribute("displayspecialfee", "Y");
                            flagVO.setSpecialFeeFlag("Y");
                        }
                    }

                    // added origAndNewProductsSame check for florist order.
                    // Set unavail flag if the status is U -- product is unavaiable if status is "U"
                    flagVO.setDisplayProductUnavailable(COMConstants.CONS_NO);
                    if ( status != null && status.equalsIgnoreCase("U") )
                    {
                      //if product is florist item and is unavailable, check if the new and orig
                      //products are the same.  If the orig and new products are the same,
                      //bypass the check. else, trip a flag.
                      if (origAndNewProductIdsSame && !vendorOrder)
                      {}
                      else
                        flagVO.setDisplayProductUnavailable(COMConstants.CONS_YES);
                    }


                    // Set unavail flag if an unavailable codified product
                    if (
                        // product is unavaiable if it is a regular codified product
                        // and it is set as unavailable for this zip code in the
                        // CODIFIED_PRODUCTS table.  It also must not be carrier delivered
                          ( codifiedAvailable != null                                           &&
                            codifiedAvailable.equalsIgnoreCase(COMConstants.CONS_NO)            &&
                            (shipMethodCarrier != null && !shipMethodCarrier.equalsIgnoreCase("Y"))) ||

                        // product is unavaiable if it is a regular codified product
                        // and GNADD is at level 3
                         (codifiedProduct != null                                               &&
                          codifiedProduct.equalsIgnoreCase(COMConstants.CONS_YES)               &&
                          globalGNADDLevel.equalsIgnoreCase("3"))                                   ||

                        //added this as part of PHASE III product unavaiability check
                        //if codified undeliverable product that cannot be deliveried via FedEx
                        // product is unavaiable if it is a regular codified product
                        // and it is not deliverable to a zip code
                         (  codifiedProduct != null                                             &&
                            codifiedDeliverable != null                                         &&
                            shipMethodCarrier != null                                           &&
                            codifiedProduct.equalsIgnoreCase(COMConstants.CONS_YES)             &&
                            codifiedDeliverable.equalsIgnoreCase(COMConstants.CONS_NO)          &&
                            !shipMethodCarrier.equalsIgnoreCase(COMConstants.CONS_YES) )

                       )
                    {
                        flagVO.setDisplayProductUnavailable(COMConstants.CONS_YES);
                    }

                    // Personalized products should not be selectable
                    if ( element.getAttribute("personalizationtemplateid") != null &&
                        !element.getAttribute("personalizationtemplateid").equals(StringUtils.EMPTY) &&
                        !element.getAttribute("personalizationtemplateid").equalsIgnoreCase("NONE")) {
                    
                        flagVO.setDisplayPersonalizedProduct(GeneralConstants.YES);        
                    }
                    else {
                        flagVO.setDisplayPersonalizedProduct(GeneralConstants.NO);           
                    }

                    // Special Codified products should be deliverable if GNADD is on.
                    if (status != null                                                              &&
                        codifiedSpecial != null                                                     &&
                        codifiedDeliverable != null                                                 &&
                        !status.equalsIgnoreCase("U")                                               &&
                        codifiedSpecial.equalsIgnoreCase(COMConstants.CONS_YES)                     &&
                        !codifiedDeliverable.equalsIgnoreCase("N")                                  &&
                        !codifiedDeliverable.equalsIgnoreCase("D")
                       )
                    {
                        flagVO.setDisplayProductUnavailable(COMConstants.CONS_NO);
                    }
                    //if (Canada or Puerto Rico or Virgin Islands) & (FreshCut or Special Gift),
                    //do not allow
                    if(sendToCustomerState != null && sendToCustomerCountry != null && productType != null)
                    {
                        if  ( sendToCustomerCountry.equalsIgnoreCase(COMConstants.CONS_COUNTRY_CODE_CANADA_CA)    ||
                              sendToCustomerCountry.equalsIgnoreCase(COMConstants.CONS_STATE_CODE_PUERTO_RICO)    ||
                              //sendToCustomerState.equalsIgnoreCase(COMConstants.CONS_STATE_CODE_PUERTO_RICO)      ||
                              sendToCustomerCountry.equalsIgnoreCase(COMConstants.CONS_STATE_CODE_VIRGIN_ISLANDS)
                              //sendToCustomerState.equalsIgnoreCase(COMConstants.CONS_STATE_CODE_VIRGIN_ISLANDS)
                            )
                        {
                          if (productType.equalsIgnoreCase(COMConstants.CONS_PRODUCT_TYPE_SPEGFT))
                          {
                            flagVO.setDropShipAllowed("N");
                          }
                          else if (productType.equalsIgnoreCase(COMConstants.CONS_PRODUCT_TYPE_FRECUT))
                          {
                            flagVO.setDisplayProductUnavailable("Y");
                          }
                        }
                    }

                    //LoadDeliveryDates is a flag which is passed into this procedure.  Delivery date loads
                    //is not always needed, and since it is time consuming this flag was put in. (e.g.
                    //delivery date loads and needed by the getProductById method)
                    if(loadDeliveryDates)
                    {
                        // retreive first available delivery date information for product list
                        if ( isList )
                        {
                            OEDeliveryDate firstAvailableDate = null;

                            //This method returns the first available delivery date.
                            List dates = this.getItemDeliveryDates(
                                        productList,
                                        productId,
                                        true, zipCode, isCustomerDomesticFlag,
                                        domesticServiceFee, internationalServiceFee,
                                        flagVO, sendToCustomerState, sendToCustomerCountry,
                                        sourceCode, customOrderFlag);

                            firstAvailableDate = (OEDeliveryDate)dates.get(0);

                            // Determine which carrier to show (florist or fed ex)
                            //c2
                            for (int ii = 0; ii < firstAvailableDate.getShippingMethods().size(); ii++)
                            {
                                ShippingMethod shipMethod = (ShippingMethod)firstAvailableDate.getShippingMethods().get(ii);
                                if(shipMethod.getCode().equalsIgnoreCase(COMConstants.CONS_DELIVERY_FLORIST_CODE))
                                {
                                    element.setAttribute("firstdeliverymethod", "florist");

                                    // If this is SDG and a florist can deliver then remove all other shipping methods
                                    if(productType.equalsIgnoreCase(COMConstants.CONS_PRODUCT_TYPE_SDG) ||
                                       productType.equalsIgnoreCase(COMConstants.CONS_PRODUCT_TYPE_SDFC))
                                    {
                                        element.setAttribute("deliverytype1", "");
                                        element.setAttribute("deliverytype2", "");
                                        element.setAttribute("deliverytype3", "");
                                        element.setAttribute("deliverytype4", "");
                                        element.setAttribute("deliverytype5", "");
                                    }
                                    break;
                                }
                                else
                                {
                                    element.setAttribute("firstdeliverymethod", "carrier");
                                }
                            }

                            element.setAttribute("deliverydate", firstAvailableDate.getDisplayDate());
                        }
                        // retrieve delivery date information for selected product
                        else
                        {
                            List dateList = getItemDeliveryDates(productList, productId,
                                            false, zipCode, isCustomerDomesticFlag,
                                            domesticServiceFee, internationalServiceFee,
                                            flagVO, sendToCustomerState, sendToCustomerCountry,
                                            sourceCode, customOrderFlag);


                            deliveryDatesListElement = Document.createElement("DELIVERYDATES");

                            Element deliveryDateElement = null;
                            boolean firstDate = true;

                            for(int z = 0; z < dateList.size(); z++) {
                                deliveryDateElement = Document.createElement("DELIVERYDATE");


                                OEDeliveryDate deliveryDate = (OEDeliveryDate) dateList.get(z);
                                if ( deliveryDate.getDeliverableFlag().equalsIgnoreCase("Y") )
                                {
                                    if (firstDate) {
                                        element.setAttribute("deliverydate", deliveryDate.getDeliveryDate());
                                        firstDate = false;
                                    }

                                    deliveryDateElement.setAttribute("date", deliveryDate.getDeliveryDate());
                                    deliveryDateElement.setAttribute("dayofweek", deliveryDate.getDayOfWeek());

                                    // Loop through the delivery methods and concat them
                                    StringBuffer sbTypes = new StringBuffer();
                                    ShippingMethod shippingMethod = null;
                                    for (int d = 0; d < deliveryDate.getShippingMethods().size(); d++)
                                    {
                                        shippingMethod = (ShippingMethod)deliveryDate.getShippingMethods().get(d);
                                        sbTypes.append(shippingMethod.getCode()).append(":");
                                        shipMethodsMap.put(shippingMethod.getCode(), shippingMethod.getDeliveryCharge());
                                    }
                                    deliveryDateElement.setAttribute("types", sbTypes.toString());

                                    deliveryDateElement.setAttribute("displaydate", deliveryDate.getDisplayDate());
                                    deliveryDatesListElement.appendChild(deliveryDateElement);
                                }
                            }

                            if ( productType.equalsIgnoreCase(COMConstants.CONS_PRODUCT_TYPE_FRECUT) ||
                                 productType.equalsIgnoreCase(COMConstants.CONS_PRODUCT_TYPE_SPEGFT) )
                            {
                                if ( shipMethodsMap.size() > 0 )
                                {
                                    shipMethodElement = Document.createElement("SHIPPINGMETHOD");


                                    Collection methodCollection = shipMethodsMap.keySet();
                                    Iterator iter = methodCollection.iterator();

                                    while ( iter.hasNext() )
                                    {
                                        String description = (String) iter.next();
                                        BigDecimal cost = (BigDecimal) shipMethodsMap.get(description);

                                        Element entry = (Element) Document.createElement("METHOD");
                                        entry.setAttribute("description", description);
                                        entry.setAttribute("cost", cost.toString());

                                        shipMethodElement.appendChild(entry);

                                    }
                                }
                            }
                        }
                    }

                    // Convert prices to XX.XX format
                    String price = null;
                    price = element.getAttribute("standardprice");
                    if(price != null && price.length() > 0)
                    {
                        price = new BigDecimal(price).setScale(2).toString();
                        element.setAttribute("standardprice", price);
                    }

                    price = element.getAttribute("deluxeprice");
                    if(price != null && price.length() > 0)
                    {
                        price = new BigDecimal(price).setScale(2).toString();
                        element.setAttribute("deluxeprice", price);
                    }
                    price = element.getAttribute("premiumprice");
                    if(price != null && price.length() > 0)
                    {
                        price = new BigDecimal(price).setScale(2).toString();
                        element.setAttribute("premiumprice", price);
                    }

                    // Calculate discounts for product
                    standardDiscountAmount = element.getAttribute("standarddiscountamt");
                    deluxeDiscountAmount = element.getAttribute("deluxediscountamt");
                    premiumDiscountAmount = element.getAttribute("premiumdiscountamt");

                    if(standardDiscountAmount != null && !standardDiscountAmount.equalsIgnoreCase("")) {
                        element.setAttribute("standardrewardvalue", standardDiscountAmount);
                    }

                    if(deluxeDiscountAmount != null && !deluxeDiscountAmount.equalsIgnoreCase("")) {
                        element.setAttribute("deluxerewardvalue", deluxeDiscountAmount);
                    }

                    if(premiumDiscountAmount != null && !premiumDiscountAmount.equalsIgnoreCase("")) {
                        element.setAttribute("premiumrewardvalue", premiumDiscountAmount);
                    }

                    // Determine if its a partner or standard discount
                    if(pricingCode != null && pricingCode.equalsIgnoreCase(COMConstants.CONS_PARTNER_PRICE_CODE) && partnerId != null && !partnerId.equalsIgnoreCase("")) {

                        if(element.getAttribute("standardprice") != null && !element.getAttribute("standardprice").equalsIgnoreCase("")) {
                            currentPrice = new BigDecimal(element.getAttribute("standardprice")).setScale(0, BigDecimal.ROUND_UP).doubleValue();
                            element.setAttribute("standardrewardvalue", retrievePromotionValue(sourceCode, currentPrice));
                        }
                        if(element.getAttribute("deluxeprice") != null && !element.getAttribute("deluxeprice").equalsIgnoreCase("")) {
                            currentPrice =  new BigDecimal(element.getAttribute("deluxeprice")).setScale(0, BigDecimal.ROUND_UP).doubleValue();
                            element.setAttribute("deluxerewardvalue", retrievePromotionValue(sourceCode, currentPrice));
                        }
                        if(element.getAttribute("premiumPrice") != null && !element.getAttribute("premiumprice").equalsIgnoreCase("")) {
                            currentPrice = new BigDecimal(element.getAttribute("premiumprice")).setScale(0, BigDecimal.ROUND_UP).doubleValue();
                            element.setAttribute("premiumrewardvalue", retrievePromotionValue(sourceCode, currentPrice));
                        }
                    }

                    else
                    {

                        if(rewardType != null)
                        {
                            // Discount type of dollars off
                            // if rewardType = "D"
                            if(rewardType.equalsIgnoreCase("D"))
                            {
                                if(standardDiscountAmount != null && !standardDiscountAmount.equalsIgnoreCase(""))
                                {
                                    currentPrice = Double.parseDouble(element.getAttribute("standardprice"));
                                    currentPrice = currentPrice - Double.parseDouble(standardDiscountAmount);
                                    element.setAttribute("standarddiscountprice", new BigDecimal(currentPrice).setScale(2, BigDecimal.ROUND_HALF_DOWN).toString());
                                }

                                if(deluxeDiscountAmount != null && !deluxeDiscountAmount.equalsIgnoreCase(""))
                                {
                                    currentPrice = Double.parseDouble(element.getAttribute("deluxeprice"));
                                    currentPrice = currentPrice - Double.parseDouble(deluxeDiscountAmount);
                                    element.setAttribute("deluxediscountprice", new BigDecimal(currentPrice).setScale(2, BigDecimal.ROUND_HALF_DOWN).toString());
                                }

                                if(premiumDiscountAmount != null && !premiumDiscountAmount.equalsIgnoreCase(""))
                                {
                                    currentPrice = Double.parseDouble(element.getAttribute("premiumprice"));
                                    currentPrice = currentPrice - Double.parseDouble((premiumDiscountAmount));
                                    element.setAttribute("premiumdiscountprice", new BigDecimal(currentPrice).setScale(2, BigDecimal.ROUND_HALF_DOWN).toString());
                                }
                            }

                            // Discount type of percent off
                            // if rewardType = "P"
                            else if(rewardType.equalsIgnoreCase("P"))
                            {
                                if(standardDiscountAmount != null && !standardDiscountAmount.equalsIgnoreCase(""))
                                {
                                    currentPrice = Double.parseDouble(element.getAttribute("standardprice"));
                                    currentPrice = currentPrice - new BigDecimal((currentPrice * (Double.parseDouble(standardDiscountAmount) / 100))).setScale(2, BigDecimal.ROUND_DOWN).doubleValue();
                                    element.setAttribute("standarddiscountprice", new BigDecimal(currentPrice).setScale(2, BigDecimal.ROUND_HALF_DOWN).toString());
                                }

                                if(deluxeDiscountAmount != null && !deluxeDiscountAmount.equalsIgnoreCase(""))
                                {
                                    currentPrice = Double.parseDouble(element.getAttribute("deluxeprice"));
                                    currentPrice = currentPrice - new BigDecimal((currentPrice * (Double.parseDouble(deluxeDiscountAmount) / 100))).setScale(2, BigDecimal.ROUND_DOWN).doubleValue();
                                    element.setAttribute("deluxediscountprice", new BigDecimal(currentPrice).setScale(2, BigDecimal.ROUND_HALF_DOWN).toString());
                                }

                                if(premiumDiscountAmount != null && !premiumDiscountAmount.equalsIgnoreCase(""))
                                {
                                    currentPrice = Double.parseDouble(element.getAttribute("premiumprice"));
                                    currentPrice = currentPrice - new BigDecimal((currentPrice * (Double.parseDouble(premiumDiscountAmount) / 100))).setScale(2, BigDecimal.ROUND_DOWN).doubleValue();
                                    element.setAttribute("premiumdiscountprice", new BigDecimal(currentPrice).setScale(2, BigDecimal.ROUND_HALF_DOWN).toString());
                                }
                            }
                        }
                    }

                    productsElement.appendChild(Document.importNode(element, true));


                    // Do only if product detail (only one cycle in loop)
                    if ( !isList && loadDeliveryDates )
                    {
                        rootElement.appendChild(deliveryDatesListElement);

                        Document deliveryDateDocument = DOMUtil.getDefaultDocument();
                        Element deliveryDateElement = deliveryDateDocument.createElement("DELIVERYDATELIST");
                        deliveryDateElement.appendChild(deliveryDateDocument.importNode(deliveryDatesListElement, true));
                        deliveryDateDocument.appendChild(deliveryDateElement);
                    }

                } // end of For...Loop

                // only output for Product List
                Element shipMethods = Document.createElement("SHIPPINGMETHODS");

                // If zip code state is Alaska (AK) or Hawaii (HI) output valid
                // delivery types for state for presentation layer to use
                if  ( (stateId != null) &&
                      (stateId.equalsIgnoreCase("AK") ||
                       stateId.equalsIgnoreCase("HI")) )
                {
                    // if 2-day delivery is not one of the ship methods, then do not show any methods
                    // and set the product as undeliverable
                    if ( isList ||
                         shipMethodsMap.containsKey(COMConstants.CONS_DELIVERY_TWO_DAY) )
                    {
                        BigDecimal cost = (BigDecimal) shipMethodsMap.get(COMConstants.CONS_DELIVERY_TWO_DAY);
                        if ( cost == null )
                        {
                            cost = new BigDecimal("0.00");
                        }

                        shipMethodElement = Document.createElement("SHIPPINGMETHOD");

                        Element entry = (Element) Document.createElement("METHOD");
                        entry.setAttribute("description", COMConstants.CONS_DELIVERY_TWO_DAY);
                        entry.setAttribute("cost", cost.toString());

                        shipMethodElement.appendChild(entry);

                    }
                    else if ( productType.equalsIgnoreCase(COMConstants.CONS_PRODUCT_TYPE_FLORAL) )
                    {
                        BigDecimal cost = (BigDecimal) shipMethodsMap.get(COMConstants.CONS_DELIVERY_STANDARD);
                        if ( cost == null )
                        {
                            cost = new BigDecimal("0.00");
                        }

                        shipMethodElement = Document.createElement("SHIPPINGMETHOD");
                        Element entry = (Element) Document.createElement("METHOD");
                        entry.setAttribute("description", COMConstants.CONS_DELIVERY_STANDARD);
                        entry.setAttribute("cost", cost.toString());

                        shipMethodElement.appendChild(entry);
                    }
                    else if(productType.equalsIgnoreCase(COMConstants.CONS_PRODUCT_TYPE_FRECUT))
                    // If the state is AK or HI and this is a freshcut then
                    // delivery is not available unless to day is available
                    {
                        flagVO.setDisplayProductUnavailable(COMConstants.CONS_YES);
                    }
                }
                // product list, show all delivery methods
                else if ( isList )
                {
                    // retrieve the Global Parameter information
                    nl = DOMUtil.selectNodes(productList,"//SHIPPINGMETHOD");

                    if ( nl.getLength() > 0 )
                    {
                        shipMethodElement = Document.createElement("SHIPPINGMETHOD");

                        for (int z = 0; z < nl.getLength(); z++)
                        {
                            element = (Element) nl.item(z);

                            Element entry = (Element) Document.createElement("METHOD");
                            entry.setAttribute("description", element.getAttribute("description"));
                            entry.setAttribute("cost", element.getAttribute("cost"));
                            shipMethodElement.appendChild(entry);
                        }
                    }
                }
                // product detail / delivery information, only show available delivery methods
                if ( shipMethodElement != null )
                {
                    shipMethods.appendChild(shipMethodElement);
                }

                rootElement.appendChild(shipMethods);
            }
            else
            {
              //no products in the list
              logger.debug("No products in the list");
            }
        }

        finally
        {
        }
        return rootElement;
    }





    /**
     * This method determines if a product is available for shipping for the given
     * day of the week.  This method only takes into account the day or the week, not the
     * date.
     *
     * @param int The day of the week to check (1-sunday, 2-monday...)
     * @param Element The product XML Element
     * @returns true if product is available for delivery
     */
    private boolean isAvailableForShipping(int currentDayOfWeek, Element productElement) {

        String availableFlag = "N";

        switch (currentDayOfWeek)
        {
            case 1:     // sunday
                availableFlag = productElement.getAttribute("sundayflag");
                break;
            case 2:     // monday
                availableFlag = productElement.getAttribute("mondayflag");
                break;
            case 3:     // tuesday
                availableFlag = productElement.getAttribute("tuesdayflag");
                break;
            case 4:     // wednesday
                availableFlag = productElement.getAttribute("wednesdayflag");
                break;
            case 5:     // thursday
                availableFlag = productElement.getAttribute("thursdayflag");
                break;
            case 6:     // friday
                availableFlag = productElement.getAttribute("fridayflag");
                break;
            case 7:     // saturday
                availableFlag = productElement.getAttribute("saturdayflag");
                break;
        }

        if(availableFlag.equalsIgnoreCase("Y"))
            return true;
        else
            return false;
    }

  /* public Element convertLookupAnchors(Document sourceLookUpXML)
        throws Exception{

        NodeList nl = null;
        String anchor = null;
        LinkedList anchorList = new LinkedList();
        Document Document = DOMUtil.getDocument();
        Element rootElement = rootElement = Document.createElement("root");


            String xpath = "sourceCodeLookup/searchResults/searchResult";

            nl = DOMUtil.selectNodes(sourceLookUpXML,xpath);

            if (nl.getLength() > 0)
            {
                Element element = null;

                for(int i = 0; i < nl.getLength(); i++) {
                    element = (Element) nl.item(i);
                    anchor = element.getAttribute("anchor");

                    if(!anchorList.contains(anchor)) {
                        anchorList.add(anchor);
                    }
                }

                Collections.sort(anchorList);

                Element anchorListElement = Document.createElement("anchors");
                rootElement.appendChild(anchorListElement);
                Element anchorElement = null;

                Iterator anchorIterator = anchorList.iterator();
                while(anchorIterator.hasNext()) {
                    anchorElement = Document.createElement("anchor");
                    anchorElement.setAttribute("value", (String) anchorIterator.next());
                    anchorListElement.appendChild(anchorElement);
                }
            }


        return rootElement;
    }*/

    /* This method returns a list of promotions for the specified source code. */
//    public String retrievePromotionValue(List promotionList, double price) {
//      return "";
//    }

  /** 
   * This method returns the number of reward points that will give given for the 
   * passed in source code and price. 
   * 
   * @param sourceCode String source code associated with this order
   * @param price double of the product
   * @return String The number of reward points to award*/
    public String retrievePromotionValue(String sourceCode, double price) 
          throws Exception{

        logger.debug("retrievePromotionValue: " + sourceCode + " " + price);

        UpdateOrderDAO uoDAO = new UpdateOrderDAO(this.con);
        Map outputs = uoDAO.getPromotionProgramRS(sourceCode, price);
        
        String milesPoints = (String) outputs.get("OUT_MILES_POINTS");
        if (milesPoints == null) milesPoints = "0";
        String rewardType = (String) outputs.get("OUT_REWARD_TYPE");

        logger.debug("rewardType: " + rewardType + " - value: " + milesPoints);

        return milesPoints;
    }


    /**
     * Depending on the type passed in, this method returns either
     * an occasion description or a country description.
     *
     * @param String Occasion or country code
     * @param int (GET_OCCASION_description or GET_COUNTRY_description)
     * @returns String description
     */
    public String getdescriptionForCode(String code, int searchType) throws Exception
    {
        String ret = null;

        switch(searchType)
        {
            case GET_OCCASION_description:
                ret = getOccasiondescription(code);
                break;
            case GET_COUNTRY_description:
                ret = getCountrydescription(code);
                break;
            default:
                break;
        }

        return ret;
    }

    /**
     * This methods returns the description for the passed in occasion code.
     *
     * @param String occasion id
     * @returns String occasion description
     * @throws Exception
     */
    private String getOccasiondescription(String occasionId) throws Exception
    {

        UpdateOrderDAO uoDAO = new UpdateOrderDAO(this.con);

        String ret = "";

        Document dataResponse = (Document) uoDAO.getOccasionByIdXML(occasionId);

        String xpath = "//OCCASION";
        NodeList nl = DOMUtil.selectNodes(dataResponse,xpath);

        if (nl.getLength() > 0)
        {
            Element occasionData = (Element) nl.item(0);
            ret = occasionData.getAttribute("description");
        }

        return ret;
    }


    /**
     * This method calls a stored procedure to get all the available ship methods
     * for a product.  The stored procedure returns the ship method code, ship
     * method description and the shipping cost.  This method will take all the data
     * returned from the stored procedure and create objects from it..the objects
     * are then placed into a hashmap and returned.
     *
     * @param String productId
     * @param String standardPrice
     * @param String shippingKeyId
     * @returns Hashmap of ShippingMethod objects
     * @thorws Exception
     */
//********************************************************************************************
// TESTED
//********************************************************************************************
    public HashMap getProductShippingMethods(String productId, String standardPrice, String shippingKeyId) throws Exception
    {

        UpdateOrderDAO uoDAO = new UpdateOrderDAO(this.con);

        HashMap ret = new HashMap();

        try
        {

        Document dataResponse = (Document) uoDAO.getShippingMethodXML(productId, shippingKeyId, standardPrice);

        NodeList nl = DOMUtil.selectNodes(dataResponse,"//SHIPMETHOD");

        Element shipMethodElement = null;
        ShippingMethod shipMethod = null;
        for(int i = 0; i < nl.getLength(); i++)
        {
            shipMethod = new ShippingMethod();
            shipMethodElement = (Element) nl.item(i);
            shipMethod.setCode(shipMethodElement.getAttribute("shipmethodcode"));
            shipMethod.setDescription(shipMethodElement.getAttribute("shipmethoddesc"));
            shipMethod.setDeliveryCharge(new BigDecimal(shipMethodElement.getAttribute("shipcost")));

            ret.put(shipMethod.getDescription(), shipMethod);
        }

        }
        finally
        {
        }

        return ret;
    }


    /**
     * This method returns the country description for the passed in
     * country code.
     *
     * @param String country code
     * @returns String country description
     * @throws Exception
     */
    private String getCountrydescription(String code) throws Exception
    {

        UpdateOrderDAO uoDAO = new UpdateOrderDAO(this.con);
        String ret = "";

        try
        {

          Document dataResponse = (Document)uoDAO.getCountryMasterByIdXML(code);

          String xpath = "countryLookup/searchResults/searchResult";
          NodeList nl = DOMUtil.selectNodes(dataResponse,xpath);

          if (nl.getLength() > 0)
          {
              Element occasionData = (Element) nl.item(0);
              ret = occasionData.getAttribute("countryname");
          }

        }
        finally
        {
        }

        return ret;
    }

    /**
      * This method takes in the XML document which contains all the products
      * for the selected category and the XML document which contains only the
      * products for the current page.  The method checks if any products
      * have been removed from the current page XML document do to product
      * unavailablity.  This method will remove unavailable products for the
      * xml which contains the entire product list.
      *
      * This method also copies the page data from one XML document to the other.
      *
      * @param Document searchResultsIndex Paginated list of products
      * @param Document searchResultsPerpage Non-paginated list of products
      * @param int currentPage
      * @param int pageSize
      * @returns void
      * @throws Exception
     */
//********************************************************************************************
// TESTED
//********************************************************************************************
    public static void rebuildSearchResultsIndex(Document searchResultsIndex,
                                               Document searchResultsPerPage,
                                               int currentPage,
                                               int pageSize) throws Exception{

      // assumes that the index is pre-paginated
      NodeList nodesOnIndex =
          DOMUtil.selectNodes(searchResultsIndex,"//PRODUCT[@page='"+ currentPage +"']");
      int nodeCountOnIndex = nodesOnIndex.getLength();

      // assumes the search results have not been paginated
      NodeList nodesOnsearchResultsPerPage =
          DOMUtil.selectNodes(searchResultsPerPage,"//PRODUCT");
      int nodeCountOnsearchResultsPerPage = nodesOnsearchResultsPerPage.getLength();

       // check to see if any products have been made unavailable
        if (nodeCountOnIndex > nodeCountOnsearchResultsPerPage)
        {
            // remove nodes from the search results index that are missing in the search results
            Element nodeOnIndex = null;
            String productID = null;
            Element productsNodeOnsearchResultsPerPage = (Element)(DOMUtil.selectNodes(searchResultsPerPage,"//PRODUCT").item(0));
            NodeList productNodeOnsearchResultsPerPageNL = null;
            Element productNodeOnSearchResultsPerPage = null;
            Element importedNode = null;

            for (int i = 0; i < nodeCountOnIndex; i++)
            {
                nodeOnIndex = (Element) nodesOnIndex.item(i);

                if (nodeOnIndex != null)
                {
                    // get the product id from the index
                    productID = nodeOnIndex.getAttribute("productid");

                    // check to see if the product id exists on search results
                    productNodeOnsearchResultsPerPageNL =
                        DOMUtil.selectNodes(searchResultsPerPage,"//PRODUCT[@productid='"+ productID +"']");


                    // if the product id on the index cannot be found on the search results
                    if (productNodeOnsearchResultsPerPageNL.getLength() == 0)
                    {
                        nodeOnIndex.getParentNode().removeChild(nodeOnIndex);
                    }

                }
             }
        }

      // insert the pagination node on the search results DOM
      //paginate(searchResultsPerPage, currentPage, pageSize);
        Element pageData = (Element) DOMUtil.selectNodes(searchResultsIndex,"//pageData").item(0);
        if (pageData != null)  {
            Element importedNode = (Element) searchResultsPerPage.importNode(pageData, true);
            searchResultsPerPage.getDocumentElement().appendChild(importedNode);
        }
        else {
            logger.debug("Element 'pageData' not found");
        }
    }


  /**
   * This method takes in a product list and adds page number elements
   * to each of the products.
   *
   * @params Document doc An xml document containing all the products which are available
   * @params int currentPage, the page that the user is currently on
   * @params int pageSize the number of products which can be displayed on a page
   * @return total page count
   */
//********************************************************************************************
// TESTED
//********************************************************************************************
  public static long paginate(Document doc,  int currentPage, int pageSize) throws Exception{

      NodeList nodeList =   DOMUtil.selectNodes(doc,"//PRODUCT");
      int page = 1, totalPages = 1; // start at 1
      int counter = 0;

      Element node = null;

      for (int i = 0; i < nodeList.getLength(); i++,counter++)  {
          // control pagination
          if (counter >= pageSize)  {
              page++; // increment the page number
              totalPages = page;
              counter = 0; // reset counter
          }

          node = (Element) nodeList.item(i);
          // rewrite the page attribute
          node.setAttribute("page", String.valueOf(page));
          // set the status attribute to available
          node.setAttribute("status", "available");

      }

      // check if the current page is greater that the total page count
      // reset it to the last page
      if (currentPage > totalPages)  {
          currentPage = totalPages;
      }

      // set up pagination nodes on the search results
      Element pageData = (Element) DOMUtil.selectNodes(doc,"//pageData").item(0);
      Element data = null;

      if (pageData == null)  {
          pageData = doc.createElement("pageData");
          // total pages
          data = (Element) doc.createElement("data");
          data.setAttribute("name", "totalPages" );
          data.setAttribute("value", String.valueOf(totalPages) );
          pageData.appendChild(data);

          // current page
          data = (Element) doc.createElement("data");
          data.setAttribute("name", "currentPage" );
          data.setAttribute("value", String.valueOf(currentPage) );
          pageData.appendChild(data);

          // append the page data node to the root node
          doc.getDocumentElement().appendChild(pageData);
      }
      else {
          // total pages
          data = (Element) DOMUtil.selectNodes(doc,"//pageData/data[@name='totalPages']").item(0);
          data.setAttribute("value", String.valueOf(totalPages) );
          // current page
          data = (Element) DOMUtil.selectNodes(doc,"//pageData/data[@name='currentPage']").item(0);
          data.setAttribute("value", String.valueOf(currentPage) );
      }

      return totalPages;
  }


    /**
     * @param Document searchResultsIndex List of available products to display in list
     * @param String strCurrentPage Current page number
     * @param String pricePointId Price filtering code..if any
     * @param String sourceCode Current source code
     * @param boolean isDomesticFlag domestic flag
     * @param String sendToCustomerZipCode recipient zip code
     * @param String sendToCountry Recipient country
     * @param String daysOut Value from Global Params table
     */
//********************************************************************************************
// TESTED - this method is called from getProductsByCategory.  It takes multiple product-ids
//********************************************************************************************
    public Document getProductsByIDs(Document searchResultsIndex,
          String strCurrentPage, String pricePointId, String sourceCode,
          boolean isDomesticFlag,String sendToCustomerZipCode, String sendToCountry,
          String daysOut, boolean customOrderFlag, String deliveryDate) throws Exception
    {

        //Get the list of products for the current page
        NodeList nodeList=null;
        String strProductId;
        String strPage;

        Element node = null;
        Document searchResults = null;

        StringBuffer sb = new StringBuffer();
        HashMap productOrderMap = new HashMap();
        String productOrder = null;
        String productId = null;

        nodeList=DOMUtil.selectNodes(searchResultsIndex,"//PRODUCT");
        if (!(nodeList.getLength() > 0))
          nodeList=DOMUtil.selectNodes(searchResultsIndex,"//product");

        for (int i = 0; i < nodeList.getLength(); i++ )
        {
            // control pagination
            node = (Element) nodeList.item(i);
            strProductId = node.getAttribute("novatorid");
            productId = node.getAttribute("productid");
            strPage = node.getAttribute("page");
            productOrder = node.getAttribute("displayorder");
            productOrderMap.put(productId, productOrder);

            //if this product is part of hte current page add it to the string buffer
            if( strProductId!=null && strPage!=null && strPage.compareTo(strCurrentPage)==0 )
            {
                sb.append("'");
                sb.append(strProductId);
                sb.append("'");
                sb.append(",");
            }
        }


        //delete trailing comma if the string buffer contains data
        if(sb.length() > 0)
        {
            sb.deleteCharAt(sb.length()-1);
        }
        else
        {
            //buffer does not contain anything, put in an empty quoted string
            sb.append("''");
        }

        UpdateOrderDAO uoDAO = new UpdateOrderDAO(this.con);

      try{

        //price point will have a value if the user choose to filter product list by price
        if(pricePointId != null && pricePointId.length() == 0)
        {
            pricePointId = null;
        }

        //set a domestic/international value
        String domIntFlag = isDomesticFlag?COMConstants.CONS_DELIVERY_TYPE_DOMESTIC:COMConstants.CONS_DELIVERY_TYPE_INTERNATIONAL;

        //set the zip code
        String zipCode = isDomesticFlag?sendToCustomerZipCode:null;

        //set the delivery end date
        Calendar deliveryEndDate = Calendar.getInstance();
        deliveryEndDate.add(Calendar.DATE, Integer.parseInt(daysOut));
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
        String sDeliveryEndDate = dateFormat.format(deliveryEndDate.getTime());

        //this procedure gets the detailed information about each product in the list
        searchResults = (Document) uoDAO.getProductXML(sb.toString(),pricePointId,sourceCode,domIntFlag,zipCode,
                                                          sDeliveryEndDate, sendToCountry, deliveryDate);

        //append global params
        OEParameters oeGlobalParms = getGlobalParms();

        //create an xml object for the oeGlobalParms
        Document globalXML = (Document) DOMUtil.getDefaultDocument();

        //generate the top node
        Element stateElement = globalXML.createElement("OEParameters");
        //and append it
        globalXML.appendChild(stateElement);

        Document oeGlobalDoc = DOMUtil.getDocument(oeGlobalParms.toXML(0));
        globalXML.getDocumentElement().appendChild(globalXML.importNode(oeGlobalDoc.getFirstChild(), true));

        //append it to the main document.
        DOMUtil.addSection(searchResults, globalXML.getChildNodes());

        //append holidays for the recipient country
        Document holidaysByCountryIdXML = (Document) uoDAO.getHolidayByIdXML(sendToCountry);
        DOMUtil.addSection(searchResults, holidaysByCountryIdXML.getChildNodes());

        //append ship methods
        Document allShippingMethodsXML = (Document) uoDAO.getAllShippingMethodXML();
        DOMUtil.addSection(searchResults, allShippingMethodsXML.getChildNodes());

        //get delivery data ranges.  These are date ranges in which products cannot be delivered.
        Document deliverDateRangeXML = (Document) uoDAO.getDeliveryDateRangeXML();
        DOMUtil.addSection(searchResults, deliverDateRangeXML.getChildNodes());


        //For each of the products returned from the product detail query set the
        //display order.  The display order is obtained from the XML which was passed
        //into this method.  Basically the displayOrder id being transferred from one
        //XML document to another.
        nodeList=DOMUtil.selectNodes(searchResults,"//PRODUCT");
        Element element = null;
        productId = null;
        for(int i = 0; i < nodeList.getLength(); i++)
        {
            element = (Element)nodeList.item(i);
            productId = element.getAttribute("productid");
            element.setAttribute("displayorder", (String)productOrderMap.get(productId));
        }


        }
        finally
        {
        }
        return searchResults;
    }


    /**
     * This method retrieves the row of data in the FTD_APPS.GLOBAL_PARMS table
     * and put all the data into OEParameter object.
     * @returns OEParameters
     * @throws Exception */
//********************************************************************************************
// TESTED
//********************************************************************************************
    public OEParameters getGlobalParms() throws Exception
    {
      OEParameters oeGlobalParms = new OEParameters();

      UpdateOrderDAO uoDAO = new UpdateOrderDAO(this.con);

      CachedResultSet rs;
      String name;
      String value;

      //Call method in the DAO
      rs = (CachedResultSet)uoDAO.getGlobalParmByIdRS(COMConstants.CONS_FTDAPPS_PARMS);

      while(rs.next())
      {
        name = (String) rs.getObject("name");
        value = (String) rs.getObject("value");

        if (name.equalsIgnoreCase("ALLOW_SUBSTITUTION"))
        {}
        else if (name.equalsIgnoreCase("CANADIAN_EXCHANGE_RATE"))
        {}
        else if (name.equalsIgnoreCase("CHECK_JC_PENNEY"))
        {}
        else if (name.equalsIgnoreCase("DELIVERY_DAYS_OUT"))
          oeGlobalParms.setDeliveryDaysOut(Integer.parseInt(value));
        else if (name.equalsIgnoreCase("DISPATCH_ORDER_FLAG"))
        {}
        else if (name.equalsIgnoreCase("EXOTIC_CUTOFF"))
          oeGlobalParms.setExoticCutoff(value);
        else if (name.equalsIgnoreCase("FRESHCUTS_CUTOFF"))
          oeGlobalParms.setFreshCutCutoff(value);
        else if (name.equalsIgnoreCase("FRESHCUTS_SAT_CHARGE"))
        {}
        else if (name.equalsIgnoreCase("FRESHCUTS_SAT_CHARGE_TRIGGER"))
        {}
        else if (name.equalsIgnoreCase("FRESHCUTS_SVC_CHARGE"))
        {
          if(value != null && !value.equalsIgnoreCase(""))
          {
            oeGlobalParms.setFreshCutSrvcCharge(new BigDecimal(value));
          }
        }
        else if (name.equalsIgnoreCase("FRESHCUTS_SVC_CHARGE_TRIGGER"))
          oeGlobalParms.setFreshCutSrvcChargeTrigger(value);
        else if (name.equalsIgnoreCase("FRIDAY_CUTOFF"))
          oeGlobalParms.setFridayCutoff(value);
        else if (name.equalsIgnoreCase("GLOBAL_PARMS_KEY"))
        {}
        else if (name.equalsIgnoreCase("GNADD_DATE"))
        {
          SimpleDateFormat sdfInput = new SimpleDateFormat("MMM dd, yyyy");
          Date d = sdfInput.parse(value);

          oeGlobalParms.setGNADDDate(d);

          //oeGlobalParms.setGNADDDate(sdfInput.parse(value));
        }
        else if (name.equalsIgnoreCase("GNADD_LEVEL"))
          oeGlobalParms.setGNADDLevel(value);
        else if (name.equalsIgnoreCase("INTL_CUTOFF"))
          oeGlobalParms.setIntlCutoff(value);
        else if (name.equalsIgnoreCase("INTL_OVERRIDE_DAYS"))
          oeGlobalParms.setIntlAddOnDays(Integer.parseInt(value));
        else if (name.equalsIgnoreCase("INTL_OVERRIDE_LEVEL"))
        {}
        else if (name.equalsIgnoreCase("MONDAY_CUTOFF"))
          oeGlobalParms.setMondayCutoff(value);
        else if (name.equalsIgnoreCase("SATURDAY_CUTOFF"))
          oeGlobalParms.setSaturdayCutoff(value);
        else if (name.equalsIgnoreCase("SPECIALITYGIFT_CUTOFF"))
          oeGlobalParms.setSpecialtyGiftCutoff(value);
        else if (name.equalsIgnoreCase("SPECIAL_SVC_CHARGE"))
        {
          if(value != null && !value.equalsIgnoreCase(""))
          {
            oeGlobalParms.setSpecialSrvcCharge(new BigDecimal(value));
          }
        }
        else if (name.equalsIgnoreCase("SUNDAY_CUTOFF"))
          oeGlobalParms.setSundayCutoff(value);
        else if (name.equalsIgnoreCase("THURSDAY_CUTOFF"))
          oeGlobalParms.setThursdayCutoff(value);
        else if (name.equalsIgnoreCase("TUESDAY_CUTOFF"))
          oeGlobalParms.setTuesdayCutoff(value);
        else if (name.equalsIgnoreCase("WEDNESDAY_CUTOFF"))
          oeGlobalParms.setWednesdayCutoff(value);

        //names specific to Modify Orders
        else if (name.equalsIgnoreCase("MOD_ORDER_CHOCOLATES_AVAILABLE"))
          oeGlobalParms.setMOChocolatesAvailable(value);
        else if (name.equalsIgnoreCase("MOD_ORDER_ORDER_MAX_DAYS"))
        {}
        //pre-existing variables that were added during Modify Order
        else if (name.equalsIgnoreCase("DELIVERY_DAYS_OUT_MAX"))
          oeGlobalParms.setDeliveryDaysOutMax(Integer.parseInt(value));
        else if (name.equalsIgnoreCase("DELIVERY_DAYS_OUT_MIN"))
          oeGlobalParms.setDeliveryDaysOutMin(Integer.parseInt(value));
        

      } //end-while

      return oeGlobalParms;
    }



    /**
     * This method is used to sort the product list.  It uses the
     * passed in xslFile to sort the passed in XML document.
     *
     * @param File sortXSLFile
     * @param Document xml
     * @returns Document, sort products
     * @throws Exception*/
    public Document sortProductList(File sortXSLFile, Document xml)
            throws Exception
    {
      Document result = null;

      Document fixedXml = fixXMLByRemovingNullAttributes(xml);

      TransformerFactory factory = TransformerFactory.newInstance();
      //String sortSheet = xslLocation + sortType + ".xsl";


      Templates pss = factory.newTemplates(new StreamSource(sortXSLFile));
      //Templates pss = factory.newTemplates(new StreamSource(new File(sortSheet)));
      Transformer transformer = pss.newTransformer();

      StringWriter sortOutput = new StringWriter();

      transformer.transform(new DOMSource(fixedXml), new StreamResult(sortOutput));

      result = createDocument(sortOutput.toString());

      return result;

    }

    /**
     * Performs a product list search with the paramers passed.  There is a large
     * amount of business logic in the stored procdures.  For each product the
     * procedure returns the product id, standard price and the display order.
     *
     * See the stored procudure documentation and code for more infomration.  The
     * following text was copied from the stored procdure comment section:
     *
      -- description:   Searches PRODUCT_MASTER by the supplied index ID using PRODUCT_INDEX_XREF.
      --                Price point ID specifies a desired price range within which returned
      --                products should fall (by standard price).
      --
      --                Source code, domestic/international flag (D or I) and the recipient
      --                zip code must be provided.  Some of the product information is source
      --                code dependent or depends on domestic vs. international.  Zip code
      --                is used to exclude products not sold in the recipient's state.

     * @param String queryName Contains the data constant referring to a search procedure
     * @param String sourceCode current source code
     * @param String indexId category id
     * @param zipCode the zip code
     * @param String pricePointId Price filtering info
     * @param String countryID Recipient country id
     * @param String deliveryEndDate
     * @param String domesticFlag I=Internationial, D=Domestic
     * @exception Exception if a SQL Exception occures
     * @author Jeff Penney
     *
     */
//********************************************************************************************
// TESTED
//********************************************************************************************
    public static Document performProductListSearch(Connection connection,
        String queryName, String sourceCode, String indexID, String zipCode, String pricePointID,
        String countryID, String deliveryEndDate, String domesticFlag)
        throws Exception
    {
        Document ret = null;
        Document result = null;

        UpdateOrderDAO uoDAO = new UpdateOrderDAO(connection);

        try
        {

        //if blank null it out
        if (zipCode != null && zipCode.length() <=0)
        {
          zipCode = null;
        }
        if (indexID != null && indexID.length() <=0)
        {
          indexID = null;
        }

        // All searches
                //this procedure gets the detailed information about each product in the list
        result = (Document) uoDAO.getProductListXML(indexID,pricePointID,sourceCode,domesticFlag,
                                                        zipCode, deliveryEndDate, countryID);

        }
        finally
        {
        }

        return result;
    }


    /**
     * Performs a product list search with the paramers passed.  There is a large
     * amount of business logic in the stored procdures.  For each product the
     * procedure returns the product id, standard price and the display order.
     *
     * See the stored procudure documentation and code for more infomration.  The
     * following text was copied from the stored procdure comment section:
     *
      -- description:   Searches PRODUCT_MASTER by the supplied index ID using PRODUCT_INDEX_XREF.
      --                Price point ID specifies a desired price range within which returned
      --                products should fall (by standard price).
      --
      --                Source code, domestic/international flag (D or I) and the recipient
      --                zip code must be provided.  Some of the product information is source
      --                code dependent or depends on domestic vs. international.  Zip code
      --                is used to exclude products not sold in the recipient's state.

     * @param String queryName Contains the data constant referring to a search procedure
     * @param String sourceCode current source code
     * @param String indexId category id
     * @param zipCode the zip code
     * @param String pricePointId Price filtering info
     * @param String countryID Recipient country id
     * @param String deliveryEndDate
     * @param String domesticFlag I=Internationial, D=Domestic
     * @exception Exception if a SQL Exception occures
     * @author Jeff Penney
     *
     */
//********************************************************************************************
// New
//********************************************************************************************
    public static Document performProductListSearchForKeywordSearch(Connection connection,
        String searchKeywordList, String pricePointID, String sourceCode,
        String domesticFlag, String zipCode, String deliveryEndDate,
        String countryID)
        throws Exception
    {
        Document ret = null;
        Document result = null;

        UpdateOrderDAO uoDAO = new UpdateOrderDAO(connection);

        try
        {

        //if blank null it out
        if (zipCode != null && zipCode.length() <=0)
        {
          zipCode = null;
        }

        // All searches
                //this procedure gets the detailed information about each product in the list
        result = (Document) uoDAO.getProductListForKeywordSearchXML
                                (searchKeywordList, pricePointID, sourceCode, domesticFlag,
                                 zipCode, deliveryEndDate, countryID);

        }
        finally
        {
        }

        return result;
    }


    /**
     * This method tokenizes a Map into a String
     * @param inMap A Map of search criteria that will be converted to a String
     * @author Jeff Penney
     *
     */
    public static String getSearchCriteriaString(Map inMap)
    {
        StringBuffer ret = new StringBuffer();

        Set keySet = inMap.keySet();
        Iterator it = keySet.iterator();
        String key = null;
        String value = null;
        while(it.hasNext())
        {
            key = (String)it.next();
            value = (String)inMap.get(key);
            if(value == null)
            {
                value = "null";
            }

            ret.append(key).append(":").append(value).append(" ");
        }

        return ret.toString();
    }

    /**
     * This method tokenizes a String into a Map
     * @param inString A String of search criteria that will be converted to a Map
     * @author Jeff Penney
     *
     */
    public static Map getSearchCriteriaMap(String inString)
    {
        Map ret = new HashMap();
        StringTokenizer tokenizer1 = new StringTokenizer(inString, " ");
        String token = null;
        StringTokenizer tokenizer2 = null;
        String key = null;
        String value = null;

        while(tokenizer1.hasMoreTokens())
        {
            token = tokenizer1.nextToken();

            tokenizer2 = new StringTokenizer(token, ":");
            key = tokenizer2.nextToken();
            value = tokenizer2.nextToken();
            if(value.equalsIgnoreCase("null"))
            {
                value = null;
            }
            ret.put(key, value);
        }
        return ret;
    }

    /**
     * This method sets flags in in the ProductFlagsVO that indicate
     * what messages should be displayed to the user related to
     * product availablity.
     *
     * @param OEDeliveryDateParam delparm Delivery Dates
     * @param List shipMethods Possible ship methods
     * @param ProductFlagsVO flagVO Flags
     * @param String sendToState Recipient state
     * @param String sendToCountry Recipient country
     * @return void
     **/
    private void checkForSDGAvailable(OEDeliveryDateParm delParms, List shipMethods,
            ProductFlagsVO flagVO, String sendToState, String sendToCountry)
    {
        String codifiedAvailable = delParms.getCodifiedAvailable();
        if(codifiedAvailable == null) codifiedAvailable = "";
        String codifiedProduct = delParms.getCodifiedProduct();
        if(codifiedProduct == null) codifiedProduct = "";
        String codifiedSpecial = delParms.getCodifiedSpecialFlag();
        if(codifiedSpecial == null) codifiedSpecial = "";
        String codifiedDeliverable = delParms.getCodifiedDeliverable();
        if(codifiedDeliverable == null) codifiedDeliverable = "";

        if(codifiedProduct.equalsIgnoreCase(GeneralConstants.YES) &&
           (delParms.getProductType().equalsIgnoreCase(GeneralConstants.OE_PRODUCT_TYPE_SDG) ||
            delParms.getProductType().equalsIgnoreCase(GeneralConstants.OE_PRODUCT_TYPE_SDFC)))
        {
            // if the delivery country is Canada, the Virgin Islands or
            // Puerto Rico and thier is no codified florist then set the
            // displayProductUnavailable flag
            if(codifiedAvailable.equalsIgnoreCase(GeneralConstants.NO) &&
               (sendToCountry.equalsIgnoreCase(GeneralConstants.COUNTRY_CODE_CANADA_CA) ||
               sendToState.equalsIgnoreCase(GeneralConstants.STATE_CODE_PUERTO_RICO) ||
               sendToState.equalsIgnoreCase(GeneralConstants.STATE_CODE_VIRGIN_ISLANDS)) &&
               codifiedDeliverable.equalsIgnoreCase("N"))
            {
                flagVO.setDisplayProductUnavailable(GeneralConstants.YES);
                flagVO.setDisplayNoProduct(GeneralConstants.NO);
            }
            // if the delivery country is Canada, the Virgin Islands or
            // Puerto Rico and thier is no florist coverage then set the
            // displayNoProduct flag
            else if(codifiedAvailable.equalsIgnoreCase(GeneralConstants.NO) &&
                    (sendToCountry.equalsIgnoreCase(GeneralConstants.COUNTRY_CODE_CANADA_CA) ||
                     sendToState.equalsIgnoreCase(GeneralConstants.STATE_CODE_PUERTO_RICO) ||
                     sendToState.equalsIgnoreCase(GeneralConstants.STATE_CODE_VIRGIN_ISLANDS)) &&
                     codifiedDeliverable.equalsIgnoreCase("D"))
            {
                flagVO.setDisplayNoProduct(GeneralConstants.YES);
                flagVO.setDisplayProductUnavailable(GeneralConstants.NO);
            }
            // This case is when the product only has two day delivery (ex. AK, HI)
            else if(codifiedAvailable.equalsIgnoreCase(GeneralConstants.NO) && shipMethods.size() == 1 &&
                    shipMethods.contains(GeneralConstants.DELIVERY_TWO_DAY_CODE))
            {
                flagVO.setDisplayCodifiedFloristHasTwoDayDeliveryOnly(GeneralConstants.YES);
            }
            // if the zip code can still take orders but just not for this product
            else if(codifiedAvailable.equalsIgnoreCase(GeneralConstants.NO) &&
                    codifiedDeliverable.equalsIgnoreCase("N") && shipMethods.size() > 0)
            {
                flagVO.setDisplayNoCodifiedFloristHasCommonCarrier(GeneralConstants.YES);
                flagVO.setDisplayNoProduct(GeneralConstants.NO);
            }
            // if the zip code is shut down
            else if(codifiedAvailable.equalsIgnoreCase(GeneralConstants.NO) &&
                    codifiedDeliverable.equalsIgnoreCase("D") && shipMethods.size() > 0)
            {
                flagVO.setDisplayNoFloristHasCommonCarrier(GeneralConstants.YES);
                flagVO.setDisplayNoProduct(GeneralConstants.NO);
            }
            // No florist and no carrier delivery methods
            else if(codifiedAvailable.equalsIgnoreCase(GeneralConstants.NO) && shipMethods.size() == 0)
            {
                flagVO.setDisplayProductUnavailable(GeneralConstants.YES);
            }
            else
            {
                flagVO.setDisplayNoProduct("N");
                flagVO.setDisplayNoCodifiedFloristHasCommonCarrier("N");
                flagVO.setDisplayNoFloristHasCommonCarrier("N");
            }
        }
    }

    /**
     * This method gets the Delivery dates for a line item in an order. This
     * method contains the business logic which determines when a product is
     * deliverable.
     *
     * @param Document document The XML document containing product information
     * @param String productId The product ID that the delivery dates will be calculated for
     * @param boolean getFirstAvailableDate Determines if all delivery dates should be calculated or just the first available
     * @param String zipCode
     * @param boolean isDomesticFlag
     * @param String domesticServiceFee
     * @param String internationialServiceFee
     * @param ProductFlagsVO flagVO
     * @param String sendToState
     * @param String sendToCountry
     * @param String sourceCode
     * @return A List of Delivery date objects
     * @throws Exception
     * @author Jeff Penney
     *
     */
    public List getItemDeliveryDates(Document document, String productId,
          boolean getFirstAvailableDate, String zipCode, boolean isDomesticFlag,
          String domesticServiceFee, String internationalServiceFee,
          ProductFlagsVO flagVO, String sendToState, String sendToCountry, String sourceCode,
          boolean customOrderFlag)
           throws Exception
    {
        List deliveryDates = null;
        try
        {
            SimpleDateFormat sdfInput = new SimpleDateFormat("MM/dd/yyyy");
            DeliveryDateUTIL deliveryDateUtil = new DeliveryDateUTIL();
            SearchUtil SearchUtil = new SearchUtil(this.con);
            OEDeliveryDateParm deliveryDateParm = new OEDeliveryDateParm();
            Map holidayMap = null;

            String zipSundayFlag = null;

            flagVO.setDisplayFloral(COMConstants.CONS_NO);
            flagVO.setDisplayNoCodifiedFloristHasCommonCarrier(COMConstants.CONS_NO);
            flagVO.setDisplayNoFloristHasCommonCarrier(COMConstants.CONS_NO);
            flagVO.setDisplayNoProduct(COMConstants.CONS_NO);
            flagVO.setDisplayProductUnavailable(COMConstants.CONS_NO);
            flagVO.setDisplaySpecialtyGift(COMConstants.CONS_NO);
            flagVO.setDisplayCodifiedFloristHasTwoDayDeliveryOnly(COMConstants.CONS_NO);

            // parse global parameters from the returned XML
            OEParameters oeGlobalParms = getGlobalParms();

            // retrieve the holiday date information for the country
            holidayMap = getHolidays(document);

            // retrieve the product information
            String xpath = "//PRODUCTS/PRODUCT[@productid = '" + productId + "']";
            NodeList nl = DOMUtil.selectNodes(document,xpath);
            boolean floralServiceChargeSet = false;
            if (nl.getLength() > 0)
            {
                double currentPrice;
                double currentDiscountAmount;

                String standardDiscountAmount = null;
                String deluxeDiscountAmount = null;
                String premiumDiscountAmount = null;
                String cntryAddOnDays = null;

                Element element = (Element) nl.item(0);

                deliveryDateParm.setConn(this.con);
                deliveryDateParm.setGlobalParms(oeGlobalParms);
                deliveryDateParm.setDeliverTodayFlag(Boolean.TRUE);
                deliveryDateParm.setSundayDelivery(Boolean.TRUE);
                deliveryDateParm.setProductType(element.getAttribute("producttype"));
                deliveryDateParm.setProductSubType(element.getAttribute("productsubtype"));
                deliveryDateParm.setProductId(element.getAttribute("productid"));
                cntryAddOnDays = element.getAttribute("addondays");

                if ( (cntryAddOnDays != null) &&
                     (!cntryAddOnDays.trim().equalsIgnoreCase("")) )
                {
                    deliveryDateParm.setCntryAddOnDays(Integer.parseInt(cntryAddOnDays));
                }
                else
                {
                    deliveryDateParm.setCntryAddOnDays(0);
                }

                // Use zip code information if exists
                if ( zipCode != null && !zipCode.equalsIgnoreCase("") )
                {
                    deliveryDateParm.setZipTimeZone(element.getAttribute("timezone"));
                    deliveryDateParm.setZipCodeGNADDFlag(element.getAttribute("zipgnaddflag").trim());
                    deliveryDateParm.setZipCodeGotoFloristFlag(element.getAttribute("zipgotofloristflag").trim());
                    zipSundayFlag = element.getAttribute("zipsundayflag").trim();
                }
                // zip code information does not exist, use default values
                else {
                    deliveryDateParm.setZipTimeZone(COMConstants.CONS_TIMEZONE_DEFAULT);
                    deliveryDateParm.setZipCodeGNADDFlag(COMConstants.CONS_NO);
                    deliveryDateParm.setZipCodeGotoFloristFlag(COMConstants.CONS_YES);
                    zipSundayFlag = COMConstants.CONS_YES;
                }

                // zip code does support Sunday delivery
                if ( zipSundayFlag != null &&
                     zipSundayFlag.equalsIgnoreCase(COMConstants.CONS_YES) )
                {
                    // codified products follow different rules under normal processing
                    if ( element.getAttribute("codifiedproduct") != null &&
                         element.getAttribute("codifiedproduct").equalsIgnoreCase(COMConstants.CONS_YES) &&
                         oeGlobalParms.getGNADDLevel().equalsIgnoreCase("0") )
                    {
                        // Valid valued are: N-codified; NA-not codified/not available
                        //                   Y-codified (No Sunday); S-codified (Sunday)
                        if ( element.getAttribute("codifiedavailable") != null &&
                             element.getAttribute("codifiedavailable").equalsIgnoreCase("S") )
                        {
                            deliveryDateParm.setSundayDelivery(Boolean.TRUE);
                        }
                        else
                        {
                            deliveryDateParm.setSundayDelivery(Boolean.FALSE);
                        }
                    }
                }
                // zip code does NOT support Sunday delivery
                else
                {
                   deliveryDateParm.setSundayDelivery(Boolean.FALSE);
                }

                // Exotic floral items cannot be delivered same day
                if ( element.getAttribute("productsubtype").equalsIgnoreCase(COMConstants.CONS_PRODUCT_SUBTYPE_EXOTIC) ||
                     element.getAttribute("producttype").equalsIgnoreCase(COMConstants.CONS_PRODUCT_TYPE_FRECUT) ||
                     element.getAttribute("producttype").equalsIgnoreCase(COMConstants.CONS_PRODUCT_TYPE_SPEGFT) ||
                     element.getAttribute("deliverytype").equalsIgnoreCase(COMConstants.CONS_DELIVERY_TYPE_INTERNATIONAL) )
                {
                    deliveryDateParm.setDeliverTodayFlag(Boolean.FALSE);
                }

                // set the exception dates for product
                deliveryDateParm.setExceptionCode(element.getAttribute("exceptioncode"));
                try {
                    deliveryDateParm.setExceptionFrom(sdfInput.parse(element.getAttribute("exceptionstartdate")));
                    deliveryDateParm.setExceptionTo(sdfInput.parse(element.getAttribute("exceptionenddate")));
                }
                catch (Exception e) {
                }

                // set the vendor no delivery dates for product
                HashMap vendorNoDeliverFrom = new HashMap();
                HashMap vendorNoDeliverTo = new HashMap();
                for ( int y=1; y < 7; y++ )
                {
                    String vendorFrom = element.getAttribute("vendordelivblockstart" + y);

                    if ( vendorFrom != null && !vendorFrom.trim().equalsIgnoreCase("") )
                    {
                        vendorNoDeliverFrom.put(String.valueOf(y-1), sdfInput.parse(element.getAttribute("vendordelivblockstart" + y)));
                        vendorNoDeliverTo.put(String.valueOf(y-1), sdfInput.parse(element.getAttribute("vendordelivblockend" + y)));
                    }
                }

                if ( vendorNoDeliverFrom.size() > 0 )
                {
                    deliveryDateParm.setVendorNoDeliverFlag(Boolean.TRUE);
                    deliveryDateParm.setVendorNoDeliverFrom(vendorNoDeliverFrom);
                    deliveryDateParm.setVendorNoDeliverTo(vendorNoDeliverTo);
                }
                else
                {
                    deliveryDateParm.setVendorNoDeliverFlag(Boolean.FALSE);
                }

                // set the vendor no ship dates for product
                HashMap vendorNoShipFrom = new HashMap();
                HashMap vendorNoShipTo = new HashMap();
                for ( int y=1; y < 7; y++ )
                {
                    String vendorFrom = element.getAttribute("vendorshipblockstart" + y);

                    if ( vendorFrom != null && !vendorFrom.trim().equalsIgnoreCase("") )
                    {
                        vendorNoShipFrom.put(String.valueOf(y-1), sdfInput.parse(element.getAttribute("vendorshipblockstart" + y)));
                        vendorNoShipTo.put(String.valueOf(y-1), sdfInput.parse(element.getAttribute("vendorshipblockend" + y)));
                    }
                }

                if ( vendorNoShipFrom.size() > 0 )
                {
                    deliveryDateParm.setVendorNoShipFlag(Boolean.TRUE);
                    deliveryDateParm.setVendorNoShipFrom(vendorNoShipFrom);
                    deliveryDateParm.setVendorNoShipTo(vendorNoShipTo);
                }
                else
                {
                    deliveryDateParm.setVendorNoShipFlag(Boolean.FALSE);
                }

                Date firstArrivalDate = null;

                // set Florist delivery information
                if ( element.getAttribute("shipmethodflorist") != null &&
                     element.getAttribute("shipmethodflorist").equalsIgnoreCase(COMConstants.CONS_YES) )
                {
                    deliveryDateParm.setShipMethodFlorist(true);

                    if ( element.getAttribute("servicecharge") != null &&
                          !element.getAttribute("servicecharge").trim().equalsIgnoreCase("") )
                    {
                            floralServiceChargeSet = true;
                            deliveryDateParm.setFloralServiceCharge(new BigDecimal(element.getAttribute("servicecharge")));
                    }
                }

                // Check delivery type for carrier ship methods
                if ( element.getAttribute("shipmethodcarrier") != null &&
                     element.getAttribute("shipmethodcarrier").equalsIgnoreCase(COMConstants.CONS_YES) )
                {
                    // Only set Sunday Flag to false if this is not a same day gift product
                    if(!deliveryDateParm.getProductType().equalsIgnoreCase(COMConstants.CONS_PRODUCT_TYPE_SDG) &&
                       !deliveryDateParm.getProductType().equalsIgnoreCase(COMConstants.CONS_PRODUCT_TYPE_SDFC))
                    {
                        deliveryDateParm.setSundayDelivery(Boolean.FALSE);
                    }
                    deliveryDateParm.setShipMethodCarrier(true);
                    // Get the shipping methods for this product
                    HashMap shippingMethods = SearchUtil.getProductShippingMethods(element.getAttribute("productid"), element.getAttribute("standardprice"), element.getAttribute("shippingkey"));
                    deliveryDateParm.setShipMethods(shippingMethods);

                    // build values of No Ship Days for product
                    HashSet noShipDays = new HashSet();
                    if ( element.getAttribute("sundayflag") != null &&
                         element.getAttribute("sundayflag").equalsIgnoreCase(COMConstants.CONS_NO) )
                    {
                        noShipDays.add(new Integer(Calendar.SUNDAY));
                    }
                    if ( element.getAttribute("mondayflag") != null &&
                         element.getAttribute("mondayflag").equalsIgnoreCase(COMConstants.CONS_NO) )
                    {
                        noShipDays.add(new Integer(Calendar.MONDAY));
                    }
                    if ( element.getAttribute("tuesdayflag") != null &&
                         element.getAttribute("tuesdayflag").equalsIgnoreCase(COMConstants.CONS_NO) )
                    {
                        noShipDays.add(new Integer(Calendar.TUESDAY));
                    }
                    if ( element.getAttribute("wednesdayflag") != null &&
                         element.getAttribute("wednesdayflag").equalsIgnoreCase(COMConstants.CONS_NO) )
                    {
                        noShipDays.add(new Integer(Calendar.WEDNESDAY));
                    }
                    if ( element.getAttribute("thursdayflag") != null &&
                         element.getAttribute("thursdayflag").equalsIgnoreCase(COMConstants.CONS_NO) )
                    {
                        noShipDays.add(new Integer(Calendar.THURSDAY));
                    }
                    if ( element.getAttribute("fridayflag") != null &&
                         element.getAttribute("fridayflag").equalsIgnoreCase(COMConstants.CONS_NO) )
                    {
                        noShipDays.add(new Integer(Calendar.FRIDAY));
                    }
                    if ( element.getAttribute("saturdayflag") != null &&
                         element.getAttribute("saturdayflag").equalsIgnoreCase(COMConstants.CONS_NO) )
                    {
                        noShipDays.add(new Integer(Calendar.SATURDAY));
                    }
                    deliveryDateParm.setNoShipDays(noShipDays);
                }

                // if the product is not available or is codified and not available in the zip code
                // set flag on order for later processing
                String codifiedAvailable = element.getAttribute("codifiedavailable");
                String codifiedSpecialFlag = element.getAttribute("codifiedspecial");
                String deliverableFlag = element.getAttribute("codifieddeliverable");
                String codifiedProduct = element.getAttribute("codifiedproduct");
                String status = element.getAttribute("status");

                // set codified flags in delParms
                deliveryDateParm.setCodifiedProduct(codifiedProduct);
                deliveryDateParm.setCodifiedAvailable(codifiedAvailable);
                deliveryDateParm.setCodifiedSpecialFlag(codifiedSpecialFlag);
                deliveryDateParm.setCodifiedDeliverable(deliverableFlag);

                if (
                    // product is unavaiable if status is "U"
                    (status != null &&
                      status.equalsIgnoreCase("U")) ||

                    // product is unavaiable if it is a regular codified product
                    // and it is set as unavailable for this zip code in the
                    // CODIFIED_PRODUCTS table.  It also must not be carrier delivered
                     (codifiedAvailable != null &&
                      codifiedAvailable.equalsIgnoreCase(GeneralConstants.NO) &&
                      (codifiedSpecialFlag != null &&
                       codifiedSpecialFlag.equalsIgnoreCase(GeneralConstants.NO)) &&
                       !deliveryDateParm.isShipMethodCarrier()) ||

                    // product is unavaiable if it is a regular codified product
                    // and GNADD is at level 3
                     (codifiedProduct != null &&
                      codifiedProduct.equalsIgnoreCase(GeneralConstants.YES) &&
                      codifiedSpecialFlag != null &&
                      codifiedSpecialFlag.equalsIgnoreCase(GeneralConstants.NO) &&
                      oeGlobalParms.getGNADDLevel().equalsIgnoreCase("3"))
                    )
                {
                    flagVO.setDisplayProductUnavailable(COMConstants.CONS_YES);
                }
                else
                {
                    flagVO.setDisplayProductUnavailable(COMConstants.CONS_NO);
                }


                // zip code does not exist or is shut down
                // so can only delivery Carrier delivered products
                if (isDomesticFlag &&
                    (deliveryDateParm.getZipCodeGNADDFlag().equalsIgnoreCase("Y") &&
                     deliveryDateParm.getGlobalParms().getGNADDLevel().equalsIgnoreCase("0")))
                {
                    // only display if has not been displayed before
                    if ( deliveryDateParm.getProductType().equalsIgnoreCase(GeneralConstants.OE_PRODUCT_TYPE_FLORAL) )
                    {
                        flagVO.setDisplaySpecialtyGift("Y");
                    }
                }

            }

            if(!floralServiceChargeSet)
            {
                if(isDomesticFlag)
                {
                    deliveryDateParm.setFloralServiceCharge(new BigDecimal(domesticServiceFee));
                }
                else
                {
                    deliveryDateParm.setFloralServiceCharge(new BigDecimal(internationalServiceFee));
                }
            }


            String orderIntlFlag = "";
            if(isDomesticFlag)
            {
              orderIntlFlag = "N";
            }
            else
            {
              orderIntlFlag = "Y";
            }

            OrderVO order = buildOSPVO(productId, orderIntlFlag, sourceCode);
            deliveryDateParm.setOrder(order);
            deliveryDateParm.setItemNumber(1);



            deliveryDateParm.setGlobalParms(oeGlobalParms);
            deliveryDateParm.setHolidayDates((HashMap)holidayMap);

            // retrieve the delivery range information from the XML and put into the
            // delivery date parameter object
            deliveryDateUtil.getDeliveryRanges(COMConstants.CONS_XML_PRODUCT_LIST, document, deliveryDateParm);

            if(getFirstAvailableDate)
            {
                deliveryDates = new ArrayList();
                int maxDays = oeGlobalParms.getDeliveryDaysOutMin() + 1;
                deliveryDates.add(deliveryDateUtil.getFirstAvailableDate(deliveryDateParm, maxDays));
            }
            else
            {
                deliveryDates = deliveryDateUtil.getOrderProductDeliveryDates(deliveryDateParm);
            }

            // This method will check all the same day gift rules and
            // set the availability flags on the order
            List shipMethods = getDeliveryMethodsFromDateList(deliveryDates);
            checkForSDGAvailable(deliveryDateParm, shipMethods,
                  flagVO,sendToState,sendToCountry);

        }
        finally{ }

        return deliveryDates;
    }

    /**
     * This method takes in a list of dates objects which contain
     * ship methods.  This method then returns a distince list of
     * all the ship methods found in those objects.
     *
     * @param List dates Delivery Dates
     * @returns List Delivery Date Methods
     */
    public List getDeliveryMethodsFromDateList(List dates)
    {
        boolean foundFlorist = false;
        boolean foundGround = false;
        boolean foundTwoDay = false;
        boolean foundNextDay = false;
        boolean foundSaturday = false;
        List shippingMethods = new ArrayList();
        ShippingMethod floristShipping = new ShippingMethod();
        floristShipping.setCode(GeneralConstants.DELIVERY_FLORIST_CODE);
        ShippingMethod standardShipping = new ShippingMethod();
        standardShipping.setCode(GeneralConstants.DELIVERY_STANDARD_CODE);
        ShippingMethod twoDayShipping = new ShippingMethod();
        twoDayShipping.setCode(GeneralConstants.DELIVERY_TWO_DAY_CODE);
        ShippingMethod nextDayShipping = new ShippingMethod();
        nextDayShipping.setCode(GeneralConstants.DELIVERY_NEXT_DAY_CODE);
        ShippingMethod saturdayShipping = new ShippingMethod();
        saturdayShipping.setCode(GeneralConstants.DELIVERY_SATURDAY_CODE);

        for (int i = 0; i < dates.size(); i++)
        {
            OEDeliveryDate date = (OEDeliveryDate)dates.get(i);
            if(date.getShippingMethods().contains(floristShipping) &&
                    !shippingMethods.contains(GeneralConstants.DELIVERY_FLORIST_CODE))
            {
                foundFlorist = true;
                shippingMethods.add(GeneralConstants.DELIVERY_FLORIST_CODE);
            }
            else if(date.getShippingMethods().contains(standardShipping) &&
                    !shippingMethods.contains(GeneralConstants.DELIVERY_STANDARD_CODE))
            {
                foundGround = true;
                shippingMethods.add(GeneralConstants.DELIVERY_STANDARD_CODE);
            }
            else if(date.getShippingMethods().contains(twoDayShipping) &&
                    !shippingMethods.contains(GeneralConstants.DELIVERY_TWO_DAY_CODE))
            {
                foundTwoDay = true;
                shippingMethods.add(GeneralConstants.DELIVERY_TWO_DAY_CODE);
            }
            else if(date.getShippingMethods().contains(nextDayShipping) &&
                    !shippingMethods.contains(GeneralConstants.DELIVERY_NEXT_DAY_CODE))
            {
                foundNextDay = true;
                shippingMethods.add(GeneralConstants.DELIVERY_NEXT_DAY_CODE);
            }
            else if(date.getShippingMethods().contains(saturdayShipping) &&
                    !shippingMethods.contains(GeneralConstants.DELIVERY_SATURDAY_CODE))
            {
                foundSaturday = true;
                shippingMethods.add(GeneralConstants.DELIVERY_SATURDAY_CODE);
            }

            if(foundFlorist && foundGround && foundTwoDay && foundNextDay &&
               foundSaturday)
            {
                break;
            }
        }
        return shippingMethods;
    }


    /**
     * This method creates an XML document.
     *
     * @param String xml root name. Name use for XML root element.
     * @returns Document
     */
    public static Document createDocument(String xml)
        throws Exception
    {

        if(xml == null || xml.equalsIgnoreCase(""))
            return null;
        return DOMUtil.getDocument(xml);

    }



   /** This method converts an XML document to a String.
     * @param Document document to convert
     * @return String version of XML */
    public static String convertDocToString(Document xmlDoc) throws Exception
    {
            String xmlString = "";

            StringWriter s = new StringWriter();
            DOMUtil.print(xmlDoc,new PrintWriter(s));
            xmlString = s.toString();

            return xmlString;
        }


  /**
   * This mehtod creates an Order Srub Project order VO using the
   * passed in data.
   *
   * @String item id
   * @String reciepientIntlFlag
   * @param String sourceCode
   * @returns OrderVO
   * */
  private OrderVO buildOSPVO(String item, String recipientIntlFlag, String sourceCode) throws Exception
  {
      OrderVO order = new OrderVO();

      String origin = getOrigin(sourceCode);
      order.setOrderOrigin(origin);

      OrderDetailsVO detail = new OrderDetailsVO();
      List details = new ArrayList();
      detail.setProductId(item);

      RecipientAddressesVO addr = new RecipientAddressesVO();
      addr.setInternational(recipientIntlFlag);
      List addrs = new ArrayList();
      addrs.add(addr);

      RecipientsVO recip = new RecipientsVO();
      List recips = new ArrayList();
      recip.setRecipientAddresses(addrs);
      recips.add(recip);

      detail.setRecipients(recips);
      details.add(detail);

      order.setOrderDetail(details);

      return order;
  }

   /**
     * Retrieve the origin based on the source code passed in.
     * We needed to add this because of code that was added to the Delivery Date UTIL class.
     * There are now checks in there because of the Amazon project, which check specifically
     * for the Amazon origin.  There is functionality in place in that class
     * that only gets executed for Amazon orders only.  That is the only reason why
     * we need to set the origin in the order that we pass to the Delivery Date UTIL class.
     *
     * @param String sourceCode
     * @return String
     *
     */
    private String getOrigin(String sourceCode) throws IOException, ParserConfigurationException,
            SQLException, Exception
    {
          //Strings to hold the output parameters
          String outStatus;
          String outMessage;
          String outOrigin = "";

          UpdateOrderDAO uoDAO = new UpdateOrderDAO(this.con);

          HashMap originFromSourceHash = (HashMap) uoDAO.getOriginByIdMap(sourceCode);

          outStatus   = (String) originFromSourceHash.get("OUT_STATUS");

          if(outStatus.equalsIgnoreCase("N"))
          {
              outMessage  = (String) originFromSourceHash.get("OUT_MESSAGE");

              throw new Exception(outMessage);
          }
          else
          {
              if( originFromSourceHash.get("OUT_ORIGIN") != null)
                outOrigin= originFromSourceHash.get("OUT_ORIGIN").toString();
          }

          return outOrigin;
    }

  /** This method takes in a country code and returns
   * a flag indicating if the passed in country is domestic
   * or not.
   *
   * @param String country code
   * @returns Boolean  True=Domestic
   * */
//********************************************************************************************
// TESTED
//********************************************************************************************
  public boolean isDomestic(String country)
    throws IOException, ParserConfigurationException,
            SQLException, Exception
  {

      boolean domesticFlag = false;

      // Get the order header infomration
      UpdateOrderDAO uoDAO = new UpdateOrderDAO(this.con);

      try
      {
        Document countryXML = (Document) uoDAO.getCountryMasterByIdXML(country);

        String xpath = "//OCCASION";
        NodeList nl = DOMUtil.selectNodes(countryXML,xpath);
        Element element = null;
        String sCountryType = null;

        if (nl.getLength()>0)
        {
          element = (Element) nl.item(0);
          sCountryType = element.getAttribute("oecountrytype");
        }

        if(sCountryType == null || sCountryType.equalsIgnoreCase(COMConstants.CONS_DELIVERY_TYPE_DOMESTIC))
        {
          domesticFlag = true;
        }
      }
      finally
      {
      }

      return domesticFlag;

  }

    /**
     * Get product detail information.  This method will get product or upsell
     * product information.  It generates the xml used on the product detail page.
     *
     * @param String sourceCode
     * @param String zipCode
     * @param String country
     * @param String productId
     * @param String searchType
     * @boolean domesticFlag
     * @String state
     * @String pricingCode
     * @String domesticServiceFee
     * @String internationalServiceFee
     * @String partnerId
     * @List promotionList
     * @String rewardType
     * @String ProductFlagsVO
     * @String occasion
     * @String company
     * @String deliveryDateDisplay
     * @String city
     * @String deliveryDate
     * @HashMap pageData
     * @returns Document
     * @throws Exception
     */
//********************************************************************************************
// TESTED - this method is called individually to search for ONLY 1 product-id
//********************************************************************************************
    public Document getProductByID(String sourceCode, String zipCode, String country,
              String productId, String searchType, boolean domesticFlag,
              String state, String pricingCode, String domesticServiceFee,
              String internationalServiceFee, String partnerId, List promotionList,
              String rewardType, ProductFlagsVO flagsVO, String occasion,
              String company, String deliveryDateDisplay,
              String city, String deliveryDate, String origProductId, String orderShipMethod,
              HashMap pageData) throws Exception
	{
        Document document = null;
        Element documentTopElement = null;

        String customOrder = (String) pageData.get("custom_order");
        boolean customOrderFlag = false;
        if (customOrder != null && customOrder.equalsIgnoreCase(COMConstants.CONS_YES))
          customOrderFlag = true;

        long searchCount = 0;

        //reset flag
        flagsVO.setUpsellExists(false);

        UpdateOrderDAO uoDAO = new UpdateOrderDAO(this.con);

        try
        {

            SearchUtil SearchUtil = new SearchUtil(this.con);

            boolean simpleSearch = false;
            if (!customOrderFlag && searchType != null && searchType.equalsIgnoreCase("simpleproductsearch"))
              simpleSearch = true;

            //get global parameter values from FTD_APPS
            OEParameters oeGlobalParms = getGlobalParms();

            //is this guy domestic?
            String domIntlFlag = "";
            if ( domesticFlag )
            {
                domIntlFlag =  COMConstants.CONS_DELIVERY_TYPE_DOMESTIC;
            }
            else
            {
                 domIntlFlag =  COMConstants.CONS_DELIVERY_TYPE_INTERNATIONAL;
            }


             /* This methods creates an XML document which contains the following:
             *    Product Master data for the given product
             *    Global Paramater record
             *    Holidays for the given country
             *    Zip code flags
             *    Delivery Date range restrictions
             *    All the shipping methods in FTD_APPS*/
            Document productList = getProductDetail( productId,  sourceCode,  domIntlFlag,
                                           zipCode,  country, customOrderFlag, deliveryDate);

            //get count of items in list
            searchCount =  DOMUtil.selectNodes(productList,"//PRODUCT").getLength();

            //get a new document. note that the new document is not retrieved from the DOMUtil
            //because we do NOT want a <root> element at this point.
            document = DOMUtil.getDefaultDocument();
            documentTopElement = document.createElement("PRODUCTRESULTS");
            document.appendChild(documentTopElement);


            //The page builder method contains all the business logic to calculate prices,
            //discounts, ship methods... for each of the products.  The page builder
            //methods puts all this information into the XML.  The method also sets
            //various flags that indicate whether or not popup messages needs to be
            //shown to the user.
//1
            Element productDetailElementList = SearchUtil.pageBuilder(false, false,
              productList, 1, null, null, domesticFlag, domesticServiceFee,
              internationalServiceFee, state, country, pricingCode, partnerId,
              promotionList, rewardType, zipCode, flagsVO, sourceCode, customOrderFlag,
              origProductId, orderShipMethod);

            DOMUtil.addSection(document, productDetailElementList.getChildNodes());

            if ( simpleSearch )
            {
                String countryId = null;
                String zipGnaddFlag = null;
                String productType = null;
                String gnaddLevel = null;

                Element element = null;
                NodeList nl = null;
                String xpath = null;

                nl = productDetailElementList.getElementsByTagName("product");
                Element productDetailElement = (Element) nl.item(0);
                if ( productDetailElement != null )
                {
                    productId = productDetailElement.getAttribute("productid");
                }

                // retrieve the Global Parameter information
                gnaddLevel = oeGlobalParms.getGNADDLevel();


                // retrieve the product information
                xpath = "//PRODUCT";
                //q = new XPathQuery();
                nl = DOMUtil.selectNodes(productList,xpath);

                if (nl.getLength() > 0)
                {
                    element = (Element) nl.item(0);
                    zipGnaddFlag = element.getAttribute("zipgnaddflag").trim();
                    productType = element.getAttribute("producttype");
                }
                else
                {
                    zipGnaddFlag = "N";
                }

                // country is Canada, Puerto Rico, Virgin Islands, or International
                if ( countryId != null &&
                     (countryId.equalsIgnoreCase("CA") ||       // Canada
                     countryId.equalsIgnoreCase("PR") ||       // Puerto Rico
                     countryId.equalsIgnoreCase("VI") ))        // Virgin Islands
                {
                    // Zip code does not exist or is shut down,
                    // so can not delivery any products
                    if ( (zipGnaddFlag.equalsIgnoreCase("Y")) &&
                         (gnaddLevel.equalsIgnoreCase("0")) )
                    {
                        // only display if has not been displayed before
                         pageData.put("displayNoProductPopup", "Y");


                    }
                    // can only delivery floral products
                    else if ( productType != null && !productType.equalsIgnoreCase(GeneralConstants.OE_PRODUCT_TYPE_FLORAL) &&
                             (!productType.equalsIgnoreCase(GeneralConstants.OE_PRODUCT_TYPE_SDG) &&
                              !productType.equalsIgnoreCase(GeneralConstants.OE_PRODUCT_TYPE_SDFC)))
                    {
                        pageData.put("displayFloristPopup", "Y");

                        // set order flag so will not be displayed unless zip code changed later

                    }
                }

                // zip code does not exist or is shut down
                // so can only delivery Carrier delivered products
                else if ( (zipGnaddFlag.equalsIgnoreCase("Y")) &&
                          (gnaddLevel.equalsIgnoreCase("0")) )
                {
                    // only display if has not been displayed before
                    if ( productType.equalsIgnoreCase(GeneralConstants.OE_PRODUCT_TYPE_FLORAL) )
                    {
                        pageData.put("displaySpecGiftPopup", "Y");

                   }
                }

                // Logic for same day gift pop-ups
                if(productType != null &&
                   (productType.equalsIgnoreCase(GeneralConstants.OE_PRODUCT_TYPE_SDG) ||
                    productType.equalsIgnoreCase(GeneralConstants.OE_PRODUCT_TYPE_SDFC)))
                {
                  pageData.put("displayNoCodifiedFloristHasCommonCarrier", flagsVO.getDisplayNoCodifiedFloristHasCommonCarrier()!=null?flagsVO.getDisplayNoCodifiedFloristHasCommonCarrier().toUpperCase():null);
                  pageData.put("displayNoFloristHasCommonCarrier", flagsVO.getDisplayNoFloristHasCommonCarrier()!=null?flagsVO.getDisplayNoFloristHasCommonCarrier().toUpperCase():null);
                  pageData.put("displayProductUnavailable", flagsVO.getDisplayProductUnavailable()!=null?flagsVO.getDisplayProductUnavailable().toUpperCase():null);

                }
            }

            // indicates whether product is codified special and should be displayed
            String displayCodifiedSpecial = getDisplayCodifiedSpecial("productList", productList, customOrderFlag);
            pageData.put("displayCodifiedSpecial", displayCodifiedSpecial!=null?displayCodifiedSpecial.toUpperCase():null);

            // personalized products
            pageData.put("displayPersonalizedProduct", flagsVO.getDisplayPersonalizedProduct());

            if(!((String)pageData.get("displayCodifiedSpecial")).equalsIgnoreCase("Y"))
            {
                pageData.put("displayProductUnavailable", flagsVO.getDisplayProductUnavailable()!=null?flagsVO.getDisplayProductUnavailable().toUpperCase():null);
            }

            //occasion information, and the product's subcode information.
            Document extraProdDetail = getExtraProductDetail( company,  occasion,  productId, sourceCode);

            DOMUtil.addSection(document, (extraProdDetail.getChildNodes()));

            pageData.put("displaySpecialFee", flagsVO.getSpecialFeeFlag()!=null?flagsVO.getSpecialFeeFlag().toUpperCase():null);

            // indicates whether a drop ship product is allowed to go to the state selected
            pageData.put("dropShipAllowed", flagsVO.getDropShipAllowed()!=null?flagsVO.getDropShipAllowed().toUpperCase():null);

            //If no products were returned, check if it is an upsell product
            String xpath = "//PRODUCT";
            NodeList nl = DOMUtil.selectNodes(productList,xpath);

            if (nl.getLength() == 0)
            {

							Document upsellDetailXML = (Document) uoDAO.getUpsellDetailByIdXML(productId);

              xpath = "//UPSELLDETAIL";
              nl = DOMUtil.selectNodes(upsellDetailXML,xpath);

              //if upsell was found
              if(nl.getLength() > 0)
              {
								HashMap upsellMasterData = new HashMap();

								String upsellProductId = null;
								String upsellProductName = null;
								Element upsellElement = null;

								String skipUpsell = "Y";      //"N";
								String noneAvailable = "Y";
								String baseAvailable = "N";   //"Y";
								String showScripting = "N";

								//get a new document. note that the new document is not retrieved from
								//the DOMUtil because we do NOT want a <root> element at this point.
								document = DOMUtil.getDefaultDocument();
								documentTopElement = document.createElement("PRODUCTRESULTS");
								document.appendChild(documentTopElement);

								flagsVO.setUpsellExists(true);

								Document upsellTopDocument = DOMUtil.getDefaultDocument();
								Element upsellTopElement = upsellTopDocument.createElement("UPSELLS");
								upsellTopDocument.appendChild(upsellTopElement);
								//for each upsell in the list
								for(int i=0; i < nl.getLength(); i++)
                {
									int count = i + 1;

									Document upsellBottomDocument = DOMUtil.getDefaultDocument();
									Element upsellBottomElement = upsellBottomDocument.createElement("UPSELL");
									upsellBottomElement.setAttribute("num", new Integer(count).toString());
									upsellBottomDocument.appendChild(upsellBottomElement);

									upsellElement = (Element) nl.item(i);
									upsellProductId = upsellElement.getAttribute("upselldetailid");
									upsellProductName = upsellElement.getAttribute("upselldetailname");
								        upsellProductName = upsellProductName.replaceAll("\\<.*?>","");

									//set domestic flag
									String domIntlValue = "";
									if ( domesticFlag )
									{
										domIntlValue = GeneralConstants.OE_DELIVERY_TYPE_DOMESTIC;
									}
									else
									{
										domIntlValue = GeneralConstants.OE_DELIVERY_TYPE_INTERNATIONAL;
									}

									 /* This methods creates an XML document which contains the following:
									 *    Product Master data for the given product
									 *    Global Paramater record
									 *    Holidays for the given country
									 *    Zip code flags
									 *    Delivery Date range restrictions
									 *    All the shipping methods in FTD_APPS*/
									Document searchResultList = getProductDetail( upsellProductId,  sourceCode,  domIntlValue,
																		 zipCode,  country, customOrderFlag, deliveryDate);

									//The page builder method contains all the business logic to calculate prices,
									//discounts, ship methods... for each of the products.  The page builder
									//methods puts all this information into the XML.  The method also sets
									//various flags that indicate whether or not popup messages needs to be
									//shown to the user.
									productDetailElementList = SearchUtil.pageBuilder(true, true,
													searchResultList, 1, null, null, domesticFlag,  domesticServiceFee,
													internationalServiceFee, state,  country, pricingCode, partnerId,
													promotionList, rewardType,  zipCode, flagsVO, sourceCode, customOrderFlag,
													origProductId, orderShipMethod);


									Element productDetailElement = (Element) productDetailElementList.getElementsByTagName("PRODUCT").item(0);

									//if nothing found exist
									if(productDetailElement != null)
									{

										productDetailElement.setAttribute("upsellsequence", new Integer(i + 1).toString());
										productDetailElement.setAttribute("upsellproductname", upsellProductName);

										//This method sets the 'specialunavailable' attribute for the productDetailElement
										createUpsellDetail(productDetailElement, searchResultList, country, state, flagsVO, customOrderFlag);

										// Check if upsell page should be skipped
										if(productDetailElement.getAttribute("upsellsequence") != null && productDetailElement.getAttribute("upsellsequence").equalsIgnoreCase("1"))
										{
											upsellMasterData.put("masterId", upsellElement.getAttribute("upsellmasterid"));
											upsellMasterData.put("masterName", upsellElement.getAttribute("upsellmastername"));
											upsellMasterData.put("masterdescription", upsellElement.getAttribute("upsellmasterdescription"));
											upsellMasterData.put("masterStatus", upsellElement.getAttribute("upsellmasterstatus"));

											//if unavailable skip the upsell
											if((productDetailElement.getAttribute("status") != null
												 && productDetailElement.getAttribute("status").equalsIgnoreCase("U"))
												 || (productDetailElement.getAttribute("specialunavailable") != null
												 && productDetailElement.getAttribute("specialunavailable").equalsIgnoreCase("Y")))
											{
												//skipUpsell = "N";
												//baseAvailable = "N";
											}
											else
											{
												//skipUpsell = upsellProductId;
												//noneAvailable = "N";
												skipUpsell = "N";
												noneAvailable = "N";
												baseAvailable = "Y";
											}
										}
										else
										{
											if(productDetailElement.getAttribute("status") != null
												 && productDetailElement.getAttribute("status").equalsIgnoreCase("A")
												 && productDetailElement.getAttribute("specialunavailable") != null
												 && productDetailElement.getAttribute("specialunavailable").equalsIgnoreCase("N"))
											{
												skipUpsell = "N";
												noneAvailable = "N";
												baseAvailable = "Y"; //newly added

													// If the last product is available show scripting
													if(i+1 == nl.getLength())
													{
														showScripting = "Y";
													}
											}
										}

										//add the list to the bottom xml
										DOMUtil.addSection(upsellBottomDocument, productDetailElementList.getChildNodes());

										//add the bottom xml to the top xml
										DOMUtil.addSection(upsellTopDocument, upsellBottomDocument.getChildNodes());
									}

								} //end-for

                //add the top xml to the document
                DOMUtil.addSection(document, upsellTopDocument.getChildNodes());

                //set upsell flags
								flagsVO.setUpsellSkip(skipUpsell);
								upsellMasterData.put("noneAvailable", noneAvailable);
								upsellMasterData.put("baseAvailable", baseAvailable);
								upsellMasterData.put("showScripting", showScripting);

                DOMUtil.addSection(document, "UPSELLMASTER", "UPSELLDETAIL", upsellMasterData, false);

              } //end -- if(nl.getLength() > 0)
						} //end -- if (nl.getLength() == 0)
        }
        finally
        {
        }

        return document;
	}
  /**
   * This method creates an XML document containing occasion information
   * and the product's subcode information.
   *
   * @param String company
   * @param String pageId
   * @param String occasionid
   * @param String productId
   * @returns Document
   *
   */
//********************************************************************************************
// TESTED
//********************************************************************************************
  private Document getExtraProductDetail(String company, String occasionId, String productId, String sourceCode) throws Exception
  {
      UpdateOrderDAO uoDAO = new UpdateOrderDAO(this.con);

      Document responseDocument = null;

      try{
          javax.xml.parsers.DocumentBuilder docBuilder = DOMUtil.getDocumentBuilder();
          Document doc = docBuilder.newDocument();
          Element root = (Element) doc.createElement("EXTRAINFO");
          doc.appendChild(root);
          responseDocument = (Document)doc;

          String nullString = null;

          //subcode information
          Document productSubcodesActiveXML = (Document) uoDAO.getProductSubcodeActiveByIdXML(productId);
          DOMUtil.addSection(responseDocument, productSubcodesActiveXML.getChildNodes());

          //get addon information
          AddOnUtility aau = new AddOnUtility();
          int occasion;
          try {
            occasion = Integer.parseInt(occasionId);
          } catch (Exception e) {
            logger.error("getExtraProductDetail - Occasion ID is non-numeric: " + occasionId);
            occasion = 0;
          }
          HashMap aaMap = aau.getActiveAddonListByProductIdAndOccasionAndSourceCode(productId, occasion, sourceCode, false, this.con);
          DOMUtil.addSection(responseDocument, ((Document) aau.convertAddOnMapToXML(aaMap)).getChildNodes()); 

      }
      finally
      {
      }

      return responseDocument;
  }


  /**
   * This methods creates an XML document which contains the following:
   *    Product Master data for the given product
   *    Global Paramater record
   *    Holidays for the given country
   *    Zip code flags
   *    Delivery Date range restrictions
   *    All the shipping methods in FTD_APPS
   *
   * @String productId
   * @String sourceCode
   * @String domIntlFlag
   * @String zipCode
   * @String country
   * @returns Document
   */
//********************************************************************************************
// TESTED
//********************************************************************************************
  private Document getProductDetail(String productId, String sourceCode, String domIntlFlag,
          String zipCode, String country, boolean customOrderFlag, String deliveryDate)
          throws Exception
  {

      UpdateOrderDAO uoDAO = new UpdateOrderDAO(this.con);

      Document responseDocument;

      try{
          responseDocument = (Document) DOMUtil.getDocument();

          String nullString = null;

          //product details
          logger.debug("Calling getProductDetailXML: " + productId + " " + sourceCode + " " + deliveryDate);
          Document productDetailsXML = uoDAO.getProductDetailXML(productId, sourceCode, domIntlFlag, zipCode,nullString, deliveryDate);

          DOMUtil.addSection(responseDocument, productDetailsXML.getChildNodes());

          //append global params
          OEParameters oeGlobalParms = getGlobalParms();

          //create an xml object for the oeGlobalParms
          Document globalXML = (Document) DOMUtil.getDefaultDocument();

          //generate the top node
          Element stateElement = globalXML.createElement("OEParameters");
          //and append it
          globalXML.appendChild(stateElement);

          Document oeGlobalDoc = DOMUtil.getDocument(oeGlobalParms.toXML(0));
          globalXML.getDocumentElement().appendChild(globalXML.importNode(oeGlobalDoc.getFirstChild(), true));

          //append it to the main document.
          DOMUtil.addSection(responseDocument, globalXML.getChildNodes());

          //ship methods
          Document allShippingMethodsXML = (Document) uoDAO.getAllShippingMethodXML();
          DOMUtil.addSection(responseDocument, allShippingMethodsXML.getChildNodes());

      }
      finally
      {
      }

        return  responseDocument;
  }


    /**
     * This method returns a flag related to codified products.
     *
     * @params String elementName Not currently used
     * @params XMLDoucment which contains product information
     * @returns String "Y" or "N"
     */
//********************************************************************************************
// TESTED
//********************************************************************************************
    private String getDisplayCodifiedSpecial(String elementName, Document xmlResponse,
            boolean customOrderFlag)
      throws Exception
    {

        // Get global params
        SearchUtil SearchUtil = new SearchUtil(this.con);
        OEParameters oeGlobalParms = getGlobalParms();

        // retrieve the product special codified flag
        String xpath = "//PRODUCT";
        NodeList nl = DOMUtil.selectNodes(xmlResponse,xpath);
        Element product = null;
        String codifiedSpecialFlag = "N";
        String deliverableFlag = "N";
        if(nl.getLength() > 0)
        {
            product = (Element) nl.item(0);
            codifiedSpecialFlag = product.getAttribute("codifiedspecial");
            if(codifiedSpecialFlag == null) codifiedSpecialFlag = "N";

            deliverableFlag = product.getAttribute("codifieddeliverable");
            if(deliverableFlag == null) deliverableFlag = "N";
        }

        // retrieve the florist information
        //xpath = "productList/floristListData/data";
        //q = new XPathQuery();
        //NodeList floristNodelist = q.query(dataResponse.getDataVO().getData().toString(), xpath);
        String ret = "N";

        // check for at least one florist that has this product available
        if(codifiedSpecialFlag.equalsIgnoreCase("Y") && (deliverableFlag.equalsIgnoreCase("N")) /*&& oeGlobalParms.getGNADDLevel().equalsIgnoreCase("0")*/)
        {
            ret = "Y";
        }

        return ret;
    }

    /**
     * This method sets the 'specialunavailable' attribute for the productDetailElement
     *
     * @param Element productDetailElement
     * @param Document ProductList
     * @param String country
     * @param String state
     * @param ProductFlagsVO flagsVO
     * @returns void
     * @throws Exception
     */
//********************************************************************************************
// TESTED
//********************************************************************************************
    private void createUpsellDetail(Element productDetailElement, Document productList,
            String country, String state, ProductFlagsVO flagsVO, boolean customOrderFlag)
      throws Exception
    {
        String countryId = null;
        String zipGnaddFlag = null;
        String productType = null;
        String gnaddLevel = null;

        Element element = null;
        NodeList nl = null;
        String xpath = null;

        productDetailElement.setAttribute("specialunavailable","N");

        //obtain the gnadd level
        OEParameters oeGlobalParms = getGlobalParms();

        gnaddLevel = oeGlobalParms.getGNADDLevel();

        //get the product
        xpath = "//PRODUCT";
        nl = DOMUtil.selectNodes(productList,xpath);
        if (nl.getLength() > 0)
        {
            element = (Element) nl.item(0);
            zipGnaddFlag = element.getAttribute("zipgnaddflag").trim();
            productType = element.getAttribute("producttype");
        }
        else
        {
            zipGnaddFlag = "N";
        }

        if ( countryId != null &&
             (countryId.equalsIgnoreCase("CA") ||       // Canada
             countryId.equalsIgnoreCase("PR") ||        // Puerto Rico
             countryId.equalsIgnoreCase("VI") ))        // Virgin Islands
        {
            if ( (zipGnaddFlag.equalsIgnoreCase("Y")) &&
                 (gnaddLevel.equalsIgnoreCase("0")) )
            {
                productDetailElement.setAttribute("specialunavailable","Y");
            }
             else if ( productType != null && !productType.equalsIgnoreCase(GeneralConstants.OE_PRODUCT_TYPE_FLORAL) )
            {
                productDetailElement.setAttribute("specialUnavailable","Y");
            }
        }
        else if ( (zipGnaddFlag.equalsIgnoreCase("Y")) &&
                  (gnaddLevel.equalsIgnoreCase("0")) )
        {
            if ( productType.equalsIgnoreCase(GeneralConstants.OE_PRODUCT_TYPE_FLORAL) )
            {
                productDetailElement.setAttribute("specialunavailable","Y");
            }
        }

        if(getDisplayCodifiedSpecial("productlist", productList, customOrderFlag).equalsIgnoreCase("Y")) {
            productDetailElement.setAttribute("specialunavailable","Y");
        }

        if(flagsVO.getDisplayProductUnavailable() != null && flagsVO.getDisplayProductUnavailable().equalsIgnoreCase("Y"))
        {
            productDetailElement.setAttribute("specialunavailable","Y");
        }

        if (state != null && (state.equalsIgnoreCase("AK") || state.equalsIgnoreCase("HI")))
        {
            if(productType != null && productType.equalsIgnoreCase(GeneralConstants.OE_PRODUCT_TYPE_FRECUT))
            {
                productDetailElement.setAttribute("specialunavailable","Y");
            }
        }
    }


    /** This method returns a list of prducts.
     * @param String pageNumber The current page number
     * @param String indexId The category index id
     * @param String orderDeliveryZipCode The zip code to where product is being delivered
     * @param boolean isSendToCustomerDomestic Flag indicates if customer is domestic
     * @param String sourceCode The source code
     * @param String pricePointId The price point id for the the product
     * @param String sendToCustomerCounter The country to where product is being sent
     * @param String domesticServiceFee Domestic Service Fee
     * @param String internationialServiceFee Intl Service Fee
     * @param String sendToCustomerState Send to customer state
     * @param String pricingCode the products pricing code
     * @param String partnerId The partner id related to the this source code
     * @param List promotionList List or promotions available to this source code
     * @param String rewardType The reward type associated with this order
     * @param ProductFlagsVO flagVO Flags used by the XSL page
     * @param String sortType sort value
     * @param String customerName Name of customer
     * @param HashMap pageDataMap Data which needs to be passed to the XSL page
     * @throws Exception*/
    public Document getProductsByCategory(String pageNumber,String indexId,
          String orderDeliveryZipCode, boolean isSendToCustomerDomestic,
          String sourceCode,String pricePointId,String sendToCustomerCountry,
          String domesticServiceFee, String internationalServiceFee,
          String sendToCustomerState, String pricingCode, String partnerId,
          List promotionList, String rewardType, ProductFlagsVO flagVO,
          String sortType, String customerName, String origProductId, String orderShipMethod,
          HashMap pageData, ServletContext context, String deliveryDate)
          throws Exception
	{


      Document document = null;
      Element documentTopElement = null;

      String customOrder = (String) pageData.get("custom_order");
      boolean customOrderFlag = false;
      if (customOrder != null && customOrder.equalsIgnoreCase(COMConstants.CONS_YES))
        customOrderFlag = true;

      long pageCount = 0;

        try {

            UpdateOrderDAO uoDAO = new UpdateOrderDAO(this.con);

            SearchUtil SearchUtil = new SearchUtil(this.con);

            //get a new document. note that the new document is not retrieved from the DOMUtil
            //because we do NOT want a <root> element at this point.
            document = DOMUtil.getDefaultDocument();
            documentTopElement = document.createElement("PRODUCTRESULTS");
            document.appendChild(documentTopElement);



            String intDomFlag = null;
            if ( isSendToCustomerDomestic )
            {
                intDomFlag = COMConstants.CONS_DELIVERY_TYPE_DOMESTIC;
            }
            else
            {
                intDomFlag = COMConstants.CONS_DELIVERY_TYPE_INTERNATIONAL;
            }


            //the days out from the global params
            OEParameters oeGlobalParms = getGlobalParms();


            NodeList nl;

            String daysOut = Integer.toString(oeGlobalParms.getDeliveryDaysOut());

            //determine the end date string by adding the daysout to the current date
            Calendar deliveryEndDate = Calendar.getInstance();
            deliveryEndDate.add(Calendar.DATE, Integer.parseInt(daysOut));
            SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
            String endDateStr = dateFormat.format(deliveryEndDate.getTime());

            // Initial search hit loads all products matching the query for all pages
            if ( (pageNumber == null) || (pageNumber.equalsIgnoreCase("init")|| (pageNumber.equalsIgnoreCase("1")) ))
            {

                String categoryName = "";
                String parentCategoryName = "";

                //Load Super Index List by index id.  The index id is the
                //id of the selected category.  This stored proc retruns the
                //name of selected index id.  The proc also returns the name of the
                //parent index if one exists.
                Document searchIndexDetails = (Document) uoDAO.getProductIndexByIdXML(indexId);

                nl = DOMUtil.selectNodes(searchIndexDetails,"/INDEXDETAIL/DATA");

                //if an index was found get the category and parent category name
                if (nl.getLength() > 0)
                {
                    Element indexElement = (Element) nl.item(0);
                    categoryName = indexElement.getAttribute("indexname");
                    if(indexElement.getAttribute("parentindexname") != null && !indexElement.getAttribute("parentindexname").equalsIgnoreCase("")) {
                        parentCategoryName = indexElement.getAttribute("parentindexname");
                        categoryName = " (" + categoryName + ")";
                    }
                }


                //clear out zip code if needed
                if(orderDeliveryZipCode.equalsIgnoreCase("N/A"))
                {
                    orderDeliveryZipCode = "";
                }

                // set arguments
                Map searchArgs = new HashMap();
                searchArgs.put(COMConstants.CONS_SEARCH_INDEX_ID, indexId);//category id
                searchArgs.put(COMConstants.CONS_SEARCH_SOURCE_CODE, sourceCode);
                searchArgs.put(COMConstants.CONS_SEARCH_ZIP_CODE, orderDeliveryZipCode);
                searchArgs.put(COMConstants.CONS_SEARCH_PRICE_POINT_ID, pricePointId);//this will contain a value if the products filtered by price
                searchArgs.put(COMConstants.CONS_SEARCH_DOMESTIC_INTL_FLAG, intDomFlag);
                searchArgs.put(COMConstants.CONS_SEARCH_COUNTRY_ID, sendToCustomerCountry);
                searchArgs.put(COMConstants.CONS_SEARCH_DELIVERY_END_DATE, endDateStr);//current date + days out



                //Perform search.  This method returns all the products which will
                //be displayed on the pages  (not just this one page).
                Document searchResultsIndex = SearchUtil.performProductListSearch(this.con,
                      COMConstants.CONS_SEARCH_GET_PRODUCTS_BY_INDEX,
                      sourceCode, indexId, orderDeliveryZipCode, pricePointId,
                      sendToCustomerCountry, endDateStr, intDomFlag);


                //Converts the search criteria map into a tokenized string
                String searchCriteria = SearchUtil.getSearchCriteriaString(searchArgs);

                //Sort the entire product list..this the ENTIRE list of products which
                //fall under the selected category
                if(sortType != null && !(sortType.equalsIgnoreCase("")))
                {                    String filename = "/xsl/"+sortType+".xsl";
                    File xslFile = new File(context.getRealPath(filename));
                    searchResultsIndex = SearchUtil.sortProductList(xslFile, searchResultsIndex);

                }
                else{
                    String filename = "/xsl/sortDisplayOrder.xsl";
                    File xslFile = new File(context.getRealPath(filename));
                    searchResultsIndex = SearchUtil.sortProductList(xslFile, searchResultsIndex);
                }


                //This method adds page numbers to each of the product elements.  It returns
                //the total count of pages.
                pageCount = SearchUtil.paginate(searchResultsIndex,1, COMConstants.CONS_PRODUCTS_PER_PAGE);


                //This method takes in a list of the available products and returns
                //detailed product information for the products on the current page
                //(only products for this page! --this where all products for the other
                //page get filterd out).
                Document searchResultsPerPage= SearchUtil.getProductsByIDs(searchResultsIndex, "1", pricePointId, sourceCode,
                    isSendToCustomerDomestic,orderDeliveryZipCode, sendToCustomerCountry,
                    daysOut, customOrderFlag, deliveryDate);


                //Now sort the products that will be displayed on the current page.
                //At this point the sort which was done on the ENTIRE list is lost
                if(sortType != null && !(sortType.equalsIgnoreCase("")))
                {                    String filename = "/xsl/"+sortType+".xsl";
                    File xslFile = new File(context.getRealPath(filename));
                    searchResultsPerPage = SearchUtil.sortProductList(xslFile, searchResultsPerPage);

                }
                else
                {
                    String filename = "/xsl/sortDisplayOrder.xsl";
                    File xslFile = new File(context.getRealPath(filename));
                    searchResultsPerPage = SearchUtil.sortProductList(xslFile, searchResultsPerPage);
                }


                //This method removes unavailable prodcuts from the searchResultsIndex document
                //This method also copes the PageData from teh SearchResultsIndext to the SearchResultsPerPage doc
                SearchUtil.rebuildSearchResultsIndex(searchResultsIndex, searchResultsPerPage, 1, COMConstants.CONS_PRODUCTS_PER_PAGE);

                Element data = (Element) DOMUtil.selectNodes(searchResultsIndex,"//pageData/data[@name='totalPages']").item(0);

                //The page builder method contains all the business logic to calculate prices,
                //discounts, ship methods... for each of the products.  The page builder
                //methods puts all this information into the XML.  The method also sets
                //various flags that indicate whether or not popup messages needs to be
                //shown to the user.
                DOMUtil.addSection(document, SearchUtil.pageBuilder(
                          true, true, searchResultsPerPage,
                          COMConstants.CONS_PRODUCTS_PER_PAGE, null, null,
                          isSendToCustomerDomestic,  domesticServiceFee,
                          internationalServiceFee, sendToCustomerState,  sendToCustomerCountry,
                          pricingCode, partnerId,  promotionList, rewardType,
                          orderDeliveryZipCode, flagVO, sourceCode,
                          customOrderFlag, origProductId, orderShipMethod).getChildNodes());



            }
            else
            {

                //Set serach results index to null.  In OE the search results
                //were stored in a persitiant dataobject.  Currently that
                //is not being done here.
                Document searchResultsIndex = null;

                // If search results are null then perform another search.
                // This could happen if the session was loaded from the database and it did not already
                // exist in the app server cache
                if(searchResultsIndex == null)
                {

                // set arguments
                Map searchCiteria = new HashMap();
                searchCiteria.put(COMConstants.CONS_SEARCH_INDEX_ID, indexId);
                searchCiteria.put(COMConstants.CONS_SEARCH_SOURCE_CODE, sourceCode);
                searchCiteria.put(COMConstants.CONS_SEARCH_ZIP_CODE, orderDeliveryZipCode);
                searchCiteria.put(COMConstants.CONS_SEARCH_PRICE_POINT_ID, pricePointId);
                searchCiteria.put(COMConstants.CONS_SEARCH_DOMESTIC_INTL_FLAG, intDomFlag);
                searchCiteria.put(COMConstants.CONS_SEARCH_COUNTRY_ID, sendToCustomerCountry);
                searchCiteria.put(COMConstants.CONS_SEARCH_DELIVERY_END_DATE, endDateStr);

                // perform search
                searchResultsIndex = SearchUtil.performProductListSearch(this.con,
                      COMConstants.CONS_SEARCH_GET_PRODUCTS_BY_INDEX,
                      sourceCode, indexId, orderDeliveryZipCode, pricePointId,
                      sendToCustomerCountry, endDateStr, intDomFlag);

                    pageCount = SearchUtil.paginate(searchResultsIndex,Integer.valueOf(pageNumber).intValue(), COMConstants.CONS_PRODUCTS_PER_PAGE);
                }

                // if we are sorting by price then we need to paginate the sorted list and save it
                // to the order

                if(sortType != null && !(sortType.equalsIgnoreCase("")))
                {

                    String filename = "/xsl/"+sortType+".xsl";
                    File xslFile = new File(context.getRealPath(filename));
                    searchResultsIndex = SearchUtil.sortProductList(xslFile, searchResultsIndex);
                    pageCount = SearchUtil.paginate(searchResultsIndex,Integer.valueOf(pageNumber).intValue(), COMConstants.CONS_PRODUCTS_PER_PAGE);
                }


                // Get the product details for the current page
                Document searchResultsPerPage= SearchUtil.getProductsByIDs(searchResultsIndex, pageNumber, pricePointId, sourceCode,
                    isSendToCustomerDomestic,orderDeliveryZipCode, sendToCustomerCountry,
                    daysOut, customOrderFlag, deliveryDate);

                // if we are not sorting by anything then the default sort is by
                // the order in the product index
                if(sortType == null || (sortType.equalsIgnoreCase("")))
                {
                    String filename = "/xsl/sortDisplayOrder.xsl";
                    File xslFile = new File(context.getRealPath(filename));
                    searchResultsPerPage = SearchUtil.sortProductList(xslFile, searchResultsPerPage);
                }
                // else sort by whatever type is passed in the arguments
                else if(sortType != null && !(sortType.equalsIgnoreCase("")))
                {
                    String filename = "/xsl/"+sortType+".xsl";
                    File xslFile = new File(context.getRealPath(filename));
                    searchResultsPerPage = SearchUtil.sortProductList(xslFile, searchResultsPerPage);
                }

                int iCurrentPage = Integer.parseInt(pageNumber);

                //This method removes unavailable prodcuts from the searchResultsIndex document
                //This method also copes the PageData from the SearchResultsIndext to the SearchResultsPerPage doc
                SearchUtil.rebuildSearchResultsIndex(searchResultsIndex, searchResultsPerPage, iCurrentPage, COMConstants.CONS_PRODUCTS_PER_PAGE);

                //The page builder method contains all the business logic to calculate prices,
                //discounts, ship methods... for each of the products.  The page builder
                //methods puts all this information into the XML.  The method also sets
                //various flags that indicate whether or not popup messages needs to be
                //shown to the user.
                DOMUtil.addSection(document, SearchUtil.pageBuilder(
                      true, true, searchResultsPerPage,
                      COMConstants.CONS_PRODUCTS_PER_PAGE, null, null,
                      isSendToCustomerDomestic,  domesticServiceFee,  internationalServiceFee,
                      sendToCustomerState,  sendToCustomerCountry, pricingCode,
                      partnerId,  promotionList, rewardType, orderDeliveryZipCode,
                      flagVO, sourceCode, customOrderFlag,
                      origProductId, orderShipMethod).getChildNodes());


           }

            Document dataResponse = (Document) uoDAO.getAllPricePointXML();
            // Encode previously loaded price points
            DOMUtil.addSection(document, dataResponse.getChildNodes());

            dataResponse = (Document) uoDAO.getAllShippingMethodXML();
            DOMUtil.addSection(document, dataResponse.getChildNodes());

            if(pageNumber == null || pageNumber.length() <=0 || pageNumber.equalsIgnoreCase("init"))
            {
              pageNumber = "1";
            }


            // Encode page data for product list

            pageData.put("total_pages", Long.toString(pageCount));
            pageData.put("page_number", pageNumber);

        }
        finally
        {
        }

        return document;
	}


    /** This method returns a list of prducts.
     * @param String pageNumber The current page number
     * @param String indexId The category index id
     * @param String orderDeliveryZipCode The zip code to where product is being delivered
     * @param boolean isSendToCustomerDomestic Flag indicates if customer is domestic
     * @param String sourceCode The source code
     * @param String pricePointId The price point id for the the product
     * @param String sendToCustomerCounter The country to where product is being sent
     * @param String domesticServiceFee Domestic Service Fee
     * @param String internationialServiceFee Intl Service Fee
     * @param String sendToCustomerState Send to customer state
     * @param String pricingCode the products pricing code
     * @param String partnerId The partner id related to the this source code
     * @param List promotionList List or promotions available to this source code
     * @param String rewardType The reward type associated with this order
     * @param ProductFlagsVO flagVO Flags used by the XSL page
     * @param String sortType sort value
     * @param String customerName Name of customer
     * @param HashMap pageDataMap Data which needs to be passed to the XSL page
     * @throws Exception*/
    public Document getProductsForKeywordSearch(String pageNumber,String keywordSearchList,
          String orderDeliveryZipCode, boolean isSendToCustomerDomestic,
          String sourceCode,String pricePointId,String sendToCustomerCountry,
          String domesticServiceFee, String internationalServiceFee,
          String sendToCustomerState, String pricingCode, String partnerId,
          List promotionList, String rewardType, ProductFlagsVO flagVO,
          String sortType, String customerName, String origProductId, String orderShipMethod,
          HashMap pageData, String deliveryDate)
          throws Exception
	{


      Document document = null;
      Element documentTopElement = null;

      String customOrder = (String) pageData.get("custom_order");
      boolean customOrderFlag = false;
      if (customOrder != null && customOrder.equalsIgnoreCase(COMConstants.CONS_YES))
        customOrderFlag = true;

      long pageCount = 0;

        try {

            UpdateOrderDAO uoDAO = new UpdateOrderDAO(this.con);

            SearchUtil SearchUtil = new SearchUtil(this.con);

            //get a new document. note that the new document is not retrieved from the DOMUtil
            //because we do NOT want a <root> element at this point.
            document = DOMUtil.getDefaultDocument();
            documentTopElement = document.createElement("PRODUCTRESULTS");
            document.appendChild(documentTopElement);



            String intDomFlag = null;
            if ( isSendToCustomerDomestic )
            {
                intDomFlag = COMConstants.CONS_DELIVERY_TYPE_DOMESTIC;
            }
            else
            {
                intDomFlag = COMConstants.CONS_DELIVERY_TYPE_INTERNATIONAL;
            }


            //the days out from the global params
            OEParameters oeGlobalParms = getGlobalParms();

            NodeList nl;

            String daysOut = Integer.toString(oeGlobalParms.getDeliveryDaysOut());

            //determine the end date string by adding the daysout to the current date
            Calendar deliveryEndDate = Calendar.getInstance();
            deliveryEndDate.add(Calendar.DATE, Integer.parseInt(daysOut));
            SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
            String endDateStr = dateFormat.format(deliveryEndDate.getTime());

            // Initial search hit loads all products matching the query for all pages
            if ( (pageNumber == null) || (pageNumber.equalsIgnoreCase("init")|| (pageNumber.equalsIgnoreCase("1")) ))
            {


                //clear out zip code if needed
                if(orderDeliveryZipCode.equalsIgnoreCase("N/A"))
                {
                    orderDeliveryZipCode = "";
                }


                //Perform search.  This method returns all the products which will
                //be displayed on the pages  (not just this one page).
                Document searchResultsIndex = SearchUtil.performProductListSearchForKeywordSearch(this.con,
                      keywordSearchList, pricePointId, sourceCode, intDomFlag, orderDeliveryZipCode,
                      endDateStr, sendToCustomerCountry);


                //This method adds page numbers to each of the product elements.  It returns
                //the total count of pages.
                pageCount = SearchUtil.paginate(searchResultsIndex,1, COMConstants.CONS_PRODUCTS_PER_PAGE);


                //This method takes in a list of the available products and returns
                //detailed product information for the products on the current page
                //(only products for this page! --this where all products for the other
                //page get filterd out).
                Document searchResultsPerPage= SearchUtil.getProductsByIDs(searchResultsIndex, "1", pricePointId, sourceCode,
                    isSendToCustomerDomestic,orderDeliveryZipCode, sendToCustomerCountry,
                    daysOut, customOrderFlag, deliveryDate);

                //This method removes unavailable prodcuts from the searchResultsIndex document
                //This method also copes the PageData from teh SearchResultsIndext to the SearchResultsPerPage doc
                SearchUtil.rebuildSearchResultsIndex(searchResultsIndex, searchResultsPerPage, 1, COMConstants.CONS_PRODUCTS_PER_PAGE);

                Element data = (Element) DOMUtil.selectNodes(searchResultsIndex,"//pageData/data[@name='totalPages']").item(0);

                //The page builder method contains all the business logic to calculate prices,
                //discounts, ship methods... for each of the products.  The page builder
                //methods puts all this information into the XML.  The method also sets
                //various flags that indicate whether or not popup messages needs to be
                //shown to the user.
                DOMUtil.addSection(document, SearchUtil.pageBuilder(
                      true, true, searchResultsPerPage,
                      COMConstants.CONS_PRODUCTS_PER_PAGE, null, null,
                      isSendToCustomerDomestic,  domesticServiceFee,  internationalServiceFee,
                      sendToCustomerState,  sendToCustomerCountry, pricingCode,
                      partnerId,  promotionList, rewardType, orderDeliveryZipCode,
                      flagVO, sourceCode, customOrderFlag,
                      origProductId, orderShipMethod).getChildNodes());



            }
            else
            {

                //Set serach results index to null.  In OE the search results
                //were stored in a persitiant dataobject.  Currently that
                //is not being done here.
                Document searchResultsIndex = null;

                // If search results are null then perform another search.
                // This could happen if the session was loaded from the database and it did not already
                // exist in the app server cache
                if(searchResultsIndex == null)
                {
                  // perform search
                  searchResultsIndex = SearchUtil.performProductListSearchForKeywordSearch(this.con,
                        keywordSearchList, pricePointId, sourceCode, intDomFlag, orderDeliveryZipCode,
                        endDateStr, sendToCustomerCountry);

                  pageCount = SearchUtil.paginate(searchResultsIndex,Integer.valueOf(pageNumber).intValue(), COMConstants.CONS_PRODUCTS_PER_PAGE);
                }

                // Get the product details for the current page
                Document searchResultsPerPage= SearchUtil.getProductsByIDs(searchResultsIndex, pageNumber, pricePointId, sourceCode,
                    isSendToCustomerDomestic,orderDeliveryZipCode, sendToCustomerCountry,
                    daysOut, customOrderFlag, deliveryDate);

                int iCurrentPage = Integer.parseInt(pageNumber);

                //This method removes unavailable prodcuts from the searchResultsIndex document
                //This method also copes the PageData from the SearchResultsIndext to the SearchResultsPerPage doc
                SearchUtil.rebuildSearchResultsIndex(searchResultsIndex, searchResultsPerPage, iCurrentPage, COMConstants.CONS_PRODUCTS_PER_PAGE);

                //The page builder method contains all the business logic to calculate prices,
                //discounts, ship methods... for each of the products.  The page builder
                //methods puts all this information into the XML.  The method also sets
                //various flags that indicate whether or not popup messages needs to be
                //shown to the user.
                DOMUtil.addSection(document, SearchUtil.pageBuilder(
                      true, true, searchResultsPerPage,
                      COMConstants.CONS_PRODUCTS_PER_PAGE, null, null,
                      isSendToCustomerDomestic,  domesticServiceFee,  internationalServiceFee,
                      sendToCustomerState,  sendToCustomerCountry, pricingCode,
                      partnerId,  promotionList, rewardType, orderDeliveryZipCode,
                      flagVO, sourceCode, customOrderFlag,
                      origProductId, orderShipMethod).getChildNodes());
           }

            Document dataResponse = (Document) uoDAO.getAllPricePointXML();
            // Encode previously loaded price points
            DOMUtil.addSection(document, dataResponse.getChildNodes());

            dataResponse = (Document) uoDAO.getAllShippingMethodXML();
            DOMUtil.addSection(document, dataResponse.getChildNodes());

            if(pageNumber == null || pageNumber.length() <=0 || pageNumber.equalsIgnoreCase("init"))
            {
              pageNumber = "1";
            }


            // Encode page data for product list

            pageData.put("total_pages", Long.toString(pageCount));
            pageData.put("page_number", pageNumber);

        }
        finally
        {
        }

        return document;
	}


  /**
   * This method descodes certain values in an Document.  This is done because the DB
   * objects return the XML values encoded and they values need to be
   * decoded when they are displayed on the XSL page.  This is important
   * when displaying fields like product description which may have contain
   * symbols like the register trademark.
   *
   * @param String before
   * @return String de-coded value
   */
  public static Document fixXML(Document xml) throws Exception
  {
    Document fixed = xml;
    NodeList nl = DOMUtil.selectNodes(xml,"//PRODUCT");
    for ( int i = 0; i < nl.getLength(); i++ )
    {
        Element element = (Element) nl.item(i);
        String before = element.getAttribute("longdescription");
        element.setAttribute("longdescription",fixHTMLEncoding(before));
        before = element.getAttribute("novatorname");
        element.setAttribute("novatorname",fixHTMLEncoding(before));
        before = element.getAttribute("exceptionmessage");
        element.setAttribute("exceptionmessage",fixHTMLEncoding(before));
        before = element.getAttribute("secondchoice");
        element.setAttribute("secondchoice",fixHTMLEncoding(before));
        before = element.getAttribute("masterdescription");
        element.setAttribute("masterdescription",fixHTMLEncoding(before));
    }


    nl = DOMUtil.selectNodes(xml,"//UPSELLDETAIL");
    for ( int i = 0; i < nl.getLength(); i++ )
    {

        Element element = (Element) nl.item(i);
        String before = element.getAttribute("value");
        element.setAttribute("value",fixHTMLEncoding(before));

    }


    return fixed;
  }

  /**
   * This method decodes the XML encoding.  This is done because the DB
   * objects return the XML values encoded and they values need to be
   * decoded when they are displayed on the XSL page.  This is important
   * when displaying fields like product description which may have contain
   * symbols like the register trademark.
   *
   * @param String before
   * @return String de-coded value
   */
  public static String fixHTMLEncoding(String before)
  {
    //System.out.println("-----------------------------------------------------------------------");
    //System.out.println(before);
    String fixed = FieldUtils.replaceAll(before,"&lt;","<");
    fixed = FieldUtils.replaceAll(fixed,"&gt;",">");
    fixed = FieldUtils.replaceAll(fixed,"&amp;reg;","&reg;");
    fixed = FieldUtils.replaceAll(fixed,"&#39;","'");
    fixed = FieldUtils.replaceAll(fixed,"&amp;","&");
    //System.out.println(fixed);
    //System.out.println("-----------------------------------------------------------------------");
    return fixed;
  }

  /**
   * Main method used for testing.
   * @param String[] args
   * @returns void
   */
  public static void main(String[] args)
  {
    String before = "Send special wishes to those who have family members serving in the military,  or just brighten someone&amp;#39;s day with this vibrant yellow bouquet with yellow ribbon. Bouquet contains yellow lilies, yellow freesia, white roses, yellow alstroemeria and more.&amp;lt;BR&amp;gt;";
    String after = fixHTMLEncoding(before);
    System.out.println(after);

  }

  /**
   * Get a string representation for the value contained in the object.
   * @param Object
   * @returns String
   */
  private String getValue(Object obj)
  {

    String value = null;

    if(obj == null)
    {
      return null;
    }

    if(obj instanceof BigDecimal) {
      value = obj.toString();
    }
    else if(obj instanceof String) {
      value = (String)obj;
    }
    else if(obj instanceof Timestamp) {
      value = obj.toString();
    }
    else
    {
      value = obj.toString();
    }

    return value;
  }

 /**
  * This method creates an Document which contains all the addon
  * information found in the request object.
  *
  * @param HttpServletRequest request
  * @param Document xmlDoc
  * @returns Document
  */
//********************************************************************************************
// TESTED
//********************************************************************************************
 public Document appendAddons(HttpServletRequest request, Document xmlDoc)
 {
      Element temp = null;
      Element addon = null;
      String addonId = null;

      //header
      Element addonHeader = createElement(xmlDoc,"ADDONS");

	    //bear
      addonId = request.getParameter("orig_bear");
      if(addonId != null && addonId.length() > 0){
          addon = createElement(xmlDoc,"ADDON");
          temp = createElement(xmlDoc,"addon_id",addonId);
          addon.appendChild(temp);
          temp = createElement(xmlDoc,"addon_qty",request.getParameter("orig_bear_quantity"));
          addon.appendChild(temp);
          addonHeader.appendChild(addon);
      }

	    //baloon
      addonId = request.getParameter("orig_baloon");
      if(addonId != null && addonId.length() > 0){
          addon = createElement(xmlDoc,"ADDON");
          temp = createElement(xmlDoc,"addon_id",addonId);
          addon.appendChild(temp);
          temp = createElement(xmlDoc,"addon_qty",request.getParameter("orig_baloon_quantity"));
          addon.appendChild(temp);
          addonHeader.appendChild(addon);
      }

	    //chocolate
      addonId = request.getParameter("orig_chocolate");
      if(addonId != null && addonId.length() > 0){
          addon = createElement(xmlDoc,"ADDON");
          temp = createElement(xmlDoc,"addon_id",addonId);
          addon.appendChild(temp);
          temp = createElement(xmlDoc,"addon_qty",request.getParameter("orig_chocolate_quantity"));
          addon.appendChild(temp);
          addonHeader.appendChild(addon);
      }

	    //funeral_banner
      addonId = request.getParameter("orig_funeral_banner");
      if(addonId != null && addonId.length() > 0){
          addon = createElement(xmlDoc,"ADDON");
          temp = createElement(xmlDoc,"addon_id",addonId);
          addon.appendChild(temp);
          temp = createElement(xmlDoc,"addon_qty",request.getParameter("orig_funeral_banner_quantity"));
          addon.appendChild(temp);
          addonHeader.appendChild(addon);
      }

	    //greeting_card
      addonId = request.getParameter("orig_greeting_card");
      if(addonId != null && addonId.length() > 0){
          addon = createElement(xmlDoc,"ADDON");
          temp = createElement(xmlDoc,"addon_id",addonId);
          addon.appendChild(temp);
          addonHeader.appendChild(addon);
      }

      boolean dataFound = addonHeader.hasChildNodes();
      if (dataFound)
      {
        xmlDoc.getDocumentElement().appendChild(addonHeader);
      }

      return xmlDoc;
 }


  /** This method creates an XML element using the passed in field name and value.
   *  The passed in value is is appened to the the element as a Text node.
   *  The created element is NOT added to the passed in XML document.
   *  @param Document the document to which this element will be part of
   *  @param String field the name of the element to be added
   *  @param String the text value that should be added to the element
   *  @returns the created Element with the attached Text value */
    public static Element createElement(Document xmlDoc, String field, String value)
    {
            //build the field
            Element fieldElement = xmlDoc.createElement(field);
            Text textNode = xmlDoc.createTextNode(value);

            //append the text
            fieldElement.appendChild(textNode);

            return fieldElement;
    }

  /** This method creates an XML element using the passed in field name.
   *  A value\textnode is not appended to this element.
   *  The created element is NOT added to the passed in XML document.
   *  @param Document the document to which this element will be part of
   *  @param String field the name of the element to be added
   *  @returns the created Element with the attached Text value */
//********************************************************************************************
// TESTED
//********************************************************************************************
    public static Element createElement(Document xmlDoc, String field)
    {
            //build the field
            Element fieldElement = xmlDoc.createElement(field);

            return fieldElement;
    }

  /**
   *
   * @param String before
   * @return String de-coded value
   */
  public static Document fixXMLByRemovingNullAttributes(Document xml) throws Exception
  {

    Document fixed = xml;

    ArrayList aCheckList = new ArrayList();
    aCheckList.add("PRODUCT");
    aCheckList.add("UPSELLDETAIL");
    aCheckList.add("OEParameter");
    aCheckList.add("HOLIDAY");
    aCheckList.add("SHIPPINGMETHOD");

    int total = aCheckList.size();

    for (int count = 0; count < total; count++)
    {
      String nodeNameToRetrieve = "//" + aCheckList.get(count).toString();
      NodeList nl = DOMUtil.selectNodes(xml,nodeNameToRetrieve);
      int nlLength = nl.getLength();

      //go thru all the nodes
      for ( int i = 0; i < nlLength; i++ )
      {
        //get the node/element
        Node node = (Node) nl.item(i);
        Element element = (Element) nl.item(i);

        //get all the attributes in a map
        NamedNodeMap nMap = node.getAttributes();

        //get the total of all the attributes.
        int nMapLength = nMap.getLength();

        Node attribute;
        String name, value;
        ArrayList aList = new ArrayList();

        //pass thru all the attributes and add the null/blank ones to an array list
        for (int j=0; j < nMapLength; j++)
        {
          //retrieve the attribute
          attribute = nMap.item(j);

          //get the name of the attribute
          name = attribute.getNodeName();

          //get the value of the attribute
          value = attribute.getNodeValue();

          //if the attribute value is null or blanks, store the name in an array.
          if (value == null || value.equalsIgnoreCase(""))
            aList.add(name);
        }

        //get the array size of all the attributes that are null/blank
        int aListSize = aList.size();

        //go thru the array list of problem attributes, and remove them from the element.
        for (int k = 0; k < aListSize; k++)
        {
          element.removeAttribute(aList.get(k).toString());
        }
      }
    }

    //return the fixed xml.
    return fixed;
  }


}