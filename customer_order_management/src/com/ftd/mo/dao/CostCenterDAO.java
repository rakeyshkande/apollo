package com.ftd.mo.dao;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;

import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;
import java.util.HashMap;

/**
 * CostCenterDAO
 *
 * Interacts with Cost Center data.
 *
 */
public class CostCenterDAO
{
    private Connection          connection;
    private static Logger       logger  = new Logger("com.ftd.mo.dao.CostCenterDAO");

  /**
   * Constructor
   * @param connection Connection
   */
  public CostCenterDAO(Connection connection)
  {
    this.connection = connection;
  }


  /**
   * Determines if cost center information is valid
   *
   * @param costCenterId - Cost center information
   * @param partnerId - Partner id
   * @param sourceCode - Source code
	 * @return boolean - True if the cost center exists
   * @throws java.lang.Exception
   */
  public boolean costCenterExists(String costCenterId, String partnerId, 
		String sourceCode) throws Exception
  {
		DataRequest dataRequest = new DataRequest();
		dataRequest.setConnection(this.connection);
		dataRequest.setStatementID("COST_CENTER_EXISTS");
		dataRequest.addInputParam("inCostCenterId", costCenterId);
		dataRequest.addInputParam("inPartnerId", partnerId);
		dataRequest.addInputParam("inSourceCode", sourceCode);
		
		DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
		boolean result = Boolean.valueOf((String)dataAccessUtil.execute(dataRequest)).booleanValue();      
	  
		return result;		
  }
 
}