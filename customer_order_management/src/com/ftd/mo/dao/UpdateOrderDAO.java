package com.ftd.mo.dao;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.vo.CommentsVO;
import com.ftd.messaging.constants.MessagingConstants;
import com.ftd.messaging.vo.FTDMessageVO;
import com.ftd.mo.vo.DeliveryInfoVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.order.vo.AddOnsVO;
import com.ftd.osp.utilities.order.vo.CoBrandVO;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.order.vo.RecipientAddressesVO;
import com.ftd.osp.utilities.order.vo.RecipientsVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.customerordermanagement.vo.OrderBillVO;

import com.ftd.messaging.vo.MessageOrderStatusVO;

import java.io.IOException;

import java.math.BigDecimal;

import java.sql.Connection;

import java.sql.DriverManager;
import java.sql.SQLException;

import java.sql.Timestamp;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

//import oracle.xml.parser.v2.Document;
//
//

import org.apache.commons.lang.StringUtils;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/**
 * UpdateOrderDAO
 *
 * This is the DAO that handles loading/updating Order Update info.
 *
 */


public class UpdateOrderDAO
{
    private Connection          connection;
    private static Logger       logger  = new Logger("com.ftd.mo.dao.UpdateOrderDAO");
    private static final String NAME_DELIMITER = ",";

  /*
   * Constructor
   * @param connection Connection
   */
  public UpdateOrderDAO(Connection connection)
  {
    this.connection = connection;

  }


  /**
   * updateOrderDetailsUpdate
   *
   * Updates delivery info in order_details_update table.
   * Note that a row gets inserted into this table via getOrderDetailsUpdate
   * (when startUpdateFlag is Y).
   *
   * @param a_deliveryInfoVO
   * @throws java.lang.Exception
   */
  public void updateOrderDetailsUpdate(DeliveryInfoVO a_deliveryInfoVO) throws Exception
  {
    java.sql.Date deliveryDate = null;
    java.sql.Date deliveryDateRangeEnd = null;
    java.sql.Date shipDate = null;

    if (a_deliveryInfoVO.getDeliveryDate() != null) {
      deliveryDate = new java.sql.Date(a_deliveryInfoVO.getDeliveryDate().getTimeInMillis());
    }
    if (a_deliveryInfoVO.getDeliveryDateRangeEnd() != null) {
      deliveryDateRangeEnd = new java.sql.Date(a_deliveryInfoVO.getDeliveryDateRangeEnd().getTimeInMillis());
    }
    if (a_deliveryInfoVO.getShipDate() != null) {
      shipDate = new java.sql.Date(a_deliveryInfoVO.getShipDate().getTimeInMillis());
    }
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(this.connection);
    dataRequest.setStatementID("UPDATE_ORDER_DETAILS_UPDATE");
    dataRequest.addInputParam("IN_ORDER_DETAIL_ID", a_deliveryInfoVO.getOrderDetailId());
    dataRequest.addInputParam("IN_UPDATED_BY", a_deliveryInfoVO.getUpdatedBy());
    dataRequest.addInputParam("IN_SOURCE_CODE", a_deliveryInfoVO.getSourceCode());
    dataRequest.addInputParam("IN_DELIVERY_DATE", deliveryDate);
    dataRequest.addInputParam("IN_DELIVERY_DATE_RANGE_END", deliveryDateRangeEnd);
    dataRequest.addInputParam("IN_CARD_MESSAGE", a_deliveryInfoVO.getCardMessage());
    dataRequest.addInputParam("IN_CARD_SIGNATURE", a_deliveryInfoVO.getCardSignature());
    dataRequest.addInputParam("IN_SPECIAL_INSTRUCTIONS", a_deliveryInfoVO.getSpecialInstructions());
    dataRequest.addInputParam("IN_SHIP_METHOD", a_deliveryInfoVO.getShipMethod());
    dataRequest.addInputParam("IN_SHIP_DATE", shipDate);
    dataRequest.addInputParam("IN_MEMBERSHIP_NUMBER", a_deliveryInfoVO.getMembershipNumber());
    dataRequest.addInputParam("IN_MEMBERSHIP_FIRST_NAME", a_deliveryInfoVO.getMembershipFirstName());
    dataRequest.addInputParam("IN_MEMBERSHIP_LAST_NAME", a_deliveryInfoVO.getMembershipLastName());
    dataRequest.addInputParam("IN_RECIPIENT_ID", a_deliveryInfoVO.getRecipientId());
    dataRequest.addInputParam("IN_RECIPIENT_PHONE_NUMBER", a_deliveryInfoVO.getRecipientPhoneNumber());
    dataRequest.addInputParam("IN_RECIPIENT_EXTENSION", a_deliveryInfoVO.getRecipientExtension());
    dataRequest.addInputParam("IN_RECIPIENT_FIRST_NAME", a_deliveryInfoVO.getRecipientFirstName());
    dataRequest.addInputParam("IN_RECIPIENT_LAST_NAME", a_deliveryInfoVO.getRecipientLastName());
    dataRequest.addInputParam("IN_RECIPIENT_ADDRESS_1", a_deliveryInfoVO.getRecipientAddress1());
    dataRequest.addInputParam("IN_RECIPIENT_ADDRESS_2", a_deliveryInfoVO.getRecipientAddress2());
    dataRequest.addInputParam("IN_RECIPIENT_CITY", a_deliveryInfoVO.getRecipientCity());
    dataRequest.addInputParam("IN_RECIPIENT_STATE", a_deliveryInfoVO.getRecipientState());
    dataRequest.addInputParam("IN_RECIPIENT_ZIP_CODE", a_deliveryInfoVO.getRecipientZipCode());
    dataRequest.addInputParam("IN_RECIPIENT_COUNTRY", a_deliveryInfoVO.getRecipientCountry());
    dataRequest.addInputParam("IN_RECIPIENT_ADDRESS_TYPE", a_deliveryInfoVO.getRecipientAddressType());
    dataRequest.addInputParam("IN_RECIPIENT_BUSINESS_NAME", a_deliveryInfoVO.getRecipientBusinessName());
    dataRequest.addInputParam("IN_RECIPIENT_BUSINESS_INFO", a_deliveryInfoVO.getRecipientBusinessInfo());
    dataRequest.addInputParam("IN_RELEASE_INFO_INDICATOR", a_deliveryInfoVO.getReleaseInfoIndicator());
    dataRequest.addInputParam("IN_FLORIST_ID", a_deliveryInfoVO.getFloristId());

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

    Map outputs = (Map) dataAccessUtil.execute(dataRequest);

    String status = (String) outputs.get(COMConstants.STATUS_PARAM);
    if( StringUtils.equals(status, "N") )
    {
        String message = (String) outputs.get(COMConstants.MESSAGE_PARAM);
        throw new Exception(message);
    }
    logger.debug("Update Order Details Update Successful");
  }


  /**
   * updateODUAmounts
   *
   * Updates product amount information in order_details_update table.
   *
   * @param a_orderDetailsVO - contains pricing info to save to database
   * @throws java.lang.Exception
   */
  public void updateODUAmounts(
                OrderDetailsVO odvo, OrderBillVO obvo, String orderDetailId, String csrId) throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(this.connection);
    dataRequest.setStatementID("UPDATE_PRODUCT_AMOUNTS_NEW");
    dataRequest.addInputParam("IN_ORDER_DETAIL_ID", orderDetailId);
    dataRequest.addInputParam("IN_UPDATED_BY", csrId);
    dataRequest.addInputParam("IN_PRODUCT_AMOUNT", odvo.getProductsAmount());
    dataRequest.addInputParam("IN_ADD_ON_AMOUNT", odvo.getAddOnAmount());
    dataRequest.addInputParam("IN_SERVICE_FEE", odvo.getServiceFeeAmount());
    dataRequest.addInputParam("IN_SHIPPING_FEE", odvo.getShippingFeeAmount());
    dataRequest.addInputParam("IN_DISCOUNT_AMOUNT", odvo.getDiscountAmount());
    dataRequest.addInputParam("IN_SHIPPING_TAX", odvo.getShippingTax());
    dataRequest.addInputParam("IN_TAX", odvo.getTaxAmount());

    //update only if the new and old addon amounts are different
    double dNewAddOnAmount = new Double(odvo.getAddOnAmount()).doubleValue();
    if(obvo.getAddOnAmount() != dNewAddOnAmount)
      dataRequest.addInputParam("IN_ACTUAL_ADD_ON_AMOUNT", odvo.getAddOnAmount());
    else
      dataRequest.addInputParam("IN_ACTUAL_ADD_ON_AMOUNT", null);


    //update only if the new and old discount amounts are different
    double dNewDiscountAmount = new Double(odvo.getDiscountAmount()).doubleValue();
    if(obvo.getDiscountAmount() != dNewDiscountAmount)
      dataRequest.addInputParam("IN_ACTUAL_DISCOUNT_AMOUNT", odvo.getDiscountAmount());
    else
      dataRequest.addInputParam("IN_ACTUAL_DISCOUNT_AMOUNT", null);


    //update only if the new and old product amounts are different
    double dNewProductAmount = new Double(odvo.getProductsAmount()).doubleValue();
    if(obvo.getProductAmount() != dNewProductAmount)
      dataRequest.addInputParam("IN_ACTUAL_PRODUCT_AMOUNT", odvo.getProductsAmount());
    else
      dataRequest.addInputParam("IN_ACTUAL_PRODUCT_AMOUNT", null);


    //update only if the new and old service amounts are different
    double dNewServiceFeeAmount = new Double(odvo.getServiceFeeAmount()).doubleValue();
    if(obvo.getServiceFee() != dNewServiceFeeAmount)
      dataRequest.addInputParam("IN_ACTUAL_SERVICE_FEE", odvo.getServiceFeeAmount());
    else
      dataRequest.addInputParam("IN_ACTUAL_SERVICE_FEE", null);


    //update only if the new and old shipping amounts are different
    double dNewShippingFeeAmount = new Double(odvo.getShippingFeeAmount()).doubleValue();
    if(obvo.getShippingFee() != dNewShippingFeeAmount)
      dataRequest.addInputParam("IN_ACTUAL_SHIPPING_FEE", odvo.getShippingFeeAmount());
    else
      dataRequest.addInputParam("IN_ACTUAL_SHIPPING_FEE", null);


    //update only if the new and old shipping tax amounts are different
    double dNewShippingTaxAmount = new Double(odvo.getShippingTax()).doubleValue();
    if(obvo.getShippingTax() != dNewShippingTaxAmount)
      dataRequest.addInputParam("IN_ACTUAL_SHIPPING_TAX", odvo.getShippingTax());
    else
      dataRequest.addInputParam("IN_ACTUAL_SHIPPING_TAX", null);


    //update only if the new and old tax amounts are different
    double dNewTaxAmount = new Double(odvo.getTaxAmount()).doubleValue();
    if(obvo.getTax() != dNewTaxAmount)
      dataRequest.addInputParam("IN_ACTUAL_TAX", odvo.getTaxAmount());
    else
      dataRequest.addInputParam("IN_ACTUAL_TAX", null);

    dataRequest.addInputParam("IN_SERVICE_FEE_SAVED", odvo.getServiceFeeAmountSavings());
    dataRequest.addInputParam("IN_SHIPPING_FEE_SAVED", odvo.getShippingFeeAmountSavings());

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

 
    Map outputs = (Map) dataAccessUtil.execute(dataRequest);

    String status = (String) outputs.get(COMConstants.STATUS_PARAM);
    if( StringUtils.equals(status, "N") )
    {
        String message = (String) outputs.get(COMConstants.MESSAGE_PARAM);
        throw new Exception(message);
    }
    logger.debug("Update Order Details Update Products Amount Successful");
  }


  /**
   * Ali Lakhani
   * resetODUAmounts
   *
   * Updates product amount information in order_details_update table.
   *
   * @param a_orderDetailsVO - contains pricing info to save to database
   * @throws java.lang.Exception
   */
  public void resetODUAmounts(
                String productAmount, String orderDetailId, String csrId) throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(this.connection);
    dataRequest.setStatementID("UPDATE_PRODUCT_AMOUNTS_NEW");
    dataRequest.addInputParam("IN_ORDER_DETAIL_ID",       orderDetailId);
    dataRequest.addInputParam("IN_UPDATED_BY",            csrId);
    dataRequest.addInputParam("IN_PRODUCT_AMOUNT",        productAmount);
    dataRequest.addInputParam("IN_ADD_ON_AMOUNT",         "0");
    dataRequest.addInputParam("IN_SERVICE_FEE",           "0");
    dataRequest.addInputParam("IN_SHIPPING_FEE",          "0");
    dataRequest.addInputParam("IN_DISCOUNT_AMOUNT",       "0");
    dataRequest.addInputParam("IN_SHIPPING_TAX",          "0");
    dataRequest.addInputParam("IN_TAX",                   "0");
    dataRequest.addInputParam("IN_ACTUAL_PRODUCT_AMOUNT", null);
    dataRequest.addInputParam("IN_ACTUAL_ADD_ON_AMOUNT",  null);
    dataRequest.addInputParam("IN_ACTUAL_SERVICE_FEE",    null);
    dataRequest.addInputParam("IN_ACTUAL_SHIPPING_FEE",   null);
    dataRequest.addInputParam("IN_ACTUAL_DISCOUNT_AMOUNT",null);
    dataRequest.addInputParam("IN_ACTUAL_SHIPPING_TAX",   null);
    dataRequest.addInputParam("IN_ACTUAL_TAX",            null);
    dataRequest.addInputParam("IN_SERVICE_FEE_SAVED",     null);
    dataRequest.addInputParam("IN_SHIPPING_FEE_SAVED",    null);
    
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
 
    Map outputs = (Map) dataAccessUtil.execute(dataRequest);

    String status = (String) outputs.get(COMConstants.STATUS_PARAM);
    if( StringUtils.equals(status, "N") )
    {
        String message = (String) outputs.get(COMConstants.MESSAGE_PARAM);
        throw new Exception(message);
    }
    logger.debug("Reset Products Amount Successful");
  }


  /**
   * updateOrderContactUpdate
   *
   * Updates order contact info in order_contact_update table.
   *
   * @param a_deliveryInfoVO
   * @throws java.lang.Exception
   */
  public void updateOrderContactUpdate(DeliveryInfoVO a_deliveryInfoVO) throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(this.connection);
    dataRequest.setStatementID("UPDATE_ORDER_CONTACT_UPDATE");
    dataRequest.addInputParam("IN_ORDER_CONTACT_INFO_ID", a_deliveryInfoVO.getAltContactInfoId());
    dataRequest.addInputParam("IN_UPDATED_BY", a_deliveryInfoVO.getUpdatedBy());
    dataRequest.addInputParam("IN_FIRST_NAME", a_deliveryInfoVO.getAltContactFirstName());
    dataRequest.addInputParam("IN_LAST_NAME", a_deliveryInfoVO.getAltContactLastName());
    dataRequest.addInputParam("IN_PHONE_NUMBER", a_deliveryInfoVO.getAltContactPhoneNumber());
    dataRequest.addInputParam("IN_EXTENSION", a_deliveryInfoVO.getAltContactExtension());
    dataRequest.addInputParam("IN_EMAIL_ADDRESS", a_deliveryInfoVO.getAltContactEmail());

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

 
    Map outputs = (Map) dataAccessUtil.execute(dataRequest);

    String status = (String) outputs.get(COMConstants.STATUS_PARAM);
    if( StringUtils.equals(status, "N") )
    {
        String message = (String) outputs.get(COMConstants.MESSAGE_PARAM);
        throw new Exception(message);
    }
    logger.debug("Update Order Contact Update Successful");
  }


  /**
   * insertOrderContactUpdate
   *
   * Insert order contact info in order_contact_update table.
   *
   * @param a_deliveryInfoVO
   * @throws java.lang.Exception
   */
  public void insertOrderContactUpdate(DeliveryInfoVO a_deliveryInfoVO) throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(this.connection);
    dataRequest.setStatementID("INSERT_ORDER_CONTACT_UPDATE");
    dataRequest.addInputParam("IN_ORDER_GUID", a_deliveryInfoVO.getOrderGuid());
    dataRequest.addInputParam("IN_UPDATED_BY", a_deliveryInfoVO.getUpdatedBy());
    dataRequest.addInputParam("IN_FIRST_NAME", a_deliveryInfoVO.getAltContactFirstName());
    dataRequest.addInputParam("IN_LAST_NAME", a_deliveryInfoVO.getAltContactLastName());
    dataRequest.addInputParam("IN_PHONE_NUMBER", a_deliveryInfoVO.getAltContactPhoneNumber());
    dataRequest.addInputParam("IN_EXTENSION", a_deliveryInfoVO.getAltContactExtension());
    dataRequest.addInputParam("IN_EMAIL_ADDRESS", a_deliveryInfoVO.getAltContactEmail());
    dataRequest.addInputParam("IN_ORDER_DETAIL_ID", a_deliveryInfoVO.getOrderDetailId());
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

 
    Map outputs = (Map) dataAccessUtil.execute(dataRequest);

    String status = (String) outputs.get(COMConstants.STATUS_PARAM);
    if( StringUtils.equals(status, "N") )
    {
        String message = (String) outputs.get(COMConstants.MESSAGE_PARAM);
        throw new Exception(message);
    }
    logger.debug("Insert Order Contact Update Successful");
  }


  /**
   * insertCommentsUpdate
   *
   * Inserts comment record into comments_update table.
   *
   * @param a_commentVO
   * @return Comment ID
   * @throws java.lang.Exception
   */
  public String insertCommentsUpdate(CommentsVO a_commentVO) throws Exception
  {
    String commentId = null;
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(this.connection);
    dataRequest.setStatementID("INSERT_COMMENTS_UPDATE");
    dataRequest.addInputParam("IN_CUSTOMER_ID", a_commentVO.getCustomerId());
    dataRequest.addInputParam("IN_ORDER_GUID", a_commentVO.getOrderGuid());
    dataRequest.addInputParam("IN_ORDER_DETAIL_ID", a_commentVO.getOrderDetailId());
    dataRequest.addInputParam("IN_COMMENT_ORIGIN", a_commentVO.getCommentOrigin());
    dataRequest.addInputParam("IN_DNIS_ID", a_commentVO.getDnisId());
    dataRequest.addInputParam("IN_COMMENT_TEXT", a_commentVO.getComment());
    dataRequest.addInputParam("IN_COMMENT_TYPE", a_commentVO.getCommentType());
    dataRequest.addInputParam("IN_UPDATED_BY", a_commentVO.getUpdatedBy());

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

 
    Map outputs = (Map) dataAccessUtil.execute(dataRequest);

    String status = (String) outputs.get(COMConstants.STATUS_PARAM);
    if( StringUtils.equals(status, "N") )
    {
        String message = (String) outputs.get(COMConstants.MESSAGE_PARAM);
        throw new Exception(message);
    }
    commentId = (String) outputs.get("OUT_COMMENT_ID");
    logger.debug("Insert Comments Update Successful");
    return commentId;
  }

  /**
   * updateCommentsUpdate
   *
   * Updates comment info in the comments_update table.
   *
   * @param a_commentsVO
   * @throws java.lang.Exception
   */
  public void updateCommentsUpdate(CommentsVO a_commentVO) throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(this.connection);
    
    
    
    dataRequest.setStatementID("MO_UPDATE_COMMENTS_UPDATE");
    dataRequest.addInputParam("IN_COMMENT_ID", a_commentVO.getCommentId());
    dataRequest.addInputParam("IN_COMMENT_TEXT", a_commentVO.getComment());
    dataRequest.addInputParam("IN_COMMENT_TYPE", a_commentVO.getCommentType());
    dataRequest.addInputParam("IN_UPDATED_BY", a_commentVO.getUpdatedBy());

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
 
    Map outputs = (Map) dataAccessUtil.execute(dataRequest);

    String status = (String) outputs.get(COMConstants.STATUS_PARAM);
    if( StringUtils.equals(status, "N") )
    {
        String message = (String) outputs.get(COMConstants.MESSAGE_PARAM);
        throw new Exception(message);
    }
    logger.debug("Update Comment Update Successful");
  }

  /**
   * cancelUpdateOrder
   *
   * @param a_orderDetailId
   * @throws java.lang.Exception
   */
   public void cancelUpdateOrder(String a_orderDetailId) throws Exception
   {
       cancelUpdateOrder(a_orderDetailId,"N");
   }


  /**
   * cancelUpdateOrder
   *
   * @param a_orderDetailId
   * @throws java.lang.Exception
   */
   public void cancelUpdateOrder(String a_orderDetailId,String updateRecipIndicator) throws Exception
   {
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(this.connection);
    dataRequest.setStatementID("CANCEL_UPDATE_ORDER");
    dataRequest.addInputParam("IN_ORDER_DETAIL_ID", a_orderDetailId);
    dataRequest.addInputParam("IN_UPDATE_RECIPIENT_IND", updateRecipIndicator);

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

 
    Map outputs = (Map) dataAccessUtil.execute(dataRequest);

    String status = (String) outputs.get(COMConstants.STATUS_PARAM);
    if( StringUtils.equals(status, "N") )
    {
        String message = (String) outputs.get(COMConstants.MESSAGE_PARAM);
        throw new Exception(message);
    }
    logger.debug("Cancel Update Successful");
   }


  /**
   * commitUpdateOrder
   *
   * @param a_orderDetailId
   * @param updateRecipIndicator
   * @throws java.lang.Exception
   */
   public void commitUpdateOrder(String a_orderDetailId,String updateRecipIndicator) throws Exception
   {
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(this.connection);
    dataRequest.setStatementID("COMMIT_UPDATE_ORDER");
    dataRequest.addInputParam("IN_ORDER_DETAIL_ID", a_orderDetailId);

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

 
    Map outputs = (Map) dataAccessUtil.execute(dataRequest);

    String status = (String) outputs.get(COMConstants.STATUS_PARAM);
    if( StringUtils.equals(status, "N") )
    {
        String message = (String) outputs.get(COMConstants.MESSAGE_PARAM);
        throw new Exception(message);
    }
    logger.debug("Commit Update Successful");
   }

  /**
   * getOrderDetailsUpdate
   *
   * This gets different types of order detail information (based on
   * in_details_type) for the specified order ID.
   *
   * @param a_orderDetailId   - Order detail ID
   * @param a_detailsType     - Type of order detail info to retrieve
   * @param a_startUpdateFlag - If true, info is copied from original order
   * @param a_updatedBy       - Updated by
   * @param a_productId       - Product ID (optional, if none is present, CLEAN.ORDER_DETAILS.PRODUCT_ID will be used)
   * @return hashmap containing CachedResultSets with the keys 
   * OUT_ORIG_ADD_ONS
   * OUT_UPD_ORDER_CUR
   * OUT_UPD_ADDON_CUR
   * OUT_UPD_COMMENTS_CUR
   * OUT_PAYMENTS_CUR
   * OUT_ORDER_EXTENSIONS_CUR
   * 
   * 
   * @throws java.lang.Exception
   */
  public HashMap<String,CachedResultSet> getOrderDetailsUpdate(String a_orderDetailId, String a_detailsType,
                                        boolean a_startUpdateFlag, String a_updatedBy, String a_productId) throws Exception
  {
    String startUpdateFlagStr = "N";
    if (a_startUpdateFlag) {
      startUpdateFlagStr = "Y";
    }
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(this.connection);
    dataRequest.setStatementID("GET_ORDER_DETAILS_UPDATE");
    dataRequest.addInputParam("IN_ORDER_DETAIL_ID", a_orderDetailId);
    dataRequest.addInputParam("IN_INFO_TYPE", a_detailsType);
    dataRequest.addInputParam("IN_START_UPDATE", startUpdateFlagStr);
    dataRequest.addInputParam("IN_UPDATED_BY", a_updatedBy);
    dataRequest.addInputParam("IN_PRODUCT_ID", a_productId);

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

 
    HashMap<String,CachedResultSet> orderDetailsMap = (HashMap)dataAccessUtil.execute(dataRequest);

    return orderDetailsMap;
  }


  /**
   * buildRecalculateOrderDetailsVO
   *
   * Builds a OrderDetailsVO for use in recalculating pricing info.
   * An OrderDetailsVO is built based on the updated order detail ID
   * passed in.
   * Note that the resulting OrderDetailsVO should only be relied on for
   * the pricing info.  The pertinent returned attributes are:
   *     productsAmount
   *     taxAmount
   *     serviceFeeAmount
   *     shippingFeeAmount
   *     addOnAmount
   *     discountAmount
   *     milesPoints
   *
   * This method is primarily used by getRecalculatedUpdatePricingInfo, but
   * is public so any desired changes to the VO can be made before recalculating
   * (this is used in DeliveryInfoBO since recipient info is not in updated tables yet).
   *
   * @param a_orderDetailId - Order detail ID
   * @return OrderDetailsVO - Contains recalc'ed price info for updated order detail
   * @throws java.lang.Exception
   */
  public OrderDetailsVO buildRecalculateOrderDetailsVO(String a_orderDetailId) throws Exception
  {
    CachedResultSet rsDi = null;
    CachedResultSet rsPd = null;
    CachedResultSet rsAdd = null;
    String productId = null;
    OrderDetailsVO odvo = new OrderDetailsVO();

    // Get Delivery Info cursor (for updated order)
    //
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(this.connection);
    dataRequest.setStatementID("GET_ORDER_DETAILS_UPDATE");
    dataRequest.addInputParam("IN_ORDER_DETAIL_ID", a_orderDetailId);
    dataRequest.addInputParam("IN_INFO_TYPE", "DELIVERY_INFO");
    dataRequest.addInputParam("IN_START_UPDATE", "N");
    dataRequest.addInputParam("IN_UPDATED_BY", "");
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    HashMap orderDetailsMap = (HashMap)dataAccessUtil.execute(dataRequest);

    // Get Product Detail cursor (containing product/addons associated with updated order)
    //
    DataRequest dataRequest2 = new DataRequest();
    dataRequest2.setConnection(this.connection);
    dataRequest2.setStatementID("GET_ORDER_DETAILS_UPDATE");
    //dataRequest2.setStatementID("VIEW_PRODUCT_DETAIL");
    dataRequest2.addInputParam("IN_ORDER_DETAIL_ID", a_orderDetailId);
    dataRequest2.addInputParam("IN_INFO_TYPE", "PRODUCT_DETAIL");
    dataRequest2.addInputParam("IN_START_UPDATE", "N");
    dataRequest2.addInputParam("IN_UPDATED_BY", "");
    DataAccessUtil dataAccessUtil2 = DataAccessUtil.getInstance();
    HashMap productDetailsMap = (HashMap)dataAccessUtil2.execute(dataRequest2);

    try
    {
      rsDi = (CachedResultSet) orderDetailsMap.get("OUT_UPD_ORDER_CUR");
      rsPd = (CachedResultSet) productDetailsMap.get("OUT_UPD_ORDER_CUR");
      rsAdd = (CachedResultSet) productDetailsMap.get("OUT_UPD_ADDON_CUR");
      rsDi.next();

      // These are the fields that are absolutely essential to recalculating price
      //
      odvo.setSourceCode(rsDi.getString(COMConstants.UDI_SOURCE_CODE));
      odvo.setShipMethod(rsDi.getString(COMConstants.UDI_SHIP_METHOD));
      odvo.setNoTaxFlag(rsDi.getString(COMConstants.UDI_NO_TAX_FLAG));
      odvo.setLineNumber("1");  // Set to 1 since we're recalc'ing for single line item
      odvo.setOriginalOrderHasSDU(rsDi.getString("original_order_has_sdu"));

      RecipientsVO recipient = new RecipientsVO();
      RecipientAddressesVO recipientAddr = new RecipientAddressesVO();
      recipient.setFirstName(rsDi.getString(COMConstants.UDI_RECIPIENT_FIRST_NAME));
      recipient.setLastName(rsDi.getString(COMConstants.UDI_RECIPIENT_LAST_NAME));
      recipientAddr.setAddressType(rsDi.getString(COMConstants.UDI_RECIPIENT_ADDRESS_TYPE));
      recipientAddr.setAddressLine1(rsDi.getString(COMConstants.UDI_RECIPIENT_ADDRESS_1));
      recipientAddr.setAddressLine2(rsDi.getString(COMConstants.UDI_RECIPIENT_ADDRESS_2));
      recipientAddr.setCity(rsDi.getString(COMConstants.UDI_RECIPIENT_CITY));
      recipientAddr.setStateProvince(rsDi.getString(COMConstants.UDI_RECIPIENT_STATE));
      recipientAddr.setPostalCode(rsDi.getString(COMConstants.UDI_RECIPIENT_ZIP_CODE));
      recipientAddr.setCountry(rsDi.getString(COMConstants.UDI_RECIPIENT_COUNTRY));
      Collection recipients = new ArrayList();
      Collection recipientAddrs = new ArrayList();
      recipientAddrs.add(recipientAddr);
      recipient.setRecipientAddresses((List)recipientAddrs);
      recipients.add(recipient);
      odvo.setRecipients((List)recipients);

      Collection addOns = new ArrayList();
      int count = 0;
      while(rsPd.next())
      {
        count++;
        if (count == 1)
        {
          productId = rsPd.getString(COMConstants.UDI_PRODUCT_ID);
          odvo.setProductId(productId);
          odvo.setProductsAmount(rsPd.getString("product_amount"));
          odvo.setSizeChoice(rsPd.getString(COMConstants.UDI_SIZE_INDICATOR));
          // These are not used in recalc, but are saved in case caller wants originals
          odvo.setTaxAmount(rsPd.getString("tax"));
          odvo.setServiceFeeAmount(rsPd.getString("service_fee"));
          odvo.setShippingFeeAmount(rsPd.getString("shipping_fee"));
          odvo.setDiscountAmount(rsPd.getString("discount_amount"));
          odvo.setShippingTax(rsPd.getString("shipping_tax"));
          
        }
      }
      while (rsAdd.next())
      {
        AddOnsVO addOn = new AddOnsVO();
        String addOnId = rsAdd.getString("order_add_on_id");
        if (addOnId != null && addOnId.length() > 0)
        {
          addOn.setAddOnId(Long.parseLong(rsAdd.getString("order_add_on_id")));
          addOn.setAddOnCode(rsAdd.getString("add_on_code"));
          addOn.setAddOnQuantity(rsAdd.getString("add_on_quantity"));
          addOns.add(addOn);
        }
      }
      odvo.setAddOns((List)addOns);

      // Get product info
      //
      CachedResultSet crs = getProductInfo(productId);
      crs.next();
      odvo.setProductType(crs.getString("productType"));
      odvo.setShipMethodCarrierFlag(crs.getString("shipMethodCarrier"));

      // These fields are not needed for recalc.  Uncomment if you need 'em.
      //
      //odvo.setOrderDetailId(Long.parseLong(rsDi.getString(COMConstants.UDI_ORDER_DETAIL_ID)));
      //odvo.setGuid(rsDi.getString(COMConstants.UDI_ORDER_GUID));
      //odvo.setSourceDescription(rsDi.getString(COMConstants.UDI_SOURCE_CODE_DESCRIPTION));
      //odvo.setDeliveryDate(rsDi.getString(COMConstants.UDI_DELIVERY_DATE));
      //odvo.setDeliveryDateRangeEnd(rsDi.getString(COMConstants.UDI_DELIVERY_DATE_RANGE_END));
      //odvo.setCardMessage(rsDi.getString(COMConstants.UDI_CARD_MESSAGE));
      //odvo.setCardSignature(rsDi.getString(COMConstants.UDI_CARD_SIGNATURE));
      //odvo.setShipDate(rsDi.getString(COMConstants.UDI_SHIP_DATE));
      //odvo.setSpecialInstructions(rsDi.getString(COMConstants.UDI_SPECIAL_INSTRUCTIONS));
      //odvo.setRecipientId(rsDi.getString(COMConstants.UDI_RECIPIENT_ID));

    }
    catch (Exception e)
    {
      throw e;
    }

    return odvo;
  }


  /**
   * getFloristProductInfo
   *
   * This gets florist info (including block type related to product ID)
   * for specified florist.  If florist doesn't cover any zips and passed city
   * is not efos city for florist, then nothing is returned.
   *
   * @param a_floristId - Florist ID
   * @param a_productId - Product ID
   * @param a_city      - Delivery city
   * @throws java.lang.Exception
   */
   public CachedResultSet getFloristProductInfo(String a_floristId, String a_productId,
                                                 String a_zip, String a_city, Calendar a_deliveryDate) throws Exception
   {
    java.sql.Date dDeliveryDate = null;
    if (a_deliveryDate != null) {
      dDeliveryDate = new java.sql.Date(a_deliveryDate.getTimeInMillis());
    }
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(this.connection);
    dataRequest.setStatementID("GET_ROUTABLE_FLORIST_BY_ID");
    dataRequest.addInputParam("IN_PRODUCT_ID", a_productId);
    dataRequest.addInputParam("IN_FLORIST_ID", a_floristId);
    dataRequest.addInputParam("IN_ZIP_CODE", StringUtils.isNotEmpty(a_zip)?a_zip.toUpperCase():a_zip);
    dataRequest.addInputParam("IN_CITY_NAME", a_city);
    dataRequest.addInputParam("IN_DELIVERY_DATE", dDeliveryDate);

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

    CachedResultSet searchResults = (CachedResultSet) dataAccessUtil.execute(dataRequest);

    return searchResults;
   }


 /**
  * getUpdatedDeliveryInfoVO()
  *
  * Get updated delivery info from database and place in DeliveryInfoVO.
  *
  * @param a_orderDetailId - Order detail ID.
  * @return DeliveryInfoVO
  * @throws Exception
  */
  public DeliveryInfoVO getUpdatedDeliveryInfoVO(String a_orderDetailId) throws Exception
  {
    CachedResultSet rs = null;
    DeliveryInfoVO divo = null;
    HashMap deliveryInfoMap = this.getOrderDetailsUpdate(
                              a_orderDetailId,
                              COMConstants.CONS_ORDER_DETAILS_UPDATE_TYPE,
                              false, null, null);

    // Build DeliveryInfoVO from CachedResultSet
    //
    try
    {
      rs = (CachedResultSet) deliveryInfoMap.get("OUT_CUR");
      rs.next();
      divo = buildDeliveryInfoVOFromDb(rs);
      // Also save customer ID in case needed
      divo.setCustomerId(rs.getString(COMConstants.CUSTOMER_ID));
    }
    catch (Exception e)
    {
      throw e;
    }

    return divo;
  }


 /**
  * getOriginalDeliveryInfoVO()
  *
  * Get original delivery info from database and place in DeliveryInfoVO.
  * This is used when comparing updated info against original info.
  *
  * @param a_orderDetailId - Order detail ID.
  * @return DeliveryInfoVO
  * @throws Exception
  */
  public DeliveryInfoVO getOriginalDeliveryInfoVO(String a_orderDetailId) throws Exception
  {
    CachedResultSet rs = null;
    DeliveryInfoVO divo = null;
    CachedResultSet deliveryInfoRs = this.getOriginalDeliveryInfo(a_orderDetailId);

    // Build DeliveryInfoVO from CachedResultSet
    //
    try
    {
      deliveryInfoRs.next();
      divo = buildDeliveryInfoVOFromDb(deliveryInfoRs);
    }
    catch (Exception e)
    {
      throw e;
    }
    return divo;
  }


  /**
   * getOriginalDeliveryInfo()
   *
   * Get original delivery info from database.  This is used when comparing
   * updated delivery info against original info.
   *
   * @param a_orderDetailId   - Order detail ID
   * @return hashmap containing cursors for order details
   * @throws java.lang.Exception
   */
  public CachedResultSet getOriginalDeliveryInfo(String a_orderDetailId) throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(this.connection);
    dataRequest.setStatementID("GET_ORIGINAL_DELIVERY_INFO");
    dataRequest.addInputParam("IN_ORDER_DETAIL_ID", a_orderDetailId);

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
 
    CachedResultSet rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);

    return rs;
  }

  /**
     *  Get the additionial bills for an order detail record
     * @param orderDetailId
     * @return
     * @throws java.lang.Exception
     */
  public HashMap getAdditionialBills(String orderDetailId) throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(this.connection);
    dataRequest.setStatementID("VIEW_ADDTNL_ORDER_BILLS");
    dataRequest.addInputParam("IN_ORDER_DETAIL_ID", orderDetailId);

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

    HashMap billMap = (HashMap)dataAccessUtil.execute(dataRequest);

    return billMap;
  }




 /**
  *
  * buildDeliveryInfoVOFromDb()
  *
  * Internal method to help construct DeliveryInfoVO from result set
  *
  */
  private DeliveryInfoVO buildDeliveryInfoVOFromDb(CachedResultSet rs) throws Exception
  {
    Calendar cUpdatedOn = null;
    Calendar cDeliveryDate = null;
    Calendar cDeliveryDateRangeEnd = null;
    Calendar cShipDate = null;
    DeliveryInfoVO divo = new DeliveryInfoVO();

    // Get delivery info from result set and place in VO
    //

    // Convert Date's to Calendar's first
    //
    if (rs.getObject(COMConstants.UDI_UPDATED_ON) != null) {
      cUpdatedOn = Calendar.getInstance();
      cUpdatedOn.setTime((Date) rs.getObject(COMConstants.UDI_UPDATED_ON));
    }
    if (rs.getObject(COMConstants.UDI_DELIVERY_DATE) != null) {
      cDeliveryDate = Calendar.getInstance();
      cDeliveryDate.setTime((Date) rs.getObject(COMConstants.UDI_DELIVERY_DATE));
    }
    if (rs.getObject(COMConstants.UDI_DELIVERY_DATE_RANGE_END) != null) {
      cDeliveryDateRangeEnd = Calendar.getInstance();
      cDeliveryDateRangeEnd.setTime((Date) rs.getObject(COMConstants.UDI_DELIVERY_DATE_RANGE_END));
    }
    if (rs.getObject(COMConstants.UDI_SHIP_DATE) != null) {
      cShipDate = Calendar.getInstance();
      cShipDate.setTime((Date) rs.getObject(COMConstants.UDI_SHIP_DATE));
    }

    // Now put everything in VO
    //
    divo.setOrderGuid(rs.getString(COMConstants.UDI_ORDER_GUID));
    divo.setOrderDetailId(rs.getString(COMConstants.UDI_ORDER_DETAIL_ID));
    divo.setUpdatedOn(cUpdatedOn);
    divo.setUpdatedBy(rs.getString(COMConstants.UDI_UPDATED_BY));
    divo.setSourceCode(rs.getString(COMConstants.UDI_SOURCE_CODE));
    divo.setDeliveryDate(cDeliveryDate);
    divo.setDeliveryDateRangeEnd(cDeliveryDateRangeEnd);
    divo.setCardMessage(rs.getString(COMConstants.UDI_CARD_MESSAGE));
    divo.setCardSignature(rs.getString(COMConstants.UDI_CARD_SIGNATURE));
    divo.setSpecialInstructions(rs.getString(COMConstants.UDI_SPECIAL_INSTRUCTIONS));
    divo.setShipMethod(rs.getString(COMConstants.UDI_SHIP_METHOD));
    divo.setShipDate(cShipDate);
    divo.setMembershipNumber(rs.getString(COMConstants.UDI_MEMBERSHIP_NUMBER));
    divo.setMembershipFirstName(rs.getString(COMConstants.UDI_MEMBERSHIP_FIRST_NAME));
    divo.setMembershipLastName(rs.getString(COMConstants.UDI_MEMBERSHIP_LAST_NAME));
    divo.setRecipientId(rs.getString(COMConstants.UDI_RECIPIENT_ID));
    divo.setRecipientPhoneNumber(rs.getString(COMConstants.UDI_RECIPIENT_PHONE_NUMBER));
    divo.setRecipientExtension(rs.getString(COMConstants.UDI_RECIPIENT_EXTENSION));
    divo.setRecipientFirstName(rs.getString(COMConstants.UDI_RECIPIENT_FIRST_NAME));
    divo.setRecipientLastName(rs.getString(COMConstants.UDI_RECIPIENT_LAST_NAME));
    divo.setRecipientAddress1(rs.getString(COMConstants.UDI_RECIPIENT_ADDRESS_1));
    divo.setRecipientAddress2(rs.getString(COMConstants.UDI_RECIPIENT_ADDRESS_2));
    divo.setRecipientCity(rs.getString(COMConstants.UDI_RECIPIENT_CITY));
    divo.setRecipientState(rs.getString(COMConstants.UDI_RECIPIENT_STATE));
    divo.setRecipientZipCode(rs.getString(COMConstants.UDI_RECIPIENT_ZIP_CODE));
    divo.setRecipientCountry(rs.getString(COMConstants.UDI_RECIPIENT_COUNTRY));
    divo.setRecipientAddressType(rs.getString(COMConstants.UDI_RECIPIENT_ADDRESS_TYPE));
    divo.setRecipientBusinessName(rs.getString(COMConstants.UDI_RECIPIENT_BUSINESS_NAME));
    divo.setRecipientBusinessInfo(rs.getString(COMConstants.UDI_RECIPIENT_BUSINESS_INFO));
    divo.setAltContactFirstName(rs.getString(COMConstants.UDI_ALT_CONTACT_FIRST_NAME));
    divo.setAltContactLastName(rs.getString(COMConstants.UDI_ALT_CONTACT_LAST_NAME));
    divo.setAltContactPhoneNumber(rs.getString(COMConstants.UDI_ALT_CONTACT_PHONE_NUMBER));
    divo.setAltContactExtension(rs.getString(COMConstants.UDI_ALT_CONTACT_EXTENSION));
    divo.setAltContactEmail(rs.getString(COMConstants.UDI_ALT_CONTACT_EMAIL));
    divo.setAltContactInfoId(rs.getString(COMConstants.UDI_ALT_CONTACT_INFO_ID));
    divo.setVendorFlag(rs.getString(COMConstants.UDI_VENDOR_FLAG));
    // Needed for validation
    divo.setFloristId(rs.getString(COMConstants.UDI_FLORIST_ID));
    divo.setOriginId(rs.getString(COMConstants.UDI_ORIGIN_ID));
    divo.setOccasion(rs.getString(COMConstants.UDI_OCCASION));
    divo.setCompanyId(rs.getString(COMConstants.COMPANY_ID));

    //set the actual amounts
    //retrieve the actual addon amount
    String      sActualAddOnAmount = null;
    BigDecimal  bActualAddOnAmount;
    double      dActualAddOnAmount = 0;
    if (rs.getObject("actual_add_on_amount") != null)
    {
      bActualAddOnAmount = (BigDecimal)rs.getObject("actual_add_on_amount");
      sActualAddOnAmount = bActualAddOnAmount.toString();
      dActualAddOnAmount = Double.valueOf(sActualAddOnAmount).doubleValue();
    }
    divo.setActualAddOnAmount(dActualAddOnAmount);


    //retrieve the actual Discount amount
    String      sActualDiscountAmount = null;
    BigDecimal  bActualDiscountAmount;
    double      dActualDiscountAmount = 0;
    if (rs.getObject("actual_discount_amount") != null)
    {
      bActualDiscountAmount = (BigDecimal)rs.getObject("actual_discount_amount");
      sActualDiscountAmount = bActualDiscountAmount.toString();
      dActualDiscountAmount = Double.valueOf(sActualDiscountAmount).doubleValue();
    }
    divo.setActualDiscountAmount(dActualDiscountAmount);


    //retrieve the actual product amount
    String      sActualProductAmount = null;
    BigDecimal  bActualProductAmount;
    double      dActualProductAmount = 0;
    if (rs.getObject("actual_product_amount") != null)
    {
      bActualProductAmount = (BigDecimal)rs.getObject("actual_product_amount");
      sActualProductAmount = bActualProductAmount.toString();
      dActualProductAmount = Double.valueOf(sActualProductAmount).doubleValue();
    }
    divo.setActualProductAmount(dActualProductAmount);


    //retrieve the actual Service Fee amount
    String      sActualServiceFeeAmount = null;
    BigDecimal  bActualServiceFeeAmount;
    double      dActualServiceFeeAmount = 0;
    if (rs.getObject("actual_service_fee") != null)
    {
      bActualServiceFeeAmount = (BigDecimal)rs.getObject("actual_service_fee");
      sActualServiceFeeAmount = bActualServiceFeeAmount.toString();
      dActualServiceFeeAmount = Double.valueOf(sActualServiceFeeAmount).doubleValue();
    }
    divo.setActualServiceFee(dActualServiceFeeAmount);


    //retrieve the actual shipping fee
    String      sActualShippingFeeAmount = null;
    BigDecimal  bActualShippingFeeAmount;
    double      dActualShippingFeeAmount = 0;
    if (rs.getObject("actual_shipping_fee") != null)
    {
      bActualShippingFeeAmount = (BigDecimal)rs.getObject("actual_shipping_fee");
      sActualShippingFeeAmount = bActualShippingFeeAmount.toString();
      dActualShippingFeeAmount = Double.valueOf(sActualShippingFeeAmount).doubleValue();
    }
    divo.setActualShippingFee(dActualShippingFeeAmount);


    //retrieve the actual Shipping Tax amount
    String      sActualShippingTaxAmount = null;
    BigDecimal  bActualShippingTaxAmount;
    double      dActualShippingTaxAmount = 0;
    if (rs.getObject("actual_shipping_tax") != null)
    {
      bActualShippingTaxAmount = (BigDecimal)rs.getObject("actual_shipping_tax");
      sActualShippingTaxAmount = bActualShippingTaxAmount.toString();
      dActualShippingTaxAmount = Double.valueOf(sActualShippingTaxAmount).doubleValue();
    }
    divo.setActualShippingTax(dActualShippingTaxAmount);


    //retrieve the actual tax amount
    String      sActualTaxAmount = null;
    BigDecimal  bActualTaxAmount;
    double      dActualTaxAmount = 0;
    if (rs.getObject("actual_tax") != null)
    {
      bActualTaxAmount = (BigDecimal)rs.getObject("actual_tax");
      sActualTaxAmount = bActualTaxAmount.toString();
      dActualTaxAmount = Double.valueOf(sActualTaxAmount).doubleValue();
    }
    divo.setActualTax(dActualTaxAmount);

    return divo;

  }


  /**
   * Ali Lakhani
   *
   * @return Document containing the values from the cursor
   *
   */
  public Document getSourceCodeInfoByIdXML(String sourceCode) throws Exception
  {
        Document searchResults = DOMUtil.getDocument();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("MO_SOURCE_CODE_RECORD_LOOKUP");
        dataRequest.addInputParam("IN_SOURCE_CODE", sourceCode);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

        searchResults = (Document) dataAccessUtil.execute(dataRequest);

        return searchResults;

  }


  /**
   * Ali Lakhani
   *
   * @return Document containing the values from the cursor
   *
   */
  public String getDiscountRewardTypeById(String sourceCode) throws Exception
  {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("MO_GET_DISCOUNT_REWARD_TYPE");
        dataRequest.addInputParam("IN_SOURCE_CODE", sourceCode);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
 
        String searchResults = (String) dataAccessUtil.execute(dataRequest);

        return searchResults;

  }


  /**
   * Ali Lakhani
   *
   * @return Document containing the values from the cursor
   *
   */
  public Document getFeeByIdXML(String snhId, String deliveryDate) throws Exception
  {
        Document searchResults = DOMUtil.getDocument();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("MO_SNH_BY_ID");
        dataRequest.addInputParam("IN_SNH_ID", snhId);
        dataRequest.addInputParam("IN_DELIVERY_DATE", deliveryDate);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

        logger.debug("Calling MO_SNH_BY_ID: " + snhId + " " + deliveryDate) ;
        searchResults = (Document) dataAccessUtil.execute(dataRequest);

        return searchResults;

  }


  /**
   * Ali Lakhani
   *
   * @return Document containing the values from the cursor
   *
   */
  public Document getScriptXML(String companyId, String pageId) throws Exception
  {
        Document searchResults = DOMUtil.getDocument();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("MO_SCRIPT_BY_PAGE_SFMB");
        dataRequest.addInputParam("IN_COMPANY_ID", companyId);
        dataRequest.addInputParam("IN_PAGE_ID", pageId);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

 
        searchResults = (Document) dataAccessUtil.execute(dataRequest);

        return searchResults;

  }


  /**
   * Ali Lakhani
   *
   * @return Document containing the values from the cursor
   *
   */
  public CachedResultSet getSuperIndexRS(String liveFlag, String occasionFlag,
          String productsFlag, String dropShipFlag, String countryId, String sourceCode)
          throws Exception
  {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("MO_SUPER_INDEX_LIST_SFMB");
        dataRequest.addInputParam("IN_LIVE_FLAG",       liveFlag);
        dataRequest.addInputParam("IN_OCCASIONS_FLAG",  occasionFlag);
        dataRequest.addInputParam("IN_PRODUCTS_FLAG",   productsFlag);
        dataRequest.addInputParam("IN_DROPSHIP_FLAG",   dropShipFlag);
        dataRequest.addInputParam("IN_COUNTRY_ID",      countryId);
        dataRequest.addInputParam("IN_SOURCE_CODE_ID",  sourceCode);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

 
        CachedResultSet searchResults = (CachedResultSet) dataAccessUtil.execute(dataRequest);

        return searchResults;

  }


  /**
   * Ali Lakhani
   *
   * @return Document containing the values from the cursor
   *
   */
  public Document getSubIndexXML(String liveFlag, String dropShipFlag, String countryId, String sourceCode)
          throws Exception
  {
        Document searchResults = DOMUtil.getDocument();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("MO_SUB_INDEX_LIST_SFMB");
        dataRequest.addInputParam("IN_LIVE_FLAG",       liveFlag);
        dataRequest.addInputParam("IN_DROPSHIP_FLAG",   dropShipFlag);
        dataRequest.addInputParam("IN_COUNTRY_ID",      countryId);
        dataRequest.addInputParam("IN_SOURCE_CODE_ID",  sourceCode);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

 
        searchResults = (Document) dataAccessUtil.execute(dataRequest);

        return searchResults;

  }

  /**
   * Ali Lakhani
   *
   * @return Document containing the values from the cursor
   *
   */
  public Document getAllPricePointXML() throws Exception
  {
        Document searchResults = DOMUtil.getDocument();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("MO_PRICE_POINTS_LIST");

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

 
        searchResults = (Document) dataAccessUtil.execute(dataRequest);

        return searchResults;

  }


  /**
   * Ali Lakhani
   *
   * @return Document containing the values from the cursor
   *
   */
  public Document getAllProductCategoryXML() throws Exception
  {
        Document searchResults = DOMUtil.getDocument();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("MO_PRODUCT_CATEGORIES");

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

 
        searchResults = (Document) dataAccessUtil.execute(dataRequest);

        return searchResults;

  }


  /**
   * Ali Lakhani
   *
   * @return Document containing the values from the cursor
   *
   */
  public Document getAllRecipientXML() throws Exception
  {
        Document searchResults = DOMUtil.getDocument();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("MO_RECIPIENT_SEARCH_LIST");

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

 
        searchResults = (Document) dataAccessUtil.execute(dataRequest);

        return searchResults;

  }


  /**
   * Ali Lakhani
   *
   * @return Document containing the values from the cursor
   *
   */
  public Document getAllFlowerXML() throws Exception
  {
        Document searchResults = DOMUtil.getDocument();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("MO_FLOWER_LIST");

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

 
        searchResults = (Document) dataAccessUtil.execute(dataRequest);

        return searchResults;

  }

  /**
   * Ali Lakhani
   *
   * @return Document containing the values from the cursor
   *
   */
  public Document getAllColorXML() throws Exception
  {
        Document searchResults = DOMUtil.getDocument();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("MO_COLORS_LIST");

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

 
        searchResults = (Document) dataAccessUtil.execute(dataRequest);

        return searchResults;

  }

  /**
   * Ali Lakhani
   *
   * @return Document containing the values from the cursor
   *
   */
  public Document getAllProductOccasionXML() throws Exception
  {
        Document searchResults = DOMUtil.getDocument();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("MO_PRODUCT_OCCASIONS");

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

 
        searchResults = (Document) dataAccessUtil.execute(dataRequest);

        return searchResults;

  }


  /**
   * Ali Lakhani
   *
   * @return Document containing the values from the cursor
   *
   */
  public Document getAllOccasionXML() throws Exception
  {
        Document searchResults = DOMUtil.getDocument();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("MO_OCCASION_LIST_LOOKUP");

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

 
        searchResults = (Document) dataAccessUtil.execute(dataRequest);

        return searchResults;

  }


  /**
   * Ali Lakhani
   *
   * @return Document containing the values from the cursor
   *
   */
  public CachedResultSet getGlobalParmByIdRS(String context) throws Exception
  {
      DataRequest dataRequest = new DataRequest();
      dataRequest.setConnection(this.connection);
      dataRequest.addInputParam("IN_CONTEXT",  context);
      dataRequest.setStatementID("MO_GLOBAL_PARMS_LOOKUP_XML");

      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

 
      CachedResultSet searchResults = (CachedResultSet) dataAccessUtil.execute(dataRequest);

      return searchResults;

  }


  /**
   * Ali Lakhani
   *
   * @return Document containing the values from the cursor
   *
   */
  public Map getPromotionProgramRS(String sourceCode, double price) throws Exception
  {
      DataRequest dataRequest = new DataRequest();
      dataRequest.setConnection(this.connection);
      dataRequest.setStatementID("GET_MILES_POINTS");
      dataRequest.addInputParam("IN_SOURCE_CODE", sourceCode);
      dataRequest.addInputParam("IN_PRODUCT_AMOUNT", price);
      dataRequest.addInputParam("IN_ORDER_AMOUNT", price);

      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      Map outputs = (Map) dataAccessUtil.execute(dataRequest);

      return outputs;

  }



  /**
   * Ali Lakhani
   *
   * @return Document containing the values from the cursor
   *
   */
  public Document getOccasionByIdXML(String occasionId) throws Exception
  {
        Document searchResults = DOMUtil.getDocument();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("MO_OCCASION");
        dataRequest.addInputParam("IN_OCCASION_ID",     occasionId);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

 
        searchResults = (Document) dataAccessUtil.execute(dataRequest);

        return searchResults;

  }



  /**
   * Ali Lakhani
   *
   * @return Document containing the values from the cursor
   *
   */
  public Document getShippingMethodXML(String productId, String shippingKey, String price) throws Exception
  {


        Document searchResults = DOMUtil.getDocument();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("MO_SHIPPING_METHODS");
        dataRequest.addInputParam("IN_PRODUCT_ID",        productId);
        dataRequest.addInputParam("IN_SHIPPING_KEY_ID",   new Long(shippingKey));
        dataRequest.addInputParam("IN_STANDARD_PRICE",    new Double(price));

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

 
        searchResults = (Document) dataAccessUtil.execute(dataRequest);

        return searchResults;

  }


  /**
   * Ali Lakhani
   *
   * @return Document containing the values from the cursor
   *
   */
  public Document getCountryMasterByIdXML(String countryId) throws Exception
  {

        Document searchResults = DOMUtil.getDocument();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("MO_COUNTRY_MASTER_BY_ID");
        dataRequest.addInputParam("IN_COUNTRY_ID",        countryId);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

 
        searchResults = (Document) dataAccessUtil.execute(dataRequest);

        return searchResults;

  }


  /**
   * Ali Lakhani
   *
   * @return Document containing the values from the cursor
   *
   */
  public Document getProductXML(String indexId, String pricePointId,
          String sourceCode, String domesticIntlFlag, String zipCode, String deliveryEndDate,
          String countryId, String deliveryDate)
          throws Exception
  {

        Document searchResults = DOMUtil.getDocument();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);


        dataRequest.addInputParam("IN_INDEX_ID",                    indexId);

        if(pricePointId == null || pricePointId.length() <= 0 || !StringUtils.isNumeric(pricePointId))
        {
          dataRequest.setStatementID("MO_PRODUCTS_BY_ID_SDG_NULLPRICEPPOINT");
          dataRequest.addInputParam("IN_PRICE_POINT_ID",            new Integer(java.sql.Types.INTEGER));
        }
        else
        {
          dataRequest.setStatementID("MO_PRODUCTS_BY_ID_SDG");
          dataRequest.addInputParam("IN_PRICE_POINT_ID",            new Long(pricePointId));
        }

        dataRequest.addInputParam("IN_SOURCE_CODE",                 sourceCode);
        dataRequest.addInputParam("IN_DOMESTIC_INTL_FLAG",          domesticIntlFlag);
        dataRequest.addInputParam("IN_ZIP_CODE",                    zipCode!=null?(zipCode.length()>5?zipCode.substring(0,5):zipCode):null);
        dataRequest.addInputParam("IN_DELIVERY_END_DATE",		        deliveryEndDate);
        dataRequest.addInputParam("IN_COUNTRY_ID",					        countryId);
        dataRequest.addInputParam("IN_SCRIPT_CODE",					        COMConstants.CONS_MO_SCRIPT_CODE);
        dataRequest.addInputParam("IN_DELIVERY_DATE", deliveryDate);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

 
        searchResults = (Document) dataAccessUtil.execute(dataRequest);

        return searchResults;

  }


  /**
   * Ali Lakhani
   *
   * @return Document containing the values from the cursor
   *
   */
  public Document getProductListXML(String indexId, String pricePointId,
          String sourceCode, String domesticIntlFlag, String zipCode, String deliveryEndDate,
          String countryId)
          throws Exception
  {

        Document searchResults = DOMUtil.getDocument();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);

        dataRequest.addInputParam("IN_INDEX_ID",              indexId);

        if(pricePointId == null || pricePointId.length() <= 0 || !StringUtils.isNumeric(pricePointId))
        {
          dataRequest.setStatementID("MO_PRODUCT_LIST_BY_ID_SDG_NULLPRICEPPOINT");
          dataRequest.addInputParam("IN_PRICE_POINT_ID",      new Integer(java.sql.Types.INTEGER));
        }
        else
        {
          dataRequest.setStatementID("MO_PRODUCT_LIST_BY_ID_SDG");
          dataRequest.addInputParam("IN_PRICE_POINT_ID",      new Long(pricePointId));
        }

        dataRequest.addInputParam("IN_SOURCE_CODE",           sourceCode);
        dataRequest.addInputParam("IN_DOMESTIC_INTL_FLAG",    domesticIntlFlag);
        dataRequest.addInputParam("IN_ZIP_CODE",              zipCode!=null?(zipCode.length()>5?zipCode.substring(0,5):zipCode):null);
        dataRequest.addInputParam("IN_DELIVERY_END_DATE",		  deliveryEndDate);
        dataRequest.addInputParam("IN_COUNTRY_ID",					  countryId);
        dataRequest.addInputParam("IN_SCRIPT_CODE",					  COMConstants.CONS_MO_SCRIPT_CODE);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

 
        searchResults = (Document) dataAccessUtil.execute(dataRequest);

        return searchResults;

  }


  /**
   * Ali Lakhani
   *
   * @return Document containing the values from the cursor
   *
   */
  public Document getProductListForKeywordSearchXML(String keywordSearchList, String pricePointId,
          String sourceCode, String domesticIntlFlag, String zipCode, String deliveryEndDate,
          String countryId)
          throws Exception
  {

        Document searchResults = DOMUtil.getDocument();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);

        dataRequest.addInputParam("IN_KEYWORD_SEARCH_LIST",         keywordSearchList);

        if(pricePointId == null || pricePointId.length() <= 0 || !StringUtils.isNumeric(pricePointId))
        {
          dataRequest.addInputParam("IN_PRICE_POINT_ID",            new Integer(java.sql.Types.INTEGER));
          dataRequest.setStatementID("MO_PRODUCT_LIST_FOR_KEYWORD_SEARCH_BY_ID_SDG_NULLPRICEPPOINT");
        }
        else
        {
          dataRequest.addInputParam("IN_PRICE_POINT_ID",            new Long(pricePointId));
          dataRequest.setStatementID("MO_PRODUCT_LIST_FOR_KEYWORD_SEARCH_BY_ID_SDG");
        }

        dataRequest.addInputParam("IN_SOURCE_CODE",					        sourceCode);
        dataRequest.addInputParam("IN_DOMESTIC_INTL_FLAG",	        domesticIntlFlag);
        dataRequest.addInputParam("IN_ZIP_CODE",                    zipCode!=null?(zipCode.length()>5?zipCode.substring(0,5):zipCode):null);
        dataRequest.addInputParam("IN_DELIVERY_END_DATE",		        deliveryEndDate);
        dataRequest.addInputParam("IN_COUNTRY_ID",					        countryId);
        dataRequest.addInputParam("IN_SCRIPT_CODE",					        COMConstants.CONS_MO_SCRIPT_CODE);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

 
        searchResults = (Document) dataAccessUtil.execute(dataRequest);

        return searchResults;

  }


  /**
   * Ali Lakhani
   *
   * @return Document containing the values from the cursor
   *
   */
  public Document getHolidayByIdXML(String countryId) throws Exception
  {

        Document searchResults = DOMUtil.getDocument();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("MO_HOLIDAYS_BY_COUNTRY");
        dataRequest.addInputParam("IN_COUNTRY_ID",        countryId);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

 
        searchResults = (Document) dataAccessUtil.execute(dataRequest);

        return searchResults;

  }



  /**
   * Ali Lakhani
   *
   * @return Document containing the values from the cursor
   *
   */
  public Document getAllShippingMethodXML() throws Exception
  {

        Document searchResults = DOMUtil.getDocument();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("MO_ALL_SHIPPING_METHODS");

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

 
        searchResults = (Document) dataAccessUtil.execute(dataRequest);

        return searchResults;

  }


  /**
   * Ali Lakhani
   *
   * @return Document containing the values from the cursor
   *
   */
  public Document getDeliveryDateRangeXML() throws Exception
  {

        Document searchResults = DOMUtil.getDocument();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("MO_DELIVERY_DATE_RANGES");

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

 
        searchResults = (Document) dataAccessUtil.execute(dataRequest);

        return searchResults;

  }


  /**
   * Ali Lakhani
   *
   * @return HashMap containing the values from the cursor
   *
   */
  public HashMap getOriginByIdMap(String sourceCode) throws Exception
  {
    HashMap searchResults = new HashMap();

    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(connection);
    dataRequest.setStatementID("MO_ORIGIN_FROM_SOURCE");
    dataRequest.addInputParam("IN_SOURCE_CODE",          sourceCode);

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

 
    searchResults = (HashMap) dataAccessUtil.execute(dataRequest);

    return searchResults;

  }


  /**
   * Ali Lakhani
   *
   * @return Document containing the values from the cursor
   *
   */
  public Document getUpsellDetailByIdXML(String upsellMasterId) throws Exception
  {

        Document searchResults = DOMUtil.getDocument();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("MO_UPSELL_DETAILS");
        dataRequest.addInputParam("IN_UPSELL_MASTER_ID",    upsellMasterId);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

 
        searchResults = (Document) dataAccessUtil.execute(dataRequest);

        return searchResults;

  }


  /**
   * Ali Lakhani
   *
   * @return Document containing the values from the cursor
   *
   */
  public Document getProductSubcodeActiveByIdXML(String productId) throws Exception
  {

        Document searchResults = DOMUtil.getDocument();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("MO_PRODUCT_SUBCODES_ACTIVE");
        dataRequest.addInputParam("IN_PRODUCT_ID",    productId);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

 
        searchResults = (Document) dataAccessUtil.execute(dataRequest);

        return searchResults;

  }


  /**
   * Ali Lakhani
   *
   * @return Document containing the values from the cursor
   *
   */
  public Document getProductDetailXML(String productId, String sourceCode,
          String domesticIntlFlag, String zipCode, String countryId, String deliveryDate)
          throws Exception
  {

        Document searchResults = DOMUtil.getDocument();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("MO_PRODUCT_DETAILS_SDG");
        dataRequest.addInputParam("IN_PRODUCT_ID",					productId);
        dataRequest.addInputParam("IN_SOURCE_CODE",					sourceCode);
        dataRequest.addInputParam("IN_DOMESTIC_INTL_FLAG",	domesticIntlFlag);
        dataRequest.addInputParam("IN_ZIP_CODE",            zipCode!=null?(zipCode.length()>5?zipCode.substring(0,5):zipCode):null);
        dataRequest.addInputParam("IN_COUNTRY_ID",					countryId);
        dataRequest.addInputParam("IN_DELIVERY_DATE", deliveryDate);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

 
        searchResults = (Document) dataAccessUtil.execute(dataRequest);

        return searchResults;

  }


  /**
   * Ali Lakhani
   *
   * @return Document containing the values from the cursor
   *
   */
  public Document getZipCodeByIdXML(String domesticIntlFlag, String zipCode)
          throws Exception
  {

        Document searchResults = DOMUtil.getDocument();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("MO_ZIPCODE_INFO");
        dataRequest.addInputParam("IN_ZIP_CODE_ID",  zipCode!=null?(zipCode.length()>5?zipCode.substring(0,5):zipCode):null);
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

 
        searchResults = (Document) dataAccessUtil.execute(dataRequest);

        return searchResults;

  }




  /**
   * Ali Lakhani
   *
   * @return Document containing the values from the cursor
   *
   */
  public Document getProductIndexByIdXML(String indexId)
          throws Exception
  {

        Document searchResults = DOMUtil.getDocument();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("MO_PRODUCT_INDEX_BY_ID");
        dataRequest.addInputParam("IN_INDEX_ID",		indexId);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

 
        searchResults = (Document) dataAccessUtil.execute(dataRequest);

        return searchResults;

  }




  /**
   * hasLiveMercury
   *
   * @param a_orderDetailId
   * @throws java.lang.Exception
   */
  public boolean hasLiveMercury(String a_orderDetailId) throws Exception
  {
    boolean hasLive = false;
    String nullMessageType = null;

    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(this.connection);
    dataRequest.setStatementID("GET_MERCURY_ORDER_MESSAGE_STATUS");
    dataRequest.addInputParam("IN_ORDER_DETAIL_ID", a_orderDetailId);
    dataRequest.addInputParam("IN_MESSAGE_TYPE", nullMessageType);
    dataRequest.addInputParam("IN_COMP_ORDER", "N");

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

 
    Map outputs = (Map) dataAccessUtil.execute(dataRequest);

    String liveFtd = (String) outputs.get("OUT_HAS_LIVE_FTD");
    hasLive = StringUtils.equals(liveFtd, "Y");

    logger.debug("hasLiveMercury call successful");
    return hasLive;
  }



  /**
   * hasLiveMercury
   *
   * This method is simliar to the hasLiveMercury method above, except
   * that is calls a stored procedure that contains an additionial parameter.
   *
   * String orderDetailId: order detail id of record
   * String messageType: Venus or Mecury.  Or Null an proc will determine based on ship type
   * boolean includeComp: true = include comp orders with in results
   *
   * @param a_orderDetailId
   * @throws java.lang.Exception
   */
  public boolean hasLiveMercury(String orderDetailId, String messageType, boolean includeComp) throws Exception
  {

    String compFlag = includeComp ? "Y" : "N";

    boolean hasLive = false;
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(this.connection);
    dataRequest.setStatementID("GET_MERCURY_ORDER_MESSAGE_STATUS");
    dataRequest.addInputParam("IN_ORDER_DETAIL_ID", orderDetailId);
    dataRequest.addInputParam("IN_MESSAGE_TYPE", messageType);
    dataRequest.addInputParam("IN_COMP_ORDER", compFlag);

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

 
    Map outputs = (Map) dataAccessUtil.execute(dataRequest);

    String liveFtd = (String) outputs.get("OUT_HAS_LIVE_FTD");
    hasLive = StringUtils.equals(liveFtd, "Y");


    return hasLive;
  }



  /**
   * Ali Lakhani
   * hasLiveMercury - since the stored proc is passing more info than just 1 flag, this method
   *                  will return the Cached Result Set to the calling class.
   *
   * @param a_orderDetailId
   * @throws java.lang.Exception
   */
  public HashMap hasLiveMercuryMap(String orderDetailId) throws Exception
  {
    HashMap searchResults = new HashMap();
    String nullMessageType = null;

    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(this.connection);
    dataRequest.setStatementID("GET_MERCURY_ORDER_MESSAGE_STATUS");
    dataRequest.addInputParam("IN_ORDER_DETAIL_ID", orderDetailId);
    dataRequest.addInputParam("IN_MESSAGE_TYPE", nullMessageType);
    dataRequest.addInputParam("IN_COMP_ORDER", "N");

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

 
    searchResults = (HashMap) dataAccessUtil.execute(dataRequest);

    return searchResults;

  }


  /**
   * getPaymentMethods
   *
   * Gets cursor of payment methods for an order.  Note that its a cursor since
   * you may have, for example, an order paid by both gift certificate and charge card.
   *
   * @param a_orderDetailId
   * @throws java.lang.Exception
   */
  public CachedResultSet getPaymentMethods(String a_orderDetailId) throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(this.connection);
    dataRequest.setStatementID("GET_ORDER_PAYMENT_METHODS");
    dataRequest.addInputParam("IN_ORDER_DETAIL_ID", a_orderDetailId);

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

 
    CachedResultSet searchResults = (CachedResultSet) dataAccessUtil.execute(dataRequest);

    return searchResults;
  }


  /**
   * getPartnerProgram
   *
   * Gets partner_program for a source code.
   *
   * @param a_sourceCode
   * @throws java.lang.Exception
   */
  public CachedResultSet getPartnerProgram(String a_sourceCode) throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(this.connection);
    dataRequest.setStatementID("GET_PARTNER_PROGRAM_BY_SOURCE_CODE");
    dataRequest.addInputParam("IN_SOURCE_CODE", a_sourceCode);

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

 
    CachedResultSet searchResults = (CachedResultSet) dataAccessUtil.execute(dataRequest);

    return searchResults;
  }


  /**
   * getCoBrand
   *
   * Retrieve co brand information for an order
   *
   * @param a_orderGuid
   * @return  Collection of co brand information
   * @exception Exception
   */
  public Collection getCoBrand(String a_orderGuid) throws Exception
  {
    Collection coBrands = new ArrayList();
    CoBrandVO coBrand = null;
    CachedResultSet rs = null;

    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(this.connection);
    dataRequest.setStatementID("VIEW_CO_BRAND");
    dataRequest.addInputParam("IN_ORDER_GUID", a_orderGuid);
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

 
    rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

    while(rs != null && rs.next()) {
        coBrand = new CoBrandVO();

        coBrand.setCoBrandId(Long.parseLong(rs.getObject(1).toString()));
        coBrand.setGuid((String)rs.getObject(2));
        coBrand.setInfoName((String)rs.getObject(3));
        coBrand.setInfoData((String)rs.getObject(4));
        coBrands.add(coBrand);
    }
    return coBrands;
  }


  /**
   * getSourceCodeInfo
   *
   * @param a_sourceCode
   * @throws java.lang.Exception
   */
   public CachedResultSet getSourceCodeInfo(String a_sourceCode) throws Exception
   {
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(this.connection);
    dataRequest.setStatementID("GET_SOURCE_CODE_INFO_CURSOR");
    dataRequest.addInputParam("IN_SOURCE_CODE", a_sourceCode);

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
 
    CachedResultSet searchResults = (CachedResultSet) dataAccessUtil.execute(dataRequest);

    return searchResults;
   }


  /**
   * getProductInfo
   *
   * @param a_prodId
   * @throws java.lang.Exception
   */
  //public CachedResultSet getProductInfo(String a_prodId) throws Exception
  public CachedResultSet getProductInfo(String a_prodId) throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(this.connection);
    dataRequest.setStatementID("GET_PRODUCT_BY_ANY_ID");
    dataRequest.addInputParam("PRODUCTID", a_prodId);

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
 
    CachedResultSet searchResults = (CachedResultSet) dataAccessUtil.execute(dataRequest);

    return searchResults;
  }


  /**
   * getZipCodeList
   *
   * @param a_zip
   * @param a_city
   * @param a_state
   * @throws java.lang.Exception
   */
  public CachedResultSet getZipCodeList(String a_zip, String a_city, String a_state) throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(this.connection);
    dataRequest.setStatementID("GET_ZIP_CODE_LIST_CURSOR");
    dataRequest.addInputParam("IN_ZIP_CODE_ID", a_zip);
    dataRequest.addInputParam("IN_CITY", a_city);
    dataRequest.addInputParam("IN_STATE_MASTER_ID", a_state);

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
 
    CachedResultSet searchResults = (CachedResultSet) dataAccessUtil.execute(dataRequest);

    return searchResults;
  }


  /**
   * findOrderNumber
   *
   * @param a_orderNum
   * @throws java.lang.Exception
   */
  public HashMap findOrderNumber(String a_orderNum) throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(this.connection);
    dataRequest.setStatementID("FIND_ORDER_NUMBER");
    dataRequest.addInputParam("IN_ORDER_NUMBER", a_orderNum);

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

 
    HashMap searchResults = (HashMap) dataAccessUtil.execute(dataRequest);

    return searchResults;
  }


  /**
   * floristAvailableForZip
   *
   * Checks if specified florist covers specified zip.  Returns true if it does,
   * false otherwise.
   *
   * @param a_floristId
   * @param a_zip
   * @throws java.lang.Exception
   */
  public boolean floristAvailableForZip(String a_floristId, String a_zip) throws Exception
  {
    boolean zipIsCoveredByFlorist = false;
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(this.connection);
    dataRequest.setStatementID("FLORIST_AVAILABLE_FOR_ZIP");
    dataRequest.addInputParam("IN_FLORIST_ID", a_floristId);
    dataRequest.addInputParam("IN_ZIP_CODE", a_zip);

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

    //Map outputs = (Map) dataAccessUtil.execute(dataRequest);
    //BigDecimal count = (BigDecimal) outputs.get("OUT_NUMBER");

 
    BigDecimal count = (BigDecimal) dataAccessUtil.execute(dataRequest);

    if (count.longValue() > 0) {
      zipIsCoveredByFlorist = true;
    } else {
      logger.debug("floristAvailableForZip - Florist " + a_floristId + " not available for zip: " + a_zip);
    }
    return zipIsCoveredByFlorist;
  }


  /**
   * floristInfo
   *
   * Returns information for florist id sent in.
   *
   * @param floristId
   * @throws java.lang.Exception
   */
  public CachedResultSet getFloristInfo(String floristId) throws Exception
  {
    boolean sundayCodified = false;
    CachedResultSet rs = null;
    String sundayStr = null;

    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(this.connection);
    dataRequest.setStatementID("FLORIST_INFO");
    dataRequest.addInputParam("IN_FLORIST_ID", floristId);

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    
    rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
   
    return rs;
  }


  /**
   * Ali Lakhani
   *
   * @return HashMap containing the status and message
   *
   * @throws java.lang.Exception
   */
  public HashMap deleteExistingAddOns(long orderAddOnId, String orderDetailId)
        throws Exception
  {
    HashMap searchResults = new HashMap();
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(this.connection);
    dataRequest.setStatementID("MO_DELETE_ORDER_ADD_ONS_UPDATE");
    dataRequest.addInputParam("IN_ORDER_ADD_ON_ID",	new Long(orderAddOnId));
    dataRequest.addInputParam("IN_ORDER_DETAIL_ID",	orderDetailId);

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

 
    searchResults = (HashMap) dataAccessUtil.execute(dataRequest);

    return searchResults;

  }


  /**
   * @return HashMap containing the status and message
   *
   * @throws java.lang.Exception
   */
  public HashMap deleteExistingAddOns(String orderDetailId)
        throws Exception
  {
    HashMap searchResults = new HashMap();
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(this.connection);
    dataRequest.setStatementID("MO_DELETE_ORDER_ADD_ONS_UPDATE_OD");
    dataRequest.addInputParam("IN_ORDER_DETAIL_ID",	orderDetailId);

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

 
    searchResults = (HashMap) dataAccessUtil.execute(dataRequest);

    return searchResults;

  }


  /**
   * Ali Lakhani
   *
   * @return HashMap containing the status and message
   *
   * @throws java.lang.Exception
   */
  public HashMap insertNewAddOns(String orderDetailId, String csrId, String orderAddOnCode, long orderAddOnQty)
        throws Exception
  {
    HashMap searchResults = new HashMap();
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(this.connection);
    dataRequest.setStatementID("MO_INSERT_ORDER_ADD_ONS_UPDATE");
    dataRequest.addInputParam("IN_ORDER_DETAIL_ID",	orderDetailId);
    dataRequest.addInputParam("IN_UPDATED_BY",	    csrId);
    dataRequest.addInputParam("IN_ADD_ON_CODE",	    orderAddOnCode);
    dataRequest.addInputParam("IN_ADD_ON_QUANTITY",	new Long(orderAddOnQty));

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

 
    searchResults = (HashMap) dataAccessUtil.execute(dataRequest);

    return searchResults;

  }



  /**
   * Retrieves Delivery Confirmation in XML format.
   * @param orderDetailId
   * @param csrId
   * @param sessionId
   * @return Document
   * @throws java.lang.Exception
   */
  public Document getDeliveryConfirmationInfo(String orderDetailId, String csrId, String sessionId)
      throws Exception {
    Document searchResults = DOMUtil.getDocument();
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection( this.connection );
    dataRequest.setStatementID( "GET_DELIVERY_CONFIRMATION_INFO" );
    dataRequest.addInputParam( "ORDER_DETAIL_ID", orderDetailId );
    dataRequest.addInputParam( "CSR_ID", csrId );
    dataRequest.addInputParam( "SESSION_ID", sessionId );
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

 
    searchResults = (Document) dataAccessUtil.execute( dataRequest );

    return searchResults;
  }


  /**
   * Ali Lakhani
   *
   * @return HashMap containing the status and message
   *
   * @throws java.lang.Exception
   */
  public HashMap updateAddOns(long addOnId, String csrId, String orderAddOnCode, long orderAddOnQty)
        throws Exception
  {
    HashMap searchResults = new HashMap();
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(this.connection);
    dataRequest.setStatementID("MO_UPDATE_ORDER_ADD_ONS_UPDATE");

    dataRequest.addInputParam("IN_ORDER_ADD_ON_ID",	new Long(addOnId).toString());
    dataRequest.addInputParam("IN_UPDATED_BY",	      csrId);
    dataRequest.addInputParam("IN_ADD_ON_CODE",	      orderAddOnCode);
    dataRequest.addInputParam("IN_ADD_ON_QUANTITY",	  new Long(orderAddOnQty).toString());

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

 
    searchResults = (HashMap) dataAccessUtil.execute(dataRequest);

    return searchResults;

  }


  /**
   * Ali Lakhani
   *
   * @return HashMap containing the status and message
   *
   * @throws java.lang.Exception
   */
  public HashMap  updateProductDetailInfo(String orderDetailId, String csrId, Calendar deliveryDate,
                        Calendar deliveryDateRange, String productId, String color1,  String color2,
                        String substitutionIndicator, String specialInstructions, String shipMethod,
                        String sizeIndicator, Calendar shipDate, String subcode, String personalGreetingId)
         throws Exception
  {
    java.sql.Date dDeliveryDate = null;
    java.sql.Date dDeliveryDateRangeEnd = null;
    java.sql.Date dShipDate = null;

    if (deliveryDate != null) {
      dDeliveryDate = new java.sql.Date(deliveryDate.getTimeInMillis());
    }
    if (deliveryDateRange != null) {
      dDeliveryDateRangeEnd = new java.sql.Date(deliveryDateRange.getTimeInMillis());
    }
    if (shipDate != null) {
      dShipDate = new java.sql.Date(shipDate.getTimeInMillis());
    }


    HashMap searchResults = new HashMap();
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(this.connection);
    dataRequest.setStatementID("MO_UPDATE_PRODUCT_DETAIL");

    dataRequest.addInputParam("IN_ORDER_DETAIL_ID", orderDetailId);
    dataRequest.addInputParam("IN_UPDATED_BY", csrId);
    dataRequest.addInputParam("IN_DELIVERY_DATE", dDeliveryDate);
    dataRequest.addInputParam("IN_DELIVERY_DATE_RANGE_END", dDeliveryDateRangeEnd);
    dataRequest.addInputParam("IN_PRODUCT_ID", productId);
    dataRequest.addInputParam("IN_COLOR_1", color1);
    dataRequest.addInputParam("IN_COLOR_2", color2);
    dataRequest.addInputParam("IN_SUBSTITUTION_INDICATOR", substitutionIndicator);
    dataRequest.addInputParam("IN_SPECIAL_INSTRUCTIONS", specialInstructions);
    dataRequest.addInputParam("IN_SHIP_METHOD", shipMethod);
    dataRequest.addInputParam("IN_SIZE_INDICATOR", sizeIndicator);
    dataRequest.addInputParam("IN_SHIP_DATE", dShipDate);
    dataRequest.addInputParam("IN_SUBCODE", subcode);
    dataRequest.addInputParam("IN_PERSONAL_GREETING_ID", personalGreetingId);

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

 
    searchResults = (HashMap) dataAccessUtil.execute(dataRequest);

    return searchResults;

  }


  /**
   * Ali Lakhani
   *
   * @return HashMap containing the status and message
   *
   * @throws java.lang.Exception
   */
  public HashMap  updateProductDetailInfo(String orderDetailId, String csrId, String deliveryDate,
                        String deliveryDateRange, String productId, String color1,  String color2,
                        String substitutionIndicator, String specialInstructions, String shipMethod,
                        String sizeIndicator, String shipDate)
         throws Exception
  {
    HashMap searchResults = new HashMap();
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(this.connection);
    dataRequest.setStatementID("MO_UPDATE_PRODUCT_DETAIL");

    dataRequest.addInputParam("IN_ORDER_DETAIL_ID",					orderDetailId);
    dataRequest.addInputParam("IN_UPDATED_BY",							csrId);
    dataRequest.addInputParam("IN_DELIVERY_DATE",						deliveryDate);
    dataRequest.addInputParam("IN_DELIVERY_DATE_RANGE_END",	deliveryDateRange);
    dataRequest.addInputParam("IN_PRODUCT_ID",							productId);
    dataRequest.addInputParam("IN_COLOR_1",									color1);
    dataRequest.addInputParam("IN_COLOR_2",									color2);
    dataRequest.addInputParam("IN_SUBSTITUTION_INDICATOR",	substitutionIndicator);
    dataRequest.addInputParam("IN_SPECIAL_INSTRUCTIONS",		specialInstructions);
    dataRequest.addInputParam("IN_SHIP_METHOD",							shipMethod);
    dataRequest.addInputParam("IN_SIZE_INDICATOR",					sizeIndicator);
    dataRequest.addInputParam("IN_SHIP_DATE",						    shipDate!=null?shipDate.toString():null);

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

 
    searchResults = (HashMap) dataAccessUtil.execute(dataRequest);

    return searchResults;

  }


  /**
   * Ali Lakhani
   *
   * @param orderDetailId
   * @param request type Id
   * @return Document
   * @throws java.lang.Exception
   *
   */
  public CachedResultSet getQueueInfoByIdXML(String orderDetailId, String requestType) throws Exception
  {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("COM_GET_QUEUE_RECORD_BY_ORDER");
        dataRequest.addInputParam("IN_ORDER_NUMBER", orderDetailId);
        dataRequest.addInputParam("IN_REQUEST_TYPE", requestType);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

 
        CachedResultSet searchResults = (CachedResultSet) dataAccessUtil.execute(dataRequest);

        return searchResults;

  }

   /**
   * Retrieve the mercury status.
   *
   * @param orderDetailId
   * @param message type
   * @param comp order
   * @return CachedResultSet
   * @throws java.lang.Exception
   *
   */
  public CachedResultSet getMercuryOrderMessageStatus(String orderDetailId, String messageType, String compOrder) throws Exception
  {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("GET_MERCURY_STATUS");
        dataRequest.addInputParam("IN_ORDER_DETAIL_ID", orderDetailId);
        dataRequest.addInputParam("IN_MESSAGE_TYPE", messageType);
        dataRequest.addInputParam("IN_COMP_ORDER", compOrder);


        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

 
        CachedResultSet searchResults = (CachedResultSet) dataAccessUtil.execute(dataRequest);

        return searchResults;

  }

  /**
   * Retrieve the ftd message attempted not verified status.
   *
   * @param orderDetailId
   * @return CachedResultSet
   * @throws java.lang.Exception
   *
   */
  public String getMercuryAttemptedStatus(String orderDetailId) throws Exception
  {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("MO_FTD_MESSAGE_ATTD_NOT_VER");
        dataRequest.addInputParam("IN_ORDER_DETAIL_ID", orderDetailId);
       
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
 
        String wasAttempted = (String) dataAccessUtil.execute(dataRequest);
        
        return wasAttempted;

  }

   /**
   * Check to see if the product is a monthly product.
   *
   * @param productId
   * @return String
   * @throws java.lang.Exception
   *
   */
   public String isFlowerMonthlyProduct(String productId) throws Exception
    {
      DataRequest dataRequest = new DataRequest();
      dataRequest.setConnection(this.connection);
      dataRequest.setStatementID("IS_FLOWER_MONTHLY_PRODUCT");
      dataRequest.addInputParam("IN_PRODUCT_ID", productId);

      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

 
      String searchResults = (String) dataAccessUtil.execute(dataRequest);

      return searchResults;

    }

  /**
   * updateOrderDetailUpdateFlorist
   *
   * Updates the florist id on the order_details_update table.
   *
   */
  public void updateFloristIdTempTable(String orderDetailId, String updatedBy,String floristId) throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(this.connection);
    dataRequest.setStatementID("UPDATE_FLORIST_TEMP_TABLE");
    dataRequest.addInputParam("IN_ORDER_DETAIL_ID", orderDetailId);
    dataRequest.addInputParam("IN_UPDATED_BY", updatedBy);
    dataRequest.addInputParam("IN_FLORIST_ID", floristId);

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

 
    Map outputs = (Map) dataAccessUtil.execute(dataRequest);

    String status = (String) outputs.get(COMConstants.STATUS_PARAM);
    if( StringUtils.equals(status, "N") )
    {
        String message = (String) outputs.get(COMConstants.MESSAGE_PARAM);
        throw new Exception(message);
    }

  }



  /**
   * Ali Lakhani
   * updateMilesPoints
   *
   * Updates the florist id on the order_details_update table.
   *
   */
  public void updateMilesPoints(String orderDetailId, String updatedBy, String milesPoints) throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(this.connection);
    dataRequest.setStatementID("MO_UPDATE_MILES_POINTS");
    dataRequest.addInputParam("IN_ORDER_DETAIL_ID",   orderDetailId);
    dataRequest.addInputParam("IN_UPDATED_BY",        updatedBy);
    dataRequest.addInputParam("IN_MILES_POINTS",      milesPoints);

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

 
    Map outputs = (Map) dataAccessUtil.execute(dataRequest);

    String status = (String) outputs.get(COMConstants.STATUS_PARAM);
    if( StringUtils.equals(status, "N") )
    {
        String message = (String) outputs.get(COMConstants.MESSAGE_PARAM);
        throw new Exception(message);
    }

  }


  /**
   * Ali Lakhani
   *
   * @return Document containing the values from the cursor
   *
   */
  public String getFloralServiceCharge(String productType, String sourceCode, String domesticIntlFlag,
                                       String price, String shippingKey)
         throws Exception
  {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("MO_OE_GET_FLORAL_SERVICE_CHARGE");
        dataRequest.addInputParam("IN_PRODUCT_TYPE",        productType);
        dataRequest.addInputParam("IN_DOMESTIC_INTL_FLAG",  domesticIntlFlag);
        dataRequest.addInputParam("IN_SOURCE_CODE",         sourceCode);
        dataRequest.addInputParam("IN_PRICE",               new Double(price));
        dataRequest.addInputParam("IN_SHIPPING_KEY",        new Long(shippingKey));

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

 
        String searchResults = (String) dataAccessUtil.execute(dataRequest);

        return searchResults;

  }






  /**
   * getLastPaymentUsed
   *
   * Gets cursor of last payment used on an order.
   *
   * @param a_orderDetailId
   * @return HashMap - last payment record results
   * @throws java.lang.Exception
   */
  public HashMap getLastPaymentUsed(String a_orderDetailId) throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    String lastPayMethod = null;
		String ccId = null;
		String cardNumber = null;
		String expirationDate = null;
		String gcCouponNumber = null;
		HashMap resultsMap = new HashMap();

    dataRequest.setConnection(this.connection);
    dataRequest.setStatementID("GET_LAST_PAYMENT_USED");
    dataRequest.addInputParam("IN_ORDER_DETAIL_ID", a_orderDetailId);

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

		Date startTime = null;
		if(logger.isDebugEnabled())
		{
			startTime = new Date();
		}

    CachedResultSet searchResults = (CachedResultSet) dataAccessUtil.execute(dataRequest);

     /* populate object */
      if (searchResults.next())
      {
        /* There was no VO that made sense to store the values in so a
				 * HashMap was used
				 */
				lastPayMethod = searchResults.getString("PAYMENT_TYPE");
				ccId= searchResults.getString("cc_id");
        cardNumber = searchResults.getString("card_number");
			  expirationDate = searchResults.getString("expiration_date");
			  gcCouponNumber = searchResults.getString("gc_coupon_number");

				if(lastPayMethod != null)
					resultsMap.put(COMConstants.HM_PAYMENT_TYPE, lastPayMethod);

				if(ccId != null)
					resultsMap.put(COMConstants.HM_CC_ID, ccId);

				if(cardNumber != null)
					resultsMap.put(COMConstants.HM_CARD_NUMBER, cardNumber);

				if(expirationDate != null)
					resultsMap.put(COMConstants.HM_EXPIRATION_DATE, expirationDate);

				if(gcCouponNumber != null)
					resultsMap.put(COMConstants.HM_GC_COUPON_NUMBER, gcCouponNumber);

				if(logger.isDebugEnabled())
				{
					logger.debug("lastPayMethod: " + lastPayMethod);
					logger.debug("ccId: " + ccId);
					logger.debug("cardNumber: " + cardNumber);
					logger.debug("expirationDate: " + expirationDate);
					logger.debug("gcCouponNumber: " + gcCouponNumber);
				}
      }

    return resultsMap;
  }
  
  
    /**
     * Call stored procedure to return Message Value Object based 
     * on order detail id and message type.
     * 
     * Populates the following fields in the MessagseVO
     * -sending florist
     * -filling florist
     * -message id
     * 
     */
    public FTDMessageVO getMessageDetailLastFTD(String orderDetailID, String messageType)
        throws Exception
    {          
       FTDMessageVO messageVO = new FTDMessageVO();
       messageVO = getMessageDetailLastFTD(orderDetailID, messageType, "Y");
        
       return messageVO;

    }
    
    /**
     * Overloaded method to call stored procedure to return Message Value Object based 
     * on order detail id and message type.
     * 
     * Populates the following fields in the MessagseVO
     * -sending florist
     * -filling florist
     * -message id
     * 
     */
    public FTDMessageVO getMessageDetailLastFTD(String orderDetailID, String messageType, String compOrder)
        throws Exception
    {            
    
        DataRequest request = new DataRequest();
        FTDMessageVO messageVO = new FTDMessageVO();
        CachedResultSet messageRS=null;
        
        try {
        
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put("IN_ORDER_DETAIL_ID",orderDetailID);
            inputParams.put("IN_MESSAGE_TYPE",messageType);
            inputParams.put("IN_COMP_ORDER",compOrder);            
            
            /* build DataRequest object */
            request.setConnection(connection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID("GET_MESG_DETAIL_FROM_LAST_FTD");
        
           /* execute the store prodcedure */
            DataAccessUtil dau = DataAccessUtil.getInstance();
            messageRS = (CachedResultSet) dau.execute(request);
            SimpleDateFormat sdfOutput = new SimpleDateFormat (MessagingConstants.MESSAGE_DATE_FORMAT);
            SimpleDateFormat sdfEndDeliveryOutput = new SimpleDateFormat (MessagingConstants.MESSAGE_END_DELIVERY_DATE_FORMAT);          
                       
            String textDate = "";
            java.util.Date utilDate;
            if(messageRS.next())
            {  
                //check if florist of vendor order
                if(messageType.equals(MessagingConstants.MERCURY_MESSAGE))
                {
                    messageVO.setFillingFloristCode(messageRS.getString("filling_florist"));
                    messageVO.setSendingFloristCode(messageRS.getString("sending_florist"));                    
                    messageVO.setCurrentPrice(getDouble(messageRS.getObject("price")));
                    messageVO.setMessageOrderId(messageRS.getString("message_order_number"));
                    messageVO.setDeliveryDate(messageRS.getDate("delivery_date"));
                }
                else
                {
                    messageVO.setFillingFloristCode(messageRS.getString("filling_vendor"));
                    messageVO.setSendingFloristCode(messageRS.getString("sending_vendor"));  
                    messageVO.setCurrentPrice(getDouble(messageRS.getObject("price")));
                    messageVO.setMessageOrderId(messageRS.getString("message_order_number"));
                  
                    if(messageRS.getDate("ship_date") != null)
                    {
                      textDate = sdfOutput.format(messageRS.getDate("ship_date"));
                      utilDate = sdfOutput.parse( textDate );
                      messageVO.setShipDate(utilDate);
                    }

                    messageVO.setShippingSystem(messageRS.getString("shipping_system"));
                    messageVO.setOrderDetailId(messageRS.getString("reference_number"));
                    messageVO.setZoneJumpFlag(messageRS.getString("ZONE_JUMP_FLAG") == null ? false : true);
                    messageVO.setZoneJumpLabelDate(getUtilDate(messageRS.getObject("ZONE_JUMP_LABEL_DATE")));
                    messageVO.setSdsStatus(messageRS.getString("sds_status"));
                    messageVO.setDeliveryDate(messageRS.getDate("delivery_date"));
                    messageVO.setShipDate(messageRS.getDate("ship_date"));
                }

                messageVO.setMessageId(messageRS.getString("ftd_message_id"));
            }

        }finally {
           // this.closeResultSet(messageRS);
            if(logger.isDebugEnabled()){
               logger.debug("Exiting getMessageDetailNewFTD");
            } 
        }
        
        return messageVO;

    
    }
    
    private double getDouble(Object obj)
    {
        double value = 0.00;
        if(obj != null)
        {
            value = Double.parseDouble(obj.toString());
        }
        
        return value;
    }

    /**
     * This method will record that changes that were made to order in a table.
     * 
     * @param origOrderDetailId
     * @param origExternalOrderNumber
     * @param newOrderDetailId
     * @param origOrderDetailid
     * @param csrId
     * @param changesMap
     */
    public void insertChanges(String origOrderDetailId, 
                                String origExternalOrderNumber,
                                String newOrderDetailId,
                                String newExternalOrderNumber,
                                String csrId,
                                Map changesMap) throws Exception
    {
    String commentId = null;
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(this.connection);
    dataRequest.setStatementID("INSERT_MO_XREF");
    dataRequest.addInputParam("IN_ORIG_ORDER_DETAIL_ID", origOrderDetailId);
    dataRequest.addInputParam("IN_NEW_ORDER_DETAIL_ID", newOrderDetailId);
    dataRequest.addInputParam("IN_ORIG_EXTERNAL_ORDER_NUMBER",origExternalOrderNumber);    
    dataRequest.addInputParam("IN_NEW_EXTERNAL_ORDER_NUMBER", newExternalOrderNumber);
    dataRequest.addInputParam("IN_RECIP_NAME", getChangeValue((String)changesMap.get("recip_name")));
    dataRequest.addInputParam("IN_RECIP_BUSINESS", getChangeValue((String)changesMap.get("recip_business")));
    dataRequest.addInputParam("IN_RECIP_ADDRESS", getChangeValue((String)changesMap.get("recip_address")));
    dataRequest.addInputParam("IN_RECIP_DELIVERY_LOCATION", getChangeValue((String)changesMap.get("recip_delivery_location")));
    dataRequest.addInputParam("IN_RECIP_CITY", getChangeValue((String)changesMap.get("recip_city")));
    dataRequest.addInputParam("IN_RECIP_STATE", getChangeValue((String)changesMap.get("recip_state")));
    dataRequest.addInputParam("IN_RECIP_ZIPCODE", getChangeValue((String)changesMap.get("recip_zipcode")));
    dataRequest.addInputParam("IN_RECIP_COUNTRY", getChangeValue((String)changesMap.get("recip_country")));
    dataRequest.addInputParam("IN_RECIP_PHONE", getChangeValue((String)changesMap.get("recip_phone")));
    dataRequest.addInputParam("IN_DELIVERY_DATE", getChangeValue((String)changesMap.get("delivery_date")));
    dataRequest.addInputParam("IN_CARD_MESSAGE", getChangeValue((String)changesMap.get("card_message")));
    dataRequest.addInputParam("IN_SIGNATURE", getChangeValue((String)changesMap.get("signature")));
    dataRequest.addInputParam("IN_SPECIAL_INSTRUCTIONS", getChangeValue((String)changesMap.get("special_instructions")));
    dataRequest.addInputParam("IN_ALT_CONTACT_INFO", getChangeValue((String)changesMap.get("alt_contact_info")));
    dataRequest.addInputParam("IN_PRODUCT", getChangeValue((String)changesMap.get("product")));
    dataRequest.addInputParam("IN_PRODUCT_PRICE", getChangeValue((String)changesMap.get("product_price")));
    dataRequest.addInputParam("IN_SHIP_TYPE_CHANGED", getChangeValue((String)changesMap.get("ship_type_changed")));
    dataRequest.addInputParam("IN_COLOR_FIRST_CHOICE", getChangeValue((String)changesMap.get("color_first_choice")));
    dataRequest.addInputParam("IN_COLOR_SECOND_CHOICE", getChangeValue((String)changesMap.get("color_second_choice")));
    dataRequest.addInputParam("IN_SUBSTITUTION_INDICATOR", getChangeValue((String)changesMap.get("substitution_indicator")));
    dataRequest.addInputParam("IN_SHIP_METHOD", getChangeValue((String)changesMap.get("ship_method")));
    dataRequest.addInputParam("IN_SOURCE_CODE", getChangeValue((String)changesMap.get("source_code")));
    dataRequest.addInputParam("IN_MEMBERSHIP_NAME", getChangeValue((String)changesMap.get("membership_name")));
    dataRequest.addInputParam("IN_MEMBERSHIP_NUMBER", getChangeValue((String)changesMap.get("membership_number")));    
    dataRequest.addInputParam("IN_ADD_ONS", getChangeValue((String)changesMap.get("add_ons")));
    dataRequest.addInputParam("IN_VENDOR_TO_FLORIST", getChangeValue((String)changesMap.get("vendor_to_florist")));
    dataRequest.addInputParam("IN_FLORIST_TO_VENDOR", getChangeValue((String)changesMap.get("florist_to_vendor")));    
    dataRequest.addInputParam("IN_CREATED_BY", getChangeValue((String)changesMap.get("created_by")));
    dataRequest.addInputParam("IN_PERSONAL_GREETING_ID", getChangeValue((String)changesMap.get("personal_greeting_id")));
    
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

    Map outputs = (Map) dataAccessUtil.execute(dataRequest);

    String status = (String) outputs.get(COMConstants.STATUS_PARAM);
    if( StringUtils.equals(status, "N") )
    {
        String message = (String) outputs.get(COMConstants.MESSAGE_PARAM);
        throw new Exception(message);
    }

    }

	
	private String getChangeValue(String value)
  {
      return value == null ? "N" : value;
  }


	/**
	* Obtains the next sequence number for the external order number
	*/
  public String getExternalOrderSeqNum() throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(this.connection);
    dataRequest.setStatementID("MO_GET_ORDER_SEQUENCE");
    
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    CachedResultSet crs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
    
    String externalOrderSeqNumber = null;
    if(crs.next())
    {
      externalOrderSeqNumber = (String) crs.getString("orderSequence");
    }
    
    return externalOrderSeqNumber;
  }
  

	/**
	* Obtains the next sequence number for the master order number
	*/
  public String getMasterOrderSeqNum() throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(this.connection);
    dataRequest.setStatementID("MO_GET_SEQUENCE_UTIL");
    
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    CachedResultSet crs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
    
    String masterOrderSeqNumber = null;
    if(crs.next())
    {
      masterOrderSeqNumber = (String) crs.getString("NEXTVAL");
    }
    
    return masterOrderSeqNumber;
  }
  
 public String getPaymentType(String paymentMethod) throws Exception
 {
      Map paramMap = new HashMap();     

      boolean exists = false;

      paramMap.put("payment_method_id",paymentMethod);

      // Get the order header infomration
      DataRequest dataRequest = new DataRequest();
      dataRequest.setConnection(connection);
      dataRequest.setStatementID("GET_PAYMENT_METHOD_BY_ID_SET");
      dataRequest.setInputParams(paramMap);

      Map addonMap = null;
      
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      CachedResultSet rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);       
      String paymentType = null;
      if(rs != null && rs.next())
      {
        paymentType = (String)rs.getObject("payment_type");
      }   

      return paymentType;
 }
 
 public String getPartnerName(String sourceCode) throws Exception
 {
      Map paramMap = new HashMap();     

      boolean exists = false;

      paramMap.put("IN_SOURCE_CODE",sourceCode);

      // Get the order header infomration
      DataRequest dataRequest = new DataRequest();
      dataRequest.setConnection(connection);
      dataRequest.setStatementID("GET_PARTNER_NAME");
      dataRequest.setInputParams(paramMap);
      
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      HashMap map = (HashMap)dataAccessUtil.execute(dataRequest);       
      String partnerName = null;
      if(map != null)
      {
          partnerName = (String)map.get("OUT_PROGRAM_NAME");          
      }

      return partnerName;
 } 

  /**
   * This is a wrapper for CLEAN.ORDER_MAINT_PKG.DELETE_ORDER_HOLD.
   * @throws javayeah.lang.Exception
   * @param String orderDetailId
   */
  public void deleteOrderHold(String orderDetailId) throws Exception
  {
      logger.debug(" deleteOrderHold(String orderDetailId("+orderDetailId+"))");
      DataRequest dataRequest = new DataRequest();
      Map outputs = null;   
       
      /* setup store procedure input parameters */
      HashMap inputParams = new HashMap();
      inputParams.put("IN_ORDER_DETAIL_ID", orderDetailId);

      /* build DataRequest object */
      dataRequest.setConnection(connection);
      dataRequest.setStatementID("DELETE_ORDER_HOLD");
      dataRequest.setInputParams(inputParams);
      
      /* execute the store prodcedure */
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
      outputs = (Map) dataAccessUtil.execute(dataRequest);
      
      /* read store prodcedure output parameters to determine 
       * if the procedure executed successfully */
      String status = (String) outputs.get("OUT_STATUS");
      if(status != null && status.equalsIgnoreCase("N"))
      {
        String message = (String) outputs.get("OUT_ERROR_MESSAGE");
        
        //This proc returns warnings, so only throw exception if error occured
        if(message.startsWith("ERROR"))
        {
            throw new Exception(message);            
        }
      }
    }

public String getScrubOrderDetailId(String externalOrderNumber) throws Exception
{
      Map paramMap = new HashMap();     

      boolean exists = false;

      paramMap.put("IN_EXTERNAL_ORDER_NUMBER",externalOrderNumber);

      // Get the order header infomration
      DataRequest dataRequest = new DataRequest();
      dataRequest.setConnection(connection);
      dataRequest.setStatementID("GET_SCRUB_ORDER_DETAIL_ID");
      dataRequest.setInputParams(paramMap);
      
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      HashMap map = (HashMap)dataAccessUtil.execute(dataRequest);       
      String orderDetailId = null;
      if(map != null)
      {
          orderDetailId = (String)map.get("OUT_ORDER_DETAIL_ID");          
      }

      return orderDetailId;    
}
 

  /**
   * This method will check if an order has already been modified.  If so, it will return a 'Y'
   * 
   * @throws javayeah.lang.Exception
   * @param String externalOrderNumber
   */

  public String hasOrderBeenModified(String externalOrderNumber) throws Exception
  {
      DataRequest dataRequest = new DataRequest();
      dataRequest.setConnection(this.connection);
      dataRequest.setStatementID("MO_HAS_ORDER_BEEN_MODIFIED");
      dataRequest.addInputParam("IN_EXTERNAL_ORDER_NUMBER", externalOrderNumber);
      
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      String searchResults = (String) dataAccessUtil.execute(dataRequest);
      
      return searchResults;

  }

  /**
   * This method will check if cart is in Final state.  If so, it will return a 'Y'
   * 
   * @throws javayeah.lang.Exception
   * @param String externalOrderNumber
   */

  public String isCartInFinalState(String orderDetailId) throws Exception
  {
      DataRequest dataRequest = new DataRequest();
      dataRequest.setConnection(this.connection);
      dataRequest.setStatementID("MO_IS_CART_IN_FINAL_STATE");
      dataRequest.addInputParam("IN_ORDER_DETAIL_ID", orderDetailId);
      
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      String searchResults = (String) dataAccessUtil.execute(dataRequest);
      
      return searchResults;

  }
  
  public String getInternationalFlorist(String orderDetailId) throws Exception
  {    
      /* setup store procedure input parameters */
      HashMap inputParamsIntl = new HashMap();
      inputParamsIntl.put("IN_ORDER_DETAIL_ID", new Long(orderDetailId));

      /* build DataRequest object */      
      DataRequest dataRequestIntl = new DataRequest();
      dataRequestIntl.setConnection(this.connection);
      dataRequestIntl.setStatementID("GET_INTERNATIONAL_FLORIST");
      dataRequestIntl.setInputParams(inputParamsIntl);
      
      /* execute the store prodcedure */
      DataAccessUtil dataAccessUtilIntl = DataAccessUtil.getInstance();    
      Map outputsIntl = (Map) dataAccessUtilIntl.execute(dataRequestIntl);
      
      /* read store prodcedure output parameters to determine 
       * if the procedure executed successfully */
      String status = (String) outputsIntl.get("OUT_STATUS");
     
      if(status != null && status.equalsIgnoreCase("N"))
      {
        String message = (String) outputsIntl.get("OUT_ERROR_MESSAGE");
        throw new Exception(message);
      }

      return (String) outputsIntl.get("OUT_FLORIST_CODE");
  }
  
    /**
     * This method returns the name, type and member number for the passed in vendor id.
     * 
     * @param String - vendorId
     * @return CachedResultSet
     */
     public CachedResultSet getVendorInfo(String vendorId) throws Exception
    {        
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("GET_VENDOR");
        dataRequest.addInputParam("VENDOR_ID", vendorId);
    
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        CachedResultSet rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);
    
        return rs;

    }
    
   
    
    /**
     * This method is responsible for returning mercury/venus message details
     * for the given order
     * @param messageOrderNumber - String
     * @param messageType - String
     * @return CachedResultSet
     * @throws SAXException
     * @throws IOException
     * @throws ParserConfigurationException
     * @throws SQLException
     */
    public CachedResultSet getMessageStatus(String messageOrderNumber, String messageType) 
        throws SAXException, IOException, SQLException, ParserConfigurationException
   {        
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("GET_MESSAGE_STATUS");
        dataRequest.addInputParam("IN_MESSAGE_ORDER_NUMBER", messageOrderNumber);
        dataRequest.addInputParam("IN_MESSAGE_TYPE", messageType);
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        CachedResultSet rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);
        
        return rs;
    }
    
    /**
    * This method is a wrapper for the GET_ORIGINAL_ORDER_BILL SP.
    * It returns original Order Bill info.
    *
    * @param orderDetailId - String  
    * @return CachedResultSet
    * @throws Exception
    */
    public CachedResultSet getOrderBill(String orderDetailId) throws Exception {
        
        logger.debug("getOrderBill (String orderDetailId (" + orderDetailId + 
                     ")) :: CachedResultSet");
                     
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("GET_ORIGINAL_ORDER_BILL");
        dataRequest.addInputParam("IN_ORDER_DETAIL_ID", orderDetailId);
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        CachedResultSet rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);
        
        return rs;
    }
    
    /**
    * Update the delivery date, date range end, florist id and ship date on the order.  
    * This method is used by Update Delivery Date function.
    * @param orderDetailID - String
    * @param csr_id - String
    * @param startDate - Calendar
    * @param endDate - Calendar
    * @param shipDate - Calendar
    * @param floristId - String
    * @throws java.io.IOException
    * @throws org.xml.sax.SAXException
    * @throws javax.xml.parsers.ParserConfigurationException
    * @throws java.sql.SQLException
    * @throws java.lang.Exception
    */
    public void updateUDDOrderDetails(String orderDetailID, String csr_id, Calendar startDate, 
                                        Calendar endDate, Calendar shipDate, String floristId) 
        throws IOException, SAXException, ParserConfigurationException, 
        SQLException, Exception
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering updateUDDOrderDetails");
            logger.debug("orderDetailID: " + orderDetailID);
            logger.debug("start date: " + startDate);
            logger.debug("end date: " + endDate);
            logger.debug("ship date: " + shipDate);
            logger.debug("florist id: " + floristId);
        }
        
        java.sql.Date uddEndDate = null;
        java.sql.Date uddShipDate = null;
        
         
        if(endDate != null)
            uddEndDate = new java.sql.Date(endDate.getTimeInMillis());
            
        if(shipDate != null)
            uddShipDate = new java.sql.Date(shipDate.getTimeInMillis());    
        
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("UDD_UPDATE_ORDER_DETAILS");
        dataRequest.addInputParam("IN_ORDER_DETAIL_ID",  orderDetailID);
        dataRequest.addInputParam("IN_DELIVERY_DATE",   new java.sql.Date(startDate.getTimeInMillis()));
        dataRequest.addInputParam("IN_DELIVERY_DATE_RANGE_END", uddEndDate);
        dataRequest.addInputParam("IN_SHIP_DATE",  uddShipDate);
        dataRequest.addInputParam("IN_FLORIST_ID",  floristId);
        dataRequest.addInputParam("IN_UPDATED_BY",  csr_id);
                
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);      
        String status = (String) outputs.get(COMConstants.STATUS_PARAM);
        if(status != null && status.equals("N"))
        {
            String message = (String) outputs.get(COMConstants.MESSAGE_PARAM);
            throw new Exception(message);
        }
        logger.debug("Update Order Delivery Date Successful. Status="+status);
        
    }     
    
    /**
    * This method will return comp add on info for product/source code passed in.
    * @param sourceCode
    * @param productId
    * @return CachedResultSet
    * @throws Exception
    */
    public CachedResultSet getCompAddOnInfo(String sourceCode, String productId)  throws Exception
    {
       DataRequest dataRequest = new DataRequest();
       dataRequest.setConnection(this.connection);
       dataRequest.setStatementID("GET_COMP_ADD_ON_INFO");
       dataRequest.addInputParam("IN_SOURCE_CODE",sourceCode);
       dataRequest.addInputParam("IN_PRODUCT_ID",productId);
    
       DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    
       CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
       return rs;
    }
    
    /*
     * Converts a database timestamp to a java.util.Date.
     */
    private java.util.Date getUtilDate(Object time)
    {
        java.util.Date theDate = null;
        boolean test  = false;
        if(time != null)
        {
            if(test || time instanceof Timestamp)
            {
                Timestamp timestamp = (Timestamp) time;
                theDate = new java.util.Date(timestamp.getTime());
            }
            else
            {
                theDate = (java.util.Date) time;            
            }
        }
        
        return theDate;
    }

    /**
     * Get the oroder from phoenix_order_tracking
     */
    public boolean getPhoenixOrder(String externalOrderNumber) throws Exception {
    

    	DataRequest dataRequest = new DataRequest();       
    	dataRequest.setConnection(connection);
    	dataRequest.setStatementID("IS_PHOENIX_ORDER");
    	dataRequest.addInputParam("IN_ORDER_NUMBER",externalOrderNumber);
    	
    	DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    	String searchResults = (String) dataAccessUtil.execute(dataRequest);
    	
    	if(searchResults != null && searchResults.equalsIgnoreCase("Y"))
    		return true;
    	else
    		return false;
    }
    
   
    
    

}