package com.ftd.mo.dao;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;

import org.w3c.dom.Document;

//import oracle.xml.parser.v2.Document;

/**
 * InvoiceDAO
 * 
 * @version $Id: InvoiceDAO.java
 */


public class InvoiceDAO 
{
    private Connection connection;
    private static Logger logger  = new Logger("com.ftd.customerordermanagement.dao.InvoiceDAO");
    
    
  /**
   * Constructor
   * @param connection Connection
   */
  public InvoiceDAO(Connection connection)
  {
    this.connection = connection;
  }


  /**
   * Obtains billing info for an Invoice payment
   * 
   * @param sourceCode - Source Code
   * 
   * @return Document containing billing info as XML 
   * 
   * @throws java.lang.Exception
   */
  public Document getBillingInfo(String sourceCode) 
        throws Exception
  {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("GET_BILLING_INFO_SC");

        dataRequest.addInputParam("sourceCode", sourceCode);
        dataRequest.addInputParam("prompt", null);
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      
        Document searchResults = (Document) dataAccessUtil.execute(dataRequest);      

        return searchResults;
  }


  /**
   * Obtains billing info options for an Invoice payment
   * 
   * @param sourceCode - Source Code
   * 
   * @return Document containing billing info as XML 
   * 
   * @throws java.lang.Exception
   */
  public Document getBillingInfoOptions(String sourceCode) 
        throws Exception
  {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("GET_BILLING_INFO_OPT_SC");

        dataRequest.addInputParam("sourceCode", sourceCode);
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      
        Document searchResults = (Document) dataAccessUtil.execute(dataRequest);      

        return searchResults;
  }
	
}