package com.ftd.mo.vo;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * DeliveryDateVO encapsulates a COM delivery date.
 * @author Mark Moon, Software Architects Inc.
 * @version $Revision: 1.2 $
 */
public class DeliveryDateVO {

  public static final String DEFAULT_FORMAT = "EEE MM/dd/yyyy";

  private boolean isRange;
  private String dateFormat;
  private String displayDate;
  private Date startDate;
  private Date endDate;
  private SimpleDateFormat formatter;

  public DeliveryDateVO() {
    super();
    dateFormat = "EEE MM/dd/yyyy";
    formatter = new SimpleDateFormat();
  }

  /**
   * @return Returns the displayDate.
   */
  public String getDisplayDate() {

    if ( StringUtils.isBlank(displayDate) && startDate != null ) {
      if ( StringUtils.isBlank(dateFormat) ) {
        dateFormat = DEFAULT_FORMAT;
      }
      formatter.applyPattern( dateFormat );

      displayDate = formatter.format(startDate).toString();
      if ( isRange && endDate != null ) {
       displayDate += " - " + formatter.format(endDate).toString();
      }
    }
    return displayDate;
  }

  /**
   * @param displayDate The displayDate to set.
   */
  public void setDisplayDate( String displayDate ) {
    this.displayDate = displayDate;
  }

  /**
   * @return Returns the endDate.
   */
  public Date getEndDate() {
    return endDate;
  }

  /**
   * @param endDate The endDate to set.
   */
  public void setEndDate( Date endDate ) {
    this.endDate = endDate;
  }

  /**
   * @return Returns the isRange.
   */
  public boolean isRange() {
    return isRange;
  }

  /**
   * @param isRange The isRange to set.
   */
  public void setRange( boolean isRange ) {
    this.isRange = isRange;
  }

  /**
   * @return Returns the startDate.
   */
  public Date getStartDate() {
    return startDate;
  }

  /**
   * @param startDate The startDate to set.
   */
  public void setStartDate( Date startDate ) {
    this.startDate = startDate;
  }

  /**
   * @return Returns the dateFormat.
   */
  public String getDateFormat() {
    return dateFormat;
  }

  /**
   * @param dateFormat The dateFormat to set.
   */
  public void setDateFormat( String dateFormat ) {
    this.dateFormat = dateFormat;
  }

  /**
   * Returns a string representation of this DeliveryDateVO
   * @return String
   */
  public String toString(){
    return ToStringBuilder.reflectionToString(this);
  }
}