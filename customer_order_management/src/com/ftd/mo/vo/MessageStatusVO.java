package com.ftd.mo.vo;


/**
 * This class contains flags that indicate message order status.
 * @author Rose Lazuk
 */
public class MessageStatusVO
{

    private boolean compOrder = false;
    private boolean liveMercury = true;




  public void setCompOrder(boolean isCompOrder)
  {
    this.compOrder = isCompOrder;
  }


  public boolean isLiveMercury()
  {
    return liveMercury;
  }
  
    public void setLiveMercury(boolean isLiveMercury)
    {
      this.liveMercury = isLiveMercury;
    }


    public boolean isCompOrder()
    {
      return compOrder;
    }

}
