package com.ftd.mo.vo;

import java.text.SimpleDateFormat;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.sql.Timestamp;
import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.osp.utilities.order.vo.OrderExtensionsVO;
import com.ftd.osp.utilities.xml.DOMUtil;


import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;



public class DeliveryInfoVO extends BaseVO implements XMLInterface, Cloneable {




  private static final String DATE_FORMAT_WITH_DAY = "EEE MM/dd/yyyy";
  private static final String DATE_FORMAT = "MM/dd/yyyy";

  // This first set of attributes correspond to fields in order_detail_update table
  private double   actualAddOnAmount;
  private double   actualDiscountAmount;
  private double   actualProductAmount;
  private double   actualServiceFee;
  private double   actualShippingFee;
  private double   actualShippingTax;
  private double   actualTax;
  private String   cardMessage;
  private String   cardSignature;
  private String   color1;
  private String   color2;
  private Calendar deliveryDate;
  private Calendar deliveryDateRangeEnd;
  private Calendar EODDeliveryDate;
  private String   EODDeliveryIndicator;
  private String   exceptionCode;
  private Calendar exceptionEndDate;
  private Calendar exceptionStartDate;
  private String   floristId;
  private String   membershipFirstName;
  private String   membershipLastName;
  private String   membershipNumber;
  private double   milesPoints;
  private String   occasion;
  private String   occasionDescription;
  private String   orderDetailId;
  private String   orderGuid;
  private String   productAmount;
  private String   productId;
  private long     quantity;
  private String   recipientAddress1;
  private String   recipientAddress2;
  private String   recipientAddressType;
  private String   recipientBusinessInfo;
  private String   recipientBusinessName;
  private String   recipientCity;
  private String   recipientCountry;
  private String   recipientExtension;
  private String   recipientFirstName;
  private String   recipientId;
  private String   recipientLastName;
  private String   recipientPhoneNumber;
  private String   recipientState;
  private String   recipientZipCode;
  private String   releaseInfoIndicator;
  private String   secondChoiceProduct;
  private Calendar shipDate;
  private String   shipMethod;
  private String   sourceCode;
  private String   specialInstructions;
  private String   subcode;
  private String   substitutionIndicator;
  private String   updatedBy;
  private Calendar updatedOn;
  private String personalGreetingId;
  private String noTaxFlag;
  private String   bearAddonId;
  private String   chocolateAddonId;

  // These attributes correspond to fields in order_contact_update table
  private String   altContactFirstName;
  private String   altContactLastName;
  private String   altContactPhoneNumber;
  private String   altContactExtension;
  private String   altContactEmail;
  private String   altContactInfoId;

  // These attributes are retrieved from miscellaneous DB tables for display,
  // but are NOT updated during UpdateDeliveryInfo processing.  To be specific,
  // the clean.modify_order_pkg.VIEW_DELIVERY_INFO stored proc retrieves this info.
  private String   canChangeOrderSource;
  private String   companyId;
  private String   customerCity;
  private String   customerCountry;
  private String   customerFirstName;
  private String   customerId;
  private String   customerLastName;
  private String   customerState;
  private String   customerZipCode;
  private String   masterOrderNumber;
  private String   membershipRequired;
  private String   occasionCategoryIndex;
  private String   originId;
  private String   sizeIndicator;
  private String   shipMethodCarrier;
  private String   shipMethodDesc;
  private String   shipMethodFlorist;
  private String   shippingFee;
  private String   sourceCodeDescription;
  private String   sourceType;
  private String   vendorFlag;
  private String   pcFlag;
  private String   pcMembershipId;
  private String   pcGroupId;


  // These attributes are also NOT updated during UpdateDeliveryInfo processing,
  // but are used to flag differences when comparing DeliveryInfoVO's.
  private boolean  changesToZip            = false;
  private boolean  changesToDeliveryDate   = false;
  private boolean  changesToSourceCode     = false;
  private boolean  changesToShipMethod     = false;
  private boolean  changesToCountry        = false;
  private boolean  changedToSundayDelivery = false;

    private String messageId;
    private String messageOrderNumber;
    private String ftdmIndicator;

  private List <OrderExtensionsVO> orderExtensions = null;
  
  private String buyerSignedInFlag;
  
  private String originalOrderHasSDU;
  
  private String origNewProdSame;
  

public String getOrigNewProdSame() {
	return origNewProdSame;
}

public void setOrigNewProdSame(String origNewProdSame) {
	this.origNewProdSame = origNewProdSame;
}

public DeliveryInfoVO() {
  }

  public Object clone() throws CloneNotSupportedException
  {
      return super.clone();
  }

  public void setOrderDetailId(String a_value) {
    if (valueChanged(orderDetailId, a_value)) {
      setChanged(true);
    }
    orderDetailId = trim(a_value);
  }
  public String getOrderDetailId() {
    return orderDetailId;
  }


  public void setOrderGuid(String a_value) {
    if (valueChanged(orderGuid, a_value)) {
      setChanged(true);
    }
    orderGuid = trim(a_value);
  }
  public String getOrderGuid() {
    return orderGuid;
  }


  public void setUpdatedOn(Calendar a_value) {
    if (valueChanged(updatedOn, a_value)) {
      setChanged(true);
    }
    updatedOn = a_value;
  }
  public Calendar getUpdatedOn() {
    return updatedOn;
  }


  public void setUpdatedBy(String a_value) {
    if (valueChanged(updatedBy, a_value)) {
      setChanged(true);
    }
    updatedBy = trim(a_value);
  }
  public String getUpdatedBy() {
    return updatedBy;
  }


  public void setSourceCode(String a_value) {
    if (valueChanged(sourceCode, a_value)) {
      setChanged(true);
    }
    sourceCode = trim(a_value);
  }
  public String getSourceCode() {
    return sourceCode;
  }


  public void setDeliveryDate(Calendar a_value) {
    if (valueChanged(deliveryDate, a_value)) {
      setChanged(true);
    }
    deliveryDate = a_value;
  }
  public Calendar getDeliveryDate() {
    return deliveryDate;
  }


  public void setEODDeliveryDate(Calendar a_value) {
    if (valueChanged(EODDeliveryDate, a_value)) {
      setChanged(true);
    }
    EODDeliveryDate = a_value;
  }
  public Calendar getEODDeliveryDate() {
    return EODDeliveryDate;
  }


  public void setDeliveryDateRangeEnd(Calendar a_value) {
    if (valueChanged(deliveryDateRangeEnd, a_value)) {
      setChanged(true);
    }
    deliveryDateRangeEnd = a_value;
  }
  public Calendar getDeliveryDateRangeEnd() {
    return deliveryDateRangeEnd;
  }


  public void setCardMessage(String a_value) {
    if (valueChanged(cardMessage, a_value)) {
      setChanged(true);
    }
    cardMessage = trim(a_value);
  }
  public String getCardMessage() {
    return cardMessage;
  }


  public void setCardSignature(String a_value) {
    if (valueChanged(cardSignature, a_value)) {
      setChanged(true);
    }
    cardSignature = trim(a_value);
  }
  public String getCardSignature() {
    return cardSignature;
  }


  public void setSpecialInstructions(String a_value) {
    if (valueChanged(specialInstructions, a_value)) {
      setChanged(true);
    }
    specialInstructions = trim(a_value);
  }
  public String getSpecialInstructions() {
    return specialInstructions;
  }


  public void setShipMethod(String a_value) {
    if (valueChanged(shipMethod, a_value)) {
      setChanged(true);
    }
    shipMethod = trim(a_value);
  }
  public String getShipMethod() {
    return shipMethod;
  }


  public void setShipDate(Calendar a_value) {
    if (valueChanged(shipDate, a_value)) {
      setChanged(true);
    }
    shipDate = a_value;
  }
  public Calendar getShipDate() {
    return shipDate;
  }


  public void setMembershipNumber(String a_value) {
    if (valueChanged(membershipNumber, a_value)) {
      setChanged(true);
    }
    membershipNumber = trim(a_value);
  }
  public String getMembershipNumber() {
    return membershipNumber;
  }


  public void setMembershipFirstName(String a_value) {
    if (valueChanged(membershipFirstName, a_value)) {
      setChanged(true);
    }
    membershipFirstName = trim(a_value);
  }
  public String getMembershipFirstName() {
    return membershipFirstName;
  }


  public void setMembershipLastName(String a_value) {
    if (valueChanged(membershipLastName, a_value)) {
      setChanged(true);
    }
    membershipLastName = trim(a_value);
  }
  public String getMembershipLastName() {
    return membershipLastName;
  }


  public void setRecipientId(String a_value) {
    if (valueChanged(recipientId, a_value)) {
      setChanged(true);
    }
    recipientId = trim(a_value);
  }
  public String getRecipientId() {
    return recipientId;
  }


  public void setRecipientPhoneNumber(String a_value) {
    if (valueChanged(recipientPhoneNumber, a_value)) {
      setChanged(true);
    }
    recipientPhoneNumber = trim(a_value);
  }
  public String getRecipientPhoneNumber() {
    return recipientPhoneNumber;
  }


  public void setRecipientExtension(String a_value) {
    if (valueChanged(recipientExtension, a_value)) {
      setChanged(true);
    }
    recipientExtension = trim(a_value);
  }
  public String getRecipientExtension() {
    return recipientExtension;
  }


  public void setRecipientFirstName(String a_value) {
    if (valueChanged(recipientFirstName, a_value)) {
      setChanged(true);
    }
    recipientFirstName = trim(a_value);
  }
  public String getRecipientFirstName() {
    return recipientFirstName;
  }


  public void setRecipientLastName(String a_value) {
    if (valueChanged(recipientLastName, a_value)) {
      setChanged(true);
    }
    recipientLastName = trim(a_value);
  }
  public String getRecipientLastName() {
    return recipientLastName;
  }


  public void setRecipientAddress1(String a_value) {
    if (valueChanged(recipientAddress1, a_value)) {
      setChanged(true);
    }
    recipientAddress1 = trim(a_value);
  }
  public String getRecipientAddress1() {
    return recipientAddress1;
  }


  public void setRecipientAddress2(String a_value) {
    if (valueChanged(recipientAddress2, a_value)) {
      setChanged(true);
    }
    recipientAddress2 = trim(a_value);
  }
  public String getRecipientAddress2() {
    return recipientAddress2;
  }


  public void setRecipientCity(String a_value) {
    if (valueChanged(recipientCity, a_value)) {
      setChanged(true);
    }
    recipientCity = trim(a_value);
  }
  public String getRecipientCity() {
    return recipientCity;
  }


  public void setRecipientState(String a_value) {
    if (valueChanged(recipientState, a_value)) {
      setChanged(true);
    }
    recipientState = trim(a_value);
  }
  public String getRecipientState() {
    return recipientState;
  }


  public void setRecipientZipCode(String a_value) {
    if (valueChanged(recipientZipCode, a_value)) {
      setChanged(true);
    }
    recipientZipCode = trim(a_value);
  }
  public String getRecipientZipCode() {
    return recipientZipCode;
  }
  public String getRecipientFiveDigitZipCode() {
    if (recipientZipCode != null && recipientZipCode.length() > 5) {
      return recipientZipCode.substring(0,5);
    }
    return recipientZipCode;
  }
  public String getRecipientSmartZipCode() {
    if (recipientZipCode != null) {
      // If recipient country is Canada, trim zip to 3 chars
      if (COMConstants.CONS_COUNTRY_CODE_CANADA_CA.equals(recipientCountry)) {
        return recipientZipCode.substring(0,3);
      // Else trim zip to 5 chars (if necessary)
      } else if (recipientZipCode.length() > 5) {
        return recipientZipCode.substring(0,5);
      }
    }
    return recipientZipCode;
  }


  public void setRecipientCountry(String a_value) {
    if (valueChanged(recipientCountry, a_value)) {
      setChanged(true);
    }
    recipientCountry = trim(a_value);
  }
  public String getRecipientCountry() {
    return recipientCountry;
  }


  public void setRecipientAddressType(String a_value) {
    if (valueChanged(recipientAddressType, a_value)) {
      setChanged(true);
    }
    recipientAddressType = trim(a_value);
  }
  public String getRecipientAddressType() {
    return recipientAddressType;
  }


  public void setRecipientBusinessName(String a_value) {
    if (valueChanged(recipientBusinessName, a_value)) {
      setChanged(true);
    }
    recipientBusinessName = trim(a_value);
  }
  public String getRecipientBusinessName() {
    return recipientBusinessName;
  }


  public void setRecipientBusinessInfo(String a_value) {
    if (valueChanged(recipientBusinessInfo, a_value)) {
      setChanged(true);
    }
    recipientBusinessInfo = trim(a_value);
  }
  public String getRecipientBusinessInfo() {
    return recipientBusinessInfo;
  }


  public void setAltContactFirstName(String a_value) {
    if (valueChanged(altContactFirstName, a_value)) {
      setChanged(true);
    }
    altContactFirstName = trim(a_value);
  }
  public String getAltContactFirstName() {
    return altContactFirstName;
  }


  public void setAltContactLastName(String a_value) {
    if (valueChanged(altContactLastName, a_value)) {
      setChanged(true);
    }
    altContactLastName = trim(a_value);
  }
  public String getAltContactLastName() {
    return altContactLastName;
  }


  public void setAltContactPhoneNumber(String a_value) {
    if (valueChanged(altContactPhoneNumber, a_value)) {
      setChanged(true);
    }
    altContactPhoneNumber = trim(a_value);
  }
  public String getAltContactPhoneNumber() {
    return altContactPhoneNumber;
  }


  public void setAltContactExtension(String a_value) {
    if (valueChanged(altContactExtension, a_value)) {
      setChanged(true);
    }
    altContactExtension = trim(a_value);
  }
  public String getAltContactExtension() {
    return altContactExtension;
  }


  public void setAltContactInfoId(String a_value) {
    if (valueChanged(altContactInfoId, a_value)) {
      setChanged(true);
    }
    altContactInfoId = trim(a_value);
  }
  public String getAltContactInfoId() {
    return altContactInfoId;
  }


  public void setAltContactEmail(String a_value) {
    if (valueChanged(altContactEmail, a_value)) {
      setChanged(true);
    }
    altContactEmail = trim(a_value);
  }
  public String getAltContactEmail() {
    return altContactEmail;
  }


  public void setVendorFlag(String a_value) {
    if (valueChanged(vendorFlag, a_value)) {
      setChanged(true);
    }
    vendorFlag = trim(a_value);
  }
  public String getVendorFlag() {
    return vendorFlag;
  }


  public void setFloristId(String a_value) {
    if (valueChanged(floristId, a_value)) {
      setChanged(true);
    }
    floristId = trim(a_value);
  }
  public String getFloristId() {
    return floristId;
  }


  public void setProductId(String a_value) {
    if (valueChanged(productId, a_value)) {
      setChanged(true);
    }
    productId = trim(a_value);
  }
  public String getProductId() {
    return productId;
  }


  public void setProductAmount(String a_value) {
    if (valueChanged(productAmount, a_value)) {
      setChanged(true);
    }
    productAmount = trim(a_value);
  }
  public String getProductAmount() {
    return productAmount;
  }


  public void setActualAddOnAmount(double actualAddOnAmount)
  {
    if(valueChanged(new Double(this.actualAddOnAmount), new Double(actualAddOnAmount)))
    {
      setChanged(true);
    }
    this.actualAddOnAmount = actualAddOnAmount;
  }


  public double getActualAddOnAmount()
  {
    return this.actualAddOnAmount;
  }


  public void setActualDiscountAmount(double actualDiscountAmount)
  {
    if(valueChanged(new Double(this.actualDiscountAmount), new Double(actualDiscountAmount)))
    {
      setChanged(true);
    }
    this.actualDiscountAmount = actualDiscountAmount;
  }


  public double getActualDiscountAmount()
  {
    return this.actualDiscountAmount;
  }


  public void setActualProductAmount(double actualProductAmount)
  {
    if(valueChanged(new Double(this.actualProductAmount), new Double(actualProductAmount)))
    {
      setChanged(true);
    }
    this.actualProductAmount = actualProductAmount;
  }


  public double getActualProductAmount()
  {
    return this.actualProductAmount;
  }


  public void setActualServiceFee(double actualServiceFee)
  {
    if(valueChanged(new Double(this.actualServiceFee), new Double(actualServiceFee)))
    {
      setChanged(true);
    }
    this.actualServiceFee = actualServiceFee;
  }


  public double getActualServiceFee()
  {
    return this.actualServiceFee;
  }


  public void setActualShippingFee(double actualShippingFee)
  {
    if(valueChanged(new Double(this.actualShippingFee), new Double(actualShippingFee)))
    {
      setChanged(true);
    }
    this.actualShippingFee = actualShippingFee;
  }


  public double getActualShippingFee()
  {
    return this.actualShippingFee;
  }


  public void setActualShippingTax(double actualShippingTax)
  {
    if(valueChanged(new Double(this.actualShippingTax), new Double(actualShippingTax)))
    {
      setChanged(true);
    }
    this.actualShippingTax = actualShippingTax;
  }


  public double getActualShippingTax()
  {
    return this.actualShippingTax;
  }


  public void setActualTax(double actualTax)
  {
    if(valueChanged(new Double(this.actualTax), new Double(actualTax)))
    {
      setChanged(true);
    }
    this.actualTax = actualTax;
  }


  public double getActualTax()
  {
    return this.actualTax;
  }



  public void setCompanyId(String a_value) {
    if (valueChanged(companyId, a_value)) {
      setChanged(true);
    }
    companyId = trim(a_value);
  }
  public String getCompanyId() {
    return companyId;
  }


  public void setOriginId(String a_value) {
    if (valueChanged(originId, a_value)) {
      setChanged(true);
    }
    originId = trim(a_value);
  }
  public String getOriginId() {
    return originId;
  }


  public void setOccasion(String a_value) {
    if (valueChanged(occasion, a_value)) {
      setChanged(true);
    }
    occasion = trim(a_value);
  }
  public String getOccasion() {
    return occasion;
  }

  public void setOccasionDescription(String a_value) {
    if (valueChanged(occasionDescription, a_value)) {
      setChanged(true);
    }
    occasionDescription = trim(a_value);
  }
  public String getOccasionDescription() {
    return occasionDescription;
  }


  public void setCanChangeOrderSource(String a_value) {
    if (valueChanged(canChangeOrderSource, a_value)) {setChanged(true);}
    canChangeOrderSource = trim(a_value);
  }
  public String getCanChangeOrderSource() {return canChangeOrderSource;}


  public void setMembershipRequired(String a_value) {
    if (valueChanged(membershipRequired, a_value)) {setChanged(true);}
    membershipRequired = trim(a_value);
  }
  public String getMembershipRequired() {return membershipRequired;}


  public void setSourceCodeDescription(String a_value) {
    if (valueChanged(sourceCodeDescription, a_value)) {setChanged(true);}
    sourceCodeDescription = trim(a_value);
  }
  public String getSourceCodeDescription() {return sourceCodeDescription;}


  public void setSourceType(String a_value) {
    if (valueChanged(sourceType, a_value)) {setChanged(true);}
    sourceType = trim(a_value);
  }
  public String getSourceType() {return sourceType;}


  public void setOccasionCategoryIndex(String a_value) {
    if (valueChanged(occasionCategoryIndex, a_value)) {setChanged(true);}
    occasionCategoryIndex = trim(a_value);
  }
  public String getOccasionCategoryIndex() {return occasionCategoryIndex;}


  public void setSizeIndicator(String a_value) {
    if (valueChanged(sizeIndicator, a_value)) {setChanged(true);}
    sizeIndicator = trim(a_value);
  }
  public String getSizeIndicator() {return sizeIndicator;}


  public void setMasterOrderNumber(String a_value) {
    if (valueChanged(masterOrderNumber, a_value)) {setChanged(true);}
    masterOrderNumber = trim(a_value);
  }
  public String getMasterOrderNumber() {return masterOrderNumber;}


  public void setCustomerFirstName(String a_value) {
    if (valueChanged(customerFirstName, a_value)) {setChanged(true);}
    customerFirstName = trim(a_value);
  }
  public String getCustomerFirstName() {return customerFirstName;}


  public void setCustomerLastName(String a_value) {
    if (valueChanged(customerLastName, a_value)) {setChanged(true);}
    customerLastName = trim(a_value);
  }
  public String getCustomerLastName() {return customerLastName;}


  public void setCustomerCity(String a_value) {
    if (valueChanged(customerCity, a_value)) {setChanged(true);}
    customerCity = trim(a_value);
  }
  public String getCustomerCity() {return customerCity;}


  public void setCustomerState(String a_value) {
    if (valueChanged(customerState, a_value)) {setChanged(true);}
    customerState = trim(a_value);
  }
  public String getCustomerState() {return customerState;}


  public void setCustomerZipCode(String a_value) {
    if (valueChanged(customerZipCode, a_value)) {setChanged(true);}
    customerZipCode = trim(a_value);
  }
  public String getCustomerZipCode() {return customerZipCode;}


  public void setCustomerCountry(String a_value) {
    if (valueChanged(customerCountry, a_value)) {setChanged(true);}
    customerCountry = trim(a_value);
  }
  public String getCustomerCountry() {return customerCountry;}


  public void setCustomerId(String a_value) {
    if (valueChanged(customerId, a_value)) {setChanged(true);}
    customerId = trim(a_value);
  }
  public String getCustomerId() {return customerId;}


  public void setShipMethodCarrier(String a_value) {
    if (valueChanged(shipMethodCarrier, a_value)) {setChanged(true);}
    shipMethodCarrier = trim(a_value);
  }
  public String getShipMethodCarrier() {return shipMethodCarrier;}


  public void setShipMethodFlorist(String a_value) {
    if (valueChanged(shipMethodFlorist, a_value)) {setChanged(true);}
    shipMethodFlorist = trim(a_value);
  }
  public String getShipMethodFlorist() {return shipMethodFlorist;}



  public void setChangesToZip(boolean a_value) {
    if (valueChanged(changesToZip, a_value)) {setChanged(true);}
    changesToZip = a_value;
  }
  public boolean getChangesToZip() {return changesToZip;}


  public void setChangesToCountry(boolean a_value) {
    if (valueChanged(changesToCountry, a_value)) {setChanged(true);}
    changesToCountry = a_value;
  }
  public boolean getChangesToCountry() {return changesToCountry;}


  public void setChangesToDeliveryDate(boolean a_value) {
    if (valueChanged(changesToDeliveryDate, a_value)) {setChanged(true);}
    changesToDeliveryDate = a_value;
  }
  public boolean getChangesToDeliveryDate() {return changesToDeliveryDate;}


  public void setChangesToSourceCode(boolean a_value) {
    if (valueChanged(changesToSourceCode, a_value)) {setChanged(true);}
    changesToSourceCode = a_value;
  }
  public boolean getChangesToSourceCode() {return changesToSourceCode;}


  public void setChangesToShipMethod(boolean a_value) {
    if (valueChanged(changesToShipMethod, a_value)) {setChanged(true);}
    changesToShipMethod = a_value;
  }
  public boolean getChangesToShipMethod() {return changesToShipMethod;}


  public void setChangedToSundayDelivery(boolean a_value) {
    if (valueChanged(changedToSundayDelivery, a_value)) {setChanged(true);}
    changedToSundayDelivery = a_value;
  }
  public boolean getChangedToSundayDelivery() {return changedToSundayDelivery;}


  public void setColor1(String a_value) {
    if (valueChanged(color1, a_value)) {
      setChanged(true);
    }
    color1 = trim(a_value);
  }
  public String getColor1() {
    return color1;
  }


  public void setColor2(String a_value) {
    if (valueChanged(color2, a_value)) {
      setChanged(true);
    }
    color2 = trim(a_value);
  }
  public String getColor2() {
    return color2;
  }


  public void setEODDeliveryIndicator(String a_value) {
    if (valueChanged(EODDeliveryIndicator, a_value)) {
      setChanged(true);
    }
    EODDeliveryIndicator = trim(a_value);
  }
  public String getEODDeliveryIndicator() {
    return EODDeliveryIndicator;
  }


  public void setMilesPoints(double a_value) {
    if (valueChanged(milesPoints, a_value)) {
      setChanged(true);
    }
    milesPoints = a_value;
  }
  public double getMilesPoints() {
    return milesPoints;
  }


  public void setQuantity(long a_value) {
    if (valueChanged(quantity, a_value)) {
      setChanged(true);
    }
    quantity = a_value;
  }
  public long getQuantity() {
    return quantity;
  }


  public void setReleaseInfoIndicator(String a_value) {
    if (valueChanged(releaseInfoIndicator, a_value)) {
      setChanged(true);
    }
    releaseInfoIndicator = trim(a_value);
  }
  public String getReleaseInfoIndicator() {
    return releaseInfoIndicator;
  }


  public void setSecondChoiceProduct(String a_value) {
    if (valueChanged(secondChoiceProduct, a_value)) {
      setChanged(true);
    }
    secondChoiceProduct = trim(a_value);
  }
  public String getSecondChoiceProduct() {
    return secondChoiceProduct;
  }


  public void setSubcode(String a_value) {
    if (valueChanged(subcode, a_value)) {
      setChanged(true);
    }
    subcode = trim(a_value);
  }
  public String getSubcode() {
    return subcode;
  }


  public void setSubstitutionIndicator(String a_value) {
    if (valueChanged(substitutionIndicator, a_value)) {
      setChanged(true);
    }
    substitutionIndicator = trim(a_value);
  }
  public String getSubstitutionIndicator() {
    return substitutionIndicator;
  }



  public void setExceptionCode(String a_value) {
    if (valueChanged(exceptionCode, a_value)) {
      setChanged(true);
    }
    exceptionCode = trim(a_value);
  }
  public String getExceptionCode() {
    return exceptionCode;
  }



  public void setExceptionStartDate(Calendar a_value) {
    if (valueChanged(exceptionStartDate, a_value)) {
      setChanged(true);
    }
    exceptionStartDate = a_value;
  }
  public Calendar getExceptionStartDate() {
    return exceptionStartDate;
  }


  public void setExceptionEndDate(Calendar a_value) {
    if (valueChanged(exceptionEndDate, a_value)) {
      setChanged(true);
    }
    exceptionEndDate = a_value;
  }
  public Calendar getExceptionEndDate() {
    return exceptionEndDate;
  }



  private String trim(String str) {
    return (str != null)?str.trim():str;
  }

  /*
   * Arrrgh - using brute force to convert to XML since front end expects underscores.
   * Originally was hoping to use iframes on submit so wouldn't have to return variables
   * if form validation fails.  However iframe can't POST, so # chars is limited
   * and this form is massive.  So, we resorted to this until we can come up
   * with a more elegant solution...
   */
  public Document toXMLforFrontEnd() throws Exception {
    Document docXML;
    Element topElement;

    // Generate top node
    docXML = (Document) DOMUtil.getDefaultDocument();
    topElement = docXML.createElement("PD_ORDER_PRODUCTS");
    docXML.appendChild(topElement);

    Element detailElement = docXML.createElement("PD_ORDER_PRODUCT");
    topElement.appendChild(detailElement);


    addXMLNodeToElement(docXML, detailElement, "actual_add_on_amount",					new Double(getActualAddOnAmount()).toString());
    addXMLNodeToElement(docXML, detailElement, "actual_discount_amount",				new Double(getActualDiscountAmount()).toString());
    addXMLNodeToElement(docXML, detailElement, "actual_product_amount",					new Double(getActualProductAmount()).toString());
    addXMLNodeToElement(docXML, detailElement, "actual_service_fee",						new Double(getActualServiceFee()).toString());
    addXMLNodeToElement(docXML, detailElement, "actual_shipping_fee",						new Double(getActualShippingFee()).toString());
    addXMLNodeToElement(docXML, detailElement, "actual_shipping_tax",						new Double(getActualShippingTax()).toString());
    addXMLNodeToElement(docXML, detailElement, "actual_tax",										new Double(getActualTax()).toString());
    addXMLNodeToElement(docXML, detailElement, "alt_contact_email",							getAltContactEmail());
    addXMLNodeToElement(docXML, detailElement, "alt_contact_extension",					getAltContactExtension());
    addXMLNodeToElement(docXML, detailElement, "alt_contact_first_name",				getAltContactFirstName());
    addXMLNodeToElement(docXML, detailElement, "alt_contact_info_id",						getAltContactInfoId());
    addXMLNodeToElement(docXML, detailElement, "alt_contact_last_name",					getAltContactLastName());
    addXMLNodeToElement(docXML, detailElement, "alt_contact_phone_number", 			getAltContactPhoneNumber());
    addXMLNodeToElement(docXML, detailElement, "can_change_order_source", 			getCanChangeOrderSource());
    addXMLNodeToElement(docXML, detailElement, "card_message", 									getCardMessage());
    addXMLNodeToElement(docXML, detailElement, "card_signature", 								getCardSignature());
    addXMLNodeToElement(docXML, detailElement, "color_1", 											getColor1());
    addXMLNodeToElement(docXML, detailElement, "color_2", 											getColor2());
    addXMLNodeToElement(docXML, detailElement, "company_id", 										getCompanyId());
    addXMLNodeToElement(docXML, detailElement, "customer_city", 								getCustomerCity());
    addXMLNodeToElement(docXML, detailElement, "customer_country", 							getCustomerCountry());
    addXMLNodeToElement(docXML, detailElement, "customer_first_name", 					getCustomerFirstName());
    addXMLNodeToElement(docXML, detailElement, "customer_id", 									getCustomerId());
    addXMLNodeToElement(docXML, detailElement, "customer_last_name", 						getCustomerLastName());
    addXMLNodeToElement(docXML, detailElement, "customer_state", 								getCustomerState());
    addXMLNodeToElement(docXML, detailElement, "customer_zip_code", 						getCustomerZipCode());
    addXMLNodeToElement(docXML, detailElement, "eod_delivery_indicator", 				getEODDeliveryIndicator());
    addXMLNodeToElement(docXML, detailElement, "exception_code", 				        getExceptionCode());
    addXMLNodeToElement(docXML, detailElement, "florist_id", 										getFloristId());
    addXMLNodeToElement(docXML, detailElement, "master_order_number", 					getMasterOrderNumber());
    addXMLNodeToElement(docXML, detailElement, "membership_first_name",					getMembershipFirstName());
    addXMLNodeToElement(docXML, detailElement, "membership_last_name", 					getMembershipLastName());
    addXMLNodeToElement(docXML, detailElement, "membership_number", 						getMembershipNumber());
    addXMLNodeToElement(docXML, detailElement, "membership_required", 					getMembershipRequired());
    addXMLNodeToElement(docXML, detailElement, "miles_points", 									new Double(getMilesPoints()).toString());
    addXMLNodeToElement(docXML, detailElement, "occasion", 											getOccasion());
    addXMLNodeToElement(docXML, detailElement, "occasion_category_index", 			getOccasionCategoryIndex());
    addXMLNodeToElement(docXML, detailElement, "occasion_description", 					getOccasionDescription());
    addXMLNodeToElement(docXML, detailElement, "order_detail_id", 							getOrderDetailId());
    addXMLNodeToElement(docXML, detailElement, "order_guid", 										getOrderGuid());
    addXMLNodeToElement(docXML, detailElement, "origin_id", 										getOriginId());
    addXMLNodeToElement(docXML, detailElement, "product_amount", 								getProductAmount());
    addXMLNodeToElement(docXML, detailElement, "product_id", 										getProductId());
    addXMLNodeToElement(docXML, detailElement, "quantity", 											new Long(getQuantity()).toString());
    addXMLNodeToElement(docXML, detailElement, "recipient_address_1",						getRecipientAddress1());
    addXMLNodeToElement(docXML, detailElement, "recipient_address_2", 					getRecipientAddress2());
    addXMLNodeToElement(docXML, detailElement, "recipient_address_type", 				getRecipientAddressType());
    addXMLNodeToElement(docXML, detailElement, "recipient_business_name", 			getRecipientBusinessName());
    addXMLNodeToElement(docXML, detailElement, "recipient_business_info", 			getRecipientBusinessInfo());
    addXMLNodeToElement(docXML, detailElement, "recipient_city", 								getRecipientCity());
    addXMLNodeToElement(docXML, detailElement, "recipient_country", 						getRecipientCountry());
    addXMLNodeToElement(docXML, detailElement, "recipient_extension", 					getRecipientExtension());
    addXMLNodeToElement(docXML, detailElement, "recipient_first_name", 					getRecipientFirstName());
    addXMLNodeToElement(docXML, detailElement, "recipient_id", 									getRecipientId());
    addXMLNodeToElement(docXML, detailElement, "recipient_last_name", 					getRecipientLastName());
    addXMLNodeToElement(docXML, detailElement, "recipient_phone_number", 				getRecipientPhoneNumber());
    addXMLNodeToElement(docXML, detailElement, "recipient_state", 							getRecipientState());
    addXMLNodeToElement(docXML, detailElement, "recipient_zip_code", 						getRecipientZipCode());
    addXMLNodeToElement(docXML, detailElement, "release_info_indicator", 				getReleaseInfoIndicator());
    addXMLNodeToElement(docXML, detailElement, "ship_method", 									getShipMethod());
    addXMLNodeToElement(docXML, detailElement, "ship_method_carrier", 					getShipMethodCarrier());
    addXMLNodeToElement(docXML, detailElement, "ship_method_desc", 							getShipMethodDesc());
    addXMLNodeToElement(docXML, detailElement, "ship_method_florist", 					getShipMethodFlorist());
    addXMLNodeToElement(docXML, detailElement, "shipping_fee", 									getShippingFee());
    addXMLNodeToElement(docXML, detailElement, "second_choice_product", 				getSecondChoiceProduct());
    addXMLNodeToElement(docXML, detailElement, "size_indicator", 								getSizeIndicator());
    addXMLNodeToElement(docXML, detailElement, "source_code", 									getSourceCode());
    addXMLNodeToElement(docXML, detailElement, "source_code_description", 			getSourceCodeDescription());
    addXMLNodeToElement(docXML, detailElement, "source_type", 									getSourceType());
    addXMLNodeToElement(docXML, detailElement, "special_instructions", 					getSpecialInstructions());
    addXMLNodeToElement(docXML, detailElement, "subcode", 											getSubcode());
    addXMLNodeToElement(docXML, detailElement, "substitution_indicator", 				getSubstitutionIndicator());
    addXMLNodeToElement(docXML, detailElement, "updated_by", 										getUpdatedBy());
    addXMLNodeToElement(docXML, detailElement, "vendor_flag", 									getVendorFlag());


    SimpleDateFormat sdfWithDay = new SimpleDateFormat(DATE_FORMAT_WITH_DAY);
    SimpleDateFormat sdfNoDay = new SimpleDateFormat(DATE_FORMAT);
    String sdate;

    //updated_on
    if (getUpdatedOn() != null) {
      Date ldate = getUpdatedOn().getTime();
      sdate = sdfNoDay.format(ldate).toString();
    } else {
      sdate = null;
    }
    addXMLNodeToElement(docXML, detailElement, "updated_on", sdate);


    //delivery_date
    if (getDeliveryDate() != null) {
      Date ldate = getDeliveryDate().getTime();
      sdate = sdfNoDay.format(ldate).toString();
    } else {
      sdate = null;
    }
    addXMLNodeToElement(docXML, detailElement, "delivery_date", sdate);


    //eod_delivery_date
    if (getEODDeliveryDate() != null) {
      Date ldate = getEODDeliveryDate().getTime();
      sdate = sdfWithDay.format(ldate).toString();
    } else {
      sdate = null;
    }
    addXMLNodeToElement(docXML, detailElement, "eod_delivery_date", sdate);


    //delivery_date_range_end
    if (getDeliveryDateRangeEnd() != null) {
      Date ldate = getDeliveryDateRangeEnd().getTime();
      sdate = sdfNoDay.format(ldate).toString();
    } else {
      sdate = null;
    }
    addXMLNodeToElement(docXML, detailElement, "delivery_date_range_end", sdate);


    //ship_date
    if (getShipDate() != null) {
      Date ldate = getShipDate().getTime();
      sdate = sdfNoDay.format(ldate).toString();
    } else {
      sdate = null;
    }
    addXMLNodeToElement(docXML, detailElement, "ship_date", sdate);


    //exception start date
    if (getExceptionStartDate() != null) {
      Date ldate = getExceptionStartDate().getTime();
      sdate = sdfNoDay.format(ldate).toString();
    } else {
      sdate = null;
    }
    addXMLNodeToElement(docXML, detailElement, "exception_start_date", sdate);


    //exception end date
    if (getExceptionEndDate() != null) {
      Date ldate = getExceptionEndDate().getTime();
      sdate = sdfNoDay.format(ldate).toString();
    } else {
      sdate = null;
    }
    addXMLNodeToElement(docXML, detailElement, "exception_end_date", sdate);



    return docXML;
  }

  // Internal helper method to add XML node to element
  //
  private void addXMLNodeToElement(Document a_doc, Element a_element, String a_name, String a_value) {
    Element node = a_doc.createElement(a_name);
    a_element.appendChild(node);
    if (a_value != null) {node.appendChild(a_doc.createTextNode(a_value));}
  }


  /**
    * This method uses the Reflection API to generate an XML string that will be
    * passed back to the calling module.
    * The XML string will contain all the fields within this VO, including the
    * variables as well as (a collection of) ValueObjects.
    *
    * @param  None
    * @return XML string
   **/
  public String toXML()
  {
    StringBuffer sb = new StringBuffer();
    try
    {
      sb.append("<DeliveryInfoVO>");
      Field[] fields = this.getClass().getDeclaredFields();

      for (int i = 0; i < fields.length; i++)
      {
        //if the field retrieved was a list of VO
        if(fields[i].getType().equals(Class.forName("java.util.List")))
        {
          List list = (List)fields[i].get(this);
          if(list != null)
          {
            for (int j = 0; j < list.size(); j++)
            {
              XMLInterface xmlInt = (XMLInterface)list.get(j);
              String sXmlVO = xmlInt.toXML();
              sb.append(sXmlVO);
            }
          }
        }
        else
        {
          //if the field retrieved was a VO
          if (fields[i].getType().toString().matches("(?i).*vo"))
          {
            XMLInterface xmlInt = (XMLInterface)fields[i].get(this);
            String sXmlVO = xmlInt.toXML();
            sb.append(sXmlVO);
          }
          //if the field retrieved was a Calendar object
          else if (fields[i].getType().toString().matches("(?i).*calendar"))
          {
            Date date;
            String fDate = null;
            if (fields[i].get(this) != null)
            {
              date = (((GregorianCalendar)fields[i].get(this)).getTime());
              SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss a zzz");
              fDate = sdf.format(date).toString();
            }
            sb.append("<" + fields[i].getName() + ">");
            sb.append(fDate);
            sb.append("</" + fields[i].getName() + ">");

          }
          else
          {
            sb.append("<" + fields[i].getName() + ">");
            sb.append(fields[i].get(this));
            sb.append("</" + fields[i].getName() + ">");
          }
        }
      }
      sb.append("</DeliveryInfoVO>");
    }

    catch (Exception e)
    {
      e.printStackTrace();
    }

    return sb.toString();
  }


  /**
    * This method uses the Reflection API to generate an XML string that will be
    * passed back to the calling module.
    * The XML string will contain all the fields within this VO, including the
    * variables as well as (a collection of) ValueObjects.
    *
    * @param  None
    * @return XML string
   **/
  public String toXML(int count)
  {
    StringBuffer sb = new StringBuffer();
    try
    {
      if (count == 0)
      {
        sb.append("<DeliveryInfoVO>");
      }
      else
      {
        sb.append("<DeliveryInfoVO num=" + '"' + count + '"' + ">");
      }
      Field[] fields = this.getClass().getDeclaredFields();

      for (int i = 0; i < fields.length; i++)
      {
        //if the field retrieved was a list of VO
        if(fields[i].getType().equals(Class.forName("java.util.List")))
        {
          List list = (List)fields[i].get(this);
          if(list != null)
          {
            for (int j = 0; j < list.size(); j++)
            {
              XMLInterface xmlInt = (XMLInterface)list.get(j);
              int k = j + 1;
              String sXmlVO = xmlInt.toXML(k);
              sb.append(sXmlVO);
            }
          }
        }
        else
        {
          //if the field retrieved was a VO
          if (fields[i].getType().toString().matches("(?i).*vo"))
          {
            XMLInterface xmlInt = (XMLInterface)fields[i].get(this);
            int k = i + 1;
            String sXmlVO = xmlInt.toXML(k);
            sb.append(sXmlVO);
          }
          //if the field retrieved was a Calendar object
          else if (fields[i].getType().toString().matches("(?i).*calendar"))
          {
            Date date;
            String fDate = null;
            if (fields[i].get(this) != null)
            {
              date = (((GregorianCalendar)fields[i].get(this)).getTime());
              SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss a zzz");
              fDate = sdf.format(date).toString();
  	          sb.append("<" + fields[i].getName() + ">");
              sb.append(fDate);
              sb.append("</" + fields[i].getName() + ">");
            }
            else
            {
							sb.append("<" + fields[i].getName() + "/>");
            }

          }
          else
          {
						if (fields[i].get(this) == null)
						{
							sb.append("<" + fields[i].getName() + "/>");
						}
						else
						{
							sb.append("<" + fields[i].getName() + ">");
							sb.append(fields[i].get(this));
							sb.append("</" + fields[i].getName() + ">");
						}
          }
        }
      }
      sb.append("</DeliveryInfoVO>");
    }

    catch (Exception e)
    {
      e.printStackTrace();
    }

    return sb.toString();
  }

    public String getMessageId()
    {
        return messageId;
    }

    public void setMessageId(String messageId)
    {
        this.messageId = messageId;
    }

    public String getMessageOrderNumber()
    {
        return messageOrderNumber;
    }

    public void setMessageOrderNumber(String messageOrderNumber)
    {
        this.messageOrderNumber = messageOrderNumber;
    }

    public String getFtdmIndicator()
    {
        return ftdmIndicator;
    }

    public void setFtdmIndicator(String ftdmIndicator)
    {
        this.ftdmIndicator = ftdmIndicator;
    }


  public void setShippingFee(String shippingFee)
  {
    this.shippingFee = shippingFee;
  }


  public String getShippingFee()
  {
    return shippingFee;
  }


  public void setShipMethodDesc(String shipMethodDesc)
  {
    this.shipMethodDesc = shipMethodDesc;
  }


  public String getShipMethodDesc()
  {
    return shipMethodDesc;
  }

    public void setPersonalGreetingId(String personalGreetingId) {
        this.personalGreetingId = personalGreetingId;
    }

    public String getPersonalGreetingId() {
        return personalGreetingId;
    }

    public void setNoTaxFlag(String noTaxFlag) {
        this.noTaxFlag = noTaxFlag;
    }

    public String getNoTaxFlag() {
        return noTaxFlag;
    }

    public List<OrderExtensionsVO> getOrderExtensions()
    {
        return orderExtensions;
    }

    public void setOrderExtensions(List<OrderExtensionsVO> orderExtensions)
    {
        this.orderExtensions = orderExtensions;
    }

    public void setBuyerSignedInFlag(String buyerSignedInFlag)
    {
      this.buyerSignedInFlag = buyerSignedInFlag;
    }
    
    public String getBuyerSignedInFlag()
    {
      return buyerSignedInFlag;
    }

	public String getOriginalOrderHasSDU() {
		return originalOrderHasSDU;
	}

	public void setOriginalOrderHasSDU(String originalOrderHasSDU) {
		this.originalOrderHasSDU = originalOrderHasSDU;
	}

	public String getPcFlag() {
		return pcFlag;
	}

	public void setPcFlag(String pcFlag) {
		this.pcFlag = pcFlag;
	}

	public String getPcMembershipId() {
		return pcMembershipId;
	}

	public void setPcMembershipId(String pcMembershipId) {
		this.pcMembershipId = pcMembershipId;
	}

	public String getPcGroupId() {
		return pcGroupId;
	}

	public void setPcGroupId(String pcGroupId) {
		this.pcGroupId = pcGroupId;
	}

	public void setBearAddonId(String bearAddonId) {
		this.bearAddonId = bearAddonId;
	}

	public String getBearAddonId() {
		return bearAddonId;
	}

	public void setChocolateAddonId(String chocolateAddonId) {
		this.chocolateAddonId = chocolateAddonId;
	}

	public String getChocolateAddonId() {
		return chocolateAddonId;
	}
	
	
}
