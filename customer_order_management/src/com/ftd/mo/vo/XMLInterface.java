package com.ftd.mo.vo;

public interface XMLInterface
{

  public String toXML();

  public String toXML(int count);
}