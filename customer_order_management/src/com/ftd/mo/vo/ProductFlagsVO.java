package com.ftd.mo.vo;

public class ProductFlagsVO
{

  private String  DisplayCodifiedFloristHasTwoDayDeliveryOnly;
  private String  DisplayFloral;
          String  DisplayNoCodifiedFloristHasCommonCarrier;
  private String  DisplayNoFloristHasCommonCarrier;
          String  DisplayNoProduct;
  private String  DisplayProductUnavailable;
  private String  DisplaySpecialtyGift;
  private String  DisplayPersonalizedProduct;
  private String  DropShipAllowed;
  private String  setDisplayNoProduct;
  private String  SpecialFeeFlag;
  private boolean Upsell;
  private boolean upsellExists;
  private String  upsellSkip;


  public ProductFlagsVO()
  {
  }

  public String getDisplayProductUnavailable()
  {
    return DisplayProductUnavailable;
  }

  public void setDisplayProductUnavailable(String newDisplayProductUnavailable)
  {
    DisplayProductUnavailable = newDisplayProductUnavailable;
  }

  public String getDisplayNoProduct()
  {
    return DisplayNoProduct;
  }

  public void setDisplayNoProduct(String newDisplayNoProduct)
  {
    DisplayNoProduct = newDisplayNoProduct;
  }

  public String getDisplayCodifiedFloristHasTwoDayDeliveryOnly()
  {
    return DisplayCodifiedFloristHasTwoDayDeliveryOnly;
  }

  public void setDisplayCodifiedFloristHasTwoDayDeliveryOnly(String newDisplayCodifiedFloristHasTwoDayDeliveryOnly)
  {
    DisplayCodifiedFloristHasTwoDayDeliveryOnly = newDisplayCodifiedFloristHasTwoDayDeliveryOnly;
  }

  public String getDisplayNoCodifiedFloristHasCommonCarrier()
  {
    return DisplayNoCodifiedFloristHasCommonCarrier;
  }

  public void setDisplayNoCodifiedFloristHasCommonCarrier(String newDisplayNoCodifiedFloristHasCommonCarrier)
  {
    DisplayNoCodifiedFloristHasCommonCarrier = newDisplayNoCodifiedFloristHasCommonCarrier;
  }

  public String getSetDisplayNoProduct()
  {
    return setDisplayNoProduct;
  }

  public void setSetDisplayNoProduct(String newSetDisplayNoProduct)
  {
    setDisplayNoProduct = newSetDisplayNoProduct;
  }

  public String getDisplayNoFloristHasCommonCarrier()
  {
    return DisplayNoFloristHasCommonCarrier;
  }

  public void setDisplayNoFloristHasCommonCarrier(String newDisplayNoFloristHasCommonCarrier)
  {
    DisplayNoFloristHasCommonCarrier = newDisplayNoFloristHasCommonCarrier;
  }

  public String getSpecialFeeFlag()
  {
    return SpecialFeeFlag;
  }

  public void setSpecialFeeFlag(String newSpecialFeeFlag)
  {
    SpecialFeeFlag = newSpecialFeeFlag;
  }

  public boolean isUpsell()
  {
    return Upsell;
  }

  public String getUpsellSkip()
  {
    return upsellSkip;
  }

  public void setUpsellSkip(String newUpsellSkip)
  {
    upsellSkip = newUpsellSkip;
  }

  public boolean isUpsellExists()
  {
    return upsellExists;
  }

  public void setUpsellExists(boolean newUpsellExists)
  {
    upsellExists = newUpsellExists;
  }

  public String getDisplayFloral()
  {
    return DisplayFloral;
  }

  public void setDisplayFloral(String newDisplayFloral)
  {
    DisplayFloral = newDisplayFloral;
  }

  public String getDisplaySpecialtyGift()
  {
    return DisplaySpecialtyGift;
  }

  public void setDisplaySpecialtyGift(String newDisplaySpecialtyGift)
  {
    DisplaySpecialtyGift = newDisplaySpecialtyGift;
  }

  public String getDropShipAllowed()
  {
    return DropShipAllowed;
  }

  public void setDropShipAllowed(String newDropShipAllowed)
  {
    this.DropShipAllowed = newDropShipAllowed;
  }


    public void setDisplayPersonalizedProduct(String displayPersonalizedProduct) {
        this.DisplayPersonalizedProduct = displayPersonalizedProduct;
    }

    public String getDisplayPersonalizedProduct() {
        return DisplayPersonalizedProduct;
    }
}
