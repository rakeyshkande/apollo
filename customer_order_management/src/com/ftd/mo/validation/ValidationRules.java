package com.ftd.mo.validation;

/**
 * ValidationRules define the keys used to denote any failures during validation.  The ValidationRules
 * will be the key of the map if Validator.getValidationResults is called after validation.
 * If Validator.getValidationResultsAsList is called after validation, the ValidationRules will be wrapped, along with
 * additional validation information, in the ValidationInfo object contained with in the List.
 *
 * @author Mark Moon, Software Architects Inc.
 * @version $Revision: 1.2 $
 */
public class ValidationRules {

  /*
   * Florist Delivery Date Validation Rules
   */
  public static final String SUNDAY_DELIVERY_NOT_ALLOWED_FOR_EXOTIC = "SUNDAY_DELIVERY_NOT_ALLOWED_FOR_EXOTIC";
  public static final String SAME_DAY_EXOTIC_UNAVAILABLE = "SAME_DAY_EXOTIC_UNAVAILABLE";
  public static final String SAME_DAY_DELIVERY_FOR_INTERNATIONAL_PRODUCTS = "SAME_DAY_DELIVERY_FOR_INTERNATIONAL_PRODUCTS";
  public static final String PRODUCT_DELIVERY_DATE_RESTRICTIONS = "PRODUCT_DELIVERY_DATE_RESTRICTIONS";
  public static final String ZIP_IN_GNADD = "ZIP_IN_GNADD";

  /*
   * Vendor Delivery Date Validation Rules
   */
  public static final String PRODUCT_STATE_AGRICULTURAL_RESTRICATIONS_FOR_DELIVERY_DAY_SELECTED  = "PRODUCT_STATE_AGRICULTURAL_RESTRICATIONS_FOR_DELIVERY_DAY_SELECTED";
  public static final String PRODUCT_STATE_AGRICULTURAL_RESTRICATIONS_FOR_ENTIRE_WEEK = "PRODUCT_STATE_AGRICULTURAL_RESTRICATIONS_FOR_PARTIAL_WEEK";
  public static final String TWO_DAY_SHIPPING_REQUIRED_FOR_AK_HI = "TWO_DAY_SHIPPING_REQUIRED_FOR_AK_HI";
  public static final String DELIVERY_SHIP_METHOD_NOT_ALLOWED = "DELIVERY_SHIP_METHOD_NOT_ALLOWED";

  /*
   * Florist and Vendor Delivery Date Validation Rules (common)
   */
  public static final String SAME_DAY_CUTOFF_EXPIRED = "SAME_DAY_CUTOFF_EXPIRED";
  public static final String REQUESTED_DELIVERY_DATE_HAS_PASSED = "REQUESTED_DELIVERY_DATE_HAS_PASSED";
  public static final String DELIVERY_OPTION_NOT_ALLOWED = "DELIVERY_OPTION_NOT_ALLOWED";


  /*
   * Florist Delivered Validation Rules
   */
  public static final String NO_LIVE_MERCURY = "NO_LIVE_MERCURY";
  public static final String INVALID_FLORIST_NUMBER = "INVALID_FLORIST_NUMBER";
  public static final String FLORIST_ACTIVE = "FLORIST_ACTIVE";
  public static final String FLORIST_SUSPENDED = "FLORIST_SUSPENDED";
  public static final String FLORIST_OK_FOR_SUNDAY = "FLORIST_OK_FOR_SUNDAY";
  public static final String OK_FOR_ZIP = "OK_FOR_ZIP";
  public static final String OK_FOR_CITY = "OK_FOR_CITY";
  public static final String OK_FOR_ZIP_OR_CITY = "OK_FOR_ZIP_OR_CITY";
  public static final String OK_FOR_CODIFICATION = "OK_FOR_CODIFICATION";
  public static final String FLORIST_DELIVERY_NOT_AVAILABLE = "FLORIST_DELIVERY_NOT_AVAILABLE";
  public static final String OTHER_FLORISTS_ACTIVE = "OTHER_FLORISTS_ACTIVE";
  public static final String OTHER_FLORISTS_OK_FOR_SUNDAY = "OTHER_FLORISTS_OK_FOR_SUNDAY";
  public static final String OTHER_FLORISTS_OK_FOR_CODIFICATION = "OTHER_FLORISTS_OK_FOR_CODIFICATION";
  public static final String OTHER_FLORISTS_OK_FOR_CODIFICATION_AND_SUN = "OTHER_FLORISTS_OK_FOR_CODIFICATION_AND_SUN";
  public static final String OTHER_FLORISTS_OK_FOR_ZIP_OR_CITY = "OTHER_FLORISTS_OK_FOR_ZIP_OR_CITY";
  public static final String PRODUCT_IS_CODIFIED = "PRODUCT_IS_CODIFIED";
  //public static final String OTHER_FLORIST_CAN_DELIVER = "OTHER_FLORIST_CAN_DELIVER";
  //public static final String OTHER_FLORIST_CAN_DELIVER_ON_SUNDAY = "OTHER_FLORIST_CAN_DELIVER_ON_SUNDAY";

  /*
   * Vendor Delivered Validation Rules
   */
  public static final String DROP_SHIP_PRODUCT_DELIVERY_RESTRICTIONS = "DROP_SHIP_PRODUCT_DELIVERY_RESTRICTIONS";
  public static final String ORDER_ALREADY_PRINTED = "ORDER_ALREADY_PRINTED";
  public static final String DROP_SHIP_NOT_AVAILABLE = "DROP_SHIP_NOT_AVAILABLE";
  public static final String RECIP_PO_NOT_ALLOWED_ADDRESS_ONE = "PO or APO box is not allowed in address line one";
  public static final String RECIP_PO_NOT_ALLOWED_ADDRESS_TWO = "PO or APO box is not allowed in address line two";

  /*
   * Florist and Vendor Delivered Validation Rules (common)
   */
  public static final String PRE_VALIDATION_ERROR = "PRE_VALIDATION_ERROR";
  public static final String PRODUCT_UNAVAILABLE = "PRODUCT_UNAVAILABLE";
}