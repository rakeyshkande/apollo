package com.ftd.mo.validation;

/**
 * Defines all possible validation levels.
 *
 * @author Mark Moon, Software Architects Inc.
 * @version $Revision: 1.2 $
 */
public class ValidationLevels {

  public static final Integer ERROR = new Integer(0);
  public static final Integer WARNING = new Integer(1);
  public static final Integer INFO = new Integer(2);

}