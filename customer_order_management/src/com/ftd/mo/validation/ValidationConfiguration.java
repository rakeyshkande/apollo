package com.ftd.mo.validation;

/**
 * ValidationConfiguration defines all configuration settings for Validators.
 *
 * Defaults:
 *    stopOnFirstFailure = false
 *    failureThreshold = Integer.MAX_VALUE;
 *    validationLevel = ValidationLevels.ERROR
 *
 * @author Mark Moon, Software Architects Inc.
 * @version $Revision: 1.2 $
 */
public class ValidationConfiguration {

  private boolean stopOnFirstFailure;
  private int failureThreshold;
  private Integer validationLevel;


  /**
   * Default constructor.
   */
  public ValidationConfiguration() {
    super();
    failureThreshold = Integer.MAX_VALUE;
    validationLevel = ValidationLevels.ERROR;
  }

  /**
   * Sets the validation level.  Only failures of the specified level will be generated.
   * @param validationLevel
   */
  public void setValidationLevel(Integer validationLevel) {
    this.validationLevel = validationLevel;
  }

  /**
   * Returns the validation level to be used during validation.
   * @return int validation level
   */
  public Integer getValidationLevel() {
    return validationLevel;
  }

  /**
   * Sets how the validator should respond to errors.  If true, the validator will
   * abort upon the first error, if false the validator will return all validation errors.
   * @param stopOnFirstFailure true if the validator should abort upon the first error or
   *   false if the validator should return all validation errors.
   */
  public void setStopOnFirstFailure(boolean stopOnFirstFailure) {
    this.stopOnFirstFailure = stopOnFirstFailure;
  }

  /**
   * Returns how the validator will respond to errors.  If true, the validator will
   * abort upon the first error, if false the validator will return all validation errors.
   * @return true if the validator should abort upon the first error or
   *   false if the validator should return all validation errors.
   */
  public boolean isStopOnFirstFailure() {
    return stopOnFirstFailure;
  }

  /**
   * Sets the number of ValidationResults that can be set before discontinuing validation.
   * @param failureThreshold
   */
  public void setFailureThreshold(int failureThreshold) {
    this.failureThreshold = failureThreshold;
  }

  /**
   * Returns the number of ValidationResults that can be set before discontinuing validation.
   * @return
   */
  public int getFailureThreshold() {
    return failureThreshold;
  }
}