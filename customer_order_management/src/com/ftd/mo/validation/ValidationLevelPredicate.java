package com.ftd.mo.validation;

import org.apache.commons.collections.Predicate;

/**
 * Returns true if validating at this level or greater, otherwise false.
 * Ex: If validating at WARNING level, both WARNINGs and ERRORs will return true.
 *
 * @author Mark Moon, Software Architects Inc.
 * @version $Revision: 1.2 $
 */
public class ValidationLevelPredicate
    implements Predicate {

  private Integer validationLevel;

  /*
   * Private constructor suggests use of interface type (Predicate).
   * @param validationLevel The validation level to check against.
   */
  private ValidationLevelPredicate(Integer validationLevel) {
    super();
    this.validationLevel = validationLevel;
  }

  /**
   * Returns an instance of ValidationLevelPredicate.
   * @param validationLevel The validation level to check against.
   * @throws IllegalArgumentException if validationLevel is null
   */
  public static Predicate getInstance(Integer validationLevel) {

    // null check input
    if ( validationLevel == null ) {
      throw new IllegalArgumentException("Illegal parameter: validationLevel was null.");
    }

    return new ValidationLevelPredicate(validationLevel);
  }

  /**
   * Returns true if validating at this level or greater, otherwise false.
   * Ex: If validating at WARNING level, both WARNINGs and ERRORs will return true.
   *
   * @param input java.lang.Number containing the validation level to check
   * @return true if validating at this level or greater, otherwise false
   * @see com.ftd.customerordermanagement.validation.ValidationLevels
   */
  public boolean evaluate(Object input) {

    // null check input
    if ( input == null ) {
      throw new IllegalArgumentException("Illegal parameter: input was null.");
    }

    // type check input
    if ( !(input instanceof Number) ) {
      throw new IllegalArgumentException("Illegal parameter: expecting type java.lang.Number");
    }

    return ((Number)input).intValue() <= validationLevel.intValue();
  }
}