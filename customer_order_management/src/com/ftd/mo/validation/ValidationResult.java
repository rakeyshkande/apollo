package com.ftd.mo.validation;

/**
 * ValidationResult defines information generated via the validation process.
 *
 * @author Mark Moon, Software Architects Inc.
 * @version $Revision: 1.2 $
 */
public class ValidationResult {

  private Integer validationLevel;
  private String validationRule;
  private String validationMessage;

  /**
   * Default constructor.
   */
  public ValidationResult() {
    super();
  }


  public void setValidationLevel(Integer validationLevel) {
    this.validationLevel = validationLevel;
  }


  public Integer getValidationLevel() {
    return validationLevel;
  }


  public void setValidationRule(String validationRule) {
    this.validationRule = validationRule;
  }


  public String getValidationRule() {
    return validationRule;
  }


  public void setValidationMessage(String validationMessage) {
    this.validationMessage = validationMessage;
  }


  public String getValidationMessage() {
    return validationMessage;
  }
}