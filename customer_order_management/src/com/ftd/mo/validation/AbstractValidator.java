package com.ftd.mo.validation;

import com.ftd.osp.utilities.plugins.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.Predicate;
import org.apache.commons.lang.StringUtils;

/**
 * AbstractValidator implements the base features required of Validators.
 *
 * @author Mark Moon, Software Architects Inc.
 * @version $Revision: 1.2 $
 */
public abstract class AbstractValidator
    implements Validator {

  private static final Logger logger = new Logger( AbstractValidator.class.getName() );

  private Map validationResults;
  private ValidationConfiguration validationConfiguration;
  private Predicate validationLevelPredicate;

  protected boolean continueValidation;
  protected Integer validationLevel;


  /**
   * Default constructor.
   */
  public AbstractValidator() {
    super();
    continueValidation = true;
    validationResults = new HashMap();
  }


  /**
   * Returns the configuration settings for the validator.
   * @return validationConfiguration
   */
  public void setValidationConfiguration(ValidationConfiguration validationConfiguration) {
    this.validationConfiguration = validationConfiguration;
    this.validationLevel = validationConfiguration.getValidationLevel();

    // set the predicate used to filter validation results
    this.validationLevelPredicate = ValidationLevelPredicate.getInstance( validationLevel );
  }

  /**
   * Performs all validation rules defined within the implementing class.
   * @return true if all validation is successful, otherwise false.
   */
  public ValidationConfiguration getValidationConfiguration() {
    return validationConfiguration;
  }


  /**
   * Convienence method to create a ValidationResult.  Preferred way to create a ValidationResult
   * in the event that ValidationResult is subclassed thus requiring an interface.  If the supplied
   * parameters are null, empty strings will be set on the ValidationResult in the place of null.
   *
   * @param validationLevel
   * @param validationRule
   * @param validationMessage
   * @return ValidationResult
   * @throws IllegalArgumentException if validationLevel is null
   */
  public ValidationResult createValidationResult(Integer validationLevel, String validationRule, String validationMessage) {

    // null check validationLevel
    if ( validationLevel == null ) {
      logger.error("Invalid parameter: validationLevel was null");
      throw new IllegalArgumentException("Invalid parameter: validationLevel was null");
    }

    ValidationResult toReturn = new ValidationResult();
    toReturn.setValidationLevel( validationLevel );
    toReturn.setValidationRule( StringUtils.defaultString(validationRule) );
    toReturn.setValidationMessage( StringUtils.defaultString(validationMessage) );
    return toReturn;
  }


  /**
   * Convienence method to create a ValidationResult and add it to the list. If the supplied
   * parameters are null, empty strings will be set on the ValidationResult in the place of null.
   * Checks that the supplied ValidationResult is of the correct validation level as defined in
   * the ValidationConfiguration.
   *
   * @param validationLevel
   * @param validationRule
   * @param validationMessage
   * @throws IllegalArgumentException if validationLevel is null
   */
  public void addValidationResult(Integer validationLevel, String validationRule, String validationMessage) {
    addValidationResult( createValidationResult(validationLevel, validationRule, validationMessage) );
  }


  /**
   * Adds ValidationResults to a List.  Checks that the supplied ValidationResult is
   * of the correct validation level as defined in the ValidationConfiguration.
   *
   * @param validationResult The ValidationResult to add.
   */
  public void addValidationResult(ValidationResult validationResult) {

    // only add ValidationResults of the configured level
    if ( validationLevelPredicate.evaluate(validationResult.getValidationLevel()) ) {
      validationResults.put(validationResult.getValidationRule(), validationResult);

      // if stopping on the first failure, set flag to stop validation
      if ( validationConfiguration.isStopOnFirstFailure() ) {
        continueValidation = false;
        if ( logger.isDebugEnabled() ) logger.debug("Stopping validation: validator configured to stop on first failure");
      }

      // if the failure threshold has been reached, set flag to stop validation
      if ( validationResults.size() == validationConfiguration.getFailureThreshold()  ) {
        continueValidation = false;
        if ( logger.isDebugEnabled() ) logger.debug("Stopping validation: validator configured to stop at " + validationConfiguration.getFailureThreshold() + " failures");
      }
    }
  }


  /**
   * Returns the validation results.
   * @return Map of ValidationResults
   */
  public Map getValidationResults() {
    return validationResults;
  }


  /**
   * Returns the validation results.
   * @return List of ValidationResults
   */
  public List getValidationResultsAsList() {
    return new ArrayList(validationResults.values());
  }


  /**
   * Performs all validation rules defined within the implementing class.
   * @return true if all validation is successful, otherwise false.
   */
  public abstract boolean validate()
      throws Exception;
}