package com.ftd.mo.validation;

/**
 * Validator defines the contract that all implementations must fulfill.
 *
 * @author Mark Moon, Software Architects Inc.
 * @version $Revision: 1.2 $
 */
public interface Validator {

  /**
   * Sets all configuration settings for the validator.
   * @param validationConfiguration
   */
  public void setValidationConfiguration(ValidationConfiguration validationConfiguration);

  /**
   * Returns the configuration settings for the validator.
   * @return validationConfiguration
   */
  public ValidationConfiguration getValidationConfiguration();

  /**
   * Performs all validation rules defined within the implementing class.
   * @return true if all validation is successful, otherwise false.
   */
  public boolean validate()
      throws Exception;

  /**
   * Returns the validation results.
   * @return Map key<ValidationRule>, value<ValidationResult>
   */
  public java.util.Map getValidationResults();

  /**
   * Returns the validation results.
   * @return List
   */
  public java.util.List getValidationResultsAsList();

}