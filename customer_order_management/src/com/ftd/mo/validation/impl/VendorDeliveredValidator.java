package com.ftd.mo.validation.impl;

import java.sql.Connection;
import java.util.HashMap;

import org.apache.commons.lang.StringUtils;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.mo.dao.UpdateOrderDAO;
import com.ftd.mo.validation.ValidationLevels;
import com.ftd.mo.validation.ValidationRules;
import com.ftd.mo.vo.DeliveryInfoVO;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * Performs Vendor Delivered specific validations.
 *
 */
public class VendorDeliveredValidator
    extends BaseDeliveredValidator {

  private static final String COUNTRY_HANDLER_NAME = "COUNTRY";
  private static final String COUNTRY_DOMESTIC = "D";
  private static final String PRODUCT_STATUS_AVAILABLE = "A";
  private static final String ORDER_PRINTED = "Printed";
  private static final String ORDER_SHIPPED = "Shipped";


  private static final Logger logger = new Logger( VendorDeliveredValidator.class.getName() );
  private UpdateOrderDAO updateOrderDao = null;
  private boolean checkIfFloristCoversZip = true;
  private String   floristSundayFlag = null;
  private String   floristCurrentBlock = null;
  private String   floristProducts = null;

  /**
   * Creates a new VendorDeliveredValidator.
   * @param deliveryInfoVO The DeliveryInfoVO object to validate
   */
  public VendorDeliveredValidator(DeliveryInfoVO a_divo, Connection a_con) {
    super(a_divo, a_con);
  }


  /**
   * Overloaded validate method to perform all validations, but with option
   * to skip (i.e., short-circuit) validateFloristAvailableForZip check.
   *
   * @return true if all validation is successful, otherwise false.
   */
  public boolean validate(boolean a_checkIfFloristCoversZip)
      throws Exception {
    this.checkIfFloristCoversZip = a_checkIfFloristCoversZip;
    return validate();
  }


  /**
   * Performs all validation rules for florist delivered validation.
   *
   * @return true if all validation is successful, otherwise false.
   */
  public boolean validate()
      throws Exception {

    boolean toReturn = super.validate();
    updateOrderDao = new UpdateOrderDAO(this.con);

    if ( continueValidation ) {
      toReturn = preValidate();
      if (!toReturn) return false;  // If a_divo bad, no sense continuing
    }
    if ( continueValidation ) {
      toReturn = validateDomesticOrder();
      if (!toReturn) return false;
    }
    if ( continueValidation ) {
      toReturn = validateProductAvailability();
      if (!toReturn) return false;
    }
    if ( continueValidation ) {
      toReturn = validateOrderNotPrinted();
      if (!toReturn) return false;
    }
    return toReturn;
  }


  /**
   * Pre-validation to ensure we have necessary attributes set in DeliveryInfoVO
   */
  private boolean preValidate() throws Exception {
    boolean toReturn = true;

    if (StringUtils.isBlank(this.divo.getOrderDetailId()) ||
        StringUtils.isBlank(this.divo.getProductId()) ||
        StringUtils.isBlank(this.divo.getRecipientCountry())) {
      addValidationResult(ValidationLevels.ERROR, ValidationRules.PRE_VALIDATION_ERROR, null);
      toReturn = false;
      if ( logger.isDebugEnabled() ) logger.debug("ERROR in VendorDeliveredValidator: Invalid/incomplete DeliveryInfoVO object");
    }
    return toReturn;
  }


  /**
   * Check if domestic order
   * Check to ensure recipient address is not APO or PO BOX
   */
  private boolean validateDomesticOrder() throws Exception {
    boolean toReturn = true;
    /*
    CountryHandler ch = (CountryHandler) CacheController.getInstance().getHandler(COUNTRY_HANDLER_NAME);
    CountryVO countryVo = ch.getCountryById(this.divo.getRecipientCountry());
    if (countryVo != null && !COUNTRY_DOMESTIC.equalsIgnoreCase(countryVo.getType())) {
    */
    if (!COMConstants.CONS_COUNTRY_CODE_US.equalsIgnoreCase(this.divo.getRecipientCountry())) {
      addValidationResult(ValidationLevels.ERROR, ValidationRules.DROP_SHIP_PRODUCT_DELIVERY_RESTRICTIONS, null);
      toReturn = false;
      if ( logger.isDebugEnabled() ) logger.debug("VendorDeliveredValidator: Drop ship products can't be delivered outside of US");
    }

    String state = null;
    String countryCode = null;
    try {
      CachedResultSet rs = updateOrderDao.getZipCodeList(this.divo.getRecipientSmartZipCode(), null, null);
      if (rs.next()) {
        state = rs.getString("state");
        countryCode = rs.getString("countryCode");
      }
    }
    catch (Exception e)
    {
      throw e;
    }
    if (state == null && countryCode == null) {
      addValidationResult(ValidationLevels.ERROR, ValidationRules.DROP_SHIP_PRODUCT_DELIVERY_RESTRICTIONS, null);
      toReturn = false;
      if ( logger.isDebugEnabled() ) logger.debug("VendorDeliveredValidator: Drop ship must be in US - zipcode not found");
    } else {
      // Country code is returned as null for US.
      if (StringUtils.isNotEmpty(countryCode) || StringUtils.equals(state, COMConstants.CONS_STATE_CODE_PUERTO_RICO) ||
          StringUtils.equals(state, COMConstants.CONS_STATE_CODE_VIRGIN_ISLANDS)) {
        addValidationResult(ValidationLevels.ERROR, ValidationRules.DROP_SHIP_PRODUCT_DELIVERY_RESTRICTIONS, null);
        toReturn = false;
        if ( logger.isDebugEnabled() ) logger.debug("VendorDeliveredValidator: Drop ship must be in US - zip not in US");
      }
    }

    // Check for APO and PO boxes in address lines
    String address1Upper = this.divo.getRecipientAddress1() == null ? "" : this.divo.getRecipientAddress1().toUpperCase();
    String address2Upper = this.divo.getRecipientAddress2() == null ? "" : this.divo.getRecipientAddress2().toUpperCase();

    if(checkForPO(address1Upper))
    {
      addValidationResult(ValidationLevels.ERROR, ValidationRules.RECIP_PO_NOT_ALLOWED_ADDRESS_ONE, null);
      toReturn = false;
      if ( logger.isDebugEnabled() ) logger.debug("VendorDeliveredValidator: PO or APO box is not allowed in address line one");
    }
    if(checkForPO(address2Upper))
    {
      addValidationResult(ValidationLevels.ERROR, ValidationRules.RECIP_PO_NOT_ALLOWED_ADDRESS_TWO, null);
      toReturn = false;
      if ( logger.isDebugEnabled() ) logger.debug("VendorDeliveredValidator: PO or APO box is not allowed in address line two");
    }
    // End check for APO and PO boxes in address lines

    return toReturn;
  }

  private static boolean checkForPO(String inStr)
  {
    boolean ret = false;

    if((inStr.indexOf("APO ") != -1) || (inStr.indexOf("A.P.O.") != -1) ||
       (inStr.indexOf("PO BOX") != -1) || (inStr.indexOf("P.O.") != -1))
    {
      ret = true;
    }

    return ret;
  }
  /**
   * Check product availability
   */
  private boolean validateProductAvailability() throws Exception {
    boolean toReturn = true;
    CachedResultSet rs = updateOrderDao.getProductInfo(this.divo.getProductId());
    rs.next();
    String prodStatus = rs.getString("status");
    String shipMethodCarrier = rs.getString("shipMethodCarrier");
    if (!PRODUCT_STATUS_AVAILABLE.equalsIgnoreCase(prodStatus)) {
      addValidationResult(ValidationLevels.ERROR, ValidationRules.PRODUCT_UNAVAILABLE, null);
      toReturn = false;
      if ( logger.isDebugEnabled() ) logger.debug("VendorDeliveredValidator: Product unavailable");
    } else if (shipMethodCarrier == null || !shipMethodCarrier.equalsIgnoreCase("Y")) {
      addValidationResult(ValidationLevels.ERROR, ValidationRules.DROP_SHIP_NOT_AVAILABLE, null);
      toReturn = false;
      if ( logger.isDebugEnabled() ) logger.debug("VendorDeliveredValidator: Product unavailable for vendor delivery");
    }
    return toReturn;
  }


  /**
   * Check if order already printed
   */
  private boolean validateOrderNotPrinted() throws Exception {
    boolean toReturn = true;
    HashMap orderMap = updateOrderDao.findOrderNumber(this.divo.getOrderDetailId());
    if (orderMap != null) {
      String orderDispCode = (String)orderMap.get("OUT_ORDER_DISP_CODE");
      if (ORDER_PRINTED.equalsIgnoreCase(orderDispCode) || ORDER_SHIPPED.equalsIgnoreCase(orderDispCode)) {
        toReturn = false;
        addValidationResult(ValidationLevels.ERROR, ValidationRules.ORDER_ALREADY_PRINTED, null);
        if ( logger.isDebugEnabled() ) logger.debug("VendorDeliveredValidator: Order already printed");
      }
    }
    return toReturn;
  }

}