package com.ftd.mo.validation.impl;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.dao.LockDAO;
import com.ftd.mo.dao.UpdateOrderDAO;
import com.ftd.mo.util.SearchUtil;
import com.ftd.mo.validation.ValidationLevels;
import com.ftd.mo.validation.ValidationRules;
import com.ftd.mo.vo.DeliveryInfoVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.ftdutilities.DeliveryDateUTIL;
import com.ftd.ftdutilities.OEDeliveryDateParm;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;

import java.util.Calendar;
import java.util.Date;
import java.sql.Connection;

import org.apache.commons.lang.StringUtils;

/**
 * Performs Florist Delivered specific validations.
 *
 */
public class FloristDeliveredValidator
    extends BaseDeliveredValidator {

  private static final Logger logger = new Logger( FloristDeliveredValidator.class.getName() );
  private static String floristNumber = null;

  private UpdateOrderDAO updateOrderDao = null;
  private String   floristSundayFlag = null;
  private String   floristCodificationBlock = null;
  private String   floristBlock = null;
  private String   floristStatus = null;
  private String   floristZipBlockFlag = null;
  private String   floristCityMatchIndicator = null;
  private boolean floristRetransCountry = false;
  private String   shipMethodFlorist = null;
  private String   shipMethodCarrier = null;
  private boolean floristIdMatch = false;
  private boolean otherFloristMatch = false;
  private boolean otherFloristsAllowSunday = false;
  private boolean otherFloristActive = false;
  private String   productCodified = null;
  private String   productStatus = null;
  private boolean allOtherFloristsHaveCodificationBlocks = false;
  private boolean allOtherFloristsUnavailable            = false;
  private boolean allOtherFloristsHaveZipBlocks          = false;
  private boolean allOtherFloristsDontMatchCity          = false;
  private boolean otherFloristOkCodificationAndSunday;
  private boolean internationalOrder = false;

  private static final String RETRANS_GLOBAL_CONTEXT = "International Florist";
  private static final String RETRANS_GLOBAL_NAME    = "Florist Number";

  /**
   * Creates a new FloristValidator.
   * @param deliveryInfoVO The DeliveryInfoVO object to validate
   */
  public FloristDeliveredValidator(DeliveryInfoVO a_divo, Connection a_con) {
    super(a_divo, a_con);
  }


  /**
   * Performs all validation rules for florist delivered validation.
   *
   * @return true if all validation is successful, otherwise false.
   */
  public boolean validate()
      throws Exception {

    boolean toReturn = super.validate();
    updateOrderDao = new UpdateOrderDAO(this.con);

    if ( continueValidation ) {
      toReturn = preValidate();
      if (!toReturn) return false;  // If a_divo bad, no sense continuing
    }
    if ( continueValidation ) toReturn = validateNoLiveMercuryOrder() && toReturn;
    if ( continueValidation ) validateFloristId();
    if ( continueValidation ) toReturn = validateFloristActive() && toReturn;
    if ( continueValidation ) toReturn = validateCodifications() && toReturn;
    if ( continueValidation ) toReturn = validateFloristAvailableForZipOrCity() && toReturn;
    return toReturn;
  }


  /**
   * Pre-validation to ensure we have necessary attributes set in DeliveryInfoVO
   */
  private boolean preValidate() throws Exception {
    boolean toReturn = true;

    if (StringUtils.isBlank(this.divo.getOrderDetailId()) ||
        StringUtils.isBlank(this.divo.getProductId()) ||
        StringUtils.isBlank(this.divo.getRecipientCountry())) {
      addValidationResult(ValidationLevels.ERROR, ValidationRules.PRE_VALIDATION_ERROR, null);
      toReturn = false;
      if ( logger.isDebugEnabled() ) logger.debug("ERROR in FloristDeliveredValidator: Invalid/incomplete DeliveryInfoVO object");
    }
    return toReturn;
  }


  /**
   * Invalid if no live Mercury order (i.e., FTD Mercury order has been cancelled or rejected)
   */
  private boolean validateNoLiveMercuryOrder() throws Exception {
    boolean toReturn = true;

    if (updateOrderDao.hasLiveMercury(this.divo.getOrderDetailId()) == false) {
      addValidationResult(ValidationLevels.ERROR, ValidationRules.NO_LIVE_MERCURY, null);
      toReturn = false;
      if ( logger.isDebugEnabled() ) logger.debug("No live Mercury order");
    }
    return toReturn;
  }


  /**
   * Gets florist/product info based on florist ID and product ID.
   * This is needed for the remainder of the validations.
   */
  private void validateFloristId() throws Exception {
    String currFloristId;
    String floristStr = "";

    // Obtain pertinent florist/product info from DB and set local flags appropriately
    //
    CachedResultSet rs = null;
    rs = updateOrderDao.getFloristProductInfo(this.divo.getFloristId(), this.divo.getProductId(),
                                              this.divo.getRecipientSmartZipCode(), this.divo.getRecipientCity(),
                                              this.divo.getDeliveryDate());

    this.allOtherFloristsHaveCodificationBlocks = true;  // Assume the worst
    this.allOtherFloristsUnavailable            = true;
    this.allOtherFloristsHaveZipBlocks          = true;
    this.allOtherFloristsDontMatchCity          = true;
    this.otherFloristOkCodificationAndSunday    = false;
    boolean gotCommonStuff = false;
    try {
      while (rs.next()) {

        // These will be same across result set
        //
        if ( !gotCommonStuff) {
          gotCommonStuff = true;
          this.shipMethodFlorist = StringUtils.trim(rs.getString("ship_method_florist"));
          this.shipMethodCarrier = StringUtils.trim(rs.getString("ship_method_carrier"));
          this.productCodified   = StringUtils.trimToEmpty(rs.getString("product_codified"));
          this.productStatus     = StringUtils.trimToEmpty(rs.getString("product_status"));
        }

        //------------
        //
        // Florist currently associated with order was found
        //
        currFloristId = StringUtils.trim(rs.getString("florist_id"));
        if (StringUtils.trimToEmpty(rs.getString("requested_florist_flag")).equalsIgnoreCase("Y")) {
          if (currFloristId.equals("N/A")) {
            // If floristId was null for some reason, stored proc will return 'N/A', so
            // just ignore it.
            continue;
          }
          this.floristIdMatch = true;
          this.floristSundayFlag = StringUtils.trim(rs.getString("sunday_delivery_flag"));
          this.floristStatus = StringUtils.trim(rs.getString("florist_status"));
          this.floristCodificationBlock = StringUtils.trim(rs.getString("florist_codification_block"));
          this.floristBlock = StringUtils.trim(rs.getString("florist_block"));
          this.floristZipBlockFlag = StringUtils.trim(rs.getString("florist_zip_block_flag"));
          this.floristCityMatchIndicator = StringUtils.trim(rs.getString("city_match_indicator"));

        //------------
        //
        // Other florist(s) were found
        //
        } else {
          this.otherFloristMatch = true;
          if (logger.isDebugEnabled()) {
            floristStr += rs.getString("florist_id") + " ";
          }

          // If florist_block is not empty then skip it since it's blocked
          //
          if (StringUtils.isNotBlank(rs.getString("florist_block"))) {
            continue;
          } else {
            this.allOtherFloristsUnavailable = false;
          }

          boolean currFloristOkCodificationAndSunday = true;  // Assume the best

          // If zipBlockFlag is non-null (which means florist covers zip) and
          // zip block is not yes, then a florist is available so clear flag
          //
          String zbFlag = StringUtils.trim(rs.getString("florist_zip_block_flag"));
          if (zbFlag != null && !zbFlag.equals("Y")) {
            this.allOtherFloristsHaveZipBlocks = false;
          }

          // If city_match_indicator is set then a florist covers efos city so clear flag
          //
          if (StringUtils.trimToEmpty(rs.getString("city_match_indicator")).equalsIgnoreCase("Y")) {
            this.allOtherFloristsDontMatchCity = false;
          }

          // Stored proc sets floristCodificationBlock to 'Y' if codified product and codification is blocked
          // so clear flag if any florist does NOT have codification block (or product not codified)
          //
          if ( ! StringUtils.trimToEmpty(rs.getString("florist_codification_block")).equalsIgnoreCase("Y")) {
            this.allOtherFloristsHaveCodificationBlocks = false;
          } else {
            currFloristOkCodificationAndSunday = false;
          }

          // If sunday delivery, then at least one florist has sunday delivery so set flag
          //
          if (StringUtils.trimToEmpty(rs.getString("sunday_delivery_flag")).equalsIgnoreCase("Y")) {
            this.otherFloristsAllowSunday = true;
          } else {
            currFloristOkCodificationAndSunday = false;
          }

          if (currFloristOkCodificationAndSunday) {
            this.otherFloristOkCodificationAndSunday = true;
          }
        }

      } // end while
    }
    catch (Exception e)
    {
      throw e;
    }

    String recipCountry = this.divo.getRecipientCountry();

    //check if recipient is domestic
    SearchUtil sUtil = new SearchUtil(this.con);
    boolean domestic = sUtil.isDomestic(recipCountry);

    // If not US, see if it's a retrans florist
    if (recipCountry != null && !domestic) {
      String retransFlorist = updateOrderDao.getInternationalFlorist(divo.getOrderDetailId());
      if (retransFlorist.equals(this.divo.getFloristId())) {
        this.floristRetransCountry = true;
      }
      this.internationalOrder = true;
    } else {
      this.internationalOrder = false;
    }

    if (this.floristIdMatch || this.otherFloristMatch) {
      if (logger.isDebugEnabled()) {
        logger.debug("Validating order: " + this.divo.getOrderDetailId());
        logger.debug("For florist,product,zip,city,country: " +
                     this.divo.getFloristId() + "," + this.divo.getProductId() + "," +
                     this.divo.getRecipientSmartZipCode() + "," + this.divo.getRecipientCity() + "," +
                     this.divo.getRecipientCountry());
        if (this.floristIdMatch) {
          logger.debug("Florist status: " + floristStatus);
          logger.debug("Sunday flag: " + floristSundayFlag);
          logger.debug("Product status,codified: " + productStatus + "," + productCodified);
          logger.debug("Indicators (cityMatch, retrans): " + floristCityMatchIndicator + "," + floristRetransCountry);
          logger.debug("ShipMethod (florist,carrier): " + shipMethodFlorist + "," + shipMethodCarrier);
          logger.debug("Florist block info: (floristBlock,floristCodificationBlock,floristZipBlockFlag): " +
                       floristBlock + "," + floristCodificationBlock + "," + floristZipBlockFlag);
        }
        if (this.otherFloristMatch) {
          logger.debug("Other florists for zip found: " + floristStr);
          logger.debug("Any other florists allow Sunday: " + otherFloristsAllowSunday);
          logger.debug("All other florists are unavailable: " + allOtherFloristsUnavailable);
          logger.debug("All other florists have CodificationBlocks,ZipBlocks: " +
                      allOtherFloristsHaveCodificationBlocks + "," + allOtherFloristsHaveZipBlocks);
        } else {
          logger.debug("No other florists found");
        }
      } // end isDebugEnabled
    }
  }

  /**
   * Check if florist is active/suspended and make sure product is
   * florist deliverable and available.
   */
  private boolean validateFloristActive() throws Exception {

    // Check if current florist is active
    //
    if (this.floristIdMatch && StringUtils.isEmpty(this.floristBlock) &&
        ( "Active".equalsIgnoreCase(this.floristStatus))) {
      addValidationResult(ValidationLevels.INFO, ValidationRules.FLORIST_ACTIVE, null);
    } else {
      if ( logger.isDebugEnabled() ) logger.debug("Assigned florist is inactive, blocked, or suspended");
      addValidationResult(ValidationLevels.INFO, ValidationRules.FLORIST_SUSPENDED, null);
    }

    // Check if any other florists are active
    //
    if (this.otherFloristMatch && !this.allOtherFloristsUnavailable) {
      addValidationResult(ValidationLevels.INFO, ValidationRules.OTHER_FLORISTS_ACTIVE, null);
      if ( logger.isDebugEnabled() ) logger.debug("Other florists active");
      this.otherFloristActive = true;
    } else {
      this.otherFloristActive = false;
    }

    // Make sure product is florist deliverable
    //
    if (this.shipMethodFlorist == null || !this.shipMethodFlorist.equalsIgnoreCase("Y")) {
      addValidationResult(ValidationLevels.ERROR, ValidationRules.FLORIST_DELIVERY_NOT_AVAILABLE, null);
      if ( logger.isDebugEnabled() ) logger.debug("Product unavailable for florist delivery");
    }

    // Make sure product is available
    //
    if (!"A".equalsIgnoreCase(this.productStatus)) {
      addValidationResult(ValidationLevels.ERROR, ValidationRules.PRODUCT_UNAVAILABLE, null);
    }

    return true;  // Never an error, just informational
  }


  /**
   * Get codification details and make sure florist can deliver this product if codified.
   * Also check sunday delivery/codification combo.  Note that the OK_FOR_CODIFICATION
   * indicators are set if codification is not blocked OR product is NOT codified (this makes
   * it easier for the calling logic, which can use PRODUCT_IS_CODIFIED indicator if it
   * really needs to know).
   */
  private boolean validateCodifications() throws Exception {
    boolean okForCodification = true;  // Default to true in case product not codified
    /*
    OEDeliveryDateParm ddParms = new OEDeliveryDateParm();
    DeliveryDateUTIL.getCodificationDetails(ddParms, this.divo.getRecipientCountry(),
                                            this.divo.getProductId(), this.divo.getRecipientSmartZipCode(),
                                            COMConstants.CONS_COUNTRY_CODE_CANADA_CA, this.con);
    if (ddParms.getCodifiedProduct() != null && ddParms.getCodifiedProduct().equals("Y")) {
      if (this.floristProducts == null || this.floristProducts.indexOf(this.divo.getProductId()) == -1) {
        okForCodification = false;  // Product was codified but not for this florist
      }
    }
    */

    // Set indicator if product is codified.  Invoking classes might need to know this.
    //
    if (this.productCodified.equals("Y")) {
      addValidationResult(ValidationLevels.INFO, ValidationRules.PRODUCT_IS_CODIFIED, null);
    }

    // Check if current florist ok for codification and/or sunday delivery.
    // Note that GET_ROUTABLE_FLORIST_BY_ID sets floristCodificationBlock to 'Y'
    // if codified product and codification is blocked.
    //
    if (this.floristIdMatch) {
      if (this.floristCodificationBlock != null && this.floristCodificationBlock.equals("Y")) {
        okForCodification = false;
      }

      if (okForCodification) {
        addValidationResult(ValidationLevels.INFO, ValidationRules.OK_FOR_CODIFICATION, null);
      } else {
        if ( logger.isDebugEnabled() ) logger.debug("Florist not codified for product");
      }

      if (this.floristSundayFlag != null && !this.floristSundayFlag.equals("N")) {
        addValidationResult(ValidationLevels.INFO, ValidationRules.FLORIST_OK_FOR_SUNDAY, null);
      }
    }

    // Check if any other florists allow that codification and/or sunday delivery
    //
    if (this.otherFloristMatch) {

      if (this.otherFloristActive && this.otherFloristsAllowSunday) {
        addValidationResult(ValidationLevels.INFO, ValidationRules.OTHER_FLORISTS_OK_FOR_SUNDAY, null);
        if ( logger.isDebugEnabled() ) logger.debug("Other florists allow Sunday delivery");
      }

      if (!this.allOtherFloristsHaveCodificationBlocks) {
        addValidationResult(ValidationLevels.INFO, ValidationRules.OTHER_FLORISTS_OK_FOR_CODIFICATION, null);
        if (this.otherFloristOkCodificationAndSunday) {
          addValidationResult(ValidationLevels.INFO, ValidationRules.OTHER_FLORISTS_OK_FOR_CODIFICATION_AND_SUN, null);
        }
      }
    }

    return true;  // Never an error, just informational
  }


  /**
   * Check if florist covers zip, city, or country
   */
  private boolean validateFloristAvailableForZipOrCity() throws Exception {
    boolean okForZip;
    boolean okForCity;
    boolean okForCountry;

    if (this.floristIdMatch) {

      // Check if current florist ok for city
      //
      if ((this.floristCityMatchIndicator != null) && (this.floristCityMatchIndicator.equals("Y"))) {
        okForCity = true;
        addValidationResult(ValidationLevels.INFO, ValidationRules.OK_FOR_CITY, null);
        if ( logger.isDebugEnabled() ) logger.debug("Florist matches efos city");
      } else {
        okForCity = false;
        if ( logger.isDebugEnabled() ) logger.debug("Florist doesn't match efos city");
      }

      // Check if current florist ok for zip
      //
      okForZip = true;     // Default to true
      //okForZip = this.updateOrderDao.floristAvailableForZip(this.divo.getFloristId(), this.divo.getRecipientSmartZipCode());
      if (this.floristZipBlockFlag == null || this.floristZipBlockFlag.equals("Y") ||
          this.internationalOrder) {
        // Note that zipBlockFlag is non-null only if florist delivers to zipcode
        okForZip = false;
      }
      if (okForZip) {
        addValidationResult(ValidationLevels.INFO, ValidationRules.OK_FOR_ZIP, null);
        if ( logger.isDebugEnabled() ) logger.debug("Florist supports zip");
      } else {
        if ( logger.isDebugEnabled() ) logger.debug("Florist does not deliver in zip");
      }

      // Check if current (retrans) florist ok for country
      //
      if (this.floristRetransCountry) {
        okForCountry = true;
        if ( logger.isDebugEnabled() ) logger.debug("Florist (retrans) ok for country");
      } else {
        okForCountry = false;
      }

      if (okForZip || okForCity || okForCountry) {
        addValidationResult(ValidationLevels.INFO, ValidationRules.OK_FOR_ZIP_OR_CITY, null);
      }
    }

    // Check if any other florists are available for zip or city.
    //
    if (this.otherFloristMatch) {
      if (this.allOtherFloristsHaveZipBlocks == false) {
        addValidationResult(ValidationLevels.INFO, ValidationRules.OTHER_FLORISTS_OK_FOR_ZIP_OR_CITY, null);
      }
      if (this.allOtherFloristsDontMatchCity == false) {
        addValidationResult(ValidationLevels.INFO, ValidationRules.OTHER_FLORISTS_OK_FOR_ZIP_OR_CITY, null);
      }
    }

    return true;  // Never an error, just informational
  }

}