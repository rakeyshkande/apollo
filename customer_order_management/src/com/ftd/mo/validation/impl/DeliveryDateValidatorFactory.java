package com.ftd.mo.validation.impl;

import com.ftd.mo.validation.ValidationConfiguration;
import com.ftd.mo.validation.Validator;
import com.ftd.mo.vo.DeliveryInfoVO;
import com.ftd.osp.utilities.plugins.Logger;
import java.sql.Connection;

import org.apache.commons.lang.StringUtils;

/**
 * DeliveryDateValidatorFactory creates DeliveryDateValidators based on the DeliveryInfoVO
 * supplied. Validator behavior can be altered via ValidationConfigurations.
 *
 * @author Mark Moon, Software Architects Inc.
 * @version $Revision: 1.2 $
 * @see com.ftd.customerordermanagement.vo.DeliveryInfoVO
 * @see com.ftd.customerordermanagement.validation.ValidationConfiguration
 */
public class DeliveryDateValidatorFactory {

  private static final DeliveryDateValidatorFactory FACTORY = new DeliveryDateValidatorFactory();
  private static final Logger logger = new Logger( DeliveryDateValidatorFactory.class.getName() );

  /**
   * Protected constructor enforces singleton usage while allowing subtypes to be created.
   */
  protected DeliveryDateValidatorFactory() {
    super();
  }


  /**
   * Returns an instace of DeliveryDateValidatorFactory.
   * @return DeliveryDateValidatorFactory
   */
  public static DeliveryDateValidatorFactory getInstance() {
    return FACTORY;
  }


  /**
   * Returns a DeliveryDate validator based on the supplied DeliveryInfoVO.
   * If DeliveryInfoVO.vendorFlag is "N", a FloristDeliveryDateValidator will
   * be returned, otherwise a VendorDeliveryDateValidator will be returned.  If
   * validationConfiguration is null, a default ValidationConfiguration will be used.
   *
   * @param validationConfiguration The configuration details for implementing validator.
   * @param deliveryInfoVO The DeliveryInfoVO object to validate
   * @return Validator
   * @throws IllegalArgumentException if validationConfiguration or deliveryInfoVO are null.
   */
  public Validator getValidator(ValidationConfiguration validationConfiguration, DeliveryInfoVO deliveryInfoVO,
                                Connection connection)
  {

    // null check deliveryInfoVO
    if ( deliveryInfoVO == null ) {
      logger.error("Invalid parameter: DeliveryInfoVO is null");
      throw new IllegalArgumentException("Invalid parameter: DeliveryInfoVO is null");
    }

    // use ValidationConfiguration defaults if null was supplied
    if ( validationConfiguration == null ) {
      validationConfiguration = new ValidationConfiguration();
    }

    Validator toReturn;
    if ( StringUtils.equals(deliveryInfoVO.getVendorFlag(), "N") ) {
      toReturn = new FloristDeliveryDateValidator( deliveryInfoVO, connection );
    }
    else {
      toReturn = new VendorDeliveryDateValidator( deliveryInfoVO, connection );
    }
    toReturn.setValidationConfiguration( validationConfiguration );

    if ( logger.isDebugEnabled() ) logger.debug( "Returning new DeliveryDateValidator of type: " + toReturn.getClass().getName() );
    return toReturn;
  }
}