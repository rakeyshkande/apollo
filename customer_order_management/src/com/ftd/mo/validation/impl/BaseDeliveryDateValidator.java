package com.ftd.mo.validation.impl;

import com.ftd.customerordermanagement.util.COMDeliveryDateUtil;
import com.ftd.customerordermanagement.util.DeliveryDateParameters;
import com.ftd.mo.validation.AbstractValidator;
import com.ftd.mo.validation.ValidationLevels;
import com.ftd.mo.validation.ValidationRules;
import com.ftd.mo.vo.DeliveryInfoVO;
import com.ftd.ftdutilities.DeliveryDateUTIL;
import com.ftd.ftdutilities.OEDeliveryDate;
import com.ftd.ftdutilities.OEDeliveryDateParm;
import com.ftd.osp.utilities.plugins.Logger;

import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.List;
import java.util.Date;
import java.sql.Connection;

import org.apache.commons.lang.StringUtils;

/**
 * BaseDeliveryDateValidator provides common method which may be useful
 * for implementing DeliveryDateValidators.
 *
 * @author Mark Moon, Software Architects Inc.
 */
public class BaseDeliveryDateValidator
    extends AbstractValidator {

  private static final Logger logger = new Logger( BaseDeliveryDateValidator.class.getName() );

  protected DeliveryInfoVO deliveryInfoVO;
  protected DeliveryDateParameters deliveryDateParameters;
  protected OEDeliveryDateParm oeDeliveryDateParameters;
  protected COMDeliveryDateUtil dateUtil;
  protected Connection con;

  /**
   * Creates a new BaseDeliveryDateValidator.
   */
  protected BaseDeliveryDateValidator(DeliveryInfoVO deliveryInfoVO, Connection connection) {
    super();
    this.deliveryInfoVO = deliveryInfoVO;
    this.con = connection;
    dateUtil = new COMDeliveryDateUtil();
  }

  /**
   * Prepares the data members which may be be useful to subclassed DeliveryDateValidators.
   * Any common/shared validation between all DeliveryDateValidators can be placed here.
   * @return true if valid, otherwise false
   */
  public boolean validate()
      throws Exception {

    // null check requested delivery date
    if ( deliveryInfoVO.getDeliveryDate() == null ) {
      logger.error("Invalid parameter: requested delivery date was null.");
      throw new IllegalArgumentException("Invalid parameter: requested delivery date was null.");
    }

    createDeliveryDateParameters();
    createOEDeliveryDateParameters();

    boolean toReturn = true;
    if ( continueValidation ) toReturn = validateDeliveryAddress() && toReturn;
    if ( continueValidation ) toReturn = validateDeliveryDateAfterZipCutoff() && toReturn;
    if ( continueValidation ) toReturn = validateRequestedAndTodaysDates() && toReturn;

    return toReturn;
  }

  /**
   * Creates DeliveryDateParameters.
   */
  protected void createDeliveryDateParameters() {
    if ( deliveryDateParameters == null ) {
      deliveryDateParameters = new DeliveryDateParameters();
      deliveryDateParameters.setRecipientState( deliveryInfoVO.getRecipientState() );
      deliveryDateParameters.setRecipientZipCode( deliveryInfoVO.getRecipientZipCode() );
      deliveryDateParameters.setRecipientCountry( deliveryInfoVO.getRecipientCountry() );
      deliveryDateParameters.setProductId( deliveryInfoVO.getProductId() );
      deliveryDateParameters.setOccasionId( deliveryInfoVO.getOccasion() );
      deliveryDateParameters.setOrderOrigin( deliveryInfoVO.getOriginId() );
      deliveryDateParameters.setIsFlorist( StringUtils.equals(deliveryInfoVO.getVendorFlag(), "N") );
      deliveryDateParameters.setSourceCode(deliveryInfoVO.getSourceCode());
    }
  }

  /**
   * Creates OEDeliveryDateParameters.
   * @throws java.lang.Exception
   */
  protected void createOEDeliveryDateParameters()
     throws Exception {

    // ensure deliveryDateParameters have been created
    if ( deliveryDateParameters == null ) {
      createDeliveryDateParameters();
    }

    // create oeDeliveryDateParameters if not already created
    // also ensure that COMDeliveryDateUtil has the same set of OEDeliveryDateParms
    if ( oeDeliveryDateParameters == null ) {
      dateUtil.setParameters( deliveryDateParameters );
      oeDeliveryDateParameters = dateUtil.getOEParameters(con);
     }
     else {
       dateUtil.setOEParameters( oeDeliveryDateParameters );
     }
  }


  /**
   * Selected delivery date after zip/postal code cutoff date is invalid.
   */
  protected boolean validateDeliveryDateAfterZipCutoff() {
    boolean toReturn = true;
    Calendar cutoff = dateUtil.getCutoffDate();
    Calendar selectedDeliveryCal = deliveryInfoVO.getDeliveryDate();
    Calendar currentDate = Calendar.getInstance();

    //if the selected delivery date is same as todays
    if ( COMDeliveryDateUtil.isSameDay(cutoff, selectedDeliveryCal) )
    {
      selectedDeliveryCal.set(Calendar.HOUR_OF_DAY, cutoff.get(Calendar.HOUR_OF_DAY));
      selectedDeliveryCal.set(Calendar.MINUTE, cutoff.get(Calendar.MINUTE));
      selectedDeliveryCal.set(Calendar.SECOND, cutoff.get(Calendar.SECOND));

      Calendar zipCutoffDate;

      //create a calendar object with the hour/minute/second set according to the cutoff time from
      //the global parms table.
      zipCutoffDate = COMDeliveryDateUtil.parseCutoffTime(oeDeliveryDateParameters.getZipCodeCutoff());

      //check if the current time has passed the cutoff time.
      if ( currentDate.after(zipCutoffDate) ) {
        addValidationResult( ValidationLevels.WARNING, ValidationRules.SAME_DAY_CUTOFF_EXPIRED, null);
        toReturn = false;
        if ( logger.isDebugEnabled() ) logger.debug("Invalid delivery date: selected delivery date after zip/postal code cutoff date");
      }
    }
    return toReturn;
  }

  /**
   * Delivery Address is P.O. or APO address.
   */
  protected boolean validateDeliveryAddress()
  {
    boolean toReturn = true;
    // Check for APO and PO boxes in address lines
    String address1Upper = deliveryInfoVO.getRecipientAddress1() == null ? "" : deliveryInfoVO.getRecipientAddress1().toUpperCase();
    String address2Upper = deliveryInfoVO.getRecipientAddress2() == null ? "" : deliveryInfoVO.getRecipientAddress2().toUpperCase();

    if(checkForPO(address1Upper))
    {
      addValidationResult(ValidationLevels.ERROR, ValidationRules.RECIP_PO_NOT_ALLOWED_ADDRESS_ONE, null);
      toReturn = false;
      if ( logger.isDebugEnabled() ) logger.debug("BaseDeliveryDateValidator: PO or APO box is not allowed in address line one");
    }
    if(checkForPO(address2Upper))
    {
      addValidationResult(ValidationLevels.ERROR, ValidationRules.RECIP_PO_NOT_ALLOWED_ADDRESS_TWO, null);
      toReturn = false;
      if ( logger.isDebugEnabled() ) logger.debug("BaseDeliveryDateValidator: PO or APO box is not allowed in address line two");
    }
    // End check for APO and PO boxes in address lines

    return toReturn;

  }

  protected boolean checkForPO(String inStr)
  {
    boolean ret = false;

    if((inStr.indexOf("APO ") != -1) || (inStr.indexOf("A.P.O.") != -1) ||
       (inStr.indexOf("PO BOX") != -1) || (inStr.indexOf("P.O.") != -1) ||
       (inStr.indexOf("POBOX")  != -1))
    {
      ret = true;
    }

    return ret;
  }




  /*
   * validate the delivery option
   */
  protected boolean validateRequestedAndTodaysDates() throws Exception {
    boolean toReturn = true;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");

    String sRequestedDeliveryDateYYYYMMDD = sdf.format(deliveryInfoVO.getDeliveryDate().getTime());

    Calendar cTodaysDate = Calendar.getInstance();
    String sTodaysDateYYYYMMDD =  sdf.format(cTodaysDate.getTime());

    //if requested date is less than todays date, set REQUESTED_DELIVERY_DATE_HAS_PASSED message
    if (sRequestedDeliveryDateYYYYMMDD.compareTo(sTodaysDateYYYYMMDD) < 0 )
    {
      toReturn = false;
      addValidationResult( ValidationLevels.WARNING, ValidationRules.REQUESTED_DELIVERY_DATE_HAS_PASSED, null);
      if ( logger.isDebugEnabled() )
        logger.debug("Requested date is prior to today's date");
    }

    return toReturn;

  }



}