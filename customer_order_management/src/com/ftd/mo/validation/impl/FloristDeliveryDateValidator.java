package com.ftd.mo.validation.impl;

import com.ftd.customerordermanagement.util.COMDeliveryDateUtil;
import com.ftd.mo.validation.ValidationLevels;
import com.ftd.mo.validation.ValidationRules;
import com.ftd.mo.vo.DeliveryInfoVO;
import com.ftd.ftdutilities.IsDomesticPredicate;
import com.ftd.osp.utilities.plugins.Logger;
import java.sql.Connection;

import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang.StringUtils;

/**
 * Performs Florist specific validation.
 *
 * @author Mark Moon, Software Architects Inc.
 * @version $Revision: 1.2 $
 */
public class FloristDeliveryDateValidator
    extends BaseDeliveryDateValidator {

  private static final Logger logger = new Logger( FloristDeliveryDateValidator.class.getName() );

  /**
   * Creates a new FloristDeliveryDateValidator.
   * @param deliveryInfoVO The DeliveryInfoVO object to validate
   */
  protected FloristDeliveryDateValidator(DeliveryInfoVO deliveryInfoVO, Connection connection) {
    super(deliveryInfoVO, connection);
  }

  /**
   * Performs all validation rules for florist validation.
   * @return true if all validation is successful, otherwise false.
   */
  public boolean validate()
      throws Exception {

    boolean toReturn = super.validate();
    if ( continueValidation ) toReturn = validateExoticProductAndSundayDelivery() && toReturn;
    if ( continueValidation ) toReturn = validateExoticProductAndSameDayDelivery() && toReturn;
    if ( continueValidation ) toReturn = validateInternationalProductAndSameDayDelivery() && toReturn;
    if ( continueValidation ) toReturn = validateDeliveryDateRestrictions() && toReturn;
    if ( continueValidation ) toReturn = validateDeliveryDatePriorToZipInGNADD() && toReturn;
    return toReturn;
  }

  /*
   * Exotic product and sunday delivery is invalid.
   */
  protected boolean validateExoticProductAndSundayDelivery() {
    boolean toReturn = true;
    boolean isSunday = deliveryInfoVO.getDeliveryDate().get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY;
    boolean isExoticProduct = StringUtils.equals( oeDeliveryDateParameters.getProductSubType(), "EXOTIC" );

    if ( isExoticProduct && isSunday ) {
      addValidationResult( ValidationLevels.WARNING, ValidationRules.SUNDAY_DELIVERY_NOT_ALLOWED_FOR_EXOTIC, null);
      toReturn = false;
      if ( logger.isDebugEnabled() ) logger.debug("Invalid delivery date: Sunday delivery AND exotic product");
    }
    return toReturn;
  }

  /*
   * Exotic product and same day delivery is invalid.
   */
  protected boolean validateExoticProductAndSameDayDelivery() {
    boolean toReturn = true;
    boolean isSameDayDelivery = COMDeliveryDateUtil.isSameDay( deliveryInfoVO.getDeliveryDate(), Calendar.getInstance());
    boolean isExoticProduct = StringUtils.equals( oeDeliveryDateParameters.getProductSubType(), "EXOTIC" );

    if ( isExoticProduct && isSameDayDelivery ) {
      addValidationResult( ValidationLevels.WARNING, ValidationRules.SAME_DAY_EXOTIC_UNAVAILABLE, null);
      toReturn = false;
      if ( logger.isDebugEnabled() ) logger.debug("Invalid delivery date: Same day delivery (SD) AND exotic product");
    }
    return toReturn;
  }

  /*
   * International product and same day delivery is invalid.
   */
  protected boolean validateInternationalProductAndSameDayDelivery() {
    boolean toReturn = true;
    boolean isSameDayDelivery = COMDeliveryDateUtil.isSameDay( deliveryInfoVO.getDeliveryDate(), Calendar.getInstance());
    boolean isInternationalProduct = StringUtils.equals( oeDeliveryDateParameters.getDeliveryType(), "I" );
    boolean isInternationalDelivery = !IsDomesticPredicate.getInstance().evaluate(deliveryInfoVO.getRecipientCountry());

    if ( isInternationalProduct && isInternationalDelivery && isSameDayDelivery ) {
      addValidationResult( ValidationLevels.WARNING, ValidationRules.SAME_DAY_DELIVERY_FOR_INTERNATIONAL_PRODUCTS, null);
      toReturn = false;
      if ( logger.isDebugEnabled() ) logger.debug("Invalid delivery date: Same day delivery (SD) AND international product");
    }
    return toReturn;
  }

  /*
   * Selected delivery dates that conflict with product restrictions are invalid.
   */
  protected boolean validateDeliveryDateRestrictions() {
    boolean toReturn = true;

    if ( StringUtils.equals(oeDeliveryDateParameters.getExceptionCode(), "U") ) {
      Date selectedDeliveryDate = deliveryInfoVO.getDeliveryDate().getTime();
      Date productExceptionFromDate = oeDeliveryDateParameters.getExceptionFrom();
      Date productExceptionToDate = oeDeliveryDateParameters.getExceptionTo();

      if ( COMDeliveryDateUtil.getDiffInDays(productExceptionFromDate, selectedDeliveryDate) > -1 &&
            COMDeliveryDateUtil.getDiffInDays(productExceptionToDate, selectedDeliveryDate) < 1 ) {

        addValidationResult( ValidationLevels.WARNING, ValidationRules.PRODUCT_DELIVERY_DATE_RESTRICTIONS, null);
        toReturn = false;
        if ( logger.isDebugEnabled() ) logger.debug("Invalid delivery date: delivery date conflicts with product restrictions");
      }
    }
    return toReturn;
  }

  /*
   * Selected delivery date prior to the GNADD date and the zip / postal code is in GNADD is invalid.
   */
  protected boolean validateDeliveryDatePriorToZipInGNADD() {
    boolean toReturn = true;
    boolean isZipGnaddActive = StringUtils.equals( oeDeliveryDateParameters.getZipCodeGNADDFlag(), "Y" );
    boolean isGnaddActive = !StringUtils.equals( oeDeliveryDateParameters.getGlobalParms().getGNADDLevel(), "0" ); // GNADD active when not zero
    Date gnaddDate= oeDeliveryDateParameters.getGlobalParms().getGNADDDate();
    Date selectedDeliveryDate = deliveryInfoVO.getDeliveryDate().getTime();

    if ( isZipGnaddActive && isGnaddActive ) {
      if ( gnaddDate != null ) {
        if ( selectedDeliveryDate.before(gnaddDate) ) {
          addValidationResult( ValidationLevels.WARNING, ValidationRules.ZIP_IN_GNADD, null);
          toReturn = false;
          if ( logger.isDebugEnabled() ) logger.debug("Invalid delivery date: selected delivery date prior to GNADD and zip/postal code in GNADD");
        }
      }
    }
    return toReturn;
  }
}