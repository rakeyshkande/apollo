package com.ftd.mo.validation.impl;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.mo.validation.ValidationRules;
import com.ftd.mo.validation.ValidationLevels;
import com.ftd.mo.vo.DeliveryInfoVO;
import com.ftd.ftdutilities.DeliveryDateUTIL;
import com.ftd.ftdutilities.ShippingMethod;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.ftdutilities.OEDeliveryDate;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.List;
import java.sql.Connection;

/**
 * Performs Vendor specific validation.
 *
 * @author Mark Moon, Software Architects Inc.
 */
public class VendorDeliveryDateValidator
    extends BaseDeliveryDateValidator {

  private static final Logger logger = new Logger( VendorDeliveryDateValidator.class.getName() );

  /**
   * Creates a new VendorDeliveryDateValidator.
   * @param deliveryInfoVO The DeliveryInfoVO object to validate
   */
  protected VendorDeliveryDateValidator(DeliveryInfoVO deliveryInfoVO, Connection connection) {
    super(deliveryInfoVO, connection);
  }

  /**
   * Performs all validation rules for vendor validation.
   * @return true if all validation is successful, otherwise false.
   */
  public boolean validate()
      throws Exception {

    boolean toReturn = super.validate();
    if ( continueValidation ) toReturn = validateProdStateExcForDeliveryDaySelected() && toReturn;
    if ( continueValidation ) toReturn = validateProdStateExcForEntireWeek() && toReturn;
    if ( continueValidation ) toReturn = validateDeliveryOption() && toReturn;
    if ( continueValidation ) toReturn = validateTwoDayShippingForAlaskaHawaii() && toReturn;

    return toReturn;
  }


  /*
   * This method determines if the product cannot be delivered to the specified state for the passed in
   * day of the week. (PRODUCT_EXCLUDED_STATES table in FTD_APPS)
   */
  protected boolean validateProdStateExcForDeliveryDaySelected() {
    boolean toReturn = true;
    Calendar selectedDeliveryCal = deliveryInfoVO.getDeliveryDate();

    //if isStateDAYExclusion() is true, then its not available for that day
    switch( selectedDeliveryCal.get(Calendar.DAY_OF_WEEK) ) {
      case(Calendar.SUNDAY):
        toReturn = !oeDeliveryDateParameters.isStateSunExclusion();
        break;
      case(Calendar.MONDAY):
        toReturn = !oeDeliveryDateParameters.isStateMonExclusion();
        break;
      case(Calendar.TUESDAY):
        toReturn = !oeDeliveryDateParameters.isStateTueExclusion();
        break;
      case(Calendar.WEDNESDAY):
        toReturn = !oeDeliveryDateParameters.isStateWedExclusion();
        break;
      case(Calendar.THURSDAY):
        toReturn = !oeDeliveryDateParameters.isStateThrExclusion();
        break;
      case(Calendar.FRIDAY):
        toReturn = !oeDeliveryDateParameters.isStateFriExclusion();
        break;
      case(Calendar.SATURDAY):
        toReturn = !oeDeliveryDateParameters.isStateSatExclusion();
        break;
    }

    if ( !toReturn ) {
      addValidationResult( ValidationLevels.WARNING, ValidationRules.PRODUCT_STATE_AGRICULTURAL_RESTRICATIONS_FOR_DELIVERY_DAY_SELECTED, null);
      if ( logger.isDebugEnabled() ) logger.debug("Invalid delivery date: selected delivery date prior to GNADD and zip/postal code in GNADD");
    }

    return toReturn;
  }



  /*
   * This method determines if the product cannot be delivered to the specified state for the passed in
   * day of the week. (PRODUCT_EXCLUDED_STATES table in FTD_APPS)
   */
  protected boolean validateProdStateExcForEntireWeek() {

    boolean availableAtLeastOneDay = true;

    //if isStateDAYExclusion() is true, then its not available for that day
    //thus if one day is not excluded, its available for partial.
    if (oeDeliveryDateParameters.isStateSunExclusion()      &&
        oeDeliveryDateParameters.isStateMonExclusion()      &&
        oeDeliveryDateParameters.isStateTueExclusion()      &&
        oeDeliveryDateParameters.isStateWedExclusion()      &&
        oeDeliveryDateParameters.isStateThrExclusion()      &&
        oeDeliveryDateParameters.isStateFriExclusion()      &&
        oeDeliveryDateParameters.isStateSatExclusion()
       )
    {
      availableAtLeastOneDay = false;
    }


    if ( !availableAtLeastOneDay ) {
      addValidationResult( ValidationLevels.WARNING, ValidationRules.PRODUCT_STATE_AGRICULTURAL_RESTRICATIONS_FOR_ENTIRE_WEEK, null);
      if ( logger.isDebugEnabled() ) logger.debug("Product unavailable for the entire week");
    }

    return availableAtLeastOneDay;
  }




  /*
   * validate the delivery option
   */
  protected boolean validateDeliveryOption() throws Exception {
    boolean toReturn = true;
    boolean shipMethodFound = false;
    boolean deliveryDateFound = false;
    OEDeliveryDate oeDeliveryDate = null;


		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
    String sDeliveryDateSelected = sdf.format(deliveryInfoVO.getDeliveryDate().getTime());
    String shipMethodSelected = deliveryInfoVO.getShipMethod();
    java.util.Date dDeliveryDateSelected = deliveryInfoVO.getDeliveryDate().getTime();
    String oeDeliveryDateSelected = null;
    Calendar cCurrentDate = Calendar.getInstance();
    java.util.Date currentDate = cCurrentDate.getTime();

    HashMap shippingMethods = (HashMap) oeDeliveryDateParameters.getShipMethods();
    Set ks = shippingMethods.keySet();
    Iterator iter = ks.iterator();
    String key;

    while(iter.hasNext())
    {
      key = iter.next().toString();
      ShippingMethod shippingMethod = (ShippingMethod)shippingMethods.get(key);
      if( ( shippingMethod.getCode() != null &&
            deliveryInfoVO.getShipMethod() != null &&
            shippingMethod.getCode().equalsIgnoreCase(deliveryInfoVO.getShipMethod())
          ) ||
          ( ( shipMethodSelected == null                                                          ||
              shipMethodSelected.equalsIgnoreCase("")                                             ||
              shipMethodSelected.equalsIgnoreCase(COMConstants.CONS_DELIVERY_SAME_DAY_CODE)       ||
              shipMethodSelected.equalsIgnoreCase(COMConstants.CONS_DELIVERY_NEXT_DAY_CODE)       ||
              shipMethodSelected.equalsIgnoreCase(COMConstants.CONS_DELIVERY_FLORIST_CODE))
            &&
            ( shippingMethod.getCode() == null                                                    ||
              shippingMethod.getCode().equalsIgnoreCase("")                                       ||
              shippingMethod.getCode().equalsIgnoreCase(COMConstants.CONS_DELIVERY_SAME_DAY_CODE) ||
              shippingMethod.getCode().equalsIgnoreCase(COMConstants.CONS_DELIVERY_NEXT_DAY_CODE) ||
              shippingMethod.getCode().equalsIgnoreCase(COMConstants.CONS_DELIVERY_FLORIST_CODE))
          )
        )
      {
        shipMethodFound = true;
        break;
      }
    }

    DeliveryDateUTIL dateUtil = new DeliveryDateUTIL();
    List dates;
    int datesTotal;
    if (shipMethodFound)
    {
      dates = dateUtil.getOrderProductDeliveryDates( oeDeliveryDateParameters );

      datesTotal = dates.size();

      for (int i = 0; i < datesTotal; i++)
      {
        oeDeliveryDate = (OEDeliveryDate) dates.get(i);
        oeDeliveryDateSelected = oeDeliveryDate.getDeliveryDate();
        if (sDeliveryDateSelected.equalsIgnoreCase(oeDeliveryDateSelected))
        {
          deliveryDateFound = true;
          break;
        }
      }
    }

    long diff = 0;
    diff = dDeliveryDateSelected.getTime() - currentDate .getTime();

    //if not found, and the delivery date >= current date, and only minDays were sought, get all 120
    //days.
    if (shipMethodFound && !deliveryDateFound && diff > 0 && deliveryDateParameters.getMinDaysOut())
    {
      deliveryDateParameters.setMinDaysOut(false);

      oeDeliveryDateParameters = DeliveryDateUTIL.getCOMParameterData(
                            deliveryDateParameters.getRecipientCountry(),
                            deliveryDateParameters.getProductId(),
                            deliveryDateParameters.getProductSubCodeId(),
                            deliveryDateParameters.getRecipientZipCode(),
                            deliveryDateParameters.getCanadaCountryCode(),
                            deliveryDateParameters.getUsCountryCode(),
                            "1",
                            deliveryDateParameters.getOccasionId(),
                            this.con,
                            deliveryDateParameters.getRecipientState(),
                            deliveryDateParameters.getOrderOrigin(),
                            deliveryDateParameters.isFlorist(),
                            deliveryDateParameters.getMinDaysOut()
                          );

      dates = dateUtil.getOrderProductDeliveryDates( oeDeliveryDateParameters );

      datesTotal = dates.size();

      for (int i = 0; i < datesTotal; i++)
      {
        oeDeliveryDate = (OEDeliveryDate) dates.get(i);
        oeDeliveryDateSelected = oeDeliveryDate.getDeliveryDate();
        if (sDeliveryDateSelected.equalsIgnoreCase(oeDeliveryDateSelected))
        {
          deliveryDateFound = true;
          break;
        }
      }
    }


    //12/1/05  Added validation to make sure ship method is valid for item and date
    if (deliveryDateFound)
    {
      //if can deliver on date
      if (oeDeliveryDate.getDeliverableFlag().equalsIgnoreCase("Y"))
      {
//Ali Lakhani -- Ed, I have already added logic as such above.  Pleae take a look at that before
//               uncommenting the following.

  /*COMMENTING OUT DELIVERY OPTION NOT ALLOWED UNTIL MUELLER FIGURES THINGS OUT

            //check if ship method is valid
            List shipMethods = currentDate.getShippingMethods();
            Iterator methodIter = shipMethods.iterator();
            boolean validShipMethod = false;
            while(methodIter.hasNext())
            {
                ShippingMethod shippingMethod = (ShippingMethod)methodIter.next();
                if(shippingMethod.getCode().equals(deliveryInfoVO.getShipMethod()))
                {
                    validShipMethod = true;
                }
            }

            //check if not found
            if(!validShipMethod)
            {
                  toReturn = false;
                  addValidationResult( ValidationLevels.WARNING, ValidationRules.DELIVERY_SHIP_METHOD_NOT_ALLOWED, null);
                  if ( logger.isDebugEnabled() ) logger.debug("Ship method not valid");
            }
*/
      }
      else
      {
          toReturn = false;
          addValidationResult( ValidationLevels.WARNING, ValidationRules.DELIVERY_OPTION_NOT_ALLOWED, null);
          if ( logger.isDebugEnabled() ) logger.debug("Invalid delivery date/day selected");
      }
    }
    else
    {
          toReturn = false;
          addValidationResult( ValidationLevels.WARNING, ValidationRules.DELIVERY_OPTION_NOT_ALLOWED, null);
          if ( logger.isDebugEnabled() ) logger.debug("Invalid delivery date/day selected");
    }



    return toReturn;
  }


 protected boolean validateTwoDayShippingForAlaskaHawaii() throws Exception {

  boolean toReturn = true;
  if (deliveryInfoVO.getShipMethod()!= null)
  {
    if ((deliveryInfoVO.getRecipientState().equalsIgnoreCase(COMConstants.CONS_STATE_CODE_ALASKA)||
        deliveryInfoVO.getRecipientState().equalsIgnoreCase(COMConstants.CONS_STATE_CODE_HAWAII)) &&
        !deliveryInfoVO.getShipMethod().equalsIgnoreCase(COMConstants.CONS_DELIVERY_TWO_DAY_CODE))
        {
          addValidationResult( ValidationLevels.WARNING, ValidationRules.TWO_DAY_SHIPPING_REQUIRED_FOR_AK_HI, null);
          if ( logger.isDebugEnabled() ) logger.debug("Invalid delivery method selected for AK & HI deliveries.");
          toReturn = false;
        }
  }
   return toReturn;
 }










}