package com.ftd.mo.validation.impl;

import com.ftd.mo.validation.AbstractValidator;
import com.ftd.mo.validation.ValidationRules;
import com.ftd.mo.validation.ValidationLevels;
import com.ftd.mo.vo.DeliveryInfoVO;
import com.ftd.osp.utilities.plugins.Logger;

import java.util.Calendar;
import java.sql.Connection;

import org.apache.commons.lang.StringUtils;

/**
 * BaseDeliveredValidator provides common method which may be useful
 * for implementing DeliveredValidators.
 */
public class BaseDeliveredValidator
    extends AbstractValidator {

  private static final Logger logger = new Logger( BaseDeliveredValidator.class.getName() );

  protected DeliveryInfoVO divo;
  protected Connection con;

  /**
   * Creates a new BaseDeliveryValidator.
   */
  protected BaseDeliveredValidator(DeliveryInfoVO a_divo, Connection a_con) {
    super();
    this.divo = a_divo;
    this.con  = a_con;
  }

  /**
   * Any common/shared validation between all DeliveryValidators can be placed here.
   * @return true if valid, otherwise false
   */
  public boolean validate()
      throws Exception {

    boolean toReturn = true;

    // Currently there is nothing in common to validate

    return toReturn;
  }

}