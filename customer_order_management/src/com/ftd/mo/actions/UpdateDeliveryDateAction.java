package com.ftd.mo.actions;

import java.io.File;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.messaging.vo.MessageOrderStatusVO;
import com.ftd.mo.bo.UpdateDeliveryDateBO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;




public final class UpdateDeliveryDateAction extends Action
{
    private static Logger logger  = new Logger("com.ftd.mo.actions.UpdateDeliveryDateAction");

  /**
   * This is the main action called from the Struts framework.
   * @param mapping The ActionMapping used to select this instance.
   * @param form The optional ActionForm bean for this request.
   * @param request The HTTP Request we are processing.
   * @param response The HTTP Response we are processing.
   */
  public  ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                                HttpServletResponse response)

  {
    ActionForward forward = null;
    Connection con = null;
    String action = null;

    try
    {
      String xslName = "";
      String requestedSizeDescription = null;
      
      //Connection/Database info
      con = DataSourceUtil.getInstance().getConnection(
            ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE,
            COMConstants.DATASOURCE_NAME));


      //Document that will contain the final XML, to be passed to the TransUtil and XSL page
      Document responseDocument = (Document) DOMUtil.getDocument();

      //This Hashmap will be used to hold all the XML objects, page data, and search criteria that
      //will be returned from the business object
      HashMap responseHash = new HashMap();

      //Call the Business Object, which will return a hashmap containing
      //    0 to Many - XML documents
      //    HashMap1  = pageData - hashmap that contains String data at page level
      UpdateDeliveryDateBO uddBO = new UpdateDeliveryDateBO(request, con, mapping);
      responseHash = uddBO.processRequest();

      //After the Business Object returns control back, we need to populate the XML document that
      //will be passed to the Transform Utility.  This entails:
      //  1)  append all the XML documents from the responseHash to this returnDocument
      //  2)  convert pageData in responseHash (from HashMap to Document), and append
      //      to returnDocument

      //Get all the keys in the hashmap returned from the business object
      Set ks = responseHash.keySet();
      Iterator iter = ks.iterator();
      String key;

      //Iterate thru the hashmap returned from the business object using the keyset
      while(iter.hasNext())
      {
        key = iter.next().toString();
        if (key.equalsIgnoreCase("pageData"))
        {
          //The page data hashmap within the main hashmap contains an object whose key is "XSL "
          //This key refers to the page where the control will be transferred to.  However, this key
          //should not be included in the page data XML
          xslName = (String)((HashMap) responseHash.get("pageData")).get("XSL").toString();
          //requestedSizeDescription = (String)((HashMap) responseHash.get("pageData")).get("requested_size_description");
         // if (StringUtils.isEmpty(requestedSizeDescription))
         //   requestedSizeDescription = "standard";
          ((HashMap)responseHash.get("pageData")).remove("XSL");

          //Convert the page data hashmap to XML and append it to the final XML
          DOMUtil.addSection(responseDocument, "pageData", "data",
                              (HashMap)responseHash.get("pageData"), true);
        }
        else
        {
          //Append all the existing XMLs to the final XML
          Document xmlDoc = (Document) responseHash.get(key);
          if (xmlDoc != null)
          {
            NodeList nl = xmlDoc.getChildNodes();
            DOMUtil.addSection(responseDocument, nl);
          }
        }
      }

      //for local development, uncomment
     // responseDocument.print(System.out);

      //check if we have to forward to another action, or just transform the XSL.

       if (xslName.endsWith("Action"))
       {
         forward = mapping.findForward(xslName);
         
         if(xslName.equalsIgnoreCase(COMConstants.XSL_UDD_CUSTOMER_SEARCH_ACTION))
         {
           forward =  
             bulidParameterizedForward(forward, (HashMap)responseHash.get("pageData"), request);

         }
         request.setAttribute(COMConstants.PD_UDD_ERROR, (HashMap)responseHash.get("pageData"));
         return forward;
       }
      else if (xslName.equalsIgnoreCase("forward"))
      {
        //get forward from map
        forward = (ActionForward)((HashMap) responseHash.get("pageData")).get("forward");

        //get attributes map
        HashMap pageDataMap = (HashMap)responseHash.get("pageData");
        HashMap attributes = null;
        if(pageDataMap.get("attributes") != null)
        {
           attributes = (HashMap)pageDataMap.get("attributes");
           Set attributeSet = attributes.keySet();    
           Iterator attributeIter = attributeSet.iterator();
           while(attributeIter.hasNext())
           {
               String attributeName = (String)attributeIter.next();
               MessageOrderStatusVO attributeValue = (MessageOrderStatusVO)attributes.get(attributeName);
               request.setAttribute(attributeName, attributeValue);
           }
        }
   
        Object obj = ((HashMap) responseHash.get("pageData")).get("error_message")  ;
        if(obj != null)
        {
            request.setAttribute("error_messages", (Document)obj);              
        }

        return forward;
      }

      else
      {
        //Get XSL File name
        File xslFile = getXSL(xslName, mapping);

        forward = mapping.findForward(xslName);
        String xslFilePathAndName = forward.getPath(); //get real file name
        
        // Change to client side transform
        TraxUtil.getInstance().getInstance().transform(request, response, responseDocument, 
                            xslFile, xslFilePathAndName, (HashMap)request.getAttribute(COMConstants.CONS_APP_PARAMETERS));

        return null;
      }

    }

    catch(Exception e)
    {
      //if logging is enabled, display the customer id, and external and master order #s
      if(logger.isDebugEnabled())
      {
        logger.debug("Update Delivery Date Action --- Order Detail Id = " +
                      request.getParameter(COMConstants.ORDER_DETAIL_ID));
      }

      logger.error(e);
      forward = mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
      return forward;
    }
    catch(Throwable t)
    {
      logger.error(t);
      forward = mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
      return forward;
    }

    finally
    {
      try
      {
        //Close the connection
        if(con != null)
        {
          con.close();
        }
      }
      catch(Exception e)
      {
        logger.error(e);
        forward = mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
        return forward;
      }
    }

  }



  /******************************************************************************
  *             getRequestInfo(HttpServletRequest request)
  *******************************************************************************
  * Retrieve the info from the request object, and set class level variables
  * @param  HttpServletRequest
  * @return none
  * @throws none
  */
  private String getRequestInfo(HttpServletRequest request)
  {
    String action = null;

    if(request.getAttribute(COMConstants.ACTION) != null)
      action = request.getAttribute(COMConstants.ACTION).toString();
    else if(request.getParameter(COMConstants.ACTION)!=null)
      action = request.getParameter(COMConstants.ACTION);

    return action;
  }



  /******************************************************************************
  *                                     getXSL()
  *******************************************************************************
  * Retrieve name of Customer Order Search Action XSL file
  * @param1 String - the xsl name returned from the Business Object
  * @param2 ActionMapping
  * @return File - XSL File name
  * @throws none
  */

  private File getXSL(String xslName, ActionMapping mapping)
  {

    File xslFile = null;
    String xslFilePathAndName = null;

    ActionForward forward = mapping.findForward(xslName);
    xslFilePathAndName = forward.getPath();                //get real file name
    xslFile = new File(this.getServlet().getServletContext().getRealPath(xslFilePathAndName));
    return xslFile;
  }


  private ActionForward bulidParameterizedForward(ActionForward forward, HashMap map, HttpServletRequest req)
  {
    String path = forward.getPath() 
      + "?" + "action=search"
      + "&" + "in_order_number=" + (String) map.get(COMConstants.PD_MO_EXTERNAL_ORDER_NUMBER)
      + "&" + "recipient_flag=y";
    
      forward = new ActionForward(path,false); 
      
      return  forward; 
  }
  

}