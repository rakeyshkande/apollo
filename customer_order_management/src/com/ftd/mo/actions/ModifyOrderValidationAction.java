package com.ftd.mo.actions;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.dao.OrderDAO;
import com.ftd.customerordermanagement.dao.PaymentDAO;
import com.ftd.customerordermanagement.dao.RefundDAO;
import com.ftd.customerordermanagement.vo.PaymentVO;
import com.ftd.messaging.util.MessageUtil;
import com.ftd.messaging.vo.FTDMessageVO;
import com.ftd.mo.dao.UpdateOrderDAO;
import com.ftd.mo.util.ModifyOrderUTIL;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityManager;

import java.io.File;
import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;


/**
 * LockAction class
 *
 */

public final class ModifyOrderValidationAction extends Action
{
    private static Logger logger  = new Logger("com.ftd.mo.actions.ModifyOrderValidationAction");


    //Document that will contain the final XML, to be passed to the TransUtil and XSL page
    private Document responseDocument;

  /**
   * This is the main action called from the Struts framework.
   *
   * @param mapping The ActionMapping used to select this instance.
   * @param form The optional ActionForm bean for this request.
   * @param request The HTTP Request we are processing.
   * @param response The HTTP Response we are processing.
   */
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
      Connection conn = null;
      HashMap pageData = new HashMap();

      try
      {
        this.responseDocument = (Document) DOMUtil.getDocument();

        // Set request variables
        String orderDetailId = (String) request.getParameter(COMConstants.ORDER_DETAIL_ID);
        String externalOrderNumber = (String) request.getParameter(COMConstants.EXTERNAL_ORDER_NUMBER);
        String vendorFlag = (String) request.getParameter(COMConstants.VENDOR_FLAG);
        String productId = (String) request.getParameter(COMConstants.PRODUCT_ID);
        String partnerName = (String) request.getParameter(COMConstants.PD_MO_PARTNER_NAME);
        String mercuryFlag = (String) request.getParameter("mercury_flag");
        String floristId = (String) request.getParameter("florist_id");
        
        String messageType = (vendorFlag != null && vendorFlag.equals("Y")) ? "Venus" : "Mercury";

        //retrieve the session id and user id
        String securityToken = (String) request.getParameter(COMConstants.SEC_TOKEN);
        String userId = SecurityManager.getInstance().getUserInfo(securityToken).getUserID();

        // Pull DB connection
        conn = DataSourceUtil.getInstance().getConnection(ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, COMConstants.DATASOURCE_NAME));

        if( orderDetailId != null)
        {
          String securityFlag = ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, COMConstants.SECURITY_IS_ON);
          if(securityFlag != null && securityFlag.equals("true"))
          {
            if (SecurityManager.getInstance().getUserInfo(securityToken) != null)
            {

              // Instantiate OrderDAO
              OrderDAO orderDAO = new OrderDAO(conn);
              // Instantiate UpdateOrderDAO
              UpdateOrderDAO uoDAO = new UpdateOrderDAO(conn);
              // Instantiate RefundDAO
              RefundDAO refundDAO = new RefundDAO(conn);
             
              CachedResultSet orderDetailsInfo = orderDAO.getOrderDetailsInfo(orderDetailId);
              String orderFullyRefundIndicator = refundDAO.isOrderFullyRefundedCRS(orderDetailId);

              CachedResultSet mercuryOrderMessageStatusInfo = uoDAO.getMercuryOrderMessageStatus(orderDetailId, messageType, "N");
              String flowerMonthlyIndicator = uoDAO.isFlowerMonthlyProduct(productId);
              
              String ftdMsgAttemptedNotVer = uoDAO.getMercuryAttemptedStatus(orderDetailId);
              String orderHasBeenModified = uoDAO.hasOrderBeenModified(externalOrderNumber);
              String orderInFinalState = uoDAO.isCartInFinalState(orderDetailId);
              
              ModifyOrderUTIL moUTIL = new ModifyOrderUTIL(conn);
              FTDMessageVO lastFtdMessageVO = moUTIL.getMessageDetail(orderDetailId);
              boolean zjOrderInTransit = moUTIL.isZJOrderInTransit(orderDetailId, lastFtdMessageVO);
              String sdsStatus = lastFtdMessageVO.getSdsStatus();
              Date dDeliveryDate = lastFtdMessageVO.getDeliveryDate();
              Date dShipDate = lastFtdMessageVO.getShipDate();

              String attemptedFTD = null;
              String cancelSent = null;
              String ftdStatus = null;
              String hasLiveFTD = null;
              String holdReason = null;
              String orderCancelledIndicator = null;
              String orderInCreditQ = null;
              String rejectSent = null;
              boolean validationPassed = true;
                            
              //populate strings from the query results
              if(orderDetailsInfo.next())
              {
                orderInCreditQ = orderDetailsInfo.getString("order_in_credit_q");
                holdReason = orderDetailsInfo.getString("hold_reason");
                orderCancelledIndicator = orderDetailsInfo.getString("cancelled_flag");
              }

              //populate mercury order message status from the mercury status query results
              if(mercuryOrderMessageStatusInfo.next())
              {
                if(mercuryOrderMessageStatusInfo.getString("reject_sent") != null)
                  rejectSent = mercuryOrderMessageStatusInfo.getString("reject_sent").trim();
                if(mercuryOrderMessageStatusInfo.getString("cancel_sent") != null)
                  cancelSent = mercuryOrderMessageStatusInfo.getString("cancel_sent").trim();
                if(mercuryOrderMessageStatusInfo.getString("attempted_ftd") != null)
                  attemptedFTD = mercuryOrderMessageStatusInfo.getString("attempted_ftd").trim();
                if(mercuryOrderMessageStatusInfo.getString("has_live_ftd") != null)
                  hasLiveFTD = mercuryOrderMessageStatusInfo.getString("has_live_ftd").trim();
                if(mercuryOrderMessageStatusInfo.getString("ftd_status") != null)
                  ftdStatus = mercuryOrderMessageStatusInfo.getString("ftd_status").trim();
            
              }
              
              /**********************************************************************************
               * -----Validation Rule ???-----
               * Issue 1589 - CSR(s) were able to re-modify an order.  This happened because the
               * CSR hit the Update Order button again prior to a CAN being sent. 
               * 
               * If an order has been modified, do NOT allow a CSR to hit the Update Order button
               * again
               **********************************************************************************/
              if (validationPassed &&
                  orderHasBeenModified != null &&
                  orderHasBeenModified.equalsIgnoreCase(COMConstants.CONS_YES))
              {
            	  
            	  if(!partnerName.isEmpty() && partnerName != null){
            		  pageData.put(COMConstants.PD_MESSAGE_DISPLAY,   COMConstants.PARTNER_ORDER_ALREADY_MODIFIED);
            	  }
            	  else{
            		  pageData.put(COMConstants.PD_MESSAGE_DISPLAY,   COMConstants.ORDER_ALREADY_MODIFIED);
            	  }
               
                pageData.put("success",                         COMConstants.CONS_NO);
                validationPassed = false;
              }
              
              /**********************************************************************************
               * -----Validation Rule -----
               *                 
               * Defect 2638 - Zone Jump
               * Do not allow csr to do anything to the order once a Zone Jump label is printed
               * and it is after the zone jump label date
               **********************************************************************************/
               if (validationPassed && zjOrderInTransit)
               {
                 pageData.put(COMConstants.PD_MESSAGE_DISPLAY,   COMConstants.ZJ_ORDER_IN_TRANSIT);
                 pageData.put("success",                         COMConstants.CONS_NO);
                 validationPassed = false;
               }
              /**********************************************************************************
               * -----Validation Rule ???-----
               * This was covered in "Modify Order Change Request 001.doc" 
               * 
               * All FOTM orders should be handled manually by the special accounts team, until 
               * an automated solution for handling FOTM orders has been established.
               **********************************************************************************/
              if (validationPassed &&
                  flowerMonthlyIndicator != null &&
                  flowerMonthlyIndicator.equalsIgnoreCase(COMConstants.CONS_YES))
              {
                pageData.put(COMConstants.PD_MESSAGE_DISPLAY,   COMConstants.FLOWER_MONTHLY_PRODUCT);
                pageData.put("success",                         COMConstants.CONS_NO);
                validationPassed = false;
              }
              
               /**********************************************************************************
               * -----Validation Rule 2.10-----
               * 
               * The system will not allow FTDM type orders to by updated through the 
               * Update Order application.  Please note this check only applies to florist orders.
               **********************************************************************************/
              if (validationPassed &&
                  floristId != null && 
                  !floristId.equals("") && 
                  vendorFlag != null && 
                  vendorFlag.equals("N"))
              {
                if(mercuryFlag == null || mercuryFlag.equals(""))
                {
                  pageData.put(COMConstants.PD_MESSAGE_DISPLAY,   COMConstants.FTDM_ORDER);
                  pageData.put("success",                         COMConstants.CONS_NO);
                  validationPassed= false;
                }
              }

              /**********************************************************************************
               * -----Validation Rule 2.15-----
               * The system will not allow orders that have been fully refunded and cancelled to be
               * updated through the Update order application. 
               **********************************************************************************/
              if (validationPassed &&
                  orderFullyRefundIndicator != null &&
                  orderFullyRefundIndicator.equalsIgnoreCase(COMConstants.CONS_YES) &&
                  orderCancelledIndicator != null &&
                  orderCancelledIndicator.equalsIgnoreCase(COMConstants.CONS_YES))

              {
                pageData.put(COMConstants.PD_MESSAGE_DISPLAY,   COMConstants.ORDER_CANCELLED_AND_REFUNDED);
                pageData.put("success",                         COMConstants.CONS_NO);
                validationPassed= false;

              }
              /**********************************************************************************
               * -----Validation Rule 2.14-----
               * The system will not allow orders that are currently in the Credit queue to be
               * updated through the Update order application
               **********************************************************************************/
              if (validationPassed &&
                  orderInCreditQ != null && orderInCreditQ.equalsIgnoreCase(COMConstants.CONS_YES))
              {
                pageData.put(COMConstants.PD_MESSAGE_DISPLAY,   COMConstants.ORDER_CURRENTLY_IN_CREDIT_QUEUE);
                pageData.put("success",                         COMConstants.CONS_NO);
                validationPassed = false;
              }
              /**********************************************************************************
               * -----Validation Rule 2.11-----
               * The system will not allow non-verified payments to be updated through the Update 
               * order application(should be in Credit Queue)
               **********************************************************************************/
              if (validationPassed &&
                  holdReason != null && holdReason.equalsIgnoreCase(COMConstants.HOLD_REASON_REAUTH))
              {
                pageData.put(COMConstants.PD_MESSAGE_DISPLAY,   COMConstants.PAYMENT_CANNOT_BE_AUTHORIZED);
                pageData.put("success",                         COMConstants.CONS_NO);
                validationPassed= false;
              }
              /**********************************************************************************
               * -----Validation Rule 2.12-----
               * The system will not allow orders where the live mercury message has not been 
               * verified to be updated through the Update order application.
               **********************************************************************************/
              if(validationPassed && messageType.equals("Mercury"))
              {
                if ( (hasLiveFTD != null    && hasLiveFTD.equalsIgnoreCase(COMConstants.CONS_NO) &&
                      attemptedFTD != null  && attemptedFTD.equalsIgnoreCase(COMConstants.CONS_YES) &&
                      rejectSent != null    && rejectSent.equalsIgnoreCase(COMConstants.CONS_NO) && 
                      cancelSent != null    && cancelSent.equalsIgnoreCase(COMConstants.CONS_NO)) 
                      ||
                      (hasLiveFTD != null    && hasLiveFTD.equalsIgnoreCase(COMConstants.CONS_NO) &&
                      attemptedFTD != null  && attemptedFTD.equalsIgnoreCase(COMConstants.CONS_NO) &&
                      rejectSent != null    && rejectSent.equalsIgnoreCase(COMConstants.CONS_NO) && 
                      cancelSent != null    && cancelSent.equalsIgnoreCase(COMConstants.CONS_NO) &&
                      ftdMsgAttemptedNotVer != null && ftdMsgAttemptedNotVer.equalsIgnoreCase(COMConstants.CONS_YES) 
                      )
                    )
                {
                  pageData.put(COMConstants.PD_MESSAGE_DISPLAY,   COMConstants.UNVERIFIED_MERCURY_MESSAGE);
                  pageData.put("success",                         COMConstants.CONS_NO);
                  validationPassed = false;
                }
              }
              
              /**
               *  Is cart in final state check
               */
              logger.debug("####orderInFinalState : "+orderInFinalState);
              if(orderInFinalState !=null && orderInFinalState.equalsIgnoreCase("N")){
            	  pageData.put(COMConstants.PD_MESSAGE_DISPLAY,   COMConstants.CART_NOT_IN_FINAL_STATE_MESSAGE);
                  pageData.put("success",                         COMConstants.CONS_NO);
                  validationPassed = false;
              }
              /**********************************************************************************
               * -----Validation Rule 2.12-----
               * The system will not allow orders where the live mercury message has not been 
               * verified to be updated through the Update order application.
               **********************************************************************************/
              //check to see if the venus ftd is verified
              if(validationPassed && messageType.equals("Venus"))
              {
                if( (hasLiveFTD != null    && hasLiveFTD.equalsIgnoreCase(COMConstants.CONS_YES) &&
                    attemptedFTD != null  && attemptedFTD.equalsIgnoreCase(COMConstants.CONS_YES) && 
                    rejectSent != null    && rejectSent.equalsIgnoreCase(COMConstants.CONS_NO) &&
                    cancelSent != null    && cancelSent.equalsIgnoreCase(COMConstants.CONS_NO) && 
                    ftdStatus != null     && ftdStatus.equalsIgnoreCase("MO") )
                	||
                	(hasLiveFTD != null    && hasLiveFTD.equalsIgnoreCase(COMConstants.CONS_NO) &&
                    attemptedFTD != null  && attemptedFTD.equalsIgnoreCase(COMConstants.CONS_YES) &&
                    rejectSent != null    && rejectSent.equalsIgnoreCase(COMConstants.CONS_NO) && 
                    cancelSent != null    && cancelSent.equalsIgnoreCase(COMConstants.CONS_NO) &&
                    ftdStatus != null     && ftdStatus.equalsIgnoreCase("MO")) )
                {
                  pageData.put(COMConstants.PD_MESSAGE_DISPLAY,   COMConstants.UNVERIFIED_MERCURY_MESSAGE);
                  pageData.put("success",                         COMConstants.CONS_NO);
                  validationPassed = false;
                }
                /**********************************************************************************
                 * -----Validation Rule 2.7-----
                 * The system will not allow Venus orders that have been printed but have not been 
                 * shipped to be updated through the Update order application
                 * Defect 3004:  Change case to be: Cannot modify order if order is in printed
                 * status and is on or after the ship date and before the delivery date 
                 * and not rejected.
                 * Defect 3735:  Need to look at sds status and not order disp code
                 **********************************************************************************/
                else if (sdsStatus != null && sdsStatus.equalsIgnoreCase(COMConstants.SDS_STATUS_PRINTED)
                    && moUTIL.isOrderInTransit(dShipDate, dDeliveryDate)
                    && (hasLiveFTD != null && hasLiveFTD.equalsIgnoreCase(COMConstants.CONS_YES)) 
                )       
                {
                  pageData.put(COMConstants.PD_MESSAGE_DISPLAY,   COMConstants.VENUS_ORDER_PRINTED_STATUS);
                  pageData.put("success",                         COMConstants.CONS_NO);
                  validationPassed = false;
                }
                /**********************************************************************************
                 * -----Validation Rule 2.8-----
                 * The system will not allow shipped Venus orders to be updated through the Update 
                 * order application on or before the delivery date.
                 * Defect 3004:  Change case to be just before the delivery date.  On the delivery 
                 * date is fine.
                 * Defect 3735:  Need to look at sds status and not order disp code
                 **********************************************************************************/
                else if (sdsStatus != null && sdsStatus.equalsIgnoreCase(COMConstants.SDS_STATUS_SHIPPED))
                {
                  if (dDeliveryDate == null)
                  {
                    pageData.put(COMConstants.PD_MESSAGE_DISPLAY,   COMConstants.DELIVERY_DATE_NULL);
                    pageData.put("success",                         COMConstants.CONS_NO);
                    validationPassed = false;
                  }
                  else if (moUTIL.isOrderInTransit(dShipDate, dDeliveryDate))
                  {
                    pageData.put(COMConstants.PD_MESSAGE_DISPLAY,   COMConstants.VENUS_ORDER_SHIPPED_STATUS_DELIVERY_DATE);
                    pageData.put("success",                         COMConstants.CONS_NO);
                    validationPassed = false;
                  }
                }
              }

              
              
              /**********************************************************************************
               * -----Validation Rule if the payment method is Military Start the Order cannot be modified-----              
               **********************************************************************************/
              
              
              if (validationPassed && isMilitartyStartPaymnetType(conn,orderDetailId))
              {
            	  
                pageData.put(COMConstants.PD_MESSAGE_DISPLAY,   COMConstants.PAYMENT_TYPE_MS_ERROR_MSG);
                pageData.put("success",                         COMConstants.CONS_NO);
                validationPassed = false;
              }
      
              /**********************************************************************************
               * If everything has been verified
               **********************************************************************************/
              if (validationPassed)
              {
                pageData.put("success",                         COMConstants.CONS_YES);
              }
            }//end "if (SecurityManager..."
          }//end "if (securityFlag..."
        }//end else if(action.equalsIgnoreCase("retrieve"))

        else
        {
          //set an output message
          String message = "Order Detail Id was missing.";

          pageData.put(COMConstants.PD_MESSAGE_DISPLAY,         message);
          pageData.put("success",                               COMConstants.CONS_NO);
        }

        //Convert the page data hashmap to XML and append it to the final XML
        DOMUtil.addSection(this.responseDocument, "pageData", "data", pageData, true);

        //set the iFrame name to be given control back to.
        String xslName = COMConstants.XSL_MODIFY_ORDER_VALIDATION_IFRAME;

        //Get XSL File name
        File xslFile = getXSL(xslName, mapping);

        ActionForward forward = mapping.findForward(xslName);
        String xslFilePathAndName = forward.getPath(); //get real file name
        
        // Change to client side transform
        TraxUtil.getInstance().getInstance().transform(request, response, responseDocument, 
                            xslFile, xslFilePathAndName, (HashMap)request.getAttribute(COMConstants.CONS_APP_PARAMETERS));

      }//end try


      catch (Throwable ex)
      {
        try
        {
          //if logging is enabled, display the input parms
          if(logger.isDebugEnabled())
          {
            logger.debug("Modify Order Validation Action --- OrderDetailId = " + request.getParameter("action_type"));
          }

          logger.error(ex);

          //set an output message
          String message = "A system exception occurred.  Please contact your system administrator.";

          pageData.put(COMConstants.PD_MESSAGE_DISPLAY,         message);
          pageData.put("success",                               COMConstants.CONS_NO);

          //Convert the page data hashmap to XML and append it to the final XML
          DOMUtil.addSection(this.responseDocument, "pageData", "data", pageData, true);

          //set the iFrame name to be given control back to.
          String xslName = COMConstants.XSL_MODIFY_ORDER_VALIDATION_IFRAME;

          //Get XSL File name
          File xslFile = getXSL(xslName, mapping);

          ActionForward forward = mapping.findForward(xslName);
          String xslFilePathAndName = forward.getPath(); //get real file name
          
          // Change to client side transform
          TraxUtil.getInstance().getInstance().transform(request, response, responseDocument, 
                              xslFile, xslFilePathAndName, (HashMap)request.getAttribute(COMConstants.CONS_APP_PARAMETERS));
        }
        catch (Exception e)
        {
          if(logger.isDebugEnabled())
          {
            logger.debug("Modify Order Validation Action --- An Exception occurred when" +
                          " iFrame was launched to throw the system exception error");
          }
          logger.error(e);
        }
      }
      finally
      {
        try
        {
          conn.close();
        }
        catch (SQLException se)
        {
          logger.error(se);
        }
      }

      return null;
    }


  /******************************************************************************
  *                                     getXSL()
  *******************************************************************************
  * Retrieve name of Customer Order Search Action XSL file
  * @param1 String - the xsl name returned from the Business Object
  * @param2 ActionMapping
  * @return File - XSL File name
  * @throws none
  */

  private File getXSL(String xslName, ActionMapping mapping)
  {

    File xslFile = null;
    String xslFilePathAndName = null;

    ActionForward forward = mapping.findForward(xslName);
    xslFilePathAndName = forward.getPath();                //get real file name
    xslFile = new File(this.getServlet().getServletContext().getRealPath(xslFilePathAndName));
    return xslFile;
  }
  
  
  private boolean isMilitartyStartPaymnetType(Connection conn,String orderDetailId) throws Exception{
	  
	  boolean isMilitaryStarPmt=false;	  
	  PaymentDAO paymentDAO=new PaymentDAO(conn);	  
	  PaymentVO paymentVO=paymentDAO.getCartPaymentCC(orderDetailId);
	  
	  if(paymentVO != null && paymentVO.getPaymentType()!=null ) {
		  isMilitaryStarPmt=  COMConstants.MILITARY_STAR.equalsIgnoreCase(paymentVO.getPaymentType());
	  }
	  
	  return isMilitaryStarPmt;
  }


}