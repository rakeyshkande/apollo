package com.ftd.mo.actions;

import com.ftd.customerordermanagement.bo.GiftCardBO;
import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.dao.OrderDAO;
import com.ftd.customerordermanagement.vo.BillingVO;
import com.ftd.customerordermanagement.vo.CommonCreditCardVO;
import com.ftd.customerordermanagement.vo.CreditCardVO;
import com.ftd.customerordermanagement.vo.GcCouponVO;
import com.ftd.customerordermanagement.vo.GiftCardVO;
import com.ftd.customerordermanagement.vo.PaymentVO;
import com.ftd.ftdutilities.FTDCommonUtils;
import com.ftd.mo.bo.CreateNewOrderBO;
import com.ftd.mo.bo.PaymentBO;
import com.ftd.mo.dao.UpdateOrderDAO;
import com.ftd.mo.exceptions.GiftCardException;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityManager;

import com.ftd.security.cache.vo.UserInfo;
import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;

import com.ftd.osp.utilities.ConfigurationUtil;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


import javax.naming.InitialContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.UserTransaction;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Element;


public final class UpdatePaymentAction extends Action
{
	private static Logger logger  = new Logger("com.ftd.mo.actions.UpdatePaymentAction");

	// Class level constants
	private static final String DATA_ROOT_NODE = "root";

  /**
   * Action called to update a Modify Order Payment.
	 * 
   * @param mapping The ActionMapping used to select this instance.
   * @param form The optional ActionForm bean for this request.
   * @param request The HTTP Request we are processing.
   * @param response The HTTP Response we are processing.
   */
	public  ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
	        HttpServletResponse response)
	{
	    // Variables
	    Connection conn = null;
	    Document statusXML = null;

	    try
	    {
	        // Obtain connection
	        conn = DataSourceUtil.getInstance().getConnection(
	                ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE,
	                        COMConstants.DATASOURCE_NAME));

	        // Authorize and Update payment information
	        statusXML = applyPayment(request, conn);
	        logger.debug("Status_XML" + DOMUtil.convertToString(statusXML));
	 
	        if(logger.isDebugEnabled())
	        {
	            //IMPORTANT:  Because StringWriter contents are NOT removed even when the flush
	            //            method is executed, make StringWriter go away through scope control.
	            //            Both the flush and close methods were tested with but were of none affect.
	            StringWriter sw = new StringWriter();       //string representation of xml document
	            DOMUtil.print((Document) statusXML,new PrintWriter(sw));
	            logger.debug("- execute(): COM returned = \n" + sw.toString());
	        }

	        // Send back status document as XML.  The front end utilizes AJAX
	        response.setContentType("text/xml");
	        DOMUtil.print(statusXML,response.getWriter());
	    }
	    catch (GiftCardException ge) {
	        ge.printStackTrace();
	        try
            {
                statusXML = createErrorStatus(ge.getMessage());
            }
            catch (Exception e)
            {
                logger.error(e);
            }
	    } catch (Throwable e) {
	        e.printStackTrace();

	        //if logging is enabled, display the customer id, and external and master order #s
	        if(logger.isDebugEnabled())
	        {
	            logger.debug("Update Payment Action --- Order Detail Id = " +
	                    request.getParameter(COMConstants.ORDER_DETAIL_ID) + " and securitytoken = " +
	                    request.getParameter(COMConstants.SEC_TOKEN));
	        }

	        logger.error(e);

	        // Send back error status document
	        try
	        {
	            response.setContentType("text/xml");
	            statusXML = createErrorStatus( (new GiftCardBO()).getErrorMessage(GiftCardBO.DEFAULT_ERROR_CODE.toString()));
	            DOMUtil.print(statusXML,response.getWriter());
	        }
	        catch(Exception e1)
	        {
	            logger.error(e1);
	        }
	    }

	    finally
	    {
	        try
	        {
	            // Close the connection
	            if(conn != null)
	            {
	                conn.close();
	            }
	        }
	        catch(Exception e)
	        {
	            logger.error(e);

	            // Send back error status document
	            try
	            {
	                statusXML = createErrorStatus(e.getMessage());
	                DOMUtil.print(statusXML,response.getWriter());
	            }
	            catch(Exception e1)
	            {
	                logger.error(e1);
	            }
	        }
	    }

	    return null;
	}


  /**
   * Action called to authorize and create a Modify Order Payment.
	 * 
   * @param request The HTTP Request we are processing.
   * @param conn Database connection.
	 * @throws Exception
   */
  public Document applyPayment(HttpServletRequest request, Connection conn) 
		throws Exception
  {
		// Variables
		Document statusDoc;
		
        // In case the cart has a removed item and EOD has not run yet, recalculate the payment records.
        // If EOD has run or there are no removed records, this will be a no-op
        FTDCommonUtils.recalculateRemovedOrders(conn, request.getParameter("order_guid"));

		// Obtain payment type
		String payType = request.getParameter("pay_type");
		
		// If payment type is I apply an Invoice payment
		if(payType != null && payType.equalsIgnoreCase(COMConstants.CONS_INVOICE_PAYMENT_TYPE))
		{
			statusDoc = applyInvoicePayment(request, conn);
		}
		// If payment type is not I apply a general payment
		else
		{
			statusDoc = applyGenPayment(request, conn);
		}
		
		return statusDoc;
	}


  /**
   * Action called to authorize and create an Invoice payment.
	 * 
   * @param request The HTTP Request we are processing.
   * @param conn Database connection.
	 * @throws Exception
   */
  public Document applyInvoicePayment(HttpServletRequest request, 
		Connection conn) 
		throws Exception
  {	
		// Variables
		PaymentVO gcPVO = null;
		GcCouponVO gcVO = null;
		String payMethod = null;
		PaymentVO pVO = null;
		HashMap billingInfo = null;

    // Obtain payment type
		payMethod = request.getParameter("pay_method");

		// Obtain page and required data passed in
		String orderGuid = request.getParameter("order_guid");
		String orderDetailId = request.getParameter("order_detail_id");
		String sourceCode = request.getParameter("source_code");
		String partnerId = request.getParameter("partner_id");
		
		// Using the SecurityManager get the csr id
		UserInfo userInfo = SecurityManager.getInstance().getUserInfo(request.getParameter("securitytoken"));
		if (userInfo ==null)
		{
				// TODO create is error status doc
				throw new Exception("Csr id could not be obtained.");
		}
		String csrId = userInfo.getUserID();

		// Obtain the charge amount and remove formatting
    String total = request.getParameter("charge_amt");
    double totalAmt = 0.00;
    if(total.indexOf(",") != -1)
    {
      DecimalFormat decimalFormat = new DecimalFormat("#,###,###.00");
      totalAmt = decimalFormat.parse(total).doubleValue();
    }
		else
    {
      totalAmt=Double.parseDouble(total);
    }

		// If payment is not a gift certificate create a general PaymentVO
		if(!payMethod.equalsIgnoreCase(COMConstants.GIFT_CERTIFICATE))
		{
			// Populate a PaymentVO
			pVO = new PaymentVO();
	
			pVO.setOrderGuid(request.getParameter("order_guid"));
			pVO.setPaymentType(request.getParameter("pay_method"));  
			pVO.setUpdatedBy(csrId);
			pVO.setAuthOverrideFlag("N");
			pVO.setPaymentInd("P");
			pVO.setAuthDate(new Date());

			pVO.setCreditAmount(totalAmt);          
			
			// Create map of billing/co-brand information entered on the screen
			billingInfo = new HashMap();
			HashMap info = null;
			String result = null;
			String key = null;
			String nKey = "name";
			String vKey = "value";
			boolean populate = true;
			int i = 0;
			Map pMap = request.getParameterMap();
			while(populate)
			{
				// Prompt name increments for each piece of co-brand information
				key = "promptName" + i;

				// While prompt names exists populate the prompt name and value
				if(pMap.containsKey(key))
				{
					info = new HashMap();
					info.put(nKey, request.getParameter(key));
					info.put(vKey, request.getParameter("promptValue" + i));
					billingInfo.put(key, info);
					i++;
				}
				// Stop processing when no more prompts are available
				else
				{
					populate = false;
				}
			}
		}
		
		// Add the paymentVO to the array when a gift certificate payment is found
		// or a gift certificate number exists
		String giftCertId = request.getParameter("appr_gcc_num");
		if (payMethod.equalsIgnoreCase(COMConstants.GIFT_CERTIFICATE)
			|| (giftCertId != null && giftCertId.length() > 0))
    {
			gcVO = new GcCouponVO();
			gcPVO = new PaymentVO();

			// Populate gift certificate payment information
			populateGc(request, gcVO, gcPVO, giftCertId, csrId);     
		}
		
		// Apply invoice payment
		Document statusDoc = new PaymentBO(conn).applyInvoicePayment(orderGuid, 
		orderDetailId, sourceCode, partnerId, pVO, billingInfo, gcVO, gcPVO);
		
		return statusDoc;
	}


  /**
   * Action called to authorize and create a general payment.
	 * 
   * @param request The HTTP Request we are processing.
   * @param conn The database connection.
	 * @throws Exception
   */
  public Document applyGenPayment(HttpServletRequest request, 
		Connection conn) 
		throws Exception
  {
		// Variables 
		PaymentVO pVO = null;
		PaymentVO gcPVO = null;
		CreditCardVO ccVO = null;
		CommonCreditCardVO cccVO = null;
		GcCouponVO gcVO = null;
        GiftCardVO gdVO = null;
		String payMethod = null;
		String mgrCode = request.getParameter("approval_id");
		String mgrPassword = request.getParameter("approval_password");
		String context = request.getParameter(COMConstants.CONTEXT);
		String orderDetailId = request.getParameter("order_detail_id");

		// Using the SecurityManager get the csr id
		UserInfo userInfo = SecurityManager.getInstance().getUserInfo(request.getParameter("securitytoken"));
		if (userInfo ==null)
		{
				// TODO create is error status doc
				throw new Exception("Csr id could not be obtained.");
		}
		String csrId = userInfo.getUserID();

		// Obtain payment type
		payMethod = request.getParameter("pay_method");
		
		// If payment is not a gift certificate 
		if(!payMethod.equalsIgnoreCase(COMConstants.GIFT_CERTIFICATE))
		{
				// Populate a PaymentVO
				pVO = new PaymentVO();
				
				pVO.setOrderGuid(request.getParameter("order_guid"));
		
				// Obtain Military Star aafes code if it exists
				if (request.getParameter("aafes_code") != null 
					&& request.getParameter("aafes_code").length() > 0)
				{
					pVO.setAafesTicketNumber(request.getParameter("aafes_code"));
				}        

				// Set the payment method
				pVO.setPaymentType(payMethod);  

				pVO.setUpdatedBy(csrId);

				// Populate credit card authorization code if it exists
				if (request.getParameter("auth_code") != null  
					&& request.getParameter("auth_code").length() > 0)
				{
					pVO.setAuthOverrideFlag("Y");
					pVO.setAuthorization(request.getParameter("auth_code"));
				}
				else
				{
					pVO.setAuthOverrideFlag("N");
				}
                                
                                if (mgrCode != null && mgrCode.length() > 0) {
                                    pVO.setNcApprovalId(mgrCode);
                                }

				// Obtain charge amount and remove any formatting
				String total = request.getParameter("charge_amt");
				double paymentAmount = 0.00;
				if(total.indexOf(",") != -1)
				{
					DecimalFormat decimalFormat = new DecimalFormat("#,###,###.00");
					paymentAmount = decimalFormat.parse(total).doubleValue();
				}else
				{
					paymentAmount = Double.parseDouble(total);
				}
				
				// Set payment amount
				pVO.setCreditAmount(paymentAmount);          
				
				pVO.setPaymentInd("P");
				pVO.setAuthDate(new Date());
		
				// If the payment is not gift certificate or no charge and a credit
				// card number exists create credit card payment records
				if (!payMethod.equalsIgnoreCase(COMConstants.GIFT_CERTIFICATE) &&
					!payMethod.equalsIgnoreCase(COMConstants.NO_CHARGE) &&
					request.getParameter("card_num") != null && 
					request.getParameter("card_num").length() > 0)
				{
					ccVO = new CreditCardVO();
					cccVO = new CommonCreditCardVO();
					
					//if the last credit card is used, only 4 digits are coming in from the screen
					//set the card fields to null and go get the full card number in the bo.
					logger.info("is_orig_cc = " + request.getParameter("is_orig_cc"));
		            logger.info("Wallet Indicator = " + request.getParameter("wallet_indicator"));
					// If original credit card was used blank out credit card number
					if(request.getParameter("is_orig_cc").equalsIgnoreCase("Y"))
					{
						cccVO.setCreditCardNumber(null);
						ccVO.setCcNumber(null);
						pVO.setCardNumber(null);
						pVO.setWalletIndicator(request.getParameter("wallet_indicator"));
					}
					// Else populate credit card info with what was entered on the screen
					else
					{
						cccVO.setCreditCardNumber(request.getParameter("card_num"));
						ccVO.setCcNumber(request.getParameter("card_num"));
						pVO.setCardNumber(request.getParameter("card_num"));
						pVO.setWalletIndicator(null);
					}
				
					// Populate CommonCreditCardVO used for validation
					cccVO.setExpirationDate(request.getParameter("exp_year") + request.getParameter("exp_month"));
					cccVO.setCreditCardType(payMethod);
					cccVO.setAmount(request.getParameter("charge_amt"));
					cccVO.setApprovalCode(request.getParameter("auth_code"));
                                        String ccSkipAuth = request.getParameter("cc_skip_auth");
                                        if ("Y".equalsIgnoreCase(ccSkipAuth)) {
                                            cccVO.setCcSkipAuth(true);
                                        }
					
					// Populate CreditCardVO used to store the payment
					ccVO.setCcExpiration(request.getParameter("exp_month") + "/" + request.getParameter("exp_year"));
					ccVO.setCcType(cccVO.getCreditCardType());
					ccVO.setUpdatedBy(csrId);
				}
		}    
    
		// Add the paymentVO to the array when a gift certificate payment is found
		// or a gift certificate number exists
		String giftCertId = request.getParameter("appr_gcc_num");
		if (payMethod.equalsIgnoreCase(COMConstants.GIFT_CERTIFICATE)
			|| (giftCertId != null && giftCertId.length() > 0))
		{
		    String gdorgc = request.getParameter(COMConstants.GD_OR_GC);

			gcPVO = new PaymentVO();

			if (gdorgc != null && gdorgc.equals(COMConstants.GIFT_CARD_PAYMENT))
			{
	             // Populate gift card payment information
	            gdVO = new GiftCardVO();
                populateGd(request, gdVO, gcPVO, csrId, conn);
			}
			else
			{
			    // Populate gift certificate payment information
	            gcVO = new GcCouponVO();
			    populateGc(request, gcVO, gcPVO, giftCertId, csrId);
			}
		}
		

		//Authorize the payment and check balances
		PaymentBO paymentBO = new PaymentBO(conn);
		// Authorize the payment if it has not been manually authed
		Document statusDoc = paymentBO.authorize(request.getParameter("order_guid"), orderDetailId, mgrCode, mgrPassword, context, 
			ccVO, cccVO, gcPVO, pVO, gcVO, gdVO);
		
		// If authorization failed OR if just validating the payment method, return the status
		/**
		 * This is a huge deal.  It determines if we go back to the UI to ask the user for more payment or not
		 */
		if(statusDoc !=null) {
	        logger.debug("Status_doc in UpdatePaymentAction" + DOMUtil.convertToString(statusDoc));	        		
			return statusDoc;
		}
		
		// Start transaction
		UserTransaction ut = createUserTransaction();
		ut.begin();
		
		// Apply payment except for gift card - they are handled in createneworderBO.processrequest
		try {
			statusDoc = new PaymentBO(conn).applyGenPayment(request.getParameter("order_guid"),
	                                                    request.getParameter("order_detail_id"),
	                                                    mgrCode, mgrPassword,
	                                                    context, gcPVO, pVO, gcVO, gdVO, ccVO, cccVO);
			if ((DOMUtil.getNodeText(statusDoc, "root/is_error").equals("N"))
					&& DOMUtil.getNodeText(statusDoc, "root/status/data/message_status").equals("valid")
					&& !DOMUtil.getNodeText(statusDoc, "root/status/data/message_action").equals("amount_owed")) {
				
			    // expect processrequest to throw an exception if a rollback is required since there is no return code
				new CreateNewOrderBO(request, conn).processRequest();
			}
			
			// Commit the transaction
			ut.commit();
		} catch (GiftCardException ge) {
			statusDoc = createErrorStatus(ge.getMessage());
			ut.rollback();
			ge.printStackTrace();
		} catch (Throwable e) {
			statusDoc = createErrorStatus( (new GiftCardBO()).getErrorMessage(GiftCardBO.DEFAULT_ERROR_CODE.toString()));
			ut.rollback();
			e.printStackTrace();
		}
		


		return statusDoc;
	}


  /**
   * Populates gift certificate objects required for payment.
	 * 
   * @param request The HTTP Request we are processing.
   * @param gcVO Gift certificate information.
   * @param gcPVO Gift certificate payment information.
   * @param giftCertId Gift certificate id.
   * @param csrId Csr id creating the payment.
	 * @throws Exception
   */
  private void populateGc(HttpServletRequest request, GcCouponVO gcVO, 
		PaymentVO gcPVO, String giftCertId, String csrId) 
		throws Exception
  {
		gcPVO.setOrderGuid(request.getParameter("order_guid"));
		gcPVO.setPaymentType(COMConstants.CONS_GC_PAYMENT_TYPE);      
		
		// If giftCertId exists the gift certificate was already approved and
		// the payment will be the full amount of the gift certificate.  Another
		// payment in addition to the gift certificate payment is being processed.
		if(giftCertId != null && giftCertId.length() > 0)
		{
		 gcPVO.setCreditAmount(Double.valueOf(request.getParameter("appr_gcc_amt")).doubleValue());				 				
		 gcPVO.setGcCouponNumber(giftCertId);
		 gcVO.setGcCouponNumber(giftCertId);
		}
		// If giftCertId does not exist then the charge amount should be used.
		else
		{
		 gcPVO.setCreditAmount(Double.valueOf(request.getParameter("charge_amt")).doubleValue());				 
		 gcPVO.setGcCouponNumber(request.getParameter("gcc_num"));
		 gcVO.setGcCouponNumber(request.getParameter("gcc_num"));
		}
		
		gcPVO.setPaymentInd("P");
		gcPVO.setUpdatedBy(csrId);
		gcPVO.setAuthDate(new Date());
		gcVO.setUpdatedBy(csrId);
	}

  /**
   * Populates gift card objects required for payment.
   * 
   * @param request The HTTP Request we are processing.
   * @param gdVO Gift certificate information.
   * @param gdPVO Gift certificate payment information.
   * @param giftCardId Gift certificate id.
   * @param csrId Csr id creating the payment.
   * @throws Exception
   */
  private void populateGd(HttpServletRequest request, GiftCardVO gdVO, 
          PaymentVO gdPVO, String csrId, Connection conn) 
  throws Exception
  {
      gdPVO.setOrderGuid(request.getParameter("order_guid"));
      gdPVO.setPaymentType(COMConstants.CONS_GC_PAYMENT_TYPE);
      
      // get the GD/GC number
      String gccnum = "";
      //always get the pin from gd_pin field
      String gccpin = request.getParameter("gd_pin");
      // If giftCardId does not exist then the charge amount should be used. 
      // This branch is executed upon first entry of the gift card which also means we may have to find
      // the gcc num if this is reusing the original
      gccnum = request.getParameter("gcc_num");
      if (gccnum != null && !gccnum.equals("")) {
          // This is the first call from the client
          gdPVO.setCreditAmount(Double.valueOf(request.getParameter("charge_amt")).doubleValue());  // total cost of the order              
      } else if (request.getParameter("appr_gcc_num") != null && !request.getParameter("appr_gcc_num").equals("")) {
          // If giftCardId exists the gift card was already approved and
          // the payment will be the full amount of the gift card.  Another
          // payment in addition to the gift card payment is being processed. 
          // That is, this branch is executed when a second payment form is required, e.g. CC.
          gccnum = request.getParameter("appr_gcc_num");
          gdPVO.setCreditAmount(Double.valueOf(request.getParameter("appr_gcc_amt")).doubleValue());      
      }

      gccnum = GiftCardBO.getUnmaskedGiftCardNum(gccnum, request.getParameter(COMConstants.ORDER_GUID), conn);
      
      gdVO.setGcCouponNumber(gccnum);
      gdPVO.setCardNumber(gccnum);
      gdPVO.setGiftCardPin(gccpin);
      gdVO.setPin(gccpin);      

      gdPVO.setPaymentInd("P");
      gdPVO.setUpdatedBy(csrId);
      gdPVO.setAuthDate(new Date());
      gdPVO.setPaymentType("GD");


      gdVO.setUpdatedBy(csrId);
  }


  /**
   * Create error status document
   * @return Document - status
   * @throws Exception
   */
   
  private Document createErrorStatus(String message)
        throws Exception
  {
    Document returnDocument = DOMUtil.getDefaultDocument();
    Element root = (Element) returnDocument.createElement(DATA_ROOT_NODE);
    returnDocument.appendChild(root);

    Element errorNode = (Element)returnDocument.createElement("is_error");
		errorNode.appendChild(returnDocument.createTextNode("Y"));
    root.appendChild(errorNode);
    
    Element el2 = (Element)returnDocument.createElement("error_message");
    el2.appendChild(returnDocument.createTextNode(message));
    root.appendChild(el2);
    
    return returnDocument;  
  }
  
	  /**
		*	Creates user transaction.
		*
		* @return UserTransaction - User transaction
	* @throws Exception
	*/
	private UserTransaction createUserTransaction() 
			throws Exception
	{
			// Obtain the transaction timeout
			int transactionTimeout = Integer.parseInt((String)ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, COMConstants.TRANSACTION_TIMEOUT_IN_SECONDS));
			// Obtain UserTransaction object
			String jndi_usertransaction_entry = ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, COMConstants.JNDI_USERTRANSACTION_ENTRY);
			// Obtain initial context
			InitialContext iContext = new InitialContext();
			// Generate UserTransaction
			UserTransaction userTransaction = (UserTransaction)  iContext.lookup(jndi_usertransaction_entry);
			// Set timeout
			userTransaction.setTransactionTimeout(transactionTimeout);
			return userTransaction;
	}
}