package com.ftd.mo.actions;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.util.BasePageBuilder;
import com.ftd.mo.bo.PaymentBO;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;

import com.ftd.osp.utilities.ConfigurationUtil;

import java.util.HashMap;
import java.util.Iterator;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;


public final class LoadPaymentAction extends Action
{
    private static Logger logger  = new Logger("com.ftd.mo.actions.LoadPaymentAction");


  /**
   * Action called to load the Modify Order Payment Screen.
	 * 
   * @param mapping The ActionMapping used to select this instance.
   * @param form The optional ActionForm bean for this request.
   * @param request The HTTP Request we are processing.
   * @param response The HTTP Response we are processing.
   */
  public  ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                                HttpServletResponse response)
  {
		ActionForward forward = null;
    Connection conn = null;
			
    try
    {
      // Obtain connection
      conn = DataSourceUtil.getInstance().getConnection(
            ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE,
            COMConstants.DATASOURCE_NAME));

			// Obtain payment information
			Document paymentXML = getPaymentInfo(request, conn);

			// Obtain xsl file
			String xslName = "Payment";
			File xslFile = getXSL(xslName, mapping);	

	        forward = mapping.findForward(xslName);
	        String xslFilePathAndName = forward.getPath(); //get real file name
	          
	        // Change to client side transform
	        TraxUtil.getInstance().getInstance().transform(request, response, paymentXML, 
	                            xslFile, xslFilePathAndName, (HashMap)request.getAttribute(COMConstants.CONS_APP_PARAMETERS));
    }
    catch(Exception e)
    {
      //if logging is enabled, display the customer id, and external and master order #s
      if(logger.isDebugEnabled())
      {
        logger.debug("Load Payment Action --- Order Detail Id = " +
                      request.getParameter(COMConstants.ORDER_DETAIL_ID) +
                      " and securitytoken = " +
                      request.getParameter(COMConstants.SEC_TOKEN));
      }

      logger.error(e);
      forward = mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
      return forward;
    }

    finally
    {
      try
      {
        // Close the connection
        if(conn != null)
        {
          conn.close();
        }
      }
      catch(Exception e)
      {
        logger.error(e);
        forward = mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
        return forward;
      }
    }

		return null;
  }


  /**
   * Action called to load the Modify Order Payment Screen.
	 * 
   * @param request The HTTP Request we are processing.
   * @param conn The connection.
	 * @throws Exception
   */
  public Document getPaymentInfo(HttpServletRequest request, 
		Connection conn) 
		throws Exception
  {
		// Variables
		String sourceCode;
		String orderGuid;
		String orderDetailId;
		String securityToken;
		String orderTotal;
		HashMap pageData = new HashMap();
		
		// BO's
		PaymentBO pBO = new PaymentBO(conn);

    // Document that will contain the final XML, to be passed to the TransUtil and XSL page
    Document responseDocument = (Document) DOMUtil.getDocument();
		
		// Obtain data from the request
		sourceCode = request.getParameter(COMConstants.SOURCE_CODE);
		orderGuid = request.getParameter(COMConstants.ORDER_GUID);
		orderDetailId = request.getParameter(COMConstants.ORDER_DETAIL_ID);
		securityToken = request.getParameter(COMConstants.SEC_TOKEN);
		orderTotal = request.getParameter(COMConstants.ORDER_TOTAL);
		
		// Obtain Modify Order Payment Screen information
		HashMap paymentMap = pBO.getPaymentInfo(orderGuid, orderDetailId, sourceCode, pageData);

		// Obtain header information
		Document hdrXML = populateSubHeaderInfo(request);
		paymentMap.put(COMConstants.HK_SUB_HEADER, hdrXML);
		
		// Get all the keys in the hashmap returned from the business object
		Iterator iter = paymentMap.keySet().iterator();
		String key;
		
		// Iterate thru the HashMap and returned to create an XML document for the
		// payment screen
		while(iter.hasNext())
		{
			key = (String) iter.next();

			//Append all the existing XMLs to the final XML
			Document xmlDoc = (Document) paymentMap.get(key);
			NodeList nl = xmlDoc.getChildNodes();
			DOMUtil.addSection(responseDocument, nl);
		}
		
		//Convert the page data hashmap to XML and append it to the final XML
		pageData.put(COMConstants.ORDER_GUID, orderGuid);
		pageData.put(COMConstants.ORDER_DETAIL_ID, orderDetailId);
		pageData.put(COMConstants.SOURCE_CODE, sourceCode);
		pageData.put(COMConstants.ORDER_TOTAL, orderTotal);
		pageData.put(COMConstants.EXTERNAL_ORDER_NUMBER, request.getParameter(COMConstants.EXTERNAL_ORDER_NUMBER));
		pageData.put(COMConstants.CUSTOMER_ID, request.getParameter(COMConstants.CUSTOMER_ID));
		pageData.put(COMConstants.MASTER_ORDER_NUMBER, request.getParameter(COMConstants.MASTER_ORDER_NUMBER));

		DOMUtil.addSection(responseDocument, "pageData", "data", 
												pageData, true); 

		return responseDocument;
	}


 /**
  * Retrieve name of Payment screen XSL file
  * @param1 String - the xsl name returned from the Business Object
  * @param2 ActionMapping
  * @return File - XSL File name
  * @throws none
  */

  private File getXSL(String xslName, ActionMapping mapping)
  {

    File xslFile = null;
    String xslFilePathAndName = null;

    ActionForward forward = mapping.findForward(xslName);
    xslFilePathAndName = forward.getPath();                //get real file name
    xslFile = new File(this.getServlet().getServletContext().getRealPath(xslFilePathAndName));
    return xslFile;
  }

 /**
  * Creates an XML document of header information
	* 
	* @param request - Http request object
	* @return Document - Header information in the form of XML
	* @throws Exception
  */

  private Document populateSubHeaderInfo(HttpServletRequest request) throws Exception
  {
    // Obtain sub header info
    return new BasePageBuilder().retrieveSubHeaderData(request);
  }

}