package com.ftd.mo.actions;
import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.dao.LookupDAO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;

import java.io.File;
import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;


/**
 * <code>SourceCodeLookupAction</code> retrieves source codes using a supplied source code
 * or source code description.
 *
 * @author Mark Moon, Software Architects Inc.
 * @version $Id: SourceCodeLookupAction.java,v 1.3 2011/06/30 14:39:52 gsergeycvs Exp $
 */
public class SourceCodeLookupAction extends Action {

    private static Logger logger  = new Logger("com.ftd.mo.actions.SourceCodeLookupAction");

    /**
     * Default constructor.
     */
    public SourceCodeLookupAction() {
        super();
    }

    /**
    * This is the main action called from the Struts framework.
    * @param mapping The ActionMapping used to select this instance.
    * @param form The optional ActionForm bean for this request.
    * @param request The HTTP Request we are processing.
    * @param response The HTTP Response we are processing.
    */
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException, Exception {

        Connection connection = DataSourceUtil.getInstance().getConnection(ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, COMConstants.DATASOURCE_NAME));
        try {
            // user enters 'source code' or 'description' into the same textbox
            String searchValue = StringUtils.upperCase(request.getParameter("sourceCodeInput"));
            String companyId = StringUtils.upperCase(request.getParameter("companyId"));

            Document responseDocument = (Document) DOMUtil.getDocument(); // create base document
            LookupDAO dao = new LookupDAO(connection);
            Document results = dao.getSourceCodes(searchValue, companyId);

            DOMUtil.addSection( responseDocument, results.getChildNodes() ); // source codes

            ActionForward forward = mapping.findForward(COMConstants.XSL_SUCCESS);
            File xslFile = new File(this.getServlet().getServletContext().getRealPath(forward.getPath()));
            String xslFilePathAndName = forward.getPath(); //get real file name
            
            // Change to client side transform
            TraxUtil.getInstance().getInstance().transform(request, response, responseDocument, 
                                xslFile, xslFilePathAndName, (HashMap)request.getAttribute(COMConstants.CONS_APP_PARAMETERS));
            return null;
        }
        catch (SQLException se) {
            logger.error(se);
            return mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
        }
        finally {
            try {
                if(connection != null)
                    connection.close();
            }
            catch (SQLException se) {
                logger.error(se);
                return mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
            }
        }
    }
}