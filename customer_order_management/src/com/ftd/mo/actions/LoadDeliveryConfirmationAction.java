package com.ftd.mo.actions;


import com.ftd.mo.bo.LoadDeliveryConfirmationBO;
import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;

import java.io.File;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import java.sql.Connection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.w3c.dom.NodeList;
import org.w3c.dom.Document;
/**
 *
 * @author Mark Moon, Software Architects Inc.
 * @version $Revision: 1.3 $
 */
public class LoadDeliveryConfirmationAction
    extends Action {

  private static Logger logger  = new Logger(LoadDeliveryConfirmationAction.class.getName());

  /**
   * Default Constructor.
   */
  public LoadDeliveryConfirmationAction() {
    super();
  }


  /**
   * This is the main action called from the Struts framework.
   * @param mapping The ActionMapping used to select this instance.
   * @param form The optional ActionForm bean for this request.
   * @param request The HTTP Request we are processing.
   * @param response The HTTP Response we are processing.
   */
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {

		ActionForward forward;
    Connection connection = null;
    Document responseDocument;
    File xslFile;

    try {
      connection = DataSourceUtil.getInstance().getConnection( ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, COMConstants.DATASOURCE_NAME) );

      LoadDeliveryConfirmationBO bo = new LoadDeliveryConfirmationBO(request, connection);
      Map results = bo.processRequest();

      responseDocument = (Document)results.get("DELIVERY_CONFIRMATION_INFO");
      String xslName = (String)results.get("FORWARD");
      xslFile = getXSL( xslName, mapping);

      forward = mapping.findForward(xslName);
      String xslFilePathAndName = forward.getPath(); //get real file name
      
      // Change to client side transform
      TraxUtil.getInstance().getInstance().transform(request, response, responseDocument, 
                          xslFile, xslFilePathAndName, (HashMap)request.getAttribute(COMConstants.CONS_APP_PARAMETERS));
    }
    catch (Exception e) {
      logger.error(e);
      forward = mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
      return forward;
    }
    finally {
      try {
        if(connection != null) {
          connection.close();
        }
      }
      catch(Exception e) {
        logger.error(e);
        forward = mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
        return forward;
      }
    }

    return null;
  }


  /**
   *
   *
   * @param xslName
   * @return XSL File
   */
  private File getXSL(String xslName, ActionMapping mapping) {
    ActionForward forward = mapping.findForward(xslName);
    String xslFilePathAndName = forward.getPath();
    File xslFile = new File(this.getServlet().getServletContext().getRealPath(xslFilePathAndName));
    return xslFile;
  }
}