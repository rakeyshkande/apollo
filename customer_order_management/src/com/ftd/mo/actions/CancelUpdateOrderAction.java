package com.ftd.mo.actions;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.dao.LockDAO;
import com.ftd.mo.dao.UpdateOrderDAO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityManager;
import com.ftd.security.cache.vo.UserInfo;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;

import java.sql.Connection;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;






public final class CancelUpdateOrderAction extends Action
{

    private static  Logger  logger                = new Logger("com.ftd.mo.actions.CancelUpdateOrderAction");
    private String          csrId                 = null;
    private String          customerId            = null;
    private String          externalOrderNumber	  = null;
    private String          masterOrderNumber		  = null;
    private String          orderDetailId		      = null;
    private String          orderGuid					    = null;
    private HashMap         pageData              = new HashMap();
    private String          sessionId             = null;


  /**
   * This is the main action called from the Struts framework.
   * @param mapping The ActionMapping used to select this instance.
   * @param form The optional ActionForm bean for this request.
   * @param request The HTTP Request we are processing.
   * @param response The HTTP Response we are processing.
   */
  public  ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                                HttpServletResponse response)
  {
		ActionForward forward = null;
    Connection con = null;
    Document responseDocument = null;
    String xslName = COMConstants.XSL_CANCEL_UPDATE_ORDER_IFRAME;
    File xslFile = getXSL(xslName, mapping);

    try {
      //Document that will contain the final XML, to be passed to the TransUtil and XSL page
      responseDocument = (Document) DOMUtil.getDocument();

      //Connection/Database info
      con = DataSourceUtil.getInstance().getConnection(
            ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE,
            COMConstants.DATASOURCE_NAME));

      //Get info passed in the request object
      HashMap requestParms = new HashMap();
      getRequestInfo(request);

      setModifyFlag( request );

      //Retrieve the data
      processCancelOrder(con);

      //Convert the page data hashmap to XML and append it to the final XML
      DOMUtil.addSection(responseDocument, "pageData", "data", this.pageData, true);

			if(logger.isDebugEnabled())
			{
				//IMPORTANT:  Because StringWriter contents are NOT removed even when the flush
				//            method is executed, make StringWriter go away through scope control.
				//            Both the flush and close methods were tested with but were of none affect.
				StringWriter sw = new StringWriter();       //string representation of xml document
				DOMUtil.print((Document) responseDocument,new PrintWriter(sw));
				logger.debug("Cancel Update Order Action" + sw.toString());
			}

	  forward = mapping.findForward(xslName);
	  String xslFilePathAndName = forward.getPath(); //get real file name
	          
	  // Change to client side transform
	  TraxUtil.getInstance().getInstance().transform(request, response, responseDocument, 
	                        xslFile, xslFilePathAndName, (HashMap)request.getAttribute(COMConstants.CONS_APP_PARAMETERS));

      return null;
    }
    catch(Exception e) {
      try {
        logger.error(e);
        forward = mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);

        // set the error data
        pageData.clear();
        pageData.put("errorFlag", "Y");
        pageData.put("errorUrl", forward.getPath());
        DOMUtil.addSection(responseDocument, "errorData", "data", this.pageData, true);

        String xslFilePathAndName = forward.getPath(); //get real file name
        
        // Change to client side transform
        TraxUtil.getInstance().getInstance().transform(request, response, responseDocument, 
                            xslFile, xslFilePathAndName, (HashMap)request.getAttribute(COMConstants.CONS_APP_PARAMETERS));

        return null;
      }
      catch (Exception ex)
      {
        //if logging is enabled, display the input parameters
        if(logger.isDebugEnabled())
        {
          logger.debug( "Cancel Update Order Action --- CSR " + this.csrId +
                        " canceling out of external order number" + this.externalOrderNumber +
                        " in Modify Order");
        }

        logger.error("Failed trying to send back error url - "  + ex);
        forward = mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
        return forward;
      }
    }
    finally {
      try
      {
        //Close the connection
        if(con != null)
        {
          con.close();
        }
      }
      catch(Exception e)
      {
        logger.error(e);
        forward = mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
        return forward;
      }
    }
  }



  /******************************************************************************
  * getRequestInfo(HttpServletRequest request)
  *******************************************************************************
  * Retrieve the info from the request object, and set class level variables
  * @param  HttpServletRequest
  * @return none
  * @throws none
  */
  private void getRequestInfo(HttpServletRequest request) throws Exception
  {
    ConfigurationUtil cu = null;
    cu = ConfigurationUtil.getInstance();

    //retrieve the customer id
    if(request.getParameter(COMConstants.CUSTOMER_ID)!=null)
    {
      this.customerId = request.getParameter(COMConstants.CUSTOMER_ID);
      this.pageData.put(COMConstants.PD_CUSTOMER_ID, this.customerId);
    }

    //retrieve the item order number
    if(request.getParameter(COMConstants.EXTERNAL_ORDER_NUMBER)!=null)
    {
      this.externalOrderNumber = request.getParameter(COMConstants.EXTERNAL_ORDER_NUMBER);
      this.pageData.put(COMConstants.PD_EXTERNAL_ORDER_NUMBER, this.externalOrderNumber);
    }

    //retrieve the master order number
    if(request.getParameter(COMConstants.MASTER_ORDER_NUMBER)!=null)
    {
      this.masterOrderNumber = request.getParameter(COMConstants.MASTER_ORDER_NUMBER);
      this.pageData.put(COMConstants.PD_MASTER_ORDER_NUMBER, this.masterOrderNumber);
    }

    //retrieve the order detail id
    if(request.getParameter(COMConstants.ORDER_DETAIL_ID)!=null)
    {
      this.orderDetailId = request.getParameter(COMConstants.ORDER_DETAIL_ID);
      this.pageData.put(COMConstants.PD_ORDER_DETAIL_ID, this.orderDetailId);
    }

    //retrieve the order guid
    if(request.getParameter(COMConstants.ORDER_GUID)!=null)
    {
      this.orderGuid = request.getParameter(COMConstants.ORDER_GUID);
      this.pageData.put(COMConstants.PD_ORDER_GUID, this.orderGuid);
    }

    /****************************************************************************************
     *  Retrieve Security Info
     ***************************************************************************************/

    this.sessionId   = request.getParameter(COMConstants.SEC_TOKEN);

    //get csr Id
    String security = (String) cu.getProperty(COMConstants.PROPERTY_FILE,COMConstants.SECURITY_IS_ON);
    boolean securityIsOn = security.equalsIgnoreCase("true") ? true : false;

   //delete this before going to production.  the following should only check on securityIsOn
    if(securityIsOn || this.sessionId != null)
    {
      SecurityManager sm = SecurityManager.getInstance();
      UserInfo ui = sm.getUserInfo(this.sessionId);
      this.csrId = ui.getUserID();
    }

  }



/*******************************************************************************************
 * processCancelOrder()
 *******************************************************************************************
  * This method is used to cancel the order
  *
  */
  private void processCancelOrder(Connection con) throws Exception
  {
    /***********************************************************************************************
     * cancel the order
     **********************************************************************************************/
    //Instantiate UpdateOrderDAO
    UpdateOrderDAO uoDAO = new UpdateOrderDAO(con);

    //call the cancel method to delete the temp order
    logger.debug("CSR " +
                  this.csrId +
                  " canceling out of order number" +
                  this.externalOrderNumber +
                  " in Modify Order");
    uoDAO.cancelUpdateOrder(this.orderDetailId);


    /***********************************************************************************************
     * release the lock
     **********************************************************************************************/
    //Instantiate LockDAO
    LockDAO lockDAO = new LockDAO(con);

    //Call releaseLock method in the DAO
    logger.debug("CANCEL NOT WORKING --- calling lockDAO.releaseLock("
      + this.sessionId + ", "
      + this.csrId + ", "
      + this.orderDetailId + ", "
      + COMConstants.CONS_ENTITY_TYPE_MODIFY_ORDER + ")"
      );
    lockDAO.releaseLock(this.sessionId, this.csrId, this.orderDetailId, COMConstants.CONS_ENTITY_TYPE_MODIFY_ORDER);

  }



  /******************************************************************************
  *                                     getXSL()
  *******************************************************************************
  * Retrieve name of Customer Order Search Action XSL file
  * @param1 String - the xsl name returned from the Business Object
  * @param2 ActionMapping
  * @return File - XSL File name
  * @throws none
  */

  private File getXSL(String xslName, ActionMapping mapping)
  {

    File xslFile = null;
    String xslFilePathAndName = null;

    ActionForward forward = mapping.findForward(xslName);
    xslFilePathAndName = forward.getPath();                //get real file name
    xslFile = new File(this.getServlet().getServletContext().getRealPath(xslFilePathAndName));
    return xslFile;
  }


 /*******************************************************************************************
  * setModifyFlag()
  *******************************************************************************************
  *
  * A change was detected.  Thus, update the MODIFY_ORDER_UPDATED flag in the request parameters
  *
  * @return nothing
  * @throws java.lang.Exception
  */
  private void setModifyFlag(HttpServletRequest request)
  {
    HashMap parameters = (HashMap)request.getAttribute( COMConstants.CONS_APP_PARAMETERS);
    parameters.put(COMConstants.MODIFY_ORDER_UPDATED, COMConstants.CONS_NO);
    request.setAttribute(COMConstants.CONS_APP_PARAMETERS, parameters);
  }
}