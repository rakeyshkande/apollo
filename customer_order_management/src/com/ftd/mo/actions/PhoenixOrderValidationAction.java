package com.ftd.mo.actions;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.dao.OrderDAO;
import com.ftd.mo.bo.PhoenixOrderBO;
import com.ftd.mo.vo.DeliveryInfoVO;
import com.ftd.op.order.dao.PhoenixDAO;
import com.ftd.op.order.vo.PhoenixVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;


public class PhoenixOrderValidationAction extends Action 
{
	private static Logger logger  = new Logger("com.ftd.mo.actions.PhoenixOrderValidationAction");
	private Document responseDocument;

	Connection conn = null;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
	{
		HashMap pageData = new HashMap();
		HashMap<String,String> deliveryDatesList = null;
		String phoenixProduct = null;
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		
		String orderDetailId = (String) request.getParameter(COMConstants.ORDER_DETAIL_ID);
		try
		{
			conn = DataSourceUtil.getInstance().getConnection(
		            ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE,
		            COMConstants.DATASOURCE_NAME));
			
			this.responseDocument = (Document) DOMUtil.getDocument();
			
			// Instantiate OrderDAO
            OrderDAO orderDAO = new OrderDAO(conn);
			CachedResultSet orderDetailsInfo = orderDAO.getOrderDetailsInfo(orderDetailId);
		
			String orderCancelledIndicator = null;
			
			//String orderCancelledIndicator = null;
            String orderInCreditQ = null;
            String holdReason = null;
            String sourceCode = null;
            String companyId = null;
            String originId = null;
           
            
            //String holdReason = null;
           
            
            if(orderDetailsInfo.next())
            {
              orderInCreditQ = orderDetailsInfo.getString("order_in_credit_q");
              holdReason = orderDetailsInfo.getString("hold_reason");
              orderCancelledIndicator = orderDetailsInfo.getString("cancelled_flag");
              sourceCode = orderDetailsInfo.getString("SOURCE_CODE");
              companyId = orderDetailsInfo.getString("COMPANY_ID");
              originId = orderDetailsInfo.getString("ORIGIN_ID");
            }
            
			
			pageData.put("orderDetailId",orderDetailId);
			pageData.put("companyId", companyId);
			pageData.put("sourceCode", sourceCode);
			pageData.put("originId", originId);
			
			
			if(validateMercuryAddress(conn,orderDetailId))
			{
				//check if the order is credit Q. It yes, return a message that this order is not
				//phoenix eligible
				if (orderInCreditQ != null && orderInCreditQ.equalsIgnoreCase(COMConstants.CONS_YES))
		        {
					logger.info("Order in credit Queue");
					pageData.put("success",COMConstants.CONS_NO);
					pageData.put(COMConstants.PD_MESSAGE_DISPLAY, "This order is not Phoenix eligible");
	                
		        }
				else
				{

					PhoenixOrderBO bo = new PhoenixOrderBO(conn);			
					DeliveryInfoVO deliveryInfoVo  = bo.getPhoenixOrderInfo(orderDetailId);
					
					String addons = request.getParameter("addons");
					logger.info("addons: " + addons);
					String newAddons = null;
					
					if(deliveryInfoVo != null)
					{
						
						if (deliveryInfoVo.getBearAddonId() != null) {
							newAddons = deliveryInfoVo.getBearAddonId();
						}
						if (deliveryInfoVo.getChocolateAddonId() != null) {
							if (newAddons == null) {
								newAddons = deliveryInfoVo.getChocolateAddonId();
							} else {
								newAddons = newAddons + "," + deliveryInfoVo.getChocolateAddonId();
							}
						}
						logger.info("newAddons: " + newAddons);
						addons = newAddons;
						pageData.put("addons", addons);
			            pageData.put("sourceCode", sourceCode);
							
						
						
						logger.info("getPhoenixOrderInfo returned data");
						//Truncating last 4 digits if zipcode size is more than 5.
						String zipCode = deliveryInfoVo.getRecipientZipCode();
						if(zipCode != null && zipCode.trim().length() > 5){
							zipCode = zipCode.trim().substring(0, 5);
						}
						
						if(deliveryInfoVo.getProductId()!=null){
						String[] productList = deliveryInfoVo.getProductId().split(",");
						
						for(String product : productList){
							deliveryDatesList = bo.getDeliveryDatesFromPAS(product, zipCode, addons, sourceCode);
							if(deliveryDatesList !=null && deliveryDatesList.size() > 0){
								phoenixProduct = product;
								break;
							}
						}
						}
						
		
						if(deliveryDatesList != null && deliveryDatesList.size() > 0)
						{	
							logger.info("Pas returned delivery dates");
							pageData.put("success",COMConstants.CONS_YES);				
							pageData.put("zipcode",zipCode);
							if(deliveryInfoVo.getDeliveryDate() != null){
								pageData.put("deliveryDate",sdf.format(deliveryInfoVo.getDeliveryDate().getTime()));
							}
							pageData.put("phoenixProductId",deliveryInfoVo.getProductId());
							pageData.put("isOrigNewProdsame", deliveryInfoVo.getOrigNewProdSame());
							request.setAttribute("phoenix_product_id", deliveryInfoVo.getProductId());
						}
						else
						{
							pageData.put("success",COMConstants.CONS_NO);
							pageData.put(COMConstants.PD_MESSAGE_DISPLAY, "This order is not Phoenix eligible");
						}
					}
					else
					{	
						logger.info("getPhoenixOrderInfo returned null.");
						pageData.put("success",COMConstants.CONS_NO);
						pageData.put(COMConstants.PD_MESSAGE_DISPLAY, "This order is not Phoenix eligible");
					}
				}
				
			}else{
				logger.info("Recipient address on Mercury message is different than recipient address on order");
				pageData.put("success",COMConstants.CONS_NO);
				pageData.put(COMConstants.PD_MESSAGE_DISPLAY, "This order is not Phoenix eligible");
			}
			
			
			
				
			pageData.put("externalOrderNumber", request.getParameter("external_order_number"));
			DOMUtil.addSection(this.responseDocument, "pageData", "data", pageData, true);

			//set the iFrame name to be given control back to.			
			transform(mapping,request,response,responseDocument);
		}//end try
		catch (Throwable ex)
		{
			try
			{
			    logger.error("Error occurred during Phoenix order validation: " + getStackTrace(ex));
			    //set an output message
				String message = "Unexpected System Error.";

				pageData.put(COMConstants.PD_MESSAGE_DISPLAY, message);
				pageData.put("success",COMConstants.CONS_NO);

				//Convert the page data hashmap to XML and append it to the final XML
				DOMUtil.addSection(this.responseDocument, "pageData", "data", pageData, true);
				
				transform(mapping,request,response,responseDocument);

			}
			catch (Exception e)
			{
				if(logger.isDebugEnabled())
				{
					logger.debug("Phoenix Order Validation Action --- " + e.toString());
				}
				logger.error(e);
			}
		}
		finally
		{
			try
			{
				conn.close();
			}
			catch (SQLException se)
			{
				logger.error(se);
			}
		}
		return null;
	}
		


	/******************************************************************************
	 *                                     getXSL()
	 *******************************************************************************
	 * Retrieve xsl file name
	 * @param1 String - the xsl name returned from the Business Object
	 * @param2 ActionMapping
	 * @return File - XSL File name
	 */

	private void transform(ActionMapping mapping,HttpServletRequest request, HttpServletResponse response,Document responseDocument) throws Exception
	{
		String xslName = "PhoenixOrderValidationIFrame";
		
		ActionForward forward = mapping.findForward(xslName);
		String xslFilePathAndName = forward.getPath(); //get real file name

		//Get XSL File name
		File xslFile = xslFile = new File(this.getServlet().getServletContext().getRealPath(xslFilePathAndName));

		// Change to client side transform
		TraxUtil.getInstance().getInstance().transform(request, response, responseDocument, 
				xslFile, xslFilePathAndName, (HashMap)request.getAttribute(COMConstants.CONS_APP_PARAMETERS));
		//DOMUtil.print(responseDocument, System.out);

	}    

	
	private String getStackTrace( Throwable aThrowable ) {
	    final Writer result = new StringWriter();
	    final PrintWriter printWriter = new PrintWriter( result );
	    aThrowable.printStackTrace( printWriter );
	    return result.toString();
	  }
	
	private boolean validateMercuryAddress(Connection conn, String orderDetailId){
		
		String mercuryOrderNumber = null;
	    com.ftd.op.order.dao.OrderDAO dao = new com.ftd.op.order.dao.OrderDAO(conn);
		
					try {
						mercuryOrderNumber = dao.getLatestMercuryNumberForOrder(orderDetailId);
					} catch (Exception e) {
						logger.error("Error fetching the latest mercury order number : "+e.getMessage());
						mercuryOrderNumber = null;
					}
	            
	           logger.info("mercuryOrderNumber : "+mercuryOrderNumber); 
	            PhoenixDAO phoenixDAO = new PhoenixDAO(conn);
	            PhoenixVO pVO = phoenixDAO.getPhoenixDetails(String.valueOf(orderDetailId), "COM", mercuryOrderNumber);
	            String mercuryAddress = pVO.getMercuryAddress();
	      	  	String address1 = pVO.getRecipientAddress1();
	      	  	String address2 = pVO.getRecipientAddress2();
	      	  	String businessName = pVO.getBusinessName();
	      	  	Boolean addressCheckSuccess = true;
	            
	      	  if (mercuryAddress != null) {
	        	  int addressLength = mercuryAddress.length();
	        	  if (mercuryAddress.substring(addressLength-1).equals(",")) {
	        		  mercuryAddress = mercuryAddress.substring(0, addressLength-1);
	        	  }

	              String tempAddress = null;
	              if (businessName != null) {
	            	  businessName = businessName.trim();
	                  if (businessName .equals("")) {
	                	  businessName = null;
	                  } else {
	            	      tempAddress = businessName;
	                  }
	              }
	              if (tempAddress == null) {
	            	  tempAddress = address1;
	              } else {
	            	  address1 = address1.trim();
	            	  tempAddress = tempAddress + " " + address1;
	              }
	              if (address2 != null) {
	            	  address2 = address2.trim();
	            	  tempAddress = tempAddress + " " + address2;
	              }
	            
	              String[] addressArray = mercuryAddress.split(",");
	              if (addressArray.length == 1) {
	              	if (!tempAddress.equalsIgnoreCase(mercuryAddress)) {
	              		logger.info("mercuryAddress does not match");
	              		addressCheckSuccess = false;
	              	}
	              } else {
	                  for (int i=0; i<addressArray.length; i++) {
	                	  //Address 1 is always in the first row
	                  	  if (i == 0) {
	                  		  if (!addressArray[i].equalsIgnoreCase(address1)) {
	                  			  logger.info("address1 does not match");
	                  			addressCheckSuccess = false;
	                  			  break;
	                  		  }
	                  	  } else {
	                  		  //The second row can either be Address2 or Business Name
	                  		  if (i == 1) {
	                  			  if (address2 != null && !address2.equals("")) {
	                  				  if (!addressArray[i].equalsIgnoreCase(address2)) {
	                  					  logger.info("address2 does not match");
	                  					addressCheckSuccess = false;
	                  					  break;
	                  				  }
	                  			  } else {
	                  				  if (businessName != null && !businessName.equals("")) {
	                  					  if (!addressArray[i].equalsIgnoreCase(businessName)) {
	                  						  logger.info("business name does not match (1)");
	                  						addressCheckSuccess = false;
	                  						  break;
	                  					  }
	                  				  } else {
	                  					  logger.info("No address2/business name");
	                  					addressCheckSuccess = false;
	                  					  break;
	                  				  }
	                  			  }
	                  		  } else {
	                  			  //The third row (if it exists) is always the Business Name
	              				  if (businessName != null && !businessName.equals("")) {
	              					  if (!addressArray[i].equalsIgnoreCase(businessName)) {
	              						  logger.info("business name does not match (2)");
	              						addressCheckSuccess = false;
	              						  break;
	              					  }
	              				  } else {
	              					  logger.info("no business name");
	              					addressCheckSuccess = false;
	              					  break;
	              				  }
	                  		  }
	                  	  }
	                  }

	              }
	      	  }
	      	  logger.info("addressCheckSuccess :"+addressCheckSuccess);
	      return addressCheckSuccess;
	}
	
}
