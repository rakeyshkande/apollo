package com.ftd.mo.actions;

import java.io.IOException;
import java.io.Writer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.handlers.SourceMasterHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.SourceMasterVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import com.thoughtworks.xstream.io.json.JsonHierarchicalStreamDriver;
import com.thoughtworks.xstream.io.json.JsonWriter;
import com.thoughtworks.xstream.mapper.MapperWrapper;

public class SourceCodeLookUpAjaxAction
  extends Action
{

  private static Logger logger = new Logger("com.ftd.mo.actions.SourceCodeLookUpAjaxAction");

  /**
     * Default constructor.
     */
  public

  SourceCodeLookUpAjaxAction()
  {
    super();
  }

  /**
    * This is the main action called from the Struts framework.
    * @param mapping The ActionMapping used to select this instance.
    * @param form The optional ActionForm bean for this request.
    * @param request The HTTP Request we are processing.
    * @param response The HTTP Response we are processing.
    */
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, 
                               HttpServletResponse response)
    throws IOException, ServletException, Exception
  {

    try
    {
       
        XStream xstream = new XStream(new JsonHierarchicalStreamDriver() {
        public HierarchicalStreamWriter createWriter(Writer writer) {
            return new JsonWriter(writer, JsonWriter.DROP_ROOT_MODE);
        }
        
       protected MapperWrapper wrapMapper(MapperWrapper next)
        {
          return new MapperWrapper(next)
            {
              public boolean shouldSerializeMember(Class definedIn, String fieldName)
              {
                return "allowFreeShippingFlag".equalsIgnoreCase(fieldName);
              }
            };
        }        
        });
        

      String sourceCode = StringUtils.upperCase(request.getParameter("sourceCodeInput"));
      SourceMasterHandler handler = (SourceMasterHandler) CacheManager.getInstance().getHandler("CACHE_NAME_SOURCE_MASTER");
      
      SourceMasterVO vo = handler.getSourceCodeById(sourceCode);
      
      response.getOutputStream().write( xstream.toXML(vo).getBytes());        

    }
    catch (Exception e)
    {
      logger.error(e);
      throw new RuntimeException(e);
    }
     return null;
  }



}
