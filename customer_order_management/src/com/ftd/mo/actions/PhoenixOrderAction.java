package com.ftd.mo.actions;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.dao.StockMessageDAO;
import com.ftd.ftdutilities.FTDCommonUtils;
import com.ftd.mo.bo.PhoenixOrderBO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.PartnerUtility;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.PartnerVO;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.order.MercentOrderPrefixes;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.PartnerMappingVO;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityManager;
import com.ftd.security.cache.vo.UserInfo;

public class PhoenixOrderAction extends Action {
	private static Logger logger = new Logger(
			"com.ftd.mo.actions.PhoenixOrderAction");
	private Document responseDocument;

	private String csrId = null;
	private String customerId = null;
	private String externalOrderNumber = null;
	private String masterOrderNumber = null;
	private String orderDetailId = null;
	private String orderGuid = null;
	@SuppressWarnings("rawtypes")
	private HashMap pageData = new HashMap();
	private String sessionId = null;

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		String action = (String) request.getParameter("action");
		if (action.equalsIgnoreCase("load"))
			return load(mapping, request, response);
		else if (action.equalsIgnoreCase("submit")) {
			return submit(mapping, request, response);
		} else if (action.equalsIgnoreCase("cancel")) {
			return cancel(mapping, request, response);
		} else if (action.equalsIgnoreCase("validateShipDate")) {
			return validateShipDateNMethod(mapping, request, response);
		}

		return null;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private ActionForward load(ActionMapping mapping,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		HashMap pageData = new HashMap();
		HashMap<String, String> deliveryDatesList = null;
		HashMap<String, String> emailTypeList = null;
		Connection conn = null;
		ActionForward forward = null;
		try {
			conn = DataSourceUtil.getInstance().getConnection(
					ConfigurationUtil.getInstance().getProperty(
							COMConstants.PROPERTY_FILE,
							COMConstants.DATASOURCE_NAME));

			this.responseDocument = (Document) DOMUtil.getDocument();
			String orderDetailId = (String) request
					.getParameter(COMConstants.ORDER_DETAIL_ID);
			String recipZipCode = (String) request
					.getParameter("recip_zip_code");
			String deliveryDate = (String) request
					.getParameter("delivery_date");
			String phoenixProductId = (String) request
					.getParameter("phoenix_product_id");
			String externalOrderNumber = (String) request
					.getParameter("externalOrderNumber");
			String orderGUId = (String) request.getParameter("order_guid");
			String customerId = (String) request.getParameter("customer_id");
			String masterOrderNumber = (String) request
					.getParameter("master_order_number");
			String shipMethod = (String) request.getParameter("ship_method");
			String shipDate = (String) request.getParameter("ship_date");
			String isOrigNewProdSame = (String) request
					.getParameter("isOrigNewProdSame");
			String addons = (String) request.getParameter("addons");
			logger.info("addons: " + addons);
			String sourceCode = (String) request.getParameter("sourceCode");
			String companyId = (String) request.getParameter("companyId");
			String originId = (String) request.getParameter("originId");

			pageData.put("external_order_number", externalOrderNumber);
			pageData.put("order_guid", orderGUId);
			pageData.put("customer_id", customerId);
			pageData.put("master_order_number", masterOrderNumber);
			pageData.put("order_detail_id", orderDetailId);
			pageData.put("cancelText", "Customer requested to Cancel");
			pageData.put("recip_zip_code", recipZipCode);
			pageData.put("phoenix_product_id", phoenixProductId);
			pageData.put("ship_method", shipMethod);
			pageData.put("ship_date", shipDate);
			pageData.put("isOrigNewProdSame", isOrigNewProdSame);
			pageData.put("addons", addons);
			pageData.put("sourceCode", sourceCode);
			pageData.put("companyId", companyId);

			PhoenixOrderBO phoenixOrderBo = new PhoenixOrderBO(conn);
			// deliveryDatesList =
			// phoenixOrderBo.getDeliveryDatesFromPAS(phoenixProductId,recipZipCode,
			// addons,sourceCode);

			HashMap<String, String> deliveryDatesProductList = new HashMap<String, String>();
			String productString = "";
			String[] productList = phoenixProductId.split(",");

			for (String product : productList) {
				deliveryDatesList = phoenixOrderBo.getDeliveryDatesFromPAS(
						product, recipZipCode, addons, sourceCode);

				// deliveryDatesList

				logger.info("DeliveryDate for Product : " + product + " are : "
						+ deliveryDatesList);
				if (deliveryDatesList != null && deliveryDatesList.size() > 0) {

					for (String key : deliveryDatesList.keySet()) {
						if (deliveryDatesProductList.get(key) != null) {
							productString = deliveryDatesProductList.get(key);
							productString = productString + "," + product;
							deliveryDatesProductList.put(key, productString);
						} else {
							deliveryDatesProductList.put(key, product);
						}

					}
					phoenixProductId = product;
					logger.info("Phoenix product Avaialble : "
							+ phoenixProductId);

				}

			}

			deliveryDatesProductList = sortHashMapOnKeys(deliveryDatesProductList);

			logger.info("Product - Dates : " + deliveryDatesProductList);

			boolean isProductExistsInPAS = false;

			if (deliveryDatesProductList != null
					&& deliveryDatesProductList.size() > 0) {
				String pasDeliveryDate = deliveryDatesProductList
						.get(deliveryDate);
				if (pasDeliveryDate != null)
					isProductExistsInPAS = true;

				// Check if product exists for the deliverydate
				if (isProductExistsInPAS) {
					pageData.put("deliveryDateExistsInPAS", "Y");
					pageData.put("deliveryDate", deliveryDate);
				} else {
					pageData.put("deliveryDateExistsInPAS", "N");

				}
				DOMUtil.addSection(this.responseDocument, "deliveryDatesList",
						"dDate", deliveryDatesProductList, true);
				// check if Live florist
				if (phoenixOrderBo.isLiveFTD(orderDetailId)) {
					pageData.put("liveFTD", "Y");
				} else {
					pageData.put("liveFTD", "N");
				}

				// check if the original sku is same as new sku. then email drop
				// down should have only the type of none.
				if (isOrigNewProdSame.equalsIgnoreCase("Y")) {
					emailTypeList = new HashMap<String, String>();
					emailTypeList.put("none", "None (No Email)");
				} else {

					emailTypeList = (HashMap<String, String>) phoenixOrderBo
							.getPhoenixEmailTypes();
					for (String key : emailTypeList.keySet()) {
						logger.info(key + " : " + emailTypeList.get(key));
					}

					// emailTypeList.put("stock_email",
					// "Send Stock Email (Select Below)");

				}

				DOMUtil.addSection(this.responseDocument, "emailTypeList",
						"emailType", emailTypeList, true);

				// Setting the stock email for Phoenix
				StockMessageDAO stockMsg = new StockMessageDAO(conn);
				/*
				 * Document messageXML = null; logger.info(
				 * "Inputs : messageType : Email, originId : null, - companyId : "
				 * +companyId); messageXML =
				 * stockMsg.loadMessageTitlesXML("Email", null, companyId, "N",
				 * "N", null); DOMUtil.addSection(this.responseDocument,
				 * messageXML.getChildNodes());
				 */

				PartnerVO partnerVO = FTDCommonUtils
						.getPreferredPartnerBySource(sourceCode);
				PartnerMappingVO partnerMappingVO = new PartnerUtility()
						.getPartnerOriginsInfo(originId, sourceCode, conn);
				boolean isMercentOrder = false;
				if (originId != null) {
					String originList = ConfigurationUtil.getInstance()
							.getProperty(COMConstants.PROPERTY_FILE,
									COMConstants.EMAIL_ORIGINS);

					if (!(originList.indexOf(originId) >= 0)) {
						MercentOrderPrefixes orderPrefixes = new MercentOrderPrefixes(
								conn);
						List<String> origins = orderPrefixes
								.getMercentOrigins();

						if (origins.contains((String) originId)) {
							isMercentOrder = true;
						}
					}
					if (originList.indexOf(originId) >= 0 || isMercentOrder
							|| (partnerMappingVO != null)) {
						// keep the origin id
					} else {
						originId = null;
					}
				}

				Document msgXML = null;

				if (partnerVO == null) {
					// For partner orders check if the flag is 'ON'
					if (partnerMappingVO != null) {
						if (partnerMappingVO.getCreateStockEmail().equals("Y")) {
							msgXML = stockMsg.loadMessageTitlesXML(
									COMConstants.EMAIL_MESSAGE_TYPE, originId,
									companyId, "N", "N", null);
							logger.info("create stock email is set to 'Y' for partner orders, show stock emails for origin "
									+ originId);
						} else {
							msgXML = stockMsg.loadMessageTitlesXML(
									COMConstants.EMAIL_MESSAGE_TYPE, null,
									companyId, "N", "N", null);
							logger.info("create stock email is set to 'N' for partner orders, show FTD stock email titles");
						}
					} else {
						logger.info("Inputs : messageType: "
								+ COMConstants.EMAIL_MESSAGE_TYPE
								+ ", originId : " + originId
								+ " - companyId : " + companyId);
						msgXML = stockMsg.loadMessageTitlesXML(
								COMConstants.EMAIL_MESSAGE_TYPE, originId,
								companyId, "N", "N", null);
						logger.info("Show stock emails for origin " + originId);
					}
				} else {
					msgXML = stockMsg.loadMessageTitlesXML(
							COMConstants.EMAIL_MESSAGE_TYPE, null, companyId,
							"N", "N", partnerVO.getPartnerName());
					logger.info("Show stock emails for partner "
							+ partnerVO.getPartnerName());

				}

				DOMUtil.addSection(this.responseDocument,
						msgXML.getChildNodes());

				String xslName = "PhoenixOrderIFrame";

				File xslFile = getXSL(xslName, mapping);

				pageData.put("success", COMConstants.CONS_YES);
				DOMUtil.addSection(this.responseDocument, "pageData", "data",
						pageData, true);

				forward = mapping.findForward(xslName);
				String xslFilePathAndName = forward.getPath(); // get real file name

				// Change to client side transform
				TraxUtil.getInstance().transform(
								request,
								response,
								responseDocument,
								xslFile,
								xslFilePathAndName,
								(HashMap) request
										.getAttribute(COMConstants.CONS_APP_PARAMETERS));
				return null;
			} else {
				if (logger.isDebugEnabled())
					logger.debug("Phoenix order Action failed due to no delivery dates in PAS--- ");
			}
		}// end try
		catch (Throwable ex) {
			try {
				logger.error("Error occured on load of PhoenixOrderAction ..>"
						+ ex.getMessage());
				String xslName = "Error";
				forward = mapping.findForward(xslName);
				return forward;
			} catch (Exception e) {
				if (logger.isDebugEnabled())
					logger.debug("Phoenix order Action --- " + e.toString());
				logger.error(e);
			}
		} finally {
			try {
				conn.close();
			} catch (SQLException se) {
				logger.error(se);
			}
		}
		return null;
	}

	private HashMap<String, String> sortHashMapOnKeys(
			HashMap<String, String> input) {

		List<String> mapKeys = new ArrayList<String>(input.keySet());

		HashMap<String, String> sortedMap = new LinkedHashMap<String, String>();
		TreeSet<String> sortedSet = new TreeSet<String>(mapKeys);

		for (String date : sortedSet) {
			sortedMap.put(date, input.get(date));
		}
		return sortedMap;

	}

	@SuppressWarnings("rawtypes")
	private ActionForward validateShipDateNMethod(ActionMapping mapping,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		Connection conn = null;
		try {
			String recipZipCode = (String) request
					.getParameter("recip_zip_code");
			String[] phoenixProductList = ((String) request
					.getParameter("phoenix_product_id")).split(",");
			String phoenixProductId = phoenixProductList[0];
			SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
			Date deliveryDate = formatter.parse(request
					.getParameter("delivery_date"));
			String addons = (String) request.getParameter("addons");
			String sourceCode = (String) request.getParameter("sourceCode");

			logger.info("validateShipDateNMethod addons: " + addons);
			
			conn = DataSourceUtil.getInstance().getConnection(
					ConfigurationUtil.getInstance().getProperty(
							COMConstants.PROPERTY_FILE,
							COMConstants.DATASOURCE_NAME));
			PhoenixOrderBO bo = new PhoenixOrderBO(conn);
			Map<String, String> shipDateNMethodMap = bo.getShipMethodNDate(
					phoenixProductId, deliveryDate, recipZipCode, addons,
					sourceCode);
			StringBuffer sb = new StringBuffer();
			sb.append("{");
			if (shipDateNMethodMap != null) {
				Set set = shipDateNMethodMap.keySet();
				Iterator iter = set.iterator();
				int i = 0;
				while (iter.hasNext()) {
					String key = (String) iter.next();
					sb.append("'" + key + "':'"
							+ (String) shipDateNMethodMap.get(key) + "'");
					if (i == 0) {
						sb.append(",");
					}
					i++;
				}

			}
			sb.append("}");

			String resStr = sb.toString();
			// ship method and date are not available so sending empty JSON
			// string.
			if (StringUtils.isEmpty(resStr)) {
				resStr = "{}";
			}
			response.getOutputStream().write(resStr.getBytes());

		} catch (Exception e) {
			logger.error("Error occured while validating Ship method and Ship date"
					+ e.getMessage());
			response.getOutputStream().write("{}".getBytes());
		} finally {
			try {
				if (!conn.isClosed()) {
					conn.close();
				}
			} catch (SQLException e) {
				logger.error("Error closing connection");
			}
		}
		return null;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private ActionForward submit(ActionMapping mapping,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		HashMap pageData = new HashMap();
		Connection conn = null;
		ActionForward forward = null;
		try {
			this.responseDocument = DOMUtil.getDocument();
			conn = DataSourceUtil.getInstance().getConnection(
					ConfigurationUtil.getInstance().getProperty(
							COMConstants.PROPERTY_FILE,
							COMConstants.DATASOURCE_NAME));
			getRequestInfo(request);
			String orderDetailId = (String) request
					.getParameter(COMConstants.ORDER_DETAIL_ID);
			String recipZipCode = (String) request
					.getParameter("recip_zip_code");
			String[] phoenixProductList = ((String) request
					.getParameter("phoenix_product_id")).split(",");
			String phoenixProductId = phoenixProductList[0];
			String externalOrderNumber = (String) request
					.getParameter("external_order_number");
			String orderGUId = (String) request.getParameter("order_guid");
			String customerId = (String) request.getParameter("customer_id");
			String masterOrderNumber = (String) request
					.getParameter("master_order_number");
			String shipMethod = (String) request.getParameter("ship_method");
			String shipDate = (String) request.getParameter("ship_date");
			String emailTypeId = (String) request.getParameter("emailType");
			String addons = (String) request.getParameter("addons");
			String orderComments = (String) request
					.getParameter("orderComments");
			String emailBody = (String) request.getParameter("newBody");
			String emailSubject = (String) request.getParameter("emailSubject");
			String emailTitle = (String) request.getParameter("emailTitle");
			String startOrigin = (String) request.getParameter("start_origin");
			logger.info("submit addons: " + addons);
			pageData.put("external_order_number", externalOrderNumber);
			pageData.put("order_guid", orderGUId);
			pageData.put("customer_id", customerId);
			pageData.put("master_order_number", masterOrderNumber);
			pageData.put("order_detail_id", orderDetailId);
			pageData.put("recip_zip_code", recipZipCode);
			pageData.put("phoenix_product_id", phoenixProductId);

			SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
			Date deliveryDate = formatter.parse(request
					.getParameter("deliveryDate"));
			orderDetailId = request.getParameter(COMConstants.ORDER_DETAIL_ID);
			String cancelFlag = request.getParameter("cancelId");
			String cancelText = null;
			if (cancelFlag != null && cancelFlag.equalsIgnoreCase("YES")) {
				cancelText = request.getParameter("cancelText");
			}
			/*
			 * SecurityManager sm = SecurityManager.getInstance(); UserInfo ui =
			 * sm.getUserInfo(this.sessionId); this.csrId = ui.getUserID();
			 */

			PhoenixOrderBO phoenixOrderBO = new PhoenixOrderBO(conn);
			phoenixOrderBO.savePhoenixOrder(orderDetailId, deliveryDate,
					cancelText, shipMethod, shipDate, this.csrId, emailTypeId,
					emailBody, emailSubject, orderComments, phoenixProductId,
					emailTitle, startOrigin);

			// Convert the page data hashmap to XML and append it to the final
			// XML
			DOMUtil.addSection(responseDocument, "pageData", "data",
					this.pageData, true);

			if (logger.isDebugEnabled()) {
				// IMPORTANT: Because StringWriter contents are NOT removed even
				// when the flush
				// method is executed, make StringWriter go away through scope
				// control.
				// Both the flush and close methods were tested with but were of
				// none affect.
				StringWriter sw = new StringWriter(); // string representation
														// of xml document
				DOMUtil.print((Document) responseDocument, new PrintWriter(sw));
				logger.debug("Cancel Phoenix Order Action" + sw.toString());
			}

			// String xslName = COMConstants.XSL_CANCEL_PHOENIX_ORDER_IFRAME;
			//String xslName = "customerSearchAction";

			request.setAttribute(COMConstants.ACTION,
					COMConstants.ACTION_CUSTOMER_ACCOUNT_SEARCH);
			request.setAttribute(COMConstants.ORDER_NUMBER, externalOrderNumber);
			request.setAttribute(COMConstants.ORDER_GUID, orderGUId);
			request.setAttribute(COMConstants.CUSTOMER_ID, customerId);
			request.setAttribute(
					COMConstants.SEARCHED_ON_EXTERNAL_ORDER_NUMBER,
					externalOrderNumber);

			forward = mapping.findForward("customerSearchAction");

			return forward;

			/*
			 * File xslFile = getXSL(xslName, mapping); forward =
			 * mapping.findForward(xslName); String xslFilePathAndName =
			 * forward.getPath(); //get real file name
			 * 
			 * // Change to client side transform
			 * TraxUtil.getInstance().getInstance().transform(request, response,
			 * responseDocument, xslFile, xslFilePathAndName,
			 * (HashMap)request.getAttribute(COMConstants.CONS_APP_PARAMETERS));
			 * 
			 * return null;
			 */

		} catch (Throwable t) {
			try {

				logger.error("Error occured in submit method ..>"
						+ t.getMessage());
				String xslName = "Error";
				forward = mapping.findForward(xslName);
				return forward;

			} catch (Exception e) {
				if (logger.isDebugEnabled()) {
					logger.debug("Phoenix order Action -- submit --- "
							+ e.toString());
				}
				logger.error(e);
			}
		} finally {
			try {
				conn.close();
			} catch (SQLException se) {
				logger.error(se);
			}
		}
		return null;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private ActionForward cancel(ActionMapping mapping,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		ActionForward forward = null; 
		Document responseDocument = null;
		String xslName = COMConstants.XSL_CANCEL_PHOENIX_ORDER_IFRAME;
		File xslFile = getXSL(xslName, mapping);
		HashMap pageData = new HashMap();

		try {
			// Document that will contain the final XML, to be passed to the
			// TransUtil and XSL page
			responseDocument = (Document) DOMUtil.getDocument();

			// Get info passed in the request object 
			getRequestInfo(request);

			// Convert the page data hashmap to XML and append it to the final
			// XML
			DOMUtil.addSection(responseDocument, "pageData", "data",
					this.pageData, true);

			if (logger.isDebugEnabled()) {
				// IMPORTANT: Because StringWriter contents are NOT removed even
				// when the flush
				// method is executed, make StringWriter go away through scope
				// control.
				// Both the flush and close methods were tested with but were of
				// none affect.
				StringWriter sw = new StringWriter(); // string representation
														// of xml document
				DOMUtil.print((Document) responseDocument, new PrintWriter(sw));
				logger.debug("Cancel Phoenix Order Action" + sw.toString());
			}

			forward = mapping.findForward(xslName);
			String xslFilePathAndName = forward.getPath(); // get real file name

			// Change to client side transform
			TraxUtil.getInstance().transform(
							request,
							response,
							responseDocument,
							xslFile,
							xslFilePathAndName,
							(HashMap) request.getAttribute(COMConstants.CONS_APP_PARAMETERS));
			DOMUtil.print(responseDocument, System.out);

			return null;
		} catch (Exception e) {
			try {
				logger.error(e);
				forward = mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);

				// set the error data
				pageData.clear();
				pageData.put("errorFlag", "Y");
				pageData.put("errorUrl", forward.getPath());
				DOMUtil.addSection(responseDocument, "errorData", "data",
						this.pageData, true);

				String xslFilePathAndName = forward.getPath(); // get real file name

				// Change to client side transform
				TraxUtil.getInstance().transform(
								request,
								response,
								responseDocument,
								xslFile,
								xslFilePathAndName,
								(HashMap) request.getAttribute(COMConstants.CONS_APP_PARAMETERS));

				return null;
			} catch (Exception ex) {
				// if logging is enabled, display the input parameters
				if (logger.isDebugEnabled()) {
					logger.debug("Cancel Phoenix Order Action --- CSR "
							+ this.csrId
							+ " canceling out of external order number"
							+ this.externalOrderNumber + " in Phoenix Order");
				}

				logger.error("Failed trying to send back error url - " + ex);
				forward = mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
				return forward;
			}
		}

	}

	/******************************************************************************
	 * getXSL()
	 *******************************************************************************
	 * Retrieve xsl file name
	 * 
	 * @param1 String - the xsl name returned from the Business Object
	 * @param2 ActionMapping
	 * @return File - XSL File name
	 */

	private File getXSL(String xslName, ActionMapping mapping) {
		File xslFile = null;
		String xslFilePathAndName = null;

		ActionForward forward = mapping.findForward(xslName);
		xslFilePathAndName = forward.getPath(); // get real file name
		xslFile = new File(this.getServlet().getServletContext()
				.getRealPath(xslFilePathAndName));
		return xslFile;
	}

	/******************************************************************************
	 * getRequestInfo(HttpServletRequest request)
	 *******************************************************************************
	 * Retrieve the info from the request object, and set class level variables
	 * 
	 * @param HttpServletRequest
	 * @return none
	 * @throws none
	 */
	@SuppressWarnings("unchecked")
	private void getRequestInfo(HttpServletRequest request) throws Exception {
		ConfigurationUtil cu = null;
		cu = ConfigurationUtil.getInstance();

		// retrieve the customer id
		if (request.getParameter(COMConstants.CUSTOMER_ID) != null) {
			this.customerId = request.getParameter(COMConstants.CUSTOMER_ID);
			this.pageData.put(COMConstants.PD_CUSTOMER_ID, this.customerId);
		}

		// retrieve the item order number
		if (request.getParameter(COMConstants.EXTERNAL_ORDER_NUMBER) != null) {
			this.externalOrderNumber = request
					.getParameter(COMConstants.EXTERNAL_ORDER_NUMBER);
			this.pageData.put(COMConstants.PD_EXTERNAL_ORDER_NUMBER,
					this.externalOrderNumber);
		}

		// retrieve the master order number
		if (request.getParameter(COMConstants.MASTER_ORDER_NUMBER) != null) {
			this.masterOrderNumber = request
					.getParameter(COMConstants.MASTER_ORDER_NUMBER);
			this.pageData.put(COMConstants.PD_MASTER_ORDER_NUMBER,
					this.masterOrderNumber);
		}

		// retrieve the order detail id
		if (request.getParameter(COMConstants.ORDER_DETAIL_ID) != null) {
			this.orderDetailId = request
					.getParameter(COMConstants.ORDER_DETAIL_ID);
			this.pageData.put(COMConstants.PD_ORDER_DETAIL_ID,
					this.orderDetailId);
		}

		// retrieve the order guid
		if (request.getParameter(COMConstants.ORDER_GUID) != null) {
			this.orderGuid = request.getParameter(COMConstants.ORDER_GUID);
			this.pageData.put(COMConstants.PD_ORDER_GUID, this.orderGuid);
		}

		/****************************************************************************************
		 * Retrieve Security Info
		 ***************************************************************************************/

		this.sessionId = request.getParameter(COMConstants.SEC_TOKEN);

		// get csr Id
		String security = (String) cu.getProperty(COMConstants.PROPERTY_FILE,
				COMConstants.SECURITY_IS_ON);
		boolean securityIsOn = security.equalsIgnoreCase("true") ? true : false;

		if (securityIsOn || this.sessionId != null) {
			SecurityManager sm = SecurityManager.getInstance();
			UserInfo ui = sm.getUserInfo(this.sessionId);
			this.csrId = ui.getUserID();
		}

	}

}
