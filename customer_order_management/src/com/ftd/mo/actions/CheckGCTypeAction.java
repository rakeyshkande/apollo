package com.ftd.mo.actions;

import com.ftd.customerordermanagement.bo.GiftCardBO;
import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.dao.OrderDAO;
import com.ftd.customerordermanagement.vo.BillingVO;
import com.ftd.customerordermanagement.vo.CommonCreditCardVO;
import com.ftd.customerordermanagement.vo.CreditCardVO;
import com.ftd.customerordermanagement.vo.GcCouponVO;
import com.ftd.customerordermanagement.vo.PaymentVO;
import com.ftd.ftdutilities.FTDCommonUtils;
import com.ftd.mo.bo.PaymentBO;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityManager;

import com.ftd.security.cache.vo.UserInfo;
import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;

import com.ftd.osp.utilities.ConfigurationUtil;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Element;


public final class CheckGCTypeAction extends Action
{
    private static Logger logger  = new Logger("com.ftd.mo.actions.CheckGCTypeAction");

    // Class level constants
    private static final String DATA_ROOT_NODE = "root";

    /**
     * Action called to determine if gcc_num is a gift card or gift certificate. .
     * 
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     */
    public  ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
            HttpServletResponse response)
    {
        // Variables
        ActionForward forward = null;
        Connection conn = null;
        Document statusXML = null;

        try
        {
            // Obtain connection
            conn = DataSourceUtil.getInstance().getConnection(ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE,
                    COMConstants.DATASOURCE_NAME));


            boolean gdOrGc = isGiftCard(request, conn);
            // determine if this is a gd or gc number
            if (gdOrGc)
            {
                statusXML = createStatusDocument(COMConstants.GIFT_CARD_PAYMENT);
            }
            else
            {
                statusXML = createStatusDocument(COMConstants.GIFT_CERTIFICATE_PAYMENT);
            }

            if(logger.isDebugEnabled())
            {
                //IMPORTANT:  Because StringWriter contents are NOT removed even when the flush
                //            method is executed, make StringWriter go away through scope control.
                //            Both the flush and close methods were tested with but were of none affect.
                StringWriter sw = new StringWriter();       //string representation of xml document
                DOMUtil.print((Document) statusXML, new PrintWriter(sw));
                logger.debug("- execute(): COM returned = \n" + sw.toString());
            }

            // Send back status document as XML.  The front end utilizes AJAX
            response.setContentType("text/xml");
            DOMUtil.print(statusXML,response.getWriter());
        }
        catch(Exception e)
        {
            //if logging is enabled, display the customer id, and external and master order #s
            if(logger.isDebugEnabled())
            {
                logger.debug("Update Payment Action --- Order Detail Id = " +
                        request.getParameter(COMConstants.ORDER_DETAIL_ID) +
                        " and securitytoken = " +
                        request.getParameter(COMConstants.SEC_TOKEN));
            }

            logger.error(e);

            // Send back error status document
            try
            {
                statusXML = createErrorStatus();
                DOMUtil.print(statusXML,response.getWriter());
            }
            catch(Exception e1)
            {
                logger.error(e1);
            }
        }

        finally
        {
            try
            {
                // Close the connection
                if(conn != null)
                {
                    conn.close();
                }
            }
            catch(Exception e)
            {
                logger.error(e);

                // Send back error status document
                try
                {
                    statusXML = createErrorStatus();
                    DOMUtil.print(statusXML,response.getWriter());
                }
                catch(Exception e1)
                {
                    logger.error(e1);
                }
            }
        }

        return null;
    }

    // call store proc to determine if this a valid gift card - if gift card payment is not enabled then treat like a non-GD
    private boolean isGiftCard(HttpServletRequest request, Connection conn) throws Exception
    {
        String gdenabled = request.getParameter("giftCardPaymentEnabled");
        if ("N".equals(gdenabled))
            return false;
        
        String cardnum = GiftCardBO.getUnmaskedGiftCardNum(request.getParameter("gcc_num"), request.getParameter(COMConstants.ORDER_GUID), conn);
        
        String cardType = FTDCommonUtils.getPaymentMethodIdByNumber(cardnum, false);
        logger.info("gcc num is of type " + cardType); 
        if ("GD".equals(cardType))
            return true;
        else
            return false;
    }


    /**
     * Create error status document
     * @return Document - status
     * @throws Exception
     */

    private Document createStatusDocument(String gdorgc) throws Exception
    {
        Document returnDocument = DOMUtil.getDefaultDocument();
        Element root = (Element) returnDocument.createElement(DATA_ROOT_NODE);
        returnDocument.appendChild(root);

        Element errorNode = (Element)returnDocument.createElement("is_error");
        errorNode.appendChild(returnDocument.createTextNode("N"));
        root.appendChild(errorNode);

        Element gccNode = (Element)returnDocument.createElement(COMConstants.GD_OR_GC);
        gccNode.appendChild(returnDocument.createTextNode(gdorgc));
        root.appendChild(gccNode);

        return returnDocument;  
    }

    /**
     * Create error status document
     * @return Document - status
     * @throws Exception
     */

    private Document createErrorStatus()
    throws Exception
    {
        Document returnDocument = DOMUtil.getDefaultDocument();
        Element root = (Element) returnDocument.createElement(DATA_ROOT_NODE);
        returnDocument.appendChild(root);

        Element errorNode = (Element)returnDocument.createElement("is_error");
        errorNode.appendChild(returnDocument.createTextNode("Y"));
        root.appendChild(errorNode);
        
        Element gccNode = (Element)returnDocument.createElement(COMConstants.GD_OR_GC);
        gccNode.appendChild(returnDocument.createTextNode("GC"));
        root.appendChild(gccNode);

        return returnDocument;  
    }
}