package com.ftd.mo.actions;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.dao.OrderDAO;
import com.ftd.customerordermanagement.util.COMDeliveryDateUtil;
import com.ftd.ftdutilities.FieldUtils;
import com.ftd.messaging.dao.MessageDAO;
import com.ftd.messaging.vo.FTDMessageVO;
import com.ftd.mo.dao.UpdateOrderDAO;
import com.ftd.mo.util.ModifyOrderUTIL;
import com.ftd.mo.vo.MessageStatusVO;
import com.ftd.osp.utilities.AddOnUtility;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.handlers.GlobalParmHandler;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityManager;


/**
 * Update Delivery Date Validation Class.
 *
 */

public final class UpdateDeliveryDateValidationAction extends Action
{
    private static Logger logger  = new Logger("com.ftd.mo.actions.UpdateDeliveryDateValidationAction");

    private static String XSL_UPDATE_DELIVERY_DATE_VALIDATION_IFRAME   = "UpdateDeliveryDateValidationIFrame";
    
    private String status                   = null; 
    private String vendorDeliveryAllowed    = null;    

    //Document that will contain the final XML, to be passed to the TraxUtil and XSL page
    private Document responseDocument;

  /**
   * This is the main action called from the Struts framework.
   *
   * @param mapping The ActionMapping used to select this instance.
   * @param form The optional ActionForm bean for this request.
   * @param request The HTTP Request we are processing.
   * @param response The HTTP Response we are processing.
   */
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
        Connection conn = null;
        HashMap pageData = new HashMap();

        try
        {
          this.responseDocument = (Document) DOMUtil.getDocument();

          // Set request variables
          String orderDetailId = (String) request.getParameter(COMConstants.ORDER_DETAIL_ID);
          String vendorFlag = (String) request.getParameter(COMConstants.VENDOR_FLAG);

          String productId = (String) request.getParameter(COMConstants.PRODUCT_ID);
          String mercuryFlag = (String) request.getParameter("mercury_flag");
          String floristId = (String) request.getParameter("florist_id");
          
          String messageType = (vendorFlag != null && vendorFlag.equals("Y")) ? "Venus" : "Mercury";
          
          BigDecimal actualDeliveryDaysPast = null;

          //retrieve the session id and user id
          String securityToken = (String) request.getParameter(COMConstants.SEC_TOKEN);

          // Pull DB connection
          conn = DataSourceUtil.getInstance().getConnection(ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, COMConstants.DATASOURCE_NAME));

          if( orderDetailId != null)
          {
            String securityFlag = ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, COMConstants.SECURITY_IS_ON);
            if(securityFlag != null && securityFlag.equals("true"))
            {
              if (SecurityManager.getInstance().getUserInfo(securityToken) != null)
              {

                // Instantiate OrderDAO
                OrderDAO orderDAO = new OrderDAO(conn);
                // Instantiate UpdateOrderDAO
                UpdateOrderDAO uoDAO = new UpdateOrderDAO(conn);
                
                ModifyOrderUTIL moUTIL = new ModifyOrderUTIL(conn);
                MessageStatusVO msVO =  moUTIL.getMessageStatus(orderDetailId, messageType, "Y");

                FTDMessageVO lastFtdMessageVO = moUTIL.getMessageDetail(orderDetailId, messageType);
                boolean zjOrderInTransit = moUTIL.isZJOrderInTransit(orderDetailId, lastFtdMessageVO);
                String sdsStatus = lastFtdMessageVO.getSdsStatus();
                Date dDeliveryDate = lastFtdMessageVO.getDeliveryDate();
                Date dShipDate = lastFtdMessageVO.getShipDate();

                // global max delivery days past flag for update delivery date
                GlobalParmHandler globalParms = (GlobalParmHandler)CacheManager.getInstance().getHandler("CACHE_NAME_GLOBAL_PARM");
                BigDecimal maxDeliveryDaysPast = new BigDecimal(globalParms.getGlobalParm("UPDATE_DELIVERY_DATE", "MAX_DELIVERY_DAYS_PAST"));
                BigDecimal maxShipDaysOut = new BigDecimal(globalParms.getGlobalParm("SHIPPING_PARMS", "MAX_SHIP_DAYS_OUT"));

                Calendar cMaxDeliveryDate = Calendar.getInstance();
                 
                if (dDeliveryDate != null)
                {
                  //Define the format
                  SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
              
                  //set max delivery days in the past
                  //delivery date - max delivery days out
                  cMaxDeliveryDate.add(Calendar.DATE, -(maxDeliveryDaysPast.intValue()));
                  cMaxDeliveryDate.set(Calendar.HOUR, 0); 
                  cMaxDeliveryDate.set(Calendar.MINUTE, 0); 
                  cMaxDeliveryDate.set(Calendar.SECOND, 0); 
                  cMaxDeliveryDate.set(Calendar.MILLISECOND, 0);
                  cMaxDeliveryDate.set( Calendar.AM_PM, Calendar.AM );
              
                  actualDeliveryDaysPast = new BigDecimal(COMDeliveryDateUtil.getDiffInDays(dDeliveryDate, cMaxDeliveryDate.getTime()));
                }
              
                
                String flowerMonthlyIndicator = uoDAO.isFlowerMonthlyProduct(productId);
                getProductInfo(productId, conn);
                boolean validationPassed = true;               
                
                //check if order has add ons associated with it.  If it does, check to see
                //if the add ons are still available
                AddOnUtility addOnUTIL = new AddOnUtility();
                boolean orderAddOnsAvailable = addOnUTIL.orderAddOnAvailable(orderDetailId, conn);  
               
                /**********************************************************************************
                 * All Flowers of the Month orders should be handled manually by the special accounts team, until 
                 * an automated solution for handling FOTM orders has been established.
                 **********************************************************************************/
                if (validationPassed &&
                    flowerMonthlyIndicator != null &&
                    flowerMonthlyIndicator.equalsIgnoreCase(COMConstants.CONS_YES))
                {
                  pageData.put(COMConstants.PD_MESSAGE_DISPLAY,   COMConstants.UDD_FLOWER_MONTHLY_PRODUCT);
                  pageData.put("success",                         COMConstants.CONS_NO);
                  validationPassed = false;
                }
                
                /**********************************************************************************
                * The system will not allow FTDM type orders to by updated through the 
                * Update Delivery Date application.  Please note this check only applies to florist orders.
                **********************************************************************************/
                if (validationPassed &&
                    floristId != null && 
                    !floristId.equals("") && 
                    vendorFlag != null && 
                    vendorFlag.equalsIgnoreCase(COMConstants.CONS_NO))
                {
                  if(mercuryFlag == null || mercuryFlag.equals(""))
                  {
                    pageData.put(COMConstants.PD_MESSAGE_DISPLAY,   COMConstants.UDD_FTDM_ORDER);
                    pageData.put("success",                         COMConstants.CONS_NO);
                    validationPassed= false;
                  }
                }
                
                 /**********************************************************************************
                  * The system will not allow FTDC type orders to by updated through the 
                  * Update Delivery Date application.  Please note this check applies to both
                  * florist and vendor orders.
                  **********************************************************************************/
                  if (validationPassed &&
                     floristId != null && 
                     !floristId.equals("")  
                     )
                  {
                   if(msVO.isCompOrder())
                   {
                     pageData.put(COMConstants.PD_MESSAGE_DISPLAY,   COMConstants.UDD_FTDC_ORDER);
                     pageData.put("success",                         COMConstants.CONS_NO);
                     validationPassed= false;
                   }
                  }

                /**********************************************************************************
                 * The system will not allow Mercury or Venus orders where the live mercury message has not been 
                 * verified to be updated through the Update Delivery Date application.
                 **********************************************************************************/
                if(validationPassed)
                {
                  if ( !msVO.isLiveMercury())
                     
                  {
                    pageData.put(COMConstants.PD_MESSAGE_DISPLAY,   COMConstants.UDD_NO_LIVE_MERCURY);
                    pageData.put("success",                         COMConstants.CONS_NO);
                    validationPassed = false;
                  }
                }

                /**********************************************************************************
                 * The system will not allow orders to be updated through the Update Delivery Date 
                 * application if delivery date is further out then the max delivery days in the past.
                 **********************************************************************************/
                if (validationPassed &&
                         actualDeliveryDaysPast.doubleValue() > 0)
                {
                    
                    pageData.put(COMConstants.PD_MESSAGE_DISPLAY, FieldUtils.replaceAll(COMConstants.UDD_DELIVERY_DATE_PAST_MAX_ERROR, "{max_number_of_days}", maxDeliveryDaysPast.toString()));
                    pageData.put("success",                         COMConstants.CONS_NO);
                    validationPassed = false;
                }
                  
                /**********************************************************************************
                 * The system will not allow Venus orders in Printed status to be updated through the 
                 * Update Delivery Date application if today's date is greater than or equal to printed on date and 
                 * < delivery date
                 * Defect 3735:  Need to look at sds status and not order disp code
                 **********************************************************************************/
                if (validationPassed && 
                    messageType.equals("Venus") && 
                    sdsStatus != null && sdsStatus.equalsIgnoreCase(COMConstants.SDS_STATUS_PRINTED) &&
                    moUTIL.isOrderInTransit(dShipDate, dDeliveryDate)
                   )
                {
                    pageData.put(COMConstants.PD_MESSAGE_DISPLAY,   COMConstants.UDD_PRINTED_ORDER_ERROR);
                    pageData.put("success",                         COMConstants.CONS_NO);
                    validationPassed = false;
                }
                /**********************************************************************************
                 * The system will not allow Venus orders in Shipped status to be updated through the 
                 * Update Delivery Date application if delivery date is prior to today's date. 
                 * Defect 3735:  Need to look at sds status and not order disp code
                 **********************************************************************************/
                if (validationPassed && 
                    messageType.equals("Venus") && 
                    sdsStatus != null && sdsStatus.equalsIgnoreCase(COMConstants.SDS_STATUS_SHIPPED) &&
                    moUTIL.isOrderInTransit(dShipDate, dDeliveryDate)
                   )
                {
                    pageData.put(COMConstants.PD_MESSAGE_DISPLAY,   COMConstants.UDD_SHIPPED_STATUS_BEFORE_DELIVERY_DATE);
                    pageData.put("success",                         COMConstants.CONS_NO);
                    validationPassed = false;
                }
                
                /**********************************************************************************
                 * The system will not allow orders where the product is no longer available 
                 * to be updated through the Update Delivery Date application
                 **********************************************************************************/
                if (validationPassed && this.status.equalsIgnoreCase("U") )
                {
                  pageData.put(COMConstants.PD_MESSAGE_DISPLAY,   COMConstants.UDD_UNAVAILABLE_ERROR);
                  pageData.put("success",                         COMConstants.CONS_NO);
                  validationPassed = false;
                }
                /**********************************************************************************
                 * The system will not allow orders where the vase and/or add on is no longer available 
                 * to be updated through the Update Delivery Date application
                 **********************************************************************************/
                if (validationPassed && !orderAddOnsAvailable ) 
                {
                  pageData.put(COMConstants.PD_MESSAGE_DISPLAY,   COMConstants.UDD_ADD_ON_UNAVAILABLE_ERROR);
                  pageData.put("success",                         COMConstants.CONS_NO);
                  validationPassed = false;
                }
                /**********************************************************************************
                 * The system will not allow orders where the vendor is no longer available 
                 * to be updated through the Update Delivery Date application
                 **********************************************************************************/
                if (validationPassed && messageType.equals("Venus") && this.vendorDeliveryAllowed.equalsIgnoreCase(COMConstants.CONS_NO))
                {
                    pageData.put(COMConstants.PD_MESSAGE_DISPLAY,   COMConstants.UDD_UNAVAILABLE_ERROR);
                    pageData.put("success",                         COMConstants.CONS_NO);
                    validationPassed = false;
                }
                /**********************************************************************************
                 * -----Validation Rule -----
                 *                 
                 * Defect 2638 - Zone Jump
                 * Do not allow csr to do anything to the order once a Zone Jump label is printed
                 * and it is after the zone jump label date
                 **********************************************************************************/
                 if (validationPassed && zjOrderInTransit)
                 {
                   pageData.put(COMConstants.PD_MESSAGE_DISPLAY,   COMConstants.ZJ_ORDER_IN_TRANSIT);
                   pageData.put("success",                         COMConstants.CONS_NO);
                   validationPassed = false;
                 }
                /**********************************************************************************
                 * If everything has been verified
                 **********************************************************************************/
                if (validationPassed)
                {
                  pageData.put("success",                         COMConstants.CONS_YES);
                }
              }//end "if (SecurityManager..."
            }//end "if (securityFlag..."
          }//end else if(action.equalsIgnoreCase("retrieve"))

          else
          {
            //set an output message
            String message = "Order Detail Id was missing.";

            pageData.put(COMConstants.PD_MESSAGE_DISPLAY,         message);
            pageData.put("success",                               COMConstants.CONS_NO);
          }

          //Convert the page data hashmap to XML and append it to the final XML
          DOMUtil.addSection(this.responseDocument, "pageData", "data", pageData, true);

          //set the iFrame name to be given control back to.
          String xslName = COMConstants.XSL_UDD_VALIDATION_IFRAME;

          //Get XSL File name
          File xslFile = getXSL(xslName, mapping);

          ActionForward forward = mapping.findForward(xslName);
          String xslFilePathAndName = forward.getPath(); //get real file name
          
          // Change to client side transform
          TraxUtil.getInstance().getInstance().transform(request, response, responseDocument, 
                              xslFile, xslFilePathAndName, (HashMap)request.getAttribute(COMConstants.CONS_APP_PARAMETERS));

        }//end try


      catch (Throwable ex)
      {
        try
        {
          //if logging is enabled, display the input parms
          if(logger.isDebugEnabled())
          {
            logger.debug("Update Delivery Date Validation Action --- OrderDetailId = " + request.getParameter("order_detail_id"));
          }

          logger.error(ex);

          //set an output message
          String message = "Unexpected System Error.";

          pageData.put(COMConstants.PD_MESSAGE_DISPLAY,         message);
          pageData.put("success",                               COMConstants.CONS_NO);

          //Convert the page data hashmap to XML and append it to the final XML
          DOMUtil.addSection(this.responseDocument, "pageData", "data", pageData, true);

          //set the iFrame name to be given control back to.
          String xslName = COMConstants.XSL_UDD_VALIDATION_IFRAME;

          //Get XSL File name
          File xslFile = getXSL(xslName, mapping);

          ActionForward forward = mapping.findForward(xslName);
          String xslFilePathAndName = forward.getPath(); //get real file name
          
          // Change to client side transform
          TraxUtil.getInstance().getInstance().transform(request, response, responseDocument, 
                              xslFile, xslFilePathAndName, (HashMap)request.getAttribute(COMConstants.CONS_APP_PARAMETERS));
        }
        catch (Exception e)
        {
          if(logger.isDebugEnabled())
          {
            logger.debug("Update Delivery Date Validation Action --- " + e.toString());
          }
          logger.error(e);
        }
      }
      finally
      {
        try
        {
          conn.close();
        }
        catch (SQLException se)
        {
          logger.error(se);
        }
      }

      return null;
    }


  /******************************************************************************
  *                                     getXSL()
  *******************************************************************************
  * Retrieve xsl file name
  * @param1 String - the xsl name returned from the Business Object
  * @param2 ActionMapping
  * @return File - XSL File name
  */

  private File getXSL(String xslName, ActionMapping mapping)
  {

    File xslFile = null;
    String xslFilePathAndName = null;

    ActionForward forward = mapping.findForward(xslName);
    xslFilePathAndName = forward.getPath();                //get real file name
    xslFile = new File(this.getServlet().getServletContext().getRealPath(xslFilePathAndName));
    return xslFile;
  }
  
    /**
     * Obtain the product status and the ship method carrier flag
     */
    private void getProductInfo(String productId, Connection dbConnection) throws Exception
     {
       MessageDAO messageDAO = new MessageDAO(dbConnection);
       
       CachedResultSet crs =  messageDAO.getProductInfo(productId);
       
       if(crs.next())
       {
         this.status = (String) crs.getString("status");
         this.vendorDeliveryAllowed = (String) crs.getString("shipMethodCarrier");
       }
     }


}