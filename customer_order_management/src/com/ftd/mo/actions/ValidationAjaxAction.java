package com.ftd.mo.actions;

import java.io.IOException;
import java.io.Writer;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.xml.sax.SAXException;

import com.ftd.customerordermanagement.bo.OrderBO;
import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.dao.OrderDAO;
import com.ftd.messaging.constants.MessagingConstants;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.PASServiceUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pas.core.domain.ProductAvailVO;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import com.thoughtworks.xstream.io.json.JsonHierarchicalStreamDriver;
import com.thoughtworks.xstream.io.json.JsonWriter;

public class ValidationAjaxAction extends DispatchAction {
	private static final Logger logger = new Logger("com.ftd.mo.actions.ValidationAjaxAction");
	public ValidationAjaxAction() {
	}

	
	public ActionForward validateState(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
		Connection conn = null;
		List<String> result = new ArrayList<String>();
		
		try{
			 //Connection/Database info
			conn = getDBConnection();
			OrderBO orderBO = new OrderBO(conn);
			String state = request.getParameter(MessagingConstants.REQUEST_RECIPIENT_STATE);
			String zipCode = request.getParameter(MessagingConstants.REQUEST_RECIP_ZIP_CODE);
			String city = request.getParameter(MessagingConstants.REQUEST_RECIPIENT_CITY);
			String outState = orderBO.getStateByZipcodeAndCity(zipCode, city);
			if(outState != null && state.equalsIgnoreCase(outState)){
				result.add("YES");
			}else{
				result.add("NO");
			}
			XStream xstream = new XStream(new JsonHierarchicalStreamDriver() {
		          public HierarchicalStreamWriter createWriter(Writer writer) {
		              return new JsonWriter(writer, JsonWriter.DROP_ROOT_MODE);
		          }
		    });
		    response.getOutputStream().write(xstream.toXML(result).getBytes());
			
		} catch (Throwable t) {
	        logger.error(t);
	        throw new RuntimeException(t);
	    }
		finally{
			if(conn != null && !conn.isClosed()){
				conn.close();
			}
		}
		return null;
	}
	
	/**
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 * This method is used to validate whether the given source code
	 * is valid for the gift card.
	 */
	public ActionForward validateGDSrcCd(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		Connection conn = null;
		List<String> result = new ArrayList<String>();
		try{
			conn = getDBConnection();
			OrderDAO orderDAO = new OrderDAO(conn);
			String sourceCode = request.getParameter("source_code");
			//logger.info("Source Code : "+sourceCode);
			boolean flag = orderDAO.getBillingInfoForCertificate(sourceCode);
			if(flag){
				result.add("YES");
			}else{
				result.add("NO");
			}
			//logger.info("Flag : "+flag);
			XStream xstream = new XStream(new JsonHierarchicalStreamDriver() {
		          public HierarchicalStreamWriter createWriter(Writer writer) {
		              return new JsonWriter(writer, JsonWriter.DROP_ROOT_MODE);
		          }
		    });
			//logger.info("Json : " +xstream.toXML(result));
		    response.getOutputStream().write(xstream.toXML(result).getBytes());
		} catch (Throwable t) {
	        logger.error(t);
	        throw new RuntimeException(t);
	    }
		finally{
			if(conn != null && !conn.isClosed()){
				conn.close();
			}
		}
		return null;
	}
	
	
	private Connection getDBConnection()
	throws IOException, ParserConfigurationException, SAXException, TransformerException, 
           Exception
    {
	    Connection conn = null;
		conn = DataSourceUtil.getInstance().getConnection(
				   ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE,
															   COMConstants.DATASOURCE_NAME));
		return conn;
    }

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ActionForward validateDeliveryDate(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
		Connection conn = null;
		List<String> result = new ArrayList<String>();
		// SimpleDateFormat sdfOutput = new SimpleDateFormat("EEE MMM dd hh:mm:ss z yyyy");
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		
		try{
			 //Connection/Database info
			conn = getDBConnection();
			String deliveryDateStr = request.getParameter("delivery_date");
			String zipCode = request.getParameter("zip_code");
			String productId = request.getParameter("product_id");
			Date deliveryDate = sdf.parse(deliveryDateStr);
			String orderAddons = request.getParameter("order_add_ons");
			String morningDelivery = request.getParameter("morning_delivery");
			String country = request.getParameter("country");
			String sourceCode = request.getParameter("source_code");
			
			logger.info("deliveryDate: " + deliveryDateStr + " productId: " + productId +
					" zipCode: " + zipCode + " orderAddOns: " + orderAddons + " " +
					" morningDelivery: " + morningDelivery + " country: " + country + "sourceCode: "+sourceCode);

            List addonIds = new ArrayList();
            if (orderAddons != null && orderAddons.length() > 0) {
            	addonIds = Arrays.asList(orderAddons.split(","));
            } 
            ProductAvailVO paVO = new ProductAvailVO();
            if (country == null || country.equals("")) {
            	country = "US";
            }
            if (country != null && (country.equalsIgnoreCase("US") ||
            		country.equalsIgnoreCase("CA"))) {
            	paVO = PASServiceUtil.getProductAvailability(productId, deliveryDate, zipCode, country, addonIds, sourceCode, false, true);
            } else {
            	boolean isProductAvailable = PASServiceUtil.isInternationalProductAvailable(productId, deliveryDate, country);
            	paVO.setIsAvailable(isProductAvailable);
            	paVO.setIsAddOnAvailable(false);
            	paVO.setIsMorningDeliveryAvailable(false);
            }
			logger.info("available: " + paVO.isIsAvailable() + " addons: " + paVO.isIsAddOnAvailable() +
					" morning del: " + paVO.isIsMorningDeliveryAvailable());
			if (paVO.isIsAvailable()) {
				if (!paVO.isIsAddOnAvailable()) {
					result.add("NO ADDON");
				} else  if (!paVO.isIsMorningDeliveryAvailable() && morningDelivery != null &&
						morningDelivery.equalsIgnoreCase("Y")) {
					result.add("NO MD");
				} else {
    		        result.add("YES");
				}
			} else {
				result.add("NO");
			}

			XStream xstream = new XStream(new JsonHierarchicalStreamDriver() {
		          public HierarchicalStreamWriter createWriter(Writer writer) {
		              return new JsonWriter(writer, JsonWriter.DROP_ROOT_MODE);
		          }
		    });
		    response.getOutputStream().write(xstream.toXML(result).getBytes());
			
		} catch (Throwable t) {
	        logger.error(t);
	        throw new RuntimeException(t);
	    }
		finally{
			if(conn != null && !conn.isClosed()){
				conn.close();
			}
		}
		return null;
	}
	
}
