/**
 * 
 */
package com.ftd.mo.exceptions;

/**
 * @author cjohnson
 *
 */
public class GiftCardException extends Exception {
	
	public GiftCardException(String message) {
		super(message);
	}

}
