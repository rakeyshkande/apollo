/*
 * Created on Apr 21, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code TemplatesvendorDeliveryAllowed
 */
package com.ftd.messaging.action;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.util.BasePageBuilder;
import com.ftd.messaging.bo.PrepareMessageBO;
import com.ftd.messaging.constants.MessagingConstants;
import com.ftd.messaging.dao.MessageDAO;
import com.ftd.messaging.util.MessagingDOMUtil;
import com.ftd.messaging.vo.MessageOrderStatusVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.security.SecurityManager;
import com.ftd.security.exceptions.AuthenticationException;
import com.ftd.security.exceptions.ExpiredCredentialsException;
import com.ftd.security.exceptions.ExpiredIdentityException;
import com.ftd.security.exceptions.TooManyAttemptsException;

/**
 * @author achen
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class PayOriginalSendNewFTDAction extends PrepareMessageAction {

    private Logger logger = new Logger(
            "com.ftd.messaging.action.PayOriginalSendNewFTDAction");
            
    private String status                   = null; 
    private String vendorDeliveryAllowed    = null; 
    private String productType              = null;
    private String shippingSytem            = null;
    
    private static final String COMP_ERROR_MESSAGE = "comp_error_message";
    private static final String RESOURCE_COMP_ORDER = "CompOrder";
    private static final String RESOURCE_COMP_ORDER_PERMISSION = "Yes";
    
    
    /** 
     * @param request
     * @return
     * @see com.ftd.messaging.action.PrepareMessageAction#getPageTitle(javax.servlet.http.HttpServletRequest)
     */

    public String getPageTitle(HttpServletRequest request) {
        return this.getMessageFromResource("pay_original_send_new_title", request);
    }

    /** 
     * If there was never a successful FTD sent on the order they should receive the following error message �You cannot send an FTD on this order�
     * @param request
     * @param mapping
     * @param document
     * @param messageOrderStatus
     * @return
     * @see com.ftd.messaging.action.PrepareMessageAction#validateStatus(javax.servlet.http.HttpServletRequest, org.apache.struts.action.ActionMapping, org.w3c.dom.Document, com.ftd.messaging.vo.MessageOrderStatusVO)
     */

    public boolean validateStatus(HttpServletRequest request,
            ActionMapping mapping, Document document,
            MessageOrderStatusVO messageOrderStatus) throws Exception{
        
        boolean valid = false;
        String country=messageOrderStatus.getRecipientCountry();
        String actionType = request.getParameter("action_type");
        
        if(actionType != null && actionType.equalsIgnoreCase("send_comp_order")
           || actionType != null && actionType.equalsIgnoreCase("pay_original_send_new")
           && messageOrderStatus.getOrderType() != null 
           && messageOrderStatus.getOrderType().equalsIgnoreCase("Florist"))
        {
          boolean authorized = authorizeCompOrder(request);
          if (!authorized)
          {
            MessagingDOMUtil.addElementToXML(COMP_ERROR_MESSAGE, this.getMessageFromResource("comp_order_permission_error", request), document);
          }
          else
          {
            valid=true;
          }
        }
        
        if(messageOrderStatus.isVendor() && actionType != null && actionType.equalsIgnoreCase("pay_original_send_new"))
        {
          MessagingDOMUtil.addElementToXML(MessagingConstants.ERROR_MESSAGE, this.getMessageFromResource("pay_original_send_new_vendor_error", request), document);
          valid = false;
        }
        else if(messageOrderStatus.isVendor() && actionType != null && actionType.equalsIgnoreCase("send_comp_order"))
        {
            Connection dbConnection = null;

            try{
              dbConnection = this.createDatabaseConnection();
            
              String orderDetailId = request.getParameter(MessagingConstants.REQUEST_ORDER_DETAIL_ID);
              String productId = getOrderDetailsInfo(orderDetailId, dbConnection);
              getProductInfo(productId, dbConnection);
              String venusid=request.getParameter(MessagingConstants.REQUEST_FTD_MESSAGE_ID);//request.getParameter("ftd_message_id");
              MessageDAO messageDAO = new MessageDAO(dbConnection);
        	  String state = messageDAO.getStateCode(venusid,MessagingConstants.VENUS_MESSAGE,dbConnection);
              logger.info("venusid : "+venusid+" state_code ::"+state+" isVendor order::"+messageOrderStatus.isVendor()+" actionType:::"+actionType);
              
              if( this.status.equalsIgnoreCase("U") || this.vendorDeliveryAllowed.equalsIgnoreCase("N"))
              {
                MessagingDOMUtil.addElementToXML(MessagingConstants.ERROR_MESSAGE, this.getMessageFromResource("vendor_product_not_avail", request), document);
                valid = false;
              }
              logger.info(this.productType+"  "+state+" "+this.shippingSytem);
              if((COMConstants.CONS_PRODUCT_TYPE_FRECUT.equalsIgnoreCase(this.productType) && ((COMConstants.CONS_STATE_CODE_ALASKA).equalsIgnoreCase(state)||(COMConstants.CONS_STATE_CODE_HAWAII).equalsIgnoreCase(state)))
        			  ||(MessagingConstants.SHIPPING_SYSTEM_FTD_WEST.equalsIgnoreCase(this.shippingSytem) && ((COMConstants.CONS_STATE_CODE_PUERTO_RICO).equalsIgnoreCase(state)||(COMConstants.CONS_STATE_CODE_VIRGIN_ISLANDS).equalsIgnoreCase(state)))){
            	  MessagingDOMUtil.addElementToXML(MessagingConstants.ERROR_MESSAGE, this.getMessageFromResource("vendor_product_not_avail", request), document);
                  valid = false;
                  logger.info("FRECUTS will not allow to AK/HI or WEST dropships not delivered to PR/VI");
              }
              
            }
            catch (SQLException sqle) {
                  logger.error(sqle);
                  throw new Exception(sqle);
            }
            finally
              {
                if(dbConnection!=null)
                {
                  try
                  {
                    dbConnection.close();
                  }
                  catch (Exception e)
                  {
                     logger.error(e);
                  }
                }
              }
        }
        else if(StringUtils.isNotBlank(country)&&!country.equalsIgnoreCase("US")&&!country.equalsIgnoreCase("USA")&&!country.equalsIgnoreCase("CA"))
        {
          MessagingDOMUtil.addElementToXML(MessagingConstants.ERROR_MESSAGE, this.getMessageFromResource("comp_intl_error", request), document);
          valid = false;
        } 
        else if(!messageOrderStatus.isAttemptedFTD())
        {
            MessagingDOMUtil.addElementToXML(MessagingConstants.ERROR_MESSAGE, this.getMessageFromResource(MessagingConstants.NEVER_ATTEMPTED_FTD_ERROR, request), document);
            valid = false;   
        }else
        {
          valid=true;
        }
        
        return valid;
    }
    
    /**
     * get the order information with which the new message is associated with. 
     * @param request
     * @param prepareMessageBO
     * @return
     * @throws SAXException
     * @throws IOException
     * @throws SQLException
     * @throws ParserConfigurationException
     * @see com.ftd.messaging.action.PrepareMessageAction#prepareMessage(javax.servlet.http.HttpServletRequest, com.ftd.messaging.bo.PrepareMessageBO)
     */
    public Document prepareMessage(HttpServletRequest request,
            PrepareMessageBO prepareMessageBO) throws SAXException,
            IOException, SQLException, ParserConfigurationException, ParseException, Exception {
            HashMap parameters = this.getRequestDataMap(request);
            //set comp_order to true
            parameters.put(MessagingConstants.REQUEST_PARAMETER_COMP_ORDER,"true");
            Document msgXML = this.prepareMessageForCurrentOrder(request, prepareMessageBO,parameters);
                        
             //get states drop down.
             Document stateDoc=new  BasePageBuilder().retrieveStates(prepareMessageBO.getConnection());
             DOMUtil.addSection(msgXML, stateDoc.getChildNodes());            
             //get today's date.
             SimpleDateFormat sdf=new SimpleDateFormat("MMM dd");
             String today=sdf.format(new Date());
             MessagingDOMUtil.addElementToXML("today", today.toUpperCase(), msgXML);
             this.setRequestDataMap(request, parameters);
             
             Document occasionXml = this.getActiveOccaions();
             DOMUtil.addSection(msgXML, occasionXml.getChildNodes());
             
            return msgXML;
    }
    
    
    
    
    
  /**
   * Overrides parent class. 
   * @param request
   * @return the Message Type.
   */
     public String getForward(HttpServletRequest request){

        HashMap parameters=this.getRequestDataMap(request);
        if(parameters!=null)
        {
          return (String)parameters.get(MessagingConstants.MESSAGE_TYPE);
        }else
        {
          logger.warn("parameters is null, return mercury as default.");
          return "Mercury"; //default is Mercury.
        }

    }

 /**
	* Obtain the product id for this order
	*/
 private String getOrderDetailsInfo(String orderDetailid, Connection dbConnection) throws Exception
  {
    String productId = null;
    
    MessageDAO messageDAO = new MessageDAO(dbConnection);
    
    CachedResultSet crs =  messageDAO.getOrderDetailsInfo(orderDetailid);
    
    if(crs.next())
    {
      productId = (String) crs.getString("product_id");
    }
    return productId;
  }

 /**
	* Obtain the product status and the ship method carrier flag
	*/
 private void getProductInfo(String productId, Connection dbConnection) throws Exception
  {
    MessageDAO messageDAO = new MessageDAO(dbConnection);
    
    CachedResultSet crs =  messageDAO.getProductInfo(productId);
    
    if(crs.next())
    {
      this.status = (String) crs.getString("status");
      this.vendorDeliveryAllowed = (String) crs.getString("shipMethodCarrier");
      this.productType =(String)crs.getString("productType");
      this.shippingSytem=(String)crs.getString("shippingSystem");
    }
  }
  
  /**
	*	Authorize user for comp order.
	*
	* @param mgrCode - Approval username
	* @param mgrPassword - Approval password
	* @param context - Security context
	* @return Document - Status document
  * @throws Exception
  */
  private boolean authorizeCompOrder(HttpServletRequest request) 
		throws Exception
  {
		// Variables
	  Object token = null;
	  boolean authenticated = false;
		String unitID = null;
    String mgrCode = request.getParameter("manager_code");
    String mgrPassword = request.getParameter("manager_password");
    String context = request.getParameter(COMConstants.CONTEXT);
		    
    // Obtain unit id
		unitID = ConfigurationUtil.getInstance().getProperty(COMConstants.SECURITY_FILE, COMConstants.UNIT_ID);

		try
		{
			// Authenticate comp order
			token = SecurityManager.getInstance().authenticateIdentity(context, unitID, mgrCode, mgrPassword);		  
			authenticated = true;
		}
		catch(ExpiredIdentityException e){logger.error("User was not authenticated:", e);}
		catch(ExpiredCredentialsException e){logger.error("User was not authenticated:", e);}
		catch(AuthenticationException e){logger.error("User was not authenticated:", e);}
		catch(TooManyAttemptsException e){logger.error("User was not authenticated:", e);}
		boolean hasPerm = SecurityManager.getInstance().assertPermission(COMConstants.RESOURCE_CONTEXT, token, 
										this.RESOURCE_COMP_ORDER, this.RESOURCE_COMP_ORDER_PERMISSION);	
 	  // Check authentication status and user permission status
		if(!authenticated || !SecurityManager.getInstance().assertPermission(COMConstants.RESOURCE_CONTEXT, token, 
										this.RESOURCE_COMP_ORDER, this.RESOURCE_COMP_ORDER_PERMISSION))
		{
				 authenticated = false;
         return authenticated;
		}
		
		return authenticated;
	}
  
  
}
