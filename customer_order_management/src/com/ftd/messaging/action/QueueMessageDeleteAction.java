package com.ftd.messaging.action;

import com.ftd.customerordermanagement.actions.LoadSendMessageAction;
import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.dao.OrderDAO;
import com.ftd.customerordermanagement.dao.RefundDAO;
import com.ftd.customerordermanagement.dao.StockMessageDAO;
import com.ftd.ftdutilities.FTDCommonUtils;
import com.ftd.ftdutilities.FieldUtils;
import com.ftd.messagegenerator.StockMessageGenerator;
import com.ftd.messagegenerator.vo.PointOfContactVO;
import com.ftd.messaging.bo.QueueBO;
import com.ftd.messaging.cache.CancelReasonCodeHandler;
import com.ftd.messaging.constants.MessagingConstants;
import com.ftd.messaging.dao.QueueDAO;
import com.ftd.messaging.vo.CancelReasonVO;
import com.ftd.messaging.vo.QueueDeleteSpreadsheetRowVO;
import com.ftd.osp.framework.dispatcher.Dispatcher;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.handlers.GlobalParmHandler;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.XMLFormat;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.osp.utilities.vo.QueueDeleteDetailVO;
import com.ftd.osp.utilities.vo.QueueDeleteHeaderVO;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityFilter;
import com.ftd.security.SecurityManager;
import com.ftd.security.exceptions.ExcelProcessingException;
import com.ftd.security.util.FileProcessor;
import com.ftd.security.util.ServletHelper;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;
import org.w3c.dom.Document;
import org.w3c.dom.Element;


/**
 * @author JP Puzon
 */
public class QueueMessageDeleteAction
  extends BaseAction
{
  private Logger logger = new Logger("com.ftd.messaging.action.QueueMessageDeleteAction");

  public QueueMessageDeleteAction()
  {
  }

  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, 
                               HttpServletResponse response)
    throws IOException, ServletException
  {

    Connection dbConnection = null;
    String  maxANSPAmount  = "" ;
    GlobalParmHandler gph = (GlobalParmHandler) CacheManager.getInstance().getHandler("CACHE_NAME_GLOBAL_PARM");
    String order_detail_id = request.getParameter("order_detail_id");
    String company_id = request.getParameter("company_id");
    

    
    try
    {
      Document queues = DOMUtil.getDocument();
      Document responseDocument = (Document) DOMUtil.getDocument();

      HashMap parmMap = new HashMap();
      if (SecurityFilter.isMultipartFormData(request))
      {
        parmMap = ServletHelper.getMultipartParam(request);
      }
      else
      {
        parmMap = this.getCleanHttpParameters(request);
      }

          if (gph != null)
          {
            maxANSPAmount  = gph.getGlobalParm("QUEUE_CONFIG", "MAX_ANSP_AMOUNT");
           
          } 
          if (StringUtils.isNotBlank(maxANSPAmount))
          {
                parmMap.put("maxAnspAmount", maxANSPAmount);
          }
     
      dbConnection = DataSourceUtil.getInstance().getConnection(ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, COMConstants.DATASOURCE_NAME));

      QueueBO queueBO = new QueueBO(dbConnection);

      /*  retrieve security token from request */
      String securityToken = (String) parmMap.get(MessagingConstants.REQUEST_SECURITY_TOKEN);
      // get action type     
      String actionType = (String) parmMap.get(COMConstants.ACTION_TYPE);

      /**
       *  actionType = load_content HAS NOTHING TO DO WITH INITIAL PAGE LOAD... it pertains to stock email messages.
       *    
       *  On queueMessageDelete.xsl, when a user selects the email type from the drop down, we make an AJAX
       *  call where action type = load_content to populate the subject and message.
       **/
      if (actionType.equalsIgnoreCase("load_content"))
      {

        /*
         * Need an explicit catch for refresh of email content; called using ajax so an error XML must be sent back
         * instead of forwarding to the error page.
         */
        try
        {

          // Retrieve order data using OrderDAO to be used for the token replacement
          Document dataDocXML = null;
          
        	if(order_detail_id != null && order_detail_id != ""){
        		dataDocXML = new LoadSendMessageAction().getOrderDataDocumentXML(request.getParameter("order_detail_id"), "Y", dbConnection);
        	}
        	else{
        		dataDocXML = (Document) DOMUtil.getDocument();
        	}
           
        	
          // get csr_name from securityManager and append to dataDocXML
          if (SecurityManager.getInstance().getUserInfo((String) parmMap.get(COMConstants.SEC_TOKEN)) != null)
          {
            String csrFirstName = 
              SecurityManager.getInstance().getUserInfo((String) parmMap.get(COMConstants.SEC_TOKEN)).getFirstName();
            String csrLastName = 
              SecurityManager.getInstance().getUserInfo((String) parmMap.get(COMConstants.SEC_TOKEN)).getLastName();
            Document csrXML = 
              this.toXML("<CSR><first_name>" + csrFirstName + "</first_name><last_name>" + csrLastName + "</last_name></CSR>");
            DOMUtil.addSection(dataDocXML, csrXML.getChildNodes());
          }

          final String messageType = "Email";

          // Create a PointOfCOntactVO setting the pocType, customerId, orderDetailId and companyId from
          // the request object and order dataDocument just created
          PointOfContactVO pocObj = new PointOfContactVO();
          pocObj.setPointOfContactType(messageType);
          pocObj.setCompanyId(company_id);
          pocObj.setDataDocument(dataDocXML);
          pocObj.setLetterTitle((String) parmMap.get(COMConstants.MESSAGE_TITLE));
          pocObj.setCommentType(messageType);
          pocObj.setTemplateId((String) parmMap.get(COMConstants.MESSAGE_ID));
          pocObj.setOriginId(null);
          

          // Build the message using StockMessageGenerator passing the POCVO and returning another POCVO
          // containing the merged body and subject
          StockMessageGenerator msgGen = new StockMessageGenerator(dbConnection);
          pocObj = msgGen.buildMessage(pocObj);

          // Create XML from the POC object and append to primary document
          String pocXMLString = pocObj.toXML();

          // Clean up any & chars
          pocXMLString = FieldUtils.replaceAll(pocXMLString, "&", "&amp;");

          // Generate XML
          Document pocXML = this.toXML(pocXMLString);

          // Send back document as XML.  The front end utilizes AJAX
          DOMUtil.print(pocXML,response.getWriter());
        }
        catch (Throwable t)
        {
          logger.error(t);

          // Send back error status document as XML.  The front end utilizes AJAX
          DOMUtil.print(createErrorXML(),response.getWriter());
        }

        return null;

      }
      else if (actionType.equalsIgnoreCase("delete"))
      {
      
        QueueDeleteHeaderVO queueDelHdrVO = new QueueDeleteHeaderVO();
        //Get Batch Description      
        String batchDesc = (String) parmMap.get("batch_description");
        queueDelHdrVO.setBatchDescription(batchDesc);
        
        
        //Check for the partners that need to be excluded.
        List<String> excludedPartners = new ArrayList<String>();
        List<String> includedPartners = new ArrayList<String>();
        Map prefPartnerMap = FTDCommonUtils.getPreferredPartner();
        Iterator partnerIt = prefPartnerMap.keySet().iterator();
        while (partnerIt.hasNext()) {
            String mapKey = (String)partnerIt.next();
            String includePartnerFlag = (String)parmMap.get("cb" + mapKey);  

            if (includePartnerFlag == null || includePartnerFlag.equalsIgnoreCase("off")) {
              excludedPartners.add(mapKey);
              logger.info("excluding partner:" + mapKey);
            }else if (includePartnerFlag != null && includePartnerFlag.equalsIgnoreCase("on")) //7419 Batch Processing - adding include partner to List
            {
              includedPartners.add(mapKey);
              logger.info("including partner:" + mapKey);
            }
        }
        //7419 converting include and exclude partner to comma separated strings.
        if(excludedPartners.size()>0){
          StringBuffer sbExclPartners = new StringBuffer();
          for (String str : excludedPartners){
            sbExclPartners.append(str);
            sbExclPartners.append(",");
          }
          String exclPartnerStr = sbExclPartners.toString().substring(0, sbExclPartners.toString().length()-1);
          queueDelHdrVO.setExcludePartnerName(exclPartnerStr);
        }
        if(includedPartners.size() > 0){
          StringBuffer sbIncPartners = new StringBuffer();
          for (String str : includedPartners){
            sbIncPartners.append(str);
            sbIncPartners.append(",");
          }
          String incPartnerStr = sbIncPartners.toString().substring(0, sbIncPartners.toString().length()-1);
          queueDelHdrVO.setIncludePartnerName(incPartnerStr);
        }

        /**************************************************/
        /** DELETE ASSOCIATED QUEUE MESSAGES **/
        /**************************************************/
        boolean bDeleteOtherQueues = false;
        String cbDeleteOtherQueuesRaw = (String) parmMap.get("cbDeleteOtherQueues");

        if (cbDeleteOtherQueuesRaw != null && cbDeleteOtherQueuesRaw.equalsIgnoreCase("on"))
        {
          bDeleteOtherQueues = true;
        }
        String deleteOtherQueuesSelected = (String) parmMap.get("delete_other_queues_selected");
        //7419 BatchProcessing change
        if(bDeleteOtherQueues)
        {
         
          queueDelHdrVO.setDeleteQueueMsgFlag("Y");
          queueDelHdrVO.setDeleteQueueMsgType(deleteOtherQueuesSelected);
        }else
        {
          queueDelHdrVO.setDeleteQueueMsgFlag("N");
          
        }
        
        /**************************************************/
        /** SEND MESSAGE **/
        /**************************************************/
        boolean bSendMessage = false;
        String cbSendMessageRaw = (String) parmMap.get("cbSendMessage");

        if (cbSendMessageRaw != null && cbSendMessageRaw.equalsIgnoreCase("on"))
        {
          bSendMessage = true;
        }
        
        String sSendMessageType = (String) parmMap.get("sSendMessage");
        
        //7419 BatchProcessing change
        if(bSendMessage)
        {
          queueDelHdrVO.setSendMessageFlag("Y");
          queueDelHdrVO.setMessageType(sSendMessageType);
        }else
        {
          queueDelHdrVO.setSendMessageFlag("N");
          
        }
        
        /**************************************************/
        /** ANSP Amount **/
        /**************************************************/
        //7419 BatchProcessing change
        if(parmMap.get("sAnspAmount") != null && !((String)parmMap.get("sAnspAmount")).equalsIgnoreCase(""))
          queueDelHdrVO.setAnspAmount(new BigDecimal((String)parmMap.get("sAnspAmount")));

        /*
         *If sAnspOverride = on, set give florist asking price to `Y', else set it to `N' 
         *Set cancel reason code = (String) fieldMap.get("sCancelReasonCode")
         *Set text or reason = (String) fieldMap.get("newMessage")
         */
         String sAnspOverride = (String)parmMap.get("sAnspOverride");
         if(sAnspOverride != null && sAnspOverride.equalsIgnoreCase("on"))
           queueDelHdrVO.setGiveFloristAskingPrice("Y");
         else
           queueDelHdrVO.setGiveFloristAskingPrice("N");

         queueDelHdrVO.setCancelReasonCode((String)parmMap.get("sCancelReasonCode"));
         queueDelHdrVO.setTextOrReason((String)parmMap.get("newMessage"));

        /**************************************************/
        /** RESUBMIT ORDER FOR PROCESSING **/
        /**************************************************/
        boolean isOrderReprocessed = false;
        String isOrderReprocessedRaw = (String) parmMap.get("isOrderReprocessed");

        /*
         *If isOrderReprocessedRaw = true, set resubmit order flag = `Y', else set it to 'N'
         */
        if (isOrderReprocessedRaw != null && isOrderReprocessedRaw.equalsIgnoreCase("on"))
        {
          isOrderReprocessed = true;
        }
        //7419 BatchProcessing change
       if(isOrderReprocessed)
       {
         queueDelHdrVO.setResubmitOrderFlag("Y");
       }else
       {
         queueDelHdrVO.setResubmitOrderFlag("N");
       }

        /**************************************************/
        /** ADD ORDER COMMENT **/
        /**************************************************/
        boolean isCommentAdded = false;
        String isCommentAddedRaw = (String) parmMap.get("isCommentAdded");

        if (isCommentAddedRaw != null && isCommentAddedRaw.equalsIgnoreCase("on"))
        {
          isCommentAdded = true;
        }
        //7419 BatchProcessing change
       if(isCommentAdded)
       {
         queueDelHdrVO.setIncludeCommentFlag("Y");
         queueDelHdrVO.setCommentText((String)parmMap.get(COMConstants.COMMENT_TEXT));
       }else
       {
         queueDelHdrVO.setIncludeCommentFlag("N");
         
       }

        /**************************************************/
        /** SEND CUSTOMER EMAIL **/
        /**************************************************/
        boolean isEmailAdded = false;
        String isEmailAddedRaw = (String) parmMap.get("isEmailAdded");

        if (isEmailAddedRaw != null && isEmailAddedRaw.equalsIgnoreCase("on"))
        {
          isEmailAdded = true;
        }
        //7419 BatchProcessing change
        if(isEmailAdded)
        {
          queueDelHdrVO.setSendEmailFlag("Y");
          queueDelHdrVO.setEmailTitle((String)parmMap.get(COMConstants.MESSAGE_TITLE));
          queueDelHdrVO.setEmailSubject((String)parmMap.get(COMConstants.MESSAGE_SUBJECT));
          queueDelHdrVO.setEmailBody((String)parmMap.get(COMConstants.MESSAGE_CONTENT));
        }else
        {
          queueDelHdrVO.setSendEmailFlag("N");
          
        }

        /**************************************************/
        /** REFUND ORDER **/
        /**************************************************/
        boolean isOrderRefunded = false;
        String isOrderRefundedRaw = (String) parmMap.get("isOrderRefunded");
        if (isOrderRefundedRaw != null && isOrderRefundedRaw.equalsIgnoreCase("on"))
        {
          isOrderRefunded = true;
        }
        
        
        String refundDisposition = (String) parmMap.get("refundDisposition");
        String responsibleParty = (String) parmMap.get("responsibleParty");
        String inputType = (String) parmMap.get("inputType");
        
        
        if(isOrderRefunded)
        {
          queueDelHdrVO.setRefundOrderFlag("Y");
          queueDelHdrVO.setRefundDispCode(refundDisposition);
          queueDelHdrVO.setResponsibleParty(responsibleParty);
          
        }else
        {
          queueDelHdrVO.setRefundOrderFlag("N");
          
        }

        logger.debug("Flags set based on checkboxes checked are as follows:");
        logger.debug("deleteOtherQueueTypes: " + bDeleteOtherQueues);
        logger.debug("sendMercuryVenus: " + bSendMessage);
        logger.debug("isOrderReprocessed: " + isOrderReprocessed);
        logger.debug("isCommentAdded: " + isCommentAdded);
        logger.debug("isEmailAdded: " + isEmailAdded);
        logger.debug("isOrderRefunded: " + isOrderRefunded);
        logger.debug("-----------------------------------------------------------------------------------------------------------");

        /* Call lookupCurrentUserId to get the current user's id */
        String userId = queueBO.lookupCurrentUserId(securityToken);

        if (userId == null)
        {
          throw new RuntimeException("execute: User ID can not be null.");
        }

        try
        {
          // Iterate through each message ID, performing the requested workflow.
          List idsToDeleteList = new ArrayList();
          boolean deleteByMessageId = false;
          boolean deleteByExternalOrderNumber = false;
          List unableToDeleteMessages = new ArrayList();
                   
          // Obtain the list of candidate message IDs to queue delete.
          // Note: The first entry is column headings.
          if (inputType.equalsIgnoreCase("FILE"))
          {
            deleteByMessageId = true;
            List fileMessageIdsToDelete = processFileUpload(parmMap);
            if (fileMessageIdsToDelete.size() < 1)
            {
              throw new Exception("execute: Spreadsheet input must contain at least one data row.");
            }
            for (int i = 0; i < fileMessageIdsToDelete.size(); i++)
            {
              String messageId = 
                ((QueueDeleteSpreadsheetRowVO) fileMessageIdsToDelete.get(i)).getFieldByName(QueueDeleteSpreadsheetRowVO.FIELD_NAME_MESSAGE_ID).getValue();
              idsToDeleteList.add(messageId);
            }
          }
          else if (inputType.equalsIgnoreCase("INPUTBOX"))
          {
            deleteByMessageId = true;
            String inputTextArea = (String) parmMap.get("inputTextArea");
            StringTokenizer st = new StringTokenizer(inputTextArea, "\n");
            while (st.hasMoreTokens())
            {
              String messageId = st.nextToken().trim();
              if (messageId != null && !messageId.equalsIgnoreCase(""))
                idsToDeleteList.add(messageId);
            }
          }
          else if (inputType.equalsIgnoreCase("EXTERNAL_ORDER_NUMBERS_BOX"))
          {
            deleteByExternalOrderNumber = true;
            String inputExternalOrderNumbersTextArea = (String) parmMap.get("inputExternalOrderNumbersTextArea");
            StringTokenizer st = new StringTokenizer(inputExternalOrderNumbersTextArea, "\n");
            while (st.hasMoreTokens())
            {
              String externalOrderNumber = st.nextToken().trim();
              if (externalOrderNumber != null && !externalOrderNumber.equalsIgnoreCase(""))
                idsToDeleteList.add(externalOrderNumber.toUpperCase());
            }
          }
          //7419 BatchProcessing change
          queueDelHdrVO.setBatchCount(idsToDeleteList.size());
          queueDelHdrVO.setBatchProcessedFlag("N");
          queueDelHdrVO.setCreatedBy(userId);
          if(deleteByMessageId)
          {
            queueDelHdrVO.setDeleteBatchByType("MESSAGE_ID");
            
          }else if(deleteByExternalOrderNumber)
          {
            queueDelHdrVO.setDeleteBatchByType("EXTERNAL_ORDER_NUMBER");
          }
          
            //Retrieve the max number of records that the MQD can process at any given time. 
            //If the user has entered DUPLICATE message ids such that the total number of ids go over the
            //threshold, we will still display an error.  We will NOT discount for duplicate entries.
            boolean tooManyMsgs = false;
            if (gph != null)
            {
              //if the flag is yes, authorize
              String maxRecords = gph.getGlobalParm("QUEUE_CONFIG", "MQD_MAX_RECORDS");
                         
            if (idsToDeleteList.size() > ((new Integer(maxRecords)).intValue())){
                tooManyMsgs = true;
                unableToDeleteMessages.add("You have entered more ids than queue delete can handle. Please enter " + 
                                           maxRecords + " at a time.");
                 parmMap.put("validation_error", "You have entered more ids than queue delete can handle. Please enter " + 
                                           maxRecords + " at a time.");
                idsToDeleteList.clear();
              }
            }
          
          if(idsToDeleteList.size()>0){
            queueDelHdrVO.setQueueDeleteHeaderId(queueBO.insertQueueDeleteHeaderRecord(queueDelHdrVO));
          
         
              QueueDeleteDetailVO queueDelDetailVO = null;
              Long queueDeleteDetailId = null;
              for (int i = 0; i < idsToDeleteList.size(); i++)
              {
                queueDelDetailVO = new QueueDeleteDetailVO();
                
                String idToDelete = (String) idsToDeleteList.get(i);
                logger.info("MQD working on " + idToDelete);
     
                try //outer catch block
                {
                  //if the user entered the queue id, search by queue id
                  if (deleteByMessageId)
                  {
                    if (!StringUtils.isNumeric(idToDelete))
                      throw new Exception("Invalid Queue Id detected");
                  }
    
                  queueDelDetailVO.setQueueDeleteHeaderId(queueDelHdrVO.getQueueDeleteHeaderId());
                  if(deleteByMessageId)
                  {
                    queueDelDetailVO.setMessageId(new Long(idToDelete));
                    if (queueBO.getExtOrdNumByMsgId(idToDelete) != null)
                    {
                        String extOrdNum = queueBO.getExtOrdNumByMsgId(idToDelete);
                        queueDelDetailVO.setExternalOrderNumber(extOrdNum);
                    }
                    
                  }else if(deleteByExternalOrderNumber)
                  {
                    queueDelDetailVO.setExternalOrderNumber(idToDelete);
                  }
                  queueDelDetailVO.setCreatedBy(userId);
                  queueDelDetailVO.setProcessedStatus("N");
                  
                  queueDeleteDetailId = queueBO.insertDeleteDetailRecord(queueDelDetailVO);
                  
                  logger.debug("execute: Finished processing messageId= " + idToDelete);
    
                } //end - outer catch block
    
                catch (Exception e)
                {
                  // record the error and proceeed to the next record.
                  logger.error("MQD execute: Failed Id To Delete = " + idToDelete, e);
                              
                  // Record the failed message ID.
                  unableToDeleteMessages.add(idToDelete + " : " + e.getMessage());
    
                  // Continue with the next message ID.
                  continue;
                }
              } // End for loop for each id.
              
              //dispatch batch to queue delete for processing 
              this.sendQueueDeleteMessage(queueDelHdrVO.getQueueDeleteHeaderId().toString());
              
              //forward it to status page
              return mapping.findForward(MessagingConstants.SHOW_STATUS);
          }
          // Now that the workflow is complete for all candidate messages, compose the response document using the collections created from the workflow.
          MessageResources messageResources = super.getResources(request);

          queues = queueBO.getUnableDeleteQueueDoc(unableToDeleteMessages, messageResources, "\n", false);
          
          
        }
        catch (ExcelProcessingException e)
        {
          // Record the data validation exception.
          parmMap.put("validation_error", e.getMessage());
        }
      }
      
      // Append common page elements to the response XML.
      composeResponseDoc(parmMap, dbConnection, responseDocument);

      // Forward to transformation action
      //Get XSL File name
      String result = "QueueMessageDelete";
      File xslFile = getXSL(result, mapping);
      ActionForward forward = mapping.findForward(result);
      String xslFilePathAndName = forward.getPath(); //get real file name

      // Change to client side transform
      TraxUtil.getInstance().getInstance().transform(request, response, responseDocument, xslFile, xslFilePathAndName, 
                                                     (HashMap) request.getAttribute(COMConstants.CONS_APP_PARAMETERS));

    }
    catch (Exception e)
    {
      logger.error(e);
      return mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
    }
    finally
    {
      try
      {
        /* close the database connection */
        if (dbConnection != null)
        {
          // dbConnection.rollback();
          dbConnection.close();
        }
      }
      catch (Exception e)
      {
        logger.error(e);
        return mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
      }
    }
    return null;
  }

  public String[] stringToArray(String text)
  {
    String[] returnVal = null;
    if ((text != null) && (text.length() > 0))
    {
      //logger.debug("queue ids="+text);
      StringTokenizer t = new StringTokenizer(text, ",");
      returnVal = new String[t.countTokens()];
      for (int i = 0; i < returnVal.length; i++)
      {
        returnVal[i] = t.nextToken();
        //logger.debug("add queue id "+i+" "+returnVal[ i ]);
      }
    }
    return returnVal;
  }


  private Document toXML(String xmlString)
    throws Exception
  {
	  return DOMUtil.getDocument(xmlString);
  }

  private void composeResponseDoc(HashMap fieldMap, Connection dbConnection, Document responseDocument)
    throws Exception
  {

    DOMUtil.addSection(responseDocument, "pageData", "data", fieldMap, true);

    //Append message titles for "Send Customer Email" option
    StockMessageDAO msgAccess = new StockMessageDAO(dbConnection);
    Document msgXML = msgAccess.loadMessageTitlesXML(COMConstants.EMAIL_MESSAGE_TYPE, null, null, null, "Y", null);
    DOMUtil.addSection(responseDocument, msgXML.getChildNodes());

    //Append refund dispositions for "Refund Order" option
    RefundDAO refund = new RefundDAO(dbConnection);
    Document refundXML = refund.loadRefundDispositionsXML(null);
    DOMUtil.addSection(responseDocument, refundXML.getChildNodes());

    //Append all the queue types available
    QueueDAO qDAO = new QueueDAO(dbConnection);
    Document queueTypesDoc = qDAO.queryAllQueueTypes();
    DOMUtil.addSection(responseDocument, queueTypesDoc.getChildNodes());

    //add cancel reason code
    CancelReasonCodeHandler cancelReasonHandler = 
      (CancelReasonCodeHandler) CacheManager.getInstance().getHandler(MessagingConstants.CancelReasonCodeHandler);
    Map newCancelReasonCodeMap = new HashMap();

    //Add preferred partners
    Document prefPartnerDoc = FTDCommonUtils.getPreferredPartnerForMassProcess();
    DOMUtil.addSection(responseDocument, prefPartnerDoc.getChildNodes());
    
    //Note that the handler is changing the footprint of this hashmap in the refineCancelReasonCodes() method. 
    //To utilize the existing DOMUtil methods, we need to simulate the code that exists in refineCancelReasonCodes() without refining results
    Set ks = cancelReasonHandler.getCancelReasonCode().keySet();
    Iterator iter = ks.iterator();
    while (iter.hasNext())
    {
      String key = (String) iter.next();
      CancelReasonVO canVO = (CancelReasonVO) cancelReasonHandler.getCancelReasonCode().get(key);
      newCancelReasonCodeMap.put(key, canVO.getDescritpion());
    }
    DOMUtil.addSection(responseDocument, "CancelCodes", "CancelCode", (HashMap) newCancelReasonCodeMap, false);
  }

  private List processFileUpload(Map fieldMap)
    throws ExcelProcessingException, Exception
  {
    logger.debug("Entering processFileUpload...");
    FileItem fileItem = (FileItem) fieldMap.get("file_item");

    if (fileItem == null)
    {
      throw new RuntimeException("processFileUpload: Upload data file can not be null.");
    }
    else
    {
      return FileProcessor.getInstance().processSpreadsheetFile(fileItem.getInputStream(), QueueDeleteSpreadsheetRowVO.class);
    }
  }

  private Document createErrorXML() throws ParserConfigurationException
  {
    Document errorDocument = DOMUtil.getDefaultDocument();
    Element root = errorDocument.createElement("root");
    errorDocument.appendChild(root);

    Element node;
    node = errorDocument.createElement("is_error");
    node.appendChild(errorDocument.createTextNode("Y"));
    root.appendChild(node);

    root.appendChild(node);

    return errorDocument;
  }

  protected HashMap getCleanHttpParameters(HttpServletRequest request)
  {
    HashMap result = new HashMap();

    for (Enumeration i = request.getParameterNames(); i.hasMoreElements(); )
    {
      String key = (String) i.nextElement();
      String value = request.getParameter(key);

      if ((value != null) && (value.trim().length() > 0))
      {
        result.put(key, value.trim());
      }
    }

    return result;
  }

  protected Document getOrderDataDocumentXML(String orderDetailId, String includeComments, Connection conn)
    throws Exception
  {
    //Instantiate OrderDAO
    OrderDAO orderDAO = new OrderDAO(conn);

    // pull base document
    Document primaryOrderXML = (Document) DOMUtil.getDocument();

    if (orderDetailId != null && !orderDetailId.equals(""))
    {
      //hashmap that will contain the output from the stored proc
      HashMap printOrderInfo = new HashMap();

      //Call getCartInfo method in the DAO
      printOrderInfo = 
          orderDAO.getOrderInfoForPrint(orderDetailId, includeComments, COMConstants.CONS_PROCESSING_ID_CUSTOMER);

      //Create an Document and call a method that will transform the CachedResultSet from the cursor:
      //OUT_ORDERS_CUR into an master Document. 
      DOMUtil.addSection(primaryOrderXML, buildXML(printOrderInfo, "OUT_ORDERS_CUR", "ORDERS", "ORDER").getChildNodes());

      //OUT_ORDER_ADDON_CUR into an Document.  Append this a master XML document
      DOMUtil.addSection(primaryOrderXML, 
                         buildXML(printOrderInfo, "OUT_ORDER_ADDON_CUR", "ORDER_ADDONS", "ORDER_ADDON").getChildNodes());

      //OUT_ORDER_BILLS_CUR into an Document. Append this a master XML document
      DOMUtil.addSection(primaryOrderXML, 
                         buildXML(printOrderInfo, "OUT_ORDER_BILLS_CUR", "ORDER_BILLS", "ORDER_BILL").getChildNodes());

      //OUT_ORDER_REFUNDS_CUR into an Document. Append this a master XML document
      DOMUtil.addSection(primaryOrderXML, 
                         buildXML(printOrderInfo, "OUT_ORDER_REFUNDS_CUR", "ORDER_REFUNDS", "ORDER_REFUND").getChildNodes());

      //OUT_ORDER_TOTAL_CUR into an Document. Append this a master XML document
      DOMUtil.addSection(primaryOrderXML, 
                         buildXML(printOrderInfo, "OUT_ORDER_TOTAL_CUR", "ORDER_TOTALS", "ORDER_TOTAL").getChildNodes());

      //OUT_ORDER_PAYMENT_CUR into an Document. Append this a master XML document
      DOMUtil.addSection(primaryOrderXML, 
                         buildXML(printOrderInfo, "OUT_ORDER_PAYMENT_CUR", "ORDER_PAYMENTS", "ORDER_PAYMENT").getChildNodes());

      //CUST_CART_CUR into an Document. Append this a master XML document
      DOMUtil.addSection(primaryOrderXML, buildXML(printOrderInfo, "CUST_CART_CUR", "CUSTOMERS", "CUSTOMER").getChildNodes());

      //OUT_CUSTOMER_PHONE_CUR into an Document. Append this a master XML document
      DOMUtil.addSection(primaryOrderXML, 
                         buildXML(printOrderInfo, "OUT_CUSTOMER_PHONE_CUR", "CUSTOMER_PHONES", "CUSTOMER_PHONE").getChildNodes());

      //OUT_MEMBERSHIP_CUR into an Document. Append this a master XML document
      DOMUtil.addSection(primaryOrderXML, 
                         buildXML(printOrderInfo, "OUT_MEMBERSHIP_CUR", "MEMBERSHIPS", "MEMBERSHIP").getChildNodes());

      //OUT_TAX_REFUND_CUR into an Document. Append this a master XML document
      DOMUtil.addSection(primaryOrderXML, 
                         buildXML(printOrderInfo, "OUT_TAX_REFUND_CUR", "TAX_REFUNDS", "TAX_REFUND").getChildNodes());

      // append the current date to the document
      Date rightNow = new Date();
      String dateNow = DateFormat.getDateInstance(DateFormat.SHORT).format(rightNow);
      Document dateXML = this.toXML("<cur_date>" + dateNow + "</cur_date>");
      DOMUtil.addSection(primaryOrderXML, dateXML.getChildNodes());
      //primaryOrderXML.print(System.out);
    }

    return primaryOrderXML;
  }

  /*******************************************************************************************
     * buildXML()
     *******************************************************************************************/
  private Document buildXML(HashMap outMap, String cursorName, String topName, String bottomName)
    throws Exception
  {

    //Create a CachedResultSet object using the cursor name that was passed. 
    CachedResultSet rs = (CachedResultSet) outMap.get(cursorName);
    Document doc = null;

    try
    {
      //Create an XMLFormat, and initialize the top and bottom node.  Note that since this method
      //can be/is called by various other methods, the top and botton names MUST be passed to it. 
      XMLFormat xmlFormat = new XMLFormat();
      xmlFormat.setAttribute("type", "element");
      xmlFormat.setAttribute("top", topName);
      xmlFormat.setAttribute("bottom", bottomName);

      //call the DOMUtil's converToXMLDOM method
      doc = (Document) DOMUtil.convertToXMLDOM(rs, xmlFormat);
    }
    finally
    {
    }

    //return the xml document. 
    return doc;
  }


    private void sendQueueDeleteMessage(String queueDeleteHeaderId) throws Exception
    {
        logger.debug("Sending queue delete header id: " + queueDeleteHeaderId + " to queue delete...");

        MessageToken token = new MessageToken();
        Context context = new InitialContext();
        token.setMessage("B" + queueDeleteHeaderId);
        token.setStatus("QUEUEDELETE");
        token.setJMSCorrelationID(queueDeleteHeaderId);
        //Per Jason Weiss on 9/26/2006, put in a 5 second delay
        token.setProperty("JMS_OracleDelay", String.valueOf(5),"int");
        Dispatcher dispatcher = Dispatcher.getInstance();
        dispatcher.dispatchTextMessage(context, token);
    }
    
    


}
