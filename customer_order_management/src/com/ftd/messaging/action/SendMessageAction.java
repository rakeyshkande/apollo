package com.ftd.messaging.action;
import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.messaging.bo.SendMessageBO;
import com.ftd.messaging.constants.MessagingConstants;
import com.ftd.osp.utilities.plugins.Logger;
import java.io.IOException;
import java.sql.Connection;
import java.util.HashMap;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * This class contains business logic to send Mercury/Venus messages.
 * @author Charles Fox
 * @todo check comments, error handling, and message handling
 * @todo confirm stored procedure setup against actual stored procedures
 */
public class SendMessageAction extends BaseAction
{

    private Logger logger = 
        new Logger("com.ftd.messaging.action.SendMessageAction");
        
    public SendMessageAction()
    {
    }

    /**
     * � Call sendMessage method of SendMessageBO to get forward action name.
     * � Add the request parameter map to HTTP request attributes. 
     * � Return ActionForward instance based on the forward action name.
     * @param mapping - ActionMapping
     * @param form - ActionForm
     * @param request - HttpServletRequest
     * @param response - HttpServletResponse
     * @return ActionForward
     * @throws Exception????
     * @todo - code and determine exceptions
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form, 
        HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException
        {
        
            logger.info("Entering Send Message Action");
            String forwardName="";
            ActionForward actionForward = new ActionForward();
            String actionType = "";
            Connection con=null;
            try
            { 
                con=this.createDatabaseConnection();
                // get action type
                if(request.getParameter(COMConstants.ACTION_TYPE)!=null)
                {
                  actionType = request.getParameter(COMConstants.ACTION_TYPE);
                }
                else
                {
                    //assume display communication screen action
                    actionType = MessagingConstants.ACTION_SEND_MSG;
                }
                
                if(actionType.equals(MessagingConstants.ACTION_SEND_MSG)){
                    /* Call sendMessage method of SendMessageBO to get 
                     * forward action name. */
                    SendMessageBO sendMsgBO = new SendMessageBO(con);
                    /* Add the request parameter map to HTTP request attributes. */
                    HashMap parameters = this.getRequestDataMap(request);
                    forwardName  = sendMsgBO.sendMessage(request,parameters);     
                   
                    this.setRequestDataMap(request, parameters);
                    
                    /* Return ActionForward instance based on the forward 
                     * action name. */
                     
                     actionForward=this.getActionForward(forwardName, request, mapping, parameters); 
                     
                }
                else
                {
                    /* invalid action type passed, throw an error */
                    String errorMsg = actionType + MessagingConstants.INVALID_ACTION_TYPE_MSG + 
                        MessagingConstants.SEND_MESSAGE;
                    throw new ServletException(errorMsg);    
                }
                
                
            }catch(Exception e){
                actionForward = mapping.findForward(MessagingConstants.ERROR);
                logger.error(e);
                
          }finally{
                 //release lock as needed.
                  releaseLock(request, con, forwardName);
                 
                 if (con != null) {
                    try {
                        con.close();
                    } catch (Exception e) {
                        logger.error("can not close connection", e);
                    }
                }
                logger.info("Exiting Send Message Action");
            }
            return actionForward;
            
        }


}