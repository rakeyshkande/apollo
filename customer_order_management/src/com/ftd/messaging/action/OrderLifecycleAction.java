package com.ftd.messaging.action;

import com.ftd.messaging.bo.ViewMessageBO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import java.sql.Connection;
import java.util.HashMap;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;


public class OrderLifecycleAction extends BaseAction
{
    private Logger logger = new Logger("com.ftd.messaging.action.OrderLifecycleAction");
    private static String SUCCESS = "success";
    private static String FAILURE = "failure";

    public OrderLifecycleAction() {
    }

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException {

        logger.debug("Entering execute()");
        
        String mercuryId = request.getParameter("mercury_id");
        logger.debug("mercuryId: " + mercuryId);

        Connection dbConnection = null;
        String result = null;

        try {
            dbConnection = createDatabaseConnection();
            HashMap parameters = this.getRequestDataMap(request);
            result = SUCCESS;

            Document lifecycles = DOMUtil.getDocument();
            ViewMessageBO viewMessageBO = new ViewMessageBO(dbConnection);
            lifecycles = viewMessageBO.getOrderLifecycleMessages(mercuryId);

            StringWriter sw = new StringWriter();
            DOMUtil.print((Document) lifecycles,new PrintWriter(sw));
            logger.debug(sw.toString());

            this.doForward(result, lifecycles, mapping, request, response, parameters);
            return null;
        } catch(IOException e) {
            logger.error(e);
            result = FAILURE;
        } catch(ServletException e) {
            logger.error(e);
            result = FAILURE;
        } catch(Exception e) {
            logger.error(e);
            result = FAILURE;
        } finally {
            try {
                /* close the database connection */
                if (dbConnection!=null) {
                    dbConnection.close();
                }
            } catch(Exception e) {
                throw new ServletException(e);
            }

            logger.debug("Exiting execute()");
        }
        return mapping.findForward(result);
    }

}