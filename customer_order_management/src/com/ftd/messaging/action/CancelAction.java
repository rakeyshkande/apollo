/*
 * Created on Apr 20, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.ftd.messaging.action;

import com.ftd.customerordermanagement.dao.OrderDAO;
import com.ftd.customerordermanagement.vo.OrderDetailVO;
import com.ftd.ftdutilities.FTDCommonUtils;
import com.ftd.messaging.cache.CancelReasonCodeHandler;
import com.ftd.messaging.cache.CancelComplaintCommTypeHandler;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.ftd.messaging.bo.PrepareMessageBO;
import com.ftd.messaging.constants.MessagingConstants;
import com.ftd.messaging.util.MessageUtil;
import com.ftd.messaging.util.MessagingDOMUtil;
import com.ftd.messaging.vo.MessageOrderStatusVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.PartnerVO;
import com.ftd.osp.utilities.xml.JAXPUtil;

import java.sql.Connection;

import java.text.SimpleDateFormat;

import java.util.Date;

import org.w3c.dom.Element;

/**
 * @author achen
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class CancelAction extends PrepareMessageAction {


  /**
   * prepareMessage() - prepare the message
   * 
   * @param request  - HttpServletRequest
   * @param prepareMessageBO - PrepareMessageBO
   * 
   * @return Document - message
   * 
   * @throws SAXException
   * @throws IOException
   * @throws SQLException
   * @throws ParserConfigurationException
   * @throws Exception
   */
    public Document prepareMessage(HttpServletRequest request,
            PrepareMessageBO prepareMessageBO) throws SAXException,
            IOException, SQLException, ParserConfigurationException, Exception {
        
        
        Document doc = this.prepareMessageForCurrentFlorist(
            request, prepareMessageBO);

       //add cancel reason code
        CancelReasonCodeHandler cancelReasonHandler = 
            (CancelReasonCodeHandler) CacheManager.getInstance().getHandler(
                                      MessagingConstants.CancelReasonCodeHandler);
        
        CancelComplaintCommTypeHandler cancelComplaintCommTypeHandler =
           (CancelComplaintCommTypeHandler) CacheManager.getInstance().getHandler(
                                      MessagingConstants.CancelComplaintCommHandler);
        
        String messageType=request.getParameter(MessagingConstants.MESSAGE_TYPE);

        //retrieve the order detail id
        String orderDetailId = request.getParameter(MessagingConstants.REQUEST_ORDER_DETAIL_ID); 
      
        //define the connection
        Connection connection = null; 
        
        //create a new Order Detail VO
        OrderDetailVO odVO = new OrderDetailVO(); 
        try
        {
          //create a connection
          connection = this.createDatabaseConnection();
          
          //instantiate the order dao
          OrderDAO orderDAO = new OrderDAO(connection);

          //retrieve the order detail info
          odVO = orderDAO.getOrderDetail(orderDetailId);
          
          String preferredPartner = "N";
          PartnerVO partnerVO = FTDCommonUtils.getPreferredPartnerBySource(odVO.getSourceCode());
          if (partnerVO != null)
          {
            preferredPartner = "Y";
          }
          
          Element rootDocElement = doc.getDocumentElement();
          Element preferredPartnerElement = JAXPUtil.buildSimpleXmlNode(doc,"preferred_partner", preferredPartner);
          rootDocElement.appendChild(preferredPartnerElement);
         
          if(messageType.equalsIgnoreCase(MessagingConstants.VENUS_MESSAGE))
          {
            //retrieve the ship date from the request
            String shipDate = request.getParameter("ship_date");
            
            //Ship date should not be null on the Venus record
            if (StringUtils.isEmpty(shipDate))
              throw new Exception("Ship date is null on the Venus records.  Order Detail Id = " + request.getParameter("order_detail_id"));
  
            //parse the input ship date
            SimpleDateFormat sdfOutput = new SimpleDateFormat("EEE MMM dd hh:mm:ss z yyyy");
            Date dShipDate =  sdfOutput.parse(shipDate);
  
            //filter out unwanted cancel reason codes based on order detail parms
            cancelReasonHandler.refineCancelReasonCodes(odVO, dShipDate);
          }

        }
        finally
        {
          //close the connection
          if (connection != null)
          {
            try 
            {
              connection.close();
            }
            //No particular need to throw an exception if the connection cannot be closed.
            //Debug statements have been added on the server which will show the connection leaks
            catch(Exception e)
            {}
          }
        }

        //transform to XML
        cancelReasonHandler.toXML(doc);     
        cancelComplaintCommTypeHandler.toXML(doc);

        return doc;

    }

    
    public String getPageTitle(HttpServletRequest request) {
        return this.getMessageFromResource("cancel_page_title", request);
    }

    /**
     * Return false if a successful CAN has already been sent for the order 
     * or if a REJ has come in from the current florist
     */
   
    public boolean validateStatus(HttpServletRequest request,
            ActionMapping mapping, Document document,
            MessageOrderStatusVO messageOrderStatus) throws SAXException,
            IOException, SQLException, ParserConfigurationException, Exception{
        
        boolean valid=false;
        String orderType =  messageOrderStatus.getOrderType();
        
        if(orderType.equalsIgnoreCase("Vendor"))
        {
            String ftdMessageID = request.getParameter(MessagingConstants.REQUEST_FTD_MESSAGE_ID);
            
            //define the connection
            Connection connection = null; 
            
            try
            {
              //create a connection
              connection = this.createDatabaseConnection();
              MessageUtil messageUtil = new MessageUtil(connection);
              if(messageUtil.isZJOrderInTransit(ftdMessageID))
              {
                MessagingDOMUtil.addElementToXML(MessagingConstants.ERROR_MESSAGE, this.getMessageFromResource(MessagingConstants.ZJ_ORDER_IN_TRANSIT, request), document);
                valid = false;
              }
              else
              {
                valid = true;
              }
            }
            finally
            {
              //close the connection
              if (connection != null)
              {
                try 
                {
                  connection.close();
                }
                //No particular need to throw an exception if the connection cannot be closed.
                //Debug statements have been added on the server which will show the connection leaks
                catch(Exception e)
                {}
              }
            }
        }
        else if(messageOrderStatus.isCancelSent()){
            
            MessagingDOMUtil.addElementToXML(MessagingConstants.ERROR_MESSAGE, this.getMessageFromResource(MessagingConstants.ZJ_ORDER_IN_TRANSIT, request), document);
            
        }
        else if(messageOrderStatus.isFloristRejectOrder()){
                
            MessagingDOMUtil.addElementToXML(MessagingConstants.ERROR_MESSAGE, this.getMessageFromResource(MessagingConstants.REJECTED_ERROR, request), document);
            
            
        }
        /*
            COMMENTED OUT PER PRODUCTION DEFECT #207 -- Should be no a type val on cancels
          
            else if(!messageOrderStatus.isCompOrder()&& messageOrderStatus.isATypeRefund()){
                
            MessagingDOMUtil.addElementToXML(MessagingConstants.ERROR_MESSAGE, this.getMessageFromResource(MessagingConstants.A_TYPE_REFUND_ERROR, request), document);
        }
        */
        else{
            valid=true;
        }
        
        
        
        return valid;
    }
    
    
    public String getForward(HttpServletRequest request){
        
       
        return request.getParameter(MessagingConstants.MESSAGE_ORDER_TYPE);
        
    }
    
    
     /**
     * Initialize �reason� field as �PLEASE CANCEL ORDER.�
     */
    public String getInitialReasonText(HttpServletRequest request){
    	String systemReason=request.getParameter("reason");//system generated comments by Modify Order.
      if(StringUtils.isBlank(systemReason)){
        return this.getMessageFromResource("cancel_init_reason", request);
      }else
      {
        return null;
      }
      
    }

}
