/*
 * Created on Apr 21, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.ftd.messaging.action;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.ftd.messaging.bo.PrepareMessageBO;
import com.ftd.messaging.constants.MessagingConstants;
import com.ftd.messaging.util.MessagingDOMUtil;
import com.ftd.messaging.vo.MessageOrderStatusVO;

/**
 * @author achen
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class AskFloristPriceChangeAction extends PrepareMessageAction {

    /** 
     * @param request
     * @param prepareMessageBO
     * @return
     * @throws SAXException
     * @throws IOException
     * @throws SQLException
     * @throws ParserConfigurationException
     * @see com.ftd.messaging.action.PrepareMessageAction#prepareMessage(javax.servlet.http.HttpServletRequest, com.ftd.messaging.bo.PrepareMessageBO)
     */

    public Document prepareMessage(HttpServletRequest request,
            PrepareMessageBO prepareMessageBO) throws SAXException,
            IOException, SQLException, ParserConfigurationException, Exception {
       String messageOrderNumber = request
                .getParameter(MessagingConstants.MESSAGE_ORDER_NUMBER);
       
       HashMap parameters = this.getRequestDataMap(request);
       

       if(request.getServletPath().contains("Cogs")){
    	   
    	   parameters.put("COGS", "Y");
       }
       
        String messageType = request.getParameter(MessagingConstants.MESSAGE_TYPE);
        Document Doc = prepareMessageBO.retrieveASKPMessageForCurrentFlorist(
                messageOrderNumber, messageType, parameters);
        return Doc;
    }

    /** 
     * @param request
     * @return
     * @see com.ftd.messaging.action.PrepareMessageAction#getPageTitle(javax.servlet.http.HttpServletRequest)
     */

    public String getPageTitle(HttpServletRequest request) {
        return this.getMessageFromResource("send_florist_price_change_page_title", request);
    }

    /** 
     * Return false if the order is Vendor filled orders or if a REJ has come in from the current florist.
     * @param request
     * @param mapping
     * @param document
     * @param messageOrderStatus
     * @return
     * @see com.ftd.messaging.action.PrepareMessageAction#validateStatus(javax.servlet.http.HttpServletRequest, org.apache.struts.action.ActionMapping, org.w3c.dom.Document, com.ftd.messaging.vo.MessageOrderStatusVO)
     */

    public boolean validateStatus(HttpServletRequest request,
            ActionMapping mapping, Document document,
            MessageOrderStatusVO messageOrderStatus) {
        boolean valid =false;
        
        if(messageOrderStatus.isCancelSent()&&!messageOrderStatus.isCancelDenied()){
            
            MessagingDOMUtil.addElementToXML(MessagingConstants.ERROR_MESSAGE, this.getMessageFromResource(MessagingConstants.CANCELLED_ERROR, request), document);
            
        }else if(messageOrderStatus.isVendor()){
            
            MessagingDOMUtil.addElementToXML(MessagingConstants.ERROR_MESSAGE, this.getMessageFromResource(MessagingConstants.VENDOR_ERROR, request), document);
            
         }else  if(messageOrderStatus.isFloristRejectOrder()){
                
             MessagingDOMUtil.addElementToXML(MessagingConstants.ERROR_MESSAGE, this.getMessageFromResource(MessagingConstants.REJECTED_ERROR, request), document);
            
            
        }else{
            valid=true;
        }
        
        return valid;
    }
    
    
    
    /**
     * Initialize �reason� field as �PLEASE FIND YOUR NEW PRICE LISTED ABOVE.�
     */
    public String getInitialReasonText(HttpServletRequest request){
    	
    	return this.getMessageFromResource("send_price_change_init_reason", request);
    }
    

}
