/*
 * Created on Apr 20, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.ftd.messaging.action;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.util.BasePageBuilder;
import com.ftd.messaging.bo.PrepareMessageBO;
import com.ftd.messaging.constants.MessagingConstants;
import com.ftd.messaging.dao.MessageDAO;
import com.ftd.messaging.util.MessageUtil;
import com.ftd.messaging.util.MessagingDOMUtil;
import com.ftd.messaging.vo.MessageOrderStatusVO;
import com.ftd.osp.utilities.AddOnUtility;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;

/**
 * @author achen
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class NewFTDAction extends PrepareMessageAction {

    private Logger logger = new Logger(
            "com.ftd.messaging.action.NewFTDAction");
    
    private String status                           = null; 
    private String vendorDeliveryAllowed            = null;  
    private String floristDeliveryAllowed           = null;
    private String shippingSytem            = null;
    private String productType              = null;
        
    /** 
     * @param request
     * @param prepareMessageBO
     * @return
     * @throws SAXException
     * @throws IOException
     * @throws SQLException
     * @throws ParserConfigurationException
     * @see com.ftd.messaging.action.PrepareMessageAction#prepareMessage(javax.servlet.http.HttpServletRequest, com.ftd.messaging.bo.PrepareMessageBO)
     */

    public Document prepareMessage(HttpServletRequest request,
            PrepareMessageBO prepareMessageBO) throws SAXException,
            IOException, SQLException, ParserConfigurationException, ParseException, Exception {
            HashMap parameters = this.getRequestDataMap(request);
            Document msgXML = this.prepareMessageForCurrentOrder(request, prepareMessageBO,parameters);
            
            //get states drop down.
            Document stateDoc=new  BasePageBuilder().retrieveStates(prepareMessageBO.getConnection());
            DOMUtil.addSection(msgXML, stateDoc.getChildNodes());     
            //get today's date.
            SimpleDateFormat sdf=new SimpleDateFormat("MMM dd");
            String today=sdf.format(new Date());
            MessagingDOMUtil.addElementToXML("today", today.toUpperCase(), msgXML);                         
            this.setRequestDataMap(request, parameters);

            Document occasionXml = this.getActiveOccaions();
            DOMUtil.addSection(msgXML, occasionXml.getChildNodes());
            
            return msgXML;
    }

    /** 
     * @param request
     * @return
     * @see com.ftd.messaging.action.PrepareMessageAction#getPageTitle(javax.servlet.http.HttpServletRequest)
     */

    public String getPageTitle(HttpServletRequest request) {
        return this.getMessageFromResource("new_ftd_page_title", request);
    }

    /** 
     * If there is a Live FTD on the order, user will get an error message �You must cancel the original order before you can send a new FTD�. 
     * If there was never a successful FTD sent on the order they should receive the following error message �You cannot send an FTD on this order�. 
     * If the current date is past the delivery date the following error message will display �You cannot send a new FTD order past the delivery date�.
     * @param request
     * @param mapping
     * @param document
     * @param messageOrderStatus
     * @return
     * @see com.ftd.messaging.action.PrepareMessageAction#validateStatus(javax.servlet.http.HttpServletRequest, org.apache.struts.action.ActionMapping, org.w3c.dom.Document, com.ftd.messaging.vo.MessageOrderStatusVO)
     */

    public boolean validateStatus(HttpServletRequest request,
            ActionMapping mapping, Document document,
            MessageOrderStatusVO messageOrderStatus) throws Exception{
        boolean valid=true;
        String orderDetailId = request.getParameter(MessagingConstants.REQUEST_ORDER_DETAIL_ID);
        String actionType = request.getParameter("action_type");
        
        if(messageOrderStatus.isHasLiveFTD()){
            MessagingDOMUtil.addElementToXML(MessagingConstants.ERROR_MESSAGE, this.getMessageFromResource(MessagingConstants.HAS_LIVE_FTD_ERROR, request), document);
            valid = false;
        }else if(!messageOrderStatus.isAttemptedFTD()){
            MessagingDOMUtil.addElementToXML(MessagingConstants.ERROR_MESSAGE, this.getMessageFromResource(MessagingConstants.NEVER_ATTEMPTED_FTD_ERROR, request), document);
            valid = false;
        }/*Open FTD for modify order. Per Jenny's change request.*/
        else if(messageOrderStatus.isPastDeliveryDate()&&messageOrderStatus.isVendor()){
            MessagingDOMUtil.addElementToXML(MessagingConstants.ERROR_MESSAGE, this.getMessageFromResource(MessagingConstants.PAST_DELIVERY_DATE_ERROR, request), document);
            valid = false;
        }else if(messageOrderStatus.isATypeRefund()){
            MessagingDOMUtil.addElementToXML(MessagingConstants.ERROR_MESSAGE, this.getMessageFromResource(MessagingConstants.A_TYPE_REFUND_ERROR, request), document);
            valid = false;               
        }else if (valid) 
        {
            Connection dbConnection = null;
            
            try{
              dbConnection = this.createDatabaseConnection();
              String productId =getOrderDetailsInfo(orderDetailId, dbConnection);
              getProductInfo(productId, dbConnection);
             
              if(messageOrderStatus.isFlorist() && actionType != null && actionType.equalsIgnoreCase("cancel_original_send_new")
                                 || messageOrderStatus.isFlorist() && actionType != null && actionType.equalsIgnoreCase("send_new_ftd") )
              {
                  if( this.status.equalsIgnoreCase("U") || this.floristDeliveryAllowed.equalsIgnoreCase("N") )
                  {
                    MessagingDOMUtil.addElementToXML(MessagingConstants.ERROR_MESSAGE, this.getMessageFromResource("florist_product_not_avail", request), document);
                    valid = false;
                  }
              }
              
              if(messageOrderStatus.isVendor() && actionType != null && (actionType.equalsIgnoreCase("send_new_ftd")))
              {
            	  String venusid=request.getParameter(MessagingConstants.REQUEST_FTD_MESSAGE_ID);//request.getParameter("ftd_message_id");
            	  MessageDAO messageDAO = new MessageDAO(dbConnection);
            	  String state = messageDAO.getStateCode(venusid,MessagingConstants.VENUS_MESSAGE,dbConnection);
            	  logger.info("venusid : "+venusid+" state_code ::"+state+" isVendor order::"+messageOrderStatus.isVendor()+" actionType:::"+actionType);
            	  if((COMConstants.CONS_PRODUCT_TYPE_FRECUT.equalsIgnoreCase(this.productType) && ((COMConstants.CONS_STATE_CODE_ALASKA).equalsIgnoreCase(state)||(COMConstants.CONS_STATE_CODE_HAWAII).equalsIgnoreCase(state)))
            			  ||(MessagingConstants.SHIPPING_SYSTEM_FTD_WEST.equalsIgnoreCase(this.shippingSytem) && ((COMConstants.CONS_STATE_CODE_PUERTO_RICO).equalsIgnoreCase(state)||(COMConstants.CONS_STATE_CODE_VIRGIN_ISLANDS).equalsIgnoreCase(state)))){
                	  MessagingDOMUtil.addElementToXML(MessagingConstants.ERROR_MESSAGE, this.getMessageFromResource("vendor_product_not_avail", request), document);
                      valid = false;
                      logger.info("FRECUTS will not allow to AK/HI or WEST dropships not delivered to PR/VI");
                  }
              }
              
              //check if order has add ons associated with it.  If it does, check to see
              //if the add ons are still available
              AddOnUtility addOnUTIL = new AddOnUtility();
              boolean orderAddOnsAvailable = addOnUTIL.orderAddOnAvailable(orderDetailId, dbConnection);
              if( !orderAddOnsAvailable )
              {
                MessagingDOMUtil.addElementToXML(MessagingConstants.ERROR_MESSAGE, this.getMessageFromResource("add_on_not_available", request), document);
                valid = false;
              }             
            }
            catch (SQLException sqle) {
                  logger.error(sqle);
                  throw new Exception(sqle);
            }
            finally
              {
                if(dbConnection!=null)
                {
                  try
                  {
                    dbConnection.close();
                  }
                  catch (Exception e)
                  {
                     logger.error(e);
                  }
                }
              }
        }//end else
          
        return valid;
    }
    
  /**
   * Overrides parent class. 
   * @param request
   * @return the Message Type.
   */
     public String getForward(HttpServletRequest request){

        HashMap parameters=this.getRequestDataMap(request);
        
        if(parameters!=null)
        {
          return (String)parameters.get(MessagingConstants.MESSAGE_TYPE);
        }else
        {
          //logger.warn("parameters is null, return mercury as default.");
          return "Mercury"; //default is Mercury.
        }
  }
  
  /**
	* Obtain the product id for this order
	*/
 private String getOrderDetailsInfo(String orderDetailid, Connection dbConnection) throws Exception
  {
    String productId = null;
    
    MessageDAO messageDAO = new MessageDAO(dbConnection);
    
    CachedResultSet crs =  messageDAO.getOrderDetailsInfo(orderDetailid);
    
    if(crs.next())
    {
      productId = (String) crs.getString("product_id");
    }
    return productId;
  }
  
 /**
	* Obtain the product status and the ship method carrier flag
	*/
  private void getProductInfo(String productId, Connection dbConnection) throws Exception
  {
    MessageDAO messageDAO = new MessageDAO(dbConnection);
    
    CachedResultSet crs =  messageDAO.getProductInfo(productId);
    
    if(crs.next())
    {
      this.status = (String) crs.getString("status");
      this.vendorDeliveryAllowed = (String) crs.getString("shipMethodCarrier");
      this.floristDeliveryAllowed = (String) crs.getString("shipMethodFlorist");
      this.shippingSytem =(String)crs.getString("shippingSystem");
      this.productType =(String)crs.getString("productType");
    }
  }
  
}
