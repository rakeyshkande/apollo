package com.ftd.messaging.action;
import com.ftd.messaging.bo.CalloutBO;
import com.ftd.messaging.constants.MessagingConstants;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.xml.sax.SAXException;

/**
 * This class extends BaseAction class. It contains the logic to 
 * process a call out.
 * @author Charles Fox
 */
public class SendCalloutAction extends BaseAction 
{
    private Logger logger = 
        new Logger("com.ftd.messaging.action.SendCalloutAction");
    private static String SUCCESS = "success";
    private static String FAILURE = MessagingConstants.ERROR;
    
    public SendCalloutAction()
    {
    } 
    
    /**
     * process send call out action
     * @param mapping - ActionMapping
     * @param form - ActionForm
     * @param request - HttpServletRequest
     * @param response - HttpServletResponse
     * @return ActionForward
     * @throws IOException
     * @throws ServletException
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form, 
        HttpServletRequest request, HttpServletResponse response)
        throws ServletException {
        
        Connection dbConnection = null;
        String result = null;
        String forwardAction = "";    
        logger.info("Entering Send Callout Action");
        ActionForward actionForward = new ActionForward();
        
        try{
            HashMap parameters = this.getRequestDataMap(request);
            dbConnection = this.createDatabaseConnection();
            CalloutBO calloutBO = new CalloutBO(dbConnection);
            forwardAction=calloutBO.processCallout(request,parameters, this.getResources(request));
            this.setRequestDataMap(request, parameters);
            actionForward= this.getActionForward(forwardAction, request, mapping, parameters); 
                 
            
                
            
        }catch(SAXException e){
            logger.error(e);
            actionForward = mapping.findForward(MessagingConstants.ERROR);
        }catch(ServletException e){
            logger.error(e);
            actionForward = mapping.findForward(MessagingConstants.ERROR);
        }catch(IOException e){
            logger.error(e);
            actionForward = mapping.findForward(MessagingConstants.ERROR);
        }catch(ParserConfigurationException e){
            logger.error(e);
            actionForward = mapping.findForward(MessagingConstants.ERROR);
        }catch(SQLException e){
            logger.error(e);
            actionForward = mapping.findForward(MessagingConstants.ERROR);
        }catch(Exception e){
            logger.error(e);
            actionForward = mapping.findForward(MessagingConstants.ERROR);
      }finally{
            try
            {
                 //release lock as needed.
                  releaseLock(request, dbConnection, forwardAction);
                /* close the database connection */
                if(dbConnection!=null){
                    dbConnection.close();
                } 
            }catch(Exception e){
                throw new ServletException(e);
            }
            
            logger.info("Exiting Send Callout Action");
        }            

    return actionForward;
    }
}