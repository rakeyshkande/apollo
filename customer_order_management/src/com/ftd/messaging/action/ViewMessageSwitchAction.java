package com.ftd.messaging.action;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ftd.messaging.bo.ViewMessageBO;
import com.ftd.messaging.constants.MessagingConstants;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * This class is responsible for forwarding control to a URI 
 * based on message type.     
 * @author Charles Fox
 * @todo check comments, error handling, and message handling
 */
public class ViewMessageSwitchAction extends BaseAction
{

    private Logger logger = 
        new Logger("com.ftd.messaging.action.ViewMessageSwitchAction");
        
    public ViewMessageSwitchAction()
    {
    }
    
    /**
     *  1. Call getMessageType method of ViewMessageBO.
     *  2. Get forward URI from struts configuration file based the 
     *  message type, and forward control to this URI.
     * @param n/a
     * @return n/a
     * @throws Exception????
     * @todo - code and determine exceptions
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form, 
        HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException{
        
            logger.info("Entering View Message Switch Action");
            
            ActionForward forward= new ActionForward();
            String messageInfo = request.getParameter(MessagingConstants.REQUEST_MESSAGE_INFO);
            
            try{
                    /* Call getMessageType method of ViewMessageBO. */
                    ViewMessageBO viewMsgBO = new ViewMessageBO();
                    
                    if(messageInfo!=null&&messageInfo.indexOf("|")!=-1){
                        /* Get forward URI from struts configuration file based the 
                        *  message type, and forward control to this URI. */
                        int separator = messageInfo.indexOf("|");
                        String messageType=messageInfo.substring(separator + 1);
                        if(messageType!=null&&messageType.indexOf(" ")!=-1)
                        {
                          messageType=messageType.substring(0, messageType.indexOf(" "));
                          
                        }
                        logger.debug("message type="+messageType);
                        String messageId=messageInfo.substring(0, separator);
                        forward = mapping.findForward(messageType);
                        HashMap dataMap=this.getRequestDataMap(request);
                        this.setRequestDataMap(request, dataMap);
                        request.setAttribute(MessagingConstants.REQUEST_MESSAGE_ID, messageId );
                        request.setAttribute(MessagingConstants.MESSAGE_TYPE, messageType);
                        String message_line_number = request.getParameter(MessagingConstants.MESSAGE_LINE_NUMBER);
                        if(forward!=null&&forward.getPath()!=null)
                        {
                          String path=forward.getPath();
                          if(path.indexOf("?")!=-1)
                          {
                            path +=messageId;
                            path +="&message_line_number="+message_line_number;
                            forward=new ActionForward(path);
                          }
                        }
                    }
                    else
                    {
                        throw new ServletException("Message Type not found");
                    }
                
                
          }catch(Exception e){
                logger.error(e);
                forward = mapping.findForward(MessagingConstants.ERROR);
                throw new ServletException(e);
          }finally{
                logger.info("Exiting View Message Switch Action");
            }
            logger.debug("path="+forward.getPath());
            return forward;
        }
        
        
}