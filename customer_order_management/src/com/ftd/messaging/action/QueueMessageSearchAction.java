package com.ftd.messaging.action;

import java.io.BufferedWriter;
import java.io.File;
import java.math.BigDecimal;
import java.sql.Connection;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.ftdutilities.FTDCommonUtils;
import com.ftd.messaging.bo.QueueBO;
import com.ftd.messaging.vo.QueueSearchVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;


public final class QueueMessageSearchAction extends Action {
    private static Logger logger = 
        new Logger("com.ftd.customerordermanagement.actions.QueueMessageSearchAction");

    /**
   * This is the main action called from the Struts framework.
   * @param mapping The ActionMapping used to select this instance.
   * @param form The optional ActionForm bean for this request.
   * @param request The HTTP Request we are processing.
   * @param response The HTTP Response we are processing.
   */
    public ActionForward execute(ActionMapping mapping, ActionForm form, 
                                 HttpServletRequest request, 
                                 HttpServletResponse response) {
        Connection conn = null;

        try {
            String xslName = "QueueMessageSearch";

            //Connection/Database info
            conn = DataSourceUtil.getInstance().getConnection(ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, 
                                                                                       COMConstants.DATASOURCE_NAME));

            //Document that will contain the final XML, to be passed to the TransUtil and XSL page
            Document responseDocument = (Document)DOMUtil.getDocument();

            QueueBO queueBO = new QueueBO(conn);

            //Get info passed in the request object
            String actionType = request.getParameter(COMConstants.ACTION_TYPE);
            
            // Page data which can be passed to the next page.
            HashMap pageDataMap = new HashMap();
            pageDataMap.put(COMConstants.ACTION_TYPE, actionType);
            
            HashMap prodPropMap = FTDCommonUtils.getPriorityProductProperties();
            Map prefPartnerMap = FTDCommonUtils.getPreferredPartner();
            Document prefPartnerDoc = FTDCommonUtils.getPreferredPartnerForMassProcess();
            
            if (actionType.equals("search")) {
                //This object has created as part of Mass Queue Delete Enhancements change.
                QueueSearchVO queueSearchVO = new QueueSearchVO();
                

                // Obtain the search parameters.
                String queueType = request.getParameter("scQueueTypeId");               
                if (queueType != null && queueType.length() < 1) {
                    throw new RuntimeException("execute: queueType can not be null.");
                }
                
                pageDataMap.put("scQueueTypeId", queueType);
                queueSearchVO.setQueueType(queueType);
                
                String messageSystem = request.getParameter("scMessageSystemId");
                    if (messageSystem != null && messageSystem.length() < 1) {
                        messageSystem = null;
                    } else {
                
                pageDataMap.put("scMessageSystemId", messageSystem);
                queueSearchVO.setMessageSystem(messageSystem);
                    }
                
                String messageDirection = 
                    request.getParameter("scMessageDirectionId");                
                if (messageDirection != null && messageDirection.length() < 1) {
                    messageDirection = null;
                } else {
                
                pageDataMap.put("scMessageDirectionId", messageDirection);
                queueSearchVO.setMessageDirection(messageDirection);
                    }
                //General Non Partner logic    
                String nonPartner = request.getParameter("scNonPartner");
                if(nonPartner == null )
                {
                  nonPartner = "off";
                  
                }
                queueSearchVO.setNonPartner(nonPartner);
                //Customer Email logic
                String custEmail = request.getParameter("scCustomerEmail");
                if(custEmail == null )
                {
                  
                  custEmail = "off";
                  
                }
                queueSearchVO.setCustomerEmail(custEmail);
                
                /*ASKP LOGIC -->messageSystem  is 'Merc' and queueType is 'ASKP' and message direction is 'INBOUND'
                 * incremental prices (fromAmount and toAmount) needs to be set to  queueSearchVO that
                 * passes back to business object
                 */
                if(queueType.equalsIgnoreCase("ASKP") && messageSystem.equalsIgnoreCase("Merc") && 
                  (messageDirection != null && messageDirection.equalsIgnoreCase("INBOUND")))
                {
                  if(request.getParameter("scAmountFrom") != null && !request.getParameter("scAmountFrom").trim().equals("")){
                    double amtFrom = Double.parseDouble(request.getParameter("scAmountFrom"));
                    queueSearchVO.setAmountFrom(amtFrom);
                  }else
                  {
                    queueSearchVO.setAmountFrom(null);
                  }
                  if(request.getParameter("scAmountTo") != null && !request.getParameter("scAmountTo").trim().equals("")){
                    double amtTo = Double.parseDouble(request.getParameter("scAmountTo"));  
                    queueSearchVO.setAmountTo(amtTo);
                  }else
                  {
                    queueSearchVO.setAmountTo(null);
                  }
                }else
                {
                  queueSearchVO.setAmountFrom(null);
                  queueSearchVO.setAmountTo(null);
                }
                
                /*Keyword Search logic
                 * messageSystem  is 'Merc' and queueType is 'ASK' or 'ANS' and message direction is 'INBOUND'
                 * keywords from all theree textboxes and its associated conditons (if they are not null) needs 
                 * to be set to queueSearchVO that passes back to business object.
                 * If user enters text in textbox2 then setting condition1 is valid otherwise null.
                 * If user enters text in textbox3 then setting condition2 is valid otherwise null
                 * 
                 */
                if( messageSystem.equalsIgnoreCase("Merc") && (queueType.equalsIgnoreCase("ASK") || queueType.equalsIgnoreCase("ANS") || queueType.equalsIgnoreCase("ASKP")) 
                && (messageDirection != null && messageDirection.equalsIgnoreCase("INBOUND")))
                {
                  if(request.getParameter("scSearchText1") != null && !request.getParameter("scSearchText1").trim().equals(""))
                  {
                    queueSearchVO.setKeywordSearchText1(request.getParameter("scSearchText1"));  
                  }else
                  {
                    queueSearchVO.setKeywordSearchText1(null);
                  }
                  
                  if(request.getParameter("scSearchText2") != null && !request.getParameter("scSearchText2").trim().equals(""))
                  {
                    queueSearchVO.setKeywordSearchText2(request.getParameter("scSearchText2"));
                    queueSearchVO.setKeywordSearchCondition1(request.getParameter("scKeywordSearchCondition1"));
                  }else
                  {
                    queueSearchVO.setKeywordSearchText2(null);
                    queueSearchVO.setKeywordSearchCondition1(null);
                  }
                  
                  if(request.getParameter("scSearchText3") != null && !request.getParameter("scSearchText3").trim().equals(""))
                  {
                    queueSearchVO.setKeywordSearchText3(request.getParameter("scSearchText3"));
                    queueSearchVO.setKeywordSearchCondition2(request.getParameter("scKeywordSearchCondition2"));
                  }else
                  {
                    queueSearchVO.setKeywordSearchText3(null);
                    queueSearchVO.setKeywordSearchCondition2(null);
                  }
                  
                }else
                {
                  queueSearchVO.setKeywordSearchText1(null);
                  queueSearchVO.setKeywordSearchText2(null);
                  queueSearchVO.setKeywordSearchText3(null);
                  queueSearchVO.setKeywordSearchCondition1(null);
                  queueSearchVO.setKeywordSearchCondition2(null);
                }
                
                
                /*In prodPropMap, each key is a column name on product_master table.
                *For example, PREMIER_COLLECTION_FLAG.
                *For each entry in prodPropMap, there is a checkbox on the search page
                *named sc_<mapKey>. Loop through the map and see if any of the checkbox
                *is checkd. When checked, items with that product property needs to be added 
                *to the list which goes back to BO. */
                List includedProdProperties = new ArrayList();
                List includedPartners = new ArrayList();
                String prodPropStr = null;
                Iterator prodPropIt = prodPropMap.keySet().iterator();
                while (prodPropIt.hasNext()) {
                    String mapKey = (String)prodPropIt.next();
                    String includeProdPropKeyFlag =
                        request.getParameter("sc" + mapKey);   

                    if (includeProdPropKeyFlag != null && includeProdPropKeyFlag.equalsIgnoreCase("on")) {
                      includedProdProperties.add(mapKey);
                    }
                    logger.debug("prodPropStr is:" + prodPropStr);
                }
                
                /*In prefPartnerMap, each key is a column name on partner_master table.
                 * For each entry in prefPartnerMap, there is checkbox on the search page
                 * named sc_<mapKey>. Loop through the map ans see if any of the checkbox
                 * is checked. Checked partners need to be added to the list which goes back to BO.
                 */
                Iterator partnerIt = prefPartnerMap.keySet().iterator();
                while (partnerIt.hasNext()) {
                    String mapKey = (String)partnerIt.next();
                    String includePartnerFlag = request.getParameter("sc" + mapKey);   

                    if (includePartnerFlag != null && includePartnerFlag.equalsIgnoreCase("on")) {
                      includedPartners.add(mapKey);
                    }
                    logger.debug("excluding partner:" + mapKey);
                }
                //Set Ask Answer Code value as "P" if queue type is ASKP
                if(queueType.equalsIgnoreCase("ASKP"))
                {
                  queueSearchVO.setQueueType("ASK");
                  queueSearchVO.setAskAnsCode("P");
                  
                }
                queueSearchVO.setIncludedProdProperties(includedProdProperties);
                queueSearchVO.setIncludedPartners(includedPartners);
                CachedResultSet tuples = 
                    queueBO.getQueueMessages(queueSearchVO);

                // Compose the collection of records for presentation,
                // a delimited list of values.
                StringBuffer sb = new StringBuffer("SYSTEM\t");
                sb.append("MESSAGE ID\t");
                sb.append("QUEUE TYPE\t");
                sb.append("MESSAGE TYPE\t");
                sb.append("MESSAGE TIMESTAMP\t");
                sb.append("MERCURY NUMBER\t");
                sb.append("EXTERNAL ORDER NUMBER\t");
                sb.append("SENDING ENTITY\t");
                sb.append("PRICE\t");
                sb.append("INCREMENTAL AMOUNT\t");
                sb.append("OPERATOR\t");
                sb.append("PARTNER ORDERS\t");
                sb.append("PREMIER COLLECTION\t");
                sb.append("Is TAGGED ORDER\t");
                sb.append("HAS ADD-ONs\t");
                sb.append("DELIVERY DATE\t");
                sb.append("NOVATOR SKU\t");
                sb.append("COMMENTS\n");
                
                final DateFormat DF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                final DecimalFormat NF0 = new DecimalFormat("#####################0");
                final DecimalFormat NF2 = new DecimalFormat("#,###,##0.00");

                // tab-delimited so Excel will provide automatic formatting.
                while (tuples.next()) {
                    
                    sb.append(messageSystem);
                    sb.append("\t");
                    
                    BigDecimal messageId = tuples.getBigDecimal("message_id");
                    if (messageId != null) {
                    sb.append(NF0.format(messageId));
                    }
                    sb.append("\t");
                    
                    String queuetype = (String)tuples.getObject("queue_type");
                    if (queuetype != null) {
                    sb.append(queuetype);
                    }
                    sb.append("\t");
                    
                    String messageType = (String)tuples.getObject("message_type");
                    if (messageType != null) {
                    sb.append(messageType);
                    }
                    sb.append("\t");
                    
                    Date messageTimestamp = tuples.getDate("message_timestamp");
                    if (messageTimestamp != null) {
                    sb.append(DF.format(messageTimestamp));
                    }
                    sb.append("\t");
                    
                    String mercuryNumber = (String)tuples.getObject("mercury_number");
                    if (mercuryNumber != null) {
                    sb.append(mercuryNumber);
                    }
                    sb.append("\t");
                    
                    String externalOrderNumber = (String)tuples.getObject("external_order_number");
                    if (externalOrderNumber != null) {
                    sb.append(externalOrderNumber);
                    }
                    sb.append("\t");
                    
                    String sendingEntity =  (String)tuples.getObject("sending_entity");
                    if (sendingEntity != null) {
                    sb.append(sendingEntity);
                    }
                    sb.append("\t");
                    if(tuples.getObject("price") != null){
                      BigDecimal price = tuples.getBigDecimal("price");
                      if (price != null) {
                      sb.append(NF2.format(price));
                      }
                      
                    }
                    sb.append("\t");
                    if(tuples.getObject("price") != null && tuples.getObject("price_askp") != null)
                    {
                      BigDecimal priceFtd = tuples.getBigDecimal("price");
                      BigDecimal priceAskp = tuples.getBigDecimal("price_askp");
                      BigDecimal incPrice = priceAskp.subtract(priceFtd);
                      sb.append(NF2.format(incPrice));
                      
                    }
                    sb.append("\t");
                    
                    String operator = (String)tuples.getObject("operator");
                    if (operator != null) {
                    sb.append(operator);
                    }
                    sb.append("\t");
                    
                    
                    // PARTNER ORDERS
                    String partnerName = (String)tuples.getObject("partner_name");
                    if(partnerName != null) 
                      sb.append(partnerName);
                    sb.append("\t");
                    
                    // PREMIER COLLECTION
                    String premierCollectionFlag = (String)tuples.getObject("premier_collection_flag");
                    if (premierCollectionFlag != null && premierCollectionFlag.equalsIgnoreCase("Y")) {
                        sb.append(premierCollectionFlag);
                    } else {
                        sb.append(" ");
                    }
                    sb.append("\t");
                    
                    //
                    // Tagged or not
                    String isTagged = (String)tuples.getObject("is_tagged");
                    if (isTagged != null) {
                        sb.append("Y");
                    } else {
                        sb.append("N");
                    }
                    sb.append("\t");
                    
                    // Has Addons
                    String hasAddons = (String)tuples.getObject("has_addons");
                    if (hasAddons != null) {
                        sb.append("Y");
                    } else {
                        sb.append("N");
                    }
                    sb.append("\t");
                    
                    // Delivery Date
                    Date deliveryDate = (Date)tuples.getObject("delivery_date");
                    if (deliveryDate != null) {
                        sb.append(deliveryDate);
                    } else {
                        sb.append(" ");
                    }
                    sb.append("\t");
                    
                    // Novator Id
                    String novatorId = (String)tuples.getObject("novator_id");
                    if (novatorId != null) {
                        sb.append(novatorId);
                    } else {
                        sb.append(" ");
                    }
                    sb.append("\t");
                    
                    String comments = (String)tuples.getObject("comments");
                    if (comments != null) { 
                    // Replace any hard-returns and tabs.
                    comments = comments.replaceAll("\n|\r|\t", " ");
                    sb.append(comments);
                    }
                    sb.append("\n");
                }

                // Set the output data's mime type
                response.setContentType("application/vnd.ms-excel"); // MIME type for excel sheet

                BufferedWriter writer = 
                    new BufferedWriter(response.getWriter());

                try {
                    writer.write(sb.toString());
                } finally {
                    if (writer != null) {
                        writer.flush();
                        writer.close();
                    }
                }

                return null;
            }
            
            // Append pageData to the response.
             DOMUtil.addSection(responseDocument, "pageData", "data", pageDataMap, false);

            // Compose common screen elements.
            Document queueTypesDoc = queueBO.getAllMQDQueueTypes();
            DOMUtil.addSection(responseDocument, queueTypesDoc.getChildNodes());
            DOMUtil.addSection(responseDocument, prefPartnerDoc.getChildNodes());
            DOMUtil.addSection(responseDocument, "productProperties", "productProperty", prodPropMap, true);
            
            //Get XSL File name
            File xslFile = getXSL(xslName, mapping);
            //Transform the XSL File
                ActionForward forward = mapping.findForward(xslName);
                String xslFilePathAndName = 
                    forward.getPath(); //get real file name

                // Change to client side transform
                TraxUtil.getInstance().getInstance().transform(request, 
                                                               response, 
                                                               responseDocument, 
                                                               xslFile, 
                                                               xslFilePathAndName, 
                                                               (HashMap)request.getAttribute(COMConstants.CONS_APP_PARAMETERS));
        

        } catch (Exception e) {
            logger.error(e);
            return mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
        } finally {
            try {
                //Close the connection
                if (conn != null) {
                    //conn.rollback();
                    conn.close();
                }
            } catch (Exception e) {
                logger.error(e);
                return mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
            }
        }
        
        return null;
    }
    
    
    
   

    private File getXSL(String xslName, ActionMapping mapping) {

        File xslFile = null;
        String xslFilePathAndName = null;

        ActionForward forward = mapping.findForward(xslName);
        xslFilePathAndName = forward.getPath(); //get real file name
        xslFile = 
                new File(this.getServlet().getServletContext().getRealPath(xslFilePathAndName));
        return xslFile;
    }


}
