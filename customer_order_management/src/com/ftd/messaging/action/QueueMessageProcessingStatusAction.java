package com.ftd.messaging.action;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.ftdutilities.FTDCommonUtils;
import com.ftd.messaging.bo.QueueBO;
import com.ftd.messaging.constants.MessagingConstants;
import com.ftd.messaging.form.QueueMessageProcessingForm;
import com.ftd.messaging.util.QueueBatchExcelFileCreator;
import com.ftd.messaging.vo.QueueDeleteHeaderStatusVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.handlers.GlobalParmHandler;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.QueueDeleteDetailVO;
import com.ftd.osp.utilities.xml.DOMUtil;

import com.ftd.security.SecurityManager;

import java.io.BufferedWriter;
import java.io.File;

import java.io.IOException;

import java.io.ObjectOutputStream;
import java.io.OutputStream;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.HashMap;

import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import javax.servlet.http.HttpServletResponse;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;





import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import org.xml.sax.SAXException;

/**
 * QueueMessageProcessingStatusAction extends DispatchAction
 * to handle the queue message processing screen events.
 * @author - Vinay Shivaswamy
 * @date - 12/1/2010
 * @version - 1.0 for MQD 4.7.0
 */
public class QueueMessageProcessingStatusAction extends DispatchAction{
    public QueueMessageProcessingStatusAction(){}
  
    private Logger logger = new Logger("com.ftd.messaging.action.QueueMessageProcessingStatusAction");
        
    private SecurityManager securityManager; 
    
    /**
    * This method will be executed on page load and click of refresh button with parameter action=loadData
    * @param mapping
    * @param form
    * @param request
    * @param response
    * @return ActionForward
    * @throws Exception
    */
    public ActionForward loadData(ActionMapping mapping,ActionForm form, HttpServletRequest request,HttpServletResponse response) throws Exception {
        String pageSize = null;

        String securityToken = request.getParameter(COMConstants.SEC_TOKEN);
            if(securityToken == null)
                throw new RuntimeException("load()- Security Token request parameter can not be null, try login again.");
        String context = request.getParameter(COMConstants.CONTEXT);
            if(context == null)
                throw new RuntimeException("load()- context request parameter can not be null, try login again.");                 
        try{
            QueueMessageProcessingForm qForm = (QueueMessageProcessingForm) form;
            boolean isQueueDeleteManager = securityManager.getInstance().assertPermission(context, securityToken, MessagingConstants.QUEUE_BATCH_USER_ROLE, MessagingConstants.QUEUE_BATCH_USER_ROLECH_USER_PERMISSION); 
            qForm.setQueueDeleteManager(isQueueDeleteManager); 
            if(isQueueDeleteManager){
                qForm.setUsers((ArrayList)getUsers());
                String selectedUser = request.getParameter(MessagingConstants.USER_LIST);
                //For initial load, selectedUser will be null.  So, default it to "All". Subsequent refresh will be driven by the user id that the user has selected
                if (selectedUser == null || selectedUser.equalsIgnoreCase(""))
                  selectedUser = "All";
                qForm.setSelectedUser(selectedUser);
                qForm.setHeaders(getBatchData(selectedUser));
            }else{                
                qForm.setHeaders(getBatchData(getUserId(securityToken)));
            }
            
            if(qForm.getHeaders()!=null && qForm.getHeaders().size()>0){
                //Get the GlobalParmHandler to retrieve the param that dictates the number of rows to display per page.
                GlobalParmHandler gHandler = (GlobalParmHandler) CacheManager.getInstance().getHandler("CACHE_NAME_GLOBAL_PARM");
                if(gHandler != null)
                    pageSize  = gHandler.getGlobalParm(MessagingConstants.GLOBAL_PARM_QUEUE_CONFIG, MessagingConstants.MAX_RECORDS_PER_PAGE);
                if(StringUtils.isNotBlank(pageSize))
                    qForm.setPageSize(Integer.parseInt(pageSize));
                else
                    qForm.setPageSize(Integer.parseInt(MessagingConstants.PAGE_SIZE));
            }
        }
        catch (Exception e){
            if(logger.isDebugEnabled())
                logger.debug("loadData()-Error while retrieving BatchData: "+e.getMessage());
            return mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
        }
    
        return mapping.findForward(MessagingConstants.SUCCESS_STATUS);
    }
    
    
    /**
    * This method will be executed when parameter action=generateExcel
    * @param mapping
    * @param form
    * @param request
    * @param response
    * @return ActionForward
    * @throws Exception
    */
    public ActionForward generateExcel(ActionMapping mapping,ActionForm form, HttpServletRequest request,HttpServletResponse response) throws Exception {
        BufferedWriter writer = null;
        OutputStream os = null;
        ObjectOutputStream objectOut = null;
        try {
            String securityToken = request.getParameter(COMConstants.SEC_TOKEN);
            if(securityToken == null)
                throw new RuntimeException("generateExcel()-Security Token request parameter can not be null, try login again.");
                        
            String qDeleteHeaderId = request.getParameter(COMConstants.QUEUE_DELETE_HEADER_ID);
            if(StringUtils.isEmpty(qDeleteHeaderId))
                throw new RuntimeException("generateExcel()-QueueDeleteHeaderId request parameter can not be null or blank, try login again.");
            
            //Now convert qDeleteHeaderId into Long  
            Long headerId = new Long(qDeleteHeaderId);  
            
            //Call getExcelData() to get QueueDeleteHeaderStatusVO
            QueueDeleteHeaderStatusVO statusVO = getExcelData(securityToken, headerId);
            if(statusVO == null)
                throw new RuntimeException("generateExcel()-Error retrieving QueueHeaderStatusVO, try login again.");

            //Call buildExcelData() to build the data string
            String data = buildExcelData(statusVO);

            //set the response header and content
            response.setContentType("application/vnd.ms-excel");
            response.setHeader( "Content-Disposition", "attachment; filename=\"" + statusVO.getLinkToSave() + "\"" );
            response.setHeader("Pragma", "public");
            response.setHeader("Cache-Control", "max-age=10");

            os = response.getOutputStream();
            QueueBatchExcelFileCreator excelCreator = new QueueBatchExcelFileCreator();
            HSSFWorkbook workbook = excelCreator.createWorkbook(statusVO);
            workbook.write(os);
        }
        catch(IOException io)
        {
          logger.error("generateExcel()-Error writing Output Stream: "+io.getMessage());
          throw new Exception(io);
        }
        catch(Exception e)
        {
          logger.error("generateExcel()-Error processing Excel data: "+e.getMessage());
          throw new Exception(e);
        }
        finally {
          try{
              os.flush();
              os.close();
          }
          catch(Exception e){
            return mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
          }
        }
        
        return null;
    }
    
    /**
    * This method calls QueueBO.getQueueDeleteHeaderStatusByUserId() to retrieve the List<QueueDeleteHeaderStatusVO>
    * @param String securityToken
    * @return List<QueueDeleteHeaderStatusVO>
    * @throws Exception
    */
    private List<QueueDeleteHeaderStatusVO> getBatchData(String userId) throws Exception{
        ArrayList<QueueDeleteHeaderStatusVO> headers = null;
        Connection connection = null;
        try{
            connection = getDatabaseConnection();
            QueueBO queueBo = new QueueBO(connection);
            //String userId = queueBo.lookupCurrentUserId(securityToken);
            headers = (ArrayList<QueueDeleteHeaderStatusVO>)queueBo.getQueueHeaderStatus(userId, null);
        }
        catch (Exception e){
          if(logger.isDebugEnabled())
            logger.debug("getBatchData - Error retrieving BatchData: "+e.getMessage());
          throw e;
        }
        finally {
          if (connection != null)
            connection.close();
        }
        
        return headers;
    }
  
    /**
    * This method calls QueueBO.getQueueDeleteHeaderStatusByHeaderId() to retrieve the QueueDeleteHeaderStatusVO
    * @param String securityToken
    * @param Long queueDeleteHeaderID
    * @return QueueDeleteHeaderStatusVO
    * @throws Exception
    */
    private QueueDeleteHeaderStatusVO getExcelData(String securityToken, Long queueDeleteHeaderID) throws Exception{
        QueueDeleteHeaderStatusVO status = null;
        Connection connection = null;
        try{
            connection = getDatabaseConnection();
            QueueBO queueBo = new QueueBO(connection);
            String userId = queueBo.lookupCurrentUserId(securityToken);
            //passing null for now since userId could be different than actaully the user created the batch
            status = queueBo.getQueueHeaderDetails(null, queueDeleteHeaderID);
        }
        catch (Exception e){
          if(logger.isDebugEnabled())
            logger.debug("Error retrieving ExcelData: "+e.getMessage());
          throw e;
        }
        finally {
          if (connection != null)
            connection.close();
        }
        
        return status;
    }
    
    /**
    * This method calls QueueBO.getUsersFromQueueDeleteHeader() to retrieve all the users available. 
    * @return List<String>
    * @throws Exception
    */
    private List<String> getUsers() throws Exception{
        List<String> users = null;
        Connection connection = null;
        try{
            connection = getDatabaseConnection();
            QueueBO queueBo = new QueueBO(connection);
            users = queueBo.getUsersFromQueueDeleteHeader();
        }
        catch (Exception e){
          if(logger.isDebugEnabled())
            logger.debug("Error retrieving Users: "+e.getMessage());
          throw e;
        }
        finally {
          if (connection != null)
            connection.close();
        }
        
        return users;
    }
    
    /**
    * @return String
    * @throws Exception
    */
    private String getUserId(String securityToken) throws Exception{
        String userId = null;
        Connection connection = null;
        try{
            connection = getDatabaseConnection();
            QueueBO queueBo = new QueueBO(connection);
            userId = queueBo.lookupCurrentUserId(securityToken);
        }
        catch (Exception e){
          if(logger.isDebugEnabled())
            logger.debug("Error retrieving UserId: "+e.getMessage());
          throw e;
        }
        finally {
          if (connection != null)
            connection.close();
        }
        
        return userId;
    }
    
    
    /**
    * Creates and returns Database connection
    * @param None
    * @return Connection 
    * @throws Exception
    */
    private Connection getDatabaseConnection() throws Exception{
        if(logger.isDebugEnabled())
          logger.debug("Creating Database Connection");
            
        Connection connection = null;
          try{
              connection = DataSourceUtil.getInstance().getConnection(ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE,COMConstants.DATASOURCE_NAME));
            }
            catch (Exception e){
            if(logger.isDebugEnabled())
              logger.debug("Error while creating Database Connection: "+e.getMessage());
            throw e;
            }
        if(logger.isDebugEnabled())
          logger.debug("Created Database Connection");
          
        return connection;
    } 
  
    /**
    * This method builds the formatted data for excel
    * @param QueueDeleteHeaderStatusVO status
    * @return String
    */
    private String buildExcelData(QueueDeleteHeaderStatusVO status){
        StringBuffer sb = new StringBuffer();
        sb.append("Message Id \t")
          .append("External Order Number \t")
          .append("Result \t")
          .append("Reason for Failure \n");
        
        List<QueueDeleteDetailVO> details = status.getQueueDeleteDetailVOList();
        if(details.size()>0){
          for(QueueDeleteDetailVO detailVO:details){
            sb.append(detailVO.getMessageId())
              .append("\t")
              .append(detailVO.getExternalOrderNumber())
              .append("\t");
            if(StringUtils.isEmpty(detailVO.getProcessedStatus() )){
              sb.append(" ").append("\t").append(" ").append("\n");
            }
            else {
                if(detailVO.getProcessedStatus().equalsIgnoreCase(MessagingConstants.SUCCESS_STATUS)){
                  sb.append(MessagingConstants.SUCCESS_STATUS).append("\t").append(" ").append("\n");
                }
                if(detailVO.getProcessedStatus().equalsIgnoreCase(MessagingConstants.FAILURE_STATUS)){
                  sb.append(MessagingConstants.FAILURE_STATUS).append("\t").append(detailVO.getErrorText()).append("\n");
                }
                if(detailVO.getProcessedStatus().equalsIgnoreCase(MessagingConstants.DUPLICATE_STATUS)){
                  sb.append(MessagingConstants.DUPLICATE_STATUS).append("\t").append(" ").append("\n");
                }
            }
          }
        }
        return sb.toString();
    }

}//End of Class
