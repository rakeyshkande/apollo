package com.ftd.messaging.action;

import com.ftd.messaging.bo.FloristSelectBO;
import com.ftd.messaging.constants.MessagingConstants;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.w3c.dom.Document;


/**
 * This class extends the com.ftd.messaging.action.BaseAction. It handles
 * retrieving florist list based the order recipient zip code.
 * @author Charles Fox
 * @todo check comments, error handling, and message handling
 * @todo confirm stored procedure setup against actual stored procedures
 */
public class FloristLookupAction extends BaseAction
{

    private Logger logger = 
        new Logger("com.ftd.messaging.action.FloristLookupAction");
        
    private static String SUCCESS = "success";
    private static String FAILURE = MessagingConstants.ERROR;
    public FloristLookupAction()
    { 
    }

    /**
     * � Call lookupFloristList method of FloristSelectBO to get the Document 
     *   object that contains a list of florists that serve the zip code.
     * � Call doForward of its parent class to add security node to the 
     *  Document object, get XSL file and utilize TraxUtil to transform XML 
     *  and XSL to render UI page.
     * @param mapping - ActionMapping
     * @param form - ActionForm
     * @param request - HttpServletRequest
     * @param response - HttpServletResponse
     * @return ActionForward
     * @throws Exception????
     * @todo - code and determine exceptions
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form, 
        HttpServletRequest request, HttpServletResponse response){

        logger.info("Entering Action Florist Lookup");
        String result = null;
        Connection con=null;
        try{
            con=this.createDatabaseConnection();
            HashMap parameters = this.getRequestDataMap(request);
            FloristSelectBO floristSelect = new FloristSelectBO();
            floristSelect.setConn(con);
            Document florists = floristSelect.lookupFloristList(request,parameters);
            
            this.doForward(SUCCESS, florists, mapping, request, response, parameters); 
            return null;
           
        }catch(Exception e){
            logger.error(e);
            result = FAILURE;
        }finally{
            if(con!=null)
            {
              try {
                    con.close();
                } catch (Exception e) {
                    logger.error("can not close connection", e);
                }
            }
            logger.info("Exiting Action Florist Lookup");
        }
        return mapping.findForward(result);
    }
}