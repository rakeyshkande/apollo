package com.ftd.messaging.action;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.dao.LockDAO;
import com.ftd.messaging.constants.MessagingConstants;
import com.ftd.messaging.util.MessagingDOMUtil;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/**
 * This class is an abstract class. It defines common behaviors
 * (i.e. setting security, retrieving XSL file and transforming the file) of 
 * its concreted sub-action classes. 
 * @author Charles Fox
 * @todo check comments, error handling, and message handling
 */
public abstract class BaseAction extends Action
{
    private Logger logger = 
        new Logger("com.ftd.messaging.action.BaseAction");
        
    public BaseAction()
    {
    }

    /**
     * Return appropriate xsl stylesheet. The XSL file is configured in 
     * struts-config.xml as an action forward
     * String xslFileName=actionMapping.findForward(forwardName).getPath();
     * File xslFile=this.servlet.getServletContext().getRealPath(xslFileName);
     * @param forwardName - String
     * @param actionMapping - ActionMapping
     * @return File - xsl stylesheet
     * @throws Exception????
     * @todo - code and determine exceptions
     */
    protected File getXSL(String forwardName, ActionMapping actionMapping) 
        throws Exception{
        
        if(logger.isDebugEnabled()){
            logger.debug("Entering getXSL");
            logger.debug("forwardName " + forwardName);
            logger.debug("actionMapping " + actionMapping.toString());
        }    
        
        File xslFile = null;
        
        try{
            String xslFileName=actionMapping.findForward(forwardName).getPath();
            xslFile=new File(this.servlet.getServletContext().getRealPath(xslFileName));
        
        }finally{
            if(logger.isDebugEnabled()){
                logger.debug("Exiting getXSL");
            }
        }
        return xslFile;

    }

    /**
     * Call the transform method in the com.ftd.osp.utilities.xml.TraxUtil 
     * class, pass over a Document object which will contain all of the 
     * message/order related data and security data and the name of the 
     * XSL page to be rendered.
     * @param doc - Document
     * @param xslFile - File
     * @param request - HttpServletRequest
     * @param response - HttpServletResponse
     * @return n/a
     * @throws TransformerException
     * @throws IOException
     * @throws Exception
     * @todo - code and determine exceptions
     */
    protected void transform(Document doc, File xslFile, String xslFilePathAndName, 
        HttpServletRequest request, HttpServletResponse response, 
        HashMap parameters)
        throws TransformerException, IOException, Exception
        {
            if(logger.isDebugEnabled()){
                logger.debug("Entering transform");
                logger.debug("doc " + doc);
                logger.debug("xslFile " + xslFile);
                logger.debug("request " + request);
                logger.debug("response " + response);
            } 
            try{
                //add load member data url to the doc.
                String loadMemberDataUrl= ConfigurationUtil.getInstance().getFrpGlobalParm(COMConstants.COM_CONTEXT, COMConstants.CONS_LOAD_MEMBER_DATA_URL);
    
                if(loadMemberDataUrl!=null){
                    MessagingDOMUtil.addElementToXML("load_member_data_url", loadMemberDataUrl, doc);
                }
                if (request.getParameter(MessagingConstants.REQUEST_FLORIST_SELECTION_LOG_ID) != null)
                {
                	String floristSelectionLogId = request.getParameter(MessagingConstants.REQUEST_FLORIST_SELECTION_LOG_ID).toString();
                	logger.info("floristSelectionLogId: " + floristSelectionLogId);
                	MessagingDOMUtil.addElementToXML(MessagingConstants.REQUEST_FLORIST_SELECTION_LOG_ID, floristSelectionLogId, doc);
                }
                if (request.getParameter("zip_city_flag") != null)
                {
                	String zipCityFlag = request.getParameter("zip_city_flag").toString();
                	logger.info("zipCityFlag: " + zipCityFlag);
                	MessagingDOMUtil.addElementToXML("zip_city_flag", zipCityFlag, doc);
                }
                /*if(logger.isDebugEnabled()){
                    Document xml=(Document)doc;
                    StringWriter sw = new StringWriter(); 
                    xml.print(new PrintWriter(sw));
                    
                    logger.debug("Messaging XML= \n" + sw.toString());
                }*/
                //StringWriter sw = new StringWriter();       //string representation of xml document
                //DOMUtil.print((Document) doc,new PrintWriter(sw));
                //logger.info("- execute(): COM returned = \n" + sw.toString());
                
                // Change to client side transform
                TraxUtil.getInstance().getInstance().transform(request, response, (Document) doc, 
                                    xslFile, xslFilePathAndName, parameters);
                
            }finally{
                if(logger.isDebugEnabled()){
                    logger.debug("Exiting transform");
                } 
            }
        }

    /**
     * The main processing method that will be called by its subclasses.
     * @param forwardName - String
     * @param doc - Document
     * @param actionMapping - ActionMapping
     * @param request - HttpServletRequest
     * @return n/a
     * @throws TransformerException
     * @throws IOException
     * @throws Exception
     * @todo - code and determine exceptions
     */
    public void doForward(String forwardName, Document doc, 
        ActionMapping actionMapping, HttpServletRequest request, 
        HttpServletResponse response, HashMap parameters) 
        throws TransformerException, IOException, Exception
        {

            if(logger.isDebugEnabled()){
                logger.debug("Entering doForward");
                logger.debug("forwardName " + forwardName);
                logger.debug("doc " + doc);
                logger.debug("actionMapping " + actionMapping);
                logger.debug("request " + request);
                logger.debug("response " + response);
                logger.debug("parameters " + parameters);
            } 
            try{
                //StringWriter sw = new StringWriter();       //string representation of xml document
                //DOMUtil.print((Document) doc,new PrintWriter(sw));

            	/* Call the getXSL method to obtain appropriate xsl stylesheet. */
                 File xslFile = getXSL(forwardName,actionMapping);
                 
                 ActionForward forward = actionMapping.findForward(forwardName);
                 String xslFilePathAndName = forward.getPath(); //get real file name
                 
                /* Call the transform method to render UI page. */
                if(parameters==null)
                {
                  transform(doc,xslFile,xslFilePathAndName,request,response,null);
                }else{
                    if(parameters.isEmpty())
                    {
                        transform(doc,xslFile,xslFilePathAndName,request,response,null);    
                    }
                    else
                    {
                        transform(doc,xslFile,xslFilePathAndName,request,response,parameters);    
                    }
                  
                }
                
            }finally{
                if(logger.isDebugEnabled()){
                    logger.debug("Exiting doForward");
                } 
            }
        }
    
    
    /**
     * Create a connection to the database
     * @param n/a
     * @return Connection
     * @throws Exception
     */
     protected Connection createDatabaseConnection() 
         throws IOException, SAXException, ParserConfigurationException, 
         TransformerException, Exception
     {
         return createDatabaseConnection(false);
     }
     
    protected Connection createDatabaseConnection(boolean isAutoCommitOff) 
        throws IOException, SAXException, ParserConfigurationException, 
        TransformerException, Exception
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering createDatabaseConnection");
        }

        Connection connection = null;
        try{
            /* create a connection to the database */
            connection = DataSourceUtil.getInstance().
                getConnection(ConfigurationUtil.getInstance().getProperty(
                COMConstants.PROPERTY_FILE,COMConstants.DATASOURCE_NAME));
            
            if (isAutoCommitOff) {
                // We need to control the transaction boundaries.
                connection.setAutoCommit(false);
            }

        }finally{
            if(logger.isDebugEnabled()){
               logger.debug("Exiting createDatabaseConnection");
            }
        }
        
        return connection;
    }

    public String getMessageFromResource(String key, HttpServletRequest request) {

        MessageResources resource = this.getResources(request);
        return resource.getMessage(key);

    }
    
    public HashMap getRequestDataMap(HttpServletRequest request)
    {
      HashMap dataMap=(HashMap)request.getAttribute(COMConstants.CONS_APP_PARAMETERS);
      
      if(dataMap.get("destination") != null)
        if(dataMap.get("destination").toString().equals(""))
          dataMap.put("destination", request.getParameter("destination"));
          
      if(dataMap.get("msg_message_type") != null)
        if(dataMap.get("msg_message_type").toString().equals(""))
          dataMap.put("msg_message_type", request.getParameter("msg_message_type"));
      
      if(dataMap.get("reason") != null)
        if(dataMap.get("reason").toString().equals(""))
          dataMap.put("reason", request.getParameter("reason"));
          
      if(dataMap.get("new_mercury_order_amount") != null)
        if(dataMap.get("new_mercury_order_amount").toString().equals(""))
          dataMap.put("new_mercury_order_amount", request.getParameter("new_mercury_order_amount"));
          
      
      if(dataMap.get("new_msg_message_type") != null)
        if(dataMap.get("new_msg_message_type").toString().equals(""))
          dataMap.put("new_msg_message_type", request.getParameter("new_msg_message_type"));
      
          
      if(dataMap.get("msg_order_type") != null)
        if(dataMap.get("msg_order_type").toString().equals(""))
          dataMap.put("msg_order_type", request.getParameter("msg_order_type"));
      
          
      if(dataMap.get("order_detail_id") != null)
        if(dataMap.get("order_detail_id").toString().equals(""))
          dataMap.put("order_detail_id", request.getParameter("order_detail_id"));
          
      if(dataMap.get("msg_message_order_number") != null)
        if(dataMap.get("msg_message_order_number").toString().equals(""))
          dataMap.put("msg_message_order_number", request.getParameter("msg_message_order_number"));
      
      if(dataMap.get("securitytoken") != null)
        if(dataMap.get("securitytoken").toString().equals(""))
          dataMap.put("securitytoken", request.getParameter("securitytoken"));
          
      if(dataMap.get("context") != null)
        if(dataMap.get("context").toString().equals(""))
          dataMap.put("context", request.getParameter("context"));
      
      if(dataMap.get("msg_external_order_number") != null)
        if(dataMap.get("msg_external_order_number").toString().equals(""))
          dataMap.put("msg_external_order_number", request.getParameter("msg_external_order_number"));
          
      if(dataMap.get("msg_guid") != null)
        if(dataMap.get("msg_guid").toString().equals(""))
          dataMap.put("msg_guid", request.getParameter("msg_guid"));
          
      if(dataMap.get("msg_customer_id") != null)
        if(dataMap.get("msg_customer_id").toString().equals(""))
          dataMap.put("msg_customer_id", request.getParameter("msg_customer_id"));

      if (dataMap.get("source_code") != null)
        if(dataMap.get("source_code").toString().equals(""))
          dataMap.put("source_code", request.getParameter("source_code"));
      
      if(dataMap==null)
      {
        dataMap=new HashMap();
      }
      return dataMap;
    }
    
    
    public void setRequestDataMap(HttpServletRequest request, HashMap dataMap)
    {
      
       request.setAttribute(COMConstants.CONS_APP_PARAMETERS, dataMap);
    }
    
    
    
     public void releaseLock(HttpServletRequest request, Connection dbConnection, String forward) 
    {
      
        if(forward!=null&&forward.equalsIgnoreCase(MessagingConstants.ACTION_FORWARD_COMMUNICATION))
        {
          String sessionId=request.getParameter("securitytoken");
          String operator = request.getParameter(MessagingConstants.MESSAGE_OPERATOR);
          String orderDetailId = request.getParameter(MessagingConstants.REQUEST_ORDER_DETAIL_ID);
          LockDAO lockDAO=new LockDAO(dbConnection);
          try
          {
            lockDAO.releaseLock(sessionId, operator,orderDetailId, "COMMUNICATION");
          }
          catch (Exception e)
          {
            logger.error("can not release lock", e);
          }
        }
      
      
    }
    
   public ActionForward getActionForward(String destination, HttpServletRequest request, ActionMapping mapping,  HashMap requestDataMap)
   {
     
        //HashMap requestDataMap=this.getRequestDataMap(request);
       /* Return ActionForward instance based on the forward action name. */
        ActionForward actionForward = mapping.findForward(destination);
        if (actionForward != null) {
            String path = actionForward.getPath();
            logger.debug("forward path=" + path);
            if (path != null) {
                
                     if(destination.equalsIgnoreCase(MessagingConstants.ACTION_FORWARD_COMMUNICATION)){
                        path = path + requestDataMap.get(MessagingConstants.REQUEST_ORDER_DETAIL_ID);//go back to communication screen
                        
                     }else if(destination.equalsIgnoreCase("recipient_order_page")){
                     
                          path +=requestDataMap.get(MessagingConstants.MESSAGE_EXTERNAL_ORDER_NUMBER)
                                +"&order_guid="+requestDataMap.get(MessagingConstants.MESSAGE_GUID)
                                + "&customer_id="  +requestDataMap.get(MessagingConstants.MESSAGE_CUSTOMER_ID);
                      }else //append message_order_number
                     {
                       if(path.indexOf("?")!=-1)
                       {
                         path +="&";
                       }else
                       {
                         path +="?";
                       }
                      path +=MessagingConstants.MESSAGE_ORDER_NUMBER+"="+requestDataMap.get(MessagingConstants.MESSAGE_ORDER_NUMBER);
                     }
                     
                     if(!path.startsWith("/"))
                     {
                       
                        path="/"+path;
                     }
                     
                     logger.debug("after path="+path);
                     actionForward= new ActionForward(path);  
            } else {
                logger.error("the forward path is null for forward name="
                        + destination);
                actionForward = mapping.findForward(MessagingConstants.ERROR);
            }
        } else {
            logger.error("the action forward is null for forward name="
                    + destination);
            actionForward = mapping.findForward(MessagingConstants.ERROR);
        }
        logger.debug("path="+actionForward.getPath());
        return actionForward;
     
   }
    
    
}