/*
 * Created on Apr 21, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.ftd.messaging.action;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.ftd.messaging.bo.PrepareMessageBO;
import com.ftd.messaging.cache.AdjustmentReasonCodeHandler;
import com.ftd.messaging.constants.MessagingConstants;
import com.ftd.messaging.util.MessagingDOMUtil;
import com.ftd.messaging.vo.MessageOrderStatusVO;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.security.SecurityManager;

/**
 * @author achen
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class AdjustmentAction extends PrepareMessageAction {

    /**
     * @param request
     * @param prepareMessageBO
     * @return
     * @throws SAXException
     * @throws IOException
     * @throws SQLException
     * @throws ParserConfigurationException
     * @see com.ftd.messaging.action.PrepareMessageAction#prepareMessage(javax.servlet.http.HttpServletRequest, com.ftd.messaging.bo.PrepareMessageBO)
     */

    private Logger logger = new Logger("com.ftd.messaging.action.AdjustmentAction");

    public Document prepareMessage(HttpServletRequest request,
            PrepareMessageBO prepareMessageBO) throws SAXException,
            IOException, SQLException, ParserConfigurationException, Exception {

        Document currentFlorist = this.prepareAdjustmentMessageForCurrentFlorist(
        request, prepareMessageBO);
        
        

       //add adjustment reason codes
        AdjustmentReasonCodeHandler adjReasonHandler = (AdjustmentReasonCodeHandler)
            CacheManager.getInstance().getHandler(
            MessagingConstants.AdjustmentReasonCodeHandler);
        adjReasonHandler.toXML(currentFlorist);

      //add cancel reason code to be used for ADJ.  This is only used for ESCALATE orders.
      MessagingDOMUtil.addElementToXML("ADJCancelReasonCode", (String)request.getAttribute("cancel_reason_code"), currentFlorist);

       return currentFlorist;
    }

    /**
     * @param request
     * @return
     * @see com.ftd.messaging.action.PrepareMessageAction#getPageTitle(javax.servlet.http.HttpServletRequest)
     */

    public String getPageTitle(HttpServletRequest request) {
        return this.getMessageFromResource("adjustment_page_title", request);
    }

    /**
     *
     * @param request
     * @param mapping
     * @param document
     * @param messageOrderStatus
     * @return
     * @see com.ftd.messaging.action.PrepareMessageAction#validateStatus(javax.servlet.http.HttpServletRequest, org.apache.struts.action.ActionMapping, org.w3c.dom.Document, com.ftd.messaging.vo.MessageOrderStatusVO)
     */

    public boolean validateStatus(HttpServletRequest request,
            ActionMapping mapping, Document document,
            MessageOrderStatusVO messageOrderStatus) throws Exception{

        logger.info("Entering validateStatus");

        boolean userInRole = false;
        boolean pastDeliveryDateMonth = messageOrderStatus.isPastDeliveryDateMonth();
        String messageType = request.getParameter(MessagingConstants.MESSAGE_TYPE);

        try{
            String subsequentAction = request.getParameter(MessagingConstants.SUBSEQUENT_ACTION);
            if(StringUtils.isBlank(subsequentAction)){
                //check the delivery date. The delivery date month has to be past of the current date month.
                if(messageType.equalsIgnoreCase("Venus")){
                    MessagingDOMUtil.addElementToXML(MessagingConstants.ERROR_MESSAGE, this.getMessageFromResource("adjustment_dropship_error", request), document);
                }
                else if(pastDeliveryDateMonth){
                //check for subsequent action, if there do not do security check
                
                    userInRole = SecurityManager.getInstance().assertPermission(
                    request.getParameter(MessagingConstants.REQUEST_CONTEXT),
                    request.getParameter(
                    MessagingConstants.REQUEST_SECURITY_TOKEN),
                    MessagingConstants.ADJUSTMENT_SECURITY_RESOURCE,
                    MessagingConstants.PERMISSION_VIEW);
                    
                     if(!userInRole)
                    {
                       MessagingDOMUtil.addElementToXML(MessagingConstants.ERROR_MESSAGE, this.getMessageFromResource("adjustment_user_role_error", request), document);
                    }

                    
                }else
                {
                  MessagingDOMUtil.addElementToXML(MessagingConstants.ERROR_MESSAGE, this.getMessageFromResource("adjustment_del_date_error", request), document);
                }
            }
            else
            {
                userInRole = true;
            }

        } finally {
            logger.info("Exiting validateStatus user in role="+userInRole);

        }

       
        return userInRole;
    }
    
    
     /**
     * Initialize �reason� field as �ORDER CANCELLED AFTER DELIVERY DATE. AUTO ADJ GENERATED.� for Venus ADJ
     */
    public String getInitialReasonText(HttpServletRequest request){
    	
    	  boolean systemGenerated=new Boolean(request.getParameter("system")).booleanValue();
        String messageType = request.getParameter(MessagingConstants.MESSAGE_TYPE);
        if(systemGenerated&&messageType.equalsIgnoreCase("Venus")){
          return this.getMessageFromResource("venus_adj_text", request);
        }else
        {
          return null;
        }
    }

}
