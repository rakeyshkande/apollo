package com.ftd.messaging.action;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.ftd.messaging.bo.FloristSelectBO;
import com.ftd.messaging.constants.MessagingConstants;
import com.ftd.messaging.util.MessagingDOMUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;

public class CheckFloristStatusAction extends BaseAction 
{
   private Logger logger = new Logger(
            "com.ftd.messaging.action.CheckFloristStatusAction");
  
  public CheckFloristStatusAction()
  {
  }
  
   public ActionForward execute(ActionMapping mapping, ActionForm form, 
        HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
            String floristCode=request.getParameter("florist_code");
            String messageType=request.getParameter(MessagingConstants.MESSAGE_TYPE);
            String forward="";
            String action=mapping.getParameter();
           
            Connection conn=null;
          try
          {
             Document doc=DOMUtil.getDocument();
             HashMap parameters=this.getRequestDataMap(request);
             
             /* Recipient zip code is used by the Florist Lookup screen via the 
              * Recipient Order Update Florist functionality.  
              */ 
             parameters.put(MessagingConstants.REQUEST_RECIP_ZIP_CODE, request.getParameter(MessagingConstants.REQUEST_RECIP_ZIP_CODE));  
             
             if(!action.equalsIgnoreCase("update_florist_form")){
                   
                  if(messageType.equalsIgnoreCase("Mercury"))
                  {
                   
                      conn=this.createDatabaseConnection();
                     
                     
                       //retrieve the florist from the database to make sure it is a valid florist.
                       FloristSelectBO floristBO=new FloristSelectBO();
                       floristBO.setConn(conn);
                       String forwardingURI=floristBO.findFloristStatusAndSendFTD(request, mapping);
                       logger.debug("forwardingURI=["+forwardingURI+"]");
                       if(forwardingURI.indexOf("error")!=-1){                           
                           //updated based on the messages defined in modify order functional spec.
                           String errorMsg=this.getMessageFromResource(forwardingURI,request);
                 
                           MessagingDOMUtil.addElementToXML(MessagingConstants.ERROR_MESSAGE, errorMsg, doc);
                       }else if(forwardingURI.indexOf("Florist is ")!=-1){  
                        	 MessagingDOMUtil.addElementToXML(MessagingConstants.ERROR_MESSAGE, forwardingURI, doc);
                       }else
                         {                          
                           MessagingDOMUtil.addElementToXML(MessagingConstants.FORWARDING_ACTION, mapping.findForward(forwardingURI).getPath(), doc);
                         }
                     }
                              
               }
               this.doForward(action, doc, mapping,request, response, parameters);
               return null;
             }
              catch (SAXException se) {
                logger.error(se);
                forward = MessagingConstants.ERROR;
            } catch (IOException ioe) {
                logger.error(ioe);
                forward = MessagingConstants.ERROR;
            } catch (SQLException sqle) {
                logger.error(sqle);
                forward = MessagingConstants.ERROR;
            } catch (ParserConfigurationException pe) {
                logger.error(pe);
                forward = MessagingConstants.ERROR;
            } catch (TransformerException te) {
                logger.error(te);
                forward = MessagingConstants.ERROR;
            } catch (Throwable t) {
                logger.error(t);
                forward = MessagingConstants.ERROR;
            }
              finally
              {
                //close database connection
                try
                {
                    /* close the database connection */
                    if(conn!=null){
                        conn.close();
                    } 
                }catch(Exception e){
                    throw new ServletException(e);
                }
              }
              
            
            
            return mapping.findForward(forward); 
        }
}