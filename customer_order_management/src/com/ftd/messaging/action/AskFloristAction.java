package com.ftd.messaging.action;
import com.ftd.messaging.constants.MessagingConstants;
import com.ftd.messaging.util.MessagingDOMUtil;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;

import com.ftd.messaging.bo.PrepareMessageBO;
import com.ftd.messaging.vo.MessageOrderStatusVO;
import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.ParserConfigurationException;


import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/**
 * This class extends PrepareMessageAction class. It will determine whether
 * we need to send user an error message or take user to an appropriate UI page.       
 * @author Charles Fox
 * @todo check comments, error handling, and message handling
 */
public class AskFloristAction extends PrepareMessageAction
{
    public AskFloristAction()
    {
    }

   
    /**
     * Add page_title element of the addition_info node of Document Object
     * @param request - HttpServletRequest
     * @param doc - Document
     * @return n/a
     * @throws Exception????
     * @todo - code and determine exceptions
     */
    public String getPageTitle(HttpServletRequest request){
       /* Override the parent class method. 
        Look up page title from Application Resource properties file that 
        configured in struts-config.xml.
            String pageTitle=this.getResource(request).getMessage(�ask_florist_page_title�);
        Add page_title element to the Document Object.
        */
    	return this.getMessageFromResource("ask_florist_page_title", request);
    }

    
    /**
     * � Call prepareMessage method of PrepareMessageBO to prepare a Document 
     * object for Delivery Confirmation page.
     * � Initialize �reason� field as �PLEASE CONFIRM DELIVERY UPON COMPLETION 
     * WITH DATE, TIME & SIGNATURE IF AVAILABLE.�
     * @param request - HttpServletRequest
     * @return Document
     * @throws ParserConfigurationException
     * @throws SQLException
     * @throws IOException
     * @throws SAXException
     * @throws Exception????
     * @todo - code and determine exceptions
     */
    public Document prepareMessage(HttpServletRequest request, PrepareMessageBO prepareMessageBO) throws SAXException, IOException, SQLException, ParserConfigurationException, Exception
    {
    	
      return this.prepareMessageForCurrentFlorist(request,prepareMessageBO);
    }


    /**
     * If the order is a vendor filled orde, return false.
     * @param request - HttpServletRequest
     * @param messageOrderStatus - MessageOrderStatusVO
     * @return boolean
     * @throws Exception????
     * @todo - code and determine exceptions
     */
    public boolean validateStatus(HttpServletRequest request,
            ActionMapping mapping, Document document,
            MessageOrderStatusVO messageOrderStatus){
        if(messageOrderStatus.isVendor()){
            
            MessagingDOMUtil.addElementToXML(MessagingConstants.ERROR_MESSAGE, this.getMessageFromResource(MessagingConstants.VENDOR_ERROR, request), document);
            return false;
         }else
         {
            return true;
         }
       
    }
}