package com.ftd.messaging.action;
import com.ftd.messaging.bo.PrepareMessageBO;
import com.ftd.messaging.constants.MessagingConstants;
import com.ftd.messaging.util.MessagingDOMUtil;
import com.ftd.messaging.vo.MessageOrderStatusVO;
import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class FloristAdditionInfoAction extends PrepareMessageAction 
{
  public FloristAdditionInfoAction()
  {
  }
  
  /**
     * Add page_title element of the addition_info node of Document Object
     * @param request - HttpServletRequest
     * @param doc - Document
     * @return n/a
     * @throws Exception????
     * @todo - code and determine exceptions
     */
    public String getPageTitle(HttpServletRequest request){
      
    	return this.getMessageFromResource("addition_info_page_title", request);
    }

    
    /**
     * @param request - HttpServletRequest
     * @return Document
     * @throws ParserConfigurationException
     * @throws SQLException
     * @throws IOException
     * @throws SAXException
     * @throws Exception????
     * @todo - code and determine exceptions
     */
    public Document prepareMessage(HttpServletRequest request, PrepareMessageBO prepareMessageBO) throws SAXException, IOException, SQLException, ParserConfigurationException, Exception
    {
    	return this.prepareMessageForCurrentFlorist(request,prepareMessageBO);
    }


    /**
     * If the order is Vendor filled orders or if a REJ has come in from the current florist or if a CAN has been sent to the current florist. return false
     * @param request - HttpServletRequest
     * @param messageOrderStatus - MessageOrderStatusVO
     * @return boolean
     * @throws Exception????
     * @todo - code and determine exceptions
     */
    public boolean validateStatus(HttpServletRequest request,
            ActionMapping mapping, Document document,
            MessageOrderStatusVO messageOrderStatus){
         boolean valid=false;
         if(messageOrderStatus.isVendor()){
            
            MessagingDOMUtil.addElementToXML(MessagingConstants.ERROR_MESSAGE, this.getMessageFromResource(MessagingConstants.VENDOR_ERROR, request), document);
            
         }else  if(messageOrderStatus.isFloristRejectOrder()){
                
             MessagingDOMUtil.addElementToXML(MessagingConstants.ERROR_MESSAGE, this.getMessageFromResource(MessagingConstants.REJECTED_ERROR, request), document);
            
            
        }else{
            valid=true;
        }
        
        
        
        return valid;
       
    }
}