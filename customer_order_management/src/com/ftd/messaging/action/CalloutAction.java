package com.ftd.messaging.action;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.messaging.bo.PrepareMessageBO;
import com.ftd.messaging.constants.MessagingConstants;
import com.ftd.messaging.util.MessagingDOMUtil;
import com.ftd.messaging.vo.MessageOrderStatusVO;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;

import java.sql.SQLException;

import java.util.HashMap;

import java.util.Iterator;
import javax.servlet.http.HttpServletRequest;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.struts.action.ActionMapping;

import org.w3c.dom.Document;

import org.xml.sax.SAXException;

/**
 * This class extends PrepareMessageAction class. It will determine whether 
 * we need to send user an error message or take user to an appropriate UI page.  
 * @author Charles Fox
 */
public class CalloutAction extends PrepareMessageAction 
{

    private Logger logger = new Logger(
        "com.ftd.messaging.action.CalloutAction");
    
    /**
     * prepare a Document object for Call Out Script page.
     * @param request - HttpServletRequest
     * @param prepareMessageBO - PrepareMessageBO
     * @return Document
     * @throws SAXException
     * @throws IOException
     * @throws SQLException
     * @throws ParserConfigurationException
     * @throws Exception
     */
    public Document prepareMessage(HttpServletRequest request,
            PrepareMessageBO prepareMessageBO) throws SAXException,
            IOException, SQLException, ParserConfigurationException, Exception
    {
        logger.debug("Entering prepareMessage");
        Document prepareMsgDoc = null;
        
        try{
            HashMap parameters = this.getRequestDataMap(request);
            prepareMsgDoc = this.prepareMessageForCallout(
                request,prepareMessageBO,parameters);
            
            this.setRequestDataMap(request, parameters);
        } finally {
            logger.debug("Exiting prepareMessage");
        }
        return prepareMsgDoc;
    }


    public String getPageTitle(HttpServletRequest request) { 
        return this.getMessageFromResource(
            MessagingConstants.CALL_OUT_PAGE_TITLE, request);        
    }
    /**
     * If the order is a vendor filled order, call setErrorMessage method to
     * add an error message node, return false. Otherwise return true.
     * @param request - HttpServletRequest
     * @param mapping - ActionMapping
     * @param document - Document
     * @param messageOrderStatus - MessageOrderStatusVO
     * @return Document
     * @throws Exception
     */
    public boolean validateStatus(HttpServletRequest request,
            ActionMapping mapping, Document document,
            MessageOrderStatusVO messageOrderStatus) throws Exception{
        
        boolean valid=false;
        try{
            logger.info("Entering validateStatus");
            
            if(messageOrderStatus.isVendor()){
                MessagingDOMUtil.addElementToXML(MessagingConstants.ERROR_MESSAGE, 
                    this.getMessageFromResource(MessagingConstants.VENDOR_ERROR, 
                    request), document);
                    
            }else if(!messageOrderStatus.isAttemptedFTD()){
                MessagingDOMUtil.addElementToXML(MessagingConstants.ERROR_MESSAGE, 
                    this.getMessageFromResource(
                    MessagingConstants.NEVER_ATTEMPTED_FTD_ERROR, 
                    request), document);

            }else if(messageOrderStatus.isHasLiveFTD()){
                MessagingDOMUtil.addElementToXML(MessagingConstants.ERROR_MESSAGE, 
                    this.getMessageFromResource(
                    MessagingConstants.HAS_LIVE_FTD_ERROR, 
                    request), document);
            
            }/*else if(messageOrderStatus.isPastDeliveryDate()){
                MessagingDOMUtil.addElementToXML(MessagingConstants.ERROR_MESSAGE, 
                    this.getMessageFromResource(
                    MessagingConstants.PAST_DELIVERY_DATE_ERROR, 
                    request), document);
            
            }*/else if(messageOrderStatus.isATypeRefund()){
                MessagingDOMUtil.addElementToXML(MessagingConstants.ERROR_MESSAGE, 
                    this.getMessageFromResource(
                    MessagingConstants.A_TYPE_REFUND_ERROR, 
                    request), document);
            
            }else{
                valid=true;
            }
       } finally {
               logger.info("Exiting validateStatus");
        }
        return valid;
    }
    
    
    public void process(HttpServletRequest request,
            ActionMapping actionMapping, Document doc,
            MessageOrderStatusVO messageOrderStatus,
            PrepareMessageBO prepareMessageBO) throws Exception{
      
       String forward=MessagingConstants.ACTION_PREPARE_MESSAGE;
       HashMap parameters=this.getRequestDataMap(request);
       String ftdMessageStatus=(String)parameters.get(MessagingConstants.FTD_MESSAGE_STATUS);
       logger.debug("ftdMessageStatus="+ftdMessageStatus);
       boolean ftdMessageCancelled=messageOrderStatus.isCancelSent();
       logger.debug("ftdMessageCancelled="+ftdMessageCancelled);  
       if(!ftdMessageStatus.equalsIgnoreCase("MM")||ftdMessageCancelled)
       {
         forward="callout_new_ftd";
       }
       
       logger.debug("call out forward="+forward);
       String forwardURI = actionMapping.findForward(forward).getPath();
       logger.debug("forwardURI: "+forwardURI);
       MessagingDOMUtil.addElementToXML(MessagingConstants.FORWARDING_ACTION, forwardURI, doc);
       MessagingDOMUtil.addElementToXML(COMConstants.ACTION,
                MessagingConstants.ACTION_PREPARE_MESSAGE, doc);

    }
    
}