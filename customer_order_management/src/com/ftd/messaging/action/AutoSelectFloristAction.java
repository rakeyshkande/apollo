package com.ftd.messaging.action;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.ftd.messaging.bo.FloristSelectBO;
import com.ftd.messaging.constants.MessagingConstants;
import com.ftd.osp.utilities.plugins.Logger;

public class AutoSelectFloristAction extends BaseAction 
{
  
   private Logger logger = new Logger(
            "com.ftd.messaging.action.AutoSelectFloristAction");
  
  
  public AutoSelectFloristAction()
  {
  }
  
  public ActionForward execute(ActionMapping mapping, ActionForm form, 
        HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
          
            
          String action = mapping.getParameter();
          String forward="";
          Connection con=null;
          try
          {
                con=this.createDatabaseConnection();
                HashMap parameters=this.getRequestDataMap(request);
                MessageResources resources=this.getResources(request);
                FloristSelectBO floristBO=new FloristSelectBO();
                floristBO.setConn(con);
                Document doc=floristBO.doAutoSelect(request, mapping, resources, parameters);
                
                if(StringUtils.isNotEmpty(action))
                  this.doForward(action, doc, mapping,request, response, parameters);
                else
                  this.doForward("auto_select_iframe", doc, mapping,request, response, parameters);
                
                return null;
              }
              catch (SAXException se) {
                logger.error(se);
                forward = MessagingConstants.ERROR;
            } catch (IOException ioe) {
                logger.error(ioe);
                forward = MessagingConstants.ERROR;
            } catch (SQLException sqle) {
                logger.error(sqle);
                forward = MessagingConstants.ERROR;
            } catch (ParserConfigurationException pe) {
                logger.error(pe);
                forward = MessagingConstants.ERROR;
            } catch (TransformerException te) {
                logger.error(te);
                forward = MessagingConstants.ERROR;
            } catch (Throwable t) {
                logger.error(t);
                forward = MessagingConstants.ERROR;
            }
              finally
              {
                 try {
                    if(con!=null){
                        con.close();
                    }
                } catch (Exception e) {
                    logger.error("can not close connection", e);
                }
                 logger.info("exit AutoSelectFloristAction");
              }
              
            
            
            return mapping.findForward(forward); 
        }
        
        
        
       
}