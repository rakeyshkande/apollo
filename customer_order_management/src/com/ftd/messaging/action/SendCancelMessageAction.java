package com.ftd.messaging.action;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.filter.DataFilter;
import com.ftd.messaging.bo.SendMessageBO;
import com.ftd.messaging.constants.MessagingConstants;
import com.ftd.messaging.vo.MessageOrderStatusVO;
import com.ftd.osp.utilities.plugins.Logger;
import java.sql.Connection;
import java.text.ParseException;
import java.util.HashMap;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * This class handles sending Cancel Message request.
 * 
 * @author Charles Fox
 * @todo check comments, error handling, and message handling
 * @todo confirm stored procedure setup against actual stored procedures
 */
public class SendCancelMessageAction extends BaseAction {
    private Logger logger = new Logger(
            "com.ftd.messaging.action.SendCancelMessageAction");

    public SendCancelMessageAction() {
    }

    /**
     * � Call sendCancelMessage method of SendMessageBO to get the forward
     * action name. � Add the request parameter map to HTTP request attributes. �
     * Return the ActionForward instance.
     * 
     * @param mapping -
     *            ActionMapping
     * @param form -
     *            ActionForm
     * @param request -
     *            HttpServletRequest
     * @param response -
     *            HttpServletResponse
     * @return ActionForward
     * @throws Exception????
     * @todo - code and determine exceptions
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws ServletException {

        String forward = "";
        ActionForward actionForward = null;
        HashMap requestDataMap = this.getRequestDataMap(request);
        Connection con = null;
        try {
            con = this.createDatabaseConnection();
            SendMessageBO sendMessageBO = new SendMessageBO();
            
            sendMessageBO.setDbConnection(con);
            forward = sendMessageBO.sendCancelMessage(request, requestDataMap);
            this.setRequestDataMap(request, requestDataMap);

            // Set DataFilter param indicating order was cancelled (for later Call Disposition check if order cancelled and resent)
            String orderDetailId = request.getParameter("order_detail_id");
            DataFilter.updateParameter(request, COMConstants.CDISP_CANCELLED_ORDER, orderDetailId);

        } catch (ParseException e) {
            logger.error(e);
            forward = MessagingConstants.ERROR;
        } catch (Exception e) {
            logger.error(e);
            forward = MessagingConstants.ERROR;
        } finally {
           
            //release lock as needed.
            releaseLock(request, con, forward);
            
            if (con != null) {
                
                
                try {
                    con.close();
                } catch (Exception e) {
                    logger.error("can not close connection", e);
                }
            }
        }

        logger.debug("forward=" + forward);

        /* Return ActionForward instance based on the forward action name. */
        actionForward = this.getActionForward(forward, request, mapping, requestDataMap);
        logger.debug("path="+actionForward.getPath());
        return actionForward;

    }

}