package com.ftd.messaging.action;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.dao.OrderDAO;
import com.ftd.messaging.bo.ViewMessageBO;
import com.ftd.messaging.constants.MessagingConstants;
import com.ftd.messaging.util.MessagingDOMUtil;
import com.ftd.messaging.vo.FTDMessageVO;
import com.ftd.messaging.vo.MessageVO;
import com.ftd.mo.util.ModifyOrderUTIL;
import com.ftd.osp.utilities.PASServiceUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.pas.core.domain.ProductAvailVO;

/**
 * This class extends com.ftd.messaging.action.BaseAction class. It is 
 * responsible for retrieving the information that is associated with messages
 * and forwarding control to a URI based on message detail type.    
 * @author Charles Fox
 * @todo check comments, error handling, and message handling
 */
public class ViewMessageAction extends BaseAction
{
    private Logger logger = 
        new Logger("com.ftd.messaging.action.ViewMessageAction");
   // private static String SUCCESS = "success";
    private static String FAILURE = MessagingConstants.ERROR;
    
    public ViewMessageAction()
    {
    }

    /**
     *  -Pass message id and message type over to retrieveMessage method of 
     *  ViewMessageBO class.
     *  -Call createDocument method to create Document object that contains 
     *  message data.
     *  -Look up page title for the given message type from Struts 
     *  application resource properties file.
     *  -Call findForward method to get the name of the forward.
     *  -Call doForward method of its super class and perform adding security 
     *  node to the Document, get XSL file and    transform XML and XSL to 
     *  render UI page.
     * @param n/a
     * @return n/a
     * @throws IOException
     * @throws ServletException
     * @todo - code and determine exceptions
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
	public ActionForward execute(ActionMapping mapping, ActionForm form, 
        HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException{

            logger.info("Entering View Message Action");
            
            Connection dbConnection = null;
            String result = null;
            String actionType = null;
            String msgExternalOrderNumber = null;
            String msgOrderGuid = null;
            String msgCustomerId = null;
            
            
            if(request.getParameter("ext_order_number") != null){
            	msgExternalOrderNumber = request.getParameter("ext_order_number");
            }
            if(request.getParameter("ord_guid") != null){
            	msgOrderGuid = request.getParameter("ord_guid");
            }
            if(request.getParameter("cus_id") != null){
            	msgCustomerId = request.getParameter("cus_id");
            }
            
            try
            {
                // get action type
                if(request.getParameter(COMConstants.ACTION_TYPE)!=null)
                {
                  actionType = request.getParameter(COMConstants.ACTION_TYPE);
                }
                else
                {
                    //assume display communication screen action
                    actionType = MessagingConstants.ACTION_VIEW_MESSAGE;
                }
                if(actionType.equals(MessagingConstants.ACTION_VIEW_MESSAGE)){
                    /*create database connection and View Message BO */
                    dbConnection = createDatabaseConnection();
                    ViewMessageBO viewMsg = new ViewMessageBO(dbConnection);
                    /* Pass message id and message type over to retrieveMessage 
                    *  method of ViewMessageBO class. */
                    String msgID = (request.getParameter(MessagingConstants.REQUEST_MESSAGE_ID)!=null)?request.getParameter(MessagingConstants.REQUEST_MESSAGE_ID): (String)request.getAttribute(
                        MessagingConstants.REQUEST_MESSAGE_ID);
                                        
                    String msgType =(StringUtils.isNotEmpty(request.getParameter(MessagingConstants.MESSAGE_TYPE)))?request.getParameter(MessagingConstants.MESSAGE_TYPE):(String)request.getAttribute(
                        MessagingConstants.MESSAGE_TYPE);
                        
                    String msgSubType =(StringUtils.isNotEmpty(request.getParameter(MessagingConstants.MESSAGE_SUB_TYPE)))?request.getParameter(MessagingConstants.MESSAGE_SUB_TYPE):(String)request.getAttribute(
                        MessagingConstants.MESSAGE_SUB_TYPE);
                                        
                    MessageVO msgVO = viewMsg.retrieveMessage(msgID,msgType,msgSubType);
                    ModifyOrderUTIL moUTIL = new ModifyOrderUTIL(dbConnection);
                    FTDMessageVO lastFtdMessageVO = moUTIL.getMessageDetail(msgVO.getOrderDetailId());
                    Date dShipDate = lastFtdMessageVO.getShipDate();
                    Date dDeliveryDate = lastFtdMessageVO.getDeliveryDate();
                    boolean isOrderInTransitBoolean = moUTIL.isOrderInTransit(msgVO.getOrderDetailId(), dShipDate, dDeliveryDate);

                    HashMap parameters=this.getRequestDataMap(request);

                    if(msgVO != null){
                        /* Call createDocument method to create Document object 
                         *  that contains message data. */
                        Document messages = createDocument(msgVO);
                       // if(msgVO!=null){
                            /* Call findForward method to get the name of the forward. */
                            String forwardLocation = findForward(msgVO,mapping);
                            setMessageIds(msgVO, parameters);
                            this.setRequestDataMap(request, parameters);
                            if(StringUtils.isBlank(forwardLocation)){
                               forwardLocation = "default";
                            }
                            setAdditionalInfo(request,mapping,messages);
   
                            //flag to indicate if a new FTD is allowed.
                            //always Y for florist items
                            //set to N for vendor orders where delivery date is invalid
                            String ftdAllowed = "N";
                            String isPersonalized = "N";                            
                            String isOrderInTransit = "N";
                            String sourceCode = (String) parameters.get("source_code");
                            //for vendor orders check if the delivery date on the FTD
                            //is still valid for the given ship method
                            if(msgType.equalsIgnoreCase("Venus"))
                            {
                                //get order information
                                OrderDAO orderDAO = new OrderDAO(dbConnection);
                                String orderDetailId = request.getParameter(MessagingConstants.REQUEST_ORDER_DETAIL_ID);
                                CachedResultSet orderDetailsInfo = orderDAO.getOrderDetailsInfo(orderDetailId);
                                orderDetailsInfo.next();
                                //String country = orderDetailsInfo.getString("country");
                                String product = orderDetailsInfo.getString("product_id");
                                String zip = orderDetailsInfo.getString("zip_code");
                                //String occasion = orderDetailsInfo.getString("occasion");
                                //String state = orderDetailsInfo.getString("state");
                                sourceCode = orderDetailsInfo.getString("source_code");
                                
                                //emueller 3/7/06 Added line to get ship method from order detail record
                                //String shipMethod = orderDetailsInfo.getString("ship_method");
                                
                                //get number of days before delivery date
                                //long diffMillis = msgVO.getDeliveryDate().getTime() - (new Date()).getTime();
                                //int diffDays = new Long(diffMillis/(24*60*60*1000)).intValue() + 1;  

                                //check if the deliery date on the order is still valid
                                //DeliveryDateUTIL util = new DeliveryDateUTIL();
                                //OEDeliveryDateParm oeParms = util.getCOMParameterData(country,product,null,zip,"CA", "US", "1", occasion,dbConnection, state, "FTD", false, false,diffDays);        

                                String orderAddons = orderDetailsInfo.getString("order_add_ons");
                                logger.info("orderAddons: " + orderAddons);
                                List addonIds = new ArrayList();
                                if (orderAddons != null && orderAddons.length() > 0) {
                                	addonIds = Arrays.asList(orderAddons.split(","));
                                }
        
                                //ProductAvailVO[] availableDates = null;
                                String recipientCountry = orderDetailsInfo.getString("country");
                                if (zip == null) {
                                	zip = "";
                                }
                                logger.info(recipientCountry + " " + zip);
                                Boolean isProductAvailable = Boolean.FALSE;
                                if (recipientCountry != null && (recipientCountry.equalsIgnoreCase("US") ||
                                		recipientCountry.equalsIgnoreCase("CA"))) {
                                	ProductAvailVO paVO = PASServiceUtil.getProductAvailability(product, msgVO.getDeliveryDate(), zip, recipientCountry, 
                                			addonIds, sourceCode, false, true);
                        			logger.info("available: " + paVO.isIsAvailable() + " " + paVO.isIsAddOnAvailable() +
                        					" " + paVO.isIsMorningDeliveryAvailable());
                                	if (paVO != null && paVO.isIsAvailable() != null && paVO.isIsAvailable() && 
                                			paVO.isIsAddOnAvailable() != null && paVO.isIsAddOnAvailable()) {
                                		isProductAvailable = Boolean.TRUE;
                                	}
								try {
									if (!isProductAvailable) {
										logger.info("product is not available, checking for future/other date availability.");
										Date nextAvailDate = PASServiceUtil.getNextAvailableDeliveryDate(product, addonIds, zip, recipientCountry, sourceCode);
										if (nextAvailDate == null) {
											logger.info("There are no delivery dates available for this product. ftdAllowed = N");
										} else {
											isProductAvailable = Boolean.TRUE;
										}
									}
								} catch (Exception e) {
									logger.error("Unable to get the next available date for the product. ftdAllowed = N, " + e); 
								}
                                } else {
                                	isProductAvailable = PASServiceUtil.isInternationalProductAvailable(product, msgVO.getDeliveryDate(), recipientCountry); 
                                	try {
	                                	if (!isProductAvailable) {
	                                		logger.info("product is not available for international, checking for future/other date availability");
	                                        ProductAvailVO[] dates = PASServiceUtil.getInternationalProductAvailableDates(product, recipientCountry, 0);
	                                        if (dates == null || dates.length <= 0) {
	                                        	logger.info("There are no international delivery dates available for this product. ftdAllowed = N");
	                                        } else {
	                                        	isProductAvailable = Boolean.TRUE;
	                                        }
	                                    }
                                	} catch (Exception e) {
    									logger.error("Unable to get the next international available date for the product. ftdAllowed = N, " + e); 
    								}
                                }

                                if(isProductAvailable)
                                {
                                    ftdAllowed = "Y";
                                }
                                logger.info("ftdAllowed: " + ftdAllowed);
                                
                                String personalizationTemplateId = orderDetailsInfo.getString("personalization_template_id");
                                
                                if( personalizationTemplateId != null && 
                                   !personalizationTemplateId.equals(StringUtils.EMPTY) &&
                                   !personalizationTemplateId.equalsIgnoreCase("NONE")) {
                                    
                                    isPersonalized = "Y";
                                }
                                if (isOrderInTransitBoolean){
                                    isOrderInTransit = "Y";
                                }else{
                                    isOrderInTransit = "N";                                    
                                }
                            } else {
                            	ftdAllowed = "Y";
                            }

                            parameters.put("ftd_allowed", ftdAllowed);
                            parameters.put("personalized", isPersonalized);
                            parameters.put("in_transit", isOrderInTransit);
                            parameters.put("source_code", sourceCode);
                            parameters.put("msgExternalOrderNumber", msgExternalOrderNumber);
                            parameters.put("msgOrderGuid", msgOrderGuid);
                            parameters.put("msgCustomerId", msgCustomerId);

                            //for GEN message only.
                            if(forwardLocation.equalsIgnoreCase("GEN"))
                            {
                                String queueId=request.getParameter("queue_message_id");
                                String taggedCsrId=request.getParameter("tagged_csr_id");
                                String errorMsg=(String)request.getAttribute("queue_delete_security_error");
                                String removeQdeleteFlag=(String)request.getAttribute("remove_queue_flag");
                                parameters.put("queue_message_id", queueId);
                                parameters.put("message_id", msgID);
                                parameters.put("tagged_csr_id", taggedCsrId);
                                if(StringUtils.isNotBlank(errorMsg))
                                {
                                   parameters.put("queue_delete_security_error", errorMsg);
                                }else
                                {
                                  parameters.put("queue_delete_security_error", "");
                                }
                                
                                if(StringUtils.isNotBlank(removeQdeleteFlag))
                                {
                                   parameters.put("remove_queue_flag", removeQdeleteFlag);
                                }else
                                {
                                  parameters.put("remove_queue_flag", "Y");
                                }
                                
                            }
                            
                            Document XMLitems = (Document)messages;
                            StringWriter sw = new StringWriter();
                            DOMUtil.print((Document) XMLitems,new PrintWriter(sw));
                            logger.debug("messages:\n" + sw.toString());

                            doForward(forwardLocation,messages,mapping,request,response,parameters);
                           
                       /* }
                        else{
                            throw new Exception("Message Document not found!");      
                        }
*/                    }
                   /* else
                    {
                        throw new Exception("Message not found!");   
                    }*/
                }    
                else
                {
                    /* invalid action type passed, throw an error */
                    String errorMsg = actionType + MessagingConstants.INVALID_ACTION_TYPE_MSG + 
                        MessagingConstants.VIEW_MESSAGE;
                    throw new ServletException(errorMsg);    
                }
        }catch(SAXException e){
            logger.error(e);
            result = FAILURE;
        }catch(ServletException e){
            logger.error(e);
            result = FAILURE;
        }catch(IOException e){
            logger.error(e);
            result = FAILURE;
        }catch(TransformerException e){
            logger.error(e);
            result = FAILURE;
        }catch(ParserConfigurationException e){
            logger.error(e);
            result = FAILURE;
        }catch(SQLException e){
            logger.error(e);
            result = FAILURE;
        }catch(Exception e){
            logger.error(e);
            result = FAILURE;
      }finally{
            try
            {
                /* close the database connection */
                if(dbConnection!=null){
                    dbConnection.close();
                } 
            }catch(Exception e){
                throw new ServletException(e);
            }
            
            logger.info("Exiting Action View Message");
        }
        
        return mapping.findForward(result);
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
	private void setMessageIds(MessageVO msgVO, HashMap parameters)
    {
    	if(msgVO.getOrderDetailId()!=null)
      {
        parameters.put(MessagingConstants.REQUEST_ORDER_DETAIL_ID, msgVO.getOrderDetailId());
      }
      
    	parameters.put(MessagingConstants.MESSAGE_TYPE, msgVO.getType());
      if(msgVO.getMessageOrderId()!=null)
      {
        parameters.put(MessagingConstants.MESSAGE_ORDER_NUMBER, msgVO.getMessageOrderId());
      }
    	
    	parameters.put(MessagingConstants.MESSAGE_DETAIL_TYPE, msgVO.getDetailType());
      
      if(msgVO.getDetailType().indexOf("FTD")!=-1){
        parameters.put(MessagingConstants.FTD_MESSAGE_STATUS, msgVO.getMessageStatus());
        logger.debug("FTD message status="+msgVO.getMessageStatus());
      }
    }

  public void setAdditionalInfo(HttpServletRequest request,
            ActionMapping actionMapping, Document doc) {
               
        String pageTitle = this.getPageTitle(request);
        MessagingDOMUtil.addElementToXML(MessagingConstants.PAGE_TITLE, pageTitle, doc);
        
    }
    
    
    

    /**
     *  Based on the message detail type, return the forward name that maps 
     *  the action forward name configured in struts-config.xml.
     * @param messageVO - MessageVO
     * @return String
     * @throws Exception
     * @todo - code and determine exceptions
     */
     public String findForward(MessageVO messageVO, ActionMapping mapping)
     {
       logger.debug("message detail: ["+messageVO.getDetailType()+"]");
       String forward=messageVO.getDetailType();
       String subType=messageVO.getSubType();
       if(forward!=null)
       {
         if(subType!=null&&subType.indexOf(MessagingConstants.PRICE_SUB_TYPE)!=-1)
         {
            forward +=MessagingConstants.PRICE_SUB_TYPE;
         }
       }
       logger.debug("forward: ["+forward.toUpperCase()+"]");
       return forward.trim().toUpperCase();
       
     }
     
     
     

    /**
     *  Call toXML method of MessageVO, return a Document object that 
     *  contains message data.
     * @param messageVO - MessageVO
     * @return Document
     * @throws Exception????
     * @todo - code and determine exceptions
     */
    public Document createDocument(MessageVO messageVO)
        throws IOException, SAXException, ParserConfigurationException
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering createDocument");
            logger.debug("messageVO: " + messageVO);
        }
        
        Document message = null;
        try
        {
            String xmlMessage = messageVO.toXML();
            message = DOMUtil.getDocument(xmlMessage);

        }finally{
           
            if(logger.isDebugEnabled()){
                logger.debug("Exiting createDocument");
            }
        }
        return message;
    }

    /** 
     * @param request
     * @return
     * @see com.ftd.messaging.action.PrepareMessageAction#getPageTitle(javax.servlet.http.HttpServletRequest)
     */

    public String getPageTitle(HttpServletRequest request) {
        
        return this.getMessageFromResource(MessagingConstants.VIEW_MESSAGE_PAGE_TITLE, request);
    }
}