package com.ftd.messaging.action;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.messaging.bo.RetrieveMessageBO;
import com.ftd.messaging.constants.MessagingConstants;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;

/**
 * This class is used to load the Communication Screen.  It contains the main
 * flow of logic needed for security processing and XSL stylesheet
 * determination. It also makes calls to the business object to retrieve 
 * order/message/florist information.  The transform method in the 
 * com.ftd.osp.utilities.xml.TraxUtil class will be used to render the page.   
 * @author Charles Fox
 * @todo check comments, error handling, and message handling
 */
public class DisplayCommunicationAction extends BaseAction
{
   
    private Logger logger = 
        new Logger("com.ftd.messaging.action.DisplayCommunicationAction");
        
    private static String SUCCESS = "success";
    private static String FAILURE = MessagingConstants.ERROR;
        
    public DisplayCommunicationAction()
    {
    }
    
    /**
     * 1.If action passed on request is null or equals communication,  
     * call the retrieveMessageInfo method in the Retrieve Messge Business 
     * Object to obtain message related data.
     * 2.If action passed on request equals florist_dashboard, call the 
     * retrieveFloristDashboard method in the Retrieve Messge Business 
     * Object to obtain florist dashboard related data
     * 3.Call doForward method of its parent class to:
     *      Obtain security setting
     *      Get XSL file 
     *      Transform XML and XSL to render UI page.
     * @param n/a
     * @return n/a
     * @throws Exception????
     * @todo - code and determine exceptions
     */   
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException{
      
      
     
        logger.info("Entering Action Display Communication");
      
        
        Connection dbConnection = null;
        String actionType = null;
        Document items = null;
        String result = null;
        try{
        
            // get action type
            if(StringUtils.isNotBlank(request.getParameter(COMConstants.ACTION_TYPE)))
            {
              actionType = request.getParameter(COMConstants.ACTION_TYPE).trim();
            }
            else
            {
                //assume display communication screen action
                actionType = MessagingConstants.ACTION_DISPLAY_COMMUNICATION;
            }
            HashMap parameters = this.getRequestDataMap(request);
            
            if(actionType.equals(MessagingConstants.ACTION_DISPLAY_COMMUNICATION)){
                dbConnection = createDatabaseConnection();
                RetrieveMessageBO msgBO = new RetrieveMessageBO(dbConnection);
                items = msgBO.retrieveMessageInfo(request,parameters);
            }else{
                if(actionType.equals(MessagingConstants.ACTION_DISPLAY_FLORIST_DASHBOARD)){
                    dbConnection = createDatabaseConnection();
                    RetrieveMessageBO msgBO = new RetrieveMessageBO(dbConnection);
                    items = DOMUtil.getDocument();
                    /* Retrieve order detail id from HTTP request */
                    String orderDetailID = request.getParameter(
                        MessagingConstants.REQUEST_ORDER_DETAIL_ID);
                    items = msgBO.retrieveFloristDashBoard(request,parameters);
                }
                else
                {
                    /* invalid action type passed, throw an error */
                    String errorMsg = actionType + MessagingConstants.INVALID_ACTION_TYPE_MSG + 
                        MessagingConstants.DISPLAY_COMMUNICATION;
                    throw new ServletException(errorMsg);    
                }
            }
            if(request.getParameter("read_only_flag")!=null&&request.getParameter("read_only_flag").trim().equalsIgnoreCase("Y"))
            {
              parameters.put("read_only_flag", "Y");
              result="readonly";
            }else
            {
              result=SUCCESS;
            }
            parameters.put("master_order_number", request.getParameter("master_order_number"));
            this.doForward(result, items, mapping, request, response, parameters); 
           return null;
        }catch(SAXException e){
            logger.error(e);
            result = FAILURE;
        }catch(IOException e){
            logger.error(e);
            result = FAILURE;
        }catch(ServletException e){
            logger.error(e);
            result = FAILURE;
        }catch(TransformerException e){
            logger.error(e);
            result = FAILURE;
        }catch(ParserConfigurationException e){
            logger.error(e);
            result = FAILURE;
        }catch(SQLException e){
            logger.error(e);
            result = FAILURE;
        }catch(Exception e){
            logger.error(e);
            result = FAILURE;
      }finally{
            try
            {
                /* close the database connection */
                if(dbConnection!=null){
                    dbConnection.close();
                } 
            }catch(Exception e){
                throw new ServletException(e);
            }
            
            logger.info("Exiting Action Display Communication");
        }
        return mapping.findForward(result);
    }

    
}