package com.ftd.messaging.action;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.messaging.bo.PrepareMessageBO;
import com.ftd.messaging.cache.HotKeyMessageHandler;
import com.ftd.messaging.constants.MessagingConstants;
import com.ftd.messaging.util.MessagingDOMUtil;
import com.ftd.messaging.vo.MessageOrderStatusVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.handlers.ActiveOccasionHandler;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * This class is an abstract class that contains common behaviors shared by its
 * subclasses.
 *
 * @author Charles Fox
 * @todo check comments, error handling, and message handling
 */
public abstract class PrepareMessageAction extends BaseAction {

    private Logger logger = new Logger(
            "com.ftd.messaging.action.PrepareMessageAction");

    /**
     * Retrieve action from request and process
     *
     * @param mapping -
     *            ActionMapping
     * @param form -
     *            ActionForm
     * @param request -
     *            HttpServletRequest
     * @param response -
     *            HttpServletResponse
     * @return ActionForward
     * @throws Exception????
     * @todo - code and determine exceptions
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        /*
         * Retrieve action from request, if action is null or action does not
         * equal �prepare_message� Call the createXmlDoc method of
         * PrepareMessageBO to create an empty Document object. Call
         * getMessageOrderStatusVO method to retrieve MessageOrderStatusVO from
         * HTTP request. Call validateStatus method to determine whether or not
         * the status is valid. If status is not valid, call setErrorMessage
         * method to add an error message node to the empty Document. If status
         * is valid, call process method to process the request and add forward
         * action nodes to the empty Document object. if action is not null and
         * action equals �prepare_message� Call prepareMessage method that
         * returns a Document object that contains information associate with
         * the order and outgoing message type Call setAdditionalInfo method to
         * add request parameter map, message types and page title to the
         * Document object returned by prepareMessageBO. Call setSecurity method
         * to add security node to the Document object. Call getXSL method to
         * get XSL file Call transform method to utilize TraxUtil to transform
         * XML and XSL to render ui page.
         *
         *
         */
        String action = request.getParameter(COMConstants.ACTION_TYPE);
        PrepareMessageBO prepareMessageBO = new PrepareMessageBO();
        String forward = null;
        if (StringUtils.isNotBlank(action)
                && !action
                        .equalsIgnoreCase(MessagingConstants.ACTION_PREPARE_MESSAGE)) {

            MessageOrderStatusVO statusVO = this
                    .getMessageOrderStatusVO(request);
            logger.info("action="+action);

            Document rootDoc;
            try {
                rootDoc = MessagingDOMUtil.createEmptyDocument();
                boolean valid = this.validateStatus(request, mapping, rootDoc,
                        statusVO);
                if (valid) {
                    this.process(request, mapping, rootDoc, statusVO,
                            prepareMessageBO);
                }
                HashMap parameters = this.getRequestDataMap(request);
                
                parameters.put(MessagingConstants.MESSAGE_ORDER_TYPE, (statusVO.getOrderType()!=null)?statusVO.getOrderType().trim():"");// statusVO.getOrderType());
                parameters.put(MessagingConstants.MESSAGE_ORDER_STATUS, (statusVO.getOrderStatus()!=null)?statusVO.getOrderStatus().trim():"");
                logger.debug("prepare message order type: "+MessagingConstants.MESSAGE_ORDER_TYPE+"\t ="+statusVO.getOrderType());
                logger.debug("prepare message order status: "+MessagingConstants.MESSAGE_ORDER_STATUS+"\t ="+statusVO.getOrderStatus());
                parameters.put(MessagingConstants.FTD_MESSAGE_CANCELLED, (statusVO.isCancelSent()||statusVO.isFloristRejectOrder())?"true":"false");
                logger.debug("prepare message cancelled: "+statusVO.isCancelSent()+"\t rejected: "+statusVO.isFloristRejectOrder());
               
                this.setRequestDataMap(request, parameters);


                this.doForward("messaging_forward_iframe",
                        rootDoc, mapping, request, response,parameters);
            } catch (ParserConfigurationException e) {
                logger.error(e);
                forward = MessagingConstants.ERROR;

            } catch (TransformerException te) {
                logger.error(te);
                forward = MessagingConstants.ERROR;
            } catch (IOException ioe) {
                logger.error(ioe);
                forward = MessagingConstants.ERROR;
            } catch (Throwable t) {
                logger.error(t);
                forward = MessagingConstants.ERROR;
            }

        } else {
            Connection conn=null;
             logger.debug("prepare message ");
            try {
                conn=this.createDatabaseConnection();
                prepareMessageBO.setConnection(conn);
                      
                Document doc = this.prepareMessage(request, prepareMessageBO);
                addHotKeyMessages(doc);
                this.setAdditionalInfo(request, mapping, doc);
                HashMap parameters = this.getRequestDataMap(request);
              //  this.setRequestDataMap(request, parameters);
                String forwardURI=getForward(request);
                logger.debug("forwardURI="+forwardURI);
                
                this.doForward(forwardURI, doc, mapping,
                        request, response,parameters);

            } catch (SAXException se) {
                logger.error(se);
                forward = MessagingConstants.ERROR;
            } catch (IOException ioe) {
                logger.error(ioe);
                forward = MessagingConstants.ERROR;
            } catch (SQLException sqle) {
                logger.error(sqle);
                forward = MessagingConstants.ERROR;
            } catch (ParserConfigurationException pe) {
                logger.error(pe);
                forward = MessagingConstants.ERROR;
            } catch (TransformerException te) {
                logger.error(te);
                forward = MessagingConstants.ERROR;
            } catch (Throwable t) {
                logger.error(t);
                forward = MessagingConstants.ERROR;
            }finally
            {
              if(conn!=null)
              {
                try
                {
                  conn.close();
                }
                catch (Exception e)
                {
                   logger.error(e);
                }
              }
            }

        }

        if (forward != null) {
            return mapping.findForward(forward);
        } else {
            return null;
        }

    }


    public String getForward(HttpServletRequest request){

        return MessagingConstants.SUCCESS;

    }

    /**
     * Retrieve MessageOrderStatusVO from HTTP request that passed by
     * MessageForwardAction.
     *
     * @param request -
     *            HttpServletRequest
     * @return MessageOrderStatusVO
     * @throws Exception????
     * @todo - code and determine exceptions
     */
    public MessageOrderStatusVO getMessageOrderStatusVO(
            HttpServletRequest request) {

        return (MessageOrderStatusVO) request
                .getAttribute(MessagingConstants.ATTR_MESSAGE_STATUS_VO);
    }

    /**
     * � Utilize DomUtil to add request parameter map to the Document object. �
     * Call setMessageTypes to add message type related information to the
     * �addition_info� node of Document object. � Call setPageTitle method to
     * add page title node to the �addition-info� node of the Document object.
     *
     * @param request -
     *            HttpServletRequest
     * @param actionMapping -
     *            ActionMapping
     * @param doc -
     *            Document
     * @return n/a
     * @throws ParserConfigurationException 
     * @throws SAXException 
     * @throws IOException 
     * @throws Exception????
     * @todo - code and determine exceptions
     */
    public void setAdditionalInfo(HttpServletRequest request,
            ActionMapping actionMapping, Document doc) throws IOException, SAXException, ParserConfigurationException, Exception {

        String pageTitle = this.getPageTitle(request);
        MessagingDOMUtil.addElementToXML(MessagingConstants.PAGE_TITLE, pageTitle, doc);
        String initialReason = this.getInitialReasonText(request);
        String systemReason=request.getParameter("reason");//system generated comments by Modify Order.
       
        String newPrice=request.getParameter("new_mercury_order_amount");//system generated price by Modify Order.
        String newProduct=request.getParameter("new_product_id");//new florist id selected by Modify Order.
        String price_increase_threshold = ConfigurationUtil.getInstance().getFrpGlobalParm(COMConstants.COM_CONTEXT, COMConstants.PRICE_INCREASE_AUTH_THRESHOLD); 
        
        if (initialReason != null) {

            MessagingDOMUtil.addElementToXML(MessagingConstants.INIT_REASON_TEXT, initialReason,
                    doc);

        }
        
        if(StringUtils.isNotBlank(systemReason))
        {
          MessagingDOMUtil.addElementToXML("message_system_reason", systemReason,
                    doc);
        }
        
        
        if(StringUtils.isNotBlank(newPrice))
        {
          MessagingDOMUtil.addElementToXML("new_mercury_order_amount", newPrice,
                    doc);
        }
        
               
        if(StringUtils.isNotBlank(newProduct))
        {
          MessagingDOMUtil.addElementToXML("new_product_id", newProduct,
                    doc);
        }
        
        if(StringUtils.isNotBlank(price_increase_threshold))
        {
          MessagingDOMUtil.addElementToXML("PRICE_INCREASE_AUTH_THRESHOLD", price_increase_threshold,
                    doc);
        }
        
        this.setMessageTypes(request, actionMapping, doc);

    }

    /**
     * � Look up message detail type from action mapping parameter. � Add
     * message_detail_type to the Document.
     *
     * @param request -
     *            HttpServletRequest
     * @param actionMapping -
     *            ActionMapping
     * @param doc -
     *            Document
     * @return n/a
     * @throws Exception????
     * @todo - code and determine exceptions
     */
    public void setMessageTypes(HttpServletRequest request,
            ActionMapping actionMapping, Document doc) {
        /*
         * � Look up message detail type from action mapping parameter. String
         * messageDetailType=actionMapping.getParameter(); � Add
         * message_detail_type to the Document. <addition-info> �
         * <name>message_detail_type </name> <value>ASK </value> �
         * </addition_info>
         */
         /*
        String messageType = request.getParameter(MessagingConstants.MESSAGE_TYPE);
        String messageDetailType = actionMapping.getParameter();
        HashMap parameters=this.getRequestDataMap(request);
        logger.debug("set message type parameters: "+parameters);
        parameters.put(MessagingConstants.MESSAGE_TYPE, messageType);
        parameters.put(MessagingConstants.MESSAGE_DETAIL_TYPE, messageDetailType);
        this.setRequestDataMap(request, parameters);
        */
        HashMap parameters=this.getRequestDataMap(request);
        String messageType = (String) parameters.get(MessagingConstants.MESSAGE_TYPE);
        String messageDetailType = actionMapping.getParameter();//(String) parameters.get(MessagingConstants.MESSAGE_DETAIL_TYPE);
        parameters.put(MessagingConstants.MESSAGE_DETAIL_TYPE, messageDetailType );
        this.setRequestDataMap(request, parameters);
        MessagingDOMUtil.addElementToXML(MessagingConstants.MESSAGE_TYPE, messageType,
                    doc);
        MessagingDOMUtil.addElementToXML(MessagingConstants.MESSAGE_DETAIL_TYPE, messageDetailType,
                    doc);

    }

    /**
     * � Find forwarding URI from ActionMapping object. � Add a forward URI node
     * to the DOM Document object � Add an �action� element to the Document with
     * value �prepare_message�.
     *
     * @param request -
     *            HttpServletRequest
     * @param actionMapping -
     *            ActionMapping
     * @param doc -
     *            Document
     * @param messageOrderStatus -
     *            MessageOrderStatusVO
     * @return n/a
     * @throws Exception????
     * @todo - code and determine exceptions
     */
    public void process(HttpServletRequest request,
            ActionMapping actionMapping, Document doc,
            MessageOrderStatusVO messageOrderStatus,
            PrepareMessageBO prepareMessageBO) throws Exception{
        String forwardURI = actionMapping.findForward(
                MessagingConstants.ACTION_PREPARE_MESSAGE).getPath();
       logger.debug("forwardURI: "+forwardURI);
       MessagingDOMUtil.addElementToXML(MessagingConstants.FORWARDING_ACTION, forwardURI, doc);
       MessagingDOMUtil.addElementToXML(COMConstants.ACTION,
                MessagingConstants.ACTION_PREPARE_MESSAGE, doc);

    }

    /**
     * � Call prepareMessage method of PrepareMessageBO to prepare a Document
     * object for Delivery Confirmation page. � Initialize �reason� field as
     * �PLEASE CONFIRM DELIVERY UPON COMPLETION WITH DATE, TIME & SIGNATURE IF
     * AVAILABLE.�
     *
     * @param request -
     *            HttpServletRequest
     * @return Document
     * @throws ParserConfigurationException
     * @throws SQLException
     * @throws IOException
     * @throws SAXException
     * @throws Exception????
     * @todo - code and determine exceptions
     */
    public Document prepareMessageForCurrentFlorist(HttpServletRequest request,
            PrepareMessageBO prepareMessageBO) throws SAXException,
            IOException, SQLException, ParserConfigurationException,Exception {
        String messageOrderNumber = request
                .getParameter(MessagingConstants.MESSAGE_ORDER_NUMBER);
        String messageType = request.getParameter(MessagingConstants.MESSAGE_TYPE);
        return prepareMessageBO.retrieveMessageForCurrentFlorist(
                messageOrderNumber, messageType, this.getRequestDataMap(request));
    }


    public Document prepareAdjustmentMessageForCurrentFlorist(HttpServletRequest request,
            PrepareMessageBO prepareMessageBO) throws SAXException,
            IOException, SQLException, ParserConfigurationException, Exception {
        String messageOrderNumber = request
                .getParameter(MessagingConstants.MESSAGE_ORDER_NUMBER);
        String messageType = request.getParameter(MessagingConstants.MESSAGE_TYPE);
        boolean systemGenerated=new Boolean(request.getParameter("system")).booleanValue();
        boolean getOldPrice=new Boolean(request.getParameter("old_price")).booleanValue();
        return prepareMessageBO.retrieveAdjustmentMessageForCurrentFlorist(
                messageOrderNumber, messageType, this.getRequestDataMap(request), systemGenerated, getOldPrice);
    }


    public void addHotKeyMessages(Document doc) throws ParserConfigurationException
    {
        HotKeyMessageHandler hotKeyMessageHandler = (HotKeyMessageHandler)
            CacheManager.getInstance().getHandler(
            MessagingConstants.HotKeyMessageHandler);
        hotKeyMessageHandler.toXML(doc);
    }
    /**
     *
     * @param request
     * @param prepareMessageBO
     * @return
     * @throws SAXException
     * @throws IOException
     * @throws SQLException
     * @throws ParserConfigurationException
     * Document
     */
    public Document prepareMessageForCurrentOrder(HttpServletRequest request,
            PrepareMessageBO prepareMessageBO, HashMap parameters) throws SAXException,
            IOException, SQLException, ParserConfigurationException, ParseException, Exception {
        String orderDetailId = request
                .getParameter(MessagingConstants.REQUEST_ORDER_DETAIL_ID);
        
        String messageType = request.getParameter(MessagingConstants.MESSAGE_TYPE);
        if(StringUtils.isNotBlank(request.getParameter(MessagingConstants.NEW_MESSAGE_TYPE)))
        {
          messageType=request.getParameter(MessagingConstants.NEW_MESSAGE_TYPE);
        }
        return prepareMessageBO.retrieveMessageForCurrentOrder(
                orderDetailId, messageType,parameters);
    }

    /**
     *Call prepareMessageForCurrentCallout method of PrepareMessageBO to return 
     * a Document object which contains Message related data of the
     * current florist on the message. 
     * @param request
     * @param prepareMessageBO
     * @return Document
     * @throws SAXException
     * @throws IOException
     * @throws SQLException
     * @throws ParserConfigurationException
     * Document
     */
    public Document prepareMessageForCallout(HttpServletRequest request, 
        PrepareMessageBO prepareMessageBO, HashMap parameters) 
        throws SAXException, IOException, SQLException, ParserConfigurationException, 
        ParseException, Exception
        {
            

            String orderDetailId = request
                    .getParameter(MessagingConstants.REQUEST_ORDER_DETAIL_ID);
            String messageType = request.getParameter(MessagingConstants.MESSAGE_TYPE);
            String ftdMessageID = request.getParameter(MessagingConstants.REQUEST_FTD_MESSAGE_ID);
            String securityToken = (String) parameters.get(MessagingConstants.REQUEST_SECURITY_TOKEN);
            if(securityToken == null)
            {
                /* attempt to retrieve from request */
                securityToken = request.getParameter(MessagingConstants.REQUEST_SECURITY_TOKEN);
                /*set token */
                parameters.put(MessagingConstants.REQUEST_SECURITY_TOKEN,securityToken);
            }

            String context = (String) parameters.get(MessagingConstants.REQUEST_CONTEXT);
            if(context == null)
            {
                /* attempt to retrieve from request */
                context = request.getParameter(MessagingConstants.REQUEST_CONTEXT);
                /*set context */
                parameters.put(MessagingConstants.REQUEST_CONTEXT,context);
            }
            
            boolean compOrder = new Boolean((String) parameters.get(MessagingConstants.REQUEST_PARAMETER_COMP_ORDER)).booleanValue();
            Document callOut = null;
            
            if(compOrder==false)
            {
                callOut = prepareMessageBO.retrieveMessageForCallout(orderDetailId,messageType,securityToken,request);
               
            }
            else
            {   
                callOut = prepareMessageBO.createMessageForCallout(request,parameters);     
            }

            parameters.put(MessagingConstants.REQUEST_FTD_MESSAGE_ID, (ftdMessageID!=null)?ftdMessageID.trim():"");
            
            return callOut;

        }


    /**
     * This method should be overriden by its subclassed.
     * @param request
     * @param prepareMessageBO
     * @return
     * @throws SAXException
     * @throws IOException
     * @throws SQLException
     * @throws ParserConfigurationException
     * @see com.ftd.messaging.action.PrepareMessageAction#prepareMessage(javax.servlet.http.HttpServletRequest, com.ftd.messaging.bo.PrepareMessageBO)
     */

    public abstract Document prepareMessage(HttpServletRequest request,
            PrepareMessageBO prepareMessageBO) throws SAXException,
            IOException, SQLException, ParserConfigurationException,ParseException,
            Exception;


    /**
     * Abstract method. Should be overridden by its subclasses.
     *
     * @param request -
     *            HttpServletRequest
     * @param doc -
     *            Document
     * @return n/a
     * @throws Exception????
     * @todo - code and determine exceptions
     */
    public abstract String getPageTitle(HttpServletRequest request);

    public String getInitialReasonText(HttpServletRequest request) {

        return null;
    }

    /**
     * Abstract method. Return true is status is valid, otherwise return false.
     * Should be overridden by its subclasses.
     *
     * @param request -
     *            HttpServletRequest
     * @param actionMapping -
     *            ActionMapping
     * @param doc -
     *            Document
     * @param messageOrderStatus -
     *            MessageOrderStatusVO
     * @return boolean
     * @throws Exception????
     * @todo - code and determine exceptions
     */
    public abstract boolean validateStatus(HttpServletRequest request,
            ActionMapping mapping, Document document,
            MessageOrderStatusVO messageOrderStatus) throws Exception;

    /**
     * Get the active occasions from the occasions cache
     * 
     * @return Document
     * @throws IOException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws SQLException
     * @throws TransformerException
     * @throws Exception
     */
  	protected Document getActiveOccaions() throws IOException,
  			ParserConfigurationException, SAXException, SQLException,
  			TransformerException, Exception {
  		ActiveOccasionHandler occasionHandler = null;
  		occasionHandler = (ActiveOccasionHandler) CacheManager.getInstance().getHandler(
  				COMConstants.ACTIVE_OCCASIONS_HANDLER);

  		return occasionHandler.getOccasionDocument();
  	}

}