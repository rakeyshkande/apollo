/*
 * Created on Apr 20, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.ftd.messaging.action;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.filter.DataFilter;
import com.ftd.messaging.bo.PrepareMessageBO;
import com.ftd.messaging.bo.SendMessageBO;
import com.ftd.messaging.constants.MessagingConstants;
import com.ftd.messaging.dao.MessageDAO;
import com.ftd.messaging.util.MessageUtil;
import com.ftd.messaging.util.MessagingDOMUtil;
import com.ftd.messaging.vo.MessageOrderStatusVO;
import com.ftd.osp.utilities.AddOnUtility;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/**
 * @author achen
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class CancelOriginalSendNewFTDAction extends PrepareMessageAction {

     private Logger logger = new Logger(
            "com.ftd.messaging.action.CancelOriginalSendNewFTDAction");
            
     private String status                   = null; 
     private String vendorDeliveryAllowed    = null;  
     private String floristDeliveryAllowed   = null;  
     private boolean checkAddOnAvailability  = true;
     private String shippingSytem            = null;
     private String productType              = null;
    
     
     
    /** 
     * @param request
     * @return
     * @see com.ftd.messaging.action.PrepareMessageAction#getPageTitle(javax.servlet.http.HttpServletRequest)
     */

    public String getPageTitle(HttpServletRequest request) {
        
        return this.getMessageFromResource("cancel_original_send_new_title", request);
    }

    /** 
     * if a successful CAN has already been sent for the order or if a REJ has come in from the current florist, return false.
     * If there was never a successful FTD sent on the order they should receive the following error message �You cannot send an FTD on this order�. 
     * If the current date is past the delivery date the following error message will display �You cannot send a new FTD order past the delivery date�.
     * @param request
     * @param mapping
     * @param document
     * @param messageOrderStatus
     * @return
     * @see com.ftd.messaging.action.PrepareMessageAction#validateStatus(javax.servlet.http.HttpServletRequest, org.apache.struts.action.ActionMapping, org.w3c.dom.Document, com.ftd.messaging.vo.MessageOrderStatusVO)
     */

    public boolean validateStatus(HttpServletRequest request,
            ActionMapping mapping, Document document,
            MessageOrderStatusVO messageOrderStatus) throws Exception{
        
        boolean valid=true;
        String orderDetailId = request.getParameter(MessagingConstants.REQUEST_ORDER_DETAIL_ID);
        String orderType =  messageOrderStatus.getOrderType();
        
        String actionType = request.getParameter("action_type");
        Connection dbConnection = null;
        dbConnection = this.createDatabaseConnection();
        
        
        if(messageOrderStatus.isCancelSent()){
            MessagingDOMUtil.addElementToXML(MessagingConstants.ERROR_MESSAGE, this.getMessageFromResource(MessagingConstants.CANCELLED_ERROR, request), document);
            valid = false;
        }else if(messageOrderStatus.isFloristRejectOrder()){
            MessagingDOMUtil.addElementToXML(MessagingConstants.ERROR_MESSAGE, this.getMessageFromResource(MessagingConstants.REJECTED_ERROR, request), document);
            valid = false;
        }else if(!messageOrderStatus.isAttemptedFTD()){
            MessagingDOMUtil.addElementToXML(MessagingConstants.ERROR_MESSAGE, this.getMessageFromResource(MessagingConstants.NEVER_ATTEMPTED_FTD_ERROR, request), document);
            valid = false;
        }else if(messageOrderStatus.isATypeRefund()){
            MessagingDOMUtil.addElementToXML(MessagingConstants.ERROR_MESSAGE, this.getMessageFromResource(MessagingConstants.A_TYPE_REFUND_ERROR, request), document);
            valid = false;
        }else if (valid) {        
            try{              
              String productId = getOrderDetailsInfo(orderDetailId, dbConnection);
              getProductInfo(productId, dbConnection);
              
              
              //check if order has add ons associated with it.  If it does, check to see
              //if the add ons are still available
              AddOnUtility addOnUTIL = new AddOnUtility();
              boolean orderAddOnsAvailable = addOnUTIL.orderAddOnAvailable(orderDetailId, dbConnection);
              if( !orderAddOnsAvailable )
              {
                MessagingDOMUtil.addElementToXML(MessagingConstants.ERROR_MESSAGE, this.getMessageFromResource("add_on_not_available", request), document);
                valid = false;
              }    
                     
              if(messageOrderStatus.isVendor() && actionType != null && actionType.equalsIgnoreCase("cancel_original_send_new"))
              { 
            	  String venusid=request.getParameter(MessagingConstants.REQUEST_FTD_MESSAGE_ID);//request.getParameter("ftd_message_id");
            	  MessageDAO messageDAO = new MessageDAO(dbConnection);
            	  String state = messageDAO.getStateCode(venusid,MessagingConstants.VENUS_MESSAGE,dbConnection);
                  logger.info("venusid : "+venusid+" state_code ::"+state+" isVendor order::"+messageOrderStatus.isVendor()+" actionType:::"+actionType); String ftdMessageID = request.getParameter(MessagingConstants.REQUEST_FTD_MESSAGE_ID);
                  
                  MessageUtil messageUtil = new MessageUtil(dbConnection);
                  
                  if( this.status.equalsIgnoreCase("U") || this.vendorDeliveryAllowed.equalsIgnoreCase("N") )
                  {
                    MessagingDOMUtil.addElementToXML(MessagingConstants.ERROR_MESSAGE, this.getMessageFromResource("vendor_product_not_avail", request), document);
                    valid = false;
                  }                  
                  
                  if(messageUtil.isZJOrderInTransit(ftdMessageID))
                  {
                    MessagingDOMUtil.addElementToXML(MessagingConstants.ERROR_MESSAGE, this.getMessageFromResource(MessagingConstants.ZJ_ORDER_IN_TRANSIT, request), document);
                    valid = false;
                  }
                  if((COMConstants.CONS_PRODUCT_TYPE_FRECUT.equalsIgnoreCase(this.productType) && ((COMConstants.CONS_STATE_CODE_ALASKA).equalsIgnoreCase(state)||(COMConstants.CONS_STATE_CODE_HAWAII).equalsIgnoreCase(state)))
            			  ||(MessagingConstants.SHIPPING_SYSTEM_FTD_WEST.equalsIgnoreCase(this.shippingSytem) && ((COMConstants.CONS_STATE_CODE_PUERTO_RICO).equalsIgnoreCase(state)||(COMConstants.CONS_STATE_CODE_VIRGIN_ISLANDS).equalsIgnoreCase(state)))){
                	  MessagingDOMUtil.addElementToXML(MessagingConstants.ERROR_MESSAGE, this.getMessageFromResource("vendor_product_not_avail", request), document);
                      valid = false;
                      logger.info("FRECUTS will not allow to AK/HI or WEST dropships not delivered to PR/VI");
                  }
                  
              }if(messageOrderStatus.isFlorist() && actionType != null && actionType.equalsIgnoreCase("cancel_original_send_new"))
              {
                    if( this.status.equalsIgnoreCase("U") || this.floristDeliveryAllowed.equalsIgnoreCase("N") )
                    {
                      MessagingDOMUtil.addElementToXML(MessagingConstants.ERROR_MESSAGE, this.getMessageFromResource("florist_product_not_avail", request), document);
                      valid = false;
                    }
              } 
            }catch (SQLException sqle) {
              logger.error(sqle);
              throw new Exception(sqle);
            }
            finally
            {
              if(dbConnection!=null)
              {
                try
                {
                  dbConnection.close();
                }
                catch (Exception e)
                {
                   logger.error(e);
                }
              }
            }
        }
        return valid;
    }
    
    
    
    
    /**
     * First, send CAN message and take the user to New FTD Message Screen 
     * @param request
     * @param actionMapping
     * @param doc
     * @param messageOrderStatus
     * @param prepareMessageBO
     * @see com.ftd.messaging.action.PrepareMessageAction#process(javax.servlet.http.HttpServletRequest, org.apache.struts.action.ActionMapping, org.w3c.dom.Document, com.ftd.messaging.vo.MessageOrderStatusVO, com.ftd.messaging.bo.PrepareMessageBO)
     */
    public void process(HttpServletRequest request,
            ActionMapping actionMapping, Document doc,
            MessageOrderStatusVO messageOrderStatus,
            PrepareMessageBO prepareMessageBO) throws Exception{
        
        Connection con=null;
        SendMessageBO sendMessageBO=new SendMessageBO();
        HashMap requestDataMap=this.getRequestDataMap(request);
        String forward="";
        try{ 
               con = this.createDatabaseConnection();
               sendMessageBO.setDbConnection(con);
               forward=sendMessageBO.sendCancelOriginalMessage(messageOrderStatus, request,requestDataMap);
               this.setRequestDataMap(request, requestDataMap);
               
                // Set DataFilter param indicating order was cancelled (for later Call Disposition check if order cancelled and resent)
                String orderDetailId = request.getParameter("order_detail_id");
                DataFilter.updateParameter(request, COMConstants.CDISP_CANCELLED_ORDER, orderDetailId);
        } catch (ParseException e) {
            logger.error(e);
            forward = MessagingConstants.ERROR;
        } catch (Exception e) {
            logger.error(e);
            forward = MessagingConstants.ERROR;
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (Exception e) {
                    logger.error("can not close connection", e);
                }
            }
        }
        logger.debug("forward="+forward);
        String forwardURI=actionMapping.findForward(forward).getPath();
        logger.debug("forwardURI="+forwardURI);
             
        MessagingDOMUtil.addElementToXML(MessagingConstants.FORWARDING_ACTION, forwardURI, doc);
         
        MessagingDOMUtil.addElementToXML(COMConstants.ACTION,
                      MessagingConstants.ACTION_PREPARE_MESSAGE, doc);
    }
    
    
    public  Document prepareMessage(HttpServletRequest request,
            PrepareMessageBO prepareMessageBO) throws SAXException,
            IOException, SQLException, ParserConfigurationException {
        
        return null;
    }
    
    /**
     * Initialize �reason� field as �PLEASE CANCEL ORDER�
     */
    public String getInitialReasonText(HttpServletRequest request){
    	
    	return this.getMessageFromResource("cancel_init_reason", request);
    }
    
 /**
	* Obtain the product id for this order
	*/
 private String getOrderDetailsInfo(String orderDetailid, Connection dbConnection) throws Exception
  {
    String productId = null;
    
    MessageDAO messageDAO = new MessageDAO(dbConnection);
    
    CachedResultSet crs =  messageDAO.getOrderDetailsInfo(orderDetailid);
    
    if(crs.next())
    {
      productId = (String) crs.getString("product_id");
    }
    return productId;
  }
  
 /**
	* Obtain the product status and the ship method carrier flag
	*/
  private void getProductInfo(String productId, Connection dbConnection) throws Exception
  {
    MessageDAO messageDAO = new MessageDAO(dbConnection);
    
    CachedResultSet crs =  messageDAO.getProductInfo(productId);
    
    if(crs.next())
    {
      this.status = (String) crs.getString("status");
      this.vendorDeliveryAllowed = (String) crs.getString("shipMethodCarrier");
      this.floristDeliveryAllowed = (String) crs.getString("shipMethodFlorist");
      this.shippingSytem =(String)crs.getString("shippingSystem");
      this.productType =(String)crs.getString("productType");
    }
  }
    
}
