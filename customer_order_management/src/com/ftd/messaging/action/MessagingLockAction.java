package com.ftd.messaging.action;


import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.dao.LockDAO;
import com.ftd.messaging.constants.MessagingConstants;
import com.ftd.messaging.dao.MessageDAO;
import com.ftd.messaging.util.MessagingDOMUtil;
import com.ftd.messaging.util.MessagingHelper;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.security.SecurityManager;


public class MessagingLockAction extends BaseAction 
{
   private Logger logger = 
        new Logger(this.getClass().getName());
  
  public MessagingLockAction()
  {
  }
  
  
  
  
   public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException{
    	
    	
    	Connection dbConnection = null;
    	String action=mapping.getParameter();
      boolean debug=logger.isDebugEnabled();
        logger.info("Start MessagingLockAction Action");
    	
    	
			try {
				dbConnection=this.createDatabaseConnection();
        HashMap parameters=this.getRequestDataMap(request);
        String actionType=request.getParameter("action_type");
        
        LockDAO lockDAO=new LockDAO(dbConnection);
         Document rootDoc = DOMUtil.getDocument();   
        //try to obtain lock first.
        String orderDetailId=request.getParameter(MessagingConstants.REQUEST_ORDER_DETAIL_ID);
        if (request.getParameter(MessagingConstants.REQUEST_FLORIST_SELECTION_LOG_ID) != null)
        {
        	String floristSelectionLogId = request.getParameter(MessagingConstants.REQUEST_FLORIST_SELECTION_LOG_ID).toString();
        	logger.info("floristSelectionLogId: " + floristSelectionLogId);
        	MessagingDOMUtil.addElementToXML(MessagingConstants.REQUEST_FLORIST_SELECTION_LOG_ID, floristSelectionLogId, rootDoc);
        }
        if (request.getParameter("zip_city_flag") != null)
        {
        	String zipCityFlag = request.getParameter("zip_city_flag".toString());
        	MessagingDOMUtil.addElementToXML("zip_city_flag", zipCityFlag, rootDoc);
        }
        String securityToken = (String) parameters.get(MessagingConstants.REQUEST_SECURITY_TOKEN);
        String csrId=lookupCurrentUserId(securityToken);
        String applicationName=request.getParameter("lock_app");
        if(StringUtils.isBlank(action)){
          Document lockDoc=lockDAO.retrieveLockXML(securityToken, csrId, orderDetailId, "COMMUNICATION");
        
        // check if lock obtained is N. Then, forward without saving comment.
          if(DOMUtil.getNodeValue(lockDoc, "OUT_LOCK_OBTAINED").equalsIgnoreCase("N"))
          {
               String sCsrId = DOMUtil.getNodeValue(lockDoc, "OUT_LOCKED_CSR_ID");
               logger.debug("CSR lock is not available for COMMUNICATION. order detail id="+orderDetailId);
          
              
               String message = COMConstants.MSG_RECORD_O_LOCKED1 + sCsrId + COMConstants.MSG_RECORD_O_LOCKED2;
               MessagingDOMUtil.addElementToXML(MessagingConstants.ERROR_MESSAGE, message, rootDoc);
               
                     
               
          }else
          {
             MessagingDOMUtil.addElementToXML("actionType", actionType, rootDoc);
          }
          
          this.doForward("messaging_lock_iframe",
                        rootDoc, mapping, request, response,parameters);
          
        }else if(action.equalsIgnoreCase("unlock"))
        {
          
           lockDAO.releaseLock(securityToken, csrId, orderDetailId, applicationName);
           MessagingDOMUtil.addElementToXML("actionType", actionType, rootDoc);
           this.doForward("messaging_lock_iframe",
                        rootDoc, mapping, request, response,parameters);
          
        }else if(action.equalsIgnoreCase("cancel_update_unlock"))
        {
          
           this.updateOrderDeliveryDate(request, dbConnection);
           lockDAO.releaseLock(securityToken, csrId, orderDetailId, applicationName);
           //
        }
        
        
        
        
  				
			} catch (SAXException se) {
                logger.error(se);
                action = MessagingConstants.ERROR;
            } catch (IOException ioe) {
                logger.error(ioe);
                action = MessagingConstants.ERROR;
            } catch (SQLException sqle) {
                logger.error(sqle);
                action = MessagingConstants.ERROR;
            } catch (ParserConfigurationException pe) {
                logger.error(pe);
                action = MessagingConstants.ERROR;
            } catch (TransformerException te) {
                logger.error(te);
                action = MessagingConstants.ERROR;
            } catch (Throwable t) {
                logger.error(t);
                action = MessagingConstants.ERROR;
            }finally{
				
                if(dbConnection!=null){
                  try {
                    dbConnection.close();
                  } catch (Exception e) {
                    logger.error(e);
                    action = MessagingConstants.ERROR;
                  }
                  
                }
            }
            
            
        
			
		    if(action!=null&&action.equalsIgnoreCase(MessagingConstants.ERROR)){
            return mapping.findForward(action);
      
    	  }else
        {
           return null;
        }
    
    	
    
    }
    
    
    
    
    /**
   * The temporary solution to substitute Modify Order. Will be removed after modify order goes live.
   * Update delivery date on order details. Insert the original delivery date to a temp table.
   * @param request
   * @throws java.lang.Exception
   */
    private void updateOrderDeliveryDate(HttpServletRequest request, Connection conn)throws Exception
    {
      boolean deliveryDateUpdated=new Boolean(request.getParameter("deliveryDateUpdated")).booleanValue();
      //only update once.
      if(!deliveryDateUpdated)
      {
            MessagingHelper helper=new MessagingHelper();
            MessageDAO msgDao=new MessageDAO(conn);
            SimpleDateFormat sdfOutput = new SimpleDateFormat("EEE MMM dd hh:mm:ss z yyyy");
            SimpleDateFormat sdfEndDeliveryOutput = 
                       new SimpleDateFormat (MessagingConstants.MESSAGE_END_DELIVERY_DATE_FORMAT);  
            String orderDetailID = request.getParameter(MessagingConstants.REQUEST_ORDER_DETAIL_ID);
            String securityToken = request.getParameter(MessagingConstants.REQUEST_SECURITY_TOKEN);
            String csrId=helper.lookupCurrentUserId(securityToken);
            Date deliveryDate =  sdfOutput.parse(request.getParameter("order_delivery_date"));
            Date deliveryDateEnd=null;
            if(StringUtils.isNotBlank(request.getParameter("order_delivery_date_end")))
            {
              deliveryDateEnd = sdfEndDeliveryOutput.parse(request.getParameter("order_delivery_date_end"));
                        
            }
            //update order detail.
            //msgDao.insertOrderDeliveryDateToTemp(orderDetailID);
            msgDao.updateOrderDeliveryDate(orderDetailID, csrId, deliveryDate, deliveryDateEnd);
            
      }
      
      
    }
    
    
    
    /**
     * Utilize Security Manager to obtain user�s id based on the security token.
     * @param securityToken - String
     * @return String
     * @throws Exception????
     * @todo - Need code from Mike Kruger
     */
    private String lookupCurrentUserId(String securityToken)
        throws Exception {
        String userId = "";

        if (SecurityManager.getInstance().getUserInfo(securityToken) != null) {
            userId = SecurityManager.getInstance().getUserInfo(securityToken)
                                    .getUserID();
        }

        return userId;
    }
  
  
  
  
}