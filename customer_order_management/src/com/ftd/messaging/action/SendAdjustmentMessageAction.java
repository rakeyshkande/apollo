package com.ftd.messaging.action;
import com.ftd.messaging.bo.SendMessageBO;
import com.ftd.messaging.constants.MessagingConstants;
import com.ftd.osp.utilities.plugins.Logger;
import java.sql.Connection;
import java.util.HashMap;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * This class handles sending Adjustment Message. 
 * @author Charles Fox
 * @todo check comments, error handling, and message handling
 */
public class SendAdjustmentMessageAction extends BaseAction
{
    private Logger logger = 
        new Logger("com.ftd.messaging.action.SendAdjustmentMessageAction");
        
    public SendAdjustmentMessageAction()
    {
    }

    /**
     * � Call sendAdjustmentMessage method of SendMessageBO to get the 
     *   forward action name.
     * � Add the request parameter map to HTTP request attributes.
     * � Return the ActionForward instance.
     * @param mapping - ActionMapping
     * @param form - ActionForm
     * @param request - HttpServletRequest
     * @param response - HttpServletResponse
     * @return ActionForward
     * @throws Exception????
     * @todo - code and determine exceptions
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form, 
        HttpServletRequest request, HttpServletResponse response)
        throws ServletException
        {

            logger.debug("Entering Send Adjustment Message Action");
            
            ActionForward actionForward = new ActionForward();
            Connection con=null;
            String forward=null;
            try
            { 
                con=this.createDatabaseConnection();
                SendMessageBO sendMessageBO=new SendMessageBO(con);
                HashMap requestDataMap=this.getRequestDataMap(request);
                forward=sendMessageBO.sendAdjustmentMessage(request,requestDataMap);
          
                 this.setRequestDataMap(request, requestDataMap);
           
                 actionForward=this.getActionForward(forward, request, mapping, requestDataMap );
            
                      
           
            }catch(Exception e){
                logger.error(e);
                actionForward = mapping.findForward(MessagingConstants.ERROR);
                
            }finally{
                
                 //release lock as needed.
                releaseLock(request, con, forward);
                if (con != null) {
                    try {
                        con.close();
                    } catch (Exception e) {
                        logger.error("can not close connection", e);
                    }
                }
                
                logger.info("Exiting Send Adjustment Message Action");
            }
           return actionForward;
        }
}