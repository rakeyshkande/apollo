package com.ftd.messaging.action;

import com.ftd.osp.utilities.plugins.Logger;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;

import com.ftd.messaging.bo.PrepareMessageBO;
import com.ftd.messaging.constants.MessagingConstants;
import com.ftd.messaging.util.MessagingDOMUtil;
import com.ftd.messaging.vo.MessageOrderStatusVO;
import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

/**
 * This class extends PrepareMessageAction class. It will determine whether
 * we need to send user an error message or take user to an appropriate UI page.      
 * @author Charles Fox
 * @todo check comments, error handling, and message handling
 */
public class DeliveryConfirmationAction extends PrepareMessageAction
{
    private Logger logger = 
        new Logger("com.ftd.messaging.action.DeliveryConfirmationAction");
    public DeliveryConfirmationAction()
    {
    }

    /**
     * Add page_title element of the addition_info node of Document Object
     * @param request - HttpServletRequest
     * @param doc - Document
     * @return n/a
     * @throws Exception????
     * @todo - code and determine exceptions
     */
    public String getPageTitle(HttpServletRequest request){
       /* 
        * Override the parent class method. 
        * o	Look up page title from Application Resource properties file that configured in struts-config.xml.
        * String pageTitle=this.getResource(request).getMessage(�delivery_confirm_page_title�);
        * o	Add page_title element of the addition_info node of Document Object.
        <addition_info>
            �
                <name>page_title</name>
                <value>pageTitle</value>
            �
        </addition_info> 
        
        */
    	
    	return this.getMessageFromResource("delivery_confirmation_page_title", request);
    	
    	
    }
    

    /**
     * � Call prepareMessage method of PrepareMessageBO to prepare a Document 
     * object for Delivery Confirmation page.
     * 
     * @param request - HttpServletRequest
     * @return Document
     * @throws ParserConfigurationException
     * @throws SQLException
     * @throws IOException
     * @throws SAXException
     * @throws Exception????
     * @todo - code and determine exceptions
     */
    public Document prepareMessage(HttpServletRequest request, PrepareMessageBO prepareMessageBO) throws SAXException, IOException, SQLException, ParserConfigurationException, Exception
    {
    	return this.prepareMessageForCurrentFlorist(request,prepareMessageBO);
    }
    
    
    
    /**
     * Initialize �reason� field as �PLEASE CONFIRM DELIVERY UPON COMPLETION 
     * WITH DATE, TIME & SIGNATURE IF AVAILABLE.�
     */
    public String getInitialReasonText(HttpServletRequest request){
    	
    	return this.getMessageFromResource("delivery_confirmation_init_reason", request);
    }



    /**
     * Retrieve action from request and process
     * @param request - HttpServletRequest
     * @param mapping - ActionMapping
     * @param doc - Document
     * @param messageOrderStatus - MessageOrderStatusVO
     * @return ActionForward
     * @throws Exception????
     * @todo - code and determine exceptions
     */
    public boolean validateStatus(HttpServletRequest request,
            ActionMapping mapping, Document document,
            MessageOrderStatusVO messageOrderStatus) {
        if(logger.isDebugEnabled())
        {
           logger.debug("in delivery confirmation validation");
           logger.debug("message status, Cancelled: "+messageOrderStatus.isCancelSent());
           logger.debug("message status, rejected: "+messageOrderStatus.isFloristRejectOrder());
          
        }
        boolean valid=false;
        if(messageOrderStatus.isCancelSent()&&!messageOrderStatus.isCancelDenied()){
            
            MessagingDOMUtil.addElementToXML(MessagingConstants.ERROR_MESSAGE, this.getMessageFromResource(MessagingConstants.CANCELLED_ERROR, request), document);
            
        }else if(messageOrderStatus.isVendor()){
            
            MessagingDOMUtil.addElementToXML(MessagingConstants.ERROR_MESSAGE, this.getMessageFromResource(MessagingConstants.VENDOR_ERROR, request), document);
            
         }else  if(messageOrderStatus.isFloristRejectOrder()){
                
             MessagingDOMUtil.addElementToXML(MessagingConstants.ERROR_MESSAGE, this.getMessageFromResource(MessagingConstants.REJECTED_ERROR, request), document);
            
            
        }else{
            valid=true;
        }
        
        
        
        return valid;
    }
}