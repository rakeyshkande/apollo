package com.ftd.messaging.action;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.messaging.dao.MessageDAO;
import com.ftd.messaging.vo.SDSTransactionVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.JAXPUtil;
import com.ftd.osp.utilities.xml.TraxUtil;


public class RetrieveSDSTransactionAction extends Action
{

  private static  Logger  logger                = new Logger("com.ftd.messaging.action.RetrieveSDSTransactionAction");
  private HashMap         pageData              = new HashMap(); 
  private String          venusId               = null;
  private String          venusOrderNumber      = null;

  public RetrieveSDSTransactionAction()
  {
  }

    /**
     *  Retrieve the SDS Transaction based on the venus id #
     * 
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     */
  public  ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                                HttpServletResponse response)
  {
    ActionForward forward = null;
    Connection con = null;
    Document responseDocument = null;
    String xslName = COMConstants.XSL_DISPLAY_SDS_TRANSACTION;
    File xslFile = getXSL(xslName, mapping);

    try 
    {
      //Document that will contain the final XML, to be passed to the TransUtil and XSL page
      responseDocument = (Document) DOMUtil.getDocument();

      //Connection/Database info
      con = DataSourceUtil.getInstance().getConnection(
            ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE,
            COMConstants.DATASOURCE_NAME));

      //Get info passed in the request object
      getRequestInfo(request);

      //Retrieve the data
      Document sdsDoc = retrieveSDSTransaction(con);

      //append the data to the final xml to be sent to the Transformaer
      NodeList nl = sdsDoc.getChildNodes();
      DOMUtil.addSection(responseDocument, nl);
        
			if(logger.isDebugEnabled())
			{
				//IMPORTANT:  Because StringWriter contents are NOT removed even when the flush
				//            method is executed, make StringWriter go away through scope control.
				//            Both the flush and close methods were tested with but were of none affect.
				StringWriter sw = new StringWriter();       //string representation of xml document
				DOMUtil.print((Document) responseDocument,new PrintWriter(sw));
				logger.debug("Retrieve SDS Transaction Action" + sw.toString());
			}

      forward = mapping.findForward(xslName);
      String xslFilePathAndName = forward.getPath(); //get real file name
              
      // Change to client side transform
      TraxUtil.getInstance().getInstance().transform(request, response, responseDocument, 
                            xslFile, xslFilePathAndName, (HashMap)request.getAttribute(COMConstants.CONS_APP_PARAMETERS));

      return null;
    }
    catch(Exception e) {
      try {
        logger.error(e);
        forward = mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);

        // set the error data
        pageData.clear();
        pageData.put("errorFlag", "Y");
        pageData.put("errorUrl", forward.getPath());
        DOMUtil.addSection(responseDocument, "errorData", "data", this.pageData, true);

        String xslFilePathAndName = forward.getPath(); //get real file name
        
        // Change to client side transform
        TraxUtil.getInstance().getInstance().transform(request, response, responseDocument, 
                            xslFile, xslFilePathAndName, (HashMap)request.getAttribute(COMConstants.CONS_APP_PARAMETERS));

        return null;
      }
      catch (Exception ex)
      {
        //if logging is enabled, display the input parameters
        if(logger.isDebugEnabled())
        {
          logger.debug( "SDS Transaction unable to display for Venus Id = " + this.venusId);
        }

        logger.error("Failed trying to send back error url - "  + ex);
        forward = mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
        return forward;
      }
    }
    finally {
      try
      {
        //Close the connection
        if(con != null)
        {
          con.close();
        }
      }
      catch(Exception e)
      {
        logger.error(e);
        forward = mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
        return forward;
      }
    }
  }



  /******************************************************************************
  * getRequestInfo(HttpServletRequest request)
  *******************************************************************************
  * Retrieve the info from the request object, and set class level variables
  * @param  HttpServletRequest
  * @return none
  * @throws none
  */
  private void getRequestInfo(HttpServletRequest request) throws Exception
  {
    //retrieve the venus id
    if(request.getParameter(COMConstants.VENUS_ID)!=null)
    {
      this.venusId = request.getParameter(COMConstants.VENUS_ID);
    }

    //retrieve the venus order number
    if(request.getParameter(COMConstants.VENUS_ORDER_NUMBER)!=null)
    {
      this.venusOrderNumber = request.getParameter(COMConstants.VENUS_ORDER_NUMBER);
    }

  }



/*******************************************************************************************
 * retrieveSDSTransaction()
 *******************************************************************************************
  * This method is used to retrieve the transaction 
  *
  */
  private Document retrieveSDSTransaction(Connection con) throws Exception
  {
    //Instantiate MessageDAO
    MessageDAO msgDAO = new MessageDAO(con);
    
    //declare the return xml document
    Document sdsDoc; 

    //retrieve the SDS transaction information
    SDSTransactionVO sdsVO = (SDSTransactionVO) msgDAO.getSDSTransaction(this.venusId);

    //if the vo is populated, populate data in the xml
    if (sdsVO != null)
    {
      String id = sdsVO.getId().toString(); 
      String venusId = sdsVO.getVenusId(); 
      String requestString = sdsVO.getRequest(); 
      String responseString = sdsVO.getResponse(); 
      sdsDoc = createDocument(id, venusId, requestString, responseString);
    
    }
    //else create an empty xml document
    else
    {
      sdsDoc = DOMUtil.getDocument(); 
    }

    return sdsDoc; 
  }



  /******************************************************************************
  *                                     createDocument()
  *******************************************************************************
  */

  private Document createDocument(String id, String venusId, String requestString,  
                                        String responseString) throws Exception
  {
    //Document that will contain the final XML, to be passed to the TransUtil and XSL page
    Document sdsDoc = JAXPUtil.createDocument(false, false);

    //*****************************************************************************************
    //create the highest element
    //*****************************************************************************************
    Element sdsElement = sdsDoc.createElement("SDS_Transaction");
    sdsDoc.appendChild( sdsElement );

    //*****************************************************************************************
    //create the node for the id
    //*****************************************************************************************
    Element idElement = sdsDoc.createElement("id");
    idElement.appendChild(sdsDoc.createTextNode(id));
    sdsElement.appendChild(idElement);

    //*****************************************************************************************
    //create the node for the venus id
    //*****************************************************************************************
    Element venusIdElement = sdsDoc.createElement("venus_id");
    venusIdElement.appendChild(sdsDoc.createTextNode(venusId));
    sdsElement.appendChild(venusIdElement);

    //*****************************************************************************************
    //create the node for the request string
    //*****************************************************************************************
    Element requestStringElement = sdsDoc.createElement("request_string");
    requestStringElement.appendChild(sdsDoc.createTextNode(requestString));
    sdsElement.appendChild(requestStringElement);

    //*****************************************************************************************
    //create the node for the response string
    //*****************************************************************************************
    Element responseStringElement = sdsDoc.createElement("response_string");
    responseStringElement.appendChild(sdsDoc.createTextNode(responseString));
    sdsElement.appendChild(responseStringElement);

    return (Document)sdsDoc; 

  }

  /******************************************************************************
  *                                     getXSL()
  *******************************************************************************
  * Retrieve name of Customer Order Search Action XSL file
  * @param1 String - the xsl name returned from the Business Object
  * @param2 ActionMapping
  * @return File - XSL File name
  * @throws none
  */

  private File getXSL(String xslName, ActionMapping mapping)
  {

    File xslFile = null;
    String xslFilePathAndName = null;

    ActionForward forward = mapping.findForward(xslName);
    xslFilePathAndName = forward.getPath();                //get real file name
    xslFile = new File(this.getServlet().getServletContext().getRealPath(xslFilePathAndName));
    return xslFile;
  }


}
