package com.ftd.messaging.action;
import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.messaging.bo.QueueBO;
import com.ftd.messaging.bo.RetrieveMessageBO;
import com.ftd.messaging.constants.MessagingConstants;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.StringTokenizer;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;



import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;


/**
 * This class extends com.ftd.messaging.action.BaseAction. It makes calls
 * to the business object to retrieve/delete queues.  It also contains the
 * main flow of logic needed for security processing and XSL stylesheet
 * determination. The transform method in the
 * com.ftd.osp.utilities.xml.TraxUtil class will be used to render the page.
 * @author Charles Fox
 * @todo check comments, error handling, and message handling
 */
public class QueueAction extends BaseAction
{
    private Logger logger =
        new Logger("com.ftd.messaging.action.QueueAction");
    private static String SUCCESS = "success";
    private static String FAILURE = "failure";

    public QueueAction()
    {
    }

    /**
     *  1.	If action passed on request equals �display�, call the
     *  retrieveQueues method in the Queue Business Object to obtain queue
     *  related data.
     *  2.	If action passed on request equals �delete�, call the
     *  getUnableDeleteQueueDoc method in the Queue Business Object to
     *  delete queues from database and obtain unable delete queues.
     *  3.	Call doForward method of its super class to retrieve the
     *  ppropriate XSL file, add security node to the Document and transform
     *  XML and XSL to render UI page.
     * @param n/a
     * @return n/a
     * @throws Exception????
     * @todo - code and determine exceptions
     */
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException{
        logger.info("Entering Action Delete Queue");

        Connection dbConnection = null;
        String actionType = mapping.getParameter();
        Document queues = null;
        String result = null;

        try{
            /*  retrieve security token from request */
            String security= request.getParameter(MessagingConstants.REQUEST_SECURITY_TOKEN);
            dbConnection = createDatabaseConnection();
            QueueBO queueBO = new QueueBO(dbConnection);
            // get action type
           
            if(actionType.equals(MessagingConstants.ACTION_DISPLAY_QDELETE)){
               
                queues = DOMUtil.getDocument();
                queues = queueBO.retrieveQueues(request);

            }else if(actionType.equalsIgnoreCase("delete_gen_queue"))
            {
               String queueId=request.getParameter("queue_message_id");
               String taggedCsrId=request.getParameter("tagged_csr_id");
               boolean deleted=queueBO.deleteQueue(queueId, security, taggedCsrId);
               
               if(!deleted)
               {
                  // Set error to request
                  request.setAttribute("queue_delete_security_error", "Y");
               }else
               {
                 
                  request.setAttribute("remove_queue_flag", "N");
               }
               
               return mapping.findForward(SUCCESS);
               
            }else{
                /* Retrieve order detail id from HTTP request */
                String orderDetailID = request.getParameter(
                    MessagingConstants.REQUEST_ORDER_DETAIL_ID);

                if(orderDetailID==null)
                {
                    /* Order Detail ID not included, throw Exception   */
                    throw new Exception("Order Detail ID not included");
                }

                String queueids = request.getParameter(MessagingConstants.REQUEST_QUEUE_IDS);

                if(queueids==null)
                {
                    /* Queue IDs not included, throw Exception   */
                    throw new Exception("Queue IDs not included");
                }

               String[] queuesToDelete = stringToArray(queueids);
                
                queues = DOMUtil.getDocument();
                MessageResources messageResources = super.getResources(request);

                queues = queueBO.getUnableDeleteQueueDoc(orderDetailID,security,
                    queuesToDelete,messageResources);

            }
            HashMap parameters = this.getRequestDataMap(request);
            result = SUCCESS;

            this.doForward(result, queues, mapping, request, response, parameters);
            return null;
        }catch(SAXException e){
            logger.error(e);
            result = FAILURE;
        }catch(IOException e){
            logger.error(e);
            result = FAILURE;
        }catch(TransformerException e){
            logger.error(e);
            result = FAILURE;
        }catch(ParserConfigurationException e){
            logger.error(e);
            result = FAILURE;
        }catch(SQLException e){
            logger.error(e);
            result = FAILURE;
        }catch(Exception e){
            logger.error(e);
            result = FAILURE;
      }finally{
            try
            {
                /* close the database connection */
                if(dbConnection!=null){
                    dbConnection.close();
                }
            }catch(Exception e){
                throw new ServletException(e);
            }

            logger.info("Exiting Action Delete Queue");
        }
        return mapping.findForward(result);
    }

    public String[] stringToArray( String text )
    {
        String[] returnVal = null ;
        if ( ( text != null ) && ( text.length() > 0 ) )
        {
            //logger.debug("queue ids="+text);
            StringTokenizer t = new StringTokenizer( text,"," );
            returnVal = new String[ t.countTokens() ];
            for( int i = 0 ; i < returnVal.length ; i++ )
            {
                returnVal[ i ] = t.nextToken();
                //logger.debug("add queue id "+i+" "+returnVal[ i ]);
            }
        }
        return returnVal ;
    }


}