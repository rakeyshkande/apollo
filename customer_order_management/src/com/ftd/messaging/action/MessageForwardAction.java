package com.ftd.messaging.action;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.xml.sax.SAXException;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.messaging.bo.PrepareMessageBO;
import com.ftd.messaging.constants.MessagingConstants;
import com.ftd.messaging.vo.MessageOrderStatusVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.security.SecurityManager;

/**
 * This class is used to forward control to a URI based on action type.   
 * @author Charles Fox
 * @todo check comments, error handling, and message handling
 */
public class MessageForwardAction extends BaseAction
{
	 private Logger logger = 
        new Logger(this.getClass().getName());

    /**
     *  1. Call retrieveMessageOrderStatus method of PrepareMessageBO.
     *  2. Put MessageOrderStatusVO in request.
     *  3. Retrieve action from HTTP request.
     *  4. Get forward URI from struts configuration file based the action
     *  then forward control to this URI.
     * @param n/a
     * @return n/a
     * @throws Exception????
     * @todo - code and determine exceptions
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException{
    	
    	
    	Connection dbConnection = null;
    	String action=null;
      boolean debug=logger.isDebugEnabled();
      if(debug)
      {
        logger.debug("Start Message Forward Action");
      }
    	
    	
			try {
				dbConnection=this.createDatabaseConnection();
		        HashMap parameters=this.getRequestDataMap(request);
		        String messageOrderNumber=request.getParameter(MessagingConstants.MESSAGE_ORDER_NUMBER);
				String messageType=request.getParameter(MessagingConstants.MESSAGE_TYPE);
				String orderDetailId = request.getParameter(MessagingConstants.REQUEST_ORDER_DETAIL_ID);
		        
                if(request.getParameter(COMConstants.ACTION_TYPE)!=null)
                {
                  action = request.getParameter(COMConstants.ACTION_TYPE);
                  if(debug)
                  {
                    logger.debug("message forward action: "+action);
                  }
                }
                else
                {
                    throw new ServletException("Message Forward Action is null");
                }
                
                PrepareMessageBO prepareMessageBO=new PrepareMessageBO(dbConnection);
                           
                if(debug)
                {
                  logger.debug("message order number: "+messageOrderNumber);
                  logger.debug("message type: "+messageType);
                }
                
                MessageOrderStatusVO statusVO=prepareMessageBO.retrieveMessageOrderStatus(messageOrderNumber, messageType, orderDetailId);
                if(statusVO!=null){
                  request.setAttribute(MessagingConstants.ATTR_MESSAGE_STATUS_VO, statusVO);
                  //add order type to  parameters
                   parameters.put(MessagingConstants.MESSAGE_ORDER_TYPE, statusVO.getOrderType());
                   parameters.put(MessagingConstants.MESSAGE_ORDER_STATUS, statusVO.getOrderStatus());
                }
          
				
				
			} catch (SAXException se) {
                logger.error(se);
                action = MessagingConstants.ERROR;
            } catch (IOException ioe) {
                logger.error(ioe);
                action = MessagingConstants.ERROR;
            } catch (SQLException sqle) {
                logger.error(sqle);
                action = MessagingConstants.ERROR;
            } catch (ParserConfigurationException pe) {
                logger.error(pe);
                action = MessagingConstants.ERROR;
            } catch (TransformerException te) {
                logger.error(te);
                action = MessagingConstants.ERROR;
            } catch (Throwable t) {
                logger.error(t);
                action = MessagingConstants.ERROR;
            }finally{
				
                if(dbConnection!=null){
                  try {
                    dbConnection.close();
                  } catch (Exception e) {
                    logger.error(e);
                    action = MessagingConstants.ERROR;
                  }
                  
                }
            }
            
            
        
			
		    if(action!=null){
            return mapping.findForward(action);
      
    	  }else
        {
           return null;
        }
    
    	
    
    }
    
    
    
    
    /**
     * Utilize Security Manager to obtain user�s id based on the security token.
     * @param securityToken - String
     * @return String
     * @throws Exception????
     * @todo - Need code from Mike Kruger
     */
    private String lookupCurrentUserId(String securityToken)
        throws Exception {
        String userId = "";

        if (SecurityManager.getInstance().getUserInfo(securityToken) != null) {
            userId = SecurityManager.getInstance().getUserInfo(securityToken)
                                    .getUserID();
        }

        return userId;
    }
    
    
    
}