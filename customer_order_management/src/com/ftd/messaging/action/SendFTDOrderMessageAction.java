package com.ftd.messaging.action;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.filter.DataFilter;
import com.ftd.messaging.bo.SendMessageBO;
import com.ftd.messaging.constants.MessagingConstants;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.decisionresult.filter.DecisionResultDataFilter;

import java.sql.Connection;
import java.util.HashMap;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * This class handles sending New FTD Message.
 * @author Charles Fox
 * @todo check comments, error handling, and message handling
 * @todo confirm stored procedure setup against actual stored procedures
 */
public class SendFTDOrderMessageAction extends BaseAction
{

    private Logger logger =
        new Logger("com.ftd.messaging.action.SendFTDOrderMessageAction");

    public SendFTDOrderMessageAction()
    {
    }

    /**
     * � Call sendFtdMessage method of SendMessageBO to get forward action name.
     * � Add the request parameter map to HTTP request attributes.
     * � Return the ActionForward instance.
     * @param mapping - ActionMapping
     * @param form - ActionForm
     * @param request - HttpServletRequest
     * @param response - HttpServletResponse
     * @return ActionForward
     * @throws Exception????
     * @todo - code and determine exceptions
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response)
        throws ServletException
        {
            logger.info("Entering Send FTD Message Action");

            ActionForward actionForward = new ActionForward();
            Connection con=null;
             String forward = "";
            try
            {
                con=this.createDatabaseConnection();
                String actionType=request.getParameter(COMConstants.ACTION_TYPE);
                String messageType=request.getParameter(MessagingConstants.MESSAGE_TYPE);

                SendMessageBO sendMessageBO=new SendMessageBO(con);
                
                HashMap requestDataMap=this.getRequestDataMap(request);
                boolean comp_order =new Boolean(request.getParameter(MessagingConstants.REQUEST_PARAMETER_COMP_ORDER)).booleanValue();
               
                if(comp_order){
                    forward=sendMessageBO.sendCOMPMessage(request,requestDataMap);
                }
                else
                {
                    forward=sendMessageBO.sendFTDMessage(request,requestDataMap);
                }
                
                this.setRequestDataMap(request, requestDataMap);
                
                // Check if same order was cancelled during this order session.  If so, then a Call Disposition is required
                //
                String orderDetailId = DataFilter.getParameter(request, COMConstants.ORDER_DETAIL_ID);
                String cancelledOrder = DataFilter.getParameter(request, COMConstants.CDISP_CANCELLED_ORDER);
                if ((cancelledOrder != null) && (cancelledOrder.length() > 0)) {
                    if (cancelledOrder.equals(request.getParameter("order_detail_id"))) {
                        DecisionResultDataFilter.updateEntity(request, orderDetailId, true);
                        if (logger.isDebugEnabled()) {
                            logger.debug("Call Disposition required for: " + cancelledOrder);
                        }
                    }
                }
                
                /* Return ActionForward instance based on the forward action name.*/
                 actionForward=this.getActionForward(forward, request, mapping, requestDataMap);
                 
            }catch(Exception e){
                logger.error(e);
                actionForward = mapping.findForward(MessagingConstants.ERROR);

            }finally{
                //release lock as needed.
                 releaseLock(request, con, forward);
                if(con!=null)
                {
                  try
                  {
                    con.close();
                  }catch(Exception e)
                  {
                    logger.error(e);
                    actionForward = mapping.findForward(MessagingConstants.ERROR);
                  }
                }

                logger.info("Exiting Send FTD Message Action");
            }

            return actionForward;
        }
}