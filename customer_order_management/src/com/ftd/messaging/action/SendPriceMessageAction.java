package com.ftd.messaging.action;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.messaging.bo.SendMessageBO;
import com.ftd.messaging.constants.MessagingConstants;
import com.ftd.messaging.util.MessagingDOMUtil;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityManager;
import com.ftd.security.exceptions.AuthenticationException;
import com.ftd.security.exceptions.ExpiredCredentialsException;
import com.ftd.security.exceptions.ExpiredIdentityException;
import com.ftd.security.exceptions.TooManyAttemptsException;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.util.HashMap;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;

public class SendPriceMessageAction extends BaseAction

{
    private Logger logger = 
        new Logger("com.ftd.messaging.action.SendPriceMessageAction");
  
    private static final String RESOURCE_PRICE_INCREASE = "CSPriceIncrease";
    private static final String RESOURCE_PRICE_INCREASE_PERMISSION = "Yes";

    public SendPriceMessageAction()
    {
    }

    /**
     * Performs 2 functions: 
     * � Call sendPriceMessage method of SendMessageBO to get forward 
     *   action name.
     * � Add the request parameter map to HTTP request attributes. 
     * � Return ActionForward instance based on the forward action name.
     * Or it can be used to authorize a price increase by passing a parameter 
     *   "authorizing" set to any value. The latter case will return an XML document indicating
     *   if authorization was successful or not.
     * @param mapping - ActionMapping
     * @param form - ActionForm
     * @param request - HttpServletRequest
     * @param response - HttpServletResponse
     * @return ActionForward
     * @throws IOException
     * @throws ServletException
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form, 
        HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException
        {

            logger.info("Entering Send Price Message Action");
            
            ActionForward actionForward = new ActionForward();
            String actionType = "";
            Connection con=null;
            String forwardName="";
            try
            {
                con=this.createDatabaseConnection();
                // get action type
                if(request.getParameter(COMConstants.ACTION_TYPE)!=null)
                {
                  actionType = request.getParameter(COMConstants.ACTION_TYPE);
                }
                else
                {
                    //assume display communication screen action
                    actionType = MessagingConstants.ACTION_SEND_PRICE_MSG;
                }
                
                if(actionType.equals(MessagingConstants.ACTION_SEND_PRICE_MSG)){                	
                	// If manager_code is set then this is a request for authorization to make a price change
                	String authorizing = request.getParameter("authorize_price_increase");
                	if (authorizing != null) {
                		return authorizePriceIncrease(mapping, request, response);
                	}

                    /* Call sendPriceMessage method of SendMessageBO to get forward action name. */
                    SendMessageBO sendBO = new SendMessageBO(con);
                    HashMap parameters = this.getRequestDataMap(request);
                    forwardName = sendBO.sendPriceMessage(request,parameters);
                    
                    /*  Add the request parameter map to HTTP request attributes???? */
                    this.setRequestDataMap(request, parameters);
                    
                    /* Return ActionForward instance based on the forward action name.*/ 
                     actionForward=this.getActionForward(forwardName, request, mapping, parameters);                     
                }
                else
                {
                    /* invalid action type passed, throw an error */
                    String errorMsg = actionType + MessagingConstants.INVALID_ACTION_TYPE_MSG + 
                        MessagingConstants.SEND_PRICE_MESSAGE;
                    throw new ServletException(errorMsg);    
                }
            }catch(Exception e){
                actionForward = mapping.findForward(MessagingConstants.ERROR);
                logger.error(e);
                
            }finally{
                 
                  //release lock as needed.
                 releaseLock(request, con, forwardName);
                 if (con != null) {                    
                    try {
                        con.close();
                    } catch (Exception e) {
                        logger.error("can not close connection", e);
                    }
                }
                logger.info("Exiting Send Price Message Action");
            }
            logger.debug("path="+actionForward.getPath());
            return actionForward;
        }

	private ActionForward authorizePriceIncrease(ActionMapping mapping, HttpServletRequest request, HttpServletResponse response)
			throws ParserConfigurationException, Exception, TransformerException, IOException {
		/* validate permissions */
		Document responseDocument = (Document) DOMUtil.getDocument();

		if (validatePriceIncrease(request)) 
			MessagingDOMUtil.addElementToXML("AUTHORIZED", "Y", responseDocument);
		else
			MessagingDOMUtil.addElementToXML("AUTHORIZED", "N", responseDocument);
		
		String xslName = "authorize";
		File xslFile = getXSL(xslName, mapping);

		ActionForward forward = mapping.findForward(xslName);
		String xslFilePathAndName = forward.getPath(); //get real file name
		
		TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, xslFilePathAndName, null);          
		return null;
	}


	/**
     *	Authorize user for a price increase.
	 * @param request
	 * @return - true = user is authorized, false = user is not authorized
	 * @throws Exception
	 */
    private boolean validatePriceIncrease(HttpServletRequest request) 
    throws Exception
    {
    	// Variables
    	Object token = null;
    	boolean authenticated = false;
    	String unitID = null;
    	String mgrCode = request.getParameter(MessagingConstants.APPROVAL_IDENTITY_ID);
    	String mgrPassword = request.getParameter(MessagingConstants.APPROVAL_IDENTITY_PASSWORD);
    	String context = request.getParameter(COMConstants.CONTEXT);

    	// Obtain unit id
    	unitID = ConfigurationUtil.getInstance().getProperty(COMConstants.SECURITY_FILE, COMConstants.UNIT_ID);

    	try
    	{
    		// Authenticate comp order
    		token = SecurityManager.getInstance().authenticateIdentity(context, unitID, mgrCode, mgrPassword);		  
    		authenticated = true;
    	}
    	catch(ExpiredIdentityException e){
    		logger.error("User was not authenticated:", e);
    	}
    	catch(ExpiredCredentialsException e){
    		logger.error("User was not authenticated:", e);
    	}
    	catch(AuthenticationException e){
    		logger.error("User was not authenticated:", e);
    	}
    	catch(TooManyAttemptsException e){
    		logger.error("User was not authenticated:", e);
    	}
    	
    	boolean hasPerm = SecurityManager.getInstance().assertPermission(COMConstants.RESOURCE_CONTEXT, token, 
    			this.RESOURCE_PRICE_INCREASE, this.RESOURCE_PRICE_INCREASE_PERMISSION);	
    	// Check authentication status and user permission status
    	if(!authenticated || !SecurityManager.getInstance().assertPermission(COMConstants.RESOURCE_CONTEXT, token, 
    			this.RESOURCE_PRICE_INCREASE, this.RESOURCE_PRICE_INCREASE_PERMISSION))
    	{
    		authenticated = false;
    		return authenticated;
    	}

    	return authenticated;
    }

}