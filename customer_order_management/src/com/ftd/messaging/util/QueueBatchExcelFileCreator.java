package com.ftd.messaging.util;

import com.ftd.messaging.constants.MessagingConstants;
import com.ftd.messaging.vo.QueueDeleteHeaderStatusVO;

import com.ftd.osp.utilities.vo.QueueDeleteDetailVO;

import java.util.ArrayList;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;

/**
 * Utility class to generate Excel file
 * @author Vinay Shivaswamy
 * @date 12/14/2010
 * @version 1.0 for MQD FIX_4_7_0
 */
public class QueueBatchExcelFileCreator {
    public QueueBatchExcelFileCreator() {
    }
      
    public HSSFWorkbook createWorkbook(QueueDeleteHeaderStatusVO statusVO) throws Exception {

            HSSFWorkbook wb = new HSSFWorkbook();
            HSSFSheet sheet = wb.createSheet(statusVO.getLinkToSave());
            
            //set the header column width
            sheet.setColumnWidth((short)0, (short)3500);
            sheet.setColumnWidth((short)1, (short)5500);
            sheet.setColumnWidth((short)2, (short)4000);
            sheet.setColumnWidth((short)3, (short)7000);
            
            
            //Create the style for the header rows(cells).
            HSSFCellStyle headerCellStyle = wb.createCellStyle();
            headerCellStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
            headerCellStyle.setFillBackgroundColor(HSSFColor.GREY_25_PERCENT.index);
            HSSFFont boldFont = wb.createFont();
            boldFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            headerCellStyle.setFont(boldFont);
            
            //Set the header row, cell and value
            HSSFRow row = sheet.createRow((int)0);
            HSSFCell cell = row.createCell((short)0);
            cell.setCellStyle(headerCellStyle);
            cell.setCellValue("Message Id");
            
            cell = row.createCell((short)1);
            cell.setCellStyle(headerCellStyle);
            cell.setCellValue("External Order Number");
            
            cell = row.createCell((short)2);
            cell.setCellStyle(headerCellStyle);
            cell.setCellValue("Result");

            cell = row.createCell((short)3);
            cell.setCellStyle(headerCellStyle);
            cell.setCellValue("Reason for Failure");
            
            //rowNum = 0 => header.
            int rowNum = 0; 
            
            //Build the content rows with cell and values
            for (int detailVOPosition = 0; detailVOPosition < statusVO.getQueueDeleteDetailVOList().size(); detailVOPosition++) {
                
                rowNum++;
                row = sheet.createRow((short)rowNum);
                
                QueueDeleteDetailVO detailVO = (QueueDeleteDetailVO)statusVO.getQueueDeleteDetailVOList().get(detailVOPosition);
                
                cell = row.createCell((short)0);
                if(detailVO.getMessageId() == null )
                  cell.setCellValue(" ");
                else
                  cell.setCellValue(detailVO.getMessageId());
                
                cell = row.createCell((short)1);
                if(StringUtils.isEmpty(detailVO.getExternalOrderNumber()))
                  cell.setCellValue(" ");
                else
                  cell.setCellValue(detailVO.getExternalOrderNumber());
                
                
                if(StringUtils.isEmpty(detailVO.getProcessedStatus() )){
                    cell = row.createCell((short)2);
                    cell.setCellValue(" ");
                    cell = row.createCell((short)3);
                    cell.setCellValue(" ");
                }
                else {
                    if(detailVO.getProcessedStatus().equalsIgnoreCase(MessagingConstants.SUCCESS_STATUS)){
                        cell = row.createCell((short)2);
                        cell.setCellValue(MessagingConstants.SUCCESS_STATUS);
                        cell = row.createCell((short)3);
                        cell.setCellValue(" ");
                    }
                    if(detailVO.getProcessedStatus().equalsIgnoreCase(MessagingConstants.FAILURE_STATUS)){
                        cell = row.createCell((short)2);
                        cell.setCellValue(MessagingConstants.FAILURE_STATUS);
                        cell = row.createCell((short)3);
                        cell.setCellValue(detailVO.getErrorText());
                    }
                    if(detailVO.getProcessedStatus().equalsIgnoreCase(MessagingConstants.DUPLICATE_STATUS)){
                        cell = row.createCell((short)2);
                        cell.setCellValue(MessagingConstants.DUPLICATE_STATUS);
                        cell = row.createCell((short)3);
                        cell.setCellValue(" ");
                    }
                }
                
            }
            return wb;
        }
}
