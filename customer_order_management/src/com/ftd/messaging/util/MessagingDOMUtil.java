/*
 * Created on Apr 20, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.ftd.messaging.util;

import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.XMLFormat;
import java.util.Iterator;
import java.util.Map;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;


import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.ftd.osp.utilities.xml.DOMUtil;

/**
 * @author achen
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class MessagingDOMUtil {

	
	public static Document createEmptyDocument() throws ParserConfigurationException {
    	
    	Document doc=DOMUtil.getDefaultDocument();
      Element root=doc.createElement("root");
      doc.appendChild(root);
      return doc;
    	
    }
    
    /**
     *  Add an error message node to the DOM Document object.
     * @param errorMessage - String  
     * @param doc - Document
     * @return n/a
     * @throws Exception????
     * @todo - code and determine exceptions
     */
    public static void addElementToXML(String name, String value, Document doc)
    {
      
    	Element entry=(Element)doc.createElement(name);
    	entry.appendChild(doc.createTextNode(value));
      
    	doc.getDocumentElement().appendChild(entry);
        
    }
    
    
    
    public static void addElementsToXML(String elementName, String entryName, Map dataMap, Document doc)
    {
      
    	Element element=(Element)doc.createElement(elementName);
      Iterator it=dataMap.keySet().iterator();
      while(it.hasNext())
      {
        Element entry=(Element)doc.createElement(entryName);
        String key=(String)it.next();
        String[] obj=(String[])dataMap.get(key);
        if(obj!=null&&obj.length>0){
          String value=obj[0];
          if(value!=null&&value.trim().length()>0){
            Element keyElm=(Element)doc.createElement(key);
            keyElm.appendChild(doc.createTextNode(value));
            entry.appendChild(keyElm);
            element.appendChild(entry);
          }
        }
      }
    	      
    	doc.getDocumentElement().appendChild(element);
        
    }
    
    
    public static String getNodeValue(Document doc, String path) throws XPathExpressionException {
        String value="";
        Document xmlDoc=(Document)doc;
        
        Node xmlNode= DOMUtil.selectSingleNode(xmlDoc,path);
        
        if(xmlNode!=null){
            value=xmlNode.getNodeValue();
        }
        
        return value;
    }
    
    /**
     * For a resultSet, use the XMLFormat to create an Document
     * 
     * @param ResultSet, XMLFormat
     * @return Document
     */
    public static Document convertToXMLDOM(CachedResultSet rset, XMLFormat xmlFormat) 
      throws Exception
    {
 
        boolean upper;
        String xmlType = "attribute";
        String xmlTop = "rowset";
        String xmlBottom = "row";

        if(xmlFormat != null) {
            xmlType = xmlFormat.getAttribute("type");
            xmlTop = xmlFormat.getAttribute("top");
            xmlBottom = xmlFormat.getAttribute("bottom");
        }

        if (rset == null)  {
            return null;
        }

        Document doc = null;

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setIgnoringElementContentWhitespace(true);
        factory.setNamespaceAware(false);
        DocumentBuilder builder = factory.newDocumentBuilder();
        doc = builder.newDocument();
        Element rowset = null, row = null, column = null;

        rowset = doc.createElement(xmlTop);

        // loop thru the result set and generate xml
        int i = 0;
        while (rset.next())  
        {
            i++;
            int columnCount = rset.getColumnCount();
            // create the row tag
            row = doc.createElement(xmlBottom);
            // set up the num attrib on the row tag
            row.setAttribute("num", String.valueOf(i));
            for (int j = 0; j < columnCount; )  
            {
              String colName=rset.getFieldName(++j).toLowerCase();
              String value=rset.getString(j);
              if(xmlType.equals("attribute")){
                row.setAttribute(colName, value);
              }else
              {
                 column = doc.createElement(colName);
                if(StringUtils.isNotEmpty(value))
                {
                  column.appendChild(doc.createTextNode(value));
                }
                 row.appendChild(column);
              }
            }// end for
            
            
      
            rowset.appendChild(row);
            
        } // end while
        
        doc.appendChild(rowset);
        
        return doc;
    }
        
    
  
	
}
