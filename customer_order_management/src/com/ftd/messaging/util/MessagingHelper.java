package com.ftd.messaging.util;

import com.ftd.customerordermanagement.util.COMDeliveryDateUtil;
import com.ftd.customerordermanagement.util.DeliveryDateConfiguration;
import com.ftd.customerordermanagement.util.DeliveryDateParameters;
import com.ftd.messaging.vo.FTDMessageVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.security.SecurityManager;

import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;

import org.w3c.dom.Document;




public class MessagingHelper 
{
  private Logger logger;

  
  public MessagingHelper()
  {
    logger = new Logger("com.ftd.messaging.util.MessagingHelper");
  }
  
  
  /**
     * Utilize Security Manager to obtain user�s id based on the security token.
     * @param securityToken - String
     * @return String
     * @throws Exception????
     * @todo - Need code from Mike Kruger
     */
    public String lookupCurrentUserId(String securityToken)
        throws Exception {
        String userId = "";

        if (SecurityManager.getInstance().getUserInfo(securityToken) != null) {
            userId = SecurityManager.getInstance().getUserInfo(securityToken)
                                    .getUserID();
        }

        return userId;
    }
    
    
    
    
  public Date createDeliveryDate(String monthnStr, String dateStr)
  {
   
    Calendar c = Calendar.getInstance();
    c.setTime(new Date());
    int year = c.get(Calendar.YEAR);
    int currentMonth = c.get(Calendar.MONTH);
    int month = Integer.parseInt(monthnStr);
    c.set(Calendar.MONTH, month);
    c.set(Calendar.DATE, Integer.parseInt(dateStr));
    if (month < currentMonth)
    {
      c.set(Calendar.YEAR, year + 1);
    }
    else
    {
      c.set(Calendar.YEAR, year);
    }
    return c.getTime();
   
  }
 
/*******************************************************************************************
 * retrieveVendorDeliveryDates()
 *******************************************************************************************
  * This method is used to load the valid ship methods and delivery dates
  *
  */
  public Document retrieveVendorDeliveryDates(boolean minDateFlag, FTDMessageVO ftdMsgVO) throws Exception
  {
    String DATE_FORMAT = "MM/dd/yyyy";

    Document deliveryDatesXML = DOMUtil.getDocument();

    DeliveryDateParameters ddParms = new DeliveryDateParameters();
    ddParms.setMinDaysOut( minDateFlag );
    ddParms.setRecipientState( ftdMsgVO.getRecipientState());
    ddParms.setRecipientZipCode( ftdMsgVO.getRecipientZip() );
    ddParms.setRecipientCountry( ftdMsgVO.getRecipientCountry() );
    ddParms.setProductId( ftdMsgVO.getProductId() );
    ddParms.setOccasionId( ftdMsgVO.getOccasionCode() ); 
    ddParms.setOrderOrigin( "Test" );  //remove once the origin is passed in correctly
    //ddParms.setOrderOrigin( ftdMsgVO.getOrderOrigin ); //need to get
    ddParms.setIsFlorist( false );
    ddParms.setAddons(ftdMsgVO.getOrderAddOns());
    ddParms.setSourceCode(ftdMsgVO.getSourceCode());
    
    SimpleDateFormat dateFormat = new SimpleDateFormat( DATE_FORMAT );
    Calendar date = Calendar.getInstance();
    
    COMDeliveryDateUtil dateUtil = new COMDeliveryDateUtil();
    dateUtil.setParameters( ddParms );

    try
    {
      // do not show date ranges for vendor products
      DeliveryDateConfiguration deliveryDateConfig = new DeliveryDateConfiguration();

      deliveryDateConfig.setDisplayDateFormat( DATE_FORMAT );
      deliveryDateConfig.setSubmitDateFormat( DATE_FORMAT );
      deliveryDateConfig.setShowDateRanges(false);
      deliveryDateConfig.setIncludeRequestedDeliveryDate( true );
      dateUtil.setConfiguration( deliveryDateConfig );

      deliveryDatesXML = dateUtil.getXmlCompVendorDeliveryDates();
      
      return deliveryDatesXML;
    }
    catch (Exception e)
    {
      logger.error(e);
      throw(e);
    }

  }
  
  
}