package com.ftd.messaging.util;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.dao.OrderDAO;
import com.ftd.customerordermanagement.util.COMDeliveryDateUtil;
import com.ftd.customerordermanagement.util.DeliveryDateConfiguration;
import com.ftd.customerordermanagement.util.DeliveryDateParameters;
import com.ftd.messaging.constants.MessagingConstants;
import com.ftd.messaging.dao.MessageDAO;
import com.ftd.messaging.vo.FTDMessageVO;
import com.ftd.messaging.vo.MessageVO;
import com.ftd.mo.dao.UpdateOrderDAO;
import com.ftd.op.common.to.ResultTO;
import com.ftd.op.venus.to.AdjustmentMessageTO;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.security.SecurityManager;

import java.sql.Connection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;




import org.apache.commons.lang.StringUtils;


public class MessageUtil 
{
  private Logger logger = new Logger("com.ftd.messaging.util.MessageUtil");
  private Connection conn;

  
  public MessageUtil(Connection conn)
  {
    super();
    this.conn = conn;
  }
  
  
  /**
  * Is an auto ADJ required.  This functionality only evaluates SDS and FTD WEST orders.  
  * The Venus functionality has been left untouched.  The Venus functionality
  * for sending auto ADJ with a CAN currently exists within RefundBO, SendMessageBO, and 
  * CreateNewOrderBO.  There is a document detailing a possible refactoring
  * of all the dropship SendCancel and SendCancel with an auto ADJ code.
  * The document has been attached to a future enhancement defect.
  * 
  * Logic:  If shipping system is SDS and order status is shipped or
  * order status is printed and ship date is on or after the current date
  * an auto ADJ is required.
  * 
  * Note - For Dropship Integration Project DI-9, we are adding FTD WEST shipping system to this as well.
  * 
  * @param msgVO - message to be evaluated
  * @return boolean - true if an auto ADJ is required
  * @throws Exception
  */
  public boolean requiresSDSAutoADJ(MessageVO msgVO)
      throws Exception 
  {
    if(msgVO.getShippingSystem().equalsIgnoreCase(MessagingConstants.SHIPPING_SYSTEM_SDS) ||
       msgVO.getShippingSystem().equalsIgnoreCase(MessagingConstants.SHIPPING_SYSTEM_FTD_WEST))
    {
      // Obtain order disposition (aka order status)
      String orderDisp = new OrderDAO(conn).getOrderDispCode(msgVO.getOrderDetailId());
      
      /* 
       * Logic:  If shipping system is SDS and order status is shipped or
       * order status is printed and ship date is on or after the current date
       * an auto ADJ is required.
       */
      if(orderDisp.equalsIgnoreCase(MessagingConstants.ORDER_DISP_SHIPPED) 
        || ( orderDisp.equalsIgnoreCase(MessagingConstants.ORDER_DISP_PRINTED) &&  checkShipDate(msgVO) ))
      {
        return true;
      }
    }
    return false;
  }
 

  /**
   * Determines if the message shipping system is SDS or FTD WEST. 
   * 
   * Note - For Dropship Integration Project DI-9, we are adding FTD WEST shipping system to this as well.
   * 
   * @param msgVO - message to be evaluated
   * @return boolean - true if shipping system is SDS or FTD WEST
   * @throws Exception
  */
  public boolean isShippingSystemSDS(MessageVO msgVO)
    throws Exception 
  {
    if(msgVO == null)
      throw new Exception("Unable to obtain FTD message shipping system.  Message is null.");
    
    if(msgVO.getShippingSystem().equalsIgnoreCase(MessagingConstants.SHIPPING_SYSTEM_SDS) ||
    		msgVO.getShippingSystem().equalsIgnoreCase(MessagingConstants.SHIPPING_SYSTEM_FTD_WEST))
      return true;
    else
      return false;
  }


  /**
   * Obtains an FTD message based on venus order number.
   * 
   * @param venusOrderNum - order number from venus record
   * @return MessageVO - FTD message
   * @throws Exception
  */
  public MessageVO getMessage(String venusOrderNum)
      throws Exception 
  {
    MessageVO ftdMsg = null;
    
    if(!StringUtils.isBlank(venusOrderNum))
    {
      // Mimics SendMessageBO
      ftdMsg = new MessageDAO(conn).getMessageForCurrentFlorist(venusOrderNum, MessagingConstants.VENUS_MESSAGE, false, null, false);
    }
    else
    {
      throw new Exception("Unable to obtain FTD message for SDS auto ADJ determination.  No venusOrderNum.");
    }
    
    if(ftdMsg == null)
      throw new Exception("Unable to obtain FTD message for SDS auto ADJ determination.");
    
    return ftdMsg;
  }
 
 
  /**
   * Check if the current date is greater than or equal to the ship date.
   * 
   * @param msgVO - message to be evaluated
   * @return boolean - true if current date is greater than or equal to today
   * @throws ParseException
   */
  public boolean checkShipDate(MessageVO msgVO)
      throws ParseException 
      {
      
      if (logger.isDebugEnabled()) 
      {
          logger.debug("Entering checkShipDate");
      }

      Date shipDate = msgVO.getShipDate();

      if (logger.isDebugEnabled()) 
      {
          logger.debug("shipDate: " + shipDate);
      }
  
      boolean dateComparison = false;
  
      try 
      {
          Calendar calCurrent = Calendar.getInstance(TimeZone.getDefault());
          Date currentDate = calCurrent.getTime();
  
          if (!currentDate.before(shipDate)) {
              dateComparison = true;
          }
          
      } 
      finally 
      {
          if (logger.isDebugEnabled()) 
          {
              logger.debug("Exiting checkShipDate");
          }
      }
  
      return dateComparison;
  } 
  
    /**
     * Returns true if order is in printed status and it is after the zone
     * jump label date 
     * @param venusId
     * @return CachedResultSet
     * @throws Exception
     */

    public boolean isZJOrderInTransit(String venusId)
        throws Exception 
    {
        MessageDAO mDAO = new MessageDAO(this.conn);
        FTDMessageVO messageVO = mDAO.getVenusMessageForZJ(venusId);
            
        if(messageVO != null && messageVO.isZoneJumpFlag())
        {
            Date today = new Date();
            String sdsStatus = messageVO.getSdsStatus();
            if((sdsStatus != null && sdsStatus.equalsIgnoreCase(COMConstants.SDS_STATUS_PRINTED)) 
              &&  today.after(messageVO.getZoneJumpLabelDate()))
            {
                return true;
            }
        }  
      return false;
    }
    
}





  /**  SAVE JUST IN CASE WE WANT TO CALL GET MESSAGE FROM RefundBO OR CreateNewOrderBO
   * Obtains an FTD message based on any of the parameters passed in.  All 
   * three parameters are acceptable to facilitate integration with current code.
   * 
   * @param orderDetailId - order detail id
   * @param venusOrderNum - order number from venus record
   * @param messageId - venus message id
   * @return MessageVO - FTD message
   * @throws Exception
  public MessageVO getMessage(String orderDetailId, String venusOrderNum, String messageId)
      throws Exception 
  {
    MessageVO ftdMsg = null;
    
    if(!StringUtils.isBlank(messageId))
    {
      // Mimics RefundBO
      ftdMsg = new MessageDAO(conn).getMessage(messageId, MessagingConstants.VENUS_MESSAGE);
    }
    else if(!StringUtils.isBlank(venusOrderNum))
    {
      // Mimics SendMessageBO
      ftdMsg = new MessageDAO(conn).getMessageForCurrentFlorist(venusOrderNum, MessagingConstants.VENUS_MESSAGE, false, null, false);
    }
    else if(!StringUtils.isBlank(orderDetailId))
    {
      // Mimics CreateNewOrderBO
      ftdMsg = new UpdateOrderDAO(conn).getMessageDetailLastFTD(orderDetailId, MessagingConstants.VENUS_MESSAGE);
    }
    else
    {
      throw new Exception("Unable to obtain FTD message for SDS auto ADJ determination.  All parameters are empty.");
    }
    
    if(ftdMsg == null)
      throw new Exception("Unable to obtain FTD message for SDS auto ADJ determination.");
    
    return ftdMsg;
  }
   */
