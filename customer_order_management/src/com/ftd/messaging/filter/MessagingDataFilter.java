package com.ftd.messaging.filter;
import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.messaging.constants.MessagingConstants;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.xml.sax.SAXException;

public class MessagingDataFilter implements Filter
{
    private FilterConfig _filterConfig = null;
    private Logger logger = new Logger("com.ftd.messaging.filter.MessagingDataFilter");

    private static final String ADMIN_ACTION = "adminaction";
    private static final String SITE_NAME = "sitename";

    private static final String HEADER_CONTENT_TYPE = "CONTENT-TYPE";
    private static final String MULTPART_FORM_DATA = "multipart/form-data";

    private static final String PROPERTY_FILE = "security-config.xml";

    private static final String ERROR_PAGE = "error_page";


    public void init(FilterConfig filterConfig)
        throws ServletException
    {
        _filterConfig = filterConfig;
    }


    public void destroy()
    {
        _filterConfig = null;
    }


    /**
     * Get shared data.
     *
     *
     * @param ServletRequest  - input request object
     * @param ServletResponse - object used to respond to the user
     * @param FilterChain     - next program to be executed in the chain
     *
     * @throws IOException
     * @throws ServletException
     */
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
        throws IOException, ServletException
    {
        String adminAction = null,
               siteName = null;


        try
        {
            HashMap parameters  = new HashMap();

            if(request.getAttribute(COMConstants.CONS_APP_PARAMETERS)!=null)
            {
              parameters=(HashMap)request.getAttribute(COMConstants.CONS_APP_PARAMETERS);
            }


            String inTest = (request.getParameter(COMConstants.IN_LAST_NAME) != null?request.getParameter(COMConstants.IN_LAST_NAME).toString().trim():"");
            String hashTest = (parameters.get(COMConstants.SC_LAST_NAME) != null?parameters.get(COMConstants.SC_LAST_NAME).toString().trim():"");
            String scTest = (request.getParameter(COMConstants.SC_LAST_NAME) != null?request.getParameter(COMConstants.SC_LAST_NAME).toString().trim():"");



            if(isMultipartFormData(((HttpServletRequest)request )))
            {
                //populate admin and site info only if multiple forms were found
                parameters.put(adminAction,                                 ((request.getParameter(ADMIN_ACTION))                               !=null?request.getParameter(ADMIN_ACTION).toString().trim():""));
                parameters.put(siteName,                                    ((request.getParameter(SITE_NAME))                                  !=null?request.getParameter(SITE_NAME).toString().trim():""));

            }

                //messaging
                this.populateMessagingInfo(request, parameters);


            request.setAttribute(COMConstants.CONS_APP_PARAMETERS, parameters);
            chain.doFilter(request, response);
        }
        catch (Exception ex)
        {
            logger.error(ex);
            try
            {
                redirectToError(response);
            }
            catch (Exception e)
            {
                logger.error(e);
            }
        }
    }


    /**
     * Redirect csr to the error page.
     *
     * @param ServletResponse
     *
     * @throws IOException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws TransformerException
     */
    private void redirectToError(ServletResponse response)
        throws IOException, ParserConfigurationException, SAXException,
               TransformerException
    {
        ((HttpServletResponse) response).sendRedirect(ConfigurationUtil.getInstance().getProperty(PROPERTY_FILE,ERROR_PAGE));
    }


    /**
     * Returns true if request content type is multipart/form-data.
     *
     * @param request HttpServlet request to be analyzed
     * @throws Exception
     */
     public static boolean isMultipartFormData(HttpServletRequest request)
        throws Exception
     {
        boolean isMultipartFormData = false;
        String headerContentType = request.getHeader(HEADER_CONTENT_TYPE);
        if(headerContentType != null  &&  headerContentType.startsWith(MULTPART_FORM_DATA))
        {
            isMultipartFormData = true;
        }

        return isMultipartFormData;
       }




      /**
     * Populate Messaging data
     *
     */
    private void populateMessagingInfo(ServletRequest request, HashMap parameters)
    {
      parameters.put(MessagingConstants.REQUEST_ORDER_DETAIL_ID, ((request.getParameter(MessagingConstants.REQUEST_ORDER_DETAIL_ID))!=null?request.getParameter(MessagingConstants.REQUEST_ORDER_DETAIL_ID).trim():""));
      parameters.put(MessagingConstants.REQUEST_SOURCE_CODE, ((request.getParameter(MessagingConstants.REQUEST_SOURCE_CODE))!=null?request.getParameter(MessagingConstants.REQUEST_SOURCE_CODE).trim():""));
    	parameters.put(MessagingConstants.MESSAGE_TYPE, ((request.getParameter(MessagingConstants.MESSAGE_TYPE))!=null?request.getParameter(MessagingConstants.MESSAGE_TYPE).toString().trim():""));
    	parameters.put(MessagingConstants.MESSAGE_ORDER_NUMBER, ((request.getParameter(MessagingConstants.MESSAGE_ORDER_NUMBER))!=null?request.getParameter(MessagingConstants.MESSAGE_ORDER_NUMBER).trim():""));
    	parameters.put(MessagingConstants.MESSAGE_DETAIL_TYPE, ((request.getParameter(MessagingConstants.MESSAGE_DETAIL_TYPE))!=null?request.getParameter(MessagingConstants.MESSAGE_DETAIL_TYPE).trim():""));

      String orderType=(request.getParameter(MessagingConstants.MESSAGE_ORDER_TYPE))!=null?request.getParameter(MessagingConstants.MESSAGE_ORDER_TYPE).toString().trim():(String)request.getAttribute(MessagingConstants.MESSAGE_ORDER_TYPE);

      parameters.put(MessagingConstants.MESSAGE_ORDER_TYPE, (orderType!=null?orderType.trim():""));

      parameters.put(MessagingConstants.MESSAGE_GUID, ((request.getParameter(MessagingConstants.MESSAGE_GUID))!=null?request.getParameter(MessagingConstants.MESSAGE_GUID).toString().trim():""));
      parameters.put(MessagingConstants.MESSAGE_EXTERNAL_ORDER_NUMBER, ((request.getParameter(MessagingConstants.MESSAGE_EXTERNAL_ORDER_NUMBER))!=null?request.getParameter(MessagingConstants.MESSAGE_EXTERNAL_ORDER_NUMBER).trim():""));
      parameters.put(MessagingConstants.MESSAGE_MASTER_ORDER_NUMBER, ((request.getParameter(MessagingConstants.MESSAGE_MASTER_ORDER_NUMBER))!=null?request.getParameter(MessagingConstants.MESSAGE_MASTER_ORDER_NUMBER).trim():""));
      parameters.put(MessagingConstants.MESSAGE_EMAIL_INDICATOR, ((request.getParameter(MessagingConstants.MESSAGE_EMAIL_INDICATOR))!=null?request.getParameter(MessagingConstants.MESSAGE_EMAIL_INDICATOR).trim():""));
      parameters.put(MessagingConstants.MESSAGE_LETTER_INDICATOR, ((request.getParameter(MessagingConstants.MESSAGE_LETTER_INDICATOR))!=null?request.getParameter(MessagingConstants.MESSAGE_LETTER_INDICATOR).trim():""));
     
      parameters.put(MessagingConstants.MESSAGE_ORDER_STATUS, ((request.getParameter(MessagingConstants.MESSAGE_ORDER_STATUS))!=null?request.getParameter(MessagingConstants.MESSAGE_ORDER_STATUS).trim():""));
      parameters.put(MessagingConstants.REQUEST_PARAMETER_COMP_ORDER, ((request.getParameter(MessagingConstants.REQUEST_PARAMETER_COMP_ORDER))!=null?request.getParameter(MessagingConstants.REQUEST_PARAMETER_COMP_ORDER).trim():""));
      parameters.put(MessagingConstants.SUBSEQUENT_ACTION, ((request.getParameter(MessagingConstants.SUBSEQUENT_ACTION))!=null?request.getParameter(MessagingConstants.SUBSEQUENT_ACTION).trim():""));

      String customerId=request.getParameter(MessagingConstants.MESSAGE_CUSTOMER_ID);
      
      parameters.put(MessagingConstants.MESSAGE_CUSTOMER_ID, (customerId!=null?customerId.trim():""));

      parameters.put(MessagingConstants.FTD_MESSAGE_STATUS, ((request.getParameter(MessagingConstants.FTD_MESSAGE_STATUS))!=null?request.getParameter(MessagingConstants.FTD_MESSAGE_STATUS).trim():""));
      parameters.put(MessagingConstants.FTD_MESSAGE_CANCELLED, ((request.getParameter(MessagingConstants.FTD_MESSAGE_CANCELLED))!=null?request.getParameter(MessagingConstants.FTD_MESSAGE_CANCELLED).trim():""));

      parameters.put(MessagingConstants.FORWARD_DESTINATION, ((request.getParameter(MessagingConstants.FORWARD_DESTINATION))!=null?request.getParameter(MessagingConstants.FORWARD_DESTINATION).trim():""));
      parameters.put(MessagingConstants.REQUEST_QUEUE_MESSAGE_ID, ((request.getParameter(MessagingConstants.REQUEST_QUEUE_MESSAGE_ID))!=null?request.getParameter(MessagingConstants.REQUEST_QUEUE_MESSAGE_ID).trim():""));
      
      parameters.put(MessagingConstants.NEW_FLORIST_ID, ((request.getParameter(MessagingConstants.NEW_FLORIST_ID))!=null?request.getParameter(MessagingConstants.NEW_FLORIST_ID).trim():""));
      parameters.put(MessagingConstants.NEW_MESSAGE_TYPE, ((request.getParameter(MessagingConstants.NEW_MESSAGE_TYPE))!=null?request.getParameter(MessagingConstants.NEW_MESSAGE_TYPE).trim():""));
      parameters.put(MessagingConstants.REQUEST_FLORIST_SELECTION_LOG_ID, ((request.getParameter(MessagingConstants.REQUEST_FLORIST_SELECTION_LOG_ID))!=null?request.getParameter(MessagingConstants.REQUEST_FLORIST_SELECTION_LOG_ID).trim():""));
      parameters.put(MessagingConstants.REQUEST_ZIP_CITY_FLAG, ((request.getParameter(MessagingConstants.REQUEST_ZIP_CITY_FLAG))!=null?request.getParameter(MessagingConstants.REQUEST_ZIP_CITY_FLAG).trim():""));
            
      logger.debug("parameter ="+parameters);

    }

}