package com.ftd.messaging.dao;

import com.ftd.messaging.constants.MessagingConstants;
import com.ftd.messaging.vo.QueueDeleteHeaderStatusVO;
import com.ftd.messaging.vo.QueueSearchVO;
import com.ftd.messaging.vo.QueueVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.QueueDeleteDetailVO;
import com.ftd.osp.utilities.vo.QueueDeleteHeaderVO;

import java.io.IOException;

import java.math.BigDecimal;

import java.sql.Connection;
import java.sql.SQLException;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;



import org.w3c.dom.Document;

import org.xml.sax.SAXException;


/**
 * This Data Access Object retrieves/deletes queues.
 * @author Charles Fox
 * @todo check comments, error handling, and message handling
 */
public class QueueDAO 
{

    private Logger logger = new Logger("com.ftd.messaging.dao.QueueDAO");
    private Connection dbConnection = null;
    private MessagingConstants messagingConstants;
    
    public QueueDAO(Connection conn)
    {
        super();
        dbConnection = conn;
    }

    public Document queryAllQueueTypes() throws SAXException, 
        ParserConfigurationException, IOException, SQLException, Exception
               {
        
        DataRequest request = new DataRequest();
        Document xmlMessages = null;

        try {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
                
            /* build DataRequest object */
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID(messagingConstants.GET_QUEUE_TYPES);
            
            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            xmlMessages = (Document) dau.execute(request);
            //xmlMessages = (Document) outputs.get(messagingConstants.GET_QUEUE_RECORD_BY_ORDER_OUT_CURSOR);

        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting queryAllQueueTypes");
            } 
            
        }
            return xmlMessages;
                  
    }
    
    public Document queryAllMQDQueueTypes() throws SAXException, 
        ParserConfigurationException, IOException, SQLException, Exception
               {
        
        DataRequest request = new DataRequest();
        Document xmlMessages = null;

        try {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
                
            /* build DataRequest object */
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID(messagingConstants.GET_MQD_QUEUE_TYPES);
            
            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            xmlMessages = (Document) dau.execute(request);

        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting queryAllMQDQueueTypes");
            } 
            
        }
            return xmlMessages;
                  
    }
    
     public CachedResultSet queryQueueMessages(QueueSearchVO queueSearchVO)
        throws SAXException, 
        ParserConfigurationException, IOException, SQLException, Exception {
                
        // Build db stored procedure input parms
        // *****  stored procedure takes no input parms  *****
        Map inputParams = new HashMap();
        if (queueSearchVO.getMessageSystem() == null) {
        throw new Exception("queryQueueMessages: Message System input can not be null.");
        }
        inputParams.put("IN_SYSTEM", queueSearchVO.getMessageSystem());
        
        if (queueSearchVO.getQueueType() != null) {
          inputParams.put("IN_QUEUE_TYPE", queueSearchVO.getQueueType());
        }
        
        if(queueSearchVO.getAskAnsCode() != null)
        {
          inputParams.put("IN_ASK_ANS_CODE", queueSearchVO.getAskAnsCode());
        }
        
        
        
        if (queueSearchVO.getMessageDirection() != null) {
        inputParams.put("IN_MESSAGE_DIRECTION", queueSearchVO.getMessageDirection());
        }
        
        if(queueSearchVO.getNonPartner() != null)
        {
          inputParams.put("IN_NON_PARTNER",queueSearchVO.getNonPartner());
        }
        
        if(queueSearchVO.getIncludedProdProperties() != null && queueSearchVO.getIncludedProdProperties().size() > 0)
        {
          logger.info("selected Properties ==>>> "+queueSearchVO.getIncludedProdProperties().toString().substring(1,queueSearchVO.getIncludedProdProperties().toString().length()-1));
          inputParams.put("IN_PRODUCT_PROPERTY_INCL",queueSearchVO.getIncludedProdProperties().toString().substring(1,queueSearchVO.getIncludedProdProperties().toString().length()-1));
        }
        
        if(queueSearchVO.getIncludedPartners() != null && queueSearchVO.getIncludedPartners().size() > 0)
        {
          logger.info("selected Partners ==>>> "+queueSearchVO.getIncludedPartners().toString().substring(1,queueSearchVO.getIncludedPartners().toString().length()-1));
          inputParams.put("IN_PARTNER_INCL",queueSearchVO.getIncludedPartners().toString().substring(1,queueSearchVO.getIncludedPartners().toString().length()-1));
        }
        
        if(queueSearchVO.getCustomerEmail() != null)
        {
          inputParams.put("IN_EMAIL_ADDRESS",queueSearchVO.getCustomerEmail());
        }
        
        if(queueSearchVO.getAmountFrom() != null)
        {
          inputParams.put("IN_AMOUNT_FROM",queueSearchVO.getAmountFrom());
        }
        
        if(queueSearchVO.getAmountTo() != null)
        {
          inputParams.put("IN_AMOUNT_TO",queueSearchVO.getAmountTo());
        }
                     
        if(queueSearchVO.getKeywordSearchText1() != null)
        {
          inputParams.put("IN_SEARCH_TEXT1",queueSearchVO.getKeywordSearchText1());
        }
        
        if(queueSearchVO.getKeywordSearchText2() != null)
        {
          inputParams.put("IN_SEARCH_TEXT2",queueSearchVO.getKeywordSearchText2());
          inputParams.put("IN_SEARCH_COND1",queueSearchVO.getKeywordSearchCondition1());
        }
        
         if(queueSearchVO.getKeywordSearchText3() != null)
        {
          inputParams.put("IN_SEARCH_TEXT3",queueSearchVO.getKeywordSearchText3());
          inputParams.put("IN_SEARCH_COND2",queueSearchVO.getKeywordSearchCondition2());
        }
        
            
        Iterator itr = inputParams.keySet().iterator();
        String str = "";
        while(itr.hasNext())
        {
          str = (String)itr.next();
          logger.info(str+ "===>>> "+inputParams.get(str));
        }
        
        DataRequest request = new DataRequest();
        request.setConnection(dbConnection);
        request.reset();
        request.setInputParams(inputParams);
        request.setStatementID("SEARCH_QUEUE_MESSAGES");

        // Submit the request to the database.
        try {
            // Execute the stored procedure
            DataAccessUtil dau = DataAccessUtil.getInstance();

            return (CachedResultSet) dau.execute(request);
        } catch (SQLException e) {
            String errorMsg = "queryQueueMessages: Failed.";
            logger.error(errorMsg, e);
            throw new Exception(errorMsg, e);
        }
        
    }
    

    
    public CachedResultSet getQueueMessage(String messageId)
        throws SAXException, 
        ParserConfigurationException, IOException, SQLException, Exception {   
        // Build db stored procedure input parms

        Map inputParams = new HashMap(1);
        inputParams.put("IN_MESSAGE_ID", messageId);
        DataRequest request = new DataRequest();
        request.setConnection(dbConnection);
        request.reset();
        request.setInputParams(inputParams);
        request.setStatementID("GET_QUEUE_MESSAGE_BY_ID");

        // Submit the request to the database.
        try {
            // Execute the stored procedure
            DataAccessUtil dau = DataAccessUtil.getInstance();
            return (CachedResultSet) dau.execute(request);
        } catch (SQLException e) {
            String errorMsg = "getQueueMessage: Failed.";
            logger.error(errorMsg, e);
            throw new Exception(errorMsg, e);
        }
    }

  public void reprocessOrder(String orderDetailId)
    throws SAXException, IOException, SQLException, 
        ParserConfigurationException, Exception
    {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap(1);                
            inputParams.put(messagingConstants.REPROCESS_ORDER_IN_ORDER_DETAIL_ID, orderDetailId);
            
            // build DataRequest object
            DataRequest request = new DataRequest();
            request.setConnection(dbConnection); 
            request.reset();          
            request.setInputParams(inputParams);
            request.setStatementID(messagingConstants.REPROCESS_ORDER);

            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            Map outputs = (Map) dau.execute(request);
                String status = (String) outputs.get(
                    messagingConstants.REPROCESS_ORDER_OUT_STATUS);
                if(status.equals("N"))
                {
                    
                    String message = (String) outputs.get(
                        messagingConstants.REPROCESS_ORDER_OUT_MESSAGE);

                       throw new Exception(message);                    
                }
    }

    /**
     * Call stored procedure GET_QUEUE_RECORD_BY_ORDER and return a 
     * Document object that contains a list of queues.
     * @param orderDetailId - String
     * @return Document
     * @throws SAXException
     * @throws IOException
     * @throws SQLException
     * @throws ParserConfigurationException
     * @throws Exception
     * @todo - code and determine exceptions
     */
    public Document getQueues(String orderDetailId) throws SAXException, 
        ParserConfigurationException, IOException, SQLException, Exception
               {
               
       if(logger.isDebugEnabled()){
            logger.debug("Entering getQueues");
            logger.debug("orderDetailId : " + orderDetailId);
        }
        
        DataRequest request = new DataRequest();
        Document xmlMessages = null;

        try {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put(messagingConstants.GET_QUEUE_RECORD_BY_ORDER_IN_ORDER_DETAIL_ID, 
                orderDetailId);
            inputParams.put(messagingConstants.GET_QUEUE_RECORD_BY_ORDER_IN_REQUEST_TYPE, 
                null);
                
            /* build DataRequest object */
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID(messagingConstants.GET_QUEUE_RECORD_BY_ORDER);
            
            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            xmlMessages = (Document) dau.execute(request);
            //xmlMessages = (Document) outputs.get(messagingConstants.GET_QUEUE_RECORD_BY_ORDER_OUT_CURSOR);

        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting getMessages");
            } 
            
        }
            return xmlMessages;
                  
    }

    /**
     * Call stored procedure QUEUE_PKG. DELETE_QUEUE_RECORD  and delete 
     * queue from database.  Return true if the CSR has authority to delete 
     * the given queue. Otherwise, return false.
     * @param queueId - String
     * @param csrId - String
     * @return boolean
     * @throws SAXException
     * @throws IOException
     * @throws SQLException
     * @throws ParserConfigurationException
     * @throws Exception
     * @todo - code and determine exceptions
     */
    public boolean deleteQueue(String queueId, String csrId, String taggedCsrId)
    throws SAXException, IOException, SQLException, 
        ParserConfigurationException, Exception
    {

       if(logger.isDebugEnabled()){
            logger.debug("Entering deleteQueue");
            logger.debug("QueueID : " + queueId);
            logger.debug("CSRID : " + csrId);
            logger.debug("TAGGED CSRID : " + taggedCsrId);
        }
         DataRequest request = new DataRequest();
         boolean csrHasAuthority = true;
        try {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put(
                messagingConstants.DELETE_QUEUE_RECORD_IN_MESSAGE_ID, queueId);
                
            inputParams.put("IN_CSR_ID", csrId);
            
            inputParams.put("IN_TAGGED_CSR_ID", taggedCsrId);
            
            // build DataRequest object
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID(messagingConstants.DELETE_QUEUE_RECORD);

            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            Map outputs = (Map) dau.execute(request);
                String status = (String) outputs.get(
                    messagingConstants.DELETE_QUEUE_RECORD_OUT_STATUS);
                if(status.equals("N"))
                {
                    
                    String message = (String) outputs.get(
                        messagingConstants.DELETE_QUEUE_RECORD_OUT_MESSAGE);
                    if(message.equals(messagingConstants.CSR_NO_DELETE_AUTHROITY)||message.equalsIgnoreCase(messagingConstants.QUEUE_TAGGED_HIGHER_AUTHROITY))
                    {
                        csrHasAuthority=false;   
                    }
                    else
                    {
                       throw new Exception(message); 
                    }
                    
                }
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting deleteQueue");
            } 
        }
        
        return csrHasAuthority;
    }
 
 
 
    /**
     * Call stored procedure to delete queue from queue table.
     * @param queueMessageId - String
     * @return n/a
     * @throws SAXException
     * @throws IOException
     * @throws SQLException
     * @throws ParserConfigurationException
     * @throws Exception
     */
    public void deleteQueue(String queueMessageId, String csrID)
    throws 
        IOException, SAXException, SQLException, ParserConfigurationException, 
        Exception
        {
        if(logger.isDebugEnabled()){
            logger.debug("Entering removeQueue");
            logger.debug("MessageID : " + queueMessageId);
        }
         DataRequest request = new DataRequest();
         
        try {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put(
                MessagingConstants.DELETE_QUEUE_RECORD_NO_AUTH_IN_MESSAGE_ID, queueMessageId);
            inputParams.put(
                MessagingConstants.DELETE_QUEUE_RECORD_NO_AUTH_IN_CSR_ID, csrID);
                
            // build DataRequest object
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            
            // Note that this DB proc will delete the queue message regardless
            // if the message is tagged by a CSR.
            request.setStatementID(MessagingConstants.DELETE_QUEUE_RECORD_NO_AUTH);

            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            Map outputs = (Map) dau.execute(request);
                String status = (String) outputs.get(
                    MessagingConstants.DELETE_QUEUE_RECORD_NO_AUTH_OUT_STATUS);
                if(status.equals("N"))
                {
                    String message = (String) outputs.get(
                        MessagingConstants.DELETE_QUEUE_RECORD_NO_AUTH_OUT_MESSAGE);

                    throw new Exception(message);
                }
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting removeQueue");
            } 
        }       
        
    }

  /**
     * Call stored procedure to get the queue record based off of the external order number.
     * @param externalOrderNumber - String
     * @return n/a
     * @throws Exception
     */
  public CachedResultSet getQueueMessageByExternalOrderNumber(String externalOrderNumber, String queueTypesSelected)
    throws Exception
  {
    // Build db stored procedure input parms

    Map inputParams = new HashMap(1);
    inputParams.put("IN_EXTERNAL_ORDER_NUMBER", externalOrderNumber);
    inputParams.put("IN_QUEUE_TYPES", queueTypesSelected);
    DataRequest request = new DataRequest();
    request.setConnection(dbConnection);
    request.reset();
    request.setInputParams(inputParams);
    request.setStatementID("GET_QUEUE_MESSAGE_BY_EXTERNAL_ORDER_NUMBER");

    // Submit the request to the database.
    try
    {
      // Execute the stored procedure
      DataAccessUtil dau = DataAccessUtil.getInstance();
      return (CachedResultSet) dau.execute(request);
    }
    catch (Exception e)
    {
      String errorMsg = "getQueueMessageByExternalOrderNumber: Failed for " + externalOrderNumber;
      logger.error(errorMsg, e);
      throw new Exception(errorMsg, e);
    }
  }


  /**
     * Call stored procedure to delete selected queue types from queue table based off of the external order number and queue type
     * @param externalOrderNumber - String
     * @return n/a
     * @throws Exception
     */
  public void deleteAllQsByExtOrdNum(String externalOrderNumber, String queueTypesSelected, String csrID)
    throws Exception
  {
    logger.debug("Entering deleteQueueByExtOrdNum - External Order Number = " + externalOrderNumber + 
                 " and queueTypesSelected = " + queueTypesSelected);

    DataRequest request = new DataRequest();

    /* setup store procedure input parameters */
    HashMap inputParams = new HashMap();
    inputParams.put("IN_EXTERNAL_ORDER_NUMBER", externalOrderNumber);
    inputParams.put("IN_QUEUE_TYPES", queueTypesSelected);
    inputParams.put("IN_CSR_ID", csrID);

    // build DataRequest object
    request.setConnection(dbConnection);
    request.reset();
    request.setInputParams(inputParams);

    // Note that this DB proc will delete the queue message regardless
    // if the message is tagged by a CSR.
    request.setStatementID("DELETE_ALL_QUEUE_RECORDS_BY_EXT_ORD_NO_AUTH");

    // get data
    DataAccessUtil dau = DataAccessUtil.getInstance();
    Map outputs = (Map) dau.execute(request);
    String status = (String) outputs.get("OUT_STATUS");
    if (status.equals("N"))
    {
      String message = (String) outputs.get("OUT_MESSAGE");
      throw new Exception(message);
    }

  }


  /**
     * Delete queue records based on order detail id and queue types
     * @param orderDetailId - String
     * @param queueTypesSelected - String - all the queue types selected
     * @return n/a
     * @throws Exception
     */
  public void deleteAllQsByOrdDetId(String orderDetailId, String queueTypesSelected, String csrID)
    throws Exception
  {
    logger.debug("Entering deleteAllQsByOrdDetId - Order Detail Id = " + orderDetailId + " and queueTypesSelected = " + 
                 queueTypesSelected);

    DataRequest request = new DataRequest();

    /* setup store procedure input parameters */
    HashMap inputParams = new HashMap();
    inputParams.put("IN_ORDER_DETAIL_ID", orderDetailId);
    inputParams.put("IN_QUEUE_TYPES", queueTypesSelected);
    inputParams.put("IN_CSR_ID", csrID);

    // build DataRequest object
    request.setConnection(dbConnection);
    request.reset();
    request.setInputParams(inputParams);

    // Note that this DB proc will delete the queue message regardless
    // if the message is tagged by a CSR.
    request.setStatementID("DELETE_ALL_QUEUE_RECORDS_BY_ORD_DET_NO_AUTH");

    // get data
    DataAccessUtil dau = DataAccessUtil.getInstance();
    Map outputs = (Map) dau.execute(request);
    String status = (String) outputs.get("OUT_STATUS");
    if (status.equals("N"))
    {
      String message = (String) outputs.get("OUT_MESSAGE");
      throw new Exception(message);
    }

  }

  public String insertQueueRecord(QueueVO queueRecord) throws Exception{
    DataRequest dataRequest = new DataRequest();
    HashMap outputs = null;

    /* setup stored procedure input parameters */
    Map inputParams = new HashMap();

    inputParams.put("IN_QUEUE_TYPE", queueRecord.getQueueType());
    inputParams.put("IN_MESSAGE_TYPE", queueRecord.getMessageType());
    java.sql.Timestamp sqlTimeStamp = new java.sql.Timestamp(queueRecord.getMessageTimestamp().getTime());
    inputParams.put("IN_MESSAGE_TIMESTAMP", sqlTimeStamp);
    inputParams.put("IN_SYSTEM", queueRecord.getSystem());
    inputParams.put("IN_MERCURY_NUMBER", queueRecord.getMercuryNumber());
    inputParams.put("IN_MASTER_ORDER_NUMBER", queueRecord.getMasterOrderNumber());
    inputParams.put("IN_ORDER_GUID", queueRecord.getOrderGuid());
    inputParams.put("IN_ORDER_DETAIL_ID", queueRecord.getOrderDetailId());
    inputParams.put("IN_EXTERNAL_ORDER_NUMBER", queueRecord.getExternalOrderNumber());
    inputParams.put("IN_POINT_OF_CONTACT_ID", queueRecord.getPointOfContactId());
    inputParams.put("IN_EMAIL_ADDRESS", queueRecord.getEmailAddress());
    inputParams.put("IN_MERCURY_ID", queueRecord.getMercuryId());

    /* build DataRequest object */
    dataRequest.setConnection(dbConnection);
    dataRequest.setStatementID("INSERT_QUEUE_RECORD");
    dataRequest.setInputParams(inputParams);

    /* execute the store prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    outputs = (HashMap) dataAccessUtil.execute(dataRequest);

    /* read stored procedure output parameters to determine
     * if the procedure executed successfully */
    String status = (String) outputs.get("OUT_STATUS");
    if (status.equals("N"))
    {
      String message = (String) outputs.get("OUT_MESSAGE");
      throw new Exception(message);
    }
    
    String newQueueId = (String) outputs.get("OUT_MESSAGE_ID"); 

    return newQueueId; 

    }
    
    
    public Long insertQueueDeleteHeaderRecord(QueueDeleteHeaderVO queueDeleteHeaderVO) throws Exception
    {
    
      DataRequest dataRequest = new DataRequest();
      
      Map outputs = null;

      /* setup stored procedure input parameters */
      Map inputParams = new HashMap();
      inputParams.put("IN_BATCH_DESCRIPTION",queueDeleteHeaderVO.getBatchDescription());
      inputParams.put("IN_BATCH_COUNT",queueDeleteHeaderVO.getBatchCount());
      inputParams.put("IN_BATCH_PROCESSED_FLAG",queueDeleteHeaderVO.getBatchProcessedFlag());
      inputParams.put("IN_DELETE_QUEUE_MSG_FLAG",queueDeleteHeaderVO.getDeleteQueueMsgFlag());
      inputParams.put("IN_DELETE_QUEUE_MSG_TYPE",queueDeleteHeaderVO.getDeleteQueueMsgType());
      inputParams.put("IN_SEND_MESSAGE_FLAG",queueDeleteHeaderVO.getSendMessageFlag());
      inputParams.put("IN_MESSAGE_TYPE",queueDeleteHeaderVO.getMessageType());
      inputParams.put("IN_ANSP_AMOUNT",queueDeleteHeaderVO.getAnspAmount());
      inputParams.put("IN_GIVE_FLORIST_ASKING_PRICE",queueDeleteHeaderVO.getGiveFloristAskingPrice());
      inputParams.put("IN_CANCEL_REASON_CODE",queueDeleteHeaderVO.getCancelReasonCode());
      inputParams.put("IN_TEXT_OR_REASON",queueDeleteHeaderVO.getTextOrReason());
      inputParams.put("IN_RESUBMIT_ORDER_FLAG",queueDeleteHeaderVO.getResubmitOrderFlag());
      inputParams.put("IN_INCLUDE_COMMENT_FLAG",queueDeleteHeaderVO.getIncludeCommentFlag());
      inputParams.put("IN_COMMENT_TEXT",queueDeleteHeaderVO.getCommentText());
      inputParams.put("IN_SEND_EMAIL_FLAG",queueDeleteHeaderVO.getSendEmailFlag());
      inputParams.put("IN_EMAIL_TITLE",queueDeleteHeaderVO.getEmailTitle());
      inputParams.put("IN_EMAIL_SUBJECT",queueDeleteHeaderVO.getEmailSubject());
      inputParams.put("IN_EMAIL_BODY",queueDeleteHeaderVO.getEmailBody());
      inputParams.put("IN_REFUND_ORDER_FLAG",queueDeleteHeaderVO.getRefundOrderFlag());
      inputParams.put("IN_REFUND_DISP_CODE",queueDeleteHeaderVO.getRefundDispCode());
      inputParams.put("IN_RESPONSIBLE_PARTY",queueDeleteHeaderVO.getResponsibleParty());
      inputParams.put("IN_DELETE_BATCH_BY_TYPE",queueDeleteHeaderVO.getDeleteBatchByType());
      inputParams.put("IN_CREATED_BY",queueDeleteHeaderVO.getCreatedBy());
      inputParams.put("IN_INCLUDE_PARTNER_NAME",queueDeleteHeaderVO.getIncludePartnerName());
      inputParams.put("IN_EXCLUDE_PARTNER_NAME",queueDeleteHeaderVO.getExcludePartnerName());
      /* build DataRequest object */
      dataRequest.setConnection(dbConnection);
      dataRequest.setStatementID("INSERT_QUEUE_DELETE_HEADER");
      dataRequest.setInputParams(inputParams);

      /* execute the store prodcedure */
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      outputs = (Map) dataAccessUtil.execute(dataRequest);
      String status = (String) outputs.get("OUT_STATUS");
      if (status.equals("N"))
      {
        String message = (String) outputs.get("OUT_MESSAGE");
        throw new Exception(message);
      }
      BigDecimal queueDeleteHdrId = ((BigDecimal)outputs.get("OUT_QUEUE_DELETE_HEADER_ID"));
      
      return queueDeleteHdrId.longValue();
      
    }
    
    public Long insertDeleteDetailRecord(QueueDeleteDetailVO queueDeleteDetailVO) throws Exception
    {
      DataRequest dataRequest = new DataRequest();
      Map outputs = null;
       
      /* setup stored procedure input parameters */
      Map inputParams = new HashMap();
      inputParams.put("IN_QUEUE_DELETE_HEADER_ID",new Long(queueDeleteDetailVO.getQueueDeleteHeaderId()));
      if(queueDeleteDetailVO.getMessageId() != null)
        inputParams.put("IN_MESSAGE_ID",new Long(queueDeleteDetailVO.getMessageId()));
      else
        inputParams.put("IN_MESSAGE_ID",new Long(0));
      if(queueDeleteDetailVO.getExternalOrderNumber() != null)
        inputParams.put("IN_EXTERNAL_ORDER_NUMBER",queueDeleteDetailVO.getExternalOrderNumber().toString());
      
      
      inputParams.put("IN_PROCESSED_STATUS",queueDeleteDetailVO.getProcessedStatus());
      inputParams.put("IN_ERROR_TEXT",queueDeleteDetailVO.getErrorText());
      inputParams.put("IN_CREATED_BY",queueDeleteDetailVO.getCreatedBy());
      
            /* build DataRequest object */
      dataRequest.setConnection(dbConnection);
      dataRequest.setStatementID("INSERT_QUEUE_DELETE_DETAIL");
      dataRequest.setInputParams(inputParams);

      /* execute the store prodcedure */
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      outputs = (Map) dataAccessUtil.execute(dataRequest);
      String status = (String) outputs.get("OUT_STATUS");
      if (status.equals("N"))
      {
        String message = (String) outputs.get("OUT_MESSAGE");
        throw new Exception(message);
      }
       BigDecimal queueDeleteDetailId = ((BigDecimal)outputs.get("OUT_QUEUE_DELETE_DETAIL_ID"));
       return queueDeleteDetailId.longValue();
    }
    
    
    /**
        * This method will be called by getQueueDeleteHeaderStatusByUserId() of QueueBO by passing userId
        * Loads all the batch data available for last 2 days for the specified user.
        * @param - String userId
        * @param - Long queueDeleteHeaderId
        * @return - List<QueueDeleteHeaderStatusVO>
        * @throws - Exception
        */
        public List<QueueDeleteHeaderStatusVO> getQueueDeleteHeaderStatus(String userId, Long queueDeleteHeaderId) throws Exception{
            //Define and set input params into a Map
            Map inputs = new HashMap();
            SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String sCreatedOn = null;
            inputs.put("IN_USER_ID", userId);
            /*
             * Since there are glitches in the framework 
             * No java.lang.Long is allowed
             * Also primitive data type long with null value is not handled
             */
            inputs.put("IN_QUEUE_DELETE_HEADER_ID", queueDeleteHeaderId==null?0:queueDeleteHeaderId);
            
            //Craete a data request and set the connection, statement Id, input parama 
            DataRequest request = new DataRequest();
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputs);
            request.setStatementID(MessagingConstants.GET_QUEUE_DELETE_HEADER_WITH_COUNT);
            
            //Get the dataAccessUtil instance
            DataAccessUtil daUtil = DataAccessUtil.getInstance();
            
            //Get the cachedresultset and return null if the results size is 0
            CachedResultSet results = (CachedResultSet) daUtil.execute(request);
            if(results.getRowCount()==0){
                return null;
            }
            
            //Now create a ArrayList to load data and return
            List<QueueDeleteHeaderStatusVO> headers = new ArrayList();
            
            /*
             * Loop through the cachedresultset and rettrieve following fields
                QUEUE_DELETE_HEADER_ID, BATCH_DESCRIPTION, BATCH_COUNT, 
                TOTAL_PROCESSED, TOTAL_SUCCESS, TOTAL_FAILURE, TOTAL_DUPLICATE
             */
             while(results.next()){
                 QueueDeleteHeaderStatusVO statusVO = new QueueDeleteHeaderStatusVO();    
                
                 if (results.getObject("QUEUE_DELETE_HEADER_ID") == null)
                     continue;
                
                 statusVO.setQueueDeleteHeaderId(results.getLong("QUEUE_DELETE_HEADER_ID"));
                 
                 if (results.getObject("BATCH_DESCRIPTION") != null)
                     statusVO.setBatchDescription(results.getString("BATCH_DESCRIPTION"));
                
                 if (results.getObject("BATCH_COUNT") != null)
                     statusVO.setBatchCount(new Integer(results.getInt("BATCH_COUNT")));
                
                 if (results.getObject("CREATED_ON") != null)
                 {
                     sCreatedOn = results.getString("CREATED_ON");
                     statusVO.setCreatedOn(inputFormat.parse(sCreatedOn));
                 }
                      
                 if (results.getObject("CREATED_BY") != null)
                     statusVO.setCreatedBy(results.getString("CREATED_BY"));
                      
                 if (results.getObject("TOTAL_PROCESSED") != null)
                     statusVO.setTotalProcessed(new Integer(results.getInt("TOTAL_PROCESSED")));
                 else
                     statusVO.setTotalProcessed(new Integer(0));
                     
                 if (results.getObject("TOTAL_SUCCESS") != null)
                     statusVO.setTotalSuccess(new Integer(results.getInt("TOTAL_SUCCESS")));
                 else
                     statusVO.setTotalSuccess(new Integer(0));
                     
                 if (results.getObject("TOTAL_FAILURE") != null)
                     statusVO.setTotalFailure(new Integer(results.getInt("TOTAL_FAILURE")));
                 else
                     statusVO.setTotalFailure(new Integer(0));
                     
                 if (results.getObject("TOTAL_DUPLICATE") != null)
                     statusVO.setTotalDuplicate(new Integer(results.getInt("TOTAL_DUPLICATE")));
                 else
                     statusVO.setTotalDuplicate(new Integer(0));
                     
                 headers.add(statusVO);
             }
            return   headers;
        }
      
        /**
        * This method will be called by getQueueDeleteHeaderStatusByHeaderId() of QueueBO by passing queueDeleteHeaderId
        * Loads the batch data for the specified queueDeleteHeaderId. 
        * @param - String userId
        * @param - Long queueDeleteHeaderId
        * @return - QueueDeleteHeaderStatusVO
        * @throws - Exception
        */
        public QueueDeleteHeaderStatusVO getQueueDeleteHeaderWithDetails(String userId, Long queueDeleteHeaderId) throws Exception{
            QueueDeleteHeaderStatusVO headerStatus = new QueueDeleteHeaderStatusVO();
            //Define and set input params into a Map
            Map inputs = new HashMap();
            SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String sCreatedOn = null;
            inputs.put("IN_USER_ID", userId);
            inputs.put("IN_QUEUE_DELETE_HEADER_ID", queueDeleteHeaderId);
            
            //Craete a data request and set the connection, statement Id, input parama 
            DataRequest request = new DataRequest();
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputs);
            request.setStatementID(MessagingConstants.GET_QUEUE_DELETE_HEADER_WITH_DETAILS);
            
            //Get the dataAccessUtil instance
            DataAccessUtil daUtil = DataAccessUtil.getInstance();
            Map results = (Map)daUtil.execute(request);
            
            //Return null if the results is empty
            if(results.isEmpty())
                return null;
                
            CachedResultSet headers = (CachedResultSet)results.get("OUT_HEADER");
            CachedResultSet details = (CachedResultSet)results.get("OUT_DETAIL");
            
            //Get the Header cachedresultset and return null if the size is 0
            if(headers.getRowCount()==0){
              return null;
            }
          
            //Now create a QueueDeleteHeaderStatusVO to load data and return
            QueueDeleteHeaderStatusVO statusVO = new QueueDeleteHeaderStatusVO();    
            
            /*
             * Loop through the headers cachedresultset and rettrieve following fields
             * QUEUE_DELETE_HEADER_ID, BATCH_DESCRIPTION, BATCH_COUNT, 
             * CREATED_ON and CREATED_BY
             */
            while(headers.next()){
                if (headers.getObject("QUEUE_DELETE_HEADER_ID") != null)
                  statusVO.setQueueDeleteHeaderId(headers.getLong("QUEUE_DELETE_HEADER_ID"));
                
                if (headers.getObject("BATCH_DESCRIPTION") != null)
                  statusVO.setBatchDescription(headers.getString("BATCH_DESCRIPTION"));
                
                if (headers.getObject("BATCH_COUNT") != null)
                   statusVO.setBatchCount(new Integer(headers.getInt("BATCH_COUNT")));
                  
                if (headers.getObject("CREATED_ON") != null)
                {
                   sCreatedOn = headers.getString("CREATED_ON");
                   statusVO.setCreatedOn(inputFormat.parse(sCreatedOn));
                }
                      
                if (headers.getObject("CREATED_BY") != null)
                    statusVO.setCreatedBy(headers.getString("CREATED_BY"));
            }
          
             //return null if the statusVO is null
             if(statusVO == null)
                return null;
            
             List<QueueDeleteDetailVO> detailsList = statusVO.getQueueDeleteDetailVOList();
             if(detailsList == null)
                 detailsList = new ArrayList();
             
           /*
            * Loop through the details cachedresultset and rettrieve following fields
            * MESSAGE_ID, EXTERNAL_ORDER_NUMBER, PROCESSED_STATUS, ERROR_TEXT
            */
            while(details.next()){
                QueueDeleteDetailVO detailVO = new QueueDeleteDetailVO();
                if (details.getObject("MESSAGE_ID") != null)
                    detailVO.setMessageId(details.getLong("MESSAGE_ID"));
                
                if (details.getObject("EXTERNAL_ORDER_NUMBER") != null)
                    detailVO.setExternalOrderNumber(details.getString("EXTERNAL_ORDER_NUMBER"));
                
                detailVO.setProcessedStatus(details.getString("PROCESSED_STATUS"));
                detailVO.setErrorText(details.getString("ERROR_TEXT"));
                detailsList.add(detailVO);
            }
            statusVO.setQueueDeleteDetailVOList(detailsList);
        return   statusVO;
      }
      
     /**
         * This method will be called by getUsersFromQueueDeleteHeader() of QueueBO.
         * Loads all the users that submitted batch for last 2 days from queue delete header table.
         * @return - List<String>
         * @throws - Exception
         */
         public List<String> getUsersFromQueueDeleteHeader() throws Exception{
             
             //Craete a data request and set the connection, statement Id, input parama 
             DataRequest request = new DataRequest();
             request.setConnection(dbConnection);
             request.reset();
             request.setStatementID(MessagingConstants.GET_USERS_FROM_QUEUE_DELETE_HEADER);
             
             //Get the dataAccessUtil instance
             DataAccessUtil daUtil = DataAccessUtil.getInstance();
             
             //Get the cachedresultset and return null if the results size is 0
             CachedResultSet results = (CachedResultSet) daUtil.execute(request);
             if(results.getRowCount()==0){
                 return null;
             }
             
             //Now create a ArrayList to load data and return
             List<String> users = new ArrayList();
             users.add(MessagingConstants.ALL_OPTION);
             /*
              * Loop through the cachedresultset and retrieve CREATED_BY 
              */
              while(results.next()){
                 if (results.getObject("CREATED_BY") != null)
                     users.add(results.getString("CREATED_BY"));
                  
              }
             return users;
         }
         
    public String getExtOrdNumByMsgId(String messageId) throws SAXException, IOException, SQLException, 
        ParserConfigurationException, Exception {   
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(dbConnection);
        dataRequest.setStatementID("GET_EXT_ORD_NUM_BY_MSG_ID");
        dataRequest.addInputParam("IN_MESSAGE_ID", messageId);
       
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

        String externalOrderNumber = (String) dataAccessUtil.execute(dataRequest);
          
        return externalOrderNumber;
    }
    
}
