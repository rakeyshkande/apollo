package com.ftd.messaging.dao;


import java.io.IOException;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.ftd.messaging.constants.MessagingConstants;
import com.ftd.messaging.vo.CallOutInfo;
import com.ftd.messaging.vo.FTDMessageVO;
import com.ftd.messaging.vo.FloristStatusVO;
import com.ftd.messaging.vo.MercuryVO;
import com.ftd.messaging.vo.MessageOrderStatusVO;
import com.ftd.messaging.vo.MessageVO;
import com.ftd.messaging.vo.SDSTransactionVO;
import com.ftd.messaging.vo.VenusVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * This data access object retrieves/stores message/order related information.
 * @author Charles Fox
 * @todo check comments, error handling, and message handling
 */
public class MessageDAO 
{
    private Logger logger = new Logger("com.ftd.messaging.dao.MessageDAO");
    private Connection dbConnection = null;
    
    
    public MessageDAO(Connection conn)
    {
        super();
        dbConnection = conn;
    }

    
    /**
     * Call stored procedure ORDER_MESG_PKG.GET_COMMUNICATION_MESSAGES and 
     * return Mercury/Venus Messages and emails/letters for the given 
     * order detail ID.
     * @param orderDetailId - String
     * @param startingNumber - int
     * @param maxNumber - int
     * @param maxCommentLen - String
     * @param maxEmailLen - String
     * @return HashMap
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     * @throws Exception
     */
    public HashMap getMessages(String orderDetailId, int startingNumber, 
        int maxNumber, String maxCommentLen, String maxEmailLen) 
        throws SAXException, ParserConfigurationException, IOException, 
               SQLException, Exception
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering getMessages");
            logger.debug("orderDetailId : " + orderDetailId);
            logger.debug("startingNumber : " + startingNumber);
            logger.debug("maxNumber : " + maxNumber);
        }
         DataRequest request = new DataRequest();
         Document xmlMessages = null;
         HashMap outputs = new HashMap();
         
        try {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put(MessagingConstants.GET_COMMUNICATION_MESSAGES_IN_ORDER_DETAIL_ID, 
                new Long(orderDetailId));
            inputParams.put(MessagingConstants.GET_COMMUNICATION_MESSAGES_IN_START_POSITION, 
                new Long(startingNumber));
            inputParams.put(MessagingConstants.GET_COMMUNICATION_MESSAGES_IN_MAX_NUMBER_RETURNED, 
                new Long(maxNumber));
            inputParams.put("IN_MAX_COMMENT_LEN_RETURNED", new Long(maxCommentLen));
            inputParams.put("IN_MAX_EMAIL_LEN_RETURNED", new Long(maxEmailLen));

            /* build DataRequest object */
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID(MessagingConstants.GET_COMMUNICATION_MESSAGES);

            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            outputs = (HashMap) dau.execute(request);
               
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting getMessages");
            } 
            
        }
            return outputs;
                  
    }
    
    /**
     * Call stored procedure ORDER_MESG_PKG. GET_COMM_ITEM_IN_QUEUE and 
     * return the queues and queue tags for the given order if it exists 
     * in the credit, zip or order queues.
     * @param orderDetailId - String
     * @return Document - queue records
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     * @throws Exception
     */
    public Document getQueues(String orderDetailId) throws SAXException,
        ParserConfigurationException, IOException, SQLException, Exception
        
    {
        
         if(logger.isDebugEnabled()){
            logger.debug("Entering getQueues");
            logger.debug("orderDetailId : " + orderDetailId);
        }
         DataRequest request = new DataRequest();
         Document xmlQueues = null;
         
        try {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put(MessagingConstants.GET_COMM_ITEM_IN_QUEUE_IN_ORDER_DETAIL_ID, 
                new Long(orderDetailId));

            /* build DataRequest object */
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID(MessagingConstants.GET_COMM_ITEM_IN_QUEUE);

            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            xmlQueues = (Document) dau.execute(request);

        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting getQueues");
            } 
           
        }   

         return xmlQueues;
    }

    
    /**
     * Call stored procedure ORDER_MESG_PKG.GET_FLORIST_DASHBOARD 
     * and return info required for florist dashboard based on 
     * order detail ID, zip code, and product id.
     * @param orderDetailId - String
     * @return Document
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     * @throws Exception
     */
    public Document getFloristDashboard(String orderDetailId) 
        throws SAXException,
        ParserConfigurationException, IOException, SQLException, Exception
        {
    
         if(logger.isDebugEnabled()){
            logger.debug("Entering getFloristDashboard");
            logger.debug("orderDetailId : " + orderDetailId);
        }
         DataRequest request = new DataRequest();
         Document xmlFloristDashboard = null;

        try {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put(MessagingConstants.GET_FLORIST_DASHBOARD_IN_QUEUE_IN_ORDER_DETAIL_ID, 
                new Long(orderDetailId));
                
            /* build DataRequest object */
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID(MessagingConstants.GET_FLORIST_DASHBOARD);

            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            xmlFloristDashboard = (Document) dau.execute(request);

        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting getFloristDashboard");
            } 
           
        }   

         return xmlFloristDashboard;
    }

    /**
     * Call stored procedure ???? and return Message Value Object based 
     * on message id and message type.
     * @param messageId - String
     * @param messageType - String
     * @return MessageVO
     * @throws IOException
     * @throws SAXException
     * @throws SQLException
     * @throws ParserConfigurationException
     * @throws Exception
     */
    public MessageVO getMessage(String messageId, String messageType)
        throws IOException, SAXException, SQLException, 
            ParserConfigurationException, Exception
    {            
        if(logger.isDebugEnabled()) {
            logger.debug("Entering getMessage");
            logger.debug("messageId : " + messageId);
            logger.debug("messageType : " + messageType);
        }
        
         DataRequest request = new DataRequest();
         MessageVO messageVO = new MessageVO();
         CachedResultSet messageRS=null;
        try {

            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put(
                MessagingConstants.GET_MESSAGE_DETAIL_IN_MESSAGE_ID, messageId);
            inputParams.put(
                MessagingConstants.GET_MESSAGE_DETAIL_IN_MESSAGE_TYPE, messageType);
            
            /* build DataRequest object */
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID(MessagingConstants.GET_MESSAGE_DETAIL);
 
           /* execute the store prodcedure */
            DataAccessUtil dau = DataAccessUtil.getInstance();
            messageRS = (CachedResultSet) dau.execute(request);
            SimpleDateFormat sdfOutput = 
                       new SimpleDateFormat (MessagingConstants.MESSAGE_DATE_FORMAT);
            
            String textDate = "";
            java.util.Date utilDate=null;
            if(messageRS.next())
            {  
                if(messageRS.getDate("delivery_date")!=null){
                  textDate = sdfOutput.format(messageRS.getDate("delivery_date"));
                  utilDate = sdfOutput.parse( textDate );
                  messageVO.setDeliveryDate(utilDate);
                }
                if(messageRS.getDate("ship_date")!=null){
                  textDate = sdfOutput.format(messageRS.getDate("ship_date"));
                  utilDate = sdfOutput.parse( textDate );
                  messageVO.setShipDate(utilDate);
                }
                messageVO.setAdjustmentReasonCode(messageRS.getString("adj_reason_code"));
                messageVO.setAdjustmentReasonDesc(messageRS.getString("adj_reason_desc"));
                messageVO.setCancelReasonCode(messageRS.getString("cancel_reason_code"));
                messageVO.setCancelReasonDesc(messageRS.getString("cancel_reason_desc"));
                messageVO.setCardMessage(messageRS.getString("card_message"));
                messageVO.setComments(messageRS.getString("comments"));
                messageVO.setCurrentPrice(messageRS.getDouble("price"));
                String delText=messageRS.getString("delivery_date_text");
                //there are two different date format in this field, we need to display the same format: MON DD- MON 
                if(delText!=null&&delText.indexOf("/")!=-1)
                {
                  SimpleDateFormat newDelDateFormat = new SimpleDateFormat ("MMM dd");
                  SimpleDateFormat oldDelDateFormat = new SimpleDateFormat ("MM/dd/yy");
                  try
                  {
                    delText=newDelDateFormat.format(oldDelDateFormat.parse(delText));
                  }catch(Exception e)
                  {
                    logger.error("can't parse delivery data text "+delText);
                  }
                  
                }
                if(delText!=null){
                  messageVO.setDeliveryText(delText.toUpperCase());
                }
                messageVO.setDetailType(messageRS.getString("msg_type"));
                messageVO.setFillingFloristCode(messageRS.getString("filling_florist"));
                messageVO.setFirstChoice(messageRS.getString("first_choice"));
                messageVO.setIndicator(messageRS.getString("message_direction"));
                messageVO.setMessageId(messageId);
                messageVO.setMessageOrderId(messageRS.getString("message_order_number"));
                messageVO.setOccasionCode(messageRS.getString("occasion"));
                messageVO.setOccasionDesc(messageRS.getString("occasion"));
                messageVO.setOldPrice(messageRS.getDouble("old_price"));
                messageVO.setOperator(messageRS.getString("operator"));
                if(messageRS.getDate("order_date")!=null){
                  textDate = sdfOutput.format(messageRS.getDate("order_date"));
                  utilDate = sdfOutput.parse( textDate );
                  messageVO.setOrderDate(utilDate);
                }
                messageVO.setOrderDetailId(messageRS.getString("order_detail_id"));
                messageVO.setPriority(messageRS.getString("priority"));
                messageVO.setRecipientCityStateZip(messageRS.getString("city_state_zip"));
                messageVO.setRecipientName(messageRS.getString("recipient"));
                messageVO.setRecipientPhone(messageRS.getString("phone_number"));
                messageVO.setRecipientStreet(messageRS.getString("address"));
                messageVO.setSendingFloristCode(messageRS.getString("sending_florist"));
                messageVO.setSpecialInstruction(messageRS.getString("special_instructions"));
                               
                messageVO.setStatusDate(messageRS.getTimestamp("status_date"));
                          
                messageVO.setStatusDateText();
                messageVO.setStatus(messageRS.getString("verified"));
                logger.debug("message status="+messageVO.getStatus());
                
                messageVO.setCombineRptNum(messageRS.getString("combined_report_number"));
                messageVO.setSubstitution(messageRS.getString("second_choice"));
                messageVO.setType(messageRS.getString("detail_type"));
                messageVO.setSubType(messageRS.getString("sub_type"));
                
                messageVO.setVerified(messageRS.getString("verified"));
                messageVO.setWithinMessageWindow(flagToBoolean(messageRS.getString("within_message_window")));
                messageVO.setWineDotComProduct(flagToBoolean(messageRS.getString("ftp_vendor_product")));
                messageVO.setOrderDateText(messageRS.getString("order_date_text"));
                messageVO.setFTDMessageID(messageRS.getString("ftd_message_id"));
                messageVO.setMessageStatus(messageRS.getString("mercury_status"));
                messageVO.setFtdCancelledOrRejected(flagToBoolean(messageRS.getString("can_rej_flag")));
                /* do setIsSubsequentMessageAllowed last, it needs other variables set*/
                messageVO.setIsSubsequentMessageAllowed();
                messageVO.setOverUnderCharge(messageRS.getDouble("over_under_charge"));
                messageVO.setShippingSystem(messageRS.getString("shipping_system"));

                if(messageRS.getDate("zone_jump_label_date")!=null)
                {
                  textDate = sdfOutput.format(messageRS.getDate("zone_jump_label_date"));
                  utilDate = sdfOutput.parse( textDate );
                  messageVO.setZoneJumpLabelDate(utilDate);
                }
                messageVO.setZoneJumpFlag(flagToBoolean(messageRS.getString("zone_jump_flag")));
                messageVO.setZoneJumpTrailerNumber(messageRS.getString("zone_jump_trailer_number"));
                messageVO.setOrderAddOns(messageRS.getString("order_add_ons"));
                
                textDate = new SimpleDateFormat("EEEE, MMMMM dd").format(messageRS.getDate("delivery_date"));
                messageVO.setDeliveryText(textDate);
                textDate = new SimpleDateFormat("EEEE, MMMMM dd").format(messageRS.getDate("order_date"));
                messageVO.setOrderDateText(textDate);

            }

        }finally {
            
            
            if(logger.isDebugEnabled()){
               logger.debug("Exiting getMessage");
            } 
        }
        
        return messageVO;

    }


    /**
     * This is copy of getMessage that calls a slightly different stored
		 * procedure to obtain message data for a GEN message.  Building of the 
		 * MessageVO could have been pulled out into a single method of which 
		 * getMessage and getGENMessage could have utilized but seemed risky during 
		 * a production fix scenario.  The decision was made not to pull this logic
		 * into a single method.
		 * 
		 * Call stored procedure and return Message Value Object based 
     * on message id and message type for a GEN message.
     * @param messageNum - String
     * @param messageType - String
     * @return MessageVO
     * @throws IOException
     * @throws SAXException
     * @throws SQLException
     * @throws ParserConfigurationException
     * @throws Exception
     */
    public MessageVO getGENMessage(String messageNum, String messageType)
        throws IOException, SAXException, SQLException, 
            ParserConfigurationException, Exception
    {            
        if(logger.isDebugEnabled()) {
            logger.debug("Entering getGENMessage");
            logger.debug("messageNum : " + messageNum);
            logger.debug("messageType : " + messageType);
        }
        
         DataRequest request = new DataRequest();
         MessageVO messageVO = new MessageVO();
         CachedResultSet messageRS=null;
        try {

            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put(
                MessagingConstants.GET_MESSAGE_DETAIL_IN_MESSAGE_NUMBER, messageNum);
            inputParams.put(
                MessagingConstants.GET_MESSAGE_DETAIL_IN_MESSAGE_TYPE, messageType);
            
            /* build DataRequest object */
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID(MessagingConstants.GET_MESSAGE_DETAIL_GEN_MESG);
 
           /* execute the store prodcedure */
            DataAccessUtil dau = DataAccessUtil.getInstance();
            messageRS = (CachedResultSet) dau.execute(request);
            SimpleDateFormat sdfOutput = 
                       new SimpleDateFormat (MessagingConstants.MESSAGE_DATE_FORMAT);
            
            String textDate = "";
            java.util.Date utilDate=null;
            if(messageRS.next())
            {  
                if(messageRS.getDate("delivery_date")!=null){
                  textDate = sdfOutput.format(messageRS.getDate("delivery_date"));
                  utilDate = sdfOutput.parse( textDate );
                  messageVO.setDeliveryDate(utilDate);
                }
                messageVO.setAdjustmentReasonCode(messageRS.getString("adj_reason_code"));
                messageVO.setAdjustmentReasonDesc(messageRS.getString("adj_reason_desc"));
                messageVO.setCancelReasonCode(messageRS.getString("cancel_reason_code"));
                messageVO.setCancelReasonDesc(messageRS.getString("cancel_reason_desc"));
                messageVO.setCardMessage(messageRS.getString("card_message"));
                messageVO.setComments(messageRS.getString("comments"));
                messageVO.setCurrentPrice(messageRS.getDouble("price"));
                String delText=messageRS.getString("delivery_date_text");
                //there are two different date format in this field, we need to display the same format: MON DD- MON 
                if(delText!=null&&delText.indexOf("/")!=-1)
                {
                  SimpleDateFormat newDelDateFormat = new SimpleDateFormat ("MMM dd");
                  SimpleDateFormat oldDelDateFormat = new SimpleDateFormat ("MM/dd/yy");
                  try
                  {
                    delText=newDelDateFormat.format(oldDelDateFormat.parse(delText));
                  }catch(Exception e)
                  {
                    logger.error("can't parse delivery data text "+delText);
                  }
                  
                }
                if(delText!=null){
                  messageVO.setDeliveryText(delText.toUpperCase());
                }
                messageVO.setDetailType(messageRS.getString("msg_type"));
                messageVO.setFillingFloristCode(messageRS.getString("filling_florist"));
                messageVO.setFirstChoice(messageRS.getString("first_choice"));
                messageVO.setIndicator(messageRS.getString("message_direction"));
                messageVO.setMessageId(messageNum);
                messageVO.setMessageOrderId(messageRS.getString("message_order_number"));
                messageVO.setOccasionCode(messageRS.getString("occasion"));
                messageVO.setOccasionDesc(messageRS.getString("occasion"));
                messageVO.setOldPrice(messageRS.getDouble("old_price"));
                messageVO.setOperator(messageRS.getString("operator"));
                if(messageRS.getDate("order_date")!=null){
                  textDate = sdfOutput.format(messageRS.getDate("order_date"));
                  utilDate = sdfOutput.parse( textDate );
                }
                messageVO.setOrderDate(utilDate);
                messageVO.setOrderDetailId(messageRS.getString("order_detail_id"));
                messageVO.setPriority(messageRS.getString("priority"));
                messageVO.setRecipientCityStateZip(messageRS.getString("city_state_zip"));
                messageVO.setRecipientName(messageRS.getString("recipient"));
                messageVO.setRecipientPhone(messageRS.getString("phone_number"));
                messageVO.setRecipientStreet(messageRS.getString("address"));
                messageVO.setSendingFloristCode(messageRS.getString("sending_florist"));
                messageVO.setSpecialInstruction(messageRS.getString("special_instructions"));
                               
                messageVO.setStatusDate(messageRS.getTimestamp("status_date"));
                          
                messageVO.setStatusDateText();
                messageVO.setStatus(messageRS.getString("verified"));
                logger.debug("message status="+messageVO.getStatus());
                
                messageVO.setCombineRptNum(messageRS.getString("combined_report_number"));
                messageVO.setSubstitution(messageRS.getString("second_choice"));
                messageVO.setType(messageRS.getString("detail_type"));
                messageVO.setSubType(messageRS.getString("sub_type"));
                
                messageVO.setVerified(messageRS.getString("verified"));
                messageVO.setWithinMessageWindow(flagToBoolean(messageRS.getString("within_message_window")));
                messageVO.setWineDotComProduct(flagToBoolean(messageRS.getString("ftp_vendor_product")));
                messageVO.setOrderDateText(messageRS.getString("order_date_text"));
                messageVO.setFTDMessageID(messageRS.getString("ftd_message_id"));
                messageVO.setMessageStatus(messageRS.getString("mercury_status"));
                messageVO.setFtdCancelledOrRejected(flagToBoolean(messageRS.getString("can_rej_flag")));
                /* do setIsSubsequentMessageAllowed last, it needs other variables set*/
                messageVO.setIsSubsequentMessageAllowed();
                messageVO.setOverUnderCharge(messageRS.getDouble("over_under_charge"));
            }

        }finally {
            
            
            if(logger.isDebugEnabled()){
               logger.debug("Exiting getGENMessage");
            } 
        }
        
        return messageVO;

    }
    
    
    public void closeResultSet(ResultSet rs) throws SQLException
    {
      if(rs!=null)
            {
              try
              {
                 rs.close();
              }catch(Exception e)
              {
                logger.error(e);
              }
              
              if(rs.getStatement()!=null)
              {
                try
                {
                  rs.getStatement().close();
                }catch(Exception e)
                {
                   logger.error(e);
                }
              }
        }
            
    }
    
    
    

   /**
     * Call stored procedure ???? and return Message Value Object based 
     * on message id and message type.
     * @param messageId - String
     * @param messageType - String
     * @return MessageVO
     * @throws IOException
     * @throws SAXException
     * @throws SQLException
     * @throws ParserConfigurationException
     * @throws Exception
     */
    public void retrieveFloristInformation(FTDMessageVO ftdMsgVO)
        throws IOException, SAXException, SQLException, 
            ParserConfigurationException, Exception
    {            
        if(logger.isDebugEnabled()) {
            logger.debug("Entering retrieveFloristInformation");

        }
        
         DataRequest request = new DataRequest();
        CachedResultSet messageRS=null;
        try {

            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put(
                MessagingConstants.GET_FLORIST_NAME_PHONE_IN_FLORIST_ID, 
                    ftdMsgVO.getFillingFloristCode());

            /* build DataRequest object */
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID(MessagingConstants.GET_FLORIST_NAME_PHONE);
 
           /* execute the store prodcedure */
            DataAccessUtil dau = DataAccessUtil.getInstance();
            messageRS = (CachedResultSet) dau.execute(request);
            while(messageRS.next())
            {  
                 ftdMsgVO.setFloristName(messageRS.getString(2));
                 ftdMsgVO.setFloristPhoneNumber(messageRS.getString(3));
            }

        }finally {
            //closeResultSet(messageRS);
            if(logger.isDebugEnabled()){
               logger.debug("Exiting retrieveFloristInformation");
            } 
        }

    }
    
    
    
    
    public void setOrderIdsAndFlags(FTDMessageVO ftdMsgVO)
        throws IOException, SAXException, SQLException, 
            ParserConfigurationException, Exception
    {            
        if(logger.isDebugEnabled()) {
            logger.debug("Entering retrieveFloristInformation");

        }
        
        DataRequest request = new DataRequest();
        CachedResultSet messageRS=null;
        try {

            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put(
                "IN_ORDER_DETAIL_ID", 
                    ftdMsgVO.getOrderDetailId());

            /* build DataRequest object */
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID("GET_ORDER_IDS");
 
           /* execute the store prodcedure */
            DataAccessUtil dau = DataAccessUtil.getInstance();
            messageRS = (CachedResultSet) dau.execute(request);
            if(messageRS.next())
            {  
                 ftdMsgVO.setOrderGUID(messageRS.getString("order_guid"));
                 ftdMsgVO.setMasterOrderNumber(messageRS.getString("master_order_number"));
                 ftdMsgVO.setExternalOrderNumber(messageRS.getString("external_order_number"));
                 ftdMsgVO.setCustomerID(messageRS.getString("customer_id"));
                 ftdMsgVO.setLetterFlag(messageRS.getString("order_has_letters"));
                 ftdMsgVO.setEmailFlag(messageRS.getString("order_has_emails"));
                 
            }

        }finally {
            //closeResultSet(messageRS);
            if(logger.isDebugEnabled()){
               logger.debug("Exiting retrieveFloristInformation");
            } 
        }

    }
    
    

    /**
     * Call stored procedure GET_MESSAGE_ORDER_STATUS and return 
     * MessageOrderStatus Value Object 
     * based on message order number and order detail id.
     * @param messageOrderNumber - String
     * @param messageType - String
     * @param orderDetailId 
     * @return MessageOrderStatusVO
     * @throws SAXException
     * @throws IOException
     * @throws ParserConfigurationException
     * @throws SQLException
     */
    public MessageOrderStatusVO getMessageOrderStatus(
        String messageOrderNumber, String messageType, String orderDetailId) 
        throws SAXException, IOException, SQLException, ParserConfigurationException{
    	
    	MessageOrderStatusVO statusVO=new MessageOrderStatusVO();
    	DataRequest dataRequest = new DataRequest();
    	dataRequest.setConnection(this.dbConnection);
    	dataRequest.setStatementID("GET_MESSAGE_ORDER_STATUS");
    	dataRequest.addInputParam("IN_MESSAGE_ORDER_NUMBER", messageOrderNumber);
    	dataRequest.addInputParam("IN_MESSAGE_TYPE", messageType);
    	dataRequest.addInputParam("IN_ORDER_DETAIL_ID", orderDetailId);
    	DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      CachedResultSet result=(CachedResultSet)dataAccessUtil.execute(dataRequest);
    	statusVO.setMessageOrderNumber(messageOrderNumber);
      if(result.next()){
        statusVO.setOrderDetailId(result.getString("order_detail_id"));
        statusVO.setCancelSent(this.flagToBoolean(result.getString("cancel_sent")));
        statusVO.setDeliveryDate(result.getTimestamp("delivery_date"));
        statusVO.setFillingFloristCode(result.getString("filling_florist"));
        statusVO.setFloristRejectOrder(this.flagToBoolean(result.getString("florist_rej_order")));
        statusVO.setHasLiveFTD(this.flagToBoolean(result.getString("has_live_ftd")));
        
        statusVO.setAttemptedFTD(this.flagToBoolean(result.getString("ATTEMPTED_FTD")));
        statusVO.setOrderStatus(result.getString("order_status"));
        statusVO.setOrderType(result.getString("order_type"));
        logger.debug("OUT_ORDER_TYPE="+statusVO.getOrderType());
        statusVO.setHasInboundASKP(this.flagToBoolean(result.getString("inbound_askp")));
        statusVO.setATypeRefund(this.flagToBoolean(result.getString("a_type_refund")));
        statusVO.setCancelDenied(this.flagToBoolean(result.getString("cancel_denied")));
        statusVO.setRecipientCountry(result.getString("recipient_country"));
        logger.debug("ATTEMPTED_FTD "+result.getString("ATTEMPTED_FTD"));
        String compOrderFlag=result.getString("comp_order");
        statusVO.setCompOrder(StringUtils.isNotBlank(compOrderFlag)&&compOrderFlag.equalsIgnoreCase("C"));
        logger.debug("Comp Order "+statusVO.isCompOrder());
      }
      
      if(logger.isDebugEnabled())
      {
        
        logger.debug("status: attempted_ftd: "+statusVO.isAttemptedFTD());
        logger.debug("status: cancelled: "+statusVO.isCancelSent());
        logger.debug("status: cancel denied: "+statusVO.isCancelDenied());
        logger.debug("status: recipient_country: "+statusVO.getRecipientCountry());
      }
    	return statusVO;
        
      
    }
    
    
     private String getResultAsString(String key, Map resultMap)
     {
       
       Object obj=resultMap.get(key);
       if(obj!=null && obj instanceof String)
       {
         return (String)obj;
       }else
       {
         return null;
       }
       
     }
     
     
     
     private Date getResultAsDate(String key, Map resultMap)
     {
       
       Object obj=resultMap.get(key);
       if(obj!=null && obj instanceof Date)
       {
         return (Date)obj;
       }else
       {
         return null;
       }
       
     }



    /**
     * Call stored procedure ???? and return retrieve DOM Document object that
     * reflects the current order information based on the order message id 
     * and order detail id.
     * @param orderDetailId - String
     * @param messageOrderNumber - String
     * @return FTDMessageVO
     * @throws Exception
     * @todo - change process based on tech spec
     */
    public FTDMessageVO getMessageForCurrentOrder(
        String orderDetailId, String messageType) 
        throws IOException, SAXException, SQLException, 
            ParserConfigurationException, ParseException{
        
        if(logger.isDebugEnabled()) {
            logger.debug("Entering getMessageForCurrentOrder");
            logger.debug("orderDetailId : " + orderDetailId);
            logger.debug("messageType : " + messageType);
        }
        
        FTDMessageVO messageVO = new FTDMessageVO();
         try{
      
            messageVO = getMessageDetailNewFTD(orderDetailId,messageType);
            
            
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting getMessageForCurrentOrder");
            } 
           
        } 
            return messageVO;
        }
        
        
        
        
     public String getBoxXOccasionDesc(
        String occasionCode) 
        throws IOException, SAXException, SQLException, 
            ParserConfigurationException, ParseException{
        
        if(logger.isDebugEnabled()) {
            logger.debug("Entering getBoxXOccasionDesc");
            logger.debug("occasionCode : " + occasionCode);
            
        }
       DataRequest request = new DataRequest();
       String desc=null;
       CachedResultSet rs=null;
         try{
          /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put(
                "IN_OCCASION_ID",
                occasionCode);  
             /* build DataRequest object */
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID("GET_OCCASION_BOX_X_DESC_BY_ID");
            rs=(CachedResultSet)DataAccessUtil.getInstance().execute(request);
            if(rs.next())//should only return one.
            {
              desc=rs.getString(1);
              logger.debug("occasion desc="+desc);
              
            }
        } finally {
            //closeResultSet(rs);//close resultset and statement.
            if(logger.isDebugEnabled()){
               logger.debug("Exiting getBoxXOccasionDesc");
            } 
           
        } 
            return desc;
        }



    public MessageVO getMessageForCurrentFlorist(String messageOrderNumber, 
        String messageType, boolean system, String messageDirection, boolean verified)throws SAXException,
        ParserConfigurationException, IOException, SQLException, ParseException{
          
    
         if(logger.isDebugEnabled()){
            logger.debug("Entering getMessageWithCurrentFlorist");
            logger.debug("messageOrderNumber : " + messageOrderNumber + 
                " message type: "+messageType+"\t system "+system);
        }
         DataRequest request = new DataRequest();
         MessageVO messageVO = new MessageVO();
        CachedResultSet rs=null;
        try {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put(
                MessagingConstants.GET_MESSAGE_DETAIL_FROM_FTD_IN_MESSAGE_ORDER_NUMBER, 
                messageOrderNumber);
            inputParams.put(
                MessagingConstants.GET_MESSAGE_DETAIL_FROM_FTD_IN_MESSAGE_TYPE,
                messageType);  
            inputParams.put(
                  "IN_MESSAGE_DIRECTION",
                  messageDirection);
            if(system){
              inputParams.put(
                  MessagingConstants.GET_MESSAGE_DETAIL_FROM_FTD_IN_PRICE_INDICATOR,
                  "SecondLast");
            }else
            {
              
              inputParams.put(
                  MessagingConstants.GET_MESSAGE_DETAIL_FROM_FTD_IN_PRICE_INDICATOR,
                  "Last");
            }
            
            
            inputParams.put("IN_VERIFIED_INDICATOR",  (verified ?"Y":"N"));
            
            /* build DataRequest object */
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID(MessagingConstants.GET_MESSAGE_DETAIL_FROM_FTD);

            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            rs= (CachedResultSet) dau.execute(request);
            while(rs.next())
            {
              populateMessageVO(messageType, messageVO, rs, system);
            }

        } finally {
           // closeResultSet(rs);
            if(logger.isDebugEnabled()){
               logger.debug("Exiting getMessageWithCurrentFlorist");
            } 
           
        }   

         return messageVO;
            
        
        }
    

    /**
     * Call stored procedure CLEAN.order_mesg_pkg.GET_MESSAGE_DETAIL_FROM_FTD 
     * and return DOM Document object that reflects 
     * the order information for the filling florist/vendor based on the 
     * message order id, order detail id and filling florist id.
     * @param messageOrderNumber - String
     * @param messageType - String
     * @return MessageVO
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     */
    public MessageVO getMessageForCurrentFlorist(String messageOrderNumber, 
        String messageType)throws SAXException,
        ParserConfigurationException, IOException, SQLException, ParseException{
          
    
              return this.getMessageForCurrentFlorist(messageOrderNumber, messageType, false, null, false);
            
        
        }


    /**
     * @param messageType
     * @param messageVO
     * @param rs
     * @throws SQLException
     * void
     */
    private void populateMessageVO(String messageType, MessageVO messageVO, CachedResultSet rs, boolean system) throws SQLException, ParseException {
          SimpleDateFormat sdfOutput = new SimpleDateFormat (MessagingConstants.MESSAGE_DATE_FORMAT);
          String textDate = "";
          java.util.Date utilDate;

          messageVO.setType(messageType);
          messageVO.setFillingFloristCode(rs.getString(1));
          messageVO.setSendingFloristCode(rs.getString(2));
          messageVO.setOrderDate(rs.getDate(3));
          messageVO.setRecipientName(rs.getString(4));
          messageVO.setRecipientStreet(rs.getString(5));
          messageVO.setRecipientCityStateZip(rs.getString(6));
          messageVO.setDeliveryDate(rs.getDate(7));
          //messageVO.setDetailType(rs.getString(8));
          
          //messageVO.setDeliveryText(rs.getString(9));
          textDate = new SimpleDateFormat("EEEE, MMMMM dd").format(rs.getDate(7));
          messageVO.setDeliveryText(textDate);
          
          messageVO.setMessageOrderId(rs.getString(10));
          
          //messageVO.setOrderDateText(rs.getString(11));
          textDate = new SimpleDateFormat("EEEE, MMMMM dd").format(rs.getDate(3));
          messageVO.setOrderDateText(textDate);
          
          messageVO.setFTDMessageID(rs.getString(13));
          messageVO.setFloristName(rs.getString(14));
          messageVO.setFloristPhoneNumber(rs.getString(15));
          
          messageVO.setCurrentPrice(rs.getDouble(12));
          if(system)
          {
            messageVO.setOldPrice(rs.getDouble(16));
          }
          
          if(rs.getDate("ship_date") != null)
          {
            textDate = sdfOutput.format(rs.getDate("ship_date"));
            utilDate = sdfOutput.parse( textDate );
            messageVO.setShipDate(utilDate);
          }
          
          messageVO.setShippingSystem(rs.getString(18));
          messageVO.setOrderDetailId(rs.getString(19));
          messageVO.setRecipientPhone(rs.getString(20));
    }
        

    /**
     * Retrieve the default florist id from company master table
     * @return String - Default Florist ID
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     */
    public String findDefaultFloristID(String orderDetailId)
    throws SAXException,
        ParserConfigurationException, IOException, SQLException
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering findDefaultFloristID");
        }
        
        String defaultFlorist = "";
        CachedResultSet defaultFloristRS =null;
        try
        {
            // build DataRequest object
            DataRequest request = new DataRequest();
            request.reset();
            request.setConnection(dbConnection);
            HashMap inputParams = new HashMap();
            inputParams.put("IN_ORDER_DETAIL_ID", orderDetailId);
            request.setInputParams(inputParams);
            request.setStatementID(MessagingConstants.GET_SENDING_FLORIST_BY_ID);

            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();

            defaultFloristRS = (CachedResultSet) dau.execute(request);
            /* populate object */
            while (defaultFloristRS.next()) {
            	defaultFlorist = defaultFloristRS.getString("SENDING_FLORIST_NUMBER");
            }
            
            
        }

       finally
        {
           // this.closeResultSet(defaultFloristRS);
            if(logger.isDebugEnabled()){
                logger.debug("Exiting findDefaultFloristID");
            }
        }
       return defaultFlorist;
    }
    
    
    
    /**
     * @param floristCode
     * @return String - Default Florist ID
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     * @throws Exception
     */ 
   public FloristStatusVO getFloristStatus(String floristCode) 
    throws SAXException, ParserConfigurationException, IOException, 
               SQLException, Exception
  {
	   FloristStatusVO floristStatus=null;
	   Date deliveryDate = new Date();
	   Date deliveryDateEnd = new Date();
	   floristStatus = this.getFloristStatus(floristCode, deliveryDate, deliveryDateEnd);
	   return floristStatus;
  }

    /**
     * @param floristCode
     * @return String - Default Florist ID
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     * @throws Exception
     */ 
   public FloristStatusVO getFloristStatus(String floristCode, Date deliveryDate, Date deliveryDateEnd) 
    throws SAXException, ParserConfigurationException, IOException, 
               SQLException, Exception
  {
      FloristStatusVO floristStatus=null;
      java.sql.Date thisDate = new java.sql.Date(deliveryDate.getTime());
           
        DataRequest dataRequest = new DataRequest();
    	dataRequest.setConnection(this.dbConnection);
    	dataRequest.setStatementID("GET_FLORIST_STATUS");
    	dataRequest.addInputParam("IN_FLORIST_ID", floristCode);
    	dataRequest.addInputParam("IN_DELIVERY_DATE", thisDate);
    	dataRequest.addInputParam("IN_DELIVERY_DATE_END", deliveryDateEnd == null ? null : new java.sql.Date(deliveryDateEnd.getTime()));
     
    	DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      CachedResultSet crs=(CachedResultSet)dataAccessUtil.execute(dataRequest);
      if(crs.next())
      {
        floristStatus=new FloristStatusVO();
        floristStatus.setFloristCode(floristCode);
        floristStatus.setStatus(crs.getString("status"));
        floristStatus.setMercury(crs.getString("mercury_flag"));
        floristStatus.setVendor(this.flagToBoolean(crs.getString("vendor_flag")));
        floristStatus.setOptout(this.flagToBoolean(crs.getString("opt_out_flag")));
        floristStatus.setFloristCurrentlyOpen(crs.getString("florist_open_in_eros"));
        floristStatus.setCurrentlyClosedMsg(crs.getString("florist_currently_closed_msg"));
        floristStatus.setOpenOnDeliveryDate(crs.getString("open_on_delivery"));
        floristStatus.setClosedOnDeliveryDateMsg(crs.getString("florist_closed_on_dd_msg"));
        floristStatus.setSundayDeliveryFlag(crs.getString("sunday_delivery_flag"));
        floristStatus.setFloristSuspended(crs.getString("florist_suspended"));
      }   	
    
    return floristStatus;
  }    
  
   /**
    * @param floristCode
    * @return String - Default Florist ID
    * @throws SAXException
    * @throws ParserConfigurationException
    * @throws IOException
    * @throws SQLException
    * @throws Exception
    */ 
  public FloristStatusVO getUDDFloristStatus(String floristCode, Date deliveryDate) 
   throws SAXException, ParserConfigurationException, IOException, 
              SQLException, Exception
 {
    FloristStatusVO floristStatus=null;
    java.sql.Date thisDate = new java.sql.Date(deliveryDate.getTime());
     
    DataRequest dataRequest = new DataRequest();
   	dataRequest.setConnection(this.dbConnection);
   	dataRequest.setStatementID("GET_UDD_FLORIST_STATUS");
   	dataRequest.addInputParam("IN_FLORIST_ID", floristCode);
   	dataRequest.addInputParam("IN_DELIVERY_DATE", thisDate);
    
   	DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
     CachedResultSet crs=(CachedResultSet)dataAccessUtil.execute(dataRequest);
     if(crs.next())
     {
       floristStatus=new FloristStatusVO();
       floristStatus.setFloristCode(floristCode);
       floristStatus.setStatus(crs.getString("status"));
       floristStatus.setMercury(crs.getString("mercury_flag"));
       floristStatus.setVendor(this.flagToBoolean(crs.getString("vendor_flag")));
       floristStatus.setOptout(this.flagToBoolean(crs.getString("opt_out_flag")));
       floristStatus.setFloristCurrentlyOpen(crs.getString("florist_open_in_eros"));
       floristStatus.setCurrentlyClosedMsg(crs.getString("florist_currently_closed_msg"));
       floristStatus.setOpenOnDeliveryDate(crs.getString("open_on_delivery"));
       floristStatus.setClosedOnDeliveryDateMsg(crs.getString("florist_closed_on_dd_msg"));
       floristStatus.setSundayDeliveryFlag(crs.getString("sunday_delivery_flag"));
       floristStatus.setFloristBlocked(crs.getString("florist_blocked"));
       floristStatus.setFloristSuspended(crs.getString("florist_suspended"));
     }   	
   
   return floristStatus;
 }    

    /**
     * updates the ORDER_FLORIST_USED table to indicate that a florist has
     * been used for an order
     * @param orderDetailID
     * @param floristID
     * @return n/a
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     * @throws Exception
     */ 
    public void orderFloristUsed(String orderDetailID, String floristID,
    		String selectionData)
           throws IOException, SAXException, SQLException, 
        ParserConfigurationException, Exception
        {
        if(logger.isDebugEnabled()){
            logger.debug("Entering orderFloristUsed");
            logger.debug("orderDetailID : " + orderDetailID);
            logger.debug("floristID : " + floristID);
            logger.debug("selectionData : " + selectionData);
        }
         if(selectionData == null){
        	 logger.info("selectionData is null");
         }
         if(selectionData.equalsIgnoreCase("")){
        	 logger.info("selectionData is empty string");
         }
         if(selectionData != null){
        	 logger.info("selectionData : " + selectionData);
         }
         DataRequest request = new DataRequest();
        
        try {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();

            inputParams.put(MessagingConstants.INSERT_ORDER_FLORIST_USED_IN_ORDER_DETAIL_ID, 
                new Long(orderDetailID));
            inputParams.put(MessagingConstants.INSERT_ORDER_FLORIST_USED_IN_FLORIST_ID,
                floristID);
            inputParams.put("IN_SELECTION_DATA", (selectionData != null && !selectionData.equalsIgnoreCase("")) ? selectionData : "Manual");
                  
            /* build DataRequest object */
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID(MessagingConstants.INSERT_ORDER_FLORIST_USED);

           /* execute the store prodcedure and retrieve output*/
            DataAccessUtil dau = DataAccessUtil.getInstance();
            Map outputs = (Map) dau.execute(request);
            /* read store prodcedure output parameters to determine 
             * if the procedure executed successfully */
                String status = (String) outputs.get(
                    MessagingConstants.INSERT_ORDER_FLORIST_USED_OUT_STATUS);
                if(status.equals("N"))
                {
                    String message = (String) outputs.get(
                        MessagingConstants.INSERT_ORDER_FLORIST_USED_OUT_ERROR_MESSAGE);

                    throw new Exception(message);
                }
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting orderFloristUsed");
            } 
        }
    }

    /**
     * Update the order detail for an order
     * @param orderDetailID - String
     * @param floristID - String
     * @param csr_id - String
     * @return n/a
     * @throws IOException, SAXException, ParserConfigurationException, 
        SQLException, Exception
     */
    public void modifyOrder(String orderDetailID, String floristID, String csr_id, String orderDispostion, Date deliveryDate, Date deliveryDateEnd) 
        throws IOException, SAXException, ParserConfigurationException, 
        SQLException, Exception
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering modifyOrder");
            logger.debug("orderDetailID: " + orderDetailID);
            logger.debug("floristID: [" + floristID+"]");

        }
         DataRequest request = new DataRequest();

        try {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();

            inputParams.put(MessagingConstants.UPDATE_ORDER_DETAILS_IN_DELIVERY_DATE, 
                            (deliveryDate != null) ? new java.sql.Date(deliveryDate.getTime()) : null);
            inputParams.put(MessagingConstants.UPDATE_ORDER_DETAILS_IN_DELIVERY_DATE_RANGE_END, 
                            (deliveryDateEnd != null) ? new java.sql.Date(deliveryDateEnd.getTime()) : null);

            inputParams.put(MessagingConstants.UPDATE_ORDER_DETAILS_IN_ORDER_DETAIL_ID, 
                new Long(orderDetailID));
            inputParams.put(MessagingConstants.UPDATE_ORDER_DETAILS_IN_RECIPIENT_ID, 
                null);
            inputParams.put(MessagingConstants.UPDATE_ORDER_DETAILS_IN_PRODUCT_ID, 
                null);                
            inputParams.put(MessagingConstants.UPDATE_ORDER_DETAILS_IN_QUANTITY, 
                null);
            inputParams.put(MessagingConstants.UPDATE_ORDER_DETAILS_IN_COLOR_1, 
                null);
            inputParams.put(MessagingConstants.UPDATE_ORDER_DETAILS_IN_COLOR_2, 
                null);
            inputParams.put(MessagingConstants.UPDATE_ORDER_DETAILS_IN_SUBSTITUTION_INDICATOR, 
                null);
            inputParams.put(MessagingConstants.UPDATE_ORDER_DETAILS_IN_SAME_DAY_GIFT, 
                null);
            inputParams.put(MessagingConstants.UPDATE_ORDER_DETAILS_IN_OCCASION, 
                null);
            inputParams.put(MessagingConstants.UPDATE_ORDER_DETAILS_IN_CARD_MESSAGE, 
                null);
            inputParams.put(MessagingConstants.UPDATE_ORDER_DETAILS_IN_CARD_SIGNATURE, 
                null);
            inputParams.put(MessagingConstants.UPDATE_ORDER_DETAILS_IN_SPECIAL_INSTRUCTIONS, 
                null);
            inputParams.put(MessagingConstants.UPDATE_ORDER_DETAILS_IN_RELEASE_INFO_INDICATOR, 
                null);
            inputParams.put(MessagingConstants.UPDATE_ORDER_DETAILS_IN_FLORIST_ID, 
                floristID.trim());            
            inputParams.put(MessagingConstants.UPDATE_ORDER_DETAILS_IN_SHIP_METHOD, 
                null);            
            inputParams.put(MessagingConstants.UPDATE_ORDER_DETAILS_IN_SHIP_DATE, 
                null);   
            inputParams.put(MessagingConstants.UPDATE_ORDER_DETAILS_IN_ORDER_DISP_CODE, 
                orderDispostion);           
            inputParams.put(MessagingConstants.UPDATE_ORDER_DETAILS_IN_SECOND_CHOICE_PRODUCT, 
                null);   
            inputParams.put(MessagingConstants.UPDATE_ORDER_DETAILS_IN_ZIP_QUEUE_COUNT, 
                null);         
            inputParams.put(MessagingConstants.UPDATE_ORDER_DETAILS_IN_RANDOM_WEIGHT_DIST_FAILURES, 
                null);   
            inputParams.put(MessagingConstants.UPDATE_ORDER_DETAILS_IN_UPDATED_BY, 
                csr_id);         
            inputParams.put(MessagingConstants.UPDATE_ORDER_DETAILS_IN_SCRUBBED_ON, 
                null);         
            inputParams.put(MessagingConstants.UPDATE_ORDER_DETAILS_IN_SCRUBBED_BY, 
                null);   
            inputParams.put(MessagingConstants.UPDATE_ORDER_DETAILS_IN_ARIBA_UNSPSC_CODE, 
                null); 
            inputParams.put(MessagingConstants.UPDATE_ORDER_DETAILS_IN_ARIBA_PO_NUMBER, 
                null);   
            inputParams.put(MessagingConstants.UPDATE_ORDER_DETAILS_IN_ARIBA_AMS_PROJECT_CODE, 
                null);         
            inputParams.put(MessagingConstants.UPDATE_ORDER_DETAILS_IN_ARIBA_COST_CENTER, 
                null);   
            inputParams.put(MessagingConstants.UPDATE_ORDER_DETAILS_IN_SIZE_INDICATOR, 
                null); 
            inputParams.put(MessagingConstants.UPDATE_ORDER_DETAILS_IN_MILES_POINTS, 
                null);   
            inputParams.put(MessagingConstants.UPDATE_ORDER_DETAILS_IN_SUBCODE, 
                null);             
            inputParams.put("IN_SOURCE_CODE", null);    

            /* build DataRequest object */
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID(MessagingConstants.UPDATE_ORDER_DETAILS);

            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            Map outputs = (Map) dau.execute(request);
            /* read store prodcedure output parameters to determine 
             * if the procedure executed successfully */
                String status = (String) outputs.get(
                    MessagingConstants.UPDATE_ORDER_DETAILS_OUT_STATUS);
                if(status.equals("N"))
                {
                    String message = (String) outputs.get(
                    MessagingConstants.UPDATE_ORDER_DETAILS_OUT_MESSAGE);

                    throw new Exception(message);
                }
      
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting modifyOrder");
            } 
        }
    }
    
    /**
     * Call stored procedure ???? and return Message Value Object based 
     * on message id and message type.
     * @param messageId - String
     * @param messageType - String
     * @return MessageVO
     * @throws IOException
     * @throws SAXException
     * @throws SQLException
     * @throws ParserConfigurationException
     * @throws Exception
     * @todo - change process based on tech spec
     */
    public FTDMessageVO getMessageDetailNewFTD(String orderDetailID, String messageType)
        throws IOException, SAXException, SQLException, 
            ParserConfigurationException, ParseException
    {            
        if(logger.isDebugEnabled()) {
            logger.debug("Entering getMessageDetailNewFTD");
            logger.debug("orderDetailID : " + orderDetailID);
            logger.debug("orderDetailID long: [" + new Long(orderDetailID)+"]");
            logger.debug("messageType : " + messageType);
        }
        
         DataRequest request = new DataRequest();
         FTDMessageVO messageVO = new FTDMessageVO();
         CachedResultSet messageRS=null;
        try {

            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put("IN_ORDER_DETAIL_ID", 
                new Long(orderDetailID));
            inputParams.put(
                MessagingConstants.GET_MESSAGE_DETAIL_NEW_FTD_IN_MESSAGE_TYPE, messageType);
            
            /* build DataRequest object */
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID(MessagingConstants.GET_MESSAGE_DETAIL_NEW_FTD);
 
           /* execute the store prodcedure */
            DataAccessUtil dau = DataAccessUtil.getInstance();
            messageRS = (CachedResultSet) dau.execute(request);
            SimpleDateFormat sdfOutput = 
                       new SimpleDateFormat (MessagingConstants.MESSAGE_DATE_FORMAT);
            SimpleDateFormat sdfEndDeliveryOutput = 
                       new SimpleDateFormat (MessagingConstants.MESSAGE_END_DELIVERY_DATE_FORMAT);          
                       
            String textDate = "";
            java.util.Date utilDate;
            if(messageRS.next())
            {  
                messageVO.setOrderDetailId(orderDetailID);
                messageVO.setRecipientName(messageRS.getString("first_name") + " " + messageRS.getString("last_name"));
                messageVO.setRecipientStreet(messageRS.getString("recipient_address"));
                messageVO.setRecipientCity(messageRS.getString("city"));
                messageVO.setRecipientState(messageRS.getString("state"));
                messageVO.setRecipientZip(messageRS.getString("zip_code"));
                messageVO.setRecipientPhone(messageRS.getString("recipient_phone_number"));
                textDate = sdfOutput.format(messageRS.getDate("delivery_date"));
                utilDate = sdfOutput.parse( textDate );
                messageVO.setDeliveryDate(utilDate);
                if(messageRS.getDate("delivery_date_range_end") != null){
                    messageVO.setEndDeliveryDate(sdfEndDeliveryOutput.format(messageRS.getDate("delivery_date_range_end")));            
                }
                messageVO.setOccasionCode(messageRS.getString("occasion"));
                messageVO.setOccasionDesc(messageRS.getString("occasion_description"));
                messageVO.setCardMessage(messageRS.getString("card_message"));
                messageVO.setDeliveryText(messageRS.getString("delivery_date_text"));
                textDate = sdfOutput.format(messageRS.getDate("order_date"));
                utilDate = sdfOutput.parse( textDate );
                messageVO.setOrderDate(utilDate);
                messageVO.setOrderDateText(messageRS.getString("order_date_text"));
                messageVO.setFillingFloristCode(messageRS.getString("florist_id"));
                messageVO.setExternalOrderNumber(messageRS.getString("external_order_number"));
                messageVO.setFloristName(messageRS.getString("florist_name"));
                logger.debug("florist name="+messageVO.getFloristName());
                messageVO.setFloristPhoneNumber(messageRS.getString("phone_number"));
                logger.debug("florist phone="+messageVO.getFloristPhoneNumber());
                messageVO.setVendorCost(messageRS.getDouble("vendor_cost"));
                messageVO.setVendorSku(messageRS.getString("vendor_sku"));
                messageVO.setProductId(messageRS.getString("product_id"));
                messageVO.setBusinessName(messageRS.getString("business_name"));
                messageVO.setSourceCode(messageRS.getString("source_code"));
                messageVO.setRecipientCountry(messageRS.getString("country"));
                messageVO.setSpecialInstruction(messageRS.getString("special_instructions"));
                messageVO.setOrderHasMorningDelivery(messageRS.getString("order_has_morning_delivery"));
                messageVO.setOrderAddOns(messageRS.getString("order_add_ons"));
                messageVO.setFTDWOrder("Y".equalsIgnoreCase(messageRS.getString("is_ftdw_order"))?true:false);
                logger.debug("source_code="+messageVO.getSourceCode());
            }

        }finally {
           // this.closeResultSet(messageRS);
            if(logger.isDebugEnabled()){
               logger.debug("Exiting getMessageDetailNewFTD");
            } 
        }
        
        return messageVO;

    }

    private boolean flagToBoolean(String flag){
    	logger.debug("flag: ["+flag+"]");
    	if(flag!=null&&flag.trim().equalsIgnoreCase("Y")){
    		return true;
    	}else{
    		return false;
    	}
    }
    
    
    
    
     public void insertCallOutLog(String messageId, String csrId, CallOutInfo callOutInfo)
        throws IOException, SAXException, SQLException, 
            ParserConfigurationException, Exception
        {
        if(logger.isDebugEnabled()){
            logger.debug("Entering insertCallOutLog");
            logger.debug("messageId : " + messageId);
            logger.info("csrId : " + csrId);
            logger.debug("callOutInfo : " + callOutInfo);
        }
         DataRequest request = new DataRequest();
         
        try {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put("IN_MERCURY_ID",     messageId);
            inputParams.put("IN_SHOP_NAME",      callOutInfo.getShopName());
            inputParams.put("IN_SHOP_PHONE",     callOutInfo.getShopPhone());
            inputParams.put("IN_SPOKE_TO",       callOutInfo.getSpokeTo());
            inputParams.put("IN_CREATED_BY",     csrId);
            
            // build DataRequest object
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID("INSERT_CALL_OUT_LOG");

            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            Map outputs = (Map) dau.execute(request);
                String status = (String) outputs.get(
                    MessagingConstants.INSERT_COMMENTS_OUT_STATUS_PARAM);
                if(status.equals("N"))
                {
                    String message = (String) outputs.get(
                        MessagingConstants.INSERT_COMMENTS_OUT_MESSAGE_PARAM);
                    logger.error("can not insertCallOutLog "+message);
                    throw new Exception(message);
                }
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting insertCallOutLog");
            } 
        }       
    }
    
    
     /**
   * Check for if the order is tagged by a user in
   *     the order queue and if the user has tagged the order.
   *     Output:
   *     0 - order is not tagged to any user
   *     1Y - order is tagged to the given user
   *     1N - order is tagged but no to the given user
   *     2Y - order is tagged by 2 users, one of them being the given user
   *     2N - order is tagged by 2 users, neither by the given user
   * @param orderDetailId
   * @param csrId
   * @return 
   * @throws java.io.IOException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws org.xml.sax.SAXException
   * @throws java.sql.SQLException
   */
     public Document getOrderTaggedIndicator(String orderDetailId, String csrId)
        throws IOException, ParserConfigurationException, SAXException, SQLException
    {
        HashMap inputParams = new HashMap();
        HashMap outParametersMap = new HashMap();
        DataRequest request = new DataRequest();
        // 1. build db stored procedure input parms
        logger.debug("stored procedure:  IS_ORDER_TAGGED" );
        logger.debug("IN_ORDER_DETAIL_ID = " + orderDetailId);
        logger.debug("IN_CSR_ID = " + csrId);
        inputParams.put("IN_ORDER_DETAIL_ID", orderDetailId);
        inputParams.put("IN_CSR_ID", csrId);
        //build DataRequest object
        request = new DataRequest();
        request.reset();
        request.setConnection(this.dbConnection);
        request.setInputParams(inputParams);
        request.setStatementID("IS_ORDER_TAGGED");

        // 2. get data
        DataAccessUtil dau = DataAccessUtil.getInstance();
        return (Document) dau.execute(request);
    }
    

    public void updateMercuryFTDPrice(String messageId, Double price)
        throws IOException, SAXException, SQLException, 
            ParserConfigurationException, Exception
        {
        if(logger.isDebugEnabled()){
            logger.debug("Entering updateMercuryFTDPrice");
            logger.debug("messageId : " + messageId);
            logger.debug("new price : " + price);
           
        }
         DataRequest request = new DataRequest();
         
        try {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put("IN_MERCURY_ID", messageId);
            inputParams.put("IN_PRICE", price);
            
            
            // build DataRequest object
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID("UPDATE_MERCURY_FTD_PRICE");

            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            Map outputs = (Map) dau.execute(request);
                String status = (String) outputs.get(
                    "OUT_STATUS");
                if(status!=null&&status.equals("N"))
                {
                    String message = (String) outputs.get("OUT_ERROR_MESSAGE");
                    logger.error("can not updateMercuryFTDPrice. Error: "+message);
                    throw new Exception(message);
                }
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting updateMercuryFTDPrice");
            } 
        }       
    }
    
    
    
    
    
    
    /**
   *The temporary solution to substitute modify order. 
   * @param orderdetailId
   * @throws java.io.IOException
   * @throws org.xml.sax.SAXException
   * @throws java.sql.SQLException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws java.lang.Exception
   */
    public void insertOrderDeliveryDateToTemp(String orderdetailId)
        throws IOException, SAXException, SQLException, 
            ParserConfigurationException, Exception
        {
        if(logger.isDebugEnabled()){
            logger.debug("Entering insertOrderDeliveryDateToTemp");
            logger.debug("orderdetailId : " + orderdetailId);
           
           
        }
         DataRequest request = new DataRequest();
         
        try {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put("IN_ORDER_DETAIL_ID", orderdetailId);
                        
            
            // build DataRequest object
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID("INSERT_DELIVERY_DATE_UPDATE");

            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            Map outputs = (Map) dau.execute(request);
                String status = (String) outputs.get(
                    "OUT_STATUS");
                if(status!=null&&status.equals("N"))
                {
                    String message = (String) outputs.get("OUT_MESSAGE");
                    logger.warn("can not insertOrderDeliveryDateToTemp. Error: "+message);
                    
                }
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting insertOrderDeliveryDateToTemp");
            } 
        }       
    }
    
    
    
    
    /**
   *The temporary solution to substitute modify order. 
   * @param orderdetailId
   * @throws java.io.IOException
   * @throws org.xml.sax.SAXException
   * @throws java.sql.SQLException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws java.lang.Exception
   */
    public void cancelOrderDeliveryDateChange(String orderdetailId)
        throws IOException, SAXException, SQLException, 
            ParserConfigurationException, Exception
        {
        if(logger.isDebugEnabled()){
            logger.debug("Entering cancelOrderDeliveryDateChange");
            logger.debug("orderdetailId : " + orderdetailId);
           
           
        }
         DataRequest request = new DataRequest();
         
        try {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put("IN_ORDER_DETAIL_ID", orderdetailId);
                        
            
            // build DataRequest object
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID("INSERT_DELIVERY_DATE_UPDATE");

            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            Map outputs = (Map) dau.execute(request);
                String status = (String) outputs.get(
                    "OUT_STATUS");
                if(status!=null&&status.equals("N"))
                {
                    String message = (String) outputs.get("OUT_MESSAGE");
                    logger.error("can not cancelOrderDeliveryDateChange. Error: "+message);
                    throw new Exception(message);
                }
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting cancelOrderDeliveryDateChange");
            } 
        }       
    }
    
    
    
    /**
   * The temporary solution to substitute modify order. 
   * @param orderDetailID
   * @param csr_id
   * @param startDate
   * @param endDate
   * @throws java.io.IOException
   * @throws org.xml.sax.SAXException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws java.sql.SQLException
   * @throws java.lang.Exception
   */
    public void updateOrderDeliveryDate(String orderDetailID, String csr_id, Date startDate, Date endDate) 
        throws IOException, SAXException, ParserConfigurationException, 
        SQLException, Exception
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering updateOrderDeliveryDate");
            logger.debug("orderDetailID: " + orderDetailID);
            logger.debug("start date: " + startDate);
            logger.debug("end date: " + endDate);

        }
         DataRequest request = new DataRequest();

        try {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();


            inputParams.put(MessagingConstants.UPDATE_ORDER_DETAILS_IN_ORDER_DETAIL_ID, 
                new Long(orderDetailID));
            inputParams.put(MessagingConstants.UPDATE_ORDER_DETAILS_IN_DELIVERY_DATE, 
                new java.sql.Date(startDate.getTime()));
                        
            inputParams.put(MessagingConstants.UPDATE_ORDER_DETAILS_IN_UPDATED_BY, 
                csr_id);  
            if(endDate!=null){
              inputParams.put(MessagingConstants.UPDATE_ORDER_DETAILS_IN_DELIVERY_DATE_RANGE_END, 
                  new java.sql.Date(endDate.getTime()));   
            }
             

            /* build DataRequest object */
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID(MessagingConstants.UPDATE_ORDER_DETAILS);

            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            Map outputs = (Map) dau.execute(request);
            /* read store prodcedure output parameters to determine 
             * if the procedure executed successfully */
                String status = (String) outputs.get(
                    MessagingConstants.UPDATE_ORDER_DETAILS_OUT_STATUS);
                if(status.equals("N"))
                {
                    String message = (String) outputs.get(
                    MessagingConstants.UPDATE_ORDER_DETAILS_OUT_MESSAGE);

                    throw new Exception(message);
                }
      
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting updateOrderDeliveryDate");
            } 
        }
    }
    
/**
* Obtain product information
*/
 public CachedResultSet getProductInfo(String productId) throws Exception
  {
    Map paramMap = new HashMap();

    paramMap.put("PRODUCT_ID",productId);
    
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(this.dbConnection);
    dataRequest.setStatementID("GET_PRODUCT_INFO");
    dataRequest.setInputParams(paramMap);
    
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    CachedResultSet crs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
    
    return crs;
   
  }    
  
  public CachedResultSet getOrderDetailsInfo(String orderDetailId) throws Exception
  {
      DataRequest dataRequest = new DataRequest();
      dataRequest.setConnection(this.dbConnection);
      dataRequest.setStatementID("GET_ORDER_DETAILS_BY_ID");
      dataRequest.addInputParam("IN_ORDER_DETAIL_ID", orderDetailId);

      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      
      CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
      return rs;
  }



    /**
   * @param orderDetailID
   * @param csr_id
   * @param deliveryDate
   * @param deliveryDateEnd
   * @param shipDate
   * @param shipMethod
   * 
   * @throws java.io.IOException
   * @throws org.xml.sax.SAXException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws java.sql.SQLException
   * @throws java.lang.Exception
   */
    public void updateOrder(String orderDetailID, String csr_id, Date deliveryDate, 
                            Date deliveryDateEnd, Date shipDate, String shipMethod) 
        throws IOException, SAXException, ParserConfigurationException, 
        SQLException, Exception
    {
      DataRequest request = new DataRequest();

      try 
      {
        /* setup store procedure input parameters */
        HashMap inputParams = new HashMap();
      
        inputParams.put(MessagingConstants.UPDATE_ORDER_DETAILS_IN_ORDER_DETAIL_ID, new Long(orderDetailID));
        inputParams.put(MessagingConstants.UPDATE_ORDER_DETAILS_IN_UPDATED_BY,      csr_id);  
        inputParams.put(MessagingConstants.UPDATE_ORDER_DETAILS_IN_DELIVERY_DATE,   new java.sql.Date(deliveryDate.getTime()));
        inputParams.put(MessagingConstants.UPDATE_ORDER_DETAILS_IN_SHIP_DATE,       new java.sql.Date(shipDate.getTime()));
        inputParams.put(MessagingConstants.UPDATE_ORDER_DETAILS_IN_SHIP_METHOD,     shipMethod);
        if (deliveryDateEnd != null)
        {
          inputParams.put(MessagingConstants.UPDATE_ORDER_DETAILS_IN_DELIVERY_DATE_RANGE_END, new java.sql.Date(deliveryDateEnd.getTime()));
        }

        /* build DataRequest object */
        request.setConnection(dbConnection);
        request.reset();
        request.setInputParams(inputParams);
        request.setStatementID(MessagingConstants.UPDATE_ORDER_DETAILS);

        // get data
        DataAccessUtil dau = DataAccessUtil.getInstance();
        Map outputs = (Map) dau.execute(request);

        /* read store prodcedure output parameters to determine if the procedure executed successfully */
        String status = (String) outputs.get(
        MessagingConstants.UPDATE_ORDER_DETAILS_OUT_STATUS);
        if(status.equals("N"))
        {
          String message = (String) outputs.get(
          MessagingConstants.UPDATE_ORDER_DETAILS_OUT_MESSAGE);

          throw new Exception(message);
        }

      } 
      finally 
      {
        if(logger.isDebugEnabled())
          logger.debug("Exiting updateOrder");
      }
    }
    


    /**
     * Get the SDS transaction for the passed in venus id
     * @param connection database connection
     * @param venusId to get transaction for
     * @return
     * @throws Exception
     */
    public SDSTransactionVO getSDSTransaction(String venusId) throws Exception {
        
        SDSTransactionVO vo = null;
        
        CachedResultSet results = null;
        
        DataRequest dataRequest = new DataRequest();
        dataRequest.reset();
        dataRequest.setConnection(this.dbConnection);
        dataRequest.setStatementID("SHIP_GET_SDS_TRANS_BY_VENUS_ID");
        dataRequest.addInputParam("IN_VENUS_ID",venusId);
        
        DataAccessUtil dau = DataAccessUtil.getInstance();
        
        results = (CachedResultSet)dau.execute(dataRequest);

        if ( results!=null && results.next() )
        {
            vo = new SDSTransactionVO();
            vo.setId(results.getBigDecimal("id"));
            vo.setVenusId(results.getString("venus_id"));
            Clob clob = results.getClob("request");
            vo.setRequest(clob.getSubString(1L,(int)clob.length()));
            clob = results.getClob("response");
            vo.setResponse(clob.getSubString(1L,(int)clob.length()));
        }
        
        return vo;
    }
    
    /**
     * Get venus message for zone jump order for the passed in venus id
     * @param venusId to get transaction for
     * @return FTDMessageVO
     * @throws Exception
     */
     public FTDMessageVO getVenusMessageForZJ(String venusId) throws Exception
    {
        FTDMessageVO messageVO = new FTDMessageVO();
        
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.dbConnection);
        dataRequest.setStatementID("GET_VENUS_MESSAGE_FOR_ZJ");
        dataRequest.addInputParam("IN_VENUS_ID", venusId);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
        
        if(rs.next())
        {  
            messageVO.setZoneJumpFlag(rs.getString("ZONE_JUMP_FLAG") == null ? false : true);
            messageVO.setZoneJumpLabelDate(getUtilDate(rs.getObject("ZONE_JUMP_LABEL_DATE")));
            messageVO.setSdsStatus(rs.getString("sds_status"));
        }
        
        return messageVO;
    }
    
    /*
     * Converts a database timestamp to a java.util.Date.
     */
    private java.util.Date getUtilDate(Object time)
    {
        java.util.Date theDate = null;
        boolean test  = false;
        if(time != null)
        {
            if(test || time instanceof Timestamp)
            {
                Timestamp timestamp = (Timestamp) time;
                theDate = new java.util.Date(timestamp.getTime());
            }
            else
            {
                theDate = (java.util.Date) time;            
            }
        }
        
        return theDate;
    }

    /**
    * Check to see if the florist has rejected this order already.
    *
    * @param orderDetailId
    * @param fillingFlorist
    * @return boolean
    * @throws java.lang.Exception
    *
    */
    public boolean hasFloristRejectedOrder(String orderDetailId, String fillingFlorist) throws Exception
     {
       boolean wasRejected = false;
       
       DataRequest dataRequest = new DataRequest();
       dataRequest.setConnection(this.dbConnection);
       dataRequest.setStatementID("HAS_FLORIST_REJECTED_ORDER");
       dataRequest.addInputParam("IN_ORDER_DETAIL_ID", orderDetailId);
       dataRequest.addInputParam("IN_FILLING_FLORIST", fillingFlorist);
       DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

       String orderRejected = (String) dataAccessUtil.execute(dataRequest);
       
       if(orderRejected.equalsIgnoreCase("Y"))
       {
        wasRejected = true;
       }       
       return wasRejected;
     }


  /**
     * @param connection database connection
     * @param venusId to get transaction for
     * @return
     * @throws Exception
     */
  public ArrayList getAllMercuryOrVenusMessages(String orderDetailId, String mercuryVenusId, String mercuryMessageNumber, 
                                              String mercuryVenusOrderNumber, String msgType, String messageType, 
                                              String levelOfDetails)
    throws Exception
  {
    ArrayList returnList = new ArrayList(); 
    CachedResultSet output = null;

    DataRequest dataRequest = new DataRequest();
    dataRequest.reset();
    dataRequest.setConnection(this.dbConnection);
    dataRequest.setStatementID("GET_MESSAGE_DETAILS_GENERIC");
    dataRequest.addInputParam("IN_REFERENCE_NUMBER", orderDetailId);
    dataRequest.addInputParam("IN_MERCURY_VENUS_ID", mercuryVenusId);
    dataRequest.addInputParam("IN_MERCURY_MESSAGE_NUMBER", mercuryMessageNumber);
    dataRequest.addInputParam("IN_MERCURY_VENUS_ORDER_NUMBER", mercuryVenusOrderNumber);
    dataRequest.addInputParam("IN_MSG_TYPE", msgType);
    dataRequest.addInputParam("IN_MESSAGE_TYPE", messageType);
    dataRequest.addInputParam("IN_ORD_OR_MSG_OR_ID_LEVEL", levelOfDetails);

    DataAccessUtil dau = DataAccessUtil.getInstance();

    output = (CachedResultSet) dau.execute(dataRequest);
    MercuryVO mVO = null; 
    VenusVO vVO = null; 

    while (output.next())
    {
      if (messageType.equalsIgnoreCase("Mercury"))
      {
        mVO = new MercuryVO(); 
        
        mVO.setMercuryId(output.getString("MERCURY_ID"));
        mVO.setMercuryMessageNumber(output.getString("MERCURY_MESSAGE_NUMBER"));
        mVO.setMercuryOrderNumber(output.getString("MERCURY_ORDER_NUMBER"));
        mVO.setMercuryStatus(output.getString("MERCURY_STATUS"));
        mVO.setMsgType(output.getString("MSG_TYPE"));
        mVO.setOutboundId(output.getString("OUTBOUND_ID"));
        mVO.setSendingFlorist(output.getString("SENDING_FLORIST"));
        mVO.setFillingFlorist(output.getString("FILLING_FLORIST"));
        mVO.setOrderDate(getUtilDate(output.getDate("ORDER_DATE")));
        mVO.setRecipient(output.getString("RECIPIENT"));
        mVO.setAddress(output.getString("ADDRESS"));
        mVO.setCityStateZip(output.getString("CITY_STATE_ZIP"));
        mVO.setPhoneNumber(output.getString("PHONE_NUMBER"));
        mVO.setDeliveryDate(getUtilDate(output.getDate("DELIVERY_DATE")));
        mVO.setDeliveryDateText(output.getString("DELIVERY_DATE_TEXT"));
        mVO.setFirstChoice(output.getString("FIRST_CHOICE"));
        mVO.setSecondChoice(output.getString("SECOND_CHOICE"));
        mVO.setPrice(new Double(output.getDouble("PRICE")));
        mVO.setCardMessage(output.getString("CARD_MESSAGE"));
        mVO.setOccasion(output.getString("OCCASION"));
        mVO.setSpecialInstructions(output.getString("SPECIAL_INSTRUCTIONS"));
        mVO.setPriority(output.getString("PRIORITY"));
        mVO.setOperator(output.getString("OPERATOR"));
        mVO.setComments(output.getString("COMMENTS"));
        mVO.setSakText(output.getString("SAK_TEXT"));
        mVO.setCtseq(output.getInt("CTSEQ"));
        mVO.setCrseq(output.getInt("CRSEQ"));
        mVO.setTransmissionDate(getUtilDate(output.getDate("TRANSMISSION_TIME")));
        mVO.setReferenceNumber(output.getString("REFERENCE_NUMBER"));
        mVO.setProductId(output.getString("PRODUCT_ID"));
        mVO.setZipCode(output.getString("ZIP_CODE"));
        mVO.setAskAnswerCode(output.getString("ASK_ANSWER_CODE"));
        mVO.setSortValue(output.getString("SORT_VALUE"));
        mVO.setRetrievalFlag(output.getString("RETRIEVAL_FLAG"));
        mVO.setCombinedReportNumber(output.getString("COMBINED_REPORT_NUMBER"));
        mVO.setRofNumber(output.getString("ROF_NUMBER"));
        mVO.setAdjReasonCode(output.getString("ADJ_REASON_CODE"));
        mVO.setOverUnderCharge(new Double(output.getDouble("OVER_UNDER_CHARGE")));
        mVO.setFromMessageNumber(output.getString("FROM_MESSAGE_NUMBER"));
        mVO.setFromMessageDate(getUtilDate(output.getDate("FROM_MESSAGE_DATE")));
        mVO.setToMessageNumber(output.getString("TO_MESSAGE_NUMBER"));
        mVO.setToMessageDate(getUtilDate(output.getDate("TO_MESSAGE_DATE")));
        mVO.setCreatedOn(getUtilDate(output.getDate("CREATED_ON")));
        mVO.setViewQueue(output.getString("VIEW_QUEUE"));
        mVO.setResponsibleFlorist(output.getString("RESPONSIBLE_FLORIST"));
        mVO.setLockedOn(getUtilDate(output.getDate("LOCKED_ON")));
        mVO.setLockedBy(output.getString("LOCKED_BY"));
        mVO.setCompOrder(output.getString("COMP_ORDER"));
        mVO.setRequireConfirmation(output.getString("REQUIRE_CONFIRMATION"));
        mVO.setSuffix(output.getString("SUFFIX"));
        mVO.setOrderSequence(output.getString("ORDER_SEQ"));
        mVO.setAdminSequence(output.getString("ADMIN_SEQ"));
        mVO.setOldPrice(new Double(output.getDouble("OLD_PRICE")));
        mVO.setMessageDirection(output.getString("MESSAGE_DIRECTION"));
        mVO.setManualReconcileDate(getUtilDate(output.getDate("MANUAL_RECONCILE_DATE")));
        mVO.setRequireConfirmation(output.getString("REQUIRE_CONFIRMATION"));

        returnList.add(mVO); 
      }
      else if (messageType.equalsIgnoreCase("Venus"))
      {
        vVO = new VenusVO(); 
        
        vVO.setVenusId((String)output.getObject("VENUS_ID"));
        vVO.setVenusOrderNumber((String)output.getObject("VENUS_ORDER_NUMBER"));
        vVO.setVenusStatus((String)output.getObject("VENUS_STATUS"));
        vVO.setMsgType((String)output.getObject("MSG_TYPE"));
        vVO.setSendingVendor((String)output.getObject("SENDING_VENDOR"));
        vVO.setFillingVendor((String)output.getObject("FILLING_VENDOR"));
        vVO.setOrderDate(getUtilDate(output.getObject("ORDER_DATE")));
        vVO.setRecipient((String)output.getObject("RECIPIENT"));
        vVO.setBusinessName((String)output.getObject("BUSINESS_NAME"));
        vVO.setAddress1((String)output.getObject("ADDRESS_1"));
        vVO.setAddress2((String)output.getObject("ADDRESS_2"));
        vVO.setCity((String)output.getObject("CITY"));
        vVO.setState((String)output.getObject("STATE"));
        vVO.setZip((String)output.getObject("ZIP"));
        vVO.setPhoneNumber((String)output.getObject("PHONE_NUMBER"));
        vVO.setDeliveryDate(getUtilDate(output.getObject("DELIVERY_DATE")));
        vVO.setFirstChoice((String)output.getObject("FIRST_CHOICE"));
        vVO.setPrice(new Double(output.getDouble("PRICE")));
        vVO.setCardMessage((String)output.getObject("CARD_MESSAGE"));
        vVO.setShipDate(getUtilDate(output.getObject("SHIP_DATE")));
        vVO.setOperator((String)output.getObject("OPERATOR"));
        vVO.setComments((String)output.getObject("COMMENTS"));
        vVO.setTransmissionTime(getUtilDate(output.getObject("TRANSMISSION_TIME")));
        vVO.setReferenceNumber((String)output.getObject("REFERENCE_NUMBER"));
        vVO.setProductID((String)output.getObject("PRODUCT_ID"));
        vVO.setCombinedReportNumber((String)output.getObject("COMBINED_REPORT_NUMBER"));
        vVO.setRofNumber((String)output.getObject("ROF_NUMBER"));
        vVO.setAdjReasonCode((String)output.getObject("ADJ_REASON_CODE"));
        vVO.setOverUnderCharge(new Double(output.getDouble("OVER_UNDER_CHARGE")));
        vVO.setProcessedIndicator((String)output.getObject("PROCESSED_INDICATOR"));
        vVO.setMessageText((String)output.getObject("MESSAGE_TEXT"));
        vVO.setSendToVenus((String)output.getObject("SEND_TO_VENUS"));
        vVO.setOrderPrinted((String)output.getObject("ORDER_PRINTED"));
        vVO.setSubType((String)output.getObject("SUB_TYPE"));
        vVO.setCreatedOn(getUtilDate(output.getObject("CREATED_ON")));
        vVO.setUpdatedOn(getUtilDate(output.getObject("UPDATED_ON")));
        vVO.setErrorDescription((String)output.getObject("ERROR_DESCRIPTION"));
        vVO.setCountry((String)output.getObject("COUNTRY"));
        vVO.setCancelReasonCode((String)output.getObject("CANCEL_REASON_CODE"));
        vVO.setCompOrder((String)output.getObject("COMP_ORDER"));
        vVO.setOldPrice(new Double(output.getDouble("OLD_PRICE")));
        vVO.setShipMethod(output.getString("SHIP_METHOD"));
        vVO.setVendorSKU(output.getString("VENDOR_SKU"));
        vVO.setProductDescription(output.getString("PRODUCT_DESCRIPTION"));
        vVO.setProductWeight(output.getString("PRODUCT_WEIGHT"));
        vVO.setMessageDirection(output.getString("MESSAGE_DIRECTION"));
        vVO.setExternalSystemStatus(output.getString("EXTERNAL_SYSTEM_STATUS"));
        vVO.setVendorId(output.getString("VENDOR_ID"));
        vVO.setShippingSystem(output.getString("SHIPPING_SYSTEM"));
        vVO.setTrackingNumber(output.getString("TRACKING_NUMBER"));
        vVO.setPrintedStatusDate(getUtilDate(output.getObject("PRINTED")));
        vVO.setShippedStatusDate(getUtilDate(output.getObject("SHIPPED")));
        vVO.setCancelledStatusDate(getUtilDate(output.getObject("CANCELLED")));
        vVO.setRejectedStatusDate(getUtilDate(output.getObject("REJECTED")));
        vVO.setFinalCarrier(output.getString("FINAL_CARRIER"));
        vVO.setFinalShipMethod(output.getString("FINAL_SHIP_METHOD"));
        vVO.setSdsStatus(output.getString("SDS_STATUS"));
        vVO.setForcedShipment(output.getString("FORCED_SHIPMENT"));
        vVO.setRatedShipment(output.getString("RATED_SHIPMENT"));
        vVO.setBatchNumber(output.getString("BATCH_NUMBER"));
        vVO.setBatchSequence(output.getString("BATCH_SEQUENCE"));
        vVO.setLastScan(getUtilDate(output.getObject("LAST_SCAN")));
        vVO.setDeliveryScan(getUtilDate(output.getObject("DELIVERY_SCAN")));
        vVO.setGetSdsResponse(output.getString("GET_SDS_RESPONSE"));
        vVO.setZoneJumpFlag(output.getString("ZONE_JUMP_FLAG"));
        vVO.setZoneJumpLabelDate(getUtilDate(output.getObject("ZONE_JUMP_LABEL_DATE")));
        vVO.setZoneJumpTrailerNumber(output.getString("ZONE_JUMP_TRAILER_NUMBER"));

        returnList.add(vVO); 
      }


    }

    return returnList;
  }

    /**
     * Call stored procedure ORDER_MESG_PKG.GET_LIFECYCLE_BY_ID 
     * and return order lifecycle data for the given mercury id 
     * @param mercuryId - String
     * @return Document
     * @throws Exception
     */
    public Document getOrderLifecycleMessages(String mercuryId) throws Exception {
    
        logger.debug("Entering getOrderLifecycleMessages(" + mercuryId + ")");
        DataRequest request = new DataRequest();
        Document lifecycleXML = null;

        try {
            HashMap inputParams = new HashMap();
            inputParams.put("IN_MERCURY_ID", mercuryId);

            /* build DataRequest object */
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID("GET_LIFECYCLE_BY_ID");

            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            lifecycleXML = (Document) dau.execute(request);

        } catch (Exception e) {
            logger.error(e);
        } finally {
            logger.debug("Exiting getOrderLifecycleMessages()");
        }   

         return lifecycleXML;
    }
    
    /**
     * This method retrieves default sending florist number details from
     * ftd_apps.company_master table for given order detail id
     *
     * @param String orderDetailId
     * @return MessageVO
     */
      public MessageVO retrieveDefaultFloristDetails(String orderDetailId) throws Exception {
          DataRequest dataRequest = new DataRequest();
          MessageVO messageVO = new MessageVO();
          CachedResultSet outputs = null;

          logger.info("retrieveDefaultFloristDetails ( String orderDetailId :" + 
        		  orderDetailId + " ):: String");
          /* setup store procedure input parameters */
          HashMap inputParams = new HashMap();
          inputParams.put("IN_ORDER_DETAIL_ID", orderDetailId);

          /* build DataRequest object */
          dataRequest.setConnection(dbConnection);
          dataRequest.setStatementID("GET_SENDING_FLORIST_BY_ID");
          dataRequest.setInputParams(inputParams);

          /* execute the store procedure */
          DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
          outputs = (CachedResultSet)dataAccessUtil.execute(dataRequest);
          /* populate object */
          while (outputs.next()) {
              messageVO.setSendingFloristCode(outputs.getString("SENDING_FLORIST_NUMBER"));
              messageVO.setSendingFloristCompanyName(outputs.getString("COMPANY_NAME"));
          }
          return messageVO;
      }
      
      /**
       * This method updates the florist selection log id on the mercury.mercury table
       * @param mercuryID
       * @param floristSelectionLogId
       * @throws Exception
       */
        public void updateFloristSelectionLogId(String mercuryId, String floristSelectionLogId) throws Exception
        {         
      	 DataRequest dataRequest = new DataRequest();
           dataRequest.setConnection(dbConnection);
          
           dataRequest.setStatementID("UPDATE_FLORIST_SELECTION_ID");
           Map paramMap = new HashMap();   
           
           paramMap.put("IN_MERCURY_ID",mercuryId);
           paramMap.put("IN_FLORIST_SELECTION_LOG_ID", floristSelectionLogId);
           
           dataRequest.setInputParams(paramMap);
           DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
           Map outputs = (Map) dataAccessUtil.execute(dataRequest);
          
           String status = (String) outputs.get("OUT_STATUS");
           if(status.equals("N"))
           {
               String message = (String) outputs.get("OUT_MESSAGE");
               throw new Exception(message);
           }
        }  
       
        /**
         * This method updates the apollo override flag on the clean.avs_address table
         * @param avsAddressId
         * @param apolloOverrideFlag
         * @throws Exception
         */
          public void updateApolloOverrideFlag(String avsAddressId, String apolloOverrideFlag) throws Exception
          {         
        	 DataRequest dataRequest = new DataRequest();
             dataRequest.setConnection(dbConnection);
            
             dataRequest.setStatementID("UPDATE_APOLLO_OVERRIDE_FLAG");
             Map paramMap = new HashMap();   
             
             paramMap.put("IN_AVS_ADDRESS_ID", avsAddressId);
             paramMap.put("IN_APOLLO_OVERRIDE_FLAG", apolloOverrideFlag);
             
             dataRequest.setInputParams(paramMap);
             DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
             Map outputs = (Map) dataAccessUtil.execute(dataRequest);
            
             String status = (String) outputs.get("OUT_STATUS");
             if(status.equals("N"))
             {
                 String message = (String) outputs.get("OUT_MESSAGE");
                 throw new Exception(message);
             }
          }
          
          /**
           * This method  gives sate code for Vendor orders from venus table .For Mercury we need to write the logic to get the sate code which comes as CITY,STATE,ZIP 
           * @param messageId
           * @param messagetType
           * @param Connection
           * @throws NullPointerException or Exception
           */
          public String getStateCode(String messageId,String messagetType,Connection con)throws Exception
        	{
        	  DataRequest dataRequest = new DataRequest();
              dataRequest.setConnection(con);
              dataRequest.setStatementID("GET_STATECODE");
              dataRequest.addInputParam("IN_MESSAGE_ID", messageId);
              dataRequest.addInputParam("IN_MESSAGE_TYPE", messagetType);
              DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
              String  out_state = (String) dataAccessUtil.execute(dataRequest);
              String state = "";//For mercury write the logic for getting sate_code  in place of empty
              try{
              if(!MessagingConstants.MERCURY_MESSAGE.equalsIgnoreCase(messagetType))
            	  state = out_state;
              else
              {
          			state = (null!=out_state && !"".equalsIgnoreCase(out_state))?(out_state.substring(out_state.lastIndexOf(",") + 1, out_state.lastIndexOf(" "))).trim():"";
              }
              }
              catch(Exception e){
            	  logger.error("Error while getting State code from"+messagetType);
              }
              logger.info("state_code:::"+state);
              return state;
        		
        	}
      
}


