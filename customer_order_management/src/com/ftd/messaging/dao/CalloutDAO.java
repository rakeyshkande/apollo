package com.ftd.messaging.dao;
import com.ftd.customerordermanagement.vo.CommentsVO;
import com.ftd.messaging.constants.MessagingConstants;
import com.ftd.messaging.vo.MessageVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;


/**
 * This class contains the business logic to update order comments and
 * delete FTD queue with which the order is associated.
 * @author Charles Fox
 */
public class CalloutDAO 
{
    private Logger logger = new Logger("com.ftd.messaging.dao.CalloutDAO");
    private Connection dbConnection = null;

    public CalloutDAO(Connection conn)
    {
        super();
        dbConnection = conn;
    }
    
    /**
     * Call stored procedure COMMENT_HISTORY_PKG. INSERT_COMMENTS to
     * insert order comments.
     * @param comments - CommentsVO
     * @return n/a
     * @throws SAXException
     * @throws IOException
     * @throws SQLException
     * @throws ParserConfigurationException
     * @throws Exception
     */
    public void addOrderComment(CommentsVO comments)
        throws IOException, SAXException, SQLException, 
            ParserConfigurationException, Exception
        {
        if(logger.isDebugEnabled()){
            logger.debug("Entering addOrderComment ");
            logger.debug("Comment : " + comments.getComment());
        }
         DataRequest request = new DataRequest();
         
        try {
            
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put(
                MessagingConstants.INSERT_COMMENTS_IN_CUSTOMER_ID, comments.getCustomerId());
            inputParams.put(
                MessagingConstants.INSERT_COMMENTS_IN_ORDER_GUID, comments.getOrderGuid());
            inputParams.put(
                MessagingConstants.INSERT_COMMENTS_IN_ORDER_DETAIL_ID, comments.getOrderDetailId());
            inputParams.put(
                MessagingConstants.INSERT_COMMENTS_IN_COMMENT_ORIGIN, comments.getCommentOrigin());
            inputParams.put(
                MessagingConstants.INSERT_COMMENTS_IN_REASON, comments.getReason());
            inputParams.put(
                MessagingConstants.INSERT_COMMENTS_IN_DNIS_ID, comments.getDnisId());
            inputParams.put(
                MessagingConstants.INSERT_COMMENTS_IN_COMMENT_TEXT, comments.getComment());
            inputParams.put(
                MessagingConstants.INSERT_COMMENTS_IN_COMMENT_TYPE, comments.getCommentType());
            inputParams.put(
                MessagingConstants.INSERT_COMMENTS_IN_CSR_ID, comments.getUpdatedBy());
            // build DataRequest object
            request.setConnection(dbConnection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID(MessagingConstants.INSERT_COMMENTS_PRODCEDURE);

            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            Map outputs = (Map) dau.execute(request);
                String status = (String) outputs.get(
                    MessagingConstants.INSERT_COMMENTS_OUT_STATUS_PARAM);
                if(status.equals("N"))
                {
                    String message = (String) outputs.get(
                        MessagingConstants.INSERT_COMMENTS_OUT_MESSAGE_PARAM);

                    throw new Exception(message);
                }
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting addOrderComment");
            } 
        }       
    }
    
    
    
   

    
}