package com.ftd.messaging.constants;

public abstract class MessagingConstants
{
    public MessagingConstants()
    {
    }

    /* Order disposition/status */
    public static String ORDER_DISP_PRINTED = "Printed";
    public static String ORDER_DISP_SHIPPED = "Shipped";

    /* Venus message */
    public static final String VENUS_STATUS_VERIFIED = "VERIFIED";

    /* Shipping system */
    public static String SHIPPING_SYSTEM_FTD_WEST = "FTD WEST"; 
    public static String SHIPPING_SYSTEM_SDS = "SDS";
     

    /* Data Access Util constants */
    public static String OUT_PARAMETERS = "out-parameters";

    /* Generic Action parameters */
    public static final String REQUEST_ORDER_DETAIL_ID = "order_detail_id";
    public static final String REQUEST_SOURCE_CODE = "source_code";
    public static final String INVALID_ACTION_TYPE_MSG
        = " is an invalid action type for action ";
    public static final String DISPLAY_COMMUNICATION = "Display Communication";
    public static final String VIEW_MESSAGE = "View Message";
    public static final String VIEW_MESSAGE_SWITCH = "View Message Switch";
    public static final String SEND_MESSAGE = "Send Message";
    public static final String SEND_PRICE_MESSAGE = "Send Price Message";

    /* Display Communication Constants */
    public static final String REQUEST_PAGE_NUMBER = "page_number";
    public static final String ACTION_DISPLAY_COMMUNICATION = "display_communication";
    public static final String ACTION_DISPLAY_FLORIST_DASHBOARD = "florist_dashboard";
    public static final String NUMBER_OF_RECORDS = "NumberOfRecords";
    public static final String CURRENT_PAGE = "CurrentPage";
    public static final String MAX_PAGES = "NumberOfPages";

    /* Delete Queue Constants */
    public static final String REQUEST_QUEUE_IDS = "queue_ids";
    public static final String ACTION_DISPLAY_QDELETE = "display";
    public static final String ACTION_DELETE_QUEUE = "delete";
    public static final String QUEUE_DELETE_ERROR_MSG = "queue_delete_error_msg";
    public static final String QUEUE_NODE = "queue";
    public static final String QUEUE_MESSAGE_ID = "message_id";
    public static final String QUEUE_LINE_ITEM_NUMBER = "line_item_number";
    public static final String REQUEST_SECURITY_TOKEN = "securitytoken";
    public static final String REQUEST_CONTEXT = "context";

    /* View Message Action constants */
    public static final String REQUEST_MESSAGE_ID = "message_id";
    public static final String REQUEST_MESSAGE_TYPE = "message_type";
    public static final String VIEW_MESSAGE_PAGE_TITLE = "view_message_title";
    public static final String ACTION_VIEW_MESSAGE = "view";

   /* View Message Switch Action constants */
    public static final String REQUEST_MESSAGE_INFO = "message_info";
    public static final String ACTION_VIEW_MESSAGE_SWITCH = "switch";

   /* View Message constants */
    public static final String GEN_SUBTYPE = "GEN";

    /* Send Price Message constants */
    public static final String ACTION_SEND_PRICE_MSG = "send_price_message";

    /* Send Message constants */
    public static final String ACTION_SEND_MSG = "send_message";

    /*Message Types and attributes*/
    public static final String ASK_MESSAGE = "ASK";
    public static final String ANS_MESSAGE = "ANS";
    public static final String FTD_MESSAGE = "FTD";
    public static final String ADJ_MESSAGE = "ADJ";
    public static final String OTHER_MESSAGE = "OTHER";

    public static final String INBOUND_ASK_ANS = "INASKANS";
    public static final String OUTBOUND_ASK_ANS = "OUTASKANS";
    public static final String SYSTEM_ADJ_MSG = "ADJ";
    public static final String FTD_MSG = "FTD";
    public static final String INBOUND_MSG = "INBOUNDMSG";
    public static final String OUTBOUND_MSG = "OUTBOUNDMSG";


    public static final String PRICE_SUB_TYPE = "P";

    public static final String INBOUND_MESSAGE = "INBOUND";
    public static final String OUTBOUND_MESSAGE = "OUTBOUND";

    public static final String MERCURY_MESSAGE = "Mercury";
    public static final String VENUS_MESSAGE = "Venus";
    public static final String EMAIL_MESSAGE = "Email";
    public static final String LETTER_MESSAGE = "Letter";

  /*  ASK=ASK Message
ANS=Answer Message
ASKP=ASK Price Change
ANSP=Accept Price Change
*/

    /* Messaging config constants */
    public static final String MESSAGING_CONFIG = "customer_order_management_config.xml";
    public static final String DEFAULT_PAGE_NUMBER = "DEFAULT_PAGE_NUMBER";
    public static final String MAX_MSG_DISPLAY = "MAX_MSG_DISPLAY";

    /* Constants for CLEAN.ORDER_MESG_PKG.GET_COMMUNICATION_MESSAGES
     * stored proc */
     public static final String GET_COMMUNICATION_MESSAGES =
        "GET_COMMUNICATION_MESSAGES";
     public static final String GET_COMMUNICATION_MESSAGES_IN_ORDER_DETAIL_ID =
        "IN_ORDER_DETAIL_ID";
     public static final String GET_COMMUNICATION_MESSAGES_IN_START_POSITION =
        "IN_START_POSITION";
     public static final String GET_COMMUNICATION_MESSAGES_IN_MAX_NUMBER_RETURNED =
        "IN_MAX_NUMBER_RETURNED";
     public static final String GET_COMMUNICATION_MESSAGES_OUT_CUR =
        "OUT_CUR";
     public static final String GET_COMMUNICATION_MESSAGES_OUT_NUMBER_OF_RECORDS =
        "OUT_NUMBER_OF_RECORDS";
     public static final String GET_COMMUNICATION_MESSAGES_OUT_CUST_ORDER_CUR =
        "OUT_CUST_ORDER_CUR";

     public static final String GET_COMMUNICATION_MESSAGES_TOP_NODE =
        "messages";
     public static final String GET_COMMUNICATION_MESSAGES_BOTTOM_NODE =
        "message";

    /* Constants for CLEAN.ORDER_MESG_PKG.GET_COMM_ITEM_IN_QUEUE
     * stored proc */
     public static final String GET_COMM_ITEM_IN_QUEUE =
        "GET_COMM_ITEM_IN_QUEUE";
     public static final String GET_COMM_ITEM_IN_QUEUE_IN_ORDER_DETAIL_ID =
        "IN_ORDER_DETAIL_ID";

    /* Constants for CLEAN.ORDER_MESG_PKG.GET_FLORIST_DASHBOARD
     * stored proc */
     public static final String GET_FLORIST_DASHBOARD =
        "GET_FLORIST_DASHBOARD";
     public static final String GET_FLORIST_DASHBOARD_IN_QUEUE_IN_ORDER_DETAIL_ID =
        "IN_ORDER_DETAIL_ID";

    /* Constants for CLEAN.QUEUE_PKG.GET_QUEUE_RECORD_BY_ORDER
     * stored proc */
     public static final String GET_QUEUE_RECORD_BY_ORDER =
        "GET_QUEUE_RECORD_BY_ORDER";
     public static final String GET_QUEUE_RECORD_BY_ORDER_IN_ORDER_DETAIL_ID =
        "IN_ORDER_NUMBER";
    public static final String GET_QUEUE_RECORD_BY_ORDER_IN_REQUEST_TYPE =
        "IN_REQUEST_TYPE";
     public static final String GET_QUEUE_RECORD_BY_ORDER_OUT_CURSOR =
        "OUT_CURSOR";
    
    public static final String GET_QUEUE_TYPES =
       "GET_QUEUE_TYPES";
    public static final String GET_MQD_QUEUE_TYPES =
       "GET_MQD_QUEUE_TYPES";
    public static final String GET_QUEUE_TYPES_OUT_CURSOR =
       "OUT_CUR";

    /* remove queue entries stored procedure and parameters */
    public static final String DELETE_QUEUE_RECORD =
    "DELETE_QUEUE_RECORD";
    public static final String DELETE_QUEUE_RECORD_IN_MESSAGE_ID
    = "IN_MESSAGE_ID";
    public static final String DELETE_QUEUE_RECORD_OUT_STATUS
    = "OUT_STATUS";
    public static final String DELETE_QUEUE_RECORD_OUT_MESSAGE
    = "OUT_ERROR_MESSAGE";
    
    public static final String REPROCESS_ORDER =
    "REPROCESS_ORDER";
    public static final String REPROCESS_ORDER_IN_ORDER_DETAIL_ID
    = "IN_ORDER_DETAIL_ID";
    public static final String REPROCESS_ORDER_OUT_STATUS
    = "OUT_STATUS";
    public static final String REPROCESS_ORDER_OUT_MESSAGE
    = "OUT_MESSAGE";


    /* Constants for CLEAN.order_mesg_pkg.GET_MESSAGE_DETAIL
     * stored proc */
     public static final String GET_MESSAGE_DETAIL =
        "GET_MESSAGE_DETAIL";
     public static final String GET_MESSAGE_DETAIL_GEN_MESG =
        "GET_MESSAGE_DETAIL_GEN_MESG";
     public static final String GET_MESSAGE_DETAIL_IN_MESSAGE_ID =
        "IN_MESSAGE_ID";
     public static final String GET_MESSAGE_DETAIL_IN_MESSAGE_NUMBER =
        "IN_MESSAGE_NUMBER";
    public static final String GET_MESSAGE_DETAIL_IN_MESSAGE_TYPE =
        "IN_MESSAGE_TYPE";

    /* Constants for CLEAN.QUEUE_PKG.get_message_detail_from_order
     * stored proc */
     public static final String GET_MESSAGE_DETAIL_FROM_FTD = 
        "GET_MESSAGE_DETAIL_FROM_FTD";
     public static final String GET_MESSAGE_DETAIL_FROM_FTD_IN_MESSAGE_ORDER_NUMBER =
        "IN_MESSAGE_ORDER_NUMBER";
    public static final String GET_MESSAGE_DETAIL_FROM_FTD_IN_MESSAGE_TYPE =
        "IN_MESSAGE_TYPE";
    public static final String GET_MESSAGE_DETAIL_FROM_FTD_IN_PRICE_INDICATOR =
        "IN_PRICE_INDICATOR";
     public static final String GET_MESSAGE_DETAIL_FROM_FTD_OUT_CUR =
        "OUT_CUR";

    /* Constants for CLEAN.QUEUE_PKG.GET_MESSAGE_DETAIL_NEW_FTD
     * stored proc */
     public static final String GET_MESSAGE_DETAIL_NEW_FTD =
        "GET_MESSAGE_DETAIL_FROM_ORDER";
     public static final String GET_MESSAGE_DETAIL_NEW_FTD_IN_ORDER_DETAIL_ID =
        "IN_ORDER_NUMBER";
    public static final String GET_MESSAGE_DETAIL_NEW_FTD_IN_MESSAGE_TYPE =
        "IN_MESSAGE_TYPE";
     public static final String GET_MESSAGE_DETAIL_NEW_FTD_OUT_CUR =
        "OUT_CUR";

    /* INSERT_ORDER_FLORIST_USED stored procedure and parameters */
    public static final String INSERT_ORDER_FLORIST_USED =
    "INSERT_ORDER_FLORIST_USED";
    public static final String INSERT_ORDER_FLORIST_USED_IN_ORDER_DETAIL_ID
    = "IN_ORDER_DETAIL_ID";
    public static final String INSERT_ORDER_FLORIST_USED_IN_FLORIST_ID
    = "IN_FLORIST_ID";
    public static final String INSERT_ORDER_FLORIST_USED_OUT_STATUS
    = "OUT_STATUS";
    public static final String INSERT_ORDER_FLORIST_USED_OUT_ERROR_MESSAGE
    = "OUT_ERROR_MESSAGE";

    /* VIEW_PARMS_BY_CONTEXT stored procedure and parameters */
    public static final String VIEW_PARMS_BY_CONTEXT =
    "VIEW_PARMS_BY_CONTEXT";
    public static final String VIEW_PARMS_BY_CONTEXT_CONTEXT_ID
    = "CONTEXT_ID";
    public static final String VIEW_PARMS_BY_CONTEXT_QUEUE_TYPE
    = "QUEUE_TYPE";
    public static final String GET_SENDING_FLORIST_BY_ID = "GET_SENDING_FLORIST_BY_ID";

    public static final String GLOBAL_PARAMS_MESSAGING_CONTEXT = "CUSTOMER_ORDER_MGMT_MESSAGING";
    public static final String GLOBAL_PARAMS_MESSAGING_DEFAULT_FLORIST = "DEFAULT_FLORIST";

    /* INSERT_ORDER_FLORIST_USED stored procedure and parameters */
    public static final String UPDATE_ORDER_DETAILS =
    "UPDATE_ORDER_DETAILS";
    public static final String UPDATE_ORDER_DETAILS_IN_ORDER_DETAIL_ID
    = "IN_ORDER_DETAIL_ID";
    public static final String UPDATE_ORDER_DETAILS_IN_DELIVERY_DATE
    = "IN_DELIVERY_DATE";
    public static final String UPDATE_ORDER_DETAILS_IN_RECIPIENT_ID
    = "IN_RECIPIENT_ID";
    public static final String UPDATE_ORDER_DETAILS_IN_PRODUCT_ID
    = "IN_PRODUCT_ID";
    public static final String UPDATE_ORDER_DETAILS_IN_QUANTITY
    = "IN_QUANTITY";
    public static final String UPDATE_ORDER_DETAILS_IN_COLOR_1
    = "IN_COLOR_1";
    public static final String UPDATE_ORDER_DETAILS_IN_COLOR_2
    = "IN_COLOR_2";
    public static final String UPDATE_ORDER_DETAILS_IN_SUBSTITUTION_INDICATOR
    = "IN_SUBSTITUTION_INDICATOR";
    public static final String UPDATE_ORDER_DETAILS_IN_SAME_DAY_GIFT
    = "IN_SAME_DAY_GIFT";
    public static final String UPDATE_ORDER_DETAILS_IN_OCCASION
    = "IN_OCCASION";
    public static final String UPDATE_ORDER_DETAILS_IN_CARD_MESSAGE
    = "IN_CARD_MESSAGE";
    public static final String UPDATE_ORDER_DETAILS_IN_CARD_SIGNATURE
    = "IN_CARD_SIGNATURE";
    public static final String UPDATE_ORDER_DETAILS_IN_SPECIAL_INSTRUCTIONS
    = "IN_SPECIAL_INSTRUCTIONS";
    public static final String UPDATE_ORDER_DETAILS_IN_RELEASE_INFO_INDICATOR
    = "IN_RELEASE_INFO_INDICATOR";
    public static final String UPDATE_ORDER_DETAILS_IN_FLORIST_ID
    = "IN_FLORIST_ID";
    public static final String UPDATE_ORDER_DETAILS_IN_SHIP_METHOD
    = "IN_SHIP_METHOD";
    public static final String UPDATE_ORDER_DETAILS_IN_SHIP_DATE
    = "IN_SHIP_DATE";
    public static final String UPDATE_ORDER_DETAILS_IN_ORDER_DISP_CODE
    = "IN_ORDER_DISP_CODE";
    public static final String UPDATE_ORDER_DETAILS_IN_SECOND_CHOICE_PRODUCT
    = "IN_SECOND_CHOICE_PRODUCT";
    public static final String UPDATE_ORDER_DETAILS_IN_ZIP_QUEUE_COUNT
    = "IN_ZIP_QUEUE_COUNT";
    public static final String UPDATE_ORDER_DETAILS_IN_RANDOM_WEIGHT_DIST_FAILURES
    = "IN_RANDOM_WEIGHT_DIST_FAILURES";
    public static final String UPDATE_ORDER_DETAILS_IN_UPDATED_BY
    = "IN_UPDATED_BY";
    public static final String UPDATE_ORDER_DETAILS_IN_DELIVERY_DATE_RANGE_END
    = "IN_DELIVERY_DATE_RANGE_END";
    public static final String UPDATE_ORDER_DETAILS_IN_SCRUBBED_ON
    = "IN_SCRUBBED_ON";
    public static final String UPDATE_ORDER_DETAILS_IN_SCRUBBED_BY
    = "IN_SCRUBBED_BY";
    public static final String UPDATE_ORDER_DETAILS_IN_ARIBA_UNSPSC_CODE
    = "IN_ARIBA_UNSPSC_CODE";
    public static final String UPDATE_ORDER_DETAILS_IN_ARIBA_PO_NUMBER
    = "IN_ARIBA_PO_NUMBER";
    public static final String UPDATE_ORDER_DETAILS_IN_ARIBA_AMS_PROJECT_CODE
    = "IN_ARIBA_AMS_PROJECT_CODE";
    public static final String UPDATE_ORDER_DETAILS_IN_ARIBA_COST_CENTER
    = "IN_ARIBA_COST_CENTER";
    public static final String UPDATE_ORDER_DETAILS_IN_SIZE_INDICATOR
    = "IN_SIZE_INDICATOR";
    public static final String UPDATE_ORDER_DETAILS_IN_MILES_POINTS
    = "IN_MILES_POINTS";
    public static final String UPDATE_ORDER_DETAILS_IN_SUBCODE
    = "IN_SUBCODE";
    public static final String UPDATE_ORDER_DETAILS_OUT_STATUS
    = "OUT_STATUS";
    public static final String UPDATE_ORDER_DETAILS_OUT_MESSAGE
    = "OUT_MESSAGE";

    public static final String MERCURY_MESSAGE_VERIFIED = "VERIFIED";
    public static final String MERCURY_MESSAGE_MANUAL = "MANUAL";
    
    public static final String CSR_NO_DELETE_AUTHROITY =
        "Csr does not have authority to delete from queues";
    public static final String QUEUE_TAGGED_HIGHER_AUTHROITY =
        "Queue item is tagged by higher authority level";

    /* VIEW_CANCEL_REASON_CODES stored procedure constants */
    public static final String VIEW_CANCEL_REASON_CODES
        = "VIEW_CANCEL_REASON_CODES";
    public static final String VIEW_CANCEL_REASON_CODES_OUT_CUR
        = "OUT_CUR";
    /* VIEW_COMPLAINT_COMM_TYPES */
    public static final String VIEW_COMPLAINT_COMM_TYPE
        = "REFUND_VIEW_COMPLAINT_COMM_TYPE";

    /* VIEW_ADJ_REASON_CODES stored procedure constants */
    public static final String VIEW_ADJ_REASON_CODES
        = "VIEW_ADJ_REASON_CODES";
    public static final String VIEW_ADJ_REASON_CODES_OUT_CUR
        = "OUT_CUR";

    /* VIEW_HOT_KEY_MESSAGES stored procedure constants */
    public static final String VIEW_HOT_KEY_MESSAGES
        = "VIEW_HOT_KEY_MESSAGES";
    public static final String VIEW_HOT_KEY_MESSAGES_OUT_CUR
        = "OUT_CUR";

    /* remove queue entries stored procedure and parameters */
    public static final String QUEUE_REMOVE_QUEUE 
        = "DELETE_QUEUE_RECORD";
    public static final String REMOVE_QUEUE_IN_MESSAGE_ID
        = "IN_MESSAGE_ID";
    public static final String REMOVE_QUEUE_OUT_STATUS 
        = "OUT_STATUS";
    public static final String REMOVE_QUEUE_OUT_OUT_MESSAGE 
        = "OUT_ERROR_MESSAGE";

    /*  Insert Comments Stored Procedure and parameters */
    public static final String INSERT_COMMENTS_PRODCEDURE 
        = "INSERT_COMMENTS";
    public static final String INSERT_COMMENTS_IN_CUSTOMER_ID 
    = "IN_CUSTOMER_ID";
    public static final String INSERT_COMMENTS_IN_ORDER_GUID = "IN_ORDER_GUID";
    public static final String INSERT_COMMENTS_IN_ORDER_DETAIL_ID 
    = "IN_ORDER_DETAIL_ID";
    public static final String INSERT_COMMENTS_IN_COMMENT_ORIGIN 
    = "IN_COMMENT_ORIGIN";
    public static final String INSERT_COMMENTS_IN_REASON= "IN_REASON";
    public static final String INSERT_COMMENTS_IN_DNIS_ID = "IN_DNIS_ID";
    public static final String INSERT_COMMENTS_IN_COMMENT_TEXT 
    = "IN_COMMENT_TEXT";
    public static final String INSERT_COMMENTS_IN_COMMENT_TYPE 
    = "IN_COMMENT_TYPE";
    public static final String INSERT_COMMENTS_IN_CSR_ID = "IN_CSR_ID";    
    public static final String INSERT_COMMENTS_OUT_STATUS_PARAM = "OUT_STATUS";
    public static final String INSERT_COMMENTS_OUT_MESSAGE_PARAM 
    = "OUT_MESSAGE";
    
    public static final String CancelReasonCodeHandler = "CANCEL_REASON_CODE";
    public static final String CancelComplaintCommHandler = "CANCEL_COMPLAINT_COMM_TYPE";
    public static final String AdjustmentReasonCodeHandler = "ADJUSTMENT_REASON_CODE";
    public static final String HotKeyMessageHandler = "HOT_KEY_MESSAGE";

    /* Send message constants */
    public static final String MESSAGE_COMMENTS = "message_comment";
    public static final String MESSAGE_OPERATOR = "message_operator";
    public static final String MESSAGE_OLD_PRICE = "message_old_price";
    public static final String MESSAGE_PRICE = "message_price";
   // public static final String MESSAGE_SENDING_FLORIST = "sending_florist";
    //public static final String MESSAGE_FILLING_FLORIST = "filling_florist_code";
    public static final String APPROVAL_IDENTITY_ID = "manager_code";
    public static final String APPROVAL_IDENTITY_PASSWORD = "manager_passwd";

    public static final String MESSAGE_COMBINED_RPT_NUM = "combined_report_number";
    public static final String MESSAGE_ROF_NUMBER = "rof_number";
    public static final String MESSAGE_ADJ_REASON_CODE = "adj_reason_code";
    public static final String MESSAGE_LINE_NUMBER =  "message_line_number";
    public static final String MESSAGE_ORDER_DATE = "order_date";
    public static final String MESSAGE_RECIPIENT = "recipient";
    public static final String MESSAGE_ADDRESS = "address";
    public static final String MESSAGE_CITY_STATE_ZIP = "city_state_zip";
    public static final String MESSAGE_PHONE_NUMBER = "phone_number";
    public static final String MESSAGE_DELIVERY_DATE = "delivery_date";
    public static final String MESSAGE_DELIVERY_DATE_TEXT = "delivery_date_text";
    public static final String MESSAGE_FIRST_CHOICE = "first_choice";
    public static final String MESSAGE_SECOND_CHOICE = "second_choice";
    public static final String MESSAGE_CARD_MESSAGE = "card_message";
    public static final String MESSAGE_OCCASION = "occasion";
    public static final String MESSAGE_SPECIAL_INSTRUCTIONS = "special_instructions";
    public static final String MESSAGE_PRIORITY = "priority";
    public static final String MESSAGE_REFERENCE_NUMBER = "reference_number";
    public static final String MESSAGE_PRODUCT_ID = "product_id";
    public static final String MESSAGE_ZIP_CODE = "zip_code";
    public static final String MESSAGE_COMP_ORDER = "comp_order";
    public static final String MESSAGE_REQUIRE_CONFIRMATION = "require_confirmation";

    public static final String MERCURY_ORDER_STATUS = "MO";
    public static final String MERCURY_REJECTED_STATUS = "MR";

    public static final String MESSAGE_DATE_FORMAT =  "yyyy-MM-dd";//2004-10-11
    public static final String MESSAGE_END_DELIVERY_DATE_FORMAT = "MMM dd yyyy";
    public static final String ATTR_MESSAGE_STATUS_VO="MESSAGE_STATUS_VO";



    public static final String SUCCESS="success";

    public static final String ERROR="Error";

    public static final String HIDDEN_FIELDS="hidden-fields";

    public static final String HIDDEN_FIELD="hidden-field";

    public static final String PAGE_TITLE="page_title";

    public static final String INIT_REASON_TEXT="init_reason_text";


    public static final String MESSAGING_IFRAME_TEMPLATE="messaging_forward_iframe";

    public static final String ORDER_TYPE = "order_type";

    public static final String MESSAGE_TYPE="msg_message_type";

    public static final String MESSAGE_SUB_TYPE="msg_sub_type";

    public static final String MESSAGE_DETAIL_TYPE="msg_message_detail_type";
    
    public static final String ADJ_CAN_REASON_CODE="adj_cancel_reason_code";
  
    public static final String MESSAGE_TYPES="message_types";

    public static final String MESSAGE_ORDER_TYPE="msg_order_type";

    public static final String MESSAGE_GUID="msg_guid";
    
    public static final String FTD_MESSAGE_STATUS="msg_ftd_message_status";
    
    public static final String FTD_MESSAGE_CANCELLED="msg_ftd_message_cancelled";

    public static final String MESSAGE_EXTERNAL_ORDER_NUMBER="msg_external_order_number";

    public static final String MESSAGE_MASTER_ORDER_NUMBER="msg_master_order_number";

    public static final String MESSAGE_LIFECYCLE_DELIVERED_STATUS="msg_lifecycle_delivered_status";
    public static final String MESSAGE_STATUS_URL="msg_status_url";
    public static final String MESSAGE_STATUS_URL_ACTIVE="msg_status_url_active";
    public static final String MESSAGE_STATUS_URL_EXPIRED_MSG="msg_status_url_expired_msg";
    public static final String MESSAGE_CUSTOMER_ID="msg_customer_id";
    public static final String MESSAGE_FILTER_ORDER_DATE="msg_order_date";

    public static final String MESSAGE_EMAIL_INDICATOR="msg_email_indicator";
    
    public static final String MESSAGE_LETTER_INDICATOR="msg_letter_indicator";

    public static final String TYPE="type";

    public static final String FORWARDING_ACTION="forwarding_action";
    
    public static final String ACTION_TYPE="action_type";
  
    public static final String SUBSEQUENT_ACTION="subsequent_action";

    public static final String MESSAGE_ORDER_NUMBER="msg_message_order_number";

    public static final String ERROR_MESSAGE="error_message";

    public static final String CANCELLED_ERROR="already_cancel_error";
    
    public static final String ZJ_ORDER_IN_TRANSIT="zj_order_printed_and_passed_label_date";

    public static final String REJECTED_ERROR="already_rejected_error";

    public static final String HAS_LIVE_FTD_ERROR="has_live_ftd_error";

    public static final String NEVER_ATTEMPTED_FTD_ERROR="never_attempted_ftd_error";

    public static final String PAST_DELIVERY_DATE_ERROR="past_delivery_date_error";

    public static final String A_TYPE_REFUND_ERROR="a_type_refund_error";
    
    public static final String VENDOR_ERROR="vendor_error";

    public static final String PERMISSION_VIEW = "View";

    public static final String MESSAGE_CSR_ID = "msg_csr_id";

    public static final String COMP_ORDER_INDICATOR = "C";

    /* Action Forward values */
    public static final String ACTION_FORWARD_COMMUNICATION="communication";
    public static final String ACTION_FORWARD_QUEUE="queue";
    public static final String ACTION_FORWARD_ADJUSTMENT="adjustment";
    public static final String ACTION_FORWARD_CALLOUT="call_out";
    public static final String ACTION_PREPARE_MESSAGE="prepare_message";
    public static final String ACTION_PAST_DELIVERY_MONTH="past_delivery_month";
    public static final String ACTION_CANCEL_VENDOR="cancel_vendor";
    public static final String ACTION_CANCEL_FTDM="cancel_ftdm";
    public static final String ACTION_VENDOR_PRINTED="vendor_printed";
    public static final String ACTION_FTDM_PAST_DELIVERY_MONTH="ftdm_past_delivery_month";
	public static final String ACTION_FORWARD_SEND_FTD="send_ftd";
    /* Request Parameter Names */
    public static final String PARAMETER_LOOKUP = "?";
    public static final String MESSAGE_ORDER_STATUS = "msg_order_status";
    public static final String REQUEST_PARAMETER_COMP_ORDER= "comp_order";

    /* Role Constants */
    public static final String ADJUSTMENT_SECURITY_RESOURCE = "ADJ";

    /* Request Attribute Names */
    public static final String REQUEST_ATTRIBUTE_ACTION = "action";
    
    public static final String REQUEST_SENDING_FLORIST = "sending_florist_code";
    public static final String REQUEST_FILLING_FLORIST = "filling_florist_code";
    public static final String REQUEST_FTD_MESSAGE_ID = "ftd_message_id";
    public static final String REQUEST_RECIP_ZIP_CODE = "recip_zip_code";
    public static final String REQUEST_FLORIST_SELECTION_LOG_ID = "florist_selection_log_id";
    public static final String REQUEST_ZIP_CITY_FLAG = "zip_city_flag";
    
    public static final String REQUEST_COMPLAINT_COMM_ORIGIN_TYPE_ID = "complaint_comm_origin_type_id";
    public static final String REQUEST_COMPLAINT_COMM_NOTIFICATION_TYPE_ID = "complaint_comm_notification_type_id";
    
    
    /* Call out script constants */
    public static final String CALL_OUT_PAGE_TITLE = "call_out_page_title";
    public static final String CALL_OUT_DISPOSITION = "call_out_disposition";
    public static final String CALL_OUT_COMMENT_TYPE = "Order";
    public static final String CALL_OUT_SUCCESS
        = "call_out_disposition_success";
    public static final String CALL_OUT_FAILURE
        = "call_out_disposition_failure";
        
     public static final String CALL_OUT_NOREACH
        = "call_out_disposition_unreachable";
    
        
    public static final String REQUEST_QUEUE_MESSAGE_ID = "queue_id";
    public static final String REQUEST_TAGGED = "tagged";
    public static final String REQUEST_ATTACHED = "attached";
    public static final String REQUEST_T_COMMENT_ORIGIN_TYPE = "t_comment_origin_type";
    public static final String REQUEST_T_ENTITY_HISTORY_ID = "t_entity_history_id";
    public static final String REQUEST_QUEUE_TYPE = "queue_type";
    public static final String REQUEST_QUEUE_IND = "queue_ind";
    public static final String REQUEST_USER = "user";
    public static final String REQUEST_GROUP = "group";
    public static final String REQUEST_PERSON_SPOKE_TO = "person_spoke_to";

    //public static final String REQUEST_ORDER_DELIVERY_DATE = "order_delivery_date";
    public static final String REQUEST_ORDER_DELIVERY_DATE_END = "order_delivery_date_end";
    public static final String REQUEST_ORDER_DATE_TEXT = "order_date_text";
    public static final String REQUEST_OCCASION_DESC = "occasion_desc";

    public static final String REQUEST_RECIPIENT_STREET = "recipient_street";
    public static final String REQUEST_RECIPIENT_NAME = "recipient_name";
    public static final String REQUEST_RECIPIENT_STATE = "recipient_state";
    public static final String REQUEST_RECIPIENT_ZIP = "recipient_zip";
    public static final String REQUEST_RECIPIENT_PHONE = "recipient_phone";
    public static final String REQUEST_RECIPIENT_CITY = "recipient_city";
    
    /* DELETE_QUEUE_RECORD_NO_AUTH stored procedure and parameters */
    public static final String DELETE_QUEUE_RECORD_NO_AUTH =
    "DELETE_QUEUE_RECORD_NO_AUTH";
    public static final String DELETE_QUEUE_RECORD_NO_AUTH_IN_MESSAGE_ID
    = "IN_MESSAGE_ID";
    public static final String DELETE_QUEUE_RECORD_NO_AUTH_IN_CSR_ID
    = "IN_CSR_ID";
    public static final String DELETE_QUEUE_RECORD_NO_AUTH_OUT_STATUS
    = "OUT_STATUS";
    public static final String DELETE_QUEUE_RECORD_NO_AUTH_OUT_MESSAGE
    = "OUT_ERROR_MESSAGE";

    /* Constants for ftd_apps.florist_query_pkg.GET_FLORIST_NAME_PHONE
     * stored proc */
     public static final String GET_FLORIST_NAME_PHONE =
        "GET_FLORIST_NAME_PHONE";
     public static final String GET_FLORIST_NAME_PHONE_IN_FLORIST_ID =
        "IN_FLORIST_ID";
    
    public static final String DELIVERY_MONTH = "delivery_month";
    public static final String DELIVERY_DATE = "delivery_date";
    
    public static final String DELIVERY_MONTH_END = "delivery_month_end";
    public static final String DELIVERY_DATE_END = "delivery_date_end";
    
    public static final String CALL_OUT_DATE_FORMAT = "MMM dd, yyyy";
    
    
    public static final String FORWARD_DESTINATION="destination";
    
    public static final String NEW_FLORIST_ID="new_florist_id";
    
     public static final String NEW_MESSAGE_TYPE="new_msg_message_type";
     
     public static final String AUTOMATIC_ADJ_FROM_CANCEL_COMMENTS = "ORDER CANCELLED AFTER DELIVERY DATE. AUTO ADJ GENERATED.";
     public static final Double AUTOMATIC_ADJ_FROM_CANCEL_OVER_UNDER_CHARGE = new Double("0.00");
    
    
    /* Constants for CLEAN.QUEUE_DELETE_PKG stored procs */
    public static final String GET_QUEUE_DELETE_HEADER_WITH_COUNT = "GET_Q_DELETE_HEADER_WITH_COUNT";
    public static final String GET_QUEUE_DELETE_HEADER_WITH_DETAILS = "GET_Q_DELETE_HEADER_DETAIL";
    public static final String GET_USERS_FROM_QUEUE_DELETE_HEADER = "GET_USERS_FROM_Q_DELETE_HEADER";
    
    /* queue processing screen related constants*/
    public static final String ALL_OPTION = "All";
    public static final String USER_LIST = "user";
    public final static String QUEUE_BO="QueueBO";
    public final static String SUCCESS_STATUS="SUCCESS";
    public final static String SHOW_STATUS="SHOW_STATUS";
    public final static String FAILURE_STATUS="FAILURE";
    public final static String DUPLICATE_STATUS="DUPLICATE";
    public final static String FAILURE_DISPLAY="FAILED";
    public final static String PAGE_SIZE="20";
    public final static String GLOBAL_PARM_QUEUE_CONFIG = "QUEUE_CONFIG"; 
    public final static String MAX_RECORDS_PER_PAGE = "MAX_RECORDS_PER_PAGE";
    public final static String QUEUE_BATCH_USER_ROLE = "QueueBatchStatusByUser";
    public final static String QUEUE_BATCH_USER_ROLECH_USER_PERMISSION="View";
    public static final String STATE_CODE = "state_code";
    
    public static final String PRO_COMPANY_NAME = "ProFlowers";
    public static final String FTD_COMPANY_NAME = "FTD.COM";
	public static final String PHONE_NUMBER = "PHONE_NUMBER";
	public static final String CALLOUT_PHONE_LABEL = "CALLOUT_PHONE";
    
    
     
}