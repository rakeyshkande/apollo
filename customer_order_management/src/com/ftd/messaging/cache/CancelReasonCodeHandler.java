package com.ftd.messaging.cache;
import com.ftd.customerordermanagement.util.COMDeliveryDateUtil;
import com.ftd.customerordermanagement.vo.OrderDetailVO;
import com.ftd.messaging.constants.MessagingConstants;
import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.cacheMgr.vo.CacheHandlerBase;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.xml.DOMUtil;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import java.util.Set;
import java.util.Iterator;
import org.apache.commons.lang.StringUtils;
import com.ftd.customerordermanagement.constants.COMConstants;

import com.ftd.messaging.vo.CancelReasonVO;

import java.util.Date;

public class CancelReasonCodeHandler extends CacheHandlerBase 
{
  
  private Map cancelReasonCodeMap;
  public CancelReasonCodeHandler()
  {
    super();
  }
  
  
   /**
     * Return the list of hot key messages that need to be cached.
     * @param con - Connection
     * @return Object
     * @throws CacheException
     * @todo - change process based on tech spec
     */
    public Object load(Connection con) throws CacheException{
        Map cachedCancelReasonCodeMap = new HashMap();
        try
        {
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(con);
            dataRequest.setStatementID(MessagingConstants.VIEW_CANCEL_REASON_CODES);
            
            //add additional SQL code and add to xml file
            /* execute the store prodcedure */
            DataAccessUtil dau = DataAccessUtil.getInstance();
            CachedResultSet messageRS = (CachedResultSet) dau.execute(dataRequest);
            while(messageRS.next())
            {  
                CancelReasonVO canVO = new CancelReasonVO();
                canVO.setReasonCode(messageRS.getString("CANCEL_REASON_CODE"));
                canVO.setDescritpion(messageRS.getString("DESCRIPTION"));
                canVO.setAllowBeforeShip(messageRS.getString("ALLOW_BEFORE_SHIP_FLAG"));
                canVO.setAllowAfterShip(messageRS.getString("ALLOW_AFTER_SHIP_FLAG"));
                cachedCancelReasonCodeMap.put(canVO.getReasonCode(), canVO);
            }

        }catch(Exception ex)
        {
          super.logger.error(ex);
          throw new CacheException("Could not set the cached cancelReasonCodeMap."+ex.toString());
        }
       
        return (Object) cachedCancelReasonCodeMap;

    }


    /**
     * Return the list of hot key messages that need to be cached.
     * @param n/a
     * @return List
     * @throws Exception??????????
     * @todo - change process based on tech spec
     */
    public Map getCancelReasonCode(){
        return cancelReasonCodeMap;
    }

    /**
     * Set the cached the list of hot key messages in the cache handler.
     * @param cachedMessageList - Object
     * @return n/a
     * @throws CacheException
     * @todo - change process based on tech spec
     */
    public void setCachedObject(Object cachedCancelReasonMap) 
        throws CacheException{
        
        this.cancelReasonCodeMap=(Map)cachedCancelReasonMap;
        
    }

    public void toXML(Document canCodeDoc)
        throws ParserConfigurationException
    {
        try{
           DOMUtil.addSection(canCodeDoc,"CancelCodes","CancelCode",
                (HashMap) getCancelReasonCode(),false);
        }catch(Exception e){
            super.logger.error(e);
       }

    }
  

    /**
     * refineCancelReasonCodes -  filter unwated reason codes from the cancelReasonCodeMap 
     *                            based on order detail parms
     *                            
     * @param odVO  - OrderDetailVO
     * @return n/a
     * @throws Exception
     */
     
    public void refineCancelReasonCodes(OrderDetailVO odVO, Date dShipDate) throws Exception
    {
      //Create a Calendar Object for todays date
      Calendar cToday = Calendar.getInstance();

      //Since the ship date's time is 00:00:00, set today's date time to 00:00:00 too
      cToday.set(Calendar.HOUR, 0); 
      cToday.set(Calendar.MINUTE, 0); 
      cToday.set(Calendar.SECOND, 0); 
      cToday.set(Calendar.MILLISECOND, 0);
      cToday.set( Calendar.AM_PM, Calendar.AM );

      //get the difference in the ship date and the current date
      int diffInDays = COMDeliveryDateUtil.getDiffInDays(cToday.getTime(), dShipDate);

      //create a new hashmap that will hold the updated cancel reason codes
      Map refinedCancelReasonCodesMap = new HashMap();

      //key and iterator for the existing cancel reason code hashmap
      Set ks = cancelReasonCodeMap.keySet();
      Iterator iter = ks.iterator();
      
      if (  StringUtils.equalsIgnoreCase(odVO.getOrderDispCode(), COMConstants.ORDER_DISPOSITION_PROCESSED) ||
            ( StringUtils.equalsIgnoreCase(odVO.getOrderDispCode(), COMConstants.ORDER_DISPOSITION_PRINTED) &&
              diffInDays > 0
            )
          )
      {
        while(iter.hasNext())
        {
          String key = (String) iter.next();
          CancelReasonVO canVO = (CancelReasonVO)cancelReasonCodeMap.get(key);
          if (StringUtils.equalsIgnoreCase(canVO.getAllowBeforeShip(),"Y"))
          {
            refinedCancelReasonCodesMap.put(key, canVO.getDescritpion());
          }
        }
      }
      else if ( ( StringUtils.equalsIgnoreCase(odVO.getOrderDispCode(), COMConstants.ORDER_DISPOSITION_PRINTED) &&
                  diffInDays <= 0
                ) ||
                StringUtils.equalsIgnoreCase(odVO.getOrderDispCode(), COMConstants.ORDER_DISPOSITION_SHIPED)
              )
      {
        while(iter.hasNext())
        {
          String key = (String) iter.next();
          CancelReasonVO canVO = (CancelReasonVO)cancelReasonCodeMap.get(key);
          if (StringUtils.equalsIgnoreCase(canVO.getAllowAfterShip(),"Y"))
          {
              refinedCancelReasonCodesMap.put(key, canVO.getDescritpion());
          }
        }
      }
      /*
      //implicitly, if the above conditions are not met, the cancel reason codes are set to null
      else
      {}
       */
      
      this.cancelReasonCodeMap = refinedCancelReasonCodesMap; 
       
    }

  
  
}