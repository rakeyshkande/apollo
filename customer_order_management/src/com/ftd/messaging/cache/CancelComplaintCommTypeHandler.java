package com.ftd.messaging.cache;
import com.ftd.messaging.constants.MessagingConstants;
import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.cacheMgr.vo.CacheHandlerBase;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.xml.DOMUtil;
import java.sql.Connection;

import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;

public class CancelComplaintCommTypeHandler extends CacheHandlerBase 
{
  
  private Document cachedCancelComplaintCommTypeDocument;
  public CancelComplaintCommTypeHandler()
  {
    super();
  }
  
  
   /**
     * Return the list of complaint com types in a document that need to be cached.
     * @param con - Connection
     * @return Object
     * @throws CacheException
     * @todo - change process based on tech spec
     */
    public Object load(Connection con) throws CacheException{
        Document cachedCancelComplaintCommTypeDocument;
		try {
			cachedCancelComplaintCommTypeDocument = DOMUtil.getDocument();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        try
        {
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(con);
            dataRequest.setStatementID(MessagingConstants.VIEW_COMPLAINT_COMM_TYPE);
            
            //add additional SQL code and add to xml file
            /* execute the store prodcedure */
            DataAccessUtil dau = DataAccessUtil.getInstance();
            cachedCancelComplaintCommTypeDocument = (Document) dau.execute(dataRequest);

        }catch(Exception ex)
        {
          super.logger.error(ex);
          throw new CacheException("Could not set the cached cancel complaint comm types. "+ex.toString());
        }
       
        return (Object) cachedCancelComplaintCommTypeDocument;

    }


    /**
     * Return the list of hot key messages that need to be cached.
     * @param n/a
     * @return List
     * @throws Exception??????????
     * @todo - change process based on tech spec
     */
    public Document getCancelComplaintCommType(){
        return cachedCancelComplaintCommTypeDocument;
    }

    /**
     * Set the cached the list of complaint comm types in the cache handler.
     * @param cachedMessageList - Object
     * @return n/a
     * @throws CacheException
     * @todo - change process based on tech spec
     */
    public void setCachedObject(Object cachedCancelComplaintCommTypeDocument) 
        throws CacheException{
        
        this.cachedCancelComplaintCommTypeDocument=(Document)cachedCancelComplaintCommTypeDocument;
        
    }

    public void toXML(Document canCodeDoc)
        throws ParserConfigurationException
    {
        try{
            DOMUtil.addSection(canCodeDoc,cachedCancelComplaintCommTypeDocument.getChildNodes());             
        }catch(Exception e){
            super.logger.error(e);
       }

    }
}