package com.ftd.messaging.cache;
import com.ftd.messaging.constants.MessagingConstants;
import com.ftd.messaging.util.MessagingDOMUtil;
import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.cacheMgr.vo.CacheHandlerBase;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.xml.DOMUtil;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;

public class AdjustmentReasonCodeHandler extends CacheHandlerBase 
{
  private Map adjustmentReasonCodeMap;

  public AdjustmentReasonCodeHandler()
  {
        super();

  }
  
  /**
     * Return the map of Adjustment reason codes that need to be cached.
     * @param con - Connection
     * @return Object
     * @throws CacheException
     * @todo - change process based on tech spec
     */
    public Object load(Connection con) throws CacheException{
        Map cachedAdjustmentReasonCodeMap = new HashMap();
        try
        {
           DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(con);
            dataRequest.setStatementID("VIEW_ADJ_REASON_CODES");
            
            //add additional SQL code and add to xml file
            /* execute the store prodcedure */
            DataAccessUtil dau = DataAccessUtil.getInstance();
            CachedResultSet messageRS = (CachedResultSet) dau.execute(dataRequest);
            while(messageRS.next())
            {  

                cachedAdjustmentReasonCodeMap.put((String) messageRS.getObject(1),
                                   (String) messageRS.getObject(2));
            }
            
        }catch(Exception ex)
        {
          super.logger.error(ex);
          throw new CacheException("Could not set the cached AdjustmentReasonCodeMap."+ex.toString());
        }
       
        return cachedAdjustmentReasonCodeMap;

    }


    /**
     * Return the map of Adjustment reason codes that need to be cached.
     * @param n/a
     * @return Map
     * @throws Exception??????????
     * @todo - change process based on tech spec
     */
    public Map getAdjustmentReasonCode(){
        return adjustmentReasonCodeMap;
    }

    /**
     * Set the cached the map of Adjustment reason codes that need to be cached.
     * @param cachedMessageList - Object
     * @return n/a
     * @throws CacheException
     * @todo - change process based on tech spec
     */
    public void setCachedObject(Object cachedAdjustmentReasonMap) 
        throws CacheException{
        
        this.adjustmentReasonCodeMap=(Map)cachedAdjustmentReasonMap;
        
    }

    public Document toXML(Document adjCodeDoc)
        throws ParserConfigurationException
    {
        try{
           DOMUtil.addSection(adjCodeDoc,"AdjustmentCodes","AdjustmentCode",
                (HashMap) getAdjustmentReasonCode(),false);
        }catch(Exception e){
        
           super.logger.error(e);
        }
        
        return adjCodeDoc; 
    }
  
}