package com.ftd.messaging.cache;
import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.cacheMgr.vo.CacheHandlerBase;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.xml.DOMUtil;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;

public class OccasionListCacheHandler extends CacheHandlerBase 
{
  
  private Map occasionMap;
  
  public OccasionListCacheHandler()
  {
  }/**
     * Return the list of hot key messages that need to be cached.
     * @param con - Connection
     * @return Object
     * @throws CacheException
     * @todo - change process based on tech spec
     */
    public Object load(Connection con) throws CacheException{
        Map cachedMap = new HashMap();
        try
        {
           DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(con);
            dataRequest.setStatementID("GET_OCCASION_LIST");
            
            //add additional SQL code and add to xml file
            /* execute the store prodcedure */
            DataAccessUtil dau = DataAccessUtil.getInstance();
            CachedResultSet messageRS = (CachedResultSet) dau.execute(dataRequest);
            while(messageRS.next())
            {  
                
                             
                cachedMap.put(messageRS.getString(1), messageRS.getString(2));
            }
            
        }catch(Exception ex)
        {
          super.logger.error(ex);
          throw new CacheException("Could not set the cached occasion list. "+ex.toString());
        }
       
        return cachedMap;
    }


    /**
     * Return the list of hot key messages that need to be cached.
     * @param n/a
     * @return List
     * @throws Exception??????????
     * @todo - change process based on tech spec
     */
    public Map getOccasionList(){
        return this.occasionMap;
    }

    /**
     * Set the cached the list of hot key messages in the cache handler.
     * @param cachedMessageList - Object
     * @return n/a
     * @throws CacheException
     * @todo - change process based on tech spec
     */
    public void setCachedObject(Object cachedMessageList) 
        throws CacheException{
        
        this.occasionMap=(Map)cachedMessageList;
        
    }
    public void toXML(Document rootDoc)
        throws ParserConfigurationException
    {
        try{
           DOMUtil.addSection(rootDoc,"OccasionList","Occasion",
                (HashMap) this.getOccasionList(),false);
        }catch(Exception e){
             super.logger.error(e);
    
        }
        
       
    }

}