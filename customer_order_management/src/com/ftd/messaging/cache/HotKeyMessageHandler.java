package com.ftd.messaging.cache;

import com.ftd.messaging.constants.MessagingConstants;
import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.cacheMgr.vo.CacheHandlerBase;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.xml.DOMUtil;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;

/**
 * This class extends com.ftd.osp.utilities.cache.vo.CacheHandlerBase. It is 
 * a cache handler is responsible for the following:
 * � Act as a cache loader, to load the hot key messages that need to be cached.
 * � Provide application specific API, to enable access to the cached hot 
 *   key messages.
 * @author Charles Fox
 * @todo check comments, error handling, and message handling
 */
public class HotKeyMessageHandler extends CacheHandlerBase
{
    private Map hotKeyCodeMap;
    
    public HotKeyMessageHandler()
    {
        super();
    }

    /**
     * Return the list of hot key messages that need to be cached.
     * @param con - Connection
     * @return Object
     * @throws CacheException
     * @todo - change process based on tech spec
     */
    public Object load(Connection con) throws CacheException{
        Map cachedHotKeyMessageMap = new HashMap();
        try
        {
           DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(con);
            dataRequest.setStatementID(MessagingConstants.VIEW_HOT_KEY_MESSAGES);
            
            //add additional SQL code and add to xml file
            /* execute the store prodcedure */
            DataAccessUtil dau = DataAccessUtil.getInstance();
            CachedResultSet messageRS = (CachedResultSet) dau.execute(dataRequest);
            while(messageRS.next())
            {  
                
                BigDecimal id = messageRS.getBigDecimal(1);
                
                cachedHotKeyMessageMap.put(id.toString(),
                                   (String) messageRS.getObject(2));
            }
            
        }catch(Exception ex)
        {
          super.logger.error(ex);
          throw new CacheException("Could not set the cached HotKeyMessageMap."+ex.toString());
        }
       
        return cachedHotKeyMessageMap;
    }


    /**
     * Return the list of hot key messages that need to be cached.
     * @param n/a
     * @return List
     * @throws Exception??????????
     * @todo - change process based on tech spec
     */
    public Map getHotKeyMessages(){
        return hotKeyCodeMap;
    }

    /**
     * Set the cached the list of hot key messages in the cache handler.
     * @param cachedMessageList - Object
     * @return n/a
     * @throws CacheException
     * @todo - change process based on tech spec
     */
    public void setCachedObject(Object cachedMessageList) 
        throws CacheException{
        
        this.hotKeyCodeMap=(Map)cachedMessageList;
        
    }
    public void toXML(Document adjCodeDoc)
        throws ParserConfigurationException
    {
        try{
           DOMUtil.addSection(adjCodeDoc,"HotKeys","HotKey",
                (HashMap) getHotKeyMessages(),false);
        }catch(Exception e){
             super.logger.error(e);
    
        }
        
       // return adjCodeDoc; 
    }

}