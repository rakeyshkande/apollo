package com.ftd.messaging.bo;


import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.messaging.constants.MessagingConstants;
import com.ftd.messaging.dao.MessageDAO;
import com.ftd.messaging.util.MessagingHelper;
import com.ftd.messaging.util.MessagingServiceLocator;
import com.ftd.messaging.vo.FTDMessageVO;
import com.ftd.messaging.vo.MessageOrderStatusVO;
import com.ftd.messaging.vo.MessageVO;
import com.ftd.op.mercury.bo.MercuryAPIBO;
import com.ftd.op.mercury.to.CalculatedFieldsTO;
import com.ftd.op.mercury.to.OrderDetailKeyTO;
import com.ftd.op.venus.bo.VenusAPIBO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.handlers.GlobalParmHandler;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.security.SecurityManager;


/**
 * This class contains business logic to retrieve message related
 * information from database.
 * @author Charles Fox
 * @todo check comments, error handling, and message handling
 */
public class PrepareMessageBO
{
    private Logger logger = 
        new Logger("com.ftd.messaging.bo.PrepareMessageBO");
    private Connection connection;
    private String status = null;
    private String productType =null;
    
	public PrepareMessageBO()
    {
    }

    public PrepareMessageBO(Connection conn)

    {
    	this.connection=conn;
    }

    /**
     * Call the getMessageOrderStatus method of MessageDAO,
     * retrieve MessageOrderStatusVO based on the message order number and message type
     * @param orderDetailId 
     * @param messageId - String
     * @param orderDetailId - String
     * @return MessageOrderStatusVO
     * @throws ParserConfigurationException
     * @throws SQLException
     * @throws IOException
     * @throws SAXException
     * @throws Exception????
     * @todo - code and determine exceptions
     */
    public MessageOrderStatusVO retrieveMessageOrderStatus(String messageOrderNumber,
        String messageType, String orderDetailId) throws SAXException, IOException, SQLException, ParserConfigurationException{
        MessageDAO messageDao=new MessageDAO(this.connection);
    	return messageDao.getMessageOrderStatus(messageOrderNumber, messageType, orderDetailId);
    }

    
    
   public Document retrieveMessageForCurrentFlorist(String messageOrderNumber,
            String messageType, HashMap parameters) throws SAXException, IOException, SQLException, ParserConfigurationException, Exception{
            String messageDetailType=(String)parameters.get(MessagingConstants.MESSAGE_DETAIL_TYPE);
            
            
            return this.retrieveMessageForCurrentFlorist(messageOrderNumber, messageType, parameters, false, null, false);
    }
    
    
    
    public Document retrieveANSPMessageForCurrentFlorist(String messageOrderNumber,
            String messageType, HashMap parameters) throws SAXException, IOException, SQLException, ParserConfigurationException, Exception{
            
            return this.retrieveMessageForCurrentFlorist(messageOrderNumber, messageType, parameters, true, null, true);
    }
    
    
    
     public Document retrieveASKPMessageForCurrentFlorist(String messageOrderNumber,
            String messageType, HashMap parameters) throws SAXException, IOException, SQLException, ParserConfigurationException, Exception{
    	 	
            return this.retrieveMessageForCurrentFlorist(messageOrderNumber, messageType, parameters, false, "OUTBOUND", true);
    }
    




    public Document retrieveMessageForCurrentFlorist(String messageOrderNumber,
            String messageType, HashMap parameters, boolean getOldPrice, String messageDirection, boolean verified) throws SAXException, IOException, SQLException, ParserConfigurationException, Exception{
            String messageDetailType=(String)parameters.get(MessagingConstants.MESSAGE_DETAIL_TYPE);
            
            MessageDAO messageDao=new MessageDAO(this.connection);
            MessageVO messageVO=messageDao.getMessageForCurrentFlorist(messageOrderNumber, messageType, getOldPrice, messageDirection, verified);
            String securityToken = (String) parameters.get(MessagingConstants.REQUEST_SECURITY_TOKEN);
            messageVO.setOperator(lookupCurrentUserId(securityToken));
            String cogsFlag = (String)parameters.get("COGS");
            if("Y".equalsIgnoreCase(cogsFlag)){
            
	            double mercValue  = messageVO.getCurrentPrice();
	            double deliveryFee = 0;
	            double cogsMultpr = 0;
	            try{
	            	
	                	GlobalParmHandler globalParms = (GlobalParmHandler)CacheManager.getInstance().getHandler("CACHE_NAME_GLOBAL_PARM");
	                	
	                	deliveryFee = Double.parseDouble(globalParms.getGlobalParm("CUSTOMER_ORDER_MGMT_MESSAGING", "COGS_DELIVERY_FEE").replaceAll("\\s+",""));
	                	cogsMultpr = Double.parseDouble(globalParms.getGlobalParm("CUSTOMER_ORDER_MGMT_MESSAGING", "COGS_MULTIPLIER").replaceAll("\\s+",""));
	                
	            }
	            catch(Exception e){
	            	logger.error(e);
	            	deliveryFee = 10;
	            	cogsMultpr = 35;
	            }
	            
	            double cogs = ((mercValue - deliveryFee)*(cogsMultpr/100)) + deliveryFee;
	            messageVO.setCogs(String.format("%.2f", cogs));
            }
        	
            
            
            return DOMUtil.getDocument(messageVO.toXML());
    }


    public Document retrieveAdjustmentMessageForCurrentFlorist(String messageOrderNumber,
            String messageType, HashMap parameters, boolean system, boolean getOldPrice) throws SAXException, IOException, SQLException, ParserConfigurationException, Exception{
            MessageDAO messageDao=new MessageDAO(this.connection);
            boolean verified=(!system);
            MessageVO messageVO=messageDao.getMessageForCurrentFlorist(messageOrderNumber, messageType, system, "OUTBOUND", verified);

            //Calendar cal = Calendar.getInstance();
            //int month = cal.get(Calendar.MONTH) + 1;
            //messageVO.setCombineRptNum(month + "-1");
            Date deliveryDate = messageVO.getDeliveryDate();
            int month = deliveryDate.getMonth() + 1;
            String sMonth = String.valueOf(month);
            if (sMonth.length() == 1) sMonth = '0' + sMonth;
            messageVO.setCombineRptNum(sMonth + "-1");
            
            String recipName = messageVO.getRecipientName();
            int startPos = recipName.indexOf(' ');
            int endPos = 0;
            if (startPos == -1)
            {
              startPos = 0;
              endPos = 3;
              if (endPos > recipName.length()) endPos = recipName.length();
            } else 
            {
              startPos = startPos + 1;
              endPos = startPos + 3;
              if (endPos > recipName.length()) endPos = recipName.length();
            }
            messageVO.setRecipientName(recipName.substring(startPos,endPos).toUpperCase());

            if(getOldPrice)
            {
              messageVO.setSubType("S");//need old price
            }
                    
            String securityToken = (String) parameters.get(MessagingConstants.REQUEST_SECURITY_TOKEN);
            messageVO.setOperator(lookupCurrentUserId(securityToken));
            return DOMUtil.getDocument(messageVO.toXML());
    }
    
    
    
   private boolean isWthinPastSixMonths(Date deliveryDate) {

		      
       Calendar cal=Calendar.getInstance();
       int year=Calendar.getInstance().get(Calendar.YEAR);
       int month=Calendar.getInstance().get(Calendar.MONTH);
       int day=Calendar.getInstance().get(Calendar.DATE);
       cal.set(year,month, day-1,23,59,59);
       
       Date current=cal.getTime();
       //convert delivery date to the same format.
       cal.setTime(deliveryDate);
       
       Date del=cal.getTime();
       
       if(current.after(del))
       {
           long diff=(current.getTime()-del.getTime())/(3600*24*1000);
           return (diff<=180);
           
       }else
       {
         return false;
       }
    
    
   
	}
    


    public Document retrieveMessageForCurrentOrder(String orderDetailId,
            String messageType, HashMap parameters) throws SAXException, IOException, SQLException,
            ParserConfigurationException, ParseException, Exception{
            
            Document deliveryDatesXML = null;
            boolean compOrder = new Boolean((String) parameters.get(MessagingConstants.REQUEST_PARAMETER_COMP_ORDER)).booleanValue();
            
            Date today=new Date();
            MessageDAO messageDao=new MessageDAO(this.connection);
            FTDMessageVO ftdMsgVO = messageDao.getMessageForCurrentOrder(orderDetailId, messageType);
            String newFloristId=(String)parameters.get(MessagingConstants.NEW_FLORIST_ID);

            logger.debug("new florist id="+newFloristId);
            //reset delivery date to today's date.
            //We should assume it is in the past if it is within 6 months backwards.  
            //For example, if today is November 4th and the order date is october 1st assume the date is in the past and default to today.
            if(ftdMsgVO.getDeliveryDate()!=null){
              logger.debug("Origianl Order delivery date="+ftdMsgVO.getDeliveryDate());
              if(this.isWthinPastSixMonths(ftdMsgVO.getDeliveryDate()))
              {
                   ftdMsgVO.setDeliveryDate(today); 
              }
                            
            }else
            {
              ftdMsgVO.setDeliveryDate(today);
            }
            
            //reset delivery date text.
            SimpleDateFormat format=new SimpleDateFormat("MMM dd");
            String delText=format.format(ftdMsgVO.getDeliveryDate());
            ftdMsgVO.setDeliveryText(delText.toUpperCase());
            logger.debug("new Order delivery date="+ftdMsgVO.getDeliveryText());
            
            if(ftdMsgVO.getEndDeliveryDate()!=null)
            {
              logger.debug("Origianl Order delivery date end="+ftdMsgVO.getEndDeliveryDate());
              SimpleDateFormat sdfEndDeliveryOutput = 
                       new SimpleDateFormat (MessagingConstants.MESSAGE_END_DELIVERY_DATE_FORMAT);   
              Date endDel=sdfEndDeliveryOutput.parse(ftdMsgVO.getEndDeliveryDate());
              if(this.isWthinPastSixMonths(endDel))
              {
                   ftdMsgVO.setEndDeliveryDate(sdfEndDeliveryOutput.format(today)); 
              }
                            
            }
            
            
            if(StringUtils.isNotBlank(newFloristId))
            {
              ftdMsgVO.setFillingFloristCode(newFloristId);
            }
            
            String securityToken = (String) parameters.get(MessagingConstants.REQUEST_SECURITY_TOKEN);
            ftdMsgVO.setOperatorFirstName(lookupCurrentUserFirstName(securityToken));
            ftdMsgVO.setOperator(lookupCurrentUserId(securityToken));
            //add sending florist
            MessageVO messageVO = messageDao.retrieveDefaultFloristDetails(ftdMsgVO.getOrderDetailId());
            ftdMsgVO.setSendingFloristCode(messageVO.getSendingFloristCode());
            ftdMsgVO.setSendingFloristCompanyName(messageVO.getSendingFloristCompanyName());   
            

			// SP-73 if the sending florist company name is PRO - show a different number per content configuration
			String calloutPhone = null;
			try {
				if (MessagingConstants.PRO_COMPANY_NAME.equalsIgnoreCase(messageVO.getSendingFloristCompanyName())) {
					calloutPhone = ConfigurationUtil.getInstance().getContentWithFilter(connection, MessagingConstants.PHONE_NUMBER,
									MessagingConstants.CALLOUT_PHONE_LABEL, messageVO.getSendingFloristCompanyName(), null);				
					ftdMsgVO.setSendingFloristCompanyName(MessagingConstants.FTD_COMPANY_NAME);
				} else {
					calloutPhone = ConfigurationUtil.getInstance().getContentWithFilter(connection, MessagingConstants.PHONE_NUMBER,
									MessagingConstants.CALLOUT_PHONE_LABEL, null, null);				
				}
			} catch (Exception e) {
				logger.error("Unable to get the configured Call out phone from content filter. defaulting", e);
				calloutPhone = "1-800-554-0993";
			}		
			ftdMsgVO.setCalloutPhone(calloutPhone);
			
            //get the first choice, second choice and price.
            logger.info("messageType ="+messageType);
            if(messageType.equalsIgnoreCase(MessagingConstants.MERCURY_MESSAGE)){
              logger.debug("setMercuryFTDMessageVOInfo");
              setMercuryFTDMessageVOInfo(ftdMsgVO, orderDetailId);
            }else
            {
              logger.debug("setVenusFTDMessageVOInfo");
              setVenusFTDMessageVOInfo(ftdMsgVO, orderDetailId);
            }
            
			setLDROrderValues(ftdMsgVO, null);

            Document doc=DOMUtil.getDocument(ftdMsgVO.toXML());

            Document productDoc = this.setProductInfo(ftdMsgVO.getProductId());
                        
            if (productDoc != null)
            {
              NodeList nl = productDoc.getChildNodes();
              DOMUtil.addSection(doc, nl);
            }
            
            //if vendor order then retrieve the valid ship methods and delivery
            //dates for this product
            if(messageType.equalsIgnoreCase(MessagingConstants.VENUS_MESSAGE))
            {
              MessagingHelper helper = new MessagingHelper();
              deliveryDatesXML = helper.retrieveVendorDeliveryDates(true, ftdMsgVO);
                          
              if (deliveryDatesXML != null)
              {
                NodeList nl = deliveryDatesXML.getChildNodes();
                DOMUtil.addSection(doc, nl);
              }
              
            }
            return doc;
    }

	private void setLDROrderValues(FTDMessageVO ftdMsgVO, String actionType) throws Exception {
		//get if the filler code is EFA FTDM florist id
		ConfigurationUtil configUtil = new ConfigurationUtil();
		String efaFTDMFlorist = configUtil.getFrpGlobalParm("SERVICE","EFA_FTDM_FLORIST");
		logger.info("Configured EFA FloristId is ::"+efaFTDMFlorist);
		String isEfaFTDMFlorist = "N";
		if(ftdMsgVO != null && efaFTDMFlorist.equalsIgnoreCase(ftdMsgVO.getFillingFloristCode())){
			logger.info("FillingFloristCode:: "+ftdMsgVO.getFillingFloristCode());
			isEfaFTDMFlorist = "Y";
		}
		logger.info("isEfa Flag :: " + isEfaFTDMFlorist + ", action: " + actionType);
		ftdMsgVO.setIsEfaFTDMFlorist(isEfaFTDMFlorist);
		
		if("Y".equalsIgnoreCase(isEfaFTDMFlorist) && "callout".equalsIgnoreCase(actionType)){
			String ldrPercent = configUtil.getFrpGlobalParm("SERVICE","LDR_PRODUCT_PRICE_PERCENT");
			double ldrPricePercent = 73;
			if(ldrPercent!= null && ldrPercent.length() >0){
				ldrPricePercent = Double.valueOf(ldrPercent);
				logger.info("Configured legacy domestic retrans Price Percent::"+ ldrPricePercent);
			}
	
			double mercValue = ftdMsgVO.getCurrentPrice();
			logger.info("Current mercValue::"+ mercValue);
			double ldrPrice = 0;
			if(mercValue != 0 && mercValue > 0){
				ldrPrice = (mercValue*ldrPricePercent)/100;
			} 
			else {
				mercValue = ftdMsgVO.getOldPrice();
				ldrPrice = (ldrPricePercent*mercValue)/100;
			}
			logger.info("Updated mercValue::"+ ldrPrice);
			ftdMsgVO.setCurrentPrice(ldrPrice);
		}

	}

  void setMercuryFTDMessageVOInfo(FTDMessageVO ftdMsgVO, String orderDetailId) throws Exception, java.rmi.RemoteException
  {
    MercuryAPIBO mercuryAPI = MessagingServiceLocator.getInstance().getMercuryAPI();
    OrderDetailKeyTO keyTO = new OrderDetailKeyTO();
    keyTO.setOrderDetailId(orderDetailId);
    CalculatedFieldsTO fieldsTO = mercuryAPI.getCalculatedFields(keyTO, connection);
    if (fieldsTO != null)
    {
      logger.debug("fields to is not null");
      logger.debug("first choice: "+fieldsTO.getFirstChoice());
      logger.debug("special instructions: "+fieldsTO.getSpecialInstructions());
      ftdMsgVO.setFirstChoice(fieldsTO.getFirstChoice());
      ftdMsgVO.setSubstitution(fieldsTO.getSecondChoice());
      ftdMsgVO.setCurrentPrice(fieldsTO.getPrice().doubleValue());
      ftdMsgVO.setSpecialInstruction(fieldsTO.getSpecialInstructions());
      if (fieldsTO.getCardMessage() != null) {
    	  ftdMsgVO.setCardMessage(fieldsTO.getCardMessage());  
      }
      if (fieldsTO.getOccasion() != null) {
    	  ftdMsgVO.setOccasionCode(fieldsTO.getOccasion());  
      }
    }
  }
  
  
  
  
  void setVenusFTDMessageVOInfo(FTDMessageVO ftdMsgVO, String orderDetailId) throws Exception, java.rmi.RemoteException
  {
    ftdMsgVO.setSubstitution("NONE");
    VenusAPIBO venusAPI = MessagingServiceLocator.getInstance().getVenusAPI();
    com.ftd.op.venus.to.OrderDetailKeyTO keyTO = new com.ftd.op.venus.to.OrderDetailKeyTO();
    keyTO.setOrderDetailId(orderDetailId);
    //call the getCompVendorCalculatedFields method so the first choice String that gets 
    //returned will only return the product id and product description, not the current ship method
    com.ftd.op.venus.to.CalculatedFieldsTO fieldsTO = venusAPI.getCompVendorCalculatedFields(keyTO, connection);
    if (fieldsTO != null)
    {
      ftdMsgVO.setFirstChoice(fieldsTO.getFirstChoice());
      
      if(fieldsTO.getPrice()!=null){
        logger.error("Venus fieldsTO.getPrice() is "+fieldsTO.getPrice());
        ftdMsgVO.setCurrentPrice(fieldsTO.getPrice().doubleValue());
        
      }else
      {
        logger.error("Venus fieldsTO.getPrice() is null.");
      }
     
    }else
    {
      logger.error("Venus fieldsTO is null "+orderDetailId);
    }
  }
  
 
     /**
     * Utilize Security Manager to obtain user�s id based on the security token.
     * @param securityToken - String
     * @return String
     * @throws Exception????
     * @todo - Need code from Mike Kruger
     */
    public String lookupCurrentUserId(String securityToken) throws Exception {


        String userId = "";

            if (SecurityManager.getInstance().getUserInfo(securityToken)!=null)
            {
                userId = SecurityManager.getInstance().getUserInfo(securityToken).getUserID();
            }



        return userId;
    }

 /**
     * Call getMessageForCurrentOrder method of MessageDAO to obtain a Document 
     * that contains message related data for the current florist. Call 
     * SercurityManager to obtain the user�s first name. Add the user first 
     * name to the Document object. 
     * @param securityToken - String
     * @return String
     * @throws Exception????
     * @todo - Need code from Mike Kruger
     */
    public Document retrieveMessageForCallout (String orderDetailId, 
        String messageType, String securityToken,HttpServletRequest request) 
        throws SAXException, IOException, SQLException,
            ParserConfigurationException, ParseException, Exception
        {

            if (logger.isDebugEnabled()) {
                logger.debug("Entering retrieveMessageForCallout");
            }
            String ordGUID=null;
            HashMap parameters=null;
            if(request.getAttribute(COMConstants.CONS_APP_PARAMETERS)!=null)
            {
              parameters=(HashMap)request.getAttribute(COMConstants.CONS_APP_PARAMETERS);
              if(parameters.get(MessagingConstants.MESSAGE_GUID)!=null)
              {
                ordGUID=(String)parameters.get(MessagingConstants.MESSAGE_GUID);
              }
            }else
            {
              parameters=new HashMap();
            }
            
            
            FTDMessageVO ftdMsgVO = new FTDMessageVO();
            try{
                MessageDAO messageDao=new MessageDAO(this.connection);
                ftdMsgVO = messageDao.getMessageForCurrentOrder(orderDetailId, messageType);
                ftdMsgVO.setOperatorFirstName(lookupCurrentUserFirstName(securityToken));
                ftdMsgVO.setOperator(lookupCurrentUserId(securityToken));
                //add sending florist
                MessageVO messageVO = messageDao.retrieveDefaultFloristDetails(orderDetailId);
                ftdMsgVO.setSendingFloristCode(messageVO.getSendingFloristCode());
                ftdMsgVO.setSendingFloristCompanyName(messageVO.getSendingFloristCompanyName());
                
				// SP-73 if the sending florist company name is PRO - show a different number per content configuration
				String calloutPhone = null;
				try {
					if (MessagingConstants.PRO_COMPANY_NAME.equalsIgnoreCase(messageVO.getSendingFloristCompanyName())) {
						calloutPhone = ConfigurationUtil.getInstance().getContentWithFilter(connection, MessagingConstants.PHONE_NUMBER,
								MessagingConstants.CALLOUT_PHONE_LABEL, messageVO.getSendingFloristCompanyName(), null);					
						ftdMsgVO.setSendingFloristCompanyName(MessagingConstants.FTD_COMPANY_NAME);				
					} else {
						calloutPhone = ConfigurationUtil.getInstance().getContentWithFilter(connection,
										MessagingConstants.PHONE_NUMBER, MessagingConstants.CALLOUT_PHONE_LABEL, null, null);					
					}
				} catch (Exception e) {
					logger.error("Unable to get the configured Call out phone from content filter. defaulting" , e);
					calloutPhone = "1-800-554-0993";				
				}			
	            ftdMsgVO.setCalloutPhone(calloutPhone);  
            
                setFillingFloristInfo(ftdMsgVO, request);
                //get the first choice, second choice and price.
             
                setMercuryFTDMessageVOInfo(ftdMsgVO, orderDetailId);
                //check for order ids.
                if(StringUtils.isEmpty(ordGUID))
                {
                  messageDao.setOrderIdsAndFlags(ftdMsgVO);
                  //add ids to the parameters
                  parameters.put(MessagingConstants.MESSAGE_GUID, ((ftdMsgVO.getOrderGUID())!=null?ftdMsgVO.getOrderGUID().trim():""));
                  parameters.put(MessagingConstants.MESSAGE_EXTERNAL_ORDER_NUMBER, ((ftdMsgVO.getExternalOrderNumber())!=null?ftdMsgVO.getExternalOrderNumber().trim():""));
                  parameters.put(MessagingConstants.MESSAGE_MASTER_ORDER_NUMBER, ((ftdMsgVO.getMasterOrderNumber())!=null?ftdMsgVO.getMasterOrderNumber().trim():""));
                  parameters.put(MessagingConstants.MESSAGE_EMAIL_INDICATOR, ((ftdMsgVO.getEmailFlag()!=null&&ftdMsgVO.getEmailFlag().trim().equalsIgnoreCase("Y"))?"true":"false"));
                  parameters.put(MessagingConstants.MESSAGE_LETTER_INDICATOR, ((ftdMsgVO.getLetterFlag()!=null&&ftdMsgVO.getLetterFlag().trim().equalsIgnoreCase("Y"))?"true":"false"));
                  parameters.put(MessagingConstants.MESSAGE_CUSTOMER_ID, (ftdMsgVO.getCustomerID()!=null?ftdMsgVO.getCustomerID().trim():""));
                  request.setAttribute(COMConstants.CONS_APP_PARAMETERS, parameters);
                }
                
                setLDROrderValues(ftdMsgVO, "callout");
                logger.info("ftdMsgVO- EFA flag ::"+ftdMsgVO.getIsEfaFTDMFlorist()+", updated merc value "+ftdMsgVO.getCurrentPrice());
                
            } finally {
                if (logger.isDebugEnabled()) {
                    logger.debug("Exiting retrieveMessageForCallout");
                }
            }
            
            return DOMUtil.getDocument(ftdMsgVO.toXML());
        }
        
        
        private void setFillingFloristInfo(FTDMessageVO ftdMsgVO, HttpServletRequest request) throws Exception
        {
          
          if(StringUtils.isNotBlank(request.getParameter(MessagingConstants.REQUEST_FILLING_FLORIST))){
                   ftdMsgVO.setFillingFloristCode(request.getParameter(MessagingConstants.REQUEST_FILLING_FLORIST).trim().toUpperCase());
                   MessageDAO msgDAO = new MessageDAO(connection);
                   msgDAO.retrieveFloristInformation(ftdMsgVO);
                }
        }
        

 /**
     * Call getMessageForCurrentOrder method of MessageDAO to obtain a Document 
     * that contains message related data for the current florist. Call 
     * SercurityManager to obtain the user�s first name. Add the user first 
     * name to the Document object. 
     * @param securityToken - String
     * @return String
     * @throws Exception????
     * @todo - Need code from Mike Kruger
     */
    public Document createMessageForCallout(HttpServletRequest request, 
        HashMap parameters) 
        throws SAXException, IOException, SQLException,
            ParserConfigurationException, ParseException, Exception
        {

            if (logger.isDebugEnabled()) {
                logger.debug("Entering createMessageForCallout");
            }
            FTDMessageVO ftdMsgVO = new FTDMessageVO();
            SimpleDateFormat formatter=new SimpleDateFormat("MMM dd - EEE");
            SimpleDateFormat sdfOutput = new SimpleDateFormat("EEE MMM dd hh:mm:ss z yyyy");
            try{
                ftdMsgVO.setOrderDetailId(request.getParameter(MessagingConstants.REQUEST_ORDER_DETAIL_ID));
                ftdMsgVO.setRecipientName(request.getParameter(MessagingConstants.REQUEST_RECIPIENT_NAME));
                ftdMsgVO.setRecipientStreet(request.getParameter(MessagingConstants.REQUEST_RECIPIENT_STREET));
                ftdMsgVO.setRecipientCity(request.getParameter(MessagingConstants.REQUEST_RECIPIENT_CITY));
                ftdMsgVO.setRecipientState(request.getParameter(MessagingConstants.REQUEST_RECIPIENT_STATE));
                ftdMsgVO.setRecipientZip(request.getParameter(MessagingConstants.REQUEST_RECIPIENT_ZIP));
                ftdMsgVO.setRecipientPhone(request.getParameter("recipient_phone_area")+request.getParameter("recipient_phone_prefix")+request.getParameter("recipient_phone_number"));
                Date deliveryDate=null;
                String delDate="";
                if(StringUtils.isNotEmpty(request.getParameter(
                            MessagingConstants.DELIVERY_MONTH)))
    
                {
                    String month = request.getParameter(
                            MessagingConstants.DELIVERY_MONTH);
                    String day = request.getParameter(
                            MessagingConstants.DELIVERY_DATE);
                    
                   
                    deliveryDate = createDeliveryDate(month,day);
                    delDate = formatter.format(deliveryDate);
                   
                }
                if(StringUtils.isNotEmpty(request.getParameter(MessagingConstants.DELIVERY_MONTH_END))){
                    String endMonth = request.getParameter(
                            MessagingConstants.DELIVERY_MONTH_END);
                    String endDay = request.getParameter(
                            MessagingConstants.DELIVERY_DATE_END);
                    delDate = delDate + " or " + formatter.format(createDeliveryDate(endMonth,endDay));
                }
                ftdMsgVO.setOccasionCode(request.getParameter("occasion_code"));
                ftdMsgVO.setOccasionDesc(request.getParameter(MessagingConstants.REQUEST_OCCASION_DESC));
                ftdMsgVO.setCardMessage(request.getParameter("card_message"));
                ftdMsgVO.setSpecialInstruction(request.getParameter("spec_instruction"));
                ftdMsgVO.setDeliveryText(delDate.toUpperCase());
                ftdMsgVO.setDeliveryDate(deliveryDate);
                if(StringUtils.isNotEmpty(request.getParameter(
                            MessagingConstants.MESSAGE_ORDER_DATE)))
                {
                    
                    String textDate = (String)(request.getParameter(
                            MessagingConstants.MESSAGE_ORDER_DATE));
                    Date utilDate = sdfOutput.parse( textDate );
                    ftdMsgVO.setOrderDate(utilDate);
                }   
                ftdMsgVO.setOrderDateText(request.getParameter(MessagingConstants.REQUEST_ORDER_DATE_TEXT));
                ftdMsgVO.setExternalOrderNumber((String) parameters.get(MessagingConstants.MESSAGE_EXTERNAL_ORDER_NUMBER));
                setFillingFloristInfo(ftdMsgVO, request);

                String securityToken = request.getParameter(MessagingConstants.REQUEST_SECURITY_TOKEN);
                ftdMsgVO.setOperatorFirstName(lookupCurrentUserFirstName(securityToken));
                ftdMsgVO.setOperator(lookupCurrentUserId(securityToken));
                //add sending florist
                ftdMsgVO.setSendingFloristCode(request.getParameter(MessagingConstants.REQUEST_SENDING_FLORIST));
                ftdMsgVO.setFirstChoice(request.getParameter("first_choice")); //todo :add code from other project
                ftdMsgVO.setSubstitution(request.getParameter("allowed_substitution")); //todo :add code from other project
                ftdMsgVO.setCurrentPrice(Double.parseDouble(request.getParameter("florist_prd_price"))); 

            } finally {
                if (logger.isDebugEnabled()) {
                    logger.debug("Exiting createMessageForCallout");
                }
            }
            
            return DOMUtil.getDocument(ftdMsgVO.toXML());
        }
    /**
     * Utilize Security Manager to obtain user�s first name based on the security token.
     * @param securityToken - String
     * @return String
     * @throws Exception
     */
    public String lookupCurrentUserFirstName(String securityToken) throws Exception
    {
    
        if (logger.isDebugEnabled()) {
            logger.debug("Entering lookupCurrentUserFirstName");
        }

        String firstName = "";

        try {
        

            if (SecurityManager.getInstance().getUserInfo(securityToken)!=null)
            {
                firstName = SecurityManager.getInstance().getUserInfo(securityToken).getFirstName();
            }

        } finally {
            if (logger.isDebugEnabled()) {
                logger.debug("Exiting lookupCurrentUserFirstName");
            }
        }
        return firstName;        
    }

    private Date createDeliveryDate(String month, String date)
    {
        
        Calendar cal = Calendar.getInstance(TimeZone.getDefault());
               
        int year = cal.get(Calendar.YEAR);
        
        int currMonth = cal.get(Calendar.MONTH);
        int testMonth = Integer.parseInt(month);
        if(currMonth < testMonth)
        {
            cal.set(year+1, testMonth, Integer.parseInt(date));          
        }
        else
        {
           cal.set(year, testMonth, Integer.parseInt(date));   
        }
        return cal.getTime();
    }
    


/**
	* Obtain the product status
	*/
 private void getProductInfo(String productId) throws Exception
  {
    MessageDAO messageDAO = new MessageDAO(this.connection);
    
    CachedResultSet crs =  messageDAO.getProductInfo(productId);
    
    String productType = null;
    
    if(crs.next())
    {
      this.status = (String) crs.getString("status");
      this.productType = (String)crs.getString("productType");
    }
  }
  
  /**
	* Set product information
	*/
 private Document setProductInfo(String productId) throws Exception
  {
    Document pDoc = DOMUtil.getDefaultDocument();
    getProductInfo(productId);
                
    Element parent = pDoc.createElement("product");
    pDoc.appendChild( parent );
    
    Element productInfoChild = pDoc.createElement("product_info");
    parent.appendChild(productInfoChild);
    
    Element product_info_node = pDoc.createElement("product_status");
    product_info_node.appendChild( pDoc.createTextNode(this.status) );
    productInfoChild.appendChild(product_info_node); 
    
    Element productType = pDoc.createElement("productType");
    productType.appendChild( pDoc.createTextNode(this.productType) );
    productInfoChild.appendChild(productType);
    
    return pDoc;
  }
 

	/**
	 * @param connection The connection to set.
	 */
	public void setConnection(Connection connection) {
		this.connection = connection;
	}


  public Connection getConnection()
  {
    return connection;
  }
  
  
}