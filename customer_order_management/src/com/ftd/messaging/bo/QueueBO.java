package com.ftd.messaging.bo;

import com.ftd.messaging.constants.MessagingConstants;
import com.ftd.messaging.dao.QueueDAO;
import com.ftd.messaging.util.MessagingDOMUtil;
import com.ftd.messaging.vo.QueueDeleteHeaderStatusVO;
import com.ftd.osp.utilities.vo.QueueDeleteDetailVO;
import com.ftd.osp.utilities.vo.QueueDeleteHeaderVO;
import com.ftd.messaging.vo.QueueSearchVO;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.security.SecurityManager;

import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.util.MessageResources;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import org.xml.sax.SAXException;


/**
 * This class contains business logic to:
 * Retrieve queues based on order detail ID passed in the request.
 * Delete queues based on queue ids passed in the request, current user
 * role and queue creator role.
 * Send the information back to the QueueAction for further processing.
 * @author Charles Fox
 * @todo check comments, error handling, and message handling
 */
public class QueueBO
{
  private Logger logger = new Logger("com.ftd.messaging.bo.QueueBO");
  private Connection dbConnection = null;
  private MessagingConstants messagingConstants;

  public QueueBO(Connection conn)
  {
    super();
    dbConnection = conn;
  }

  /**
     * Receive the request object and call other methods; in the business
     * object itself as well as in the QueueDAO for further processing.
     * @param orderDetailId - String
     * @return Document
     * @throws SAXException
     * @throws IOException
     * @throws SQLException
     * @throws ParserConfigurationException
     * @throws Exception
     * @todo - code and determine exceptions
     */
  public Document retrieveQueues(HttpServletRequest request)
    throws SAXException, ParserConfigurationException, IOException, SQLException, Exception
  {
    if (logger.isDebugEnabled())
    {
      logger.debug("Entering retrieveQueues");
      logger.debug("HttpServletRequest : " + request.getRequestURL().toString());
    }

    Document queues = DOMUtil.getDocument();

    try
    {
      /* Retrieve order detail id from HTTP request */
      String orderDetailID = request.getParameter(MessagingConstants.REQUEST_ORDER_DETAIL_ID);
      if (orderDetailID == null)
      {
        /* Order Detail ID not included, throw Exception   */
        throw new Exception("Order Detail ID not included");
      }

      HashMap msgLineItemNum = new HashMap();
      Enumeration requestParam = request.getParameterNames();
      String value = "";
      while (requestParam.hasMoreElements())
      {
        String check = (String) requestParam.nextElement();
        String svalue = request.getParameter(check);
        logger.debug("queue param=" + check + "\t value=" + svalue);
        if (hasOnlyDigits(check))
        {
          msgLineItemNum.put(svalue, check);
        }
        else if (check.endsWith("E"))
        { //message line number=1 and email/letter line number =1E
          String lineNum = check.substring(0, check.length() - 1);
          logger.debug("lineNum=" + lineNum);
          if (hasOnlyDigits(lineNum))
          {
            msgLineItemNum.put(svalue, check);
          }
        }
      }

      QueueDAO queueDAO = new QueueDAO(dbConnection);
      queues = queueDAO.getQueues(orderDetailID);
      addMessageLineItemNumber(queues, msgLineItemNum);

    }
    finally
    {
      if (logger.isDebugEnabled())
      {
        logger.debug("Exiting retrieveQueues");
      }
    }
    return queues;
  }

  public boolean hasOnlyDigits(String s)
  {
    boolean isNumber = true;
    try
    {
      Integer.parseInt(s);
    }
    catch (NumberFormatException e)
    {
      isNumber = false;
    }

    return isNumber;
  }

  /**
     * Utilize Security Manager to obtain user�s id based on the security token.
     * @param securityToken - String
     * @return String
     * @throws Exception????
     * @todo - Need code from Mike Kruger
     */
  public String lookupCurrentUserId(String securityToken)
    throws Exception
  {

    if (logger.isDebugEnabled())
    {
      logger.debug("Entering lookupCurrentUserId");
      logger.debug("securityToken : " + securityToken);
    }
    String userId = "";
    try
    {
      if (SecurityManager.getInstance().getUserInfo(securityToken) != null)
      {
        userId = SecurityManager.getInstance().getUserInfo(securityToken).getUserID();
      }
    }
    finally
    {
      if (logger.isDebugEnabled())
      {
        logger.debug("Exiting getUnableDeleteQueueDoc");
      }
    }

    return userId;
  }


  /**
     * Call getUnableDeleteQueues method to obtain a list of unable delete
     * queue ids.
     * Add the list of unable delete queue ids to the Document.
     * Return the Document object.
     * @param orderDetailID - String
     * @param securityToken - String
     * @param queueIds - String[]
     * @return Document
     * @throws ParserConfigurationException
     * @throws Exception
     * @todo - code and determine exceptions
     */
  public Document getUnableDeleteQueueDoc(String orderDetailID, String securityToken, String[] queueIds, 
                                          MessageResources messageResource)
    throws SAXException, IOException, SQLException, ParserConfigurationException, Exception

  {
    if (logger.isDebugEnabled())
    {
      logger.debug("Entering getUnableDeleteQueueDoc");
      logger.debug("orderDetailId : " + orderDetailID);
      logger.debug("securityToken : " + securityToken);
    }

    Document queuesDOM = DOMUtil.getDocument();
    QueueDAO queueDAO = new QueueDAO(dbConnection);
    try
    {
      /* Call getUnableDeleteQueues method to obtain a list
                 * of unable delete queue ids. */
      List unableDeleteQueues = getUnableDeleteQueues(securityToken, queueIds, queueDAO);
      queuesDOM = queueDAO.getQueues(orderDetailID);
      /* add element stating delete has been attempted */
      MessagingDOMUtil.addElementToXML("DeleteAttempted", "Y", queuesDOM);
      Iterator queue = unableDeleteQueues.iterator();

      // loop thru the result set and generate xml
      String queueID = null;
      String queueIDS = "";
      int count = 0;
      while (queue.hasNext())
      {
        queueID = (String) queue.next();

        if (queueID != null && queueID.trim().length() > 0)
        {
          queueIDS += queueID + ",";
        }
        count++;
      }

      MessagingDOMUtil.addElementToXML("unableDeleteQueues", queueIDS, queuesDOM);
      if (queueIDS != null && queueIDS.trim().length() > 0)
      {
        String msgResource = (String) messageResource.getMessage(MessagingConstants.QUEUE_DELETE_ERROR_MSG);
        //String msg = MessageFormat.format(msgResource,new String[]{queueIDS});
        MessagingDOMUtil.addElementToXML("ErrorMessage", msgResource, queuesDOM);
      }
    }
    finally
    {
      if (logger.isDebugEnabled())
      {
        logger.debug("Exiting getUnableDeleteQueueDoc");
      }

    }

    return queuesDOM;
  }

  public Document getUnableDeleteQueueDoc(String orderDetailID, List unableDeleteQueues, MessageResources messageResource)
    throws SAXException, IOException, SQLException, ParserConfigurationException, Exception

  {
    Document queuesDOM = DOMUtil.getDocument();
    QueueDAO queueDAO = new QueueDAO(dbConnection);
    try
    {
      queuesDOM = queueDAO.getQueues(orderDetailID);
      /* add element stating delete has been attempted */
      MessagingDOMUtil.addElementToXML("DeleteAttempted", "Y", queuesDOM);
      Iterator queue = unableDeleteQueues.iterator();

      // loop thru the result set and generate xml
      String queueID = null;
      String queueIDS = "";
      int count = 0;
      while (queue.hasNext())
      {
        queueID = (String) queue.next();

        if (queueID != null && queueID.trim().length() > 0)
        {
          queueIDS += queueID + ",";
        }
        count++;
      }

      MessagingDOMUtil.addElementToXML("unableDeleteQueues", queueIDS, queuesDOM);
      if (queueIDS != null && queueIDS.trim().length() > 0)
      {
        String msgResource = (String) messageResource.getMessage(MessagingConstants.QUEUE_DELETE_ERROR_MSG);
        //String msg = MessageFormat.format(msgResource,new String[]{queueIDS});
        MessagingDOMUtil.addElementToXML("ErrorMessage", msgResource, queuesDOM);
      }
    }
    finally
    {
      if (logger.isDebugEnabled())
      {
        logger.debug("Exiting getUnableDeleteQueueDoc");
      }
    }

    return queuesDOM;
  }

  public Document getUnableDeleteQueueDoc(List unableDeleteQueues, MessageResources messageResource)
    throws SAXException, IOException, SQLException, ParserConfigurationException, Exception
  {
    return getUnableDeleteQueueDoc(unableDeleteQueues, messageResource, false);
  }

  public Document getUnableDeleteQueueDoc(List unableDeleteQueues, MessageResources messageResource, 
                                          boolean removeTrailingComma)
    throws SAXException, IOException, SQLException, ParserConfigurationException, Exception
  {
    Document queuesDOM = DOMUtil.getDocument();
    try
    {
      Iterator queue = unableDeleteQueues.iterator();

      // loop thru the result set and generate xml
      String queueID = null;
      String queueIDS = "";
      int count = 0;
      while (queue.hasNext())
      {
        queueID = (String) queue.next();

        if (queueID != null && queueID.trim().length() > 0)
        {
          queueIDS += queueID + ",";
        }
        count++;
      }

      if (queueIDS != null && queueIDS.trim().length() > 0 && removeTrailingComma)
      {
        queueIDS = queueIDS.substring(0, queueIDS.lastIndexOf(","));
      }

      MessagingDOMUtil.addElementToXML("unableDeleteQueues", queueIDS, queuesDOM);
      if (queueIDS != null && queueIDS.trim().length() > 0)
      {
        String msgResource = (String) messageResource.getMessage(MessagingConstants.QUEUE_DELETE_ERROR_MSG);
        //String msg = MessageFormat.format(msgResource,new String[]{queueIDS});
        MessagingDOMUtil.addElementToXML("ErrorMessage", msgResource, queuesDOM);
      }
    }
    finally
    {
      if (logger.isDebugEnabled())
      {
        logger.debug("Exiting getUnableDeleteQueueDoc");
      }
    }

    return queuesDOM;
  }


  public Document getUnableDeleteQueueDoc(List unableDeleteQueues, MessageResources messageResource, 
                                          String delimitingCharacter, boolean removeTrailingDelimitingCharacter)
    throws SAXException, IOException, SQLException, ParserConfigurationException, Exception
  {
    Document queuesDOM = DOMUtil.getDocument();
    try
    {
      Iterator queue = unableDeleteQueues.iterator();

      // loop thru the result set and generate xml
      String queueID = null;
      String queueIDS = "";
      int count = 0;
      while (queue.hasNext())
      {
        queueID = (String) queue.next();

        if (queueID != null && queueID.trim().length() > 0)
        {
          queueIDS += queueID + delimitingCharacter;
        }
        count++;
      }

      if (queueIDS != null && queueIDS.trim().length() > 0 && removeTrailingDelimitingCharacter)
        queueIDS = queueIDS.substring(0, queueIDS.lastIndexOf(delimitingCharacter));

      MessagingDOMUtil.addElementToXML("unableDeleteQueues", queueIDS, queuesDOM);
      if (queueIDS != null && queueIDS.trim().length() > 0)
      {
        String msgResource = (String) messageResource.getMessage(MessagingConstants.QUEUE_DELETE_ERROR_MSG);
        MessagingDOMUtil.addElementToXML("ErrorMessage", msgResource, queuesDOM);
      }
    }
    finally
    {
      if (logger.isDebugEnabled())
      {
        logger.debug("Exiting getUnableDeleteQueueDoc");
      }
    }

    return queuesDOM;
  }


  public boolean deleteQueue(String queueId, String securityToken, String taggedCsrId)
    throws SAXException, IOException, SQLException, ParserConfigurationException, Exception

  {
    if (logger.isDebugEnabled())
    {
      logger.debug("Entering deleteQueue");
      logger.debug("securityToken : " + securityToken);
    }

    QueueDAO queueDAO = new QueueDAO(dbConnection);
    try
    {
      String currentUserId = this.lookupCurrentUserId(securityToken);
      return this.deleteQueue(queueId, currentUserId, taggedCsrId, queueDAO);
    }
    finally
    {
      if (logger.isDebugEnabled())
      {
        logger.debug("Exiting deleteQueue");
      }
    }
  }


  /**
     * �	Call lookupCurrentUserId to get the current user�s id
     * �	Iterate through queue id array, call deleteQueue method and try to
     *      delete the queue. If deleteQueue method return false, add the queue
     *      id to the unable delete queue list.
     * �	Return a list of queues that can not be deleted.
     * @param securityToken - String
     * @param queueIds - String[]
     * @return List
     * @throws Exception????
     * @todo - code and determine exceptions
     */
  public List getUnableDeleteQueues(String securityToken, String[] queueIds, QueueDAO queueDAO)
    throws SAXException, IOException, SQLException, ParserConfigurationException, Exception
  {
    if (logger.isDebugEnabled())
    {
      logger.debug("Entering getUnableDeleteQueues");
      logger.debug("securityToken : " + securityToken);
    }

    List deleteQueues = new ArrayList();
    try
    {
      /* Call lookupCurrentUserId to get the current user�s id */
      String userID = lookupCurrentUserId(securityToken);

      /* Iterate through queue id array, call deleteQueue method and
             * try to delete the queue. If deleteQueue method return false,
             * add the queue id to the unable delete queue list.
             */
      boolean ableToDelete = false;

      for (int i = 0; i < queueIds.length; i++)
      {
        logger.debug("queue id= [" + queueIds[i] + "]");
        String queueTagId = queueIds[i];
        String queueId = "";
        String taggedCsrId = null;
        int idx = queueTagId.indexOf("|");
        if (idx != -1 && !queueTagId.endsWith("|"))
        {
          queueId = queueTagId.substring(0, idx);
          taggedCsrId = queueTagId.substring(idx + 1);
        }
        else if (queueTagId.endsWith("|"))
        {
          queueId = queueTagId.substring(0, idx);
        }
        else
        {
          queueId = queueTagId;
        }
        logger.debug("queue id= [" + queueId + "]");
        logger.debug("tagged csr id= [" + taggedCsrId + "]");
        //Iterate through queues
        ableToDelete = deleteQueue(queueId, userID, taggedCsrId, queueDAO);
        if (ableToDelete == false)
        {
          deleteQueues.add(queueTagId);
        }
      }
    }
    finally
    {
      if (logger.isDebugEnabled())
      {
        logger.debug("Exiting getUnableDeleteQueueDoc");
      }
    }

    return deleteQueues;
  }

  public void deleteQueueMessage(String messageId, String userId)
    throws SAXException, IOException, SQLException, ParserConfigurationException, Exception
  {
    QueueDAO queueDAO = new QueueDAO(dbConnection);
    queueDAO.deleteQueue(messageId, userId);
  }

  /**
     * If the numeric value of the current user role is not greater than the
     * numeric value of the queue creator role, call deleteQueue method
     * of QueueDAO to delete the queue and return true. Otherwise, return false.
     * @param queueId - String
     * @param currentUserRoleInt - int
     * @param queueCreatorRoleInt - String
     * @return boolean
     * @throws Exception????
     * @todo - code and determine exceptions
     */
  private boolean deleteQueue(String queueMessageId, String currentUserId, String taggedCsrId, QueueDAO queueDAO)
    throws SAXException, IOException, SQLException, ParserConfigurationException, Exception
  {
    if (logger.isDebugEnabled())
    {
      logger.debug("Entering deleteQueue");
      logger.debug("queueMessageId : " + queueMessageId);
      logger.debug("currentUserId : " + currentUserId);
    }

    boolean ableToDelete = false;

    try
    {
      ableToDelete = queueDAO.deleteQueue(queueMessageId, currentUserId, taggedCsrId);
    }
    finally
    {
      if (logger.isDebugEnabled())
      {
        logger.debug("Exiting deleteQueue");
      }
    }
    return ableToDelete;
  }

  /**
     * Iterate through queue Node list in queue Document Object, use message
     * id as key, get line item number from message line item map, and add
     * message line item number to queue Document.
     * @param queueDoc - Document
     * @param messageLineItemMap - Map
     * @return n/a
     * @throws Exception????
     * @todo - code and determine exceptions
     */
  private void addMessageLineItemNumber(Document queueDoc, HashMap messageLineItemMap)
  {
    NodeList queues = queueDoc.getElementsByTagName(MessagingConstants.QUEUE_NODE);
    for (int i = 0; i < queues.getLength(); i++)
    {
      Node queue = queues.item(i);
      NodeList queueAttributes = queue.getChildNodes();
      String lineItem = null;
      String messageId = null;
      for (int j = 0; j < queueAttributes.getLength(); j++)
      {
        Node attribute = queueAttributes.item(j);
        String nodeName = attribute.getNodeName();
        if (nodeName.equals("mercury_number"))
        {
          if (attribute.getFirstChild() != null)
          {
            messageId = attribute.getFirstChild().getNodeValue();
          }

          if (messageId != null && messageLineItemMap.get(messageId) != null)
          {
            lineItem = (String) messageLineItemMap.get(messageId);
          }
        }

        if (StringUtils.isNotEmpty(lineItem))
        {
          break; //get line item number, exit loop.
        }
        else // it is may be an email queue, we need to look up point of contact id.
        {
          if (nodeName.equals("point_of_contact_id"))
          {
            if (attribute.getFirstChild() != null)
            {
              messageId = attribute.getFirstChild().getNodeValue();
            }
            if (messageId != null && messageLineItemMap.get(messageId) != null)
            {
              lineItem = (String) messageLineItemMap.get(messageId);
            }

          }
        }
      }
      if (lineItem != null && lineItem.trim().length() > 0)
      {
        addMessageLineItemNumber(lineItem, queueDoc, queue);
      }

    }

  }

  private void addMessageLineItemNumber(String lineItem, Document queueDoc, Node queue)
  {
    Node element = queueDoc.createElement(MessagingConstants.QUEUE_LINE_ITEM_NUMBER);
    element.appendChild(queueDoc.createTextNode(lineItem));
    queue.appendChild(element);
  }

  public Document getAllMQDQueueTypes()
    throws Exception
  {
    QueueDAO queueDAO = new QueueDAO(dbConnection);
    return queueDAO.queryAllMQDQueueTypes();
  }

 
  
  public CachedResultSet getQueueMessages(QueueSearchVO queueSearchVO)
    throws Exception
  {
    QueueDAO queueDAO = new QueueDAO(dbConnection);
    return queueDAO.queryQueueMessages(queueSearchVO);
  }

  public CachedResultSet getQueueMessage(String messageId)
    throws Exception
  {
    QueueDAO queueDAO = new QueueDAO(dbConnection);
    return queueDAO.getQueueMessage(messageId);
  }

  public void reprocessOrder(String orderDetailId)
    throws Exception
  {
    QueueDAO queueDAO = new QueueDAO(dbConnection);
    queueDAO.reprocessOrder(orderDetailId);
  }

  /**
     * Use queue as the key, call getQueueCreatorRole method of QueueDAO
     * to determine which role the current user is in among of the following
     * roles: CSRDirector, CSRManager, CSRSupervisor, and CSR.
     * @param queueId - String
     * @return String
     * @throws Exception????
     * @todo - code and determine exceptions
    /*
    private String retrieveQueueCreatorRole(String queueId){
        return null;
    }
    */


  /**
     * Get queue info based on the external order number
     * @param externalOrderNumber - String
     * @param otherQueuesList - String - all the queue types selected
     * @return CachedResultSet
     * @throws Exception
     */
  public CachedResultSet getQueueMessageByExternalOrderNumber(String externalOrderNumber, List otherQueuesList)
    throws Exception
  {
    QueueDAO queueDAO = new QueueDAO(dbConnection);

    String queueTypesSelected = "";
    for (int i = 0; i < otherQueuesList.size(); i++)
    {
      queueTypesSelected += "'" + (String) otherQueuesList.get(i) + "',";
    }

    //if queue types were selected, remove the last ,
    if (queueTypesSelected.length() > 0)
      queueTypesSelected = queueTypesSelected.substring(0, queueTypesSelected.length() - 1);
    //else initialize to null 
    else
      queueTypesSelected = null;

    return queueDAO.getQueueMessageByExternalOrderNumber(externalOrderNumber, queueTypesSelected);
  }


  /**
     * Delete queue records based on external order number and queue types
     * @param externalOrderNumber - String
     * @param otherQueuesList - String - all the queue types selected
     * @return CachedResultSet
     * @throws Exception
     */
  public void deleteAllQsByExtOrdNum(String externalOrderNumber, List otherQueuesList, String userId)
    throws Exception
  {
    QueueDAO queueDAO = new QueueDAO(dbConnection);

    String queueTypesSelected = "";
    for (int i = 0; i < otherQueuesList.size(); i++)
    {
      queueTypesSelected += "'" + (String) otherQueuesList.get(i) + "',";
    }

    //if queue types were selected, remove the last ,
    if (queueTypesSelected.length() > 0)
      queueTypesSelected = queueTypesSelected.substring(0, queueTypesSelected.length() - 1);
    //else initialize to null 
    else
      queueTypesSelected = null;

    queueDAO.deleteAllQsByExtOrdNum(externalOrderNumber, queueTypesSelected, userId);
  }


  /**
     * Delete queue records based on order detail id and queue types
     * @param orderDetailId - String
     * @param deleteOtherQueuesList - String - all the queue types selected
     * @return CachedResultSet
     * @throws Exception
     */
  public void deleteAllQsByOrdDetId(String orderDetailId, List deleteOtherQueuesList, String userId)
    throws Exception
  {
    QueueDAO queueDAO = new QueueDAO(dbConnection);

    String queueTypesSelected = "";
    for (int i = 0; i < deleteOtherQueuesList.size(); i++)
    {
      queueTypesSelected += "'" + (String) deleteOtherQueuesList.get(i) + "',";
    }

    //if queue types were selected, remove the last ,
    if (queueTypesSelected.length() > 0)
      queueTypesSelected = queueTypesSelected.substring(0, queueTypesSelected.length() - 1);
    //else initialize to null 
    else
      queueTypesSelected = null;

    queueDAO.deleteAllQsByOrdDetId(orderDetailId, queueTypesSelected, userId);
  }
  
  /**
   * 
   * @param queueDeleteHeaderVO
   * @return QueueDeleteHeaderID - Long
   * @throws Exception
   */
  public Long insertQueueDeleteHeaderRecord(QueueDeleteHeaderVO queueDeleteHeaderVO) throws Exception
  {
    QueueDAO queueDAO = new QueueDAO(dbConnection);
    return queueDAO.insertQueueDeleteHeaderRecord(queueDeleteHeaderVO);
  }
  
  /**
   * 
   * @param queueDeleteDetailVO
   * @return QueueDeleteDetailId - Long
   * @throws Exception
   */
  public Long insertDeleteDetailRecord(QueueDeleteDetailVO queueDeleteDetailVO) throws Exception
  {
    QueueDAO queueDAO = new QueueDAO(dbConnection);
    return queueDAO.insertDeleteDetailRecord(queueDeleteDetailVO);
  }
  
   /**
      * This method will be called from loadData() of QueueMessageProcessingStatusAction 
      * when the user requests for the batch status screen or refreshes the data on batch screen
      * @param String userId
      * @param Long queueDeleteHeaderId
      * @return List<QueueDeleteHeaderStatusVO>
      * @throws Exception
      */
     public List<QueueDeleteHeaderStatusVO> getQueueHeaderStatus(String userId, Long queueDeleteHeaderId) throws Exception {
       QueueDAO queueDAO = new QueueDAO(dbConnection);
       return queueDAO.getQueueDeleteHeaderStatus(userId, queueDeleteHeaderId);
     }
     
     
     /**
      * This method will be called by generateExcel() of QueueMessageProcessingStatusAction
      * when user wants to download the batch details data for the specified queueDeleteHeaderId 
      * @param String userId
      * @param queueDeleteHeaderId
      * @return QueueDeleteHeaderStatusVO
      * @throws Exception
      */
     public QueueDeleteHeaderStatusVO getQueueHeaderDetails(String userId, Long queueDeleteHeaderId) throws Exception {
       QueueDAO queueDAO = new QueueDAO(dbConnection);
       return queueDAO.getQueueDeleteHeaderWithDetails(userId, queueDeleteHeaderId);
     }

//getUsersFromQueueDeleteHeader()
    
  /**
    * This method will be called from load() of QueueMessageProcessingStatusAction 
    * to load all the available users in to the drop down for admin to choose.
    * @return List<String>
    * @throws Exception
    */
    public List<String> getUsersFromQueueDeleteHeader() throws Exception{
        QueueDAO queueDAO = new QueueDAO(dbConnection);
        return queueDAO.getUsersFromQueueDeleteHeader();
    }
    
    /**
       * Retrieve associated external order number for passed in message id
       * @param String messageId
       * @return null
       * @throws Exception
       */
      public String getExtOrdNumByMsgId(String messageId) throws Exception {
        QueueDAO queueDAO = new QueueDAO(dbConnection);
        return queueDAO.getExtOrdNumByMsgId(messageId);
      }

}
