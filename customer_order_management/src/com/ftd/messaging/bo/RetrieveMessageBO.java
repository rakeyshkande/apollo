package com.ftd.messaging.bo;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.messaging.constants.MessagingConstants;
import com.ftd.messaging.dao.MessageDAO;
import com.ftd.messaging.util.MessagingDOMUtil;
import com.ftd.messaging.util.MessagingHelper;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.XMLFormat;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.io.IOException;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;

import org.xml.sax.SAXException;

/**
 * This class contains business logic to retrieve order-message related 
 * data based on order detail ID passed in the request, and send the 
 * information back to the DisplayCommunicationAction for further processing.  
 * @author Charles Fox
 * @todo check comments, error handling, and message handling
 */
public class RetrieveMessageBO 
{

    private Logger logger = new Logger(
        "com.ftd.messaging.bo.RetrieveMessageBO");
    private Connection dbConnection = null;
    private MessagingConstants messagingConstants;
    
    public RetrieveMessageBO(Connection conn)
    {
        super();
        dbConnection = conn;
    }


    /**
     *  This method is the main method for processing in the business object.  
     *  It will call other methods, in the business object itself for further processing. 
     * @param orderDetailId - String
     * @return Document
     * @throws SAXException
     * @throws IOException
     * @throws SQLException
     * @throws ParserConfigurationException
     * @throws Exception
     * @todo - code and determine exceptions
     */
    public Document retrieveMessageInfo(HttpServletRequest request, HashMap parameters)
        throws TransformerException, IOException, SAXException, 
        ParserConfigurationException, SQLException, Exception
    {
         if(logger.isDebugEnabled()){
            logger.debug("Entering retrieveMessageInfo");
            logger.debug("HttpServletRequest : " + request.getRequestURL().toString());
        }
        
        Document xmlMessages = DOMUtil.getDocument();
        
        try {
        	/* Retrieve order detail id from HTTP request */
            String orderDetailID = request.getParameter(
                messagingConstants.REQUEST_ORDER_DETAIL_ID);
            String securityToken=request.getParameter(MessagingConstants.REQUEST_SECURITY_TOKEN);
            if(StringUtils.isBlank(orderDetailID))
            {
                logger.debug("no order detail id in request parameter");
                /* attempt to retrieve from parameters hashmap */
                orderDetailID = (String) parameters.get(messagingConstants.REQUEST_ORDER_DETAIL_ID);
                /* Order Detail ID not included, throw Exception   */
                if(StringUtils.isBlank(orderDetailID))
                {           
                    logger.error("Order Detail ID not included");
                    throw new Exception("Order Detail ID not included");
                }
            }
            
            /* Retrieve page number from HTTP request */
            int pageNumber = getPageNumber(request);
            
            
            /* get maximum message display number from the configuration file. */
            int maxDisplayNumber = Integer.parseInt(
                    ConfigurationUtil.getInstance().getProperty(
                    messagingConstants.MESSAGING_CONFIG, 
                    messagingConstants.MAX_MSG_DISPLAY));
            
            /* Get starting message display number. */
            int startingMsgNum = calculateStartingNumber(pageNumber,
                maxDisplayNumber);
             if(logger.isDebugEnabled()){
                logger.debug("order detail id ="+orderDetailID);
                logger.debug("lookupPageNumber ="+pageNumber);
                logger.debug("maxDisplayNumber ="+maxDisplayNumber);
            }
            parameters.put(messagingConstants.REQUEST_ORDER_DETAIL_ID,orderDetailID);
            parameters.put(messagingConstants.REQUEST_PAGE_NUMBER,""+pageNumber);
            parameters.put(messagingConstants.MAX_MSG_DISPLAY,String.valueOf(maxDisplayNumber));
            
            /* Call retrieveMessages method to retrieve the Document that 
            contains messages/emails/letters. */
            HashMap messages = retrieveMessages(
                orderDetailID,startingMsgNum,maxDisplayNumber);
    
            // pull base document
            //xmlMessages = (Document) DOMUtil.getDocument();
            //Create an Document and call a method that will transform the
            //resultset from the cursor:
            //add mercury messages. 
            Document messageDoc=buildXML(messages,"OUT_MESSAGE_CUR", 
                MessagingConstants.GET_COMMUNICATION_MESSAGES_TOP_NODE,
                MessagingConstants.GET_COMMUNICATION_MESSAGES_BOTTOM_NODE);
            if(messageDoc!=null){
              DOMUtil.addSection(xmlMessages, messageDoc.getChildNodes());
            }
            //add email/letters
            Document emailLetterDoc=this.buildXML(messages, "OUT_EMAIL_LETTER_CUR","email-letters", "email-letter");
            if(emailLetterDoc!=null){
              DOMUtil.addSection(xmlMessages, emailLetterDoc.getChildNodes());
            }
         
            //add order related info, i.e. master order number, order guid, external order number and customer id to the parameter map.
            setOrderInfo(parameters, messages);
            
            //set letter flag.
            setLetterFlag(parameters, messages);
            
            /* calculate total number of pages */
       
            addPaginationInfo(xmlMessages, maxDisplayNumber, pageNumber, messages);
                
            /* add email/letter indicator */
            String emailLetterFlag = (String)messages.get("OUT_EMAIL_LETTER_INDICATOR");
            MessagingDOMUtil.addElementToXML("email-letter-flag", emailLetterFlag, xmlMessages);
            
            //add queues.
            Document queues = retrieveQueues(orderDetailID);
            
            //get order tagged indicator
            Document orderTagged=retrieveOrderTaggedIndicator(securityToken, orderDetailID);
            
            /* Merge Document objects. */
           
            DOMUtil.addSection(xmlMessages,queues.getChildNodes());
            DOMUtil.addSection(xmlMessages,orderTagged.getChildNodes());

        } finally {
            
            if(logger.isDebugEnabled()){
               logger.debug("Exiting retrieveMessageInfo");
            } 
           
        } 
           
        return xmlMessages;
    }

  void setLetterFlag(HashMap parameters, HashMap messages)
  {
    String letterFlg = (String)messages.get("OUT_LETTERS_EXIST");
    boolean hasLetter = (letterFlg != null && letterFlg.equalsIgnoreCase("Y")) ? true : false;
    if (hasLetter)
    {
      parameters.put(MessagingConstants.MESSAGE_LETTER_INDICATOR, "true");
    }
    else
    {
      parameters.put(MessagingConstants.MESSAGE_LETTER_INDICATOR, "false");
    }
  }

  void setOrderInfo(HashMap parameters, HashMap messages)
  {
    CachedResultSet rsCustOrder = (CachedResultSet)messages.get(MessagingConstants.GET_COMMUNICATION_MESSAGES_OUT_CUST_ORDER_CUR);
    if (rsCustOrder.next())
    {
      parameters.put(MessagingConstants.MESSAGE_CUSTOMER_ID, (rsCustOrder.getString(6)) != null ? rsCustOrder.getString(6) : "");
      parameters.put(MessagingConstants.MESSAGE_EMAIL_INDICATOR, (rsCustOrder.getString(7)) != null ? "true" : "false");
      parameters.put(MessagingConstants.MESSAGE_GUID, (rsCustOrder.getString(1)) != null ? rsCustOrder.getString(1) : "");
      parameters.put(MessagingConstants.MESSAGE_EXTERNAL_ORDER_NUMBER, (rsCustOrder.getString(4)) != null ? rsCustOrder.getString(4) : "");
      parameters.put(MessagingConstants.MESSAGE_MASTER_ORDER_NUMBER, (rsCustOrder.getString(3)) != null ? rsCustOrder.getString(3) : "");
      parameters.put(MessagingConstants.MESSAGE_LIFECYCLE_DELIVERED_STATUS, rsCustOrder.getString("lifecycle_delivered_status"));
      parameters.put(MessagingConstants.MESSAGE_STATUS_URL, rsCustOrder.getString("status_url"));
      parameters.put(MessagingConstants.MESSAGE_STATUS_URL_ACTIVE, rsCustOrder.getString("status_url_active"));
      parameters.put(MessagingConstants.MESSAGE_STATUS_URL_EXPIRED_MSG, rsCustOrder.getString("status_url_expired_msg"));
    }
  }

  int getPageNumber(HttpServletRequest request) throws SAXException, ParserConfigurationException, IOException, TransformerException
  {
    String lookupPageNumber = request.getParameter(messagingConstants.REQUEST_PAGE_NUMBER);
    int pageNumber = 0;
    if (lookupPageNumber != null)
    {
      pageNumber = Integer.parseInt(lookupPageNumber);
    }
    else
    {
      pageNumber = Integer.parseInt(ConfigurationUtil.getInstance().getProperty(messagingConstants.MESSAGING_CONFIG, messagingConstants.DEFAULT_PAGE_NUMBER));
    }
    return pageNumber;
  }

  void addPaginationInfo(Document xmlMessages, int maxDisplayNumber, int pageNumber, HashMap messages)
  {
    BigDecimal records = (BigDecimal)messages.get(MessagingConstants.GET_COMMUNICATION_MESSAGES_OUT_NUMBER_OF_RECORDS);
    int totalNumberofRecords = records.intValue();
    if (totalNumberofRecords > maxDisplayNumber)
    {
      MessagingDOMUtil.addElementToXML(MessagingConstants.NUMBER_OF_RECORDS, String.valueOf(totalNumberofRecords), xmlMessages);
    }
    MessagingDOMUtil.addElementToXML(MessagingConstants.CURRENT_PAGE, String.valueOf(pageNumber), xmlMessages);
    double maxPages = (double)totalNumberofRecords / maxDisplayNumber;
    double calcMaxPages = Math.ceil(maxPages);
    int pages = (int)calcMaxPages;
    MessagingDOMUtil.addElementToXML(MessagingConstants.MAX_PAGES, String.valueOf(pages), xmlMessages);
  }
    
    
   

/*******************************************************************************************
 * buildXML()
 *******************************************************************************************/
  private Document buildXML(HashMap outMap, String cursorName, String topName, String bottomName) 
        throws Exception
  {

    //Create a resultset object using the cursor name that was passed. 
    CachedResultSet rs = (CachedResultSet) outMap.get( cursorName);
    Document doc =  null;
    try{
  
    
    
        //Create an XMLFormat, and initialize the top and bottom node.  Note that since this method
        //can be/is called by various other methods, the top and botton names MUST be passed to it. 
        XMLFormat xmlFormat = new XMLFormat();
        xmlFormat.setAttribute("type", "element");
        xmlFormat.setAttribute("top", topName);
        xmlFormat.setAttribute("bottom", bottomName );
         
        
        //call the DOMUtil's converToXMLDOM method
        doc = (Document) MessagingDOMUtil.convertToXMLDOM(rs, xmlFormat);
    }finally{
        logger.debug("finish buildXML");
        //if the resultset was not null, close the result set. 
        //this.closeResultSet(rs);
    }
   
  
    //return the xml document. 
    return doc;
  
  }

    private int calculateStartingNumber(int pageNumber, int maxDisplayNumber)
    {
         if(logger.isDebugEnabled()){
            logger.debug("Entering calculateStartingNumber");
            logger.debug("pageNumber : " + pageNumber);
            logger.debug("maxDisplayNumber : " + maxDisplayNumber);
        }
        
        int startingNumber=0;
        
        try {
            startingNumber = (pageNumber-1) * maxDisplayNumber  + 1;
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting calculateStartingNumber");
            } 
        } 
        return startingNumber;
    }
    
    /**
     *  Call getMessages method of MessageDAO to return Mercury/Venus Messages 
     *  and Emails/Letters based on the given order detail ID and 
     *  starting/ending number
     * @param orderDetailId - String
     * @return Document
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     * @throws Exception
     * @todo - code and determine exceptions
     */
    public HashMap retrieveMessages(String orderDetailId, int startingNumber, int maxNumber )
    throws SAXException, ParserConfigurationException, IOException, 
               SQLException, Exception{
         if(logger.isDebugEnabled()){
            logger.debug("Entering retrieveMessages");
            logger.debug("orderDetailId : " + orderDetailId);
        }
        
        HashMap docMsgs = new HashMap();
        
        try {
            // Get max display lengths (for message mouseover on comm page) from global_parms
            String maxCommentLen = ConfigurationUtil.getInstance().getFrpGlobalParm(
                                   COMConstants.COM_CONTEXT, COMConstants.CONS_MAX_MOUSEOVER_COMMENT_LEN);
            String maxEmailLen   = ConfigurationUtil.getInstance().getFrpGlobalParm(
                                   COMConstants.COM_CONTEXT, COMConstants.CONS_MAX_MOUSEOVER_EMAIL_LEN);
        
            MessageDAO msgDAO = new MessageDAO(dbConnection);
            docMsgs = msgDAO.getMessages(orderDetailId,startingNumber,maxNumber,maxCommentLen,maxEmailLen);
            
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting retrieveMessages");
            } 
           
        } 
        
        return docMsgs;
    }
    
    
    
     public Document retrieveOrderTaggedIndicator(String securityToken, String orderDetailId)
         throws IOException, ParserConfigurationException, SAXException, SQLException,
                TransformerException, Exception
    {
        MessageDAO msgDAO = new MessageDAO(dbConnection);
        String csrId=new MessagingHelper().lookupCurrentUserId(securityToken);
      
        return msgDAO.getOrderTaggedIndicator(orderDetailId, csrId);   
    }


    /**
     *  Call getQueues method of MessageDAO to return the queues and queue 
     *  tags for the given order if it exists in the credit, zip or order queues.
     * @param orderDetailId - String
     * @return Document
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     * @throws Exception
     * @todo - code and determine exceptions
     */
    public Document retrieveQueues(String orderDetailId) throws SAXException,
        ParserConfigurationException, IOException, SQLException, Exception{
        
         if(logger.isDebugEnabled()){
            logger.debug("Entering retrieveQueues");
            logger.debug("orderDetailId : " + orderDetailId);
        }
        
        Document docMsgs = DOMUtil.getDocument();
        
        try {
            MessageDAO msgDAO = new MessageDAO(dbConnection);
            docMsgs = msgDAO.getQueues(orderDetailId);
            
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting retrieveQueues");
            } 
        } 
        return docMsgs;
    }

    /**
     *  Call getFloristDashborard method of MessageDAO to return info required
     *  for florist dashboard based on order detail ID.  
     * @param orderDetailId - String
     * @return Document
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     * @throws Exception
     * @todo - code and determine exceptions
     */
    public Document retrieveFloristDashBoard(HttpServletRequest request, HashMap parameters)
        throws SAXException,ParserConfigurationException, IOException, 
        SQLException, Exception
        {
         if(logger.isDebugEnabled()){
            logger.debug("Entering retrieveFloristDashBoard");
            logger.debug("request : " + request);
        }
        
        Document docMsgs = DOMUtil.getDocument();
        
        try {
        	/* Retrieve order detail id from HTTP request */
            String orderDetailID = request.getParameter(
                messagingConstants.REQUEST_ORDER_DETAIL_ID);
            if(orderDetailID==null)
            {
                /* Order Detail ID not included, throw Exception   */
                throw new Exception("Order Detail ID not included");
            }
            parameters.put(messagingConstants.REQUEST_ORDER_DETAIL_ID,orderDetailID);
            MessageDAO msgDAO = new MessageDAO(dbConnection);
            docMsgs = msgDAO.getFloristDashboard(orderDetailID);
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting retrieveFloristDashBoard");
            } 
           
        } 
        return docMsgs;
    }
 
}