package com.ftd.messaging.bo;
import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.vo.CommentsVO;
import com.ftd.messaging.constants.MessagingConstants;
import com.ftd.messaging.dao.CalloutDAO;
import com.ftd.messaging.dao.MessageDAO;
import com.ftd.messaging.vo.CallOutInfo;
import com.ftd.messaging.vo.MessageVO;
import com.ftd.osp.utilities.plugins.Logger;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.commons.lang.StringUtils;
import org.apache.struts.util.MessageResources;
import org.xml.sax.SAXException;

/**
 * This class contains the business logic to update order comments 
 * and delete FTD queue with which the order is associated.
 * @author Charles Fox
 */
public class CalloutBO 
{
    private Logger logger = new Logger("com.ftd.messaging.bo.CalloutBO");
    private Connection dbConnection = null;

    public CalloutBO(Connection conn)
    {
        super();
        dbConnection = conn;
    }

    /**
     * 
     * @param request - HttpServletRequest
     * @return n/a
     * @throws SAXException
     * @throws IOException
     * @throws SQLException
     * @throws ParserConfigurationException
     * @throws Exception
     */
    public String processCallout(HttpServletRequest request, HashMap parameters, MessageResources resource) 
        throws IOException, SAXException, SQLException, 
            ParserConfigurationException, Exception
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering processCallout");
        }
         
        try {
            CalloutDAO calloutDAO = new CalloutDAO(dbConnection);
            SendMessageBO sendBO = new SendMessageBO(dbConnection);
            String ftdMessageStatus=request.getParameter(MessagingConstants.FTD_MESSAGE_STATUS);
            String ftdMessageID = request.getParameter(MessagingConstants.REQUEST_FTD_MESSAGE_ID);
            /* Obtain call out disposition option from the HTTP request */
            String disposition = "";
            if(request.getParameter(MessagingConstants.CALL_OUT_DISPOSITION)!=null)
            {
              disposition = request.getParameter(MessagingConstants.CALL_OUT_DISPOSITION);
            }
            else
            {
                //disposition not found, throw error
              throw new Exception(
                "Disposition not found in request for callout action");
            }
            CommentsVO commentVO = createCommentsVO(request,parameters, disposition, resource);
            String csrID ="";
            if(request.getParameter(MessagingConstants.MESSAGE_OPERATOR)!=null)
            {
                        csrID = request.getParameter(MessagingConstants.MESSAGE_OPERATOR);
                        commentVO.setCreatedBy(csrID);
                        commentVO.setUpdatedBy(csrID);
                        
             }
             else
             {
                        throw new Exception(
                            "CSR ID not found in request for callout action");           
             }
           
                   
            //"Successfully Called Out"
            if(disposition.equals(MessagingConstants.CALL_OUT_SUCCESS)){
                /* Create a com.ftd.customerordermanagement.vo.CommentsVO object 
                based up the values passed over the HTTP request. Call 
                addOrderComment method of CalloutDAO to update order comments.
                Note: the comment type is �Order�.* */
                sendBO.sendFTDMMessage(request,parameters, commentVO, calloutDAO, resource);
                
            }else{
              calloutDAO.addOrderComment(commentVO);
              if(disposition.equals(MessagingConstants.CALL_OUT_FAILURE)){
                if(ftdMessageStatus!=null&&ftdMessageStatus.trim().equalsIgnoreCase("MM")){
                  //send cancel M message
                  sendBO.sendCancelMessage(request, parameters, true);
                }
              }
            }
            
            return sendBO.getForwardDestination(parameters);
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting processCallout");
            } 
        }       
    }

    /**
     * retrieve information from request and parameters to create a CommentsVO
     * @param request - HttpServletRequest
     * @param parameters - HashMap
     * @return CommentsVO
     * @throws Exception
     */
    private CommentsVO createCommentsVO(
        HttpServletRequest request, HashMap parameters, String disposition, MessageResources resource)
        throws Exception
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering createCommentsVO");
        }
        
        CommentsVO commentVO = new CommentsVO();
        
        try{
            /* get comment type */
            commentVO.setCommentType(MessagingConstants.CALL_OUT_COMMENT_TYPE);
            
            String orderDetailID = "";
            String externalOrderNumber=request.getParameter("external_order_number");
            /* get order detail id */
            if(parameters.get(MessagingConstants.REQUEST_ORDER_DETAIL_ID)!=null)
            {
                orderDetailID = (String) parameters.get(
                    MessagingConstants.REQUEST_ORDER_DETAIL_ID);
                commentVO.setOrderDetailId(orderDetailID);
            }
            //String spokeTo= request.getParameter("spoke_to");

			 //get the entire callout info to same variable 
            CallOutInfo callOutInfo = setCallOutFloristInfo(request);
            
            if(disposition.equalsIgnoreCase(MessagingConstants.CALL_OUT_NOREACH))
            {
            	callOutInfo.setSpokeTo("");
            	callOutInfo.setShopName("");
            	callOutInfo.setShopPhone("");
            }
            
            String fillingFlorist = request.getParameter(MessagingConstants.REQUEST_FILLING_FLORIST);
            
            String comments=resource.getMessage(disposition, new String[]{externalOrderNumber, callOutInfo.getSpokeTo(), fillingFlorist, callOutInfo.getShopName(), callOutInfo.getShopPhone()});
            
            logger.info("callout comments= "+comments);         
            /* get comment text */
            commentVO.setComment(comments);
        
          
            /* get customer ID */
            if(request.getParameter(COMConstants.CUSTOMER_ID)!=null)
            {
                commentVO.setCustomerId((String) parameters.get(MessagingConstants.MESSAGE_CUSTOMER_ID));
            }
            
            /* get order guid */
            if(request.getParameter(COMConstants.ORDER_GUID)!=null)
            {
                commentVO.setOrderGuid((String) parameters.get(MessagingConstants.MESSAGE_GUID));
            }

            /* get created_by */
            if(StringUtils.isNotEmpty(request.getParameter(MessagingConstants.MESSAGE_OPERATOR)))

            {
                String csrID = request.getParameter(MessagingConstants.MESSAGE_OPERATOR);
                commentVO.setCreatedBy(csrID);
                commentVO.setUpdatedBy(csrID);
            }
            else
            {
                throw new Exception(
                    "CSR ID not found in request for callout action");           
            }

        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting createCommentsVO");
            } 
        }     
        return commentVO;
    }
    
    public CallOutInfo setCallOutFloristInfo(HttpServletRequest request) {
		
		String shopName = request.getParameter("nonftd_florist_shop_name");
		String shopPhone = request.getParameter("nonftd_florist_phone");
		String spokeTo = request.getParameter("spoke_to");
		
		CallOutInfo callOutInfo = new CallOutInfo();
		callOutInfo.setShopName(shopName);
		callOutInfo.setShopPhone(shopPhone);
		callOutInfo.setSpokeTo(spokeTo);
		
		logger.info("**"+callOutInfo+"**");
		
		return callOutInfo;
	} 
    

}