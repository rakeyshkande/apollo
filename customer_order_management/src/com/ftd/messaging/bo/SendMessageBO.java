package com.ftd.messaging.bo;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.vo.CommentsVO;
import com.ftd.customerordermanagement.dao.CommentDAO;
import com.ftd.customerordermanagement.dao.OrderDAO;
import com.ftd.ftdutilities.DeliveryDateUTIL;
import com.ftd.ftdutilities.FTDCommonUtils;
import com.ftd.ftdutilities.OEDeliveryDateParm;
import com.ftd.messaging.constants.MessagingConstants;
import com.ftd.messaging.dao.CalloutDAO;
import com.ftd.messaging.dao.MessageDAO;
import com.ftd.messaging.dao.QueueDAO;
import com.ftd.messaging.util.MessageUtil;
import com.ftd.messaging.util.MessagingHelper;
import com.ftd.messaging.util.MessagingServiceLocator;
import com.ftd.messaging.vo.CallOutInfo;
import com.ftd.messaging.vo.MessageOrderStatusVO;
import com.ftd.messaging.vo.MessageVO;
import com.ftd.op.common.to.ResultTO;
import com.ftd.op.mercury.to.*;
import com.ftd.op.venus.to.AdjustmentMessageTO;
import com.ftd.op.venus.to.CancelOrderTO;
import com.ftd.op.venus.to.OrderTO;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;

import java.rmi.RemoteException;

import java.sql.Connection;
import java.sql.SQLException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.text.DateFormat;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import org.xml.sax.SAXException;


/**
 * This class contains business logic to send Mercury/Venus messages.
 * @author Charles Fox
 * @todo check comments, error handling, and message handling
 * @todo confirm stored procedure setup against actual stored procedures
 */
public class SendMessageBO {
    private Logger logger = new Logger("com.ftd.messaging.bo.SendMessageBO");
    private Connection dbConnection = null;
	private CalloutBO callOutBO = null;
	
    private String productType = null;

    public SendMessageBO() {
    }

    public SendMessageBO(Connection conn) {
        super();
        dbConnection = conn;
		callOutBO = new CalloutBO(conn);
    }

    /**
     * �        Call createMessageVO to retrieve message related data, such as
     *      type, message detail type, messageOrderNumber and orderDetailId
     *      from HTTP request.
     * �        Create Message Value Object.
     * �        If message type equals �Mercury�, call sendMercuryMessage method,
     *      if message type equals �Venus�, call sendVenusMessage method.
     * �        Return �communication�.
     * @param request - HttpServletRequest
     * @return String
     * @throws Exception????
     * @todo - code and determine exceptions
     */
    public String sendMessage(HttpServletRequest request, HashMap parameters) throws Exception{
        boolean debug=logger.isDebugEnabled();
        String forward=MessagingConstants.ACTION_FORWARD_COMMUNICATION;
        if (debug) {
            logger.debug("Entering sendMessage");
        }

        try {
            /* Call createMessageVO to retrieve message related data, such as
            * type, message detail type, messageOrderNumber and orderDetailId
            * from HTTP request.  */

            String messageType = (String) parameters.get(MessagingConstants.MESSAGE_TYPE);
            String messagDetailType=(String)parameters.get(MessagingConstants.MESSAGE_DETAIL_TYPE);
            if(debug)
            {
              logger.debug("message type ="+messageType);
            }
            if (messageType.equalsIgnoreCase(MessagingConstants.MERCURY_MESSAGE)) {



                logger.debug("Message detail type="+messagDetailType);
                ResultTO result=null;
                if (messagDetailType.equals("ASK")) {
                    ASKMessageTO askMessage = createASKMessageTO(request, parameters, null);
                    result=MessagingServiceLocator.getInstance().getMercuryAPI().sendASKMessage(askMessage, dbConnection);

                } else if (messagDetailType.equals("ANS")) {
                    ANSMessageTO ansMessage = createANSMessageTO(request,
                            parameters, null);

                    result=MessagingServiceLocator.getInstance().getMercuryAPI().sendANSMessage(ansMessage, dbConnection);
                }

                if(result==null||!result.isSuccess())
                   {
                     logger.error("Could not store mercury message [" +messagDetailType+"]. ERROR: "+result.getErrorString());
                     forward=MessagingConstants.ERROR;
                   }else
                   {
                           forward=this.getForwardDestination(parameters);


                   }




            } //no ask/ans messages for venus.

        } finally {
            if (logger.isDebugEnabled()) {
                logger.debug("Exiting sendMessage");
            }
        }

        return forward;
    }

    /**
     *  Call createMessageVO to retrieve message related data, such as type,
     *  message detail type, messageOrderNumber and orderDetailId from
     *  HTTP request.
     *  Create Message Value Object.
     *  Call sendMercuryMessage method.
     *  If the current date is greater than the last day of the delivery
     *  date month, return adjustment, otherwise return communication.
     * @param request - HttpServletRequest
     * @return String
     * @throws Exception????
     * @todo - code and determine exceptions
     */
    public String sendPriceMessage(HttpServletRequest request,
        HashMap parameters) throws ParseException, Exception {
        if (logger.isDebugEnabled()) {
            logger.debug("Entering sendPriceMessage");
        }

        String returnAction = "";
        ResultTO result=null;
        try {
            String messageType = (String) parameters.get(MessagingConstants.MESSAGE_DETAIL_TYPE);
            logger.debug("Message detail type="+messageType);
            if (messageType.equals("ASKP")) {
                ASKMessageTO askMessage = createASKMessageTO(request,
                        parameters, "P");

                result=MessagingServiceLocator.getInstance().getMercuryAPI().sendASKMessage(askMessage, dbConnection);

            } else if (messageType.equals("ANSP")) {
                ANSMessageTO ansMessage = createANSMessageTO(request,
                        parameters, "P");

                result=MessagingServiceLocator.getInstance().getMercuryAPI().sendANSMessage(ansMessage, dbConnection);
            }

            if(result==null||!result.isSuccess())
            {
            	logger.error("Could not store mercury message [" +messageType+"]. ERROR: "+result.getErrorString());
            	returnAction=MessagingConstants.ERROR;
            }else{

            	/*  If the current date is greater than the last day of the delivery
            	 *  date month, return adjustment, otherwise return communication. */
            	SimpleDateFormat sdfOutput = new SimpleDateFormat(MessagingConstants.MESSAGE_DATE_FORMAT);

            	Date deliveryDate = sdfOutput.parse(request.getParameter(
            			MessagingConstants.MESSAGE_DELIVERY_DATE));

            	if (checkDeliveryDate(deliveryDate) == true) {
            		returnAction = MessagingConstants.ACTION_FORWARD_ADJUSTMENT;
            	} else {
            		returnAction=this.getForwardDestination(parameters);
            	}
            }
        } finally {
            if (logger.isDebugEnabled()) {
                logger.debug("Exiting sendPriceMessage");
            }
        }

        return returnAction;
    }

    /**
     *  check if the current date is greater than the
     *  last day of the delivery date month
     * @param deliveryDate - Date
     * @return boolean
     * @throws ParseException
     */
    private boolean checkDeliveryDate(Date deliveryDate)
        throws ParseException {
        if (logger.isDebugEnabled()) {
            logger.debug("Entering checkDeliveryDate");
            logger.debug("deliveryDate: " + deliveryDate);
        }

        boolean dateComparision = false;

        try {
            Calendar calCurrent = Calendar.getInstance(TimeZone.getDefault());
            Date currentDate = calCurrent.getTime();

            SimpleDateFormat sdfOutput = new SimpleDateFormat(MessagingConstants.MESSAGE_DATE_FORMAT);

            if (currentDate.after(deliveryDate)) {
                Calendar calDelivery = Calendar.getInstance(TimeZone.getDefault());
                calDelivery.setTime(deliveryDate);

                int day = calDelivery.getActualMaximum(Calendar.DAY_OF_MONTH);

                Calendar calAdjustment = Calendar.getInstance(TimeZone.getDefault());
                calAdjustment.setTime(deliveryDate);
                calAdjustment.set(calDelivery.get(Calendar.YEAR),
                    calDelivery.get(Calendar.MONTH), day);

                Date adjustmentDate = calAdjustment.getTime();

                if (currentDate.after(adjustmentDate)) {
                    dateComparision = true;
                }
            }
        } finally {
            if (logger.isDebugEnabled()) {
                logger.debug("Exiting checkDeliveryDate");
            }
        }

        return dateComparision;
    }

    /**
     *  Retrieve message related data, such as type, message detail type,
     *  messageOrderNumber and orderDetailId from HTTP request
     *  and create an ASK Message TO.
     * @param request - HttpServletRequest
     * @param paramters - HashMap
     * @return MessageVO
     * @throws Exception????
     * @todo - code and determine exceptions
     */
    public ASKMessageTO createASKMessageTO(HttpServletRequest request,
        HashMap parameters, String subTypeCode) {
        if (logger.isDebugEnabled()) {
            logger.debug("Entering createASKMessageTO");
        }

        ASKMessageTO askTO = new ASKMessageTO();
        String reason="";
        try {
            String messageOrderNumber = (String) parameters.get(MessagingConstants.MESSAGE_ORDER_NUMBER);
            String oldPrice = request.getParameter(MessagingConstants.MESSAGE_OLD_PRICE);
            String price = request.getParameter("new_price");
            String operator = request.getParameter(MessagingConstants.MESSAGE_OPERATOR);
            String systemComments=request.getParameter("message_system_reason");
            String comments = request.getParameter(MessagingConstants.MESSAGE_COMMENTS);
            String sendingFlorist = this.getSenderFloristCode(request);
            String fillingFlorist = request.getParameter(MessagingConstants.REQUEST_FILLING_FLORIST);
            String ftdMessageID = request.getParameter(MessagingConstants.REQUEST_FTD_MESSAGE_ID);
            String approval_id = request.getParameter(MessagingConstants.APPROVAL_IDENTITY_ID);

            askTO.setMercuryId(ftdMessageID);

            if(StringUtils.isNotBlank(systemComments))
            {
              reason +=systemComments.trim();
            }

            if(StringUtils.isNotBlank(comments))
            {
              reason +="\t"+comments.trim();
            }
            askTO.setComments(reason);
            askTO.setAskAnswerCode(subTypeCode);
            askTO.setOperator(operator);
            askTO.setMercuryStatus(BaseMercuryMessageTO.MERCURY_OPEN);
            askTO.setDirection(BaseMercuryMessageTO.OUTBOUND);
            askTO.setOldPrice(getMessageTOPrice(oldPrice));
            askTO.setPrice(getMessageTOPrice(price));
            askTO.setFillingFlorist(fillingFlorist);
            askTO.setSendingFlorist(sendingFlorist);
            askTO.setApproval_identity_id(approval_id);

            if (logger.isDebugEnabled()) {
                logger.debug("createASKMessageTO:\n");
                logger.debug("order number ["+askTO.getMercuryId()+"]");
                logger.debug("comment ["+ askTO.getComments()+"]");
                logger.debug("ask_ans code ["+askTO.getAskAnswerCode()+"]");
                logger.debug("Operator ["+askTO.getOperator()+"]");
                logger.debug("Status ["+askTO.getMercuryStatus()+"]");
                logger.debug("direction ["+askTO.getDirection()+"]");
                logger.debug("message type ["+askTO.getMessageType()+"]");
                logger.debug("old price ["+askTO.getOldPrice()+"]");
                logger.debug("price ["+askTO.getPrice()+"]");
                logger.debug("Filling Florist ["+askTO.getFillingFlorist()+"]");
                logger.debug("Sending Florist ["+askTO.getSendingFlorist()+"]");
                logger.debug("approval identity id ["+askTO.getApproval_identity_id()+"]");
            }

        } finally {
            if (logger.isDebugEnabled()) {
                logger.debug("Exiting createASKMessageTO");
            }
        }

        return askTO;
    }

    private Double getMessageTOPrice(String priceStr) {
        String regex = "(?<=\\d),(?=\\d)";
        if (priceStr != null )
        {
            priceStr = priceStr.replaceAll(regex, "");
        }
        Double price = new Double(0);

        if ((priceStr != null) && (priceStr.trim().length() > 0)) {
            try {
                price = (new Double(priceStr));
            } catch (NumberFormatException ne) {
                logger.error("can not parse price. ", ne);
                //don't need to throw an exception.
            }
        }

        return price;
    }

    /**
     *  Retrieve message related data, such as type, message detail type,
     *  messageOrderNumber and orderDetailId from HTTP request
     *  and create an ANS Message TO.  In addition, insert any information
     *  retrieved from the request into the parameters HashMap
     * @param request - HttpServletRequest
     * @param paramters - HashMap
     * @return MessageVO
     * @throws Exception????
     * @todo - code and determine exceptions
     */
    public ANSMessageTO createANSMessageTO(HttpServletRequest request,
        HashMap parameters, String subTypeCode) {
        ANSMessageTO ansTO = new ANSMessageTO();

        if (logger.isDebugEnabled()) {
            logger.debug("Entering createANSMessageTO");
        }

        try {
            String messageOrderNumber = (String) parameters.get(MessagingConstants.MESSAGE_ORDER_NUMBER);
            String oldPrice = request.getParameter(MessagingConstants.MESSAGE_OLD_PRICE);
            String price = request.getParameter("new_price");
            String operator = request.getParameter(MessagingConstants.MESSAGE_OPERATOR);
            String comments = request.getParameter(MessagingConstants.MESSAGE_COMMENTS);
            String sendingFlorist = this.getSenderFloristCode(request);
            String fillingFlorist = request.getParameter(MessagingConstants.REQUEST_FILLING_FLORIST);
            String ftdMessageID = request.getParameter(MessagingConstants.REQUEST_FTD_MESSAGE_ID);
            String approval_id = request.getParameter(MessagingConstants.APPROVAL_IDENTITY_ID);

            ansTO.setMercuryId(ftdMessageID);
            ansTO.setComments(comments);
            ansTO.setAskAnswerCode(subTypeCode);
            ansTO.setOperator(operator);
            ansTO.setMercuryStatus(BaseMercuryMessageTO.MERCURY_OPEN);
            ansTO.setDirection(BaseMercuryMessageTO.OUTBOUND);
            ansTO.setOldPrice(getMessageTOPrice(oldPrice));
            ansTO.setPrice(getMessageTOPrice(price));
            ansTO.setFillingFlorist(fillingFlorist);
            ansTO.setSendingFlorist(sendingFlorist);
            ansTO.setApproval_identity_id(approval_id);

        } finally {
            if (logger.isDebugEnabled()) {
                logger.debug("Exiting createANSMessageTO");
            }
        }

        return ansTO;
    }



     public CANMessageTO createCANMessageTO(HttpServletRequest request,
        HashMap parameters) throws Exception
        {
          return this.createCANMessageTO(request, parameters, false);
        }



    /**
     *  Retrieve message related data, such as type, message detail type,
     *  messageOrderNumber and orderDetailId from HTTP request
     *  and create an CAN Message TO.  In addition, insert any information
     *  retrieved from the request into the parameters HashMap
     * @param request - HttpServletRequest
     * @param paramters - HashMap
     * @return MessageVO
     * @throws Exception????
     * @todo - code and determine exceptions
     */
    public CANMessageTO createCANMessageTO(HttpServletRequest request,
        HashMap parameters, boolean fromCallout) throws Exception {

        CANMessageTO canTO = new CANMessageTO();
        if (logger.isDebugEnabled()) {
            logger.debug("Entering createCANMessageTO");
        }

        try {
            String messageOrderNumber = (String) parameters.get(MessagingConstants.MESSAGE_ORDER_NUMBER);
            String operator = request.getParameter(MessagingConstants.MESSAGE_OPERATOR);
            String comments = request.getParameter(MessagingConstants.MESSAGE_COMMENTS);
            String sendingFlorist = this.getSenderFloristCode(request);
            String fillingFlorist = request.getParameter(MessagingConstants.REQUEST_FILLING_FLORIST);
            String ftdMessageID = request.getParameter(MessagingConstants.REQUEST_FTD_MESSAGE_ID);
            String complaintCommOriginTypeId = request.getParameter(MessagingConstants.REQUEST_COMPLAINT_COMM_ORIGIN_TYPE_ID);
            String complaintCommNotificationTypeId = request.getParameter(MessagingConstants.REQUEST_COMPLAINT_COMM_NOTIFICATION_TYPE_ID);
          
            canTO.setMercuryId(ftdMessageID);
            canTO.setComments(comments);

            canTO.setOperator(operator);
            if(fromCallout)
            {
              canTO.setMercuryStatus(BaseMercuryMessageTO.MERCURY_NOSEND);
            }else
            {
              canTO.setMercuryStatus(getMessageTOStatus(request));
            }


            canTO.setDirection(BaseMercuryMessageTO.OUTBOUND);
            canTO.setFillingFlorist(fillingFlorist);
            canTO.setSendingFlorist(sendingFlorist);
            canTO.setComplaintCommOriginTypeId(complaintCommOriginTypeId);
            canTO.setComplaintCommNotificationTypeId(complaintCommNotificationTypeId);




        } finally {
            if (logger.isDebugEnabled()) {
                logger.debug("Exiting createCANMessageTO");
            }
        }
        return canTO;
    }






    public CancelOrderTO createVenusCANMessageTO(HttpServletRequest request,
        HashMap parameters, boolean adjustmentNeed) throws Exception {
        CancelOrderTO canTO = new CancelOrderTO();

        if (logger.isDebugEnabled()) {
            logger.debug("Entering createCANMessageTO");
        }

        try {
            String operator = request.getParameter(MessagingConstants.MESSAGE_OPERATOR);
            String comments = request.getParameter(MessagingConstants.MESSAGE_COMMENTS);
            String cancelReasonCode = request.getParameter("reason_code");
            String ftdMessageID = request.getParameter(MessagingConstants.REQUEST_FTD_MESSAGE_ID);
            String complaintCommOriginTypeId = request.getParameter(MessagingConstants.REQUEST_COMPLAINT_COMM_ORIGIN_TYPE_ID);
            String complaintCommNotificationTypeId = request.getParameter(MessagingConstants.REQUEST_COMPLAINT_COMM_NOTIFICATION_TYPE_ID);

            canTO.setVenusId(ftdMessageID);
            canTO.setComments(comments);
            canTO.setCsr(operator);
            canTO.setCancelReasonCode(cancelReasonCode);
            canTO.setComplaintCommOriginTypeId(complaintCommOriginTypeId);
            canTO.setComplaintCommNotificationTypeId(complaintCommNotificationTypeId);
            boolean venusOrderPrintedOrShipped = false; // if a venus order is already printed or shipped, cancelling causes an automatic ADJ
            if(adjustmentNeed)
            {
              canTO.setTransmitCancel(false);
            }
        } finally {
            if (logger.isDebugEnabled()) {
                logger.debug("Exiting createCANMessageTO");
            }
        }

        return canTO;
    }




    private String getMessageTOStatus(HttpServletRequest request) {
        String status = BaseMercuryMessageTO.MERCURY_OPEN;
        String orderType = request.getParameter(MessagingConstants.MESSAGE_ORDER_TYPE);
        String msgDetailType=request.getParameter(MessagingConstants.MESSAGE_DETAIL_TYPE);
        logger.debug("message detail type ="+msgDetailType);
        if(msgDetailType!=null&&!msgDetailType.trim().equalsIgnoreCase("FTD")){

            if ((orderType != null) &&
                    orderType.equalsIgnoreCase(MessageOrderStatusVO.FTDM)) {
                status = BaseMercuryMessageTO.MERCURY_NOSEND;
            }
        }

        return status;
    }

    /**
     *  Retrieve message related data, such as type, message detail type,
     *  messageOrderNumber and orderDetailId from HTTP request
     *  and create an ADJ Message TO.  In addition, insert any information
     *  retrieved from the request into the parameters HashMap
     * @param request - HttpServletRequest
     * @param paramters - HashMap
     * @return MessageVO
     * @throws Exception????
     * @todo - code and determine exceptions
     */
    public ADJMessageTO createADJMessageTO(HttpServletRequest request,
        HashMap parameters) {
        if (logger.isDebugEnabled()) {
            logger.debug("Entering createADJMessageTO");
        }

        ADJMessageTO adjTO = new ADJMessageTO();

        try {
            String messageOrderNumber = (String) parameters.get(MessagingConstants.MESSAGE_ORDER_NUMBER);
            String oldPrice = request.getParameter(MessagingConstants.MESSAGE_OLD_PRICE);
            String price = request.getParameter(MessagingConstants.MESSAGE_PRICE);
            String operator = request.getParameter(MessagingConstants.MESSAGE_OPERATOR);
            String comments = request.getParameter(MessagingConstants.MESSAGE_COMMENTS);
            String combinedReportNum = request.getParameter(MessagingConstants.MESSAGE_COMBINED_RPT_NUM);
            String rofNumber = request.getParameter(MessagingConstants.MESSAGE_ROF_NUMBER);
            String adjReasonCode = request.getParameter("reason_code");
            String sendingFlorist = getSenderFloristCode(request);
            String fillingFlorist = request.getParameter(MessagingConstants.REQUEST_FILLING_FLORIST);
            String ftdMessageID = request.getParameter(MessagingConstants.REQUEST_FTD_MESSAGE_ID);

            adjTO.setMercuryId(ftdMessageID);
            adjTO.setComments(comments);
            adjTO.setCombinedReportNumber(combinedReportNum);
            adjTO.setOperator(operator);

            adjTO.setMercuryStatus(this.getMessageTOStatus(request));

            adjTO.setDirection(BaseMercuryMessageTO.OUTBOUND);
            adjTO.setOldPrice(getMessageTOPrice(oldPrice));
            //adjTO.setPrice(getMessageTOPrice(price));
            adjTO.setOverUnderCharge(getMessageTOPrice(price));
            adjTO.setAdjustmentReasonCode(adjReasonCode);
            adjTO.setSendingFlorist(sendingFlorist);
            adjTO.setFillingFlorist(fillingFlorist);


        } finally {
            if (logger.isDebugEnabled()) {
                logger.debug("Exiting createADJMessageTO");
            }
        }
        return adjTO;
    }


  String getSenderFloristCode(HttpServletRequest request)
  {
    String sendingFlorist = request.getParameter(MessagingConstants.REQUEST_SENDING_FLORIST);
    if(sendingFlorist!=null&&sendingFlorist.length()>7)
    {
      sendingFlorist=sendingFlorist.substring(0,7);
    }
    logger.debug("sendingFlorist =["+sendingFlorist+"]");
    return sendingFlorist;
  }


    /**
     *  Retrieve message related data, such as type, message detail type,
     *  messageOrderNumber and orderDetailId from HTTP request
     *  and create an ADJ Message TO.  In addition, insert any information
     *  retrieved from the request into the parameters HashMap
     * @param request - HttpServletRequest
     * @param paramters - HashMap
     * @return AdjustmentMessageTO
     * @throws Exception
     * @todo - code and determine exceptions
     */
    public AdjustmentMessageTO createVenusAdjustmentMessageTO(HttpServletRequest request,
        HashMap parameters) throws Exception{
        if (logger.isDebugEnabled()) {
            logger.debug("Entering createVenusAdjustmentMessageTO");
        }

        AdjustmentMessageTO adjTO = new AdjustmentMessageTO();

        try {
            String messageOrderNumber = (String) parameters.get(MessagingConstants.MESSAGE_ORDER_NUMBER);
            String oldPrice = request.getParameter(MessagingConstants.MESSAGE_OLD_PRICE);
            String price = request.getParameter(MessagingConstants.MESSAGE_PRICE);
            String operator = request.getParameter(MessagingConstants.MESSAGE_OPERATOR);
            String comments = request.getParameter(MessagingConstants.MESSAGE_COMMENTS);
            String fillingVendor = request.getParameter(MessagingConstants.REQUEST_FILLING_FLORIST);
            String orderDetailID = (String) parameters.get(MessagingConstants.REQUEST_ORDER_DETAIL_ID);
            String messageType = (String) parameters.get(MessagingConstants.MESSAGE_DETAIL_TYPE);
            String canReasonCode = request.getParameter(MessagingConstants.ADJ_CAN_REASON_CODE);

            adjTO.setComments(comments);
            adjTO.setOperator(operator);
            adjTO.setPrice(getMessageTOPrice(oldPrice));
            adjTO.setMessageType(messageType);
            adjTO.setOrderDetailId(orderDetailID);
            adjTO.setVenusOrderNumber(messageOrderNumber);
            adjTO.setFillingVendor(fillingVendor);
            adjTO.setOverUnderCharge(getMessageTOPrice(price));
            
            //add cancel reason code to be used for ADJ.  This is only used for ESCALATE orders.
            if(canReasonCode != null)
              adjTO.setCancelReasonCode(canReasonCode);
            
        }
        finally {
            if (logger.isDebugEnabled()) {
                logger.debug("Exiting createVenusAdjustmentMessageTO");
            }
        }

        return adjTO;
    }

      /**
     *  This method creates an automatic ADJ for vendor orders that are cancelled after
     *  they are printed/shipped.  Retrieve message related data, such as type, message detail type,
     *  messageOrderNumber and orderDetailId from HTTP request
     *  and create an ADJ Message TO.  In addition, insert any information
     *  retrieved from the request into the parameters HashMap
     * @param request - HttpServletRequest
     * @param paramters - HashMap
     * @return AdjustmentMessageTO
     * @throws Exception
     * @todo - code and determine exceptions
     */
    public AdjustmentMessageTO createVenusAdjustmentMessageTOFromCAN(HttpServletRequest request,
        HashMap parameters) throws Exception{
        if (logger.isDebugEnabled()) {
            logger.debug("Entering createVenusAdjustmentMessageTOFromCAN");
        }

        AdjustmentMessageTO adjTO = new AdjustmentMessageTO();

        try {
              // directly from CAN //
            String messageOrderNumber = (String) parameters.get(MessagingConstants.MESSAGE_ORDER_NUMBER);
            String operator = request.getParameter(MessagingConstants.MESSAGE_OPERATOR);
            String fillingVendor = request.getParameter(MessagingConstants.REQUEST_FILLING_FLORIST);
            String orderDetailID = (String) parameters.get(MessagingConstants.REQUEST_ORDER_DETAIL_ID);
            adjTO.setVenusOrderNumber(messageOrderNumber);
            adjTO.setOperator(operator);
            adjTO.setFillingVendor(fillingVendor);
            adjTO.setOrderDetailId(orderDetailID);
            MessageDAO messageDao = new MessageDAO(this.dbConnection);
            MessageVO messageVO=messageDao.getMessageForCurrentFlorist(messageOrderNumber, MessagingConstants.ADJ_MESSAGE, false, null, true);
            // New ADJ Fields //
            String messageType = (String) parameters.get(MessagingConstants.MESSAGE_DETAIL_TYPE);
            String oldPrice = new Double(messageVO.getOldPrice()).toString();
            String price = new Double(messageVO.getCurrentPrice()).toString();
            adjTO.setPrice(getMessageTOPrice(price));
            adjTO.setComments(MessagingConstants.AUTOMATIC_ADJ_FROM_CANCEL_COMMENTS);
            adjTO.setMessageType(MessagingConstants.ADJ_MESSAGE);
            adjTO.setOverUnderCharge(MessagingConstants.AUTOMATIC_ADJ_FROM_CANCEL_OVER_UNDER_CHARGE);
            adjTO.setCancelReasonCode(request.getParameter("reason_code"));     
        }
        finally {
            if (logger.isDebugEnabled()) {
                logger.debug("Exiting createVenusAdjustmentMessageTOFromCAN");
            }
        }

        return adjTO;
    }
    /**
     *  Retrieve message related data, such as type, message detail type,
     *  messageOrderNumber and orderDetailId from HTTP request
     *  and create an FTD Message TO.  In addition, insert any information
     *  retrieved from the request into the parameters HashMap
     * @param request - HttpServletRequest
     * @param paramters - HashMap
     * @return MessageVO
     * @throws Exception????
     * @todo - code and determine exceptions
     */
    public FTDMessageTO createFTDCOMPMessageTO(HttpServletRequest request,
        HashMap parameters,boolean ftdmMessage) throws ParseException, ParserConfigurationException, IOException, SAXException, SQLException {
        FTDMessageTO ftdMsg = new FTDMessageTO();
        if (logger.isDebugEnabled()) {
            logger.debug("Entering createFTDCOMPMessageTO");
        }

        try {
            MessageDAO msgDAO=new  MessageDAO(this.dbConnection);
            boolean compOrder =new Boolean(request.getParameter(MessagingConstants.MESSAGE_COMP_ORDER)).booleanValue();
            String sendingFlorist = this.getSenderFloristCode(request);
            String fillingFlorist = request.getParameter(MessagingConstants.REQUEST_FILLING_FLORIST);
            logger.info("fillingFlorist: " + fillingFlorist);
            String zipCity = request.getParameter("zip_city_flag");
            logger.info("zipCity: " + zipCity);
            String recipientName=request.getParameter("recipient_name");
            String recipientStreet=request.getParameter("recipient_street");
            String businessName=request.getParameter("business_name");
            if (businessName != null && !businessName.equals("")) {
            	recipientStreet += "\n" + businessName;
            }
            String recipientCity=request.getParameter("recipient_city");
            String recipientState=request.getParameter("recipient_state");
            String recipientZip=request.getParameter("recipient_zip");
            String occasion = request.getParameter("occasion_code");
            SimpleDateFormat sdfOutput = new SimpleDateFormat("EEE MMM dd hh:mm:ss z yyyy");
            SimpleDateFormat sdfDeliveryDateRange = new SimpleDateFormat("MMM dd - EEE");
            String sOrderDate = request.getParameter(MessagingConstants.MESSAGE_ORDER_DATE);
            Date orderDate = null;
            java.sql.Date sqlOrderDate = null;
            if (StringUtils.isNotBlank(sOrderDate))
            {
              orderDate = sdfOutput.parse(sOrderDate);
              sqlOrderDate = new java.sql.Date(orderDate.getTime());
            }

            String phoneNumber="";
            if(StringUtils.isNotBlank(request.getParameter("recipient_phone")))
            {
              phoneNumber=request.getParameter("recipient_phone");
            }else {
              phoneNumber = request.getParameter("recipient_phone_area")+"/"+request.getParameter("recipient_phone_prefix")+"-"+request.getParameter("recipient_phone_number");

            }
            
            String country=request.getParameter("recipient_country");

            Date deliveryDate = getDeliveryDate(request);
            Date deliveryDateEnd = getDeliveryDateEnd(request);

            String deliveryDateText = null;
            if (StringUtils.isNotBlank(request.getParameter("delivery_month")) && 
                StringUtils.isNotBlank(request.getParameter("delivery_date"))) 
            {
                SimpleDateFormat sdfDeliveryDate = new SimpleDateFormat("MMM dd");
                deliveryDateText = this.getDateString(deliveryDate, sdfDeliveryDate );                        
            } else {
                deliveryDateText = request.getParameter("order_delivery_date");
            }
            
            if(deliveryDate!=null){
              java.sql.Date sqlDeliveryDate = new java.sql.Date(deliveryDate.getTime());
              ftdMsg.setDeliveryDate(sqlDeliveryDate);
              logger.debug("setDeliveryDate="+ftdMsg.getDeliveryDate());
            }

            String specialInstructions = request.getParameter("spec_instruction");
            if(deliveryDateEnd!=null)
            {
                String dateRange = this.getDateString(deliveryDate, sdfDeliveryDateRange);
                specialInstructions += " Delivery " + dateRange + " or " + this.getDateString(deliveryDateEnd, sdfDeliveryDateRange) + " OK ";
            }
            ftdMsg.setDeliveryDateText(deliveryDateText);

            logger.debug("setDeliveryDateText="+ftdMsg.getDeliveryDateText());
            String firstChoice = request.getParameter(MessagingConstants.MESSAGE_FIRST_CHOICE);
            String secondChoice = request.getParameter("allowed_substitution");
            Double price = getMessageTOPrice(request.getParameter("florist_prd_price"));
            Double oldPrice = getMessageTOPrice(request.getParameter("ftd_origin_price"));
            logger.debug("ftd old price="+oldPrice);
			          
            Double additionalFee = getMessageTOPrice(request.getParameter("additional_delivery_fee"));

            String cardMessage = request.getParameter(MessagingConstants.MESSAGE_CARD_MESSAGE);

            String operator = request.getParameter(MessagingConstants.MESSAGE_OPERATOR);

            String productID = request.getParameter(MessagingConstants.MESSAGE_PRODUCT_ID);

            String messageDirection = MessagingConstants.OUTBOUND_MESSAGE;

            String requireConfirmation = request.getParameter(MessagingConstants.MESSAGE_REQUIRE_CONFIRMATION);
            String orderDetailID = (String) parameters.get(MessagingConstants.REQUEST_ORDER_DETAIL_ID);

            //get Box X occasion desc.
            String occasionDesc=msgDAO.getBoxXOccasionDesc(occasion);

            ftdMsg.setAddress(recipientStreet);
            ftdMsg.setCardMessage(cardMessage);

            String cityStateZip=this.buildCityStateZipString(country, recipientCity, recipientState, recipientZip);
            logger.debug("cityStateZip="+cityStateZip);
            ftdMsg.setCityStateZip(cityStateZip);
            ftdMsg.setPhoneNumber(phoneNumber);
            ftdMsg.setDirection(messageDirection);
            ftdMsg.setFillingFlorist(fillingFlorist);
            ftdMsg.setFirstChoice(firstChoice);
            if(compOrder){
                ftdMsg.setIsCompOrder("C");
            }

            if(ftdmMessage==false){
                ftdMsg.setMercuryStatus(getMessageTOStatus(request));
            }
            else
            {
                ftdMsg.setMercuryStatus(BaseMercuryMessageTO.MERCURY_NOSEND);
                if(oldPrice.doubleValue()!=0&&oldPrice.compareTo(price)!=0)
                {
                  //set the old price as needed.
                  ftdMsg.setOldPrice(oldPrice);
                }
            }
            ftdMsg.setOccasion(occasionDesc);
            ftdMsg.setOperator(operator);
            ftdMsg.setOrderDate(sqlOrderDate);
            ftdMsg.setOrderDetailId(orderDetailID);
            ftdMsg.setPhoneNumber(phoneNumber);
			
			//adding the additional delivery fee if exists 
            if(additionalFee != null && additionalFee.SIZE > 0){
            	logger.info("Original Price:: "+ price +", additional delivery Fee:: "+ additionalFee);
                price = additionalFee + price;
                price = Math.round(price * 100D) / 100D;
                logger.info("New Merc price:: "+ price);
            }
            ftdMsg.setPrice(price);

            ftdMsg.setProductId(productID);
            ftdMsg.setRecipient(recipientName);
            ftdMsg.setRequestConfirmation(requireConfirmation);
            ftdMsg.setSecondChoice(secondChoice);
            ftdMsg.setSendingFlorist(sendingFlorist);
            ftdMsg.setSpecialInstructions(specialInstructions);
            logger.debug("setSpecialInstructions="+ftdMsg.getSpecialInstructions());
            logger.debug("setOccasion="+ftdMsg.getOccasion());
            ftdMsg.setZipCode(recipientZip);
        } finally {
            if (logger.isDebugEnabled()) {
                logger.debug("Exiting createFTDMessageTO");
            }
        }

        return ftdMsg;
    }
    
    
    private Date getDeliveryDate(HttpServletRequest request) {
        String monthStr = request.getParameter("delivery_month");
        String dateStr=request.getParameter("delivery_date");
        Date deliveryDate = null;
        MessagingHelper helper=new MessagingHelper();
        SimpleDateFormat sdfOutput = new SimpleDateFormat("EEE MMM dd hh:mm:ss z yyyy");
                
        if (StringUtils.isNotBlank(monthStr)&&StringUtils.isNotBlank(dateStr)) {
          deliveryDate = helper.createDeliveryDate(monthStr, dateStr);
        } else {
           String sDeliveryDate = request.getParameter("order_delivery_timestamp");
           if (StringUtils.isNotBlank(sDeliveryDate)) {
               try {
                   deliveryDate = sdfOutput.parse(sDeliveryDate);
               } catch (ParseException e) {
                   deliveryDate = null;
               }
           }
        }
        return deliveryDate;
    }

    private Date getDeliveryDateEnd(HttpServletRequest request) {
        String monthStrEnd = request.getParameter("delivery_month_end");
        String dateStrEnd=request.getParameter("delivery_date_end");
        Date deliveryDateEnd=null;
        MessagingHelper helper=new MessagingHelper();
                
        if (StringUtils.isNotBlank(monthStrEnd)&&StringUtils.isNotBlank(dateStrEnd)) {
          deliveryDateEnd = helper.createDeliveryDate(monthStrEnd, dateStrEnd);
        }
        return deliveryDateEnd;
    }




    private String buildCityStateZipString(String country, String city, String state, String zipCode)
   {
        String cityStateZip = "";


        if (StringUtils.isBlank(country)|| country.equalsIgnoreCase("US") || country.equalsIgnoreCase("USA")||country.equalsIgnoreCase("CA"))
        {
          cityStateZip = city+ ", "+state+" "+zipCode;
        }
        else
        {
          cityStateZip = city + ", ";
          if (state == null)
          {
            cityStateZip = cityStateZip.concat("NA ");
          }
          else
          {
            cityStateZip = cityStateZip.concat(state + " ");
          }
          if ((zipCode != null)&&(!zipCode.equalsIgnoreCase("99999")))
          {
            cityStateZip = cityStateZip.concat(zipCode);
          }

        }
        return cityStateZip;
   }




    private String getDateString(Date date, SimpleDateFormat sdfOutput)
    {
        if (sdfOutput == null) {
            sdfOutput=new SimpleDateFormat("MMM dd");
        }
        return sdfOutput.format(date).toUpperCase();
    }



    /**
     *  Retrieve message related data, such as type, message detail type,
     *  messageOrderNumber and orderDetailId from HTTP request
     *  and create an FTD Message TO.  In addition, insert any information
     *  retrieved from the request into the parameters HashMap
     * @param request - HttpServletRequest
     * @param paramters - HashMap
     * @return MessageVO
     * @throws Exception????
     * @todo - code and determine exceptions
     */
    public OrderTO createVenusFTDOrderMessageTO(HttpServletRequest request,
        HashMap parameters) throws ParseException, Exception {


        OrderTO orderTO = new OrderTO();
        Date deliveryDate = null;
        Date shipDate = null;

        if (logger.isDebugEnabled()) {
            logger.debug("Entering createVenusFTDOrderMessageTO");
        }

        try {
            MessagingHelper helper=new MessagingHelper();
            String fillingFlorist = request.getParameter(MessagingConstants.REQUEST_FILLING_FLORIST);
            SimpleDateFormat sdfOutput = new SimpleDateFormat("EEE MMM dd hh:mm:ss z yyyy");
            Date orderDate = sdfOutput.parse(request.getParameter(
                        MessagingConstants.MESSAGE_ORDER_DATE));

            boolean comp=new Boolean(request.getParameter("comp_order")).booleanValue();
            String recipientStreet=request.getParameter("recipient_street");
            String recipientCity=request.getParameter("recipient_city");
            String recipientState=request.getParameter("recipient_state");
            String recipientZip=request.getParameter("recipient_zip");
            String recipientCountry=request.getParameter("recipient_country");
            String phoneNumber = request.getParameter("recipient_phone");
            String monthStr = request.getParameter("delivery_month");
            String dateStr=request.getParameter("delivery_date");
            String businessName=request.getParameter("business_name");

            String shipMethod=request.getParameter("ship_method");
            logger.debug("shipMethod="+shipMethod);

            String productID = request.getParameter(MessagingConstants.MESSAGE_PRODUCT_ID);
            logger.debug("productID="+productID);

            //need to add logic that if this is a vendor new ftd or comp order
            //then we need to get the real delivery date out of the request
            //and then create a calendar object out of it
            String pageDeliveryDate = request.getParameter("delivery_date");
            logger.debug("pageDeliveryDate="+pageDeliveryDate);

            deliveryDate = new SimpleDateFormat("MM/dd/yyyy").parse(pageDeliveryDate);

            String sDate = computeShipDate(shipMethod, pageDeliveryDate, recipientState, recipientCountry, productID);
            logger.debug("shipDate="+sDate);
            shipDate = new SimpleDateFormat("MM/dd/yyyy").parse(sDate);


            Double price = getMessageTOPrice(request.getParameter(
                        MessagingConstants.MESSAGE_PRICE));
            String cardMessage = request.getParameter(MessagingConstants.MESSAGE_CARD_MESSAGE);
            String operator = request.getParameter(MessagingConstants.MESSAGE_OPERATOR);



            String messageDirection = MessagingConstants.OUTBOUND_MESSAGE;

            String orderDetailID = (String) parameters.get(MessagingConstants.REQUEST_ORDER_DETAIL_ID);
            String messageType = (String) parameters.get(MessagingConstants.MESSAGE_TYPE);
            String messageOrderNumber = (String) parameters.get(MessagingConstants.MESSAGE_ORDER_NUMBER);
            String recipientName = request.getParameter("recipient_name");

            orderTO.setReferenceNumber(orderDetailID);
            logger.debug("orderTO.getReferenceNumber()="+orderTO.getReferenceNumber());
            orderTO.setAddress1(recipientStreet);
            orderTO.setAddress2("");
            orderTO.setBusinessName(recipientName);
            orderTO.setCardMessage(cardMessage);
            orderTO.setCity(recipientCity);
            orderTO.setDeliveryDate(deliveryDate);
            orderTO.setFillingVendor(fillingFlorist);
            orderTO.setMessageType("FTD");
            orderTO.setOperator(operator);
            orderTO.setOrderDate(orderDate);
            orderTO.setOverUnderCharge(new Double(0.0));
            orderTO.setPhoneNumber(phoneNumber);
            orderTO.setPrice(price);
            orderTO.setProductId(productID);
            orderTO.setRecipient(recipientName);
            orderTO.setState(recipientState);
            orderTO.setShipMethod(shipMethod);
            orderTO.setShipDate(shipDate);
            orderTO.setBusinessName(businessName);
            orderTO.setVenusOrderNumber(messageOrderNumber);
            orderTO.setZipCode(recipientZip);
            orderTO.setCountry(recipientCountry);
            orderTO.setFromCommunicationScreen(true);


            if(comp)
            {
              orderTO.setCompOrder("C");
            }
            logger.debug("new delivery date ="+orderTO.getDeliveryDate());

        } finally {
            if (logger.isDebugEnabled()) {
                logger.debug("Exiting createVenusFTDOrderMessageTO");
            }
        }

        return orderTO;
    }



     public String sendCancelMessage(HttpServletRequest request,
        HashMap parameters) throws ParseException, Exception {

        return this.sendCancelMessage(request, parameters, false);
    }




    /**
     *  Send Venus messages via Mercury Message API.
     * @param request - HttpServletRequest
     * @return String
     * @throws Exception????
     * @todo - code and determine exceptions
     */
    public String sendCancelMessage(HttpServletRequest request,
        HashMap parameters, boolean fromCallout) throws ParseException, Exception {

      if (logger.isDebugEnabled()) {
        logger.debug("Entering sendCancelMessage");
      }

      String forward = MessagingConstants.ACTION_FORWARD_COMMUNICATION;
      String csrId="";
      ResultTO result=new ResultTO();
      MessageVO ftdMsg = null;
      boolean venusOrderPrintedOrShipped=false;
      boolean adjustmentNeed=false;
      boolean SDSAdjRequired = false;
      boolean SDSShippingSystem = false;
      String subsequentAction=request.getParameter(MessagingConstants.SUBSEQUENT_ACTION);
      try {
        String orderType = (String) parameters.get(MessagingConstants.MESSAGE_ORDER_TYPE);


        if (orderType.equals(MessageOrderStatusVO.VENDOR)) {
          
          /* Test to see if an adjustment is required for SDS or FTD WEST orders.
           * DI-9 we will consider FTD WEST as an SDS shipping system.  
           * If so set flag to send auto ADJ and use the SDSShippingSystem flag to 
           * suppress ESCALATE adjustment logic/save processing time.
           */
          MessageUtil msgUtil = new MessageUtil(dbConnection);
          ftdMsg = msgUtil.getMessage((String) parameters.get(MessagingConstants.MESSAGE_ORDER_NUMBER));
          SDSAdjRequired = msgUtil.requiresSDSAutoADJ(ftdMsg);
          SDSShippingSystem = msgUtil.isShippingSystemSDS(ftdMsg);
          if(SDSAdjRequired)
          {
            forward = MessagingConstants.ACTION_FORWARD_ADJUSTMENT;
            adjustmentNeed=true;
          }
          
          String orderPrinted = request.getParameter(MessagingConstants.MESSAGE_ORDER_STATUS);

          // If SDSShippingSystem no need to check ESCALATE requirements
          if (!SDSShippingSystem && orderPrinted != null) {
            boolean printed = (orderPrinted.equalsIgnoreCase("shipped"));

            if (printed) {
              forward = MessagingConstants.ACTION_FORWARD_ADJUSTMENT;
              adjustmentNeed=true;
              venusOrderPrintedOrShipped=true;
            }
          }
        }

        // Do not check delivery date if shipping system is SDS
        if (!adjustmentNeed && !SDSShippingSystem)
        {
          SimpleDateFormat sdfOutput=null;
          Date deliveryDate=null;
          if(fromCallout)
          {
            sdfOutput = new SimpleDateFormat("EEE MMM dd hh:mm:ss z yyyy");
            deliveryDate= sdfOutput.parse(request.getParameter("order_delivery_timestamp"));

          }else
          {
            sdfOutput = new SimpleDateFormat(MessagingConstants.MESSAGE_DATE_FORMAT);

            deliveryDate = sdfOutput.parse( request.getParameter(
                   MessagingConstants.MESSAGE_DELIVERY_DATE));

          }

          if (checkDeliveryDate(deliveryDate) == true) {
            request.setAttribute( MessagingConstants.REQUEST_ATTRIBUTE_ACTION,
                                  MessagingConstants.ACTION_PREPARE_MESSAGE);
            adjustmentNeed=true;

          }

        }

        String messageType = (String) parameters.get(MessagingConstants.MESSAGE_TYPE);

        if (messageType.equals(MessagingConstants.MERCURY_MESSAGE)) {
          /* create cancel message TO */
          CANMessageTO cancelMsg = createCANMessageTO(request, parameters, fromCallout);
          csrId=cancelMsg.getOperator();
          result=MessagingServiceLocator.getInstance().getMercuryAPI().sendCANMessage(cancelMsg, dbConnection);
        } else if (messageType.equals(MessagingConstants.VENUS_MESSAGE)) {
          /* If message type equals �Venus�, call sendVenusMessage method */

          CancelOrderTO cancelTO=this.createVenusCANMessageTO(request, parameters, adjustmentNeed);
          csrId=cancelTO.getCsr();
          
          /* If shipping system is SDS and an ADJ is required set the 
           * Venus Status to VERIFIED so the CAN is not sent to SDS.
           */ 
          if(SDSShippingSystem && adjustmentNeed)
          {
            cancelTO.setVenusStatus(MessagingConstants.VENUS_STATUS_VERIFIED);
          }
          
          result= MessagingServiceLocator.getInstance().getVenusAPI().sendCancel(cancelTO, dbConnection);
        }

        if(!result.isSuccess())
        {
          logger.error("Could not store "+messageType+" CAN message. ERROR: "+result.getErrorString());
          //forward=MessagingConstants.ERROR;
          forward=MessagingConstants.ACTION_FORWARD_COMMUNICATION;
          parameters.put("ERROR_ON_SEND", result.getErrorString());          
        }else{



          if (orderType.equals(MessageOrderStatusVO.FTDM)||fromCallout) {

            //update Call out log:
            String messageId=result.getKey();
            
            CalloutBO callOutBO = new CalloutBO(this.dbConnection);
            CallOutInfo callOutInfo = callOutBO.setCallOutFloristInfo(request);
                
            MessageDAO messageDAO=new MessageDAO(this.dbConnection);
            messageDAO.insertCallOutLog(messageId, csrId, callOutInfo);

            updateOrderComments(parameters, request, callOutInfo, csrId);
          }

          if(adjustmentNeed)
          {
            if(venusOrderPrintedOrShipped || SDSAdjRequired)
            {
              AdjustmentMessageTO adjTO = this.createVenusAdjustmentMessageTOFromCAN(request, parameters);
              result = MessagingServiceLocator.getInstance().getVenusAPI().sendAdjustment(adjTO, dbConnection);
              forward=this.getForward(request, parameters, result, messageType, "ADJ");
            }
            else{
              // Set cancel reason code on request to be used for the manual ADJ - this only needs to be set for ESCALATE orders
              request.setAttribute("cancel_reason_code", request.getParameter("reason_code"));
              
              forward = MessagingConstants.ACTION_FORWARD_ADJUSTMENT;
            }
          }else if(StringUtils.isNotBlank(subsequentAction))
          {

            forward=subsequentAction;
          }else
          {

              forward=this.getForwardDestination(parameters);

          }
        }
        logger.debug("sendCancelMessage forward="+forward);
      } finally {
        if (logger.isDebugEnabled()) {
          logger.debug("Exiting sendCancelMessage");
        }
      }

      return forward;


    }





    private void updateOrderComments(HashMap parameters, HttpServletRequest request, CallOutInfo callOutInfo, String csrId) throws ParseException, Exception{

          //update Order Comments.
                          CommentsVO commentVO = new CommentsVO();
                          commentVO.setCommentType(MessagingConstants.CALL_OUT_COMMENT_TYPE);
                          String ftdMessageId=request.getParameter(MessagingConstants.REQUEST_FTD_MESSAGE_ID);
                          String externalOrderNumber=request.getParameter("msg_external_order_number");
                          String fillingFlorist=request.getParameter("filling_florist_code");
                          /* get order detail id */
                          if(parameters.get(MessagingConstants.REQUEST_ORDER_DETAIL_ID)!=null)
                          {
                              String orderDetailID = (String) parameters.get(
                                  MessagingConstants.REQUEST_ORDER_DETAIL_ID);
                              commentVO.setOrderDetailId(orderDetailID);
                          }
                          /* get comment text */
                          if(callOutInfo.getShopName()!= null && callOutInfo.getShopName().length() > 0 ){
                        	  commentVO.setComment("Order ("+externalOrderNumber+") has been cancelled  to Florist: " + callOutInfo.getSpokeTo()+", Shop Name: "+callOutInfo.getShopName() + ", Shop Phone: "+ callOutInfo.getShopPhone() +" at "+fillingFlorist);
                          } else {
							  commentVO.setComment("Order ("+externalOrderNumber+") has been cancelled  to " + callOutInfo.getSpokeTo()+" at "+fillingFlorist);
						  }

                          /* get customer ID */
                          if(request.getParameter(COMConstants.CUSTOMER_ID)!=null)
                          {
                              commentVO.setCustomerId((String) parameters.get(MessagingConstants.MESSAGE_CUSTOMER_ID));
                          }

                          /* get order guid */
                          if(request.getParameter(COMConstants.ORDER_GUID)!=null)
                          {
                              commentVO.setOrderGuid((String) parameters.get(MessagingConstants.MESSAGE_GUID));
                          }
                          commentVO.setCreatedBy(csrId);
                          commentVO.setUpdatedBy(csrId);

                          CalloutDAO calloutDAO=new CalloutDAO(this.dbConnection);
                          calloutDAO.addOrderComment(commentVO);

    }

    /**
     *
     *        Send the CAN message. return the fowarding name based the message order status.
     *        If message type equals �Mercury�, call sendMercuryMessage method, if message type equals �Venus�, call sendVenusMessage method.
     *        If order type equals �Florist�:
     *        If the current date is greater than the last day of the delivery date month:
     *        request.setAttribute(�action�, �prepare_message�);
     *        return �past_delivery_month�;
     *        Else return �communication�;
     *        If order type equals �FTDM�:
     *        If the current date is greater than the last day of the delivery date month;
     *        request.setAttribute(�action�, �prepare_message�);
     *        return �ftdm_past_delivery_month�
     *        Else return �communication�;
     *        If order type equals �Vendor�:
     *        If order is printed:
     *        request.setAttribute(�action�, �prepare_message�);
     *        return �vendor_printed�;
     *        Else return �communication�;
     * @param messageStatus
     * @param request
     * @return
     * String
     */
    public String sendCancelOriginalMessage(
            MessageOrderStatusVO messageStatus, HttpServletRequest request,
            HashMap parameters)
        throws Exception{

        String forward = MessagingConstants.ACTION_PREPARE_MESSAGE;
        ResultTO result=new ResultTO();
        String messageType = (String) parameters.get(MessagingConstants.MESSAGE_TYPE);


        if (messageStatus.isFTDM()) {
                forward = MessagingConstants.ACTION_CANCEL_FTDM;
        } else if (messageStatus.isVendor()) {
                forward = MessagingConstants.ACTION_CANCEL_VENDOR;
        }else if (messageStatus.isFlorist()) {


             /* create cancel message TO */
             CANMessageTO cancelMsg = createCANMessageTO(request, parameters);
             //set the sending florist code = default.
             //in case reps try to send out cancel original & send new from an Inbound message.
             MessageDAO messageDAO=new MessageDAO(this.dbConnection);
             cancelMsg.setSendingFlorist(messageDAO.findDefaultFloristID(messageStatus.getOrderDetailId()));
             logger.debug("ftd message id="+cancelMsg.getMercuryId());
             logger.debug("cancelMsg.setSendingFlorist="+cancelMsg.getSendingFlorist());
             result=MessagingServiceLocator.getInstance().getMercuryAPI().sendCANMessage(cancelMsg, dbConnection);
              if(!result.isSuccess())
              {
                     logger.error("Could not store CAN "+messageType+" message. ERROR: "+result.getErrorString());
                     forward=MessagingConstants.ERROR;
              }else
              {
                  //reset message status.
                  messageStatus.setCancelSent(true);
                  if (messageStatus.isPastDeliveryDateMonth()) {
                      forward = MessagingConstants.ACTION_PAST_DELIVERY_MONTH;
                  }
              }

        }



        return forward;
    }

    public String sendCOMPMessage(HttpServletRequest request, HashMap parameters)
        throws IOException, SAXException, ParserConfigurationException,
            SQLException, Exception {
        /* get lock */
        /* SAVE FILLING florist ID for mercury messages */
        /* send order detail id to stored proce used the florist table */
        /* release lock */
        if (logger.isDebugEnabled()) {
            logger.debug("Entering sendCOMPMessage");
        }
        String forward = MessagingConstants.ACTION_FORWARD_COMMUNICATION;
        ResultTO result=new ResultTO();
        String queueId=request.getParameter("queue_id");
        MessagingHelper helper=new MessagingHelper();
        try {
            String messageType =this.getFTDMessageType(request);
            String securityToken = (String) parameters.get(MessagingConstants.REQUEST_SECURITY_TOKEN);
            String csrId=helper.lookupCurrentUserId(securityToken);
            if (messageType.equalsIgnoreCase(MessagingConstants.MERCURY_MESSAGE)) {
                /* get lock */
                /* SAVE FILLING florist ID for mercury messages */
                /* send order detail id to stored proce used the florist table */
                /* release lock */
                String fillingFlorist = request.getParameter(MessagingConstants.REQUEST_FILLING_FLORIST);
                String orderDetailId = (String) parameters.get(MessagingConstants.REQUEST_ORDER_DETAIL_ID);
                String zipCityFlag = request.getParameter("zip_city_flag");

                this.updateOrder(queueId, csrId, orderDetailId, fillingFlorist, request, zipCityFlag);

                //create Mercury FTD message TO.
                FTDMessageTO ftdMsg = createFTDCOMPMessageTO(request, parameters,false);

                //create Mercury FTD message TO.

                result=MessagingServiceLocator.getInstance().getMercuryAPI().sendFTDMessage(ftdMsg, dbConnection);
                
                /* add floristSelectionLogId to the mercury table */
                String floristSelectionLogId = request.getParameter(MessagingConstants.REQUEST_FLORIST_SELECTION_LOG_ID);
                logger.info("floristSelectionLogId: " + floristSelectionLogId);
                logger.info("key: " + result.getKey());
                if(floristSelectionLogId != null && result.getKey() != null)
                {
                	MessageDAO messageDAO = new MessageDAO(dbConnection);
                	messageDAO.updateFloristSelectionLogId(result.getKey(), floristSelectionLogId);
                }
            } else {
                if (messageType.equalsIgnoreCase(
                            MessagingConstants.VENUS_MESSAGE)) {
                    //create Venus FTD message TO.
                    OrderTO ftdMsg = createVenusFTDOrderMessageTO(request, parameters);
                    result=MessagingServiceLocator.getInstance().getVenusAPI().sendOrder(ftdMsg, dbConnection);
                }
            }
             if(!result.isSuccess())
            {
                    logger.error("Could not store "+messageType+" COMP message. ERROR: "+result.getErrorString());
                    //forward=MessagingConstants.ERROR;  
                    forward=MessagingConstants.ACTION_FORWARD_COMMUNICATION;
                    parameters.put("ERROR_ON_SEND", result.getErrorString());
            }else
            {

                 this.deleteQueue(queueId, csrId);
                 forward=this.getForwardDestination(parameters);

            }
        } finally {
            if (logger.isDebugEnabled()) {
                logger.debug("Exiting sendCOMPMessage");
            }
        }

        return forward;
    }

    public String sendFTDMMessage(HttpServletRequest request, HashMap parameters, CommentsVO commentsVO, CalloutDAO calloutDAO, MessageResources resource)
        throws IOException, SAXException, ParserConfigurationException,
            SQLException, Exception {
        /* get lock */
        /* SAVE FILLING florist ID for mercury messages */
        /* send order detail id to stored proce used the florist table */
        /* release lock */
        if (logger.isDebugEnabled()) {
            logger.debug("Entering sendFTDMMessage");
        }
        String forward = MessagingConstants.ACTION_FORWARD_COMMUNICATION;
        String queueId=request.getParameter("queue_id");
        try {
            String messageType = (String) parameters.get(MessagingConstants.MESSAGE_TYPE);

            if (messageType.equalsIgnoreCase(MessagingConstants.MERCURY_MESSAGE)) {
                String ftdMessageStatus=(String)parameters.get(MessagingConstants.FTD_MESSAGE_STATUS);
                boolean ftdMessageCancelled=new Boolean((String)parameters.get(MessagingConstants.FTD_MESSAGE_CANCELLED)).booleanValue();
                boolean compOrder =new Boolean(request.getParameter(MessagingConstants.MESSAGE_COMP_ORDER)).booleanValue();
                 //get call out florist info
				 CallOutInfo callOutInfo = callOutBO.setCallOutFloristInfo(request);
                
                 Double price = getMessageTOPrice(request.getParameter("florist_prd_price"));
                 Double oldPrice = getMessageTOPrice(request.getParameter("ftd_origin_price"));
				 
                 Double additionalFee = getMessageTOPrice(request.getParameter("additional_delivery_fee"));
            	 //adding the additional delivery fee if exists 
                 if(additionalFee != null && additionalFee.SIZE > 0){
                 	logger.info("Original Price:: "+ price +", additional delivery Fee:: "+ additionalFee);
                 	price = additionalFee + price;
                 	logger.info("New Merc price:: "+ price);
                 }
  
                 String fillingFlorist = request.getParameter(MessagingConstants.REQUEST_FILLING_FLORIST);
                String orderDetailId = (String) parameters.get(MessagingConstants.REQUEST_ORDER_DETAIL_ID);
                String ftdMessageID = request.getParameter(MessagingConstants.REQUEST_FTD_MESSAGE_ID);
                String securityToken = (String) parameters.get(MessagingConstants.REQUEST_SECURITY_TOKEN);
                String csrId=new MessagingHelper().lookupCurrentUserId(securityToken);
                String zipCityFlag = request.getParameter("zip_city_flag");

                MessageDAO msgDAO = new MessageDAO(dbConnection);
                logger.debug("FTDM ftd cancelled?"+ftdMessageCancelled);
                logger.debug("FTDM compOrder?"+compOrder);
                logger.debug("FTDM ftdMessageStatus?"+ftdMessageStatus);
                logger.debug("FTDM ftdMessage ID"+ftdMessageID);

                if((ftdMessageID == null || ftdMessageID.equals("")) && ftdMessageStatus.equalsIgnoreCase("MM"))
                {
                  ftdMessageStatus = "MN";
                  logger.info("ftdMessageStatus changed to MN");
                }

                if(ftdMessageCancelled||compOrder||!ftdMessageStatus.equalsIgnoreCase("MM")){
                        logger.info("create and send FTDM");
                        this.updateOrder(queueId, csrId, orderDetailId, fillingFlorist, request, zipCityFlag);

                        //create Mercury FTD message TO.
                        FTDMessageTO ftdMsg = createFTDCOMPMessageTO(request, parameters,true);
                        //TODO: store the original price as the old price
                        ResultTO result=MessagingServiceLocator.getInstance().getMercuryAPI().sendFTDMessage(ftdMsg, dbConnection);
                        if(result==null||!result.isSuccess())
                         {
                           logger.error("Could not store mercury manual FTD message. ERROR: "+result.getErrorString());
                           forward=MessagingConstants.ERROR;
                         }else
                         {
                           //update Call out.
                           String messageId=result.getKey();
                           logger.debug("get message id from result ="+messageId);
                           msgDAO.insertCallOutLog(messageId, csrId, callOutInfo);
                           this.deleteQueue(queueId, csrId);

                           // update comments
                           insertOrderComments(messageId, commentsVO, price, calloutDAO, oldPrice, resource, msgDAO);

                         }
                }else
                {
                  logger.info("no FTDM created. insert callout log only "+ftdMessageID);
                  //update price as needed.
                  if(oldPrice.compareTo(price)!=0)
                  {
                    msgDAO.updateMercuryFTDPrice(ftdMessageID, price);
                  }


                  msgDAO.insertCallOutLog(ftdMessageID, csrId, callOutInfo);
                  // update comments
                  insertOrderComments(ftdMessageID, commentsVO, price, calloutDAO, oldPrice, resource, msgDAO);

                }



                forward=this.getForwardDestination(parameters);

            }
        } finally {
            if (logger.isDebugEnabled()) {
                logger.debug("Exiting sendFTDMMessage");
            }
        }

        return forward;
    }

  void insertOrderComments(String ftdMsgId, CommentsVO commentsVO, Double price, CalloutDAO calloutDAO, Double oldPrice, MessageResources resource, MessageDAO msgDAO) throws Exception, ParserConfigurationException, SQLException, SAXException, IOException
  {
    String comments=commentsVO.getComment();
    String mercuryOrderNumber= getMercuryOrderNumber(ftdMsgId, msgDAO);
     comments +=" "+mercuryOrderNumber;
    if (oldPrice.compareTo(price) != 0)
    {
      comments +=" "+resource.getMessage("call_out_price_change", new String[] {oldPrice.toString(), price.toString()});
    }
    commentsVO.setComment(comments);
    calloutDAO.addOrderComment(commentsVO);
  }



     public String getMercuryOrderNumber(String ftdMessageId, MessageDAO mDAO) throws Exception
    {


      MessageVO mVO=mDAO.getMessage(ftdMessageId, "Mercury");

      if(mVO!=null)
      {
        return mVO.getMessageOrderId();
      }else
      {
        return "";
      }
    }


    private String getFTDMessageType(HttpServletRequest request)
    {

       String messageType=request.getParameter(MessagingConstants.NEW_MESSAGE_TYPE);


        logger.debug("newMessageType="+messageType);
        if(StringUtils.isBlank(messageType))
        {
          messageType=request.getParameter(MessagingConstants.MESSAGE_TYPE);
        }
        logger.debug("messageType="+messageType);
        return messageType;

    }


    /**
     * � Overloaded method
     * � Return �communication�.
     * @param request - HttpServletRequest
     * @return String
     * @throws Exception????
     * @todo - code and determine exceptions
     */
     public String sendFTDMessage(HttpServletRequest request, HashMap parameters)
       throws IOException, SAXException, ParserConfigurationException,
             SQLException, Exception
     {
        String forward=MessagingConstants.ACTION_FORWARD_COMMUNICATION;
        forward = sendFTDMessage(request, parameters, null);
        return forward;
     }


  /**
   * � Call createMessageVO to retrieve message related data, such as type,
   * message detail type, messageOrderNumber and orderDetailId from HTTP request.
   * � If message type equals �Mercury�, call sendMercuryMessage method,
   * if message type equals �Venus�, call sendVenusMessage method.
   * � Return �communication�.
   * @param request - HttpServletRequest
   * @return String
   * @throws Exception????
   * @todo - code and determine exceptions
   */
    public String sendFTDMessage(HttpServletRequest request, HashMap parameters, ActionMapping mapping)
        throws IOException, SAXException, ParserConfigurationException,
            SQLException, Exception {
        if (logger.isDebugEnabled()) {
            logger.debug("Entering sendFTDMessage");
        }
        String forward = MessagingConstants.ACTION_FORWARD_COMMUNICATION;
        ResultTO result=new ResultTO();
        String queueId=request.getParameter("queue_id");
        MessagingHelper helper=new MessagingHelper();
        try {
            String messageType =this.getFTDMessageType(request);
            logger.debug("FTD order messsage type="+messageType);
            String securityToken = (String) parameters.get(MessagingConstants.REQUEST_SECURITY_TOKEN);
            String csrId=helper.lookupCurrentUserId(securityToken);
            if (messageType.equalsIgnoreCase(MessagingConstants.MERCURY_MESSAGE)) 
            {
              /* get lock */
              /* SAVE FILLING florist ID for mercury messages */
              /* send order detail id to stored proce used the florist table */
              /* release lock */
              String fillingFlorist = request.getParameter(MessagingConstants.REQUEST_FILLING_FLORIST);
              String orderDetailId = (String) parameters.get(MessagingConstants.REQUEST_ORDER_DETAIL_ID);
              String zipCityFlag = request.getParameter("zip_city_flag");
              this.updateOrder(queueId, csrId, orderDetailId, fillingFlorist, request, zipCityFlag);

              String action = null; 
              if (mapping != null)
              {
                action=mapping.getParameter();
              }
              if (StringUtils.isNotBlank(action) && StringUtils.equalsIgnoreCase(action, "update_florist_iframe"))
              {
                //create Mercury FTD message TO.
                com.ftd.op.mercury.to.OrderDetailKeyTO orderDetail = new com.ftd.op.mercury.to.OrderDetailKeyTO();
                orderDetail.setOrderDetailId((String) parameters.get(MessagingConstants.REQUEST_ORDER_DETAIL_ID));
                result=MessagingServiceLocator.getInstance().getMercuryAPI().sendFTDMessage(orderDetail, dbConnection);
                zipCityFlag = request.getParameter("zip_city_flag");
                /* add floristSelectionLogId to the mercury table */
                String floristSelectionLogId = request.getParameter(MessagingConstants.REQUEST_FLORIST_SELECTION_LOG_ID);
                logger.info("floristSelectionLOgId: " + floristSelectionLogId);
                logger.info("key: " + result.getKey());
                if(floristSelectionLogId != null && result.getKey() != null)
                {
                	MessageDAO messageDAO = new MessageDAO(dbConnection);
                	messageDAO.updateFloristSelectionLogId(result.getKey(), floristSelectionLogId);
                }
              }
              else
              {
            
            	  //create Mercury FTD message TO.
                FTDMessageTO ftdMsg = createFTDCOMPMessageTO(request, parameters,false);
                zipCityFlag = request.getParameter("zip_city_flag");
                //create Mercury FTD message TO.
                result=MessagingServiceLocator.getInstance().getMercuryAPI().sendFTDMessage(ftdMsg, dbConnection);
             
                /* add floristSelectionLogId to the mercury table */
                String floristSelectionLogId = request.getParameter(MessagingConstants.REQUEST_FLORIST_SELECTION_LOG_ID);
                logger.info("floristSelectionLOgId: " + floristSelectionLogId);
                logger.info("key: " + result.getKey());
                if(floristSelectionLogId != null && result.getKey() != null)
                {
                	MessageDAO messageDAO = new MessageDAO(dbConnection);
                	messageDAO.updateFloristSelectionLogId(result.getKey(), floristSelectionLogId);
                }
              }
            } 
            else 
            {
                if (messageType.equalsIgnoreCase(
                            MessagingConstants.VENUS_MESSAGE)) {
                    //create Venus FTD message TO.
                    OrderTO ftdMsg = createVenusFTDOrderMessageTO(request, parameters);
                    result=MessagingServiceLocator.getInstance().getVenusAPI().sendOrder(ftdMsg, dbConnection);
                }
            }
             if(!result.isSuccess())
            {
                    logger.error("Could not store "+messageType+" FTD message. ERROR: "+result.getErrorString());
                    //forward=MessagingConstants.ERROR;  
                    forward=MessagingConstants.ACTION_FORWARD_COMMUNICATION;
                    parameters.put("ERROR_ON_SEND", result.getErrorString());
            }else
            {

                 this.deleteQueue(queueId, csrId);
                 forward=this.getForwardDestination(parameters);

            }
        } finally {
            if (logger.isDebugEnabled()) {
                logger.debug("Exiting sendFTDMessage");
            }
        }

        return forward;
    }



    private void deleteQueue(String queueId, String csrId) throws Exception
    {

                        if(StringUtils.isNotBlank(queueId))
                        {

                           QueueDAO queueDAO=new QueueDAO(this.dbConnection);
                           queueDAO.deleteQueue(queueId, csrId);

                        }

    }



     private void updateOrder(String queueId, String csrId, String orderDetailId, String fillingFlorist, HttpServletRequest request, String selectionData) throws Exception
    {
                //if new_florist_id is passed, the order is already be updated.
                if(StringUtils.isBlank(request.getParameter("new_florist_id"))){
                    String orderDispostion=null;
                    if(StringUtils.isNotBlank(queueId))
                    {
                      orderDispostion="Processed";

                    }
                    Date deliveryDate = getDeliveryDate(request);
                    Date deliveryDateEnd = getDeliveryDateEnd(request);  
                    Date ordDeliveryDate = FTDCommonUtils.getOrderDeliveryDate(dbConnection, orderDetailId);
                                        
                    if (deliveryDate != null) {
                        if (ordDeliveryDate != null) {
                            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                            String deliveryDateStr = df.format(deliveryDate);
                            String ordDeliveryDateStr = df.format(ordDeliveryDate);

                            if (!deliveryDateStr.equals(ordDeliveryDateStr)) {
                                // Add comment since delivery date has changed
                                CommentDAO commentDAO = new CommentDAO(this.dbConnection);
                                CommentsVO commentsVO = new CommentsVO();
                                String cmntStr = "Delivery date was changed from " + ordDeliveryDateStr + " to " + deliveryDateStr +
                                                 ". To view the original date requested by the customer, see original view tab.";
                                commentsVO.setComment(cmntStr);
                                commentsVO.setCommentOrigin(request.getParameter(COMConstants.START_ORIGIN));
                                commentsVO.setCommentType("Order");
                                commentsVO.setCreatedBy(csrId);
                                commentsVO.setUpdatedBy(csrId);
                                commentsVO.setOrderDetailId(orderDetailId);
                                commentDAO.insertComment(commentsVO); 
                            }
                        }
                    }

                    MessageDAO msgDAO = new MessageDAO(dbConnection);
                    msgDAO.modifyOrder(orderDetailId, fillingFlorist.toUpperCase(),
                        csrId, orderDispostion, deliveryDate, deliveryDateEnd);
                    logger.info("selectionData: " + selectionData);
                    msgDAO.orderFloristUsed(orderDetailId, fillingFlorist,selectionData);
                }

    }


    /**
     * �        Call createMessageVO to retrieve message related data, such as type, message detail type, messageOrderNumber and orderDetailId from HTTP request. If message detail type equals �ADJM�, set message status to �MN� (not to be sent).
     * �        If message type equals �Mercury�, call sendMercuryMessage method, if message type equals �Venus�, call sendVenusMessage method.
     * �        If(request.getParameter(�subsequent_action�)!=null){
     * return request.getParameter(�subsequent_action�);
     * }else{
     * return �communication�;
     * }
     * @param request - HttpServletRequest
     * @return String
     * @throws Exception????
     * @todo - code and determine exceptions
     */
  public String sendAdjustmentMessage(HttpServletRequest request, HashMap parameters) throws RemoteException, Exception
  {
        ResultTO result =new ResultTO();
        String messageType = (String) parameters.get(MessagingConstants.MESSAGE_TYPE);

        String forward = request.getParameter(MessagingConstants.SUBSEQUENT_ACTION);
        if (messageType.equals(MessagingConstants.MERCURY_MESSAGE)) {
            ADJMessageTO adjMsg = createADJMessageTO(request, parameters);
          result=MessagingServiceLocator.getInstance().getMercuryAPI().sendADJMessage(adjMsg, dbConnection);

        }  else if (messageType.equals(MessagingConstants.VENUS_MESSAGE)) {
           AdjustmentMessageTO adjTO = createVenusAdjustmentMessageTO(request, parameters);
        result = MessagingServiceLocator.getInstance().getVenusAPI().sendAdjustment(adjTO, dbConnection);

        }


        forward=this.getForward(request, parameters, result, messageType, "ADJ");
      logger.debug("adjustment forward= "+forward);
     return forward;
  }




  private String getForward(HttpServletRequest request, HashMap parameters, ResultTO result, String messageType, String messageDetailType)
  {
      String forward=request.getParameter(MessagingConstants.SUBSEQUENT_ACTION);
      if(!result.isSuccess())
            {
                logger.error("Could not store "+messageDetailType+"\t"+ messageType+" message. ERROR: "+result.getErrorString());
                forward=MessagingConstants.ERROR;
            }else
            {
                if (StringUtils.isBlank(forward))
                {
                      String destination=this.getForwardDestination(parameters);
                      if(StringUtils.isNotBlank(destination))
                      {
                        forward=destination;
                      }else{
                        forward= MessagingConstants.ACTION_FORWARD_COMMUNICATION;
                      }
                }
            }

     return forward;
  }



  /**
   * get forward destination from Date HashMap, if the value is null, return "Communication" as default.
   * @param parameters
   * @return
   */
  public String getForwardDestination( HashMap parameters)
  {
      String destination=    (String)parameters.get(MessagingConstants.FORWARD_DESTINATION);
      if(StringUtils.isNotBlank(destination))
      {
        return destination;
      }else
      {
        return MessagingConstants.ACTION_FORWARD_COMMUNICATION;
      }

  }


 /*******************************************************************************************
 * computeShipDate()
 *******************************************************************************************
  * Calculate the ship date
  *
  */
  private String computeShipDate(String shipMethod, String requestedDeliveryDate, String recipState,
                               String recipCountry, String productId) throws Exception
  {
    DeliveryDateUTIL ddu = new DeliveryDateUTIL();

    OEDeliveryDateParm oeParms = new OEDeliveryDateParm();

    getProductInfo(productId);

    oeParms.setProductType(productType);

    String newShipDate =  ddu.getShipDate(oeParms, shipMethod,
                          requestedDeliveryDate, recipState,
                          recipCountry, productId, this.dbConnection);

    return newShipDate;
  }

 /**
	* Obtain the product type and vendor id
	*/
 private void getProductInfo(String productId) throws Exception
  {
    MessageDAO messageDAO = new MessageDAO(this.dbConnection);

    CachedResultSet crs =  messageDAO.getProductInfo(productId);
    String productType = null;
    if(crs.next())
    {
      this.productType = (String) crs.getString("productType");
    }
  }




  public void setDbConnection(Connection dbConnection)
  {
    this.dbConnection = dbConnection;
  }



  /**
   * This method will update the Order Details table with the correct dates/methods for a vendor
   * order.
   * On the "New FTD Message" screen, CSR now has the ability update the delivery date and select
   * a new shipping method.  Since the Venus API is set such that it will read from the Order Details
   * table, originally Mercury and now Venus orders, will update the Order Details tables.
   *
   * Update delivery date, delivery date end, ship date and ship method on the order details.
   *
   * @param request
   * @throws java.lang.Exception
   */
    private void updateVendorOrder(HttpServletRequest request)throws Exception
    {

      MessagingHelper helper=new MessagingHelper();
      MessageDAO msgDao=new MessageDAO(this.dbConnection);

      //Date format
      SimpleDateFormat inputFormat = new SimpleDateFormat("MM/dd/yyyy");

      //retrieve parms from the request
      String orderDetailID    = request.getParameter(MessagingConstants.REQUEST_ORDER_DETAIL_ID);
      String productId        = request.getParameter("product_id");
      String recipientCountry = request.getParameter("recipient_country");
      String recipientState   = request.getParameter("recipient_state");
      String sDeliveryDate 		= request.getParameter("delivery_date");
      String securityToken    = request.getParameter(MessagingConstants.REQUEST_SECURITY_TOKEN);
      String sShipMethod      = request.getParameter("ship_method");
      String csrId            = helper.lookupCurrentUserId(securityToken);

      java.util.Date dDeliveryDate  = null;
      java.util.Date dShipDate  = null;

      //if the delivery date was not null, change the String to Date format
      if (StringUtils.isNotEmpty(sDeliveryDate))
      {
        dDeliveryDate = inputFormat.parse(sDeliveryDate);
      }

      //retrieve the ship date based on the delivery date, product id and recipient info
      String sShipDate = computeShipDate(sShipMethod, sDeliveryDate, recipientState, recipientCountry, productId);

      //if the ship date was not null, change the String to Date format
      if (StringUtils.isNotEmpty(sShipDate))
      {
        dShipDate = inputFormat.parse(sShipDate);
      }

      //update order detail.
      msgDao.updateOrder(orderDetailID, csrId, dDeliveryDate, null, dShipDate, sShipMethod );

    }




}
