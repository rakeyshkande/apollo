package com.ftd.messaging.bo;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.dao.OrderDAO;
import com.ftd.ftdutilities.FTDCommonUtils;
import com.ftd.legacy.dao.LegacySourceCodeDAO;
import com.ftd.messaging.constants.MessagingConstants;
import com.ftd.messaging.dao.MessageDAO;
import com.ftd.messaging.util.MessagingDOMUtil;
import com.ftd.messaging.util.MessagingHelper;
import com.ftd.messaging.util.MessagingServiceLocator;
import com.ftd.messaging.vo.FloristStatusVO;
import com.ftd.op.order.to.OrderTO;
import com.ftd.op.order.to.RWDFloristTO;
import com.ftd.op.order.to.RWDFloristTOList;
import com.ftd.op.order.to.RWDTO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.handlers.GlobalParmHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.SourceMasterHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.PartnerVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.SourceMasterVO;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;


/**
 * This class contains business logic to handle auto florist selection,
 * florist lookup and florist status validation.
 * @author Charles Fox
 * @todo check comments, error handling, and message handling
 * @todo confirm stored procedure setup against actual stored procedures
 */
public class FloristSelectBO {
    private Connection conn;
    private SimpleDateFormat sdf = new SimpleDateFormat(
            "yyyy-MM-dd hh:mm:ss a zzz");
    private Logger logger = new Logger("com.ftd.messaging.bo.FloristSelectBO");

    public FloristSelectBO() {
    }


    public RWDFloristTO autoSelectFloristByOrderDetailId(HttpServletRequest request) throws Exception{
          OrderTO orderTO = new OrderTO();
          //Call florist selection API to get the list of florists
          orderTO.setOrderDetailId(request.getParameter(MessagingConstants.REQUEST_ORDER_DETAIL_ID));
          orderTO.setRWDFlagOverride(Boolean.TRUE);
          logger.debug("order detail id=" + request.getParameter(MessagingConstants.REQUEST_ORDER_DETAIL_ID));

          return MessagingServiceLocator.getInstance().getOrderAPI().getFillingFlorist(orderTO, conn);
    }


    /**
     *   Retrieve zip code from HTTP request.
     *   Auto select florist by using the Random Weighted Distribution &
     *  Preferred Florist Logic for the zip code in Order Processing.
     *   If no florist is available, return null, otherwise, return the
     *  florist code selected.
     * @param request - HttpServletRequest
     * @return String
     * @throws Exception????
     * @todo - code and determine exceptions
     */
    public RWDFloristTO autoSelectFlorist(HttpServletRequest request, HashMap parameters) throws Exception{

    	RWDTO rwdTO=new RWDTO();
        RWDFloristTO floristTO = new RWDFloristTO();
        MessagingHelper msgHelper=new MessagingHelper();
        try {
            boolean compOrder =new Boolean(request.getParameter(MessagingConstants.MESSAGE_COMP_ORDER)).booleanValue();
            String orderDetailId=(String) parameters.get(
                    MessagingConstants.REQUEST_ORDER_DETAIL_ID);
            String city=request.getParameter("recipient_city");
            String state=request.getParameter("recipient_state");
            String zipCode =request.getParameter("recipient_zip");
            String price=request.getParameter("florist_prd_price");
            String regex = "(?<=\\d),(?=\\d)";
            price = price.replaceAll(regex, "");
            String product=request.getParameter("product_id");
            String sourceCode=request.getParameter("source_code");

            String monthStr = request.getParameter("delivery_month");
            String dateStr=request.getParameter("delivery_date");
            String monthStrEnd = request.getParameter("delivery_month_end");
            String dateStrEnd=request.getParameter("delivery_date_end");

            Date deliveryDate = null;
            Date deliveryDateEnd=null;
            if(StringUtils.isNotBlank(monthStr) && StringUtils.isNotBlank(dateStr))
            {
              deliveryDate =msgHelper.createDeliveryDate(monthStr, dateStr);
            }
            if(StringUtils.isNotBlank(monthStrEnd) && StringUtils.isNotBlank(dateStrEnd))
            {
              deliveryDateEnd = msgHelper.createDeliveryDate(monthStrEnd, dateStrEnd);
            }

            rwdTO.setOrderDetailId(orderDetailId);
            rwdTO.setDeliveryCity(city);
            rwdTO.setDeliveryState(state);

            rwdTO.setZipCode(zipCode.toUpperCase());

            rwdTO.setReturnAll(false);
            rwdTO.setOrderValue(Double.parseDouble(price));
            rwdTO.setProduct(product);
            rwdTO.setDeliveryDate(deliveryDate);
            rwdTO.setDeliveryDateEnd(deliveryDateEnd);
            rwdTO.setSourceCode(sourceCode);
            rwdTO.setRWDFlagOverride(Boolean.TRUE);
            floristTO = MessagingServiceLocator.getInstance().getOrderAPI().getFillingFlorist(rwdTO, conn);
            } finally {
                if (logger.isDebugEnabled()) {
                    logger.debug("Exiting autoSelectFlorist");
                }
            }
            return floristTO;
    }




  /**
   * The temporary solution to substitute Modify Order. Will be removed after modify order goes live.
   * Update delivery date on order details. Insert the original delivery date to a temp table.
   * @param request
   * @throws java.lang.Exception
   */
    private void updateOrderDeliveryDate(HttpServletRequest request)throws Exception
    {

            MessagingHelper helper=new MessagingHelper();
            MessageDAO msgDao=new MessageDAO(this.conn);
            String monthStr = request.getParameter("delivery_month");
            String dateStr=request.getParameter("delivery_date");
            String monthStrEnd = request.getParameter("delivery_month_end");
            String dateStrEnd=request.getParameter("delivery_date_end");
            String orderDetailID = request.getParameter(MessagingConstants.REQUEST_ORDER_DETAIL_ID);
            String securityToken = request.getParameter(MessagingConstants.REQUEST_SECURITY_TOKEN);
            String csrId=helper.lookupCurrentUserId(securityToken);
            Date deliveryDate = helper.createDeliveryDate(monthStr, dateStr);;
            Date deliveryDateEnd=null;
            if(StringUtils.isNotBlank(monthStrEnd)&&StringUtils.isNotBlank(dateStrEnd))
            {
              deliveryDateEnd = helper.createDeliveryDate(monthStrEnd, dateStrEnd);

            }
            //update order detail.
            //msgDao.insertOrderDeliveryDateToTemp(orderDetailID);
            msgDao.updateOrderDeliveryDate(orderDetailID, csrId, deliveryDate, deliveryDateEnd);




    }


    /**
     *  Retrieve filling florist code from HTTP request.
     *  Get florist status on the load member data load
     *  Return florist status. If the florist is not valid, return null.
     * @param request - HttpServletRequest
     * @return String
     * @throws Exception????
     * @todo - code and determine exceptions
     */
    public String findFloristStatusAndSendFTD(HttpServletRequest request, ActionMapping mapping)
        throws SAXException, ParserConfigurationException, IOException,
            SQLException, Exception {

        String destination=request.getParameter(MessagingConstants.FORWARD_DESTINATION);
        String compOrder=request.getParameter(MessagingConstants.REQUEST_PARAMETER_COMP_ORDER);
        boolean isComp=new Boolean(compOrder).booleanValue();
        if (logger.isDebugEnabled()) {
            logger.debug("comp order="+compOrder+" isComp="+isComp+" destination="+destination);
        }

        return this.checkFloristStatus(request, isComp, destination, mapping);

    }



    public Document doAutoSelect(HttpServletRequest request, ActionMapping mapping, MessageResources resources, HashMap parameters)throws SAXException, ParserConfigurationException, IOException,
            SQLException, Exception
            {
                RWDFloristTO floristTO = null;

                boolean callout=new Boolean(request.getParameter("call_out")).booleanValue();
                logger.debug("call out="+callout);
                Document doc=MessagingDOMUtil.createEmptyDocument();

                //retrieve the florist from the database to make sure it is a valid florist.

                boolean lookupByOrder = new Boolean(request.getParameter("lookup_by_order")).booleanValue();
                String sourceCode = request.getParameter("source_code");
                logger.debug("lookupByOrder=" + lookupByOrder + " and source code = " + sourceCode);

                SourceMasterHandler handler = (SourceMasterHandler) CacheManager.getInstance().getHandler("CACHE_NAME_SOURCE_MASTER");
                SourceMasterVO sVO = handler.getSourceCodeById(sourceCode);

                //check if the order detail record has an avs address id associated with it
                //if it does return it
            	String orderDetailId = request.getParameter(MessagingConstants.REQUEST_ORDER_DETAIL_ID);
                String avsAddressId = orderHasAVSId(orderDetailId);

                //only if the order has an avs id associated to it
                //check to see if the recipient address is different from the address on the order
                //retrieve recipient address
                if(avsAddressId != null)
                {
                	boolean recipAddressDifferent = this.isRecipientAddressDifferent(request, orderDetailId);

                	if(recipAddressDifferent)
	                {
	                	//update apollo override flag to 'Y'
	                	MessageDAO messageDAO = new MessageDAO(conn);
	                	messageDAO.updateApolloOverrideFlag(avsAddressId, "Y");
	                }
	                else
	                {
	                	//update apollo override flag to 'N'
	                	MessageDAO messageDAO = new MessageDAO(conn);
	                	messageDAO.updateApolloOverrideFlag(avsAddressId, "N");
	                }
                }
                if(lookupByOrder)
                {
                    floristTO = this.autoSelectFloristByOrderDetailId(request);
                    logger.info("floristSelectionLogId: " + floristTO.getFloristSelectionLogId());

                }else{
                	floristTO = this.autoSelectFlorist(request, parameters);
                    logger.info("floristSelectionLogId: " + floristTO.getFloristSelectionLogId());

                }


              //  RWDFloristTOList floristListTO=this.lookupFillingFloristList(request, parameters, false);

                // If there are no more florists available for auto selection,
                //the user will get the following message next to the textbox in red font There are no more florists available for auto selection.
                if(!floristExist(floristTO)){

                  logger.debug("florist does not exist");
                  String errorMsg=null;
                  if (sVO != null && sVO.getPrimaryFlorist() != null & sVO.getPrimaryFlorist() != "")
                    errorMsg = resources.getMessage("florist_not_available_error_prim_bkup");
                  else
                    errorMsg = resources.getMessage("florist_not_available_error");

                  MessagingDOMUtil.addElementToXML(MessagingConstants.ERROR_MESSAGE, errorMsg, doc);
                }else{
                   //add the auto selected florist code
                   MessagingDOMUtil.addElementToXML("filling_florist_code", floristTO.getFloristId(), doc);
                   //add the florist selection log id
                   MessagingDOMUtil.addElementToXML(MessagingConstants.REQUEST_FLORIST_SELECTION_LOG_ID, floristTO.getFloristSelectionLogId(), doc);
                   //QE-97 add the Zip City Flag
                   logger.info("floristTO.getZipCityFlag(): " + floristTO.getZipCityFlag());
                   MessagingDOMUtil.addElementToXML("zip_city_flag", floristTO.getZipCityFlag(), doc);
                   //if the florist exist, but it is suspended or no mercury machine.
                   if(callout||(floristTO.getMercuryFlag() == null)||!floristTO.getMercuryFlag().equalsIgnoreCase("M")||floristTO.getStatus().indexOf("Suspended")!=-1){
                	   MessagingDOMUtil.addElementToXML(MessagingConstants.FORWARDING_ACTION, mapping.findForward(MessagingConstants.ACTION_FORWARD_CALLOUT).getPath(), doc);
                   }else
                    {
                	   MessagingDOMUtil.addElementToXML(MessagingConstants.FORWARDING_ACTION, mapping.findForward("send_ftd").getPath(), doc);

                      /* Update Florist (auto select) uses the filling_florist_code and an action type.
                       * Could have used forwarding action to hold the action type but that may have been
                       * confusing from a maintenance perspective.
                       */
                      MessagingDOMUtil.addElementToXML(MessagingConstants.ACTION_TYPE, "send_ftd", doc);
                    }
                }

                return doc;

            }


     private boolean floristExist(RWDFloristTO floristTO)
        {

          return (floristTO!=null&&floristTO.getFloristId()!=null&&floristTO.getFloristId().trim().length()>0);
        }

    /**
     *   Retrieve zip code from HTTP request.
     *   Get a list of florists that serve the zip code.
     *   Create a Document object that contains the list of florists.
     *   Return the Document.
     * @param request - HttpServletRequest
     * @return Document
     * @throws Exception????
     * @todo - code and determine exceptions
     */
    public Document lookupFloristList(HttpServletRequest request,
        HashMap parameters)
        throws java.rmi.RemoteException, IOException, SAXException,
            ParserConfigurationException, Exception {
        if (logger.isDebugEnabled()) {
            logger.debug("Entering lookupFloristList");
        }
         Document xmlList =null;
         boolean lookupByOrder=new Boolean(request.getParameter("lookup_by_order")).booleanValue();
         if(lookupByOrder)
         {
            //update order delivery date.
            //this.updateOrderDeliveryDate(request);
            xmlList=lookupFloristListByOrderDetailId(parameters, request);

         }else
         {
            xmlList=this.lookupFillingFloristList(request, parameters);
         }


        //return the Document.
        return xmlList;
    }

  Document lookupFloristListByOrderDetailId(HashMap parameters, HttpServletRequest request) throws IOException, java.rmi.RemoteException, Exception, ParserConfigurationException, SAXException
  {
    Document xmlList = null;
    String zipCode = request.getParameter("zip_code");
    String modifyOrderFlag = request.getParameter("modify_order_flag");
    logger.debug("modifyOrderFlag=" + modifyOrderFlag);
    try
    {
      OrderTO orderTO = new OrderTO();
      String orderDetailId = (String)parameters.get(MessagingConstants.REQUEST_ORDER_DETAIL_ID);
      logger.debug("lookup florist for order detail id [" + orderDetailId + "]");
      orderTO.setOrderDetailId(orderDetailId);
      orderTO.setRWDFlagOverride(Boolean.TRUE);
      RWDFloristTOList floristTO = MessagingServiceLocator.getInstance().getOrderAPI().getFullFloristList(orderTO, conn);
      if (floristTO == null)
      {
        logger.debug("floristTO==null");
      }
      String listXML = toXML(floristTO);
      xmlList = DOMUtil.getDocument(listXML);
      if (zipCode != null)
      {
        MessagingDOMUtil.addElementToXML("zip_code", zipCode, xmlList);
      }
      MessagingDOMUtil.addElementToXML("modify_order_flag", StringUtils.isNotBlank(modifyOrderFlag) ? "Y" : "N", xmlList);
    }
    finally
    {
      if (logger.isDebugEnabled())
      {
        logger.debug("Exiting lookupFloristList");
      }
    }

    return xmlList;
  }





    /**
     *   Retrieve zip code from HTTP request.
     *   Get a list of florists that serve the zip code.
     *   Create a Document object that contains the list of florists.
     *   Return the Document.
     * @param request - HttpServletRequest
     * @return Document
     * @throws Exception????
     * @todo - code and determine exceptions
     */
    public Document lookupFillingFloristList(HttpServletRequest request,
        HashMap parameters)
        throws java.rmi.RemoteException, IOException, SAXException,
            ParserConfigurationException, Exception {
        if (logger.isDebugEnabled()) {
            logger.debug("Entering lookupFillingFloristList");
        }
        Document xmlList = null;
        String legacyId = null;
        SimpleDateFormat sdfOutput = new SimpleDateFormat("EEE MM/dd/yyyy");//Sat 10/22/2005
        String modifyOrderFlag=request.getParameter("modify_order_flag");
        RWDTO rwdTO=new RWDTO();
        MessagingHelper msgHelper=new MessagingHelper();
        OrderDAO orderDAO = new OrderDAO(this.conn);
        LegacySourceCodeDAO legacyDAO = new LegacySourceCodeDAO(this.conn);
        logger.debug("modifyOrderFlag="+modifyOrderFlag);
        try {
            boolean compOrder =new Boolean(request.getParameter(MessagingConstants.MESSAGE_COMP_ORDER)).booleanValue();
            String orderDetailId=(String) parameters.get(
                    MessagingConstants.REQUEST_ORDER_DETAIL_ID);
            String city=request.getParameter("recipient_city");
            String state=request.getParameter("recipient_state");
            String zipCode =request.getParameter("recipient_zip");
            String price=request.getParameter("florist_prd_price");
            String regex = "(?<=\\d),(?=\\d)";
            price = price.replaceAll(regex, "");
            String product=request.getParameter("product_id");
            String sourceCode=request.getParameter("source_code");

            String monthStr = request.getParameter("delivery_month");
            String dateStr=request.getParameter("delivery_date");
            String monthStrEnd = request.getParameter("delivery_month_end");
            String dateStrEnd=request.getParameter("delivery_date_end");

            Date deliveryDate = null;
            Date deliveryDateEnd=null;
            if(StringUtils.isNotBlank(monthStr) && StringUtils.isNotBlank(dateStr))
            {
              deliveryDate =msgHelper.createDeliveryDate(monthStr, dateStr);
            }
            if(StringUtils.isNotBlank(monthStrEnd) && StringUtils.isNotBlank(dateStrEnd))
            {
              deliveryDateEnd = msgHelper.createDeliveryDate(monthStrEnd, dateStrEnd);
            }

            rwdTO.setOrderDetailId(orderDetailId);
            rwdTO.setDeliveryCity(city);
            rwdTO.setDeliveryState(state);

            rwdTO.setZipCode(zipCode.toUpperCase());

            rwdTO.setReturnAll(true);
            rwdTO.setOrderValue(Double.parseDouble(price));
            rwdTO.setProduct(product);
            rwdTO.setDeliveryDate(deliveryDate);
            rwdTO.setDeliveryDateEnd(deliveryDateEnd);

            ConfigurationUtil configUtil = new ConfigurationUtil();

			String lpsfCheck = configUtil.getFrpGlobalParm("PI_CONFIG","PRMY_BCKP_FLORIST_LEGACY_ID_LOOK_UP");

            legacyId = getLegacyID(orderDetailId);
            logger.info("LegacyID received : "+legacyId);
            if(lpsfCheck.equals("Y") && legacyId != null){
            	sourceCode = legacyDAO.getSourceCode(legacyId);
            	logger.info("Source code for Legacy Id set to : "+sourceCode);
            }
            rwdTO.setSourceCode(sourceCode);

            rwdTO.setRWDFlagOverride(Boolean.TRUE);
            RWDFloristTOList floristTO = MessagingServiceLocator.getInstance().getOrderAPI().getFillingFloristList(rwdTO, conn);
            if(floristTO==null)
            {
              logger.debug("floristTO==null");
            }
            //create a Document
            String listXML = toXML(floristTO);
            xmlList = DOMUtil.getDocument(listXML);
            if(zipCode!=null)
            {
              MessagingDOMUtil.addElementToXML("zip_code", zipCode, xmlList);
            }
            MessagingDOMUtil.addElementToXML("modify_order_flag", StringUtils.isNotBlank(modifyOrderFlag)?"Y":"N", xmlList);
        } finally {
            if (logger.isDebugEnabled()) {
                logger.debug("Exiting lookupFloristList");
            }
        }

        //return the Document.
        return xmlList;
    }


	/**
     *  Used for Auto Select Florist
     *  Retrieve zip code from HTTP request.
     *  Get a next available florist for the zip code sent in the request.
     *
     * @param request - HttpServletRequest
     * @param parameters - HashMap
     * @param returnAll - boolean
     * @return RWDFloristTOList
     * @throws java.rmi.RemoteException, IOException, SAXException,
            ParserConfigurationException, Exception
     */
    public RWDFloristTOList lookupFillingFloristList(HttpServletRequest request,
        HashMap parameters, boolean returnAll)
        throws java.rmi.RemoteException, IOException, SAXException,
            ParserConfigurationException, Exception {
        if (logger.isDebugEnabled()) {
            logger.debug("Entering lookupFillingFloristList(HttpServletRequest request,\n" +
            "        HashMap parameters, boolean returnAll)");
        }
        RWDFloristTOList floristTO = new RWDFloristTOList();
        SimpleDateFormat sdfOutput = new SimpleDateFormat("EEE MM/dd/yyyy");//Sat 10/22/2005
        String modifyOrderFlag=request.getParameter("modify_order_flag");
        RWDTO rwdTO=new RWDTO();
        MessagingHelper msgHelper=new MessagingHelper();
        logger.debug("modifyOrderFlag="+modifyOrderFlag);
        try {
            boolean compOrder =new Boolean(request.getParameter(MessagingConstants.MESSAGE_COMP_ORDER)).booleanValue();
            String orderDetailId=(String) parameters.get(
                    MessagingConstants.REQUEST_ORDER_DETAIL_ID);
            String city=request.getParameter("recipient_city");
            String state=request.getParameter("recipient_state");
            String zipCode =request.getParameter("recipient_zip");
            String price=request.getParameter("florist_prd_price");
            String regex = "(?<=\\d),(?=\\d)";
            price = price.replaceAll(regex, "");
            String product=request.getParameter("product_id");
            String sourceCode=request.getParameter("source_code");

            String monthStr = request.getParameter("delivery_month");
            String dateStr=request.getParameter("delivery_date");
            String monthStrEnd = request.getParameter("delivery_month_end");
            String dateStrEnd=request.getParameter("delivery_date_end");

            Date deliveryDate = null;
            Date deliveryDateEnd=null;
            if(StringUtils.isNotBlank(monthStr) && StringUtils.isNotBlank(dateStr))
            {
              deliveryDate =msgHelper.createDeliveryDate(monthStr, dateStr);
            }
            if(StringUtils.isNotBlank(monthStrEnd) && StringUtils.isNotBlank(dateStrEnd))
            {
              deliveryDateEnd = msgHelper.createDeliveryDate(monthStrEnd, dateStrEnd);
            }

            rwdTO.setOrderDetailId(orderDetailId);
            rwdTO.setDeliveryCity(city);
            rwdTO.setDeliveryState(state);

            rwdTO.setZipCode(zipCode.toUpperCase());

            rwdTO.setReturnAll(returnAll);
            rwdTO.setOrderValue(Double.parseDouble(price));
            rwdTO.setProduct(product);
            rwdTO.setDeliveryDate(deliveryDate);
            rwdTO.setDeliveryDateEnd(deliveryDateEnd);
            rwdTO.setSourceCode(sourceCode);
            rwdTO.setRWDFlagOverride(Boolean.TRUE);
            floristTO = MessagingServiceLocator.getInstance().getOrderAPI().getFillingFloristList(rwdTO, conn);
            if(floristTO==null)
            {
              logger.debug("floristTO==null");
            }

        } finally {
            if (logger.isDebugEnabled()) {
                logger.debug("Exiting lookupFloristList");
            }
        }

        //return the Document.
        return floristTO;
    }



    public String toXML(Object obj) throws Exception {
        StringBuffer sb = new StringBuffer();
        sb.append("<root>");
        if(obj!=null)
        {
          toXML(obj, sb);
        }

        sb.append("</root>");

        return sb.toString();
    }

    private void toXML(Object obj, StringBuffer sb)
        throws InvocationTargetException, IllegalAccessException,
            NoSuchMethodException {
        Map map = PropertyUtils.describe(obj);
        Iterator it = map.keySet().iterator();

        while (it.hasNext()) {
            String key = (String) it.next();
            //System.out.println("key="+key);
            Object value = map.get(key);
            //System.out.println("key="+key+"\tvalue="+value);
            if (value != null) {
                if (!key.equalsIgnoreCase("class")) {
                    sb.append("<" + key + ">");

                    if (value instanceof List) {
                        List objList = (List) value;
                        Iterator listIt = objList.iterator();

                        while (listIt.hasNext()) {
                            Object listVal = listIt.next();
                            sb.append("<item>");
                            toXML(listVal, sb);
                            sb.append("</item>");
                        }
                    } else if (value instanceof Date) {
                        sb.append(sdf.format((Date) value));
                    } else {
                        sb.append(DOMUtil.encodeChars(value.toString()));
                    }

                    sb.append("</" + key + ">");
                }
            }
        }
    }

    private String checkFloristStatus(HttpServletRequest request, boolean isComp, String destination, ActionMapping mapping)
        throws SAXException, ParserConfigurationException, IOException,
            SQLException, Exception {
        String floristCode = request.getParameter("filling_florist_code");
        String sourceCode = request.getParameter("source_code");
        String monthStr = request.getParameter("delivery_month");
        String dateStr=request.getParameter("delivery_date");
        String monthStrEnd = request.getParameter("delivery_month_end");
        String dateStrEnd=request.getParameter("delivery_date_end");
        MessagingHelper msgHelper=new MessagingHelper();
        Date deliveryDate = new Date();
        Date deliveryDateEnd=null;
        if(StringUtils.isNotBlank(monthStrEnd) && StringUtils.isNotBlank(dateStrEnd))
        {
        	deliveryDateEnd = msgHelper.createDeliveryDate(monthStrEnd, dateStrEnd);
        }
        if(StringUtils.isNotBlank(monthStr) && StringUtils.isNotBlank(dateStr))
        {
        	deliveryDate =msgHelper.createDeliveryDate(monthStr, dateStr);
        }
        else
        {
        	//this is coming from the Update Florist button on the Recipient Order Information Screen.
        	//Delivery Date isn't being passed in so retrieve the delivery date and delivery date range end from the Order Details record
        	OrderDAO orderDAO = new OrderDAO(conn);
            CachedResultSet rs = null;
            rs = orderDAO.getOrderDetailsInfo(request.getParameter(MessagingConstants.REQUEST_ORDER_DETAIL_ID));

            if (rs.next())
            {
                //retrieve/format the delivery date - note that the stored proc returns this as a String
                if (rs.getObject("delivery_date") != null)
                {
                  String sDeliveryDate = rs.getString("delivery_date");
                  deliveryDate = new Date(sDeliveryDate);
                }
                if (rs.getObject("delivery_date_range_end") != null)
                {
                  String sDeliveryDateRangeEnd = rs.getString("delivery_date_range_end");
                  deliveryDateEnd = new Date(sDeliveryDateRangeEnd);
                }
            }
        }

        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        String forward="";
        logger.debug("filling florist code=["+floristCode+"]" + " and source code = " + sourceCode + "and dateStr: " + dateStr);
        boolean callout=new Boolean(request.getParameter("call_out")).booleanValue();
        logger.debug("call out="+callout);
        if (floristCode != null) {
            MessageDAO dao = new MessageDAO(this.conn);
            FloristStatusVO floristStatus = dao.getFloristStatus(floristCode.trim().toUpperCase(), deliveryDate, deliveryDateEnd);
            //Defect 5150 - add disable florist flag which will be used for turning on/off functionality to be able to select/not select
            //a florist from the florist lookup screen when they have rejected an order
            String orderDetailId = request.getParameter("order_detail_id");
            String fillingFlorist = request.getParameter(MessagingConstants.REQUEST_FILLING_FLORIST);
            GlobalParmHandler globalParms = (GlobalParmHandler)CacheManager.getInstance().getHandler("CACHE_NAME_GLOBAL_PARM");
            String disableFlorist = globalParms.getGlobalParm("CUSTOMER_ORDER_MGMT_MESSAGING", "DISABLE_FLORIST_FLAG");

            PartnerVO preferredPartnerVO = FTDCommonUtils.getPreferredPartnerBySource(sourceCode);
            if (preferredPartnerVO != null && preferredPartnerVO.isFloristResendAllowed())
                disableFlorist = "N";
            
            //allow csrs to manually add the Non-FTD FTDM florist code
            if (floristStatus == null||floristStatus.getStatus()==null|| ( floristStatus.getStatus().equalsIgnoreCase("Inactive") && !fillingFlorist.equalsIgnoreCase(globalParms.getGlobalParm("SERVICE",
					"EFA_FTDM_FLORIST"))) ) {
                forward= "invalid_florist_error";
            } else if (disableFlorist.equalsIgnoreCase("Y") && dao.hasFloristRejectedOrder(orderDetailId , fillingFlorist) )
                forward= "reject_florist_error";
            else if(floristStatus.isVendor()){
                 forward= "vendor_florist_error";
            }else if(floristStatus.isOptout()){
               forward= "optout_florist_error";
            }else if(floristStatus.getOpenOnDeliveryDate().equalsIgnoreCase("N")){
		    	forward= floristStatus.getClosedOnDeliveryDateMsg();
			}else if(floristStatus.getFloristCurrentlyOpen().equalsIgnoreCase("N")){
            	forward= floristStatus.getCurrentlyClosedMsg();
        	}else{
        		logger.info("suspended: " + floristStatus.getFloristSuspended());
                //boolean suspend = floristStatus.getStatus().toUpperCase().indexOf("SUSPEND") != -1;
                boolean suspend = false;
                if (floristStatus.getFloristSuspended() != null && floristStatus.getFloristSuspended().equalsIgnoreCase("Y")) {
                	suspend = true;
                }

                if (callout||suspend || !floristStatus.isMercury()) {
                   if(isComp)
                   {
                     forward= "comp_call_out";
                   }else{
                      forward= MessagingConstants.ACTION_FORWARD_CALLOUT;
                   }
                } else {
                   if(StringUtils.isNotBlank(destination))
                   {
                     //send new ftd
                     SendMessageBO sendMessageBO=new SendMessageBO(this.conn);
                     forward= sendMessageBO.sendFTDMessage(
                              request, (HashMap)request.getAttribute(COMConstants.CONS_APP_PARAMETERS), mapping);


                   }else{
                     forward= MessagingConstants.ACTION_FORWARD_SEND_FTD;
                   }

                }

                 //update the delivery date of order detail. This is the temporay solution before modify order goes live.
                   if(StringUtils.isBlank(destination)&&!isComp)
                   {
                      this.updateOrderDeliveryDate(request);
                   }

            }
        }else
        {
          forward= "invalid_florist_error";

        }

        logger.debug("checkFloristStatus forward="+forward);

        return forward;
    }


    /**
     * Checks if recipient address has been changed
     *
     * @return boolean
     * @throws Exception
     */
    public boolean isRecipientAddressDifferent(HttpServletRequest request, String orderDetailId) throws Exception {

    	boolean recipAddressChanged = false;
    	String recipientAddressFromRequest = request.getParameter("recipient_street");
    	logger.info("recipientAddressFromRequest: " + recipientAddressFromRequest);
        String recipientCityFromRequest = request.getParameter("recipient_city");
    	logger.info("recipientCityFromRequest: " + recipientCityFromRequest);
        String recipientStateFromRequest = request.getParameter("recipient_state");
    	logger.info("recipientStateFromRequest: " + recipientStateFromRequest);
        String recipientZipFromRequest = request.getParameter("recipient_zip");
    	logger.info("recipientZipFromRequest: " + recipientZipFromRequest);
        String recipientCountryFromRequest = request.getParameter("recipient_country");
    	logger.info("recipientCountryFromRequest : " + recipientCountryFromRequest );

    	String recipAddressFromOrder = null;
    	String recipCityFromOrder = null;
    	String recipStateFromOrder = null;
    	String recipZipFromOrder = null;
    	String recipCountryFromOrder = null;

    	OrderDAO orderDAO = new OrderDAO(conn);
        CachedResultSet rs = null;
        rs = orderDAO.getOrderDetailsInfo(orderDetailId);

        if (rs.next())
        {
            recipAddressFromOrder = rs.getString("address_1");
        	logger.info("recipAddressFromOrder : " + recipAddressFromOrder );
            recipCityFromOrder = rs.getString("city");
        	logger.info("recipCityFromOrder : " + recipCityFromOrder );
            recipStateFromOrder = rs.getString("state");
        	logger.info("recipStateFromOrder : " + recipStateFromOrder );
            recipZipFromOrder = rs.getString("zip_code");
        	logger.info("recipZipFromOrder : " + recipZipFromOrder );
            recipCountryFromOrder = rs.getString("country");
        	logger.info("recipCountryFromOrder : " + recipCountryFromOrder );
        }
        else
        {
            logger.error("Order Detail Info missing from DB for orderDetailId="+orderDetailId);
        }

        if(recipientAddressFromRequest != null)
        {
	        if (!recipAddressFromOrder.equals(recipientAddressFromRequest)) {
	            recipAddressChanged = true;
	        }
        }
        if(recipientCityFromRequest != null)
        {
        	if (!recipCityFromOrder.equals(recipientCityFromRequest)) {
        		recipAddressChanged = true;
        	}
        }
        if(recipientStateFromRequest != null)
        {
	        if (!recipStateFromOrder.equals(recipientStateFromRequest)) {
		        recipAddressChanged = true;
	        }
        }
        if(recipientZipFromRequest != null)
        {
        	if (!recipZipFromOrder.substring(0,5).equals(recipientZipFromRequest.substring(0,5))) {
		        recipAddressChanged = true;
		    }
        }
        if(recipientCountryFromRequest != null)
        {
	        if (!recipCountryFromOrder.equals(recipientCountryFromRequest)) {
		        recipAddressChanged = true;
		    }
        }
        logger.info("recipAddressChanged: " + recipAddressChanged);
        return recipAddressChanged;
    }

    /**
     * Checks if order detail record has an avs address id associated with it
     * @param CachedResultSet
     * @return String
     * @throws Exception
     */
    public String orderHasAVSId(String orderDetailId) throws Exception {

        OrderDAO orderDAO = new OrderDAO(conn);
        CachedResultSet rs = null;
        rs = orderDAO.getOrderDetailsInfo(orderDetailId);

    	String avsAddressId = null;

        if (rs.next())
        {
        	if(rs.getString("avs_address_id") != null)
        	{
        		avsAddressId = rs.getString("avs_address_id");
        	}
        }
        logger.info("avsAddressId: " + avsAddressId);
        return avsAddressId;
    }

    private String getLegacyID(String orderDetailId) throws Exception{
    	 OrderDAO orderDAO = new OrderDAO(conn);
         CachedResultSet rs = null;
         rs = orderDAO.getOrderDetailsInfo(orderDetailId);

     	String legacyId = null;

         if (rs.next())
         {
         	if(rs.getString("legacy_id") != null)
         	{
         		legacyId = rs.getString("legacy_id");
         	}
         }
         logger.info("legacyId: " + legacyId);
         return legacyId;
	}

    public void setConn(Connection conn) {
        this.conn = conn;
    }
}
