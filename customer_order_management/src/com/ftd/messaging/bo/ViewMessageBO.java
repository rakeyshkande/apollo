package com.ftd.messaging.bo;

import com.ftd.messaging.constants.MessagingConstants;
import com.ftd.messaging.dao.MessageDAO;
import com.ftd.messaging.vo.MessageVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/**
 * This class contains business logic to display message details based on
 * message ID passed in the request and send the information back to the 
 * ViewMessageAction/ViewMessageSwitchAction for further processing. 
 * @author Charles Fox
 * @todo check comments, error handling, and message handling
 */
public class ViewMessageBO 
{
    private Logger logger = new Logger("com.ftd.messaging.bo.ViewMessageBO");
    private Connection dbConnection = null;
    private MessagingConstants messagingConstants;
    
    public ViewMessageBO(Connection conn)
    {
        super();
        dbConnection = conn;
    }

    public ViewMessageBO()
    {
        super();
    }
    
    
    /**
     *  Retrieve Mercury/Venus message details based on message id and 
     *  message type.
     * @param messageId - String
     * @param messageType - String
     * @param messageSubType - String
     * @return MessageVO
     * @throws IOException
     * @throws SAXException
     * @throws SQLException
     * @throws ParserConfigurationException
     * @throws Exception
     */  
    public MessageVO retrieveMessage(String messageId, String messageType,
																			String messageSubType)
        throws IOException, SAXException, SQLException, 
            ParserConfigurationException, Exception
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering retrieveMessage");
            logger.debug("messageId : " + messageId);
            logger.debug("messageType : " + messageType);
        } 
        
        MessageVO msgVO = new MessageVO();
        try
        {
            MessageDAO msgDAO = new MessageDAO(dbConnection);

						if(messageSubType != null && messageSubType.equalsIgnoreCase(MessagingConstants.GEN_SUBTYPE))
						{
							msgVO = msgDAO.getGENMessage(messageId,messageType);
						}
						else
						{
							msgVO = msgDAO.getMessage(messageId,messageType);							
						}
            
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting retrieveMessage");
            } 
            
        }
        
        return msgVO;
    }


    /**
     *  �   Call toXML method of MessageVO.
     *  �	Return a Document object that contains message data.
     *  message type.
     * @param messageId - String
     * @param messageType - String
     * @return MessageVO
     * @throws Exception????
     * @todo - code and determine exceptions
     */ 
    public Document createDocument(MessageVO messageVO)
        throws ParserConfigurationException, SAXException, IOException
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering createDocument");
            logger.debug("messageVO : " + messageVO);

        } 
        Document message;
        try
        {
            String messageXML = messageVO.toXML();
            
            message = DOMUtil.getDocument(messageXML);
        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting getMessageType");
            } 
            
        }
        return message;
    }
	
    public Document getOrderLifecycleMessages(String mercuryId) throws Exception {
        logger.debug("Entering getOrderLifecycleMessages()");
        Document lifecycleXML = null;
        try {
            MessageDAO msgDAO = new MessageDAO(dbConnection);
            lifecycleXML = msgDAO.getOrderLifecycleMessages(mercuryId);
        } catch (Exception e) {
            logger.error(e);
        }
        
        return lifecycleXML;
    }

}