package com.ftd.messaging.vo;

public class FloristStatusVO 
{
  private String floristCode;
  private boolean optout;
  private String mercury;
  private String status;
  private boolean vendor;
  private String sundayDeliveryFlag;
  private String floristCurrentlyOpen;
  private String currentlyClosedMsg;
  private String openOnDeliveryDate;
  private String closedOnDeliveryDateMsg;
  private String floristSuspended;
  private String floristBlocked;
  public static final String MERCURY_FLAG="M";
  
  
  public FloristStatusVO()
  {
  }


  public void setFloristCode(String floristCode)
  {
    this.floristCode = floristCode;
  }


  public String getFloristCode()
  {
    return floristCode;
  }


  public void setOptout(boolean optout)
  {
    this.optout = optout;
  }


  public boolean isOptout()
  {
    return optout;
  }


  public boolean isMercury()
  {
    return (this.mercury!=null&&this.mercury.trim().equalsIgnoreCase(MERCURY_FLAG));
  }


  public void setMercury(String value)
  {
    this.mercury = value;
  }


  public String getMercury()
  {
    return mercury;
  }


  public void setStatus(String status)
  {
    this.status = status;
  }


  public String getStatus()
  {
    return status;
  }


  public void setVendor(boolean vendor)
  {
    this.vendor = vendor;
  }


  public boolean isVendor()
  {
    return vendor;
  }


public String getFloristCurrentlyOpen() {
	return floristCurrentlyOpen;
}


public void setFloristCurrentlyOpen(String floristCurrentlyOpen) {
	this.floristCurrentlyOpen = floristCurrentlyOpen;
}


public String getCurrentlyClosedMsg() {
	return currentlyClosedMsg;
}


public void setCurrentlyClosedMsg(String currentlyClosedMsg) {
	this.currentlyClosedMsg = currentlyClosedMsg;
}


public String getOpenOnDeliveryDate() {
	return openOnDeliveryDate;
}


public void setOpenOnDeliveryDate(String openOnDeliveryDate) {
	this.openOnDeliveryDate = openOnDeliveryDate;
}


public String getClosedOnDeliveryDateMsg() {
	return closedOnDeliveryDateMsg;
}


public void setClosedOnDeliveryDateMsg(String closedOnDeliveryDateMsg) {
	this.closedOnDeliveryDateMsg = closedOnDeliveryDateMsg;
}


public String getSundayDeliveryFlag() {
	return sundayDeliveryFlag;
}


public void setSundayDeliveryFlag(String sundayDeliveryFlag) {
	this.sundayDeliveryFlag = sundayDeliveryFlag;
}


public String getFloristSuspended() {
	return floristSuspended;
}


public void setFloristSuspended(String floristSuspended) {
	this.floristSuspended = floristSuspended;
}


public String getFloristBlocked() {
	return floristBlocked;
}


public void setFloristBlocked(String floristBlocked) {
	this.floristBlocked = floristBlocked;
}
}