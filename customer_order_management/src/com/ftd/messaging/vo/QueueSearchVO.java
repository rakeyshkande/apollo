package com.ftd.messaging.vo;
import java.util.List;
public class QueueSearchVO
{
  private String messageSystem;
  private String queueType;
  private String askAnsCode;
  private String messageDirection;
  private String nonPartner;
  private List includedPartners;
  private List includedProdProperties;
  private String customerEmail;
  private Double amountFrom;
  private Double amountTo;
  private String keywordSearchText1;
  private String keywordSearchText2;
  private String keywordSearchText3;
  private String keywordSearchCondition1;
  private String keywordSearchCondition2;


  public void setMessageSystem(String messageSystem)
  {
    this.messageSystem = messageSystem;
  }

  public String getMessageSystem()
  {
    return messageSystem;
  }

  public void setQueueType(String queueType)
  {
    this.queueType = queueType;
  }

  public String getQueueType()
  {
    return queueType;
  }

  public void setMessageDirection(String messageDirection)
  {
    this.messageDirection = messageDirection;
  }

  public String getMessageDirection()
  {
    return messageDirection;
  }

  public void setNonPartner(String nonPartner)
  {
    this.nonPartner = nonPartner;
  }

  public String getNonPartner()
  {
    return nonPartner;
  }

  public void setIncludedPartners(List includedPartners)
  {
    this.includedPartners = includedPartners;
  }

  public List getIncludedPartners()
  {
    return includedPartners;
  }

  public void setIncludedProdProperties(List includedProdProperties)
  {
    this.includedProdProperties = includedProdProperties;
  }

  public List getIncludedProdProperties()
  {
    return includedProdProperties;
  }

  public void setCustomerEmail(String customerEmail)
  {
    this.customerEmail = customerEmail;
  }

  public String getCustomerEmail()
  {
    return customerEmail;
  }

  public void setAmountFrom(Double amountFrom)
  {
    this.amountFrom = amountFrom;
  }

  public Double getAmountFrom()
  {
    return amountFrom;
  }

  public void setAmountTo(Double amountTo)
  {
    this.amountTo = amountTo;
  }

  public Double getAmountTo()
  {
    return amountTo;
  }

  public void setKeywordSearchText1(String keywordSearchText1)
  {
    this.keywordSearchText1 = keywordSearchText1;
  }

  public String getKeywordSearchText1()
  {
    return keywordSearchText1;
  }

  public void setKeywordSearchText2(String keywordSearchText2)
  {
    this.keywordSearchText2 = keywordSearchText2;
  }

  public String getKeywordSearchText2()
  {
    return keywordSearchText2;
  }

  public void setKeywordSearchText3(String keywordSearchText3)
  {
    this.keywordSearchText3 = keywordSearchText3;
  }

  public String getKeywordSearchText3()
  {
    return keywordSearchText3;
  }

  public void setKeywordSearchCondition1(String keywordSearchCondition1)
  {
    this.keywordSearchCondition1 = keywordSearchCondition1;
  }

  public String getKeywordSearchCondition1()
  {
    return keywordSearchCondition1;
  }

  public void setKeywordSearchCondition2(String keywordSearchCondition2)
  {
    this.keywordSearchCondition2 = keywordSearchCondition2;
  }

  public String getKeywordSearchCondition2()
  {
    return keywordSearchCondition2;
  }


  public void setAskAnsCode(String askAnsCode)
  {
    this.askAnsCode = askAnsCode;
  }

  public String getAskAnsCode()
  {
    return askAnsCode;
  }
}
