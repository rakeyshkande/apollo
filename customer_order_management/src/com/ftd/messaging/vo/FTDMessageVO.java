package com.ftd.messaging.vo;

import java.lang.reflect.Field;
import java.util.Date;

public class FTDMessageVO extends MessageVO {

	String recipientCity = "";
	String recipientState = "";
	String recipientZip = "";
	String recipientCountry = "";
	double vendorCost;
	String productId;
	String vendorSku;
	String orderGUID = "";
	String customerID = "";
	String externalOrderNumber = "";
	String masterOrderNumber = "";
	String letterFlag = "";
	String emailFlag = "";
	String businessName = "";
	String sourceCode = "";
	String shipMethod = "";
	boolean zoneJumpFlag;
	Date zoneJumpLabelDate;
	boolean printed;
	String orderHasMorningDelivery = "";
	boolean isFTDWOrder = false;
	String calloutPhone;
	String isEfaFTDMFlorist;

	public FTDMessageVO() {
	}

	/* External Order Number */
	public String getExternalOrderNumber() {
		return externalOrderNumber;
	}

	public void setExternalOrderNumber(String newExternalOrderNumber) {
		externalOrderNumber = newExternalOrderNumber;
	}

	/* Order recipient city */
	public String getRecipientCity() {
		return recipientCity;
	}

	public void setRecipientCity(String newRecipientCity) {
		recipientCity = newRecipientCity;
	}

	/* Order recipient state */
	public String getRecipientState() {
		return recipientState;
	}

	public void setRecipientState(String newRecipientState) {
		recipientState = newRecipientState;
	}

	/* Order recipient zip code */
	public String getRecipientZip() {
		return recipientZip;
	}

	public void setRecipientZip(String newRecipientZip) {
		recipientZip = newRecipientZip;
	}

	public String toXML() {
		StringBuffer sb = new StringBuffer();
		try {
			sb.append("<message>");
			Field[] superFields = this.getClass().getSuperclass()
					.getDeclaredFields();
			appendFields(sb, superFields);
			Field[] fields = this.getClass().getDeclaredFields();
			appendFields(sb, fields);

			sb.append("</message>");
		}

		catch (Exception e) {
			e.printStackTrace();
		}

		return sb.toString();
	}

	public void setVendorCost(double vendorCost) {
		this.vendorCost = vendorCost;
	}

	public double getVendorCost() {
		return vendorCost;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getProductId() {
		return productId;
	}

	public void setVendorSku(String vendorSku) {
		this.vendorSku = vendorSku;
	}

	public String getVendorSku() {
		return vendorSku;
	}

	public void setOrderGUID(String orderGUID) {
		this.orderGUID = orderGUID;
	}

	public String getOrderGUID() {
		return orderGUID;
	}

	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}

	public String getCustomerID() {
		return customerID;
	}

	public void setMasterOrderNumber(String masterOrderNumber) {
		this.masterOrderNumber = masterOrderNumber;
	}

	public String getMasterOrderNumber() {
		return masterOrderNumber;
	}

	public void setLetterFlag(String letterFlag) {
		this.letterFlag = letterFlag;
	}

	public String getLetterFlag() {
		return letterFlag;
	}

	public void setEmailFlag(String emailFlag) {
		this.emailFlag = emailFlag;
	}

	public String getEmailFlag() {
		return emailFlag;
	}

	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}

	public String getBusinessName() {
		return businessName;
	}

	public void setSourceCode(String sourceCode) {
		this.sourceCode = sourceCode;
	}

	public String getSourceCode() {
		return sourceCode;
	}

	public void setRecipientCountry(String recipientCountry) {
		if (recipientCountry != null
				&& recipientCountry.trim().equalsIgnoreCase("USA")) {
			recipientCountry = "US";
		}
		this.recipientCountry = recipientCountry;
	}

	public String getRecipientCountry() {
		return recipientCountry;
	}

	public void setShipMethod(String shipMethod) {
		this.shipMethod = shipMethod;
	}

	public String getShipMethod() {
		return shipMethod;
	}

	public boolean isZoneJumpFlag() {
		return zoneJumpFlag;
	}

	public void setZoneJumpFlag(boolean zoneJumpFlag) {
		this.zoneJumpFlag = zoneJumpFlag;
	}

	public void setZoneJumpLabelDate(Date zoneJumpLabelDate) {
		this.zoneJumpLabelDate = zoneJumpLabelDate;
	}

	public Date getZoneJumpLabelDate() {
		return zoneJumpLabelDate;
	}

	public boolean isPrinted() {
		return printed;
	}

	public void setPrinted(boolean printed) {
		this.printed = printed;
	}

	public String getOrderHasMorningDelivery() {
		return orderHasMorningDelivery;
	}

	public void setOrderHasMorningDelivery(String orderHasMorningDelivery) {
		this.orderHasMorningDelivery = orderHasMorningDelivery;
	}

	public boolean isFTDWOrder() {
		return isFTDWOrder;
	}

	public void setFTDWOrder(boolean isFTDWOrder) {
		this.isFTDWOrder = isFTDWOrder;
	}

	public String getCalloutPhone() {
		return calloutPhone;
	}

	public void setCalloutPhone(String calloutPhone) {
		this.calloutPhone = calloutPhone;
	}

	public String getIsEfaFTDMFlorist() {
		return isEfaFTDMFlorist;
	}

	public void setIsEfaFTDMFlorist(String isEfaFTDMFlorist) {
		this.isEfaFTDMFlorist = isEfaFTDMFlorist;
	}
}