package com.ftd.messaging.vo;

import java.util.Calendar;
import java.util.Date;

/**
 * This class contains flags that indicate message order status.
 * @author Charles Fox
 * @todo check comments, error handling, and message handling
 */
public class MessageOrderStatusVO
{

    public static final String VENDOR="Vendor";
    public static final String FLORIST="Florist";
    public static final String FTDM="FTDM";
	  private Date today=new Date();
    private boolean attemptedFTD;
    private boolean pastDeliveryDate;
    private boolean hasLiveFTD ;
    private boolean cancelSent ;
    private boolean floristRejectOrder;
    private String fillingFloristCode = "";
    private Date deliveryDate;
    private String orderDetailId = "";
    private String messageOrderNumber = "";
    private String messageId = "";
    private boolean hasInboundASKP;
    private boolean aTypeRefund;
    private boolean cancelDenied;
    private String recipientCountry="";
    private boolean compOrder;
    
    /**
     * for Vendor filled orders. (i.e. shipped, printed)
     */
    private String orderStatus;
    /**
     * Vendor, Florist and FTDM
     */
    private String orderType="";

    private boolean pastDeliveryDateMonth;
	/**
	 * @return Returns the cancelSent.
	 */
	public boolean isCancelSent() {
		return cancelSent;
	}
	/**
	 * @param cancelSent The cancelSent to set.
	 */
	public void setCancelSent(boolean cancelSent) {
		this.cancelSent = cancelSent;
	}
	/**
	 * @return Returns the deliveryDate.
	 */
	public Date getDeliveryDate() {
		return deliveryDate;
	}
	/**
	 * @param deliveryDate The deliveryDate to set.
	 */
	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}
	/**
	 * @return Returns the fillingFloristCode.
	 */
	public String getFillingFloristCode() {
		return fillingFloristCode;
	}
	/**
	 * @param fillingFloristCode The fillingFloristCode to set.
	 */
	public void setFillingFloristCode(String fillingFloristCode) {
		this.fillingFloristCode = trim(fillingFloristCode);
	}
	/**
	 * @return Returns the floristRejectOrder.
	 */
	public boolean isFloristRejectOrder() {
		return floristRejectOrder;
	}
	/**
	 * @param floristRejectOrder The floristRejectOrder to set.
	 */
	public void setFloristRejectOrder(boolean floristRejectOrder) {
		this.floristRejectOrder = floristRejectOrder;
	}
	/**
	 * @return Returns the hasLiveFTD.
	 */
	public boolean isHasLiveFTD() {
		return hasLiveFTD;
	}
	/**
	 * @param hasLiveFTD The hasLiveFTD to set.
	 */
	public void setHasLiveFTD(boolean hasLiveFTD) {
		this.hasLiveFTD = hasLiveFTD;
	}
	/**
	 * @return Returns the messageId.
	 */
	public String getMessageId() {
		return messageId;
	}
	/**
	 * @param messageId The messageId to set.
	 */
	public void setMessageId(String messageId) {
		this.messageId = trim(messageId);
	}
	/**
	 * @return Returns the messageOrderNumber.
	 */
	public String getMessageOrderNumber() {
		return messageOrderNumber;
	}
	/**
	 * @param messageOrderNumber The messageOrderNumber to set.
	 */
	public void setMessageOrderNumber(String messageOrderNumber) {
		this.messageOrderNumber = messageOrderNumber;
	}

	/**
	 * @return Returns the orderDetailId.
	 */
	public String getOrderDetailId() {
		return orderDetailId;
	}
	/**
	 * @param orderDetailId The orderDetailId to set.
	 */
	public void setOrderDetailId(String orderDetailId) {
		this.orderDetailId = trim(orderDetailId);
	}
	/**
	 * @return Returns the orderDisposiiton.
	 */
	public String getOrderStatus() {
		return orderStatus;
	}
	/**
	 * @param orderDisposiiton The orderDisposiiton to set.
	 */
	public void setOrderStatus(String orderDisposiiton) {
		this.orderStatus = trim(orderDisposiiton);
	}
	/**
	 * Must be called after delivery date is set.
   * @return the pastDeliveryDate.
   * 
	 */
	public boolean isPastDeliveryDate() {

		boolean past=false;
    if(this.deliveryDate!=null)
    {
       Calendar cal=Calendar.getInstance();
       int year=Calendar.getInstance().get(Calendar.YEAR);
       int month=Calendar.getInstance().get(Calendar.MONTH);
       int day=Calendar.getInstance().get(Calendar.DATE);
       cal.set(year,month, day-1,23,59,59);
       Date current=cal.getTime();
       //convert delivery date to the same format.
       cal.setTime(this.deliveryDate);
       Date del=cal.getTime();
       
       past= current.after(del);
     
      
    }
    
    return past;
	}

  public boolean isAttemptedFTD()
  {
    return this.attemptedFTD;
  }


  public void setAttemptedFTD(boolean attempted )
  {
    this.attemptedFTD= attempted;
  }

	/**
	 * @return Returns the orderType.
	 */
	public String getOrderType() {
		return orderType;
	}
	/**
	 * @param orderType The orderType to set.
	 */
	public void setOrderType(String orderType) {
		this.orderType = trim(orderType);
	}
	/**
	 * @return Returns the pastDeliveryDateMonth.
	 */
	public boolean isPastDeliveryDateMonth() {

		if(this.deliveryDate!=null){
      Calendar c=Calendar.getInstance();
      c.setTime(today);
      int currentMonth=c.get(c.MONTH);
      int currentYear=c.get(c.YEAR);
      c.setTime(this.deliveryDate);
      int delMonth=c.get(c.MONTH);
      int delYear=c.get(c.YEAR);
                  
      if(currentYear>delYear)
      {
        return true;
      }else if(currentYear<delYear)
      {
        return false;
      }
      else
      {
        return currentMonth>delMonth;
      }
    }else
    {
      return false;
    }
    
	}

	public boolean isVendor(){

		return (this.orderType!=null&&this.orderType.equalsIgnoreCase(VENDOR));
	}


	public boolean isFlorist(){

		return (this.orderType!=null&&this.orderType.equalsIgnoreCase(FLORIST));
	}


	public boolean isFTDM(){

		return (this.orderType!=null&&this.orderType.equalsIgnoreCase(FTDM));
	}


  /**
	 * @return Returns the hasLiveFTD.
	 */
	public boolean isHasInboundASKP() {
		return this.hasInboundASKP;
	}
	/**
	 * @param hasLiveFTD The hasLiveFTD to set.
	 */
	public void setHasInboundASKP(boolean hasInboundASKP) {
		this.hasInboundASKP = hasInboundASKP;
	}

	/**
	 * @return Returns the aTypeRefund.
	 */
	public boolean isATypeRefund() {
		return aTypeRefund;
	}
	/**
	 * @param newATypeRefund The aTypeRefund to set.
	 */
	public void setATypeRefund(boolean newATypeRefund) {
		this.aTypeRefund = newATypeRefund;
	}

    
    private String trim(String str)
    {
        return (str != null)?str.trim():str;
    }


  public void setCancelDenied(boolean cancelDenied)
  {
    this.cancelDenied = cancelDenied;
  }


  public boolean isCancelDenied()
  {
    return cancelDenied;
  }


  public void setRecipientCountry(String recipientCountry)
  {
    this.recipientCountry = recipientCountry;
  }


  public String getRecipientCountry()
  {
    return recipientCountry;
  }


  public void setCompOrder(boolean isCompOrder)
  {
    this.compOrder = isCompOrder;
  }


  public boolean isCompOrder()
  {
    return compOrder;
  }

}