package com.ftd.messaging.vo;

public class CallOutInfo {
	 private String shopName;
	 private String shopPhone;
	 private String spokeTo;

	 	public CallOutInfo()
	 	{
	 	}
		public String getShopName() {
			return shopName;
		}
		public void setShopName(String shopName) {
			this.shopName = shopName;
		}
		public String getShopPhone() {
			return shopPhone;
		}
		public void setShopPhone(String shopPhone) {
			this.shopPhone = shopPhone;
		}
		public String getSpokeTo() {
			return spokeTo;
		}
		public void setSpokeTo(String spokeTo) {
			this.spokeTo = spokeTo;
		}

		@Override
		public String toString() {
			return "CallOutFloristInfo [shopName=" + shopName + ", shopPhone="
					+ shopPhone + ", spokeTo=" + spokeTo + "]";
		}
		
}
