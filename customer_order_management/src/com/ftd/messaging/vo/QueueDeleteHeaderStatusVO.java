package com.ftd.messaging.vo;

import com.ftd.osp.utilities.vo.QueueDeleteHeaderVO;

import java.text.MessageFormat;
import java.text.SimpleDateFormat;

import java.util.Date;


/**
 * Value Object used to display the batch submittal process status for queue delete
 * @author Vinay Shivaswamy
 * @Date 11/30/2010
 * @version 1.0 for MQD 4.7.0
 */
public class QueueDeleteHeaderStatusVO extends QueueDeleteHeaderVO{
  
    /*
    * Declare members
    */
    private String batchName;
    private String progress;
    private String linkToDisplay;
    private String linkToSave;
    private Integer totalProcessed;
    private Integer totalSuccess;
    private Integer totalFailure;
    private Integer totalDuplicate;
    private final String dashDateFormat="MM-dd-yyyy_hh:mma";
    private final String slashDateFormat="MM/dd/yyyy-hh:mma";
    private final String processedFormat="{0} of {1} processed";
    private final String fileExt=".xls";
    
    /*
    * default no-arg constructor
    */
    public QueueDeleteHeaderStatusVO()  {
    
    }
    
    /**
    * Returns formatted "11-30-2010_16:09PM_Flushed ZIP queue" string.
    * @param None
    * @return String 
    */
    public String getBatchName(){
        String dateFormat = getDateFormat(getCreatedOn(),dashDateFormat);
        String[] msgs = new String[]{dateFormat,getBatchDescription()};
        return MessageFormat.format("{0}_{1}", msgs);
    }
    
    /**
    * Returns formatted "10 of 100 processed" string.
    * @param None
    * @return String 
    */
    public String getProgress(){
        Integer[] counts = new Integer[] {getTotalProcessed(), getBatchCount()}; 
        return MessageFormat.format(processedFormat, counts);
    }
    
    /**
    * Returns formatted "11-30-2010_16:09PM_Flushed ZIP queue.xls" string.
    * @param None
    * @return String 
    */
    public String getLinkToDisplay(){
        String dateFormat = getDateFormat(getCreatedOn(),slashDateFormat);
        String[] msgs = new String[]{getCreatedBy(),dateFormat,getBatchDescription(),fileExt};
        return MessageFormat.format("{0}_{1}-{2}{3}", msgs);
    }
    
    /**
    * Returns formatted "11-30-2010_16:09PM_Flushed ZIP queue.xls" string.
    * @param None
    * @return String 
    */
    public String getLinkToSave(){
        String dateFormat = getDateFormat(getCreatedOn(),dashDateFormat);
        String[] msgs = new String[]{getCreatedBy(),dateFormat,getBatchDescription(),fileExt};
        return MessageFormat.format("{0}_{1}-{2}{3}", msgs);
    }
    
    public void setTotalProcessed(Integer totalProcessed){
        this.totalProcessed = totalProcessed;
    }
    
    public Integer getTotalProcessed(){
        return totalProcessed;
    }

    public void setTotalSuccess(Integer totalSuccess){
        this.totalSuccess = totalSuccess;
    }
    
    public Integer getTotalSuccess(){
        return totalSuccess;
    }
    
    public void setTotalFailure(Integer totalFailure){
        this.totalFailure = totalFailure;
    }
    
    public Integer getTotalFailure(){
        return totalFailure;
    }
  
    public void setTotalDuplicate(Integer totalDuplicate) {
        this.totalDuplicate = totalDuplicate;
    }

    public Integer getTotalDuplicate() {
        return totalDuplicate;
    }
    
    /**
     * Formats the date using specified formatter
     * @param - Date date
     * @return - Formatted date in String
     */
    private String getDateFormat(Date date, String format){
      SimpleDateFormat formatter = new SimpleDateFormat(format);
      return formatter.format(date);
    }
   
}
