package com.ftd.messaging.vo;

import com.ftd.customerordermanagement.vo.XMLInterface;
import com.ftd.messaging.constants.MessagingConstants;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import java.io.IOException;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/**
 * This class contains message details.
 * @author Charles Fox
 * @todo check comments, error handling, and message handling
 */
public class MessageVO implements XMLInterface
{
    private Logger logger = new Logger("com.ftd.messaging.vo.MessageVO");

    public MessageVO()
    {
    }


    String type = "";
    String messageId = "";
    String messageOrderId = "";
    String detailType = "";
    String subType = "";
    String indicator ="";
    String fillingFloristCode = "";
    String sendingFloristCode = "";
    String recipientName = "";
    String recipientStreet = "";
    String recipientCityStateZip="";

    String recipientPhone = "";
    Date deliveryDate;
    String endDeliveryDate;
    String firstChoice = "";
    String substitution = "";
    double currentPrice = 0;
    String cardMessage = "";
    String occasionCode = "";
    String occasionDesc = "";
    String specialInstruction = "";
    String priority = "";
    String operator = "";
    String operatorFirstName = "";
    double oldPrice = 0;
    String orderDetailId = "";
    Date orderDate;
    String comments="";
    boolean verified = false;
    Date statusDate;
    String combinedReportNumber = "";
    String adjustmentReasonCode = "";
    String adjustmentReasonDesc = "";
    String cancelReasonCode = "";
    String cancelReasonDesc = "";
    boolean withinMessageWindow = false;
    boolean isSubsequentMessageAllowed = false;
    String deliveryText = "";
    String status = "";
    String orderDateText="";
    String combineRptNum = "";
    String ftdMessageID = "";
    String floristName = "";
    String floristPhoneNumber = "";
    boolean wineDotComProduct = false;
    String statusDateText="";
    String messageStatus="";
    boolean ftdCancelledOrRejected;
    double overUnderCharge = 0;
    Date shipDate;
    String shippingSystem="";
    String sdsStatus = "";
    boolean zoneJumpFlag = false; 
    Date zoneJumpLabelDate;
    String zoneJumpTrailerNumber = ""; 
    String sendingFloristCompanyName = "";
    String orderAddOns;
    String cogs;

     /* Order florist phone number */
    public String getFloristPhoneNumber() {
        return floristPhoneNumber;
    }

    public void setFloristPhoneNumber(String newFloristPhoneNumber) {
        floristPhoneNumber = newFloristPhoneNumber;
    }

   /* Order florist name */
    public String getFloristName() {
        return floristName;
    }

    public void setFloristName(String newFloristName) {
        floristName = newFloristName;
    }
    /* Message Type (i.e. Mercury and Venus) */
    public String getType() {
        return type;
    }

    public void setType(String newType) {
        type = trim(newType);
    }

    /* Message id. */
    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String newMessageId) {
        messageId = trim(newMessageId);
    }

    /* Message order id. */
    public String getMessageOrderId() {
        return messageOrderId;
    }

    public void setMessageOrderId(String newMessageOrderId) {
        messageOrderId = trim(newMessageOrderId);
    }

    /* Message Detail Type (i.e. FTD, CAN, ASK, ANS�, etc.) */
    public String getDetailType() {
        return detailType;
    }

    public void setDetailType(String newDetailType) {
        detailType = trim(newDetailType);
    }

    public String getSubType() {
        return subType;
    }

    public void setSubType(String newSubType) {
        subType = trim(newSubType);
    }

    public String getIndicator() {
        return indicator;
    }

    public void setIndicator(String newIndicator) {
        indicator = trim(newIndicator);
    }

    /* Filling florist code */
    public String getFillingFloristCode() {
        return fillingFloristCode;
    }

    public void setFillingFloristCode(String newFillingFloristCode) {
        fillingFloristCode = trim(newFillingFloristCode);
    }

    /* Filling florist code */
    public String getSendingFloristCode() {
        return sendingFloristCode;
    }

    public void setSendingFloristCode(String newSendingFloristCode) {
        sendingFloristCode = trim(newSendingFloristCode);
    }


    /* Order recipient name */
    public String getRecipientName() {
        return recipientName;
    }

    public void setRecipientName(String newRecipientName) {
        recipientName = trim(newRecipientName);
    }

    /* Order recipient street address */
    public String getRecipientStreet() {
        return recipientStreet;
    }

    public void setRecipientStreet(String newRecipientStreet) {
        recipientStreet = trim(newRecipientStreet);
    }



    /* Order recipient phone number */
    public String getRecipientPhone() {
        return recipientPhone;
    }

    public void setRecipientPhone(String newRecipientPhone) {
        recipientPhone = trim(newRecipientPhone);
    }

    /* Order delivery date */
    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(Date newDeliveryDate) {
        deliveryDate = newDeliveryDate;
    }

    /* Order end delivery date */
    public String getEndDeliveryDate() {
        return endDeliveryDate;
    }

    public void setEndDeliveryDate(String newEndDeliveryDate) {
        endDeliveryDate = trim(newEndDeliveryDate);
    }

    /* text for delivery*/
    public String getDeliveryText() {
        return deliveryText;
    }

    public void setDeliveryText(String newDeliveryText) {
        deliveryText = trim(newDeliveryText);
    }



    /* First Choice Product Selection and description */
    public String getFirstChoice() {
        return firstChoice;
    }

    public void setFirstChoice(String newFirstChoice) {
        firstChoice = trim(newFirstChoice);
    }

    /* Second Choice or substitution product and description */
    public String getSubstitution() {
        return substitution;
    }

    public void setSubstitution(String newSubstitution) {
        substitution = trim(newSubstitution);
    }

    /* Florist�s Product Price */
    public double getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(double newCurrentPrice) {
        currentPrice = newCurrentPrice;
    }

    /* Card Message Provided By the Sender */
    public String getCardMessage() {
        return cardMessage;
    }

    public void setCardMessage(String newCardMessage) {
        cardMessage = trim(newCardMessage);
    }

    /* The numeric value of occasion code. */
    public String getOccasionCode() {
        return occasionCode;
    }

    public void setOccasionCode(String newOccasionCode) {
        occasionCode = trim(newOccasionCode);
    }

    /* The occasion description (e.g. Holiday). */
    public String getOccasionDesc() {
        return occasionDesc;
    }

    public void setOccasionDesc(String newOccasionDesc) {
        occasionDesc = trim(newOccasionDesc);
    }

    /* Special instructions provided by the sender. */
    public String getSpecialInstruction() {
        return specialInstruction;
    }

    public void setSpecialInstruction(String newSpecialInstruction) {
        specialInstruction = trim(newSpecialInstruction);
    }

    /* Priority Code */
    public String getPriority() {
        return priority;
    }

    public void setPriority(String newPriority) {
        priority = trim(newPriority);
    }

    /* Operator Code Assigned to the message */
    public String getOperator() {
        return operator;
    }

    public void setOperator(String newOperator) {
        operator = trim(newOperator);
    }

    /* Operator First Name */
    public String getOperatorFirstName() {
        return operatorFirstName;
    }

    public void setOperatorFirstName(String newOperatorFirstName) {
        operatorFirstName = trim(newOperatorFirstName);
    }
    /* New price */
    public double getOldPrice() {
        return oldPrice;
    }

    public void setOldPrice(double newOldPriceValue) {
        oldPrice = newOldPriceValue;
    }

    /* Over/Under charge */
    public double getOverUnderCharge() {
        return overUnderCharge;
    }

    public void setOverUnderCharge(double newOverUnderCharge) {
        overUnderCharge = newOverUnderCharge;
    }

    /* Order detail id. */
    public String getOrderDetailId() {
        return orderDetailId;
    }

    public void setOrderDetailId(String newOrderDetailId) {
        orderDetailId = trim(newOrderDetailId);
    }

    /* Order date */
    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date newOrderDate) {
        orderDate = newOrderDate;
    }

    /* Reason */
    public String getComments() {
        return comments;
    }

    public void setComments(String newComments) {
        comments = trim(newComments);
    }

    /* Flag of EFOS verification status. */
    public boolean getVerified() {
        return verified;
    }

    public void setVerified(String newVerified) {

        String verify = trim(newVerified);

        if(verify!=null){
            if(verify.toUpperCase().equals(MessagingConstants.MERCURY_MESSAGE_VERIFIED) ||
            verify.toUpperCase().equals(MessagingConstants.MERCURY_MESSAGE_MANUAL))
            {
                verified = true;
            }
            else
            {
                verified = false;
            }
        }
        else
        {
            verified = false;
        }
    }

    /* EFOS verification date */
    public Date getStatusDate() {
        return this.statusDate;
    }

    public void setStatusDate(Date newStatusDate) {
        this.statusDate = newStatusDate;
    }

    /* Combined report number */
    public String getCombinedReportNumber() {
        return combinedReportNumber;
    }

    public void getCombinedReportNumber(String newCombinedReportNumber) {
        combinedReportNumber = trim(newCombinedReportNumber);
    }

    /* Adjustment reason code */
    public String getAdjustmentReasonCode() {
        return adjustmentReasonCode;
    }

    public void setAdjustmentReasonCode(String newAdjustmentReasonCode) {
        adjustmentReasonCode = trim(newAdjustmentReasonCode);
    }

    /* Adjustment reason code description */
    public String getAdjustmentReasonDesc() {
        return adjustmentReasonDesc;
    }

    public void setAdjustmentReasonDesc(String newAdjustmentReasonDesc) {
        adjustmentReasonDesc = trim(newAdjustmentReasonDesc);
    }


    public String getCombineRptNum() {
        return combineRptNum;
    }

    public void setCombineRptNum(String newCombineRptNum) {
        combineRptNum = trim(newCombineRptNum);
    }


    /* Cancel reason code */
    public String getCancelReasonCode() {
        return cancelReasonCode;
    }

    public void setCancelReasonCode(String newCancelReasonCode) {
        cancelReasonCode = trim(newCancelReasonCode);
    }

    /* Cancel reason code description */
    public String getCancelReasonDesc() {
        return cancelReasonDesc;
    }

    public void setCancelReasonDesc(String newCancelReasonDesc) {
        cancelReasonDesc = trim(newCancelReasonDesc);
    }

    /* Determine if the current date is not past the 90 days of order delivery date*/
    public boolean getWithinMessageWindow() {
        return withinMessageWindow;
    }

    public void setWithinMessageWindow(boolean newWithinMessageWindow) {
        withinMessageWindow = newWithinMessageWindow;
    }


    /* Flag that indicates whether or not the subsequent messages are allowed. */
    public boolean getIsSubsequentMessageAllowed() {
        return isSubsequentMessageAllowed;
    }

    public void setIsSubsequentMessageAllowed() {
        if(wineDotComProduct==false)
        {
            if(withinMessageWindow==true){
                    if(indicator.equals(MessagingConstants.INBOUND_MESSAGE))
                    {
                        isSubsequentMessageAllowed = true;
                    }
                    else
                    {
                        if(detailType.equals(MessagingConstants.FTD_MESSAGE)){
                             // System.out.println("this.messageStatus="+this.messageStatus);
                              if (this.status != null &&
                                  !this.status.equalsIgnoreCase("") &&
                                  this.status.equalsIgnoreCase("OPEN"))
                              {
                                 isSubsequentMessageAllowed = false;
                              }
                              else{
                                isSubsequentMessageAllowed = true;
                              }

                        }
                        else
                        {
                            isSubsequentMessageAllowed = false;
                        }
                    }
                }
                else
                {
                        isSubsequentMessageAllowed = false;
                }
        }
        else
        {
            isSubsequentMessageAllowed = false;
        }
    }

    /* Determine if this for a wine.com order*/
    public boolean getWineDotComProduct() {
        return wineDotComProduct;
    }

    public void setWineDotComProduct(boolean newWineDotComProduct) {
        wineDotComProduct = newWineDotComProduct;
    }

    /* EFOS verification status */
    public String getStatus() {
        return status;
    }

    public void setStatus(String newStatus) {
        status = trim(newStatus);
    }

  /**
    * This method uses the Reflection API to generate an XML string that will be
    * passed back to the calling module.
    * The XML string will contain all the fields within this VO, including the
    * variables as well as (a collection of) ValueObjects.
    *
    * @param  None
    * @return XML string
   **/
  public String toXML()
  {
    StringBuffer sb = new StringBuffer();
    try
    {
      sb.append("<message>");
      Field[] fields = this.getClass().getDeclaredFields();

      appendFields(sb, fields);
      sb.append("</message>");
    }

    catch (Exception e)
    {
      e.printStackTrace();
    }

    return sb.toString();
  }


  protected void appendFields(StringBuffer sb, Field[] fields) throws IllegalAccessException, ClassNotFoundException
  {
  	for (int i = 0; i < fields.length; i++)
  	{
  	  //if the field retrieved was a list of VO
  	  if(fields[i].getType().equals(Class.forName("java.util.List")))
  	  {
  	    List list = (List)fields[i].get(this);
  	    if(list != null)
  	    {
  	      for (int j = 0; j < list.size(); j++)
  	      {
  	        XMLInterface xmlInt = (XMLInterface)list.get(j);
  	        String sXmlVO = xmlInt.toXML();
  	        sb.append(sXmlVO);
  	      }
  	    }
  	  }
  	  else
  	  {
  	    //if the field retrieved was a VO
  	    if (fields[i].getType().toString().matches("(?i).*vo"))
  	    {
  	      XMLInterface xmlInt = (XMLInterface)fields[i].get(this);
  	      String sXmlVO = xmlInt.toXML();
  	      sb.append(sXmlVO);
  	    }
  	    //if the field retrieved was a Calendar object
  	    else if (fields[i].getType().toString().matches("(?i).*calendar"))
  	    {
  	      Date date;
  	      String fDate = null;
  	      if (fields[i].get(this) != null)
  	      {
  	        date = (((GregorianCalendar)fields[i].get(this)).getTime());
  	        SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss a zzz");
  	        fDate = sdf.format(date).toString();
  	      }
  	      sb.append("<" + fields[i].getName() + ">");
  	      sb.append(fDate);
  	      sb.append("</" + fields[i].getName() + ">");
  	    }
  	    else
  	    {

  	      sb.append("<" + fields[i].getName() + ">");
  	      String value = "";

          if(fields[i].get(this)!=null){
              value = fields[i].get(this).toString();
              value = DOMUtil.encodeChars(value);
          }
          else
          {
              value = "";
          }


  	      sb.append(value);
  	      sb.append("</" + fields[i].getName() + ">");
  	    }
  	  }
  	}
  }



  /**
    * This method uses the Reflection API to generate an XML string that will be
    * passed back to the calling module.
    * The XML string will contain all the fields within this VO, including the
    * variables as well as (a collection of) ValueObjects.
    *
    * @param  None
    * @return XML string
   **/
  public String toXML(int count)
  {
    StringBuffer sb = new StringBuffer();
    try
    {
      if (count == 0)
      {
        sb.append("<message>");
      }
      else
      {
        sb.append("<message num=" + '"' + count + '"' + ">");
      }
      Field[] fields = this.getClass().getDeclaredFields();

      appendFieldsWithCount(sb, fields);
      sb.append("</message>");
    }

    catch (Exception e)
    {
      e.printStackTrace();
    }

    return sb.toString();
  }


  protected void appendFieldsWithCount(StringBuffer sb, Field[] fields) throws IllegalAccessException, ClassNotFoundException
  {
  	for (int i = 0; i < fields.length; i++)
  	{
  	  //if the field retrieved was a list of VO
  	  if(fields[i].getType().equals(Class.forName("java.util.List")))
  	  {
  	    List list = (List)fields[i].get(this);
  	    if(list != null)
  	    {
  	      for (int j = 0; j < list.size(); j++)
  	      {
  	        XMLInterface xmlInt = (XMLInterface)list.get(j);
            int k = j + 1;
            String sXmlVO = xmlInt.toXML(k);
  	        sb.append(sXmlVO);
  	      }
  	    }
  	  }
  	  else
  	  {
  	    //if the field retrieved was a VO
  	    if (fields[i].getType().toString().matches("(?i).*vo"))
  	    {
  	      XMLInterface xmlInt = (XMLInterface)fields[i].get(this);
          int k = i + 1;
          String sXmlVO = xmlInt.toXML(k);
  	      sb.append(sXmlVO);
  	    }
  	    //if the field retrieved was a Calendar object
  	    else if (fields[i].getType().toString().matches("(?i).*calendar"))
  	    {
  	      Date date;
  	      String fDate = null;
  	      if (fields[i].get(this) != null)
  	      {
  	        date = (((GregorianCalendar)fields[i].get(this)).getTime());
  	        SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss a zzz");
  	        fDate = sdf.format(date).toString();
	          sb.append("<" + fields[i].getName() + ">");
            sb.append(fDate);
            sb.append("</" + fields[i].getName() + ">");
          }
          else
          {
  					sb.append("<" + fields[i].getName() + "/>");
          }
  	    }
  	    else
  	    {
					if (fields[i].get(this) == null)
					{
						sb.append("<" + fields[i].getName() + "/>");
					}
					else
					{
						sb.append("<" + fields[i].getName() + ">");
						sb.append(fields[i].get(this));
						sb.append("</" + fields[i].getName() + ">");
					}
  	    }
  	  }
  	}
  }


    private String trim(String str)
    {
        return (str != null)?str.trim():str;
    }


  public void setRecipientCityStateZip(String recipientCityStateZip)
  {
    this.recipientCityStateZip = recipientCityStateZip;
  }


  public String getRecipientCityStateZip()
  {
    return recipientCityStateZip;
  }


  public void setOrderDateText(String orderDateText)
  {
    this.orderDateText = orderDateText;
  }


  public String getOrderDateText()
  {
    return orderDateText;
  }

      /* FTD Message IF */
    public String getFTDMessageID() {
        return ftdMessageID;
    }

    public void setFTDMessageID(String newFTDMessageID) {
        ftdMessageID = trim(newFTDMessageID);
    }

    public String getStatusDateText()
    {

      return this.statusDateText;
    }

    public void setStatusDateText()
    {


      if(this.getStatusDate()!=null)
      {

        SimpleDateFormat sdf=new SimpleDateFormat("EEE MMMMM dd yy hh:mm a");
        this.statusDateText= sdf.format(this.getStatusDate());
      }

    }


  public void setMessageStatus(String messageStatus)
  {
    this.messageStatus = messageStatus;
  }


  public String getMessageStatus()
  {
    return messageStatus;
  }


  public void setFtdCancelledOrRejected(boolean ftdCancelledOrRejected)
  {
    this.ftdCancelledOrRejected = ftdCancelledOrRejected;
  }


  public boolean isFtdCancelledOrRejected()
  {
    return ftdCancelledOrRejected;
  }


  /* Order ship date */
  public Date getShipDate() {
      return shipDate;
  }

  public void setShipDate(Date newShipDate) {
      shipDate = newShipDate;
  }


  public void setShippingSystem(String shippingSystem)
  {
    this.shippingSystem = shippingSystem;
  }

  public String getShippingSystem()
  {
    return shippingSystem;
  }
  
  public void setSdsStatus(String sdsStatus)
  {
    this.sdsStatus = sdsStatus;
  }

  public String getSdsStatus()
  {
    return sdsStatus;
  }

  public void setZoneJumpFlag(boolean zoneJumpFlag)
  {
    this.zoneJumpFlag = zoneJumpFlag;
  }

  public boolean isZoneJumpFlag()
  {
    return zoneJumpFlag;
  }

  public void setZoneJumpLabelDate(Date zoneJumpLabelDate)
  {
    this.zoneJumpLabelDate = zoneJumpLabelDate;
  }

  public Date getZoneJumpLabelDate()
  {
    return zoneJumpLabelDate;
  }

  public void setZoneJumpTrailerNumber(String zoneJumpTrailerNumber)
  {
    this.zoneJumpTrailerNumber = zoneJumpTrailerNumber;
  }

  public String getZoneJumpTrailerNumber()
  {
    return zoneJumpTrailerNumber;
  }

public String getSendingFloristCompanyName() {
	return sendingFloristCompanyName;
}

public void setSendingFloristCompanyName(String sendingFloristCompanyName) {
	this.sendingFloristCompanyName = sendingFloristCompanyName;
}

public String getOrderAddOns() {
	return orderAddOns;
}

public void setOrderAddOns(String orderAddOns) {
	this.orderAddOns = orderAddOns;
}
/* cost of goods User story - 296*/
public String getCogs() {
		return cogs;
	}

public void setCogs(String cogs) {
	this.cogs = cogs;	
}


}
