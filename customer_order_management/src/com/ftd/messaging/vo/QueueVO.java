package com.ftd.messaging.vo;

import java.util.Date;


/**
 * Adding queue
 * @author Ali Lakhani
 */
public class QueueVO
{

  private String messageId;
  private String queueType;
  private String messageType;
  private Date   messageTimestamp;
  private String system;
  private String mercuryNumber;
  private String masterOrderNumber;
  private String orderGuid;
  private String orderDetailId;
  private String pointOfContactId;
  private String timezone;
  private Date   deliveryDate;
  private String emailAddress;
  private String recipientLocation;
  private String untaggedNewItems;
  private Date   createdOn;
  private String externalOrderNumber;
  private String sameDayGift;
  private String mercuryId;

  public QueueVO()
  {
  }


  public void setMessageId(String messageId)
  {
    this.messageId = messageId;
  }

  public String getMessageId()
  {
    return messageId;
  }

  public void setQueueType(String queueType)
  {
    this.queueType = queueType;
  }

  public String getQueueType()
  {
    return queueType;
  }

  public void setMessageType(String messageType)
  {
    this.messageType = messageType;
  }

  public String getMessageType()
  {
    return messageType;
  }

  public void setMessageTimestamp(Date messageTimestamp)
  {
    this.messageTimestamp = messageTimestamp;
  }

  public Date getMessageTimestamp()
  {
    return messageTimestamp;
  }

  public void setSystem(String system)
  {
    this.system = system;
  }

  public String getSystem()
  {
    return system;
  }

  public void setMercuryNumber(String mercuryNumber)
  {
    this.mercuryNumber = mercuryNumber;
  }

  public String getMercuryNumber()
  {
    return mercuryNumber;
  }

  public void setMasterOrderNumber(String masterOrderNumber)
  {
    this.masterOrderNumber = masterOrderNumber;
  }

  public String getMasterOrderNumber()
  {
    return masterOrderNumber;
  }

  public void setOrderGuid(String orderGuid)
  {
    this.orderGuid = orderGuid;
  }

  public String getOrderGuid()
  {
    return orderGuid;
  }

  public void setOrderDetailId(String orderDetailId)
  {
    this.orderDetailId = orderDetailId;
  }

  public String getOrderDetailId()
  {
    return orderDetailId;
  }

  public void setPointOfContactId(String pointOfContactId)
  {
    this.pointOfContactId = pointOfContactId;
  }

  public String getPointOfContactId()
  {
    return pointOfContactId;
  }

  public void setTimezone(String timezone)
  {
    this.timezone = timezone;
  }

  public String getTimezone()
  {
    return timezone;
  }

  public void setDeliveryDate(Date deliveryDate)
  {
    this.deliveryDate = deliveryDate;
  }

  public Date getDeliveryDate()
  {
    return deliveryDate;
  }

  public void setEmailAddress(String emailAddress)
  {
    this.emailAddress = emailAddress;
  }

  public String getEmailAddress()
  {
    return emailAddress;
  }

  public void setRecipientLocation(String recipientLocation)
  {
    this.recipientLocation = recipientLocation;
  }

  public String getRecipientLocation()
  {
    return recipientLocation;
  }

  public void setUntaggedNewItems(String untaggedNewItems)
  {
    this.untaggedNewItems = untaggedNewItems;
  }

  public String getUntaggedNewItems()
  {
    return untaggedNewItems;
  }

  public void setCreatedOn(Date createdOn)
  {
    this.createdOn = createdOn;
  }

  public Date getCreatedOn()
  {
    return createdOn;
  }

  public void setExternalOrderNumber(String externalOrderNumber)
  {
    this.externalOrderNumber = externalOrderNumber;
  }

  public String getExternalOrderNumber()
  {
    return externalOrderNumber;
  }

  public void setSameDayGift(String sameDayGift)
  {
    this.sameDayGift = sameDayGift;
  }

  public String getSameDayGift()
  {
    return sameDayGift;
  }

  public void setMercuryId(String mercuryId)
  {
    this.mercuryId = mercuryId;
  }

  public String getMercuryId()
  {
    return mercuryId;
  }
}
