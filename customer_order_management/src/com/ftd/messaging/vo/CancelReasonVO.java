package com.ftd.messaging.vo;

public class CancelReasonVO 
{
  private String reasonCode;
  private String descritpion;
  private String allowBeforeShip;
  private String allowAfterShip;
  
  
  public CancelReasonVO()
  {
  }


    public void setReasonCode(String reasonCode) {
        this.reasonCode = reasonCode;
    }

    public String getReasonCode() {
        return reasonCode;
    }

    public void setDescritpion(String descritpion) {
        this.descritpion = descritpion;
    }

    public String getDescritpion() {
        return descritpion;
    }

    public void setAllowBeforeShip(String allowBeforeShip) {
        this.allowBeforeShip = allowBeforeShip;
    }

    public String getAllowBeforeShip() {
        return allowBeforeShip;
    }

    public void setAllowAfterShip(String allowAfterShip) {
        this.allowAfterShip = allowAfterShip;
    }

    public String getAllowAfterShip() {
        return allowAfterShip;
    }
}
