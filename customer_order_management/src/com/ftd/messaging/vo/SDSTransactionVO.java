package com.ftd.messaging.vo;

import java.math.BigDecimal;

public class SDSTransactionVO {
    private BigDecimal id;
    private String venusId;
    private String request;
    private String response;
    
    public SDSTransactionVO() {
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setVenusId(String venusId) {
        this.venusId = venusId;
    }

    public String getVenusId() {
        return venusId;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public String getRequest() {
        return request;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getResponse() {
        return response;
    }
}
