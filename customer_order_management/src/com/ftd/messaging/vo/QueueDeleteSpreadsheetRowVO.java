package com.ftd.messaging.vo;

//import com.ftd.security.util.XMLHelper;
//import org.w3c.dom.Document;
//import org.w3c.dom.Element;
import com.ftd.security.cache.vo.*;

import com.ftd.security.exceptions.ExcelProcessingException;


/**
 * Representation of a QueueDelete line item to be loaded from Excel spreadsheet.
 * @author JP Puzon
 */
public class QueueDeleteSpreadsheetRowVO extends LineItemVO
{
    // attributes common to all instances.
    private final static int COLUMNS = 2;
    private static final String ERROR_REQUIRED_FIELD_MISSING = "Required Field Missing";
    private static final String ERROR_DATA_TOO_LARGE = "Data Too Large";
    private static final String ERROR_INVALID_NUMERIC = "Invalid Numeric Value";
    
    public final static String FIELD_NAME_MESSAGE_ID = "Message ID";
    public final static String FIELD_NAME_MESSAGE_SYSTEM = "System";

      // Output XML structure.
//      public static final String XML_TOP = "USERS";
//      public static final String XML_BOTTOM = "USER";
//
//      public static final String RQ_LAST_NAME = "last_name";
//      public static final String RQ_FIRST_NAME = "first_name";
//      public static final String RQ_IDENTITY_ID = "identity_id";
//      public static final String RQ_CREDENTIALS = "credentials";

    // attribute to each instance
    // fieldList inherited from parent.
//    private int userId;
//    private String password;
    
    static {
        QueueDeleteSpreadsheetRowVO.setColumnSize(COLUMNS);
    }

    /**
     * Initialize the list of Field. Specifies the Field name, type, 
     * column position, max size, and if it's a required field.
     */
    public QueueDeleteSpreadsheetRowVO() 
    {
        // create fieldList if it's null.
        super();
        fieldList.add(new Field(FIELD_NAME_MESSAGE_SYSTEM, Field.TYPE_STRING, 0, 50, true));
        fieldList.add(new Field(FIELD_NAME_MESSAGE_ID, Field.TYPE_LONG, 1, 22, true));
    }
    
    public void validate() throws Exception {
    
        // Get list of all fields.
        for (int i = 0; i < fieldList.size(); i++) {
            Field field = (Field)fieldList.get(i);
            boolean isRequired = field.isRequired();
            int maxSize = field.getMaxSize();
            String value = field.getValue();
            String type = field.getType();
            String columnName = field.getName();
            
            
            // For each field, validate the following:
            // If the field is required, it's value cannot be null.
            if (isRequired && (value == null || value.length() == 0)) {
                throw new ExcelProcessingException(ERROR_REQUIRED_FIELD_MISSING, getLineNumber()+1, columnName);             
            }
            // Field size cannot be greater than field max size.
            if (value != null && value.length() > maxSize) {
                throw new ExcelProcessingException(ERROR_DATA_TOO_LARGE, getLineNumber()+1, columnName);
            }
            if (type.equals(Field.TYPE_LONG)) {
                try {
                    Long.parseLong(value);
                }
                catch (Throwable e) {
                    throw new ExcelProcessingException(ERROR_INVALID_NUMERIC, getLineNumber()+1, columnName);
                }
            }
            
        }
    }

//    public int getUserId() {
//        return userId;
//    }
//
//    public void setUserId(int arg0) {
//        userId = arg0;
//    }
//
//    public String getPassword() {
//        return password;
//    }
//
//    public void setPassword(String arg0) {
//        password = arg0;
//    }

       /**
        * Returns document for displayAdd. FIELDS/FIELD (@fieldname, @value)
        */
//        public Document getFieldDoc() throws Exception {        
//            Document doc = XMLHelper.getDocumentWithRoot(SecurityAdminConstants.XML_FIELD_TOP);
//
//            // Field firstName
//            Element firstNameElem = doc.createElement(SecurityAdminConstants.XML_FIELD_BOTTOM);
//            firstNameElem.setAttribute(SecurityAdminConstants.XML_ATTR_FIELD, RQ_FIRST_NAME);
//            firstNameElem.setAttribute(SecurityAdminConstants.XML_ATTR_VALUE, 
//                                       this.getFieldByName(FIRST_NAME).getValue());
//            doc.getDocumentElement().appendChild(firstNameElem);
//
//            // Field lastName
//            Element lastNameElem = doc.createElement(SecurityAdminConstants.XML_FIELD_BOTTOM);
//            lastNameElem.setAttribute(SecurityAdminConstants.XML_ATTR_FIELD, RQ_LAST_NAME);
//            lastNameElem.setAttribute(SecurityAdminConstants.XML_ATTR_VALUE, 
//                                      this.getFieldByName(LAST_NAME).getValue());
//            doc.getDocumentElement().appendChild(lastNameElem);
//            
//            // Field lastName
//            Element identityElem = doc.createElement(SecurityAdminConstants.XML_FIELD_BOTTOM);
//            identityElem.setAttribute(SecurityAdminConstants.XML_ATTR_FIELD, RQ_IDENTITY_ID);
//            identityElem.setAttribute(SecurityAdminConstants.XML_ATTR_VALUE, 
//                                      this.getFieldByName(IDENTITY_ID).getValue());
//            doc.getDocumentElement().appendChild(identityElem);
//            
//            // Credentials
//            Element credentialElem = doc.createElement(SecurityAdminConstants.XML_FIELD_BOTTOM);
//            credentialElem.setAttribute(SecurityAdminConstants.XML_ATTR_FIELD, RQ_CREDENTIALS);
//            credentialElem.setAttribute(SecurityAdminConstants.XML_ATTR_VALUE, getPassword());
//            doc.getDocumentElement().appendChild(credentialElem);
//            
//            return doc;
//        }
}
