package com.ftd.messaging.form;

import com.ftd.messaging.vo.QueueDeleteHeaderStatusVO;

import java.util.ArrayList;

import java.util.List;

import org.apache.struts.action.ActionForm;

/**
 * QueueMessageProcessingForm is a ActionForm class for QueueMessageProcessingStatus screen.
 * @author Vinay Shivaswamy
 * @version 1.0 for MQD FIX_4_7_0
 * @date 12/13/2010
 */
public class QueueMessageProcessingForm extends ActionForm{
    public QueueMessageProcessingForm() {
    }
    
    private List<QueueDeleteHeaderStatusVO> headers;
    private ArrayList<String> users;
    private String selectedUser;
    private int pageSize;
    private boolean queueDeleteManager;
    
    public void setHeaders(List<QueueDeleteHeaderStatusVO> headers) {
        this.headers = headers;
    }

    public List<QueueDeleteHeaderStatusVO> getHeaders() {
        return headers;
    }

    public void setUsers(ArrayList<String> users) {
        this.users = users;
    }

    public ArrayList<String> getUsers() {
        return users;
    }
    
    public void setSelectedUser(String selectedUser) {
        this.selectedUser = selectedUser;
    }

    public String getSelectedUser() {
        return selectedUser;
    }
    
    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setQueueDeleteManager(boolean queueDeleteManager) {
        this.queueDeleteManager = queueDeleteManager;
    }

    public boolean isQueueDeleteManager() {
        return queueDeleteManager;
    }

    
}
