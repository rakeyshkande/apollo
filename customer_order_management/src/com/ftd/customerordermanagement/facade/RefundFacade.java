package com.ftd.customerordermanagement.facade;

import com.ftd.customerordermanagement.to.FullRefundTO;
import com.ftd.customerordermanagement.bo.RefundAPIBO;
import com.ftd.customerordermanagement.bo.RefundBO;
import java.sql.Connection;

/**
 * RefundFacade
 * 
 * This is a facade between RefundBO and the RefundAPIBean
 * to create single calls for API use
 */

public class RefundFacade  {
  private Connection conn;

  public RefundFacade(Connection conn) {
    this.conn = conn;
  }


  /**
   * This method wraps the posting of full refund messages into a single 
   * call for API use
   * 
   * @param to FullRefundTO
   * @deprecated Use {@link RefundAPIBO#postFullRemainingRefund(String, String, String, String, String, Connection)} instead 
   */
  public void postFullRemainingRefund(FullRefundTO to) throws Exception
  {
    RefundAPIBO refund = new RefundAPIBO();
    refund.postFullRemainingRefund(to.getOrderDetailId(), 
                                   to.getRefundDispCode(),
                                   to.getResponsibleParty(),
                                   to.getRefundStatus(), to.getUserId(), conn);
  }

}