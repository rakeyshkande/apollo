package com.ftd.customerordermanagement.util;

import java.util.Comparator;

public class ShipMethodSortDescription implements Comparator
{
  public int compare(Object origMethod, Object newMethod) 
  {
    String origDescription = ((ShipMethod) origMethod).getDescription().toUpperCase();
    String newDescription = ((ShipMethod) newMethod).getDescription().toUpperCase();

    return origDescription.compareTo(newDescription);

  }
}
