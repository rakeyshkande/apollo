package com.ftd.customerordermanagement.util;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.commons.collections.Predicate;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang.time.DateUtils;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.util.functors.AllDaysBlockedViaCountryMaintenancePredicate;
import com.ftd.customerordermanagement.util.functors.IsDayBlockedViaCountryMaintenacePredicate;
import com.ftd.ftdutilities.DeliveryDateUTIL;
import com.ftd.ftdutilities.FieldUtils;
import com.ftd.ftdutilities.GeneralConstants;
import com.ftd.ftdutilities.OEDeliveryDate;
import com.ftd.ftdutilities.OEDeliveryDateParm;
import com.ftd.ftdutilities.OEParameters;
import com.ftd.mo.util.exceptions.DeliveryDateException;
import com.ftd.mo.vo.DeliveryDateVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.PASServiceUtil;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.cacheMgr.handlers.CountryMasterHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.CountryMasterVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.order.vo.RecipientAddressesVO;
import com.ftd.osp.utilities.order.vo.RecipientsVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.JAXPUtil;
import com.ftd.pas.core.domain.ProductAvailVO;

/**
 * COMDeliveryDateUtil generates COM and OE/Scrub delivery dates.
 * 
 * @author Mark Moon, Software Architects Inc.
 * @see com.ftd.ftdutilities.DeliveryDateUTIL
 */
public class COMDeliveryDateUtil {

  public static boolean MIN_DAYS_OUT = true;
  public static boolean MAX_DAYS_OUT = false;

  private static Logger logger = new Logger(COMDeliveryDateUtil.class.getName());

  private boolean requestedDeliveryDateFound;
  private boolean retrievedMax;
  private boolean hasCutoffPassed; 
  private String dateFormat; 
  private Connection connection;
  private OEDeliveryDateParm oeDeliveryDateParms;
  private DeliveryDateParameters parameters;
  private DeliveryDateConfiguration configuration;

  /**
   * Default constructor.
   */
  public COMDeliveryDateUtil() {
    super(); 
  }


  /**
   * Returns a Document of delivery date data using the supplied parameters.
   * The XML format for COM delivery dates is:
   * 
   * <deliveryDates>
   *   <deliveryDate>
   *     <startDate></startDate>
   *     <endDate></endDate>
   *     <displayDate></displayDate>
   *     <isRange></isRange>
   *   </deliveryDate>
   * </deliveryDates>
   * 
   * The flags the effect COM delivery dates include:
   * 
   *   Flag             Effect
   * - showDateRanges:  If true date ranges will be retrieved from the database (FTD_APPS.DELIVERY_DATE_RANGE)
   *                    and included in the returned List.
   * 
   * - checkCutoff:     If true and if today has passed the cutoff date for this day of the week, the next day will 
   *                    become the start date.  Example: Tuesday's cutoff is 2:00pm, if it is Tuesday 
   *                    at 3:00pm, Wednesday will become the start of the list rather than Tuesday.
   * 
   * - checkBlockedDatesViaCountryMaintenance: If true, values from FTD_APPS.COUNTRY_MASTER.*_CLOSED will be used to check
   *                    if each day of the week is closed or not.  Example:  If COUNTRY_MASTER.MONDAY_CLOSED = 'F'
   *                    all Monday's will be omitted from the List.
   * 
   * - showDateRangeIfRangeStartDateBeforeRange: If true, the date range will be considered in range 
   *                    if the range start date is less than minDate.  
   * 
   * - rangeBeforeOrAfter: if BEFORE(true) date ranges will be placed before the start of the date range
   *                    if AFTER(false) date ranges will be placed after the end of the date range
   * 
   * - adjustDateRangeIfRangeStartDateBeforeRange: If true, the date range start date will be adjusted to today.
   *                    If the resulting adjustment results in the date range having the same start and end date
   *                    the date range will not be included.
   * 
   * The XML format for OE delivery dates is:
   * 
   * 
   * @param parms
   * @return Document of delivery date data
   * @throws java.io.IOException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws org.xml.sax.SAXException
   * @throws java.sql.SQLException
   * @throws java.text.ParseException
   * @throws com.ftd.customerordermanagement.exceptions.DeliveryDateException if all countries are blocked via country maintenance
   * @see DeliveryDateVO
   * @see DeliveryDateConfiguration
   * @return 
   * @throws java.lang.Exception
   */
  public Document getXmlDeliveryDates() 
      throws Exception, IOException, ParserConfigurationException, SAXException, SQLException, ParseException, DeliveryDateException {

    Document document;

    try {
      connection = getConnection();

      oeDeliveryDateParms = getOEParameters(connection);
  
      // florist items use COMDeliveryDateUtil
      if ( parameters.isFlorist() ) {
        ensureConfigurationIsSet();  // ensures a DeliveryDateConfiguration is created
        document = getOEXmlDeliveryDates(connection);            

      }
  
      // vendor item use ftdutilities DeliveryDateUTIL delivery dates
      else {
        document = getOEXmlDeliveryDates(connection);
      }
    }

    // close the connection
    finally {
      try {
        if( connection != null ) {
          connection.close();
          connection = null;
        }
      }
      catch( Exception e ) {
        logger.error(e);
      }
    }
    return document;
  }

   /** 
   * The XML format for Comp Vendor delivery dates.  This is vendor delivery dates
   * for the minimum number of days.
   * 
   * 
   * @param parms
   * @return Document of delivery date data
   * @throws java.io.IOException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws org.xml.sax.SAXException
   * @throws java.sql.SQLException
   * @throws java.text.ParseException
   * @throws com.ftd.customerordermanagement.exceptions.DeliveryDateException if all countries are blocked via country maintenance
   * @see DeliveryDateVO
   * @see DeliveryDateConfiguration
   * @return 
   * @throws java.lang.Exception
   */
public Document getXmlCompVendorDeliveryDates() 
      throws Exception, IOException, ParserConfigurationException, SAXException, SQLException, ParseException, DeliveryDateException {

    Document document;

    try {
      connection = getConnection(); 
      
      oeDeliveryDateParms = getOEParameters(connection);
  
      document = getOEXmlDeliveryDates(connection);
    }

    // close the connection
    finally {
      try {
        if( connection != null ) {
          connection.close();
          connection = null;
        }
      }
      catch( Exception e ) {
        logger.error(e);
      }
    }
    return document;
  }

  /**
   *
   * @param oeDeliveryDateParms
   */
  public void setOEParameters(OEDeliveryDateParm oeDeliveryDateParms) {
    this.oeDeliveryDateParms = oeDeliveryDateParms;
  }


  /*
   * This method obtains an instance of OEDeliveryDateParm.
   * @return OEDeliveryDateParm
   */
  public OEDeliveryDateParm getOEParameters(Connection con) 
      throws Exception, IOException, ParserConfigurationException, SAXException, SQLException, ParseException {

    // only get parms if they haven't been retrieved
    if ( oeDeliveryDateParms == null ) {

        oeDeliveryDateParms = DeliveryDateUTIL.getCOMParameterData(
                        parameters.getRecipientCountry(),
                        parameters.getProductId(),
                        parameters.getProductSubCodeId(),
                        parameters.getRecipientZipCode(),
                        parameters.getCanadaCountryCode(),
                        parameters.getUsCountryCode(),
                        "1",
                        parameters.getOccasionId(),
                        con,
                        parameters.getRecipientState(),
                        parameters.getOrderOrigin(),
                        parameters.isFlorist(),
                        parameters.getMinDaysOut()
                      );
    }

    return oeDeliveryDateParms;
  }


  /*
   * Returns COM delivery dates.
   * @return Document of COM delivery dates
   * @throws com.ftd.customerordermanagement.exceptions.DeliveryDateException if all countries are blocked via country maintenance
   * @throws java.lang.Exception
   */
 /* private Document getCOMXmlDeliveryDates() 
      throws IOException, ParserConfigurationException, SAXException, SQLException, ParseException, DeliveryDateException, CacheException {

    List dates = getDeliveryDates();
    SimpleDateFormat formatter = new SimpleDateFormat( configuration.getSubmitDateFormat() );

    Document document = DOMUtil.getDefaultDocument();
    Element deliveryDates = (Element) document.createElement("deliveryDates");
    document.appendChild(deliveryDates);

    DeliveryDateVO date;
    Element element, node;
		SimpleDateFormat sdfDate = new SimpleDateFormat("MM/dd/yyyy");
		SimpleDateFormat sdfDay = new SimpleDateFormat("EEE");

    for(Iterator i = dates.iterator(); i.hasNext(); ) {
      date = (DeliveryDateVO)i.next();

      String startDate  = "";
      String startDay   = "";
      String endDate    = "";
      String endDay     = "";
      
      if (date.getStartDate() != null)
      {
        startDate = sdfDate.format(date.getStartDate());
        startDay  = sdfDay.format(date.getStartDate());
      }

      if (date.getEndDate() != null)
      {
        endDate = sdfDate.format(date.getEndDate());
        endDay  = sdfDay.format(date.getEndDate());
      }
      
      //create the main element
      element = (Element)document.createElement("deliveryDate");

      //create the startDate element
      node = (Element)document.createElement("startDate");
      node.appendChild(document.createTextNode(startDate) );
      element.appendChild(node);

      //create the startDate element
      node = (Element)document.createElement("startDay");
      node.appendChild(document.createTextNode(startDay));
      element.appendChild(node);

      //create the endDate element
      node = (Element)document.createElement("endDate");
      node.appendChild(document.createTextNode(endDate));
      element.appendChild(node);

      //create the endDay element
      node = (Element)document.createElement("endDay");
      node.appendChild(document.createTextNode(endDay));
      element.appendChild(node);

      //create the displayDate element
      node = (Element)document.createElement("displayDate");
      node.appendChild(document.createTextNode( date.getDisplayDate() ));
      element.appendChild(node);

      //create the isRange element
      node = (Element)document.createElement("isRange");
      node.appendChild(document.createTextNode( date.isRange()+"" ));
      element.appendChild(node);

      deliveryDates.appendChild(element);
    }
      
    return document;
  }*/

  /*
   * Returns OE delivery dates.
   * @return Document OE delivery dates
   * @throws com.ftd.customerordermanagement.exceptions.DeliveryDateException if all countries are blocked via country maintenance
   * @throws java.lang.Exception
   * @see com.ftd.ftdutilities.DeliveryDateUTIL
   */
  @SuppressWarnings({ "rawtypes", "unchecked" })
private Document getOEXmlDeliveryDates(Connection con) 
      throws ParserConfigurationException, SAXException, IOException, TransformerException, 
      ParseException, java.lang.Exception
  {
    //decide how many delivery dates to retrieve
    // if the cutoff day has passed, increase the limit by one to include one more day
    int limit = 0;
    if ( parameters.getMinDaysOut() ) {
      limit = oeDeliveryDateParms.getGlobalParms().getDeliveryDaysOutMin();
    }
    else {
      limit = oeDeliveryDateParms.getGlobalParms().getDeliveryDaysOutMax();
    }
    if ( !hasCutoffPassed ) {
      limit++;
    }
    logger.info("limit: " + limit + " " + parameters.getMinDaysOut());
    List addonIds = new ArrayList();
    if (parameters.getAddons() != null && parameters.getAddons().length() > 0) {
    	addonIds = Arrays.asList(parameters.getAddons().split(","));
    }     
    
    ProductAvailVO[] availableDates = null;
    String recipientCountry = parameters.getRecipientCountry();
    String recipientZip = parameters.getRecipientZipCode();
    String sourceCode = parameters.getSourceCode();
    if (recipientZip == null) {
    	recipientZip = "";
    }
    logger.info(recipientCountry + " " + recipientZip);
    if (recipientCountry != null && (recipientCountry.equalsIgnoreCase("US") ||
    		recipientCountry.equalsIgnoreCase("CA"))) {    	 
    	List<ProductAvailVO> productAvailVOs = PASServiceUtil.getProductAvailableDates(parameters.getProductId(), addonIds, recipientZip, recipientCountry, sourceCode, limit);
        if(productAvailVOs != null && productAvailVOs.size() > 0) {
        	availableDates = productAvailVOs.toArray(new ProductAvailVO[productAvailVOs.size()]);
		}
    } else { 
    	availableDates = PASServiceUtil.getInternationalProductAvailableDates(parameters.getProductId(), recipientCountry, limit);
    }
    List dates = PASServiceUtil.getOEDeliveryDates(availableDates);  
    logger.info("dates size: " + dates.size());
    
    //if floral get data ranges
    if(parameters.isFlorist()) {
        dates = populateDateRanges(dates);        
    } 
    
    Document formattedDeliveryDates = null;
    BasePageBuilder bpb = new BasePageBuilder();
    if(parameters.isFlorist())
    {
        formattedDeliveryDates = bpb.formatFloristDeliveryDates(configuration,dates);            
    }
    else
    {
        DeliveryDateUTIL dateUtil = new DeliveryDateUTIL();
        Document unformattedDeliveryDates = dateUtil.getXMLEncodedDeliveryDates( dates, oeDeliveryDateParms );
        formattedDeliveryDates = bpb.formatVendorDeliveryDates(unformattedDeliveryDates);    
    }
    logger.info(JAXPUtil.toString(formattedDeliveryDates));
    
    return formattedDeliveryDates;
  }

  @SuppressWarnings("rawtypes")
private List populateDateRanges(List dates)  throws Exception
  {
    // include date ranges if true
    List dateRanges;
    DeliveryDateVO date;
    if ( configuration.isShowDateRanges() ) {  //default is true
      // get all date ranges from database
      dateRanges = getDateRanges(dates);

      // loop through all date ranges, merging each if it falls within today and daysOut
      for ( Iterator iter = dateRanges.iterator(); iter.hasNext(); ) {
        date = (DeliveryDateVO)iter.next();

        // does this date range fall within today and daysOut
        if ( isDateInRange(dates, date) ) {
          mergeDateRange( dates, date );
        }
        
        //remove any dates from the list that part of a date range
        Iterator dateIter = dates.iterator();
        while(dateIter.hasNext())
        {
            Object dateObj = dateIter.next();
        
            //only check if object is OE date object.  All other objects are ranges and not need be removed
            if(dateObj instanceof OEDeliveryDate)
            {
                OEDeliveryDate delDate = (OEDeliveryDate)dateObj;
                Date deliveryDate = FieldUtils.formatStringToUtilDate(delDate.getDeliveryDate());
                if(deliveryDate.compareTo(date.getStartDate()) >= 0 &&
                    deliveryDate.compareTo(date.getEndDate()) <= 0)
                    {
                        dateIter.remove();
                    }                
            }            
        }
                
      }
    }    
    
    return dates;
  }



  /**
   * Returns a List of DeliveryDateVO objects using the supplied parameters.
   * Flags that effect this method include:
   * 
   *   Flag             Effect
   * - showDateRanges:  If true date ranges will be retrieved from the database (FTD_APPS.DELIVERY_DATE_RANGE)
   *                    and included in the returned List.
   *
   * - checkCutoff:     If true and if today has passed the cutoff date for this day of the week, the next day will 
   *                    become the start date.  Example: Tuesday's cutoff is 2:00pm, if it is Tuesday 
   *                    at 3:00pm, Wednesday will become the start of the list rather than Tuesday.
   *
   * - checkBlockedDatesViaCountryMaintenance: If true, values from FTD_APPS.COUNTRY_MASTER.*_CLOSED will be used to check
   *                    if each day of the week is closed or not.  Example:  If COUNTRY_MASTER.MONDAY_CLOSED = 'F'
   *                    all Monday's will be omitted from the List.
   *
   * - showDateRangeIfRangeStartDateBeforeRange: If true, the date range will be considered in range 
   *                    if the range start date is less than minDate.  
   *
   * - rangeBeforeOrAfter: if BEFORE(true) date ranges will be placed before the start of the date range
   *                    if AFTER(false) date ranges will be placed after the end of the date range
   *
   * - adjustDateRangeIfRangeStartDateBeforeRange: If true, the date range start date will be adjusted to today.
   *                    If the resulting adjustment results in the date range having the same start and end date
   *                    the date range will not be included.
   *
   * @param parms
   * @return List of DeliveryDateVO objects
   * @throws java.io.IOException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws org.xml.sax.SAXException
   * @throws java.sql.SQLException
   * @throws java.text.ParseException
   * @throws com.ftd.customerordermanagement.exceptions.DeliveryDateException if all countries are blocked via country maintenance
 * @throws CacheException 
   * @see DeliveryDateConfiguration
   */
  @SuppressWarnings("rawtypes")
public List getDeliveryDates() 
      throws IOException, ParserConfigurationException, SAXException, SQLException, ParseException, DeliveryDateException, CacheException {

    // ensures a DeliveryDateConfiguration is created
    ensureConfigurationIsSet();

    // get consecutive days from today through daysOut
    List dates = getConsecutiveDays();

    // include date ranges if true
    List dateRanges;
    DeliveryDateVO date;
    if ( configuration.isShowDateRanges() ) {  //default is true
      // get all date ranges from database
      dateRanges = getDateRanges(dates);

      // loop through all date ranges, merging each if it falls within today and daysOut
      for ( Iterator iter = dateRanges.iterator(); iter.hasNext(); ) {
        date = (DeliveryDateVO)iter.next();

        // does this date range fall within today and daysOut
        if ( isDateInRange(dates, date) ) {
          mergeDateRange( dates, date );
        }
        
        //remove any dates from the list that part of a date range
        Iterator dateIter = dates.iterator();
        while(dateIter.hasNext())
        {
            DeliveryDateVO delDate = (DeliveryDateVO)dateIter.next();
            if(!delDate.isRange() &&  delDate.getStartDate().compareTo(date.getStartDate()) >= 0 &&
                delDate.getStartDate().compareTo(date.getEndDate()) <= 0)
                {
                    dateIter.remove();
                }
            
        }
                
      }
    }
    
    //remove any dates that fall within 

    return dates;
  }

  /* 
   * Returns a List of DeliveryDateVO objects from today through daysOut.  
   * Flags that effect the returned List include:
   * 
   *   Flag         Effect
   * - checkCutoff: If true and if today has passed the cutoff date for this day of the week, the next day will 
   *                become the start date.  Example: Tuesday's cutoff is 2:00pm, if it is Tuesday 
   *                at 3:00pm, Wednesday will become the start of the list rather than Tuesday.
   *
   * - checkBlockedDatesViaCountryMaintenance: If true, values from FTD_APPS.COUNTRY_MASTER.*DAY_CLOSED will be used to check
   *                if each day of the week is closed or not.  Example:  If COUNTRY_MASTER.MONDAY_CLOSED = 'F' || 'H'
   *                all Monday's will be omitted from the List.
   * 
   * 
   * @throws com.ftd.customerordermanagement.exceptions.DeliveryDateException if all countries are blocked via country maintenance
   * @return List DeliveryDateVO objects
   */
  @SuppressWarnings({ "rawtypes", "unchecked" })
private List getConsecutiveDays() 
      throws SAXException, ParserConfigurationException, IOException, SQLException, DeliveryDateException, CacheException 
  {

    // if blocking days via country maintenance, get CountryVO
    CountryMasterVO countryVO;
    Predicate isBlockedPredicate = null;
    if ( configuration.isCheckBlockedDatesViaCountryMaintenance() ) { // default is true
      countryVO = getCountryVOById();
      if ( countryVO != null ) {
        isBlockedPredicate = IsDayBlockedViaCountryMaintenacePredicate.getInstance( countryVO );
        
        // if all days are blocked, throw exception
        // this is not likely, but the business asked for a message to be displayed if this were to occur
        if ( AllDaysBlockedViaCountryMaintenancePredicate.getInstance().evaluate(countryVO) ) {
          logger.error("All days blocked via country maintenance for country: " + countryVO.getCountryId());
          throw new DeliveryDateException("All days blocked via country maintenance for country: " + countryVO.getCountryId());
        }
      }

      // country could not be found, log error and turn off country maintenance date blocking
      else {
        logger.error("CountryVO could not be obtained to check for blocked delivery dates, turning off country maintenance date blocking.");
        configuration.setCheckBlockedDatesViaCountryMaintenance( false );
      }
    }

    HashMap holidayDates = null;
    // retrieve the holiday dates for the selected country
    if ( oeDeliveryDateParms.getHolidayDates() != null )
    {
      holidayDates = oeDeliveryDateParms.getHolidayDates();
    }



    // check cutoff day if flag is set, otherwise use today
    Calendar currentDay;
    Calendar maxDay;
    if ( configuration.isCheckCutoff() ) {  // default is true
      currentDay = getCutoffDate();
      currentDay = getStartDate( currentDay );
    }
    else {
      currentDay = Calendar.getInstance();
    }

    // ignore the time, just use the month, day, year
    currentDay.setTime( DateUtils.truncate( currentDay.getTime(), Calendar.DATE ) );

    // if the requested delivery date should be attempted to be placed in the list of dates,
    // determine if Requested Delivery Date is before the start date of the list
    Calendar requestedDate = null;
    boolean reqDeliveryDateBeforeStartDate = true;
    if ( configuration.isIncludeRequestedDeliveryDate() && parameters.getRequestedDeliveryDate() != null ) {
      requestedDate = parameters.getRequestedDeliveryDate();
      requestedDate.setTime( DateUtils.truncate( requestedDate.getTime(), Calendar.DATE ) );
      reqDeliveryDateBeforeStartDate = requestedDate.before(currentDay) || requestedDate.equals(currentDay);
    }

    // fill dates util it contains 'daysOut' days
    // if the cutoff day has passed, increase the limit by one to include one more day
    int limit = 0;
    if ( parameters.getMinDaysOut() ) {
      limit = oeDeliveryDateParms.getGlobalParms().getDeliveryDaysOutMin();
    }
    else {
      limit = oeDeliveryDateParms.getGlobalParms().getDeliveryDaysOutMax();
    }
    if ( !hasCutoffPassed ) {
      limit++;
    }
    List dates = new ArrayList( limit );

    currentDay.add( Calendar.DATE, -1 );

    maxDay = Calendar.getInstance();
    maxDay.setTime(currentDay.getTime());
    maxDay.add(Calendar.DATE, limit);

    DeliveryDateVO deliveryDateVO;
    Object obj = null;

    SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");

    //get delievery date info from DeliveryDate Util
  //  internalGetOEParameters


    while ( getDiffInDays(currentDay.getTime(), maxDay.getTime()) > 0 ) 
    {
      currentDay.add( Calendar.DATE, 1 );
      String sCurrentDay = sdf.format(currentDay.getTime());

      if (holidayDates != null)
      {
        obj = holidayDates.get(sCurrentDay);
      }

      // if blocking days via country maintenance, check if the currentDay is blocked
      // this flag, if true will be set to false if all days are turned off for a country
      // or if the CountryVO object was null upon retrieval
      if ( configuration.isCheckBlockedDatesViaCountryMaintenance() )  // default is true
      {
        if ( isBlockedPredicate.evaluate(currentDay) || obj != null ) 
        {
          continue;
        }
      }

      deliveryDateVO = new DeliveryDateVO();
      deliveryDateVO.setStartDate( currentDay.getTime() );
      deliveryDateVO.setEndDate( currentDay.getTime() );
      deliveryDateVO.setRange( false );
      deliveryDateVO.setDateFormat( dateFormat );
      dates.add( deliveryDateVO );
      
      // if the requested delivery date should always be included in the returned list, 
      // check to see if it matches the current day being added to the list
      // if the requested delivery date is not found within start date and min days out, retrieve max days out and continue checking
      // if the requested delivery date is still not found, it must be added to the list after completing this loop
      if ( configuration.isIncludeRequestedDeliveryDate() && parameters.getRequestedDeliveryDate() != null && !reqDeliveryDateBeforeStartDate && !requestedDeliveryDateFound ) 
      {
        SimpleDateFormat parser = new SimpleDateFormat("MM/dd/yyyy");
        requestedDeliveryDateFound = parser.format(requestedDate.getTime()).equalsIgnoreCase(parser.format(currentDay.getTime()));
        if ( !requestedDeliveryDateFound && !retrievedMax && (dates.size() == (limit - 1)) ) 
        {
          if ( logger.isDebugEnabled() ) 
            logger.debug("Requested delivery date not found in min days out, checking max days out.");
          retrievedMax = true;
          limit += (oeDeliveryDateParms.getGlobalParms().getDeliveryDaysOutMax()-limit);
          maxDay.add(Calendar.DATE, limit);
        }
      }
    }

    // if the requested delivery date should always be included (DeliveryDateConfiguration.isIncludeRequestedDeliveryDate) in the returned 
    // list and it did not fall within start date and max days out, add the requested delivery date to the list
    if ( configuration.isIncludeRequestedDeliveryDate() && !requestedDeliveryDateFound && parameters.getRequestedDeliveryDate() != null && parameters.getRequestedDeliveryDate().getTime().after( ((DeliveryDateVO)dates.get(dates.size()-1)).getStartDate()) ) {
      if ( logger.isDebugEnabled() ) logger.debug("Requested delivery date not found in max days out, adding requested delivery date to end of list.");
      deliveryDateVO = new DeliveryDateVO();
      deliveryDateVO.setStartDate( parameters.getRequestedDeliveryDate().getTime() );
      if ( parameters.getRequestedDeliveryDateRangeEnd() != null ){
        deliveryDateVO.setEndDate( parameters.getRequestedDeliveryDateRangeEnd().getTime() );
        deliveryDateVO.setRange( !parameters.getRequestedDeliveryDate().equals(parameters.getRequestedDeliveryDateRangeEnd()) );
      }
      else {
        deliveryDateVO.setEndDate( parameters.getRequestedDeliveryDate().getTime() );
        deliveryDateVO.setRange( false );        
      }
      deliveryDateVO.setDateFormat( dateFormat );
      dates.add( deliveryDateVO );
    }
    
    return dates;
  }

  /*
   * Returns today or today + 1 (tomorrow) if the cutoff time for today has passed.
   *
   * @param cutoffDate The cutoff date to compare with today
   * @return today or today + 1 if the cutoff time for today has passed
   */
  private Calendar getStartDate(Calendar cutoffDate) {

    // null check cutoffDate
    if ( cutoffDate == null ) {
      logger.error("Invalid parameter: cutoffDate='" + cutoffDate + "'  was null.");
      throw new IllegalArgumentException("Invalid parameter: cutoffDate='" + cutoffDate + "'  was null.");
    }

    Calendar toReturn = Calendar.getInstance( cutoffDate.getTimeZone() );
    toReturn.clear(Calendar.HOUR_OF_DAY);

    // check if GNADD is on and active: GNADD active when not zero
    if ( StringUtils.equals( oeDeliveryDateParms.getZipCodeGNADDFlag(), "Y" ) && !StringUtils.equals( oeDeliveryDateParms.getGlobalParms().getGNADDLevel(), "0" )) {
      Calendar gnadd = Calendar.getInstance();
      gnadd.setTime( oeDeliveryDateParms.getGlobalParms().getGNADDDate() );
      toReturn.set( Calendar.MONTH, gnadd.get( Calendar.MONTH) );
      toReturn.set( Calendar.DATE, gnadd.get( Calendar.DATE) );
      toReturn.set( Calendar.YEAR, gnadd.get( Calendar.YEAR) );
    }
    // if the current date/time is past the cutoff date/time return the next day
    else if ( toReturn.after(cutoffDate) ) {
      cutoffDate.add( Calendar.DATE, 1 );
      toReturn = cutoffDate;
      hasCutoffPassed = true;
      if ( logger.isDebugEnabled() ) logger.debug("Cutoff time has passed, using " + toReturn.getTime() + " as the start date.");
    }
    else {
      // do nothing, return today
      if ( logger.isDebugEnabled() ) logger.debug("Cutoff time has NOT passed, using " + toReturn.getTime() + " as the start date.");
    }

    return toReturn;
  }

  /*
   * Gets the cutoff time from the DB for the day of the week.
   * @return The cutoff date for today using the cutoff times from global params
   */
  public Calendar getCutoffDate() {
    Calendar toReturn = Calendar.getInstance();
    OEParameters globalParms = oeDeliveryDateParms.getGlobalParms();

    // get the cutoff time for today, format from DB: '1400'
    String cutoffTime = "";

    switch ( toReturn.get( Calendar.DAY_OF_WEEK ) ) 
    {
      case Calendar.SUNDAY:
        cutoffTime = globalParms.getSundayCutoff();
        break;
      case Calendar.MONDAY:
        cutoffTime = globalParms.getMondayCutoff();
        break;
      case Calendar.TUESDAY:
        cutoffTime = globalParms.getTuesdayCutoff();
        break;
      case Calendar.WEDNESDAY:
        cutoffTime = globalParms.getWednesdayCutoff();
        break;
      case Calendar.THURSDAY:
        cutoffTime = globalParms.getThursdayCutoff();
        break;
      case Calendar.FRIDAY:
        cutoffTime = globalParms.getFridayCutoff();
        break;
      case Calendar.SATURDAY:
        cutoffTime = globalParms.getSaturdayCutoff();
        break;
    }

    //get cutoff for product type
    String typeCutoffTime = null;
    if ( StringUtils.equals(oeDeliveryDateParms.getProductSubType(), GeneralConstants.OE_PRODUCT_SUBTYPE_EXOTIC) ) {
      typeCutoffTime = globalParms.getExoticCutoff();
    }
    else if ( StringUtils.equals(oeDeliveryDateParms.getProductType(), GeneralConstants.OE_PRODUCT_TYPE_SPEGFT)) {
      typeCutoffTime = globalParms.getSpecialtyGiftCutoff();
    }
    else if ( StringUtils.equals(oeDeliveryDateParms.getProductType(), GeneralConstants.OE_PRODUCT_TYPE_FRECUT) ) {
      typeCutoffTime = globalParms.getFreshCutCutoff();
    }
    else if ( StringUtils.equals(oeDeliveryDateParms.getProductType(), GeneralConstants.OE_PRODUCT_TYPE_SDG) ||
             StringUtils.equals(oeDeliveryDateParms.getProductType(), (GeneralConstants.OE_PRODUCT_TYPE_SDFC))) {
      typeCutoffTime = globalParms.getSpecialtyGiftCutoff();
    }

    //use product type cutoff time if it is sooner
    if ( StringUtils.isNotEmpty(cutoffTime) && StringUtils.isNotEmpty(typeCutoffTime) ){
      int typeCutoffTimeInt = Integer.parseInt(typeCutoffTime);
      int cutoffTimeInt = Integer.parseInt(cutoffTime);
      if ( typeCutoffTimeInt < cutoffTimeInt ){
          cutoffTime = typeCutoffTime;
      }
    }

    toReturn.setTime( parseCutoffTime( cutoffTime ).getTime() );
    
    return toReturn;
  }
  
  /**
   * Returns the current day with the supplied cutoff time.  If the value is non-numeric
   * zero will be used for the hours and the minutes.
   * @param cutoffTime
   * @return Calendar set with the cutoff time for the current day
   */
  public static Calendar parseCutoffTime(String cutoffTime) {
    Calendar toReturn = Calendar.getInstance();
    boolean isPM = false;

    // parse the cutoff time into hours and minutes
    int iCutoffTime = 0, cutoffHour = 0, cutoffMin = 0;
    if ( NumberUtils.isNumber(cutoffTime) ) {
      iCutoffTime = Integer.parseInt( cutoffTime );
      cutoffHour = iCutoffTime / 100;
      cutoffMin = iCutoffTime % 100;

      // because the time is stored in the DB in 24 hour time, a check needs to be done
      // to determine if the cutoff time is in the AM or PM
      if ( cutoffHour >= 12 ) {
        cutoffHour -= 12;
        isPM = true;
      }
    }
    else {
      logger.error("Cutoff time: '" + cutoffTime + "' was non-numeric, using defaults");
    }

    // set the cutoff hours, minutes and AM/PM for today
    toReturn.set( Calendar.HOUR, cutoffHour );
    toReturn.set( Calendar.MINUTE, cutoffMin );
    toReturn.set( Calendar.AM_PM, (isPM) ? Calendar.PM : Calendar.AM );

    if ( logger.isDebugEnabled() ) logger.debug("Returning cutoff time of: " + toReturn.getTime());
    return toReturn;
  }


  /*
   * Returns a List of DeliveryDateVO objects.
   * @return A List of DeliveryDateVO object representing date ranges.
   */
  @SuppressWarnings({ "rawtypes", "unchecked" })
private List getDateRanges(List validDates)  
      throws IOException, ParserConfigurationException, SAXException, SQLException, ParseException {

    List toReturn = new ArrayList();
    DataAccessUtil dau = DataAccessUtil.getInstance();
    DataRequest dataRequest = new DataRequest();
    dataRequest.setStatementID("GET_DELIVERY_DATE_RANGES_CSR");
    dataRequest.setConnection( connection );
    CachedResultSet rs = (CachedResultSet)dau.execute(dataRequest);
    dataRequest.reset();

    DeliveryDateVO date;
    SimpleDateFormat parser = new SimpleDateFormat("MM/dd/yyyy");
    while( rs.next() ) {
      date = new DeliveryDateVO();
      date.setStartDate( parser.parse(rs.getString(1)) );
      date.setEndDate( parser.parse(rs.getString(2)) );
      date.setRange( true );
      
      date = adjustRange(validDates,date);
      if(date != null)
      {
          toReturn.add(date );          
      }
      
    }

    return toReturn;
  }
  
  /*
   * Adjust date range based on available dates.
   * For example if the date range in the database is 4/27/06 - 4/29/06 and
   * 4/27 is not a valid date (e.g. country holiday) then the returned
   * date range will be 4/28/06 - 4/29/06.
   * 
   */
   @SuppressWarnings("rawtypes")
private DeliveryDateVO adjustRange(List validDates, DeliveryDateVO date)
   {
     
     boolean startFound = false;
     boolean endFound = false;     

     //search for start date
     Date checkStartDate = date.getStartDate();
     //keep looking until date found or start date is after the end date
     while(!startFound && checkStartDate.compareTo(date.getEndDate()) < 0)
     {
        //check if date is valid
        if(validDate(validDates,checkStartDate))
        {
            //fould valid start date
            date.setStartDate(checkStartDate);
            startFound = true;
        }         
        else
        {
            //move up one day
            Calendar cal = Calendar.getInstance();
            cal.setTime(checkStartDate);
            cal.add(Calendar.DAY_OF_MONTH,+1);
            checkStartDate = cal.getTime();            
        }
     }
     
     //only continue if start was found
     if(startFound)
     {
              
         //search for end date
         Date checkEndDate = date.getEndDate();
         //search until end date found or end date is before start date
         while(!endFound && checkEndDate.compareTo(date.getStartDate()) > 0)
         {
            //check if date is valid
            if(validDate(validDates,checkEndDate))
            {
                //valid date found
                date.setEndDate(checkEndDate);
                endFound = true;
            }         
            else
            {
                //go back one day
                Calendar cal = Calendar.getInstance();
                cal.setTime(checkEndDate);
                cal.add(Calendar.DAY_OF_MONTH,-1);
                checkEndDate = cal.getTime();
            }
         }         
     }
     
     //if date range cannot be used return null
     if(!startFound || !endFound)
     {
         date = null;
     }
     
     return date;
     
     
   }

   /*
    * Check if the passed in date is valid.  The passed in 
    * date is considered valid if it exist in the passed in array list.
    */
   @SuppressWarnings("rawtypes")
private boolean validDate(List validDates, Date date) 
   {
      boolean matchFound = false;
   
     Iterator iter = validDates.iterator();
     //end loop after all dates checked or when a match is found
     while(!matchFound && iter.hasNext())
     {
         OEDeliveryDate validOEDate = (OEDeliveryDate)iter.next();
         
         if(validOEDate.getDeliverableFlag().equalsIgnoreCase("Y"))
         {
             Date validDate = null;
             try
             {
                 validDate = FieldUtils.formatStringToUtilDate(validOEDate.getDeliveryDate());             
             }
             catch(Exception e)
             {
                 return false;
             }
    
             if( validDate.compareTo(date) == 0)
             {
                 matchFound = true;
             }             
         }
         

     }
     
     return matchFound;
   }
   
   
   
  

  /*
   * Returns true if the date range is within today and daysOut.  This method assumes
   * the List of dates is in consecutive order from today to today+N.
   *
   * @param dates consecutive List of DeliveryDateVOs from today through daysOut
   * @param dateRange DeliveryDateVO representing a date range (different start and end dates)
   * @return true if the date range is within today and daysOut
   */
  @SuppressWarnings("rawtypes")
private boolean isDateInRange(List dates, DeliveryDateVO dateRange) 
  {
    boolean toReturn = false;

    try
    {
        // null check dates, size check dates
        if ( dates != null && !dates.isEmpty() ) {
          OEDeliveryDate minDate = (OEDeliveryDate)dates.get( 0 );
          OEDeliveryDate maxDate = (OEDeliveryDate)dates.get( dates.size() - 1 );
          toReturn = isDateInRange(minDate, maxDate, dateRange);
        }
        else {
          logger.error("Supplied list was null or empty, returning false.");
        }        
    }
    catch(Exception e)
    {
       logger.error("Date range change check could not be done.");   
    }

    return toReturn;
  }

  /*
   * Returns true if the date range is within today and daysOut.  This method assumes
   * the List of dates are in consecutive order from today to today+N.  
   * Flags that effect this method include:
   * 
   *   Flag                                      Effect
   * - showDateRangeIfRangeStartDateBeforeRange: If true, the date range will be considered in range 
   *                                             if the range start date is less than minDate.
   * 
   * @param minDate The lower bound of the range
   * @param maxDate The upper bound of the range
   * @param dateRange A DeliveryDateVO with startDate and endDate representing a date range
   * @return true if the date range is within today and daysOut
   * @throws IllegalArgumentException if any of minDate, maxDate, or dateRange are null
   */
  private boolean isDateInRange(OEDeliveryDate minDate, OEDeliveryDate maxDate, DeliveryDateVO dateRange) throws Exception
  {
    // null check inputs
    if ( minDate == null || maxDate == null || dateRange == null ) 
    {
      throw new IllegalArgumentException("Invalid parameter: one or both of minDate='" + minDate + "', maxDate='" + maxDate + "', dateRange='" + dateRange + "' were null.");
    }

    Date minDateValue = FieldUtils.formatStringToUtilDate(minDate.getDeliveryDate());
    Date maxDateValue = FieldUtils.formatStringToUtilDate(maxDate.getDeliveryDate());

    return ( dateRange.getStartDate().getTime() >= minDateValue.getTime() &&
              dateRange.getEndDate().getTime() <= maxDateValue.getTime() ) ||
               ( configuration.isShowDateRangeIfRangeStartDateBeforeRange() &&
                  dateRange.getStartDate().getTime() < minDateValue.getTime() &&
                   dateRange.getEndDate().getTime() <= maxDateValue.getTime() );
  }


  /*
   * Places the DeliveryDateVO representing a date range in the List at the correct location.
   * Flags that effect this method include:
   * 
   *   Flag                Effect
   * - rangeBeforeOrAfter: if BEFORE(true) date ranges will be placed before the start of the date range
   *                       if AFTER(false) date ranges will be placed after the end of the date range
   *
   * - adjustDateRangeIfRangeStartDateBeforeRange: If true, the date range start date will be adjusted to today.
   *                       If the resulting adjustment results in the date range having the same start and end date
   *                       the date range will not be included.
   * 
   * @param days List of DeliveryDateVO objects in consecutive order
   * @param date DeliveryDateVO representing a date range
   */
  @SuppressWarnings({ "rawtypes", "unchecked" })
private void mergeDateRange( List days, DeliveryDateVO date )  
  {

    try
    {
    int relativePosition = 0; 
    
    // null check days, size check days
    if ( days != null && !days.isEmpty() ) 
    {
      boolean add = true;
      int totalDays = days.size();
      int diffInDays = 0; 
      
      //determine the relative positioning as to where in the list should we put the date range. 
      while (relativePosition < totalDays)
      {
        Object dateObject = days.get(relativePosition);
        if(!(dateObject instanceof OEDeliveryDate))
        {
            //date ranges will be of a type DeliveryVO, we want to skip these
        }
        else
        {
            OEDeliveryDate oeDate = (OEDeliveryDate)days.get(relativePosition);
    
            Date delDate = FieldUtils.formatStringToUtilDate(oeDate.getDeliveryDate());
            diffInDays = getDiffInDays(delDate,date.getStartDate());
            if (diffInDays <= 0) 
            {
              break;
            }            
        }

        relativePosition++;

      }

      if (relativePosition == 0) 
      {
            Object dateObject = days.get(relativePosition);
            if(!(dateObject instanceof OEDeliveryDate))
            {
                //date ranges will be of a type DeliveryVO, we want to skip these
            }
            else
            {      
              OEDeliveryDate oeDate = (OEDeliveryDate)days.get(0);
              Date delDate = FieldUtils.formatStringToUtilDate(oeDate.getDeliveryDate());
              date.setStartDate(delDate);
    
              if (getDiffInDays(date.getStartDate(), date.getEndDate()) <= 0)
                add = false;
            }
      }

      // add the date range to the list of dates
      if (add)
        days.add( relativePosition, date );

    }        
    }
    catch(Exception e)
    {
        logger.error(e);
        logger.error("Date ranges cannot be merged.");
    }



  }

  /**
   * Returns the difference in days between the startDate and endDate.
   * 
   * @param startDate
   * @param endDate
   * @return The difference in days between the startDate and endDate
   * @throws IllegalArgumentException if either startDate or endDate are null
   */
  @SuppressWarnings("deprecation")
public static int getDiffInDays(Date startDate, Date endDate) {

    // null check inputs
    if ( startDate == null || endDate == null ) {
      throw new IllegalArgumentException("Invalid parameter: one or both of startDate='" + startDate + "', endDate='" + endDate + "' were null.");
    }

    long diffInMillis = endDate.getTime() - startDate.getTime();

    int diffInDays = (int)(diffInMillis / DateUtils.MILLIS_IN_DAY);

    return diffInDays;
  }

  /**
   * Checks if two calendar objects are on the same day ignoring time.
   *
   * @param cal1  the first calendar, not altered, not null
   * @param cal2  the second calendar, not altered, not null
   * @return true if they represent the same day
   * @throws IllegalArgumentException if either calendar is <code>null</code>
   */
  public static boolean isSameDay(Calendar cal1, Calendar cal2) {

    // null check inputs
    if (cal1 == null || cal2 == null) {
      throw new IllegalArgumentException("Invalid parameter: one or both of cal1='" + cal1 + "', cal2='" + cal2 + "' were null.");
    }

    return (cal1.get(Calendar.ERA) == cal2.get(Calendar.ERA) &&
            cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
            cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR));
  }

  /*
   * Retrieves a CountryVO based on the recipient's country. 
   *
   * @param parms
   * @return CountryVO based on the recipient's country
   * @throws org.xml.sax.SAXException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws java.io.IOException
   * @throws java.sql.SQLException
   */
  private CountryMasterVO getCountryVOById()
      throws SAXException, ParserConfigurationException, IOException, SQLException, CacheException {
 
    OrderVO order = oeDeliveryDateParms.getOrder();
    OrderDetailsVO item = (OrderDetailsVO)order.getOrderDetail().get(oeDeliveryDateParms.getItemNumber() - 1);
    RecipientsVO recipient = (RecipientsVO)item.getRecipients().get(0);
    RecipientAddressesVO recipAddress = (RecipientAddressesVO)recipient.getRecipientAddresses().get(0);

    // Try cache first
    CountryMasterHandler countryHandler = (CountryMasterHandler)CacheManager.getInstance().getHandler("CACHE_NAME_COUNTRY_MASTER");
    return countryHandler.getCountryById( recipAddress.getCountry() );
  }


  /*private TimeZone getTimeZone() {
    Calendar calendar = Calendar.getInstance();
    TimeZone tz = calendar.getTimeZone();
    TimeZone timeZone = null;

    // cutoff time for all Specialty Gift & Fresh Cut products is Central Time
    if ( (oeDeliveryDateParms.getProductType() != null) &&
         (oeDeliveryDateParms.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_FRECUT) ||
          oeDeliveryDateParms.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_SPEGFT)) ){
        timeZone = tz.getTimeZone("America/Chicago");
    }
    else if ( StringUtils.isNotEmpty(oeDeliveryDateParms.getZipTimeZone()) ) {
      switch ( Integer.parseInt(oeDeliveryDateParms.getZipTimeZone()) ) {
        case 0:     // international
          timeZone = tz.getTimeZone("America/Chicago");
          break;
        case 1:     // eastern
          timeZone = tz.getTimeZone("America/New_York");
          break;
        case 2:     // central
          timeZone = tz.getTimeZone("America/Chicago");
          break;
        case 3:     // mountain
          timeZone = tz.getTimeZone("America/Denver");
          break;
        case 4:     // pacific
          timeZone = tz.getTimeZone("America/Los_Angeles");
          break;
        case 5:     // hawaiian (alaska, hawaii)
          timeZone = tz.getTimeZone("HST");
          break;
        case 6:     // atlantic (puerto rico , virgin islands)
          timeZone = tz.getTimeZone("America/Puerto_Rico");
          break;
      }
    }
    else {
      timeZone = tz;
      if ( logger.isDebugEnabled() ) logger.debug("Using default Central Time TimeZone");
    }
    return timeZone;
  }*/


  /**
   * Sets the DeliveryDateConfiguration.
   * @param configuration
   */
  public void setConfiguration(DeliveryDateConfiguration configuration) {
    this.configuration = configuration;
  }

  /**
   * Returns the DeliveryDateConfiguration.
   * @return DeliveryDateConfiguration
   */
  public DeliveryDateConfiguration getConfiguration() {
    return configuration;
  }

  /**
   * Sets DeliveryDateParameters.
   * @param parameters DeliveryDateParameters
   */
  public void setParameters(DeliveryDateParameters parameters) {
    this.parameters = parameters;
  }

  /**
   * Return DeliveryDateParameters.
   * @return DeliveryDateParameters
   */
  public DeliveryDateParameters getParameters() {
    return parameters;
  }


  /*
   * Ensures that the DeliveryDateConfiguration has been set.  If a configuration
   * has not been set, a DeliveryDateConfiguration will be created using the defaults.
   * 
   */
  private void ensureConfigurationIsSet() {
    if ( configuration == null ) {
      configuration = new DeliveryDateConfiguration();
    }
  }

  /*
   * Returns a Connection
   * @return java.sql.Connection
   * @throws java.lang.Exception
   */
  private Connection getConnection() 
      throws Exception {

    if ( connection == null ) {
      connection = DataSourceUtil.getInstance().getConnection(
                  ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE,
                  COMConstants.DATASOURCE_NAME));
    }
    return connection;
  }

  /**
   * 
   * @return 
   */
  public boolean wasMaxRetrieved() {
    return retrievedMax;
  }
  
}