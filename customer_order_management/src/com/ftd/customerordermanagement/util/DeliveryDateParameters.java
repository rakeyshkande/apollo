package com.ftd.customerordermanagement.util;

import java.io.Serializable;
import java.sql.Connection;
import java.util.Calendar;

/**
 * DeliveryDateParameters wraps the parameters needed by the DeliveryDateUtil to calculate
 * delivery dates.
 *
 * @author Mark Moon, Software Architects Inc.
 */
public class DeliveryDateParameters 
    implements Serializable {

  private boolean isFlorist;
  private boolean minDaysOut;
  private String recipientState;
  private String recipientZipCode;
  private String recipientCountry;
  private String productId;
  private String occasionId;
  private String usCountryCode;
  private String canadaCountryCode;
  private String orderOrigin;
  private Calendar requestedDeliveryDate;
  private Calendar requestedDeliveryDateRangeEnd;
  private String productSubCodeId;
  private String addons;
  private String sourceCode;

  /**
   * Default constructor.
   */
  public DeliveryDateParameters() {
      super();
      usCountryCode = "US";
      canadaCountryCode = "CA";
      minDaysOut = true;
      isFlorist = true;
  }


  public boolean isFlorist() {
      return isFlorist;
  }
  public void setIsFlorist(boolean isFlorist) {
      this.isFlorist = isFlorist;
  }

  /*
   * True if the min days out should be used, false if max days out should be used.
   */
  public boolean getMinDaysOut() {
      return minDaysOut;
  }
  /*
   * True if the min days out should be used, false if max days out should be used.
   */
  public void setMinDaysOut(boolean minDaysOut) {
      this.minDaysOut = minDaysOut;
  }

  public String getRecipientState() {
      return recipientState;
  }
  public void setRecipientState(String recipientState) {
      this.recipientState = recipientState;
  }
  
  public String getRecipientZipCode() {
      return recipientZipCode;
  }
  public void setRecipientZipCode(String recipientZipCode) {
      this.recipientZipCode = recipientZipCode;
  }
  
  public String getRecipientCountry() {
      return recipientCountry;
  }
  public void setRecipientCountry(String recipientCountry) {
      this.recipientCountry = recipientCountry;
  }
  
  public String getProductId() {
      return productId;
  }
  public void setProductId(String productId) {
      this.productId = productId;
  }
  
  public String getOccasionId() {
      return occasionId;
  }
  public void setOccasionId(String occasionId) {
      this.occasionId = occasionId;
  }
  
  public String getUsCountryCode() {
      return usCountryCode;
  }
  public void setUsCountryCode(String usCountryCode) {
      this.usCountryCode = usCountryCode;
  }
  
  public String getCanadaCountryCode() {
      return canadaCountryCode;
  }
  public void setCanadaCountryCode(String canadaCountryCode) {
      this.canadaCountryCode = canadaCountryCode;
  }
  
  public String getOrderOrigin() {
      return orderOrigin;
  }
  public void setOrderOrigin(String orderOrigin) {
      this.orderOrigin = orderOrigin;
  }

  public void setRequestedDeliveryDate(Calendar requestedDeliveryDate){
    this.requestedDeliveryDate = requestedDeliveryDate;
  }
  public Calendar getRequestedDeliveryDate() {
    return requestedDeliveryDate;
  }

  public void setRequestedDeliveryDateRangeEnd(Calendar requestedDeliveryDateRangeEnd) {
    this.requestedDeliveryDateRangeEnd = requestedDeliveryDateRangeEnd;
  }
  public Calendar getRequestedDeliveryDateRangeEnd() {
    return requestedDeliveryDateRangeEnd;
  }

  public void setProductSubCodeId(String productSubCodeId)
  {
    this.productSubCodeId = productSubCodeId;
  }

  public String getProductSubCodeId()
  {
    return productSubCodeId;
  }


public void setAddons(String addons) {
	this.addons = addons;
}


public String getAddons() {
	return addons;
}


public String getSourceCode() {
	// TODO Auto-generated method stub
	return sourceCode;
}


public void setSourceCode(String sourceCode) {
	this.sourceCode = sourceCode;
}
}
