package com.ftd.customerordermanagement.util;
import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.dao.LockDAO;
import com.ftd.customerordermanagement.dao.ViewDAO;
import com.ftd.osp.utilities.plugins.Logger;
import java.sql.Connection;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;




public class DatabaseMaintainUtil 
{
  private Connection con;
  private String sessionId;
  private Logger logger;

  /** 
  * Constructor 1
  */
  public DatabaseMaintainUtil(Connection con)
  {
    logger = new Logger("com.ftd.customerordermanagement.util.DatabaseMaintainUtil");
  }
  
  /** 
  * Constructor 2
  */
  public DatabaseMaintainUtil(Connection con, String sessionId)
  {
    logger = new Logger("com.ftd.customerordermanagement.util.DatabaseMaintainUtil");
    this.sessionId = sessionId;
    this.con = con;
  }


	/**
	* 
	*
	*/
	public HashMap deleteViewingRecords() throws Exception
  {
    HashMap searchResults = new HashMap();
    
    //Instantiate ViewDAO
    ViewDAO viewDAO = new ViewDAO(this.con);

    //Call deleteSpecificEntity method in the DAO
    searchResults = viewDAO.deleteCsrViewing(this.sessionId);

    return searchResults;
  }

  
  
  
}