package com.ftd.customerordermanagement.util;

import com.ftd.customerordermanagement.dao.CallTimerDAO;

import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

/**
 * This class contains the business logic to retrieve queue data, obtain navigation 
 * configuration information, and send the information to the QueueRequestAction
 * for further processing.
 *
 * @author Matt Wilcoxen
 */

public class CallTimer 
{
    private Logger logger = new Logger("com.ftd.customerordermanagement.util.CallTimer");

    //database objects
    private Connection conn = null;

    /**
     * constructor
     * 
     * @param Connection - database connection
     * @return n/a
     * @throws n/a
     */
    public CallTimer(Connection conn)
    {
        super();
        this.conn = conn;
    }


    /**
     * start a call timer.
     * 
     * @param String - dnis
     * @param String - csrId
     * 
     * @return none
     * 
     * @throws IOException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws SQLException
     */
    public String start(String dnis, String csrId)
        throws IOException, ParserConfigurationException, SAXException, SQLException
    {
        CallTimerDAO ctDAO = new CallTimerDAO(conn);
        return ctDAO.insertCallLog(dnis, csrId);
    }


    /**
     * stop a call timer.
     * 
     * @param  String - call Log id
     * @param  String - reason code
     * @param  String - reason type
     * 
     * @return none
     * 
     * @throws IOException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws SQLException
     */
    public void stop(String callLogId, String reasonCode, String reasonType)
        throws IOException, ParserConfigurationException, SAXException, SQLException
    {
        CallTimerDAO ctDAO = new CallTimerDAO(conn);
        ctDAO.updateCallLog(callLogId, reasonCode, reasonType);
    }
    
    
    /**
     * delete a call timer log entry.
     * 
     * @param String - call Log Id
     * 
     * @return none
     * 
     * @throws IOException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws SQLException
     */
    public void delete(String callLogId)
        throws IOException, ParserConfigurationException, SAXException, SQLException
    {
        CallTimerDAO ctDAO = new CallTimerDAO(conn);
        ctDAO.deleteCallLog(callLogId);
    }

}