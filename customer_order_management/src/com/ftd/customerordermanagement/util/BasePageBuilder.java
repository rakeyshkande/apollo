package com.ftd.customerordermanagement.util;
import java.math.BigDecimal;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.dao.CustomerDAO;
import com.ftd.ftdutilities.FieldUtils;
import com.ftd.ftdutilities.OEDeliveryDate;
import com.ftd.mo.dao.UpdateOrderDAO;
import com.ftd.mo.vo.DeliveryDateVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.PartnerUtility;
import com.ftd.osp.utilities.SourceProductUtility;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.handlers.AddressTypeHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.CountryMasterHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.GiftMessageHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.StateMasterHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.CountryMasterVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.StateMasterVO;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.order.vo.AddressTypeVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;

public class BasePageBuilder 
{
  private Logger logger;
  private static final String COUNTRY_CODE_00 = "00";
  private static final String COUNTRY_CODE_01 = "01";
  
  private final static String WALMART_ORDER_PREFIX = "W";
  private final static String MERCENT_ORDER_DELIMITER = "-";
  private static String PRO_ORDER_SEARCH_DELIMITER;
  

  // Stuff needed to deal with the host name for new ActionForward() issues
  private static final String ERROR_PAGE = "error_page";
  private static String rootURL = null;

    /** 
     * Constructor
     */
  public BasePageBuilder()
  {
       logger = new Logger("com.ftd.customerordermanagement.util.BasePageBuilder");
       try {
			PRO_ORDER_SEARCH_DELIMITER = ConfigurationUtil.getInstance().getFrpGlobalParm(COMConstants.GLOBAL_CONFIG, COMConstants.PRO_ORDER_SEARCH_APPENDER);
		} catch (Exception e) {
			logger.info("Error getting the PRO_ORDER_SEARCH_APPENDER from cache defaulting to - ");
			PRO_ORDER_SEARCH_DELIMITER = "-";
		}
  }
  

/*******************************************************************************************
  * populateSearchCriteria()
  ******************************************************************************************
  */
  public HashMap populateSearchCriteria(HttpServletRequest request)
  {
    HashMap searchCriteriaMap = new HashMap();
        
    searchCriteriaMap.put(COMConstants.SC_ORDER_NUMBER,        ((request.getParameter(COMConstants.SC_ORDER_NUMBER))        !=null?request.getParameter(COMConstants.SC_ORDER_NUMBER).trim()       :""));
    searchCriteriaMap.put(COMConstants.SC_LAST_NAME,           ((request.getParameter(COMConstants.SC_LAST_NAME))           !=null?request.getParameter(COMConstants.SC_LAST_NAME).trim()          :""));
    searchCriteriaMap.put(COMConstants.SC_PHONE_NUMBER,        ((request.getParameter(COMConstants.SC_PHONE_NUMBER))        !=null?request.getParameter(COMConstants.SC_PHONE_NUMBER).trim()       :""));
    searchCriteriaMap.put(COMConstants.SC_EMAIL_ADDRESS,       ((request.getParameter(COMConstants.SC_EMAIL_ADDRESS))       !=null?request.getParameter(COMConstants.SC_EMAIL_ADDRESS).trim()      :""));
    searchCriteriaMap.put(COMConstants.SC_ZIP_CODE,            ((request.getParameter(COMConstants.SC_ZIP_CODE))            !=null?request.getParameter(COMConstants.SC_ZIP_CODE).trim()           :""));
    searchCriteriaMap.put(COMConstants.SC_CUST_NUMBER,         ((request.getParameter(COMConstants.SC_CUST_NUMBER))         !=null?request.getParameter(COMConstants.SC_CUST_NUMBER).trim()        :""));
    searchCriteriaMap.put(COMConstants.SC_TRACKING_NUMBER,     ((request.getParameter(COMConstants.SC_TRACKING_NUMBER))     !=null?request.getParameter(COMConstants.SC_TRACKING_NUMBER).trim()    :""));
    searchCriteriaMap.put(COMConstants.SC_MEMBERSHIP_NUMBER,   ((request.getParameter(COMConstants.SC_MEMBERSHIP_NUMBER))   !=null?request.getParameter(COMConstants.SC_MEMBERSHIP_NUMBER).trim()  :""));
    searchCriteriaMap.put(COMConstants.SC_PC_MEMBERSHIP,   	   ((request.getParameter(COMConstants.SC_PC_MEMBERSHIP))   	!=null?request.getParameter(COMConstants.SC_PC_MEMBERSHIP).trim()  	   :""));
    searchCriteriaMap.put(COMConstants.SC_PRO_ORDER_NUMBER,    ((request.getParameter(COMConstants.SC_PRO_ORDER_NUMBER))   	!= null ? request.getParameter(COMConstants.SC_PRO_ORDER_NUMBER).trim()    : ""));
     
     
    String scCustInd = request.getParameter(COMConstants.SC_CUST_IND);
    if ((scCustInd == null) || scCustInd.equalsIgnoreCase("") || scCustInd.equalsIgnoreCase("Y"))
    {
      searchCriteriaMap.put(COMConstants.SC_CUST_IND, "Y");
    }
    else
    {
      searchCriteriaMap.put(COMConstants.SC_CUST_IND, "N");
    }
      
    String scRecipInd = request.getParameter(COMConstants.SC_RECIP_IND);
    if ((scRecipInd == null)||scRecipInd.equalsIgnoreCase("") || scRecipInd.equalsIgnoreCase("Y"))
    {
      searchCriteriaMap.put(COMConstants.SC_RECIP_IND, "Y");
    }
    else
    {
      searchCriteriaMap.put(COMConstants.SC_RECIP_IND, "N");
    }
    return searchCriteriaMap;
  }


/*******************************************************************************************
  * populateSearchCriteriaFirst()
  ******************************************************************************************
  */
  public HashMap populateSearchCriteriaFirst(HttpServletRequest request, Connection con)
  {
    HashMap parameters = (HashMap) request.getAttribute(COMConstants.CONS_APP_PARAMETERS);
    HashMap searchCriteriaMap = new HashMap();
   
    String requestOrderNumber = request.getParameter(COMConstants.IN_ORDER_NUMBER);
    if(requestOrderNumber == null)
    {
        requestOrderNumber = "";
    }
    else
    {
        requestOrderNumber = requestOrderNumber.trim();
    }
    
    String searchOrderNumber = requestOrderNumber;
    
    // for pro order search
    String proOrderNumber = request.getParameter(COMConstants.IN_PRO_ORDER_NUMBER);
    try {    
	    if(!StringUtils.isEmpty(proOrderNumber)) {
	    	proOrderNumber = proOrderNumber.trim();  
	    	
	    	//get the count of the PRO partner orders for the given web order number. 
	    	CachedResultSet proOrderSearchResult = new CustomerDAO(con).getProOrders(proOrderNumber.concat(PRO_ORDER_SEARCH_DELIMITER));
	    	int recordCount = 0;
	    	String proOrder = null;
	    	if(proOrderSearchResult != null) {
	    		while(proOrderSearchResult.next()) {
	    			recordCount++;
	    			proOrder = proOrderSearchResult.getString("CONFIRMATION_NUMBER");
	    		}
	    	}
	    	
	    	searchCriteriaMap.put("PRO_ORDER_COUNT", String.valueOf(recordCount));
	    	
	    	// if exactly one match found get the 
	    	if(recordCount == 1) {
	    		searchOrderNumber = proOrder;
	    	}
	    	
	    } else {
	    	proOrderNumber = ""; 
	    	// search if it is a partner orders other than PRO
	    	searchOrderNumber = formatPartnerOrder(requestOrderNumber, con);
	    }
    } catch(Exception e) {
    	logger.error("Unable to process the partner order search, ", e);
    }
    
    searchCriteriaMap.put(COMConstants.SC_PRO_ORDER_NUMBER, proOrderNumber);
	parameters.put(COMConstants.SC_PRO_ORDER_NUMBER, proOrderNumber);
	
		    
    //If it is partner order,
    //searchOrderNumber contains characters. But walmart order does not contain characters. Hence no ambiguity between walmart and partner orders.
    //searchOrderNumber does not contain MERCENT_ORDER_DELIMITER(-). Hence no ambiguity between mercent and partner orders.

    if (searchOrderNumber.indexOf(this.MERCENT_ORDER_DELIMITER)> 0)
    {
    	searchOrderNumber = formatMercentOrderNumber(searchOrderNumber,con);
    }
   
    searchCriteriaMap.put(COMConstants.SC_ORDER_NUMBER,  searchOrderNumber );
    parameters.put(COMConstants.SC_ORDER_NUMBER,  requestOrderNumber);    
    
    searchCriteriaMap.put(COMConstants.FORMATTED_ORD_NUMBER,  searchOrderNumber );
    parameters.put(COMConstants.FORMATTED_ORD_NUMBER,  searchOrderNumber);
    
    searchCriteriaMap.put(COMConstants.SC_LAST_NAME,            ((request.getParameter(COMConstants.IN_LAST_NAME))           !=null?request.getParameter(COMConstants.IN_LAST_NAME).trim()          :""));
    parameters.put(COMConstants.SC_LAST_NAME,                   searchCriteriaMap.get(COMConstants.SC_LAST_NAME).toString());
    
    searchCriteriaMap.put(COMConstants.SC_PHONE_NUMBER,         ((request.getParameter(COMConstants.IN_PHONE_NUMBER))        !=null?request.getParameter(COMConstants.IN_PHONE_NUMBER).trim()       :""));
    parameters.put(COMConstants.SC_PHONE_NUMBER,                searchCriteriaMap.get(COMConstants.SC_PHONE_NUMBER).toString());
    
    searchCriteriaMap.put(COMConstants.SC_EMAIL_ADDRESS,        ((request.getParameter(COMConstants.IN_EMAIL_ADDRESS))       !=null?request.getParameter(COMConstants.IN_EMAIL_ADDRESS).trim()      :""));
    parameters.put(COMConstants.SC_EMAIL_ADDRESS,               searchCriteriaMap.get(COMConstants.SC_EMAIL_ADDRESS).toString());
    
    searchCriteriaMap.put(COMConstants.SC_ZIP_CODE,             ((request.getParameter(COMConstants.IN_ZIP_CODE))            !=null?request.getParameter(COMConstants.IN_ZIP_CODE).trim()           :""));
    parameters.put(COMConstants.SC_ZIP_CODE,                    searchCriteriaMap.get(COMConstants.SC_ZIP_CODE).toString());
    
    searchCriteriaMap.put(COMConstants.SC_CC_NUMBER,            ((request.getParameter(COMConstants.IN_CC_NUMBER))           !=null?request.getParameter(COMConstants.IN_CC_NUMBER).trim()          :""));

    searchCriteriaMap.put(COMConstants.SC_CUST_NUMBER,          ((request.getParameter(COMConstants.IN_CUST_NUMBER))         !=null?request.getParameter(COMConstants.IN_CUST_NUMBER).trim()        :""));
    parameters.put(COMConstants.SC_CUST_NUMBER,                 searchCriteriaMap.get(COMConstants.SC_CUST_NUMBER).toString());
    
    searchCriteriaMap.put(COMConstants.SC_TRACKING_NUMBER,      ((request.getParameter(COMConstants.IN_TRACKING_NUMBER))     !=null?request.getParameter(COMConstants.IN_TRACKING_NUMBER).trim()    :""));
    parameters.put(COMConstants.SC_TRACKING_NUMBER,             searchCriteriaMap.get(COMConstants.SC_TRACKING_NUMBER).toString());
    
    searchCriteriaMap.put(COMConstants.SC_MEMBERSHIP_NUMBER,    ((request.getParameter(COMConstants.IN_MEMBERSHIP_NUMBER))   !=null?request.getParameter(COMConstants.IN_MEMBERSHIP_NUMBER).trim()  :""));
    parameters.put(COMConstants.SC_MEMBERSHIP_NUMBER,           searchCriteriaMap.get(COMConstants.SC_MEMBERSHIP_NUMBER).toString());
    
    searchCriteriaMap.put(COMConstants.SC_PC_MEMBERSHIP,    ((request.getParameter(COMConstants.IN_PC_MEMBERSHIP))   !=null?request.getParameter(COMConstants.IN_PC_MEMBERSHIP).trim()  :""));
    parameters.put(COMConstants.SC_PC_MEMBERSHIP,           searchCriteriaMap.get(COMConstants.SC_PC_MEMBERSHIP).toString());

    searchCriteriaMap.put(COMConstants.SC_AP_ACCOUNT,           ((request.getParameter(COMConstants.IN_AP_ACCOUNT))          !=null?request.getParameter(COMConstants.IN_AP_ACCOUNT).trim()         :""));
    parameters.put(COMConstants.SC_AP_ACCOUNT,                  searchCriteriaMap.get(COMConstants.SC_AP_ACCOUNT).toString());

    String scCustInd = request.getParameter(COMConstants.SC_CUST_IND);
    if ((scCustInd == null) || scCustInd.equalsIgnoreCase("") || scCustInd.equalsIgnoreCase("Y"))
    {
      searchCriteriaMap.put(COMConstants.SC_CUST_IND, "Y");
    }
    else
    {
      searchCriteriaMap.put(COMConstants.SC_CUST_IND, "N");
    }
      
    String scRecipInd = request.getParameter(COMConstants.SC_RECIP_IND);
    if ((scRecipInd == null)||scRecipInd.equalsIgnoreCase("") || scRecipInd.equalsIgnoreCase("Y"))
    {
      searchCriteriaMap.put(COMConstants.SC_RECIP_IND, "Y");
    }
    else
    {
      searchCriteriaMap.put(COMConstants.SC_RECIP_IND, "N");
    }
    parameters.put(COMConstants.SC_CUST_IND,    searchCriteriaMap.get(COMConstants.SC_CUST_IND).toString());
    parameters.put(COMConstants.SC_RECIP_IND,   searchCriteriaMap.get(COMConstants.SC_RECIP_IND).toString());
    
    request.setAttribute(COMConstants.CONS_APP_PARAMETERS, parameters);

    return searchCriteriaMap;
    
  }



  public Document retrieveStates(Connection con) throws Exception
  {
      return retrieveStates(con,false);
  }


/*******************************************************************************************
 * retrieveStates()
 *******************************************************************************************
  * This method is used to call the CustomerDAO.getStates
  * 
  * @throws none
  */
  public Document retrieveStates(Connection con,boolean includeBlankState) throws Exception
  {
    Map statesVOMap = new HashMap();
    Map statesVOTMap = new TreeMap();

    //create a new xml document
    Document statesXML = (Document) DOMUtil.getDefaultDocument();

    StateMasterHandler stateHandler = (StateMasterHandler) CacheManager.getInstance().getHandler("CACHE_NAME_STATE_MASTER");

    //get a hashmap of stateVOs
    statesVOMap = (Map) stateHandler.getStatesByNameMap();

    //if the states could not be loaded from the cache
    if(statesVOMap == null)
    {
      //Instantiate CustomerDAO
      CustomerDAO customerDAO = new CustomerDAO(con);

      //Create a CachedResultSet object using the cursor name that was passed. 
      CachedResultSet rs = (CachedResultSet) customerDAO.getStates();

      while(rs.next())
      {
        StateMasterVO sVO = new StateMasterVO();

        String id = rs.getObject("statemasterid").toString().toUpperCase();
        sVO.setStateMasterId(id);
        String name = rs.getObject("statename").toString().toUpperCase();
        sVO.setStateName(name);
        sVO.setCountryCode((String) rs.getObject("countrycode"));
        sVO.setTimeZone((String) rs.getObject("timezone"));

        statesVOTMap.put(name, sVO);
      }

    }
    else
    {
      Set ks1 = statesVOMap.keySet(); 
      Iterator iter = ks1.iterator();
      String key = null;
      
      //Iterate thru the keyset
      while(iter.hasNext())
      {
        //get the key
        key = iter.next().toString();
        
        statesVOTMap.put(key, (StateMasterVO) statesVOMap.get(key));
      }
    }

    //if the states are loaded within the cache
    if (statesVOTMap != null)
    {
      //generate the top node
      Element stateElement = statesXML.createElement("STATES");
      //and append it
      statesXML.appendChild(stateElement);

      //instantiate a new parser
      //DOMParser parser = new DOMParser();
        
      //get the keyset of the hashmap that contains the stateVOs
      Set ks1 = statesVOTMap.keySet(); 
      Iterator iter = ks1.iterator();
      String key = null;
      StateMasterVO sVO;      
      int count = 0;
      
      //insert an empty record if needed
      if(includeBlankState)
      {
          sVO = new StateMasterVO();
          Document sDoc = DOMUtil.getDocument(sVO.toXML(0));
          statesXML.getDocumentElement().appendChild(statesXML.importNode(sDoc.getFirstChild(), true));
      }
      
      
      //iterate thru the hashmap, get the vo and process
      while(iter.hasNext())
      {
        count++;
        
        //get the key
        key = iter.next().toString();
        
        //get an object from the hashmap into a stateVO
        sVO = (StateMasterVO) statesVOTMap.get(key);
          
        //call the toXML() method  
        Document sDoc = DOMUtil.getDocument(sVO.toXML(count));
        statesXML.getDocumentElement().appendChild(statesXML.importNode(sDoc.getFirstChild(), true));
      }  
    }

    return statesXML;

  }



/*******************************************************************************************
 * retrieveCountries()
 *******************************************************************************************
  * This method is used to call the CustomerDAO.getStates
  * 
  * @throws none
  */
  public Document retrieveCountries(Connection con) throws Exception
  {
    List countriesList = new ArrayList();
    Document countriesXML = (Document) DOMUtil.getDefaultDocument();

    CountryMasterHandler countryHandler = (CountryMasterHandler) CacheManager.getInstance().getHandler("CACHE_NAME_COUNTRY_MASTER");

    //get a list of stateVOs
    countriesList = countryHandler.getCountryList();

    //if the states could not be loaded from the cache
    if(countriesList == null)
    {
      countriesList = new ArrayList();
      
      //Instantiate CustomerDAO
      CustomerDAO customerDAO = new CustomerDAO(con);

      //Create a CachedResultSet object using the cursor name that was passed. 
      CachedResultSet rs = (CachedResultSet) customerDAO.getCountries();

      while(rs.next())
      {
        String countryId = (String) rs.getObject("country_id");
        if (COUNTRY_CODE_00.equals(countryId) || COUNTRY_CODE_01.equals(countryId)) {
          continue; // Ignore country codes of 00 and 01
        }
        CountryMasterVO cVO = new CountryMasterVO();
        cVO.setCountryId(countryId);
        cVO.setCountryName((String) rs.getObject("name"));
        cVO.setDisplayOrder((BigDecimal) rs.getObject("oe_display_order"));
        cVO.setCountryType((String) rs.getObject("oe_country_type"));
        cVO.setMondayClosed((String) rs.getObject("monday_closed"));
        cVO.setTuesdayClosed((String) rs.getObject("tuesday_closed"));
        cVO.setWednesdayClosed((String) rs.getObject("wednesday_closed"));
        cVO.setThursdayClosed((String) rs.getObject("thursday_closed"));
        cVO.setFridayClosed((String) rs.getObject("friday_closed"));
        cVO.setSaturdayClosed((String) rs.getObject("saturday_closed"));
        cVO.setSundayClosed((String) rs.getObject("sunday_closed"));
        Object addonDays = rs.getObject("add_on_days");
        if(addonDays == null)
        {
          addonDays = new BigDecimal("0");
        }
        cVO.setAddonDays(((BigDecimal)addonDays).longValue());
        cVO.setThreeCharacterId((String) rs.getObject("three_character_id"));

        countriesList.add(cVO);
      }
    }


    //if the countries are loaded within the cache
    if (countriesList != null)
    {
      //create a new xml document
      countriesXML = (Document) DOMUtil.getDefaultDocument();

      //generate the top node
      Element stateElement = countriesXML.createElement("COUNTRIES");
      //and append it
      countriesXML.appendChild(stateElement);
      //DOMParser parser = new DOMParser();
      Iterator iter = countriesList.iterator();
      CountryMasterVO cVO;      
      int count = 0;

      //iterate thru the list, get the vo and process
      while(iter.hasNext())
      {
        cVO = (CountryMasterVO) iter.next();
        String countryId = cVO.getCountryId();
        if (COUNTRY_CODE_00.equals(countryId) || COUNTRY_CODE_01.equals(countryId)) {
          continue; // Ignore country codes of 00 and 01
        }
        count++;
            
        //parser.parse(new InputSource(new StringReader(cVO.toXML(count))));
        
        countriesXML.getDocumentElement().appendChild(countriesXML.importNode(DOMUtil.getDocument(cVO.toXML(count)).getFirstChild(), true));
      }  
    }

    return countriesXML;


  }
  

/*******************************************************************************************
 * retrieveAddressTypes()
 *******************************************************************************************
  * This method is used to retrieve the Address Types from the object cache (or database)
  * 
  */
  public Document retrieveAddressTypes(Connection con) throws Exception
  {
    List addrTypeList = new ArrayList();
    Document addrTypeXML = (Document) DOMUtil.getDefaultDocument();

    AddressTypeHandler addrTypeHandler = (AddressTypeHandler) CacheManager.getInstance().getHandler("CACHE_NAME_ADDRESS");

    Map addrTypeMap = addrTypeHandler.getAddressTypeVOMap();

    // Generate top node 
    Element addrTypeTopElement = addrTypeXML.createElement("ADDRESS_TYPES");
    addrTypeXML.appendChild(addrTypeTopElement);

    if (addrTypeMap != null) {
      for (java.util.Iterator i = addrTypeMap.keySet().iterator(); i.hasNext();) {
        String addrTypeStr = (String) i.next();
        AddressTypeVO vo = (AddressTypeVO) addrTypeMap.get(addrTypeStr);
        // Add address type node
        Element addrTypeElement    = addrTypeXML.createElement("ADDRESS_TYPE");
        addrTypeTopElement.appendChild(addrTypeElement);
        Element addrTypeNode = addrTypeXML.createElement("TYPE");
        Element addrCodeNode = addrTypeXML.createElement("CODE");
        Element addrDescriptionNode = addrTypeXML.createElement("DESCRIPTION");
        
        Element addrPromptForBusinessNode = addrTypeXML.createElement("PROMPT_FOR_BUSINESS_FLAG");
        Element addrPromptForRoomNode = addrTypeXML.createElement("PROMPT_FOR_ROOM_FLAG");
        Element addrRoomLabelNode = addrTypeXML.createElement("ROOM_LABEL_TXT");
        Element addrPromptForHoursNode = addrTypeXML.createElement("PROMPT_FOR_HOURS_FLAG");
        Element addrHoursLabelNode = addrTypeXML.createElement("HOURS_LABEL_TXT");
        
        addrTypeNode.appendChild(addrTypeXML.createTextNode(addrTypeStr));
        addrCodeNode.appendChild(addrTypeXML.createTextNode(vo.getCode()));
        addrDescriptionNode.appendChild(addrTypeXML.createTextNode(vo.getDescription()));
        addrPromptForBusinessNode.appendChild(addrTypeXML.createTextNode(vo.isPromptForBusiness() ? "Y" : "N"));
        addrPromptForRoomNode.appendChild(addrTypeXML.createTextNode(vo.isPromptForRoom() ? "Y" : "N"));
        addrRoomLabelNode.appendChild(addrTypeXML.createTextNode(vo.getRoomLabelTxt()));
        addrPromptForHoursNode.appendChild(addrTypeXML.createTextNode(vo.isPromptForHours() ? "Y" : "N"));
        addrHoursLabelNode.appendChild(addrTypeXML.createTextNode(vo.getHoursLabelTxt()));
        addrTypeElement.appendChild(addrTypeNode);
        addrTypeElement.appendChild(addrCodeNode);
        addrTypeElement.appendChild(addrDescriptionNode);
        addrTypeElement.appendChild(addrPromptForBusinessNode);
        addrTypeElement.appendChild(addrPromptForRoomNode);
        addrTypeElement.appendChild(addrRoomLabelNode);
        addrTypeElement.appendChild(addrPromptForHoursNode);
        addrTypeElement.appendChild(addrHoursLabelNode);
      }
    } else {
      logger.error("BasePageBuilder failed to load ADDRESS types from object cache");
    }
    return addrTypeXML;
  }

  

/*******************************************************************************************
 * retrieveGiftMessages()
 *******************************************************************************************
  * This method is used to retrieve the Gift Messages from the object cache (or database)
  * 
  */
  public Document retrieveGiftMessages(Connection con) throws Exception
  {
    List giftMsgList = new ArrayList();
    Document giftMsgXML = (Document) DOMUtil.getDefaultDocument();

    GiftMessageHandler giftMsgHandler = (GiftMessageHandler) CacheManager.getInstance().getHandler("GIFT_MESSAGE");

    Map giftMsgMap = giftMsgHandler.getGiftMessagesIdMap();

    // Generate top node 
    Element giftMsgTopElement = giftMsgXML.createElement("GIFT_MESSAGES");
    giftMsgXML.appendChild(giftMsgTopElement);

    if (giftMsgMap != null) {
      for (java.util.Iterator i = giftMsgMap.keySet().iterator(); i.hasNext();) {
        String giftMsgStr = (String) i.next();
        String addrCodeStr = (String) giftMsgMap.get(giftMsgStr);
        // Add address type node
        Element giftMsgElement    = giftMsgXML.createElement("GIFT_MESSAGE");
        giftMsgTopElement.appendChild(giftMsgElement);
        Element giftMsgNode = giftMsgXML.createElement("ID");
        Element addrCodeNode = giftMsgXML.createElement("TEXT");
        giftMsgNode.appendChild(giftMsgXML.createTextNode(giftMsgStr));
        addrCodeNode.appendChild(giftMsgXML.createTextNode(addrCodeStr));
        giftMsgElement.appendChild(giftMsgNode);
        giftMsgElement.appendChild(addrCodeNode);
      }
    } else {
      logger.error("BasePageBuilder failed to load ADDRESS types from object cache");
    }
    return giftMsgXML;
  }


  /**
   * Returns all Product data found in the request using the following XML format:
   *
   *  <subheader>
   *    <subheader_info>
   *        <sh_customer_first_name>Debbi</sh_customer_first_name>
   *        <sh_customer_last_name>Mayer</sh_customer_last_name>
   *        <sh_external_order_number>C1000554652</sh_external_order_number>
   *        <sh_product_id>6033</sh_product_id>
   *        <sh_product_name>HAPPY B-DAY BALLOON BUNCH</sh_product_name>
   *        <sh_short_description/>
   *        <sh_color1_description/>
   *        <sh_color2_description/>
   *        <sh_quantity>1</sh_quantity>
   *        <sh_color_1/>
   *        <sh_color_2/>
   *        <sh_second_choice_product/>
   *        <sh_product_amount>34.99</sh_product_amount>
   *        <sh_discount_amount>0</sh_discount_amount>
   *        <sh_tax>0</sh_tax>
   *    </subheader_info>
   *    <subheader_addons>
   *        <subheader_addon>
   *            <sh_description>Chocolate</sh_description>
   *            <sh_add_on_quantity>1</sh_add_on_quantity>
   *            <sh_price>15</sh_price>
   *            <sh_add_on_code>C</sh_add_on_code>
   *            <sh_add_on_type_description>Chocolate</sh_add_on_type_description>
   *        </subheader_addon>
   *        <subheader_addon>
   *            <sh_description>Mylar Balloon</sh_description>
   *            <sh_add_on_quantity>1</sh_add_on_quantity>
   *            <sh_price>5</sh_price>
   *            <sh_add_on_code>A</sh_add_on_code>
   *            <sh_add_on_type_description>Balloon</sh_add_on_type_description>
   *        </subheader_addon>
   *    </subheader_addons>
   *  <subheader>
   *
   * @param request The request containing the Product data.
   * @return Document containing Product data.
   * @throws java.lang.Exception
   */
  public Document retrieveSubHeaderData(HttpServletRequest request)
      throws Exception {

    //Document that will contain the final XML, to be passed to the TransUtil and XSL page
    Document subHeaderDoc = DOMUtil.getDefaultDocument();

    //*****************************************************************************************
    //create the highest element
    //*****************************************************************************************
    Element parent = subHeaderDoc.createElement("subheader");
    subHeaderDoc.appendChild( parent );

    //*****************************************************************************************
    //create the subheader_info element
    //*****************************************************************************************
    String sh_customer_first_name = StringUtils.defaultString( request.getParameter("sh_customer_first_name") );
    String sh_customer_last_name = StringUtils.defaultString( request.getParameter("sh_customer_last_name") );
    String sh_external_order_number = StringUtils.defaultString( request.getParameter("sh_external_order_number") );
    String sh_product_id = StringUtils.defaultString( request.getParameter("sh_product_id") );
    String sh_product_name = StringUtils.defaultString( request.getParameter("sh_product_name") );
    String sh_short_description = StringUtils.defaultString( request.getParameter("sh_short_description") );
    String sh_color_1 = StringUtils.defaultString( request.getParameter("sh_color_1") );
    String sh_color_2 = StringUtils.defaultString( request.getParameter("sh_color_2") );
    String sh_color1_description = StringUtils.defaultString( request.getParameter("sh_color1_description") );
    String sh_color2_description = StringUtils.defaultString( request.getParameter("sh_color2_description") );
    String sh_product_amount = StringUtils.defaultString( request.getParameter("sh_product_amount") );
    String sh_discount_amount = StringUtils.defaultString( request.getParameter("sh_discount_amount") );
    String sh_tax = StringUtils.defaultString( request.getParameter("sh_tax") );
    String sh_miles_points = StringUtils.defaultString( request.getParameter("sh_miles_points") );
    String sh_miles_points_posted = StringUtils.defaultString( request.getParameter("sh_miles_points_posted") );
    String sh_discount_reward_type = StringUtils.defaultString( request.getParameter("sh_discount_reward_type") );
    String sh_add_on_amount = StringUtils.defaultString( request.getParameter("sh_add_on_amount") );
    String sh_delivery_date = StringUtils.defaultString( request.getParameter("sh_delivery_date") );
    String sh_delivery_date_range_end = StringUtils.defaultString( request.getParameter("sh_delivery_date_range_end") );
    String sh_ship_method_desc = StringUtils.defaultString( request.getParameter("sh_ship_method_desc") );

    Element subHeaderInfoChild = subHeaderDoc.createElement("subheader_info");
    parent.appendChild(subHeaderInfoChild);

    Element sh_info_node = subHeaderDoc.createElement("sh_customer_first_name");
    sh_info_node.appendChild( subHeaderDoc.createTextNode(sh_customer_first_name) );
    subHeaderInfoChild.appendChild(sh_info_node);

    sh_info_node = subHeaderDoc.createElement("sh_customer_last_name");
    sh_info_node.appendChild( subHeaderDoc.createTextNode(sh_customer_last_name) );
    subHeaderInfoChild.appendChild(sh_info_node);

    sh_info_node = subHeaderDoc.createElement("sh_external_order_number");
    sh_info_node.appendChild( subHeaderDoc.createTextNode(sh_external_order_number) );
    subHeaderInfoChild.appendChild(sh_info_node);

    sh_info_node = subHeaderDoc.createElement("sh_product_id");
    sh_info_node.appendChild( subHeaderDoc.createTextNode(sh_product_id) );
    subHeaderInfoChild.appendChild(sh_info_node);

    sh_info_node = subHeaderDoc.createElement("sh_product_name");
    sh_info_node.appendChild( subHeaderDoc.createTextNode(sh_product_name) );
    subHeaderInfoChild.appendChild(sh_info_node);

    sh_info_node = subHeaderDoc.createElement("sh_short_description");
    sh_info_node.appendChild( subHeaderDoc.createTextNode(sh_short_description) );
    subHeaderInfoChild.appendChild(sh_info_node);

    sh_info_node = subHeaderDoc.createElement("sh_color_1");
    sh_info_node.appendChild( subHeaderDoc.createTextNode(sh_color_1) );
    subHeaderInfoChild.appendChild(sh_info_node);

    sh_info_node = subHeaderDoc.createElement("sh_color_2");
    sh_info_node.appendChild( subHeaderDoc.createTextNode(sh_color_2) );
    subHeaderInfoChild.appendChild(sh_info_node);

    sh_info_node = subHeaderDoc.createElement("sh_color1_description");
    sh_info_node.appendChild( subHeaderDoc.createTextNode(sh_color1_description) );
    subHeaderInfoChild.appendChild(sh_info_node);

    sh_info_node = subHeaderDoc.createElement("sh_color2_description");
    sh_info_node.appendChild( subHeaderDoc.createTextNode(sh_color2_description) );
    subHeaderInfoChild.appendChild(sh_info_node);

    sh_info_node = subHeaderDoc.createElement("sh_product_amount");
    sh_info_node.appendChild( subHeaderDoc.createTextNode(sh_product_amount) );
    subHeaderInfoChild.appendChild(sh_info_node);

    sh_info_node = subHeaderDoc.createElement("sh_discount_amount");
    sh_info_node.appendChild( subHeaderDoc.createTextNode(sh_discount_amount) );
    subHeaderInfoChild.appendChild(sh_info_node);

    sh_info_node = subHeaderDoc.createElement("sh_tax");
    sh_info_node.appendChild( subHeaderDoc.createTextNode(sh_tax) );
    subHeaderInfoChild.appendChild(sh_info_node);

    sh_info_node = subHeaderDoc.createElement("sh_miles_points");
    sh_info_node.appendChild( subHeaderDoc.createTextNode(sh_miles_points) );
    subHeaderInfoChild.appendChild(sh_info_node);

    sh_info_node = subHeaderDoc.createElement("sh_miles_points_posted");
    sh_info_node.appendChild( subHeaderDoc.createTextNode(sh_miles_points_posted) );
    subHeaderInfoChild.appendChild(sh_info_node);

    sh_info_node = subHeaderDoc.createElement("sh_discount_reward_type");
    sh_info_node.appendChild( subHeaderDoc.createTextNode(sh_discount_reward_type) );
    subHeaderInfoChild.appendChild(sh_info_node);

    sh_info_node = subHeaderDoc.createElement("sh_add_on_amount");
    sh_info_node.appendChild( subHeaderDoc.createTextNode(sh_add_on_amount) );
    subHeaderInfoChild.appendChild(sh_info_node);

    sh_info_node = subHeaderDoc.createElement("sh_delivery_date");
    sh_info_node.appendChild( subHeaderDoc.createTextNode(sh_delivery_date) );
    subHeaderInfoChild.appendChild(sh_info_node);

    sh_info_node = subHeaderDoc.createElement("sh_delivery_date_range_end");
    sh_info_node.appendChild( subHeaderDoc.createTextNode(sh_delivery_date_range_end) );
    subHeaderInfoChild.appendChild(sh_info_node);
    
    sh_info_node = subHeaderDoc.createElement("sh_ship_method_desc");
    sh_info_node.appendChild( subHeaderDoc.createTextNode(sh_ship_method_desc) );
    subHeaderInfoChild.appendChild(sh_info_node);



    //*****************************************************************************************
    //create the subheader_addons element
    //*****************************************************************************************
    int sh_addonCount = Integer.parseInt(StringUtils.defaultString( request.getParameter("sh_addon_count"), "0" ));
    String[] sh_descriptions = new String[sh_addonCount];
    String[] sh_quantities = new String[sh_addonCount];
    String[] sh_prices = new String[sh_addonCount];
    for ( int i = 1; i <= sh_addonCount; i++ ) {
      sh_descriptions[i-1] = StringUtils.defaultString( request.getParameter("sh_add_on_description_" + i) );
      sh_quantities[i-1] = StringUtils.defaultString( request.getParameter("sh_add_on_quantity_" + i) );
      sh_prices[i-1] = StringUtils.defaultString( request.getParameter("sh_add_on_price_" + i) );
    }

    Element subHeaderAddOnsParent = subHeaderDoc.createElement("subheader_addons");
    parent.appendChild(subHeaderAddOnsParent);

    Element subHeaderAddOnsChild, sh_ao_node;
    for ( int i = 0; i < sh_addonCount; i++ )
    {
      subHeaderAddOnsChild = subHeaderDoc.createElement("subheader_addon");
      subHeaderAddOnsChild.setAttribute("num", new Integer(i+1).toString());
      subHeaderAddOnsParent.appendChild(subHeaderAddOnsChild);

      sh_ao_node = subHeaderDoc.createElement("sh_add_on_description");
      sh_ao_node.appendChild( subHeaderDoc.createTextNode( sh_descriptions[i] ) );
      subHeaderAddOnsChild.appendChild(sh_ao_node);

      sh_ao_node = subHeaderDoc.createElement("sh_add_on_quantity");
      sh_ao_node.appendChild( subHeaderDoc.createTextNode( sh_quantities[i] ) );
      subHeaderAddOnsChild.appendChild(sh_ao_node);

      sh_ao_node = subHeaderDoc.createElement("sh_add_on_price");
      sh_ao_node.appendChild( subHeaderDoc.createTextNode( sh_prices[i] ) );
      subHeaderAddOnsChild.appendChild(sh_ao_node);
    }

    //*****************************************************************************************
    //create the subheader_taxes element
    //*****************************************************************************************
    int sh_taxesCount = Integer.parseInt(StringUtils.defaultString( request.getParameter("sh_taxes_count"), "0" ));
    String[] sh_tax_descriptions = new String[sh_taxesCount];
    String[] sh_tax_amounts = new String[sh_taxesCount];
    String[] sh_display_orders = new String[sh_taxesCount];
    for ( int i = 1; i <= sh_taxesCount; i++ ) {
        sh_tax_descriptions[i-1] = StringUtils.defaultString( request.getParameter("sh_tax_description_" + i) );
        sh_tax_amounts[i-1] = StringUtils.defaultString( request.getParameter("sh_tax_amount_" + i) );
        sh_display_orders[i-1] = StringUtils.defaultString( request.getParameter("sh_display_order_" + i) );
    }

    Element subHeaderTaxesParent = subHeaderDoc.createElement("subheader_taxes");
    parent.appendChild(subHeaderTaxesParent);

    Element subHeaderTaxesChild, sh_tax_node;
    for ( int i = 0; i < sh_taxesCount; i++ ) {
    	subHeaderTaxesChild = subHeaderDoc.createElement("subheader_tax");
    	subHeaderTaxesChild.setAttribute("num", new Integer(i+1).toString());
        subHeaderTaxesParent.appendChild(subHeaderTaxesChild);

        sh_tax_node = subHeaderDoc.createElement("sh_tax_description");
        sh_tax_node.appendChild( subHeaderDoc.createTextNode( sh_tax_descriptions[i] ) );
        subHeaderTaxesChild.appendChild(sh_tax_node);

        sh_tax_node = subHeaderDoc.createElement("sh_tax_amount");
        sh_tax_node.appendChild( subHeaderDoc.createTextNode( sh_tax_amounts[i] ) );
        subHeaderTaxesChild.appendChild(sh_tax_node);

        sh_tax_node = subHeaderDoc.createElement("sh_display_order");
        sh_tax_node.appendChild( subHeaderDoc.createTextNode( sh_display_orders[i] ) );
        subHeaderTaxesChild.appendChild(sh_tax_node);
    }

    return subHeaderDoc;
  }



/**
 * Description:   Replaces ALL occurences of a string(what) in a string(source)
 *                with a given string (with).
 *
 * @return java.lang.String
 */
 public static String replaceAll(String source, String what, String with)
	{
		String strReturn = null;

		strReturn = translateNullToString(source);
		strReturn = translateNullToString(strReturn);
		int index = -1;

		if (with == null) {
			return strReturn;
		}

		index = strReturn.indexOf (what);
		while (index >=0)
			{
				strReturn = strReturn.substring (0,index) + with + strReturn.substring (index + what.length (),strReturn.length ());
				index = strReturn.indexOf (what,index + with.length ());
			}

		return strReturn;
	}

/**
 * Description:   returns "" vs null string for null string
 *
 * @return java.lang.String
 */
public static String translateNullToString(Object source)
	{
		String strReturn = "";
		if (source == null)
			strReturn = "";
		else
			strReturn = "" + source;
		return (String)strReturn;
	}



/*******************************************************************************************
 * retrieveProductBySourceCodeInfo() (MO_SOURCE_CODE_RECORD_LOOKUP)
 *******************************************************************************************
  * This method is used to call the GLOBAL.GLOBAL_PKG.GET_SOURCE_CODE_RECORD to retrieve
  * all the product by source code info. 
  *
  */
  public Document retrieveProductBySourceCodeInfo(Connection con, String sourceCode) throws Exception
  {
    //Instantiate ViewDAO
    UpdateOrderDAO uoDAO = new UpdateOrderDAO(con);

    //Document that will contain the output from the stored proce
    Document sourceCodeInfo = DOMUtil.getDocument();

    //Call getCSRViewed method in the DAO
    sourceCodeInfo = uoDAO.getSourceCodeInfoByIdXML(sourceCode);

    return sourceCodeInfo;

  }



/*******************************************************************************************
 * retrieveFeeInfo() (COM_SNH_BY_ID)
 *******************************************************************************************
  * This method is used to call the
  *
  */
  public Document retrieveFeeInfo(Connection con, String snhId, String deliveryDate) throws Exception
  {
    //Instantiate ViewDAO
    UpdateOrderDAO uoDAO = new UpdateOrderDAO(con);

    //Document that will contain the output from the stored proce
    Document feeInfo = DOMUtil.getDocument();

    //Call getCSRViewed method in the DAO
    feeInfo = uoDAO.getFeeByIdXML(snhId, deliveryDate);

    return feeInfo;

  }


/*******************************************************************************************
  * formatDeliveryDates(DeliveryDates) - This method is used to format the delivery dates. 
  ****************************************************************************************** 
  * 
  * Input XML will have the following format:
  * -----------------------------------------
  *	<shippingData>
  *		<deliveryDates>
  *			<deliveryDate date="12/13/2005" dayOfWeek="Tue" types="ND:" displayDate="12/13/2005" index="index2"/>
  *		</deliveryDates>
  *
  *		<shippingMethods>
  *			<shippingMethod>
  *				<method cost="0" description="Saturday Delivery" code="SA"/>
  *			</shippingMethod>
  *		</shippingMethods>
  *	</shippingData>
  *
  *
  * Output XML will have the following format:
  * ------------------------------------------
  *	<shippingData>
  *		<deliveryDates>
  *			<deliveryDate>
  *				<startDate>12/11/2005</startDate>
  *				<startDay>Sun</startDay>
  *				<endDate>12/11/2005</endDate>
  *				<endDay>Sun</endDay>
  *				<displayDate>Sun 12/11/2005</displayDate>
  *				<isRange>false</isRange>
  *				<types>FL:</types>
  *				<index>index0</index>
  *			</deliveryDate>
  *		</deliveryDates>
  *	
  *		<shippingMethods>
  *			<shippingMethod>
  *				<method cost="0" description="Saturday Delivery" code="SA"/>
  *			</shippingMethod>
  *		</shippingMethods>
  *	</shippingData>
  * 
  ********************************************************************************************/
  
  public Document formatVendorDeliveryDates(Document unfShippingData) throws Exception
  {
    Document document = DOMUtil.getDefaultDocument();
    Element shippingData = (Element) document.createElement("shippingData");
    document.appendChild(shippingData);

    Element deliveryDates = (Element) document.createElement("deliveryDates");
    shippingData.appendChild(deliveryDates);

    Element shippingMethods = (Element) document.createElement("shippingMethods");
    shippingData.appendChild(shippingMethods);

    Element inElement, outElement, node;
    String xpath;
    NodeList nl;

    //Process the deliveryDates
    xpath = "//deliveryDate";
    nl = DOMUtil.selectNodes(unfShippingData,xpath);

    if ( nl.getLength() > 0 )
    {
      String startDate   = "";
      String startDay    = "";
      String endDate     = "";
      String endDay      = "";
      String types       = "";
      String index       = "";
      String displayDate = "";
      String displayWeek = "";
      String isRange     = "";

      for ( int i = 0; i < nl.getLength(); i++ )
      {
        inElement = (Element) nl.item(i);

        startDate   = inElement.getAttribute("date");
        startDay    = inElement.getAttribute("dayOfWeek");
        types       = inElement.getAttribute("types");
        displayDate = inElement.getAttribute("displayDate");
        index       = inElement.getAttribute("index");
        
        if (startDay != null && !startDay.equalsIgnoreCase(""))
        {
          endDate = startDate;
          endDay  = startDay;
        }
        else
        {
          StringTokenizer s1 = new StringTokenizer(displayDate.trim());
          String OR; 
          boolean startDayDateRetrieved = false; 
          while(s1.hasMoreTokens())
          {
            
            if (!startDayDateRetrieved)
            {
              startDay = s1.nextToken();
              startDay.trim();
              startDate = s1.nextToken();
              startDate.trim();
              startDayDateRetrieved = true;
            }
            else
            {
              OR = s1.nextToken();
              endDay = s1.nextToken();
              endDay.trim();
              endDate = s1.nextToken();
              endDate.trim();
            }
          }
        }
     
        if (startDate.equalsIgnoreCase(endDate))
        {
          displayDate = startDay + " " + startDate;
          isRange     = "false";
        }
        else
        {
          displayDate = startDay + " " + startDate + " - " + endDay + " " + endDate;
          isRange     = "true";
        }
     
        //create the main element
        outElement = (Element)document.createElement("deliveryDate");
  
        //create the startDate element
        node = (Element)document.createElement("startDate");
        node.appendChild(document.createTextNode(startDate) );
        outElement.appendChild(node);
  
        //create the startDate element
        node = (Element)document.createElement("startDay");
        node.appendChild(document.createTextNode(startDay));
        outElement.appendChild(node);
  
        //create the endDate element
        node = (Element)document.createElement("endDate");
        node.appendChild(document.createTextNode(endDate));
        outElement.appendChild(node);
  
        //create the endDay element
        node = (Element)document.createElement("endDay");
        node.appendChild(document.createTextNode(endDay));
        outElement.appendChild(node);
  
        //create the displayDate element
        node = (Element)document.createElement("displayDate");
        node.appendChild(document.createTextNode(displayDate));
        outElement.appendChild(node);
  
        //create the isRange element
        node = (Element)document.createElement("isRange");
        node.appendChild(document.createTextNode( isRange ));
        outElement.appendChild(node);

        //create the types element
        node = (Element)document.createElement("types");
        node.appendChild(document.createTextNode( types ));
        outElement.appendChild(node);

        //create the index element
        node = (Element)document.createElement("index");
        node.appendChild(document.createTextNode( index ));
        outElement.appendChild(node);
  
        deliveryDates.appendChild(outElement);
  
      }
    } // end - if ( nl.getLength() > 0 )


    //Process the shippingMethods
    xpath = "//method";
    nl = DOMUtil.selectNodes(unfShippingData,xpath);

    if ( nl.getLength() > 0 )
    {
      ShipMethod[] methods = new ShipMethod[nl.getLength()]; 
      //create the main element
      outElement = (Element)document.createElement("shippingMethod");
      String cost = "";
      String description = "";
      String code = "";
      for ( int i = 0; i < nl.getLength(); i++ )
      {
        //retrieve the <method ... > 
        inElement = (Element) nl.item(i);
      
        cost = inElement.getAttribute("cost");
        description = inElement.getAttribute("description");
        code = inElement.getAttribute("code");

        methods[i] = new ShipMethod(); 
        methods[i].setCost(cost);
        methods[i].setDescription(description);
        methods[i].setCode(code);

      }

      Arrays.sort(methods, new ShipMethodSortDescription()); 

      for (int i = 0 ; i < nl.getLength(); i++)
      {
        ShipMethod method = methods[i]; 
        //create the <method ...>
        node = (Element)document.createElement("method");
        node.setAttribute("cost", method.getCost());
        node.setAttribute("description", method.getDescription());
        node.setAttribute("code", method.getCode());
        outElement.appendChild(node);
      }

      shippingMethods.appendChild(outElement);
    }

    return document;
  }
 
 
  public Document formatFloristDeliveryDates(DeliveryDateConfiguration configuration,List dates) 
      throws Exception {

    SimpleDateFormat formatter = new SimpleDateFormat( configuration.getSubmitDateFormat() );

    Document document = DOMUtil.getDefaultDocument();
    Element deliveryDates = (Element) document.createElement("deliveryDates");
    document.appendChild(deliveryDates);

    DeliveryDateVO date;
    Element element, node;
		SimpleDateFormat sdfDate = new SimpleDateFormat("MM/dd/yyyy");
		SimpleDateFormat sdfDay = new SimpleDateFormat("EEE");

    for(Iterator i = dates.iterator(); i.hasNext(); ) {

      Object objDate = i.next();
      if(objDate instanceof OEDeliveryDate)
      {
          OEDeliveryDate oeDate = (OEDeliveryDate)objDate;
      
          date = new DeliveryDateVO();
          date.setStartDate(FieldUtils.formatStringToUtilDate(oeDate.getDeliveryDate()));
          date.setEndDate(date.getStartDate());
          date.setRange(false);
          
          if(oeDate.getDeliverableFlag().equalsIgnoreCase("N"))
          {
              continue;
          }
      }
      else
      {
          date = (DeliveryDateVO)objDate;          
      }



      String startDate  = "";
      String startDay   = "";
      String endDate    = "";
      String endDay     = "";
      
      if (date.getStartDate() != null)
      {
        startDate = sdfDate.format(date.getStartDate());
        startDay  = sdfDay.format(date.getStartDate());
      }

      if (date.getEndDate() != null)
      {
        endDate = sdfDate.format(date.getEndDate());
        endDay  = sdfDay.format(date.getEndDate());
      }
      
      //create the main element
      element = (Element)document.createElement("deliveryDate");

      //create the startDate element
      node = (Element)document.createElement("startDate");
      node.appendChild(document.createTextNode(startDate) );
      element.appendChild(node);

      //create the startDate element
      node = (Element)document.createElement("startDay");
      node.appendChild(document.createTextNode(startDay));
      element.appendChild(node);

      //create the endDate element
      node = (Element)document.createElement("endDate");
      node.appendChild(document.createTextNode(endDate));
      element.appendChild(node);

      //create the endDay element
      node = (Element)document.createElement("endDay");
      node.appendChild(document.createTextNode(endDay));
      element.appendChild(node);

      //create the displayDate element
      node = (Element)document.createElement("displayDate");
      node.appendChild(document.createTextNode( date.getDisplayDate() ));
      element.appendChild(node);

      //create the isRange element
      node = (Element)document.createElement("isRange");
      node.appendChild(document.createTextNode( date.isRange()+"" ));
      element.appendChild(node);

      deliveryDates.appendChild(element);
    }
      
    return document;
  } 
 
 
 
/*******************************************************************************************
  * formatDeliveryDates(DeliveryDates) - This method is used to format the delivery dates. 
  ****************************************************************************************** 
  * 
  * Input XML will have the following format:
  * -----------------------------------------
  *		<deliveryDates>
  *			<deliveryDate date="12/13/2005" dayOfWeek="Tue" types="ND:" displayDate="12/13/2005" index="index2"/>
  *		</deliveryDates>
  *
  *
  *
  * Output XML will have the following format:
  * ------------------------------------------
  *		<deliveryDates>
  *			<deliveryDate>
  *				<startDate>12/11/2005</startDate>
  *				<startDay>Sun</startDay>
  *				<endDate>12/11/2005</endDate>
  *				<endDay>Sun</endDay>
  *				<displayDate>Sun 12/11/2005</displayDate>
  *				<isRange>false</isRange>
  *				<types>FL:</types>
  *				<index>index0</index>
  *			</deliveryDate>
  *		</deliveryDates>
  *	
  * 
  ********************************************************************************************/
  /*
  public Document formatFloristDeliveryDates(Document unfShippingData) throws Exception
  {
    Document document = DOMUtil.getDefaultDocument();
    Element deliveryDates = (Element) document.createElement("deliveryDates");
    document.appendChild(deliveryDates);

    Element inElement, outElement, node;
    String xpath;
    NodeList nl;

    //Process the deliveryDates
    xpath = "//deliveryDate";
    nl = unfShippingData.selectNodes(xpath);

    if ( nl.getLength() > 0 )
    {
      String startDate   = "";
      String startDay    = "";
      String endDate     = "";
      String endDay      = "";
      String types       = "";
      String index       = "";
      String displayDate = "";
      String displayWeek = "";
      String isRange     = "";

      for ( int i = 0; i < nl.getLength(); i++ )
      {
        inElement = (Element) nl.item(i);

        startDate   = inElement.getAttribute("date");
        startDay    = inElement.getAttribute("dayOfWeek");
        types       = inElement.getAttribute("types");
        displayDate = inElement.getAttribute("displayDate");
        index       = inElement.getAttribute("index");
        
        if (startDay != null && !startDay.equalsIgnoreCase(""))
        {
          endDate = startDate;
          endDay  = startDay;
        }
        else
        {
          StringTokenizer s1 = new StringTokenizer(displayDate.trim());
          String OR; 
          boolean startDayDateRetrieved = false; 
          while(s1.hasMoreTokens())
          {
            
            if (!startDayDateRetrieved)
            {
              startDay = s1.nextToken();
              startDay.trim();
              startDate = s1.nextToken();
              startDate.trim();
              startDayDateRetrieved = true;
            }
            else
            {
              OR = s1.nextToken();
              endDay = s1.nextToken();
              endDay.trim();
              endDate = s1.nextToken();
              endDate.trim();
            }
          }
        }
     
        if (startDate.equalsIgnoreCase(endDate))
        {
          displayDate = startDay + " " + startDate;
          isRange     = "false";
        }
        else
        {
          displayDate = startDay + " " + startDate + " - " + endDay + " " + endDate;
          isRange     = "true";
        }
     
        //create the main element
        outElement = (Element)document.createElement("deliveryDate");
  
        //create the startDate element
        node = (Element)document.createElement("startDate");
        node.appendChild(document.createTextNode(startDate) );
        outElement.appendChild(node);
  
        //create the startDate element
        node = (Element)document.createElement("startDay");
        node.appendChild(document.createTextNode(startDay));
        outElement.appendChild(node);
  
        //create the endDate element
        node = (Element)document.createElement("endDate");
        node.appendChild(document.createTextNode(endDate));
        outElement.appendChild(node);
  
        //create the endDay element
        node = (Element)document.createElement("endDay");
        node.appendChild(document.createTextNode(endDay));
        outElement.appendChild(node);
  
        //create the displayDate element
        node = (Element)document.createElement("displayDate");
        node.appendChild(document.createTextNode(displayDate));
        outElement.appendChild(node);
  
        //create the isRange element
        node = (Element)document.createElement("isRange");
        node.appendChild(document.createTextNode( isRange ));
        outElement.appendChild(node);

        //create the types element
        node = (Element)document.createElement("types");
        node.appendChild(document.createTextNode( types ));
        outElement.appendChild(node);

        //create the index element
        node = (Element)document.createElement("index");
        node.appendChild(document.createTextNode( index ));
        outElement.appendChild(node);
  
        deliveryDates.appendChild(outElement);
  
      }
    } // end - if ( nl.getLength() > 0 )


    return document;
  } 
 
 */
  /**
   * Get the root of the URL.  This will be hostname and the application path.
   * @return the root of the url.
   */
  public static String getRootURL() throws Exception
  {
  	if (rootURL == null)
  	{
  		StringBuffer sb = new StringBuffer();
      
          // This gives us a big longer of a url, but we just chop off what we don't want
          sb.append(ConfigurationUtil.getInstance().getFrpGlobalParm(COMConstants.COM_CONTEXT,ERROR_PAGE));
          int chopIndex = sb.indexOf("html");
          rootURL = sb.substring(0,chopIndex);
  	}
      return rootURL;
  }

  /**
   * Check product attribute source restriction.
   * @return the error message if applies.
   */
  public String loadProductAttributeExclusions(String productId, String sourceCode, Connection conn) throws Exception
  {
      // Validate product attributes.
      SourceProductUtility spUtil = new SourceProductUtility();
      String productAttrRestricError = spUtil.getProductAttributeRestrictionMsg(sourceCode, productId, conn);
      return productAttrRestricError;
  } 
  
  public String formatMercentOrderNumber(String orderNumber, Connection con){	
	  CustomerDAO customerDAO = new CustomerDAO(con);
	  String masterOrderNumberForMercent=null;
	try {
		masterOrderNumberForMercent = customerDAO.getOrderDetailsByMercentOrdNumber(orderNumber);
	} catch (Exception e) {
		e.printStackTrace();
		logger.error(e);
	}
	  if(masterOrderNumberForMercent != null && !masterOrderNumberForMercent.isEmpty()) {
		  return masterOrderNumberForMercent;
	  }
	  else 
	  {
		  return orderNumber;
	  }
  }
  
	/**
	 * @param requestOrderNumber
	 * @param con
	 * @return
	 */
	private String formatPartnerOrder(String requestOrderNumber, Connection con) {
		CachedResultSet crs = new PartnerUtility().getPartnerOrderInfoByOrderNumber(requestOrderNumber, null, con);
		String orderNumber = requestOrderNumber; 
		if (crs != null && crs.next()) {
			if (crs.getString("MASTER_ORDER_NUMBER") != null){
				orderNumber = crs.getString("MASTER_ORDER_NUMBER");
			}
		}
		return orderNumber;
	}
}