package com.ftd.customerordermanagement.util;
import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.dao.TimerDAO;

import java.sql.Connection;
import java.sql.ResultSet;

import java.util.HashMap;


public class TimerFilter 
{
  private Connection con;
  
  public TimerFilter()
  {
  }

  public TimerFilter(Connection connection)
  {
   this.con = connection;   
  }

  
	/**
	* 
	* 
	* @param request HttpServletRequest
	*
	*/
	public HashMap startTimer(String entityType, String entityId, String commentOrigin,
                            String csrId, String commentOriginType, String callLogId) throws Exception
  {
    HashMap timerMap = new HashMap();

    TimerDAO tDao = new TimerDAO(this.con);    
    
    HashMap timerResults = tDao.insertEntityHistory(entityType, entityId, commentOrigin, 
                                                    csrId, callLogId);
    
    timerMap = buildTimer(timerResults, commentOriginType);
    
    return timerMap;
  }


	/**
	* 
	* 
	* @param request HttpServletRequest
	*
	*/
	private HashMap buildTimer(HashMap timerResults, String commentOriginType) throws Exception
  {
    HashMap timerMap = new HashMap();

    String historyId = (String) timerResults.get("OUT_ENTITY_HISTORY_ID");
    
    timerMap.put(COMConstants.TIMER_ENTITY_HISTORY_ID, historyId);
    timerMap.put(COMConstants.TIMER_COMMENT_ORIGIN_TYPE, commentOriginType);
    
   
    return timerMap;
  }



	/**
	* 
	* 
	* @param request HttpServletRequest
	*
	*/
	public void stopTimer(String entityHistoryId) throws Exception
  {
    TimerDAO tDao = new TimerDAO(this.con);    
    
   tDao.updateEntityHistory(entityHistoryId);
    
  }




}