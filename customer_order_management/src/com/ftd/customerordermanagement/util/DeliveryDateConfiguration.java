package com.ftd.customerordermanagement.util;

import java.sql.Connection;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * Defines the configurable settings for the DeliveryDateUtil.
 * 
 * @author Mark Moon, Software Architects Inc.
 * @version $Revision: 1.2 $
 */
public class DeliveryDateConfiguration {

  public static boolean RANGE_BEFORE = true;
  public static boolean RANGE_AFTER = false;

  private boolean showDateRanges;
  private boolean rangeBeforeOrAfter;
  private boolean checkCutoff;
  private boolean adjustDateRangeIfRangeStartDateBeforeRange;
  private boolean showDateRangeIfRangeStartDateBeforeRange;
  private boolean checkBlockedDatesViaCountryMaintenance;
  private boolean includeRequestedDeliveryDate;
  private String displayDateFormat;
  private String submitDateFormat;

  /**
   * Default constructor that sets default DeliveryDateUtil values:
   * 
   * showDateRanges                             true
   * rangeBeforeOrAfter                         RANGE_BEFORE
   * checkCutoff                                true
   * adjustDateRangeIfRangeStartDateBeforeRange true
   * showDateRangeIfRangeStartDateBeforeRange   true
   * checkBlockedDatesViaCountryMaintenance     true
   * displayDateFormat                          MM/dd/yyyy
   * submitDateFormat                           MM/dd/yyyy
   */
  public DeliveryDateConfiguration() {
    super();

    // set defaults
    showDateRanges = true;
    rangeBeforeOrAfter = RANGE_BEFORE;
    checkCutoff = true;
    adjustDateRangeIfRangeStartDateBeforeRange = true;
    showDateRangeIfRangeStartDateBeforeRange = true;
    checkBlockedDatesViaCountryMaintenance = true;
    displayDateFormat = "MM/dd/yyyy";
    submitDateFormat = "MM/dd/yyyy";
  }


  /**
   * Returns the display date format.  Two date formats are exposed to handle the
   * scenario where the display date is different from the date format which is
   * submitted via a HTML form.
   * @return Returns the displayDateFormat.
   */
  public String getDisplayDateFormat() {
    return displayDateFormat;
  }

  /**
   * Sets the display date format.  Two date formats are exposed to handle the
   * scenario where the display date is different from the date format which is
   * submitted via a HTML form.
   * @param displayDateFormat The displayDateFormat to set.
   */
  public void setDisplayDateFormat( String displayDateFormat ) {
    this.displayDateFormat = displayDateFormat;
  }

  /**
   * Returns the submit date format.  Two date formats are exposed to handle the
   * scenario where the display date is different from the date format which is
   * submitted via a HTML form.
   * @return Returns the submitDateFormat.
   */
  public String getSubmitDateFormat() {
    return submitDateFormat;
  }

  /**
   * Sets the submit date format.  Two date formats are exposed to handle the
   * scenario where the display date is different from the date format which is
   * submitted via a HTML form.
   * @param submitDateFormat The submitDateFormat to set.
   */
  public void setSubmitDateFormat( String submitDateFormat ) {
    this.submitDateFormat = submitDateFormat;
  }

  /**
   * @return Returns the rangeBeforeOrAfter.
   */
  public boolean isRangeBeforeOrAfter() {
    return rangeBeforeOrAfter;
  }

  /**
   * @param rangeBeforeOrAfter The rangeBeforeOrAfter to set.
   */
  public void isRangeBeforeOrAfter( boolean rangeBeforeOrAfter ) {
    this.rangeBeforeOrAfter = rangeBeforeOrAfter;
  }

  /**
   * @return Returns the showDateRangeIfRangeStartDateBeforeRange.
   */
  public boolean isShowDateRangeIfRangeStartDateBeforeRange() {
      return showDateRangeIfRangeStartDateBeforeRange;
  }

  /**
   * @param showDateRangeIfRangeStartDateBeforeRange The showDateRangeIfRangeStartDateBeforeRange to set.
   */
  public void setShowDateRangeIfRangeStartDateBeforeRange(boolean showDateRangeIfRangeStartDateBeforeRange) {
      this.showDateRangeIfRangeStartDateBeforeRange = showDateRangeIfRangeStartDateBeforeRange;
  }

  /**
   * @return Returns the adjustDateRangeIfRangeStartDateBeforeRange.
   */
  public boolean isAdjustDateRangeIfRangeStartDateBeforeRange() {
      return adjustDateRangeIfRangeStartDateBeforeRange;
  }

  /**
   * @param showDateRangeIfRangeStartDateBeforeRange The adjustDateRangeIfRangeStartDateBeforeRange to set.
   */
  public void setAdjustDateRangeIfRangeStartDateBeforeRange(boolean adjustDateRangeIfRangeStartDateBeforeRange) {
      this.adjustDateRangeIfRangeStartDateBeforeRange = adjustDateRangeIfRangeStartDateBeforeRange;
  }

  /**
   * @return Returns the checkCutoff.
   */
  public boolean isCheckCutoff() {
      return checkCutoff;
  }

  /**
   * @param checkCutoff The checkCutoff to set.
   */
  public void setCheckCutoff(boolean checkCutoff) {
      this.checkCutoff = checkCutoff;
  }

  /**
   * @return Returns the showDateRanges.
   */
  public boolean isShowDateRanges() {
      return showDateRanges;
  }

  /**
   * @param showDateRanges The showDateRanges to set.
   */
  public void setShowDateRanges(boolean showDateRanges) {
      this.showDateRanges = showDateRanges;
  }

  /**
   * @return Returns the checkBlockedDatesViaCountryMaintenance.
   */
  public boolean isCheckBlockedDatesViaCountryMaintenance() {
      return checkBlockedDatesViaCountryMaintenance;
  }
  /**
   * @param checkBlockedDatesViaCountryMaintenance true or false if dates blocked via Country Maintenance should be checked.
   */
  public void setCheckBlockedDatesViaCountryMaintenance(boolean checkBlockedDatesViaCountryMaintenance) {
      this.checkBlockedDatesViaCountryMaintenance = checkBlockedDatesViaCountryMaintenance;
  }

  /**
   * String representation of this DeliveryDateConfiguration.
   * @return String
   */
  public String toString() {
    return ToStringBuilder.reflectionToString(this);
  }

  /**
   * @param includeRequestedDeliveryDate true or false if the request delivery date should be forced included into the returned list
   */
  public void setIncludeRequestedDeliveryDate(boolean includeRequestedDeliveryDate) {
    this.includeRequestedDeliveryDate = includeRequestedDeliveryDate;
  }

  /**
   * @return true if the request delivery date should be forced included into the returned list, otherwise false
   */
  public boolean isIncludeRequestedDeliveryDate() {
    return includeRequestedDeliveryDate;
  }
}