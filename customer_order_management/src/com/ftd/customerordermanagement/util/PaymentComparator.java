package com.ftd.customerordermanagement.util;

import java.util.Comparator;

import com.ftd.customerordermanagement.vo.PaymentVO;

public class PaymentComparator implements Comparator<PaymentVO> {
	
	/*
	 * Below are the available pay types in order 
	 * 		P - Paypal
	 * 		C - CrediCard
	 * 		G - Giftcard
	 * 		R - Refund (Not a pay type)
	 * 
	 * ----------------------------------
		P1		P2		Result		
		--------------------------
		P		P		0			
		P		C		-1			
		P		G		-1			
		P		R		-1			
		C		P		1		
		C		C		0			
		C		G		1			
		C		R		-1			
		G		P		1			
		G		C		1			
		G		G		0			
		G		R		-1			
		R		P		1			
		R		C		1			
		R		G		1			
		R		R		0			
		--------------------------
	 * 
	 *
	 */
	@Override
	public int compare(PaymentVO p1, PaymentVO p2) {
		
		if(p1.getPaymentType().equalsIgnoreCase(p2.getPaymentType())) {		//#1
			return 0;
		} 
		else if("P".equalsIgnoreCase(p1.getPaymentType()) && "C".equalsIgnoreCase(p2.getPaymentType())) {
			return -1;
		}
		else if("P".equalsIgnoreCase(p1.getPaymentType()) && "G".equalsIgnoreCase(p2.getPaymentType())) {
			return -1;
		}
		else if("C".equalsIgnoreCase(p1.getPaymentType()) && "G".equalsIgnoreCase(p2.getPaymentType())) {
			return -1;
		}
		else if("C".equalsIgnoreCase(p1.getPaymentType()) && "P".equalsIgnoreCase(p2.getPaymentType())) {
			return 1;
		}
		else {
			return 1;
		}
		
		
	}
}
