package com.ftd.customerordermanagement.util.functors;

import com.ftd.osp.utilities.dataaccess.valueobjects.XMLFormat;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.collections.Transformer;
import org.w3c.dom.Document;

/**
 * 
 * @author Mark Moon, Software Architects Inc.
 * @version $Revision: 1.2 $
 */
public class ResultSetToXmlTransformer 
    implements Transformer {

  public static final String TYPE = "type";
  public static final String ELEMENT = "element";
  public static final String TOP = "top";
  public static final String BOTTOM = "bottom";

  private boolean closeResultSet;  
  private String topName;
  private String bottomName;

  /**
   * Constructs a ResultSetToXmlTransformer using the supplied values.
   * @param cursorName
   * @param topName
   * @param bottomName
   */
  private ResultSetToXmlTransformer(String topName, String bottomName, boolean closeResultSet) {
    this.topName = topName;
    this.bottomName = bottomName;
    this.closeResultSet = closeResultSet;
  }


  /**
   * Returns a Transformer instance.
   * @return a ResultSetToXmlTransformer instance
   * @throws IllegalArgumentException if any of the supplied parameters are null (or whitespace only).
   */
  public static Transformer getInstance(String topName, String bottomName, boolean closeResultSet) {
    // null check topName
    if ( StringUtils.isBlank(topName) ) {
      throw new IllegalArgumentException("Invalid argument, topName was null or whitespace only.");
    }

    // null check bottomName
    if ( StringUtils.isBlank(bottomName) ) {
      throw new IllegalArgumentException("Invalid argument, bottomName was null or whitespace only.");
    }

    return new ResultSetToXmlTransformer(topName, bottomName, closeResultSet);
  }


  /**
   * Transforms a the supplied input CachedResultSet into XML.
   * 
   * @param input CachedResultSet to transform into XML
   * @return XML generated using the supplied input CachedResultSet
   * @throws IllegalArgumentException if the supplied input is not of CachedResultSet or is null
   * @throws java.lang.Exception
   * @see com.ftd.osp.utilities.xml.DOMUtil
   */
  public Object transform(Object input) {
    // null check input
    if ( input == null ) {
      throw new IllegalArgumentException("Invalid argument, input was null.");
    }

    // type check input
    if ( !(input instanceof CachedResultSet) ) {
      throw new IllegalArgumentException("Invalid argument, expecting input type of CachedResultSet");
    }

    // create a CachedResultSet object using the cursor name that was passed. 
    CachedResultSet rs = (CachedResultSet)input;
    Document toReturn;
    try {
      //Create an XMLFormat, and initialize the top and bottom node.  Note that since this method
      //can be/is called by various other methods, the top and botton names MUST be passed to it. 
      XMLFormat xmlFormat = new XMLFormat();
      xmlFormat.setAttribute(TYPE, ELEMENT);
      xmlFormat.setAttribute(TOP, topName);
      xmlFormat.setAttribute(BOTTOM, bottomName );
  
      // call the DOMUtil's converToXMLDOM method
      toReturn = (Document) DOMUtil.convertToXMLDOM(rs, xmlFormat);  
    }
    catch (Exception e) {
      throw new RuntimeException(e);
    }
    finally {
    }

    return toReturn;
  }
}