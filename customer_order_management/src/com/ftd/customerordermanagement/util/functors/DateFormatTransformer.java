package com.ftd.customerordermanagement.util.functors;

import java.util.Date;
import java.text.SimpleDateFormat;
import java.text.ParseException;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.collections.Transformer;

/**
 * DateFormatTransformer transforms the input string to the date format supplied in the getInstance() method.
 * If the supplied input was null, whitespace only, or an exception occurrs during parsing, the input will 
 * be returned.
 * @author Mark Moon, Software Architects Inc.
 * @version $Revision: 1.2 $
 */
public class DateFormatTransformer 
    implements Transformer {
  
  private String inFormat;
  private String outFormat;
  private SimpleDateFormat inFormatter;
  private SimpleDateFormat outFormatter;
  
  /**
   * Constructs a DateFormatTransformer using the supplied parameters to
   * perform the transformation.
   * @param inFormat The format of the String to transform
   * @param outFormat The format of the returned String after
   */
  private DateFormatTransformer(String inFormat, String outFormat) {
    this.inFormat = inFormat;
    this.outFormat = outFormat;
    inFormatter = new SimpleDateFormat(inFormat);
    outFormatter = new SimpleDateFormat(outFormat);
  }


  /**
   * Returns a Transformer instance.
   * @return a ResultSetToXmlTransformer instance
   * @throws IllegalArgumentException if any of the supplied parameters are null (or whitespace only).
   */
  public static Transformer getInstance(String inFormat, String outFormat) {
    // null check inFormat
    if ( StringUtils.isEmpty(inFormat) ) {
      throw new IllegalArgumentException("Invalid argument, inFormat was null or whitespace only.");
    }

    // null check outFormat
    if ( StringUtils.isEmpty(outFormat) ) {
      throw new IllegalArgumentException("Invalid argument, outFormat was null or whitespace only.");
    }

    return new DateFormatTransformer(inFormat, outFormat);
  }

  /**
   * Transforms the input string to the date format supplied in the getInstance() method.  If the supplied
   * input was null, whitespace only, or an exception occurrs during parsing, the input will be returned.
   * @param input String to format
   * @return String formatted using 'outFormat' or 'input' if null, whitespace only, or an expection
   * occurs during parsing/formatting
   * @throws IllegalArgumentException if input is not of type java.lang.String
   */
  public Object transform(Object input) {
    // type check input
    if ( !(input instanceof String) ) {
      throw new IllegalArgumentException("Invalid argument: expecting input type of java.lang.String");
    }

    String toReturn = "";
    String toTransform = (String)input;
    if ( StringUtils.isNotEmpty(toTransform) ) {
      try {

        // parse the input String into a Date
        Date parsedDate = inFormatter.parse(toTransform);

        // format the Date using the outFormat
        toReturn = outFormatter.format(parsedDate);
      }

      // return what was passed in
      catch (ParseException pe) {
        toReturn = toTransform;
      }
    }
    
    // return what was passed in
    else {
      toReturn = toTransform;
    }

    return toReturn;
  }
}