package com.ftd.customerordermanagement.util.functors;

import java.util.Calendar;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.CountryMasterVO;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.collections.Predicate;

/**
 * AllDaysBlockedViaCountryMaintenancePredicate checks to see if all days are blocked via Country Maintenace.
 * 
 * @author Mark Moon, Software Architects Inc.
 * @version $Revision: 1.3 $
 */
public class AllDaysBlockedViaCountryMaintenancePredicate 
    implements Predicate {

  // singleton instance
  private static final Predicate INSTANCE = new AllDaysBlockedViaCountryMaintenancePredicate();
  private static final String FULL_BLOCK = "F";

  /*
   * Private constructor enforces singleton pattern.
   */
  private AllDaysBlockedViaCountryMaintenancePredicate() {
    super();
  }

  /**
   * Returns a Predicate instance.
   * @return a AllDaysBlockedViaCountryMaintenancePredicate instance
   */
  public static Predicate getInstance() {
    return INSTANCE;
  }

  /**
   * Determines if all days are blocked via country maintenance.  If country maintenance has a value of 
   * 'N', the day is not closed, otherwise, the day is closed.
   *
   * @param input The CountryMasterVO to check for all days being blocked.
   * @return true if all days are blocked, otherwise, false
   * @throws IllegalArgumentException if the input is not of type com.ftd.osp.utilities.cache.handlers.vo.CountryMasterVO
   */
  public boolean evaluate(Object input) {

    // type check input
    if ( !(input instanceof CountryMasterVO) ) {
        throw new IllegalArgumentException("Excepting type of: com.ftd.osp.utilities.cache.handlers.vo.CountryMasterVO");
    }

    CountryMasterVO CountryMasterVO = (CountryMasterVO)input;
    return  StringUtils.equals(CountryMasterVO.getSundayClosed(), FULL_BLOCK) &&
              StringUtils.equals(CountryMasterVO.getMondayClosed(), FULL_BLOCK) &&
              StringUtils.equals(CountryMasterVO.getTuesdayClosed(), FULL_BLOCK) &&
              StringUtils.equals(CountryMasterVO.getWednesdayClosed(), FULL_BLOCK) &&
              StringUtils.equals(CountryMasterVO.getThursdayClosed(), FULL_BLOCK) &&
              StringUtils.equals(CountryMasterVO.getFridayClosed(), FULL_BLOCK) &&
              StringUtils.equals(CountryMasterVO.getSaturdayClosed(), FULL_BLOCK);
  }
}