package com.ftd.customerordermanagement.util.functors;

import java.util.Calendar;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.CountryMasterVO;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.collections.Predicate;

/**
 * IsDayBlockedViaCountryMaintenacePredicate determines if a day is blocked via Country Maintenance.
 *
 * @author Mark Moon, Software Architects Inc.
 * @version $Revision: 1.3 $
 */
public class IsDayBlockedViaCountryMaintenacePredicate
        implements Predicate {

    private CountryMasterVO CountryMasterVO;

    /**
     * Private constructor enforces singleton pattern.
     */
    private IsDayBlockedViaCountryMaintenacePredicate(CountryMasterVO CountryMasterVO) {
        super();
        this.CountryMasterVO = CountryMasterVO;
    }

    /**
     * Returns a Predicate instance.
     * @return a IsDayBlockedViaCountryMaintenacePredicate instance
     */
    public static Predicate getInstance(CountryMasterVO CountryMasterVO) {

        // null check CountryMasterVO
        if ( CountryMasterVO == null ){
          throw new IllegalArgumentException("Invalid argument, CountryMasterVO was null.");
        }

        return new IsDayBlockedViaCountryMaintenacePredicate(CountryMasterVO);
    }

    /**
     * Determines if a day is blocked via country maintenance.  If country maintenance has a value of 
     * 'N', the day is not closed, otherwise, the day is closed.
     *
     * @param input The Calendar object representing the day to check against country maintenance blocked days.
     * @return true if the day is blocked, otherwise, false
     * @throws IllegalArgumentException if the input is not of type java.util.Calendar
     */
    public boolean evaluate(Object input) {

        // type check input
        if ( !(input instanceof Calendar) ) {
            throw new IllegalArgumentException("Excepting type of java.util.Calendar");
        }

        Calendar calendar = (Calendar)input;

        String isClosed;
        switch ( calendar.get( Calendar.DAY_OF_WEEK ) ) {
          case Calendar.SUNDAY:
            isClosed = CountryMasterVO.getSundayClosed();
            break;
          case Calendar.MONDAY:
            isClosed = CountryMasterVO.getMondayClosed();
            break;
          case Calendar.TUESDAY:
            isClosed = CountryMasterVO.getTuesdayClosed();
            break;
          case Calendar.WEDNESDAY:
            isClosed = CountryMasterVO.getWednesdayClosed();
            break;
          case Calendar.THURSDAY:
            isClosed = CountryMasterVO.getThursdayClosed();
            break;
          case Calendar.FRIDAY:
            isClosed = CountryMasterVO.getFridayClosed();
            break;
          case Calendar.SATURDAY:
            isClosed = CountryMasterVO.getSaturdayClosed();
            break;
          default:
            isClosed = "N";
        }

        return StringUtils.equals(isClosed, "F");
    }
}