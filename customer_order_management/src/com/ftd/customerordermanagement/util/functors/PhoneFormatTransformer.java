package com.ftd.customerordermanagement.util.functors;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.collections.Transformer;

/**
 * Formats a input String into a correctly formatted phone number: '(###)###-####'.
 * If the input string is null or not 10 characters, the input string will be
 * returned as is.
 * 
 * @author Mark Moon, Software Architects Inc.
 * @version $Revision: 1.2 $
 */
public class PhoneFormatTransformer 
    implements Transformer {
  
  private static final Transformer INSTANCE = new PhoneFormatTransformer();
  
  /**
   * Defalut constructor.
   */
  protected PhoneFormatTransformer() {
    super();
  }


  /**
   * Returns a Transformer instance.
   * @return a PhoneFormatTransformer instance
   */
  public static Transformer getInstance() {
    return INSTANCE;
  }


  /**
   * Transforms the input string to '(###)###-####' format.
   * @param input String to format
   * @return String (###)###-####
   * @throws IllegalArgumentException if input is not of type java.lang.String
   */
  public Object transform(Object input) {
    // type check input
    if ( !(input instanceof String) ) {
      throw new IllegalArgumentException("Invalid argument: expecting input type of java.lang.String");
    }

    String toFormat = (String)input;
    String toReturn = toFormat;

    if ( StringUtils.isNotEmpty(toFormat) && toFormat.length() == 10) {
      toReturn = "(" + toFormat.substring(0,3) + ")" + toFormat.substring(3, 6) + "-" + toFormat.substring(6);
    }

    return toReturn;
  }
}