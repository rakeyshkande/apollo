package com.ftd.customerordermanagement.util.functors;

import com.ftd.osp.utilities.xml.DOMUtil;



import org.apache.commons.collections.Closure;
import org.apache.commons.collections.Transformer;
import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;

/**
 * Using the XPath input, this method updates the node value using the supplied Transformer.  If
 * the XPath is null or whitespace only, no update will occur.  If an exception occurrs
 * when trying to update, the node will maintain its original value.
 * 
 * @author Mark Moon, Software Architects Inc.
 * @version $Revision: 1.3 $
 */
public class NodeUpdateClosure 
    implements Closure {

  private Transformer transformer;
  private Document document;

  /*
   * Constructs a NodeUpdateClosure using the supplied parameters.
   * @param transformer
   * @param document
   */
  private NodeUpdateClosure(Transformer transformer, Document document) {
    this.transformer = transformer;
    this.document = document;
  }


  /**
   * Returns a Closure instance.
   * @return a ResultSetToXmlTransformer instance
   * @throws IllegalArgumentException if any of the supplied parameters are null (or whitespace only).
   */
  public static Closure getInstance(Transformer transformer, Document document) {
    // null check topName
    if ( transformer == null ) {
      throw new IllegalArgumentException("Invalid argument: transformer was null.");
    }

    // null check document
    if ( document == null ) {
      throw new IllegalArgumentException("Invalid argument: document was null.");
    }

    return new NodeUpdateClosure(transformer, document);
  }


  /**
   * Using the XPath input, this method updates the node value using the supplied Transformer.  If
   * the XPath is null or whitespace only, no update will occur.  If an exception occurrs
   * when trying to update, the node will maintain its original value.
   *
   * @param input (java.lang.String) XPath to location of node to update
   * @throws IllegalArgumentException if the input is not of type java.lang.String
   */
  public void execute(Object input) {
    // type check input
    if ( !(input instanceof String) ) {
     throw new IllegalArgumentException("Invalid argument: input was not of type java.lang.String.");
    }

    String xPath = (String)input;
    if ( StringUtils.isNotEmpty(xPath) ) {
      try {
        Object transformed = transformer.transform( DOMUtil.getNodeText( document, xPath ) );
        if ( transformed != null ) {
          DOMUtil.updateNodeValue( document, xPath,  transformed.toString());
        }
      }
      catch (Exception ex) {
        // do nothing, leave original value unchanged
      }
    }
    else {
      // do nothing, leave original value unchanged
    }
  }
}