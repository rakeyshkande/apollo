package com.ftd.customerordermanagement.util;

public class ShipMethod
{
  private String cost;
  private String description;
  private String code;

  public void setCost(String cost)
  {
    this.cost = cost;
  }

  public String getCost()
  {
    return cost;
  }

  public void setDescription(String description)
  {
    this.description = description;
  }

  public String getDescription()
  {
    return description;
  }

  public void setCode(String code)
  {
    this.code = code;
  }

  public String getCode()
  {
    return code;
  }


}
