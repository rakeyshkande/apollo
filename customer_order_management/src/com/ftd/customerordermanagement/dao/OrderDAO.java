package com.ftd.customerordermanagement.dao;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.ftd.customerordermanagement.vo.OrderBillVO;
import com.ftd.customerordermanagement.vo.OrderDetailHoldVO;
import com.ftd.customerordermanagement.vo.OrderDetailVO;
import com.ftd.customerordermanagement.vo.OrderVO;
import com.ftd.customerordermanagement.vo.PaymentTotalVO;
import com.ftd.customerordermanagement.vo.PaymentVO;
import com.ftd.customerordermanagement.vo.SourceCodeVO;
import com.ftd.messaging.constants.MessagingConstants;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.MembershipDetailsVO;
import com.ftd.osp.utilities.xml.DOMUtil;

/**
 * OrderDAO
 * This is the DAO that handles loading order comments, loading order detail comments, inserting order
 * comments and viewing detail IDs for the Order Comments functionality
 *
 * @author Srinivas Makam
 * @version $Id: OrderDAO.java
 */

public class OrderDAO
{
    private Connection connection;
    private static Logger logger  = new Logger("com.ftd.customerordermanagement.dao.OrderDAO");

  /**
   * Constructor
   * @param connection Connection
   */
  public OrderDAO(Connection connection)
  {
    this.connection = connection;
  }

  /**
   * This method returns a set of order detail ids
   * @param orderGuid
   * @return
   * @throws java.lang.Exception
   */
  public CachedResultSet viewDetailIds(String orderGuid) throws Exception
  {
        // get the list of order detail ids for this order guid
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("GET_CART_ORDERS");
        dataRequest.addInputParam("IN_ORDER_GUID",orderGuid);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        CachedResultSet idSet = (CachedResultSet) dataAccessUtil.execute(dataRequest);
        
        return idSet;
  }

  /**
   * This method returns order hold information
   * @param orderGuid
   * @return
   * @throws java.lang.Exception
   */
  public ArrayList getHold(long orderDetailId) throws Exception
  {
     // get the list of order detail ids for this order guid
     DataRequest dataRequest = new DataRequest();
     dataRequest.setConnection(connection);
     dataRequest.setStatementID("GET_ORDER_HOLD");
     dataRequest.addInputParam("IN_ORDER_DETAIL_ID", String.valueOf(orderDetailId));
     OrderDetailHoldVO odhVO = new OrderDetailHoldVO();
     ArrayList searchResults = new ArrayList();

     DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
     CachedResultSet outputs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
     while (outputs.next())
     {
       odhVO = new OrderDetailHoldVO();
       odhVO.setOrderDetailId(new Long(outputs.getString("order_detail_id")).longValue());
       odhVO.setReason(outputs.getString("reason"));
       odhVO.setReasonText(outputs.getString("reason_text"));
       odhVO.setStatus(outputs.getString("status"));
       searchResults.add(odhVO);
     }
     return searchResults;
  }

   public void deleteOrderHold(long orderDetailId, String reason)
        throws Exception
  {
      DataRequest dataRequest = new DataRequest();
      Map outputs = null;

      /* setup store procedure input parameters */
      HashMap inputParams = new HashMap();
      inputParams.put("IN_ORDER_DETAIL_ID", String.valueOf(orderDetailId));
      inputParams.put("IN_REASON", reason);

      /* build DataRequest object */
      dataRequest.setConnection(connection);
      dataRequest.setStatementID("DELETE_ORDER_HOLD");
      dataRequest.setInputParams(inputParams);

      /* execute the store prodcedure */
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      outputs = (Map) dataAccessUtil.execute(dataRequest);
      
      /* read store prodcedure output parameters to determine
       * if the procedure executed successfully */
      String status = (String) outputs.get("OUT_STATUS_PARAM");
      if(status != null && status.equalsIgnoreCase("N"))
      {
        String message = (String) outputs.get("OUT_MESSAGE_PARAM");
        throw new Exception(message);
      }
    }
  /**
   * This method loads all the history records for this order guid
   * @param orderGuid
   * @param position
   * @param maxRecords
   * @param managerRole
   * @return HashMap containing all the history records for this order detail id
   * @throws java.lang.Exception
   */
  public HashMap loadOrderHistoryXML(String orderDetailId, int position, int maxRecords, String managerRole) throws Exception
  {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("GET_ORDER_HISTORY");
        dataRequest.addInputParam("IN_ORDER_DETAIL_ID", orderDetailId);
        dataRequest.addInputParam("IN_START_POSITION", new Long(position));
        dataRequest.addInputParam("IN_MAX_NUMBER_RETURNED", new Long(maxRecords));

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        HashMap results = (HashMap) dataAccessUtil.execute(dataRequest);
        return results;
  }

  public void updateOrderDisposition(long orderDetailId, String newDisposition, String updatedBy) throws Exception
  {
      DataRequest dataRequest = new DataRequest();
      Map outputs = null;

      /* setup store procedure input parameters */
      HashMap inputParams = new HashMap();
      inputParams.put("IN_ORDER_DETAIL_ID", new Long(orderDetailId));
      inputParams.put("IN_DISPOSITION", newDisposition);
      inputParams.put("IN_UPDATED_BY", updatedBy);

      /* build DataRequest object */
      dataRequest.setConnection(connection);
      dataRequest.setStatementID("UPDATE_ORDER_DISPOSITION");
      dataRequest.setInputParams(inputParams);

      /* execute the store prodcedure */
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      outputs = (Map) dataAccessUtil.execute(dataRequest);
      
      /* read store prodcedure output parameters to determine
       * if the procedure executed successfully */
      String status = (String) outputs.get("OUT_STATUS_PARAM");
      if(status != null && status.equalsIgnoreCase("N"))
      {
        String message = (String) outputs.get("OUT_MESSAGE_PARAM");
        throw new Exception(message);
      }
    }

  /**
   * method to search for a shopping cart based on the input data
   *
   * @param session id
   * @param customer service representative id
   * @param master order number
   * @param order guid
   * @param start position
   * @param maxRecords
   *
   * @return HashMap containing cursors and output parameters
   *
   * @throws java.lang.Exception
   */

  public HashMap getCartInfo( String sessionId, String csrId, String masterOrderNumber,
                              String orderGuid, long startPosition, long recordsToBeReturned)
          throws Exception
  {
    HashMap searchResults = new HashMap();
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(this.connection);
    dataRequest.setStatementID("GET_CART_INFO");

    dataRequest.addInputParam("IN_ORDER_NUMBER", 		      masterOrderNumber);
    dataRequest.addInputParam("IN_START_POSITION", 		    new Long(startPosition));
    dataRequest.addInputParam("IN_MAX_NUMBER_RETURNED",   new Long(recordsToBeReturned));
    dataRequest.addInputParam("IN_SESSION_ID", 	  	      sessionId);
    dataRequest.addInputParam("IN_CSR_ID", 		            csrId);
    dataRequest.addInputParam("IN_ORDER_GUID",            orderGuid);

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      
    searchResults = (HashMap) dataAccessUtil.execute(dataRequest);

    return searchResults;

  }


  /**
   * method to search for a shopping cart based on the input data
   *
   * @param session id
   * @param customer service representative id
   * @param master order number
   * @param order guid
   * @param start position
   * @param maxRecords
   *
   * @return HashMap containing cursors and output parameters
   *
   * @throws java.lang.Exception
   */

  public HashMap getOrderInfoForPrint(String orderDetailId, String includeComments, String processingId)
          throws Exception
  {
    HashMap searchResults = new HashMap();
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(this.connection);
    dataRequest.setStatementID("GET_ORDER_INFO_FOR_PRINT");

    dataRequest.addInputParam("IN_ORDER_DETAIL_ID",       orderDetailId);
    dataRequest.addInputParam("IN_INCLUDE_COMMENTS",	    includeComments);
    dataRequest.addInputParam("IN_PROCESSING_ID",   	    processingId);

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      
    searchResults = (HashMap) dataAccessUtil.execute(dataRequest);

    return searchResults;

  }



  /**
   * This method returns a CachedResultSet which will contain the order info
   * @param orderNumber
   * @return
   * @throws java.lang.Exception
   */
  public CachedResultSet getOrderArchive(String masterOrderNumber) throws Exception
  {
        // get the list of order detail ids for this order guid
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("VIEW_ORDER_XML_ARCHIVE");
        dataRequest.addInputParam("IN_MASTER_ORDER_NUMBER",masterOrderNumber);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

        //Since there is only 1 cursor, the data access util will NOT return a hashmap.  Instead,
        //it will perform the GET on the HashMap and return that object.
        CachedResultSet searchResults = (CachedResultSet) dataAccessUtil.execute(dataRequest);
        
        return searchResults;
  }

/**
 * Returns product info in the form of XML.  Accepts a product id, novator id,
 * or product subcode id.
 * @param productId
 * @return Document
 * @throws java.lang.Exception
 */
  public Document getProductInfo(String productId) throws Exception
  {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("GET_PRODUCT_BY_NOV_OR_SUB");
        dataRequest.addInputParam("IN_PRODUCT_ID", productId);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Document productInfo = (Document) dataAccessUtil.execute(dataRequest);
        
        return productInfo;
  }

  /**
   * method for
   *
   *
   * @return Document containing the values from the cursor
   *
   * @throws java.lang.Exception
   */


  public Document getSourceCodeInfo(String sourceCode)
        throws Exception
  {
        Document searchResults = DOMUtil.getDocument();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("GET_SOURCE_CODE_INFO");

        dataRequest.addInputParam("IN_SOURCE_CODE",    sourceCode);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      
        searchResults = (Document) dataAccessUtil.execute(dataRequest);

        return searchResults;

  }


  /**
   * Obtains source code information
   *
   *
   * @return SourceCodeVO
   *
   * @throws java.lang.Exception
   */
  public SourceCodeVO getSourceCodeRecord(String sourceCode)
        throws Exception
  {
        CachedResultSet searchResults = null;
		SourceCodeVO scVO = new SourceCodeVO();
		DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("GET_SOURCE_CODE_RECORD");

        dataRequest.addInputParam("IN_SOURCE_CODE",    sourceCode);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      
        searchResults = (CachedResultSet) dataAccessUtil.execute(dataRequest);
		
		//  Create a SourceCodeVO
		if(searchResults.next())
		{
			scVO.setMarketingGroup(searchResults.getString("MARKETING_GROUP"));
			scVO.setValidPayMethod(searchResults.getString("VALID_PAY_METHOD"));
			scVO.setPartnerId(searchResults.getString("PARTNER_ID"));
		}
		else
		{
			throw new Exception("No data found for source code:" + sourceCode);
		}

        return scVO;
  }


  /**
   * Obtains a flag signifying if a gift certificate payment is allowed
   * for the given source code
   *
   * @return boolean - If true a gift certificate payment may be used for the
   * given source code
   *
   * @throws java.lang.Exception
   */
  public boolean getBillingInfoForCertificate(String sourceCode)
        throws Exception
  {
        CachedResultSet searchResults = null;
		SourceCodeVO scVO = new SourceCodeVO();
		String flag = null;
		boolean biGcFlag = false;

		DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("GET_BILLING_INFO_CERTIFICATE");

        dataRequest.addInputParam("IN_SOURCE_CODE",    sourceCode);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      
        searchResults = (CachedResultSet) dataAccessUtil.execute(dataRequest);
		
		if(searchResults.next())
		{
			flag = searchResults.getString("CERTIFICATE_EXISTS");
		}

		biGcFlag = ((flag != null) && (flag.equalsIgnoreCase("Y")))? true: false;

        return biGcFlag;
  }
  /**
   * Obtains a flag signifying if a corporate purchase is allowed
   * for the given source code
   *
   * @return boolean - If true a corporate purchase may be used for the
   * given source code
   *
   * @throws java.lang.Exception
   */
  public boolean getBillingInfoForNonCertificate(String sourceCode)
        throws Exception
  {
        CachedResultSet searchResults = null;
		SourceCodeVO scVO = new SourceCodeVO();
		boolean corpPurch = false;

		DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("GET_BILLING_INFO_NONCERTIFICATE");

        dataRequest.addInputParam("IN_SOURCE_CODE",    sourceCode);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      
        searchResults = (CachedResultSet) dataAccessUtil.execute(dataRequest);
		
		if(searchResults.next())
		{
			corpPurch = true;
		}

        return corpPurch;
  }

  /**
   * method for
   *
   *
   * @return Document containing the values from the cursor
   *
   * @throws java.lang.Exception
   */


  public Document getPaymentInfo(String functionName, String payInfoNumber)
        throws Exception
  {
        Document searchResults = DOMUtil.getDocument();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        if (functionName.equalsIgnoreCase("cart"))
        {
          dataRequest.setStatementID("GET_CART_PAYMENTS");
          dataRequest.addInputParam("IN_ORDER_GUID",      payInfoNumber);
        }
        else
        {
          dataRequest.setStatementID("GET_ORDER_PAYMENTS");
          dataRequest.addInputParam("IN_ORDER_DETAIL_ID", payInfoNumber);
        }


        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      
        searchResults = (Document) dataAccessUtil.execute(dataRequest);

        return searchResults;

  }

  public CachedResultSet getOrderCustomerInfo(String orderDetailId) throws Exception
  {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("GET_ORDER_CUSTOMER_INFO");
        dataRequest.addInputParam("IN_ORDER_DETAIL_ID",orderDetailId);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
        return rs;
  }

  public CachedResultSet getOrderRecipientInfo(String orderDetailId) throws Exception
  {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("GET_OD_CUST_DETAILS");
        dataRequest.addInputParam("IN_ORDER_DETAIL_ID",orderDetailId);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
        return rs;
  }

  public CachedResultSet getOrderDetailsInfo(String orderDetailId) throws Exception
  {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("GET_ORDER_DETAILS");
        dataRequest.addInputParam("IN_ORDER_DETAIL_ID", orderDetailId);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
        return rs;
  }

  public HashMap loadCartEmailsXML(String orderGuid, long startPosition, long recordsToBeReturned) throws Exception
  {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("GET_EMAIL_FOR_CART_UPDATE");
        dataRequest.addInputParam("IN_ORDER_GUID", orderGuid);
        dataRequest.addInputParam("IN_START_POSITION", new Long(startPosition));
        dataRequest.addInputParam("IN_MAX_NUMBER_RETURNED", new Long(recordsToBeReturned));

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        HashMap results = (HashMap) dataAccessUtil.execute(dataRequest);
        return results;
  }

  public void updateCartEmail(String orderGuid, String emailAddress, String updatedBy) throws Exception
  {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);

        dataRequest.addInputParam("IN_ORDER_GUID", orderGuid);
        dataRequest.addInputParam("IN_EMAIL_ADDRESS", emailAddress);
        dataRequest.addInputParam("IN_UPDATED_BY", updatedBy);
        dataRequest.setStatementID("UPDATE_CART_EMAIL");

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
        String status = (String) outputs.get("OUT_STATUS_PARAM");
        if(status != null && status.equalsIgnoreCase("N"))
        {
            String message = (String) outputs.get("OUT_MESSAGE_PARAM");
            throw new Exception(message);
        }
  }

  /**
   * This method is a wrapper for the SP_GET_ORDER SP.
   * It populates a Order VO based on the returned record set.
   *
   * @param String - orderGuid
   * @return OrderVO
   */
  public OrderVO getOrder(String orderGuid) throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    OrderVO order = new OrderVO();
    boolean isEmpty = true;
    CachedResultSet outputs = null;
    SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    try
    {
      logger.debug("getOrder (String orderGuid) :: OrderVO");
      /* setup store procedure input parameters */
      HashMap inputParams = new HashMap();
      inputParams.put("ORDER_GUID", orderGuid);

      /* build DataRequest object */
      dataRequest.setConnection(connection);
      dataRequest.setStatementID("SP_GET_ORDER_BY_GUID");
      dataRequest.setInputParams(inputParams);

      /* execute the store prodcedure */
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      outputs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
      
      /* populate object */
      while (outputs.next())
      {
        isEmpty = false;
        order.setMasterOrderNumber(outputs.getString("MASTER_ORDER_NUMBER"));
        order.setCustomerId(outputs.getLong("CUSTOMER_ID"));
        order.setMembershipId(outputs.getLong("MEMBERSHIP_ID"));
        order.setCompanyId(outputs.getString("COMPANY_ID"));
        order.setSourceCode(outputs.getString("SOURCE_CODE"));
        order.setOriginId(outputs.getString("ORIGIN_ID"));
        order.setOrderTotal(outputs.getDouble("ORDER_TOTAL"));
        order.setProductTotal(outputs.getDouble("PRODUCT_TOTAL"));
        order.setAddOnTotal(outputs.getDouble("ADD_ON_TOTAL"));
        order.setServiceFeeTotal(outputs.getDouble("SERVICE_FEE_TOTAL"));
        order.setShippingFeeTotal(outputs.getDouble("SHIPPING_FEE_TOTAL"));
        order.setDiscountTotal(outputs.getDouble("DISCOUNT_TOTAL"));
        order.setTaxTotal(outputs.getDouble("TAX_TOTAL"));
        order.setLossPrevention(outputs.getString("loss_prevention_indicator"));
        Date od = outputs.getDate("order_date");
        if (od != null) {
          Calendar odc = Calendar.getInstance();
          odc.setTime(od);
          order.setOrderDate(odc);
        }
        order.setBuyerSignedInFlag(outputs.getString("buyer_signed_in_flag"));
      }
    }
    catch (Exception e)
    {
      logger.error(e);
      throw e;
    }
    if (isEmpty)
      return null;
    else
      return order;
  }

   /**
   * method for getting shipping methods by order Detail Id.
   * @return Document containing the values from the cursor
   * @throws java.lang.Exception
   */
  public Document getShipMethod(String orderDetailId)
        throws Exception
  {
        Document searchResults = DOMUtil.getDocument();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("GET_SHIPPING_METHODS");
        dataRequest.addInputParam("IN_ORDER_DETAIL_ID", orderDetailId);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        searchResults = (Document) dataAccessUtil.execute(dataRequest);

        return searchResults;

  }
    /**
   * method for obtaining original and modifed order amounts
   * @params orderDetailId
   * @return Document containing the values from the cursor
   * @throws java.lang.Exception
   */

  public HashMap getOriginalAndModifiedOrder(String orderDetailId)
        throws Exception
  {
        HashMap searchResults = new HashMap();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("VIEW_ORDER_TOTALS");
        dataRequest.addInputParam("IN_ORDER_DETAIL_ID", orderDetailId);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        searchResults = (HashMap) dataAccessUtil.execute(dataRequest);
        return searchResults;
  }

   /**
   * This method is responsible for populating and returning payment total VOs.
   * It populates a PaymentTotalVO based on the returned record set.
   *
   * @param String - orderDetailId
   * @return PaymentTotalVO
   */
  public ArrayList getPaymentTotals(String orderDetailId) throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    PaymentTotalVO ptVO = new PaymentTotalVO();
    ArrayList results = new ArrayList();
    boolean isEmpty = true;
    CachedResultSet outputs = null;

    try
    {
      logger.debug("getPaymentTotals (String orderDetailId) :: PaymentTotalVO");
      /* setup store procedure input parameters */
      HashMap inputParams = new HashMap();
      inputParams.put("IN_ORDER_DETAIL_ID", orderDetailId);

      /* build DataRequest object */
      dataRequest.setConnection(connection);
      dataRequest.setStatementID("VIEW_PAYMENT_TOTALS");
      dataRequest.setInputParams(inputParams);

      /* execute the store prodcedure */
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      outputs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
      
      /* populate object */
      while (outputs.next())
      {
        isEmpty = false;
				ptVO.setPaymentType(outputs.getString("payment_type"));
        ptVO.setPaymentId(outputs.getString("payment_type_id"));
        ptVO.setProductAmt(outputs.getDouble("product_amount"));
        ptVO.setAddOnAmt(outputs.getDouble("add_on_amount"));
        ptVO.setServShipFee(outputs.getDouble("serv_ship_fee"));
        ptVO.setDiscountamt(outputs.getDouble("discount_amount"));
        ptVO.setTax(outputs.getDouble("tax"));
        ptVO.setOrderTotal(outputs.getDouble("order_total"));
        ptVO.setServiceFee(outputs.getDouble("service_fee"));
        ptVO.setShippingFee(outputs.getDouble("shipping_fee"));
        ptVO.setShippingTax(outputs.getDouble("shipping_tax"));
        ptVO.setServiceFeeTax(outputs.getDouble("service_fee_tax"));
        ptVO.setProductTax(outputs.getDouble("product_tax"));
        results.add(ptVO);
      }
    }
    catch (Exception e)
    {
      logger.error(e);
      throw e;
    }
    if (isEmpty)
      return null;
    else
      return results;
  }


    /**
     * Update the order detail for an order
     * @param orderDetailID - String
     * @param floristID - String
     * @param csr_id - String
     * @return n/a
     * @throws IOException, SAXException, ParserConfigurationException,
        SQLException, Exception
     */
    public void updateFloristId(String orderDetailID, String floristID,String csrId)
        throws Exception
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering modifyOrder");
            logger.debug("orderDetailID: " + orderDetailID);
            logger.debug("floristID: " + floristID);

        }
         DataRequest request = new DataRequest();

        try {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();


            inputParams.put(MessagingConstants.UPDATE_ORDER_DETAILS_IN_ORDER_DETAIL_ID,
                new Long(orderDetailID));
            inputParams.put(MessagingConstants.UPDATE_ORDER_DETAILS_IN_DELIVERY_DATE,
                null);
            inputParams.put(MessagingConstants.UPDATE_ORDER_DETAILS_IN_RECIPIENT_ID,
                null);
            inputParams.put(MessagingConstants.UPDATE_ORDER_DETAILS_IN_PRODUCT_ID,
                null);
            inputParams.put(MessagingConstants.UPDATE_ORDER_DETAILS_IN_QUANTITY,
                null);
            inputParams.put(MessagingConstants.UPDATE_ORDER_DETAILS_IN_COLOR_1,
                null);
            inputParams.put(MessagingConstants.UPDATE_ORDER_DETAILS_IN_COLOR_2,
                null);
            inputParams.put(MessagingConstants.UPDATE_ORDER_DETAILS_IN_SUBSTITUTION_INDICATOR,
                null);
            inputParams.put(MessagingConstants.UPDATE_ORDER_DETAILS_IN_SAME_DAY_GIFT,
                null);
            inputParams.put(MessagingConstants.UPDATE_ORDER_DETAILS_IN_OCCASION,
                null);
            inputParams.put(MessagingConstants.UPDATE_ORDER_DETAILS_IN_CARD_MESSAGE,
                null);
            inputParams.put(MessagingConstants.UPDATE_ORDER_DETAILS_IN_CARD_SIGNATURE,
                null);
            inputParams.put(MessagingConstants.UPDATE_ORDER_DETAILS_IN_SPECIAL_INSTRUCTIONS,
                null);
            inputParams.put(MessagingConstants.UPDATE_ORDER_DETAILS_IN_RELEASE_INFO_INDICATOR,
                null);
            inputParams.put(MessagingConstants.UPDATE_ORDER_DETAILS_IN_FLORIST_ID,
                floristID);
            inputParams.put(MessagingConstants.UPDATE_ORDER_DETAILS_IN_SHIP_METHOD,
                null);
            inputParams.put(MessagingConstants.UPDATE_ORDER_DETAILS_IN_SHIP_DATE,
                null);
            inputParams.put(MessagingConstants.UPDATE_ORDER_DETAILS_IN_ORDER_DISP_CODE,
                null);
            inputParams.put(MessagingConstants.UPDATE_ORDER_DETAILS_IN_SECOND_CHOICE_PRODUCT,
                null);
            inputParams.put(MessagingConstants.UPDATE_ORDER_DETAILS_IN_ZIP_QUEUE_COUNT,
                null);
            inputParams.put(MessagingConstants.UPDATE_ORDER_DETAILS_IN_RANDOM_WEIGHT_DIST_FAILURES,
                null);
            inputParams.put(MessagingConstants.UPDATE_ORDER_DETAILS_IN_UPDATED_BY,
                csrId);
            inputParams.put(MessagingConstants.UPDATE_ORDER_DETAILS_IN_DELIVERY_DATE_RANGE_END,
                null);
            inputParams.put(MessagingConstants.UPDATE_ORDER_DETAILS_IN_SCRUBBED_ON,
                null);
            inputParams.put(MessagingConstants.UPDATE_ORDER_DETAILS_IN_SCRUBBED_BY,
                null);
            inputParams.put(MessagingConstants.UPDATE_ORDER_DETAILS_IN_ARIBA_UNSPSC_CODE,
                null);
            inputParams.put(MessagingConstants.UPDATE_ORDER_DETAILS_IN_ARIBA_PO_NUMBER,
                null);
            inputParams.put(MessagingConstants.UPDATE_ORDER_DETAILS_IN_ARIBA_AMS_PROJECT_CODE,
                null);
            inputParams.put(MessagingConstants.UPDATE_ORDER_DETAILS_IN_ARIBA_COST_CENTER,
                null);
            inputParams.put(MessagingConstants.UPDATE_ORDER_DETAILS_IN_SIZE_INDICATOR,
                null);
            inputParams.put(MessagingConstants.UPDATE_ORDER_DETAILS_IN_MILES_POINTS,
                null);
            inputParams.put(MessagingConstants.UPDATE_ORDER_DETAILS_IN_SUBCODE,
                null);
            inputParams.put("IN_SOURCE_CODE", null);

            /* build DataRequest object */
            request.setConnection(connection);
            request.reset();
            request.setInputParams(inputParams);
            request.setStatementID(MessagingConstants.UPDATE_ORDER_DETAILS);

            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            Map outputs = (Map) dau.execute(request);
            /* read store prodcedure output parameters to determine
             * if the procedure executed successfully */
                String status = (String) outputs.get(
                    MessagingConstants.UPDATE_ORDER_DETAILS_OUT_STATUS);
                if(status.equals("N"))
                {
                    String message = (String) outputs.get(
                    MessagingConstants.UPDATE_ORDER_DETAILS_OUT_MESSAGE);

                    throw new Exception(message);
                }

        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting modifyOrder");
            }
        }
    }

   /**
   * This method is a wrapper for the SP_INSERT_ORDER_FLORIST_USED SP.
   * It is used to insert a record into the order_florist_used table
   * which is used to keep track of the florists that have been used for
   * a particular order.
   *
   * @param OrderDetailVO
   * @param String - floristId
   * @return none
   */
  public void insertOrderFloristUsed(String orderDetailId, String floristId,
  		String selectionData) throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    Map outputs = null;

    try
    {
      logger.debug("insertOrderFloristUsed (OrderDetailVO orderDtl, String floristId) :: void");
      /* setup store procedure input parameters */
      HashMap inputParams = new HashMap();
      inputParams.put("ORDER_DETAIL_ID", new Long(orderDetailId));
      inputParams.put("FLORIST_ID", floristId);
      inputParams.put("SELECTION_DATA", selectionData);

      /* build DataRequest object */
      dataRequest.setConnection(connection);
      dataRequest.setStatementID("COM_INSERT_ORDER_FLORIST_USED");
      dataRequest.setInputParams(inputParams);

      /* execute the store prodcedure */
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      outputs = (Map) dataAccessUtil.execute(dataRequest);
      
      /* read store prodcedure output parameters to determine
       * if the procedure executed successfully */
      String status = (String) outputs.get("OUT_STATUS");
      if(status != null && status.equalsIgnoreCase("N"))
      {
        String message = (String) outputs.get("OUT_MESSAGE");
        throw new Exception(message);
      }
    }
    catch (Exception e)
    {
      logger.error(e);
      throw e;
    }
  }


   /**
   * This method obtains the order disposition code for an order
   *
   * @param orderDetailId - order detail id
   * @return String - order disposition code
   * @throws SAXException
   * @throws ParserConfigurationException
   * @throws IOException
   * @throws SQLException
   * @throws Exception
   */
  public String getOrderDispCode(String orderDetailId)
	  throws SAXException, ParserConfigurationException,
		IOException, SQLException, Exception
  {
		String orderDispCode = null;

		DataRequest dataRequest = new DataRequest();
		dataRequest.setConnection(connection);
		dataRequest.setStatementID("GET_ORDER_DETAILS_II");
		dataRequest.addInputParam("IN_ORDER_DETAIL_ID", orderDetailId);

		DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
		
		CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
		
		if(rs.next())
		{
		  orderDispCode = rs.getString("order_disp_code");
		}
		else
		{
		  throw new Exception("No order was found for order detail id:" + orderDetailId);
		}

		return orderDispCode;
 }


   /**
   * This method updates an order hold record
   *
   * @param OrderDetailHoldVO - Updated order hold values
	 *
   * @throws SAXException
   * @throws ParserConfigurationException
   * @throws IOException
   * @throws SQLException
	 */
  public void updateOrderHold(OrderDetailHoldVO orderHoldVO)
	  throws SAXException, ParserConfigurationException,
		IOException, SQLException
  {
      DataRequest dataRequest = new DataRequest();
      Map outputs = null;

      /* setup store procedure input parameters */
      HashMap inputParams = new HashMap();
      inputParams.put("IN_ORDER_DETAIL_ID", String.valueOf(orderHoldVO.getOrderDetailId()));
      inputParams.put("IN_REASON", orderHoldVO.getReason());
      inputParams.put("IN_REASON_TEXT", null);
      inputParams.put("IN_TIMEZONE", null);
      inputParams.put("IN_AUTH_COUNT", null);
      inputParams.put("IN_RELEASE_DATE", null);
      inputParams.put("IN_STATUS", orderHoldVO.getStatus());

      /* build DataRequest object */
      dataRequest.setConnection(connection);
      dataRequest.setStatementID("UPDATE_ORDER_HOLD");
      dataRequest.setInputParams(inputParams);

      /* execute the store prodcedure */
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      outputs = (Map) dataAccessUtil.execute(dataRequest);
      
      /* read store prodcedure output parameters to determine
       * if the procedure executed successfully */
      String status = (String) outputs.get("OUT_STATUS");
      if(status != null && status.equalsIgnoreCase("N"))
      {
        String message = (String) outputs.get("OUT_ERROR_MESSAGE");
        throw new SQLException(message);
      }
    }


    
  /**
     * Get any co_brand information associated with the order
     * 
     * @param a_orderDetailId
     * @return Document containing the values from the cursor 
     * @throws java.lang.Exception
     */
  public Document getOrderCoBrands(String a_orderDetailId) 
        throws Exception
  {
      Document searchResults = DOMUtil.getDocument();
      DataRequest dataRequest = new DataRequest();
      dataRequest.setConnection(this.connection);
      dataRequest.setStatementID("GET_ORDER_CO_BRANDS");
      dataRequest.addInputParam("IN_ORDER_DETAIL_ID", a_orderDetailId);
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      searchResults = (Document) dataAccessUtil.execute(dataRequest);      
      return searchResults;
  }
    

    
  /**
   * method for retrieving the recipient id off of an order detail.
   * @return BigDecimal 
   * @throws java.lang.Exception
   */

 public BigDecimal getRecipientId(String orderDetailId) throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(this.connection);
    dataRequest.setStatementID("GET_RECIPIENT_ID");
    dataRequest.addInputParam("IN_ORDER_DETAIL_ID", orderDetailId);
        
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

    BigDecimal receiptId = (BigDecimal) dataAccessUtil.execute(dataRequest);

    return receiptId;
  }

  /**
   * This method sets all the other fields that you need to set after you cancel
   * a held order. 
   * 1. clean.refund.refund_status = 'Billed'
   * 2. clean.order_bills.bill_status = 'Billed'
   * 3. clean.order_details.eod_delivery_indicator  = 'X'
   * 4. clean.order_details.order_disp_code = 'Processed'
   * @param orderDetailId
   * @param CSR
   * @throws java.lang.Exception
   */
 public void postCancelHeldOrder(String orderDetailId, String csr_id) throws Exception
  {
      logger.debug("postCancelHeldOrder(String orderDetailId "+orderDetailId+", String csr_id "+csr_id+") ");
      DataRequest dataRequest = new DataRequest();
      Map outputs = null;
      
      dataRequest.setConnection(this.connection);
      dataRequest.setStatementID("CANCEL_HELD_ORDER");
      dataRequest.addInputParam("IN_ORDER_DETAIL_ID", orderDetailId);
      dataRequest.addInputParam("IN_CSR_ID", csr_id);

      /* execute the store prodcedure */
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      outputs = (Map) dataAccessUtil.execute(dataRequest);
      
      /* read store prodcedure output parameters to determine
       * if the procedure executed successfully */
      String status = (String) outputs.get("OUT_STATUS");
      if(status != null && status.equalsIgnoreCase("N"))
      {
        String message = (String) outputs.get("OUT_MESSAGE");
        throw new SQLException(message);
      }

  }
    
  //Get the hold reason for a given order detail id
  public String getHoldReason(String orderDetailId) throws Exception
  {
      CachedResultSet orderDetailsInfo = getOrderDetailsInfo(orderDetailId);

      String holdReason = null;

      //populate strings from the query results
      if(orderDetailsInfo.next())
      {
        holdReason = orderDetailsInfo.getString("hold_reason");
      }
      
      return holdReason;
  }    
    
  /**
   * This method is a wrapper for the GET_ORDER_DETAILS.  
   * It populates an Order Detail VO based on the returned record set.
   * 
   * @param String - order detail id
   * @return OrderDetailVO 
   * @throws Exception
   */
  public OrderDetailVO getOrderDetail(String orderDetailID) throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    OrderDetailVO odVO = new OrderDetailVO();
    //Define the format
    SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");

    //build the data request
    dataRequest.setConnection(this.connection);
    dataRequest.setStatementID("GET_ORDER_DETAILS_OP");
    /* setup store procedure input parameters */
    dataRequest.addInputParam("IN_ORDER_DETAIL_ID", orderDetailID);
    /* execute the store prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
    CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
    
    /* populate object */
    while (rs.next())
    {        
      odVO.setCardMessage(rs.getObject("card_message") != null? rs.getString("card_message"):null);
      odVO.setCardSignature(rs.getObject("card_signature") != null? rs.getString("card_signature"):null);
      odVO.setColor1(rs.getObject("color_1") != null? rs.getString("color_1"):null);
      odVO.setColor2(rs.getObject("color_2") != null? rs.getString("color_2"):null);

      //retrieve/format the delivery date - note that the stored proc returns this as a String
      if (rs.getObject("delivery_date") != null)
      {
        String sDeliveryDate = rs.getString("delivery_date");
        //Create a Calendar Object
        Calendar cDeliveryDate = Calendar.getInstance();
        //Set the Calendar Object using the date retrieved in sCreatedOn
        cDeliveryDate.setTime(sdf.parse(sDeliveryDate));
        odVO.setDeliveryDate(cDeliveryDate);
      }

      //retrieve/format the delivery date end - note that the stored proc returns this as a String
      if (rs.getObject("delivery_date_range_end") != null)
      {
        String sDeliveryDateRange = rs.getString("delivery_date_range_end");
        //Create a Calendar Object
        Calendar cDeliveryDateRange = Calendar.getInstance();
        //Set the Calendar Object using the date retrieved in sCreatedOn
        cDeliveryDateRange.setTime(sdf.parse(sDeliveryDateRange));
        odVO.setDeliveryDateRangeEnd(cDeliveryDateRange);
      }


      odVO.setEODDeliveryIndicator(rs.getObject("eod_delivery_indicator") != null? rs.getString("eod_delivery_indicator"):null);
      odVO.setExternalOrderNumber(rs.getObject("external_order_number") != null? rs.getString("external_order_number"):null);
      odVO.setFloristId(rs.getObject("florist_id") != null? rs.getString("florist_id"):null);
      odVO.setHpOrderNumber(rs.getObject("hp_order_number") != null? rs.getString("hp_order_number"):null);

      //retrieve the miles/points
      String      milesPoints = null;
      BigDecimal  bMilesPoints;
      double      dMilesPoints = 0;
      if (rs.getObject("miles_points") != null)
      {
        bMilesPoints = (BigDecimal)rs.getObject("miles_points");
        milesPoints = bMilesPoints.toString();
        dMilesPoints = Double.valueOf(milesPoints).doubleValue();
      }
      odVO.setMilesPoint(dMilesPoints);

      odVO.setOccasion(rs.getObject("occasion") != null? rs.getString("occasion"):null);
      odVO.setOrderDetailId(orderDetailID);
      odVO.setOrderDispCode(rs.getObject("order_disp_code") != null? rs.getString("order_disp_code"):null);
      odVO.setOrderGuid(rs.getObject("order_guid") != null? rs.getString("order_guid"):null);
      odVO.setProductId(rs.getObject("product_id") != null? rs.getString("product_id").toUpperCase():null);

      //retrieve the quantity
      String      qty = null;
      BigDecimal  bQty;
      long        lQty = 0;
      if (rs.getObject("quantity") != null)
      {
        bQty = (BigDecimal)rs.getObject("quantity");
        qty = bQty.toString();
        lQty = Long.valueOf(qty).longValue();
      }
      odVO.setQuantity(lQty);

      odVO.setRecipientId(rs.getObject("recipient_id") != null? rs.getString("recipient_id"):null);
      odVO.setReleaseInfoIndicator(rs.getObject("release_info_indicator") != null? rs.getString("release_info_indicator"):null);

      //retrieve/format the ship date - note that the stored proc returns this as a Date
      java.sql.Date dShipDate = null;
      if (rs.getObject("ship_date") != null)
      {
        dShipDate = (java.sql.Date)rs.getObject("ship_date");
        //Create a Calendar Object
        Calendar cShipDate = Calendar.getInstance();
        //Set the Calendar Object using the date retrieved in sCreatedOn
        cShipDate.setTime(new java.util.Date(dShipDate.getTime()));
        odVO.setShipDate(cShipDate);
      }

      odVO.setSameDayGift(rs.getObject("same_day_gift") != null? rs.getString("same_day_gift"):null);
      odVO.setSecondChoiceProduct(rs.getObject("second_choice_product") != null? rs.getString("second_choice_product"):null);
      odVO.setShipMethod(rs.getObject("ship_method") != null? rs.getString("ship_method"):null);
      odVO.setSizeIndicator(rs.getObject("size_indicator") != null? rs.getString("size_indicator"):null);
      odVO.setSourceCode(rs.getObject("source_code") != null? rs.getString("source_code"):null);
      odVO.setSpecialInstructions(rs.getObject("special_instructions") != null? rs.getString("special_instructions"):null);
      odVO.setSubcode(rs.getObject("subcode") != null? rs.getString("subcode"):null);
      odVO.setSubstitutionIndicator(rs.getObject("substitution_indicator") != null? rs.getString("substitution_indicator"):null);
      odVO.setRecipientCountry(rs.getObject("recipient_country") != null? rs.getString("recipient_country"):null);
      odVO.setAVSAddressId(rs.getObject("avs_address_id") != null? rs.getString("avs_address_id"):null);
    }

    return odVO;
  }
  
    /**
     * method for retrieving the vendor id off of an order detail.
     * @return String
     * @throws java.lang.Exception
     */

    public String getVendorId(String orderDetailId) throws Exception
    {
      DataRequest dataRequest = new DataRequest();
      dataRequest.setConnection(this.connection);
      dataRequest.setStatementID("GET_VENDOR_ID");
      dataRequest.addInputParam("IN_ORDER_DETAIL_ID", orderDetailId);
          
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

      String vendorId = (String) dataAccessUtil.execute(dataRequest);

      return vendorId;
    }
    
    /**
     * This method will update the delivery confirmation status on the order detail record
     * @param orderDetailId
     * @param status
     * @param csrId
     * @throws Exception
     */
    public void updateDCONStatus(String orderDetailId, String status, String csrId) throws Exception
    {         
      DataRequest dataRequest = new DataRequest();
      Map outputs = null;

      /* setup store procedure input parameters */
      HashMap inputParams = new HashMap();
      inputParams.put("IN_ORDER_DETAIL_ID", orderDetailId);
      inputParams.put("IN_STATUS", status);
      inputParams.put("IN_CSR_ID", csrId);
      
      /* build DataRequest object */
      dataRequest.setConnection(connection);
      dataRequest.setStatementID("UPDATE_DCON_STATUS");
      dataRequest.setInputParams(inputParams);

      /* execute the store prodcedure */
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      outputs = (Map) dataAccessUtil.execute(dataRequest);
      
      /* read store prodcedure output parameters to determine
       * if the procedure executed successfully */
      String rsStatus = (String) outputs.get("OUT_STATUS");
      if(rsStatus != null && rsStatus.equalsIgnoreCase("N"))
      {
        String message = (String) outputs.get("OUT_ERROR_MESSAGE");
        throw new SQLException(message);
      }
    }

    public String getStateByZipcodeAndCityAjax(Connection conn, String zipCode, String city) throws Exception{
      	logger.debug("getStateByZipcodeAndCity( ZipCode :" + zipCode + ")");
      	logger.debug("getStateByZipcodeAndCity( City : " + city + ")");

          /* build DataRequest object */
          DataRequest dataRequest = new DataRequest();
          dataRequest.setConnection(conn);
          dataRequest.setStatementID("AJAX_GET_STATE_BY_ZIPCODE_CITY");
          Map inputParams = new HashMap();
          inputParams.put("IN_ZIP_CODE", zipCode);
          inputParams.put("IN_CITY", city);
          dataRequest.setInputParams(inputParams);
          
          
          /* execute the stored procedure */
          DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
          Map outputs = (Map) dataAccessUtil.execute(dataRequest);
          CachedResultSet resultSet = (CachedResultSet)outputs.get("OUT_CUR");
          String outState = null;
          if(resultSet.next()){
        	  outState = resultSet.getString("state_id");  
          }
          
          return outState;
      	
      }    
    
    public String getContentWithFilter(String context, String name, String filter1Value, String filter2Value) 
               throws Exception {

        String value = null;

        logger.debug("IN_CONTENT_CONTEXT:" + context);
        logger.debug("IN_CONTENT_NAME:" + name);
        logger.debug("IN_CONTENT_FILTER_1_VALUE:" + filter1Value);
        logger.debug("IN_CONTENT_FILTER_2_VALUE:" + filter2Value);

        DataAccessUtil dau = DataAccessUtil.getInstance();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.addInputParam("IN_CONTENT_CONTEXT", context);
        dataRequest.addInputParam("IN_CONTENT_NAME", name);
        dataRequest.addInputParam("IN_CONTENT_FILTER_1_VALUE", filter1Value);
        dataRequest.addInputParam("IN_CONTENT_FILTER_2_VALUE", filter2Value);
        dataRequest.setStatementID("GET_CONTENT_TEXT_WITH_FILTER");
        value = (String)dau.execute(dataRequest); 
          
        return value;
    }

    public Document getOrderTaxesByOrderDetailIdXML(String orderDetailId) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        
        dataRequest.setStatementID("GET_ORDER_DETAIL_TAXES");
        dataRequest.addInputParam("IN_ORDER_DETAIL_ID", orderDetailId);
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        return (Document) dataAccessUtil.execute(dataRequest);
    }
    
    /**
     * Gets gift card payments for an orderguid (cart) and retrives only a few select fields. Values returned in
     * a PaymentVO. NOTE: the "amount" field is used to store the value from SCRUB if there is one (or 0 if not).  
     * @param orderGuid
     * @return
     * @throws Exception
     */
    public ArrayList<PaymentVO> getGiftCardPayments(String orderGuid) throws Exception
    {
    	DataRequest dataRequest = new DataRequest();
    	ArrayList <PaymentVO>results = new ArrayList<PaymentVO>();
    	CachedResultSet outputs = null;

    	try
    	{
    		logger.debug("getGiftCardPayments (String orderGuid) :: PaymentVO");
    		/* setup store procedure input parameters */
    		HashMap inputParams = new HashMap();
    		inputParams.put("IN_ORDER_GUID", orderGuid);

    		/* build DataRequest object */
    		dataRequest.setConnection(connection);
    		dataRequest.setStatementID("GET_GIFT_CARD_PAYMENTS");
    		dataRequest.setInputParams(inputParams);

    		/* execute the store prodcedure */
    		DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    		outputs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

    		/* populate object */
    		while (outputs.next())
    		{
    	    	PaymentVO paymentVO = new PaymentVO();
                paymentVO.setPaymentId(outputs.getLong("payment_id"));
    			paymentVO.setCreditAmount(outputs.getDouble("credit_amount"));
    			paymentVO.setDebitAmount(outputs.getDouble("debit_amount"));
    			paymentVO.setPaymentInd(outputs.getString("payment_indicator"));
    			paymentVO.setBillStatus(outputs.getString("bill_status"));
    			paymentVO.setCardNumber(outputs.getString("gift_card_number"));
    			paymentVO.setGiftCardPin(outputs.getString("gift_card_pin"));
                String scrubamt = outputs.getString("orig_auth_amount_txt");
                if (scrubamt != null)
                    paymentVO.setAmount(Double.valueOf(scrubamt));
    			results.add(paymentVO);
    		}
    	}
    	catch (Exception e)
    	{
    		logger.error(e);
    		throw e;
    	}
    	if (results.isEmpty())
    		return null;
    	else
    		return results;
    }
    
    public CachedResultSet getOrderTotals(String orderDetailId) throws Exception 
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("GET_ORDER_TOTALS");
        dataRequest.addInputParam("IN_ORDER_DETAIL_ID", orderDetailId);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();        
        CachedResultSet rsSet = (CachedResultSet) dataAccessUtil.execute(dataRequest);
        
        return rsSet;
    }

	public void updatePremierCircleInfo(String orderGuid,
			MembershipDetailsVO pcMemDetails, String updatedBy) throws Exception {
		DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);

        dataRequest.addInputParam("IN_ORDER_GUID", orderGuid);
        dataRequest.addInputParam("IN_PC_GROUP_ID", pcMemDetails.getGroupId());
        dataRequest.addInputParam("IN_PC_MEMBERSHIP_ID", pcMemDetails.getMembershipId());
        dataRequest.addInputParam("IN_PC_FLAG", pcMemDetails.getStatus());
        dataRequest.addInputParam("IN_UPDATED_BY", updatedBy);
        dataRequest.setStatementID("UPDATE_PREMIER_CIRCLE_INFO");

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equalsIgnoreCase("N"))
        {
            String message = (String) outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }
	}

	public CachedResultSet getPremierCircleInfo(String orderGuid) throws Exception {
	        DataRequest dataRequest = new DataRequest();
	        dataRequest.setConnection(connection);
	        dataRequest.setStatementID("GET_PREMIER_CIRCLE_INFO");
	        dataRequest.addInputParam("IN_ORDER_GUID",orderGuid);

	        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
	        
	        CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
	        return rs;
	}

	public OrderBillVO getOrderBillTaxes(String orderDetailId) throws Exception { 
			Map searchResults = new HashMap();
		    DataRequest dataRequest = new DataRequest();
		    dataRequest.setConnection(this.connection);
		    dataRequest.setStatementID("GET_ORDER_ITEM_TAX");

		    dataRequest.addInputParam("IN_ORDER_DTL_ID", orderDetailId); 
		    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();		      
		    searchResults = (HashMap)dataAccessUtil.execute(dataRequest);
		    
		    CachedResultSet rs = (CachedResultSet) searchResults.get("OUT_CUR");
		    CachedResultSet rsDisp = (CachedResultSet) searchResults.get("OUT_DISP_CUR");
		    
		    if(rs.next() && rsDisp.next()) {
		    	OrderBillVO vo = new OrderBillVO();
		    	vo.setTax(rs.getDouble("Tax"));
		    	
		    	vo.setTax1Name(rsDisp.getString("tax1_name"));
		    	vo.setTax1Description(rsDisp.getString("tax1_description"));
		    	
		    	vo.setTax1Amount(rs.getBigDecimal("tax1_amount"));
		    	vo.setTax1Rate(rs.getBigDecimal("tax1_rate"));
		    	
		    	vo.setTax2Name(rsDisp.getString("tax2_name"));
		    	vo.setTax2Description(rsDisp.getString("tax2_description"));
		    	
		    	vo.setTax2Amount(rs.getBigDecimal("tax2_amount"));
		    	vo.setTax2Rate(rs.getBigDecimal("tax2_rate"));
		    	
		    	vo.setTax3Name(rsDisp.getString("tax3_name"));
		    	vo.setTax3Description(rsDisp.getString("tax3_description"));
		    	
		    	vo.setTax3Amount(rs.getBigDecimal("tax3_amount"));
		    	vo.setTax3Rate(rs.getBigDecimal("tax3_rate"));
		    	
		    	vo.setTax4Name(rsDisp.getString("tax4_name"));
		    	vo.setTax4Description(rsDisp.getString("tax4_description"));
		    	
		    	vo.setTax4Amount(rs.getBigDecimal("tax4_amount"));
		    	vo.setTax4Rate(rs.getBigDecimal("tax4_rate"));
		    	
		    	vo.setTax5Name(rsDisp.getString("tax5_name"));
		    	vo.setTax5Description(rsDisp.getString("tax5_description"));
		    	
		    	vo.setTax5Amount(rs.getBigDecimal("tax5_amount"));
		    	vo.setTax5Rate(rs.getBigDecimal("tax5_rate"));
		    	
		    	return vo;
		    }

		    throw new Exception("No order bill infor found for order detail id : " + orderDetailId);		
	}
	
	public OrderBillVO getOrderItemRemainingTaxes(String orderDetailId) throws Exception { 
		Map searchResults = new HashMap();
	    DataRequest dataRequest = new DataRequest();
	    dataRequest.setConnection(this.connection);
	    dataRequest.setStatementID("GET_ORDER_ITEM_REMAINING_TAX");

	    dataRequest.addInputParam("IN_ORDER_DTL_ID", orderDetailId); 
	    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();		      
	    searchResults = (HashMap)dataAccessUtil.execute(dataRequest);
	    
	    CachedResultSet rs = (CachedResultSet) searchResults.get("OUT_CUR");
	    CachedResultSet rsDisp = (CachedResultSet) searchResults.get("OUT_DISP_CUR");
	    
	    if(rs.next() && rsDisp.next()) {
	    	OrderBillVO vo = new OrderBillVO();
	    	vo.setTax(rs.getDouble("Tax"));
	    	
	    	vo.setTax1Name(rsDisp.getString("tax1_name"));
	    	vo.setTax1Description(rsDisp.getString("tax1_description"));
	    	
	    	vo.setTax1Amount(rs.getBigDecimal("tax1_amount"));
	    	vo.setTax1Rate(rs.getBigDecimal("tax1_rate"));
	    	
	    	vo.setTax2Name(rsDisp.getString("tax2_name"));
	    	vo.setTax2Description(rsDisp.getString("tax2_description"));
	    	
	    	vo.setTax2Amount(rs.getBigDecimal("tax2_amount"));
	    	vo.setTax2Rate(rs.getBigDecimal("tax2_rate"));
	    	
	    	vo.setTax3Name(rsDisp.getString("tax3_name"));
	    	vo.setTax3Description(rsDisp.getString("tax3_description"));
	    	
	    	vo.setTax3Amount(rs.getBigDecimal("tax3_amount"));
	    	vo.setTax3Rate(rs.getBigDecimal("tax3_rate"));
	    	
	    	vo.setTax4Name(rsDisp.getString("tax4_name"));
	    	vo.setTax4Description(rsDisp.getString("tax4_description"));
	    	
	    	vo.setTax4Amount(rs.getBigDecimal("tax4_amount"));
	    	vo.setTax4Rate(rs.getBigDecimal("tax4_rate"));
	    	
	    	vo.setTax5Name(rsDisp.getString("tax5_name"));
	    	vo.setTax5Description(rsDisp.getString("tax5_description"));
	    	
	    	vo.setTax5Amount(rs.getBigDecimal("tax5_amount"));
	    	vo.setTax5Rate(rs.getBigDecimal("tax5_rate"));
	    	
	    	return vo;
	    }

	    throw new Exception("No order bill infor found for order detail id : " + orderDetailId);		
	}

	public String getFullyRefundMerchAmountFlag(String sourceCode) throws Exception {
		
		String flagValue = null;
		Map searchResults = new HashMap();
	    DataRequest dataRequest = new DataRequest();
	    dataRequest.setConnection(this.connection);
	    dataRequest.setStatementID("GET_REFUND_MERCH_AMT_FLAG");

	    dataRequest.addInputParam("IN_SOURCE_CODE", sourceCode); 
	    
	    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();		      
	    flagValue = (String)dataAccessUtil.execute(dataRequest);
	    
	    return flagValue;
	}
	
	
}