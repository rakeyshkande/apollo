package com.ftd.customerordermanagement.dao;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.HashMap;

import javax.xml.parsers.ParserConfigurationException;



import org.w3c.dom.Document;

import org.xml.sax.SAXException;


/**
 * This class obtains Call Reason Code information and sets a reason code for 
 * the call.
 *
 * @author Matt Wilcoxen
 */

public class CallReasonCodeDAO
{
    private Logger logger = new Logger("com.ftd.customerordermanagement.dao.CallTimerDAO");

    //database
    private DataRequest request;
    private Connection conn;

    //database:  stored procedure names - CALL_LOG_PKG
    public final static String DB_VIEW_CALL_REASON_VAL = "VIEW_CALL_REASON_VAL";
    
    //database:  CALL_LOG_PKG.VIEW_CALL_REASON_VAL db stored procedure input parms
    //*****  stored procedure takes no input parms  *****


    /**
     * constructor
     * 
     * @param Connection - database connection
     * @return n/a
     * @throws none
     */
    public CallReasonCodeDAO(Connection conn)
    {
        super();
        this.conn = conn;
    }


    /**
    * get list of reason codes
    * 
    * @param none
    * 
    * @return Document - list of call reason codes
    * 
    * @throws IOException
    * @throws ParserConfigurationException
    * @throws SAXException
    * @throws SQLException
    */
    public Document getReasonCodes()
        throws IOException, ParserConfigurationException, SAXException, SQLException
    {
        HashMap outParametersMap = new HashMap();

        HashMap inputParams = new HashMap();

        // 1. build db stored procedure input parms
        // *****  stored procedure takes no input parms  *****
        
        //build DataRequest object
        request = new DataRequest();
        request.reset();
        request.setConnection(conn);
        request.setInputParams(inputParams);
        request.setStatementID(DB_VIEW_CALL_REASON_VAL);

        // 2. get data
        DataAccessUtil dau = DataAccessUtil.getInstance();
        return (Document) dau.execute(request);
//        outParametersMap = (HashMap) dau.execute(request);
//        return (Document) outParametersMap.get(DB_OUT_XXXXXXXXX);
    }
 
}