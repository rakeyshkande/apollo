package com.ftd.customerordermanagement.dao;

import com.ftd.customerordermanagement.vo.PaymentMethodVO;
import com.ftd.customerordermanagement.vo.PaymentVO;
import com.ftd.ftdutilities.FieldUtils;
import com.ftd.osp.utilities.constants.PaymentExtensionConstants;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.order.vo.CoBrandVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;




import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/**
 * PaymentDAO
 * This is the DAO that handles loading and updating payment information
 * 
 * @author Luke W. Pennings
 * @version $Id: PaymentDAO.java
 */

public class PaymentDAO 
{
    private Connection connection;
    private static Logger logger  = new Logger("com.ftd.customerordermanagement.dao.PaymentDAO");
    
  /**
   * Constructor
   * @param connection Connection
   */
  public PaymentDAO(Connection connection)
  {
    this.connection = connection;
  }

  /**
   * method for getting payment info by function and paymentInfoNumber.
   * @return Document containing the values from the cursor 
   * @throws java.lang.Exception
   */

  public Document getPaymentInfo(String functionName, String payInfoNumber) 
        throws Exception
  {
        Document searchResults = DOMUtil.getDocument();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        if (functionName.equalsIgnoreCase("cart"))
        {
          dataRequest.setStatementID("GET_CART_PAYMENTS");
          dataRequest.addInputParam("IN_ORDER_GUID",      payInfoNumber);
        }
        else
        {
          dataRequest.setStatementID("GET_ORDER_PAYMENTS");
          dataRequest.addInputParam("IN_ORDER_DETAIL_ID", payInfoNumber);
        }
        
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        searchResults = (Document) dataAccessUtil.execute(dataRequest);      

        return searchResults;

  }
  
  public List<PaymentVO> getPaymentsForOrder(String orderGUID) throws Exception {
	
	  List<PaymentVO> paymentList = new ArrayList<PaymentVO>();
	  DataRequest dataRequest = new DataRequest();
      dataRequest.setConnection(this.connection);

	  dataRequest.setStatementID("GET_CART_PAYMENTS");
	  dataRequest.addInputParam("IN_ORDER_GUID", orderGUID);
	
	  DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
	  CachedResultSet crs = (CachedResultSet)dataAccessUtil.execute(dataRequest);  

	  while (crs != null && crs.next()) {
		  PaymentVO payment = new PaymentVO();
		  payment.setOrderGuid(orderGUID);
		  payment.setPaymentId(crs.getLong("payment_id"));
		  payment.setPaymentInd(crs.getString("payment_indicator"));
		  payment.setPaymentType(crs.getString("payment_type"));
		  payment.setCardNumber(crs.getString("card_number"));
		  payment.setAuthorization(crs.getString("auth_number"));
		  payment.setCreditAmount(crs.getDouble("credit_amount"));
		  payment.setBillDate(crs.getDate("bill_date"));
		  //payment.setCreatedOn(new GregorianCalendar(crs.getDate("created_on").getTime()));
		  payment.setTransactionId(crs.getString("transaction_id"));
		  payment.setAvsCode(crs.getString("avs_code"));

		  paymentList.add(payment);
		  
	  }
	  
	  
	  return paymentList;
	  
  }

  /**
   * method for getting payment methods by source code.
   * @return Document containing the values from the cursor 
   * @throws java.lang.Exception
   */
  public Document getPaymentMethodBySourceCode(String sourceCode) 
        throws Exception
  {
        Document searchResults = DOMUtil.getDocument();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("GET_PAYMENT_METHODS_BY_SOURCE");
        dataRequest.addInputParam("IN_SOURCE_CODE", sourceCode);
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        searchResults = (Document) dataAccessUtil.execute(dataRequest);      

        return searchResults;

  }
  
    /**
   * Method obtains all payment methods.
   * @return ArrayList of PaymentMethodVO's
   * @throws java.lang.Exception
   */
  public ArrayList getAllPaymentMethod()
        throws Exception
  {
        ArrayList paymentMethods = new ArrayList();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("GET_PAYMENT_METHOD_LIST");
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        CachedResultSet searchResults = (CachedResultSet) dataAccessUtil.execute(dataRequest);    

		//  Create a list of PaymentMethodVO's
		PaymentMethodVO pmVO;
		while(searchResults.next())
		{
			pmVO = new PaymentMethodVO();
			pmVO.setPaymentMethodId(searchResults.getString("PAYMENT_METHOD_ID"));
			pmVO.setDescription(searchResults.getString("DESCRIPTION"));
			pmVO.setPaymentType(searchResults.getString("PAYMENT_TYPE"));
			pmVO.setHasExpirationDate(searchResults.getString("HAS_EXPIRATION_DATE"));
			
			paymentMethods.add(pmVO);
		}

        return paymentMethods;
  }

  
  /**
   * method for verifying the validity of a gift certificate id.
   * @return Document containing the values from the cursor 
   * @throws java.lang.Exception
   */

  public Document verifyGiftCertificate(String giftCertificateID) 
        throws Exception
  {
        Document searchResults = DOMUtil.getDocument();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("GET_GIFT_CERTIFICATE");
        dataRequest.addInputParam(" IN_GIFT_CERTIFICATE_ID", giftCertificateID);
      
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        searchResults = (Document) dataAccessUtil.execute(dataRequest);      

        return searchResults;

  }
  
  /**
   * method for updating payment information
   * @params PaymentVO[] 
   * @return void 
   * 
   * @throws java.lang.Exception
   */
  public void addOrderPayments(PaymentVO[] pvo) 
        throws Exception
  {
      DataRequest dataRequest = new DataRequest();
      Map outputs = null; 
      dataRequest.setConnection(this.connection);
      int i = 0;
      /* Loop through the payment array and populate the params for updates 
      /* setup store procedure input parameters */
      logger.info("lenght of Payment VOs in addOrderPayments method: " + pvo);
      while(i < pvo.length)
      {
        HashMap inputParams = new HashMap();
        
        inputParams.put("IN_ADDITIONAL_BILL_ID", (pvo[i].getAdditionalBillId() == 0) ? null : String.valueOf(pvo[i].getAdditionalBillId()));           
        inputParams.put("IN_PAYMENT_TYPE", pvo[i].getPaymentType());                 
        inputParams.put("IN_CC_ID", pvo[i].getCardId());                        
        inputParams.put("IN_UPDATED_BY", pvo[i].getUpdatedBy());                   
        inputParams.put("IN_AUTH_RESULT", pvo[i].getAuthResult()); 
        inputParams.put("IN_AUTH_NUMBER", pvo[i].getAuthorization());   
        inputParams.put("IN_AVS_CODE", pvo[i].getAvsCode());                     
        inputParams.put("IN_ACQ_REFERENCE_NUMBER", pvo[i].getAcqRefNumber());         
        inputParams.put("IN_GC_COUPON_NUMBER", pvo[i].getGcCouponNumber());             
        inputParams.put("IN_AUTH_OVERRIDE_FLAG", pvo[i].getAuthOverrideFlag());           
        inputParams.put("IN_CREDIT_AMOUNT", String.valueOf(pvo[i].getCreditAmount()));              
        inputParams.put("IN_DEBIT_AMOUNT", String.valueOf(pvo[i].getDebitAmount()));                  
        inputParams.put("IN_PAYMENT_INDICATOR", pvo[i].getPaymentInd());        
        inputParams.put("IN_REFUND_ID", null); 
        if(pvo[i].getAuthDate() == null)
        {
          inputParams.put("IN_AUTH_DATE", new java.sql.Date(new java.util.Date().getTime()));  
        }
        else
        {
          inputParams.put("IN_AUTH_DATE", new java.sql.Date(pvo[i].getAuthDate().getTime()));                  
        }
        inputParams.put("IN_AAFES_TICKET_NUMBER", pvo[i].getAafesTicketNumber()); 
        inputParams.put("IN_ORDER_GUID", pvo[i].getOrderGuid());
        inputParams.put("IO_PAYMENT_ID", null);
        inputParams.put("IN_MILES_POINTS_CREDIT_AMT", null);                
        inputParams.put("IN_MILES_POINTS_DEBIT_AMT", null);                 
        inputParams.put("IN_NC_APPROVAL_ID", pvo[i].getNcApprovalId());
        inputParams.put("IN_WALLET_INDICATOR", pvo[i].getWalletIndicator());
        
        inputParams.put("IN_CSC_RESPONSE_CODE", null);
        inputParams.put("IN_CSC_VALIDATED_FLAG", "N");
        inputParams.put("IN_CSC_FAILURE_CNT", new BigDecimal("0"));
        inputParams.put("IN_IS_VOICE_AUTH", (pvo[i].isVoiceAuth()==true)?"Y":null);
        inputParams.put("IN_CC_AUTH_PROVIDER", pvo[i].getCcAuthProvider());
        inputParams.put("IN_AUTH_TRANSACTION_ID", pvo[i].getAuthorizationTransactionId());
        inputParams.put("IN_MERCHANT_REF_ID", pvo[i].getMerchantRefId());
        
        String delimitedPaymentExtFields = null;
        if(pvo[i].getPaymentExtMap()!=null && pvo[i].getPaymentExtMap().size()>0){
        	Map<String,Object> paymentExtInfo = new HashMap(pvo[i].getPaymentExtMap());
        	Map<String,Object> cardSpecificDetailsMap = (Map<String, Object>) paymentExtInfo.remove(PaymentExtensionConstants.CARD_SPECIFIC_DETAIL);
        	if(cardSpecificDetailsMap != null && cardSpecificDetailsMap.size()>0){
        		paymentExtInfo.putAll(cardSpecificDetailsMap);
        	}
        	delimitedPaymentExtFields = FieldUtils.getDelimitedStringFromMap(paymentExtInfo,"###","&&&");
        }
        inputParams.put("IN_PAYMENT_EXT_INFO", delimitedPaymentExtFields);
        inputParams.put("IN_CARDINAL_VERIFIED_FLAG",pvo[i].getCardinalVerifiedFlag() );
        inputParams.put("IN_ROUTE",pvo[i].getRoute());
        
        logger.info("addOrderPayments :: Input map: " + inputParams);
        /* build DataRequest object */
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("REFUND_UPDATE_PAYMENTS");
        dataRequest.setInputParams(inputParams);
      
        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        outputs = (Map) dataAccessUtil.execute(dataRequest);
        i++;
        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equalsIgnoreCase("N"))
        {
            String message = (String) outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }
      }
  }

  /**
   * method for updating GD payment information
   * @params PaymentVO[] 
   * @return void 
   * 
   * @throws java.lang.Exception
   */
  public void addOrderPaymentsGD(PaymentVO[] pvo) 
        throws Exception
  {
      DataRequest dataRequest = new DataRequest();
      Map outputs = null; 
      dataRequest.setConnection(this.connection);
      int i = 0;
      /* Loop through the payment array and populate the params for updates 
      /* setup store procedure input parameters */
      while(i < pvo.length)
      {
        HashMap inputParams = new HashMap();
        
        inputParams.put("IN_ADDITIONAL_BILL_ID", (pvo[i].getAdditionalBillId() == 0) ? null : String.valueOf(pvo[i].getAdditionalBillId()));           
        inputParams.put("IN_PAYMENT_TYPE", pvo[i].getPaymentType());                 
        inputParams.put("IN_CC_ID", pvo[i].getCardId());                        
        inputParams.put("IN_UPDATED_BY", pvo[i].getUpdatedBy());                   
        inputParams.put("IN_AUTH_RESULT", pvo[i].getAuthResult()); 
        inputParams.put("IN_AUTH_NUMBER", pvo[i].getAuthorization());
        inputParams.put("IN_AP_AUTH_TXT", pvo[i].getAuthorization());
        inputParams.put("IN_AVS_CODE", pvo[i].getAvsCode());                     
        inputParams.put("IN_ACQ_REFERENCE_NUMBER", pvo[i].getAcqRefNumber());         
        inputParams.put("IN_GC_COUPON_NUMBER", pvo[i].getGcCouponNumber());             
        inputParams.put("IN_AUTH_OVERRIDE_FLAG", pvo[i].getAuthOverrideFlag());           
        inputParams.put("IN_CREDIT_AMOUNT", String.valueOf(pvo[i].getCreditAmount()));              
        inputParams.put("IN_DEBIT_AMOUNT", String.valueOf(pvo[i].getDebitAmount()));                  
        inputParams.put("IN_PAYMENT_INDICATOR", pvo[i].getPaymentInd());        
        inputParams.put("IN_REFUND_ID", null); 
        if(pvo[i].getAuthDate() == null)
        {
          inputParams.put("IN_AUTH_DATE", new java.sql.Date(new java.util.Date().getTime()));  
        }
        else
        {
          inputParams.put("IN_AUTH_DATE", new java.sql.Date(pvo[i].getAuthDate().getTime()));                  
        }
        inputParams.put("IN_AAFES_TICKET_NUMBER", pvo[i].getAafesTicketNumber()); 
        inputParams.put("IN_ORDER_GUID", pvo[i].getOrderGuid());
        inputParams.put("IO_PAYMENT_ID", null);
        inputParams.put("IN_MILES_POINTS_CREDIT_AMT", null);                
        inputParams.put("IN_MILES_POINTS_DEBIT_AMT", null);                 
        inputParams.put("IN_NC_APPROVAL_ID", pvo[i].getNcApprovalId());
        
        inputParams.put("IN_CSC_RESPONSE_CODE", null);
        inputParams.put("IN_CSC_VALIDATED_FLAG", "N");
        inputParams.put("IN_CSC_FAILURE_CNT", new BigDecimal("0"));
        inputParams.put("IN_CARDINAL_VERIFIED_FLAG",pvo[i].getCardinalVerifiedFlag() );
      
        /* build DataRequest object */
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("REFUND_UPDATE_PAYMENTS_GD");
        dataRequest.setInputParams(inputParams);
      
        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        outputs = (Map) dataAccessUtil.execute(dataRequest);
        i++;
        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equalsIgnoreCase("N"))
        {
            String message = (String) outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }
      }
  }
  
  /**
   * method for updating payment information containing a refund id
   * @params PaymentVO[] 
   * @return void 
   * 
   * @throws java.lang.Exception
   */
  public void addOrderPaymentsRID(PaymentVO[] pvo) 
        throws Exception
  {
      DataRequest dataRequest = new DataRequest();
      Map outputs = null; 
      dataRequest.setConnection(this.connection);
      int i = 0;
      /* Loop through the payment array and populate the params for updates 
      /* setup store procedure input parameters */
      while(i < pvo.length)
      {
        HashMap inputParams = new HashMap();
        
        inputParams.put("IN_ADDITIONAL_BILL_ID", (pvo[i].getAdditionalBillId() == 0) ? null : String.valueOf(pvo[i].getAdditionalBillId()));           
        inputParams.put("IN_PAYMENT_TYPE", pvo[i].getPaymentType());                 
        inputParams.put("IN_CC_ID", pvo[i].getCardId());                        
        inputParams.put("IN_UPDATED_BY", pvo[i].getUpdatedBy());                   
        inputParams.put("IN_AUTH_RESULT", pvo[i].getAuthResult()); 
        inputParams.put("IN_AUTH_NUMBER", pvo[i].getAuthorization());   
        inputParams.put("IN_AVS_CODE", pvo[i].getAvsCode());                     
        inputParams.put("IN_ACQ_REFERENCE_NUMBER", pvo[i].getAcqRefNumber());         
        inputParams.put("IN_GC_COUPON_NUMBER", pvo[i].getGcCouponNumber());             
        inputParams.put("IN_AUTH_OVERRIDE_FLAG", pvo[i].getAuthOverrideFlag());           
        inputParams.put("IN_CREDIT_AMOUNT", String.valueOf(pvo[i].getCreditAmount()));              
        inputParams.put("IN_DEBIT_AMOUNT", String.valueOf(pvo[i].getDebitAmount()));                  
        inputParams.put("IN_PAYMENT_INDICATOR", pvo[i].getPaymentInd());        
        inputParams.put("IN_REFUND_ID", pvo[i].getRefundId()); 
        if(pvo[i].getAuthDate() == null)
        {
          inputParams.put("IN_AUTH_DATE", new java.sql.Date(new java.util.Date().getTime()));  
        }
        else
        {
          inputParams.put("IN_AUTH_DATE", new java.sql.Date(pvo[i].getAuthDate().getTime()));                  
        }
        inputParams.put("IN_AAFES_TICKET_NUMBER", pvo[i].getAafesTicketNumber()); 
        inputParams.put("IN_ORDER_GUID", pvo[i].getOrderGuid());
        inputParams.put("IO_PAYMENT_ID", null);
        inputParams.put("IN_MILES_POINTS_CREDIT_AMT", null);                
        inputParams.put("IN_MILES_POINTS_DEBIT_AMT", null);     
        
        inputParams.put("IN_CSC_RESPONSE_CODE", null);
        inputParams.put("IN_CSC_VALIDATED_FLAG", "N");
        inputParams.put("IN_CSC_FAILURE_CNT", new BigDecimal("0"));
        inputParams.put("IN_IS_VOICE_AUTH", (pvo[i].isVoiceAuth()==true)?"Y":null);
        inputParams.put("IN_CC_AUTH_PROVIDER", null);
        inputParams.put("IN_PAYMENT_EXT_INFO", null);
        inputParams.put("IN_CARDINAL_VERIFIED_FLAG", pvo[i].getCardinalVerifiedFlag() );
      
        /* build DataRequest object */
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("REFUND_UPDATE_PAYMENTS");
        dataRequest.setInputParams(inputParams);
      
        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        outputs = (Map) dataAccessUtil.execute(dataRequest);
        i++;
        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equalsIgnoreCase("N"))
        {
            String message = (String) outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }
      }
  }


  /**
   * method for updating payment information
   * @params PaymentVO[] 
   * @return void 
   * 
   * @throws java.lang.Exception
   */

  public void updatePayments(PaymentVO[] pvo)
        throws Exception
  {
      DataRequest dataRequest = new DataRequest();
      Map outputs = null; 
      dataRequest.setConnection(this.connection);
      int i = 0;
      /* Loop through the payment array and populate the params for updates 
      /* setup store procedure input parameters */
      while(i < pvo.length)
      {
        HashMap inputParams = new HashMap();

        inputParams.put("IN_ADDITIONAL_BILL_ID", null);           
        inputParams.put("IN_PAYMENT_TYPE", null);                 
        inputParams.put("IN_CC_ID", null);                        
        inputParams.put("IN_UPDATED_BY", pvo[i].getUpdatedBy());                   
        inputParams.put("IN_AUTH_RESULT", pvo[i].getAuthResult()); 
        inputParams.put("IN_AUTH_NUMBER", pvo[i].getAuthorization());   
        inputParams.put("IN_AVS_CODE", null);                     
        inputParams.put("IN_ACQ_REFERENCE_NUMBER", null);         
        inputParams.put("IN_GC_COUPON_NUMBER", null);             
        inputParams.put("IN_AUTH_OVERRIDE_FLAG", pvo[i].getAuthOverrideFlag());           
        inputParams.put("IN_CREDIT_AMOUNT", null);              
        inputParams.put("IN_DEBIT_AMOUNT", null);                  
        inputParams.put("IN_PAYMENT_INDICATOR", null);        
        inputParams.put("IN_REFUND_ID", null); 
        inputParams.put("IN_BILL_STATUS", pvo[i].getBillStatus());
        if(pvo[i].getBillDate() == null) {
        	inputParams.put("IN_BILL_DATE", null);	
        }else {
        	inputParams.put("IN_BILL_DATE", new java.sql.Date(pvo[i].getBillDate().getTime()));
        }
        if(pvo[i].getAuthDate() == null)
        {
          inputParams.put("IN_AUTH_DATE", new java.sql.Date(new java.util.Date().getTime()));  
        }
        else
        {
          inputParams.put("IN_AUTH_DATE", new java.sql.Date(pvo[i].getAuthDate().getTime()));                  
        }
        inputParams.put("IN_AAFES_TICKET_NUMBER", pvo[i].getAafesTicketNumber()); 
        
        // Added a check for invalid payment id.  
        if(pvo[i].getPaymentId() == 0)
          throw new Exception("Payment ID cannot be 0.");

        inputParams.put("IO_PAYMENT_ID", String.valueOf(pvo[i].getPaymentId()));
        inputParams.put("IN_MILES_POINTS_CREDIT_AMT", null);                
        inputParams.put("IN_MILES_POINTS_DEBIT_AMT", null);                 
        inputParams.put("IN_NC_APPROVAL_ID", pvo[i].getNcApprovalId());
        
        inputParams.put("IN_CSC_RESPONSE_CODE", null);
        inputParams.put("IN_CSC_VALIDATED_FLAG", "N");
        inputParams.put("IN_CSC_FAILURE_CNT", new BigDecimal("0"));
        inputParams.put("IN_IS_VOICE_AUTH", (pvo[i].isVoiceAuth()==true)?"Y":null);
        inputParams.put("IN_CC_AUTH_PROVIDER", pvo[i].getCcAuthProvider());
        String delimitedPaymentExtFields = null;
        if(pvo[i].getPaymentExtMap()!=null && pvo[i].getPaymentExtMap().size()>0){
        	Map<String,Object> paymentExtInfo = new HashMap(pvo[i].getPaymentExtMap());
        	Map<String,Object> cardSpecificDetailsMap = (Map<String, Object>) paymentExtInfo.remove(PaymentExtensionConstants.CARD_SPECIFIC_DETAIL);
        	if(cardSpecificDetailsMap != null && cardSpecificDetailsMap.size()>0){
        		paymentExtInfo.putAll(cardSpecificDetailsMap);
        	}
        	delimitedPaymentExtFields = FieldUtils.getDelimitedStringFromMap(paymentExtInfo,"###","&&&");
        }
        inputParams.put("IN_PAYMENT_EXT_INFO", delimitedPaymentExtFields);
        inputParams.put("IN_CARDINAL_VERIFIED_FLAG",pvo[i].getCardinalVerifiedFlag() );
        inputParams.put("IN_ROUTE",pvo[i].getRoute());
        /* build DataRequest object */
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("REFUND_UPDATE_PAYMENTS");
        dataRequest.setInputParams(inputParams);
      
        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        outputs = (Map) dataAccessUtil.execute(dataRequest);
        i++;
        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equalsIgnoreCase("N"))
        {
            String message = (String) outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }
      }
  }
  
    /**
   * This method is a wrapper for the GET_PAYMENT_METHOD_BY_TYPE.  
   * It populates a HashMap based on the returned record set.
   * 
   * @param String - paymentType
   * @return HashMap 
   */

  public ArrayList getPaymentIDByType(String paymentType) throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    boolean isEmpty = true;
    CachedResultSet outputs = null;
    ArrayList paymentIDs = new ArrayList();
    
    try
    {
      logger.debug("getPaymentIDByType (String paymentType) :: HashMap");
      /* setup store procedure input parameters */
      HashMap inputParams = new HashMap();
      inputParams.put("IN_PAYMENT_TYPE", paymentType);

      /* build DataRequest object */
      dataRequest.setConnection(connection);
      dataRequest.setStatementID("GET_PAYMENT_METHODS_BY_TYPE");
      dataRequest.setInputParams(inputParams);
      
      /* execute the store prodcedure */
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
      outputs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
      
      /* populate object */
      while (outputs.next())
      {
        isEmpty = false;
        paymentIDs.add(outputs.getString("PAYMENT_METHOD_ID"));     
      }
    }
    catch (Exception e) 
    {            
      logger.error(e);
      throw e;
    } 
    if (isEmpty)
      return null;
    else 
      return paymentIDs;
  }  

  /**
   * Method for inserting payment information into Modify Order temp tables.
	 * 
   * @params PaymentVO[] - Payments
   * @throws java.lang.Exception
   */
  public void addOrderPaymentsMO(PaymentVO[] pvo) 
        throws Exception
  {
      DataRequest dataRequest = new DataRequest();
      Map outputs = null; 
      dataRequest.setConnection(this.connection);
      int i = 0;
      /* Loop through the payment array and populate the params for updates 
      /* setup store procedure input parameters */
      while(i < pvo.length)
      {
        HashMap inputParams = new HashMap();

        inputParams.put("IN_ORDER_GUID", (pvo[i].getOrderGuid() == "") ? null : String.valueOf(pvo[i].getOrderGuid()));

        logger.info(" IN_ORDER_GUID " + String.valueOf(pvo[i].getOrderGuid()));
        
        inputParams.put("IN_ADDITIONAL_BILL_ID", (pvo[i].getAdditionalBillId() == 0) ? null : String.valueOf(pvo[i].getAdditionalBillId()));           
        inputParams.put("IN_PAYMENT_TYPE", pvo[i].getPaymentType());                 
        inputParams.put("IN_CC_ID", pvo[i].getCardId());                        
        inputParams.put("IN_UPDATED_BY", pvo[i].getUpdatedBy());                   
        inputParams.put("IN_AUTH_RESULT", pvo[i].getAuthResult()); 
        inputParams.put("IN_AUTH_NUMBER", pvo[i].getAuthorization());   
        inputParams.put("IN_AVS_CODE", pvo[i].getAvsCode());                     
        inputParams.put("IN_ACQ_REFERENCE_NUMBER", pvo[i].getAcqRefNumber());         
        inputParams.put("IN_GC_COUPON_NUMBER", pvo[i].getGcCouponNumber());             
        inputParams.put("IN_AUTH_OVERRIDE_FLAG", pvo[i].getAuthOverrideFlag());           
        inputParams.put("IN_CREDIT_AMOUNT", String.valueOf(pvo[i].getCreditAmount()));              
        inputParams.put("IN_DEBIT_AMOUNT", String.valueOf(pvo[i].getDebitAmount()));                  
        inputParams.put("IN_PAYMENT_INDICATOR", pvo[i].getPaymentInd());        
        inputParams.put("IN_REFUND_ID", pvo[i].getRefundId()); 
        if(pvo[i].getAuthDate() == null)
        {
          inputParams.put("IN_AUTH_DATE", new java.sql.Date(new java.util.Date().getTime()));  
        }
        else
        {
          inputParams.put("IN_AUTH_DATE", new java.sql.Date(pvo[i].getAuthDate().getTime()));                  
        }
        inputParams.put("IN_AAFES_TICKET_NUMBER", pvo[i].getAafesTicketNumber()); 
        inputParams.put("IN_PAYMENT_TRANSACTION_ID", pvo[i].getTransactionId());
        inputParams.put("IN_NC_APPROVAL_ID", pvo[i].getNcApprovalId());
        inputParams.put("IN_WALLET_INDICATOR", pvo[i].getWalletIndicator());
        
        logger.info(" WALLET_INDICATOR " + pvo[i].getWalletIndicator());
        
        /* build DataRequest object */
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("INSERT_PAYMENTS_UPDATE");
        dataRequest.setInputParams(inputParams);
      
        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        outputs = (Map) dataAccessUtil.execute(dataRequest);
        i++;
        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equalsIgnoreCase("N"))
        {
            String message = (String) outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }
      }
  }


    /**
   * method for verifying the validity of a gift certificate id.
   * @return Document containing the values from the cursor 
   * @throws java.lang.Exception
   */

  public BigDecimal getTransactionId() 
        throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(this.connection);
    dataRequest.setStatementID("GET_PAYMENT_TRANSACTION_ID");
        
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    BigDecimal tranId = (BigDecimal) dataAccessUtil.execute(dataRequest);
    return tranId;
  }
 
  /**
   * method for verifying the validity of a gift certificate id.
   * @return Document containing the values from the cursor 
   * @throws java.lang.Exception
   */  
  public void deleteCartPayment(long paymentId)
       throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(connection);
    dataRequest.setStatementID("DELETE_CART_PAYMENT");       
    dataRequest.addInputParam("IN_PAYMENT_ID", new Long(paymentId));
       
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
    Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
    String status = (String) outputs.get("OUT_STATUS_PARAM");
    if(status != null && status.equalsIgnoreCase("N"))
    {
      String message = (String) outputs.get("OUT_MESSAGE_PARAM");
      throw new Exception(message);
    }
  }


   /**
   * This method obtains payment information for a credit card
	 * cart payment
   * 
   * @param orderDetailId - order detail id 
   * @return String - order disposition code
   * @throws SAXException
   * @throws ParserConfigurationException
   * @throws IOException
   * @throws SQLException
   * @throws Exception
   */
  public PaymentVO getCartPaymentCC(String orderDetailId)
	  throws SAXException, ParserConfigurationException,
		IOException, SQLException, Exception
  {
    DataRequest dataRequest = new DataRequest();
    PaymentVO payment = null;
    CachedResultSet outputs = null;
    
    /* setup store procedure input parameters */
    HashMap inputParams = new HashMap();
    inputParams.put("IN_ORDER_DETAIL_ID", orderDetailId);
    
    /* build DataRequest object */
    dataRequest.setConnection(connection);
    dataRequest.setStatementID("GET_ORDER_DETAIL_PAYMENT");
    dataRequest.setInputParams(inputParams);
    
    /* execute the store prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
    outputs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
    
		/* populate object */
    
    // always store the first record, unless a credit card is in one of the
    // other records (store the credit card instead)
    
    String paymentType = null;
		// There should only be one payment other than GC, IN, or NC
		while (outputs.next())
    {
			payment = new PaymentVO();
			paymentType = outputs.getString("payment_type");
			if(!paymentType.equalsIgnoreCase("GC") &&
				!paymentType.equalsIgnoreCase("IN") &&
				!paymentType.equalsIgnoreCase("NC")) 
      {
				payment.setPaymentId(outputs.getLong("payment_id"));
				payment.setOrderGuid(outputs.getString("order_guid"));
				payment.setAdditionalBillId(outputs.getLong("additional_bill_id"));
				payment.setPaymentType(paymentType);
				payment.setCardId(outputs.getString("cc_id"));
				payment.setAuthResult(outputs.getString("auth_result"));
				payment.setAuthorization(outputs.getString("auth_number"));
				payment.setAvsCode(outputs.getString("avs_code"));
				payment.setAcqRefNumber(outputs.getString("acq_reference_number"));
				payment.setGcCouponNumber(outputs.getString("gc_coupon_number,"));
				payment.setAuthOverrideFlag(outputs.getString("auth_override_flag"));
				payment.setCreditAmount(outputs.getDouble("credit_amount"));
				payment.setDebitAmount(outputs.getDouble("debit_amount"));
				payment.setPaymentInd(outputs.getString("payment_indicator"));
				payment.setRefundId(outputs.getString("refund_id"));
				payment.setAuthDate(outputs.getDate("auth_date"));
				payment.setTransactionId(outputs.getString("transaction_id"));
				payment.setAafesTicketNumber(outputs.getString("aafes_ticket_number"));
			}
    }  

    return payment;
  }


  /**
   * Inserts co brand information.
   * @return Document containing the values from the cursor 
   * @throws java.lang.Exception
   */  
  public void updateCoBrandMO(String orderGuid, HashMap billingInfo)
       throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(connection);
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
		
		dataRequest.setStatementID("INSERT_CO_BRAND_UPDATE");       
		dataRequest.addInputParam("IN_ORDER_GUID", orderGuid);

		String key = null;
		HashMap prompt = null;
		for(Iterator it = billingInfo.keySet().iterator(); it.hasNext();)
		{
      key = (String) it.next();
			prompt = (HashMap)billingInfo.get(key);
			dataRequest.addInputParam("IN_INFO_NAME", (String)prompt.get("name"));
			dataRequest.addInputParam("IN_INFO_DATA", (String)prompt.get("value"));
      
			Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
			String status = (String) outputs.get("OUT_STATUS");
			if(status != null && status.equalsIgnoreCase("N"))
			{
				String message = (String) outputs.get("OUT_MESSAGE");
				throw new Exception(message);
			}
		}
  }
  

  /**
   * This method obtains the Order Detail Id from Order Details using input parameter as Order Guid  
   * @return OrderDetailId
   * @throws java.lang.Exception
   */
  public String getOrderDetailId(String orderGuid)
	       throws Exception
	  {
	    DataRequest dataRequest = new DataRequest();
	    dataRequest.setConnection(this.connection);
	    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    			
		dataRequest.setStatementID("GET_ORDER_DETAIL_ID");       
			 Map paramMap = new HashMap();
			 paramMap.put("IN_ORDER_GUID",orderGuid);
			 dataRequest.setInputParams(paramMap);	
			 String orderDetailId = (String) dataAccessUtil.execute(dataRequest);
			  	
			 return orderDetailId;			   
	   }
  
  /**
   * This method will delete Cardinal Commerce values in 
   * the clean.payments_ext table
   * @param long paymentId
   * @return void 
   * @throws Exception
   */  
  public void deleteCardinalCommerceValues(long paymentId)
       throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(connection);
    dataRequest.setStatementID("DELETE_CARDINAL_COMMERCE_VALUES");       
    dataRequest.addInputParam("IN_PAYMENT_ID", new Long(paymentId));
       
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
    Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
    String status = (String) outputs.get("OUT_STATUS_PARAM");
    if(status != null && status.equalsIgnoreCase("N"))
    {
      String message = (String) outputs.get("OUT_MESSAGE_PARAM");
      throw new Exception(message);
    }
  }


	public void addDefaultPaymentExtensions(PaymentVO pVO) throws Exception {
		DataRequest dataRequest = new DataRequest();
		dataRequest.setConnection(connection);
		dataRequest.setStatementID("DELETE_PAYMENT_EXT_VALUES");
		dataRequest.addInputParam("IN_PAYMENT_ID", new Long(pVO.getPaymentId()));
		dataRequest.addInputParam("IN_PAYMENT_AMOUNT", new Double(pVO.getAmount()));
		
		DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
		Map outputs = (Map) dataAccessUtil.execute(dataRequest);

		String status = (String) outputs.get("OUT_STATUS_PARAM");
		if (status != null && status.equalsIgnoreCase("N")) {
			String message = (String) outputs.get("OUT_MESSAGE_PARAM");
			throw new Exception(message);
		}
		
	}


}
