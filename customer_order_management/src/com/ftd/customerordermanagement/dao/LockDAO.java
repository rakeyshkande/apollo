package com.ftd.customerordermanagement.dao;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.osp.utilities.xml.DOMUtil;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.StringWriter;

import java.sql.Connection;
import java.sql.DriverManager;

import java.util.HashMap;
import java.util.Map;

import org.w3c.dom.Document;



/**
 * LockDAO
 * This is the DAO that handles creating a lock for a requesting CSR, checking whether a record is locked
 * by another CSR, retrieving the lock for a CSR.
 * 
 * @author Srinivas Makam
 */

public class LockDAO 
{
    private Connection connection;
    private static Logger logger  = new Logger("com.ftd.customerordermanagement.dao.LockDAO");

  public LockDAO(Connection connection)
  {
    this.connection = connection;
  }

  public Document checkLockXML(String sessionId, String userId, String lockId, String applicationName) throws Exception
  {
        logger.debug("Im in 4 param method");
        Document lock = null;
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("CHECK_CSR_LOCKED_ENTITIES");
        dataRequest.addInputParam("IN_SESSION_ID", sessionId);
        dataRequest.addInputParam("IN_CSR_ID", userId);
        dataRequest.addInputParam("IN_ENTITY_TYPE", applicationName);
        dataRequest.addInputParam("IN_ENTITY_ID", lockId);
        dataRequest.addInputParam("IN_ORDER_LEVEL", null);
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        lock = (Document) dataAccessUtil.execute(dataRequest);
        
        return lock;
  }

  public Document checkLockXML
      (String sessionId, String userId, String lockId, String applicationName, String orderLevel) 
      throws Exception
  {
        logger.debug("Im in 5 param method");
        logger.debug(orderLevel);
        Document lock = null;
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("CHECK_CSR_LOCKED_ENTITIES");
        dataRequest.addInputParam("IN_SESSION_ID",      sessionId);
        dataRequest.addInputParam("IN_CSR_ID",          userId);
        dataRequest.addInputParam("IN_ENTITY_TYPE",     applicationName);
        dataRequest.addInputParam("IN_ENTITY_ID",       lockId);
        dataRequest.addInputParam("IN_ORDER_LEVEL",     orderLevel);
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        lock = (Document) dataAccessUtil.execute(dataRequest);
        
        return lock;
  }


  public Document retrieveLockXML(String sessionId, String userId, String lockId, String applicationName) throws Exception
  {
        Document lock = null;
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("UPDATE_CSR_LOCKED_ENTITIES");
        dataRequest.addInputParam("IN_SESSION_ID", sessionId);
        dataRequest.addInputParam("IN_CSR_ID", userId);
        dataRequest.addInputParam("IN_ENTITY_TYPE", applicationName);
        dataRequest.addInputParam("IN_ENTITY_ID", lockId);
        dataRequest.addInputParam("IN_ORDER_LEVEL", null);
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        lock = (Document) dataAccessUtil.execute(dataRequest);
        
        String status = DOMUtil.getNodeValue(lock, COMConstants.STATUS_PARAM);

        //if the status is N, its a database error.  so, throw an error.  Check for the record
        //to be locked or not will be handle via OUT_LOCKED_OBTAINED parm in the BO/Action
        if(status != null && status.equalsIgnoreCase(COMConstants.CONS_NO))
        {
            String message = DOMUtil.getNodeValue(lock, COMConstants.MESSAGE_PARAM);

            logger.debug("****LockDAO**** An DB error was encountered while retrieving a lock.  Error text = "  + message);
            throw new Exception(message);
        }
        
        return lock;
  }


  public Document retrieveLockXML
        (String sessionId, String userId, String lockId, String applicationName, String orderLevel) 
        throws Exception
  {
        Document lock = null;
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("UPDATE_CSR_LOCKED_ENTITIES");
        dataRequest.addInputParam("IN_SESSION_ID",    sessionId);
        dataRequest.addInputParam("IN_CSR_ID",        userId);
        dataRequest.addInputParam("IN_ENTITY_TYPE",   applicationName);
        dataRequest.addInputParam("IN_ENTITY_ID",     lockId);
        dataRequest.addInputParam("IN_ORDER_LEVEL",   orderLevel);
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        lock = (Document) dataAccessUtil.execute(dataRequest);
        
        String status = DOMUtil.getNodeValue(lock, COMConstants.STATUS_PARAM);

        //if the status is N, its a database error.  so, throw an error.  Check for the record
        //to be locked or not will be handle via OUT_LOCKED_OBTAINED parm in the BO/Action
        if(status != null && status.equalsIgnoreCase(COMConstants.CONS_NO))
        {
            String message = DOMUtil.getNodeValue(lock, COMConstants.MESSAGE_PARAM);

            logger.debug("****LockDAO**** An DB error was encountered while retrieving a lock.  Error text = "  + message);
            throw new Exception(message);
        }
        
        return lock;
        
  }


  public void releaseLock(String sessionId, String userId, String lockId, String applicationName) throws Exception
  {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("DELETE_CSR_LOCKED_ENTITIES");
        dataRequest.addInputParam("IN_SESSION_ID", sessionId);
        dataRequest.addInputParam("IN_CSR_ID", userId);
        dataRequest.addInputParam("IN_ENTITY_TYPE", applicationName);
        dataRequest.addInputParam("IN_ENTITY_ID", lockId);
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);          
        String status = (String) outputs.get(COMConstants.STATUS_PARAM);
        String message = (String) outputs.get(COMConstants.MESSAGE_PARAM);
        if(status != null && status.equals("N"))
        {
            // Do not throw exception because we do not want the application
            // to error out if the lock is not present to delete
            logger.debug(message);
        }
        logger.debug("Lock successfuly released: " + message);
  }
  
  public Document getCsrLockedEntities (String userId) 
        throws Exception
  {
        Document csrLockedEntities = null;
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("GET_CSR_LOCKED_ENTITIES");
        dataRequest.addInputParam("IN_CSR_ID", userId);
                
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        csrLockedEntities = (Document) dataAccessUtil.execute(dataRequest);
        
        String status = DOMUtil.getNodeValue(csrLockedEntities, COMConstants.STATUS_PARAM);

        //if the status is N, its a database error.  so, throw an error.  
        if(status != null && status.equalsIgnoreCase(COMConstants.CONS_NO))
        {
            String message = DOMUtil.getNodeValue(csrLockedEntities, COMConstants.MESSAGE_PARAM);

            logger.debug("****LockDAO**** An DB error was encountered while retrieving the csr locked entities.  Error text = "  + message);
            throw new Exception(message);
        }
        
        return csrLockedEntities;
        
  }
  
  public void releaseLocksForCsr(String csrId, String applicationName, String lockId) throws Exception
  {
        //this stored proc is used by the unlock record process
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("RELEASE_LOCKS_FOR_CSR");
        
        dataRequest.addInputParam("IN_CSR_ID", csrId);
        dataRequest.addInputParam("IN_ENTITY_TYPE", applicationName);
        dataRequest.addInputParam("IN_ENTITY_ID", lockId);
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);          
        String status = (String) outputs.get(COMConstants.STATUS_PARAM);
        String message = (String) outputs.get(COMConstants.MESSAGE_PARAM);
        
        if(status != null && status.equals("N"))
        {
            logger.debug("****LockDAO**** An DB error was encountered while releasing the csr locked entities.  Error text = "  + message);
            throw new Exception(message);
        }
       
  }
  
  public void resetScrubStatus(String masterOrderNumber) throws Exception
  {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("UPDATE_TO_PREV_STATUS_ANY_MON");
        dataRequest.addInputParam("IN_MASTER_ORDER_NUMBER", masterOrderNumber);
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);          
        String status = (String) outputs.get(COMConstants.STATUS_PARAM);
        String message = (String) outputs.get(COMConstants.MESSAGE_PARAM);
        if(status != null && status.equals("N"))
        {
            logger.debug(message);
        }
        
        logger.debug("Scrub status successfuly reset: " + message);
  }
  
  
  public String isCsrLastInScrub(String masterOrderNumber, String csrId) throws Exception
  {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("IS_CSR_LAST_IN_SCRUB_ORDER");
        dataRequest.addInputParam("IN_MASTER_ORDER_NUMBER", masterOrderNumber);
        dataRequest.addInputParam("IN_CSR_ID", csrId);
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        String output = (String) dataAccessUtil.execute(dataRequest);   
        
        logger.debug("Was the csr " + csrId +  " last in scrub for " + masterOrderNumber + ": " + output);
        
        return output;
  }
  
    public HashMap findMasterOrderNumber(String a_orderNum) throws Exception 
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("FIND_ORDER_NUMBER");
        dataRequest.addInputParam("IN_ORDER_NUMBER", a_orderNum);
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        HashMap searchResults = (HashMap) dataAccessUtil.execute(dataRequest);
         
        return searchResults;
    }
  
  
}