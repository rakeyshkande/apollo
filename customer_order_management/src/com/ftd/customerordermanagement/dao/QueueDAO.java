package com.ftd.customerordermanagement.dao;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.HashMap;

import java.util.Map;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;


/**
 * This class handles queue table interactions.
 *
 * @author Mike Kruger
 */

public class QueueDAO
{
    private Logger logger = new Logger("com.ftd.customerordermanagement.dao.QueueDAO");

    //database
    private DataRequest request;
    private Connection conn;

    private final static String DB_IS_ORDER_IN_CREDIT  = "IS_ORDER_IN_CREDIT_Q_HISTORY";
    private final static String DB_DELETE_QUEUE_NO_AUTH  = "DELETE_QUEUE_RECORD_NO_AUTH";

    private final static String DB_IN_ORDER_DETAIL_ID  = "IN_ORDER_DETAIL_ID";
    private final static String DB_IN_MESSAGE_ID  = "IN_MESSAGE_ID";
    private final static String DB_IN_CSR_ID  = "IN_CSR_ID";

    /**
     * constructor
     * 
     * @param Connection - database connection
     * @return n/a
     * @throws none
     */
    public QueueDAO(Connection conn)
    {
        super();
        this.conn = conn;
    }


    /**
    * Obtain information for a Credit queue item if one exists for an
		* order
    *     
    * @param String  - orderDetailId
    * 
    * @return HashMap - Credit queue item information
    * 
    * @throws IOException
    * @throws ParserConfigurationException
    * @throws SAXException
    * @throws SQLException
    */
    public CachedResultSet isOrderInCredit(String orderDetailId)
        throws IOException, ParserConfigurationException, SAXException, SQLException
    {
        HashMap inputParams = new HashMap();
				CachedResultSet crs = null;

        // 1. build db stored procedure input parms
				if(logger.isDebugEnabled())
				{
					logger.debug("input parameters for " + DB_IS_ORDER_IN_CREDIT + " stored procedure are: \n" + 
							DB_IN_ORDER_DETAIL_ID + " = " + orderDetailId);					
				}

        inputParams.put(DB_IN_ORDER_DETAIL_ID, orderDetailId);

        //build DataRequest object
        request = new DataRequest();
        request.reset();
        request.setConnection(conn);
        request.setInputParams(inputParams);
        request.setStatementID(DB_IS_ORDER_IN_CREDIT);

        // 2. get data
        DataAccessUtil dau = DataAccessUtil.getInstance();
        crs = (CachedResultSet) dau.execute(request);
        return crs;
    }
    
		
  /** Deletes a record from the queue without authorization
   * 
   * @param String messageId
   * @param String csrId
	 * 
   * @throws IOException
   * @throws ParserConfigurationException
   * @throws SAXException
   * @throws SQLException
   */
  public void deleteFromQueue(String messageId, String csrId)
		throws IOException, ParserConfigurationException, SAXException, SQLException
  {
    DataRequest dataRequest = new DataRequest();    
     
		if(logger.isDebugEnabled())
		{
			logger.debug("input parameters for " + DB_DELETE_QUEUE_NO_AUTH + " stored procedure are: \n" + 
					DB_IN_MESSAGE_ID + " = " + messageId + "\n" + 
					DB_IN_CSR_ID + " = " + csrId);					
		}

    /* setup store procedure input parameters */
    HashMap inputParams = new HashMap();
    inputParams.put(DB_IN_MESSAGE_ID, messageId);
    inputParams.put(DB_IN_CSR_ID, csrId);    

    /* build DataRequest object */
    dataRequest.setConnection(conn);
    dataRequest.setStatementID(DB_DELETE_QUEUE_NO_AUTH);
    dataRequest.setInputParams(inputParams);
    
    /* execute the store prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
    /* read store procedure output parameters to determine 
     * if the procedure executed successfully */
    String status = (String) outputs.get("OUT_STATUS");
    if(status != null && status.equalsIgnoreCase("N"))
    {
      String message = (String) outputs.get("OUT_MESSAGE");
      throw new SQLException(message);
    }
  }          
}