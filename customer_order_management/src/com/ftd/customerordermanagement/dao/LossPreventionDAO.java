package com.ftd.customerordermanagement.dao;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.HashMap;

import java.util.Map;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;


/**
 * This class obtains loss prevention data.
 *
 * @author Matt Wilcoxen
 */

public class LossPreventionDAO
{
    private Logger logger = new Logger("com.ftd.customerordermanagement.dao.LossPreventionDAO");

    //database
    private DataRequest request;
    private Connection conn;

    //database:  stored procedure names - QUEUE_PKG
    private final static String DB_GET_LP_INFO  = "GET_LP_INFO";

    //database:  ORDER_QUERY_PKG.GET_LP_INFO db stored procedure input parms
    private final static String DB_IN_ORDER_GUID    = "IN_ORDER_GUID";

    /**
     * constructor
     * 
     * @param Connection - database connection
     * @return n/a
     * @throws none
     */
    public LossPreventionDAO(Connection conn)
    {
        super();
        this.conn = conn;
    }


    /**
    * get loss prevention information
    *     1. build db stored procedure input parms
    *     2. get db data
    *     
    * @param String  - order GUID
    * 
    * @return HashMap - Loss Prevention information
    * 
    * @throws IOException
    * @throws ParserConfigurationException
    * @throws SAXException
    * @throws SQLException
    */
    public HashMap getLossPreventionData(String orderGuid)
        throws IOException, ParserConfigurationException, SAXException, SQLException
    {
        HashMap inputParams = new HashMap();
        HashMap outParametersMap = new HashMap();

        // 1. build db stored procedure input parms
        logger.debug("input parameters for " + DB_GET_LP_INFO + " stored procedure are: \n" + 
            DB_IN_ORDER_GUID + " = " + orderGuid);

        inputParams.put(DB_IN_ORDER_GUID, orderGuid);

        //build DataRequest object
        request = new DataRequest();
        request.reset();
        request.setConnection(conn);
        request.setInputParams(inputParams);
        request.setStatementID(DB_GET_LP_INFO);

        // 2. get data
        DataAccessUtil dau = DataAccessUtil.getInstance();
        outParametersMap = (HashMap) dau.execute(request);
        return outParametersMap;
    }
    
  /* Deletes a record from the queue
   * 
     * 
     * @param orderDetailId
     * @param queueType
     * @param csr
     * @throws java.lang.Exception
     */
  public void deleteFromQueue(String orderDetailId, String queueType, String csr) throws Exception
  {
    DataRequest dataRequest = new DataRequest();    
     
    /* setup store procedure input parameters */
    HashMap inputParams = new HashMap();
    inputParams.put("IN_ORDER_DETAIL_ID", orderDetailId);
    inputParams.put("IN_QUEUE_TYPE", queueType);    
    inputParams.put("IN_CSR", csr);    

    /* build DataRequest object */
    dataRequest.setConnection(conn);
    dataRequest.setStatementID("DELETE_FROM_QUEUE");
    dataRequest.setInputParams(inputParams);
    
    /* execute the store prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
    /* read store prodcedure output parameters to determine 
     * if the procedure executed successfully */
    String status = (String) outputs.get("OUT_STATUS");
    if(status != null && status.equalsIgnoreCase("N"))
    {
      String message = (String) outputs.get("OUT_MESSAGE");
      throw new Exception(message);
    }
      
  }          

}