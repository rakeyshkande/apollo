package com.ftd.customerordermanagement.dao;

import com.ftd.customerordermanagement.vo.BillingVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.math.BigDecimal;
import java.sql.Connection;

import java.util.Map;

import org.w3c.dom.Document;



/**
 * BillingDAO
 * This is the DAO that handles loading and updating Order Billing information
 * 
 * @author Luke W. Pennings
 * @version $Id: BillingDAO.java
 */

public class BillingDAO 
{
    private Connection connection;
    private static Logger logger  = new Logger("com.ftd.customerordermanagement.dao.BillingDAO");
    
  /**
   * Constructor
   * @param connection Connection
   */
  public BillingDAO(Connection connection)
  {
    this.connection = connection;
  }

  /**
   * method for inserting billing info.
   * @return String billId 
   * @throws java.lang.Exception
   */

  public BigDecimal addBilling(BillingVO bVO) 
        throws Exception
  {
    Document searchResults = DOMUtil.getDocument();
    Map outputs = null; 
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(this.connection);
    dataRequest.setStatementID("INSERT_ORDER_BILLS");
    dataRequest.addInputParam("IN_ORDER_DETAIL_ID", new Long(bVO.getOrderDetailId()));
    dataRequest.addInputParam("IN_PRODUCT_AMOUNT", new Double(bVO.getProductAmt()));
    dataRequest.addInputParam("IN_ADD_ON_AMOUNT", new Double(bVO.getAddOnAmt()));
    dataRequest.addInputParam("IN_SERVICE_FEE", new Double(bVO.getServiceFee()));
    dataRequest.addInputParam("IN_SHIPPING_FEE", new Double(bVO.getShippingFee()));
    dataRequest.addInputParam("IN_DISCOUNT_AMOUNT", new Double(bVO.getDiscountAmt()));
    dataRequest.addInputParam("IN_SHIPPING_TAX", new Double(bVO.getShippingTax()));
    dataRequest.addInputParam("IN_TAX", new Double(bVO.getProductTax()));
    dataRequest.addInputParam("IN_ADDITIONAL_BILL_INDICATOR", bVO.getAddtBillInd());
    dataRequest.addInputParam("IN_CREATED_BY",  bVO.getCreatedBy());
    dataRequest.addInputParam("IN_SAME_DAY_FEE", null);
    dataRequest.addInputParam("IN_BILL_STATUS",  bVO.getBillStatus());
    dataRequest.addInputParam("IN_SERVICE_FEE_TAX", null);
    dataRequest.addInputParam("IN_COMMISSION_AMOUNT", null);
    dataRequest.addInputParam("IN_WHOLESALE_AMOUNT", null);
    dataRequest.addInputParam("IN_TRANSACTION_AMOUNT", null);
    dataRequest.addInputParam("IN_PDB_PRICE", null);
    dataRequest.addInputParam("IN_DISCOUNT_PRODUCT_PRICE", new Double(bVO.getDiscountProdPrice()));
    dataRequest.addInputParam("IN_DISCOUNT_TYPE", bVO.getDiscountType());
    dataRequest.addInputParam("IN_PARTNER_COST", null);  
    dataRequest.addInputParam("IN_FIRST_ORDER_DOMESTIC", null);
    dataRequest.addInputParam("IN_FIRST_ORDER_INTERNATIONAL", null);
    dataRequest.addInputParam("IN_SHIPPING_COST", null);
    dataRequest.addInputParam("IN_VENDOR_CHARGE", null);
    dataRequest.addInputParam("IN_VENDOR_SAT_UPCHARGE", null);
    dataRequest.addInputParam("IN_AK_HI_SPECIAL_SVC_CHARGE", null);
    dataRequest.addInputParam("IN_FUEL_SURCHARGE_AMT", null);
    dataRequest.addInputParam("IN_SHIPPING_FEE_SAVED", null);
    dataRequest.addInputParam("IN_SERVICE_FEE_SAVED", null);    

    dataRequest.addInputParam("IN_TAX1_NAME", bVO.getTax1Name());
    dataRequest.addInputParam("IN_TAX1_AMOUNT", bVO.getTax1Amount());
    dataRequest.addInputParam("IN_TAX1_DESCRIPTION", bVO.getTax1Description());
    dataRequest.addInputParam("IN_TAX1_RATE", bVO.getTax1Rate());
    
    dataRequest.addInputParam("IN_TAX2_NAME", bVO.getTax2Name());
    dataRequest.addInputParam("IN_TAX2_AMOUNT", bVO.getTax2Amount());
    dataRequest.addInputParam("IN_TAX2_DESCRIPTION", bVO.getTax2Description());
    dataRequest.addInputParam("IN_TAX2_RATE", bVO.getTax2Rate());

    dataRequest.addInputParam("IN_TAX3_NAME", bVO.getTax3Name());
    dataRequest.addInputParam("IN_TAX3_AMOUNT", bVO.getTax3Amount());
    dataRequest.addInputParam("IN_TAX3_DESCRIPTION", bVO.getTax3Description());
    dataRequest.addInputParam("IN_TAX3_RATE", bVO.getTax3Rate());
    
    dataRequest.addInputParam("IN_TAX4_NAME", bVO.getTax4Name());
    dataRequest.addInputParam("IN_TAX4_AMOUNT", bVO.getTax4Amount());
    dataRequest.addInputParam("IN_TAX4_DESCRIPTION", bVO.getTax4Description());
    dataRequest.addInputParam("IN_TAX4_RATE", bVO.getTax4Rate());
    
    dataRequest.addInputParam("IN_TAX5_NAME", bVO.getTax5Name());
    dataRequest.addInputParam("IN_TAX5_AMOUNT", bVO.getTax5Amount());
    dataRequest.addInputParam("IN_TAX5_DESCRIPTION", bVO.getTax5Description());
    dataRequest.addInputParam("IN_TAX5_RATE", bVO.getTax5Rate());
    dataRequest.addInputParam("IN_SAME_DAY_UPCHARGE", null);
    dataRequest.addInputParam("IN_MORNING_DELIVERY_FEE", null);
    dataRequest.addInputParam("IN_LATE_CUTOFF_FEE", null);
    dataRequest.addInputParam("IN_VENDOR_SUN_UPCHARGE", null);
    dataRequest.addInputParam("IN_VENDOR_MON_UPCHARGE", null);
    
    
    
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    outputs = (Map) dataAccessUtil.execute(dataRequest);
    String status = (String) outputs.get("OUT_STATUS");
    if(status != null && status.equalsIgnoreCase("N"))
    {
      String message = (String) outputs.get("OUT_MESSAGE");
      throw new Exception(message);
    }
    return (BigDecimal) outputs.get("OUT_ORDER_BILL_ID");
  }


  /**
   * Inserts billing info into Modify Order temp tables
   * @param bVO - Billing information 
   * @return BigDecimal - Bill id 
   * @throws java.lang.Exception
   */
  public BigDecimal addBillingMO(BillingVO bVO) 
        throws Exception
  {
    Document searchResults = DOMUtil.getDocument();
    Map outputs = null; 
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(this.connection);
    dataRequest.setStatementID("INSERT_ORDER_BILLS_UPDATE");
    dataRequest.addInputParam("IN_ORDER_DETAIL_ID", new Long(bVO.getOrderDetailId()));
    dataRequest.addInputParam("IN_PRODUCT_AMOUNT", new Double(bVO.getProductAmt()));
    dataRequest.addInputParam("IN_ADD_ON_AMOUNT", new Double(bVO.getAddOnAmt()));
    dataRequest.addInputParam("IN_SERVICE_FEE ", new Double(bVO.getServiceFee()));
    dataRequest.addInputParam("IN_SHIPPING_FEE", new Double(bVO.getShippingFee()));
    dataRequest.addInputParam("IN_DISCOUNT_AMOUNT", new Double(bVO.getDiscountAmt()));
    dataRequest.addInputParam("IN_SHIPPING_TAX", new Double(bVO.getShippingTax()));
    dataRequest.addInputParam("IN_TAX", new Double(bVO.getProductTax()));
    dataRequest.addInputParam("IN_ADDITIONAL_BILL_INDICATOR", bVO.getAddtBillInd());
    dataRequest.addInputParam("IN_UPDATED_BY",  bVO.getCreatedBy());
    dataRequest.addInputParam("IN_SAME_DAY_FEE", null);
    dataRequest.addInputParam("IN_BILL_STATUS",  bVO.getBillStatus());
    dataRequest.addInputParam("IN_SERVICE_FEE_TAX", new Double(bVO.getServiceFeeTax()));
    dataRequest.addInputParam("IN_COMMISSION_AMOUNT", null);
    dataRequest.addInputParam("IN_WHOLESALE_AMOUNT", null);
    dataRequest.addInputParam("IN_TRANSACTION_AMOUNT", null);
    dataRequest.addInputParam("IN_PDB_PRICE", null);
    dataRequest.addInputParam("IN_DISCOUNT_PRODUCT_PRICE", new Double(bVO.getDiscountProdPrice()));
    dataRequest.addInputParam("IN_DISCOUNT_TYPE", bVO.getDiscountType());
    dataRequest.addInputParam("IN_PARTNER_COST", null);  
    dataRequest.addInputParam("IN_ACCT_TRANS_IND", bVO.getAcctTransInd());
    dataRequest.addInputParam("IN_SHIPPING_FEE_SAVED", null);
    dataRequest.addInputParam("IN_SERVICE_FEE_SAVED", null);       
    dataRequest.addInputParam("IN_SERVICE_FEE_SAVED", null);       
    dataRequest.addInputParam("IN_TAX1_NAME", bVO.getTax1Name());       
    dataRequest.addInputParam("IN_TAX1_DESCRIPTION", bVO.getTax1Description());       
    dataRequest.addInputParam("IN_TAX1_RATE", bVO.getTax1Rate());       
    dataRequest.addInputParam("IN_TAX1_AMOUNT", bVO.getTax1Amount());       
    dataRequest.addInputParam("IN_TAX2_NAME", bVO.getTax2Name());       
    dataRequest.addInputParam("IN_TAX2_DESCRIPTION", bVO.getTax2Description());       
    dataRequest.addInputParam("IN_TAX2_RATE", bVO.getTax2Rate());       
    dataRequest.addInputParam("IN_TAX2_AMOUNT", bVO.getTax2Amount());       
    dataRequest.addInputParam("IN_TAX3_NAME", bVO.getTax3Name());       
    dataRequest.addInputParam("IN_TAX3_DESCRIPTION", bVO.getTax3Description());       
    dataRequest.addInputParam("IN_TAX3_RATE", bVO.getTax3Rate());       
    dataRequest.addInputParam("IN_TAX3_AMOUNT", bVO.getTax3Amount());       
    dataRequest.addInputParam("IN_TAX4_NAME", bVO.getTax4Name());       
    dataRequest.addInputParam("IN_TAX4_DESCRIPTION", bVO.getTax4Description());       
    dataRequest.addInputParam("IN_TAX4_RATE", bVO.getTax4Rate());       
    dataRequest.addInputParam("IN_TAX4_AMOUNT", bVO.getTax4Amount());       
    dataRequest.addInputParam("IN_TAX5_NAME", bVO.getTax5Name());       
    dataRequest.addInputParam("IN_TAX5_DESCRIPTION", bVO.getTax5Description());       
    dataRequest.addInputParam("IN_TAX5_RATE", bVO.getTax5Rate());       
    dataRequest.addInputParam("IN_TAX5_AMOUNT", bVO.getTax5Amount());
    dataRequest.addInputParam("IN_SAME_DAY_UPCHARGE", null);

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    outputs = (Map) dataAccessUtil.execute(dataRequest);
    String status = (String) outputs.get("OUT_STATUS");
    if(status != null && status.equalsIgnoreCase("N"))
    {
      String message = (String) outputs.get("OUT_MESSAGE");
      throw new Exception(message);
    }
    return (BigDecimal) outputs.get("OUT_ORDER_BILL_ID");
  }
}