package com.ftd.customerordermanagement.dao;

import com.ftd.customerordermanagement.constants.COMConstants;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.HashMap;

import javax.xml.parsers.ParserConfigurationException;



import org.w3c.dom.Document;

import org.xml.sax.SAXException;


/**
 * This class handles tagging database interactions.
 *
 * @author Matt Wilcoxen
 */

public class TagDAO
{
    private Logger logger = new Logger("com.ftd.customerordermanagement.dao.TagDAO");

    //database
    private DataRequest request;
    private Connection conn;

    //database:  stored procedure names - QUEUE_PKG
    private final static String DB_INSERT_ORDER_QUEUE_TAG  = "INSERT_ORDER_QUEUE_TAG";
    private final static String DB_GET_TAG_DISPOSITION_VAL = "GET_TAG_DISPOSITION_VAL";
    private final static String DB_GET_TAG_PRIORITY_VAL    = "GET_TAG_PRIORITY_VAL";
    private final static String DB_GET_ORDER_TAG_CSR       = "GET_ORDER_TAG_CSR";
    private final static String DB_GET_TAG_CSR             = "GET_TAG_CSR";

    //database:  QUEUE_PKG.INSERT_ORDER_QUEUE_TAG db stored procedure input parms
    private final static String DB_IN_ORDER_DETAIL_ID    = "IN_ORDER_DETAIL_ID";
    private final static String DB_IN_CSR_ID             = "IN_CSR_ID";
    private final static String DB_IN_TAG_PRIORITY       = "IN_TAG_PRIORITY";
    private final static String DB_IN_TAG_DISPOSITION    = "IN_TAG_DISPOSITION";
    private final static String DB_IN_SESSION_ID         = "IN_SESSION_ID";

    //constants
    private final static String MULTIPLE_TAGS = "multiple_tags";
    private final static String MAXIMUM_TAGS_ALREADY_EXIST_MSG = "Order has the maximum number of tags";

    private final static String ORDER_ALREADY_TAGGED_TO_CSR = "order_already_tagged_to_csr";
    private final static String ORDER_ALREADY_TAGGED_TO_CSR_MSG = "Order already tagged to csr";

    /**
     * constructor
     * 
     * @param Connection - database connection
     * @return n/a
     * @throws none
     */
    public TagDAO(Connection conn)
    {
        super();
        this.conn = conn;
    }


    /**
    * insert a tag for the selected order.
    *     1. build db stored procedure input parms
    *     2. get db data
    *     3. check for problem in db call
    *     
    * Throw SQLException if a db error occurs.
    * 
    * @param String  - disposition code
    * @param String  - priority code
    * @param String  - csr Id
    * @param String  - order detail id
    * @param String  - security token
    * 
    * @return HashMap - multiple tags on order ?
    *                 - order already tagged to csr ?
    * 
    * @throws IOException
    * @throws ParserConfigurationException
    * @throws SAXException
    * @throws SQLException
    */
    public HashMap insertOrderTag(String dispositionCode, String priorityCode,
                                 String csrId, String orderDetailId, String securityToken)
        throws IOException, ParserConfigurationException, SAXException, SQLException
    {
        HashMap inputParams = new HashMap();
        HashMap outParametersMap = new HashMap();
        HashMap insertOrderTagResultMap = new HashMap();
        boolean multipleTags = false;
        boolean orderAlreadyTaggedToCSR = false;

        // 1. build db stored procedure input parms
        logger.debug("stored procedure:  " + DB_INSERT_ORDER_QUEUE_TAG + "\n" +
            DB_IN_ORDER_DETAIL_ID + " = " + orderDetailId + "\n" +
            DB_IN_CSR_ID + " = " + csrId + "\n" +
            DB_IN_TAG_PRIORITY + " = " + priorityCode + "\n" +
            DB_IN_TAG_DISPOSITION + " = " + dispositionCode + "\n" +
            DB_IN_SESSION_ID + " = " + securityToken);

        inputParams.put(DB_IN_ORDER_DETAIL_ID, new Long(new Integer(orderDetailId).longValue()));
        inputParams.put(DB_IN_CSR_ID, csrId);
        inputParams.put(DB_IN_TAG_PRIORITY, priorityCode);
        inputParams.put(DB_IN_TAG_DISPOSITION, dispositionCode);
        inputParams.put(DB_IN_SESSION_ID, securityToken);

        //build DataRequest object
        request = new DataRequest();
        request.reset();
        request.setConnection(conn);
        request.setInputParams(inputParams);
        request.setStatementID(DB_INSERT_ORDER_QUEUE_TAG);

        // 2. get data
        DataAccessUtil dau = DataAccessUtil.getInstance();
        outParametersMap = (HashMap) dau.execute(request);

        String dbCallResult = (String) outParametersMap.get(COMConstants.STATUS_PARAM);
        // 3. check for problem in db call
        if(dbCallResult != null  &&  dbCallResult.equalsIgnoreCase("N"))
        {
            String dbOutMessage = (String) outParametersMap.get(COMConstants.MESSAGE_PARAM);
            if(dbOutMessage.equalsIgnoreCase(ORDER_ALREADY_TAGGED_TO_CSR_MSG))
            {
                orderAlreadyTaggedToCSR = true;
            }
            else if(dbOutMessage.equalsIgnoreCase(MAXIMUM_TAGS_ALREADY_EXIST_MSG))
            {
                multipleTags = true;
            }
            else
            {
                throw new SQLException("QUEUE_PKG." + DB_INSERT_ORDER_QUEUE_TAG + 
                                       " failed with out_message = " + dbOutMessage +
                                       " in_order_detail_id = " + orderDetailId + 
                                       " in_csr_id = " + csrId + 
                                       " in_tag_priority = " + priorityCode + 
                                       " in_tag_disposition = " + dispositionCode);
            }
        }

        insertOrderTagResultMap.put(MULTIPLE_TAGS, new Boolean(multipleTags));
        insertOrderTagResultMap.put(ORDER_ALREADY_TAGGED_TO_CSR, new Boolean(orderAlreadyTaggedToCSR));

        return insertOrderTagResultMap;
    }


    /**
    * get list of tag disposition codes.
    * 
    * @param none
    * 
    * @return Document - list of tag disposition codes.
    * 
    * @throws IOException
    * @throws ParserConfigurationException
    * @throws SAXException
    * @throws SQLException
    */
    public Document getDispositionCodes()
        throws IOException, ParserConfigurationException, SAXException, SQLException
    {
        HashMap outParametersMap = new HashMap();
        HashMap inputParams = new HashMap();

        // 1. build db stored procedure input parms
        // *****  stored procedure takes no input parms  *****
        
        //build DataRequest object
        request = new DataRequest();
        request.reset();
        request.setConnection(conn);
        request.setInputParams(inputParams);
        request.setStatementID(DB_GET_TAG_DISPOSITION_VAL);

        // 2. get data
        DataAccessUtil dau = DataAccessUtil.getInstance();
        return (Document) dau.execute(request);
    }


    /**
    * get list of tag priority codes.
    * 
    * @param none
    * 
    * @return Document - list of tag priority codes.
    * 
    * @throws IOException
    * @throws ParserConfigurationException
    * @throws SAXException
    * @throws SQLException
    */
    public Document getPriorityCodes()
        throws IOException, ParserConfigurationException, SAXException, SQLException
    {
        HashMap outParametersMap = new HashMap();
        HashMap inputParams = new HashMap();

        // 1. build db stored procedure input parms
        // *****  stored procedure takes no input parms  *****
        
        //build DataRequest object
        request = new DataRequest();
        request.reset();
        request.setConnection(conn);
        request.setInputParams(inputParams);
        request.setStatementID(DB_GET_TAG_PRIORITY_VAL);

        // 2. get data
        DataAccessUtil dau = DataAccessUtil.getInstance();
        return (Document) dau.execute(request);
    }
 

    /**
    * get list of csrs assigned to this order.
    *     1. build db stored procedure input parms
    *     2. get db data
    *     
    * @param String  - order detail id
    * 
    * @return Document - list of csrs assigned to order
    * 
    * @throws IOException
    * @throws ParserConfigurationException
    * @throws SAXException
    * @throws SQLException
    */
    public Document getCsrIds(String orderDetailId)
        throws IOException, ParserConfigurationException, SAXException, SQLException
    {
        HashMap inputParams = new HashMap();
        HashMap outParametersMap = new HashMap();

        // 1. build db stored procedure input parms
        logger.debug("stored procedure:  " + DB_GET_ORDER_TAG_CSR);
        logger.debug(DB_IN_ORDER_DETAIL_ID + " = " + orderDetailId);

        inputParams.put(DB_IN_ORDER_DETAIL_ID, new Long(new Integer(orderDetailId).longValue()));

        //build DataRequest object
        request = new DataRequest();
        request.reset();
        request.setConnection(conn);
        request.setInputParams(inputParams);
        request.setStatementID(DB_GET_ORDER_TAG_CSR);

        // 2. get data
        DataAccessUtil dau = DataAccessUtil.getInstance();
        return (Document) dau.execute(request);
    }

    /**
    * get list of csrs assigned to this order and all messages
    * attached to this order.
    *     1. build db stored procedure input parms
    *     2. get db data
    *     
    * @param String  - order detail id
    * 
    * @return Document - list of csrs assigned to order
    * 
    * @throws IOException
    * @throws ParserConfigurationException
    * @throws SAXException
    * @throws SQLException
    */
    public Document getTagCsr(String orderDetailId)
        throws IOException, ParserConfigurationException, SAXException, SQLException
    {
        HashMap inputParams = new HashMap();
        HashMap outParametersMap = new HashMap();

        // 1. build db stored procedure input parms
        logger.debug("stored procedure:  " + DB_GET_TAG_CSR);
        logger.debug(DB_IN_ORDER_DETAIL_ID + " = " + orderDetailId);

        inputParams.put(DB_IN_ORDER_DETAIL_ID, new Long(new Integer(orderDetailId).longValue()));

        //build DataRequest object
        request = new DataRequest();
        request.reset();
        request.setConnection(conn);
        request.setInputParams(inputParams);
        request.setStatementID(DB_GET_TAG_CSR);

        // 2. get data
        DataAccessUtil dau = DataAccessUtil.getInstance();
        return (Document) dau.execute(request);
    }
    
 }
