package com.ftd.customerordermanagement.dao;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;

import java.math.BigDecimal;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.HashMap;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

/**
 * This class updates the database table call_log with new or updated Call Timer
 * information.
 *
 * @author Matt Wilcoxen
 */

public class CallTimerDAO
{
    private Logger logger = new Logger("com.ftd.customerordermanagement.dao.CallTimerDAO");

    //database
    private DataRequest request;
    private Connection conn;

    //database:  stored procedure names - CALL_LOG_PKG
    public final static String DB_INSERT_CALL_LOG = "INSERT_CALL_LOG";
    public final static String DB_UPDATE_CALL_LOG = "UPDATE_CALL_LOG";
    public final static String DB_DELETE_CALL_LOG = "DELETE_CALL_LOG";
    
    //database:  CALL_LOG_PKG.INSERT_CALL_LOG db stored procedure input parms
    public final static String DB_IN_INS_DNIS          = "IN_DNIS";
    public final static String DB_IN_INS_CSR_ID        = "IN_CSR_ID";
    public final static String DB_OUT_INS_CALL_LOG_ID  = "OUT_CALL_LOG_ID";

    //database:  CALL_LOG_PKG.UPDATE_CALL_LOG db stored procedure input parms
    public final static String DB_IN_UPD_CALL_LOG_ID      = "IN_CALL_LOG_ID";
    public final static String DB_IN_UPD_CUSTOMER_ID      = "IN_CUSTOMER_ID";
    public final static String DB_IN_UPD_ORDER_GUID       = "IN_ORDER_GUID";
    public final static String DB_IN_UPD_CALL_REASON_CODE = "IN_CALL_REASON_CODE";
    public final static String DB_IN_UPD_CALL_REASON_TYPE = "IN_CALL_REASON_TYPE";

    //database:  CALL_LOG_PKG.DELETE_CALL_LOG db stored procedure input parms
    public final static String DB_IN_DEL_CALL_LOG_ID  = "IN_CALL_LOG_ID";

    //database:  common output fields from db stored procedures calls
    public final static String DB_OUT_STATUS  = "OUT_STATUS";
    public final static String DB_OUT_MESSAGE = "OUT_MESSAGE";


    /**
     * constructor
     * 
     * @param Connection - database connection
     * @return n/a
     * @throws none
     */
    public CallTimerDAO(Connection conn)
    {
        super();
        this.conn = conn;
    }


    /**
    * add an entry to the call log.
    *     1. build db stored procedure input parms
    *     2. get db data
    * 
    * @param String  - dnis
    * @param String  - csr Id
    * 
    * @return String - call Log Id
    * 
    * @throws IOException
    * @throws ParserConfigurationException
    * @throws SAXException
    * @throws SQLException
    */
    public String insertCallLog(String dnis, String csrId) 
        throws IOException, ParserConfigurationException, SAXException, SQLException
    {
        HashMap inputParams = new HashMap();
        HashMap outParametersMap = new HashMap();

        // 1. build db stored procedure input parms
        logger.debug("stored procedure:  " + DB_INSERT_CALL_LOG);
        logger.debug(DB_IN_INS_DNIS + " = " + dnis);
        logger.debug(DB_IN_INS_CSR_ID + " = " + csrId);
        
        inputParams.put(DB_IN_INS_DNIS, new Long(new Integer(dnis).longValue()));
        inputParams.put(DB_IN_INS_CSR_ID, csrId);

        //build DataRequest object
        request = new DataRequest();
        request.reset();
        request.setConnection(conn);
        request.setInputParams(inputParams);
        request.setStatementID(DB_INSERT_CALL_LOG);

        // 2. get data
        DataAccessUtil dau = DataAccessUtil.getInstance();
        outParametersMap = (HashMap) dau.execute(request);

        String dbCallResult = (String) outParametersMap.get(DB_OUT_STATUS);
        if(dbCallResult != null  &&  dbCallResult.equalsIgnoreCase("N"))
        {
            String dbOutMessage = (String) outParametersMap.get(DB_OUT_MESSAGE);
            throw new SQLException("CALL_LOG_PKG." + DB_INSERT_CALL_LOG + 
                                   " failed with out_message = " + dbOutMessage +
                                   " in_dnis = " + dnis + 
                                   " in_csr_id = " + csrId);
        }

        String callLogId = new Long(((BigDecimal) outParametersMap.get(DB_OUT_INS_CALL_LOG_ID)).longValue()).toString();
        return callLogId;
    }


    /**
    * update an entry to the call log.
    *     1. build db stored procedure input parms
    *     2. get db data
    * 
    * @param String  - call Log Id
    * @param String  - reason code
    * @param String  - reason type
    * 
    * @return none
    * 
    * @throws IOException
    * @throws ParserConfigurationException
    * @throws SAXException
    * @throws SQLException
    */
    public void updateCallLog(String callLogId, String reasonCode, String reasonType)
        throws IOException, ParserConfigurationException, SAXException, SQLException
    {
        HashMap inputParams = new HashMap();
        HashMap outParametersMap = new HashMap();

        // 1. build db stored procedure input parms
        logger.debug("stored procedure:  " + DB_UPDATE_CALL_LOG);
        logger.debug(DB_IN_UPD_CALL_LOG_ID + " = " + callLogId);
        logger.debug(DB_IN_UPD_CALL_REASON_CODE + " = " + reasonCode);
        logger.debug(DB_IN_UPD_CALL_REASON_TYPE + " = " + reasonType);

        inputParams.put(DB_IN_UPD_CALL_LOG_ID, new Long(new Integer(callLogId).longValue()));
        inputParams.put(DB_IN_UPD_CALL_REASON_CODE, reasonCode);
        inputParams.put(DB_IN_UPD_CALL_REASON_TYPE, reasonType);

        //build DataRequest object
        request = new DataRequest();
        request.reset();
        request.setConnection(conn);
        request.setInputParams(inputParams);
        request.setStatementID(DB_UPDATE_CALL_LOG);

        // 2. get data
        DataAccessUtil dau = DataAccessUtil.getInstance();
        outParametersMap = (HashMap) dau.execute(request);

        String dbCallResult = (String) outParametersMap.get(DB_OUT_STATUS);
        if(dbCallResult != null  &&  dbCallResult.equalsIgnoreCase("N"))
        {
            String dbOutMessage = (String) outParametersMap.get(DB_OUT_MESSAGE);
            throw new SQLException("CALL_LOG_PKG." + DB_UPDATE_CALL_LOG + 
                                   " failed with out_message = " + dbOutMessage +
                                   " in_call_log_id = " + callLogId + 
                                   " in_call_reason_code = " + reasonCode +
                                   " in_call_reason_type = " + reasonType);
        }

        return;
   }


    /**
    * delete an entry from the call log.
    *     1. build db stored procedure input parms
    *     2. get db data
    * 
    * @param String  - call Log Id
    * 
    * @return none
    * 
    * @throws IOException
    * @throws ParserConfigurationException
    * @throws SAXException
    * @throws SQLException
    */
    public void deleteCallLog(String callLogId)
        throws IOException, ParserConfigurationException, SAXException, SQLException
    {
        HashMap inputParams = new HashMap();
        HashMap outParametersMap = new HashMap();

        // 1. build db stored procedure input parms
        logger.debug("stored procedure:  " + DB_DELETE_CALL_LOG);
        logger.debug(DB_IN_DEL_CALL_LOG_ID + " = " + callLogId);
        
        inputParams.put(DB_IN_DEL_CALL_LOG_ID, new Long(new Integer(callLogId).longValue()));

        //build DataRequest object
        request = new DataRequest();
        request.reset();
        request.setConnection(conn);
        request.setInputParams(inputParams);
        request.setStatementID(DB_DELETE_CALL_LOG);

        // 2. get data
        DataAccessUtil dau = DataAccessUtil.getInstance();
        outParametersMap = (HashMap) dau.execute(request);

        String dbCallResult = (String) outParametersMap.get(DB_OUT_STATUS);
        if(dbCallResult != null  &&  dbCallResult.equalsIgnoreCase("N"))
        {
            String dbOutMessage = (String) outParametersMap.get(DB_OUT_MESSAGE);
            throw new SQLException("CALL_LOG_PKG." + DB_DELETE_CALL_LOG + 
                                   " failed with out_message = " + dbOutMessage +
                                   " in_call_log_id = " + callLogId);
        }

        return;
   }
 
}