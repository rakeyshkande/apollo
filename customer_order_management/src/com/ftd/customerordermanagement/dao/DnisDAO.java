package com.ftd.customerordermanagement.dao;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.vo.DnisInfoVO;
import com.ftd.customerordermanagement.vo.DnisVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;

import java.util.HashMap;
import java.util.Map;

import org.w3c.dom.Document;



/**
 * DnisDAO
 * 
 * This DAO maintains DNIS data in the database. It loads, updates, inserts, deletes DNIS data.
 * It also loads data for drop down lists such as origin list, country script list, script id list.
 * It checks the existance of a DNIS record or a source code in the database.
 */
public class DnisDAO 
{
    private Connection connection = null;
    private static Logger logger  = new Logger("com.ftd.customerordermanagement.dao.DnisDAO");
    private static final String OUT_CUR = "OUT_CUR";
    private static final String OUT_ID_COUNT = "OUT_ID_COUNT";
    private static final String OUT_PARAMETERS = "out-parameters";
    
/**
 * Constructor
 * @param conn
 */
  public DnisDAO(Connection conn)
  {
    this.connection = conn;
    logger.debug("DnisDAO constructed");
  }
  
  
  public Document loadDnisXML(String currentDnis) throws Exception
  {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        
        dataRequest.setStatementID("GET_DNIS_BY_ID");
        dataRequest.addInputParam("IN_DNIS_ID", currentDnis);
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        return (Document) dataAccessUtil.execute(dataRequest);
  }
  
  public HashMap loadAllDnisXML(int startRecord, int maxRecords) throws Exception
  {
      DataRequest dataRequest = new DataRequest();
      dataRequest.setConnection(connection);
      
      dataRequest.setStatementID("GET_DNIS");
      dataRequest.addInputParam("IN_MAX_NUMBER_RETURNED", new Long(maxRecords));
      dataRequest.addInputParam("IN_START_POSITION", new Long(startRecord));
      
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      return (HashMap) dataAccessUtil.execute(dataRequest);

/*      
      Document dnisXML = (Document) dataAccessUtil.execute(dataRequest);
      
      //get the total number of records in DNIS table
      dataRequest = new DataRequest();
      dataRequest.setConnection(connection);
      dataRequest.setStatementID("GET_TOTAL_COUNT_DNIS");
      CachedResultSet countSet = (CachedResultSet) dataAccessUtil.execute(dataRequest);
      long count = 0;
      while(countSet.next()) {
        count = countSet.getLong(1);
      }
      HashMap countMap = new HashMap();
      countMap.put("dniscount", new Long(count));
      DOMUtil.addSection(dnisXML, "total", "count", countMap, true);
      return dnisXML;
*/
  }
  
  public void updateDnis(String currentDnis, DnisVO dnis) throws Exception
  {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("UPDATE_DNIS");
        dataRequest.addInputParam("IN_YELLOW_PAGES_FLAG", dnis.getYellowPagesFlag());
        dataRequest.addInputParam("IN_DEFAULT_SOURCE_CODE", dnis.getDefaultSourceCode());
        dataRequest.addInputParam("IN_DESCRIPTION", dnis.getDescription());
        dataRequest.addInputParam("IN_DNIS_ID", currentDnis);
        dataRequest.addInputParam("IN_OE_FLAG", dnis.getOeFlag());
        dataRequest.addInputParam("IN_ORIGIN", dnis.getOrigin());
        dataRequest.addInputParam("IN_SCRIPT_CODE", dnis.getScriptCode());
        dataRequest.addInputParam("IN_SCRIPT_ID", dnis.getScriptId());
        dataRequest.addInputParam("IN_STATUS_FLAG", dnis.getStatusFlag());
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();   
        
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
        String status = (String) outputs.get(COMConstants.STATUS_PARAM);
        if(status != null && status.equals("N"))
        {
            String message = (String) outputs.get(COMConstants.MESSAGE_PARAM);
            throw new Exception(message);
        }
        logger.debug("Update Dnis Successful. Status="+status);    
  }
  
  public void insertDnis(DnisVO dnis) throws Exception
  {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("INSERT_DNIS");
        dataRequest.addInputParam("IN_YELLOW_PAGES_FLAG", dnis.getYellowPagesFlag());
        dataRequest.addInputParam("IN_DEFAULT_SOURCE_CODE", dnis.getDefaultSourceCode());
        dataRequest.addInputParam("IN_DESCRIPTION", dnis.getDescription());
        dataRequest.addInputParam("IN_DNIS_ID", new Long(dnis.getDnisId()));
        dataRequest.addInputParam("IN_OE_FLAG", dnis.getOeFlag());
        dataRequest.addInputParam("IN_ORIGIN", dnis.getOrigin());
        dataRequest.addInputParam("IN_SCRIPT_CODE", dnis.getScriptCode());
        dataRequest.addInputParam("IN_SCRIPT_ID", dnis.getScriptId());
        dataRequest.addInputParam("IN_STATUS_FLAG", dnis.getStatusFlag());
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();        
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
        String status = (String) outputs.get(COMConstants.STATUS_PARAM);
        if(status != null && status.equals("N"))
        {
            String message = (String) outputs.get(COMConstants.MESSAGE_PARAM);
            throw new Exception(message);
        }
        logger.debug("Insert Dnis Successful. Status="+status);
    
  }
  
  public void deleteDnis(String currentDnis) throws Exception
  {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("DELETE_DNIS");
        dataRequest.addInputParam("IN_DNIS_ID", new Long(currentDnis));
       
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();        
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
        String status = (String) outputs.get(COMConstants.STATUS_PARAM);
        if(status != null && status.equals("N"))
        {
            String message = (String) outputs.get(COMConstants.MESSAGE_PARAM);
            throw new Exception(message);
        }
        logger.debug("Delete Dnis Successful. Status="+status);    
  }
  
  public Document loadOriginListXML() throws Exception
  {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        
        dataRequest.setStatementID("GET_COMPANY");
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        return (Document) dataAccessUtil.execute(dataRequest);
  }
  
  public Document loadCountryScriptListXML() throws Exception
  {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        
        dataRequest.setStatementID("GET_DNIS_SCRIPT_CODE");
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        return (Document) dataAccessUtil.execute(dataRequest);
  }
  
  public Document loadScriptListXML() throws Exception
  {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        
        dataRequest.setStatementID("GET_SCRIPT_ID");
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        return (Document) dataAccessUtil.execute(dataRequest);
  }
  
  public boolean dnisExists(String dnisNumber) throws Exception
  {
        Document dnisXML = loadDnisXML(dnisNumber);
        boolean found = dnisXML.getChildNodes().item(0).hasChildNodes();
        return found;
  }
  
  public boolean sourceExists(String sourceCode) throws Exception
  {
        boolean found = false;
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        
        dataRequest.setStatementID("GET_SOURCE_CODE_INFO");
        dataRequest.addInputParam("IN_SOURCE_CODE", sourceCode);
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        Document sourceRec = (Document) dataAccessUtil.execute(dataRequest);
        if(sourceRec.getChildNodes().item(0).hasChildNodes())
        {
          found = true;
        }
        return found;
}

  public DnisInfoVO getDnisData(String dnisId) throws Exception
  {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        
        dataRequest.setStatementID("GET_DNIS_PHONE_NUMBER");
        dataRequest.addInputParam("IN_DNIS_ID", dnisId);
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        DnisInfoVO diVO = null;
        CachedResultSet dnisInfoRS = null;

        try
        {
            dnisInfoRS = (CachedResultSet) dataAccessUtil.execute(dataRequest);
            
            if(dnisInfoRS.next())
            {
                // object(1) = dnis Id - we are requesting the data for this
                // object(2) = oe flag
                // object(3) = cs number (phone number)
                // object(4) = brand name (company name)
                diVO = new DnisInfoVO();
                diVO.setDnis(dnisId);
                diVO.setOEFlag((String) dnisInfoRS.getObject(2));
                diVO.setCSNumber((String) dnisInfoRS.getObject(3));
                diVO.setBrandName((String) dnisInfoRS.getObject(4));
                diVO.setStatus((String) dnisInfoRS.getObject(5));
            }
        }
        finally
        {
        }
        
        return diVO;
  }
  
}