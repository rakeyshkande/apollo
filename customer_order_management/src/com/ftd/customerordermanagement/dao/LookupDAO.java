package com.ftd.customerordermanagement.dao;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

import java.math.BigDecimal;
import java.sql.Connection;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;


/**
 * <code>LookupDAO</code> is a generic DAO used to retrieve data for a variety of uses.
 * Reference customer-order-management-statements.xml for corresponding stored procedures.
 * 
 * @author Mark Moon
 */
public class LookupDAO {

    private static Logger logger  = new Logger("com.ftd.customerordermanagement.dao.LookupDAO");
    private Connection connection;

    /**
     * Constructs a <code>LookupDAO</code> using the supplied connection.
     * @param connection The connection for this DAO.
     */
    public LookupDAO(Connection connection) {
        this.connection = connection;
    }


    /**
     * Returns an Document of all states.  The XML format is:
     *
     * <STATES>
     *   <STATE>
     *     {data for single state: statemasterid, statename, countrycode, timezone, taxrate}
     *   </STATE>
     * </STATES>
     *
     * @return Document containing all states
     * @throws java.lang.Exception
     */
    public Document getStates() 
        throws Exception {

        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("GET_STATES_DOM");
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        return (Document) dataAccessUtil.execute(dataRequest);
    }

    /**
     * Returns an Document of all states.  The XML format is:
     *
     * <zipList>
     *   <zip num="{index}">
     *     {data for single zip: zipcode, city, state, countrycode}
     *   </zip>
     * </zipList>
     * 
     * @param zipcode
     * @param city
     * @param state
     * @return Document containing all zipcodes
     * @throws java.lang.Exception
     */
    public Document getZipcodes(String zipcode, String city, String state) 
        throws Exception {

        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("GET_ZIP_CODE_LIST");
        dataRequest.addInputParam("zip_code", zipcode);
        dataRequest.addInputParam("city", city);
        dataRequest.addInputParam("state", state);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        return (Document) dataAccessUtil.execute(dataRequest);
    }

    /**
     * Returns an Document of source code meeting the supplied criteria.  The XML format is:
     * 
     * <sourceCodeList>
     *   <sourceCode>
     *     {data for a single source code: sourcedescription, offerdescription, servicecharge, expirationdate, ordersource, sourcecode}
     *   </sourceCode>
     * </sourceCodeList>
     * 
     * @param sourceCode
     * @param description
     * @param dateFlag
     * @return Document 
     * @throws java.lang.Exception
     */
    public Document getSourceCodes(String searchValue, String companyId)
            throws Exception {

        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("GET_SOURCECODELIST_BY_COMP");
        dataRequest.addInputParam("IN_SEARCH_VALUE", searchValue);
        dataRequest.addInputParam("IN_COMPANY_ID", companyId);
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        return (Document)dataAccessUtil.execute(dataRequest);
    }

    /**
     * Returns an Document of customers/recipients using the supplied phone number.  The XML format is:
     * 
     * <customerList>
     *   <customer>
     *     {data for a single customer: customer_id, first_name, last_name, address_1, address_2, city, state, zip_code}
     *   </customer>
     * </customerList>
     * 
     * @param phoneNumber
     * @param buyerIndicator
     * @param recipientIndicator
     * @return Document 
     * @throws java.lang.Exception
     */
    public Document getCustomer(String phoneNumber, String buyerIndicator, String recipientIndicator)
            throws Exception {

        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("SEARCH_CUSTOMER_BY_PHONE");
        dataRequest.addInputParam("phone_number", phoneNumber);
        dataRequest.addInputParam("buyer_indicator", buyerIndicator);
        dataRequest.addInputParam("recipient_indicator", recipientIndicator);
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        return (Document)dataAccessUtil.execute(dataRequest);
    }

    /**
     * Returns an Document of addons using the supplied parameters.  The XML format is: 
     * 
     * @param addonType
     * @param occasion
     * @param cardId
     * @return Document 
     * @throws java.lang.Exception
     */
    public Document getAddons(String addonType, String occasion, String cardId)
            throws Exception {

        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("GET_ADDONS_BY_TYPE_OCCASION");
        dataRequest.addInputParam("IN_ADDON_TYPE", addonType);
        dataRequest.addInputParam("IN_OCCASION_ID", occasion);
        dataRequest.addInputParam("IN_ADDON_ID", cardId);
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        return (Document)dataAccessUtil.execute(dataRequest);
    }

    /**
     * Returns an Document of businesses/institutions using the supplied parameters.  The XML format is: 
     * 
     * <institutions>
     *   <institution>
     *     {data for a single institution: }
     *   <institution>
     * <institutions>
     * 
     * @param institution
     * @param city
     * @param state
     * @param institutionType
     * @param zip
     * @return Document 
     * @throws java.lang.Exception
     */
    public Document getInstitutions(String institution, String city, String state, String institutionType, String zip)
            throws Exception {

        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("GET_INSTITUTIONS");
        dataRequest.addInputParam("institution_name", institution);
        dataRequest.addInputParam("city", city);
        dataRequest.addInputParam("state", state);
        dataRequest.addInputParam("zip", zip);
        dataRequest.addInputParam("institution_type", institutionType);
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        return (Document)dataAccessUtil.execute(dataRequest);
    }
    
  /**
     * Get fraud description by fraud id.
     * 
     * @param fraudId - Fraud Id
     * @return String - fraud description
     * @throws java.lang.Exception
     */
  public String getFraudDescriptionById(String fraudId) throws Exception 
  {
     if(fraudId == null)
     {
         return "";
     }
  
     DataRequest dataRequest = new DataRequest();
     dataRequest.setConnection(this.connection);
    
     dataRequest.setStatementID("GET_FRAUD_DESCRIPTION_BY_ID");
     Map paramMap = new HashMap();   
     
     paramMap.put("IN_FRAUD_ID", fraudId);

     dataRequest.setInputParams(paramMap);
     DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
     String output = (String) dataAccessUtil.execute(dataRequest);
    
     return output;
  }    

  /**
     * Get color description by color id.
     * 
     * @param colorId
     * @return 
     * @throws java.lang.Exception
     */
  public String getColorDescriptionById(String colorId) throws Exception 
  {
  
     if(colorId == null)
     {
         return "";
     }
  
     colorId = colorId.toUpperCase();
  
     DataRequest dataRequest = new DataRequest();
     dataRequest.setConnection(this.connection);
    
     dataRequest.setStatementID("GET_COLOR_BY_ID");
     Map paramMap = new HashMap();   
     
     paramMap.put("IN_COLOR_ID",colorId);

     dataRequest.setInputParams(paramMap);
     DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
     String output = (String) dataAccessUtil.execute(dataRequest);
    
     return output;
  }    

  /**
     * Get addon description by addon id.
     * 
     * @param colorId
     * @return 
     * @throws java.lang.Exception
     */
  public String getAddonDescriptionById(String addonId) throws Exception 
  {
  
     if(addonId == null)
     {
         return "";
     }
  
     addonId = addonId.toUpperCase();  
  
     DataRequest dataRequest = new DataRequest();
     dataRequest.setConnection(this.connection);
     String addonDescription = "";
    
     dataRequest.setStatementID("GET_ADDON_BY_ID");
     Map paramMap = new HashMap();   
     
     paramMap.put("IN_ADDON_ID",addonId);

     dataRequest.setInputParams(paramMap);
     DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
     CachedResultSet rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);
     
     if(rs.next())
     {
         addonDescription = rs.getString("DESCRIPTION");
     }
    
     return addonDescription;
  }  

    /**
       * Get order detail information by orderDetailId.
       * 
       * @param orderDetailId
       * @return 
       * @throws java.lang.Exception
       */
    public String getExternalOrderNumber(String orderDetailId) throws Exception 
    {
    
       if(orderDetailId == null)
       {
           return "";
       }
    
       DataRequest dataRequest = new DataRequest();
       dataRequest.setConnection(this.connection);
       String externalOrderNumber = "";
      
       dataRequest.setStatementID("GET_ORDER_DETAILS_II");
       Map paramMap = new HashMap();   
       
       paramMap.put("IN_ORDER_DETAIL_ID",orderDetailId);

       dataRequest.setInputParams(paramMap);
       DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
       CachedResultSet rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);
       
       if(rs.next())
       {
           externalOrderNumber = rs.getString("EXTERNAL_ORDER_NUMBER");
       }
      
       return externalOrderNumber;
    }  

    public String getTaxDisplayOrderVal(String taxName) throws Exception 
    {
    	if(StringUtils.isEmpty(taxName)) {
            return "";
        }
     
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        String dispOrderVal = "";
       
        dataRequest.setStatementID("GET_TAX_DISPLAY_ORDER_VAL");
        Map paramMap = new HashMap();   
        paramMap.put("IN_TAX_NAME",taxName);

        dataRequest.setInputParams(paramMap);
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        BigDecimal result = (BigDecimal)dataAccessUtil.execute(dataRequest);
        if(result != null)
        {
        	dispOrderVal = result.toString();
        }
       
        return dispOrderVal;
    }
}