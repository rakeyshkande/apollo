package com.ftd.customerordermanagement.dao;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.vo.CommonCreditCardVO;
import com.ftd.customerordermanagement.vo.CreditCardVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.math.BigDecimal;

import java.sql.Connection;

import java.util.Map;

import org.w3c.dom.Document;


/**
 * CreditCardDAO
 *
 * This is the DAO that handles loading/updating Credit Card info.
 *
 */


public class CreditCardDAO
{
    private Connection connection;
    private static Logger logger  = new Logger("com.ftd.customerordermanagement.dao.CreditCardDAO");
    private static final String NAME_DELIMITER = ",";

  /*
   * Constructor
   * @param connection Connection
   */
  public CreditCardDAO(Connection connection)
  {
    this.connection = connection;

  }

  /**
   * updateCreditCard
   *
   * Updates or inserts Credit Card information for a Customer.
   * Note that a row gets inserted into this table via update_unique_credit_cards_update
   * @param CreditCardVO
   * @returns null
   * @throws java.lang.Exception
   */
  public void updateCreditCard(CreditCardVO ccVO, String custID) throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(this.connection);

    dataRequest.setStatementID("UPDATE_UNIQUE_CREDIT_CARDS");
    dataRequest.addInputParam("IN_CC_TYPE", ccVO.getCcType());
    dataRequest.addInputParam("IN_CC_NUMBER", ccVO.getCcNumber());
    dataRequest.addInputParam("IN_CC_EXPIRATION", ccVO.getCcExpiration());
    dataRequest.addInputParam("IN_NAME", ccVO.getName());
    dataRequest.addInputParam("IN_ADDRESS_LINE_1", ccVO.getAddressLine1());
    dataRequest.addInputParam("IN_ADDRESS_LINE_2", ccVO.getAddressLine2());
    dataRequest.addInputParam("IN_CITY", ccVO.getCity());
    dataRequest.addInputParam("IN_STATE", ccVO.getState());
    dataRequest.addInputParam("IN_ZIP_CODE", ccVO.getZipCode());
    dataRequest.addInputParam("IN_COUNTRY", ccVO.getCountry());
    dataRequest.addInputParam("IN_UPDATED_BY", ccVO.getUpdatedBy());
    dataRequest.addInputParam("IN_CUSTOMER_ID", custID);

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    Map outputs = (Map) dataAccessUtil.execute(dataRequest);
    String status = (String) outputs.get(COMConstants.STATUS_PARAM);
    if(status != null && status.equals("N"))
    {
        String message = (String) outputs.get(COMConstants.MESSAGE_PARAM);
        throw new Exception(message);
    }
    logger.debug("Update Credit Card Successful");
  }


  /**
   * Method to retrieve the last credit card for a customer
   * @param orderDetailId - order detail id
   * @return Document containing the values from the cursor
   *
   */

  public Document getLastCreditCard(String orderDetailId) throws Exception
  {
        Document searchResults = DOMUtil.getDocument();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("GET_LAST_CC_USED");
        dataRequest.addInputParam("IN_ORDER_DETAIL_ID", orderDetailId.trim());

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        searchResults = (Document) dataAccessUtil.execute(dataRequest);      

        return searchResults;
   }

  /**
   * Method to retrieve the last credit card for a customer
   * @param orderDetailId - order detail id
   * @return CreditCardVO containing credit card values
   *
   */

  public CreditCardVO getLastCreditCardRS(String orderDetailId) throws Exception
  {
			CreditCardVO ccVO = new CreditCardVO();
      Document searchResults = DOMUtil.getDocument();
      DataRequest dataRequest = new DataRequest();
      dataRequest.setConnection(this.connection);
      dataRequest.setStatementID("GET_LAST_CC_USED_RS");
      dataRequest.addInputParam("IN_ORDER_DETAIL_ID", orderDetailId.trim());

      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
			CachedResultSet crs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
		
			if(crs.next())
			{
			    ccVO.setCcId(crs.getLong("cc_id"));
			    ccVO.setCcNumber(crs.getString("cc_number"));			
			}
				
			return ccVO;
   }


    /**
   * method for retrieving credit card info.
   * @param orderGuid - order guid
   * @return Document - CC info
   * @throws java.lang.Exception
   */

  public Document getOriginalCreditCardFromOrder(String orderGuid)
        throws Exception
  {
    Document searchResults = DOMUtil.getDocument();
    Map outputs = null;
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(this.connection);
    dataRequest.setStatementID("GET_ORIGINAL_USED_CC");
    dataRequest.addInputParam("IN_ORDER_GUID", orderGuid);

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    searchResults = (Document) dataAccessUtil.execute(dataRequest);

    return searchResults;
  }
    /**
   * method for retrieving credit card info.
   * @param orderGuid - order guid
   * @return Document - CC info
   * @throws java.lang.Exception
   */

  public CreditCardVO getOriginalCreditCardFromOrderRS(String orderGuid)
        throws Exception
  {
    CreditCardVO ccVO = new CreditCardVO();
		Map outputs = null;
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(this.connection);
    dataRequest.setStatementID("GET_ORIGINAL_USED_CC_RS");
    dataRequest.addInputParam("IN_ORDER_GUID", orderGuid);

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    CachedResultSet crs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

		if(crs.next())
		{
			ccVO.setCcId(crs.getLong("cc_id"));
			ccVO.setCcNumber(crs.getString("cc_number"));			
		}
		
		return ccVO;
  }
  /**
   * method for inserting credit card info.
   * @return String ccId
   * @throws java.lang.Exception
   */

  public BigDecimal addCreditCard(CreditCardVO ccVO, CommonCreditCardVO cccVO)
        throws Exception
  {
    Map outputs = null;
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(this.connection);
    dataRequest.setStatementID("UPDATE_UNIQUE_CREDIT_CARDS");
    dataRequest.addInputParam("IN_CC_TYPE", cccVO.getCreditCardType());
    dataRequest.addInputParam("IN_CC_NUMBER", cccVO.getCreditCardNumber());
    dataRequest.addInputParam("IN_CC_EXPIRATION", ccVO.getCcExpiration());
    dataRequest.addInputParam("IN_NAME ", cccVO.getName());
    dataRequest.addInputParam("IN_ADDRESS_LINE_1", cccVO.getAddressLine());
    dataRequest.addInputParam("IN_ADDRESS_LINE_2", cccVO.getAddress2());
    dataRequest.addInputParam("IN_CITY", cccVO.getCity());
    dataRequest.addInputParam("IN_STATE", ccVO.getState());
    dataRequest.addInputParam("IN_ZIP_CODE", cccVO.getZipCode());
    dataRequest.addInputParam("IN_COUNTRY",  cccVO.getCountry());
    dataRequest.addInputParam("IN_UPDATED_BY", ccVO.getUpdatedBy());
    dataRequest.addInputParam("IN_CUSTOMER_ID",  cccVO.getCustomerId());

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    outputs = (Map) dataAccessUtil.execute(dataRequest);
    String status = (String) outputs.get("OUT_STATUS");
    if(status != null && status.equalsIgnoreCase("N"))
    {
      String message = (String) outputs.get("OUT_MESSAGE");
      throw new Exception(message);
    }
    return (BigDecimal) outputs.get("OUT_CC_ID");
  }
  /**
   * method for inserting Gift card info.
   * @return String ccId
   * @throws java.lang.Exception
   */

  public BigDecimal addCreditCardGD(CreditCardVO ccVO, CommonCreditCardVO cccVO)
        throws Exception
  {
    Map outputs = null;
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(this.connection);
    dataRequest.setStatementID("UPDATE_UNIQUE_CREDIT_CARDS_GD");
    dataRequest.addInputParam("IN_CC_TYPE", cccVO.getCreditCardType());
    dataRequest.addInputParam("IN_CC_NUMBER", cccVO.getCreditCardNumber());
    dataRequest.addInputParam("IN_GIFT_CARD_PIN", ccVO.getGiftCardPin());
    dataRequest.addInputParam("IN_CC_EXPIRATION", ccVO.getCcExpiration());
    dataRequest.addInputParam("IN_NAME ", cccVO.getName());
    dataRequest.addInputParam("IN_ADDRESS_LINE_1", cccVO.getAddressLine());
    dataRequest.addInputParam("IN_ADDRESS_LINE_2", cccVO.getAddress2());
    dataRequest.addInputParam("IN_CITY", cccVO.getCity());
    dataRequest.addInputParam("IN_STATE", ccVO.getState());
    dataRequest.addInputParam("IN_ZIP_CODE", cccVO.getZipCode());
    dataRequest.addInputParam("IN_COUNTRY",  cccVO.getCountry());
    dataRequest.addInputParam("IN_UPDATED_BY", ccVO.getUpdatedBy());
    dataRequest.addInputParam("IN_CUSTOMER_ID",  cccVO.getCustomerId());

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    outputs = (Map) dataAccessUtil.execute(dataRequest);
    String status = (String) outputs.get("OUT_STATUS");
    if(status != null && status.equalsIgnoreCase("N"))
    {
      String message = (String) outputs.get("OUT_MESSAGE");
      throw new Exception(message);
    }
    return (BigDecimal) outputs.get("OUT_CC_ID");
  }
  /**
   * method for inserting credit card info. into the Modify Order temp tables
   * @param ccVO - Credit card information
   * @param orderDetailId - Order detail id
   * @return String - Credit card id
   * @throws java.lang.Exception
   */
  public BigDecimal addCreditCardMO(CreditCardVO ccVO, String orderDetailId)
        throws Exception
  {
    Map outputs = null;
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(this.connection);
    dataRequest.setStatementID("INSERT_CREDIT_CARDS_UPDATE");
    dataRequest.addInputParam("IN_ORDER_DETAIL_ID", orderDetailId);
    dataRequest.addInputParam("IN_CC_TYPE", ccVO.getCcType());
    dataRequest.addInputParam("IN_CC_NUMBER", ccVO.getCcNumber());
    dataRequest.addInputParam("IN_CC_EXPIRATION", ccVO.getCcExpiration());
    dataRequest.addInputParam("IN_UPDATED_BY", ccVO.getUpdatedBy());
    dataRequest.addInputParam("IN_GIFT_CARD_PIN", ccVO.getGiftCardPin());
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    outputs = (Map) dataAccessUtil.execute(dataRequest);
    String status = (String) outputs.get("OUT_STATUS");
    if(status != null && status.equalsIgnoreCase("N"))
    {
      String message = (String) outputs.get("OUT_MESSAGE");
      throw new Exception(message);
    }
    return (BigDecimal) outputs.get("OUT_CC_ID");
  }
}
