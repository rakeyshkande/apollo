package com.ftd.customerordermanagement.dao;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.vo.AcctTransVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;

import java.util.Map;

/**
 * AcctTransDAO
 *
 * This is the DAO that handles loading/updating Accounting Transaction info.
 *
 */


public class AcctTransDAO
{
    private Connection connection;
    private static Logger logger  = new Logger("com.ftd.customerordermanagement.dao.AcctTransDAO");
    private static final String NAME_DELIMITER = ",";

  /*
   * Constructor
   * @param connection Connection
   */
  public AcctTransDAO(Connection connection)
  {
    this.connection = connection;

  }

  /**
   * Insert Accounting Transactions
   *
   * Updates or inserts Accounting Transaction information for a payment.
   * @param AcctTransVO
   * @returns null
   * @throws java.lang.Exception
   */
  public void insertAcctTransMO(AcctTransVO atVO)
    throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(this.connection);

    dataRequest.setStatementID("INSERT_ACCOUNTING_TRANS_UPD");
    dataRequest.addInputParam("IN_TRANSACTION_TYPE", atVO.getTranType());
    dataRequest.addInputParam("IN_TRANSACTION_DATE", atVO.getTranDate());
    dataRequest.addInputParam("IN_ORDER_DETAIL_ID", atVO.getOrderDetailId());
    dataRequest.addInputParam("IN_DELIVERY_DATE", atVO.getDeliveryDate());
    dataRequest.addInputParam("IN_PRODUCT_ID", atVO.getProductId());
    dataRequest.addInputParam("IN_SOURCE_CODE", atVO.getSourceCode());
    dataRequest.addInputParam("IN_SHIP_METHOD", atVO.getShipMethod());
    dataRequest.addInputParam("IN_PRODUCT_AMOUNT", new Double(atVO.getProductAmount()));
    dataRequest.addInputParam("IN_ADD_ON_AMOUNT", new Double(atVO.getAddOnAmount()));
    dataRequest.addInputParam("IN_SHIPPING_FEE", new Double(atVO.getShippingFee()));
    dataRequest.addInputParam("IN_SERVICE_FEE", new Double(atVO.getServiceFee()));
    dataRequest.addInputParam("IN_DISCOUNT_AMOUNT", new Double(atVO.getDiscountAmount()));
    dataRequest.addInputParam("IN_SHIPPING_TAX", new Double(atVO.getShippingTax()));
    dataRequest.addInputParam("IN_SERVICE_FEE_TAX", new Double(atVO.getServiceFeeTax()));
    dataRequest.addInputParam("IN_TAX", new Double(atVO.getTax()));
    dataRequest.addInputParam("IN_PAYMENT_TYPE", atVO.getPaymentType());
    dataRequest.addInputParam("IN_REFUND_DISP_CODE", atVO.getRefundDispCode());
    dataRequest.addInputParam("IN_COMMISSION_AMOUNT", new Double(atVO.getCommissionAmount()));
    dataRequest.addInputParam("IN_WHOLESALE_AMOUNT", new Double(atVO.getWholesaleAmount()));
    dataRequest.addInputParam("IN_ADMIN_FEE", new Double(atVO.getAdminFee()));
    dataRequest.addInputParam("IN_REFUND_ID", atVO.getRefundId());
    dataRequest.addInputParam("IN_ORDER_BILL_ID", atVO.getOrderBillId());
    dataRequest.addInputParam("IN_NEW_ORDER_SEQ", atVO.getNewOrderSeq());

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    Map outputs = (Map) dataAccessUtil.execute(dataRequest);
    String status = (String) outputs.get(COMConstants.STATUS_PARAM);
    if(status != null && status.equals("N"))
    {
        String message = (String) outputs.get(COMConstants.MESSAGE_PARAM);
        throw new Exception(message);
    }
    logger.debug("Update Accounting Transaction Successful");
  }
}
