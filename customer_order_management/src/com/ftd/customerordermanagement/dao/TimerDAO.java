package com.ftd.customerordermanagement.dao;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;

import java.util.HashMap;


public class TimerDAO 
{
  private Connection connection;
  private static Logger logger  = new Logger("com.ftd.customerordermanagement.dao.CallTimerDAO");
    
  public TimerDAO(Connection connection)
  {
    this.connection = connection;
  }
  

  /**
   * 
   * @param 
   * @return 
   * @throws java.lang.Exception
   */
  public HashMap insertEntityHistory(String entityType, String entityId, String commentOrigin,
                            String csrId, String callLogId) throws Exception
  {
    HashMap searchResults = new HashMap();
    // get the list of order detail ids for this order guid

    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(connection);
    dataRequest.setStatementID("INSERT_UPDATE_ENTITY_HISTORY");
    dataRequest.addInputParam("IN_ENTITY_HISTORY_ID",   null);
    dataRequest.addInputParam("IN_ENTITY_TYPE",         entityType);
    dataRequest.addInputParam("IN_ENTITY_ID",           entityId);
    dataRequest.addInputParam("IN_COMMENT_ORIGIN",      commentOrigin);
    dataRequest.addInputParam("IN_CSR_ID",              csrId);
    dataRequest.addInputParam("IN_CALL_LOG_ID",         callLogId);
        
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
    searchResults = (HashMap) dataAccessUtil.execute(dataRequest);
    return searchResults;    
  }



  /**
   * 
   * @param 
   * @return 
   * @throws java.lang.Exception
   */
  public void updateEntityHistory(String entityHistoryId) throws Exception
  {
    HashMap searchResults = new HashMap();

    // get the list of order detail ids for this order guid
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(connection);
    dataRequest.setStatementID("INSERT_UPDATE_ENTITY_HISTORY");
    dataRequest.addInputParam("IN_ENTITY_HISTORY_ID",   entityHistoryId);
    dataRequest.addInputParam("IN_ENTITY_TYPE",         null);
    dataRequest.addInputParam("IN_ENTITY_ID",           null);
    dataRequest.addInputParam("IN_COMMENT_ORIGIN",      null);
    dataRequest.addInputParam("IN_CSR_ID",              null);
        
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

    searchResults = (HashMap) dataAccessUtil.execute(dataRequest);
  }

}