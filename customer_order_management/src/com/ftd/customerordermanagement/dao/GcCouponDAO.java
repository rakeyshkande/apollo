package com.ftd.customerordermanagement.dao;

import com.ftd.customerordermanagement.vo.BillingVO;
import com.ftd.customerordermanagement.vo.GcCouponVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;

import java.util.Map;



/**
 * GcCouponDAO
 * This is the DAO that handles loading and updating of Gift Certificate information
 * 
 * @author Luke W. Pennings
 * @version $Id: GcCouponDAO.java
 */

public class GcCouponDAO 
{
    private Connection connection;
    private static Logger logger  = new Logger("com.ftd.customerordermanagement.dao.GcCouponDAO");
    
  /**
   * Constructor
   * @param connection Connection
   */
  public GcCouponDAO(Connection connection)
  {
    this.connection = connection;
  }

  /**
   * method for redeeming a gift certificate.
   * @return String status 
   * @throws java.lang.Exception
   */

  public String redeemGcCoupon(GcCouponVO gcVO, String orderDetailId) 
        throws Exception
  {
    Map outputs = null; 
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(this.connection);
    dataRequest.setStatementID("REDEEM_GC_COUPON");
    dataRequest.addInputParam("IN_GC_COUPON_NUMBER", gcVO.getGcCouponNumber());
    dataRequest.addInputParam("IN_ORDER_DETAIL_ID", orderDetailId);
    dataRequest.addInputParam("IN_CSR_ID", gcVO.getUpdatedBy());
    dataRequest.addInputParam("IN_REDEMPTION_AMOUNT", gcVO.getAmount());
    
    logger.debug("gcVO.getGcCouponNumber = " + gcVO.getGcCouponNumber());
    logger.debug("orderDetailId = " + orderDetailId);
    logger.debug("gcVO.getUpdatedBy = " + gcVO.getUpdatedBy());
    logger.debug("gcVO.getAmount = " + gcVO.getAmount());




    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    outputs = (Map) dataAccessUtil.execute(dataRequest);
    String status = (String) outputs.get("OUT_STATUS");
    if(status != null && status.equalsIgnoreCase("N"))
    {
      String message = (String) outputs.get("OUT_MESSAGE");
      throw new Exception(message);
    }
    return (String) outputs.get("OUT_REDEEM_STATUS");
  }

  /**
   * method for validating a gift certificate.
   * @return String status 
   * @throws java.lang.Exception
   */

  public GcCouponVO getGcCouponByNumber(String gcCouponNumber)
        throws Exception
    {
       boolean isEmpty = true;
       GcCouponVO gccVO = new GcCouponVO();
    try
    {

       DataRequest dataRequest = new DataRequest();
       dataRequest.setConnection(this.connection);
       dataRequest.setStatementID("GET_GCCREQ_BY_REQNUM_OR_GCCNUM");
       dataRequest.addInputParam("IN_REQUEST_NUMBER", null);
       dataRequest.addInputParam("IN_GC_COUPON_NUMBER", gcCouponNumber);

       DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
       CachedResultSet outputs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
      
       /* populate object */
       while (outputs.next())
       {
         isEmpty = false;
         gccVO.setGcCouponNumber(outputs.getString("GC_COUPON_NUMBER"));
         gccVO.setAmount(outputs.getString("ISSUE_AMOUNT"));
         gccVO.setExpDate(outputs.getString("EXPIRATION_DATE"));
         gccVO.setStatus(outputs.getString("GC_COUPON_STATUS"));
         gccVO.setUpdatedBy(outputs.getString("REQUESTED_BY"));
       }
    }
    catch (Exception e) 
    {            
      logger.error(e);
      throw e;
    } 
    if (isEmpty)
      return null;
    else 
      return gccVO;
  }  
}