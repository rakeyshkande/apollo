package com.ftd.customerordermanagement.dao;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.vo.OrderBillVO;
import com.ftd.customerordermanagement.vo.PaymentVO;
import com.ftd.customerordermanagement.vo.RefundVO;
import com.ftd.op.common.constants.OrderProcessingConstants;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.GiftCodeUtil;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.RedemptionDetails;
import com.ftd.osp.utilities.vo.ReinstateDetail;
import com.ftd.osp.utilities.vo.ReinstateInfoResponse;
import com.ftd.osp.utilities.vo.RetrieveGiftCodeResponse;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;



/**
 * RefundDAO
 *
 *
 * @author Munter
 * @version $Id: OrderDAO.java
 */

public class RefundDAO
{
    private Connection connection;
    private static Logger logger  = new Logger("com.ftd.customerordermanagement.dao.RefundDAO");

    public static final String REFUND_PAYMENT_INDICATOR     = "R";
    public static final String PAYMENT_LIST                 = "PAYMENT_LIST";
    public static final String REFUND_LIST                  = "REFUND_LIST";
    public static final String ORDER_DISPOSITION            = "ORDER_DISPOSITION";
    public static final String ORDER_DELIVERY_DATE          = "ORDER_DELIVERY_DATE";
    public static final String ORDER_LIVE_FLAG              = "ORDER_LIVE_FLAG";
    public static final String ORDER_CANCEL_FLAG            = "ORDER_CANCEL_FLAG";
    public static final String ORDER_VENDOR_FLAG            = "ORDER_VENDOR_FLAG";
    public static final String ORDER_NUMBER                 = "ORDER_NUMBER";
    public static final String CUSTOMER_ID                  = "CUSTOMER_ID";
    public static final String FRESH_CUT_FLAG               = "FRESH_CUT_FLAG";
    public static final String PRODUCT_TYPE                 = "PRODUCT_TYPE";
    public static final String SHIP_METHOD                  = "SHIP_METHOD";
    private static final String complaintCommTypeIdNotAComplaint = "NC";

  /**
   * Constructor
   * @param connection Connection
   */
    public RefundDAO(Connection connection)
    {
        this.connection = connection;
    }

    public Document loadRefundDispositionsXML(String displayType) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);

        dataRequest.setStatementID("REFUND_VIEW_REFUND_DISPOSITIONS");
        dataRequest.addInputParam("IN_REFUND_DISPLAY_TYPE", displayType);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

        return (Document) dataAccessUtil.execute(dataRequest);
    }

    public Document loadOrderFloristsXML(String orderDetailId) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);

        dataRequest.setStatementID("REFUND_VIEW_ORDER_FLORISTS");
        dataRequest.addInputParam("IN_ORDER_DETAIL_ID", orderDetailId);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

        return (Document) dataAccessUtil.execute(dataRequest);
    }

    public Document loadOrderFulfillmentXML(String orderDetailId) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);

        dataRequest.setStatementID("REFUND_VIEW_ORDER_FULFILLMENT");
        dataRequest.addInputParam("IN_ORDER_DETAIL_ID", orderDetailId);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

        return (Document) dataAccessUtil.execute(dataRequest);
    }

    public Document loadComplaintCommTypeXML() throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);

        dataRequest.setStatementID("REFUND_VIEW_COMPLAINT_COMM_TYPE");

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

        return (Document) dataAccessUtil.execute(dataRequest);
    }


    public HashMap loadRefundReviewXML(String csrId, int position, int maxRecords,String sortColumn,String sortStatus,String userIdFilter,String groupIdFilter,String Code) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);

        dataRequest.setStatementID("REFUND_VIEW_DAILY_REFUNDS");
        dataRequest.addInputParam("IN_CSR_ID", csrId);
        dataRequest.addInputParam("IN_START_POSITION", new Long(position));
        dataRequest.addInputParam("IN_MAX_NUMBER_RETURNED", new Long(maxRecords));
        dataRequest.addInputParam("IN_SORT_COLUMN_NAME", sortColumn);
        dataRequest.addInputParam("IN_SORT_COLUMN_STATUS", sortStatus);
        dataRequest.addInputParam("IN_USER_ID_FILTER", userIdFilter);
        dataRequest.addInputParam("IN_GROUP_ID_FILTER", groupIdFilter);
        dataRequest.addInputParam("IN_CODES_FILTER", Code);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

        return (HashMap) dataAccessUtil.execute(dataRequest);
    }

    public HashMap loadPaymentRefund(String orderDetailId) throws Exception
    {
        HashMap billPaymentMap = new HashMap();

        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);

        dataRequest.setStatementID("REFUND_VIEW_PAYMENT_REFUND");
        dataRequest.addInputParam("IN_ORDER_DETAIL_ID", orderDetailId);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        HashMap paymentMap = (HashMap) dataAccessUtil.execute(dataRequest);

        HashMap paymentRefundMap = new HashMap();

        // LOAD BILLS
        CachedResultSet billResultSet = null;
        HashMap orderBillMap = new HashMap();

        try
        {
            billResultSet = (CachedResultSet) paymentMap.get("OUT_BILL_CUR");

            OrderBillVO orderBill = null;
            while(billResultSet != null && billResultSet.next())
            {
                orderBill = new OrderBillVO();

                orderBill.setOrderBillId(billResultSet.getString("order_bill_id"));
                orderBill.setProductAmount(billResultSet.getObject("discount_product_price") != null ? new Double(billResultSet.getString("discount_product_price")).doubleValue() : 0);
                orderBill.setAddOnAmount(billResultSet.getObject("add_on_amount") != null ? new Double(billResultSet.getString("add_on_amount")).doubleValue() : 0);
                orderBill.setFeeAmount(billResultSet.getObject("serv_ship_fee") != null ? new Double(billResultSet.getString("serv_ship_fee")).doubleValue() : 0);
                orderBill.setTax(billResultSet.getObject("tax") != null ? new Double(billResultSet.getString("tax")).doubleValue() : 0);
                orderBill.setCommissionAmount(billResultSet.getObject("commission_amount") != null ? new Double(billResultSet.getString("commission_amount")).doubleValue() : 0);
                orderBill.setGrossProductAmount(billResultSet.getObject("product_amount") != null ? new Double(billResultSet.getString("product_amount")).doubleValue() : 0);
                orderBill.setDiscountAmount(billResultSet.getObject("discount_amount") != null ? new Double(billResultSet.getString("discount_amount")).doubleValue() : 0);
                orderBill.setAddOnDiscountAmount(billResultSet.getObject("add_on_discount_amount") != null ? new Double(billResultSet.getString("add_on_discount_amount")).doubleValue() : 0);
                orderBill.setWholesaleAmount(billResultSet.getObject("wholesale_amount") != null ? new Double(billResultSet.getString("wholesale_amount")).doubleValue() : 0);
                orderBill.setMerchandiseTax(billResultSet.getObject("product_tax") != null ? new Double(billResultSet.getString("product_tax")).doubleValue() : 0);
                orderBill.setShippingTax(billResultSet.getObject("shipping_tax") != null ? new Double(billResultSet.getString("shipping_tax")).doubleValue() : 0);
                orderBill.setServiceFeeTax(billResultSet.getObject("service_fee_tax") != null ? new Double(billResultSet.getString("service_fee_tax")).doubleValue() : 0);
                orderBill.setWholesaleServiceFee(billResultSet.getObject("wholesale_service_fee") != null ? new Double(billResultSet.getString("wholesale_service_fee")).doubleValue() : 0);
                orderBill.setServiceFee(billResultSet.getObject("service_fee") != null ? new Double(billResultSet.getString("service_fee")).doubleValue() : 0);
                orderBill.setShippingFee(billResultSet.getObject("shipping_fee") != null ? new Double(billResultSet.getString("shipping_fee")).doubleValue() : 0);
                orderBill.setMilesPointsAmount(billResultSet.getObject("miles_points_amt") != null ? new Integer(billResultSet.getString("miles_points_amt")).intValue() : 0);
                //do not delete the milespointsoriginal line it is not a duplicate of milespointsamount they are different
                orderBill.setMilesPointsOriginal(billResultSet.getObject("miles_points_amt") != null ? new Integer(billResultSet.getString("miles_points_amt")).intValue() : 0);
                orderBill.setSaturdayUpcharge(billResultSet.getObject("vend_sat_upcharge") != null ? new Double(billResultSet.getString("vend_sat_upcharge")).doubleValue() : 0);
                orderBill.setAlsHawaiiUpcharge(billResultSet.getObject("ak_hi_special_charge") != null ? new Double(billResultSet.getString("ak_hi_special_charge")).doubleValue() : 0);
                orderBill.setFuelSurchargeAmount(billResultSet.getObject("fuel_surcharge_amt") != null ? new Double(billResultSet.getString("fuel_surcharge_amt")).doubleValue() : 0);
                orderBill.setFuelSurchargeDescription(billResultSet.getObject("surcharge_description") != null ? billResultSet.getString("surcharge_description") : "");
                orderBill.setApplySurchargeCode(billResultSet.getObject("apply_surcharge_code") != null ? billResultSet.getString("apply_surcharge_code") : "");
                orderBill.setSameDayUpcharge(billResultSet.getObject("same_day_upcharge") != null ? new Double(billResultSet.getString("same_day_upcharge")).doubleValue() : 0);
                orderBill.setMorningDeliveryFee(billResultSet.getObject("morning_delivery_fee") != null ? new Double(billResultSet.getString("morning_delivery_fee")).doubleValue() : 0);
                //DI-27: Refund - Show new fees broken out in Mouseovers 
                orderBill.setSundayUpcharge(billResultSet.getObject("vend_sun_upcharge") != null ? new Double(billResultSet.getString("vend_sun_upcharge")).doubleValue() : 0);
                orderBill.setMondayUpcharge(billResultSet.getObject("vend_mon_upcharge") != null ? new Double(billResultSet.getString("vend_mon_upcharge")).doubleValue() : 0);
                orderBill.setLateCutoffFee(billResultSet.getObject("late_cutoff_fee") != null ? new Double(billResultSet.getString("late_cutoff_fee")).doubleValue() : 0);
                orderBill.setInternationalFee(billResultSet.getObject("international_fee") != null ? new Double(billResultSet.getString("international_fee")).doubleValue() : 0);
                orderBillMap.put(orderBill.getOrderBillId(), orderBill);
            }
        }
        finally
        {
        }

        // LOAD PAYMENTS
        CachedResultSet paymentResultSet = null;

        try
        {
            paymentResultSet = (CachedResultSet) paymentMap.get("OUT_PAYMENT_CUR");

            PaymentVO payment = null;
            OrderBillVO billClone = null;
            // ArrayList paymentList = new ArrayList();
            while(paymentResultSet != null && paymentResultSet.next())
            {
              payment = new PaymentVO();
              payment.setCardId(paymentResultSet.getObject("card_id") != null? paymentResultSet.getString("card_id") : "0");
              if(paymentResultSet.getString("payment_type").equalsIgnoreCase("GC")){
            	RetrieveGiftCodeResponse retrieveGiftCodeResponse = null;
                payment.setCardId(paymentResultSet.getObject("card_id") != null? paymentResultSet.getString("card_id") : "0");
                try { 
                	retrieveGiftCodeResponse = GiftCodeUtil.getGiftCodeById(payment.getCardId());
                } catch (IllegalStateException ex) {
                	
                }
                boolean setStatus = false;
          		if(retrieveGiftCodeResponse != null){
          			  payment.setGcCouponStatus(retrieveGiftCodeResponse.getStatus() != null? retrieveGiftCodeResponse.getStatus() : "");
                      List<RedemptionDetails> lRedDetails = new ArrayList(); 
                      RedemptionDetails redemptionDetails = new RedemptionDetails();
                      List<ReinstateDetail> lReinstatedDetails = new ArrayList(); 
                      ReinstateDetail reinstateDetail = new ReinstateDetail();
                      lRedDetails = retrieveGiftCodeResponse.getRedemptionDetails();
                      lReinstatedDetails = retrieveGiftCodeResponse.getReinstateDetails();
                      BigDecimal redemptionAmount = new BigDecimal(0);
                      BigDecimal totalRedemptionAmount = new BigDecimal(0);
                      BigDecimal totalAmountAlreadyRedeemed = new BigDecimal(0);
                      BigDecimal totalAmountAlreadyReinstated = new BigDecimal(0);
                      BigDecimal issueAmount = retrieveGiftCodeResponse.getIssueAmount();
                      BigDecimal remainingAmount = retrieveGiftCodeResponse.getRemainingAmount();
                      String redeemedRefNumber = null;
                      String gcAcrossOrders = "N";
            		  boolean gcUsedOnOrder = false;
          	   		if (lRedDetails != null && lRedDetails.size() > 0 ) {
              			if (lRedDetails.size() > 1){
          	   				gcAcrossOrders = "Y";
          	   			}
              			for(RedemptionDetails redemptDetails:lRedDetails){	
          	   				//check to see if the gc has been redeemed against this order detail id
          	   				//if so, retrieve the redemption amount
          	   				logger.info("redeeemed ref #: " + redemptDetails.getRedeemedRefNo());
          	   				if(redemptDetails.getRedeemedRefNo().equalsIgnoreCase(orderDetailId)){
          	   					logger.info("redeemed order detail id equals redeemed order detail id");
          	   					gcUsedOnOrder = true;
          	   					redemptionAmount = redemptDetails.getRedemptionAmount();
          	   					totalAmountAlreadyRedeemed = totalAmountAlreadyRedeemed.add(redemptionAmount);
          	   				}
          	   			}
          	   		}
          	   		else if (lReinstatedDetails != null){
          	   			if (lReinstatedDetails.size() > 1){
          	   				gcAcrossOrders = "Y";
          	   			}
	          	   		for(ReinstateDetail rDetail:lReinstatedDetails){	
	      	   				//check to see if the gc has been reinstated against this order detail id
	      	   				//if so, retrieve the amounts and set to the redemption amount
	              			//the screen needs it so it displays the gc correctly
	      	   				if(rDetail.getReinstateRefNo().equalsIgnoreCase(orderDetailId)){
	      	   					redemptionAmount = rDetail.getReinstateAmount();
	      	   					gcUsedOnOrder = true;
          	   					totalAmountAlreadyReinstated = totalAmountAlreadyReinstated.add(redemptionAmount);
          	   				    totalAmountAlreadyRedeemed = totalAmountAlreadyReinstated;
          	   				}
	      	   			}
          	   		}
          	   		
          	   		//add a check to see if the order detail id is still the order the gc is currently used on.
          			//Otherwise check to see if it was reinstated and used on another order.  Pull details from gift code history table to
          			//populate gc info if it was.
          	   		if(!gcUsedOnOrder){
	          	   		ReinstateInfoResponse reinstateInfoResposne = null;
	      	   			try{
	      	   				reinstateInfoResposne = GiftCodeUtil.getGiftCodeReinstateHistoryById(payment.getCardId(), orderDetailId);
	      	   			}
	      	   			catch(Exception e)
	      	   			{
	      	   				
	      	   			}
          	   			if(reinstateInfoResposne != null){
          	   				logger.info("gc was reinstated on different order number");
          	   				redemptionAmount = reinstateInfoResposne.getReinstateAmount();
      	   					totalAmountAlreadyReinstated = totalAmountAlreadyReinstated.add(redemptionAmount);
      	   				    totalAmountAlreadyRedeemed = totalAmountAlreadyReinstated;
      	   				    setStatus = true;
          	   				
          	   			}
          	   		}
          	   		totalRedemptionAmount = totalRedemptionAmount.add(totalAmountAlreadyRedeemed);
          	   	    payment.setGcCouponMultiOrderFlag(gcAcrossOrders != null? gcAcrossOrders : "");
                    payment.setCreditAmount(new Double(totalRedemptionAmount.toString() != null? totalRedemptionAmount.toString() : "0").doubleValue());
                    payment.setGcCouponAmount(new Double(issueAmount.toString() != null? issueAmount.toString() : "0").doubleValue());
                    if(setStatus){
                    	payment.setGcCouponStatus("Reinstate");
                    }
                    else{
                    	payment.setGcCouponStatus(retrieveGiftCodeResponse.getStatus() != null? retrieveGiftCodeResponse.getStatus() : "");
                    }
                    logger.info("gc status: " + payment.getGcCouponStatus());
                    payment.setCardNumber(paymentResultSet.getObject("card_number") != null? paymentResultSet.getString("card_number") : "N/A");
                    payment.setPaymentMethodDescription((issueAmount.toString() != null? "$" +issueAmount.toString() + " ":"") + (paymentResultSet.getString("payment_type_desc")));
                    
                    String wasGcReinstated = "N";
                    if(payment.getGcCouponStatus().equalsIgnoreCase("Refund")){
                    	wasGcReinstated = "Y";                    			
                    }
                    payment.setGcReinstatedOnOriginalOrder(wasGcReinstated);
          		} 
              }
              else{
                payment.setCreditAmount(new Double(paymentResultSet.getObject("credit_amount") != null? paymentResultSet.getString("credit_amount") : "0").doubleValue());
                payment.setGcCouponAmount(new Double(paymentResultSet.getObject("original_gc_coupon_amount") != null? paymentResultSet.getString("original_gc_coupon_amount") : "0").doubleValue());
                payment.setGcCouponStatus(paymentResultSet.getObject("gc_coupon_status") != null? paymentResultSet.getString("gc_coupon_status") : "");
                payment.setGcCouponMultiOrderFlag(paymentResultSet.getObject("gc_across_orders") != null? paymentResultSet.getString("gc_across_orders") : "");
                payment.setPaymentMethodDescription((paymentResultSet.getObject("original_gc_coupon_amount") != null? "$" + paymentResultSet.getString("original_gc_coupon_amount") + " ":"") + (paymentResultSet.getString("payment_type_desc")));
                payment.setGcReinstatedOnOriginalOrder(paymentResultSet.getString("was_gc_reinstated"));
                payment.setCardId(paymentResultSet.getObject("card_id") != null? paymentResultSet.getString("card_id") : "0");
                payment.setCardNumber(paymentResultSet.getObject("card_number") != null? paymentResultSet.getString("card_number") : "N/A");
              }
              
                payment.setPaymentType(paymentResultSet.getString("payment_type_method"));
                payment.setPaymentMethodId(paymentResultSet.getString("payment_type"));
                                
                payment.setAuthCode(paymentResultSet.getObject("auth_number")!= null? paymentResultSet.getString("auth_number") : "");
                payment.setAuthorizationTransactionId(paymentResultSet.getObject("authorization_transaction_id")!= null? paymentResultSet.getString("authorization_transaction_id") : "");
                
                String route = paymentResultSet.getString("route");
                payment.setRoute(route);
                if(StringUtils.isNotBlank(route) && isNewRoute(route)) {
                	payment.setAuthorization(paymentResultSet.getObject("authorization_transaction_id")!= null? paymentResultSet.getString("authorization_transaction_id") : "");
                } else {
                	payment.setAuthorization(paymentResultSet.getObject("auth_number")!= null? paymentResultSet.getString("auth_number") : "");
                }
                
                payment.setTransactionId(payment.getAuthorization());
                payment.setGiftCardPin(paymentResultSet.getString("gift_card_pin"));
                payment.setPaymentId(paymentResultSet.getObject("payment_id") != null ? ((BigDecimal)paymentResultSet.getObject("payment_id")).longValue() : null);
                payment.setPaymentInd(paymentResultSet.getString("payment_indicator"));
                payment.setBillStatus(paymentResultSet.getString("bill_status"));
                
                payment.setSettlementTransactionId(paymentResultSet.getString("settlement_transaction_id"));
                payment.setRefundTransactionId(paymentResultSet.getString("refund_transaction_id"));
                payment.setRequestToken(paymentResultSet.getString("request_token"));
                payment.setMerchantRefId(paymentResultSet.getString("MERCHANT_REF_ID"));

                billClone = new OrderBillVO();
                billClone.setOrderBillId(((OrderBillVO)orderBillMap.get(paymentResultSet.getObject(1).toString())).getOrderBillId());
                billClone.setProductAmount(((OrderBillVO)orderBillMap.get(paymentResultSet.getObject(1).toString())).getProductAmount());
                billClone.setAddOnAmount(((OrderBillVO)orderBillMap.get(paymentResultSet.getObject(1).toString())).getAddOnAmount());
                billClone.setFeeAmount(((OrderBillVO)orderBillMap.get(paymentResultSet.getObject(1).toString())).getFeeAmount());
                billClone.setTax(((OrderBillVO)orderBillMap.get(paymentResultSet.getObject(1).toString())).getTax());
                billClone.setCommissionAmount(((OrderBillVO)orderBillMap.get(paymentResultSet.getObject(1).toString())).getCommissionAmount());
                billClone.setGrossProductAmount(((OrderBillVO)orderBillMap.get(paymentResultSet.getObject(1).toString())).getGrossProductAmount());
                billClone.setDiscountAmount(((OrderBillVO)orderBillMap.get(paymentResultSet.getObject(1).toString())).getDiscountAmount());
                billClone.setAddOnDiscountAmount(((OrderBillVO)orderBillMap.get(paymentResultSet.getObject(1).toString())).getAddOnDiscountAmount());
                billClone.setWholesaleAmount(((OrderBillVO)orderBillMap.get(paymentResultSet.getObject(1).toString())).getWholesaleAmount());
                billClone.setMerchandiseTax(((OrderBillVO)orderBillMap.get(paymentResultSet.getObject(1).toString())).getMerchandiseTax());
                billClone.setShippingTax(((OrderBillVO)orderBillMap.get(paymentResultSet.getObject(1).toString())).getShippingTax());
                billClone.setServiceFeeTax(((OrderBillVO)orderBillMap.get(paymentResultSet.getObject(1).toString())).getServiceFeeTax());
                billClone.setWholesaleServiceFee(((OrderBillVO)orderBillMap.get(paymentResultSet.getObject(1).toString())).getWholesaleServiceFee());
                billClone.setServiceFee(((OrderBillVO)orderBillMap.get(paymentResultSet.getObject(1).toString())).getServiceFee());
                billClone.setShippingFee(((OrderBillVO)orderBillMap.get(paymentResultSet.getObject(1).toString())).getShippingFee());
                billClone.setMilesPointsAmount(((OrderBillVO)orderBillMap.get(paymentResultSet.getObject(1).toString())).getMilesPointsAmount());
                billClone.setMilesPointsOriginal(((OrderBillVO)orderBillMap.get(paymentResultSet.getObject(1).toString())).getMilesPointsOriginal());
                billClone.setSaturdayUpcharge(((OrderBillVO)orderBillMap.get(paymentResultSet.getObject(1).toString())).getSaturdayUpcharge());
                billClone.setAlsHawaiiUpcharge(((OrderBillVO)orderBillMap.get(paymentResultSet.getObject(1).toString())).getAlsHawaiiUpcharge());
                billClone.setFuelSurchargeAmount(((OrderBillVO)orderBillMap.get(paymentResultSet.getObject(1).toString())).getFuelSurchargeAmount());
                billClone.setFuelSurchargeDescription(((OrderBillVO)orderBillMap.get(paymentResultSet.getObject(1).toString())).getFuelSurchargeDescription());
                billClone.setApplySurchargeCode(((OrderBillVO)orderBillMap.get(paymentResultSet.getObject(1).toString())).getApplySurchargeCode());
                billClone.setSameDayUpcharge(((OrderBillVO)orderBillMap.get(paymentResultSet.getObject(1).toString())).getSameDayUpcharge());
                billClone.setMorningDeliveryFee(((OrderBillVO)orderBillMap.get(paymentResultSet.getObject(1).toString())).getMorningDeliveryFee());
                //DI-27: Refund - Show new fees broken out in Mouseovers
                billClone.setSundayUpcharge(((OrderBillVO)orderBillMap.get(paymentResultSet.getObject(1).toString())).getSundayUpcharge());
                billClone.setMondayUpcharge(((OrderBillVO)orderBillMap.get(paymentResultSet.getObject(1).toString())).getMondayUpcharge());
                billClone.setLateCutoffFee(((OrderBillVO)orderBillMap.get(paymentResultSet.getObject(1).toString())).getLateCutoffFee());
                billClone.setInternationalFee(((OrderBillVO)orderBillMap.get(paymentResultSet.getObject(1).toString())).getInternationalFee());
                payment.setOrderBill(billClone);

                // paymentList.add(payment);

                if(billPaymentMap.containsKey(payment.getOrderBill().getOrderBillId()))
                {
                    ArrayList paymentList = (ArrayList) billPaymentMap.get(payment.getOrderBill().getOrderBillId());
                    paymentList.add(payment);
                }
                else
                {
                    ArrayList paymentList = new ArrayList();
                    paymentList.add(payment);
                    billPaymentMap.put(payment.getOrderBill().getOrderBillId(), paymentList);
                }
            }

            paymentRefundMap.put(this.PAYMENT_LIST, billPaymentMap);
        }
        finally
        {
        }

        // LOAD REFUNDS
        CachedResultSet refundResultSet = null;

        try
        {
            refundResultSet = (CachedResultSet) paymentMap.get("OUT_REFUND_CUR");

            int i = 1;
            RefundVO refund = null;
            OrderBillVO refundBill = null;
            ArrayList refundList = new ArrayList();
            while(refundResultSet != null && refundResultSet.next())
            {
            	refund = new RefundVO();
                refund.setRefundSeqId(i++);
                refund.setRefundId(refundResultSet.getString("refund_id"));
                refund.setRefundDispCode(refundResultSet.getObject("refund_disp_code") != null ? refundResultSet.getString("refund_disp_code") : null);
                refund.setCardId(refundResultSet.getObject("card_id") != null ? refundResultSet.getString("card_id") : null);
                refund.setCardNumber(refundResultSet.getObject("card_number") != null ? refundResultSet.getString("card_number") : "N/A");
                refund.setPaymentMethodId(refundResultSet.getObject("payment_type") != null ? refundResultSet.getString("payment_type") : null);
                refund.setPaymentType(refundResultSet.getObject("payment_type_method") != null ? refundResultSet.getString("payment_type_method") : null);
                refund.setCreatedBy(refundResultSet.getString("created_by"));
                refund.setAccountingDate(refundResultSet.getObject("accounting_date") != null ? (String)refundResultSet.getString("accounting_date") : "N/A");
                refund.setResponsibleParty(refundResultSet.getObject("responsible_party") != null ? refundResultSet.getString("responsible_party") : null);
                refund.setRefundStatus(refundResultSet.getObject("refund_status") != null ? refundResultSet.getString("refund_status") : null);
                refund.setComplaintCommOriginTypeId(refundResultSet.getObject("origin_complaint_comm_type_id") != null ? refundResultSet.getString("origin_complaint_comm_type_id") : null);
                refund.setComplaintCommNotificationTypeId(refundResultSet.getObject("notif_complaint_comm_type_id") != null ? refundResultSet.getString("notif_complaint_comm_type_id") : null);
                refund.setPaymentId(refundResultSet.getObject("payment_id") != null ?((BigDecimal)refundResultSet.getObject("payment_id")).longValue() : null);
                refund.setAuthCode(refundResultSet.getObject("auth_number") != null ? refundResultSet.getString("auth_number") : null);
                refund.setAuthorizationTransactionId(refundResultSet.getObject("authorization_transaction_id") != null ? refundResultSet.getString("authorization_transaction_id") : null);
                
                String route = refundResultSet.getString("route");
                refund.setRoute(route);
                if(StringUtils.isNotBlank(route) && isNewRoute(route)) {
                	refund.setAuthorization(refundResultSet.getObject("authorization_transaction_id")!= null? refundResultSet.getString("authorization_transaction_id") : "");
                } else {
                	refund.setAuthorization(refundResultSet.getObject("auth_number")!= null? refundResultSet.getString("auth_number") : "");
                }
                
                refund.setDebitAmount(new Double(refundResultSet.getObject("debit_amount") != null? refundResultSet.getString("debit_amount") : "0").doubleValue());
                
                refundBill = new OrderBillVO();
                refundBill.setProductAmount(new Double(refundResultSet.getString("refund_product_amount")).doubleValue());
                refundBill.setAddOnAmount(new Double(refundResultSet.getString("refund_addon_amount")).doubleValue());
                refundBill.setFeeAmount(new Double(refundResultSet.getString("refund_serv_ship_fee")).doubleValue());
                refundBill.setTax(refundResultSet.getObject("refund_tax") != null ? new Double(refundResultSet.getString("refund_tax")).doubleValue() : 0);
                refundBill.setAdminFee(refundResultSet.getObject("refund_admin_fee") != null ? new Double(refundResultSet.getString("refund_admin_fee")).doubleValue() : 0);
                refundBill.setCommissionAmount(refundResultSet.getObject("refund_commission_amount") != null ? new Double(refundResultSet.getString("refund_commission_amount")).doubleValue() : 0);
                refundBill.setDiscountAmount(refundResultSet.getObject("refund_discount_amount") != null ? new Double(refundResultSet.getString("refund_discount_amount")).doubleValue() : 0);
                refundBill.setAddOnDiscountAmount(refundResultSet.getObject("refund_addon_discount_amt") != null ? new Double(refundResultSet.getString("refund_addon_discount_amt")).doubleValue() : 0);
                refundBill.setWholesaleAmount(refundResultSet.getObject("refund_wholesale_amount") != null ? new Double(refundResultSet.getString("refund_wholesale_amount")).doubleValue() : 0);
                refundBill.setMerchandiseTax(refundResultSet.getObject("refund_product_tax") != null ? new Double(refundResultSet.getString("refund_product_tax")).doubleValue() : 0);
                refundBill.setShippingTax(refundResultSet.getObject("refund_shipping_tax") != null ? new Double(refundResultSet.getString("refund_shipping_tax")).doubleValue() : 0);
                refundBill.setServiceFeeTax(refundResultSet.getObject("refund_service_fee_tax") != null ? new Double(refundResultSet.getString("refund_service_fee_tax")).doubleValue() : 0);
                refundBill.setWholesaleServiceFee(refundResultSet.getObject("refund_wholesale_service_fee") != null ? new Double(refundResultSet.getString("refund_shipping_tax")).doubleValue() : 0);
                refundBill.setServiceFeeTax(refundResultSet.getObject("refund_service_fee_tax") != null ? new Double(refundResultSet.getString("refund_service_fee_tax")).doubleValue() : 0);
                refundBill.setWholesaleServiceFee(refundResultSet.getObject("refund_wholesale_service_fee") != null ? new Double(refundResultSet.getString("refund_shipping_tax")).doubleValue() : 0);
                refundBill.setMilesPointsAmount(refundResultSet.getObject("miles_points_debit_amt") != null ? new Integer(refundResultSet.getString("miles_points_debit_amt")).intValue() : 0);

                if(refundBill.getDiscountAmount() > 0)
                {
                    refundBill.setProductAmount(refundBill.getProductAmount() - refundBill.getDiscountAmount());
                }
//                if(refundBill.getAddOnDiscountAmount() > 0)
//                {                	
//                	refundBill.setAddOnAmount(refundBill.getAddOnAmount() - refundBill.getAddOnDiscountAmount());
//                }

                refund.setOrderBill(refundBill);

                refundList.add(refund);
            }

            paymentRefundMap.put(this.REFUND_LIST, refundList);
        }
        finally
        {
        }

        // ORDER INFO
        CachedResultSet orderInfoResultSet = null;

        try
        {
            orderInfoResultSet = (CachedResultSet) paymentMap.get("OUT_ORDER_INFO_CUR");

            while(orderInfoResultSet != null && orderInfoResultSet.next())
            {
                paymentRefundMap.put(this.ORDER_DISPOSITION,   orderInfoResultSet.getString("order_disp_code")       != null?(String)orderInfoResultSet.getString("order_disp_code"):null);
                paymentRefundMap.put(this.ORDER_DELIVERY_DATE, orderInfoResultSet.getString("delivery_date")         != null?(String)orderInfoResultSet.getString("delivery_date"):null);
                paymentRefundMap.put("recipient_state",        orderInfoResultSet.getString("recipient_state")       != null?(String)orderInfoResultSet.getString("recipient_state"):null);
                paymentRefundMap.put(this.ORDER_VENDOR_FLAG,   orderInfoResultSet.getString("vendor_flag")           != null?(String)orderInfoResultSet.getString("vendor_flag"):null);
                paymentRefundMap.put(this.ORDER_NUMBER,        orderInfoResultSet.getString("external_order_number") != null?(String)orderInfoResultSet.getString("external_order_number"):null);
                paymentRefundMap.put(this.CUSTOMER_ID,         orderInfoResultSet.getString("customer_id")           != null?(String)orderInfoResultSet.getString("customer_id"):null);
                paymentRefundMap.put(this.FRESH_CUT_FLAG,      orderInfoResultSet.getString("fresh_cut_flag")        != null?(String)orderInfoResultSet.getString("fresh_cut_flag"):null);
                paymentRefundMap.put(this.PRODUCT_TYPE,        orderInfoResultSet.getString("product_type")          != null?(String)orderInfoResultSet.getString("product_type"):null);
                paymentRefundMap.put(this.SHIP_METHOD,         orderInfoResultSet.getString("ship_method")           != null?(String)orderInfoResultSet.getString("ship_method"):null);
                paymentRefundMap.put("mp_redemption_rate_amt", orderInfoResultSet.getString("mp_redemption_rate_amt")!= null?(String)orderInfoResultSet.getString("mp_redemption_rate_amt"):null);
                paymentRefundMap.put("company_id",             orderInfoResultSet.getString("company_id")            != null?(String)orderInfoResultSet.getString("company_id"):null);
                paymentRefundMap.put("recipient_country",      orderInfoResultSet.getString("recipient_country")     != null?(String)orderInfoResultSet.getString("recipient_country"):null);
            }
        }
        finally
        {
        }


        // ORDER INFO
        CachedResultSet orderStatusResultSet = null;

        try
        {
            orderStatusResultSet = (CachedResultSet) paymentMap.get("OUT_ORDER_STATUS_CUR");

            while(orderStatusResultSet != null && orderStatusResultSet.next())
            {
                paymentRefundMap.put(this.ORDER_LIVE_FLAG, orderStatusResultSet.getObject("has_live_ftd") != null ? orderStatusResultSet.getString("has_live_ftd") : null);
                paymentRefundMap.put(this.ORDER_CANCEL_FLAG, orderStatusResultSet.getObject("cancel_sent") != null ? orderStatusResultSet.getString("cancel_sent") : null);
            }
        }
        finally
        {
        }

        return paymentRefundMap;
    }

    private boolean isNewRoute(String route) {
    	try {
    		if (StringUtils.isNotBlank(route)) {
    			String[] supportedPGRoutes = null;
	    		ConfigurationUtil cu = ConfigurationUtil.getInstance();
	    		String strSupportedPGRoutes = cu.getFrpGlobalParm(OrderProcessingConstants.SERVICE, OrderProcessingConstants.PG_ROUTES);
	    		String isNewPGEnabled = cu.getFrpGlobalParm(OrderProcessingConstants.SERVICE, OrderProcessingConstants.PG_SVC_ENABLED);
	    		if ("Y".equalsIgnoreCase(isNewPGEnabled) && !StringUtils.isEmpty(strSupportedPGRoutes)) {
	    			supportedPGRoutes = StringUtils.split(strSupportedPGRoutes, ",");
	    		}
    		
				if(supportedPGRoutes != null) {
					for (String supportedRoute : supportedPGRoutes) {
						if (StringUtils.equalsIgnoreCase(supportedRoute, route)) {
							return true;
						}
					}
				}
    		}
    	} catch(Exception e) {
    		logger.error("Exception occured while checking new route", e);
    	}
    	
		return false;
	}

    public String insertRefund(RefundVO refund) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);

        dataRequest.setStatementID("REFUND_INSERT_REFUND");
        dataRequest.addInputParam("IN_REFUND_DISP_CODE", refund.getRefundDispCode());
        dataRequest.addInputParam("IN_CREATED_BY", refund.getCreatedBy());
        dataRequest.addInputParam("IN_REFUND_PRODUCT_AMOUNT", refund.getOrderBill().getProductAmount() > 0?new Double(refund.getOrderBill().getProductAmount()):new Double(refund.getOrderBill().getGrossProductAmount()));
        dataRequest.addInputParam("IN_REFUND_ADDON_AMOUNT", new Double(refund.getOrderBill().getAddOnAmount()));
        dataRequest.addInputParam("IN_REFUND_TAX", new Double(refund.getOrderBill().getTax()));
        dataRequest.addInputParam("IN_ORDER_DETAIL_ID", refund.getOrderDetailId());
        dataRequest.addInputParam("IN_RESPONSIBLE_PARTY", refund.getResponsibleParty());

        if(refund.getOrderBill().getVendorFlag() != null
            && refund.getOrderBill().getVendorFlag().equals("SD"))
        {
            dataRequest.addInputParam("IN_REFUND_SERVICE_FEE", new Double(refund.getOrderBill().getServiceFee()));
            dataRequest.addInputParam("IN_REFUND_SHIPPING_FEE", new Double(refund.getOrderBill().getShippingFee()));
        }
        else if(refund.getOrderBill().getVendorFlag() != null
            && refund.getOrderBill().getVendorFlag().equals("Y"))
        {
            dataRequest.addInputParam("IN_REFUND_SERVICE_FEE", new Double(0));
            dataRequest.addInputParam("IN_REFUND_SHIPPING_FEE", new Double(refund.getOrderBill().getFeeAmount()));
        }
        else
        {
            dataRequest.addInputParam("IN_REFUND_SERVICE_FEE", new Double(refund.getOrderBill().getFeeAmount()));
            dataRequest.addInputParam("IN_REFUND_SHIPPING_FEE", new Double(0));
        }

        dataRequest.addInputParam("IN_REFUND_STATUS", refund.getRefundStatus());
        dataRequest.addInputParam("IN_ACCT_TRANS_IND", refund.getAccountTransactionInd());
        dataRequest.addInputParam("IN_REFUND_ADMIN_FEE", new Double(refund.getOrderBill().getAdminFee()).toString());
        dataRequest.addInputParam("IN_REFUND_SERVICE_FEE_TAX", null);
        dataRequest.addInputParam("IN_REFUND_SHIPPING_TAX", new Double(refund.getOrderBill().getShippingTax()).toString());
        dataRequest.addInputParam("IN_REFUND_DISCOUNT_AMOUNT", new Double(refund.getOrderBill().getDiscountAmount()).toString());
        dataRequest.addInputParam("IN_REFUND_COMMISSION_AMOUNT", new Double(refund.getOrderBill().getCommissionAmount()).toString());
        dataRequest.addInputParam("IN_REFUND_WHOLESALE_AMOUNT", new Double(refund.getOrderBill().getWholesaleAmount()).toString());
        dataRequest.addInputParam("IN_WHOLESALE_SERVICE_FEE", new Double(refund.getOrderBill().getWholesaleServiceFee()).toString());

        //These values have to be defaultted if they are not entered
        if (refund.getComplaintCommOriginTypeId() == null || refund.getComplaintCommNotificationTypeId().equals(""))
        {
            refund.setComplaintCommOriginTypeId(complaintCommTypeIdNotAComplaint);
        }
        if (refund.getComplaintCommNotificationTypeId() == null || refund.getComplaintCommNotificationTypeId().equals(""))
        {
            refund.setComplaintCommNotificationTypeId(complaintCommTypeIdNotAComplaint);
        }

        dataRequest.addInputParam("IN_ORIGIN_COMPLAINT_COMM_TYPE_ID", refund.getComplaintCommOriginTypeId());
        dataRequest.addInputParam("IN_NOTIF_COMPLAINT_COMM_TYPE_ID", refund.getComplaintCommNotificationTypeId());

        OrderBillVO obVO = refund.getOrderBill();

        dataRequest.addInputParam("IN_TAX1_NAME", obVO.getTax1Name());
        dataRequest.addInputParam("IN_TAX1_AMOUNT", obVO.getTax1Amount());
        dataRequest.addInputParam("IN_TAX1_DESCRIPTION", obVO.getTax1Description());
        dataRequest.addInputParam("IN_TAX1_RATE", obVO.getTax1Rate());

        dataRequest.addInputParam("IN_TAX2_NAME", obVO.getTax2Name());
        dataRequest.addInputParam("IN_TAX2_AMOUNT", obVO.getTax2Amount());
        dataRequest.addInputParam("IN_TAX2_DESCRIPTION", obVO.getTax2Description());
        dataRequest.addInputParam("IN_TAX2_RATE", obVO.getTax2Rate());

        dataRequest.addInputParam("IN_TAX3_NAME", obVO.getTax3Name());
        dataRequest.addInputParam("IN_TAX3_AMOUNT", obVO.getTax3Amount());
        dataRequest.addInputParam("IN_TAX3_DESCRIPTION", obVO.getTax3Description());
        dataRequest.addInputParam("IN_TAX3_RATE", obVO.getTax3Rate());

        dataRequest.addInputParam("IN_TAX4_NAME", obVO.getTax4Name());
        dataRequest.addInputParam("IN_TAX4_AMOUNT", obVO.getTax4Amount());
        dataRequest.addInputParam("IN_TAX4_DESCRIPTION", obVO.getTax4Description());
        dataRequest.addInputParam("IN_TAX4_RATE", obVO.getTax4Rate());

        dataRequest.addInputParam("IN_TAX5_NAME", obVO.getTax5Name());
        dataRequest.addInputParam("IN_TAX5_AMOUNT", obVO.getTax5Amount());
        dataRequest.addInputParam("IN_TAX5_DESCRIPTION", obVO.getTax5Description());
        dataRequest.addInputParam("IN_TAX5_RATE", obVO.getTax5Rate());
        dataRequest.addInputParam("IN_REFUND_ADDON_DISCOUNT_AMT", new Double(refund.getOrderBill().getAddOnDiscountAmount()).toString());

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        String status = (String) outputs.get(COMConstants.STATUS_PARAM);
        String refundId = (String) outputs.get("OUT_REFUND_ID");
        if(status != null && status.equals("N"))
        {
            String message = (String) outputs.get(COMConstants.MESSAGE_PARAM);
            throw new Exception(message);
        }

        refund.setRefundId(refundId);
        Long paymentRid = this.insertRefundPayment(refund);
        refund.setPaymentRefundId(paymentRid);

        logger.debug("Insert Refund Successful for refund ID: " + refundId);

        return refundId;
    }

    public Long insertRefundPayment(RefundVO refund) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);

        dataRequest.setStatementID("REFUND_UPDATE_PAYMENTS");

        dataRequest.addInputParam("IN_ORDER_GUID", null);
        dataRequest.addInputParam("IN_ADDITIONAL_BILL_ID", null);
        dataRequest.addInputParam("IN_PAYMENT_TYPE", refund.getPaymentMethodId());
        dataRequest.addInputParam("IN_UPDATED_BY", refund.getCreatedBy());
        dataRequest.addInputParam("IN_AUTH_RESULT", null);
        dataRequest.addInputParam("IN_AUTH_NUMBER", null);
        dataRequest.addInputParam("IN_AVS_CODE", null);
        dataRequest.addInputParam("IN_ACQ_REFERENCE_NUMBER", null);
        dataRequest.addInputParam("IN_AUTH_OVERRIDE_FLAG", null);


        dataRequest.addInputParam("IN_CREDIT_AMOUNT", null);
        dataRequest.addInputParam("IN_DEBIT_AMOUNT", refund.getCalculatedRefundAmount().toString());
        dataRequest.addInputParam("IN_PAYMENT_INDICATOR", this.REFUND_PAYMENT_INDICATOR);
        dataRequest.addInputParam("IN_REFUND_ID", refund.getRefundId());
        dataRequest.addInputParam("IN_AUTH_DATE", null);
        dataRequest.addInputParam("IN_AAFES_TICKET_NUMBER", null);
        dataRequest.addInputParam("IO_PAYMENT_ID", null);

        if(refund.getPaymentType() != null)
        {
            if(refund.getPaymentType().equals("C"))
            {
                dataRequest.addInputParam("IN_CC_ID", refund.getCardId() != null && !refund.getCardId().equals("0")?refund.getCardId():null);
                dataRequest.addInputParam("IN_GC_COUPON_NUMBER", null);
            }
            else if(refund.getPaymentType().equals("G"))
            {
                dataRequest.addInputParam("IN_CC_ID", null);
                dataRequest.addInputParam("IN_GC_COUPON_NUMBER", refund.getCardNumber());
            }
            else if(refund.getPaymentType().equals("R"))
            {
                dataRequest.addInputParam("IN_CC_ID", refund.getCardId() != null && !refund.getCardId().equals("0")?refund.getCardId():null);
                if(!StringUtils.isEmpty(refund.getCardNumber())) {
                  refund.setCardNumber("***************" + refund.getCardNumber().substring(refund.getCardNumber().length() - 4,refund.getCardNumber().length()));
                }                
                dataRequest.addInputParam("IN_GC_COUPON_NUMBER", refund.getCardNumber());
            }
            else if(refund.getPaymentType().equals("I"))
            {
                dataRequest.addInputParam("IN_CC_ID", null);
                dataRequest.addInputParam("IN_GC_COUPON_NUMBER", null);
            }
        }
        dataRequest.addInputParam("IN_MILES_POINTS_CREDIT_AMT", null);
        dataRequest.addInputParam("IN_MILES_POINTS_DEBIT_AMT", new Double( refund.getOrderBill().getMilesPointsAmount()).toString());

        dataRequest.addInputParam("IN_CSC_RESPONSE_CODE", null);
        dataRequest.addInputParam("IN_CSC_VALIDATED_FLAG", "N");
        dataRequest.addInputParam("IN_CSC_FAILURE_CNT", new BigDecimal("0"));
        dataRequest.addInputParam("IN_BILL_STATUS", refund.getRefundStatus());
        dataRequest.addInputParam("IN_BILL_DATE", (refund.getRefundDate() != null ? new java.sql.Date(refund.getRefundDate().getTime()) : null));
        dataRequest.addInputParam("IN_IS_VOICE_AUTH", null);
        dataRequest.addInputParam("IN_ASSOCIATED_PAYMENT_ID", refund.getAssociatedPaymentId());
        
        //PaymentVO pavo = refund.getPayment();
        dataRequest.addInputParam("IN_CARDINAL_VERIFIED_FLAG", "N"); //For Refunds we don't required this flag.
        
        dataRequest.addInputParam("IN_AUTH_TRANSACTION_ID",refund.getAuthorizationTransactionId());
        dataRequest.addInputParam("IN_MERCHANT_REF_ID",refund.getMerchantRefID());
        dataRequest.addInputParam("IN_ROUTE",refund.getRoute());
        dataRequest.addInputParam("IN_SETTLEMENT_TRANSACTION_ID",refund.getSettlmentTransactionId());
        dataRequest.addInputParam("IN_REQUEST_TOKEN",refund.getRequestToken());
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        String status = (String) outputs.get(COMConstants.STATUS_PARAM);
        String paymentId = (String) outputs.get("IO_PAYMENT_ID");
        if(status != null && status.equals("N"))
        {
            String message = (String) outputs.get(COMConstants.MESSAGE_PARAM);
            throw new Exception(message);
        }

        logger.debug("Insert Payment Successful for payment ID: " + paymentId);

        return Long.valueOf(paymentId);
    }

    public void updateRefundReviewer(RefundVO refund) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);

        dataRequest.setStatementID("REFUND_UPDATE_REFUND_REVIEWER");
        dataRequest.addInputParam("refund_id", refund.getRefundId());
        dataRequest.addInputParam("reviewed_by", refund.getReviewedBy());

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        String status = (String) outputs.get(COMConstants.STATUS_PARAM);
        if(status != null && status.equals("N"))
        {
            String message = (String) outputs.get(COMConstants.MESSAGE_PARAM);
            throw new Exception(message);
        }

        logger.debug("Update Refund Reviewer Successful for refund ID: " + refund.getRefundId());
    }

    public void updateRefund(RefundVO refund) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);

        dataRequest.setStatementID("UPDATE_REFUND");
        dataRequest.addInputParam("IN_REFUND_ID", refund.getRefundId());
        dataRequest.addInputParam("IN_REFUND_DISP_CODE", refund.getRefundDispCode());
        dataRequest.addInputParam("IN_UPDATED_BY", refund.getUpdatedBy());
        dataRequest.addInputParam("IN_REFUND_STATUS", refund.getRefundStatus());
        dataRequest.addInputParam("IN_REFUND_DATE", refund.getRefundDate() != null ? new java.sql.Date(refund.getRefundDate().getTime()) : null);
        dataRequest.addInputParam("IN_RESPONSIBLE_PARTY", refund.getResponsibleParty());

        //These values have to be defaultted if they are not entered
        if (refund.getComplaintCommOriginTypeId() == null || refund.getComplaintCommNotificationTypeId().equals(""))
        {
            refund.setComplaintCommOriginTypeId(complaintCommTypeIdNotAComplaint);
        }
        if (refund.getComplaintCommNotificationTypeId() == null || refund.getComplaintCommNotificationTypeId().equals(""))
        {
            refund.setComplaintCommNotificationTypeId(complaintCommTypeIdNotAComplaint);
        }

        dataRequest.addInputParam("IN_ORIGIN_COMPLAINT_COMM_TYPE_ID", refund.getComplaintCommOriginTypeId());
        dataRequest.addInputParam("IN_NOTIF_COMPLAINT_COMM_TYPE_ID", refund.getComplaintCommNotificationTypeId());

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        String status = (String) outputs.get(COMConstants.STATUS_PARAM);
        if(status != null && status.equals("N"))
        {
            String message = (String) outputs.get(COMConstants.MESSAGE_PARAM);
            throw new Exception(message);
        }

        logger.debug("Update Refund Successful for refund ID: " + refund.getRefundId());
    }

    public void deleteRefund(String refundId) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);

        dataRequest.setStatementID("REFUND_DELETE_REFUND");
        dataRequest.addInputParam("IN_REFUND_ID", refundId);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        String status = (String) outputs.get(COMConstants.STATUS_PARAM);
        if(status != null && status.equals("N"))
        {
            String message = (String) outputs.get(COMConstants.MESSAGE_PARAM);
            throw new Exception(message);
        }

        logger.debug("Delete Refund Successful for refund ID: " + refundId);
    }

    public void updateGiftCertStatus(String gcNumber, String gcStatus) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);

        dataRequest.setStatementID("REFUND_UPDATE_GCC_STATUS");
        dataRequest.addInputParam("IN_GC_COUPON_NUMBER", gcNumber);
        dataRequest.addInputParam("IN_GC_COUPON_RECIP_ID", new Integer(0));
        dataRequest.addInputParam("IN_GC_COUPON_STATUS", gcStatus);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        String status = (String) outputs.get(COMConstants.STATUS_PARAM);
        if(status != null && status.equals("N"))
        {
            String message = (String) outputs.get(COMConstants.MESSAGE_PARAM);
            throw new Exception(message);
        }

        logger.debug("Update GC Status Successful for giftCardNumber: " + gcNumber);
    }

    public HashMap getCartRefundStatusXML(String orderGuid) throws Exception
    {
        HashMap statusMap = new HashMap();

        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);

        dataRequest.setStatementID("REFUND_GET_CART_REFUND_STATUS");
        dataRequest.addInputParam("IN_ORDER_GUID",  orderGuid);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

        CachedResultSet refundStatusResultSet  = (CachedResultSet) dataAccessUtil.execute(dataRequest);

        while(refundStatusResultSet != null && refundStatusResultSet.next())
        {
            statusMap.put("OUT_HAS_REFUNDS_IND", refundStatusResultSet.getObject("has_refunds_ind") != null ? refundStatusResultSet.getString("has_refunds_ind") : "N");
            statusMap.put("OUT_IN_SCRUB_IND", refundStatusResultSet.getObject("in_scrub_ind") != null ? refundStatusResultSet.getString("in_scrub_ind") : "N");
            statusMap.put("OUT_ALL_CANCELLED_IND", refundStatusResultSet.getObject("all_cancelled_ind") != null ? refundStatusResultSet.getString("all_cancelled_ind") : "N");
            statusMap.put("OUT_DELIVERED_PRINTED_IND", refundStatusResultSet.getObject("delivered_printed_ind") != null ? refundStatusResultSet.getString("delivered_printed_ind") : "N");
            statusMap.put("OUT_PAST_DEVLIVERY_DATE_IND", refundStatusResultSet.getObject("past_devlivery_date_ind") != null ? refundStatusResultSet.getString("past_devlivery_date_ind") : "N");
            statusMap.put("OUT_LIVE_FTD_IND", refundStatusResultSet.getObject("has_live_ftd_ind") != null ? refundStatusResultSet.getString("has_live_ftd_ind") : "N");
        }

        return statusMap;
    }

    public boolean canBeReinstated(String couponNumber) throws Exception
    {
    	boolean reinstateFlag = false;
        BigDecimal refundedAmt = new BigDecimal(0);
        PaymentVO payment = null;
        String refundedOrderDetailId = null;
    	
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);

        dataRequest.setStatementID("GET_GC_ORDER_REFUND_AMT");
        dataRequest.addInputParam("IN_GIFT_CERTIFICATE_ID",  couponNumber);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        CachedResultSet refundStatusResultSet  = (CachedResultSet) dataAccessUtil.execute(dataRequest);

        while(refundStatusResultSet != null && refundStatusResultSet.next())
        {
        	refundedOrderDetailId = refundStatusResultSet.getString("order_detail_id") != null? refundStatusResultSet.getString("order_detail_id") : "";
        	
        	
            logger.info("refundedAmt: " + refundedAmt.toString());
            logger.info("refundedOrderDetailId: " + refundedOrderDetailId);
     
            //call Gift Code Service and retrieve totalRedemption Amount
            RetrieveGiftCodeResponse retrieveGiftCodeResponse = GiftCodeUtil.getGiftCodeById(couponNumber);
      		if(retrieveGiftCodeResponse != null){
      			payment = new PaymentVO();
      			payment.setGcCouponStatus(retrieveGiftCodeResponse.getStatus() != null? retrieveGiftCodeResponse.getStatus() : "");
                List<RedemptionDetails> lRedDetails = new ArrayList(); 
                RedemptionDetails redemptionDetails = new RedemptionDetails();
                lRedDetails = retrieveGiftCodeResponse.getRedemptionDetails();
                BigDecimal redemptionAmount = new BigDecimal(0);
                String redeemedRefNo = null;
               
    	   		if (lRedDetails != null) {
    	   			for(RedemptionDetails redemptDetails:lRedDetails){	
    	   				redemptionAmount = redemptDetails.getRedemptionAmount();
    	   				logger.info("redemptionAmount: " + redemptionAmount);
    	   				//totalAmountAlreadyRedeemed = totalAmountAlreadyRedeemed.add(redemptionAmount);
    	   				redeemedRefNo = redemptDetails.getRedeemedRefNo();
    	   				logger.info("redeemedRefNo: " + redeemedRefNo);
    	   				if (redeemedRefNo.equalsIgnoreCase(refundedOrderDetailId)){
    	   					refundedAmt = refundedAmt.add((BigDecimal)refundStatusResultSet.getObject("refund_amount"));
    	   				    //Compare the redemption amount to the refund payment of associated to the
    	   		      		//gift certificate and order detail id.  If the amounts are the same then the gift certificate can be reinstated.
    	   		      		if(redemptionAmount.compareTo(refundedAmt) == 0){
    	   		      			reinstateFlag = true;
    	   		      		}    	   					
    	   				}
    	   			}
    	   		}
      		}
      		
      		
        	
        }
        


        return reinstateFlag;
    }

  /**
   * method to insert refund info into the Modify Order temp tables
   *
   * @param RefundVO - refund
   * @return long - RefundId
   * @throws java.lang.Exception
   */

    public String insertRefundMO(RefundVO refund)
        throws Exception
    {
        String zero = "0.00";
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);

        dataRequest.setStatementID("REFUND_INSERT_REFUND");
        dataRequest.addInputParam("IN_REFUND_DISP_CODE", refund.getRefundDispCode());
        dataRequest.addInputParam("IN_CREATED_BY", refund.getCreatedBy());
        dataRequest.addInputParam("IN_REFUND_PRODUCT_AMOUNT", refund.getOrderBill().getProductAmount() > 0?new Double(refund.getOrderBill().getProductAmount()):new Double(refund.getOrderBill().getGrossProductAmount()));
        dataRequest.addInputParam("IN_REFUND_ADDON_AMOUNT", new Double(refund.getOrderBill().getAddOnAmount()));
        dataRequest.addInputParam("IN_REFUND_SERVICE_FEE", new Double(refund.getOrderBill().getServiceFee()));
        dataRequest.addInputParam("IN_REFUND_TAX", new Double(refund.getOrderBill().getTax()));
        dataRequest.addInputParam("IN_ORDER_DETAIL_ID", refund.getOrderDetailId());
        dataRequest.addInputParam("IN_RESPONSIBLE_PARTY", refund.getResponsibleParty());
        dataRequest.addInputParam("IN_REFUND_STATUS", refund.getRefundStatus());
        dataRequest.addInputParam("IN_ACCT_TRANS_IND", refund.getAccountTransactionInd());
        dataRequest.addInputParam("IN_REFUND_ADMIN_FEE", String.valueOf(refund.getOrderBill().getAdminFee()));
        dataRequest.addInputParam("IN_REFUND_SHIPPING_FEE", new Double(refund.getOrderBill().getShippingFee()));
        dataRequest.addInputParam("IN_REFUND_SERVICE_FEE_TAX", zero);
        dataRequest.addInputParam("IN_REFUND_SHIPPING_TAX", String.valueOf(refund.getOrderBill().getShippingTax()));
        dataRequest.addInputParam("IN_REFUND_DISCOUNT_AMOUNT", String.valueOf(refund.getOrderBill().getDiscountAmount()));
        dataRequest.addInputParam("IN_REFUND_COMMISSION_AMOUNT", String.valueOf(refund.getOrderBill().getCommissionAmount()));
        dataRequest.addInputParam("IN_REFUND_WHOLESALE_AMOUNT", String.valueOf(refund.getOrderBill().getWholesaleAmount()));
        dataRequest.addInputParam("IN_WHOLESALE_SERVICE_FEE", zero);

        OrderBillVO obVO = refund.getOrderBill();

        dataRequest.addInputParam("IN_TAX1_NAME", obVO.getTax1Name());
        dataRequest.addInputParam("IN_TAX1_AMOUNT", obVO.getTax1Amount());
        dataRequest.addInputParam("IN_TAX1_DESCRIPTION", obVO.getTax1Description());
        dataRequest.addInputParam("IN_TAX1_RATE", obVO.getTax1Rate());

        dataRequest.addInputParam("IN_TAX2_NAME", obVO.getTax2Name());
        dataRequest.addInputParam("IN_TAX2_AMOUNT", obVO.getTax2Amount());
        dataRequest.addInputParam("IN_TAX2_DESCRIPTION", obVO.getTax2Description());
        dataRequest.addInputParam("IN_TAX2_RATE", obVO.getTax2Rate());

        dataRequest.addInputParam("IN_TAX3_NAME", obVO.getTax3Name());
        dataRequest.addInputParam("IN_TAX3_AMOUNT", obVO.getTax3Amount());
        dataRequest.addInputParam("IN_TAX3_DESCRIPTION", obVO.getTax3Description());
        dataRequest.addInputParam("IN_TAX3_RATE", obVO.getTax3Rate());

        dataRequest.addInputParam("IN_TAX4_NAME", obVO.getTax4Name());
        dataRequest.addInputParam("IN_TAX4_AMOUNT", obVO.getTax4Amount());
        dataRequest.addInputParam("IN_TAX4_DESCRIPTION", obVO.getTax4Description());
        dataRequest.addInputParam("IN_TAX4_RATE", obVO.getTax4Rate());

        dataRequest.addInputParam("IN_TAX5_NAME", obVO.getTax5Name());
        dataRequest.addInputParam("IN_TAX5_AMOUNT", obVO.getTax5Amount());
        dataRequest.addInputParam("IN_TAX5_DESCRIPTION", obVO.getTax5Description());
        dataRequest.addInputParam("IN_TAX5_RATE", obVO.getTax5Rate());

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        String status = (String) outputs.get(COMConstants.STATUS_PARAM);
        String refundId = (String) outputs.get("OUT_REFUND_ID");
        if(status != null && status.equals("N"))
        {
            String message = (String) outputs.get(COMConstants.MESSAGE_PARAM);
            throw new Exception(message);
        }
        logger.debug("Insert Refund Successful for refund ID: " + refundId);
        return refundId;
    }

    public String isOrderFullyRefundedCRS(String orderDetailId) throws Exception
    {
      DataRequest dataRequest = new DataRequest();
      dataRequest.setConnection(this.connection);
      dataRequest.setStatementID("REFUND_IS_ORDER_FULLY_REFUNDED");
      dataRequest.addInputParam("IN_ORDER_DETAIL_ID", orderDetailId);

      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

      String searchResults = (String) dataAccessUtil.execute(dataRequest);

      return searchResults;

    }
    public boolean isRefundReturned(long paymentId) throws Exception
    {
            logger.debug("Entering isRefundReturned");
            String paymentCode = null;
        try{
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(connection);
            dataRequest.setStatementID("GET_GD_BY_PAY_ID");
            dataRequest.addInputParam("IN_PAYMENT_ID", String.valueOf(paymentId));
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            
            logger.info("paymentId = " + paymentId);
            paymentCode = ((String)dataAccessUtil.execute(dataRequest));
            dataRequest.reset();
            logger.debug("Exiting isRefundReturned");
        } finally {
        }
        return paymentCode != null && paymentCode.equalsIgnoreCase("R")?true:false;
    }
    
    public boolean isNewPaymentRoute(String orderDetailId) throws Exception
    {
            logger.debug("Entering isNewPaymentRoute");
        try{
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(connection);
            dataRequest.setStatementID("GET_PAYMENT_ROUTE");
            dataRequest.addInputParam("IN_ORDER_DETAIL_ID", orderDetailId);
            logger.info("orderDetailId = " + orderDetailId);
            
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

            CachedResultSet paymentRouteResultSet  = (CachedResultSet) dataAccessUtil.execute(dataRequest);
            
            String[] supportedPGRoutes = null;
            ConfigurationUtil cu = ConfigurationUtil.getInstance();
			String strSupportedPGRoutes = cu.getFrpGlobalParm(OrderProcessingConstants.SERVICE, OrderProcessingConstants.PG_ROUTES);
			String isNewPGEnabled = cu.getFrpGlobalParm(OrderProcessingConstants.SERVICE, OrderProcessingConstants.PG_SVC_ENABLED);
			if(!StringUtils.isEmpty(strSupportedPGRoutes)) {
				supportedPGRoutes = StringUtils.split(strSupportedPGRoutes, ",");
			}

			if("Y".equalsIgnoreCase(isNewPGEnabled) && supportedPGRoutes != null && supportedPGRoutes.length > 0) {
				while(paymentRouteResultSet != null && paymentRouteResultSet.next()) {
					for(String supportedRoute : supportedPGRoutes) {
		                String route =(String) paymentRouteResultSet.getObject("ROUTE");
		                if(StringUtils.equalsIgnoreCase(supportedRoute, route)) {
		                	logger.debug("Exiting isNewPaymentRoute: return value is true");
		                	return true;
		                }
		            }
				}
				
			}
        } finally {
        }
        logger.debug("Exiting isNewPaymentRoute: return value is false");
        return false;
    } 
 
}