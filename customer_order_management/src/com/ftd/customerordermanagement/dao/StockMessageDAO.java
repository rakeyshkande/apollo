package com.ftd.customerordermanagement.dao;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.vo.StockMessageVO;
import com.ftd.messagegenerator.vo.PointOfContactVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

import java.math.BigDecimal;
import java.sql.Clob;
import java.sql.Connection;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;




import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * StockMessageDAO
 * 
 */
public class StockMessageDAO 
{
    private Connection connection = null;
    private static Logger logger  = new Logger("com.ftd.customerordermanagement.dao.StockMessageDAO");
    private static final String OUT_CUR = "OUT_CUR";
    private static final String OUT_ID_COUNT = "OUT_ID_COUNT";
    private static final String OUT_PARAMETERS = "out-parameters";
    
/**
 * Constructor
 * @param conn
 */
  public StockMessageDAO(Connection conn)
  {
    this.connection = conn;
  }
  
  public Document loadMessageTitlesXML(String messageType, String autoResponseFlag) throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(connection);
    
    dataRequest.setStatementID(messageType.equals(COMConstants.EMAIL_MESSAGE_TYPE)? "VIEW_EMAIL_TITLES":"VIEW_LETTER_TITLES");
    if(messageType.equals(COMConstants.EMAIL_MESSAGE_TYPE))
    {
      dataRequest.addInputParam("IN_AUTO_RESPONSE_INDICATOR", autoResponseFlag);
    }
    dataRequest.addInputParam("IN_ORIGIN_ID", null);    
    dataRequest.addInputParam("IN_PARTNER_NAME", null);    
    dataRequest.addInputParam("IN_ALL_TITLES_INDICATOR", "Y");        
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    
    return (Document) dataAccessUtil.execute(dataRequest);
  }
  
  public Document loadMessageTitlesXML(String messageType, String originId, String companyId, String autoResponseFlag, String listAllFlag, String partnerName) throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(connection);
    
    dataRequest.setStatementID(messageType.equals(COMConstants.EMAIL_MESSAGE_TYPE)? "VIEW_EMAIL_TITLES":"VIEW_LETTER_TITLES");        
    if(messageType.equals(COMConstants.EMAIL_MESSAGE_TYPE))
    {
      dataRequest.addInputParam("IN_AUTO_RESPONSE_INDICATOR", autoResponseFlag);
    }
    dataRequest.addInputParam("IN_ORIGIN_ID", originId);        
      dataRequest.addInputParam("IN_PARTNER_NAME", partnerName);        
    dataRequest.addInputParam("IN_ALL_TITLES_INDICATOR", listAllFlag);     
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    
    return (Document) dataAccessUtil.execute(dataRequest);    
  }
  
  public Document loadMessageContent(String messageType, String messageId) throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(connection);
    
    dataRequest.setStatementID(messageType.equals(COMConstants.EMAIL_MESSAGE_TYPE)? "VIEW_EMAIL_CONTENT":"VIEW_LETTER_CONTENT");
    dataRequest.addInputParam(messageType.equals(COMConstants.EMAIL_MESSAGE_TYPE)? "IN_STOCK_EMAIL_ID":"IN_STOCK_LETTER_ID", messageId);        
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    
    return (Document) dataAccessUtil.execute(dataRequest);
  }

  public void insertStockMessage(StockMessageVO messageObj) throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(connection);
    dataRequest.setStatementID(messageObj.getMessageType().equals(COMConstants.EMAIL_MESSAGE_TYPE)
     ? "INSERT_STOCK_EMAIL":"INSERT_STOCK_LETTER");
    dataRequest.addInputParam("IN_TITLE", messageObj.getTitle());
    dataRequest.addInputParam("IN_BODY", messageObj.getContent());
    if(messageObj.getMessageType().equals(COMConstants.EMAIL_MESSAGE_TYPE))
    {
      dataRequest.addInputParam("IN_SUBJECT", messageObj.getSubject());
      dataRequest.addInputParam("IN_AUTO_RESPONSE_INDICATOR", messageObj.getAutoResponseIndicator());
    }
    dataRequest.addInputParam("IN_CREATED_BY", messageObj.getCreatedBy());
    dataRequest.addInputParam("IN_ORIGIN_ID", messageObj.getOriginId());
    dataRequest.addInputParam("IN_PARTNER_NAME", messageObj.getPartnerName());
    dataRequest.addInputParam("IN_STOCK_EMAIL_TYPE_ID", messageObj.getStockEmailType());
    
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();        
    Map outputs = (Map) dataAccessUtil.execute(dataRequest);
    
    String status = (String) outputs.get(COMConstants.STATUS_PARAM);
    if(status != null && status.equals("N"))
    {
        String message = (String) outputs.get(COMConstants.MESSAGE_PARAM);
        throw new Exception(message);
    }
    logger.debug("Insert message Successful. Status="+status);    
  }

  public void updateStockMessage(StockMessageVO messageObj, String originUpdateFlag) throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(connection);
    dataRequest.setStatementID(messageObj.getMessageType().equals(COMConstants.EMAIL_MESSAGE_TYPE)
     ? "UPDATE_STOCK_EMAIL":"UPDATE_STOCK_LETTER");
    dataRequest.addInputParam("IN_TITLE", messageObj.getTitle());
    dataRequest.addInputParam(messageObj.getMessageType().equals(COMConstants.EMAIL_MESSAGE_TYPE)
     ? "IN_STOCK_EMAIL_ID":"IN_STOCK_LETTER_ID", messageObj.getMessageId());
    dataRequest.addInputParam("IN_BODY", messageObj.getContent());
    if(messageObj.getMessageType().equals(COMConstants.EMAIL_MESSAGE_TYPE))
    {
      dataRequest.addInputParam("IN_SUBJECT", messageObj.getSubject());
      dataRequest.addInputParam("IN_AUTO_RESPONSE_INDICATOR", messageObj.getAutoResponseIndicator());
    }
    dataRequest.addInputParam("IN_STOCK_EMAIL_TYPE_ID", messageObj.getStockEmailType());
    dataRequest.addInputParam("IN_UPDATED_BY", messageObj.getUpdatedBy());
    dataRequest.addInputParam("IN_ORIGIN_ID", messageObj.getOriginId());
    dataRequest.addInputParam("IN_PARTNER_NAME", messageObj.getPartnerName());
    dataRequest.addInputParam("IN_UPDATE_ORIGIN_INDICATOR", originUpdateFlag);
    
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();  
    
    Map outputs = (Map) dataAccessUtil.execute(dataRequest);
    
    String status = (String) outputs.get(COMConstants.STATUS_PARAM);
    if(status != null && status.equals("N"))
    {
        String message = (String) outputs.get(COMConstants.MESSAGE_PARAM);
        throw new Exception(message);
    }
    logger.debug("Update message Successful. Status="+status);    
  }
  
  public boolean stockMessageExists(String messageType, String messageTitle) throws Exception
  {
    // view all stock message titles
    Document msgTitlesXML = loadMessageTitlesXML(messageType, null);
    int num = msgTitlesXML.getChildNodes().item(0).getChildNodes().getLength();
     Element x1 = (Element) msgTitlesXML.getChildNodes().item(0);
    for(int i=0; i<num; i++)
    {
      Element x2 = (Element) x1.getChildNodes().item(i);
      Element x3 = (Element) x2.getLastChild();
      String title = x3.getFirstChild().getNodeValue();
      if(messageTitle.equals(title))
      {
        return true;
      }
    }
    // the stored proc should search the db to see if a email or letter exists with the same title
    return false;
  }

  public StockMessageVO loadMessageTemplate(String messageType, String companyId, String stockMessageId) throws Exception
  {
    StockMessageVO msgObj = null;
    
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(connection);
    
    dataRequest.setStatementID(messageType.equalsIgnoreCase(COMConstants.EMAIL_MESSAGE_TYPE)? "VIEW_STOCK_EMAIL":"VIEW_STOCK_LETTER");
    dataRequest.addInputParam(messageType.equalsIgnoreCase(COMConstants.EMAIL_MESSAGE_TYPE) ? "IN_STOCK_EMAIL_ID":"IN_STOCK_LETTER_ID", stockMessageId);
    
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    
    CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
        
    if(rs.next())
    {
      msgObj = new StockMessageVO();
      msgObj.setMessageId(stockMessageId);
      msgObj.setMessageType(messageType);
      msgObj.setOriginId("");
      msgObj.setTitle(rs.getString("title"));
      if(messageType.equalsIgnoreCase(COMConstants.EMAIL_MESSAGE_TYPE))
      {
        msgObj.setSubject(rs.getString("subject"));
      }
      
      msgObj.setContent(rs.getClob("body").getSubString((long)1, (int)rs.getClob("body").length()));
    }
    
    return msgObj;    
  }
 
  public PointOfContactVO loadPointOfContact(String pocId) throws Exception
  {
    PointOfContactVO pocObj = null;    
    DataRequest dataRequest = new DataRequest();
    
    dataRequest.setConnection(connection);
    dataRequest.setStatementID("VIEW_POINT_OF_CONTACT");
    dataRequest.addInputParam("IN_POINT_OF_CONTACT_ID", pocId);
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();        

    CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
    
    if(rs.next())
    {
      pocObj = new PointOfContactVO(); 
      pocObj.setPointOfContactId(rs.getLong("POINT_OF_CONTACT_ID"));
      pocObj.setCustomerId(rs.getInt("customer_id"));
      pocObj.setSenderEmailAddress(rs.getString("sender_email_address"));
      pocObj.setRecipientEmailAddress(rs.getString("RECIPIENT_EMAIL_ADDRESS"));
      pocObj.setEmailSubject(rs.getString("EMAIL_SUBJECT"));
      Clob pocBody = rs.getClob("body");
      if(pocBody!=null)
        pocObj.setBody(pocBody.getSubString((long)1, (int) pocBody.length()));
      pocObj.setOrderDetailId(rs.getLong("order_detail_id"));
      pocObj.setOrderGuid(rs.getString("order_guid"));
      pocObj.setFirstName(rs.getString("FIRST_NAME"));
      pocObj.setLastName(rs.getString("LAST_NAME"));
      pocObj.setDaytimePhoneNumber(rs.getString("DAYTIME_PHONE_NUMBER"));
      
      if(rs.getDate("DELIVERY_DATE") != null)
      {
            Calendar cal = Calendar.getInstance();
            cal.setTime(rs.getDate("DELIVERY_DATE"));
            pocObj.setDeliveryDate(cal);
      }
      
      pocObj.setNewRecipFirstName(rs.getString("NEW_RECIP_FIRST_NAME"));
      pocObj.setNewRecipLastName(rs.getString("NEW_RECIP_LAST_NAME"));
      pocObj.setNewAddress(rs.getString("NEW_ADDRESS"));
      pocObj.setNewCity(rs.getString("NEW_CITY"));
      pocObj.setNewState(rs.getString("NEW_STATE"));
      pocObj.setNewZipCode(rs.getString("NEW_ZIP_CODE"));
      pocObj.setNewCountry(rs.getString("NEW_COUNTRY"));
      pocObj.setCompanyId(rs.getString("COMPANY_ID"));
    }
    
    return pocObj;
  }
  
  public Document loadPointOfContactXML(String pocId) throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    
    dataRequest.setConnection(connection);
    dataRequest.setStatementID("GET_POINT_OF_CONTACT_AND_FTD_XML");
    dataRequest.addInputParam("IN_POINT_OF_CONTACT_ID", pocId);
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();        
    return (Document) dataAccessUtil.execute(dataRequest);
  }
  
  public Document loadPointOfContactAsksXML(String mercOrderNum) throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    
    dataRequest.setConnection(connection);
    dataRequest.setStatementID("GET_POINT_OF_CONTACT_ASKS_XML");
    dataRequest.addInputParam("IN_MERCURY_ORDER_NUM", mercOrderNum);
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();        
    return (Document) dataAccessUtil.execute(dataRequest);
  }
  
  public String loadLetterLogo(String CompanyId) throws Exception
  {
    String logoFileName = null;
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(connection);
    
    dataRequest.setStatementID("GET_COMPANY_LOGO");        
    dataRequest.addInputParam("IN_COMPANY_ID", CompanyId);
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    
    CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
        
    if(rs.next())
    {
      logoFileName = rs.getString("logo_filename");
    }
    
    return logoFileName;
  }


  public void updatePOCSubjectBody(String pocId, String subject, String body, String updatedBy) throws Exception
  {
    DataRequest dataRequest = new DataRequest();

    dataRequest.setConnection(connection);
    dataRequest.setStatementID("UPDATE_POINT_OF_CONTACT");
    
    dataRequest.addInputParam("IN_POINT_OF_CONTACT_ID", pocId);
    dataRequest.addInputParam("IN_EMAIL_SUBJECT", subject);
    dataRequest.addInputParam("IN_BODY", body);
    dataRequest.addInputParam("IN_UPDATED_BY", updatedBy);
    
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();        
    Map outputs = (Map) dataAccessUtil.execute(dataRequest);
    
    String status = (String) outputs.get(COMConstants.STATUS_PARAM);
    if(status != null && status.equals("N"))
    {
        String message = (String) outputs.get(COMConstants.MESSAGE_PARAM);
        throw new Exception(message);
    }
    
  }
  
/**
 * This method will check if stock email type id is DCON.  If it is, it will return a 'Y'
 * 
 * @throws java.lang.Exception
 * @param String stockEmailId
 * @return boolean
 */

public boolean isDCONEmail(String stockEmailId) throws Exception
{
    boolean isDCONEmail = false;
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(this.connection);
    dataRequest.setStatementID("IS_DCON_EMAIL");
    dataRequest.addInputParam("IN_STOCK_EMAIL_ID", stockEmailId);
    
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    String stockEmailTypeId = (String) dataAccessUtil.execute(dataRequest);
    
    if(stockEmailTypeId != null && stockEmailTypeId.equalsIgnoreCase("Y"))
    {        
        isDCONEmail = true;    
    }
    return isDCONEmail;
}
 

}