package com.ftd.customerordermanagement.dao;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.vo.CommentsVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;

import java.util.HashMap;
import java.util.Map;

/**
 * CommentDAO
 */

public class CommentDAO 
{
    private Connection connection;
    private static Logger logger  = new Logger("com.ftd.customerordermanagement.dao.CommentDAO");
    
  /**
   * Constructor
   * @param connection Connection
   */
  public CommentDAO(Connection connection)
  {
    this.connection = connection;
  }

  /**
   * <<deprecated>>
   * This method loads order comments
   * @param orderGuid
   * @param position
   * @param maxRecords
   * @param managerRole
   * @return 
   * @throws java.lang.Exception
   */
  public HashMap loadOrderCommentsXML(String orderGuid, int position, int maxRecords, String managerRole, String uoOnly, String hideSystem) throws Exception
  {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("GET_COMMENTS");
        dataRequest.addInputParam("IN_COMMENT_TYPE", COMConstants.ORDER_COMMENT_TYPE);
        dataRequest.addInputParam("IN_CUSTOMER_ID", null);
        dataRequest.addInputParam("IN_ORDER_GUID", orderGuid);
        dataRequest.addInputParam("IN_ORDER_DETAIL_ID", null);
        dataRequest.addInputParam("IN_ROLE_ID", managerRole);
        dataRequest.addInputParam("IN_START_POSITION", new Long(position));
        dataRequest.addInputParam("IN_MAX_NUMBER_RETURNED", new Long(maxRecords));
        dataRequest.addInputParam("IN_MO_COMMENT_INDICATOR", uoOnly);
        dataRequest.addInputParam("IN_HIDE_SYS_COMMENTS", hideSystem);
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        return (HashMap) dataAccessUtil.execute(dataRequest);

  }

  /**
   * This method loads all the comments for this order detail id
   * @param orderDetailId
   * @param position
   * @param maxRecords
   * @param managerRole
   * @return 
   * @throws java.lang.Exception
   */
  public HashMap loadOrderDetailCommentsXML(String orderDetailId, int position, int maxRecords, String managerRole, String uoOnly, String hideSystem, String originTab) throws Exception
  {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        
        if(COMConstants.DEFAULT_TAB.equalsIgnoreCase(originTab))
        {	
        	dataRequest.setStatementID("GET_FILTERED_COMMENTS");
        	logger.info("In CommentDAO.loadOrderDetailCommentsXML -> Executing GET_FILTERED_COMMENTS...");
        }	
        else
        {	
        	dataRequest.setStatementID("GET_COMMENTS");
        }	
        
        dataRequest.addInputParam("IN_COMMENT_TYPE", COMConstants.ORDER_COMMENT_TYPE);
        dataRequest.addInputParam("IN_CUSTOMER_ID", null);
        dataRequest.addInputParam("IN_ORDER_GUID", null);
        dataRequest.addInputParam("IN_ORDER_DETAIL_ID",orderDetailId);
        dataRequest.addInputParam("IN_ROLE_ID", managerRole);
        dataRequest.addInputParam("IN_START_POSITION", new Long(position));
        dataRequest.addInputParam("IN_MAX_NUMBER_RETURNED", new Long(maxRecords));
        dataRequest.addInputParam("IN_MO_COMMENT_INDICATOR", uoOnly);
        dataRequest.addInputParam("IN_HIDE_SYS_COMMENTS", hideSystem);
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        return (HashMap) dataAccessUtil.execute(dataRequest);      
 
  }

  public void insertComment(CommentsVO comment) throws Exception
  {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("INSERT_COMMENTS");
        dataRequest.addInputParam("IN_CUSTOMER_ID", comment.getCustomerId());
        dataRequest.addInputParam("IN_ORDER_GUID", comment.getOrderGuid());
        dataRequest.addInputParam("IN_ORDER_DETAIL_ID", comment.getOrderDetailId());
        dataRequest.addInputParam("IN_COMMENT_ORIGIN", comment.getCommentOrigin());
        dataRequest.addInputParam("IN_REASON", comment.getReason());
        dataRequest.addInputParam("IN_DNIS_ID", comment.getDnisId());
        dataRequest.addInputParam("IN_COMMENT_TEXT", comment.getComment());
        dataRequest.addInputParam("IN_COMMENT_TYPE", comment.getCommentType());
        dataRequest.addInputParam("IN_CSR_ID", comment.getUpdatedBy());
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);      
        String status = (String) outputs.get(COMConstants.STATUS_PARAM);
        if(status != null && status.equals("N"))
        {
            String message = (String) outputs.get(COMConstants.MESSAGE_PARAM);
            throw new Exception(message);
        }
        logger.debug("Insert Comments Successful. Status="+status);

  }
  
  /**
   * method for loading customer comments
   * @param customerId
   * @param position
   * @param maxRecords
   * @param managerRole
   * @return 
   * @throws java.lang.Exception
   */

  public HashMap loadAllCustomerCommentsXML(String customerId, int position, int maxRecords, String managerRole, String uoOnly, String hideSystem) throws Exception
  {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("GET_COMMENTS");
        dataRequest.addInputParam("IN_COMMENT_TYPE", COMConstants.CUSTOMER_COMMENT_TYPE);
        dataRequest.addInputParam("IN_CUSTOMER_ID", customerId);
        dataRequest.addInputParam("IN_ORDER_GUID", null);
        dataRequest.addInputParam("IN_ORDER_DETAIL_ID", null);
        dataRequest.addInputParam("IN_ROLE_ID", managerRole); //from where?
        dataRequest.addInputParam("IN_START_POSITION", new Long(position));
        dataRequest.addInputParam("IN_MAX_NUMBER_RETURNED", new Long(maxRecords));
        dataRequest.addInputParam("IN_MO_COMMENT_INDICATOR", uoOnly);
        dataRequest.addInputParam("IN_HIDE_SYS_COMMENTS", hideSystem);
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        return (HashMap)dataAccessUtil.execute(dataRequest);      
  }


  public void editComment(CommentsVO comment) throws Exception
  {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("UPDATE_COMMENTS");
        dataRequest.addInputParam("IN_COMMENT_ID", comment.getCommentId());
        dataRequest.addInputParam("IN_COMMENT_TEXT", comment.getComment());
        dataRequest.addInputParam("IN_CSR_ID", comment.getUpdatedBy());
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);      
        String status = (String) outputs.get(COMConstants.STATUS_PARAM);
        if(status != null && status.equals("N"))
        {
            String message = (String) outputs.get(COMConstants.MESSAGE_PARAM);
            throw new Exception(message);
        }
        logger.debug("Edit Comments Successful for comment id=" + comment.getCommentId() + ". Status="+status);

  }
  
  
 
}