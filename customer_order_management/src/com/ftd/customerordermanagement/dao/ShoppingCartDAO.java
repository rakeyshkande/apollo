package com.ftd.customerordermanagement.dao;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.sql.Connection;

import org.w3c.dom.Document;



/**
 * ShoppingCartDAO
 * This is the DAO that handles loading and updating shopping cart information
 * 
 * @author Luke W. Pennings
 * @version $Id: ShoppingCartDAO.java
 */

public class ShoppingCartDAO 
{
    private Connection connection;
    private static Logger logger  = new Logger("com.ftd.customerordermanagement.dao.ShoppingCartDAO");
    
  /**
   * Constructor
   * @param connection Connection
   */
  public ShoppingCartDAO(Connection connection)
  {
    this.connection = connection;
  }

  /**
   * method for geting shopping cart info by orderGuid.
   * @return Document containing the values from the cursor 
   * @throws java.lang.Exception
   */

  public Document getCurrentBillingTotals(String orderGuid) 
        throws Exception
  {
        Document searchResults = DOMUtil.getDocument();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("GET_CART_TOTALS");
        dataRequest.addInputParam("IN_ORDER_GUID", orderGuid);
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        searchResults = (Document) dataAccessUtil.execute(dataRequest);      

        return searchResults;
  }
}