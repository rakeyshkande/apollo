package com.ftd.customerordermanagement.dao;

import java.sql.Connection;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.vo.OrderDetailVO;
import com.ftd.customerordermanagement.vo.OrderVO;
import com.ftd.ftdutilities.FTDCAMSUtils;
import com.ftd.ftdutilities.FieldUtils;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.cacheMgr.CacheUtil;
import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.AccountProgramMasterVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.MembershipDetailsVO;

/**
 * Provides DAO methods for Services and Program Information.
 */
public class ServicesProgramDAO
{
  private Logger logger = new Logger(ServicesProgramDAO.class.getName());

  
  private static final String STMT_GET_PRODUCT_BY_ID = "GET_PRODUCT_BY_ID";
  private static final String STMT_GET_ACCOUNT_ACTIVE_PROGRAMS  = "UTIL_ACCT_VIEW_ACCOUNT_ACTIVE_PROGRAMS";  

  private static final String PROGRAM_TYPE_FREESHIP = "FREESHIP";
  private static final String PRODUCT_TYPE_SERVICES = "SERVICES";
  private static final String PRODUCT_SUBTYPE_FREESHIP = "FREESHIP";

  private static final String CACHE_ACCOUNT_PROGRAM_MASTER = "CACHE_NAME_ACCOUNT_PROGRAM_MASTER";

  public ServicesProgramDAO()
  {
  }


  /**
   * Returns true if the passed in Product Id is a free Shipping Product
   * @param connection
   * @param productId
   * @return
   */
  public boolean isProductFreeShipping(Connection connection, String productId)
    throws Exception
  {
    Map paramMap = new HashMap();

    paramMap.put("IN_PRODUCT_ID", productId);

    // Get the order header infomration
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(connection);
    dataRequest.setStatementID(STMT_GET_PRODUCT_BY_ID);
    dataRequest.setInputParams(paramMap);

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

    if (rs == null || !rs.next())
    {
      return false;
    }

    String productType = rs.getString("productType");
    String productSubType = rs.getString("productSubType");

    if (PRODUCT_TYPE_SERVICES.equalsIgnoreCase(productType) && PRODUCT_SUBTYPE_FREESHIP.equalsIgnoreCase(productSubType))
    {
      return true;
    }

    return false;
  }

  /**
   * Returns the Information related to the Free Shipping Program from the Account.Program_Master Table.
   * @return
   * @throws Exception
   */
  public AccountProgramMasterVO getFreeShippingProgramInfo()
  {
	
	  try{
		  CacheUtil cacheUtil = CacheUtil.getInstance();  
    
      
		  AccountProgramMasterVO apmVO = cacheUtil.getFSAccountProgramDetailsFromContent();
		  return apmVO;
	  }catch(Exception e){
		logger.error("Error accessing content filter cache");
		throw new CacheException("Error accessing content filter cache.", e);
	  }
  }
  
  
  /**
   * Returns true if the buyer has free shipping on the specified Order.
   * The Buyer Signed In must = 'Y', and the Buyer must have a FREESHIP
   * program active for the Order Date.
   * @param connection
   * @param orderDetailId
   * @return
   */
  public Boolean getBuyerHasFreeShipping(Connection connection, String orderDetailId)
      throws Exception
  {
    OrderDAO orderDAO = new OrderDAO(connection);
    OrderDetailVO orderDetailVO = orderDAO.getOrderDetail(orderDetailId);
    
    OrderVO orderVO = orderDAO.getOrder(orderDetailVO.getOrderGuid());
    // Check buyer Signed In, if not 'Y' return false
    if (!StringUtils.equalsIgnoreCase(orderVO.getBuyerSignedInFlag(), "Y"))
    {
      return false;
    }
    
    
    // Get Customer Information
    CachedResultSet customerInfo  = orderDAO.getOrderCustomerInfo(orderDetailId);
    
    String companyId = orderVO.getCompanyId();
    Date orderDate = orderVO.getOrderDate().getTime();
    String emailAddress = "";
    
    if(customerInfo.next())
    {
      emailAddress = customerInfo.getString("email_address");
    }
    Boolean flag = null;
    List<String> emailList = new ArrayList<String>();
	emailList.add(emailAddress);
	Map<String, MembershipDetailsVO> membershipMap = FTDCAMSUtils.getMembershipData(emailList, orderDate);
	if(membershipMap == null){
		flag = null;		
	}else{
		if(membershipMap.containsKey(FTDCAMSUtils.FREESHIPPING_MEMBER_TYPE)){
			MembershipDetailsVO fsMemberDetails = membershipMap.get(FTDCAMSUtils.FREESHIPPING_MEMBER_TYPE);
			if(fsMemberDetails != null){
				flag = fsMemberDetails.isMembershipBenefitsApplicable();
			}else{
				flag = false;
			}
		}else{
			flag = false;
		}
		
	}
    return flag;
  }
  
  
  
  /**
   * Returns a Set containing the Active Program Names for the Apollo Account with the passed in Email Address
   * and order date.
   * 
   * @param connection
   * @param emailAddress
   * @param companyId
   * @param orderDate
   * @return
   */
  private Set<String> getActiveProgramsForAccount(Connection connection, String emailAddress, Date orderDate)
  { 
    Map paramMap = new HashMap();
    paramMap.put("IN_EMAIL_ADDRESS", emailAddress);
    paramMap.put("IN_ACTIVE_DATE", new java.sql.Timestamp(orderDate.getTime()));
    
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(connection);
    dataRequest.setStatementID(STMT_GET_ACCOUNT_ACTIVE_PROGRAMS);
    dataRequest.setInputParams(paramMap);
    Set<String> retVal = new HashSet<String>();

    try
    {
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      Map outputs = (Map) dataAccessUtil.execute(dataRequest);

      CachedResultSet rs = (CachedResultSet) outputs.get("OUT_CUR");
      String out_status = (String) outputs.get("OUT_STATUS");
      String out_message = (String) outputs.get("OUT_MESSAGE");
      
      if("Y".equalsIgnoreCase(out_status) == false)
      {
        throw new Exception("Failed to retrieve account program information: " + out_message);
      }
      
      while(rs.next())
      {
        String programName = rs.getString("PROGRAM_NAME");
        
        if(!StringUtils.isEmpty(programName))
        {
          retVal.add(programName);
        }
      }
      
      return retVal;
    }
    catch (Exception e)
    {
      throw new RuntimeException(e.getMessage(), e);
    } 
  }  
  
  
  /**
   * Returns the Msg to be displayed for the passed in savings amount.
   * 
   * @param connection
   * @param savingsAmount
   * @return
   */
  public String getFreeShippingOrderSavingsMsg(Connection connection, double savingsAmount)
    throws Exception
  {
    String cartFeesSavedMsg = ConfigurationUtil.getInstance().getContentWithFilter(connection, COMConstants.FREE_SHIPPING_CONTEXT,
                                                         COMConstants.ORDER_FEES_SAVED_MESSAGE, null, null);

    AccountProgramMasterVO apmVO = getFreeShippingProgramInfo();
    cartFeesSavedMsg = FieldUtils.replaceAll(cartFeesSavedMsg, "~program_display_name~", apmVO.getDisplayName());
    cartFeesSavedMsg = FieldUtils.replaceAll(cartFeesSavedMsg, "~total_order_savings~", formatCurrencyString(savingsAmount));    
    
    return cartFeesSavedMsg;    
  }  
  
  
  /**
   * Returns the Msg to be displayed for the passed in savings amount.
   * 
   * @param connection
   * @param savingsAmount
   * @return
   */
  public String getFreeShippingCartSavingsMsg(Connection connection, double savingsAmount)
    throws Exception
  {
    String cartFeesSavedMsg = ConfigurationUtil.getInstance().getContentWithFilter(connection, COMConstants.FREE_SHIPPING_CONTEXT,
                                                         COMConstants.CART_FEES_SAVED_MESSAGE, null, null);

    AccountProgramMasterVO apmVO = getFreeShippingProgramInfo();
    cartFeesSavedMsg = FieldUtils.replaceAll(cartFeesSavedMsg, "~program_display_name~", apmVO.getDisplayName());
    cartFeesSavedMsg = FieldUtils.replaceAll(cartFeesSavedMsg, "~total_cart_savings~", formatCurrencyString(savingsAmount));    
    
    return cartFeesSavedMsg;    
  }    
  
  /**
   * Formats a String as a Currency Number
   * @param amount
   * @return
   */
  private String formatCurrencyString(double amount)
  {
    DecimalFormat twoDec = new DecimalFormat("#,###,##0.00");
    twoDec.setGroupingUsed(true);
    return twoDec.format(amount);
  }   
}
