package com.ftd.customerordermanagement.dao;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.sql.Connection;

import java.util.HashMap;

import org.w3c.dom.Document;


//import javax.transaction.UserTransaction;

/**
 * ViewDAO
 * 
 * @author Ali Lakhani
 * @version $Id: ViewDAO.java
 */


public class ViewDAO 
{
    private Connection connection;
    private static Logger logger  = new Logger("com.ftd.customerordermanagement.dao.ViewDAO");
    
    
  /**
   * Constructor
   * @param connection Connection
   */
  public ViewDAO(Connection connection)
  {
    this.connection = connection;

  }


  /**
   * method for loading the last/last_50 records viewed
   * 
   * @param customer service representative id
   * @param maxRecords
   * 
   * @return Document containing the values from the cursor 
   * 
   * @throws java.lang.Exception
   */


  public Document getCSRViewed(String csrId, long recordsToBeReturned) 
        throws Exception
  {
        Document searchResults = DOMUtil.getDocument();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("GET_CSR_VIEWED");

        dataRequest.addInputParam("IN_CSR_ID", 		    csrId);
        dataRequest.addInputParam("IN_REC_COUNT",     new Long(recordsToBeReturned));
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      
        searchResults = (Document) dataAccessUtil.execute(dataRequest);      

        return searchResults;

  }

  /**
   * method for loading the csr info that is currently viewing an order
   * 
   * @param session id
   * @param customer service representative id
   * @param entity type
   * @param entity id
   * 
   * @return Document containing the values from the cursor 
   * 
   * @throws java.lang.Exception
   */


  public Document getCSRViewing(String sessionId, String csrId, String entityType, String entityId) 
        throws Exception
  {
        Document searchResults = DOMUtil.getDocument();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("GET_CSR_VIEWING");

        dataRequest.addInputParam("IN_SESSION_ID",    sessionId);
        dataRequest.addInputParam("IN_CSR_ID", 		    csrId);
        dataRequest.addInputParam("IN_ENTITY_TYPE",   entityType);
        dataRequest.addInputParam("IN_ENTITY_ID",     entityId);
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      
        searchResults = (Document) dataAccessUtil.execute(dataRequest);      

        return searchResults;

  }


  /**
   * 
   * @param 
   * @return 
   * @throws java.lang.Exception
   */
  public HashMap deleteCsrViewing(String sessionId) throws Exception
  {
    HashMap searchResults = new HashMap();

    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(connection);
    dataRequest.setStatementID("DELETE_SPECIFIC_CSR_VIEWING");

    dataRequest.addInputParam("IN_SESSION_ID",          sessionId);
        
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
    searchResults = (HashMap) dataAccessUtil.execute(dataRequest);
    return searchResults;    

  }

  /**
   * 
   * @param 
   * @return 
   * @throws java.lang.Exception
   */
  public HashMap deleteCsrViewed(String csrId, String orderGuid, String orderDetailId, String customerId, String sessionId) throws Exception
  {
    HashMap searchResults = new HashMap();

    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(connection);
    dataRequest.setStatementID("DELETE_SPECIFIC_CSR_VIEWED");

    dataRequest.addInputParam("IN_CSR_ID",              csrId);
    dataRequest.addInputParam("IN_ORDER_GUID",          orderGuid);
    dataRequest.addInputParam("IN_ORDER_DETAIL_ID",     orderDetailId);
    dataRequest.addInputParam("IN_CUSTOMER_ID",         customerId);
    dataRequest.addInputParam("IN_SESSION_ID",          sessionId);
        
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
    searchResults = (HashMap) dataAccessUtil.execute(dataRequest);
    return searchResults;    

  }



}