package com.ftd.customerordermanagement.dao;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.vo.CustomerHoldVO;
import com.ftd.customerordermanagement.vo.CustomerPhoneVO;
import com.ftd.customerordermanagement.vo.CustomerVO;
import com.ftd.customerordermanagement.vo.DirectMailVO;
import com.ftd.customerordermanagement.vo.EmailVO;
import com.ftd.customerordermanagement.vo.PrivacyPolicyOptionVO;
import com.ftd.ftdutilities.FTDCAMSUtils;
import com.ftd.ftdutilities.FieldUtils;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.dataaccess.valueobjects.XMLFormat;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.xml.parsers.ParserConfigurationException;




import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/**
 * CustomerDAO
 *
 * This is the DAO that handles loading all customer comments, loading order comments,
 * and inserting customer comments for the Customer Comments functionality.  It will also be used
 * to search for customer specific data, and will pass back the results that will help display
 * search customers, multiple records found, customer account, and customer shopping cart pages.
 *
 * @author Srinivas Makam
 * @version $Id: CustomerDAO.java
 */


public class CustomerDAO
{
    private Connection connection;
    private static Logger logger  = new Logger("com.ftd.customerordermanagement.dao.CustomerDAO");
    private static final String NAME_DELIMITER = ",";

  /*
   * Constructor
   * @param connection Connection
   */
  public CustomerDAO(Connection connection)
  {
    this.connection = connection;
  }

  /**
   * This method loads customer/account history
   * @param customerId
   * @param startNumber
   * @param maxRecords
   * @param managerRole
   * @return hashmap containing XML document for customer history and a count of such records found
   * @throws java.lang.Exception
   */
  public HashMap loadCustomerHistoryXML(String customerId, int position, int maxRecords, String managerRole) throws Exception
  {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("GET_CUSTOMER_HISTORY");
        dataRequest.addInputParam("IN_CUSTOMER_ID", customerId);
        dataRequest.addInputParam("IN_START_POSITION", new Long(position));
        dataRequest.addInputParam("IN_MAX_NUMBER_RETURNED", new Long(maxRecords));

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

        HashMap results = (HashMap)dataAccessUtil.execute(dataRequest);

        return results;

  }

  /**
   * method to search a customer based on the input data (entered on the Search Customers page)
   *
   * @param searchCriteria - A HashMap that contains the value from the request object
   * @param start position
   * @param maxRecords
   *
   * @return HashMap containing a cursor and output parameters
   *
   * @throws java.lang.Exception
   */

  public HashMap searchCustomers(HashMap searchCriteria, long startPosition, long recordsToBeReturned)
          throws Exception
  {
        HashMap searchResults = new HashMap();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("SEARCH_CUSTOMERS");
        
        //PC is no more used 
        /*String pcSeqNumber = "";
        if(searchCriteria.get(COMConstants.SC_PC_MEMBERSHIP) != null && !"".equals((String)searchCriteria.get(COMConstants.SC_PC_MEMBERSHIP)))
        {
        	pcSeqNumber = FTDCAMSUtils.getPCMembershipIdByMembershipNumber((String)searchCriteria.get(COMConstants.SC_PC_MEMBERSHIP));
        }*/

        String sCust = " " + searchCriteria.get(COMConstants.SC_LAST_NAME).toString();
        StringTokenizer stCust = new StringTokenizer(sCust, this.NAME_DELIMITER);
        int cCount = stCust.countTokens();
        if (cCount < 1)
        {
          dataRequest.addInputParam("IN_LAST_NAME", 		      "");
          dataRequest.addInputParam("IN_FIRST_NAME", 		      "");
        }
        else if (cCount < 2)
        {
          dataRequest.addInputParam("IN_LAST_NAME",
              searchCriteria.get(COMConstants.SC_LAST_NAME).toString().trim());
          dataRequest.addInputParam("IN_FIRST_NAME", 		      "");
        }
        else
        {
          dataRequest.addInputParam("IN_LAST_NAME", 		      stCust.nextToken().toString().trim());
          dataRequest.addInputParam("IN_FIRST_NAME", 		      stCust.nextToken().toString().trim());
        }

        //retrieve the phone number
        String sPhoneNumber = searchCriteria.get(COMConstants.SC_PHONE_NUMBER).toString();

        //remove the dashes
        sPhoneNumber = sPhoneNumber.replaceAll("\\-", "");
        //remove the left paranthesis
        sPhoneNumber = sPhoneNumber.replaceAll("\\(",  "");
        //remove the right paranthesis
        sPhoneNumber = sPhoneNumber.replaceAll("\\)",  "");

        dataRequest.addInputParam("IN_PHONE_NUMBER",        sPhoneNumber);
        dataRequest.addInputParam("IN_ZIP_CODE", 		        searchCriteria.get(COMConstants.SC_ZIP_CODE).toString());
        dataRequest.addInputParam("IN_EMAIL_ADDRESS", 	    searchCriteria.get(COMConstants.SC_EMAIL_ADDRESS).toString());
        dataRequest.addInputParam("IN_CC_NUMBER", 		      searchCriteria.get(COMConstants.SC_CC_NUMBER)!=null?searchCriteria.get(COMConstants.SC_CC_NUMBER).toString():"");
        dataRequest.addInputParam("IN_CUSTOMER_ID", 		    searchCriteria.get(COMConstants.SC_CUST_NUMBER).toString());
        dataRequest.addInputParam("IN_MEMBERSHIP_NUMBER",	  searchCriteria.get(COMConstants.SC_MEMBERSHIP_NUMBER).toString());
        dataRequest.addInputParam("IN_ORDER_NUMBER",        searchCriteria.get(COMConstants.SC_ORDER_NUMBER).toString());
        dataRequest.addInputParam("IN_TRACKING_NUMBER",     searchCriteria.get(COMConstants.SC_TRACKING_NUMBER).toString());
        dataRequest.addInputParam("IN_AP_ACCOUNT",          searchCriteria.get(COMConstants.SC_AP_ACCOUNT)!=null?searchCriteria.get(COMConstants.SC_AP_ACCOUNT).toString():"");
        dataRequest.addInputParam("IN_BUYER_INDICATOR",     searchCriteria.get(COMConstants.SC_CUST_IND).toString());
        dataRequest.addInputParam("IN_RECIPIENT_INDICATOR", searchCriteria.get(COMConstants.SC_RECIP_IND).toString());
        dataRequest.addInputParam("IN_START_POSITION", 		  new Long(startPosition));
        dataRequest.addInputParam("IN_MAX_NUMBER_RETURNED", new Long(recordsToBeReturned));
        //dataRequest.addInputParam("IN_PC_MEMBERSHIP",	  pcSeqNumber != null?pcSeqNumber:"");
        dataRequest.addInputParam("IN_PC_MEMBERSHIP", null);
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

        searchResults = (HashMap) dataAccessUtil.execute(dataRequest);

        return searchResults;

  }

  /**
   * method to search a customer account info based on the input data
   *
   * @param searchCriteria - A HashMap that contains the value from the request object
   * @param customer id
   * @param session id
   * @param customer service representative id
   * @param start position
   * @param maxRecords
   *
   * @return HashMap containing cursors and output parameters
   *
   * @throws java.lang.Exception
   */

  public HashMap getCustomerAcct( HashMap searchCriteria, String customerId, String sessionId,
                                  String csrId, long startPosition, long recordsToBeReturned)
          throws Exception
  {
        HashMap searchResults = new HashMap();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("GET_CUSTOMER_ACCT");

        dataRequest.addInputParam("IN_CUSTOMER_ID", 		      customerId);
        dataRequest.addInputParam("IN_ORDER_NUMBER",
            searchCriteria.get(COMConstants.SC_ORDER_NUMBER).toString());
        dataRequest.addInputParam("IN_START_POSITION", 		    new Long(startPosition));
        dataRequest.addInputParam("IN_MAX_NUMBER_RETURNED",   new Long(recordsToBeReturned));
        dataRequest.addInputParam("IN_SESSION_ID", 	  	      sessionId);
        dataRequest.addInputParam("IN_CSR_ID", 		            csrId);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

        searchResults = (HashMap) dataAccessUtil.execute(dataRequest);

        return searchResults;

  }


  /**
   * method to search a customer account info based on the input data
   *
   * @param customer id
   * @param session id
   * @param customer service representative id
   *
   * @return HashMap containing cursors and output parameters
   *
   * @throws java.lang.Exception
   */

  public HashMap getCustomerAcctOnly(String customerId, String sessionId, String csrId)
          throws Exception
  {
        HashMap searchResults = new HashMap();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("GET_CUSTOMER_ACCT_ONLY");

        dataRequest.addInputParam("IN_CUSTOMER_ID", 		      customerId);
        dataRequest.addInputParam("IN_SESSION_ID", 	  	      sessionId);
        dataRequest.addInputParam("IN_CSR_ID", 		            csrId);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

        searchResults = (HashMap) dataAccessUtil.execute(dataRequest);

        return searchResults;

  }


  /**
   * method to search a customer account info based on the input data
   *
   * @param String - customer id
   *
   * @return String - customer name
   *
   * @throws IOException
   * @throws ParserConfiguraitonException
   * @throws SAXException
   * @throws SQLException
   */

  public String getCustomerName(String customerId)
    throws IOException, ParserConfigurationException, SAXException, SQLException
  {
        HashMap searchResults = new HashMap();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("GET_CUSTOMER");

        dataRequest.addInputParam("IN_CUSTOMER_ID",customerId);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

        String custFullName = null;

        CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

        if(rs.next())
        {
            custFullName = rs.getString("customer_first_name")+" "+rs.getString("customer_last_name");
        }

        return custFullName;
  }

    /**
   * method to search a customer account info based on the input data
   *
   * @param String - customer id
   *
   * @return ArrayList - customer info
   *
   * @throws IOException
   * @throws ParserConfiguraitonException
   * @throws SAXException
   * @throws SQLException
   */

  public List getCustomerById(String customerId)
    throws IOException, ParserConfigurationException, SAXException, SQLException, Exception
  {
    CustomerVO custVO = new CustomerVO();
    ArrayList results = new ArrayList();
    CachedResultSet outputs = null;
    boolean isEmpty = true;

    try
    {
      DataRequest dataRequest = new DataRequest();
      dataRequest.setConnection(this.connection);
      dataRequest.setStatementID("GET_CUSTOMER_INFO_BY_ID");
      dataRequest.addInputParam("IN_CUSTOMER_ID",customerId);

      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

      outputs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

      /* populate object */
      while (outputs.next())
      {
        isEmpty = false;
        custVO.setFirstName(outputs.getString("customer_first_name"));
        custVO.setLastName(outputs.getString("customer_last_name"));
        custVO.setBusinessName(outputs.getString("business_name"));
        custVO.setAddress1(outputs.getString("customer_address_1"));
        custVO.setAddress2(outputs.getString("customer_address_2"));
        custVO.setCity(outputs.getString("customer_city"));
        custVO.setState(outputs.getString("customer_state"));
        custVO.setZipCode(outputs.getString("customer_zip_code"));
        custVO.setCountry(outputs.getString("customer_country"));
        custVO.setAddressType(outputs.getString("address_type"));
        custVO.setCounty(outputs.getString("customer_county"));
        custVO.setVipCustomer((outputs.getString("vip_customer")!= null ? FieldUtils.convertStringToBoolean(outputs.getString("vip_customer"))  : false) );
        results.add(custVO);
      }
    }
    catch (Exception e)
    {
      logger.error(e);
      throw e;
    }
    if (isEmpty)
      return null;
    else
      return results;
  }

  /**
   * method to search customer emails based on the input data
   *
   * @param searchCriteria - A HashMap that contains the value from the request object
   * @param start position
   * @param maxRecords
   *
   * @return HashMap containing cursors and output parameters
   *
   * @throws java.lang.Exception
   */

  public HashMap searchCustomerEmails(String customerId, long startPosition, long recordsToBeReturned)
        throws Exception
  {
        HashMap searchResults = new HashMap();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("SEARCH_CUSTOMER_EMAILS");

        dataRequest.addInputParam("IN_CUSTOMER_ID", 	        customerId);
        dataRequest.addInputParam("IN_START_POSITION", 		    new Long(startPosition));
        dataRequest.addInputParam("IN_MAX_NUMBER_RETURNED",   new Long(recordsToBeReturned));

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

        searchResults = (HashMap) dataAccessUtil.execute(dataRequest);

        return searchResults;

  }


  /**
   * method to search customer emails based on the input data
   *
   * @param searchCriteria - A HashMap that contains the value from the request object
   * @param start position
   * @param maxRecords
   *
   * @return HashMap containing cursors and output parameters
   *
   * @throws java.lang.Exception
   */

  public HashMap searchCustomerEmailsUnique(String customerId, long startPosition, long recordsToBeReturned)
        throws Exception
  {
        HashMap searchResults = new HashMap();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("SEARCH_CUSTOMER_EMAILS_UNIQUE");

        dataRequest.addInputParam("IN_CUSTOMER_ID", 	        customerId);
        dataRequest.addInputParam("IN_START_POSITION", 		    new Long(startPosition));
        dataRequest.addInputParam("IN_MAX_NUMBER_RETURNED",   new Long(recordsToBeReturned));

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

        searchResults = (HashMap) dataAccessUtil.execute(dataRequest);

        return searchResults;

  }



  /**
   * method to search customer email info based on the input data
   *
   * @param searchCriteria - A HashMap that contains the value from the request object
   * @param start position
   * @param maxRecords
   *
   * @return HashMap containing cursors and output parameters
   *
   * @throws java.lang.Exception
   */

  public CachedResultSet searchCustomerEmailInfo( String customerId, String emailAddress)
        throws Exception
  {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("SEARCH_CUSTOMER_EMAIL_INFO");

        dataRequest.addInputParam("IN_CUSTOMER_ID", 	        customerId);
        dataRequest.addInputParam("IN_EMAIL_ADDRESS",         emailAddress);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

        CachedResultSet searchResults = (CachedResultSet) dataAccessUtil.execute(dataRequest);

        return searchResults;

  }


  /**
   * method for
   *
   *
   * @return Document containing the values from the cursor
   *
   * @throws java.lang.Exception
   */


  public Document getMembershipInfo(String orderDetailId)
        throws Exception
  {
        Document searchResults = DOMUtil.getDocument();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("GET_MEMBERSHIP_BY_ORDER");
        dataRequest.addInputParam("IN_ORDER_DETAIL_ID", orderDetailId);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

        searchResults = (Document) dataAccessUtil.execute(dataRequest);

        return searchResults;
  }

  

  public List<String> getEmailByCustomerId(String customerId)
  throws Exception {
	  List<String> emailAddressList = new ArrayList<String>();
	  DataRequest dataRequest = new DataRequest();
	  dataRequest.setConnection(this.connection);
	  dataRequest.setStatementID("GET_EMAIL_BY_CUSTOMER_ID_INFO");
	  dataRequest.addInputParam("IN_CUSTOMER_ID", customerId);
	
	  DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
	
	  CachedResultSet searchResults = (CachedResultSet) dataAccessUtil.execute(dataRequest);
	  while(searchResults.next()) {
		  emailAddressList.add(searchResults.getString("email_address"));  
	  }
	
	  return emailAddressList;
  }

  /**
   * method to retrieve all the states.
   *
   * @return Document containing the values of all the states
   *
   * @throws java.lang.Exception
   */


  public CachedResultSet getStates()
        throws Exception
  {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("GET_STATES");

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

        CachedResultSet searchResults = (CachedResultSet) dataAccessUtil.execute(dataRequest);

        return searchResults;
  }



  /**
   * method to retrieve all the COUNTRIES
   *
   * @return Document containing the values of all the states
   *
   * @throws java.lang.Exception
   */


  public CachedResultSet getCountries()
        throws Exception
  {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("GET_COUNTRIES");

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

        CachedResultSet searchResults = (CachedResultSet) dataAccessUtil.execute(dataRequest);

        return searchResults;

  }


  /**
   * method to update the customer info
   *
   * @return HashMap containing the status and message
   *
   * @throws java.lang.Exception
   */


  public HashMap updateCustomer(CustomerVO customerVO)
        throws Exception
  {
        HashMap searchResults = new HashMap();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("UPDATE_CUSTOMER");

        dataRequest.addInputParam("IN_CUSTOMER_ID",					String.valueOf(customerVO.getCustomerId()));
        dataRequest.addInputParam("IN_FIRST_NAME",					customerVO.getFirstName()!=null?customerVO.getFirstName().trim():null);
        dataRequest.addInputParam("IN_LAST_NAME",						customerVO.getLastName()!=null?customerVO.getLastName().trim():null);
        dataRequest.addInputParam("IN_BUSINESS_NAME",				customerVO.getBusinessName()!=null?customerVO.getBusinessName().trim():null);
        dataRequest.addInputParam("IN_ADDRESS_1",    				customerVO.getAddress1());
        dataRequest.addInputParam("IN_ADDRESS_2",    				customerVO.getAddress2());
        dataRequest.addInputParam("IN_CITY",    						customerVO.getCity());
        dataRequest.addInputParam("IN_STATE",    						customerVO.getState());
        dataRequest.addInputParam("IN_ZIP_CODE",    				customerVO.getZipCode());
        dataRequest.addInputParam("IN_COUNTRY",    					customerVO.getCountry());
        dataRequest.addInputParam("IN_ADDRESS_TYPE",    		customerVO.getAddressType());
        dataRequest.addInputParam("IN_VIP_CUSTOMER",    		FieldUtils.convertBooleanToString(customerVO.isVipCustomer()));

        char cPreferredCustomer = customerVO.getPreferredCustomer();
        String sPreferredCustomer = null;
        //if (= String) cPreferredCustomer.toString();
        dataRequest.addInputParam("IN_PREFERRED_CUSTOMER",	sPreferredCustomer);

        char cBuyerInd = customerVO.getBuyerIndicator();
        String sBuyerInd = null;
        //cBuyerInd.toString();
        dataRequest.addInputParam("IN_BUYER_INDICATOR",    	sBuyerInd);

        char cRecipientInd = customerVO.getRecipientIndicator();
        String sRecipientInd = null;
        //cRecipientInd.toString();
        dataRequest.addInputParam("IN_RECIPIENT_INDICATOR",	sRecipientInd);

        dataRequest.addInputParam("IN_ORIGIN_ID",    				customerVO.getOrigin());
        dataRequest.addInputParam("IN_FIRST_ORDER_DATE",    customerVO.getFirstOrderDate());
        dataRequest.addInputParam("IN_UPDATED_BY",    			customerVO.getUpdatedBy());
        dataRequest.addInputParam("IN_COUNTY",    					customerVO.getCounty());
        dataRequest.addInputParam("IN_UPDATE_SCRUB",    		"Y");
        dataRequest.addInputParam("IN_BUSINESS_INFO",    		customerVO.getBusinessInfo());

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

        searchResults = (HashMap) dataAccessUtil.execute(dataRequest);

        return searchResults;

  }


  /**
   * method to update the customer phone info
   *
   * @return HashMap containing the status and message
   *
   * @throws java.lang.Exception
   */


  public HashMap updatePhone(CustomerPhoneVO phoneVO)
        throws Exception
  {
        HashMap searchResults = new HashMap();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("UPDATE_CUSTOMER_PHONES");

        dataRequest.addInputParam("IN_CUSTOMER_ID",			String.valueOf(phoneVO.getCustomerId()));
        dataRequest.addInputParam("IN_PHONE_TYPE",			phoneVO.getPhoneType());
        dataRequest.addInputParam("IN_PHONE_NUMBER",		phoneVO.getPhoneNumber());
        dataRequest.addInputParam("IN_EXTENSION",			phoneVO.getExtension());
        dataRequest.addInputParam("IN_PHONE_NUMBER_TYPE",	phoneVO.getPhoneNumberType());
        dataRequest.addInputParam("IN_SMS_OPT_IN",			phoneVO.getSmsOptIn());
        dataRequest.addInputParam("IN_UPDATED_BY",			phoneVO.getUpdatedBy());
        dataRequest.addInputParam("IN_UPDATE_SCRUB",	"Y");

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

        searchResults = (HashMap) dataAccessUtil.execute(dataRequest);

        return searchResults;

  }


  /**
   * method to update the customer direct mail info
   *
   * @return HashMap containing the status and message
   *
   * @throws java.lang.Exception
   */


  public HashMap updateDirectMail(DirectMailVO dmVO)
        throws Exception
  {
        HashMap searchResults = new HashMap();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("UPDATE_DIRECT_MAIL");

        dataRequest.addInputParam("IN_CUSTOMER_ID",				String.valueOf(dmVO.getCustomerId()));
        dataRequest.addInputParam("IN_COMPANY_ID",				dmVO.getCompanyId());
        dataRequest.addInputParam("IN_UPDATED_BY",				dmVO.getUpdatedBy());
        dataRequest.addInputParam("IN_SUBSCRIBE_STATUS",	dmVO.getStatus());

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

        searchResults = (HashMap) dataAccessUtil.execute(dataRequest);

        return searchResults;

  }



  /**
   * method to update the customer email info
   *
   * @return HashMap containing the status and message
   *
   * @throws java.lang.Exception
   */


  public HashMap updateEmail(EmailVO eVO)
        throws Exception
  {
        HashMap searchResults = new HashMap();
        String nullString = null;
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("UPDATE_UNIQUE_EMAIL");

        logger.debug("IN_CUSTOMER_ID " + String.valueOf(eVO.getCustomerId()));
        logger.debug("IN_COMPANY_ID " +	eVO.getCompanyId());
        logger.debug("IN_EMAIL_ADDRESS " +	eVO.getEmailAddress());
        logger.debug("IN_ACTIVE_INDICATOR " + eVO.getActiveIndicator());
        logger.debug("IN_SUBSCRIBE_STATUS " +	eVO.getSubscribeStatus());
        logger.debug("IN_UPDATED_BY " + 			eVO.getUpdatedBy());
        logger.debug("IN_MOST_RECENT_BY_COMPANY " + 	eVO.getMostRecentByCompany().toUpperCase());
        logger.debug("IN_UPDATE_SCRUB " +	"Y");
        logger.debug("Email = " + eVO.getEmailId());
        logger.debug("statementID = " + dataRequest.getStatementID());

        dataRequest.addInputParam("IN_CUSTOMER_ID",	eVO.getCustomerId()==0?null:String.valueOf(eVO.getCustomerId()));
        dataRequest.addInputParam("IN_COMPANY_ID",							eVO.getCompanyId());
        dataRequest.addInputParam("IN_EMAIL_ADDRESS",						eVO.getEmailAddress());
        dataRequest.addInputParam("IN_ACTIVE_INDICATOR",				eVO.getActiveIndicator());
        dataRequest.addInputParam("IN_SUBSCRIBE_STATUS",				eVO.getSubscribeStatus());
        dataRequest.addInputParam("IN_UPDATED_BY",							eVO.getUpdatedBy());
        dataRequest.addInputParam("IN_MOST_RECENT_BY_COMPANY",	eVO.getMostRecentByCompany().toUpperCase());
        dataRequest.addInputParam("IN_UPDATE_SCRUB",						"Y");
        if (eVO.getEmailId() == 0)
          dataRequest.addInputParam("IO_EMAIL_ID",							null);
        else
          dataRequest.addInputParam("IO_EMAIL_ID",					                String.valueOf(eVO.getEmailId()));

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

        searchResults = (HashMap) dataAccessUtil.execute(dataRequest);

        return searchResults;

  }

  /**
     Delete a customer hold record into the database
     *
     * @param holdVO
     * @throws java.lang.Exception
     */
  public void deleteCustomerHold(CustomerHoldVO holdVO) throws Exception
  {
    DataRequest dataRequest = new DataRequest();

    /* setup store procedure input parameters */
    HashMap inputParams = new HashMap();
    inputParams.put("IN_CUSTOMER_HOLD_ENTITY_ID", holdVO.getEntityId());

    /* build DataRequest object */
    dataRequest.setConnection(connection);
    dataRequest.setStatementID("DELETE_CUSTOMER_HOLD_ENTITIES");
    dataRequest.setInputParams(inputParams);

    /* execute the store prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

    Map outputs = (Map) dataAccessUtil.execute(dataRequest);

    /* read store prodcedure output parameters to determine
     * if the procedure executed successfully */
    String status = (String) outputs.get("OUT_STATUS");
    if(status != null && status.equalsIgnoreCase("N"))
    {
      String message = (String) outputs.get("OUT_MESSAGE");
    }

  }

/* Retrieve all customer hold information for a given customer
 *
     *
     * @param customerId
     * @return
     * @throws java.lang.Exception
     */
  public Document getCustomerHoldInfo(String customerId) throws Exception
  {
    DataRequest dataRequest = new DataRequest();

    Document xml = (Document) DOMUtil.getDocument();

    /* setup store procedure input parameters */
    HashMap inputParams = new HashMap();
    inputParams.put("IN_CUSTOMER_ID", customerId);

    /* build DataRequest object */
    dataRequest.setConnection(connection);
    dataRequest.setStatementID("GET_CUSTOMER_HOLD_CUST_INFO");
    dataRequest.setInputParams(inputParams);

    /* execute the store prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

    HashMap cursors = (HashMap) dataAccessUtil.execute(dataRequest);

    Document custDoc = buildXML(cursors,"OUT_CURSOR_CUSTOMER","customers","customer");
    Document creditDoc = buildXML(cursors,"OUT_CURSOR_CREDIT_CARDS","credit_cards","credit_card");
    Document phoneDoc = buildXML(cursors,"OUT_CURSOR_PHONES","phonenumbers","phonenumber");
    Document memberDoc = buildXML(cursors,"OUT_CURSOR_MEMBERSHIPS","memberships","membership");
    Document emailDoc = buildXML(cursors,"OUT_CURSOR_EMAILS","emails","email");
    Document apDoc = buildXML(cursors,"OUT_CURSOR_AP_ACCOUNTS", "ap_accounts", "ap_account");

    DOMUtil.addSection(xml,custDoc.getChildNodes());
    DOMUtil.addSection(xml,creditDoc.getChildNodes());
    DOMUtil.addSection(xml,phoneDoc.getChildNodes());
    DOMUtil.addSection(xml,memberDoc.getChildNodes());
    DOMUtil.addSection(xml,emailDoc.getChildNodes());
    DOMUtil.addSection(xml,apDoc.getChildNodes());

    return xml;


  }

  /* Retrieve customer hold information related to a specific order.
   *
     *
     * @param orderDetailId
     * @param customerType
     * @return
     * @throws java.lang.Exception
     */
  public Document getCustomerHoldInfo(String orderDetailId, String customerType) throws Exception
  {
    DataRequest dataRequest = new DataRequest();

    Document xml = (Document) DOMUtil.getDocument();

    /* setup store procedure input parameters */
    HashMap inputParams = new HashMap();
    inputParams.put("IN_ORDER_DETAIL_ID", new Long(orderDetailId));
    inputParams.put("IN_CUTOMER_TYPE", customerType);

    /* build DataRequest object */
    dataRequest.setConnection(connection);
    dataRequest.setStatementID("GET_CUSTOMER_HOLD_ORDER_INFO");
    dataRequest.setInputParams(inputParams);

    /* execute the store procedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

    HashMap cursors = (HashMap) dataAccessUtil.execute(dataRequest);

    Document custDoc = buildXML(cursors,"OUT_CURSOR_CUSTOMER","customers","customer");
    Document creditDoc = buildXML(cursors,"OUT_CURSOR_CREDIT_CARDS","credit_cards","credit_card");
    Document phoneDoc = buildXML(cursors,"OUT_CURSOR_PHONES","phonenumbers","phonenumber");
    Document memberDoc = buildXML(cursors,"OUT_CURSOR_MEMBERSHIPS","memberships","membership");
    Document emailDoc = buildXML(cursors,"OUT_CURSOR_EMAILS","emails","email");
    Document lossPrevDoc = buildXML(cursors,"OUT_CURSOR_LOSS_PREVENTION","lps","lp");
    Document apDoc = buildXML(cursors,"OUT_CURSOR_AP_ACCOUNTS", "ap_accounts", "ap_account");

    DOMUtil.addSection(xml,custDoc.getChildNodes());
    DOMUtil.addSection(xml,creditDoc.getChildNodes());
    DOMUtil.addSection(xml,phoneDoc.getChildNodes());
    DOMUtil.addSection(xml,memberDoc.getChildNodes());
    DOMUtil.addSection(xml,emailDoc.getChildNodes());
    DOMUtil.addSection(xml,lossPrevDoc.getChildNodes());
    DOMUtil.addSection(xml,apDoc.getChildNodes());

    return xml;
  }

  /**
     * Get a list of held orders for a given customer
     *
     * @param customerId
     * @return
     * @throws java.io.IOException
     * @throws javax.xml.parsers.ParserConfigurationException
     * @throws org.xml.sax.SAXException
     * @throws java.sql.SQLException
     */
  public List getHeldOrders(String customerId) throws IOException, ParserConfigurationException, SAXException, SQLException
  {
    DataRequest dataRequest = new DataRequest();

    List orderList = new ArrayList();

    /* setup store procedure input parameters */
    HashMap inputParams = new HashMap();
    inputParams.put("IN_CUSTOMER_ID", customerId);


    /* build DataRequest object */
    dataRequest.setConnection(connection);
    dataRequest.setStatementID("GET_HELD_ORDERS");
    dataRequest.setInputParams(inputParams);

    /* execute the store prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

    CachedResultSet outputs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

    while (outputs.next())
    {
      orderList.add(outputs.getString("order_detail_id"));
    }

    return orderList;
  }


  public void deleteHeldOrders(String orderDetailid) throws IOException, ParserConfigurationException, SAXException, SQLException, Exception
  {
    DataRequest dataRequest = new DataRequest();

    List orderList = new ArrayList();

    /* setup store procedure input parameters */
    HashMap inputParams = new HashMap();
    inputParams.put("IN_ORDER_DETAIL_ID", orderDetailid);


    /* build DataRequest object */
    dataRequest.setConnection(connection);
    dataRequest.setStatementID("DELETE_HELD_ORDERS");
    dataRequest.setInputParams(inputParams);

    /* execute the store prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

    Map outputs = (Map) dataAccessUtil.execute(dataRequest);

    /* read store prodcedure output parameters to determine
     * if the procedure executed successfully */
    String status = (String) outputs.get("OUT_STATUS");
    if(status != null && status.equalsIgnoreCase("N"))
    {
      String message = (String) outputs.get("OUT_MESSAGE");
      throw new Exception(message);
    }
  }

  /**
   * Calls the hold reasons stored procedure that returns the all the occasions types.
   *
   * @param none
   * @return XML Document
   */
  public Document getHoldReasonsXML() throws IOException, ParserConfigurationException, SAXException, SQLException
  {
    DataRequest dataRequest = new DataRequest();


    /* build DataRequest object */
    dataRequest.setConnection(connection);
    dataRequest.setStatementID("GET_CUST_HOLD_REASON_VAL_LIST");

    /* execute the store prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

    Document doc = (Document)dataAccessUtil.execute(dataRequest);

    return doc;
  }

  /**
     Insert a customer hold record into the database
     *
     * @param holdVO
     * @throws java.lang.Exception
     */
  public void insertCustomerHold(CustomerHoldVO holdVO) throws Exception
  {
    DataRequest dataRequest = new DataRequest();

    /* setup store procedure input parameters */
    HashMap inputParams = new HashMap();
    inputParams.put("IN_ENTITY_TYPE", holdVO.getEntityType());
    inputParams.put("IN_HOLD_VALUE_1", holdVO.getHoldValue1());
    inputParams.put("IN_HOLD_VALUE_2", holdVO.getHoldValue2());
    inputParams.put("IN_GLOBAL_ADD_ON", holdVO.getGlobalAddonOn());
    inputParams.put("IN_HOLD_REASON_CODE", holdVO.getHoldReasonCode());
    inputParams.put("IN_CREATED_BY", holdVO.getUpdatedBy());

    /* build DataRequest object */
    dataRequest.setConnection(connection);
    dataRequest.setStatementID("INSERT_CUSTOMER_HOLD_ENTITIES");
    dataRequest.setInputParams(inputParams);

    /* execute the store prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

    Map outputs = (Map) dataAccessUtil.execute(dataRequest);

    /* read store prodcedure output parameters to determine
     * if the procedure executed successfully */
    String status = (String) outputs.get("OUT_STATUS");
    if(status != null && status.equalsIgnoreCase("N"))
    {
      String message = (String) outputs.get("OUT_MESSAGE");
      throw new Exception(message);
    }

  }

  /**
     * Get list of hold orders for a given customer and order guid
     *
     * @param customerId
     * @param orderGuid
     * @return
     * @throws java.io.IOException
     * @throws javax.xml.parsers.ParserConfigurationException
     * @throws org.xml.sax.SAXException
     * @throws java.sql.SQLException
     */
  public List getHeldOrders(String customerId,String orderGuid) throws IOException, ParserConfigurationException, SAXException, SQLException
  {
    DataRequest dataRequest = new DataRequest();

    List orderList = new ArrayList();

    /* setup store procedure input parameters */
    HashMap inputParams = new HashMap();
    inputParams.put("IN_CUSTOMER_ID", customerId);
    inputParams.put("IN_ORDER_GUID", orderGuid);


    /* build DataRequest object */
    dataRequest.setConnection(connection);
    dataRequest.setStatementID("GET_HELD_ORDERS_BY_GUID");
    dataRequest.setInputParams(inputParams);

    /* execute the store prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

    CachedResultSet outputs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

    while (outputs.next())
    {
      orderList.add(outputs.getString("order_detail_id"));
    }

    return orderList;
  }

  /**
     * Get the customer info by orderGuid
     *
     * @param orderGuid
     * @return CustomerVO
     * @throws java.io.IOException
     * @throws javax.xml.parsers.ParserConfigurationException
     * @throws org.xml.sax.SAXException
     * @throws java.sql.SQLException
     */
  public CustomerVO getCustomerByOrder(String orderGuid)
    throws IOException, ParserConfigurationException, SAXException, SQLException
  {
    DataRequest dataRequest = new DataRequest();
    CustomerVO cVO = new CustomerVO();
    dataRequest.setConnection(this.connection);
    HashMap inputParams = new HashMap();
    inputParams.put("IN_ORDER_GUID", orderGuid);

    /* build DataRequest object */
    dataRequest.setConnection(connection);
    dataRequest.setStatementID("GET_CUST_INFO_BY_ORDER");
    dataRequest.setInputParams(inputParams);

    /* execute the store prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

    CachedResultSet outputs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

    while (outputs.next())
    {
      cVO.setAddress1(outputs.getString("address_1"));
      cVO.setCustomerId(outputs.getLong("customer_id"));
      cVO.setZipCode(outputs.getString("zip_code"));
      cVO.setState(outputs.getString("state"));
      cVO.setCompanyId(outputs.getString("company_id"));
    }
    return cVO;
  }

    /**
   * This is a wrapper for CLEAN.CUSTOMER_QUERY_PKG.GET_CUSTOMER_EMAIL_INFO.
   * @throws java.lang.Exception
   * @return EmailVO
   * @param companyId
   * @param customerId
   */
     public EmailVO getCustomerEmailInfo(long customerId, String companyId) throws Exception
    {
         CachedResultSet results = null;
         EmailVO email = null;   

         DataRequest dataRequest = new DataRequest();
         HashMap inParms = new HashMap();
        
         inParms.put("IN_CUSTOMER_ID", new Long(customerId));
         inParms.put("IN_COMPANY_ID", companyId);         
        
         dataRequest.setConnection(connection);
         dataRequest.setStatementID( "GET_CUSTOMER_EMAIL_INFO" );
         dataRequest.setInputParams(inParms);
         
         DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
         
         results = (CachedResultSet) dataAccessUtil.execute(dataRequest);
         if( results.next() )
         {
              if (results.getObject(3) != null)
              {
                email = new EmailVO();
                email.setEmailAddress(results.getObject(3).toString());
                if(results.getObject("subscribe_status") != null)
                {
                    email.setSubscribeStatus(results.getObject("subscribe_status").toString());                    
                }
              }
         }  
         return email;         
    }  


  /**
   * This method is a wrapper for the SP_GET_CUSTOMER SP.   
   * It populates a customer VO based on the returned record set.
   * 
   * @param String - customerId
   * @return CustomerVO
   */
  public List getCustomerPhones(long customerId) throws Exception
  {
    DataRequest dataRequest = new DataRequest();

    CachedResultSet outputs = null;
    

      /* setup store procedure input parameters */
      HashMap inputParams = new HashMap();
      inputParams.put("CUSTOMER_ID", Long.toString(customerId));
      
      /* build DataRequest object */
      dataRequest.setConnection(connection);
      dataRequest.setStatementID("GET_CUSTOMER_PHONES");
      dataRequest.setInputParams(inputParams);
      
      /* execute the store prodcedure */
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
      CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
      
      List phoneList = new ArrayList();
      
      while(rs.next())
      {
          CustomerPhoneVO phoneVO = new CustomerPhoneVO();
          phoneVO.setExtension((String)rs.getObject("customer_extension"));
          phoneVO.setPhoneNumber((String)rs.getObject("customer_phone_number"));
          phoneVO.setPhoneType((String)rs.getObject("customer_phone_type"));
          phoneVO.setPhoneNumberType((String)rs.getObject("cus_phone_number_type"));
          phoneVO.setSmsOptIn((String)rs.getObject("customer_sms_opt_in"));
          
          phoneList.add(phoneVO);
      }
      
      return phoneList;

      
  }

    /**
     * This method will check if a customer is a preferred customer.  If they are, it will return a 'Y'
     * 
     * @throws java.lang.Exception
     * @param String customerId
     */

    public String isPreferredCustomer(String customerId) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("IS_PREFERRED_CUSTOMER");
        dataRequest.addInputParam("IN_CUSTOMER_ID", customerId);
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        String searchResults = (String) dataAccessUtil.execute(dataRequest);
        
        return searchResults;

    }
    
/* This proc is used to join mutliple cursors into one XML document.
     *
     * @param outMap
     * @param cursorName
     * @param topName
     * @param bottomName
     * @return
     * @throws java.lang.Exception
     */
private Document buildXML(HashMap outMap, String cursorName, String topName, String bottomName) throws Exception
{
    CachedResultSet rs = (CachedResultSet) outMap.get(cursorName);

    Document doc =  null;
    XMLFormat xmlFormat = new XMLFormat();
    xmlFormat.setAttribute("type", "element");
    xmlFormat.setAttribute("top", topName);
    xmlFormat.setAttribute("bottom", bottomName );

    doc = (Document) DOMUtil.convertToXMLDOM(rs, xmlFormat);

    return doc;
  }

  /**
   * This method will check if the cart has any orders that have the source code tied to a preferred partner. 
   * If it is, it will return true; else it will return false
   * 
   * @throws java.lang.Exception
   * @param String orderGuid
   * @return boolean
   */
  
  public boolean cartHasAPreferredPartnerOrder(String orderGuid) throws Exception
  {
    boolean cartHasAPreferredPartnerOrder = false;
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(this.connection);
    dataRequest.setStatementID("CART_HAS_PREF_PARTNER_ORDER");
    dataRequest.addInputParam("IN_ORDER_GUID", orderGuid);
    
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    String cartHasAPreferredPartnerOrderString = (String) dataAccessUtil.execute(dataRequest);
    
    if(cartHasAPreferredPartnerOrderString != null && cartHasAPreferredPartnerOrderString.equalsIgnoreCase("Y"))
    {        
        cartHasAPreferredPartnerOrder = true;    
    }
    return cartHasAPreferredPartnerOrder;
  }
  
  
  /**
   * method to retrieve validated customers and orders for a given call log id
   *
   * @param callLogId - call log id for which to retrieve the validated customer / order data
   * @return HashMap containing a cursor and output parameters
   * @throws java.lang.Exception
   */

  public HashMap getValidatedPrivacyPolicy(String callLogId) throws Exception
  {
    HashMap searchResults = new HashMap();
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(this.connection);
    dataRequest.setStatementID("GET_VALIDATED_PRIVACY_POLICY");
    dataRequest.addInputParam("IN_CALL_LOG_ID", callLogId);
    
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    searchResults = (HashMap) dataAccessUtil.execute(dataRequest);
    
    return searchResults;

  }


  /**
   * Method to retrieve PPV data based on customer id and order detail id. 
   * This method will retrieve all the data to display PPV popup
   *
   * @param customerId - customer id from COM
   * @param orderDetailId - order detail id of the order being currently viewed
   * @return HashMap containing a cursor and output parameters
   * @throws java.lang.Exception
   */

  public HashMap getPrivacyPolicy(String customerId, String orderDetailId) throws Exception
  {
    HashMap searchResults = new HashMap();
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(this.connection);
    dataRequest.setStatementID("GET_PRIVACY_POLICY");
    dataRequest.addInputParam("IN_CUSTOMER_ID", customerId);
    dataRequest.addInputParam("IN_ORDER_DETAIL_ID", orderDetailId==null||orderDetailId.equalsIgnoreCase("")?null:orderDetailId);
    
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    searchResults = (HashMap) dataAccessUtil.execute(dataRequest);
    
    return searchResults;

  }

  /**
   * method to retrieve ALL privacy policy options
   *
   * @return ArrayList containing a PrivacyPolicyOptionVO
   * @throws java.lang.Exception
   */

  public ArrayList getPrivacyPolicyOptions() throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    ArrayList privacyPolicyOptions = new ArrayList();

    dataRequest.setConnection(this.connection);
    dataRequest.setStatementID("GET_ALL_PRIVACY_POLICY_OPTION");
    
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
   
    BigDecimal  bDisplayOrderSeqNum;
    
    while(rs.next())
    {
      PrivacyPolicyOptionVO ppoVO = new PrivacyPolicyOptionVO();
      ppoVO.setPrivacyPolicyOptionCode((String)rs.getString("privacy_policy_option_code"));
      ppoVO.setPrivacyPolicyOptTypeDesc((String)rs.getString("privacy_policy_opt_type_desc"));
      ppoVO.setPrivacyPolicyOptDetailDesc((String)rs.getString("privacy_policy_opt_detail_desc"));
      ppoVO.setBypassPrivPolicyVerifFlag((String)rs.getString("bypass_priv_policy_verif_flag"));
      ppoVO.setDisplayOrderSeqNum(0);
      if (rs.getObject("display_order_seq_num") != null)
      {
        bDisplayOrderSeqNum = (BigDecimal)rs.getObject("display_order_seq_num");
        ppoVO.setDisplayOrderSeqNum(Long.valueOf(bDisplayOrderSeqNum.toString()).longValue());
      }
     
      privacyPolicyOptions.add(ppoVO);
    }
    
    return privacyPolicyOptions;

  }


  /**
   * method to insert the privacy policy verification record
   *
   * @return void
   * @throws java.lang.Exception
   */
  public void insertPrivacyPolicyVerification(String customerId, String orderDetailId, String csrId, String callLogId, PrivacyPolicyOptionVO ppoVO) throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(this.connection);
    dataRequest.setStatementID("INSERT_PRIVACY_POLICY_VERF");
    dataRequest.addInputParam("IN_CALL_LOG_ID", callLogId);
    dataRequest.addInputParam("IN_PRIVACY_POLICY_OPTION_CODE", ppoVO.getPrivacyPolicyOptionCode());
    dataRequest.addInputParam("IN_CUSTOMER_ID", customerId);
    dataRequest.addInputParam("IN_ORDER_DETAIL_ID", orderDetailId);
    dataRequest.addInputParam("IN_CSR_ID", csrId);

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    Map outputs = (Map) dataAccessUtil.execute(dataRequest);

    // check the status to ensure that the procedure executed successfully
    String status = (String) outputs.get("OUT_STATUS");
    if (status != null && status.equalsIgnoreCase("N"))
    {
      String message = (String) outputs.get("OUT_MESSAGE");
      throw new Exception(message);
    }


  }
  
  
    /**
     * Retrieve preferred partners associated to customer
     *
     * @param String customer id    
     * @return CachedResultSet 
     * @throws java.lang.Exception
     */
    public CachedResultSet getPreferredPartners(String customerId)
            throws Exception
    {
          DataRequest dataRequest = new DataRequest();
          dataRequest.setConnection(this.connection);
          dataRequest.setStatementID("GET_PREFERRED_PARTNERS");
          dataRequest.addInputParam("IN_CUSTOMER_ID", customerId);
         
          DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
          CachedResultSet preferredPartners = (CachedResultSet) dataAccessUtil.execute(dataRequest);
        
          return preferredPartners;
    }
    
    public String getOrderDetailsByMercentOrdNumber(String orderNumber) throws Exception{
    	
    	logger.debug("getMercentOrderDetails()");
    	DataRequest dataRequest = new DataRequest();
    	dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("GET_MERCENT_ORDER_NUMBER");
        dataRequest.addInputParam("IN_CHANNEL_ORDER_ID", orderNumber);
        DataAccessUtil dau = DataAccessUtil.getInstance();
        String result = (String)dau.execute(dataRequest);
        String masterOrderNumber = null;
        // Only one record (or none) should return.
        if (result!=null && !result.isEmpty() ) {
        	 masterOrderNumber = result;        	        	        	
        }
        
        return masterOrderNumber;
    }
    
    /** Get Pro Order numbers for a given sequence number/ web order number match.
	 * @param webOrderNumber
	 * @return
	 * @throws Exception
	 */
	public CachedResultSet getProOrders(String webOrderNumber) throws Exception {
		DataRequest dataRequest = new DataRequest();
		
		dataRequest.setConnection(this.connection);
		dataRequest.setStatementID("GET_PRO_ORDERS");
		dataRequest.addInputParam("IN_PRO_ORDER_NUMBER", webOrderNumber);

		DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
		return (CachedResultSet) dataAccessUtil.execute(dataRequest);	
	}

	
	public HashMap searchProCustomer(String webOrderNumber,long firstRecordPos, long lastRecordPos) throws Exception {
		DataRequest dataRequest = new DataRequest();
		
		dataRequest.setConnection(this.connection);
		dataRequest.setStatementID("SEARCH_PRO_CUSTOMER");
		dataRequest.addInputParam("IN_PRO_ORDER_NUMBER", webOrderNumber);
		dataRequest.addInputParam("IN_START_POSITION", new Long(firstRecordPos));
		dataRequest.addInputParam("IN_MAX_NUMBER_RETURNED", new Long(lastRecordPos));
	
		DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
		return (HashMap) dataAccessUtil.execute(dataRequest);	
	}
    
    
    
}
