package com.ftd.customerordermanagement.api;

import com.ftd.customerordermanagement.to.ResultTO;
import com.ftd.customerordermanagement.to.FullRefundTO;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;
import javax.ejb.EJBObject;
import java.rmi.RemoteException;

public interface RefundAPI extends EJBObject  {
  ResultTO postFullRemainingRefund(FullRefundTO to) throws RemoteException;
}