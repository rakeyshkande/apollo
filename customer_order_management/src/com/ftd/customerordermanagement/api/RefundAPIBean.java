package com.ftd.customerordermanagement.api;

import com.ftd.customerordermanagement.facade.RefundFacade;
import com.ftd.customerordermanagement.to.ResultTO;
import com.ftd.customerordermanagement.to.FullRefundTO;
import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import java.sql.Connection;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.xml.sax.SAXException;


public class RefundAPIBean implements SessionBean  {
  private Connection conn;
  private Logger logger;

  public void ejbCreate() {
  }

  public void ejbActivate() {
  }

  public void ejbPassivate() {
  }

  public void ejbRemove() {
  }

  public void setSessionContext(SessionContext ctx) {
    logger = new Logger("com.ftd.customerordermanagement.api.RefundAPIBean");
  }

  /**
   * 
   * @param refund
   * @return
   */
  @Deprecated
  public ResultTO postFullRemainingRefund(FullRefundTO refund) {
    ResultTO result = new ResultTO();  // Assume success
    try {
      logger.debug("postFullRemainingRefund invoked for order detail ID: " + refund.getOrderDetailId());
      conn = getDBConnection();
      RefundFacade rf = new RefundFacade(conn);
      rf.postFullRemainingRefund(refund);
    } catch (Exception e) {
      logger.error(e);
      result.setSuccess(false); // Failure
      result.setErrorString(e.getMessage());  
    } finally {
      try {
        if (conn != null) conn.close();
      } catch (Exception e) {
        //EMPTY
      }
    }
    return result;
  }
  

	/**
	 * Obtain connectivity with the database
   */
	private Connection getDBConnection()
		throws IOException, ParserConfigurationException, SAXException, TransformerException, 
               Exception
	{
		Connection conn = null;
		conn = DataSourceUtil.getInstance().getConnection(
				   ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE,
															   COMConstants.DATASOURCE_NAME));

		return conn;
	}
  

}