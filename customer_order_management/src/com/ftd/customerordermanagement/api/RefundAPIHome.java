package com.ftd.customerordermanagement.api;
import javax.ejb.EJBHome;
import java.rmi.RemoteException;
import javax.ejb.CreateException;

public interface RefundAPIHome extends EJBHome  {
  RefundAPI create() throws RemoteException, CreateException;
}