package com.ftd.customerordermanagement.api;
import javax.ejb.EJBLocalHome;
import javax.ejb.CreateException;

public interface RefundAPILocalHome extends EJBLocalHome  {
  RefundAPILocal create() throws CreateException;
}