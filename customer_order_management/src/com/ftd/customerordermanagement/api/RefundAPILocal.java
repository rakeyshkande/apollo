package com.ftd.customerordermanagement.api;

import com.ftd.customerordermanagement.to.ResultTO;
import com.ftd.customerordermanagement.to.FullRefundTO;
import javax.ejb.EJBLocalObject;

public interface RefundAPILocal extends EJBLocalObject  {
  ResultTO postFullRemainingRefund(FullRefundTO to);
}
