package com.ftd.customerordermanagement.actions;

import com.ftd.customerordermanagement.bo.CSDnisBO;
import com.ftd.customerordermanagement.bo.CallReasonCodeBO;
import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.util.CallTimer;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;




import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/**
 * This class is used to obtain information for the Call Reason Code screen as
 * well as submitting a reason code for a call and stopping the Call Timer.
 *
 * @author Matt Wilcoxen
 */

public class CallReasonCodeAction extends Action 
{
	private Logger logger = new Logger("com.ftd.customerordermanagement.actions.CallReasonCodeAction");

    //request parameters:
	private static final String R_CALL_REASON_CODE = "call_reason_code";
	private static final String R_CALL_REASON_CODE_TYPE = "call_reason_code_type";
	private static final String R_CUSTOMER_ID = "customer_id";
	private static final String R_CALL_LOG_ID = "call_log_id";

    private static final String ERROR_PAGE = "errorPage";

	/**
	 * This is the Call Reason Code Action called from the Struts framework.
     *     1. get db connection
     *     2. get parameters
     *     3. get call reason code list, if call reason code not supplied
     *     4. go to Call Reason Code screen
     *     5. stop the timer with the supplied call reason
     *     6. go to the "main menu, i.e., customer service menu"
	 * 
	 * @param mapping            - ActionMapping used to select this instance
	 * @param form               - ActionForm (optional) bean for this request
	 * @param request            - HTTP Request we are processing
	 * @param response           - HTTP Response we are processing
     * 
	 * @return forwarding action - next action to "process" request
     * 
	 * @throws IOException
	 * @throws ServletException
	 */
	public  ActionForward execute(ActionMapping mapping, ActionForm form, 
                                  HttpServletRequest request, HttpServletResponse response)
		throws IOException, ServletException
	{
        Connection conn = null;
        ConfigurationUtil cu = null;
		Document callReasonCodeXMLData = null;
		String callPage = null;
		String callPageURL = null;
        ActionForward forward = null;

		try
		{
            cu = ConfigurationUtil.getInstance();

            // 1. get db connection
			conn = getDBConnection();

            // 2. get parameters
            String security_token     = request.getParameter(COMConstants.SEC_TOKEN);
			String callReasonCode     = request.getParameter(R_CALL_REASON_CODE);
			String callReasonCodeType = request.getParameter(R_CALL_REASON_CODE_TYPE);
			String customerId         = "";

            CallReasonCodeBO crcBO = new CallReasonCodeBO(conn);

            // 3. get call reason code list, if call reason code not supplied
            if(callReasonCode == null  ||  callReasonCode.trim().length() == 0  ||
               callReasonCodeType == null  ||  callReasonCodeType.trim().length() == 0)
            {
    			customerId = request.getParameter(R_CUSTOMER_ID);
        		callReasonCodeXMLData = (Document) crcBO.getReasonCodeData(customerId);
                if(logger.isDebugEnabled())
                {
                    //IMPORTANT:  Because StringWriter contents are NOT removed even when the flush
                    //            method is executed, make StringWriter go away through scope control.
                    //            Both the flush and close methods were tested with but were of non-affect.
                    StringWriter sw = new StringWriter();       //string representation of xml document
    //                DOMUtil.print((Document) callReasonCodeXMLData,new PrintWriter(sw));
                    DOMUtil.print(callReasonCodeXMLData,new PrintWriter(sw));
                    logger.debug("execute(): call reason code data DOM = \n" + sw.toString());
                }
                // 4. go to Call Reason Code screen
                File xslFile = getXSL(mapping, request, COMConstants.XSL_CALL_REASON_CODE);
                forward = mapping.findForward(COMConstants.XSL_CALL_REASON_CODE);
                String xslFilePathAndName = forward.getPath(); //get real file name
                
                // Change to client side transform
                TraxUtil.getInstance().getInstance().transform(request, response, callReasonCodeXMLData, 
                                    xslFile, xslFilePathAndName, (HashMap)request.getAttribute(COMConstants.CONS_APP_PARAMETERS));
                return null;
            }
            else
            {
                // 5. stop the timer with the supplied call reason
                HashMap parameters = (HashMap) request.getAttribute(COMConstants.CONS_APP_PARAMETERS);
    			String callLogId = (String) parameters.get(COMConstants.TIMER_CALL_LOG_ID);
                crcBO.setReasonCode(callReasonCode,callReasonCodeType,callLogId);

                // 6. go to the "main menu, i.e., customer service menu"
                callPageURL = cu.getFrpGlobalParm(COMConstants.COM_CONTEXT,COMConstants.CONS_MAIN_MENU_URL)
                    + "?adminAction=customerService"
                    + "&context=" + request.getParameter(COMConstants.CONTEXT)
                    + "&securitytoken=" + request.getParameter(COMConstants.SEC_TOKEN);
                
				response.sendRedirect(callPageURL);
                return null;
            }
		}
		catch (ClassNotFoundException  cnfe)
		{
			logger.error(cnfe);
            callPage = ERROR_PAGE;
		}
		catch (IOException  ioe)
		{
			logger.error(ioe);
            callPage = ERROR_PAGE;
		}
		catch (ParserConfigurationException  pcex)
		{
			logger.error(pcex);
            callPage = ERROR_PAGE;
		}
		catch (SAXException  sec)
		{
			logger.error(sec);
            callPage = ERROR_PAGE;
		}
		catch (SQLException  sqle)
		{
			logger.error(sqle);
            callPage = ERROR_PAGE;
		}
		catch (TransformerException  tex)
		{
			logger.error(tex);
            callPage = ERROR_PAGE;
		}
		catch (Throwable  t)
		{
			logger.error(t);
            callPage = ERROR_PAGE;
		}
		finally
		{
			try
			{
				conn.close();
			}
			catch (SQLException se)
			{
				logger.error(se);
                callPage = ERROR_PAGE;
			}
		}

        if(callPage != null  &&  callPage.trim().length() > 0)
        {
            return forward = mapping.findForward(callPage);
        }
        else
        {
            return null;
        }
	}


	/**
	 * Obtain connectivity with the database
	 * 
	 * @param none
     * 
	 * @return Connection - db connection
     * 
     * @throws Exception
     * @throws IOException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws TransformerException
     * @throws XSLException
	 */
	private Connection getDBConnection()
		throws IOException, ParserConfigurationException, SAXException, TransformerException, 
               Exception
	{
		Connection conn = null;
		conn = DataSourceUtil.getInstance().getConnection(
				   ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE,
															   COMConstants.DATASOURCE_NAME));

		return conn;
	}


	/**
	 * Retrieve name and location of XSL file
     *     1. get file lookup name
     *     2. get real file name
     *     3. get xsl file location
	 * 
	 * @param ActionMapping      - Stuts mapping used for actionforward lookup
     * @param HttpServletRequest - user request
	 * @param String             - call reason code xsl file name
     * 
	 * @return File              - XSL File name with real path
     * 
	 * @throws none
	 */
    public File getXSL(ActionMapping mapping, HttpServletRequest request, String call_reason_code_file)
	{
		File XSLFile = null;
		String XSLFileLookUpName = null;
		String XSLFileName = null;

        // 1. get file lookup name
        XSLFileLookUpName = call_reason_code_file;

        // 2. get real file name
        ActionForward forward = mapping.findForward(XSLFileLookUpName);
        XSLFileName = forward.getPath();

        // 3. get xsl file location
        XSLFile = new File(this.getServlet().getServletContext().getRealPath(XSLFileName));
        return XSLFile;
    }

}