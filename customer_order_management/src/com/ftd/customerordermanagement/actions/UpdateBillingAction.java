package com.ftd.customerordermanagement.actions;

import com.ftd.customerordermanagement.bo.OrderBO;
import com.ftd.customerordermanagement.bo.UpdateBillingBO;
import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.dao.LockDAO;
import com.ftd.customerordermanagement.dao.OrderDAO;
import com.ftd.customerordermanagement.dao.PaymentDAO;
import com.ftd.customerordermanagement.util.BasePageBuilder;
import com.ftd.customerordermanagement.vo.PaymentVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.handlers.GlobalParmHandler;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityManager;
import com.ftd.security.cache.vo.UserInfo;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;


import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

import org.xml.sax.SAXException;
 
public final class UpdateBillingAction extends Action 
{
    private static Logger logger  = new Logger("com.ftd.customerordermanagement.actions.UpdateBillingAction");
    private static String manual_auth_result = "AP";
    private static String editable = "editable";
    private static String none = "none";
    private static String CUSTOMER_ORDER_SEARCH = "CustomerOrderSearch";
    private HashMap pageData = new HashMap();
    private HashMap returnHash = new HashMap();
    private static final String DATA_ROOT_NODE = "root";

  /**
   * This is the main action called from the Struts framework.
   * @param mapping The ActionMapping used to select this instance.
   * @param form The optional ActionForm bean for this request.
   * @param request The HTTP Request we are processing.
   * @param response The HTTP Response we are processing.
   */
  public  ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, 
                                HttpServletResponse response) 
          
  {
    ActionForward forward = null;
    Connection conn = null;
    String path = null;
    String xslName = "updateBilling";
		boolean redirect = false;
        
    try
    {
      String action = request.getParameter(COMConstants.PAGE_ACTION);
      String extOrdNum = request.getParameter(COMConstants.EXTERNAL_ORDER_NUMBER);
      String scOrdNum = request.getParameter(COMConstants.SC_ORDER_NUMBER);
      String mstOrdNum = request.getParameter(COMConstants.MASTER_ORDER_NUMBER);
      String orderDetailId = request.getParameter(COMConstants.ORDER_DETAIL_ID);
      String orderGuid = request.getParameter(COMConstants.ORDER_GUID);
      
      //Connection/Database info
			conn = getDBConnection();
      
      //For an UPDATE action, update the billing info and redirect to the
      //Recipient Order Page
      //If not UPDATE, retrieve the Payment Information and return
      if(action.equalsIgnoreCase(COMConstants.CONS_UPDATE))
      {
        this.UpdateBilling(request, conn);
        
        //Release the lock by Utilizing the SecurityManger to obtain the csrId
        ConfigurationUtil cu = ConfigurationUtil.getInstance();
        //get csr Id
        String security = (String) cu.getProperty(COMConstants.PROPERTY_FILE,COMConstants.SECURITY_IS_ON);
        boolean securityIsOn = security.equalsIgnoreCase("true") ? true : false;
        String sessionId   = request.getParameter(COMConstants.SEC_TOKEN);
    
        String csrId = null;
        if(securityIsOn || sessionId != null)
        {
          SecurityManager sm = SecurityManager.getInstance();
          UserInfo ui = sm.getUserInfo(sessionId);
          csrId = ui.getUserID();
        }
      
        //Obtain entity_type
        //Based upon billing level, release the lock using Guid or detailId and redirect.
        String entityType = null;
        LockDAO lDAO = new LockDAO(conn);
        //Defect 872 - entity type for billing should be PAYMENTS
        entityType = COMConstants.CONS_ENTITY_TYPE_PAYMENTS;
        if(request.getParameter("billing_level").equalsIgnoreCase(COMConstants.BILLING_LEVEL_CART))
        {
          lDAO.releaseLock(sessionId, csrId, orderGuid, entityType);

          forward = mapping.findForward(CUSTOMER_ORDER_SEARCH);
          path = forward.getPath() 
                        + "?" + "action=" + COMConstants.ACTION_SEARCH
                        + "&" + COMConstants.RECIPIENT_FLAG + "=" + "y"
                        + "&" + COMConstants.IN_ORDER_NUMBER + "=" + scOrdNum
       					+ "&" + COMConstants.SEC_TOKEN + "=" + request.getParameter(COMConstants.SEC_TOKEN)
    					+ "&" + COMConstants.CONTEXT + "=" + request.getParameter(COMConstants.CONTEXT);
        }
        else
        {
          lDAO.releaseLock(sessionId, csrId, orderDetailId, entityType);

          forward = mapping.findForward(CUSTOMER_ORDER_SEARCH);
          path = forward.getPath() 
                        + "?" + "action=" + COMConstants.ACTION_SEARCH
                        + "&" + COMConstants.RECIPIENT_FLAG + "=" + "y"
                        + "&" + COMConstants.IN_ORDER_NUMBER + "=" + extOrdNum
       					+ "&" + COMConstants.SEC_TOKEN + "=" + request.getParameter(COMConstants.SEC_TOKEN)
    					+ "&" + COMConstants.CONTEXT + "=" + request.getParameter(COMConstants.CONTEXT);
        }
                        
        logger.debug("execute() - action is update - about to go to Recipient Order Screen \n" +
						"forward/path = " + path);
        if(path != null  &&  path.trim().length() > 0)
		{
			logger.debug("execute() - about to leave page and go to \n" +
					"forward/path = " + path);
			forward = new ActionForward(path, redirect);
			return forward;
		}
		else
		{
			logger.error("No forward path was specified.");
			forward = mapping.findForward(COMConstants.ERROR_DISPLAY_PAGE);
			return forward;
		}
      }
      else
      {
        //Document that will contain the final XML, to be passed to the TransUtil and XSL page
        Document returnDocument = (Document) DOMUtil.getDocumentBuilder().newDocument();
        Element root = (Element) returnDocument.createElement(DATA_ROOT_NODE);
        returnDocument.appendChild(root);

        Document responseDocument = (Document) DOMUtil.getDocument();
        
		UpdateBillingBO upBo = new UpdateBillingBO(conn);
		
		if(orderDetailId!=null && orderDetailId!="")
		{
			responseDocument = upBo.getPaymentInfo(orderDetailId);
		}
		else
		{
			PaymentDAO pdao=new PaymentDAO(conn);
			orderDetailId=pdao.getOrderDetailId(orderGuid);
			responseDocument = upBo.getPaymentInfo(orderDetailId);
		}

		NodeList nl = null;
		try {
			nl = responseDocument.getChildNodes();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
        DOMUtil.addSection(returnDocument, nl);

        this.populatePageData(request); 
        this.populateSubHeaderInfo(request, returnHash);

        //append pageData to returnHash
        returnHash.put("pageData", (HashMap)this.pageData);  
        //After the Business Object returns control back, we need to populate the XML document that 
        //will be passed to the Transform Utility.  This entails:
        //  1)  append all the XML documents from the responseHash to this returnDocument
        //  2)  convert pageData in responseHash (from HashMap to Document), and append 
        //      to returnDocument
  
        //Get all the keys in the hashmap 
        Set ks = returnHash.keySet();
        Iterator iter = ks.iterator();
        String key; 
        
        //Iterate thru the hashmap returned from the business object using the keyset
        while(iter.hasNext())
        {
            key = iter.next().toString();
            if (key.equalsIgnoreCase("pageData"))
            {
              //Convert the page data hashmap to XML and append it to the final XML
              DOMUtil.addSection(returnDocument, "pageData", "data", 
                              (HashMap)returnHash.get("pageData"), true); 
            }
            else
            {
                //Append all the existing XMLs to the final XML
                Document xmlDoc = (Document) returnHash.get(key);
                nl = xmlDoc.getChildNodes();
                DOMUtil.addSection(returnDocument, nl);
            }
        }
        
        if(logger.isDebugEnabled())
        {
            //IMPORTANT:  Because StringWriter contents are NOT removed even when the flush
            //            method is executed, make StringWriter go away through scope control.
            //            Both the flush and close methods were tested with but were of none affect.
            StringWriter sw = new StringWriter();       //string representation of xml document
            DOMUtil.print((Document) returnDocument,new PrintWriter(sw));
            logger.debug("- execute(): COM returned = \n" + sw.toString());
        }

        //Get XSL File name
        File xslFile = getXSL(xslName, mapping);	
  
        forward = mapping.findForward(xslName);
        String xslFilePathAndName = forward.getPath(); //get real file name
        
        // Change to client side transform
        TraxUtil.getInstance().getInstance().transform(request, response, returnDocument, 
                            xslFile, xslFilePathAndName, (HashMap)request.getAttribute(COMConstants.CONS_APP_PARAMETERS));

        return null;
      }
    }
    catch(Throwable t)
    {
      logger.error(t);
      forward = mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
      return forward;
    }
    finally
    {
      try
      {
        //Close the connection
        if(conn != null)
        {
          conn.close();
        }
      }
      catch(Exception e)
      {
        logger.error(e);
        forward = mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
        return forward;
      }
    }
  }

  /**
   * Update Billing Information
   * @param request
   * @param connection
   * @return Document - payment information
   * @throws Exception
   * @throws IOException
   * @throws ParserConfigurationException
   * @throws SAXException
   * @throws TransformerException
   * @throws XSLException
   */
   
  private void UpdateBilling(HttpServletRequest request, Connection conn)
		throws IOException, ParserConfigurationException, SAXException, TransformerException, 
               NumberFormatException, Exception, Throwable
  {
    int numOfPayments = Integer.parseInt(request.getParameter("num_of_payments"));
    String billingLevel = request.getParameter("billing_level");
    String orderGuid = request.getParameter("order_guid");
    String orderDetailId = request.getParameter("order_detail_id");
    logger.debug("number of payments = " + numOfPayments);
    int count = 1;
    PaymentDAO dao = new PaymentDAO(conn);
    PaymentVO pVO = null;
    String sessionId = null;
    String csrId = null;
    ArrayList payments = new ArrayList();
    
    ConfigurationUtil cu = null;
    cu = ConfigurationUtil.getInstance();
    //get csr Id
    String security = (String) cu.getProperty(COMConstants.PROPERTY_FILE,COMConstants.SECURITY_IS_ON);
    boolean securityIsOn = security.equalsIgnoreCase("true") ? true : false;
    sessionId   = request.getParameter(COMConstants.SEC_TOKEN);

    
    if(securityIsOn || sessionId != null)
    {
      SecurityManager sm = SecurityManager.getInstance();
      UserInfo ui = sm.getUserInfo(sessionId);
      csrId = ui.getUserID();
    }
      
    while(count <= numOfPayments)
    {
      String status = request.getParameter("status"+count);
      String authCode = request.getParameter("auth_code"+count);
      logger.debug("count" + count);
      logger.debug("status = " + status);	
      logger.debug("authCode = " + authCode);	
      
      if(status.equalsIgnoreCase(editable))
      {
        if(authCode != null  &&  authCode.trim().length() > 0)
        {
          long paymentId = Long.parseLong(request.getParameter("payment_id"+count));
          String AAFESCode = request.getParameter("aafes_code"+count);
          String cardinalVerifiedFlag = request.getParameter("cardinal_verified_flag"+count);
          String route= request.getParameter("route"+count);
          pVO = new PaymentVO();
          pVO.setPaymentId(paymentId);
          pVO.setAuthorization(authCode);
          pVO.setAafesTicketNumber(AAFESCode);
          pVO.setUpdatedBy(csrId);
          pVO.setAuthOverrideFlag("Y");
		  pVO.setAuthResult(manual_auth_result);
		  pVO.setVoiceAuth(true);
		  //2746 Remove Cardinal Commerce values when Manual Auth is done 
		  if(cardinalVerifiedFlag != null && cardinalVerifiedFlag.equalsIgnoreCase("Y")){
			  pVO.setCardinalVerifiedFlag("N");
			  dao.deleteCardinalCommerceValues(paymentId);
		  }
		  pVO.setRoute(null);
		  
          payments.add(pVO);
        }
      }
      count++;
    }

		UpdateBillingBO ubBo = new UpdateBillingBO(conn);
		ubBo.UpdateBilling(orderGuid, orderDetailId, payments, billingLevel, csrId);
  }
  

  /**
   * Obtain connectivity with the database
   * @param none
   * @return Connection - db connection
   * @throws Exception
   * @throws IOException
   * @throws ParserConfigurationException
   * @throws SAXException
   * @throws TransformerException
   * @throws XSLException
   */

	private Connection getDBConnection()
		throws IOException, ParserConfigurationException, SAXException, TransformerException, 
               Exception
	{
        Connection conn = null;
		conn = DataSourceUtil.getInstance().getConnection(
				   ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE,
															   COMConstants.DATASOURCE_NAME));
		return conn;
	}
    
  /**
  * Retrieve name of Update Billing Action XSL file
  * @param1 String - the xsl name  
  * @param2 ActionMapping
  * @return File - XSL File name
  * @throws none
  */

  private File getXSL(String xslName, ActionMapping mapping)
  {

    File xslFile = null;
    String xslFilePathAndName = null;

    ActionForward forward = mapping.findForward(xslName);
    xslFilePathAndName = forward.getPath();
    //get real file name
    xslFile = new File(this.getServlet().getServletContext().getRealPath(xslFilePathAndName));
    return xslFile;
  }
  
  /**
  * Populate the page data 
  * 
  * @param extOrdNum
  * @return 
  * @throws none
  */
  private void populatePageData(HttpServletRequest request)
		throws IOException, ParserConfigurationException, SAXException, TransformerException, 
               Exception
  {
    //store the master order number in the page data
    if (request.getParameter(COMConstants.EXTERNAL_ORDER_NUMBER) != null)
    {
      this.pageData.put(COMConstants.EXTERNAL_ORDER_NUMBER, request.getParameter(COMConstants.EXTERNAL_ORDER_NUMBER));
    }
    else
    {
      this.pageData.put(COMConstants.EXTERNAL_ORDER_NUMBER, null);
    }
    //store the master order number in the page data
    if (request.getParameter(COMConstants.MASTER_ORDER_NUMBER) != null)
    {
      this.pageData.put(COMConstants.MASTER_ORDER_NUMBER, request.getParameter(COMConstants.MASTER_ORDER_NUMBER));
    }
    else
    {
      this.pageData.put(COMConstants.MASTER_ORDER_NUMBER, null);
    }
    //store the order detail ID in the page data
    if(request.getParameter(COMConstants.ORDER_DETAIL_ID) != null)
    {
      this.pageData.put(COMConstants.ORDER_DETAIL_ID, request.getParameter(COMConstants.ORDER_DETAIL_ID));
    }
    else
    {
      this.pageData.put(COMConstants.ORDER_DETAIL_ID, null);
    }    

    //store the order guid ID in the page data
    if(request.getParameter(COMConstants.ORDER_GUID) != null)
    {
      this.pageData.put(COMConstants.ORDER_GUID, request.getParameter(COMConstants.ORDER_GUID));
    }
    else
    {
      this.pageData.put(COMConstants.ORDER_GUID, null);
    }
    
    //store the billing level in the page data
    if(request.getParameter("billing_level") != null)
    {
      this.pageData.put("billing_level", request.getParameter("billing_level"));
    }
    else
    {
      this.pageData.put("billing_level", null);
    }   
    
    //store the source code in the page data
    if(request.getParameter("source_code") != null)
    {
      this.pageData.put("source_code", request.getParameter("source_code"));
    }
    else
    {
      this.pageData.put("source_code", null);
    } 
	
		GlobalParmHandler gph = (GlobalParmHandler)CacheManager.getInstance().getHandler("CACHE_NAME_GLOBAL_PARM");
    if (gph != null)
    {
		//if the flag is yes, authorize
        String authFlag = gph.getGlobalParm(COMConstants.GLOBAL_CONTEXT, COMConstants.GLOBAL_AUTH_FLAG);
	    this.pageData.put("glbl_auth_message_flag", authFlag);
	}
  }
  
  /**
  * populateSubHeaderInfo()
  * Populate the page data with the remainder of the fields
  *
  * @param request
  * @return HashMap
  * @throws Exception
  */
  private HashMap populateSubHeaderInfo(HttpServletRequest request, HashMap returnHash)
      throws Exception
  {
	BasePageBuilder bpb = new BasePageBuilder();
	//xml document for sub header
	Document subHeaderXML = DOMUtil.getDocument();
	//retrieve the sub header info
	subHeaderXML = bpb.retrieveSubHeaderData(request);
	//save the original product xml
	returnHash.put(COMConstants.HK_SUB_HEADER, subHeaderXML);
    
    return returnHash;
  }
}