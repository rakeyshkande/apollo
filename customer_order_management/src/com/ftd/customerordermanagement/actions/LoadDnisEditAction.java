package com.ftd.customerordermanagement.actions;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.dao.DnisDAO;
import com.ftd.customerordermanagement.dao.LockDAO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityManager;

import java.io.File;
import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;

/**
 * LoadDnisEditAction class
 * This entails loading the popup used for updating a specific DNIS information for a current DNIS
 * 
 */

public final class LoadDnisEditAction extends Action 
{
    private static Logger logger  = new Logger("com.ftd.customerordermanagement.actions.LoadDnisEditAction");
    private static String OUT_CUR = "OUT_CUR";
    private static String OUT_ID_COUNT = "OUT_ID_COUNT";
    private static String OUT_PARAMETERS = "out-parameters";
    private static String OUT_LOCKED_IND = "OUT_LOCKED_IND";
    private static String OUT_UPDATE_IND = "OUT_UPDATE_IND";
    private static String YES = "Y";
    private static String NO = "N";
    private static String ON = "on";
  
  /**
   * This is the main action called from the Struts framework.
   * @param mapping The ActionMapping used to select this instance.
   * @param form The optional ActionForm bean for this request.
   * @param request The HTTP Request we are processing.
   * @param response The HTTP Response we are processing.
   */
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException
  {
    String dnisId = null; 
    HashMap secondaryMap = new HashMap();
    int maxRecords = 0;
    Connection conn = null;
    String actionType = null;
    String userId = null;
    
    try
    {
      // pull base document
      Document responseDocument = (Document) DOMUtil.getDocument();
      
      //get all the parameters in the request object
      // action type
      if(request.getParameter(COMConstants.ACTION_TYPE)!=null)
      {
        actionType = request.getParameter(COMConstants.ACTION_TYPE);
      }
      else
      {
        logger.error("Missing action_type parameter in request object");
        return mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
      }
      secondaryMap.put(COMConstants.ACTION_TYPE, actionType);
      // dnis id
      if(request.getParameter(COMConstants.DNIS_ID)!=null)
      {
        dnisId = request.getParameter(COMConstants.DNIS_ID); // for update DNIS action
      }
      else // assume add DNIS action
      {
        dnisId = null;
        actionType = COMConstants.INSERT_DNIS;
      }
      secondaryMap.put(COMConstants.DNIS_ID, dnisId);
                                      
      // get database connection
      conn =
      DataSourceUtil.getInstance().getConnection(ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE,
                                                  COMConstants.DATASOURCE_NAME));
       
      // If this is a response from validation, pull the errorMessage attribute from request object
      // and convert to XML using DOMUtil and append to responseDocument
      HashMap errMsgMap = null;
      if(request.getAttribute("errorMessage")!=null)
      {
        errMsgMap = (HashMap) request.getAttribute("errorMessage");
        DOMUtil.addSection(responseDocument, "validation", "error", errMsgMap, true);
        
      }
      // put a flag in pageData that indicates to client that it is a response for Submit action
      if(request.getAttribute("SUBMIT_ACTION")!=null)
        secondaryMap.put("SUBMIT_ACTION", YES);
      
      // retrieve lock and append it to the main document
      // get lock parameters
      String sessionId = (String) request.getParameter(COMConstants.SEC_TOKEN);
      if(sessionId==null)
      {
        logger.error("Security token not found in request object");
      }
      if (SecurityManager.getInstance().getUserInfo(sessionId)!=null)
      {
        userId = SecurityManager.getInstance().getUserInfo(sessionId).getUserID();
      }
      
      // If initial load or submit with error, retrieve lock
      if(request.getAttribute("SUBMIT_ACTION") == null || (errMsgMap != null && errMsgMap.get("VALIDATION_SUCCESS").equals("N")))
      {
          String lockId  = dnisId;
          
          LockDAO lockObj = new LockDAO(conn);
          Document xmlLock = lockObj.retrieveLockXML(sessionId, userId, lockId, COMConstants.CONS_ENTITY_TYPE_DNIS);
          DOMUtil.addSection(responseDocument, xmlLock.getChildNodes());
      }
          
          // check if lock status is Y and CSR_ID is someone else. Then, forward without db access.
          //if(DOMUtil.getNodeValue(xmlLock, OUT_LOCKED_IND).equals(YES) && DOMUtil.getNodeValue(xmlLock, OUT_UPDATE_IND).equals(NO))
          //{
          //  logger.debug("CSR lock is not available for adding or editing DNIS.");          
          //}
          //else
          //{        
            // Call load DAO method to load the specific data for the specified DNIS
            DnisDAO dnis = new DnisDAO(conn);
            Document dnisXML = dnis.loadDnisXML(dnisId);
            DOMUtil.addSection(responseDocument, dnisXML.getChildNodes());
        
            // load the list of available origins for display on the page dropdown
            Document originsXML = dnis.loadOriginListXML();
            
            // load the list of available country script ids for display on the page dropdown
            Document countryScriptsXML = dnis.loadCountryScriptListXML();
            
            // load the list of available scripts to display on the page dropdown
            Document scriptsXML = dnis.loadScriptListXML();
            
            // append the XML returned from all the above calls to the responseDocument
            DOMUtil.addSection(responseDocument, originsXML.getChildNodes());
            DOMUtil.addSection(responseDocument, countryScriptsXML.getChildNodes());
            DOMUtil.addSection(responseDocument, scriptsXML.getChildNodes());
            
            // Generate XML from secondary HashMap using the DOMUtil HashMap to XML conversion method addSection() .
            DOMUtil.addSection(responseDocument, "pageData", "data", secondaryMap, true);
            //responseDocument.print(System.out);
          //}
      //}
      
      // Forward to transformation action with EditDNIS.xsl
      //Get XSL File name
      String xslName = "EditDnis";
      File xslFile = getXSL(xslName, mapping);  
      ActionForward forward = mapping.findForward(xslName);
      String xslFilePathAndName = forward.getPath(); //get real file name
      
      // Change to client side transform
      TraxUtil.getInstance().getInstance().transform(request, response, responseDocument, 
                          xslFile, xslFilePathAndName, (HashMap)request.getAttribute(COMConstants.CONS_APP_PARAMETERS));
      return null;
    }
    catch (Throwable ex)
    {
      logger.error(ex);
      return mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
    }
    finally 
    {
      try
      {
        if(conn!=null)
          conn.close();
      }
      catch (SQLException se)
      {
        logger.error(se);
        return mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
      }
    }  
  }

  /******************************************************************************
  *                                     getXSL()
  *******************************************************************************
  * Retrieve name of Customer Order Search Action XSL file
  * @param1 String - the xsl name returned from the Business Object
  * @param2 ActionMapping
  * @return File - XSL File name
  * @throws none
  */

  private File getXSL(String xslName, ActionMapping mapping)
  {

    File xslFile = null;
    String xslFilePathAndName = null;

    ActionForward forward = mapping.findForward(xslName);
    xslFilePathAndName = forward.getPath();                //get real file name
    xslFile = new File(this.getServlet().getServletContext().getRealPath(xslFilePathAndName));
    return xslFile;
  }
}