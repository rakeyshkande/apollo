package com.ftd.customerordermanagement.actions;

import com.ftd.customerordermanagement.bo.UpdateCustomerEmailBO;
import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;

import java.sql.Connection;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;




public final class UpdateCustomerEmailAction extends Action
{
    private static  Logger logger         = new Logger("com.ftd.customerordermanagement.actions.UpdateCustomerEmailAction");
    private String action                 = null;
    private String submittedViaIFrame     = null;
    private Document responseDocument  = null;

    //This Hashmap will be used to hold all the XML objects, page data, and search criteria that
    //will be returned from the business object
    private HashMap responseHash          = new HashMap();

  /**
   * This is the main action called from the Struts framework.
   * @param mapping The ActionMapping used to select this instance.
   * @param form The optional ActionForm bean for this request.
   * @param request The HTTP Request we are processing.
   * @param response The HTTP Response we are processing.
   */
  public  ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                                HttpServletResponse response)

  {
    this.action = null;
    this.submittedViaIFrame = null;
		ActionForward forward = null;
    Connection con = null;

    try
    {
      String xslName = "";

      //Connection/Database info
      con = DataSourceUtil.getInstance().getConnection(
            ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE,
            COMConstants.DATASOURCE_NAME));


      //Document that will contain the final XML, to be passed to the TransUtil and XSL page
      responseDocument = (Document) DOMUtil.getDocument();

      //Get info passed in the request object
      getRequestInfo(request);

      //Call the Business Object, which will return a hashmap containing
      //    0 to Many - XML documents
      //    HashMap1  = pageData - hashmap that contains String data at page level
      UpdateCustomerEmailBO uceBO = new UpdateCustomerEmailBO(request, con);
      responseHash = uceBO.processRequest(this.action);

      //After the Business Object returns control back, we need to populate the XML document that
      //will be passed to the Transform Utility.  This entails:
      //  1)  append all the XML documents from the responseHash to this returnDocument
      //  2)  convert pageData in responseHash (from HashMap to Document), and append
      //      to returnDocument

      //Get all the keys in the hashmap returned from the business object
      Set ks = responseHash.keySet();
      Iterator iter = ks.iterator();
      String key;

      //Iterate thru the hashmap returned from the business object using the keyset
      while(iter.hasNext())
      {
        key = iter.next().toString();
        if (key.equalsIgnoreCase("pageData"))
        {
          //The page data hashmap within the main hashmap contains an object whose key is "XSL "
          //This key refers to the page where the control will be transferred to.  However, this key
          //should not be included in the page data XML
          xslName = (String)((HashMap) responseHash.get("pageData")).get("XSL").toString();
          ((HashMap)responseHash.get("pageData")).remove("XSL");

          //Convert the page data hashmap to XML and append it to the final XML
          DOMUtil.addSection(responseDocument, "pageData", "data",
                              (HashMap)responseHash.get("pageData"), true);
        }
        else
        {
          //Append all the existing XMLs to the final XML
          Document xmlDoc = (Document) responseHash.get(key);
          NodeList nl = xmlDoc.getChildNodes();
          DOMUtil.addSection(responseDocument, nl);
        }
      }

      //for local development, uncomment
      //responseDocument.print(System.out);

      if(logger.isDebugEnabled())
      {
        //IMPORTANT:  Because StringWriter contents are NOT removed even when the flush
        //            method is executed, make StringWriter go away through scope control.
        //            Both the flush and close methods were tested with but were of none affect.
        StringWriter sw = new StringWriter();       //string representation of xml document
        DOMUtil.print((Document) responseDocument,new PrintWriter(sw));
        logger.debug("Update Customer Email Action " + sw.toString());
      }

      //check if we have to forward to another action, or just transform the XSL.
      if (xslName.equalsIgnoreCase(COMConstants.XSL_REFUND_UPDATE_ACTION))
      {
        forward = mapping.findForward(COMConstants.XSL_REFUND_UPDATE_ACTION);
        request.setAttribute(COMConstants.ACTION, COMConstants.ACTION_CUSTOMER_ACCOUNT_SEARCH);
        return forward;
      }
      else
      {
        //Get XSL File name
        File xslFile = getXSL(xslName, mapping);
        forward = mapping.findForward(xslName);
        String xslFilePathAndName = forward.getPath(); //get real file name
        
        // Change to client side transform
        TraxUtil.getInstance().getInstance().transform(request, response, responseDocument, 
                            xslFile, xslFilePathAndName, (HashMap)request.getAttribute(COMConstants.CONS_APP_PARAMETERS));

        return null;
      }

    }

    catch(Exception e)
    {
      logger.error(e);
      try
      {
        if (this.submittedViaIFrame != null && this.submittedViaIFrame.equalsIgnoreCase(COMConstants.CONS_YES))
        {
          HashMap pageData = new HashMap();
          pageData.put(COMConstants.PD_MESSAGE_DISPLAY, "exception found");
          //Convert the page data hashmap to XML and append it to the final XML
          DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true);

          File xslFile = getXSL(COMConstants.XSL_PROCESS_CUSTOMER_EMAIL_IFRAME, mapping);

          //Transform the XSL File
          TraxUtil.getInstance().getInstance().transform(request, response, responseDocument,
                              xslFile, (HashMap)request.getAttribute(COMConstants.CONS_APP_PARAMETERS));

          return null;
        }
        else
        {
          forward = mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
          return forward;
        }
      }
      catch (Exception e2)
      {
        logger.error(e2);
        forward = mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
        return forward;
      }
    }

    finally
    {
      try
      {
        //Close the connection
        if(con != null)
        {
          con.close();
        }
      }
      catch(Exception e)
      {
        logger.error(e);
        forward = mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
        return forward;
      }
    }

  }



  /******************************************************************************
  *             getRequestInfo(HttpServletRequest request)
  *******************************************************************************
  * Retrieve the info from the request object, and set class level variables
  * @param  HttpServletRequest
  * @return none
  * @throws none
  */
  private void getRequestInfo(HttpServletRequest request)
  {
    if(request.getAttribute(COMConstants.ACTION) != null)
      this.action = request.getAttribute(COMConstants.ACTION).toString();
    else if(request.getParameter(COMConstants.ACTION)!=null)
      this.action = request.getParameter(COMConstants.ACTION);

    if (request.getParameter("submittedViaIFrame") != null)
      this.submittedViaIFrame = request.getParameter("submittedViaIFrame");

  }



  /******************************************************************************
  *                                     getXSL()
  *******************************************************************************
  * Retrieve name of Customer Order Search Action XSL file
  * @param1 String - the xsl name returned from the Business Object
  * @param2 ActionMapping
  * @return File - XSL File name
  * @throws none
  */

  private File getXSL(String xslName, ActionMapping mapping)
  {

    File xslFile = null;
    String xslFilePathAndName = null;

    ActionForward forward = mapping.findForward(xslName);
    xslFilePathAndName = forward.getPath();                //get real file name
    xslFile = new File(this.getServlet().getServletContext().getRealPath(xslFilePathAndName));
    return xslFile;
  }


}