package com.ftd.customerordermanagement.actions;

import com.ftd.customerordermanagement.bo.CallReasonCodeBO;
import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.util.DatabaseMaintainUtil;
import com.ftd.customerordermanagement.util.TimerFilter;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityManager;

import com.ftd.security.cache.vo.UserInfo;
import java.io.File;
import java.io.IOException;

import java.sql.Connection;

import java.util.HashMap;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.commons.lang.StringUtils;

import org.w3c.dom.Document;

/**
 * This class redirects the user to either the Main Menu page or the Call Code Reason page.
 *
 * @author Ali Lakhani
 */



public final class MainMenuAction extends Action
{

    private static    Logger logger  = new Logger("com.ftd.customerordermanagement.actions.MainMenuAction");

  /******************************************************************************
  * execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)
  *******************************************************************************
  *
  * This is the main action called from the Struts framework.
  * @param mapping The ActionMapping used to select this instance.
  * @param form The optional ActionForm bean for this request.
  * @param request The HTTP Request we are processing.
  * @param response The HTTP Response we are processing.
  */
  public  ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                                HttpServletResponse response)
          throws IOException, ServletException, Exception
  {

    String  mainMenuUrl     = "";
    String  callPage        = "";
    boolean workComplete    = true;
    ActionForward forward   = null;
    HashMap requestParms    = new HashMap();
    //Connection/Database info
    Connection con = DataSourceUtil.getInstance().getConnection(
                      ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE,
                      COMConstants.DATASOURCE_NAME));
    String xslName = null;

    try
    {
      //Get info passed in the request object
      requestParms = getRequestInfo(request);

      //delete records from CSR_VIEWING_ENTITIES table for the current session id.
      deleteRecordsBasedOnCurrentSessionId(con, (String)requestParms.get("securityToken"));

      //process request
      xslName = processRequest(con, request, (String)requestParms.get("timerEntityHistoryId"));
      workComplete = ("N".equalsIgnoreCase((String)requestParms.get("workComplete")))?false:true;  // We want to default to true
    }
    finally
    {
      //close the connection
			try
			{
				con.close();
			}
			catch(Exception e)
			{}
    }

    if(ConfigurationUtil.getInstance().getFrpGlobalParm(COMConstants.COM_CONTEXT, COMConstants.CONS_MAIN_MENU_URL) != null)
    {
        //main menu url
        mainMenuUrl = ConfigurationUtil.getInstance().getFrpGlobalParm(COMConstants.COM_CONTEXT, COMConstants.CONS_MAIN_MENU_URL);

        // The work_complete value represents the response to the dialog "Have you completed your work on this order?" when
        // selecting the "Main Menu" button.  For 4.8.0, this dialog was removed and the value set to Y in all of COM.  However,
        // the "else" logic below represents the original logic when N was set.  We leave it in case other entry points still exist.
        //
        if (workComplete) {
            // This is same url constructed by CallReasonCodeAction when a Call Reason Code was finally entered.
            // It was copied here since the Call Reason Code logic was bypassed in 4.8.0.
            // The key effect is that t_call_log_id and call_dnis will not get propagated (thus we're starting anew).
            mainMenuUrl += "?adminAction=customerService";
            mainMenuUrl += "&context=";
            mainMenuUrl += (String)requestParms.get("context");
            mainMenuUrl += "&securitytoken=";
            mainMenuUrl += (String)requestParms.get("securityToken");
        } else {
        
            //append adminAction
            mainMenuUrl += "?adminAction=";
            mainMenuUrl += (String)requestParms.get("adminAction");
            
            //append security to the main menu url
            mainMenuUrl += "&context=";
            mainMenuUrl += (String)requestParms.get("context");
            mainMenuUrl += "&securitytoken=";
            mainMenuUrl += (String)requestParms.get("securityToken");
            
            //append header info to the main menu url
            mainMenuUrl += "&call_dnis=";
            mainMenuUrl += (String)requestParms.get("headerDnisId");
            mainMenuUrl += "&call_brand_name=";
            mainMenuUrl += (String)requestParms.get("headerBrandName");
            mainMenuUrl += "&call_cs_number=";
            mainMenuUrl += (String)requestParms.get("headerCustomerServiceNumber");
            mainMenuUrl += "&call_oe_flag=";
            mainMenuUrl += (String)requestParms.get("headerCustomerOrderIndicator");
            
            //append security to the main menu url
            mainMenuUrl += "&t_call_log_id=";
            mainMenuUrl += (String)requestParms.get("timerCallLogId");
            mainMenuUrl += "&t_comment_origin_type=";
            mainMenuUrl += (String)requestParms.get("timerCommentOriginType");
            mainMenuUrl += "&t_entity_history_id=";
            mainMenuUrl += (String)requestParms.get("timerEntityHistoryId");
        }
        response.sendRedirect(mainMenuUrl);
    }
    
    return null;

  }



  /******************************************************************************
  * getRequestInfo(HttpServletRequest request)
  *******************************************************************************
  * Retrieve the info from the request object, and set class level variables
  * @param  HttpServletRequest
  * @return none
  * @throws none
  */
  private HashMap getRequestInfo(HttpServletRequest request)
  {
    String  adminAction                   = "customerService";
    String  context                       = "";
    String  headerBrandName               = "";
    String  headerCustomerOrderIndicator  = "";
    String  headerCustomerServiceNumber   = "";
    String  headerDnisId                  = "";
    String  securityToken                 = "";
    String  timerCallLogId                = "";
    String  timerCommentOriginType        = "";
    String  timerEntityHistoryId          = "";
    String  workComplete                  = "";
    HashMap requestParms                  = new HashMap();

    /**********************retreive the timer info**********************/
    //retrieve the t_comment_origin_type
    if(request.getParameter(COMConstants.TIMER_COMMENT_ORIGIN_TYPE)!=null)
      timerCommentOriginType = request.getParameter(COMConstants.TIMER_COMMENT_ORIGIN_TYPE);

    //retrieve the t_entity_history_id
    if(request.getParameter(COMConstants.TIMER_ENTITY_HISTORY_ID)!=null)
      timerEntityHistoryId = request.getParameter(COMConstants.TIMER_ENTITY_HISTORY_ID);

    //retrieve the t_call_log_id
    if(request.getParameter(COMConstants.TIMER_CALL_LOG_ID)!=null)
      timerCallLogId = request.getParameter(COMConstants.TIMER_CALL_LOG_ID);


    /**********************retreive the header info**********************/
    //retrieve the brand name
    if(request.getParameter(COMConstants.H_BRAND_NAME)!=null)
      headerBrandName = request.getParameter(COMConstants.H_BRAND_NAME);

    //retrieve the customer order indicator
    if(request.getParameter(COMConstants.H_CUSTOMER_ORDER_INDICATOR)!=null)
      headerCustomerOrderIndicator = request.getParameter(COMConstants.H_CUSTOMER_ORDER_INDICATOR);

    //retrieve the cserv number
    if(request.getParameter(COMConstants.H_CUSTOMER_SERVICE_NUMBER)!=null)
      headerCustomerServiceNumber = request.getParameter(COMConstants.H_CUSTOMER_SERVICE_NUMBER);

    //retrieve the dnis id
    if(request.getParameter(COMConstants.H_DNIS_ID)!=null)
      headerDnisId = request.getParameter(COMConstants.H_DNIS_ID);


    /**********************retreive the security info**********************/
    //retrieve the context
    if(request.getParameter(COMConstants.CONTEXT)!=null)
      context = request.getParameter(COMConstants.CONTEXT);

    //retrieve the security token
    if(request.getParameter(COMConstants.SEC_TOKEN)!=null)
      securityToken = request.getParameter(COMConstants.SEC_TOKEN);

    /**********************retreive miscellaneous info**********************/
    //retrieve the work_complete
    if(request.getParameter(COMConstants.WORK_COMPLETE)!=null)
      workComplete = request.getParameter(COMConstants.WORK_COMPLETE);

    //retrieve the admin actionwork_complete
    if(request.getParameter(COMConstants.ADMIN_ACTION)!=null)
      if(!request.getParameter(COMConstants.ADMIN_ACTION).equalsIgnoreCase(""))
        adminAction = request.getParameter(COMConstants.ADMIN_ACTION);


    requestParms.put("adminAction",         		      adminAction);
    requestParms.put("context",             		      context);
    requestParms.put("headerBrandName",     		      headerBrandName);
    requestParms.put("headerCustomerOrderIndicator",  headerCustomerOrderIndicator);
    requestParms.put("headerCustomerServiceNumber",   headerCustomerServiceNumber);
    requestParms.put("headerDnisId",                  headerDnisId);
    requestParms.put("securityToken", 			          securityToken);
    requestParms.put("timerCallLogId", 			          timerCallLogId);
    requestParms.put("timerCommentOriginType", 		    timerCommentOriginType);
    requestParms.put("timerEntityHistoryId", 		      timerEntityHistoryId);
    requestParms.put("workComplete", 			            workComplete);

    return requestParms;
  }



  /******************************************************************************
  * processRequest(Document doc)
  *******************************************************************************
  *
  * @return
  * @throws
  */
  private String processRequest(Connection con, HttpServletRequest request, String timerEntityHistoryId)
  throws Exception
  {
    String callLogId = null;
    
    //if started, stop the timer
    if (timerEntityHistoryId != null && !timerEntityHistoryId.equalsIgnoreCase(""))
    {
      timerStop(con, timerEntityHistoryId);
    }
    HashMap parameters = (HashMap) request.getAttribute(COMConstants.CONS_APP_PARAMETERS);
    if (parameters != null) {
        callLogId = (String) parameters.get(COMConstants.TIMER_CALL_LOG_ID);
    }
    
    // The Call Reason Code page was removed (4.8.0), but we are still updating the CALL_LOG table for now
    if (StringUtils.isNotBlank(callLogId)) {
        CallReasonCodeBO crcBO = new CallReasonCodeBO(con);
        crcBO.setReasonCode(null, null, callLogId);
    }
    
    return COMConstants.XSL_CUSTOMER_SERVICE_MENU;
  }


  /******************************************************************************
  * timerStop()
  *******************************************************************************
  *
  */

  private void timerStop(Connection con, String timerEntityHistoryId)         throws Exception
  {

    TimerFilter tFilter = new TimerFilter(con);

    //stopTimer(entityHistoryId)
    tFilter.stopTimer(timerEntityHistoryId);

    timerEntityHistoryId = null;

  }



  /******************************************************************************
  * getXSL()
  *******************************************************************************
  * Retrieve name of Customer Order Search Action XSL file
  * @param1 String - the xsl name returned from the Business Object
  * @param2 ActionMapping
  * @return File - XSL File name
  * @throws none
  */

  private File getXSL(String xslName, ActionMapping mapping)
  {

    File xslFile = null;
    String xslFilePathAndName = null;

    ActionForward forward = mapping.findForward(xslName);
    xslFilePathAndName = forward.getPath();                //get real file name
    xslFile = new File(this.getServlet().getServletContext().getRealPath(xslFilePathAndName));
    return xslFile;
  }


/*******************************************************************************************
 * deleteRecordsBasedOnCurrentSessionId()
 *******************************************************************************************/
  private void deleteRecordsBasedOnCurrentSessionId(Connection con, String securityToken)         throws Exception
  {

    DatabaseMaintainUtil dmu = new DatabaseMaintainUtil(con, securityToken);
    HashMap csrDeleteInfo = (HashMap) dmu.deleteViewingRecords();

  }


}