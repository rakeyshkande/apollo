package com.ftd.customerordermanagement.actions;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.dao.CommentDAO;
import com.ftd.customerordermanagement.dao.CustomerDAO;
import com.ftd.customerordermanagement.dao.OrderDAO;
import com.ftd.customerordermanagement.dao.StockMessageDAO;
import com.ftd.customerordermanagement.vo.CommentsVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.handlers.GlobalParmHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.FTDAppsGlobalParmsVO;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.XMLFormat;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityManager;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;

import java.math.BigDecimal;

import java.sql.Connection;
import java.sql.SQLException;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



import org.apache.fop.apps.Driver;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import org.xml.sax.InputSource;

/**
 * ReceiptAction class
 *
 * @author Brian Munter
 */

public final class ReceiptAction extends Action
{
    private static Logger logger  = new Logger("com.ftd.customerordermanagement.actions.ReceiptAction");
    private String languageId = null;
    private boolean canadianRecipient;
    
  /**
   * This is the main action called from the Struts framework.
   *
   * @param mapping The ActionMapping used to select this instance.
   * @param form The optional ActionForm bean for this request.
   * @param request The HTTP Request we are processing.
   * @param response The HTTP Response we are processing.
   */
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
        Connection conn = null;
        CachedResultSet detailIdSet = null;
        
        try
        {
            conn = DataSourceUtil.getInstance().getConnection(ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, COMConstants.DATASOURCE_NAME));
            
            Document orderDocument = (Document) DOMUtil.getDefaultDocument();
            OrderDAO orderDAO = new OrderDAO(conn);
            CustomerDAO custDAO = new CustomerDAO(conn);
               
            List xmlList = null;   
            Document xmlDoc = null;
            Element orderElement = null;
            NodeList nl = null;
            
            Element rootElement = orderDocument.createElement("root");
            orderDocument.appendChild(rootElement);
                    
            detailIdSet = orderDAO.viewDetailIds(request.getParameter("order_guid"));
            while(detailIdSet != null && detailIdSet.next())
            {
                orderElement = orderDocument.createElement("ORDER");
                rootElement.appendChild(orderElement);
                
                xmlList = this.retrieveOrderInfoForReceipt(detailIdSet.getObject(1).toString(), conn);
                
                //Iterate thru the hashmap returned from the business object using the keyset
                for(int i=0; i < xmlList.size(); i++)
                {
                    //Append all the existing XMLs to the final XML
                    xmlDoc = (Document) xmlList.get(i);
                    nl = xmlDoc.getChildNodes();
        
                    for(int x = 0; x < nl.getLength(); x++)
                    {
                        Node myNode = nl.item(x);
                        if(myNode != null)
                        {
                            orderElement.appendChild(orderDocument.importNode(myNode, true));
                        }
                    }
                }
            }
            
            // Add any extra data needed to the XML documant
            String originId = request.getParameter("origin_id");
            String originList = ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, COMConstants.EMAIL_ORIGINS);
            
            HashMap letterValues = new HashMap();
            if(originId!=null && originList.indexOf(originId) >= 0) // origin found
            {
                // do nothing
            }
            else
            {
                StockMessageDAO msgAccess = new StockMessageDAO(conn);
                
                // if origin is not found in the configuration file, Pull logo name from database based on company id
                String logoFileName = msgAccess.loadLetterLogo(request.getParameter("company_id"));
                letterValues.put("logo", this.getServlet().getServletContext().getRealPath("/images/" + logoFileName));
            }
            
            // Add current date to xml
            String DATE_FORMAT = "MMMM dd, yyyy"; 
            SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT); 
            letterValues.put("current_date", sdf.format(new Date()));
            
            // Add company name to xml
            if (custDAO.cartHasAPreferredPartnerOrder(request.getParameter("order_guid")))
              letterValues.put("company_name", request.getParameter("company_id"));
            else
              letterValues.put("company_name", request.getParameter("company_name"));
      
                  // Pull the special service charge
            GlobalParmHandler globalParamHandler = (GlobalParmHandler)CacheManager.getInstance().getHandler("CACHE_NAME_GLOBAL_PARM");
            FTDAppsGlobalParmsVO global = globalParamHandler.getFTDAppsGlobalParms();
            if(global == null)
            {
                String msg = "Global params not found in database. (cache)";
                logger.error(msg);
            }
            
            BigDecimal specialServiceCharge = global.getSpecialSvcCharge();
            if(specialServiceCharge != null)
            {
                letterValues.put("special_service_charge", specialServiceCharge.setScale(2, BigDecimal.ROUND_HALF_UP).toString());
            }
            
            String companyId = request.getParameter("company_id");            
            if(!"FTDCA".equalsIgnoreCase(companyId) && this.canadianRecipient){
          	  String surchargeExplanation  = ConfigurationUtil.getInstance().
  													getContent(conn, "TAX", "TAX_EXPLANATION");
          	  surchargeExplanation = surchargeExplanation.replaceAll("<br>", "<fo:block/>");
          	  letterValues.put("surchargeExplanation", surchargeExplanation);
            }
            
            String morningDeliveryLabel = ConfigurationUtil.getInstance().
				getContentWithFilter(conn, "MORNING_DELIVERY", "MORNING_DELIVERY_LABEL", null, null); 
            letterValues.put("morningDeliveryLabel", morningDeliveryLabel);
            /*
            // Only give the chance to display the language message and phone number if the language
            // on the order equals FRCA or ESMX.  If the phone number is null, don't display either.
            String languageMessage = orderDAO.getContentWithFilter("LANGUAGE", "LANGUAGE.MESSAGE", "TEXT", languageId);
            String phoneNumber = orderDAO.getContentWithFilter("LANGUAGE", "PHONE_NUMBER", companyId, languageId);   

            if("FRCA".equals(languageId) || "ESMX".equals(languageId)){
	            if(phoneNumber != null){
	            	letterValues.put("phone_number", phoneNumber);
	                if (languageMessage != null) {
	                	letterValues.put("language_message", languageMessage);
	                }
	            }
            }
            */
            
            // Add map to xml
            DOMUtil.addSection(orderDocument, "letter", "detail", letterValues, true);
            
            // Transform FO file for PDF
            File xslFile = new File(this.getServlet().getServletContext().getRealPath("/xsl/receiptfo.xsl")); 
            String foString = TraxUtil.getInstance().transform(orderDocument, xslFile, null);
            
            // Call insertComment() on OrderDAO passing the specified comment attached to the current order and customer
            this.createComment("Order Receipt Printed", conn, request);
            
            // Render PDF to response
            StringReader sr = new StringReader(foString);
            renderFO(new InputSource(sr), response);
        }
        catch (Throwable ex)
        {
            logger.error(ex);
            //forward = mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
            //return forward;
        }
        finally
        {
            try
            {
                if(conn != null)
                {
                    conn.close();
                }
            }
            catch (SQLException se)
            {
                logger.error(se);
            }
        }
        
        return null;
    }
    
    private ArrayList retrieveOrderInfoForReceipt(String orderDetailId, Connection conn) throws Exception
    {
        //Instantiate OrderDAO
        OrderDAO orderDAO = new OrderDAO(conn);
        
        //hashmap that will contain the output from the stored proc
        HashMap printOrderInfo = new HashMap();
        ArrayList xslList = new ArrayList();
        
        //Call getCartInfo method in the DAO
        printOrderInfo = orderDAO.getOrderInfoForPrint(orderDetailId, "N", COMConstants.CONS_PROCESSING_ID_CUSTOMER);
        
        //Create an Document and call a method that will transform the CachedResultSet from the cursor:
        //OUT_ORDERS_CUR into an Document.  Store this XML object in hashXML HashMap
        Document printOrderXML = buildXML(printOrderInfo, "OUT_ORDERS_CUR", "PRINT_ORDERS", "PRINT_ORDER");
        xslList.add(printOrderXML);
        
        //Create an Document and call a method that will transform the CachedResultSet from the cursor:
        //OUT_ORDER_ADDON_CUR into an Document.  Store this XML object in hashXML HashMap
        Document printOrderAddonXML = buildXML(printOrderInfo, "OUT_ORDER_ADDON_CUR", "PRINT_ORDER_ADDONS", "PRINT_ORDER_ADDON");
        xslList.add(printOrderAddonXML);
        
        //Create an Document and call a method that will transform the CachedResultSet from the cursor:
        //OUT_ORDER_BILLS_CUR into an Document.  Store this XML object in hashXML HashMap
        Document printOrderBillsXML = buildXML(printOrderInfo, "OUT_ORDER_BILLS_CUR", "PRINT_ORDER_BILLS", "PRINT_ORDER_BILL");
        xslList.add(printOrderBillsXML);
        
        //Create an Document and call a method that will transform the CachedResultSet from the cursor:
        //OUT_ORDER_REFUNDS_CUR into an Document.  Store this XML object in hashXML HashMap
        Document  printOrderRefundsXML = buildXML(printOrderInfo, "OUT_ORDER_REFUNDS_CUR", "PRINT_ORDER_REFUNDS", "PRINT_ORDER_REFUND");
        xslList.add(printOrderRefundsXML);
        
        //Create an Document and call a method that will transform the CachedResultSet from the cursor:
        //OUT_ORDER_TOTAL_CUR into an Document.  Store this XML object in hashXML HashMap
        Document  printOrderTotalXML = buildXML(printOrderInfo, "OUT_ORDER_TOTAL_CUR", "PRINT_ORDER_TOTALS", "PRINT_ORDER_TOTAL");
        xslList.add(printOrderTotalXML);
        
        //Create an Document and call a method that will transform the CachedResultSet from the cursor:
        //OUT_ORDER_PAYMENT_CUR into an Document.  Store this XML object in hashXML HashMap
        Document  printOrderPaymentXML = buildXML(printOrderInfo, "OUT_ORDER_PAYMENT_CUR", "PRINT_ORDER_PAYMENTS", "PRINT_ORDER_PAYMENT");
        xslList.add(printOrderPaymentXML);
        
        //Create an Document and call a method that will transform the CachedResultSet from the cursor:
        //OUT_ORDER_COMMENT_CUR into an Document.  Store this XML object in hashXML HashMap
        Document  printOrderCommentXML = buildXML(printOrderInfo, "OUT_ORDER_COMMENT_CUR", "PRINT_ORDER_COMMENTS", "PRINT_ORDER_COMMENT");
        xslList.add(printOrderCommentXML);
        
        //Create an Document and call a method that will transform the CachedResultSet from the cursor:
        //CUST_CART_CUR into an Document.  Store this XML object in hashXML HashMap
        Document  printCustomerXML = buildXML(printOrderInfo, "CUST_CART_CUR", "PRINT_CUSTOMERS", "PRINT_CUSTOMER");
        xslList.add(printCustomerXML);
        
        //Create an Document and call a method that will transform the CachedResultSet from the cursor:
        //OUT_CUSTOMER_PHONE_CUR into an Document.  Store this XML object in hashXML HashMap
        Document  printCustomerPhoneXML = buildXML(printOrderInfo, "OUT_CUSTOMER_PHONE_CUR", "PRINT_CUSTOMER_PHONES", "PRINT_CUSTOMER_PHONE");
        xslList.add(printCustomerPhoneXML);
        
        //Create an Document and call a method that will transform the CachedResultSet from the cursor:
        //OUT_MEMBERSHIP_CUR into an Document.  Store this XML object in hashXML HashMap
        Document  printMembershipXML = buildXML(printOrderInfo, "OUT_MEMBERSHIP_CUR", "PRINT_MEMBERSHIPS", "PRINT_MEMBERSHIP");
        xslList.add(printMembershipXML);
        
        //Create an Document and call a method that will transform the CachedResultSet from the cursor:
        //OUT_TAX_REFUND_CUR into an Document.  Store this XML object in hashXML HashMap
        Document  printTaxRefundXML = buildXML(printOrderInfo, "OUT_TAX_REFUND_CUR", "PRINT_TAX_REFUNDS", "PRINT_TAX_REFUND");
        xslList.add(printTaxRefundXML);
        
        //Create an Document and call a method that will transform the CachedResultSet from the cursor:
        //OUT_TAX_REFUND_CUR into an Document.  Store this XML object in hashXML HashMap
        Document  printRolledPaymentXML = buildXML(printOrderInfo, "OUT_ROLLED_PAYMENT_CUR", "OUT_ROLLED_PAYMENTS", "OUT_ROLLED_PAYMENT");
        xslList.add(printRolledPaymentXML);
        
        //Create an Document and call a method that will transform the CachedResultSet from the cursor:
        //OUT_ORDER_TAXES_CUR into an Document.  Store this XML object in hashXML HashMap
        Document  printOrderTaxesXML = buildXML(printOrderInfo, "OUT_ORDER_TAXES_CUR", "PRINT_ORDER_TAXES", "PRINT_ORDER_TAX");
        xslList.add(printOrderTaxesXML);
        // Get language id from PRINT_ORDERS xml
        String XPath = "//PRINT_ORDERS/PRINT_ORDER";
        NodeList nodeList = DOMUtil.selectNodes(printOrderXML, XPath);
        Node node1 = null;
        if (nodeList != null) {
          for (int i = 0; i < nodeList.getLength(); i++) {
            node1 = (Node) nodeList.item(i);
            if (node1 != null) {
              if (DOMUtil.selectSingleNode(node1,"language_id/text()") != null) {
            	  languageId = DOMUtil.selectSingleNode(node1,"language_id/text()").getNodeValue();
              }
              

              if (DOMUtil.selectSingleNode(node1,"country/text()") != null) {
            	  String country = DOMUtil.selectSingleNode(node1,"country/text()").getNodeValue();
            	  if("CA".equalsIgnoreCase(country)){
            		  canadianRecipient = true;
            	  }
            	  else
            		  canadianRecipient = false;
              }
            }
          }
        }

        return xslList;
    }
    
    private Document buildXML(HashMap outMap, String cursorName, String topName, String bottomName) 
        throws Exception
    {
        //Create a CachedResultSet object using the cursor name that was passed. 
        CachedResultSet rs = (CachedResultSet) outMap.get(cursorName);
        
        Document doc =  null;
        
        //Create an XMLFormat, and initialize the top and bottom node.  Note that since this method
        //can be/is called by various other methods, the top and botton names MUST be passed to it. 
        XMLFormat xmlFormat = new XMLFormat();
        xmlFormat.setAttribute("type", "element");
        xmlFormat.setAttribute("top", topName);
        xmlFormat.setAttribute("bottom", bottomName);
        
        //call the DOMUtil's converToXMLDOM method
        doc = (Document) DOMUtil.convertToXMLDOM(rs, xmlFormat);
        
        //return the xml document. 
        return doc;
    }
    
    public void renderFO(InputSource foFile, HttpServletResponse response) throws ServletException 
    {
        try 
        {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            
            response.setContentType("application/pdf");
            
            Driver driver = new Driver(foFile, out);
            driver.setRenderer(Driver.RENDER_PDF);
            driver.run();
            
            byte[] content = out.toByteArray();
            response.setContentLength(content.length);
            response.getOutputStream().write(content);
            response.getOutputStream().flush();
        }
        catch (Exception ex) 
        {
            logger.error(ex.getMessage());
        }
        finally
        {
            try
            {
                if(response.getOutputStream() != null)
                {
                    response.getOutputStream().close();
                }
            }
            catch (Exception ex) 
            {
                logger.error(ex.getMessage());
            }
        }
    }
    
    private void createComment(String messageTitle, Connection conn, HttpServletRequest request) throws Exception
    {
        CommentDAO commentDAO = new CommentDAO(conn);
        
        // Call insertComment() on OrderDAO passing the specified comment attached to the current order and customer
        CommentsVO commentVO = new CommentsVO();
        commentVO.setComment(messageTitle);
        commentVO.setCommentType(COMConstants.CUSTOMER_COMMENT_TYPE);
        commentVO.setOrderGuid(request.getParameter(COMConstants.ORDER_GUID));
        commentVO.setCustomerId(request.getParameter(COMConstants.CUSTOMER_ID));
        
        if (SecurityManager.getInstance().getUserInfo(request.getParameter(COMConstants.SEC_TOKEN))!=null)
        {
            String userId = SecurityManager.getInstance().getUserInfo(request.getParameter(COMConstants.SEC_TOKEN)).getUserID();
            commentVO.setCreatedBy(userId);
            commentVO.setUpdatedBy(userId);
        }
        
        commentVO.setCommentOrigin(request.getParameter(COMConstants.START_ORIGIN));
        commentVO.setDnisId(request.getParameter(COMConstants.H_DNIS_ID));
        commentVO.setReason("email");
        commentDAO.insertComment(commentVO);
    }
}