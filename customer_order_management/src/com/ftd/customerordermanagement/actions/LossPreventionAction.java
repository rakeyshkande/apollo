package com.ftd.customerordermanagement.actions;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.ftd.customerordermanagement.bo.CustomerOrderSearchBO;
import com.ftd.customerordermanagement.bo.LossPreventionBO;
import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;

/**
 * The LossPreventionAction will obtain customer, loss prevention and recipient order
 * data for a given customer.
 *
 * @author Matt Wilcoxen
 */

public class LossPreventionAction extends Action 
{
	private Logger logger = new Logger("com.ftd.customerordermanagement.actions.LossPreventionAction");
    
    //request parameters:
	private static final String R_ORDER_GUID = "order_guid";
	private static final String R_CUSTOMER_ID = "customer_id";
	private static final String R_REC_PAGE_POSITION = "rec_page_position";

    //XML nodes:
    private static final String X_OUT_PARAMETERS = "out-parameters";
    private static final String X_TAG_STATUS = "TAG_STATUS";
    private static final String X_ERR_MSG = "ERR_MSG";

    // XML document node names
    private static final String LP_DATA_NODE = "OUT_LP_INFO_CUR";
    private static final String LP_CUSTOMERS_NODE = "OUT_LP_CUSTOMERS_CUR";
    private static final String LP_PAGEDATA_NODE = "LP_PAGEDATA";

    private static final String ERROR_PAGE = "errorPage";


	/**
	 * This is the Tag Order Action called from the Struts framework.
     *     1. get db connection
     *     2. get parameters
     *     3. add customer nodes to root document
     *     4. add page data node to root document
     *     5. add loss prevention nodes to root document
     *     6. get additional parameters from request
     *     7. display Loss Prevention Action screen
	 * 
	 * @param mapping            - ActionMapping used to select this instance
	 * @param form               - ActionForm (optional) bean for this request
	 * @param request            - HTTP Request we are processing
	 * @param response           - HTTP Response we are processing
     * 
	 * @return forwarding action - next action to "process" request
     * 
	 * @throws IOException
	 * @throws ServletException
	 */
	public  ActionForward execute(ActionMapping mapping, ActionForm form, 
                                  HttpServletRequest request, HttpServletResponse response)
		throws IOException, ServletException
	{
        Connection conn = null;
        ConfigurationUtil cu = null;

        //String security = "";
        String securityToken  = null;
        String context  = "";
        String action   = "";
        String csrId = null;

        Document lpInfoXMLDoc = null;
        Document lpCustInfoXMLDoc = null;

        ActionForward forward = null;
		boolean callErrorPage = false;
        String errorMessage = null;
        File xslFile = null;
        HashMap parameters = new HashMap();

		try
		{
            cu = ConfigurationUtil.getInstance();

            // 1. get db connection
            conn = getDBConnection();
			
            String orderGuid = request.getParameter(R_ORDER_GUID);
            String customerId = request.getParameter(R_CUSTOMER_ID);
            String recipientPagePosition = request.getParameter(R_REC_PAGE_POSITION);

            CustomerOrderSearchBO coSearchBO = new CustomerOrderSearchBO(request, conn);
            HashMap custAcctInfo = coSearchBO.processRequestForLossPrevention(customerId, recipientPagePosition);

			LossPreventionBO lpBO = new LossPreventionBO(conn);
			HashMap lpData = lpBO.buildLossPreventionData(customerId, orderGuid);

            // 2. get parameters
            context = request.getParameter(COMConstants.CONTEXT);

            // 3. add customer nodes to root document
            Document lossPreventionXMLData = (Document) DOMUtil.getDocument();
            Document xmlDoc = null;
 
            xmlDoc = (Document) custAcctInfo.get(COMConstants.HK_CAI_CUSTOMER_XML);
            lossPreventionXMLData.getDocumentElement().appendChild(lossPreventionXMLData.importNode(xmlDoc.getDocumentElement(),true));
        
            xmlDoc = (Document) custAcctInfo.get(COMConstants.HK_CAI_EMAIL_XML);
            lossPreventionXMLData.getDocumentElement().appendChild(lossPreventionXMLData.importNode(xmlDoc.getDocumentElement(),true));
            
            xmlDoc = (Document) custAcctInfo.get(COMConstants.HK_CAI_ORDER_XML);
            lossPreventionXMLData.getDocumentElement().appendChild(lossPreventionXMLData.importNode(xmlDoc.getDocumentElement(),true));
        
            xmlDoc = (Document) custAcctInfo.get(COMConstants.HK_CAI_VIEWING_XML);
            lossPreventionXMLData.getDocumentElement().appendChild(lossPreventionXMLData.importNode(xmlDoc.getDocumentElement(),true));
        
            xmlDoc = (Document) custAcctInfo.get(COMConstants.HK_CAI_PHONE_XML);
            lossPreventionXMLData.getDocumentElement().appendChild(lossPreventionXMLData.importNode(xmlDoc.getDocumentElement(),true));

            // 4. add page data node to root document
            xmlDoc = (Document) custAcctInfo.get(COMConstants.PD_PAGE_DATA);
            lossPreventionXMLData.getDocumentElement().appendChild(lossPreventionXMLData.importNode(xmlDoc.getDocumentElement(),true));
 
            // 5. add loss prevention nodes to root document
            xmlDoc = (Document) lpData.get(LP_DATA_NODE);
            lossPreventionXMLData.getDocumentElement().appendChild(lossPreventionXMLData.importNode(xmlDoc.getDocumentElement(),true));

            xmlDoc = (Document) lpData.get(LP_CUSTOMERS_NODE);
            lossPreventionXMLData.getDocumentElement().appendChild(lossPreventionXMLData.importNode(xmlDoc.getDocumentElement(),true));

            xmlDoc = (Document) lpData.get(LP_PAGEDATA_NODE);
            lossPreventionXMLData.getDocumentElement().appendChild(lossPreventionXMLData.importNode(xmlDoc.getDocumentElement(),true));

			Document xmlDoc1 = lossPreventionXMLData;
			if(logger.isDebugEnabled())
			{
				//IMPORTANT:  Because StringWriter contents are NOT removed even when the flush
				//            method is executed, make StringWriter go away through scope control.
				//            Both the flush and close methods were tested with but were of non-affect.
				StringWriter sw = new StringWriter();       //string representation of xml document
				DOMUtil.print((Document) xmlDoc1,new PrintWriter(sw));
				logger.debug("execute(): Loss Prevention information = \n" + sw.toString());
				//for printing to the console --> DOMUtil.print((Document) xmlDoc,System.out);
			}

            // 6. get additional parameters from request
            parameters = (HashMap) request.getAttribute(COMConstants.CONS_APP_PARAMETERS);
            if(parameters == null)
            {
                parameters = new HashMap();
            }

            // 7. display Loss Prevention Action screen
            xslFile = getXSL(mapping, request, COMConstants.XSL_LOSS_PREVENTION);
            forward = mapping.findForward(COMConstants.XSL_LOSS_PREVENTION);
            String xslFilePathAndName = forward.getPath(); //get real file name
            
            // Change to client side transform
            TraxUtil.getInstance().getInstance().transform(request, response, lossPreventionXMLData, 
                                xslFile, xslFilePathAndName, (HashMap)request.getAttribute(COMConstants.CONS_APP_PARAMETERS));
            return null;
		}
		catch (ClassNotFoundException  cnfe)
		{
            errorMessage = cnfe.getMessage();
			logger.error(cnfe);
            callErrorPage = true;
        }
		catch (IOException  ioe)
		{
            errorMessage = ioe.getMessage();
			logger.error(ioe);
            callErrorPage = true;
		}
		catch (ParserConfigurationException  pcex)
		{
            errorMessage = pcex.getMessage();
			logger.error(pcex);
            callErrorPage = true;
		}
		catch (SAXException  sec)
		{
            errorMessage = sec.getMessage();
			logger.error(sec);
            callErrorPage = true;
		}
		catch (SQLException  sqle)
		{
            errorMessage = sqle.getMessage();
			logger.error(sqle);
            callErrorPage = true;
		}
		catch (TransformerException  tex)
		{
            errorMessage = tex.getMessage();
			logger.error(tex);
            callErrorPage = true;
		}
		catch (Throwable  t)
		{
            errorMessage = t.getMessage();
			logger.error(t);
            callErrorPage = true;
		}
		finally
		{
            try
            {
				if(conn != null)
				{
	                conn.close();
				}
                if(callErrorPage)
                {
                    return forward = mapping.findForward(ERROR_PAGE);
                }
            }
            catch (SQLException  se)
            {
                logger.error(se);
            }

            catch (Exception  ex)
            {
                logger.error(ex);
            }
		}

		return null;
	}


	/**
	 * Obtain connectivity with the database
	 * 
	 * @param none
     * 
	 * @return Connection - db connection
     * 
     * @throws Exception
     * @throws IOException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws TransformerException
     * @throws XSLException
	 */
	private Connection getDBConnection()
		throws IOException, ParserConfigurationException, SAXException, TransformerException, 
               Exception
	{
		Connection conn = null;
		conn = DataSourceUtil.getInstance().getConnection(
				   ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE,
															   COMConstants.DATASOURCE_NAME));

		return conn;
	}


	/**
	 * Retrieve name and location of XSL file
     *     1. get file lookup name
     *     2. get real file name
     *     3. get xsl file location
	 * 
	 * @param ActionMapping      - Stuts mapping used for actionforward lookup
     * @param HttpServletRequest - user request
	 * @param String             - name of xsl file
     * 
	 * @return File              - XSL File name with real path
     * 
	 * @throws none
	 */
    public File getXSL(ActionMapping mapping, HttpServletRequest request, String loss_prevention_file)
	{
		File XSLFile = null;
		String XSLFileLookUpName = null;
		String XSLFileName = null;

        // 1. get file lookup name
        XSLFileLookUpName = loss_prevention_file;

        // 2. get real file name
        ActionForward forward = mapping.findForward(XSLFileLookUpName);
        XSLFileName = forward.getPath();

        // 3. get xsl file location
        XSLFile = new File(this.getServlet().getServletContext().getRealPath(XSLFileName));
        return XSLFile;
    }
}