package com.ftd.customerordermanagement.actions;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.util.CallTimer;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.security.SecurityManager;

import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


/**
 * This class stops the call timer if it exists and redirects the user to the
 * appropriate Order Entry application.
 *
 * @author Mike Kruger
 */
public final class OrderEntryAction extends Action 
{
	private static    Logger logger  = new Logger("com.ftd.customerordermanagement.actions.OrderEntryAction");

    /**
     * This is the Order Entry Action called from the Struts framework.
	 * The execute method stops the call timer if it exists and redirects 
         * the user to the appropriate Order Entry application.
     * 
     * @param mapping  - ActionMapping used to select this instance
     * @param form     - ActionForm (optional) bean for this request
     * @param request  - HTTP Request we are processing
     * @param response - HTTP Response we are processing
     * @return forwarding action - next action to "process" request
     * @throws IOException
     * @throws ServletException
     */
	public  ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, 
		HttpServletResponse response) 
        throws IOException, ServletException		 
	{
		ActionForward forward = null;
		String callLogId = null;
		CallTimer callTimer = null;
                Connection conn = null;
		String dnis = null;
		String defaultDnis = null;
		ConfigurationUtil cu = null;
		String callPageURL = null;
		String securityToken = null;
		String context = null;
		
		try
		{
			// obtain an instance of ConfigurationUtil
			cu = ConfigurationUtil.getInstance();

			// get database connection
			conn =
			DataSourceUtil.getInstance().getConnection(cu.getProperty(COMConstants.PROPERTY_FILE,
														COMConstants.DATASOURCE_NAME));

			// stop the call timer if it exists
			callLogId = (String)request.getParameter(COMConstants.TIMER_CALL_LOG_ID);
                        if(callLogId ==  null || "".equals(callLogId) || callLogId.equalsIgnoreCase(COMConstants.TIMER_CALL_LOG_ID))
                          callLogId = (String)request.getAttribute(COMConstants.TIMER_CALL_LOG_ID);
                        if(callLogId != null && !"".equals(callLogId))
			{
				callTimer = new CallTimer(conn);
				callTimer.delete(callLogId);
			}

			// obtain DNIS
			dnis = (String)request.getParameter(COMConstants.H_DNIS_ID);
                        if(dnis ==  null || "".equals(dnis))
                          dnis = (String)request.getAttribute(COMConstants.H_DNIS_ID);
                        
			// obtain Security information
			securityToken = (String)request.getParameter(COMConstants.SEC_TOKEN);
			context = (String)request.getParameter(COMConstants.CONTEXT);
			
                        /* If the user has access to OldWebOE route the user
                         * to Web Order Entry otherwise route the user to JOE.
                         */ 
                        SecurityManager securityManager = SecurityManager.getInstance();
                        if(securityManager.assertPermission(context, securityToken, 
                            COMConstants.OLD_WEBOE, COMConstants.VIEW))
                        {
                            callPageURL  = cu.getFrpGlobalParm(COMConstants.COM_CONTEXT, COMConstants.CONS_WEB_OE_URL);
                            callPageURL += "?&sessionId=" + securityToken 
                                                    + "&reqsource=csrMenu"
                                                    + "&dnis=" + dnis;
                        }
                        else
                        {
                            callPageURL  = cu.getFrpGlobalParm(COMConstants.COM_CONTEXT, COMConstants.CONS_JOE_URL);
                            callPageURL += "?&securitytoken=" + securityToken
                                                    + "&context=" + context
                                                    + "&dnis=" + dnis;
			}
			
                        if(logger.isDebugEnabled())
                        {
                            logger.debug("callPageURL: " + callPageURL);
			}
                        
			response.sendRedirect(callPageURL);				
		}
		catch(Throwable t)
		{
			logger.error(t);			
              forward = mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);        
		}
		finally
		{
			try
			{
				if(conn != null)
				{
					conn.close();
				}
			}
			catch (SQLException se)
			{
				logger.error(se);
                forward = mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);        
			}
		}

		return forward;
    }
    
}
