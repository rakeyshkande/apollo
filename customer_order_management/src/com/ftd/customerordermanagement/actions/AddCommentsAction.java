package com.ftd.customerordermanagement.actions;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.dao.CommentDAO;
import com.ftd.customerordermanagement.dao.LockDAO;
import com.ftd.customerordermanagement.dao.OrderDAO;
import com.ftd.customerordermanagement.vo.CommentsVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityManager;

import java.io.File;
import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;

public final class AddCommentsAction extends Action 
{
    private static Logger logger  = new Logger("com.ftd.customerordermanagement.actions.AddCommentsAction");
    private static String YES = "Y";
    private static String NO = "N";
    private static String OUT_LOCK_OBTAINED = "OUT_LOCK_OBTAINED";
    private static String OUT_LOCKED_IND = "OUT_LOCKED_IND";
    private static String OUT_UPDATE_IND = "OUT_UPDATE_IND";
    private static String DEFAULT_USER_ID = "default user";
    private static String SUCCESS = "success";
  
  /**
   * This is the main action called from the Struts framework.
   * @param mapping The ActionMapping used to select this instance.
   * @param form The optional ActionForm bean for this request.
   * @param request The HTTP Request we are processing.
   * @param response The HTTP Response we are processing.
   */
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException
  {
    String commentType = null;
    int pagePosition = 0;
    HashMap secondaryMap = new HashMap();
    Document xmlLock = null;
    Document xmlComments = null;
    String userId = null;
    String lockId = null; //customer ID or order detail ID or order guid
    String customerId = null;
    String orderGuid = null;
    String managerRole = null;
    int maxRecords = 0;
    String commentOrigin = null;
    String orderDetailId = null;
    Connection conn = null;
    String actionType = null;
    CachedResultSet detailIds = null;

    try
    {
        // pull base document
        Document responseDocument = (Document) DOMUtil.getDocument();
        
        // get common request parameters
        // get action type
        if(request.getParameter(COMConstants.ACTION_TYPE)!=null)
        {
          actionType = request.getParameter(COMConstants.ACTION_TYPE);
        }
        else
        {
          //forward to error page
          logger.error("Missing action_type in AddCommentsAction");
          return mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
        }
        // get comment type
        if(request.getParameter(COMConstants.COMMENT_TYPE)!=null)
        {
          commentType = request.getParameter(COMConstants.COMMENT_TYPE);
          secondaryMap.put(COMConstants.COMMENT_TYPE, commentType);
        }
        else
        {
            //forward to error page
          logger.error("Missing comment_type in AddCommentsAction");
          return mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
        }
        // get customer ID
        if(request.getParameter(COMConstants.CUSTOMER_ID)!=null)
        {
          customerId = request.getParameter(COMConstants.CUSTOMER_ID);
        }
        secondaryMap.put(COMConstants.CUSTOMER_ID, customerId);
        // get order guid
        if(request.getParameter(COMConstants.ORDER_GUID)!=null)
        {
          orderGuid = request.getParameter(COMConstants.ORDER_GUID);
        }
        secondaryMap.put(COMConstants.ORDER_GUID, orderGuid);
        // get order detail id
        if(request.getParameter(COMConstants.ORDER_DETAIL_ID)!=null)
        {
          orderDetailId = request.getParameter(COMConstants.ORDER_DETAIL_ID);
        }
        secondaryMap.put(COMConstants.ORDER_DETAIL_ID, orderDetailId);
        // determine the lock id
        if(customerId!=null && !customerId.equals("") && commentType.equals(COMConstants.CUSTOMER_COMMENT_TYPE))
        {
          lockId =  customerId;
        }
        else
        if(orderDetailId!=null && !orderDetailId.equals("") && commentType.equals(COMConstants.ORDER_COMMENT_TYPE))
        {
          lockId = orderDetailId;
        }
        // get security token
        //String sessionId = "FTD_GUID_-21215273640-18575809880-16921224640-14167063930-320113690-11742279200758240883023319059226070618310100764667509408190270531727613252-616369349200517759397175-2208572067-2095968743232"; // modify this for security token
        String sessionId = (String) request.getParameter(COMConstants.SEC_TOKEN);
        if(sessionId==null)
        {
          logger.error("Security token not found in request object");
        }
        // get user ID
        // use security manager to get user info object. extract csr_id from it.
        // check for null pointers
        if (SecurityManager.getInstance().getUserInfo(sessionId)!=null)
        {
          userId = SecurityManager.getInstance().getUserInfo(sessionId).getUserID();
        }

        //check to see if the user-role can modify a comment
        boolean userInRole = false; 
    
        userInRole = SecurityManager.getInstance().assertPermission(request.getParameter("context"), sessionId, "Comments And Email Resource", "Update"); 
        secondaryMap.put("user_can_update", userInRole?"Y":"N");
                        
        // get database connection
        conn =
        DataSourceUtil.getInstance().getConnection(ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE,
                                                    COMConstants.DATASOURCE_NAME));
        
        // create a lock object
        LockDAO lock = new LockDAO(conn);
        
        // Lock check sub-action
        // Call the retrieveLockXML() on the LockDAO to return lock status XML data for specific ID.
        // The comment_type parameter will determine which type of ID to check the lock for.
        // If lock is available, the stored procedure will assign the lock to the current user.
        if(actionType.equals(COMConstants.ADD_COMMENT))
        {
          // Call the retrieveLockXML() on the LockDAO to return lock status XML data for specific ID.
          // The comment_type parameter will determine which type of ID to check the lock for.        
          xmlLock = lock.retrieveLockXML(sessionId, userId, lockId, COMConstants.CONS_ENTITY_TYPE_COMMENTS);
          
          // check if lock obtained is N. Then, forward without saving comment.
          if(DOMUtil.getNodeValue(xmlLock, OUT_LOCK_OBTAINED).equalsIgnoreCase(NO))
          {
            logger.debug("CSR lock is not available for adding comment.");
          }
                  
          // Append the XML returned from retrieveLock() to the primary document
          DOMUtil.addSection(responseDocument, xmlLock.getChildNodes());   
                      
          // Forward to iFrame XSL action
          //Get XSL File name
          String xslName = "LockedForAdd";
          File xslFile = getXSL(xslName, mapping);

          ActionForward forward = mapping.findForward(xslName);
          String xslFilePathAndName = forward.getPath(); //get real file name
          
          TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, xslFilePathAndName, (HashMap)request.getAttribute(COMConstants.CONS_APP_PARAMETERS));          
          return null;
        }
        
        // Submit (save) comment sub-action
        //get all the comment parameters in the request object and build CommentVO object
        if(actionType.equals(COMConstants.SAVE_COMMENT) || 
           actionType.equals(COMConstants.CART_SAVE_COMMENT) || 
           actionType.equals(COMConstants.EDIT_COMMENT) ||
           actionType.equals(COMConstants.DELETE_COMMENT))
        {
          CommentsVO comment = new CommentsVO();
          // check lock one more time before saving comments
          xmlLock = lock.retrieveLockXML(sessionId, userId, lockId, COMConstants.CONS_ENTITY_TYPE_COMMENTS);
          
          // check if lock obtained is N. Then, forward without saving comment.
          if(DOMUtil.getNodeValue(xmlLock, OUT_LOCK_OBTAINED).equalsIgnoreCase(NO))
          {
            // you come here only when current user times out after pushing Add button
            // Append the XML returned from retrieveLock() to the primary document
            logger.debug("CSR lock is not available for saving comment.");
            DOMUtil.addSection(responseDocument, xmlLock.getChildNodes());          
                        
            // Forward to iFrame XSL action
            //Get XSL File name
            String xslName = "LockedForSave";
            File xslFile = getXSL(xslName, mapping);  
            ActionForward forward = mapping.findForward(xslName);
            String xslFilePathAndName = forward.getPath(); //get real file name
            
            // Change to client side transform
            TraxUtil.getInstance().getInstance().transform(request, response, responseDocument, 
                                xslFile, xslFilePathAndName, (HashMap)request.getAttribute(COMConstants.CONS_APP_PARAMETERS));
            return null;
          }
          // Append the XML returned from retrieveLock() to the primary document
          DOMUtil.addSection(responseDocument, xmlLock.getChildNodes());          
           
          // set up comment object
          comment.setCommentType(commentType);          
          comment.setCustomerId(customerId);

          if(request.getParameter(COMConstants.COMMENT_ID)!=null)
          {
            comment.setCommentId(request.getParameter(COMConstants.COMMENT_ID));
          }

          if(commentType.equals(COMConstants.ORDER_COMMENT_TYPE))
          {
            comment.setOrderGuid(orderGuid);
          }
          
          // get order detail id
          if(request.getParameter(COMConstants.ORDER_DETAIL_ID)!=null && commentType.equals(COMConstants.ORDER_COMMENT_TYPE))
          {
            comment.setOrderDetailId(request.getParameter(COMConstants.ORDER_DETAIL_ID));
          }
          // get comment origin
          comment.setCommentOrigin(request.getParameter(COMConstants.START_ORIGIN));
          // get comment reason
          if(request.getParameter(COMConstants.COMMENT_REASON)!=null)
          {
            comment.setReason(request.getParameter(COMConstants.COMMENT_REASON));
          }
          // get comment text
          if(request.getParameter(COMConstants.COMMENT_TEXT)!=null)
          {
            comment.setComment(request.getParameter(COMConstants.COMMENT_TEXT));
          }
          // get dnis id
          if(request.getParameter(COMConstants.H_DNIS_ID)!=null && !request.getParameter(COMConstants.H_DNIS_ID).equals(""))
          {
            comment.setDnisId(request.getParameter(COMConstants.H_DNIS_ID));
          }
          // get created_by
          if(request.getParameter(COMConstants.CREATED_BY)!=null)
          {
            comment.setCreatedBy(request.getParameter(COMConstants.CREATED_BY));
          }
          else
          {
            comment.setCreatedBy(userId);            
          }
          // get updated_by
          if(request.getParameter(COMConstants.UPDATED_BY)!=null)
          {
            comment.setUpdatedBy(request.getParameter(COMConstants.UPDATED_BY));
          }
          else
          {
            comment.setUpdatedBy(userId);            
          }
             
          CommentDAO commentDAO = new CommentDAO(conn);
          if (actionType.equals(COMConstants.EDIT_COMMENT))
          {
            //check if the comment id was passed or not.  If comment id was not passsed, throw an exception. 
            if (comment.getCommentId() == null || comment.getCommentId().equalsIgnoreCase(""))
              throw new Exception("Cannot Edit a comment because comment_id was not passed");
            
            //else edit the comment
            commentDAO.editComment(comment);
          }
          else if (actionType.equals(COMConstants.DELETE_COMMENT))
          {
            //Create a Calendar Object for todays date
            Calendar cToday = Calendar.getInstance();

            //Define a date format 
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
            
            //create the comment text
            String commentText = "Comment deleted by " + userId + " on " + sdf.format(cToday.getTime());
          
            //and store it in the VO
            comment.setComment(commentText);
            
            //else edit the comment
            commentDAO.editComment(comment);
          }
          else
          {
            // Call the insert method on CommentsDAO passing the value object to be saved.
            if(commentType.equals(COMConstants.CUSTOMER_COMMENT_TYPE))
            {
              commentDAO.insertComment(comment);
            }
            else // order comments
            {
              OrderDAO orderDAO = new OrderDAO(conn);
              // loop through order detail ids for a cart save or use just one order detail id for one item
              // get a list of order detail ids for cart save using order guid
              if(actionType.equals(COMConstants.CART_SAVE_COMMENT))
              {
                detailIds = orderDAO.viewDetailIds(orderGuid);
                while (detailIds.next())
                {
                  comment.setOrderDetailId(detailIds.getObject(1).toString());
                  commentDAO.insertComment(comment);
                }
              } else
              {
                comment.setOrderDetailId(orderDetailId);
                commentDAO.insertComment(comment);
              }
            }
          }

          // release the lock before forwarding to load comments page
          lock.releaseLock(sessionId, userId, lockId, COMConstants.CONS_ENTITY_TYPE_COMMENTS);
        }
         
        // Forward to LoadCommentsAction
        return mapping.findForward(SUCCESS);
    }
    catch (Throwable ex)
    {
      try {
        if(conn!=null)
          conn.close();
      }
      catch(SQLException sqlex)
      {
        logger.error(sqlex);
        return mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);        
      }
      logger.error(ex);
      return mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
    }
    finally 
    {
      try
      {
        if(conn!=null)
          conn.close();
      }
      catch (SQLException se)
      {
        logger.error(se);
        return mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
      }
    }
  }

  /******************************************************************************
  *                                     getXSL()
  *******************************************************************************
  * Retrieve name of Customer Order Search Action XSL file
  * @param1 String - the xsl name returned from the Business Object
  * @param2 ActionMapping
  * @return File - XSL File name
  * @throws none
  */

  private File getXSL(String xslName, ActionMapping mapping)
  {

    File xslFile = null;
    String xslFilePathAndName = null;

    ActionForward forward = mapping.findForward(xslName);
    xslFilePathAndName = forward.getPath();                //get real file name
    xslFile = new File(this.getServlet().getServletContext().getRealPath(xslFilePathAndName));
    return xslFile;
  }
}