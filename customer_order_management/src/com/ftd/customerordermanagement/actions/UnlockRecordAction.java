package com.ftd.customerordermanagement.actions;

import com.ftd.customerordermanagement.util.functors.DateFormatTransformer;
import com.ftd.customerordermanagement.util.functors.NodeUpdateClosure;
import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.dao.LockDAO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;

import java.io.File;
import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.apache.commons.collections.Closure;
import org.apache.commons.collections.Transformer;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * This class is used to retrieve csr locked records and to release
 * csr locked records.  
 */
public final class UnlockRecordAction extends Action 
{
    private static Logger logger  = new Logger("com.ftd.customerordermanagement.actions.UnlockRecordAction");
    private static String UNLOCK_RECORD = "unlockRecord";
    private static String CSR_LOCKED_RECORDS = "csrLockedRecords";
    private static String NO_RECORDS_FOUND = "NO RECORDS FOUND FOR CSR.";
    private static String CSR_ID = "in_csr_id";
    private static final String IN_FORMAT = "yyyy-MM-dd hh:mm:ss";
    private static final String OUT_FORMAT = "MM/dd/yyyy h:mm a";

  
  /**
   * This is the main action called from the Struts framework.
   * @param mapping The ActionMapping used to select this instance.
   * @param form The optional ActionForm bean for this request.
   * @param request The HTTP Request we are processing.
   * @param response The HTTP Response we are processing.
   */
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException
  {
    Document xmlCsrLockedEntities = null;
    Connection conn = null;
    Node Node = null;
    Node itemNode = null;
    String csrId = null; 
    String action = null;
    String entitytype = null;
    String entityid = null;
    
    
    try
    {
        ActionForward forward;

        // pull base document
        Document responseDocument = (Document) DOMUtil.getDocument();
        
        // get database connection
        conn =
        DataSourceUtil.getInstance().getConnection(ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE,
                                                    COMConstants.DATASOURCE_NAME));
          
        //get all the parameters in the request object
        
        // retrieve action
        action = request.getParameter(COMConstants.ACTION);
               
        //if action = load
        //just forward user to the unlockRecord.xsl page
        if(action.equalsIgnoreCase("load"))
        {
          //when user clicks the back button on the csrLockedRecords page, we
          //need to return the csr_id so we can repopulate the csr id text box with the csr id
          //that was searched on.
          if(request.getParameter(this.CSR_ID)!=null)
          {
                csrId = request.getParameter(this.CSR_ID);
                
                HashMap pageData = new HashMap();
                pageData.put(this.CSR_ID, csrId);
                
                DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true);;        
          }
          String displayPage = this.UNLOCK_RECORD;
          
          File xslFile = getXSL(displayPage, mapping);  
          forward = mapping.findForward(displayPage);
          String xslFilePathAndName = forward.getPath(); //get real file name
          
          // Change to client side transform
          TraxUtil.getInstance().getInstance().transform(request, response, responseDocument, 
                              xslFile, xslFilePathAndName, (HashMap)request.getAttribute(COMConstants.CONS_APP_PARAMETERS));
       
        }
        
              
        //if action equals get_locked_records, retrieved all records locked
        //by the csr id passed in the request
        if(action.equalsIgnoreCase("get_locked_records"))
        {
          csrId = request.getParameter(this.CSR_ID);
          
          HashMap pageData = new HashMap();
          pageData.put(this.CSR_ID, csrId);   
                     
          xmlCsrLockedEntities = this.retrieveCSRLockedEntities(conn, csrId);
          
          try
          {
            //need to check to see if we got any results back from our search
            //if we do get results back forward user on to the csrLockedRecords screen
            Node = (Node)DOMUtil.selectSingleNode(xmlCsrLockedEntities,"/csr_locked_entities/csr_locked_entity");
            itemNode =  DOMUtil.selectSingleNode(Node,"entity_type/text()");
            
            DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true);
            DOMUtil.addSection(responseDocument, xmlCsrLockedEntities.getChildNodes());

            // transform all dates
            Transformer transformer = DateFormatTransformer.getInstance(IN_FORMAT, OUT_FORMAT);
            Closure closure = NodeUpdateClosure.getInstance(transformer, responseDocument);
            NodeList list = responseDocument.getElementsByTagName("locked_on");
            for ( int i = 1; i <= list.getLength(); i++ ) {
              closure.execute( "root/csr_locked_entities/csr_locked_entity[" + i +"]/locked_on/text()" );
            }

            String displayPage = this.CSR_LOCKED_RECORDS;
            File xslFile = getXSL(displayPage, mapping);  
            forward = mapping.findForward(displayPage);
            String xslFilePathAndName = forward.getPath(); //get real file name
            
            // Change to client side transform
            TraxUtil.getInstance().getInstance().transform(request, response, responseDocument, 
                                xslFile, xslFilePathAndName, (HashMap)request.getAttribute(COMConstants.CONS_APP_PARAMETERS));
          }//end try
          catch(Exception e)
          {
             //if we are in here, that means we got a null pointer exception when trying
             //to parse the value for entity_type.  This means there were no records
             //locked for the csr id provided.
             pageData.put("message_display", this.NO_RECORDS_FOUND); 
             DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true);
             DOMUtil.addSection(responseDocument, xmlCsrLockedEntities.getChildNodes());
             
             String displayPage = this.UNLOCK_RECORD;
             File xslFile = getXSL(displayPage, mapping);  
             forward = mapping.findForward(displayPage);
             String xslFilePathAndName = forward.getPath(); //get real file name
             
             // Change to client side transform
             TraxUtil.getInstance().getInstance().transform(request, response, responseDocument, 
                                 xslFile, xslFilePathAndName, (HashMap)request.getAttribute(COMConstants.CONS_APP_PARAMETERS));
          }//end catch
        }//end if
     
        //if action = release_all
        if(action.equalsIgnoreCase("release_all"))
        {
          csrId = request.getParameter(this.CSR_ID);
          
          //pass in csr id, and null for both entity type and entity id to release all
          //locks for this csr
          this.releaseLockForCsr(conn, csrId, null, null);
          
          //return the user to the customer service main menu
          this.redirectToMainMenu(request, response);
       
        }//end if
        
             
        //if action = release_selected_records
        if(action.equalsIgnoreCase("release_selected_records"))
        {
          csrId = request.getParameter(this.CSR_ID);
           
          //get all entity types/entity ids that need to be release
          //iterate through that list calling the this.releaseCsrLockedRecord
          String[] entitytypeArray = request.getParameterValues("entitytype");
          String[] entityidArray = request.getParameterValues("entityid");

          if (entitytypeArray != null && entityidArray != null) 
          
          for (int i = 0; i < entitytypeArray.length; i++) 
            {
              entitytype = entitytypeArray[i];
              entityid   = entityidArray[i]; // both arrays are assumed to be equal in length
                           
              if(entitytype.length() > 0 )
              {
                logger.debug("release the lock for this csr: " + csrId + ", " + entitytype + ", " + entityid);
                this.releaseLockForCsr(conn, csrId, entitytype, entityid);
              }//end if
            }//end for loop

          //return the user to the customer service main menu
          this.redirectToMainMenu(request, response);
          
        }//end if
        
             
       return null;
    }
    catch (Throwable ex)
    {
      try
      {
        
        if(conn!=null)
          conn.close();
      }
      catch (SQLException se)
      {
        logger.error(se);
        return mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
      }
      logger.error(ex);
      return mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
    }
    finally 
    {
      try
      {
        if(conn!=null)
          conn.close();
      }
      catch (SQLException se)
      {
        logger.error(se);
        return mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
      }
    }
  }

  /******************************************************************************
  *                                     getXSL()
  *******************************************************************************
  * Retrieve name of Customer Order Search Action XSL file
  * @param1 String - the xsl name returned from the Business Object
  * @param2 ActionMapping
  * @return File - XSL File name
  * @throws none
  */

  private File getXSL(String xslName, ActionMapping mapping)
  {

    File xslFile = null;
    String xslFilePathAndName = null;

    ActionForward forward = mapping.findForward(xslName);
    xslFilePathAndName = forward.getPath();                //get real file name
    xslFile = new File(this.getServlet().getServletContext().getRealPath(xslFilePathAndName));
    return xslFile;
  }
  
   
  /******************************************************************************
  * redirectToMainMenu
  *******************************************************************************
  * Take user back to the customer service main menu
  * @param1 request The HTTP Request we are processing.
  * @param2 response The HTTP Response we are processing.
  * @throws Exception
  */

  private void redirectToMainMenu(HttpServletRequest request, HttpServletResponse response) throws Exception
  {       
          String mainMenuUrl = "";
          //main menu url
          mainMenuUrl = ConfigurationUtil.getInstance().getFrpGlobalParm(COMConstants.COM_CONTEXT, COMConstants.CONS_MAIN_MENU_URL);
          
          //append adminAction
          mainMenuUrl += "?adminAction=";
          mainMenuUrl += (String)request.getParameter("adminAction");
  
          //append security to the main menu url
          mainMenuUrl += "&context=";
          mainMenuUrl += (String)request.getParameter("context");
          mainMenuUrl += "&securitytoken=";
          mainMenuUrl += (String)request.getParameter("securitytoken");
          logger.debug("mainMenuURL: " + mainMenuUrl);
          response.sendRedirect(mainMenuUrl);
  }
  
   /**
   * @param1 Connection - conn
   * @param String - csrId
   * @return Document - csr locked entities
   * @throws java.lang.Exception
   */
    private Document retrieveCSRLockedEntities(Connection conn, String csrId) throws Exception
    {
        //Instantiate LockDAO
        LockDAO lockDAO = new LockDAO(conn);
    
        //Document that will contain the output from the stored proce
        Document csrLockedEntitiesXML = DOMUtil.getDocument();
        
        //Call getCSRLockedEntities method in the DAO
        csrLockedEntitiesXML = lockDAO.getCsrLockedEntities(csrId);
           
        return csrLockedEntitiesXML;
    }

/**
   * @param1 Connection - conn
   * @param String - csrId
   * @return Document - csr locked entities
   * @throws java.lang.Exception
   */
    private void releaseLockForCsr(Connection conn, String csrId, String entityType, String entityId) throws Exception
    {
      //Instantiate LockDAO
      LockDAO lockDAO = new LockDAO(conn);
  
      //Call releaseLocksForCsr method in the DAO
      lockDAO.releaseLocksForCsr(csrId, entityType, entityId);
      
    }
    

}