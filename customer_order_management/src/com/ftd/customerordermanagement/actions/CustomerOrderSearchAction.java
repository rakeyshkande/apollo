package com.ftd.customerordermanagement.actions;

import com.ftd.customerordermanagement.bo.CustomerOrderSearchBO;
import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.filter.DataFilter;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityManager;
import com.ftd.decisionresult.constant.DecisionResultConstants;
import com.ftd.decisionresult.filter.DecisionResultDataFilter;

import java.io.File;
import java.io.OutputStream;

import java.io.PrintWriter;
import java.io.StringWriter;

import java.sql.Connection;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.commons.lang.StringUtils;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

 


public final class CustomerOrderSearchAction extends Action 
{
    private static Logger logger  = new Logger("com.ftd.customerordermanagement.actions.CustomerOrderSearchAction");

  /**
   * This is the main action called from the Struts framework.
   * @param mapping The ActionMapping used to select this instance.
   * @param form The optional ActionForm bean for this request.
   * @param request The HTTP Request we are processing.
   * @param response The HTTP Response we are processing.
   */
  public  ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, 
                                HttpServletResponse response) 
          
  {
		ActionForward forward = null;
    Connection con = null;
    String action = null;
    ConfigurationUtil cu = null;
    boolean cdispPermission = false;
    boolean cdispActive = false;
    
    try
    {
      String xslName = "";
      
  
      //Connection/Database info
      con = DataSourceUtil.getInstance().getConnection(
            ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE,
            COMConstants.DATASOURCE_NAME));
  
  
      //Document that will contain the final XML, to be passed to the TransUtil and XSL page
      Document responseDocument = (Document) DOMUtil.getDocument();
  
      //This Hashmap will be used to hold all the XML objects, page data, and search criteria that 
      //will be returned from the business object
      HashMap responseHash = new HashMap();
  
      //Get info passed in the request object
      action = getRequestInfo(request);
  
      //Call the Business Object, which will return a hashmap containing
      //    0 to Many - XML documents 
      //    HashMap1  = pageData - hashmap that contains String data at page level
      CustomerOrderSearchBO cosBO = new CustomerOrderSearchBO(request, con);
      responseHash = cosBO.processRequest(action);
      
      SecurityManager securityManager = SecurityManager.getInstance();
      String updateAllowed = null;
      String context = (String) request.getParameter(COMConstants.CONTEXT);
      String token = (String) request.getParameter(COMConstants.SEC_TOKEN);

      String  customerHoldPermission = null;
      if (securityManager.assertPermission(context, token, "CustomerHold", COMConstants.UPDATE))
      {
          customerHoldPermission = "Y";
      }
      else
      {
         customerHoldPermission = "N";
      }
      
      String lossPreventionPermission = null;
      if (securityManager.assertPermission(context, token, "LossPreventionUpdate", COMConstants.UPDATE))
      {
          lossPreventionPermission = "Y";
      }
      else
      {
         lossPreventionPermission = "N";
      }
      String vipCustUpdatePermission = null;
      if(securityManager.assertPermission(context, token, COMConstants.VIP_CUSTOMER_UPDATE, COMConstants.YES))
      { 
    	  vipCustUpdatePermission = "Y";
      }
      else
      {
    	  vipCustUpdatePermission = "N";
      }

      // Since user may be returning to COM from accounting (Reconciliation Inquiry or Billing Transaction pages)
      // we need to reinitialize any Call Disposition variables since they will be gone
      //
      cu = ConfigurationUtil.getInstance();
      cdispActive = ("Y".equalsIgnoreCase(cu.getFrpGlobalParm(COMConstants.COM_CONTEXT, COMConstants.CONS_CDISP_ACTIVE_FLAG))?true:false);
      cdispPermission = securityManager.assertPermission(context, token, COMConstants.CDISP_ADMIN, COMConstants.VIEW);
      String dnisId = request.getParameter(COMConstants.H_DNIS_ID);

      // Call Disposition is only enabled if global flag indicates it's active and
      // DNIS was specified (and not '0000') and CSR has proper permission
      //
      if (cdispActive && StringUtils.isNotBlank(dnisId) && !StringUtils.trim(dnisId).equals("0000") && cdispPermission) {
          DataFilter.updateParameter(request, COMConstants.CDISP_ENABLED_FLAG, "Y");
          DecisionResultDataFilter.updateParameter(request, DecisionResultConstants.REQ_DNIS_ID, dnisId);
          if (logger.isDebugEnabled()) {
            logger.debug("Call Disposition renabled for this user, DNIS=" + dnisId);
          }
      }
      
      String vipCustomerMessage = null;
      vipCustomerMessage= cu.getContentWithFilter(con, "VIP_CUSTOMER", COMConstants.COM_CONTEXT, "VIP_CUSTOMER_MESSAGE", null);
      
      String vipCustUpdateDeniedMessage = null;
      vipCustUpdateDeniedMessage= cu.getContentWithFilter(con, "VIP_CUSTOMER", COMConstants.COM_CONTEXT, "VIP_CUSTOMER_UPDATE_DENIED_MESSAGE", null);
      
      //After the Business Object returns control back, we need to populate the XML document that 
      //will be passed to the Transform Utility.  This entails:
      //  1)  append all the XML documents from the responseHash to this returnDocument
      //  2)  convert pageData in responseHash (from HashMap to Document), and append 
      //      to returnDocument
  
      //Get all the keys in the hashmap returned from the business object
      Set ks = responseHash.keySet();
      Iterator iter = ks.iterator();
      String key; 
      
      //Iterate thru the hashmap returned from the business object using the keyset
      while(iter.hasNext())
      {
        key = iter.next().toString();
        
        if (key.equalsIgnoreCase("pageData"))
        {
          //The page data hashmap within the main hashmap contains an object whose key is "XSL "
          //This key refers to the page where the control will be transferred to.  However, this key
          //should not be included in the page data XML.  
          xslName = (String)((HashMap) responseHash.get("pageData")).get("XSL").toString();
          ((HashMap)responseHash.get("pageData")).remove("XSL");
          HashMap pageData = (HashMap) responseHash.get("pageData");
          
          // add security permission for customerHold and lossPrevention, vipCustUpdatePermission, vipCustomerMessage and vipCustUpdateDeniedMessage//
          pageData.put("customerHoldPermission", customerHoldPermission);
          pageData.put("lossPreventionPermission", lossPreventionPermission);
          pageData.put("vipCustUpdatePermission", vipCustUpdatePermission);
          pageData.put("vipCustomerMessage", vipCustomerMessage);
          pageData.put("vipCustUpdateDeniedMessage", vipCustUpdateDeniedMessage);
          //Convert the page data hashmap to XML and append it to the final XML
          DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true); 
        }
        else
        {
          //Append all the existing XMLs to the final XML
          Document xmlDoc = (Document) responseHash.get(key);
          NodeList nl = xmlDoc.getChildNodes();
          DOMUtil.addSection(responseDocument, nl);
        }
      }
     
      if (xslName.equalsIgnoreCase(COMConstants.XSL_REFUND_UPDATE_ACTION))
      {
        forward = mapping.findForward(COMConstants.XSL_REFUND_UPDATE_ACTION);
        request.setAttribute(COMConstants.ACTION, COMConstants.ACTION_CUSTOMER_ACCOUNT_SEARCH);
        return forward;
      }
      else
      {
        //Get XSL File name
        File xslFile = getXSL(xslName, mapping);        
        if (xslFile != null && xslFile.exists()) {
        //Transform the XSL File
        try
        {
          forward = mapping.findForward(xslName);
          String xslFilePathAndName = forward.getPath(); //get real file name
          
          // Change to client side transform
          TraxUtil.getInstance().getInstance().transform(request, response, responseDocument, 
                              xslFile, xslFilePathAndName, (HashMap)request.getAttribute(COMConstants.CONS_APP_PARAMETERS));
        }
        catch (Exception e)
        {
          logger.error("XSL file " + (xslFile == null ? "null" : xslFile.getCanonicalPath()) + " transformation failed for Customer Id = " +
                      request.getParameter(COMConstants.CUSTOMER_ID) +
                      " and external order number = " +
                      request.getParameter(COMConstants.EXTERNAL_ORDER_NUMBER) +
                      " and master order number = " +
                      request.getParameter(COMConstants.MASTER_ORDER_NUMBER) +
                      " and action = " + xslName, e);
          if (!response.isCommitted()) 
          {
            throw e;
          }
        }
        } else 
        {
        throw new Exception("XSL file " + (xslFile == null ? "null" : xslFile.getCanonicalPath()) + " does not exist for Customer Id = " +
                      request.getParameter(COMConstants.CUSTOMER_ID) +
                      " and external order number = " +
                      request.getParameter(COMConstants.EXTERNAL_ORDER_NUMBER) +
                      " and master order number = " +
                      request.getParameter(COMConstants.MASTER_ORDER_NUMBER) +
                      " and action = " + xslName);
        }
        
        return null;
      }

    }

    catch(Exception e)
    {
      logger.error(e);
      forward = mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
      return forward;
    }

    finally
    {
      try
      {
        //Close the connection
        if(con != null)
        {
          con.close();
        }
      }
      catch(Exception e)
      {
        logger.error(e);
        forward = mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
        return forward;
      }
    }
   
  }



  /******************************************************************************
  *             getRequestInfo(HttpServletRequest request)
  *******************************************************************************
  * Retrieve the info from the request object, and set class level variables
  * @param  HttpServletRequest
  * @return none 
  * @throws none
  */
  private String getRequestInfo(HttpServletRequest request)
  {
    String action = null;
    
    if(request.getAttribute(COMConstants.ACTION) != null)
      action = request.getAttribute(COMConstants.ACTION).toString();
    else if(request.getParameter(COMConstants.ACTION)!=null)
      action = request.getParameter(COMConstants.ACTION);

    return action;
  }



  /******************************************************************************
  *                                     getXSL()
  *******************************************************************************
  * Retrieve name of Customer Order Search Action XSL file
  * @param1 String - the xsl name returned from the Business Object
  * @param2 ActionMapping
  * @return File - XSL File name
  * @throws none
  */

  private File getXSL(String xslName, ActionMapping mapping)
  {

    File xslFile = null;
    String xslFilePathAndName = null;

    ActionForward forward = mapping.findForward(xslName);
    xslFilePathAndName = forward.getPath();                //get real file name
    xslFile = new File(this.getServlet().getServletContext().getRealPath(xslFilePathAndName));
    return xslFile;
  }


}