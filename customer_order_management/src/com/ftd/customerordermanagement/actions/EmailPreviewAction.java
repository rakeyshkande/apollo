package com.ftd.customerordermanagement.actions;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.fop.apps.Driver;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import com.ftd.ftdutilities.FieldUtils;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;


public final class EmailPreviewAction extends Action 
{
  /**
   * This is the main action called from the Struts framework.
   * @param mapping The ActionMapping used to select this instance.
   * @param form The optional ActionForm bean for this request.
   * @param request The HTTP Request we are processing.
   * @param response The HTTP Response we are processing.
   */
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException, Exception
    {
    
        try
        {
            Document responseDocument = (Document) DOMUtil.getDocument();
            HashMap letterValues = new HashMap();
            
            // Pull logo name from database based on company id
            
            letterValues.put("logo", this.getServlet().getServletContext().getRealPath("/images/" + "fusa.gif"));
            
            String body = request.getParameter("body");
            
            body = body.replaceAll("\\r", "");
            body = body.replaceAll("\\n", "</fo:block><fo:block>");
            body = FieldUtils.replaceAll(body, "<fo:block></fo:block>", "<fo:block space-after.optimum='15pt'></fo:block>");
            
            letterValues.put("body", body);
            DOMUtil.addSection(responseDocument, "letter", "detail", letterValues, true);
            
            File xslFile = new File(this.getServlet().getServletContext().getRealPath("/xsl/letterfo.xsl"));
            String foString = TraxUtil.getInstance().transform(responseDocument, xslFile, null);
            
            StringReader sr = new StringReader(foString);
            renderFO(new InputSource(sr), response);
        }
        catch (Throwable ex)
        {
          ex.printStackTrace(); 
        }
        finally 
        {
        }
        
        return null;
    }
    
    public void renderFO(InputSource foFile,
                         HttpServletResponse response) throws ServletException {
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();

            response.setContentType("application/pdf");

            Driver driver = new Driver(foFile, out);
           // driver.setLogger(log);
            driver.setRenderer(Driver.RENDER_PDF);
            driver.run();

            byte[] content = out.toByteArray();
            response.setContentLength(content.length);
            response.getOutputStream().write(content);
            response.getOutputStream().flush();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}