package com.ftd.customerordermanagement.actions;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import com.ftd.customerordermanagement.bo.GiftCardBO;
import com.ftd.customerordermanagement.bo.RefundBO;
import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.dao.LockDAO;
import com.ftd.customerordermanagement.dao.OrderDAO;
import com.ftd.customerordermanagement.dao.RefundDAO;
import com.ftd.customerordermanagement.dao.ServicesProgramDAO;
import com.ftd.customerordermanagement.dao.ViewDAO;
import com.ftd.customerordermanagement.util.TimerFilter;
import com.ftd.customerordermanagement.vo.OrderDetailVO;
import com.ftd.customerordermanagement.vo.OrderVO;
import com.ftd.customerordermanagement.vo.PaymentVO;
import com.ftd.ftdutilities.FTDCommonUtils;
import com.ftd.messaging.vo.FTDMessageVO;
import com.ftd.mo.util.ModifyOrderUTIL;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.handlers.GlobalParmHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.PartnerVO;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityManager;

/**
 * LoadRefundAction class
 *
 * @author Brian Munter
 */

public final class LoadRefundAction extends Action
{
    private static Logger logger  = new Logger("com.ftd.customerordermanagement.actions.LoadRefundAction");
    private static String OUT_CUR = "OUT_CUR";
    private static String OUT_ID_COUNT = "OUT_ID_COUNT";
    private static String OUT_PARAMETERS = "out-parameters";
    private static String REFUND_PAGE = "refund";
    private static String PG_REFUND_PAGE = "pg_refund";
    private static String REFUND_REVIEW_PAGE = "refund_review";
    private static String REFUND_PERCENT_PAGE = "refund_percent";
    private static String OUT_CODES_STRING = "OUT_CODES_STRING";

  /**
   * This is the main action called from the Struts framework.
   *
   * @param mapping The ActionMapping used to select this instance.
   * @param form The optional ActionForm bean for this request.
   * @param request The HTTP Request we are processing.
   * @param response The HTTP Response we are processing.
   */
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
        Connection conn = null;
        ActionForward forward = null;

        try
        {
            String action = request.getParameter("action_type");

            // Action which loads the refund review screen
            if("RefundReview".equals(action))
            {
                conn = refundReview(mapping, request, response);
            }
            // Action which loads the refund detail
            else
            {
                conn = refundDetails(mapping, request, response);
            }

            return null;
        }
        catch (Throwable ex)
        {
            logger.error(ex);
            forward = mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
            return forward;
        }
        finally
        {
            try
            {
                conn.close();
            }
            catch (SQLException se)
            {
                logger.error(se);
            }
        }
    }

    private Connection refundDetails(ActionMapping mapping, HttpServletRequest request, HttpServletResponse response)
    throws ParserConfigurationException, Exception, TransformerException, IOException, SAXException, SQLException
    {
        Connection conn;
        ActionForward forward;
        Document responseDocument = (Document) DOMUtil.getDocument();
        boolean isNewRoute = false;
        conn = DataSourceUtil.getInstance().getConnection(ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, COMConstants.DATASOURCE_NAME));

        String orderDetailId = request.getParameter("order_detail_id");

        // Retrieve lock so payments and refunds can not be updated while user is viewing
        String securityFlag = ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, COMConstants.SECURITY_IS_ON);
        if(securityFlag != null && securityFlag.equals("true"))
        {
            String securityToken = (String) request.getParameter(COMConstants.SEC_TOKEN);

            if (SecurityManager.getInstance().getUserInfo(securityToken) != null)
            {
                LockDAO lockObj = new LockDAO(conn);
                Document xmlLock = lockObj.retrieveLockXML(securityToken, SecurityManager.getInstance().getUserInfo(securityToken).getUserID(), orderDetailId, COMConstants.CONS_ENTITY_TYPE_PAYMENTS, COMConstants.CONS_ENTITY_TYPE_ORDER_DETAILS);
                DOMUtil.addSection(responseDocument, xmlLock.getChildNodes());
            }

            // Load the csr viewing data
            Document csrViewingXML = this.retrieveCSRViewing(conn, request.getParameter("order_detail_id"), SecurityManager.getInstance().getUserInfo(securityToken).getUserID(), securityToken);
            DOMUtil.addSection(responseDocument, csrViewingXML.getChildNodes());
        }

        // Load the refund dispositions
        RefundDAO refundDAO = new RefundDAO(conn);
        
        isNewRoute = refundDAO.isNewPaymentRoute(orderDetailId);
        
        Document dispositionXML = refundDAO.loadRefundDispositionsXML(null);
        DOMUtil.addSection(responseDocument, dispositionXML.getChildNodes());

        // Load the responsible parties
        Document orderFloristsXML = refundDAO.loadOrderFloristsXML(orderDetailId);
        DOMUtil.addSection(responseDocument, orderFloristsXML.getChildNodes());

        // Load the mercury/venus parameters
        Document orderFullfillmentXML = refundDAO.loadOrderFulfillmentXML(orderDetailId);
        DOMUtil.addSection(responseDocument, orderFullfillmentXML.getChildNodes());

        // Load the complaint comm types
        Document complaintCommTypes = refundDAO.loadComplaintCommTypeXML();
        DOMUtil.addSection(responseDocument, complaintCommTypes.getChildNodes());

        // Load the payment and refund details
        RefundBO refundBO = new RefundBO(request, conn);
        responseDocument = refundBO.loadRefund(responseDocument, orderDetailId, isNewRoute);

        // Find out if order is vendor and in transit, thus cannot be canceled
        ModifyOrderUTIL moUTIL = new ModifyOrderUTIL(conn);
        FTDMessageVO lastFtdMessageVO = moUTIL.getMessageDetail(orderDetailId);
        Date dShipDate = lastFtdMessageVO.getShipDate();
        Date dDeliveryDate = lastFtdMessageVO.getDeliveryDate();
        boolean isOrderInTransitBoolean = moUTIL.isOrderInTransit(orderDetailId, dShipDate, dDeliveryDate);

        OrderDAO orderDAO = new OrderDAO(conn);
        OrderDetailVO orderDetailVO = orderDAO.getOrderDetail(orderDetailId);
        String isOrderInTransit = "N";
        if (isOrderInTransitBoolean){
            isOrderInTransit = "Y";
        }

        boolean isZoneJumpOrderInTransitBoolean = moUTIL.isZJOrderInTransit(orderDetailId);
        String isZoneJumpOrderInTransit = "N";
        if (isZoneJumpOrderInTransitBoolean){
            isZoneJumpOrderInTransit = "Y";
        }

        // See if order was placed too long ago for a refund (the front-end will
        // pop-up an confirmation if so)
        String exceededRefundMaxDaysOut = "N";
        String orderGuid = orderDetailVO.getOrderGuid();
        OrderVO orderVO = orderDAO.getOrder(orderGuid);
        //changing this check to look at delivery date instead of order date
        if (orderDetailVO.getDeliveryDate() != null) {
            Date deliveryDate  = orderDetailVO.getDeliveryDate().getTime();
            Date todaysDate = Calendar.getInstance().getTime();
            int refundDayLimit;
            try {
            	GlobalParmHandler globalParms = (GlobalParmHandler)CacheManager.getInstance().getHandler("CACHE_NAME_GLOBAL_PARM");
            	refundDayLimit = Integer.parseInt(globalParms.getGlobalParm("CUSTOMER_ORDER_MANAGEMENT", "MAX_REFUND_DAYS_OUT"));
            } catch (Exception e) {
                logger.error("Defaulting refund max day limit since error occurred: " + e);
                refundDayLimit = 183;                        
            }
            long dateDiff = (todaysDate.getTime() - deliveryDate.getTime())/(1000*3600*24);  // Diff in days
            if (dateDiff > refundDayLimit) {
                exceededRefundMaxDaysOut = "Y";
            }
        }
        HashMap pageData = new HashMap();
        String preferredPartner = "N";
        PartnerVO partnerVO = FTDCommonUtils.getPreferredPartnerBySource(orderVO.getSourceCode());
        if (partnerVO != null)
        {
            preferredPartner = "Y";
        }
        pageData.put("preferred_partner", preferredPartner);

        
        // Add the pagedata to XML
        pageData.put("in_transit", isOrderInTransit);
        pageData.put("zj_order_in_transit", isZoneJumpOrderInTransit);
        pageData.put("exceededRefundMaxDaysOut", exceededRefundMaxDaysOut);
        
        
        //Determine if Fully Refund Merchandising Amount flag at Source Code Level.
        String fullyRefundMerchAmtFlag = orderDAO.getFullyRefundMerchAmountFlag(orderDetailVO.getSourceCode());
        logger.info("fullyRefundMechAmtFlag  "+fullyRefundMerchAmtFlag);
        if(fullyRefundMerchAmtFlag == null) fullyRefundMerchAmtFlag ="Y"; //Default to True
        pageData.put("fullyRefundMechAmtFlag",fullyRefundMerchAmtFlag);
        
        setFreeShippingData(conn, pageData, orderDetailVO);
        
        // Determine if gift card payments are enabled
        setGiftCardEnabled(conn, orderGuid, responseDocument);

        // Set the customer email address into page data
        setCustomerEmail(conn, orderDetailId, pageData);
       
        logger.info("PAGE DATA: " + pageData);
        
        DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true);

//        printDocument(responseDocument);

        
        // Load the XSL
        if(isNewRoute) {
        	forward = mapping.findForward(PG_REFUND_PAGE);
        } else {
        	forward = mapping.findForward(REFUND_PAGE);	
        }
        
        String xslFileName = forward.getPath();

        // Perform transformation
        File xslFile = new File(this.getServlet().getServletContext().getRealPath(xslFileName));
        String xslFilePathAndName = forward.getPath(); //get real file name

        // Change to client side transform
        TraxUtil.getInstance().getInstance().transform(request, response, responseDocument, 
                xslFile, xslFilePathAndName, (HashMap)request.getAttribute(COMConstants.CONS_APP_PARAMETERS));
        return conn;
    }

    /**
     * Determine if gift card payments are enabled for C30 refunds. This is based on 2 factors:
     * 1. the global parm for gift card payments
     * 2. if there is already a gift card payment for this order. We don't allow >1 gift card payment per order
     * @param conn
     * @param respDoc
     * @throws Exception
     */
    private void setGiftCardEnabled(Connection conn, String orderGuid, Document respDoc) throws Exception
    {
        // check global parm
        Document gdGlobalSwitch = GiftCardBO.getGiftCardGlobalSwitch(conn);
        Node gdNode = DOMUtil.selectSingleNode(gdGlobalSwitch, "GIFTCARD_SETTINGS/gift_card_payment_enabled/text()");
        String gdEnabled = gdNode.getNodeValue();
        if ("Y".equals(gdEnabled))
        {

            // check for gd payments
            OrderDAO dao = new OrderDAO(conn);           
            List <PaymentVO> payments = dao.getGiftCardPayments(orderGuid);
            if (payments != null && !payments.isEmpty())
            {
                gdNode.setNodeValue("N");
            }
        }

        DOMUtil.addSection(respDoc, gdGlobalSwitch.getChildNodes());
    }

    private Connection refundReview(ActionMapping mapping, HttpServletRequest request, HttpServletResponse response)
    throws ParserConfigurationException, Exception, TransformerException, IOException, SAXException, SQLException
    {
        Connection conn;
        ActionForward forward;
        Document responseDocument = (Document) DOMUtil.getDocument();
        conn = DataSourceUtil.getInstance().getConnection(ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, COMConstants.DATASOURCE_NAME));

        // Start timer if needed
        String entityHistoryId = request.getParameter(COMConstants.TIMER_ENTITY_HISTORY_ID);

        if(entityHistoryId == null || entityHistoryId.equals(""))
        {
            entityHistoryId = this.timerStart(conn, request.getParameter(COMConstants.SEC_TOKEN));
        }

        // Pull page position from request
        int pagePosition = 0;

        final String pagePositionAsString = request.getParameter(COMConstants.PAGE_POSITION);

        if(null != pagePositionAsString)
        {
            try
            {
                pagePosition = Integer.parseInt(request.getParameter(COMConstants.PAGE_POSITION));
            }
            catch (Exception e) 
            {
                // Catch non-numeric parsing exceptions
            }

            pagePosition = pagePosition > 0 ? pagePosition : 1; // cannot be zero or negative!
        }

        // Pull max refunds per page from config file
        int maxRecs = 0;
        String maxRecords = ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, COMConstants.CONS_MAX_REFUNDS_PER_PAGE);
        if(maxRecords != null && !maxRecords.equals(""))
        {
            maxRecs = Integer.parseInt(maxRecords);
        }

        // Calculate start number
        int startNumber = (pagePosition-1) * maxRecs + 1;

        // Pull current user id from token
        String userId = "TEST";
        String securityFlag = ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, COMConstants.SECURITY_IS_ON);
        if(securityFlag != null && securityFlag.equals("true"))
        {
            String securityToken = (String) request.getParameter(COMConstants.SEC_TOKEN);

            if (SecurityManager.getInstance().getUserInfo(securityToken) != null)
            {
                userId = SecurityManager.getInstance().getUserInfo(securityToken).getUserID();
            }
        }
        
        //Column data sorting parameters reading.
        String currentSortStat =(String)request.getParameter(COMConstants.CURRENT_SORT_COLUMN_STATUS); 
        currentSortStat =currentSortStat !=null?currentSortStat.trim().length()>0?currentSortStat:COMConstants.DEFAULT_SORT_COLUMN_STATUS:COMConstants.DEFAULT_SORT_COLUMN_STATUS;
        String sortColumn =(String)request.getParameter(COMConstants.SORT_COLUMN_NAME); 
        if(sortColumn == null){
        	currentSortStat="ASC";
        }
        sortColumn =sortColumn !=null?sortColumn.trim().length()>0?sortColumn:COMConstants.DEFAULT_SORT_COLUMN_NAME:COMConstants.DEFAULT_SORT_COLUMN_NAME;
        String sortStatus =(String)request.getParameter(COMConstants.SORT_COLUMN_STATUS);
        sortStatus =sortStatus !=null?sortStatus.trim().length()>0?sortStatus:COMConstants.DEFAULT_SORT_COLUMN_STATUS:COMConstants.DEFAULT_SORT_COLUMN_STATUS;
        String isFromHeader =(String)request.getParameter(COMConstants.IS_FROM_HEADER); 
       
        //Filters Data capturing
        String userIdFilter =(String)request.getParameter(COMConstants.USER_ID_FILTER); 
        userIdFilter =userIdFilter !=null?userIdFilter.trim().length()>0?userIdFilter:"":"";
        String groupIdFilter =(String)request.getParameter(COMConstants.GROUP_ID_FILTER); 
        groupIdFilter =groupIdFilter !=null?groupIdFilter.trim().length()>0?groupIdFilter:"":"";
        String codesFilter =(String)request.getParameter(COMConstants.CODES_FILTER); 
        codesFilter =codesFilter !=null?codesFilter.trim().length()>0?codesFilter:"":"";
        String codesFilterData ="";
        if(codesFilter != null && codesFilter.trim().length() >0){
        	String codes[]=codesFilter.split(",");
        	for(String code:codes){
        		codesFilterData =codesFilterData.trim().length()>0?codesFilterData+","+"'"+code.trim()+"'":"'"+code.trim()+"'";	
        	}
        }
        
        // Load refunds for review and add to XML
        RefundDAO refundDAO = new RefundDAO(conn);
        //Method has changed to pass more parameters like sortColumn,sortStatus,userIdFilter,groupIdFilter,codesFilter.
        HashMap refundReviewMap = refundDAO.loadRefundReviewXML(userId, startNumber, maxRecs,sortColumn,sortStatus,userIdFilter,groupIdFilter,codesFilterData);
        Document xmlRefundReview = (Document) refundReviewMap.get(OUT_CUR);
        int totalCount = Integer.parseInt(DOMUtil.getNodeValue((Document) refundReviewMap.get(OUT_PARAMETERS), OUT_ID_COUNT));
        String codesData =(DOMUtil.getNodeValue((Document) refundReviewMap.get(OUT_PARAMETERS), OUT_CODES_STRING)).toString();
        DOMUtil.addSection(responseDocument, xmlRefundReview.getChildNodes());
        
        LinkedHashMap<String,String> codes = new LinkedHashMap<String,String>();//This is holding both selected codes and unselected codes.
        LinkedHashMap<String,String> unSelectedCodes = new LinkedHashMap<String,String>();
        if(codesData != null && codesData.trim().length() >0){
        	codesData =codesData.replace("'","");
        	String codess[] =codesData.split(",");
        	for(String code:codess){
        		if(codesFilter !=null && codesFilter.trim().length() >0 && codesFilter.indexOf(code) >= 0){
        			codes.put(code, "true");//Selected value.
        		}else{
        			unSelectedCodes.put(code, "false");	
        		}
        	}
        	codes.putAll(unSelectedCodes);//Here adding unselected codes after selected codes.
        }
       
        // Calculate the total pages
        int totalPage = (int) Math.floor(totalCount / maxRecs);
        if(totalCount%maxRecs > 0)
            totalPage += 1;

        // Assure the start position is still valid
        while(startNumber > totalCount)
        {
            startNumber -= maxRecs;
            pagePosition--;
        }

        /* If the totalCount is 0 the startNumber will become negative.
         * The startNumber and pagePosition should be set back to 1.
         * To be safe the startNumber is tested for a negative value after the loop
         * instead of testing that totalCount equals 0, bypassing the loop,
         * and setting startNumber and pagePosition equal to 1.
         */ 
        if(startNumber < 1)
        {
            startNumber = 1;
            pagePosition = 1;
        }

        // Add the pagedata to XML
        HashMap pageData = new HashMap();
        pageData.put(COMConstants.TIMER_ENTITY_HISTORY_ID, entityHistoryId);
        pageData.put(COMConstants.PAGE_POSITION, new Integer(pagePosition).toString());
        pageData.put(COMConstants.CONS_MAX_REFUNDS_PER_PAGE, maxRecords);
        pageData.put(COMConstants.START_POSITION, new Integer(startNumber).toString());
        pageData.put(COMConstants.PD_TOTAL_PAGES, new Integer(totalPage).toString());
        pageData.put(COMConstants.SORT_COLUMN_NAME,sortColumn);
        pageData.put(COMConstants.SORT_COLUMN_STATUS,sortStatus);
        if(isFromHeader !=null && isFromHeader.trim().equalsIgnoreCase("true")){//If it is from header then it will chnaged the status.
        	if(sortStatus != null && sortStatus.trim().equalsIgnoreCase("DESC")){
        		pageData.put(COMConstants.CURRENT_SORT_COLUMN_STATUS,"ASC");
        	}else{
        		pageData.put(COMConstants.CURRENT_SORT_COLUMN_STATUS,"DESC");
        	}
        }else{
        	pageData.put(COMConstants.CURRENT_SORT_COLUMN_STATUS,currentSortStat);
        }
        pageData.put(COMConstants.USER_ID_FILTER,userIdFilter);
        pageData.put(COMConstants.GROUP_ID_FILTER,groupIdFilter);
        pageData.put(COMConstants.CODES_FILTER,codesFilter);
        DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true);
        
        //Adding Codes to xml document.
        DOMUtil.addSection(responseDocument, "CODES", "CODE", codes, true);

       // logger.info(JAXPUtil.toString(responseDocument));
        // Load the XSL
        forward = mapping.findForward(REFUND_REVIEW_PAGE);
        String xslFileName = forward.getPath();

        // Perform transformation
        File xslFile = new File(this.getServlet().getServletContext().getRealPath(xslFileName));
        String xslFilePathAndName = forward.getPath(); //get real file name

        // Change to client side transform
        TraxUtil.getInstance().getInstance().transform(request, response, responseDocument, 
                xslFile, xslFilePathAndName, (HashMap)request.getAttribute(COMConstants.CONS_APP_PARAMETERS));
        return conn;
    }

    private Document retrieveCSRViewing(Connection conn, String entityId, String userId, String securityToken) throws Exception
    {
        //Instantiate ViewDAO
        ViewDAO viewDAO = new ViewDAO(conn);

        //Document that will contain the output from the stored proce
        Document csrViewingXML = DOMUtil.getDocument();

        //Call getCSRViewed method in the DAO
        csrViewingXML = viewDAO.getCSRViewing(securityToken, userId, "ORDER_DETAILS", entityId);

        return csrViewingXML;
    }

    private String timerStart(Connection connection, String securityToken) throws Exception
    {
        TimerFilter tFilter = new TimerFilter(connection);
        HashMap timerResults = new HashMap();

        String csrId = null;
        if (SecurityManager.getInstance().getUserInfo(securityToken)!=null)
        {
            csrId = SecurityManager.getInstance().getUserInfo(securityToken).getUserID();
        }

        timerResults =  tFilter.startTimer( COMConstants.CONS_ENTITY_TYPE_USER, securityToken,
                                            COMConstants.CONS_REFUND_REVIEW_ORIGIN, csrId,
                                            COMConstants.CONS_COM_COMMENT_ORIGIN_TYPE,
                                            null);

        return timerResults.get(COMConstants.TIMER_ENTITY_HISTORY_ID).toString();
    }

   /**
   * If the Item is a Free Shipping Membership, populates the free shipping fields.
   * 
   * @param connection
   * @param pageData
   * @param orderDetailVO
   */
    private void setFreeShippingData(Connection connection, HashMap pageData, OrderDetailVO orderDetailVO)
      throws Exception
    {
        ServicesProgramDAO servicesProgramDAO = new  ServicesProgramDAO();     
        if(servicesProgramDAO.isProductFreeShipping(connection, orderDetailVO.getProductId()))
        {
          pageData.put("refundDisp", "B60");
          pageData.put(COMConstants.FREESHIP_MEMBERSHIP_ORDER, "Y");
          pageData.put(COMConstants.FREESHIP_PROGRAM_NAME, servicesProgramDAO.getFreeShippingProgramInfo().getDisplayName());
        } else 
        {
          pageData.put(COMConstants.FREESHIP_MEMBERSHIP_ORDER, "N");
          pageData.put(COMConstants.FREESHIP_PROGRAM_NAME, "");            
        }
    }
    
    /** Defect 1814  - Add Email link to refunds screen.
     * get the customer email address and set into page data. 
     * @param connection
     * @param orderDetailId
     * @param pageData
     * @throws Exception
     */
    private void setCustomerEmail(Connection connection, String orderDetailId, HashMap pageData) throws Exception {
        OrderDAO orderAccess = new OrderDAO(connection);
        CachedResultSet rs = orderAccess.getOrderCustomerInfo(orderDetailId);
        if(rs.next()) {
        	if(!StringUtils.isEmpty(rs.getString("email_address"))) {
        		 pageData.put("msg_email_indicator", true);
        	}
        }
    }
	//TODO: should remove logging xml after successful testing
    public static void printDocument(Document doc) throws IOException, TransformerException {
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = tf.newTransformer();
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
        transformer.setOutputProperty(OutputKeys.METHOD, "xml");
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
        StringWriter outWriter = new StringWriter();
        StreamResult result = new StreamResult( outWriter );
        transformer.transform(new DOMSource(doc), result );  
        StringBuffer sb = outWriter.getBuffer(); 
        
        logger.info("printDocument :::: " + sb.toString());
        
    }
}