package com.ftd.customerordermanagement.actions;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.dao.LockDAO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityManager;

import java.io.File;
import java.io.IOException;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;

import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;



/**
 * LockAction class
 *
 */

public final class LockAction extends Action
{
    private static Logger logger  = new Logger("com.ftd.customerordermanagement.actions.LockAction");

  /**
   * This is the main action called from the Struts framework.
   *
   * @param mapping The ActionMapping used to select this instance.
   * @param form The optional ActionForm bean for this request.
   * @param request The HTTP Request we are processing.
   * @param response The HTTP Response we are processing.
   */
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
        Connection conn = null;
        HashMap pageData = new HashMap();

        try
        {
            // Set request variables
            String action = request.getParameter("action_type");
            String securityToken = (String) request.getParameter(COMConstants.SEC_TOKEN);

						//note: this is only for testing.  This initialization should be removed before going to Production
						if (securityToken == null || securityToken.equals(""))
							securityToken = "FTD_GUID_152110970-17679447720-20941587290-6907072310-10953264730124013835701947683073011955788001-1283253546020013916030-7696321941251307593-2130522201491121547217254-15091192821131515399335142";

            String lockId = (String) request.getParameter(COMConstants.CONS_LOCK_ID);
            String lockApp = (String) request.getParameter(COMConstants.CONS_LOCK_APP);
            String orderLevel = (String) request.getParameter(COMConstants.CONS_ORDER_LEVEL);
            String forwardAction = (String) request.getParameter("forward_action");

            //retrieve the user id
            String userId = SecurityManager.getInstance().getUserInfo(securityToken).getUserID();
            if (userId == null || userId.equalsIgnoreCase(""))
              userId = "SYS";
              
            pageData.put("forward_action", forwardAction);

            //Document that will contain the final XML, to be passed to the TransUtil and XSL page
            Document responseDocument = (Document) DOMUtil.getDocument();


            // Pull DB connection
            conn = DataSourceUtil.getInstance().getConnection(ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, COMConstants.DATASOURCE_NAME));

            // Unlock ID
            if( action != null
                && action.equals("unlock")
                && lockId != null
                && !lockId.equals(""))
            {
                String securityFlag = ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, COMConstants.SECURITY_IS_ON);
                if(securityFlag != null && securityFlag.equals("true"))
                {
                    if (SecurityManager.getInstance().getUserInfo(securityToken) != null)
                    {
                        LockDAO lockObj = new LockDAO(conn);
                        lockObj.releaseLock(securityToken, userId, lockId, lockApp);
                    }
                }
            }
            // Retrieve Lock
            else if( action != null
                && action.equalsIgnoreCase("retrieve")
                && lockId != null
                && !lockId.equals(""))
            {
                String securityFlag = ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, COMConstants.SECURITY_IS_ON);
                if(securityFlag != null && securityFlag.equals("true"))
                {
                    if (SecurityManager.getInstance().getUserInfo(securityToken) != null)
                    {
                        Document lockInfo = DOMUtil.getDocument();

                        LockDAO lockObj = new LockDAO(conn);

                        //Call retrieveLockInfo which will call the DAO to retrieve the results
                        lockInfo = lockObj.retrieveLockXML(securityToken, userId, lockId, lockApp, orderLevel);

                        //get the lock obtained indicator.
                        String sLockedObtained = DOMUtil.getNodeValue(lockInfo, "OUT_LOCK_OBTAINED");

                        //if locked_obtained = yes, you have the lock (regardless if you just established a lock
                        //or have had it for a while).  Else, somebody else has the record locked
                        if (sLockedObtained.equalsIgnoreCase(COMConstants.CONS_NO))
                        {
                          //get the locked csr id
                          String sCsrId = DOMUtil.getNodeValue(lockInfo, "OUT_LOCKED_CSR_ID");

                          //set an output message
                          String message = COMConstants.MSG_RECORD_O_LOCKED1 + sCsrId + COMConstants.MSG_RECORD_O_LOCKED2;

                          pageData.put(COMConstants.PD_MESSAGE_DISPLAY,   message);
                          pageData.put(COMConstants.PD_GOT_LOCK,          COMConstants.CONS_NO);
                        }
                        else
                        {
                          pageData.put(COMConstants.PD_GOT_LOCK,          COMConstants.CONS_YES);
                        }

                        //Convert the page data hashmap to XML and append it to the final XML
                        DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true);

                        //set the iFrame name to be given control back to.
                        String xslName = COMConstants.XSL_CHECK_LOCK_IFRAME;

                        //Get XSL File name
                        File xslFile = getXSL(xslName, mapping);

                        ActionForward forward = mapping.findForward(xslName);
                        String xslFilePathAndName = forward.getPath(); //get real file name
                        
                        // Change to client side transform
                        TraxUtil.getInstance().getInstance().transform(request, response, responseDocument, 
                                            xslFile, xslFilePathAndName, (HashMap)request.getAttribute(COMConstants.CONS_APP_PARAMETERS));
                    }//end "if (SecurityManager..."
                }//end "if (securityFlag..."
            }//end else if(action.equalsIgnoreCase("retrieve"))
            // Check Lock
            else if( action != null
                && action.equalsIgnoreCase("check")
                && lockId != null
                && !lockId.equals(""))
            {
                String securityFlag = ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, COMConstants.SECURITY_IS_ON);
                if(securityFlag != null && securityFlag.equals("true"))
                {
                    if (SecurityManager.getInstance().getUserInfo(securityToken) != null)
                    {
                        Document lockInfo = DOMUtil.getDocument();

                        LockDAO lockObj = new LockDAO(conn);
                        logger.debug("orderLevel");
                        logger.debug(orderLevel);
                        //Call retrieveLockInfo which will call the DAO to retrieve the results
                        lockInfo = lockObj.checkLockXML(securityToken, userId, lockId, lockApp, orderLevel);

                        //get the locked indicator.  If somebody else has the lock, this will have
                        //a value = 'Y'.  If you have the lock, or nobody has the lock, it will
                        //carry a value of 'N'.
                        String sLockedIndicator = DOMUtil.getNodeValue(lockInfo, "OUT_LOCKED_IND");

                        if (sLockedIndicator.equalsIgnoreCase(COMConstants.CONS_YES))
                        {
                          //get the locked csr id
                          String sCsrId = DOMUtil.getNodeValue(lockInfo, "OUT_LOCKED_CSR_ID");

                          //set an output message
                          String message = COMConstants.MSG_RECORD_O_LOCKED1 + sCsrId + COMConstants.MSG_RECORD_O_LOCKED2;

                          pageData.put(COMConstants.PD_MESSAGE_DISPLAY,   message);
                          pageData.put(COMConstants.PD_GOT_LOCK,          COMConstants.CONS_NO);
                        }
                        else
                        {
                          pageData.put(COMConstants.PD_GOT_LOCK,          COMConstants.CONS_YES);
                        }

                        //Convert the page data hashmap to XML and append it to the final XML
                        DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true);

                        //set the iFrame name to be given control back to.
                        String xslName = COMConstants.XSL_CHECK_LOCK_IFRAME;

                        //Get XSL File name
                        File xslFile = getXSL(xslName, mapping);

                        ActionForward forward = mapping.findForward(xslName);
                        String xslFilePathAndName = forward.getPath(); //get real file name
                        
                        // Change to client side transform
                        TraxUtil.getInstance().getInstance().transform(request, response, responseDocument, 
                                            xslFile, xslFilePathAndName, (HashMap)request.getAttribute(COMConstants.CONS_APP_PARAMETERS));
                    }//end "if (SecurityManager..."
                }//end "if (securityFlag..."
            }//end else if(action.equalsIgnoreCase("check"))
            else if( action != null
                && action.equalsIgnoreCase("reset_scrub_lock")
                && lockId != null
                && !lockId.equals(""))
            {
                String securityFlag = ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, COMConstants.SECURITY_IS_ON);
                if(securityFlag != null && securityFlag.equals("true"))
                {
                    if (SecurityManager.getInstance().getUserInfo(securityToken) != null)
                    {
                        LockDAO lockObj = new LockDAO(conn);
                        
                        HashMap searchResults = lockObj.findMasterOrderNumber(lockId);                        
                        String lockFlag = lockObj.isCsrLastInScrub((String) searchResults.get("OUT_MASTER_ORDER_NUMBER"), userId);

                        // Make sure the current user still has lock
                        if(lockFlag != null && lockFlag.equals("Y"))
                        {
                            // reset scrub stautus
                            lockObj.resetScrubStatus((String) searchResults.get("OUT_MASTER_ORDER_NUMBER"));
                        }
                        
                        // Unlock customer lock from scrub
                        lockObj.releaseLock(securityToken, userId, ((BigDecimal) searchResults.get("OUT_CUSTOMER_ID")).toString(), "CUSTOMER");
                        
                    }//end "if (SecurityManager..."
                }//end "if (securityFlag..."
            }//end else if(action.equalsIgnoreCase("check"))
        }//end try
        catch (Throwable ex)
        {
            logger.error(ex);
        }
        finally
        {
            try
            {
                conn.close();
            }
            catch (SQLException se)
            {
                logger.error(se);
            }
        }

        return null;
    }


  /******************************************************************************
  *                                     getXSL()
  *******************************************************************************
  * Retrieve name of Customer Order Search Action XSL file
  * @param1 String - the xsl name returned from the Business Object
  * @param2 ActionMapping
  * @return File - XSL File name
  * @throws none
  */

  private File getXSL(String xslName, ActionMapping mapping)
  {

    File xslFile = null;
    String xslFilePathAndName = null;

    ActionForward forward = mapping.findForward(xslName);
    xslFilePathAndName = forward.getPath();                //get real file name
    xslFile = new File(this.getServlet().getServletContext().getRealPath(xslFilePathAndName));
    return xslFile;
  }


}