package com.ftd.customerordermanagement.actions;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.dao.LockDAO;
import com.ftd.customerordermanagement.dao.StockMessageDAO;
import com.ftd.customerordermanagement.vo.StockMessageVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.PartnerUtility;
import com.ftd.osp.utilities.PreferredPartnerUtility;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.order.MercentOrderPrefixes;
import com.ftd.osp.utilities.order.vo.MercentChannelMappingVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.PartnerMappingVO;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityManager;


public final class EditMessageMaintAction extends Action 
{
    private static Logger logger  = new Logger("com.ftd.customerordermanagement.actions.EditMessageMaintAction");
    private static String YES = "Y";
    private static String NO = "N";
    private static String OUT_LOCK_OBTAINED = "OUT_LOCK_OBTAINED";
    private static String OUT_LOCKED_IND = "OUT_LOCKED_IND";
    private static String OUT_UPDATE_IND = "OUT_UPDATE_IND";
    private static String DEFAULT_USER_ID = "default user";
    private static String SUCCESS = "success";
    private static String FAILURE = "failure";
    private static String VALIDATION_SUCCESS = "VALIDATION_SUCCESS";
  
  /**
   * This is the main action called from the Struts framework.
   * @param mapping The ActionMapping used to select this instance.
   * @param form The optional ActionForm bean for this request.
   * @param request The HTTP Request we are processing.
   * @param response The HTTP Response we are processing.
   */
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException
  {
		ActionForward forward = null;
    String messageType = null;
    String messageId = request.getParameter(COMConstants.MESSAGE_ID);
    HashMap secondaryMap = new HashMap();
    Document xmlLock = null;
    String userId = null;
    String lockId = null; //emailId or letterId or messageId
    String appName = null;
    String managerRole = null;
    Connection conn = null;
    String actionType = null;
    String newMessageFlag = null;
    Document msgContentXML = null;
    String autoEmail = null;
    String originId = null;
    String originUpdateFlag = null;

    try
    {
        // pull base document
        Document responseDocument = (Document) DOMUtil.getDocument();
        
        // get database connection
        conn =
        DataSourceUtil.getInstance().getConnection(ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE,
                                                    COMConstants.DATASOURCE_NAME));
        
        // get common request parameters
        // get action type
        if(request.getParameter(COMConstants.ACTION_TYPE)!=null)
        {
          actionType = request.getParameter(COMConstants.ACTION_TYPE);
          secondaryMap.put(COMConstants.ACTION_TYPE, actionType);
        }
        else
        {
          //forward to error page
          logger.error("Missing action_type in EditMessageMaintAction");
          return mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
        }
        // get message type
        if(request.getParameter(COMConstants.MESSAGE_TYPE)!=null)
        {
          messageType = request.getParameter(COMConstants.MESSAGE_TYPE);
          secondaryMap.put(COMConstants.MESSAGE_TYPE, messageType);
        }
        else
        {
          //forward to error page
          logger.error("Missing messageType in EditMessageMaintAction");
          return mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
        }
        // get new message flag
        newMessageFlag = request.getParameter(COMConstants.NEW_MESSAGE_FLAG);
        secondaryMap.put(COMConstants.NEW_MESSAGE_FLAG, newMessageFlag);
        
        // get origin update/change flag
        if(request.getParameter(COMConstants.ORIGIN_UPDATE_FLAG)!=null)
        {
          originUpdateFlag = request.getParameter(COMConstants.ORIGIN_UPDATE_FLAG);
          secondaryMap.put(COMConstants.ORIGIN_UPDATE_FLAG, originUpdateFlag);
        }
        
        SecurityManager securityManager = SecurityManager.getInstance();
        if (securityManager.assertPermission((String) request.getParameter(COMConstants.CONTEXT), (String) request.getParameter(COMConstants.SEC_TOKEN), ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, COMConstants.MESSAGE_MAINTENANCE), COMConstants.ADD))
        {
            secondaryMap.put("add_new_flag", "Y"); 
        }
        else
        {
            secondaryMap.put("add_new_flag", "N"); 
        }
        
        // get automated email flag
        if(request.getParameter(COMConstants.AUTO_EMAIL_FLAG)!=null)
        {
          autoEmail = "true";
        }
        else
        {
          autoEmail = "false";
        }
        // get origin id
        originId = request.getParameter(COMConstants.ORIGIN_ID);
        
        // determine the lock id
        if(messageId!=null && !messageId.equals(""))
        {
          lockId =  messageId;
        }
        // store the message header as xml document in the response
        String messageTitle = request.getParameter(COMConstants.MESSAGE_TITLE);
        
        Document msgHeaderXML = DOMUtil.getDocument("<StockMessageHeader><message_id>"+messageId
          +"</message_id><title>"+messageTitle+"</title><auto_email>"+autoEmail
          +"</auto_email></StockMessageHeader>");
                    
        StockMessageVO msgObj = new StockMessageVO();
          
        // create the stock message DAO
        StockMessageDAO msgAccess = new StockMessageDAO(conn);
        // get applicationName
        appName = "Stock "+messageType+" Maintenance";
        
        // get security token
        String sessionId = (String) request.getParameter(COMConstants.SEC_TOKEN);
        if(sessionId==null)
        {
          logger.error("Security token not found in request object");
        }
        // get user ID
        // use security manager to get user info object. extract csr_id from it.
        // check for null pointers
        if (SecurityManager.getInstance().getUserInfo(sessionId)!=null)
        {
          userId = SecurityManager.getInstance().getUserInfo(sessionId).getUserID();
        }
                        
        // create a lock object
        LockDAO lock = new LockDAO(conn);
        
        // Lock check sub-action
        // If lock is available, the stored procedure will assign the lock to the current user.
        if(actionType.equals(COMConstants.LOAD_MSG_CONTENT))
        {
            String previousMessageId = request.getParameter("previous_msg_id");
            
            // Unlock currently view message if exists
            if(previousMessageId != null && !previousMessageId.equals(""))
            {
                lock.releaseLock(sessionId, userId, previousMessageId, appName);
            }
        
          // Call the retrieveLockXML() on the LockDAO to return lock status XML data for specific ID.
          // The messageType parameter will determine which type of ID to check the lock for.        
          xmlLock = lock.retrieveLockXML(sessionId, userId, lockId, appName);
          
          // check if lock obtained is N. Then, forward without saving comment.
          if(DOMUtil.getNodeValue(xmlLock, OUT_LOCK_OBTAINED).equalsIgnoreCase(NO))
          {
            logger.debug("CSR lock is not available for loading Email/Letter content.");
            
            //load the message content                        
            msgContentXML = msgAccess.loadMessageContent(messageType, messageId);
            DOMUtil.addSection(responseDocument, msgContentXML.getChildNodes());
          }
          else
          {
            //load the message content                        
            msgContentXML = msgAccess.loadMessageContent(messageType, messageId);
            DOMUtil.addSection(responseDocument, msgContentXML.getChildNodes());
          }
                  
          // Append the XML returned from retrieveLock() to the primary document
          DOMUtil.addSection(responseDocument, xmlLock.getChildNodes());  
        }
        else
        // Submit (save) message sub-action
        //get all the email or letter parameters in the request object and build StockMessageVO object
        if(actionType.equals(COMConstants.SAVE_MESSAGE))
        {
          // check the lock one more time before saving message
          xmlLock = lock.retrieveLockXML(sessionId, userId, lockId, appName);
          DOMUtil.addSection(responseDocument, xmlLock.getChildNodes());          
          
          // check if lock obtained is N. Then, forward without saving comment.
          if(DOMUtil.getNodeValue(xmlLock, OUT_LOCK_OBTAINED).equalsIgnoreCase(NO))
          {
            // you come here only when current user times out after pushing Refresh button
            // save the lock object in request and append to primary document in LoadMessageMaintAction
            logger.debug("CSR lock is not available for saving Email/Letter.");                        
            request.setAttribute("lockObject", xmlLock);
            
            // Forward to LoadMessageMaintAction
            return mapping.findForward(FAILURE);
          }
          
          // set up StockMessage object
          msgObj.setMessageType(messageType);          
          // get message ID
           msgObj.setMessageId(messageId);
          // get message title
           msgObj.setTitle(messageTitle);
          // get email subject
           msgObj.setSubject(request.getParameter(COMConstants.MESSAGE_SUBJECT));
          // get message content
          if(request.getParameter(COMConstants.MESSAGE_CONTENT)!=null)
          {            
            msgObj.setContent(request.getParameter(COMConstants.MESSAGE_CONTENT));
          }
          // get origin id
          // The field originId contains the value of the drop down called Company on the screen
          // This field can have the value of FTD for all, an origin or a partner name
          // null the value out for FTD, then check origin, then assume partner_name
          if ("FTD".equals(originId))
          {
              originId = null;
              msgObj.setPartnerName(null);
              msgObj.setOriginId(null);
          }
          else if(originId!=null)
          {
            String originList = ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE,
                                                    COMConstants.EMAIL_ORIGINS);
            String partnerOriginList = ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE,
                    COMConstants.PARTNER_ORIGINS);
            
            
            if(originList.indexOf(originId)>=0 || partnerOriginList.indexOf(originId)>=0 || new MercentOrderPrefixes(conn).isMercentOrder(originId))
            {
              msgObj.setOriginId(originId);
              msgObj.setPartnerName(null);
            }
            else
            {
              // If it isn't an origin we are going to assume it is a partner
              msgObj.setOriginId(null);
              msgObj.setPartnerName(originId);
            }
          }
          
          // get created_by
          if(request.getParameter(COMConstants.CREATED_BY)!=null)
          {
            msgObj.setCreatedBy(request.getParameter(COMConstants.CREATED_BY));
          }
          else
          {
            msgObj.setCreatedBy(userId);       
          }
          // get updated_by
          if(request.getParameter(COMConstants.UPDATED_BY)!=null)
          {
            msgObj.setUpdatedBy(request.getParameter(COMConstants.UPDATED_BY));
          }
          else
          {
            msgObj.setUpdatedBy(userId);            
          }
          msgObj.setAutoResponseIndicator(autoEmail.equals("true") ? YES : NO);
          
          HashMap errMsg = new HashMap();
          // Call the insert or update method on StockMessageDAO passing the value object to be saved.
          if(newMessageFlag.equals(YES)) // new message
          {
            // validate the message title
            if(msgAccess.stockMessageExists(messageType, msgObj.getTitle()))
            {
              //load the message content                        
              msgContentXML = DOMUtil.getDocument("<StockMessageContent><Content><subject>"+msgObj.getSubject()
                +"</subject><body>"+msgObj.getContent()+"</body><auto_response_indicator>"+msgObj.getAutoResponseIndicator()
                +"</auto_response_indicator><origin_id>"+msgObj.getOriginId()+"</origin_id><partner_name>"+msgObj.getPartnerName()
                +"</partner_name></Content></StockMessageContent>");
            
              //request.setAttribute(VALIDATION_SUCCESS, NO);
              errMsg.put(VALIDATION_SUCCESS, NO);
            }
            else
            {
              errMsg.put(VALIDATION_SUCCESS, YES);
              msgAccess.insertStockMessage(msgObj);
              
              msgHeaderXML = null;
              msgContentXML = null;
            }
          }
          else // existing message
          {
            errMsg.put(VALIDATION_SUCCESS, YES);
            msgAccess.updateStockMessage(msgObj, originUpdateFlag);
            
            msgHeaderXML = null;
            msgContentXML = null;
          }
          // release the lock before forwarding to load message maintenance page
          lock.releaseLock(sessionId, userId, lockId, appName);
          DOMUtil.addSection(responseDocument, "errorMessage", "error", errMsg, true);
        }
        
        // Repopulate XML for redisplay
        if(msgHeaderXML != null)
        {
            DOMUtil.addSection(responseDocument, msgHeaderXML.getChildNodes());
        }
        
        if(msgContentXML != null)
        {
            DOMUtil.addSection(responseDocument, msgContentXML.getChildNodes());
        }
        
        // Call load DAO method to load all email titles or letter titles
        Document messageXML = msgAccess.loadMessageTitlesXML(messageType, null);
        DOMUtil.addSection(responseDocument, messageXML.getChildNodes());
            
        DOMUtil.addSection(responseDocument, "pageData", "data", secondaryMap, true);
        
        getPreferredPartnerList(responseDocument, conn);
        
        //Get XSL File name
        String xslName = "MessageMaintenance";
        File xslFile = getXSL(xslName, mapping);  
        ActionForward xslForward = mapping.findForward(xslName);
        String xslFilePathAndName = xslForward.getPath(); //get real file name
        
        // Change to client side transform
        TraxUtil.getInstance().getInstance().transform(request, response, responseDocument, 
                            xslFile, xslFilePathAndName, (HashMap)request.getAttribute(COMConstants.CONS_APP_PARAMETERS));
        return forward;
    }
    catch (Throwable ex)
    {
      logger.error(ex);
      forward =  mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
      return forward;
    }
    finally 
    {
      try
      {
        if(conn!=null)
          conn.close();
      }
      catch (SQLException se)
      {
        logger.error(se);
        return mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
      }
    }  
  }

  /******************************************************************************
  *                                     getXSL()
  *******************************************************************************
  * Retrieve name of Customer Order Search Action XSL file
  * @param1 String - the xsl name returned from the Business Object
  * @param2 ActionMapping
  * @return File - XSL File name
  * @throws none
  */

  private File getXSL(String xslName, ActionMapping mapping)
  {

    File xslFile = null;
    String xslFilePathAndName = null;

    ActionForward forward = mapping.findForward(xslName);
    xslFilePathAndName = forward.getPath();                //get real file name
    xslFile = new File(this.getServlet().getServletContext().getRealPath(xslFilePathAndName));
    return xslFile;
  }

    /**
     * @param xmlDoc
     * @param connection
     */
    @SuppressWarnings("unchecked")
	private void getPreferredPartnerList(Document xmlDoc, Connection connection) {
        try {
            HashMap<String, String> ppMap = PreferredPartnerUtility.getPreferredPartnerDisplayNames();
            // #17864 - Add the partners from partner mapping if the flag to 'CREATE_STOCK_EMAIL' is ON.
            PartnerUtility util = new PartnerUtility();
            List<PartnerMappingVO> partnerMappings = util.getAllPartnersMappingInfo();
            
            //#17900 - To add the mercent Partners			
			MercentOrderPrefixes mercentPrefixes = new MercentOrderPrefixes(connection);
        	List<MercentChannelMappingVO> mercentPartnerList = mercentPrefixes.getMercentChannelDetails();
        	
			if (ppMap == null && ((partnerMappings != null && partnerMappings.size() > 0) || (mercentPartnerList != null && mercentPartnerList.size() > 0))) {
				ppMap = new HashMap<String, String>();
			}
			
			if(partnerMappings!= null && partnerMappings.size() > 0) {
				for (PartnerMappingVO partnerMappingVO : partnerMappings) {
					if ("Y".equals(partnerMappingVO.getCreateStockEmail())) {
						ppMap.put(partnerMappingVO.getFtdOriginMapping(), partnerMappingVO.getPartnerName());
					}
				}
			}
			
			if(mercentPartnerList!= null && mercentPartnerList.size() > 0) {
				for (MercentChannelMappingVO mercentChannelMappingVO : mercentPartnerList) {
					ppMap.put(mercentChannelMappingVO.getFtdOriginMapping(), mercentChannelMappingVO.getChannelName());
				}
			}				
			
            DOMUtil.addSection(xmlDoc, "PreferredPartnerList", "PreferredPartner", ppMap, true);
        } catch (Exception e) {
        	logger.error("Error caught getting preferred partner list " + e.getMessage());
        }
    }
}