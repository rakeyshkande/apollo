package com.ftd.customerordermanagement.actions;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.util.DatabaseMaintainUtil;
import com.ftd.customerordermanagement.util.TimerFilter;
import com.ftd.messaging.dao.QueueDAO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;

import com.ftd.security.SecurityManager;

import java.io.File;
import java.io.IOException;

import java.sql.Connection;

import java.sql.SQLException;
import java.util.HashMap;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.w3c.dom.Document;

/**
 * RemoveEmailQueueAction class
 * 
 */
public final class RemoveEmailQueueAction extends Action 
{
    private static Logger logger  = new Logger("com.ftd.customerordermanagement.actions.RemoveEmailQueueAction");
    private static String SUCCESS = "success";
  
    /**
    * This is the main action called from the Struts framework.
    * @param mapping The ActionMapping used to select this instance.
    * @param form The optional ActionForm bean for this request.
    * @param request The HTTP Request we are processing.
    * @param response The HTTP Response we are processing.
    */
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException
    {
        Connection conn = null;
    
        try
        {
            // Pull DB connection
            conn = DataSourceUtil.getInstance().getConnection(ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, COMConstants.DATASOURCE_NAME));
            
            // Load request parameters
            String messageId = request.getParameter("message_id");
            String orderDetailId = request.getParameter("order_detail_id");
            String status = request.getParameter("status");
            String taggedCsrId = request.getParameter("tagged_csr_id");
            String sessionId = (String) request.getParameter(COMConstants.SEC_TOKEN);
            
            // Pull user id
            String userId = null;
            if (SecurityManager.getInstance().getUserInfo(sessionId)!=null)
            {
                userId = SecurityManager.getInstance().getUserInfo(sessionId).getUserID();
            }
            
            // Only allow delete if status is removed or scrub or email is unattached to an order
            if((status != null 
                && (status.equalsIgnoreCase(ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, COMConstants.REMOVED_STATUS))
                || status.equalsIgnoreCase(ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, COMConstants.PENDING_STATUS))
                || status.equalsIgnoreCase(ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, COMConstants.SCRUB_STATUS))))
                || (orderDetailId == null || orderDetailId.equals("")))
            {
                QueueDAO queueDAO = new QueueDAO(conn);
                boolean ableToDelete = queueDAO.deleteQueue(messageId, userId, taggedCsrId);
                
                if(!ableToDelete)
                {
                    // Set error to request
                    request.setAttribute("queue_delete_security_error", "Y");
                }
                else
                {
                    request.setAttribute("remove_email_queue_flag", "N");
                }
            }
            else
            {
                // Set error to request
                request.setAttribute("queue_delete_general_error", "Y");
            }
        }
        catch (Throwable ex)
        {
            try
            {                
                if(conn!=null)
                    conn.close();
            }
            catch (SQLException se)
            {
                logger.error(se);
                return mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
            }
        
            logger.error(ex);
            return mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
        }
        finally 
        {
            try
            {
                if(conn!=null)
                  conn.close();
            }
            catch (SQLException se)
            {
                logger.error(se);
                return mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
            }
        }
        
        // Forward to LoadMessageAction
        return mapping.findForward(SUCCESS);
    }
  
}