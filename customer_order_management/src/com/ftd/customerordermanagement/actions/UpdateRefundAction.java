package com.ftd.customerordermanagement.actions;

import com.ftd.customerordermanagement.bo.RefundBO;
import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.dao.LockDAO;
import com.ftd.customerordermanagement.util.BasePageBuilder;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityManager;
import com.ftd.decisionresult.filter.DecisionResultDataFilter;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;




import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;

/**
 * UpdateRefundAction class
 * 
 * @author Brian Munter
 */

public final class UpdateRefundAction extends Action 
{
    private static Logger logger  = new Logger("com.ftd.customerordermanagement.actions.UpdateRefundAction");
    private static String SUCCESS = "success";
    private static String ORDER = "order";
    private static String FAILURE = "failure";
    private static String OUT_LOCK_OBTAINED = "OUT_LOCK_OBTAINED";
    private static String OUT_LOCKED_IND = "OUT_LOCKED_IND";
    private static String YES = "Y";
    private static String NO = "N";
    
    
    private static final String ERROR_PAGE = "error_page";
    private static final String PROPERTY_FILE = "security-config.xml";

    private static String rootURL = null;
    
  /**
   * This is the main action called from the Struts framework.
   * 
   * @param mapping The ActionMapping used to select this instance.
   * @param form The optional ActionForm bean for this request.
   * @param request The HTTP Request we are processing.
   * @param response The HTTP Response we are processing.
   */
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
        Connection conn = null;
        ActionForward forward = null;
    
        try
        {
            String action = request.getParameter("action_type");
            String paymentId = request.getParameter("payment_id");
            logger.info("******** paymentId:" + paymentId);
            conn = DataSourceUtil.getInstance().getConnection(ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, COMConstants.DATASOURCE_NAME));
            RefundBO refundBO = new RefundBO(request, conn);
            
            // Full cart refund
            if( action.equalsIgnoreCase("refund_cart")      &&
                request.getParameter("order_guid") != null  && 
                !request.getParameter("order_guid").equals(""))
            {
                logger.info("refund_cart");
            	// Post refund to each item
                refundBO.postFullRefund();
                
                // Release lock
                this.releaseLock(request.getParameter("master_order_number"), request.getParameter(COMConstants.SEC_TOKEN), conn);

                // Forward back to cart
                checkIfCallDispoRequired(request);                
                return mapping.findForward(ORDER);
            }
            // Standard refund
            else if(action != null)
            {
                // Refund review selection
                if(action.equals("reviewed"))
                {
                    logger.info("reviewed");
                	// 
                    HashMap timerResults = refundBO.updateReviewer();
                      
                    ActionForward actionForward = mapping.findForward(SUCCESS);
                    String path = actionForward.getPath() 
                                    + "?" + COMConstants.ORDER_DETAIL_ID + "=" + request.getParameter(COMConstants.ORDER_DETAIL_ID)
                                    + "&" + COMConstants.PREV_PAGE + "=" + COMConstants.REFUND_REVIEW_PAGE
                                    + "&" + COMConstants.PREV_PAGE_POSITION + "=" + request.getParameter(COMConstants.PAGE_POSITION)
                                    + "&" + COMConstants.TIMER_ENTITY_HISTORY_ID + "=" + timerResults.get(COMConstants.TIMER_ENTITY_HISTORY_ID).toString()
                                    + "&" + COMConstants.TIMER_COMMENT_ORIGIN_TYPE + "=" + timerResults.get(COMConstants.TIMER_COMMENT_ORIGIN_TYPE).toString()
                                    + "&" + COMConstants.START_ORIGIN + "=" + request.getParameter(COMConstants.START_ORIGIN)
                                    + "&" + COMConstants.SEC_TOKEN + "=" + request.getParameter(COMConstants.SEC_TOKEN)
                                    + "&" + COMConstants.CONTEXT + "=" +request.getParameter(COMConstants.CONTEXT)
                                    //Refund Review Sort Filters Values Retain
                                    + "&" + COMConstants.PREV_SORT_COLUMN_NAME + "=" +request.getParameter(COMConstants.SORT_COLUMN_NAME)
                                    + "&" + COMConstants.PREV_SORT_COLUMN_STATUS + "=" +request.getParameter(COMConstants.SORT_COLUMN_STATUS)
                                    + "&" + COMConstants.PREV_CURRENT_SORT_COLUMN_STATUS + "=" +request.getParameter(COMConstants.CURRENT_SORT_COLUMN_STATUS)
                                    + "&" + COMConstants.PREV_IS_FROM_HEADER + "=" +request.getParameter(COMConstants.IS_FROM_HEADER)
                                    + "&" + COMConstants.PREV_USER_ID_FILTER + "=" +request.getParameter(COMConstants.USER_ID_FILTER)
                                    + "&" + COMConstants.PREV_GROUP_ID_FILTER + "=" +request.getParameter(COMConstants.GROUP_ID_FILTER)
                                    + "&" + COMConstants.PREV_CODES_FILTER + "=" +request.getParameter(COMConstants.CODES_FILTER);
                   
                    // Redirect to path
                    path = BasePageBuilder.getRootURL() + path; 
                    return new ActionForward(path,true);
                }
                else if(action.equals("reinstate_gc"))
                {
                    logger.info("action.equals(reinstate_gc)");
                	refundBO.reinstateGiftCertificate();
                }
                else if(action.equals("validate_reinstate"))
                {
                    logger.info("validate_reinstate");
                	// Get default document
                    Document responseDocument = (Document) DOMUtil.getDocument();
                
                    HashMap pageData = new HashMap();
                    
                    // Check if refund can be reinstated
                    if(refundBO.validateReinstate())
                    {
                        pageData.put("reinstate_flag", "Y");
                    }
                    else
                    {
                        pageData.put("reinstate_flag", "N");
                    }
                    
                    DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true);
                    
                    // Generate page
                    String xslName = "CheckReinstateStatusIFrame"; 
                      
                    File xslFile = getXSL(xslName, mapping); 
                    forward = mapping.findForward(xslName);
                    String xslFilePathAndName = forward.getPath(); //get real file name
                    
                    // Change to client side transform
                    TraxUtil.getInstance().getInstance().transform(request, response, responseDocument, 
                                        xslFile, xslFilePathAndName, (HashMap)request.getAttribute(COMConstants.CONS_APP_PARAMETERS));
                
                    return null;
                }
                // Only perform if current user has the lock
                else if(!this.retrieveLock(request.getParameter("order_detail_id"), request.getParameter(COMConstants.SEC_TOKEN), conn))
                {
                    // Create a refund
                    if(action.equals("post"))
                    {
                        refundBO.postRefund();
                        checkIfCallDispoRequired(request);                
                    }
                    // Create a C30 refund - this is a payment method swap from CC to GC.  Therefore
                    // we will refund the CC and create a payment. 
                    else if (action.equals("postC30"))
                    {
                        refundBO.postC30Refund();
                    }
                    // Update a refund
                    else if(action.equals("edit"))
                    {
                        refundBO.editRefund();
                    }
                    // Delete a refund
                    else if(action.equals("delete"))
                    {
                        refundBO.deleteRefund();
                    }
                }
            }
            
            // Set forward to the load refund action
            forward = mapping.findForward(SUCCESS);
            return forward;     
        }
        catch (Throwable ex)
        {
            logger.error(ex);
            forward = mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
            return forward;
        }
        finally 
        {
            try
            {
                conn.close();
            }
            catch (SQLException se)
            {
                logger.error(se);
            }
        }  
    }
    
    private boolean retrieveLock(String lockId, String securityToken, Connection conn) throws Exception
    {
         boolean locked = false;
            
        String securityFlag = ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, COMConstants.SECURITY_IS_ON);
        if(securityFlag != null && securityFlag.equals("true"))
        {        
            if (SecurityManager.getInstance().getUserInfo(securityToken) != null)
            {
                LockDAO lockObj = new LockDAO(conn);
                Document xmlLock = lockObj.retrieveLockXML(securityToken, SecurityManager.getInstance().getUserInfo(securityToken).getUserID(), lockId, COMConstants.CONS_ENTITY_TYPE_PAYMENTS, COMConstants.CONS_ENTITY_TYPE_ORDER_DETAILS);
                if(DOMUtil.getNodeValue(xmlLock, OUT_LOCK_OBTAINED).equalsIgnoreCase(NO))
                {
                    locked = true;
                }
            }
        }
        
        return locked;
    }
    
    private void releaseLock(String lockId, String securityToken, Connection conn) throws Exception
    {
        String securityFlag = ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, COMConstants.SECURITY_IS_ON);
        if(securityFlag != null && securityFlag.equals("true"))
        {        
            if (SecurityManager.getInstance().getUserInfo(securityToken) != null)
            {
                LockDAO lockObj = new LockDAO(conn);
                lockObj.releaseLock(securityToken, SecurityManager.getInstance().getUserInfo(securityToken).getUserID(), lockId, COMConstants.CONS_ENTITY_TYPE_PAYMENTS);
            }
        }
    }
    
    private File getXSL(String xslName, ActionMapping mapping)
    {
        File xslFile = null;
        String xslFilePathAndName = null;
        
        ActionForward forward = mapping.findForward(xslName);
        xslFilePathAndName = forward.getPath(); 
        xslFile = new File(this.getServlet().getServletContext().getRealPath(xslFilePathAndName));
        return xslFile;
    }


    /**
     * Determines if a Call Disposition is required based on the refund type
     * and sets Entity List flag accordingly.
     * 
     * @param request
     */
    private void checkIfCallDispoRequired(HttpServletRequest request) throws Exception {
        String dispCode = null;
       
        // If a B-type refund, then set flag indicating a Call Disposition is required
        //
        if(StringUtils.isNotEmpty(request.getParameter("disp_code"))) {
            dispCode = StringUtils.trim(request.getParameter("disp_code"));
        }
        if (dispCode != null) {
            // ??? These shouldn't be hardcoded
            if (dispCode.equalsIgnoreCase("B10") || dispCode.equalsIgnoreCase("B20") ||
                dispCode.equalsIgnoreCase("B22") || dispCode.equalsIgnoreCase("B25") ||
                dispCode.equalsIgnoreCase("B30") || dispCode.equalsIgnoreCase("B50") ||
                dispCode.equalsIgnoreCase("B60")) 
            {
                String orderDetailId = StringUtils.trim(request.getParameter("order_detail_id"));
                DecisionResultDataFilter.updateEntity(request, orderDetailId, true);
            }
        }
    }

}