package com.ftd.customerordermanagement.actions;


import com.ftd.customerordermanagement.bo.GcCouponBO;
import com.ftd.customerordermanagement.bo.GiftCardBO;
import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.vo.PaymentVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.NumberFormat;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;



import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import org.xml.sax.SAXException;


public final class GcCouponAction extends Action
{
    private static Logger logger  = new Logger("com.ftd.customerordermanagement.actions.GcCouponAction");
    private static final String MESSAGE_ROOT_NODE = "root";

    /**
     * This is the main action called from the Struts framework.
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     */
    public  ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
            HttpServletResponse response)

    {
        ActionForward forward = null;
        Connection conn = null;
        String xslName = "gcCoupon";
        //Obtain a database connection
        //Obtain the action
        //Obtain total amount owed
        //Create a root document containing the total amount owed.
        Document gccStatusDoc = null;
        try
        {
            //Connection/Database info
            conn = getDBConnection();

            String action = request.getParameter("page_action");
            //Validate gift certificate status and obtain expiration date and amount, or validate gift card
            if (action.equalsIgnoreCase("redeem"))
            {
                String gdorgc = request.getParameter(COMConstants.GD_OR_GC);
                if (gdorgc != null && gdorgc.equals(COMConstants.GIFT_CARD_PAYMENT))
                {
                    validateGiftCard(mapping, request, response, conn, xslName);                    
                }
                else
                {
                    validateGiftCertificate(mapping, request, response, conn, xslName);
                }
                return null;
            }
            else
            {
                Document returnDocument = DOMUtil.getDefaultDocument();
                Element root = (Element) returnDocument.createElement(MESSAGE_ROOT_NODE);
                returnDocument.appendChild(root);

                Element node;
                node = (Element)returnDocument.createElement("total_owed");
                node.appendChild(returnDocument.createTextNode(request.getParameter("total_owed")));
                root.appendChild(node);

                if(logger.isDebugEnabled())
                {
                    //IMPORTANT:  Because StringWriter contents are NOT removed even when the flush
                    //            method is executed, make StringWriter go away through scope control.
                    //            Both the flush and close methods were tested with but were of none affect.
                    StringWriter sw = new StringWriter();       //string representation of xml document
                    DOMUtil.print((Document) returnDocument,new PrintWriter(sw));
                    logger.debug("- execute(): COM returned = \n" + sw.toString());
                }

                //Get XSL File name
                File xslFile = getXSL(xslName, mapping);

                //Transform the XSL File
                TraxUtil.getInstance().getInstance().transform(request, response, returnDocument,
                        xslFile, (HashMap)request.getAttribute(COMConstants.CONS_APP_PARAMETERS));
                return null;
            }
        }
        catch(Exception e)
        {
            logger.error(e);
            forward = mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
            return forward;
        }
        finally
        {
            try
            {
                //Close the connection
                if(conn != null)
                {
                    conn.close();
                }
            }
            catch(Exception e)
            {
                logger.error(e);
                forward = mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
                return forward;
            }
        }
    }


    /**
     * validate a gift card, i.e. compare gc value to requested amount
     * @param mapping
     * @param request
     * @param response
     * @param conn
     * @param xslName
     * @throws Exception
     */
    private void validateGiftCard(ActionMapping mapping, HttpServletRequest request, HttpServletResponse response,
            Connection conn, String xslName) throws Exception
    {
        String totalOwed = request.getParameter("total_owed");
        String gdNum = request.getParameter("gc_coupon_number");
        String gdPin = request.getParameter("gd_pin");
        String methodToInvoke = request.getParameter("method_to_invoke");
        double authAmt = Double.valueOf(totalOwed);
        
        GcCouponBO gcBO = new GcCouponBO(conn); // has common methods for generating expected return documents
        GiftCardBO gdBO = new GiftCardBO(conn); // gets current gd balance

        Document gdStatusDoc = DOMUtil.getDefaultDocument();        
        Document returnDocument;       
        
        // get the current balance on the gift card. Then we will compare it to the requested amount and process errors/success
        Double currentBalance = gdBO.getCurrentBalance(gdStatusDoc, gdNum, gdPin, totalOwed);
        
        if (currentBalance.compareTo(0.00) == 0)
        {
            // special error case
            returnDocument = gcBO.createInvalidGccXML(COMConstants.GCC_STATUS_REDEEMED, totalOwed, methodToInvoke);
        }
        else if (currentBalance.compareTo(authAmt) < 0) // which includes balance = -1, the error case
        {
            // error case
            returnDocument = gcBO.createInvalidGccXML(null, totalOwed, methodToInvoke);
        }
        else
        {
            // success case
            returnDocument = gcBO.calculateValidGCXML(totalOwed, gdNum, methodToInvoke, currentBalance.toString());
        }

        // translate return document
        processValidationResults(mapping, request, response, xslName, returnDocument);
    }


    /**
     * validate a gift certificate, i.e. compare gc value to requested amount
     * @param mapping
     * @param request
     * @param response
     * @param conn
     * @param xslName
     * @throws IOException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws SQLException
     * @throws TransformerException
     * @throws Exception
     */
    private void validateGiftCertificate(ActionMapping mapping, HttpServletRequest request,
            HttpServletResponse response, Connection conn, String xslName) throws IOException,
            ParserConfigurationException, SAXException, SQLException, TransformerException, Exception
    {
        Document gccStatusDoc;
        GcCouponBO gccBO = new GcCouponBO(conn);

        gccStatusDoc = gccBO.validateGiftCertificateCoupon(
                request.getParameter("gc_coupon_number"),
                request.getParameter("total_owed"),
                request.getParameter("method_to_invoke")
        );

        processValidationResults(mapping, request, response, xslName, gccStatusDoc);
    }

    private void processValidationResults(ActionMapping mapping, HttpServletRequest request,
            HttpServletResponse response, String xslName, Document gdStatusDoc) throws Exception
    {
        ActionForward forward;
        if(logger.isDebugEnabled())
        {
            //IMPORTANT:  Because StringWriter contents are NOT removed even when the flush
            //            method is executed, make StringWriter go away through scope control.
            //            Both the flush and close methods were tested with but were of none affect.
            StringWriter sw = new StringWriter();       //string representation of xml document
            DOMUtil.print((Document) gdStatusDoc,new PrintWriter(sw));
            logger.debug("- execute(): COM returned = \n" + sw.toString());
        }
        //Get XSL File name
        File xslFile = getXSL(xslName, mapping);

        forward = mapping.findForward(xslName);
        String xslFilePathAndName = forward.getPath(); //get real file name

        // Change to client side transform
        TraxUtil.getInstance().getInstance().transform(request, response, gdStatusDoc,
                xslFile, xslFilePathAndName, (HashMap)request.getAttribute(COMConstants.CONS_APP_PARAMETERS));
    }


    /**
     * Obtain connectivity with the database
     * @param none
     * @return Connection - db connection
     * @throws Exception
     * @throws IOException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws TransformerException
     * @throws XSLException
     */

    private Connection getDBConnection()
    throws IOException, ParserConfigurationException, SAXException, TransformerException,
    Exception
    {
        Connection conn = null;
        conn = DataSourceUtil.getInstance().getConnection(
                ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE,
                        COMConstants.DATASOURCE_NAME));
        return conn;
    }

    /**
     * Retrieve name of Update Billing Action XSL file
     * @param1 String - the xsl name
     * @param2 ActionMapping
     * @return File - XSL File name
     * @throws none
     */

    private File getXSL(String xslName, ActionMapping mapping)
    {
        File xslFile = null;
        String xslFilePathAndName = null;

        ActionForward forward = mapping.findForward(xslName);
        xslFilePathAndName = forward.getPath();
        //get real file name
        xslFile = new File(this.getServlet().getServletContext().getRealPath(xslFilePathAndName));
        return xslFile;
    }
}