package com.ftd.customerordermanagement.actions;

import java.io.File;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.dao.StockMessageDAO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.PartnerUtility;
import com.ftd.osp.utilities.PreferredPartnerUtility;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.order.MercentOrderPrefixes;
import com.ftd.osp.utilities.order.vo.MercentChannelMappingVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.PartnerMappingVO;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityManager;

/**
 * LoadMessageMaintAction class
 * 
 */

public final class LoadMessageMaintAction extends Action 
{
    private static Logger logger  = new Logger("com.ftd.customerordermanagement.actions.LoadMessageMaintAction");
    private static String OUT_CUR = "OUT_CUR";
    private static String OUT_ID_COUNT = "OUT_ID_COUNT";
    private static String OUT_PARAMETERS = "out-parameters";
    private static String YES = "Y";
    private static String NO = "N";
    private static String ON = "on";
  
  /**
   * This is the main action called from the Struts framework.
   * @param mapping The ActionMapping used to select this instance.
   * @param form The optional ActionForm bean for this request.
   * @param request The HTTP Request we are processing.
   * @param response The HTTP Response we are processing.
   */
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException
  {
    HashMap secondaryMap = new HashMap();
    Connection conn = null;
    String messageType = null;
    String newMessageFlag = null;
    String spellCheckFlag = null;
    String actionType = null;
    
    try
    {
        // pull base document
        Document responseDocument = (Document) DOMUtil.getDocument();
        
        //get all the parameters in the request object
        // get action type
        if(request.getParameter(COMConstants.ACTION_TYPE)!=null)
        {
          actionType = request.getParameter(COMConstants.ACTION_TYPE);
          secondaryMap.put(COMConstants.ACTION_TYPE, actionType);
        }
        
        // message type
        if(request.getParameter(COMConstants.MESSAGE_TYPE)!=null)
        {
          messageType = request.getParameter(COMConstants.MESSAGE_TYPE);
        }
        secondaryMap.put(COMConstants.MESSAGE_TYPE, messageType);
        // new message flag
        if(request.getParameter(COMConstants.NEW_MESSAGE_FLAG)!=null)
        {
          newMessageFlag = request.getParameter(COMConstants.NEW_MESSAGE_FLAG);
        }
        secondaryMap.put(COMConstants.NEW_MESSAGE_FLAG, newMessageFlag);
        // spell check flag
        if(request.getParameter(COMConstants.SPELL_CHECK_FLAG)!=null)
        {
          spellCheckFlag = request.getParameter(COMConstants.SPELL_CHECK_FLAG);
        }
        secondaryMap.put(COMConstants.SPELL_CHECK_FLAG, spellCheckFlag);
        
        String context = (String) request.getParameter(COMConstants.CONTEXT);
        String securityToken = (String) request.getParameter(COMConstants.SEC_TOKEN);
        SecurityManager securityManager = SecurityManager.getInstance();
        if (securityManager.assertPermission(context, securityToken, ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, COMConstants.MESSAGE_MAINTENANCE), COMConstants.ADD))
        {
            secondaryMap.put("add_new_flag", "Y"); 
        }
        else
        {
            secondaryMap.put("add_new_flag", "N"); 
        }
                        
        // get database connection
        conn = DataSourceUtil.getInstance().getConnection(ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE,
                                                            COMConstants.DATASOURCE_NAME));
                      
        // Call load DAO method to load email titles or letter titles
        if(actionType.equals(COMConstants.LOAD_MSG_TITLES))
        {
          StockMessageDAO stockMsg = new StockMessageDAO(conn);
          Document messageXML = null;
          messageXML = stockMsg.loadMessageTitlesXML(messageType, null);
          DOMUtil.addSection(responseDocument, messageXML.getChildNodes());        
          
          // Generate XML from secondary HashMap using the DOMUtil HashMap to XML conversion method addSection() .
           DOMUtil.addSection(responseDocument, "pageData", "data", secondaryMap, true);
        }
        
        getPreferredPartnerList(responseDocument, conn);
        
        // Forward to transformation action
        String xslName = "MessageMaintenance";
        File xslFile = getXSL(xslName, mapping);  
        ActionForward forward = mapping.findForward(xslName);
        String xslFilePathAndName = forward.getPath(); //get real file name
        
        // Change to client side transform
        TraxUtil.getInstance().getInstance().transform(request, response, responseDocument, 
                            xslFile, xslFilePathAndName, (HashMap)request.getAttribute(COMConstants.CONS_APP_PARAMETERS));
        return null;
    }
    catch (Throwable ex)
    {
      logger.error(ex);
      return mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
    }
    finally 
    {
      try
      {
        if(conn!=null)
          conn.close();
      }
      catch (SQLException se)
      {
        logger.error(se);
        return mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
      }
    }  
  }

  /******************************************************************************
  *                                     getXSL()
  *******************************************************************************
  * Retrieve name of Customer Order Search Action XSL file
  * @param1 String - the xsl name returned from the Business Object
  * @param2 ActionMapping
  * @return File - XSL File name
  * @throws none
  */

  private File getXSL(String xslName, ActionMapping mapping)
  {

    File xslFile = null;
    String xslFilePathAndName = null;

    ActionForward forward = mapping.findForward(xslName);
    xslFilePathAndName = forward.getPath();                //get real file name
    xslFile = new File(this.getServlet().getServletContext().getRealPath(xslFilePathAndName));
    return xslFile;
  }
  
	/**
	 * @param xmlDoc
	 * @param connection
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void getPreferredPartnerList(Document xmlDoc, Connection connection) {
		try {			
			HashMap ppMap = PreferredPartnerUtility.getPreferredPartnerDisplayNames();
			// #17864 - Add the partners from partner mapping if the flag to
			// 'CREATE_STOCK_EMAIL' is ON.
			PartnerUtility util = new PartnerUtility();
			List<PartnerMappingVO> partnerMappings = util.getAllPartnersMappingInfo();
			
			//#17900 - To add the mercent Partners			
			MercentOrderPrefixes mercentPrefixes = new MercentOrderPrefixes(connection);
        	List<MercentChannelMappingVO> mercentPartnerList = mercentPrefixes.getMercentChannelDetails();
        	
			if (ppMap == null && ((partnerMappings != null && partnerMappings.size() > 0) || (mercentPartnerList != null && mercentPartnerList.size() > 0))) {
				ppMap = new HashMap<String, String>();
			}
			
			if(partnerMappings!= null && partnerMappings.size() > 0) {
				for (PartnerMappingVO partnerMappingVO : partnerMappings) {
					if ("Y".equals(partnerMappingVO.getCreateStockEmail()) && partnerMappingVO.isDefaultBehaviour()) {
						ppMap.put(partnerMappingVO.getFtdOriginMapping(), partnerMappingVO.getPartnerName());
					}
				}
			}
			
			if(mercentPartnerList!= null && mercentPartnerList.size() > 0) {
				for (MercentChannelMappingVO mercentChannelMappingVO : mercentPartnerList) {
					ppMap.put(mercentChannelMappingVO.getFtdOriginMapping(), mercentChannelMappingVO.getChannelName());
				}
			}
			
			DOMUtil.addSection(xmlDoc, "PreferredPartnerList", "PreferredPartner", ppMap, true);
			
		} catch (Exception e) {
			logger.error("Error caught getting preferred partner list " + e.getMessage());
		}
	}	
}



