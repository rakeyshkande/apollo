package com.ftd.customerordermanagement.actions;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.ftd.customerordermanagement.bo.AddBillingBO;
import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.util.BasePageBuilder;
import com.ftd.customerordermanagement.vo.BillingVO;
import com.ftd.customerordermanagement.vo.CommonCreditCardVO;
import com.ftd.customerordermanagement.vo.CreditCardVO;
import com.ftd.customerordermanagement.vo.GcCouponVO;
import com.ftd.customerordermanagement.vo.PaymentVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.handlers.GlobalParmHandler;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.RecipientAddressesVO;
import com.ftd.osp.utilities.order.vo.RecipientsVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityManager;
import com.ftd.security.cache.vo.UserInfo;
/**
 * The AddBillingAction will obtain existing billing and payment information,
 * current billing information and the last credit card used by the customer.
 *
 * @author Luke W. Pennings
 */
 
public final class AddBillingAction extends Action 
{
    private static Logger logger  = new Logger("com.ftd.customerordermanagement.actions.AddBillingAction");
    private static final String DATA_ROOT_NODE = "root";
    private static final String CREDIT_CARD_PAYMENT_TYPE = "C";
    private static final String COUPON_PAYMENT_TYPE = "G";
    private static final String GCC_PAYMENT_TYPE = "GC";
    private static final String STATUS_ROOT_NODE = "status";


  /**
   * This is the main action called from the Struts framework.
   * @param mapping The ActionMapping used to select this instance.
   * @param form The optional ActionForm bean for this request.
   * @param request The HTTP Request we are processing.
   * @param response The HTTP Response we are processing.
   */
  public  ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, 
                                HttpServletResponse response) 
  {
    ActionForward forward = null;
    Connection conn = null;
    try
    {
      String action = request.getParameter(COMConstants.PAGE_ACTION);
      logger.debug("action = " + request.getParameter(COMConstants.PAGE_ACTION));
      logger.debug("exp_date_month = " + request.getParameter("exp_date_month"));
      logger.debug("exp_date_year = " + request.getParameter("exp_date_year"));
      logger.debug("total = " + request.getParameter("total"));

      //Connection/Database info
	    conn = getDBConnection();
      
      //For an apply_billing action,  load the VOs and call the addBillingBO
      //to apply billing
      if(action.equalsIgnoreCase("apply_billing"))
      {
        File xslFile = null;

				/*  Need an explicit catch for apply_billing.  apply_billing is called 
				 * 	using ajax so an error XML must be sent back instead of 
				 *  forwarding to the error page.
				*/
				try
				{
					// Apply the billing
					Document statusDoc = this.applyBilling(request, conn);
					
					// Send back apply billing status document as XML.  The front end utilizes AJAX
					DOMUtil.print(statusDoc,response.getWriter());
				}
				catch(Throwable t)
				{
					logger.error(t);
					
					// Send back error status document as XML.  The front end utilizes AJAX
					DOMUtil.print(createErrorXML(),response.getWriter());
				}
		
				return null;			
      }
      else if(action.equalsIgnoreCase("recalculate_order"))
      {
				/*  Need an explicit catch for recalculate_order.  recalculate_order is called 
				 * 	using ajax so an error XML must be sent back instead of 
				 *  forwarding to the error page.
				*/
				try
				{
					// Recalculate the order
					Document recalcOrdDoc = this.recalculateOrder(request, conn); 
	
					// Send back recalculate status document as XML.  The front end utilizes AJAX
					DOMUtil.print(recalcOrdDoc,response.getWriter());
				}				
				catch(Throwable t)
				{
					logger.error(t);
					
					// Send back error status document as XML.  The front end utilizes AJAX
					DOMUtil.print(createErrorXML(),response.getWriter());
				}
		
				return null;			
      }
      else
      {
				Document paymentInfoXML = this.getPaymentInfo(request, conn);
				//Get XSL File name
				String xslName = "addBilling";
				File xslFile = getXSL(xslName, mapping);	

				forward = mapping.findForward(xslName);
				String xslFilePathAndName = forward.getPath(); //get real file name
				//Transform the XSL File
				TraxUtil.getInstance().getInstance().transform(request, response, paymentInfoXML, 
												xslFile, xslFilePathAndName, (HashMap)request.getAttribute(COMConstants.CONS_APP_PARAMETERS));
				return null;
      }
    }
    catch(Exception e)
    {
      logger.error(e);
      forward = mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
      return forward;
    }
    finally
    {
      try
      {
        //Close the connection
        if(conn != null)
        {
          conn.close();
        }
      }
      catch(Exception e)
      {
        logger.error(e);
        forward = mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
        return forward;
      }
    }
  }


  /**
   * Obtain connectivity with the database
   * @param none
   * @return Document - Error document
 * @throws ParserConfigurationException 
   */

	private Document createErrorXML() throws ParserConfigurationException
	{
		Document errorDocument = DOMUtil.getDefaultDocument();
		Element root = (Element) errorDocument.createElement("root");
		errorDocument.appendChild(root);
	
		Element node;
		node = (Element)errorDocument.createElement("is_error");
		node.appendChild(errorDocument.createTextNode("Y"));
		root.appendChild(node);
	
		root.appendChild(node);	
		
		return errorDocument;
	}



  /**
   * Get Payment Information
   * @param request
   * @param connection
   * @return Document - payment information
   * @throws Exception
   * @throws IOException
   * @throws ParserConfigurationException
   * @throws SAXException
   * @throws TransformerException
   * @throws XSLException
   */
   
  private Document getPaymentInfo(HttpServletRequest request, Connection conn)
		throws IOException, ParserConfigurationException, SAXException, TransformerException, 
               NumberFormatException, Exception
  {
    String billingLevel  = request.getParameter("billing_level");
    String sourceCode    = request.getParameter("source_code");
    String orderGuid     = request.getParameter("order_guid");
    String orderDetailId = request.getParameter("order_detail_id");
    String secToken      = request.getParameter(COMConstants.SEC_TOKEN);
    String extOrdNum     = request.getParameter(COMConstants.EXTERNAL_ORDER_NUMBER);
    String mstOrdNum     = request.getParameter(COMConstants.MASTER_ORDER_NUMBER);
    
    AddBillingBO aBO = new AddBillingBO(conn);
    HashMap paymentInfo = aBO.getPaymentInfo(orderGuid, orderDetailId, billingLevel, sourceCode, 
                                            secToken, extOrdNum, mstOrdNum);
    this.populateSubHeaderInfo(request, paymentInfo);

    //Document that will contain the final XML, to be passed to the TransUtil and XSL page
    Document returnDocument = (Document) DOMUtil.getDocumentBuilder().newDocument();
    Element root = (Element) returnDocument.createElement(DATA_ROOT_NODE);
    returnDocument.appendChild(root);

    //Get all the keys in the hashmap 
    Set ks = paymentInfo.keySet();
    Iterator iter = ks.iterator();
    String key = null; 
        
    //Iterate thru the hashmap returned from the business object using the keyset
    while(iter.hasNext())
    {
       key = iter.next().toString();
       if (key.equalsIgnoreCase("pageData"))
       {
         //Convert the page data hashmap to XML and append it to the final XML
         DOMUtil.addSection(returnDocument, "pageData", "data", 
                              (HashMap)paymentInfo.get("pageData"), true); 
       }
       else
       {
          //Append all the existing XMLs to the final XML
          Document xmlDoc = (Document) paymentInfo.get(key);
          NodeList nl = xmlDoc.getChildNodes();
          DOMUtil.addSection(returnDocument, nl);
       }
    }

    if(logger.isDebugEnabled())
    {
      //IMPORTANT:  Because StringWriter contents are NOT removed even when the flush
      //            method is executed, make StringWriter go away through scope control.
      //            Both the flush and close methods were tested with but were of none affect.
      StringWriter sw = new StringWriter();       //string representation of xml document
      DOMUtil.print((Document) returnDocument,new PrintWriter(sw));
      logger.debug("- execute(): COM returned = \n" + sw.toString());
    }
    return returnDocument;
  }
  
 /**
  * populateSubHeaderInfo()
  * Populate the page data with the remainder of the fields
  *
  * @param request
  * @return HashMap
  * @throws Exception
  */
  private HashMap populateSubHeaderInfo(HttpServletRequest request, HashMap paymentInfo)
      throws Exception
  {
	BasePageBuilder bpb = new BasePageBuilder();
	//xml document for sub header
	Document subHeaderXML = DOMUtil.getDocument();
	//retrieve the sub header info
	subHeaderXML = bpb.retrieveSubHeaderData(request);
	//save the original product xml
	paymentInfo.put(COMConstants.HK_SUB_HEADER, subHeaderXML);
    
    return paymentInfo;
  }
  
  /**
   * Apply billing activity to the database
   * @param Request
   * @param Connection - db connection
   * @return Document - status of updates
   * @throws Exception
   * @throws IOException
   * @throws ParserConfigurationException
   * @throws SAXException
   * @throws TransformerException
   * @throws XSLException
   */

  private Document applyBilling(HttpServletRequest request, Connection conn)
		throws IOException, ParserConfigurationException, SAXException, TransformerException, 
               Exception
  {
    //Get the global params
    GlobalParmHandler gph = (GlobalParmHandler)CacheManager.getInstance().getHandler("CACHE_NAME_GLOBAL_PARM");
    if (gph != null)
    {
      //if the flag is no, allow the process to continue.
      //if the flag is yes, stop the process and return a message
      String authFlag = gph.getGlobalParm(COMConstants.GLOBAL_CONTEXT, COMConstants.GLOBAL_AUTH_FLAG);
      if (authFlag.equalsIgnoreCase(COMConstants.GLOBAL_AUTH_FLAG_ON))
      {
        Document statusDoc = this.createStatusDocument(COMConstants.AB_REASON_DECLINE, 
                    COMConstants.AB_REASON_DECLINE, COMConstants.AB_REASON_MSG,"N");
        return statusDoc;
      }
    }

    String sessionId = null;
    String csrId = null;
    ConfigurationUtil cu = null;
    BillingVO bVO = null;
    PaymentVO pVO = new PaymentVO();
    PaymentVO[] payVO = null;
    GcCouponVO gcVO = new GcCouponVO();
    CreditCardVO ccVO = null;
    CommonCreditCardVO cccVO = null;
    ArrayList payments = new ArrayList();
    AddBillingBO abBO = new AddBillingBO(conn);
    cu = ConfigurationUtil.getInstance();
    //get csr Id
    String security = (String) cu.getProperty(COMConstants.PROPERTY_FILE,COMConstants.SECURITY_IS_ON);
    boolean securityIsOn = security.equalsIgnoreCase("true") ? true : false;
    sessionId   = request.getParameter(COMConstants.SEC_TOKEN);
    
    if(securityIsOn || sessionId != null)
    {
      SecurityManager sm = SecurityManager.getInstance();
      UserInfo ui = sm.getUserInfo(sessionId);
      csrId = ui.getUserID();
    }

    String billingLevel = request.getParameter("billing_level");
    String mgrCode = request.getParameter("mgr_appr_code");
    String mgrPassword = request.getParameter("mgr_password");
    String context = request.getParameter(COMConstants.CONTEXT);
    //Populate a billingVO for non CART billing.
    if (!billingLevel.equalsIgnoreCase(COMConstants.BILLING_LEVEL_CART))
    {
       bVO = new BillingVO();
       if(request.getParameter("add_on") != null && request.getParameter("add_on").length() > 0)
       {
         bVO.setAddOnAmt(Double.valueOf(request.getParameter("add_on")).doubleValue());
       }
       else
       {
         bVO.setAddOnAmt(Double.valueOf("0").doubleValue());  
       }

	   /*
		* We will never have a discount for an add bill that is for an 
		* order.  msk 10/04/2005
		
	   if(request.getParameter("discount") != null && request.getParameter("discount").length() > 0)
       {
         bVO.setDiscountAmt(Double.valueOf(request.getParameter("discount")).doubleValue());
       }
       else
       {
         bVO.setDiscountAmt(Double.valueOf("0").doubleValue());  
       }
	   */
       
       if(request.getParameter("merchandise") != null && request.getParameter("merchandise").length() > 0)
       {
         bVO.setProductAmt(Double.valueOf(request.getParameter("merchandise")).doubleValue());
       }
       else
       {
         bVO.setProductAmt(Double.valueOf("0").doubleValue());  
       }

	   /* Discount product price will always be the same as merchandise since
	   * an add bill for an order will not have a discount. msk 10/04/2005
	   */
	   bVO.setDiscountProdPrice(bVO.getProductAmt());

       if(request.getParameter("service_fee_tax") != null && request.getParameter("service_fee_tax").length() > 0)
       {
         bVO.setServiceFeeTax(Double.valueOf(request.getParameter("service_fee_tax")).doubleValue());
       }
       else
       {
         bVO.setServiceFeeTax(Double.valueOf("0").doubleValue());  
       }
       
       if(request.getParameter("shipping_tax") != null && request.getParameter("shipping_tax").length() > 0)
       {
         bVO.setShippingTax(Double.valueOf(request.getParameter("shipping_tax")).doubleValue());
       }
       else
       {
         bVO.setShippingTax(Double.valueOf("0").doubleValue());  
       }

       String shippingServiceFee = request.getParameter("service_shipping");
       // If it is a fresh cut product
       if(request.getParameter("product_type") != null &&
    		   (request.getParameter("product_type").equalsIgnoreCase("FRECUT") ||
				request.getParameter("product_type").equalsIgnoreCase("SDFC")))
       {
         if(shippingServiceFee != null && shippingServiceFee.length() > 0)
         {
           bVO.setServiceFee(Double.valueOf(shippingServiceFee).doubleValue());
         }
         else
         {
           bVO.setServiceFee(Double.valueOf("0").doubleValue());  
         }								
       }
       // If it is a same day gift
       else if(
			(request.getParameter("ship_method") != null &&
				request.getParameter("ship_method").equalsIgnoreCase("SD"))
			&&
			(request.getParameter("product_type") != null &&
				request.getParameter("product_type").equalsIgnoreCase("SDG"))
			)
			{
         if(shippingServiceFee != null && shippingServiceFee.length() > 0)
         {
           bVO.setShippingFee(Double.valueOf(shippingServiceFee).doubleValue());
         }
         else
         {
           bVO.setShippingFee(Double.valueOf("0").doubleValue());  
         }					
			}
       // If it is dropship
       else if(request.getParameter("vendor_flag") != null &&
				request.getParameter("vendor_flag").equalsIgnoreCase("Y"))
       {
         if(shippingServiceFee != null && shippingServiceFee.length() > 0)
         {
           bVO.setShippingFee(Double.valueOf(shippingServiceFee).doubleValue());
         }
         else
         {
           bVO.setShippingFee(Double.valueOf("0").doubleValue());  
         }					
			}
       // If it is floral
       else
       {
         if(shippingServiceFee != null && shippingServiceFee.length() > 0)
         {
           bVO.setServiceFee(Double.valueOf(shippingServiceFee).doubleValue());
         }
         else
         {
           bVO.setServiceFee(Double.valueOf("0").doubleValue());  
         }
       }
 
       if(request.getParameter("product_tax") != null && request.getParameter("product_tax").length() > 0)
       {
         bVO.setProductTax(Double.valueOf(request.getParameter("product_tax")).doubleValue());
       }
       else
       {
         bVO.setProductTax(Double.valueOf("0").doubleValue());  
       }
       bVO.setBillStatus("Unbilled");
       bVO.setCreatedBy(csrId);
       bVO.setDiscountType("N");
       bVO.setOrderDetailId(Long.valueOf(request.getParameter("order_detail_id")).longValue());
       bVO.setAddtBillInd("Y");
       
       
       // Set tax info, ensuring that null tax infos are not populated and that any blanks in the 
       // sequence are filled in, e.g. if 1 and 3 are populated, but tax2 is not, then tax3 goes into tax2
       populateNextTaxInfo(request, bVO, "tax1");
       populateNextTaxInfo(request, bVO, "tax2");
       populateNextTaxInfo(request, bVO, "tax3");
       populateNextTaxInfo(request, bVO, "tax4");
       populateNextTaxInfo(request, bVO, "tax5");
    }

    //Populate a PaymentVO 
    pVO.setOrderGuid(request.getParameter("order_guid"));

    logger.debug("aafes_code = " + request.getParameter("aafes_code"));
    logger.debug("auth_code = " + request.getParameter("auth_code"));
    logger.debug("manual_auth = " + request.getParameter("mauual_auth"));

    if (request.getParameter("aafes_code") != null)
    {
      pVO.setAafesTicketNumber(request.getParameter("aafes_code"));
    }        
    pVO.setPaymentType(request.getParameter("pay_type"));  
    pVO.setUpdatedBy(csrId);
    if (request.getParameter("manual_auth").equalsIgnoreCase("Y"))
    {
      pVO.setAuthOverrideFlag("Y");
      pVO.setVoiceAuth(true);
    }
    else
    {
      pVO.setAuthOverrideFlag("N");
    }
    //1,987.38
    String total=request.getParameter("total");
    double creditAmount=0.00;
    logger.debug("total amount="+total);
    if(total.indexOf(",")!=-1)
    {
      DecimalFormat decimalFormat=new DecimalFormat("#,###,###.00");
      creditAmount = decimalFormat.parse(total).doubleValue();
      
    }else
    {
      creditAmount=Double.parseDouble(total);
    }
    logger.debug("total creditAmount="+creditAmount);
    
    pVO.setCreditAmount(creditAmount);          
    
    pVO.setPaymentInd("P");
    pVO.setAuthDate(new Date());

    logger.debug("pay_type = " + request.getParameter("pay_type"));
    logger.debug("exp_date_month = " + request.getParameter("exp_date_month"));
    logger.debug("exp_date_year = " + request.getParameter("exp_date_year"));
    logger.debug("total = " + request.getParameter("total"));
    logger.debug("cardinal_verified_flag = " + request.getParameter("cardinal_verified_flag"));
    logger.debug("Payment Gateway route = " + request.getParameter("route"));
    String cardinalVerifiedFlag = request.getParameter("cardinal_verified_flag");
    //Add the paymentVO to the array when a credit card payment is found
    if (request.getParameter("credit_card_num") != null && request.getParameter("credit_card_num").length() > 0)
    {
	  ccVO = new CreditCardVO();
	  cccVO = new CommonCreditCardVO();
      
	  //if the original credit card is used, only 4 digits are coming in from the screen
      //set the card fields to null and go get the full card number in the bo.
      logger.debug("is_orig_cc = " + request.getParameter("is_orig_cc"));
      logger.debug("billingLevel: " + billingLevel);
 
      if(request.getParameter("is_orig_cc").equalsIgnoreCase("Y"))
      {
        cccVO.setCreditCardNumber(null);
        pVO.setCardNumber(null);
        if(billingLevel.equalsIgnoreCase(COMConstants.BILLING_LEVEL_CART)){
        	pVO.setWalletIndicator(request.getParameter("wallet_indicator"));
	        //2746 Retain Cardinal Commerce values when same credit card number is used 
	        if(cardinalVerifiedFlag != null && cardinalVerifiedFlag.equalsIgnoreCase("Y")){
				pVO.setCardinalVerifiedFlag("Y");
	        }
	        else{
	        	pVO.setCardinalVerifiedFlag("N");
	        }
		}
      }
      else
      {
        cccVO.setCreditCardNumber(request.getParameter("credit_card_num"));
        pVO.setCardNumber(request.getParameter("credit_card_num"));
        if(billingLevel.equalsIgnoreCase(COMConstants.BILLING_LEVEL_CART)){
        	pVO.setWalletIndicator(null);
	        //2746 Remove Cardinal Commerce values when Manual Auth is done 
			if(cardinalVerifiedFlag != null && cardinalVerifiedFlag.equalsIgnoreCase("Y")){
				pVO.setCardinalVerifiedFlag("N");
			}
		}
      }
      	
      	
      	pVO.setRoute(request.getParameter("route"));
      
	    cccVO.setExpirationDate(request.getParameter("exp_date_year") + request.getParameter("exp_date_month"));
	    cccVO.setCreditCardType(request.getParameter("pay_type"));
	    cccVO.setAmount(request.getParameter("total"));
	    cccVO.setApprovalCode(request.getParameter("auth_code"));
	    ccVO.setCcExpiration(request.getParameter("exp_date_month") + "/" + request.getParameter("exp_date_year"));
		ccVO.setUpdatedBy(csrId);
		payments.add(pVO);
    }
    else
    {
      /*  Add the paymentVO to the array if the payment is not for a gift
          certificate or coupon
      */
      if(!pVO.getPaymentType().equalsIgnoreCase(GCC_PAYMENT_TYPE))
      {      
        payments.add(pVO);
      }
    }
    
    //Add the paymentVO to the array when a gift certificate payment is found
    if (request.getParameter("gcc_num") != null && request.getParameter("gcc_num").length() > 0)
    {
       pVO = new PaymentVO();
       pVO.setOrderGuid(request.getParameter("order_guid"));
       pVO.setPaymentType("GC");      
       pVO.setCreditAmount(Double.valueOf(request.getParameter("total")).doubleValue());
       pVO.setGcCouponNumber(request.getParameter("gcc_num"));
       pVO.setPaymentInd("P");
       pVO.setUpdatedBy(csrId);
       pVO.setAuthDate(new Date());
       payments.add(pVO);
       
       gcVO.setGcCouponNumber(request.getParameter("gcc_num"));
       gcVO.setAmount(request.getParameter("gcc_amt"));
       gcVO.setUpdatedBy(csrId);
    }
    
    if (payments != null && payments.size() > 0)
    {
      payVO = (PaymentVO[]) payments.toArray(new PaymentVO [payments.size()]); 
    }
    else
    {
      payVO = null;
    }
    Document applyBillingDoc = abBO.applyBilling(request.getParameter("order_detail_id"),
                                                    request.getParameter("order_guid"),
                                                    billingLevel, mgrCode, mgrPassword,
                                                    context, bVO, payVO, gcVO, ccVO, cccVO, csrId);

    if(logger.isDebugEnabled())
    {
       //IMPORTANT:  Because StringWriter contents are NOT removed even when the flush
       //            method is executed, make StringWriter go away through scope control.
       //            Both the flush and close methods were tested with but were of none affect.
       StringWriter sw = new StringWriter();       //string representation of xml document
       DOMUtil.print((Document) applyBillingDoc,new PrintWriter(sw));
       logger.debug("- execute(): COM returned = \n" + sw.toString());
    }
    return applyBillingDoc;
  }

  private void populateNextTaxInfo(HttpServletRequest request, BillingVO bVO, String taxPrefix) {
	   String taxname = request.getParameter(taxPrefix + "_name");
	   if (StringUtils.isBlank(taxname))
		   return;

	  if (bVO.getTax1Name() == null) {
		  bVO.setTax1Name(request.getParameter(taxPrefix + "_name"));
		  bVO.setTax1Description(request.getParameter(taxPrefix + "_description"));
		  bVO.setTax1Rate(new BigDecimal(request.getParameter(taxPrefix + "_rate")));
		  bVO.setTax1Amount(new BigDecimal(request.getParameter(taxPrefix + "_amount")));
	  } else if (bVO.getTax2Name() == null) {
		  bVO.setTax2Name(request.getParameter(taxPrefix + "_name"));
		  bVO.setTax2Description(request.getParameter(taxPrefix + "_description"));
		  bVO.setTax2Rate(new BigDecimal(request.getParameter(taxPrefix + "_rate")));
		  bVO.setTax2Amount(new BigDecimal(request.getParameter(taxPrefix + "_amount")));
	  } else if (bVO.getTax3Name() == null) {
		  bVO.setTax3Name(request.getParameter(taxPrefix + "_name"));
		  bVO.setTax3Description(request.getParameter(taxPrefix + "_description"));
		  bVO.setTax3Rate(new BigDecimal(request.getParameter(taxPrefix + "_rate")));
		  bVO.setTax3Amount(new BigDecimal(request.getParameter(taxPrefix + "_amount")));
	  } else if (bVO.getTax4Name() == null) {
		  bVO.setTax4Name(request.getParameter(taxPrefix + "_name"));
		  bVO.setTax4Description(request.getParameter(taxPrefix + "_description"));
		  bVO.setTax4Rate(new BigDecimal(request.getParameter(taxPrefix + "_rate")));
		  bVO.setTax4Amount(new BigDecimal(request.getParameter(taxPrefix + "_amount")));
	  } else if (bVO.getTax5Name() == null) {
		  bVO.setTax5Name(request.getParameter(taxPrefix + "_name"));
		  bVO.setTax5Description(request.getParameter(taxPrefix + "_description"));
		  bVO.setTax5Rate(new BigDecimal(request.getParameter(taxPrefix + "_rate")));
		  bVO.setTax5Amount(new BigDecimal(request.getParameter(taxPrefix + "_amount")));
	  }
  }
  
  /**
   * Recalculate Order
   * @param request
   * @param connection
   * @return Document - recalculated order
   * @throws Exception
   * @throws IOException
   * @throws ParserConfigurationException
   * @throws SAXException
   * @throws TransformerException
   * @throws XSLException
   */
   
  private Document recalculateOrder(HttpServletRequest request, Connection conn)
		throws IOException, ParserConfigurationException, SAXException, TransformerException, 
               NumberFormatException, Exception
  {
    logger.debug("Got to the recalculateOrder method = " + request.getParameter("merchandise"));

    AddBillingBO abBO = new AddBillingBO(conn);
    OrderDetailsVO odVO = new OrderDetailsVO();    
 
    odVO.setOrderDetailId(new Long(request.getParameter(COMConstants.ORDER_DETAIL_ID)).longValue());
    odVO.setProductsAmount(request.getParameter("merchandise"));
    odVO.setDiscountAmount(request.getParameter("discount"));
    odVO.setAddOnAmount(request.getParameter("add_on"));
    odVO.setNoTaxFlag(request.getParameter("no_tax_flag"));

     if(!request.getParameter("ship_method").equalsIgnoreCase("SD") &&
        request.getParameter("ship_method") != null)
    {
      odVO.setShippingFeeAmount(request.getParameter("service_shipping"));
    }
    else
    {
      odVO.setServiceFeeAmount(request.getParameter("service_shipping"));
    }
    
    String gccAmt = request.getParameter("gcc_amt");
    odVO.setGccAmt(gccAmt == null || gccAmt.equalsIgnoreCase("") ? "0" : gccAmt);
    
    odVO.setProductId(request.getParameter("product_id"));
    odVO.setGuid(request.getParameter("order_guid"));

    
    // order detail has recipients, which have addresses, which have a state/province
    RecipientAddressesVO raVO = new RecipientAddressesVO();
    raVO.setStateProvince(request.getParameter("recipient_state"));    
    ArrayList recip_addresses = new ArrayList();
    recip_addresses.add(raVO);
    RecipientsVO rVO = new RecipientsVO();
    rVO.setRecipientAddresses(recip_addresses);
    ArrayList recips = new ArrayList(); 
    recips.add(rVO);
    odVO.setRecipients(recips);        
    
    Document recalcOrdDoc = abBO.recalculateOrder(odVO, request.getParameter("order_guid"), request.getParameter("return_action"));

    if(logger.isDebugEnabled())
    {
       //IMPORTANT:  Because StringWriter contents are NOT removed even when the flush
       //            method is executed, make StringWriter go away through scope control.
       //            Both the flush and close methods were tested with but were of none affect.
       StringWriter sw = new StringWriter();       //string representation of xml document
       DOMUtil.print((Document) recalcOrdDoc,new PrintWriter(sw));
       logger.debug("- execute(): COM returned = \n" + sw.toString());
    }
    return recalcOrdDoc;
  }

  /**
   * create status document
   * @param String  - status
   * @param String  - action
   * @param String  - message
   * @return Document - status
   * @throws Exception
   * @throws IOException
   * @throws ParserConfigurationException
   * @throws SAXException
   * @throws SQLException
   * @throws TransformerException
   * @throws XSLException
   */
   
  private Document createStatusDocument(String status, String action, String msg, String isError)
        throws IOException, ParserConfigurationException, SAXException, 
                TransformerException, Exception
  {
    String messageText = ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, msg);

    Document returnDocument = DOMUtil.getDefaultDocument();
    Element root = (Element) returnDocument.createElement(DATA_ROOT_NODE);
    returnDocument.appendChild(root);

    Element errorNode = (Element)returnDocument.createElement("is_error");
    errorNode.appendChild(returnDocument.createTextNode(isError));
    root.appendChild(errorNode);
    
    Element statusElement = (Element) returnDocument.createElement(STATUS_ROOT_NODE);
    root.appendChild(statusElement);
    
    Element dataElement = (Element)returnDocument.createElement("data");  
    statusElement.appendChild(dataElement);

    Element node = (Element)returnDocument.createElement("message_status");
    node.appendChild(returnDocument.createTextNode(status));
    dataElement.appendChild(node);
   
    node = (Element)returnDocument.createElement("message_action");
    node.appendChild(returnDocument.createTextNode(action));
    dataElement.appendChild(node);

    node = (Element)returnDocument.createElement("message_text");
    node.appendChild(returnDocument.createTextNode(messageText));
    dataElement.appendChild(node);
    
    root.appendChild(statusElement);
    
    return returnDocument;  
  }

  /**
   * Obtain connectivity with the database
   * @param none
   * @return Connection - db connection
   * @throws Exception
   * @throws IOException
   * @throws ParserConfigurationException
   * @throws SAXException
   * @throws TransformerException
   * @throws XSLException
   */

	private Connection getDBConnection()
		throws IOException, ParserConfigurationException, SAXException, TransformerException, 
               Exception
	{
        Connection conn = null;
		conn = DataSourceUtil.getInstance().getConnection(
				   ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE,
															   COMConstants.DATASOURCE_NAME));
		return conn;
	}
    
  /**
  * Retrieve name of Update Billing Action XSL file
  * @param1 String - the xsl name  
  * @param2 ActionMapping
  * @return File - XSL File name
  * @throws none
  */

  private File getXSL(String xslName, ActionMapping mapping)
  {

    File xslFile = null;
    String xslFilePathAndName = null;

    ActionForward forward = mapping.findForward(xslName);
    xslFilePathAndName = forward.getPath();
    //get real file name
    xslFile = new File(this.getServlet().getServletContext().getRealPath(xslFilePathAndName));
    return xslFile;
  }
}