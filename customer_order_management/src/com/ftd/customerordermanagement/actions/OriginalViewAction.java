package com.ftd.customerordermanagement.actions;

import java.io.File;
import java.math.BigDecimal;
import java.sql.Clob;
import java.sql.Connection;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.dao.LookupDAO;
import com.ftd.customerordermanagement.dao.OrderDAO;
import com.ftd.customerordermanagement.util.BasePageBuilder;
import com.ftd.ftdutilities.CalculateTaxUtil;
import com.ftd.mo.dao.UpdateOrderDAO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.handlers.FraudCodeHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.SourceMasterHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.SourceMasterVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.JAXPUtil;
import com.ftd.osp.utilities.xml.TraxUtil;



public final class OriginalViewAction extends Action 
{

    private static Logger logger  = new Logger("com.ftd.customerordermanagement.actions.OriginalViewAction");
    private String companyId;
    private boolean canadianRecipient;


  /**
   * This is the main action called from the Struts framework.
   * @param mapping The ActionMapping used to select this instance.
   * @param form The optional ActionForm bean for this request.
   * @param request The HTTP Request we are processing.
   * @param response The HTTP Response we are processing.
   */
  public  ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, 
                                HttpServletResponse response) 
  {
		ActionForward forward = null;
    Connection con = null;

    try
    {
      String xslName = "";
  
      //Connection/Database info
      con = DataSourceUtil.getInstance().getConnection(
            ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE,
            COMConstants.DATASOURCE_NAME));
  
  
      //Document that will contain the final XML, to be passed to the TransUtil and XSL page
      Document responseDocument = (Document) DOMUtil.getDocument();

      //Get info passed in the request object
      HashMap requestParms = new HashMap();
      requestParms = getRequestInfo(request);
  
      //Retrieve the data
      HashMap originalViewHash = getOrderXMLArchive(con, requestParms);

      //if data was found
      if(originalViewHash.get("dataFound").toString().equalsIgnoreCase(COMConstants.CONS_TRUE))
      {
        //if the request was to load the page, transfer to OriginalView.xsl
        if(requestParms.get(COMConstants.ACTION).toString().equalsIgnoreCase(COMConstants.ACTION_ORIGINAL_VIEW_LOAD))
        {
          //Retrieve the data
          Document originalViewDoc = (Document) originalViewHash.get("orderArchiveXML");
          logger.info(JAXPUtil.toString(originalViewDoc));
          Element productIdElement = (Element) DOMUtil.selectSingleNode(originalViewDoc,"/order/items/item[order-number='"+request.getParameter("display_order_number")+"']/productid");
          Element productSubCodeIdElement = (Element) DOMUtil.selectSingleNode(originalViewDoc,"/order/items/item[order-number='"+request.getParameter("display_order_number")+"']/product-subcode-id");
          if(productIdElement != null && productIdElement.getNodeValue() != null)
          {
              String productId = productIdElement.getNodeValue();

              // Search by product subcode id if it exists
              if(productSubCodeIdElement != null)
              {
                  String productSubCodeId = productSubCodeIdElement.getNodeValue();
                  
                  if(StringUtils.isNotBlank(productSubCodeId))
                  {
                      productId = productSubCodeId;
                  }
              }
              
              logger.debug("** Loading product info for product:" + productId);
              
              OrderDAO orderDAO = new OrderDAO(con);
              Document productInfo = orderDAO.getProductInfo(productId);
              DOMUtil.addSection(responseDocument, productInfo.getChildNodes());
          }
          Element addOnIdElement = (Element)DOMUtil.selectSingleNode(originalViewDoc,"order/items/item/add-ons/add-on/id");
          if(addOnIdElement != null && addOnIdElement.getNodeValue() != null)
          {
            Document addOnDoc = this.getAddsOnIdList(originalViewDoc, con);
            DOMUtil.addSection(responseDocument, addOnDoc.getChildNodes());
          }
          Element sourceCodeElement = (Element)DOMUtil.selectSingleNode(originalViewDoc,"order/header/source-code");
//          Element membershipElement = (Element)DOMUtil.selectSingleNode(originalViDOMUtil,ewDoc,"order/header/co-brands/co-brands/data"); 
          if(sourceCodeElement != null && sourceCodeElement.getNodeValue() != null)
          {
            Document membershipDoc = this.getMembershipInfo(sourceCodeElement.getNodeValue(), originalViewDoc, con);
            DOMUtil.addSection(responseDocument, membershipDoc.getChildNodes());
          }
          Element fraudIdElement = (Element) DOMUtil.selectSingleNode(originalViewDoc,"order/header/fraud-id");
          if(fraudIdElement != null && fraudIdElement.getNodeValue() != null)
          {
            Document fraudDescriptionDoc = this.getFraudDescriptionById(fraudIdElement.getNodeValue(), con);
            DOMUtil.addSection(responseDocument, fraudDescriptionDoc.getChildNodes());
          }
          Element noChargeOrderReference = (Element)DOMUtil.selectSingleNode(originalViewDoc,"order/header/no-charge/order-reference");
          if(noChargeOrderReference != null && noChargeOrderReference.getNodeValue() != null) {
              Document externalReferenceDoc = this.getExternalOrderNumber(noChargeOrderReference.getNodeValue(), con);
              DOMUtil.addSection(responseDocument, externalReferenceDoc.getChildNodes());
          }

          Element fraudCodeElement = (Element) DOMUtil.selectSingleNode(originalViewDoc,"order/header/fraud_codes/fraud_code");
          if(fraudCodeElement != null && fraudCodeElement.getNodeValue() != null)
          {
              Document fraudDescriptionDoc = this.getFraudCodeList(originalViewDoc, con);
              DOMUtil.addSection(responseDocument, fraudDescriptionDoc.getChildNodes());
          }

          HashMap pageData = new HashMap();
          pageData.put(COMConstants.CUSTOMER_ID, request.getParameter(COMConstants.CUSTOMER_ID));
          pageData.put("display_order_number", request.getParameter("display_order_number"));
        
          String orderNumber = request.getParameter("display_order_number");
          String recipCountry = DOMUtil.getNodeText(originalViewDoc, "//order/items/item[order-number='"+orderNumber+"']/recip-country/text()");
          
         // NodeList taxNodeList = DOMUtil.selectNodeList(originalViewDoc, "/order/items/item[order-number='"+orderNumber+"']/taxes/tax");
          //if(taxNodeList != null && taxNodeList.getLength() > 0){
          DOMUtil.addSection(responseDocument, this.getTaxesList(originalViewDoc, con, orderNumber, recipCountry).getChildNodes());
         // }  
          
          if("CA".equalsIgnoreCase(recipCountry)){
        	  this.canadianRecipient = true;
          }else{
        	  this.canadianRecipient = false;
          }
          
          if(!"FTDCA".equalsIgnoreCase(this.companyId) && this.canadianRecipient){
        	  String surchargeExplanation  = ConfigurationUtil.getInstance().
													getContent(con, "TAX", "TAX_EXPLANATION");
        	  pageData.put("surchargeExplanation", surchargeExplanation);
          }
          
          BigDecimal totalFees = this.calculateFees(originalViewDoc, orderNumber);
          pageData.put("total_fees", totalFees.toString());
          
          //Convert the page data hashmap to XML and append it to the final XML
          DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true); 
          UpdateOrderDAO updateOrderDAO = new UpdateOrderDAO(con);
          BasePageBuilder basePageBuilder = new BasePageBuilder();
          DOMUtil.addSection(responseDocument, updateOrderDAO.getAllShippingMethodXML().getChildNodes()); 
          DOMUtil.addSection(responseDocument, updateOrderDAO.getAllOccasionXML().getChildNodes()); 
          DOMUtil.addSection(responseDocument, basePageBuilder.retrieveAddressTypes(con).getChildNodes()); 
                    
          //Append all the existing XMLs to the final XML
          NodeList nl = originalViewDoc.getChildNodes();
          DOMUtil.addSection(responseDocument, nl);
        
          xslName = COMConstants.XSL_ORIGINAL_VIEW;
        }
        //if the request was to check that data exists, go to CheckOriginalViewStatusIFrame.xsl with <root/>
        else
          xslName = COMConstants.XSL_CHECK_ORIGINAL_VIEW;
      }
      //if the request was to check that data exists, go to CheckOriginalViewStatusIFrame.xsl 
      //and send a message as part of the pageData
      else
      {
        HashMap pageData = new HashMap();
        pageData.put(COMConstants.PD_MESSAGE_DISPLAY, COMConstants.MSG_NO_RECORDS);

        //Convert the page data hashmap to XML and append it to the final XML
        DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true); 
        
        xslName = COMConstants.XSL_CHECK_ORIGINAL_VIEW;
        
      }
  
      //Get XSL File name
      File xslFile = getXSL(xslName, mapping);	
  
      forward = mapping.findForward(xslName);
      String xslFilePathAndName = forward.getPath(); //get real file name
      
      // Change to client side transform
      TraxUtil.getInstance().getInstance().transform(request, response, responseDocument, 
                          xslFile, xslFilePathAndName, (HashMap)request.getAttribute(COMConstants.CONS_APP_PARAMETERS));
  
      return null;

    }
    catch(Exception e)
    {
      logger.error(e);
      forward = mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
      return forward;
    }

    finally
    {
      try
      {
        //Close the connection
        if(con != null)
        {
          con.close();
        }
      }
      catch(Exception e)
      {
        logger.error(e);
        forward = mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
        return forward;
      }
    }
  }

/******************************************************************************
  * getRequestInfo(HttpServletRequest request)
  *******************************************************************************
  * Retrieve the info from the request object, and set class level variables
  * @param  HttpServletRequest
  * @return none 
  * @throws none
  */
  private HashMap getRequestInfo(HttpServletRequest request)
  {
    HashMap requestParms = new HashMap();
    String masterOrderNumber = "";
    String action = "";
    
    if(request.getParameter(COMConstants.MASTER_ORDER_NUMBER)!=null)
      masterOrderNumber = request.getParameter(COMConstants.MASTER_ORDER_NUMBER);

    if(request.getParameter(COMConstants.ACTION)!=null)
      action = request.getParameter(COMConstants.ACTION);

    requestParms.put(COMConstants.MASTER_ORDER_NUMBER,  masterOrderNumber);
    requestParms.put(COMConstants.ACTION,               action);
    
    return requestParms;
  }

































  /******************************************************************************
  * getOrderXMLArchive()
  *******************************************************************************
  * Set security within XML Document
  * @return Document - contains input Document + security
  * @throws none
  */
  private HashMap getOrderXMLArchive(Connection con, HashMap requestParms) throws Exception
  {
    //create DataAccessUtil object
    DataAccessUtil util = DataAccessUtil.getInstance();
    
    //HashMap to be returned back.  The first element will tell if the data was found.  Subsequent
    //elements will be the XMLs
    HashMap orderArchiveHash = new HashMap();
    
    //Document(s) to be returned
    Document orderArchiveDoc = DOMUtil.getDocument();
    
    //Instantiate OrderDAO
    OrderDAO orderDAO = new OrderDAO(con);

    //master order number
    String masterOrderNumber = requestParms.get(COMConstants.MASTER_ORDER_NUMBER).toString();
  
    //action
    String action = requestParms.get(COMConstants.ACTION).toString();

    //Create a CachedResultSet object using the cursor name that was returned
    CachedResultSet orderInfo = (CachedResultSet) orderDAO.getOrderArchive(masterOrderNumber);

    //flag that will govern the value of the first element in the HashMap.
    boolean dataFound = false;
    
    //issue the NEXT
    while (orderInfo.next())
    {
      //set the flag
      dataFound = true;
      
      if (action.equalsIgnoreCase(COMConstants.ACTION_ORIGINAL_VIEW_LOAD))
      {
        //get the clob from the resultset
        Clob cOrderArchive = (Clob) orderInfo.getClob("ORDER_XML");
        
        //convert the clob in a string
        String xmlString = util.toXMLString(cOrderArchive);
    
        //assign the parsed info in the Document
        orderArchiveDoc = DOMUtil.getDocument(xmlString);
      }
    }
    
    if (dataFound)
      orderArchiveHash.put("dataFound",       "true");
    else
      orderArchiveHash.put("dataFound",       "false");
    
    orderArchiveHash.put("orderArchiveXML", orderArchiveDoc);
    
    //return the HashMap
    return orderArchiveHash;

  }
  /**
   * Looks up the descriptions for the necessary Add-On ids for original view page.
   * Returns an XML document with the AddOn ids/descriptions.
   * 
   * @param originalViewDoc
   * @param con
   * @return Document 
   * @throws java.lang.Exception
   */
  private Document getAddsOnIdList(Document originalViewDoc, Connection con)throws Exception
  {
    LookupDAO lookupDAO = new LookupDAO(con);
    NodeList addOns = (NodeList) DOMUtil.selectNodes(originalViewDoc,"order/items/item/add-ons/add-on/id");
    Document newDocument =  DOMUtil.getDefaultDocument();
    Element root = (Element) newDocument.createElement("root");
    Element addOnIds = (Element) newDocument.createElement("addOnIds");
    for (int i = 0; i < addOns.getLength(); i++)
    {
      Node node = (Node)addOns.item(i);
      Element addOnId = (Element) newDocument.createElement("addOnId"); 
      Element id = (Element) newDocument.createElement("id"); 
      String idStr  = node.getFirstChild().getNodeValue();
      id.appendChild(newDocument.createTextNode(idStr));
      addOnId.appendChild(id);
      Element description = (Element) newDocument.createElement("description"); 
      description.appendChild(newDocument.createTextNode(lookupDAO.getAddonDescriptionById(idStr)));
      addOnId.appendChild(description);
      addOnIds.appendChild(addOnId);
    }
    newDocument.appendChild(addOnIds);
    return newDocument;
  }

    /**
     * Looks up the fraud description for the given fraud ids.
     * Returns an XML document with the fraud descriptions.
     * 
     * @param originalViewDoc
     * @param con
     * @return Document 
     * @throws java.lang.Exception
     */
  private Document getFraudCodeList(Document originalViewDoc, Connection con) throws Exception {
      NodeList fraudCodes = DOMUtil.selectNodes(originalViewDoc,"order/header/fraud_codes/fraud_code");
      Document newDocument =  DOMUtil.getDocument();
      Element novatorFraudDescription = newDocument.createElement("novator-fraud-description");
      String description = null;

      FraudCodeHandler fraudCodeHandler = (FraudCodeHandler)CacheManager.getInstance().getHandler("CACHE_NAME_FRAUD_CODES");

      for (int i = 0; i < fraudCodes.getLength(); i++) {
          Node node = fraudCodes.item(i);
          String fraudCode = node.getFirstChild().getNodeValue();
          
          String fraudDescription = fraudCode;
          // look up code description from cache
          if(fraudCodeHandler != null)
          {
              fraudDescription = fraudCodeHandler.getFraudDescriptionById(fraudCode);
              if (fraudDescription == null) fraudDescription = fraudCode;
          }
          else
          {
              logger.error("Fraud codes were not loaded correctly into cache.");
          }
          if (description == null) {
              description = fraudDescription;
          } else {
              description = description + ", " + fraudDescription;
          }
      }
      novatorFraudDescription.appendChild(newDocument.createTextNode(description));
      newDocument.appendChild(novatorFraudDescription);
      return newDocument;
  }

  /**
   * Looks up the fraud disposition description for the given fraud
   * id.
   * Returns an XML document with the fraud disposition description.
   * 
   * @param fraudId
   * @param con
   * @return Document 
   * @throws java.lang.Exception
   */
  private Document getFraudDescriptionById(String fraudId, Connection con)throws Exception
  {
    LookupDAO lookupDAO = new LookupDAO(con);
    Document newDocument =  DOMUtil.getDefaultDocument();
    Element fraudDescription = (Element) newDocument.createElement("fraud-description");
    fraudDescription.appendChild(newDocument.createTextNode(lookupDAO.getFraudDescriptionById(fraudId)));
    newDocument.appendChild(fraudDescription);
    return newDocument;
  }

  private Document getMembershipInfo(String sourceCode, Document originalViewDoc, Connection con)throws Exception
  {
    Document newDocument =  DOMUtil.getDocument();
    UpdateOrderDAO updateOrderDAO = new UpdateOrderDAO(con);
    CachedResultSet rs = null;
    String progName = null;
    try {
      rs = updateOrderDAO.getPartnerProgram(sourceCode);
      if (rs != null && rs.next()) {
        progName = rs.getString("program_name");
      }
    }
    finally { 
    }
    if (progName != null)
    {
      NodeList data = (NodeList) DOMUtil.selectNodes(originalViewDoc,"order/header/co-brands/co-brand/data");
      NodeList names = (NodeList) DOMUtil.selectNodes(originalViewDoc,"order/header/co-brands/co-brand/name");
      String firstName = null;
      String lastName = null; 
      String membershipNumber = null; 
      for (int i = 0; i < data.getLength(); i++)
      {
        if (((Node) data.item(i)).getFirstChild() == null) 
        {
          continue;
        }
        String dataStr = ((Node) data.item(i)).getFirstChild().getNodeValue();
        String nameStr = ((Node) names.item(i)).getFirstChild().getNodeValue();
        if (nameStr != null && nameStr.equalsIgnoreCase("FNAME"))
        {
          firstName = dataStr;
        }
        else if (nameStr != null && nameStr.equalsIgnoreCase("LNAME"))
        {
          lastName = dataStr;
        }
        else if (nameStr != null && nameStr.equalsIgnoreCase(progName))
        {
          membershipNumber = dataStr;
        }
      }
      Element root = (Element) newDocument.createElement("membershipInfos");
      if ((progName != null)&& (membershipNumber!= null)&&(firstName != null) && (lastName != null))
      {
        Element membershipInfo = (Element) newDocument.createElement("membershipInfo");
        Element firstNameElem = (Element) newDocument.createElement("firstName"); 
        firstNameElem.appendChild(newDocument.createTextNode(firstName));
        membershipInfo.appendChild(firstNameElem);
        Element lastNameElem = (Element) newDocument.createElement("lastName"); 
        lastNameElem.appendChild(newDocument.createTextNode(lastName));
        membershipInfo.appendChild(lastNameElem);
        Element membershipNumberElem = (Element) newDocument.createElement("membershipNumber"); 
        membershipNumberElem.appendChild(newDocument.createTextNode(membershipNumber));
        membershipInfo.appendChild(membershipNumberElem);
        Element programNameElem = (Element) newDocument.createElement("programName"); 
        programNameElem.appendChild(newDocument.createTextNode(progName));
        membershipInfo.appendChild(programNameElem);
        root.appendChild(membershipInfo);
        newDocument.appendChild(root);
      }
    }
    return newDocument;
  }

    /**
     * Looks up the order detail record for the given order detail id
     * Returns an XML document with the external order number.
     * 
     * @param orderDetailId
     * @param con
     * @return Document 
     * @throws java.lang.Exception
     */
    private Document getExternalOrderNumber(String orderDetailId, Connection con)throws Exception
    {
      LookupDAO lookupDAO = new LookupDAO(con);
      Document newDocument =  DOMUtil.getDefaultDocument();
      Element externalOrderNumber = newDocument.createElement("nc-external-order-number");
      externalOrderNumber.appendChild(newDocument.createTextNode(lookupDAO.getExternalOrderNumber(orderDetailId)));
      newDocument.appendChild(externalOrderNumber);
      return newDocument;
    }

  /******************************************************************************
  *                                     getXSL()
  *******************************************************************************
  * Retrieve name of Customer Order Search Action XSL file
  * @param1 String - the xsl name returned from the Business Object
  * @param2 ActionMapping
  * @return File - XSL File name
  * @throws none
  */

  private File getXSL(String xslName, ActionMapping mapping)
  {

    File xslFile = null;
    String xslFilePathAndName = null;

    ActionForward forward = mapping.findForward(xslName);
    xslFilePathAndName = forward.getPath();                //get real file name
    xslFile = new File(this.getServlet().getServletContext().getRealPath(xslFilePathAndName));
    return xslFile;
  }

  private Document getTaxesList(Document originalViewDoc, Connection con, String orderNumber, String recipientCountry) throws Exception {
		LookupDAO lookupDAO = new LookupDAO(con);

		Document resultDoc = DOMUtil.getDefaultDocument();
		Element root = (Element) resultDoc.createElement("order");
		Element taxesEle = (Element) resultDoc.createElement("taxes");
		root.appendChild(taxesEle);
		resultDoc.appendChild(root);
		
		Element sourceCodeElement = (Element) DOMUtil.selectSingleNode(originalViewDoc, "order/header/source-code");
		String sourceCode = sourceCodeElement.getTextContent();
		SourceMasterHandler handler = (SourceMasterHandler) CacheManager.getInstance().getHandler("CACHE_NAME_SOURCE_MASTER");
		SourceMasterVO vo = handler.getSourceCodeById(sourceCode);
		String companyId = "FTD";
		if (vo != null) {
			companyId = vo.getCompanyId();
		}
		
		this.companyId = companyId;
		String defaultDesc = new CalculateTaxUtil().getDefaultTaxLabel(recipientCountry, companyId);
		BigDecimal totalamt = null;
		Node totTaxes = null;
		
		// Tax Split
		NodeList taxes = (NodeList) DOMUtil.selectNodes(originalViewDoc, "/order/items/item[order-number='" + orderNumber + "']/taxes/tax");
		boolean showSplit = false;
		
		if(taxes != null && taxes.getLength() > 0) {
	  
			for (int i = 0; i < taxes.getLength(); i++) {
				
				Node taxNode = taxes.item(i);
				NodeList childNodes = taxNode.getChildNodes();
				String taxName = "";
				String txtTaxAmount = "0";
				String taxDesc = "";
				int length = childNodes.getLength();
				
				for (int j = 0; j < length; j++) {
					Node item = childNodes.item(j);
	
					if (item.getNodeType() == 1) {
						if ("name".equals(item.getNodeName()) || "type".equals(item.getNodeName())) {
							taxName = item.getTextContent();
						} else if ("amount".equals(item.getNodeName())) {
							txtTaxAmount = item.getTextContent();
						} else if ("description".equals(item.getNodeName())) {
							taxDesc = item.getTextContent();
						}
					}
				}
			  
			  
				BigDecimal taxAmount = new BigDecimal(txtTaxAmount);
				if (taxAmount.doubleValue() > 0) {
					
					String taxSortOrder = null;
					try {
						if(StringUtils.isEmpty(taxDesc)) {
							taxSortOrder = lookupDAO.getTaxDisplayOrderVal(taxName);
						} else {
							taxSortOrder = lookupDAO.getTaxDisplayOrderVal(taxDesc); 
						}
					} catch (Exception e) {
						logger.error("unable to get the display orer for taxName: "
								+ taxName + " or taxDesc:" + taxDesc + ", error message:" + e.getMessage());
						taxSortOrder = "0";
					}
	
					// display only when display order is greater than 0.
					if (!StringUtils.isEmpty(taxSortOrder) && Integer.parseInt(taxSortOrder) > 0) {
						Element taxEle = (Element) resultDoc.createElement("tax");
						
						Element descriptionEle = (Element) resultDoc.createElement("tax_description");
						if(StringUtils.isEmpty(taxDesc)) {
							taxDesc = defaultDesc;
						}
						descriptionEle.appendChild(resultDoc.createTextNode(taxDesc));
						taxEle.appendChild(descriptionEle);
						
						Element amountEle = (Element) resultDoc.createElement("tax_amount");
						amountEle.appendChild(resultDoc.createTextNode(String.valueOf(taxAmount)));
						taxEle.appendChild(amountEle);
						
						Element displayOrderEle = (Element) resultDoc.createElement("tax_display_order");
						displayOrderEle.appendChild(resultDoc.createTextNode(taxSortOrder));
						taxEle.appendChild(displayOrderEle);
						
						taxesEle.appendChild(taxEle);
	
						showSplit = true; // once entered we do not have to set total tax for US orders.
					}
				}
			}
		}
			
		// Show total tax for US recipients (new order XML)
		// Source code / company is at order level so moved out of for loop.
		if(!showSplit) {
						
			totTaxes = (Node) DOMUtil.selectSingleNode(originalViewDoc, "/order/items/item[order-number='" + orderNumber + "']/taxes/total_tax");
			Node totTaxAmount = null;
			
			if(totTaxes != null) {
				totTaxAmount = DOMUtil.selectSingleNode(totTaxes, "amount/text()");
						
				
				if(totTaxAmount != null && !StringUtils.isEmpty(totTaxAmount.getNodeValue())) {
					totalamt = new BigDecimal(totTaxAmount.getNodeValue());
				}
			
				Node totalDesc = DOMUtil.selectSingleNode(totTaxes, "description/text()");
				String desc = null;
				if(totalDesc == null || StringUtils.isEmpty(totalDesc.getNodeValue())) { // for joe orders total description will be null in original order xml
					desc = defaultDesc;
				} else {
					desc = totalDesc.getNodeValue();
				}
			
				if(totalamt.compareTo(BigDecimal.ZERO) > 0) {
					Element taxEle = (Element) resultDoc.createElement("tax");
					
					Element descriptionEle = (Element) resultDoc.createElement("tax_description");
					descriptionEle.appendChild(resultDoc.createTextNode(desc)); 
					taxEle.appendChild(descriptionEle);
					
					Element amountEle = (Element) resultDoc.createElement("tax_amount");
					amountEle.appendChild(resultDoc.createTextNode(String.valueOf(totalamt)));
					taxEle.appendChild(amountEle);
					
					Element displayOrderEle = (Element) resultDoc.createElement("tax_display_order");
					displayOrderEle.appendChild(resultDoc.createTextNode("1"));
					taxEle.appendChild(displayOrderEle);
					taxesEle.appendChild(taxEle);
				}			
			}
		}
		
		// Show total tax for US recipients (old order XML)
		if(!showSplit && totTaxes == null) {
			
			NodeList totTaxNodes = (NodeList) DOMUtil.selectNodeList(originalViewDoc, "/order/items/item[order-number='" + orderNumber + "']/tax-amount");
			Node totTaxAmount = null;
			if(totTaxNodes != null && totTaxNodes.getLength() == 1) {
				totTaxAmount = totTaxNodes.item(0);
			}
			if(totTaxAmount != null && !StringUtils.isEmpty(totTaxAmount.getTextContent())) {
				totalamt = new BigDecimal(totTaxAmount.getTextContent());
			}
			
			if(totalamt != null && totalamt.compareTo(BigDecimal.ZERO) > 0) {
				Element taxEle = (Element) resultDoc.createElement("tax");
				
				Element descriptionEle = (Element) resultDoc.createElement("tax_description");
				descriptionEle.appendChild(resultDoc.createTextNode(defaultDesc)); 
				taxEle.appendChild(descriptionEle);
				
				Element amountEle = (Element) resultDoc.createElement("tax_amount");
				amountEle.appendChild(resultDoc.createTextNode(String.valueOf(totalamt)));
				taxEle.appendChild(amountEle);
				
				Element displayOrderEle = (Element) resultDoc.createElement("tax_display_order");
				displayOrderEle.appendChild(resultDoc.createTextNode("1"));
				taxEle.appendChild(displayOrderEle);
				taxesEle.appendChild(taxEle);
			}
		
		}
		
		return resultDoc;
	}

    private BigDecimal calculateFees(Document originalViewDoc, String orderNumber) {
        BigDecimal totalFees = new BigDecimal("0");
        
        try {
            String nodeValue = DOMUtil.getNodeText(originalViewDoc, "//order/items/item[order-number='"+orderNumber+"']/service-fee/text()");
            if (!StringUtils.isEmpty(nodeValue)) {
                totalFees = totalFees.add(new BigDecimal(nodeValue));
            }
            nodeValue = DOMUtil.getNodeText(originalViewDoc, "//order/items/item[order-number='"+orderNumber+"']/drop-ship-charges/text()");
            if (!StringUtils.isEmpty(nodeValue)) {
                totalFees = totalFees.add(new BigDecimal(nodeValue));
            }
            nodeValue = DOMUtil.getNodeText(originalViewDoc, "//order/items/item[order-number='"+orderNumber+"']/same-day-upcharge/text()");
            if (!StringUtils.isEmpty(nodeValue)) {
                totalFees = totalFees.add(new BigDecimal(nodeValue));
            }
            nodeValue = DOMUtil.getNodeText(originalViewDoc, "//order/items/item[order-number='"+orderNumber+"']/morning-delivery-fee/text()");
            if (!StringUtils.isEmpty(nodeValue)) {
                totalFees = totalFees.add(new BigDecimal(nodeValue));
            }
            nodeValue = DOMUtil.getNodeText(originalViewDoc, "//order/items/item[order-number='"+orderNumber+"']/vendor-sat-upcharge/text()");
            if (!StringUtils.isEmpty(nodeValue)) {
                totalFees = totalFees.add(new BigDecimal(nodeValue));
            }
            nodeValue = DOMUtil.getNodeText(originalViewDoc, "//order/items/item[order-number='"+orderNumber+"']/vendor-sun-upcharge/text()");
            if (!StringUtils.isEmpty(nodeValue)) {
                totalFees = totalFees.add(new BigDecimal(nodeValue));
            }
            nodeValue = DOMUtil.getNodeText(originalViewDoc, "//order/items/item[order-number='"+orderNumber+"']/vendor-mon-upcharge/text()");
            if (!StringUtils.isEmpty(nodeValue)) {
                totalFees = totalFees.add(new BigDecimal(nodeValue));
            }
            nodeValue = DOMUtil.getNodeText(originalViewDoc, "//order/items/item[order-number='"+orderNumber+"']/fuel-surcharge/text()");
            if (!StringUtils.isEmpty(nodeValue)) {
                totalFees = totalFees.add(new BigDecimal(nodeValue));
            }
            nodeValue = DOMUtil.getNodeText(originalViewDoc, "//order/items/item[order-number='"+orderNumber+"']/alaska-hawaii-fee/text()");
            if (!StringUtils.isEmpty(nodeValue)) {
                totalFees = totalFees.add(new BigDecimal(nodeValue));
            }
            nodeValue = DOMUtil.getNodeText(originalViewDoc, "//order/items/item[order-number='"+orderNumber+"']/late-cutoff-fee/text()");
            if (!StringUtils.isEmpty(nodeValue)) {
                totalFees = totalFees.add(new BigDecimal(nodeValue));
            }
        } catch (Exception e) {
            logger.error("Bad fees: ", e);
        }
        logger.info("totalFees: " + totalFees.toString());
        return totalFees;
    }
}
