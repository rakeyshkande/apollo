package com.ftd.customerordermanagement.actions;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.dao.CustomerDAO;
import com.ftd.customerordermanagement.dao.LockDAO;
import com.ftd.customerordermanagement.dao.OrderDAO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityManager;

import java.io.File;
import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;

public class LoadHistoryAction extends Action 
{
    private static Logger logger  = new Logger("com.ftd.customerordermanagement.actions.LoadHistoryAction");
    private static String MANAGER_HISTORY_ROLE = "MANAGER_HISTORY_ROLE";
    private static String HISTORY_APP_NAME = "HISTORY_APP_NAME";
    private static String OUT_CUR = "OUT_CUR";
    private static String OUT_ID_COUNT = "OUT_ID_COUNT";
    private static String OUT_PARAMETERS = "out-parameters";
    private static String YES = "Y";
    private static String NO = "N";
    
  /**
   * This is the main action called from the Struts framework.
   * @param mapping The ActionMapping used to select this instance.
   * @param form The optional ActionForm bean for this request.
   * @param request The HTTP Request we are processing.
   * @param response The HTTP Response we are processing.
   */
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
  {
    String historyType = null;
    int pagePosition = 0; // page_position value in request object
    HashMap secondaryMap = new HashMap();
    // Document xmlLock = null;
    Document xmlHistory = null;
    // String userId = null; //default
    // String lockId = null; //customer ID or order guid or order detail id
    String customerId = null;
    String orderDetailId = null;
    String externalOrderNumber = null;
    String managerRole = null;
    int totalPage = 0;
    int totalCount = 0;
    int maxRecords = 0;
    Connection conn = null;
    
    try
    {
        // pull base document
        Document responseDocument = (Document) DOMUtil.getDocument();
        
        //get all the parameters in the request object
        // history type
        if(request.getParameter(COMConstants.HISTORY_TYPE)!=null)
        {
          historyType = request.getParameter(COMConstants.HISTORY_TYPE);
        }
        secondaryMap.put(COMConstants.HISTORY_TYPE, historyType);
        // page position
        if(request.getParameter(COMConstants.PAGE_POSITION)!=null)
        {
          pagePosition = Integer.parseInt(request.getParameter(COMConstants.PAGE_POSITION));
        }
        secondaryMap.put(COMConstants.PAGE_POSITION, new Integer(pagePosition).toString());
        // get customer ID
        if(request.getParameter(COMConstants.CUSTOMER_ID)!=null)
        {
          customerId = request.getParameter(COMConstants.CUSTOMER_ID);
        }
        secondaryMap.put(COMConstants.CUSTOMER_ID, customerId);
        // get order detail id
        if(request.getParameter(COMConstants.ORDER_DETAIL_ID)!=null)
        {
          orderDetailId = request.getParameter(COMConstants.ORDER_DETAIL_ID);
        }
        secondaryMap.put(COMConstants.ORDER_DETAIL_ID, orderDetailId);
        if(request.getParameter(COMConstants.EXTERNAL_ORDER_NUMBER)!=null)
        {
          externalOrderNumber = request.getParameter(COMConstants.EXTERNAL_ORDER_NUMBER);
        }
        secondaryMap.put(COMConstants.EXTERNAL_ORDER_NUMBER, externalOrderNumber);
        
        // get security token
        String sessionId = (String) request.getParameter(COMConstants.SEC_TOKEN);
        
        // get maxRecords from config file
        String maxrec = ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, COMConstants.CONS_MAX_HISTORY_PER_PAGE);
        if(maxrec!=null && !maxrec.equals(""))
          maxRecords = Integer.parseInt(maxrec);
        
        //get managerRole from config file
        managerRole = ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, MANAGER_HISTORY_ROLE);
        
        // calculate startNumber to pass to the page and also to the database
        int startNumber = (pagePosition-1) * maxRecords + 1;
        secondaryMap.put(COMConstants.PD_START_POSITION, new Integer(startNumber).toString());
        
        // get database connection
        conn =
        DataSourceUtil.getInstance().getConnection(ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE,
                                                    COMConstants.DATASOURCE_NAME));
          
            
        // Call load DAO method to load history based on the history_type you are looking for, by passing in the startNumber
        // and maxRecords and managerComment role found earlier.
        HashMap historyMap = null;
        if(historyType.equals(COMConstants.ORDER_HISTORY_TYPE))
        {
          OrderDAO order = new OrderDAO(conn);
          historyMap = order.loadOrderHistoryXML(orderDetailId, startNumber, maxRecords, managerRole);
        }
        else
        if (historyType.equals(COMConstants.CUSTOMER_HISTORY_TYPE))
        {
          CustomerDAO customer = new CustomerDAO(conn);
          historyMap = customer.loadCustomerHistoryXML(customerId, startNumber, maxRecords, managerRole);          
        }
        xmlHistory = (Document) historyMap.get(OUT_CUR);
        Document xmlCount = (Document) historyMap.get(OUT_PARAMETERS);
        totalCount = Integer.parseInt(DOMUtil.getNodeValue(xmlCount, OUT_ID_COUNT));
        DOMUtil.addSection(responseDocument, xmlHistory.getChildNodes());
        
        // Calculate the totalPage and add this to the secondary XML HashMap.
        // This is calculated by pulling the totalCount from the XML returned from the database (server-side Xpath)
        // and dividing it by the maxRecords.        
        totalPage = (int) Math.floor(totalCount / maxRecords);
        if(totalCount%maxRecords > 0)
          totalPage += 1;
          
        secondaryMap.put(COMConstants.PD_TOTAL_PAGES, new Integer(totalPage).toString());
        
        // Generate XML from secondary HashMap using the DOMUtil HashMap to XML conversion method addSection() .
         DOMUtil.addSection(responseDocument, "pageData", "data", secondaryMap, true);
         
         //close db connection
         conn.close();
         
        // Forward to transformation action with customerComments.xsl or orderComments.xsl based on the comment_type.
        //Get XSL File name
        File xslFile = getXSL(historyType, mapping);  
        ActionForward forward = mapping.findForward(historyType);
        String xslFilePathAndName = forward.getPath(); //get real file name
        
        // Change to client side transform
        TraxUtil.getInstance().getInstance().transform(request, response, responseDocument, 
                            xslFile, xslFilePathAndName, (HashMap)request.getAttribute(COMConstants.CONS_APP_PARAMETERS));
       
       return null;
    }
    catch (Throwable ex)
    {
      try
      {
        if(conn!=null)
          conn.close();
      }
      catch (SQLException se)
      {
        logger.error(se);
        return mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
      }
      logger.error(ex);
        return mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
    }
    finally 
    {
      try
      {
        if(conn!=null)
          conn.close();
      }
      catch (SQLException se)
      {
        logger.error(se);
        return mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
      }
    }  
  }

  /******************************************************************************
  *                                     getXSL()
  *******************************************************************************
  * Retrieve name of Customer Order Search Action XSL file
  * @param1 String - the xsl name returned from the Business Object
  * @param2 ActionMapping
  * @return File - XSL File name
  * @throws none
  */

  private File getXSL(String xslName, ActionMapping mapping)
  {

    File xslFile = null;
    String xslFilePathAndName = null;

    ActionForward forward = mapping.findForward(xslName);
    xslFilePathAndName = forward.getPath();                //get real file name
    xslFile = new File(this.getServlet().getServletContext().getRealPath(xslFilePathAndName));
    return xslFile;
  }
}