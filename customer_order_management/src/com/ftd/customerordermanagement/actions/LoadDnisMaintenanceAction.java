package com.ftd.customerordermanagement.actions;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.dao.DnisDAO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;

import java.io.File;
import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;

/**
 * LoadDnisMaintenanceAction class
 * 
 */

public final class LoadDnisMaintenanceAction extends Action 
{
    private static Logger logger  = new Logger("com.ftd.customerordermanagement.actions.LoadDnisMaintenanceAction");
    private static String OUT_CUR = "OUT_CUR";
    private static String OUT_ID_COUNT = "OUT_ID_COUNT";
    private static String OUT_PARAMETERS = "out-parameters";
    private static String YES = "Y";
    private static String NO = "N";
    private static String ON = "on";
  
  /**
   * This is the main action called from the Struts framework.
   * @param mapping The ActionMapping used to select this instance.
   * @param form The optional ActionForm bean for this request.
   * @param request The HTTP Request we are processing.
   * @param response The HTTP Response we are processing.
   */
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException
  {
    int pagePosition = 0; // page_position value in request object
    HashMap secondaryMap = new HashMap();
    int maxRecords = 0;
    Connection conn = null;
    
    try
    {
      // pull base document
      Document responseDocument = (Document) DOMUtil.getDocument();
      
      //get all the parameters in the request object
      // page position
      if(request.getParameter(COMConstants.PAGE_POSITION)!=null)
      {
        pagePosition = Integer.parseInt(request.getParameter(COMConstants.PAGE_POSITION));
      }
      secondaryMap.put(COMConstants.PAGE_POSITION, new Integer(pagePosition).toString());
      
      // get maxRecords from config file
      String maxrec = ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, COMConstants.CONS_MAX_DNIS_PER_PAGE);
      if(maxrec!=null && !maxrec.equals(""))
        maxRecords = Integer.parseInt(maxrec);
              
      // calculate startNumber to pass to the page and also to the database
      int startNumber = (pagePosition-1) * maxRecords + 1;
      secondaryMap.put(COMConstants.PD_START_POSITION, new Integer(startNumber).toString());
              
      // get database connection
      conn =
      DataSourceUtil.getInstance().getConnection(ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE,
                                                  COMConstants.DATASOURCE_NAME));
                    
      // Call load DAO method to load all dnis, by passing in the startNumber
      // and maxRecords and managerComment role found earlier.
      DnisDAO dnis = new DnisDAO(conn);
      HashMap dnisMap =  dnis.loadAllDnisXML(startNumber, maxRecords);
      Document xmlDnis = (Document) dnisMap.get(OUT_CUR);
      Document xmlCount = (Document) dnisMap.get(OUT_PARAMETERS);
      int totalCount = Integer.parseInt(DOMUtil.getNodeValue(xmlCount, OUT_ID_COUNT));
      DOMUtil.addSection(responseDocument, xmlDnis.getChildNodes());        

      // Calculate the totalPage and add this to the secondary XML HashMap.
      // This is calculated by pulling the totalCount from the XML returned from the database (server-side Xpath)
      // and dividing it by the maxRecords. 
      int totalPage = (int) Math.floor(totalCount / maxRecords);
      if(totalCount%maxRecords > 0)
        totalPage += 1;
      secondaryMap.put(COMConstants.PD_TOTAL_PAGES, new Integer(totalPage).toString());
      
      // Generate XML from secondary HashMap using the DOMUtil HashMap to XML conversion method addSection() .
       DOMUtil.addSection(responseDocument, "pageData", "data", secondaryMap, true);
      //responseDocument.print(System.out);
      //close db connection
      conn.close();
       
      // Forward to transformation action
      //Get XSL File name
      String xslName = "LoadDnisMaint";
      File xslFile = getXSL(xslName, mapping);  
      ActionForward forward = mapping.findForward(xslName);
      String xslFilePathAndName = forward.getPath(); //get real file name
      
      // Change to client side transform
      TraxUtil.getInstance().getInstance().transform(request, response, responseDocument, 
                          xslFile, xslFilePathAndName, (HashMap)request.getAttribute(COMConstants.CONS_APP_PARAMETERS));
      return null;
    }
    catch (Throwable ex)
    {
      try
      {
        if(conn!=null)
          conn.close();
      }
      catch (SQLException se)
      {
        logger.error(se);
        return mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
      }
      logger.error(ex);
      return mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
    }
    finally 
    {
      try
      {
        if(conn!=null)
          conn.close();
      }
      catch (SQLException se)
      {
        logger.error(se);
        return mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
      }
    }  
  }

  /******************************************************************************
  *                                     getXSL()
  *******************************************************************************
  * Retrieve name of Customer Order Search Action XSL file
  * @param1 String - the xsl name returned from the Business Object
  * @param2 ActionMapping
  * @return File - XSL File name
  * @throws none
  */

  private File getXSL(String xslName, ActionMapping mapping)
  {

    File xslFile = null;
    String xslFilePathAndName = null;

    ActionForward forward = mapping.findForward(xslName);
    xslFilePathAndName = forward.getPath();                //get real file name
    xslFile = new File(this.getServlet().getServletContext().getRealPath(xslFilePathAndName));
    return xslFile;
  }
}