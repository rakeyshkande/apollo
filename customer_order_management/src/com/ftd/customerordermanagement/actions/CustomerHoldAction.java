package com.ftd.customerordermanagement.actions;

import com.ftd.customerordermanagement.bo.RefundBO;
import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.dao.CommentDAO;
import com.ftd.customerordermanagement.dao.CustomerDAO;
import com.ftd.customerordermanagement.dao.DnisDAO;
import com.ftd.customerordermanagement.dao.LockDAO;
import com.ftd.customerordermanagement.dao.LossPreventionDAO;
import com.ftd.customerordermanagement.dao.OrderDAO;
import com.ftd.customerordermanagement.util.BasePageBuilder;
import com.ftd.customerordermanagement.vo.CommentsVO;
import com.ftd.customerordermanagement.vo.CustomerHoldVO;
import com.ftd.customerordermanagement.vo.OrderVO;
import com.ftd.mo.dao.UpdateOrderDAO;
import com.ftd.osp.framework.dispatcher.Dispatcher;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityManager;
import com.ftd.security.cache.vo.UserInfo;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;

import java.sql.Connection;
import java.sql.DriverManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.naming.InitialContext;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;


public class CustomerHoldAction extends Action
{


    private static Logger logger  = new Logger("com.ftd.customerordermanagement.actions.CustomerHoldAction");
    private String DISPLAY_LOAD_CUSTOMER = "loadcustomer";
    private String SAVE_CUSTOMER = "savecustomer";
    private static final String CANCEL_ORDERS = "cancelorders";
    private static final String RELEASE_ORDERS = "releaseorders";   
    
    private final static String ORDER_DETAIL_LOCK_ENTITY = "ORDER_DETAILS";    
    
    private static String OUT_LOCK_OBTAINED = "OUT_LOCK_OBTAINED";
    private static String NO = "N";    
    
    private static final String HOLD_REASON_CUSTOMER = "Customer";
    
    private final static String ORDER_PROCESSING_STATUS = "ORDER PROCESSING";
    
    private static final int DO_NOTHING = 1;
    private static final int INSERT_HOLD = 2;
    private static final int DELETE_HOLD = 3;
    private static final int UPDATE_HOLD = 4;
    
    private static final String REQUEST_PARAM_FROM_PAGE = "from_page";
    private static final String REQUEST_PARAM_CUST_ID = "customer_id";
    private static final String REQUEST_PARAM_DETAIL_ID= "order_detail_id";
    private static final String FROM_PAGE_LOSS = "loss_prevention";    
    private static final String REQUEST_PARAM_ORDER_GUID= "order_guid";    
    private static final String REQUEST_PARAM_DNIS_ID = "call_dnis";
    private static final String REQUEST_PARAM_DNIS_DESC = "dnis_desc";
    private static final String REQUEST_PARAM_SECURITYTOKEN = "securitytoken";
    private static final String REQUEST_PARAM_LOCK_OBTAINED = "lockobtained";
    private static final String REQUEST_PARAM_USER_ID = "userid";
    private static final String REQUEST_PARAM_MASTER_ORDER_NUMBER = "master_order_number";
    private static final String REQUEST_PARAM_EXTERNAL_ORDER_NUMBER = "external_order_number";
    
    private static final String LOSS_PREVENTION = "goto_loss_prevention";
    private static final String CUSTOMER_ACCOUNT = "goto_customer_account";
    private static final String SEARCH_ACTION = "search_action";
    private static final String MAIN_MENU = "main_menu";
    
    private static final String CUSTOMER_HOLD_LOCK_ENTITY = "CUSTOMER_HOLD";
    
    private static final String FROM_PAGE_CUSTOMER_ACCOUNT = "customer_account";
    private static final String FROM_PAGE_CART = "cart";
    private static final String FROM_PAGE_ORDER_DETAIL = "order_detail";   
    private static final String FROM_PAGE_CHARGE_BACK = "charge_back";
    
    private static final String CUSTOMER_TYPE_RECIPIENT = "R";
    private static final String CUSTOMER_TYPE_BUYER = "B";
    
    private static final String LOSS_PREVENTION_QUEUE = "LP";
    private static final String CREDIT_QUEUE = "CREDIT";
    
    private static final String EMPTY_USER_ID = "empty";

    
    
    public CustomerHoldAction()
    {
    }
    
    /**
     * This is the method that the struts framework invokes.  It takes a look
     * at the request and determines what type of processing should be done.
     * 
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return 
     * @throws java.lang.Exception
     */
	public  ActionForward execute(ActionMapping mapping, ActionForm form, 
                                  HttpServletRequest request, HttpServletResponse response) 
                                throws Exception
      {
          
        Connection conn = null;          
        ActionForward forward = null;    
        Document lockXML = null;
        
        try
            {
              //Get the parameter that determine what needs to be done
              String actionType = request.getParameter(COMConstants.ACTION_TYPE);
        
              /* get action type request parameter */
              if(actionType ==null)
              {
                //forward to error page
                logger.error("Missing action_type in CustomerHoldAction");
                return mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
              }

              //get guid and type hold that is being done                  
              String holdType = request.getParameter("r_hold_type");     
                    
              /* get db connection */
              ConfigurationUtil cu = ConfigurationUtil.getInstance();
              conn = DataSourceUtil.getInstance().getConnection(cu.getProperty(COMConstants.PROPERTY_FILE,
														COMConstants.DATASOURCE_NAME));

              LockDAO lockDAO = new LockDAO(conn);
      
              /* get security token */
              String sessionId = (String) request.getParameter(COMConstants.SEC_TOKEN);
              String context = (String) request.getParameter(COMConstants.CONTEXT);
              String customerId = (String) request.getParameter(REQUEST_PARAM_CUST_ID);
              String lockValue = (String) request.getParameter(REQUEST_PARAM_LOCK_OBTAINED);
              String fromPage = (String) request.getParameter(REQUEST_PARAM_FROM_PAGE);
              String lockResult = "";
              String userId = null;
        
              if(sessionId==null)
              {
                logger.error("Security token not found in request object");
              }
        
              /* retrieve user information */
              if(SecurityManager.getInstance().getUserInfo(sessionId)!=null)
              {
                userId = SecurityManager.getInstance().getUserInfo(sessionId).getUserID();
              }
			
              //get token
              String securityToken = request.getParameter(REQUEST_PARAM_SECURITYTOKEN);                  
            
              /* determine which action to process */
      
              //Execute the method to do whatever the user requested to be done 
              if(actionType.equals(DISPLAY_LOAD_CUSTOMER))
              {                      
                  //lock the customer
                 lockXML = lockDAO.retrieveLockXML(securityToken,userId,customerId,CUSTOMER_HOLD_LOCK_ENTITY);
      
                 processLoadCustomer(conn,request,response,mapping,lockXML,"");
              }
              else if(actionType.equals(SAVE_CUSTOMER))
              {        
                  processSaveCustomer(conn,request,response,mapping);
                  
                  //unlock customer
//                  if(lockValue != null && lockValue.length() > 0){
//                      lockDAO.releaseLock(securityToken,userId,customerId,CUSTOMER_HOLD_LOCK_ENTITY);
//                  }       
//                 return getForward(request, mapping);
                 
                 // Defect 1240: PCI compliance protections to remain on the same page.
                 //lock the customer
                 lockXML = lockDAO.retrieveLockXML(securityToken,userId,customerId,CUSTOMER_HOLD_LOCK_ENTITY);     
                 processLoadCustomer(conn,request,response,mapping,lockXML,"");
                 
              }
              else if(actionType.equals(CANCEL_ORDERS))
              {        
                  processCancel(conn,request,response,mapping,userId);

                  //unlock cusstomer
                  if(lockValue != null && lockValue.equals("Y")){
                      lockDAO.releaseLock(securityToken,userId,customerId,CUSTOMER_HOLD_LOCK_ENTITY);
                  }
                  
                   return getForward(request, mapping);
              }
              else if(actionType.equals(RELEASE_ORDERS))
              {        
                  String errorMsg = processRelease(conn,request,response,mapping,userId);

                  //unlock cusstomer
                  if(lockValue != null && lockValue.equals("Y")){
                      lockDAO.releaseLock(securityToken,userId,customerId,CUSTOMER_HOLD_LOCK_ENTITY);
                  } 

                  //if an error occured during release, get a new lock and load page
                  if(errorMsg != null && errorMsg.length() > 0)
                  {
                      //lock the customer
                     lockXML = lockDAO.retrieveLockXML(securityToken,userId,customerId,CUSTOMER_HOLD_LOCK_ENTITY);      
                     processLoadCustomer(conn,request,response,mapping,lockXML,errorMsg);                      
                  }
                  else
                  {
                       return getForward(request, mapping);                      
                  }//end if error message

              }              
              else if(actionType.equals(CUSTOMER_ACCOUNT))
              {        
                  
                  //unlock cusstomer
                  if(lockValue != null && lockValue.equals("Y")){
                      lockDAO.releaseLock(securityToken,userId,customerId,CUSTOMER_HOLD_LOCK_ENTITY);
                  } 

                    ActionForward actionForward = null;
                    
                    //get flag to indicate if this a recipient search
                    String recipSearchFlag = request.getParameter("recipient_search");
                    String action = "customer_search";
                    if(recipSearchFlag != null && recipSearchFlag.equals("Y"))
                    {
                        action = "recipient_search";
                    }
                    

                    //forward to Customer Account
                    actionForward = mapping.findForward("CustomerAccount");
                    String path = actionForward.getPath() 
                                + "?" + "action=" + action
                                + "&" + "call_dnis=" + request.getParameter("call_dnis")
                                + "&" + "call_brand_name=" + request.getParameter("call_brand_name")      
                                + "&" + "call_cs_number=" + request.getParameter("call_cs_number")
                                + "&" + COMConstants.CUSTOMER_ID + "=" + request.getParameter(REQUEST_PARAM_CUST_ID)
                                + "&" + COMConstants.TIMER_CALL_LOG_ID + "=" + request.getParameter(COMConstants.TIMER_CALL_LOG_ID)
                                + "&" + COMConstants.TIMER_ENTITY_HISTORY_ID + "=" + request.getParameter(COMConstants.TIMER_ENTITY_HISTORY_ID)
                                + "&" + COMConstants.TIMER_COMMENT_ORIGIN_TYPE + "=" + request.getParameter(COMConstants.TIMER_COMMENT_ORIGIN_TYPE)
                                + "&" + COMConstants.START_ORIGIN + "=" + request.getParameter(COMConstants.START_ORIGIN)
                                + "&" + COMConstants.SEC_TOKEN + "=" + request.getParameter(COMConstants.SEC_TOKEN)
                                + "&" + COMConstants.CONTEXT + "=" +request.getParameter(COMConstants.CONTEXT);
                                
                    
                    actionForward = new ActionForward(path, false);
                    
                   return actionForward;
              }              
              else if(actionType.equals(LOSS_PREVENTION))
              {        

                  
                  //unlock cusstomer
                  if(lockValue != null && lockValue.equals("Y")){
                      lockDAO.releaseLock(securityToken,userId,customerId,CUSTOMER_HOLD_LOCK_ENTITY);
                  } 
                  
                 ActionForward actionForward = null;
                  
                 //forward to Customer Account
                 actionForward = mapping.findForward("LossPrevention");
                 String path = actionForward.getPath() 
                                + "?" + COMConstants.ORDER_GUID + "=" + request.getParameter(COMConstants.ORDER_GUID)
                                + "&" + "call_dnis=" + request.getParameter("call_dnis")
                                + "&" + "call_brand_name=" + request.getParameter("call_brand_name")                                
                                + "&" + "call_cs_number=" + request.getParameter("call_cs_number")
                                + "&" + COMConstants.TIMER_CALL_LOG_ID + "=" + request.getParameter(COMConstants.TIMER_CALL_LOG_ID)
                                + "&" + COMConstants.CUSTOMER_ID + "=" + request.getParameter(REQUEST_PARAM_CUST_ID)
                                + "&" + COMConstants.TIMER_ENTITY_HISTORY_ID + "=" + request.getParameter(COMConstants.TIMER_ENTITY_HISTORY_ID)
                                + "&" + COMConstants.TIMER_COMMENT_ORIGIN_TYPE + "=" + request.getParameter(COMConstants.TIMER_COMMENT_ORIGIN_TYPE)
                                + "&" + COMConstants.START_ORIGIN + "=" + request.getParameter(COMConstants.START_ORIGIN)
                                + "&" + COMConstants.SEC_TOKEN + "=" + request.getParameter(COMConstants.SEC_TOKEN)
                                + "&" + COMConstants.CONTEXT + "=" +request.getParameter(COMConstants.CONTEXT);
                
                 return new ActionForward(path, false);



              }    
              else if(actionType.equals(SEARCH_ACTION))
              {
                  ActionForward actionForward = null;

                  //unlock cusstomer
                  if(lockValue != null && lockValue.equals("Y")){
                      lockDAO.releaseLock(securityToken,userId,customerId,CUSTOMER_HOLD_LOCK_ENTITY);
                  } 

                 //forward to Customer Account
                 actionForward = mapping.findForward("CustomerOrderSearch");
                 String path = actionForward.getPath() 
                                + "?" + "action=load_page"  
                                + "&" + "call_dnis=" + request.getParameter("call_dnis")
                                + "&" + "call_brand_name=" + request.getParameter("call_brand_name")                                
                                + "&" + "call_cs_number=" + request.getParameter("call_cs_number")
                                + "&" + COMConstants.TIMER_CALL_LOG_ID + "=" + request.getParameter(COMConstants.TIMER_CALL_LOG_ID)
                                + "&" + COMConstants.SEC_TOKEN + "=" + request.getParameter(COMConstants.SEC_TOKEN)
                                + "&" + COMConstants.CONTEXT + "=" +request.getParameter(COMConstants.CONTEXT);
                
                 return new ActionForward(path, false);    
              }
              else if(actionType.equals(MAIN_MENU))
              {        
                  ActionForward actionForward = null;

                  //unlock cusstomer
                  if(lockValue != null && lockValue.equals("Y")){
                      lockDAO.releaseLock(securityToken,userId,customerId,CUSTOMER_HOLD_LOCK_ENTITY);
                  } 

                 //forward to Customer Account
                 actionForward = mapping.findForward("MainMenu");
                 String path = actionForward.getPath() 
                                + "?" + "action_load=load_page"  
                                + "&" + "call_dnis=" + request.getParameter("call_dnis")
                                + "&" + "call_brand_name=" + request.getParameter("call_brand_name")                                
                                + "&" + "call_cs_number=" + request.getParameter("call_cs_number")
                                + "&" + COMConstants.TIMER_CALL_LOG_ID + "=" + request.getParameter(COMConstants.TIMER_CALL_LOG_ID)
                                + "&" + COMConstants.SEC_TOKEN + "=" + request.getParameter(COMConstants.SEC_TOKEN)
                                + "&" + COMConstants.CONTEXT + "=" +request.getParameter(COMConstants.CONTEXT);
                
                 return new ActionForward(path, false);                  
              }              
            }
            catch(Exception e)
            {
              logger.error(e);  
              forward = mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
            }
            finally
            {
                if(conn != null && !conn.isClosed())
                {
                    conn.close();
                }
            }
            
            return null;
            
      }
      
    private ActionForward getForward(HttpServletRequest request, ActionMapping mapping)
        throws Exception
    {
    
        ActionForward actionForward = null;
        
        String fromPage = (String) request.getParameter(REQUEST_PARAM_FROM_PAGE);
        
        String actionType = request.getParameter(COMConstants.ACTION_TYPE);
        
        
        
        logger.debug("From page:" + fromPage);
    
        if(fromPage.equals(FROM_PAGE_CUSTOMER_ACCOUNT))
        {
        
            //get flag to indicate if this a recipient search
            String recipSearchFlag = request.getParameter("recipient_search");
            String action = "customer_search";
            if(recipSearchFlag != null && recipSearchFlag.equals("Y"))
            {
                action = "recipient_search";
            }        
        
        
             //forward to Customer Account
             actionForward = mapping.findForward("CustomerAccount");
             String path = actionForward.getPath() 
                            + "?" + "action=" + action
                            + "&" + "call_dnis=" + request.getParameter("call_dnis")
                            + "&" + "call_brand_name=" + request.getParameter("call_brand_name")                                
                            + "&" + "call_cs_number=" + request.getParameter("call_cs_number")
                            + "&" + COMConstants.CUSTOMER_ID + "=" + request.getParameter(REQUEST_PARAM_CUST_ID)
                            + "&" + COMConstants.TIMER_CALL_LOG_ID + "=" + request.getParameter(COMConstants.TIMER_CALL_LOG_ID)
                            + "&" + COMConstants.TIMER_ENTITY_HISTORY_ID + "=" + request.getParameter(COMConstants.TIMER_ENTITY_HISTORY_ID)
                            + "&" + COMConstants.TIMER_COMMENT_ORIGIN_TYPE + "=" + request.getParameter(COMConstants.TIMER_COMMENT_ORIGIN_TYPE)
                            + "&" + COMConstants.START_ORIGIN + "=" + request.getParameter(COMConstants.START_ORIGIN)
                            + "&" + COMConstants.SEC_TOKEN + "=" + request.getParameter(COMConstants.SEC_TOKEN)
                            + "&" + COMConstants.CONTEXT + "=" +request.getParameter(COMConstants.CONTEXT);
    
            actionForward = new ActionForward(path, false);
                                
        }
        else if(request.getParameter(REQUEST_PARAM_FROM_PAGE).equals(FROM_PAGE_CART))
        {
             //forward to Cart page
             actionForward = mapping.findForward("CustomerAccount");
             String path = actionForward.getPath() 
                            + "?" + "action=customer_account_search"
                            + "&" + "recipient_flag=y"
                            + "&" + "in_order_number=" + request.getParameter(REQUEST_PARAM_MASTER_ORDER_NUMBER)
                            + "&" + "call_dnis=" + request.getParameter("call_dnis")
                            + "&" + "call_brand_name=" + request.getParameter("call_brand_name")                                
                            + "&" + "call_cs_number=" + request.getParameter("call_cs_number")
                            + "&" + COMConstants.CUSTOMER_ID + "=" + request.getParameter(REQUEST_PARAM_CUST_ID)
                            + "&" + COMConstants.TIMER_CALL_LOG_ID + "=" + request.getParameter(COMConstants.TIMER_CALL_LOG_ID)
                            + "&" + COMConstants.ORDER_NUMBER + "=" + request.getParameter(REQUEST_PARAM_MASTER_ORDER_NUMBER)//request.getParameter(COMConstants.ORDER_NUMBER)
                            + "&" + COMConstants.ORDER_GUID + "=" + request.getParameter(COMConstants.ORDER_GUID)                            
                            + "&" + COMConstants.TIMER_ENTITY_HISTORY_ID + "=" + request.getParameter(COMConstants.TIMER_ENTITY_HISTORY_ID)
                            + "&" + COMConstants.TIMER_COMMENT_ORIGIN_TYPE + "=" + request.getParameter(COMConstants.TIMER_COMMENT_ORIGIN_TYPE)
                            + "&" + COMConstants.START_ORIGIN + "=" + request.getParameter(COMConstants.START_ORIGIN)
                            + "&" + COMConstants.SEC_TOKEN + "=" + request.getParameter(COMConstants.SEC_TOKEN)
                            + "&" + COMConstants.CONTEXT + "=" +request.getParameter(COMConstants.CONTEXT);
    
             actionForward = new ActionForward(path, false);            
        }        
        else if(request.getParameter(REQUEST_PARAM_FROM_PAGE).equals(FROM_PAGE_ORDER_DETAIL))
        {
             //forward to Cart page
             actionForward = mapping.findForward("CustomerAccount");
             String path = actionForward.getPath() 
                            + "?" + "action=search"
                            + "&" + "recipient_flag=y"
                            + "&" + "in_order_number=" + request.getParameter("order_number")
                            + "&" + "call_dnis=" + request.getParameter("call_dnis")
                            + "&" + "call_brand_name=" + request.getParameter("call_brand_name")                                
                            + "&" + "call_cs_number=" + request.getParameter("call_cs_number")
                            + "&" + COMConstants.CUSTOMER_ID + "=" + request.getParameter(REQUEST_PARAM_CUST_ID)
                            + "&" + COMConstants.TIMER_CALL_LOG_ID + "=" + request.getParameter(COMConstants.TIMER_CALL_LOG_ID)
                            + "&" + COMConstants.ORDER_NUMBER + "=" + request.getParameter("order_number")
                            + "&" + COMConstants.ORDER_GUID + "=" + request.getParameter(COMConstants.ORDER_GUID)                            
                            + "&" + COMConstants.TIMER_ENTITY_HISTORY_ID + "=" + request.getParameter(COMConstants.TIMER_ENTITY_HISTORY_ID)
                            + "&" + COMConstants.TIMER_COMMENT_ORIGIN_TYPE + "=" + request.getParameter(COMConstants.TIMER_COMMENT_ORIGIN_TYPE)
                            + "&" + COMConstants.START_ORIGIN + "=" + request.getParameter(COMConstants.START_ORIGIN)
                            + "&" + COMConstants.SEC_TOKEN + "=" + request.getParameter(COMConstants.SEC_TOKEN)
                            + "&" + COMConstants.CONTEXT + "=" +request.getParameter(COMConstants.CONTEXT);
    
             actionForward = new ActionForward(path, false);            
        }        
        else if(request.getParameter(REQUEST_PARAM_FROM_PAGE).equals(FROM_PAGE_LOSS))
        {
                 actionForward = mapping.findForward("LossPrevention");
                 String path = actionForward.getPath() 
                                + "?" + COMConstants.ORDER_GUID + "=" + request.getParameter(COMConstants.ORDER_GUID)
                                + "&" + "call_dnis=" + request.getParameter("call_dnis")
                                + "&" + "call_brand_name=" + request.getParameter("call_brand_name")                                
                                + "&" + "call_cs_number=" + request.getParameter("call_cs_number")
                                + "&" + COMConstants.TIMER_CALL_LOG_ID + "=" + request.getParameter(COMConstants.TIMER_CALL_LOG_ID)
                                + "&" + COMConstants.CUSTOMER_ID + "=" + request.getParameter(REQUEST_PARAM_CUST_ID)
                                + "&" + COMConstants.TIMER_ENTITY_HISTORY_ID + "=" + request.getParameter(COMConstants.TIMER_ENTITY_HISTORY_ID)
                                + "&" + COMConstants.TIMER_COMMENT_ORIGIN_TYPE + "=" + request.getParameter(COMConstants.TIMER_COMMENT_ORIGIN_TYPE)
                                + "&" + COMConstants.START_ORIGIN + "=" + request.getParameter(COMConstants.START_ORIGIN)
                                + "&" + COMConstants.SEC_TOKEN + "=" + request.getParameter(COMConstants.SEC_TOKEN)
                                + "&" + COMConstants.CONTEXT + "=" +request.getParameter(COMConstants.CONTEXT);
                
                 actionForward = new ActionForward(path, false);            
        }
        else if(fromPage.equals(FROM_PAGE_CHARGE_BACK))
        {
                 actionForward = mapping.findForward("BillingTransactions");
                 String path = actionForward.getPath() 
                                + "?" + COMConstants.ORDER_GUID + "=" + request.getParameter(COMConstants.ORDER_GUID)
                                + "&" + COMConstants.MASTER_ORDER_NUMBER + "=" + request.getParameter(COMConstants.MASTER_ORDER_NUMBER)
                                + "&" + "call_dnis=" + request.getParameter("call_dnis")
                                + "&" + "call_brand_name=" + request.getParameter("call_brand_name")                                
                                + "&" + "call_cs_number=" + request.getParameter("call_cs_number")
                                + "&" + COMConstants.TIMER_CALL_LOG_ID + "=" + request.getParameter(COMConstants.TIMER_CALL_LOG_ID)
                                + "&" + COMConstants.CUSTOMER_ID + "=" + request.getParameter(REQUEST_PARAM_CUST_ID)
                                + "&" + COMConstants.TIMER_ENTITY_HISTORY_ID + "=" + request.getParameter(COMConstants.TIMER_ENTITY_HISTORY_ID)
                                + "&" + COMConstants.TIMER_COMMENT_ORIGIN_TYPE + "=" + request.getParameter(COMConstants.TIMER_COMMENT_ORIGIN_TYPE)
                                + "&" + COMConstants.START_ORIGIN + "=" + request.getParameter(COMConstants.START_ORIGIN)
                                + "&" + COMConstants.SEC_TOKEN + "=" + request.getParameter(COMConstants.SEC_TOKEN)
                                + "&" + COMConstants.CONTEXT + "=" +request.getParameter(COMConstants.CONTEXT);
                
                 actionForward = new ActionForward(path,true);         
        }
        else
        {
            throw new Exception("Unknown from page: " + request.getParameter(REQUEST_PARAM_FROM_PAGE));
        }
        
        return actionForward; 

    }
            
  
            
            
            
    /**
     * This method loads the page up with the initial data.
     * 
     * @param conn
     * @param request
     * @param response
     * @param mapping
     * @throws java.lang.Exception
     */
    private void processLoadCustomer(Connection conn,HttpServletRequest request, HttpServletResponse response,ActionMapping mapping,Document lockResult,String errorMsg)
        throws Exception
    {
    
     // Document xmlDoc = (Document) DOMUtil.getDocument(); 
      String xslFile = "customerHold.xsl";      

      //Get the customer id from request
      String customerId = request.getParameter(REQUEST_PARAM_CUST_ID);
      String orderDetailId = request.getParameter(COMConstants.ORDER_DETAIL_ID);
      String securityToken = request.getParameter(REQUEST_PARAM_SECURITYTOKEN); 
      String orderGuid = request.getParameter(REQUEST_PARAM_ORDER_GUID); 
      String masterOrderNumber = request.getParameter(REQUEST_PARAM_MASTER_ORDER_NUMBER); 
      String external_order_number = request.getParameter(REQUEST_PARAM_EXTERNAL_ORDER_NUMBER); 
      String recipSearchFlag = request.getParameter("recipient_search");      

      //daos
      CustomerDAO customerDAO = new CustomerDAO(conn);
      OrderDAO orderDAO = new OrderDAO(conn);
     
      OrderVO orderVO = orderDAO.getOrder(orderGuid);     
     
      //check if user has access to update hold info
      SecurityManager securityManager = SecurityManager.getInstance();
      String updateAllowed = null;
      String context = (String) request.getParameter(COMConstants.CONTEXT);
      String token = (String) request.getParameter(COMConstants.SEC_TOKEN);

      if (securityManager.assertPermission(context, token, "CustomerHold", COMConstants.UPDATE))
      {
          updateAllowed = "Y";
      }
      else
      {
          updateAllowed = "N";
      }
     
      //if coming from account page get all holds associated with the customer
      String fromPage = request.getParameter(REQUEST_PARAM_FROM_PAGE);
      Document xmlDoc = null;
     if(fromPage == null)
     {
         throw new Exception("Page attribute " + REQUEST_PARAM_FROM_PAGE + " is null.");
     }
      if(fromPage.equalsIgnoreCase(FROM_PAGE_CUSTOMER_ACCOUNT) || fromPage.equalsIgnoreCase(FROM_PAGE_CHARGE_BACK))
      {          
          xmlDoc = (Document)customerDAO.getCustomerHoldInfo(customerId);
      }
      else if(fromPage.equalsIgnoreCase(FROM_PAGE_CART))
      {
          //get an order detail id that is related to that guid
          CachedResultSet detailIds = orderDAO.viewDetailIds(orderGuid);
          String detailId = null;
          if(detailIds == null)
          {
              throw new Exception("Cannot find order details for guid " + orderGuid);
          }
          if (detailIds.next())
          {
              detailId = detailIds.getObject(1).toString();          
          }
      
          xmlDoc = (Document)customerDAO.getCustomerHoldInfo(detailId,CUSTOMER_TYPE_BUYER);
      }
      else if(fromPage.equalsIgnoreCase(FROM_PAGE_ORDER_DETAIL))
      {
      
          xmlDoc = (Document)customerDAO.getCustomerHoldInfo(orderDetailId,CUSTOMER_TYPE_RECIPIENT);
      }
      else if(fromPage.equalsIgnoreCase(FROM_PAGE_LOSS))
      {      
          //get an order detail id that is related to that guid
          CachedResultSet detailIds = orderDAO.viewDetailIds(orderGuid);
          String detailId = null;
          if(detailIds == null)
          {
              throw new Exception("Cannot find order details for guid " + orderGuid);
          }
          if (detailIds.next())
          {
              detailId = detailIds.getObject(1).toString();          
          }
      
          xmlDoc = (Document)customerDAO.getCustomerHoldInfo(detailId,CUSTOMER_TYPE_BUYER);
      }      
      else
      {
          throw new Exception("Invalid value in 'page_from' parameter.:" + fromPage);
      }
      

      
     //create page data document
      Document pageDataDoc = DOMUtil.getDocumentBuilder().newDocument();
      Element pageDataXML = (Element) pageDataDoc.createElement("pagedata");
      pageDataDoc.appendChild(pageDataXML);      

      //get security info
      securityManager = SecurityManager.getInstance();
      UserInfo userInfo = securityManager.getUserInfo(request.getParameter("securitytoken"));

      //dnis info
      DnisDAO dnisDAO = new DnisDAO(conn);
      String dnisId = request.getParameter(REQUEST_PARAM_DNIS_ID);


      //page data
      addElement((Document)pageDataDoc,"holdreason",getHoldReason(xmlDoc));
      addElement((Document)pageDataDoc,REQUEST_PARAM_FROM_PAGE,request.getParameter(REQUEST_PARAM_FROM_PAGE));
      addElement((Document)pageDataDoc,REQUEST_PARAM_CUST_ID,request.getParameter(REQUEST_PARAM_CUST_ID));
      addElement((Document)pageDataDoc,REQUEST_PARAM_DETAIL_ID,request.getParameter(REQUEST_PARAM_DETAIL_ID));
      addElement((Document)pageDataDoc,REQUEST_PARAM_ORDER_GUID,request.getParameter(REQUEST_PARAM_ORDER_GUID));
      addElement((Document)pageDataDoc,REQUEST_PARAM_MASTER_ORDER_NUMBER,masterOrderNumber);      
      addElement((Document)pageDataDoc,REQUEST_PARAM_USER_ID,userInfo.getUserID());                  
      addElement((Document)pageDataDoc,COMConstants.ORDER_DETAIL_ID,orderDetailId);                  
      addElement((Document)pageDataDoc,REQUEST_PARAM_EXTERNAL_ORDER_NUMBER,external_order_number);                        
      addElement((Document)pageDataDoc,"updateAllowed",updateAllowed);
      addElement((Document)pageDataDoc,"recipient_search",recipSearchFlag);
      addElement((Document)pageDataDoc,"error_message",errorMsg);
      
      if(orderVO == null || orderVO.getLossPrevention() == null)
      {
          addElement((Document)pageDataDoc,"loss_prevention_ind","");                    
      }
      else
      {
          addElement((Document)pageDataDoc,"loss_prevention_ind",orderVO.getLossPrevention());          
      }


      DOMUtil.addSection(xmlDoc,lockResult.getChildNodes());
      DOMUtil.addSection(xmlDoc,pageDataDoc.getChildNodes());
            
      //add hold reason
      DOMUtil.addSection(xmlDoc,customerDAO.getHoldReasonsXML().getChildNodes());
      
      // JP Puzon 01-21-2006
      // The line below has been commented out for PCI compliance
      // since the xml document contains credit card numbers.
      //logger.debug(convertDocToString(xmlDoc));
      File xslFileFile = getXSL(xslFile,mapping);
      ActionForward forward = mapping.findForward("CustomerHoldXSL");
      String xslFilePathAndName = forward.getPath(); //get real file name
      
      // Change to client side transform
      TraxUtil.getInstance().getInstance().transform(request, response, xmlDoc, 
                          xslFileFile, xslFilePathAndName, (HashMap)request.getAttribute(COMConstants.CONS_APP_PARAMETERS));
      

    }
    

    
  /**
     * This method is invoked when teh user clicks save on the page.  It causes
     * the checked holds to be inserted and unchecked holds to be deleted.
     * 
     * @param conn
     * @param request
     * @param response
     * @param mapping
     * @throws java.lang.Exception
     */
  private void processSaveCustomer(Connection conn,HttpServletRequest request, HttpServletResponse response,ActionMapping mapping)
    throws Exception
  {
        int counter = 1;
        int change = 0;
  
        String origHoldType = null; 
        String holdChkBox = null;
        boolean holdTypeChange = false;
        boolean entityTypeInserted = false;
        
        //history of what happened
        List insertHoldList = new ArrayList();
        List updateHoldList = new ArrayList();
        List deleteHoldList = new ArrayList();
        
        CustomerHoldVO holdVO = new CustomerHoldVO();
  
        //get original hold type
        String origOverallHoldType = request.getParameter("orig_hold_reason");

        //get customer id
        String customerId = request.getParameter("cust_num_value");
       
        //get new hold type        
        holdVO.setHoldReasonCode(request.getParameter("holdreason"));;
        
        //check if the hold type changed
        if(!origOverallHoldType.equals(holdVO.getHoldReasonCode()))
        {
            holdTypeChange = true;
        }
        
        //get user
        SecurityManager securityManager = SecurityManager.getInstance();
        UserInfo userInfo = securityManager.getUserInfo(request.getParameter("securitytoken"));        
        
        //set the udpated by value in the hold vo
        holdVO.setUpdatedBy(userInfo.getUserID());
        
        //check customer id        
        origHoldType = request.getParameter("cust_num_hold_value");        
        holdVO.setHoldValue1(request.getParameter("cust_num_value"));        
        holdChkBox = request.getParameter("cust_num_hold_chk");
        holdVO.setEntityId(request.getParameter("cust_num_hold_id"));
        holdVO.setEntityType("CUSTOMER_ID");        
        change = checkForChange(origHoldType,holdVO.getHoldReasonCode(),holdChkBox);
        performChange(conn,change,holdVO,insertHoldList, updateHoldList, deleteHoldList);
        
        //check customer name        
        origHoldType = request.getParameter("cust_name_hold_value");        
        holdVO.setHoldValue1(request.getParameter("cust_name_value"));        
        holdChkBox = request.getParameter("cust_name_hold_chk");
        holdVO.setEntityId(request.getParameter("cust_name_hold_id"));
        holdVO.setEntityType("NAME");
        change = checkForChange(origHoldType,holdVO.getHoldReasonCode(),holdChkBox);
        performChange(conn,change,holdVO,insertHoldList, updateHoldList, deleteHoldList);

        //check customer address        
        origHoldType = request.getParameter("cust_addr_hold_value");        
        holdVO.setHoldValue1(request.getParameter("cust_addr_value"));        
        holdChkBox = request.getParameter("cust_addr_hold_chk");
        holdVO.setEntityId(request.getParameter("cust_addr_hold_id"));
        holdVO.setEntityType("ADDRESS");
        change = checkForChange(origHoldType,holdVO.getHoldReasonCode(),holdChkBox);
        performChange(conn,change,holdVO,insertHoldList, updateHoldList, deleteHoldList);
        
        //check credit card
        counter = 1;
        holdVO.setHoldValue1(request.getParameter("cust_card_value" + counter));
        entityTypeInserted = false;
        while(holdVO.getHoldValue1() != null && holdVO.getHoldValue1().length() > 0)
        {
            origHoldType = request.getParameter("cust_card_hold_value" + counter);        
            holdChkBox = request.getParameter("cust_card_hold_chk" + counter);
            holdVO.setEntityId(request.getParameter("cust_card_hold_id" + counter));
            holdVO.setEntityType("CC_NUMBER");
            change = checkForChange(origHoldType,holdVO.getHoldReasonCode(),holdChkBox);
            
            //only update the list if the entity type was not inserted
            if(entityTypeInserted)
            {
                performChange(conn,change,holdVO);                
            }
            else
            {
                entityTypeInserted = true;
                performChange(conn,change,holdVO,insertHoldList, updateHoldList, deleteHoldList);                
            }
            
            counter++;
            holdVO.setHoldValue1(request.getParameter("cust_card_value" + counter));        
        }

        //check Alternate Payment account
        counter = 1;
        holdVO.setHoldValue1(request.getParameter("ap_account_value" + counter));
        entityTypeInserted = false;
        while(holdVO.getHoldValue1() != null && holdVO.getHoldValue1().length() > 0)
        {
            origHoldType = request.getParameter("ap_account_hold_value" + counter);        
            holdChkBox = request.getParameter("ap_account_hold_chk" + counter);
            holdVO.setEntityId(request.getParameter("ap_account_hold_id" + counter));
            holdVO.setEntityType("AP_ACCOUNT");
            change = checkForChange(origHoldType,holdVO.getHoldReasonCode(),holdChkBox);
            
            //only update the list if the entity type was not inserted
            if(entityTypeInserted)
            {
                performChange(conn,change,holdVO);                
            }
            else
            {
                entityTypeInserted = true;
                performChange(conn,change,holdVO,insertHoldList, updateHoldList, deleteHoldList);                
            }
          
            counter++;
            holdVO.setHoldValue1(request.getParameter("ap_account_value" + counter));        
        }

        //check phone number
        counter = 1;
        holdVO.setHoldValue1(request.getParameter("cust_phone_value" + counter));
        entityTypeInserted = false;        
        while(holdVO.getHoldValue1() != null && holdVO.getHoldValue1().length() > 0)
        {
            origHoldType = request.getParameter("cust_phone_hold_value" + counter);        
            holdChkBox = request.getParameter("cust_phone_hold_chk" + counter);
            holdVO.setEntityId(request.getParameter("cust_phone_hold_id" + counter));
            holdVO.setEntityType("PHONE_NUMBER");
            change = checkForChange(origHoldType,holdVO.getHoldReasonCode(),holdChkBox);

            //only update the list if the entity type was not inserted
            if(entityTypeInserted)
            {
                performChange(conn,change,holdVO);                
            }
            else
            {
                entityTypeInserted = true;
                performChange(conn,change,holdVO,insertHoldList, updateHoldList, deleteHoldList);                
            }
            
            counter++;
            holdVO.setHoldValue1(request.getParameter("cust_phone_value" + counter));        
        }

        //check member number
        counter = 1;
        holdVO.setHoldValue1(request.getParameter("cust_member_value" + counter));
        entityTypeInserted = false;        
        while(holdVO.getHoldValue1() != null && holdVO.getHoldValue1().length() > 0)
        {
            origHoldType = request.getParameter("cust_member_hold_value" + counter);        
            holdChkBox = request.getParameter("cust_member_hold_chk" + counter);
            holdVO.setEntityId(request.getParameter("cust_member_hold_id" + counter));    
            holdVO.setEntityType("MEMBERSHIP_ID");
            change = checkForChange(origHoldType,holdVO.getHoldReasonCode(),holdChkBox);

            //only update the list if the entity type was not inserted
            if(entityTypeInserted)
            {
                performChange(conn,change,holdVO);                
            }
            else
            {
                entityTypeInserted = true;
                performChange(conn,change,holdVO,insertHoldList, updateHoldList, deleteHoldList);                
            }
            
            counter++;
            holdVO.setHoldValue1(request.getParameter("cust_member_value" + counter));        
        }
        
        //check email
        counter = 1;
        holdVO.setHoldValue1(request.getParameter("cust_email_value" + counter));
        entityTypeInserted = false;        
        while(holdVO.getHoldValue1() != null && holdVO.getHoldValue1().length() > 0)
        {
            origHoldType = request.getParameter("cust_email_hold_value" + counter);        
            holdChkBox = request.getParameter("cust_email_hold_chk" + counter);
            holdVO.setEntityId(request.getParameter("cust_email_hold_id" + counter));
            holdVO.setEntityType("EMAIL_ADDRESS");
            change = checkForChange(origHoldType,holdVO.getHoldReasonCode(),holdChkBox);

            //only update the list if the entity type was not inserted
            if(entityTypeInserted)
            {
                performChange(conn,change,holdVO);                
            }
            else
            {
                entityTypeInserted = true;
                performChange(conn,change,holdVO,insertHoldList, updateHoldList, deleteHoldList);                
            }
            
            counter++;
            holdVO.setHoldValue1(request.getParameter("cust_email_value" + counter));        
        }        

        //check lp indicator
        counter = 1;
        holdVO.setHoldValue1(request.getParameter("cust_lp_value" + counter));
        entityTypeInserted = false;        
        while(holdVO.getHoldValue1() != null && holdVO.getHoldValue1().length() > 0)
        {
            origHoldType = request.getParameter("cust_lp_hold_value" + counter);
            holdChkBox = request.getParameter("cust_lp_hold_chk" + counter);
            holdVO.setEntityId(request.getParameter("cust_lp_hold_id" + counter));
            holdVO.setEntityType("LOSS_PREVENTION_INDICATOR");
            change = checkForChange(origHoldType,holdVO.getHoldReasonCode(),holdChkBox);

            //only update the list if the entity type was not inserted
            if(entityTypeInserted)
            {
                performChange(conn,change,holdVO);                
            }
            else
            {
                entityTypeInserted = true;
                performChange(conn,change,holdVO,insertHoldList, updateHoldList, deleteHoldList);                
            }
            
            counter++;
            holdVO.setHoldValue1(request.getParameter("cust_lp_value" + counter));        
        }        

        //get commetn origin
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();         
        String origin = configUtil.getProperty(COMConstants.PROPERTY_FILE,"CUST_HOLD_COMMENT_ORIGIN");



        //Comment object
        CommentsVO commentsVO = new CommentsVO();        
        commentsVO.setCommentOrigin(origin);
        commentsVO.setCommentType("Customer");
        commentsVO.setCustomerId(customerId);    
        commentsVO.setUpdatedBy(userInfo.getUserID());
        
        if(commentsVO.getUpdatedBy() == null || commentsVO.getUpdatedBy().length() == 0)
        {
            commentsVO.setUpdatedBy("empty");
        }
        
        //dao to insert comments
        CommentDAO commentDAO = new CommentDAO(conn);
        
        //add comments to customer for inserted holds
        if(insertHoldList != null && insertHoldList.size() > 0)
        {
           String msg = "Customer field(s) " +convertToString(insertHoldList )+" placed on hold status for " + holdVO.getHoldReasonCode() + " by " + userInfo.getUserID(); 
           commentsVO.setComment(msg);
           commentDAO.insertComment(commentsVO);
        }

        //add comments to customer for deleted holds
        if(deleteHoldList != null && deleteHoldList.size() > 0)
        {
           String msg = "Customer field(s) " +convertToString(deleteHoldList )+" removed from hold status for " + holdVO.getHoldReasonCode() + " by " + userInfo.getUserID(); 
           commentsVO.setComment(msg);
           commentDAO.insertComment(commentsVO);
        }

        //add comments to customer for updated holds
        if(updateHoldList != null && updateHoldList.size() > 0)
        {
           String msg = "Customer field(s) " +convertToString(updateHoldList )+" hold status type changed to " + holdVO.getHoldReasonCode() + " by " + userInfo.getUserID(); 
           commentsVO.setComment(msg);
           commentDAO.insertComment(commentsVO);
        }        
        
        


        
  }


  private String processRelease(Connection conn,HttpServletRequest request, HttpServletResponse response,ActionMapping mapping,String userId)
      throws Exception
  {


     String fromPage = request.getParameter(REQUEST_PARAM_FROM_PAGE);
     String customerId = request.getParameter(REQUEST_PARAM_CUST_ID);
     String orderDetailId = request.getParameter(REQUEST_PARAM_DETAIL_ID);
     String orderGuid = request.getParameter(REQUEST_PARAM_ORDER_GUID);
     
     String securityToken = request.getParameter("securitytoken");
     
     String errorMessage = "";
     
     //get commetn origin
     ConfigurationUtil configUtil = ConfigurationUtil.getInstance();         
     String origin = configUtil.getProperty(COMConstants.PROPERTY_FILE,"CUST_HOLD_COMMENT_ORIGIN");     
     
     //daos
     CustomerDAO customerDAO = new CustomerDAO(conn);
     OrderDAO orderDAO = new OrderDAO(conn);
     CommentDAO commentDAO = new CommentDAO(conn);
     
     List orderList = new ArrayList();

     CommentsVO commentsVO = new CommentsVO();        
     commentsVO.setCommentOrigin(origin);
     commentsVO.setCommentType("Order");
     commentsVO.setCustomerId(customerId);    
     commentsVO.setUpdatedBy(userId);
     
     //if we are releasing all orders on the cart
     if(fromPage.equalsIgnoreCase(FROM_PAGE_CART))
     {
        //get list of orders on hold
        orderList = customerDAO.getHeldOrders(customerId,orderGuid);   
        if(orderList != null && orderList.size() > 0)
        {
            //attempt to locks orders
            if(obtainLocks(conn,securityToken,userId,orderList))
            {
                //retrieve the associated order
                OrderVO order = orderDAO.getOrder(orderGuid);       
            
                //put comment on order
                String msg = "Shopping cart "+order.getMasterOrderNumber() +" orders released for processing by " + userId +".";
                commentsVO.setComment(msg); 
                commentsVO.setOrderGuid(orderGuid);
                commentDAO.insertComment(commentsVO);
            
                String commentText = "Shopping Cart#" + order.getMasterOrderNumber();
            
                //loop through orders
                Iterator iter = orderList.iterator();
                while(iter.hasNext())                        
                {
                    String orderId = (String)iter.next();
                    
                    customerDAO.deleteHeldOrders(orderId);                    

                    releaseOrder(conn,securityToken,orderId,origin,customerId,commentText,orderGuid);
    

                }//end while loop
                
                //release locks
                this.releaseLocks(conn,securityToken,userId,orderList);  
                
            }
            else
            {
                errorMessage = "Order(s) in shopping cart are currently locked by another user or process.  Cart was not released.";
            }//end if locks obtained

        }//end if list null

     }
     else if(fromPage.equalsIgnoreCase(FROM_PAGE_ORDER_DETAIL))
     {

        // see if this order_detail record is held... only allow a release to happen if it is
        List list = orderDAO.getHold(new Long(orderDetailId).longValue());
        if(list != null && list.size() > 0) {
            //attempt to locks orders
            if(obtainLocks(conn,securityToken,userId,orderList))
            {
              releaseOrder(conn,request.getParameter("securitytoken"),orderDetailId,origin,customerId,"",orderGuid);            
              customerDAO.deleteHeldOrders(orderDetailId);    
              
              //release locks
              releaseLocks(conn,securityToken,userId,orderList);              
              
            }
            else
            {
                errorMessage = "Order is currently locked by another user or process.  Order was not released.";
            }//if lock obtained
        }//if there are orders to lock

     }
     else if(fromPage.equalsIgnoreCase(FROM_PAGE_CUSTOMER_ACCOUNT))
     {
        //get list of orders on hold
        orderList = customerDAO.getHeldOrders(customerId);   
        if(orderList != null)
        {
            //attempt to locks orders
            if(obtainLocks(conn,securityToken,userId,orderList))
            {        
                //loop through orders
                Iterator iter = orderList.iterator();
                while(iter.hasNext())
                {                    
                    String orderId = (String)iter.next();                
                    releaseOrder(conn,request.getParameter("securitytoken"),orderId,origin,customerId,"",orderGuid);                    
                    customerDAO.deleteHeldOrders(orderId);                
                }//end while loop
                
                //release locks
                this.releaseLocks(conn,securityToken,userId,orderList);                
            }
            else
            {
                errorMessage = "Order(s) belonging to the customer are currently locked by another user or process.  Orders were not released.";                
            }//end if locks obtained
            
        }//end order list null         
     }
     else
     {
         throw new Exception("Unknown value in 'from_page' parameter.");
     }
     
     return errorMessage;
     
  }
 
 
     /*
     * Release locks.
     */
    private void releaseLocks(Connection conn,String securityToken, String userId,List orderList)
        throws Exception
    {
        boolean lockObtained = true;
        
        LockDAO lockDAO = new LockDAO(conn);        

        Iterator iter = orderList.iterator();
        while(iter.hasNext())
        {
            String lockOrderDetailId = (String)iter.next();
            lockDAO.releaseLock(securityToken,userId,lockOrderDetailId,ORDER_DETAIL_LOCK_ENTITY);
        }

    }
 
 
 
    /*
     * Attept to obtain an order lock for each order in list.
     */
    private boolean obtainLocks(Connection conn,String securityToken, String userId,List orderList)
        throws Exception
    {
        boolean lockObtained = true;
        
        LockDAO lockDAO = new LockDAO(conn);
        
        //list of locks
        List cannotLockList = new ArrayList();
        List lockObtainedList = new ArrayList();

        //only continue if there is a list
        if(orderList != null)
        {
            //look throught the list of orders until a lock is not obtained, or all locks obtained
            Iterator iter = orderList.iterator();
            while(lockObtained && iter.hasNext())
            {
                String orderDetailId = (String)iter.next();
                
                //obtain lock
                Document xmlLock = lockDAO.retrieveLockXML(securityToken, userId, orderDetailId, ORDER_DETAIL_LOCK_ENTITY);
                if(DOMUtil.getNodeValue(xmlLock, OUT_LOCK_OBTAINED).equalsIgnoreCase(NO))
                {
                    lockObtained = false;

                    //create a list of orders that could not be locked
                    cannotLockList.add(orderDetailId);
                }
                else
                {
                    //create a list of orders that were locked
                    lockObtainedList.add(orderDetailId);
                }
            }//look through passed in list
            
            //if any lock could not be obtained, then release all the obtained locks
            if(!lockObtained)
            {
                Iterator lockIter = lockObtainedList.iterator();
                while(lockIter.hasNext())
                {
                    String lockOrderDetailId = (String)iter.next();
                    lockDAO.releaseLock(securityToken,userId,lockOrderDetailId,ORDER_DETAIL_LOCK_ENTITY);
                }
            }
            
        }//order list null
        
        return lockObtained;
    }

    private void processCancel(Connection conn,HttpServletRequest request, HttpServletResponse response,ActionMapping mapping,String userId)
      throws Exception
  {

     String fromPage = request.getParameter(REQUEST_PARAM_FROM_PAGE);
     String customerId = request.getParameter(REQUEST_PARAM_CUST_ID);
     String orderDetailId = request.getParameter(REQUEST_PARAM_DETAIL_ID);
     String orderGuid = request.getParameter(REQUEST_PARAM_ORDER_GUID);
     
    
     ConfigurationUtil configUtil = ConfigurationUtil.getInstance();         
     String refundDisposition = configUtil.getProperty(COMConstants.PROPERTY_FILE,"CUSTOMER_HOLD_REFUND_DISPOSITION");     
     String responsibleParty = configUtil.getProperty(COMConstants.PROPERTY_FILE,"CUSTOMER_HOLD_RESPONSIBLE_PARTY");     
     String commentOrigin = configUtil.getProperty(COMConstants.PROPERTY_FILE,"CUST_HOLD_COMMENT_ORIGIN");
    
     //get user
     SecurityManager securityManager = SecurityManager.getInstance();
     UserInfo userInfo = securityManager.getUserInfo(request.getParameter("securitytoken"));
     
     CustomerDAO customerDAO = new CustomerDAO(conn);
     OrderDAO orderDAO = new OrderDAO(conn);
     CommentDAO commentDAO = new CommentDAO(conn);
     UpdateOrderDAO updateOrderDAO = new UpdateOrderDAO(conn);

     
     List orderList = new ArrayList();
     
     CommentsVO commentsVO = new CommentsVO();        
     commentsVO.setCommentOrigin(commentOrigin);
     commentsVO.setCommentType("Order");
     commentsVO.setCustomerId(customerId);    
     commentsVO.setUpdatedBy(userId);     
     
     //if we are releasing all orders on the cart
     if(fromPage == null)
     {
         throw new Exception("Page attribute " + REQUEST_PARAM_FROM_PAGE + " is null.");
     }
     if(fromPage.equalsIgnoreCase(FROM_PAGE_CART))
     {
        //get the master order number for this guid.
        OrderVO orderVO = orderDAO.getOrder(orderGuid);
        
        //put comment on order
        String msg = "Shopping cart "+orderVO.getMasterOrderNumber() +" cancelled by " + userId +".";
        commentsVO.setComment(msg); 
        commentsVO.setOrderGuid(orderGuid);
        commentDAO.insertComment(commentsVO);        
        
        String commentText = "Shopping Cart#" + orderVO.getMasterOrderNumber();
        
        //get list of orders on hold
        orderList = customerDAO.getHeldOrders(customerId,orderGuid);   
        if(orderList != null && orderList.size() > 0)
        {
            //loop through orders
            Iterator iter = orderList.iterator();
            while(iter.hasNext())
            {
           
                String orderId = (String)iter.next();
                cancelOrder(conn,request.getParameter("securitytoken"),orderId, commentOrigin,customerId,commentText,orderGuid);

                // Defect 3737: delete from order hold regardless of hold reason.
                updateOrderDAO.deleteOrderHold(orderId);
            }
            
            //trigger a refunds
            RefundBO refundBO = new RefundBO(request, conn);
            refundBO.postFullRefund(orderList,refundDisposition,responsibleParty,COMConstants.REFUND_STATUS_UNBILLED);            

            // have to do this after the refund, because we're marking the refund as billed //
            iter = orderList.iterator();
            while(iter.hasNext())
            {
               String orderId = (String)iter.next();
               /* Sets related fields after cancelling a held order
                * 1. clean.refund.refund_status = 'Billed',  * 2. clean.order_bills.bill_status = 'Billed'
               * 3. clean.order_details.eod_delivery_indicator  = 'X',  4. clean.order_details.order_disp_code = 'Processed' */
              orderDAO.postCancelHeldOrder(orderId, userId);
            }
        }
     }
     else if(fromPage.equalsIgnoreCase(FROM_PAGE_ORDER_DETAIL))
     {
     
         cancelOrder(conn,request.getParameter("securitytoken"),orderDetailId,commentOrigin,customerId,"",orderGuid);

         // Defect 3737: delete from order hold regardless of hold reason.
         updateOrderDAO.deleteOrderHold(orderDetailId);
         
        //trigger a refunds
        RefundBO refundBO = new RefundBO(request, conn);
        orderList.add(orderDetailId);
        refundBO.postFullRefund(orderList,refundDisposition,responsibleParty,COMConstants.REFUND_STATUS_UNBILLED);                   
        /* Sets related fields after cancelling a held order
        * 1. clean.refund.refund_status = 'Billed',  * 2. clean.order_bills.bill_status = 'Billed'
        * 3. clean.order_details.eod_delivery_indicator  = 'X',  4. clean.order_details.order_disp_code = 'Processed' */
        orderDAO.postCancelHeldOrder(orderDetailId, userId);

     }
     else if(fromPage.equalsIgnoreCase(FROM_PAGE_CUSTOMER_ACCOUNT))
     {
      
        //get list of orders on hold
        orderList = customerDAO.getHeldOrders(customerId);   
        if(orderList != null)
        {
            //loop through orders
            Iterator iter = orderList.iterator();
            while(iter.hasNext())
            {
            
                String orderId = (String)iter.next();
                cancelOrder(conn,request.getParameter("securitytoken"),orderId,commentOrigin,customerId,"",orderGuid);
                // Defect 3737: delete from order hold regardless of hold reason.
                updateOrderDAO.deleteOrderHold(orderId);
            }
            //trigger a refunds
            RefundBO refundBO = new RefundBO(request, conn);
            refundBO.postFullRefund(orderList,refundDisposition,responsibleParty,COMConstants.REFUND_STATUS_UNBILLED);

            // have to do this after the refund, because we're marking the refund as billed //
            iter = orderList.iterator();
            while(iter.hasNext())
            {
               String orderId = (String)iter.next();
              /* Sets related fields after cancelling a held order
              * 1. clean.refund.refund_status = 'Billed',  * 2. clean.order_bills.bill_status = 'Billed'
              * 3. clean.order_details.eod_delivery_indicator  = 'X',  4. clean.order_details.order_disp_code = 'Processed' */
              orderDAO.postCancelHeldOrder(orderId, userId);
            }

        }     
     }
     else
     {
         throw new Exception("Unknown value in 'from_page' parameter.");
     }
     
  }

  private void cancelOrder(Connection conn,String securitytoken, String orderDetailId, String commentOrigin,String customerId,String commentText,String orderGuid)
    throws Exception
  {
        //get user
        SecurityManager securityManager = SecurityManager.getInstance();
        UserInfo userInfo = securityManager.getUserInfo(securitytoken);

        /* retrieve user information */
        String userId = EMPTY_USER_ID;
        if(SecurityManager.getInstance().getUserInfo(securitytoken)!=null)
        {
            userId = SecurityManager.getInstance().getUserInfo(securitytoken).getUserID();
        }

        LossPreventionDAO lossDAO = new LossPreventionDAO(conn);    
        lossDAO.deleteFromQueue(orderDetailId,LOSS_PREVENTION_QUEUE,userId);
        //defect 3737: also delete from CREDIT queue
        lossDAO.deleteFromQueue(orderDetailId,CREDIT_QUEUE,userInfo.getUserID());

        OrderDAO orderDAO = new OrderDAO(conn);
     
        //Comment object
        CommentDAO commentDAO = new CommentDAO(conn);
        CommentsVO commentsVO = new CommentsVO();        
        commentsVO.setCommentOrigin(commentOrigin);
        commentsVO.setCommentType("Order");
        commentsVO.setCustomerId(customerId); 
        commentsVO.setOrderGuid(orderGuid);
        commentsVO.setOrderDetailId(orderDetailId);
        commentsVO.setUpdatedBy(userInfo.getUserID());  
        commentsVO.setComment("Order cancelled by " + userInfo.getUserID() + " from Hold Maintenance page.  " + commentText);
        commentDAO.insertComment(commentsVO);
        
     
  }


  private void releaseOrder(Connection conn,String securitytoken,String orderDetailId, String commentOrigin, String customerId,String commentText,String orderGuid)
    throws Exception
  {
  
    //get flag to determine if JMS should be sent out.  (used while testing)
    ConfigurationUtil configUtil = ConfigurationUtil.getInstance();         
    String sendJMS = configUtil.getProperty(COMConstants.PROPERTY_FILE,"SEND_JMS_TO_OP");  
  
    //send  out notification to order processing
    if(sendJMS == null || !sendJMS.equals("N")){    
    
        MessageToken messageToken = new MessageToken();
        messageToken.setMessage("RELEASE|" + orderDetailId);          
        
        logger.debug("Releasing order detail id:" + orderDetailId);
        messageToken.setProperty("JMS_OracleDelay", "30", "int");
        messageToken.setStatus(ORDER_PROCESSING_STATUS);
        Dispatcher dispatcher = Dispatcher.getInstance();       
        dispatcher.dispatchTextMessage(new InitialContext(),messageToken);  
    }      
    else
    {
        logger.info("Running in test mode.  Order detail id:" + orderDetailId + " will not be released.");
    }

        //get user
        SecurityManager securityManager = SecurityManager.getInstance();
        UserInfo userInfo = securityManager.getUserInfo(securitytoken);

        /* retrieve user information */
        String userId = EMPTY_USER_ID;
        if(SecurityManager.getInstance().getUserInfo(securitytoken)!=null)
        {
            userId = SecurityManager.getInstance().getUserInfo(securitytoken).getUserID();
        }

        //remove from LP queue    
        LossPreventionDAO lossDAO = new LossPreventionDAO(conn);    
        lossDAO.deleteFromQueue(orderDetailId,LOSS_PREVENTION_QUEUE,userId);
    
        //update order disposition
        OrderDAO orderDAO = new OrderDAO(conn);
        orderDAO.updateOrderDisposition(Long.parseLong(orderDetailId),COMConstants.ORDER_DISPOSITION_VALIDATED,userId);           
    
        //Comment object
        CommentDAO commentDAO = new CommentDAO(conn);
        CommentsVO commentsVO = new CommentsVO();        
        commentsVO.setCommentOrigin(commentOrigin);
        commentsVO.setCommentType("Order");
        commentsVO.setCustomerId(customerId); 
        commentsVO.setOrderDetailId(orderDetailId);
        commentsVO.setOrderGuid(orderGuid);
        commentsVO.setUpdatedBy(userInfo.getUserID());  
        commentsVO.setComment("Order released by " + userInfo.getUserID() + " from Hold Maintenance page.  " + commentText);
        commentDAO.insertComment(commentsVO);    
    
  }


  private String convertToString(List list)
  {
        if(list == null)
        {
            return null;
        }
        
        Iterator iter = list.iterator();
        String msg = "";
        while(iter.hasNext())
        {
            if(msg.length() > 0)
            {
                msg = msg + ",";                
            }
            msg = msg + (String)iter.next();
        }
        
        return msg;
  }

  /*
     * 
     * @param conn = Database connection
     * @param origHoldType = The current hold type for the item (or null if not on hold)
     * @param holdChkBox = The value of the hold checkbox
     * @param holdType = The hold type to be used on any holds that are created
     * @param holdTypeChange = 
     * @param value = The hold value
     * @param existingHoldId = the id of the existing hold record..if one exists
     * @throws java.lang.Exception
     */
  private int checkForChange(  String origHoldType,                                 
                                String currentholdType,
                                String holdChkBox)
    throws Exception
  {

      
      //Was this item on hold before?
      if(origHoldType == null || origHoldType.length() == 0)
      {
          //It wasn't on hold, check if should now be put on hold
          if(checked(holdChkBox))
          {
              //insert the hold
              return INSERT_HOLD;
          }
          else
          {
              //Do nothing. Item was not hold befoe and will still not be on hold.
              return DO_NOTHING;
          }
      }
      //else this was on hold before, should we keep it on hold?
      else if(checked(holdChkBox))
      {
          //Keep item on hold, check if the hold type should be changed
          if(origHoldType.equals(currentholdType))
          {
              //Do nothing.  The item will stay on hold with the same hold type.
              return DO_NOTHING;
          }
          else
          {
              //hold type was changed, update record
              return UPDATE_HOLD;
          }
      }else
      {
          //Take the item off hold
          return DELETE_HOLD;
      }
      

  }


  private void performChange(Connection conn,
                                int change,
                                CustomerHoldVO holdVO,
                                List insertHoldList,
                                List updateHoldList,
                                List deleteHoldList)
    throws Exception
  {
        performChange(conn,change,holdVO,insertHoldList,updateHoldList,deleteHoldList,true);
  }


  private void performChange(Connection conn,
                                int change,
                                CustomerHoldVO holdVO)
    throws Exception
  {
        performChange(conn,change,holdVO,null,null,null,false);
  }

 
  private void performChange(Connection conn,
                                int change,
                                CustomerHoldVO holdVO,
                                List insertHoldList,
                                List updateHoldList,
                                List deleteHoldList,
                                boolean insertIntoList)
    throws Exception
  {


      //Create dao for saving the changes
      CustomerDAO customerDAO = new CustomerDAO(conn);     
      
     
      //Was this item on hold before?
      if(change == DO_NOTHING)
      {
          // JP Puzon 01-21-2006
          // The line below has been commented out for PCI compliance
          // since the hold value can be a credit card number.
          //logger.debug("Do nothing. Value=" + holdVO.getHoldValue1() + ", Type=" + holdVO.getEntityType());      
      }
      else if(change == INSERT_HOLD)
      {
          logger.debug("Insert Hold. Value=" + holdVO.getHoldValue1() + ", Type=" + holdVO.getEntityType()); 
          customerDAO.insertCustomerHold(holdVO);          
          
          if(insertIntoList)
          {
              insertHoldList.add(holdVO.getEntityType());              
          }
      }
      else if(change == UPDATE_HOLD)
      {
          logger.debug("Update hold type. Value=" + holdVO.getHoldValue1() + ", Type=" + holdVO.getEntityType());
          //dao.updateCustomerHold(holdVO);    
          customerDAO.deleteCustomerHold(holdVO);
          customerDAO.insertCustomerHold(holdVO);
          
          if(insertIntoList)
          {
              updateHoldList.add(holdVO.getEntityType());
          }          
      }
      else if(change == DELETE_HOLD)
      {
          logger.debug("Delete hold type. Value=" + holdVO.getHoldValue1() + ", Type=" + holdVO.getEntityType());
          customerDAO.deleteCustomerHold(holdVO);    
          
          if(insertIntoList)
          {
              deleteHoldList.add(holdVO.getEntityType());
          }                    
      }
      else
      {
          throw new Exception("Invalud change type: " + change);
      }

  }
  
  private boolean checked(String value)
  {
      return (value !=null && value.equalsIgnoreCase("on")) ? true : false;
  }
  
    
  private String getHoldReason(Document xmlDoc) throws Exception
  {
        String xpath = "root/customers/customer/id_reason";
        String holdValue = null;
        NodeList nl = DOMUtil.selectNodes(xmlDoc,xpath);
        if(nl.getLength() > 0){
            for (int i = 0; i < nl.getLength(); i++)
            {
                Text node = (Text)nl.item(i).getFirstChild();
                if(node != null)
                {
                    holdValue = node.getNodeValue();                    
                }                
                
                if(holdValue != null && holdValue.length() >0)
                {
                    return holdValue;
                }
            }
        }

        
        if(holdValue == null || holdValue.length() == 0)
        {
            xpath = "root/customers/customer/name_reason";
            nl = DOMUtil.selectNodes(xmlDoc,xpath);
            if(nl.getLength() > 0){
                for (int i = 0; i < nl.getLength(); i++)
                {
                    Text node = (Text)nl.item(i).getFirstChild();
                    if(node != null)
                    {
                        holdValue = node.getNodeValue();                    
                    }               
                
                    if(holdValue != null && holdValue.length() >0)
                    {
                        return holdValue;
                    }                
                }
            }            
        }

        if(holdValue == null || holdValue.length() == 0)
        {
            xpath = "root/customers/customer/address_reason";
            nl = DOMUtil.selectNodes(xmlDoc,xpath);
            if(nl.getLength() > 0){
                for (int i = 0; i < nl.getLength(); i++)
                {
                    Text node = (Text)nl.item(i).getFirstChild();
                    if(node != null)
                    {
                        holdValue = node.getNodeValue();                    
                    }               
                    
                    if(holdValue != null && holdValue.length() >0)
                    {
                       return holdValue;
                    }                                    
                }
            }            
        }

        if(holdValue == null || holdValue.length() == 0)
        {
            xpath = "//cc_reason";
            nl = DOMUtil.selectNodes(xmlDoc,xpath);
            if(nl.getLength() > 0){
                for (int i = 0; i < nl.getLength(); i++)
                {
                    Text node = (Text)nl.item(i).getFirstChild();
                    if(node != null)
                    {
                        holdValue = node.getNodeValue();                    
                    }               
                    
                    if(holdValue != null && holdValue.length() >0)
                    {
                        return holdValue;
                    }                                    
                }
            }            
        }
        
        if(holdValue == null || holdValue.length() == 0)
        {
            xpath = "//phone_reason";
            nl = DOMUtil.selectNodes(xmlDoc,xpath);
            if(nl.getLength() > 0){
                for (int i = 0; i < nl.getLength(); i++)
                {
                    Text node = (Text)nl.item(i).getFirstChild();
                    if(node != null)
                    {
                        holdValue = node.getNodeValue();                    
                    }               
                    
                    if(holdValue != null && holdValue.length() >0)
                    {
                        return holdValue;
                    }                                    
                }
            }            
        }
        
        if(holdValue == null || holdValue.length() == 0)
        {
            xpath = "//email_reason";
            nl = DOMUtil.selectNodes(xmlDoc,xpath);
            if(nl.getLength() > 0){
                for (int i = 0; i < nl.getLength(); i++)
                {
                    Text node = (Text)nl.item(i).getFirstChild();
                    if(node != null)
                    {
                        holdValue = node.getNodeValue();                    
                    }               
                    
                    if(holdValue != null && holdValue.length() >0)
                    {
                        return holdValue;
                    }                                    
                }
            }            
        }
        
        if(holdValue == null || holdValue.length() == 0)
        {
            xpath = "//loss_prevention_reason";
            nl = DOMUtil.selectNodes(xmlDoc,xpath);
            if(nl.getLength() > 0){
                for (int i = 0; i < nl.getLength(); i++)
                {
                    Text node = (Text)nl.item(i).getFirstChild();
                    if(node != null)
                    {
                        holdValue = node.getNodeValue();                    
                    }               
                    
                    if(holdValue != null && holdValue.length() >0)
                    {
                        return holdValue;
                    }                                    
                }
            }            
        }
        
        if(holdValue == null || holdValue.length() == 0)
        {
            xpath = "//membership_reason";
            nl = DOMUtil.selectNodes(xmlDoc,xpath);
            if(nl.getLength() > 0){
                for (int i = 0; i < nl.getLength(); i++)
                {
                    Text node = (Text)nl.item(i).getFirstChild();
                    if(node != null)
                    {
                        holdValue = node.getNodeValue();                    
                    }               
                    
                    if(holdValue != null && holdValue.length() >0)
                    {
                        return holdValue;
                    }                                    
                }
            }            
        }        
        
        return holdValue;
                
  }
    
  private File getXSL(String xslName, ActionMapping mapping)
  {

    File xslFile = null;
    String xslFilePathAndName = null;

    ActionForward forward = mapping.findForward("CustomerHoldXSL");
    xslFilePathAndName = forward.getPath();                //get real file name
    xslFile = new File(this.getServlet().getServletContext().getRealPath(xslFilePathAndName));
    return xslFile;
  }        
 
     public static void addElement(Document xml, String keyName, String value)
    {
        Element nameElement = (Element) xml.createElement(keyName);
        
        if(value != null && value.length() > 0 )
        {
            nameElement.appendChild(xml.createTextNode(value));
        }
        xml.getDocumentElement().appendChild(nameElement);    
    }    

 
  
     /** This method converts an XML document to a String. 
     * @param Document document to convert 
     * @return String version of XML */
    private String convertDocToString(Document xmlDoc) throws Exception
    {
            String xmlString = "";      
          
            StringWriter s = new StringWriter();
            DOMUtil.print(xmlDoc,new PrintWriter(s));
            xmlString = s.toString();

            return xmlString;
        }        
        
    
}
