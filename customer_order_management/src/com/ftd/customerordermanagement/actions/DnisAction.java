package com.ftd.customerordermanagement.actions;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import com.ftd.customerordermanagement.bo.CSDnisBO;
import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.dao.DnisDAO;
import com.ftd.customerordermanagement.filter.DataFilter;
import com.ftd.customerordermanagement.util.CallTimer;
import com.ftd.customerordermanagement.vo.DnisInfoVO;
import com.ftd.decisionresult.constant.DecisionResultConstants;
import com.ftd.decisionresult.filter.DecisionResultDataFilter;
import com.ftd.messaging.constants.MessagingConstants;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityManager;
import com.ftd.security.cache.vo.UserInfo;



/**
 * This class is used to used to obtain information associated with the DNIS, set
 * the call_type as C/S or O/E, start the call timer and call the customer service
 * dnis entered screen.  The dnis data will be transformed by the customer service 
 * dnis entered xsl utilizing the TraxUtil class.
 *
 * @author Matt Wilcoxen
 */

public class DnisAction extends Action 
{
	private Logger logger = new Logger("com.ftd.customerordermanagement.actions.DnisAction");
    
    //request parameters:
	private static final String R_CALL_DNIS = "call_dnis";
	private static final String R_DNIS_NUM = "dnis_num";
	private static final String R_ACTION = "action";

    //XML nodes:
    private static final String X_ROOT_NODE = "root";
	// private static final String X_OE_FLAG = "OE_FLAG";
	// private static final String X_BRAND_NAME = "COMPANY_NAME";
	// private static final String X_CUST_SERV_NUM = "PHONE_NUMBER";

    private static final String CUSTOMER_SERVICE = "customer_service";
    private static final String LOSS_PREVENTION_SEARCH = "loss_prevention_search";
    private static final String ERROR_PAGE = "errorPage";
    

	/**
	 * This is the DNIS Action called from the Struts framework.
     *     1. get db connection
     *     2. get parameters
     *     3. get csr info
     *     4. if requested action is customer service
     *            if no dnis entered
     *                get default dnis
     *                start call timer
     *                build parameters hashmap
     *            go to customer order search page  
     *     5. else
     *            get dnis data
     *            if invalid dnis, forward to customer service menu with error
     *            set order indicator
     *            append csr script
     *            start call timer
     *            build parameters object
     *            go to customer service dnis entered page
	 * 
	 * @param mapping            - ActionMapping used to select this instance
	 * @param form               - ActionForm (optional) bean for this request
	 * @param request            - HTTP Request we are processing
	 * @param response           - HTTP Response we are processing
     * 
	 * @return forwarding action - next action to "process" request
     * 
	 * @throws IOException
	 * @throws ServletException
	 */
	public  ActionForward execute(ActionMapping mapping, ActionForm form, 
                                  HttpServletRequest request, HttpServletResponse response)
		throws IOException, ServletException
	{
        Connection conn = null;
        ConfigurationUtil cu = null;
		DnisInfoVO dnisDataVO;
		String callPage = null;
		String callPageURL = null;
        ActionForward forward = null;

        //security:  security on/off ind, security manager
        String security = "";
        boolean securityIsOn = false;
        boolean cdispPermission = false;
        boolean cdispActive = false;
        SecurityManager sm = null;
        UserInfo ui = null;
        String csrId = null;
        String csrIdFirstName = "";

        //request parameters
        String context   = "";
        String securityToken   = "";
        String dnisId = "";
        String action = "";
        
        String orderIndicator = "";
		String startOrigin = "";

        HashMap parameters = new HashMap();

		try
		{
            cu = ConfigurationUtil.getInstance();

            // 1. get db connection
			conn = getDBConnection();

            // 2. get parameters
            context = request.getParameter(COMConstants.CONTEXT);
            securityToken   = request.getParameter(COMConstants.SEC_TOKEN);
            action = request.getParameter(R_ACTION);
            cdispActive = ("Y".equalsIgnoreCase(cu.getFrpGlobalParm(COMConstants.COM_CONTEXT, COMConstants.CONS_CDISP_ACTIVE_FLAG))?true:false);
            logger.debug("execute(): request parms are: \n" +
                "context = " + context + "\n" +
                "securityToken = " + securityToken + "\n" +
                "dnisId = " + dnisId + "\n" +
                "action = " + action);

            // 3. get csr Id and 1st name
            security = (String) cu.getProperty(COMConstants.PROPERTY_FILE,COMConstants.SECURITY_IS_ON);
            securityIsOn = security.equalsIgnoreCase("true") ? true : false;
            if(securityIsOn)
            {
                sm = SecurityManager.getInstance();
                ui = sm.getUserInfo(securityToken);
                csrId = ui.getUserID();
                csrIdFirstName = ui.getFirstName();
                cdispPermission = sm.assertPermission(context, securityToken, COMConstants.CDISP_ADMIN, COMConstants.VIEW);
            }

            // 4. customer service action requested ?
            if(action != null  &&  (action.equalsIgnoreCase(CUSTOMER_SERVICE) || action.equalsIgnoreCase(LOSS_PREVENTION_SEARCH)))
            {
                dnisId = request.getParameter(R_CALL_DNIS);
                //no dnis entered ?
                if(dnisId == null  ||  dnisId.trim().length() == 0)
                {
                    dnisId = (String) cu.getProperty(COMConstants.PROPERTY_FILE,COMConstants.CONS_DEFAULT_DNIS);

                    //start call timer
                    String callLogId = startTimer(conn,dnisId,csrId);
                    
                    //add additional parameters to request
                    parameters = (HashMap) request.getAttribute(COMConstants.CONS_APP_PARAMETERS);
                    if(parameters == null)
                    {
                        parameters = new HashMap();
                    }
                    logger.debug("NO dnis id, using default dnis id.  parameters.put are: \n" +
                        "brand name = " + "" + "\n" +
                        "customer order indicator = " + "" + "\n" +
                        "customer service number = " + "" + "\n" +
                        "dnis id = " + dnisId + "\n" +
                        "call log id = " + callLogId);

                    parameters.put(COMConstants.H_BRAND_NAME,"");
                    parameters.put(COMConstants.H_CUSTOMER_ORDER_INDICATOR,"");
                    parameters.put(COMConstants.H_CUSTOMER_SERVICE_NUMBER,"");
                    parameters.put(COMConstants.H_DNIS_ID,dnisId);
                    parameters.put(COMConstants.TIMER_CALL_LOG_ID,callLogId);
					parameters.put(COMConstants.START_ORIGIN,COMConstants.START_ORIGIN_CS);
                }
                
                //go to customer order search page  
                if(action.equalsIgnoreCase(CUSTOMER_SERVICE)) {
                    callPage = COMConstants.CONS_CUSTOMER_ORDER_SEARCH;    
                }
                else {
                    callPage = COMConstants.CONS_LOSS_PREVENTION_SEARCH;
                }
                
            }
            // 5. not customer service action - go to cs dnis page
            else
            {
                dnisId = request.getParameter(R_DNIS_NUM);

                // Call Disposition is only enabled if global flag indicates it's active and
                // DNIS was specified (and not '0000') and CSR has proper permission
                //
                if (cdispActive && StringUtils.isNotBlank(dnisId) && !StringUtils.trim(dnisId).equals("0000") && cdispPermission) {
                    DataFilter.updateParameter(request, COMConstants.CDISP_ENABLED_FLAG, "Y");
                    DecisionResultDataFilter.updateParameter(request, DecisionResultConstants.REQ_DNIS_ID, dnisId);
                    if (logger.isDebugEnabled()) {
                        logger.debug("Call Disposition enabled for this user");
                    }
                }

                //get dnis info
                DnisDAO dDAO = new DnisDAO(conn);
                dnisDataVO = dDAO.getDnisData(dnisId);
                //if invalid dnis, forward to customer service menu with error
                if((dnisDataVO == null) || 
									(dnisDataVO != null && dnisDataVO.getStatus() != null && dnisDataVO.getStatus().equalsIgnoreCase(COMConstants.CONS_DNIS_INACTIVE)))
                {
                    String errorMsg = null;
										if(dnisDataVO != null && dnisDataVO.getStatus() != null && dnisDataVO.getStatus().equalsIgnoreCase(COMConstants.CONS_DNIS_INACTIVE))
										{
											errorMsg = cu.getProperty(COMConstants.PROPERTY_FILE, COMConstants.CONS_DNIS_INACTIVE_MSG);																						
										}
										else
										{
											errorMsg = cu.getProperty(COMConstants.PROPERTY_FILE, COMConstants.CONS_DNIS_ERROR_MSG);											
										}
										
                    callPageURL  = cu.getFrpGlobalParm(COMConstants.COM_CONTEXT, COMConstants.CONS_MAIN_MENU_URL);
                    callPageURL += "?adminAction=customerService";
                    callPageURL += "&context=" + context;
                    callPageURL += "&securitytoken=" + securityToken;
                    callPageURL += "&call_dnis=";
                    callPageURL += "&call_brand_name=";
                    callPageURL += "&call_cs_number=";
                    callPageURL += "&call_oe_flag=";
                    callPageURL += "&t_call_log_id=" + COMConstants.TIMER_CALL_LOG_ID;
                    callPageURL += "&t_comment_origin_type=";
                    callPageURL += "&t_entity_history_id=";
                    callPageURL += "&dnis_error_msg=" + errorMsg;
                    logger.debug("callPageURL = " + callPageURL);          
                    response.sendRedirect(callPageURL);
                    return null;
                }
                /* If OE flag is 'Y' and user does not have access to OldWebOE
                 * route the user directly to JOE
                 */
                else if(dnisDataVO.getOEFlag().equalsIgnoreCase(COMConstants.CONS_YES)
                        && !SecurityManager.getInstance().assertPermission(context, securityToken, COMConstants.OLD_WEBOE, COMConstants.VIEW))
                {
                    //start call timer
                    String callLogId = startTimer(conn,dnisId,csrId);

                    //set required information
                    request.setAttribute(COMConstants.TIMER_CALL_LOG_ID,callLogId);
                    request.setAttribute(COMConstants.H_DNIS_ID, dnisId);
                    
                    return mapping.findForward("orderEntry");
                }

                //print dnis data from DAO
                logger.debug("execute(): dnis Data = \n" + 
                    "Brand Name = " + dnisDataVO.getBrandName() + "\n" +
                    "Dnis = " + dnisDataVO.getDnis() + "\n" +
                    "CS Number = " + dnisDataVO.getCSNumber() + "\n" +
                    "OE Flag = " + dnisDataVO.getOEFlag());

                //set order indicator
                if(dnisDataVO.getOEFlag().equalsIgnoreCase(COMConstants.CONS_YES))
                {
                    orderIndicator = cu.getProperty(COMConstants.PROPERTY_FILE,COMConstants.CONS_OE_INDICATOR);
					startOrigin = COMConstants.START_ORIGIN_OE;
                }
                else
                {
                    orderIndicator = cu.getProperty(COMConstants.PROPERTY_FILE,COMConstants.CONS_CS_INDICATOR);
					startOrigin = COMConstants.START_ORIGIN_CS;
                }
                
                // SP-73
                if(MessagingConstants.PRO_COMPANY_NAME.equalsIgnoreCase(dnisDataVO.getBrandName())) {
                	dnisDataVO.setBrandName(MessagingConstants.FTD_COMPANY_NAME);
                }

                //append csr script
                Document dnisXMLDoc = (Document) DOMUtil.getDefaultDocument();
                Element root = dnisXMLDoc.createElement(X_ROOT_NODE);
                dnisXMLDoc.appendChild(root);
                CSDnisBO cDnisBO = new CSDnisBO();
                cDnisBO.appendScript(dnisXMLDoc,csrIdFirstName,dnisDataVO.getBrandName());

                //start call timer
                String callLogId = startTimer(conn,dnisId,csrId);

                //build parameters object
                parameters = (HashMap) request.getAttribute(COMConstants.CONS_APP_PARAMETERS);
                if(parameters == null)
                {
                    parameters = new HashMap();
                }

                String brandName = dnisDataVO.getBrandName();
                String custServNum = dnisDataVO.getCSNumber();

                logger.debug("dnis id supplied.  parameters.put are: \n" +
                    "brand name = " + brandName + "\n" +
                    "customer order indicator = " + orderIndicator + "\n" +
                    "customer service number = " + custServNum + "\n" +
                    "dnis id = " + dnisId + "\n" +
                    "call log id = " + callLogId);

                parameters.put(COMConstants.H_BRAND_NAME,brandName);
                parameters.put(COMConstants.H_CUSTOMER_ORDER_INDICATOR,orderIndicator);
                parameters.put(COMConstants.H_CUSTOMER_SERVICE_NUMBER,custServNum);
                parameters.put(COMConstants.H_DNIS_ID,dnisId);
                parameters.put(COMConstants.TIMER_CALL_LOG_ID,callLogId);
			    parameters.put(COMConstants.START_ORIGIN,startOrigin);
    
                //go to Customer Service DNIS Entered screen
                File xslFile = getXSL(mapping, request, COMConstants.XSL_CUSTOMER_SERVICE_DNIS_ENTERED);
                forward = mapping.findForward(COMConstants.XSL_CUSTOMER_SERVICE_DNIS_ENTERED);
                String xslFilePathAndName = forward.getPath(); //get real file name
                
                // Change to client side transform
                TraxUtil.getInstance().getInstance().transform(request, response, dnisXMLDoc, 
                                    xslFile, xslFilePathAndName, parameters);

                return null;
            }            
			
		}
		catch (ClassNotFoundException  cnfe)
		{
			logger.error(cnfe);
            callPage = ERROR_PAGE;
		}
		catch (IOException  ioe)
		{
			logger.error(ioe);
            callPage = ERROR_PAGE;
		}
		catch (ParserConfigurationException  pcex)
		{
			logger.error(pcex);
            callPage = ERROR_PAGE;
		}
		catch (SAXException  sec)
		{
			logger.error(sec);
            callPage = ERROR_PAGE;
		}
		catch (SQLException  sqle)
		{
			logger.error(sqle);
            callPage = ERROR_PAGE;
		}
		catch (TransformerException  tex)
		{
			logger.error(tex);
            callPage = ERROR_PAGE;
		}
		catch (Throwable  t)
		{
			logger.error(t);
            callPage = ERROR_PAGE;
		}
		finally
		{
			try
			{
				conn.close();
			}
			catch (SQLException se)
			{
				logger.error(se);
                callPage = ERROR_PAGE;
			}
		}

        if(callPage != null  &&  callPage.trim().length() > 0)
        {
            forward = mapping.findForward(callPage);
            return forward;
        }
        else
        {
            return null;
        }
	}


	/**
	 * Obtain connectivity with the database
	 * 
	 * @param none
     * 
	 * @return Connection - db connection
     * 
     * @throws Exception
     * @throws IOException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws TransformerException
     * @throws XSLException
	 */
	private Connection getDBConnection()
		throws IOException, ParserConfigurationException, SAXException, TransformerException, 
               Exception
	{
		Connection conn = null;
		conn = DataSourceUtil.getInstance().getConnection(
				   ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE,
															   COMConstants.DATASOURCE_NAME));

		return conn;
	}


	/**
	 * Start the call timer.
	 * 
	 * @param Connection - db connection
	 * @param String     - dnis Id
	 * @param String     - csr Id
     * 
	 * @return String    - call timer log id
     * 
     * @throws IOException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws SQLException
	 */
	private String startTimer(Connection conn, String dnisId, String csrId)
        throws IOException, ParserConfigurationException, SAXException,
               SQLException
    {
        //start call timer
        CallTimer ct = new CallTimer(conn);
        return ct.start(dnisId,csrId);
    }

                    
	/**
	 * Retrieve name and location of XSL file
     *     1. get file lookup name
     *     2. get real file name
     *     3. get xsl file location
	 * 
	 * @param ActionMapping      - Stuts mapping used for actionforward lookup
     * @param HttpServletRequest - user request
	 * @param QueueRequestVO     - queue request value object
     * 
	 * @return File              - XSL File name with real path
     * 
	 * @throws none
	 */
    public File getXSL(ActionMapping mapping, HttpServletRequest request, String cust_ord_inq_file)
	{
		File XSLFile = null;
		String XSLFileLookUpName = null;
		String XSLFileName = null;

        // 1. get file lookup name
        XSLFileLookUpName = cust_ord_inq_file;

        // 2. get real file name
        ActionForward forward = mapping.findForward(XSLFileLookUpName);
        XSLFileName = forward.getPath();

        // 3. get xsl file location
        XSLFile = new File(this.getServlet().getServletContext().getRealPath(XSLFileName));
        return XSLFile;
    }

}