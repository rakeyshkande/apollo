package com.ftd.customerordermanagement.actions;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.dao.StockMessageDAO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;

import com.ftd.security.SecurityManager;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import java.io.File;
import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * LoadMessageAction class
 * 
 */

public final class LoadMessageAction extends Action 
{
    private static Logger logger  = new Logger("com.ftd.customerordermanagement.actions.LoadMessageAction");
    private static String OUT_CUR = "OUT_CUR";
    private static String OUT_ID_COUNT = "OUT_ID_COUNT";
    private static String OUT_PARAMETERS = "out-parameters";
    private static String YES = "Y";
    private static String NO = "N";
    private static String ON = "on";
  
  /**
   * This is the main action called from the Struts framework.
   * @param mapping The ActionMapping used to select this instance.
   * @param form The optional ActionForm bean for this request.
   * @param request The HTTP Request we are processing.
   * @param response The HTTP Response we are processing.
   */
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException
  {
    HashMap secondaryMap = new HashMap();
    Connection conn = null;
    String messageType = null;
    String removeEmailQueueFlag = null;
    String pocId = null;
    String actionType = null;
    String lineNum = null;
    String newEmailSubject = null; 
    String newEmailBody = null;
    String userId = null;
    String sessionId = null;
    String mercOrderNumber = null;
    
    try
    {
      // pull base document
      Document responseDocument = (Document) DOMUtil.getDocument();
      
      //get all the parameters in the request object
      // get action type
      actionType = request.getParameter(COMConstants.ACTION_TYPE);

      //retrieve the security token
      sessionId = (String) request.getParameter(COMConstants.SEC_TOKEN);
      if(sessionId==null)
        throw new Exception("Security token not found in request object");
  
      //retrieve the user id
      if (SecurityManager.getInstance().getUserInfo(sessionId)!=null)
        userId = SecurityManager.getInstance().getUserInfo(sessionId).getUserID();

      secondaryMap.put(COMConstants.ACTION_TYPE, actionType);
      
      // remove email queue flag
      removeEmailQueueFlag = request.getParameter(COMConstants.REMOVE_EMAIL_QUEUE_FLAG);
      
      if(request.getAttribute("remove_email_queue_flag") != null && 
            !request.getAttribute("remove_email_queue_flag").equals(""))
      {
          secondaryMap.put(COMConstants.REMOVE_EMAIL_QUEUE_FLAG, request.getAttribute("remove_email_queue_flag"));
      }
      else
      {
          secondaryMap.put(COMConstants.REMOVE_EMAIL_QUEUE_FLAG, removeEmailQueueFlag);
      }
      
      // point of contact id
      pocId = request.getParameter(COMConstants.POINT_OF_CONTACT_ID);
      if(pocId==null || Long.parseLong(pocId) == 0)
      {
        logger.error("pocId is missing or is zero!");
      }
      secondaryMap.put(COMConstants.POINT_OF_CONTACT_ID, pocId);
      
      secondaryMap.put("status", request.getParameter("status"));
      secondaryMap.put("tagged_csr_id", request.getParameter("tagged_csr_id"));
      secondaryMap.put("message_id", request.getParameter("message_id"));
      secondaryMap.put("order_detail_id", request.getParameter("order_detail_id"));
      secondaryMap.put("customer_id", request.getParameter("msg_customer_id"));
      secondaryMap.put("attached", request.getParameter("attached"));
      
      // message line number
      lineNum = request.getParameter(COMConstants.MESSAGE_LINE_NUMBER);
      secondaryMap.put(COMConstants.MESSAGE_LINE_NUMBER, lineNum);
      
      // Set any error messages to the page data
      secondaryMap.put("queue_delete_security_error", request.getAttribute("queue_delete_security_error"));
      secondaryMap.put("queue_delete_general_error", request.getAttribute("queue_delete_general_error"));
                      
      // get database connection
      conn = DataSourceUtil.getInstance().getConnection(ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, COMConstants.DATASOURCE_NAME));

      // Call load DAO method to load email or letter view
      StockMessageDAO stockMsg = new StockMessageDAO(conn);

      //check to see if the user-role can modify an email
      boolean userInRole = false; 
  
      userInRole = SecurityManager.getInstance().assertPermission(request.getParameter("context"), sessionId, "Comments And Email Resource", "Update"); 
      secondaryMap.put("user_can_update", userInRole?"Y":"N");
      
      if (actionType != null)
      {
        if (actionType.equalsIgnoreCase("delete_email"))
        {
          //Create a Calendar Object for todays date
          Calendar cToday = Calendar.getInstance();
      
          //Define a date format 
          SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
          
          //create the new Email Content
          //create the comment text
          newEmailBody = "Email text deleted by " + userId + " on " + sdf.format(cToday.getTime());

          //create the new Subject
          newEmailSubject = newEmailBody;
      

          //update the subject and body
          stockMsg.updatePOCSubjectBody(pocId, newEmailSubject, newEmailBody, userId);
        }
      }

      // Get poc data from database
      Document pocXML = stockMsg.loadPointOfContactXML(pocId);
      
      // message type should come from poc
      ArrayList nameList = new ArrayList();
      nameList.add(0, "PointOfContact");
      nameList.add(1, "poc");
      nameList.add(2, "point_of_contact_type");
      messageType = DOMUtil.getNodeValue(pocXML, nameList);
      secondaryMap.put(COMConstants.MESSAGE_TYPE, messageType);
      
      // replace all anchor references
      NodeList nl = pocXML.getElementsByTagName("body");
      for (int j=0; j<nl.getLength(); j++) {
          Element anchor = (Element) nl.item(j);
          String value = "";
          if (anchor.hasChildNodes()) {
              Node n = anchor.getFirstChild();
              value = n.getNodeValue();
              int startPos = 0;
              int i = value.indexOf("href=", startPos);
              while (i > -1) {
              	String delim = value.substring(i+5, i+6);
              	int nexti = value.indexOf(delim, i+6);
              	if (nexti > 0) {
              		String two = value.substring(nexti+1);
              		String one = value.substring(0, i);
              		value = one + two;
              	} else {
              	    startPos = i+5;
              	}
              	i = value.indexOf("href=", startPos);
              }
              n.setNodeValue(value);
          }
      }

      // verify email addresses?
      DOMUtil.addSection(responseDocument, pocXML.getChildNodes());

      // If mercury FTD exists, get any associated ASK messages (since they may
      // contain changes requested by CSR on behalf of customer).  We currently
      // just display these on the View Email page.
      nameList.set(2, "mo_curr_merc_order_num");
      mercOrderNumber = DOMUtil.getNodeValue(pocXML, nameList);
      if (mercOrderNumber != null) {
         Document pocAskXML = stockMsg.loadPointOfContactAsksXML(mercOrderNumber);
         DOMUtil.addSection(responseDocument, pocAskXML.getChildNodes());
      }
      
      // Generate XML from secondary HashMap using the DOMUtil HashMap to XML conversion method addSection() .
      DOMUtil.addSection(responseDocument, "pageData", "data", secondaryMap, true);
      //responseDocument.print(System.out);  
      //close db connection
      conn.close();
        
      // Forward to transformation action
      //Get XSL File name
      File xslFile = getXSL(messageType, mapping);  
      ActionForward forward = mapping.findForward(messageType);
      String xslFilePathAndName = forward.getPath(); //get real file name
      
      // Change to client side transform
      TraxUtil.getInstance().getInstance().transform(request, response, responseDocument, 
                          xslFile, xslFilePathAndName, (HashMap)request.getAttribute(COMConstants.CONS_APP_PARAMETERS));
      return null;
    }
    catch (Throwable ex)
    {
      try
      {
        if(conn!=null)
          conn.close();
      }
      catch (SQLException se)
      {
        logger.error(se);
        return mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
      }
      logger.error(ex);
      return mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
    }  
    finally 
    {
      try
      {
        if(conn!=null)
          conn.close();
      }
      catch (SQLException se)
      {
        logger.error(se);
        return mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
      }
    }  
  }

  /******************************************************************************
  *                                     getXSL()
  *******************************************************************************
  * Retrieve name of Customer Order Search Action XSL file
  * @param1 String - the xsl name returned from the Business Object
  * @param2 ActionMapping
  * @return File - XSL File name
  * @throws none
  */

  private File getXSL(String xslName, ActionMapping mapping)
  {

    File xslFile = null;
    String xslFilePathAndName = null;

    ActionForward forward = mapping.findForward(xslName);
    xslFilePathAndName = forward.getPath();                //get real file name
    xslFile = new File(this.getServlet().getServletContext().getRealPath(xslFilePathAndName));
    return xslFile;
  }
}