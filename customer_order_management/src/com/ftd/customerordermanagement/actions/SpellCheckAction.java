package com.ftd.customerordermanagement.actions;


import com.ftd.customerordermanagement.util.ISpellCheck;
import com.ftd.customerordermanagement.util.SpellCheckImpl;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public final class SpellCheckAction extends Action 
{
    private ISpellCheck spell = null;
    private static Logger logger  = new Logger("com.ftd.customerordermanagement.actions.SpellCheckAction");
   
  /**
   * This is the main action called from the Struts framework.
   * @param mapping The ActionMapping used to select this instance.
   * @param form The optional ActionForm bean for this request.
   * @param request The HTTP Request we are processing.
   * @param response The HTTP Response we are processing.
   */
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException
  {
      processCheckSpell( request, response );
      return null;
  } 
/**
  * check the word to see if it is valid. If not, return suggestions
  * The returned suggestion format must comply with the javaScript rules.
  * @param word String to be checked
  * @param int end the absolute position in the original string
  * @return String suggestions
  */
    private String getSuggestions( String word, int end ) throws IOException
    {
      
        if ( word == null || word.length() == 0 )
            return null;
        if ( spell.spellCheck( word ) )
            return null;
        List suggest = spell.suggest( word );
        int start = end - word.length();
        StringBuffer result = new StringBuffer();
        result.append( "{start:" );
        result.append( start );
        result.append( ",end:" );
        result.append( end );
        result.append( ",suggestions:[" );
        if ( suggest != null )
        {
            for ( int i=0; i<suggest.size(); i++ )
            {
                result.append( "\"" + (String)suggest.get(i) + "\"");
                if ( i < suggest.size() -1 )
                    result.append( "," );
            }
        }
        result.append( "]}" );
        return result.toString();
    }

    private void processCheckSpell( HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException
    {
      
        try
        {
            spell = new SpellCheckImpl(  );
        }
        catch ( Exception ioe )
        {
            logger.error(ioe);
            throw new ServletException(ioe);
        }
      String original = request.getParameter( "content" );

        if ( original == null )
            return;

        original = parseValue( original );

        String temp = original + " " ;

        ArrayList suggestions = new ArrayList();

        StringBuffer sb = new StringBuffer();
        String suggest = null;
        for (int location=0; location<temp.length(); location++)
        {
            char c = temp.charAt( location );
            if ( charOmitted( c ) ) {
                if ( !spell.spellCheck( sb.toString() ) ) {
                    suggest = getSuggestions( sb.toString(), location );

                    if ( suggest != null ) {
                        suggestions.add( suggest );
					}
                }
                // reset
                sb = new StringBuffer();

            }
            else {
                sb.append( c );
            }
        }

        String corrections = "[";
        for (int i=0; i<suggestions.size(); i++) {
            corrections += (String)suggestions.get(i);
            if ( i < suggestions.size() - 1 )
                corrections += ",";
        }
        corrections += "]";

        StringBuffer sbOriginal = new StringBuffer();

        for (int i=0; i<original.length(); i++)
        {
            char c = original.charAt( i );
            if ( c == '"' )
                sbOriginal.append( "\\" );
            sbOriginal.append( c );
        }

        request.setAttribute( "original", sbOriginal.toString() );
        request.setAttribute( "text", sbOriginal.toString() );
        request.setAttribute( "corrections", corrections );
        request.setAttribute( "callerName", request.getParameter("callerName"));
        request.getRequestDispatcher("/response.jsp").forward( request, response );
    }

/**
  * Omit the char in the string to be spell checked
  * All the character that do not want to be checked should go here
  * @param c the char to be checked.
  * @return true if the passed character should be omitted.
  */
    private static boolean charOmitted ( char c )
    {
        return ( Character.isWhitespace( c ) || (c == ' ') || (c == '"') || ( c =='.') || ( c == ':' )
                 || ( c == ';' ) || ( c == '$') || ( c == '!' ) || ( c == '?' ) || ( c == ',') );
    }
/**
  * Change end of line character to html <br> tag.
  * @param String value to be changed
  * @return the  new formatted String
  */
    private String parseValue( String value )
    {
        StringBuffer sb = new StringBuffer();
		logger.debug("parseValue(): value :: " + value);
        for (int i=0; i<value.length(); i++)
        {
            char c = value.charAt( i );
            if ( c == '\r' )
                sb.append( "" );
            else if ( c == '\n' )
                sb.append( " <br> " );
            else
                sb.append( c );
        }
        return sb.toString();
    }
}