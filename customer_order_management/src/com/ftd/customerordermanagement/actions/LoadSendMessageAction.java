package com.ftd.customerordermanagement.actions;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.dao.OrderDAO;
import com.ftd.customerordermanagement.dao.StockMessageDAO;
import com.ftd.ftdutilities.FTDCommonUtils;
import com.ftd.ftdutilities.FieldUtils;
import com.ftd.messagegenerator.StockMessageGenerator;
import com.ftd.messagegenerator.vo.PointOfContactVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.PartnerUtility;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.handlers.PaymentMethodHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.PartnerVO;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.XMLFormat;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.order.MercentOrderPrefixes;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.PartnerMappingVO;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.JAXPUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityManager;


/**
 * LoadSendMessageAction class
 *
 */
public final class LoadSendMessageAction extends Action 
{
    private static Logger logger  = new Logger("com.ftd.customerordermanagement.actions.LoadSendMessageAction");
    private static String OUT_CUR = "OUT_CUR";
    private static String OUT_ID_COUNT = "OUT_ID_COUNT";
    private static String OUT_PARAMETERS = "out-parameters";
    private static String YES = "Y";
    private static String NO = "N";
    private static String ON = "on";
    
  /**
   * This is the main action called from the Struts framework.
   * @param mapping The ActionMapping used to select this instance.
   * @param form The optional ActionForm bean for this request.
   * @param request The HTTP Request we are processing.
   * @param response The HTTP Response we are processing.
   */
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException
  {
    Connection conn = null;
    CachedResultSet rs = null;
    String orderDetailId = null;
    String includeComments  = "Y"; 
    HashMap secondaryMap = new HashMap();
    String messageType = null;
    String newMessageFlag = null;
    String spellCheckFlag = null;
    String actionType = null;
    String originId = null;
    String companyId = null;
    String messageId = null;
    String replyFlag = null;
    String origMessageSubject = null;
    String origMessageBody = null;
    String extOrderNum = null;
    String masterOrderNum = null;
    String customerFirstname = null;
    String customerLastname = null;
    String customerEmailAddress = null;
    String customerAltEmailAddress = null;
    String customerId = null;
    String orderGuid = null;
    String csrFirstName = null;
    String csrLastName = null;
    String sourceCode = null;
    String vip_customer = null;
    String product_type = null;
    String ship_method = null;
    String vendor_flag = null;
    
    try
    {
      // pull base document
      Document responseDocument = (Document) DOMUtil.getDocument();
      
      //get all the parameters in the request object
      // get action type
      actionType = request.getParameter(COMConstants.ACTION_TYPE);
      secondaryMap.put(COMConstants.ACTION_TYPE, actionType);
      // message type
      messageType = request.getParameter(COMConstants.MESSAGE_TYPE);
      secondaryMap.put(COMConstants.MESSAGE_TYPE, messageType);
      // new message flag
      newMessageFlag = request.getParameter(COMConstants.NEW_MESSAGE_FLAG);
      secondaryMap.put(COMConstants.NEW_MESSAGE_FLAG, newMessageFlag);
      // spell check flag
      spellCheckFlag = request.getParameter(COMConstants.SPELL_CHECK_FLAG);
      secondaryMap.put(COMConstants.SPELL_CHECK_FLAG, spellCheckFlag);
      // reply flag
      if(request.getParameter(COMConstants.EMAIL_REPLY_FLAG)!=null)
      {
        replyFlag = request.getParameter(COMConstants.EMAIL_REPLY_FLAG);
      }
      else
        replyFlag = "N";
      secondaryMap.put(COMConstants.EMAIL_REPLY_FLAG, replyFlag);
      // original email subject
      if(request.getParameter(COMConstants.MESSAGE_SUBJECT)!=null)
      {
        origMessageSubject = request.getParameter(COMConstants.MESSAGE_SUBJECT);
        secondaryMap.put(COMConstants.MESSAGE_SUBJECT, origMessageSubject);
      }
      // original email body
      origMessageBody = request.getParameter(COMConstants.MESSAGE_CONTENT);
      secondaryMap.put(COMConstants.MESSAGE_CONTENT, origMessageBody);
      // get order detail id
      orderDetailId = request.getParameter(COMConstants.ORDER_DETAIL_ID);
      secondaryMap.put(COMConstants.ORDER_DETAIL_ID, orderDetailId);
      
      //bypass_com
      if(request.getParameter(COMConstants.BYPASS_COM)!= null)
          secondaryMap.put(COMConstants.BYPASS_COM, request.getParameter(COMConstants.BYPASS_COM));
      else
          secondaryMap.put(COMConstants.BYPASS_COM, "N");
      
      // get vip_customer flag
      vip_customer = request.getParameter(COMConstants.VIP_CUSTOMER);
      secondaryMap.put(COMConstants.VIP_CUSTOMER, vip_customer);
      
      // get product type
      product_type = request.getParameter(COMConstants.PRODUCT_TYPE);
      secondaryMap.put(COMConstants.PRODUCT_TYPE, product_type);
      
      // get ship_method
      ship_method = request.getParameter(COMConstants.SHIP_METHOD);
      secondaryMap.put(COMConstants.SHIP_METHOD, ship_method);
      
      // get order vendor_flag
      vendor_flag = request.getParameter(COMConstants.VENDOR_FLAG);
      secondaryMap.put(COMConstants.VENDOR_FLAG, vendor_flag);
      
      // message id
      messageId = request.getParameter(COMConstants.MESSAGE_ID);
      secondaryMap.put(COMConstants.MESSAGE_ID, messageId);
      
      secondaryMap.put("email_address", request.getParameter("email_address"));
                      
      // get database connection
      conn =
      DataSourceUtil.getInstance().getConnection(ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE,
                                                  COMConstants.DATASOURCE_NAME));

      // use OrderDAO to access some header info on the sendEMail.xsl page
      OrderDAO orderAccess = new OrderDAO(conn);
      rs = orderAccess.getOrderCustomerInfo(orderDetailId);
      if(rs.next())
      {
        customerId = rs.getString("customer_id");
        orderGuid = rs.getString("order_guid");
        extOrderNum = rs.getString("external_order_number");
        masterOrderNum = rs.getString("master_order_number");
        customerFirstname = rs.getString("first_name");
        customerLastname = rs.getString("last_name");
        customerEmailAddress = rs.getString("email_address");
        customerAltEmailAddress = rs.getString("alt_email_address");
        companyId = rs.getString("company_id");
        originId = rs.getString("origin_id");
      }
      else
      {
        logger.error("Customer Info missing from DB for orderDetailId="+orderDetailId);
      }
      
      rs = orderAccess.getOrderDetailsInfo(orderDetailId);
      if (rs.next())
      {
          sourceCode = rs.getString("source_code");
      }
      else
      {
          logger.error("Order Detail Info missing from DB for orderDetailId="+orderDetailId);
      }
        
      extOrderNum = getOrderNumber(extOrderNum, originId,conn);
      
      String pocId = request.getParameter("poc_id");
      if(pocId != null && !pocId.equals(""))
      {
          StockMessageDAO stockMessageDAO = new StockMessageDAO(conn);
          PointOfContactVO pointOfContact = stockMessageDAO.loadPointOfContact(pocId);
          secondaryMap.put("sender_email_address", pointOfContact.getSenderEmailAddress());
          secondaryMap.put("recip_email_address", pointOfContact.getRecipientEmailAddress());
          secondaryMap.put("poc_id", request.getParameter("poc_id"));
          if (companyId == null && pointOfContact.getCompanyId() != null) {
        	  companyId = pointOfContact.getCompanyId();
          }
      }
      logger.info("companyId: " + companyId);
      
      secondaryMap.put(COMConstants.CUSTOMER_ID, customerId);
      secondaryMap.put(COMConstants.EXTERNAL_ORDER_NUMBER, extOrderNum);
      secondaryMap.put(COMConstants.MASTER_ORDER_NUMBER, masterOrderNum);
      secondaryMap.put(COMConstants.CUSTOMER_FIRST_NAME, customerFirstname);
      secondaryMap.put(COMConstants.CUSTOMER_LAST_NAME, customerLastname);
      secondaryMap.put(COMConstants.CUSTOMER_EMAIL_ADDRESS, customerEmailAddress);
      secondaryMap.put(COMConstants.CUSTOMER_ALT_EMAIL_ADDRESS, customerAltEmailAddress);
      secondaryMap.put(COMConstants.ORDER_GUID, orderGuid);
      secondaryMap.put(COMConstants.COMPANY_ID, companyId);
      secondaryMap.put(COMConstants.ORIGIN_ID, originId);
        
      // Generate XML from secondary HashMap using the DOMUtil HashMap to XML conversion method addSection() .
      DOMUtil.addSection(responseDocument, "pageData", "data", secondaryMap, true);
      
      StockMessageDAO msgAccess = new StockMessageDAO(conn);
      boolean isMercentOrder = false;
      PartnerMappingVO partnerMappingVO = new PartnerUtility().getPartnerOriginsInfo(originId,sourceCode, conn);
	        
      // scrub the origin id against config file origins list
      if(originId!=null && !originId.equalsIgnoreCase("GRPN"))
      {
        String originList = ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, COMConstants.EMAIL_ORIGINS);
       
        if(!(originList.indexOf(originId)>=0)){
        	MercentOrderPrefixes orderPrefixes = new MercentOrderPrefixes(conn);
        	List<String> origins = orderPrefixes.getMercentOrigins();
        	
        	if(origins.contains((String)originId)){
        		isMercentOrder = true;
        	}
        }
        if(originList.indexOf(originId)>=0 || isMercentOrder || (partnerMappingVO != null))
        {
          // keep the origin id
        }
        else
        {
          originId = null;
        }
      }
      
      //load email or letter titles based off of pocType and originid
      /*String attachedEmailIndicator = "N";
      if(extOrderNum!=null)
      {
        attachedEmailIndicator = "Y";
      }*/
      
      // Document msgXML = msgAccess.loadMessageTitlesXML(messageType, originId, companyId, NO, attachedEmailIndicator);
      // Check for preferred partner based on source code
      PartnerVO partnerVO = FTDCommonUtils.getPreferredPartnerBySource(sourceCode);       			
			
      Document msgXML = null;
			
	  if (partnerVO == null) {
		  // For partner orders check if the flag is 'ON'
		  if (partnerMappingVO != null) {
			  if (partnerMappingVO.getCreateStockEmail().equals("Y")) {
				  msgXML = msgAccess.loadMessageTitlesXML(messageType, originId, companyId, NO, NO, null);
				  logger.debug("create stock email is set to 'Y' for partner orders, show stock emails for origin " + originId);
			  } else {
				  msgXML = msgAccess.loadMessageTitlesXML(messageType, null, companyId, NO, NO, null) ;
				  logger.debug("create stock email is set to 'N' for partner orders, show FTD stock email titles");
			  }
		  } else {
			  msgXML = msgAccess.loadMessageTitlesXML(messageType, originId, companyId, NO, NO, null);
			  logger.debug("Show stock emails for origin " + originId);
		  }
	  } else {
		  msgXML = msgAccess.loadMessageTitlesXML(messageType, null, companyId, NO, NO, partnerVO.getPartnerName());
		  logger.debug("Show stock emails for partner " + partnerVO.getPartnerName());
	  }
      DOMUtil.addSection(responseDocument, msgXML.getChildNodes());
            
      if(actionType.equals(COMConstants.LOAD_MSG_CONTENT) || actionType.equals(COMConstants.LOAD_MSG_CONTENT_AJAX))
      {
        // Retrieve order data using OrderDAO to be used for the token replacement
        Document dataDocXML = this.getOrderDataDocumentXML(orderDetailId, includeComments, conn);
        
        // get csr_name from securityManager and append to dataDocXML
        if(SecurityManager.getInstance().getUserInfo(request.getParameter(COMConstants.SEC_TOKEN))!=null)
        {
          csrFirstName = SecurityManager.getInstance().getUserInfo(request.getParameter(COMConstants.SEC_TOKEN)).getFirstName();
          csrLastName = SecurityManager.getInstance().getUserInfo(request.getParameter(COMConstants.SEC_TOKEN)).getLastName();
          Document csrXML = this.toXML("<CSR><first_name>"+csrFirstName+"</first_name><last_name>"+csrLastName+"</last_name></CSR>");
          DOMUtil.addSection(dataDocXML, csrXML.getChildNodes());          
        }
        
		if(partnerMappingVO != null)
		{
        Node n = DOMUtil.selectSingleNode(dataDocXML,"/root/ORDERS/ORDER/external_order_number").getFirstChild();
		n.setTextContent(extOrderNum);
		originId = null;
		logger.debug("For partner orders override extOrderNum : "+extOrderNum);
		}
      
        
		
        // Create a PointOfCOntactVO setting the pocType, customerId, orderDetailId and companyId from
        // the request object and order dataDocument just created
        PointOfContactVO pocObj = new PointOfContactVO();
        pocObj.setPointOfContactType(messageType);
        pocObj.setCompanyId(companyId);
        pocObj.setDataDocument(dataDocXML);
        pocObj.setLetterTitle(request.getParameter(COMConstants.MESSAGE_TITLE));
        pocObj.setCommentType(messageType);
        pocObj.setTemplateId(messageId);
        pocObj.setOriginId(originId);
        
        
        // Build the message using StockMessageGenerator passing the POCVO and returning another POCVO
        // containing the merged body and subject
        StockMessageGenerator msgGen = new StockMessageGenerator();
        pocObj = msgGen.buildMessage(pocObj);
        
        // Create XML from the POC object and append to primary document
        String pocXMLString = pocObj.toXML();
        
        // Clean up any & chars
        pocXMLString = FieldUtils.replaceAll(pocXMLString, "&", "&amp;");
        
        // Generate XML
        Document pocXML = this.toXML(pocXMLString);
        
        
     // Send back error status document as XML.  The front end utilizes AJAX
        if(actionType.equals(COMConstants.LOAD_MSG_CONTENT_AJAX)){
        	DOMUtil.print(pocXML,response.getWriter());
        	return null;
        }
        else{
        	DOMUtil.addSection(responseDocument, pocXML.getChildNodes());
        }
        
      }    
        
      // Forward to transformation action
      //Get XSL File name
      File xslFile = getXSL(messageType, mapping);  
      ActionForward forward = mapping.findForward(messageType);
      String xslFilePathAndName = forward.getPath(); //get real file name
      
      // Change to client side transform
      TraxUtil.getInstance().getInstance().transform(request, response, responseDocument, 
                          xslFile, xslFilePathAndName, (HashMap)request.getAttribute(COMConstants.CONS_APP_PARAMETERS));
      return null;
    }
    catch (Throwable ex)
    {
      try
      {
        if(conn!=null)
          conn.close();
      }
      catch (SQLException se)
      {
        logger.error(se);
        return mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
      }
      logger.error(ex);
      return mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
    }
    finally 
    {
      try
      {
        if(conn!=null)
          conn.close();
      }
      catch (SQLException se)
      {
        logger.error(se);
        return mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
      }
    }  
  }
  
  public Document getOrderDataDocumentXML(String orderDetailId, String includeComments, Connection conn) throws Exception
  {
    //Instantiate OrderDAO
    OrderDAO orderDAO = new OrderDAO(conn);
    
    // pull base document
    Document primaryOrderXML = (Document) DOMUtil.getDocument();

    if(orderDetailId != null && !orderDetailId.equals(""))
    {
        //hashmap that will contain the output from the stored proc
        HashMap printOrderInfo = new HashMap();
    
        //Call getCartInfo method in the DAO
        printOrderInfo = orderDAO.getOrderInfoForPrint(orderDetailId, includeComments, COMConstants.CONS_PROCESSING_ID_CUSTOMER);
    
        //Create an Document and call a method that will transform the CachedResultSet from the cursor:
        //OUT_ORDERS_CUR into an master Document. 
        DOMUtil.addSection(primaryOrderXML, buildXML(printOrderInfo, "OUT_ORDERS_CUR", "ORDERS", "ORDER").getChildNodes());
        
        //OUT_ORDER_ADDON_CUR into an Document.  Append this a master XML document
        DOMUtil.addSection(primaryOrderXML, buildXML(printOrderInfo, "OUT_ORDER_ADDON_CUR", "ORDER_ADDONS", "ORDER_ADDON").getChildNodes());
        
        //OUT_ORDER_BILLS_CUR into an Document. Append this a master XML document
        DOMUtil.addSection(primaryOrderXML, buildXML(printOrderInfo, "OUT_ORDER_BILLS_CUR", "ORDER_BILLS", "ORDER_BILL").getChildNodes());
        
        //OUT_ORDER_REFUNDS_CUR into an Document. Append this a master XML document
        DOMUtil.addSection(primaryOrderXML, buildXML(printOrderInfo, "OUT_ORDER_REFUNDS_CUR", "ORDER_REFUNDS", "ORDER_REFUND").getChildNodes());
        
        //OUT_ORDER_TOTAL_CUR into an Document. Append this a master XML document
        DOMUtil.addSection(primaryOrderXML, buildXML(printOrderInfo, "OUT_ORDER_TOTAL_CUR", "ORDER_TOTALS", "ORDER_TOTAL").getChildNodes());
        
        //OUT_ORDER_PAYMENT_CUR into an Document. Append this a master XML document
        Document orderPaymentXML = buildXML(printOrderInfo, "OUT_ORDER_PAYMENT_CUR", "ORDER_PAYMENTS", "ORDER_PAYMENT");
        DOMUtil.addSection(primaryOrderXML, orderPaymentXML.getChildNodes());
        
        //Obtain all payment methods
        ArrayList paymentMethods = new ArrayList();
        String XPath = "//ORDER_PAYMENTS/ORDER_PAYMENT";
        NodeList nodeList = DOMUtil.selectNodes(orderPaymentXML,XPath);
        Node node = null;
        if (nodeList != null)
        {
         for (int i = 0; i < nodeList.getLength(); i++)
         {
           node = (Node) nodeList.item(i);
           if (node != null)
           {
             if (DOMUtil.selectSingleNode(node,"payment_type/text()") != null)
             {
               paymentMethods.add((DOMUtil.selectSingleNode(node,"payment_type/text()").getNodeValue()));
             }
           }        
         }
        }
        
        //Set payment method text
        Element pmElement = primaryOrderXML.createElement("payment_method_text");
        String paymentMethodTxt = ((PaymentMethodHandler)CacheManager.getInstance().getHandler("CACHE_NAME_PAYMENT_METHOD")).getText(paymentMethods, null);
        pmElement.appendChild(primaryOrderXML.createTextNode(paymentMethodTxt));
        Node rootNode = DOMUtil.selectSingleNode(primaryOrderXML,"root");
        rootNode.appendChild(pmElement);

        //CUST_CART_CUR into an Document. Append this a master XML document
        DOMUtil.addSection(primaryOrderXML, buildXML(printOrderInfo, "CUST_CART_CUR", "CUSTOMERS", "CUSTOMER").getChildNodes());
        
        //OUT_CUSTOMER_PHONE_CUR into an Document. Append this a master XML document
        DOMUtil.addSection(primaryOrderXML, buildXML(printOrderInfo, "OUT_CUSTOMER_PHONE_CUR", "CUSTOMER_PHONES", "CUSTOMER_PHONE").getChildNodes());
        
        //OUT_MEMBERSHIP_CUR into an Document. Append this a master XML document
        DOMUtil.addSection(primaryOrderXML, buildXML(printOrderInfo, "OUT_MEMBERSHIP_CUR", "MEMBERSHIPS", "MEMBERSHIP").getChildNodes());
        
        //OUT_TAX_REFUND_CUR into an Document. Append this a master XML document
        DOMUtil.addSection(primaryOrderXML, buildXML(printOrderInfo, "OUT_TAX_REFUND_CUR", "TAX_REFUNDS", "TAX_REFUND").getChildNodes());
        
        // append the current date to the document
        Date rightNow = new Date();
        String dateNow = DateFormat.getDateInstance(DateFormat.SHORT).format(rightNow);
        Document dateXML = this.toXML("<cur_date>"+dateNow+"</cur_date>");
        DOMUtil.addSection(primaryOrderXML, dateXML.getChildNodes());
        //primaryOrderXML.print(System.out);
    }
    
    return primaryOrderXML;
  }
  
/*******************************************************************************************
 * buildXML()
 *******************************************************************************************/
  private Document buildXML(HashMap outMap, String cursorName, String topName, String bottomName) 
        throws Exception
  {

    //Create a CachedResultSet object using the cursor name that was passed. 
    CachedResultSet rs = (CachedResultSet) outMap.get(cursorName);
    Document doc =  null;
    
    try
    {
        //Create an XMLFormat, and initialize the top and bottom node.  Note that since this method
        //can be/is called by various other methods, the top and botton names MUST be passed to it. 
        XMLFormat xmlFormat = new XMLFormat();
        xmlFormat.setAttribute("type", "element");
        xmlFormat.setAttribute("top", topName);
        xmlFormat.setAttribute("bottom", bottomName );
      
        //call the DOMUtil's converToXMLDOM method
        doc = (Document) DOMUtil.convertToXMLDOM(rs, xmlFormat);
    }
    finally
    {
    }
  
    //return the xml document. 
    return doc;
  
  }
  
  private Document toXML(String xmlString) throws Exception
  {
	return DOMUtil.getDocument(xmlString);
  }

  /******************************************************************************
  *                                     getXSL()
  *******************************************************************************
  * Retrieve name of Customer Order Search Action XSL file
  * @param1 String - the xsl name returned from the Business Object
  * @param2 ActionMapping
  * @return File - XSL File name
  * @throws none
  */

  private File getXSL(String xslName, ActionMapping mapping)
  {

    File xslFile = null;
    String xslFilePathAndName = null;

    ActionForward forward = mapping.findForward(xslName);
    xslFilePathAndName = forward.getPath();                //get real file name
    xslFile = new File(this.getServlet().getServletContext().getRealPath(xslFilePathAndName));
    return xslFile;
  }
  
  private String replace(String src, String oldPattern, String newPattern)   {
		
		String dst = "";		// the new bult up string based on src
		int i;					// index of found token
		int last = 0;			// last valid non token string data for concat	
		boolean done = false;	// determines if we're done.
		
		// while we'er not done, try finding and replacing
		while( !done)  {
				// search for the pattern...
			i = src.indexOf(oldPattern, last);
				// if it's not found from our last point in the src string....
			if( i == -1) {	
					// we're done.
				done = true;
					// if our last point, happens to be before the end of the string
				if( last < src.length())  {
						// concat the rest of the string to our dst string
					dst = dst.concat( src.substring(last, (src.length())) );
					}
				}
			else {
					// we found the pattern
				if( i != last) {
						// if the pattern's not at the very first char of our searching point....
						// we need to concat the text up to that point..
					dst = dst.concat( src.substring(last,i) );
					}
					// update our last var to our current found pattern, plus the lenght of the pattern
				last = i + oldPattern.length();
					// concat the new pattern to the dst string
				dst = dst.concat( newPattern);
				}
			}
			// finally, return the new string
		return dst;
		}

  /** Get order number as per the origin. If not FTD order, replace order number with the XX partner order number.
	 * @param tmpDetailVO
	 * @param orderOrigin
	 * @return
	 */
	private String getOrderNumber(String extOrdNum, String orderOrigin, Connection conn) {
		// Default value is FTD external order number.
		String orderNumber = extOrdNum;
		
		if(StringUtils.isEmpty(orderOrigin)) {
			logger.error("Unable to determine if the order is partner order, invalid order origin");
			return orderNumber;
		}

		// If partner order replace it with partner order number
		CachedResultSet result = new PartnerUtility()
				.getPtnOrderDetailByConfNumber(extOrdNum, orderOrigin, conn);
		if (result != null && result.next()) {
			if (!StringUtils.isEmpty(result.getString("PARTNER_ORDER_ITEM_NUMBER"))) {
				orderNumber = result.getString("PARTNER_ORDER_ITEM_NUMBER");
			}
		}
		return orderNumber;
	}
}