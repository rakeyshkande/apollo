package com.ftd.customerordermanagement.actions;


import com.ftd.customerordermanagement.bo.CcDeclineBO;
import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;

 
public final class CcDeclineAction extends Action 
{
    private static Logger logger  = new Logger("com.ftd.customerordermanagement.actions.CcDeclineAction");


  /**
   * This is the main action called from the Struts framework.
   * @param mapping The ActionMapping used to select this instance.
   * @param form The optional ActionForm bean for this request.
   * @param request The HTTP Request we are processing.
   * @param response The HTTP Response we are processing.
   */
  public  ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, 
                                HttpServletResponse response) 
          
  {
    ActionForward forward = null;
    String xslName = "ccDecline";
        
    try
    {
      //Invoke the BO and get the decline message.
      CcDeclineBO ccDeclineBO = new CcDeclineBO();
      Document declineMessageDoc = ccDeclineBO.getMessage(request.getParameter("pay_type"));      

      if(logger.isDebugEnabled())
      {
         //IMPORTANT:  Because StringWriter contents are NOT removed even when the flush
         //            method is executed, make StringWriter go away through scope control.
         //            Both the flush and close methods were tested with but were of none affect.
         StringWriter sw = new StringWriter();       //string representation of xml document
         DOMUtil.print((Document) declineMessageDoc,new PrintWriter(sw));
         logger.debug("- execute(): COM returned = \n" + sw.toString());
      }

      //Get XSL File name
      File xslFile = getXSL(xslName, mapping);	
  
      forward = mapping.findForward(xslName);
      String xslFilePathAndName = forward.getPath(); //get real file name
      
      // Change to client side transform
      TraxUtil.getInstance().getInstance().transform(request, response, declineMessageDoc, 
                          xslFile, xslFilePathAndName, (HashMap)request.getAttribute(COMConstants.CONS_APP_PARAMETERS));
      return null;
    }
    catch(Exception e)
    {
      logger.error(e);
      forward = mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
      return forward;
    }
  }
    
  /**
  * Retrieve name of Credit Card Decline Action XSL file
  * @param1 String - the xsl name  
  * @param2 ActionMapping
  * @return File - XSL File name
  * @throws none
  */

  private File getXSL(String xslName, ActionMapping mapping)
  {
    File xslFile = null;
    String xslFilePathAndName = null;

    ActionForward forward = mapping.findForward(xslName);
    xslFilePathAndName = forward.getPath();
    //get real file name
    xslFile = new File(this.getServlet().getServletContext().getRealPath(xslFilePathAndName));
    return xslFile;
  }
}