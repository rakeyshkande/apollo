package com.ftd.customerordermanagement.actions;

import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * This class redirects the user to the Error page. 
 *
 * @author Ali Lakhani
 */
public final class ErrorAction extends Action 
{
	private static    Logger logger  = new Logger("com.ftd.customerordermanagement.actions.ErrorAction");

    /**
     * This is the Error Action called from the Struts framework.
	 * The execute method redirects to the Error Page.
     * 
     * @param mapping  - ActionMapping used to select this instance
     * @param form     - ActionForm (optional) bean for this request
     * @param request  - HTTP Request we are processing
     * @param response - HTTP Response we are processing
     * @return forwarding action - next action to "process" request
     * @throws IOException
     * @throws ServletException
     */
	public  ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, 
		HttpServletResponse response) 
        throws IOException, ServletException		 
	{		
		ActionForward forward = null;

		try
		{
            forward = mapping.findForward("ErrorPage");
		}
		catch(Throwable t)
		{
			logger.error(t);			
		}
		
		return forward;
    }
    
}
