package com.ftd.customerordermanagement.actions;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.dao.LockDAO;
import com.ftd.customerordermanagement.dao.OrderDAO;
import com.ftd.ftdutilities.FTDCAMSUtils;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.security.SecurityManager;

import com.ftd.osp.utilities.vo.MembershipDetailsVO;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;

public final class CartEmailAction extends Action 
{
    private static Logger logger  = new Logger("com.ftd.customerordermanagement.actions.AddCommentsAction");
    private static String OUT_CUR = "OUT_CUR";
    private static String OUT_ID_COUNT = "OUT_ID_COUNT";
    private static String OUT_PARAMETERS = "out-parameters";
    private static String SUCCESS = "success";
    
  /**
   * This is the main action called from the Struts framework.
   * @param mapping The ActionMapping used to select this instance.
   * @param form The optional ActionForm bean for this request.
   * @param request The HTTP Request we are processing.
   * @param response The HTTP Response we are processing.
   */
   
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException
    {
        Connection conn = null;
        
        try
        {
            String actionType = request.getParameter(COMConstants.ACTION_TYPE);
            conn = DataSourceUtil.getInstance().getConnection(ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, COMConstants.DATASOURCE_NAME));
            
            if(actionType.equalsIgnoreCase("load"))
            {
                // Set request params
                String orderGuid = request.getParameter(COMConstants.ORDER_GUID);    
                String securityToken = (String) request.getParameter(COMConstants.SEC_TOKEN);
                long recordsToBeReturned = new Long(ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, COMConstants.CONS_MAX_CART_EMAIL_PER_PAGE)).longValue();
                long pagePosition = request.getParameter(COMConstants.PAGE_POSITION) != null?new Long(request.getParameter(COMConstants.PAGE_POSITION)).longValue():0;
                long startPosition = (pagePosition-1) * recordsToBeReturned + 1;
                
                // Get default document
                Document responseDocument = (Document) DOMUtil.getDocument();
                
                // Load email data
                OrderDAO orderDAO = new OrderDAO(conn);
                HashMap emailMap = orderDAO.loadCartEmailsXML(orderGuid, startPosition, recordsToBeReturned);
               
                // Build email xml
                Document xmlEmailAddresses = (Document) emailMap.get(OUT_CUR);
                DOMUtil.addSection(responseDocument, xmlEmailAddresses.getChildNodes());
                
                // Call the checkLockXML() on the LockDAO to return lock status XML data for specific ID.
                LockDAO lock = new LockDAO(conn);
                Document xmlLock = lock.retrieveLockXML(securityToken, SecurityManager.getInstance().getUserInfo(securityToken).getUserID(), orderGuid, COMConstants.CONS_ENTITY_TYPE_ORDERS);
                DOMUtil.addSection(responseDocument, xmlLock.getChildNodes());
                
                // Calculate total pages
                int totalCount = Integer.parseInt(DOMUtil.getNodeValue((Document) emailMap.get(OUT_PARAMETERS), OUT_ID_COUNT));
                int totalPages = (int) Math.floor(totalCount / recordsToBeReturned);
                if(totalCount%recordsToBeReturned > 0)
                {
                    totalPages += 1;
                }
                
                // Generate pageData
                HashMap secondaryMap = new HashMap();
                secondaryMap.put(COMConstants.PD_TOTAL_PAGES, new Integer(totalPages).toString());
                secondaryMap.put(COMConstants.PD_START_POSITION, new Long(startPosition).toString());
                secondaryMap.put(COMConstants.ORDER_GUID, orderGuid);
                secondaryMap.put(COMConstants.PAGE_POSITION, new Long(pagePosition).toString());
                secondaryMap.put(COMConstants.PREMIER_CIRCLE_MEMBERSHIP, request.getParameter(COMConstants.PREMIER_CIRCLE_MEMBERSHIP));
                DOMUtil.addSection(responseDocument, "pageData", "data", secondaryMap, true);
                                
                // Generate page
                String xslName = "UpdateCartEmail";
                File xslFile = getXSL(xslName, mapping);  
                ActionForward forward = mapping.findForward(xslName);
                String xslFilePathAndName = forward.getPath(); //get real file name
                
                // Change to client side transform
                TraxUtil.getInstance().getInstance().transform(request, response, responseDocument, 
                                    xslFile, xslFilePathAndName, (HashMap)request.getAttribute(COMConstants.CONS_APP_PARAMETERS));
            }
            else if(actionType.equalsIgnoreCase("update"))
            {
                // Get default document
                Document responseDocument = (Document) DOMUtil.getDocument();
                
                // Set request params
                String orderGuid = request.getParameter(COMConstants.ORDER_GUID);    
                String emailAddress = request.getParameter(COMConstants.EMAIL_ADDRESS);
                String securityToken = request.getParameter(COMConstants.SEC_TOKEN);
                
                // Load email data
                OrderDAO orderDAO = new OrderDAO(conn);
                orderDAO.updateCartEmail(orderGuid, emailAddress, SecurityManager.getInstance().getUserInfo(securityToken).getUserID());
            
                if( !Boolean.valueOf(request.getParameter(COMConstants.PREMIER_CIRCLE_MEMBERSHIP)) ){
	                //call CAMS to check if the email address is Premier Circle
	                Map<String, MembershipDetailsVO> membershipMap = new HashMap<String, MembershipDetailsVO>();
	                Date now = new Date();
	                Calendar calendar = Calendar.getInstance();
	                calendar.setTime(now);
	                now = calendar.getTime();
	
	                	List<String> emailList = new ArrayList<String>();
	                	emailList.add(emailAddress);
	                	membershipMap = FTDCAMSUtils.getMembershipData(emailList, now);

	                	MembershipDetailsVO pcMemDetails = null;
		                if(membershipMap != null && membershipMap.containsKey(FTDCAMSUtils.PREMIERCIRCLE_MEMBER_TYPE)){
		            			pcMemDetails = membershipMap.get(FTDCAMSUtils.PREMIERCIRCLE_MEMBER_TYPE);
		            	}
		            if(pcMemDetails != null && "Y".equalsIgnoreCase(pcMemDetails.getStatus())){
		            	orderDAO.updatePremierCircleInfo(orderGuid, pcMemDetails, SecurityManager.getInstance().getUserInfo(securityToken).getUserID());
		            }
                }
                // Generate page
                String xslName = "UpdateCartEmailIFrame";
                File xslFile = getXSL(xslName, mapping);
                ActionForward forward = mapping.findForward(xslName);
                String xslFilePathAndName = forward.getPath(); //get real file name
                
                // Change to client side transform
                TraxUtil.getInstance().getInstance().transform(request, response, responseDocument, 
                                    xslFile, xslFilePathAndName, (HashMap)request.getAttribute(COMConstants.CONS_APP_PARAMETERS));
                
            }
            else
            {
                //forward to error page
                logger.error("Missing action_type in UpdateCartEmailAction");
                return mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
            }
            
            return null;
        }
        catch (Throwable ex)
        {
            logger.error(ex);
            return mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
        }
        finally 
        {
            try
            {
                if(conn!=null)
                {
                    conn.close();
                }
            }
            catch (SQLException se)
            {
                logger.error(se);
                return mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
            }
        }
    }
    
    private void loadCartEmailAddresses()
    {
        
    }
    
    private void updateCartEmailAddress()
    {
        
    }
    
    /******************************************************************************
    *                                     getXSL()
    *******************************************************************************
    * Retrieve name of Customer Order Search Action XSL file
    * @param1 String - the xsl name returned from the Business Object
    * @param2 ActionMapping
    * @return File - XSL File name
    * @throws none
    */
    
    private File getXSL(String xslName, ActionMapping mapping)
    {
        File xslFile = null;
        String xslFilePathAndName = null;
        
        ActionForward forward = mapping.findForward(xslName);
        xslFilePathAndName = forward.getPath(); 
        xslFile = new File(this.getServlet().getServletContext().getRealPath(xslFilePathAndName));
        return xslFile;
    }
  
}