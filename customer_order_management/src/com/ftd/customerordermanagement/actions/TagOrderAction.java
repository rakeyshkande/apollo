package com.ftd.customerordermanagement.actions;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;

import com.ftd.customerordermanagement.bo.TagBO;
import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.dao.TagDAO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;

/**
 * The TagOrderAction will obtain tag disposition and priority codes from the TagDAO
 * to display on the Tag Order Screen and utilize the TagBO and TagDAO to tag an 
 * order once a tag disposition and priority code have been chosen.
 *
 * @author Matt Wilcoxen
 */

public class TagOrderAction extends Action 
{
	private Logger logger = new Logger("com.ftd.customerordermanagement.actions.TagOrderAction");
    
    //request parameters:
	private static final String R_ACTION = "action";
	private static final String R_CSR_ID = "csr_id";
	private static final String R_ORDER_DETAIL_ID = "order_detail_id";
	private static final String R_TAG_DISPOSITION = "tag_disposition";
	private static final String R_TAG_PRIORITY = "tag_priority";

    //XML nodes:
    private static final String X_ROOT = "root";
    private static final String X_ORDER_DETAIL_ID = "order_detail_id";
    private static final String X_OUT_PARAMETERS = "out-parameters";
    private static final String X_TAG_STATUS = "TAG_STATUS";
    private static final String X_ERR_MSG = "ERR_MSG";

    private static final String TAG_ORDER = "tag_order";
    private static final String ERROR_PAGE = "errorPage";


	/**
	 * This is the Tag Order Action called from the Struts framework.
     *     1. get db connection
     *     2. get parameters
     *     3. if requested action is tag action
     *            check if the order:
     *              - is already tagged to the csr, or
     *              - has multiple tags
     *     4. else
     *            display Tag Order Action page
	 * 
	 * @param mapping            - ActionMapping used to select this instance
	 * @param form               - ActionForm (optional) bean for this request
	 * @param request            - HTTP Request we are processing
	 * @param response           - HTTP Response we are processing
     * 
	 * @return forwarding action - next action to "process" request
     * 
	 * @throws IOException
	 * @throws ServletException
	 */
	public  ActionForward execute(ActionMapping mapping, ActionForm form, 
                                  HttpServletRequest request, HttpServletResponse response)
		throws IOException, ServletException
	{
        Connection conn = null;
        ConfigurationUtil cu = null;

        String security = "";
        String securityToken  = null;
        String context  = "";
        String action   = "";
        String csrId = null;

        Document tagOrderErrorsXMLDoc = null;
        Document tagOrderXMLDoc = null;

        ActionForward forward = null;
		boolean callErrorPage = false;
        String errorMessage = null;
        File xslFile = null;
        HashMap parameters = new HashMap();

		try
		{
            cu = ConfigurationUtil.getInstance();

            // 1. get db connection
			conn = getDBConnection();
            // 2. get parameters
            context = request.getParameter(COMConstants.CONTEXT);
            action = request.getParameter(R_ACTION);

            //get additional parameters from request
            parameters = (HashMap) request.getAttribute(COMConstants.CONS_APP_PARAMETERS);
            if(parameters == null)
            {
                parameters = new HashMap();
            }

            // 3. if requested action is tag action, check for multiple tags
            if(action != null  &&  action.equalsIgnoreCase(TAG_ORDER))
            {
                tagOrderErrorsXMLDoc = (Document) tagOrder(request, securityToken, conn, context, action, tagOrderErrorsXMLDoc);
                xslFile = getXSL(mapping, request, COMConstants.XSL_MULTIPLE_TAGS_ASSIGNED);
                forward = mapping.findForward(COMConstants.XSL_MULTIPLE_TAGS_ASSIGNED);
                String xslFilePathAndName = forward.getPath(); //get real file name
                
                // Change to client side transform
                TraxUtil.getInstance().getInstance().transform(request, response, tagOrderErrorsXMLDoc, 
                                    xslFile, xslFilePathAndName, (HashMap)request.getAttribute(COMConstants.CONS_APP_PARAMETERS));
                return null;
            }
            else
            {
                //display Tag Order Action screen
                tagOrderXMLDoc = (Document) buildTagData(request, securityToken, conn, context, action, tagOrderXMLDoc);
                xslFile = getXSL(mapping, request, COMConstants.XSL_ASSIGN_TAG_DISPOSITION_PRIORITY);
                forward = mapping.findForward(COMConstants.XSL_ASSIGN_TAG_DISPOSITION_PRIORITY);
                String xslFilePathAndName = forward.getPath(); //get real file name
                
                // Change to client side transform
                TraxUtil.getInstance().getInstance().transform(request, response, tagOrderXMLDoc, 
                                    xslFile, xslFilePathAndName, (HashMap)request.getAttribute(COMConstants.CONS_APP_PARAMETERS));
                return null;
            }
		}
		catch (ClassNotFoundException  cnfe)
		{
            errorMessage = cnfe.getMessage();
			logger.error(cnfe);
            callErrorPage = true;
        }
		catch (IOException  ioe)
		{
            errorMessage = ioe.getMessage();
			logger.error(ioe);
            callErrorPage = true;
		}
		catch (ParserConfigurationException  pcex)
		{
            errorMessage = pcex.getMessage();
			logger.error(pcex);
            callErrorPage = true;
		}
		catch (SAXException  sec)
		{
            errorMessage = sec.getMessage();
			logger.error(sec);
            callErrorPage = true;
		}
		catch (SQLException  sqle)
		{
            errorMessage = sqle.getMessage();
			logger.error(sqle);
            callErrorPage = true;
		}
		catch (TransformerException  tex)
		{
            errorMessage = tex.getMessage();
			logger.error(tex);
            callErrorPage = true;
		}
		catch (Throwable  t)
		{
            errorMessage = t.getMessage();
			logger.error(t);
            callErrorPage = true;
		}
		finally
		{
            try
            {
				if(conn != null)
				{
	                conn.close();
				}
                if(callErrorPage)
                {
                    Document xmlDoc = createOutParms(errorMessage);
                    TraxUtil.getInstance().transform(request, response, xmlDoc, xslFile, null);
                    return null;
                }
            }
            catch (SQLException  se)
            {
                logger.error(se);
            }
            catch (ParserConfigurationException  pcex)
            {
                logger.error(pcex);
            }
            catch (TransformerException  tex)
            {
                logger.error(tex);
            }
            catch (Exception  ex)
            {
                logger.error(ex);
            }
		}

		return null;
	}


	/**
	 * Tag order to csr.
     *     1. retrieve request parameters
     *     2. build multiple tags document root
     *     3. tag order with hashmap boolean values
     *            <root>
     *                <multiple_tags>Y</multiple_tags>
     *                <order_already_tagged_to_csr>N</order_already_tagged_to_csr>
     *            </root>
	 * 
	 * @param HttpServletRequest - request
	 * @param String             - security token
	 * @param Connection         - database connection
	 * @param String             - context
	 * @param String             - action
	 * @param Document        - document of possible db errors

	 * @param Document        - xml document as detailed above
     * 
     * @throws Exception
     * @throws IOException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws SQLException
     * @throws TransformerException
     * @throws XSLException
	 */
	private Document tagOrder(HttpServletRequest request, String securityToken, Connection conn, 
            String context, String action, Document tagOrderErrorsXMLDoc)
        throws IOException, ParserConfigurationException, SAXException, SQLException,
               TransformerException, Exception
    {
        // 1. retrieve request parameters
        securityToken = request.getParameter(COMConstants.SEC_TOKEN);
        String orderDetailId  = request.getParameter(R_ORDER_DETAIL_ID);
        String tagDisposition = request.getParameter(R_TAG_DISPOSITION);
        String tagPriority = request.getParameter(R_TAG_PRIORITY);

        logger.debug("tagOrder(): request parms are: \n" +
            "context = " + context + "\n" +
            "action = " + action + "\n" +
            "securitytoken = " + securityToken + "\n" +
            "order_detail_id = " + orderDetailId + "\n" +
            "tag_disposition = " + tagDisposition + "\n" +
            "tag_priority = " + tagPriority);

        // 2. build multiple tags document root
        tagOrderErrorsXMLDoc = (Document) DOMUtil.getDefaultDocument();
        Element root = tagOrderErrorsXMLDoc.createElement(X_ROOT);
        tagOrderErrorsXMLDoc.appendChild(root);

        Element newEle = null;
        Text newText = null;

        // 3. tag order with hashmap boolean values
        TagBO tBO = new TagBO(conn);
        HashMap tagResults = tBO.tagOrder(tagDisposition,tagPriority,securityToken,orderDetailId);
        
        Set tagKeys = tagResults.keySet();
        Iterator iTagKeys = tagKeys.iterator();
        while(iTagKeys.hasNext())
        {
            String tagKey = (String) iTagKeys.next();
            Boolean bTagResult = (Boolean) tagResults.get(tagKey);
            boolean tagResult = bTagResult.booleanValue();

            newEle = tagOrderErrorsXMLDoc.createElement(tagKey);
            newText = tagOrderErrorsXMLDoc.createTextNode(tagResult == true  ? "Y"  : "N");
            newEle.appendChild(newText);

            tagOrderErrorsXMLDoc.getDocumentElement().appendChild(newEle);
        }
        
        if(logger.isDebugEnabled())
        {
            //IMPORTANT:  Because StringWriter contents are NOT removed even when the flush
            //            method is executed, make StringWriter go away through scope control.
            //            Both the flush and close methods were tested with but were of non-affect.
            StringWriter sw = new StringWriter();       //string representation of xml document
            DOMUtil.print(tagOrderErrorsXMLDoc,new PrintWriter(sw));
            logger.debug("tagOrder(): document = \n" + sw.toString());
        }

        return tagOrderErrorsXMLDoc;
    }


	/**
	 * Build the document of tag dispositions and tag priority codes.
     *     1. retrieve request parameters
     *     2. retrive tag disposition codes, tag priority codes and csr ids
     *     3. build tag description & priority codes document
     *            <root>
     *                <disposition>
     *                    <disposition_code>
     *                        <tag_disposition>DCON</tag_disposition>
     *                        <description>Delivery Confirmation</description>
     *                    </disposition_code>
     *                    �* many disposition_code nodes will be returned
     *                </disposition>
     *                <priority>
     *                    <priority_code>
     *                        <tag_priority>30M</tag_disposition>
     *                        <description>30 Minutes</description>
     *                    </priority_code>
     *                    �* many priority_code nodes will be returned
     *                </priority>
     *                <order_tag>
     *                    <csr_id>tlayne</csr_id>
     *                    �* many csr_id nodes will be returned
     *                </order_tag>
     *                <order_detail_id>126550</order_detail_id>
     *            </root>
	 * 
	 * @param HttpServletRequest - request
	 * @param String             - security token
	 * @param Connection         - database connection
	 * @param String             - context
	 * @param String             - action
	 * @param Document        - xml document as detailed above
     * 
     * @throws IOException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws SQLException
	 */
	private Document buildTagData(HttpServletRequest request, String securityToken, 
            Connection conn, String context, String action, Document tagOrderXMLDoc)
        throws IOException, ParserConfigurationException, SAXException, SQLException, Exception
    {
        // 1. retrieve request parameters
        String orderDetailId = request.getParameter(R_ORDER_DETAIL_ID);

        logger.debug("buildTagData(): request parms are: \n" +
            "context = " + context + "\n" +
            "action = " + action + "\n" +
            "securitytoken = " + securityToken + "\n" +
            "order_detail_id = " + orderDetailId);

        // 2. retrive tag disposition codes, tag priority codes and csr ids
        TagBO tBO = new TagBO(conn);

        Document tagDispositionCodesXMLDoc = (Document) tBO.getDispositionCodes();
        if(logger.isDebugEnabled())
        {
            //IMPORTANT:  Because StringWriter contents are NOT removed even when the flush
            //            method is executed, make StringWriter go away through scope control.
            //            Both the flush and close methods were tested with but were of non-affect.
            StringWriter sw = new StringWriter();       //string representation of xml document
            DOMUtil.print(tagDispositionCodesXMLDoc,new PrintWriter(sw));
            logger.debug("buildTagData(): tagDispositionCodesXMLDoc = \n" + sw.toString());
        }

        Document tagPriorityCodesXMLDoc = (Document) tBO.getPriorityCodes();
        if(logger.isDebugEnabled())
        {
            //IMPORTANT:  Because StringWriter contents are NOT removed even when the flush
            //            method is executed, make StringWriter go away through scope control.
            //            Both the flush and close methods were tested with but were of non-affect.
            StringWriter sw = new StringWriter();       //string representation of xml document
            DOMUtil.print(tagPriorityCodesXMLDoc,new PrintWriter(sw));
            logger.debug("buildTagData(): tagPriorityCodesXMLDoc = \n" + sw.toString());
        }

        TagDAO tDAO = new TagDAO(conn);
        Document tagCSRIdsXMLDoc = (Document) tDAO.getCsrIds(orderDetailId);
        if(logger.isDebugEnabled())
        {
            //IMPORTANT:  Because StringWriter contents are NOT removed even when the flush
            //            method is executed, make StringWriter go away through scope control.
            //            Both the flush and close methods were tested with but were of non-affect.
            StringWriter sw = new StringWriter();       //string representation of xml document
            DOMUtil.print(tagCSRIdsXMLDoc,new PrintWriter(sw));
            logger.debug("buildTagData(): tagCSRIdsXMLDoc = \n" + sw.toString());
        }

        // 3. build tag description & priority codes document
        Element newEle = null;
        Text newText = null;

        tagOrderXMLDoc = (Document) DOMUtil.getDefaultDocument();
        Element root = tagOrderXMLDoc.createElement(X_ROOT);
        tagOrderXMLDoc.appendChild(root);

        tagOrderXMLDoc.getDocumentElement().appendChild(tagOrderXMLDoc.importNode(tagDispositionCodesXMLDoc.getDocumentElement(),true));
        tagOrderXMLDoc.getDocumentElement().appendChild(tagOrderXMLDoc.importNode(tagPriorityCodesXMLDoc.getDocumentElement(),true));
        tagOrderXMLDoc.getDocumentElement().appendChild(tagOrderXMLDoc.importNode(tagCSRIdsXMLDoc.getDocumentElement(),true));

        newEle = tagOrderXMLDoc.createElement(X_ORDER_DETAIL_ID);
        newText = tagOrderXMLDoc.createTextNode(orderDetailId);
        newEle.appendChild(newText);
        tagOrderXMLDoc.getDocumentElement().appendChild(newEle);
        
        if(logger.isDebugEnabled())
        {
            //IMPORTANT:  Because StringWriter contents are NOT removed even when the flush
            //            method is executed, make StringWriter go away through scope control.
            //            Both the flush and close methods were tested with but were of non-affect.
            StringWriter sw = new StringWriter();       //string representation of xml document
            DOMUtil.print(tagOrderXMLDoc,new PrintWriter(sw));
            logger.debug("buildTagData(): document = \n" + sw.toString());
        }

        return tagOrderXMLDoc;
    }


	/**
	 * Obtain connectivity with the database
	 * 
	 * @param none
     * 
	 * @return Connection - db connection
     * 
     * @throws Exception
     * @throws IOException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws TransformerException
     * @throws XSLException
	 */
	private Connection getDBConnection()
		throws IOException, ParserConfigurationException, SAXException, TransformerException, 
               Exception
	{
		Connection conn = null;
		conn = DataSourceUtil.getInstance().getConnection(
				   ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE,
															   COMConstants.DATASOURCE_NAME));

		return conn;
	}


	/**
	 * Retrieve name and location of XSL file
     *     1. get file lookup name
     *     2. get real file name
     *     3. get xsl file location
	 * 
	 * @param ActionMapping      - Stuts mapping used for actionforward lookup
     * @param HttpServletRequest - user request
	 * @param String             - name of xsl file
     * 
	 * @return File              - XSL File name with real path
     * 
	 * @throws none
	 */
    public File getXSL(ActionMapping mapping, HttpServletRequest request, String tag_order_file)
	{
		File XSLFile = null;
		String XSLFileLookUpName = null;
		String XSLFileName = null;

        // 1. get file lookup name
        XSLFileLookUpName = tag_order_file;

        // 2. get real file name
        ActionForward forward = mapping.findForward(XSLFileLookUpName);
        XSLFileName = forward.getPath();

        // 3. get xsl file location
        XSLFile = new File(this.getServlet().getServletContext().getRealPath(XSLFileName));
        return XSLFile;
    }


    /**
     * Due to an exception occuring earlier in the program, create a limited set of out parameters.
     *     <out-parameters>
     *         <TAG_STATUS>N</TAG_STATUS>
     *         <ERR_MSG>error-specific text</ERR_MSG>
     *     </out-parameters>
     * 
     *     1. create default document
     *     2. create error indicator element
     *     3. create error message element
     *
     * @param none    - error message
     * 
     * @return Object - Document of out-parameters of error page
     * 
     * @throws ParserConfigurationException
     */
    public Document createOutParms(String errMsg)
        throws ParserConfigurationException
    {
        Document xmlDoc = null;

        // 1. create default document
        xmlDoc = (Document) DOMUtil.getDefaultDocument();
        Element eleRoot = (Element) xmlDoc.createElement(X_OUT_PARAMETERS);
        xmlDoc.appendChild(eleRoot);
        
        // 2. create error indicator element
        createDocElement(xmlDoc, X_TAG_STATUS, "N");

        // 3. create error message element
        createDocElement(xmlDoc, X_ERR_MSG, errMsg);

        return xmlDoc;
    }


    /**
     * Add a particular element and value to a passed document.
     *
     * @param  Document - xml root document
     * @param  String      - name of element to attach to document
     * @param  String      - text of element to attach to document
     * 
     * @return Document - updated Document of out-parameters
     * 
     * @throws none
     */
    public Document createDocElement(Document xmlDoc, String eleName, String eleText)
    {
        Element newEle = xmlDoc.createElement(eleName);
        Text newText = xmlDoc.createTextNode(eleText);
        newEle.appendChild(newText);

        xmlDoc.getDocumentElement().appendChild(newEle);

        return xmlDoc;
    }

}
