package com.ftd.customerordermanagement.actions;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.dao.DnisDAO;
import com.ftd.customerordermanagement.dao.LockDAO;
import com.ftd.customerordermanagement.vo.DnisVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityManager;

import java.io.File;
import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;

/**
 * EditDnisAction class
 * This action performs server-side validation of DNIS data edited by the client, update/insert/delete
 * of the DNIS data in the database
 * If the action type is Edit or Delete the appropriate db lock is retrieved and sent to the client
 * After the update or insert or delete operation is successful, the lock is released
 * 
 */

public final class EditDnisAction extends Action 
{
    private static Logger logger  = new Logger("com.ftd.customerordermanagement.actions.EditDnisAction");
    private static String OUT_CUR = "OUT_CUR";
    private static String OUT_ID_COUNT = "OUT_ID_COUNT";
    private static String OUT_PARAMETERS = "out-parameters";
    private static String OUT_LOCK_OBTAINED = "OUT_LOCK_OBTAINED";
    private static String OUT_LOCKED_IND = "OUT_LOCKED_IND";
    private static String OUT_UPDATE_IND = "OUT_UPDATE_IND";
    private static String SUCCESS = "success";
    private static String FAILURE = "failure";
    private static String YES = "Y";
    private static String NO = "N";
    private static String ON = "on";
    private static String VALIDATION_SUCCESS = "VALIDATION_SUCCESS";
  
  /**
   * This is the main action called from the Struts framework.
   * @param mapping The ActionMapping used to select this instance.
   * @param form The optional ActionForm bean for this request.
   * @param request The HTTP Request we are processing.
   * @param response The HTTP Response we are processing.
   */
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException
  {
    String dnisId = null;
    String currentDnisId = null;
    String actionType = null;
    HashMap secondaryMap = new HashMap();
    int maxRecords = 0;
    Connection conn = null;
    DnisVO dnisObj = new DnisVO();
    String sourceCode = null;
    String userId = null;
    
    try
    {
        // pull base document
        Document responseDocument = (Document) DOMUtil.getDocument();
        
        //get all the parameters in the request object
        // get action_type
        if(request.getParameter(COMConstants.ACTION_TYPE)!=null)
        {
          actionType = request.getParameter(COMConstants.ACTION_TYPE);
          secondaryMap.put(COMConstants.ACTION_TYPE, actionType);
        }
        else
        {
          logger.error("Missing action_type parameter in request object");
          return mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
        }
        // dnis id
        if(request.getParameter(COMConstants.DNIS_ID)!=null)
        {
          dnisId = request.getParameter(COMConstants.DNIS_ID);
          dnisObj.setDnisId(Long.parseLong(dnisId));
          secondaryMap.put(COMConstants.DNIS_ID, dnisId);
        }
        else
        {
          logger.error("Missing Dnis ID in request");
          return mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
        }
        
        // get description
        if(request.getParameter(COMConstants.DNIS_DESCRIPTION)!=null)
        {
          dnisObj.setDescription(request.getParameter(COMConstants.DNIS_DESCRIPTION));
        }
        // get source_code
        if(request.getParameter(COMConstants.DNIS_SOURCE_CODE)!=null)
        {
          sourceCode = request.getParameter(COMConstants.DNIS_SOURCE_CODE);
          dnisObj.setDefaultSourceCode(sourceCode);
        }
        // get origin
        if(request.getParameter(COMConstants.DNIS_ORIGIN)!=null)
        {
          dnisObj.setOrigin(request.getParameter(COMConstants.DNIS_ORIGIN));
        }
        // get status_flag
        if(request.getParameter(COMConstants.DNIS_STATUS)!=null)
        {
          dnisObj.setStatusFlag(request.getParameter(COMConstants.DNIS_STATUS));
        }
        // get oe_flag
        if(request.getParameter(COMConstants.DNIS_OE_FLAG)!=null)
        {
          dnisObj.setOeFlag(request.getParameter(COMConstants.DNIS_OE_FLAG));
        }
        // get yellow_pages_flag
        if(request.getParameter(COMConstants.DNIS_YP_FLAG)!=null)
        {
          dnisObj.setYellowPagesFlag(request.getParameter(COMConstants.DNIS_YP_FLAG));
        }
        // get script id
        if(request.getParameter(COMConstants.DNIS_SCRIPT_ID)!=null)
        {
          dnisObj.setScriptId(request.getParameter(COMConstants.DNIS_SCRIPT_ID));
        }
        // get country script
        if(request.getParameter(COMConstants.DNIS_COUNTRY_SCRIPT_ID)!=null)
        {
          dnisObj.setScriptCode(request.getParameter(COMConstants.DNIS_COUNTRY_SCRIPT_ID));
        }
                                                
        // get database connection
        conn =
        DataSourceUtil.getInstance().getConnection(ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE,
                                                    COMConstants.DATASOURCE_NAME));
        
        // set up session id, user id, lock id, entity id, entity type and call retrieve lock on LockDAO
        String sessionId = (String) request.getParameter(COMConstants.SEC_TOKEN);;
        if (SecurityManager.getInstance().getUserInfo(sessionId)!=null)
        {
          userId = SecurityManager.getInstance().getUserInfo(sessionId).getUserID();
        }
        String lockId = dnisId;
        
        // if db is locked, return an iFrame without saving DNIS
        LockDAO dnisLock =  new LockDAO(conn);
        Document xmlLock = dnisLock.retrieveLockXML(sessionId, userId, lockId, COMConstants.CONS_ENTITY_TYPE_DNIS);
        // check if lock obtained in N. Then, forward without saving DNIS.
        if(DOMUtil.getNodeValue(xmlLock, OUT_LOCK_OBTAINED).equalsIgnoreCase(NO))
        {
          // Append the XML returned from retrieveLock() to the primary document
          logger.debug("CSR lock is not available for editing or adding DNIS.");
          DOMUtil.addSection(responseDocument, xmlLock.getChildNodes());          
                      
          // Forward to iFrame XSL action
          //Get XSL File name
          String xslName = "DnisLockedForSave";
          File xslFile = getXSL(xslName, mapping);  
          ActionForward forward = mapping.findForward(xslName);
          String xslFilePathAndName = forward.getPath(); //get real file name
          
          // Change to client side transform
          TraxUtil.getInstance().getInstance().transform(request, response, responseDocument, 
                              xslFile, xslFilePathAndName, (HashMap)request.getAttribute(COMConstants.CONS_APP_PARAMETERS));
          return null;
        }
                
        DnisDAO dnisAccess = new DnisDAO(conn);
        HashMap errorMessage = validateDnis(dnisAccess, actionType, dnisId, sourceCode);
        DOMUtil.addSection(responseDocument, "validation", "error", errorMessage, true); 
        if(((String)errorMessage.get(VALIDATION_SUCCESS)).equals(YES))
        {
          if(actionType.equals(COMConstants.UPDATE_DNIS))
          {
            dnisAccess.updateDnis(dnisId, dnisObj);
          }
          else
          if(actionType.equals(COMConstants.INSERT_DNIS))
          {
            dnisAccess.insertDnis(dnisObj);
          }
          // release db lock
          dnisLock.releaseLock(sessionId, userId, lockId, COMConstants.CONS_ENTITY_TYPE_DNIS);
        }
        //set an attribute in request object to indicate submit action
        request.setAttribute("SUBMIT_ACTION", YES);
        
        // Generate XML from secondary HashMap using the DOMUtil HashMap to XML conversion method addSection() .
        DOMUtil.addSection(responseDocument, "pageData", "data", secondaryMap, true);
        //responseDocument.print(System.out);
                
        //set the errorMessage hash map to the request object
        request.setAttribute("errorMessage", errorMessage);
      
        //redirect to loadDnisEditAction servlet to send the edit DNIS parameters to EditDNIS.xsl
        return mapping.findForward(FAILURE);
    }
    catch (Throwable ex)
    {
      try
      {
        if(conn!=null)
          conn.close();
      }
      catch (SQLException se)
      {
        logger.error(se);
          return mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
      }
      logger.error(ex);
      return mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
    }
    finally 
    {
      try
      {
        if(conn!=null)
          conn.close();
      }
      catch (SQLException se)
      {
        logger.error(se);
        return mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
      }
    }  
  }
  
  // private method to validate DNIS data
  private HashMap validateDnis(DnisDAO dnisAccess, String actionType, String dnisId, String sourceCode) throws Exception
  {
    boolean errFound = false;
    
    // use a hash map for storing all the error messages
    HashMap errorMessage = new HashMap();
    
    // Validate the DNIS data entered on the EditDNIS.xsl page
    if(actionType.equals(COMConstants.UPDATE_DNIS))
    {
      if(!dnisAccess.dnisExists(dnisId))
      {
        // non-existent DNIS in db
        errorMessage.put("MISSING_DNIS", "DNIS does not exist."+" ("+dnisId+")");
        errFound = true;
      }
      if(!dnisAccess.sourceExists(sourceCode))
      {
        // non-existent source code
        errorMessage.put("INVALID_SOURCE", "Source code not found."+" ("+sourceCode+")");
        errFound = true;
      }
      if(errFound)
      {
        errorMessage.put(VALIDATION_SUCCESS, NO);
      }
      else
      {
        errorMessage.put(VALIDATION_SUCCESS, YES);
      }
    }
    else
    if(actionType.equals(COMConstants.INSERT_DNIS))
    {
      // If action type is Insert
      if(dnisAccess.dnisExists(dnisId))
      {
        // duplicate DNIS in db
        errorMessage.put("INVALID_DNIS", "DNIS already exists."+" ("+dnisId+")");
        errFound = true;
      }
      if(!dnisAccess.sourceExists(sourceCode))
      {
        // non-existent source code
        errorMessage.put("INVALID_SOURCE", "Source code not found."+" ("+sourceCode+")");
        errFound = true;
      }
      if(errFound)
      {
        errorMessage.put(VALIDATION_SUCCESS, NO);
      }
      else
      {
        errorMessage.put(VALIDATION_SUCCESS, YES);
      }
    }
    return errorMessage;
  }

  /******************************************************************************
  *                                     getXSL()
  *******************************************************************************
  * Retrieve name of Customer Order Search Action XSL file
  * @param1 String - the xsl name returned from the Business Object
  * @param2 ActionMapping
  * @return File - XSL File name
  * @throws none
  */

  private File getXSL(String xslName, ActionMapping mapping)
  {

    File xslFile = null;
    String xslFilePathAndName = null;

    ActionForward forward = mapping.findForward(xslName);
    xslFilePathAndName = forward.getPath();                //get real file name
    xslFile = new File(this.getServlet().getServletContext().getRealPath(xslFilePathAndName));
    return xslFile;
  }
}