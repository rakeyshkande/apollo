package com.ftd.customerordermanagement.actions;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.dao.CommentDAO;
import com.ftd.customerordermanagement.dao.OrderDAO;
import com.ftd.customerordermanagement.dao.StockMessageDAO;
import com.ftd.customerordermanagement.vo.CommentsVO;
import com.ftd.ftdutilities.FTDCommonUtils;
import com.ftd.ftdutilities.FieldUtils;
import com.ftd.ftdutilities.GeneralConstants;
import com.ftd.messagegenerator.StockMessageGenerator;
import com.ftd.messagegenerator.constants.MessageConstants;
import com.ftd.messagegenerator.dao.MessageGeneratorDAO;
import com.ftd.messagegenerator.vo.PointOfContactVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.PartnerUtility;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.PartnerVO;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.PartnerMappingVO;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityManager;
import com.ftd.security.cache.vo.UserInfo;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.fop.apps.Driver;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
//import org.apache.avalon.framework.logger.Logger;



/**
 * SendMessageAction class
 * 
 */
public final class SendMessageAction extends Action 
{
    private static Logger logger  = new Logger("com.ftd.customerordermanagement.actions.SendMessageAction");
    private static String OUT_CUR = "OUT_CUR";
    private static String OUT_ID_COUNT = "OUT_ID_COUNT";
    private static String OUT_PARAMETERS = "out-parameters";
    private static String YES = "Y";
    private static String NO = "N";
    private static String OUTGOING_MESSAGE = "O";
    private static String ON = "on";
  
  /**
   * This is the main action called from the Struts framework.
   * @param mapping The ActionMapping used to select this instance.
   * @param form The optional ActionForm bean for this request.
   * @param request The HTTP Request we are processing.
   * @param response The HTTP Response we are processing.
   */
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException
  {
    HashMap secondaryMap = new HashMap();
    Connection conn = null;
    CachedResultSet rs = null;
    String messageType = null;
    String actionType = null;
    String originId = null;
    String companyId = null;
    String messageId = null;
    String customerId = null;
    String orderGuid = null;
    String extOrderNum = null;
    String customerEmailAddress = null;
    String masterOrderNum =  null;
    String customerFirstname = null;
    String customerLastname = null;
    String customerPhone = null;
    String sourceCode = null;
    
    try
    {
      // pull base document
      Document responseDocument = (Document) DOMUtil.getDocument();
      
      //get all the parameters in the request object
      // get action type
      if(request.getParameter(COMConstants.ACTION_TYPE)!=null)
      {
        actionType = request.getParameter(COMConstants.ACTION_TYPE);
        secondaryMap.put(COMConstants.ACTION_TYPE, actionType);
      }        
      // message type
      messageType = request.getParameter(COMConstants.MESSAGE_TYPE);
      secondaryMap.put(COMConstants.MESSAGE_TYPE, messageType);
      // message id
      messageId = request.getParameter(COMConstants.MESSAGE_ID);
      secondaryMap.put(COMConstants.MESSAGE_ID, messageId);
      
      customerEmailAddress = request.getParameter("email_address");
      
      // misc parameters
      String orderDetailId = request.getParameter(COMConstants.ORDER_DETAIL_ID);
      secondaryMap.put(COMConstants.ORDER_DETAIL_ID, orderDetailId);
      secondaryMap.put(COMConstants.MESSAGE_CONTENT, request.getParameter(COMConstants.MESSAGE_CONTENT));
      secondaryMap.put(COMConstants.MESSAGE_TITLE, request.getParameter(COMConstants.MESSAGE_TITLE));
      secondaryMap.put(COMConstants.MESSAGE_SUBJECT, request.getParameter(COMConstants.MESSAGE_SUBJECT));
      secondaryMap.put("message_content", request.getParameter("replyBody")); // for refresh of SendEmail.xsl
      secondaryMap.put(COMConstants.EMAIL_REPLY_FLAG, request.getParameter(COMConstants.EMAIL_REPLY_FLAG));
      secondaryMap.put("email_address", request.getParameter("email_address"));
                      
      // get database connection
      conn = DataSourceUtil.getInstance().getConnection(ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE,
                                                            COMConstants.DATASOURCE_NAME));
       
      // use OrderDAO to access some header info on the sendEMail.xsl page
      OrderDAO orderAccess = new OrderDAO(conn);
      rs = orderAccess.getOrderCustomerInfo(orderDetailId);
      
      if(rs.next())
      {
        customerId = rs.getString("customer_id");
        orderGuid = rs.getString("order_guid");
        extOrderNum = rs.getString("external_order_number");
        masterOrderNum = rs.getString("master_order_number");
        customerFirstname = rs.getString("first_name");
        customerLastname = rs.getString("last_name");
        // customerEmailAddress = rs.getString("email_address");
        companyId = rs.getString("company_id");
        originId = rs.getString("origin_id");
        customerPhone = rs.getString("customer_phone_number");
        
        secondaryMap.put(COMConstants.CUSTOMER_ID, customerId);
        secondaryMap.put(COMConstants.ORDER_GUID, orderGuid);
        secondaryMap.put(COMConstants.EXTERNAL_ORDER_NUMBER, extOrderNum);
        secondaryMap.put(COMConstants.MASTER_ORDER_NUMBER, masterOrderNum);
        secondaryMap.put(COMConstants.CUSTOMER_FIRST_NAME, customerFirstname);
        secondaryMap.put(COMConstants.CUSTOMER_LAST_NAME, customerLastname);
        secondaryMap.put(COMConstants.CUSTOMER_EMAIL_ADDRESS, customerEmailAddress);
        secondaryMap.put(COMConstants.COMPANY_ID, companyId);
        secondaryMap.put(COMConstants.ORIGIN_ID, originId);
      }
      else
      {
        logger.error("Customer Info missing from DB for orderDetailId="+orderDetailId);
      }
      if (companyId == null) {
    	  companyId = request.getParameter(COMConstants.COMPANY_ID);
      }
      logger.info("companyId: " + companyId);
      logger.info("request origin_id: " + request.getParameter(COMConstants.ORIGIN_ID));

      rs = orderAccess.getOrderDetailsInfo(orderDetailId);
      if (rs.next())
      {
          sourceCode = rs.getString("source_code");
      }
      else
      {
          logger.error("Order Detail Info missing from DB for orderDetailId="+orderDetailId);
      }
                  
      StockMessageDAO msgAccess = new StockMessageDAO(conn);
      PartnerMappingVO partnerMappingVO = new PartnerUtility().getPartnerOriginsInfo(originId,sourceCode, conn);
      // scrub the origin id against config file origins list
      if(originId!=null)
      {
        String originList = ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE,
                                                COMConstants.EMAIL_ORIGINS);
        if(originList.indexOf(originId)>=0 || (partnerMappingVO != null))
        {
          // keep the origin id
        }
        else
        {
          originId = null;
        }
      }
      
      //load email or letter titles based off of pocType and originid
      /*String attachedEmailIndicator = "N";
      if(extOrderNum!=null)
      {
        attachedEmailIndicator = "Y";
      }*/
     
      // Document msgXML = msgAccess.loadMessageTitlesXML(messageType, originId, companyId, NO, attachedEmailIndicator);
      // Check for preferred partner based on source code
      Document msgXML = null;
      PartnerVO partnerVO = FTDCommonUtils.getPreferredPartnerBySource(sourceCode);  
      if (partnerVO == null) {
          if (partnerMappingVO != null) {
              if (partnerMappingVO.getCreateStockEmail().equals("Y")) {
                  msgXML = msgAccess.loadMessageTitlesXML(messageType, originId, companyId, NO, NO, null);
                  logger.debug("create stock email is set to 'Y' for partner orders, show stock emails for origin: " + originId);
              } else {
                  msgXML = msgAccess.loadMessageTitlesXML(messageType, null, companyId, NO, NO, null) ;
                  logger.debug("create stock email is set to 'N' for partner orders, show FTD stock email titles");
              }
          } else {
            msgXML = msgAccess.loadMessageTitlesXML(messageType, originId, companyId, NO, NO, null);
            logger.debug("Show stock email for origin: " + originId);
          }
      } else {
          msgXML = msgAccess.loadMessageTitlesXML(messageType, null, companyId, NO, NO, partnerVO.getPartnerName());   
          logger.debug("Show stock emails for partner " + partnerVO.getPartnerName());
      }
      DOMUtil.addSection(responseDocument, msgXML.getChildNodes());
      
      // get an instance of StockMessageGenerator
      StockMessageGenerator msgGen = new StockMessageGenerator(conn); 
      MessageGeneratorDAO msgGenDAO = new MessageGeneratorDAO(conn);
      
      // Create a PointOfContactVO using pocType, customerId, guid, orderDetailId, companyId and body from request object
      PointOfContactVO pocObj = new PointOfContactVO();
      pocObj.setPointOfContactType(messageType);
      
      long customerIdNum = 0;
      if(customerId!=null)
        customerIdNum = Long.parseLong(customerId);
      
      long orderDetailIdNum = 0;
      if(orderDetailId!=null && !orderDetailId.equals(""))
        orderDetailIdNum = Long.parseLong(orderDetailId);
        
      pocObj.setCustomerId(customerIdNum);
      pocObj.setOrderGuid(orderGuid);
      pocObj.setOrderDetailId(orderDetailIdNum);
      pocObj.setCompanyId(companyId);
      pocObj.setMasterOrderNumber(masterOrderNum);
      pocObj.setExternalOrderNumber(extOrderNum);
      pocObj.setFirstName(customerFirstname);
      pocObj.setLastName(customerLastname);
      pocObj.setDaytimePhoneNumber(customerPhone);
      pocObj.setSentReceivedIndicator(OUTGOING_MESSAGE);
      pocObj.setBody(request.getParameter(COMConstants.MESSAGE_CONTENT));
      pocObj.setCommentType(COMConstants.ORDER_COMMENT_TYPE);
      pocObj.setLetterTitle(request.getParameter(COMConstants.MESSAGE_TITLE));
      
      
      
      
      if (SecurityManager.getInstance().getUserInfo(request.getParameter(COMConstants.SEC_TOKEN))!=null)
      {
        pocObj.setCreatedBy(SecurityManager.getInstance().getUserInfo(request.getParameter(COMConstants.SEC_TOKEN)).getUserID());
        pocObj.setUpdatedBy(SecurityManager.getInstance().getUserInfo(request.getParameter(COMConstants.SEC_TOKEN)).getUserID());
      }
      
      if(messageType.equalsIgnoreCase(COMConstants.EMAIL_MESSAGE_TYPE))
      {
        // use company_id to pull SenderEmailAddress from db
        String companyEmailAddress = msgGenDAO.loadSenderEmailAddress(pocObj.getLetterTitle(), companyId, orderDetailIdNum);
        
        pocObj.setSenderEmailAddress(companyEmailAddress);
        pocObj.setRecipientEmailAddress(customerEmailAddress);
        pocObj.setEmailSubject(request.getParameter(COMConstants.MESSAGE_SUBJECT));
      }
      
      // changes related to FICO
      pocObj.setSourceCode(sourceCode);
      pocObj.setEmailType(MessageConstants.COM_EMAIL_TYPE);

      if (orderGuid != null) {
    	pocObj.setRecordAttributesXML(msgGen.generateRecordAttributeXMLFromOrderGuid(orderGuid));  
      }
      else {
    	  logger.error("Unable to create XML attributes string for orderGuid="+orderGuid);
      }
      
      // Insert the email into the db table by passing the newly created POC object and returning a pocId
      pocObj = msgGen.insertMessage(pocObj);
        
      HashMap letterValues = new HashMap();
      if(messageType.equalsIgnoreCase(COMConstants.LETTER_MESSAGE_TYPE))
      {
        // Call insertComment() on OrderDAO passing the specified comment attached to the current order and customer
        this.createComment("Letter Title: " + pocObj.getLetterTitle(), conn, request);
      
        // if the origin is found in the configuration file, do not pull company logo
        String originList = ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE,
                                                                        COMConstants.EMAIL_ORIGINS);
                                                
        if(originId!=null && originList.indexOf(originId)>=0) // origin found
        {
            // do nothing
        }
        else
        {
          // if origin is not found in the configuration file, Pull logo name from database based on company id
          String logoFileName = msgAccess.loadLetterLogo(companyId);
          letterValues.put("logo", this.getServlet().getServletContext().getRealPath("/images/" + logoFileName));
        }
        
        // Put the logo file name and letter body (loaded from request) into a HashMap and use TraxUtil to convert to PDF
        
        String body = request.getParameter(COMConstants.MESSAGE_CONTENT);
        if(body!=null)
        {
          // body = body.replaceAll(" ", "&#160;");
          body = body.replaceAll("\\r", "");
          body = body.replaceAll("\\n", "</fo:block><fo:block>");
          body = FieldUtils.replaceAll(body, "<fo:block></fo:block>", "<fo:block space-after.optimum='15pt'></fo:block>");
        }
        
        letterValues.put("body", body);
        
        DOMUtil.addSection(responseDocument, "letter", "detail", letterValues, true);
        
        //Get XSL File name
        File xslFile = getXSL("LetterPreview", mapping);  
        String foString = TraxUtil.getInstance().transform(responseDocument, xslFile, null);
        
        StringReader sr = new StringReader(foString);
        renderFO(new InputSource(sr), response);
      }
      else
      if(messageType.equalsIgnoreCase(COMConstants.EMAIL_MESSAGE_TYPE))
      {
        // Send the email by calling sendMessage() of StockMessageGenerator
        pocObj = msgGen.sendMessage(pocObj);
        String statusMsg = "Email sent to " + pocObj.getRecipientEmailAddress();
        secondaryMap.put("status_flag", statusMsg);
        
        // Call insertComment() on OrderDAO passing the specified comment attached to the current order and customer
        this.createComment("Email Title: " + pocObj.getLetterTitle(), conn, request);
        
        // Update delivery confirmation flag to 'Sent' for email that have stock email type id = DCON 
        if(msgAccess.isDCONEmail(messageId))
        {
            //get csr Id
            ConfigurationUtil cu = ConfigurationUtil.getInstance();
            String security = (String) cu.getProperty(COMConstants.PROPERTY_FILE,COMConstants.SECURITY_IS_ON);
            boolean securityIsOn = security.equalsIgnoreCase("true") ? true : false;
            String sessionId   = request.getParameter(COMConstants.SEC_TOKEN);
            String csrId = null;
            if(securityIsOn || sessionId != null)
            {
              
              SecurityManager sm = SecurityManager.getInstance();
              UserInfo ui = sm.getUserInfo(sessionId);
              csrId = ui.getUserID();
            }
            orderAccess.updateDCONStatus(orderDetailId, GeneralConstants.DCON_CONFIRMED, csrId);       
        }
        
        // Create XML from the POC object and append to primary document
        if(request.getParameter(COMConstants.EMAIL_REPLY_FLAG).equals("Y"))
        {
          // recover the text of the new email body
          pocObj.setBody(request.getParameter("newBody"));
        }
        
        String pocXMLString = pocObj.toXML();
        
        // Clean up any & chars
        pocXMLString = FieldUtils.replaceAll(pocXMLString, "&", "&amp;");
        
        Document pocXML = this.toXML(pocXMLString);
        DOMUtil.addSection(responseDocument, pocXML.getChildNodes());
        
        // Generate XML from secondary HashMap using the DOMUtil HashMap to XML conversion method addSection() .
        DOMUtil.addSection(responseDocument, "pageData", "data", secondaryMap, true);
      
        // Forward the page to point of entry (eg., sendEmail.xsl)
        //Get XSL File name
        File xslFile = getXSL(messageType, mapping);
        ActionForward forward = mapping.findForward(messageType);
        String xslFilePathAndName = forward.getPath(); //get real file name
        
        // Change to client side transform
        TraxUtil.getInstance().getInstance().transform(request, response, responseDocument, 
                            xslFile, xslFilePathAndName, (HashMap)request.getAttribute(COMConstants.CONS_APP_PARAMETERS));
        
        return null;
      }
    }
    catch (Throwable ex)
    {
      logger.error(ex);
      return mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
    }
    finally 
    {
      try
      {
        if(conn!=null)
            conn.close();
      }
      catch (SQLException se)
      {
        logger.error(se);
        return mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
      }
    }
    return null;
  }
  
  public void renderFO(InputSource foFile,
                       HttpServletResponse response) throws ServletException {
      try 
      {
          ByteArrayOutputStream out = new ByteArrayOutputStream();
          response.setContentType("application/pdf");

          logger.debug("Pulling FOP driver");
          Driver driver = new Driver(foFile, out);
          //driver.setLogger(logger);
          driver.setRenderer(Driver.RENDER_PDF);
          logger.debug("Running FOP driver");
          driver.run();

          logger.debug("Pushing to PDF response");
          byte[] content = out.toByteArray();
          response.setContentLength(content.length);
          response.getOutputStream().write(content);
          logger.debug("Flushing response stream");
          response.getOutputStream().flush();
      }
      catch (Exception ex) {
          logger.error(ex.getMessage());
      }
      finally
      {
          try
          {
              if(response.getOutputStream() != null)
              {
                  logger.debug("Closing output stream");
                  response.getOutputStream().close();
              }
          }
          catch (Exception ex) 
          {
            logger.error(ex.getMessage());
          }
      }
  }
  
  private void createComment(String messageTitle, Connection conn, HttpServletRequest request) throws Exception
  {
        CommentDAO commentDAO = new CommentDAO(conn);
        
        // Call insertComment() on OrderDAO passing the specified comment attached to the current order and customer
        CommentsVO commentObj = new CommentsVO();
        commentObj.setComment(messageTitle);
        commentObj.setCommentType(COMConstants.ORDER_COMMENT_TYPE);
        commentObj.setOrderDetailId(request.getParameter(COMConstants.ORDER_DETAIL_ID));
        commentObj.setOrderGuid(request.getParameter(COMConstants.ORDER_GUID));
        commentObj.setCustomerId(request.getParameter(COMConstants.CUSTOMER_ID));
        
        if (SecurityManager.getInstance().getUserInfo(request.getParameter(COMConstants.SEC_TOKEN))!=null)
        {
          String userId = SecurityManager.getInstance().getUserInfo(request.getParameter(COMConstants.SEC_TOKEN)).getUserID();
          commentObj.setCreatedBy(userId);
          commentObj.setUpdatedBy(userId);
        }
        
        commentObj.setCommentOrigin(request.getParameter(COMConstants.START_ORIGIN));
        commentObj.setDnisId(request.getParameter(COMConstants.H_DNIS_ID));
        commentObj.setReason("email");
        commentDAO.insertComment(commentObj);
  }
  
  private Document toXML(String xmlString) throws Exception
  {
	  return DOMUtil.getDocument(xmlString);
  }

  /******************************************************************************
  *                                     getXSL()
  *******************************************************************************
  * Retrieve name of Customer Order Search Action XSL file
  * @param1 String - the xsl name returned from the Business Object
  * @param2 ActionMapping
  * @return File - XSL File name
  * @throws none
  */

  private File getXSL(String xslName, ActionMapping mapping)
  {

    File xslFile = null;
    String xslFilePathAndName = null;

    ActionForward forward = mapping.findForward(xslName);
    xslFilePathAndName = forward.getPath();                //get real file name
    xslFile = new File(this.getServlet().getServletContext().getRealPath(xslFilePathAndName));
    return xslFile;
  }
}