package com.ftd.customerordermanagement.actions;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.dao.CommentDAO;
import com.ftd.customerordermanagement.dao.LockDAO;
import com.ftd.customerordermanagement.dao.OrderDAO;
import com.ftd.customerordermanagement.dao.TagDAO;
import com.ftd.customerordermanagement.dao.ViewDAO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityManager;

import java.io.File;
import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;

import org.apache.commons.lang.StringUtils;

public final class LoadCommentsAction extends Action 
{
    private static Logger logger  = new Logger("com.ftd.customerordermanagement.actions.LoadCommentsAction");
    private static String OUT_CUR = "OUT_CUR";
    private static String OUT_ID_COUNT = "OUT_ID_COUNT";
    private static String OUT_PARAMETERS = "out-parameters";
    private static String YES = "Y";
    private static String NO = "N";
    private static String ON = "on";
    private static String MANAGER_COMMENT_ROLE = "MANAGER_COMMENT_ROLE";
    private static String COMMENTS_APP_NAME = "COMMENTS_APP_NAME";
  
  /**
   * This is the main action called from the Struts framework.
   * @param mapping The ActionMapping used to select this instance.
   * @param form The optional ActionForm bean for this request.
   * @param request The HTTP Request we are processing.
   * @param response The HTTP Response we are processing.
   */
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException
  {
    String commentType = null;
    int pagePosition = 0; // page_position value in request object
    HashMap secondaryMap = new HashMap();
    Document xmlLock = null;
    Document xmlComments = null;
    Document xmlOrderComments = null;
    String userId = null; //default
    String lockId = null; //customer ID or order guid or order detail id
    String customerId = null;
    String orderGuid = null;
    String managerRole = null;
    int totalPage = 0;
    int totalCount = 0;
    int maxRecords = 0;
    String orderDetailId = null;
    String commentOrigin = null;
    String mgrOnly = null;
    String hideSystem = null;
    Connection conn = null;
    CachedResultSet orderResultSet = null;
    Date orderDate = null;
    String masterOrderNumber = null;
    String extOrderNumber = null;
    String orderCommentOrigin = null;
    String originTab = null;
    String cmntsTotalPages = null;
    //origin_id, product_type, ship_method, vendor_flag, order_guid
    String origin_id = null;
    String product_type = null;
    String ship_method = null;
    String vendor_flag = null;
    //vip_customer
    String vip_customer = null;
    
    try
    {
        // pull base document
        Document responseDocument = (Document) DOMUtil.getDocument();
        
        // get database connection
        conn =
        DataSourceUtil.getInstance().getConnection(ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE,
                                                    COMConstants.DATASOURCE_NAME));
        
        // get order detail id
        orderDetailId = request.getParameter(COMConstants.ORDER_DETAIL_ID);
        secondaryMap.put(COMConstants.ORDER_DETAIL_ID, orderDetailId);
        
        // get vip_customer flag
        vip_customer = request.getParameter(COMConstants.VIP_CUSTOMER);
        secondaryMap.put(COMConstants.VIP_CUSTOMER, vip_customer);
	
       // get origin id
        origin_id = request.getParameter(COMConstants.ORIGIN_ID);
        secondaryMap.put(COMConstants.ORIGIN_ID, origin_id);
        
        // get product type
        product_type = request.getParameter(COMConstants.PRODUCT_TYPE);
        secondaryMap.put(COMConstants.PRODUCT_TYPE, product_type);
        
        // get ship_method
        ship_method = request.getParameter(COMConstants.SHIP_METHOD);
        secondaryMap.put(COMConstants.SHIP_METHOD, ship_method);
        
        // get order vendor_flag
        vendor_flag = request.getParameter(COMConstants.VENDOR_FLAG);
        secondaryMap.put(COMConstants.VENDOR_FLAG, vendor_flag);
        
        //Document that will contain the output from the stored proce
        Document csrTaggedInfo = DOMUtil.getDocument();

        // Ensure orderDetailId is passed in
        if (null != orderDetailId && !"".equals(orderDetailId))
        {
          //Call getTagCsr method in the DAO
          TagDAO tagDAO = new TagDAO(conn);
          csrTaggedInfo = (Document) tagDAO.getTagCsr(orderDetailId);
        }
        
        DOMUtil.addSection(responseDocument, csrTaggedInfo.getChildNodes());
        
        
        /*
        DCE
        */
        originTab = request.getParameter(COMConstants.ORIGIN_TAB);
        cmntsTotalPages = request.getParameter(COMConstants.PD_COMMENTS_TOTAL_PAGES);
        
        if(originTab == null || "".equalsIgnoreCase(originTab))
        {	
        	originTab = COMConstants.DEFAULT_TAB;
        }
        logger.info("origin_tab:"+originTab);
        logger.info("cmntsTotalPages:"+cmntsTotalPages);
        
        secondaryMap.put(COMConstants.ORIGIN_TAB, originTab);
        
        //get all the parameters in the request object
        // comment type
        commentType = request.getParameter(COMConstants.COMMENT_TYPE);
        secondaryMap.put(COMConstants.COMMENT_TYPE, commentType);
        // page position
        if(request.getParameter(COMConstants.PAGE_POSITION)!=null)
        {
          pagePosition = Integer.parseInt(request.getParameter(COMConstants.PAGE_POSITION));
          pagePosition = pagePosition>0 ? pagePosition : 1; // cannot be zero or negative!
        }
        
        logger.info("pagePosition:"+pagePosition);
        
        secondaryMap.put(COMConstants.PAGE_POSITION, new Integer(pagePosition).toString());
        
        // get customer ID and order Guid from request object for Customer comments
        // get customer ID and order Guid from db for Order comments
        if(commentType.equals(COMConstants.ORDER_COMMENT_TYPE))
        {
            orderCommentOrigin = "Y";
        }
        else
        {
            orderCommentOrigin = request.getParameter("order_comment_origin");
        }
        
        OrderDAO orderAccess = new OrderDAO(conn);
        orderResultSet = orderAccess.getOrderCustomerInfo(orderDetailId);
        if(orderResultSet.next())
        {
            customerId = orderResultSet.getString("customer_id");
            orderGuid = orderResultSet.getString("order_guid");
            orderDate = orderResultSet.getDate("order_date");
            masterOrderNumber = orderResultSet.getString("master_order_number");
            extOrderNumber = orderResultSet.getString("external_order_number");
        }
        
        if(customerId == null || customerId.equalsIgnoreCase(""))
        {
            customerId = request.getParameter("customer_id");
        }
        
        secondaryMap.put(COMConstants.CUSTOMER_ID, customerId);
        secondaryMap.put(COMConstants.ORDER_GUID, orderGuid);
        secondaryMap.put(COMConstants.MASTER_ORDER_NUMBER, masterOrderNumber);
        secondaryMap.put(COMConstants.EXTERNAL_ORDER_NUMBER, extOrderNumber);
        secondaryMap.put("order_comment_origin", orderCommentOrigin);
        
        if(orderDate != null && !orderDate.equals(""))
        {
            String DATE_FORMAT = "MM/dd/yy";
            SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT); 
            secondaryMap.put(COMConstants.ORDER_DATE, sdf.format(orderDate));
        }
        
        // get uo only comments
        if(request.getParameter(COMConstants.MGR_ONLY_COMMENTS)!=null)
        {
          mgrOnly = YES;
          secondaryMap.put(COMConstants.MGR_ONLY_COMMENTS, ON);
        }
        else {
          mgrOnly = NO;
        }
        // get hide system comments
        if(request.getParameter(COMConstants.HIDE_SYSTEM_COMMENTS)!=null)
        {
          hideSystem = YES;
          secondaryMap.put(COMConstants.HIDE_SYSTEM_COMMENTS, ON);
        }
        else {
          hideSystem = NO;
        }
        
        // get security token and other hidden input parameters from the request object
        //String securityToken = "FTD_GUID_1100147550436499";
        String securityToken = (String) request.getParameter(COMConstants.SEC_TOKEN);
        
        // get user ID. it should not be null
        // use security manager to get user info object. extract csr_id from it.
        // check for null pointers
        if (SecurityManager.getInstance().getUserInfo(securityToken)!=null)
        {
          userId = SecurityManager.getInstance().getUserInfo(securityToken).getUserID();
        }

        //check to see if the user-role can modify a comment
        boolean userInRole = false; 
    
        userInRole = SecurityManager.getInstance().assertPermission(request.getParameter("context"), securityToken, "Comments And Email Resource", "Update"); 
        secondaryMap.put("user_can_update", userInRole?"Y":"N");
      
        String vipCustUpdatePermission = null;
        if(SecurityManager.getInstance().assertPermission(request.getParameter("context"), securityToken, COMConstants.VIP_CUSTOMER_UPDATE, COMConstants.YES))
        { 
      	  vipCustUpdatePermission = "Y";
        }
        else
        {
      	  vipCustUpdatePermission = "N";
        }
        secondaryMap.put("vipCustUpdatePermission",vipCustUpdatePermission);
        
        String vipCustomerMessage = null;
        vipCustomerMessage = ConfigurationUtil.getInstance().getContentWithFilter(conn, "VIP_CUSTOMER", COMConstants.COM_CONTEXT, "VIP_CUSTOMER_MESSAGE", null);
        secondaryMap.put("vipCustomerMessage",vipCustomerMessage);
        
        String vipCustUpdateDeniedMessage = null;
        vipCustUpdateDeniedMessage = ConfigurationUtil.getInstance().getContentWithFilter(conn, "VIP_CUSTOMER", COMConstants.COM_CONTEXT, "VIP_CUSTOMER_UPDATE_DENIED_MESSAGE", null);
        secondaryMap.put("vipCustUpdateDeniedMessage",vipCustUpdateDeniedMessage);

        // Load the csr viewing data
        Document csrViewingXML = null;
        if(commentType.equals(COMConstants.ORDER_COMMENT_TYPE))
        {
            csrViewingXML = this.retrieveCSRViewing(conn, "ORDER_DETAILS", request.getParameter(COMConstants.ORDER_DETAIL_ID), userId, securityToken);
        }
        else
        {
            csrViewingXML = this.retrieveCSRViewing(conn, "CUSTOMER", request.getParameter(COMConstants.CUSTOMER_ID), userId, securityToken);
        }
        
        DOMUtil.addSection(responseDocument, csrViewingXML.getChildNodes());
        
        // get maxRecords from config file
        String maxrec = ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, COMConstants.CONS_MAX_COMMENTS_PER_PAGE);
        if(maxrec!=null && !maxrec.equals(""))
          maxRecords = Integer.parseInt(maxrec);
        secondaryMap.put(COMConstants.CONS_MAX_COMMENTS_PER_PAGE, new Integer(maxRecords).toString());
        
        //get managerRole from config file for bolding the comments on the screen
        managerRole = ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, MANAGER_COMMENT_ROLE);
        
        // calculate startNumber to pass to the page and also to the database
        int startNumber = (pagePosition-1) * maxRecords + 1;
        secondaryMap.put(COMConstants.PD_START_POSITION, new Integer(startNumber).toString());
        
        // Call the checkLockXML() on the LockDAO to return lock status XML data for specific ID.
        // The comment_type parameter will determine which type of ID to check the lock for.        
        LockDAO lock = new LockDAO(conn);
        // determine the lock id
        if(customerId!=null && !customerId.equals("") && commentType.equals(COMConstants.CUSTOMER_COMMENT_TYPE))
        {
          lockId =  customerId;
        } else
        if(orderDetailId!=null && !orderDetailId.equals("") && commentType.equals(COMConstants.ORDER_COMMENT_TYPE))
        {
          lockId = orderDetailId;
        }
        xmlLock = lock.checkLockXML(securityToken, userId, lockId, COMConstants.CONS_ENTITY_TYPE_COMMENTS);
        
        DOMUtil.addSection(responseDocument, xmlLock.getChildNodes());
            
        // Call load DAO method to load comments based on the comment_type you are looking for, by passing in the currentPage
        // and maxRecords and managerComment role found earlier. Need to pass uo_comments_only and hide_system_comments flags
        HashMap commentsMap = null;
        HashMap orderCommentsMap = null;
        if(commentType.equals(COMConstants.ORDER_COMMENT_TYPE))
        {
          CommentDAO commentDAO = new CommentDAO(conn);
          commentsMap = commentDAO.loadOrderDetailCommentsXML(orderDetailId, startNumber, maxRecords, managerRole, mgrOnly, hideSystem, originTab);
        }
        else if (commentType.equals(COMConstants.CUSTOMER_COMMENT_TYPE))
        {
          CommentDAO commentDAO = new CommentDAO(conn);
          commentsMap = commentDAO.loadAllCustomerCommentsXML(customerId, startNumber, maxRecords, managerRole, mgrOnly, hideSystem);          
        }
        xmlComments = (Document) commentsMap.get(OUT_CUR);
        totalCount = Integer.parseInt(DOMUtil.getNodeValue((Document) commentsMap.get(OUT_PARAMETERS), OUT_ID_COUNT));
        DOMUtil.addSection(responseDocument, xmlComments.getChildNodes());
        
        // Calculate the totalPage and add this to the secondary XML HashMap.
        // This is calculated by pulling the totalCount from the XML returned from the database (server-side Xpath)
        // and dividing it by the maxRecords.        
        totalPage = (int) Math.floor(totalCount / maxRecords);
        if(totalCount%maxRecords > 0)
          totalPage += 1;
        secondaryMap.put(COMConstants.PD_TOTAL_PAGES, new Integer(totalPage).toString());
        
        /*
         * DCE-15 : DCON � Order Comment�s page navigation buttons moves users through pages on both tabs
         * Added if block to include total # of pages available for COMMENTS. 
         * This is required to address the scenario where user tries to switch to COMMENTS tab from DETAILED COMMENTS tab, while on the page number which is  
         * applicable to DETAILED COMMENTS but not to COMMENTS.
         * Having different no. of pages for COMMENTS and DETAILED COMMENTS is a possible scenario because records displayed in COMMENTS are the ones filtered out from 
         * DETAILED COMMENTS excluding the comments provided by configured user IDs.   
         */
        if(COMConstants.ORDER_COMMENT_TYPE.equals(commentType))
        {
        	if(COMConstants.DEFAULT_TAB.equalsIgnoreCase(originTab))
        		secondaryMap.put(COMConstants.PD_COMMENTS_TOTAL_PAGES, new Integer(totalPage).toString());
            else
        		secondaryMap.put(COMConstants.PD_COMMENTS_TOTAL_PAGES, StringUtils.isEmpty(cmntsTotalPages)?1:cmntsTotalPages);
        }
        
        logger.info(COMConstants.PD_COMMENTS_TOTAL_PAGES+":"+secondaryMap.get(COMConstants.PD_COMMENTS_TOTAL_PAGES));
        
        if(request.getParameter(COMConstants.BYPASS_COM)!= null)
            secondaryMap.put(COMConstants.BYPASS_COM, request.getParameter(COMConstants.BYPASS_COM));
        else
            secondaryMap.put(COMConstants.BYPASS_COM, "N"); 
        if(request.getParameter("lossPreventionPermission")!= null)
            secondaryMap.put("lossPreventionPermission", request.getParameter("lossPreventionPermission"));
        else
            secondaryMap.put("lossPreventionPermission", "N");    
        // Generate XML from secondary HashMap using the DOMUtil HashMap to XML conversion method addSection() .
         DOMUtil.addSection(responseDocument, "pageData", "data", secondaryMap, true);
                
        // Forward to transformation action with customerComments.xsl or orderComments.xsl based on the comment_type.
        //Get XSL File name
        File xslFile = getXSL(commentType, mapping);  
        ActionForward forward = mapping.findForward(commentType);
        String xslFilePathAndName = forward.getPath(); //get real file name
        
        // Change to client side transform
        TraxUtil.getInstance().getInstance().transform(request, response, responseDocument, 
                            xslFile, xslFilePathAndName, (HashMap)request.getAttribute(COMConstants.CONS_APP_PARAMETERS));
       
       return null;
    }
    catch (Throwable ex)
    {
      try
      {
        if(conn!=null)
          conn.close();
      }
      catch (SQLException se)
      {
        logger.error(se);
        return mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
      }
      logger.error(ex);
      return mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
    }
    finally 
    {
      try
      {
        if(conn!=null)
          conn.close();
      }
      catch (SQLException se)
      {
        logger.error(se);
        return mapping.findForward(COMConstants.CONS_SYSTEM_ERROR);
      }
    }
  }

  /******************************************************************************
  *                                     getXSL()
  *******************************************************************************
  * Retrieve name of Customer Order Search Action XSL file
  * @param1 String - the xsl name returned from the Business Object
  * @param2 ActionMapping
  * @return File - XSL File name
  * @throws none
  */

  private File getXSL(String xslName, ActionMapping mapping)
  {

    File xslFile = null;
    String xslFilePathAndName = null;

    ActionForward forward = mapping.findForward(xslName);
    xslFilePathAndName = forward.getPath();                //get real file name
    xslFile = new File(this.getServlet().getServletContext().getRealPath(xslFilePathAndName));
    return xslFile;
  }
  
    private Document retrieveCSRViewing(Connection conn, String entityType, String entityId, String userId, String securityToken) throws Exception
    {
        //Instantiate ViewDAO
        ViewDAO viewDAO = new ViewDAO(conn);
    
        //Document that will contain the output from the stored proce
        Document csrViewingXML = DOMUtil.getDocument();
        
        //Call getCSRViewed method in the DAO
        csrViewingXML = viewDAO.getCSRViewing(securityToken, userId, entityType, entityId);
    
        return csrViewingXML;
    }

}

