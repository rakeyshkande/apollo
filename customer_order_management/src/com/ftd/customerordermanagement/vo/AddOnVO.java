package com.ftd.customerordermanagement.vo;
import java.lang.reflect.Field;

import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class AddOnVO extends BaseVO implements XMLInterface
{
  private	String	  addOnCode;
  private String    addOnDescription;
  private	long	    addOnId;
  private double    addOnPrice;
  private	long	    addOnQuantity;
  private String    addOnStatus;
  private String    addOnTypeDescription;
  private	String 	  createdBy;
  private	Calendar	createdOn;
  private	long	    orderDetailId;
  private	String 	  updatedBy;
  private	Calendar	updatedOn;

  
  public AddOnVO()
  {
  }

  public void setOrderDetailId(long orderDetailId)
  {
    if(valueChanged(this.orderDetailId, orderDetailId))
    {
      setChanged(true);
    }
    this.orderDetailId = orderDetailId;
  }


  public long getOrderDetailId()
  {
    return this.orderDetailId;
  }


  public void setCreatedOn(Calendar createdOn)
  {
    if(valueChanged(this.createdOn, createdOn))
    {
      setChanged(true);
    }
    this.createdOn = createdOn;
  }


  public Calendar getCreatedOn()
  {
    return this.createdOn;
  }


  public void setCreatedBy(String createdBy)
  {
    if(valueChanged(this.createdBy, createdBy))
    {
      setChanged(true);
    }

    this.createdBy = trim(createdBy);
  }


  public String getCreatedBy()
  {
    return this.createdBy;
  }


  public void setUpdatedOn(Calendar updatedOn)
  {
    if(valueChanged(this.updatedOn, updatedOn ))
    {
      setChanged(true);
    }
    this.updatedOn = updatedOn;
  }


  public Calendar getUpdatedOn()
  {
    return this.updatedOn;
  }


  public void setUpdatedBy(String updatedBy)
  {
    if(valueChanged(this.updatedBy, updatedBy ))
    {
      setChanged(true);
    }
    this.updatedBy = trim(updatedBy);
  }


  public String getUpdatedBy()
  {
    return this.updatedBy;
  }


  public void setAddOnId(long addOnId)
  {
    if(valueChanged(this.addOnId, addOnId))
    {
      setChanged(true);
    }
    this.addOnId = addOnId;
  }


  public long getAddOnId()
  {
    return this.addOnId;
  }


  public void setAddOnCode(String addOnCode)
  {
    if(valueChanged(this.addOnCode, addOnCode))
    {
      setChanged(true);
    }
    this.addOnCode = trim(addOnCode);
  }


  public String getAddOnCode()
  {
    return this.addOnCode;
  }


  public void setAddOnQuantity(long addOnQuantity)
  {
    if(valueChanged(this.addOnQuantity, addOnQuantity))
    {
      setChanged(true);
    }
    this.addOnQuantity = addOnQuantity;
  }


  public long getAddOnQuantity()
  {
    return this.addOnQuantity;
  }



  public void setAddOnTypeDescription(String addOnTypeDescription)
  {
    if(valueChanged(this.addOnTypeDescription, addOnTypeDescription ))
    {
      setChanged(true);
    }
    this.addOnTypeDescription = trim(addOnTypeDescription);
  }


  public String getAddOnTypeDescription()
  {
    return this.addOnTypeDescription;
  }


  public void setAddOnDescription(String addOnDescription)
  {
    if(valueChanged(this.addOnDescription, addOnDescription ))
    {
      setChanged(true);
    }
    this.addOnDescription = trim(addOnDescription);
  }


  public String getAddOnDescription()
  {
    return this.addOnDescription;
  }


  public void setAddOnPrice(double addOnPrice)
  {
    if(valueChanged(this.addOnPrice, addOnPrice))
    {
      setChanged(true);
    }
    this.addOnPrice = addOnPrice;
  }


  public double getAddOnPrice()
  {
    return this.addOnPrice;
  }



  public void setAddOnStatus(String addOnStatus)
  {
    if(valueChanged(this.addOnStatus, addOnStatus ))
    {
      setChanged(true);
    }
    this.addOnStatus = trim(addOnStatus);
  }


  public String getAddOnStatus()
  {
    return this.addOnStatus;
  }


  /**
    * This method uses the Reflection API to generate an XML string that will be
    * passed back to the calling module.
    * The XML string will contain all the fields within this VO, including the
    * variables as well as (a collection of) ValueObjects.
    *
    * @param  None
    * @return XML string
   **/
  public String toXML()
  {
    StringBuffer sb = new StringBuffer();
    try
    {
      sb.append("<AddOnVO>");
      Field[] fields = this.getClass().getDeclaredFields();

      for (int i = 0; i < fields.length; i++)
      {
        //if the field retrieved was a list of VO
        if(fields[i].getType().equals(Class.forName("java.util.List")))
        {
          List list = (List)fields[i].get(this);
          if(list != null)
          {
            for (int j = 0; j < list.size(); j++)
            {
              XMLInterface xmlInt = (XMLInterface)list.get(j);
              String sXmlVO = xmlInt.toXML();
              sb.append(sXmlVO);
            }
          }
        }
        else
        {
          //if the field retrieved was a VO
          if (fields[i].getType().toString().matches("(?i).*vo"))
          {
            XMLInterface xmlInt = (XMLInterface)fields[i].get(this);
            String sXmlVO = xmlInt.toXML();
            sb.append(sXmlVO);
          }
          //if the field retrieved was a Calendar object
          else if (fields[i].getType().toString().matches("(?i).*calendar"))
          {
            Date date;
            String fDate = null;
            if (fields[i].get(this) != null)
            {
              date = (((GregorianCalendar)fields[i].get(this)).getTime());
              SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss a zzz");
              fDate = sdf.format(date).toString();
            }
            sb.append("<" + fields[i].getName() + ">");
            sb.append(fDate);
            sb.append("</" + fields[i].getName() + ">");

          }
          else
          {
            sb.append("<" + fields[i].getName() + ">");
            sb.append(fields[i].get(this));
            sb.append("</" + fields[i].getName() + ">");
          }
        }
      }
      sb.append("</AddOnVO>");
    }

    catch (Exception e)
    {
      e.printStackTrace();
    }

    return sb.toString();
  }


  /**
    * This method uses the Reflection API to generate an XML string that will be
    * passed back to the calling module.
    * The XML string will contain all the fields within this VO, including the
    * variables as well as (a collection of) ValueObjects.
    *
    * @param  None
    * @return XML string
   **/
  public String toXML(int count)
  {
    StringBuffer sb = new StringBuffer();
    try
    {
      if (count == 0)
      {
        sb.append("<AddOnVO>");
      }
      else
      {
        sb.append("<AddOnVO num=" + '"' + count + '"' + ">");
      }
      Field[] fields = this.getClass().getDeclaredFields();

      for (int i = 0; i < fields.length; i++)
      {
        //if the field retrieved was a list of VO
        if(fields[i].getType().equals(Class.forName("java.util.List")))
        {
          List list = (List)fields[i].get(this);
          if(list != null)
          {
            for (int j = 0; j < list.size(); j++)
            {
              XMLInterface xmlInt = (XMLInterface)list.get(j);
              int k = j + 1;
              String sXmlVO = xmlInt.toXML(k);
              sb.append(sXmlVO);
            }
          }
        }
        else
        {
          //if the field retrieved was a VO
          if (fields[i].getType().toString().matches("(?i).*vo"))
          {
            XMLInterface xmlInt = (XMLInterface)fields[i].get(this);
            int k = i + 1;
            String sXmlVO = xmlInt.toXML(k);
            sb.append(sXmlVO);
          }
          //if the field retrieved was a Calendar object
          else if (fields[i].getType().toString().matches("(?i).*calendar"))
          {
            Date date;
            String fDate = null;
            if (fields[i].get(this) != null)
            {
              date = (((GregorianCalendar)fields[i].get(this)).getTime());
              SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss a zzz");
              fDate = sdf.format(date).toString();
  	          sb.append("<" + fields[i].getName() + ">");
              sb.append(fDate);
              sb.append("</" + fields[i].getName() + ">");
            }
            else
            {
							sb.append("<" + fields[i].getName() + "/>");
            }

          }
          else
          {
						if (fields[i].get(this) == null)
						{
							sb.append("<" + fields[i].getName() + "/>");
						}
						else
						{
							sb.append("<" + fields[i].getName() + ">");
							sb.append(fields[i].get(this));
							sb.append("</" + fields[i].getName() + ">");
						}
          }
        }
      }
      sb.append("</AddOnVO>");
    }

    catch (Exception e)
    {
      e.printStackTrace();
    }

    return sb.toString();
  }


  private String trim(String str)
  {
    return (str != null)?str.trim():str;
  }


}