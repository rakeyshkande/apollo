package com.ftd.customerordermanagement.vo;

import java.io.Serializable;
import java.math.BigDecimal;

public class GiftCodeUpdateVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6834916525431773339L;
	private String giftCodeId;
    private String status;
    private BigDecimal redemptionAmount;
    private String redeemedReferenceNumber;
   
   	public GiftCodeUpdateVO() {
		super();
	}
	public GiftCodeUpdateVO(String giftCodeId, String status,
			BigDecimal redemptionAmount, String redeemedReferenceNumber) {
		super();
		this.giftCodeId = giftCodeId;
		this.status = status;
		this.redemptionAmount = redemptionAmount;
		this.redeemedReferenceNumber = redeemedReferenceNumber;
	}
	
	public String getGiftCodeId() {
		return giftCodeId;
	}
	public void setGiftCodeId(String giftCodeId) {
		this.giftCodeId = giftCodeId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public BigDecimal getRedemptionAmount() {
		return redemptionAmount;
	}
	public void setRedemptionAmount(BigDecimal redemptionAmount) {
		this.redemptionAmount = redemptionAmount;
	}
	public String getRedeemedReferenceNumber() {
		return redeemedReferenceNumber;
	}
	public void setRedeemedReferenceNumber(String redeemedReferenceNumber) {
		this.redeemedReferenceNumber = redeemedReferenceNumber;
	}
	
		
	@Override
	public String toString() {
		return " GiftCodeUpdateVO [giftCodeId=" + giftCodeId
				+ ", giftCodeId=" + giftCodeId + ", status="
				+ status + ", redemptionAmount=" + redemptionAmount.toString()
				+ ", redeemedReferenceNumber=" + redeemedReferenceNumber + "]";
	}
	
    
}

