package com.ftd.customerordermanagement.vo;
import java.lang.reflect.Field;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class AcctTransVO extends BaseVO implements XMLInterface
{
  private	long	    tranId;
  private	String	    tranType;
  private	Date        tranDate;
  private	String	    orderDetailId;
  private   Date        deliveryDate;
  private	String	    productId;
  private	String	    sourceCode;
  private	String	    shipMethod;
  private	double      productAmount;
  private	double 	    addOnAmount;
  private	double      shippingFee;
  private	double      serviceFee;
  private	double      discountAmount;
  private	double      shippingTax;
  private	double      serviceFeeTax;
  private	double      tax;
  private	String      paymentType;
  private	String      refundDispCode;
  private	double      commissionAmount;
  private	double      wholesaleAmount;
  private	double      adminFee;
  private	String      refundId;
  private	String      orderBillId;
  private   String      newOrderSeq;
  private   double      wholesaleServiceFee;

  public AcctTransVO()
  {
  }

    public void setTranId(long tranId)
    {
      if(valueChanged(this.tranId, tranId))
      {
        setChanged(true);
      }
        this.tranId = tranId;
    }


    public long getTranId()
    {
        return tranId;
    }


    public void setTranType(String tranType)
    {
      if(valueChanged(this.tranType, tranType))
      {
        setChanged(true);
      }
        this.tranType = tranType;
    }


    public String getTranType()
    {
        return tranType;
    }


    public void setTranDate(Date tranDate)
    {
      if(valueChanged(this.tranDate, tranDate))
      {
        setChanged(true);
      }
        this.tranDate = tranDate;
    }


    public Date getTranDate()
    {
        return tranDate;
    }


    public void setOrderDetailId(String orderDetailId)
    {
      if(valueChanged(this.orderDetailId, orderDetailId))
      {
        setChanged(true);
      }
        this.orderDetailId = orderDetailId;
    }


    public String getOrderDetailId()
    {
        return orderDetailId;
    }


    public void setDeliveryDate(Date deliveryDate)
    {
      if(valueChanged(this.deliveryDate, deliveryDate))
      {
        setChanged(true);
      }
        this.deliveryDate = deliveryDate;
    }


    public Date getDeliveryDate()
    {
        return deliveryDate;
    }


    public void setProductId(String productId)
    {
      if(valueChanged(this.productId, productId))
      {
        setChanged(true);
      }
        this.productId = productId;
    }


    public String getProductId()
    {
        return productId;
    }


    public void setSourceCode(String sourceCode)
    {
      if(valueChanged(this.sourceCode, sourceCode))
      {
        setChanged(true);
      }
        this.sourceCode = sourceCode;
    }


    public String getSourceCode()
    {
        return sourceCode;
    }


    public void setShipMethod(String shipMethod)
    {
      if(valueChanged(this.shipMethod, shipMethod))
      {
        setChanged(true);
      }
        this.shipMethod = shipMethod;
    }


    public String getShipMethod()
    {
        return shipMethod;
    }


    public void setProductAmount(double productAmount)
    {
      if(valueChanged(new Double(this.productAmount), new Double(productAmount)))
      {
        setChanged(true);
      }
        this.productAmount = productAmount;
    }


    public double getProductAmount()
    {
        return productAmount;
    }


    public void setAddOnAmount(double addOnAmount)
    {
      if(valueChanged(new Double(this.addOnAmount), new Double(addOnAmount)))
      {
        setChanged(true);
      }
      this.addOnAmount = addOnAmount;
    }


    public double getAddOnAmount()
    {
        return addOnAmount;
    }


    public void setShippingFee(double shippingFee)
    {
      if(valueChanged(new Double(this.shippingFee), new Double(shippingFee)))
      {
        setChanged(true);
      }
      this.shippingFee = shippingFee;
    }


    public double getShippingFee()
    {
        return shippingFee;
    }


    public void setServiceFee(double serviceFee)
    {
      if(valueChanged(new Double(this.serviceFee), new Double(serviceFee)))
      {
        setChanged(true);
      }
      this.serviceFee = serviceFee;
    }


    public double getServiceFee()
    {
        return serviceFee;
    }


    public void setDiscountAmount(double discountAmount)
    {
      if(valueChanged(new Double(this.discountAmount), new Double(discountAmount)))
      {
        setChanged(true);
      }
      this.discountAmount = discountAmount;
    }


    public double getDiscountAmount()
    {
        return discountAmount;
    }


    public void setShippingTax(double shippingTax)
    {
      if(valueChanged(new Double(this.shippingTax), new Double(shippingTax)))
      {
        setChanged(true);
      }
      this.shippingTax = shippingTax;
    }


    public double getShippingTax()
    {
        return shippingTax;
    }


    public void setServiceFeeTax(double serviceFeeTax)
    {
      if(valueChanged(new Double(this.serviceFeeTax), new Double(serviceFeeTax)))
      {
        setChanged(true);
      }
      this.serviceFeeTax = serviceFeeTax;
    }


    public double getServiceFeeTax()
    {
        return serviceFeeTax;
    }


    public void setTax(double tax)
    {
      if(valueChanged(new Double(this.tax), new Double(tax)))
      {
        setChanged(true);
      }
      this.tax = tax;
    }


    public double getTax()
    {
        return tax;
    }


    public void setPaymentType(String paymentType)
    {
      if(valueChanged(this.paymentType, paymentType))
      {
        setChanged(true);
      }
        this.paymentType = paymentType;
    }


    public String getPaymentType()
    {
        return paymentType;
    }


    public void setRefundDispCode(String refundDispCode)
    {
      if(valueChanged(this.refundDispCode, refundDispCode))
      {
        setChanged(true);
      }
        this.refundDispCode = refundDispCode;
    }


    public String getRefundDispCode()
    {
        return refundDispCode;
    }


    public void setCommissionAmount(double commissionAmount)
    {
      if(valueChanged(new Double(this.commissionAmount), new Double(commissionAmount)))
      {
        setChanged(true);
      }
      this.commissionAmount = commissionAmount;
    }


    public double getCommissionAmount()
    {
        return commissionAmount;
    }


    public void setWholesaleAmount(double wholesaleAmount)
    {
      if(valueChanged(new Double(this.wholesaleAmount), new Double(wholesaleAmount)))
      {
        setChanged(true);
      }
      this.wholesaleAmount = wholesaleAmount;
    }


    public double getWholesaleAmount()
    {
        return wholesaleAmount;
    }


    public void setAdminFee(double adminFee)
    {
      if(valueChanged(new Double(this.adminFee), new Double(adminFee)))
      {
        setChanged(true);
      }
      this.adminFee = adminFee;
    }


    public double getAdminFee()
    {
        return adminFee;
    }


    public void setRefundId(String refundId)
    {
      if(valueChanged(this.refundId, refundId))
      {
        setChanged(true);
      }
        this.refundId = refundId;
    }


    public String getRefundId()
    {
        return refundId;
    }


    public void setOrderBillId(String orderBillId)
    {
      if(valueChanged(this.orderBillId, orderBillId))
      {
        setChanged(true);
      }
        this.orderBillId = orderBillId;
    }


    public String getOrderBillId()
    {
        return orderBillId;
    }


    public void setNewOrderSeq(String newOrderSeq)
    {
      if(valueChanged(this.newOrderSeq, newOrderSeq))
      {
        setChanged(true);
      }
        this.newOrderSeq = newOrderSeq;
    }


    public String getNewOrderSeq()
    {
        return newOrderSeq;
    }


    public void setWholesaleServiceFee(double wholesaleServiceFee)
    {
      if(valueChanged(new Double(this.wholesaleServiceFee), new Double(wholesaleServiceFee)))
      {
        setChanged(true);
      }
      this.wholesaleServiceFee = wholesaleServiceFee;
    }


    public double getWholesaleServiceFee()
    {
        return wholesaleServiceFee;
    }


  /**
    * This method uses the Reflection API to generate an XML string that will be
    * passed back to the calling module.
    * The XML string will contain all the fields within this VO, including the
    * variables as well as (a collection of) ValueObjects.
    *
    * @param  None
    * @return XML string
   **/
  public String toXML()
  {
    StringBuffer sb = new StringBuffer();
    try
    {
      sb.append("<AcctTransVO>");
      Field[] fields = this.getClass().getDeclaredFields();

      for (int i = 0; i < fields.length; i++)
      {
        //if the field retrieved was a list of VO
        if(fields[i].getType().equals(Class.forName("java.util.List")))
        {
          List list = (List)fields[i].get(this);
          if(list != null)
          {
            for (int j = 0; j < list.size(); j++)
            {
              XMLInterface xmlInt = (XMLInterface)list.get(j);
              String sXmlVO = xmlInt.toXML();
              sb.append(sXmlVO);
            }
          }
        }
        else
        {
          //if the field retrieved was a VO
          if (fields[i].getType().toString().matches("(?i).*vo"))
          {
            XMLInterface xmlInt = (XMLInterface)fields[i].get(this);
            String sXmlVO = xmlInt.toXML();
            sb.append(sXmlVO);
          }
          //if the field retrieved was a Calendar object
          else if (fields[i].getType().toString().matches("(?i).*calendar"))
          {
            Date date;
            String fDate = null;
            if (fields[i].get(this) != null)
            {
              date = (((GregorianCalendar)fields[i].get(this)).getTime());
              SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss a zzz");
              fDate = sdf.format(date).toString();
            }
            sb.append("<" + fields[i].getName() + ">");
            sb.append(fDate);
            sb.append("</" + fields[i].getName() + ">");
          }
          else
          {
            sb.append("<" + fields[i].getName() + ">");
            sb.append(fields[i].get(this));
            sb.append("</" + fields[i].getName() + ">");
          }
        }
      }
      sb.append("</AcctTransVO>");
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
    return sb.toString();
  }


  /**
    * This method uses the Reflection API to generate an XML string that will be
    * passed back to the calling module.
    * The XML string will contain all the fields within this VO, including the
    * variables as well as (a collection of) ValueObjects.
    *
    * @param  None
    * @return XML string
   **/
  public String toXML(int count)
  {
    StringBuffer sb = new StringBuffer();
    try
    {
      if (count == 0)
      {
        sb.append("<AcctTransVO>");
      }
      else
      {
        sb.append("<AcctTransVO num=" + '"' + count + '"' + ">");
      }
      Field[] fields = this.getClass().getDeclaredFields();

      for (int i = 0; i < fields.length; i++)
      {
        //if the field retrieved was a list of VO
        if(fields[i].getType().equals(Class.forName("java.util.List")))
        {
          List list = (List)fields[i].get(this);
          if(list != null)
          {
            for (int j = 0; j < list.size(); j++)
            {
              XMLInterface xmlInt = (XMLInterface)list.get(j);
              int k = j + 1;
              String sXmlVO = xmlInt.toXML(k);
              sb.append(sXmlVO);
            }
          }
        }
        else
        {
          //if the field retrieved was a VO
          if (fields[i].getType().toString().matches("(?i).*vo"))
          {
            XMLInterface xmlInt = (XMLInterface)fields[i].get(this);
            int k = i + 1;
            String sXmlVO = xmlInt.toXML(k);
            sb.append(sXmlVO);
          }
          //if the field retrieved was a Calendar object
          else if (fields[i].getType().toString().matches("(?i).*calendar"))
          {
            Date date;
            String fDate = null;
            if (fields[i].get(this) != null)
            {
              date = (((GregorianCalendar)fields[i].get(this)).getTime());
              SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss a zzz");
              fDate = sdf.format(date).toString();
  	          sb.append("<" + fields[i].getName() + ">");
              sb.append(fDate);
              sb.append("</" + fields[i].getName() + ">");
            }
            else
            {
			  sb.append("<" + fields[i].getName() + "/>");
            }
          }
          else
          {
			if (fields[i].get(this) == null)
			{
			  sb.append("<" + fields[i].getName() + "/>");
			}
			else
			{
			  sb.append("<" + fields[i].getName() + ">");
			  sb.append(fields[i].get(this));
			  sb.append("</" + fields[i].getName() + ">");
			}
          }
        }
      }
      sb.append("</AcctTransVO>");
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
    return sb.toString();
  }
}