package com.ftd.customerordermanagement.vo;
import java.lang.reflect.Field;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

public class RecipientVO extends BaseVO implements XMLInterface
{
  private long 		  customerId;
  private String 		concatId;
  private String 		firstName;
  private String 		lastName;
  private String 		businessName;
  private String 		address1;
  private String 		address2;
  private String 		city;
  private String 		state;
  private String 		zipCode;
  private String 		country;
  private String 		addressType;
  private char 		  preferredCustomer;
  private char 		  buyerIndicator;
  private char 		  recipientIndicator;
  private String 		origin;
  private Calendar 	firstOrderDate;
  private Calendar 	createdOn;
  private String 		createdBy;
  private Calendar 	updatedOn;
  private String 		updatedBy;

  private List      recipientHistoryVOList;
  private List      recipientPhoneVOList;
  private List      recipientCommentsVOList;
  private List      recipientEmailVOList;


  public RecipientVO()
  {
    recipientHistoryVOList  = new ArrayList();
    recipientPhoneVOList    = new ArrayList();
    recipientCommentsVOList = new ArrayList();
    recipientEmailVOList    = new ArrayList();
  }

    public void setCustomerId(long customerId)
  {
    if(valueChanged(this.customerId, customerId ))
    {
      setChanged(true);
    }
    this.customerId = customerId;
  }


  public long getCustomerId()
  {
    return customerId;
  }


  public void setCreatedOn(Calendar createdOn)
  {
    if(valueChanged(this.createdOn, createdOn))
    {
      setChanged(true);
    }
    this.createdOn = createdOn;
  }


  public Calendar getCreatedOn()
  {
    return createdOn;
  }


  public void setCreatedBy(String createdBy)
  {
    if(valueChanged(this.createdBy, createdBy))
    {
      setChanged(true);
    }

    this.createdBy = trim(createdBy);
  }


  public String getCreatedBy()
  {
    return createdBy;
  }


  public void setUpdatedOn(Calendar updatedOn)
  {
    if(valueChanged(this.updatedOn, updatedOn ))
    {
      setChanged(true);
    }
    this.updatedOn = updatedOn;
  }


  public Calendar getUpdatedOn()
  {
    return updatedOn;
  }


  public void setUpdatedBy(String updatedBy)
  {
    if(valueChanged(this.updatedBy, updatedBy ))
    {
      setChanged(true);
    }
    this.updatedBy = trim(updatedBy);
  }


  public String getUpdatedBy()
  {
    return updatedBy;
  }


  public void setConcatId(String concatId)
  {
    if(valueChanged(this.concatId, concatId))
    {
      setChanged(true);
    }
    this.concatId = trim(concatId);
  }


  public String getConcatId()
  {
    return concatId;
  }


  public void setFirstName(String firstName)
  {
    if(valueChanged(this.firstName, firstName))
    {
      setChanged(true);
    }
    this.firstName = trim(firstName);
  }


  public String getFirstName()
  {
    return firstName;
  }


  public void setLastName(String lastName)
  {
    if(valueChanged(this.lastName, lastName))
    {
      setChanged(true);
    }
    this.lastName = trim(lastName);
  }


  public String getLastName()
  {
    return lastName;
  }


  public void setBusinessName(String businessName)
  {
    if(valueChanged(this.businessName, businessName))
    {
      setChanged(true);
    }
    this.businessName = trim(businessName);
  }


  public String getBusinessName()
  {
    return businessName;
  }


  public void setAddress1(String address1)
  {
    if(valueChanged(this.address1, address1))
    {
      setChanged(true);
    }
    this.address1 = trim(address1);
  }


  public String getAddress1()
  {
    return address1;
  }


  public void setAddress2(String address2)
  {
    if(valueChanged(this.address2, address2))
    {
      setChanged(true);
    }
    this.address2 = trim(address2);
  }


  public String getAddress2()
  {
    return address2;
  }


  public void setCity(String city)
  {
    if(valueChanged(this.city, city))
    {
      setChanged(true);
    }
    this.city = trim(city);
  }


  public String getCity()
  {
    return city;
  }


  public void setState(String state)
  {
    if(valueChanged(this.state, state))
    {
      setChanged(true);
    }
    this.state = trim(state);
  }


  public String getState()
  {
    return state;
  }


  public void setZipCode(String zipCode)
  {
    if(valueChanged(this.zipCode, zipCode))
    {
      setChanged(true);
    }
    this.zipCode = trim(zipCode);
  }


  public String getZipCode()
  {
    return zipCode;
  }


  public void setCountry(String country)
  {
    if(valueChanged(this.country, country))
    {
      setChanged(true);
    }
    this.country = trim(country);
  }


  public String getCountry()
  {
    return country;
  }


  public void setAddressType(String addressType)
  {
    if(valueChanged(this.addressType, addressType))
    {
      setChanged(true);
    }
    this.addressType = trim(addressType);
  }


  public String getAddressType()
  {
    return addressType;
  }


  public void setPreferredCustomer(char preferredCustomer)
  {
    if(valueChanged(this.preferredCustomer, preferredCustomer))
    {
      setChanged(true);
    }
    this.preferredCustomer = preferredCustomer;
  }


  public char getPreferredCustomer()
  {
    return preferredCustomer;
  }


  public void setBuyerIndicator(char buyerIndicator)
  {
    if(valueChanged(this.buyerIndicator, buyerIndicator))
    {
      setChanged(true);
    }
    this.buyerIndicator = buyerIndicator;
  }


  public char getBuyerIndicator()
  {
    return buyerIndicator;
  }


  public void setRecipientIndicator(char recipientIndicator)
  {
    if(valueChanged(this.recipientIndicator, recipientIndicator))
    {
      setChanged(true);
    }
    this.recipientIndicator = recipientIndicator;
  }


  public char getRecipientIndicator()
  {
    return recipientIndicator;
  }


  public void setOrigin(String origin)
  {
    if(valueChanged(this.origin, origin))
    {
      setChanged(true);
    }
    this.origin = trim(origin);
  }


  public String getOrigin()
  {
    return origin;
  }


  public void setFirstOrderDate(Calendar firstOrderDate)
  {
    if(valueChanged(this.firstOrderDate, firstOrderDate))
    {
      setChanged(true);
    }
    this.firstOrderDate = firstOrderDate;
  }


  public Calendar getFirstOrderDate()
  {
    return firstOrderDate;
  }


  public void setRecipientHistoryVOList(List recipientHistoryVOList)
  {
		if(recipientHistoryVOList != null)
		{
      Iterator it = recipientHistoryVOList.iterator();
      while(it.hasNext())
      {
        RecipientHistoryVO recipientHistoryVO = (RecipientHistoryVO) it.next();
        recipientHistoryVO.setChanged(true);
      }
		}
    this.recipientHistoryVOList = recipientHistoryVOList;
  }


  public List getRecipientHistoryVOList()
  {
    return recipientHistoryVOList;
  }


  public void setRecipientPhoneVOList(List recipientPhoneVOList)
  {
		if(recipientPhoneVOList != null)
		{
      Iterator it = recipientPhoneVOList.iterator();
      while(it.hasNext())
      {
        RecipientPhoneVO recipientPhoneVO = (RecipientPhoneVO) it.next();
        recipientPhoneVO.setChanged(true);
      }
		}
    this.recipientPhoneVOList = recipientPhoneVOList;
  }


  public List getRecipientPhoneVOList()
  {
    return recipientPhoneVOList;
  }


  public void setRecipientCommentsVOList(List recipientCommentsVOList)
  {
		if(recipientCommentsVOList != null)
		{
      Iterator it = recipientCommentsVOList.iterator();
      while(it.hasNext())
      {
        RecipientCommentsVO recipientCommentsVO = (RecipientCommentsVO) it.next();
        recipientCommentsVO.setChanged(true);
      }
		}
    this.recipientCommentsVOList = recipientCommentsVOList;
  }


  public List getRecipientCommentsVOList()
  {
    return recipientCommentsVOList;
  }


  public void setRecipientEmailVOList(List recipientEmailVOList)
  {
		if(recipientEmailVOList != null)
		{
      Iterator it = recipientEmailVOList.iterator();
      while(it.hasNext())
      {
        RecipientEmailVO recipientEmailVO = (RecipientEmailVO) it.next();
        recipientEmailVO.setChanged(true);
      }
		}
    this.recipientEmailVOList = recipientEmailVOList;
  }


  public List getRecipientEmailVOList()
  {
    return recipientEmailVOList;
  }


  /**
    * This method uses the Reflection API to generate an XML string that will be
    * passed back to the calling module.
    * The XML string will contain all the fields within this VO, including the
    * variables as well as (a collection of) ValueObjects.
    *
    * @param  None
    * @return XML string
   **/
  public String toXML()
  {
    StringBuffer sb = new StringBuffer();
    try
    {
      sb.append("<RecipientVO>");
      Field[] fields = this.getClass().getDeclaredFields();

      for (int i = 0; i < fields.length; i++)
      {
        //if the field retrieved was a list of VO
        if(fields[i].getType().equals(Class.forName("java.util.List")))
        {
          List list = (List)fields[i].get(this);
          if(list != null)
          {
            for (int j = 0; j < list.size(); j++)
            {
              XMLInterface xmlInt = (XMLInterface)list.get(j);
              String sXmlVO = xmlInt.toXML();
              sb.append(sXmlVO);
            }
          }
        }
        else
        {
          //if the field retrieved was a VO
          if (fields[i].getType().toString().matches("(?i).*vo"))
          {
            XMLInterface xmlInt = (XMLInterface)fields[i].get(this);
            String sXmlVO = xmlInt.toXML();
            sb.append(sXmlVO);
          }
          //if the field retrieved was a Calendar object
          else if (fields[i].getType().toString().matches("(?i).*calendar"))
          {
            Date date;
            String fDate = null;
            if (fields[i].get(this) != null)
            {
              date = (((GregorianCalendar)fields[i].get(this)).getTime());
              SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss a zzz");
              fDate = sdf.format(date).toString();
            }
            sb.append("<" + fields[i].getName() + ">");
            sb.append(fDate);
            sb.append("</" + fields[i].getName() + ">");

          }
          else
          {
            sb.append("<" + fields[i].getName() + ">");
            sb.append(fields[i].get(this));
            sb.append("</" + fields[i].getName() + ">");
          }
        }
      }
      sb.append("</RecipientVO>");
    }

    catch (Exception e)
    {
      e.printStackTrace();
    }

    return sb.toString();
  }


  /**
    * This method uses the Reflection API to generate an XML string that will be
    * passed back to the calling module.
    * The XML string will contain all the fields within this VO, including the
    * variables as well as (a collection of) ValueObjects.
    *
    * @param  None
    * @return XML string
   **/
  public String toXML(int count)
  {
    StringBuffer sb = new StringBuffer();
    try
    {
      if (count == 0)
      {
        sb.append("<RecipientVO>");
      }
      else
      {
        sb.append("<RecipientVO num=" + '"' + count + '"' + ">");
      }
      Field[] fields = this.getClass().getDeclaredFields();

      for (int i = 0; i < fields.length; i++)
      {
        //if the field retrieved was a list of VO
        if(fields[i].getType().equals(Class.forName("java.util.List")))
        {
          List list = (List)fields[i].get(this);
          if(list != null)
          {
            for (int j = 0; j < list.size(); j++)
            {
              XMLInterface xmlInt = (XMLInterface)list.get(j);
              int k = j + 1;
              String sXmlVO = xmlInt.toXML(k);
              sb.append(sXmlVO);
            }
          }
        }
        else
        {
          //if the field retrieved was a VO
          if (fields[i].getType().toString().matches("(?i).*vo"))
          {
            XMLInterface xmlInt = (XMLInterface)fields[i].get(this);
            int k = i + 1;
            String sXmlVO = xmlInt.toXML(k);
            sb.append(sXmlVO);
          }
          //if the field retrieved was a Calendar object
          else if (fields[i].getType().toString().matches("(?i).*calendar"))
          {
            Date date;
            String fDate = null;
            if (fields[i].get(this) != null)
            {
              date = (((GregorianCalendar)fields[i].get(this)).getTime());
              SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss a zzz");
              fDate = sdf.format(date).toString();
  	          sb.append("<" + fields[i].getName() + ">");
              sb.append(fDate);
              sb.append("</" + fields[i].getName() + ">");
            }
            else
            {
							sb.append("<" + fields[i].getName() + "/>");
            }

          }
          else
          {
						if (fields[i].get(this) == null)
						{
							sb.append("<" + fields[i].getName() + "/>");
						}
						else
						{
							sb.append("<" + fields[i].getName() + ">");
							sb.append(fields[i].get(this));
							sb.append("</" + fields[i].getName() + ">");
						}
          }
        }
      }
      sb.append("</RecipientVO>");
    }

    catch (Exception e)
    {
      e.printStackTrace();
    }

    return sb.toString();
  }


  private String trim(String str)
  {
    return (str != null)?str.trim():str;
  }


}