/**
 * 
 */
package com.ftd.customerordermanagement.vo;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.ftd.osp.utilities.vo.MembershipDetailsVO;
import com.ftd.util.adapters.CalendarTypeAdapter;

/**
 * @author smeka
 * 
 */

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class CustomerMemberShipVO implements Comparable<CustomerMemberShipVO> {

	private long emailId;
	private long customerId;
	private String companyId;
	private String emailAddress;
	private String subscribeStatus;

	@XmlJavaTypeAdapter(value = CalendarTypeAdapter.class)
	private Calendar updatedOn;

	private List<MembershipDetailsVO> membershipList;

	public long getEmailId() {
		return emailId;
	}

	public void setEmailId(long emailId) {
		this.emailId = emailId;
	}

	public long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getSubscribeStatus() {
		return subscribeStatus;
	}

	public void setSubscribeStatus(String subscribeStatus) {
		this.subscribeStatus = subscribeStatus;
	}

	public Calendar getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Calendar updatedOn) {
		this.updatedOn = updatedOn;
	}

	public List<MembershipDetailsVO> getMembershipList() {
		if (membershipList == null) {
			membershipList = new ArrayList<MembershipDetailsVO>();
		}
		return membershipList;
	}

	public void setMembershipList(List<MembershipDetailsVO> membershipList) {
		this.membershipList = membershipList;
	}

	@Override
	public int compareTo(CustomerMemberShipVO obj) {
		return this.emailAddress.compareTo(((CustomerMemberShipVO) obj)
				.getEmailAddress());
	}

}
