package com.ftd.customerordermanagement.vo;
import java.lang.reflect.Field;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;


public class GiftCardVO extends BaseVO implements XMLInterface
{
    private	String    gcCouponNumber;
    private	String	  updatedBy;
    private	String    status;
    private   String    expDate;
    private   String    balance; 
    private   String    pin;

    public void setGcCouponNumber(String gcCouponNumber)
    {
        if(valueChanged(this.gcCouponNumber, gcCouponNumber))
        {
            setChanged(true);
        }
        this.gcCouponNumber = gcCouponNumber;
    }


    public String getGcCouponNumber()
    {
        return gcCouponNumber;
    }


    public void setUpdatedBy(String updatedBy)
    {
        if(valueChanged(this.updatedBy, updatedBy))
        {
            setChanged(true);
        }
        this.updatedBy = updatedBy;
    }


    public String getUpdatedBy()
    {
        return updatedBy;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }


    public String getStatus()
    {
        return status;
    }


    public void setExpDate(String expDate)
    {
        this.expDate = expDate;
    }


    public String getExpDate()
    {
        return expDate;
    }


    public void setBalance(String balance)
    {
        this.balance = balance;
    }


    public String getBalance()
    {
        return balance;
    }


    public void setPin(String pin)
    {
        if(valueChanged(this.pin, pin))
        {
            setChanged(true);
        }
        this.pin = pin;
    }


    public String getPin()
    {
        return pin;
    }
    
    public CreditCardVO getCreditCardVO() {
    	CreditCardVO ccvo = new CreditCardVO();
    	ccvo.setCcNumber(gcCouponNumber);
    	ccvo.setCcType("GD");
    	ccvo.setCcExpiration(expDate);
    	ccvo.setUpdatedBy(updatedBy);
    	ccvo.setGiftCardPin(pin);
    	
    	return ccvo;
    }

    /**
     * This method uses the Reflection API to generate an XML string that will be
     * passed back to the calling module.
     * The XML string will contain all the fields within this VO, including the
     * variables as well as (a collection of) ValueObjects.
     *
     * @param  None
     * @return XML string
     **/
    public String toXML()
    {
        StringBuffer sb = new StringBuffer();
        try
        {
            sb.append("<GiftCardVO>");
            Field[] fields = this.getClass().getDeclaredFields();

            for (int i = 0; i < fields.length; i++)
            {
                //if the field retrieved was a list of VO
                if(fields[i].getType().equals(Class.forName("java.util.List")))
                {
                    List list = (List)fields[i].get(this);
                    if(list != null)
                    {
                        for (int j = 0; j < list.size(); j++)
                        {
                            XMLInterface xmlInt = (XMLInterface)list.get(j);
                            String sXmlVO = xmlInt.toXML();
                            sb.append(sXmlVO);
                        }
                    }
                }
                else
                {
                    //if the field retrieved was a VO
                    if (fields[i].getType().toString().matches("(?i).*vo"))
                    {
                        XMLInterface xmlInt = (XMLInterface)fields[i].get(this);
                        String sXmlVO = xmlInt.toXML();
                        sb.append(sXmlVO);
                    }
                    //if the field retrieved was a Calendar object
                    else if (fields[i].getType().toString().matches("(?i).*calendar"))
                    {
                        Date date;
                        String fDate = null;
                        if (fields[i].get(this) != null)
                        {
                            date = (((GregorianCalendar)fields[i].get(this)).getTime());
                            SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss a zzz");
                            fDate = sdf.format(date).toString();
                        }
                        sb.append("<" + fields[i].getName() + ">");
                        sb.append(fDate);
                        sb.append("</" + fields[i].getName() + ">");

                    }
                    else
                    {
                        sb.append("<" + fields[i].getName() + ">");
                        sb.append(fields[i].get(this));
                        sb.append("</" + fields[i].getName() + ">");
                    }
                }
            }
            sb.append("</GiftCardVO>");
        }

        catch (Exception e)
        {
            e.printStackTrace();
        }
        return sb.toString();
    }


    /**
     * This method uses the Reflection API to generate an XML string that will be
     * passed back to the calling module.
     * The XML string will contain all the fields within this VO, including the
     * variables as well as (a collection of) ValueObjects.
     *
     * @param  None
     * @return XML string
     **/
    public String toXML(int count)
    {
        StringBuffer sb = new StringBuffer();
        try
        {
            if (count == 0)
            {
                sb.append("<GiftCardVO>");
            }
            else
            {
                sb.append("<GiftCardVO num=" + '"' + count + '"' + ">");
            }
            Field[] fields = this.getClass().getDeclaredFields();

            for (int i = 0; i < fields.length; i++)
            {
                //if the field retrieved was a list of VO
                if(fields[i].getType().equals(Class.forName("java.util.List")))
                {
                    List list = (List)fields[i].get(this);
                    if(list != null)
                    {
                        for (int j = 0; j < list.size(); j++)
                        {
                            XMLInterface xmlInt = (XMLInterface)list.get(j);
                            int k = j + 1;
                            String sXmlVO = xmlInt.toXML(k);
                            sb.append(sXmlVO);
                        }
                    }
                }
                else
                {
                    //if the field retrieved was a VO
                    if (fields[i].getType().toString().matches("(?i).*vo"))
                    {
                        XMLInterface xmlInt = (XMLInterface)fields[i].get(this);
                        int k = i + 1;
                        String sXmlVO = xmlInt.toXML(k);
                        sb.append(sXmlVO);
                    }
                    //if the field retrieved was a Calendar object
                    else if (fields[i].getType().toString().matches("(?i).*calendar"))
                    {
                        Date date;
                        String fDate = null;
                        if (fields[i].get(this) != null)
                        {
                            date = (((GregorianCalendar)fields[i].get(this)).getTime());
                            SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss a zzz");
                            fDate = sdf.format(date).toString();
                            sb.append("<" + fields[i].getName() + ">");
                            sb.append(fDate);
                            sb.append("</" + fields[i].getName() + ">");
                        }
                        else
                        {
                            sb.append("<" + fields[i].getName() + "/>");
                        }

                    }
                    else
                    {
                        if (fields[i].get(this) == null)
                        {
                            sb.append("<" + fields[i].getName() + "/>");
                        }
                        else
                        {
                            sb.append("<" + fields[i].getName() + ">");
                            sb.append(fields[i].get(this));
                            sb.append("</" + fields[i].getName() + ">");
                        }
                    }
                }
            }
            sb.append("</GiftCardVO>");
        }

        catch (Exception e)
        {
            e.printStackTrace();
        }
        return sb.toString();
    }


    private String trim(String str)
    {
        return (str != null)?str.trim():str;
    }

}