package com.ftd.customerordermanagement.vo;
import java.lang.reflect.Field;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;


public class OrderDetailVO extends BaseVO implements XMLInterface
{
  private	String	  card_message;
  private	String	  card_signature;
  private	String    color_1;
  private	String    color_1_description;
  private	String    color_2;
  private	String    color_2_description;
  private	String 	  created_by;
  private	Calendar	created_on;
  private	Calendar	delivery_date;
  private	Calendar	delivery_date_range_end;
  private	Calendar	eod_delivery_date;
  private	String	  eod_delivery_indicator;
  private	String	  external_order_number;
  private	String	  florist_id;
  private	String	  hp_order_number;
  private	double    miles_point;
  private	String	  occasion;
  private	String    order_detail_id;
  private	String	  order_disp_code;
  private	String	  order_guid;
  private	String    product_id;
  private	long	    quantity;
  private	String    recipient_id;
  private	String	  release_info_indicator;
  private	String	  same_day_gift;
  private	String	  second_choice_product;
  private	Calendar	ship_date;
  private	String	  ship_method;
  private	String 	  size_indicator;
  private	String	  source_code;
  private	String	  special_instructions;
  private	String	  subcode;
  private	String	  substitution_indicator;
  private	String 	  updated_by;
  private	Calendar	updated_on;

  private       List      orderDetailHoldVOList;
  private       List      addOnVOList;
  private       List      recipientVOList;
  private       List      orderHistoryVOList;
  private       List      commentsVOList;
  private       List      orderBillVOList;
  private       List      orderTrackingVOList;
  private       String    personalGreetingId;

  private String recipientCountry;
  private String avsAddressId;


  public OrderDetailVO()
  {
    orderDetailHoldVOList = new ArrayList();
    addOnVOList           = new ArrayList();
    recipientVOList       = new ArrayList();
    orderHistoryVOList    = new ArrayList();
    commentsVOList        = new ArrayList();
    orderBillVOList       = new ArrayList();
    orderTrackingVOList   = new ArrayList();
  }


  public void setOrderDetailId(String order_detail_id)
  {
    if(valueChanged(this.order_detail_id, order_detail_id))
    {
      setChanged(true);
    }
    this.order_detail_id = order_detail_id;
  }


  public String getOrderDetailId()
  {
    return order_detail_id;
  }


  public void setOrderGuid(String order_guid)
  {
    if(valueChanged(this.order_guid, order_guid))
    {
      setChanged(true);
    }
    this.order_guid = trim(order_guid);
  }


  public String getOrderGuid()
  {
    return order_guid;
  }


  public void setCreatedOn(Calendar created_on)
  {
    if(valueChanged(this.created_on, created_on))
    {
      setChanged(true);
    }
    this.created_on = created_on;
  }


  public Calendar getCreatedOn()
  {
    return created_on;
  }


  public void setCreatedBy(String created_by)
  {
    if(valueChanged(this.created_by, created_by))
    {
      setChanged(true);
    }

    this.created_by = trim(created_by);
  }


  public String getCreatedBy()
  {
    return created_by;
  }


  public void setUpdatedOn(Calendar updated_on)
  {
    if(valueChanged(this.updated_on, updated_on ))
    {
      setChanged(true);
    }
    this.updated_on = updated_on;
  }


  public Calendar getUpdatedOn()
  {
    return updated_on;
  }


  public void setUpdatedBy(String updated_by)
  {
    if(valueChanged(this.updated_by, updated_by ))
    {
      setChanged(true);
    }
    this.updated_by = trim(updated_by);
  }


  public String getUpdatedBy()
  {
    return updated_by;
  }


  public void setExternalOrderNumber(String external_order_number)
  {
    if(valueChanged(this.external_order_number, external_order_number))
    {
      setChanged(true);
    }
    this.external_order_number = trim(external_order_number);
  }


  public String getExternalOrderNumber()
  {
    return external_order_number;
  }


  public void setHpOrderNumber(String hp_order_number)
  {
    if(valueChanged(this.hp_order_number, hp_order_number))
    {
      setChanged(true);
    }
    this.hp_order_number = trim(hp_order_number);
  }


  public String getHpOrderNumber()
  {
    return hp_order_number;
  }


  public void setSourceCode(String source_code)
  {
    if(valueChanged(this.source_code, source_code))
    {
      setChanged(true);
    }
    this.source_code = trim(source_code);
  }


  public String getSourceCode()
  {
    return source_code;
  }


  public void setDeliveryDate(Calendar delivery_date)
  {
    if(valueChanged(this.delivery_date, delivery_date))
    {
      setChanged(true);
    }
    this.delivery_date = delivery_date;
  }


  public Calendar getDeliveryDate()
  {
    return delivery_date;
  }



  public void setDeliveryDateRangeEnd(Calendar delivery_date_range_end)
  {
    if(valueChanged(this.delivery_date_range_end, delivery_date_range_end))
    {
      setChanged(true);
    }
    this.delivery_date_range_end = delivery_date_range_end;
  }


  public Calendar getDeliveryDateRangeEnd()
  {
    return delivery_date_range_end;
  }



  public void setEODDeliveryDate(Calendar eod_delivery_date)
  {
    if(valueChanged(this.eod_delivery_date, eod_delivery_date))
    {
      setChanged(true);
    }
    this.eod_delivery_date = eod_delivery_date;
  }


  public Calendar getEODDeliveryDate()
  {
    return eod_delivery_date;
  }



  public void setRecipientId(String recipient_id)
  {
    if(valueChanged(this.recipient_id, recipient_id))
    {
      setChanged(true);
    }
    this.recipient_id = recipient_id;
  }


  public String getRecipientId()
  {
    return recipient_id;
  }


  public void setProductId(String product_id)
  {
    if(valueChanged(this.product_id, product_id))
    {
      setChanged(true);
    }
    this.product_id = product_id;
  }


  public String getProductId()
  {
    return product_id;
  }


  public void setQuantity(long quantity)
  {
     if(valueChanged(this.quantity, quantity))
    {
      setChanged(true);
    }
   this.quantity = quantity;
  }


  public long getQuantity()
  {
    return quantity;
  }


  public void setMilesPoint(double miles_point)
  {
     if(valueChanged(this.miles_point, miles_point))
    {
      setChanged(true);
    }
   this.miles_point = miles_point;
  }


  public double getMilesPoint()
  {
    return miles_point;
  }


  public void setColor1(String color_1)
  {
    if(valueChanged(this.color_1, color_1))
    {
      setChanged(true);
    }
    this.color_1 = color_1;
  }


  public String getColor1()
  {
    return color_1;
  }


  public void setColor1Description(String color_1_description)
  {
    if(valueChanged(this.color_1_description, color_1_description))
    {
      setChanged(true);
    }
    this.color_1_description = color_1_description;
  }


  public String getColor1Description()
  {
    return color_1_description;
  }


  public void setColor2(String color_2)
  {
    if(valueChanged(this.color_2, color_2))
    {
      setChanged(true);
    }
    this.color_2 = color_2;
  }


  public String getColor2()
  {
    return color_2;
  }


  public void setColor2Description(String color_2_description)
  {
    if(valueChanged(this.color_2_description, color_2_description))
    {
      setChanged(true);
    }
    this.color_2_description = color_2_description;
  }


  public String getColor2Description()
  {
    return color_2_description;
  }


  public void setSubstitutionIndicator(String substitution_indicator)
  {
    if(valueChanged(this.substitution_indicator, substitution_indicator))
    {
      setChanged(true);
    }
    this.substitution_indicator = trim(substitution_indicator);
  }


  public String getSubstitutionIndicator()
  {
    return substitution_indicator;
  }


  public void setSubcode(String subcode)
  {
    if(valueChanged(this.subcode, subcode))
    {
      setChanged(true);
    }
    this.subcode = trim(subcode);
  }


  public String getSubcode()
  {
    return subcode;
  }


  public void setSameDayGift(String same_day_gift)
  {
    if(valueChanged(this.same_day_gift, same_day_gift))
    {
      setChanged(true);
    }
    this.same_day_gift = trim(same_day_gift);
  }


  public String getSameDayGift()
  {
    return same_day_gift;
  }


  public void setSecondChoiceProduct(String second_choice_product)
  {
    if(valueChanged(this.second_choice_product, second_choice_product))
    {
      setChanged(true);
    }
    this.second_choice_product = trim(second_choice_product);
  }


  public String getSecondChoiceProduct()
  {
    return second_choice_product;
  }


  public void setOccasion(String occasion)
  {
    if(valueChanged(this.occasion, occasion))
    {
      setChanged(true);
    }
    this.occasion = trim(occasion);
  }


  public String getOccasion()
  {
    return occasion;
  }


  public void setCardMessage(String card_message)
  {
    if(valueChanged(this.card_message, card_message))
    {
      setChanged(true);
    }
    this.card_message = trim(card_message);
  }


  public String getCardMessage()
  {
    return card_message;
  }


  public void setCardSignature(String card_signature)
  {
    if(valueChanged(this.card_signature, card_signature))
    {
      setChanged(true);
    }
    this.card_signature = trim(card_signature);
  }


  public String getCardSignature()
  {
    return card_signature;
  }


  public void setSpecialInstructions(String special_instructions)
  {
    if(valueChanged(this.special_instructions, special_instructions))
    {
      setChanged(true);
    }
    this.special_instructions = trim(special_instructions);
  }


  public String getSpecialInstructions()
  {
    return special_instructions;
  }


  public void setReleaseInfoIndicator(String release_info_indicator)
  {
    if(valueChanged(this.release_info_indicator, release_info_indicator))
    {
      setChanged(true);
    }
    this.release_info_indicator = trim(release_info_indicator);
  }


  public String getReleaseInfoIndicator()
  {
    return release_info_indicator;
  }


  public void setEODDeliveryIndicator(String eod_delivery_indicator)
  {
    if(valueChanged(this.eod_delivery_indicator, eod_delivery_indicator))
    {
      setChanged(true);
    }
    this.eod_delivery_indicator = trim(eod_delivery_indicator);
  }


  public String getEODDeliveryIndicator()
  {
    return eod_delivery_indicator;
  }


  public void setFloristId(String florist_id)
  {
    if(valueChanged(this.florist_id, florist_id))
    {
      setChanged(true);
    }
    this.florist_id = trim(florist_id);
  }


  public String getFloristId()
  {
    return florist_id;
  }


  public void setShipMethod(String ship_method)
  {
    if(valueChanged(this.ship_method, ship_method))
    {
      setChanged(true);
    }
    this.ship_method = trim(ship_method);
  }


  public String getShipMethod()
  {
    return ship_method;
  }


  public void setShipDate(Calendar ship_date)
  {
    if(valueChanged(this.ship_date, ship_date))
    {
      setChanged(true);
    }
    this.ship_date = ship_date;
  }


  public Calendar getShipDate()
  {
    return ship_date;
  }


  public void setOrderDispCode(String order_disp_code)
  {
    if(valueChanged(this.order_disp_code, order_disp_code))
    {
      setChanged(true);
    }
    this.order_disp_code = trim(order_disp_code);
  }


  public String getOrderDispCode()
  {
    return order_disp_code;
  }


  public void setSizeIndicator(String size_indicator)
  {
    if(valueChanged(this.size_indicator, size_indicator))
    {
      setChanged(true);
    }
    this.size_indicator = size_indicator;
  }


  public String getSizeIndicator()
  {
    return size_indicator;
  }
  
  public void setPersonalGreetingId(String personalGreetingId)
  {
      if(valueChanged(this.personalGreetingId, personalGreetingId))
      {
        setChanged(true);
      }
      this.personalGreetingId = personalGreetingId;
  }

  public String getPersonalGreetingId()
  {
      return personalGreetingId;
  }




  public void setOrderDetailHoldVOList(List orderDetailHoldVOList)
  {
		if(orderDetailHoldVOList != null)
		{
      Iterator it = orderDetailHoldVOList.iterator();
      while(it.hasNext())
      {
        OrderDetailHoldVO orderDetailHoldVO = (OrderDetailHoldVO) it.next();
        orderDetailHoldVO.setChanged(true);
      }
		}
    this.orderDetailHoldVOList = orderDetailHoldVOList;
  }


  public List getOrderDetailHoldVOList()
  {
    return orderDetailHoldVOList;
  }


  public void setAddOnVOList(List addOnVOList)
  {
		if(addOnVOList != null)
		{
      Iterator it = addOnVOList.iterator();
      while(it.hasNext())
      {
        AddOnVO addOnVO = (AddOnVO) it.next();
        addOnVO.setChanged(true);
      }
		}
    this.addOnVOList = addOnVOList;
  }


  public List getAddOnVOList()
  {
    return addOnVOList;
  }


  public void setRecipientVOList(List recipientVOList)
  {
		if(recipientVOList != null)
		{
      Iterator it = recipientVOList.iterator();
      while(it.hasNext())
      {
        RecipientVO recipientVO = (RecipientVO) it.next();
        recipientVO.setChanged(true);
      }
		}
    this.recipientVOList = recipientVOList;
  }


  public List getRecipientVOList()
  {
    return recipientVOList;
  }


  public void setOrderHistoryVOList(List orderHistoryVOList)
  {
		if(orderHistoryVOList != null)
		{
      Iterator it = orderHistoryVOList.iterator();
      while(it.hasNext())
      {
        OrderHistoryVO orderHistoryVO = (OrderHistoryVO) it.next();
        orderHistoryVO.setChanged(true);
      }
		}
    this.orderHistoryVOList = orderHistoryVOList;
  }


  public List getOrderHistoryVOList()
  {
    return orderHistoryVOList;
  }


  public void setCommentsVOList(List commentsVOList)
  {
		if(commentsVOList != null)
		{
      Iterator it = commentsVOList.iterator();
      while(it.hasNext())
      {
        CommentsVO commentsVO = (CommentsVO) it.next();
        commentsVO.setChanged(true);
      }
		}
    this.commentsVOList = commentsVOList;
  }


  public List getCommentsVOList()
  {
    return commentsVOList;
  }


  public void setOrderBillVOList(List orderBillVOList)
  {
		if(orderBillVOList != null)
		{
      Iterator it = orderBillVOList.iterator();
      while(it.hasNext())
      {
        OrderBillVO orderBillVO = (OrderBillVO) it.next();
        orderBillVO.setChanged(true);
      }
		}
    this.orderBillVOList = orderBillVOList;
  }


  public List getOrderBillVOList()
  {
    return orderBillVOList;
  }


  public void setOrderTrackingVOList(List orderTrackingVOList)
  {
		if(orderTrackingVOList != null)
		{
      Iterator it = orderTrackingVOList.iterator();
      while(it.hasNext())
      {
        OrderTrackingVO orderTrackingVO = (OrderTrackingVO) it.next();
        orderTrackingVO.setChanged(true);
      }
		}
    this.orderTrackingVOList = orderTrackingVOList;
  }


  public List getOrderTrackingVOList()
  {
    return orderTrackingVOList;
  }

  /**
    * This method uses the Reflection API to generate an XML string that will be
    * passed back to the calling module.
    * The XML string will contain all the fields within this VO, including the
    * variables as well as (a collection of) ValueObjects.
    *
    * @param  None
    * @return XML string
   **/
  public String toXML()
  {
    StringBuffer sb = new StringBuffer();
    try
    {
      sb.append("<OrderDetailVO>");
      Field[] fields = this.getClass().getDeclaredFields();

      for (int i = 0; i < fields.length; i++)
      {
        //if the field retrieved was a list of VO
        if(fields[i].getType().equals(Class.forName("java.util.List")))
        {
          List list = (List)fields[i].get(this);
          if(list != null)
          {
            for (int j = 0; j < list.size(); j++)
            {
              XMLInterface xmlInt = (XMLInterface)list.get(j);
              String sXmlVO = xmlInt.toXML();
              sb.append(sXmlVO);
            }
          }
        }
        else
        {
          //if the field retrieved was a VO
          if (fields[i].getType().toString().matches("(?i).*vo"))
          {
            XMLInterface xmlInt = (XMLInterface)fields[i].get(this);
            String sXmlVO = xmlInt.toXML();
            sb.append(sXmlVO);
          }
          //if the field retrieved was a Calendar object
          else if (fields[i].getType().toString().matches("(?i).*calendar"))
          {
            Date date;
            String fDate = null;
            if (fields[i].get(this) != null)
            {
              date = (((GregorianCalendar)fields[i].get(this)).getTime());
              SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss a zzz");
              fDate = sdf.format(date).toString();
            }
            sb.append("<" + fields[i].getName() + ">");
            sb.append(fDate);
            sb.append("</" + fields[i].getName() + ">");

          }
          else
          {
            sb.append("<" + fields[i].getName() + ">");
            sb.append(fields[i].get(this));
            sb.append("</" + fields[i].getName() + ">");
          }
        }
      }
      sb.append("</OrderDetailVO>");
    }

    catch (Exception e)
    {
      e.printStackTrace();
    }

    return sb.toString();
  }



  /**
    * This method uses the Reflection API to generate an XML string that will be
    * passed back to the calling module.
    * The XML string will contain all the fields within this VO, including the
    * variables as well as (a collection of) ValueObjects.
    *
    * @param  None
    * @return XML string
   **/
  public String toXML(int count)
  {
    StringBuffer sb = new StringBuffer();
    try
    {
      if (count == 0)
      {
        sb.append("<OrderDetailVO>");
      }
      else
      {
        sb.append("<OrderDetailVO num=" + '"' + count + '"' + ">");
      }
      Field[] fields = this.getClass().getDeclaredFields();

      for (int i = 0; i < fields.length; i++)
      {
        //if the field retrieved was a list of VO
        if(fields[i].getType().equals(Class.forName("java.util.List")))
        {
          List list = (List)fields[i].get(this);
          if(list != null)
          {
            for (int j = 0; j < list.size(); j++)
            {
              XMLInterface xmlInt = (XMLInterface)list.get(j);
              int k = j + 1;
              String sXmlVO = xmlInt.toXML(k);
              sb.append(sXmlVO);
            }
          }
        }
        else
        {
          //if the field retrieved was a VO
          if (fields[i].getType().toString().matches("(?i).*vo"))
          {
            XMLInterface xmlInt = (XMLInterface)fields[i].get(this);
            int k = i + 1;
            String sXmlVO = xmlInt.toXML(k);
            sb.append(sXmlVO);
          }
          //if the field retrieved was a Calendar object
          else if (fields[i].getType().toString().matches("(?i).*calendar"))
          {
            Date date;
            String fDate = null;
            if (fields[i].get(this) != null)
            {
              date = (((GregorianCalendar)fields[i].get(this)).getTime());
              SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss a zzz");
              fDate = sdf.format(date).toString();
  	          sb.append("<" + fields[i].getName() + ">");
              sb.append(fDate);
              sb.append("</" + fields[i].getName() + ">");
            }
            else
            {
							sb.append("<" + fields[i].getName() + "/>");
            }

          }
          else
          {
						if (fields[i].get(this) == null)
						{
							sb.append("<" + fields[i].getName() + "/>");
						}
						else
						{
							sb.append("<" + fields[i].getName() + ">");
							sb.append(fields[i].get(this));
							sb.append("</" + fields[i].getName() + ">");
						}
          }
        }
      }
      sb.append("</OrderDetailVO>");
    }

    catch (Exception e)
    {
      e.printStackTrace();
    }

    return sb.toString();
  }

	public String getRecipientCountry() {
		return recipientCountry;
	}

	public void setRecipientCountry(String recipientCountry) {
		this.recipientCountry = recipientCountry;
	}


  private String trim(String str)
  {
    return (str != null)?str.trim():str;
  }

	public String getAVSAddressId() {
		return avsAddressId;
	}

	public void setAVSAddressId(String avsAddressId) {
		this.avsAddressId = avsAddressId;
	}

}
