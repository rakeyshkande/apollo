package com.ftd.customerordermanagement.vo;
import java.lang.reflect.Field;
import java.math.BigDecimal;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;


public class BillingVO extends BaseVO implements XMLInterface
{
  private long        orderDetailId;
  private double      productAmt;
  private double      addOnAmt;
  private double      serviceFee;
  private double      shippingFee;
  private double      discountAmt;
  private double      shippingTax;
  private double      serviceFeeTax;
  private double      productTax;
  private String      addtBillInd;
  private String      createdBy;
  private String      billStatus;
  private double      commissionAmt;
  private double      wholesaleAmt;
  private double      transactionAmt;
  private double      discountProdPrice;
  private String      discountType;
  private double      partnerCost;
  private double      shippingFeeTax;
  private String      acctTransInd;
  private String      tax1Name;
  private String      tax1Description;
  private BigDecimal  tax1Rate;
  private BigDecimal  tax1Amount;
  private String      tax2Name;
  private String      tax2Description;
  private BigDecimal  tax2Rate;
  private BigDecimal  tax2Amount;
  private String      tax3Name;
  private String      tax3Description;
  private BigDecimal  tax3Rate;
  private BigDecimal  tax3Amount;
  private String      tax4Name;
  private String      tax4Description;
  private BigDecimal  tax4Rate;
  private BigDecimal  tax4Amount;
  private String      tax5Name;
  private String      tax5Description;
  private BigDecimal  tax5Rate;
  private BigDecimal  tax5Amount;


  public BillingVO()
  {
  }

  public void setOrderDetailId(long orderDetailId)
  {
    if(valueChanged(this.orderDetailId, orderDetailId))
    {
      setChanged(true);
    }
    this.orderDetailId = orderDetailId;
  }


  public long getOrderDetailId()
  {
    return orderDetailId;
  }


  public void setCreatedBy(String createdBy)
  {
    if(valueChanged(this.createdBy, createdBy))
    {
      setChanged(true);
    }

    this.createdBy = trim(createdBy);
  }


  public String getCreatedBy()
  {
    return createdBy;
  }

 
    public void setProductAmt(double productAmt)
    {
        this.productAmt = productAmt;
    }


    public double getProductAmt()
    {
        return productAmt;
    }


    public void setAddOnAmt(double addOnAmt)
    {
        this.addOnAmt = addOnAmt;
    }


    public double getAddOnAmt()
    {
        return addOnAmt;
    }


    public void setServiceFee(double serviceFee)
    {
        this.serviceFee = serviceFee;
    }


    public double getServiceFee()
    {
        return serviceFee;
    }


    public void setShippingFee(double shippingFee)
    {
        this.shippingFee = shippingFee;
    }


    public double getShippingFee()
    {
        return shippingFee;
    }


    public void setDiscountAmt(double discountAmt)
    {
        this.discountAmt = discountAmt;
    }


    public double getDiscountAmt()
    {
        return discountAmt;
    }


    public void setShippingTax(double shippingTax)
    {
        this.shippingTax = shippingTax;
    }


    public double getShippingTax()
    {
        return shippingTax;
    }

    public void setProductTax(double productTax)
    {
        this.productTax = productTax;
    }


    public double getProductTax()
    {
        return productTax;
    }


    public void setAddtBillInd(String addtBillInd)
    {
        this.addtBillInd = addtBillInd;
    }


    public String getAddtBillInd()
    {
        return addtBillInd;
    }


    public void setBillStatus(String billStatus)
    {
        this.billStatus = billStatus;
    }


    public String getBillStatus()
    {
        return billStatus;
    }


    public void setCommissionAmt(double commissionAmt)
    {
        this.commissionAmt = commissionAmt;
    }


    public double getCommissionAmt()
    {
        return commissionAmt;
    }


    public void setWholesaleAmt(double wholesaleAmt)
    {
        this.wholesaleAmt = wholesaleAmt;
    }


    public double getWholesaleAmt()
    {
        return wholesaleAmt;
    }


    public void setTransactionAmt(double transactionAmt)
    {
        this.transactionAmt = transactionAmt;
    }


    public double getTransactionAmt()
    {
        return transactionAmt;
    }


    public void setDiscountProdPrice(double discountProdPrice)
    {
        this.discountProdPrice = discountProdPrice;
    }


    public double getDiscountProdPrice()
    {
        return discountProdPrice;
    }


    public void setDiscountType(String discountType)
    {
        this.discountType = discountType;
    }


    public String getDiscountType()
    {
        return discountType;
    }


    public void setPartnerCost(double partnerCost)
    {
        this.partnerCost = partnerCost;
    }


    public double getPartnerCost()
    {
        return partnerCost;
    }
      /**
    * This method uses the Reflection API to generate an XML string that will be
    * passed back to the calling module.
    * The XML string will contain all the fields within this VO, including the
    * variables as well as (a collection of) ValueObjects.
    *
    * @param  None
    * @return XML string
   **/
  public String toXML()
  {
    StringBuffer sb = new StringBuffer();
    try
    {
      sb.append("<BillingVO>");
      Field[] fields = this.getClass().getDeclaredFields();

      for (int i = 0; i < fields.length; i++)
      {
        //if the field retrieved was a list of VO
        if(fields[i].getType().equals(Class.forName("java.util.List")))
        {
          List list = (List)fields[i].get(this);
          if(list != null)
          {
            for (int j = 0; j < list.size(); j++)
            {
              XMLInterface xmlInt = (XMLInterface)list.get(j);
              String sXmlVO = xmlInt.toXML();
              sb.append(sXmlVO);
            }
          }
        }
        else
        {
          //if the field retrieved was a VO
          if (fields[i].getType().toString().matches("(?i).*vo"))
          {
            XMLInterface xmlInt = (XMLInterface)fields[i].get(this);
            String sXmlVO = xmlInt.toXML();
            sb.append(sXmlVO);
          }
          //if the field retrieved was a Calendar object
          else if (fields[i].getType().toString().matches("(?i).*calendar"))
          {
            Date date;
            String fDate = null;
            if (fields[i].get(this) != null)
            {
              date = (((GregorianCalendar)fields[i].get(this)).getTime());
              SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss a zzz");
              fDate = sdf.format(date).toString();
            }
            sb.append("<" + fields[i].getName() + ">");
            sb.append(fDate);
            sb.append("</" + fields[i].getName() + ">");

          }
          else
          {
            sb.append("<" + fields[i].getName() + ">");
            sb.append(fields[i].get(this));
            sb.append("</" + fields[i].getName() + ">");
          }
        }
      }
      sb.append("</BillingVO>");
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
    return sb.toString();
  }


  /**
    * This method uses the Reflection API to generate an XML string that will be
    * passed back to the calling module.
    * The XML string will contain all the fields within this VO, including the
    * variables as well as (a collection of) ValueObjects.
    *
    * @param  None
    * @return XML string
   **/
  public String toXML(int count)
  {
    StringBuffer sb = new StringBuffer();
    try
    {
      if (count == 0)
      {
        sb.append("<BillingVO>");
      }
      else
      {
        sb.append("<BillingVO num=" + '"' + count + '"' + ">");
      }
      Field[] fields = this.getClass().getDeclaredFields();

      for (int i = 0; i < fields.length; i++)
      {
        //if the field retrieved was a list of VO
        if(fields[i].getType().equals(Class.forName("java.util.List")))
        {
          List list = (List)fields[i].get(this);
          if(list != null)
          {
            for (int j = 0; j < list.size(); j++)
            {
              XMLInterface xmlInt = (XMLInterface)list.get(j);
              int k = j + 1;
              String sXmlVO = xmlInt.toXML(k);
              sb.append(sXmlVO);
            }
          }
        }
        else
        {
          //if the field retrieved was a VO
          if (fields[i].getType().toString().matches("(?i).*vo"))
          {
            XMLInterface xmlInt = (XMLInterface)fields[i].get(this);
            int k = i + 1;
            String sXmlVO = xmlInt.toXML(k);
            sb.append(sXmlVO);
          }
          //if the field retrieved was a Calendar object
          else if (fields[i].getType().toString().matches("(?i).*calendar"))
          {
            Date date;
            String fDate = null;
            if (fields[i].get(this) != null)
            {
              date = (((GregorianCalendar)fields[i].get(this)).getTime());
              SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss a zzz");
              fDate = sdf.format(date).toString();
  	          sb.append("<" + fields[i].getName() + ">");
              sb.append(fDate);
              sb.append("</" + fields[i].getName() + ">");
            }
            else
            {
							sb.append("<" + fields[i].getName() + "/>");
            }

          }
          else
          {
						if (fields[i].get(this) == null)
						{
							sb.append("<" + fields[i].getName() + "/>");
						}
						else
						{
							sb.append("<" + fields[i].getName() + ">");
							sb.append(fields[i].get(this));
							sb.append("</" + fields[i].getName() + ">");
						}
          }
        }
      }
      sb.append("</BillingVO>");
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
    return sb.toString();
  }


  private String trim(String str)
  {
    return (str != null)?str.trim():str;
  }


    public void setServiceFeeTax(double serviceFeeTax)
    {
        this.serviceFeeTax = serviceFeeTax;
    }


    public double getServiceFeeTax()
    {
        return serviceFeeTax;
    }


    public void setShippingFeeTax(double shippingFeeTax)
    {
        this.shippingFeeTax = shippingFeeTax;
    }


    public double getShippingFeeTax()
    {
        return shippingFeeTax;
    }


    public void set_serviceFeeTax(double serviceFeeTax)
    {
        this.serviceFeeTax = serviceFeeTax;
    }


    public double get_serviceFeeTax()
    {
        return serviceFeeTax;
    }


    public void set_shippingFeeTax(double shippingFeeTax)
    {
        this.shippingFeeTax = shippingFeeTax;
    }


    public double get_shippingFeeTax()
    {
        return shippingFeeTax;
    }


    public void setAcctTransInd(String acctTransInd)
    {
        this.acctTransInd = acctTransInd;
    }


    public String getAcctTransInd()
    {
        return acctTransInd;
    }


  public String getTax1Name()
  {
    return tax1Name;
  }

  public void setTax1Name(String tax1Name)
  {
    this.tax1Name = tax1Name;
  }

  public String getTax1Description()
  {
    return tax1Description;
  }

  public void setTax1Description(String tax1Description)
  {
    this.tax1Description = tax1Description;
  }

  public BigDecimal getTax1Rate()
  {
    return tax1Rate;
  }

  public void setTax1Rate(BigDecimal tax1Rate)
  {
    this.tax1Rate = tax1Rate;
  }

  public BigDecimal getTax1Amount()
  {
    return tax1Amount;
  }

  public void setTax1Amount(BigDecimal tax1Amount)
  {
    this.tax1Amount = tax1Amount;
  }

  public String getTax2Name()
  {
    return tax2Name;
  }

  public void setTax2Name(String tax2Name)
  {
    this.tax2Name = tax2Name;
  }

  public String getTax2Description()
  {
    return tax2Description;
  }

  public void setTax2Description(String tax2Description)
  {
    this.tax2Description = tax2Description;
  }

  public BigDecimal getTax2Rate()
  {
    return tax2Rate;
  }

  public void setTax2Rate(BigDecimal tax2Rate)
  {
    this.tax2Rate = tax2Rate;
  }

  public BigDecimal getTax2Amount()
  {
    return tax2Amount;
  }

  public void setTax2Amount(BigDecimal tax2Amount)
  {
    this.tax2Amount = tax2Amount;
  }

  public String getTax3Name()
  {
    return tax3Name;
  }

  public void setTax3Name(String tax3Name)
  {
    this.tax3Name = tax3Name;
  }

  public String getTax3Description()
  {
    return tax3Description;
  }

  public void setTax3Description(String tax3Description)
  {
    this.tax3Description = tax3Description;
  }

  public BigDecimal getTax3Rate()
  {
    return tax3Rate;
  }

  public void setTax3Rate(BigDecimal tax3Rate)
  {
    this.tax3Rate = tax3Rate;
  }

  public BigDecimal getTax3Amount()
  {
    return tax3Amount;
  }

  public void setTax3Amount(BigDecimal tax3Amount)
  {
    this.tax3Amount = tax3Amount;
  }

  public String getTax4Name()
  {
    return tax4Name;
  }

  public void setTax4Name(String tax4Name)
  {
    this.tax4Name = tax4Name;
  }

  public String getTax4Description()
  {
    return tax4Description;
  }

  public void setTax4Description(String tax4Description)
  {
    this.tax4Description = tax4Description;
  }

  public BigDecimal getTax4Rate()
  {
    return tax4Rate;
  }

  public void setTax4Rate(BigDecimal tax4Rate)
  {
    this.tax4Rate = tax4Rate;
  }

  public BigDecimal getTax4Amount()
  {
    return tax4Amount;
  }

  public void setTax4Amount(BigDecimal tax4Amount)
  {
    this.tax4Amount = tax4Amount;
  }

  public String getTax5Name()
  {
    return tax5Name;
  }

  public void setTax5Name(String tax5Name)
  {
    this.tax5Name = tax5Name;
  }

  public String getTax5Description()
  {
    return tax5Description;
  }

  public void setTax5Description(String tax5Description)
  {
    this.tax5Description = tax5Description;
  }

  public BigDecimal getTax5Rate()
  {
    return tax5Rate;
  }

  public void setTax5Rate(BigDecimal tax5Rate)
  {
    this.tax5Rate = tax5Rate;
  }

  public BigDecimal getTax5Amount()
  {
    return tax5Amount;
  }
  public void setTax5Amount(BigDecimal tax5Amount)
  {
    this.tax5Amount = tax5Amount;
  }

}
