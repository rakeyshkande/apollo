package com.ftd.customerordermanagement.vo;
import java.lang.reflect.Field;
import java.math.BigDecimal;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;


public class RefundVO extends BaseVO implements XMLInterface
{

  private String    refundId;
  private	long	    paymentId;
  private   long      paymentRefundId; //for the payment "R" record
  private	long	    orderBillId;
  private	String 	  refundDispCode;
  private	double	  percent;
  private	double	  originalAmounts;
  private	double	  refundAmount;
  private	Calendar	createdOn;
  private	String 	  createdBy;
  private	Calendar	updatedOn;
  private	String 	  updatedBy;
  private String    reviewedBy;
  private   double      debitAmount;
    private OrderBillVO orderBill;
    private PaymentVO payment;
    private String accountingDate;
    private String cardId;
    private String cardNumber;
    private String paymentMethodId;
    private String paymentType;
    private String refundUpdateFlag;
    private String responsiblePartyUpdateFlag;
    private String responsibleParty;
    private String refundStatus;
    private Date refundDate;
    private String orderDetailId;
    //authorization holds authorizationTransactionId for new payment gateway/ authCode for old payment
    private String authorization;
    private String authCode;
    private String authorizationTransactionId;
    private String settlmentTransactionId;
    private String requestToken;
    
    private int refundSeqId;
    private String accountTransactionInd;
    private String complaintCommOriginTypeId;
    private String complaintCommNotificationTypeId;
    private String associatedPaymentId;
    
    private String merchantRefID;
    private String route;
    
    


  public RefundVO()
  {
  }


  public void setRefundId(String refundId)
  {
        this.refundId = refundId;
  }


  public String getRefundId()
  {
        return refundId;
  }


  public void setPaymentId(long paymentId)
  {
    if(valueChanged(this.paymentId, paymentId))
    {
      setChanged(true);
    }
    this.paymentId = paymentId;
  }


  public long getPaymentId()
  {
    return paymentId;
  }


  public void setOrderBillId(long orderBillId)
  {
		if(valueChanged(this.orderBillId, orderBillId))
		{
			setChanged(true);
		}
    this.orderBillId = orderBillId;
  }


  public long getOrderBillId()
  {
    return orderBillId;
  }


  public void setRefundDispCode(String refundDispCode)
  {
		if(valueChanged(this.refundDispCode, refundDispCode))
		{
			setChanged(true);
		}
    this.refundDispCode = trim(refundDispCode);
  }


  public String getRefundDispCode()
  {
    return refundDispCode;
  }


  public void setPercent(double percent)
  {
		if(valueChanged(new Double(this.percent), new Double(percent)))
		{
			setChanged(true);
		}
    this.percent = percent;
  }


  public double getPercent()
  {
    return percent;
  }


  public void setOriginalAmounts(double originalAmounts)
  {
		if(valueChanged(new Double(this.originalAmounts), new Double(originalAmounts)))
		{
			setChanged(true);
		}
    this.originalAmounts = originalAmounts;
  }


  public double getOriginalAmounts()
  {
    return originalAmounts;
  }


  public void setRefundAmount(double refundAmount)
  {
        this.refundAmount = refundAmount;
  }


  public double getRefundAmount()
  {
        return refundAmount;
  }
  


  public void setCreatedOn(Calendar createdOn)
  {
		if(valueChanged(this.createdOn, createdOn))
		{
			setChanged(true);
		}
    this.createdOn = createdOn;
  }


  public Calendar getCreatedOn()
  {
    return createdOn;
  }


  public void setCreatedBy(String createdBy)
  {
		if(valueChanged(this.createdBy, createdBy))
		{
			setChanged(true);
		}
    this.createdBy = trim(createdBy);
  }


  public String getCreatedBy()
  {
    return createdBy;
  }


  public void setUpdatedOn(Calendar updatedOn)
  {
		if(valueChanged(this.updatedOn, updatedOn))
		{
			setChanged(true);
		}
    this.updatedOn = updatedOn;
  }


  public Calendar getUpdatedOn()
  {
    return updatedOn;
  }


  public void setUpdatedBy(String updatedBy)
  {
		if(valueChanged(this.updatedBy, updatedBy))
		{
			setChanged(true);
		}
    this.updatedBy = trim(updatedBy);
  }

  public void setReviewedBy(String reviewedBy)
  {
		if(valueChanged(this.reviewedBy, reviewedBy))
		{
			setChanged(true);
		}
    this.reviewedBy = trim(reviewedBy);
  }

  public double getDebitAmount()
  {
      return debitAmount;
  }

  public void setDebitAmount(double debitAmount)
  {
      this.debitAmount = debitAmount;
  }

  public String getUpdatedBy()
  {
    return updatedBy;
  }



	public String getMerchantRefID() {
		return merchantRefID;
	}

	public void setMerchantRefID(String merchantRefID) {
		this.merchantRefID = merchantRefID;
	}

/**
    * This method uses the Reflection API to generate an XML string that will be
    * passed back to the calling module.
    * The XML string will contain all the fields within this VO, including the
    * variables as well as (a collection of) ValueObjects.
    *
    * @param  None
    * @return XML string
   **/
  public String toXML()
  {
    StringBuffer sb = new StringBuffer();
    try
    {
      sb.append("<RefundVO>");
      Field[] fields = this.getClass().getDeclaredFields();

      for (int i = 0; i < fields.length; i++)
      {
        //if the field retrieved was a list of VO
        if(fields[i].getType().equals(Class.forName("java.util.List")))
        {
          List list = (List)fields[i].get(this);
          if(list != null)
          {
            for (int j = 0; j < list.size(); j++)
            {
              XMLInterface xmlInt = (XMLInterface)list.get(j);
              String sXmlVO = xmlInt.toXML();
              sb.append(sXmlVO);
            }
          }
        }
        else if (fields[i].getType().toString().matches("(?i).*PaymentVO"))
        {
            continue;
        }
        else
        {
          //if the field retrieved was a VO
          if (fields[i].getType().toString().matches("(?i).*vo"))
          {
            XMLInterface xmlInt = (XMLInterface)fields[i].get(this);
            String sXmlVO = xmlInt.toXML();
            sb.append(sXmlVO);
          }
          //if the field retrieved was a Calendar object
          else if (fields[i].getType().toString().matches("(?i).*calendar"))
          {
            Date date;
            String fDate = null;
            if (fields[i].get(this) != null)
            {
              date = (((GregorianCalendar)fields[i].get(this)).getTime());
              SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss a zzz");
              fDate = sdf.format(date).toString();
            }
            sb.append("<" + fields[i].getName() + ">");
            sb.append(fDate);
            sb.append("</" + fields[i].getName() + ">");

          }
          else
          {
            sb.append("<" + fields[i].getName() + ">");
            sb.append(fields[i].get(this));
            sb.append("</" + fields[i].getName() + ">");
          }
        }
      }
      sb.append("</RefundVO>");
    }

    catch (Exception e)
    {
      e.printStackTrace();
    }

    return sb.toString();
  }



  /**
    * This method uses the Reflection API to generate an XML string that will be
    * passed back to the calling module.
    * The XML string will contain all the fields within this VO, including the
    * variables as well as (a collection of) ValueObjects.
    *
    * @param  None
    * @return XML string
   **/
  public String toXML(int count)
  {
    StringBuffer sb = new StringBuffer();
    try
    {
      if (count == 0)
      {
        sb.append("<RefundVO>");
      }
      else
      {
        sb.append("<RefundVO num=" + '"' + count + '"' + ">");
      }
      Field[] fields = this.getClass().getDeclaredFields();

      for (int i = 0; i < fields.length; i++)
      {
        //if the field retrieved was a list of VO
        if(fields[i].getType().equals(Class.forName("java.util.List")))
        {
          List list = (List)fields[i].get(this);
          if(list != null)
          {
            for (int j = 0; j < list.size(); j++)
            {
              XMLInterface xmlInt = (XMLInterface)list.get(j);
              int k = j + 1;
              String sXmlVO = xmlInt.toXML(k);
              sb.append(sXmlVO);
            }
          }
        }
        else if (fields[i].getType().toString().matches("(?i).*PaymentVO"))
        {
            continue;
        }
        else
        {
          //if the field retrieved was a VO - we skip paymentVOs since that is a circular reference and NPEs anyhow.
          if (fields[i].getType().toString().matches("(?i).*vo"))
          {
            XMLInterface xmlInt = (XMLInterface)fields[i].get(this);
            int k = i + 1;
            String sXmlVO = xmlInt.toXML(k);
            sb.append(sXmlVO);
          }
          //if the field retrieved was a Calendar object
          else if (fields[i].getType().toString().matches("(?i).*calendar"))
          {
            Date date;
            String fDate = null;
            if (fields[i].get(this) != null)
            {
              date = (((GregorianCalendar)fields[i].get(this)).getTime());
              SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss a zzz");
              fDate = sdf.format(date).toString();
  	          sb.append("<" + fields[i].getName() + ">");
              sb.append(fDate);
              sb.append("</" + fields[i].getName() + ">");
            }
            else
            {
							sb.append("<" + fields[i].getName() + "/>");
            }

          }
          else
          {
						if (fields[i].get(this) == null)
						{
							sb.append("<" + fields[i].getName() + "/>");
						}
						else
						{
							sb.append("<" + fields[i].getName() + ">");
							sb.append(fields[i].get(this));
							sb.append("</" + fields[i].getName() + ">");
						}
          }
        }
      }
      sb.append("</RefundVO>");
    }

    catch (Exception e)
    {
      e.printStackTrace();
    }

    return sb.toString();
  }



  private String trim(String str)
  {
    return (str != null)?str.trim():str;
  }

    public OrderBillVO getOrderBill()
    {
        return orderBill;
    }

    public void setOrderBill(OrderBillVO orderBill)
    {
        this.orderBill = orderBill;
    }

    public String getAccountingDate()
    {
        return accountingDate;
    }

    public void setAccountingDate(String accountingDate)
    {
        this.accountingDate = accountingDate;
    }

    public String getCardId()
    {
        return cardId;
    }

    public void setCardId(String cardId)
    {
        this.cardId = cardId;
    }

    public String getCardNumber()
    {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber)
    {
        this.cardNumber = cardNumber;
    }

    public String getPaymentMethodId()
    {
        return paymentMethodId;
    }

    public void setPaymentMethodId(String paymentMethodId)
    {
        this.paymentMethodId = paymentMethodId;
    }

    public String getPaymentType()
    {
        return paymentType;
    }

    public void setPaymentType(String paymentType)
    {
        this.paymentType = paymentType;
    }

    public String getRefundUpdateFlag()
    {
        return refundUpdateFlag;
    }

    public void setRefundUpdateFlag(String refundUpdateFlag)
    {
        this.refundUpdateFlag = refundUpdateFlag;
    }
    public String getResponsiblePartyUpdateFlag()
    {
        return responsiblePartyUpdateFlag;
    }

    public void setResponsiblePartyUpdateFlag(String responciblePartyUpdateFlag)
    {
        this.responsiblePartyUpdateFlag = responciblePartyUpdateFlag;
    }

    public String getResponsibleParty()
    {
        return responsibleParty;
    }

    public void setResponsibleParty(String responsibleParty)
    {
        this.responsibleParty = responsibleParty;
    }

    public String getRefundStatus()
    {
        return refundStatus;
    }

    public void setRefundStatus(String refundStatus)
    {
        this.refundStatus = refundStatus;
    }

    public String getOrderDetailId()
    {
        return orderDetailId;
    }

    public void setOrderDetailId(String orderDetailId)
    {
        this.orderDetailId = orderDetailId;
    }
    
    public String getAuthorization() {
		return authorization;
	}


	public void setAuthorization(String authorization) {
		this.authorization = authorization;
	}
	
	public String getAuthCode() {
		return authCode;
	}

	public void setAuthCode(String authCode) {
		this.authCode = authCode;
	}

	public String getAuthorizationTransactionId() {
		return authorizationTransactionId;
	}

	public void setAuthorizationTransactionId(String authorizationTransactionId) {
		this.authorizationTransactionId = authorizationTransactionId;
	}

	public int getRefundSeqId()
    {
        return refundSeqId;
    }

    public void setRefundSeqId(int refundSeqId)
    {
        this.refundSeqId = refundSeqId;
    }

    public String getReviewedBy() 
    {
      return reviewedBy;
    }

    public String getAccountTransactionInd()
    {
        return accountTransactionInd;
    }

    public void setAccountTransactionInd(String accountTransactionInd)
    {
        this.accountTransactionInd = accountTransactionInd;
    }

  public String getComplaintCommOriginTypeId()
  {
    return complaintCommOriginTypeId;
  }

  public void setComplaintCommOriginTypeId(String complaintCommOriginTypeId)
  {
    this.complaintCommOriginTypeId = complaintCommOriginTypeId;
  }

  public String getComplaintCommNotificationTypeId()
  {
    return complaintCommNotificationTypeId;
  }

  public void setComplaintCommNotificationTypeId(String complaintCommNotificationTypeId)
  {
    this.complaintCommNotificationTypeId = complaintCommNotificationTypeId;
  }

  public String getAssociatedPaymentId() {
	return associatedPaymentId;
  }

  public void setAssociatedPaymentId(String associatedPaymentId) {
	this.associatedPaymentId = associatedPaymentId;
  }


	public PaymentVO getPayment() {
		return this.payment;
	}


	public void setPayment(PaymentVO payment) {
		this.payment = payment;
	}


	public void setRefundDate(Date refundDate) {
		this.refundDate = refundDate;
	}


	public Date getRefundDate() {
		return refundDate;
	}


	/**
	 * gets a calculated refunded amount based on the order bill
	 * @param calculatedRefundAmount
	 */
	public Double getCalculatedRefundAmount() {
		
		
		double productAmount = getOrderBill().getProductAmount() > 0? getOrderBill().getProductAmount():(getOrderBill().getGrossProductAmount() - getOrderBill().getDiscountAmount());

		double refundAmt = productAmount
							+ getOrderBill().getAddOnAmount()
                            + getOrderBill().getFeeAmount()
                            + getOrderBill().getTax();
		return refundAmt;
	}


    public void setPaymentRefundId(long paymentRefundId)
    {
        this.paymentRefundId = paymentRefundId;
    }


    public long getPaymentRefundId()
    {
        return paymentRefundId;
    }


	public String getRoute() {
		return route;
	}


	public void setRoute(String route) {
		this.route = route;
	}


	public String getSettlmentTransactionId() {
		return settlmentTransactionId;
	}


	public void setSettlmentTransactionId(String settlmentTransactionId) {
		this.settlmentTransactionId = settlmentTransactionId;
	}


	public String getRequestToken() {
		return requestToken;
	}


	public void setRequestToken(String requestToken) {
		this.requestToken = requestToken;
	}
	
	
    
    
}
