package com.ftd.customerordermanagement.vo;
import java.lang.reflect.Field;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;


public class OrderVO extends BaseVO implements XMLInterface
{
  private	String	  orderGuid;
  private	String	  masterOrderNumber;
  private	long 	    customerId;
  private	long	    membershipId;
  private	String	  companyId;
  private	String	  sourceCode;
  private	String	  originId;
  private	Calendar	orderDate;
  private	double	  orderTotal;
  private	double	  productTotal;
  private	double	  addOnTotal;
  private	double	  serviceFeeTotal;
  private	double	  shippingFeeTotal;
  private	double	  discountTotal;
  private	double	  taxTotal;
  private	String	  lossPrevention;
  private	Calendar	createdOn;
  private	String 	  createdBy;
  private	Calendar	updatedOn;
  private	String 	  updatedBy;
  private	String 	  buyerSignedInFlag;

  private List      orderDetailVOList;
  private List      orderHistoryVOList;
  private List      paymentVOList;
  private List      commentsVOList;

  public OrderVO()
  {
    orderDetailVOList   = new ArrayList();
    orderHistoryVOList  = new ArrayList();
    paymentVOList       = new ArrayList();
    commentsVOList      = new ArrayList();
  }

  public void setOrderGuid(String orderGuid)
  {
    if(valueChanged(this.orderGuid, orderGuid))
    {
      setChanged(true);
    }
    this.orderGuid = trim(orderGuid);
  }


  public String getOrderGuid()
  {
    return orderGuid;
  }


  public void setCustomerId(long customerId)
  {
    if(valueChanged(this.customerId, customerId ))
    {
      setChanged(true);
    }
    this.customerId = customerId;
  }


  public long getCustomerId()
  {
    return customerId;
  }


  public void setCompanyId(String companyId)
  {
    if(valueChanged(this.companyId, companyId))
    {
      setChanged(true);
    }
    this.companyId = trim(companyId);
  }


  public String getCompanyId()
  {
    return companyId;
  }


  public void setCreatedOn(Calendar createdOn)
  {
    if(valueChanged(this.createdOn, createdOn))
    {
      setChanged(true);
    }
    this.createdOn = createdOn;
  }


  public Calendar getCreatedOn()
  {
    return createdOn;
  }


  public void setCreatedBy(String createdBy)
  {
    if(valueChanged(this.createdBy, createdBy))
    {
      setChanged(true);
    }

    this.createdBy = trim(createdBy);
  }


  public String getCreatedBy()
  {
    return createdBy;
  }


  public void setUpdatedOn(Calendar updatedOn)
  {
    if(valueChanged(this.updatedOn, updatedOn ))
    {
      setChanged(true);
    }
    this.updatedOn = updatedOn;
  }


  public Calendar getUpdatedOn()
  {
    return updatedOn;
  }


  public void setUpdatedBy(String updatedBy)
  {
    if(valueChanged(this.updatedBy, updatedBy ))
    {
      setChanged(true);
    }
    this.updatedBy = trim(updatedBy);
  }


  public String getUpdatedBy()
  {
    return updatedBy;
  }


  public void setMasterOrderNumber(String masterOrderNumber)
  {
    if(valueChanged(this.masterOrderNumber, masterOrderNumber))
    {
      setChanged(true);
    }
    this.masterOrderNumber = trim(masterOrderNumber);
  }


  public String getMasterOrderNumber()
  {
    return masterOrderNumber;
  }


  public void setMembershipId(long membershipId)
  {
    if(valueChanged(this.membershipId, membershipId))
    {
      setChanged(true);
    }
    this.membershipId = membershipId;
  }


  public long getMembershipId()
  {
    return membershipId;
  }


  public void setSourceCode(String sourceCode)
  {
    if(valueChanged(this.sourceCode, sourceCode))
    {
      setChanged(true);
    }
    this.sourceCode = trim(sourceCode);
  }


  public String getSourceCode()
  {
    return sourceCode;
  }


  public void setOriginId(String originId)
  {
    if(valueChanged(this.originId, originId))
    {
      setChanged(true);
    }
    this.originId = trim(originId);
  }


  public String getOriginId()
  {
    return originId;
  }


  public void setOrderDate(Calendar orderDate)
  {
    if(valueChanged(this.orderDate, orderDate))
    {
      setChanged(true);
    }
    this.orderDate = orderDate;
  }


  public Calendar getOrderDate()
  {
    return orderDate;
  }


  public void setOrderTotal(double orderTotal)
  {
    if(valueChanged(new Double(this.orderTotal), new Double(orderTotal)))
    {
      setChanged(true);
    }
    this.orderTotal = orderTotal;
  }


  public double getOrderTotal()
  {
    return orderTotal;
  }


  public void setProductTotal(double productTotal)
  {
    if(valueChanged(new Double(this.productTotal), new Double(productTotal)))
    {
      setChanged(true);
    }
    this.productTotal = productTotal;
  }


  public double getProductTotal()
  {
    return productTotal;
  }


  public void setAddOnTotal(double addOnTotal)
  {
    if(valueChanged(new Double(this.addOnTotal), new Double(addOnTotal)))
    {
      setChanged(true);
    }
    this.addOnTotal = addOnTotal;
  }


  public double getAddOnTotal()
  {
    return addOnTotal;
  }


  public void setServiceFeeTotal(double serviceFeeTotal)
  {
    if(valueChanged(new Double(this.serviceFeeTotal), new Double(serviceFeeTotal)))
    {
      setChanged(true);
    }
    this.serviceFeeTotal = serviceFeeTotal;
  }


  public double getServiceFeeTotal()
  {
    return serviceFeeTotal;
  }


  public void setShippingFeeTotal(double shippingFeeTotal)
  {
    if(valueChanged(new Double(this.shippingFeeTotal), new Double(shippingFeeTotal)))
    {
      setChanged(true);
    }
    this.shippingFeeTotal = shippingFeeTotal;
  }


  public double getShippingFeeTotal()
  {
    return shippingFeeTotal;
  }


  public void setDiscountTotal(double discountTotal)
  {
    if(valueChanged(new Double(this.discountTotal), new Double(discountTotal)))
    {
      setChanged(true);
    }
    this.discountTotal = discountTotal;
  }


  public double getDiscountTotal()
  {
    return discountTotal;
  }


  public void setTaxTotal(double taxTotal)
  {
    if(valueChanged(new Double(this.taxTotal), new Double(taxTotal)))
    {
      setChanged(true);
    }
    this.taxTotal = taxTotal;
  }


  public double getTaxTotal()
  {
    return taxTotal;
  }


  public void setLossPrevention(String lossPrevention)
  {
    if(valueChanged(this.lossPrevention, lossPrevention))
    {
      setChanged(true);
    }
    this.lossPrevention = trim(lossPrevention);
  }


  public String getLossPrevention()
  {
    return lossPrevention;
  }


  public void setOrderDetailVOList(List orderDetailVOList)
  {
		if(orderDetailVOList != null)
		{
      Iterator it = orderDetailVOList.iterator();
      while(it.hasNext())
      {
        OrderDetailVO orderDetailVO = (OrderDetailVO) it.next();
        orderDetailVO.setChanged(true);
      }
		}
    this.orderDetailVOList = orderDetailVOList;
  }


  public List getOrderDetailVOList()
  {
    return orderDetailVOList;
  }


  public void setOrderHistoryVOList(List orderHistoryVOList)
  {
		if(orderHistoryVOList != null)
		{
      Iterator it = orderHistoryVOList.iterator();
      while(it.hasNext())
      {
        OrderHistoryVO orderHistoryVO = (OrderHistoryVO) it.next();
        orderHistoryVO.setChanged(true);
      }
		}
    this.orderHistoryVOList = orderHistoryVOList;
  }


  public List getOrderHistoryVOList()
  {
    return orderHistoryVOList;
  }


  public void setPaymentVOList(List paymentVOList)
  {
		if(paymentVOList != null)
		{
      Iterator it = paymentVOList.iterator();
      while(it.hasNext())
      {
        PaymentVO paymentVO = (PaymentVO) it.next();
        paymentVO.setChanged(true);
      }
		}
    this.paymentVOList = paymentVOList;
  }


  public List getPaymentVOList()
  {
    return paymentVOList;
  }


  public void setCommentsVOList(List commentsVOList)
  {
		if(commentsVOList != null)
		{
      Iterator it = commentsVOList.iterator();
      while(it.hasNext())
      {
        CommentsVO commentsVO = (CommentsVO) it.next();
        commentsVO.setChanged(true);
      }
		}
    this.commentsVOList = commentsVOList;
  }


  public List getCommentsVOList()
  {
    return commentsVOList;
  }



  /**
    * This method uses the Reflection API to generate an XML string that will be
    * passed back to the calling module.
    * The XML string will contain all the fields within this VO, including the
    * variables as well as (a collection of) ValueObjects.
    *
    * @param  None
    * @return XML string
   **/
  public String toXML()
  {
    StringBuffer sb = new StringBuffer();
    try
    {
      sb.append("<OrderVO>");
      Field[] fields = this.getClass().getDeclaredFields();

      for (int i = 0; i < fields.length; i++)
      {
        //if the field retrieved was a list of VO
        if(fields[i].getType().equals(Class.forName("java.util.List")))
        {
          List list = (List)fields[i].get(this);
          if(list != null)
          {
            for (int j = 0; j < list.size(); j++)
            {
              XMLInterface xmlInt = (XMLInterface)list.get(j);
              String sXmlVO = xmlInt.toXML();
              sb.append(sXmlVO);
            }
          }
        }
        else
        {
          //if the field retrieved was a VO
          if (fields[i].getType().toString().matches("(?i).*vo"))
          {
            XMLInterface xmlInt = (XMLInterface)fields[i].get(this);
            String sXmlVO = xmlInt.toXML();
            sb.append(sXmlVO);
          }
          //if the field retrieved was a Calendar object
          else if (fields[i].getType().toString().matches("(?i).*calendar"))
          {
            Date date;
            String fDate = null;
            if (fields[i].get(this) != null)
            {
              date = (((GregorianCalendar)fields[i].get(this)).getTime());
              SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss a zzz");
              fDate = sdf.format(date).toString();
            }
            sb.append("<" + fields[i].getName() + ">");
            sb.append(fDate);
            sb.append("</" + fields[i].getName() + ">");

          }
          else
          {
            sb.append("<" + fields[i].getName() + ">");
            sb.append(fields[i].get(this));
            sb.append("</" + fields[i].getName() + ">");
          }
        }
      }
      sb.append("</OrderVO>");
    }

    catch (Exception e)
    {
      e.printStackTrace();
    }

    return sb.toString();
  }



  /**
    * This method uses the Reflection API to generate an XML string that will be
    * passed back to the calling module.
    * The XML string will contain all the fields within this VO, including the
    * variables as well as (a collection of) ValueObjects.
    *
    * @param  None
    * @return XML string
   **/
  public String toXML(int count)
  {
    StringBuffer sb = new StringBuffer();
    try
    {
      if (count == 0)
      {
        sb.append("<OrderVO>");
      }
      else
      {
        sb.append("<OrderVO num=" + '"' + count + '"' + ">");
      }
      Field[] fields = this.getClass().getDeclaredFields();

      for (int i = 0; i < fields.length; i++)
      {
        //if the field retrieved was a list of VO
        if(fields[i].getType().equals(Class.forName("java.util.List")))
        {
          List list = (List)fields[i].get(this);
          if(list != null)
          {
            for (int j = 0; j < list.size(); j++)
            {
              XMLInterface xmlInt = (XMLInterface)list.get(j);
              int k = j + 1;
              String sXmlVO = xmlInt.toXML(k);
              sb.append(sXmlVO);
            }
          }
        }
        else
        {
          //if the field retrieved was a VO
          if (fields[i].getType().toString().matches("(?i).*vo"))
          {
            XMLInterface xmlInt = (XMLInterface)fields[i].get(this);
            int k = i + 1;
            String sXmlVO = xmlInt.toXML(k);
            sb.append(sXmlVO);
          }
          //if the field retrieved was a Calendar object
          else if (fields[i].getType().toString().matches("(?i).*calendar"))
          {
            Date date;
            String fDate = null;
            if (fields[i].get(this) != null)
            {
              date = (((GregorianCalendar)fields[i].get(this)).getTime());
              SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss a zzz");
              fDate = sdf.format(date).toString();
  	          sb.append("<" + fields[i].getName() + ">");
              sb.append(fDate);
              sb.append("</" + fields[i].getName() + ">");
            }
            else
            {
							sb.append("<" + fields[i].getName() + "/>");
            }

          }
          else
          {
            if (fields[i].get(this) == null)
            {
              sb.append("<" + fields[i].getName() + "/>");
            }
            else
            {
              sb.append("<" + fields[i].getName() + ">");
              sb.append(fields[i].get(this));
              sb.append("</" + fields[i].getName() + ">");
            }
          }
        }
      }
      sb.append("</OrderVO>");
    }

    catch (Exception e)
    {
      e.printStackTrace();
    }

    return sb.toString();
  }



  private String trim(String str)
  {
    return (str != null)?str.trim():str;
  }


  public void setBuyerSignedInFlag(String buyerSignedInFlag)
  {
    this.buyerSignedInFlag = buyerSignedInFlag;
  }

  public String getBuyerSignedInFlag()
  {
    return buyerSignedInFlag;
  }
}
