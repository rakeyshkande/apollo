package com.ftd.customerordermanagement.vo;

/**
 * This class contain DNIS related information.
 * 
 * @author matt.wilcoxen
 */
public class DnisInfoVO
{
    private String  dnis;
    private String  brandName;
    private String  csNumber;
    private String  oeFlag;
    private String  status;


    /**
     * constructor
     * 
     * @param n/a
     * 
     * @return n/a
     * 
     * @throws none
     */
    public DnisInfoVO()
    {
        dnis = "";
        brandName = "";
        csNumber = "";
        oeFlag = "";
				status = "";
  	}


	/**
	 * Returns the Dnis.
   * 
	 * @return String
	 */
    public String getDnis()
    {
        return dnis;
    }


	/**
	 * Returns the brand name (company name).
     * 
	 * @return String
	 */
    public String getBrandName()
    {
        return brandName;
    }


	/**
	 * Returns the cs number (phone number).
     * 
	 * @return String
	 */
    public String getCSNumber()
    {
        return csNumber;
    }


	/**
	 * Returns the OE flag.
     *     Identifies whether the Dnis related to Order Entry or Customer Service.
     * 
	 * @return String
	 */
    public String getOEFlag()
    {
        return oeFlag;
    }


	/**
	 * Returns the Status.
   * 
	 * @return String
	 */
    public String getStatus()
    {
        return status;
    }


	/**
	 * Sets the Dnis.
     * 
	 * @param String - The dnis to set
	 */
    public void setDnis(String dnis)
    {
        this.dnis = dnis;
    }


	/**
	 * Sets the brand name (company name).
     * 
	 * @param String - The brand name to set
	 */
    public void setBrandName(String brandName)
    {
        this.brandName = brandName;
    }


	/**
	 * Sets the cs number (phone number).
     * 
	 * @param String - The cs number to set
	 */
    public void setCSNumber(String csNumber)
    {
        this.csNumber = csNumber;
    }


	/**
	 * Sets the oeFlag.
   * 
	 * @param String - The oe flag indicator to set
	 */
    public void setOEFlag(String oeFlag)
    {
        this.oeFlag = oeFlag;
    }

	/**
	 * Sets the status.
   * 
	 * @param String - The status
	 */
    public void setStatus(String status)
    {
        this.status = status;
    }

}