package com.ftd.customerordermanagement.vo;
import java.lang.reflect.Field;

import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;


public class CreditCardVO  extends BaseVO implements XMLInterface
{

  private	long	    ccId;
  private	String	  ccType;
  private	String	  ccNumber;
  private	String	  ccExpiration;
  private	String	  name;
  private	String	  addressLine1;
  private	String	  addressLine2;
  private	String 	  city;
  private	String 	  state;
  private	String 	  zipCode;
  private	String 	  country;
  private	Calendar	createdOn;
  private	String 	  createdBy;
  private	Calendar	updatedOn;
  private	String 	  updatedBy;
  private   String    customerId;
  private	String	  giftCardPin;


  public CreditCardVO()
  {
  }

  public void setCcId(long ccId)
  {
    if(valueChanged(this.ccId, ccId))
    {
      setChanged(true);
    }
    this.ccId = ccId;
  }


  public long getCcId()
  {
    return ccId;
  }


  public void setCity(String city)
  {
    if(valueChanged(this.city, city))
    {
      setChanged(true);
    }
    this.city = trim(city);
  }


  public String getCity()
  {
    return city;
  }


  public void setState(String state)
  {
    if(valueChanged(this.state, state))
    {
      setChanged(true);
    }
    this.state = trim(state);
  }


  public String getState()
  {
    return state;
  }


  public void setZipCode(String zipCode)
  {
    if(valueChanged(this.zipCode, zipCode))
    {
      setChanged(true);
    }
    this.zipCode = trim(zipCode);
  }


  public String getZipCode()
  {
    return zipCode;
  }


  public void setCountry(String country)
  {
    if(valueChanged(this.country, country))
    {
      setChanged(true);
    }
    this.country = trim(country);
  }


  public String getCountry()
  {
    return country;
  }

  public void setCreatedOn(Calendar createdOn)
  {
    if(valueChanged(this.createdOn, createdOn))
    {
      setChanged(true);
    }
    this.createdOn = createdOn;
  }


  public Calendar getCreatedOn()
  {
    return createdOn;
  }


  public void setCreatedBy(String createdBy)
  {
    if(valueChanged(this.createdBy, createdBy))
    {
      setChanged(true);
    }

    this.createdBy = trim(createdBy);
  }


  public String getCreatedBy()
  {
    return createdBy;
  }


  public void setUpdatedOn(Calendar updatedOn)
  {
    if(valueChanged(this.updatedOn, updatedOn ))
    {
      setChanged(true);
    }
    this.updatedOn = updatedOn;
  }


  public Calendar getUpdatedOn()
  {
    return updatedOn;
  }


  public void setUpdatedBy(String updatedBy)
  {
    if(valueChanged(this.updatedBy, updatedBy ))
    {
      setChanged(true);
    }
    this.updatedBy = trim(updatedBy);
  }


  public String getUpdatedBy()
  {
    return updatedBy;
  }


  public void setCcType(String ccType)
  {
    if(valueChanged(this.ccType, ccType))
    {
      setChanged(true);
    }
    this.ccType = trim(ccType);
  }


  public String getCcType()
  {
    return ccType;
  }


  public void setCcNumber(String ccNumber)
  {
   	if(valueChanged(this.ccNumber, ccNumber))
    {
      setChanged(true);
    }
    this.ccNumber = trim(ccNumber);
  }


  public String getCcNumber()
  {
    return ccNumber;
  }


  public void setCcExpiration(String ccExpiration)
  {
    if(valueChanged(this.ccExpiration, ccExpiration))
    {
      setChanged(true);
    }
    this.ccExpiration = trim(ccExpiration);
  }


  public String getCcExpiration()
  {
    return ccExpiration;
  }


  public void setName(String name)
  {
   	if(valueChanged(this.name, name))
    {
      setChanged(true);
    }
    this.name = trim(name);
  }


  public String getName()
  {
    return name;
  }


  public void setAddressLine1(String addressLine1)
  {
   	if(valueChanged(this.addressLine1, addressLine1))
    {
      setChanged(true);
    }
    this.addressLine1 = trim(addressLine1);
  }


  public String getAddressLine1()
  {
    return addressLine1;
  }


  public void setAddressLine2(String addressLine2)
  {
    if(valueChanged(this.addressLine2, addressLine2))
    {
      setChanged(true);
    }
    this.addressLine2 = trim(addressLine2);
  }


  public String getAddressLine2()
  {
    return addressLine2;
  }


  /**
    * This method uses the Reflection API to generate an XML string that will be
    * passed back to the calling module.
    * The XML string will contain all the fields within this VO, including the
    * variables as well as (a collection of) ValueObjects.
    *
    * @param  None
    * @return XML string
   **/
  public String toXML()
  {
    StringBuffer sb = new StringBuffer();
    try
    {
      sb.append("<CreditCardVO>");
      Field[] fields = this.getClass().getDeclaredFields();

      for (int i = 0; i < fields.length; i++)
      {
        //if the field retrieved was a list of VO
        if(fields[i].getType().equals(Class.forName("java.util.List")))
        {
          List list = (List)fields[i].get(this);
          if(list != null)
          {
            for (int j = 0; j < list.size(); j++)
            {
              XMLInterface xmlInt = (XMLInterface)list.get(j);
              String sXmlVO = xmlInt.toXML();
              sb.append(sXmlVO);
            }
          }
        }
        else
        {
          //if the field retrieved was a VO
          if (fields[i].getType().toString().matches("(?i).*vo"))
          {
            XMLInterface xmlInt = (XMLInterface)fields[i].get(this);
            String sXmlVO = xmlInt.toXML();
            sb.append(sXmlVO);
          }
          //if the field retrieved was a Calendar object
          else if (fields[i].getType().toString().matches("(?i).*calendar"))
          {
            Date date;
            String fDate = null;
            if (fields[i].get(this) != null)
            {
              date = (((GregorianCalendar)fields[i].get(this)).getTime());
              SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss a zzz");
              fDate = sdf.format(date).toString();
            }
            sb.append("<" + fields[i].getName() + ">");
            sb.append(fDate);
            sb.append("</" + fields[i].getName() + ">");

          }
          else
          {
            sb.append("<" + fields[i].getName() + ">");
            sb.append(fields[i].get(this));
            sb.append("</" + fields[i].getName() + ">");
          }
        }
      }
      sb.append("</CreditCardVO>");
    }

    catch (Exception e)
    {
      e.printStackTrace();
    }

    return sb.toString();
  }



  /**
    * This method uses the Reflection API to generate an XML string that will be
    * passed back to the calling module.
    * The XML string will contain all the fields within this VO, including the
    * variables as well as (a collection of) ValueObjects.
    *
    * @param  None
    * @return XML string
   **/
  public String toXML(int count)
  {
    StringBuffer sb = new StringBuffer();
    try
    {
      if (count == 0)
      {
        sb.append("<CreditCardVO>");
      }
      else
      {
        sb.append("<CreditCardVO num=" + '"' + count + '"' + ">");
      }
      Field[] fields = this.getClass().getDeclaredFields();

      for (int i = 0; i < fields.length; i++)
      {
        //if the field retrieved was a list of VO
        if(fields[i].getType().equals(Class.forName("java.util.List")))
        {
          List list = (List)fields[i].get(this);
          if(list != null)
          {
            for (int j = 0; j < list.size(); j++)
            {
              XMLInterface xmlInt = (XMLInterface)list.get(j);
              int k = j + 1;
              String sXmlVO = xmlInt.toXML(k);
              sb.append(sXmlVO);
            }
          }
        }
        else
        {
          //if the field retrieved was a VO
          if (fields[i].getType().toString().matches("(?i).*vo"))
          {
            XMLInterface xmlInt = (XMLInterface)fields[i].get(this);
            int k = i + 1;
            String sXmlVO = xmlInt.toXML(k);
            sb.append(sXmlVO);
          }
          //if the field retrieved was a Calendar object
          else if (fields[i].getType().toString().matches("(?i).*calendar"))
          {
            Date date;
            String fDate = null;
            if (fields[i].get(this) != null)
            {
              date = (((GregorianCalendar)fields[i].get(this)).getTime());
              SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss a zzz");
              fDate = sdf.format(date).toString();
  	          sb.append("<" + fields[i].getName() + ">");
              sb.append(fDate);
              sb.append("</" + fields[i].getName() + ">");
            }
            else
            {
							sb.append("<" + fields[i].getName() + "/>");
            }

          }
          else
          {
						if (fields[i].get(this) == null)
						{
							sb.append("<" + fields[i].getName() + "/>");
						}
						else
						{
							sb.append("<" + fields[i].getName() + ">");
							sb.append(fields[i].get(this));
							sb.append("</" + fields[i].getName() + ">");
						}
          }
        }
      }
      sb.append("</CreditCardVO>");
    }

    catch (Exception e)
    {
      e.printStackTrace();
    }

    return sb.toString();
  }



  private String trim(String str)
  {
    return (str != null)?str.trim():str;
  }


    public void setCustomerId(String customerId)
    {
        this.customerId = customerId;
    }


    public String getCustomerId()
    {
        return customerId;
    }

	public void setGiftCardPin(String giftCardPin) {
		this.giftCardPin = giftCardPin;
	}

	public String getGiftCardPin() {
		return giftCardPin;
	}



}