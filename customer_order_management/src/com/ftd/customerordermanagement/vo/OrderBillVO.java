package com.ftd.customerordermanagement.vo;
import java.lang.reflect.Field;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class OrderBillVO extends BaseVO implements XMLInterface
{
    private String      orderBillId;
    private long        orderDetailId;
    private double      productAmount;
    private double      addOnAmount;
    private double      serviceFee;
    private double      sameDayUpcharge;
    private double      shippingFee;
    private double      morningDeliveryFee;
    private double      discountAmount;
    private double      shippingTax;
    private double      tax;
    private String      additionalBillIndicator;
    private Calendar    createdOn;
    private String      createdBy;
    private Calendar    updatedOn;
    private String      updatedBy;
    private double      feeAmount;
    private double      commissionAmount;
    private double      adminFee;
    private String      vendorFlag;
    private double      grossProductAmount;
    private double      wholesaleAmount;
    private double      serviceFeeTax;
    private double      merchandiseTax;
    private double      wholesaleServiceFee;
    private int         milesPointsOriginal;
    private int         milesPointsAmount;
    private double      fuelSurchargeAmount;
    private String      fuelSurchargeDescription;
    private double      saturdayUpcharge;
    private double      alsHawaiiUpcharge;
    private String      applySurchargeCode;
    private String		billStatus;
    private Calendar	billDate;
    private String      tax1Name;
    private String      tax1Description;
    private BigDecimal  tax1Rate;
    private BigDecimal  tax1Amount;
    private String      tax2Name;
    private String      tax2Description;
    private BigDecimal  tax2Rate;
    private BigDecimal  tax2Amount;
    private String      tax3Name;
    private String      tax3Description;
    private BigDecimal  tax3Rate;
    private BigDecimal  tax3Amount;
    private String      tax4Name;
    private String      tax4Description;
    private BigDecimal  tax4Rate;
    private BigDecimal  tax4Amount;
    private String      tax5Name;
    private String      tax5Description;
    private BigDecimal  tax5Rate;
    private BigDecimal  tax5Amount;
    private double addOnDiscountAmount;
    private double grossAddOnAmount;
    // DI-27 : Refund - Show new fees broken out in Mouseovers
    private double      sundayUpcharge;
    private double      mondayUpcharge;
    private double      lateCutoffFee;
    private double 		internationalFee;
    
    
  public OrderBillVO()
  {
	  //just going to initialize a few things
//	  productAmount = 0.0;
//	  addOnAmount = 0.0;
//	  serviceFee = 0.0;
//	  shippingFee = 0.0;
//	  discountAmount = 0.0;
//	  shippingTax = 0.0;
//	  tax = 0.0;
  }


  public void setCreatedOn(Calendar createdOn)
  {
    if(valueChanged(this.createdOn, createdOn))
    {
      setChanged(true);
    }
    this.createdOn = createdOn;
  }


  public Calendar getCreatedOn()
  {
    return createdOn;
  }


  public void setCreatedBy(String createdBy)
  {
    if(valueChanged(this.createdBy, createdBy))
    {
      setChanged(true);
    }

    this.createdBy = trim(createdBy);
  }


  public String getCreatedBy()
  {
    return createdBy;
  }


  public void setUpdatedOn(Calendar updatedOn)
  {
    if(valueChanged(this.updatedOn, updatedOn ))
    {
      setChanged(true);
    }
    this.updatedOn = updatedOn;
  }


  public Calendar getUpdatedOn()
  {
    return updatedOn;
  }


  public void setUpdatedBy(String updatedBy)
  {
    if(valueChanged(this.updatedBy, updatedBy ))
    {
      setChanged(true);
    }
    this.updatedBy = trim(updatedBy);
  }


  public String getUpdatedBy()
  {
    return updatedBy;
  }


  public void setOrderBillId(String orderBillId)
  {
        this.orderBillId = orderBillId;
  }


  public String getOrderBillId()
  {
        return orderBillId;
  }


  public void setOrderDetailId(long orderDetailId)
  {
    if(valueChanged(this.orderDetailId, orderDetailId))
    {
      setChanged(true);
    }
    this.orderDetailId = orderDetailId;
  }


  public long getOrderDetailId()
  {
    return orderDetailId;
  }


  public void setProductAmount(double productAmount)
  {
    if(valueChanged(new Double(this.productAmount), new Double(productAmount)))
    {
      setChanged(true);
    }
    this.productAmount = productAmount;
  }


  public double getProductAmount()
  {
    return productAmount;
  }


  public void setAddOnAmount(double addOnAmount)
  {
    if(valueChanged(new Double(this.addOnAmount), new Double(addOnAmount)))
    {
      setChanged(true);
    }
    this.addOnAmount = addOnAmount;
  }


  public double getAddOnAmount()
  {
    return addOnAmount;
  }


  public void setServiceFee(double serviceFee)
  {
    if(valueChanged(new Double(this.serviceFee), new Double(serviceFee)))
    {
      setChanged(true);
    }
    this.serviceFee = serviceFee;
  }


  public double getServiceFee()
  {
    return serviceFee;
  }


  public void setShippingFee(double shippingFee)
  {
    if(valueChanged(new Double(this.shippingFee), new Double(shippingFee)))
    {
      setChanged(true);
    }
    this.shippingFee = shippingFee;
  }


  public double getShippingFee()
  {
    return shippingFee;
  }


  public void setDiscountAmount(double discountAmount)
  {
    if(valueChanged(new Double(this.discountAmount), new Double(discountAmount)))
    {
      setChanged(true);
    }
    this.discountAmount = discountAmount;
  }


  public double getDiscountAmount()
  {
    return discountAmount;
  }


  public void setShippingTax(double shippingTax)
  {
    if(valueChanged(new Double(this.shippingTax), new Double(shippingTax)))
    {
      setChanged(true);
    }
    this.shippingTax = shippingTax;
  }


  public double getShippingTax()
  {
    return shippingTax;
  }


  public void setTax(double tax)
  {
     if(valueChanged(new Double(this.tax), new Double(tax)))
    {
      setChanged(true);
    }
   this.tax = tax;
  }


  public double getTax()
  {
    return tax;
  }


  public void setAdditionalBillIndicator(String additionalBillIndicator)
  {
        this.additionalBillIndicator = additionalBillIndicator;
  }


  public String getAdditionalBillIndicator()
  {
        return additionalBillIndicator;
  }


  /**
    * This method uses the Reflection API to generate an XML string that will be
    * passed back to the calling module.
    * The XML string will contain all the fields within this VO, including the
    * variables as well as (a collection of) ValueObjects.
    *
    * @param  None
    * @return XML string
   **/
  public String toXML()
  {
    StringBuffer sb = new StringBuffer();
    try
    {
      sb.append("<OrderBillVO>");
      Field[] fields = this.getClass().getDeclaredFields();

      for (int i = 0; i < fields.length; i++)
      {
        //if the field retrieved was a list of VO
        if(fields[i].getType().equals(Class.forName("java.util.List")))
        {
          List list = (List)fields[i].get(this);
          if(list != null)
          {
            for (int j = 0; j < list.size(); j++)
            {
              XMLInterface xmlInt = (XMLInterface)list.get(j);
              String sXmlVO = xmlInt.toXML();
              sb.append(sXmlVO);
            }
          }
        }
        else
        {
          //if the field retrieved was a VO
          if (fields[i].getType().toString().matches("(?i).*vo"))
          {
            XMLInterface xmlInt = (XMLInterface)fields[i].get(this);
            String sXmlVO = xmlInt.toXML();
            sb.append(sXmlVO);
          }
          //if the field retrieved was a Calendar object
          else if (fields[i].getType().toString().matches("(?i).*calendar"))
          {
            Date date;
            String fDate = null;
            if (fields[i].get(this) != null)
            {
              date = (((GregorianCalendar)fields[i].get(this)).getTime());
              SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss a zzz");
              fDate = sdf.format(date).toString();
            }
            sb.append("<" + fields[i].getName() + ">");
            sb.append(fDate);
            sb.append("</" + fields[i].getName() + ">");

          }
          else if (fields[i].getType().toString().matches("(?i).*double"))
          {
            String formattedNumber = null;
            if (fields[i].get(this) != null)
            {
              formattedNumber = new BigDecimal(((Double) fields[i].get(this)).doubleValue()).setScale(2, BigDecimal.ROUND_HALF_UP).toString();
            }
            sb.append("<" + fields[i].getName() + ">");
            sb.append(formattedNumber);
            sb.append("</" + fields[i].getName() + ">");

          }
          else
          {
            sb.append("<" + fields[i].getName() + ">");
            if(fields[i].getName().equals("fuelSurchargeDescription")){
                if(fields[i].get(this) != null)
                    sb.append(fields[i].get(this).toString().replaceAll("&", "&amp;")); 
            }
            else
                sb.append(fields[i].get(this));
            sb.append("</" + fields[i].getName() + ">");
          }
        }
      }
      sb.append("</OrderBillVO>");
    }

    catch (Exception e)
    {
      e.printStackTrace();
    }

    return sb.toString();
  }



  /**
    * This method uses the Reflection API to generate an XML string that will be
    * passed back to the calling module.
    * The XML string will contain all the fields within this VO, including the
    * variables as well as (a collection of) ValueObjects.
    *
    * @param  None
    * @return XML string
   **/
  public String toXML(int count)
  {
    StringBuffer sb = new StringBuffer();
    try
    {
      if (count == 0)
      {
        sb.append("<OrderBillVO>");
      }
      else
      {
        sb.append("<OrderBillVO num=" + '"' + count + '"' + ">");
      }
      Field[] fields = this.getClass().getDeclaredFields();

      for (int i = 0; i < fields.length; i++)
      {
        //if the field retrieved was a list of VO
        if(fields[i].getType().equals(Class.forName("java.util.List")))
        {
          List list = (List)fields[i].get(this);
          if(list != null)
          {
            for (int j = 0; j < list.size(); j++)
            {
              XMLInterface xmlInt = (XMLInterface)list.get(j);
              int k = j + 1; 
              String sXmlVO = xmlInt.toXML(k);
              sb.append(sXmlVO);
            }
          }
        }
        else
        {
          //if the field retrieved was a VO
          if (fields[i].getType().toString().matches("(?i).*vo"))
          {
            XMLInterface xmlInt = (XMLInterface)fields[i].get(this);
            int k = i + 1; 
            String sXmlVO = xmlInt.toXML(k);
            sb.append(sXmlVO);
          }
          //if the field retrieved was a Calendar object
          else if (fields[i].getType().toString().matches("(?i).*calendar"))
          {
            Date date;
            String fDate = null;
            if (fields[i].get(this) != null)
            {
              date = (((GregorianCalendar)fields[i].get(this)).getTime());
              SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss a zzz");
              fDate = sdf.format(date).toString();
  	          sb.append("<" + fields[i].getName() + ">");
              sb.append(fDate);
              sb.append("</" + fields[i].getName() + ">");
            }
            else
            {
							sb.append("<" + fields[i].getName() + "/>");
            }

          }
          else if (fields[i].getType().toString().matches("(?i).*double"))
          {
            String formattedNumber = null;
            if (fields[i].get(this) != null)
            {
              formattedNumber = new BigDecimal(((Double) fields[i].get(this)).doubleValue()).setScale(2, BigDecimal.ROUND_HALF_UP).toString();
            }
            sb.append("<" + fields[i].getName() + ">");
            sb.append(formattedNumber);
            sb.append("</" + fields[i].getName() + ">");

          }
          else
          {
						if (fields[i].get(this) == null)
						{
							sb.append("<" + fields[i].getName() + "/>");
						}
						else
						{
							sb.append("<" + fields[i].getName() + ">");
							sb.append(fields[i].get(this));
							sb.append("</" + fields[i].getName() + ">");
						}
          }
        }
      }
      sb.append("</OrderBillVO>");
    }

    catch (Exception e)
    {
      e.printStackTrace();
    }

    return sb.toString();
  }


  private String trim(String str)
  {
    return (str != null)?str.trim():str;
  }

    public double getFeeAmount()
    {
        return feeAmount;
    }

    public void setFeeAmount(double feeAmount)
    {
        this.feeAmount = feeAmount;
    }

    public double getCommissionAmount()
    {
        return commissionAmount;
    }

    public void setCommissionAmount(double commissionAmount)
    {
        this.commissionAmount = commissionAmount;
    }

    public double getAdminFee()
    {
        return adminFee;
    }

    public void setAdminFee(double adminFee)
    {
        this.adminFee = adminFee;
    }

    public String getVendorFlag()
    {
        return vendorFlag;
    }

    public void setVendorFlag(String vendorFlag)
    {
        this.vendorFlag = vendorFlag;
    }

    public double getGrossProductAmount()
    {
        return grossProductAmount;
    }

    public void setGrossProductAmount(double grossProductAmount)
    {
        this.grossProductAmount = grossProductAmount;
    }

    public double getWholesaleAmount()
    {
        return wholesaleAmount;
    }

    public void setWholesaleAmount(double wholesaleAmount)
    {
        this.wholesaleAmount = wholesaleAmount;
    }

    public double getServiceFeeTax()
    {
        return serviceFeeTax;
    }

    public void setServiceFeeTax(double serviceFeeTax)
    {
        this.serviceFeeTax = serviceFeeTax;
    }

    public double getMerchandiseTax()
    {
        return merchandiseTax;
    }

    public void setMerchandiseTax(double merchandiseTax)
    {
        this.merchandiseTax = merchandiseTax;
    }

    public double getWholesaleServiceFee()
    {
        return wholesaleServiceFee;
    }

    public void setWholesaleServiceFee(double wholesaleServiceFee)
    {
        this.wholesaleServiceFee = wholesaleServiceFee;
    }

    public void setMilesPointsAmount(int milesPointsAmount) {
        this.milesPointsAmount = milesPointsAmount;
    }

    public int getMilesPointsAmount() {
        return milesPointsAmount;
    }

  public void setMilesPointsOriginal(int milesPointsOriginal)
  {
    this.milesPointsOriginal = milesPointsOriginal;
  }

  public int getMilesPointsOriginal()
  {
    return milesPointsOriginal;
  }

  public void setFuelSurchargeAmount(double fuelSurchargeAmount)
  {
    this.fuelSurchargeAmount = fuelSurchargeAmount;
  }

  public double getFuelSurchargeAmount()
  {
    return fuelSurchargeAmount;
  }

  public void setFuelSurchargeDescription(String fuelSurchargeDescription)
  {
    this.fuelSurchargeDescription = fuelSurchargeDescription;
  }

  public String getFuelSurchargeDescription()
  {
    return fuelSurchargeDescription;
  }
  
  public String getBillStatus() {
		return billStatus;
	}
	
	public void setBillStatus(String billStatus) {
		this.billStatus = billStatus;
	}
	
	public Calendar getBillDate() {
		return billDate;
	}
	
	public void setBillDate(Calendar billDate) {
		this.billDate = billDate;
	}

  public void setSaturdayUpcharge(double saturdayUpcharge)
  {
    this.saturdayUpcharge = saturdayUpcharge;
  }

  public double getSaturdayUpcharge()
  {
    return saturdayUpcharge;
  }

  public void setAlsHawaiiUpcharge(double alsHawaiiUpcharge)
  {
    this.alsHawaiiUpcharge = alsHawaiiUpcharge;
  }

  public double getAlsHawaiiUpcharge()
  {
    return alsHawaiiUpcharge;
  }

  public void setApplySurchargeCode(String applySurchargeCode)
  {
    this.applySurchargeCode = applySurchargeCode;
  }

  public String getApplySurchargeCode()
  {
    return applySurchargeCode;
  }

  public void setTax1Name(String tax1Name)
  {
    this.tax1Name = tax1Name;
  }

  public String getTax1Name()
  {
    return tax1Name;
  }

  public void setTax1Description(String tax1Description)
  {
    this.tax1Description = tax1Description;
  }

  public String getTax1Description()
  {
    return tax1Description;
  }

  public void setTax1Rate(BigDecimal tax1Rate)
  {
    this.tax1Rate = tax1Rate;
  }

  public BigDecimal getTax1Rate()
  {
    return tax1Rate;
  }

  public void setTax1Amount(BigDecimal tax1Amount)
  {
    this.tax1Amount = tax1Amount;
  }

  public BigDecimal getTax1Amount()
  {
    return tax1Amount;
  }

  public void setTax2Name(String tax2Name)
  {
    this.tax2Name = tax2Name;
  }

  public String getTax2Name()
  {
    return tax2Name;
  }

  public void setTax2Description(String tax2Description)
  {
    this.tax2Description = tax2Description;
  }

  public String getTax2Description()
  {
    return tax2Description;
  }

  public void setTax2Rate(BigDecimal tax2Rate)
  {
    this.tax2Rate = tax2Rate;
  }

  public BigDecimal getTax2Rate()
  {
    return tax2Rate;
  }

  public void setTax2Amount(BigDecimal tax2Amount)
  {
    this.tax2Amount = tax2Amount;
  }

  public BigDecimal getTax2Amount()
  {
    return tax2Amount;
  }

  public void setTax3Name(String tax3Name)
  {
    this.tax3Name = tax3Name;
  }

  public String getTax3Name()
  {
    return tax3Name;
  }

  public void setTax3Description(String tax3Description)
  {
    this.tax3Description = tax3Description;
  }

  public String getTax3Description()
  {
    return tax3Description;
  }

  public void setTax3Rate(BigDecimal tax3Rate)
  {
    this.tax3Rate = tax3Rate;
  }

  public BigDecimal getTax3Rate()
  {
    return tax3Rate;
  }

  public void setTax3Amount(BigDecimal tax3Amount)
  {
    this.tax3Amount = tax3Amount;
  }

  public BigDecimal getTax3Amount()
  {
    return tax3Amount;
  }

  public void setTax4Name(String tax4Name)
  {
    this.tax4Name = tax4Name;
  }

  public String getTax4Name()
  {
    return tax4Name;
  }

  public void setTax4Description(String tax4Description)
  {
    this.tax4Description = tax4Description;
  }

  public String getTax4Description()
  {
    return tax4Description;
  }

  public void setTax4Rate(BigDecimal tax4Rate)
  {
    this.tax4Rate = tax4Rate;
  }

  public BigDecimal getTax4Rate()
  {
    return tax4Rate;
  }

  public void setTax4Amount(BigDecimal tax4Amount)
  {
    this.tax4Amount = tax4Amount;
  }

  public BigDecimal getTax4Amount()
  {
    return tax4Amount;
  }

  public void setTax5Name(String tax5Name)
  {
    this.tax5Name = tax5Name;
  }

  public String getTax5Name()
  {
    return tax5Name;
  }

  public void setTax5Description(String tax5Description)
  {
    this.tax5Description = tax5Description;
  }

  public String getTax5Description()
  {
    return tax5Description;
  }

  public void setTax5Rate(BigDecimal tax5Rate)
  {
    this.tax5Rate = tax5Rate;
  }

  public BigDecimal getTax5Rate()
  {
    return tax5Rate;
  }

  public void setTax5Amount(BigDecimal tax5Amount)
  {
    this.tax5Amount = tax5Amount;
  }

  public BigDecimal getTax5Amount()
  {
    return tax5Amount;
  }


	public void setSameDayUpcharge(double sameDayUpcharge) {
		if(valueChanged(new Double(this.sameDayUpcharge), new Double(sameDayUpcharge)))
	    {
	      setChanged(true);
	    }
		this.sameDayUpcharge = sameDayUpcharge;
	}
	
	
	public double getSameDayUpcharge() {
		return sameDayUpcharge;
	}
	
	public void setMorningDeliveryFee(double morningDeliveryFee) {
		if(valueChanged(new Double(this.morningDeliveryFee), new Double(morningDeliveryFee)))
	    {
	      setChanged(true);
	    }
		this.morningDeliveryFee = morningDeliveryFee;
	}
	
	
	public double getMorningDeliveryFee() {
		return morningDeliveryFee;
	}


	public void setGrossAddOnAmount(double grossAddOnAmount) {
		this.grossAddOnAmount = grossAddOnAmount;
		
	}
	
	public double getGrossAddOnAmount() {
		return this.grossAddOnAmount;
		
	}
	
	
	 public void setAddOnDiscountAmount(double addOnDiscountAmount)
	  {
	    if(valueChanged(new Double(this.addOnDiscountAmount), new Double(addOnDiscountAmount)))
	    {
	      setChanged(true);
	    }
	    this.addOnDiscountAmount = addOnDiscountAmount;
	  }


	  public double getAddOnDiscountAmount()
	  {
	    return addOnDiscountAmount;
	  }

	//DI-27: Refund - Show new fees broken out in Mouseovers  
	public double getSundayUpcharge() {
		return sundayUpcharge;
	}


	public void setSundayUpcharge(double sundayUpcharge) {
		this.sundayUpcharge = sundayUpcharge;
	}


	public double getMondayUpcharge() {
		return mondayUpcharge;
	}


	public void setMondayUpcharge(double mondayUpcharge) {
		this.mondayUpcharge = mondayUpcharge;
	}


	public double getLateCutoffFee() {
		return lateCutoffFee;
	}


	public void setLateCutoffFee(double lateCutoffFee) {
		this.lateCutoffFee = lateCutoffFee;
	}


	public double getInternationalFee() {
		return internationalFee;
	}


	public void setInternationalFee(double internationalFee) {
		this.internationalFee = internationalFee;
	}
	
	
}
