package com.ftd.customerordermanagement.vo;

import java.util.ArrayList;
import java.util.Collection;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class CustomerMembershipDetail {

	@XmlElementWrapper(name = "CEA_CUSTOMER_EMAILS")
	@XmlElement(name = "CEA_CUSTOMER_EMAIL")
	Collection<CustomerMemberShipVO> customerMemberShipList;

	public Collection<CustomerMemberShipVO> getCustomerMemberShipList() {
		if (customerMemberShipList == null) {
			customerMemberShipList = new ArrayList<CustomerMemberShipVO>();
		}
		return customerMemberShipList;
	}

	public void setCustomerMemberShipList(
			Collection<CustomerMemberShipVO> customerMemberShipList) {
		this.customerMemberShipList = customerMemberShipList;
	}

}
