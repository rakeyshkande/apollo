package com.ftd.customerordermanagement.vo;

public class CustomerHoldVO 
{
    private String entityId;
    private String entityType;
    private String holdValue1;
    private String holdValue2;
    private String globalAddonOn;
    private String holdReasonCode;
    private String updatedBy;

    public CustomerHoldVO()
    {
    }

    public String getEntityId()
    {
        return entityId;
    }

    public void setEntityId(String entityId)
    {
        this.entityId = entityId;
    }

    public String getEntityType()
    {
        return entityType;
    }

    public void setEntityType(String entityType)
    {
        this.entityType = entityType;
    }

    public String getHoldValue1()
    {
        return holdValue1;
    }

    public void setHoldValue1(String holdValue1)
    {
        this.holdValue1 = holdValue1;
    }

    public String getHoldValue2()
    {
        return holdValue2;
    }

    public void setHoldValue2(String holdValue2)
    {
        this.holdValue2 = holdValue2;
    }

    public String getGlobalAddonOn()
    {
        return globalAddonOn;
    }

    public void setGlobalAddonOn(String globalAddonOn)
    {
        this.globalAddonOn = globalAddonOn;
    }

    public String getHoldReasonCode()
    {
        return holdReasonCode;
    }

    public void setHoldReasonCode(String holdReasonCode)
    {
        this.holdReasonCode = holdReasonCode;
    }

    public String getUpdatedBy()
    {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy)
    {
        this.updatedBy = updatedBy;
    }
}