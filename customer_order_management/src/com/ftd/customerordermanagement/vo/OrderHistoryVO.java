package com.ftd.customerordermanagement.vo;
import java.lang.reflect.Field;

import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class OrderHistoryVO extends BaseVO implements XMLInterface
{
  private	long	    orderHistoryId;
  private	String	  orderGuid;
  private	long	    orderDetailId;
  private	String	  commentOrigin;
  private	String	  csrId;
  private	Calendar	historyStartDate;
  private	Calendar	historyEndDate;
  private	char	    systemEndedIndicator;


  public OrderHistoryVO()
  {
  }

  public void setOrderGuid(String orderGuid)
  {
    if(valueChanged(this.orderGuid, orderGuid))
    {
      setChanged(true);
    }
    this.orderGuid = trim(orderGuid);
  }


  public String getOrderGuid()
  {
    return orderGuid;
  }



  public void setOrderDetailId(long orderDetailId)
  {
    if(valueChanged(this.orderDetailId, orderDetailId))
    {
      setChanged(true);
    }
    this.orderDetailId = orderDetailId;
  }


  public long getOrderDetailId()
  {
    return orderDetailId;
  }


  public void setCommentOrigin(String commentOrigin)
  {
    if(valueChanged(this.commentOrigin, commentOrigin))
    {
      setChanged(true);
    }
    this.commentOrigin = trim(commentOrigin);
  }


  public String getCommentOrigin()
  {
    return commentOrigin;
  }


  public void setOrderHistoryId(long orderHistoryId)
  {
    if(valueChanged(this.orderHistoryId, orderHistoryId))
    {
      setChanged(true);
    }
    this.orderHistoryId = orderHistoryId;
  }


  public long getOrderHistoryId()
  {
    return orderHistoryId;
  }


  public void setCsrId(String csrId)
  {
    if(valueChanged(this.csrId, csrId))
    {
      setChanged(true);
    }
    this.csrId = trim(csrId);
  }


  public String getCsrId()
  {
    return csrId;
  }


  public void setHistoryStartDate(Calendar historyStartDate)
  {
    if(valueChanged(this.historyStartDate, historyStartDate))
    {
      setChanged(true);
    }
    this.historyStartDate = historyStartDate;
  }


  public Calendar getHistoryStartDate()
  {
    return historyStartDate;
  }


  public void setHistoryEndDate(Calendar historyEndDate)
  {
    if(valueChanged(this.historyEndDate, historyEndDate))
    {
      setChanged(true);
    }
    this.historyEndDate = historyEndDate;
  }


  public Calendar getHistoryEndDate()
  {
    return historyEndDate;
  }


  public void setSystemEndedIndicator(char systemEndedIndicator)
  {
    if(valueChanged(this.systemEndedIndicator, systemEndedIndicator))
    {
      setChanged(true);
    }
    this.systemEndedIndicator = systemEndedIndicator;
  }


  public char getSystemEndedIndicator()
  {
    return systemEndedIndicator;
  }


  /**
    * This method uses the Reflection API to generate an XML string that will be
    * passed back to the calling module.
    * The XML string will contain all the fields within this VO, including the
    * variables as well as (a collection of) ValueObjects.
    *
    * @param  None
    * @return XML string
   **/
  public String toXML()
  {
    StringBuffer sb = new StringBuffer();
    try
    {
      sb.append("<OrderHistoryVO>");
      Field[] fields = this.getClass().getDeclaredFields();

      for (int i = 0; i < fields.length; i++)
      {
        //if the field retrieved was a list of VO
        if(fields[i].getType().equals(Class.forName("java.util.List")))
        {
          List list = (List)fields[i].get(this);
          if(list != null)
          {
            for (int j = 0; j < list.size(); j++)
            {
              XMLInterface xmlInt = (XMLInterface)list.get(j);
              String sXmlVO = xmlInt.toXML();
              sb.append(sXmlVO);
            }
          }
        }
        else
        {
          //if the field retrieved was a VO
          if (fields[i].getType().toString().matches("(?i).*vo"))
          {
            XMLInterface xmlInt = (XMLInterface)fields[i].get(this);
            String sXmlVO = xmlInt.toXML();
            sb.append(sXmlVO);
          }
          //if the field retrieved was a Calendar object
          else if (fields[i].getType().toString().matches("(?i).*calendar"))
          {
            Date date;
            String fDate = null;
            if (fields[i].get(this) != null)
            {
              date = (((GregorianCalendar)fields[i].get(this)).getTime());
              SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss a zzz");
              fDate = sdf.format(date).toString();
            }
            sb.append("<" + fields[i].getName() + ">");
            sb.append(fDate);
            sb.append("</" + fields[i].getName() + ">");

          }
          else
          {
            sb.append("<" + fields[i].getName() + ">");
            sb.append(fields[i].get(this));
            sb.append("</" + fields[i].getName() + ">");
          }
        }
      }
      sb.append("</OrderHistoryVO>");
    }

    catch (Exception e)
    {
      e.printStackTrace();
    }

    return sb.toString();
  }


  /**
    * This method uses the Reflection API to generate an XML string that will be
    * passed back to the calling module.
    * The XML string will contain all the fields within this VO, including the
    * variables as well as (a collection of) ValueObjects.
    *
    * @param  None
    * @return XML string
   **/
  public String toXML(int count)
  {
    StringBuffer sb = new StringBuffer();
    try
    {
      if (count == 0)
      {
        sb.append("<OrderHistoryVO>");
      }
      else
      {
        sb.append("<OrderHistoryVO num=" + '"' + count + '"' + ">");
      }
      Field[] fields = this.getClass().getDeclaredFields();

      for (int i = 0; i < fields.length; i++)
      {
        //if the field retrieved was a list of VO
        if(fields[i].getType().equals(Class.forName("java.util.List")))
        {
          List list = (List)fields[i].get(this);
          if(list != null)
          {
            for (int j = 0; j < list.size(); j++)
            {
              XMLInterface xmlInt = (XMLInterface)list.get(j);
              int k = j + 1;
              String sXmlVO = xmlInt.toXML(k);
              sb.append(sXmlVO);
            }
          }
        }
        else
        {
          //if the field retrieved was a VO
          if (fields[i].getType().toString().matches("(?i).*vo"))
          {
            XMLInterface xmlInt = (XMLInterface)fields[i].get(this);
            int k = i + 1;
            String sXmlVO = xmlInt.toXML(k);
            sb.append(sXmlVO);
          }
          //if the field retrieved was a Calendar object
          else if (fields[i].getType().toString().matches("(?i).*calendar"))
          {
            Date date;
            String fDate = null;
            if (fields[i].get(this) != null)
            {
              date = (((GregorianCalendar)fields[i].get(this)).getTime());
              SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss a zzz");
              fDate = sdf.format(date).toString();
  	          sb.append("<" + fields[i].getName() + ">");
              sb.append(fDate);
              sb.append("</" + fields[i].getName() + ">");
            }
            else
            {
							sb.append("<" + fields[i].getName() + "/>");
            }

          }
          else
          {
						if (fields[i].get(this) == null)
						{
							sb.append("<" + fields[i].getName() + "/>");
						}
						else
						{
							sb.append("<" + fields[i].getName() + ">");
							sb.append(fields[i].get(this));
							sb.append("</" + fields[i].getName() + ">");
						}
          }
        }
      }
      sb.append("</OrderHistoryVO>");
    }

    catch (Exception e)
    {
      e.printStackTrace();
    }

    return sb.toString();
  }


  private String trim(String str)
  {
    return (str != null)?str.trim():str;
  }



}