package com.ftd.customerordermanagement.vo;

public interface XMLInterface 
{

  public String toXML();

  public String toXML(int count);
}