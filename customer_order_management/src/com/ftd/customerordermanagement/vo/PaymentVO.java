package com.ftd.customerordermanagement.vo;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;

public class PaymentVO extends BaseVO implements XMLInterface, Comparable
{

  private	long	    paymentId;
  private	String	    orderGuid;
  private	long	    additionalBillId;
  private	String	    paymentType;
  private   String      cardNumber;
  private	String	    giftCertificateId;
  private	double	    amount;
  //authorization holds authorizationTransactionId for new payment gateway/ authCode for old payment
  private	String	    authorization;
  private String authCode;
  private String authorizationTransactionId;
  private String merchantRefId;
  private String settlementTransactionId;
  private String requestToken;
  private String refundTransactionId;
  
  private	Calendar    createdOn;
  private	String 	    createdBy;
  private	Calendar	updatedOn;
  private	String 	    updatedBy;
  private   String      paymentMethodId;
  private   String      paymentMethodDescription;
  private   OrderBillVO orderBill;
  private   double      creditAmount;
  private   double      debitAmount;
  private   String      cardId;
  private   List        refundList;
  private   int         paymentSeqId;
  private   String      aafesTicketNumber;
  private   String      authResult;
  private   String      avsCode;
  private   String      acqRefNumber;
  private   String      gcCouponNumber;
  private   String      authOverrideFlag;
  private   String      paymentInd;
  private   Date        authDate;
  private   String      refundId;
  private   double      gcCouponAmount;
  private   String      gcCouponStatus;
  private   String      gcCouponMultiOrderFlag;
  private   String      transactionId;
  private   String      ncApprovalId;
  private	String		billStatus;
  private	Date		billDate;
  private	String		giftCardPin;
  private	String		gcReinstatedOnOriginalOrder;
  
  private String cscResponseCode;
  private String cscValidatedFlag;
  private int cscFailureCount;

  private String walletIndicator;
  private boolean isVoiceAuth;
  private String ccAuthProvider;
  private Map<String, Object> paymentExtMap;
  
  private String cardinalVerifiedFlag;
  
  private String route;
  
  public PaymentVO()
  {
  }

  public void setOrderGuid(String orderGuid)
  {
    if(valueChanged(this.orderGuid, orderGuid))
    {
      setChanged(true);
    }
    this.orderGuid = trim(orderGuid);
  }


  public String getOrderGuid()
  {
    return orderGuid;
  }

  public void setCreatedOn(Calendar createdOn)
  {
    if(valueChanged(this.createdOn, createdOn))
    {
      setChanged(true);
    }
    this.createdOn = createdOn;
  }


  public Calendar getCreatedOn()
  {
    return createdOn;
  }


  public void setCreatedBy(String createdBy)
  {
    if(valueChanged(this.createdBy, createdBy))
    {
      setChanged(true);
    }

    this.createdBy = trim(createdBy);
  }


  public String getCreatedBy()
  {
    return createdBy;
  }


  public void setUpdatedOn(Calendar updatedOn)
  {
    if(valueChanged(this.updatedOn, updatedOn ))
    {
      setChanged(true);
    }
    this.updatedOn = updatedOn;
  }


  public Calendar getUpdatedOn()
  {
    return updatedOn;
  }


  public void setUpdatedBy(String updatedBy)
  {
    if(valueChanged(this.updatedBy, updatedBy ))
    {
      setChanged(true);
    }
    this.updatedBy = trim(updatedBy);
  }


  public String getUpdatedBy()
  {
    return updatedBy;
  }


  public void setPaymentId(long paymentId)
  {
    if(valueChanged(this.paymentId, paymentId))
    {
      setChanged(true);
    }
    this.paymentId = paymentId;
  }


  public long getPaymentId()
  {
    return paymentId;
  }


  public void setAdditionalBillId(long additionalBillId)
  {
    if(valueChanged(this.additionalBillId, additionalBillId))
    {
      setChanged(true);
    }
    this.additionalBillId = additionalBillId;
  }


  public long getAdditionalBillId()
  {
    return additionalBillId;
  }


  public void setPaymentType(String paymentType)
  {
    if(valueChanged(this.paymentType, paymentType))
    {
      setChanged(true);
    }
    this.paymentType = trim(paymentType);
  }


  public String getPaymentType()
  {
    return paymentType;
  }


  public void setCardNumber(String cardNumber)
  {
        this.cardNumber = cardNumber;
  }


  public String getCardNumber()
  {
        return cardNumber;
  }


  public void setGiftCertificateId(String giftCertificateId)
  {
    if(valueChanged(this.giftCertificateId, giftCertificateId))
    {
      setChanged(true);
    }
    this.giftCertificateId = trim(giftCertificateId);
  }


  public String getGiftCertificateId()
  {
    return giftCertificateId;
  }


  public void setAmount(double amount)
  {
    if(valueChanged(new Double(this.amount), new Double(amount)))
    {
      setChanged(true);
    }
    this.amount = amount;
  }


  public double getAmount()
  {
    return amount;
  }


  public void setAuthorization(String authorization)
  {
    if(valueChanged(this.authorization, authorization))
    {
      setChanged(true);
    }
    this.authorization = trim(authorization);
  }


  public String getAuthorization()
  {
    return authorization;
  }




  /**
    * This method uses the Reflection API to generate an XML string that will be
    * passed back to the calling module.
    * The XML string will contain all the fields within this VO, including the
    * variables as well as (a collection of) ValueObjects.
    *
    * @param  None
    * @return XML string
   **/
  public String toXML()
  {
    StringBuffer sb = new StringBuffer();
    try
    {
      sb.append("<PaymentVO>");
      Field[] fields = this.getClass().getDeclaredFields();

      for (int i = 0; i < fields.length; i++)
      {
        //if the field retrieved was a list of VO
        if(fields[i].getType().equals(Class.forName("java.util.List")))
        {
          List list = (List)fields[i].get(this);
          if(list != null)
          {
            for (int j = 0; j < list.size(); j++)
            {
              XMLInterface xmlInt = (XMLInterface)list.get(j);
              String sXmlVO = xmlInt.toXML();
              sb.append(sXmlVO);
            }
          }
        }
        else if(fields[i].getType().equals(Class.forName("com.ftd.customerordermanagement.vo.OrderBillVO")))
        {
            OrderBillVO orderBill = (OrderBillVO)fields[i].get(this);
            if(orderBill != null)
            {
                sb.append(orderBill.toXML());
            }
        }
        else
        {
          //if the field retrieved was a VO
          if (fields[i].getType().toString().matches("(?i).*vo"))
          {
            XMLInterface xmlInt = (XMLInterface)fields[i].get(this);
            String sXmlVO = xmlInt.toXML();
            sb.append(sXmlVO);
          }
          //if the field retrieved was a Calendar object
          else if (fields[i].getType().toString().matches("(?i).*calendar"))
          {
            Date date;
            String fDate = null;
            if (fields[i].get(this) != null)
            {
              date = (((GregorianCalendar)fields[i].get(this)).getTime());
              SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss a zzz");
              fDate = sdf.format(date).toString();
            }
            sb.append("<" + fields[i].getName() + ">");
            sb.append(fDate);
            sb.append("</" + fields[i].getName() + ">");

          }
          else
          {
            sb.append("<" + fields[i].getName() + ">");
            sb.append(fields[i].get(this));
            sb.append("</" + fields[i].getName() + ">");
          }
        }
      }
      sb.append("</PaymentVO>");
    }

    catch (Exception e)
    {
      e.printStackTrace();
    }

    return sb.toString();
  }


  /**
    * This method uses the Reflection API to generate an XML string that will be
    * passed back to the calling module.
    * The XML string will contain all the fields within this VO, including the
    * variables as well as (a collection of) ValueObjects.
    *
    * @param  None
    * @return XML string
   **/
  public String toXML(int count)
  {
    StringBuffer sb = new StringBuffer();
    try
    {
      if (count == 0)
      {
        sb.append("<PaymentVO>");
      }
      else
      {
        sb.append("<PaymentVO num=" + '"' + count + '"' + ">");
      }
      Field[] fields = this.getClass().getDeclaredFields();

      for (int i = 0; i < fields.length; i++)
      {
        //if the field retrieved was a list of VO
        if(fields[i].getType().equals(Class.forName("java.util.List")))
        {
          List list = (List)fields[i].get(this);
          if(list != null)
          {
            for (int j = 0; j < list.size(); j++)
            {
              XMLInterface xmlInt = (XMLInterface)list.get(j);
              int k = j + 1;
              String sXmlVO = xmlInt.toXML(k);
              sb.append(sXmlVO);
            }
          }
        }
        else if(fields[i].getType().equals(Class.forName("com.ftd.customerordermanagement.vo.OrderBillVO")))
        {
            OrderBillVO orderBill = (OrderBillVO)fields[i].get(this);
            if(orderBill != null)
            {
                sb.append(orderBill.toXML());
            }
        }
        else
        {
          //if the field retrieved was a VO
          if (fields[i].getType().toString().matches("(?i).*vo"))
          {
            XMLInterface xmlInt = (XMLInterface)fields[i].get(this);
            int k = i + 1;
            String sXmlVO = xmlInt.toXML(k);
            sb.append(sXmlVO);
          }
          //if the field retrieved was a Calendar object
          else if (fields[i].getType().toString().matches("(?i).*calendar"))
          {
            Date date;
            String fDate = null;
            if (fields[i].get(this) != null)
            {
              date = (((GregorianCalendar)fields[i].get(this)).getTime());
              SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss a zzz");
              fDate = sdf.format(date).toString();
  	          sb.append("<" + fields[i].getName() + ">");
              sb.append(fDate);
              sb.append("</" + fields[i].getName() + ">");
            }
            else
            {
							sb.append("<" + fields[i].getName() + "/>");
            }

          }
          else
          {
						if (fields[i].get(this) == null)
						{
							sb.append("<" + fields[i].getName() + "/>");
						}
						else
						{
							sb.append("<" + fields[i].getName() + ">");
							sb.append(fields[i].get(this));
							sb.append("</" + fields[i].getName() + ">");
						}
          }
        }
      }
      sb.append("</PaymentVO>");
    }

    catch (Exception e)
    {
      e.printStackTrace();
    }

    return sb.toString();
  }


  private String trim(String str)
  {
    return (str != null)?str.trim():str;
  }

    public String getPaymentMethodId()
    {
        return paymentMethodId;
    }

    public void setPaymentMethodId(String paymentMethodId)
    {
        this.paymentMethodId = paymentMethodId;
    }

    public String getPaymentMethodDescription()
    {
        return paymentMethodDescription;
    }

    public void setPaymentMethodDescription(String paymentMethodDescription)
    {
        this.paymentMethodDescription = paymentMethodDescription;
    }

    public OrderBillVO getOrderBill()
    {
        return orderBill;
    }

    public void setOrderBill(OrderBillVO orderBill)
    {
        this.orderBill = orderBill;
    }

    public double getCreditAmount()
    {
        return creditAmount;
    }

    public void setCreditAmount(double creditAmount)
    {
        this.creditAmount = creditAmount;
    }

    public double getDebitAmount()
    {
        return debitAmount;
    }

    public void setDebitAmount(double debitAmount)
    {
        this.debitAmount = debitAmount;
    }

    public String getCardId()
    {
        return cardId;
    }

    public void setCardId(String cardId)
    {
        this.cardId = cardId;
    }

    public List getRefundList()
    {
        return refundList;
    }

    public void setRefundList(List refundList)
    {
        this.refundList = refundList;
    }

    public int getPaymentSeqId()
    {
        return paymentSeqId;
    }

    public void setPaymentSeqId(int paymentSeqId)
    {
        this.paymentSeqId = paymentSeqId;
    }

  public void setAafesTicketNumber(String aafesTicketNumber)
  {
    this.aafesTicketNumber = trim(aafesTicketNumber);
  }


  public String getAafesTicketNumber()
  {
    return aafesTicketNumber;
  }


    public void setAuthResult(String authResult)
    {
        this.authResult = authResult;
    }


    public String getAuthResult()
    {
        return authResult;
    }


    public void setAvsCode(String avsCode)
    {
        this.avsCode = avsCode;
    }


    public String getAvsCode()
    {
        return avsCode;
    }


    public void setAcqRefNumber(String acqRefNumber)
    {
        this.acqRefNumber = acqRefNumber;
    }


    public String getAcqRefNumber()
    {
        return acqRefNumber;
    }


    public void setGcCouponNumber(String gcCouponNumber)
    {
        this.gcCouponNumber = gcCouponNumber;
    }


    public String getGcCouponNumber()
    {
        return gcCouponNumber;
    }


    public void setAuthOverrideFlag(String authOverrideFlag)
    {
        this.authOverrideFlag = authOverrideFlag;
    }


    public String getAuthOverrideFlag()
    {
        return authOverrideFlag;
    }


    public void setPaymentInd(String paymentInd)
    {
        this.paymentInd = paymentInd;
    }


    public String getPaymentInd()
    {
        return paymentInd;
    }


    public void setAuthDate(Date authDate)
    {
        this.authDate = authDate;
    }


    public Date getAuthDate()
    {
        return authDate;
    }


    public void setRefundId(String refundId)
    {
        this.refundId = refundId;
    }


    public String getRefundId()
    {
        return refundId;
    }

    public double getGcCouponAmount()
    {
        return gcCouponAmount;
    }

    public void setGcCouponAmount(double gcCouponAmount)
    {
        this.gcCouponAmount = gcCouponAmount;
    }

    public String getGcCouponStatus()
    {
        return gcCouponStatus;
    }

    public void setGcCouponStatus(String gcCouponStatus)
    {
        this.gcCouponStatus = gcCouponStatus;
    }

    public String getGcCouponMultiOrderFlag()
    {
        return gcCouponMultiOrderFlag;
    }

    public void setGcCouponMultiOrderFlag(String gcCouponMultiOrderFlag)
    {
        this.gcCouponMultiOrderFlag = gcCouponMultiOrderFlag;
    }

    public int compareTo(Object paymentVO)
        throws ClassCastException
    {
      if (!(paymentVO instanceof PaymentVO))
      throw new ClassCastException("Invalid Object passed.");
      double dAmount = ((PaymentVO) paymentVO).getDebitAmount();  
      return new Double(this.debitAmount).compareTo(new Double(dAmount));    
    }


    public void setTransactionId(String transactionId)
    {
        this.transactionId = transactionId;
    }


    public String getTransactionId()
    {
        return transactionId;
    }

    public void setNcApprovalId(String ncApprovalId) {
        this.ncApprovalId = ncApprovalId;
    }

    public String getNcApprovalId() {
        return ncApprovalId;
    }
    
    public void setCscResponseCode(String newCscResponseCode)
    {
      if(valueChanged(cscResponseCode, newCscResponseCode))
      {
          setChanged(true);
      }
      this.cscResponseCode = newCscResponseCode;
    }
  
    public String getCscResponseCode()
    {
      return cscResponseCode;
    }
  
    public void setCscValidatedFlag(String newCscValidatedFlag)
    {
      if(valueChanged(cscValidatedFlag, newCscValidatedFlag))
      {
          setChanged(true);
      }
      this.cscValidatedFlag = newCscValidatedFlag;
    }
  
    public String getCscValidatedFlag()
    {
      return cscValidatedFlag;
    }
  
    public void setCscFailureCount(int newCscFailureCount)
    {
      if(valueChanged(cscFailureCount, newCscFailureCount))
      {
          setChanged(true);
      }
      this.cscFailureCount = newCscFailureCount;
    }
  
    public int getCscFailureCount()
    {
      return cscFailureCount;
    }

	public String getBillStatus() {
		return billStatus;
	}

	public void setBillStatus(String billStatus) {
		this.billStatus = billStatus;
	}

	public Date getBillDate() {
		return billDate;
	}

	public void setBillDate(Date billDate) {
		this.billDate = billDate;
	}

	public void setGiftCardPin(String giftCardPin) {
		this.giftCardPin = giftCardPin;
	}

	public String getGiftCardPin() {
		return giftCardPin;
	}

	public String getGcReinstatedOnOriginalOrder() {
		return gcReinstatedOnOriginalOrder;
	}

	public void setGcReinstatedOnOriginalOrder(String gcReinstatedOnOriginalOrder) {
		this.gcReinstatedOnOriginalOrder = gcReinstatedOnOriginalOrder;
	}
	
	public String getWalletIndicator() {
		return walletIndicator;
	}

	public void setWalletIndicator(String walletIndicator) {
		this.walletIndicator = walletIndicator;
	}

	public boolean isVoiceAuth() {
		return isVoiceAuth;
	}

	public void setVoiceAuth(boolean isVoiceAuth) {
		this.isVoiceAuth = isVoiceAuth;
	}
	
	public String getAuthCode() {
		return authCode;
	}

	public void setAuthCode(String authCode) {
		this.authCode = authCode;
	}

	public String getAuthorizationTransactionId() {
		return authorizationTransactionId;
	}

	public void setAuthorizationTransactionId(String authorizationTransactionId) {
		this.authorizationTransactionId = authorizationTransactionId;
	}

	public String getMerchantRefId() {
		return merchantRefId;
	}

	public void setMerchantRefId(String merchantRefId) {
		this.merchantRefId = merchantRefId;
	}

	public Map<String, Object> getPaymentExtMap() {	
		if(this.paymentExtMap == null) {
			paymentExtMap = new HashMap<String, Object>();
		}
		return paymentExtMap;
	}

	public String getCcAuthProvider() {
		return ccAuthProvider;
	}

	public void setCcAuthProvider(String ccAuthProvider) {
		this.ccAuthProvider = ccAuthProvider;
	}

	public String getCardinalVerifiedFlag() {
		return cardinalVerifiedFlag;
	}

	public void setCardinalVerifiedFlag(String cardinalVerifiedFlag) {
		this.cardinalVerifiedFlag = cardinalVerifiedFlag;
	}

	public String getRoute() {
		return route;
	}

	public void setRoute(String route) {
		this.route = route;
	}

	public String getSettlementTransactionId() {
		return settlementTransactionId;
	}

	public void setSettlementTransactionId(String settlementTransactionId) {
		this.settlementTransactionId = settlementTransactionId;
	}

	public String getRequestToken() {
		return requestToken;
	}

	public void setRequestToken(String requestToken) {
		this.requestToken = requestToken;
	}

	public String getRefundTransactionId() {
		return refundTransactionId;
	}

	public void setRefundTransactionId(String refundTransactionId) {
		this.refundTransactionId = refundTransactionId;
	}
	
	

}
