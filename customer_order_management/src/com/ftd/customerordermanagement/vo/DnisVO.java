package com.ftd.customerordermanagement.vo;
import java.lang.reflect.Field;

import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class DnisVO extends BaseVO implements XMLInterface
{
  private	long	    dnisId;
  private	String	  description;
  private	String 	  origin;
  private	String	  statusFlag;
  private	String	  oeFlag;
  private	String	  yellowPagesFlag;
  private	String	  scriptCode;
  private	String	  defaultSourceCode;
  private	String	  scriptId;
  private	Calendar	createdOn;
  private	String 	  createdBy;
  private	Calendar	updatedOn;
  private	String 	  updatedBy;


  public DnisVO()
  {
  }



  public void setDnisId(long dnisId)
  {
    if(valueChanged(this.dnisId, dnisId))
    {
      setChanged(true);
    }
    this.dnisId = dnisId;
  }


  public long getDnisId()
  {
    return dnisId;
  }


  public void setDescription(String description)
  {
    if(valueChanged(this.description, description))
    {
      setChanged(true);
    }
    this.description = trim(description);
  }


  public String getDescription()
  {
    return description;
  }


  public void setOrigin(String origin)
  {
    if(valueChanged(this.origin, origin))
    {
      setChanged(true);
    }
    this.origin = trim(origin);
  }


  public String getOrigin()
  {
    return origin;
  }


  public void setCreatedOn(Calendar createdOn)
  {
    if(valueChanged(this.createdOn, createdOn))
    {
      setChanged(true);
    }
    this.createdOn = createdOn;
  }


  public Calendar getCreatedOn()
  {
    return createdOn;
  }


  public void setCreatedBy(String createdBy)
  {
    if(valueChanged(this.createdBy, createdBy))
    {
      setChanged(true);
    }

    this.createdBy = trim(createdBy);
  }


  public String getCreatedBy()
  {
    return createdBy;
  }


  public void setUpdatedOn(Calendar updatedOn)
  {
    if(valueChanged(this.updatedOn, updatedOn ))
    {
      setChanged(true);
    }
    this.updatedOn = updatedOn;
  }


  public Calendar getUpdatedOn()
  {
    return updatedOn;
  }


  public void setUpdatedBy(String updatedBy)
  {
    if(valueChanged(this.updatedBy, updatedBy ))
    {
      setChanged(true);
    }
    this.updatedBy = trim(updatedBy);
  }


  public String getUpdatedBy()
  {
    return updatedBy;
  }


  public void setStatusFlag(String statusFlag)
  {
    if(valueChanged(this.statusFlag, statusFlag))
    {
      setChanged(true);
    }
    this.statusFlag = trim(statusFlag);
  }


  public String getStatusFlag()
  {
    return statusFlag;
  }


  public void setOeFlag(String oeFlag)
  {
    if(valueChanged(this.oeFlag, oeFlag))
    {
      setChanged(true);
    }
    this.oeFlag = trim(oeFlag);
  }


  public String getOeFlag()
  {
    return oeFlag;
  }


  public void setYellowPagesFlag(String yellowPagesFlag)
  {
    if(valueChanged(this.yellowPagesFlag, yellowPagesFlag))
    {
      setChanged(true);
    }
    this.yellowPagesFlag = trim(yellowPagesFlag);
  }


  public String getYellowPagesFlag()
  {
    return yellowPagesFlag;
  }


  public void setScriptCode(String scriptCode)
  {
    if(valueChanged(this.scriptCode, scriptCode))
    {
      setChanged(true);
    }
    this.scriptCode = trim(scriptCode);
  }


  public String getScriptCode()
  {
    return scriptCode;
  }


  public void setDefaultSourceCode(String defaultSourceCode)
  {
    if(valueChanged(this.defaultSourceCode, defaultSourceCode))
    {
      setChanged(true);
    }
    this.defaultSourceCode = trim(defaultSourceCode);
  }


  public String getDefaultSourceCode()
  {
    return defaultSourceCode;
  }


  public void setScriptId(String scriptId)
  {
    if(valueChanged(this.scriptId, scriptId))
    {
      setChanged(true);
    }
    this.scriptId = trim(scriptId);
  }


  public String getScriptId()
  {
    return scriptId;
  }



  /**
    * This method uses the Reflection API to generate an XML string that will be
    * passed back to the calling module.
    * The XML string will contain all the fields within this VO, including the
    * variables as well as (a collection of) ValueObjects.
    *
    * @param  None
    * @return XML string
   **/
  public String toXML()
  {
    StringBuffer sb = new StringBuffer();
    try
    {
      sb.append("<DnisVO>");
      Field[] fields = this.getClass().getDeclaredFields();

      for (int i = 0; i < fields.length; i++)
      {
        //if the field retrieved was a list of VO
        if(fields[i].getType().equals(Class.forName("java.util.List")))
        {
          List list = (List)fields[i].get(this);
          if(list != null)
          {
            for (int j = 0; j < list.size(); j++)
            {
              XMLInterface xmlInt = (XMLInterface)list.get(j);
              String sXmlVO = xmlInt.toXML();
              sb.append(sXmlVO);
            }
          }
        }
        else
        {
          //if the field retrieved was a VO
          if (fields[i].getType().toString().matches("(?i).*vo"))
          {
            XMLInterface xmlInt = (XMLInterface)fields[i].get(this);
            String sXmlVO = xmlInt.toXML();
            sb.append(sXmlVO);
          }
          //if the field retrieved was a Calendar object
          else if (fields[i].getType().toString().matches("(?i).*calendar"))
          {
            Date date;
            String fDate = null;
            if (fields[i].get(this) != null)
            {
              date = (((GregorianCalendar)fields[i].get(this)).getTime());
              SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss a zzz");
              fDate = sdf.format(date).toString();
            }
            sb.append("<" + fields[i].getName() + ">");
            sb.append(fDate);
            sb.append("</" + fields[i].getName() + ">");

          }
          else
          {
            sb.append("<" + fields[i].getName() + ">");
            sb.append(fields[i].get(this));
            sb.append("</" + fields[i].getName() + ">");
          }
        }
      }
      sb.append("</DnisVO>");
    }

    catch (Exception e)
    {
      e.printStackTrace();
    }

    return sb.toString();
  }


  /**
    * This method uses the Reflection API to generate an XML string that will be
    * passed back to the calling module.
    * The XML string will contain all the fields within this VO, including the
    * variables as well as (a collection of) ValueObjects.
    *
    * @param  None
    * @return XML string
   **/
  public String toXML(int count)
  {
    StringBuffer sb = new StringBuffer();
    try
    {
      if (count == 0)
      {
        sb.append("<DnisVO>");
      }
      else
      {
        sb.append("<DnisVO num=" + '"' + count + '"' + ">");
      }
      Field[] fields = this.getClass().getDeclaredFields();

      for (int i = 0; i < fields.length; i++)
      {
        //if the field retrieved was a list of VO
        if(fields[i].getType().equals(Class.forName("java.util.List")))
        {
          List list = (List)fields[i].get(this);
          if(list != null)
          {
            for (int j = 0; j < list.size(); j++)
            {
              XMLInterface xmlInt = (XMLInterface)list.get(j);
              int k = j + 1;
              String sXmlVO = xmlInt.toXML(k);
              sb.append(sXmlVO);
            }
          }
        }
        else
        {
          //if the field retrieved was a VO
          if (fields[i].getType().toString().matches("(?i).*vo"))
          {
            XMLInterface xmlInt = (XMLInterface)fields[i].get(this);
            int k = i + 1;
            String sXmlVO = xmlInt.toXML(k);
            sb.append(sXmlVO);
          }
          //if the field retrieved was a Calendar object
          else if (fields[i].getType().toString().matches("(?i).*calendar"))
          {
            Date date;
            String fDate = null;
            if (fields[i].get(this) != null)
            {
              date = (((GregorianCalendar)fields[i].get(this)).getTime());
              SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss a zzz");
              fDate = sdf.format(date).toString();
  	          sb.append("<" + fields[i].getName() + ">");
              sb.append(fDate);
              sb.append("</" + fields[i].getName() + ">");
            }
            else
            {
							sb.append("<" + fields[i].getName() + "/>");
            }

          }
          else
          {
						if (fields[i].get(this) == null)
						{
							sb.append("<" + fields[i].getName() + "/>");
						}
						else
						{
							sb.append("<" + fields[i].getName() + ">");
							sb.append(fields[i].get(this));
							sb.append("</" + fields[i].getName() + ">");
						}
          }
        }
      }
      sb.append("</DnisVO>");
    }

    catch (Exception e)
    {
      e.printStackTrace();
    }

    return sb.toString();
  }


  private String trim(String str)
  {
    return (str != null)?str.trim():str;
  }


}