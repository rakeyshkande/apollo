package com.ftd.customerordermanagement.vo;
import java.lang.reflect.Field;

import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.ArrayList;

public class PrivacyPolicyOptionVO extends BaseVO implements XMLInterface
{

  private String      privacyPolicyOptionCode;
  private String      privacyPolicyOptTypeDesc;
  private String      privacyPolicyOptDetailDesc;
  private String      bypassPrivPolicyVerifFlag;
  private long        displayOrderSeqNum;
  private String      CallLogId;
  private String      CustomerId;
  private String      OrderDetailId;
  private String      csrId;

  public PrivacyPolicyOptionVO()
  {
  }




  /**
    * This method uses the Reflection API to generate an XML string that will be
    * passed back to the calling module.
    * The XML string will contain all the fields within this VO, including the
    * variables as well as (a collection of) ValueObjects.
    *
    * @param  None
    * @return XML string
   **/
  public String toXML()
  {
    StringBuffer sb = new StringBuffer();
    try
    {
      sb.append("<PrivacyPolicyOptionVO>");
      Field[] fields = this.getClass().getDeclaredFields();

      for (int i = 0; i < fields.length; i++)
      {
        //if the field retrieved was a list of VO
        if(fields[i].getType().equals(Class.forName("java.util.List")))
        {
          List list = (List)fields[i].get(this);
          if(list != null)
          {
            for (int j = 0; j < list.size(); j++)
            {
              XMLInterface xmlInt = (XMLInterface)list.get(j);
              String sXmlVO = xmlInt.toXML();
              sb.append(sXmlVO);
            }
          }
        }
        else
        {
          //if the field retrieved was a VO
          if (fields[i].getType().toString().matches("(?i).*vo"))
          {
            XMLInterface xmlInt = (XMLInterface)fields[i].get(this);
            String sXmlVO = xmlInt.toXML();
            sb.append(sXmlVO);
          }
          //if the field retrieved was a Calendar object
          else if (fields[i].getType().toString().matches("(?i).*calendar"))
          {
            Date date;
            String fDate = null;
            if (fields[i].get(this) != null)
            {
              date = (((GregorianCalendar)fields[i].get(this)).getTime());
              SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss a zzz");
              fDate = sdf.format(date).toString();
            }
            sb.append("<" + fields[i].getName() + ">");
            sb.append(fDate);
            sb.append("</" + fields[i].getName() + ">");

          }
          else
          {
            sb.append("<" + fields[i].getName() + ">");
            sb.append(fields[i].get(this));
            sb.append("</" + fields[i].getName() + ">");
          }
        }
      }
      sb.append("</PrivacyPolicyOptionVO>");
    }

    catch (Exception e)
    {
      e.printStackTrace();
    }

    return sb.toString();
  }



  /**
    * This method uses the Reflection API to generate an XML string that will be
    * passed back to the calling module.
    * The XML string will contain all the fields within this VO, including the
    * variables as well as (a collection of) ValueObjects.
    *
    * @param  None
    * @return XML string
   **/
  public String toXML(int count)
  {
    StringBuffer sb = new StringBuffer();
    try
    {
      if (count == 0)
      {
        sb.append("<PrivacyPolicyOptionVO>");
      }
      else
      {
        sb.append("<PrivacyPolicyOptionVO num=" + '"' + count + '"' + ">");
      }
      Field[] fields = this.getClass().getDeclaredFields();

      for (int i = 0; i < fields.length; i++)
      {
        //if the field retrieved was a list of VO
        if(fields[i].getType().equals(Class.forName("java.util.List")))
        {
          List list = (List)fields[i].get(this);
          if(list != null)
          {
            for (int j = 0; j < list.size(); j++)
            {
              XMLInterface xmlInt = (XMLInterface)list.get(j);
              int k = j + 1;
              String sXmlVO = xmlInt.toXML(k);
              sb.append(sXmlVO);
            }
          }
        }
        else
        {
          //if the field retrieved was a VO
          if (fields[i].getType().toString().matches("(?i).*vo"))
          {
            XMLInterface xmlInt = (XMLInterface)fields[i].get(this);
            int k = i + 1;
            String sXmlVO = xmlInt.toXML(k);
            sb.append(sXmlVO);
          }
          //if the field retrieved was a Calendar object
          else if (fields[i].getType().toString().matches("(?i).*calendar"))
          {
            Date date;
            String fDate = null;
            if (fields[i].get(this) != null)
            {
              date = (((GregorianCalendar)fields[i].get(this)).getTime());
              SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss a zzz");
              fDate = sdf.format(date).toString();
              sb.append("<" + fields[i].getName() + ">");
              sb.append(fDate);
              sb.append("</" + fields[i].getName() + ">");
            }
            else
            {
              sb.append("<" + fields[i].getName() + "/>");
            }

          }
          else
          {
            if (fields[i].get(this) == null)
            {
              sb.append("<" + fields[i].getName() + "/>");
            }
            else
            {
              sb.append("<" + fields[i].getName() + ">");
              sb.append(fields[i].get(this));
              sb.append("</" + fields[i].getName() + ">");
            }
          }
        }
      }
      sb.append("</PrivacyPolicyOptionVO>");
    }

    catch (Exception e)
    {
      e.printStackTrace();
    }

    return sb.toString();
  }



  private String trim(String str)
  {
    return (str != null)?str.trim():str;
  }


  public void setPrivacyPolicyOptionCode(String privacyPolicyOptionCode)
  {
    this.privacyPolicyOptionCode = privacyPolicyOptionCode;
  }

  public String getPrivacyPolicyOptionCode()
  {
    return privacyPolicyOptionCode;
  }

  public void setPrivacyPolicyOptTypeDesc(String privacyPolicyOptTypeDesc)
  {
    this.privacyPolicyOptTypeDesc = privacyPolicyOptTypeDesc;
  }

  public String getPrivacyPolicyOptTypeDesc()
  {
    return privacyPolicyOptTypeDesc;
  }

  public void setPrivacyPolicyOptDetailDesc(String privacyPolicyOptDetailDesc)
  {
    this.privacyPolicyOptDetailDesc = privacyPolicyOptDetailDesc;
  }

  public String getPrivacyPolicyOptDetailDesc()
  {
    return privacyPolicyOptDetailDesc;
  }

  public void setBypassPrivPolicyVerifFlag(String bypassPrivPolicyVerifFlag)
  {
    this.bypassPrivPolicyVerifFlag = bypassPrivPolicyVerifFlag;
  }

  public String getBypassPrivPolicyVerifFlag()
  {
    return bypassPrivPolicyVerifFlag;
  }

  public void setDisplayOrderSeqNum(long displayOrderSeqNum)
  {
    this.displayOrderSeqNum = displayOrderSeqNum;
  }

  public long getDisplayOrderSeqNum()
  {
    return displayOrderSeqNum;
  }

  public void setCallLogId(String callLogId)
  {
    this.CallLogId = callLogId;
  }

  public String getCallLogId()
  {
    return CallLogId;
  }

  public void setCustomerId(String customerId)
  {
    this.CustomerId = customerId;
  }

  public String getCustomerId()
  {
    return CustomerId;
  }

  public void setOrderDetailId(String orderDetailId)
  {
    this.OrderDetailId = orderDetailId;
  }

  public String getOrderDetailId()
  {
    return OrderDetailId;
  }

  public void setCsrId(String csrId)
  {
    this.csrId = csrId;
  }

  public String getCsrId()
  {
    return csrId;
  }
}
