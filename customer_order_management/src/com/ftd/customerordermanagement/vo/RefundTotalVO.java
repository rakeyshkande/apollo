/**
 * 
 */
package com.ftd.customerordermanagement.vo;

/**
 * @author cjohnson
 *
 */
public class RefundTotalVO {
	
    public double getMsdTotal() {
		return msdTotal;
	}
	public void setMsdTotal(double msdTotal) {
		this.msdTotal = msdTotal;
	}
	public double getAddOnTotal() {
		return addOnTotal;
	}
	public void setAddOnTotal(double addOnTotal) {
		this.addOnTotal = addOnTotal;
	}
	public double getFeeTotal() {
		return feeTotal;
	}
	public void setFeeTotal(double feeTotal) {
		this.feeTotal = feeTotal;
	}
	public double getTaxTotal() {
		return taxTotal;
	}
	public void setTaxTotal(double taxTotal) {
		this.taxTotal = taxTotal;
	}
	public int getMilesPointsTotal() {
		return milesPointsTotal;
	}
	public void setMilesPointsTotal(int milesPointsTotal) {
		this.milesPointsTotal = milesPointsTotal;
	}
	private double msdTotal = 0;
    private double addOnTotal = 0;
    private double feeTotal = 0;
    private double taxTotal = 0;
    private int milesPointsTotal = 0;

}
