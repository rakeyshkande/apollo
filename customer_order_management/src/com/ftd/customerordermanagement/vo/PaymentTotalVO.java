package com.ftd.customerordermanagement.vo;
import java.lang.reflect.Field;

import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.ArrayList;

public class PaymentTotalVO extends BaseVO implements XMLInterface
{
  private	String	    paymentType;
  private	String	    paymentId;
  private	double	    productAmt;
  private   double      addOnAmt;
  private	double	    servShipFee;
  private	double	    discountamt;
  private	double	    tax;
  private	double      orderTotal;
  private	double 	    serviceFee;
  private	double  	shippingFee;
  private	double 	    shippingTax;
  private   double      serviceFeeTax;
  private   double      productTax;

  public PaymentTotalVO()
  {
  }

    public void setPaymentType(String paymentType)
    {
      if(valueChanged(this.paymentType, paymentType))
      {
        setChanged(true);
      }
       this.paymentType = paymentType;
    }
    
    
    public String getPaymentType()
    {
        return paymentType;
    }
    
    
    public void setPaymentId(String paymentId)
    {
      if(valueChanged(this.paymentId, paymentId))
      {
        setChanged(true);
      }
        this.paymentId = paymentId;
    }
    
    
    public String getPaymentId()
    {
        return paymentId;
    }
    
    
    public void setProductAmt(double productAmt)
    {
       if(valueChanged(new Double(this.productAmt), new Double(productAmt)))
       {
         setChanged(true);
       }
       this.productAmt = productAmt;
    }
    
    
    public double getProductAmt()
    {
        return productAmt;
    }
    
    
    public void setAddOnAmt(double addOnAmt)
    {
       if(valueChanged(new Double(this.addOnAmt), new Double(addOnAmt)))
       {
         setChanged(true);
       }
       this.addOnAmt = addOnAmt;
    }
    
    
    public double getAddOnAmt()
    {
        return addOnAmt;
    }
    
    
    public void setServShipFee(double servShipFee)
    {
       if(valueChanged(new Double(this.servShipFee), new Double(servShipFee)))
       {
         setChanged(true);
       }
        this.servShipFee = servShipFee;
    }
    
    
    public double getServShipFee()
    {
        return servShipFee;
    }
    
    
    public void setDiscountamt(double discountamt)
    {
       if(valueChanged(new Double(this.discountamt), new Double(discountamt)))
       {
         setChanged(true);
       }
        this.discountamt = discountamt;
    }
    
    
    public double getDiscountamt()
    {
        return discountamt;
    }
    
    
    public void setTax(double tax)
    {
       if(valueChanged(new Double(this.tax), new Double(tax)))
       {
         setChanged(true);
       }
        this.tax = tax;
    }
    
    
    public double getTax()
    {
        return tax;
    }
    
    
    public void setOrderTotal(double orderTotal)
    {
       if(valueChanged(new Double(this.orderTotal), new Double(orderTotal)))
       {
         setChanged(true);
       }
        this.orderTotal = orderTotal;
    }
    
    
    public double getOrderTotal()
    {
        return orderTotal;
    }
    
    
    public void setServiceFee(double serviceFee)
    {
       if(valueChanged(new Double(this.serviceFee), new Double(serviceFee)))
       {
         setChanged(true);
       }
        this.serviceFee = serviceFee;
    }
    
    
    public double getServiceFee()
    {
        return serviceFee;
    }
    
    
    public void setShippingFee(double shippingFee)
    {
       if(valueChanged(new Double(this.shippingFee), new Double(shippingFee)))
       {
         setChanged(true);
       }
        this.shippingFee = shippingFee;
    }
    
    
    public double getShippingFee()
    {
        return shippingFee;
    }
    
    
    public void setShippingTax(double shippingTax)
    {
       if(valueChanged(new Double(this.shippingTax), new Double(shippingTax)))
       {
         setChanged(true);
       }
        this.shippingTax = shippingTax;
    }
    
    
    public double getShippingTax()
    {
        return shippingTax;
    }
    
    
    public void setServiceFeeTax(double serviceFeeTax)
    {
       if(valueChanged(new Double(this.serviceFeeTax), new Double(serviceFeeTax)))
       {
         setChanged(true);
       }
        this.serviceFeeTax = serviceFeeTax;
    }
    
    
    public double getServiceFeeTax()
    {
        return serviceFeeTax;
    }
    
    
    public void setProductTax(double productTax)
    {
       if(valueChanged(new Double(this.productTax), new Double(productTax)))
       {
         setChanged(true);
       }
        this.productTax = productTax;
    }
    
    
    public double getProductTax()
    {
        return productTax;
    }


  /**
    * This method uses the Reflection API to generate an XML string that will be
    * passed back to the calling module.
    * The XML string will contain all the fields within this VO, including the
    * variables as well as (a collection of) ValueObjects.
    *
    * @param  None
    * @return XML string
   **/
  public String toXML()
  {
    StringBuffer sb = new StringBuffer();
    try
    {
      sb.append("<PaymentTotalVO>");
      Field[] fields = this.getClass().getDeclaredFields();

      for (int i = 0; i < fields.length; i++)
      {
        //if the field retrieved was a list of VO
        if(fields[i].getType().equals(Class.forName("java.util.List")))
        {
          List list = (List)fields[i].get(this);
          if(list != null)
          {
            for (int j = 0; j < list.size(); j++)
            {
              XMLInterface xmlInt = (XMLInterface)list.get(j);
              String sXmlVO = xmlInt.toXML();
              sb.append(sXmlVO);
            }
          }
        }
        else
        {
          //if the field retrieved was a VO
          if (fields[i].getType().toString().matches("(?i).*vo"))
          {
            XMLInterface xmlInt = (XMLInterface)fields[i].get(this);
            String sXmlVO = xmlInt.toXML();
            sb.append(sXmlVO);
          }
          //if the field retrieved was a Calendar object
          else if (fields[i].getType().toString().matches("(?i).*calendar"))
          {
            Date date;
            String fDate = null;
            if (fields[i].get(this) != null)
            {
              date = (((GregorianCalendar)fields[i].get(this)).getTime());
              SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss a zzz");
              fDate = sdf.format(date).toString();
            }
            sb.append("<" + fields[i].getName() + ">");
            sb.append(fDate);
            sb.append("</" + fields[i].getName() + ">");

          }
          else
          {
            sb.append("<" + fields[i].getName() + ">");
            sb.append(fields[i].get(this));
            sb.append("</" + fields[i].getName() + ">");
          }
        }
      }
      sb.append("</PaymentTotalVO>");
    }

    catch (Exception e)
    {
      e.printStackTrace();
    }

    return sb.toString();
  }


  /**
    * This method uses the Reflection API to generate an XML string that will be
    * passed back to the calling module.
    * The XML string will contain all the fields within this VO, including the
    * variables as well as (a collection of) ValueObjects.
    *
    * @param  None
    * @return XML string
   **/
  public String toXML(int count)
  {
    StringBuffer sb = new StringBuffer();
    try
    {
      if (count == 0)
      {
        sb.append("<PaymentTotalVO>");
      }
      else
      {
        sb.append("<PaymentTotalVO num=" + '"' + count + '"' + ">");
      }
      Field[] fields = this.getClass().getDeclaredFields();

      for (int i = 0; i < fields.length; i++)
      {
        //if the field retrieved was a list of VO
        if(fields[i].getType().equals(Class.forName("java.util.List")))
        {
          List list = (List)fields[i].get(this);
          if(list != null)
          {
            for (int j = 0; j < list.size(); j++)
            {
              XMLInterface xmlInt = (XMLInterface)list.get(j);
              int k = j + 1;
              String sXmlVO = xmlInt.toXML(k);
              sb.append(sXmlVO);
            }
          }
        }
        else
        {
          //if the field retrieved was a VO
          if (fields[i].getType().toString().matches("(?i).*vo"))
          {
            XMLInterface xmlInt = (XMLInterface)fields[i].get(this);
            int k = i + 1;
            String sXmlVO = xmlInt.toXML(k);
            sb.append(sXmlVO);
          }
          //if the field retrieved was a Calendar object
          else if (fields[i].getType().toString().matches("(?i).*calendar"))
          {
            Date date;
            String fDate = null;
            if (fields[i].get(this) != null)
            {
              date = (((GregorianCalendar)fields[i].get(this)).getTime());
              SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss a zzz");
              fDate = sdf.format(date).toString();
  	          sb.append("<" + fields[i].getName() + ">");
              sb.append(fDate);
              sb.append("</" + fields[i].getName() + ">");
            }
            else
            {
			  sb.append("<" + fields[i].getName() + "/>");
            }
          }
          else
          {
			if (fields[i].get(this) == null)
			{
			  sb.append("<" + fields[i].getName() + "/>");
			}
			else
			{
			  sb.append("<" + fields[i].getName() + ">");
			  sb.append(fields[i].get(this));
			  sb.append("</" + fields[i].getName() + ">");
			}
          }
        }
      }
      sb.append("</PaymentTotalVO>");
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
    return sb.toString();
  }


  private String trim(String str)
  {
    return (str != null)?str.trim():str;
  }
}