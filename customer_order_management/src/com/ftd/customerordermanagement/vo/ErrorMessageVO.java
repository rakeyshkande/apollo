package com.ftd.customerordermanagement.vo;

public class ErrorMessageVO 
{
    private boolean displayYes = false;
    private boolean displayNo = false;
    private boolean displayOk = false;
    private String yesAction;
    private String noAction;
    private String okAction;
    private String message;

    public ErrorMessageVO()
    {
    }

    public boolean isDisplayYes()
    {
        return displayYes;
    }

    public void setDisplayYes(boolean displayYes)
    {
        this.displayYes = displayYes;
    }

    public boolean isDisplayNo()
    {
        return displayNo;
    }

    public void setDisplayNo(boolean displayNo)
    {
        this.displayNo = displayNo;
    }

    public boolean isDisplayOk()
    {
        return displayOk;
    }

    public void setDisplayOk(boolean displayOk)
    {
        this.displayOk = displayOk;
    }

    public String getYesAction()
    {
        return yesAction;
    }

    public void setYesAction(String yesAction)
    {
        this.yesAction = yesAction;
    }

    public String getNoAction()
    {
        return noAction;
    }

    public void setNoAction(String noAction)
    {
        this.noAction = noAction;
    }

    public String getOkAction()
    {
        return okAction;
    }

    public void setOkAction(String okAction)
    {
        this.okAction = okAction;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }
}