package com.ftd.customerordermanagement.test;

import com.ftd.osp.framework.dispatcher.Dispatcher;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.MessageToken;

import java.io.IOException;
import java.io.PrintWriter;

import javax.naming.InitialContext;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * This is a test class for the RefundPartnerOrderMDB.  When invoked it will dispatch a test order detail id
 * to the RefundPartnerOrderMDB
 *
 * @author Rose Lazuk
 */
public class RefundPartnerOrderMDBTest extends HttpServlet
{
  private Logger logger;
  private static final String COM = "EM_COM";

  public void init(ServletConfig config) throws ServletException
  {
    logger = new Logger("com.ftd.customerordermanagement.test.RefundPartnerOrderMDBTest");
    super.init(config);
  }

  public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
  logger.info("here");
    PrintWriter out = response.getWriter();
    out.println("The Refund Partner Order MDB Test!");
    out.close();   
   // logger.info("inside doGet");
    //doPost(request, response);
    
  }

/**
 *
 *  DoPost method receives and process the xml file
 *
 * @param request HttpServletRequest
 * @param response HttpServletResponse
 * @exception ServletException
 * @exception IOException
 */
  public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
  {
    logger.info("begin doPost");
   // logger.info("orderDetailId: " + request.getParameter("order_detail_id"));
      //String orderDetailId = "1555501";
      String orderDetailId = request.getParameter("order_detail_id").toString();
      logger.info("orderDetailId: " + orderDetailId);
      InitialContext context = null;
      MessageToken token = null;
      token = new MessageToken();
      //token.setStatus(this.COM);
      token.setMessage(orderDetailId);
      logger.info("order detail id " + orderDetailId);
     // token.setMessage(request.getParameter("order_detail_id"));

      try{
      //Dispatcher dispatcher = Dispatcher.getInstance();
      // get the initial context
      context = new InitialContext();
      // enlist the JMS transaction, with the other application transaction
      //dispatcher.dispatch(context, token);
       this.sendJMSMessage(new InitialContext(), token, "EM_COM", "EM_COM");
      }
      catch(Exception e)
      {
        logger.info(e.toString());     
      }
    logger.info("end doPost");
  }

 
    /**
       * Sends out a JMS message.
       * 
       * @throws java.lang.Exception
       * @param status
       * @param destinationLocationPropertyName
       * @param messageToken
       * @param context
       */
      public static void sendJMSMessage(InitialContext context,MessageToken messageToken, String destinationLocationPropertyName, String status)
          throws Exception {
         String CONFIG_FILE = "customer_order_management_config.xml";
         
         ConfigurationUtil config = ConfigurationUtil.getInstance();            
         String connectionFactoryLocation = config.getProperty(CONFIG_FILE,"CONNECTION_FACTORY_LOCATION");     
         String destinationLocation = config.getProperty(CONFIG_FILE, destinationLocationPropertyName);     
         
         //make the correlation id the same as the message
         messageToken.setJMSCorrelationID((String)messageToken.getMessage());

         Dispatcher dispatcher = Dispatcher.getInstance();
         messageToken.setStatus(status);
         dispatcher.dispatchTextMessage(context, messageToken);  
      }
  
    
    
}