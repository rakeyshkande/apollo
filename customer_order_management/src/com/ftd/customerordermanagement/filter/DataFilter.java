package com.ftd.customerordermanagement.filter;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.messaging.constants.MessagingConstants;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.security.util.ServletHelper;
import com.ftd.osp.utilities.ConfigurationUtil;

import java.io.IOException;

import java.util.HashMap;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.xml.parsers.ParserConfigurationException;

import javax.xml.transform.TransformerException;



import org.xml.sax.SAXException;

public class DataFilter implements Filter
{
    private FilterConfig _filterConfig = null;
    private Logger logger = new Logger("com.ftd.customerordermanagement.filter.DataFilter");

    private static final String SITE_NAME = "sitename";
    private static final String SITE_NAME_SSL = "sitenamessl";
    private static final String SITE_SSL_PORT = "sitesslport";
    private static final String SITE_IS_CLICK_THROUGH_SECURE = "isclickthroughsecure";
    private static final String APPLICATION_CONTEXT = "applicationcontext";

    private static final String HEADER_CONTENT_TYPE = "CONTENT-TYPE";
    private static final String MULTPART_FORM_DATA = "multipart/form-data";

    private static final String ERROR_PAGE = "error_page";


    public void init(FilterConfig filterConfig)
        throws ServletException
    {
        _filterConfig = filterConfig;
    }


    public void destroy()
    {
        _filterConfig = null;
    }


    /**
     * Get shared data.
     * 
     *   
     * @param ServletRequest  - input request object
     * @param ServletResponse - object used to respond to the user
     * @param FilterChain     - next program to be executed in the chain
     * 
     * @throws IOException
     * @throws ServletException
     */
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
        throws IOException, ServletException
    {
        String adminAction = null;
//               siteName = null;


        try
        {
            HashMap parameters  = new HashMap();
            
            if(request.getAttribute(COMConstants.CONS_APP_PARAMETERS)!=null)
            {
              parameters=(HashMap)request.getAttribute(COMConstants.CONS_APP_PARAMETERS);
            }
            

            if(isMultipartFormData(((HttpServletRequest)request )))
            {
                //populate admin and site info only if multiple forms were found
                parameters.put(COMConstants.ADMIN_ACTION,                   ((request.getParameter(COMConstants.ADMIN_ACTION))                  !=null?request.getParameter(COMConstants.ADMIN_ACTION).toString().trim():""));
//                parameters.put(siteName,                                    ((request.getParameter(SITE_NAME))                                  !=null?request.getParameter(SITE_NAME).toString().trim():""));
                
                //Timer Filter
                populateTimerInfo(request, parameters);
                
                //Header Info
                populateHeaderInfo(request, parameters);

                //Search Criteria
                populateSearchCriteria(request, parameters);
                
                //Start Origin
                populateStartOriginInfo(request, parameters);

                //Order Update Info
                populateUpdateOrderInfo(request, parameters);
                
                //Queue back button info
                populateQueueInfo(request, parameters);
               
                //Prev Page info
                populatePrevPageInfo(request, parameters);
                
                //Bypass COM info
                populateBypassComInfo(request, parameters);
                
                //Bypass lp info
                populateLossPreventionInfo(request, parameters);

                //Privacy Policy info
                populateBypassPrivacyPolicyVerificationInfo(request, parameters);
                
                // Call Disposition info
                populateDispoWidgetInfo(request, parameters);
               
            }
            else
            {
                //populate admin
                parameters.put(COMConstants.ADMIN_ACTION,                   ((request.getParameter(COMConstants.ADMIN_ACTION))                  !=null?request.getParameter(COMConstants.ADMIN_ACTION).toString().trim():""));

                //Timer Filter
                populateTimerInfo(request, parameters);
                
                //Header Info
                populateHeaderInfo(request, parameters);

                //Search Criteria
                populateSearchCriteria(request, parameters);
               
                //Start Origin
                populateStartOriginInfo(request, parameters);

                //Order Update Info
                populateUpdateOrderInfo(request, parameters);
                
                //Queue back button info
                populateQueueInfo(request, parameters);
                
                //Prev Page info
                populatePrevPageInfo(request, parameters);
                
                //Bypass COM info
                populateBypassComInfo(request, parameters);
                
                //Bypass lp info
                populateLossPreventionInfo(request, parameters);

                //Privacy Policy info
                populateBypassPrivacyPolicyVerificationInfo(request, parameters);
                
                // Call Disposition info
                populateDispoWidgetInfo(request, parameters);

            }
            
            ConfigurationUtil cu = ConfigurationUtil.getInstance();
            String siteName = cu.getFrpGlobalParm(COMConstants.COM_CONTEXT, SITE_NAME);
            String siteSslPort = cu.getFrpGlobalParm(COMConstants.COM_CONTEXT, SITE_SSL_PORT);
            
            // Defect 1240: Enabling SSL.
            parameters.put(SITE_NAME, siteName);
            parameters.put(SITE_NAME_SSL, ServletHelper.switchServerPort(
            siteName, siteSslPort));
            
            if (request instanceof HttpServletRequest) {
            HttpServletRequest httpServletRequest = (HttpServletRequest) request;
            parameters.put(APPLICATION_CONTEXT, httpServletRequest.getContextPath());
            
            // This HTTP request header Pragma-Apollo directive is populated by the Big IP iRules
            // for secure pages.
            String pragmaApollo = httpServletRequest.getHeader("Pragma-Apollo");
            // Note that the OR condition accomodates development environments not
            // fronted by a Big IP.
            if ((pragmaApollo != null && pragmaApollo.indexOf("secure-clickthrough") != -1) ||
            httpServletRequest.isSecure()) {
              parameters.put(SITE_IS_CLICK_THROUGH_SECURE, "true");
            }      
            }
            
            request.setAttribute(COMConstants.CONS_APP_PARAMETERS, parameters);
            chain.doFilter(request, response);
        } 
        catch (Exception ex)
        {
            logger.error(ex);
            try
            {
                redirectToError(response);
            }
            catch (Exception e)
            {
                logger.error(e);
            }
        }
    }


    /**
     * Redirect csr to the error page.
     * 
     * @param ServletResponse
     * 
     * @throws IOException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws TransformerException
     * @throws XSLException
     */
    private void redirectToError(ServletResponse response)
        throws Exception
    {
        ((HttpServletResponse) response).sendRedirect(ConfigurationUtil.getInstance().getFrpGlobalParm(COMConstants.COM_CONTEXT,ERROR_PAGE));
    }


    /**
     * Returns true if request content type is multipart/form-data.
     * 
     * @param request HttpServlet request to be analyzed
     * @throws Exception
     */
     public static boolean isMultipartFormData(HttpServletRequest request)
        throws Exception
     {
        boolean isMultipartFormData = false;
        String headerContentType = request.getHeader(HEADER_CONTENT_TYPE);
        if(headerContentType != null  &&  headerContentType.startsWith(MULTPART_FORM_DATA))
        {
            isMultipartFormData = true;
        }

        return isMultipartFormData;
       }


    /**
     * Populate Start Origin data
     * 
     */
    private void populateStartOriginInfo(ServletRequest request, HashMap parameters)
    {
    
      parameters.put(COMConstants.START_ORIGIN,              ((request.getParameter(COMConstants.START_ORIGIN))             !=null?request.getParameter(COMConstants.START_ORIGIN).toString().trim():""));

    }


    /**
     * Populate Timer data
     * 
     */
    private void populateTimerInfo(ServletRequest request, HashMap parameters)
    {
    
      parameters.put(COMConstants.TIMER_CALL_LOG_ID,              ((request.getParameter(COMConstants.TIMER_CALL_LOG_ID))             !=null?request.getParameter(COMConstants.TIMER_CALL_LOG_ID).toString().trim():""));
      parameters.put(COMConstants.TIMER_COMMENT_ORIGIN_TYPE,      ((request.getParameter(COMConstants.TIMER_COMMENT_ORIGIN_TYPE))     !=null?request.getParameter(COMConstants.TIMER_COMMENT_ORIGIN_TYPE).toString().trim():""));
      parameters.put(COMConstants.TIMER_ENTITY_HISTORY_ID,        ((request.getParameter(COMConstants.TIMER_ENTITY_HISTORY_ID))       !=null?request.getParameter(COMConstants.TIMER_ENTITY_HISTORY_ID).toString().trim():""));

    }
    
    /**
     * Populate Timer data
     * 
     */
    private void populateQueueInfo(ServletRequest request, HashMap parameters)
    {
      parameters.put(COMConstants.RTQ_FLAG,										((request.getParameter(COMConstants.RTQ_FLAG)) !=null										?request.getParameter(COMConstants.RTQ_FLAG).toString().trim()									:""));
      parameters.put(COMConstants.RTQ_QUEUE_TYPE,      				((request.getParameter(COMConstants.RTQ_QUEUE_TYPE)) !=null							?request.getParameter(COMConstants.RTQ_QUEUE_TYPE).toString().trim()						:""));
      parameters.put(COMConstants.RTQ_QUEUE_IND,        			((request.getParameter(COMConstants.RTQ_QUEUE_IND)) !=null							?request.getParameter(COMConstants.RTQ_QUEUE_IND).toString().trim()							:""));
      parameters.put(COMConstants.RTQ_ATTACHED,        				((request.getParameter(COMConstants.RTQ_ATTACHED)) !=null								?request.getParameter(COMConstants.RTQ_ATTACHED).toString().trim()							:""));
      parameters.put(COMConstants.RTQ_TAGGED,        					((request.getParameter(COMConstants.RTQ_TAGGED)) !=null									?request.getParameter(COMConstants.RTQ_TAGGED).toString().trim()								:""));
      parameters.put(COMConstants.RTQ_USER,        						((request.getParameter(COMConstants.RTQ_USER)) !=null										?request.getParameter(COMConstants.RTQ_USER).toString().trim()									:""));
      parameters.put(COMConstants.RTQ_GROUP,									((request.getParameter(COMConstants.RTQ_GROUP)) !=null									?request.getParameter(COMConstants.RTQ_GROUP).toString().trim()									:""));
      parameters.put(COMConstants.RTQ_MAX_RECORDS,						((request.getParameter(COMConstants.RTQ_MAX_RECORDS)) !=null						?request.getParameter(COMConstants.RTQ_MAX_RECORDS).toString().trim()						:""));
      parameters.put(COMConstants.RTQ_POSITION,								((request.getParameter(COMConstants.RTQ_POSITION)) !=null								?request.getParameter(COMConstants.RTQ_POSITION).toString().trim()							:""));
      parameters.put(COMConstants.RTQ_CURRENT_SORT_DIRECTION,	((request.getParameter(COMConstants.RTQ_CURRENT_SORT_DIRECTION)) !=null	?request.getParameter(COMConstants.RTQ_CURRENT_SORT_DIRECTION).toString().trim():""));
      parameters.put(COMConstants.RTQ_SORT_ON_RETURN,        	((request.getParameter(COMConstants.RTQ_SORT_ON_RETURN)) !=null					?request.getParameter(COMConstants.RTQ_SORT_ON_RETURN).toString().trim()				:""));
      parameters.put(COMConstants.RTQ_SORT_COLUMN,((request.getParameter(COMConstants.RTQ_SORT_COLUMN)) !=null						?request.getParameter(COMConstants.RTQ_SORT_COLUMN).toString().trim():""));
      // queue page filters //
      parameters.put(COMConstants.RTQ_RECEIVED_DATE,((request.getParameter(COMConstants.RTQ_RECEIVED_DATE)) !=null?request.getParameter(COMConstants.RTQ_RECEIVED_DATE).toString().trim():""));
      parameters.put(COMConstants.RTQ_DELIVERY_DATE,((request.getParameter(COMConstants.RTQ_DELIVERY_DATE)) !=null?request.getParameter(COMConstants.RTQ_DELIVERY_DATE).toString().trim():""));
      parameters.put(COMConstants.RTQ_TAG_DATE,((request.getParameter(COMConstants.RTQ_TAG_DATE)) !=null?request.getParameter(COMConstants.RTQ_TAG_DATE).toString().trim():""));
      parameters.put(COMConstants.RTQ_LAST_TOUCHED,((request.getParameter(COMConstants.RTQ_LAST_TOUCHED)) !=null?request.getParameter(COMConstants.RTQ_LAST_TOUCHED).toString().trim():""));
      parameters.put(COMConstants.RTQ_TYPE,((request.getParameter(COMConstants.RTQ_TYPE)) !=null?request.getParameter(COMConstants.RTQ_TYPE).toString().trim():""));
      parameters.put(COMConstants.RTQ_SYS,((request.getParameter(COMConstants.RTQ_SYS)) !=null?request.getParameter(COMConstants.RTQ_SYS).toString().trim():""));
      parameters.put(COMConstants.RTQ_TIMEZONE,((request.getParameter(COMConstants.RTQ_TIMEZONE)) !=null?request.getParameter(COMConstants.RTQ_TIMEZONE).toString().trim():""));
      parameters.put(COMConstants.RTQ_SDG,((request.getParameter(COMConstants.RTQ_SDG)) !=null?request.getParameter(COMConstants.RTQ_SDG).toString().trim():""));
      parameters.put(COMConstants.RTQ_DISPOSITION,((request.getParameter(COMConstants.RTQ_DISPOSITION)) !=null?request.getParameter(COMConstants.RTQ_DISPOSITION).toString().trim():""));
      parameters.put(COMConstants.RTQ_PRIORITY,((request.getParameter(COMConstants.RTQ_PRIORITY)) !=null?request.getParameter(COMConstants.RTQ_PRIORITY).toString().trim():""));
      parameters.put(COMConstants.RTQ_NEW_ITEM,((request.getParameter(COMConstants.RTQ_NEW_ITEM)) !=null?request.getParameter(COMConstants.RTQ_NEW_ITEM).toString().trim():""));
      parameters.put(COMConstants.RTQ_OCCASION,((request.getParameter(COMConstants.RTQ_OCCASION)) !=null?request.getParameter(COMConstants.RTQ_OCCASION).toString().trim():""));
      parameters.put(COMConstants.RTQ_LOCATION,((request.getParameter(COMConstants.RTQ_LOCATION)) !=null?request.getParameter(COMConstants.RTQ_LOCATION).toString().trim():""));
      parameters.put(COMConstants.RTQ_MEMBER_NUMBER,((request.getParameter(COMConstants.RTQ_MEMBER_NUMBER)) !=null?request.getParameter(COMConstants.RTQ_MEMBER_NUMBER).toString().trim():""));
      parameters.put(COMConstants.RTQ_ZIP,((request.getParameter(COMConstants.RTQ_ZIP)) !=null?request.getParameter(COMConstants.RTQ_ZIP).toString().trim():""));
      parameters.put(COMConstants.RTQ_TAG_BY,((request.getParameter(COMConstants.RTQ_TAG_BY)) !=null?request.getParameter(COMConstants.RTQ_TAG_BY).toString().trim():""));
      parameters.put(COMConstants.RTQ_FILTER,((request.getParameter(COMConstants.RTQ_FILTER)) !=null?request.getParameter(COMConstants.RTQ_FILTER).toString().trim():""));
    }


    /**
     * Populate Header data
     * 
     */
    private void populateHeaderInfo(ServletRequest request, HashMap parameters)
    {
      parameters.put(COMConstants.H_BRAND_NAME,                   ((request.getParameter(COMConstants.H_BRAND_NAME))                  !=null?request.getParameter(COMConstants.H_BRAND_NAME).toString().trim():""));
      parameters.put(COMConstants.H_CUSTOMER_ORDER_INDICATOR,     ((request.getParameter(COMConstants.H_CUSTOMER_ORDER_INDICATOR))    !=null?request.getParameter(COMConstants.H_CUSTOMER_ORDER_INDICATOR).toString().trim():""));
      parameters.put(COMConstants.H_CUSTOMER_SERVICE_NUMBER,      ((request.getParameter(COMConstants.H_CUSTOMER_SERVICE_NUMBER))     !=null?request.getParameter(COMConstants.H_CUSTOMER_SERVICE_NUMBER).toString().trim():""));
      parameters.put(COMConstants.H_DNIS_ID,                      ((request.getParameter(COMConstants.H_DNIS_ID))                     !=null?request.getParameter(COMConstants.H_DNIS_ID).toString().trim():""));

    }
    
    /**
     * Populate Search Criteria
     * 
     */
    private void populateSearchCriteria(ServletRequest request, HashMap parameters)
    {
      parameters.put(COMConstants.SC_ORDER_NUMBER,                ((request.getParameter(COMConstants.SC_ORDER_NUMBER))               !=null?request.getParameter(COMConstants.SC_ORDER_NUMBER).toString().trim():""));
      parameters.put(COMConstants.SC_LAST_NAME,                   ((request.getParameter(COMConstants.SC_LAST_NAME))                  !=null?request.getParameter(COMConstants.SC_LAST_NAME).toString().trim():""));
      parameters.put(COMConstants.SC_PHONE_NUMBER,                ((request.getParameter(COMConstants.SC_PHONE_NUMBER))               !=null?request.getParameter(COMConstants.SC_PHONE_NUMBER).toString().trim():""));
      parameters.put(COMConstants.SC_EMAIL_ADDRESS,               ((request.getParameter(COMConstants.SC_EMAIL_ADDRESS))              !=null?request.getParameter(COMConstants.SC_EMAIL_ADDRESS).toString().trim():""));
      parameters.put(COMConstants.SC_ZIP_CODE,                    ((request.getParameter(COMConstants.SC_ZIP_CODE))                   !=null?request.getParameter(COMConstants.SC_ZIP_CODE).toString().trim():""));
      parameters.put(COMConstants.SC_CUST_NUMBER,                 ((request.getParameter(COMConstants.SC_CUST_NUMBER))                !=null?request.getParameter(COMConstants.SC_CUST_NUMBER).toString().trim():""));
      parameters.put(COMConstants.SC_TRACKING_NUMBER,             ((request.getParameter(COMConstants.SC_TRACKING_NUMBER))            !=null?request.getParameter(COMConstants.SC_TRACKING_NUMBER).toString().trim():""));
      parameters.put(COMConstants.SC_MEMBERSHIP_NUMBER,           ((request.getParameter(COMConstants.SC_MEMBERSHIP_NUMBER))          !=null?request.getParameter(COMConstants.SC_MEMBERSHIP_NUMBER).toString().trim():""));
      parameters.put(COMConstants.SC_PC_MEMBERSHIP,           	  ((request.getParameter(COMConstants.SC_PC_MEMBERSHIP))          	  !=null?request.getParameter(COMConstants.SC_PC_MEMBERSHIP).toString().trim():""));
      parameters.put(COMConstants.SC_PRO_ORDER_NUMBER,  ((request.getParameter(COMConstants.SC_PRO_ORDER_NUMBER)) 	!= null ? request.getParameter(COMConstants.SC_PRO_ORDER_NUMBER).toString().trim() 	: ""));

      String scCustInd = request.getParameter(COMConstants.SC_CUST_IND);
      if ((scCustInd == null) || scCustInd.equalsIgnoreCase("") || scCustInd.equalsIgnoreCase("Y"))
      {
        parameters.put(COMConstants.SC_CUST_IND, "Y");
      }
      else
      {
        parameters.put(COMConstants.SC_CUST_IND, "N");
      }
        
      String scRecipInd = request.getParameter(COMConstants.SC_RECIP_IND);
      if ((scRecipInd == null)|| scRecipInd.equalsIgnoreCase("") || scRecipInd.equalsIgnoreCase("Y"))
      {
        parameters.put(COMConstants.SC_RECIP_IND, "Y");
      }
      else
      {
       parameters.put(COMConstants.SC_RECIP_IND, "N");
      }
  }

    /**
     * Populate Update Order info
     * 
     */
    private void populateUpdateOrderInfo(ServletRequest request, HashMap parameters)
    {
      parameters.put(COMConstants.UO_DISPLAY_PAGE,              ((request.getParameter(COMConstants.UO_DISPLAY_PAGE))             !=null?request.getParameter(COMConstants.UO_DISPLAY_PAGE).toString().trim():""));
      parameters.put(COMConstants.FLORIST_CANT_FULFILL,         ((request.getParameter(COMConstants.FLORIST_CANT_FULFILL))        !=null?request.getParameter(COMConstants.FLORIST_CANT_FULFILL).toString().trim():""));
      parameters.put(COMConstants.MODIFY_ORDER_UPDATED,         ((request.getParameter(COMConstants.MODIFY_ORDER_UPDATED))        !=null?request.getParameter(COMConstants.MODIFY_ORDER_UPDATED).toString().trim():""));
  

    }
    /**
     * Populate Prev Page info
     * 
     */
    private void populatePrevPageInfo(ServletRequest request, HashMap parameters)
    {
        parameters.put(COMConstants.PREV_PAGE, request.getParameter(COMConstants.PREV_PAGE));
        parameters.put(COMConstants.PREV_PAGE_POSITION, request.getParameter(COMConstants.PREV_PAGE_POSITION));
        
        //Added ,To retain refund review filters.
        parameters.put(COMConstants.PREV_SORT_COLUMN_NAME, request.getParameter(COMConstants.PREV_SORT_COLUMN_NAME));
        parameters.put(COMConstants.PREV_SORT_COLUMN_STATUS, request.getParameter(COMConstants.PREV_SORT_COLUMN_STATUS));
        parameters.put(COMConstants.PREV_CURRENT_SORT_COLUMN_STATUS, request.getParameter(COMConstants.PREV_CURRENT_SORT_COLUMN_STATUS));
        parameters.put(COMConstants.PREV_IS_FROM_HEADER, request.getParameter(COMConstants.PREV_IS_FROM_HEADER));
        parameters.put(COMConstants.PREV_USER_ID_FILTER, request.getParameter(COMConstants.PREV_USER_ID_FILTER));
        parameters.put(COMConstants.PREV_GROUP_ID_FILTER, request.getParameter(COMConstants.PREV_GROUP_ID_FILTER));
        parameters.put(COMConstants.PREV_CODES_FILTER, request.getParameter(COMConstants.PREV_CODES_FILTER));
       
    }
    
     /**
     * Populate bypass com data
     * 
     */
    private void populateBypassComInfo(ServletRequest request, HashMap parameters)
    {
        parameters.put(COMConstants.BYPASS_COM,  request.getParameter(COMConstants.BYPASS_COM));
    }
    
    /**
    * Populate lp data
    * 
    */
    private void populateLossPreventionInfo(ServletRequest request, HashMap parameters)
    {
        parameters.put(COMConstants.LP_PERMISSION, request.getParameter(COMConstants.LP_PERMISSION));
    }
    
    /**
    * Populate Privacy Policy data
    * 
    */
    private void populateBypassPrivacyPolicyVerificationInfo(ServletRequest request, HashMap parameters)
    {
        parameters.put(COMConstants.BYPASS_PRIVACY_POLICY_VERIFICATION, request.getParameter(COMConstants.BYPASS_PRIVACY_POLICY_VERIFICATION));
    }
      
    /**
     * Populate Call Disposition (decision_result) data
     * 
     */
    private void populateDispoWidgetInfo(ServletRequest request, HashMap parameters) throws Exception
    {
        // If reset flag is set, clear out all data.  Otherwise propogate.
        //
        if ("Y".equalsIgnoreCase(request.getParameter(COMConstants.CDISP_RESET_FLAG))) {
            parameters.put(COMConstants.CDISP_AUTO_OPENED_FLAG, "");
            parameters.put(COMConstants.CDISP_REMINDER_FLAG,    "");
            parameters.put(COMConstants.CDISP_CANCELLED_ORDER,  "");
            parameters.put(COMConstants.CDISP_RESET_FLAG,       "");
            //  Not resetting below since set in DnisAction class
            parameters.put(COMConstants.CDISP_ENABLED_FLAG,     ((request.getParameter(COMConstants.CDISP_ENABLED_FLAG))     !=null?request.getParameter(COMConstants.CDISP_ENABLED_FLAG).toString().trim():""));
            //  Not resetting below since originates from global_parms
            parameters.put(COMConstants.CDISP_REMINDER_FLAG,    ((request.getParameter(COMConstants.CDISP_REMINDER_FLAG))    !=null?request.getParameter(COMConstants.CDISP_REMINDER_FLAG).toString().trim():""));
        } else {
            parameters.put(COMConstants.CDISP_AUTO_OPENED_FLAG, ((request.getParameter(COMConstants.CDISP_AUTO_OPENED_FLAG)) !=null?request.getParameter(COMConstants.CDISP_AUTO_OPENED_FLAG).toString().trim():""));
            parameters.put(COMConstants.CDISP_CANCELLED_ORDER,  ((request.getParameter(COMConstants.CDISP_CANCELLED_ORDER))  !=null?request.getParameter(COMConstants.CDISP_CANCELLED_ORDER).toString().trim():""));
            parameters.put(COMConstants.CDISP_ENABLED_FLAG,     ((request.getParameter(COMConstants.CDISP_ENABLED_FLAG))     !=null?request.getParameter(COMConstants.CDISP_ENABLED_FLAG).toString().trim():""));
            parameters.put(COMConstants.CDISP_RESET_FLAG,       ((request.getParameter(COMConstants.CDISP_RESET_FLAG))       !=null?request.getParameter(COMConstants.CDISP_RESET_FLAG).toString().trim():""));
            String reminderFlag = (String)parameters.get(COMConstants.CDISP_REMINDER_FLAG);
            // Note that CDISP_REMINDER_FLAG originates from global_parms, so only set it if empty
            if ((reminderFlag == null) || (reminderFlag.length() <= 0)) {
                ConfigurationUtil cu = ConfigurationUtil.getInstance();
                reminderFlag = cu.getFrpGlobalParm(COMConstants.COM_CONTEXT, COMConstants.CONS_CDISP_REMINDER);    
                parameters.put(COMConstants.CDISP_REMINDER_FLAG, reminderFlag);
            }
        }
    }
    
    /**
     * Static method to allow retrieval of parameter data
     */
    public static String getParameter(ServletRequest request, String name) 
        throws Exception 
    {
        HashMap parameters = null;
        String retString = null;
        
        if (request.getAttribute(COMConstants.CONS_APP_PARAMETERS) != null) {
            parameters = (HashMap)request.getAttribute(COMConstants.CONS_APP_PARAMETERS);
        }
        if (parameters != null) {
            retString = (String)parameters.get(name);
        }
        return retString;
    }
    
    /**
     * Static method to allow updates of parameter data
     */
    public static void updateParameter(ServletRequest request, String name, String value) 
        throws Exception 
    {
        HashMap parameters  = new HashMap();
        
        if (request.getAttribute(COMConstants.CONS_APP_PARAMETERS) != null) {
            parameters = (HashMap)request.getAttribute(COMConstants.CONS_APP_PARAMETERS);
        }
        parameters.put(name, value);
        request.setAttribute(COMConstants.CONS_APP_PARAMETERS, parameters);
    }
}