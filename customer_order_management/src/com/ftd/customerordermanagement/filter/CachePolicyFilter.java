package com.ftd.customerordermanagement.filter;

import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;

import java.util.Enumeration;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

public class CachePolicyFilter implements Filter
{
    private FilterConfig _filterConfig = null;
    private Logger logger = new Logger("com.ftd.customerordermanagement.filter.CachePolicyFilter");


    public void init(FilterConfig filterConfig)
        throws ServletException
    {
        _filterConfig = filterConfig;
    }


    public void destroy()
    {
        _filterConfig = null;
    }


    /**
     * Get shared data.
     * 
     *   
     * @param ServletRequest  - input request object
     * @param ServletResponse - object used to respond to the user
     * @param FilterChain     - next program to be executed in the chain
     * 
     * @throws IOException
     * @throws ServletException
     */
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
        throws IOException, ServletException
    {
            HttpServletResponse res =
              (HttpServletResponse) response;
            // set the provided HTTP response parameters
            for (Enumeration e=_filterConfig.getInitParameterNames();
                e.hasMoreElements();) {
              String headerName = (String)e.nextElement();
              res.addHeader(headerName,
                         _filterConfig.getInitParameter(headerName));
            }
            // pass the request/response on
            chain.doFilter(request, response);
    }

}