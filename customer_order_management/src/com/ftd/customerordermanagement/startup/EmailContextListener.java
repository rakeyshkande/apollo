package com.ftd.customerordermanagement.startup;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.handler.EmailEventHandler;
import com.ftd.osp.utilities.ConfigurationUtil;

import java.io.IOException;
import javax.naming.InitialContext;

import javax.servlet.GenericServlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.ftd.osp.utilities.plugins.Logger;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

/**
 * This Application event listener classes implements one or more of the Servlet event listener interfaces.
 * It is instantiated and registered in the web container at the time of the deployment of the web application.
 * The event listener gets invoked automatically when a certain lifecycle events occur e.g. when a Servlet context is created.
 *
 * @author Luke W. Pennings
 */

public class EmailContextListener extends GenericServlet
{
  protected static Logger logger = new Logger(EmailContextListener.class.getName());
  public void init(ServletConfig config) throws ServletException
  {
    super.init(config); 
    logger.debug("Scheduling Job ..");
    try
    {
      ConfigurationUtil cu = null;
      cu = ConfigurationUtil.getInstance();

      InitialContext jndiContext = new InitialContext();
      // Bind a Remote object for invocation by the MDB for
      // outbound messages.
      //get alias name
//      String alias = (String) cu.getProperty(COMConstants.PROPERTY_FILE,COMConstants.EMAIL_ALIAS);
      String alias = (String) jndiContext.lookup("java:comp/env/ieventhandler-bind-name"); 

      EmailEventHandler emailEventHandler = new EmailEventHandler();
      jndiContext.rebind(alias, emailEventHandler);
      logger.info("Successfully bound EmailEventHandler implementation to alias: " + alias);
    }
    catch (Throwable e)
    {
      logger.error("init: Failed to initialize EMAIL servlet.", e);
    }

  }

  public void service(ServletRequest arg0, ServletResponse arg1)
       throws ServletException, IOException
  {

  }

  public String getServletInfo()
  {
    return null;
  }
}
