/**
 * 
 */
package com.ftd.customerordermanagement.startup;

import java.io.IOException;

import javax.servlet.GenericServlet;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import com.ftd.customerordermanagement.bo.GiftCardBO;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * @author cjohnson
 *
 */
public class InitServlet extends GenericServlet {
	
	private static Logger logger = new Logger(InitServlet.class.getName());

	/**
	 * 
	 */
	public InitServlet() {
		//warm up the PaymentService client reference so the user doesn't have to wait for it to load the first time
		logger.warn("warming up the payment service client");
		try {
			GiftCardBO.getPaymentServiceClient();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/* (non-Javadoc)
	 * @see javax.servlet.GenericServlet#service(javax.servlet.ServletRequest, javax.servlet.ServletResponse)
	 */
	@Override
	public void service(ServletRequest arg0, ServletResponse arg1)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

	}

}
