package com.ftd.customerordermanagement.bo;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.dao.GcCouponDAO;
import com.ftd.customerordermanagement.vo.GcCouponVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.GiftCodeUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.RetrieveGiftCodeResponse;
import com.ftd.osp.utilities.xml.DOMUtil;

/**
 * This class handles Gift Certificate and Coupon logic.
 *
 * @author Luke W. Pennings
 */

public class GcCouponBO
{
  private Logger logger = new Logger("com.ftd.customerordermanagement.bo.GcCouponBO");
  private static final String MESSAGE_ROOT_NODE = "root";
  private static String ACCEPT = "Accept";
  private static String CONTENT_TYPE = "Content-Type";
  private static String APPLICATION_JSON = "application/json";
  
  //database:
  private Connection conn = null;

   /**
   * constructor
   * 
   * @param none
   * @return n/a
   * @throws n/a
   */
  public GcCouponBO(Connection conn)
  {
    super();
    this.conn = conn;
  }


  /**
   * validate the gift certificate/coupon - overloaded method
   * @param String gcCouponNumber - gift certificate number
   * @param String totalOwed - total amount owed
   * @return Document - message text
   * @throws Exception
   * @throws IOException
   * @throws ParserConfigurationException
   * @throws SAXException
   * @throws SQLException
   * @throws TransformerException
   * @throws XSLException
   */

  public Document validateGiftCertificateCoupon(String gcCouponNumber, String totalOwed)
         throws IOException, ParserConfigurationException, SAXException, SQLException,
                TransformerException, Exception
  {
    return validateGiftCertificateCoupon(gcCouponNumber, totalOwed, null);
  }


  /**
   * validate the gift certificate/coupon
   * @param String gcCouponNumber - gift certificate number
   * @param String totalOwed - total amount owed
   * @param String methodToInvoke - parameter that will dictate as to what method needs to be 
   *                                invoked when control is transfered to the XSL
   * @return Document - message text
   * @throws Exception
   * @throws IOException
   * @throws ParserConfigurationException
   * @throws SAXException
   * @throws SQLException
   * @throws TransformerException
   * @throws XSLException
   */

  public Document validateGiftCertificateCoupon(String gcCouponNumber, String totalOwed, String methodToInvoke)
         throws IOException, ParserConfigurationException, SAXException, SQLException,
                TransformerException, Exception
  {
    GcCouponVO gccVO = null;
    Document returnDocument;
    GcCouponDAO gccDAO = new GcCouponDAO(conn);
    RetrieveGiftCodeResponse retrieveGiftCodeResponse = null;

    try{
    	retrieveGiftCodeResponse = GiftCodeUtil.getGiftCodeById(gcCouponNumber.toUpperCase());
    }
    catch(Exception e){
    	//something went wrong calling Gift Code Service, return error to user
    	returnDocument = this.createInvalidGccXML("error", totalOwed, methodToInvoke); 
    	return returnDocument;
    }
    
    if(retrieveGiftCodeResponse != null){
      	gccVO = new GcCouponVO();
    	gccVO.setGcCouponNumber(retrieveGiftCodeResponse.getGiftCodeId());
    	gccVO.setStatus(retrieveGiftCodeResponse.getStatus());
    	gccVO.setAmount(String.valueOf(retrieveGiftCodeResponse.getRemainingAmount()));
  	
    	if(retrieveGiftCodeResponse.getExpirationDate() != null && !retrieveGiftCodeResponse.getExpirationDate().equals("")){
    		  Date gcExpDt = new Date(retrieveGiftCodeResponse.getExpirationDate());
    		  SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
    		  String formattedGcExpDt = null;

    		  try {
    		      formattedGcExpDt = sdf.format(gcExpDt);
    		  } catch (Exception e) {
    			  logger.info(e);
    		  }
    		
    		gccVO.setExpDate(formattedGcExpDt);
    	}
    }
    if(gccVO == null)
    {
      returnDocument = this.createInvalidGccXML(null, totalOwed, methodToInvoke); 
    }
    else if(gccVO.getStatus().equalsIgnoreCase(COMConstants.GCC_STATUS_CANCELLED) ||
            gccVO.getStatus().equalsIgnoreCase(COMConstants.GCC_STATUS_REDEEMED) ||
            gccVO.getStatus().equalsIgnoreCase(COMConstants.GCC_STATUS_ISSUED_EXPIRED) ||
            gccVO.getStatus().equalsIgnoreCase(COMConstants.GCC_STATUS_ISSUED_INACTIVE) ||
            gccVO.getStatus().equalsIgnoreCase(COMConstants.GCC_STATUS_REFUND) )
    {
    	returnDocument = this.createInvalidGccXML(gccVO.getStatus(), totalOwed, methodToInvoke);
    }
    else
    {
      //Convert the string to date
      //Define the format
      DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
      Date expDate = null;
      if (gccVO.getExpDate() != null)
      {
    	  expDate = (Date)formatter.parse(gccVO.getExpDate());
      }
      Date todayDate = new Date();

      if(expDate != null && todayDate.after(expDate))
      {
        //gift certificate is expired
        returnDocument = this.createRejectedGccXML(COMConstants.GCC_STATUS_ISSUED_EXPIRED, totalOwed, methodToInvoke);
      }
      else
      {
          returnDocument = calculateValidGCXML(totalOwed, gccVO.getGcCouponNumber(), methodToInvoke, gccVO.getAmount());
      }
    }
    
    return returnDocument;
  }
  
  public Document calculateValidGCXML(String totalOwed, String gcNum, String methodToInvoke, String balance)
  throws IOException, ParserConfigurationException, SAXException, SQLException, TransformerException,
  Exception
  {
      logger.debug("totalOwed:" + totalOwed);
      logger.debug("GC Amount:" + balance);

      BigDecimal tOwed = new BigDecimal(totalOwed);
      BigDecimal gcAmt = new BigDecimal(balance);
      BigDecimal netOwed = tOwed.subtract(gcAmt);
      boolean amountOwed = false;
      if(netOwed.doubleValue() > 0)
      {
          amountOwed = true;
      }

      Document returnDocument = createValidGccXML(balance, gcNum, amountOwed, netOwed.toString(), methodToInvoke);
      return returnDocument;
  }

  
  /**
   * create an invalid xml document
   * @param status
   * @return Document - invalid document text
   * @throws Exception
   * @throws IOException
   * @throws ParserConfigurationException
   * @throws SAXException
   * @throws SQLException
   * @throws TransformerException
   * @throws XSLException
   */  
  public Document createInvalidGccXML(String status, String totalOwed, String methodToInvoke)
         throws IOException, ParserConfigurationException, SAXException, SQLException,
                TransformerException, Exception
  {
     //Call the ConfigurationUtil utility to obtain the credit card declined message from customer_order_management_config.xml
     ConfigurationUtil cu = null;
     cu = ConfigurationUtil.getInstance();
     String ccMessage = null;
     
     if (status == null)
     {
         ccMessage = (String) cu.getProperty(COMConstants.PROPERTY_FILE,COMConstants.GCC_INVALID_MSG);
     }
     else if (status.equalsIgnoreCase(null))
     {
       ccMessage = (String) cu.getProperty(COMConstants.PROPERTY_FILE,COMConstants.GCC_INVALID_MSG);
     }
     else if(status.equalsIgnoreCase(COMConstants.GCC_STATUS_REDEEMED))
     {
       ccMessage = (String) cu.getProperty(COMConstants.PROPERTY_FILE,COMConstants.GCC_REDEEMED_MSG);  
     }
     else if(status.equalsIgnoreCase(COMConstants.GCC_STATUS_REFUND))
     {
       ccMessage = (String) cu.getProperty(COMConstants.PROPERTY_FILE,COMConstants.GCC_REFUND_MSG);  
     }
     else if(status.equalsIgnoreCase(COMConstants.GCC_STATUS_CANCELLED))
     {
       ccMessage = (String) cu.getProperty(COMConstants.PROPERTY_FILE,COMConstants.GCC_CANCELLED_MSG);  
     }
     else if(status.equalsIgnoreCase(COMConstants.GCC_STATUS_ISSUED_EXPIRED))
     {
       ccMessage = (String) cu.getProperty(COMConstants.PROPERTY_FILE,COMConstants.GCC_EXPIRED_MSG);  
     }
     else if(status.equalsIgnoreCase(COMConstants.GCC_STATUS_ISSUED_INACTIVE))
     {
       ccMessage = (String) cu.getProperty(COMConstants.PROPERTY_FILE,COMConstants.GCC_INACTIVE_MSG);  
     }
     else if(status.equalsIgnoreCase("error"))
     {
       ccMessage = (String) cu.getProperty(COMConstants.PROPERTY_FILE,COMConstants.GCC_ERROR_MSG);  
     }

     Document returnDocument = DOMUtil.getDefaultDocument();
     Element root = (Element) returnDocument.createElement(MESSAGE_ROOT_NODE);
     returnDocument.appendChild(root);

     Element node;
     node = (Element)returnDocument.createElement("gcc_status");
     node.appendChild(returnDocument.createTextNode("invalid"));
     root.appendChild(node);

     node = (Element)returnDocument.createElement("total_owed");
     node.appendChild(returnDocument.createTextNode(totalOwed));
     root.appendChild(node);

     node = (Element)returnDocument.createElement("message");
     node.appendChild(returnDocument.createTextNode(ccMessage));
     root.appendChild(node);

     node = (Element)returnDocument.createElement("method_to_invoke");
     node.appendChild(returnDocument.createTextNode(methodToInvoke));
     root.appendChild(node);

     return returnDocument;  
  }
 
   /**
   * create a rejected xml document
   * @param status
   * @return Document - invalid document text
   * @throws Exception
   * @throws IOException
   * @throws ParserConfigurationException
   * @throws SAXException
   * @throws SQLException
   * @throws TransformerException
   * @throws XSLException
   */   
  public Document createRejectedGccXML(String status, String totalOwed, String methodToInvoke)
         throws IOException, ParserConfigurationException, SAXException, SQLException,
                TransformerException, Exception
  {
     //Call the ConfigurationUtil utility to obtain the credit card declined message from customer_order_management_config.xml
     ConfigurationUtil cu = null;
     cu = ConfigurationUtil.getInstance();
     String ccMessage = null;
     
     if (status.equalsIgnoreCase(null))
     {
       ccMessage = (String) cu.getProperty(COMConstants.PROPERTY_FILE,COMConstants.GCC_INVALID_MSG);
     }
     else if(status.equalsIgnoreCase(COMConstants.GCC_STATUS_REDEEMED))
     {
       ccMessage = (String) cu.getProperty(COMConstants.PROPERTY_FILE,COMConstants.GCC_REDEEMED_MSG);  
     }
     else if(status.equalsIgnoreCase(COMConstants.GCC_STATUS_CANCELLED))
     {
       ccMessage = (String) cu.getProperty(COMConstants.PROPERTY_FILE,COMConstants.GCC_CANCELLED_MSG);  
     }
     else if(status.equalsIgnoreCase(COMConstants.GCC_STATUS_ISSUED_EXPIRED))
     {
       ccMessage = (String) cu.getProperty(COMConstants.PROPERTY_FILE,COMConstants.GCC_EXPIRED_MSG);  
     }

     Document returnDocument = DOMUtil.getDefaultDocument();
     Element root = (Element) returnDocument.createElement(MESSAGE_ROOT_NODE);
     returnDocument.appendChild(root);

     Element node;
     node = (Element)returnDocument.createElement("gcc_status");
     node.appendChild(returnDocument.createTextNode("rejected"));
     root.appendChild(node);

     node = (Element)returnDocument.createElement("total_owed");
     node.appendChild(returnDocument.createTextNode(totalOwed));
     root.appendChild(node);

     node = (Element)returnDocument.createElement("message");
     node.appendChild(returnDocument.createTextNode(ccMessage));
     root.appendChild(node);

     node = (Element)returnDocument.createElement("method_to_invoke");
     node.appendChild(returnDocument.createTextNode(methodToInvoke));
     root.appendChild(node);

     return returnDocument;  
  }

  /**
   * create a valid xml document
   * @param balanceAvailable
   * @param gccNumber
   * @param amountOwed
   * @param netOwed
   * @param methodToInvoke
   * @return
   * @throws IOException
   * @throws ParserConfigurationException
   * @throws SAXException
   * @throws SQLException
   * @throws TransformerException
   * @throws Exception
   */
  private Document createValidGccXML(String balanceAvailable, String gccNumber, boolean amountOwed, String netOwed, String methodToInvoke)
  throws IOException, ParserConfigurationException, SAXException, SQLException,
  TransformerException, Exception
  {
      Document returnDocument = DOMUtil.getDefaultDocument();
      Element root = (Element) returnDocument.createElement(MESSAGE_ROOT_NODE);
      returnDocument.appendChild(root);

      Element node;
      node = (Element)returnDocument.createElement("gcc_status");
      node.appendChild(returnDocument.createTextNode("valid"));
      root.appendChild(node);

      node = (Element)returnDocument.createElement("amount_owed");
      node.appendChild(returnDocument.createTextNode(String.valueOf(amountOwed)));
      root.appendChild(node);

      node = (Element)returnDocument.createElement("amount");
      node.appendChild(returnDocument.createTextNode(balanceAvailable));
      root.appendChild(node);

      node = (Element)returnDocument.createElement("net_owed");
      node.appendChild(returnDocument.createTextNode(netOwed));
      root.appendChild(node);

      node = (Element)returnDocument.createElement("gift_number");
      node.appendChild(returnDocument.createTextNode(gccNumber));
      root.appendChild(node);

      node = (Element)returnDocument.createElement("method_to_invoke");
      node.appendChild(returnDocument.createTextNode(methodToInvoke));
      root.appendChild(node);

      return returnDocument;  
  }
  
}