package com.ftd.customerordermanagement.bo;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;




import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

import org.xml.sax.SAXException;

/**
 * This class creates the script for the Customer Service DNIS Entered screen and
 * appends the script data to a DOM document.
 *
 * @author Matt Wilcoxen
 */

public class CSDnisBO
{
    private Logger logger = new Logger("com.ftd.customerordermanagement.bo.CSDnisBO");

    //column sort order
    private static final String BRAND_NAME = "brand_name";
    private static final String CSR_ID = "csr_id";

    //elements:
    private static final String X_SCRIPT_DATA_NODE  = "script_data";
    private static final String X_CS_SCRIPT_NODE    = "cs_script";

    //security:  security on/off ind, security manager
    private String csrId = null;

    //configuration utility
    private ConfigurationUtil cu = null;

    /**
     * constructor
     * 
     * @param Connection - database connection
     * @return n/a
     * @throws n/a
     */
    public CSDnisBO()
    {
        super();
    }


    /**
     * append csr script to document
     *   1. get brand name and csr name
     *   2. add csr script node to root document to produce the following DOM
     *      <root>
     *           <script_data>
     *               <cs_script>THANK YOU FOR CALLING SAN FRANCISCO MUSIC BOX.  THIS IS MIKE HOW MAY I ASSIST YOU TODAY?</cs_script>
     *           </script_data>
     *       </root>
     *
     * @param QueueRequestVO - queue request value object
     * 
     * @return Document      - queue request data
     * 
     * @throws Exception
     * @throws IOException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws TransformerException
     * @throws XSLException
     */
    public Document appendScript(Document doc, String firstName, String brandName)
        throws IOException, ParserConfigurationException, SAXException, 
               TransformerException, Exception
    {
        cu = ConfigurationUtil.getInstance();

        Document csDnisXMLDoc = (Document) doc;

        // 2. add csr script node to root document
        String scriptName = createScript(brandName,firstName);
        Element scriptEle, newEle = null;
        Text newText = null;

        newEle = csDnisXMLDoc.createElement(X_CS_SCRIPT_NODE);
        newText = csDnisXMLDoc.createTextNode(scriptName);
        newEle.appendChild(newText);

        scriptEle = csDnisXMLDoc.createElement(X_SCRIPT_DATA_NODE);
        scriptEle.appendChild(newEle);

        csDnisXMLDoc.getDocumentElement().appendChild(scriptEle);

        if(logger.isDebugEnabled())
        {
            //IMPORTANT:  Because StringWriter contents are NOT removed even when the flush
            //            method is executed, make StringWriter go away through scope control.
            //            Both the flush and close methods were tested with but were of non-affect.
            StringWriter sw = new StringWriter();       //string representation of xml document
            DOMUtil.print(csDnisXMLDoc,new PrintWriter(sw));
            logger.debug("appendScript(): document = \n" + sw.toString());
        }

        return csDnisXMLDoc;
    }


    /**
     * create csr script
     *     1. get template script
     *     2. modify script to include brand name and csr name
     * 
     * @param  String - brand name
     * @param  String - csr name
     * 
     * @return String - csr script with brand name and csr name
     * 
     * @throws IOException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws TransformerException
     * @throws XSLException
     */
    private String createScript(String brandName, String firstName)
        throws IOException, ParserConfigurationException, SAXException, 
               TransformerException
    {
        String custServScript = cu.getProperty(COMConstants.PROPERTY_FILE, COMConstants.CONS_CUST_SERV_SCRIPT);

        custServScript = custServScript.replaceFirst(BRAND_NAME,brandName);
        custServScript = custServScript.replaceFirst(CSR_ID,firstName);

        return custServScript;
    }

}