package com.ftd.customerordermanagement.bo;

import com.ftd.customerordermanagement.cache.handler.DispositionCodeHandler;
import com.ftd.customerordermanagement.cache.handler.PriorityCodeHandler;
import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.dao.TagDAO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.security.SecurityManager;
import com.ftd.security.cache.vo.UserInfo;

import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.HashMap;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;




import org.w3c.dom.Document;

import org.xml.sax.SAXException;
/**
 * This class handles taggic logic.
 *
 * @author Matt Wilcoxen
 */

public class TagBO
{
    private Logger logger = new Logger("com.ftd.customerordermanagement.bo.TagBO");

    //database:
    private Connection conn = null;
    
    //security:  security on/off ind, security manager
    private boolean securityIsOn = false;
    private SecurityManager sm = null;
    private UserInfo ui = null;
    private String csrId = null;


    /**
     * constructor
     * 
     * @param Connection - database connection
     * 
     * @return n/a
     * 
     * @throws n/a
     */
    public TagBO(Connection conn)
    {
        super();
        this.conn = conn;
    }


    /**
     * tag order to csr
     *   1. get csr id
     *   2. tag order to csr
     *   3. return indicator of whether order has multiple tags
     *
     * @param String  - disposition code
     * @param String  - priority code
     * @param String  - security token
     * @param String  - order detail id
     * 
     * @return HashMap - multiple tags on order ?
     *                 - order already tagged to csr ?
     *                 
     * @throws Exception
     * @throws IOException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws SQLException
     * @throws TransformerException
     * @throws XSLException
     */
    public HashMap tagOrder(String dispositionCode, String priorityCode,
                            String securityToken, String orderDetailId)
        throws IOException, ParserConfigurationException, SAXException, SQLException,
                TransformerException, Exception
    {
        ConfigurationUtil cu = ConfigurationUtil.getInstance();
        String security = "";

        // 1. get csr Id
        security = (String) cu.getProperty(COMConstants.PROPERTY_FILE,COMConstants.SECURITY_IS_ON);
        securityIsOn = security.equalsIgnoreCase("true") ? true : false;
        if(securityIsOn)
        {
            sm = SecurityManager.getInstance();
            ui = sm.getUserInfo(securityToken);
            csrId = ui.getUserID();
        }

        // 2. tag order to csr
        TagDAO tDAO = new TagDAO(conn);
        HashMap tagResults = tDAO.insertOrderTag(dispositionCode, priorityCode, csrId, orderDetailId, securityToken);

        // 3. return hashmap result of tag
        return tagResults;
    }

    /**
     * get priority codes
     *   1. get priority codes
     *   2. return Document to calling module
     *
     * @param None
     * 
     * @return Document - priority codes
     *                 
     * @throws Exception
     * @throws IOException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws SQLException
     * @throws TransformerException
     * @throws XSLException
     */
     
    public Document getPriorityCodes()
         throws IOException, ParserConfigurationException, SAXException, SQLException,
                TransformerException, Exception
    {
        PriorityCodeHandler pch = null;
        Document priorCodeDoc = null;
        pch = (PriorityCodeHandler)CacheManager.getInstance().getHandler(COMConstants.PRIORITY_CODE_HANDLER);       
        priorCodeDoc = pch.getPriorityCodeDoc();
      
        return priorCodeDoc;   
    }

    /**
     * get disposition codes
     *   1. get disposition codes
     *   2. return Document to calling module
     *
     * @param None
     * 
     * @return Document - disposition codes
     *                 
     * @throws Exception
     * @throws IOException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws SQLException
     * @throws TransformerException
     * @throws XSLException
     */        
    public Document getDispositionCodes()
         throws IOException, ParserConfigurationException, SAXException, SQLException,
                TransformerException, Exception
    {
        DispositionCodeHandler dch = null;
        Document dispCodeDoc = null;
        dch = (DispositionCodeHandler)CacheManager.getInstance().getHandler(COMConstants.DISPOSITION_CODE_HANDLER);              
        dispCodeDoc = dch.getDispCodeDoc();
      
        return dispCodeDoc;         
    }
}