package com.ftd.customerordermanagement.bo;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.dao.CommentDAO;
import com.ftd.customerordermanagement.dao.CustomerDAO;
import com.ftd.customerordermanagement.dao.LockDAO;
import com.ftd.customerordermanagement.vo.CommentsVO;
import com.ftd.customerordermanagement.vo.EmailVO;

import com.ftd.newsletterevents.bo.ejb.NewsletterPubEJB;
import com.ftd.newsletterevents.bo.ejb.NewsletterPubEJBHome;
import com.ftd.newsletterevents.vo.INewsletterEvent;
import com.ftd.newsletterevents.vo.StatusType;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.XMLFormat;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;

import com.ftd.security.SecurityManager;
import com.ftd.security.cache.vo.UserInfo;

import java.math.BigDecimal;
import java.util.StringTokenizer;


import java.rmi.RemoteException;

import java.sql.Connection;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import javax.naming.Context;
import javax.naming.InitialContext;

import javax.rmi.PortableRemoteObject;

import javax.servlet.http.HttpServletRequest;

import org.w3c.dom.Document;


public class UpdateCustomerEmailBO {
    private static Logger logger = new Logger(
            "com.ftd.customerordermanagement.bo.UpdateCustomerEmailBO");
    private HttpServletRequest request = null;
    private Connection con = null;
    private String action = null;
    private String csrId = null;
    private long currentUCEPagePosition = 0;
    private String customerId = null;
    private String emailAddress = null;
    private HashMap hashChangedEmailVO = new HashMap();
    private HashMap hashOrigEmailVO = new HashMap();
    private HashMap hashNewEmailVO = new HashMap();
    private HashMap hashXML = new HashMap();
    private boolean invokedByThisSessionId = true;
    private long lCustomerId;
    private long maxCustomerEmailAddresses = 50;
    private long newPagePosition = 0;
    private String pageAction = null;
    private HashMap pageData = new HashMap();
    private long recordsToBeReturned = 0;
    private StringBuffer sbCustomerComments = null;
    private String sessionId = null;
    private long startSearchingFrom = 0;
    private long totalUCEPages = 0;
    private int uceCompanyIdCount = 0;
    private HashMap uceCompanyNameHash = new HashMap();
    private HashMap uceSubscribeStatusHash = new HashMap();
    private HashMap uceEmailIdHash = new HashMap();

    /*******************************************************************************************
     * Constructor 1
     *******************************************************************************************/
    public UpdateCustomerEmailBO() {
    }

    /*******************************************************************************************
     * Constructor 2
     *******************************************************************************************/
    public UpdateCustomerEmailBO(HttpServletRequest request, Connection con) {
        this.request = request;
        this.con = con;
    }

    /*******************************************************************************************
     * Constructor 3
     *******************************************************************************************/
    public UpdateCustomerEmailBO(Connection con) {
        this.con = con;
    }

    /*******************************************************************************************
      * processRequest(action)
      ******************************************************************************************
      * Besides the constructor, this is the only public method in this class.
      * This method will accept a string called action and after interogating the value, will
      * process accordingly.  It will generate the search criteria, the page details, and the
      * valid data to be passed back to the calling class in a HashMap.
      * Note that the search criteria and page details are returned back as HashMap objects,
      * while all other data is returned as XML objects.
      *
      * @param  String - action
      * @return HashMap - contains 0 - many XML documents of data, 1 HashMap each for page
      *                   details and search criteria
      * @throws
      */
    public HashMap processRequest(String action) throws Exception {
        boolean firstInvokation = false;

        //HashMap that will be returned to the calling class
        HashMap returnHash = new HashMap();

        //Get info from the request object
        getRequestInfo(this.request);

        //Get config file info
        getConfigInfo();

        //Initialize class level variable 'action'
        this.action = action;

        //process the action
        processAction();

        //populate the remainder of the fields on the page data
        populatePageData();

        //At this point, we should have three HashMaps.
        // HashMap1 = hashXML         - hashmap that contains zero to many Documents
        // HashMap2 = pageData        - hashmap that contains String data at page level
        // HashMap3 = searchCriteria  - hashmap that contains the search criteria that we
        //                              received in the request
        //
        //Combine all of the above HashMaps 3 into one HashMap, called returnHash
        //retrieve all the Documents from hashXML and put them in returnHash
        //retrieve the keyset
        Set ks1 = hashXML.keySet();
        Iterator iter = ks1.iterator();
        String key = null;
        Document doc = null;

        //Iterate thru the keyset
        while (iter.hasNext()) {
            //get the key
            key = iter.next().toString();

            //retrieve the XML document
            doc = (Document) hashXML.get(key);

            //put the XML object in the returnHash
            returnHash.put(key, (Document) doc);
        }

        //append pageData to returnHash
        returnHash.put("pageData", (HashMap) this.pageData);

        return returnHash;
    }

    /*******************************************************************************************
      * getRequestInfo(HttpServletRequest request)
      ******************************************************************************************
      * Retrieve the info from the request object
      *
      * @param  HttpServletRequest - request
      * @return none
      * @throws none
      */
    private void getRequestInfo(HttpServletRequest request)
        throws Exception {
        /****************************************************************************************
         *  Retrieve Page Parameters
         ***************************************************************************************/

        //retrieve the parameter that shows the page name which triggered this request.  It could
        //either be the customer account page or the customer shopping cart page.
        if (request.getParameter(COMConstants.PAGE_ACTION) != null) {
            this.pageAction = request.getParameter(COMConstants.PAGE_ACTION);
        }

        //retrieve the current page position
        if (request.getParameter(COMConstants.UCE_CURRENT_PAGE) != null) {
            if (!request.getParameter(COMConstants.UCE_CURRENT_PAGE).toString()
                            .equalsIgnoreCase("")) {
                this.currentUCEPagePosition = Long.valueOf(request.getParameter(
                            COMConstants.UCE_CURRENT_PAGE)).longValue();
            }
        }

        //retrieve the total number of pages that was initially sent to the page
        if (request.getParameter(COMConstants.PD_UCE_TOTAL_PAGES) != null) {
            if (!request.getParameter(COMConstants.PD_UCE_TOTAL_PAGES).toString()
                            .equalsIgnoreCase("")) {
                this.totalUCEPages = Long.valueOf(request.getParameter(
                            COMConstants.PD_UCE_TOTAL_PAGES)).longValue();
            }
        }

        /****************************************************************************************
         *  Retrieve search specific parameters
         ***************************************************************************************/

        //retrieve the customer id
        if (request.getParameter(COMConstants.UC_CUSTOMER_ID) != null) {
            this.customerId = request.getParameter(COMConstants.UC_CUSTOMER_ID);
            this.lCustomerId = Long.valueOf(this.customerId).longValue();
        }

        //retrieve the email address
        if (request.getParameter(COMConstants.UCE_EMAIL_ADDRESS) != null) {
            this.emailAddress = request.getParameter(COMConstants.UCE_EMAIL_ADDRESS);
        }

        /** retrieve the direct mail status info **/

        //retrieve the count
        if (request.getParameter(COMConstants.UCE_COMPANY_ID_COUNT) != null) {
            if (!request.getParameter(COMConstants.UCE_COMPANY_ID_COUNT)
                            .toString().equalsIgnoreCase("")) {
                this.uceCompanyIdCount = Integer.valueOf(request.getParameter(
                            COMConstants.UCE_COMPANY_ID_COUNT)).intValue();
            }
        }

        String status;
        String companyId;
        String companyName;
        String emailId;

        //fill the data in the array list created above
        for (int x = 1; x <= this.uceCompanyIdCount; x++) {
            status = null;
            companyId = null;
            companyName = null;
            emailId = null;

            //retrieve the direct mail statuse
            if (request.getParameter(COMConstants.UCE_SUBSCRIBE_STATUS + "_" +
                        x) != null) {
                if (!request.getParameter(COMConstants.UCE_SUBSCRIBE_STATUS +
                            "_" + x).equalsIgnoreCase("")) {
                    status = request.getParameter(COMConstants.UCE_SUBSCRIBE_STATUS +
                            "_" + x);
                }
            }

            //retrieve the email company id
            if (request.getParameter(COMConstants.UCE_COMPANY_ID + "_" + x) != null) {
                if (!request.getParameter(COMConstants.UCE_COMPANY_ID + "_" +
                            x).equalsIgnoreCase("")) {
                    companyId = request.getParameter(COMConstants.UCE_COMPANY_ID +
                            "_" + x);
                }
            }

            //retrieve the email company name
            if (request.getParameter(COMConstants.UCE_COMPANY_NAME + "_" + x) != null) {
                if (!request.getParameter(COMConstants.UCE_COMPANY_NAME + "_" +
                            x).equalsIgnoreCase("")) {
                    companyName = request.getParameter(COMConstants.UCE_COMPANY_NAME +
                            "_" + x);
                }
            }

            //retrieve the email id
            if (request.getParameter(COMConstants.UCE_EMAIL_ID + "_" + x) != null) {
                if (!request.getParameter(COMConstants.UCE_EMAIL_ID + "_" + x)
                                .equalsIgnoreCase("")) {
                    emailId = request.getParameter(COMConstants.UCE_EMAIL_ID +
                            "_" + x);
                }
            }

            if (companyId != null) {
                this.uceSubscribeStatusHash.put(companyId, status);
                this.uceEmailIdHash.put(companyId, emailId);
                this.uceCompanyNameHash.put(companyId, companyName);
            }
        }

        /****************************************************************************************
         *  Retrieve Security Info
         ***************************************************************************************/
        this.sessionId = request.getParameter(COMConstants.SEC_TOKEN);
    }

    /*******************************************************************************************
      * getConfigInfo()
      ******************************************************************************************
      * Retrieve the info from the configuration file
      *
      * @param none
      * @return
      * @throws none
      */
    private void getConfigInfo() throws Exception {
        ConfigurationUtil cu = null;
        cu = ConfigurationUtil.getInstance();

        //get MAX_CUSTOMER_EMAIL_ADDRESSES
        if (cu.getProperty(COMConstants.PROPERTY_FILE,
                    COMConstants.CONS_MAX_CUSTOMER_EMAIL_ADDRESSES) != null) {
            this.maxCustomerEmailAddresses = Long.valueOf(cu.getProperty(
                        COMConstants.PROPERTY_FILE,
                        COMConstants.CONS_MAX_CUSTOMER_EMAIL_ADDRESSES))
                                                 .longValue();
        }

        //get csr Id
        String security = (String) cu.getProperty(COMConstants.PROPERTY_FILE,
                COMConstants.SECURITY_IS_ON);
        boolean securityIsOn = security.equalsIgnoreCase("true") ? true : false;

        //delete this before going to production.  the following should only check on securityIsOn
        if (securityIsOn || (this.sessionId != null)) {
            SecurityManager sm = SecurityManager.getInstance();
            UserInfo ui = sm.getUserInfo(this.sessionId);
            this.csrId = ui.getUserID();
        }
    }

    /*******************************************************************************************
      * populatePageData()
      ******************************************************************************************
      * Populate the page data with the remainder of the fields
      *
      * @param none
      * @return
      * @throws none
      */
    private void populatePageData() {
        //store the action passed in the page data
        this.pageData.put(COMConstants.PD_ACTION, this.action);

        //store the customer id passed in the page data
        this.pageData.put(COMConstants.PD_CUSTOMER_ID, this.customerId);

        //store the page action
        this.pageData.put(COMConstants.PD_PAGE_ACTION, this.pageAction);

        //store the email address
        this.pageData.put(COMConstants.PD_UCE_EMAIL_ADDRESS, this.emailAddress);
    }

    /*******************************************************************************************
      * processAction()
      ******************************************************************************************
      *
      * @throws
      */
    private void processAction() throws Exception {
        if (this.action.equalsIgnoreCase(
                    COMConstants.ACTION_MORE_CUSTOMER_EMAILS)) {
            this.currentUCEPagePosition = 0;
            processLoadPage();
        } else if ((this.action.equalsIgnoreCase(
                    COMConstants.ACTION_UCE_FIRST_PAGE)) ||
                (this.action.equalsIgnoreCase(COMConstants.ACTION_UCE_NEXT_PAGE)) ||
                (this.action.equalsIgnoreCase(
                    COMConstants.ACTION_UCE_PREVIOUS_PAGE)) ||
                (this.action.equalsIgnoreCase(COMConstants.ACTION_UCE_LAST_PAGE))) {
            processNavigation();
        } else if (this.action.equalsIgnoreCase(
                    COMConstants.ACTION_GET_EMAIL_INFO) ||
                this.action.equalsIgnoreCase(
                    COMConstants.ACTION_ADD_CUSTOMER_EMAIL)) {
            processGetAddEmailInfo();
        } else if (this.action.equalsIgnoreCase(
                    COMConstants.ACTION_CHECK_CUSTOMER_EMAIL)) {
            processCheckCustomerEmail();
        } else if (this.action.equalsIgnoreCase(
                    COMConstants.ACTION_SAVE_CUSTOMER_EMAILS)) {
            processSaveCustomerEmails();
        }
        //    else if (this.action.equalsIgnoreCase("remote"))
        //    {
        //      this.remoteUpdateEmailStatus(new NewsletterEventVO("lukeEmailTest@ftdi.com", "FTD", "Subscribe", "SYS", "Y", new Long("0")));
        //    }
        else {
            //set the XSL parameter, which governs the page to be loaded
            this.pageData.put("XSL", COMConstants.XSL_VIEW_ALL_EMAILS);
            this.pageData.put(COMConstants.PD_MESSAGE_DISPLAY,
                COMConstants.MSG_INVALID_ACTION);
        }
    }

    /*******************************************************************************************
      * processLoadPage()
      ******************************************************************************************
      * process the action type of "load".  This method implements the initial page load-up.
      *
      * @param  none
      * @return none
      * @throws none
      */
    private void processLoadPage() throws Exception {
        //set the begin and max allowed parameters, to be passed to the DAOs.
        setBeginEndParameters(this.currentUCEPagePosition,
            this.maxCustomerEmailAddresses);

        //call retrieveEmails which will call the DAO to retrieve the results
        HashMap customerEmailResults = retrieveEmails(this.startSearchingFrom,
                this.recordsToBeReturned);

        this.newPagePosition = this.currentUCEPagePosition + 1;

        //get the value of OUT_ID_COUNT
        long outIdCount = 0;

        if (customerEmailResults.get("OUT_ID_COUNT") != null) {
            outIdCount = Long.valueOf(customerEmailResults.get("OUT_ID_COUNT")
                                                          .toString())
                             .longValue();
        }

        //Set the navigation buttons
        setButtons(outIdCount, this.newPagePosition,
            this.maxCustomerEmailAddresses, "uce");
        this.pageData.put(COMConstants.PD_UCE_START_POSITION,
            String.valueOf(startSearchingFrom));

        //set the XSL parameter, which governs the page to be loaded
        this.pageData.put("XSL", COMConstants.XSL_VIEW_ALL_EMAILS);
    }

    /*******************************************************************************************
      * processNavigation()
      ******************************************************************************************
      * This method will process the action type of "uce_first", "uce_previous", "uce_next" and
      * and "uce_last".  Essentially, this method provides navigational ability for when
      * multiple customer email accounts are found
      *
      * @param  none
      * @return none
      * @throws none
      */
    private void processNavigation() throws Exception {
        //set the new page position
        if (this.action.equalsIgnoreCase(COMConstants.ACTION_UCE_FIRST_PAGE)) {
            this.newPagePosition = 1;
        } else if (this.action.equalsIgnoreCase(
                    COMConstants.ACTION_UCE_PREVIOUS_PAGE)) {
            this.newPagePosition = this.currentUCEPagePosition - 1;
        } else if (this.action.equalsIgnoreCase(
                    COMConstants.ACTION_UCE_NEXT_PAGE)) {
            this.newPagePosition = this.currentUCEPagePosition + 1;
        } else if (this.action.equalsIgnoreCase(
                    COMConstants.ACTION_UCE_LAST_PAGE)) {
            this.newPagePosition = this.totalUCEPages;
        }

        //set the begin and max allowed parameters, to be passed to the DAOs.
        setBeginEndParameters(this.newPagePosition,
            this.maxCustomerEmailAddresses);

        //call retrieveEmails which will call the DAO to retrieve the results
        HashMap customerEmailResults = retrieveEmails(this.startSearchingFrom,
                this.recordsToBeReturned);

        //get the value of OUT_ID_COUNT
        long outIdCount = 0;

        if (customerEmailResults.get("OUT_ID_COUNT") != null) {
            outIdCount = Long.valueOf(customerEmailResults.get("OUT_ID_COUNT")
                                                          .toString())
                             .longValue();
        }

        //Set the navigation buttons
        setButtons(outIdCount, this.newPagePosition,
            this.maxCustomerEmailAddresses, "uce");
        this.pageData.put(COMConstants.PD_UCE_START_POSITION,
            String.valueOf(startSearchingFrom));

        //set the XSL parameter, which governs the page to be loaded
        this.pageData.put("XSL", COMConstants.XSL_VIEW_ALL_EMAILS);
    }

    /*******************************************************************************************
      * processGetAddEmailInfo()
      ******************************************************************************************
      * process the action type of "load".  This method implements the initial page load-up.
      *
      * @param  none
      * @return none
      * @throws none
      */
    private void processGetAddEmailInfo() throws Exception {
        boolean buildXML = true;
        boolean buildVO = false;

        //call retrieveEmailInfo which will call the DAO to retrieve the results
        retrieveEmailInfo(buildXML, buildVO);

        //set the XSL parameter, which governs the page to be loaded
        this.pageData.put("XSL", COMConstants.XSL_UPDATE_CUSTOMER_EMAIL_STATUSES);
    }

    /*******************************************************************************************
      * processCheckCustomerEmail()
      ******************************************************************************************
      * This method will process the action type of
      *
      * @param  none
      * @return none
      * @throws none
      *
      */
    private void processCheckCustomerEmail() throws Exception {
        boolean lockStillValid = true;
        boolean updateable = true;

        //check if the lock is still valid
        lockStillValid = processRetrieveLock();

        //if the lock is still in place, process
        if (lockStillValid) {
            //for add, check if the email address already exists for a customer
            if (pageAction.equalsIgnoreCase(COMConstants.ADD)) {
                updateable = checkExistingEmailAddressByCustomerId();

                if (!updateable) {
                    this.pageData.put(COMConstants.PD_MESSAGE_DISPLAY,
                        COMConstants.MSG_EMAIL_ALREADY_EXIST);
                }
            } else {
                processExistingEmailInfo();

                processNewEmailInfo();

                boolean infoChanged;
                boolean recordChanges = false;
                infoChanged = processCheckForChanges(recordChanges);

                //if nothing changed, display an error message
                if (!infoChanged) {
                    updateable = false;
                    this.pageData.put(COMConstants.PD_MESSAGE_DISPLAY,
                        COMConstants.MSG_NOTHING_CHANGED);
                }
            }
        }
        //else display an error message
        else {
            updateable = false;
        }

        this.pageData.put("updateable", updateable ? "Y" : "N");
        this.pageData.put("XSL", COMConstants.XSL_PROCESS_CUSTOMER_EMAIL_IFRAME);
    }

    /*******************************************************************************************
      * processSaveCustomerEmails()
      ******************************************************************************************
      * process the action type of "load".  This method implements the initial page load-up.
      *
      * @param  none
      * @return none
      * @throws none
      */
    private void processSaveCustomerEmails() throws Exception {
        boolean lockStillValid = true;
        boolean updated = true;

        //check if the lock is still valid
        lockStillValid = processRetrieveLock();

        //if the lock is still in place, process
        if (lockStillValid) {
            processExistingEmailInfo();

            processNewEmailInfo();

            //NOTE that this boolean will not come to use in this method.  Since processCheckForChanges
            //is used in more than one place, this indicator provides means to determine the next
            //step.
            //At this point (action=save), we have already determined (from action=check) that there
            //were some changes that the CSR made
            boolean infoChanged;

            boolean recordChanges = true;
            infoChanged = processCheckForChanges(recordChanges);

            //update the customer email record
            updated = processUpdateCustomerEmail();

            if (updated) {
                processCustomerComments();
            } else {
                this.pageData.put(COMConstants.PD_MESSAGE_DISPLAY,
                    COMConstants.MSG_UPDATE_UNSUCCESSFUL);
            }
        }
        //else display an error message
        else {
            updated = false;
        }

        this.pageData.put("XSL", COMConstants.XSL_PROCESS_CUSTOMER_EMAIL_IFRAME);
        this.pageData.put("updated", updated ? "Y" : "N");
    }

    /*******************************************************************************************
      * processAddCustomerEmails()
      ******************************************************************************************
      * process the action type of "load".  This method implements the initial page load-up.
      *
      * @param  none
      * @return none
      * @throws none
      *
      private void processAddCustomerEmails() throws Exception
      {
        boolean updateable     = true;
        boolean lockStillValid = true;

        //check if the lock is still valid
        lockStillValid = processRetrieveLock();

        //if the lock is still in place, process
        if(lockStillValid)
        {
          this.pageData.put("XSL",      COMConstants.XSL_UPDATE_CUSTOMER_EMAIL_STATUSES);
        }
        else
        {
          updateable = false;
        }

        this.pageData.put("updateable",                           updateable?"Y":"N");
        this.pageData.put("XSL",                                  COMConstants.XSL_PROCESS_CUSTOMER_EMAIL);




      }*/

    //******************************************************************************************
    //                                      END OF MAIN PROCESSES
    //******************************************************************************************

    /*******************************************************************************************
      * processRetrieveLock()
      ******************************************************************************************
      * This method will process the action type of
      *
      * @param  none
      * @return none
      * @throws none
      *
      */
    private boolean processRetrieveLock() throws Exception {
        boolean lockSuccessful = true;

        //Call retrieveLockInfo which will call the DAO to retrieve the results
        Document lockInfo = DOMUtil.getDocument();

        lockInfo = retrieveLockInfo();

        //get the lock obtained indicator.
        String sLockedObtained = DOMUtil.getNodeValue(lockInfo,
                "OUT_LOCK_OBTAINED");

        //if locked_obtained = yes, you have the lock (regardless if you just established a lock
        //or have had it for a while).  Else, somebody else has the record locked
        if (sLockedObtained.equalsIgnoreCase(COMConstants.CONS_NO)) {
            lockSuccessful = false;

            //get the locked csr id
            String sCsrId = DOMUtil.getNodeValue(lockInfo, "OUT_LOCKED_CSR_ID");

            //set an output message
            String message = COMConstants.MSG_RECORD_C_LOCKED1 + sCsrId +
                COMConstants.MSG_RECORD_C_LOCKED2;
            this.pageData.put(COMConstants.PD_MESSAGE_DISPLAY, message);
        }

        return lockSuccessful;
    }

    /*******************************************************************************************
      * processExistingEmailInfo()
      ******************************************************************************************
      * This method will process the action type of
      *
      * @param  none
      * @return none
      * @throws none
      *
      */
    private void processExistingEmailInfo() throws Exception {
        //call retrieveEmailInfo which will call the DAO to retrieve the results
        boolean buildXML = false;
        boolean buildVO = true;
        retrieveEmailInfo(buildXML, buildVO);
    }

    /*******************************************************************************************
      * processNewEmailInfo()
      ******************************************************************************************
      * This method will process the action type of
      *
      * @param  none
      * @return none
      * @throws none
      *
      */
    private void processNewEmailInfo() throws Exception {
        //retrieve the keyset
        Set ks1 = this.uceSubscribeStatusHash.keySet();
        Iterator iter = ks1.iterator();
        String key = null;

        while (iter.hasNext()) {
            //get the key
            key = iter.next().toString();

            EmailVO eVO = new EmailVO();

            //set the customer id
            eVO.setCustomerId(this.lCustomerId);

            //set the email address
            eVO.setEmailAddress(this.emailAddress);

            //set the company id
            eVO.setCompanyId(key);

            //set the email id if one is available
            long emailId = 0;

            if (this.uceEmailIdHash.get(key) != null) {
                emailId = Long.valueOf(this.uceEmailIdHash.get(key).toString())
                              .longValue();
            }

            eVO.setEmailId(emailId);

            //set the status if one is available
            String status = null;

            if (this.uceSubscribeStatusHash.get(key) != null) {
                status = this.uceSubscribeStatusHash.get(key).toString();
            }

            eVO.setSubscribeStatus(status);

            //set the updated by
            eVO.setUpdatedBy(this.csrId);

            eVO.setSubscribeDate(null);
            eVO.setActiveIndicator(null);
            eVO.setCreatedOn(null);
            eVO.setCreatedBy(null);
            eVO.setUpdatedOn(null);

            //add the email vo to the hashmap
            this.hashNewEmailVO.put(key, eVO);
        }
    }

    /*******************************************************************************************
     * processCheckForChanges() -
     *******************************************************************************************
      *
      * @throws none
      */
    private boolean processCheckForChanges(boolean recordChanges)
        throws Exception {
        if (recordChanges) {
            this.sbCustomerComments = new StringBuffer();
        }

        boolean changesMade = false;

        //retrieve the keyset
        Set ks1 = this.hashNewEmailVO.keySet();
        Iterator iter = ks1.iterator();
        String key = null;
        EmailVO origEmailVO;
        EmailVO newEmailVO;

        //Iterate thru the keyset
        while (iter.hasNext()) {
            //get the key
            key = iter.next().toString();

            //retrieve the Original Email VO
            origEmailVO = (EmailVO) this.hashOrigEmailVO.get(key);

            //retrieve the New Email VO
            newEmailVO = (EmailVO) this.hashNewEmailVO.get(key);

            String origStatus = null;
            String newStatus = null;

            if (origEmailVO != null) {
                origStatus = origEmailVO.getSubscribeStatus();
            }

            if (newEmailVO != null) {
                newStatus = newEmailVO.getSubscribeStatus();
            }

            //check the phone number
            if (((origStatus == null) && (newStatus != null)) ||
                    ((origStatus != null) && (newStatus != null) &&
                    !origStatus.equalsIgnoreCase(newStatus))) {
                changesMade = true;

                if (recordChanges) {
                    if (this.pageAction.equalsIgnoreCase(COMConstants.ADD)) {
                        sbCustomerComments.append(newEmailVO.getEmailAddress());
                        sbCustomerComments.append(" ");
                        sbCustomerComments.append(newEmailVO.getSubscribeStatus());
                        sbCustomerComments.append(" added.<br>");
                    } else {
                        sbCustomerComments.append(
                            "Customer's email status");
                        sbCustomerComments.append(" changed from ");
                        sbCustomerComments.append(((origEmailVO != null) &&
                            (origEmailVO.getEmailAddress() != null))
                            ? origEmailVO.getEmailAddress() + " - "
                            : ("No Email Address assigned" + " - "));
                        sbCustomerComments.append(((origEmailVO != null) &&
                            (origEmailVO.getSubscribeStatus() != null))
                            ? origEmailVO.getSubscribeStatus() : "Never opted in");
                        sbCustomerComments.append(" to ");
                        sbCustomerComments.append(newEmailVO.getEmailAddress() +
                            " - ");
                        sbCustomerComments.append(newEmailVO.getSubscribeStatus());
                        sbCustomerComments.append(".<br>");
                    }
                }

                newEmailVO.setMostRecentByCompany(COMConstants.CONS_YES.toUpperCase());
                this.hashChangedEmailVO.put(key, newEmailVO);
            }
        } //end while

        return changesMade;
    }

    private static NewsletterPubEJB getNewsletterPubEjb() throws Exception {

        //Invoke the remote interface for the NewsletterPubEJB
        Context jndiContext = null;

        NewsletterPubEJBHome home = null;
        
        try
        {
          jndiContext = new InitialContext();

          home = (NewsletterPubEJBHome) PortableRemoteObject.narrow(jndiContext.lookup(
                    "NewsletterPubEJB"), NewsletterPubEJBHome.class);
        }
        finally
        {
          try
          {
            jndiContext.close();
          }
          catch (Exception e)
          {}
        }
        
        // create an EJB instance
        return home.create();


    }

    /*******************************************************************************************
     * processUpdateCustomerEmail() -
     *******************************************************************************************
      * This method calls the updateDAO and NewsletterPubEJB and passes values to it for email updates
      * to COM and Novatar.
      * @throws none
      */
    private boolean processUpdateCustomerEmail() throws Exception {
        // create an EJB instance
        NewsletterPubEJB npEjb = this.getNewsletterPubEjb();

        //flag that is used to check if the update was successful, and then to be passed back to the
        //calling method
        boolean updateSuccessful = true;

        //hashmaps to hold the output resultsets
        HashMap updateEmailHash;

        //Strings to hold the output parameters
        String outStatus;
        String outMessage;

        //get the email vos from the hashmap
        Set ks1 = this.hashChangedEmailVO.keySet();
        Iterator iter = ks1.iterator();
        String key = null;
        EmailVO eVO;

        //Iterate thru the keyset
        while (iter.hasNext() && updateSuccessful) {
            //get the key
            key = iter.next().toString();

            //retrieve the Original Email VO
            eVO = (EmailVO) this.hashChangedEmailVO.get(key);

            updateEmailHash = (HashMap) updateEmail(eVO);

            //start here - the updateEmailHash is coming up empty.  check why.
            outStatus = (String) updateEmailHash.get("OUT_STATUS");
            outMessage = (String) updateEmailHash.get("OUT_MESSAGE");

            updateSuccessful = true;

            if (outStatus != null) {
                updateSuccessful = outStatus.equalsIgnoreCase(COMConstants.CONS_YES)
                    ? true : false;
            }

            if (!updateSuccessful) {
                this.pageData.put(COMConstants.PD_MESSAGE_DISPLAY,
                    COMConstants.MSG_UPDATE_UNSUCCESSFUL);
            }

            //If the update was successful pass values to the bean and call the process method.
            if (updateSuccessful) {
                StatusType st = null;
                logger.debug("Subscribed Status = " +
                    eVO.getSubscribeStatus().substring(0, 1).toUpperCase());

                if (eVO.getSubscribeStatus().substring(0, 1).toUpperCase()
                           .equals(StatusType.SUBSCRIBED.toString())) {
                    st = StatusType.SUBSCRIBED;
                } else {
                    st = StatusType.UNSUBSCRIBED;
                }

                logger.debug("EmailAddress = " + eVO.getEmailAddress());
                logger.debug("CompanyID = " + eVO.getCompanyId());
                logger.debug("ST = " + st.toString());
                npEjb.process(eVO.getEmailAddress(), eVO.getCompanyId(), st,
                    eVO.getCreatedBy(), new Date());
                
                // Defect 892: Publish email changes on cobrand companies.
                String createdCompanyList = (String) updateEmailHash.get("OUT_COMPANY_LIST");
                if (createdCompanyList != null && eVO.getSubscribeStatus().equalsIgnoreCase("Subscribe")) {
                    publishSubscribeAllCompanies(eVO, createdCompanyList);
                }
            }
        }

        return updateSuccessful;
    }

    /*******************************************************************************************
     * processCustomerComments() -
     *******************************************************************************************
      *
      * @throws none
      */
    private void processCustomerComments() throws Exception {
        String customerComments = this.sbCustomerComments.toString();

        //instantiate the customer dao
        CommentDAO commentDAO = new CommentDAO(this.con);

        //instantiate the comments vo
        CommentsVO commentsVO = new CommentsVO();

        //initialize the comments vo
        commentsVO.setCustomerId(this.customerId);
        commentsVO.setComment(customerComments);
        commentsVO.setCommentType(COMConstants.CONS_ENTITY_TYPE_CUSTOMER_TITLE_CASE);
        commentsVO.setCreatedBy(this.csrId);

        commentsVO.setOrderGuid(null);
        commentsVO.setOrderDetailId(null);
        commentsVO.setCommentOrigin(null);
        commentsVO.setReason(null);
        commentsVO.setDnisId(null);
        commentsVO.setCommentId(null);
        commentsVO.setCreatedOn(null);
        commentsVO.setUpdatedBy(this.csrId);
        commentsVO.setUpdatedOn(null);

        commentDAO.insertComment(commentsVO);
    }

    /*******************************************************************************************
     * retrieveEmails()
     *******************************************************************************************
      * This method is used to call the CustomerDAO.searchCustomerEmails
      *
      * @param1 long - start position
      * @param2 long - end position
      * @return HashMap - map containing output from the stored procedure
      * @throws none
      */
    private HashMap retrieveEmails(long startSearchingFrom,
        long recordsToBeReturned) throws Exception {
        //Instantiate CustomerDAO
        CustomerDAO customerDAO = new CustomerDAO(this.con);

        //hashmap that will contain the output from the stored proce
        HashMap customerEmailResults = new HashMap();

        //Call searchCustomerEmails method in the DAO
        customerEmailResults = (HashMap) customerDAO.searchCustomerEmailsUnique(this.customerId,
                startSearchingFrom, recordsToBeReturned);

        //Create an Document and call a method that will transform the CachedResultSet from the
        //store proce into an Document
        Document customerEmailsXML = buildXML(customerEmailResults,
                "OUT_CUR", "UCE_CUSTOMER_EMAILS", "UCE_CUSTOMER_EMAIL");

        //and store the XML object in a hashXML hashmap
        this.hashXML.put(COMConstants.HK_UCE_CUSTOMER_EMAILS_XML,
            customerEmailsXML);

        //Store the count in the pageData hashmap
        String outIdCount = "0";

        if (customerEmailResults.get("OUT_ID_COUNT") != null) {
            outIdCount = customerEmailResults.get("OUT_ID_COUNT").toString();
        }

        this.pageData.put(COMConstants.PD_UCE_COUNT, outIdCount);

        return customerEmailResults;
    }

    /*******************************************************************************************
     * retrieveEmailInfo()
     *******************************************************************************************
      * This method is used to call the CustomerDAO.searchCustomerEmails
      *
      * @param1 long - start position
      * @param2 long - end position
      * @return HashMap - map containing output from the stored procedure
      * @throws none
      */
    private void retrieveEmailInfo(boolean buildXML, boolean buildVO)
        throws Exception {
        //Instantiate CustomerDAO
        CustomerDAO customerDAO = new CustomerDAO(this.con);

        //HashMap that will contain the output from the stored proce
        HashMap customerEmailResults = new HashMap();
        CachedResultSet rs;

        //Call searchCustomerEmails method in the DAO
        rs = (CachedResultSet) customerDAO.searchCustomerEmailInfo(this.customerId,
                this.emailAddress);

        if (buildXML) {
            customerEmailResults.put("OUT_CUR", rs);

            //Create an Document and call a method that will transform the CachedResultSet from the cursor:
            //OUT_PHONE_CUR into an Document.  Store this XML object in hashXML HashMap
            Document customerEmailXML = buildXML(customerEmailResults,
                    "OUT_CUR", "EMAILS_INFO", "EMAIL_INFO");

            //and store the XML object in a hashXML hashmap
            this.hashXML.put(COMConstants.HK_UCE_CUSTOMER_EMAILS_XML,
                customerEmailXML);
        }

        if (buildVO) {
            while (rs.next()) {
                /**Create the Email VO**/
                EmailVO eVO = new EmailVO();

                eVO.setCustomerId(this.lCustomerId);
                eVO.setEmailId((rs.getObject("email_id") != null)
                    ? Long.valueOf(rs.getObject("email_id").toString())
                          .longValue() : 0);

                //set the company id
                String companyId = null;

                if (rs.getObject("company_id") != null) {
                    companyId = (String) rs.getObject("company_id");
                }

                eVO.setCompanyId(companyId);

                //set the email address
                eVO.setEmailAddress((rs.getObject("email_address") != null)
                    ? (String) rs.getObject("email_address") : null);

                //set the email subscribe status
                eVO.setSubscribeStatus((rs.getObject("subscribe_status") != null)
                    ? (String) rs.getObject("subscribe_status") : null);

                eVO.setSubscribeDate(null); //not getting returned from the stored proc
                eVO.setActiveIndicator(null);
                eVO.setCreatedOn(null); //users cannot update
                eVO.setCreatedBy(null); //users cannot update
                eVO.setUpdatedOn(null); //users cannot update
                eVO.setUpdatedBy(null); //users cannot update

                //add the email vo to the list
                if (eVO.getEmailId() > 0) {
                    hashOrigEmailVO.put(companyId, eVO);
                }
            } //end-while
        } //end-if
    }

    /*******************************************************************************************
     * retrieveLockInfo() - check if the lock on the Payment record is still valid
     *******************************************************************************************
      *
      * @throws none
      */
    private Document retrieveLockInfo() throws Exception {
        //Instantiate RefundDAO
        LockDAO lockDAO = new LockDAO(this.con);

        //hashmap that will contain the output from the stored proc
        Document lockInfo = DOMUtil.getDocument();

        //Call retrieveLockXML method in the DAO
        //lockInfo = lockDAO.retrieveLockXML(sessionId,       csrId,        entityId [aka lockId],  entityType [aka applicationName]);
        lockInfo = lockDAO.retrieveLockXML(this.sessionId, this.csrId,
                this.customerId, COMConstants.CONS_ENTITY_TYPE_CUSTOMER);

        return lockInfo;
    }

    //2

    /*******************************************************************************************
      * checkExistingEmailAddressByCustomerId()
      ******************************************************************************************
      * This method will process the action type of
      *
      * @param  none
      * @return none
      * @throws none
      *
      */
    private boolean checkExistingEmailAddressByCustomerId()
        throws Exception {
        //Instantiate CustomerDAO
        CustomerDAO customerDAO = new CustomerDAO(this.con);

        String emailId = null;
        boolean emailCanBeAdded = true;

        //HashMap that will contain the output from the stored proce
        HashMap customerEmailResults = new HashMap();
        CachedResultSet rs;

        //Call searchCustomerEmails method in the DAO
        rs = (CachedResultSet) customerDAO.searchCustomerEmailInfo(this.customerId,
                this.emailAddress);

        while (rs.next()) {
            if (rs.getObject("email_id") != null) {
                emailId = rs.getObject("email_id").toString();

                break;
            }
        }

        if ((emailId != null) && !emailId.equalsIgnoreCase("")) {
            emailCanBeAdded = false;
        }

        return emailCanBeAdded;
    }

    /*******************************************************************************************
     * updateEmail()
     *******************************************************************************************
      * This method is used to call the CustomerDAO.getStates
      *
      * @throws none
      */
    private HashMap updateEmail(EmailVO eVO) throws Exception {
        HashMap updateEmailHash = new HashMap();

        CustomerDAO customerDAO = new CustomerDAO(this.con);
        updateEmailHash = (HashMap) customerDAO.updateEmail(eVO);

        return updateEmailHash;
    }

    /*******************************************************************************************
     * remoteUpdateEmailStatus()
     *******************************************************************************************
      * This method is used to call the CustomerDAO.updateEmail with information
      * coming from Novator.
      *
      * @throws Exception
      */
    public void remoteUpdateEmailStatus(INewsletterEvent neVO)
        throws Exception, RemoteException {
        //Strings to hold the output parameters
        String outStatus;
        String outMessage;

        //flag that is used to check if the update was successful
        boolean updateSuccessful = true;
        long emailID = 0;
        HashMap updateEmailHash = new HashMap();
        CustomerDAO customerDAO = new CustomerDAO(this.con);

        logger.debug("companyID = " + neVO.getCompanyId());
        logger.debug("email  = " + neVO.getEmailAddress());
        logger.debug("ActiveIndicator = " + "Y");
        logger.debug("subscribed Status = " + neVO.getStatus());
        logger.debug("MostRecentByCompany = " +
            COMConstants.CONS_YES.toUpperCase());
        logger.debug("email id = " + emailID);
        logger.debug("UpdatedBy = " + neVO.getOperatorId());

        EmailVO eVO = new EmailVO();

        eVO.setCompanyId(neVO.getCompanyId());
        eVO.setEmailAddress(neVO.getEmailAddress());
        eVO.setActiveIndicator("Y");

        if (neVO.getStatus().equalsIgnoreCase(neVO.STATUS_SUBSCRIBED)) {
            eVO.setSubscribeStatus("Subscribe");
        } else {
            eVO.setSubscribeStatus("Unsubscribe");
        }

        String operatorId = (neVO.getOperatorId() == null) ? "SYS"
                                                           : neVO.getOperatorId();

        if (neVO.getOriginAppName().toUpperCase().indexOf("NOVATOR") != -1) {
            operatorId = "FOX";
        }

        eVO.setUpdatedBy(operatorId);
        eVO.setMostRecentByCompany(COMConstants.CONS_YES.toUpperCase());
        eVO.setEmailId(emailID);

        updateEmailHash = (HashMap) customerDAO.updateEmail(eVO);
        logger.debug("I got back to BO");
        logger.debug("updateEmailHash = " + updateEmailHash.toString());
        outStatus = (String) updateEmailHash.get("OUT_STATUS");
        outMessage = (String) updateEmailHash.get("OUT_MESSAGE");

        logger.debug("outStatus = " + outStatus);
        logger.debug("outMessage = " + outMessage);

        if (outStatus != null) {
            updateSuccessful = outStatus.equalsIgnoreCase(COMConstants.CONS_YES)
                ? true : false;
        }

        if (!updateSuccessful) {
            throw new RemoteException(COMConstants.MSG_UPDATE_UNSUCCESSFUL);
        }
        
        // Defect 892: Publish email changes on cobrand companies.
        String createdCompanyList = (String) updateEmailHash.get("OUT_COMPANY_LIST");
        if (createdCompanyList != null && eVO.getSubscribeStatus().equalsIgnoreCase("Subscribe")) {
             publishSubscribeAllCompanies(eVO, createdCompanyList);
        }
    }
    
    private static void publishSubscribeAllCompanies(EmailVO eVO, String createdCompanyList) throws Exception
    {
            logger.debug(
                "remoteUpdateEmailStatus: Subscribing to all companies customer " +
                eVO.getCustomerId() + " email " + eVO.getEmailAddress());

            if (createdCompanyList != null) {
            // For each company, publish a subscribe message.
            StringTokenizer st = new StringTokenizer(createdCompanyList, ",");
            NewsletterPubEJB npEjb = getNewsletterPubEjb();
            Date now = new Date();
            while (st.hasMoreTokens()) {
                
                String token = st.nextToken();
                if (token != null && token.length() > 0) {
                npEjb.process(eVO.getEmailAddress(), token,
                    StatusType.SUBSCRIBED, eVO.getUpdatedBy(), 
                    eVO.getUpdatedOn() == null ? now : eVO.getUpdatedOn().getTime());
                }
            }
            }
    }

    /*******************************************************************************************
     * setButtons()
     *******************************************************************************************
      * This method will utilize the inputs to determine which buttons should be enabled.
      *
      * @param1 long - total number of records
      * @param2 long - page position
      * @param3 long - maximumn number of records for that page
      * @return none
      * @throws none
      */
    private void setButtons(long outIdCount, long newPagePosition,
        long maxRecordsPerPage, String appendString) {
        //evaluate and set current_pages
        this.pageData.put(appendString + '_' + COMConstants.PD_CURRENT_PAGE,
            String.valueOf(newPagePosition));

        //evaluate and set total_pages
        long totalPages = (long) (outIdCount / maxRecordsPerPage) +
            (long) ((((long) (outIdCount % maxRecordsPerPage)) > 0) ? 1 : 0);
        this.pageData.put(appendString + '_' + COMConstants.PD_TOTAL_PAGES,
            String.valueOf(totalPages));

        //evaluate and set show_first and show_prev
        if ((outIdCount > maxRecordsPerPage) && (newPagePosition > 1)) {
            this.pageData.put(appendString + '_' + COMConstants.PD_SHOW_FIRST,
                COMConstants.CONS_YES);
            this.pageData.put(appendString + '_' +
                COMConstants.PD_SHOW_PREVIOUS, COMConstants.CONS_YES);
        } else {
            this.pageData.put(appendString + '_' + COMConstants.PD_SHOW_FIRST,
                COMConstants.CONS_NO);
            this.pageData.put(appendString + '_' +
                COMConstants.PD_SHOW_PREVIOUS, COMConstants.CONS_NO);
        }

        //evaluate and set show_next and show_last
        if ((outIdCount > maxRecordsPerPage) && (newPagePosition < totalPages)) {
            this.pageData.put(appendString + '_' + COMConstants.PD_SHOW_NEXT,
                COMConstants.CONS_YES);
            this.pageData.put(appendString + '_' + COMConstants.PD_SHOW_LAST,
                COMConstants.CONS_YES);
        } else {
            this.pageData.put(appendString + '_' + COMConstants.PD_SHOW_NEXT,
                COMConstants.CONS_NO);
            this.pageData.put(appendString + '_' + COMConstants.PD_SHOW_LAST,
                COMConstants.CONS_NO);
        }
    }

    /*******************************************************************************************
      * setBeginEndParameters();
      ******************************************************************************************
      * This method will utilize the inputs to compute and set class level variables.   These
      * variables will be used later as start and end position, to be send to the stored procs.
      *
      * @param1 long - current page position
      * @param2 long - # of records to display
      * @return none
      * @throws none
      */
    private void setBeginEndParameters(long currentPagePosition,
        long maxRecordsToBeDisplay) {
        if (currentPagePosition == 0) {
            this.startSearchingFrom = 1;

            //this.recordsToBeReturned = this.startSearchingFrom * numberOfRecordsToDisplay;
            this.recordsToBeReturned = maxRecordsToBeDisplay;
        } else {
            this.startSearchingFrom = (--currentPagePosition * maxRecordsToBeDisplay) +
                1;

            //this.recordsToBeReturned = (currentcurrentPagePosition + 1) * maxRecordsToBeDisplay;
            this.recordsToBeReturned = maxRecordsToBeDisplay;
        }
    }

    /*******************************************************************************************
     * buildXML()
     *******************************************************************************************/
    private Document buildXML(HashMap outMap, String cursorName,
        String topName, String bottomName) throws Exception {
        //Create a CachedResultSet object using the cursor name that was passed.
        CachedResultSet rs = (CachedResultSet) outMap.get(cursorName);

        Document doc = null;

        //Create an XMLFormat, and initialize the top and bottom node.  Note that since this method
        //can be/is called by various other methods, the top and botton names MUST be passed to it.
        XMLFormat xmlFormat = new XMLFormat();
        xmlFormat.setAttribute("type", "element");
        xmlFormat.setAttribute("top", topName);
        xmlFormat.setAttribute("bottom", bottomName);

        //call the DOMUtil's converToXMLDOM method
        doc = (Document) DOMUtil.convertToXMLDOM(rs, xmlFormat);

        //return the xml document.
        return doc;
    }
}
