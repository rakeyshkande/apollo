/**
 * 
 */
package com.ftd.customerordermanagement.bo;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.math.BigDecimal;
import java.sql.Connection;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;
import org.xml.sax.SAXException;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.dao.OrderDAO;
import com.ftd.customerordermanagement.dao.RefundDAO;
import com.ftd.customerordermanagement.dao.ServicesProgramDAO;
import com.ftd.customerordermanagement.vo.OrderBillVO;
import com.ftd.customerordermanagement.vo.OrderDetailVO;
import com.ftd.customerordermanagement.vo.PaymentVO;
import com.ftd.customerordermanagement.vo.RefundTotalVO;
import com.ftd.customerordermanagement.vo.RefundVO;
import com.ftd.ftdutilities.FTDCommonUtils;
import com.ftd.op.common.constants.OrderProcessingConstants;
import com.ftd.osp.account.bo.AccountBO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.order.RecalculateOrderBO;
import com.ftd.osp.utilities.order.vo.TaxVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.security.SecurityManager;

/**
 * @author cjohnson
 *
 */
public class RefundAPIBO {
	
	private static Logger logger = new Logger(RefundAPIBO.class.getName());
	
	private RefundDAO refundDAO;
	private RecalculateOrderBO recalcOrderBO;
	private OrderDAO oDAO;
	private AccountBO accountBO;
    private ServicesProgramDAO servicesProgramDAO;
	
    /**
     * This is a wrapper method (around postFullRefundMO) which is intended to be called
     * as part of the API library for COM to issue a refund.  For example, the 
     * email-request-processing module utilizes this API to do full refunds when 
     * a customer cancels an order from the customer service website.  
     *
     * Note that we use postFullRefundMO since it takes into account any existing
     * partial refunds (unlike postFullRefund).  Also note that user context/security information 
     * is not relevant from a remote context so this method should NOT be called 
     * by anything coming from the COM front-end (use postFullRefundMO or postFullRefund instead).
     */
    public List<RefundVO> postFullRemainingRefund(String orderDetailId, String refundDisposition, 
                                         String responsibleParty, String refundStatus, String userId, Connection conn) throws Exception
    {
		    ArrayList orderDetails = new ArrayList();
				orderDetails.add(orderDetailId);
        return postFullRefundMO(orderDetails, refundDisposition, responsibleParty, refundStatus, null, null, userId, conn);
    }
	
    /**
     * 
     * @param orderDetailIds
     * @param refundDisposition
     * @param responsibleParty
     * @param refundStatus
     * @param securityToken
     * @param context
     * @param userId
     * @param conn
     * @throws Exception
     */
	protected List<RefundVO> postFullRefundMO(List orderDetailIds, String refundDisposition, String responsibleParty, String refundStatus,
			String securityToken, String context, String userId, Connection conn) throws Exception
    {
        //if not ids are passed in exit
        if(orderDetailIds == null)
        {
            return null;
        }
        List<RefundVO> refundsIssuedList = new ArrayList<RefundVO>();
        HashMap primaryPaymentMap = null;
        if (this.refundDAO == null) {
        	this.refundDAO = new RefundDAO(conn);    
        }
        
        OrderDAO oDAO = getOrderDAO(conn);
        Iterator iter = orderDetailIds.iterator();
        OrderDetailVO odVO = null;
        String recipientCountry; 
        String recipientState; 
        String companyId;
        
        String orderDetailId = null;
        while(iter.hasNext())
        {
            int paymentSequence = 1;
            boolean giftCertExists = false;
            primaryPaymentMap = new HashMap();
            orderDetailId = (String)iter.next();
            
            HashMap paymentRefundMap = this.refundDAO.loadPaymentRefund(orderDetailId);            
            recipientCountry = (String)paymentRefundMap.get("recipient_country"); 
            recipientState = (String)paymentRefundMap.get("recipient_state"); 
            companyId = (String)paymentRefundMap.get("company_id"); 

            Map billPaymentMap = (Map) paymentRefundMap.get(RefundDAO.PAYMENT_LIST);
        
            boolean isNewPGRoute = false;
            isNewPGRoute = refundDAO.isNewPaymentRoute(orderDetailId);
            logger.info("isNewPGRoute flag in postFullRefundMO "+isNewPGRoute);
            
            Iterator billPaymentIterator = billPaymentMap.keySet().iterator();
            while(billPaymentIterator.hasNext())
            {
                String billKey = (String) billPaymentIterator.next();
                List paymentList = (List) billPaymentMap.get(billKey);
                
                // Create a bill to maintain remaining balances
                OrderBillVO remainingBill = new OrderBillVO();
                // QC-10474 - Calculate the remaining bill using PaymentList
                calculateRemainingBills(paymentList, remainingBill); 
            
				List refundList = (List) paymentRefundMap.get(RefundDAO.REFUND_LIST);
				
				// Consolidate all refunds
				HashMap refundMap = null;
				if(isNewPGRoute)
				refundMap = consolidateMORefunds(refundList, securityToken, context, null, conn);
				else
				refundMap = consolidateRefunds(refundList, securityToken, context, null, conn);	
				
				// Consolidate all payments within the order bill
				 
				HashMap paymentMap = null;
				if(isNewPGRoute)
					paymentMap = consolidateMOPayments(paymentList, refundMap);
				else
					paymentMap = consolidatePayments(paymentList, refundMap);
                
                HashMap paymentTypeMap = null;
                Iterator paymentTypeIterator = null;
                PaymentVO payment = null;
                //RefundVO refund = null;
                //OrderBillVO orderBill = null;
                
                // Determine if a gift certificate is one of the payments
                //if(paymentMap.containsKey("G"))
                //{
                //    giftCertExists = true;
                //}
                
                // If payments contain a gift card, use the gift card payment amount
                // to fill buckets intially before using any other payment type
                if(paymentMap.containsKey("G") || paymentMap.containsKey("R"))
                {        
                	giftCertExists = true;
                    HashMap consolidatedPaymentMap = null;
                    //PaymentVO consolidatedPayment = null;
                    //Iterator consolidatedPaymentIterator = null;
                
                    // Iterate through all gift cards on the order and apply to buckets
                    if(paymentMap.containsKey("G")) {
                    	consolidatedPaymentMap = (HashMap) paymentMap.get("G");
                    	distributeBuckets(consolidatedPaymentMap, remainingBill);
                    }
                    
                    if(paymentMap.containsKey("R")) {
                    	consolidatedPaymentMap = (HashMap) paymentMap.get("R");
                    	distributeBuckets(consolidatedPaymentMap, remainingBill);
                    }
                    
                    // Iterate through remaining PayPal payments on the order(if exist) and apply to buckets
                    if(paymentMap.containsKey("P"))
                    {
                        consolidatedPaymentMap = (HashMap) paymentMap.get("P");
                        distributeBuckets(consolidatedPaymentMap, remainingBill); 
                    }

                    // Iterate through remaining credit cards on the order(if exist) and apply to buckets
                    if(paymentMap.containsKey("C"))
                    {
                        consolidatedPaymentMap = (HashMap) paymentMap.get("C");                        
                        distributeBuckets(consolidatedPaymentMap, remainingBill); 
                    }
                }
                
				// Apply refunds against payments
               if(isNewPGRoute)
					applyMORefunds(paymentMap, refundMap, primaryPaymentMap, new RefundTotalVO());
				else
					applyRefunds(paymentMap, refundMap, primaryPaymentMap, new RefundTotalVO());

                // Consolidate all payments from all order bills on order
                Iterator paymentIterator = paymentMap.keySet().iterator();
                while(paymentIterator.hasNext())
                {
                    paymentTypeMap = (HashMap) paymentMap.get((String) paymentIterator.next());
                    
                    paymentTypeIterator = paymentTypeMap.keySet().iterator();
                    while(paymentTypeIterator.hasNext())
                    {
                        payment = (PaymentVO) paymentTypeMap.get((String) paymentTypeIterator.next());
                        
                        if(payment != null)
                        {
                        	
                            String paymentIdentifier = null;
                            if(isNewPGRoute)
                            	paymentIdentifier =  generateIdentifier(payment.getPaymentType(), payment.getPaymentMethodId(), payment.getCardNumber(),payment.getAuthorization());
                            else
                            	paymentIdentifier =  generateIdentifier(payment.getPaymentType(), payment.getPaymentMethodId(), payment.getCardNumber());
             	            
                            if(!primaryPaymentMap.containsKey(paymentIdentifier))
                            {
                            	payment.setPaymentSeqId(paymentSequence++);
                                primaryPaymentMap.put(paymentIdentifier, payment);
                            }
                            else
                            {
                            	PaymentVO previousPayment = (PaymentVO) primaryPaymentMap.get(paymentIdentifier);
                                previousPayment.getOrderBill().setProductAmount(previousPayment.getOrderBill().getProductAmount() + payment.getOrderBill().getProductAmount()); 
                                previousPayment.getOrderBill().setAddOnAmount(previousPayment.getOrderBill().getAddOnAmount() + payment.getOrderBill().getAddOnAmount()); 
                                previousPayment.getOrderBill().setFeeAmount(previousPayment.getOrderBill().getFeeAmount() + payment.getOrderBill().getFeeAmount()); 
                                previousPayment.getOrderBill().setTax(previousPayment.getOrderBill().getTax() + payment.getOrderBill().getTax()); 
                                previousPayment.getOrderBill().setGrossProductAmount(previousPayment.getOrderBill().getGrossProductAmount() + payment.getOrderBill().getGrossProductAmount()); 
                                previousPayment.getOrderBill().setDiscountAmount(previousPayment.getOrderBill().getDiscountAmount() + payment.getOrderBill().getDiscountAmount()); 
                                previousPayment.getOrderBill().setMilesPointsAmount(previousPayment.getOrderBill().getMilesPointsAmount() + payment.getOrderBill().getMilesPointsAmount()); 
                            }
                        }
                    }
                }  
            }
             
            // Create refunds for all the payments   
            PaymentVO payment = null;
            RefundVO refund = null;
            OrderBillVO orderBill = null;
                
            Iterator paymentIterator = primaryPaymentMap.keySet().iterator();
            while(paymentIterator.hasNext())
            {
            	payment = (PaymentVO) primaryPaymentMap.get((String) paymentIterator.next());
                
                refund = new RefundVO();
                refund.setOrderDetailId(orderDetailId);
                refund.setRefundDispCode(refundDisposition);
                refund.setResponsibleParty(responsibleParty);
                refund.setPaymentMethodId(payment.getPaymentMethodId());
                refund.setPaymentType(payment.getPaymentType());
                refund.setCardId(payment.getCardId());
                refund.setCardNumber(payment.getCardNumber());
                refund.setCreatedBy(userId);
                refund.setAccountTransactionInd("Y");
                refund.setRefundStatus(refundStatus);
                
                refund.setAuthorizationTransactionId(payment.getAuthorizationTransactionId());
                refund.setMerchantRefID(payment.getMerchantRefId());
                
                
                refund.setRoute(payment.getRoute());
                refund.setSettlmentTransactionId(payment.getSettlementTransactionId());
                refund.setRequestToken(payment.getRequestToken());
                
                
                refund.setPayment(payment);
                
            
               
                orderBill = new OrderBillVO();
                
                // If gift cert exists, discounts can not be present so no need to split discounts from gross amount
                if(giftCertExists)
                {
                    orderBill.setProductAmount(payment.getOrderBill().getProductAmount());
                }
                else
                {
					BigDecimal productAmt = new BigDecimal(payment.getOrderBill().getProductAmount());
					BigDecimal discount = new BigDecimal(payment.getOrderBill().getDiscountAmount());
					productAmt = productAmt.add(discount);

                    orderBill.setProductAmount(0.0);
                    orderBill.setGrossProductAmount(productAmt.doubleValue());
                    orderBill.setDiscountAmount(payment.getOrderBill().getDiscountAmount());
                }
                
                orderBill.setVendorFlag(getVendorFlag(paymentRefundMap));                    
                orderBill.setAddOnAmount(payment.getOrderBill().getAddOnAmount());
                orderBill.setFeeAmount(payment.getOrderBill().getFeeAmount());
                
                if(orderBill.getVendorFlag() != null &&
                            orderBill.getVendorFlag().equalsIgnoreCase("SD"))
                {
                    // SDG orders break refund fees out by filling shipping first and then service fee
                    if(payment.getOrderBill().getFeeAmount() > payment.getOrderBill().getShippingFee())
                    {
                        orderBill.setShippingFee(payment.getOrderBill().getShippingFee());
                        orderBill.setServiceFee(payment.getOrderBill().getFeeAmount() - payment.getOrderBill().getShippingFee());
                    }
                    else
                    {
                        orderBill.setShippingFee(payment.getOrderBill().getFeeAmount());
                        orderBill.setServiceFee(0);
                    }
                }
                
                orderBill.setCommissionAmount(payment.getOrderBill().getCommissionAmount());
                orderBill.setWholesaleAmount(payment.getOrderBill().getWholesaleAmount());
                orderBill.setTax(payment.getOrderBill().getTax());

                //set the miles/points amounts: original as well as the left overs
                orderBill.setMilesPointsAmount(payment.getOrderBill().getMilesPointsAmount());
                orderBill.setMilesPointsOriginal(payment.getOrderBill().getMilesPointsOriginal());
                
								// These are not used for Modify Order.  Modify Order does not
								// apply to WalMart and Amazon
								//orderBill.setAdminFee(this.calculateAdminFee(payment.getOrderBill().getCommissionAmount(), request.getParameter("origin_id")));
                //orderBill.setWholesaleServiceFee(calculateWholesaleFee(payment.getOrderBill().getWholesaleServiceFee(), payment.getOrderBill().getFeeAmount()));
                
                calculateTaxes(orderBill, conn, orderDetailId, 1);
				refund.setOrderBill(orderBill);

                // Only bother to insert refund if something to refund                
                if (isNonZeroRefund(orderBill)) {
                	logger.debug("Posting refund for orderDetailId: " + refund.getOrderDetailId());
                  refundDAO.insertRefund(refund);
                  refundsIssuedList.add(refund);
                } else {
                	logger.debug("Nothing to refund for orderDetailId: " + refund.getOrderDetailId());
                }
            } 
            
            // Defect 3737. If the order is in Held, change to Processed
            if (StringUtils.isNotEmpty(orderDetailId)) {
                 odVO = oDAO.getOrderDetail(orderDetailId); 
            }
            
            if (odVO != null                                              && 
                 StringUtils.isNotEmpty(odVO.getOrderDispCode())           && 
                 StringUtils.equalsIgnoreCase(odVO.getOrderDispCode(), "Held"))
            {
                oDAO.postCancelHeldOrder(odVO.getOrderDetailId(), userId);
            }
            
            
            handleRefundStatusModified(orderDetailId, userId, conn);
        }
        
        return refundsIssuedList;
                
    }
	
	protected static String getVendorFlag(HashMap paymentRefundMap)
	{
	    String freshCutFlag = (String) paymentRefundMap.get(RefundDAO.FRESH_CUT_FLAG);
	    
	    // Same day requires special logic on save
	    if(((String) paymentRefundMap.get(RefundDAO.PRODUCT_TYPE)) != null && ((String) paymentRefundMap.get(RefundDAO.PRODUCT_TYPE)).equalsIgnoreCase("SDG")
	        && ((String) paymentRefundMap.get(RefundDAO.SHIP_METHOD)) != null && ((String) paymentRefundMap.get(RefundDAO.SHIP_METHOD)).equalsIgnoreCase("SD"))
	    {
	        return "SD";
	    }
	    // Frescuts always are service fee
	    if(freshCutFlag != null && freshCutFlag.equals("Y"))
	    {
	        // Freshcuts are always service fee
	        return "N";
	    }
	    else
	    {
	        // Otherwise the database will determine the vendor flag based on ship method
	        return (String) paymentRefundMap.get(RefundDAO.ORDER_VENDOR_FLAG);
	    }
	}

	/**
	 * 
	 * @param paymentMap
	 * @param refundMap
	 * @param primaryPaymentMap
	 * @param refundTotalVO - an object which stores the state of the refund totals
	 */
	protected static void applyRefunds(HashMap paymentMap, HashMap refundMap, HashMap primaryPaymentMap, RefundTotalVO refundTotal)
	{
	    HashMap paymentTypeMap = null;
	    Iterator paymentTypeIterator = null;
	    PaymentVO payment = null;
	    ArrayList consolidatedRefundList = null;
	    RefundVO refund = null;
	    String paymentIdentifier = null;
	    
	    Iterator paymentIterator = paymentMap.keySet().iterator();
	    while(paymentIterator.hasNext())
	    {
	        paymentTypeMap = (HashMap) paymentMap.get((String) paymentIterator.next());
	        
	        paymentTypeIterator = paymentTypeMap.keySet().iterator();
	        while(paymentTypeIterator.hasNext())
	        {
	            payment = (PaymentVO) paymentTypeMap.get((String) paymentTypeIterator.next());
	            
	           	paymentIdentifier = generateIdentifier(payment.getPaymentType(), payment.getPaymentMethodId(), payment.getCardNumber());
	           	
	            
	            // Do not apply refund if payment refund has already been applied to this payment
	            if(!primaryPaymentMap.containsKey(paymentIdentifier))
	            {
	                if(refundMap != null && refundMap.containsKey(paymentIdentifier))
	                {
	                    consolidatedRefundList = (ArrayList) refundMap.get(paymentIdentifier);
	                    for(int i=0; i < consolidatedRefundList.size(); i++)
	                    {
	                        refund = (RefundVO) consolidatedRefundList.get(i);
	                        payment.getOrderBill().setProductAmount(payment.getOrderBill().getProductAmount() - refund.getOrderBill().getProductAmount());
	                        payment.getOrderBill().setAddOnAmount(payment.getOrderBill().getAddOnAmount() - refund.getOrderBill().getAddOnAmount());
	                        payment.getOrderBill().setFeeAmount(payment.getOrderBill().getFeeAmount() - refund.getOrderBill().getFeeAmount());
	                        payment.getOrderBill().setTax(payment.getOrderBill().getTax() - refund.getOrderBill().getTax());
	                        payment.getOrderBill().setMilesPointsAmount(payment.getOrderBill().getMilesPointsAmount() - refund.getOrderBill().getMilesPointsAmount());
	                    
	                        /*if(payment.getOrderBill().getProductAmount() < 0){
	                        	payment.getOrderBill().setProductAmount(0);
	                        }
	                        if(payment.getOrderBill().getAddOnAmount()< 0){
	                        	payment.getOrderBill().setAddOnAmount(0);
	                        }
	                        if(payment.getOrderBill().getFeeAmount() < 0){
	                        	payment.getOrderBill().setFeeAmount(0);
	                        }
	                        if(payment.getOrderBill().getTax() < 0){
	                        	payment.getOrderBill().setTax(0);
	                        }
	                        if(payment.getOrderBill().getMilesPointsAmount() < 0){
	                        	payment.getOrderBill().setMilesPointsAmount(0);
	                        }*/
	                        		
	                        refundTotal.setMsdTotal(refundTotal.getMsdTotal() - refund.getOrderBill().getProductAmount());
	                        refundTotal.setAddOnTotal(refundTotal.getAddOnTotal() - refund.getOrderBill().getAddOnAmount());
	                        refundTotal.setFeeTotal(refundTotal.getFeeTotal() - refund.getOrderBill().getFeeAmount());
	                        refundTotal.setTaxTotal(refundTotal.getTaxTotal() - refund.getOrderBill().getTax());
	                        refundTotal.setMilesPointsTotal(refundTotal.getMilesPointsTotal() - refund.getOrderBill().getMilesPointsAmount());
	                    }
	                }
	            }
	        }
	    }
	}
	
	/**
	 * 
	 * @param paymentMap
	 * @param refundMap
	 * @param primaryPaymentMap
	 * @param refundTotalVO - an object which stores the state of the refund totals
	 */
	protected static void applyMORefunds(HashMap paymentMap, HashMap refundMap, HashMap primaryPaymentMap, RefundTotalVO refundTotal)
	{
	    HashMap paymentTypeMap = null;
	    Iterator paymentTypeIterator = null;
	    PaymentVO payment = null;
	    ArrayList consolidatedRefundList = null;
	    RefundVO refund = null;
	    String paymentIdentifier = null;
	    
	    Iterator paymentIterator = paymentMap.keySet().iterator();
	    while(paymentIterator.hasNext())
	    {
	        paymentTypeMap = (HashMap) paymentMap.get((String) paymentIterator.next());
	        
	        paymentTypeIterator = paymentTypeMap.keySet().iterator();
	        while(paymentTypeIterator.hasNext())
	        {
	            payment = (PaymentVO) paymentTypeMap.get((String) paymentTypeIterator.next());
	            
	           	paymentIdentifier = generateIdentifier(payment.getPaymentType(), payment.getPaymentMethodId(), payment.getCardNumber(),payment.getAuthorization());
	           	
	            
	            // Do not apply refund if payment refund has already been applied to this payment
	            if(!primaryPaymentMap.containsKey(paymentIdentifier))
	            {
	                if(refundMap != null && refundMap.containsKey(paymentIdentifier))
	                {
	                    consolidatedRefundList = (ArrayList) refundMap.get(paymentIdentifier);
	                    for(int i=0; i < consolidatedRefundList.size(); i++)
	                    {
	                        refund = (RefundVO) consolidatedRefundList.get(i);
	                        payment.getOrderBill().setProductAmount(payment.getOrderBill().getProductAmount() - refund.getOrderBill().getProductAmount());
	                        payment.getOrderBill().setAddOnAmount(payment.getOrderBill().getAddOnAmount() - refund.getOrderBill().getAddOnAmount());
	                        payment.getOrderBill().setFeeAmount(payment.getOrderBill().getFeeAmount() - refund.getOrderBill().getFeeAmount());
	                        payment.getOrderBill().setTax(payment.getOrderBill().getTax() - refund.getOrderBill().getTax());
	                        payment.getOrderBill().setMilesPointsAmount(payment.getOrderBill().getMilesPointsAmount() - refund.getOrderBill().getMilesPointsAmount());
	                    
	                        /*if(payment.getOrderBill().getProductAmount() < 0){
	                        	payment.getOrderBill().setProductAmount(0);
	                        }
	                        if(payment.getOrderBill().getAddOnAmount()< 0){
	                        	payment.getOrderBill().setAddOnAmount(0);
	                        }
	                        if(payment.getOrderBill().getFeeAmount() < 0){
	                        	payment.getOrderBill().setFeeAmount(0);
	                        }
	                        if(payment.getOrderBill().getTax() < 0){
	                        	payment.getOrderBill().setTax(0);
	                        }
	                        if(payment.getOrderBill().getMilesPointsAmount() < 0){
	                        	payment.getOrderBill().setMilesPointsAmount(0);
	                        }*/
	                        		
	                        refundTotal.setMsdTotal(refundTotal.getMsdTotal() - refund.getOrderBill().getProductAmount());
	                        refundTotal.setAddOnTotal(refundTotal.getAddOnTotal() - refund.getOrderBill().getAddOnAmount());
	                        refundTotal.setFeeTotal(refundTotal.getFeeTotal() - refund.getOrderBill().getFeeAmount());
	                        refundTotal.setTaxTotal(refundTotal.getTaxTotal() - refund.getOrderBill().getTax());
	                        refundTotal.setMilesPointsTotal(refundTotal.getMilesPointsTotal() - refund.getOrderBill().getMilesPointsAmount());
	                    }
	                }
	            }
	        }
	    }
	}
	
	/** applies the remaining bill amounts to each payment in the consolidated payment map
	 * 
	 * @param consolidatedPaymentMap
	 * @param remainingBill
	 */
	protected static void distributeBuckets(HashMap consolidatedPaymentMap, OrderBillVO remainingBill)
	{
		if(consolidatedPaymentMap == null || consolidatedPaymentMap.size() == 0) {
			return;
		}
	    ArrayList emptyPaymentList = new ArrayList();
	    String key = null;
	    boolean firstTimeThrough = true;
	    PaymentVO consolidatedPayment = null;
	    Iterator consolidatedPaymentIterator = consolidatedPaymentMap.keySet().iterator();
	    NumberFormat nf = NumberFormat.getNumberInstance();
	    
	    while(consolidatedPaymentIterator.hasNext())
	    {
	        key = (String) consolidatedPaymentIterator.next();
	        consolidatedPayment = (PaymentVO) consolidatedPaymentMap.get(key);
	        
	        // Default buckets to 0 on payment auto distributions
	        consolidatedPayment.getOrderBill().setProductAmount(0);
	        consolidatedPayment.getOrderBill().setAddOnAmount(0);
	        consolidatedPayment.getOrderBill().setFeeAmount(0);
	        consolidatedPayment.getOrderBill().setTax(0);
	        
	        // Need to format number for comparison because of doubles precision when subtracting
	        //QC - 10474 - If creditAmount is 0, no amount is refunded, delete the payment object from list.
	        if(consolidatedPayment.getCreditAmount() > 0 && (new Double(nf.format(remainingBill.getProductAmount())).doubleValue() > 0
	            || new Double(nf.format(remainingBill.getAddOnAmount())).doubleValue() > 0
	            || new Double(nf.format(remainingBill.getFeeAmount())).doubleValue() > 0
	            || new Double(nf.format(remainingBill.getTax())).doubleValue() > 0
	            || (consolidatedPayment.getRefundList() != null && consolidatedPayment.getRefundList().size() > 0) )) {
	        	applyAmount(consolidatedPayment, remainingBill);
	        }
	        else
	        {
	            emptyPaymentList.add(key);
	        }
	    }
	    
	    // Remove payments with no amounts from payment list so they dont display
	    if(emptyPaymentList.size() > 0)
	    {
	        for(int i=0; i < emptyPaymentList.size(); i++)
	        {
	            consolidatedPaymentMap.remove(emptyPaymentList.get(i));
	        }
	    }
	}

	/**
	 * removes payment amounts from remaining bill and applies it to consolidated payment
	 * 
	 * @param consolidatedPayment
	 * @param remainingBill
	 */
	protected static void applyAmount(PaymentVO consolidatedPayment, OrderBillVO remainingBill)
	{
	    if(consolidatedPayment.getCreditAmount() > remainingBill.getProductAmount())
	    {
	        consolidatedPayment.getOrderBill().setProductAmount(remainingBill.getProductAmount());
	        consolidatedPayment.setCreditAmount(consolidatedPayment.getCreditAmount() - remainingBill.getProductAmount());
	        remainingBill.setProductAmount(0);
	    }
	    else
	    {
	        consolidatedPayment.getOrderBill().setProductAmount(consolidatedPayment.getOrderBill().getProductAmount() + consolidatedPayment.getCreditAmount());
	        remainingBill.setProductAmount(remainingBill.getProductAmount() - consolidatedPayment.getCreditAmount());
	        return;
	    }
	    
	    if(consolidatedPayment.getCreditAmount() > remainingBill.getAddOnAmount())
	    {
	        consolidatedPayment.getOrderBill().setAddOnAmount(remainingBill.getAddOnAmount());
	        consolidatedPayment.setCreditAmount(consolidatedPayment.getCreditAmount() - remainingBill.getAddOnAmount());
	        remainingBill.setAddOnAmount(0);
	    }
	    else
	    {
	        consolidatedPayment.getOrderBill().setAddOnAmount(consolidatedPayment.getOrderBill().getAddOnAmount() + consolidatedPayment.getCreditAmount());
	        remainingBill.setAddOnAmount(remainingBill.getAddOnAmount() - consolidatedPayment.getCreditAmount());
	        return;
	    }
	    
	    if(consolidatedPayment.getCreditAmount() > remainingBill.getFeeAmount())
	    {
	        consolidatedPayment.getOrderBill().setFeeAmount(remainingBill.getFeeAmount());
	        consolidatedPayment.setCreditAmount(consolidatedPayment.getCreditAmount() - remainingBill.getFeeAmount());
	        remainingBill.setFeeAmount(0);
	    }
	    else
	    {
	        consolidatedPayment.getOrderBill().setFeeAmount(consolidatedPayment.getOrderBill().getFeeAmount() + consolidatedPayment.getCreditAmount());
	        remainingBill.setFeeAmount(remainingBill.getFeeAmount() - consolidatedPayment.getCreditAmount());
	        return;
	    }
	    
	    if(consolidatedPayment.getCreditAmount() > remainingBill.getTax())
	    {
	        consolidatedPayment.getOrderBill().setTax(remainingBill.getTax());
	        consolidatedPayment.setCreditAmount(consolidatedPayment.getCreditAmount() - remainingBill.getTax());
	        remainingBill.setTax(0);
	    }
	    else
	    {
	        consolidatedPayment.getOrderBill().setTax(consolidatedPayment.getOrderBill().getTax() + consolidatedPayment.getCreditAmount());
	        remainingBill.setTax(remainingBill.getTax() - consolidatedPayment.getCreditAmount());
	        return;
	    }
	}

	/**
	 * Returns true if there is a non-zero refund amount
	 */
	private static boolean isNonZeroRefund(OrderBillVO orderBill) 
	{
	    boolean isNonZero = false;
	    if ((orderBill.getProductAmount() != 0)  || (orderBill.getAddOnAmount() != 0) ||
	        (orderBill.getAdminFee() != 0)        || (orderBill.getCommissionAmount() != 0) ||
	        (orderBill.getDiscountAmount() != 0)  || (orderBill.getFeeAmount() != 0) ||
	        (orderBill.getTax() != 0)             || (orderBill.getGrossProductAmount() != 0)  || 
	        (orderBill.getServiceFee() != 0)      || (orderBill.getShippingFee() != 0) ||
	        (orderBill.getShippingTax() != 0)     || (orderBill.getWholesaleServiceFee() != 0) || 
	        (orderBill.getWholesaleAmount() != 0)) 
	        {
	          isNonZero = true;
	        }
	    return isNonZero;
	}

	/**
	     * Consolidate all payments from the order bill (paymentList)
	     * 
	     * @param paymentList
	     * @param refundMap
	 * @param individualPaymentsList 
	     * @return
	     */
	    protected static HashMap<String,HashMap<String,PaymentVO>> consolidatePayments(List<PaymentVO> paymentList, Map<String, ArrayList> refundMap)
	    {
		    int paymentSequence = 1;
	        HashMap<String, HashMap<String,PaymentVO>> paymentMap = new HashMap<String,HashMap<String,PaymentVO>>();
	        ArrayList<String> billList = new ArrayList<String>();
	        //ArrayList refundList = null;
	        HashMap<String, PaymentVO> consolidatedPaymentMap = null;
	        PaymentVO payment = null;
	        PaymentVO consolidatedPayment = null;
			String paymentType = null;
	        String paymentIdentifier = null;
	    
	        Iterator<PaymentVO> paymentIterator = paymentList.iterator();
	        while(paymentIterator.hasNext())
	        {
	        	payment = (PaymentVO) paymentIterator.next();
	            paymentIdentifier = generateIdentifier(payment.getPaymentType(), payment.getPaymentMethodId(), payment.getCardNumber());
	            
	            //get or create a consolidated payment map for this payment
	            if(paymentMap.containsKey(payment.getPaymentType()))
	            {
	                consolidatedPaymentMap = paymentMap.get(payment.getPaymentType());
	            }
	            else
	            {
	                consolidatedPaymentMap = new HashMap<String,PaymentVO>();
	                paymentMap.put(payment.getPaymentType(), consolidatedPaymentMap);
	            }
	            
	            if(consolidatedPaymentMap.containsKey(paymentIdentifier))
	            {
	                // Payment already exists and needs to be consolidated                
	                consolidatedPayment = (PaymentVO) consolidatedPaymentMap.get(paymentIdentifier);
	                consolidatedPayment.setCreditAmount(consolidatedPayment.getCreditAmount() + payment.getCreditAmount());
	                
	                if(!billList.contains(payment.getOrderBill().getOrderBillId()))
	                {
	                    consolidatedPayment.getOrderBill().setProductAmount(consolidatedPayment.getOrderBill().getProductAmount() + payment.getOrderBill().getProductAmount());
	                    consolidatedPayment.getOrderBill().setAddOnAmount(consolidatedPayment.getOrderBill().getAddOnAmount() + payment.getOrderBill().getAddOnAmount());
	                    consolidatedPayment.getOrderBill().setFeeAmount(consolidatedPayment.getOrderBill().getFeeAmount() + payment.getOrderBill().getFeeAmount());
	                    consolidatedPayment.getOrderBill().setTax(consolidatedPayment.getOrderBill().getTax() + payment.getOrderBill().getTax());
	                    consolidatedPayment.getOrderBill().setMilesPointsAmount(consolidatedPayment.getOrderBill().getMilesPointsAmount() + payment.getOrderBill().getMilesPointsAmount());
	                    consolidatedPayment.getOrderBill().setServiceFee(consolidatedPayment.getOrderBill().getServiceFee());
	                    consolidatedPayment.getOrderBill().setShippingFee(consolidatedPayment.getOrderBill().getShippingFee());
	//                    consolidatedPayment.getOrderBill().setFuelSurchargeAmount(consolidatedPayment.getOrderBill().getFuelSurchargeAmount());
	//                    consolidatedPayment.getOrderBill().setAlsHawaiiUpcharge(consolidatedPayment.getOrderBill().getAlsHawaiiUpcharge());
	//                    consolidatedPayment.getOrderBill().setSaturdayUpcharge(consolidatedPayment.getOrderBill().getSaturdayUpcharge());
	                    
	                }
	            }
	            else
	            {
	                // Set any refunds to the payment
	                if(refundMap != null && refundMap.containsKey(paymentIdentifier))
	                {

	                    payment.setRefundList((ArrayList) refundMap.get(paymentIdentifier));
	                }
	                
	                // Add new payment
	                consolidatedPaymentMap.put(paymentIdentifier, payment);
	            }
	            
	            // Sum bucket totals
	            if(!billList.contains(payment.getOrderBill().getOrderBillId()))
	            {
	//                msdTotal = msdTotal + payment.getOrderBill().getProductAmount();
	//                addOnTotal = addOnTotal + payment.getOrderBill().getAddOnAmount();
	//                feeTotal = feeTotal + payment.getOrderBill().getFeeAmount();
	//                taxTotal = taxTotal + payment.getOrderBill().getTax();
	//                milesPointsTotal = milesPointsTotal + payment.getOrderBill().getMilesPointsAmount();
	//                
	//                paymentMsdTotal = paymentMsdTotal + payment.getOrderBill().getProductAmount();
	//                paymentAddOnTotal = paymentAddOnTotal + payment.getOrderBill().getAddOnAmount();
	//                paymentFeeTotal = paymentFeeTotal + payment.getOrderBill().getFeeAmount();
	//                paymentTaxTotal = paymentTaxTotal + payment.getOrderBill().getTax();
	//                paymentMilesPointsTotal = paymentMilesPointsTotal + payment.getOrderBill().getMilesPointsAmount();
	//                serviceFee = serviceFee + payment.getOrderBill().getServiceFee();
	//                shippingFee = shippingFee + payment.getOrderBill().getShippingFee();
	//                fuelSurchargeAmount = fuelSurchargeAmount + payment.getOrderBill().getFuelSurchargeAmount();
	//                fuelSurchargeDescription = payment.getOrderBill().getFuelSurchargeDescription() ;
	//                applySurchargeCode = payment.getOrderBill().getApplySurchargeCode();
	//                saturdayUpcharge = saturdayUpcharge + payment.getOrderBill().getSaturdayUpcharge();
	//                alsHawaiiUpcharge = alsHawaiiUpcharge + payment.getOrderBill().getAlsHawaiiUpcharge();
	
	                billList.add(payment.getOrderBill().getOrderBillId());
	            }
	        }
	       
	        return paymentMap;
	    }
	    
	    /**
	     * Consolidate all payments from the order bill (paymentList)
	     * 
	     * @param paymentList
	     * @param refundMap
	     * @return
	     */
	    protected static HashMap<String,HashMap<String,PaymentVO>> consolidateMOPayments(List<PaymentVO> paymentList, Map<String, ArrayList> refundMap)
	    {
		   // int paymentSequence = 1;
	        HashMap<String, HashMap<String,PaymentVO>> paymentMap = new HashMap<String,HashMap<String,PaymentVO>>();
	        ArrayList<String> billList = new ArrayList<String>();
	        //ArrayList refundList = null;
	        HashMap<String, PaymentVO> consolidatedPaymentMap = null;
	        PaymentVO payment = null;
	        PaymentVO consolidatedPayment = null;
			//String paymentType = null;
	        String paymentIdentifier = null;
	    
	        Iterator<PaymentVO> paymentIterator = paymentList.iterator();
	        while(paymentIterator.hasNext())
	        {
	        	payment = (PaymentVO) paymentIterator.next();
	            paymentIdentifier = generateIdentifier(payment.getPaymentType(), payment.getPaymentMethodId(), payment.getCardNumber(),payment.getAuthorization());
	            
	            //get or create a consolidated payment map for this payment
	            if(paymentMap.containsKey(payment.getPaymentType()))
	            {
	                consolidatedPaymentMap = paymentMap.get(payment.getPaymentType());
	            }
	            else
	            {
	                consolidatedPaymentMap = new HashMap<String,PaymentVO>();
	                paymentMap.put(payment.getPaymentType(), consolidatedPaymentMap);
	            }
	            
	            if(consolidatedPaymentMap.containsKey(paymentIdentifier))
	            {
	                // Payment already exists and needs to be consolidated                
	                consolidatedPayment = (PaymentVO) consolidatedPaymentMap.get(paymentIdentifier);
	                consolidatedPayment.setCreditAmount(consolidatedPayment.getCreditAmount() + payment.getCreditAmount());
	                
	                if(!billList.contains(payment.getOrderBill().getOrderBillId()))
	                {
	                    consolidatedPayment.getOrderBill().setProductAmount(consolidatedPayment.getOrderBill().getProductAmount() + payment.getOrderBill().getProductAmount());
	                    consolidatedPayment.getOrderBill().setAddOnAmount(consolidatedPayment.getOrderBill().getAddOnAmount() + payment.getOrderBill().getAddOnAmount());
	                    consolidatedPayment.getOrderBill().setFeeAmount(consolidatedPayment.getOrderBill().getFeeAmount() + payment.getOrderBill().getFeeAmount());
	                    consolidatedPayment.getOrderBill().setTax(consolidatedPayment.getOrderBill().getTax() + payment.getOrderBill().getTax());
	                    consolidatedPayment.getOrderBill().setMilesPointsAmount(consolidatedPayment.getOrderBill().getMilesPointsAmount() + payment.getOrderBill().getMilesPointsAmount());
	                    consolidatedPayment.getOrderBill().setServiceFee(consolidatedPayment.getOrderBill().getServiceFee());
	                    consolidatedPayment.getOrderBill().setShippingFee(consolidatedPayment.getOrderBill().getShippingFee());
	                }
	            }
	            else
	            {
	                // Set any refunds to the payment
	                if(refundMap != null && refundMap.containsKey(paymentIdentifier))
	                {

	                    payment.setRefundList((ArrayList) refundMap.get(paymentIdentifier));
	                }
	                
	                // Add new payment
	                consolidatedPaymentMap.put(paymentIdentifier, payment);
	            }
	            
	            // Sum bucket totals
	            if(!billList.contains(payment.getOrderBill().getOrderBillId()))
	            {
	                billList.add(payment.getOrderBill().getOrderBillId());
	            }
	        }
	       
	        return paymentMap;
	    }
	    
	    protected static HashMap<String,HashMap<String,PaymentVO>> buildIndividualPayments(List<PaymentVO> paymentList, Map<String, ArrayList> refundMap, List<PaymentVO> individualPaymentsList) throws Exception
	    {
			HashMap<String, HashMap<String, PaymentVO>> paymentMap = new HashMap<String, HashMap<String, PaymentVO>>();
			PaymentVO payment = null;
			String paymentIdentifier = null;
	
			Iterator<PaymentVO> paymentIterator = paymentList.iterator();
			while (paymentIterator.hasNext()) {
	
				payment = (PaymentVO) paymentIterator.next();
				PaymentVO individualPaymentVO = new PaymentVO();
				individualPaymentVO = copyPaymentVO(payment);
				paymentIdentifier = generateIdentifier(payment.getPaymentType(), payment.getPaymentMethodId(), payment.getCardNumber());
	
				// Set any refunds to the payment
				if (refundMap != null && refundMap.containsKey(paymentIdentifier)) {
					double debitAmount = 0;
	
					if (individualPaymentsList != null) {
						List<RefundVO> refundList = (ArrayList<RefundVO>) refundMap.get(paymentIdentifier);
						List<RefundVO> individualRefundList = new ArrayList<RefundVO>();
						if (refundList != null) {
							for (RefundVO refundVO : refundList) {
								if (payment.getAuthorization() != null && payment.getAuthorization().equalsIgnoreCase(refundVO.getAuthorization())) {
									individualPaymentVO.getOrderBill().setProductAmount(individualPaymentVO.getOrderBill().getProductAmount() - refundVO.getOrderBill().getProductAmount());
									individualPaymentVO.getOrderBill().setAddOnAmount(individualPaymentVO.getOrderBill().getAddOnAmount() - refundVO.getOrderBill().getAddOnAmount());
									individualPaymentVO.getOrderBill().setFeeAmount(individualPaymentVO.getOrderBill().getFeeAmount() - refundVO.getOrderBill().getFeeAmount());
									individualPaymentVO.getOrderBill().setTax(individualPaymentVO.getOrderBill().getTax() - refundVO.getOrderBill().getTax());
									individualPaymentVO.getOrderBill().setMilesPointsAmount(individualPaymentVO.getOrderBill().getMilesPointsAmount() - refundVO.getOrderBill().getMilesPointsAmount());
				                    
									individualRefundList.add(refundVO);
									debitAmount += refundVO.getDebitAmount();
								} 
							}
						}
						if (individualRefundList != null && !individualRefundList.isEmpty()) {
							individualPaymentVO.setRefundList(individualRefundList);
						} else {
							individualPaymentVO.setRefundList(null);
						}
						individualPaymentVO.setDebitAmount(debitAmount);
					}
				}
				try {
					DecimalFormat formatter = new DecimalFormat("0.00");
					double creditAmount = 0.0;
					creditAmount = (Double.valueOf(formatter.format(individualPaymentVO.getOrderBill().getProductAmount())) +
							Double.valueOf(formatter.format(individualPaymentVO.getOrderBill().getAddOnAmount())) +
							Double.valueOf(formatter.format(individualPaymentVO.getOrderBill().getFeeAmount())) +
							Double.valueOf(formatter.format(individualPaymentVO.getOrderBill().getTax())));
					individualPaymentVO.setCreditAmount(Double.valueOf(formatter.format(creditAmount)));
				}catch(Exception e) {
					logger.error("Error occured while calculating credit amount: " + e.getMessage());
				}
	
				if (individualPaymentsList != null) {
					individualPaymentsList.add(individualPaymentVO);
				}
			}
			logger.info("******************************************************************************************************");
			logger.info("individualPaymentsList:" + individualPaymentsList);
			logger.info("******************************************************************************************************");
			return paymentMap;
		}

	    /**
	     * Makes a deep copy of Java object that is passed.
	     */
	     private static PaymentVO copyPaymentVO(PaymentVO paymentVO) {
	       try {
	         ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
	         ObjectOutputStream outputStrm = new ObjectOutputStream(outputStream);
	         outputStrm.writeObject(paymentVO);
	         ByteArrayInputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());
	         ObjectInputStream objInputStream = new ObjectInputStream(inputStream);
	         return (PaymentVO) objInputStream.readObject();
	       }
	       catch (Exception e) {
	         e.printStackTrace();
	         return null;
	       }
	     }
	     
	/**
	 * Consolidate refunds from the refundList into a response object
	 * 
	 * @param refundList
	 * @param securityToken
	 * @param context
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	protected static HashMap consolidateRefunds(List<RefundVO> refundList, String securityToken, String context, String userId, Connection con) throws Exception
    {
        HashMap refundPaymentMap = new HashMap();
        String refundIdentifier = null;
        RefundVO refund = null;
        String aTypeFlag;
		RefundDAO refundDAO = new RefundDAO(con);
		
        Iterator<RefundVO> refundIterator = refundList.iterator();
        while(refundIterator.hasNext())
        {
            refund = (RefundVO) refundIterator.next();
            
            // Set A type flag for display use on page
            if(refund.getRefundDispCode() != null && refund.getRefundDispCode().startsWith("A"))
            {
                aTypeFlag = "Y";
            }
            
            
            refundIdentifier = generateIdentifier(refund.getPaymentType(), refund.getPaymentMethodId(), refund.getCardNumber());
          
            
            // If refund has not been processed or current user = refund created by user 
            // or current user contains Update action on the Refund resource
            refund.setRefundUpdateFlag("N");
            refund.setResponsiblePartyUpdateFlag("N");

            // ...but if called via remote API, context/securityToken are both non-applicable so don't bother checking
            if (context != null || securityToken != null) 
            {
                if(refund.getRefundStatus() != null && refund.getRefundStatus().equals(ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, COMConstants.REFUND_UNPROCESSED_STATUS)) && refundDAO.isRefundReturned(refund.getPaymentId()) == false)
                {
                    String securityFlag = ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, COMConstants.SECURITY_IS_ON);
                    if(securityFlag != null && securityFlag.equals("true"))
                    {
                        SecurityManager securityManager = SecurityManager.getInstance();
                        if (securityManager.assertPermission(context, securityToken, ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, COMConstants.REFUND), COMConstants.UPDATE)
                            || (userId != null && userId.equals(refund.getCreatedBy())))
                        {
                            refund.setRefundUpdateFlag("Y");
                        }
                    }
                }
                else
                {
                    String securityFlag = ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, COMConstants.SECURITY_IS_ON);
                    if(securityFlag != null && securityFlag.equals("true"))
                    {                        
                        SecurityManager securityManager = SecurityManager.getInstance();
                        if (securityManager.assertPermission(context, securityToken, ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, COMConstants.UPDATE_RESPONSIBLE_PARTY), COMConstants.UPDATE))
                        {
                            refund.setResponsiblePartyUpdateFlag("Y");
                        }
                    }
                }
                        
            }
            
            if(refundPaymentMap.containsKey(refundIdentifier))
            {
                ((ArrayList) refundPaymentMap.get(refundIdentifier)).add(refund);
            }
            else
            {
                refundList = new ArrayList();
                refundList.add(refund);
                refundPaymentMap.put(refundIdentifier, refundList);
            }
        }
        
        return refundPaymentMap;
    }
	
	/**
	 * Consolidate refunds from the refundList into a response object
	 * 
	 * @param refundList
	 * @param securityToken
	 * @param context
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	protected static HashMap consolidateMORefunds(List<RefundVO> refundList, String securityToken, String context, String userId, Connection con) throws Exception
    {
        HashMap refundPaymentMap = new HashMap();
        String refundIdentifier = null;
        RefundVO refund = null;
        String aTypeFlag;
		RefundDAO refundDAO = new RefundDAO(con);
		
        Iterator<RefundVO> refundIterator = refundList.iterator();
        while(refundIterator.hasNext())
        {
            refund = (RefundVO) refundIterator.next();
            
            // Set A type flag for display use on page
            if(refund.getRefundDispCode() != null && refund.getRefundDispCode().startsWith("A"))
            {
                aTypeFlag = "Y";
            }
            
            refundIdentifier = generateIdentifier(refund.getPaymentType(), refund.getPaymentMethodId(), refund.getCardNumber(),refund.getAuthorization());
          
            
            // If refund has not been processed or current user = refund created by user 
            // or current user contains Update action on the Refund resource
            refund.setRefundUpdateFlag("N");
            refund.setResponsiblePartyUpdateFlag("N");

            // ...but if called via remote API, context/securityToken are both non-applicable so don't bother checking
            if (context != null || securityToken != null) 
            {
                if(refund.getRefundStatus() != null && refund.getRefundStatus().equals(ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, COMConstants.REFUND_UNPROCESSED_STATUS)) && refundDAO.isRefundReturned(refund.getPaymentId()) == false)
                {
                    String securityFlag = ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, COMConstants.SECURITY_IS_ON);
                    if(securityFlag != null && securityFlag.equals("true"))
                    {
                        SecurityManager securityManager = SecurityManager.getInstance();
                        if (securityManager.assertPermission(context, securityToken, ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, COMConstants.REFUND), COMConstants.UPDATE)
                            || (userId != null && userId.equals(refund.getCreatedBy())))
                        {
                            refund.setRefundUpdateFlag("Y");
                        }
                    }
                }
                else
                {
                    String securityFlag = ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, COMConstants.SECURITY_IS_ON);
                    if(securityFlag != null && securityFlag.equals("true"))
                    {                        
                        SecurityManager securityManager = SecurityManager.getInstance();
                        if (securityManager.assertPermission(context, securityToken, ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, COMConstants.UPDATE_RESPONSIBLE_PARTY), COMConstants.UPDATE))
                        {
                            refund.setResponsiblePartyUpdateFlag("Y");
                        }
                    }
                }
                        
            }
            
            if(refundPaymentMap.containsKey(refundIdentifier))
            {
                ((ArrayList) refundPaymentMap.get(refundIdentifier)).add(refund);
            }
            else
            {
                refundList = new ArrayList();
                refundList.add(refund);
                refundPaymentMap.put(refundIdentifier, refundList);
            }
        }
        
        return refundPaymentMap;
    }
	
    protected static String generateIdentifier(String paymentType, String paymentMethodId, String cardNumber)
    {
        String identifier = null;
        
        if(paymentType != null)
        {
            if(paymentType.equals("C"))
            {
                identifier = paymentMethodId + cardNumber;
            }
            else if(paymentType.equals("G"))
            {
                identifier = paymentMethodId + cardNumber;
            }
            else if(paymentType.equals("R"))
            {
                identifier = paymentMethodId + cardNumber;
            }
            else if(paymentType.equals("I") || paymentType.equals("P"))
            {
                identifier = paymentMethodId;
            }
        }
        
        return identifier;
    }
    
    protected static String generateIdentifier(String paymentType, String paymentMethodId, String cardNumber, String authorization)
    {
        String identifier = null;
        
        if(paymentType != null)
        {
            if(paymentType.equals("C"))
            {
                identifier = paymentMethodId + cardNumber + authorization;
            }
            else if(paymentType.equals("G"))
            {
                identifier = paymentMethodId + cardNumber;
            }
            else if(paymentType.equals("R"))
            {
                identifier = paymentMethodId + cardNumber;
            }
            else if(paymentType.equals("I") || paymentType.equals("P"))
            {
                identifier = paymentMethodId;
            }
        }
        
        return identifier;
    }
    
    /**
     * Checks to see if the refund status is modified, and processes updates to
     * Free Shipping Membership accordingly.
     * @param orderDetailId
     * @throws Exception
     */
    protected void handleRefundStatusModified(String orderDetailId, String userId, Connection con) 
      throws Exception
    { 
      logger.info("Beginning handleRefundStatusModified");
      String payLoad;
      String emailAddress="";
      if (refundDAO == null) {
    	  refundDAO = new RefundDAO(con);
      }
      String isOrderFullyRefunded = refundDAO.isOrderFullyRefundedCRS(orderDetailId);
      OrderDAO oDAO = getOrderDAO(con);
      OrderDetailVO odVO = oDAO.getOrderDetail(orderDetailId);
      if("Y".equalsIgnoreCase(isOrderFullyRefunded))
      {
    	  payLoad = "<accountTask type=\"FULLY_REFUND_ORDER_UPDATE\"><fsMembershipVO><externalOrderNumber>"+odVO.getExternalOrderNumber()+"</externalOrderNumber>"
          + "</fsMembershipVO></accountTask>";
  	    try {
  			FTDCommonUtils.insertJMSMessage(con, payLoad, "ojms.ACCOUNT", "", 3);
  		} catch (Exception e) {
  			logger.error("Error occured while sending the emessage to the jms queue", e);
  			
  		}
      }
      if(!isOrderFreeShippingMembership(orderDetailId, con))
      {
        logger.info("Exiting handleRefundStatusModified: Not a Free Shipping Membership");
        return;
      }
      
      
      CachedResultSet rs = oDAO.getOrderCustomerInfo(orderDetailId);
      if(rs.next())
      {
         emailAddress = rs.getString("email_address");
      }
      
      if (refundDAO == null) {
    	  refundDAO = new RefundDAO(con);
      }
      
      
      if(accountBO == null) {
    	  accountBO = new AccountBO();
      }
      
      if("Y".equalsIgnoreCase(isOrderFullyRefunded))
      { 
        // Inactivate the Free Shipping Program on the Account if we need to
        logger.info("Ensuring Free Shipping Program is Inactive: " + odVO.getExternalOrderNumber());
        
        payLoad = "<accountTask type=\"FS_MEMBERSHIP_STATUS_UPDATE\"><fsMembershipVO><externalOrderNumber>"+odVO.getExternalOrderNumber()+"</externalOrderNumber>"
    	+"<emailAddress>"+emailAddress+"</emailAddress>"
        +"<membershipStatus>I</membershipStatus>"
        + "</fsMembershipVO></accountTask>";
	    try {
			FTDCommonUtils.insertJMSMessage(con, payLoad, "ojms.ACCOUNT", "", 3);
		} catch (Exception e) {
			logger.error("Error occured while sending ht emessage to the jms queue",e);
			
		}
        
      } else 
      {
        // Reactivate the Free Shipping Program on the Account if we need to
        logger.info("Ensuring Free Shipping Program is Active: " + odVO.getExternalOrderNumber());
      
        payLoad = "<accountTask type=\"FS_MEMBERSHIP_STATUS_UPDATE\"><fsMembershipVO><externalOrderNumber>"+odVO.getExternalOrderNumber()+"</externalOrderNumber>"
    	+"<emailAddress>"+emailAddress+"</emailAddress>"
        +"<membershipStatus>A</membershipStatus>"
        + "</fsMembershipVO></accountTask>";
	    try {
			FTDCommonUtils.insertJMSMessage(con, payLoad, "ojms.ACCOUNT", "", 3);
		} catch (Exception e) {
			logger.error("Error occured while sending ht emessage to the jms queue", e);
			
		}
      }
      
      logger.info("Exiting handleRefundStatusModified: Handled change in Free Shipping Product: Order Detail Id= " + orderDetailId);
    }
    
    /**
     * Checks the order to see if it is for a Free Shipping Item
     * @param orderDetailId
     * @return
     */
    protected boolean isOrderFreeShippingMembership(String orderDetailId, Connection con)
      throws Exception
    {
      if(StringUtils.isEmpty(orderDetailId))
      {
        logger.debug("Exiting isOrderFreeShippingMembershio: No Order Detail Id");
        return false; // Nothing to handle
      }    
      
      OrderDetailVO odVO = getOrderDAO(con).getOrderDetail(orderDetailId);
      
      if (servicesProgramDAO == null) {
    	  servicesProgramDAO = new ServicesProgramDAO();
      }
      // Is this a Free Shipping Product?
      return servicesProgramDAO.isProductFreeShipping(con, odVO.getProductId());
    }

	/**
	 * @param obVO
	 * @param connection
	 * @param orderDetailId
	 * @param refundPercentage
	 * @throws Exception
	 */
	private void calculateTaxes(OrderBillVO obVO, Connection connection, String orderDetailId, double refundPercentage) throws Exception {

		if (obVO.getTax() == 0 || StringUtils.isEmpty("orderDetailId")) {
			return;
		}

		// Get Current Buckets
		OrderDAO oDAO = null;
		OrderBillVO orderBillTax = null;

		try {
			oDAO = new OrderDAO(connection);
			orderBillTax = oDAO.getOrderItemRemainingTaxes(orderDetailId);
		} catch (Exception e) {
			logger.error("Error caught getting the original taxes ", e);
			return;
		}

		try {
			BigDecimal refundRate = new BigDecimal(refundPercentage);
			if (orderBillTax.getTax1Rate() != null && orderBillTax.getTax1Amount().compareTo(BigDecimal.ZERO) > 0) {
				obVO.setTax1Name(orderBillTax.getTax1Name());
				obVO.setTax1Description(orderBillTax.getTax1Description());
				obVO.setTax1Rate(orderBillTax.getTax1Rate());
				obVO.setTax1Amount(orderBillTax.getTax1Amount().multiply(refundRate).setScale(5, BigDecimal.ROUND_HALF_UP));
			}

			if (orderBillTax.getTax2Rate() != null && orderBillTax.getTax2Amount().compareTo(BigDecimal.ZERO) > 0) {
				obVO.setTax2Name(orderBillTax.getTax2Name());
				obVO.setTax2Description(orderBillTax.getTax2Description());
				obVO.setTax2Rate(orderBillTax.getTax2Rate());
				obVO.setTax2Amount(orderBillTax.getTax2Amount().multiply(refundRate).setScale(5, BigDecimal.ROUND_HALF_UP));
			}

			if (orderBillTax.getTax3Rate() != null && orderBillTax.getTax3Amount().compareTo(BigDecimal.ZERO) > 0) {
				obVO.setTax3Name(orderBillTax.getTax3Name());
				obVO.setTax3Description(orderBillTax.getTax3Description());
				obVO.setTax3Rate(orderBillTax.getTax3Rate());
				obVO.setTax3Amount(orderBillTax.getTax3Amount().multiply(refundRate).setScale(5, BigDecimal.ROUND_HALF_UP));
			}

			if (orderBillTax.getTax4Rate() != null && orderBillTax.getTax4Amount().compareTo(BigDecimal.ZERO) > 0) {
				obVO.setTax4Name(orderBillTax.getTax4Name());
				obVO.setTax4Description(orderBillTax.getTax4Description());
				obVO.setTax4Rate(orderBillTax.getTax4Rate());
				obVO.setTax4Amount(orderBillTax.getTax4Amount().multiply(refundRate).setScale(5, BigDecimal.ROUND_HALF_UP));
			}

			if (orderBillTax.getTax5Rate() != null && orderBillTax.getTax5Amount().compareTo(BigDecimal.ZERO) > 0) {
				obVO.setTax5Name(orderBillTax.getTax5Name());
				obVO.setTax5Description(orderBillTax.getTax5Description());
				obVO.setTax5Rate(orderBillTax.getTax5Rate());
				obVO.setTax5Amount(orderBillTax.getTax5Amount().multiply(refundRate).setScale(5, BigDecimal.ROUND_HALF_UP));
			}

		} catch (Exception e) {
			logger.error("Unable to calculate taxes for refund amount ", e);
		}
	}

  	/** This method is used to calculate the remaining bills on a particular payment object before making the bucket total to 0.
	 * If the order bill doesn't exist for a payment, set default values so that it is not added to the remaining bill.
	 * @param paymentList
	 * @param remainingBill
	 */
	private void calculateRemainingBills(List paymentList,  OrderBillVO remainingBill) {
        ArrayList<String> billList = new ArrayList<String>();
        PaymentVO payment = null;
        Iterator<PaymentVO> paymentIterator = paymentList.iterator();
        double msdTotal = 0;
        double addOnTotal = 0;
        double feeTotal = 0;
        double taxTotal = 0;
        while(paymentIterator.hasNext()) {
            payment = (PaymentVO) paymentIterator.next();            
            // Sum bucket totals
        	if (payment != null && payment.getOrderBill() != null && payment.getCreditAmount() > 0) {
	            if(!billList.contains(payment.getOrderBill().getOrderBillId())) {
	              	msdTotal = msdTotal + payment.getOrderBill().getProductAmount();
	            	addOnTotal = addOnTotal + payment.getOrderBill().getAddOnAmount();
	            	feeTotal = feeTotal + payment.getOrderBill().getFeeAmount();
	            	taxTotal = taxTotal + payment.getOrderBill().getTax();
	            	billList.add(payment.getOrderBill().getOrderBillId());
	            }
        	}
        }        
        remainingBill.setProductAmount(msdTotal);
        remainingBill.setAddOnAmount(addOnTotal);
        remainingBill.setFeeAmount(feeTotal);
        remainingBill.setTax(taxTotal);
        
	}

	/**
	 * This setter enables us to unit test the refundAPIBO
	 * @param refundDAO
	 */
	public void setRefundDAO(RefundDAO refundDAO) {
		this.refundDAO = refundDAO;
	}

	public void setRecalcOrderBO(RecalculateOrderBO recalcOrderBO) {
		this.recalcOrderBO = recalcOrderBO;
	}

	public void setoDAO(OrderDAO oDAO) {
		this.oDAO = oDAO;
	}
	
	public void setAccountBO(AccountBO accountBO) {
		this.accountBO = accountBO;
	}
	
	public void setServicesProgramDAO(ServicesProgramDAO servicesProgramDAO) {
		this.servicesProgramDAO = servicesProgramDAO;
	}
	
    private OrderDAO getOrderDAO(Connection con)
    {
        if (oDAO == null) {
            oDAO  = new OrderDAO(con);
        }
        return oDAO;
    }

    protected static boolean  isNewRoute(String route) {
    	try {
    		if (StringUtils.isNotBlank(route)) {
    			String[] supportedPGRoutes = null;
	    		ConfigurationUtil cu = ConfigurationUtil.getInstance();
	    		String strSupportedPGRoutes = cu.getFrpGlobalParm(OrderProcessingConstants.SERVICE, OrderProcessingConstants.PG_ROUTES);
	    		String isNewPGEnabled = cu.getFrpGlobalParm(OrderProcessingConstants.SERVICE, OrderProcessingConstants.PG_SVC_ENABLED);
	    		if ("Y".equalsIgnoreCase(isNewPGEnabled) && !StringUtils.isEmpty(strSupportedPGRoutes)) {
	    			supportedPGRoutes = StringUtils.split(strSupportedPGRoutes, ",");
	    		}
    		
				if(supportedPGRoutes != null) {
					for (String supportedRoute : supportedPGRoutes) {
						if (StringUtils.equalsIgnoreCase(supportedRoute, route)) {
							return true;
						}
					}
				}
    		}
    	} catch(Exception e) {
    		logger.error("Exception occured while checking new route", e);
    	}
    	
		return false;
	}

}
