package com.ftd.customerordermanagement.bo;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.dao.OrderDAO;
import com.ftd.customerordermanagement.vo.PaymentVO;
import com.ftd.ftdutilities.GeneralConstants;
import com.ftd.mo.exceptions.GiftCardException;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.IdGeneratorUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.id.vo.IdRequestVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.ps.webservice.AmountVO;
import com.ftd.ps.webservice.CardVO;
import com.ftd.ps.webservice.PaymentRequest;
import com.ftd.ps.webservice.PaymentResponse;
import com.ftd.ps.webservice.PaymentServiceClient;
import com.ftd.ps.webservice.PaymentServiceClientSVSImplService;


public class GiftCardBO
{
    private static Logger logger = new Logger("com.ftd.customerordermanagement.bo.GiftCardBO");
    private static final String MESSAGE_ROOT_NODE = "root";
    public static final Integer DEFAULT_ERROR_CODE = -1;
    public static final Integer ERROR_MESSAGE_2_CODE = -2;
    
    //database:
    private Connection conn = null;
    private static PaymentServiceClient psc;
    public static final String PAYSVC_CURRENCY = "USD";
    private static String paymentServiceURL = "";
    private static ConfigurationUtil configUtil;
    private IdGeneratorUtil idUtil;

    private DecimalFormat moneyFormat = new DecimalFormat("#.##");
    private Map <Integer, Integer>errorCode2MessageID = new HashMap<Integer, Integer>() {{
        put(2,    2);
        put(4,    2);
        put(5,    2);
        put(6,    2);
        put(7,    2);
        put(9,    2);
        put(10,    2);
        put(11,    2);
        put(12,    2);
        put(13,    2);
        put(14,    2);
        put(15,    2);
        put(16,    2);
        put(17,    2);
        put(18,    2);
        put(19,    2);
        put(21,    2);
        put(22,    2);
        put(23,    2);
        put(24,    2);
        put(25,    2);
        put(26,    2);
        put(27,    2);
        put(28,    2);
        put(29,    2);
        put(30,    2);
        put(31,    2);
        put(32,    2);
        put(33,    2);
        put(34,    2);
        put(35,    2);
        put(36,    2);
        put(37,    2);
        put(38,    2);
        put(42,    2);
        put(3,     3);
        put(8,     3);
        put(20,    3);
        put(DEFAULT_ERROR_CODE, 4);
        put(ERROR_MESSAGE_2_CODE, 2);
    }};

    public GiftCardBO() {
    	
    }
    

    /**
     * constructor
     * 
     * @param none
     * @return n/a
     * @throws ParserConfigurationException 
     * @throws SAXException 
     * @throws IOException 
     * @throws n/a
     */
    public GiftCardBO(Connection conn) throws Exception
    {
        super();
        this.conn = conn;
        if (configUtil ==null) {
        	configUtil = ConfigurationUtil.getInstance();
        }
        
        if (idUtil == null) {
        	idUtil = new IdGeneratorUtil();
        }
        
    }


    /** Determine if this gift card can be used for this order and if it is sufficient to cover the cost. 
     * orderGuid and orderDetailId may be null in which case it is basically a gift card balance check against 
     * the specified amount "totalOwed".
     * 
     * All these questions are answered in the returned Document.
     * 
     * @param orderGuid
     * @param newGiftCardNumber
     * @param pin
     * @param totalOwed
     * @return
     * @throws Exception
     */
    public Document validateGiftCard(String orderGuid, String orderDetailId, String newGiftCardNumber, String pin, String totalOwed)
    throws Exception
    {
        Document returnDocument = null;
        Double effectiveBalance = 0.0;
        Double currentBalance = 0.0;
        Double itemCost = 0.0;
        Double giftCardAmountOnOrder = 0.0;
        
        // make payment service call to get current balance. Then calculate effective balance, factoring portions of the gift card used on this order.

        returnDocument = DOMUtil.getDefaultDocument();

        currentBalance = getCurrentBalance(returnDocument, newGiftCardNumber, pin, totalOwed);
        if (currentBalance == -1)
        {
            return returnDocument;
        }
        
        // item cost is needed to figure out how much of the gift card will be refunded when  this order is updated
        itemCost = getItemCost(orderDetailId);        
        giftCardAmountOnOrder = getGiftCardAmountOnOrderAvailable(orderGuid, newGiftCardNumber, itemCost);

        effectiveBalance = currentBalance + giftCardAmountOnOrder;

        logger.debug("totalOwed:" + totalOwed);
        logger.debug("GiftCard Real Balance:" + moneyFormat.format(currentBalance));
        logger.debug("GiftCard Effective Balance:" + moneyFormat.format(effectiveBalance));

        BigDecimal tOwed = new BigDecimal(totalOwed);
        BigDecimal effBal = new BigDecimal(moneyFormat.format(effectiveBalance));
        BigDecimal netOwed = tOwed.subtract(effBal);
        logger.debug("Net owed:" + moneyFormat.format(netOwed));

        boolean amountOwed = false;
        if(netOwed.compareTo(new BigDecimal(0)) > 0)
        {
            amountOwed = true;
        }

        returnDocument = this.createValidGiftCardXML(newGiftCardNumber, moneyFormat.format(effectiveBalance), amountOwed, netOwed.toString());      

        return returnDocument;
    }
    
    private Double getItemCost(String orderDetailId) throws Exception
    {
        Double orderTotal = -1.0;
        OrderDAO dao = new OrderDAO(conn);
        CachedResultSet outputs = dao.getOrderTotals(orderDetailId);
        while (outputs.next())
        {
            orderTotal = outputs.getDouble("order_total");
        }
        // in case no order totals were found - this is an error case.
        return orderTotal; 
    }


    public double getGiftCardAmountOnOrderAvailable(String orderGuid, String giftCardNumber, Double itemCost) throws Exception {
		List<PaymentVO> payments;
		OrderDAO odao = new OrderDAO(conn);		
		payments = odao.getGiftCardPayments(orderGuid);		

		boolean itemRemovedEODRun = false;
    	double totalGDCharged = 0;
    	/* To figure how much this gift card has been used on this order that will be refunded if this order processes
    	 * subtract refunds
    	 * add payments
    	 */
    	if (payments != null)
    	{
    		for (PaymentVO vo : payments)
    		{
    			if (vo.getCardNumber().equals(giftCardNumber))
    			{
    				if (vo.getPaymentInd().equals("R"))
    				{
    					totalGDCharged -= vo.getDebitAmount();
    				}
    				else if (vo.getPaymentInd().equals("P"))
    				{
    				    /* have to figure out if we should use the clean or scrub amount. If the payment has gone through 
    				     * EOD, use the clean. If not, there is a change this cart has a removed order which means that 
    				     * the clean amount might not reflect the SVS auth amount. Scrub will have that so use scrub.
    				     * If the scrub amount (getAmount()) is 0 then it means there is no scrub record either due to purge
    				     * or an update of an updated order. If so, use the clean amount (creditAmount).
    				     */
    				    if (StringUtils.equals(vo.getBillStatus(), "Billed") || vo.getAmount() == 0)
    				    {
                            totalGDCharged += vo.getCreditAmount();
    				    }
    				    else
    				    {
                            totalGDCharged += vo.getAmount();
    				    }
    				    
                        // EOD has run and the scrub amt != clean amt then we need to ignore the current item cost
                        if (vo.getAmount() != vo.getCreditAmount())
                        {
                            itemRemovedEODRun = true;
                        }
    				}
    			}
    		}
    	}
    	
        // the amount that will be refunded to the gift card is the lesser of the item cost and the amount of the gift card 
        // applied to the order (i.e. the gift card payment - any gift card refunds for this order), unless there is a removed item in the cart
        // and EOD has not run.
    	if (!itemRemovedEODRun)
    	{
    	    totalGDCharged = Math.min(itemCost, totalGDCharged);
    	}
    	    	        
        return Double.valueOf(moneyFormat.format(totalGDCharged));
	}


    // public for easier testing
    public Double getCurrentBalance(Document errorDocument, String giftCardNumber, String pin, String totalOwed) throws Exception
    {
    	logger.info("Getting current gift card balance");
        Double balance=0.0;
        PaymentRequest reqvo = getAuthenticatedPaymentRequest();
        reqvo.setAmountVO(new AmountVO() {{setAmount(0.0);}});
        String defaultError = getErrorMessage(DEFAULT_ERROR_CODE.toString());

        try
        {
            initSvc(reqvo, giftCardNumber, pin);
        }
        catch (Exception e)
        {
            logger.error("Unable to initialize payment service: " + e.getStackTrace());
            // service not available
            errorDocument = this.createInvalidGiftCardXML(errorDocument, defaultError, totalOwed);
            balance = -1.0;
            return Double.valueOf(moneyFormat.format(balance));
        }
        
        PaymentResponse respvo = psc.getBalance(reqvo);
        
        if(respvo == null)
        {
        	logger.error("Error getting balance, null returned");
            errorDocument = this.createInvalidGiftCardXML(errorDocument, defaultError, totalOwed);
            balance = -1.0;
        }
        else if(respvo.getErrorCode() != null) // check for specific errors and map to error messages.
        {
        	logger.error("Error getting balance, error code = " + respvo.getErrorCode() + ", response code=" + respvo.getResponseCode() +
        			", message = " + respvo.getResponseMessage());
            String error = getErrorMessage(respvo.getResponseCode());
            errorDocument = this.createInvalidGiftCardXML(errorDocument, error, totalOwed);
            balance = -1.0;
        }
        else
        {
            balance = respvo.getBalanceAmountVO().getAmount();
        }
  	  
        return Double.valueOf(moneyFormat.format(balance));
    }


    public String getErrorMessage(String errorCode) throws Exception
    {
        // map error codes to configured messages
        Integer messageID = errorCode2MessageID.get(Integer.valueOf(errorCode));        
        String errorMessage = configUtil.getProperty(COMConstants.PROPERTY_FILE, "payment_service_error_"+messageID);

        // make sure error code was configured
        if (errorMessage == null || errorMessage.equals(""))
        {
        	messageID = errorCode2MessageID.get(DEFAULT_ERROR_CODE);            
            errorMessage = configUtil.getProperty(COMConstants.PROPERTY_FILE, "payment_service_error_"+messageID);        	
        }
        return errorMessage;
    }


    public static PaymentServiceClient getPaymentServiceClient() throws Exception 
    {
	 // Get handle to payment web service
    	if (configUtil == null) {
    		configUtil = ConfigurationUtil.getInstance();
    	}
        String url = configUtil.getFrpGlobalParm("SERVICE", "PAYMENT_SERVICE_URL");
        if (psc == null || (!url.equalsIgnoreCase(paymentServiceURL))) {
          try {
            URL psURL = new URL(url);
            PaymentServiceClientSVSImplService ps = new PaymentServiceClientSVSImplService(psURL);
            psc = ps.getPaymentServiceClientSVSImplPort();
            paymentServiceURL = url;
          } catch (MalformedURLException e) {
            logger.error("Error when getting GiftCard payment service client: " + e.getMessage());
            e.printStackTrace();
            psc = null;
          }
        }
        return psc;
    }

    private void initSvc(PaymentRequest reqvo, String cardNumber, String pin) throws Exception 
    {
        if (reqvo.getTransactionId() == null) {
            reqvo.setTransactionId(idUtil.generateId(new IdRequestVO("APOLLO", "SVS_TRANS_ID"), conn));
        }
        
        CardVO cardvo = new CardVO();
        cardvo.setCardCurrency(PAYSVC_CURRENCY);
        cardvo.setCardNumber(cardNumber);
        cardvo.setPinNumber(pin);
        reqvo.setCardVO(cardvo);
        
        AmountVO amountvo = new AmountVO();
        amountvo.setAmount(new Double(0));
        amountvo.setCurrency(PAYSVC_CURRENCY);
        reqvo.setAmountVO(amountvo);

        getPaymentServiceClient();
    }


    /**
     * create an invalid xml document
     * @param errorMessage
     * @return Document - invalid document text
     * @throws Exception
     * @throws IOException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws SQLException
     * @throws TransformerException
     * @throws XSLException
     */  
    private Document createInvalidGiftCardXML(Document errorDocument, String errorMessage, String totalOwed)
    throws IOException, ParserConfigurationException, SAXException, SQLException,
    TransformerException, Exception
    {
        //Call the ConfigurationUtil utility to obtain the credit card declined message from customer_order_management_config.xml

        String ccMessage = null;

        if (errorMessage == null)
        {
            ccMessage = (String) configUtil.getProperty(COMConstants.PROPERTY_FILE,COMConstants.GCC_INVALID_MSG);
        }
        else if (errorMessage.equalsIgnoreCase(null))
        {
            ccMessage = (String) configUtil.getProperty(COMConstants.PROPERTY_FILE,COMConstants.GCC_INVALID_MSG);
        }        
        else
        {
            ccMessage = errorMessage;  
        }

        Element root = (Element) errorDocument.createElement(MESSAGE_ROOT_NODE);
        errorDocument.appendChild(root);

        Element node;
        node = (Element)errorDocument.createElement("gcc_status");
        node.appendChild(errorDocument.createTextNode("invalid"));
        root.appendChild(node);

        node = (Element)errorDocument.createElement("total_owed");
        node.appendChild(errorDocument.createTextNode(totalOwed));
        root.appendChild(node);

        node = (Element)errorDocument.createElement("message");
        node.appendChild(errorDocument.createTextNode(ccMessage));
        root.appendChild(node);

        node = (Element)errorDocument.createElement("method_to_invoke");
        node.appendChild(errorDocument.createTextNode(null));
        root.appendChild(node);

        return errorDocument;  
    }


    /**
     * create a valid xml document
     * @param status
     * @return Document - invalid document text
     * @throws Exception
     * @throws IOException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws SQLException
     * @throws TransformerException
     * @throws XSLException
     */    
    private Document createValidGiftCardXML(String giftCardNumber, String amount, boolean amountOwed, String netOwed)
    throws IOException, ParserConfigurationException, SAXException, SQLException,
    TransformerException, Exception
    {
        Document returnDocument = DOMUtil.getDefaultDocument();
        Element root = (Element) returnDocument.createElement(MESSAGE_ROOT_NODE);
        returnDocument.appendChild(root);

        Element node;
        node = (Element)returnDocument.createElement("gcc_status");
        node.appendChild(returnDocument.createTextNode("valid"));
        root.appendChild(node);

        node = (Element)returnDocument.createElement("amount_owed");
        node.appendChild(returnDocument.createTextNode(String.valueOf(amountOwed)));
        root.appendChild(node);

        node = (Element)returnDocument.createElement("amount");
        node.appendChild(returnDocument.createTextNode(amount)); // value of gift card
        root.appendChild(node);

        node = (Element)returnDocument.createElement("net_owed");
        node.appendChild(returnDocument.createTextNode(netOwed));
        root.appendChild(node);

        node = (Element)returnDocument.createElement("gift_number");
        node.appendChild(returnDocument.createTextNode(giftCardNumber));
        root.appendChild(node);

        node = (Element)returnDocument.createElement("method_to_invoke");
        node.appendChild(returnDocument.createTextNode(null)); //// is this node needed?
        root.appendChild(node);
        

        return returnDocument;  
    }
    
    /**
     * Method to pre-populate the PaymentRequest object with username and password credentials.
     * This is the object that gets passed in the payment service web service call
     * Ultimately it will become a PRIVATE method since only methods within this class should be using it
     * 
     * @return
     * @throws IOException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws SQLException
     * @throws Exception
     */
    private PaymentRequest getAuthenticatedPaymentRequest() throws IOException, ParserConfigurationException, SAXException, SQLException, Exception 
    {
		PaymentRequest req = new PaymentRequest();
		req.setClientUserName(configUtil.getSecureProperty("SERVICE", "SVS_CLIENT"));
		req.setClientPassword(configUtil.getSecureProperty("SERVICE", "SVS_HASHCODE"));
		  
		return req;
    }
    
    /**
     * Method to settle a Gift Card payment.
     * Invokes the Payment Service which will perform the settle against the gift card provider
     * Returns true on success.
     * 
     * @param payment - the GC payment to be refunded
     * @return If there was an error then the PaymentVO will have it's *errors* list populated.
     * @throws Exception
     */
    public void settlePayment(PaymentVO payment) throws GiftCardException, Exception 
    {    	
    	
    	if (payment != null && payment.getBillStatus() != null && !payment.getBillStatus().equals("Unbilled")) {
    		throw new Exception("Expecting an 'Unbilled' bill status in order to perform a settle");
    	}
    	logger.debug("settling payment ID: " + payment.getPaymentId());
		PaymentRequest req = getAuthenticatedPaymentRequest();
		req.setTransactionId(payment.getTransactionId());
        initSvc(req, payment.getCardNumber(), payment.getGiftCardPin());
        

		CardVO card = new CardVO();
		card.setCardCurrency(PAYSVC_CURRENCY);
		card.setCardNumber(payment.getCardNumber());
		card.setPinNumber(payment.getGiftCardPin());
		req.setCardVO(card);
		
		AmountVO amountVO = new AmountVO();
		amountVO.setAmount(payment.getCreditAmount());
		amountVO.setCurrency(PAYSVC_CURRENCY);
		req.setAmountVO(amountVO);
		
		PaymentResponse resp = psc.settle(req);
		
		if (resp!= null) {
    		String respMsgs = "GiftCard Settle Service error code/message: " + resp.getErrorCode() + "/" + resp.getErrorMessage() +
            " --- SVS response code/message: " + resp.getResponseCode() + "/" + resp.getResponseMessage();
    		logger.info(respMsgs);
			if (!resp.getResponseCode().equals("01")) {
				logger.error(respMsgs);
				throw new GiftCardException(getErrorMessage(resp.getResponseCode()));
			}
			
		} else {
			throw new Exception("NULL response object detected from payment service SETTLE call");
		}
    }
    
    /**
     * Method to refund a Gift Card Payment
     * Invokes the payment Service which will perform the refund against the gift card provider
     * Returns true on success.
     * 
     * @param payment - the GC payment to be refunded
     * @return if there were error then the Payment object will have its *errors* list populated
     * @throws Exception
     */
    public void refundPayment(PaymentVO payment) throws GiftCardException, Exception
    {    	
    	logger.debug("refunding payment ID: "+ payment.getPaymentId());
    	
    	PaymentRequest req = getAuthenticatedPaymentRequest();
    	req.setTransactionId(null); // ensure that we get a new txid
        initSvc(req, payment.getCardNumber(), payment.getGiftCardPin());
    	
    	req.getAmountVO().setAmount(payment.getCreditAmount());
    	
    	PaymentResponse resp = psc.refund(req);
    	
    	if (resp != null) {
    		String respMsgs = "GiftCard Refund Service error code/message: " + resp.getErrorCode() + "/" + resp.getErrorMessage() +
            " --- SVS response code/message: " + resp.getResponseCode() + "/" + resp.getResponseMessage();
			logger.info(respMsgs);
			
			if (!resp.getResponseCode().equals("01")) {
				logger.error(respMsgs);
				throw new GiftCardException(getErrorMessage(DEFAULT_ERROR_CODE.toString()));
			}
    	}
    	
    	return;
    }
    
    public void authorizePayment(PaymentVO payment) throws GiftCardException, Exception
    {
        logger.info("Authorizing payment ID: "+ payment.getPaymentId());
    	PaymentRequest req = getAuthenticatedPaymentRequest();
    	req.setTransactionId(payment.getTransactionId());  //try to set the transaction ID.
        initSvc(req, payment.getCardNumber(), payment.getGiftCardPin());
    	
    	// save transaction ID in the payment            	
        String txId = req.getTransactionId();        
    	payment.setTransactionId(txId);
    	payment.setAuthorization(txId);
    	req.getAmountVO().setAmount(payment.getCreditAmount());
    	
    	PaymentResponse resp = psc.checkBalanceAndAuthorizeForExactAmount(req);
    	
    	if (resp != null) {
    		String respMsgs = "GiftCard Settle Service error code/message: " + resp.getErrorCode() + "/" + resp.getErrorMessage() +
            " --- SVS response code/message: " + resp.getResponseCode() + "/" + resp.getResponseMessage();
			logger.info(respMsgs);
			
			if (!resp.getResponseCode().equals("01")) {
				logger.error(respMsgs);
				throw new GiftCardException(getErrorMessage(resp.getResponseCode()));
			}
    	}
    }

    
    /**
     * method to facilitate TESTING of this class
     * @param configUtil
     */
    public void setConfigurationUtil(ConfigurationUtil configUtil) {
    	this.configUtil = configUtil;
    }
    
    /**
     * method to facilitate TESTING of this class
     * @param idUtil
     */
    public void setIdGeneratorUtil(IdGeneratorUtil idUtil) {
    	this.idUtil = idUtil;
    }
    
    /**
     * for testing
     * @param connection
     */
    public void setConnection(Connection connection) {
    	this.conn = connection;
    }


    public static String getUnmaskedGiftCardNum(String gccnum, String orderGuid, Connection conn) throws Exception
    {
        //String isOrigGD = request.getParameter("is_orig_gd");
        if (gccnum.startsWith("***"))
        {
            logger.info("Reusing gift card - getting original gift card number.");
            // get gcc num from payment records
            OrderDAO dao = new OrderDAO(conn);           
            List <PaymentVO> payments = dao.getGiftCardPayments(orderGuid);
            if (payments != null && !payments.isEmpty())
            {
                return payments.get(0).getCardNumber();
            }
            throw new Exception("Unable to get last gift card used for this order.");
        }
        else
        {
            logger.info("Using (possibly) new gift card.");
            return gccnum;
        }
    }
    
    /**
     * Find the gift card "global switch", i.e. if gift card payment is enabled, and return it in a document as
     * GIFTCARD_SETTINGS/gift_card_payment_enabled
     * 
     * @param conn
     * @return
     * @throws Exception
     */
    public static Document getGiftCardGlobalSwitch(Connection conn) throws Exception
    {
        String giftCardEnabled = ConfigurationUtil.getInstance().getFrpGlobalParm(GeneralConstants.GIFT_CARD_CONTEXT,
                GeneralConstants.GIFT_CARD_SWITCH);
        Document doc = DOMUtil.getDefaultDocument();
        /**
         * add to GIFTCARD_SETTINGS/gift_card_payment_enabled
         */
        Element root = (Element) doc.createElement("GIFTCARD_SETTINGS");
        doc.appendChild(root);


        Element node;
        node = (Element)doc.createElement("gift_card_payment_enabled");
        node.appendChild(doc.createTextNode(giftCardEnabled));
        root.appendChild(node);

        return doc;
    }

    
}