package com.ftd.customerordermanagement.bo;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.ftd.customerordermanagement.dao.OrderDAO;
import com.ftd.customerordermanagement.dao.PaymentDAO;
import com.ftd.customerordermanagement.vo.PaymentMethodVO;
import com.ftd.customerordermanagement.vo.SourceCodeVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;

/**
 * This class handles payment method business logic.
 *
 * @author Mike Kruger
 */

public class PaymentMethodBO 
{
  private Logger logger = new Logger("com.ftd.customerordermanagement.bo.PaymentMethodBO");

  private Connection conn = null;
  private static final String AMERICAN_EXPRESS = "AX";
  private static final String DINERS_CLUB = "DC";
  private static final String DISCOVER = "DI";
  private static final String INVOICE = "IN";
  private static final String MASTERCARD = "MC";
  private static final String NO_CHARGE = "NC";
  private static final String VISA = "VI";
  private static final String MILITARY_STAR = "MS";
  private static final String GIFT_CERTIFICATE = "GC";
  private static final String AAFES = "AAFES";
  private static final String CORPORATE_PURCHASE = "PC";

	/**
	* constructor
	* 
	* @param Connection - database connection
	* @return n/a
	* @throws n/a
	*/
	public PaymentMethodBO(Connection conn)
	{
		super();
		this.conn = conn;
	}
  
	/**
	* This method contains logic to return all valid payment methods for 
	* a given source code.
	* 
	* @param sourceCode - The source code to build payment types from
	* @param cartLevel - true if the payment is for a cart
	* @return Document - Payment methods as XML
	* @throws Exception
	*/
	public Document getPaymentMethods(String sourceCode, boolean cartLevel)
		throws Exception
	{
		PaymentMethodVO pmVO;
		
		//  Holds all valid payment methods
		ArrayList validPaymentMethods = new ArrayList();
		
		//  Holds all payment methods
		HashMap paymentMethodsMap = new HashMap();
		
		//  Obtain source code information
		OrderDAO orderDAO = new OrderDAO(conn);
		SourceCodeVO scVO = orderDAO.getSourceCodeRecord(sourceCode);

		//  Obtain billing info gift certificate flag
		boolean biGcFlag = orderDAO.getBillingInfoForCertificate(sourceCode);
		
		//  Obtain all payment methods
		PaymentDAO paymentDAO = new PaymentDAO(conn);
		ArrayList paymentMethods = paymentDAO.getAllPaymentMethod();
		for(Iterator it = paymentMethods.iterator(); it.hasNext();)
		{
			pmVO = (PaymentMethodVO)it.next();
			paymentMethodsMap.put(pmVO.getpaymentMethodId().toUpperCase(), pmVO);
		}
		
		/* Determine which payment types are needed based on valid pay method,
		 * 	source type and billing info
		 */ 
		if(scVO.getValidPayMethod() == null 
			|| scVO.getValidPayMethod().equalsIgnoreCase("")
			|| scVO.getValidPayMethod().equalsIgnoreCase(NO_CHARGE))
		{
			validPaymentMethods.add(paymentMethodsMap.get(AMERICAN_EXPRESS));
			validPaymentMethods.add(paymentMethodsMap.get(DINERS_CLUB));
			validPaymentMethods.add(paymentMethodsMap.get(DISCOVER));
			validPaymentMethods.add(paymentMethodsMap.get(MASTERCARD));
			validPaymentMethods.add(paymentMethodsMap.get(NO_CHARGE));
			validPaymentMethods.add(paymentMethodsMap.get(VISA));			
			
			if(scVO.getMarketingGroup() != null && scVO.getMarketingGroup().equalsIgnoreCase(AAFES))
				validPaymentMethods.add(paymentMethodsMap.get(MILITARY_STAR));	
		}
		else if(scVO.getValidPayMethod().equalsIgnoreCase(GIFT_CERTIFICATE))
		{
			validPaymentMethods.add(paymentMethodsMap.get(AMERICAN_EXPRESS));
			validPaymentMethods.add(paymentMethodsMap.get(DINERS_CLUB));
			validPaymentMethods.add(paymentMethodsMap.get(DISCOVER));
			validPaymentMethods.add(paymentMethodsMap.get(MASTERCARD));
			validPaymentMethods.add(paymentMethodsMap.get(NO_CHARGE));
			validPaymentMethods.add(paymentMethodsMap.get(VISA));			
			
			if(scVO.getMarketingGroup() != null && scVO.getMarketingGroup().equalsIgnoreCase(AAFES))
				validPaymentMethods.add(paymentMethodsMap.get(MILITARY_STAR));	

			if(!cartLevel)
				validPaymentMethods.add(paymentMethodsMap.get(GIFT_CERTIFICATE));
		}
		else if(scVO.getValidPayMethod().equalsIgnoreCase(CORPORATE_PURCHASE))
		{
			validPaymentMethods.add(paymentMethodsMap.get(AMERICAN_EXPRESS));
			validPaymentMethods.add(paymentMethodsMap.get(DINERS_CLUB));
			validPaymentMethods.add(paymentMethodsMap.get(DISCOVER));
			validPaymentMethods.add(paymentMethodsMap.get(MASTERCARD));
			validPaymentMethods.add(paymentMethodsMap.get(VISA));			
		}
		else
		{
			validPaymentMethods.add(paymentMethodsMap.get(scVO.getValidPayMethod().toUpperCase()));			
		}
		
		/*  If the payment is not for a cart and the billing info gift 
		 * certificate flag is true
		 */
		if(!cartLevel && biGcFlag)
			validPaymentMethods.add(paymentMethodsMap.get(GIFT_CERTIFICATE));
		
		//  Create payment methods document
		Document paymentMethodDocument = DOMUtil.getDefaultDocument();
        Element root = (Element) paymentMethodDocument.createElement("PAYMENT_METHODS");
        paymentMethodDocument.appendChild(root);
		
		for(Iterator it = validPaymentMethods.iterator(); it.hasNext();)
		{
			Document payMethodDoc = DOMUtil.getDocument(((PaymentMethodVO)it.next()).toXML());
			paymentMethodDocument.getDocumentElement().appendChild(paymentMethodDocument.importNode(payMethodDoc.getFirstChild(), true));			
		}

		
		return paymentMethodDocument;
	}
	
		/**
	* This method contains logic to return a specific payment method
	* 
	* @param payMethod - Obtain the payment method
	* @return Document - Payment methods as XML
	* @throws Exception
	*/
	public Document getPaymentMethod(String payMethod)
		throws Exception
	{
		PaymentMethodVO pmVO;
		
		//  Holds all valid payment methods
		ArrayList validPaymentMethods = new ArrayList();
		
		//  Obtain payment method requested
		PaymentDAO paymentDAO = new PaymentDAO(conn);
		ArrayList paymentMethods = paymentDAO.getAllPaymentMethod();
		for(Iterator it = paymentMethods.iterator(); it.hasNext();)
		{
			pmVO = (PaymentMethodVO)it.next();
			if(pmVO.getpaymentMethodId().equalsIgnoreCase(payMethod))
			{
				validPaymentMethods.add(pmVO);
				break;
			}
		}

		//  Create payment methods document
		Document paymentMethodDocument = DOMUtil.getDefaultDocument();
        Element root = (Element) paymentMethodDocument.createElement("PAYMENT_METHODS");
        paymentMethodDocument.appendChild(root);
		
		
		for(Iterator it = validPaymentMethods.iterator(); it.hasNext();)
		{
			Document paymentDoc = DOMUtil.getDocument(((PaymentMethodVO)it.next()).toXML());
			paymentMethodDocument.getDocumentElement().appendChild(paymentMethodDocument.importNode(paymentDoc.getFirstChild(), true));			
		}

		if(logger.isDebugEnabled())
		{
		  //IMPORTANT:  Because StringWriter contents are NOT removed even when the flush
		  //            method is executed, make StringWriter go away through scope control.
		  //            Both the flush and close methods were tested with but were of none affect.
		  StringWriter sw = new StringWriter();       //string representation of xml document
		  DOMUtil.print(paymentMethodDocument, new PrintWriter(sw));
		  logger.debug("- execute(): COM returned = \n" + sw.toString());
		}

		return paymentMethodDocument;
	}
}