package com.ftd.customerordermanagement.bo;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.dao.OrderDAO;
import com.ftd.customerordermanagement.dao.RefundDAO;
import com.ftd.customerordermanagement.dao.CommentDAO;
import com.ftd.customerordermanagement.dao.CustomerDAO;
import com.ftd.customerordermanagement.dao.LossPreventionDAO;
import com.ftd.osp.utilities.order.RecalculateOrderBO;
import com.ftd.customerordermanagement.util.PaymentComparator;
import com.ftd.customerordermanagement.util.TimerFilter;
import com.ftd.customerordermanagement.vo.BillingVO;
import com.ftd.customerordermanagement.vo.CommonCreditCardVO;
import com.ftd.customerordermanagement.vo.CreditCardVO;
import com.ftd.customerordermanagement.vo.CustomerVO;
import com.ftd.customerordermanagement.vo.GcCouponVO;
import com.ftd.customerordermanagement.vo.GiftCardVO;
import com.ftd.customerordermanagement.vo.OrderBillVO;
import com.ftd.customerordermanagement.vo.PaymentVO;
import com.ftd.customerordermanagement.vo.RefundTotalVO;
import com.ftd.customerordermanagement.vo.RefundVO;
import com.ftd.customerordermanagement.vo.CommentsVO;
import com.ftd.customerordermanagement.vo.OrderDetailVO;
import com.ftd.customerordermanagement.vo.OrderVO;
import com.ftd.ftdutilities.FTDCommonUtils;
import com.ftd.ftdutilities.FieldUtils;
import com.ftd.messaging.util.MessagingServiceLocator;
import com.ftd.op.mercury.to.ASKMessageTO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.GiftCodeUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.UpdateGiftCodeRequest;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.security.SecurityManager;
import com.ftd.security.cache.vo.UserInfo;
import com.ftd.messaging.constants.MessagingConstants;
import com.ftd.messaging.dao.MessageDAO;
import com.ftd.messaging.util.MessageUtil;
import com.ftd.messaging.vo.MessageVO;
import com.ftd.mo.dao.UpdateOrderDAO;
import com.ftd.op.mercury.to.ADJMessageTO;
import com.ftd.op.mercury.to.BaseMercuryMessageTO;
import com.ftd.op.mercury.to.CANMessageTO;
import com.ftd.op.venus.to.AdjustmentMessageTO;
import com.ftd.op.venus.to.CancelOrderTO;
import com.ftd.op.common.constants.OrderProcessingConstants;
import com.ftd.op.common.to.ResultTO;
import com.ftd.osp.account.bo.AccountBO;
//import com.ftd.osp.utilities.cache.handlers.vo.PartnerVO;
import com.ftd.osp.utilities.order.vo.PaymentMethodMilesPointsVO;
import com.ftd.osp.utilities.order.vo.TaxVO;

import java.io.IOException;
import java.io.StringReader;
import java.lang.Double;
import java.math.BigDecimal;
import java.sql.Connection;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.naming.InitialContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.UserTransaction;



import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;


/**
 * RefundBO class
 * 
 * @author Brian Munter
 */

public class RefundBO 
{
    private static Logger logger = new Logger("com.ftd.customerordermanagement.bo.RefundBO");
    private static String GC_REFUND_STATUS  = "Refund";
    private static String GC_REDEEMED_STATUS  = "Redeemed";
    private static final String LOSS_PREVENTION_QUEUE = "LP";
    private static final String CREDIT_QUEUE = "CREDIT";
    private static final String GIFT_CARD_PAYMENT_TYPE = "GD";
    
    private HttpServletRequest request = null; 
    private Connection con = null;
    private String securityToken = null;
    private String context = null;
    private String userId = null;
    private String aTypeFlag = "N";
    
    private RefundTotalVO refundTotal = new RefundTotalVO();

    private String refundApplied  = "N";
    private double serviceFee = 0;
    private double sameDayUpcharge = 0;
    private double shippingFee = 0;
    private double morningDeliveryFee = 0;
    
    private double fuelSurchargeAmount = 0;
    private String fuelSurchargeDescription = "";
    private String applySurchargeCode = "";
    
    private double saturdayUpcharge = 0;
    private double alsHawaiiUpcharge = 0;
    

    
    private double paymentMsdTotal = 0;
    private double paymentAddOnTotal = 0;
    private double paymentFeeTotal = 0;
    private double paymentTaxTotal = 0;
    private int paymentMilesPointsTotal = 0;
    private String milesPointsFlag = "N";
    
    private RefundAPIBO refundApiBO = new RefundAPIBO();
    
    //DI-27: Refund - Show new fees broken out in Mouseovers
    private double sundayUpcharge = 0;
    private double mondayUpcharge = 0;
    private double lateCutoffFee = 0;
    private double internationalFee = 0;
    
    
    public RefundBO(HttpServletRequest request, Connection con)
    {
        this.request = request;
        this.con = con;
        this.context = (String) request.getParameter(COMConstants.CONTEXT);
        this.securityToken = (String) request.getParameter(COMConstants.SEC_TOKEN);
        try {
			this.userId = this.getUserId();
		} catch (Exception e) {
			logger.error("Could not determine userID for refund request");
		}
    }
    
    public RefundBO(Connection con)
    {
        this.con = con;
    }
    
    public RefundBO(Connection con, String userId)
    {
        this.con = con;
        this.userId = userId;
    }
		
		
    /**
     * method to be invoked from a UI to do refunds
     * 
     * @param responseDocument
     * @param orderDetailId
     * @return
     * @throws Exception
     */
    public Document loadRefund(Document responseDocument, String orderDetailId, boolean isNewRoute) throws Exception
    {    
        // Initialize input amounts
        double msdAmount = 0;
        double addOnAmount = 0;
        double feeAmount = 0;
        double taxAmount = 0;
        String gcFlag = "N";
        int paymentSequence = 1;
        boolean C30RefundFound = false;
        boolean isGDPaymentExists = false;
        
        List<RefundVO> refundList = null;
        HashMap refundMap = null;
        HashMap paymentMap = null;
        HashMap paymentTypeMap = null;
        Iterator paymentTypeIterator = null;
        Iterator paymentIterator = null;
        PaymentVO payment = null;
        PaymentVO previousPayment = null;
        
        HashMap primaryPaymentMap = new HashMap();
        
        // Pull list of payments and refunds from database
        RefundDAO refundDAO = new RefundDAO(con);
        HashMap paymentRefundMap = refundDAO.loadPaymentRefund(orderDetailId);
        
        Map billPaymentMap = (Map) paymentRefundMap.get(RefundDAO.PAYMENT_LIST);
        
        Document paymentsListDocument = (Document) DOMUtil.getDefaultDocument();
		Element paymentsListElement = paymentsListDocument.createElement("PaymentsList");
        paymentsListDocument.appendChild(paymentsListElement);
        List<PaymentVO> individualPaymentsList = new ArrayList<PaymentVO>();
        
        Iterator billPaymentIterator = billPaymentMap.keySet().iterator();
        int paymentSeqId = 0;
        
        while(billPaymentIterator.hasNext())
        {
            String billKey = (String) billPaymentIterator.next();
            List paymentList = (List) billPaymentMap.get(billKey);
        
            paymentMsdTotal = 0;
            paymentAddOnTotal = 0;
            paymentFeeTotal = 0;
            paymentTaxTotal = 0;
            paymentMilesPointsTotal = 0;
            milesPointsFlag = "N";
                        
            refundList = (List) paymentRefundMap.get(RefundDAO.REFUND_LIST);
            if (refundList.size() > 0)
            {
              refundApplied = "Y";
            }
         
            //check all the refund records, associated with each payment type.  If any of the payment
            //shows a refund of C30, set the flag.  
            if (!C30RefundFound)
            {
              int totalRefunds = refundList.size();
              for (int i = 0; i < totalRefunds && !C30RefundFound; i++)
              {
                RefundVO C30RefundCheck = (RefundVO) refundList.get(i); 
                if (C30RefundCheck.getRefundDispCode().equalsIgnoreCase("C30"))
                  C30RefundFound = true; 
              }
            }
            
            // Consolidate all refunds
            refundMap = RefundAPIBO.consolidateRefunds(refundList, securityToken, context, this.userId,this.con);
            
            // Consolidate all payments within the order bill
            if(isNewRoute) {
            	paymentMap = RefundAPIBO.buildIndividualPayments(paymentList, refundMap, individualPaymentsList);
            }
            paymentMap = RefundAPIBO.consolidatePayments(paymentList, refundMap);
            //Sum bucket totals
            sumBucketTotals(paymentList);
            
            // If payments contain a gift card, use the gift card payment amount
            // to fill buckets intially before using any other payment type
            if(paymentMap.containsKey("G") || paymentMap.containsKey("R"))
            {  
            	gcFlag = "Y";
                HashMap consolidatedPaymentMap = null;
                PaymentVO consolidatedPayment = null;
                Iterator consolidatedPaymentIterator = null;
            
                // Create a bill to maintain remaining balances
                OrderBillVO remainingBill = new OrderBillVO();
                remainingBill.setProductAmount(paymentMsdTotal);
                remainingBill.setAddOnAmount(paymentAddOnTotal);
                remainingBill.setFeeAmount(paymentFeeTotal);
                remainingBill.setTax(paymentTaxTotal);
                
            
                // Iterate through all gift cards on the order and apply to buckets
                if(paymentMap.containsKey("G")) {                	
                	consolidatedPaymentMap = (HashMap) paymentMap.get("G");
                	RefundAPIBO.distributeBuckets(consolidatedPaymentMap, remainingBill);
                }
                
                if(paymentMap.containsKey("R")) {
                	consolidatedPaymentMap = (HashMap) paymentMap.get("R");
                	RefundAPIBO.distributeBuckets(consolidatedPaymentMap, remainingBill);
                }
                                
                // Iterate through remaining PayPal payments on the order(if exist) and apply to buckets
                if(paymentMap.containsKey("P"))
                {
                    consolidatedPaymentMap = (HashMap) paymentMap.get("P");
                    RefundAPIBO.distributeBuckets(consolidatedPaymentMap, remainingBill); 
                }

                // Iterate through remaining credit cards on the order(if exist) and apply to buckets
                if(paymentMap.containsKey("C"))
                {
                    consolidatedPaymentMap = (HashMap) paymentMap.get("C");
                    RefundAPIBO.distributeBuckets(consolidatedPaymentMap, remainingBill); 
                }
            }
            
            // Use Case - 22842 - non c30 refunds for GiftCard Payments.
            if(isGDPaymentExists(paymentList)) {
           		isGDPaymentExists = true;
           	}

            
            // Apply refunds against payments
            RefundAPIBO.applyRefunds(paymentMap, refundMap, primaryPaymentMap, this.refundTotal);
        
            // Consolidate all payments from all order bills on order
            paymentIterator = paymentMap.keySet().iterator();
            while(paymentIterator.hasNext())
            {
                paymentTypeMap = (HashMap) paymentMap.get((String) paymentIterator.next());
                
                paymentTypeIterator = paymentTypeMap.keySet().iterator();
                while(paymentTypeIterator.hasNext())
                {
                    payment = (PaymentVO) paymentTypeMap.get((String) paymentTypeIterator.next());
                    
                    if(payment != null)
                    {
                        String paymentIdentifier = RefundAPIBO.generateIdentifier(payment.getPaymentType(), payment.getPaymentMethodId(), payment.getCardNumber());
                        
                        if(!primaryPaymentMap.containsKey(paymentIdentifier))
                        {
                            payment.setPaymentSeqId(paymentSequence++);
                            primaryPaymentMap.put(paymentIdentifier, payment);
                        }
                        else
                        {
                            previousPayment = (PaymentVO) primaryPaymentMap.get(paymentIdentifier);
                            previousPayment.getOrderBill().setProductAmount(previousPayment.getOrderBill().getProductAmount() + payment.getOrderBill().getProductAmount()); 
                            previousPayment.getOrderBill().setAddOnAmount(previousPayment.getOrderBill().getAddOnAmount() + payment.getOrderBill().getAddOnAmount()); 
                            previousPayment.getOrderBill().setFeeAmount(previousPayment.getOrderBill().getFeeAmount() + payment.getOrderBill().getFeeAmount()); 
                            previousPayment.getOrderBill().setTax(previousPayment.getOrderBill().getTax() + payment.getOrderBill().getTax()); 
                            previousPayment.getOrderBill().setGrossProductAmount(previousPayment.getOrderBill().getGrossProductAmount() + payment.getOrderBill().getGrossProductAmount()); 
                            previousPayment.getOrderBill().setDiscountAmount(previousPayment.getOrderBill().getDiscountAmount() + payment.getOrderBill().getDiscountAmount()); 
                            previousPayment.getOrderBill().setAddOnDiscountAmount(previousPayment.getOrderBill().getAddOnDiscountAmount() + payment.getOrderBill().getAddOnDiscountAmount()); 
                            previousPayment.getOrderBill().setMilesPointsAmount(previousPayment.getOrderBill().getMilesPointsAmount() + payment.getOrderBill().getMilesPointsAmount()); 
                        }
                    }
                }
            }  
        }
        
        if(isNewRoute) {
        	addGiftcardPaymentsToIndividualPayments(individualPaymentsList);
        	populatePaymentsRefunds(paymentsListDocument, individualPaymentsList, paymentSeqId);
        	DOMUtil.addSection(responseDocument, paymentsListDocument.getChildNodes());
        }
        
        // Generate XML from primary payments
        String paymentKey = null;
        Document paymentDocument = (Document) DOMUtil.getDefaultDocument();
        Element paymentElement = paymentDocument.createElement("Payments");
        paymentDocument.appendChild(paymentElement);
        
        Iterator primaryPaymentIterator = primaryPaymentMap.keySet().iterator();
        while(primaryPaymentIterator.hasNext())
        {
            paymentKey = (String) primaryPaymentIterator.next();
            previousPayment = (PaymentVO) primaryPaymentMap.get(paymentKey);
            //Mask credit card numbers
            if (previousPayment.getPaymentType().equalsIgnoreCase("C"))
            {
                if (previousPayment.getCardNumber().length() > 4)
                {
                    previousPayment.setCardNumber("************" + previousPayment.getCardNumber().substring(previousPayment.getCardNumber().length() - 4,previousPayment.getCardNumber().length()));
                }
            } else if (previousPayment.getPaymentType().equalsIgnoreCase("R"))
            {
                if (previousPayment.getCardNumber().length() > 4)
                {
                    previousPayment.setCardNumber("***************" + previousPayment.getCardNumber().substring(previousPayment.getCardNumber().length() - 4,previousPayment.getCardNumber().length()));
                }
            }
            
            List refundCheckList = previousPayment.getRefundList();
            if (refundCheckList != null)
            {
                int totalRefunds = refundCheckList.size();
                for (int i = 0; i < totalRefunds; i++)
                {
                    RefundVO refundVO = (RefundVO) refundCheckList.get(i); 
                    if (refundVO.getPaymentType().equalsIgnoreCase("C"))
                    {
                        if (refundVO.getCardNumber().length() > 4)
                        {
                            refundVO.setCardNumber("************" + refundVO.getCardNumber().substring(refundVO.getCardNumber().length() - 4,refundVO.getCardNumber().length()));
                        }
                    } else if (refundVO.getPaymentType().equalsIgnoreCase("R")) {
                        if (refundVO.getCardNumber().length() > 4)
                        {
                        	refundVO.setCardNumber("***************" + refundVO.getCardNumber().substring(refundVO.getCardNumber().length() - 4,refundVO.getCardNumber().length()));
                        }
                    }
                }
            }
            
            Document ppDoc = DOMUtil.getDocument(previousPayment.toXML());
            paymentDocument.getDocumentElement().appendChild(paymentDocument.importNode(ppDoc.getFirstChild(), true));
        
        }
        DOMUtil.addSection(responseDocument, paymentDocument.getChildNodes());
        
        // Add totals to response document
        HashMap remainingTotals = new HashMap();
        remainingTotals.put("msdTotal", new BigDecimal(refundTotal.getMsdTotal()).setScale(2, BigDecimal.ROUND_HALF_UP));
        remainingTotals.put("addOnTotal", new BigDecimal(refundTotal.getAddOnTotal()).setScale(2, BigDecimal.ROUND_HALF_UP));
        remainingTotals.put("feeTotal", new BigDecimal(refundTotal.getFeeTotal()).setScale(2, BigDecimal.ROUND_HALF_UP));
        remainingTotals.put("taxTotal", new BigDecimal(refundTotal.getTaxTotal()).setScale(2, BigDecimal.ROUND_HALF_UP));
        remainingTotals.put("refundApplied", refundApplied);

        remainingTotals.put("serviceFee", new BigDecimal(serviceFee).setScale(2, BigDecimal.ROUND_HALF_UP));
        remainingTotals.put("sameDayUpcharge", new BigDecimal(sameDayUpcharge).setScale(2, BigDecimal.ROUND_HALF_UP));
        remainingTotals.put("shippingFee", new BigDecimal(shippingFee).setScale(2, BigDecimal.ROUND_HALF_UP));
        remainingTotals.put("morningDeliveryFee", new BigDecimal(morningDeliveryFee).setScale(2, BigDecimal.ROUND_HALF_UP));
        remainingTotals.put("satUpchargeFee", new BigDecimal(saturdayUpcharge).setScale(2, BigDecimal.ROUND_HALF_UP));
        remainingTotals.put("alHISurchargeFee", new BigDecimal(alsHawaiiUpcharge).setScale(2, BigDecimal.ROUND_HALF_UP));
        remainingTotals.put("fuelSurchargeFee", new BigDecimal(fuelSurchargeAmount).setScale(2, BigDecimal.ROUND_HALF_UP));
        remainingTotals.put("fuelSurchargeDescription", fuelSurchargeDescription);
        remainingTotals.put("applySurchargeCode", applySurchargeCode);
        
        
        remainingTotals.put("milesPointsTotal", new BigDecimal(refundTotal.getMilesPointsTotal()));
        
        //DI-27: Refund - Show new fees broken out in Mouseovers
        remainingTotals.put("sunUpchargeFee", new BigDecimal(sundayUpcharge).setScale(2, BigDecimal.ROUND_HALF_UP));
        remainingTotals.put("monnUpchargeFee", new BigDecimal(mondayUpcharge).setScale(2, BigDecimal.ROUND_HALF_UP));
        remainingTotals.put("lateCutoffFee", new BigDecimal(lateCutoffFee).setScale(2, BigDecimal.ROUND_HALF_UP));
        remainingTotals.put("internationalFee", new BigDecimal(internationalFee).setScale(2, BigDecimal.ROUND_HALF_UP));
        
        DOMUtil.addSection(responseDocument, "remainingTotals", "amount", remainingTotals, true);
        
        // If not a post refund maintain user entered values
        if(request.getAttribute("refundPost") == null || !request.getAttribute("refundPost").equals("Y"))
        {
            if(request.getParameter("product_amount") != null && !request.getParameter("product_amount").equals(""))
            {
                msdAmount = Double.parseDouble(request.getParameter("product_amount"));
            }
            
            if(request.getParameter("addon_amount") != null && !request.getParameter("addon_amount").equals(""))
            {
                addOnAmount = Double.parseDouble(request.getParameter("addon_amount"));
            }
            
            if(request.getParameter("fee_amount") != null && !request.getParameter("fee_amount").equals(""))
            {
                feeAmount = Double.parseDouble(request.getParameter("fee_amount"));
            }
            
            if(request.getParameter("tax_amount") != null && !request.getParameter("tax_amount").equals(""))
            {
                taxAmount = Double.parseDouble(request.getParameter("tax_amount"));
            }
        }
        
        // Check if there are predefined refund amounts
        String refundType = request.getParameter("refund_type");
        String modifyOrderFlag = request.getParameter("modify_order_flag");
        
        if((refundType != null && !refundType.equals(""))
            || (modifyOrderFlag != null && modifyOrderFlag.equalsIgnoreCase("Y")))
        {        
            if(request.getParameter("msd_incoming_amount") != null && !request.getParameter("msd_incoming_amount").equals(""))
            {
                msdAmount = Double.parseDouble(request.getParameter("msd_incoming_amount"));
            }
            if(request.getParameter("addon_incoming_amount") != null && !request.getParameter("addon_incoming_amount").equals(""))
            {
                addOnAmount = Double.parseDouble(request.getParameter("addon_incoming_amount"));
            }
            if(request.getParameter("fee_incoming_amount") != null && !request.getParameter("fee_incoming_amount").equals(""))
            {
                feeAmount = Double.parseDouble(request.getParameter("fee_incoming_amount"));
            }
            if(request.getParameter("tax_incoming_amount") != null && !request.getParameter("tax_incoming_amount").equals(""))
            {
                taxAmount = Double.parseDouble(request.getParameter("tax_incoming_amount"));
            }
            
            // Calculate values if it is a percent type
            if(refundType.equals("P"))
            {
            	logger.info("refund_paymentAuthorization:: " + request.getParameter("selected_payment_seq_id"));
            	int i = 1;
                if(isNewRoute){
                	i = new Integer(request.getParameter("selected_payment_seq_id")).intValue();	
                } else {
                	i = new Integer(request.getParameter("payType")).intValue();
                }
            	
                double paymentProductAmount = new Double(request.getParameter("payment_product_amount_" + i)).doubleValue();
                double paymentAddonAmount = new Double(request.getParameter("payment_addon_amount_" + i)).doubleValue();
                double paymentFeeAmount = new Double(request.getParameter("payment_fee_amount_" + i)).doubleValue();
                double paymentTaxAmount = new Double(request.getParameter("payment_tax_amount_" + i)).doubleValue();
                
                msdAmount = (msdAmount/100) * paymentProductAmount;
                addOnAmount = (addOnAmount/100) * paymentAddonAmount;
                feeAmount = (feeAmount/100) * paymentFeeAmount;
                taxAmount = (taxAmount/100) * paymentTaxAmount;
            }
        }
        
        // Add refund amounts to XML
        HashMap refundAmounts = new HashMap();
        refundAmounts.put("msdAmount", new BigDecimal(msdAmount).setScale(2, BigDecimal.ROUND_HALF_UP));
        refundAmounts.put("addOnAmount", new BigDecimal(addOnAmount).setScale(2, BigDecimal.ROUND_HALF_UP));
        refundAmounts.put("feeAmount", new BigDecimal(feeAmount).setScale(2, BigDecimal.ROUND_HALF_UP));
        refundAmounts.put("taxAmount", new BigDecimal(taxAmount).setScale(2, BigDecimal.ROUND_HALF_UP));
        DOMUtil.addSection(responseDocument, "refundAmounts", "amount", refundAmounts, true);
        
        // Build page data
        HashMap pageData = new HashMap();
        
        // Add previous page values
        pageData.put(COMConstants.PREV_PAGE, request.getParameter(COMConstants.PREV_PAGE));
        pageData.put(COMConstants.PREV_PAGE_POSITION, request.getParameter(COMConstants.PREV_PAGE_POSITION));
        
        //Added for retain refund review filters.
        pageData.put(COMConstants.PREV_SORT_COLUMN_NAME, request.getParameter(COMConstants.PREV_SORT_COLUMN_NAME));
        pageData.put(COMConstants.PREV_SORT_COLUMN_STATUS, request.getParameter(COMConstants.PREV_SORT_COLUMN_STATUS));
        pageData.put(COMConstants.PREV_CURRENT_SORT_COLUMN_STATUS, request.getParameter(COMConstants.PREV_CURRENT_SORT_COLUMN_STATUS));
        pageData.put(COMConstants.PREV_IS_FROM_HEADER, request.getParameter(COMConstants.PREV_IS_FROM_HEADER));
        pageData.put(COMConstants.PREV_USER_ID_FILTER, request.getParameter(COMConstants.PREV_USER_ID_FILTER));
        pageData.put(COMConstants.PREV_GROUP_ID_FILTER, request.getParameter(COMConstants.PREV_GROUP_ID_FILTER));
        pageData.put(COMConstants.PREV_CODES_FILTER, request.getParameter(COMConstants.PREV_CODES_FILTER));

        // Add error messages
        
        /*if(paymentMap.containsKey("G")) {
        	Map gcData = ((Map)paymentMap.get("G"));
        	if (gcData.isEmpty()) {
        		pageData.put("error_gift_certificate_call", "Y");
        	}
        }*/
        
        if(request.getAttribute("refund_amount_zero_error_flag") != null)
        {
            pageData.put("refund_amount_zero_error_flag", "Y");
        }
        
        if(request.getAttribute("refund_amount_exceeded_error_flag") != null)
        {
            pageData.put("refund_amount_exceeded_error_flag", "Y");
        }
        
        if(request.getAttribute("tax_only_error_flag") != null)
        {
            pageData.put("tax_only_error_flag", "Y");
        }
        
        // The existence of refundType means amounts are coming in from other
        // systems so we need to warn the user if they forget to save before exit
        if(refundType != null && !refundType.equals(""))
        { 
            pageData.put("exit_warning_flag", "Y");
        }
        
        // Maintain generic page data
        pageData.put("order_detail_id", orderDetailId);
        
        // Pass gift cert flag data
        pageData.put("gc_flag", gcFlag);
        
        // Pass a-type flag
        pageData.put("a_type_flag", aTypeFlag);
        
        // Hold incoming disposition ids (c-types)
        pageData.put("disposition_id", request.getParameter("disposition_id"));
        
        // Used to maintain dropdowns for new refunds
        pageData.put("refundDisp", request.getParameter("refundDisp"));
        pageData.put("responsibleParty", request.getParameter("responsibleParty"));
        pageData.put("payType", request.getParameter("payType"));
        pageData.put("refund_paymentAuthorization", request.getParameter("selected_payment_seq_id"));
        //Set the flag that will show if there was at least one C30 refund on the order
        pageData.put("C30RefundFound", C30RefundFound?"Y":"N");
        pageData.put(COMConstants.PRODUCT_TYPE, request.getParameter(COMConstants.PRODUCT_TYPE));
        pageData.put(COMConstants.SHIP_METHOD, request.getParameter(COMConstants.SHIP_METHOD));
        pageData.put(COMConstants.VENDOR_FLAG, request.getParameter(COMConstants.VENDOR_FLAG));
        pageData.put(COMConstants.ORDER_GUID, request.getParameter(COMConstants.ORDER_GUID));
        pageData.put(COMConstants.COMPLAINT_COMM_ORIGIN_TYPE_ID, request.getParameter(COMConstants.COMPLAINT_COMM_ORIGIN_TYPE_ID));
        pageData.put(COMConstants.COMPLAINT_COMM_NOTIFICATION_TYPE_ID, request.getParameter(COMConstants.COMPLAINT_COMM_NOTIFICATION_TYPE_ID));
        // Use case 22842 - Non C30 Refunds for Giftcard payments
        pageData.put("isGDPaymentExists", isGDPaymentExists?"Y":"N");
        
        if(modifyOrderFlag != null && !modifyOrderFlag.equals(""))
        {
             pageData.put("modify_order_flag", request.getParameter("modify_order_flag"));
        }
        else
        {
            pageData.put("modify_order_flag", "N");
        }
        
        // Check if user has permission to perform tax refunds
        String securityFlag = ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, COMConstants.SECURITY_IS_ON);
        if(securityFlag != null && securityFlag.equals("true"))
        {
            SecurityManager securityManager = SecurityManager.getInstance();
            if (securityManager.assertPermission(context, securityToken, ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, COMConstants.REFUND_TAX), COMConstants.UPDATE))
            {
                pageData.put("edit_tax_flag", "Y"); 
            }
            else
            {
                pageData.put("edit_tax_flag", "N"); 
            }
        }
        else
        {
            pageData.put("edit_tax_flag", "N"); 
        }
        
        // Check if no amounts remain available to refund
        if(refundTotal.getMsdTotal() > 0 || refundTotal.getAddOnTotal() > 0 || refundTotal.getFeeTotal() > 0 || refundTotal.getTaxTotal() > 0)
        {
            pageData.put("refund_avail_flag", "Y");
        }
        else
        {
            pageData.put("refund_avail_flag", "N");
        }
        
        // Set database values to page data
        pageData.put("order_disposition",      (String) paymentRefundMap.get(RefundDAO.ORDER_DISPOSITION));
        pageData.put("order_cancel_flag",      ((String) paymentRefundMap.get(RefundDAO.ORDER_CANCEL_FLAG)).trim());
        pageData.put("external_order_number",  (String) paymentRefundMap.get(RefundDAO.ORDER_NUMBER));
        pageData.put("customer_id",            (String) paymentRefundMap.get(RefundDAO.CUSTOMER_ID));
        pageData.put("vendor_flag",            RefundAPIBO.getVendorFlag(paymentRefundMap));
        pageData.put("order_live_flag",        ((String) paymentRefundMap.get(RefundDAO.ORDER_LIVE_FLAG)).trim());
        pageData.put("mp_redemption_rate_amt", ((String) paymentRefundMap.get("mp_redemption_rate_amt")));
        pageData.put("company_id",             ((String) paymentRefundMap.get("company_id")));
        pageData.put("recipient_state",        ((String) paymentRefundMap.get("recipient_state")));
        pageData.put("recipient_country",      ((String) paymentRefundMap.get("recipient_country")));

        if ( paymentRefundMap.get("mp_redemption_rate_amt") != null && 
             (new Double ( (String) paymentRefundMap.get("mp_redemption_rate_amt"))).doubleValue() != 0
           )
          milesPointsFlag = "Y";
        pageData.put("miles_points_flag",      milesPointsFlag);

        // Check if delivery date is passed
        Date deliveryDate = FieldUtils.formatStringToUtilDate((String) paymentRefundMap.get(RefundDAO.ORDER_DELIVERY_DATE));
        Calendar today = Calendar.getInstance();
        today.set(Calendar.HOUR_OF_DAY, 0);
        today.set(Calendar.MINUTE, 0);
        today.set(Calendar.SECOND, 0);
        today.set(Calendar.MILLISECOND, 0);
        
        
        if(deliveryDate != null && today.getTime().after(deliveryDate))
        {
            pageData.put("delivery_passed_flag", "Y");
        }
        else
        {
            pageData.put("delivery_passed_flag", "N");
        }
        
        // Origin Id
        pageData.put("origin_id", request.getParameter("origin_id"));
        
        // Append page data to XML
        DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true);

        return responseDocument;
    }

	private void addGiftcardPaymentsToIndividualPayments(List<PaymentVO> individualPaymentsList) {
		if(individualPaymentsList == null || individualPaymentsList.isEmpty())
			return;
		
		Map<String, PaymentVO> consolidatedPaymentMap = new HashMap<String, PaymentVO>();
		
	    String key = null;
	    PaymentVO paymentVO = null;
	    Iterator<PaymentVO> paymentIterator = individualPaymentsList.iterator();
	    while(paymentIterator.hasNext())
	    {
	        paymentVO = paymentIterator.next();

	        if(consolidatedPaymentMap.get(paymentVO.getOrderBill().getOrderBillId()) != null) {
	        	PaymentVO availPaymentVO = consolidatedPaymentMap.get(paymentVO.getOrderBill().getOrderBillId());
	        	
	        	if("G".equalsIgnoreCase(paymentVO.getPaymentType()) && !"G".equalsIgnoreCase(availPaymentVO.getPaymentType()) 
	        			&& paymentVO.getOrderBill().getOrderBillId().equalsIgnoreCase(availPaymentVO.getOrderBill().getOrderBillId())) {
	        		double giftcardAmount = paymentVO.getGcCouponAmount();
	        		double refundAmount = 0.0;
	        		List<RefundVO> refunds = paymentVO.getRefundList();
	        		
	        		if(refunds != null && !refunds.isEmpty()) {
	        			for(RefundVO refundVO : refunds) {
	        				refundAmount += refundVO.getOrderBill().getProductAmount();	
	        			}
	        		}
	        		availPaymentVO.getOrderBill().setProductAmount(availPaymentVO.getOrderBill().getProductAmount() - giftcardAmount);
	        		paymentVO.getOrderBill().setProductAmount(giftcardAmount - refundAmount);
	        		paymentVO.getOrderBill().setAddOnAmount(0);
	        		paymentVO.getOrderBill().setFeeAmount(0);
	        		paymentVO.getOrderBill().setTax(0);
//	        		adjustGiftCardAmounts(paymentVO, availPaymentVO);
	        	} else if("G".equalsIgnoreCase(availPaymentVO.getPaymentType()) && !"G".equalsIgnoreCase(paymentVO.getPaymentType())
	        			&& paymentVO.getOrderBill().getOrderBillId().equalsIgnoreCase(availPaymentVO.getOrderBill().getOrderBillId())) {
	        		double giftcardAmount = availPaymentVO.getGcCouponAmount();
	        		double refundAmount = 0.0;
	        		List<RefundVO> refunds = availPaymentVO.getRefundList();
	        		
	        		if(refunds != null && !refunds.isEmpty()) {
	        			for(RefundVO refundVO : refunds) {
	        				refundAmount += refundVO.getOrderBill().getProductAmount();	
	        			}
	        		}
	        		paymentVO.getOrderBill().setProductAmount(paymentVO.getOrderBill().getProductAmount() - giftcardAmount);
	        		availPaymentVO.getOrderBill().setProductAmount(giftcardAmount - refundAmount);
	        		availPaymentVO.getOrderBill().setAddOnAmount(0);
	        		availPaymentVO.getOrderBill().setFeeAmount(0);
	        		availPaymentVO.getOrderBill().setTax(0);
	        	}
	        	
	        } else {
	        	consolidatedPaymentMap.put(paymentVO.getOrderBill().getOrderBillId(), paymentVO);
	        }
	    }
	    logger.info("****** final consolidatedPaymentMap:" + consolidatedPaymentMap);
	    logger.info("****** final individualPaymentsList:" + individualPaymentsList);
	}

	private void adjustGiftCardAmounts(PaymentVO giftCardVO, PaymentVO creditCardVO) {
		double giftcardAmount = giftCardVO.getCreditAmount();
		double refundAmount = 0.0;
		List<RefundVO> refunds = giftCardVO.getRefundList();
		
		if(refunds != null && !refunds.isEmpty()) {
			for(RefundVO refundVO : refunds) {
				logger.info("giftCardVO:" + giftCardVO.getPaymentId() + " ::: " + "refundId:" + refundVO.getRefundId() + " :: creditCardVO:" + creditCardVO.getPaymentId());
				refundAmount += refundVO.getOrderBill().getProductAmount();	
			}
		}
		giftCardVO.getOrderBill().setProductAmount(giftcardAmount - refundAmount);
		giftCardVO.getOrderBill().setAddOnAmount(0);
		giftCardVO.getOrderBill().setFeeAmount(0);
		giftCardVO.getOrderBill().setTax(0);
		creditCardVO.getOrderBill().setProductAmount(giftCardVO.getOrderBill().getProductAmount() - giftcardAmount);
	}
	
	private boolean isNewRoute(List<PaymentVO> paymentList) {
		String[] pgRoutes = null;
		String route = null;
		
		try {
			ConfigurationUtil cu = ConfigurationUtil.getInstance();
			String strPgRoutes = cu.getFrpGlobalParm(OrderProcessingConstants.SERVICE, OrderProcessingConstants.PG_ROUTES);
			if(!StringUtils.isEmpty(strPgRoutes)) {
				pgRoutes = StringUtils.split(strPgRoutes, ",");
			}
			
			if(pgRoutes != null && pgRoutes.length > 0) {
				for(String pgRoute: pgRoutes) {
					for(PaymentVO paymentVO: paymentList) {
						if(pgRoute.trim().equalsIgnoreCase(paymentVO.getRoute())) {
							return true;
						}
					}			
				}
			}
		} catch (Exception e) {
			logger.error("Error while fetching Route information");
		}
		return false;
	}

	private void populatePaymentsRefunds(Document paymentsListDocument, List<PaymentVO> paymentList,
			int paymentSeqId) throws IOException, SAXException, ParserConfigurationException {
		
		Collections.sort(paymentList, new PaymentComparator());
		for(PaymentVO paymentVO: paymentList) {
			paymentVO.setPaymentSeqId(++paymentSeqId);
			Document ppDoc = DOMUtil.getDocument(paymentVO.toXML());
			paymentsListDocument.getDocumentElement().appendChild(paymentsListDocument.importNode(ppDoc.getFirstChild(), true));	
		}
	}

	/**
     *  total up the different charges based on the payment list
     * 
     * @param paymentList
     */
    private void sumBucketTotals(List paymentList) {
        ArrayList<String> billList = new ArrayList<String>();
        PaymentVO payment = null;
        Iterator<PaymentVO> paymentIterator = paymentList.iterator();
        while(paymentIterator.hasNext())
        {
            payment = (PaymentVO) paymentIterator.next();
            
            // Sum bucket totals
            if(!billList.contains(payment.getOrderBill().getOrderBillId()))
            {
            	refundTotal.setMsdTotal( refundTotal.getMsdTotal() + payment.getOrderBill().getProductAmount());
            	refundTotal.setAddOnTotal(refundTotal.getAddOnTotal() + payment.getOrderBill().getAddOnAmount());
            	refundTotal.setFeeTotal(refundTotal.getFeeTotal() + payment.getOrderBill().getFeeAmount());
            	refundTotal.setTaxTotal(refundTotal.getTaxTotal() + payment.getOrderBill().getTax());
            	refundTotal.setMilesPointsTotal(refundTotal.getMilesPointsTotal() + payment.getOrderBill().getMilesPointsAmount());
                
                paymentMsdTotal = paymentMsdTotal + payment.getOrderBill().getProductAmount();
                paymentAddOnTotal = paymentAddOnTotal + payment.getOrderBill().getAddOnAmount();
                paymentFeeTotal = paymentFeeTotal + payment.getOrderBill().getFeeAmount();
                paymentTaxTotal = paymentTaxTotal + payment.getOrderBill().getTax();
                paymentMilesPointsTotal = paymentMilesPointsTotal + payment.getOrderBill().getMilesPointsAmount();
                serviceFee = serviceFee + payment.getOrderBill().getServiceFee();
                sameDayUpcharge = sameDayUpcharge + payment.getOrderBill().getSameDayUpcharge();
                shippingFee = shippingFee + payment.getOrderBill().getShippingFee();
                morningDeliveryFee = morningDeliveryFee + payment.getOrderBill().getMorningDeliveryFee();
                fuelSurchargeAmount = fuelSurchargeAmount + payment.getOrderBill().getFuelSurchargeAmount();
                fuelSurchargeDescription = payment.getOrderBill().getFuelSurchargeDescription() ;
                applySurchargeCode = payment.getOrderBill().getApplySurchargeCode();
                saturdayUpcharge = saturdayUpcharge + payment.getOrderBill().getSaturdayUpcharge();
                alsHawaiiUpcharge = alsHawaiiUpcharge + payment.getOrderBill().getAlsHawaiiUpcharge();
                //DI-27: Refund - Show new fees broken out in Mouseovers
                sundayUpcharge = sundayUpcharge + payment.getOrderBill().getSundayUpcharge();
                mondayUpcharge = mondayUpcharge + payment.getOrderBill().getMondayUpcharge();
                lateCutoffFee = lateCutoffFee + payment.getOrderBill().getLateCutoffFee();
                internationalFee = internationalFee + payment.getOrderBill().getInternationalFee();
                billList.add(payment.getOrderBill().getOrderBillId());
            }
        }
		
        /* If GD can cover entire order total - check if there are any refunds on other payment types */
        Iterator<PaymentVO> payIterator = paymentList.iterator();
        boolean gdPaymentExist = false;
        boolean otherPaymentHasRefund = false;
        PaymentVO gdPaymentVO = null;
        PaymentVO otherPaymentVO = null;
        
        while(payIterator.hasNext())
        {
            payment = (PaymentVO) payIterator.next();
            if(payment.getPaymentType().equalsIgnoreCase("R")){
            	if(payment.getCreditAmount() >= (
            			payment.getOrderBill().getProductAmount() + payment.getOrderBill().getAddOnAmount() + 
            			payment.getOrderBill().getFeeAmount() + payment.getOrderBill().getTax())
            			){
            		gdPaymentExist = true;
            		gdPaymentVO = payment;
            	}
            }
            
            if(!payment.getPaymentType().equalsIgnoreCase("R")){
            	if(payment.getRefundList() != null && payment.getRefundList().size() > 0){
            		otherPaymentHasRefund = true;
            		otherPaymentVO = payment;
            	}
            }
        }
        
        if(gdPaymentExist && otherPaymentHasRefund){
        	ArrayList consolidatedRefundList = null;
        	RefundVO refund = null;
        	double refundProductTotal = 0;
        	double refundAddOnTotal = 0;
        	double refundServiceFeeTotal = 0;
        	double refundTaxTotal = 0;
        	
        	consolidatedRefundList = (ArrayList) otherPaymentVO.getRefundList();
        	for(int i=0; i < consolidatedRefundList.size(); i++)
            {
                refund = (RefundVO) consolidatedRefundList.get(i);
                refundProductTotal = refundProductTotal + refund.getOrderBill().getProductAmount();
                refundAddOnTotal = refundAddOnTotal + refund.getOrderBill().getAddOnAmount();
                refundServiceFeeTotal = refundServiceFeeTotal + refund.getOrderBill().getFeeAmount();
                refundTaxTotal = refundTaxTotal + refund.getOrderBill().getTax();
            }
        	
        	paymentMsdTotal =  gdPaymentVO.getOrderBill().getProductAmount() - refundProductTotal;
        	paymentAddOnTotal = gdPaymentVO.getOrderBill().getAddOnAmount() - refundAddOnTotal;
        	paymentFeeTotal = gdPaymentVO.getOrderBill().getFeeAmount() - refundServiceFeeTotal;
        	paymentTaxTotal = gdPaymentVO.getOrderBill().getTax() - refundTaxTotal;
        }
        /* END!!! - If GD can cover entire order total - check if there are any refunds on other payment types */
	}

	public void postRefund() throws Exception
    {
		String regex = "(?<=\\d),(?=\\d)";

        // Set flag so load knows it came from post
        request.setAttribute("refundPost", "Y");
        String vendorFlag = request.getParameter("vendor_flag");

        // Init DAO
        RefundDAO refundDAO = new RefundDAO(con);
        OrderDAO oDAO  = new OrderDAO(con);
        
        double productRemainingTotal = 0;
        double addonRemainingTotal = 0;
        double feeRemainingTotal = 0;
        double taxRemainingTotal = 0;
        
        double productAmount = 0;
        double addonAmount = 0;
        double feeAmount = 0;
        double taxAmount = 0;
        
        boolean taxRefundRecordRequired = false;
        double newRefundTaxAmount = 0;
        //Added the following for C30 refunds
        String dispCode = null;
        double gcAmount = 0;
        String C30FullRefund = null;
        
        String doAutoCan = null;
        String doAutoAskP = null;
        String autoReasonText = null;
        String autoAskPPrice = null;
        
        String complaintCommOriginTypeId = null;
        String complaintCommNotificationTypeId = null;

        String companyId = null;
        String recipientState = null;
        String recipientCountry = null;
                
        // Pull refund amounts
        if(request.getParameter("product_amount") != null && !request.getParameter("product_amount").equals(""))
        {
            productAmount = Double.parseDouble(request.getParameter("product_amount").replaceAll(regex,""));
        }
        
        if(request.getParameter("addon_amount") != null && !request.getParameter("addon_amount").equals(""))
        {
            addonAmount = Double.parseDouble(request.getParameter("addon_amount").replaceAll(regex,""));
        }
        
        if(request.getParameter("fee_amount") != null && !request.getParameter("fee_amount").equals(""))
        {
            feeAmount = Double.parseDouble(request.getParameter("fee_amount").replaceAll(regex,""));
        }
        
        if(request.getParameter("tax_amount") != null && !request.getParameter("tax_amount").equals(""))
        {
            taxAmount = Double.parseDouble(request.getParameter("tax_amount").replaceAll(regex,""));
        }
        
        // Pull remaining totals
        if(request.getParameter("product_total") != null && !request.getParameter("product_total").equals(""))
        {
            String productTotal = request.getParameter("product_total");
            productTotal = productTotal.replaceAll(regex, "");
            productRemainingTotal = Double.parseDouble(productTotal);
        }
        
        if(request.getParameter("addon_total") != null && !request.getParameter("addon_total").equals(""))
        {
            addonRemainingTotal = Double.parseDouble(request.getParameter("addon_total").replaceAll(regex,""));
        }
        
        if(request.getParameter("fee_total") != null && !request.getParameter("fee_total").equals(""))
        {
            feeRemainingTotal = Double.parseDouble(request.getParameter("fee_total").replaceAll(regex,""));
        }
        
        if(request.getParameter("tax_total") != null && !request.getParameter("tax_total").equals(""))
        {
        	taxRemainingTotal = Double.parseDouble(request.getParameter("tax_total").replaceAll(regex,""));
        }
        //get the refund disposition code
        if(StringUtils.isNotEmpty(request.getParameter("disp_code")))
          dispCode = request.getParameter("disp_code");

        //check if the entire gc was used.  if so, its a full refund
        if(StringUtils.isNotEmpty(request.getParameter("C30_full_refund")))
          C30FullRefund = request.getParameter("C30_full_refund");

        //retrieve the gc amount
        if(StringUtils.isNotEmpty(request.getParameter("gc_amount")))
          gcAmount = Double.parseDouble(request.getParameter("gc_amount"));

        //retrieve the company id
        if(StringUtils.isNotEmpty(request.getParameter("company_id")))
          companyId = request.getParameter("company_id");

        //retrieve the recipient_state
        if(StringUtils.isNotEmpty(request.getParameter("recipient_state")))
          recipientState = request.getParameter("recipient_state");

        //retrieve the recipient_country
        if(StringUtils.isNotEmpty(request.getParameter("recipient_country")))
          recipientCountry = request.getParameter("recipient_country");

        // Validate refund amounts are not zero before creation
        boolean isValid = true;
        if(productAmount == 0 && addonAmount == 0 && feeAmount == 0)
        {
            isValid = false;
            request.setAttribute("refund_amount_zero_error_flag", "Y");
        }
        
        if( (taxAmount > 0) && 
            (productAmount > 0 || addonAmount > 0 || feeAmount > 0) &&
            (!dispCode.equalsIgnoreCase("C30"))
          )
        {
            isValid = false;
            request.setAttribute("tax_only_error_flag", "Y");
        }
        
        if(StringUtils.isNotEmpty(request.getParameter(COMConstants.COMPLAINT_COMM_ORIGIN_TYPE_ID)))
        {
            complaintCommOriginTypeId = request.getParameter(COMConstants.COMPLAINT_COMM_ORIGIN_TYPE_ID);
        }
        
        if(StringUtils.isNotEmpty(request.getParameter(COMConstants.COMPLAINT_COMM_NOTIFICATION_TYPE_ID)))
        {
            complaintCommNotificationTypeId = request.getParameter(COMConstants.COMPLAINT_COMM_NOTIFICATION_TYPE_ID);
        }

        // Check for full refund
        boolean fullRefund = false;
        if (dispCode.equalsIgnoreCase("C30"))
        {
          if (C30FullRefund.equalsIgnoreCase(COMConstants.CONS_YES))
          {
            fullRefund = true;
          }
        }
        else if(productAmount == productRemainingTotal   &&
                addonAmount == addonRemainingTotal       &&
                feeAmount == feeRemainingTotal
                )
        {
            fullRefund = true;
        }
        
        //Issue 2576 - retrieve the order disposition code.  If "Held", cancel the order. Else follow the current process
        boolean cancelHeldOrder = false; 
        OrderDetailVO odVO = null;
        if (fullRefund)
        {
          //retrieve the order detail id
          String orderDetailId  = this.request.getParameter("order_detail_id");

          //retrieve the order detail vo
          if (StringUtils.isNotEmpty(orderDetailId))
            odVO = oDAO.getOrderDetail(orderDetailId); 

          //set the cancelHeldOrder flag depending on the order disposition.  
          //if order detail exist, and the order disposition code is Held, and we are issuing
          //a fullRefund, set the cancelHeldOrder flag to true
          if (odVO != null                                              && 
              StringUtils.isNotEmpty(odVO.getOrderDispCode())           && 
              StringUtils.equalsIgnoreCase(odVO.getOrderDispCode(), "Held"))
          {
            cancelHeldOrder = true;
          }
        }
        
        //
        //if the cancelHeldOrder flag is true, follow the Cancel/Held Order functionality
        if (cancelHeldOrder)
        {
          processCancelHeldOrders(odVO, false);
        }
        //else, issue the refund
        else
        {
          int i = 1;          
          RefundVO refund = null;
          ArrayList cancelList = new ArrayList();
          while(true && isValid)
          {
              boolean newRefund = false;

              if((request.getParameter("payment_seq_id_" + i) != null 
                  && !request.getParameter("payment_seq_id_" + i).equals("")) || taxRefundRecordRequired)
              {
                  
            	  /*In case of multiple payment types 
                   * eg: CC and GD OR CC and GC
                   * Create a new refund record for payment types other than GD or GC, to cover taxes, when GD or GC cannot cover them
                  */
                  if(taxRefundRecordRequired){
                	  int j = 1;
                	  
                	  for(int p=1; p<=2; p++){
                		  if(!(request.getParameter("payment_type_" + p).equalsIgnoreCase("R") || request.getParameter("payment_type_" + p).equalsIgnoreCase("G"))){
                    		  j = p;
                    	  }
                      }
                	  
                      double paymentProductAmount = 0;//new Double(request.getParameter("payment_product_amount_" + j)).doubleValue();
                      double paymentAddonAmount = 0;//new Double(request.getParameter("payment_addon_amount_" + j)).doubleValue();
                      double paymentFeeAmount = 0;//new Double(request.getParameter("payment_fee_amount_" + j)).doubleValue();
                      double paymentTaxAmount = new Double(request.getParameter("payment_tax_amount_" + j)).doubleValue();
                      
                      taxAmount = newRefundTaxAmount;                      
                      paymentTaxAmount = taxRemainingTotal;
                      
                      productAmount = 0;
                      addonAmount = 0;
                      feeAmount = 0;
                      
                      if(taxAmount > paymentTaxAmount)
                      {
                          // Set error flag
                          request.setAttribute("refund_amount_exceeded_error_flag", "Y");                          
                          // Set post to 'N' to maintain amount inputs
                          request.setAttribute("refundPost", "N");
                      }
                      else
                      {
                          // Create a refund from refund amounts
                          newRefund = true;
                          refund = new RefundVO();
                          refund.setOrderDetailId(request.getParameter("order_detail_id"));
                          refund.setRefundDispCode(request.getParameter("disp_code"));
                          refund.setResponsibleParty(request.getParameter("responsible_party"));
                          refund.setPaymentMethodId(request.getParameter("payment_method_" + j));
                          refund.setPaymentType(request.getParameter("payment_type_" + j));
                          refund.setCardId(request.getParameter("payment_card_id_" + j));
                          refund.setCardNumber(request.getParameter("payment_card_number_" + j));
                          refund.setRefundStatus(COMConstants.REFUND_STATUS_UNBILLED);
                          refund.setAccountTransactionInd("Y");
                          refund.setCreatedBy(this.getUserId());
                          refund.setComplaintCommOriginTypeId(complaintCommOriginTypeId);
                          refund.setComplaintCommNotificationTypeId(complaintCommNotificationTypeId);
                          refund.setAssociatedPaymentId(request.getParameter("payment_id_" + j));
                          
                          OrderBillVO orderBill = new OrderBillVO();
                          orderBill.setVendorFlag(vendorFlag);
                          orderBill.setProductAmount(productAmount);
                          orderBill.setAddOnAmount(addonAmount);
                          orderBill.setFeeAmount(feeAmount);
                          orderBill.setAdminFee(this.calculateAdminFee(new Double(request.getParameter("payment_commission_amount_" + j)).doubleValue(), request.getParameter("origin_id")));
                          orderBill.setWholesaleServiceFee(calculateWholesaleFee(new Double(request.getParameter("payment_wholesale_fee_" + j)).doubleValue(), feeAmount));
                          
                          // Calculate partial refund percentage to figure out commission and wholesale amounts
                          double refundPercentage = 0;
                          orderBill.setCommissionAmount((new Double(request.getParameter("payment_commission_amount_" + j)).doubleValue()) * refundPercentage);
                          orderBill.setWholesaleAmount((new Double(request.getParameter("payment_wholesale_amount_" + j)).doubleValue()) * refundPercentage);
                      
                          if(request.getParameter("vendor_flag") != null &&
                                  request.getParameter("vendor_flag").equalsIgnoreCase("SD"))
                          {
                              // SDG orders break refund fees out by filling shipping first and then service fee
                              double paymentServiceFee = request.getParameter("payment_service_fee_" + j) != null?new Double(request.getParameter("payment_service_fee_" + j)).doubleValue():0;
                              double paymentShippingFee = request.getParameter("payment_shipping_fee_" + j) != null?new Double(request.getParameter("payment_shipping_fee_" + j)).doubleValue():0;
                                                              
                              if(feeAmount > paymentShippingFee)
                              {
                                  orderBill.setShippingFee(paymentShippingFee);
                                  orderBill.setServiceFee(feeAmount - paymentShippingFee);
                              }
                              else
                              {
                                  orderBill.setShippingFee(feeAmount);
                                  orderBill.setServiceFee(0);
                              }
                          }
                                                
                                if(productAmount > 0 || addonAmount > 0 || feeAmount > 0 || taxAmount > 0)
                                {                                    
                                    orderBill.setTax(new BigDecimal(taxAmount).setScale(2, BigDecimal.ROUND_HALF_DOWN).doubleValue());                                    
                                }                            
                  
                          calculateTaxes(orderBill, con, refund.getOrderDetailId(), refundPercentage);
                          refund.setOrderBill(orderBill);
                      }
                      taxRefundRecordRequired = false;
                  }
                  /* #############  */
                  /*End creating new refund record*/
            	  
            	  if(fullRefund)
                  {
                      newRefund = true;
                      
                      // Create a refund object from payment amounts
                      refund = new RefundVO();
                      refund.setOrderDetailId(request.getParameter("order_detail_id"));
                      refund.setRefundDispCode(request.getParameter("disp_code"));
                      refund.setResponsibleParty(request.getParameter("responsible_party"));
                      refund.setPaymentMethodId(request.getParameter("payment_method_" + i));
                      refund.setPaymentType(request.getParameter("payment_type_" + i));
                      refund.setCardId(request.getParameter("payment_card_id_" + i));
                      refund.setCardNumber(request.getParameter("payment_card_number_" + i));
                      refund.setRefundStatus(COMConstants.REFUND_STATUS_UNBILLED);
                      refund.setAccountTransactionInd("Y");
                      refund.setCreatedBy(this.getUserId());
                      refund.setComplaintCommOriginTypeId(complaintCommOriginTypeId);
                      refund.setComplaintCommNotificationTypeId(complaintCommNotificationTypeId);

                      OrderBillVO orderBill = new OrderBillVO();
                      
                      if((request.getParameter("refund_count") != null 
                          && new Integer(request.getParameter("refund_count")).intValue() > 0)
                          || (request.getParameter("gc_flag") != null
                          && request.getParameter("gc_flag").equalsIgnoreCase("Y")))
                      {
                          // If refund already exists or a gift card exists(no discount) do not split discounts
                          orderBill.setProductAmount(new Double(request.getParameter("payment_product_amount_" + i)).doubleValue());
                      }
                      else
                      {
                          // If absolute full refund (no refund exists) split discount
                          orderBill.setGrossProductAmount(new Double(request.getParameter("payment_gross_" + i)).doubleValue());
                          orderBill.setDiscountAmount(new Double(request.getParameter("payment_discount_" + i)).doubleValue());
                          
                          //If absolute full refund (no refund exists) split add on discount
                          orderBill.setAddOnDiscountAmount(new Double(request.getParameter("payment_addon_discount_" + i)).doubleValue());
                          
                      }
                      
                      orderBill.setVendorFlag(vendorFlag);
                      orderBill.setAddOnAmount(new Double(request.getParameter("payment_addon_amount_" + i)).doubleValue());
                      orderBill.setFeeAmount(new Double(request.getParameter("payment_fee_amount_" + i)).doubleValue());
                      
                      if(request.getParameter("vendor_flag") != null &&
                                  request.getParameter("vendor_flag").equalsIgnoreCase("SD"))
                      {
                          // SDG orders break refund fees out by filling shipping first and then service fee
                          double paymentServiceFee = request.getParameter("payment_service_fee_" + i) != null?new Double(request.getParameter("payment_service_fee_" + i)).doubleValue():0;
                          double paymentShippingFee = request.getParameter("payment_shipping_fee_" + i) != null?new Double(request.getParameter("payment_shipping_fee_" + i)).doubleValue():0;
                          double paymentFeeTotal = new Double(request.getParameter("payment_fee_amount_" + i)).doubleValue();
                                                          
                          if(paymentFeeTotal > paymentShippingFee)
                          {
                              orderBill.setShippingFee(paymentShippingFee);
                              orderBill.setServiceFee(paymentFeeTotal - paymentShippingFee);
                          }
                          else
                          {
                              orderBill.setShippingFee(paymentFeeTotal);
                              orderBill.setServiceFee(0);
                          }
                      }
                      
                      orderBill.setTax(new Double(request.getParameter("payment_tax_amount_" + i)).doubleValue());
                      orderBill.setCommissionAmount(new Double(request.getParameter("payment_commission_amount_" + i)).doubleValue());
                      orderBill.setWholesaleAmount(new Double(request.getParameter("payment_wholesale_amount_" + i)).doubleValue());
                      orderBill.setAdminFee(this.calculateAdminFee(new Double(request.getParameter("payment_commission_amount_" + i)).doubleValue(), request.getParameter("origin_id")));
                      orderBill.setWholesaleServiceFee(calculateWholesaleFee(new Double(request.getParameter("payment_wholesale_fee_" + i)).doubleValue(), new Double(request.getParameter("payment_fee_amount_" + i)).doubleValue()));
                      
                      calculateTaxes(orderBill, con, refund.getOrderDetailId(), 1); 
                      refund.setOrderBill(orderBill);
                  }
                  else if((request.getParameter("payment_seq_id_" + i) != null && !request.getParameter("payment_seq_id_" + i).equals("")) 
                		  && (request.getParameter("payment_seq_id_" + i).equals(request.getParameter("payment_type_seq"))))
                  {
                      double paymentProductAmount = new Double(request.getParameter("payment_product_amount_" + i)).doubleValue();
                      double paymentAddonAmount = new Double(request.getParameter("payment_addon_amount_" + i)).doubleValue();
                      double paymentFeeAmount = new Double(request.getParameter("payment_fee_amount_" + i)).doubleValue();
                      double paymentTaxAmount = new Double(request.getParameter("payment_tax_amount_" + i)).doubleValue();
                      
                      
                      boolean multiPayments = false;
                      double tempPaymentTaxAmount = 0;
                      boolean isTaxMoreThanRemCheck = false;
                      boolean calculateTaxBasedOfRemainingTotal = false;
                      
                      for(int p=1; p<=2; p++){
                    	  if(request.getParameter("payment_seq_id_" + p) != null && !request.getParameter("payment_seq_id_" + p).equals("")){
                    		  if(p>1){
                    			  multiPayments = true;
                    		  }
                    	  }
                      }
                      /* When GD or GC are applied only to MSD, then Tax refund will be posted on other payment type*/
                      if((request.getParameter("payment_type_" + i).equalsIgnoreCase("R") || request.getParameter("payment_type_" + i).equalsIgnoreCase("G"))
                    		  && multiPayments
                    		  && !(paymentTaxAmount>0)){
                    	  taxRefundRecordRequired = true;
                      }
                      /*When Tax is split between payment types. Exhaust GD or GC then take from other payment types */
                      if((request.getParameter("payment_type_" + i).equalsIgnoreCase("R") || request.getParameter("payment_type_" + i).equalsIgnoreCase("G"))
                    		  && multiPayments
                    		  && paymentTaxAmount>0
                    		  && paymentTaxAmount<taxRemainingTotal){
                    	  isTaxMoreThanRemCheck = true;
                    	  tempPaymentTaxAmount = paymentTaxAmount;                    	  
                      }
                    	  
                      /* Full or Partial refund on VI, PP or BM when GD or GC are not fully refunded */
                      if(!(request.getParameter("payment_type_" + i).equalsIgnoreCase("R") || request.getParameter("payment_type_" + i).equalsIgnoreCase("G"))
                    		  && multiPayments
                    		  && (paymentProductAmount < productRemainingTotal || paymentAddonAmount < addonRemainingTotal || paymentFeeAmount < feeRemainingTotal))
                      {
                    	  calculateTaxBasedOfRemainingTotal = true;
                      }
                      
                      if(productAmount > paymentProductAmount
                          || addonAmount > paymentAddonAmount
                          || feeAmount > paymentFeeAmount
                          || taxAmount > paymentTaxAmount)
                      {
                          // Set error flag
                          request.setAttribute("refund_amount_exceeded_error_flag", "Y");
                          
                          // Set post to 'N' to maintain amount inputs
                          request.setAttribute("refundPost", "N");
                      }
                      else
                      {
                          // Create a refund from refund amounts
                          newRefund = true;
                          refund = new RefundVO();
                          refund.setOrderDetailId(request.getParameter("order_detail_id"));
                          refund.setRefundDispCode(request.getParameter("disp_code"));
                          refund.setResponsibleParty(request.getParameter("responsible_party"));
                          refund.setPaymentMethodId(request.getParameter("payment_method_" + i));
                          refund.setPaymentType(request.getParameter("payment_type_" + i));
                          refund.setCardId(request.getParameter("payment_card_id_" + i));
                          refund.setCardNumber(request.getParameter("payment_card_number_" + i));
                          refund.setRefundStatus(COMConstants.REFUND_STATUS_UNBILLED);
                          refund.setAccountTransactionInd("Y");
                          refund.setCreatedBy(this.getUserId());
                          refund.setComplaintCommOriginTypeId(complaintCommOriginTypeId);
                          refund.setComplaintCommNotificationTypeId(complaintCommNotificationTypeId);

                          OrderBillVO orderBill = new OrderBillVO();
                          orderBill.setVendorFlag(vendorFlag);
                          orderBill.setProductAmount(productAmount);
                          orderBill.setAddOnAmount(addonAmount);
                          orderBill.setFeeAmount(feeAmount);
                          orderBill.setAdminFee(this.calculateAdminFee(new Double(request.getParameter("payment_commission_amount_" + i)).doubleValue(), request.getParameter("origin_id")));
                          orderBill.setWholesaleServiceFee(calculateWholesaleFee(new Double(request.getParameter("payment_wholesale_fee_" + i)).doubleValue(), feeAmount));
                          
                          // Calculate partial refund percentage to figure out commission and wholesale amounts
                          double refundPercentage = (productAmount + addonAmount + feeAmount) / (paymentProductAmount + paymentAddonAmount + paymentFeeAmount);
                          orderBill.setCommissionAmount((new Double(request.getParameter("payment_commission_amount_" + i)).doubleValue()) * refundPercentage);
                          orderBill.setWholesaleAmount((new Double(request.getParameter("payment_wholesale_amount_" + i)).doubleValue()) * refundPercentage);
                      
                          if(request.getParameter("vendor_flag") != null &&
                                  request.getParameter("vendor_flag").equalsIgnoreCase("SD"))
                          {
                              // SDG orders break refund fees out by filling shipping first and then service fee
                              double paymentServiceFee = request.getParameter("payment_service_fee_" + i) != null?new Double(request.getParameter("payment_service_fee_" + i)).doubleValue():0;
                              double paymentShippingFee = request.getParameter("payment_shipping_fee_" + i) != null?new Double(request.getParameter("payment_shipping_fee_" + i)).doubleValue():0;
                                                              
                              if(feeAmount > paymentShippingFee)
                              {
                                  orderBill.setShippingFee(paymentShippingFee);
                                  orderBill.setServiceFee(feeAmount - paymentShippingFee);
                              }
                              else
                              {
                                  orderBill.setShippingFee(feeAmount);
                                  orderBill.setServiceFee(0);
                              }
                          }
                      
                          //for C30 refunds, the front end is sending the correct values for the tax. 
                          //We don't have to recalculate the tax and provide the refund on that. 
                          //C30 refund is more of a payment method switch from CC to GC, so, we don't have 
                          //to refund the tax based on the product, addon or shipping fee amount. 
                          if (!dispCode.equalsIgnoreCase("C30"))
                          {
                                if(productAmount > 0 || addonAmount > 0 || feeAmount > 0)
                                {
                                    double merchTaxRate = 0;
                                    double merchandiseTax = 0;
                                    double calculatedTax = 0;
                                    	                                    
                                    	if(taxRefundRecordRequired){
                                    		merchTaxRate = (productAmount + addonAmount + feeAmount) / (productRemainingTotal + addonRemainingTotal + feeRemainingTotal);
                                    		merchandiseTax = taxRemainingTotal;
                                    		orderBill.setTax(0);
                                    		newRefundTaxAmount = new BigDecimal(merchandiseTax * merchTaxRate).setScale(2, BigDecimal.ROUND_HALF_DOWN).doubleValue();
                                    	}
                                    	else{
                                    		merchTaxRate = (productAmount + addonAmount + feeAmount) / (paymentProductAmount + paymentAddonAmount + paymentFeeAmount);
                                    		merchandiseTax = new Double(request.getParameter("payment_tax_amount_" + i)).doubleValue();
                                    		orderBill.setTax(new BigDecimal(merchandiseTax * merchTaxRate).setScale(2, BigDecimal.ROUND_HALF_DOWN).doubleValue());
                                    	}
                                    	
                                    	if(calculateTaxBasedOfRemainingTotal){
                                    		merchTaxRate = (productAmount + addonAmount + feeAmount) / (productRemainingTotal + addonRemainingTotal + feeRemainingTotal);
                                    		merchandiseTax = taxRemainingTotal;
                                    		orderBill.setTax(new BigDecimal(merchandiseTax * merchTaxRate).setScale(2, BigDecimal.ROUND_HALF_DOWN).doubleValue());
                                    	}
                                    	
                                    	if(isTaxMoreThanRemCheck){
                                    		merchTaxRate = (productAmount + addonAmount + feeAmount) / (productRemainingTotal + addonRemainingTotal + feeRemainingTotal);
                                    		merchandiseTax = taxRemainingTotal;                                    		
                                    		calculatedTax = new BigDecimal(merchandiseTax * merchTaxRate).setScale(2, BigDecimal.ROUND_HALF_DOWN).doubleValue();
                                    		
                                    		if(calculatedTax > tempPaymentTaxAmount){
                                    			orderBill.setTax(tempPaymentTaxAmount);
                                    			newRefundTaxAmount = calculatedTax - tempPaymentTaxAmount;
                                    			taxRefundRecordRequired = true;
                                    		}
                                    	}
                                }
                          }
                          else if (taxAmount > 0)
                          {
                            // User supplied tax
                            orderBill.setTax(taxAmount);
                          }
                  
                          calculateTaxes(orderBill, con, refund.getOrderDetailId(), refundPercentage);
                          refund.setOrderBill(orderBill);
                      }
                  }                                    
                  
                  if(newRefund)
                  {
                      // Only insert if refund is greater than 0
                      if(refund.getOrderBill() != null 
                          && ((refund.getOrderBill().getProductAmount() > 0 ||
                              refund.getOrderBill().getGrossProductAmount() > 0)
                          || refund.getOrderBill().getAddOnAmount() > 0
                          || refund.getOrderBill().getFeeAmount() > 0
                          || refund.getOrderBill().getTax() > 0))
                      {
                        
                          double paymentMilesPointsOriginal = request.getParameter("payment_miles_points_original_" + i) != null?new Double(request.getParameter("payment_miles_points_original_" + i)).doubleValue():0;
                          double maxMilesPointsAvailableToBeRefunded = request.getParameter("payment_miles_points_amount_" + i) != null?new Double(request.getParameter("payment_miles_points_amount_" + i)).doubleValue():0;
                          //retrieve and convert the redemption rate in double. 
                          String mpRedemptionRateAmt = request.getParameter("mp_redemption_rate_amt");

                          //if the order had miles/points posted
                          if (paymentMilesPointsOriginal > 0)
                          {
                            //call calculateMilesPoints, which will calcuate the total miles to be refunded
                            int milesPointToBeRefunded = calculateMilesPoints(refund, maxMilesPointsAvailableToBeRefunded, mpRedemptionRateAmt);
                            //set the amount
                            refund.getOrderBill().setMilesPointsAmount(milesPointToBeRefunded);
                          }
                          if(refund.getAssociatedPaymentId() == null) {
                        	  if(fullRefund) {
                            	  refund.setAssociatedPaymentId(request.getParameter("payment_id_" + i));
                              } else {
                            	  refund.setAssociatedPaymentId(request.getParameter("payment_id"));
                              }  
                          }
                          logger.info("Associated payment id (" + i + "): " + refund.getAssociatedPaymentId());
                          
                          refundDAO.insertRefund(refund);
  
                          if(StringUtils.isNotEmpty(request.getParameter("doAutoCan")))
                          {
                              doAutoCan = request.getParameter("doAutoCan");
                              autoReasonText = request.getParameter("autoReasonText");
                              logger.debug("doAutoCan: " + doAutoCan);
                              logger.debug("autoReasonText: " + autoReasonText);
                              
                              if (doAutoCan.equals("Y"))
                              {
                            	  double tempProductAmount = refund.getOrderBill().getProductAmount();
                            	  if (tempProductAmount == 0 && refund.getOrderBill().getGrossProductAmount() > 0) {
                            		  tempProductAmount = refund.getOrderBill().getGrossProductAmount() - refund.getOrderBill().getDiscountAmount();
                            	  }
                                  double refundTotal = tempProductAmount + 
                                      refund.getOrderBill().getAddOnAmount() +
                                      refund.getOrderBill().getFeeAmount() +
                                      refund.getOrderBill().getTax();
                                  refundTotal = new BigDecimal(refundTotal).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
                                  logger.info("refundTotal=" + refundTotal);
                                  
                                  String commentText = refund.getRefundDispCode() +
                                      " refund applied for $" + formatCurrencyString(refundTotal) +
                                      " and mercury message cancelled by user.";
                                  insertComments(commentText);
                                  
                                  MessageDAO msgDAO = new MessageDAO(con);
                                  ResultTO result = null;
                                  
                                  String venusFlag = request.getParameter("venus_flag");
                                  String mercury_id = request.getParameter("mercury_id");
                                  logger.info("mercury_id: " + mercury_id + " venusFlag: " + venusFlag);
                                  
                                  if (venusFlag != null && mercury_id != null && cancelList.indexOf(mercury_id) < 0)
                                  {
                                      if (venusFlag.equals("Y")) 
                                      {
                                          MessageVO messageVO = msgDAO.getMessage(mercury_id, MessagingConstants.VENUS_MESSAGE);
                                          CancelOrderTO canTO = new CancelOrderTO();
                                          
                                          ConfigurationUtil configUtil = ConfigurationUtil.getInstance();         
                                          String cancelReasonCode = configUtil.getProperty(COMConstants.PROPERTY_FILE,"REFUND_CANCEL_REASON");                                           
                                            
                                          canTO.setVenusId(messageVO.getMessageId());
                                          canTO.setComments(autoReasonText);
                                          canTO.setCsr(this.getUserId());
                                          canTO.setCancelReasonCode(cancelReasonCode);
                                          
                                          
                                          boolean adjNeeded = false;
                                          
                                          // Test to see if an adjustment is required for SDS orders.
                                          MessageUtil msgUtil = new MessageUtil(con);
                                          boolean SDSShippingSystem = msgUtil.isShippingSystemSDS(messageVO);
                                        
                                          // If shipping system is SDS          
                                          if(SDSShippingSystem)
                                          {
                                            // If SDS CAN requires an ADJ
                                            if(msgUtil.requiresSDSAutoADJ(messageVO))                                            {
                                              adjNeeded = true;
                                              canTO.setVenusStatus(MessagingConstants.VENUS_STATUS_VERIFIED);
                                            }
                                          }
                                          // Use ESCALATE logic
                                          else
                                          {
                                            adjNeeded = checkDeliveryDate(messageVO.getDeliveryDate());
                                          }
                                        
                                          if (adjNeeded) 
                                          {
                                              canTO.setTransmitCancel(false);
                                          }
                                          
                                          result = MessagingServiceLocator.getInstance().getVenusAPI().sendCancel(canTO, con);
                                          if (!result.isSuccess()) 
                                          {
                                              throw new Exception("Could not create Venus CAN message: " + result.getErrorString());
                                          }
                                          
                                          if (adjNeeded) 
                                          {
                                              AdjustmentMessageTO adjTO = new AdjustmentMessageTO();
                                              adjTO.setComments(autoReasonText);
                                              adjTO.setOperator(this.getUserId());
                                              adjTO.setPrice(new Double(messageVO.getCurrentPrice()));
                                              adjTO.setMessageType("ADJ");
                                              adjTO.setOrderDetailId(messageVO.getOrderDetailId());
                                              adjTO.setVenusOrderNumber(messageVO.getMessageOrderId());
                                              adjTO.setFillingVendor(messageVO.getFillingFloristCode());
                                              adjTO.setOverUnderCharge(new Double("0.00"));
                                              adjTO.setCancelReasonCode(canTO.getCancelReasonCode());
  
                                              result = MessagingServiceLocator.getInstance().getVenusAPI().sendAdjustment(adjTO, con);
                                              if (!result.isSuccess()) 
                                              {
                                                  throw new Exception("Could not create Venus ADJ message: " + result.getErrorString());
                                              }
                                          }
                                          
                                      } else 
                                      {
                                          MessageVO messageVO = msgDAO.getMessage(mercury_id, MessagingConstants.MERCURY_MESSAGE);
                                          CANMessageTO canTO = new CANMessageTO();
                                          
                                          canTO.setMercuryId(messageVO.getMessageId());
                                          canTO.setComments(autoReasonText);
                                          canTO.setOperator(this.getUserId());
                                          canTO.setFillingFlorist(messageVO.getFillingFloristCode());
                                          canTO.setSendingFlorist(messageVO.getSendingFloristCode().substring(0,7));
                                          canTO.setMercuryStatus(BaseMercuryMessageTO.MERCURY_OPEN);
                                          canTO.setDirection(BaseMercuryMessageTO.OUTBOUND);
                                          
                                          result = MessagingServiceLocator.getInstance().getMercuryAPI().sendCANMessage(canTO, con);
                                          if (!result.isSuccess())
                                          {
                                              throw new Exception("Could not create Mercury CAN message: " + result.getErrorString());
                                          }
  
                                          boolean adjNeeded = checkDeliveryDate(messageVO.getDeliveryDate());
                                          if (adjNeeded) 
                                          {
                                              int month = messageVO.getDeliveryDate().getMonth() + 1;
                                              String sMonth = String.valueOf(month);
                                              if (sMonth.length() == 1) 
                                              {
                                                  sMonth = "0" + sMonth;
                                              }
                                              if (sMonth.equals("13")) 
                                              {
                                                  sMonth = "01";
                                              }
                                              String combinedReportNumber = sMonth + "-1";
                                              
                                              // get default reason code from configuration file
                                              ConfigurationUtil configUtil = ConfigurationUtil.getInstance();         
                                              String adjReasonCode = configUtil.getProperty(COMConstants.PROPERTY_FILE,"REFUND_ADJ_REASON");                                           
                                              
                                              if (dispCode.equals("A10")) 
                                              {
                                                  adjReasonCode = "7";
                                              } else if (dispCode.equals("B10") || dispCode.equals("B50")) 
                                              {
                                                  adjReasonCode = "10";
                                              }
                                              
                                              ADJMessageTO adjTO = new ADJMessageTO();
                                              adjTO.setMercuryId(messageVO.getMessageId());
                                              adjTO.setComments(autoReasonText);
                                              adjTO.setCombinedReportNumber(combinedReportNumber);
                                              adjTO.setOperator(this.getUserId());
                                              adjTO.setMercuryStatus(BaseMercuryMessageTO.MERCURY_OPEN);
                                              adjTO.setDirection(BaseMercuryMessageTO.OUTBOUND);
                                              adjTO.setOldPrice(new Double(messageVO.getCurrentPrice()));
                                              adjTO.setOverUnderCharge(new Double("0.00"));
                                              adjTO.setAdjustmentReasonCode(adjReasonCode);
                                              adjTO.setSendingFlorist(messageVO.getSendingFloristCode().substring(0,7));
                                              adjTO.setFillingFlorist(messageVO.getFillingFloristCode());
  
                                              result = MessagingServiceLocator.getInstance().getMercuryAPI().sendADJMessage(adjTO, con);
                                              if (!result.isSuccess())
                                              {
                                                  throw new Exception("Could not create Mercury ADJ message: " + result.getErrorString());
                                              }
                                              
                                          }
  
                                      }
                                      cancelList.add(mercury_id);
                                      logger.info("Added mercury_id: " + mercury_id + " to cancelList");
                                  } else 
                                  {
                                      logger.debug("could not create mercury message");
                                      logger.debug("venusFlag: " + venusFlag);
                                      logger.debug("mercury_id: " + mercury_id);
                                  }
  
                              } else if (doAutoCan.equals("N"))
                              {
                            	  double tempProductAmount = refund.getOrderBill().getProductAmount();
                            	  if (tempProductAmount == 0 && refund.getOrderBill().getGrossProductAmount() > 0) {
                            		  tempProductAmount = refund.getOrderBill().getGrossProductAmount() - refund.getOrderBill().getDiscountAmount();
                            	  }
                                  double refundTotal = tempProductAmount + 
                                      refund.getOrderBill().getAddOnAmount() +
                                      refund.getOrderBill().getFeeAmount() +
                                      refund.getOrderBill().getTax();
                                  refundTotal = new BigDecimal(refundTotal).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
                                  logger.info("refundTotal=" + refundTotal);
  
                                  String commentText = refund.getRefundDispCode() +
                                      " refund applied for $" + formatCurrencyString(refundTotal) +
                                      " and mercury message not cancelled by user.";
                                    
                                  // If this is a Free Shipping Order, tweak the Message
                                  if(refundApiBO.isOrderFreeShippingMembership(this.request.getParameter(COMConstants.ORDER_DETAIL_ID), con))
                                  {
                                    commentText = refund.getRefundDispCode() +
                                        " refund applied for $" + formatCurrencyString(refundTotal) + ".";
                                  }
                                  
                                  insertComments(commentText);
                                    
                              } else 
                              {
                            	  double tempProductAmount = refund.getOrderBill().getProductAmount();
                            	  if (tempProductAmount == 0 && refund.getOrderBill().getGrossProductAmount() > 0) {
                            		  tempProductAmount = refund.getOrderBill().getGrossProductAmount() - refund.getOrderBill().getDiscountAmount();
                            	  }
                                  double refundTotal = tempProductAmount + 
                                      refund.getOrderBill().getAddOnAmount() +
                                      refund.getOrderBill().getFeeAmount() +
                                      refund.getOrderBill().getTax();
                                  refundTotal = new BigDecimal(refundTotal).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
                                  logger.debug("refundTotal=" + refundTotal);
  
                                  String commentText = refund.getRefundDispCode() +
                                      " refund applied for $" + formatCurrencyString(refundTotal) + ". ";

                                  String tempCommentText = buildExceptionMessage(doAutoCan, "CAN");
                                  commentText = commentText + tempCommentText;
                                  insertComments(commentText);
                              }
                          }
  
                          if(StringUtils.isNotEmpty(request.getParameter("doAutoAskP")))
                          {
                              doAutoAskP = request.getParameter("doAutoAskP");
                              autoReasonText = request.getParameter("autoReasonText");
                              autoAskPPrice = request.getParameter("autoAskPPrice");
                              logger.debug("doAutoAskP: " + doAutoAskP);
                              logger.debug("autoReasonText: " + autoReasonText);
                              logger.debug("autoAskPPrice: " + autoAskPPrice);
                              
                              if (doAutoAskP.equals("Y")) 
                              {
                                  double refundTotal = productAmount + addonAmount + feeAmount +
                                      refund.getOrderBill().getTax();
                                  refundTotal = new BigDecimal(refundTotal).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
                                  logger.debug("refundTotal=" + refundTotal);
  
                                  String commentText = refund.getRefundDispCode() +
                                      " refund applied for $" + formatCurrencyString(refundTotal) +
                                      " and $" + autoAskPPrice + " price adjustment sent by user.";
                                  insertComments(commentText);
  
                                  MessageDAO msgDAO = new MessageDAO(con);
                                  ResultTO result = null;
                                  
                                  String venusFlag = request.getParameter("venus_flag");
                                  String mercury_id = request.getParameter("mercury_id");
                                  
                                  if (venusFlag != null && venusFlag != "Y")
                                  {
                                      MessageVO messageVO = msgDAO.getMessage(mercury_id, MessagingConstants.MERCURY_MESSAGE);
                                      ASKMessageTO askTO = new ASKMessageTO();
                                          
                                      askTO.setMercuryId(messageVO.getMessageId());
                                      askTO.setComments(autoReasonText);
                                      askTO.setAskAnswerCode("P");
                                      askTO.setPrice(new Double(autoAskPPrice));
                                      askTO.setOldPrice(new Double(messageVO.getCurrentPrice()));
                                      askTO.setOperator(this.getUserId());
                                      askTO.setFillingFlorist(messageVO.getFillingFloristCode());
                                      askTO.setSendingFlorist(messageVO.getSendingFloristCode().substring(0,7));
                                      askTO.setMercuryStatus(BaseMercuryMessageTO.MERCURY_OPEN);
                                      askTO.setDirection(BaseMercuryMessageTO.OUTBOUND);
                                          
                                      result = MessagingServiceLocator.getInstance().getMercuryAPI().sendASKMessage(askTO, con);
                                      if (!result.isSuccess())
                                      {
                                          throw new Exception("Could not create Mercury ASK message: " + result.getErrorString());
                                      }
  
                                      boolean adjNeeded = checkDeliveryDate(messageVO.getDeliveryDate());
                                      if (adjNeeded) 
                                      {
                                          int month = messageVO.getDeliveryDate().getMonth() + 1;
                                          String sMonth = String.valueOf(month);
                                          if (sMonth.length() == 1) 
                                          {
                                              sMonth = "0" + sMonth;
                                          }
                                          if (sMonth.equals("13")) 
                                          {
                                              sMonth = "01";
                                          }
                                          String combinedReportNumber = sMonth + "-1";
  
                                          // get default reason code from configuration file
                                          ConfigurationUtil configUtil = ConfigurationUtil.getInstance();         
                                          String adjReasonCode = configUtil.getProperty(COMConstants.PROPERTY_FILE,"REFUND_ADJ_REASON");                                           
                                            
                                          if (dispCode.equals("A10")) 
                                          {
                                              adjReasonCode = "7";
                                          } else if (dispCode.equals("B10") || dispCode.equals("B50")) 
                                          {
                                              adjReasonCode = "10";
                                          }
  
                                          ADJMessageTO adjTO = new ADJMessageTO();
                                          adjTO.setMercuryId(messageVO.getMessageId());
                                          adjTO.setComments(autoReasonText);
                                          adjTO.setCombinedReportNumber(combinedReportNumber);
                                          adjTO.setOperator(this.getUserId());
                                          adjTO.setMercuryStatus(BaseMercuryMessageTO.MERCURY_OPEN);
                                          adjTO.setDirection(BaseMercuryMessageTO.OUTBOUND);
                                          adjTO.setOldPrice(new Double(messageVO.getCurrentPrice()));
                                          adjTO.setOverUnderCharge(new Double(autoAskPPrice));
                                          adjTO.setAdjustmentReasonCode(adjReasonCode);
                                          adjTO.setSendingFlorist(messageVO.getSendingFloristCode().substring(0,7));
                                          adjTO.setFillingFlorist(messageVO.getFillingFloristCode());
  
                                          result = MessagingServiceLocator.getInstance().getMercuryAPI().sendADJMessage(adjTO, con);
                                          if (!result.isSuccess())
                                          {
                                              throw new Exception("Could not create Mercury ADJ message: " + result.getErrorString());
                                          }
                                              
                                      }
                                  } else 
                                  {
                                      logger.debug("could not create mercury message");
                                      logger.debug("venusFlag: " + venusFlag);
                                      logger.debug("mercury_id: " + mercury_id);
                                  }
  
                              } else if (doAutoAskP.equals("N"))
                              {
                                  double refundTotal = productAmount + addonAmount + feeAmount +
                                      refund.getOrderBill().getTax();
                                  refundTotal = new BigDecimal(refundTotal).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
                                  logger.debug("refundTotal=" + refundTotal);
  
                                  String commentText = refund.getRefundDispCode() +
                                      " refund applied for $" + formatCurrencyString(refundTotal) +
                                      " and price adjustment not sent by user.";
                                      
                                  // If this is a Free Shipping Order, tweak the Message
                                  if(refundApiBO.isOrderFreeShippingMembership(this.request.getParameter(COMConstants.ORDER_DETAIL_ID), con))
                                  {
                                    commentText = refund.getRefundDispCode() +
                                      " refund applied for $" + formatCurrencyString(refundTotal) + ".";
                                  }                                      
                                      
                                  insertComments(commentText);
                                    
                              } else 
                              {
                                  double refundTotal = productAmount + addonAmount + feeAmount +
                                      refund.getOrderBill().getTax();
                                  refundTotal = new BigDecimal(refundTotal).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
                                  logger.debug("refundTotal=" + refundTotal);

                                  String commentText = refund.getRefundDispCode() +
                                      " refund applied for $" + formatCurrencyString(refundTotal) +
                                      " and price adjustment not sent by user. ";
                                  
                                  // If this is a Free Shipping Order, tweak the Message
                                  if(refundApiBO.isOrderFreeShippingMembership(this.request.getParameter(COMConstants.ORDER_DETAIL_ID), con))
                                  {
                                    commentText = refund.getRefundDispCode() +
                                      " refund applied for $" + formatCurrencyString(refundTotal) + ". ";
                                  }                                      

                                  String tempCommentText = buildExceptionMessage(doAutoAskP, "ASKP");
                                  commentText = commentText + tempCommentText;
                                  insertComments(commentText);
                              }
                          }
          
                      }
                  }                
              }
              else
              {
                  break;
              }
              
              i++;
          } //end while
        } //end else
        
        refundApiBO.handleRefundStatusModified(this.request.getParameter(COMConstants.ORDER_DETAIL_ID), this.userId, con);
    }
    
    private void insertComments(String commentText) throws Exception
    {
        try {

            logger.debug("Adding comments: " + commentText);
            CommentsVO comVO = new CommentsVO();
            comVO.setOrderDetailId(request.getParameter("order_detail_id"));
            comVO.setCommentType("Order");
            comVO.setCommentOrigin("CS Call");
            if (request.getParameter("order_guid") != null) {
                comVO.setOrderGuid(request.getParameter("order_guid"));
            }
            if (request.getParameter("customer_id") != null) {
                comVO.setCustomerId(request.getParameter("customer_id"));
            }
            comVO.setComment(commentText);
            comVO.setUpdatedBy(this.getUserId());
        
            CommentDAO orderComments = new CommentDAO(con);
            orderComments.insertComment(comVO);

        }
        catch (Exception e)
        {
              logger.error(e);
        }

    }
    
    private String buildExceptionMessage(String msg, String msgType) 
    {
        String result = "";

        if (msg.equals("OrderCount")) {
            result = "No " + msgType + " message created. Order must have only one live mercury message";
        } else if (msg.equals("FTDM")) {
            result = "This is an FTDM order. " + msgType + " mercury message must be generated";
            result = result + " on the communication screen and called out to the florist.";
        } else if (msg.equals("BeforeDelivery")) {
            result = msgType + " automation cannot occur before the delivery date.";
        } else if (msg.equals("FillingFlorist")) {
            result = msgType + " automation cannot occur. Order florist does not match responsible florist.";
        } else if (msg.equals("Vendor")) {
            result = msgType + " automation cannot occur for Vendor Orders.";
        } else if (msg.equals("OrderNotLive")) {
        	result = msgType + " automation cannot occur. Order has no live mercury message.";
        }
        
        return result;
    }

    /**
     *  check if the current date is greater than the
     *  last day of the delivery date month
     * @param deliveryDate - Date
     * @return boolean
     * @throws ParseException
     */
    private boolean checkDeliveryDate(Date deliveryDate)
        throws Exception {
        if (logger.isDebugEnabled()) {
            logger.debug("Entering checkDeliveryDate");
            logger.debug("deliveryDate: " + deliveryDate);
        }

        boolean dateComparision = false;

        try {
            Calendar calCurrent = Calendar.getInstance(TimeZone.getDefault());
            Date currentDate = calCurrent.getTime();

            SimpleDateFormat sdfOutput = new SimpleDateFormat(MessagingConstants.MESSAGE_DATE_FORMAT);

            if (currentDate.after(deliveryDate)) {
                Calendar calDelivery = Calendar.getInstance(TimeZone.getDefault());
                calDelivery.setTime(deliveryDate);

                int day = calDelivery.getActualMaximum(Calendar.DAY_OF_MONTH);

                Calendar calAdjustment = Calendar.getInstance(TimeZone.getDefault());
                calAdjustment.setTime(deliveryDate);
                calAdjustment.set(calDelivery.get(Calendar.YEAR),
                    calDelivery.get(Calendar.MONTH), day);

                Date adjustmentDate = calAdjustment.getTime();

                if (currentDate.after(adjustmentDate)) {
                    dateComparision = true;
                }
            }
        } finally {
            if (logger.isDebugEnabled()) {
                logger.debug("Exiting checkDeliveryDate");
            }
        }

        return dateComparision;
    }                  
 
    public void postFullRefund() throws Exception
    {
        OrderDAO orderDAO = new OrderDAO(con);
        CachedResultSet detailIdSet = orderDAO.viewDetailIds(request.getParameter("order_guid")); 
    
        try
        {                
            List orderDetailList = new ArrayList();
            String refundDisp = request.getParameter("refund_disp_code");
            String refundParty = request.getParameter("responsible_party");
            OrderDAO oDAO  = new OrderDAO(con);
            
            while(detailIdSet != null && detailIdSet.next())
            {
                String orderDetailId = detailIdSet.getObject(1).toString();
                OrderDetailVO odVO = oDAO.getOrderDetail(orderDetailId);
                if (odVO != null                                              && 
                    StringUtils.isNotEmpty(odVO.getOrderDispCode())           && 
                    StringUtils.equalsIgnoreCase(odVO.getOrderDispCode(), "Held"))
                {
                    this.processCancelHeldOrders(odVO, true);
                }
                else 
                {
                    orderDetailList.add(orderDetailId);
                    postFullRefund(orderDetailList,refundDisp,refundParty,COMConstants.REFUND_STATUS_UNBILLED);
                    orderDetailList = new ArrayList();
                }
            }
            
            
            
        }
        finally
        {
        }
    }

    /**
     * This is a wrapper method (around postFullRefundMO) which is intended to be called
     * as part of the remote API for COM to issue a refund.  For example, the 
     * email-request-processing module utilizes this API to do full refunds when 
     * a customer cancels an order from the customer service website.  
     *
     * Note that we use postFullRefundMO since it takes into account any existing
     * partial refunds (unlike postFullRefund).  Also note that user context/security information 
     * is not relevant from a remote context so this method should NOT be called 
     * by anything coming from the COM front-end (use postFullRefundMO or postFullRefund instead).
     */
    public void postFullRemainingRefund(String orderDetailId, String refundDisposition, 
                                         String responsibleParty, String refundStatus) throws Exception
    {
    	//delegate to the new refundAPI
        refundApiBO.postFullRemainingRefund(orderDetailId, refundDisposition, responsibleParty, refundStatus, refundStatus, this.con);
    }


    /**
     *  This method accepts info about the order and some metadata about the refund and then performs the refund.
     * 
     * @param orderDetailIds
     * @param refundDisposition
     * @param responsibleParty
     * @param refundStatus
     * @param securityToken
     * @param context
     * @throws Exception
     */
    public List<RefundVO> postFullRefundMO(List orderDetailIds, String refundDisposition, String responsibleParty, String refundStatus,
			String securityToken, String context) throws Exception {
    	return refundApiBO.postFullRefundMO(orderDetailIds, refundDisposition, responsibleParty, refundStatus, securityToken, context, this.userId, this.con);
    }

    public List<RefundVO> postFullRefundMO(List orderDetailIds, String refundDisposition, String responsibleParty, String refundStatus,
                        String securityToken, String context, String processId) throws Exception
    {
        this.userId = processId;
        return refundApiBO.postFullRefundMO(orderDetailIds, refundDisposition, responsibleParty, refundStatus, securityToken, context, processId, this.con);
    }
 
    

    public void postFullRefund(List orderDetailIds, String refundDisposition, String responsibleParty, String refundStatus) throws Exception
    {
        //if not ids are passed in exit
        if(orderDetailIds == null)
        {
            return;
        }
        
        HashMap primaryPaymentMap = null;
        RefundDAO refundDAO = new RefundDAO(con);        
        Iterator iter = orderDetailIds.iterator();
        String recipientCountry; 
        String recipientState; 
        String companyId;
        
        String orderDetailId = null;
        while(iter.hasNext())
        {
            int paymentSequence = 1;
            boolean isNewRoute = false;
            boolean giftCertExists = false;
            primaryPaymentMap = new HashMap();
            orderDetailId = (String)iter.next();
            
            isNewRoute = refundDAO.isNewPaymentRoute(orderDetailId);
            HashMap paymentRefundMap = refundDAO.loadPaymentRefund(orderDetailId);            
            recipientCountry = (String)paymentRefundMap.get("recipient_country"); 
            recipientState = (String)paymentRefundMap.get("recipient_state"); 
            companyId = (String)paymentRefundMap.get("company_id"); 

            Map billPaymentMap = (Map) paymentRefundMap.get(RefundDAO.PAYMENT_LIST);
        
            Iterator billPaymentIterator = billPaymentMap.keySet().iterator();
            while(billPaymentIterator.hasNext())
            {
                String billKey = (String) billPaymentIterator.next();
                List paymentList = (List) billPaymentMap.get(billKey);
            
                // Reset 
                refundTotal.setMsdTotal(0);
                refundTotal.setAddOnTotal(0);
                refundTotal.setFeeTotal(0);
                refundTotal.setTaxTotal(0);
                paymentMilesPointsTotal = 0;
                milesPointsFlag = "N";
                
                // Consolidate all payments
                HashMap paymentMap = RefundAPIBO.consolidatePayments(paymentList, null);
                sumBucketTotals(paymentList);
                
                HashMap paymentTypeMap = null;
                Iterator paymentTypeIterator = null;
                PaymentVO payment = null;
                RefundVO refund = null;
                OrderBillVO orderBill = null;
                
                // Determine if a gift certificate is one of the payments
                //if(paymentMap.containsKey("G"))
                //{
                //    giftCertExists = true;
                //}
                
                // If payments contain a gift card, use the gift card payment amount
                // to fill buckets intially before using any other payment type
                if(paymentMap.containsKey("G") || paymentMap.containsKey("R"))
                {        
                	giftCertExists = true;
                	HashMap consolidatedPaymentMap = null;
                    PaymentVO consolidatedPayment = null;
                    Iterator consolidatedPaymentIterator = null;
                
                    // Create a bill to maintain remaining balances
                    OrderBillVO remainingBill = new OrderBillVO();
                    remainingBill.setProductAmount(refundTotal.getMsdTotal());
                    remainingBill.setAddOnAmount(refundTotal.getAddOnTotal());
                    remainingBill.setFeeAmount(refundTotal.getFeeTotal());
                    remainingBill.setTax(refundTotal.getTaxTotal());
                
                    // Iterate through all gift cards on the order and apply to buckets
                    if(paymentMap.containsKey("G")) {                    	
                    	consolidatedPaymentMap = (HashMap) paymentMap.get("G");
                        RefundAPIBO.distributeBuckets(consolidatedPaymentMap, remainingBill);
                    }
                    
                    if(paymentMap.containsKey("R")) {
                    	consolidatedPaymentMap = (HashMap) paymentMap.get("R");
                    	RefundAPIBO.distributeBuckets(consolidatedPaymentMap, remainingBill);
                    }
                    
                    // Iterate through remaining PayPal payments on the order(if exist) and apply to buckets
                    if(paymentMap.containsKey("P"))
                    {
                        consolidatedPaymentMap = (HashMap) paymentMap.get("P");
                        RefundAPIBO.distributeBuckets(consolidatedPaymentMap, remainingBill); 
                    }

                    // Iterate through remaining credit cards on the order(if exist) and apply to buckets
                    if(paymentMap.containsKey("C"))
                    {
                        consolidatedPaymentMap = (HashMap) paymentMap.get("C");
                        RefundAPIBO.distributeBuckets(consolidatedPaymentMap, remainingBill); 
                    }
                }
                
                // Consolidate all payments from all order bills on order
                Iterator paymentIterator = paymentMap.keySet().iterator();
                while(paymentIterator.hasNext())
                {
                    paymentTypeMap = (HashMap) paymentMap.get((String) paymentIterator.next());
                    
                    paymentTypeIterator = paymentTypeMap.keySet().iterator();
                    while(paymentTypeIterator.hasNext())
                    {
                        payment = (PaymentVO) paymentTypeMap.get((String) paymentTypeIterator.next());
                        
                        if(payment != null)
                        {
                        	String paymentIdentifier = null;
                        	if(isNewRoute) {
                        		paymentIdentifier = RefundAPIBO.generateIdentifier(payment.getPaymentType(), payment.getPaymentMethodId(), payment.getCardNumber(), payment.getAuthorization());
                        	} else {
                        		paymentIdentifier = RefundAPIBO.generateIdentifier(payment.getPaymentType(), payment.getPaymentMethodId(), payment.getCardNumber());	
                        	}
                            
                            if(!primaryPaymentMap.containsKey(paymentIdentifier))
                            {
                                payment.setPaymentSeqId(paymentSequence++);
                                primaryPaymentMap.put(paymentIdentifier, payment);
                            }
                            else
                            {
                                PaymentVO previousPayment = (PaymentVO) primaryPaymentMap.get(paymentIdentifier);
                                previousPayment.getOrderBill().setProductAmount(previousPayment.getOrderBill().getProductAmount() + payment.getOrderBill().getProductAmount()); 
                                previousPayment.getOrderBill().setAddOnAmount(previousPayment.getOrderBill().getAddOnAmount() + payment.getOrderBill().getAddOnAmount()); 
                                previousPayment.getOrderBill().setFeeAmount(previousPayment.getOrderBill().getFeeAmount() + payment.getOrderBill().getFeeAmount()); 
                                previousPayment.getOrderBill().setTax(previousPayment.getOrderBill().getTax() + payment.getOrderBill().getTax()); 
                                previousPayment.getOrderBill().setGrossProductAmount(previousPayment.getOrderBill().getGrossProductAmount() + payment.getOrderBill().getGrossProductAmount()); 
                                previousPayment.getOrderBill().setDiscountAmount(previousPayment.getOrderBill().getDiscountAmount() + payment.getOrderBill().getDiscountAmount()); 
                                previousPayment.getOrderBill().setAddOnDiscountAmount(previousPayment.getOrderBill().getAddOnDiscountAmount() + payment.getOrderBill().getAddOnDiscountAmount());
                                previousPayment.getOrderBill().setMilesPointsAmount(previousPayment.getOrderBill().getMilesPointsAmount() + payment.getOrderBill().getMilesPointsAmount()); 
                            }
                        }
                    }
                }  
            }
             
            // Create refunds for all the payments   
            PaymentVO payment = null;
            RefundVO refund = null;
            OrderBillVO orderBill = null;
                
            Iterator paymentIterator = primaryPaymentMap.keySet().iterator();
            while(paymentIterator.hasNext())
            {
                payment = (PaymentVO) primaryPaymentMap.get((String) paymentIterator.next());
                
                refund = new RefundVO();
                refund.setOrderDetailId(orderDetailId);
                refund.setRefundDispCode(refundDisposition);
                refund.setResponsibleParty(responsibleParty);
                refund.setPaymentMethodId(payment.getPaymentMethodId());
                refund.setPaymentType(payment.getPaymentType());
                refund.setCardId(payment.getCardId());
                refund.setCardNumber(payment.getCardNumber());
                refund.setCreatedBy(this.getUserId());
                refund.setAccountTransactionInd("Y");
                refund.setRefundStatus(refundStatus);
                refund.setAssociatedPaymentId(Long.toString(payment.getPaymentId()));
                
                orderBill = new OrderBillVO();
                
                // If gift cert exists, discounts can not be present so no need to split discounts from gross amount
                if(giftCertExists)
                {
                    orderBill.setProductAmount(payment.getOrderBill().getProductAmount());
                }
                else
                {
                    orderBill.setGrossProductAmount(payment.getOrderBill().getGrossProductAmount());
                    orderBill.setDiscountAmount(payment.getOrderBill().getDiscountAmount());
                    orderBill.setAddOnDiscountAmount(payment.getOrderBill().getAddOnDiscountAmount());
                }
                
                orderBill.setVendorFlag(RefundAPIBO.getVendorFlag(paymentRefundMap));                    
                orderBill.setAddOnAmount(payment.getOrderBill().getAddOnAmount());
                orderBill.setFeeAmount(payment.getOrderBill().getFeeAmount());
                
                if(orderBill.getVendorFlag() != null &&
                            orderBill.getVendorFlag().equalsIgnoreCase("SD"))
                {
                    // SDG orders break refund fees out by filling shipping first and then service fee
                    if(payment.getOrderBill().getFeeAmount() > payment.getOrderBill().getShippingFee())
                    {
                        orderBill.setShippingFee(payment.getOrderBill().getShippingFee());
                        orderBill.setServiceFee(payment.getOrderBill().getFeeAmount() - payment.getOrderBill().getShippingFee());
                    }
                    else
                    {
                        orderBill.setShippingFee(payment.getOrderBill().getFeeAmount());
                        orderBill.setServiceFee(0);
                    }
                }
                
                orderBill.setCommissionAmount(payment.getOrderBill().getCommissionAmount());
                orderBill.setWholesaleAmount(payment.getOrderBill().getWholesaleAmount());
                orderBill.setTax(payment.getOrderBill().getTax());
                orderBill.setAdminFee(this.calculateAdminFee(payment.getOrderBill().getCommissionAmount(), request.getParameter("origin_id")));
                orderBill.setWholesaleServiceFee(calculateWholesaleFee(payment.getOrderBill().getWholesaleServiceFee(), payment.getOrderBill().getFeeAmount()));

                //set the miles/points amounts: original as well as the left overs
                orderBill.setMilesPointsAmount(payment.getOrderBill().getMilesPointsAmount());
                orderBill.setMilesPointsOriginal(payment.getOrderBill().getMilesPointsOriginal());

                calculateTaxes(orderBill, con, refund.getOrderDetailId(), 1);
                refund.setOrderBill(orderBill);
                
                refundDAO.insertRefund(refund);
            }
            
            refundApiBO.handleRefundStatusModified(orderDetailId, this.userId, this.con);
        }
        
    }
    
    public void editRefund() throws Exception
    {
        RefundVO refund = null;
        RefundDAO refundDAO = new RefundDAO(con);
        
        int i = 1;
        while(true)
        {
            refund = new RefundVO();
            
            if(request.getParameter("refund_seq_id_" + i) != null 
                && !request.getParameter("refund_seq_id_" + i).equals(""))
            {
                if(request.getParameter("refund_edit_flag_" + i) != null 
                    && request.getParameter("refund_edit_flag_" + i).equals("Y"))
                {
                    refund.setRefundId(request.getParameter("refund_id_" + i));
                    refund.setRefundDispCode(request.getParameter("refund_disp_" + i));
                    refund.setResponsibleParty(request.getParameter("refund_resp_party_" + i));
                    refund.setUpdatedBy(this.getUserId());
                    refund.setComplaintCommOriginTypeId(request.getParameter("complaint_comm_origin_type_id_" + i));
                    refund.setComplaintCommNotificationTypeId(request.getParameter("complaint_comm_notification_type_id_" + i));
                
                    refundDAO.updateRefund(refund);
                }
            }
            else
            {
                break;
            }
            
            i++;
        }
    }

    public HashMap updateReviewer() throws Exception
    {
        RefundVO refund =  new RefundVO();
        RefundDAO refundDAO = new RefundDAO(con);
        
        refund.setRefundId(request.getParameter("refund_id"));
        refund.setReviewedBy(this.getUserId());
        refundDAO.updateRefundReviewer(refund);
        
        // Stop refund review timer
        this.timerStop();
        
        // Start cart timer
        return this.timerStart();
    }
    
    public void reinstateGiftCertificate() throws Exception
    {
        RefundDAO refundDAO = new RefundDAO(con);
        String gcCouponNumber = request.getParameter("gc_coupon_number");
        UpdateGiftCodeRequest updateGiftCodeReq = new UpdateGiftCodeRequest();
		updateGiftCodeReq.setGiftCodeId(gcCouponNumber.trim());
		updateGiftCodeReq.setStatus(this.GC_REFUND_STATUS);
		updateGiftCodeReq.setUpdatedBy(this.getUserId());
		String redeemStatus = GiftCodeUtil.adminUpdateStatus(updateGiftCodeReq);
	}
    
    public boolean validateReinstate() throws Exception
    {
        RefundDAO refundDAO = new RefundDAO(con);
        String gcCouponNumber = request.getParameter("gc_coupon_number");
        
        if(refundDAO.canBeReinstated(gcCouponNumber))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void deleteRefund() throws Exception
    {
        RefundDAO refundDAO = new RefundDAO(con);
        
        int i = 1;
        while(true)
        {                  
            if(request.getParameter("refund_seq_id_" + i) != null 
                && !request.getParameter("refund_seq_id_" + i).equals(""))
            {
                if(request.getParameter("refund_delete_flag_" + i) != null 
                    && request.getParameter("refund_delete_flag_" + i).equals("on"))
                {
                    refundDAO.deleteRefund(request.getParameter("refund_id_" + i));
                    
                    // If refund for a gift card deleted, set the status back to redeemed
                    if(request.getParameter("refund_payment_type_" + i) != null 
                       && request.getParameter("refund_payment_type_" + i).equals("G"))
                    {
                        UpdateGiftCodeRequest updateGiftCodeReq = new UpdateGiftCodeRequest();
                		updateGiftCodeReq.setGiftCodeId(request.getParameter("refund_coupon_number_" + i).trim());
                		updateGiftCodeReq.setStatus(this.GC_REDEEMED_STATUS);
                		updateGiftCodeReq.setUpdatedBy(this.getUserId());
                		String redeemStatus = GiftCodeUtil.adminUpdateStatus(updateGiftCodeReq);
                    }
                }
            }
            else
            {
                break;
            }
            
            i++;
        }
        
        refundApiBO.handleRefundStatusModified(this.request.getParameter(COMConstants.ORDER_DETAIL_ID), this.userId, con);
    }
    
    
    private double calculateAdminFee(double commissionAmount, String orderOrigin) throws Exception
    {
        logger.debug("orderOrigin:" + orderOrigin);
        logger.debug("commissionAmount:" + commissionAmount);
        
        double adminFee = 0;     
    
        // Amazon admin fee
        if(orderOrigin != null && 
            orderOrigin.equalsIgnoreCase(ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, COMConstants.ORDER_ORIGIN_AMAZON)))
        {
            double commissionPercent = new Double(ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, COMConstants.AMAZON_ADMIN_FEE_PCT)).doubleValue();
            double commissionMax = new Double(ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, COMConstants.AMAZON_ADMIN_FEE_MAX)).doubleValue();
            
            logger.debug("amazonComissionPercent:" + commissionPercent);
            logger.debug("amazonComissionMax:" + commissionMax);
            
            adminFee = (commissionAmount * (commissionPercent/100)) > commissionMax?commissionMax:(commissionAmount * (commissionPercent/100));
        }   
        
        logger.debug("adminFee:" + adminFee);
        
        return new BigDecimal(adminFee).setScale(2, BigDecimal.ROUND_HALF_DOWN).doubleValue();
    }
    

    
    private double calculateWholesaleFee(double wholesaleFee, double wholesaleFeeRefund) throws Exception
    {
        // Walmart only origin supporting wholesale fee
        if(request.getParameter("origin_id") != null &&
            request.getParameter("origin_id").equalsIgnoreCase(ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, COMConstants.ORDER_ORIGIN_WALMART)))
        {
            if(wholesaleFeeRefund >= wholesaleFee)
            {
                return wholesaleFee;
            }
            else
            {
                return wholesaleFeeRefund;
            }
        }
        
        return 0;
    }
    
    private double calculateRefundPercent() throws Exception
    {
        
        
        return 0;
    }
    
    private String getUserId() throws Exception
    {
        if (this.userId != null) {
          return this.userId;
        }
        String securityFlag = ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, COMConstants.SECURITY_IS_ON);
        if(securityFlag != null && securityFlag.equals("true"))
        {
            if (SecurityManager.getInstance().getUserInfo(securityToken)!=null)
            {
                return SecurityManager.getInstance().getUserInfo(securityToken).getUserID();
            }
        }
        
        return "TEST";
    }
    
    private HashMap timerStart() throws Exception
    {
        TimerFilter tFilter = new TimerFilter(this.con);
        HashMap timerResults = new HashMap();
        
        timerResults =  tFilter.startTimer( COMConstants.CONS_ENTITY_TYPE_ORDERS, 
                                            request.getParameter("refund_guid"), 
                                            COMConstants.CONS_REFUND_REVIEW_ORIGIN, this.getUserId(),
                                            COMConstants.CONS_COM_COMMENT_ORIGIN_TYPE, 
                                            null);
    
        
        return timerResults;
    }
    
    private void timerStop()         throws Exception
    {
        TimerFilter tFilter = new TimerFilter(this.con);
    
        //stopTimer(timerEntityHistoryId)
        tFilter.stopTimer(request.getParameter(COMConstants.TIMER_ENTITY_HISTORY_ID));
    }
 

  /**
   * @method postC30Refund()
   * @return Document - Update status
   * @throws Exception
   * 
   * This method will be called when UpdateRefundAction determines that the refund type is C30.
   * 
   * A C30 refund is not really a refund - it's a change of payment where a refund will be issued
   * depending on the amount of the Gift Certificate, and a payment record will be created
   * 
   * A transactional approach will be taken where the refund will be issued on the original payment
   * method and a new payment record will be created as an Add Bill, all as one transaction .  
   * The amount of refund and payment will be equal. 
   * 
   * Note that the payment record created for a C30 refund will ALWAYS be a Gift Card
   * 
   */
  public void postC30Refund() throws Exception
  {
    //define the transcation 
    UserTransaction userTransaction = null;

    try 
    {
      // get the initial context
      InitialContext iContext = new InitialContext();

      //get the transaction timeout
      int transactionTimeout = Integer.parseInt((String)ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, COMConstants.TRANSACTION_TIMEOUT_IN_SECONDS));

      // Retrieve the UserTransaction object
      String jndi_usertransaction_entry = ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, COMConstants.JNDI_USERTRANSACTION_ENTRY);
      userTransaction = (UserTransaction)  iContext.lookup(jndi_usertransaction_entry);

      //set the transaction timeout
      userTransaction.setTransactionTimeout(transactionTimeout);

      //start the transaction with the begin method
      userTransaction.begin();
      int status = userTransaction.getStatus();

      //post the refund - note that postRefund() will throw an exception which will rollback the transaction
      postRefund();
      
      //post the payment -- note that applyBilling calls AddBillingBO.applyBilling, which is handling
      //the exception and sending an Document back.  If an error/exception was encountered, 
      //we will throw a new exception
      Document resultsBilling = applyBilling(this.request, this.con);

      //retrieve the <rollback_transaction> from the XML returned while posting the payment
      String rollbackTransaction = DOMUtil.getNodeValue(resultsBilling, "rollback_transaction");
      
      //if rollback_transaction = Y, throw an exception for catch to rollback the transaction
      if (StringUtils.isNotEmpty(rollbackTransaction) && 
          rollbackTransaction.equalsIgnoreCase(COMConstants.CONS_YES)
         )
      {
        throw new Exception("C30 refund/post-payment failed");
      }
      //else assuming everything went well, commit the transaction.
      else
      {
  			userTransaction.commit();
      }
    }
    
    catch (Exception e)
    {
      //rollback
      userTransaction.rollback();
      logger.error(e);
      throw new Exception (e); 
    }
    
    finally
    {
      
    }

  }


  /**
   * Apply billing activity to the database
   * @param Request
   * @param Connection - db connection
   * @return Document - status of updates
   * @throws Exception
   * @throws IOException
   * @throws ParserConfigurationException
   * @throws SAXException
   * @throws TransformerException
   * @throws XSLException
   */

  private Document applyBilling(HttpServletRequest request, Connection conn)
		throws Exception
  {

    // will not be used.  need it to call the AddBillingBO.applyBilling
    CreditCardVO ccVO = null;
    CommonCreditCardVO cccVO = null;
    String billingLevel = "";
    String mgrCode = "";
    String mgrPassword = "";

    String sessionId = null;
    String csrId = null;
    BillingVO bVO = null;
    PaymentVO pVO = new PaymentVO();
    PaymentVO[] payVO = null;
    GcCouponVO gcVO = new GcCouponVO();
    GiftCardVO gdVO = new GiftCardVO();
    ArrayList payments = new ArrayList();
    AddBillingBO abBO = new AddBillingBO(conn);
    CustomerDAO customerDAO = null;
    CustomerVO customerVO = null;
    
    //get csr Id
    sessionId   = request.getParameter(COMConstants.SEC_TOKEN);
    SecurityManager sm = SecurityManager.getInstance();
    UserInfo ui = sm.getUserInfo(sessionId);
    csrId = ui.getUserID();

    //retrieve variables from the request object
    String addonAmount 						=	request.getParameter("addon_amount");
    String addonRemainingTotal 		=	request.getParameter("addon_total");
    String context                = request.getParameter(COMConstants.CONTEXT);
    String feeAmount 							=	request.getParameter("fee_amount");
    String feeRemainingTotal 			=	request.getParameter("fee_total");
    String gcAmount               = request.getParameter("gc_amount");
    String gcAmountRemainder      = request.getParameter("gc_amount_remainder");
    String gcNumber               = request.getParameter("gc_number");
    String gd_pin               = request.getParameter("gd_pin");
    String gdorgc = request.getParameter(COMConstants.GD_OR_GC);
    String orderDetailId          = request.getParameter(COMConstants.ORDER_DETAIL_ID);
    String orderGuid              = request.getParameter(COMConstants.ORDER_GUID);
    String productAmount					=	request.getParameter("product_amount");
    String productRemainingTotal	= request.getParameter("product_total");
    String productType            = request.getParameter(COMConstants.PRODUCT_TYPE);
    String shipMethod             = request.getParameter(COMConstants.SHIP_METHOD);
    String taxAmount 							=	request.getParameter("tax_amount");
    String taxRemainingTotal 	    = null;
    String vendorFlag             = request.getParameter("vendor_flag");
    String customerId = null;
    if(request.getParameter(COMConstants.CUSTOMER_ID)!=null)
    {
      customerId = request.getParameter(COMConstants.CUSTOMER_ID);
    }
    
    //Populate a billingVO for non CART billing.
    bVO = new BillingVO();
    
    //Product amount
    if(StringUtils.isNotEmpty(productAmount) && StringUtils.isNumeric(StringUtils.replace(productAmount, ".", "")))
      bVO.setProductAmt(Double.valueOf(productAmount).doubleValue());
    
    //Addon amount
    if(StringUtils.isNotEmpty(addonAmount)  && StringUtils.isNumeric(StringUtils.replace(addonAmount, ".", "")))
      bVO.setAddOnAmt(Double.valueOf(addonAmount).doubleValue());
    
    //Tax amount
    if(StringUtils.isNotEmpty(taxAmount) && StringUtils.isNumeric(StringUtils.replace(taxAmount, ".", "")))
      bVO.setProductTax(Double.valueOf(taxAmount).doubleValue());
    
    //Discount product amount
    bVO.setDiscountProdPrice(bVO.getProductAmt());
    
     // If it is a fresh cut product
    if  ( productType != null                      &&
          ( productType.equalsIgnoreCase(COMConstants.CONS_PRODUCT_TYPE_FRECUT) ||
            productType.equalsIgnoreCase(COMConstants.CONS_PRODUCT_TYPE_SDFC)
          )
        )
    {
      if(StringUtils.isNotEmpty(feeAmount)  && StringUtils.isNumeric(StringUtils.replace(feeAmount, ".", "")))
        bVO.setServiceFee(Double.valueOf(feeAmount).doubleValue());
    }
    // If it is a same day gift
    else if ( ( StringUtils.isNotEmpty(shipMethod) &&
                shipMethod.equalsIgnoreCase(COMConstants.CONS_DELIVERY_SAME_DAY_CODE)
              ) &&
              ( StringUtils.isNotEmpty(productType) &&
                productType.equalsIgnoreCase(COMConstants.CONS_PRODUCT_TYPE_SDG)
              )
            )
    {
      if(StringUtils.isNotEmpty(feeAmount)  && StringUtils.isNumeric(StringUtils.replace(feeAmount, ".", "")))
        bVO.setShippingFee(Double.valueOf(feeAmount).doubleValue());
    }

    // If it is dropship
    else if ( StringUtils.isNotEmpty(vendorFlag) &&
              vendorFlag.equalsIgnoreCase(COMConstants.CONS_YES)
            )
    {
      if(StringUtils.isNotEmpty(feeAmount)  && StringUtils.isNumeric(StringUtils.replace(feeAmount, ".", "")))
        bVO.setShippingFee(Double.valueOf(feeAmount).doubleValue());
    }
    // If it is floral
    else
    {
      if(StringUtils.isNotEmpty(feeAmount)  && StringUtils.isNumeric(StringUtils.replace(feeAmount, ".", "")))
        bVO.setServiceFee(Double.valueOf(feeAmount).doubleValue());
    }

    bVO.setBillStatus("Unbilled");
    bVO.setCreatedBy(csrId);
    bVO.setDiscountType("N");
    bVO.setOrderDetailId(Long.valueOf(orderDetailId).longValue());
    bVO.setAddtBillInd("Y");

    //Populate the PaymentVO 
    pVO.setOrderGuid(orderGuid);
    if (StringUtils.isNotEmpty(gcAmountRemainder) && StringUtils.isNumeric(StringUtils.replace(gcAmountRemainder, ".", "")))
    { 
    	pVO.setCreditAmount(Double.parseDouble(gcAmountRemainder)); 
    }
    
    if (gdorgc != null && gdorgc.equals(COMConstants.GIFT_CARD_PAYMENT))
    {
    	pVO.setPaymentType("GD");
    	pVO.setCardNumber(gcNumber);
    	pVO.setGiftCardPin(gd_pin);
    	pVO.setCreditAmount(Double.parseDouble(gcAmountRemainder));
    }
    else{
    pVO.setPaymentType("GC");  
    }
    pVO.setUpdatedBy(csrId);
    pVO.setPaymentInd("P");
    pVO.setAuthDate(new Date());
    pVO.setGcCouponNumber(gcNumber);
    payments.add(pVO);

    //Populate the Gift Card VO,  ccVO, cccVO
    if(pVO.getPaymentType().equalsIgnoreCase("GD")){
    	gdVO.setGcCouponNumber(gcNumber);
        gdVO.setBalance(gcAmount);
        gdVO.setUpdatedBy(csrId);
        gdVO.setPin(gd_pin);
        
        ccVO = gdVO.getCreditCardVO();
        
        if (customerDAO == null) {
        	customerDAO = new CustomerDAO(conn);                
        }
        List customerList = customerDAO.getCustomerById(customerId);        
        if(customerList != null && customerList.size() >0)
        {
            //retrieving by the primary key, so only 1 customer should be returned
            customerVO = (CustomerVO)customerList.get(0);
        }
        
        cccVO = new CommonCreditCardVO();
        cccVO.setCustomerId(customerId);
        cccVO.setCreditCardNumber(gcNumber);
        cccVO.setCreditCardType("GD");
        
        cccVO.setName(customerVO.getFirstName()+" "+customerVO.getLastName());
        cccVO.setAddressLine(customerVO.getAddress1());
        cccVO.setAddress2(customerVO.getAddress2());
        cccVO.setCity(customerVO.getCity());
        cccVO.setZipCode(customerVO.getZipCode());
        cccVO.setCountry(customerVO.getCountry());
    }
    else{
    gcVO.setGcCouponNumber(gcNumber);
    gcVO.setAmount(gcAmount);
    gcVO.setUpdatedBy(csrId);
    }
    
    payVO = (PaymentVO[]) payments.toArray(new PaymentVO [payments.size()]); 
    
    Document applyBillingDoc = abBO.applyBillingWithoutTransaction(orderDetailId, orderGuid, billingLevel,   
                                                    mgrCode, mgrPassword, context, bVO, payVO, 
                                                    gcVO, gdVO, ccVO, cccVO, csrId);
    return applyBillingDoc;


  }


  /**
   * @method processCancel()
   * @param OrderDetailVO
   * @param refundCart - if true then the request comes from refunding the shopping cart;
   *                     otherwise the request comes from refunding an order detai.
   * @return n/a
   * @throws Exception
   * 
   * This method mimics the same functionality as exhibited in CustomerHoldAction.processCancel()
   * 
   * It will be invoked if the CSR issues a full refund on an order that is in "Held" status
   * 
   */
  private void processCancelHeldOrders(OrderDetailVO odVO, boolean refundCart)
    throws Exception
  {

    //get an instance of the configuration util
    ConfigurationUtil configUtil = ConfigurationUtil.getInstance();

    //get the user info 
    SecurityManager securityManager = SecurityManager.getInstance();
    UserInfo userInfo = securityManager.getUserInfo(this.request.getParameter("securitytoken"));

    //retrieve the request parameter
    String responsibleParty = this.request.getParameter("responsible_party");
    String commentOrigin = configUtil.getProperty(COMConstants.PROPERTY_FILE, "CUST_HOLD_COMMENT_ORIGIN");
    String refundDisposition = null;
    String refundOrigin = null;
    if(refundCart) {
        refundDisposition = this.request.getParameter("refund_disp_code");
        refundOrigin = "RefundShoppingCart";
    } else {
        refundDisposition = this.request.getParameter("disp_code");
        refundOrigin = "RefundOrderDetail";
    }
    
    //instantiate the DAOs
    CustomerDAO customerDAO = new CustomerDAO(this.con);
    OrderDAO orderDAO = new OrderDAO(this.con);
    LossPreventionDAO lossDAO = new LossPreventionDAO(this.con);    

    //build the Order VO for this guid.
    OrderVO oVO = orderDAO.getOrder(odVO.getOrderGuid());

    //delete the record from Queue (LP and CREDIT)
    lossDAO.deleteFromQueue(odVO.getOrderDetailId(),LOSS_PREVENTION_QUEUE,userInfo.getUserID());
    lossDAO.deleteFromQueue(odVO.getOrderDetailId(),CREDIT_QUEUE,userInfo.getUserID());
    
    //Create and insert a Comment object
    CommentDAO commentDAO = new CommentDAO(this.con);
    CommentsVO commentsVO = new CommentsVO();        
    commentsVO.setCommentOrigin(commentOrigin);
    commentsVO.setCommentType("Order");
    commentsVO.setCustomerId(new Long(oVO.getCustomerId()).toString()); 
    commentsVO.setOrderGuid(odVO.getOrderGuid());
    commentsVO.setOrderDetailId(odVO.getOrderDetailId());
    commentsVO.setUpdatedBy(userInfo.getUserID());  
    commentsVO.setComment("Order was in Held Status - " + userInfo.getUserID() + " refunded it from " + refundOrigin + ".");
    commentDAO.insertComment(commentsVO);

    //Defect 3737. Delete the Held Orders from ORDER_HOLD table regardless of hold reason.
    UpdateOrderDAO updateOrderDAO = new UpdateOrderDAO(this.con);
    updateOrderDAO.deleteOrderHold(odVO.getOrderDetailId());

    //create a list of all orders that need to be refunded
    List orderList = new ArrayList();
    orderList.add(odVO.getOrderDetailId());

    //trigger a refund
    this.postFullRefund(orderList, refundDisposition, responsibleParty, COMConstants.REFUND_STATUS_UNBILLED);

    //Updates clean.order_details.order_disp_code to 'Processed'
    orderDAO.postCancelHeldOrder(odVO.getOrderDetailId(), userInfo.getUserID());
  }


  private int calculateMilesPoints(RefundVO refund, double maxMilesPointsAvailableToBeRefunded, String mpRedemptionRateAmt)
    throws Exception
  {
    int milesPoints = 0;
    int milesPointToBeRefunded = 0;

    PaymentMethodMilesPointsVO pmmpVO = FTDCommonUtils.getPaymentMethodMilesPoints(refund.getPaymentMethodId(), this.con);
    OrderBillVO obVO = refund.getOrderBill();
    double productAmount = 0; 
    if (obVO.getProductAmount() > 0 )
      productAmount = obVO.getProductAmount(); 
    else
      productAmount = obVO.getGrossProductAmount() - obVO.getDiscountAmount(); 

    double totalRefundAmount = productAmount + obVO.getAddOnAmount() + obVO.getFeeAmount() + obVO.getTax();

    logger.info("For order detail id = " + refund.getOrderDetailId() + 
                ", total amount sent to be converted = " + new Double(totalRefundAmount).toString() + 
                " and redemption rate = " + mpRedemptionRateAmt + 
                " and DollarToMpOperator = " + pmmpVO.getDollarToMpOperator() + 
                " and RoundingScaleQty = " + pmmpVO.getRoundingScaleQty() + 
                " and RoundingMethodId = " + pmmpVO.getRoundingMethodId());

    if (pmmpVO != null)
    {
      milesPoints = 
          FTDCommonUtils.convertDollarsToMilesPoints((new Double(totalRefundAmount)).toString(), mpRedemptionRateAmt, 
                                                     pmmpVO.getDollarToMpOperator(), pmmpVO.getRoundingScaleQty(), 
                                                     pmmpVO.getRoundingMethodId());
    }
    else
    {
      throw new Exception("No entry found in payment_method_miles_points for payment method: " + refund.getPaymentMethodId());
    }

    if (milesPoints > maxMilesPointsAvailableToBeRefunded)
      milesPointToBeRefunded = new Double(maxMilesPointsAvailableToBeRefunded).intValue();
    else
      milesPointToBeRefunded = milesPoints;

    return milesPointToBeRefunded;
  }


  public void setApplySurchargeCode(String applySurchargeCode)
  {
    this.applySurchargeCode = applySurchargeCode;
  }

  public String getApplySurchargeCode()
  {
    return applySurchargeCode;
  }


  /**
   * Formats a double as a String ensuring 2 decimal places
   * @param amount
   * @return
   */
  private String formatCurrencyString(double amount)
  {
    DecimalFormat twoDec = new DecimalFormat("#,###,##0.00");
    twoDec.setGroupingUsed(true);
    return twoDec.format(amount);
  }

	/**
	 * @param obVO
	 * @param connection
	 * @param orderDetailId
	 * @param refundPercentage
	 * @throws Exception
	 */
	private void calculateTaxes(OrderBillVO obVO, Connection connection, String orderDetailId, double refundPercentage) throws Exception {
		
		if (obVO.getTax() == 0 || StringUtils.isEmpty("orderDetailId")) {
			return;
		}
    
		// Get Current Buckets
		OrderDAO oDAO = null;
		OrderBillVO orderBillTax = null;
		
		try {
			oDAO = new OrderDAO(connection);
			orderBillTax = oDAO.getOrderItemRemainingTaxes(orderDetailId);
		} catch(Exception e) {
			logger.error("Error caught getting the original taxes ", e);
			return;
		}
    
	    try {	    	
	    	 BigDecimal refundRate = new BigDecimal(refundPercentage);
	    	 if(orderBillTax.getTax1Rate() != null && orderBillTax.getTax1Amount().compareTo(BigDecimal.ZERO) > 0) {
	    		 obVO.setTax1Name(orderBillTax.getTax1Name());
	    		 obVO.setTax1Description(orderBillTax.getTax1Description());
	    		 obVO.setTax1Rate(orderBillTax.getTax1Rate());
	    		 obVO.setTax1Amount(orderBillTax.getTax1Amount().multiply(refundRate).setScale(5, BigDecimal.ROUND_HALF_UP));
	    	 }
	    	 
	    	 if(orderBillTax.getTax2Rate() != null && orderBillTax.getTax2Amount().compareTo(BigDecimal.ZERO) > 0) {
	    		 obVO.setTax2Name(orderBillTax.getTax2Name());
	    		 obVO.setTax2Description(orderBillTax.getTax2Description());
	    		 obVO.setTax2Rate(orderBillTax.getTax2Rate());
	    		 obVO.setTax2Amount(orderBillTax.getTax2Amount().multiply(refundRate).setScale(5, BigDecimal.ROUND_HALF_UP));
	    	 }
	    	 
	    	 if(orderBillTax.getTax3Rate() != null && orderBillTax.getTax3Amount().compareTo(BigDecimal.ZERO) > 0) {
	    		 obVO.setTax3Name(orderBillTax.getTax3Name());
	    		 obVO.setTax3Description(orderBillTax.getTax3Description());
	    		 obVO.setTax3Rate(orderBillTax.getTax3Rate());
	    		 obVO.setTax3Amount(orderBillTax.getTax3Amount().multiply(refundRate).setScale(5, BigDecimal.ROUND_HALF_UP));
	    	 }
	    	 
	    	 if(orderBillTax.getTax4Rate() != null && orderBillTax.getTax4Amount().compareTo(BigDecimal.ZERO) > 0) {
	    		 obVO.setTax4Name(orderBillTax.getTax4Name());
	    		 obVO.setTax4Description(orderBillTax.getTax4Description());
	    		 obVO.setTax4Rate(orderBillTax.getTax4Rate());
	    		 obVO.setTax4Amount(orderBillTax.getTax4Amount().multiply(refundRate).setScale(5, BigDecimal.ROUND_HALF_UP));
	    	 }
	    	 
	    	 if(orderBillTax.getTax5Rate() != null && orderBillTax.getTax5Amount().compareTo(BigDecimal.ZERO) > 0) {
	    		 obVO.setTax5Name(orderBillTax.getTax5Name());
	    		 obVO.setTax5Description(orderBillTax.getTax5Description());
	    		 obVO.setTax5Rate(orderBillTax.getTax5Rate());
	    		 obVO.setTax5Amount(orderBillTax.getTax5Amount().multiply(refundRate).setScale(5, BigDecimal.ROUND_HALF_UP));
	    	 }
	    	 
	    } catch (Exception e) {
			logger.error("Unable to calculate taxes for refund amount ", e);
		}
  }
  
	/** Convenient method to check if the customer used Gift card for payment.
	 * @param paymentList
	 * @return
	 */
	private boolean isGDPaymentExists(List paymentList) {
		if (paymentList != null && paymentList.size() > 0) {
			Iterator<PaymentVO> paymentIterator = paymentList.iterator();
			while (paymentIterator.hasNext()) {
				PaymentVO paymentVO = (PaymentVO) paymentIterator.next();
				if (paymentVO.getPaymentMethodId() != null
						&& paymentVO.getPaymentMethodId().equals(GIFT_CARD_PAYMENT_TYPE)) {
						return true;
				}
			}
		}
		return false;
	}

	/**
	 * This setter enables us to unit test the RefundBO
	 * @param refundApiBO
	 */
	public void setRefundApiBO(RefundAPIBO refundApiBO) {
		this.refundApiBO = refundApiBO;
	}

}

