package com.ftd.customerordermanagement.bo;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.dao.CustomerDAO;
import com.ftd.customerordermanagement.dao.LockDAO;
import com.ftd.customerordermanagement.util.BasePageBuilder;
import com.ftd.customerordermanagement.vo.CustomerPhoneVO;
import com.ftd.customerordermanagement.vo.CustomerVO;
import com.ftd.customerordermanagement.vo.DirectMailVO;
import com.ftd.customerordermanagement.vo.EmailVO;
import com.ftd.customerordermanagement.vo.MembershipVO;
import com.ftd.ftdutilities.FieldUtils;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.XMLFormat;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.security.SecurityManager;
import com.ftd.security.cache.vo.UserInfo;


public class LoadCustomerAccountBO
{

/*******************************************************************************************
 * Class level variables
 *******************************************************************************************/
  private HttpServletRequest  request                           = null;
  private Connection          con                               = null;
  private static Logger       logger                            = new Logger("com.ftd.customerordermanagement.bo.LoadCustomerAccountBO");

  private String              action                            = "";
  private String              csrId                             = null;
  private String              customerId                        = null;
  private CustomerVO          customerVO                        = new CustomerVO();
  private String              displayPage                       = null;
  private HashMap             hashXML                           = new HashMap();
  private long                lCustomerId;
  private String              masterOrderNumber                 = null;
  private String              orderGuid                         = null;
  private HashMap             pageData                          = new HashMap();
  private String              sessionId                         = null;
  private String              emailHistoryUrl                   = null;


  private String              ucBusinessName	                  = null;
  private String              ucCustomerAddress1	              = null;
  private String              ucCustomerAddress2	              = null;
  private String              ucCustomerBirthday	              = null;
  private Calendar            ucCustomerBirthdayCal;
  private String              ucCustomerCity	                  = null;
  private String              ucCustomerCounty 	                = null;
  private String              ucCustomerCountry	                = null;
  private String              ucCustomerFirstName	              = null;
  private String              ucCustomerId                      = null;
  private String              ucCustomerLastName	              = null;
  private String              ucCustomerState	                  = null;
  private String              ucCustomerZipCode	                = null;
  private String              ucDaytimeCustomerExtension        = null;
  private String              ucDaytimeCustomerPhoneId          = null;
  private String              ucDaytimeCustomerPhoneNumber      = null;
  private HashMap             ucDirectMailSubscribeStatusHash   = new HashMap();
  private int                 ucDirectMailSubscribeStatusCount	= 0;
  private String              ucDisplayPage                     = null;
  private String              ucEveningCustomerExtension        = null;
  private boolean             ucVipCustomer 	                = false;
  private String              ucEveningCustomerPhoneId          = null;
  private String              ucEveningCustomerPhoneNumber      = null;
  private long                ucLongCustomerId;
  private String              ucMasterOrderNumber               = null;
  private String              ucOrderGuid                       = null;

  private String              dataExists                        = "";



/*******************************************************************************************
 * Constructor 1
 *******************************************************************************************/
  public LoadCustomerAccountBO()
  {
  }


/*******************************************************************************************
 * Constructor 2
 *******************************************************************************************/
  public LoadCustomerAccountBO(HttpServletRequest request, Connection con)
  {
    this.request = request;
    this.con = con;
  }


/*******************************************************************************************
  * processRequest()
  ******************************************************************************************
  * Besides the constructor, this is the only public method in this class.
  * This method will accept a string called action and after interogating the value, will
  * process accordingly.  It will generate the search criteria, the page details, and the
  * valid data to be passed back to the calling class in a HashMap.
  * Note that the search criteria and page details are returned back as HashMap objects,
  * while all other data is returned as XML objects.
  *
  * @param  String - action
  * @return HashMap - contains 0 - many XML documents of data, 1 HashMap each for page
  *                   details and search criteria
  * @throws
  */
  public HashMap processRequest() throws Exception
  {
    //HashMap that will be returned to the calling class
    HashMap returnHash = new HashMap();

    //Get info from the request object
    getRequestInfo(this.request);

    //Get config file info
    getConfigInfo();

    boolean lockSuccessful = true;

    //issue a lock
    lockSuccessful = processRetrieveLock();

    //retrieve customer account info
    if (lockSuccessful)
    {
      if (this.action.equalsIgnoreCase(COMConstants.ACTION_CHECK_CUSTOMER_ACCOUNT))
      {
        //set the XSL parameter, which governs the page to be loaded
        this.pageData.put("XSL",                              COMConstants.XSL_CHECK_CUSTOMER_ACCOUNT);
      }
      else
      {
        //get the customer account information
        processCustomerAccount();

        //get the states
        processStates();

        //get the countries
        processCountries();
        
        //get the services
        processServices();

        //set the XSL parameter, which governs the page to be loaded
        this.pageData.put("XSL",                              COMConstants.XSL_UPDATE_CUSTOMER_ACCOUNT);
      }
    }
    else
    {
      //set the XSL parameter, which governs the page to be loaded
      this.pageData.put("XSL",                              COMConstants.XSL_CHECK_CUSTOMER_ACCOUNT);
    }

    //populate page data
    populatePageData();


    //At this point, we should have three HashMaps.
    // HashMap1 = hashXML         - hashmap that contains zero to many Documents
    // HashMap2 = pageData        - hashmap that contains String data at page level
    //
    //Combine all of the above HashMaps into one HashMap, called returnHash

    //retrieve all the Documents from hashXML and put them in returnHash
    //retrieve the keyset
    Set ks1 = hashXML.keySet();
    Iterator iter = ks1.iterator();
    String key = null;
    Document doc = null;

    //Iterate thru the keyset
    while(iter.hasNext())
    {
      //get the key
      key = iter.next().toString();

      //retrieve the XML document
      doc = (Document) hashXML.get(key);

      //put the XML object in the returnHash
      returnHash.put(key, (Document) doc);
    }

    //append pageData to returnHash
    returnHash.put("pageData",        (HashMap)this.pageData);

    return returnHash;
  }



/*******************************************************************************************
  * getRequestInfo(HttpServletRequest request)
  ******************************************************************************************
  * Retrieve the info from the request object
  *
  * @param  HttpServletRequest - request
  * @return none
  * @throws none
  */
  private void getRequestInfo(HttpServletRequest request) throws Exception
  {
    /****************************************************************************************
     *  Retrieve Action request
     ***************************************************************************************/
    if(request.getAttribute(COMConstants.ACTION) != null)
      this.action = request.getAttribute(COMConstants.ACTION).toString();
    else if(request.getParameter(COMConstants.ACTION)!=null)
      this.action = request.getParameter(COMConstants.ACTION);


    /****************************************************************************************
     *  Retrieve parameters used to redirect the user to calling page
     ***************************************************************************************/
    //retrieve the master order number
    if(request.getParameter(COMConstants.MASTER_ORDER_NUMBER)!=null)
      this.masterOrderNumber = request.getParameter(COMConstants.MASTER_ORDER_NUMBER);

    //retrieve the order guid
    if(request.getParameter(COMConstants.ORDER_GUID)!=null)
      this.orderGuid = request.getParameter(COMConstants.ORDER_GUID);


    /****************************************************************************************
     *  Retrieve parameters that were persisted using the data filter
     ***************************************************************************************/
    if(request.getParameter(COMConstants.DATA_EXISTS) !=null)
        this.dataExists = request.getParameter(COMConstants.DATA_EXISTS);

    //retrieve the first name
    if(request.getParameter(COMConstants.UC_CUSTOMER_FIRST_NAME)!=null)
      if(!request.getParameter(COMConstants.UC_CUSTOMER_FIRST_NAME).equalsIgnoreCase(""))
        this.ucCustomerFirstName = request.getParameter(COMConstants.UC_CUSTOMER_FIRST_NAME);

    //retrieve the last name
    if(request.getParameter(COMConstants.UC_CUSTOMER_LAST_NAME)!=null)
      if(!request.getParameter(COMConstants.UC_CUSTOMER_LAST_NAME).equalsIgnoreCase(""))
        this.ucCustomerLastName = request.getParameter(COMConstants.UC_CUSTOMER_LAST_NAME);

    //retrieve the business name
    if(request.getParameter(COMConstants.UC_BUSINESS_NAME)!=null)
      if(!request.getParameter(COMConstants.UC_BUSINESS_NAME).equalsIgnoreCase(""))
        this.ucBusinessName = request.getParameter(COMConstants.UC_BUSINESS_NAME);

    //retrieve the address 1
    if(request.getParameter(COMConstants.UC_CUSTOMER_ADDRESS_1)!=null)
      if(!request.getParameter(COMConstants.UC_CUSTOMER_ADDRESS_1).equalsIgnoreCase(""))
        this.ucCustomerAddress1 = request.getParameter(COMConstants.UC_CUSTOMER_ADDRESS_1);

    //retrieve the address 2
    if(request.getParameter(COMConstants.UC_CUSTOMER_ADDRESS_2)!=null)
      if(!request.getParameter(COMConstants.UC_CUSTOMER_ADDRESS_2).equalsIgnoreCase(""))
        this.ucCustomerAddress2 = request.getParameter(COMConstants.UC_CUSTOMER_ADDRESS_2);

    //retrieve the city
    if(request.getParameter(COMConstants.UC_CUSTOMER_CITY)!=null)
      if(!request.getParameter(COMConstants.UC_CUSTOMER_CITY).equalsIgnoreCase(""))
        this.ucCustomerCity = request.getParameter(COMConstants.UC_CUSTOMER_CITY);

    //retrieve the state
    if(request.getParameter(COMConstants.UC_CUSTOMER_STATE)!=null)
      if(!request.getParameter(COMConstants.UC_CUSTOMER_STATE).equalsIgnoreCase(""))
        this.ucCustomerState = request.getParameter(COMConstants.UC_CUSTOMER_STATE);

    //retrieve the zip
    if(request.getParameter(COMConstants.UC_CUSTOMER_ZIP_CODE)!=null)
      if(!request.getParameter(COMConstants.UC_CUSTOMER_ZIP_CODE).equalsIgnoreCase(""))
        this.ucCustomerZipCode = request.getParameter(COMConstants.UC_CUSTOMER_ZIP_CODE);

    //retrieve the country
    if(request.getParameter(COMConstants.UC_CUSTOMER_COUNTRY)!=null)
      if(!request.getParameter(COMConstants.UC_CUSTOMER_COUNTRY).equalsIgnoreCase(""))
        this.ucCustomerCountry = request.getParameter(COMConstants.UC_CUSTOMER_COUNTRY);


    //retrieve the county
    if(request.getParameter(COMConstants.UC_CUSTOMER_COUNTY)!=null)
      if(!request.getParameter(COMConstants.UC_CUSTOMER_COUNTY).equalsIgnoreCase(""))
        this.ucCustomerCounty = request.getParameter(COMConstants.UC_CUSTOMER_COUNTY);

    //retrieve the brithday
    if(request.getParameter(COMConstants.UC_CUSTOMER_BIRTHDAY)!=null)
      if(!request.getParameter(COMConstants.UC_CUSTOMER_BIRTHDAY).equalsIgnoreCase(""))
      {
        this.ucCustomerBirthday = request.getParameter(COMConstants.UC_CUSTOMER_BIRTHDAY);
        if (this.ucCustomerBirthday != null)
        {
          SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
          this.ucCustomerBirthdayCal = Calendar.getInstance();
          this.ucCustomerBirthdayCal.setTime(inputFormat.parse(this.ucCustomerBirthday));
        }
      }


    /** retrieve the day time phone info **/
    //retrieve the phone id
    if(request.getParameter(COMConstants.UC_DAYTIME_CUSTOMER_PHONE_ID)!=null)
      if(!request.getParameter(COMConstants.UC_DAYTIME_CUSTOMER_PHONE_ID).equalsIgnoreCase(""))
        this.ucDaytimeCustomerPhoneId = request.getParameter(COMConstants.UC_DAYTIME_CUSTOMER_PHONE_ID);

    //retrieve the phone number
    if(request.getParameter(COMConstants.UC_DAYTIME_CUSTOMER_PHONE_NUMBER)!=null)
      if(!request.getParameter(COMConstants.UC_DAYTIME_CUSTOMER_PHONE_NUMBER).equalsIgnoreCase(""))
        this.ucDaytimeCustomerPhoneNumber = request.getParameter(COMConstants.UC_DAYTIME_CUSTOMER_PHONE_NUMBER);

    //retrieve the extension
    if(request.getParameter(COMConstants.UC_DAYTIME_CUSTOMER_EXTENSION)!=null)
      if(!request.getParameter(COMConstants.UC_DAYTIME_CUSTOMER_EXTENSION).equalsIgnoreCase(""))
        this.ucDaytimeCustomerExtension = request.getParameter(COMConstants.UC_DAYTIME_CUSTOMER_EXTENSION);



    /** retrieve the evening phone info **/
    //retrieve the phone id
    if(request.getParameter(COMConstants.UC_EVENING_CUSTOMER_PHONE_ID)!=null)
      if(!request.getParameter(COMConstants.UC_EVENING_CUSTOMER_PHONE_ID).equalsIgnoreCase(""))
        this.ucEveningCustomerPhoneId = request.getParameter(COMConstants.UC_EVENING_CUSTOMER_PHONE_ID);

    //retrieve the phone number
    if(request.getParameter(COMConstants.UC_EVENING_CUSTOMER_PHONE_NUMBER)!=null)
      if(!request.getParameter(COMConstants.UC_EVENING_CUSTOMER_PHONE_NUMBER).equalsIgnoreCase(""))
        this.ucEveningCustomerPhoneNumber = request.getParameter(COMConstants.UC_EVENING_CUSTOMER_PHONE_NUMBER);

    //retrieve the extension
    if(request.getParameter(COMConstants.UC_EVENING_CUSTOMER_EXTENSION)!=null)
      if(!request.getParameter(COMConstants.UC_EVENING_CUSTOMER_EXTENSION).equalsIgnoreCase(""))
        this.ucEveningCustomerExtension = request.getParameter(COMConstants.UC_EVENING_CUSTOMER_EXTENSION);

    //retrieve the vip flag
    if(request.getParameter(COMConstants.UC_VIP_CUSTOMER)!=null)
        this.ucVipCustomer = FieldUtils.convertStringToBoolean(request.getParameter(COMConstants.UC_VIP_CUSTOMER));



    /** retrieve the direct mail status info **/

    //retrieve the count
    if(request.getParameter(COMConstants.UC_DIRECT_MAIL_SUBSCRIBE_STATUS_COUNT)!=null)
    {
      if(!request.getParameter(COMConstants.UC_DIRECT_MAIL_SUBSCRIBE_STATUS_COUNT).toString().equalsIgnoreCase(""))
      {
        this.ucDirectMailSubscribeStatusCount =
          Integer.valueOf(request.getParameter(COMConstants.UC_DIRECT_MAIL_SUBSCRIBE_STATUS_COUNT)).intValue();
      }
    }

    String status;
    String company;

    //fill the data in the array list created above
    for (int x=1; x <= this.ucDirectMailSubscribeStatusCount; x++)
    {
      status = null;
      company = null;

      //retrieve the direct mail statuse
      if(request.getParameter(COMConstants.UC_DIRECT_MAIL_SUBSCRIBE_STATUS + "_" + x)!=null)
        if(!request.getParameter(COMConstants.UC_DIRECT_MAIL_SUBSCRIBE_STATUS + "_" + x).equalsIgnoreCase(""))
          status = request.getParameter(COMConstants.UC_DIRECT_MAIL_SUBSCRIBE_STATUS + "_" + x);

      //retrieve the direct mail company id
      if(request.getParameter(COMConstants.UC_DIRECT_MAIL_COMPANY_ID + "_" + x)!=null)
        if(!request.getParameter(COMConstants.UC_DIRECT_MAIL_COMPANY_ID + "_" + x).equalsIgnoreCase(""))
          company = request.getParameter(COMConstants.UC_DIRECT_MAIL_COMPANY_ID + "_" + x);

      if (company != null)
        this.ucDirectMailSubscribeStatusHash.put(company, status);

    }



    /****************************************************************************************
     *  Retrieve Page Parameters
     ***************************************************************************************/
    //retrieve the parameter that shows the page name which triggered this request.  It could
    //either be the customer account page or the customer shopping cart page.
    if(request.getParameter(COMConstants.DISPLAY_PAGE)!=null)
    {
        this.displayPage = request.getParameter(COMConstants.DISPLAY_PAGE);
    }


    /****************************************************************************************
     *  Retrieve order/search specific parameters
     ***************************************************************************************/
    //retrieve the customer id
    if(request.getParameter(COMConstants.CUSTOMER_ID)!=null)
    {
      this.customerId = request.getParameter(COMConstants.CUSTOMER_ID);
      this.lCustomerId = Long.valueOf(this.customerId).longValue();
    }


    /****************************************************************************************
     *  Retrieve Security Info
     ***************************************************************************************/
      this.sessionId   = request.getParameter(COMConstants.SEC_TOKEN);
      //check to see if the user-role can view VIP label
      
     if(SecurityManager.getInstance().assertPermission(request.getParameter("context"), sessionId, COMConstants.VIP_CUSTOMER_UPDATE, COMConstants.YES))
     { 
       this.pageData.put("vipCustUpdatePermission", "Y");
     }
     else
     {
       this.pageData.put("vipCustUpdatePermission", "N");
     }
  }


/*******************************************************************************************
  * getConfigInfo()
  ******************************************************************************************
  * Retrieve the info from the configuration file
  *
  * @param none
  * @return
  * @throws none
  */
  private void getConfigInfo() throws Exception
  {

    ConfigurationUtil cu = null;
    cu = ConfigurationUtil.getInstance();

    //get EMAIL_HISTORY_URL
    if(cu.getProperty(COMConstants.PROPERTY_FILE, COMConstants.CONS_EMAIL_HISTORY_URL) != null)
    {
      this.emailHistoryUrl = cu.getFrpGlobalParm(COMConstants.COM_CONTEXT, COMConstants.CONS_EMAIL_HISTORY_URL);
    }

    //get csr Id
    String security = (String) cu.getProperty(COMConstants.PROPERTY_FILE,COMConstants.SECURITY_IS_ON);
    boolean securityIsOn = security.equalsIgnoreCase("true") ? true : false;

   //delete this before going to production.  the following should only check on securityIsOn
    if(securityIsOn || this.sessionId != null)
    {
      SecurityManager sm = SecurityManager.getInstance();
      UserInfo ui = sm.getUserInfo(this.sessionId);
      this.csrId = ui.getUserID();
    }

  }


/*******************************************************************************************
  * populatePageData()
  ******************************************************************************************
  * Populate the page data with the remainder of the fields
  *
  * @param none
  * @return
  * @throws none
  */
  private void populatePageData()
  {
    //store the customer id passed in the page data
    this.pageData.put(COMConstants.PD_UC_CUSTOMER_ID,         this.customerId);

    //store the display page
    this.pageData.put(COMConstants.PD_DISPLAY_PAGE,           this.displayPage);

    //store the master order number
    this.pageData.put(COMConstants.PD_UC_MASTER_ORDER_NUMBER, this.masterOrderNumber);

    //store the order guid
    this.pageData.put(COMConstants.PD_UC_ORDER_GUID,          this.orderGuid);

    //store the data exists
    this.pageData.put(COMConstants.PD_DATA_EXISTS,            this.dataExists);

    //store the EMAIL HISTORY URL in the page data
    this.pageData.put(COMConstants.PD_EMAIL_HISTORY_URL,        this.emailHistoryUrl);
    
    this.pageData.put(COMConstants.UC_VIP_CUSTOMER, this.ucVipCustomer);
    
  }


/*******************************************************************************************
  * processRetrieveLock()
  ******************************************************************************************
  * This method will process the action type of
  *
  * @param  none
  * @return none
  * @throws none
  *
  */
  private boolean processRetrieveLock() throws Exception
  {
    boolean lockSuccessful = true;

    //Call retrieveLockInfo which will call the DAO to retrieve the results
    Document lockInfo = DOMUtil.getDocument();

    lockInfo = retrieveLockInfo();

    //get the lock obtained indicator.
    String sLockedObtained = DOMUtil.getNodeValue(lockInfo, "OUT_LOCK_OBTAINED");

    //if locked_obtained = yes, you have the lock (regardless if you just established a lock
    //or have had it for a while).  Else, somebody else has the record locked
    if (sLockedObtained.equalsIgnoreCase(COMConstants.CONS_NO))
    {
      lockSuccessful = false;

      //get the locked csr id
      String sCsrId = DOMUtil.getNodeValue(lockInfo, "OUT_LOCKED_CSR_ID");

      //set an output message
      String message = COMConstants.MSG_RECORD_C_LOCKED1 + sCsrId + COMConstants.MSG_RECORD_C_LOCKED2;
      this.pageData.put(COMConstants.PD_MESSAGE_DISPLAY,    message);

    }

    return lockSuccessful;
  }



/*******************************************************************************************
  * processCustomerAccount()
  ******************************************************************************************
  * This method will process the action type of "recipient_search"
  *
  * @param  none
  * @return none
  * @throws none
  */
  private void processCustomerAccount() throws Exception
  {
    //customerAccountInfoResults will store the customer info
    HashMap customerAccountInfoResults = new HashMap();

    //call retrieveCustomerAccountInfo which will call the DAO to retrieve the results
    customerAccountInfoResults = retrieveCustomerAccountInfo();

    //build the VO
    buildCustomerVOAndConverToXML(customerAccountInfoResults);

  }


/*******************************************************************************************
 * processStates() -
 *******************************************************************************************
  *
  * @throws none
  */
  private void processStates() throws Exception
  {
    //statesXML will store the states info
    Document statesXML = DOMUtil.getDocument();

    //Instantiate the BasePageBuilder
    BasePageBuilder builder = new BasePageBuilder();

    //and retrieve the states info in an xml document
    statesXML = builder.retrieveStates(this.con);
    this.hashXML.put(COMConstants.HK_STATES,  statesXML);


  }


/*******************************************************************************************
 * processCountries() -
 *******************************************************************************************
  *
  * @throws none
  */
  private void processCountries() throws Exception
  {
    //statesXML will store the states info
    Document countriesXML = DOMUtil.getDocument();

    //Instantiate the BasePageBuilder
    BasePageBuilder builder = new BasePageBuilder();

    //and retrieve the states info in an xml document
    countriesXML = builder.retrieveCountries(this.con);
    this.hashXML.put(COMConstants.HK_COUNTRIES,  countriesXML);



  }
  
  
  /*******************************************************************************************
 * processServices() -
 *******************************************************************************************
  *
  * @throws Exception
  */
  private void processServices() throws Exception{
    CustomerDAO customerDAO = new CustomerDAO(this.con);
    CustomerOrderSearchBO  cosBO = new CustomerOrderSearchBO();
    Document servicesInfo = DOMUtil.getDocument();
    
    servicesInfo = cosBO.convertToDOM(null);
    this.hashXML.put(COMConstants.HK_SERVICES_INFO_XML, servicesInfo);
  }
  


/*******************************************************************************************
 * retrieveLockInfo() - locks the record for Payment Refunds
 *******************************************************************************************
  *
  * @throws none
  */
  private Document retrieveLockInfo() throws Exception
  {
    //Instantiate RefundDAO
    LockDAO lockDAO = new LockDAO(this.con);

    //hashmap that will contain the output from the stored proc
    Document lockInfo = DOMUtil.getDocument();


  //Call retrieveLockXML method in the DAO
  //lockInfo = lockDAO.retrieveLockXML(sessionId,       csrId,       entityId [aka lockId],  entityType [aka applicationName]);
    lockInfo = lockDAO.retrieveLockXML(this.sessionId,  this.csrId,  this.customerId,        COMConstants.CONS_ENTITY_TYPE_CUSTOMER);

    return lockInfo;

  }



/*******************************************************************************************
 * retrieveCustomerAccountInfo()
 *******************************************************************************************
  * This method is used to call the CustomerDAO.getCustomerAcct
  *
  * @param1 String - customerId
  * @param2 long - start position
  * @param3 long - end position
  * @return HashMap - map containing output from the stored procedure
  * @throws none
  */
  private HashMap retrieveCustomerAccountInfo() throws Exception
  {
    //Instantiate CustomerDAO
    CustomerDAO customerDAO = new CustomerDAO(this.con);

    //hashmap that will contain the output from the stored proc
    HashMap customerAccountInfo = new HashMap();

    //Call getCustomerAcct method in the DAO
    customerAccountInfo = customerDAO.getCustomerAcctOnly(this.customerId, this.sessionId, this.csrId);

    return customerAccountInfo;
  }




/*******************************************************************************************
  * buildCustomerVOAndConverToXML()
  ******************************************************************************************
  *
  * @param  none
  * @return none
  * @throws none
  */
  private void buildCustomerVOAndConverToXML(HashMap customerAccountInfo) throws Exception
  {

    List customerPhoneVOList   = new ArrayList();
    List emailVOList           = new ArrayList();
    List membershipVOList      = new ArrayList();
    List directMailVOList      = new ArrayList();
    int emailCount = 0;


    Set ks1 = customerAccountInfo.keySet();
    Iterator iter = ks1.iterator();
    String key = null;

    //Create a CachedResultSet object using the cursor name that was passed.
    CachedResultSet rs = null;

    //Iterate thru the keyset
    while(iter.hasNext())
    {
      //get the key
      key = iter.next().toString();

      if (key.equalsIgnoreCase("OUT_CUSTOMER_CUR"))
      {
        rs = (CachedResultSet) customerAccountInfo.get(key);
        rs.next();

        //build the VO
        this.customerVO.setCustomerId(this.lCustomerId);
        this.customerVO.setFirstName(rs.getObject("customer_first_name") != null
                    ?DOMUtil.encodeChars((String)rs.getObject("customer_first_name"))              :null);
        this.customerVO.setLastName(rs.getObject("customer_last_name") != null
                    ?(String)rs.getObject("customer_last_name")                                    :null);
        this.customerVO.setBusinessName(rs.getObject("business_name") != null
                    ?DOMUtil.encodeChars((String)rs.getObject("business_name"))                    :null);
        this.customerVO.setAddress1(rs.getObject("customer_address_1") != null
                    ?(String)rs.getObject("customer_address_1")                                    :null);
        this.customerVO.setAddress2(rs.getObject("customer_address_2") != null
                    ?(String)rs.getObject("customer_address_2")                                    :null);
        this.customerVO.setCity(rs.getObject("customer_city") != null
                    ?(String)rs.getObject("customer_city")                                         :null);
        this.customerVO.setState(rs.getObject("customer_state") != null
                    ?(String)rs.getObject("customer_state")                                        :null);
        this.customerVO.setZipCode(rs.getObject("customer_zip_code") != null
                    ?(String)rs.getObject("customer_zip_code")                                     :null);
        this.customerVO.setCountry(rs.getObject("customer_country") != null
                    ?(String)rs.getObject("customer_country")                                      :null);
        this.customerVO.setBuyerIndicator(rs.getObject("buyer_indicator") != null
                    ?rs.getObject("buyer_indicator").toString().charAt(0)                     :'N');
        this.customerVO.setRecipientIndicator(rs.getObject("recipient_indicator") != null
                    ?rs.getObject("recipient_indicator").toString().charAt(0)                 :'N');
        this.customerVO.setVipCustomer((rs.getString("vip_customer")!= null ? FieldUtils.convertStringToBoolean(rs.getString("vip_customer"))  : false) );
        //retrieve/format the createdOn date
        Date dCreatedOn = null;
        if (rs.getObject("customer_created_on") != null)
            dCreatedOn = (Date) rs.getObject("customer_created_on");

        if (dCreatedOn != null)
        {
          Calendar cCreatedDate = Calendar.getInstance();
          cCreatedDate.setTime(dCreatedOn);

          //set the date
          this.customerVO.setCreatedOn(cCreatedDate);
        }
        else
          this.customerVO.setCreatedOn(null);


        /*if (rs.getObject("customer_created_on") != null)
          {
            String sCreatedOn = rs.getObject("customer_created_on").toString();
            //Define the format
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
            //Create a Calendar Object
            Calendar cCreatedOn = Calendar.getInstance();
            //Set the Calendar Object using the date retrieved in sCreatedOn
            cCreatedOn.setTime(sdf.parse(sCreatedOn));
            //set the VO with the Calendar object
            this.customerVO.setCreatedOn(cCreatedOn);
          }*/

        this.customerVO.setAddressType(null); //not getting returned from the stored proc
        this.customerVO.setPreferredCustomer('N'); //not getting returned from the stored proc
        this.customerVO.setOrigin(null); //not getting returned from the stored proc
        this.customerVO.setCounty(null); //not getting returned from the stored proc
        this.customerVO.setBirthday(null); //not getting returned from the stored proc

        this.customerVO.setCreatedBy(null); //users cannot update
        this.customerVO.setUpdatedOn(null); //users cannot update
        this.customerVO.setFirstOrderDate(null); //users cannot update

        //set the user id
        this.customerVO.setUpdatedBy(this.csrId);


        //set the customer comment indicator in the page data
        String custCmntInd =(String)  rs.getObject("customer_comment_indicator");
        this.pageData.put("customer_comment_indicator",       custCmntInd);

        //check to see if the values were overridden by a CSR.  If data was persisted using
        //the data filter, use that data.  Otherwise, keep the data from the database
        if(dataExists.equalsIgnoreCase(COMConstants.CONS_YES))
        {
          this.customerVO.setBusinessName(this.ucBusinessName);
          this.customerVO.setAddress1(this.ucCustomerAddress1);
          this.customerVO.setAddress2(this.ucCustomerAddress2);
          this.customerVO.setCity(this.ucCustomerCity);
          this.customerVO.setCountry(this.ucCustomerCountry);
          this.customerVO.setFirstName(this.ucCustomerFirstName);
          this.customerVO.setLastName(this.ucCustomerLastName);
          this.customerVO.setState(this.ucCustomerState);
          this.customerVO.setZipCode(this.ucCustomerZipCode);
          this.customerVO.setVipCustomer(ucVipCustomer);
        }

      }
      else if (key.equalsIgnoreCase("OUT_PHONE_CUR"))
      {
        rs = (CachedResultSet) customerAccountInfo.get(key);

        while(rs.next())
        {
          CustomerPhoneVO phoneVO = new CustomerPhoneVO();

          phoneVO.setCustomerId(this.lCustomerId);

          //retrieve the phone type
          String phoneType = null;
          if (rs.getObject("customer_phone_type") != null)
            phoneType = (String)rs.getObject("customer_phone_type");
          phoneVO.setPhoneType(phoneType);

          //retrieve the phone id
          String      phoneId = null;
          BigDecimal  bPhoneId;
          long        lPhoneId = 0;
          if (rs.getObject("customer_phone_id") != null)
          {
            bPhoneId = (BigDecimal)rs.getObject("customer_phone_id");
            phoneId = bPhoneId.toString();
            lPhoneId = Long.valueOf(phoneId).longValue();
          }
          phoneVO.setPhoneId(lPhoneId);


          //retrieve the phone number
          String phoneNumber = null;
          if (rs.getObject("customer_phone_number") != null)
            phoneNumber = (String) rs.getObject("customer_phone_number");
          phoneVO.setPhoneNumber(phoneNumber);

          //retrieve the extension
          String extension = null;
          if (rs.getObject("customer_extension") != null)
            extension = (String) rs.getObject("customer_extension");
          phoneVO.setExtension(extension);
          
          //retrieve the phone number type
          String phoneNumberType = null;
          if (rs.getObject("cus_phone_number_type") != null)
        	  phoneNumberType = (String) rs.getObject("cus_phone_number_type");
          phoneVO.setPhoneNumberType(phoneNumberType);
          
          //retrieve the sms opt in
          String smsOptIn = null;
          if (rs.getObject("customer_sms_opt_in") != null)
        	  smsOptIn = (String) rs.getObject("customer_sms_opt_in");
          phoneVO.setSmsOptIn(smsOptIn);
          
          
          phoneVO.setUpdatedOn(null); //users cannot update
          phoneVO.setCreatedOn(null); //users cannot update
          phoneVO.setCreatedBy(null); //users cannot update

          //set the user id
          phoneVO.setUpdatedBy(this.csrId);


          //check to see if the values were overridden by a CSR.  If data was persisted using
          //the data filter, use that data.  Otherwise, keep the data from the database
          if (dataExists.equalsIgnoreCase(COMConstants.CONS_YES))
          {
            if(phoneType.equalsIgnoreCase(COMConstants.CONS_DAY))
            {
              phoneVO.setExtension(ucDaytimeCustomerExtension);
              phoneVO.setPhoneNumber(ucDaytimeCustomerPhoneNumber);
            }
            else if(phoneType.equalsIgnoreCase(COMConstants.CONS_EVENING))
            {
              phoneVO.setExtension(ucEveningCustomerExtension);
              phoneVO.setPhoneNumber(ucEveningCustomerPhoneNumber);
            }
          }

          //add the customer phone vo to the list
          customerPhoneVOList.add(phoneVO);

        }
      }
      else if (key.equalsIgnoreCase("OUT_MEMBERSHIP_CUR"))
      {
        rs = (CachedResultSet) customerAccountInfo.get(key);

        while(rs.next())
        {
          MembershipVO mVO = new MembershipVO();

          mVO.setCustomerId(this.lCustomerId);
          mVO.setMembershipId(rs.getObject("membership_id") != null
                    ?Long.valueOf(rs.getObject("membership_id").toString()).longValue()            :0);
          mVO.setMembershipNumber(rs.getObject("membership_number") != null
                    ?(String)rs.getObject("membership_number")                                     :null);
          mVO.setMembershipType(rs.getObject("membership_type") != null
                    ?(String)rs.getObject("membership_type")                                       :null);
          mVO.setFirstName(rs.getObject("membership_first_name") != null
                    ?(String)rs.getObject("membership_first_name")                                 :null);
          mVO.setLastName(rs.getObject("membership_last_name") != null
                    ?(String)rs.getObject("membership_last_name")                                  :null);

          mVO.setCreatedOn(null); //users cannot update
          mVO.setCreatedBy(null); //users cannot update
          mVO.setUpdatedOn(null); //users cannot update
          mVO.setUpdatedBy(this.csrId); //users cannot update

          //add the membership vo to the list
          membershipVOList.add(mVO);
        }
      }
      else if (key.equalsIgnoreCase("OUT_HOLD_CUR"))
      {
        //Create an Document and call a method that will transform the CachedResultSet from the cursor:
        //OUT_HOLD_CUR into an Document.  Store this XML object in hashXML HashMap
        Document caiHoldXML =
            buildXML(customerAccountInfo, "OUT_HOLD_CUR", "CAI_HOLDS", "CAI_HOLD");
        this.hashXML.put(COMConstants.HK_CAI_HOLD_XML,         caiHoldXML);

      }
      else if (key.equalsIgnoreCase("OUT_VIEWING_CUR"))
      {
        //Create an Document and call a method that will transform the CachedResultSet from the cursor:
        //OUT_VIEWING_CUR into an Document.  Store this XML object in hashXML HashMap
        Document caiViewingXML =
            buildXML(customerAccountInfo, "OUT_VIEWING_CUR", "CSR_VIEWINGS", "CSR_VIEWING");
        this.hashXML.put(COMConstants.HK_CAI_VIEWING_XML,      caiViewingXML);

      }
      else if (key.equalsIgnoreCase("OUT_DIRECT_MAIL_EMAIL_CUR"))
      {
        rs = (CachedResultSet) customerAccountInfo.get(key);

        while(rs.next())
        {
          /**Create the Direct Mail VO**/
          DirectMailVO dVO = new DirectMailVO();

          dVO.setCustomerId(this.lCustomerId);

          //retrieve the company id
          String companyId = null;
          if (rs.getObject("company_id") != null)
            companyId = (String) rs.getObject("company_id");
          dVO.setCompanyId(companyId);

          //retrieve the direct mail status
          String dmStatus = null;
          if (rs.getObject("direct_mail_subscribe_status") != null)
            dmStatus = (String) rs.getObject("direct_mail_subscribe_status");
          dVO.setStatus(dmStatus);

          //set the company name
          dVO.setCompanyName(rs.getObject("company_name") != null
                    ?(String)rs.getObject("company_name")                                          :null);


          dVO.setUpdatedOn(null);
          dVO.setCreatedOn(null); //users cannot update
          dVO.setCreatedBy(null); //users cannot update


          //check to see if the values were overridden by a CSR.  If data was persisted using
          //the data filter, use that data.  Otherwise, keep the data from the database
          if (dataExists.equalsIgnoreCase(COMConstants.CONS_YES))
          {
            String status = (String) this.ucDirectMailSubscribeStatusHash.get(companyId);
            dVO.setStatus(status);
          }

           //set the user id
          dVO.setUpdatedBy(this.csrId);



          //add the direct mail vo to the list
          directMailVOList.add(dVO);


          /**Create the Email VO**/
          EmailVO eVO = new EmailVO();

          eVO.setCustomerId(this.lCustomerId);
          eVO.setEmailId(rs.getObject("email_id") != null
                    ?Long.valueOf(rs.getObject("email_id").toString()).longValue()                 :0);
          eVO.setEmailAddress(rs.getObject("email_address") != null
                    ?(String)rs.getObject("email_address")                                         :null);
          eVO.setActiveIndicator(rs.getObject("email_active_indicator") != null
                    ?(String)rs.getObject("email_active_indicator")                                :null);
          eVO.setSubscribeStatus(rs.getObject("email_subscribe_status") != null
                    ?(String)rs.getObject("email_subscribe_status")                                :null);

          //retrieve/format the updatedOn date
          Date dUpdatedOn = null;
          if (rs.getObject("email_updated_on") != null)
            dUpdatedOn = (Date) rs.getObject("email_updated_on");

          if (dUpdatedOn != null)
          {
            Calendar cUpdatedDate = Calendar.getInstance();
            cUpdatedDate.setTime(dUpdatedOn);

            //set the date
            eVO.setUpdatedOn(cUpdatedDate);
          }
          else
            eVO.setUpdatedOn(null); //users cannot update

          eVO.setSubscribeDate(null); //not getting returned from the stored proc
          eVO.setCompanyId(null); //not getting returned from the stored proc
          eVO.setCreatedOn(null); //users cannot update
          eVO.setCreatedBy(null); //users cannot update

          //set the user id
          eVO.setUpdatedBy(this.csrId);

          //add the email vo to the list
          emailVOList.add(eVO);

          if (eVO.getEmailId() != 0)
            emailCount++;

        }
      }
      else if (key.equalsIgnoreCase("OUT_MORE_EMAIL_INDICATOR"))
      {
        String moreEmailInd = (String)customerAccountInfo.get(key);
        this.pageData.put(COMConstants.PD_MORE_EMAIL_INDICATOR,       moreEmailInd);
      }
      else if (key.equalsIgnoreCase("OUT_LOCKED_INDICATOR"))
      {
        String lockedInd = (String)customerAccountInfo.get(key);
        this.pageData.put(COMConstants.PD_LOCKED_INDICATOR,           lockedInd);
      }

    } //end-while(iter.hasNext())

    //if at least one email was found, send a email found = y
    this.pageData.put(COMConstants.PD_UC_EMAIL_FOUND,     emailCount>0?COMConstants.CONS_YES:COMConstants.CONS_NO);

    //add the lists created above to the customerVOVO
    this.customerVO.setCustomerPhoneVOList(customerPhoneVOList);
    this.customerVO.setEmailVOList(emailVOList);
    this.customerVO.setMembershipVOList(membershipVOList);
    this.customerVO.setDirectMailVOList(directMailVOList);



    Document customerXML = (Document) DOMUtil.getDefaultDocument();

    //generate the top node
    Element stateElement = customerXML.createElement("CustomerVO");
    //and append it
    customerXML.appendChild(stateElement);

    customerXML.getDocumentElement().appendChild(customerXML.importNode(DOMUtil.getDocument(this.customerVO.toXML(0)).getFirstChild(), true));

    this.hashXML.put(COMConstants.HK_CAI_CUSTOMER_XML,    customerXML);

  }



/*******************************************************************************************
 * OLDretrieveCustomerAccountInfo()
 *******************************************************************************************
  * This method is used to call the CustomerDAO.getCustomerAcct
  *
  * @param1 String - customerId
  * @param2 long - start position
  * @param3 long - end position
  * @return HashMap - map containing output from the stored procedure
  * @throws none
  */
  private HashMap OLDretrieveCustomerAccountInfo() throws Exception
  {
    //Instantiate CustomerDAO
    CustomerDAO customerDAO = new CustomerDAO(this.con);

    //hashmap that will contain the output from the stored proc
    HashMap customerAccountInfo = new HashMap();

    //Call getCustomerAcct method in the DAO
    customerAccountInfo = customerDAO.getCustomerAcctOnly(this.customerId, this.sessionId, this.csrId);

    //Create an Document and call a method that will transform the CachedResultSet from the cursor:
    //OUT_CUSTOMER_CUR into an Document.
    Document caiCustomerXML =
        buildXML(customerAccountInfo, "OUT_CUSTOMER_CUR", "CAI_CUSTOMERS", "CAI_CUSTOMER");

    //Retrieve the value of Create Date
    ArrayList aList = new ArrayList();
    aList.add(0,"CAI_CUSTOMERS");
    aList.add(1,"CAI_CUSTOMER");
    aList.add(2,"customer_created_on");
    String sCreateDateOld = DOMUtil.getNodeValue(caiCustomerXML, aList);

    //Convert the string to timestamp
    Timestamp tCreateDate = Timestamp.valueOf(sCreateDateOld);
    //Define the format
    SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
    //Change the format
    String sCreateDateNew = sdf.format(tCreateDate);
    //Update the XML Document
    DOMUtil.updateNodeValue(caiCustomerXML, aList, sCreateDateNew);

    //Store this XML object in hashXML HashMap
    this.hashXML.put(COMConstants.HK_CAI_CUSTOMER_XML,      caiCustomerXML);


    //Create an Document and call a method that will transform the CachedResultSet from the cursor:
    //OUT_PHONE_CUR into an Document.  Store this XML object in hashXML HashMap
    Document caiPhoneXML =
        buildXML(customerAccountInfo, "OUT_PHONE_CUR", "CAI_PHONES", "CAI_PHONE");
    this.hashXML.put(COMConstants.HK_CAI_PHONE_XML,        caiPhoneXML);

    //Create an Document and call a method that will transform the CachedResultSet from the cursor:
    //OUT_EMAIL_CUR into an Document.  Store this XML object in hashXML HashMap
    Document caiMailXML =
        buildXML(customerAccountInfo, "OUT_DIRECT_MAIL_EMAIL_CUR", "CAI_MAILS", "CAI_MAIL");
    this.hashXML.put(COMConstants.HK_CAI_DIRECT_MAIL_EMAIL_XML,        caiMailXML);

    //Create an Document and call a method that will transform the CachedResultSet from the cursor:
    //OUT_MEMBERSHIP_CUR into an Document.  Store this XML object in hashXML HashMap
    Document caiMembershipXML =
        buildXML(customerAccountInfo, "OUT_MEMBERSHIP_CUR", "CAI_MEMBERSHIPS", "CAI_MEMBERSHIP");
    this.hashXML.put(COMConstants.HK_CAI_MEMBERSHIP_XML,   caiMembershipXML);

    //Create an Document and call a method that will transform the CachedResultSet from the cursor:
    //OUT_HOLD_CUR into an Document.  Store this XML object in hashXML HashMap
    Document caiHoldXML =
        buildXML(customerAccountInfo, "OUT_HOLD_CUR", "CAI_HOLDS", "CAI_HOLD");
    this.hashXML.put(COMConstants.HK_CAI_HOLD_XML,         caiHoldXML);

    //Create an Document and call a method that will transform the CachedResultSet from the cursor:
    //OUT_VIEWING_CUR into an Document.  Store this XML object in hashXML HashMap
    Document caiViewingXML =
        buildXML(customerAccountInfo, "OUT_VIEWING_CUR", "CSR_VIEWINGS", "CSR_VIEWING");
    this.hashXML.put(COMConstants.HK_CAI_VIEWING_XML,      caiViewingXML);

    String outLockedInd       = null;
    if (customerAccountInfo.get("OUT_LOCKED_INDICATOR") != null)
      outLockedInd = customerAccountInfo.get("OUT_LOCKED_INDICATOR").toString();

    String outMoreEmailInd    = null;
    if (customerAccountInfo.get("OUT_MORE_EMAIL_INDICATOR") != null)
      outMoreEmailInd = customerAccountInfo.get("OUT_MORE_EMAIL_INDICATOR").toString();

    this.pageData.put(COMConstants.PD_CAI_LOCKED_INDICATOR,     outLockedInd);
    this.pageData.put(COMConstants.PD_CAI_MORE_EMAIL_INDICATOR, outMoreEmailInd);

    return customerAccountInfo;

  }


/*******************************************************************************************
 * buildXML()
 *******************************************************************************************/
  private Document buildXML(HashMap outMap, String cursorName, String topName, String bottomName)
        throws Exception
  {

    //Create a CachedResultSet object using the cursor name that was passed.
    CachedResultSet rs = (CachedResultSet) outMap.get(cursorName);

    Document doc =  null;

    //Create an XMLFormat, and initialize the top and bottom node.  Note that since this method
    //can be/is called by various other methods, the top and botton names MUST be passed to it.
    XMLFormat xmlFormat = new XMLFormat();
    xmlFormat.setAttribute("type", "element");
    xmlFormat.setAttribute("top", topName);
    xmlFormat.setAttribute("bottom", bottomName );

    //call the DOMUtil's converToXMLDOM method
    doc = (Document) DOMUtil.convertToXMLDOM(rs, xmlFormat);

    //return the xml document.
    return doc;

  }

}
