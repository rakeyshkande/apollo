package com.ftd.customerordermanagement.bo;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.dao.LossPreventionDAO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;

/**
 * This class obtains loss prevention information.
 *
 * @author Matt Wilcoxen
 */

public class LossPreventionBO 
{
    private Logger logger = new Logger("com.ftd.customerordermanagement.bo.LossPreventionBO");

    // db info:  connection, cursors
    private Connection conn = null;
    private static final String OUT_LP_INFO_CUR = "OUT_LP_INFO_CUR";
    private static final String OUT_LP_CUSTOMERS_CUR = "OUT_LP_CUSTOMERS_CUR";

    // XML document node names
    private static final String LP_DATA_NODE = "LP_DATA";
    private static final String LOSS_PREVENTION_INDICATOR_NODE = "loss_prevention_indicator";
	private static final String LOSS_PREVENTION_INDICATOR_EXISTS_NODE = "loss_prevention_indicator_exists";
	private static final String LP_INDICATOR_MISSING_MSG_NODE = "loss_prevention_indicator_missing_msg";
    private static final String LP_CUSTOMER_COUNT_NODE = "lp_customer_count";
    private static final String LP_PAGEDATA_NODE = "LP_PAGEDATA";
    private static final String CUSTOMER_ID_NODE = "customer_id";
    private static final String ORDER_GUID_NODE = "order_guid";
    private static final String DATA_NODE = "data";

    private static final String LP_CUSTOMER = "LP_CUSTOMER";
    private static final String CUST_POSITION = "cust_position";
    private static final String ORDER_GUID = "order_guid";
    private static final String CUSTOMER_ID = "customer_id";
    private static final String SHOW_PREVIOUS = "show_previous";
    private static final String SHOW_NEXT = "show_next";
    private static final String PREV_CUSTOMER_ID = "prev_customer_id";
    private static final String PREV_ORDER_GUID = "prev_order_guid";
    private static final String NEXT_CUSTOMER_ID = "next_customer_id";
    private static final String NEXT_ORDER_GUID = "next_order_guid";

    /**
     * constructor
     * 
     * @param Connection - database connection
     * 
     * @return n/a
     * 
     * @throws n/a
     */
    public LossPreventionBO(Connection conn)
    {
        super();
        this.conn = conn;
    }


	/**
	 * Obtain the loss prevention information.
     *     1. get loss prevention data
     *     2. put lp indicator and lp customer count in LP_DATA node & put in hashmap
	 *     3. log loss prevention data
	 * 
	 * @param String   - customer id
	 * @param String   - order GUID
     * 
	 * @return HashMap - contains various XML documents of loss prevention data
     * 
	 * @throws IOException
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws SQLException
	 * @throws Exception
	 */
    public HashMap buildLossPreventionData(String customerId, String orderGuid)
        throws IOException, ParserConfigurationException, SAXException, SQLException,
               Exception
    {
        // 1. get loss prevention data
        LossPreventionDAO lpDAO = new LossPreventionDAO(conn);
        HashMap lpHashMap = lpDAO.getLossPreventionData(orderGuid);

        // 2. put lp indicator and lp customer count in LP_DATA node & put in hashmap
        Document lpInfoDoc = (Document) lpHashMap.get(OUT_LP_INFO_CUR);

        Element element = null;
        Element lpIndElement = null;
        Element lpCustCntElement = null;
        String lossPreventionIndicator_data = null;
        String lossPreventionCustomerCount_data = null;
		boolean lpExists = true;

        NodeList nl = null;
        nl = lpInfoDoc.getElementsByTagName(LOSS_PREVENTION_INDICATOR_NODE);
        if (nl.getLength() > 0)
        {
            element = (Element) nl.item(0);
            lpIndElement = (Element) element;
            lossPreventionIndicator_data = DOMUtil.getNodeValue(lpIndElement);
        }
		
		//  If no loss prevention indicator exists return null
		if(nl.getLength() <= 0 
			|| lossPreventionIndicator_data == null 
			|| lossPreventionIndicator_data.equals(""))
		{
			lpExists = false;
		}

		logger.debug("lossPreventionIndicator_data:" + lossPreventionIndicator_data);

        nl = lpInfoDoc.getElementsByTagName(LP_CUSTOMER_COUNT_NODE);
        if (nl.getLength() > 0)
        {
            element = (Element) nl.item(0);
            lpCustCntElement = (Element) element;
            lossPreventionCustomerCount_data = lpCustCntElement.getNodeValue();
        }

		logger.debug("lossPreventionCustomerCount_data:" + lossPreventionCustomerCount_data);

        Document newLpInfoDoc = (Document) DOMUtil.getDocumentBuilder().newDocument();
        Element root = (Element) newLpInfoDoc.createElement(LP_DATA_NODE);
        newLpInfoDoc.appendChild(root);

        Element newElement = null;
        Text newText = null;

        newElement = newLpInfoDoc.createElement(LOSS_PREVENTION_INDICATOR_EXISTS_NODE);
        newText = newLpInfoDoc.createTextNode(lpExists == true? "Y": "N");
        newElement.appendChild(newText);
        newLpInfoDoc.getDocumentElement().appendChild(newElement);

		if(!lpExists)
		{
			newElement = newLpInfoDoc.createElement(LP_INDICATOR_MISSING_MSG_NODE);
			newText = newLpInfoDoc.createTextNode(COMConstants.MSG_NO_LP_EXISTS);
			newElement.appendChild(newText);
			newLpInfoDoc.getDocumentElement().appendChild(newElement);	
		}
		
        newElement = newLpInfoDoc.createElement(LOSS_PREVENTION_INDICATOR_NODE);
        newText = newLpInfoDoc.createTextNode(lossPreventionIndicator_data);
        newElement.appendChild(newText);
        newLpInfoDoc.getDocumentElement().appendChild(newElement);

        newElement = newLpInfoDoc.createElement(LP_CUSTOMER_COUNT_NODE);
        newText = newLpInfoDoc.createTextNode(lossPreventionCustomerCount_data);
        newElement.appendChild(newText);
        newLpInfoDoc.getDocumentElement().appendChild(newElement);

        lpHashMap.put(OUT_LP_INFO_CUR, newLpInfoDoc);
        
        // 3. log loss prevention data
        if(logger.isDebugEnabled())
        {
            Document xmlDoc = (Document) lpHashMap.get(OUT_LP_INFO_CUR);
            
            //IMPORTANT:  Because StringWriter contents are NOT removed even when the flush
            //            method is executed, make StringWriter go away through scope control.
            //            Both the flush and close methods were tested with but were of non-affect.
            StringWriter sw = new StringWriter();       //string representation of xml document
            DOMUtil.print((Document) xmlDoc,new PrintWriter(sw));
            logger.debug("buildLossPreventionData(): Loss Prevention info document from database = \n" + sw.toString());
            //for printing to the console --> DOMUtil.print((Document) xmlDoc,System.out);

            xmlDoc = (Document) lpHashMap.get(OUT_LP_CUSTOMERS_CUR);
            sw = new StringWriter();       //string representation of xml document
            DOMUtil.print((Document) xmlDoc,new PrintWriter(sw));
            logger.debug("buildLossPreventionData(): Loss Prevention customer info document from database = \n" + sw.toString());
        }

		setLPPageData(lpHashMap, customerId, orderGuid);		
		return lpHashMap;
	}		


	/**
	 * Create the LPPageData node.
     *     1. get information for input customer from loss prevention data
     *     2. find customer previous to input customer, if applicable
     *     3. find customer after input customer, if applicable
     *     4. build and log LP_PAGEDATA node
	 * 
     * <LP_CUSTOMERS>
     *     <LP_CUSTOMER>
     *         <customer_id>1012000<customer_id>
     *         <customer_name>Joe User<customer_name>
     *         <order_guid>FTD_GUID_-1154873996055417107504920666080<order_guid>
     *     </LP_CUSTOMER>
     *     The <LP_CUSTOMER> node made repeat . . .
     * </LP_CUSTOMERS>
     * 
	 * @param HashMap  - loss prevention information hashmap
	 * @param String   - customer id
	 * @param String   - order GUID
     * 
	 * @return HashMap - contains various XML documents of loss prevention data
     * 
	 * @throws IOException
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws SQLException
	 * @throws Exception
	 */
    private HashMap setLPPageData(HashMap lpHashMap, String lpCustId, String lpOrderGuid)
        throws IOException, ParserConfigurationException, SAXException, SQLException, 
               Exception
    {
        String custPosition = "0";
        String customerId = lpCustId;
        String orderGuid = lpOrderGuid;

        String showPrevious = "N";
        String prevCustomerId = "";
        String prevOrderGuid = "";

        String showNext = "N";
        String nextCustomerId = "";
        String nextOrderGuid = "";
        
        HashMap lpPageDataHashMap = new HashMap();

        // 1. get information for input customer from loss prevention data
        Document lpCustomers = (Document) lpHashMap.get(OUT_LP_CUSTOMERS_CUR);
        Element element = null;
        Element Element = null;
        Element lpCustomerElement = null;
        NodeList nl = null;
        boolean customerFound = false;
        nl = lpCustomers.getElementsByTagName(LP_CUSTOMER);

        if (nl.getLength() > 0)
        {
            for(int i=0; i<nl.getLength(); i++)
            {
                lpCustomerElement = (Element) nl.item(i);                      // <LP_CUSTOMER>
                custPosition = lpCustomerElement.getAttribute("num"); 
                Element = (Element) lpCustomerElement.getFirstChild();   // <customer_id>
                customerId = DOMUtil.getNodeValue(Element);
                Element = (Element) lpCustomerElement.getLastChild();    // <order_guid>
                orderGuid = DOMUtil.getNodeValue(Element);
                if(customerId.equalsIgnoreCase(lpCustId))
                {
                    customerFound = true;
                    break;
                }
            }

			if (! customerFound)
			{
				logger.warn("LP customer: " + lpCustId + " not found");
				//throw new Exception("LP customer: " + lpCustId + " not found");
			}
	
			// 2. find customer previous to input customer, if it exists
			int numCustPosition = new Integer(custPosition).intValue();
	
			//*** IMPORTANT ***
			//    <LP_CUSTOMERS>
			//        <LP_CUSTOMER num="1">
			//            <customer_id>1012000<customer_id>
			//            <customer_name>Joe User<customer_name>
			//            <order_guid>FTD_GUID_-1154873996055417107504920666080<order_guid>
			//        </LP_CUSTOMER>
			//        <LP_CUSTOMER num="2">
			//            <customer_id>1012000<customer_id>
			//            <customer_name>Joe User<customer_name>
			//            <order_guid>FTD_GUID_-1154873996055417107504920666080<order_guid>
			//        </LP_CUSTOMER>
			//        <LP_CUSTOMER num="3">
			//            <customer_id>1012000<customer_id>
			//            <customer_name>Joe User<customer_name>
			//            <order_guid>FTD_GUID_-1154873996055417107504920666080<order_guid>
			//        </LP_CUSTOMER>
			//    </LP_CUSTOMERS>
			//
			//    Customer position numbering begins at 1.
			//    Node list entries begin numbering at 0.
			//    Meaning that customer position and node list entry #s are off by 1.
			//    For example, customer position # 2 equates to node list entry # 1.
			//
			//    Example: - to find the previous customer #
			//    We must subtract 2 from our customer position number (use customer number 
			//    2 for example) to point to the previous customer, in this case node list 
			//    entry # 0, which is customer # 1.
			//
			//    Example: - to find the next customer #
			//    We can use the customer position number (use customer number 2 for example), 
			//    to point to the next customer in this case node list entry # 2, which
			//    is customer # 3.
			//*** IMPORTANT ***
	
			if (numCustPosition > 1)
			{
				showPrevious = "Y";
				lpCustomerElement = (Element) nl.item(numCustPosition-2);      // <LP_CUSTOMER>
				Element = (Element) lpCustomerElement.getFirstChild();   // <customer_id>
				prevCustomerId = DOMUtil.getNodeValue(Element);
				Element = (Element) lpCustomerElement.getLastChild();    // <order_guid>
				prevOrderGuid = DOMUtil.getNodeValue(Element);
			}
	
			// 3. find customer after input customer, if it exists
			if (numCustPosition < nl.getLength())
			{
				showNext = "Y";
				lpCustomerElement = (Element) nl.item(numCustPosition);        // <LP_CUSTOMER>
				Element = (Element) lpCustomerElement.getFirstChild();   // <customer_id>
				nextCustomerId = DOMUtil.getNodeValue(Element);
				Element = (Element) lpCustomerElement.getLastChild();    // <order_guid>
				nextOrderGuid = DOMUtil.getNodeValue(Element);
			}
        }


        // 4. build and log LP_PAGEDATA node
        lpPageDataHashMap.put(CUST_POSITION, custPosition);
        lpPageDataHashMap.put(ORDER_GUID, orderGuid);
        lpPageDataHashMap.put(CUSTOMER_ID, customerId);
        lpPageDataHashMap.put(SHOW_PREVIOUS, showPrevious);
        lpPageDataHashMap.put(SHOW_NEXT, showNext);
        lpPageDataHashMap.put(PREV_CUSTOMER_ID, prevCustomerId);
        lpPageDataHashMap.put(PREV_ORDER_GUID, prevOrderGuid);
        lpPageDataHashMap.put(NEXT_CUSTOMER_ID, nextCustomerId);
        lpPageDataHashMap.put(NEXT_ORDER_GUID, nextOrderGuid);

        Document temp = (Document) DOMUtil.getDocument();
        DOMUtil.addSection(temp,LP_PAGEDATA_NODE,DATA_NODE,lpPageDataHashMap,true);

        // eliminate the "root" node and makes the root node LP_PAGEDATA
        Document lpPageData = (Document) DOMUtil.getDefaultDocument();
        Element root = lpPageData.createElement(LP_PAGEDATA_NODE);
        lpPageData.appendChild(root);
        nl = temp.getElementsByTagName(DATA_NODE);
        DOMUtil.addSection(lpPageData,nl);

        lpHashMap.put(LP_PAGEDATA_NODE, lpPageData);

        if(logger.isDebugEnabled())
        {
            Document xmlDoc = (Document) lpHashMap.get(LP_PAGEDATA_NODE);
            //IMPORTANT:  Because StringWriter contents are NOT removed even when the flush
            //            method is executed, make StringWriter go away through scope control.
            //            Both the flush and close methods were tested with but were of non-affect.
            StringWriter sw = new StringWriter();       //string representation of xml document
            DOMUtil.print((Document) xmlDoc,new PrintWriter(sw));
            logger.debug("setLPPageData(): Loss Prevention PageData = \n" + sw.toString());
            //for printing to the console --> DOMUtil.print((Document) xmlDoc,System.out);
        }
		return lpHashMap;
	}		

}