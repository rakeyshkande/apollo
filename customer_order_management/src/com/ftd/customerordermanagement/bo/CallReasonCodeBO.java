package com.ftd.customerordermanagement.bo;

import com.ftd.customerordermanagement.dao.CallReasonCodeDAO;
import com.ftd.customerordermanagement.dao.CustomerDAO;
import com.ftd.customerordermanagement.util.CallTimer;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import java.sql.Connection;
import java.sql.SQLException;

import javax.xml.parsers.ParserConfigurationException;



import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

import org.xml.sax.SAXException;

/**
 * This class assembles the data for the Call Reason Code screen as well as set 
 * the reason code chosen to disposition the call and stop the Call Timer.
 *
 * @author Matt Wilcoxen
 */

public class CallReasonCodeBO
{
    private Logger logger = new Logger("com.ftd.customerordermanagement.bo.CallReasonCodeBO");

    //elements:
    private static final String X_CUSTOMER_NAME  = "customer_name";

    //database:
    private Connection conn = null;

    /**
     * constructor
     * 
     * @param Connection - database connection
     * 
     * @return n/a
     * 
     * @throws n/a
     */
    public CallReasonCodeBO(Connection conn)
    {
        super();
        this.conn = conn;
    }


    /**
     * get reason code data
     *   1. get customer name if customer id supplied
     *   2. get list of call reason codes
     *   3. add customer name, if supplied, to list of call reason codes
     *      <crc_data>
     *          <reason_code>
     *               <call_reason_code>DC</call_reason_code>
     *               <description>Delivery Confirmation</description>
     *               <call_reason_type>Customer</call_reason_type>
     *          <reason_code>
     *          �* many reason code nodes will be returned
     *          <customer_name>Jane Doe</customer_name>
     *      <crc_data>
     *
     * @param String - customer id
     * 
     * @return Document - list of call reason codes
     * @throws Exception 
     */
    public Document getReasonCodeData(String customerId)
        throws Exception
    {
        Document callReasonCodeXMLDoc = null;

        // 1. get customer name if customer id supplied
        String customerName = "";
        if(customerId != null  &&  customerId.trim().length() > 0)
        {
            CustomerDAO custDAO = new CustomerDAO(conn);
            customerName = custDAO.getCustomerName(customerId);
        }

        // 2. get list of call reason codes
        CallReasonCodeDAO callDAO = new CallReasonCodeDAO(conn);
        callReasonCodeXMLDoc = (Document) callDAO.getReasonCodes();

        // 3. add customer name, if supplied, to list of call reason codes
        if(customerId != null  &&  customerId.trim().length() > 0)
        {
            Element newEle = null;
            Text newText = null;
    
            newEle = callReasonCodeXMLDoc.createElement(X_CUSTOMER_NAME);
            newText = callReasonCodeXMLDoc.createTextNode(customerName);
            newEle.appendChild(newText);
    
            callReasonCodeXMLDoc.getDocumentElement().appendChild(newEle);
    
            if(logger.isDebugEnabled())
            {
                //IMPORTANT:  Because StringWriter contents are NOT removed even when the flush
                //            method is executed, make StringWriter go away through scope control.
                //            Both the flush and close methods were tested with but were of non-affect.
                StringWriter sw = new StringWriter();       //string representation of xml document
                DOMUtil.print(callReasonCodeXMLDoc,new PrintWriter(sw));
                logger.debug("getReasonCodeData(): document = \n" + sw.toString());
            }
        }

        return callReasonCodeXMLDoc;
    }


    /**
     * set reason code
     *   1. stop the timer with the passed reason code
     *
     * @param String - reason code
     * @param String - reason type
     * @param String - call log id
     * 
     * @return none
     * 
     * @throws IOException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws SQLException
     */
    public void setReasonCode(String reasonCode, String reasonType, String callLogId)
        throws IOException, ParserConfigurationException, SAXException, SQLException
    {
        CallTimer ct = new CallTimer(conn);
        ct.stop(callLogId,reasonCode,reasonType);
    }

}