package com.ftd.customerordermanagement.bo;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.naming.InitialContext;
import javax.transaction.UserTransaction;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.dao.BillingDAO;
import com.ftd.customerordermanagement.dao.CreditCardDAO;
import com.ftd.customerordermanagement.dao.CustomerDAO;
import com.ftd.customerordermanagement.dao.GcCouponDAO;
import com.ftd.customerordermanagement.dao.OrderDAO;
import com.ftd.customerordermanagement.dao.PaymentDAO;
import com.ftd.customerordermanagement.dao.ShoppingCartDAO;
import com.ftd.customerordermanagement.vo.BillingVO;
import com.ftd.customerordermanagement.vo.CommonCreditCardVO;
import com.ftd.customerordermanagement.vo.CreditCardVO;
import com.ftd.customerordermanagement.vo.CustomerVO;
import com.ftd.customerordermanagement.vo.GcCouponVO;
import com.ftd.customerordermanagement.vo.GiftCardVO;
import com.ftd.customerordermanagement.vo.OrderBillVO;
import com.ftd.customerordermanagement.vo.OrderVO;
import com.ftd.customerordermanagement.vo.PaymentVO;
import com.ftd.ftdutilities.CalculateTaxUtil;
import com.ftd.mo.exceptions.GiftCardException;
import com.ftd.op.common.PGCCUtils;
import com.ftd.op.common.dao.CommonDAO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.GiftCodeUtil;
import com.ftd.osp.utilities.constants.PaymentExtensionConstants;
import com.ftd.osp.utilities.order.RecalculateOrderBO;
import com.ftd.osp.utilities.order.vo.ItemTaxVO;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.TaxVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.RedemptionDetails;
import com.ftd.osp.utilities.vo.RetrieveGiftCodeResponse;
import com.ftd.osp.utilities.vo.UpdateGiftCodeRequest;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.security.SecurityManager;
import com.ftd.security.cache.vo.UserInfo;
import com.ftd.security.exceptions.AuthenticationException;
import com.ftd.security.exceptions.ExpiredCredentialsException;
import com.ftd.security.exceptions.ExpiredIdentityException;
import com.ftd.security.exceptions.TooManyAttemptsException;
/**
 * This class handles add billing logic.
 *
 * @author Luke W. Pennings
 */

public class AddBillingBO
{
  private Logger logger = new Logger("com.ftd.customerordermanagement.bo.AddBillingBO");
  private HashMap pageData = new HashMap();
  private static final String DATA_ROOT_NODE = "root";
  private static final String STATUS_ROOT_NODE = "status";
  private static final String GCC_PAYMENT_TYPE = "GC";
  //database:
  private Connection conn = null;
    
  //security:  security on/off ind, security manager
  private boolean securityIsOn = false;
  private SecurityManager sm = null;
  private UserInfo ui = null;
  private String csrId = null;
  
  private static String ACCEPT = "Accept";
  private static String CONTENT_TYPE = "Content-Type";
  private static String APPLICATION_JSON = "application/json";

  /**
   * constructor
   * 
   * @param Connection - database connection
   * @return n/a
   * @throws n/a
   */
  public AddBillingBO(Connection conn)
  {
    super();
    this.conn = conn;
  }


  /**
   * get payment information
   * @param String  - orderGuid
   * @param String  - orderDetailID
   * @param String  - billingLevel
   * @param String  - sourceCode
   * @return HashMap - payment info
   *                 - payment methods
   *                 - cart billing info
   * @throws Exception
   * @throws IOException
   * @throws ParserConfigurationException
   * @throws SAXException
   * @throws SQLException
   * @throws TransformerException
   * @throws XSLException
   */
   
  public HashMap getPaymentInfo(String orderGuid, String orderDetailId,
                            String billingLevel, String sourceCode, String secToken, String extOrdNum,
                            String mstOrdNum)
         throws IOException, ParserConfigurationException, SAXException, SQLException,
                TransformerException, Exception
  {
    String shipMethod = new String();
    String productType = new String();
    String vendorFlag = new String();
    String productID = new String();
    String state = new String();
    String noTaxFlag = "N";
    OrderDAO oDAO = new OrderDAO(conn);
    OrderVO oVO = new OrderVO();
	PaymentMethodBO pmBO = new PaymentMethodBO(conn);
    ShoppingCartDAO scDAO = new ShoppingCartDAO(conn);
    CreditCardDAO ccDAO = new CreditCardDAO(conn);
	boolean cartLevel = billingLevel.equalsIgnoreCase(COMConstants.BILLING_LEVEL_CART)?true:false;
        
    HashMap returnHash = new HashMap();
    

    /* We never obtain cart payments specifically.  The cart payment is always included
     * in the order payments.  If the cart has not been authorized
     * there will be no order payments so there is no reason to distinguish
     * between cart or order payment queries.  We always use the order payment
     * query based on order detail id to get both cart and order info.
     * The shopping cart page passes us the first order in the shopping cart.
     * This design was used based on the fact that we are running out of time.
     */
	Document paymentInfo = oDAO.getPaymentInfo(new String(), orderDetailId);


    returnHash.put("payment_info", paymentInfo);
    
    
    if (!billingLevel.equalsIgnoreCase(COMConstants.BILLING_LEVEL_CART))
    {
      Document returnDocument = oDAO.getShipMethod(orderDetailId);

      if (returnDocument != null)
      {
        String XPath = "//SHIPPING_METHODS/SHIPPING_METHOD";
        NodeList nodeList = DOMUtil.selectNodes(returnDocument,XPath);
        Node node1 = null;
        if (nodeList != null)
        {
          for (int i = 0; i < nodeList.getLength(); i++)
          {
            node1 = (Node) nodeList.item(i);
            if (node1 != null)
            {
              if (DOMUtil.selectSingleNode(node1,"ship_method/text()") != null)
              {
                shipMethod = (DOMUtil.selectSingleNode(node1,"ship_method/text()").getNodeValue());  
              }
              if (DOMUtil.selectSingleNode(node1,"product_type/text()") != null)
              {
                productType = (DOMUtil.selectSingleNode(node1,"product_type/text()").getNodeValue());  
              }
              if (DOMUtil.selectSingleNode(node1,"vendor_flag/text()") != null)
              {
                vendorFlag = (DOMUtil.selectSingleNode(node1,"vendor_flag/text()").getNodeValue());  
              }
              if (DOMUtil.selectSingleNode(node1,"no_tax_flag/text()") != null)
              {
                noTaxFlag = (DOMUtil.selectSingleNode(node1,"no_tax_flag/text()").getNodeValue());  
              }
              if (DOMUtil.selectSingleNode(node1,"product_id/text()") != null)
              {
            	  productID = (DOMUtil.selectSingleNode(node1,"product_id/text()").getNodeValue());  
              }
              if (DOMUtil.selectSingleNode(node1,"state/text()") != null)
              {
            	  state = (DOMUtil.selectSingleNode(node1,"state/text()").getNodeValue());  
              }              
            }
          }
        }
      
        returnDocument = DOMUtil.getDefaultDocument();
        Element root = (Element) returnDocument.createElement("SHIPPING_METHODS");
        returnDocument.appendChild(root);

        Element statusElement = (Element) returnDocument.createElement("SHIPPING_METHOD");
        root.appendChild(statusElement);
    
        Element node = (Element)returnDocument.createElement("ship_method");
        node.appendChild(returnDocument.createTextNode(shipMethod));
        statusElement.appendChild(node);
  
        node = (Element)returnDocument.createElement("product_type");
        node.appendChild(returnDocument.createTextNode(productType));
        statusElement.appendChild(node);

        node = (Element)returnDocument.createElement("vendor_flag");
        node.appendChild(returnDocument.createTextNode(vendorFlag));
        statusElement.appendChild(node);

        node = (Element)returnDocument.createElement("no_tax_flag");
        node.appendChild(returnDocument.createTextNode(noTaxFlag));
        statusElement.appendChild(node);
        
        root.appendChild(statusElement);

        node = (Element)returnDocument.createElement("product_id");
        node.appendChild(returnDocument.createTextNode(productID));
        root.appendChild(node);

        node = (Element)returnDocument.createElement("recipient_state");
        node.appendChild(returnDocument.createTextNode(state));
        root.appendChild(node);
      }
      returnHash.put("shipMethods", returnDocument);
    }
    
    
	Document paymentMethods = pmBO.getPaymentMethods(sourceCode, cartLevel);
    returnHash.put("payment_methods", paymentMethods);

    if (billingLevel.equalsIgnoreCase(COMConstants.BILLING_LEVEL_CART))
    {
      Document cartBilling = scDAO.getCurrentBillingTotals(orderGuid);
      returnHash.put("cart_billing", cartBilling);
    }
    
    Document origCCDoc = ccDAO.getOriginalCreditCardFromOrder(orderGuid);

    if (origCCDoc != null)
    {
      String XPath = "//ORIGINAL_CREDITCARDS/ORIGINAL_CREDITCARD";
      NodeList nodeList = DOMUtil.selectNodes(origCCDoc,XPath);
      Node node = null;

      if (nodeList != null)
      {
        for (int i = 0; i < nodeList.getLength(); i++)
        {
          node = (Node) nodeList.item(i);
          if (node != null)
          {
            String creditCard = (DOMUtil.selectSingleNode(node,"cc_number/text()").getNodeValue());
            DOMUtil.selectSingleNode(node,"cc_number/text()").setNodeValue(creditCard.substring(creditCard.length()-4));
          }
        }
      }
      returnHash.put("original_creditcard", origCCDoc);
    }
   
    //Check for corporate orders.
    //If found, pass a flag back to the front-end to protect the creditcard info
    boolean corpPurch = oDAO.getBillingInfoForNonCertificate(sourceCode);
    //Check the order to see if the origin_id is for ariba.
    //If so, set the corpPurch to true.
    if(!corpPurch)
    {
      oVO = oDAO.getOrder(orderGuid);
      if(oVO != null)
      {
        if(oVO.getOriginId()!= null && (oVO.getOriginId().equalsIgnoreCase("ARI") || oVO.getOriginId().equalsIgnoreCase("CAT")))
        {
          corpPurch = true;
        }
      }
    }

    //Check for restricted products for add on amounts.
    //If found, pass a flag back to the front-end to protect the addon textboxes
    boolean isRestrictedProduct = false;
    //Check the order to see if the origin_id is for ariba.
    //If so, set the corpPurch to true.
    if(productType.equalsIgnoreCase(COMConstants.CONS_PRODUCT_TYPE_FRECUT) || 
       productType.equalsIgnoreCase(COMConstants.CONS_PRODUCT_TYPE_SPEGFT) || 
       productType.equalsIgnoreCase(COMConstants.CONS_PRODUCT_TYPE_SDG)    || 
       productType.equalsIgnoreCase(COMConstants.CONS_PRODUCT_TYPE_SDFC))
    {
      isRestrictedProduct = true;
    }

    this.createPageData(orderGuid, orderDetailId, billingLevel, secToken, extOrdNum,
                        sourceCode, mstOrdNum, corpPurch, isRestrictedProduct);
    
    returnHash.put("pageData", (HashMap)this.pageData);  
    
    return returnHash;
  }

  /**
   * apply payment and billing information
   * @param String  - billing level
   * @param BillingVO
   * @param PaymentVO[]
   * @param GiftCertificateCouponVO
   * @param CreditCardVO
   * @param CommonCreditCardVO
   * @return Document - Update status
   * @throws Exception
   * @throws IOException
   * @throws ParserConfigurationException
   * @throws SAXException
   * @throws SQLException
   * @throws TransformerException
   * @throws XSLException
   */
   
  public Document applyBilling(String orderDetailId, String orderGuid, String billingLevel, 
                            String mgrCode, String mgrPassword,
                            String context, BillingVO bVO, PaymentVO[] pVO,
                            GcCouponVO gcVO, CreditCardVO ccVO, CommonCreditCardVO cccVO,
														String csrId)
        throws IOException, ParserConfigurationException, SAXException, SQLException,
                TransformerException, Exception
  {
    for (int i=0;i<pVO.length;i++)
    {
     logger.debug("pVO.getAdditionalBillId = " + pVO[i].getAdditionalBillId());
    }
    
    
    //Check to see if manager code is authorized to approve
    if(mgrCode != null && mgrCode.length() > 0)
    {
      String unitID = ConfigurationUtil.getInstance().getProperty(COMConstants.SECURITY_FILE, COMConstants.UNIT_ID);
      
	  Object token = null;
	  boolean authenticated = false;
	  try
	  {
		  token = SecurityManager.getInstance().authenticateIdentity(context, unitID, mgrCode, mgrPassword);		  
		  authenticated = true;
	  }
	  catch(ExpiredIdentityException e){logger.debug("User was not authenticated:", e);}
	  catch(ExpiredCredentialsException e){logger.debug("User was not authenticated:", e);}
	  catch(AuthenticationException e){logger.debug("User was not authenticated:", e);}
	  catch(TooManyAttemptsException e){logger.debug("User was not authenticated:", e);}
      
	  if(!authenticated || !SecurityManager.getInstance().assertPermission(COMConstants.RESOURCE_CONTEXT, token, 
                    COMConstants.RESOURCE_NO_CHARGE, COMConstants.RESOURCE_NO_CHARGE_PERMISSION))
      {
         Document statusDoc = this.createStatusDocument(COMConstants.AB_REASON_DECLINE, COMConstants.AB_REASON_DECLINE, 
                                 COMConstants.MA_DECLINE_MESSAGE,"N", null);
         return statusDoc;                         
      }
    }
    UserTransaction userTransaction = null;

		// Obtain cart credit card cart payment before it is updated.  This is used
		// for processing the order later on.
		PaymentDAO pDAO = new PaymentDAO(conn);
		PaymentVO ccCartPVO = null;
		if(billingLevel.equalsIgnoreCase(COMConstants.BILLING_LEVEL_CART))
		{
			ccCartPVO = pDAO.getCartPaymentCC(orderDetailId);
		}
		
    try
    {
      // get the initial context
      InitialContext iContext = new InitialContext();

      //get the transaction timeout
      int transactionTimeout = Integer.parseInt((String)ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, COMConstants.TRANSACTION_TIMEOUT_IN_SECONDS));
      // Retrieve the UserTransaction object
      String jndi_usertransaction_entry = ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, COMConstants.JNDI_USERTRANSACTION_ENTRY);
      userTransaction = (UserTransaction)  iContext.lookup(jndi_usertransaction_entry);
      logger.debug("User transaction obtained");
      userTransaction.setTransactionTimeout(transactionTimeout);
      logger.debug("Transaction timeout set at " + transactionTimeout + " seconds");

      //Authorize the credit card request if it exists
      //First determine if global authorization exists
      if(cccVO != null)
      {
        orderGuid = pVO[0].getOrderGuid();
        CreditCardBO ccBO = new CreditCardBO(conn);
        CreditCardDAO ccDAO = new CreditCardDAO(conn);         
        if (cccVO.getCreditCardNumber() == null)
        {
          //the card will be null due an original card having the expire dates changed
          //go get the full credit card number and update the expire dates on the database
          Document origCCDoc = ccDAO.getOriginalCreditCardFromOrder(orderGuid);
          if (origCCDoc != null)
          {
            String XPath = "//ORIGINAL_CREDITCARDS/ORIGINAL_CREDITCARD";
            NodeList nodeList = DOMUtil.selectNodes(origCCDoc,XPath);
            Node node = null;
            if (nodeList != null)
            {
              for (int i = 0; i < nodeList.getLength(); i++)
              {
                node = (Node) nodeList.item(i);
                if (node != null)
                {
                  cccVO.setCreditCardNumber((DOMUtil.selectSingleNode(node,"cc_number/text()").getNodeValue()));
                  cccVO.setCcId((DOMUtil.selectSingleNode(node,"cc_id/text()").getNodeValue()));
                  pVO[0].setCardNumber((DOMUtil.selectSingleNode(node,"cc_number/text()").getNodeValue()));
                  pVO[0].setCardId((DOMUtil.selectSingleNode(node,"cc_id/text()").getNodeValue()));
                }
              }
            }
          }
        }
        
        //Setting new Route to null default
        cccVO.setRoute(null);
        
		if(pVO[0].getAuthOverrideFlag().equalsIgnoreCase("N"))
		{
		 if("Y".equalsIgnoreCase(pVO[0].getCardinalVerifiedFlag())){
			 CommonDAO opDAO = new CommonDAO(conn);
			   	com.ftd.op.common.vo.CommonCreditCardVO cardinalVO = opDAO.getCardinalData(new Long(ccCartPVO.getPaymentId()).toString());
				cccVO.setCardinalVerifiedFlag(pVO[0].getCardinalVerifiedFlag());
				cccVO.setEci(cardinalVO.getEci());
				cccVO.setCavv(cardinalVO.getCavv());
				cccVO.setXid(cardinalVO.getXid());
				cccVO.setUcaf(cardinalVO.getUcaf());
				Map<String, Object> paymentExtensionMap = cccVO.getPaymentExtMap();
				if(cardinalVO.getEci() != null && !cardinalVO.getEci().equalsIgnoreCase("null")){
					paymentExtensionMap.put(PaymentExtensionConstants.ECI, cccVO.getEci());
				}
				if(cardinalVO.getCavv() != null && !cardinalVO.getCavv().equalsIgnoreCase("null")){
					paymentExtensionMap.put(PaymentExtensionConstants.CAVV, cccVO.getCavv());
				}
				if(cardinalVO.getXid() != null && !cardinalVO.getXid().equalsIgnoreCase("null")){
					paymentExtensionMap.put(PaymentExtensionConstants.XID, cccVO.getXid());
				}
				if(cardinalVO.getUcaf() != null && !cardinalVO.getUcaf().equalsIgnoreCase("null")){
					paymentExtensionMap.put(PaymentExtensionConstants.UCAF, cccVO.getUcaf());
				}
		 }
		 else{
				 cccVO.setEci("");
				 cccVO.setCavv("");
				 cccVO.setXid("");
				 cccVO.setUcaf("");
				 cccVO.setCardinalVerifiedFlag(pVO[0].getCardinalVerifiedFlag());
			}
		  logger.debug("cccVO.getEci: " + cccVO.getEci());
		  logger.debug("cccVO.getCavv: " + cccVO.getCavv());
		  logger.debug("cccVO.getXid: " + cccVO.getXid());
		  logger.debug("cccVO.getUcaf: " + cccVO.getUcaf());
		  
		  
		  logger.info("Existing Route on Order::"+pVO[0].getRoute()+" Order Payment Type::"+pVO[0].getPaymentType());
		  if(pVO[0].getRoute()!=null && !pVO[0].getRoute().isEmpty())
		  {	  
			  cccVO.setRoute(getAuthPGRoute(pVO[0].getRoute()));
		  }  
	      logger.info("Add Bill Route Before Authorizing ::" + cccVO.getRoute());
	      if(COMConstants.PG_PS.equals(cccVO.getRoute())) {
	    	  cccVO.setNewRoute(true);
	    	  cccVO.setRequestId(PGCCUtils.getNextJccasRequestId(conn));
	      }
	      
		  cccVO = ccBO.authorize(cccVO, orderGuid);
		  if(cccVO.getValidationStatus() == null || !cccVO.getValidationStatus().equalsIgnoreCase("A"))
		  {
			Document statusDoc = this.createStatusDocument(COMConstants.AB_REASON_DECLINE, COMConstants.AB_ACTION_DECLINE_CREDIT, COMConstants.CC_DECLINE_MESSAGE,"N", null);
			return statusDoc;
		  }
		    if(cccVO.isUnderMinAuthAmt())
		    {
		          Document statusDoc = this.createStatusDocument(COMConstants.AB_REASON_DECLINE, COMConstants.AB_ACTION_UNDER_MIN_AUTH_AMT, COMConstants.CC_UNDER_MIN_AUTH_AMT_MESSAGE,"N", null);
		          return statusDoc;
		    }
		}
		
		//If no customer id exists obtain the customer id
		if(cccVO.getCustomerId() == null || cccVO.getCustomerId().equals(""))
		{
			CustomerDAO cDAO = new CustomerDAO(conn); 
			CustomerVO custVO = cDAO.getCustomerByOrder(orderGuid);
			cccVO.setCustomerId(String.valueOf(custVO.getCustomerId()));
		}
	  }

      // Start the transaction with the begin method
      userTransaction.begin();
      int status = userTransaction.getStatus();
      logger.debug("userTransStatus = " + status);

      //Update the billing tables
      BillingDAO bDAO = new BillingDAO(conn);
      BigDecimal orderBillingID = new BigDecimal(0);
    
      if(!billingLevel.equalsIgnoreCase(COMConstants.BILLING_LEVEL_CART))
      {
        orderBillingID = bDAO.addBilling(bVO);
      }
      BigDecimal creditCardId = new BigDecimal(0);
      if(ccVO != null && cccVO != null)
      {
    	CreditCardDAO ccDAO = new CreditCardDAO(conn);
        creditCardId = ccDAO.addCreditCard(ccVO, cccVO);
      }
    
      if(gcVO != null && gcVO.getGcCouponNumber() != null && gcVO.getGcCouponNumber().length()>0)
      {
        GcCouponDAO gcDAO = new GcCouponDAO(conn);
        if (bVO != null)
        {
          orderDetailId = String.valueOf(bVO.getOrderDetailId());
        }
        //determine the amount to report to the gift certificate redemption
        //if there are two payment records, the full amount of the gift card
        //was applied.  Set the gcVO amount to the gift card max and set the payment record
        //for the gift card to the gift card max.
        //if only one payment record exists, the gift card covered the entire amount
        //of the order total.  Set the gcVO to the placeholder gcVO amount minus
        //the order total amount.
        //set that result to the payment record credit amount.

        if(pVO.length > 1)
        {
          //apply the full amount of the gift card to the payment
          pVO[1].setCreditAmount(new Double(gcVO.getAmount()).doubleValue());
        }
        else
        {
          //only one payment record exists...so the gift card covered the entire purchase.
          //take the gcVO amount (placeholder for the total gift card amount and add the negative remaining amount
          //take that total and set it to the gcVO amount and the payment record credit amount.
          gcVO.setAmount(String.valueOf(new Double(gcVO.getAmount()).doubleValue() + pVO[0].getCreditAmount()));
          pVO[0].setCreditAmount(new Double(gcVO.getAmount()).doubleValue());
        }

//        if(pVO.length > 1)
//        {
//          if(pVO[0].getCreditAmount() >= pVO[1].getCreditAmount())
//             gcVO.setAmount(String.valueOf(pVO[1].getCreditAmount()));
//          else
//          {
            //the amount owed is less than zero
            //set the gift cert. amount to that difference
            //set the payment owed record to zero
            //set the gift card credit amount to the difference of the payment and the gift card amount.
//            if(pVO[0].getCreditAmount()< 0)
//            {
//              gcVO.setAmount(String.valueOf(pVO[1].getCreditAmount() + pVO[0].getCreditAmount()));
//              pVO[0].setCreditAmount(new Double("0.00").doubleValue());
//              pVO[1].setCreditAmount(new Double(gcVO.getAmount()).doubleValue());
//            }
//           else
//            {
//              gcVO.setAmount(String.valueOf(pVO[1].getCreditAmount() - pVO[0].getCreditAmount()));
//            }
//          }
//        }
//        else
//        {
//           if(new Double(gcVO.getAmount()).doubleValue() >= pVO[0].getCreditAmount())
//              gcVO.setAmount(String.valueOf(pVO[0].getCreditAmount()));/
//           else
//           {
            //the amount owed is less than zero
            //set the gift cert. amount to that difference
            //set the payment owed record to zero
            //set the gift card credit amount to the difference of the payment and the gift card amount.
//            if(new Double(gcVO.getAmount()).doubleValue()< 0)
//            {
//              gcVO.setAmount(String.valueOf(pVO[0].getCreditAmount() + new Double(gcVO.getAmount()).doubleValue()));
//              pVO[0].setCreditAmount(new Double(gcVO.getAmount()).doubleValue());
//            }
//            else
//            {
//              gcVO.setAmount(String.valueOf(new Double(gcVO.getAmount()).doubleValue() - pVO[0].getCreditAmount()));
//            }
//          } 
//        }

        //need to add code to check status of gift code if it is redeemable, call gcs to redeem
        //the code that calls this from Add Billing is broken in Production.  Once that gets fixed, Add Billing
        //will need to be tested using Gift Code Service
        //String redeemStatus = gcDAO.redeemGcCoupon(gcVO, orderDetailId);
        RetrieveGiftCodeResponse retrieveGiftCodeResponse = GiftCodeUtil.getGiftCodeById(gcVO.getGcCouponNumber());

        if(retrieveGiftCodeResponse != null){
        	gcVO.setStatus(retrieveGiftCodeResponse.getStatus());
        	logger.info("retrieveGiftCodeResponse.getStatus(): " + retrieveGiftCodeResponse.getStatus());
        }
        UpdateGiftCodeRequest updateGiftCodeRequest = this.setUpdateGiftCodeRequest(gcVO, orderDetailId);

        //String redeemStatus = this.updateGiftCodeStatus(gcVO, orderDetailId);
        if (updateGiftCodeRequest.getStatus().equalsIgnoreCase("Reinstate")
				|| updateGiftCodeRequest.getStatus().equalsIgnoreCase("Issued Active")
				|| updateGiftCodeRequest.getStatus().equalsIgnoreCase("Redeemed")) {
			updateGiftCodeRequest.setStatus("Redeemed");
		}
        String redeemStatus = GiftCodeUtil.adminUpdateStatus(updateGiftCodeRequest);
        if(!redeemStatus.equalsIgnoreCase("Redeemed"))
        {
          Document statusDoc = this.createStatusDocument(COMConstants.AB_REASON_DECLINE, COMConstants.AB_ACTION_DECLINE_GCC,
                                    COMConstants.GCC_REDEEMED_MSG,"N", null);
          rollback(userTransaction);  
          return statusDoc;        
        }
      }

      //Update the VO with Credit Card and billingID info.
      if(pVO != null && pVO.length>0)
      {
        for(int i=0;i<pVO.length;i++)
        {
          if(!billingLevel.equalsIgnoreCase(COMConstants.BILLING_LEVEL_CART))
          {
             OrderBillVO obVO = new OrderBillVO();
             obVO.setOrderBillId(orderBillingID.toString());
             pVO[i].setAdditionalBillId(orderBillingID.longValue());
             pVO[i].setOrderBill(obVO);
          }
          
          if(cccVO != null && pVO[i].getCardNumber() != null)
          {
            if (creditCardId.intValue() > 0)
            {
              pVO[i].setCardId(creditCardId.toString());
            }
            pVO[i].setCardNumber(cccVO.getCreditCardNumber());
            pVO[i].setAuthResult("AP");
            pVO[i].setAuthorization(cccVO.getApprovalCode());
            pVO[i].setAvsCode(cccVO.getAVSIndicator());
            pVO[i].setAcqRefNumber(cccVO.getAcquirerReferenceData());
            pVO[i].setAuthorizationTransactionId(cccVO.getAuthorizationTransactionId());
            pVO[i].setMerchantRefId(cccVO.getMerchantRefId());
            if (pVO[i].getPaymentType() != null && pVO[i].getPaymentType().equalsIgnoreCase(COMConstants.MILITARY_STAR)) {
                pVO[i].setAafesTicketNumber(cccVO.getAcquirerReferenceData());
            }
            pVO[i].setCcAuthProvider(cccVO.getCcAuthProvider());
            pVO[i].getPaymentExtMap().putAll(cccVO.getPaymentExtMap());
            pVO[i].setCardinalVerifiedFlag(cccVO.getCardinalVerifiedFlag());
            if("N".equalsIgnoreCase(cccVO.getCardinalVerifiedFlag())){
            	logger.debug("deleting cardinal commerce values");
            	PaymentDAO pDao = new PaymentDAO(conn);
                pDao.deleteCardinalCommerceValues(new Long(ccCartPVO.getPaymentId()));
            }
            
            //logger.info("IN loop cccVO.getRoute: "+cccVO.getRoute());
            pVO[i].setRoute(cccVO.getRoute());
          }
          if (mgrCode != null && mgrCode.length() > 0) {
              pVO[i].setNcApprovalId(mgrCode);
          }
        }

        //update the payment tables
        //For billing level of cart
        //Get all existing payments
        //if any CreditCard payments exist, move them to the history table 
        //process the new payment
        if(pVO != null && pVO.length>0)
        {
          OrderDAO oDAO = new OrderDAO(conn);
          if(billingLevel.equalsIgnoreCase(COMConstants.BILLING_LEVEL_CART))
          {
            Document paymentInfo = oDAO.getPaymentInfo("none", orderDetailId);
            this.archiveAndDeleteCartPayment(paymentInfo);
          }
          pDAO.addOrderPayments(pVO);
        }
      }

			//Assuming everything went well, commit the transaction.
			userTransaction.commit();
    }
    catch(Throwable tr)
    {
      logger.error(tr);
      rollback(userTransaction); 
      Document statusDoc = this.createStatusDocument(COMConstants.AB_STATUS_SUCCESS, COMConstants.AB_STATUS_SUCCESS,
                                                        COMConstants.AB_STATUS_SUCCESS,"Y", null);
      
      return statusDoc;
    }
    
		//Process the order if the disposition is held
		if(billingLevel.equalsIgnoreCase(COMConstants.BILLING_LEVEL_CART))
		{
			OrderBO oBO = new OrderBO(conn);
			oBO.processOrderForUpdate(pVO[0].getOrderGuid(), null, ccCartPVO, csrId);			
		}

    Document statusDoc = this.createStatusDocument(COMConstants.AB_STATUS_SUCCESS, COMConstants.AB_STATUS_SUCCESS,
                                                      COMConstants.AB_STATUS_SUCCESS,"N", null);

    return statusDoc;
  }
  
  /**
   * Archive and delete CART payments
   * @param Document - Payments
   * @return void
   * @throws Exception
   * @throws IOException
   * @throws ParserConfigurationException
   * @throws SAXException
   * @throws SQLException
   * @throws TransformerException
   * @throws XSLException
   */
   
  public void archiveAndDeleteCartPayment(Document paymentInfo)
        throws IOException, ParserConfigurationException, SAXException, SQLException,
                TransformerException, Exception
  {
    PaymentDAO pDAO = new PaymentDAO(conn);
    if(paymentInfo != null)
    {
      if(logger.isDebugEnabled())
      {
      //IMPORTANT:  Because StringWriter contents are NOT removed even when the flush
      //            method is executed, make StringWriter go away through scope control.
      //            Both the flush and close methods were tested with but were of none affect.
      StringWriter sw = new StringWriter();       //string representation of xml document
      DOMUtil.print(((Document) paymentInfo),new PrintWriter(sw));
      logger.info("- execute(): COM returned = \n" + sw.toString());
      }
      String XPath = "//ORDER_PAYMENTS/ORDER_PAYMENT";
      NodeList nodeList = DOMUtil.selectNodes(paymentInfo,XPath);
      Node node1 = null;
      long paymentId = 0;
      String paymentType = new String();
      if (nodeList != null)
      {
        for (int i = 0; i < nodeList.getLength(); i++)
        {
          node1 = (Node) nodeList.item(i);
          if (node1 != null)
          {
            if (DOMUtil.selectSingleNode(node1,"payment_type/text()") != null)
            {
              paymentType = (DOMUtil.selectSingleNode(node1,"payment_type/text()").getNodeValue());
              paymentId = new Long((DOMUtil.selectSingleNode(node1,"payment_id/text()").getNodeValue())).longValue();
              if(!paymentType.equalsIgnoreCase(GCC_PAYMENT_TYPE))
              {
                pDAO.deleteCartPayment(paymentId);
              }
            }
          }
        }
      }
    }
  }


  /**
   * create status document
   * @param String  - status
   * @param String  - action
   * @param String  - message
   * @param String  - isError
   * @param String  - rollbackTransaction
   * @return Document - status
   * @throws Exception
   * @throws IOException
   * @throws ParserConfigurationException
   * @throws SAXException
   * @throws SQLException
   * @throws TransformerException
   * @throws XSLException
   */
   
  private Document createStatusDocument(String status, String action, String msg, 
                                           String isError, String rollbackTransaction)
        throws IOException, ParserConfigurationException, SAXException, SQLException,
                TransformerException, Exception
  {
    String messageText = ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, msg);

    Document returnDocument = DOMUtil.getDefaultDocument();
    Element root = (Element) returnDocument.createElement(DATA_ROOT_NODE);
    returnDocument.appendChild(root);

    Element errorNode = (Element)returnDocument.createElement("is_error");
    errorNode.appendChild(returnDocument.createTextNode(isError));
    root.appendChild(errorNode);
    

    //isError=Y is used only if an Exception occurs... for other errors, like authentication 
    //failure, it remains 'N'.  
    //rollback_transaction will be used whenever there is any kind of error that hinders a logical
    //unit of work from completing. 
    if (StringUtils.isNotEmpty(rollbackTransaction))
    {
      Element rollbackNode = (Element)returnDocument.createElement("rollback_transaction");
      rollbackNode.appendChild(returnDocument.createTextNode(rollbackTransaction));
      root.appendChild(rollbackNode);
    }
    
    Element statusElement = (Element) returnDocument.createElement(STATUS_ROOT_NODE);
    root.appendChild(statusElement);
    
    Element dataElement = (Element)returnDocument.createElement("data");  
    statusElement.appendChild(dataElement);

    Element node = (Element)returnDocument.createElement("message_status");
    node.appendChild(returnDocument.createTextNode(status));
    dataElement.appendChild(node);
   
    node = (Element)returnDocument.createElement("message_action");
    node.appendChild(returnDocument.createTextNode(action));
    dataElement.appendChild(node);

    node = (Element)returnDocument.createElement("message_text");
    node.appendChild(returnDocument.createTextNode(messageText));
    dataElement.appendChild(node);
    
    root.appendChild(statusElement);
    
    return returnDocument;  
  }
 
  /**
   * create page data document
   * @param String  - orderGuid
   * @param String  - orderDetailId
   * @param String  - billingLevel
   * @param String  - Securtiy Token
   * @return Document - page data
   * @throws Exception
   * @throws IOException
   * @throws ParserConfigurationException
   * @throws SAXException
   * @throws SQLException
   * @throws TransformerException
   * @throws XSLException
   */ 
   
  public void createPageData(String orderGuid, String orderDetailId, String billingLevel, 
                            String securityToken, String extOrdNum, String sourceCode, String mstOrdNum,
                            boolean corpPurch, boolean isRestrictedProduct)
        throws IOException, ParserConfigurationException, SAXException, SQLException,
                TransformerException, Exception
  {
    this.pageData.put("entity_type", COMConstants.CONS_ENTITY_TYPE_PAYMENTS);
        
    if (billingLevel.equalsIgnoreCase(COMConstants.BILLING_LEVEL_CART))
    {
      this.pageData.put("entity_id", orderGuid);
      this.pageData.put("order_level", COMConstants.CONS_ENTITY_TYPE_ORDERS);
    }
    else
	{
      this.pageData.put("entity_id", orderDetailId);
      this.pageData.put("order_level", COMConstants.CONS_ENTITY_TYPE_ORDER_DETAILS);
    }

    this.pageData.put("session_id", securityToken);

    ConfigurationUtil cu = null;
    cu = ConfigurationUtil.getInstance();
        //get csr Id
    String security = (String) cu.getProperty(COMConstants.PROPERTY_FILE,COMConstants.SECURITY_IS_ON);
    boolean securityIsOn = security.equalsIgnoreCase("true") ? true : false;
    
    if(securityIsOn || securityToken != null)
    {
      SecurityManager sm = SecurityManager.getInstance();
      UserInfo ui = sm.getUserInfo(securityToken);
      csrId = ui.getUserID();
    }
    this.pageData.put("csr_id", csrId);
    
    this.pageData.put("order_guid", orderGuid);
    
    this.pageData.put("order_detail_id", orderDetailId);
    
    this.pageData.put("billing_level", billingLevel);

    this.pageData.put("source_code", sourceCode);
    
    this.pageData.put("corp_purch", String.valueOf(corpPurch));

    this.pageData.put("isRestricted_product", String.valueOf(isRestrictedProduct));
    
    //store the external order number in the page data
    if (extOrdNum != null)
    {
      this.pageData.put(COMConstants.EXTERNAL_ORDER_NUMBER, extOrdNum);
    }
    else
    {
      this.pageData.put(COMConstants.EXTERNAL_ORDER_NUMBER, null);
    }

    //store the master order number in the page data
    if (extOrdNum != null)
    {
      this.pageData.put(COMConstants.MASTER_ORDER_NUMBER, mstOrdNum);
    }
    else
    {
      this.pageData.put(COMConstants.MASTER_ORDER_NUMBER, null);
    }
  }

  /**
   * recalculate Order
   * @param OrderDetailVO
   * @return Document - order amounts
   * @throws Exception
   * @throws IOException
   * @throws ParserConfigurationException
   * @throws SAXException
   * @throws SQLException
   * @throws TransformerException
   * @throws XSLException
   */
   
  public Document recalculateOrder(OrderDetailsVO odVO, String orderGuid, String returnAction)
        throws IOException, ParserConfigurationException, SAXException, SQLException,
                TransformerException, Exception
  {
    // get an conversion object customised for a particular locale
    NumberFormat nf = java.text.NumberFormat.getInstance(Locale.US);
    // set whether you want commas (or locale equivalent) inserted
    nf.setGroupingUsed(true );
    // set how many places you want to the right of the decimal.
    nf.setMinimumFractionDigits(2);
    nf.setMaximumFractionDigits(2);
    // set how many places you want to the left of the decimal.
    nf.setMinimumIntegerDigits(1);
    nf.setMaximumIntegerDigits(10);
 
    RecalculateOrderBO roVO = new RecalculateOrderBO();
    OrderDAO oDAO = new OrderDAO(conn);
    //OrderVO oVO = oDAO.getOrder(odVO.getGuid());
    
    //odVO = roVO.recalculateTaxAndTotal(conn, odVO, oVO.getCompanyId()); -->  with the new tax service we will not calculate additional tax using service.

    //apply taxes before calculate totals   
    setOrderDetailTax(odVO, oDAO);
    roVO.calculateOrderTotal(odVO, conn);
  
    Document returnDocument = DOMUtil.getDefaultDocument();
    Element root = (Element) returnDocument.createElement(DATA_ROOT_NODE);
    returnDocument.appendChild(root);

    Element element, node;

    element = (Element)returnDocument.createElement("is_error");
    element.appendChild(returnDocument.createTextNode("N"));
    root.appendChild(element);
		
    element = (Element)returnDocument.createElement("data");

    node = (Element)returnDocument.createElement("order_amount");
    node.appendChild(returnDocument.createTextNode( (odVO.getOrderAmount() == null) ? "" : nf.format(new Double(odVO.getOrderAmount()))));
    element.appendChild(node);

    node = (Element)returnDocument.createElement("tax_amount");
    node.appendChild(returnDocument.createTextNode( (odVO.getTaxAmount()) == null ? "" : nf.format(new Double(odVO.getTaxAmount()))));
    element.appendChild(node);

    node = (Element)returnDocument.createElement("return_action");
    node.appendChild(returnDocument.createTextNode(returnAction));
    element.appendChild(node);
    
    root.appendChild(element);

    
    // Add TAXES/TAX# for each taxvo, e.g. TAXES/TAX1 with tax1_name, tax1_description, tax1_rate, tax1_amount, etc.
    Element taxesElement, taxElement;
    taxesElement = (Element)returnDocument.createElement("TAXES");
    
    List<TaxVO> taxes = null;
    if(odVO != null && odVO.getItemTaxVO() != null && odVO.getItemTaxVO().getTaxSplit() != null) {
    	taxes = odVO.getItemTaxVO().getTaxSplit();
    }
    //Iterator taxvoI = odVO.getTaxVOs().iterator();
    //List taxes = odVO.getItemTaxVO();
    
    
    int i=1;
   for (TaxVO taxvo : taxes) {
	   
        taxElement = (Element)returnDocument.createElement("TAX"+i);
        
        node = (Element)returnDocument.createElement("tax"+i+"_name");
        node.appendChild(returnDocument.createTextNode(taxvo.getName()));
        taxElement.appendChild(node);
        
        node = (Element)returnDocument.createElement("tax"+i+"_description");
        node.appendChild(returnDocument.createTextNode(taxvo.getDescription()));
        taxElement.appendChild(node);
        
        node = (Element)returnDocument.createElement("tax"+i+"_rate");
        node.appendChild(returnDocument.createTextNode(taxvo.getRate().toString()));
        taxElement.appendChild(node);
        
        node = (Element)returnDocument.createElement("tax"+i+"_amount");
        node.appendChild(returnDocument.createTextNode(taxvo.getAmount().toString()));
        taxElement.appendChild(node);
        
        taxesElement.appendChild(taxElement);
        i++;
    }

    root.appendChild(taxesElement);

    
    return returnDocument;  
  }

	private void setOrderDetailTax(OrderDetailsVO orderDetailVO, OrderDAO oDAO) {
				
		OrderBillVO orderBillTax = null;
		
		ItemTaxVO itemTaxVO = new ItemTaxVO();
		orderDetailVO.setItemTaxVO(itemTaxVO);
		
		List<TaxVO> taxes = new ArrayList<TaxVO>();
		itemTaxVO.setTaxSplit(taxes);
		
		itemTaxVO.setTaxAmount(BigDecimal.ZERO.toString()); // above set are all default values.
		
		
		CalculateTaxUtil taxUtil = new CalculateTaxUtil(); 
		
		try {
		    
			BigDecimal taxableAmount = taxUtil.getTaxableAmount(orderDetailVO);
		    BigDecimal totalTaxAmount = BigDecimal.ZERO;
			BigDecimal totalTaxRate = BigDecimal.ZERO;
			
		    if(taxableAmount.compareTo(BigDecimal.ZERO) <= 0) {	    	
				return; // return default values.
		    }
			
		    // get the actual taxes on order
			try {
				orderBillTax = oDAO.getOrderBillTaxes(String.valueOf(orderDetailVO.getOrderDetailId()));
			} catch (Exception e) {
				logger.error("Error getting the tax rates for order, " + orderDetailVO.getOrderDetailId(), e);
				return; // return default values.
			}
			
			BigDecimal taxRatePercent = null;
			BigDecimal rateScale = new BigDecimal(0.01);
			
			if (orderBillTax != null) {

				// get the total tax rates				
				if (orderBillTax.getTax1Amount() != null && orderBillTax.getTax1Amount().compareTo(	BigDecimal.ZERO) >= 0) {
					TaxVO taxVO = new TaxVO();
					taxVO.setName(orderBillTax.getTax1Name());
					taxVO.setDescription(orderBillTax.getTax1Description());
					
					taxRatePercent = orderBillTax.getTax1Rate();
					if(taxRatePercent.compareTo(BigDecimal.ONE) >= 0) {
						taxRatePercent = taxRatePercent.multiply(rateScale);
					}
					
					taxVO.setRate(taxRatePercent);
					totalTaxRate = totalTaxRate.add(taxRatePercent);
					
					taxVO.setAmount(taxableAmount.multiply(taxRatePercent).setScale(5, BigDecimal.ROUND_HALF_UP));					
					taxes.add(taxVO);
				}
				
				if (orderBillTax.getTax2Amount() != null && orderBillTax.getTax2Amount().compareTo(BigDecimal.ZERO) >= 0) {
					TaxVO taxVO = new TaxVO();
					taxVO.setName(orderBillTax.getTax2Name());
					taxVO.setDescription(orderBillTax.getTax2Description());
					
					taxRatePercent = orderBillTax.getTax2Rate();
					if(taxRatePercent.compareTo(BigDecimal.ONE) >= 0) {
						taxRatePercent = taxRatePercent.multiply(rateScale);
					}
					
					taxVO.setRate(taxRatePercent);
					totalTaxRate = totalTaxRate.add(taxRatePercent);
					
					taxVO.setAmount(taxableAmount.multiply(taxRatePercent).setScale(5, BigDecimal.ROUND_HALF_UP)); 					
					taxes.add(taxVO);
				}
				
				if (orderBillTax.getTax3Amount() != null && orderBillTax.getTax3Amount().compareTo(BigDecimal.ZERO) >= 0) {
					TaxVO taxVO = new TaxVO();
					taxVO.setName(orderBillTax.getTax3Name());
					taxVO.setDescription(orderBillTax.getTax3Description());
					
					taxRatePercent = orderBillTax.getTax3Rate();
					if(taxRatePercent.compareTo(BigDecimal.ONE) >= 0) {
						taxRatePercent = taxRatePercent.multiply(rateScale);
					}
					
					taxVO.setRate(taxRatePercent);
					totalTaxRate = totalTaxRate.add(taxRatePercent);
					
					taxVO.setAmount(taxableAmount.multiply(taxRatePercent).setScale(5, BigDecimal.ROUND_HALF_UP));					
					taxes.add(taxVO);
				}
				
				
				if (orderBillTax.getTax4Amount() != null && orderBillTax.getTax4Amount().compareTo(BigDecimal.ZERO) >= 0) {
					TaxVO taxVO = new TaxVO();
					taxVO.setName(orderBillTax.getTax4Name());
					taxVO.setDescription(orderBillTax.getTax4Description());
					
					taxRatePercent = orderBillTax.getTax4Rate();
					if(taxRatePercent.compareTo(BigDecimal.ONE) >= 0) {
						taxRatePercent = taxRatePercent.multiply(rateScale);
					}
					
					taxVO.setRate(taxRatePercent);
					totalTaxRate = totalTaxRate.add(taxRatePercent);
					
					taxVO.setAmount(taxableAmount.multiply(taxRatePercent).setScale(5, BigDecimal.ROUND_HALF_UP));
					taxes.add(taxVO);
				}
				
				if (orderBillTax.getTax5Amount() != null && orderBillTax.getTax5Amount().compareTo(BigDecimal.ZERO) >= 0) {
					TaxVO taxVO = new TaxVO();
					taxVO.setName(orderBillTax.getTax5Name());
					taxVO.setDescription(orderBillTax.getTax5Description());
					
					taxRatePercent = orderBillTax.getTax5Rate();
					if(taxRatePercent.compareTo(BigDecimal.ONE) >= 0) {
						taxRatePercent = taxRatePercent.multiply(rateScale);
					}
					
					taxVO.setRate(taxRatePercent);
					totalTaxRate = totalTaxRate.add(taxRatePercent);
					
					taxVO.setAmount(taxableAmount.multiply(taxRatePercent).setScale(5, BigDecimal.ROUND_HALF_UP));					
					taxes.add(taxVO);
				}
				
				if (totalTaxRate.compareTo(BigDecimal.ZERO) >= 0) { 
					itemTaxVO.setTotalTaxrate(totalTaxRate);	
					totalTaxAmount = totalTaxAmount.add(taxableAmount.multiply(totalTaxRate).setScale(CalculateTaxUtil.AMOUNT_SCALE, BigDecimal.ROUND_HALF_UP));
					itemTaxVO.setTaxAmount(totalTaxAmount.toString());
				}
								
				// if total tax rate is 0 and order bill has tax applied -- should be default tax rate, apply the same for add billing too
				String defaultTaxRate = null;
				if(totalTaxRate.compareTo(BigDecimal.ZERO) == 0 && orderBillTax.getTax() > 0) {
					
					try {
						defaultTaxRate = ConfigurationUtil.getInstance().getFrpGlobalParm(CalculateTaxUtil.TAX_SERVICE_CONTEXT, CalculateTaxUtil.DEFAULT_TAX_RATE_PARAM);
					} catch (Exception e) {
						logger.error("Error getting the default tax rate, " + e.getMessage());
					}
					
					if(StringUtils.isEmpty(defaultTaxRate)) {
						itemTaxVO.setTaxAmount(BigDecimal.ZERO.toString());
						return;
					}
					
					totalTaxAmount =  taxableAmount.multiply(new BigDecimal(defaultTaxRate)).multiply(rateScale).setScale(CalculateTaxUtil.AMOUNT_SCALE, BigDecimal.ROUND_HALF_UP);	
					itemTaxVO.setTaxAmount(totalTaxAmount.toString());	
				}				
				
			}
		} catch(Exception e) {
			logger.error("Error calculating the taxes for addiitonal billing, " + e.getMessage());
			itemTaxVO.setTaxAmount(BigDecimal.ZERO.toString());
		}
		
	}


/**
  * Rollback user transaction when exceptions occur
  * @param userTransaction - user transaction  
  * @return void
  * @throws none
  */ 
  
  private void rollback(UserTransaction userTransaction)
  {
     try  
     {
       if (userTransaction != null)  
       {
         // rollback the user transaction
         userTransaction.rollback();
         logger.debug("User transaction rolled back");
       }
     } 
     catch (Exception ex)  
     {
       logger.error(ex);
     } 
  }



  /**
   * apply payment and billing information
   * @param String  - billing level
   * @param BillingVO
   * @param PaymentVO[]
   * @param GiftCertificateCouponVO
   * @param CreditCardVO
   * @param CommonCreditCardVO
   * @return Document - Update status
   * @throws Exception
   * @throws IOException
   * @throws ParserConfigurationException
   * @throws SAXException
   * @throws SQLException
   * @throws TransformerException
   * @throws XSLException
   * 
   * Note that this method essentially has same logic as applyBilling.  The differences are:
   *  1) We will not use a transaction within this method.  applyBilling() instantiates a transaction
   *     vs. this method assumes that the calling method will have the code necessary for a 
   *     transactional-approach implementation
   *  2) It will not throw any exceptions.  It will log the exceptions, create an Document, and
   *     send it back
   *  3) If an exception is encountered, it will pass "Y" to createStatusDocument, which will then
   *     generate a node <rollback_transaction>.  This node is in addition to the current XML 
   *     generated, and will be used to commit/rollback by the calling method. 
   */
   
  public Document applyBillingWithoutTransaction(String orderDetailId, String orderGuid, String billingLevel, 
                            String mgrCode, String mgrPassword,
                            String context, BillingVO bVO, PaymentVO[] pVO,
                            GcCouponVO gcVO, GiftCardVO gdVO, CreditCardVO ccVO, CommonCreditCardVO cccVO,
														String csrId)
        throws IOException, ParserConfigurationException, SAXException, SQLException,
                TransformerException, Exception
  {
    for (int i=0;i<pVO.length;i++)
    {
      logger.debug("pVO.getAdditionalBillId = " + pVO[i].getAdditionalBillId());
    }

    //Check to see if manager code is authorized to approve
    if(StringUtils.isNotEmpty(mgrCode))
    {
      String unitID = ConfigurationUtil.getInstance().getProperty(COMConstants.SECURITY_FILE, COMConstants.UNIT_ID);
      
      Object token = null;
      boolean authenticated = false;
      try
      {
        token = SecurityManager.getInstance().authenticateIdentity(context, unitID, mgrCode, mgrPassword);		  
        authenticated = true;
      }
      catch(ExpiredIdentityException e){logger.debug("AddBilling BO - order_detail_id = " + orderDetailId + "  User was not authenticated:", e);}
      catch(ExpiredCredentialsException e){logger.debug("AddBilling BO - order_detail_id = " + orderDetailId + "  User was not authenticated:", e);}
      catch(AuthenticationException e){logger.debug("AddBilling BO - order_detail_id = " + orderDetailId + "  User was not authenticated:", e);}
      catch(TooManyAttemptsException e){logger.debug("AddBilling BO - order_detail_id = " + orderDetailId + "  User was not authenticated:", e);}
      
      if(!authenticated || !SecurityManager.getInstance().assertPermission(COMConstants.RESOURCE_CONTEXT, token, 
                    COMConstants.RESOURCE_NO_CHARGE, COMConstants.RESOURCE_NO_CHARGE_PERMISSION))
      {
         //not an exception, but rollback nonetheless. 
         Document statusDoc = this.createStatusDocument(COMConstants.AB_REASON_DECLINE, 
                                                           COMConstants.AB_REASON_DECLINE, 
                                                           COMConstants.MA_DECLINE_MESSAGE,
                                                           "N", "Y");
         return statusDoc;                         
      }
    }

		// Obtain cart credit card cart payment before it is updated.  This is used for processing the order later on.
		PaymentDAO pDAO = new PaymentDAO(conn);
		PaymentVO ccCartPVO = null;
		if(billingLevel.equalsIgnoreCase(COMConstants.BILLING_LEVEL_CART))
		{
			ccCartPVO = pDAO.getCartPaymentCC(orderDetailId);
		}
		
    try
    {
      //Authorize the credit card request if it exists
      //First determine if global authorization exists
      if(cccVO != null)
      {
        orderGuid = pVO[0].getOrderGuid();
        CreditCardBO ccBO = new CreditCardBO(conn);
        CreditCardDAO ccDAO = new CreditCardDAO(conn);         
        if (cccVO.getCreditCardNumber() == null)
        {
          //the card will be null due an original card having the expire dates changed
          //go get the full credit card number and update the expire dates on the database
          Document origCCDoc = ccDAO.getOriginalCreditCardFromOrder(orderGuid);
          if (origCCDoc != null)
          {
            String XPath = "//ORIGINAL_CREDITCARDS/ORIGINAL_CREDITCARD";
            NodeList nodeList = DOMUtil.selectNodes(origCCDoc,XPath);
            Node node = null;
            if (nodeList != null)
            {
              for (int i = 0; i < nodeList.getLength(); i++)
              {
                node = (Node) nodeList.item(i);
                if (node != null)
                {
                  cccVO.setCreditCardNumber((DOMUtil.selectSingleNode(node,"cc_number/text()").getNodeValue()));
                  cccVO.setCcId((DOMUtil.selectSingleNode(node,"cc_id/text()").getNodeValue()));
                  pVO[0].setCardNumber((DOMUtil.selectSingleNode(node,"cc_number/text()").getNodeValue()));
                  pVO[0].setCardId((DOMUtil.selectSingleNode(node,"cc_id/text()").getNodeValue()));
                }
              }
            }
          }
        }

        /*if(pVO[0].getAuthOverrideFlag().equalsIgnoreCase("N"))
        {
          cccVO = ccBO.authorize(cccVO, orderGuid);
          if(cccVO.getValidationStatus() == null || !cccVO.getValidationStatus().equalsIgnoreCase("A"))
          {
           //not an exception, but rollback nonetheless. 
            Document statusDoc = this.createStatusDocument(COMConstants.AB_REASON_DECLINE, 
                                                              COMConstants.AB_ACTION_DECLINE_CREDIT, 
                                                              COMConstants.CC_DECLINE_MESSAGE,
                                                              "N", null);
            return statusDoc;
          }
        }*/
		
        //If no customer id exists obtain the customer id
        if(cccVO.getCustomerId() == null || cccVO.getCustomerId().equals(""))
        {
          CustomerDAO cDAO = new CustomerDAO(conn); 
          CustomerVO custVO = cDAO.getCustomerByOrder(orderGuid);
          cccVO.setCustomerId(String.valueOf(custVO.getCustomerId()));
        }
      }

      //Update the billing tables
      BillingDAO bDAO = new BillingDAO(conn);
      BigDecimal orderBillingID = new BigDecimal(0);
    
      if(!billingLevel.equalsIgnoreCase(COMConstants.BILLING_LEVEL_CART))
      {
        orderBillingID = bDAO.addBilling(bVO);
      }
      BigDecimal creditCardId = new BigDecimal(0);
      if(ccVO != null && cccVO != null)
      {
        CreditCardDAO ccDAO = new CreditCardDAO(conn);
        creditCardId = ccDAO.addCreditCardGD(ccVO, cccVO); //ccDAO.addCreditCard(ccVO, cccVO);
      }
    
      if(gcVO != null && gcVO.getGcCouponNumber() != null && gcVO.getGcCouponNumber().length()>0)
      {
    	GcCouponDAO gcDAO = new GcCouponDAO(conn);
        if (bVO != null)
        {
          orderDetailId = String.valueOf(bVO.getOrderDetailId());
        }
        //determine the amount to report to the gift certificate redemption.  If there are two 
        //payment records, the full amount of the gift card was applied.  Set the gcVO amount to the 
        //gift card max and set the payment record for the gift card to the gift card max.
        //if only one payment record exists, the gift card covered the entire amount of the order 
        //total.  Set the gcVO to the placeholder gcVO amount minus the order total amount.
        //set that result to the payment record credit amount.

        //apply the full amount of the gift card to the payment
        if(pVO.length > 1)
        {
          pVO[1].setCreditAmount(new Double(gcVO.getAmount()).doubleValue());
        }
        //only one payment record exists...so the gift card covered the entire purchase.
        //take the gcVO amount (placeholder for the total gift card amount and add the negative remaining amount
        //take that total and set it to the gcVO amount and the payment record credit amount.
        else
        {
          gcVO.setAmount(String.valueOf(new Double(gcVO.getAmount()).doubleValue() + pVO[0].getCreditAmount()));
          pVO[0].setCreditAmount(new Double(gcVO.getAmount()).doubleValue());
        }
 
		//retrieve GC status
		RetrieveGiftCodeResponse retrieveGiftCodeResponse = GiftCodeUtil.getGiftCodeById(gcVO.getGcCouponNumber());
		
		if(retrieveGiftCodeResponse != null){
			gcVO.setStatus(retrieveGiftCodeResponse.getStatus());
		}
		UpdateGiftCodeRequest updateGiftCodeRequest = this.setUpdateGiftCodeRequest(gcVO, orderDetailId);

		if (updateGiftCodeRequest.getStatus().equalsIgnoreCase("Reinstate")
				|| updateGiftCodeRequest.getStatus().equalsIgnoreCase("Issued Active")
				|| updateGiftCodeRequest.getStatus().equalsIgnoreCase("Redeemed")) {
			updateGiftCodeRequest.setStatus("Redeemed");
		}
		String redeemStatus = GiftCodeUtil.adminUpdateStatus(updateGiftCodeRequest);
		
        if(!redeemStatus.equalsIgnoreCase("Redeemed"))
        {
          //not an exception, but rollback nonetheless. 
          Document statusDoc = this.createStatusDocument(COMConstants.AB_REASON_DECLINE, 
                                                            COMConstants.AB_ACTION_DECLINE_GCC,
                                                            COMConstants.GCC_REDEEMED_MSG,
                                                            "N", "Y");
          return statusDoc;        
        }
      }
      if(gdVO != null && gdVO.getGcCouponNumber() != null && gdVO.getGcCouponNumber().length()>0)
      {
        GiftCardBO gdBO = new GiftCardBO(conn);
        
        try {
        //apply the full amount of the gift card to the payment
        if(pVO.length > 1)
        {
          pVO[1].setCreditAmount(new Double(gdVO.getBalance()).doubleValue());
          gdBO.authorizePayment(pVO[1]);
        }
        //only one payment record exists...so the gift card covered the entire purchase.
        //take the gcVO amount (placeholder for the total gift card amount and add the negative remaining amount
        //take that total and set it to the gcVO amount and the payment record credit amount.
        else
        {
          gdVO.setBalance(String.valueOf(new Double(gdVO.getBalance()).doubleValue() - pVO[0].getCreditAmount()));
          pVO[0].setCreditAmount(new Double(gdVO.getBalance()).doubleValue());
          gdBO.authorizePayment(pVO[0]);
        }
        }
        catch (GiftCardException gdexc) {
        
         //not an exception, but rollback nonetheless. 
          Document statusDoc = this.createStatusDocument(COMConstants.AB_REASON_DECLINE, 
                                                            COMConstants.AB_ACTION_DECLINE_GCC,
                                                            COMConstants.GCC_REDEEMED_MSG,
                                                            "N", "Y");
          logger.error(gdexc);
          return statusDoc;        
        }
      }
      //Update the VO with Credit Card and billingID info.
      if(pVO != null && pVO.length>0)
      {
        for(int i=0;i<pVO.length;i++)
        {
          if(!billingLevel.equalsIgnoreCase(COMConstants.BILLING_LEVEL_CART))
          {
             OrderBillVO obVO = new OrderBillVO();
             obVO.setOrderBillId(orderBillingID.toString());
             pVO[i].setAdditionalBillId(orderBillingID.longValue());
             pVO[i].setOrderBill(obVO);
          }
          
          if(cccVO != null && pVO[i].getCardNumber() != null)
          {
            if (creditCardId.intValue() > 0)
            {
              pVO[i].setCardId(creditCardId.toString());
            }
            if (pVO[i].getGcCouponNumber().length() > 4)
            {
            	pVO[i].setGcCouponNumber("***************" + pVO[i].getGcCouponNumber().substring(pVO[i].getGcCouponNumber().length() - 4,pVO[i].getGcCouponNumber().length()));
            }
            pVO[i].setCardNumber(cccVO.getCreditCardNumber());
            pVO[i].setAuthResult("AP");
            //pVO[i].setAuthorization(cccVO.getApprovalCode());
            pVO[i].setAvsCode(cccVO.getAVSIndicator());
            pVO[i].setAcqRefNumber(cccVO.getAcquirerReferenceData());
          }
        }

        //update the payment tables
        //For billing level of cart
        //Get all existing payments
        //if any CreditCard payments exist, move them to the history table 
        //process the new payment
        if(pVO != null && pVO.length>0)
        {
          OrderDAO oDAO = new OrderDAO(conn);
          if(billingLevel.equalsIgnoreCase(COMConstants.BILLING_LEVEL_CART))
          {
            Document paymentInfo = oDAO.getPaymentInfo("none", orderDetailId);
            this.archiveAndDeleteCartPayment(paymentInfo);
          }
          pDAO.addOrderPaymentsGD(pVO); //pDAO.addOrderPayments(pVO);
        }
      }

    }
    catch(Throwable tr)
    {
     //its an exception... rollback. 
      Document statusDoc = this.createStatusDocument(COMConstants.AB_STATUS_FAILURE, 
                                                        COMConstants.AB_REASON_MSG,
                                                        tr.toString(), 
                                                        "Y", "Y");
      logger.error(tr);
      return statusDoc;
    }
    
		//Process the order if the disposition is held
		if(billingLevel.equalsIgnoreCase(COMConstants.BILLING_LEVEL_CART))
		{
			OrderBO oBO = new OrderBO(conn);
			oBO.processOrderForUpdate(pVO[0].getOrderGuid(), null, ccCartPVO, csrId);			
		}

    //Note that the following will create <rollback_transaction>N</rollback_transaction>
    Document statusDoc = this.createStatusDocument(COMConstants.AB_STATUS_SUCCESS, 
                                                      COMConstants.AB_STATUS_SUCCESS,
                                                      COMConstants.AB_STATUS_SUCCESS,
                                                      "N", "N");

    return statusDoc;
  }
  
		
	/**
	 * Setting up request for calling Gift Code Service Update
	 * 
	 * @param vo
	 * @return UpdateGiftCodeRequest
	 */
	private UpdateGiftCodeRequest setUpdateGiftCodeRequest(GcCouponVO gcVO, String orderDetailId) {
		List<RedemptionDetails> lRedemptionDetails = new ArrayList(); 
		RedemptionDetails redemptionDetails = new RedemptionDetails();
		UpdateGiftCodeRequest updateGiftCodeReq = new UpdateGiftCodeRequest();
		updateGiftCodeReq.setGiftCodeId(gcVO.getGcCouponNumber().trim());
		updateGiftCodeReq.setStatus(gcVO.getStatus());
		updateGiftCodeReq.setTotalRedemptionAmount(new BigDecimal(gcVO.getAmount()));
		updateGiftCodeReq.setUpdatedBy(this.csrId);
		redemptionDetails = new RedemptionDetails();
		redemptionDetails.setRedeemedRefNo(orderDetailId);
		redemptionDetails.setRedemptionAmount(new BigDecimal(gcVO.getAmount()));
		lRedemptionDetails.add(redemptionDetails);	
		updateGiftCodeReq.setRedemptionDetails(lRedemptionDetails);
		
		return updateGiftCodeReq;
	}


	/**
	 * @param paymentType
	 * @return
	 * @throws Exception
	 */
	private String getAuthPGRoute(String route) throws Exception
	{
		String authPGRoute = null;
		
		ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
	    String pgGlobalRoutes = configUtil.getFrpGlobalParm("SERVICE","PAYMENT_GATEWAY_ROUTES");
	    String pgSVCGlobal = configUtil.getFrpGlobalParm("SERVICE","PAYMENT_GATEWAY_SVC_ENABLED");
	    //String jccasCardTypes = configUtil.getFrpGlobalParm("AUTH_CONFIG","bams.cc.list");
	    
	    if(pgSVCGlobal !=null && !pgSVCGlobal.isEmpty() && pgSVCGlobal.equalsIgnoreCase("Y"))
	    {	
		    if(pgGlobalRoutes!=null && !pgGlobalRoutes.isEmpty()) 
		    {
		    	if(pgGlobalRoutes.contains(route))
				{ 	
					authPGRoute=COMConstants.PG_PS;
				}
		    }	
	    }
	    return authPGRoute;
	    
	}
	
	
}