package com.ftd.customerordermanagement.bo;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import javax.naming.InitialContext;
import javax.transaction.UserTransaction;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.dao.OrderDAO;
import com.ftd.customerordermanagement.dao.QueueDAO;
import com.ftd.customerordermanagement.vo.OrderDetailHoldVO;
import com.ftd.customerordermanagement.vo.PaymentVO;
import com.ftd.messaging.util.MessagingServiceLocator;
import com.ftd.op.order.to.OrderTO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * This class handles business logic necessary to determine if an order is 
 * held and if so, submits the order for processing.
 *
 * @author Mike Kruger
 */

public class OrderBO
{
  private Logger logger = new Logger("com.ftd.customerordermanagement.bo.OrderBO");
  
  //database:
  private Connection conn = null;
	
	// Database values returned
	private static final String DB_IN_CREDIT_QUEUE_HISTORY = "in_credit_queue_history";
	private static final String DB_MESSAGE_ID = "message_id";
	
	// Results received back from viewDetailIds call
	private static final String RESULTS_ORDER_DETAIL_ID = "order_detail_id";
    
  /**
   * constructor
   * 
   * @param Connection - database connection
   * @return n/a
   * @throws n/a
   */
  public OrderBO(Connection conn)
  {
    super();
    this.conn = conn;
  }


  /**
   * Process orders for an updated or additional payment
   * @param String  - orderGuid
   * @param String  - orderDetailID
   * @throws SAXException
   * @throws ParserConfigurationException
   * @throws IOException
   * @throws SQLException
   * @throws Exception
   * @throws Exception
   * @throws TransformerException
   * @throws XSLException
   */
  public void processOrderForUpdate(String orderGuid, String orderDetailId,
		PaymentVO ccCartPayment, String csrId)
		throws IOException, ParserConfigurationException, SAXException, SQLException,
    TransformerException, Exception
  {
		/* If no credit card payment exists then the order was already processed.
		 */ 
    if(ccCartPayment == null)
			return;

		// Method level variables
    UserTransaction userTransaction = null;
		boolean isCartAuthDeclined = false;
		boolean isCartAuthNull = false;
    ArrayList orderids = new ArrayList();
		ArrayList orderIdsToProcess = new ArrayList();
    OrderDAO oDAO = new OrderDAO(conn);
		
		/* Go get the order detail ids for the orderGuid
		 * else put the orderDetailId in the list
		 */
    if(orderGuid != null)
    {
      CachedResultSet results = (CachedResultSet)oDAO.viewDetailIds(orderGuid);
      while (results.next())
      {
        orderids.add(results.getString(RESULTS_ORDER_DETAIL_ID));
      }
    }
    else
    {
       orderids.add(orderDetailId);
    }

		// Decide if the cart payment was declined or null
		isCartAuthDeclined = false;
		isCartAuthNull = false;
		if(ccCartPayment.getAuthResult() == null 
			|| ccCartPayment.getAuthResult().equalsIgnoreCase(COMConstants.EMPTY_STRING))
		{
			isCartAuthNull = true;		
		}
		else if(ccCartPayment.getAuthResult() != null && 
						ccCartPayment.getAuthResult().equalsIgnoreCase(COMConstants.AUTH_RESULT_DECLINED))
		{
				isCartAuthDeclined = true;				
		}

		// get the initial context
		InitialContext iContext = new InitialContext();

		//get the transaction timeout
		int transactionTimeout = Integer.parseInt((String)ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, COMConstants.TRANSACTION_TIMEOUT_IN_SECONDS));
		// Retrieve the UserTransaction object
		String jndi_usertransaction_entry = ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, COMConstants.JNDI_USERTRANSACTION_ENTRY);
		userTransaction = (UserTransaction)  iContext.lookup(jndi_usertransaction_entry);
		userTransaction.setTransactionTimeout(transactionTimeout);

		// Start a transaction
		userTransaction.begin();

		try
		{
			/* Iterate through the list of order detail ids.
			 */
			for(Iterator it = orderids.iterator(); it.hasNext();)
			{
				boolean isCreditQ = false;
				String detailId = (String) it.next();
				String orderDispCode = (String) oDAO.getOrderDispCode(detailId);
				if(orderDispCode == null)
					orderDispCode = COMConstants.EMPTY_STRING;
	
				// Determine if order has a credit queue item associated with it
				QueueDAO qDAO = new QueueDAO(conn);
				CachedResultSet creditQueue = qDAO.isOrderInCredit(detailId);
				String inQueueHistory = null;
				String messageId = null;					
				if(creditQueue.next())
				{
					inQueueHistory = creditQueue.getString(DB_IN_CREDIT_QUEUE_HISTORY);
					messageId = creditQueue.getString(DB_MESSAGE_ID);					
				}
					
	
				// If it is has an existing queue item
				if(messageId != null && !messageId.equalsIgnoreCase(COMConstants.EMPTY_STRING))
				{
					isCreditQ = true;
				}
				// If it is has a deleted queue item
				else if (inQueueHistory != null && inQueueHistory.equalsIgnoreCase(COMConstants.CONS_YES))
				{
					isCreditQ = true;
				}
	
				// Obtain order holds			
				OrderDetailHoldVO odhVO = new OrderDetailHoldVO();
				ArrayList holds = (ArrayList) oDAO.getHold(new Long(detailId).longValue());
	
				/* Iterate through order holds.
				 */
				for(Iterator j = holds.iterator(); j.hasNext(); )
				{
					odhVO = (OrderDetailHoldVO) j.next();
	
					if(logger.isDebugEnabled())
					{
						logger.debug("Parameters to determine if the order should be processed:\n");
						logger.debug("orderDispCode:" + orderDispCode + "\n");
						logger.debug("isCartAuthNull:" + isCartAuthNull + "\n");
						logger.debug("isCartAuthDeclined:" + isCartAuthDeclined + "\n");
						logger.debug("isCreditQ:" + isCreditQ + "\n");
						logger.debug("odhVO.getReason():" + odhVO.getReason() + "\n");
						logger.debug("odhVO.getStatus():" + odhVO.getStatus() + "\n");
						logger.debug("detailId:" + detailId + "\n");
						logger.debug("messageId:" + messageId + "\n");
						logger.debug("csrId:" + csrId + "\n");
					}
					
					// If the order is Held
					if(orderDispCode.equalsIgnoreCase(COMConstants.ORDER_DISPOSITION_HELD))
					{
						/* If the cart credit card auth result is null or declined and 
						 * there is an item in the Credit queue and the order hold reason
						 * is RE_AUTH and the order is not processed
						 */ 
						if((isCartAuthNull || 
							isCartAuthDeclined) && 
							isCreditQ && 
							odhVO.getReason().equalsIgnoreCase(COMConstants.HOLD_REASON_REAUTH)  &&
							odhVO.getStatus().equalsIgnoreCase(COMConstants.HOLD_STATUS_NOT_PROCESSED)) 
						{
							// Process order and set order disposition to validated
							orderIdsToProcess.add(detailId);
							oDAO.updateOrderDisposition(Long.valueOf(detailId).longValue(), COMConstants.ORDER_DISPOSITION_VALIDATED, csrId);
							
							// Delete order hold
							oDAO.deleteOrderHold(new Long(detailId).longValue(), odhVO.getReason());
	
							// Delete credit queue item
							if(messageId != null && !messageId.equalsIgnoreCase(COMConstants.EMPTY_STRING))
							{
								qDAO.deleteFromQueue(messageId, csrId);
							}
						}					
						/* If the cart credit card auth result is null or declined and 
						 * there is not an item in the Credit queue and the order hold reason
						 * is RE_AUTH and the order is not processed
						 */ 
						else if(isCartAuthNull && 
							!isCreditQ && 
							odhVO.getReason().equalsIgnoreCase(COMConstants.HOLD_REASON_REAUTH)  &&
							odhVO.getStatus().equalsIgnoreCase(COMConstants.HOLD_STATUS_NOT_PROCESSED)) 
						{
							// Process order and set order disposition to validated
							orderIdsToProcess.add(detailId);
							oDAO.updateOrderDisposition(Long.valueOf(detailId).longValue(), COMConstants.ORDER_DISPOSITION_VALIDATED, csrId);
	
							// Set hold record status to N	
							odhVO.setStatus(COMConstants.HOLD_STATUS_PROCESSED);
							oDAO.updateOrderHold(odhVO);
						}
					}
					// If the order is Validated or Processed
					else if(orderDispCode.equalsIgnoreCase(COMConstants.ORDER_DISPOSITION_VALIDATED) ||
									orderDispCode.equalsIgnoreCase(COMConstants.ORDER_DISPOSITION_PROCESSED))
					{
						/* If the cart credit card auth result is null or declined and 
						 * there is an item in the Credit queue and the order hold reason
						 * is RE_AUTH and the order is processed
						 */ 
						if((isCartAuthNull || 
							isCartAuthDeclined) && 
							isCreditQ && 
							odhVO.getReason().equalsIgnoreCase(COMConstants.HOLD_REASON_REAUTH)  &&
							odhVO.getStatus().equalsIgnoreCase(COMConstants.HOLD_STATUS_PROCESSED)) 
						{
							// Delete order hold
							oDAO.deleteOrderHold(new Long(detailId).longValue(), odhVO.getReason());
	
							// Delete credit queue item						
							if(messageId != null && !messageId.equalsIgnoreCase(COMConstants.EMPTY_STRING))
							{
								qDAO.deleteFromQueue(messageId, csrId);
							}
						}					
						/* If the cart credit card auth result is null or declined and 
						 * there is not an item in the Credit queue and the order hold reason
						 * is RE_AUTH and the order is processed
						 */ 
						else if(isCartAuthNull && 
							!isCreditQ && 
							odhVO.getReason().equalsIgnoreCase(COMConstants.HOLD_REASON_REAUTH)  &&
							odhVO.getStatus().equalsIgnoreCase(COMConstants.HOLD_STATUS_PROCESSED)) 
						{
							// Set hold record status to N	
							odhVO.setStatus(COMConstants.HOLD_STATUS_PROCESSED);
							oDAO.updateOrderHold(odhVO);
						}
					}
				}
			}

			// Commit the transaction
			userTransaction.commit();
		}
		catch(Throwable t)
		{
			logger.error(t);

			if (userTransaction != null)  
			{
				// rollback the user transaction
				userTransaction.rollback();
				logger.debug("User transaction rolled back");
			}

			throw new Exception(t);
		}
		
		// Process the orders once all database changes have been committed
		for(Iterator it = orderIdsToProcess.iterator(); it.hasNext(); )
		{
			String processDetailId = (String)it.next();

			if(logger.isDebugEnabled())
			{
				logger.debug("Sending order detail id " + processDetailId + " for processing.");
			}
			
			processOrder(processDetailId);
		}
	}


  /**
   * Process an order 
   * @param String  - orderDetailID
   * @throws SAXException
   * @throws ParserConfigurationException
   * @throws IOException
   * @throws SQLException
   * @throws Exception
   * @throws Exception
   * @throws TransformerException
   * @throws XSLException
   */
  private void processOrder(String orderDetailId)
		throws IOException, ParserConfigurationException, SAXException, SQLException,
    TransformerException, Exception
  {
		OrderTO oTO = new OrderTO();
		oTO.setOrderDetailId(orderDetailId);
    oTO.setRWDFlagOverride(Boolean.TRUE);
		MessagingServiceLocator.getInstance().getOrderAPI().processOrder(oTO, conn);
		
		//Test code.
		//Use this code when running on apollo or locally.
		/*
		Hashtable env = new Hashtable();
		env.put(Context.INITIAL_CONTEXT_FACTORY, "com.evermind.server.rmi.RMIInitialContextFactory");
		env.put(Context.SECURITY_PRINCIPAL, "admin");
		env.put(Context.SECURITY_CREDENTIALS, "welcome");
		env.put(Context.PROVIDER_URL, "opmn:ormi://illiad-prod.ftdi.com:6003:OC4J_ORDER_PROCESSING/orderprocessing");
		env.put("dedicated.rmicontext","true"); // for 9.0.2.1 and above
		env.put("dedicated.connection","true"); // for 9.0.2.0.0
		InitialContext context = new InitialContext(env);
		Object homeObject = context.lookup("ejb/OrderAPI");
		
		OrderAPIHome hh = (OrderAPIHome)
		PortableRemoteObject.narrow(homeObject, OrderAPIHome.class);
		
		OrderAPI orderAPI = (OrderAPI)
		PortableRemoteObject.narrow(hh.create(), OrderAPI.class);
		logger.debug("output = " + orderAPI.helloWorld("Hello"));
		
		orderAPI.processOrder(oTO);          
		*/
	}
  
  public String getStateByZipcodeAndCity(String zipCode, String city) throws Exception {
  	
      String result = null;
      try {
         
          if (conn == null) {
              throw new Exception("Unable to connect to database");
          }
          if(city == null || city.trim().equals("")){
          	city = "";
          }
          OrderDAO orderDAO = new OrderDAO(conn);
          result = orderDAO.getStateByZipcodeAndCityAjax(conn, zipCode, city);
      } catch (Throwable t) {
          logger.error("Error in validateStateByZipcodeAndCity()",t);
          throw new Exception("Could not validate state code.");
      }
      
      return result;
  	
  }
}
