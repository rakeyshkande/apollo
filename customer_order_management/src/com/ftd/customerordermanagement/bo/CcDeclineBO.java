package com.ftd.customerordermanagement.bo;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.io.IOException;

import java.sql.SQLException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;




import org.w3c.dom.Document;
import org.w3c.dom.Element;

import org.xml.sax.SAXException;
/**
 * This class handles credit card decline message logic.
 *
 * @author Luke W. Pennings
 */

public class CcDeclineBO
{
  private Logger logger = new Logger("com.ftd.customerordermanagement.bo.CcDeclineBO");
  private static final String MESSAGE_ROOT_NODE = "root";

   /**
   * constructor
   * 
   * @param none
   * @return n/a
   * @throws n/a
   */
  public CcDeclineBO()
  {
    super();
  }


  /**
   * get message declined information
   * @param none
   * @return Document - message text
   * @throws Exception
   * @throws IOException
   * @throws ParserConfigurationException
   * @throws SAXException
   * @throws SQLException
   * @throws TransformerException
   * @throws XSLException
   */
  public Document getMessage(String payType)
         throws IOException, ParserConfigurationException, SAXException, SQLException,
                TransformerException, Exception
  {
    ConfigurationUtil cu = null;
    cu = ConfigurationUtil.getInstance();
//	Call the ConfigurationUtil utility to obtain the credit card declined message from customer_order_management_config.xml
    String ccMessage = (String) cu.getProperty(COMConstants.PROPERTY_FILE,COMConstants.CC_DECLINE_MESSAGE);

    Document returnDocument = DOMUtil.getDefaultDocument();
    Element root = (Element) returnDocument.createElement(MESSAGE_ROOT_NODE);
    returnDocument.appendChild(root);

    Element node;
    node = (Element)returnDocument.createElement("message");
    node.appendChild(returnDocument.createTextNode(ccMessage));
    root.appendChild(node);
    
    node = (Element)returnDocument.createElement("pay_type");
    node.appendChild(returnDocument.createTextNode(payType));
    root.appendChild(node);

    return returnDocument;  
  }
}