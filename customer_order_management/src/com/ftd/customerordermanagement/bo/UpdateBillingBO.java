package com.ftd.customerordermanagement.bo;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.dao.OrderDAO;
import com.ftd.customerordermanagement.dao.PaymentDAO;
import com.ftd.customerordermanagement.vo.PaymentVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.ArrayList;

import javax.naming.Context;
import javax.naming.InitialContext;

import javax.transaction.UserTransaction;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;


import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

import org.xml.sax.SAXException;

/**
 * This class handles update billing logic.
 *
 * @author Mike Kruger
 */

public class UpdateBillingBO
{
  private Logger logger = new Logger("com.ftd.customerordermanagement.bo.UpdateBillingBO");

	private static final String CREDIT_CARD_PAYMENT_TYPE = "C";
  private static final String NO_CHARGE_PAYMENT_TYPE = "NC";
  private static final String REFUND_PAYMENT_TYPE = "Refund";

  //database:
  private Connection conn = null;

  /**
   * constructor
   * 
   * @param Connection - database connection
   * @return n/a
   * @throws n/a
   */
  public UpdateBillingBO(Connection conn)
  {
    super();
    this.conn = conn;
  }


  /**
   * Obtain payment information including editable field status
   * @param String  - orderGuid
   * @param String  - orderDetailID
   * @param String  - billingLevel
   * @return Document - document of payment information
   * @throws IOException
   * @throws ParserConfigurationException
   * @throws SAXException
   * @throws SQLException
   * @throws TransformerException
   * @throws XSLException
   * @throws Exception
   */
   
  public Document getPaymentInfo(String orderDetailId)
         throws IOException, ParserConfigurationException, SAXException, SQLException,
                TransformerException, Exception
  {
    Document doc = null;
    ArrayList ccIds = new ArrayList();

    if (orderDetailId != null && orderDetailId.trim().length() > 0)
    {
        String XPath = "//ORDER_PAYMENTS/ORDER_PAYMENT";
        String paymentType = null;
        String eodInd = null;
        String userAuthInd = "N";
        String paymentIndicator = null;
        String yes = "Y";
        String no = "N";
        String auth_status = "editable";
        Element authStatusNode = null;
        Text newText = null;

        OrderDAO oDao = new OrderDAO(conn);

				/* We never obtain cart payments specifically.  The cart payment is always included
				 * in the order payments.  If the cart has not been authorized
				 * there will be no order payments so there is no reason to distinguish
				 * between cart or order payment queries.  We always use the order payment
				 * query based on order detail id to get both cart and order info.
				 * The shopping cart page passes us the first order in the shopping cart.
				 * This design was used based on the fact that we are running out of time.
				*/
        doc = oDao.getPaymentInfo("",orderDetailId);
					
        if(logger.isDebugEnabled())
        {
            //IMPORTANT:  Because StringWriter contents are NOT removed even when the flush
            //            method is executed, make StringWriter go away through scope control.
            //            Both the flush and close methods were tested with but were of none affect.
            StringWriter sw = new StringWriter();       //string representation of xml document
            DOMUtil.print((Document) doc,new PrintWriter(sw));
            logger.debug("- execute(): paymentInfo returned = \n" + sw.toString());
        }
        
				PaymentDAO pDao = new PaymentDAO(conn);
				ccIds = pDao.getPaymentIDByType(CREDIT_CARD_PAYMENT_TYPE);
       
        NodeList nodeList = DOMUtil.selectNodes(doc,XPath);
        Node node = null;

        if (nodeList != null)
        {
          for (int i = 0; i < nodeList.getLength(); i++)
          {
            node = (Node) nodeList.item(i);
            if (node != null)
            {
              paymentIndicator = (DOMUtil.selectSingleNode(node,"payment_indicator/text()").getNodeValue());
              authStatusNode = (Element)doc.createElement("auth_status");
              if (paymentIndicator.indexOf(REFUND_PAYMENT_TYPE) == -1)
              {
                paymentType =  (DOMUtil.selectSingleNode(node,"payment_type/text()").getNodeValue());
                if(DOMUtil.selectSingleNode(node,"user_auth_indicator/text()") != null)
                {
                  userAuthInd = (DOMUtil.selectSingleNode(node,"user_auth_indicator/text()").getNodeValue());
                }
                eodInd = (DOMUtil.selectSingleNode(node,"eod_indicator/text()").getNodeValue());
                
                /*  Don't know why No Charge is designated as a credit card type
                    but we will account for it here
                */
                if ((ccIds.contains(paymentType)) && (!paymentType.equalsIgnoreCase(NO_CHARGE_PAYMENT_TYPE)))
                {
                  if ((DOMUtil.selectSingleNode(node,"auth_number/text()") != null))
                  {
                    if (userAuthInd.equalsIgnoreCase(yes) && eodInd.equalsIgnoreCase(no))
                    {
                      auth_status = "editable";
                      authStatusNode.setAttribute("auth_status", auth_status);
                      node.appendChild(authStatusNode);
                    }
                    else
                    {
                      auth_status = "notEditable";
                      authStatusNode.setAttribute("auth_status", auth_status);
                      node.appendChild(authStatusNode);
                    }
                  }
                  else
                  {
                    auth_status = "editable";
                    authStatusNode.setAttribute("auth_status", auth_status);
                    node.appendChild(authStatusNode);
                  }
                }
                else
                {
                  auth_status = "notEditable";
                  authStatusNode.setAttribute("auth_status", auth_status);
                  node.appendChild(authStatusNode);
                }
              }
              else
              {
                auth_status = "notEditable";
                authStatusNode.setAttribute("auth_status", auth_status);
                node.appendChild(authStatusNode);
              }
            }
          }
        }
    }
    return doc;
  }
	
  /**
   * Update Billing Information
   * @param request
   * @param connection
   * @return Document - payment information
   * @throws Exception
   * @throws IOException
   * @throws ParserConfigurationException
   * @throws SAXException
   * @throws TransformerException
   * @throws XSLException
   */
   
  public void UpdateBilling(String orderGuid, String orderDetailId, 
		ArrayList payments, String billingLevel, String csrId)
		throws IOException, ParserConfigurationException, SAXException, TransformerException, 
               NumberFormatException, Exception, Throwable
  {
    PaymentDAO dao = new PaymentDAO(conn);
          
    if (payments != null && payments.size() > 0)
    {
			// Obtain cart credit card cart payment before it is updated.  This is used
			// for processing the orders later on.  Would rather send order guid
			// but there is a pre-existing procedure to obtain the info
			PaymentVO pVO = null;
			if(billingLevel.equalsIgnoreCase(COMConstants.BILLING_LEVEL_CART))
			{
				pVO = dao.getCartPaymentCC(orderDetailId);				
			}

      PaymentVO[] payVO = (PaymentVO[]) payments.toArray(new PaymentVO [payments.size()]); 
      logger.debug("going to updatePayments" + payVO);

	    UserTransaction userTransaction = null;
			try
			{
				// get the initial context
				InitialContext iContext = new InitialContext();
	
				//get the transaction timeout
				int transactionTimeout = Integer.parseInt((String)ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, COMConstants.TRANSACTION_TIMEOUT_IN_SECONDS));
				// Retrieve the UserTransaction object
				String jndi_usertransaction_entry = ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, COMConstants.JNDI_USERTRANSACTION_ENTRY);
				userTransaction = (UserTransaction)  iContext.lookup(jndi_usertransaction_entry);
				userTransaction.setTransactionTimeout(transactionTimeout);
				userTransaction.begin();
				
				dao.updatePayments(payVO);
				
				userTransaction.commit();
				
				if(billingLevel.equalsIgnoreCase(COMConstants.BILLING_LEVEL_CART))
				{
					OrderBO oBO = new OrderBO(conn);
					oBO.processOrderForUpdate(orderGuid, null, pVO, csrId);			
				}
			}
			catch(Throwable t)
			{
	      logger.error(t);
				rollback(userTransaction);
				throw t;
			}
    }
  }
	 /**
  * Rollback user transaction when exceptions occur
  * @param userTransaction - user transaction  
  * @return void
  * @throws none
  */ 
  
  private void rollback(UserTransaction userTransaction)
		throws Exception
  {
     try  
     {
       if (userTransaction != null)  
       {
         // rollback the user transaction
         userTransaction.rollback();
         logger.debug("User transaction rolled back");
       }
     } 
     catch (Exception ex)  
     {
       logger.error(ex);
			 throw ex;
     } 
  }
}