package com.ftd.customerordermanagement.bo;

import com.ftd.customerordermanagement.dao.CustomerDAO;
import com.ftd.customerordermanagement.vo.CommonCreditCardVO;
import com.ftd.customerordermanagement.vo.CustomerVO;
import com.ftd.messaging.util.MessagingServiceLocator;
import com.ftd.op.order.to.CreditCardTO;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.Hashtable;
import javax.naming.Context;
import javax.naming.InitialContext;

import javax.rmi.PortableRemoteObject;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;


import org.xml.sax.SAXException;
/**
 * This class handles credit card varification for add billing.
 *
 * @author Luke W. Pennings
 */

public class CreditCardBO
{
  private Logger logger = new Logger("com.ftd.customerordermanagement.bo.CreditCardBO");

  //database:
  private Connection conn = null;
    
  /**
   * constructor
   * 
   * @param Connection - database connection
   * 
   * @return n/a
   * 
   * @throws n/a
   */
  public CreditCardBO(Connection conn)
  {
    super();
    this.conn = conn;
  }


  /**
   * authorize the credit card
   * @param String  - orderGuid
   * @param CommonCreditCardVO
   * @return CommonCreditCardVO (updated)
   * @throws Exception
   * @throws IOException
   * @throws ParserConfigurationException
   * @throws SAXException
   * @throws SQLException
   * @throws TransformerException
   * @throws XSLException
   */
  public CommonCreditCardVO authorize(CommonCreditCardVO cccVO, String orderGuid)
         throws IOException, ParserConfigurationException, SAXException, SQLException,
                TransformerException, Exception
  {
    CustomerDAO cDAO = new CustomerDAO(conn); 
    CustomerVO custVO = cDAO.getCustomerByOrder(orderGuid);
    CreditCardTO ccTO = new CreditCardTO();
    ccTO.setAmount(cccVO.getAmount());
    ccTO.setCreditCardNumber(cccVO.getCreditCardNumber());
    ccTO.setCreditCardType(cccVO.getCreditCardType());
    ccTO.setCustomerId(String.valueOf(custVO.getCustomerId()));
    ccTO.setExpirationDate(cccVO.getExpirationDate());
    ccTO.setAddressLine(custVO.getAddress1());
    ccTO.setZipCode(custVO.getZipCode());
    ccTO.setCreditCardDnsType(custVO.getCompanyId());
    ccTO.setEci(cccVO.getEci());
    ccTO.setCavv(cccVO.getCavv());
    ccTO.setXid(cccVO.getXid());
    ccTO.setUcaf(cccVO.getUcaf());
    ccTO.setRoute(cccVO.getRoute());
    ccTO.setNewRoute(cccVO.isNewRoute());
    ccTO.setRequestId(cccVO.getRequestId());
    
    ccTO = MessagingServiceLocator.getInstance().getOrderAPI().validateCreditCard(ccTO, conn);

    //Test source only.
    //Use this source when running on apollo or locally.
/*		
      Hashtable env = new Hashtable();
      env.put(Context.INITIAL_CONTEXT_FACTORY, "com.evermind.server.rmi.RMIInitialContextFactory");
      env.put(Context.SECURITY_PRINCIPAL, "admin");
      env.put(Context.SECURITY_CREDENTIALS, "welcome");
      env.put(Context.PROVIDER_URL, "opmn:ormi://illiad-prod.ftdi.com:6003:OC4J_ORDER_PROCESSING/orderprocessing");
      env.put("dedicated.rmicontext","true"); // for 9.0.2.1 and above
      env.put("dedicated.connection","true"); // for 9.0.2.0.0
      InitialContext context = new InitialContext(env);
      Object homeObject = context.lookup("ejb/OrderAPI");

      OrderAPIHome hh = (OrderAPIHome)
      PortableRemoteObject.narrow(homeObject, OrderAPIHome.class);

      OrderAPI orderAPI = (OrderAPI)
      PortableRemoteObject.narrow(hh.create(), OrderAPI.class);
      logger.debug("output = " + orderAPI.helloWorld("Hello"));

    // Use one of the create() methods below to create a new instance
    ccTO = orderAPI.validateCreditCard(ccTO);
*/
    logger.debug("cccVO = " + cccVO);
    logger.debug("status = " + ccTO.getValidationStatus());
    logger.debug("actionCode = " + ccTO.getActionCode());

    cccVO.setValidationStatus(ccTO.getValidationStatus()); 
    cccVO.setCreditCardType(ccTO.getCreditCardType());
    cccVO.setCreditCardDnsType(ccTO.getCreditCardDnsType());
	cccVO.setCcId(ccTO.getCcId());
    cccVO.setName(ccTO.getName());
	cccVO.setAddress2(ccTO.getAddress2());
    cccVO.setCity(ccTO.getCity());
	cccVO.setCountry(ccTO.getCountry());
	cccVO.setCustomerId(ccTO.getCustomerId());
	cccVO.setAddressLine(ccTO.getAddressLine());
    cccVO.setZipCode(ccTO.getZipCode());
	cccVO.setCreditCardNumber(ccTO.getCreditCardNumber());
    cccVO.setExpirationDate(ccTO.getExpirationDate().substring(0,2) + "/" + ccTO.getExpirationDate().substring(2,4));
    logger.debug("expirationDate = " + cccVO.getExpirationDate());
    cccVO.setUserData(ccTO.getUserData());
    cccVO.setTransactionType(ccTO.getTransactionType());
    cccVO.setAmount(ccTO.getAmount());
    cccVO.setApprovedAmount(ccTO.getApprovedAmount());
    cccVO.setStatus(ccTO.getStatus());
    cccVO.setActionCode(ccTO.getActionCode());
    cccVO.setVerbiage(ccTO.getVerbiage());
    cccVO.setApprovalCode(ccTO.getApprovalCode());
	cccVO.setDateStamp(ccTO.getDateStamp());
    cccVO.setTimeStamp(ccTO.getTimeStamp());
    cccVO.setAVSIndicator(ccTO.getAVSIndicator());
    cccVO.setBatchNumber(ccTO.getBatchNumber());
    cccVO.setItemNumber(ccTO.getItemNumber());
    cccVO.setAcquirerReferenceData(ccTO.getAcquirerReferenceData());
    cccVO.setAuthorizationTransactionId(ccTO.getAuthorizationTransactionId());
    cccVO.setUnderMinAuthAmt(ccTO.isUnderMinAuthAmt());
    logger.debug("approvalCode = " + ccTO.getApprovalCode());
    logger.debug("setAVSIndicator = " + ccTO.getAVSIndicator());
    
    cccVO.setCcAuthProvider(ccTO.getCcAuthProvider());
    cccVO.getPaymentExtMap().putAll(ccTO.getPaymentExtMap());
    cccVO.setEci(ccTO.getEci());
    cccVO.setCavv(ccTO.getCavv());
    cccVO.setXid(ccTO.getXid());
    cccVO.setUcaf(ccTO.getUcaf());
    cccVO.setMerchantRefId(ccTO.getMerchantRefId());
    cccVO.getPaymentExtMap().putAll(ccTO.getPaymentExtMap());
    
    return cccVO;
  }
}