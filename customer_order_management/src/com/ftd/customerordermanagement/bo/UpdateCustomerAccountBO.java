package com.ftd.customerordermanagement.bo;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.dao.CommentDAO;
import com.ftd.customerordermanagement.dao.CustomerDAO;
import com.ftd.customerordermanagement.dao.LockDAO;
import com.ftd.customerordermanagement.util.BasePageBuilder;
import com.ftd.customerordermanagement.vo.CommentsVO;
import com.ftd.customerordermanagement.vo.CustomerPhoneVO;
import com.ftd.customerordermanagement.vo.CustomerVO;
import com.ftd.customerordermanagement.vo.DirectMailVO;
import com.ftd.customerordermanagement.vo.EmailVO;
import com.ftd.customerordermanagement.vo.MembershipVO;
import com.ftd.ftdutilities.FieldUtils;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.security.SecurityManager;
import com.ftd.security.cache.vo.UserInfo;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.w3c.dom.Document;




public class UpdateCustomerAccountBO
{

/*******************************************************************************************
 * Class level variables
 *******************************************************************************************/
  private HttpServletRequest  request                           = null;
  private Connection          con                               = null;
  private static Logger       logger                            = new Logger("com.ftd.customerordermanagement.bo.UpdateCustomerAccountBO");

  private String              action                            = "";
  private String              csrId                             = null;
  private String              displayPage                       = null;
  private String              emailHistoryUrl                   = null;
  //contains the VO that have been changed
  private HashMap             hashChangedDMVO                   = new HashMap();
  private HashMap             hashChangedPhoneVO                = new HashMap();
  //contains all the VOs that were built using the screen info
  private HashMap             hashNewDMVO                       = new HashMap();
  private HashMap             hashNewPhoneVO                    = new HashMap();
  //contains all the VOs that were built using the info from database
  private HashMap             hashOrigDMVO                      = new HashMap();
  private HashMap             hashOrigPhoneVO                   = new HashMap();
  private HashMap             hashXML                           = new HashMap();
  private CustomerVO          newCustomer                       = new CustomerVO();
  private CustomerVO          origCustomer                      = new CustomerVO();
  private HashMap             pageData                          = new HashMap();
  private StringBuffer        sbCustomerComments                = null;
  private String              sessionId                         = null;

  private String              ucBusinessName	                  = null;
  private String              ucCustomerAddress1	              = null;
  private String              ucCustomerAddress2	              = null;
  private String              ucCustomerBirthday	              = null;
  private Calendar            ucCustomerBirthdayCal;
  private String              ucCustomerCity	                  = null;
  private String              ucCustomerCounty 	                = null;
  private String              ucCustomerCountry	                = null;
  private String              ucCustomerFirstName	              = null;
  private String              ucCustomerId                      = null;
  private String              ucCustomerLastName	              = null;
  private String              ucCustomerState	                  = null;
  private String              ucCustomerZipCode	                = null;
  private String              ucDaytimeCustomerExtension        = null;
  private String              ucDaytimeCustomerPhoneId          = null;
  private String              ucDaytimeCustomerPhoneNumber      = null;
  private boolean 			  ucVipCustomer 					= false; 
  private HashMap             ucDirectMailSubscribeStatusHash   = new HashMap();
  private int                 ucDirectMailSubscribeStatusCount	= 0;
  private String              ucEveningCustomerExtension        = null;
  private String              ucEveningCustomerPhoneId          = null;
  private String              ucEveningCustomerPhoneNumber      = null;
  private long                ucLongCustomerId;
  private String              ucMasterOrderNumber               = null;
  private String              ucOrderGuid                       = null;
  private String 			  vipCustUpdatePermission 		    = null; 
  private boolean             updateCustomerRecord              = false;
  private boolean             updatePhoneRecord                 = false;
  private boolean             updateDirectMailRecord            = false;



/*******************************************************************************************
 * Constructor 1
 *******************************************************************************************/
  public UpdateCustomerAccountBO()
  {
  }


/*******************************************************************************************
 * Constructor 2
 *******************************************************************************************/
  public UpdateCustomerAccountBO(HttpServletRequest request, Connection con)
  {
    this.request = request;
    this.con = con;
  }


/*******************************************************************************************
  * processRequest()
  ******************************************************************************************
  *
  * @return HashMap - contains 0 - many XML documents of data, 1 HashMap each for page
  *                   details and search criteria
  * @throws
  */
  public HashMap processRequest() throws Exception
  {
    //HashMap that will be returned to the calling class
    HashMap returnHash = new HashMap();

    //Get info from the request object
    getRequestInfo(this.request);

    //Get config file info
    getConfigInfo();

    if(this.action.equalsIgnoreCase(COMConstants.ACTION_CHECK_CUSTOMER_ACCOUNT))
    {
      processCheckCustomerAccount();
    }
    else if(this.action.equalsIgnoreCase(COMConstants.ACTION_SAVE_CUSTOMER_CHANGES))
    {
      processSaveCustomerChanges();
    }
    else if(this.action.equalsIgnoreCase(COMConstants.ACTION_RELEASE_LOCK))
    {
      processReleaseLock();
    }

    this.pageData.put("XSL",                    COMConstants.XSL_PROCESS_CUSTOMER_ACCOUNT_IFRAME);

    //populate page data
    populatePageData();


    //At this point, we should have three HashMaps.
    // HashMap1 = hashXML         - hashmap that contains zero to many Documents
    // HashMap2 = pageData        - hashmap that contains String data at page level
    //
    //Combine all of the above HashMaps into one HashMap, called returnHash

    //retrieve all the Documents from hashXML and put them in returnHash
    //retrieve the keyset
    Set ks1 = hashXML.keySet();
    Iterator iter = ks1.iterator();
    String key = null;
    Document doc = null;

    //Iterate thru the keyset
    while(iter.hasNext())
    {
      //get the key
      key = iter.next().toString();

      //retrieve the XML document
      doc = (Document) hashXML.get(key);

      //put the XML object in the returnHash
      returnHash.put(key, (Document) doc);
    }

    //append pageData to returnHash
    returnHash.put("pageData",        (HashMap)this.pageData);

    return returnHash;
  }



/*******************************************************************************************
  * getRequestInfo(HttpServletRequest request)
  ******************************************************************************************
  * Retrieve the info from the request object
  *
  * @param  HttpServletRequest - request
  * @return none
  * @throws none
  */
  private void getRequestInfo(HttpServletRequest request) throws Exception
  {
    /****************************************************************************************
     *  Retrieve Action request
     ***************************************************************************************/
    if(request.getAttribute(COMConstants.ACTION) != null)
      this.action = request.getAttribute(COMConstants.ACTION).toString();
    else if(request.getParameter(COMConstants.ACTION)!=null)
      this.action = request.getParameter(COMConstants.ACTION);


    /****************************************************************************************
     *  Retrieve Page Parameters
     ***************************************************************************************/
    //retrieve the parameter that shows the page name which triggered this request.  It could
    //either be the customer account page or the customer shopping cart page.
    if(request.getParameter(COMConstants.DISPLAY_PAGE)!=null)
    {
        this.displayPage = request.getParameter(COMConstants.DISPLAY_PAGE);
    }

    //retrieve the first name
    if(request.getParameter(COMConstants.UC_CUSTOMER_FIRST_NAME)!=null)
    {
      if(!request.getParameter(COMConstants.UC_CUSTOMER_FIRST_NAME).equalsIgnoreCase(""))
      {
        this.ucCustomerFirstName = request.getParameter(COMConstants.UC_CUSTOMER_FIRST_NAME);
      }
    }

    //retrieve the last name
    if(request.getParameter(COMConstants.UC_CUSTOMER_LAST_NAME)!=null)
      if(!request.getParameter(COMConstants.UC_CUSTOMER_LAST_NAME).equalsIgnoreCase(""))
        this.ucCustomerLastName = request.getParameter(COMConstants.UC_CUSTOMER_LAST_NAME);

    //retrieve the business name
    if(request.getParameter(COMConstants.UC_BUSINESS_NAME)!=null)
    {
      if(!request.getParameter(COMConstants.UC_BUSINESS_NAME).equalsIgnoreCase(""))
      {
        this.ucBusinessName = request.getParameter(COMConstants.UC_BUSINESS_NAME);
      }
    }

    //retrieve the address 1
    if(request.getParameter(COMConstants.UC_CUSTOMER_ADDRESS_1)!=null)
      if(!request.getParameter(COMConstants.UC_CUSTOMER_ADDRESS_1).equalsIgnoreCase(""))
        this.ucCustomerAddress1 = request.getParameter(COMConstants.UC_CUSTOMER_ADDRESS_1);

    //retrieve the address 2
    if(request.getParameter(COMConstants.UC_CUSTOMER_ADDRESS_2)!=null)
      if(!request.getParameter(COMConstants.UC_CUSTOMER_ADDRESS_2).equalsIgnoreCase(""))
        this.ucCustomerAddress2 = request.getParameter(COMConstants.UC_CUSTOMER_ADDRESS_2);

    //retrieve the city
    if(request.getParameter(COMConstants.UC_CUSTOMER_CITY)!=null)
      if(!request.getParameter(COMConstants.UC_CUSTOMER_CITY).equalsIgnoreCase(""))
        this.ucCustomerCity = request.getParameter(COMConstants.UC_CUSTOMER_CITY);

    //retrieve the state
    if(request.getParameter(COMConstants.UC_CUSTOMER_STATE)!=null)
      if(!request.getParameter(COMConstants.UC_CUSTOMER_STATE).equalsIgnoreCase(""))
        this.ucCustomerState = request.getParameter(COMConstants.UC_CUSTOMER_STATE);

    //retrieve the zip
    if(request.getParameter(COMConstants.UC_CUSTOMER_ZIP_CODE)!=null)
      if(!request.getParameter(COMConstants.UC_CUSTOMER_ZIP_CODE).equalsIgnoreCase(""))
        this.ucCustomerZipCode = request.getParameter(COMConstants.UC_CUSTOMER_ZIP_CODE);

    //retrieve the country
    if(request.getParameter(COMConstants.UC_CUSTOMER_COUNTRY)!=null)
      if(!request.getParameter(COMConstants.UC_CUSTOMER_COUNTRY).equalsIgnoreCase(""))
        this.ucCustomerCountry = request.getParameter(COMConstants.UC_CUSTOMER_COUNTRY);


    //retrieve the county
    if(request.getParameter(COMConstants.UC_CUSTOMER_COUNTY)!=null)
      if(!request.getParameter(COMConstants.UC_CUSTOMER_COUNTY).equalsIgnoreCase(""))
        this.ucCustomerCounty = request.getParameter(COMConstants.UC_CUSTOMER_COUNTY);

    //retrieve the brithday
    if(request.getParameter(COMConstants.UC_CUSTOMER_BIRTHDAY)!=null)
      if(!request.getParameter(COMConstants.UC_CUSTOMER_BIRTHDAY).equalsIgnoreCase(""))
      {
        this.ucCustomerBirthday = request.getParameter(COMConstants.UC_CUSTOMER_BIRTHDAY);
        if (this.ucCustomerBirthday != null)
        {
          SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
          this.ucCustomerBirthdayCal = Calendar.getInstance();
          this.ucCustomerBirthdayCal.setTime(inputFormat.parse(this.ucCustomerBirthday));
        }
      }


    /** retrieve the day time phone info **/
    //retrieve the phone id
    if(request.getParameter(COMConstants.UC_DAYTIME_CUSTOMER_PHONE_ID)!=null)
      if(!request.getParameter(COMConstants.UC_DAYTIME_CUSTOMER_PHONE_ID).equalsIgnoreCase(""))
        this.ucDaytimeCustomerPhoneId = request.getParameter(COMConstants.UC_DAYTIME_CUSTOMER_PHONE_ID);

    //retrieve the phone number
    if(request.getParameter(COMConstants.UC_DAYTIME_CUSTOMER_PHONE_NUMBER)!=null)
      if(!request.getParameter(COMConstants.UC_DAYTIME_CUSTOMER_PHONE_NUMBER).equalsIgnoreCase(""))
        this.ucDaytimeCustomerPhoneNumber = request.getParameter(COMConstants.UC_DAYTIME_CUSTOMER_PHONE_NUMBER);

    //retrieve the extension
    if(request.getParameter(COMConstants.UC_DAYTIME_CUSTOMER_EXTENSION)!=null)
      if(!request.getParameter(COMConstants.UC_DAYTIME_CUSTOMER_EXTENSION).equalsIgnoreCase(""))
        this.ucDaytimeCustomerExtension = request.getParameter(COMConstants.UC_DAYTIME_CUSTOMER_EXTENSION);



    /** retrieve the evening phone info **/
    //retrieve the phone id
    if(request.getParameter(COMConstants.UC_EVENING_CUSTOMER_PHONE_ID)!=null)
      if(!request.getParameter(COMConstants.UC_EVENING_CUSTOMER_PHONE_ID).equalsIgnoreCase(""))
        this.ucEveningCustomerPhoneId = request.getParameter(COMConstants.UC_EVENING_CUSTOMER_PHONE_ID);

    //retrieve the phone number
    if(request.getParameter(COMConstants.UC_EVENING_CUSTOMER_PHONE_NUMBER)!=null)
      if(!request.getParameter(COMConstants.UC_EVENING_CUSTOMER_PHONE_NUMBER).equalsIgnoreCase(""))
        this.ucEveningCustomerPhoneNumber = request.getParameter(COMConstants.UC_EVENING_CUSTOMER_PHONE_NUMBER);

    //retrieve the extension
    if(request.getParameter(COMConstants.UC_EVENING_CUSTOMER_EXTENSION)!=null)
      if(!request.getParameter(COMConstants.UC_EVENING_CUSTOMER_EXTENSION).equalsIgnoreCase(""))
        this.ucEveningCustomerExtension = request.getParameter(COMConstants.UC_EVENING_CUSTOMER_EXTENSION);
    
    //retrieve the VIP flag
    if(request.getParameter(COMConstants.UC_VIP_CUSTOMER)!=null)
          this.ucVipCustomer = FieldUtils.convertStringToBoolean(request.getParameter(COMConstants.UC_VIP_CUSTOMER));
    




    /** retrieve the direct mail status info **/

    //retrieve the count
    if(request.getParameter(COMConstants.UC_DIRECT_MAIL_SUBSCRIBE_STATUS_COUNT)!=null)
    {
      if(!request.getParameter(COMConstants.UC_DIRECT_MAIL_SUBSCRIBE_STATUS_COUNT).toString().equalsIgnoreCase(""))
      {
        this.ucDirectMailSubscribeStatusCount =
          Integer.valueOf(request.getParameter(COMConstants.UC_DIRECT_MAIL_SUBSCRIBE_STATUS_COUNT)).intValue();
      }
    }

    String status;
    String company;

    //fill the data in the array list created above
    for (int x=1; x <= this.ucDirectMailSubscribeStatusCount; x++)
    {
      status = null;
      company = null;

      //retrieve the direct mail statuse
      if(request.getParameter(COMConstants.UC_DIRECT_MAIL_SUBSCRIBE_STATUS + "_" + x)!=null)
        if(!request.getParameter(COMConstants.UC_DIRECT_MAIL_SUBSCRIBE_STATUS + "_" + x).equalsIgnoreCase(""))
          status = request.getParameter(COMConstants.UC_DIRECT_MAIL_SUBSCRIBE_STATUS + "_" + x);

      //retrieve the direct mail company id
      if(request.getParameter(COMConstants.UC_DIRECT_MAIL_COMPANY_ID + "_" + x)!=null)
        if(!request.getParameter(COMConstants.UC_DIRECT_MAIL_COMPANY_ID + "_" + x).equalsIgnoreCase(""))
          company = request.getParameter(COMConstants.UC_DIRECT_MAIL_COMPANY_ID + "_" + x);

      if (company != null)
        this.ucDirectMailSubscribeStatusHash.put(company, status);

    }


    /****************************************************************************************
     *  Retrieve parameters used to redirect the user to calling page
     ***************************************************************************************/
    //retrieve the master order number
    if(request.getParameter(COMConstants.UC_MASTER_ORDER_NUMBER)!=null)
      this.ucMasterOrderNumber = request.getParameter(COMConstants.UC_MASTER_ORDER_NUMBER);

    //retrieve the order guid
    if(request.getParameter(COMConstants.UC_ORDER_GUID)!=null)
      this.ucOrderGuid = request.getParameter(COMConstants.UC_ORDER_GUID);


    /****************************************************************************************
     *  Retrieve order/search specific parameters
     ***************************************************************************************/
    //retrieve the customer id
    if(request.getParameter(COMConstants.UC_CUSTOMER_ID)!=null)
    {
      this.ucCustomerId = request.getParameter(COMConstants.UC_CUSTOMER_ID);
      this.ucLongCustomerId = Long.valueOf(this.ucCustomerId).longValue();
    }


    /****************************************************************************************
     *  Retrieve Security Info
     ***************************************************************************************/
      this.sessionId   = request.getParameter(COMConstants.SEC_TOKEN);

  }


/*******************************************************************************************
  * getConfigInfo()
  ******************************************************************************************
  * Retrieve the info from the configuration file
  *
  * @param none
  * @return
  * @throws none
  */
  private void getConfigInfo() throws Exception
  {

    ConfigurationUtil cu = null;
    cu = ConfigurationUtil.getInstance();

    //get EMAIL_HISTORY_URL
    if(cu.getProperty(COMConstants.PROPERTY_FILE, COMConstants.CONS_EMAIL_HISTORY_URL) != null)
    {
      this.emailHistoryUrl = cu.getFrpGlobalParm(COMConstants.COM_CONTEXT, COMConstants.CONS_EMAIL_HISTORY_URL);
    }

    //get csr Id
    String security = (String) cu.getProperty(COMConstants.PROPERTY_FILE,COMConstants.SECURITY_IS_ON);
    boolean securityIsOn = security.equalsIgnoreCase("true") ? true : false;

   //delete this before going to production.  the following should only check on securityIsOn
    if(securityIsOn || this.sessionId != null)
    {
      SecurityManager sm = SecurityManager.getInstance();
      UserInfo ui = sm.getUserInfo(this.sessionId);
      this.csrId = ui.getUserID();
      if(sm.assertPermission(request.getParameter("context"), sessionId, COMConstants.VIP_CUSTOMER_UPDATE, COMConstants.YES))
	  {
  		 this.vipCustUpdatePermission = "Y";
      }
      else
      {
    	 this.vipCustUpdatePermission = "N";
      }
    }

  }


/*******************************************************************************************
  * populatePageData()
  ******************************************************************************************
  * Populate the page data with the remainder of the fields
  *
  * @param none
  * @return
  * @throws none
  */
  private void populatePageData()
  {
    //store the customer id passed in the page data
    this.pageData.put(COMConstants.PD_UC_CUSTOMER_ID,           this.ucCustomerId);

    //store the display page
    this.pageData.put(COMConstants.PD_DISPLAY_PAGE,             this.displayPage);

    //store the master order number
    this.pageData.put(COMConstants.PD_UC_MASTER_ORDER_NUMBER,   this.ucMasterOrderNumber);

    //store the order guid
    this.pageData.put(COMConstants.PD_UC_ORDER_GUID,            this.ucOrderGuid);

    //store the EMAIL HISTORY URL in the page data
    this.pageData.put(COMConstants.PD_EMAIL_HISTORY_URL,        this.emailHistoryUrl);
    
    //Store the permission required for updating the VIP customer
    this.pageData.put("vipCustUpdatePermission", this.vipCustUpdatePermission);


  }


/*******************************************************************************************
  * processCheckCustomerAccount()
  ******************************************************************************************
  * This method will process the action type of
  *
  * @param  none
  * @return none
  * @throws none
  *
  */
  private void processCheckCustomerAccount() throws Exception
  {
    boolean lockStillValid = true;
    boolean updateable     = true;

    //check if the lock is still valid
    lockStillValid = processRetrieveLock();

    //if the lock is still in place, process
    if(lockStillValid)
    {
        processExistingCustomerAccount();

        processNewCustomerAccount();

        boolean infoChanged;
        boolean recordChanges = false;
        infoChanged = processCheckForChanges(recordChanges);

        //if nothing changed, display an error message
        if (!infoChanged)
        {
          updateable = false;
          this.pageData.put(COMConstants.PD_MESSAGE_DISPLAY,  COMConstants.MSG_NOTHING_CHANGED);
        }
    }
    //else display an error message
    else
    {
      updateable = false;
    }

    this.pageData.put("updateable",                           updateable?"Y":"N");

  }


/*******************************************************************************************
  * processSaveCustomerChanges()
  ******************************************************************************************
  * This method will process the action type of
  *
  * @param  none
  * @return none
  * @throws none
  *
  */
  private void processSaveCustomerChanges() throws Exception
  {
    boolean lockStillValid = true;
    boolean updated        = true;

    //check if the lock is still valid
    lockStillValid = processRetrieveLock();

    //if the lock is still in place, process
    if(lockStillValid)
    {
      processExistingCustomerAccount();

      processNewCustomerAccount();

      //NOTE that this boolean will not come to use in this method.  Since processCheckForChanges
      //is used in more than one place, this indicator provides means to determine the next
      //step.
      //At this point (action=save), we have already determined (from action=check) that there
      //were some changes that the CSR made
      boolean infoChanged;

      //this will update the string buffer with the correct comments to be saved later
      boolean recordChanges = true;
      infoChanged = processCheckForChanges(recordChanges);

      updated = processUpdateCustomerAccount();

      if (updated)
      {
        //update database with the comments generated above.
        processCustomerComments();

        //release the lock
        processReleaseLock();
      }
      else
        this.pageData.put(COMConstants.PD_MESSAGE_DISPLAY,   COMConstants.MSG_UPDATE_UNSUCCESSFUL);

    }
    //else display an error message
    else
    {
      updated = false;
    }

    this.pageData.put("updated",                              updated?"Y":"N");

  }



/*******************************************************************************************
  * processRetrieveLock()
  ******************************************************************************************
  * This method will process the action type of
  *
  * @param  none
  * @return none
  * @throws none
  *
  */
  private boolean processRetrieveLock() throws Exception
  {
    boolean lockSuccessful = true;

    //Call retrieveLockInfo which will call the DAO to retrieve the results
    Document lockInfo = DOMUtil.getDocument();

    lockInfo = retrieveLockInfo();

    //get the lock obtained indicator.
    String sLockedObtained = DOMUtil.getNodeValue(lockInfo, "OUT_LOCK_OBTAINED");

    //if locked_obtained = yes, you have the lock (regardless if you just established a lock
    //or have had it for a while).  Else, somebody else has the record locked
    if (sLockedObtained.equalsIgnoreCase(COMConstants.CONS_NO))
    {
      lockSuccessful = false;

      //get the locked csr id
      String sCsrId = DOMUtil.getNodeValue(lockInfo, "OUT_LOCKED_CSR_ID");

      //set an output message
      String message = COMConstants.MSG_RECORD_C_LOCKED1 + sCsrId + COMConstants.MSG_RECORD_C_LOCKED2;
      this.pageData.put(COMConstants.PD_MESSAGE_DISPLAY,    message);

    }

    return lockSuccessful;
  }


/*******************************************************************************************
 * processExistingCustomerAccount() -
 *******************************************************************************************
  *
  * @throws none
  */
  private void processExistingCustomerAccount() throws Exception
  {
    //customerAccountInfoResults will store the customer info
    HashMap customerAccountInfoResults = new HashMap();

    //call retrieveCustomerAccountInfo which will call the DAO to retrieve the results
    //note that in this instance, the CachedResultSet will not be build in the retrieveCustomerAccountInfo.
    //Instead, it will be build in the buildCustomerVO
    customerAccountInfoResults = retrieveCustomerAccountInfo();

    //build the VO
    buildCustomerVO(customerAccountInfoResults);

  }


/*******************************************************************************************
 * processNewCustomerAccount() -
 *******************************************************************************************
  *
  * @throws none
  */
  private void processNewCustomerAccount() throws Exception
  {
    List customerPhoneVOList   = new ArrayList();
    List directMailVOList      = new ArrayList();

    this.newCustomer.setCustomerId(this.ucLongCustomerId);
    this.newCustomer.setFirstName(this.ucCustomerFirstName);
    this.newCustomer.setLastName(this.ucCustomerLastName);

    this.newCustomer.setBusinessName(this.ucBusinessName);
    this.newCustomer.setAddress1(this.ucCustomerAddress1);
    this.newCustomer.setAddress2(this.ucCustomerAddress2);
    this.newCustomer.setCity(this.ucCustomerCity);
    this.newCustomer.setState(this.ucCustomerState);
    this.newCustomer.setZipCode(this.ucCustomerZipCode);
    this.newCustomer.setCountry(this.ucCustomerCountry);
    this.newCustomer.setUpdatedBy(this.csrId);
    this.newCustomer.setCounty(this.ucCustomerCounty);
    this.newCustomer.setBirthday(this.ucCustomerBirthdayCal);
    this.newCustomer.setVipCustomer(this.ucVipCustomer);


    //create a phoneVO each for the evening and the daytime phone numbers
    //instantiate a phone vo
    CustomerPhoneVO dayPhoneVO = new CustomerPhoneVO();

    /** process the daytime phone number **/
    //and set all the fields within that vo
    dayPhoneVO.setCustomerId(this.ucLongCustomerId);
    //set the extension
    dayPhoneVO.setExtension(this.ucDaytimeCustomerExtension);
    //set the phone id
    long lDaytimePhoneId = (this.ucDaytimeCustomerPhoneId!=null?Long.valueOf(this.ucDaytimeCustomerPhoneId).longValue():0);
    dayPhoneVO.setPhoneId(lDaytimePhoneId);
    //set the phone number
    dayPhoneVO.setPhoneNumber(this.ucDaytimeCustomerPhoneNumber);
    //get the phone type
    dayPhoneVO.setPhoneType(COMConstants.CONS_DAY);
    dayPhoneVO.setCreatedBy(null);
    dayPhoneVO.setCreatedOn(null);
    dayPhoneVO.setUpdatedBy(this.csrId);
    dayPhoneVO.setUpdatedOn(null);

    //add the customer phone vo to the list
    customerPhoneVOList.add(dayPhoneVO);

    this.hashNewPhoneVO.put(dayPhoneVO.getPhoneType(),      dayPhoneVO);

    /*
    //also add it to a hashmap
    if (this.ucDaytimeCustomerPhoneNumber != null)
    {
      this.hashNewPhoneVO.put(dayPhoneVO.getPhoneType(),      dayPhoneVO);
    }
    */

    /** process the evening phone number **/
    //instantiate a phone vo
    CustomerPhoneVO eveningPhoneVO = new CustomerPhoneVO();
    eveningPhoneVO = new CustomerPhoneVO();
    //and set all the fields within that vo
    eveningPhoneVO.setCustomerId(this.ucLongCustomerId);
    //set the extension
    eveningPhoneVO.setExtension(this.ucEveningCustomerExtension);
    //set the phone id
    long lEveningPhoneId = (this.ucEveningCustomerPhoneId != null?Long.valueOf(this.ucEveningCustomerPhoneId).longValue():0);
    eveningPhoneVO.setPhoneId(lEveningPhoneId);
    //set the phone number
    eveningPhoneVO.setPhoneNumber(this.ucEveningCustomerPhoneNumber);
    //get the phone type
    eveningPhoneVO.setPhoneType(COMConstants.CONS_EVENING);
    eveningPhoneVO.setCreatedBy(null);
    eveningPhoneVO.setCreatedOn(null);
    eveningPhoneVO.setUpdatedBy(this.csrId);
    eveningPhoneVO.setUpdatedOn(null);

    //add the customer phone vo to the list
    customerPhoneVOList.add(eveningPhoneVO);

    this.hashNewPhoneVO.put(eveningPhoneVO.getPhoneType(),      eveningPhoneVO);
    /*
    //also add it to a hashmap
    if (this.ucEveningCustomerPhoneNumber != null)
    {
      this.hashNewPhoneVO.put(eveningPhoneVO.getPhoneType(),      eveningPhoneVO);
    }
    */


    /** process the direct mail **/

    //Get all the keys in the hashmap returned from the business object
    Set ks = ucDirectMailSubscribeStatusHash.keySet();
    Iterator iter = ks.iterator();
    String key;
    String status;

    //Iterate thru the hashmap returned from the business object using the keyset
    while(iter.hasNext())
    {
      key = iter.next().toString();

      //instantiate a direct mail vo
      DirectMailVO dVO = new DirectMailVO();

      //and set all the fields within that vo
      dVO.setCustomerId(this.ucLongCustomerId);

      dVO.setCompanyId(key);

      String dmStatus = (String)this.ucDirectMailSubscribeStatusHash.get(key);
      dVO.setStatus(dmStatus);

      dVO.setCreatedBy(null);
      dVO.setCreatedOn(null);
      dVO.setUpdatedBy(this.csrId);
      dVO.setUpdatedOn(null);

      //add the direct mail vo to the list
      directMailVOList.add(dVO);

      //also add it to a hashmap
      this.hashNewDMVO.put(key, dVO);
    }

    //add the lists created above to the newCustomerVO
    this.newCustomer.setCustomerPhoneVOList(customerPhoneVOList);
    this.newCustomer.setDirectMailVOList(directMailVOList);

  }



/*******************************************************************************************
 * processCheckForChanges() -
 *******************************************************************************************
  *
  * @throws none
  */
  private boolean processCheckForChanges(boolean recordChanges) throws Exception
  {
    boolean changesMade = false;

    if (recordChanges)
      this.sbCustomerComments = new StringBuffer();

    //first name
    if (  (this.origCustomer.getFirstName() != null && this.newCustomer.getFirstName() == null)       ||
          (this.origCustomer.getFirstName() == null && this.newCustomer.getFirstName() != null)       ||
          ( this.origCustomer.getFirstName() != null      &&
            this.newCustomer.getFirstName() != null       &&
            !this.origCustomer.getFirstName().equalsIgnoreCase(this.newCustomer.getFirstName())))
    {
      changesMade = true;
      this.updateCustomerRecord = true;
      if (recordChanges)
      {
        sbCustomerComments.append("The First Name has been changed from ");
        sbCustomerComments.append(this.origCustomer.getFirstName());
        sbCustomerComments.append(" to ");
        sbCustomerComments.append(this.newCustomer.getFirstName());
        sbCustomerComments.append(".<br>");
      }
    }

    //last name
    if (  (this.origCustomer.getLastName() != null && this.newCustomer.getLastName() == null)         ||
          (this.origCustomer.getLastName() == null && this.newCustomer.getLastName() != null)         ||
          ( this.origCustomer.getLastName() != null      &&
            this.newCustomer.getLastName() != null       &&
            !this.origCustomer.getLastName().equalsIgnoreCase(this.newCustomer.getLastName())))
    {
      changesMade = true;
      this.updateCustomerRecord = true;
      if (recordChanges)
      {
        sbCustomerComments.append("The Last Name has been changed from ");
        sbCustomerComments.append(this.origCustomer.getLastName());
        sbCustomerComments.append(" to " );
        sbCustomerComments.append(this.newCustomer.getLastName());
        sbCustomerComments.append(".<br>");
      }
    }

    //business name
    if (  (this.origCustomer.getBusinessName() != null && this.newCustomer.getBusinessName() == null)       ||
          (this.origCustomer.getBusinessName() == null && this.newCustomer.getBusinessName() != null)       ||
          ( this.origCustomer.getBusinessName() != null      &&
            this.newCustomer.getBusinessName() != null       &&
            !this.origCustomer.getBusinessName().equalsIgnoreCase(this.newCustomer.getBusinessName())))
    {
      changesMade = true;
      this.updateCustomerRecord = true;
      if (recordChanges)
      {
        sbCustomerComments.append("The Business Name has been changed from ");
        sbCustomerComments.append(this.origCustomer.getBusinessName());
        sbCustomerComments.append(" to " );
        sbCustomerComments.append(this.newCustomer.getBusinessName());
        sbCustomerComments.append(".<br>");
      }
    }

    //address 1
    if (  (this.origCustomer.getAddress1() != null && this.newCustomer.getAddress1() == null)       ||
          (this.origCustomer.getAddress1() == null && this.newCustomer.getAddress1() != null)       ||
          ( this.origCustomer.getAddress1() != null      &&
            this.newCustomer.getAddress1() != null       &&
            !this.origCustomer.getAddress1().equalsIgnoreCase(this.newCustomer.getAddress1())))
    {
      changesMade = true;
      this.updateCustomerRecord = true;
      if (recordChanges)
      {
        sbCustomerComments.append("The Address 1 has been changed from ");
        sbCustomerComments.append(this.origCustomer.getAddress1());
        sbCustomerComments.append(" to " );
        sbCustomerComments.append(this.newCustomer.getAddress1());
        sbCustomerComments.append(".<br>");
      }
    }

    //address 2
    if (  (this.origCustomer.getAddress2() != null && this.newCustomer.getAddress2() == null)       ||
          (this.origCustomer.getAddress2() == null && this.newCustomer.getAddress2() != null)       ||
          ( this.origCustomer.getAddress2() != null      &&
            this.newCustomer.getAddress2() != null       &&
            !this.origCustomer.getAddress2().equalsIgnoreCase(this.newCustomer.getAddress2())))
    {
      changesMade = true;
      this.updateCustomerRecord = true;
      if (recordChanges)
      {
        sbCustomerComments.append("The Address 2 has been changed from ");
        sbCustomerComments.append(this.origCustomer.getAddress2());
        sbCustomerComments.append(" to " );
        sbCustomerComments.append(this.newCustomer.getAddress2());
        sbCustomerComments.append(".<br>");
      }
    }

    //city
    if (  (this.origCustomer.getCity() != null && this.newCustomer.getCity() == null)       ||
          (this.origCustomer.getCity() == null && this.newCustomer.getCity() != null)       ||
          ( this.origCustomer.getCity() != null      &&
            this.newCustomer.getCity() != null       &&
            !this.origCustomer.getCity().equalsIgnoreCase(this.newCustomer.getCity())))
    {
      changesMade = true;
      this.updateCustomerRecord = true;
      if (recordChanges)
      {
        sbCustomerComments.append("The City has been changed from ");
        sbCustomerComments.append(this.origCustomer.getCity());
        sbCustomerComments.append(" to " );
        sbCustomerComments.append(this.newCustomer.getCity());
        sbCustomerComments.append(".<br>");
      }
    }

    //state
    if (  (this.origCustomer.getState() != null && this.newCustomer.getState() == null)       ||
          (this.origCustomer.getState() == null && this.newCustomer.getState() != null)       ||
          ( this.origCustomer.getState() != null      &&
            this.newCustomer.getState() != null       &&
            !this.origCustomer.getState().equalsIgnoreCase(this.newCustomer.getState())))
    {
      changesMade = true;
      this.updateCustomerRecord = true;
      if (recordChanges)
      {
        sbCustomerComments.append("The State has been changed from ");
        sbCustomerComments.append(this.origCustomer.getState());
        sbCustomerComments.append(" to " );
        sbCustomerComments.append(this.newCustomer.getState());
        sbCustomerComments.append(".<br>");
      }
    }

    //zip
    if (  (this.origCustomer.getZipCode() != null && this.newCustomer.getZipCode() == null)       ||
          (this.origCustomer.getZipCode() == null && this.newCustomer.getZipCode() != null)       ||
          ( this.origCustomer.getZipCode() != null      &&
            this.newCustomer.getZipCode() != null       &&
            !this.origCustomer.getZipCode().equalsIgnoreCase(this.newCustomer.getZipCode())))
    {
      changesMade = true;
      this.updateCustomerRecord = true;
      if (recordChanges)
      {
        sbCustomerComments.append("The Zip Code has been changed from ");
        sbCustomerComments.append(this.origCustomer.getZipCode());
        sbCustomerComments.append(" to " );
        sbCustomerComments.append(this.newCustomer.getZipCode());
        sbCustomerComments.append(".<br>");
      }
    }

    //country
    if (  (this.origCustomer.getCountry() != null && this.newCustomer.getCountry() == null)       ||
          (this.origCustomer.getCountry() == null && this.newCustomer.getCountry() != null)       ||
          ( this.origCustomer.getCountry() != null      &&
            this.newCustomer.getCountry() != null       &&
            !this.origCustomer.getCountry().equalsIgnoreCase(this.newCustomer.getCountry())))
    {
      changesMade = true;
      this.updateCustomerRecord = true;
      if (recordChanges)
      {
        sbCustomerComments.append("The Country has been changed from ");
        sbCustomerComments.append(this.origCustomer.getCountry());
        sbCustomerComments.append(" to " );
        sbCustomerComments.append(this.newCustomer.getCountry());
        sbCustomerComments.append(".<br>");
      }
    }
    
  //vip_customer
    if (  !(this.origCustomer.isVipCustomer()==this.newCustomer.isVipCustomer()) )
    {
      changesMade = true;
      this.updateCustomerRecord = true;
      if (recordChanges)
      {
        sbCustomerComments.append("The VIP flag has been changed from ");
        sbCustomerComments.append(this.origCustomer.isVipCustomer());
        sbCustomerComments.append(" to " );
        sbCustomerComments.append(this.newCustomer.isVipCustomer());
        sbCustomerComments.append(".<br>");
      }
    }

    /** process the daytime/evening phone numbers, type and extensions **/
    //retrieve the keyset
    Set ksPhone = this.hashNewPhoneVO.keySet();
    Iterator iterPhone = ksPhone.iterator();
    String keyPhone = null;
    CustomerPhoneVO origPhoneVO;
    CustomerPhoneVO newPhoneVO;

    //Iterate thru the keyset
    while(iterPhone.hasNext())
    {
      boolean addToChangedHash = false;
      //get the key
      keyPhone = iterPhone.next().toString();

      //retrieve the Original Phone VO
      origPhoneVO = (CustomerPhoneVO) this.hashOrigPhoneVO.get(keyPhone);

      //retrieve the New Phone VO
      newPhoneVO = (CustomerPhoneVO) this.hashNewPhoneVO.get(keyPhone);


      if(origPhoneVO != null && newPhoneVO != null)
      {
        //check the phone number
        if (  (origPhoneVO.getPhoneNumber() != null && newPhoneVO.getPhoneNumber() == null)           ||
              (origPhoneVO.getPhoneNumber() == null && newPhoneVO.getPhoneNumber() != null)           ||
              ( origPhoneVO.getPhoneNumber() != null      &&
                newPhoneVO.getPhoneNumber() != null       &&
                !origPhoneVO.getPhoneNumber().equalsIgnoreCase(newPhoneVO.getPhoneNumber())))
        {
          addToChangedHash = true;
          changesMade = true;
          this.updatePhoneRecord = true;
          if (recordChanges)
          {
            if( origPhoneVO.getPhoneType() != null &&
                origPhoneVO.getPhoneType().equalsIgnoreCase(COMConstants.CONS_DAY))
            {
              sbCustomerComments.append("The Phone 1 Number has been changed from ");
            }
            else
            {
              sbCustomerComments.append("The Phone 2 Number has been changed from ");
            }
            sbCustomerComments.append(origPhoneVO.getPhoneNumber());
            sbCustomerComments.append(" to " );
            sbCustomerComments.append(newPhoneVO.getPhoneNumber());
            sbCustomerComments.append(".<br>");
          }
        }

        //check the extension
        if (  (origPhoneVO.getExtension() != null && newPhoneVO.getExtension() == null)           ||
              (origPhoneVO.getExtension() == null && newPhoneVO.getExtension() != null)           ||
              ( origPhoneVO.getExtension() != null      &&
                newPhoneVO.getExtension() != null       &&
                !origPhoneVO.getExtension().equalsIgnoreCase(newPhoneVO.getExtension())))
        {
          addToChangedHash = true;
          changesMade = true;
          this.updatePhoneRecord = true;
          if (recordChanges)
          {
            if( origPhoneVO.getPhoneType() != null &&
                origPhoneVO.getPhoneType().equalsIgnoreCase(COMConstants.CONS_DAY))
            {
              sbCustomerComments.append("The Phone 1 Extension has been changed from ");
            }
            else
            {
              sbCustomerComments.append("The Phone 2 Extension has been changed from ");
            }
            sbCustomerComments.append(origPhoneVO.getExtension());
            sbCustomerComments.append(" to " );
            sbCustomerComments.append(newPhoneVO.getExtension());
            sbCustomerComments.append(".<br>");
          }
        }
      }
      else if(origPhoneVO == null && newPhoneVO != null)
      {
        //check the phone number
        if (newPhoneVO.getPhoneNumber() != null)
        {
          addToChangedHash = true;
          changesMade = true;
          this.updatePhoneRecord = true;
          if (recordChanges)
          {
            if( newPhoneVO.getPhoneType() != null &&
                newPhoneVO.getPhoneType().equalsIgnoreCase(COMConstants.CONS_DAY))
            {
              sbCustomerComments.append("The Phone 1 Number has been changed from null to ");
            }
            else
            {
              sbCustomerComments.append("The Phone 2 Number has been changed from null to ");
            }
            sbCustomerComments.append(newPhoneVO.getPhoneNumber());
            sbCustomerComments.append(".<br>");
          }
        }

        //check the extension
        if (newPhoneVO.getExtension() != null)
        {
          addToChangedHash = true;
          changesMade = true;
          this.updatePhoneRecord = true;
          if (recordChanges)
          {
            if( newPhoneVO.getPhoneType() != null &&
                newPhoneVO.getPhoneType().equalsIgnoreCase(COMConstants.CONS_DAY))
            {
              sbCustomerComments.append("The Phone 1 Extension has been changed from null to ");
            }
            else
            {
              sbCustomerComments.append("The Phone 2 Extension has been changed from null to ");
            }
            sbCustomerComments.append(newPhoneVO.getExtension());
            sbCustomerComments.append(".<br>");
          }
        }
      }

      if(addToChangedHash)
        this.hashChangedPhoneVO.put(keyPhone, newPhoneVO);

    } //end-while



    /** process the Direct Mail status **/

    //note that since the comments should be sorted in the same order as the stored proc passes to XSL,
    //we will first get the info from the array list, and then use that info to comapare the info.

    //retrieve the array list
    List directMailVOList = this.origCustomer.getDirectMailVOList();

    //original and new Direct Mail VOs
    DirectMailVO origDMVO;
    DirectMailVO newDMVO;

    //size of the array
    int totalDM = directMailVOList.size();

    //Iterate thru the array
    for (int x = 0; x < totalDM; x++)
    {
      DirectMailVO tempDMVO = (DirectMailVO) directMailVOList.get(x);
      String hashKey = tempDMVO.getCompanyId();

      //retrieve the Original Direct Mail VO
      origDMVO = (DirectMailVO) this.hashOrigDMVO.get(hashKey);

      //retrieve the New Direct Mail VO
      newDMVO = (DirectMailVO) this.hashNewDMVO.get(hashKey);

      if (origDMVO != null && newDMVO != null)
      {
        //check the phone number
        if (  (origDMVO.getStatus() != null && newDMVO.getStatus() == null)           ||
              (origDMVO.getStatus() == null && newDMVO.getStatus() != null)           ||
              ( origDMVO.getStatus() != null      &&
                newDMVO.getStatus() != null       &&
                !origDMVO.getStatus().equalsIgnoreCase(newDMVO.getStatus())))
        {
          changesMade = true;
          this.updateDirectMailRecord = true;
          if (recordChanges)
          {
            sbCustomerComments.append("Catalog Status");
            //note that the company name is only available from the Original VO that was built
            //using the database.  The XSL is not sending the company name for the New VO to have
            //that information.
            sbCustomerComments.append(" changed from ");
            sbCustomerComments.append( (origDMVO.getStatus() != null) ? origDMVO.getStatus():"Never opted in");
            sbCustomerComments.append(" to " );
            sbCustomerComments.append(newDMVO.getStatus());
            sbCustomerComments.append(".<br>");
          }

          this.hashChangedDMVO.put(hashKey, newDMVO);
        }
      }
    }//end for

    return changesMade;

  }


/*******************************************************************************************
  * buildCustomerVO()
  ******************************************************************************************
  *
  * @param  none
  * @return none
  * @throws none
  */
  private void buildCustomerVO(HashMap customerAccountInfo) throws Exception
  {

    List customerPhoneVOList   = new ArrayList();
    List emailVOList           = new ArrayList();
    List membershipVOList      = new ArrayList();
    List directMailVOList      = new ArrayList();
    int emailCount = 0;



    Set ks1 = customerAccountInfo.keySet();
    Iterator iter = ks1.iterator();
    String key = null;

    //Create a CachedResultSet object using the cursor name that was passed.
    CachedResultSet rs = null;

    //Iterate thru the keyset
    while(iter.hasNext())
    {
      //get the key
      key = iter.next().toString();

      if (key.equalsIgnoreCase("OUT_CUSTOMER_CUR"))
      {
        rs = (CachedResultSet) customerAccountInfo.get(key);
        rs.next();

        //build the VO
        this.origCustomer.setCustomerId(this.ucLongCustomerId);
        //this.origCustomer.setConcatId(rs.getObject("concat_id") != null
        //            ?(String)rs.getObject("concat_id")                                             :null);
        this.origCustomer.setFirstName(rs.getObject("customer_first_name") != null
                    ?(String)rs.getObject("customer_first_name")                                   :null);
        this.origCustomer.setLastName(rs.getObject("customer_last_name") != null
                    ?(String)rs.getObject("customer_last_name")                                    :null);
        this.origCustomer.setBusinessName(rs.getObject("business_name") != null
                    ?(String)rs.getObject("business_name")                                         :null);
        this.origCustomer.setAddress1(rs.getObject("customer_address_1") != null
                    ?(String)rs.getObject("customer_address_1")                                    :null);
        this.origCustomer.setAddress2(rs.getObject("customer_address_2") != null
                    ?(String)rs.getObject("customer_address_2")                                    :null);
        this.origCustomer.setCity(rs.getObject("customer_city") != null
                    ?(String)rs.getObject("customer_city")                                         :null);
        this.origCustomer.setState(rs.getObject("customer_state") != null
                    ?(String)rs.getObject("customer_state")                                        :null);
        this.origCustomer.setZipCode(rs.getObject("customer_zip_code") != null
                    ?(String)rs.getObject("customer_zip_code")                                     :null);
        this.origCustomer.setCountry(rs.getObject("customer_country") != null
                    ?(String)rs.getObject("customer_country")                                      :null);
        this.origCustomer.setBuyerIndicator(rs.getObject("buyer_indicator") != null
                    ?rs.getObject("buyer_indicator").toString().charAt(0)                     :'N');
        this.origCustomer.setRecipientIndicator(rs.getObject("recipient_indicator") != null
                    ?rs.getObject("recipient_indicator").toString().charAt(0)                 :'N');
        this.origCustomer.setVipCustomer((rs.getString("vip_customer")!= null ? FieldUtils.convertStringToBoolean(rs.getString("vip_customer")) : false) );
        //retrieve/format the createdOn date
        Date dCreatedOn = null;
        if (rs.getObject("customer_created_on") != null)
            dCreatedOn = (Date) rs.getObject("customer_created_on");

        if (dCreatedOn != null)
        {
          Calendar cCreatedDate = Calendar.getInstance();
          cCreatedDate.setTime(dCreatedOn);

          //set the date
          this.origCustomer.setCreatedOn(cCreatedDate);
        }
        else
          this.origCustomer.setCreatedOn(null);


          /*if (rs.getObject("customer_created_on") != null)
          {
            String sCreatedOn = rs.getObject("customer_created_on").toString();
            //Define the format
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
            //Create a Calendar Object
            Calendar cCreatedOn = Calendar.getInstance();
            //Set the Calendar Object using the date retrieved in sCreatedOn
            cCreatedOn.setTime(sdf.parse(sCreatedOn));
            //set the VO with the Calendar object
            this.origCustomer.setCreatedOn(cCreatedOn);
          }*/

        this.origCustomer.setAddressType(null); //not getting returned from the stored proc
        this.origCustomer.setPreferredCustomer('N'); //not getting returned from the stored proc
        this.origCustomer.setOrigin(null); //not getting returned from the stored proc
        this.origCustomer.setCounty(null); //not getting returned from the stored proc
        this.origCustomer.setBirthday(null); //not getting returned from the stored proc

        this.origCustomer.setCreatedBy(null); //users cannot update
        this.origCustomer.setUpdatedOn(null); //users cannot update
        this.origCustomer.setFirstOrderDate(null); //users cannot update

        //set the user id
        this.origCustomer.setUpdatedBy(this.csrId);

      }
      else if (key.equalsIgnoreCase("OUT_PHONE_CUR"))
      {
        rs = (CachedResultSet) customerAccountInfo.get(key);

        while(rs.next())
        {
          CustomerPhoneVO phoneVO = new CustomerPhoneVO();

          phoneVO.setCustomerId(this.ucLongCustomerId);

          //retrieve the phone type
          String phoneType = null;
          if (rs.getObject("customer_phone_type") != null)
            phoneType = (String)rs.getObject("customer_phone_type");
          phoneVO.setPhoneType(phoneType);

          //retrieve the phone id
          String      phoneId = null;
          BigDecimal  bPhoneId;
          long        lPhoneId = 0;
          if (rs.getObject("customer_phone_id") != null)
          {
            bPhoneId = (BigDecimal)rs.getObject("customer_phone_id");
            phoneId = bPhoneId.toString();
            lPhoneId = Long.valueOf(phoneId).longValue();
          }
          phoneVO.setPhoneId(lPhoneId);


          //retrieve the phone number
          String phoneNumber = null;
          if (rs.getObject("customer_phone_number") != null)
            phoneNumber = (String) rs.getObject("customer_phone_number");
          phoneVO.setPhoneNumber(phoneNumber);

          //retrieve the extension
          String extension = null;
          if (rs.getObject("customer_extension") != null)
            extension = (String) rs.getObject("customer_extension");
          phoneVO.setExtension(extension);
          
          //retrieve the phone number type
          String phoneNumberType = null;
          if (rs.getObject("cus_phone_number_type") != null)
        	  phoneNumberType = (String) rs.getObject("cus_phone_number_type");
          phoneVO.setExtension(phoneNumberType);
          
          //retrieve the sms opt in
          String smsOptIn = null;
          if (rs.getObject("customer_sms_opt_in") != null)
        	  smsOptIn = (String) rs.getObject("customer_sms_opt_in");
          phoneVO.setExtension(smsOptIn);

          phoneVO.setUpdatedOn(null); //users cannot update
          phoneVO.setCreatedOn(null); //users cannot update
          phoneVO.setCreatedBy(null); //users cannot update

          //set the user id
          phoneVO.setUpdatedBy(this.csrId);

          //add the customer phone vo to the list
          customerPhoneVOList.add(phoneVO);

          //also add it in a hashmap
          if (phoneId != null)
          {
            this.hashOrigPhoneVO.put(phoneType,     phoneVO);
          }
        }
      }
      else if (key.equalsIgnoreCase("OUT_MEMBERSHIP_CUR"))
      {
        rs = (CachedResultSet) customerAccountInfo.get(key);

        while(rs.next())
        {
          MembershipVO mVO = new MembershipVO();

          mVO.setCustomerId(this.ucLongCustomerId);
          mVO.setMembershipId(rs.getObject("membership_id") != null
                    ?Long.valueOf(rs.getObject("membership_id").toString()).longValue()            :0);
          mVO.setMembershipNumber(rs.getObject("membership_number") != null
                    ?(String)rs.getObject("membership_number")                                     :null);
          mVO.setMembershipType(rs.getObject("membership_type") != null
                    ?(String)rs.getObject("membership_type")                                       :null);
          mVO.setFirstName(rs.getObject("membership_first_name") != null
                    ?(String)rs.getObject("membership_first_name")                                 :null);
          mVO.setLastName(rs.getObject("membership_last_name") != null
                    ?(String)rs.getObject("membership_last_name")                                  :null);

          mVO.setCreatedOn(null); //users cannot update
          mVO.setCreatedBy(null); //users cannot update
          mVO.setUpdatedOn(null); //users cannot update
          mVO.setUpdatedBy(this.csrId); //users cannot update

          //add the membership vo to the list
          membershipVOList.add(mVO);
        }
      }
      else if (key.equalsIgnoreCase("OUT_HOLD_CUR"))
      {
      }
      else if (key.equalsIgnoreCase("OUT_VIEWING_CUR"))
      {
      }
      else if (key.equalsIgnoreCase("OUT_DIRECT_MAIL_EMAIL_CUR"))
      {
        rs = (CachedResultSet) customerAccountInfo.get(key);

        while(rs.next())
        {
          /**Create the Direct Mail VO**/
          DirectMailVO dVO = new DirectMailVO();

          dVO.setCustomerId(this.ucLongCustomerId);

          //retrieve the company id
          String companyId = null;
          if (rs.getObject("company_id") != null)
            companyId = (String) rs.getObject("company_id");
          dVO.setCompanyId(companyId);

          //retrieve the direct mail status
          String dmStatus = null;
          if (rs.getObject("direct_mail_subscribe_status") != null)
            dmStatus = (String) rs.getObject("direct_mail_subscribe_status");
          dVO.setStatus(dmStatus);

          //retrieve the company name
          String companyName = null;
          if (rs.getObject("company_name") != null)
            companyName = (String) rs.getObject("company_name");
          dVO.setCompanyName(companyName);

          dVO.setCreatedOn(null); //users cannot update
          dVO.setCreatedBy(null); //users cannot update
          dVO.setUpdatedOn(null); //users cannot update

          //set the user id
          dVO.setUpdatedBy(this.csrId);

          //add the direct mail vo to the list
          directMailVOList.add(dVO);

          //also add it in a hashmap
          this.hashOrigDMVO.put(companyId, dVO);

          /**Create the Email VO**/
          EmailVO eVO = new EmailVO();

          eVO.setCustomerId(this.ucLongCustomerId);
          eVO.setEmailId(rs.getObject("email_id") != null
                    ?Long.valueOf(rs.getObject("email_id").toString()).longValue()                 :0);
          eVO.setEmailAddress(rs.getObject("email_address") != null
                    ?(String)rs.getObject("email_address")                                         :null);
          eVO.setActiveIndicator(rs.getObject("email_active_indicator") != null
                    ?(String)rs.getObject("email_active_indicator")                                :null);
          eVO.setSubscribeStatus(rs.getObject("email_subscribe_status") != null
                    ?(String)rs.getObject("email_subscribe_status")                                :null);

          eVO.setSubscribeDate(null); //not getting returned from the stored proc
          eVO.setCompanyId(null); //not getting returned from the stored proc
          eVO.setCreatedOn(null); //users cannot update
          eVO.setCreatedBy(null); //users cannot update
          eVO.setUpdatedOn(null); //users cannot update

          //set the user id
          eVO.setUpdatedBy(this.csrId);

          //add the email vo to the list
          emailVOList.add(eVO);

          if (eVO.getEmailId() != 0)
            emailCount++;

        }
      }
      else if (key.equalsIgnoreCase("OUT_MORE_EMAIL_INDICATOR"))
      {
        String moreEmailInd = (String)customerAccountInfo.get(key);
        this.pageData.put(COMConstants.PD_MORE_EMAIL_INDICATOR,       moreEmailInd);
      }
      else if (key.equalsIgnoreCase("OUT_LOCKED_INDICATOR"))
      {
        String lockedInd = (String)customerAccountInfo.get(key);
        this.pageData.put(COMConstants.PD_LOCKED_INDICATOR,           lockedInd);
      }

    } //end-while(iter.hasNext())

    //if at least one email was found, send a email found = y
    this.pageData.put(COMConstants.PD_UC_EMAIL_FOUND,     emailCount>0?COMConstants.CONS_YES:COMConstants.CONS_NO);

    //add the lists created above to the origCustomerVO
    this.origCustomer.setCustomerPhoneVOList(customerPhoneVOList);
    this.origCustomer.setEmailVOList(emailVOList);
    this.origCustomer.setMembershipVOList(membershipVOList);
    this.origCustomer.setDirectMailVOList(directMailVOList);


  }


/*******************************************************************************************
 * processUpdateCustomerAccount() -
 *******************************************************************************************
  *
  * @throws none
  */
  private boolean processUpdateCustomerAccount() throws Exception
  {
    //flag that is used to check if the update was successful, and then to be passed back to the
    //calling method
    boolean updateSuccessful = true;

    //Strings to hold the output parameters
    String outStatus;
    String outMessage;

    //hashmaps to hold the output resultsets
    HashMap updateCustomerResults;
    HashMap updatePhoneResults;
    HashMap updateDirectMailResults;

    //Define the VOs
    CustomerPhoneVO phoneVO;
    DirectMailVO dmVO;

    //update the customer record
    if (this.updateCustomerRecord)
    {
      updateCustomerResults = (HashMap) updateCustomer(this.newCustomer);
      outStatus   = (String) updateCustomerResults.get("OUT_STATUS");
      outMessage  = (String) updateCustomerResults.get("OUT_MESSAGE");

      updateSuccessful = true;
      if (outStatus != null)
        updateSuccessful = outStatus.equalsIgnoreCase(COMConstants.CONS_YES)?true:false;

      if (!updateSuccessful)
        this.pageData.put(COMConstants.PD_MESSAGE_DISPLAY,      COMConstants.MSG_UPDATE_UNSUCCESSFUL);
    }


    //loop thru and update all the customer phone records
    if (updateSuccessful && this.updatePhoneRecord)
    {
      //retrieve all the VOs
      //retrieve the keyset
      Set ksPhone = this.hashChangedPhoneVO.keySet();
      Iterator iterPhone = ksPhone.iterator();
      String keyPhone = null;

      //Iterate thru the keyset
      while(iterPhone.hasNext())
      {
        //get the key
        keyPhone = iterPhone.next().toString();

        //get the phoneVO
        phoneVO = (CustomerPhoneVO) this.hashChangedPhoneVO.get(keyPhone);
        updatePhoneResults = (HashMap) updatePhone(phoneVO);
        outStatus   = (String) updatePhoneResults.get("OUT_STATUS");
        outMessage  = (String) updatePhoneResults.get("OUT_MESSAGE");

        updateSuccessful = true;
        if (outStatus != null)
          updateSuccessful = outStatus.equalsIgnoreCase(COMConstants.CONS_YES)?true:false;

        if (!updateSuccessful)
          this.pageData.put(COMConstants.PD_MESSAGE_DISPLAY,      COMConstants.MSG_UPDATE_UNSUCCESSFUL);
      }
    }

    //loop thru and update all the customer mail records
    if (updateSuccessful && this.updateDirectMailRecord)
    {
      //retrieve all the VOs
      //retrieve the keyset
      Set ksDM = this.hashChangedDMVO.keySet();
      Iterator iterDM = ksDM.iterator();
      String keyDM = null;

      //Iterate thru the keyset
      while(iterDM.hasNext())
      {
        //get the key
        keyDM = iterDM.next().toString();

        //get the phoneVO
        dmVO = (DirectMailVO) this.hashChangedDMVO.get(keyDM);
        updateDirectMailResults = (HashMap) updateDirectMail(dmVO);
        outStatus   = (String) updateDirectMailResults.get("OUT_STATUS");
        outMessage  = (String) updateDirectMailResults.get("OUT_MESSAGE");

        updateSuccessful = true;
        if (outStatus != null)
          updateSuccessful = outStatus.equalsIgnoreCase(COMConstants.CONS_YES)?true:false;

        if (!updateSuccessful)
          this.pageData.put(COMConstants.PD_MESSAGE_DISPLAY,      COMConstants.MSG_UPDATE_UNSUCCESSFUL);
      }
    }

    return updateSuccessful;
  }



/*******************************************************************************************
 * processCustomerComments() -
 *******************************************************************************************
  *
  * @throws none
  */
  private void processCustomerComments() throws Exception
  {
    String customerComments = this.sbCustomerComments.toString();

    //instantiate the customer dao
    CommentDAO commentDAO = new CommentDAO(this.con);

    //instantiate the comments vo
    CommentsVO commentsVO = new CommentsVO();

    //initialize the comments vo
    commentsVO.setCustomerId(this.ucCustomerId);
    commentsVO.setComment(customerComments);
    commentsVO.setCommentType(COMConstants.CONS_ENTITY_TYPE_CUSTOMER_TITLE_CASE);
    commentsVO.setCreatedBy(this.csrId);

    commentsVO.setOrderGuid(null);
    commentsVO.setOrderDetailId(null);
    commentsVO.setCommentOrigin(null);
    commentsVO.setReason(null);
    commentsVO.setDnisId(null);
    commentsVO.setCommentId(null);
    commentsVO.setCreatedOn(null);
    commentsVO.setUpdatedBy(this.csrId);
    commentsVO.setUpdatedOn(null);

    commentDAO.insertComment(commentsVO);

  }



/*******************************************************************************************
 * processReleaseLock() -
 *******************************************************************************************
  *
  * @throws none
  */
  private void processReleaseLock() throws Exception
  {
    //release the lock
    releaseLock();
  }




/*******************************************************************************************
 * processStates() -
 *******************************************************************************************
  *
  * @throws none
  */
  private void processStates() throws Exception
  {
    //statesXML will store the states info
    Document statesXML = DOMUtil.getDocument();

    //Instantiate the BasePageBuilder
    BasePageBuilder builder = new BasePageBuilder();

    //and retrieve the states info in an xml document
    statesXML = builder.retrieveStates(this.con);
    this.hashXML.put(COMConstants.HK_STATES,  statesXML);


  }


/*******************************************************************************************
 * processCountries() -
 *******************************************************************************************
  *
  * @throws none
  */
  private void processCountries() throws Exception
  {
    //statesXML will store the states info
    Document countriesXML = DOMUtil.getDocument();

    //Instantiate the BasePageBuilder
    BasePageBuilder builder = new BasePageBuilder();

    //and retrieve the states info in an xml document
    countriesXML = builder.retrieveCountries(this.con);
    this.hashXML.put(COMConstants.HK_COUNTRIES,  countriesXML);



  }


//******************************************************************************************
//                              ** END OF PROCESSES **
//******************************************************************************************


/*******************************************************************************************
 * retrieveLockInfo() - locks the record for Payment Refunds
 *******************************************************************************************
  *
  * @throws none
  */
  private Document retrieveLockInfo() throws Exception
  {
    //Instantiate RefundDAO
    LockDAO lockDAO = new LockDAO(this.con);

    //hashmap that will contain the output from the stored proc
    Document lockInfo = DOMUtil.getDocument();


  //Call retrieveLockXML method in the DAO
  //lockInfo = lockDAO.retrieveLockXML(sessionId,       csrId,        entityId [aka lockId],  entityType [aka applicationName]);
    lockInfo = lockDAO.retrieveLockXML(this.sessionId,  this.csrId,   this.ucCustomerId,      COMConstants.CONS_ENTITY_TYPE_CUSTOMER);

    return lockInfo;

  }



/*******************************************************************************************
 * releaseLock() - if the lock exists, and CSR cancels an action, release the Payment lock
 *******************************************************************************************
  *
  * @throws none
  */
  private void releaseLock() throws Exception
  {
    //Instantiate RefundDAO
    LockDAO lockDAO = new LockDAO(this.con);

    //Call releaseLock method in the DAO
 //lockInfo.releaseLock(sessionId,        csrId,        entityId [aka lockId],  entityType [aka applicationName]);
    lockDAO.releaseLock(this.sessionId,   this.csrId,   this.ucCustomerId,        COMConstants.CONS_ENTITY_TYPE_CUSTOMER);


  }


/*******************************************************************************************
 * retrieveCustomerAccountInfo()
 *******************************************************************************************
  * This method is used to call the CustomerDAO.getCustomerAcct
  *
  * @param1 String - customerId
  * @param2 long - start position
  * @param3 long - end position
  * @return HashMap - map containing output from the stored procedure
  * @throws none
  */
  private HashMap retrieveCustomerAccountInfo() throws Exception
  {
    //Instantiate CustomerDAO
    CustomerDAO customerDAO = new CustomerDAO(this.con);

    //hashmap that will contain the output from the stored proc
    HashMap customerAccountInfo = new HashMap();

    //Call getCustomerAcct method in the DAO
    customerAccountInfo = customerDAO.getCustomerAcctOnly(this.ucCustomerId, this.sessionId, this.csrId);

    return customerAccountInfo;
  }


/*******************************************************************************************
 * updateCustomer()
 *******************************************************************************************
  * This method is used to call the CustomerDAO.getStates
  *
  * @throws none
  */
  private HashMap updateCustomer(CustomerVO cVO) throws Exception
  {
    HashMap updateCustomerHash = new HashMap();

    CustomerDAO customerDAO = new CustomerDAO(this.con);
    updateCustomerHash = (HashMap) customerDAO.updateCustomer(cVO);

    return updateCustomerHash;

  }



/*******************************************************************************************
 * updatePhone()
 *******************************************************************************************
  * This method is used to call the CustomerDAO.getStates
  *
  * @throws none
  */
  private HashMap updatePhone(CustomerPhoneVO phoneVO) throws Exception
  {
    HashMap updatePhoneHash = new HashMap();

    CustomerDAO customerDAO = new CustomerDAO(this.con);
    updatePhoneHash = (HashMap) customerDAO.updatePhone(phoneVO);

    return updatePhoneHash;

  }

/*******************************************************************************************
 * updateDirectMail()
 *******************************************************************************************
  * This method is used to call the CustomerDAO.getStates
  *
  * @throws none
  */
  private HashMap updateDirectMail(DirectMailVO dmVO) throws Exception
  {
    HashMap updateDirectMailHash = new HashMap();

    CustomerDAO customerDAO = new CustomerDAO(this.con);
    updateDirectMailHash = (HashMap) customerDAO.updateDirectMail(dmVO);

    return updateDirectMailHash;

  }


}
