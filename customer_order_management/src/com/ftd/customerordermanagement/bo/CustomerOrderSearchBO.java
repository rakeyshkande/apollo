package com.ftd.customerordermanagement.bo;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.dao.CustomerDAO;
import com.ftd.customerordermanagement.dao.LockDAO;
import com.ftd.customerordermanagement.dao.OrderDAO;
import com.ftd.customerordermanagement.dao.RefundDAO;
import com.ftd.customerordermanagement.dao.ServicesProgramDAO;
import com.ftd.customerordermanagement.dao.TagDAO;
import com.ftd.customerordermanagement.dao.ViewDAO;
import com.ftd.customerordermanagement.util.BasePageBuilder;
import com.ftd.customerordermanagement.util.DatabaseMaintainUtil;
import com.ftd.customerordermanagement.util.TimerFilter;
import com.ftd.customerordermanagement.vo.CustomerMemberShipVO;
import com.ftd.customerordermanagement.vo.CustomerMembershipDetail;
import com.ftd.customerordermanagement.vo.OrderDetailVO;
import com.ftd.customerordermanagement.vo.PrivacyPolicyOptionVO;
import com.ftd.decisionresult.filter.DecisionResultDataFilter;
import com.ftd.ftdutilities.FTDCAMSUtils;
import com.ftd.ftdutilities.FTDCommonUtils;
import com.ftd.mo.bo.PhoenixOrderBO;
import com.ftd.mo.dao.UpdateOrderDAO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.PartnerUtility;
import com.ftd.osp.utilities.PreferredPartnerUtility;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.CacheUtil;
import com.ftd.osp.utilities.cacheMgr.constant.CacheMgrConstants;
import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.cacheMgr.handlers.GlobalParmHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.SourceMasterHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.AccountProgramMasterVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.PartnerVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.SourceMasterVO;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.XMLFormat;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.order.MercentOrderPrefixes;
import com.ftd.osp.utilities.order.vo.ItemTaxVO;
import com.ftd.osp.utilities.order.vo.TaxVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;
import com.ftd.osp.utilities.vo.CustomerFSHistory;
import com.ftd.osp.utilities.vo.CustomerFSMDetails;
import com.ftd.osp.utilities.vo.MembershipDetailsVO;
import com.ftd.osp.utilities.vo.PartnerMappingVO;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.JAXPException;
import com.ftd.osp.utilities.xml.JAXPUtil;
import com.ftd.security.SecurityManager;
import com.ftd.security.cache.vo.UserInfo;


public class CustomerOrderSearchBO
{
/*******************************************************************************************
 * Class level variables
 *******************************************************************************************/
  private HttpServletRequest  request         = null;
  private Connection          con             = null;
  private static Logger logger  = new Logger("com.ftd.customerordermanagement.bo.CustomerOrderSearchBO");
  private static String SOURCE_CODE_XPATH     = "CSCI_CARTS/CSCI_CART/source_code/text()";
  private static final String KEY_PREFERRED_SOURCE = "KEY_SOURCE_PARTNER";

  
  private String    action                    = null;
  private boolean   bypassPrivacyPolicyVerificationByUserRole = false;
  private boolean   callLogBypassPrivacyPolicyVerification = false;
  private String    csrId                     = null;
  private long      currentCAIPagePosition    = 0;
  private long      currentCEAPagePosition    = 0;
  private long      currentCOSPagePosition    = 0;
  private long      currentCSCIPagePosition   = 0;
  private String    customerId                = null;
  private String    customerValidationRequiredFlag = "Y";
  private boolean   dataFilterBypassPrivacyPolicyVerification = false;
  private String    displayPage               = null;
  private String    entityId                  = null;
  private String    entityType                = null;
  private HashMap   hashXML                   = new HashMap();
  private String    includeComments           = null;
  private boolean   invokedByThisSessionId    = true;
  private String    loadMemberDataUrl         = null;
  private String    masterOrderNum            = null;
  private String    masterOrderNumberNew      = null;
  private String    masterOrderNumberPrevious = null;
  private long      maxCustomerAccountOrders  = 50;
  private long      maxCustomerEmailAddresses = 50;
  private long      maxCustomersPerPage       = 50;
  private long      maxOrderItems             = 3;
  private long      maxRecordsAllowed         = 150;
  private String    membershipId              = null;
  private long      newPagePosition           = 0;
  private long      orderCSCIPositionOld      = 0;
  private String    orderDetailId             = null;
  private String    orderGuid                 = null;
  private String    orderLevel                = null;
  private String    orderNumber               = null;
  private String    orderOrigin               = null;
  private String    orderValidationRequiredFlag = "Y";
  private HashMap   pageData                  = new HashMap();
  private String    payInfoNumber             = null;
  private String    payNumber                 = null;
  private String    recipientFlag             = null;
  private String    recipientInd              = "N";
  private long      recordsToBeReturned       = 0;
  private String    refundDispCode            = null;
  private String    responsibleParty          = null;
  private String    scrubUrl                  = null;
  private String    sessionId                 = null;
  private HashMap   searchCriteria            = new HashMap();
  private String    searchedOnExtOrderNumber  = null;
  private String    sourceCode                = null;
  private long      startSearchingFrom        = 0;
  private boolean   startTimer                = false;
  private String    startTimerOrderGuid       = null;
  private boolean   stopTimer                 = false;
  private String    timerCallLogId            = "";
  private String    timerCommentOriginType    = "";
  private String    timerEntityHistoryId      = "";
  private long      totalCAIPages             = 0;
  private long      totalCEAPages             = 0;
  private long      totalCOSPages             = 0;
  private long      totalCSCIPages            = 0;
  private String    viewBillingUrl            = null;
  private String    viewReconUrl              = null;
  private String    jcpFlag                   = "";
  private boolean   repHasAccess              = true;
  private boolean   returnFromJoe             = false;
  private boolean   modifyOrderInJoe          = false;
  private String    origOrderDetailId         = "";
  private boolean   deleteCSRViewed           = false;

  private String companyId;
  private boolean canadianRecipient;
  private String    showEmail;   
  
	private long defaultRecordStart = 1;
	private long defaultRecordFetchAll = 0;
	private final String custMembershipData = "custMembershipData";

/*******************************************************************************************
 * Constructor 1
 *******************************************************************************************/
  public CustomerOrderSearchBO()
  {
  }


/*******************************************************************************************
 * Constructor 2
 *******************************************************************************************/
  public CustomerOrderSearchBO(HttpServletRequest request, Connection con)
  {
    this.request = request;
    this.con = con;
  }


/*******************************************************************************************
  * processRequest(action)
  ******************************************************************************************
  * Besides the constructor, this is the only public method in this class.
  * This method will accept a string called action and after interogating the value, will
  * process accordingly.  It will generate the search criteria, the page details, and the
  * valid data to be passed back to the calling class in a HashMap.
  * Note that the search criteria and page details are returned back as HashMap objects,
  * while all other data is returned as XML objects.
  *
  * @param  String - action
  * @return HashMap - contains 0 - many XML documents of data, 1 HashMap each for page
  *                   details and search criteria
  * @throws
  */
  public HashMap processRequest(String action) throws Exception
  {
    boolean firstInvokation = false;

    //HashMap that will be returned to the calling class
    HashMap returnHash = new HashMap();

    //Get info from the request object
    getRequestInfo(this.request);

    //Get config file info
    getConfigInfo();
    
    //Get CSR preferred Partner permissions
    getCSRPreferredAccess();
       
/**
    //if this is the first time that a CSR has invoked this action, remove ALL the expired
    //sessionIDs from the CSR_VIEWING_ENTITIES, and trip a flag in the data filter
    if(!this.invokedByThisSessionId)
    {
      deleteAllExpiredSessionIds();
    }
**/
    //if the action = load_page, delete records from CSR_VIEWING_ENTITIES table for the current
    //session id.
    if(action.equalsIgnoreCase(COMConstants.ACTION_LOAD_PAGE) || action.equalsIgnoreCase(COMConstants.ACTION_LOAD_LPSEARCH_PAGE))
    {
      deleteCsrViewing();
    }

    //if the action = search, we want to reset the search information in the data filter
    if(action.equalsIgnoreCase(COMConstants.ACTION_SEARCH)  ||
       action.equalsIgnoreCase(COMConstants.ACTION_LAST)    ||
       action.equalsIgnoreCase(COMConstants.ACTION_LAST_50) ||
       action.equalsIgnoreCase(COMConstants.ACTION_LPSEARCH))
          firstInvokation = true;

    //Always generate the search criteria
    generateSearchCriteria(firstInvokation);

    //Initialize class level variable 'action'
    this.action = action;

    // Take care of order locks/timers if we came from JOE Modify Order
    checkIfReturningFromJoe();

    //process the action
    processAction();

    //check if this.timerCallLogId has a value and action != insert_privacy_plicy and action != load_page
    //if this.timerCallLogId = null or ""
    //  its the first invokation for this call log id, and we don't have to retrieve the data.
    //else 
    //  check data filter value 
    //  if data filter shows that we should bypass privacy policy verification
    //    bypass validated PPV retrieval 
    //  else
    //    check if already validated
    //    if already validated
    //      bypass validated PPV retrieval 
    //    else 
    //      retrieve validated PPV 
    if (    this.timerCallLogId != null && !this.timerCallLogId.equalsIgnoreCase("") 
        &&  !this.dataFilterBypassPrivacyPolicyVerification
        &&  (this.customerValidationRequiredFlag.equalsIgnoreCase("Y") || this.orderValidationRequiredFlag.equalsIgnoreCase("Y"))
        &&  !this.action.equalsIgnoreCase(COMConstants.ACTION_INSERT_PRIVACY_POLICY)
        &&  !this.action.equalsIgnoreCase(COMConstants.ACTION_LOAD_PAGE)
       )
      retrieveValidatedPrivacyPolicy();

    //populate the remainder of the fields on the page data
    populatePageData();

    //Get preferred partners associated to customer
    getCustomerPreferredPartners();

    //check if the timer has to be stopped/started
    timerStartStop();


    //At this point, we have the VIEWINGS node added to the hashXML from both the Customer Account
    //Information cursor, as well as the Customer Shopping Cart Information cursor.
    //Depending on the XSL to be displayed, remove one of these nodes.
    if (this.pageData.get("XSL") != null )
    {
      if (this.pageData.get("XSL").toString().equalsIgnoreCase(COMConstants.XSL_CUSTOMER_ACCOUNT))
      {
        this.hashXML.remove(COMConstants.HK_CSCI_VIEWING_XML);
      }
      else if(this.pageData.get("XSL").toString().equalsIgnoreCase(COMConstants.XSL_CUSTOMER_SHOPPING_CART))
      {
        this.hashXML.remove(COMConstants.HK_CAI_VIEWING_XML);
      }
    }

    //At this point, we should have three HashMaps.
    // HashMap1 = hashXML         - hashmap that contains zero to many Documents
    // HashMap2 = pageData        - hashmap that contains String data at page level
    // HashMap3 = searchCriteria  - hashmap that contains the search criteria that we
    //                              received in the request
    //
    //Combine all of the above HashMaps 3 into one HashMap, called returnHash

    //retrieve all the Documents from hashXML and put them in returnHash
    //retrieve the keyset
    Set ks1 = hashXML.keySet();
    Iterator iter = ks1.iterator();
    String key = null;
    Document doc = null;

    //Iterate thru the keyset
    while(iter.hasNext())
    {
      //get the key
      key = iter.next().toString();
      //retrieve the XML document
      doc = (Document) hashXML.get(key);
      //put the XML object in the returnHash
      returnHash.put(key, (Document) doc);
    }

    //append pageData to returnHash
    returnHash.put("pageData",        (HashMap)this.pageData);
    return returnHash;
  }



/*******************************************************************************************
  * generateSearchCriteria() -
  ******************************************************************************************
  * This method will call the BasePageBuilder to investigate the request, and generate the
  * search criteria
  *
  * @param  none
  * @return none
  * @throws
  */
  private void generateSearchCriteria(boolean firstInvokation)
  {
    BasePageBuilder bpb = new BasePageBuilder();
    if (firstInvokation)
      this.searchCriteria = (HashMap) bpb.populateSearchCriteriaFirst(this.request, this.con);
    else
      this.searchCriteria = (HashMap) bpb.populateSearchCriteria(this.request);
  }


/*******************************************************************************************
  * getRequestInfo(HttpServletRequest request)
  ******************************************************************************************
  * Retrieve the info from the request object
  *
  * @param  HttpServletRequest - request
  * @return none
  * @throws none
  */
  private void getRequestInfo(HttpServletRequest request) throws Exception
  {

    /****************************************************************************************
     *  Retrieve Page Parameters
     ***************************************************************************************/
    //retrieve the current page position
    if(request.getParameter(COMConstants.CAI_CURRENT_PAGE)!=null)
    {
      if(!request.getParameter(COMConstants.CAI_CURRENT_PAGE).toString().equalsIgnoreCase(""))
      {
        this.currentCAIPagePosition =
          Long.valueOf(request.getParameter(COMConstants.CAI_CURRENT_PAGE)).longValue();
      }
    }

    //retrieve the current page position
    if(request.getParameter(COMConstants.COS_CURRENT_PAGE)!=null)
    {
      if(!request.getParameter(COMConstants.COS_CURRENT_PAGE).toString().equalsIgnoreCase(""))
      {
        this.currentCOSPagePosition =
          Long.valueOf(request.getParameter(COMConstants.COS_CURRENT_PAGE)).longValue();
      }
    }


    //retrieve the current page position
    if(request.getParameter(COMConstants.CSCI_CURRENT_PAGE)!=null)
    {
      if(!request.getParameter(COMConstants.CSCI_CURRENT_PAGE).toString().equalsIgnoreCase(""))
      {
        this.currentCSCIPagePosition =
          Long.valueOf(request.getParameter(COMConstants.CSCI_CURRENT_PAGE)).longValue();
      }
    }


    //retrieve the current page position
    if(request.getParameter(COMConstants.CEA_CURRENT_PAGE)!=null)
    {
      if(!request.getParameter(COMConstants.CEA_CURRENT_PAGE).toString().equalsIgnoreCase(""))
      {
        this.currentCEAPagePosition =
          Long.valueOf(request.getParameter(COMConstants.CEA_CURRENT_PAGE)).longValue();
      }
    }


    //retrieve the current start position
    if(request.getParameter(COMConstants.CSCI_ORDER_POSITION)!=null)
    {
      if(!request.getParameter(COMConstants.CSCI_ORDER_POSITION).toString().equalsIgnoreCase(""))
      {
        this.orderCSCIPositionOld =
          Long.valueOf(request.getParameter(COMConstants.CSCI_ORDER_POSITION)).longValue();
      }
    }



    //retrieve the total number of pages that was initially sent to the page
    if(request.getParameter(COMConstants.PD_COS_TOTAL_PAGES)!=null)
    {
      if(!request.getParameter(COMConstants.PD_COS_TOTAL_PAGES).toString().equalsIgnoreCase(""))
      {
        this.totalCOSPages =
          Long.valueOf(request.getParameter(COMConstants.PD_COS_TOTAL_PAGES)).longValue();
      }
    }


    //retrieve the total number of pages that was initially sent to the page
    if(request.getParameter(COMConstants.PD_CEA_TOTAL_PAGES)!=null)
    {
      if(!request.getParameter(COMConstants.PD_CEA_TOTAL_PAGES).toString().equalsIgnoreCase(""))
      {
        this.totalCEAPages =
          Long.valueOf(request.getParameter(COMConstants.PD_CEA_TOTAL_PAGES)).longValue();
      }
    }


    //retrieve the total number of pages that was initially sent to the page
    if(request.getParameter(COMConstants.PD_CAI_TOTAL_PAGES)!=null)
    {
      if(!request.getParameter(COMConstants.PD_CAI_TOTAL_PAGES).toString().equalsIgnoreCase(""))
      {
        this.totalCAIPages =
          Long.valueOf(request.getParameter(COMConstants.PD_CAI_TOTAL_PAGES)).longValue();
      }
    }


    //retrieve the total number of pages that was initially sent to the page
    if(request.getParameter(COMConstants.PD_CSCI_TOTAL_PAGES)!=null)
    {
      if(!request.getParameter(COMConstants.PD_CSCI_TOTAL_PAGES).toString().equalsIgnoreCase(""))
      {
        this.totalCSCIPages =
          Long.valueOf(request.getParameter(COMConstants.PD_CSCI_TOTAL_PAGES)).longValue();
      }
    }

    //retrieve the parameter that shows the page name which triggered this request.  It could
    //either be the customer account page or the customer shopping cart page.
    if(request.getParameter(COMConstants.DISPLAY_PAGE)!=null)
    {
        this.displayPage = request.getParameter(COMConstants.DISPLAY_PAGE);
    }

    //retrieve the parameter that shows the page name which triggered this request.  It could
    //either be the customer account page or the customer shopping cart page.
    if(request.getParameter(COMConstants.RECIPIENT_FLAG)!=null)
    {
        this.recipientFlag = request.getParameter(COMConstants.RECIPIENT_FLAG);
    }


    /****************************************************************************************
     *  Retrieve order/search specific parameters
     ***************************************************************************************/
    //retrieve the order guid
    if(request.getParameter(COMConstants.ORDER_GUID)!=null)
    {
      this.orderGuid = request.getParameter(COMConstants.ORDER_GUID);
    }

    //retrieve the order number
    if(request.getParameter(COMConstants.ORDER_NUMBER)!=null)
    {
      this.orderNumber = request.getParameter(COMConstants.ORDER_NUMBER);
    }

    //retrieve the master order number
    if(request.getParameter(COMConstants.MASTER_ORDER_NUMBER)!=null)
      this.masterOrderNumberPrevious = request.getParameter(COMConstants.MASTER_ORDER_NUMBER);

    //retrieve the searched_on_external_order number
    if(request.getParameter(COMConstants.SEARCHED_ON_EXTERNAL_ORDER_NUMBER)!=null)
      this.searchedOnExtOrderNumber = request.getParameter(COMConstants.SEARCHED_ON_EXTERNAL_ORDER_NUMBER);
    else if(request.getAttribute(COMConstants.SEARCHED_ON_EXTERNAL_ORDER_NUMBER)!=null)
    	this.searchedOnExtOrderNumber = request.getAttribute(COMConstants.SEARCHED_ON_EXTERNAL_ORDER_NUMBER).toString();

    	

    //retrieve the order detail id
    if(request.getParameter(COMConstants.ORDER_DETAIL_ID)!=null)
      this.orderDetailId = request.getParameter(COMConstants.ORDER_DETAIL_ID);

    //retrieve the membership id
    if(request.getParameter(COMConstants.MEMBERSHIP_ID)!=null)
      this.membershipId = request.getParameter(COMConstants.MEMBERSHIP_ID);

    //retrieve the customer id
    if(request.getParameter(COMConstants.CUSTOMER_ID)!=null)
    {
      this.customerId = request.getParameter(COMConstants.CUSTOMER_ID);
    }

    //retrieve the last_order_date and set the this.recipientInd
    if(request.getParameter(COMConstants.LAST_ORDER_DATE)!=null)
    {
      if  (request.getParameter(COMConstants.LAST_ORDER_DATE).equalsIgnoreCase("recipient")  )
      {
        this.recipientInd = COMConstants.CONS_YES;
      }
    }

    //retrieve the source_code
    if(request.getParameter(COMConstants.SOURCE_CODE)!=null)
      this.sourceCode = request.getParameter(COMConstants.SOURCE_CODE);

    //retrieve the pay_info_number
    if(request.getParameter(COMConstants.PAY_INFO_NUMBER)!=null)
      this.payInfoNumber = request.getParameter(COMConstants.PAY_INFO_NUMBER);

    //retrieve the include_comments
    if(request.getParameter(COMConstants.INCLUDE_COMMENTS)!=null)
      this.includeComments = request.getParameter(COMConstants.INCLUDE_COMMENTS);

    //retrieve the entity type
    if(request.getParameter(COMConstants.ENTITY_TYPE)!=null)
      this.entityType = request.getParameter(COMConstants.ENTITY_TYPE);

    //retrieve the entity id
    if(request.getParameter(COMConstants.ENTITY_ID)!=null)
      this.entityId = request.getParameter(COMConstants.ENTITY_ID);

    //for refunds, retrieve and pass back the info for refund_disp_code
    if(request.getParameter(COMConstants.REFUND_DISP_CODE)!=null)
    {
      this.refundDispCode = request.getParameter(COMConstants.REFUND_DISP_CODE);
    }

    //for refunds, retrieve and pass back the info for responsible_party
    if(request.getParameter(COMConstants.RESPONSIBLE_PARTY)!=null)
    {
      this.responsibleParty = request.getParameter(COMConstants.RESPONSIBLE_PARTY);
    }

    //retrieve the order level
    if(request.getParameter(COMConstants.ORDER_LEVEL)!=null)
      this.orderLevel = request.getParameter(COMConstants.ORDER_LEVEL);


    /****************************************************************************************
     *  Retrieve Timer Info
     ***************************************************************************************/
    //retrieve the t_comment_origin_type
    if(request.getParameter(COMConstants.TIMER_COMMENT_ORIGIN_TYPE)!=null)
      this.timerCommentOriginType = request.getParameter(COMConstants.TIMER_COMMENT_ORIGIN_TYPE);


    //retrieve the t_entity_history_id
    if(request.getParameter(COMConstants.TIMER_ENTITY_HISTORY_ID)!=null)
      this.timerEntityHistoryId = request.getParameter(COMConstants.TIMER_ENTITY_HISTORY_ID);

    //retrieve the t_call_log_id
    if(request.getParameter(COMConstants.TIMER_CALL_LOG_ID)!=null)
      this.timerCallLogId = request.getParameter(COMConstants.TIMER_CALL_LOG_ID);


    /****************************************************************************************
     *  Retrieve Security Info
     ***************************************************************************************/
      this.sessionId   = request.getParameter(COMConstants.SEC_TOKEN);


    /****************************************************************************************
     *  Retrieve Data Filter Info
     ***************************************************************************************/
    if(request.getParameter(COMConstants.BYPASS_PRIVACY_POLICY_VERIFICATION)!=null &&
       request.getParameter(COMConstants.BYPASS_PRIVACY_POLICY_VERIFICATION).toString().equalsIgnoreCase("Y"))
      this.dataFilterBypassPrivacyPolicyVerification = true;

    /****************************************************************************************
     *  Retrieve Data for Privacy Policy Verification
     ***************************************************************************************/
    if(request.getParameter(COMConstants.CUSTOMER_VALIDATION_REQUIRED_FLAG)!=null)
      this.customerValidationRequiredFlag = request.getParameter(COMConstants.CUSTOMER_VALIDATION_REQUIRED_FLAG);

    if(request.getParameter(COMConstants.ORDER_VALIDATION_REQUIRED_FLAG)!=null)
      this.orderValidationRequiredFlag = request.getParameter(COMConstants.ORDER_VALIDATION_REQUIRED_FLAG);
    //To view list of email addresses associated with the account
    this.showEmail = request.getParameter("showEmailDetails");

    
    /****************************************************************************************
     *  Retrieve Modify Order Info from JOE
     ***************************************************************************************/
    // Determine if we are returning from JOE (from a Modify Order)
    String rfj = request.getParameter(COMConstants.RETURN_FROM_JOE);
    if("Y".equalsIgnoreCase(rfj)) {
        this.returnFromJoe = true;
    }
    // Determine if a Modify Order was completed in JOE 
    // (we may be returning from JOE, but CSR could have aborted the modify)
    String moij = request.getParameter(COMConstants.MODIFY_ORDER_IN_JOE);
    if("Y".equalsIgnoreCase(moij)) {
        this.modifyOrderInJoe = true;
    }
    // Get order_detail_id of original order
    if(request.getParameter(COMConstants.ORIG_ORDER_DETAIL_ID)!=null)
        this.origOrderDetailId = request.getParameter(COMConstants.ORIG_ORDER_DETAIL_ID); 

  }


  /*******************************************************************************************
    * checkIfReturningFromJoe()
    ******************************************************************************************
    * When a Modify/Update Order is selected in COM, we actually jump to JOE for processing.
    * This method checks if we are returning from JOE after that processing.
    *
    */
  private void checkIfReturningFromJoe() throws Exception {
      if (returnFromJoe) {
          // We are returning from JOE after Update Order handling so release original order lock
          // regardless if Update Order was completed or aborted.
          logger.info("Returning from JOE after Update Order on order_detail_id: " + this.origOrderDetailId);
          LockDAO lockDAO = new LockDAO(this.con);
          lockDAO.releaseLock(this.sessionId, this.csrId, this.origOrderDetailId, "MODIFY_ORDER");
          
          if (this.action.equalsIgnoreCase(COMConstants.ACTION_SEARCH)) {
              // Based on action, we are to view new order created by JOE, so stop original timer.
              timerStop();
          } else if (this.action.equalsIgnoreCase(COMConstants.ACTION_CUSTOMER_ACCOUNT_SEARCH)) {
              // Based on action, we are to view original order. 
              // If Update Order was actually completed in JOE (as opposed to being aborted), 
              // then we must indicate a Call Disposition is necessary on original order.
              if (this.modifyOrderInJoe) {
                  DecisionResultDataFilter.updateEntity(this.request, this.origOrderDetailId, true);
              }
          } else {
              // This should never happen as it would be a coding error, so just log error.
              logger.error("Returned from JOE with unexpected action: " + this.action);
          }
      }
  }

  
/*******************************************************************************************
  * getConfigInfo()
  ******************************************************************************************
  * Retrieve the info from the configuration file
  *
  * @param none
  * @return
  * @throws none
  */
  private void getConfigInfo() throws Exception
  {

    ConfigurationUtil cu = null;
    cu = ConfigurationUtil.getInstance();


    //get MAX_RECORDS_ALLOWED
    if (cu.getProperty(COMConstants.PROPERTY_FILE, COMConstants.CONS_MAX_RECORDS_ALLOWED) != null)
      this.maxRecordsAllowed  = Long.valueOf(cu.getProperty(COMConstants.PROPERTY_FILE, COMConstants.CONS_MAX_RECORDS_ALLOWED)).longValue();

    //get MAX_CUSTOMERS_PER_PAGE
    if (cu.getProperty(COMConstants.PROPERTY_FILE, COMConstants.CONS_MAX_CUSTOMERS_PER_PAGE) != null)
      this.maxCustomersPerPage  = Long.valueOf(cu.getProperty(COMConstants.PROPERTY_FILE, COMConstants.CONS_MAX_CUSTOMERS_PER_PAGE)).longValue();

    //get MAX_CUSTOMER_ACCOUNT_ORDERS
    if (cu.getProperty(COMConstants.PROPERTY_FILE, COMConstants.CONS_MAX_CUSTOMER_ACCOUNT_ORDERS) != null)
      this.maxCustomerAccountOrders = Long.valueOf(cu.getProperty(COMConstants.PROPERTY_FILE, COMConstants.CONS_MAX_CUSTOMER_ACCOUNT_ORDERS)).longValue();

    //get MAX_ORDER_ITEMS
    if (cu.getProperty(COMConstants.PROPERTY_FILE, COMConstants.CONS_MAX_ORDER_ITEMS) != null)
      this.maxOrderItems  = Long.valueOf(cu.getProperty(COMConstants.PROPERTY_FILE, COMConstants.CONS_MAX_ORDER_ITEMS)).longValue();

    //get SCRUB_URL
    if(cu.getProperty(COMConstants.PROPERTY_FILE, COMConstants.CONS_SCRUB_URL) != null)
    {
      this.scrubUrl = cu.getFrpGlobalParm(COMConstants.COM_CONTEXT, COMConstants.CONS_SCRUB_URL);
    }

    //get VIEW_RECON_URL
    if(cu.getProperty(COMConstants.PROPERTY_FILE, COMConstants.CONS_VIEW_RECON_URL) != null)
    {
      this.viewReconUrl = cu.getFrpGlobalParm(COMConstants.COM_CONTEXT, COMConstants.CONS_VIEW_RECON_URL);
    }

    //get VIEW_BILLING_URL
    if(cu.getProperty(COMConstants.PROPERTY_FILE, COMConstants.CONS_VIEW_BILLING_URL) != null)
    {
      this.viewBillingUrl = cu.getFrpGlobalParm(COMConstants.COM_CONTEXT, COMConstants.CONS_VIEW_BILLING_URL);
    }




    //get LOAD_MEMBER_DATA_URL
    if(cu.getProperty(COMConstants.PROPERTY_FILE, COMConstants.CONS_LOAD_MEMBER_DATA_URL) != null)
    {
      this.loadMemberDataUrl = cu.getFrpGlobalParm(COMConstants.COM_CONTEXT, COMConstants.CONS_LOAD_MEMBER_DATA_URL);
    }

    //get MAX_CUSTOMER_EMAIL_ADDRESSES
    if (cu.getProperty(COMConstants.PROPERTY_FILE, COMConstants.CONS_MAX_CUSTOMER_EMAIL_ADDRESSES) !=null)
      this.maxCustomerEmailAddresses  = Long.valueOf(cu.getProperty(COMConstants.PROPERTY_FILE, COMConstants.CONS_MAX_CUSTOMER_EMAIL_ADDRESSES)).longValue();


    //get csr Id
    String security = (String) cu.getProperty(COMConstants.PROPERTY_FILE,COMConstants.SECURITY_IS_ON);
    boolean securityIsOn = security.equalsIgnoreCase("true") ? true : false;

   //delete this before going to production.  the following should only check on securityIsOn
    if(securityIsOn || this.sessionId != null)
    {
      SecurityManager sm = SecurityManager.getInstance();
      UserInfo ui = sm.getUserInfo(this.sessionId);
      this.csrId = ui.getUserID();

      //Get Privacy Policy Verification role
      String context = request.getParameter(COMConstants.CONTEXT);
      String token = request.getParameter(COMConstants.SEC_TOKEN);
      this.bypassPrivacyPolicyVerificationByUserRole = sm.assertPermission(context, token, COMConstants.BYPASS_PRIVACY_POLICY_VERIFICATION_RESOURCE, COMConstants.VIEW); 
    }

  }


/*******************************************************************************************
  * populatePageData()
  ******************************************************************************************
  * Populate the page data with the remainder of the fields
  *
  * @param none
  * @return
 * @throws CacheException 
  * @throws none
  */
  private void populatePageData() throws Exception
  {

    //store the master order number in the page data
    if (this.masterOrderNumberNew != null                                                &&
        !this.masterOrderNumberNew.equalsIgnoreCase(""))
    {
      this.pageData.put(COMConstants.PD_MASTER_ORDER_NUMBER,                this.masterOrderNumberNew);
    }
    else if (this.masterOrderNumberPrevious != null                                      &&
             !this.masterOrderNumberPrevious.equalsIgnoreCase(""))
    {
      this.pageData.put(COMConstants.PD_MASTER_ORDER_NUMBER,                this.masterOrderNumberPrevious);
    }
    else
    {
      this.pageData.put(COMConstants.PD_MASTER_ORDER_NUMBER,                null);
    }

    //store the order_guid in the page data
    this.pageData.put(COMConstants.PD_ORDER_GUID, this.orderGuid);

    //store the searched_on_external_order_number in the page data
    this.pageData.put(COMConstants.PD_SEARCHED_ON_EXTERNAL_ORDER_NUMBER,    this.searchedOnExtOrderNumber);

    //store the action passed in the page data
    this.pageData.put(COMConstants.PD_ACTION,                               this.action);

    //store the customer id passed in the page data
    this.pageData.put(COMConstants.PD_CUSTOMER_ID,                          this.customerId);

    //store the refund disposition code passed in the page data
    this.pageData.put(COMConstants.PD_REFUND_DISP_CODE,                     this.refundDispCode);

    //store the responsible party code passed in the page data
    this.pageData.put(COMConstants.PD_REFUND_RESPONSIBLE_PARTY,             this.responsibleParty);

    //store the scrub URL in the page data
    this.pageData.put(COMConstants.PD_SCRUB_URL,                            this.scrubUrl);

    //store the load member URL in the page data
    this.pageData.put(COMConstants.PD_LOAD_MEMBER_DATA_URL,                 this.loadMemberDataUrl);

    //store the view billing URL in the page data
    this.pageData.put(COMConstants.PD_VIEW_BILLING_URL,                     this.viewBillingUrl);

    //store the scrub URL in the page data
    this.pageData.put(COMConstants.PD_VIEW_RECON_URL,                       this.viewReconUrl);

    //store the display page
    this.pageData.put(COMConstants.PD_DISPLAY_PAGE,                         this.displayPage);

    //store the recipient flag
    this.pageData.put(COMConstants.PD_RECIPIENT_FLAG,                       this.recipientFlag);

    // global authorization flag for update order
    GlobalParmHandler globalParms = (GlobalParmHandler)CacheManager.getInstance().getHandler("CACHE_NAME_GLOBAL_PARM");
    this.pageData.put( COMConstants.PD_GLOBAL_AUTHORIZATION_FLAG, globalParms.getGlobalParm("ORDER_PROCESSING", "AUTH_FLAG") );

    this.pageData.put("jcp_flag", this.jcpFlag);
    
    //Privacy Policy Verification
    //if role = bypass, or global = N    
    String sDisplayPopup = globalParms.getGlobalParm(COMConstants.COM_CONTEXT, COMConstants.GLOBAL_NAME_PRIVACY_POLICY_POPUP_DISPLAY); 
    boolean displayPopup = sDisplayPopup==null || sDisplayPopup.equalsIgnoreCase("") || sDisplayPopup.equalsIgnoreCase("Y")?true:false;
    if (!displayPopup || this.bypassPrivacyPolicyVerificationByUserRole || this.dataFilterBypassPrivacyPolicyVerification || this.callLogBypassPrivacyPolicyVerification)
      this.pageData.put( COMConstants.BYPASS_PRIVACY_POLICY_VERIFICATION, "Y");
    else
      this.pageData.put( COMConstants.BYPASS_PRIVACY_POLICY_VERIFICATION, "N");

    this.pageData.put(COMConstants.CUSTOMER_VALIDATION_REQUIRED_FLAG, this.customerValidationRequiredFlag);
    this.pageData.put(COMConstants.ORDER_VALIDATION_REQUIRED_FLAG, this.orderValidationRequiredFlag);
    
    // Populate Free Shipping Service Information to the page data
    ServicesProgramDAO servicesProgramDAO = new ServicesProgramDAO();
    AccountProgramMasterVO accountProgramMasterVO = servicesProgramDAO.getFreeShippingProgramInfo();
    addProgramInformationToPageData(COMConstants.PAGEDATA_SERVICE_SUFFIX_FREESHIP, accountProgramMasterVO);
    
    // Since Modify Order is now being handled through JOE...
    // Obtain and put JOE URL in PageData so we can construct form action to get to JOE from xsl page. 
    ConfigurationUtil configUtil = ConfigurationUtil.getInstance();         
    String modifyOrderJoeUrl = configUtil.getFrpGlobalParm(COMConstants.COM_CONTEXT, COMConstants.CONS_JOE_URL);
    this.pageData.put(COMConstants.PD_MO_JOE_URL, modifyOrderJoeUrl);
  }


  /**
   * Appends the following information to the Page data for the Program master.
   *  Display Name
   *  Description
   *  URL
   * The page data fields are appended with the passed in field Suffix
   * @param fieldSuffix
   * @param accountProgramMasterVO
   */
  private void addProgramInformationToPageData(String fieldSuffix, AccountProgramMasterVO accountProgramMasterVO)
  {
    this.pageData.put(COMConstants.PAGEDATA_SERVICE_DISPLAY_NAME + fieldSuffix, accountProgramMasterVO.getDisplayName());
    this.pageData.put(COMConstants.PAGEDATA_SERVICE_PROGRAM_DESCRIPTION + fieldSuffix, accountProgramMasterVO.getProgramDescription());
    this.pageData.put(COMConstants.PAGEDATA_SERVICE_PROGRAM_URL + fieldSuffix, accountProgramMasterVO.getProgramUrl());
  }

/*******************************************************************************************
  * timerStartStop()
  ******************************************************************************************
  * Check and stop/start the timer
  *
  * @param none
  * @return
  * @throws Exception
  */
  private void timerStartStop() throws Exception
  {

	//check if the TIMER has to be started/stopped
    if  (     this.stopTimer
          &&  this.timerCommentOriginType != null
          &&  !this.timerCommentOriginType.equalsIgnoreCase("")
          &&   ( this.timerCommentOriginType.equalsIgnoreCase(COMConstants.CONS_COM_COMMENT_ORIGIN_TYPE) || this.timerCommentOriginType.equalsIgnoreCase("QUEUE"))
          &&  this.timerEntityHistoryId != null
          &&  !this.timerEntityHistoryId.equalsIgnoreCase("")
          &&  (   ( (this.masterOrderNumberNew == null || this.masterOrderNumberNew.equalsIgnoreCase(""))  && (this.masterOrderNumberPrevious != null && !this.masterOrderNumberPrevious.equalsIgnoreCase("")) )
               || ( (this.masterOrderNumberNew != null && !this.masterOrderNumberNew.equalsIgnoreCase("")) && (this.masterOrderNumberPrevious != null && !this.masterOrderNumberPrevious.equalsIgnoreCase("")) && !this.masterOrderNumberNew.equalsIgnoreCase(this.masterOrderNumberPrevious))
              )
        )
    {
      //only stop the timer if the rep can access the order
      if (this.repHasAccess)
          timerStop();
    }
    if  (     this.startTimer
          &&  (this.timerEntityHistoryId == null || this.timerEntityHistoryId.equalsIgnoreCase(""))
          &&  (   ( (this.masterOrderNumberNew != null && !this.masterOrderNumberNew.equalsIgnoreCase(""))  && (this.masterOrderNumberPrevious == null || this.masterOrderNumberPrevious.equalsIgnoreCase(""))  )
               || ( (this.masterOrderNumberNew != null && !this.masterOrderNumberNew.equalsIgnoreCase(""))  && (this.masterOrderNumberPrevious != null && !this.masterOrderNumberPrevious.equalsIgnoreCase("")) && !this.masterOrderNumberNew.equalsIgnoreCase(this.masterOrderNumberPrevious))
              )
        )
    {
      //only start the timer if the rep can access the order
      if (this.repHasAccess)
        timerStart();
    }

    HashMap parameters = (HashMap) request.getAttribute(COMConstants.CONS_APP_PARAMETERS);
    parameters.put(COMConstants.TIMER_COMMENT_ORIGIN_TYPE,      this.timerCommentOriginType);
    parameters.put(COMConstants.TIMER_ENTITY_HISTORY_ID,        this.timerEntityHistoryId);
    request.setAttribute(COMConstants.CONS_APP_PARAMETERS,      parameters);
    
    if(this.deleteCSRViewed){    	
    	deleteCsrViewed(this.csrId, this.orderGuid, this.orderDetailId, this.customerId, this.sessionId);
    }

  }


/*******************************************************************************************
  * processAction()
  ******************************************************************************************
  *
  * @throws
  */
  private void processAction() throws Exception {
	  
	if (this.searchCriteria != null && this.searchCriteria.get("PRO_ORDER_COUNT") != null
			&& !StringUtils.isEmpty((String) this.searchCriteria.get(COMConstants.SC_PRO_ORDER_NUMBER))) {
		processProOrderSearch();
	}
    //If the action equals "load"
	else if(this.action.equalsIgnoreCase(COMConstants.ACTION_LOAD_PAGE))
    {
      processLoadPage();
    }
    else if(this.action.equalsIgnoreCase(COMConstants.ACTION_LOAD_LPSEARCH_PAGE))
    {
      processLossPreventionSearchPage();    
    }
    //If the action equals "search"
    else if (this.action.equalsIgnoreCase(COMConstants.ACTION_SEARCH) || this.action.equalsIgnoreCase(COMConstants.ACTION_LPSEARCH))
    {
      processSearch();
    }
    //If the action equals "last"
    else if(this.action.equalsIgnoreCase(COMConstants.ACTION_LAST))
    {
      processLast();
    }
    //If the action equals "last_50"
    else if(this.action.equalsIgnoreCase(COMConstants.ACTION_LAST_50))
    {
      processLast50();
    }
    //If the action equals "customer_search"
    else if  (this.action.equalsIgnoreCase(COMConstants.ACTION_CUSTOMER_SEARCH))
    {
      processCustomerSearch();
    }
    //If the action equals "recipient_search"
    else if  (this.action.equalsIgnoreCase(COMConstants.ACTION_RECIPIENT_SEARCH))
    {
      processRecipientSearch();
    }
    //If the action equals "first_page" or "next_page" or "previous_page" or "last_page"
    else if ( (this.action.equalsIgnoreCase(COMConstants.ACTION_FIRST_PAGE))     ||
            (this.action.equalsIgnoreCase(COMConstants.ACTION_NEXT_PAGE))        ||
            (this.action.equalsIgnoreCase(COMConstants.ACTION_PREVIOUS_PAGE))    ||
            (this.action.equalsIgnoreCase(COMConstants.ACTION_LAST_PAGE)) )
    {
      processNavigation();
    }
    //If the action equals "customer_account_search" or "recipient_order_number_search"
    else if ( (this.action.equalsIgnoreCase(COMConstants.ACTION_CUSTOMER_ACCOUNT_SEARCH)) ||
              (this.action.equalsIgnoreCase(COMConstants.ACTION_RECIP_ORD_NUM_SEARCH)))
    {
      processCustAcctRecipOrdNumSearch();
    }
    //If the action equals "csc_more" or "csc_previous"
    else if ( (this.action.equalsIgnoreCase(COMConstants.ACTION_CSCI_NEXT))     ||
              (this.action.equalsIgnoreCase(COMConstants.ACTION_CSCI_PREVIOUS)) )
    {
      processCscNavigation();
    }
    //If the action equals "ca_first" or "ca_next" or "ca_previous" or "ca_last"
    else if ( (this.action.equalsIgnoreCase(COMConstants.ACTION_CA_FIRST_PAGE))     ||
              (this.action.equalsIgnoreCase(COMConstants.ACTION_CA_NEXT_PAGE))      ||
              (this.action.equalsIgnoreCase(COMConstants.ACTION_CA_PREVIOUS_PAGE))  ||
              (this.action.equalsIgnoreCase(COMConstants.ACTION_CA_LAST_PAGE)) )
    {
      processCaNavigation();
    }
    //If the action equals "ca_csc_first" or "ca_csc_next" or "ca_csc_previous" or "ca_csc_last"
    else if ( (this.action.equalsIgnoreCase(COMConstants.ACTION_CA_CSC_FIRST_PAGE))     ||
              (this.action.equalsIgnoreCase(COMConstants.ACTION_CA_CSC_NEXT_PAGE))      ||
              (this.action.equalsIgnoreCase(COMConstants.ACTION_CA_CSC_PREVIOUS_PAGE))  ||
              (this.action.equalsIgnoreCase(COMConstants.ACTION_CA_CSC_LAST_PAGE)) )
    {
      processCaCscNavigation();
    }
    //If the action equals "more_customer_emails"
    else if (this.action.equalsIgnoreCase(COMConstants.ACTION_MORE_CUSTOMER_EMAILS))
    {
      processMoreCustomerEmails();
    }
    //If the action equals "cea_first" or "cea_next" or "cea_previous" or "cea_last"
    else if ( (this.action.equalsIgnoreCase(COMConstants.ACTION_CEA_FIRST_PAGE))     ||
              (this.action.equalsIgnoreCase(COMConstants.ACTION_CEA_NEXT_PAGE))      ||
              (this.action.equalsIgnoreCase(COMConstants.ACTION_CEA_PREVIOUS_PAGE))  ||
              (this.action.equalsIgnoreCase(COMConstants.ACTION_CEA_LAST_PAGE)) )
    {
      processCeaNavigation();
    }
    //If the action equals "refresh_ids"
    else if (this.action.equalsIgnoreCase(COMConstants.ACTION_REFRESH_IDS))
    {
      processRefreshIds();
    }
    //If the action equals "source_code_lookup"
    else if (this.action.equalsIgnoreCase(COMConstants.ACTION_SOURCE_CODE_LOOKUP))
    {
      processSourceCodeLookup();
    }
    //If the action equals "shopping_cart_payment_info_lookup"
    else if (this.action.equalsIgnoreCase(COMConstants.ACTION_SHOP_CART_PMT_INFO))
    {
      processShoppingCartPaymentInfoLookup();
    }
    //If the action equals "recipient_payment_info_lookup"
    else if (this.action.equalsIgnoreCase(COMConstants.ACTION_RECIP_PMT_INFO))
    {
      processRecipientPaymentInfoLookup();
    }
    //If the action equals "membership_info_lookup"
    else if (this.action.equalsIgnoreCase(COMConstants.ACTION_MEMBERSHIP_INFO_LOOKUP))
    {
      processMembershipInfoLookup();
    }
    //If the action equals "print_order"
    else if (this.action.equalsIgnoreCase(COMConstants.ACTION_PRINT_ORDER))
    {
      processPrintOrder();
    }
    //If the action equals "retrieve_refund_lock
    else if (this.action.equalsIgnoreCase(COMConstants.ACTION_RETRIEVE_REFUND_LOCK))
    {
      processrRetrieveRefundLock();
    }
    //If the action equals "load_refund_codes"
    else if (this.action.equalsIgnoreCase(COMConstants.ACTION_LOAD_REFUND_CODES))
    {
      processLoadRefundCodes();
    }
    //If the action equals "check_refund_status"
    else if (this.action.equalsIgnoreCase(COMConstants.ACTION_CHECK_REFUND_STATUS))
    {
      processCheckRefundStatus();
    }
    //If the action equals "refund_cart"
    else if (this.action.equalsIgnoreCase(COMConstants.ACTION_REFUND_CART))
    {
      processRefundCart();
    }
    //If the action equals "refund_release_lock"
    else if (this.action.equalsIgnoreCase(COMConstants.ACTION_REFUND_RELEASE_LOCK))
    {
      processRefundReleaseLock();
    }
    //If the action equals "check_update_florist"
    else if (this.action.equalsIgnoreCase(COMConstants.ACTION_CHECK_UPDATE_FLORIST))
    {
      processCheckUpdateFlorist();
    }
    else if (this.action.equalsIgnoreCase(COMConstants.ACTION_LOAD_PRIVACY_POLICY))
    {
      processLoadPrivacyPolicyVerification();
    }
    else if (this.action.equalsIgnoreCase(COMConstants.ACTION_INSERT_PRIVACY_POLICY))
    {
      processInsertPrivactyPolicyVerification();
    }
    //When services info link is accessed from com
    else if (this.action.equalsIgnoreCase(COMConstants.ACTION_SERVICES_INFO_LOOKUP_COM))
    {
    	processServicesInfoLookup(true);
    }
    //When services info link is accessed from menu
    else if (this.action.equalsIgnoreCase(COMConstants.ACTION_SERVICES_INFO_LOOKUP_MENU))
    {
    	processServicesInfoLookup(false);
    }
    //When getting fsm details from cams by email
    else if (this.action.equalsIgnoreCase(COMConstants.ACTION_SERVICES_GET_FSM_DETAILS))
    {
    	getFSMDetailsForEmail();
    }
    else
    {
      //set the XSL parameter, which governs the page to be loaded
      this.pageData.put("XSL", COMConstants.XSL_CUSTOMER_ORDER_SEARCH);
      this.pageData.put(COMConstants.PD_MESSAGE_DISPLAY, COMConstants.MSG_INVALID_ACTION);
    }

  }



/*******************************************************************************************
  * processLoadPage()
  ******************************************************************************************
  * process the action type of "load".  This method implements the initial page load-up.
  *
  * @param  none
  * @return none
  * @throws none
  */
  private void processLoadPage() throws Exception
  {
    this.stopTimer = true;

    //set the begin and max allowed parameters, to be passed to the DAOs.
    setBeginEndParameters(0, this.maxCustomersPerPage);

    //set the XSL parameter, which governs the page to be loaded
    this.pageData.put("XSL", COMConstants.XSL_CUSTOMER_ORDER_SEARCH);
  }


/*******************************************************************************************
  * processLossPreventionSearchPage()
  ******************************************************************************************
  * 
  *
  * @param  none
  * @return none
  * @throws none
  */
  private void processLossPreventionSearchPage() throws Exception
  {
    this.stopTimer = true;

    //set the begin and max allowed parameters, to be passed to the DAOs.
    setBeginEndParameters(0, this.maxCustomersPerPage);

    //set the XSL parameter, which governs the page to be loaded
    this.pageData.put("XSL", COMConstants.XSL_LOSS_PREVENTION_SEARCH);
  }


/*******************************************************************************************
  * processSearch()
  ******************************************************************************************
  * Process the action type of "search".  This method will search for a customer based
  * on the input parameters, and will set the data required to build the page.
  *
  * Note that this action type is invoked only from the main page: search customers
  *
  * @param  none
  * @return none
  * @throws none
  */
  private void processSearch() throws Exception
  {
    //set the begin and max allowed parameters, to be passed to the DAOs.
    setBeginEndParameters(this.currentCOSPagePosition, this.maxCustomersPerPage);

    //call retrieveCustomerOrderSearchResults which will call the DAO to retrieve the results
    HashMap customerOrderSearchResults =
      retrieveCustomerOrderSearchResults(this.startSearchingFrom, this.recordsToBeReturned);

    this.newPagePosition = this.currentCOSPagePosition + 1;

    //call checkCustomerOrderSearchResults. This method will check the validity of results
    //and will pass a TRUE if the search must go on.  Otherwise, it will return a FALSE.
    if(checkCustomerOrderSearchResults(customerOrderSearchResults, this.newPagePosition))
    {
      //create an array.
      ArrayList aList = new ArrayList();

      //String sCustomerId = "";

      //if (this.
      //Initialize the array with the top and bottom nodes of the XML, and the node for which the
      //value must be retrieved. In this instance, we will retireve the value of customer_id.
      aList.add(0,"SC_SEARCH_CUSTOMERS");
      aList.add(1,"SC_SEARCH_CUSTOMER");
      aList.add(2,"customer_id");
      String sCustomerId = DOMUtil.getNodeValue((Document)this.hashXML.get(
                              COMConstants.HK_COS_CUSTOMER_ORDER_SEARCH_XML), aList);
      this.customerId = sCustomerId;

      //Re-Initialize the array with the top and bottom nodes of the XML, and the node for which
      //the value must be retrieved. In this instance, we will retireve the value of order_number
      aList.clear();
      aList.add(0,"SC_SEARCH_CUSTOMERS");
      aList.add(1,"SC_SEARCH_CUSTOMER");
      aList.add(2,"order_number");
      String sCOSOrderNumber = DOMUtil.getNodeValue((Document)this.hashXML.get(
                                  COMConstants.HK_COS_CUSTOMER_ORDER_SEARCH_XML), aList);
                                  
      //Re-Initialize the array with the top and bottom nodes of the XML, and the node for which
      //the value must be retrieved. In this instance, we will retireve the value of ftd_customer
      aList.clear();
      aList.add(0,"SC_SEARCH_CUSTOMERS");
      aList.add(1,"SC_SEARCH_CUSTOMER");
      aList.add(2,"ftd_customer");
      String ftdCustomer = DOMUtil.getNodeValue((Document)this.hashXML.get(
                                    COMConstants.HK_COS_CUSTOMER_ORDER_SEARCH_XML), aList);
      //Re-Initialize the array with the top and bottom nodes of the XML, and the node for which
      //the value must be retrieved. In this instance, we will retireve the value of preferred_customer
      aList.clear();
      aList.add(0,"SC_SEARCH_CUSTOMERS");
      aList.add(1,"SC_SEARCH_CUSTOMER");
      aList.add(2,"preferred_customer");
      String preferredCustomer = DOMUtil.getNodeValue((Document)this.hashXML.get(
                                    COMConstants.HK_COS_CUSTOMER_ORDER_SEARCH_XML), aList);
      //Re-Initialize the array with the top and bottom nodes of the XML, and the node for which
      //the value must be retrieved. In this instance, we will retrieve the value of usaa_customer
      aList.clear();
      aList.add(0,"SC_SEARCH_CUSTOMERS");
      aList.add(1,"SC_SEARCH_CUSTOMER");
      aList.add(2,"usaa_customer");
      String usaaCustomer = DOMUtil.getNodeValue((Document)this.hashXML.get(
                                    COMConstants.HK_COS_CUSTOMER_ORDER_SEARCH_XML), aList);
      
      //Re-Initialize the array with the top and bottom nodes of the XML, and the node for which
      //the value must be retrieved. In this instance, we will retrieve the value of usaa_recipient
      aList.clear();
      aList.add(0,"SC_SEARCH_CUSTOMERS");
      aList.add(1,"SC_SEARCH_CUSTOMER");
      aList.add(2,"usaa_recipient");
      String usaaRecipient = DOMUtil.getNodeValue((Document)this.hashXML.get(
                                    COMConstants.HK_COS_CUSTOMER_ORDER_SEARCH_XML), aList);
                                      
      //set the begin and max allowed parameters, to be passed to the DAOs.
      setBeginEndParameters(0, this.maxCustomerAccountOrders);

      //call retrieveCustomerAccountInfo which will call the DAO to retrieve the results
      HashMap customerAccountInfoResults =
        retrieveCustomerAccountInfo(sCustomerId, this.startSearchingFrom, this.recordsToBeReturned);

      setButtons(Long.valueOf(this.pageData.get(COMConstants.PD_CAI_NUMBER_OF_ORDERS).toString()).longValue(), 1, this.maxCustomerAccountOrders, "cai");

      this.pageData.put(COMConstants.PD_CAI_START_POSITION, "1");

      //Re-Initialize the array with the top and bottom nodes of the XML, and the node for which
      //the value must be retrieved. In this instance, we will retireve the value of
      //master_order_number
      aList.clear();
      aList.add(0,"CAI_ORDERS");
      aList.add(1,"CAI_ORDER");
      aList.add(2,"master_order_number");
      String sCAIOrderNumber = DOMUtil.getNodeValue((Document)this.hashXML.get(
                                  COMConstants.HK_CAI_ORDER_XML), aList);

      //Re-Initialize the array with the top and bottom nodes of the XML, and the node for which
      //the value must be retrieved. In this instance, we will retireve the value of order_guid
      aList.clear();
      aList.add(0,"CAI_ORDERS");
      aList.add(1,"CAI_ORDER");
      aList.add(2,"order_guid");
      String sCAIOrderGuid = DOMUtil.getNodeValue((Document)this.hashXML.get(
                             COMConstants.HK_CAI_ORDER_XML), aList);


      //If order-number or tracking number was entered, or the search resulted in a recipient
      //record, or if there was only one record found, get the shopping cart information
      boolean csciFlag = false;

      //Create a temp field sOrderNumber.  If the search resulted in a recipient record, we
      //will choose the order number from the Customer Order Search.  Else, we will choose the
      //order number from Customer Account Information search.
      String sOrderNumber = "";
      String sOrderGuid = "";

      //if the search resulted in a recipient, such that the order number was returned back
      //implying that 1)search could have been for an order tracking # 2)given the criteria, only
      //one recipient was found, with only one external order number 3)
      if (this.recipientInd.equalsIgnoreCase(COMConstants.CONS_YES) &&
          sCOSOrderNumber != null && !sCOSOrderNumber.equalsIgnoreCase("") )
      {
        sOrderNumber = sCOSOrderNumber;
        csciFlag = true;
      }
      else
      {
        if(!this.searchCriteria.get(COMConstants.SC_ORDER_NUMBER).toString().equalsIgnoreCase(""))
        {
          sOrderNumber = this.searchCriteria.get(COMConstants.SC_ORDER_NUMBER).toString();
          csciFlag = true;
        }
        else if(this.orderNumber != null && !this.orderNumber.equalsIgnoreCase(""))
        {
          sOrderNumber = this.orderNumber;
          sOrderGuid = this.orderGuid;
          csciFlag = true;
        }
        else if (sCAIOrderNumber != null && !sCAIOrderNumber.equalsIgnoreCase("") &&
                 this.pageData.get(COMConstants.PD_CAI_NUMBER_OF_CARTS).toString().equalsIgnoreCase("1"))
        {
          sOrderNumber = sCAIOrderNumber;
          sOrderGuid = sCAIOrderGuid;
          csciFlag = true;
        } 
      }
      boolean repCanAccessCustomer = true;
      if  (csciFlag)
      {
         
        
        //set the begin and max allowed parameters, to be passed to the DAOs.
        setBeginEndParameters(this.currentCSCIPagePosition, this.maxOrderItems);

        //Call customerShoppingCartInfoResults which will call the DAO to retrieve the results
        HashMap customerShoppingCartInfoResults =
            retrieveCustomerShoppingCartInfo(this.startSearchingFrom, this.recordsToBeReturned,
            sOrderNumber, sOrderGuid);
            
        this.pageData.put(COMConstants.BYPASS_COM, "N");
        
         long outTotalIdCount = 0;
         if (customerOrderSearchResults.get("OUT_ID_COUNT") != null)
           outTotalIdCount = Long.valueOf(customerOrderSearchResults.get("OUT_ID_COUNT").toString()).longValue();

         
         //If 1 record record found
         //check to see if this is a preferred partner order and if rep has preferred partner resource
         //if the rep does not have permission to view this order, return them to the customer order search screen
         //and display an error message.  If the rep can access the order, carry on 
         
         //Added August 2017 | User Story 5551.  Modified September 2017 | User Story 5651
         //Check if customer is usaa customer.  If they are then check if rep has USAA access.  If they do continue.  If they don't return warning message.      
         if ( (outTotalIdCount == 1 && repCanAccessOrder() && usaaCustomer != null && usaaCustomer.equalsIgnoreCase("N"))
        	  ||
        	  (outTotalIdCount == 1 && repCanAccessOrder() && usaaCustomer != null && usaaCustomer.equalsIgnoreCase("Y") && repCanAccessUSAAOrder()) )
         {
          	     //get the value of OUT_NUMBER_OF_ORDERS
                 long outIdCount = 0;
                 if (customerShoppingCartInfoResults.get("OUT_NUMBER_OF_ORDERS") != null)
                     outIdCount = Long.valueOf(customerShoppingCartInfoResults.get("OUT_NUMBER_OF_ORDERS").toString()).longValue();
           
                 long outOrderPosition = 0;
                 if (customerShoppingCartInfoResults.get("OUT_ORDER_POSITION") != null)
                   outOrderPosition = Long.valueOf(customerShoppingCartInfoResults.get("OUT_ORDER_POSITION").toString()).longValue();
           
                 //Set the navigation buttons
                 setCSCIButtons(outIdCount, outOrderPosition, this.newPagePosition, this.maxOrderItems, "csci");
           
                 //set the XSL parameter, which governs the page to be loaded
                 this.pageData.put("XSL", COMConstants.XSL_CUSTOMER_SHOPPING_CART);
           
                 //Initially in the project, search on an order number would have taken the csr to the
                 //recipient order tab, and thus, when the stored proc returned <CSR_VIEWINGS> info, it was
                 //basing it on the external order number.  This was however changed so that the CSR will
                 //be routed to the Shopping Cart page (Customer Info tab).
                 //On the other hand, if the CSR search or clicked on an order from any other page, we should
                 //direct control to the Recipient Order tab.
                 //Thus, the <CSR_VIEWINGS> has to be rebuild for the first scenario
           
                 //if it exists, delete the <CSR_VIEWINGS> node,
                 //if the control was given from the Queues, the CSR wants to go to the RO page but does not 
                 //have the customer info.  thats why, action=yes.  but, we want the CSR_VIEWING from
                 //an order_detail perspective. 
                 if ((this.recipientFlag != null)&&(!this.recipientFlag.equalsIgnoreCase(COMConstants.CONS_YES)))
                 {
                   if (this.hashXML.get(COMConstants.HK_CSCI_VIEWING_XML) != null)
                   {
                     this.hashXML.remove(COMConstants.HK_CSCI_VIEWING_XML);
                     }
             
                   //retrieve the <CSR_VIEWINGS> at the ORDERS level
                   Document csrViewingInfo = retrieveCSRViewing("ORDERS", this.orderGuid);
             
                   //and re-add it to the xml
                   this.hashXML.put(COMConstants.HK_CSCI_VIEWING_XML,  csrViewingInfo);
                 }
         }
         this.repHasAccess = false;
         this.deleteCSRViewed = true;
         repCanAccessCustomer = false;
       }
      else
      {
    	//set the XSL parameter, which governs the page to be loaded
        //add a check to see if non-preferred partner rep is trying to access preferred partner customer only
        //if they are, return them to the Customer Order Search screen, else continue
        repCanAccessCustomer = true;
        if( ftdCustomer.equals("N") && preferredCustomer.equals("Y") && !repHasPreferredPartnerPermission()
        	||
            repCanAccessOrder() && usaaCustomer != null && usaaCustomer.equalsIgnoreCase("Y") && !repCanAccessUSAAOrder()
            ||
            repCanAccessOrder() && usaaRecipient != null && usaaRecipient.equalsIgnoreCase("Y") && !repCanAccessUSAAOrder())
        {
            
            repCanAccessCustomer = false;
            this.repHasAccess = false;
            this.deleteCSRViewed = true;
            //set the XSL parameter, which governs the page to be loaded
            if(this.action.equals(COMConstants.ACTION_SEARCH)) {
                this.pageData.put("XSL", COMConstants.XSL_CUSTOMER_ORDER_SEARCH);    
            }
            else {
                this.pageData.put("XSL", COMConstants.XSL_LOSS_PREVENTION_SEARCH);
            }
            
        }
        if(repCanAccessCustomer)
        {
        	this.pageData.put("XSL", COMConstants.XSL_CUSTOMER_ACCOUNT);
            setButtons(Long.valueOf(this.pageData.get(COMConstants.PD_CAI_NUMBER_OF_ORDERS).toString()).longValue(), this.newPagePosition, this.maxCustomerAccountOrders, "cai");
            this.pageData.put(COMConstants.PD_CAI_START_POSITION, String.valueOf(startSearchingFrom));
        }
      }

    }
  }



/*******************************************************************************************
  * processLast()
  ******************************************************************************************
  * This method will process the action type of "last"
  *
  * @param  none
  * @return none
  * @throws none
  */
  private void processLast() throws Exception
  {
    long recordsToBeReturned = 1;
    Document csrViewed = DOMUtil.getDocument();

    //set the begin and max allowed parameters, to be passed to the DAOs.
    setBeginEndParameters(this.currentCOSPagePosition, recordsToBeReturned);

    //Call retrieveCSRViewed which will call the DAO to retrieve the results
    csrViewed = retrieveCSRViewed(recordsToBeReturned);
    //create an array.
    ArrayList aList = new ArrayList();

    //Initialize the array with the top and bottom nodes of the XML, and the node for which
    //the value must be retrieved. In this instance, we will retireve the value of
    //customer id
    aList.clear();
    aList.add(0,"CSR_VIEWED");
    aList.add(1,"CSR");
    aList.add(2,"customer_id");
    String sCustomerId = DOMUtil.getNodeValue(csrViewed, aList);
    this.customerId = sCustomerId;
    
    //Initialize the array with the top and bottom nodes of the XML, and the node for which
    //the value must be retrieved. In this instance, we will retireve the value of
    //ftd customer
    aList.clear();
    aList.add(0,"CSR_VIEWED");
    aList.add(1,"CSR");
    aList.add(2,"ftd_customer");
    String sFtdCustomer = DOMUtil.getNodeValue(csrViewed, aList);
    
    //Initialize the array with the top and bottom nodes of the XML, and the node for which
    //the value must be retrieved. In this instance, we will retireve the value of
    //preferred customer
    aList.clear();
    aList.add(0,"CSR_VIEWED");
    aList.add(1,"CSR");
    aList.add(2,"preferred_customer");
    String sPreferredCustomer = DOMUtil.getNodeValue(csrViewed, aList);
    
    //Initialize the array with the top and bottom nodes of the XML, and the node for which
    //the value must be retrieved. In this instance, we will retrieve the value of
    //usaa customer
    aList.clear();
    aList.add(0,"CSR_VIEWED");
    aList.add(1,"CSR");
    aList.add(2,"usaa_customer");
    String sUSAACustomer = DOMUtil.getNodeValue(csrViewed, aList);
    
    //Initialize the array with the top and bottom nodes of the XML, and the node for which
    //the value must be retrieved. In this instance, we will retrieve the value of
    //usaa recipient
    aList.clear();
    aList.add(0,"CSR_VIEWED");
    aList.add(1,"CSR");
    aList.add(2,"usaa_recipient");
    String sUSAARecipient = DOMUtil.getNodeValue(csrViewed, aList);
   
        
    //retrieve preferred partner resource and check to see if the rep has that role
    SecurityManager securityManager = SecurityManager.getInstance();
   // String context = (String) request.getParameter(COMConstants.CONTEXT);
    //String token = (String) request.getParameter(COMConstants.SEC_TOKEN);
    String repCanAccessOrder = "N";
    
    boolean repCanViewOrder = true;
    if( (sFtdCustomer != null && sFtdCustomer.equals("N")) && (sPreferredCustomer !=null && sPreferredCustomer.equals("Y")) &&  !repHasPreferredPartnerPermission())
    { 
        repCanViewOrder = false;  
        repCanAccessOrder = "N";
        pageData.put("repCanAccessOrder", repCanAccessOrder);
    }
    else
    {
        repCanAccessOrder = "Y";
        pageData.put("repCanAccessOrder", repCanAccessOrder);
    }
    
    if(sCustomerId  != null && !sCustomerId.equalsIgnoreCase("") && repCanViewOrder)
    {
      processCustomerSearch();
    }
    else
    {
      //Store the XML generated by the cursor OUT_CART_CUR in hashXML HashMap
      this.hashXML.put(COMConstants.HK_COS_CSR_VIEWED_XML,        csrViewed);

      //set the XSL parameter, which governs the page to be loaded
      this.pageData.put("XSL",  COMConstants.XSL_CUSTOMER_ORDER_SEARCH);

      //set the Error message to be displayed
      this.pageData.put(COMConstants.PD_MESSAGE_DISPLAY, COMConstants.MSG_NO_RECORDS);

    }


  }



/*******************************************************************************************
  * processLast50()
  ******************************************************************************************
  * This method will process the action type of "last_50"
  *
  * @param  none
  * @return none
  * @throws none
  */
  private void processLast50() throws Exception
  {
    long recordsToBeReturned = 50;
    Document csrViewed = DOMUtil.getDocument();

    //set the begin and max allowed parameters, to be passed to the DAOs.
    setBeginEndParameters(this.currentCOSPagePosition, recordsToBeReturned);

    //Call retrieveCSRViewed which will call the DAO to retrieve the results
    csrViewed = retrieveCSRViewed(recordsToBeReturned);

    //Check if at least one row is returned
    //create an array.
    ArrayList aList = new ArrayList();

    //Initialize the array with the top and bottom nodes of the XML, and the node for which
    //the value must be retrieved. In this instance, we will retireve the value of
    //customer_id
    aList.clear();
    aList.add(0,"CSR_VIEWED");
    aList.add(1,"CSR");
    aList.add(2,"customer_id");
    String sCustomerId = DOMUtil.getNodeValue(csrViewed, aList);

    if(sCustomerId  != null && !sCustomerId.equalsIgnoreCase(""))
    {
      //Store the XML generated by the cursor OUT_CART_CUR in hashXML HashMap
      this.hashXML.put(COMConstants.HK_COS_CSR_VIEWED_XML,        csrViewed);

      //set the XSL parameter, which governs the page to be loaded
      this.pageData.put("XSL",                                    COMConstants.XSL_LAST_RECORDS_ACCESSED);


      this.pageData.put("cos_" + COMConstants.PD_CURRENT_PAGE,    "1");
      this.pageData.put(COMConstants.PD_COS_START_POSITION,       "1");
    }
    else
    {
      //Store the XML generated by the cursor OUT_CART_CUR in hashXML HashMap
      this.hashXML.put(COMConstants.HK_COS_CSR_VIEWED_XML,        csrViewed);

      //set the XSL parameter, which governs the page to be loaded
      this.pageData.put("XSL",                                    COMConstants.XSL_CUSTOMER_ORDER_SEARCH);

      //set the Error message to be displayed
      this.pageData.put(COMConstants.PD_MESSAGE_DISPLAY,          COMConstants.MSG_NO_RECORDS);

    }
  }



/*******************************************************************************************
  * processCustomerSearch()
  ******************************************************************************************
  * This method will process the action type of "customer_search"
  *
  * Note that this action type can be invoked from two pages: last/last_50 and multiple
  * records found page.  The action is triggered when the CSR either clicks on the link
  * or enters in the line number.
  *
  * @param  none
  * @return none
  * @throws none
  */
  private void processCustomerSearch() throws Exception
  {
    //set the begin and max allowed parameters, to be passed to the DAOs.
    setBeginEndParameters(0, this.maxCustomerAccountOrders);

    //set the new page position
    this.newPagePosition++;

    //call retrieveCustomerAccountInfo which will call the DAO to retrieve the results
    HashMap customerAccountInfoResults = retrieveCustomerAccountInfo(this.customerId, this.startSearchingFrom, this.recordsToBeReturned);

    setButtons(Long.valueOf(this.pageData.get(COMConstants.PD_CAI_NUMBER_OF_ORDERS).toString()).longValue(), 1, this.maxCustomerAccountOrders, "cai");
    this.pageData.put(COMConstants.PD_CAI_START_POSITION, "1");

    //create an array.
    ArrayList aList = new ArrayList();


    //Initialize the array with the top and bottom nodes of the XML, and the node for which
    //the value must be retrieved. In this instance, we will retireve the value of
    //master_order_number
    aList.clear();
    aList.add(0,"CAI_ORDERS");
    aList.add(1,"CAI_ORDER");
    aList.add(2,"master_order_number");
    String sCAIOrderNumber = DOMUtil.getNodeValue((Document)this.hashXML.get(
                                COMConstants.HK_CAI_ORDER_XML), aList);

    //Re-Initialize the array with the top and bottom nodes of the XML, and the node for which
    //the value must be retrieved. In this instance, we will retireve the value of order_guid
    aList.clear();
    aList.add(0,"CAI_ORDERS");
    aList.add(1,"CAI_ORDER");
    aList.add(2,"order_guid");
    String sCAIOrderGuid = DOMUtil.getNodeValue((Document)this.hashXML.get(
                           COMConstants.HK_CAI_ORDER_XML), aList);

    //If order-number or tracking number was entered, or the search resulted in a recipient
    //record, or if there was only one record found, get the shopping cart information
    boolean csciFlag = false;

    //Create a temp field sOrderNumber.  If the search resulted in a recipient record, we
    //will choose the order number from the Customer Order Search.  Else, we will choose the
    //order number from Customer Account Information search.
    String sOrderNumber = "";
    String sOrderGuid = "";

/* Issue 59, when the csr searches on an order # and then clicks on the recipient_id from the csci
 * page, we should not look at the original order # entered
 *
    if(!this.searchCriteria.get(COMConstants.SC_ORDER_NUMBER).toString().equalsIgnoreCase(""))
    {
      sOrderNumber = this.searchCriteria.get(COMConstants.SC_ORDER_NUMBER).toString();
      csciFlag = true;
    }
*/

    if(this.orderNumber != null && !this.orderNumber.equalsIgnoreCase(""))
    {
      sOrderNumber = this.orderNumber;
      sOrderGuid = this.orderGuid;
      csciFlag = true;
    }

    if (this.pageData.get(COMConstants.PD_CAI_NUMBER_OF_CARTS).toString().equalsIgnoreCase("1")  &&
        sCAIOrderNumber != null && !sCAIOrderNumber.equalsIgnoreCase("") )
    {
      sOrderNumber = sCAIOrderNumber;
      sOrderGuid = sCAIOrderGuid;
      csciFlag = true;
    }

    if  (csciFlag)
    {
      //set the begin and max allowed parameters, to be passed to the DAOs.
      setBeginEndParameters(this.currentCSCIPagePosition, this.maxOrderItems);

      //Call customerShoppingCartInfoResults which will call the DAO to retrieve the results
      HashMap customerShoppingCartInfoResults =
            retrieveCustomerShoppingCartInfo(this.startSearchingFrom, this.recordsToBeReturned,
            sOrderNumber, sOrderGuid);

      this.pageData.put(COMConstants.BYPASS_COM, "N");
       
      //get the value of OUT_NUMBER_OF_ORDERS
      long outIdCount = 0;
      if (customerShoppingCartInfoResults.get("OUT_NUMBER_OF_ORDERS") != null)
        outIdCount = Long.valueOf(customerShoppingCartInfoResults.get("OUT_NUMBER_OF_ORDERS").toString()).longValue();

      long outOrderPosition = 0;
      if (customerShoppingCartInfoResults.get("OUT_ORDER_POSITION") != null)
        outOrderPosition = Long.valueOf(customerShoppingCartInfoResults.get("OUT_ORDER_POSITION").toString()).longValue();

      //Set the navigation buttons
      setCSCIButtons(outIdCount, outOrderPosition, this.newPagePosition, this.maxOrderItems, "csci");
      
    }
    else
    {
      //set the XSL parameter, which governs the page to be loaded
      this.pageData.put("XSL", COMConstants.XSL_CUSTOMER_ACCOUNT);
      setButtons(Long.valueOf(this.pageData.get(COMConstants.PD_CAI_NUMBER_OF_ORDERS).toString()).longValue(), this.newPagePosition, this.maxCustomerAccountOrders, "cai");
      this.pageData.put(COMConstants.PD_CAI_START_POSITION, String.valueOf(startSearchingFrom));
    }

  }



/*******************************************************************************************
  * processRecipientSearch()
  ******************************************************************************************
  * This method will process the action type of "recipient_search"
  *
  * Note that this action type can be invoked from the shopping cart page only.  If the CSR
  * wishes to look at the recipient info ONLY, s/he will hit the 'Recipient Info' link, which
  * will trigger this process.
  * As mentioned above, in this case, the CSR will be directed to the Customer Account Page,
  * where the info pertaining to the recipient will be displayed. Note that even if there is
  * only 1 record, we will NOT go to the Customer Shopping Cart page.
  *
  * @param  none
  * @return none
  * @throws none
  */
  private void processRecipientSearch() throws Exception
  {
    //set the begin and max allowed parameters, to be passed to the DAOs.
    setBeginEndParameters(0, this.maxCustomerAccountOrders);

    //set the new page position
    this.newPagePosition++;

    //call retrieveCustomerAccountInfo which will call the DAO to retrieve the results
    HashMap customerAccountInfoResults = retrieveCustomerAccountInfo(this.customerId, this.startSearchingFrom, this.recordsToBeReturned);

/*    setButtons(Long.valueOf(this.pageData.get(COMConstants.PD_CAI_NUMBER_OF_ORDERS).toString()).longValue(), 1, this.maxCustomerAccountOrders, "cai");
    this.pageData.put(COMConstants.PD_CAI_START_POSITION, "1");
*/

    this.pageData.put("recipient_search","Y");

    //set the XSL parameter, which governs the page to be loaded
    this.pageData.put("XSL", COMConstants.XSL_CUSTOMER_ACCOUNT);
    setButtons(Long.valueOf(this.pageData.get(COMConstants.PD_CAI_NUMBER_OF_ORDERS).toString()).longValue(), this.newPagePosition, this.maxCustomerAccountOrders, "cai");
    this.pageData.put(COMConstants.PD_CAI_START_POSITION, String.valueOf(startSearchingFrom));

  }



/*******************************************************************************************
  * processNavigation()
  ******************************************************************************************
  * This method will process the action type of "first_page", "previous_page", "next_page"
  * and "last_page".  Essentially, this method provides navigational ability for multiple
  * records found page.
  *
  * Note that this action type is invoked only from the multiple records found page
  *
  * @param  none
  * @return none
  * @throws none
  */
  private void processNavigation() throws Exception
  {
    //set the new page position
    if (this.action.equalsIgnoreCase(COMConstants.ACTION_FIRST_PAGE))
      this.newPagePosition = 1;
    else if (this.action.equalsIgnoreCase(COMConstants.ACTION_PREVIOUS_PAGE))
      this.newPagePosition = this.currentCOSPagePosition - 1;
    else if (this.action.equalsIgnoreCase(COMConstants.ACTION_NEXT_PAGE))
      this.newPagePosition = this.currentCOSPagePosition + 1;
    else if (this.action.equalsIgnoreCase(COMConstants.ACTION_LAST_PAGE))
      this.newPagePosition = this.totalCOSPages;

    //set the begin and max allowed parameters, to be passed to the DAOs.
    setBeginEndParameters(this.newPagePosition, this.maxCustomersPerPage);

    //call retrieveCustomerOrderSearchResults which will call the DAO to retrieve the results
    HashMap customerOrderSearchResults =
      retrieveCustomerOrderSearchResults(this.startSearchingFrom, this.recordsToBeReturned);

    //get the value of OUT_ID_COUNT
    long outIdCount = 0;
    if (customerOrderSearchResults.get("OUT_ID_COUNT") != null)
      outIdCount = Long.valueOf(customerOrderSearchResults.get("OUT_ID_COUNT").toString()).longValue();

    //set the XSL parameter, which governs the page to be loaded
    this.pageData.put("XSL", COMConstants.XSL_MULTIPLE_RECORDS_FOUND);

    //Set the navigation buttons
    setButtons(outIdCount, this.newPagePosition, this.maxCustomersPerPage, "cos");
    this.pageData.put(COMConstants.PD_MESSAGE_DISPLAY, COMConstants.MSG_MULTIPLE);
    this.pageData.put(COMConstants.PD_COS_START_POSITION, String.valueOf(startSearchingFrom));
    //set PD_CUSTOMER_ORDER_SEARCH_COUNT and PD_TOTAL_RECORDS_FOUND
    this.pageData.put(COMConstants.PD_COS_TOTAL_RECORDS_FOUND, String.valueOf(outIdCount));


  }



/*******************************************************************************************
  * processCaNavigation()
  ******************************************************************************************
  * This method will process the action type of "ca_first", "ca_previous", "ca_next" and
  * and "ca_last".  Essentially, this method provides navigational ability for when
  * multiple customer accounts are found
  *
  * Note that this action type is invoked only from the customer account page.  The action
  * is triggered when the CSR clicks on one of navigational links displayed within the
  * "Order/Shopping Cart" frame of this page.
  *
  * @param  none
  * @return none
  * @throws none
  */
  private void processCaNavigation() throws Exception
  {
    //set the new page position
    if (this.action.equalsIgnoreCase(COMConstants.ACTION_CA_FIRST_PAGE))
      this.newPagePosition = 1;
    else if (this.action.equalsIgnoreCase(COMConstants.ACTION_CA_PREVIOUS_PAGE))
      this.newPagePosition = this.currentCAIPagePosition - 1;
    else if (this.action.equalsIgnoreCase(COMConstants.ACTION_CA_NEXT_PAGE))
      this.newPagePosition = this.currentCAIPagePosition + 1;
    else if (this.action.equalsIgnoreCase(COMConstants.ACTION_CA_LAST_PAGE))
      this.newPagePosition = this.totalCAIPages;

    //set the begin and max allowed parameters, to be passed to the DAOs.
    setBeginEndParameters(this.newPagePosition, this.maxCustomerAccountOrders);

    //call retrieveCustomerAccountInfo which will call the DAO to retrieve the results
    HashMap customerAccountInfoResults =
      retrieveCustomerAccountInfo(this.customerId, this.startSearchingFrom, this.recordsToBeReturned);

    //get the value of OUT_ID_COUNT
    long outIdCount = 0;
    if (customerAccountInfoResults.get("OUT_NUMBER_OF_ORDERS") != null)
      outIdCount = Long.valueOf(customerAccountInfoResults.get("OUT_NUMBER_OF_ORDERS").toString()).longValue();

    //set the XSL parameter, which governs the page to be loaded
    this.pageData.put("XSL", COMConstants.XSL_CUSTOMER_ACCOUNT);

    //Set the navigation buttons
    setButtons(outIdCount, this.newPagePosition, this.maxCustomerAccountOrders, "cai");
    this.pageData.put(COMConstants.PD_CAI_START_POSITION, String.valueOf(startSearchingFrom));

  }



/*******************************************************************************************
  * processCaCscNavigation()
  ******************************************************************************************
  * This method will process the action type of "ca_csc_first", "ca_csc_previous",
  * "ca_csc_next" and "ca_csc_last".  Essentially, this method provides navigational ability for
  * when multiple customer accounts are found within the shopping cart
  *
  * Note that this action type is invoked only from the customer account page.  The action
  * is triggered when the CSR clicks on one of navigational links displayed within the
  * "Order/Shopping Cart" frame of this page.
  *
  * @param  none
  * @return none
  * @throws none
  */
  private void processCaCscNavigation() throws Exception
  {
    //set the new page position
    if (this.action.equalsIgnoreCase(COMConstants.ACTION_CA_CSC_FIRST_PAGE))
      this.newPagePosition = 1;
    else if (this.action.equalsIgnoreCase(COMConstants.ACTION_CA_CSC_PREVIOUS_PAGE))
      this.newPagePosition = this.currentCAIPagePosition - 1;
    else if (this.action.equalsIgnoreCase(COMConstants.ACTION_CA_CSC_NEXT_PAGE))
      this.newPagePosition = this.currentCAIPagePosition + 1;
    else if (this.action.equalsIgnoreCase(COMConstants.ACTION_CA_CSC_LAST_PAGE))
      this.newPagePosition = this.totalCAIPages;

    //set the begin and max allowed parameters, to be passed to the DAOs.
    setBeginEndParameters(this.newPagePosition, this.maxCustomerAccountOrders);

    //call retrieveCustomerAccountInfo which will call the DAO to retrieve the results
    HashMap customerAccountInfoResults =
      retrieveCustomerAccountInfo(this.customerId, this.startSearchingFrom, this.recordsToBeReturned);

    //get the value of OUT_ID_COUNT
    long outCAIIdCount = 0;
    if (customerAccountInfoResults.get("OUT_NUMBER_OF_ORDERS") != null)
      outCAIIdCount = Long.valueOf(customerAccountInfoResults.get("OUT_NUMBER_OF_ORDERS").toString()).longValue();

    //Set the navigation buttons
    setButtons(outCAIIdCount, this.newPagePosition, this.maxCustomerAccountOrders, "cai");
    this.pageData.put(COMConstants.PD_CAI_START_POSITION, String.valueOf(startSearchingFrom));


    /******Retrieve Customer Shopping Cart Information ******/
    //call customerShoppingCartInfoResults which will call the DAO to retrieve the results
    HashMap customerShoppingCartInfoResults =
      retrieveCustomerShoppingCartInfo(this.orderCSCIPositionOld, this.maxOrderItems);

    //get the value of OUT_NUMBER_OF_ORDERS
    long outCSCIIdCount = 0;
    if (customerShoppingCartInfoResults.get("OUT_NUMBER_OF_ORDERS") != null)
      outCSCIIdCount = Long.valueOf(customerShoppingCartInfoResults.get("OUT_NUMBER_OF_ORDERS").toString()).longValue();

    long outOrderPosition = 0;
    if (customerShoppingCartInfoResults.get("OUT_ORDER_POSITION") != null)
      outOrderPosition = Long.valueOf(customerShoppingCartInfoResults.get("OUT_ORDER_POSITION").toString()).longValue();

    //Set the navigation buttons
    setCSCIButtons(outCSCIIdCount, outOrderPosition, this.newPagePosition, this.maxOrderItems, "csci");

  }


/*******************************************************************************************
  * processCustAcctRecipOrdNumSearch()
  ******************************************************************************************
  * This method will process the action type of "customer_acct_search" and
  * "recipient_order_number_search"
  *
  * 1) "customer_acct_search" action type is invoked only from the customer account page.
  * The action is triggered when the CSR clicks on one of the links displayed within the
  * "Order/Shopping Cart" frame of this page.
  * 2) "recipient_order_number_search" action type is invoked when the CSR enters in an Order # in
  * the mystery box and hits enter.
  *
  * @param  none
  * @return none
  * @throws none
  */
  private void processCustAcctRecipOrdNumSearch() throws Exception
  {
    /******Retrieve Customer Account Information ******/
    //set the begin and max allowed parameters, to be passed to the DAOs.

    setBeginEndParameters(0, this.maxCustomerAccountOrders);

    //set the new page position
    this.newPagePosition++;

    //call retrieveCustomerAccountInfo which will call the DAO to retrieve the results
    HashMap customerAccountInfoResults = retrieveCustomerAccountInfo(this.customerId, this.startSearchingFrom, this.recordsToBeReturned);

    setButtons(Long.valueOf(this.pageData.get(COMConstants.PD_CAI_NUMBER_OF_ORDERS).toString()).longValue(), 1, this.maxCustomerAccountOrders, "cai");
    this.pageData.put(COMConstants.PD_CAI_START_POSITION, "1");

    /******Retrieve Customer Shopping Cart Information ******/
    //set the begin and max allowed parameters, to be passed to the DAOs.
    setBeginEndParameters(this.currentCSCIPagePosition, this.maxOrderItems);

    //call customerShoppingCartInfoResults which will call the DAO to retrieve the results
    HashMap customerShoppingCartInfoResults =
    retrieveCustomerShoppingCartInfo(this.startSearchingFrom, this.recordsToBeReturned);

    // check to see if there's an entry in the pi.partner_properties table for this origin
    // with a property of 'BYPASS_COM_SHOPPING_CART' with a value of 'Y'

    this.pageData.put(COMConstants.BYPASS_COM, "N");
      
    //get the value of OUT_NUMBER_OF_ORDERS
    long outIdCount = 0;
    if (customerShoppingCartInfoResults.get("OUT_NUMBER_OF_ORDERS") != null)
      outIdCount = Long.valueOf(customerShoppingCartInfoResults.get("OUT_NUMBER_OF_ORDERS").toString()).longValue();

    long outOrderPosition = 0;
    if (customerShoppingCartInfoResults.get("OUT_ORDER_POSITION") != null)
      outOrderPosition = Long.valueOf(customerShoppingCartInfoResults.get("OUT_ORDER_POSITION").toString()).longValue();

    //Set the navigation buttons
    setCSCIButtons(outIdCount, outOrderPosition, this.newPagePosition, this.maxOrderItems, "csci");

    //get the value of OUT_ORDER_FOUND
    String outOrderFound = customerShoppingCartInfoResults.get("OUT_ORDER_FOUND").toString();

    if (this.action.equalsIgnoreCase(COMConstants.ACTION_RECIP_ORD_NUM_SEARCH) &&
        outOrderFound.equalsIgnoreCase(COMConstants.CONS_NO))
    {
      this.pageData.put(COMConstants.PD_MESSAGE_DISPLAY, COMConstants.MSG_ORDER_NUM_NOT_EXIST);
    }
     
  }



/*******************************************************************************************
  * processCscNavigation()
  ******************************************************************************************
  * This method will process the action type of "csc_more" and "csc_previous".  Essentially,
  * this method provides navigational ability for when multiple external orders exist
  * for a master order number.
  *
  * @param  none
  * @return none
  * @throws none
  */
  private void processCscNavigation() throws Exception
  {

    /******Retrieve Customer Account Information ******/

    //set the begin and max allowed parameters, to be passed to the DAOs.
    setBeginEndParameters(0, this.maxCustomerAccountOrders);

    //call retrieveCustomerAccountInfo which will call the DAO to retrieve the results
    HashMap customerAccountInfoResults =
      retrieveCustomerAccountInfo(this.customerId, this.startSearchingFrom, this.recordsToBeReturned);

    setButtons(Long.valueOf(this.pageData.get(COMConstants.PD_CAI_NUMBER_OF_ORDERS).toString()).longValue(), 1, this.maxCustomerAccountOrders, "cai");
    this.pageData.put(COMConstants.PD_CAI_START_POSITION, "1");


    /******Retrieve Customer Shopping Cart Information ******/
    //set the new page position
    if (this.action.equalsIgnoreCase(COMConstants.ACTION_CSCI_PREVIOUS))
    {
      if (this.orderCSCIPositionOld > this.maxOrderItems)
      {
        this.startSearchingFrom = this.orderCSCIPositionOld - this.maxOrderItems;
        this.recordsToBeReturned = this.maxOrderItems;
      }
      else
      {
        this.newPagePosition = this.currentCSCIPagePosition - 1;

        //set the begin and max allowed parameters, to be passed to the DAOs.
        setBeginEndParameters(this.newPagePosition, this.maxOrderItems);
      }
    }
    else if (this.action.equalsIgnoreCase(COMConstants.ACTION_CSCI_NEXT))
    {
      this.startSearchingFrom = this.orderCSCIPositionOld + this.maxOrderItems;
      this.recordsToBeReturned = this.maxOrderItems;
    }

    //call customerShoppingCartInfoResults which will call the DAO to retrieve the results
    HashMap customerShoppingCartInfoResults =
      retrieveCustomerShoppingCartInfo(this.startSearchingFrom, this.recordsToBeReturned);

    //get the value of OUT_NUMBER_OF_ORDERS
    long outIdCount = 0;
    if (customerShoppingCartInfoResults.get("OUT_NUMBER_OF_ORDERS") != null)
      outIdCount = Long.valueOf(customerShoppingCartInfoResults.get("OUT_NUMBER_OF_ORDERS").toString()).longValue();

    long outOrderPosition = 0;
    if (customerShoppingCartInfoResults.get("OUT_ORDER_POSITION") != null)
      outOrderPosition = Long.valueOf(customerShoppingCartInfoResults.get("OUT_ORDER_POSITION").toString()).longValue();

    //Set the navigation buttons
    setCSCIButtons(outIdCount, outOrderPosition, this.newPagePosition, this.maxOrderItems, "csci");

  }



/*******************************************************************************************
  * processMoreCustomerEmails()
  ******************************************************************************************
  * This method will process the action type of "more_customer_emails"
  *
  * @param  none
  * @return none
  * @throws none
  */
  private void processMoreCustomerEmails() throws Exception
  {
    //set the begin and max allowed parameters, to be passed to the DAOs.
    setBeginEndParameters(this.currentCEAPagePosition, this.maxCustomerEmailAddresses);

    //call retrieveEmailInfo which will call the DAO to retrieve the results
    Map customerEmailResults = retrieveCustomerEmailInfo(this.startSearchingFrom, this.recordsToBeReturned);//retrieveEmailInfo(this.startSearchingFrom, this.recordsToBeReturned);

    this.newPagePosition = this.currentCEAPagePosition + 1;

    //get the value of OUT_ID_COUNT
    long outIdCount = 0;
    if (customerEmailResults.get("OUT_ID_COUNT") != null)
      outIdCount = Long.valueOf(customerEmailResults.get("OUT_ID_COUNT").toString()).longValue();

    //Set the navigation buttons
    setButtons(outIdCount, this.newPagePosition, this.maxCustomerEmailAddresses, "cea");
    this.pageData.put(COMConstants.PD_CEA_START_POSITION, String.valueOf(startSearchingFrom));


  }



/*******************************************************************************************
  * processCeaNavigation()
  ******************************************************************************************
  * This method will process the action type of "cea_first", "cea_previous", "cea_next" and
  * and "cea_last".  Essentially, this method provides navigational ability for when
  * multiple customer email accounts are found
  *
  * @param  none
  * @return none
  * @throws none
  */
  private void processCeaNavigation() throws Exception
  {
    //set the new page position
    if (this.action.equalsIgnoreCase(COMConstants.ACTION_CEA_FIRST_PAGE))
      this.newPagePosition = 1;
    else if (this.action.equalsIgnoreCase(COMConstants.ACTION_CEA_PREVIOUS_PAGE))
      this.newPagePosition = this.currentCEAPagePosition - 1;
    else if (this.action.equalsIgnoreCase(COMConstants.ACTION_CEA_NEXT_PAGE))
      this.newPagePosition = this.currentCEAPagePosition + 1;
    else if (this.action.equalsIgnoreCase(COMConstants.ACTION_CEA_LAST_PAGE))
      this.newPagePosition = this.totalCEAPages;

    //set the begin and max allowed parameters, to be passed to the DAOs.
    setBeginEndParameters(this.newPagePosition, this.maxCustomerEmailAddresses);

    //call retrieveEmailInfo which will call the DAO to retrieve the results
    Map customerEmailResults =
      retrieveCustomerEmailInfo(this.startSearchingFrom, this.recordsToBeReturned);//retrieveEmailInfo(this.startSearchingFrom, this.recordsToBeReturned);

    //get the value of OUT_ID_COUNT
    long outIdCount = 0;
    if (customerEmailResults.get("OUT_ID_COUNT") != null)
      outIdCount = Long.valueOf(customerEmailResults.get("OUT_ID_COUNT").toString()).longValue();

    //Set the navigation buttons
    setButtons(outIdCount, this.newPagePosition, this.maxCustomerEmailAddresses, "cea");
    this.pageData.put(COMConstants.PD_CEA_START_POSITION, String.valueOf(startSearchingFrom));

  }



/*******************************************************************************************
  * processRefreshIds()
  ******************************************************************************************
  * This method will process the action type of "refreshIds"
  *
  * @param  none
  * @return none
  * @throws none
  */
  private void processRefreshIds() throws Exception
  {
    Document csrViewing = DOMUtil.getDocument();

    //Call retrieveCSRViewed which will call the DAO to retrieve the results
    csrViewing = retrieveCSRViewing();
  }


/*******************************************************************************************
  * processSourceCodeLookup()
  ******************************************************************************************
  * This method will process the action type of
  *
  * @param  none
  * @return none
  * @throws none
  *
  */
  private void processSourceCodeLookup() throws Exception
  {
    Document sourceCodeInfo = DOMUtil.getDocument();

    //Call retrieveCSRViewed which will call the DAO to retrieve the results
    sourceCodeInfo = retrieveSourceCodeInfo();
  }


/*******************************************************************************************
  * processShoppingCartPaymentInfoLookup()
  ******************************************************************************************
  * This method will process the action type of
  *
  * @param  none
  * @return none
  * @throws none
  *
  */
  private void processShoppingCartPaymentInfoLookup() throws Exception
  {
    Document paymentInfo = DOMUtil.getDocument();

    String functionName = "cart";

    //Call retrieveCSRViewed which will call the DAO to retrieve the results
    paymentInfo = retrievePaymentInfo(functionName);
  }


/*******************************************************************************************
  * processRecipientPaymentInfoLookup()
  ******************************************************************************************
  * This method will process the action type of
  *
  * @param  none
  * @return none
  * @throws none
  *
  */
  private void processRecipientPaymentInfoLookup() throws Exception
  {
    Document paymentInfo = DOMUtil.getDocument();

    String functionName = "order";

    //Call retrieveCSRViewed which will call the DAO to retrieve the results
    paymentInfo = retrievePaymentInfo(functionName);
  }


/*******************************************************************************************
  * processMembershipInfoLookup()
  ******************************************************************************************
  * This method will process the action type of
  *
  * @param  none
  * @return none
  * @throws none
  *
  */
  private void processMembershipInfoLookup() throws Exception
  {
    Document membershipInfo = DOMUtil.getDocument();

    //Call retrieveCSRViewed which will call the DAO to retrieve the results
    membershipInfo = retrieveMembershipInfo();
  }



/*******************************************************************************************
  * processPrintOrder()
  ******************************************************************************************
  * This method will process the action type of
  *
  * @param  none
  * @return none
  * @throws none
  *
  */
  private void processPrintOrder() throws Exception
  {
    HashMap printOrderInfo = new HashMap();

    //Call retrieveOrderInfoForPrint which will call the DAO to retrieve the results
    printOrderInfo = retrieveOrderInfoForPrint();
  }


/*******************************************************************************************
  * processrRetrieveRefundLock()
  ******************************************************************************************
  * This method will process the action type of
  *
  * @param  none
  * @return none
  * @throws none
  *
  */
  private void processrRetrieveRefundLock() throws Exception
  {
    boolean refundHadProblems = false;

    Document lockInfo = DOMUtil.getDocument();

    //Call retrieveLockInfo which will call the DAO to retrieve the results
    lockInfo = retrieveLockInfo(this.entityId, this.entityType, this.orderLevel);

    //get the lock obtained indicator.
    String sLockedObtained = DOMUtil.getNodeValue(lockInfo, "OUT_LOCK_OBTAINED");

    //if locked_obtained = yes, you have the lock (regardless if you just established a lock
    //or have had it for a while).  Else, somebody else has the record locked
    if (sLockedObtained.equalsIgnoreCase(COMConstants.CONS_NO))
    {
      //get the locked csr id
      String sCsrId = DOMUtil.getNodeValue(lockInfo, "OUT_LOCKED_CSR_ID");

      //set an output message
      String message = COMConstants.MSG_RECORD_O_LOCKED1 + sCsrId + COMConstants.MSG_RECORD_O_LOCKED2;
      this.pageData.put(COMConstants.PD_REFUND_MESSAGE_DISPLAY + "1",    message);
      refundHadProblems = true;
    }


    this.pageData.put(COMConstants.PD_REFUND_CAN_BE_APPLIED,  refundHadProblems?COMConstants.CONS_NO:COMConstants.CONS_YES);
    this.pageData.put("XSL",                                  COMConstants.XSL_CHECK_REFUNDS_IFRAME);

  }



/*******************************************************************************************
  * processLoadRefundCodes()
  ******************************************************************************************
  * This method will process the action type of
  *
  * @param  none
  * @return none
  * @throws none
  *
  */
  private void processLoadRefundCodes() throws Exception
  {
    Document refundDispositionXML = DOMUtil.getDocument();
    Document responsiblePartyXML  = DOMUtil.getDocument();

    //Call retrieveRefundDisposition which will call the DAO to retrieve the results
    refundDispositionXML = retrieveRefundDispositionValues();

    //Call retrieveResponsibleParty which will call the DAO to retrieve the results
    responsiblePartyXML = retrieveResponsiblePartyValues();

    //set the XSL parameter, which governs the page to be loaded
    this.pageData.put("XSL",  COMConstants.XSL_REFUND_DISPLAY);
  }



/*******************************************************************************************
  * processRefundCart()
  ******************************************************************************************
  * This method will process the action type of
  *
  * @param  none
  * @return none
  * @throws none
  *
  */
  private void processRefundCart() throws Exception
  {
    //set the XSL parameter, which governs the page to be loaded
    this.pageData.put("XSL", COMConstants.XSL_REFUND_UPDATE_ACTION);
  }



/*******************************************************************************************
  * processCheckRefundStatus()
  ******************************************************************************************
  * This method will process the action type of
  *
  * @param  none
  * @return none
  * @throws none
  *
  */
  private void processCheckRefundStatus() throws Exception
  {
    int messageNumber = 1;
    boolean refundHadProblems = false;

    /** Get the lock info **/
    Document lockInfo = DOMUtil.getDocument();

    //Call retrieveLockInfo which will call the DAO to retrieve the results
    lockInfo = retrieveLockInfo(this.entityId, this.entityType, this.orderLevel);

    //get the lock obtained indicator.
    String sLockedObtained = DOMUtil.getNodeValue(lockInfo, "OUT_LOCK_OBTAINED");

    //if locked_obtained = yes, you have the lock (regardless if you just established a lock
    //or have had it for a while).  Else, somebody else has the record locked
    if (sLockedObtained.equalsIgnoreCase(COMConstants.CONS_NO))
    {
      //get the locked csr id
      String sCsrId = DOMUtil.getNodeValue(lockInfo, "OUT_LOCKED_CSR_ID");

      //set an output message
      String message = COMConstants.MSG_RECORD_O_LOCKED1 + sCsrId + COMConstants.MSG_RECORD_O_LOCKED2;
      this.pageData.put(COMConstants.PD_REFUND_MESSAGE_DISPLAY + messageNumber,    message);
      messageNumber++;
      refundHadProblems = true;
    }


    /** Get the cart info **/
    //Retrieve the refund cart info, to check if there were any erros.
    HashMap refundCartInfo = new HashMap();

    //Call retrieveRefundCartInfo which will call the DAO to retrieve the results
    refundCartInfo = retrieveRefundCartInfo();

    String hasRefundsInd          = (String) refundCartInfo.get("OUT_HAS_REFUNDS_IND");
    String inScrubInd             = (String) refundCartInfo.get("OUT_IN_SCRUB_IND");
    String allCancelledInd        = (String) refundCartInfo.get("OUT_ALL_CANCELLED_IND");
    String hasDeliveredPrintedInd = (String) refundCartInfo.get("OUT_DELIVERED_PRINTED_IND");
    String pastDeliveryDateInd    = (String) refundCartInfo.get("OUT_PAST_DEVLIVERY_DATE_IND");
    String hasLiveFtdInd          = (String) refundCartInfo.get("OUT_LIVE_FTD_IND");

    //check for Refund Indicator
    if (hasRefundsInd != null)
    {
      if (hasRefundsInd.equalsIgnoreCase(COMConstants.CONS_YES))
      {
        //set the Error message to be displayed
        this.pageData.put(COMConstants.PD_REFUND_MESSAGE_DISPLAY + messageNumber, COMConstants.MSG_REFUND_EXIST);
        messageNumber++;
        refundHadProblems = true;
      }
    }


    //check for Scrub Indicator
    if (inScrubInd != null)
    {
      if (inScrubInd.equalsIgnoreCase(COMConstants.CONS_YES))
      {
        //set the Error message to be displayed
        this.pageData.put(COMConstants.PD_REFUND_MESSAGE_DISPLAY + messageNumber, COMConstants.MSG_REFUND_ORDER_SCRUB);
        messageNumber++;
        refundHadProblems = true;
      }
    }

    String refundDispCodeParsed = Character.toString(this.refundDispCode.charAt(0));
    boolean bATypeRefund = refundDispCodeParsed.equalsIgnoreCase("a")?true:false;

    //check for Cancelled Indicator
    if (allCancelledInd != null && hasLiveFtdInd != null)
    {
      if (hasLiveFtdInd.equalsIgnoreCase(COMConstants.CONS_YES)
          && !allCancelledInd.equalsIgnoreCase(COMConstants.CONS_YES)
          && bATypeRefund)
      {
        //set the Error message to be displayed
        this.pageData.put(COMConstants.PD_REFUND_MESSAGE_DISPLAY + messageNumber, COMConstants.MSG_REFUND_A_TYPE_CANCELLED);
        messageNumber++;
        refundHadProblems = true;
      }
    }


    //check for Delivered Indicator
    if (hasDeliveredPrintedInd != null)
    {
      if (hasDeliveredPrintedInd.equalsIgnoreCase(COMConstants.CONS_YES) && bATypeRefund)
      {
        //set the Error message to be displayed
        this.pageData.put(COMConstants.PD_REFUND_MESSAGE_DISPLAY + messageNumber, COMConstants.MSG_REFUND_A_TYPE_DLVR_PRT);
        messageNumber++;
        refundHadProblems = true;
      }
    }


    //check for Delivery Date Indicator
    if (pastDeliveryDateInd != null)
    {
      if (pastDeliveryDateInd.equalsIgnoreCase(COMConstants.CONS_YES) && bATypeRefund)
      {
        //set the Error message to be displayed
        this.pageData.put(COMConstants.PD_REFUND_MESSAGE_DISPLAY + messageNumber, COMConstants.MSG_REFUND_A_TYPE_DATE);
        messageNumber++;
        refundHadProblems = true;
      }
    }


    if (refundHadProblems)
    {
      Document refundDispositionXML = DOMUtil.getDocument();
      Document responsiblePartyXML  = DOMUtil.getDocument();

      //Call retrieveRefundDisposition which will call the DAO to retrieve the results
      refundDispositionXML = retrieveRefundDispositionValues();

      //Call retrieveResponsibleParty which will call the DAO to retrieve the results
      responsiblePartyXML = retrieveResponsiblePartyValues();
    }


    this.pageData.put(COMConstants.PD_REFUND_CAN_BE_APPLIED,  refundHadProblems?COMConstants.CONS_NO:COMConstants.CONS_YES);
    //set the XSL parameter, which governs the page to be loaded
    this.pageData.put("XSL",  COMConstants.XSL_REFUND_DISPLAY);

  }


/*******************************************************************************************
  * processRefundReleaseLock()
  ******************************************************************************************
  * This method will process the action type of
  *
  * @param  none
  * @return none
  * @throws none
  *
  */
  private void processRefundReleaseLock() throws Exception
  {
    releaseLock();

    //set the XSL parameter, which governs the page to be loaded
    this.pageData.put("XSL",                              COMConstants.XSL_LOAD_USERS_IFRAME);

  }


/*******************************************************************************************
  * processCheckUpdateFlorist()
  ******************************************************************************************
  * This method will process the action type of
  *
  * @param  none
  * @return none
  * @throws none
  *
  */
  private void processCheckUpdateFlorist() throws Exception
  {
    /** Check if the order is found in the zip queue **/
    String queueId = retrieveQueueInfo();

    //check to see if the order is in Zip Q.  If yes, check for live FTD orders.
    //else display a message
    if (queueId != null && !queueId.equalsIgnoreCase(""))
    {
      //
      boolean hasLiveFTD = retrieveLiveInfo();

      //check to see if the order has Live FTD Orders.  If yes, check for live FTD orders.
      //else display a message
      if (hasLiveFTD)
      {
        this.pageData.put(COMConstants.PD_MESSAGE_DISPLAY,    COMConstants.MERCURY_ORDER_ALREADY_ASSIGNED);
      }
      else
      {
        this.pageData.put(COMConstants.PD_QUEUE_ID,           queueId);
        this.pageData.put("recip_zip_code", request.getParameter("recip_zip_code"));
      }
    }
    else
    {
      this.pageData.put(COMConstants.PD_MESSAGE_DISPLAY,      COMConstants.ORDER_NOT_IN_ZIP_Q);
    }

    this.pageData.put("XSL",  COMConstants.XSL_CHECK_UPDATE_FLORIST);

  }

/*******************************************************************************************
  * processLoadPrivacyPolicyVerification()
  ******************************************************************************************
  * This method will process the action type of load_privacy_policy
  *
  * @param  none
  * @return none
  * @throws none
  *
  */
  private void processLoadPrivacyPolicyVerification() throws Exception
  {
    //hashmap that will contain the output from the stored proce
    HashMap privacyPolicyVerificationResults = new HashMap();

    //Instantiate CustomerDAO
    CustomerDAO customerDAO = new CustomerDAO(this.con);
    OrderDAO oDAO = new OrderDAO(this.con);

    //Call SearchCustomers method in the DAO
    privacyPolicyVerificationResults = (HashMap)customerDAO.getPrivacyPolicy(this.customerId, this.orderDetailId);

    //Create an Document and call a method that will transform the CachedResultSet from the
    //store proce into an Document
    Document customerInfoXML = buildXML(privacyPolicyVerificationResults, "OUT_CUSTOMER_INFO", "PPV_CUSTOMER_INFO", "CUSTOMER_INFO");
    //and store the XML object in a hashXML hashmap
    this.hashXML.put(COMConstants.HK_PPV_CUSTOMER_INFO_XML, customerInfoXML);

    //Create an Document and call a method that will transform the CachedResultSet from the
    //store proce into an Document
    Document customerOrderInfoXML = buildXML(privacyPolicyVerificationResults, "OUT_CUSTOMER_ORDERS_INFO", "PPV_CUSTOMER_ORDERS_INFO", "CUSTOMER_ORDERS_INFO");
    //and store the XML object in a hashXML hashmap
    this.hashXML.put(COMConstants.HK_PPV_CUSTOMER_ORDER_INFO_XML, customerOrderInfoXML);

    //Create an Document and call a method that will transform the CachedResultSet from the
    //store proce into an Document
    Document orderInfoXML = buildXML(privacyPolicyVerificationResults, "OUT_ORDER_INFO", "PPV_ORDER_INFO", "ORDER_INFO");
    //and store the XML object in a hashXML hashmap
    this.hashXML.put(COMConstants.HK_PPV_ORDER_INFO_XML, orderInfoXML);

    //Create an Document and call a method that will transform the CachedResultSet from the
    //store proce into an Document
    Document phoneInfoXML = buildXML(privacyPolicyVerificationResults, "OUT_PHONE_INFO", "PPV_PHONE_INFO", "PHONE_INFO");
    //and store the XML object in a hashXML hashmap
    this.hashXML.put(COMConstants.HK_PPV_PHONE_INFO_XML, phoneInfoXML);

    //Create an Document and call a method that will transform the CachedResultSet from the
    //store proce into an Document
    Document ccInfoXML = buildXML(privacyPolicyVerificationResults, "OUT_CC_INFO", "PPV_CC_INFO", "CC_INFO");
    //and store the XML object in a hashXML hashmap
    this.hashXML.put(COMConstants.HK_PPV_CC_INFO_XML, ccInfoXML);

    //Create an Document and call a method that will transform the CachedResultSet from the
    //store proce into an Document
    Document privacyPolicyOptionCVXML = buildXML(privacyPolicyVerificationResults, "OUT_PRIVACY_POLICY_OPTION_CV", "PPV_OPTIONS_CV", "PPV_OPTION_CV");
    //and store the XML object in a hashXML hashmap
    this.hashXML.put(COMConstants.HK_PPV_OPTION_CV_XML, privacyPolicyOptionCVXML);

    //Create an Document and call a method that will transform the CachedResultSet from the
    //store proce into an Document
    Document privacyPolicyOptionOVXML = buildXML(privacyPolicyVerificationResults, "OUT_PRIVACY_POLICY_OPTION_OV", "PPV_OPTIONS_OV", "PPV_OPTION_OV");
    //and store the XML object in a hashXML hashmap
    this.hashXML.put(COMConstants.HK_PPV_OPTION_OV_XML, privacyPolicyOptionOVXML);

    //Create an Document and call a method that will transform the CachedResultSet from the
    //store proce into an Document
    Document privacyPolicyOptionLDXML = buildXML(privacyPolicyVerificationResults, "OUT_PRIVACY_POLICY_OPTION_LD", "PPV_OPTIONS_LD", "PPV_OPTION_LD");
    //and store the XML object in a hashXML hashmap
    this.hashXML.put(COMConstants.HK_PPV_OPTION_LD_XML, privacyPolicyOptionLDXML);

    //Create an Document and call a method that will transform the CachedResultSet from the
    //store proce into an Document
    Document privacyPolicyOptionVEXML = buildXML(privacyPolicyVerificationResults, "OUT_PRIVACY_POLICY_OPTION_VE", "PPV_OPTIONS_VE", "PPV_OPTION_VE");
    //and store the XML object in a hashXML hashmap
    this.hashXML.put(COMConstants.HK_PPV_OPTION_VE_XML, privacyPolicyOptionVEXML);

    String customerValidationMessage1 = oDAO.getContentWithFilter("PRIVACY_POLICY_VERIFICATION","CUSTOMER_VALIDATION_MESSAGE_1", null, null); 
    this.pageData.put("customer_validation_message_1", customerValidationMessage1);
    
    String customerValidationMessage2 = oDAO.getContentWithFilter("PRIVACY_POLICY_VERIFICATION","CUSTOMER_VALIDATION_MESSAGE_2", null, null); 
    this.pageData.put("customer_validation_message_2", customerValidationMessage2);
    
    String customerValidationMessage3 = oDAO.getContentWithFilter("PRIVACY_POLICY_VERIFICATION","CUSTOMER_VALIDATION_MESSAGE_3", null, null); 
    this.pageData.put("customer_validation_message_3", customerValidationMessage3);
    
    String errorNotEnoughMessage1 = oDAO.getContentWithFilter("PRIVACY_POLICY_VERIFICATION","ERROR_NOT_ENOUGH_MESSAGE_1", null, null); 
    this.pageData.put("error_not_enough_message_1", errorNotEnoughMessage1);
    
    String errorNotEnoughMessage2 = oDAO.getContentWithFilter("PRIVACY_POLICY_VERIFICATION","ERROR_NOT_ENOUGH_MESSAGE_2", null, null); 
    this.pageData.put("error_not_enough_message_2", errorNotEnoughMessage2);
    
    String errorNoInfoMessage1 = oDAO.getContentWithFilter("PRIVACY_POLICY_VERIFICATION","ERROR_NO_INFO_MESSAGE_1", null, null); 
    this.pageData.put("error_no_info_message_1", errorNoInfoMessage1);
    
    String errorNoInfoMessage2 = oDAO.getContentWithFilter("PRIVACY_POLICY_VERIFICATION","ERROR_NO_INFO_MESSAGE_2", null, null); 
    this.pageData.put("error_no_info_message_2", errorNoInfoMessage2);
    
    String orderValidationMessage1 = oDAO.getContentWithFilter("PRIVACY_POLICY_VERIFICATION","ORDER_VALIDATION_MESSAGE_1", null, null); 
    this.pageData.put("order_validation_message_1", orderValidationMessage1);
    
    String rememberMessage1 = oDAO.getContentWithFilter("PRIVACY_POLICY_VERIFICATION","REMEMBER_MESSAGE_1", null, null); 
    this.pageData.put("remember_message_1", rememberMessage1);
    
    String rememberMessage2 = oDAO.getContentWithFilter("PRIVACY_POLICY_VERIFICATION","REMEMBER_MESSAGE_2", null, null); 
    this.pageData.put("remember_message_2", rememberMessage2);
    
    this.pageData.put("XSL",  COMConstants.XSL_PRIVACY_POLICY_VERIFICATION);




  }


/*******************************************************************************************
  * processInsertPrivactyPolicyVerification()
  ******************************************************************************************
  * This method will process the action type of insert_privacy_policy
  *
  * @param  none
  * @return none
  * @throws none
  *
  */
  private void processInsertPrivactyPolicyVerification() throws Exception
  {
    //Instantiate CustomerDAO
    CustomerDAO customerDAO = new CustomerDAO(this.con);
    PrivacyPolicyOptionVO ppoVO;
    String checkboxValue; 
    
    //Call SearchCustomers method in the DAO and retrieve all options
    ArrayList privacyPolicyOptions = customerDAO.getPrivacyPolicyOptions();
    //(this.customerId, this.orderDetailId, this.csrId, this.timerCallLogId);

    //go thru all the options, and if the checkbox was checked, insert it in the database
    for (int i = 0 ; i < privacyPolicyOptions.size(); i++)
    {
      ppoVO = (PrivacyPolicyOptionVO) privacyPolicyOptions.get(i); 
      checkboxValue = this.request.getParameter(ppoVO.getPrivacyPolicyOptionCode()); 
      if (checkboxValue != null && checkboxValue.equalsIgnoreCase("on"))
      {
        //insert PPV record
        if (ppoVO.getPrivacyPolicyOptTypeDesc().equalsIgnoreCase("Customer Validation"))
          customerDAO.insertPrivacyPolicyVerification(this.customerId, null, this.csrId, this.timerCallLogId, ppoVO);
        else if (ppoVO.getPrivacyPolicyOptTypeDesc().equalsIgnoreCase("Validation Exception") && !ppoVO.getPrivacyPolicyOptionCode().equalsIgnoreCase("VENotCall"))
          customerDAO.insertPrivacyPolicyVerification(null, null, this.csrId, this.timerCallLogId, ppoVO);
        else
          customerDAO.insertPrivacyPolicyVerification(this.customerId, this.orderDetailId, this.csrId, this.timerCallLogId, ppoVO);
      }
    }
    
    this.pageData.put("XSL",                              COMConstants.XSL_INSERT_PRIVACY_POLICY_IFRAME);

  }


  public Document processCAMSService(String customerId) throws Exception {
	  try{
		  GlobalParmHandler gph = (GlobalParmHandler) CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_GLOBAL_PARM);
		  CustomerDAO customerDAO = new CustomerDAO(this.con);   
		  List<String> emailList = customerDAO.getEmailByCustomerId(customerId);
		  //Added this check to fix the defect number 13050.  
		  if (emailList == null || emailList.size() == 0)
		  {
			  return convertToDOM(null);
		  }
		  List<String> eventTypeList = new ArrayList<String>();
		  eventTypeList.add("fsm_order");
		  eventTypeList.add("fs_renewed");
		  eventTypeList.add("fs_member");
		  Map<String,List<CustomerFSHistory>> cfsmDetailsMap = new HashMap<String, List<CustomerFSHistory>>();
	
		  List<CustomerFSHistory> custFsmHistList = FTDCAMSUtils.getCustomerFSHistory(emailList, eventTypeList);  
		  
		  return convertToDOM(custFsmHistList);
	  }
	  catch(Exception e)
	  {
		  logger.error("Error occured while connection to CAMS", e);
		  Connection conn = DataSourceUtil.getInstance().getConnection("ORDER SCRUB");
		  SystemMessengerVO sysMessage = new SystemMessengerVO();
		  sysMessage.setLevel(SystemMessengerVO.LEVEL_PRODUCTION);
		  sysMessage.setSource("COM_CAMS_Service");
		  sysMessage.setType("System Exception");
		  sysMessage.setMessage("Error calling CAMS from CustomerOrderSearch->processCAMSService");
		  SystemMessenger.getInstance().send(sysMessage, conn);
		  return convertToDOM(null);
	  }
  }
  
  public Document convertToDOM(List<CustomerFSHistory> custFsHistoryList) throws Exception {
  	
	  DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
      factory.setIgnoringElementContentWhitespace(true);
      factory.setNamespaceAware(false);
      DocumentBuilder builder = factory.newDocumentBuilder();
      Document xmlDocument = builder.newDocument();
      
      Element rootElement = xmlDocument.createElement("SERVICES");
      xmlDocument.appendChild(rootElement);
      int i = 0;
      
      if(custFsHistoryList != null && custFsHistoryList.size() > 0) {
    	  CacheUtil cacheUtil = CacheUtil.getInstance();
          AccountProgramMasterVO acctProgramMasterVO = new AccountProgramMasterVO(); 
          acctProgramMasterVO = cacheUtil.getFSAccountProgramDetailsFromContent();  
          
    	  for (CustomerFSHistory customerFSHistory : custFsHistoryList) {
		
	       	  Element servicesInfo = xmlDocument.createElement("SERVICES_INFO");
		      servicesInfo.setAttribute("id",new Integer(++i).toString());
		      rootElement.appendChild(servicesInfo);
		        
		      Element programName = xmlDocument.createElement("email_address");
		      programName.appendChild(xmlDocument.createTextNode(customerFSHistory.getEmail()));
		      servicesInfo.appendChild(programName);
		       
		      Element displayName = xmlDocument.createElement("display_name");
		      displayName.appendChild(xmlDocument.createTextNode(acctProgramMasterVO.getDisplayName()));
		      servicesInfo.appendChild(displayName);
		  	        
		      Element accountprogramStatus = xmlDocument.createElement("account_program_status");
		      String programStatus = customerFSHistory.getFsMember();
		      if(programStatus.equals("1")) {
		        programStatus = "Active";
		  	  } else if(programStatus.equals("0")) {
		  	    programStatus = "Expired";
		  	  } else if(programStatus.equals("2")) {
		  		programStatus = "Cancelled";
		  	  }
		      
		  	  accountprogramStatus.appendChild(xmlDocument.createTextNode(programStatus));
		  	  servicesInfo.appendChild(accountprogramStatus);
		  	  
		  	  SimpleDateFormat fmt = new SimpleDateFormat("MM/dd/yyyy");
		  	  
		  	  if(customerFSHistory.getFsStartDate() != null){
		  		  Calendar sdCal = Calendar.getInstance();
			  	  sdCal.setTime(customerFSHistory.getFsStartDate());
			  	  sdCal.add(sdCal.HOUR, 1);
			  	  
			  	  Element startDate = xmlDocument.createElement("start_date");
			  	  startDate.appendChild(xmlDocument.createTextNode(fmt.format(sdCal.getTime())));
			  	  servicesInfo.appendChild(startDate);
		  	  }
		  	  
		  	  if(customerFSHistory.getFsEndDate() != null){
		  		  Calendar edCal = Calendar.getInstance();
			  	  edCal.setTime(customerFSHistory.getFsEndDate());
			  	  edCal.add(edCal.HOUR, 1);
			  	  
			  	  Element expirationDate = xmlDocument.createElement("expiration_date");
			  	  expirationDate.appendChild(xmlDocument.createTextNode(fmt.format(edCal.getTime())));
			  	  servicesInfo.appendChild(expirationDate);
		  	  }
		  	  
		  	  Element programUrl = xmlDocument.createElement("program_url");
		  	  programUrl.appendChild(xmlDocument.createTextNode(acctProgramMasterVO.getProgramUrl()));
		  	  servicesInfo.appendChild(programUrl);
		  	  		  	  
		  	  Element externalorderNumber = xmlDocument.createElement("external_order_number");
		  	  externalorderNumber.appendChild(xmlDocument.createTextNode(customerFSHistory.getFsmOrderNumber()));
		  	  servicesInfo.appendChild(externalorderNumber);
         }
      }
      DOMUtil.convertToString(xmlDocument);
      return xmlDocument;
  }

  private Document getServices() throws Exception{
    CustomerDAO customerDAO = new CustomerDAO(this.con);
    Document servicesInfo = DOMUtil.getDocument();
    servicesInfo = processCAMSService(this.customerId);
    
    return servicesInfo;
  }

/*******************************************************************************************
  * setBeginEndParameters();
  ******************************************************************************************
  * This method will utilize the inputs to compute and set class level variables.   These
  * variables will be used later as start and end position, to be send to the stored procs.
  *
  * @param1 long - current page position
  * @param2 long - # of records to display
  * @return none
  * @throws none
  */
  private void setBeginEndParameters(long currentPagePosition, long maxRecordsToBeDisplay)
  {
    if (currentPagePosition == 0)
    {
      this.startSearchingFrom = 1;
      //this.recordsToBeReturned = this.startSearchingFrom * numberOfRecordsToDisplay;
      this.recordsToBeReturned = maxRecordsToBeDisplay;
    }
    else
    {
      this.startSearchingFrom = (--currentPagePosition * maxRecordsToBeDisplay) + 1;
      //this.recordsToBeReturned = (currentcurrentPagePosition + 1) * maxRecordsToBeDisplay;
      this.recordsToBeReturned = maxRecordsToBeDisplay;
    }

  }



/*******************************************************************************************
 * RetrieveCustomerOrderSearchResults()
 *******************************************************************************************
  * This method is used to call the CustomerDAO.searchCustomer
  *
  * @param1 long - start position
  * @param2 long - end position
  * @return HashMap - map containing output from the stored procedure
  * @throws none
  */
  private HashMap retrieveCustomerOrderSearchResults(long startSearchingFrom, long recordsToBeReturned)
          throws Exception
  {
    //hashmap that will contain the output from the stored proce
    HashMap customerSearchResults = new HashMap();

    //Instantiate CustomerDAO
    CustomerDAO customerDAO = new CustomerDAO(this.con);

    //Call SearchCustomers method in the DAO
    customerSearchResults = (HashMap)customerDAO.searchCustomers(
                            this.searchCriteria, startSearchingFrom, recordsToBeReturned);

    //Create an Document and call a method that will transform the CachedResultSet from the
    //store proce into an Document
    Document searchCustomerXML =
          buildXML(customerSearchResults, "OUT_CUR", "SC_SEARCH_CUSTOMERS", "SC_SEARCH_CUSTOMER");
    //and store the XML object in a hashXML hashmap
    this.hashXML.put(COMConstants.HK_COS_CUSTOMER_ORDER_SEARCH_XML, searchCustomerXML);

    //Store the count in the pageData hashmap
    String outIdCount = "0";
    if (customerSearchResults.get("OUT_ID_COUNT") != null)
      outIdCount = customerSearchResults.get("OUT_ID_COUNT").toString();
    this.pageData.put(COMConstants.PD_COS_COUNT, outIdCount);

    return customerSearchResults;
  }



/*******************************************************************************************
 * retrieveCustomerAccountInfo()
 *******************************************************************************************
  * This method is used to call the CustomerDAO.getCustomerAcct
  *
  * @param1 String - customerId
  * @param2 long - start position
  * @param3 long - end position
  * @return HashMap - map containing output from the stored procedure
  * @throws Exception
  */
  private HashMap retrieveCustomerAccountInfo(String customerId, long startSearchingFrom, long recordsToBeReturned) throws Exception
  {
    //Instantiate CustomerDAO
    CustomerDAO customerDAO = new CustomerDAO(this.con);

    //hashmap that will contain the output from the stored proc
    HashMap customerAccountInfo = new HashMap();

    if(customerId == null || customerId.equalsIgnoreCase(""))
    {
      customerId = this.searchCriteria.get(COMConstants.SC_CUST_NUMBER).toString();
    }

    //Call getCustomerAcct method in the DAO
    customerAccountInfo = customerDAO.getCustomerAcct(this.searchCriteria, customerId, this.sessionId, this.csrId, startSearchingFrom, recordsToBeReturned);

    //Create an Document and call a method that will transform the CachedResultSet from the cursor:
    //OUT_CUSTOMER_CUR into an Document.
    Document caiCustomerXML =
        buildXML(customerAccountInfo, "OUT_CUSTOMER_CUR", "CAI_CUSTOMERS", "CAI_CUSTOMER");

    //Retrieve the value of Create Date
    ArrayList aList = new ArrayList();
    aList.add(0,"CAI_CUSTOMERS");
    aList.add(1,"CAI_CUSTOMER");
    aList.add(2,"customer_created_on");
    String sCreateDateOld = DOMUtil.getNodeValue(caiCustomerXML, aList);

    //Convert the string to timestamp
    Timestamp tCreateDate = Timestamp.valueOf(sCreateDateOld);
    //Define the format
    SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
    //Change the format
    String sCreateDateNew = sdf.format(tCreateDate);
    //Update the XML Document
    DOMUtil.updateNodeValue(caiCustomerXML, aList, sCreateDateNew);

    ArrayList bList = new ArrayList();
    bList.add(0,"CAI_CUSTOMERS");
    bList.add(1,"CAI_CUSTOMER");
    bList.add(2,"vip_customer");
    
    String outVipCustomer = DOMUtil.getNodeValue(caiCustomerXML, bList);
    
    //Store this XML object in hashXML HashMap
    this.hashXML.put(COMConstants.HK_CAI_CUSTOMER_XML,      caiCustomerXML);


    //Create an Document and call a method that will transform the CachedResultSet from the cursor:
    //OUT_PHONE_CUR into an Document.  Store this XML object in hashXML HashMap
    Document caiPhoneXML =
        buildXML(customerAccountInfo, "OUT_PHONE_CUR", "CAI_PHONES", "CAI_PHONE");
    this.hashXML.put(COMConstants.HK_CAI_PHONE_XML,        caiPhoneXML);

    //Create an Document and call a method that will transform the CachedResultSet from the cursor:
    //OUT_EMAIL_CUR into an Document.  Store this XML object in hashXML HashMap
    Document caiEmailXML =
        buildXML(customerAccountInfo, "OUT_EMAIL_CUR", "CAI_EMAILS", "CAI_EMAIL");
    this.hashXML.put(COMConstants.HK_CAI_EMAIL_XML,        caiEmailXML);

    //Create an Document and call a method that will transform the CachedResultSet from the cursor:
    //OUT_MEMBERSHIP_CUR into an Document.  Store this XML object in hashXML HashMap
    Document caiMembershipXML =
        buildXML(customerAccountInfo, "OUT_MEMBERSHIP_CUR", "CAI_MEMBERSHIPS", "CAI_MEMBERSHIP");
    this.hashXML.put(COMConstants.HK_CAI_MEMBERSHIP_XML,   caiMembershipXML);

    //Create an Document and call a method that will transform the CachedResultSet from the cursor:
    //OUT_ORDER_CUR into an Document.  Store this XML object in hashXML HashMap
    Document caiOrderXML =
        buildXML(customerAccountInfo, "OUT_ORDER_CUR", "CAI_ORDERS", "CAI_ORDER");
    this.hashXML.put(COMConstants.HK_CAI_ORDER_XML,        caiOrderXML);

    //Create an Document and call a method that will transform the CachedResultSet from the cursor:
    //OUT_HOLD_CUR into an Document.  Store this XML object in hashXML HashMap
    Document caiHoldXML =
        buildXML(customerAccountInfo, "OUT_HOLD_CUR", "CAI_HOLDS", "CAI_HOLD");
    this.hashXML.put(COMConstants.HK_CAI_HOLD_XML,         caiHoldXML);

    //Create an Document and call a method that will transform the CachedResultSet from the cursor:
    //OUT_VIEWING_CUR into an Document.  Store this XML object in hashXML HashMap
    Document caiViewingXML =
        buildXML(customerAccountInfo, "OUT_VIEWING_CUR", "CSR_VIEWINGS", "CSR_VIEWING");
    this.hashXML.put(COMConstants.HK_CAI_VIEWING_XML,      caiViewingXML);

    //Create an Document and call a method that will transform the CachedResultSet from the cursor:
    //OUT_DIRECT_MAIL_CUR into an Document.  Store this XML object in hashXML HashMap
    Document caiDirectMailXML =
        buildXML(customerAccountInfo, "OUT_DIRECT_MAIL_CUR", "CAI_DIRECT_MAILS", "CAI_DIRECT_MAIL");
    this.hashXML.put(COMConstants.HK_CAI_DIRECT_MAIL_XML,  caiDirectMailXML);

    //Store the customer services in the HashMap
    this.hashXML.put(COMConstants.HK_SERVICES_INFO_XML, getServices());
        
    //Store other data that the store proc is returning in the pageData HashMap
    String outNumberOfOrders  = "0";
    if (customerAccountInfo.get("OUT_NUMBER_OF_ORDERS") != null)
      outNumberOfOrders = customerAccountInfo.get("OUT_NUMBER_OF_ORDERS").toString();

    String outLockedInd       = null;
    if (customerAccountInfo.get("OUT_LOCKED_INDICATOR") != null)
      outLockedInd = customerAccountInfo.get("OUT_LOCKED_INDICATOR").toString();

    String outNumberOfCarts   = "0";
    if (customerAccountInfo.get("OUT_NUMBER_OF_CARTS") != null)
      outNumberOfCarts = customerAccountInfo.get("OUT_NUMBER_OF_CARTS").toString();

    String outMoreEmailInd    = null;
    if (customerAccountInfo.get("OUT_MORE_EMAIL_INDICATOR") != null)
      outMoreEmailInd = customerAccountInfo.get("OUT_MORE_EMAIL_INDICATOR").toString();

    this.pageData.put(COMConstants.PD_CAI_NUMBER_OF_ORDERS,     outNumberOfOrders);
    this.pageData.put(COMConstants.PD_CAI_VIP_CUSTOMER,     	outVipCustomer);
    this.pageData.put(COMConstants.PD_CAI_LOCKED_INDICATOR,     outLockedInd);
    this.pageData.put(COMConstants.PD_CAI_NUMBER_OF_CARTS,      outNumberOfCarts);
    this.pageData.put(COMConstants.PD_CAI_MORE_EMAIL_INDICATOR, outMoreEmailInd);
    
    // UC 24535 - Get all the customer E-mails and membership data.
	buildEmailMembershipData(customerDAO);

    return customerAccountInfo;

  }


/*******************************************************************************************
 * retrieveCustomerShoppingCartInfo()
 *******************************************************************************************
  * This method is used to call the OrderDAO.getCartInfo
  * Note that this method takes 4 input paramseters as opposed to the other
  * retrieveCustomerShoppingCartInfo(), which only takes 2 parameters.  This is because the
  * masterOrderNumber and orderGuid passed to this method is retrieved from another stored
  * proc, while the other retrieveCustomerShoppingCartInfo() retrieves these fields from the
  * request object.
  *
  * @param1 long - start position
  * @param2 long - end position
  * @param3 String - master order number
  * @param4 String - order guid
  * @return HashMap - map containing output from the stored procedure
  * @throws none
  */
  private HashMap retrieveCustomerShoppingCartInfo(long startSearchingFrom,
            long recordsToBeReturned, String orderNumber, String orderGuid)
            throws Exception
  {
    this.stopTimer = true;
    this.startTimer = true;

    //Instantiate OrderDAO
    OrderDAO orderDAO = new OrderDAO(this.con);

    //hashmap that will contain the output from the stored proc
    HashMap customerShoppingCartInfo = new HashMap();

    //Call getCartInfo method in the DAO
    customerShoppingCartInfo = orderDAO.getCartInfo(this.sessionId, this.csrId, orderNumber,
                                    orderGuid, startSearchingFrom, recordsToBeReturned);

    //Create an Document and call a method that will transform the CachedResultSet from the cursor:
    //OUT_CART_CUR into an Document.  Store this XML object in hashXML HashMap
    Document csciCartXML =
        buildXML(customerShoppingCartInfo, "OUT_CART_CUR", "CSCI_CARTS", "CSCI_CART");
    this.hashXML.put(COMConstants.HK_CSCI_CART_XML,           csciCartXML);

    String authResult = null;
    String ccUsed = null;
    String eodInd = null;
    String authOverride = null;
    String originId = null;
    String paymentType = null;
    String paymentTypeDesc = null;
    String paymentMethodId = null;
    String apAccountId = null;
    String apAuthTxt = null;
    String authNumber = null;
    String recipientCountry = null;
    
    if (csciCartXML != null)
    {
      String XPath = "//CSCI_CARTS/CSCI_CART";
      NodeList nodeList = DOMUtil.selectNodes(csciCartXML,XPath);
      Node node1 = null;
      if (nodeList != null)
      {
        for (int i = 0; i < nodeList.getLength(); i++)
        {
          node1 = (Node) nodeList.item(i);
          if (node1 != null)
          {
            if (DOMUtil.selectSingleNode(node1,"payment_auth_result/text()") != null)
            {
              authResult = (DOMUtil.selectSingleNode(node1,"payment_auth_result/text()").getNodeValue());
            }
            if (DOMUtil.selectSingleNode(node1,"credit_card_used/text()") != null)
            {
              ccUsed = (DOMUtil.selectSingleNode(node1,"credit_card_used/text()").getNodeValue());
            }
            if (DOMUtil.selectSingleNode(node1,"eod_indicator/text()") != null)
            {
              eodInd = (DOMUtil.selectSingleNode(node1,"eod_indicator/text()").getNodeValue());
            }
            if (DOMUtil.selectSingleNode(node1,"auth_override_flag/text()") != null)
            {
            authOverride = (DOMUtil.selectSingleNode(node1,"auth_override_flag/text()").getNodeValue());
            }
            if (DOMUtil.selectSingleNode(node1,"origin_id/text()") != null)
            {
                originId = (DOMUtil.selectSingleNode(node1,"origin_id/text()").getNodeValue());
                this.orderOrigin = originId;
            }
            if (DOMUtil.selectSingleNode(node1,"master_order_number/text()") != null)
            {
                this.masterOrderNum = (DOMUtil.selectSingleNode(node1,"master_order_number/text()").getNodeValue());
            }
            if (DOMUtil.selectSingleNode(node1,"source_code/text()") != null)
            {
                this.sourceCode = (DOMUtil.selectSingleNode(node1,"source_code/text()").getNodeValue());
            }
            if (DOMUtil.selectSingleNode(node1,"payment_type/text()") != null)
            {
                paymentType = (DOMUtil.selectSingleNode(node1,"payment_type/text()").getNodeValue());
            }
            if (DOMUtil.selectSingleNode(node1,"payment_type_desc/text()") != null)
            {
                paymentTypeDesc = (DOMUtil.selectSingleNode(node1,"payment_type_desc/text()").getNodeValue());
            }            
            if (DOMUtil.selectSingleNode(node1,"payment_method_id/text()") != null)
            {
                paymentMethodId = (DOMUtil.selectSingleNode(node1,"payment_method_id/text()").getNodeValue());
            }            
            if (DOMUtil.selectSingleNode(node1,"ap_account_id/text()") != null)
            {
                apAccountId = (DOMUtil.selectSingleNode(node1,"ap_account_id/text()").getNodeValue());
            }            
            if (DOMUtil.selectSingleNode(node1,"ap_auth_txt/text()") != null)
            {
                apAuthTxt = (DOMUtil.selectSingleNode(node1,"ap_auth_txt/text()").getNodeValue());
            }            
            if (DOMUtil.selectSingleNode(node1,"auth_number/text()") != null)
            {
              authNumber = (DOMUtil.selectSingleNode(node1,"auth_number/text()").getNodeValue());
            }            
          }
        }
      }
    }

    setJCPFlag( DOMUtil.getNodeText( csciCartXML, SOURCE_CODE_XPATH ) );

    //Retrieve the value of Order Guid
    ArrayList aList = new ArrayList();
    aList.add(0,"CSCI_CARTS");
    aList.add(1,"CSCI_CART");
    aList.add(2,"order_guid");
    this.startTimerOrderGuid = DOMUtil.getNodeValue(csciCartXML, aList);
    this.orderGuid = this.startTimerOrderGuid;


    //Re-Initialize the array with the top and bottom nodes of the XML, and the node for which
    //the value must be retrieved. In this instance, we will retireve the value of master_order_number
    aList.clear();
    aList.add(0,"CSCI_CARTS");
    aList.add(1,"CSCI_CART");
    aList.add(2,"master_order_number");
    this.masterOrderNumberNew = DOMUtil.getNodeValue(csciCartXML, aList);
    
    aList.clear();
    aList.add(0, "CSCI_CARTS");
    aList.add(1,"CSCI_CART");
    aList.add(2,"company_id");
    this.companyId = DOMUtil.getNodeValue(csciCartXML, aList);

    //if the CSR searched on an external order number, store that in page data
    if (  orderNumber != null                                             &&
          !orderNumber.equalsIgnoreCase("")                               &&
          !this.masterOrderNumberNew.equalsIgnoreCase(orderNumber))
    {
      this.searchedOnExtOrderNumber = orderNumber;
    }


    //Create an Document and call a method that will transform the CachedResultSet from the cursor:
    //OUT_CART_BILLS_CUR into an Document.  Store this XML object in hashXML HashMap
    Document csciCartBillXML =
        buildXML(customerShoppingCartInfo, "OUT_CART_BILLS_CUR", "CSCI_CART_BILLS", "CSCI_CART_BILL");
    this.hashXML.put(COMConstants.HK_CSCI_CART_BILLS_XML,     csciCartBillXML);

    // Document csciCartTaxesXML =
    //    buildXML(customerShoppingCartInfo, "OUT_CART_TAXES_CUR", "CSCI_CART_TAXES", "CSCI_CART_TAX");
    // this.hashXML.put("COMConstants.HK_CSCI_CART_TAXES_XML",     csciCartTaxesXML);
    
    Map<String, BigDecimal> taxTotals = calculateTaxTotals((CachedResultSet)customerShoppingCartInfo.get("OUT_ORDER_TAXES_CUR"));    
    if(taxTotals != null) {
    	Document csciCartTaxesXML = getCSCICartTaxesDoc(taxTotals);
    	if(csciCartTaxesXML != null) {
    		this.hashXML.put("COMConstants.HK_CSCI_CART_TAXES_XML", csciCartTaxesXML);
    	}
    }
    
    

    Document csciOrderTaxesXML =
        buildXML(customerShoppingCartInfo, "OUT_ORDER_TAXES_CUR", "CSCI_ORDER_TAXES", "CSCI_ORDER_TAX");
    this.hashXML.put("COMConstants.HK_CSCI_ORDER_TAXES_XML",     csciOrderTaxesXML);

    //Create an Document and call a method that will transform the CachedResultSet from the cursor:
    //OUT_CART_REFUNDS_CUR into an Document.  Store this XML object in hashXML HashMap
    Document csciCartRefundXML =
        buildXML(customerShoppingCartInfo, "OUT_CART_REFUNDS_CUR", "CSCI_CART_REFUNDS", "CSCI_CART_REFUND");
    this.hashXML.put(COMConstants.HK_CSCI_CART_REFUNDS_XML,   csciCartRefundXML);

    //Create an Document and call a method that will transform the CachedResultSet from the cursor:
    //OUT_CART_HOLD_CUR into an Document.  Store this XML object in hashXML HashMap
    Document csciCartHoldXML =
        buildXML(customerShoppingCartInfo, "OUT_CART_HOLD_CUR", "CSCI_CARTS_HOLD", "CSCI_CART_HOLD");
    this.hashXML.put(COMConstants.HK_CSCI_CART_HOLD_XML,      csciCartHoldXML);
    
      //Create an Document and call a method that will transform the CachedResultSet from the cursor:
      //OUT_CART_FEES_SAVED_CUR into an Document. 
      Document csciCartFeesXML =
          buildXML(customerShoppingCartInfo, "OUT_CART_FEES_SAVED_CUR", "CSCI_CART_FEES_SAVED", "CSCI_CART_FEE_SAVED");
      
      double cartFeesSaved = 0;
      
      if (csciCartFeesXML != null)
      {
        String XPath = "//CSCI_CART_FEES_SAVED/CSCI_CART_FEE_SAVED";
        NodeList nodeList = DOMUtil.selectNodes(csciCartFeesXML,XPath);
        Node node1 = null;
        if (nodeList != null)
        {
          for (int i = 0; i < nodeList.getLength(); i++)
          {
            node1 = (Node) nodeList.item(i);
            if (node1 != null)
            {
              if (DOMUtil.selectSingleNode(node1,"serv_ship_fee_saved/text()") != null)
              {
                String feesSaved = (DOMUtil.selectSingleNode(node1,"serv_ship_fee_saved/text()").getNodeValue());
                cartFeesSaved = Double.parseDouble(feesSaved);
              }
            }  
          }
         } 
       }
             
       //Check if the cart fees saved is greater than zero.  If it is, then add message to page data
       if(cartFeesSaved > 0)
       {
          ServicesProgramDAO spDAO = new ServicesProgramDAO();
          String cartFeesSavedMsg = spDAO.getFreeShippingCartSavingsMsg(this.con, cartFeesSaved);
          this.pageData.put("cart_fees_saved_message", cartFeesSavedMsg);
      }

    //Create an Document and call a method that will transform the CachedResultSet from the cursor:
    //OUT_ORDERS_CUR into an Document.  Store this XML object in hashXML HashMap
    Document csciOrderXML =
        buildXML(customerShoppingCartInfo, "OUT_ORDERS_CUR", "CSCI_ORDERS", "CSCI_ORDER");
    this.hashXML.put(COMConstants.HK_CSCI_ORDERS_XML,         csciOrderXML);
    if (csciOrderXML != null)
    {
      String XPath = "//CSCI_ORDERS/CSCI_ORDER";
      NodeList nodeList = DOMUtil.selectNodes(csciOrderXML,XPath);
      Node node1 = null;
      if (nodeList != null)
      {
        // Include order numbers in Call Disposition filter (as long as they're not in scrub)
        //
        for (int i = 0; i < nodeList.getLength(); i++)
        {
          node1 = (Node) nodeList.item(i);
          if (node1 != null)
          {
            if (DOMUtil.selectSingleNode(node1,"order_detail_id/text()") != null)
            {
                String scrubStatus = null;
                if (DOMUtil.selectSingleNode(node1,"scrub_status/text()") != null) {
                     scrubStatus = DOMUtil.selectSingleNode(node1,"scrub_status/text()").getNodeValue();
                }
                if (scrubStatus == null || StringUtils.isBlank(scrubStatus)) {
                    DecisionResultDataFilter.updateEntity(this.request, DOMUtil.selectSingleNode(node1,"order_detail_id/text()").getNodeValue());
                }
            }
            
            if(DOMUtil.selectSingleNode(node1, "country/text()") != null)
            	recipientCountry = DOMUtil.selectSingleNode(node1, "country/text()").getNodeValue();
            if(recipientCountry != null && recipientCountry.equalsIgnoreCase("ca")) 
            	this.canadianRecipient = true;
            
            //Phoenix changes
            String externalOrderNubmer = DOMUtil.selectSingleNode(node1,"external_order_number/text()").getNodeValue();
            PhoenixOrderBO phoenixBO = new PhoenixOrderBO(con);
            Element phoenixEl = csciOrderXML.createElement("IS_PHOENIX_ORDER");
            phoenixEl.setTextContent("N");    
    	    if(phoenixBO.isPhoenixOrder(externalOrderNubmer))
    	    {                	
            	phoenixEl.setTextContent("Y");                   
    	    }        	    	
    	    node1.appendChild(phoenixEl);     
            
          }
        }
      }
    }
    
    //Create an Document and call a method that will transform the CachedResultSet from the cursor:
    //OUT_ORDER_ADDON_CUR into an Document.  Store this XML object in hashXML HashMap
    Document csciOrderAddonXML =
        buildXML(customerShoppingCartInfo, "OUT_ORDER_ADDON_CUR", "CSCI_ORDER_ADDONS", "CSCI_ORDER_ADDON");
    this.hashXML.put(COMConstants.HK_CSCI_ORDER_ADDON_XML,    csciOrderAddonXML);

    //Create an Document and call a method that will transform the CachedResultSet from the cursor:
    //OUT_ORDERS_BILL_CUR into an Document.  Store this XML object in hashXML HashMap
    Document csciOrderBillXML =
        buildXML(customerShoppingCartInfo, "OUT_ORDER_BILLS_CUR", "CSCI_ORDER_BILLS", "CSCI_ORDER_BILL");
    this.hashXML.put(COMConstants.HK_CSCI_ORDER_BILLS_XML,    csciOrderBillXML);
    
        
    //Create an Document and call a method that will transform the CachedResultSet from the cursor:
    //OUT_ORDER_REFUNDS_CUR into an Document.  Store this XML object in hashXML HashMap
    Document csciOrderRefundXML =
        buildXML(customerShoppingCartInfo, "OUT_ORDER_REFUNDS_CUR", "CSCI_ORDER_REFUNDS", "CSCI_ORDER_REFUND");
    this.hashXML.put(COMConstants.HK_CSCI_ORDER_REFUNDS_XML,  csciOrderRefundXML);

    //Create an Document and call a method that will transform the CachedResultSet from the cursor:
    //OUT_VIEWING_CUR into an Document.  Store this XML object in hashXML HashMap
    Document csciViewingXML =
        buildXML(customerShoppingCartInfo, "OUT_VIEWING_CUR", "CSR_VIEWINGS", "CSR_VIEWING");
    this.hashXML.put(COMConstants.HK_CSCI_VIEWING_XML,        csciViewingXML);

    //Create an Document and call a method that will transform the CachedResultSet from the cursor:
    //OUT_CART_TOTAL_CUR into an Document.  Store this XML object in hashXML HashMap
    Document csciCartTotalXML =
        buildXML(customerShoppingCartInfo, "OUT_CART_TOTAL_CUR", "CSCI_CART_TOTALS", "CSCI_CART_TOTAL");
    this.hashXML.put(COMConstants.HK_CSCI_CART_TOTALS_XML,        csciCartTotalXML);

    //Create an Document and call a method that will transform the CachedResultSet from the cursor:
    //OUT_ORDER_TOTAL_CUR into an Document.  Store this XML object in hashXML HashMap
    Document csciOrderTotalXML =
        buildXML(customerShoppingCartInfo, "OUT_ORDER_TOTAL_CUR", "CSCI_ORDER_TOTALS", "CSCI_ORDER_TOTAL");
    this.hashXML.put(COMConstants.HK_CSCI_ORDER_TOTALS_XML,        csciOrderTotalXML);
    
    Document csciOrderFeesSavedXML =
        buildXML(customerShoppingCartInfo, "OUT_ORDER_FEES_SAVED_CUR", "CSCI_ORDER_FEES_SAVED", "CSCI_ORDER_FEE_SAVED");
    this.hashXML.put(COMConstants.HK_CSCI_ORDER_FEES_SAVED_XML,  csciOrderFeesSavedXML);

    if (null == this.orderDetailId) 
    {
      Document orderBillsXml = (Document) this.hashXML.get(COMConstants.HK_CSCI_ORDER_BILLS_XML);
      
      if (null != orderBillsXml)
      { 
        Node orderDetailIdNode = DOMUtil.selectSingleNode(orderBillsXml,"//order_detail_id/text()");
        
        if (null != orderDetailIdNode)
        {
          this.orderDetailId = orderDetailIdNode.getNodeValue();
        }
      }
    }
    
    if (null != this.orderDetailId && !"".equals(this.orderDetailId)) 
    {
      //Retrieve an Document.  Store this XML object in hashXML HashMap
      Document csciCSRTaggedXML = this.retrieveCSRTagged(this.orderDetailId);
      this.hashXML.put(COMConstants.HK_CSCI_CSR_TAGGED_XML, csciCSRTaggedXML);
    }
    
    //Store other data that the store proc is returning in the pageData HashMap
    String outNumberOfOrders      = "0";
    if (customerShoppingCartInfo.get("OUT_NUMBER_OF_ORDERS") != null)
      outNumberOfOrders = customerShoppingCartInfo.get("OUT_NUMBER_OF_ORDERS").toString();

    String outOrderPosition       = "0";
    if (customerShoppingCartInfo.get("OUT_ORDER_POSITION") != null)
      outOrderPosition = customerShoppingCartInfo.get("OUT_ORDER_POSITION").toString();

    String outOrderInd            = null;
    if (customerShoppingCartInfo.get("OUT_ORDER_INDICATOR") != null)
      outOrderInd = customerShoppingCartInfo.get("OUT_ORDER_INDICATOR").toString();

    String outOrderFound          = null;
    if (customerShoppingCartInfo.get("OUT_ORDER_FOUND") != null)
      outOrderInd = customerShoppingCartInfo.get("OUT_ORDER_FOUND").toString();

    this.pageData.put(COMConstants.PD_CSCI_NUMBER_OF_ORDERS,  outNumberOfOrders);
    this.pageData.put(COMConstants.PD_CSCI_ORDER_POSITION,    outOrderPosition);
    this.pageData.put(COMConstants.PD_CSCI_ORDER_INDICATOR,   outOrderInd);
    this.pageData.put(COMConstants.PD_CSCI_ORDER_FOUND,       outOrderInd);
    this.pageData.put("payment_type_desc", paymentTypeDesc);
    
    //Check if the original payment type was Alt Pay
    if(paymentType != null && paymentType.equals(COMConstants.CONS_ALTPAY_PAYMENT_TYPE))
      this.pageData.put("isAltPay", "Y");
    else
      this.pageData.put("isAltPay", "N");
      
    //Check if the original payment was miles/points
    if (FTDCommonUtils.isPaymentMilesPoints(paymentMethodId))
        this.pageData.put("isMilesPoints", "Y");
    else 
        this.pageData.put("isMilesPoints", "N");            

    //check the authorization result to see if the cart payment is authorized.
    //if it is, do not allow any modifications to it.
    //For Alternate Payment:  PayPal and BML will have the apAuthTxt 
    //populated when authorized and United Mileage Plus will have the authNumber 
    //populated when authorized.  Technically PayPal and BML should always
    //be authorized.
    if( (ccUsed != null && ccUsed.equalsIgnoreCase("Y") &&
       ((authOverride !=null && authOverride.equalsIgnoreCase("Y") &&
       eodInd !=null && eodInd.equalsIgnoreCase("N")) ||
       authResult !=null && authResult.equalsIgnoreCase("D") ||
			 authResult == null)) ||
        (apAccountId != null && (apAuthTxt == null && authNumber == null)) )
       this.pageData.put("isAuthorized","false");
    else
      this.pageData.put("isAuthorized","true");

    //Check the origin ID for Walmart and Amazon and Mercent and Partner Orders
    //If these orders are found, restrict them from going to the 
    //Update Order and Update Payment screens.
    MercentOrderPrefixes mrcntOrderPrefixes = new MercentOrderPrefixes(this.con);
    PartnerMappingVO partnerMappingVO = null;
    if(!StringUtils.isEmpty(originId) && sourceCode != null) {
		partnerMappingVO = new PartnerUtility().getPartnerOriginsInfo(originId,sourceCode, con);
    }else{
    	logger.info("OriginId or sourceCode is null. Could not get the Partner mapping");
    }   
    
    if(mrcntOrderPrefixes.isMercentOrder(originId)) {
    	this.pageData.put("partnerName","Mercent");
    	this.pageData.put("partnerChannelIcon",mrcntOrderPrefixes.getMercentIconForChannel(originId));
    	this.pageData.put("partnerChannelName",mrcntOrderPrefixes.getChannelName(originId));
        ConfigurationUtil cu = ConfigurationUtil.getInstance();
        String imageUrl = cu.getFrpGlobalParm(COMConstants.JOE_CONTEXT, COMConstants.IMAGE_SERVER_URL);
        this.pageData.put("imageServerUrl", imageUrl);
    } else if (partnerMappingVO != null && partnerMappingVO.isDefaultBehaviour()) {
		this.pageData.put("partnerName","Partner");
    	this.pageData.put("partnerChannelIcon",partnerMappingVO.getPartnerImage());
    	this.pageData.put("partnerChannelName",partnerMappingVO.getPartnerName());		
        ConfigurationUtil cu = ConfigurationUtil.getInstance();
        String imageUrl = cu.getFrpGlobalParm(COMConstants.JOE_CONTEXT, COMConstants.IMAGE_SERVER_URL);
        this.pageData.put("imageServerUrl", imageUrl);
        this.pageData.put("modifyOrderAllowed", partnerMappingVO.isModifyOrderAllowed());
    }
    
    if(originId != null && (originId.equalsIgnoreCase("WLMTI") ||
       originId.equalsIgnoreCase("AMZNI")) || mrcntOrderPrefixes.isMercentOrder(originId) || (partnerMappingVO != null && partnerMappingVO.isDefaultBehaviour()))
       this.pageData.put("isRestricted","true");
    else
       this.pageData.put("isRestricted","false");
        
    
    if(!"FTDCA".equalsIgnoreCase(this.companyId) && this.canadianRecipient){
  	  String surchargeExplanation  = ConfigurationUtil.getInstance().
												getContent(con, "TAX", "TAX_EXPLANATION");
  	  this.pageData.put("surchargeExplanation", surchargeExplanation);
    }
    
    boolean freeShippingMembershipFlag = this.isOrderFreeShippingMembership(this.orderDetailId, con);
    logger.debug("freeShippingMembershipFlag="+freeShippingMembershipFlag);
    boolean hasExpiredAccountProgram = false;
    String programName = null;
    String premierCircleMembership ="";
    CachedResultSet pcrs = orderDAO.getPremierCircleInfo(this.orderGuid);
    if(pcrs.next())
    {
      premierCircleMembership = pcrs.getString("pc_flag");
    }
    if(freeShippingMembershipFlag)
    {
    	Boolean flag = false;
    	Date now = new Date();
    	Calendar calendar = Calendar.getInstance();
		calendar.setTime(now);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		now = calendar.getTime();
    	String buyerEmailAddress="";
    	CachedResultSet rs = orderDAO.getOrderCustomerInfo(orderDetailId);
        if(rs.next())
        {
        	buyerEmailAddress = rs.getString("email_address");
        }
        if(buyerEmailAddress != null)
        {
        	List<String> emailList = new ArrayList<String>();
        	emailList.add(buyerEmailAddress);
        	Map<String, MembershipDetailsVO> membershipMap = FTDCAMSUtils.getMembershipData(emailList, now);
        	if(membershipMap == null){
        		flag = null;		
        	}else{
        		if(membershipMap.containsKey(FTDCAMSUtils.FREESHIPPING_MEMBER_TYPE)){
        			MembershipDetailsVO fsMemberDetails = membershipMap.get(FTDCAMSUtils.FREESHIPPING_MEMBER_TYPE);
        			if(fsMemberDetails != null){
        				flag = fsMemberDetails.isMembershipBenefitsApplicable();
        			}else{
        				flag = false;
        			}
        		}else{
        			flag = false;
        		}
        		
        	}
        	if(flag != null && flag == true)
        	{
        		hasExpiredAccountProgram = false;
        	}
        	else
        	{
        		hasExpiredAccountProgram = true;
        		AccountProgramMasterVO accountProgramMasterVO = CacheUtil.getInstance().getFSAccountProgramDetailsFromContent();
        		programName = accountProgramMasterVO.getProgramName();
        	}
        }
        else
        {
        	hasExpiredAccountProgram = true;
        }
    }
    logger.debug("hasExpiredAccountProgram="+hasExpiredAccountProgram);
    if(hasExpiredAccountProgram)
	{
		this.pageData.put("isAccountProgramExpired","true");
		this.pageData.put("programName",programName);
	}
    else
    {
    	this.pageData.put("isAccountProgramExpired","false");
    }
    if(premierCircleMembership!=null && premierCircleMembership.equalsIgnoreCase("Y")){
    	this.pageData.put("premierCircleMembership","true");
    }
    else{
    	this.pageData.put("premierCircleMembership","false");
    }
    //set the XSL parameter, which governs the page to be loaded
    this.pageData.put("XSL",  COMConstants.XSL_CUSTOMER_SHOPPING_CART);
    
    return customerShoppingCartInfo;
  }



/*******************************************************************************************
 * retrieveCustomerShoppingCartInfo()
 *******************************************************************************************
  * This method is used to call the OrderDAO.getCartInfo
  * Note that this method takes 2 input paramseters as opposed to the other
  * retrieveCustomerShoppingCartInfo(), which takes 4 parameters.  This is because the
  * masterOrderNumber and orderGuid in this method are retrieved from the request object,
  * whereas in the other method, the fields are passed after retrieving them from another stored
  * proc.
  *
  * @param1 long - start position
  * @param2 long - end position
  * @return HashMap - map containing output from the stored procedure
  * @throws none
  */
  private HashMap retrieveCustomerShoppingCartInfo(long startSearchingFrom, long recordsToBeReturned)
          throws Exception
  {
    this.stopTimer = true;
    this.startTimer = true;

    //Instantiate OrderDAO
    OrderDAO orderDAO = new OrderDAO(this.con);

    //hashmap that will contain the output from the stored proc
    HashMap customerShoppingCartInfo = new HashMap();
    //Call getCartInfo method in the DAO
    customerShoppingCartInfo = orderDAO.getCartInfo(this.sessionId, this.csrId, this.orderNumber,
                                    this.orderGuid, startSearchingFrom, recordsToBeReturned);

    //Create an Document and call a method that will transform the CachedResultSet from the cursor:
    //OUT_CART_CUR into an Document.  Store this XML object in hashXML HashMap
    Document csciCartXML =
        buildXML(customerShoppingCartInfo, "OUT_CART_CUR", "CSCI_CARTS", "CSCI_CART");
    this.hashXML.put(COMConstants.HK_CSCI_CART_XML,           csciCartXML);

    String authResult = null;
    String ccUsed = null;
    String eodInd = null;
    String authOverride = null;
    String originId = null;
    String paymentType = null;
    String paymentMethodId = null;
    String apAccountId = null;
    String apAuthTxt = null;
    String authNumber = null;
    String recipientCountry = null;

    if (csciCartXML != null)
    {
      String XPath = "//CSCI_CARTS/CSCI_CART";
      NodeList nodeList = DOMUtil.selectNodes(csciCartXML,XPath);
      Node node1 = null;
      if (nodeList != null)
      {
        for (int i = 0; i < nodeList.getLength(); i++)
        {
          node1 = (Node) nodeList.item(i);
          if (node1 != null)
          {
            if (DOMUtil.selectSingleNode(node1,"payment_auth_result/text()") != null)
            {
                authResult = (DOMUtil.selectSingleNode(node1,"payment_auth_result/text()").getNodeValue());
            }
            if (DOMUtil.selectSingleNode(node1,"credit_card_used/text()") != null)
            {
                ccUsed = (DOMUtil.selectSingleNode(node1,"credit_card_used/text()").getNodeValue());
            }
            if (DOMUtil.selectSingleNode(node1,"eod_delivery_indicator/text()") != null)
            {
                eodInd = (DOMUtil.selectSingleNode(node1,"eod_delivery_indicator/text()").getNodeValue());
            }
            if (DOMUtil.selectSingleNode(node1,"auth_override_flag/text()") != null)
            {
                authOverride = (DOMUtil.selectSingleNode(node1,"auth_override_flag/text()").getNodeValue());
            }
            if (DOMUtil.selectSingleNode(node1,"origin_id/text()") != null)
            {
                originId = (DOMUtil.selectSingleNode(node1,"origin_id/text()").getNodeValue());
                this.orderOrigin = originId;
            }
            if (DOMUtil.selectSingleNode(node1,"master_order_number/text()") != null)
            {
                this.masterOrderNum = (DOMUtil.selectSingleNode(node1,"master_order_number/text()").getNodeValue());
            }
            if (DOMUtil.selectSingleNode(node1,"payment_type/text()") != null)
            {
                paymentType = (DOMUtil.selectSingleNode(node1,"payment_type/text()").getNodeValue());
            }
            if (DOMUtil.selectSingleNode(node1,"payment_method_id/text()") != null)
            {
              paymentMethodId = (DOMUtil.selectSingleNode(node1,"payment_method_id/text()").getNodeValue());
            }            
            if (DOMUtil.selectSingleNode(node1,"ap_account_id/text()") != null)
            {
              apAccountId = (DOMUtil.selectSingleNode(node1,"ap_account_id/text()").getNodeValue());
            }            
            if (DOMUtil.selectSingleNode(node1,"ap_auth_txt/text()") != null)
            {
              apAuthTxt = (DOMUtil.selectSingleNode(node1,"ap_auth_txt/text()").getNodeValue());
            }            
            if (DOMUtil.selectSingleNode(node1,"ap_auth_number/text()") != null)
            {
            apAuthTxt = (DOMUtil.selectSingleNode(node1,"ap_auth_number/text()").getNodeValue());
            }
            if (DOMUtil.selectSingleNode(node1,"source_code/text()") != null)
            {
              this.sourceCode = (DOMUtil.selectSingleNode(node1,"source_code/text()").getNodeValue());
            }
          }
        }
      }
    }

    setJCPFlag( DOMUtil.getNodeText( csciCartXML, SOURCE_CODE_XPATH ) );

    //Retrieve the value of Order Guid
    ArrayList aList = new ArrayList();
    aList.add(0,"CSCI_CARTS");
    aList.add(1,"CSCI_CART");
    aList.add(2,"order_guid");
    this.startTimerOrderGuid = DOMUtil.getNodeValue(csciCartXML, aList);
    this.orderGuid = this.startTimerOrderGuid;

    //Re-Initialize the array with the top and bottom nodes of the XML, and the node for which
    //the value must be retrieved. In this instance, we will retireve the value of master_order_number
    aList.clear();
    aList.add(0,"CSCI_CARTS");
    aList.add(1,"CSCI_CART");
    aList.add(2,"master_order_number");
    this.masterOrderNumberNew = DOMUtil.getNodeValue(csciCartXML, aList);

    aList.clear();
    aList.add(0, "CSCI_CARTS");
    aList.add(1,"CSCI_CART");
    aList.add(2,"company_id");
    this.companyId = DOMUtil.getNodeValue(csciCartXML, aList);

    //if the CSR searched on an external order number, store that in page data
    if (  this.orderNumber != null                                        &&
          !this.orderNumber.equalsIgnoreCase("")                          &&
          !this.masterOrderNumberNew.equalsIgnoreCase(this.orderNumber))
    {
      this.searchedOnExtOrderNumber = this.orderNumber;
    }

    //Create an Document and call a method that will transform the CachedResultSet from the cursor:
    //OUT_CART_BILLS_CUR into an Document.  Store this XML object in hashXML HashMap
    Document csciCartBillXML =
        buildXML(customerShoppingCartInfo, "OUT_CART_BILLS_CUR", "CSCI_CART_BILLS", "CSCI_CART_BILL");
    this.hashXML.put(COMConstants.HK_CSCI_CART_BILLS_XML,     csciCartBillXML);

    //Create an Document and call a method that will transform the CachedResultSet from the cursor:
    //OUT_CART_REFUNDS_CUR into an Document.  Store this XML object in hashXML HashMap
    Document csciCartRefundXML =
        buildXML(customerShoppingCartInfo, "OUT_CART_REFUNDS_CUR", "CSCI_CART_REFUNDS", "CSCI_CART_REFUND");
    this.hashXML.put(COMConstants.HK_CSCI_CART_REFUNDS_XML,   csciCartRefundXML);

    //Create an Document and call a method that will transform the CachedResultSet from the cursor:
    //OUT_CART_HOLD_CUR into an Document.  Store this XML object in hashXML HashMap
    Document csciCartHoldXML =
        buildXML(customerShoppingCartInfo, "OUT_CART_HOLD_CUR", "CSCI_CARTS_HOLD", "CSCI_CART_HOLD");
    this.hashXML.put(COMConstants.HK_CSCI_CART_HOLD_XML,      csciCartHoldXML);
    
    //Create an Document and call a method that will transform the CachedResultSet from the cursor:
    //OUT_CART_FEES_SAVED_CUR into an Document. 
    Document csciCartFeesXML =
        buildXML(customerShoppingCartInfo, "OUT_CART_FEES_SAVED_CUR", "CSCI_CART_FEES_SAVED", "CSCI_CART_FEE_SAVED");
    
    double cartFeesSaved = 0;
    
    if (csciCartFeesXML != null)
    {
      String XPath = "//CSCI_CART_FEES_SAVED/CSCI_CART_FEE_SAVED";
      NodeList nodeList = DOMUtil.selectNodes(csciCartFeesXML,XPath);
      Node node1 = null;
      if (nodeList != null)
      {
        for (int i = 0; i < nodeList.getLength(); i++)
        {
          node1 = (Node) nodeList.item(i);
          if (node1 != null)
          {
            if (DOMUtil.selectSingleNode(node1,"serv_ship_fee_saved/text()") != null)
            {
              String feesSaved = (DOMUtil.selectSingleNode(node1,"serv_ship_fee_saved/text()").getNodeValue());
              cartFeesSaved = Double.parseDouble(feesSaved);
            }
          }  
        }
       } 
     }
           
     //Check if the cart fees saved is greater than zero.  If it is, then add message to page data
     if(cartFeesSaved > 0)
     {
          ServicesProgramDAO spDAO = new ServicesProgramDAO();
          String cartFeesSavedMsg = spDAO.getFreeShippingCartSavingsMsg(this.con, cartFeesSaved);
          this.pageData.put("cart_fees_saved_message", cartFeesSavedMsg);
     }


    //Create an Document and call a method that will transform the CachedResultSet from the cursor:
    //OUT_ORDERS_CUR into an Document.  Store this XML object in hashXML HashMap
    Document csciOrderXML =
        buildXML(customerShoppingCartInfo, "OUT_ORDERS_CUR", "CSCI_ORDERS", "CSCI_ORDER");
    this.hashXML.put(COMConstants.HK_CSCI_ORDERS_XML,         csciOrderXML);
    if (csciOrderXML != null)
    {
      String XPath = "//CSCI_ORDERS/CSCI_ORDER";
      NodeList nodeList = DOMUtil.selectNodes(csciOrderXML,XPath);
      Node node1 = null;
      if (nodeList != null)
      {
        for (int i = 0; i < nodeList.getLength(); i++)
        {
          node1 = (Node) nodeList.item(i);
          if (node1 != null)
          {
            if (DOMUtil.selectSingleNode(node1,"order_detail_id/text()") != null)
            {
                String scrubStatus = null;
                if (DOMUtil.selectSingleNode(node1,"scrub_status/text()") != null) {
                     scrubStatus = DOMUtil.selectSingleNode(node1,"scrub_status/text()").getNodeValue();
                }
                if (scrubStatus == null || StringUtils.isBlank(scrubStatus)) {
                    DecisionResultDataFilter.updateEntity(this.request, DOMUtil.selectSingleNode(node1,"order_detail_id/text()").getNodeValue());
                }                   
            }
            
            if(DOMUtil.selectSingleNode(node1, "country/text()") != null)
            	 recipientCountry = DOMUtil.selectSingleNode(node1, "country/text()").getNodeValue();
            if (recipientCountry != null && recipientCountry.equalsIgnoreCase("ca"))
                this.canadianRecipient = true;                
            
            //Phoenix changes
            String externalOrderNubmer = DOMUtil.selectSingleNode(node1,"external_order_number/text()").getNodeValue();
            PhoenixOrderBO phoenixBO = new PhoenixOrderBO(con);
            Element phoenixEl = csciOrderXML.createElement("IS_PHOENIX_ORDER");
            phoenixEl.setTextContent("N");    
    	    if(phoenixBO.isPhoenixOrder(externalOrderNubmer))
    	    {                	
            	phoenixEl.setTextContent("Y");                   
    	    }        	    	
    	    node1.appendChild(phoenixEl);  
            
          }
        }
      }
    }

    //Create an Document and call a method that will transform the CachedResultSet from the cursor:
    //OUT_ORDER_ADDON_CUR into an Document.  Store this XML object in hashXML HashMap
    Document csciOrderAddonXML =
        buildXML(customerShoppingCartInfo, "OUT_ORDER_ADDON_CUR", "CSCI_ORDER_ADDONS", "CSCI_ORDER_ADDON");
    this.hashXML.put(COMConstants.HK_CSCI_ORDER_ADDON_XML,    csciOrderAddonXML);

    //Create an Document and call a method that will transform the CachedResultSet from the cursor:
    //OUT_ORDERS_BILL_CUR into an Document.  Store this XML object in hashXML HashMap
    Document csciOrderBillXML =
        buildXML(customerShoppingCartInfo, "OUT_ORDER_BILLS_CUR", "CSCI_ORDER_BILLS", "CSCI_ORDER_BILL");
    this.hashXML.put(COMConstants.HK_CSCI_ORDER_BILLS_XML,    csciOrderBillXML);

    //Create an Document and call a method that will transform the CachedResultSet from the cursor:
    //OUT_ORDER_REFUNDS_CUR into an Document.  Store this XML object in hashXML HashMap
    Document csciOrderRefundXML =
        buildXML(customerShoppingCartInfo, "OUT_ORDER_REFUNDS_CUR", "CSCI_ORDER_REFUNDS", "CSCI_ORDER_REFUND");
    this.hashXML.put(COMConstants.HK_CSCI_ORDER_REFUNDS_XML,  csciOrderRefundXML);

    //Create an Document and call a method that will transform the CachedResultSet from the cursor:
    //OUT_VIEWING_CUR into an Document.  Store this XML object in hashXML HashMap
    Document csciViewingXML =
        buildXML(customerShoppingCartInfo, "OUT_VIEWING_CUR", "CSR_VIEWINGS", "CSR_VIEWING");
    this.hashXML.put(COMConstants.HK_CSCI_VIEWING_XML,        csciViewingXML);

    //Create an Document and call a method that will transform the CachedResultSet from the cursor:
    //OUT_CART_TOTAL_CUR into an Document.  Store this XML object in hashXML HashMap
    Document csciCartTotalXML =
        buildXML(customerShoppingCartInfo, "OUT_CART_TOTAL_CUR", "CSCI_CART_TOTALS", "CSCI_CART_TOTAL");
    this.hashXML.put(COMConstants.HK_CSCI_CART_TOTALS_XML,        csciCartTotalXML);

    //Create an Document and call a method that will transform the CachedResultSet from the cursor:
    //OUT_ORDER_TOTAL_CUR into an Document.  Store this XML object in hashXML HashMap
    Document csciOrderTotalXML =
        buildXML(customerShoppingCartInfo, "OUT_ORDER_TOTAL_CUR", "CSCI_ORDER_TOTALS", "CSCI_ORDER_TOTAL");
    this.hashXML.put(COMConstants.HK_CSCI_ORDER_TOTALS_XML,        csciOrderTotalXML);

    Document csciOrderFeesSavedXML =
        buildXML(customerShoppingCartInfo, "OUT_ORDER_FEES_SAVED_CUR", "CSCI_ORDER_FEES_SAVED", "CSCI_ORDER_FEE_SAVED");
    this.hashXML.put(COMConstants.HK_CSCI_ORDER_FEES_SAVED_XML,  csciOrderFeesSavedXML);

    //Document csciCartTaxesXML =
     //   buildXML(customerShoppingCartInfo, "OUT_CART_TAXES_CUR", "CSCI_CART_TAXES", "CSCI_CART_TAX");
    //this.hashXML.put("COMConstants.HK_CSCI_CART_TAXES_XML",     csciCartTaxesXML);
    Map<String, BigDecimal> taxTotals = calculateTaxTotals((CachedResultSet)customerShoppingCartInfo.get("OUT_ORDER_TAXES_CUR"));    
    if(taxTotals != null) {
    	Document csciCartTaxesXML = getCSCICartTaxesDoc(taxTotals);
    	this.hashXML.put("COMConstants.HK_CSCI_CART_TAXES_XML",     csciCartTaxesXML);
    }

    Document csciOrderTaxesXML =
        buildXML(customerShoppingCartInfo, "OUT_ORDER_TAXES_CUR", "CSCI_ORDER_TAXES", "CSCI_ORDER_TAX");
    this.hashXML.put("COMConstants.HK_CSCI_ORDER_TAXES_XML",     csciOrderTaxesXML);

    if (StringUtils.isEmpty(this.orderDetailId))
    {
      Document orderBillsXml = (Document) this.hashXML.get(COMConstants.HK_CSCI_ORDER_BILLS_XML);
      
      if (null != orderBillsXml)
      { 
        if (DOMUtil.selectSingleNode(orderBillsXml,"//order_detail_id/text()") != null)
          this.orderDetailId = DOMUtil.selectSingleNode(orderBillsXml,"//order_detail_id/text()").getNodeValue();
      }
    }

    if (StringUtils.isNotEmpty(this.orderDetailId))
    {
      //Retrieve an Document.  Store this XML object in hashXML HashMap
      Document csciCsrTaggedXML = this.retrieveCSRTagged(this.orderDetailId);
      this.hashXML.put(COMConstants.HK_CSCI_CSR_TAGGED_XML, csciCsrTaggedXML);
    }
    
    //Store other data that the store proc is returning in the pageData HashMap
    String outNumberOfOrders      = "0";
    if (customerShoppingCartInfo.get("OUT_NUMBER_OF_ORDERS") != null)
      outNumberOfOrders = customerShoppingCartInfo.get("OUT_NUMBER_OF_ORDERS").toString();

    String outOrderPosition       = "0";
    if (customerShoppingCartInfo.get("OUT_ORDER_POSITION") != null)
      outOrderPosition = customerShoppingCartInfo.get("OUT_ORDER_POSITION").toString();

    String outOrderInd            = null;
    if (customerShoppingCartInfo.get("OUT_ORDER_INDICATOR") != null)
      outOrderInd = customerShoppingCartInfo.get("OUT_ORDER_INDICATOR").toString();

    String outOrderFound          = null;
    if (customerShoppingCartInfo.get("OUT_ORDER_FOUND") != null)
      outOrderInd = customerShoppingCartInfo.get("OUT_ORDER_FOUND").toString();


    this.pageData.put(COMConstants.PD_CSCI_NUMBER_OF_ORDERS,  outNumberOfOrders);
    this.pageData.put(COMConstants.PD_CSCI_ORDER_POSITION,    outOrderPosition);
    this.pageData.put(COMConstants.PD_CSCI_ORDER_INDICATOR,   outOrderInd);
    this.pageData.put(COMConstants.PD_CSCI_ORDER_FOUND,       outOrderInd);

    //Check if the original payment type was PayPal
    if(paymentType != null && paymentType.equals(COMConstants.CONS_ALTPAY_PAYMENT_TYPE))
      this.pageData.put("isAltPay", "Y");
    else
      this.pageData.put("isAltPay", "N");

    //Check if the original payment was miles/points
    if (FTDCommonUtils.isPaymentMilesPoints(paymentMethodId))
      this.pageData.put("isMilesPoints", "Y");
    else 
      this.pageData.put("isMilesPoints", "N");            

    //check the authorization result to see if the cart payment is authorized.
    //if it is, do not allow any modifications to it.
    //For Alternate Payment:  PayPal and BML will have the apAuthTxt 
    //populated when authorized and United Mileage Plus will have the authNumber 
    //populated when authorized.  Technically PayPal and BML should always
    //be authorized.
    if( (ccUsed != null && ccUsed.equalsIgnoreCase("Y") &&
       ((authOverride !=null && authOverride.equalsIgnoreCase("Y") &&
       eodInd !=null && eodInd.equalsIgnoreCase("N")) ||
       authResult !=null && authResult.equalsIgnoreCase("D") ||
        authResult == null)) ||
        (apAccountId != null && (apAuthTxt == null && authNumber == null)) )
       this.pageData.put("isAuthorized","false");
    else
      this.pageData.put("isAuthorized","true");

    //Check the origin ID for Walmart and Amazon orders and Mercent Order
    //If these orders are found, restrict them from going to the 
    //Update Order and Update Payment screens.
    MercentOrderPrefixes mrcntOrderPrefixes = new MercentOrderPrefixes(this.con);
    PartnerMappingVO partnerMappingVO = null;
    if(!StringUtils.isEmpty(originId) && sourceCode != null) {
		partnerMappingVO = new PartnerUtility().getPartnerOriginsInfo(originId,sourceCode, con);
    }
    else{
    	logger.info("OriginId or sourceCode is null. Could not get the Partner mapping");
    }  
    	
    
    
    if(mrcntOrderPrefixes.isMercentOrder(originId)) {
    	this.pageData.put("partnerName","Mercent");
    	this.pageData.put("partnerChannelIcon",mrcntOrderPrefixes.getMercentIconForChannel(originId));
    	this.pageData.put("partnerChannelName",mrcntOrderPrefixes.getChannelName(originId));
        ConfigurationUtil cu = ConfigurationUtil.getInstance();
        String imageUrl = cu.getFrpGlobalParm(COMConstants.JOE_CONTEXT, COMConstants.IMAGE_SERVER_URL);
        this.pageData.put("imageServerUrl", imageUrl);
    } else if (partnerMappingVO != null && partnerMappingVO.isDefaultBehaviour()) {
		this.pageData.put("partnerName","Partner");
    	this.pageData.put("partnerChannelIcon",partnerMappingVO.getPartnerImage());
    	this.pageData.put("partnerChannelName",partnerMappingVO.getPartnerName());		
        ConfigurationUtil cu = ConfigurationUtil.getInstance();
        String imageUrl = cu.getFrpGlobalParm(COMConstants.JOE_CONTEXT, COMConstants.IMAGE_SERVER_URL);
        this.pageData.put("imageServerUrl", imageUrl);
        this.pageData.put("modifyOrderAllowed", partnerMappingVO.isModifyOrderAllowed());
    }
	
    if(originId != null && (originId.equalsIgnoreCase("WLMTI") ||
       originId.equalsIgnoreCase("AMZNI")) || mrcntOrderPrefixes.isMercentOrder(originId) || (partnerMappingVO != null && partnerMappingVO.isDefaultBehaviour()))
       this.pageData.put("isRestricted","true");
    else
       this.pageData.put("isRestricted","false");
    
    

    if(!"FTDCA".equals(this.companyId) && this.canadianRecipient){
    	  String surchargeExplanation  = ConfigurationUtil.getInstance().
  												getContent(con, "TAX", "TAX_EXPLANATION");
    	  this.pageData.put("surchargeExplanation", surchargeExplanation);
     }
    
    boolean freeShippingMembershipFlag = this.isOrderFreeShippingMembership(this.orderDetailId, con);
    logger.debug("freeShippingMembershipFlag="+freeShippingMembershipFlag);
    boolean hasExpiredAccountProgram = false;
    String programName = null;
    String premierCircleMembership ="";
    CachedResultSet pcrs = orderDAO.getPremierCircleInfo(this.orderGuid);
    if(pcrs.next())
    {
      premierCircleMembership = pcrs.getString("pc_flag");
    }
    if(freeShippingMembershipFlag)
    {
/*    	AccountDAO accountDAO = new AccountDAO();
    	List<AccountProgramVO> programVOs = accountDAO.getProgramsByExternalOrderNumber(this.con, orderNumber);
    	Date now = new Date();
    	Calendar calendar = Calendar.getInstance();
		calendar.setTime(now);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		now = calendar.getTime();
		
    	for (AccountProgramVO accountProgramVO : programVOs) 
    	{
    		if(now.after(accountProgramVO.getExpirationDate()))
    		{
    			hasExpiredAccountProgram = true;
    			programName = accountProgramVO.getDisplayName();
    		}
		}*/
    	Boolean flag = false;
    	Date now = new Date();
    	Calendar calendar = Calendar.getInstance();
		calendar.setTime(now);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		now = calendar.getTime();
    	String buyerEmailAddress="";
    	CachedResultSet rs = orderDAO.getOrderCustomerInfo(orderDetailId);
        if(rs.next())
        {
        	buyerEmailAddress = rs.getString("email_address");
        }
        if(buyerEmailAddress != null)
        {
        	List<String> emailList = new ArrayList<String>();
        	emailList.add(buyerEmailAddress);
        	Map<String, MembershipDetailsVO> membershipMap = FTDCAMSUtils.getMembershipData(emailList, now);
        	if(membershipMap == null){
        		flag = null;		
        	}else{
        		if(membershipMap.containsKey(FTDCAMSUtils.FREESHIPPING_MEMBER_TYPE)){
        			MembershipDetailsVO fsMemberDetails = membershipMap.get(FTDCAMSUtils.FREESHIPPING_MEMBER_TYPE);
        			if(fsMemberDetails != null){
        				flag = fsMemberDetails.isMembershipBenefitsApplicable();
        			}else{
        				flag = false;
        			}
        		}else{
        			flag = false;
        		}
        		
        	}
        	
        	if(flag != null && flag == true)
        	{
        		hasExpiredAccountProgram = false;
        	}
        	else
        	{
        		hasExpiredAccountProgram = true;
        		AccountProgramMasterVO accountProgramMasterVO = CacheUtil.getInstance().getFSAccountProgramDetailsFromContent();
        		programName = accountProgramMasterVO.getDisplayName();
        	}
        }
        else
        {
        	hasExpiredAccountProgram = true;
        }    	
    }
    logger.debug("hasExpiredAccountProgram="+hasExpiredAccountProgram);
    if(hasExpiredAccountProgram)
	{
		this.pageData.put("isAccountProgramExpired","true");
		this.pageData.put("programName",programName);
	}
    else
    {
    	this.pageData.put("isAccountProgramExpired","false");
    }
    if(premierCircleMembership!=null && premierCircleMembership.equalsIgnoreCase("Y")){
    	this.pageData.put("premierCircleMembership","true");
    }
    else{
    	this.pageData.put("premierCircleMembership","false");
    } 
    
    //set the XSL parameter, which governs the page to be loaded
    this.pageData.put("XSL",  COMConstants.XSL_CUSTOMER_SHOPPING_CART);
    


    return customerShoppingCartInfo;
  }

/*******************************************************************************************
 * retrieveCSRTagged()
 *******************************************************************************************
  * This method is used to call the TagDAO.getTagCsr(String orderDetailId)
  *
  * @param1 String - order detail ID
  * @return Document - document containing output from the stored procedure
  * @throws none
  */
  private Document retrieveCSRTagged(String orderDetailId)
          throws Exception
  {
    //Instantiate ViewDAO
    TagDAO tagDAO = new TagDAO(this.con);

    //Document that will contain the output from the stored proce
    Document csrTaggedInfo = DOMUtil.getDocument();

    //Call getTagCsr method in the DAO
    csrTaggedInfo = (Document) tagDAO.getTagCsr(orderDetailId);

    return csrTaggedInfo;
  }

/*******************************************************************************************
 * retrieveCSRViewed()
 *******************************************************************************************
  * This method is used to call the ViewDAO.getCSRViewed
  *
  * @param1 long - end position
  * @return Document - document containing output from the stored procedure
  * @throws none
  */
  private Document retrieveCSRViewed(long recordsToBeReturned)
          throws Exception
  {
    //Instantiate ViewDAO
    ViewDAO viewDAO = new ViewDAO(this.con);

    //Document that will contain the output from the stored proce
    Document csrViewedInfo = DOMUtil.getDocument();

    //Call getCSRViewed method in the DAO
    csrViewedInfo = viewDAO.getCSRViewed(this.csrId, recordsToBeReturned);

    return csrViewedInfo;
  }

/*******************************************************************************************
 * retrieveCSRViewing()
 *******************************************************************************************
  * This method is used to call the ViewDAO.getCSRViewing
  *
  * @return Document - document containing output from the stored procedure
  * @throws none
  */
  private Document retrieveCSRViewing()
          throws Exception
  {
    //Instantiate ViewDAO
    ViewDAO viewDAO = new ViewDAO(this.con);

    //Document that will contain the output from the stored proce
    Document csrViewingInfo = DOMUtil.getDocument();

    //Call getCSRViewing method in the DAO
    csrViewingInfo = viewDAO.getCSRViewing(this.sessionId, this.csrId, this.entityType.toUpperCase(), this.entityId);


    //Store the XML generated by the cursor OUT_CART_CUR in hashXML HashMap
    this.hashXML.put(COMConstants.HK_COS_CSR_VIEWING_XML,  csrViewingInfo);

    //Page Data

    //set the XSL parameter, which governs the page to be loaded
    this.pageData.put("XSL",                              COMConstants.XSL_LOAD_USERS_IFRAME);

    return csrViewingInfo;
  }



/*******************************************************************************************
 * retrieveCSRViewing() - takes an entity type and entity id as input parms.
 *******************************************************************************************
  * This method is used to call the ViewDAO.getCSRViewing
  *
  * @return Document - document containing output from the stored procedure
  * @throws none
  */
  private Document retrieveCSRViewing(String entityType, String entityId)
          throws Exception
  {
    //Instantiate ViewDAO
    ViewDAO viewDAO = new ViewDAO(this.con);

    //Document that will contain the output from the stored proce
    Document csrViewingInfo = DOMUtil.getDocument();

    //Call getCSRViewing method in the DAO
    csrViewingInfo = viewDAO.getCSRViewing(this.sessionId, this.csrId, entityType.toUpperCase(), entityId);

    return csrViewingInfo;
  }



/*******************************************************************************************
 * retrieveEmailInfo()
 *******************************************************************************************
  * This method is used to call the CustomerDAO.searchCustomerEmails
  *
  * @param1 long - start position
  * @param2 long - end position
  * @return HashMap - map containing output from the stored procedure
  * @throws none
  */
  private HashMap retrieveEmailInfo(long startSearchingFrom, long recordsToBeReturned)
          throws Exception
  {
    //Instantiate CustomerDAO
    CustomerDAO customerDAO = new CustomerDAO(this.con);

    //hashmap that will contain the output from the stored proce
    HashMap customerEmailResults = new HashMap();

    //Call searchCustomerEmails method in the DAO
    customerEmailResults = (HashMap)customerDAO.searchCustomerEmails(
              this.customerId, startSearchingFrom, recordsToBeReturned);
    
    // PC changes as per the UI changes
    CachedResultSet emailResult = (CachedResultSet) customerEmailResults.get("OUT_CUR");
    SortedMap<String, CustomerMemberShipVO> custMembershipData = populateCustEmailData(emailResult); 
	marshalCustomerEmailData(custMembershipData);    
    // PC changes till here

    /* Commenting old code - before PC I3 #12963 
    Create an Document and call a method that will transform the CachedResultSet from the
    store proce into an Document
    Document customerEmailsXML = buildXML(customerEmailResults, "OUT_CUR", "CEA_CUSTOMER_EMAILS", "CEA_CUSTOMER_EMAIL");
    and store the XML object in a hashXML hashmap
    this.hashXML.put(COMConstants.HK_CEA_CUSTOMER_EMAILS_XML, customerEmailsXML);*/

    //Store the count in the pageData hashmap
    String outIdCount = "0";
    if (customerEmailResults.get("OUT_ID_COUNT") != null)
      outIdCount = customerEmailResults.get("OUT_ID_COUNT").toString();
    this.pageData.put(COMConstants.PD_CEA_COUNT, outIdCount);

    //set the XSL parameter, which governs the page to be loaded
    this.pageData.put("XSL", COMConstants.XSL_MORE_CUSTOMER_EMAILS);

    return customerEmailResults;
  }



/*******************************************************************************************
 * retrieveSourceCodeInfo()
 *******************************************************************************************
  *
  * @throws none
  */
  private Document retrieveSourceCodeInfo() throws Exception
  {
    //Instantiate OrderDAO
    OrderDAO orderDAO = new OrderDAO(this.con);

    //hashmap that will contain the output from the stored proc
    Document sourceCodeInfo = DOMUtil.getDocument();

    //Call getCartInfo method in the DAO
    sourceCodeInfo = orderDAO.getSourceCodeInfo(this.sourceCode);


    //Store the XML generated by the cursor OUT_CART_CUR in hashXML HashMap
    this.hashXML.put(COMConstants.HK_SOURCE_CODE_XML,  sourceCodeInfo);


    //set the XSL parameter, which governs the page to be loaded
    this.pageData.put("XSL",  COMConstants.XSL_SOURCE_CODE_DESCRIPTION);

    return sourceCodeInfo;
  }



/*******************************************************************************************
 * retrievePaymentInfo()
 *******************************************************************************************
  *
  * @throws none
  */
  private Document retrievePaymentInfo(String functionName) throws Exception
  {
    //Instantiate OrderDAO
    OrderDAO orderDAO = new OrderDAO(this.con);

    //hashmap that will contain the output from the stored proc
    Document paymentInfo = DOMUtil.getDocument();

    //Call getCartInfo method in the DAO
    paymentInfo = orderDAO.getPaymentInfo(functionName, this.payInfoNumber);

    //Store the XML generated by the cursor OUT_CART_CUR in hashXML HashMap
    this.hashXML.put(COMConstants.HK_PAYMENT_INFO_XML,  paymentInfo);


    //set the XSL parameter, which governs the page to be loaded
    if (functionName.equalsIgnoreCase("cart"))
      this.pageData.put("XSL",  COMConstants.XSL_CART_PAYMENT_INFORMATION);
    else
      this.pageData.put("XSL",  COMConstants.XSL_ORDER_PAYMENT_INFORMATION);


    return paymentInfo;
  }



/*******************************************************************************************
 * retrieveMembershipInfo()
 *******************************************************************************************
  *
  * @throws none
  */
  private Document retrieveMembershipInfo() throws Exception
  {
    //Instantiate OrderDAO
    CustomerDAO customerDAO = new CustomerDAO(this.con);

    //hashmap that will contain the output from the stored proc
    Document membershipInfo = DOMUtil.getDocument();

    //Call getCartInfo method in the DAO
    membershipInfo = customerDAO.getMembershipInfo(this.orderDetailId);


    //Store the XML generated by the cursor OUT_CART_CUR in hashXML HashMap
    this.hashXML.put(COMConstants.HK_MEMBERSHIP_INFO_XML,  membershipInfo);

    // Also retrieve any co_brand information
    retrieveCoBrandInfo();

    //set the XSL parameter, which governs the page to be loaded
    this.pageData.put("XSL",  COMConstants.XSL_MEMBERSHIP_INFORMATION);

    return membershipInfo;
  }



/*******************************************************************************************
 * retrieveCoBrandInfo()
 *******************************************************************************************
  *
  * @throws none
  */
  private void retrieveCoBrandInfo() throws Exception
  {
    Document coBrandInfo;

    //Instantiate OrderDAO
    OrderDAO orderDAO = new OrderDAO(this.con);

    //Call getCartInfo method in the DAO
    coBrandInfo = orderDAO.getOrderCoBrands(this.orderDetailId);

    //Store the XML generated by the cursor OUT_CUR in hashXML HashMap
    this.hashXML.put(COMConstants.HK_CO_BRAND_INFO_XML,  coBrandInfo);
  }



/*******************************************************************************************
 * retrieveOrderInfoForPrint()
 *******************************************************************************************
  *
  * @throws none
  */
  private HashMap retrieveOrderInfoForPrint() throws Exception
  {
    //Instantiate OrderDAO
    OrderDAO orderDAO = new OrderDAO(this.con);

    //hashmap that will contain the output from the stored proc
    HashMap printOrderInfo = new HashMap();

    //Call getCartInfo method in the DAO
    printOrderInfo = orderDAO.getOrderInfoForPrint(this.orderDetailId, this.includeComments, COMConstants.CONS_PROCESSING_ID_PRODUCT);

    //Create an Document and call a method that will transform the CachedResultSet from the cursor:
    //OUT_ORDERS_CUR into an Document.  Store this XML object in hashXML HashMap
    Document printOrderXML =
        buildXML(printOrderInfo, "OUT_ORDERS_CUR", "PRINT_ORDERS", "PRINT_ORDER");
    this.hashXML.put(COMConstants.HK_PRINT_ORDER_XML,             printOrderXML);

    //Create an Document and call a method that will transform the CachedResultSet from the cursor:
    //OUT_ORDER_ADDON_CUR into an Document.  Store this XML object in hashXML HashMap
    Document printOrderAddonXML =
        buildXML(printOrderInfo, "OUT_ORDER_ADDON_CUR", "PRINT_ORDER_ADDONS", "PRINT_ORDER_ADDON");
    this.hashXML.put(COMConstants.HK_PRINT_ORDER_ADDON_XML,       printOrderAddonXML);

    //Create an Document and call a method that will transform the CachedResultSet from the cursor:
    //OUT_ORDER_BILLS_CUR into an Document.  Store this XML object in hashXML HashMap
    Document printOrderBillsXML =
        buildXML(printOrderInfo, "OUT_ORDER_BILLS_CUR", "PRINT_ORDER_BILLS", "PRINT_ORDER_BILL");
    this.hashXML.put(COMConstants.HK_PRINT_ORDER_BILLS_XML,       printOrderBillsXML);

    //Create an Document and call a method that will transform the CachedResultSet from the cursor:
    //OUT_ORDER_REFUNDS_CUR into an Document.  Store this XML object in hashXML HashMap
    Document  printOrderRefundsXML =
        buildXML(printOrderInfo, "OUT_ORDER_REFUNDS_CUR", "PRINT_ORDER_REFUNDS", "PRINT_ORDER_REFUND");
    this.hashXML.put(COMConstants.HK_PRINT_ORDER_REFUNDS_XML,     printOrderRefundsXML);

    //Create an Document and call a method that will transform the CachedResultSet from the cursor:
    //OUT_ORDER_TOTAL_CUR into an Document.  Store this XML object in hashXML HashMap
    Document  printOrderTotalXML =
        buildXML(printOrderInfo, "OUT_ORDER_TOTAL_CUR", "PRINT_ORDER_TOTALS", "PRINT_ORDER_TOTAL");
    this.hashXML.put(COMConstants.HK_PRINT_ORDER_TOTAL_XML,       printOrderTotalXML);

    //Create an Document and call a method that will transform the CachedResultSet from the cursor:
    //OUT_ORDER_PAYMENT_CUR into an Document.  Store this XML object in hashXML HashMap
    Document  printOrderPaymentXML =
        buildXML(printOrderInfo, "OUT_ORDER_PAYMENT_CUR", "PRINT_ORDER_PAYMENTS", "PRINT_ORDER_PAYMENT");
    this.hashXML.put(COMConstants.HK_PRINT_ORDER_PAYMENT_XML,      printOrderPaymentXML);

    //Create an Document and call a method that will transform the CachedResultSet from the cursor:
    //OUT_ORDER_COMMENT_CUR into an Document.  Store this XML object in hashXML HashMap
    Document  printOrderCommentXML =
        buildXML(printOrderInfo, "OUT_ORDER_COMMENT_CUR", "PRINT_ORDER_COMMENTS", "PRINT_ORDER_COMMENT");
    this.hashXML.put(COMConstants.HK_PRINT_ORDER_COMMENT_XML,      printOrderCommentXML);

    //Create an Document and call a method that will transform the CachedResultSet from the cursor:
    //CUST_CART_CUR into an Document.  Store this XML object in hashXML HashMap
    Document  printCustomerXML =
        buildXML(printOrderInfo, "CUST_CART_CUR", "PRINT_CUSTOMERS", "PRINT_CUSTOMER");
    this.hashXML.put(COMConstants.HK_PRINT_ORDER_CUSTOMER_XML,      printCustomerXML);

    //Create an Document and call a method that will transform the CachedResultSet from the cursor:
    //OUT_CUSTOMER_PHONE_CUR into an Document.  Store this XML object in hashXML HashMap
    Document  printCustomerPhoneXML =
        buildXML(printOrderInfo, "OUT_CUSTOMER_PHONE_CUR", "PRINT_CUSTOMER_PHONES", "PRINT_CUSTOMER_PHONE");
    this.hashXML.put(COMConstants.HK_PRINT_ORDER_CUSTOMER_PHONE_XML, printCustomerPhoneXML);

    //Create an Document and call a method that will transform the CachedResultSet from the cursor:
    //OUT_MEMBERSHIP_CUR into an Document.  Store this XML object in hashXML HashMap
    Document  printMembershipXML =
        buildXML(printOrderInfo, "OUT_MEMBERSHIP_CUR", "PRINT_MEMBERSHIPS", "PRINT_MEMBERSHIP");
    this.hashXML.put(COMConstants.HK_PRINT_ORDER_MEMBERSHIP_XML,     printMembershipXML);

    //Create an Document and call a method that will transform the CachedResultSet from the cursor:
    //OUT_TAX_REFUND_CUR into an Document.  Store this XML object in hashXML HashMap
    Document  printTaxRefundXML =
        buildXML(printOrderInfo, "OUT_TAX_REFUND_CUR", "PRINT_TAX_REFUNDS", "PRINT_TAX_REFUND");
    this.hashXML.put(COMConstants.HK_PRINT_ORDER_TAX_REFUNDS_XML,     printTaxRefundXML);

    //set the XSL parameter, which governs the page to be loaded
    this.pageData.put("XSL",  COMConstants.XSL_PRINT_ORDER);

    return printOrderInfo;
  }



/*******************************************************************************************
 * retrieveRefundDisposition()
 *******************************************************************************************
  *
  * @throws none
  */
  private Document retrieveRefundDispositionValues() throws Exception
  {
    //Instantiate RefundDAO
    RefundDAO refundDAO = new RefundDAO(this.con);

    String displayType = null;

    //Document that will contain the output from the stored proc
    Document refundDispositionXML = DOMUtil.getDocument();

    //Call getCartInfo method in the DAO
    refundDispositionXML = refundDAO.loadRefundDispositionsXML(displayType);

    //Store the XML generated by the cursor OUT_CART_CUR in hashXML HashMap
    this.hashXML.put(COMConstants.HK_REFUND_REFUND_CODES_XML,  refundDispositionXML);

    return refundDispositionXML;
  }



/*******************************************************************************************
 * retrieveResponsibleParty()
 *******************************************************************************************
  *
  * @throws none
  */
  private Document retrieveResponsiblePartyValues() throws Exception
  {
    //Instantiate RefundDAO
    RefundDAO refundDAO = new RefundDAO(this.con);

    //hashmap that will contain the output from the stored proc
    Document responsiblePartyXML = DOMUtil.getDocument();

    //Call getCartInfo method in the DAO
    responsiblePartyXML = refundDAO.loadOrderFloristsXML(this.orderDetailId);

    //Store the XML generated by the cursor OUT_CART_CUR in hashXML HashMap
    this.hashXML.put(COMConstants.HK_REFUND_RESPONSIBLE_PARTY_XML,  responsiblePartyXML);

    return responsiblePartyXML;

  }



/*******************************************************************************************
 * retrieveRefundCartInfo()
 *******************************************************************************************
  *
  * @throws none
  */

  private HashMap retrieveRefundCartInfo() throws Exception
  {
    //Instantiate RefundDAO
    RefundDAO refundDAO = new RefundDAO(this.con);

    //hashmap that will contain the output from the stored proc
    HashMap refundStatus = new HashMap();

    //Call getCartRefundStatusXML method in the DAO
    refundStatus = refundDAO.getCartRefundStatusXML(this.orderGuid);

    return refundStatus;

  }



/*******************************************************************************************
 * retrieveLockInfo() - locks the record for Payment Refunds
 *******************************************************************************************
  *
  * @throws none
  */
  private Document retrieveLockInfo(String entityId, String entityType, String orderLevel) throws Exception
  {
    //Instantiate RefundDAO
    LockDAO lockDAO = new LockDAO(this.con);

    //hashmap that will contain the output from the stored proc
    Document lockInfo = DOMUtil.getDocument();


    //Call retrieveLockXML method in the DAO
  //lockInfo = lockDAO.retrieveLockXML(sessionId,       csrId,        entityId [aka lockId],  entityType [aka applicationName],   orderLevel);
    lockInfo = lockDAO.retrieveLockXML(this.sessionId,  this.csrId,   entityId,               entityType,                         orderLevel);

    return lockInfo;

  }



/*******************************************************************************************
 * retrieveQueueInfo() - RETRIEVE THE QUEUE INFO
 *******************************************************************************************
  *
  * @throws none
  */
  private String retrieveQueueInfo() throws Exception
  {
    String sOrderDetailId = null;
    String sQueueId = null;

    //Instantiate Update Order DAO
    UpdateOrderDAO uoDAO = new UpdateOrderDAO(this.con);

    //Resultset that will contain the output from the stored proc
    CachedResultSet rs;

    //Call getqueueretrieveLockXML method in the DAO
    rs = (CachedResultSet) uoDAO.getQueueInfoByIdXML(this.orderDetailId, "ZIP");

    while(rs.next())
    {
      //set the order detail id
      sOrderDetailId = null;
      if(rs.getObject("order_detail_id") != null)
          sOrderDetailId = ((BigDecimal) rs.getObject("order_detail_id")).toString();

      //set the queue id
      sQueueId = null;
      if(rs.getObject("message_id") != null)
          sQueueId = ((BigDecimal) rs.getObject("message_id")).toString();

      if (sOrderDetailId != null && !sOrderDetailId.equalsIgnoreCase(""))
      {
        break;
      }

    } //end-while

    return sQueueId;

  }


/*******************************************************************************************
 * retrieveLiveInfo() - RETRIEVE a flag that will show if there were any Live FTD orders
 *******************************************************************************************
  *
  * @throws none
  */
  private boolean retrieveLiveInfo() throws Exception
  {
    //Instantiate Update Order DAO
    UpdateOrderDAO uoDAO = new UpdateOrderDAO(this.con);

    //Call getqueueretrieveLockXML method in the DAO
    boolean hasLiveFTD = uoDAO.hasLiveMercury(this.orderDetailId);

    return hasLiveFTD;

  }




/*******************************************************************************************
 * releaseLock() - if the lock exists, and CSR cancels an action, release the Payment lock
 *******************************************************************************************
  *
  * @throws none
  */
  private void releaseLock() throws Exception
  {
    //Instantiate RefundDAO
    LockDAO lockDAO = new LockDAO(this.con);

    //Call releaseLock method in the DAO
    lockDAO.releaseLock(this.sessionId, this.csrId, this.entityId, this.entityType);


  }



/*******************************************************************************************
 * checkCustomerOrderSearchResults()
 *******************************************************************************************
  * This method is used to call the CustomerDAO.searchCustomer
  *
  * @param1 HashMap - customer order search results
  * @return boolean - if everything conforms to the business rules, return true;  else return false.
  * @throws none
  */
  private boolean checkCustomerOrderSearchResults(HashMap customerOrderSearchResults, long newPagePosition)
          throws Exception
  {
    //long outIdCount = Long.valueOf(DOMUtil.getNodeValue((Document)customerOrderSearchResults.get("out-parameters"), "OUT_ID_COUNT")).longValue();
    long outIdCount = 0;
    if (customerOrderSearchResults.get("OUT_ID_COUNT") != null)
      outIdCount = Long.valueOf(customerOrderSearchResults.get("OUT_ID_COUNT").toString()).longValue();

    //If no records found
    if (outIdCount < 1)
    {
      //set the XSL parameter, which governs the page to be loaded
      if(this.action.equals(COMConstants.ACTION_SEARCH)) {
          this.pageData.put("XSL", COMConstants.XSL_CUSTOMER_ORDER_SEARCH);    
      }
      else {
          this.pageData.put("XSL", COMConstants.XSL_LOSS_PREVENTION_SEARCH);
      }
   
      //set the Error message to be displayed
      this.pageData.put(COMConstants.PD_MESSAGE_DISPLAY, COMConstants.MSG_NO_RECORDS);
    
      return false;
    }
    
    //If # of records found > max allowed
    else if(outIdCount > this.maxRecordsAllowed)
    {
      //set the XSL parameter, which governs the page to be loaded
        if(this.action.equals(COMConstants.ACTION_SEARCH)) {
            this.pageData.put("XSL", COMConstants.XSL_CUSTOMER_ORDER_SEARCH);    
        }
        else {
            this.pageData.put("XSL", COMConstants.XSL_LOSS_PREVENTION_SEARCH);
        }
    
      //set the Error message to be displayed
      this.pageData.put(COMConstants.PD_MESSAGE_DISPLAY, COMConstants.MSG_TOO_MANY);

      return false;
    }
    //If 1 < # of records found < max allowed
    else if(outIdCount > 1 && outIdCount <= this.maxRecordsAllowed)
    {
      //set the XSL parameter, which governs the page to be loaded
      this.pageData.put("XSL", COMConstants.XSL_MULTIPLE_RECORDS_FOUND);
      
       //Since the multiple records page will have navigational capabilities, set the buttons
      //accordingly.
      setButtons(outIdCount, newPagePosition, this.maxCustomersPerPage, "cos");

      /** Set Page Data **/

      this.pageData.put(COMConstants.PD_COS_START_POSITION, String.valueOf(startSearchingFrom));
      //set PD_CUSTOMER_ORDER_SEARCH_COUNT and PD_TOTAL_RECORDS_FOUND
      this.pageData.put(COMConstants.PD_COS_TOTAL_RECORDS_FOUND, String.valueOf(outIdCount));


      return false;
    }
    //Only 1 record was found
    else
    {
      //create an array.
      ArrayList aScrubList = new ArrayList();

      //Initialize the array with the top and bottom nodes of the XML, and the node for which the
      //value must be retrieved. In this instance, we will retireve the value of scrub_indicator.
      aScrubList.add(0,"SC_SEARCH_CUSTOMERS");
      aScrubList.add(1,"SC_SEARCH_CUSTOMER");
      aScrubList.add(2,"scrub_status");
      String sOrderStatus = DOMUtil.getNodeValue((Document)this.hashXML.get(
                              COMConstants.HK_COS_CUSTOMER_ORDER_SEARCH_XML), aScrubList);


      //create an array.
      ArrayList aLastOrderDateList = new ArrayList();

      //Initialize the array with the top and bottom nodes of the XML, and the node for which the
      //value must be retrieved. In this instance, we will retireve the value of last_order_date.
      aLastOrderDateList.add(0,"SC_SEARCH_CUSTOMERS");
      aLastOrderDateList.add(1,"SC_SEARCH_CUSTOMER");
      aLastOrderDateList.add(2,"last_order_date");
      String sLastOrderDate = DOMUtil.getNodeValue((Document) this.hashXML.get(
                                COMConstants.HK_COS_CUSTOMER_ORDER_SEARCH_XML),  aLastOrderDateList);


      sLastOrderDate = sLastOrderDate.trim();
      //if the last_order_date shows a value "recipient", set a global indicator
      if  ( sLastOrderDate.equalsIgnoreCase("recipient")  )
      {
        this.recipientInd = COMConstants.CONS_YES;
      }

      //if Order in Scrub Status
      if(sOrderStatus.equalsIgnoreCase(COMConstants.CONS_ORDER_STATUS_SCRUB))
      {
        //set the XSL parameter, which governs the page to be loaded
          if(this.action.equals(COMConstants.ACTION_SEARCH)) {
              this.pageData.put("XSL", COMConstants.XSL_CUSTOMER_ORDER_SEARCH);    
          }
          else {
              this.pageData.put("XSL", COMConstants.XSL_LOSS_PREVENTION_SEARCH);
          }

        return false;
      }
      //if Order in Removed Status
      else if(sOrderStatus.equalsIgnoreCase(COMConstants.CONS_ORDER_STATUS_REMOVED))
      {
        //set the XSL parameter, which governs the page to be loaded
          if(this.action.equals(COMConstants.ACTION_SEARCH)) {
              this.pageData.put("XSL", COMConstants.XSL_CUSTOMER_ORDER_SEARCH);    
          }
          else {
              this.pageData.put("XSL", COMConstants.XSL_LOSS_PREVENTION_SEARCH);
          }

        return false;
      }
      //if Order in Pending Status
      else if(sOrderStatus.equalsIgnoreCase(COMConstants.CONS_ORDER_STATUS_PENDING))
      {
        //set the XSL parameter, which governs the page to be loaded
          if(this.action.equals(COMConstants.ACTION_SEARCH)) {
              this.pageData.put("XSL", COMConstants.XSL_CUSTOMER_ORDER_SEARCH);    
          }
          else {
              this.pageData.put("XSL", COMConstants.XSL_LOSS_PREVENTION_SEARCH);
          }

        return false;
      }
      //the record retrieved passes the Business Rules.  Return a "true" value
      else
      {
        return true;
      }
    }

  }



/*******************************************************************************************
 * setButtons()
 *******************************************************************************************
  * This method will utilize the inputs to determine which buttons should be enabled.
  *
  * @param1 long - total number of records
  * @param2 long - page position
  * @param3 long - maximumn number of records for that page
  * @return none
  * @throws none
  */
  private void setButtons(long outIdCount, long newPagePosition, long maxRecordsPerPage, String appendString)
  {
    //evaluate and set current_pages
    this.pageData.put(appendString + '_' +  COMConstants.PD_CURRENT_PAGE, String.valueOf(newPagePosition));

    //evaluate and set total_pages
    long totalPages = (long)(outIdCount / maxRecordsPerPage) +
                      (long)(((long)(outIdCount % maxRecordsPerPage)) > 0?1:0);
    this.pageData.put(appendString + '_' +  COMConstants.PD_TOTAL_PAGES, String.valueOf(totalPages));

    //evaluate and set show_first and show_prev
    if(outIdCount > maxRecordsPerPage && newPagePosition > 1)
    {
      this.pageData.put(appendString + '_' +  COMConstants.PD_SHOW_FIRST, COMConstants.CONS_YES);
      this.pageData.put(appendString + '_' +  COMConstants.PD_SHOW_PREVIOUS, COMConstants.CONS_YES);
    }
    else
    {
      this.pageData.put(appendString + '_' +  COMConstants.PD_SHOW_FIRST, COMConstants.CONS_NO);
      this.pageData.put(appendString + '_' +  COMConstants.PD_SHOW_PREVIOUS, COMConstants.CONS_NO);
    }

    //evaluate and set show_next and show_last
    if(outIdCount > maxRecordsPerPage && newPagePosition  < totalPages)
    {
      this.pageData.put(appendString + '_' +  COMConstants.PD_SHOW_NEXT, COMConstants.CONS_YES);
      this.pageData.put(appendString + '_' +  COMConstants.PD_SHOW_LAST, COMConstants.CONS_YES);
    }
    else
    {
      this.pageData.put(appendString + '_' +  COMConstants.PD_SHOW_NEXT, COMConstants.CONS_NO);
      this.pageData.put(appendString + '_' +  COMConstants.PD_SHOW_LAST, COMConstants.CONS_NO);
    }

  }



/*******************************************************************************************
 * setCSCIButtons()
 *******************************************************************************************
  * This method will utilize the inputs to determine which buttons should be enabled.
  *
  * @param1 long - total number of records
  * @param2 long - page position
  * @param3 long - maximumn number of records for that page
  * @return none
  * @throws none
  */
  private void setCSCIButtons(long outIdCount, long outOrderPosition, long newPagePosition, long maxRecordsPerPage, String appendString)
  {

    //included a -1 because its inclusive
    long computedPositionQuot =  (outOrderPosition + maxRecordsPerPage - 1) / maxRecordsPerPage;
    long computedPositionRem  =  (outOrderPosition + maxRecordsPerPage - 1) % maxRecordsPerPage;

    if (outIdCount <= maxRecordsPerPage)
    {
      newPagePosition = 1;
      this.pageData.put(appendString + '_' +  COMConstants.PD_SHOW_PREVIOUS, COMConstants.CONS_NO);
      this.pageData.put(appendString + '_' +  COMConstants.PD_SHOW_NEXT, COMConstants.CONS_NO);
    }
    else
    {
      if (outOrderPosition == 1)
      {
        newPagePosition = 1;
        this.pageData.put(appendString + '_' +  COMConstants.PD_SHOW_PREVIOUS, COMConstants.CONS_NO);
        this.pageData.put(appendString + '_' +  COMConstants.PD_SHOW_NEXT, COMConstants.CONS_YES);
      }
      else if ( outIdCount <= (outOrderPosition + maxRecordsPerPage - 1))
      {
        if (computedPositionRem > 0)
          newPagePosition = computedPositionQuot + 1;
        else
          newPagePosition = computedPositionQuot;

        this.pageData.put(appendString + '_' +  COMConstants.PD_SHOW_PREVIOUS, COMConstants.CONS_YES);
        this.pageData.put(appendString + '_' +  COMConstants.PD_SHOW_NEXT, COMConstants.CONS_NO);
      }
      else
      {
        if (computedPositionRem > 0)
          newPagePosition = computedPositionQuot + 1;
        else
          newPagePosition = computedPositionQuot;

        this.pageData.put(appendString + '_' +  COMConstants.PD_SHOW_PREVIOUS, COMConstants.CONS_YES);
        this.pageData.put(appendString + '_' +  COMConstants.PD_SHOW_NEXT, COMConstants.CONS_YES);
      }
    }

    this.newPagePosition = newPagePosition;

    //evaluate and set current_pages
    this.pageData.put(appendString + '_' +  COMConstants.PD_CURRENT_PAGE, String.valueOf(newPagePosition));

    //evaluate and set total_pages
    long totalPages = (long)(outIdCount / maxRecordsPerPage) +
                      (long)(((long)(outIdCount % maxRecordsPerPage)) > 0?1:0);
    this.pageData.put(appendString + '_' +  COMConstants.PD_TOTAL_PAGES, String.valueOf(totalPages));

  }



/*******************************************************************************************
 * buildXML()
 *******************************************************************************************/
  private Document buildXML(HashMap outMap, String cursorName, String topName, String bottomName)
        throws Exception
  {

    //Create a CachedResultSet object using the cursor name that was passed.
    CachedResultSet rs = (CachedResultSet) outMap.get(cursorName);

    Document doc =  null;

    //Create an XMLFormat, and initialize the top and bottom node.  Note that since this method
    //can be/is called by various other methods, the top and botton names MUST be passed to it.
    XMLFormat xmlFormat = new XMLFormat();
    xmlFormat.setAttribute("type", "element");
    xmlFormat.setAttribute("top", topName);
    xmlFormat.setAttribute("bottom", bottomName );

    //call the DOMUtil's converToXMLDOM method
    doc = (Document) DOMUtil.convertToXMLDOM(rs, xmlFormat);
    
    //return the xml document.
    return doc;

  }
  
  
/*******************************************************************************************
 * buildXML()
 *******************************************************************************************/
  private Document buildXML(CachedResultSet crs, String topName, String bottomName)
        throws Exception
  {

    Document doc =  null;

    //Create an XMLFormat, and initialize the top and bottom node.  Note that since this method
    //can be/is called by various other methods, the top and botton names MUST be passed to it.
    XMLFormat xmlFormat = new XMLFormat();
    xmlFormat.setAttribute("type", "element");
    xmlFormat.setAttribute("top", topName);
    xmlFormat.setAttribute("bottom", bottomName );

    //call the DOMUtil's converToXMLDOM method
    doc = (Document) DOMUtil.convertToXMLDOM(crs, xmlFormat);

    //return the xml document.
    return doc;

  }



/*******************************************************************************************
 * timerStart()
 *******************************************************************************************/
  private void timerStart()         throws Exception
  {
    TimerFilter tFilter = new TimerFilter(this.con);

    //hashmap that will contain the output from the stored proce
    HashMap timerResults = new HashMap();


    if (csrId == null || csrId.equalsIgnoreCase(""))
    {
      csrId = "SYSTEM";
    }

    String timerOrigin = request.getParameter(COMConstants.H_CUSTOMER_ORDER_INDICATOR);

    if(timerOrigin != null && timerOrigin.equals(ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, COMConstants.CONS_OE_INDICATOR)))
    {
        timerOrigin = COMConstants.START_ORIGIN_OE;
    }
    else if(timerOrigin != null && timerOrigin.equals(ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE, COMConstants.CONS_CS_INDICATOR)))
    {
        timerOrigin = COMConstants.START_ORIGIN_CS;
    }
    else
    {
        timerOrigin = COMConstants.START_ORIGIN_GEN;
    }

    //startTimer(entityType, entityId, commentOrigin, csrId, timerCommentOriginType)
    timerResults =  tFilter.startTimer( COMConstants.CONS_ENTITY_TYPE_ORDERS, this.startTimerOrderGuid,
                                        timerOrigin, this.csrId,
                                        COMConstants.CONS_COM_COMMENT_ORIGIN_TYPE,
                                        this.timerCallLogId);

    this.timerEntityHistoryId   = timerResults.get(COMConstants.TIMER_ENTITY_HISTORY_ID).toString();
    this.timerCommentOriginType = timerResults.get(COMConstants.TIMER_COMMENT_ORIGIN_TYPE).toString();


  }



/*******************************************************************************************
 * timerStop()
 *******************************************************************************************/
  private void timerStop()         throws Exception
  {

    TimerFilter tFilter = new TimerFilter(this.con);

    //stopTimer(timerEntityHistoryId)
    tFilter.stopTimer(this.timerEntityHistoryId);

    this.timerEntityHistoryId = "";
    this.timerCommentOriginType = "";

  }


/*******************************************************************************************
 * deleteCsrViewing()
 *******************************************************************************************/
  private void deleteCsrViewing()         throws Exception
  {

    DatabaseMaintainUtil dmu = new DatabaseMaintainUtil(this.con, this.sessionId);
    HashMap csrDeleteInfo = (HashMap) dmu.deleteViewingRecords();

  }


     /**
     * Obtains customer account information.
     *   1. get security token and csr id
     *   2. get maximum # of recipient information records to be returned
     *   3. set the current page position
     *   4. retrieve the total number of pages that was initially sent to the page
     *   5. set the new page position
     *   6. get customer account information
     *   7. get page data
     *   8. return hashmap
     *
     * @param String - customer id
     * @param String - page #
     *
     * @return HashMap - hashmap containing various customer order search xml docs
     *
     * @throws IOException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws TransformerException
     * @throws XSLException
     * @throws Exception
     */
    public HashMap processRequestForLossPrevention(String customerId, String pageNum)
        throws IOException, ParserConfigurationException, SAXException, TransformerException,
               Exception
    {
        logger.debug("processRequestForLossPrevention() parameters are: \n" +
            "customerId = " + customerId + "\n" +
            "pageNum = " + pageNum);

        // 1. get security token and csr id
        this.sessionId   = request.getParameter(COMConstants.SEC_TOKEN);
        SecurityManager sm = null;
        UserInfo ui = null;

        // get security manager instance
        ConfigurationUtil cu = ConfigurationUtil.getInstance();
        String security = (String) cu.getProperty(COMConstants.PROPERTY_FILE,COMConstants.SECURITY_IS_ON);
        boolean securityIsOn = security.equalsIgnoreCase("true") ? true : false;
        if(securityIsOn)
        {
            sm = SecurityManager.getInstance();
            ui = sm.getUserInfo(sessionId);
            csrId = ui.getUserID();
        }

        //Always generate the search criteria
        boolean firstInvokation = false;
        generateSearchCriteria(firstInvokation);

        // 2. get maximum # of recipient information records to be returned
        String maxCustAcctLPOrders = (String) cu.getProperty(COMConstants.PROPERTY_FILE,COMConstants.CONS_MAX_CUSTOMER_ACCOUNT_ORDERS_LP);
        long lMaxCustAcctLPOrders = new Long(maxCustAcctLPOrders).longValue();

        // 3. set the current page position
        long lPageNum = 1;
        if (pageNum != null  &&  pageNum.trim().length() > 0)
        {
            lPageNum = new Long(pageNum).longValue();
        }
        this.currentCAIPagePosition = lPageNum;

        // 4. retrieve the total number of pages that was initially sent to the page
        if(request.getParameter(COMConstants.PD_CAI_TOTAL_PAGES) != null)
        {
          if(!request.getParameter(COMConstants.PD_CAI_TOTAL_PAGES).toString().equalsIgnoreCase(""))
          {
            this.totalCAIPages =
              Long.valueOf(request.getParameter(COMConstants.PD_CAI_TOTAL_PAGES)).longValue();
          }
        }

        // 5. set the new page position
        this.newPagePosition = 0;
        this.action = request.getParameter(COMConstants.ACTION);
        if(action != null  &&  action.trim().length() > 0)
        {
            if (this.action.equalsIgnoreCase(COMConstants.ACTION_CA_FIRST_PAGE))
              this.newPagePosition = 1;
            else if (this.action.equalsIgnoreCase(COMConstants.ACTION_CA_PREVIOUS_PAGE))
              this.newPagePosition = this.currentCAIPagePosition - 1;
            else if (this.action.equalsIgnoreCase(COMConstants.ACTION_CA_NEXT_PAGE))
              this.newPagePosition = this.currentCAIPagePosition + 1;
            else if (this.action.equalsIgnoreCase(COMConstants.ACTION_CA_LAST_PAGE))
              this.newPagePosition = this.totalCAIPages;

            //set the begin and max allowed parameters, to be passed to the DAOs.
            setBeginEndParameters(this.newPagePosition, lMaxCustAcctLPOrders);
        }
        else
            //set the begin and max allowed parameters, to be passed to the DAOs.
            setBeginEndParameters(this.currentCAIPagePosition, lMaxCustAcctLPOrders);

        // 6. get customer account information
        //call retrieveCustomerAccountInfo which will call the DAO to retrieve the results
        HashMap acctinfo = retrieveCustomerAccountInfo(customerId, this.startSearchingFrom, this.recordsToBeReturned);
        HashMap lpData = new HashMap();

        Document xmlDoc,
                    addXMLDoc = null;

        // add customer node to root document
        addXMLDoc = (Document) this.hashXML.get(COMConstants.HK_CAI_CUSTOMER_XML);
        lpData.put(COMConstants.HK_CAI_CUSTOMER_XML,addXMLDoc);

        // add emails node to root document
        addXMLDoc = (Document) this.hashXML.get(COMConstants.HK_CAI_EMAIL_XML);
        lpData.put(COMConstants.HK_CAI_EMAIL_XML,addXMLDoc);

        // add orders node to root document
        addXMLDoc = (Document) this.hashXML.get(COMConstants.HK_CAI_ORDER_XML);
        lpData.put(COMConstants.HK_CAI_ORDER_XML,addXMLDoc);

        // add viewing node to root document
        addXMLDoc = (Document) this.hashXML.get(COMConstants.HK_CAI_VIEWING_XML);
        lpData.put(COMConstants.HK_CAI_VIEWING_XML,addXMLDoc);

        // add phone node to root document
        addXMLDoc = (Document) this.hashXML.get(COMConstants.HK_CAI_PHONE_XML);
        lpData.put(COMConstants.HK_CAI_PHONE_XML,addXMLDoc);

        // 7. get page data
        this.populatePageData();
        if(action != null  &&  action.trim().length() > 0)
            setButtons(Long.valueOf(this.pageData.get(COMConstants.PD_CAI_NUMBER_OF_ORDERS).toString()).longValue(), this.newPagePosition, lMaxCustAcctLPOrders, "cai");
        else
            setButtons(Long.valueOf(this.pageData.get(COMConstants.PD_CAI_NUMBER_OF_ORDERS).toString()).longValue(), this.currentCAIPagePosition, lMaxCustAcctLPOrders, "cai");

        Document temp = (Document) DOMUtil.getDocument();
        DOMUtil.addSection(temp, COMConstants.PD_PAGE_DATA, COMConstants.PD_DATA, pageData, true);

        Document tempPageData = (Document) DOMUtil.getDefaultDocument();
        tempPageData.appendChild(tempPageData.createElement(COMConstants.PD_PAGE_DATA));
        NodeList nl = temp.getElementsByTagName(COMConstants.PD_DATA);
        DOMUtil.addSection(tempPageData,nl);
        lpData.put(COMConstants.PD_PAGE_DATA,tempPageData);

        // 8. return hashmap
        return lpData;
    }


  /*
   * Retrieves FTD_APPS.JCPENNEY_FLAG from the SourceCode cache handler.
   * @param sourceCode The source code to lookup.
   * @throws IllegalArgumentException if sourceCode is null or whitespace only
   */
  private void setJCPFlag(String sourceCode) {
    
    SourceMasterHandler handler = (SourceMasterHandler) CacheManager.getInstance().getHandler("CACHE_NAME_SOURCE_MASTER");
    SourceMasterVO vo = handler.getSourceCodeById(sourceCode);
    if(vo!=null)
    {
      jcpFlag = StringUtils.defaultString( vo.getJcpenneyFlag(), "N" ).toUpperCase();
    }else
    {
      jcpFlag = "N";
    }
      
  }
  
    /*******************************************************************************************
     * repCanAccessOrder()
     *******************************************************************************************
      * This method checks to see if the rep is trying to access a preferred partner order and whether or not
      * that rep has access to that type of partner order.
      *
      * @return boolean
      * @throws Exception
      */
      private boolean repCanAccessOrder()
              throws Exception
      {        
          String repCanAccessOrder = null;
          String preferredPartnerName = null;
          String preferredPartnerResource = null;
            
          PartnerVO partnerVO = FTDCommonUtils.getPreferredPartnerBySource(this.sourceCode);
                       
          if( partnerVO != null && partnerVO.getSourceCode() != null && partnerVO.getSourceCode().equalsIgnoreCase(this.sourceCode))
          {
            preferredPartnerName = partnerVO.getPartnerName();
            preferredPartnerResource = partnerVO.getPreferredProcessingResource();
          }
          
          if(  preferredPartnerName != null )
          {
             //retrieve preferred partner resource and check to see if the rep has that role
             SecurityManager securityManager = SecurityManager.getInstance();
             String context = (String) request.getParameter(COMConstants.CONTEXT);
             String token = (String) request.getParameter(COMConstants.SEC_TOKEN);
             
             if (securityManager.assertPermission(context, token, preferredPartnerResource, COMConstants.VIEW))
             {
                 repCanAccessOrder = "Y";
                 pageData.put("repCanAccessOrder", repCanAccessOrder);
             }  
             else
             {
                repCanAccessOrder = "N";
                pageData.put("repCanAccessOrder", repCanAccessOrder);
                pageData.put("orderAccessDeniedMessage", ConfigurationUtil.getInstance().getContentWithFilter(this.con, COMConstants.PREFERRED_PARTNER_CONTEXT, 
                                                                                           COMConstants.PREFERRED_PARTNER_ORDER_ACCESS_RESTRICTION, 
                                                                                           preferredPartnerName, null));
                this.repHasAccess = false;
                //set the XSL parameter, which governs the page to be loaded
                if(this.action.equals(COMConstants.ACTION_SEARCH)) {
                    this.pageData.put("XSL", COMConstants.XSL_CUSTOMER_ORDER_SEARCH);    
                }
                else {
                    this.pageData.put("XSL", COMConstants.XSL_LOSS_PREVENTION_SEARCH);
                }
               return false;
             }
          }
        
        return true;
      }
      
    /*******************************************************************************************
     * repHasPreferredPartnerPermission()
     *******************************************************************************************
      * This method checks to see if the rep is trying to access a preferred partner customer record
      *
      * @return boolean
      * @throws Exception
      */
      private boolean repHasPreferredPartnerPermission()
              throws Exception
      {        
         String repHasPermission = null;
         
         CachedResultSet customerPreferredPartners = null;

         String preferredPartnerResource = null;

         //Instantiate CustomerDAO
         CustomerDAO customerDAO = new CustomerDAO(this.con);
         customerPreferredPartners = customerDAO.getPreferredPartners(customerId);
          
         PreferredPartnerUtility ppu = new PreferredPartnerUtility();
         HashSet pp = ppu.getPreferredPartnersForUser(this.sessionId);
         
         repHasPermission = "Y";
         HashSet<String> partnerAccessDeniedHash = new HashSet<String>();
         if (customerPreferredPartners != null) {
            while(customerPreferredPartners.next())
            {
                preferredPartnerResource = (String) customerPreferredPartners.getString("PREFERRED_PROCESSING_RESOURCE");  
                if (pp == null || (pp != null && !pp.contains((String) customerPreferredPartners.getString("PREFERRED_PROCESSING_RESOURCE")) ))
                {
                    partnerAccessDeniedHash.add((String) customerPreferredPartners.getString("PREFERRED_PROCESSING_RESOURCE"));
                }
            }
         }
           
         int customerPartnerCount = 0;
         int partnerAccessDeniedCount = 0;
         
         customerPartnerCount = customerPreferredPartners.getRowCount();
         partnerAccessDeniedCount = partnerAccessDeniedHash.size();
                
         if (customerPartnerCount > 0)
         {
            if ( customerPartnerCount == 1 ) 
            {
                if ( partnerAccessDeniedCount == 1 )
                {
                    //return specific partner error message
                    pageData.put("orderAccessDeniedMessage", ConfigurationUtil.getInstance().getContentWithFilter(this.con, COMConstants.PREFERRED_PARTNER_CONTEXT, 
                                                                                              COMConstants.PREFERRED_PARTNER_ORDER_ACCESS_RESTRICTION, 
                                                                                              preferredPartnerResource, null));
                    repHasPermission = "N";
                    pageData.put("repCanAccessOrder", repHasPermission);           
                    return false;
                }
            }
            else if ( partnerAccessDeniedCount > 1)
            {
                //return default partner error message
                pageData.put("orderAccessDeniedMessage",  ConfigurationUtil.getInstance().getContentWithFilter(this.con, COMConstants.PREFERRED_PARTNER_CONTEXT, 
                                                                                          COMConstants.PREFERRED_PARTNER_ORDER_ACCESS_RESTRICTION, 
                                                                                          COMConstants.DEFAULT_PREFERRED_PARTNER, null));
                repHasPermission = "N";
                pageData.put("repCanAccessOrder", repHasPermission);
                return false;
            }
        }
        
        return true;
      }


  /*******************************************************************************************
  * retrieveValidatedPrivacyPolicy()
  *******************************************************************************************
  * This method retrieve the validated PPV record
  *
  * @return void
  * @throws Exception
  */
  private void retrieveValidatedPrivacyPolicy() throws Exception
  {        
    //hashmap that will contain the output from the stored proce
    HashMap validatedPrivacyPolicy = new HashMap();

    //Instantiate CustomerDAO
    CustomerDAO customerDAO = new CustomerDAO(this.con);

    //Call SearchCustomers method in the DAO
    validatedPrivacyPolicy = (HashMap)customerDAO.getValidatedPrivacyPolicy(this.timerCallLogId);

    //Create an Document and call a method that will transform the CachedResultSet from the store proce into an Document
    Document validatedCustomersXML = buildXML(validatedPrivacyPolicy, "OUT_VALIDATED_CUSTOMERS", "PPV_VALIDATED_CUSTOMERS", "VALIDATED_CUSTOMER");
    //and store the XML object in a hashXML hashmap
    this.hashXML.put(COMConstants.HK_PPV_VALIDATED_CUSTOMERS_XML, validatedCustomersXML);

    //Create an Document and call a method that will transform the CachedResultSet from the store proce into an Document
    Document validatedOrderDetailsXML = buildXML(validatedPrivacyPolicy, "OUT_VALIDATED_ORDER_DETAILS", "PPV_VALIDATED_ORDER_DETAILS", "VALIDATED_ORDER_DETAIL");
    //and store the XML object in a hashXML hashmap
    this.hashXML.put(COMConstants.HK_PPV_VALIDATED_ORDER_DETAILS_XML, validatedOrderDetailsXML);


    //Store the count in the pageData hashmap
    String bypassPrivPolicyVerifFlag = (String) validatedPrivacyPolicy.get("OUT_BYPASS_PPV_FLAG");
    if (bypassPrivPolicyVerifFlag != null && bypassPrivPolicyVerifFlag.equalsIgnoreCase("Y"))
      this.callLogBypassPrivacyPolicyVerification = true; 

  }
  
  /*******************************************************************************************
  * getCSRPreferredAccess()
  ******************************************************************************************
  * Retrieve and add any preferredPartners this CSR has permissions for
  *
  * @param none
  * @return HashMap
  * @throws Exception
  */
  private void getCSRPreferredAccess() throws Exception
  {

     PreferredPartnerUtility ppu = new PreferredPartnerUtility();
     HashMap repAccess = ppu.getPreferredPartnersAccessForUser(this.sessionId);
     Iterator ppIterator = repAccess.keySet().iterator();
     while(ppIterator.hasNext()) { 
         String ppName = (String)ppIterator.next();
         String accessAllowed = (String) repAccess.get(ppName);
         pageData.put(ppName, accessAllowed);
     }
        
  }
  
  
    /*******************************************************************************************
    * getCustomerPreferredPartners()
    ******************************************************************************************
    * Retrieve preferred partners associated to customer
    *
    * @param none
    * @return HashMap
    * @throws Exception
    */
    private void getCustomerPreferredPartners() throws Exception
    {

        CachedResultSet customerPreferredPartners = null;

        //Instantiate CustomerDAO
        CustomerDAO customerDAO = new CustomerDAO(this.con);
        customerPreferredPartners = customerDAO.getPreferredPartners(customerId);
        
        Document custPreferredPartnerXML = buildXML(customerPreferredPartners, "CUSTOMER_PREFERRED_PARTNER", "PREFERRED_PARTNER");
        
        //and store the XML object in a hashXML hashmap
        this.hashXML.put(COMConstants.HK_CAI_CUSTOMER_PREFERRED_PARTNER_XML, custPreferredPartnerXML);
    }
    
    
	private boolean isOrderFreeShippingMembership(
			String orderDetailId, Connection con) throws Exception 
	{
		if (StringUtils.isEmpty(orderDetailId)) 
		{
			logger.debug("Exiting isOrderFreeShippingMembershio: No Order Detail Id");
			return false; // Nothing to handle
		}

		OrderDAO oDAO = new OrderDAO(con);
		OrderDetailVO odVO = oDAO.getOrderDetail(orderDetailId);

		ServicesProgramDAO servicesOrderBO = new ServicesProgramDAO();

		// Is this a Free Shipping Product?
		return servicesOrderBO.isProductFreeShipping(con, odVO.getProductId());
	}

	
	/*******************************************************************************************
	  * processServicesInfoLookup()
	  ******************************************************************************************
	  * This method will process the services info look up from com and menu page
	  *
	  * @param  isCOM Is the link accessed from COM or menu screen
	  * @return none
	  * @throws Exception
	  *
	  */
	  private void processServicesInfoLookup(boolean isCOM) throws Exception{
		//If accessed from COM, a drop down box with list of email addresses need to be displayed.
		//When accessed from menu screen,email list drop down not processed.
		if(isCOM) {									   
			processEmailListforCustid();
		}
	    this.pageData.put("XSL",  COMConstants.XSL_SERVICES_INFORMATION);
	    //This is used to display the results from the CAMS. 
	    //Results need to be displayed when user clicks on Search button but not when clicked on Services Info link
	    this.pageData.put("SHOW_SUMMARY_AND_FS_HISTORY",  COMConstants.NO);
	  }
	  
	 /**
	  * This method retrieves the list of email addresses for the customer id 
	  * and processes to view as drop down in view page
	  */
	 private void processEmailListforCustid() throws Exception {
		
		CustomerDAO customerDAO = new CustomerDAO(this.con);
		List<String> emailList = customerDAO.getEmailByCustomerId(this.customerId);
		
		this.hashXML.put(COMConstants.HK_EMAIL_LIST_FOR_CUSTOMER_ID_XML, convertEmailListToDOM(emailList));
	    this.pageData.put("SHOW_EMAIL_LIST",  COMConstants.YES);	
	 }
	 
	 /**
	  * This method converts the results from CAMS into document to display
	  */
	 public Document convertEmailListToDOM(List<String> emailList) throws Exception {
		  	
		  DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	      factory.setIgnoringElementContentWhitespace(true);
	      factory.setNamespaceAware(false);
	      DocumentBuilder builder = factory.newDocumentBuilder();
	      Document xmlDocument = builder.newDocument();
	      
	      Element rootElement = xmlDocument.createElement("EMAIL_ADDRESSES");
	      xmlDocument.appendChild(rootElement);
	      int i = 0;
	      
	      if(emailList != null && emailList.size() > 0) {
	         
	    	  for (String emailAddress : emailList) {
			    				    
	    			Element emailInfo = xmlDocument.createElement("EMAIL_INFO");
	    			emailInfo.setAttribute("id",new Integer(++i).toString());
				    rootElement.appendChild(emailInfo);
				    
	    		  Element emailAddressElement = xmlDocument.createElement("EMAIL_ADDRESS");
		       	emailAddressElement.appendChild(xmlDocument.createTextNode(emailAddress));
		       	emailInfo.appendChild(emailAddressElement);
			    
	         }
	      }
	      DOMUtil.convertToString(xmlDocument);
	      return xmlDocument;
	  }
	 
	 
	 /**
	  * This method calls the CAMS service to return FSM summary and history details.
	  * @throws Exception
	  */
	 private void getFSMDetailsForEmail() throws Exception{
		 
		 String emailAddress = this.request.getParameter("email_address");
		 try{
			  GlobalParmHandler gph = (GlobalParmHandler) CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_GLOBAL_PARM);
			 
			  Document fsmHistoryDoc = null;
			  Document fsmSummaryDoc = null;
			  List<String> emailList = new ArrayList<String>(); 
			  emailList.add(emailAddress);
			  
			  //Added this check to fix the defect number 13050.  
			  if (emailAddress == null || emailAddress.equals(""))
			  {
			   	fsmHistoryDoc =  createEmptyServicesDoc();			   				      
			   	fsmSummaryDoc = convertFSMSummaryToDOM(null);
			  } else {
				  List<String> eventTypeList = new ArrayList<String>();
				  eventTypeList.add("fsm_order"); 
				  eventTypeList.add("fs_renewed");
				  eventTypeList.add("fs_member");
				  //Get FSM Summary details from CAMS 
				  CustomerFSMDetails fsmSummaryResponse = FTDCAMSUtils.getFSMDetails(emailAddress);
				  
				  Map<String,CustomerFSMDetails> cfsmDetailsMap = new HashMap<String, CustomerFSMDetails>();
				  if(fsmSummaryResponse != null) {
		           	cfsmDetailsMap.put(emailAddress, fsmSummaryResponse);						  
				  } else {
					  cfsmDetailsMap = new HashMap<String, CustomerFSMDetails>();
				  }
				  fsmSummaryDoc = convertFSMSummaryToDOM(cfsmDetailsMap);
				  DOMUtil.convertToString(fsmSummaryDoc);
				  
				  //Get customer FS history details from CAMS 
				  List<CustomerFSHistory> fsmHistoryResponse = FTDCAMSUtils.getCustomerFSHistory(emailList, eventTypeList);	
				  
				  fsmHistoryDoc =  convertToDOM(fsmHistoryResponse);	
				  DOMUtil.convertToString(fsmHistoryDoc);
			  }
			 			  
			  this.hashXML.put(COMConstants.HK_FSM_SUMMARY_XML, fsmSummaryDoc);
			  this.hashXML.put(COMConstants.HK_FSM_DETAILS_XML, fsmHistoryDoc);
			    //set the XSL parameter, which governs the page to be loaded
			  this.pageData.put("XSL",  COMConstants.XSL_SERVICES_INFORMATION);
			  this.pageData.put("SHOW_SUMMARY_AND_FS_HISTORY",  COMConstants.YES);
			  this.pageData.put("SEARCHED_EMAIL_ADDRESS", emailAddress);
			  if(this.showEmail.equals(COMConstants.YES)) {
				  processEmailListforCustid();
			  }
		  }
		  catch(Exception e)
		  {
			  logger.error("Error occured while connection to CAMS", e);
			  Connection conn = DataSourceUtil.getInstance().getConnection("ORDER SCRUB");
			  SystemMessengerVO sysMessage = new SystemMessengerVO();
			  sysMessage.setLevel(SystemMessengerVO.LEVEL_PRODUCTION);
			  sysMessage.setSource("COM_CAMS_Service");
			  sysMessage.setType("System Exception");
			  sysMessage.setMessage("Error calling CAMS from CustomerOrderSearch->processCAMSService");
			  SystemMessenger.getInstance().send(sysMessage, conn);		
			  
			  this.hashXML.put(COMConstants.HK_FSM_SUMMARY_XML, convertFSMSummaryToDOM(null));
			  this.hashXML.put(COMConstants.HK_FSM_DETAILS_XML, createEmptyServicesDoc());
			    //set the XSL parameter, which governs the page to be loaded
			  this.pageData.put("XSL",  COMConstants.XSL_SERVICES_INFORMATION);
			  this.pageData.put("SHOW_SUMMARY_AND_FS_HISTORY",  COMConstants.YES);
			  this.pageData.put("SEARCHED_EMAIL_ADDRESS", emailAddress);
			  if(this.showEmail.equals(COMConstants.YES)) {
				  processEmailListforCustid();
			  }
		  }
	 }
	 
	 /**
	  * This method converts the CAMS FSM summary details to Document for display purpose.
	  * @param cfsmDetailsMap
	  * @return
	  * @throws Exception
	  */
	 private Document convertFSMSummaryToDOM(Map<String,CustomerFSMDetails> cfsmDetailsMap) throws Exception{
 	    	
		 DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		 factory.setIgnoringElementContentWhitespace(true);
		 factory.setNamespaceAware(false);
		 DocumentBuilder builder = factory.newDocumentBuilder();
		 Document xmlDocument = builder.newDocument();

		 CacheUtil cacheUtil = CacheUtil.getInstance();
		 AccountProgramMasterVO acctProgramMasterVO = new AccountProgramMasterVO(); 
		 acctProgramMasterVO = cacheUtil.getFSAccountProgramDetailsFromContent();  

		 Element rootElement = xmlDocument.createElement("FSM_SUMMARY");
		 xmlDocument.appendChild(rootElement);
		 if(cfsmDetailsMap != null && cfsmDetailsMap.size() > 0) {
			 Iterator<Map.Entry<String,CustomerFSMDetails>> fsmIterator = cfsmDetailsMap.entrySet().iterator();
			 //To maintain number of records in xml
			 int i = 0;
			 while(fsmIterator.hasNext()) {

				 Map.Entry<String,CustomerFSMDetails> mapEntry=(Map.Entry<String,CustomerFSMDetails>)fsmIterator.next();
				 String emailAddress = (String)mapEntry.getKey();
				 logger.debug("emailAddress "+emailAddress);
				 CustomerFSMDetails cfsmDetails = (CustomerFSMDetails)mapEntry.getValue();
				 
				 if(cfsmDetails != null){
					 Element servicesInfo = xmlDocument.createElement("SUMMARY_INFO");
					 servicesInfo.setAttribute("id",new Integer(++i).toString());
					 rootElement.appendChild(servicesInfo);
	
					 Element programName = xmlDocument.createElement("program_name");
					 programName.appendChild(xmlDocument.createTextNode(acctProgramMasterVO.getProgramName()));
					 servicesInfo.appendChild(programName);
	
					 Element displayName = xmlDocument.createElement("display_name");
					 displayName.appendChild(xmlDocument.createTextNode(acctProgramMasterVO.getDisplayName()));
					 servicesInfo.appendChild(displayName);
	
					 /*Element emailAddressNode = xmlDocument.createElement("email_address");
					 emailAddressNode.appendChild(xmlDocument.createTextNode(emailAddress));
					 servicesInfo.appendChild(emailAddressNode);*/
	
					 Element autoRenewalStatusNode = xmlDocument.createElement("auto_renewal_status");
					 String autorenewStatus = "";
					 if(cfsmDetails.getFsAutoRenew().equals("1")) {
						 autorenewStatus = "Opted In";
					 } else if(cfsmDetails.getFsAutoRenew().equals("0")){
						 autorenewStatus = "Opted Out";
					 }
					 autoRenewalStatusNode.appendChild(xmlDocument.createTextNode(autorenewStatus));
					 servicesInfo.appendChild(autoRenewalStatusNode);
	
					 Element accountprogramStatus = xmlDocument.createElement("account_program_status");
					 String programStatus = "";
					 if(cfsmDetails.getFsMember().equals("1")) {
						 programStatus = "Active";
					 } else if(cfsmDetails.getFsMember().equals("0")) {
				       	programStatus = "Expired";
				     }else if(cfsmDetails.getFsMember().equals("2")){
				       	programStatus = "Cancelled";
				     }
					 accountprogramStatus.appendChild(xmlDocument.createTextNode(programStatus));
					 servicesInfo.appendChild(accountprogramStatus);
					 SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
					 
					 Calendar sdCal = Calendar.getInstance();
					 sdCal.setTime(cfsmDetails.getFsStartDate());
					 sdCal.add(sdCal.HOUR, 1);
					 
					 if(cfsmDetails.getFsEndDate() != null){
						 Calendar edCal = Calendar.getInstance();
						 edCal.setTime(cfsmDetails.getFsEndDate());
						 edCal.add(edCal.HOUR, 1);
						 
						 Element expirationDate = xmlDocument.createElement("expiration_date");
						 expirationDate.appendChild(xmlDocument.createTextNode(dateFormat.format(edCal.getTime())));
						 servicesInfo.appendChild(expirationDate);
					 }
					 
					 if(cfsmDetails.getFsJoinDate() != null){
						 Calendar jdCal = Calendar.getInstance();
						 jdCal.setTime(cfsmDetails.getFsJoinDate());
						 jdCal.add(jdCal.HOUR, 1);
		
						 Element startDate = xmlDocument.createElement("start_date");
						 startDate.appendChild(xmlDocument.createTextNode(dateFormat.format(jdCal.getTime())));
						 servicesInfo.appendChild(startDate);
					 }
									 
					 Element programUrl = xmlDocument.createElement("program_url");
					 programUrl.appendChild(xmlDocument.createTextNode(acctProgramMasterVO.getProgramUrl()));
					 servicesInfo.appendChild(programUrl);
	
				/*	 Element externalorderNumber = xmlDocument.createElement("external_order_number");
					 externalorderNumber.appendChild(xmlDocument.createTextNode(cfsmDetails.getFsmOrderNumber())); 
					 servicesInfo.appendChild(externalorderNumber);*/            	           	            		
				}
			}

		 } else {
			 Element servicesInfo = xmlDocument.createElement("SUMMARY_INFO");
			 rootElement.appendChild(servicesInfo);
		 }
		 DOMUtil.convertToString(xmlDocument);
		 return xmlDocument;
	}
	 
	private Document createEmptyServicesDoc() throws Exception{
		 DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	      factory.setIgnoringElementContentWhitespace(true);
	      factory.setNamespaceAware(false);
	      DocumentBuilder builder = factory.newDocumentBuilder();
	      Document xmlDocument = builder.newDocument();
	      
	      Element rootElement = xmlDocument.createElement("SERVICES");
	      xmlDocument.appendChild(rootElement);
	      
	      Element servicesInfo = xmlDocument.createElement("SERVICES_INFO");	      
	      rootElement.appendChild(servicesInfo);
	      return xmlDocument;
	}
	
	/** Marshalls the Object passed in as input parameter.
	 * @param object
	 * @return
	 * @throws Exception
	 */
	public static Document marshallAsXML(Object object) throws Exception {
		Document doc = null;
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(object.getClass());
			Marshaller marshaller = jaxbContext.createMarshaller();
			
			Map<String, Object> props = new HashMap<String, Object>();
    		props.put(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
    		
			if (props != null && props.size() > 0) {
				Set<String> keySet = props.keySet();
				for (String key : keySet) {
					marshaller.setProperty(key, props.get(key));
				}
			}
			StringWriter sw = new StringWriter();

			marshaller.marshal(object, sw);
			ByteArrayInputStream xmlIn = new ByteArrayInputStream(sw.toString().getBytes());
			
			doc = DOMUtil.getDocument(xmlIn);		
			
		} catch (JAXBException e) {
			throw new Exception(e);
		}
		return doc;
	}
	
	/** #12963 - New method to get the email details from already fetched map instead of hitting DB again.
	 * As the customer lands on Customer Account screen, Apollo shows the PC member Icon and other PC related stuff based on E-mails
	 * on customer account, for which we have to pull all e-mails at one shot and send to CAMS to fetch membership data.
	 * Using the same email content fetched in previous request, if the data not found or CAMS call failed we will be redirecting the request
	 * the old method.
	 * @param startSearchingFrom2
	 * @param recordsToBeReturned2
	 * @return
	 * @throws Exception 
	 */
	private Map retrieveCustomerEmailInfo(long startSearchingFrom2, long recordsToBeReturned2) throws Exception {	
		
		ArrayList<CustomerMemberShipVO> custMemberShipList = (ArrayList<CustomerMemberShipVO>) this.request
				.getSession().getAttribute(this.custMembershipData);
		String outIdCount = "0";
		
		if(custMemberShipList == null) {
			buildEmailMembershipData(new CustomerDAO(this.con));
			custMemberShipList = (ArrayList<CustomerMemberShipVO>) this.request.getSession().getAttribute(this.custMembershipData);
		}
		if(custMemberShipList == null) {
			logger.info("Error caught getting PC member ship details, so redirecting to old method retrieveEmailInfo(). Records will be fetched from DB from "
					+ startSearchingFrom2 + " and no of records to be fetched -  " + recordsToBeReturned2);
			return retrieveEmailInfo(startSearchingFrom2, recordsToBeReturned2);
		}
		if(recordsToBeReturned2 > custMemberShipList.size()) {
			recordsToBeReturned2 = custMemberShipList.size();
		}
		
		SortedMap customerEmailResults = getSubMap((int) startSearchingFrom2, (int)recordsToBeReturned2, custMemberShipList);
		
		if(customerEmailResults == null) {
			logger.error("customerEmailResults is empty");
			customerEmailResults = new TreeMap();			
		}
		marshalCustomerEmailData(customerEmailResults);    
		
	    outIdCount = String.valueOf(custMemberShipList.size());
	    customerEmailResults.put("OUT_ID_COUNT", outIdCount);
	    
	    this.pageData.put(COMConstants.PD_CEA_COUNT, outIdCount);
	    this.pageData.put("XSL", COMConstants.XSL_MORE_CUSTOMER_EMAILS);

	    return customerEmailResults;	
	}

	/**
	 * @param startSearchingFrom
	 * @param recordsToBeReturned
	 * @param allCustomerEmailResults
	 * @return
	 */
	private SortedMap getSubMap(int startSearchingFrom, int recordsToBeReturned, ArrayList<CustomerMemberShipVO> allCustomerEmailResults) {
		SortedMap subMap = new TreeMap<String, CustomerMemberShipVO>();
		logger.info("Fetching email details from " + startSearchingFrom + " and records to be returned -  " + recordsToBeReturned);
		for (int i = startSearchingFrom; i <= (startSearchingFrom+recordsToBeReturned) && i <= allCustomerEmailResults.size(); i++) {
			CustomerMemberShipVO customerMemberShipVO = allCustomerEmailResults.get(i-1);
			subMap.put(customerMemberShipVO.getEmailAddress(), allCustomerEmailResults.get(i-1));
		}		
		return subMap;
	}
	

	/**
	 * Build Membership data for the customer email list 
	 * @param customerDAO
	 */
	private void buildEmailMembershipData(CustomerDAO customerDAO) {

		HashMap customerEmailResults = null;
		CachedResultSet emailResult = null;
		List<MembershipDetailsVO> camsMembershipData = null;

		// Replace the membership data in the session to null.
		this.request.getSession().setAttribute(this.custMembershipData, null);

		try {
			customerEmailResults = (HashMap) customerDAO.searchCustomerEmails(
					this.customerId, this.defaultRecordStart, this.defaultRecordFetchAll);
		} catch (Exception e) {
			logger.error("Unable to fetch the customer email Ids." + e.getMessage());
			return;
		}

		if (customerEmailResults == null) {
			logger.error("No Emails associated with the customer.");
			return;
		}

		emailResult = (CachedResultSet) customerEmailResults.get("OUT_CUR");
		if (emailResult == null) {
			logger.error("No Emails associated with the customer.");
			return;
		}

		SortedMap<String, CustomerMemberShipVO> custEmailData =  populateCustEmailData(emailResult);	
		if(custEmailData == null) {
			logger.error("No Emails associated with the customer.");
			return;
		}
		List<CustomerMemberShipVO> custMemberShipList = new ArrayList<CustomerMemberShipVO>(custEmailData.values());

		try {
			getAndPopulateCustMemshipData(custEmailData);			
			this.request.getSession().setAttribute(this.custMembershipData,	custMemberShipList);

		} catch (Exception e) {
			logger.error("Error caught obtaning member ship data.");
			this.pageData.put(COMConstants.PC_MEMSHIP_NOT_DETERMINED, COMConstants.PC_MEMSHIP_NOT_DETERMINED_ERR);
			this.request.getSession().setAttribute(this.custMembershipData, custMemberShipList);
			return;
		}	
	}


	/** Gets the customer member ship data from CAMS and adds membership details to the existing Apollo email data.
	 * @param camsMembershipData
	 * @param custEmailData
	 * @throws Exception
	 */
	private void getAndPopulateCustMemshipData(SortedMap<String, CustomerMemberShipVO> custEmailData) throws Exception {
		
		// For these set of customer e-mails fetch the MembershipData.
		Set emailSet = custEmailData.keySet();
		if (emailSet.size() == 0) {
			return;
		}
		List<String> emails = new ArrayList<String>();
		emails.addAll(emailSet);
		
		Map<String, MembershipDetailsVO> result = FTDCAMSUtils.getMembershipData(emails, null);
		
		if(result != null && result.values() != null && result.values().size() > 0) {		
		
			// For each of the email ID in custEmailData, update the membership data.
			for (MembershipDetailsVO membershipDetailsVO : result.values()) {
				if (membershipDetailsVO != null	&& !StringUtils.isEmpty(membershipDetailsVO.getEmail())) {
					custEmailData.get(membershipDetailsVO.getEmail()).getMembershipList().add(membershipDetailsVO);
					/*if (!isPCMember && FTDCAMSUtils.PREMIERCIRCLE_MEMBER_TYPE.equals(membershipDetailsVO.getMembershipType())) {
						// Set the PC indicator if at-least one email in the order is of PC member ship Type irrespective of itz current status.
						this.pageData.put(COMConstants.PD_CAI_MORE_EMAIL_INDICATOR, "Y");
						if (membershipDetailsVO.isMembershipBenefitsApplicable()) {
							isPCMember = true;
						}
					}*/
				}
			}
		}
		
		
		//logger.info("Customer is Eligible for PC Membership - " );
		
		this.pageData.put(COMConstants.PD_CAI_MORE_EMAIL_INDICATOR, "N");
		this.pageData.put(COMConstants.PC_CUSTOMER_IND, "N");
		
	}


	/** populates the customer email data
	 * @param emailResult
	 * @return
	 */
	private SortedMap<String, CustomerMemberShipVO> populateCustEmailData(CachedResultSet emailResult) {
		SortedMap<String, CustomerMemberShipVO> custEmailData = new TreeMap<String, CustomerMemberShipVO>();
		while (emailResult.next()) {
			CustomerMemberShipVO customerMemberShipVO = new CustomerMemberShipVO();
			customerMemberShipVO.setCustomerId(Long.parseLong(this.customerId));
			customerMemberShipVO.setEmailAddress(emailResult.getString("email_address"));
			customerMemberShipVO.setCompanyId(emailResult.getString("company_name"));
			customerMemberShipVO.setEmailId(emailResult.getInt("email_id"));
			customerMemberShipVO.setSubscribeStatus(emailResult.getString("subscribe_status"));
			Date updatedOn;
			try {
				updatedOn = new SimpleDateFormat("dd/mm/yyyy")
						.parse(emailResult.getString("updated_on"));
				Calendar c = Calendar.getInstance();
				c.setTime(updatedOn);
				customerMemberShipVO.setUpdatedOn(c);
			} catch (ParseException e) {
				logger.error("Error caught converting updatedOn details of an email - "
						+ customerMemberShipVO.getEmailAddress());
			}
			custEmailData.put(customerMemberShipVO.getEmailAddress(), customerMemberShipVO);
		}
		if (custEmailData.size() == 0) {
			return null;
		}		
		return custEmailData;
	}
	
	/**
	 * @param custMembershipData
	 */
	private void marshalCustomerEmailData(SortedMap custMembershipData) {
		CustomerMembershipDetail customerMembershipDetail = new CustomerMembershipDetail();
		customerMembershipDetail.setCustomerMemberShipList(custMembershipData.values());
		try {
			Document customerEmailsXML = marshallAsXML(customerMembershipDetail);
			this.hashXML.put(COMConstants.HK_CEA_CUSTOMER_EMAILS_XML, customerEmailsXML);
			
			//and store the XML object in a hashXML hashmap
		    this.hashXML.put(COMConstants.HK_CEA_CUSTOMER_EMAILS_XML, customerEmailsXML);
		} catch (Exception e) {
			logger.error("Unable to marshal the customerMembershipDetail");			
		}
	}
	
	
	public Map<String, BigDecimal> calculateTaxTotals(CachedResultSet rs) {
		try {
			// Seperate the line item taxes into itemTaxesMap. Eack key is order item and value is its tax vos.			
		    Map<String, ItemTaxVO> itemTaxesMap = new HashMap<String, ItemTaxVO>();
		    Map<String, BigDecimal> taxValues = null;
		    while(rs.next()) {
		    	ItemTaxVO itemTaxVO = null;
		    	List<TaxVO> itemTaxVOs = null;
		    	if(itemTaxesMap.containsKey(rs.getString("order_detail_id"))) {
		    		itemTaxVO = itemTaxesMap.get(rs.getString("order_detail_id"));	    		
		    		itemTaxVOs = itemTaxVO.getTaxSplit();
		    	} else {
		    		itemTaxVO = new ItemTaxVO();	
		    		itemTaxVOs = new ArrayList<TaxVO>();
		    		itemTaxVO.setTaxSplit(itemTaxVOs); 
		    		itemTaxesMap.put(rs.getString("order_detail_id"), itemTaxVO);
		    	}
		    	
		    	if(rs.getInt("display_order") > 0) {
		    		
		    		if("Y".equals(rs.getString("is_total"))) {
		    			itemTaxVO.setTaxAmount(rs.getString("tax_amount"));
		    			itemTaxVO.setTotalTaxDescription((rs.getString("tax_description")));
		    		} else {
		    			TaxVO taxVO = new TaxVO();
		    			taxVO.setDescription(rs.getString("tax_description"));
		    			taxVO.setAmount(rs.getBigDecimal("tax_amount"));
		    			taxVO.setDisplayOrder(rs.getInt("display_order"));
		    			itemTaxVOs.add(taxVO);
		    		}	    		
		    	}	    	
		    }
		    
		    rs.reset(); // We need to reset for item level display.
	    
		   // Construct Map for display, each descirption should have only one amount at cart level.
	       taxValues = new HashMap<String, BigDecimal>();  
	       
	    	for (String orderDetailId : itemTaxesMap.keySet()) {    		
				ItemTaxVO orderItemTaxVO = itemTaxesMap.get(orderDetailId);
				BigDecimal totalTax = null;
				
				if(orderItemTaxVO != null) {
					
					if(orderItemTaxVO.getTaxSplit() != null && orderItemTaxVO.getTaxSplit().size() > 0) {
						for (TaxVO taxVO : orderItemTaxVO.getTaxSplit()) {
							if(taxValues.containsKey(taxVO.getDescription())) {
			        			totalTax =  taxValues.get(taxVO.getDescription());     			
			        		} else {
			        			totalTax = BigDecimal.ZERO;			        			
			        		}
							totalTax = totalTax.add(taxVO.getAmount());
							taxValues.put(taxVO.getDescription(), totalTax);
						}					
					} else if(!StringUtils.isEmpty(orderItemTaxVO.getTaxAmount())) {
	
						if(taxValues.containsKey(orderItemTaxVO.getTotalTaxDescription())) {
			        			totalTax =  (BigDecimal) taxValues.get(orderItemTaxVO.getTotalTaxDescription());       			
			        	} else {
		        			totalTax = BigDecimal.ZERO;		        				        			
		        		}	
						totalTax = totalTax.add(new BigDecimal(orderItemTaxVO.getTaxAmount()));	
						taxValues.put(orderItemTaxVO.getTotalTaxDescription(), totalTax);
		            }
					
				}
				
	    	}  
	    	
	    	Map<String, BigDecimal> sortedTaxValues = new TreeMap<String, BigDecimal>(taxValues);
	    	
			return sortedTaxValues;
		} catch (Exception e) {
			logger.error("Unable to get the total tax values, " , e);
		}
		return null;
	}
	
	
	public Document getCSCICartTaxesDoc(Map<String, BigDecimal> taxes) throws JAXPException {
		try {
			Document doc = JAXPUtil.createDocument();
			Element csiCartTaxes = doc.createElement("CSCI_CART_TAXES");
			doc.appendChild(csiCartTaxes);
	
			for (String desc : taxes.keySet()) {
	
				Element csiCartTax = doc.createElement("CSCI_CART_TAX");
				csiCartTaxes.appendChild(csiCartTax);
	
				Element descElement = doc.createElement("tax_description");
				csiCartTax.appendChild(descElement);
				descElement.appendChild(doc.createTextNode(desc));
	
				Element amountElement = doc.createElement("tax_amount");
				csiCartTax.appendChild(amountElement);
				
				amountElement.appendChild(doc.createTextNode(taxes.get(desc).toString()));
			}
			return doc;
		} catch (Exception e) {
			logger.error("Unable to document the cart taxes, " , e);
		}
		return null;
	}
	
	/** Process PRO Order search for three possible cases
	 * 1. When no records found
	 * 2. when a single record found - redirect to customer cart page
	 * 3. when multiple records found - redirect to customer account page displaying all orders of that customer account.
	 * @throws Exception
	 */
	private void processProOrderSearch() throws Exception {
		int proOrderCount = Integer.parseInt(((String) this.searchCriteria.get("PRO_ORDER_COUNT")));	
		
		if(proOrderCount == 0) { 
			processNoRecFound();			
		} else if(proOrderCount == 1) {
			logger.debug("Single order found for a given web order number: " + searchCriteria.get(COMConstants.SC_PRO_ORDER_NUMBER));
			processSearch();			
		} else {
			logger.debug("Multiple records found for a given web order number: " + searchCriteria.get(COMConstants.SC_PRO_ORDER_NUMBER));
			processProMultiOrderSearch();
		}		
	}
	
	

	  private void processNoRecFound() {
		  logger.debug("No order found for a given web order number: " + searchCriteria.get(COMConstants.SC_PRO_ORDER_NUMBER));
			
			// Store the count in the pageData hashmap 
			this.pageData.put(COMConstants.PD_COS_COUNT, "0");
			this.pageData.put("XSL", COMConstants.XSL_CUSTOMER_ORDER_SEARCH);
			this.pageData.put(COMConstants.PD_MESSAGE_DISPLAY, COMConstants.MSG_NO_RECORDS);
		
	  	}


	private void processProMultiOrderSearch() throws Exception  {
		
		setBeginEndParameters(this.currentCOSPagePosition, this.maxCustomersPerPage);
	    this.newPagePosition = this.currentCOSPagePosition + 1;
	    
	    CustomerDAO customerDAO = new CustomerDAO(this.con);
	    String proSearchDelimiter = "-";
	    try {
	    	proSearchDelimiter = ConfigurationUtil.getInstance().getFrpGlobalParm(COMConstants.GLOBAL_CONFIG, COMConstants.PRO_ORDER_SEARCH_APPENDER);
		} catch (Exception e) {
			logger.info("Error getting the PRO_ORDER_SEARCH_APPENDER from cache defaulting to - ");
		}
	    //CachedResultSet custRecord = (CachedResultSet) customerDAO.retrieveProOrderCustomer((String)searchCriteria.get(COMConstants.SC_PRO_ORDER_NUMBER));
	    HashMap custSearchResults = (HashMap) customerDAO.searchProCustomer(((String) searchCriteria.get(COMConstants.SC_PRO_ORDER_NUMBER)).concat(proSearchDelimiter), this.startSearchingFrom, this.recordsToBeReturned);
	    
	    //set the begin and max allowed parameters, to be passed to the DAOs.
	    setBeginEndParameters(this.newPagePosition, this.maxCustomersPerPage);
	    
    	String sFtdCustomer = null;
 	    String sPreferredCustomer = null;
 	   String sUSAACustomer = null;
 	   String sUSAARecipient = null;
 	    BigDecimal idCount = BigDecimal.ZERO;
 	    CachedResultSet crs = null;
 	    if (custSearchResults!=null) {
 	    	idCount =  (BigDecimal) custSearchResults.get("OUT_ID_COUNT");
	    	crs = (CachedResultSet) custSearchResults.get("OUT_CUR");
 	    }
	    Long outIdCount = idCount.longValue();
	    
	    if(crs!=null) {
	    	if (outIdCount == 1) {
		    	
		 	    if(crs.next()) {
		 	    	this.customerId = crs.getString("CUSTOMER_ID");
		 	    	sFtdCustomer = crs.getString("FTD_CUSTOMER");
		 	    	sPreferredCustomer = crs.getString("PREFERRED_CUSTOMER");
		 	    	sUSAACustomer = crs.getString("USAA_CUSTOMER");
		 	    	sUSAARecipient = crs.getString("USAA_RECIPIENT");
		 	    }
		 	        
		 	    String repCanAccessOrder = "N";	 
		 	    
		 	    if( (sFtdCustomer != null && sFtdCustomer.equals("N")) 
		 	    		&& (sPreferredCustomer !=null && sPreferredCustomer.equals("Y")) &&  !repHasPreferredPartnerPermission()) {	       
		 	        repCanAccessOrder = "N";	       
		 	    } else {
		 	        repCanAccessOrder = "Y";	       
		 	    }
		 	    
		 	    pageData.put("repCanAccessOrder", repCanAccessOrder);
		 	    if(!StringUtils.isEmpty(this.customerId) && "Y".equalsIgnoreCase(repCanAccessOrder)) {
		 	      processCustomerSearch();
		 	    } else {
		 	    	processNoRecFound();
		 	    }
		    }
		     
		    // more than one customer record found
		    else if(outIdCount > 1 && outIdCount <= this.maxRecordsAllowed) {	     
		    	this.pageData.put("XSL", COMConstants.XSL_MULTIPLE_RECORDS_FOUND);
		    	setButtons(outIdCount, newPagePosition, this.maxCustomersPerPage, "cos");
		    	this.pageData.put(COMConstants.PD_COS_START_POSITION, String.valueOf(startSearchingFrom));
		    	this.pageData.put(COMConstants.PD_COS_TOTAL_RECORDS_FOUND, String.valueOf(outIdCount));
		    	
		    	 Document searchCustomerXML =
		    	          buildXML(custSearchResults, "OUT_CUR", "SC_SEARCH_CUSTOMERS", "SC_SEARCH_CUSTOMER");
		    	    //and store the XML object in a hashXML hashmap
		    	    this.hashXML.put(COMConstants.HK_COS_CUSTOMER_ORDER_SEARCH_XML, searchCustomerXML);
		    } 
			// More than max records allowed
			else if(outIdCount > this.maxRecordsAllowed) {
				this.pageData.put("XSL", COMConstants.XSL_CUSTOMER_ORDER_SEARCH); 
				this.pageData.put(COMConstants.PD_MESSAGE_DISPLAY, COMConstants.MSG_TOO_MANY);
			} else {
				processNoRecFound();
			}
	    } else {
			processNoRecFound();
		}
	}
	
    /*******************************************************************************************
     * repCanAccessUSAAOrder()
     *******************************************************************************************
      * This method checks to see if the rep has USAA access.
      *
      * @return boolean
      * @throws Exception
      */
      private boolean repCanAccessUSAAOrder() throws Exception
      {        
    	  String repCanAccessOrder = null;
          String preferredPartnerName = null;
          String preferredPartnerResource = null;
            
          PartnerVO partnerVO = FTDCommonUtils.getPreferredPartnerBySource("USAA");
                       
          if( partnerVO != null && partnerVO.getSourceCode() != null && partnerVO.getSourceCode().equalsIgnoreCase("USAA"))
          {
            preferredPartnerName = partnerVO.getPartnerName();
            preferredPartnerResource = partnerVO.getPreferredProcessingResource();
          }
          
          if(  preferredPartnerName != null )
          {
        	 //retrieve preferred partner resource and check to see if the rep has that role
             SecurityManager securityManager = SecurityManager.getInstance();
             String context = (String) request.getParameter(COMConstants.CONTEXT);
             String token = (String) request.getParameter(COMConstants.SEC_TOKEN);
             
             if (securityManager.assertPermission(context, token, preferredPartnerResource, COMConstants.VIEW))
             {
                 repCanAccessOrder = "Y";
                 pageData.put("repCanAccessOrder", repCanAccessOrder);
             }  
             else
             {
                repCanAccessOrder = "N";
                pageData.put("repCanAccessOrder", repCanAccessOrder);
                pageData.put("orderAccessDeniedMessage", ConfigurationUtil.getInstance().getContentWithFilter(this.con, COMConstants.PREFERRED_PARTNER_CONTEXT, 
                                                                                           COMConstants.PREFERRED_PARTNER_ORDER_ACCESS_RESTRICTION, 
                                                                                           preferredPartnerName, null));
                this.repHasAccess = false;
                //set the XSL parameter, which governs the page to be loaded
                if(this.action.equals(COMConstants.ACTION_SEARCH)) {
                    this.pageData.put("XSL", COMConstants.XSL_CUSTOMER_ORDER_SEARCH);    
                }
                else {
                    this.pageData.put("XSL", COMConstants.XSL_LOSS_PREVENTION_SEARCH);
                }
               return false;
             }
          }
        return true;
      }
      
      /*******************************************************************************************
       * deleteCsrViewed()
       *******************************************************************************************/
        private void deleteCsrViewed(String csrId, String orderGuid, String orderDetailId, String customerId, String sessionId) throws Exception
        {
          ViewDAO viewDAO = new ViewDAO(this.con);
          HashMap csrDeleteInfo = (HashMap) viewDAO.deleteCsrViewed(csrId, orderGuid, orderDetailId, customerId, sessionId);
        }  
      
      
}