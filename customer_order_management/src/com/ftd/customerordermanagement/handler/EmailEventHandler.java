package com.ftd.customerordermanagement.handler;

import com.ftd.customerordermanagement.bo.UpdateCustomerEmailBO;
import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.newsletterevents.bo.IEventHandler;
import com.ftd.newsletterevents.vo.INewsletterEvent;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.sql.Connection;
import com.ftd.osp.utilities.plugins.Logger;

public class EmailEventHandler extends UnicastRemoteObject implements IEventHandler
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected static Logger logger = new Logger(EmailEventHandler.class.getName());
    /*
     * Empty constructor
     */
    public EmailEventHandler() throws RemoteException
    {
    
    }

    public void processMessage(INewsletterEvent neVO)
    {
      Connection con = null;
      try
      {
   
        //Connection/Database info
        con = DataSourceUtil.getInstance().getConnection(
            ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE,
            COMConstants.DATASOURCE_NAME));

        UpdateCustomerEmailBO uceBO = new UpdateCustomerEmailBO(con);
        uceBO.remoteUpdateEmailStatus(neVO);      
      }
      catch (Exception e)
      {
        logger.error(e);
      }
      finally
      {
        try
        {
          //Close the connection
          if(con != null)
          {
            con.close();
          }
        }
        catch(Exception e)
        {
          logger.error(e);
        }
      }
    }
}
