package com.ftd.customerordermanagement.constants;


import com.ftd.osp.utilities.plugins.Logger;

/**
 * Static information for queues
 */

public class COMConstants
{

	private Logger logger = new Logger("com.ftd.customerordermanagement.COMConstants");


    public static final String EMPTY_STRING                                 = "";

    /*********************************************************************************************
    //config flie locations
    *********************************************************************************************/
    public static final String PROPERTY_FILE                                = "customer_order_management_config.xml";
    public static final String SECURITY_FILE                                = "security-config.xml";
    public static final String DATASOURCE_NAME                              = "DATASOURCE_NAME";
    public static final String EMAIL_ORIGINS                                = "EMAIL_ORIGINS";
    public static final String PARTNER_ORIGINS                              = "PARTNER_ORIGINS";
    public static final String COM_CONTEXT                                  = "CUSTOMER_ORDER_MANAGEMENT";
    public static final String CCAS_CONTEXT                                 = "CCAS_CONFIG";


    /*********************************************************************************************
    //Request Parameters
    *********************************************************************************************/
    public static final String AB_ACTION_DECLINE_CREDIT                     = "decline_credit";
    public static final String AB_ACTION_UNDER_MIN_AUTH_AMT                 = "under_min_auth_amt";
    public static final String AB_ACTION_DECLINE_GCC                        = "decline_gcc";
    public static final String AB_REASON_DECLINE                            = "declined";
    public static final String AB_REASON_MSG                                = "CANNOT_AUTHORIZE";
    public static final String AB_STATUS_SUCCESS                            = "success";
    public static final String AB_STATUS_FAILURE                            = "failure";
    public static final String ACTION                                       = "action";
    public static final String ACTION_AMOUNT_OWED                           = "amount_owed";
    public static final String ACTION_DECLINE_CREDIT                        = "decline_credit";
    public static final String ACTION_DECLINE_CREDIT_MS                     = "decline_credit_ms";
    public static final String ACTION_ERROR_CREDIT                          = "error_credit";
    public static final String ACTION_ERROR_CREDIT_MS                       = "error_credit_ms";
    public static final String ACTION_DECLINE_GCC                           = "decline_gcc";
    public static final String ACTION_DECLINE_GCCR                          = "decline_gccr";
    public static final String ACTION_DECLINE_ICC                           = "decline_icc";
    public static final String ACTION_DECLINE_NC                            = "decline_nc";
    public static final String ACTION_TYPE                                  = "action_type";
    public static final String ADMIN_ACTION                                 = "adminAction";
    public static final String PRICE_INCREASE_AUTH_THRESHOLD				= "PRICE_INCREASE_AUTH_THRESHOLD";
    public static final String AUTO_EMAIL_FLAG                              = "autoEmail";
    public static final String BILLING_LEVEL_CART                           = "cart";
    public static final String BUYER_FULL_NAME                              = "buyer_full_name";
    public static final String BYPASS_COM                                   = "bypass_com";
    public static final String CAI_CURRENT_PAGE                             = "cai_current_page";
    public static final String CATEGORY_INDEX                               = "category_index";
    public static final String CEA_CURRENT_PAGE                             = "cea_current_page";
    public static final String CC_DECLINE_MESSAGE                           = "cc_decline_msg";
    public static final String CC_UNDER_MIN_AUTH_AMT_MESSAGE                = "cc_under_min_auth_amt_msg";
    public static final String CLASS_CHANGE                                 = "class_change";
    public static final String COLOR_FIRST_CHOICE                           = "color_first_choice";
    public static final String COLOR_SECOND_CHOICE                          = "color_second_choice";
    public static final String COMMENT_ID                                   = "comment_id";
    public static final String COMMENT_ORIGIN                               = "comment_origin";
    public static final String COMMENT_REASON                               = "comment_reason";
    public static final String COMMENT_TEXT                                 = "comment_text";
    public static final String COMMENT_TYPE                                 = "comment_type";
    public static final String COMPANY_ID                                   = "company_id";
    public static final String COMPLAINT_COMM_ORIGIN_TYPE_ID                = "complaint_comm_origin_type_id";
    public static final String COMPLAINT_COMM_NOTIFICATION_TYPE_ID          = "complaint_comm_notification_type_id";
    public static final String COS_CURRENT_PAGE                             = "cos_current_page";
    public static final String COS_TOTAL_RECORDS_FOUND                      = "cos_total_records_found";
    public static final String CREATED_BY                                   = "created_by";
    public static final String CSCI_CURRENT_PAGE                            = "csci_current_page";
    public static final String CSCI_ORDER_POSITION                          = "csci_order_position";
    public static final String CUSTOMER_EMAIL_ADDRESS                       = "customer_email_address";
    public static final String EMAIL_ADDRESS                                = "email_address";
    public static final String CUSTOMER_ALT_EMAIL_ADDRESS                   = "customer_alt_email_address";
    public static final String CUSTOMER_FIRST_NAME                          = "customer_first_name";
    public static final String CUSTOMER_HISTORY_TYPE                        = "Customer";
    public static final String CUSTOMER_ID                                  = "customer_id";
    public static final String CUSTOMER_LAST_NAME                           = "customer_last_name";
    public static final String DATA_EXISTS                                  = "data_exists";
    public static final String DELIVERY_DATE                                = "delivery_date";
    public static final String DELIVERY_DATE_RANGE_END                      = "delivery_date_range_end";
    public static final String DISCOUNT_AMOUNT                              = "discount_amount";
    public static final String DISPLAY_ORDER_NUMBER                         = "display_order_number";
    public static final String DISPLAY_PAGE                                 = "display_page";
    public static final String DNIS_COUNTRY_SCRIPT_ID                       = "script_code";
    public static final String DNIS_DESCRIPTION                             = "description";
    public static final String DNIS_ID                                      = "dnis_id";
    public static final String DNIS_OE_FLAG                                 = "oe_flag";
    public static final String DNIS_ORIGIN                                  = "origin";
    public static final String DNIS_SCRIPT_ID                               = "script_id";
    public static final String DNIS_SOURCE_CODE                             = "default_source_code";
    public static final String DNIS_STATUS                                  = "status_flag";
    public static final String DNIS_YP_FLAG                                 = "yellow_pages_flag";
    public static final String DOMESTIC_SRVC_FEE                            = "domestic_srvc_fee";
    public static final String EMAIL_MESSAGE_TYPE                           = "Email";
    public static final String EMAIL_REPLY_FLAG                             = "replyFlag";
    public static final String ENTITY_TYPE                                  = "entity_type";
    public static final String ENTITY_ID                                    = "entity_id";
    public static final String ERROR_DISPLAY_PAGE                           = "error_display_page";
    public static final String EXCEPTION_CODE                               = "exception_code";
    public static final String EXCEPTION_END_DATE                           = "exception_end_date";
    public static final String EXCEPTION_START_DATE                         = "exception_start_date";
    public static final String EXTERNAL_ORDER_NUMBER                        = "external_order_number";
    public static final String FLORIST_COMMENTS                             = "florist_comments";
    public static final String FLORIST_ID                                   = "florist_id";
    public static final String FLORIST_TYPE                                 = "florist_type";
    public static final String GCC_CANCELLED_MSG                            = "gc_cancelled_msg";
    public static final String GCC_EXPIRED_MSG                              = "gc_expired_msg";
    public static final String GCC_INACTIVE_MSG                             = "gc_inactive_msg";
    public static final String GCC_INVALID_MSG                              = "gc_invalid_msg";
    public static final String GCC_REDEEMED_MSG                             = "gc_redeemed_msg";
    public static final String GCC_STATUS_CANCELLED                         = "cancelled";
    public static final String GCC_STATUS_ISSUED_EXPIRED                    = "issued expired";
    public static final String GCC_STATUS_ISSUED_INACTIVE                   = "issued inactive";
    public static final String GCC_STATUS_REDEEMED                          = "redeemed";
    public static final String GCC_STATUS_REFUND                            = "refund";
    public static final String GCC_REFUND_MSG                               = "gc_refund_msg";
    public static final String GCC_ERROR_MSG                                = "gc_error_msg";
    public static final String GLOBAL_AUTH_FLAG                             = "AUTH_FLAG";
    public static final String GLOBAL_AUTH_FLAG_ON                          = "Y";
    public static final String GLOBAL_CONTEXT                               = "ORDER_PROCESSING";
    public static final String HOLD_REASON_REAUTH                           = "RE-AUTH";
    public static final String HOLD_STATUS_NOT_PROCESSED                    = "P";
    public static final String HOLD_STATUS_PROCESSED                        = "N";
    public static final String HIDE_SYSTEM_COMMENTS                         = "hide_system";
    public static final String HISTORY_TYPE                                 = "history_type";
    public static final String IGNORE_ERROR                                 = "ignore_error";
    public static final String IN_CC_NUMBER                                 = "in_cc_number";
    public static final String IN_CUST_IND                                  = "in_cust_ind";
    public static final String IN_CUST_NUMBER                               = "in_cust_number";
    public static final String IN_EMAIL_ADDRESS                             = "in_email_address";
    public static final String IN_LAST_NAME                                 = "in_last_name";
    public static final String IN_MEMBERSHIP_NUMBER                         = "in_rewards_number";
    public static final String IN_PC_MEMBERSHIP                         	= "in_pc_membership";
    public static final String IN_ORDER_NUMBER                              = "in_order_number";
    public static final String IN_PHONE_NUMBER                              = "in_phone_number";
    public static final String IN_RECIP_IND                                 = "in_recip_ind";
    public static final String IN_TRACKING_NUMBER                           = "in_tracking_number";
    public static final String IN_ZIP_CODE                                  = "in_zip_code";
    public static final String IN_AP_ACCOUNT                                = "in_ap_account";
    public static final String INCLUDE_COMMENTS                             = "include_comments";
    public static final String INTERNATIONAL_SRVC_FEE                       = "international_srvc_fee";
    public static final String JNDI_USERTRANSACTION_ENTRY                   = "jndi_usertransaction_entry";
    public static final String LAST_ORDER_DATE                              = "last_order_date";
    public static final String LETTER_MESSAGE_TYPE                          = "Letter";
    public static final String LINE_NUMBER                                  = "line_number";
    public static final String LIST_FLAG                                    = "list_flag";
    public static final String LP_PERMISSION                                = "loss_prevention_permission";
    public static final String MA_DECLINE_MESSAGE                           = "Invalid Approval Code";
    public static final String MASTER_ORDER_NUMBER                          = "master_order_number";
    public static final String MEMBERSHIP_ID                                = "membership_id";
    public static final String MESSAGE_CONTENT                              = "message_content";
    public static final String MESSAGE_ID                                   = "message_id";
    public static final String MESSAGE_LINE_NUMBER                          = "message_line_number";
    public static final String MESSAGE_SUBJECT                              = "message_subject";
    public static final String MESSAGE_TITLE                                = "message_title";
    public static final String MESSAGE_TYPE                                 = "message_type";
    public static final String MGR_ONLY_COMMENTS                            = "mgr_only";
    public static final String MSG_TYPE                                     = "msg_type";
    public static final String NEW_MESSAGE_FLAG                             = "newMessageFlag";
    public static final String OCCASION                                     = "occasion";
    public static final String OCCASION_ID                                  = "occasion_id";
    public static final String OCCASION_TEXT                                = "occasion_description";
    public static final String ORDER_DATE                                   = "order_date";
    public static final String ORDER_DETAIL_ID                              = "order_detail_id";
    public static final String ORDER_DISP_CODE                              = "order_disp_code";
    public static final String ORDER_GUID                                   = "order_guid";
    public static final String ORDER_HISTORY_TYPE                           = "Order";
    public static final String ORDER_LEVEL                                  = "order_level";
    public static final String ORDER_NUMBER                                 = "order_number";
    public static final String ORDER_TOTAL                                  = "order_total";
    public static final String ORIGIN_ID                                    = "origin_id";
    public static final String ORIGIN_UPDATE_FLAG                           = "originChangeFlag";
    public static final String ORIG_PRODUCT_AMOUNT                          = "orig_product_amount";
    public static final String ORIG_PRODUCT_ID                              = "orig_product_id";
    public static final String ORIG_SOURCE_CODE                             = "orig_source_code";
    public static final String ORIG_SHIP_METHOD                             = "orig_ship_method";
    public static final String PAGE_ACTION                                  = "page_action";
    public static final String PAGE_FLORIST_ID                              = "page_florist_id";
    public static final String PAGE_NAME                                    = "page_name";
    public static final String PAGE_NUMBER                                  = "page_number";
    public static final String PAGE_POSITION                                = "page_position";
    public static final String PAGE_SHIP_METHOD                             = "page_ship_method";
    public static final String PARTNER_ID                                   = "partner_id";
    public static final String PAY_INFO_NUMBER                              = "pay_info_number";
    public static final String POINT_OF_CONTACT_ID                          = "poc_id";
    public static final String PREV_PAGE                                    = "prev_page";
    public static final String PREV_PAGE_POSITION                           = "prev_page_position";
    public static final String PRICE_POINT_ID                               = "price_point_id";
    public static final String PRICING_CODE                                 = "pricing_code";
    public static final String PRODUCT_AMOUNT                               = "product_amount";
    public static final String PRODUCT_ID                                   = "product_id";
    public static final String PRODUCT_SUB_TYPE                             = "product_sub_type";
    public static final String PRODUCT_TYPE                                 = "product_type";
    public static final String QUANTITY                                     = "quantity";
    public static final String RECIPIENT_CITY                               = "recipient_city";
    public static final String RECIPIENT_COUNTRY                            = "recipient_country";
    public static final String RECIPIENT_FLAG                               = "recipient_flag";
    public static final String RECIPIENT_STATE                              = "recipient_state";
    public static final String ORIG_RECIPIENT_STATE                         = "orig_recipient_state";
    public static final String RECIPIENT_ZIP_CODE                           = "recipient_zip_code";
    public static final String REFUND_DISP_CODE                             = "refund_disp_code";
    public static final String REFUND_REVIEW_PAGE                           = "refundReview";
    public static final String REMOVE_EMAIL_QUEUE_FLAG                      = "remove_email_queue_flag";
    public static final String RESPONSIBLE_PARTY                            = "responsible_party";
    public static final String RESOURCE_CONTEXT                             = "Order Proc";
    public static final String RESOURCE_NO_CHARGE                           = "NoCharge";
    public static final String RESOURCE_NO_CHARGE_PERMISSION                = "Yes";
    public static final String REWARD_TYPE                                  = "reward_type";
    public static final String SCRIPT_CODE                                  = "script_code";
    public static final String SEARCH_KEYWORD                               = "searchKeyword";
    public static final String SEARCH_TYPE                                  = "search_type";
    public static final String SEARCHED_ON_EXTERNAL_ORDER_NUMBER            = "searched_on_external_order_number";
    public static final String SHIP_DATE                                    = "ship_date";
    public static final String SHIP_METHOD                                  = "ship_method";
    public static final String SHIP_METHOD_CARRIER                          = "ship_method_carrier";
    public static final String SHIP_METHOD_FLORIST                          = "ship_method_florist";
    public static final String SIZE_INDICATOR                               = "size_indicator";
    public static final String SNH_ID                                       = "snh_id";
    public static final String SORT_TYPE                                    = "sort_type";
    public static final String SOURCE_CODE                                  = "source_code";
    public static final String SPECIAL_INSTRUCTIONS                         = "special_instructions";
    public static final String STATUS_SUCCESS                               = "valid";
    public static final String STATUS_INVALID                               = "invalid";
    public static final String SUBCODE                                      = "subcode";
    public static final String SUBSTITUTION_INDICATOR                       = "substitution_indicator";
    public static final String START_POSITION                               = "start_position";
    public static final String SPELL_CHECK_FLAG                             = "spellCheckFlag";
    public static final String TRANSACTION_TIMEOUT_IN_SECONDS               = "transaction_timeout_in_seconds";
    public static final String PREMIER_CIRCLE_MEMBERSHIP               		= "premier_circle_membership";

    public static final String UC_BUSINESS_NAME                             = "uc_business_name";
    public static final String UC_CUSTOMER_ADDRESS_1                        = "uc_customer_address_1";
    public static final String UC_CUSTOMER_ADDRESS_2                        = "uc_customer_address_2";
    public static final String UC_CUSTOMER_BIRTHDAY                         = "uc_customer_birthday";
    public static final String UC_CUSTOMER_CITY                             = "uc_customer_city";
    public static final String UC_CUSTOMER_COUNTY                           = "uc_customer_county";
    public static final String UC_CUSTOMER_COUNTRY                          = "uc_customer_country";
    public static final String UC_CUSTOMER_FIRST_NAME                       = "uc_customer_first_name";
    public static final String UC_CUSTOMER_ID                               = "uc_customer_id";
    public static final String UC_CUSTOMER_LAST_NAME                        = "uc_customer_last_name";
    public static final String UC_CUSTOMER_STATE                            = "uc_customer_state";
    public static final String UC_CUSTOMER_ZIP_CODE                         = "uc_customer_zip_code";
    public static final String UC_DAYTIME_CUSTOMER_EXTENSION                = "uc_daytime_customer_extension";
    public static final String UC_DAYTIME_CUSTOMER_PHONE_ID                 = "uc_daytime_customer_phone_id";
    public static final String UC_DAYTIME_CUSTOMER_PHONE_NUMBER             = "uc_daytime_customer_phone_number";
    public static final String UC_DAYTIME_CUSTOMER_PHONE_TYPE               = "uc_daytime_customer_phone_type";
    public static final String UC_DIRECT_MAIL_COMPANY_ID                    = "uc_direct_mail_company_id";
    public static final String UC_DIRECT_MAIL_SUBSCRIBE_STATUS              = "uc_direct_mail_subscribe_status";
    public static final String UC_DIRECT_MAIL_SUBSCRIBE_STATUS_COUNT        = "uc_direct_mail_subscribe_status_count";
    public static final String UC_EVENING_CUSTOMER_EXTENSION                = "uc_evening_customer_extension";
    public static final String UC_EVENING_CUSTOMER_PHONE_ID                 = "uc_evening_customer_phone_id";
    public static final String UC_EVENING_CUSTOMER_PHONE_NUMBER             = "uc_evening_customer_phone_number";
    public static final String UC_EVENING_CUSTOMER_PHONE_TYPE               = "uc_evening_customer_phone_type";
    public static final String UC_MASTER_ORDER_NUMBER                       = "uc_master_order_number";
    public static final String UC_ORDER_GUID                                = "uc_order_guid";
    public static final String UC_VIP_CUSTOMER								= "uc_vip_customer";

    public static final String UCE_COMPANY_ID                               = "uce_company_id";
    public static final String UCE_COMPANY_ID_COUNT                         = "uce_company_id_count";
    public static final String UCE_COMPANY_NAME                             = "uce_company_name";
    public static final String UCE_CURRENT_PAGE                             = "uce_current_page";
    public static final String UCE_CUSTOMER_ID                              = "uce_customer_id";
    public static final String UCE_EMAIL_ADDRESS                            = "uce_email_address";
    public static final String UCE_EMAIL_ID                                 = "uce_email_id";
    public static final String UCE_SUBSCRIBE_STATUS                         = "uce_subscribe_status";

    public static final String UDI_ALT_CONTACT_EXTENSION                    = "alt_contact_extension";
    public static final String UDI_ALT_CONTACT_FIRST_NAME                   = "alt_contact_first_name";
    public static final String UDI_ALT_CONTACT_LAST_NAME                    = "alt_contact_last_name";
    public static final String UDI_ALT_CONTACT_PHONE_NUMBER                 = "alt_contact_phone_number";
    public static final String UDI_ALT_CONTACT_EMAIL                        = "alt_contact_email";
    public static final String UDI_ALT_CONTACT_INFO_ID                      = "alt_contact_info_id";
    public static final String UDI_CARD_MESSAGE                             = "card_message";
    public static final String UDI_CARD_SIGNATURE                           = "card_signature";
    public static final String UDI_DELIVERY_DATE                            = "delivery_date";
    public static final String UDI_DELIVERY_DATE_RANGE_END                  = "delivery_date_range_end";
    public static final String UDI_FLORIST_ID                               = "florist_id";
    public static final String UDI_MEMBERSHIP_FIRST_NAME                    = "membership_first_name";
    public static final String UDI_MEMBERSHIP_LAST_NAME                     = "membership_last_name";
    public static final String UDI_MEMBERSHIP_NUMBER                        = "membership_number";
    public static final String UDI_OCCASION                                 = "occasion";
    public static final String UDI_OCCASION_ID                              = "occasion_id";
    public static final String UDI_OCCASION_DESCRIPTION                     = "occasion_description";
    public static final String UDI_ORDER_DETAIL_ID                          = "order_detail_id";
    public static final String UDI_ORDER_GUID                               = "order_guid";
    public static final String UDI_ORIGIN_ID                                = "origin_id";
    public static final String UDI_PRODUCT_ID                               = "product_id";
    public static final String UDI_RECIPIENT_ADDRESS_1                      = "recipient_address_1";
    public static final String UDI_RECIPIENT_ADDRESS_2                      = "recipient_address_2";
    public static final String UDI_RECIPIENT_ADDRESS_CODE                   = "recipient_address_code";
    public static final String UDI_RECIPIENT_ADDRESS_TYPE                   = "recipient_address_type";
    public static final String UDI_RECIPIENT_CITY                           = "recipient_city";
    public static final String UDI_RECIPIENT_BUSINESS_NAME                  = "recipient_business_name";
    public static final String UDI_RECIPIENT_BUSINESS_INFO                  = "recipient_business_info";
    public static final String UDI_RECIPIENT_COUNTRY                        = "recipient_country";
    public static final String UDI_RECIPIENT_EXTENSION                      = "recipient_extension";
    public static final String UDI_RECIPIENT_FIRST_NAME                     = "recipient_first_name";
    public static final String UDI_RECIPIENT_ID                             = "recipient_id";
    public static final String UDI_RECIPIENT_LAST_NAME                      = "recipient_last_name";
    public static final String UDI_RECIPIENT_PHONE_NUMBER                   = "recipient_phone_number";
    public static final String UDI_RECIPIENT_STATE                          = "recipient_state";
    public static final String UDI_RECIPIENT_ZIP_CODE                       = "recipient_zip_code";
    public static final String UDI_SHIP_METHOD                              = "ship_method";
    public static final String UDI_NO_TAX_FLAG                              = "no_tax_flag";
    public static final String UDI_SHIP_DATE                                = "ship_date";
    public static final String UDI_SHIP_METHOD_DESC                         = "ship_method_desc";
    public static final String UDI_SHIPPING_FEE                             = "shipping_fee";
    public static final String UDI_SKIP_FLORIST_VALIDATION                  = "skip_florist_validation";
    public static final String UDI_SOURCE_CODE                              = "source_code";
    public static final String UDI_SOURCE_CODE_DESCRIPTION                  = "source_code_description";
    public static final String UDI_SPECIAL_INSTRUCTIONS                     = "special_instructions";
    public static final String UDI_SPEC_ROOM_NUMBER                         = "special_instructions_room_number";
    public static final String UDI_SPEC_START_TIME                          = "special_instructions_start_time";
    public static final String UDI_SPEC_END_TIME                            = "special_instructions_end_time";
    public static final String UDI_UPDATED_BY                               = "updated_by";
    public static final String UDI_UPDATED_ON                               = "updated_on";
    public static final String UDI_VENDOR_FLAG                              = "vendor_flag";
    public static final String UDI_CAN_CHANGE_ORDER_SOURCE                  = "can_change_order_source";
    public static final String UDI_SOURCE_TYPE                              = "source_type";
    public static final String UDI_OCCASION_CATEGORY_INDEX                  = "occasion_category_index";
    public static final String UDI_SIZE_INDICATOR                           = "size_indicator";
    public static final String UDI_MASTER_ORDER_NUMBER                      = "master_order_number";
    public static final String UDI_MEMBERSHIP_REQUIRED                      = "membership_required";
    public static final String UDI_CUSTOMER_FIRST_NAME                      = "customer_first_name";
    public static final String UDI_CUSTOMER_LAST_NAME                       = "customer_last_name";
    public static final String UDI_CUSTOMER_CITY                            = "customer_city";
    public static final String UDI_CUSTOMER_STATE                           = "customer_state";
    public static final String UDI_CUSTOMER_ZIP_CODE                        = "customer_zip_code";
    public static final String UDI_CUSTOMER_COUNTRY                         = "customer_country";
    public static final String UDI_START_NEW_UPDATE                         = "start_new_update";
    public static final String UDI_RELEASE_INFO_INDICATOR                   = "release_info_indicator";

    public static final String UNIT_ID                                      = "unitID";
    public static final String UO_DISPLAY_PAGE                              = "uo_display_page";
    public static final String UPSELL_FLAG                                  = "upsell_flag";
    public static final String UPSELL_ID                                    = "upsell_id";
    public static final String UPDATED_BY                                   = "updated_by";
    public static final String VENDOR_FLAG                                  = "vendor_flag";
    public static final String VENDOR_ID                                    = "vendor_id";
    public static final String VENUS_ID                                     = "venus_id";
    public static final String VENUS_ORDER_NUMBER                           = "venus_order_number";
    public static final String VIP_CUSTOMER                                 = "vip_customer";
    public static final String WORK_COMPLETE                                = "work_complete";
    public static final String ZIP_CODE                                     = "zip_code";
    public static final String ZIP_SUNDAY_FLAG                              = "zip_sunday_flag";


    /**
     *  Request info used by the DataFilter
    **/
    //Header Data
    public static final String H_BRAND_NAME                                 = "call_brand_name";
    public static final String H_CUSTOMER_ORDER_INDICATOR                   = "call_type_flag";
    public static final String H_CUSTOMER_SERVICE_NUMBER                    = "call_cs_number";
    public static final String H_DNIS_ID                                    = "call_dnis";
    public static final String H_CALL_LOG_ID                                = "call_log_id";
    //Search Criteria
    public static final String SC_CC_NUMBER                                 = "sc_cc_number";
    public static final String SC_CUST_IND                                  = "sc_cust_ind";
    public static final String SC_CUST_NUMBER                               = "sc_cust_number";
    public static final String SC_EMAIL_ADDRESS                             = "sc_email_address";
    public static final String SC_LAST_NAME                                 = "sc_last_name";
    public static final String SC_MEMBERSHIP_NUMBER                         = "sc_rewards_number";
    public static final String SC_PC_MEMBERSHIP								= "sc_pc_membership";
    public static final String SC_ORDER_NUMBER                              = "sc_order_number";
    public static final String SC_PHONE_NUMBER                              = "sc_phone_number";
    public static final String SC_RECIP_IND                                 = "sc_recip_ind";
    public static final String SC_TRACKING_NUMBER                           = "sc_tracking_number";
    public static final String SC_ZIP_CODE                                  = "sc_zip_code";
    public static final String SC_AP_ACCOUNT                                = "sc_ap_account";
    //Timer Filter
    public static final String TIMER_CALL_LOG_ID                            = "t_call_log_id";
    public static final String TIMER_COMMENT_ORIGIN_TYPE                    = "t_comment_origin_type";
    public static final String TIMER_ENTITY_HISTORY_ID                      = "t_entity_history_id";
    //comments related
    public static final String START_ORIGIN                                 = "start_origin";
    //Order Update
    public static final String FLORIST_CANT_FULFILL                         = "florist_cant_fulfill";
    public static final String MODIFY_ORDER_UPDATED                         = "modify_order_updated";

    //Privacy Policy Verification
    public static final String BYPASS_PRIVACY_POLICY_VERIFICATION           = "bypass_privacy_policy_verification";
    public static final String CUSTOMER_VALIDATION_REQUIRED_FLAG            = "customer_validation_required_flag";
    public static final String ORDER_VALIDATION_REQUIRED_FLAG               = "order_validation_required_flag";

    /*********************************************************************************************
    //start origin values
    *********************************************************************************************/
    public static final String START_ORIGIN_CS                              = "CS Call";
    public static final String START_ORIGIN_OE                              = "OE Call";
    public static final String START_ORIGIN_GEN                             = "GEN";


    /*********************************************************************************************
    //comments
    *********************************************************************************************/
    public static final String CUSTOMER_COMMENT_TYPE                        = "Customer";
    public static final String ORDER_COMMENT_TYPE                           = "Order";


    /*********************************************************************************************
    // Call Disposition (decision_result) DataFilter variables
    *********************************************************************************************/
    public static final String CDISP_AUTO_OPENED_FLAG                       = "cdisp_autoOpenedFlag";
    public static final String CDISP_REMINDER_FLAG                          = "cdisp_reminderFlag";
    public static final String CDISP_CANCELLED_ORDER                        = "cdisp_cancelledOrder";
    public static final String CDISP_ENABLED_FLAG                           = "cdisp_enabledFlag";
    public static final String CDISP_RESET_FLAG                             = "cdisp_resetFlag";
    // Call Disposition Admin Role (If CSR has this role, then Call Dispo logic is enabled)
    public static final String CDISP_ADMIN                                  = "CallDisposition"; 


    /*********************************************************************************************
    //Valid Values for Request Parameter of action_type
    *********************************************************************************************/
    public static final String ACTION_ADD_CUSTOMER_EMAIL                    = "add_customer_email";
    public static final String ACTION_CA_FIRST_PAGE                         = "ca_first";
    public static final String ACTION_CA_LAST_PAGE                          = "ca_last";
    public static final String ACTION_CA_NEXT_PAGE                          = "ca_next";
    public static final String ACTION_CA_PREVIOUS_PAGE                      = "ca_previous";
    public static final String ACTION_CA_CSC_FIRST_PAGE                     = "ca_csc_first";
    public static final String ACTION_CA_CSC_LAST_PAGE                      = "ca_csc_last";
    public static final String ACTION_CA_CSC_NEXT_PAGE                      = "ca_csc_next";
    public static final String ACTION_CA_CSC_PREVIOUS_PAGE                  = "ca_csc_previous";
    public static final String ACTION_CEA_FIRST_PAGE                        = "cea_first";
    public static final String ACTION_CEA_LAST_PAGE                         = "cea_last";
    public static final String ACTION_CEA_NEXT_PAGE                         = "cea_next";
    public static final String ACTION_CEA_PREVIOUS_PAGE                     = "cea_previous";
    public static final String ACTION_CHECK_CUSTOMER_ACCOUNT                = "check_customer_account";
    public static final String ACTION_CHECK_CUSTOMER_EMAIL                  = "check_customer_email";
    public static final String ACTION_CHECK_REFUND_STATUS                   = "check_refund_status";
    public static final String ACTION_CHECK_UPDATE_FLORIST                  = "check_update_florist";
    public static final String ACTION_CHECK_UPDATE_ORDER_STATUS             = "check_update_order_status";
    public static final String ACTION_CSCI_NEXT                             = "csc_more";
    public static final String ACTION_CSCI_PREVIOUS                         = "csc_previous";
    public static final String ACTION_CUSTOMER_ACCOUNT_SEARCH               = "customer_account_search";
    public static final String ACTION_CUSTOMER_SEARCH                       = "customer_search";
    public static final String ACTION_DELETE_ADDON                          = "delete_addon";
    public static final String ACTION_FIRST_PAGE                            = "first_page";
    public static final String ACTION_GET_EMAIL_INFO                        = "get_email_info";
    public static final String ACTION_INSERT_PRIVACY_POLICY                 = "insert_privacy_policy";
    public static final String ACTION_SERVICES_INFO_LOOKUP_COM              = "services_info_lookup_com";
    public static final String ACTION_SERVICES_INFO_LOOKUP_MENU             = "services_info_lookup_menu";
    public static final String ACTION_SERVICES_GET_FSM_DETAILS              = "get_fsm_details";
    public static final String ACTION_LAST                                  = "last";
    public static final String ACTION_LAST_50                               = "last_50";
    public static final String ACTION_LAST_PAGE                             = "last_page";
    public static final String ACTION_LOAD_CUSTOMER_ACCOUNT                 = "load_customer_account";
    public static final String ACTION_LOAD_DELIVERY_INFO                    = "load_delivery_info";
    public static final String ACTION_LOAD_PAGE                             = "load_page";
    public static final String ACTION_LOAD_LPSEARCH_PAGE                    = "load_lpsearch_page";
    public static final String ACTION_LOAD_PRIVACY_POLICY                   = "load_privacy_policy";
    public static final String ACTION_LOAD_REFUND_CODES                     = "load_refund_codes";
    public static final String ACTION_LPSEARCH                              = "lpsearch";
    public static final String ACTION_MEMBERSHIP_INFO_LOOKUP                = "membership_info_lookup";
    public static final String ACTION_MORE_CUSTOMER_EMAILS                  = "more_customer_emails";
    public static final String ACTION_NEXT_PAGE                             = "next_page";
    public static final String ACTION_ORIGINAL_VIEW_CHECK                   = "original_view_check";
    public static final String ACTION_ORIGINAL_VIEW_LOAD                    = "original_view_load";
    public static final String ACTION_PREVIOUS_PAGE                         = "previous_page";
    public static final String ACTION_PRINT_ORDER                           = "print_order";
    public static final String ACTION_RECIPIENT_SEARCH                      = "recipient_search";
    public static final String ACTION_RECIP_ORD_NUM_SEARCH                  = "recipient_order_number_search";
    public static final String ACTION_RECIP_PMT_INFO                        = "recipient_payment_info_lookup";
    public static final String ACTION_REFRESH_IDS                           = "refresh_ids";
    public static final String ACTION_REFUND_CART                           = "refund_cart";
    public static final String ACTION_REFUND_RELEASE_LOCK                   = "refund_release_lock";
    public static final String ACTION_RELEASE_LOCK                          = "release_lock";
    public static final String ACTION_RETRIEVE_REFUND_LOCK                  = "retrieve_refund_lock";
    public static final String ACTION_SAVE_CUSTOMER_EMAILS                  = "save_customer_emails";
    public static final String ACTION_SAVE_CUSTOMER_CHANGES                 = "save_customer_changes";
    public static final String ACTION_SEARCH                                = "search";
    public static final String ACTION_SHOP_CART_PMT_INFO                    = "shopping_cart_payment_info_lookup";
    public static final String ACTION_SOURCE_CODE_LOOKUP                    = "source_code_lookup";
    public static final String ACTION_UCE_FIRST_PAGE                        = "uce_first";
    public static final String ACTION_UCE_LAST_PAGE                         = "uce_last";
    public static final String ACTION_UCE_NEXT_PAGE                         = "uce_next";
    public static final String ACTION_UCE_PREVIOUS_PAGE                     = "uce_previous";
    public static final String ACTION_UDI_CANCEL_UPDATE                     = "cancel_update";
    public static final String ACTION_UDI_MAX_DATES                         = "max_dates";
    public static final String ACTION_UDI_SAVE_CHANGES                      = "save";
    public static final String ACTION_UDI_SAVE_AND_COMPLETE                 = "save_and_complete";
    public static final String ACTION_UDI_GET_UPDATED                       = "get_updated_delivery_info";


    public static final String ADD_COMMENT                                  = "add_comment";
    public static final String SAVE_COMMENT                                 = "save_comment";
    public static final String EDIT_COMMENT                                 = "edit_comment";
    public static final String DELETE_COMMENT                               = "delete_comment";
    public static final String CART_SAVE_COMMENT                            = "cart_save_comment";

    public static final String UPDATE_DNIS                                  = "update_dnis";
    public static final String INSERT_DNIS                                  = "insert_dnis";
    public static final String DELETE_DNIS                                  = "delete_dnis";

    public static final String LOAD_MSG_TITLES                              = "load_titles";
    public static final String LOAD_MSG_CONTENT                             = "load_content";
    public static final String LOAD_MSG_CONTENT_AJAX                        = "load_content_ajax";
    public static final String SAVE_MESSAGE                                 = "save_message";
    public static final String LOAD_MSG_VIEW                                = "load_message";
    public static final String SEND_MESSAGE                                 = "send_message";

    // Queue constants
    public static final String RTQ_ATTACHED                                 = "rtq_attached";
    public static final String RTQ_CURRENT_SORT_DIRECTION                   = "rtq_current_sort_direction";
    public static final String RTQ_FLAG                                     = "rtq";
    public static final String RTQ_GROUP                                    = "rtq_group";
    public static final String RTQ_MAX_RECORDS                              = "rtq_max_records";
    public static final String RTQ_POSITION                                 = "rtq_position";
    public static final String RTQ_SORT_COLUMN                              = "rtq_sort_column";
    public static final String RTQ_SORT_ON_RETURN                           = "rtq_sort_on_return";
    public static final String RTQ_QUEUE_IND                                = "rtq_queue_ind";
    public static final String RTQ_QUEUE_TYPE                               = "rtq_queue_type";
    public static final String RTQ_TAGGED                                   = "rtq_tagged";
    public static final String RTQ_USER                                     = "rtq_user";
    public static final String RTQ_RECEIVED_DATE                            = "rtq_received_date";    
    public static final String RTQ_DELIVERY_DATE                            = "rtq_delivery_date";    
    public static final String RTQ_TAG_DATE                                 = "rtq_tag_date";    
    public static final String RTQ_LAST_TOUCHED                             = "rtq_last_touched";    
    public static final String RTQ_TYPE                                     = "rtq_type"; 
    public static final String RTQ_SYS                                      = "rtq_sys"; 
    public static final String RTQ_TIMEZONE                                 = "rtq_timezone"; 
    public static final String RTQ_SDG                                      = "rtq_sdg"; 
    public static final String RTQ_DISPOSITION                              = "rtq_disposition"; 
    public static final String RTQ_PRIORITY                                 = "rtq_priority"; 
    public static final String RTQ_NEW_ITEM                                 = "rtq_new_item"; 
    public static final String RTQ_OCCASION                                 = "rtq_occasion"; 
    public static final String RTQ_LOCATION                                 = "rtq_location"; 
    public static final String RTQ_MEMBER_NUMBER                            = "rtq_member_num"; 
    public static final String RTQ_ZIP                                      = "rtq_zip"; 
    public static final String RTQ_TAG_BY                                   = "rtq_tag_by"; 
    public static final String RTQ_FILTER                                   = "rtq_filter"; 



    /*********************************************************************************************
    //Valid Values for Request Parameter of message_display
    *********************************************************************************************/

    //Refunds
    public static final String  MSG_REFUND_A_TYPE_CANCELLED                 = "ALL ORDERS MUST BE CANCELLED BEFORE A-TYPE REFUND MAY BE SELECTED";
    public static final String  MSG_REFUND_A_TYPE_DATE                      = "YOU CANNOT CHOOSE AN A-TYPE REFUND IF ANY ORDER IN THE SHOPPING CART IS PAST THE DELIVERY DATE";
    public static final String  MSG_REFUND_A_TYPE_DLVR_PRT                  = "YOU CANNOT CHOOSE AN A-TYPE REFUND IF ANY ORDER IN THE SHOPPING CART HAS BEEN DELIVERED OR PRINTED";
    public static final String  MSG_REFUND_EXIST                            = "REFUND ALREADY EXISTS WITHIN THE SHOPPING CART.  NO CART LEVEL REFUNDS ALLOWED";
    public static final String  MSG_REFUND_ORDER_SCRUB                      = "PART OF THIS ORDER IS STILL IN SCRUB.  NO CART LEVEL REFUNDS ALLOWED";

    //Miscellaneous
    public static final String  MSG_EMAIL_ALREADY_EXIST                     = "EMAIL ADDRESS ALREADY EXISTS FOR THIS CUSTOMER... PLEASE TRY AGAIN";
    public static final String  MSG_INVALID_ACTION                          = "INVALID ACTION CODE FOUND.  CONTACT SYSTEM ADMIN";
    public static final String  MSG_MULTIPLE                                = "MULTIPLE RECORDS FOUND";
    public static final String  MSG_NO_RECORDS                              = "NO RECORDS FOUND";
    public static final String  MSG_NOTHING_CHANGED                         = "NO CHANGE DETECTED... PLEASE TRY AGAIN";
    public static final String  MSG_ORDER_NUM_NOT_EXIST                     = "ORDER NUMBER DOES NOT EXIST";
    public static final String  MSG_ORDER_PENDING                           = "POPUP ORDER IN PENDING";
    public static final String  MSG_ORDER_REMOVED                           = "POPUP ORDER IN REMOVED";
    public static final String  MSG_ORDER_SCRUB                             = "POPUP ORDER IN SCRUB";
    public static final String  MSG_NO_LP_EXISTS                            = "There is no loss prevention indicator for this shopping cart.";
    public static final String  MSG_RECORD_C_LOCKED1                        = "The customer record is currently being updated by ";
    public static final String  MSG_RECORD_C_LOCKED2                        = ".  No other updates can be made to the customer record at this time.";
    public static final String  MSG_RECORD_O_LOCKED1                        = "The order is currently being updated by ";
    public static final String  MSG_RECORD_O_LOCKED2                        = ".  No other updates can be made to the order at this time.";


    public static final String  MSG_TOO_MANY                                = "TOO MANY RESULTS.  REFINE SEARCH";
    public static final String  MSG_UPDATE_UNSUCCESSFUL                     = "UPDATE WAS NOT SUCCESSFUL... PLEASE TRY AGAIN LATER";



    /*********************************************************************************************
    //Valid values for XSL Files
    *********************************************************************************************/
    public static final String XSL_ASSIGN_TAG_DISPOSITION_PRIORITY          = "assignTagDispositionPriority";
    public static final String XSL_CALL_REASON_CODE                         = "CallReasonCode";
    public static final String XSL_CANCEL_UPDATE_ORDER_IFRAME               = "CancelUpdateOrderIFrame";
    public static final String XSL_CANCEL_PHOENIX_ORDER_IFRAME               = "CancelPhoenixOrderIFrame";
    public static final String XSL_CART_PAYMENT_INFORMATION                 = "CartPaymentInformation";
    public static final String XSL_CHECK_CUSTOMER_ACCOUNT                   = "CheckUpdateCustomerStatusIFrame";
    public static final String XSL_CHECK_LOCK_IFRAME                        = "CheckLockIFrame";
    public static final String XSL_CHECK_ORIGINAL_VIEW                      = "CheckOriginalViewStatusIFrame";
    public static final String XSL_CHECK_REFUNDS_IFRAME                     = "CheckRefundsIFrame";
    public static final String XSL_CHECK_UPDATE_FLORIST                     = "CheckUpdateFloristIFrame";
    public static final String XSL_CUSTOM_ORDER                             = "CustomOrder";
    public static final String XSL_CUSTOMER_ACCOUNT                         = "CustomerAccount";
    public static final String XSL_CUSTOMER_SERVICE_DNIS_ENTERED            = "CustomerServiceDnisEntered";
    public static final String XSL_CUSTOMER_ORDER_SEARCH                    = "CustomerOrderSearch";
    public static final String XSL_CUSTOMER_SERVICE_MENU                    = "CustomerServiceMenu";
    public static final String XSL_CUSTOMER_SHOPPING_CART                   = "CustomerShoppingCart";
    public static final String XSL_DISPLAY_SDS_TRANSACTION                  = "DisplaySDSTransaction";
    public static final String XSL_FAILURE                                  = "failure";
    public static final String XSL_INSERT_PRIVACY_POLICY_IFRAME             = "InsertPrivacyPolicyIFrame";
    public static final String XSL_LAST_RECORDS_ACCESSED                    = "LastRecordsAccessed";
    public static final String XSL_LOAD_CUSTOM_ORDER_ACTION                 = "LoadCustomOrderAction";
    public static final String XSL_LOAD_DELIVERY_CONF_ACTION                = "LoadDeliveryConfirmationAction";
    public static final String XSL_LOAD_DELIVERY_INFO_ACTION                = "LoadDeliveryInfoAction";
    public static final String XSL_LOAD_PRODUCT_DETAIL_ACTION               = "LoadProductDetailAction";
    public static final String XSL_LOAD_USERS_IFRAME                        = "LoadUsersIFrame";
    public static final String XSL_LOOKUP_GREETING                          = "LookupGreeting";
    public static final String XSL_LOOKUP_GREETING_DETAIL                   = "LookupGreetingDetail";
    public static final String XSL_LOSS_PREVENTION                          = "lossPrevention";
    public static final String XSL_LOSS_PREVENTION_SEARCH                   = "LossPreventionSearch";
    public static final String XSL_MEMBERSHIP_INFORMATION                   = "MembershipInformation";
    public static final String XSL_SERVICES_INFORMATION                     = "ServicesInformation";    
    public static final String XSL_MODIFY_ORDER_VALIDATION_IFRAME           = "ModifyOrderValidationIFrame";
    public static final String XSL_MORE_CUSTOMER_EMAILS                     = "MoreCustomerEmails";
    public static final String XSL_MULTIPLE_RECORDS_FOUND                   = "MultipleRecordsFound";
    public static final String XSL_MULTIPLE_TAGS_ASSIGNED                   = "multipleTagsAssigned";
    public static final String XSL_ORDER_PAYMENT_INFORMATION                = "OrderPaymentInformation";
    public static final String XSL_ORIGINAL_VIEW                            = "OriginalView";
    public static final String XSL_ORIGINAL_VIEW_ACTION                     = "OriginalViewAction";
    public static final String XSL_PRINT_ORDER                              = "PrintOrder";
    public static final String XSL_PRIVACY_POLICY_VERIFICATION              = "PrivacyPolicyVerification";
    public static final String XSL_PROCESS_CUSTOMER_ACCOUNT_IFRAME          = "ProcessUpdateCustomerAccountIFrame";
    public static final String XSL_PROCESS_CUSTOMER_EMAIL_IFRAME            = "ProcessUpdateCustomerEmailIFrame";
    public static final String XSL_REFUND_DISPLAY                           = "RefundDisplay";
    public static final String XSL_SUCCESS                                  = "success";
    public static final String XSL_REFUND_UPDATE_ACTION                     = "UpdateRefundAction";
    public static final String XSL_SOURCE_CODE_DESCRIPTION                  = "SourceCodeDescription";
    public static final String XSL_UPDATE_CUSTOMER_ACCOUNT                  = "UpdateCustomerAccount";
    public static final String XSL_UPDATE_CUSTOMER_EMAIL_STATUSES           = "UpdateCustomerEmailStatuses";
    public static final String XSL_UDD_VALIDATION_IFRAME                    = "UpdateDeliveryDateValidationIFrame";
    public static final String XSL_UDD_LOAD_DELIVERY_DATE_ACTION            = "LoadDeliveryConfirmationAction";
    public static final String XSL_UDD_CUSTOMER_SEARCH_ACTION               = "customerOrderSearchAction";
    public static final String XSL_UDD_INIT_LOAD_DELIVERY_DATE_ACTION       = "LoadDeliveryDateAction";
    public static final String XSL_VIEW_ALL_EMAILS                          = "ViewAllEmails";
    public static final String XSL_UDI_DISPLAY                              = "Display";
    public static final String XSL_UDI_REDISPLAY_WITH_ERROR                 = "RedisplayWithError";
    public static final String XSL_UDI_SUCCESS_SAVE_AND_CONTINUE            = "SuccessSaveAndContinue";
    public static final String XSL_UDI_SUCCESS_SAVE_AND_COMPLETE            = "SuccessSaveAndComplete";
    public static final String XSL_UDI_UPDATE_IFRAME                        = "ProcessUpdateDeliveryInfoIFrame";
    public static final String XSL_UPDATE_DELIVERY_INFO                     = "UpdateDeliveryInfo";
    public static final String XSL_UPDATE_DELIVERY_DATE                     = "UpdateDeliveryDate";
    public static final String XSL_UPDATE_PRODUCT_CATEGORY                  = "UpdateProductCategory";
    public static final String XSL_UPDATE_PRODUCT_DETAIL                    = "UpdateProductDetail";
    public static final String XSL_UPDATE_PRODUCT_LIST                      = "UpdateProductList";
    public static final String XSL_UPSELL_DETAIL                            = "UpsellDetail";

    
    /*********************************************************************************************
    // JOE Modify/Update Order return variables
    *********************************************************************************************/
    public static final String RETURN_FROM_JOE                              = "return_from_joe";
    public static final String MODIFY_ORDER_IN_JOE                          = "modify_order_in_joe";
    public static final String ORIG_ORDER_DETAIL_ID                         = "orig_order_detail_id";
    
    /*********************************************************************************************
    // Breadcrumb titles
    *********************************************************************************************/
    public static final String BCT_PRODUCT_CATEGORY                         = "Product Category";
    public static final String BCT_CUSTOM_ORDER                             = "Custom Order";
    public static final String BCT_PRODUCT_DETAIL                           = "Product Detail";
    public static final String BCT_UPSELL_DETAIL                            = "Upsell Detail";
    public static final String BCT_PRODUCT_LIST                             = "Product List";
    public static final String BCT_DELIVERY_CONFIRMATION                    = "Delivery Confirmation";


    /*********************************************************************************************
    // Breadcrumb actions
    *********************************************************************************************/
    public static final String BCA_PRODUCT_CATEGORY                         = "loadProductCategory.do";
    public static final String BCA_CUSTOM_ORDER                             = "loadCustomOrder.do";
    public static final String BCA_PRODUCT_DETAIL                           = "loadProductDetail.do";
    public static final String BCA_UPSELL_DETAIL                            = "search.do";
    public static final String BCA_KEYWORD_SEARCH_RESULTS                   = "search.do";
    public static final String BCA_PRODUCT_LIST                             = "loadProductList.do";
    public static final String BCA_DELIVERY_CONFIRMATION                    = "loadDeliveryConfirmation.do";



    /*********************************************************************************************
    //Miscellaneous fields for the pageData
    *********************************************************************************************/
    public static final String PD_PAGE_DATA                                 = "pageData";
    public static final String PD_DATA                                      = "data";
    //Header Data
    public static final String PD_CUSTOMER_SERVICE_NUMBER                   = "cserv_number";
    public static final String PD_CUSTOMER_ORDER_INDICATOR                  = "cs_ord_ind";
    public static final String PD_BRAND_NAME                                = "brand_name";
    public static final String PD_DNIS_ID                                   = "dnis_id";

    //Customer Order Search
    public static final String PD_COS_COUNT                                 = "cos_search_customer_count";
    public static final String PD_COS_START_POSITION                        = "cos_start_position";
    public static final String PD_COS_TOTAL_PAGES                           = "cos_total_pages";
    public static final String PD_COS_TOTAL_RECORDS_FOUND                   = "cos_total_records_found";
    //Customer Account Information
    public static final String PD_CAI_LOCKED_INDICATOR                      = "cai_locked_indicator";
    public static final String PD_CAI_MORE_EMAIL_INDICATOR                  = "cai_more_email_indicator";
    public static final String PD_CAI_NUMBER_OF_CARTS                       = "cai_number_of_carts";
    public static final String PD_CAI_NUMBER_OF_ORDERS                      = "cai_number_of_orders";
    public static final String PD_CAI_VIP_CUSTOMER                      	= "cai_vip_customer";
    public static final String PD_CAI_START_POSITION                        = "cai_start_position";
    public static final String PD_CAI_TOTAL_PAGES                           = "cai_total_pages";
    //Customer Shopping Cart Information
    public static final String PD_CSCI_NUMBER_OF_ORDERS                     = "csci_number_of_orders";
    public static final String PD_CSCI_ORDER_POSITION                       = "csci_order_position";
    public static final String PD_CSCI_ORDER_INDICATOR                      = "csci_order_indicator";
    public static final String PD_CSCI_ORDER_FOUND                          = "csci_order_found";
    public static final String PD_CSCI_START_POSITION                       = "csci_start_position";
    public static final String PD_CSCI_TOTAL_PAGES                          = "csci_total_pages";
    //Customer Email Account (View only)
    public static final String PD_CEA_COUNT                                 = "cea_customer_email_count";
    public static final String PD_CEA_START_POSITION                        = "cea_start_position";
    public static final String PD_CEA_TOTAL_PAGES                           = "cea_total_pages";
    //Refund
    public static final String PD_REFUND_CAN_BE_APPLIED                     = "refund_can_be_applied";
    public static final String PD_REFUND_DISP_CODE                          = "refund_disp_code";
    public static final String PD_REFUND_MESSAGE_DISPLAY                    = "refund_message_display";
    public static final String PD_REFUND_RESPONSIBLE_PARTY                  = "responsible_party";
    //Update Customer Account
    public static final String PD_UC_CUSTOMER_ID                            = "uc_customer_id";
    public static final String PD_UC_EMAIL_FOUND                            = "uc_email_found";
    public static final String PD_UC_MASTER_ORDER_NUMBER                    = "uc_master_order_number";
    public static final String PD_UC_ORDER_GUID                             = "uc_order_guid";
    //Update Customer Email Account
    public static final String PD_UCE_COUNT                                 = "uce_customer_email_count";
    public static final String PD_UCE_EMAIL_ADDRESS                         = "uce_email_address";
    public static final String PD_UCE_START_POSITION                        = "uce_start_position";
    public static final String PD_UCE_TOTAL_PAGES                           = "uce_total_pages";

    //Update DeliveryDate
     public static final String PD_UDD_ERROR                                = "service_fee_message";

    
    //Modify Orders
    public static final String PD_MO_JOE_URL                                = "modify_order_joe_url";
    //
    public static final String PD_MO_AGRICULTURE_RESTRICTION_FOUND          = "agricultureRestrictionFound";
    public static final String PD_MO_BUYER_FULL_NAME                        =  "buyer_full_name";
    public static final String PD_MO_CATEGORY_INDEX                         =  "category_index";
    public static final String PD_MO_COMPANY_ID                             =  "company_id";
    public static final String PD_MO_CUSTOMER_ID                            =  "customer_id";
    public static final String PD_MO_DELIVERY_DATE                          = "delivery_date";
    public static final String PD_MO_DELIVERY_DATE_RANGE_END                = "delivery_date_range_end";
    public static final String PD_MO_DOMESTIC_INT_FLAG                      = "domestic_international_flag";
    public static final String PD_MO_DOMESTIC_SRVC_FEE                      = "domestic_srvc_fee";
    public static final String PD_MO_EXTERNAL_ORDER_NUMBER                  =  "external_order_number";
    public static final String PD_MO_FLORAL_SERVICE_CHARGES                 = "floral_service_charges";
    public static final String PD_MO_INTERNATIONAL_SRVC_FEE                 = "international_srvc_fee";
    public static final String PD_MO_MASTER_ORDER_NUMBER                    =  "master_order_number";
    public static final String PD_MO_MAX_VARIABLE_PRICE                     = "max_variable_price";
    public static final String PD_MO_OCCASION                               =  "occasion";
    public static final String PD_MO_OCCASION_TEXT                          =  "occasion_description";
    public static final String PD_MO_ORDER_DETAIL_ID                        =  "order_detail_id";
    public static final String PD_MO_ORDER_GUID                             =  "order_guid";
    public static final String PD_MO_ORIGIN_ID                              =  "origin_id";
    public static final String PD_MO_ORIG_PRODUCT_AMOUNT                    = "orig_product_amount";
    public static final String PD_MO_ORIG_PRODUCT_ID                        = "orig_product_id";
    public static final String PD_MO_PAGE_NUMBER                            =  "page_number";
    public static final String PD_MO_PARTNER_ID                             =  "partner_id";
    public static final String PD_MO_PARTNER_NAME                           =  "partner_name";
    public static final String PD_MO_PRICE_POINT_ID                         =  "price_point_id";
    public static final String PD_MO_PRICING_CODE                           =  "pricing_code";
    public static final String PD_MO_PRODUCT_ID                             = "product_id";
    public static final String PD_MO_PRODUCT_IMAGE_LOCATION                 = "product_images";
    public static final String PD_MO_RECIPIENT_CITY                         =  "recipient_city";
    public static final String PD_MO_RECIPIENT_COUNTRY                      =  "recipient_country";
    public static final String PD_MO_RECIPIENT_STATE                        =  "recipient_state";
    public static final String PD_MO_RECIPIENT_ZIP_CODE                     =  "recipient_zip_code";
    public static final String PD_MO_REWARD_TYPE                            =  "reward_type";
    public static final String PD_MO_SHIP_METHOD                            =  "ship_method";
    public static final String PD_MO_SNH_ID                                 =  "snh_id";
    public static final String PD_MO_SORT_TYPE                              =  "sort_type";
    public static final String PD_MO_SOURCE_CODE                            =  "source_code";
    public static final String PD_MO_TOTAL_PAGES                            =  "total_pages";
    public static final String PD_MO_UPSELL_FLAG                            =  "upsell_flag";
    public static final String PD_MO_MAX_DELIVERY_DATES_RETRIEVED           = "max_delivery_dates_retrieved";
    public static final String PD_MO_FLORAL_LABEL_STANDARD                  = "floralLabelStandard";
    public static final String PD_MO_FLORAL_LABEL_DELUXE                    = "floralLabelDeluxe";
    public static final String PD_MO_FLORAL_LABEL_PREMIUM                   = "floralLabelPremium";

    //Others
    public static final String PD_ACTION                                    = "action";
    public static final String PD_CALL_CENTER_URL                           = "call_center_url";
    public static final String PD_CUSTOMER_ID                               = "customer_id";
    public static final String PD_DATA_EXISTS                               = "data_exists";
    public static final String PD_DISPLAY_PAGE                              = "display_page";
    public static final String PD_EMAIL_HISTORY_URL                         = "email_history_url";
    public static final String PD_EXTERNAL_ORDER_NUMBER                     = "external_order_number";
    public static final String PD_GOT_LOCK                                  = "got_lock";
    public static final String PD_LOAD_MEMBER_DATA_URL                      = "load_member_data_url";
    public static final String PD_LOCKED_INDICATOR                          = "locked_indicator";
    public static final String PD_MASTER_ORDER_NUMBER                       = "master_order_number";
    public static final String PD_MESSAGE_DISPLAY                           = "message_display";
    public static final String PD_MORE_EMAIL_INDICATOR                      = "more_email_indicator";
    public static final String PD_ORDER_DETAIL_ID                           = "order_detail_id";
    public static final String PD_ORDER_GUID                                = "order_guid";
    public static final String PD_PAGE_ACTION                               = "page_action";
    public static final String PD_PAGE_POSITION                             = "page_position";
    public static final String PD_QUEUE_ID                                  = "queue_id";
    public static final String PD_RECIPIENT_FLAG                            = "recipient_flag";
    public static final String PD_GLOBAL_AUTHORIZATION_FLAG                 = "global_authorization_flag";
    public static final String PD_SCRUB_URL                                 = "scrub_url";
    public static final String PD_SEARCHED_ON_EXTERNAL_ORDER_NUMBER         = "searched_on_external_order_number";
    public static final String PD_START_POSITION                            = "start_position";
    public static final String PD_VIEW_BILLING_URL                          = "view_billing_url";
    public static final String PD_VIEW_RECON_URL                            = "view_recon_url";


    //Parameters that will be used to enable/disable buttons
    public static final String PD_CURRENT_PAGE                              = "current_page";
    public static final String PD_SHOW_FIRST                                = "show_first";
    public static final String PD_SHOW_NEXT                                 = "show_next";
    public static final String PD_SHOW_PREVIOUS                             = "show_previous";
    public static final String PD_SHOW_LAST                                 = "show_last";
    public static final String PD_TOTAL_PAGES                               = "total_pages";
    public static final String PD_COMMENTS_TOTAL_PAGES                      = "comments_total_pages";
    

    //Customer
    public static final String REFUND_STATUS_UNBILLED                       = "Unbilled";

    //Customer Hold and Update/Add Billing
    public static final String ORDER_DISPOSITION_CANCELLED                  = "Cancelled";
    public static final String ORDER_DISPOSITION_HELD                       =  "Held";
    public static final String ORDER_DISPOSITION_PRINTED                    =  "Printed";
    public static final String ORDER_DISPOSITION_PROCESSED                  =  "Processed";
    public static final String ORDER_DISPOSITION_SHIPED                     =  "Shipped";
    public static final String ORDER_DISPOSITION_VALIDATED                  = "Validated";

    //Custom Order and Update Delivery Confirmation
    public static final String IS_CUSTOM_ORDER                              = "is_custom_order";

    public static final String SDS_STATUS_PRINTED                           = "Printed";
    public static final String SDS_STATUS_SHIPPED                           = "Shipped";


    // Free Shipping Membership
    public static final String FREESHIP_MEMBERSHIP_ORDER                    = "freeShippingMembershipOrder";
    public static final String FREESHIP_PROGRAM_NAME                        = "freeShippingProgramName";
    public static final String FREESHIP_BUYER_IS_ACTIVE_MEMBER              = "freeShippingActiveMember";
    public static final String FREESHIP_ORDER_SAVINGS_MSG                   = "freeShippingOrderSavingsMsg";
    public static final String ALLOW_FREESHIPPING_FLAG                      = "allowFreeShippingFlag";
    /*********************************************************************************************
    //HashMap keys that will be used to store the Documents in a HashMap
    *********************************************************************************************/
    public static final String HK_ADDON_XML                                 = "HK_addon";
    public static final String HK_ADDON_FLAGS_XML                           = "HK_addonFlags";
    public static final String HK_ADDON_OCCASION_XML                        = "HK_addonOccassion";
    public static final String HK_ADDRESS_TYPES                             = "HK_addressTypes";
    public static final String HK_CAI_CUSTOMER_XML                          = "HK_caiCustomer";
    public static final String HK_CAI_CUSTOMER_PREFERRED_PARTNER_XML        = "HK_caiCustomerPreferredPartner";
    public static final String HK_CAI_DIRECT_MAIL_EMAIL_XML                 = "HK_caiMail";
    public static final String HK_CAI_DIRECT_MAIL_XML                       = "HK_caiDirectMail";
    public static final String HK_CAI_EMAIL_XML                             = "HK_caiEmail";
    public static final String HK_CAI_HOLD_XML                              = "HK_caiHold";
    public static final String HK_CAI_MEMBERSHIP_XML                        = "HK_caiMembership";
    public static final String HK_CAI_ORDER_XML                             = "HK_caiOrder";
    public static final String HK_CAI_PHONE_XML                             = "HK_caiPhone";
    public static final String HK_CAI_VIEWING_XML                           = "HK_caiViewing";
    public static final String HK_CEA_CUSTOMER_EMAILS_XML                   = "HK_ceaCustomerEmails";
    public static final String HK_CO_BRAND_INFO_XML                         = "HK_coBrandInfo";
    public static final String HK_COLOR_XML                                 = "HK_color";
    public static final String HK_COS_CUSTOMER_ORDER_SEARCH_XML             = "HK_cosCustomerOrderSearch";
    public static final String HK_COS_CSR_VIEWED_XML                        = "HK_cosCSRViewed";
    public static final String HK_COS_CSR_VIEWING_XML                       = "HK_cosCSRViewing";
    public static final String HK_COUNTRIES                                 = "HK_countries";
    public static final String HK_CSCI_CART_XML                             = "HK_csciCart";
    public static final String HK_CSCI_CART_BILLS_XML                       = "HK_csciCartBills";
    public static final String HK_CSCI_CART_REFUNDS_XML                     = "HK_csciCartRefunds";
    public static final String HK_CSCI_CART_HOLD_XML                        = "HK_csciCartHold";
    public static final String HK_CSCI_ORDERS_XML                           = "HK_csciOrders";
    public static final String HK_CSCI_ORDER_ADDON_XML                      = "HK_csciOrderAddon";
    public static final String HK_CSCI_ORDER_BILLS_XML                      = "HK_csciOrderBills";
    public static final String HK_CSCI_ORDER_REFUNDS_XML                    = "HK_csciOrderRefunds";
    public static final String HK_CSCI_VIEWING_XML                          = "HK_csciViewing";
    public static final String HK_CSCI_CART_TOTALS_XML                      = "HK_csciCartTotals";
    public static final String HK_CSCI_ORDER_TOTALS_XML                     = "HK_csciOrderTotals";
    public static final String HK_CSCI_CSR_TAGGED_XML                       = "HK_csciCsrTagged";
    public static final String HK_DELIVERY_DATE_RANGE_XML                   = "HK_deliveryDateRange";
    public static final String HK_DELIVERY_DATES_XML                        = "HK_deliveryDates";
    public static final String HK_ERROR_MESSAGES                            = "HK_errorMessages";
    public static final String HK_FEE_XML                                   = "HK_fee";
    public static final String HK_FLOWER_XML                                = "HK_flower";
    public static final String HK_GIFT_MESSAGES                             = "HK_giftMessages";
    public static final String HK_GLOBAL_PARMS_CUSTOM_ORDER_XML             = "HK_globalParmsCO";
    public static final String HK_HOLIDAYS_BY_COUNTRY_XML                   = "HK_holidaysByCountry";
    public static final String HK_MEMBERSHIP_INFO_XML                       = "HK_membershipInfo";
    public static final String HK_SERVICES_INFO_XML                         = "HK_servicesInfo";
    public static final String HK_EMAIL_LIST_FOR_CUSTOMER_ID_XML            = "HK_emailList";
    public static final String HK_FSM_DETAILS_XML                           = "HK_fsmDetails";
    public static final String HK_FSM_SUMMARY_XML                           = "HK_fsmSummary";
    public static final String HK_NEW_ADDON_XML                             = "HK_newAddOn";
    public static final String HK_OCCASION_LIST_XML                         = "HK_occasionList";
    public static final String HK_ORDER_PRODUCT_XML                         = "HK_orderProduct";
    public static final String HK_ORDER_ADDON_XML                           = "HK_orderAddOn";
    public static final String HK_ORIG_PRODUCT_XML                          = "HK_origProduct";
    public static final String HK_ORIG_ADDON_XML                            = "HK_origAddOn";
    public static final String HK_ORDER_TAX_XML                             = "HK_orderTax";
    public static final String HK_PAYMENT_INFO_XML                          = "HK_paymentInfo";
    public static final String HK_PPV_CC_INFO_XML                           = "HK_ppvCCInfo";
    public static final String HK_PPV_CUSTOMER_INFO_XML                     = "HK_ppvCustomerInfo";
    public static final String HK_PPV_CUSTOMER_ORDER_INFO_XML               = "HK_ppvCustomerOrderInfo";
    public static final String HK_PPV_OPTION_CV_XML                         = "HK_ppvOptionCV";
    public static final String HK_PPV_OPTION_LD_XML                         = "HK_ppvOptionLD";
    public static final String HK_PPV_OPTION_OV_XML                         = "HK_ppvOptionOV";
    public static final String HK_PPV_OPTION_VE_XML                         = "HK_ppvOptionVE";
    public static final String HK_PPV_ORDER_INFO_XML                        = "HK_ppvOrderInfo";
    public static final String HK_PPV_PHONE_INFO_XML                        = "HK_ppvPhoneInfo";
    public static final String HK_PPV_VALIDATED_CUSTOMERS_XML               = "HK_ppvValidatedCustomers";
    public static final String HK_PPV_VALIDATED_ORDER_DETAILS_XML           = "HK_ppvValidatedOrderDetails";
    public static final String HK_PRICE_POINT_XML                           = "HK_pricePoint";
    public static final String HK_PRINT_ORDER_ADDON_XML                     = "HK_printOrderAddon";
    public static final String HK_PRINT_ORDER_BILLS_XML                     = "HK_printOrderBills";
    public static final String HK_PRINT_ORDER_COMMENT_XML                   = "HK_printOrderComment";
    public static final String HK_PRINT_ORDER_CUSTOMER_PHONE_XML            = "HK_printOrderCustomerPhone";
    public static final String HK_PRINT_ORDER_CUSTOMER_XML                  = "HK_printOrderCustomer";
    public static final String HK_PRINT_ORDER_MEMBERSHIP_XML                = "HK_printOrderMembership";
    public static final String HK_PRINT_ORDER_PAYMENT_XML                   = "HK_printOrderPayment";
    public static final String HK_PRINT_ORDER_REFUNDS_XML                   = "HK_printOrderRefunds";
    public static final String HK_PRINT_ORDER_TAX_REFUNDS_XML               = "HK_printOrderTaxRefunds";
    public static final String HK_PRINT_ORDER_TOTAL_XML                     = "HK_printOrderTotal";
    public static final String HK_PRINT_ORDER_XML                           = "HK_printOrder";
    public static final String HK_PRODUCT_CATEGORIES_XML                    = "HK_productCategories";
    public static final String HK_PRODUCT_CATEGORY_XML                      = "HK_productCategory";
    public static final String HK_PRODUCT_DETAIL_XML                        = "HK_productDetail";
    public static final String HK_PRODUCT_DETAIL_LIST_XML                   = "HK_productDetailList";
    public static final String HK_PRODUCT_IDS_XML                           = "HK_productIds";
    public static final String HK_PRODUCT_IDS_NOVATOR_XML                   = "HK_productIdsNovator";
    public static final String HK_PRODUCT_OCCASION_XML                      = "HK_productOccasion";
    public static final String HK_PRODUCT_SUBCODES_XML                      = "HK_productSubcodes";
    public static final String HK_RECIPIENT_SEARCH_LIST_XML                 = "HK_recipientSearchList";
    public static final String HK_REFUND_REFUND_CODES_XML                   = "HK_refundRefundCodes";
    public static final String HK_REFUND_RESPONSIBLE_PARTY_XML              = "HK_refundResponsible";
    public static final String HK_SCRIPT_XML                                = "HK_script";
    public static final String HK_SEARCH_RESULTS_ATTRIBUTE                  = "HK_searchResultsAttribute";
    public static final String HK_SHIP_COST_XML                             = "HK_shipCost";
    public static final String HK_SHIPPING_METHODS_XML                      = "HK_shippingMethods";
    public static final String HK_SOURCE_CODE_XML                           = "HK_sourceCode";
    public static final String HK_STATES                                    = "HK_states";
    public static final String HK_SUB_HEADER                                = "HK_subHeader";
    public static final String HK_SUB_INDEX_XML                             = "HK_subIndex";
    public static final String HK_SUPER_INDEX_XML                           = "HK_superIndex";
    public static final String HK_SUPER_INDEX_WITH_DATE_XML                 = "HK_superIndexWithDate";
    public static final String HK_UCE_CUSTOMER_EMAILS_XML                   = "HK_uceCustomerEmails";
    public static final String HK_UDI_DELIVERY_INFO_XML                     = "HK_deliveryInfo";
    public static final String HK_UDI_PRODUCT_INFO_XML                      = "HK_productInfo";
    public static final String HK_UDI_SHIP_COST_XML                         = "HK_shipCost";
    public static final String HK_UDI_ADDONS_XML                            = "HK_addOns";
    public static final String HK_UPDATE_CUSTOM_ORDER_ATTRIBUTE             = "HK_updateCustomOrderAttribute";
    public static final String HK_UPD_ATTRIBUTE                             = "HK_updateProductDetailAttribute";
    public static final String HK_UPD_NEW_ORDER_DETAIL                      = "HK_updateProductDetailNewOrderDetail";
    public static final String HK_UPSELL_XML                                = "HK_upsell";
    public static final String HK_ZIPCODES_CUSTOM_ORDER_XML                 = "HK_zipcodesCustomOrder";
    public static final String HK_CSCI_ORDER_FEES_SAVED_XML                 = "HK_csciOrderFeesSaved";    
    public static final String HK_CSCI_CART_TAXES_XML                       = "HK_csciCartTaxes";
    public static final String HK_CSCI_ORDER_TAXES_XML                      = "HK_csciOrderTaxes";

    /*********************************************************************************************
    //HashMap keys that will be used to pass data where a VO is not appropriate
    *********************************************************************************************/
    public static final String HM_PAYMENT_TYPE                              = "payment_type";
    public static final String HM_CC_ID                                     = "cc_id";
    public static final String HM_CARD_NUMBER                               = "card_number";
    public static final String HM_EXPIRATION_DATE                           = "expiration_date";
    public static final String HM_GC_COUPON_NUMBER                          = "gc_coupon_number";


    /*********************************************************************************************
    //Constants
    *********************************************************************************************/
    //General Constants
    public static final String AMAZON_ADMIN_FEE_PCT                         = "AMAZON_ADMIN_FEE_PCT";
    public static final String AMAZON_ADMIN_FEE_MAX                         = "AMAZON_ADMIN_FEE_MAX";
    public static final String AUTH_RESULT_DECLINED                         = "D";
    public static final String AUTH_RESULT_ERROR                            = "E";
    public static final String CONS_APP_PARAMETERS                          = "parameters";
    public static final String CONS_CCAS_AUTH_CLIENT_ACTIVE                 = "ccas.auth.client.active";
    public static final String CONS_COM_COMMENT_ORIGIN_TYPE                 = "COM";
    public static final String CONS_CS_INDICATOR                            = "CALL_TYPE_CS";
    public static final String CONS_CUST_SERV_SCRIPT                        = "CUST_SERV_SCRIPT";
    public static final String CONS_CUSTOMER_ORDER_SEARCH                   = "customerOrderSearch";
    public static final String CONS_DAY                                     = "Day";
    public static final String CONS_DEFAULT_DNIS                            = "DEFAULT_DNIS";
    public static final char   CONS_DELIMITER_CHAR                          = '\n';
    public static final String CONS_DNIS_ERROR_MSG                          = "DNIS_ERROR_MSG";
    public static final String CONS_DNIS_INACTIVE_MSG                       = "DNIS_INACTIVE_MSG";
    public static final String CONS_DNIS_INACTIVE                           = "I";
    public static final String CONS_EMAIL_HISTORY_URL                       = "EMAIL_HISTORY_URL";
    public static final String CONS_EVENING                                 = "Evening";
    public static final String CONS_FLORIST                                 = "F";
    public static final String CONS_GC_PAYMENT_TYPE                         = "GC";
    public static final String CONS_INACTIVE_TEXT                           = "inactive";
    public static final String CONS_INVOICE_PAYMENT_TYPE                    = "I";
    public static final String CONS_LOAD_MEMBER_DATA_URL                    = "LOAD_MEMBER_DATA_URL";
    public static final String CONS_LOCK_APP                                = "lock_app";
    public static final String CONS_LOCK_ID                                 = "lock_id";
    public static final String CONS_LOSS_PREVENTION_SEARCH                  = "lossPreventionSearch";
    public static final String CONS_MAIN_MENU_URL                           = "MAIN_MENU_URL";
    public static final String CONS_MAX_CART_EMAIL_PER_PAGE                 = "MAX_CART_EMAIL_PER_PAGE";
    public static final String CONS_MAX_COMMENTS_PER_PAGE                   = "MAX_COMMENTS_PER_PAGE";
    public static final String CONS_MAX_MOUSEOVER_COMMENT_LEN               = "MAX_MOUSEOVER_COMMENT_LEN";
    public static final String CONS_MAX_MOUSEOVER_EMAIL_LEN                 = "MAX_MOUSEOVER_EMAIL_LEN";
    public static final String CONS_MAX_REFUNDS_PER_PAGE                    = "MAX_REFUNDS_PER_PAGE";
    public static final String CONS_MAX_CUSTOMER_ACCOUNT_ORDERS             = "MAX_CUSTOMER_ACCOUNT_ORDERS";
    public static final String CONS_MAX_CUSTOMER_ACCOUNT_ORDERS_LP          = "MAX_CUSTOMER_ACCOUNT_ORDERS_LP";
    public static final String CONS_MAX_CUSTOMER_EMAIL_ADDRESSES            = "MAX_CUSTOMER_EMAIL_ADDRESSES";
    public static final String CONS_MAX_CUSTOMERS_PER_PAGE                  = "MAX_CUSTOMERS_PER_PAGE";
    public static final String CONS_MAX_DNIS_PER_PAGE                       = "MAX_DNIS_PER_PAGE";
    public static final String CONS_MAX_HISTORY_PER_PAGE                    = "MAX_HISTORY_PER_PAGE";
    public static final String CONS_MAX_ORDER_ITEMS                         = "MAX_ORDER_ITEMS";
    public static final String CONS_MAX_RECORDS_ALLOWED                     = "MAX_RECORDS_ALLOWED";
    public static final String CONS_NO                                      = "n";
    public static final String CONS_OE_INDICATOR                            = "CALL_TYPE_OE";
    public static final String CONS_ORDER_DETAILS_UPDATE_TYPE               = "DELIVERY_INFO";
    public static final String CONS_ORDER_LEVEL                             = "order_level";
    public static final String CONS_ORDER_STATUS_REMOVED                    = "r";
    public static final String CONS_ORDER_STATUS_SCRUB                      = "s";
    public static final String CONS_ORDER_STATUS_PENDING                    = "p";
    public static final String CONS_ALTPAY_PAYMENT_TYPE                     = "P";
    public static final String CONS_PCARD_PAYMENT_TYPE                      = "PC";
    public static final String CONS_REFUND_MAX_DAYS_OUT                     = "REFUND_MAX_DAYS_OUT";
    public static final String CONS_REFUND_REVIEW_ORIGIN                    = "Refund Review Screen";
    public static final String CONS_SCRUB_URL                               = "SCRUB_URL";
    public static final String CONS_SYSTEM_ERROR                            = "Error";
    public static final String CONS_UPDATE                                  = "UPDATE";
    public static final String CONS_VENDOR                                  = "V";
    public static final String CONS_VIEW_BILLING_URL                        = "VIEW_BILLING_URL";
    public static final String CONS_VIEW_RECON_URL                          = "VIEW_RECON_URL";
    public static final String CONS_WEB_OE_URL                              = "WEB_OE_URL";
    public static final String CONS_JOE_URL                                 = "JOE_URL";
    public static final String CONS_YES                                     = "y";
    public static final String CONS_TRUE                                    = "true";
    public static final String CONS_CDISP_REMINDER                          = "CDISP_OPTIONAL_REMINDER";
    public static final String CONS_CDISP_ACTIVE_FLAG                       = "DECISION_WIDGET_DISPLAY";
    public static final String ORDER_ORIGIN_AMAZON                          = "ORDER_ORIGIN_AMAZON";
    public static final String ORDER_ORIGIN_WALMART                         = "ORDER_ORIGIN_WALMART";
    public static final String REFUND_UNPROCESSED_STATUS                    = "REFUND_UNPROCESSED_STATUS";
    public static final String REMOVED_STATUS                               = "REMOVED_STATUS";
    public static final String PENDING_STATUS                               = "PENDING_STATUS";
    public static final String SCRUB_STATUS                                 = "SCRUB_STATUS";
    public static final String PRODUCT_IMAGE_LOCATION                       = "PRODUCT_IMAGE_LOCATION";

    // Payment types
    public static final String MILITARY_STAR                                = "MS";
    public static final String GIFT_CERTIFICATE                             = "GC";
    public static final String NO_CHARGE                                    = "NC";
    public static final String GD_OR_GC                                     = "gd_or_gc";
    public static final String GIFT_CARD_PAYMENT                            = "GD";
    public static final String GIFT_CERTIFICATE_PAYMENT                     = "GC";

    // Refund types
    public static final String PARTNER_REFUND_DISP                          = "A20";
    public static final String A40                                          = "A40";
    public static final String B40                                          = "B40";

    //constants for Global Parms
    public static final String CONS_FTDAPPS_PARMS                           = "FTDAPPS_PARMS";
    public static final String GLOBAL_NAME_PRIVACY_POLICY_POPUP_DISPLAY     = "PRIVACY_POLICY_POPUP_DISPLAY";

    //constants for origin ids
    public static final String CONS_ORIGIN_ID_AMAZON                        = "AMZNI";
    public static final String CONS_ORIGIN_ID_WALMART                       = "WLMTI";


    /*********************************************************************************************
    //constants to generate the error node
    *********************************************************************************************/
    public static final String CONS_ERROR_POPUP_INDICATOR_OK                = "O";
    public static final String CONS_ERROR_POPUP_INDICATOR_CONFIRM           = "C";


    /*********************************************************************************************
    //entity types for locking
    *********************************************************************************************/
    public static final String CONS_ENTITY_TYPE_COMMENTS                    = "COMMENTS";
    public static final String CONS_ENTITY_TYPE_CUSTOMER                    = "CUSTOMER";
    public static final String CONS_ENTITY_TYPE_CUSTOMER_TITLE_CASE         = "Customer";
    public static final String CONS_ENTITY_TYPE_DNIS                        = "DNIS";
    public static final String CONS_ENTITY_TYPE_MODIFY_ORDER                = "MODIFY_ORDER";
    public static final String CONS_ENTITY_TYPE_ORDER_DETAILS               = "ORDER_DETAILS";
    public static final String CONS_ENTITY_TYPE_ORDERS                      = "ORDERS";
    public static final String CONS_ENTITY_TYPE_PAYMENTS                    = "PAYMENTS";
    public static final String CONS_ENTITY_TYPE_PRODUCT_DETAIL              = "PRODUCT_DETAIL";
    public static final String CONS_ENTITY_TYPE_REFUND                      = "REFUND";
    public static final String CONS_ENTITY_TYPE_USER                        = "USER";


    /*********************************************************************************************
    //constants to pass in for processing id
    *********************************************************************************************/
    public static final String CONS_PROCESSING_ID_CUSTOMER                  = "CUSTOMER";
    public static final String CONS_PROCESSING_ID_PRODUCT                   = "PRODUCT";


    /*********************************************************************************************
    //modify order
    *********************************************************************************************/

    public static final String CONS_PRODUCT_IMAGE_LOCATION                  = "PRODUCT_IMAGE_LOCATION";
    public static final String CONS_CUSTOM_ORDER_PRODUCT_ID                 = "CUSTOM_ORDER_PRODUCT_ID";
    public static final String CONS_KEYWORD_SEARCH_SERVER_IP                = "KEYWORD_SEARCH_SERVER_IP";
    public static final String CONS_KEYWORD_SEARCH_SERVER_PORT              = "KEYWORD_SEARCH_SERVER_PORT";
    public static final String CONS_KEYWORD_SEARCH_MARKCODE_SFMB            = "KEYWORD_SEARCH_MARKCODE_SFMB";
    public static final String CONS_KEYWORD_SEARCH_MARKCODE_GIFT            = "KEYWORD_SEARCH_MARKCODE_GIFT";
    public static final String CONS_KEYWORD_SEARCH_MARKCODE_HIGH            = "KEYWORD_SEARCH_MARKCODE_HIGH";

    /* Global Parm processing constants */
    public static final String CONS_XML_PRODUCT_LIST                        = "productList";

    /* Time Zone Constants */
    public static final String CONS_TIMEZONE_DEFAULT                        = "5";

    /* Product Constants */
    public static final int CONS_PRODUCTS_PER_PAGE                          = 9;

    /* Product delivery type constants */
    public static final String CONS_DELIVERY_TYPE_DOMESTIC                  = "D";
    public static final String CONS_DELIVERY_TYPE_INTERNATIONAL             = "I";

    public static final String CONS_SEARCH_GET_PRODUCTS_BY_INDEX            = "search service::get products by index";

    /* General Order Arguments */
    public static final String CONS_DNIS                                    = "dnis";
    public static final String CONS_COUNTRY_CODE                            = "countryCode";
    public static final String CONS_COUNTRY                                 = "country";

    /* product search constants */
    public static final String CONS_SEARCH_SOURCE_CODE                      = "sourceCode";
    public static final String CONS_SEARCH_ZIP_CODE                         = "zipCode";
    public static final String CONS_SEARCH_DOMESTIC_INTL_FLAG               = "domesticIntlFlag";
    public static final String CONS_SEARCH_COUNTRY_ID                       = "countryId";
    public static final String CONS_SEARCH_INDEX_ID                         = "indexId";
    public static final String CONS_SEARCH_PRICE_POINT_ID                   = "inPricePointId";
    public static final String CONS_SEARCH_DELIVERY_END_DATE                = "inDeliveryEndDate";
    public static final String CONS_SEARCH_DELIVERY_END                     = "deliveryEnd";
    public static final String CONS_SEARCH_KEYWORD_SEARCH                   = "keywordsearch";

    /* Product Type & Sub-Type constants */
    public static final String CONS_PRODUCT_TYPE_FLORAL                     = "FLORAL";
    public static final String CONS_PRODUCT_TYPE_FRECUT                     = "FRECUT";
    public static final String CONS_PRODUCT_TYPE_SPEGFT                     = "SPEGFT";
    public static final String CONS_PRODUCT_TYPE_SDG                        = "SDG";
    public static final String CONS_PRODUCT_TYPE_SDFC                       = "SDFC";
    public static final String CONS_PRODUCT_SUBTYPE_EXOTIC                  = "EXOTIC";
    public static final String CONS_PRODUCT_SUBTYPE_NONE                    = "NONE";

    /* Delivery Constants */
    public static final String CONS_DELIVERY_NEXT_DAY                       = "Next Day Delivery";
    public static final String CONS_DELIVERY_TWO_DAY                        = "Two Day Delivery";
    public static final String CONS_DELIVERY_STANDARD                       = "Standard Delivery";
    public static final String CONS_DELIVERY_SATURDAY                       = "Saturday Delivery";
    public static final String CONS_DELIVERY_FLORIST                        = "Florist Delivery";
    public static final String CONS_DELIVERY_NEXT_DAY_CODE                  = "ND";
    public static final String CONS_DELIVERY_TWO_DAY_CODE                   = "2F";
    public static final String CONS_DELIVERY_STANDARD_CODE                  = "GR";
    public static final String CONS_DELIVERY_SATURDAY_CODE                  = "SA";
    public static final String CONS_DELIVERY_FLORIST_CODE                   = "FL";
    public static final String CONS_DELIVERY_SAME_DAY_CODE                  = "SD";

    /* Country Code Constants */
    public static final String CONS_COUNTRY_CODE_US                         = "US";
    public static final String CONS_COUNTRY_CODE_CANADA_CA                  = "CA";
    public static final String CONS_COUNTRY_CODE_CANADA_CAN                 = "CAN";
    public static final String CONS_STATE_CODE_VIRGIN_ISLANDS               = "VI";
    public static final String CONS_STATE_CODE_PUERTO_RICO                  = "PR";
    public static final String CONS_STATE_CODE_ALASKA                       = "AK";
    public static final String CONS_STATE_CODE_HAWAII                       = "HI";
    public static final String CONS_STATE_CODE_ARIZONA                      = "AZ";

    /* Source Code and DNIS Constants */
    public final static String CONS_ADVO_CODE                               = "ADVO";
    public final static String CONS_CORP15_CODE                             = "CORP15";
    public final static String CONS_CORP20_CODE                             = "CORP20";
    public final static String CONS_DISCOVER_CODE                           = "DISC1";
    public static final String CONS_PARTNER_PRICE_CODE                      = "ZZ";
    public final static String CONS_TARGET_CODE                             = "TARGET";
    public final static String CONS_UPROMISE_CODE                           = "UPROM";
    public final static String CONS_UPROMISE1_CODE                          = "UPROM1";

    //Some of the stored procs require a script code.  Since Mod Orders does not require a script code,
    //we have created this as a bogus script code to be passed around.
    public static final String CONS_MO_SCRIPT_CODE                          = "MO";


    /* Update Delivery Confirmation */
    public static final String DELIVERY_CONFIRMATION_UPDATE_TYPE            = "DELIVERY_CONFIRMATION";

    //*********************************************************************************************
    // end : Modify Order
    //*********************************************************************************************


    /*********************************************************************************************
    // Email alias parameters
    *********************************************************************************************/
    public static final String EMAIL_ALIAS                                  = "EMAIL_ALIAS";


    /*********************************************************************************************
    // Security parameters
    *********************************************************************************************/
    public static final String CONTEXT                                      = "context";
    public static final String SEC_TOKEN                                    = "securitytoken";
    public static final String SECURITY_IS_ON                               = "SECURITY_IS_ON";


    /*********************************************************************************************
    // Security resources
    *********************************************************************************************/
    public static final String REFUND_TAX                                   = "REFUND_TAX";
    public static final String REFUND                                       = "REFUND";
    public static final String UPDATE_RESPONSIBLE_PARTY                     = "UPDATE_RESPONSIBLE_PARTY";
    public static final String MESSAGE_MAINTENANCE                          = "MESSAGE_MAINTENANCE";
    public static final String OLD_WEBOE                                    = "OldWebOE";
    public static final String BYPASS_PRIVACY_POLICY_VERIFICATION_RESOURCE  = "BypassPrivacyPolicyVerificationResource";
    public static final String VIP_CUSTOMER_UPDATE 							= "VIP Customer Update";

    public static final String ADD                                          = "Add";
    public static final String DELETE                                       = "Delete";
    public static final String NO                                           = "No";
    public static final String UPDATE                                       = "Update";
    public static final String VIEW                                         = "View";
    public static final String YES                                          = "Yes";


    /*********************************************************************************************
    //preferred partner 
    *********************************************************************************************/
    public static final String PREFERRED_PARTNER_CONTEXT = "PREFERRED_PARTNER";
    public static final String PREFERRED_PARTNER_ORDER_ACCESS_RESTRICTION = "ORDER_ACCESS_RESTRICTION";
    public static final String PREFERRED_PARTNER_SOURCE_CODE_RESTRICTION = "COM_SOURCE_CODE_RESTRICTION"; 
    public static final String DEFAULT_PREFERRED_PARTNER = "DEFAULT";
    
    /*********************************************************************************************
    //services 
    *********************************************************************************************/
    public static final String SERVICE_CONTEXT = "SERVICE";
    public static final String GIFT_CODE_SERVICE_URL = "GIFT_CODE_SERVICE_URL";
    public static final String GIFT_CODE_SERVICE_TIMEOUT = "GIFT_CODE_SERVICE_TIMEOUT";
    
    /*********************************************************************************************
    //service products 
    *********************************************************************************************/
    public static final String FREE_SHIPPING_CONTEXT = "SERVICE_CONFIG";
    public static final String CART_FEES_SAVED_MESSAGE = "CART_FEES_SAVED_MESSAGE";
    public static final String ORDER_FEES_SAVED_MESSAGE = "ORDER_FEES_SAVED_MESSAGE";
    
    // Page data fields for services, they are appended with a service suffix
    public static final String PAGEDATA_SERVICE_DISPLAY_NAME = "service_displayname_";
    public static final String PAGEDATA_SERVICE_PROGRAM_DESCRIPTION = "service_description_";
    public static final String PAGEDATA_SERVICE_PROGRAM_URL = "service_url_";    
    public static final String PAGEDATA_SERVICE_SUFFIX_FREESHIP = "freeship";
    
    /*********************************************************************************************
    //custom parameters returned from database procedure
    *********************************************************************************************/
    public static final String STATUS_PARAM                                 = "OUT_STATUS";
    public static final String MESSAGE_PARAM                                = "OUT_MESSAGE";


    /*********************************************************************************************
    //handler descriptions
    *********************************************************************************************/
    public static final String PRIORITY_CODE_HANDLER = "PriorityCodeHandler";
    public static final String DISPOSITION_CODE_HANDLER = "DispositionCodeHandler";
    public static final String ACTIVE_OCCASIONS_HANDLER = "CACHE_ACTIVE_OCCASIONS";

    /*********************************************************************************************
    // Warning messages
    *********************************************************************************************/
    public static final String INTERNATIONAL_ORDER_NOT_SUPPORTED =
            "The current florist does not handle international orders.";
    public static final String PRODUCT_CANNOT_BE_DELIVERED =
            "The assigned product cannot be delivered on the assigned delivery date.  You will need " +
            "to select a new product or update the delivery date.";
    public static final String DELIVERY_SHIP_METHOD_NOT_ALLOWED =
            "The assigned product cannot be delivered with the assigned ship method.";
    public static final String PRODUCT_STATE_AGRICULTURAL_RESTRICATIONS =
            "The assigned product cannot be delivered to the requested state due to agricultural restrictions.";
    public static final String SAME_DAY_EXOTIC_UNAVAILABLE =
            "Same Day delivery is not available for exotic products. ";
    public static final String SUNDAY_DELIVERY_NOT_AVAILABLE_IN_ZIP =
            "Sunday delivery may not be available to this area. ";
    public static final String SAME_DAY_CUTOFF_EXPIRED =
            "Same Day cut-off has expired.";
    public static final String ZIP_IN_GNADD =
            "The zip / postal code is currently in GNADD. ";
    public static final String FLORIST_CANNOT_DELIVER_PRODUCT =
            "Currently none of the available florists, who are signed up for the " +
            "zip / postal code are able to deliver the requested product. ";
    public static final String PRODUCT_UNAVAILABLE =
            "The product is currently unavailable.  You will need to select another product.";
    public static final String SUNDAY_DELIVERY_NOT_ALLOWED_FOR_EXOTIC =
            "Sunday delivery may not be available to this area and the selected product is an exotic product.  " +
            "Exotic products require one day lead time for delivery. ";
    public static final String PRODUCT_DELIVERY_DATE_RESTRICTIONS =
            "The selected product has delivery date restrictions. ";
    public static final String PRODUCT_DELIVERY_DATE_RESTRICTIONS_WITH_DATE1 =
            "The selected product has delivery date restrictions of ";
    public static final String PRODUCT_DELIVERY_DATE_RESTRICTIONS_WITH_DATE2 =
            " to ";
    public static final String PRODUCT_DELIVERY_DATE_RESTRICTIONS_WITH_DATE3 =
            ".";
    public static final String INVALID_SOURCE_CODE =
            "The source code is invalid.  Please change the source code.";
    public static final String FLORIST_IN_OPT_OUT_STATUS =
            "The florist is in an opt out status and is not able to be selected.";
    public static final String INVALID_SOURCE_CODE_FOR_COMPANY =
            "The source code is invalid for company code: ";
    public static final String ORDER_ALREADY_PRINTED =
            "The vendor has already printed this order.";
    public static final String CANNOT_INCREASE_AMAZON_ORDER_TOTAL =
            "Cannot increase the order total for Amazon orders.";
    public static final String MERCURY_ORDER_ALREADY_ASSIGNED =
            "Mercury order has already been assigned.  Use the Communication screen.";
    public static final String INVALID_FLORIST_NUMBER =
            "Invalid florist number.";
    public static final String FLORIST_CODE_FLAGGED_AS_VENDOR =
            "Florist code is flagged as a vendor.";
    public static final String ORDER_NOT_IN_ZIP_Q =
            "Order not in Zip Q.  Update florist code through Update Order or Mercury Messaging.";
    public static final String DROP_SHIP_PRODUCT_DELIVERY_RESTRICTIONS =
            "Drop ship products cannot be delivered outside of the US.";
    public static final String NO_LIVE_MERCURY =
            "There is no live mercury order.";
    public static final String FLORIST_UNAVAILABLE =
            "Assigned florist is unavailable or blocked.";
    public static final String FLORIST_CANT_DELIVER_TO_ZIP =
            "Assigned florist cannot deliver to current zipcode.";
    public static final String FLORIST_CANT_DELIVER_TO_ZIP_OR_CITY =
            "Assigned florist cannot deliver to current zipcode or city.";
    public static final String FLORIST_DELIVERY_NOT_AVAILABLE =
            "Assigned product cannot be delivered by a florist. " +
            "Please choose another product or delivery method.";
    public static final String DROP_SHIP_NOT_AVAILABLE =
            "Vendor delivery for product is no longer available. ";
    public static final String SAME_DAY_DELIVERY_FOR_INTERNATIONAL_PRODUCTS =
            "Same day delivery is not available for International Products.";
    public static final String ASSIGNED_FLORIST_CANNOT_DELIVER_PRODUCT =
            "The assigned florist cannot deliver the requested product.";
    public static final String CORRECTIVE_ACTION_CONFIRMATION =
            " Do you wish to continue?";
    public static final String FLORIST_DOESNT_DELIVER_ON_SUNDAY =
            "The assigned florist does not deliver on Sunday.";
    public static final String NEW_FLORIST_REQUIRED =
            "You will be required to assign a new florist upon completing your order updates.";
    public static final String RECIP_PO_NOT_ALLOWED_ADDRESS_ONE =
            "PO or APO box is not allowed in address line one";
    public static final String RECIP_PO_NOT_ALLOWED_ADDRESS_TWO =
            "PO or APO box is not allowed in address line two";
    public static final String NO_FLORIST_ENTERED =
            "A florist will need to be assigned before this order can be sent to a florist.";
    public static final String NO_FLORISTS_AVAILABLE =
            "There are currently no florists available to deliver the selected product on the selected delivery date.  Please consult your florist directory.";
    public static final String CURRENT_DELIVERY_METHOD_NO_LONGER_AVAILABLE =
            "Current delivery method is no longer available for this product.  Please select another product or delivery method.";
    public static final String KEEP_ORDER_WITH_FLORIST =
            "Do you wish to leave this order with the current florist?";
    //order has been cancelled and refunded
    public static final String ORDER_CANCELLED_AND_REFUNDED =
            "Order has been refunded and cancelled and cannot be updated.";
    //order is in the credit queue
    public static final String ORDER_CURRENTLY_IN_CREDIT_QUEUE =
            "Order currently in Credit queue and cannot be updated.";
    //payments cannot be authorized
    public static final String PAYMENT_CANNOT_BE_AUTHORIZED =
            "Payment has not been verified and cannot be updated.";
    //mercury message cannot be authorized
    public static final String UNVERIFIED_MERCURY_MESSAGE =
            "Mercury has not been verified and cannot be updated.";
    //cart is not in final state
    public static final String CART_NOT_IN_FINAL_STATE_MESSAGE =
            "Order cannot be updated because another order in the cart is in Scrub, Pending, or on Hold.";
    //flower of the month product
    public static final String FLOWER_MONTHLY_PRODUCT =
            "Update Order cannot be accessed as this is a flower of the month order.";
    //ftdm - florist is a manual florist
    public static final String FTDM_ORDER =
            "FTDM order cannot be updated.";
    //requested delivery date has passed
    public static final String REQUESTED_DELIVERY_DATE_HAS_PASSED =
            "Selected Delivery Date has passed.  Please select another delivery date.";
    //delivery date was null
    public static final String DELIVERY_DATE_NULL =
            "The order does not have a Delivery Date.  Please contact the system admin.";
    //venus order in printed status
    public static final String VENUS_ORDER_PRINTED_STATUS =
            "Live printed Venus orders cannot be updated on or after the ship date and before the delivery date.";
    //venus order in shipped status failing the delivery date test
    public static final String VENUS_ORDER_SHIPPED_STATUS_DELIVERY_DATE =
            "Shipped Venus orders cannot be updated until on or after the delivery date.";
    //Partner order has already been modified
    public static final String PARTNER_ORDER_ALREADY_MODIFIED =
            "Order has already been updated.  This order can no longer be updated.";
    //order has already been modified
    public static final String ORDER_ALREADY_MODIFIED =
            "Order has already been updated, additional updates must be made to the new order number.  This order can no longer be updated.";
    //zone jump order in transit
    public static final String ZJ_ORDER_IN_TRANSIT =
             "This order is being prepared for shipment and cannot be changed at this time.";
    // This combo of addons can't be fulfilled by single vendor             
    public static final String ADDON_NO_VENDORS = "There is not a vendor that has the combination of vase and/or add-ons selected. Please make a different selection";  
    
 // MilitaryStar payment type cannot be modified in COM             
    public static final String PAYMENT_TYPE_MS_ERROR_MSG = "Order with Military Star payment cannot be updated.";  
    


    /*********************************************************************************************
    // Update Delivery Date Warning messages
    *********************************************************************************************/ 
    public static final String UDD_FTDM_ORDER =
            "This is a call out order (FTDM).  Please contact the filling florist by phone to make any updates to this order.";
    public static final String UDD_FTDC_ORDER = 
            "The order is no longer actively being filled by the florist/vendor.  Please check the message queue to determine what additional work needs to be done to fulfill this order.";
    public static final String UDD_NO_LIVE_MERCURY = 
            "The order is no longer actively being filled by the florist/vendor.  Please check the message queue to determine what additional work needs to be done to fulfill this order.";
    public static final String UDD_DELIVERY_DATE_PAST_MAX_ERROR = 
            "Order is greater than {max_number_of_days} days past delivery and cannot be updated.  Please see your supervisor, manager or trainer on how to handle this order.";
    public static final String UDD_UNAVAILABLE_ERROR = 
            "No additional delivery dates available for selected product, please use Update Order to select a different product.";
    public static final String UDD_NO_FLORISTS_AVAILABLE = 
            "No florists are available for selected delivery date. Please use Update Order to find an item for delivery on the date requested.";
    public static final String UDD_PRINTED_ORDER_ERROR = 
            "Order is in transit, no updates are possible at this time.";
    public static final String UDD_DELIVERY_DATE_TODAY_ERROR = 
            "Order is in transit, no updates are possible at this time.";
    public static final String UDD_FLOWER_MONTHLY_PRODUCT = 
            "Update Delivery Date cannot be accessed as this is a flower of the month order.";
    public static final String UDD_FTP_VENDOR_ORDER = 
            "Dropship orders cannot be updated.";
    public static final String UDD_SERVICE_FEE_ERROR = 
            "Some dates not available for current service fee, please use update order to select new date.";
    public static final String UDD_SHIPPED_STATUS_BEFORE_DELIVERY_DATE =
            "Order is in transit, no updates are possible at this time.";
    public static final String UDD_ADD_ON_UNAVAILABLE_ERROR = 
            "No additional delivery dates are available for selected vase and/or add-on(s); please use Update Order to select a different product.";

    //SN: Added this for UC 24094    
    public static final String RESPONSE_CAMS_UNREACHABLE = "System cannot determine {programName} eligibility at this time. Ask if customer has an {programName}.";
    
    // PC I3 12963 - UC : 24535
    public static final String PC_MEMSHIP_NOT_DETERMINED = "pcMembershipChkErrorMsg";
    public static final String PC_MEMSHIP_NOT_DETERMINED_ERR = "System cannot determine Premier Circle Membership status at this time.";
    public static final String PC_MEMBERSHIP_TYPE = "PC";
    public static final String PC_CUSTOMER_IND = "IS_PC_CUSTOMER";
    
    public static final String PHOENIX_IND = "IS_PHOENIX_ORDER";
    
    public static final String JOE_CONTEXT = "OE_CONFIG";
    public static final String IMAGE_SERVER_URL = "IMAGE_SERVER_URL";
    
    public static final String IN_PRO_ORDER_NUMBER = "in_pro_order_number";
    public static final String SC_PRO_ORDER_NUMBER = "sc_pro_order_number";
    public static final String PRO_ORDER_SEARCH_APPENDER = "PRO_ORDER_SEARCH_APPENDER";
    public static final String GLOBAL_CONFIG = "GLOBAL_CONFIG";
    public static final Object FORMATTED_ORD_NUMBER = "sc_formatted_ord_number";
    
    // Added for sorting columns data based on user input(header)
    public static final String SORT_COLUMN_NAME  = "sort_column";
    public static final String SORT_COLUMN_STATUS = "sort_status"; 
    public static final String DEFAULT_SORT_COLUMN_NAME  = "created_by";
    public static final String DEFAULT_SORT_COLUMN_STATUS = "DESC"; 
    public static final String CURRENT_SORT_COLUMN_STATUS = "current_sort_status";
    public static final String IS_FROM_HEADER = "is_from_header"; 
    public static final String USER_ID_FILTER = "userId_filter"; 
    public static final String GROUP_ID_FILTER = "groupId_filter"; 
    public static final String CODES_FILTER = "codes_filter";
    
    //Added to  retain Refund review sort filters.
    public static final String PREV_SORT_COLUMN_NAME  = "prev_sort_column";
    public static final String PREV_SORT_COLUMN_STATUS = "prev_sort_status"; 
    public static final String PREV_CURRENT_SORT_COLUMN_STATUS = "prev_current_sort_status";
    public static final String PREV_IS_FROM_HEADER = "prev_is_from_header"; 
    public static final String PREV_USER_ID_FILTER = "prev_userId_filter"; 
    public static final String PREV_GROUP_ID_FILTER = "prev_groupId_filter"; 
    public static final String PREV_CODES_FILTER = "prev_codes_filter";
    
    // Constants related to OrderComments tabs
    public static final String DEFAULT_TAB = "Comments";
    public static final String ORIGIN_TAB = "origin_tab";
    
	/**
	 * constructor
	 * 
	 * @param none
	 * @return n/a
	 * @throws none
	 */
	public COMConstants() {
		super();
	}

	//
	public static final String QUEUE_DELETE_HEADER_ID = "queueDeleteHeaderId";

	public static final String VL_YES = "Y";

	// Payment Routes
	public static final String PG_PS = "PG-PS";
	public static final String PG_JCCAS = "PG-JCCAS";
	public static final String PAYMENT_GATEWAY_ROUTES ="PAYMENT_GATEWAY_ROUTES";
	
}
