package com.ftd.customerordermanagement.cache.handler;

import com.ftd.customerordermanagement.dao.TagDAO;
import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.cacheMgr.vo.CacheHandlerBase;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import java.sql.Connection;
import java.sql.SQLException;

import javax.xml.parsers.ParserConfigurationException;



import org.w3c.dom.Document;
import org.xml.sax.SAXException;
/**
 * This class retrieves priority code information from database
 * tag order tables.
 *
 * @author Luke W. Pennings
 */

public class PriorityCodeHandler extends CacheHandlerBase
{
    private Logger logger = new Logger("com.ftd.customerordermanagement.cache.handler.PriorityCodeHandler");
    private Document tagPriorityCodesXMLDoc = null;
    /**
     * constructor
     * 
     * @param none
     * @return n/a
     * @throws none
     */
    public PriorityCodeHandler()
    {
        super();
    }
    
    
    /**
     * load priority code data
     * 
     * if priority code information is not returned, throw cache exception.
     * 
     * @param Connection      - database connection
     * @return String         - XML document of priority code information
     * @throws CacheException - cachecontroller error
     */
    public Object load(Connection conn) throws CacheException
    {
        try
        {
            // get data
            TagDAO tDAO = new TagDAO(conn);
            tagPriorityCodesXMLDoc = (Document) tDAO.getPriorityCodes();
            if(logger.isDebugEnabled())
            {
                //IMPORTANT:  Because StringWriter contents are NOT removed even when the flush
                //            method is executed, make StringWriter go away through scope control.
                //            Both the flush and close methods were tested with but were of non-affect.
                StringWriter sw = new StringWriter();       //string representation of xml document
                DOMUtil.print(tagPriorityCodesXMLDoc,new PrintWriter(sw));
                logger.debug("load(): tagPriorityCodesXMLDoc = \n" + sw.toString());
            }
        }
        catch (IOException ioe)
        {
            logger.error(ioe);
            throw new CacheException("IOException", ioe);
        }
        catch (ParserConfigurationException pce)
        {
            logger.error(pce);
            throw new CacheException("ParserConfigurationException", pce);
        }
        catch (SAXException saxe)
        {
            logger.error(saxe);
            throw new CacheException("SAXEException", saxe);
        }
        catch (SQLException sqle)
        {
            logger.error(sqle);
            throw new CacheException("SQLException", sqle);
        }
        catch (Exception e)
        {
            logger.error(e);
            throw new CacheException("Exception " +  e.toString());
        }

        return tagPriorityCodesXMLDoc;
    }
    
    
    /**
     * set cached object
     * 
     * @param Object  - cached object
     * @return void
     * @throws CacheException
     */
    public void setCachedObject(Object cachedObject) throws CacheException
    {
        tagPriorityCodesXMLDoc = (Document) cachedObject;
        return;
    }
    /**
     * get cached Document
     * 
     * @param None
     * @return Document
     * @throws None
     */
    public Document getPriorityCodeDoc()
    {
        return tagPriorityCodesXMLDoc;
    }
}