package com.ftd.customerordermanagement.to;

import java.io.Serializable;

public class FullRefundTO implements Serializable {
  private String orderDetailId;
	private String refundDispCode;
  private String responsibleParty;
  private String refundStatus;
  private String userId;

  public FullRefundTO() {
  }


  public void setOrderDetailId(String orderDetailId) {
    this.orderDetailId = orderDetailId;
  }


  public String getOrderDetailId() {
    return orderDetailId;
  }


  public void setRefundDispCode(String refundDispCode) {
    this.refundDispCode = refundDispCode;
  }


  public String getRefundDispCode() {
    return refundDispCode;
  }


  public void setResponsibleParty(String responsibleParty) {
    this.responsibleParty = responsibleParty;
  }


  public String getResponsibleParty() {
    return responsibleParty;
  }


  public void setRefundStatus(String refundStatus) {
    this.refundStatus = refundStatus;
  }


  public String getRefundStatus() {
    return refundStatus;
  }


  public void setUserId(String userId) {
    this.userId = userId;
  }


  public String getUserId() {
    return userId;
  }
  
  
}