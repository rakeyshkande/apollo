package com.ftd.customerordermanagement.mdb;

import com.ftd.customerordermanagement.bo.RefundBO;
import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.customerordermanagement.dao.CommentDAO;
import com.ftd.customerordermanagement.dao.OrderDAO;
import com.ftd.customerordermanagement.vo.CommentsVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;

import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.ArrayList;

import javax.ejb.MessageDrivenBean;
import javax.ejb.MessageDrivenContext;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import javax.sql.DataSource;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;



import org.xml.sax.SAXException;


public class RefundPartnerOrderMDB implements MessageDrivenBean, MessageListener 
{
  private MessageDrivenContext context;
  private static Logger logger  = 
          new Logger("com.ftd.customerordermanagement.mdb.RefundPartnerOrderMDB");
  private static final String USER_ID = "REFUND PARTNER ORDER";  
  private Connection conn = null;
  
  public void ejbCreate()
  {

  }

  public void onMessage(Message msg){
    logger.info("Entering onMessage");   
    
    try {
      conn = getDBConnection();
      
      //get message and extract order detail_id
      TextMessage textMessage = (TextMessage)msg;
      String orderDetailId = textMessage.getText();
     
      logger.info("processing EM_COM JMS MESSAGE: " + orderDetailId);
      
      // Initiate the RefundBO
      RefundBO rBO = new RefundBO(conn);
    
      // Variables                            
      ArrayList orderDetails = new ArrayList();
    
      // Add order to refund
      orderDetails.add(orderDetailId);
      
      //retrieve the vendor from the order detail record
      OrderDAO oDAO = new OrderDAO(conn);
       
      // Refund the order
      rBO.postFullRefundMO(orderDetails, COMConstants.PARTNER_REFUND_DISP, oDAO.getVendorId(orderDetailId),
                           "Unbilled", null, null, this.USER_ID);
      
      //Add order comment
      this.insertOrderComment(orderDetailId, conn);      
      
      logger.info("Leaving onMessage"); 
      
    } catch (Exception e) { 
      logger.info(e.toString());
      TextMessage message = (TextMessage) msg;
      try {
        this.sendSystemMessage(message.getText(), e.toString());
      } catch (Exception e2) 
      {
        logger.error(e2);
      }
    } finally 
    {
      try { 
        conn.close();
      } catch (Exception e) 
      {
        logger.error(e);
      }
    }
  }

  public void ejbRemove()
  {
  }

    public void setMessageDrivenContext(MessageDrivenContext ctx)
    {
      this.context = ctx;
    }
  
    /**
     * This method is used to enter comments on the original order.
     * @param orderDetailId
     * @throws java.lang.Exception
     */
    private void insertOrderComment(String orderDetailId, Connection connection) throws Exception 
    {

      //insert a record into the comments table
      //Instantiate CommentDAO
      CommentDAO commentDAO = new CommentDAO(connection);

      CommentsVO commentsVO = new CommentsVO();
      commentsVO.setComment(COMConstants.PARTNER_REFUND_DISP + " refund issued due to auto cancel of partner order.");
      commentsVO.setCommentOrigin(null);
      commentsVO.setCommentType("Order");
      commentsVO.setCreatedBy(this.USER_ID);
      commentsVO.setUpdatedBy(this.USER_ID);
      commentsVO.setOrderDetailId(orderDetailId);
      commentsVO.setOrderGuid(null);
      commentsVO.setCustomerId(null);
      commentDAO.insertComment(commentsVO);
    } 
    
    /**
     * Obtain connectivity with the database
     * @param none
     * @return Connection - db connection
     * @throws Exception
     * @throws IOException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws TransformerException
     * @throws XSLException
     */

      private Connection getDBConnection()
              throws IOException, ParserConfigurationException, SAXException, TransformerException, 
             Exception
      {
        Connection conn = null;
        conn = DataSourceUtil.getInstance().getConnection(
                ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE,
                                                            COMConstants.DATASOURCE_NAME));
        return conn;
      }
    
  
    /*
     * This method sends a message to the System Messenger.
     * 
     * @param String message
     * @returns String message id
     */
    private String sendSystemMessage(String message, String error) throws Exception
    {
        Connection conn = null;
        String messageID = "";
    
        try{
          logger.info("Sending System Message:" + message);         
                
          ConfigurationUtil configUtil = ConfigurationUtil.getInstance();            
          String messageSource = "REFUND PARTNER ORDERS";
    
          //build system vo
          SystemMessengerVO sysMessage = new SystemMessengerVO();
          sysMessage.setLevel(SystemMessengerVO.LEVEL_PRODUCTION);
          sysMessage.setSource(messageSource);
          sysMessage.setType("ERROR");
          sysMessage.setMessage(message);
       
          SystemMessenger sysMessenger = SystemMessenger.getInstance();
          messageID = sysMessenger.send(sysMessage,conn);
    
          if(messageID == null)
          {
            String msg = "Unrecoverable error processing partner refund: " + message + " Caused exception: " + error;
            logger.info(msg);
          }
        }
        finally
        {
                if( conn!=null && !conn.isClosed() ) {
                                conn.close();
                }
        }
    
          return messageID;  
    } 
    
  
}