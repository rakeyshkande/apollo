package com.ftd.customerordermanagement.heartbeat;

import com.ftd.customerordermanagement.constants.COMConstants;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.ping.dao.HeartBeatDAO;
import com.ftd.osp.utilities.ping.servlet.BaseHeartBeatServlet;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;
import java.io.PrintWriter;

import java.sql.Connection;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;



import org.xml.sax.SAXException;


public class CheckPulseServlet
  extends BaseHeartBeatServlet
{
  private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
  
  private Logger logger = 
        new Logger("com.ftd.customerordermanagement.heartbeat.CheckPulseServlet");

  public void init(ServletConfig config)
    throws ServletException
  {
    super.init(config);
  }
  /**
   * 
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
  */
  public void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException
  {
    logger.debug("Entered doGet()"); 
    
    super.doGet(request, response);
    
    logger.debug("Leaving doGet()"); 
    
  }
  
  
 /**
   * Obtain connectivity with the database
   * @return Database Connection
   * @throws Exception
   */
	public Connection getDBConnection()
		throws Exception
	{
    logger.debug("Entered getDBConnection()");
		Connection conn = null;
		conn = DataSourceUtil.getInstance().getConnection(
				   ConfigurationUtil.getInstance().getProperty(COMConstants.PROPERTY_FILE,
															   COMConstants.DATASOURCE_NAME));
    logger.debug("Established DB Connection"); 
		return conn;
	}


  
}
