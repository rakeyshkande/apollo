var UPDATE_COMMENT = UPDATE_COMMENT || {};

//****************************************************************************************************
//* updateComment
//****************************************************************************************************
function updateComment(commentType, commentId)
{

	//Modal_URL
	var modalUrl = "updateComment.html";

	//Modal_Arguments
	var modalArguments = new Object();

	//Modal_Features
	var modalFeatures = 'dialogWidth:650px; dialogHeight:500px; center:yes; status=no; help=no; resizable:no; scroll:yes';

	//create the arguments
	var type = 'comment';
	var text = document.getElementById("td_" + commentId).innerText;
	var title = 'Update ' + commentType;

	//set the arguments
	modalArguments.type = type;
	modalArguments.text = text;
	modalArguments.title = title;

	//inovke the popup
	var ret = window.showModalDialog(modalUrl, modalArguments, modalFeatures);

	//NOTE that the returned field is an array.  Retrieve the values from the array
	var action = ret[1];
	var newText = ret[2];

	if (action == 'edit')
	{
		document.getElementById("comment_id").value = commentId;
		document.getElementById("comment_text").disabled = false;
		document.getElementById("comment_text").value = newText;
		document.getElementById("action_type").value = 'edit_comment';
		document.forms[0].action = "addComments.do";
		document.forms[0].submit();
	}

	if (action == 'delete')
	{
		document.getElementById("comment_id").value = commentId;
		document.getElementById("action_type").value = 'delete_comment';
		document.forms[0].action = "addComments.do";
		document.forms[0].submit();

	}


}


//****************************************************************************************************
//* Initialization
//****************************************************************************************************
function init()
{
	//populate the text area, displaying existing text, dynamically
	var regExp = new RegExp("<br>","g");
	var regExp2 = new RegExp("<BR>","g");
	text = text.replace(regExp,"\n");
	text = text.replace(regExp2,"\n");
	document.getElementById("taForNewText").value = text;

	//set the return data
	returnData[1] = "cancel";
	returnData[2] = "";

	//this is the default returnData, which is same as if the CSR hit NO
	window.returnValue = returnData;
}



/*********************************************************************************************
* doDelete() - pass delete info back to calling method
**********************************************************************************************/
function doDelete()
{

	//set the return data
	returnData[1] = "delete";
	returnData[2] = "";

  returnNewText(returnData);

}


/*********************************************************************************************
* doExit() - pass cancel info back to calling method
**********************************************************************************************/
function doExit()
{
	//set the return data
	returnData[1] = "cancel";
	returnData[2] = "";

  returnNewText(returnData);

}


/*********************************************************************************************
* doSubmit() - pass save info back to calling method
**********************************************************************************************/
function doSubmit()
{
	var newText = document.getElementById("taForNewText").value;

	newText = stripInitialWhitespace(newText);

	if (newText.length == 0)
	{
		alert("Please enter something to save.");
	}
	else
	{
		if (newText.length > 4000)
		{
			alert("Text must be less than 4000 characters.");
		}
		else
		{
			returnData[1] = "edit";
			returnData[2] = newText;

			returnNewText(returnData);
		}
	}

}


/*********************************************************************************************
* returnNewText() - close the window and return a value.
**********************************************************************************************/
function returnNewText(returnData)
{
	window.returnValue = returnData;
	window.close();
}

/**
 * checkLock.  called from customerComments.xsl and orderComments.xsl
 * requires jquery
 * load the XML document that locks comment editing for us
 * this is a jquery method that will load the document XML asynchronously.
 * The success method will execute upon a HTTP 200 response.
 *  http://api.jquery.com/jQuery.ajax/
 *  
 *  Normally we would load the load the contents of the remote document into a hidden iframe
 *  which would execute some javascript.  However the customer comments and order comments page
 *  have been having "freezing" issues where the user clicks the add button and the comments box 
 *  gets enabled but the save button never gets enabled.  Therefore the code has been refactored
 *  to more reliably enable the comments box.  
 *  prod issues: #9760, #7212
 */
UPDATE_COMMENT.checkLock = function() {
	var form = document.forms[0];
	var url = "addComments.do?action_type=add_comment" +
    "&comment_type=" + form.comment_type.value +
    "&order_detail_id=" + form.order_detail_id.value +
    "&customer_id=" + form.customer_id.value+
    "&securitytoken="+form.securitytoken.value+
    "&context="+form.context.value+
    "&t="+new Date().getTime();

	$.ajax({
		type: "GET",
		url: url,
		dataType: "xml",
		cache: false,
		success: function(xml) {
		  //enable the comment box if we got the lock
		  var lock_obtained = $(xml).find("OUT_LOCK_OBTAINED").text();
		  if (lock_obtained == "Y") {
			  $("#comment_text")[0].disabled=false;     
		      $("#comment_text").focus();   
		      $("#saveAction")[0].disabled=false;
		      $("#addComment")[0].disabled=true;
		      if($("#shoppingCart")[0]!=undefined) {
		        $("#shoppingCart")[0].disabled=false;		        
		      }
		      //remove any lock error text
		      $("#lockError").text("");
		      
		  } else {
			  $("#comment_text")[0].disabled=true;
			  var locked_by_id = $(xml).find("OUT_LOCKED_CSR_ID").text();
			  $("#lockError").text("Cannot add new comment. Current record is locked by " + locked_by_id);
		  }
		},
		error: function() {
			alert('Server error while trying to obtain comment edit lock');
		}
	});
};


