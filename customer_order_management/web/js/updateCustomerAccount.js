/*
 * sets the height of the brands scrolling area
 */
function setScrollingDivHeight(){
   var orderListingDiv = document.getElementById("orderListing");
   var newHeight = document.body.clientHeight - orderListingDiv.getBoundingClientRect().top - 185;
   if (newHeight > 15)
      orderListingDiv.style.height = newHeight;
}

/*******************************************************************************
*  setUpdateCustomerAccountIndexes()
*  Sets tabs for Update Customer Account Page
*  See tabIndexMaintenance.js
*******************************************************************************/
function setUpdateCustomerAccountIndexes()
{
  var begIndex = findBeginIndex();
  var tabArray1 = new Array('customerRecordLink', 'uc_customer_first_name',
  'uc_customer_last_name', 'uc_business_name', 'uc_customer_address_1',
  'uc_customer_address_2', 'uc_customer_city','uc_customer_state',
  'uc_customer_zip_code', 'cityLookupLink', 'uc_customer_country',
  'uc_daytime_customer_phone_number', 'uc_daytime_customer_extension',
  'uc_evening_customer_phone_number', 'uc_evening_customer_extension','uc_vip_customer');
  begIndex = setTabIndexes(tabArray1, begIndex);
  var tabArray2 = document.getElementsByName('uc_direct_mail_subscribe_status');
  for(var x = 0; x < tabArray2.length; x++)
  {
    var element1 = tabArray2[x];
    if ((element1 != null))
    {
      element1.tabIndex = begIndex;
      begIndex++;
    }
  }
  var tabArray3 = new Array ('updateCustEmail', 'addCustEmail',
    'ucaCustomerComments', 'ucaEmailHistory', 'ucaSubmitChanges','ucaSearch',
    'ucaBack', 'ucaMainMenu');
  begIndex = setTabIndexes(tabArray3, begIndex);
  return begIndex;
}

/*
 * Validate form fields before submit
 * Validates form fields to make sure
 * they are in the required format
 */
function doValidate(){
   var returnString;
   if( isRecipient == 'Y'  ){
     returnValue = true;
   }
   else  {
     var dayPhone = doValidateDayPhone();
     var evePhone = doValidateEveningPhone();
     var phones = doValidatePhones();
     var dayExt = doValidateDayPhoneExt();
     var eveExt = doValidateEvePhoneExt();
     var zip = doValidateZipCode();
     var name = doValidateName();
     var state = doValidateState();
     returnValue = phones && dayPhone && evePhone && dayExt && eveExt && zip && name && state;     
   }
   return returnValue;
}

function doValidatePhones() {
  var day = document.getElementById("uc_daytime_customer_phone_number").value;
  var eve = document.getElementById("uc_evening_customer_phone_number").value;

  if ( isWhitespace(day) && isWhitespace(eve) ) {
    errorFields.push('uc_daytime_customer_phone_number');
    errorDivs.push('ucaDayPhoneError');
    errorFields.push('uc_evening_customer_phone_number');
    errorDivs.push('ucaEveningPhoneError');
    return false;
  }
  return true;
}

/*
 * Validates the First & Last Name fields
 */
function doValidateName(){
  var fname = document.getElementById('uc_customer_first_name').value;
  var lname = document.getElementById('uc_customer_last_name').value;

  var fnameEditted;
  var lnameEditted;
  var i;

  for (i = 0; i < lname.length; i++)
  {
    // remove all the whitespace from the last name and then check if the CSR entered
    // valid alphabets
    var c = lname.charAt(i);
    if (isWhitespace(c) || c == "."  || c == "'" || c == "-")
    {}
    else
    {
    lnameEditted += c;
    }
  }

  for (i = 0; i < fname.length; i++)
  {
    // remove all the whitespace from the last name and then check if the CSR entered
    // valid alphabets
    var c = fname.charAt(i);
    if (isWhitespace(c) || c == "." || c == "&" || c == "'" || c == "-")
    {}
    else
    {
    fnameEditted += c;
    }
  }

  if(!isAlphabetic(fnameEditted))
  {
    errorFields.push('uc_customer_first_name');
    errorDivs.push('ucaFirstNameError');
  }else{
    for(var i = 0; i < errorDivs.length; i++)
    {
      if(errorDivs[i] == 'ucaFirstNameError')
      {
        clearErrorDiv(errorDivs[i]);
        clearErrorFields(errorFields[i]);
        errorDivs.splice(i,1);
        errorFields.splice(i,1);
      }
    }
  }
  if(!isAlphabetic(lnameEditted))
  {
    errorFields.push('uc_customer_last_name');
    errorDivs.push('ucaLastNameError');
  }else{
    for(var i = 0; i < errorDivs.length; i++)
    {
      if(errorDivs[i] == 'ucaLastNameError')
      {
        clearErrorDiv(errorDivs[i]);
        clearErrorFields(errorFields[i]);
        errorDivs.splice(i,1);
        errorFields.splice(i,1);
      }
    }
  }
  var ret = ( isAlphabetic(lnameEditted) && isAlphabetic(fnameEditted) ) ? true:false;
 return ret;
}

//validate state By zipcode.
function doValidateState(){
	if(!validateStateByZipCode())
	{
		errorFields.push('uc_customer_state');
	    errorDivs.push('ucaStateError');
	    return false;
	}
	return true;
}

/*
 * Validate Zip Code
 */
function doValidateZipCode(){

  var isValid = validateZipCode('uc_customer_zip_code','uc_customer_country',false);
  if( isValid )
  {
    for(var i = 0; i < errorDivs.length; i++)
    {
      if(errorDivs[i] == 'ucaZipCodeError')
      {
         clearErrorDiv(errorDivs[i]);
         clearErrorFields(errorFields[i]);
         errorDivs.splice(i,1);
         errorFields.splice(i,1);
      }
    }
    return true;
  }
  else{
      errorFields.push('uc_customer_zip_code');
      errorDivs.push('ucaZipCodeError');
  return false;
  }
}

/*
 * Validate Phone Numbers
 */
function doValidateDayPhone(){
  var field = document.getElementById('uc_daytime_customer_phone_number').value;
  var re = /[\s()-]+/g;
  var newField = field.replace(re,"");
  var countrySelected = document.getElementById('uc_customer_country').value;

  if( isWhitespace(newField) ||
      (countrySelected.toUpperCase() == 'US' && isUSPhoneNumber(newField,false))    ||
      (countrySelected.toUpperCase() != 'US' && isInternationalPhoneNumber(newField,false)) )
  {
    for(var i = 0; i < errorDivs.length; i++)
    {
      if(errorDivs[i] == 'ucaDayPhoneError')
      {
        clearErrorDiv(errorDivs[i]);
        clearErrorFields(errorFields[i]);
        errorDivs.splice(i,1);
        errorFields.splice(i,1);
      }
    }
    return true;
  }
  else
  {
    errorFields.push('uc_daytime_customer_phone_number');
    errorDivs.push('ucaDayPhoneError');
    return false;
  }
}

/*
 * Validate the Daytime phone extention
 */
function doValidateDayPhoneExt(){
  var phoneNum = document.getElementById('uc_daytime_customer_phone_number').value;
  var re = /[\s()-]+/g;
  var phoneNumEditted = phoneNum.replace(re,"");
  var ext = document.getElementById('uc_daytime_customer_extension').value;

  if(isInteger(ext,true))
  {
    if (ext.length > 0 && phoneNumEditted.length == 0)
    {
      errorFields.push('uc_daytime_customer_extension');
      errorDivs.push('ucaDayPhoneError');
      return false;
    }
    else
    {
      for(var i = 0; i < errorFields.length; i++)
      {
        if(errorFields[i] == 'uc_daytime_customer_extension')
        {
          clearErrorDiv(errorDivs[i]);
          clearErrorFields(errorFields[i]);
          errorDivs.splice(i,1);
          errorFields.splice(i,1);
        }
      }
      return true;
    }
  }
  else
  {
    errorFields.push('uc_daytime_customer_extension');
    errorDivs.push('ucaDayPhoneError');
    return false;
  }
}
/*
 * Validate the evening phone extentsion
 */
function doValidateEvePhoneExt(){
  var phoneNum = document.getElementById('uc_evening_customer_phone_number').value;
  var re = /[\s()-]+/g;
  var phoneNumEditted = phoneNum.replace(re,"");

  var ext = document.getElementById('uc_evening_customer_extension').value;

  if(isInteger(ext,true))
  {
    if (ext.length > 0 && phoneNumEditted.length == 0)
    {
      errorFields.push('uc_evening_customer_extension');
      errorDivs.push('ucaEveningPhoneError');
      return false;
    }
    else
    {
      for(var i = 0; i < errorFields.length; i++)
      {
        if(errorFields[i] == 'uc_evening_customer_extension')
        {
          clearErrorDiv(errorDivs[i]);
          clearErrorFields(errorFields[i]);
          errorDivs.splice(i,1);
          errorFields.splice(i,1);
        }
      }
      return true;
    }
  }
  else
  {
    errorFields.push('uc_evening_customer_extension');
    errorDivs.push('ucaEveningPhoneError');
    return false;
  }
}
/*
 * Validates the Evening Phone Number Field
 */
function doValidateEveningPhone(){
  var field = document.getElementById('uc_evening_customer_phone_number').value;
  var re = /[\s()-]+/g;
  var newField = field.replace(re,"");
  var countrySelected = document.getElementById('uc_customer_country').value;


  if( isWhitespace(newField) ||
      (countrySelected.toUpperCase() == 'US' && isUSPhoneNumber(newField,false))               ||
      (countrySelected.toUpperCase() != 'US' && isInternationalPhoneNumber(newField,false)) )
  {
    for(var i = 0; i < errorDivs.length; i++)
    {
      if(errorDivs[i] == 'ucaEveningPhoneError')
      {
        clearErrorDiv(errorDivs[i]);
        clearErrorFields(errorFields[i]);
        errorDivs.splice(i,1);
        errorFields.splice(i,1);
      }
    }
    return true;
  }
  else
  {
    errorFields.push('uc_evening_customer_phone_number');
    errorDivs.push('ucaEveningPhoneError');
    return false;
  }
}
/*
 * Set the country drop down list
 * to the customers current country code.
 */
function setCountry(selectedCountry){
    if(selectedCountry != ''){
     var selectBox = document.getElementById('uc_customer_country');
     for (var i = 0; i < selectBox.options.length; i++){
        if (selectBox[i].value == selectedCountry){
            selectBox.selectedIndex = i;
            break;
        }
    }
   }
}


/*
 * Displays the Subscribe-Unsubscribe history
 * of the customer
 */
function doEmailHistoryAction(url){

   var modalUrl = url +
       '?scCustomerId=' + document.getElementById('uc_customer_id').value +
        getSecurityParams(false);
     var modalFeatures  =  'dialogWidth:800px;dialogHeight:445px;center:yes;status:no;help:no;';
          modalFeatures +=  'resizable:no;scroll:no';
     showModalDialogIFrame(modalUrl, "", modalFeatures);
}
/*
 * This function is executed on click of Submit Changes button
 * The server transforms the processUpdateCustomerAccountIFrame.xsl
 * and then calls the page's doSubmitChangesAction() to finish the process
 */
function doSubmitChangesAction(){
pageAction = 'complete';
  var url = '';
  url = 'updateCustomerAccount.do?action=save_customer_changes';
  var frame = document.getElementById('ucActions');

  frame.src = (url + retrieveAllFields(false) +  getSecurityParams(false));
}

function doCheckCustomerAccount() {

  var reqF =  doRequiredFields();
  var valid = doValidate();

  if (reqF || !reqF) {
    if(valid) {
      resetErrorFields();

      pageAction = 'submit';
      if(!uc_change_flag && refresh_flag.toUpperCase() != 'Y'){
       alert('Customer account information was not updated, no changes were made.');
       return;
      }
      showWaitMessage("outterContent", "wait", "Please wait validating input");
      var frame = document.getElementById('ucActions');
      var url = 'updateCustomerAccount.do?action=check_customer_account' + getSecurityParams(false);
      frame.src = (url + retrieveAllFields(false));
    }
    else {
      setErrorFields(errorFields);
      setErrorDivs(errorDivs);
      displayErrorAlert();
    }
 }
}

function displayErrorAlert(){
  alert('Please correct the marked fields.');
  return;
}
/*
 * Display the update customer account page
 */
function displayCustomerPage(){
   var content = document.getElementById('outterContent');
   var waitDiv = document.getElementById("waitDiv").style;
   content.style.display = "block";
       waitDiv.display = "none";
}

/*
 * This function is used to tack on all the fields and send them to the server.
 * So the server may double check whether or not the user has made
 * any changes to the data.
 */
function retrieveAllFields(appendFlag){
   /*replaces the (), space, and - symbols in the phone fileds */
var phReg=/[\s()-]+/g;
   /* replaces the # symbol in the address fields */
var reg = /#/g;
var toReplace = "%23";
var value = '';
var name = '';
  var form = document.forms[0];
  var url = '';
  for(var i = 0; i < requiredParms.length; i++)
  {

    var ele = document.getElementById( requiredParms[i] );
    name = ele.name;
    value = ele.value;
    if(name=='uc_vip_customer')
    	{
    	 value = document.getElementById("uc_vip_customer").checked;
    	 url += '&' + name + '=' + value;
    	}
    
    if( ele.type == 'text' )
    {
      if(name == 'uc_evening_customer_phone_number' ||
             name == 'uc_daytime_customer_phone_number')
        {
        value = ele.value.replace(phReg,"");
      }
      else
      {
        value = ele.value.replace(reg,toReplace);
      }
    }
    if(name == 'uc_customer_id' && appendFlag){
      name = 'customer_id';
    }
    url += '&' + name + '=' + escape(value);
  }

  for(var m = 0; m < form.elements.length; m++){
    if(form.elements[m].options )
    {
      url += '&' + form.elements[m].name + '=' +
      form.elements[m].options[form.elements[m].selectedIndex].value;
    }
  }

  for(var x = 1; x <= form.uc_direct_mail_subscribe_status_count.value; x++)
  {
    url +=  '&' + document.getElementById('uc_direct_mail_company_id_' + x).name + '=' +
            document.getElementById('uc_direct_mail_company_id_' + x).value;
  }

  return url;
}

function getCustomerPhoneIds(){
  var url = '';
  return url += "&" + document.getElementById('uc_daytime_customer_phone_id').name +
        "=" + document.getElementById('uc_daytime_customer_phone_id').value +
        "&" + document.getElementById('uc_evening_customer_phone_id').name +
        "=" + document.getElementById('uc_evening_customer_phone_id').value

}
/*
 * If any changes were made to this page this flag is set to true.
 * We make no data checks here the server will handle that.
 * We simply check if a user modified any fields in anyway.
 */
function doOnChangeAction(obj){
  uc_change_flag = true;
}

function doCheckOnKeyDown(obj){
  uc_change_flag = true;
}


/*
 * Verify wether or not changes were made to the page.
 * If no changes go to Search page.
 * If there were changes ask the csr if they wish to continue w/out saving.
 */
function doUCASearchAction(){
 if ( uc_change_flag )
 {
  var flag = doExitPageAction(WARNING_MSG_4);
  if(flag){
    errorOverride = true;
    pageAction = 'search';
    doReleaseLockAction();
  }else{
    return;
  }
  }else{
   doReleaseLockAction();
   doSearchAction();
 }
}

/*
 * Executes the Main Menu button action.
 */
function doUCAMainMenuAction(){
 if ( uc_change_flag )
 {
  var flag = doExitPageAction(WARNING_MSG_4);
  if(flag){
        errorOverride = true;
    pageAction = 'main_menu';
    doReleaseLockAction();
  }else{
    return;
  }
  }else{
    doReleaseLockAction();
   doMainMenuAction();
 }
}


/*
 *  Perorms the Back button functionality
 */
function doBackAction()
{
  var url =  "customerOrderSearch.do?action=customer_search&customer_id=" +
              document.getElementById('customer_id').value +
              "&last_order_date=Recipient";
  performAction(url);

}



/*
 *  Perorms the Back button functionality
 */
function doUCABackAction()
{
  if ( uc_change_flag )
  {
    var flag = doExitPageAction(WARNING_MSG_4);
    if(flag)
    {
      errorOverride = true;
      pageAction = 'back';
      doReleaseLockAction();
    }
    else
    {
      return;
    }
  }
  else
  {
    doReleaseLockAction();
    doBackAction();
  }

}




/*
 * Checks to see if any changes have been made.
 * If no changes are made forward user  to the Customer Comments page.
 * If there were changes made ask the user if the wish to navigate away
 * without saving changes.
 */
function doUCACustomerCommentsAction(){
  if ( uc_change_flag ) {
    var flag = doExitPageAction(WARNING_MSG_4);
    if(flag){
      errorOverride = true;
      pageAction = 'comments';
      doReleaseLockAction();
    }else{
      return;
    }
  }else{
    doReleaseLockAction();
    doRetrieveCustomerComments();
  }
}

/*
 * Retrieve the customers comments
 */
function doRetrieveCustomerComments(){
  var url = "loadComments.do?page_position=1&comment_type=Customer";
  performAction(url);

}

/*
 * This function is called when the user wants to navigate
 * away from the update customer account page w/out saving changes.
 * This function will release the lock on the record and the xsl
 * that is popuplated in the IFrame will call the appropraite function
 */
function doReleaseLockAction(){
  var frame = document.getElementById('ucActions');
  var url = 'updateCustomerAccount.do?action=release_lock' +
            '&entity_id=' + document.getElementById('uc_customer_id').value +
            getSecurityParams(false) +  '&entity_type=CUSTOMER';

 frame.src = (url + retrieveAllFields(false));
}

/*
 * Handles the Add Email Address button click
 */
function doAddEmailAddress(){
    var modalUrl = 'updateCustomerEmail.do?action=add_customer_email' +
          getSecurityParams(false) + retrieveAllFields(false) + '&page_action=add';
    var modalFeatures  =  'dialogWidth:800px;dialogHeight:445px;center:yes;status:no;help:no;';
        modalFeatures +=  'resizable:no;scroll:no';

    ret = showModalDialogIFrame(modalUrl, "", modalFeatures);
    refreshPage();
}

/*
 * Handles the Update Email Status button click
 * Navigates the user to Email Addresses page.
 */
function doUpdateCustomerEmail(){
  var modalUrl = 'updateCustomerEmail.do?action=more_customer_emails' +
         getSecurityParams(false);

  var modalFeatures  =  'dialogWidth:800px;dialogHeight:445px;center:yes;status:no;help:no;';
      modalFeatures +=  'resizable:no;scroll:no';

  modalUrl += retrieveAllFields(false);
  ret = showModalDialogIFrame(modalUrl, "", modalFeatures);

  refreshPage();
}

/*
 * This function refreshes the updateCustomerAccount page
 * we set the refreshOverride variable to tru in order to by pass
 * this page's validation -
 */
function refreshPage(){
   refreshOverride = true;
   var url = 'loadCustomerAccount.do?action=load_customer_account' +
      retrieveAllFields(true) + getSecurityParams(false) + '&data_exists=y';
    performAction(url);
}

/*
 * After the form changes are updated correctly and there
 * are no problems return the user to the order.
 */
function doUpdateCustomerSearch(){
  var url;
  var display = document.getElementById('display_page').value.toUpperCase();
  if(display == 'ORDERS' || display == 'ACCTINFO')
  {
    url = "customerOrderSearch.do?action=customer_account_search" +
                  "&order_guid=" + document.getElementById('uc_order_guid').value +
                  "&order_number=" + document.getElementById('uc_master_order_number').value +
                  "&customer_id=" + document.getElementById('uc_customer_id').value +
                   getSecurityParams(false);
  }
  if(display == 'CUSTOMER')
  {
    url = "customerOrderSearch.do?action=customer_search" +
                  "&customer_id=" + document.getElementById('uc_customer_id').value +
                   getSecurityParams(false);
  }

    performAction(url);
}
/*
 * Loop through the errorDivs array; if any elements
 * are available set the display on them to signal the user
 * there is a problem
 */
function setErrorDivs(elements){
  var element;
    for (var i = 0; i < elements.length; i++){
        element = document.getElementById(elements[i]);
        if (element != null){
            element.style.display = "block";
        }
    }
}
/*
 * if the text boxes are set to invalid
 * becuase of bad data this will clear them when
 * they are fixed by the user
 */
function clearErrorFields(element){
  var element;
  element = document.getElementById(element);
  if(element != null)
    element.className = '';
}


/*
 * clear out the error divs, we do not want
 * the errorDiv array to have divs that do not belong.
 */
function clearErrorDiv(element){
  var element;
    element = document.getElementById(element);
    if (element != null){
       element.style.display = "none";
    }
}
/*
 * Loop through the requiredFields array; if there
 * are any elements we then call the setErrorFields()
 * method which sets the background color of the text fields
 * to signal an error.
 */
function doRequiredFields(){
 var fields = new Array();
 for(var i = 0; i < requiredFields.length; i++){
    if(document.getElementById(requiredFields[i]).type == 'text'){
       if(document.getElementById(requiredFields[i]).value == ''){
         fields.push(requiredFields[i]);
       }
     }
   }

   if(fields.length > 0){
      setErrorFields(fields);
    return false;
   }
  return true;
}


/*
 * Opens the city lookup lookup div. Note that this method will override the enterCheckCustomerAccount()
 * which is used at the <body> tag level.
 */
function doSubmitCityLookup()
{
	if (window.event.keyCode == 13)
	{
		doCityLookup();
	}
}

/*
 * Opens the city lookup lookup div.
 */
function doCityLookup(){
	document.getElementById("submitSwitch").value = 'no';
	uc_change_flag = true;

  CityPopup.setup(
  {
   displayArea:'cityLookupLink',
   city: 'uc_customer_city',
   state: 'uc_customer_state',
   zip: 'uc_customer_zip_code',
   country: 'uc_customer_country'
  }
  );
}
/*
 * resetErrorFields()
 */
function resetErrorFields(){
    for(var i = 0; i < errorFields.length; i++)
    {
      clearErrorDiv(errorDivs[i]);
      clearErrorFields(errorFields[i]);
    }

    for(var i = 0; i < requiredFields.length; i++)
    {
      clearErrorFields(requiredFields[i]);
    }
}


/*
 * Main function of this page
 * -initiates actions
 */
function performAction(url){
  if(doRequiredFields() || errorOverride || refreshOverride)
  {
  if(doValidate() || errorOverride || refreshOverride)
  {
         scroll(0,0);
         showWaitMessage("outterContent", "wait", "Please wait validating input");
         document.forms[0].action = url;
         document.forms[0].submit();
  }
  else
    {
      setErrorFields(errorFields);
      setErrorDivs(errorDivs);
    }
  }
}

function enterCheckCustomerAccount(){

		var submitSwitch = document.getElementById("submitSwitch").value;

		if (window.event.keyCode == 13)
		{
			if (submitSwitch == 'no')
				document.getElementById("submitSwitch").value = 'yes'
			else
	      doCheckCustomerAccount();
		}

}

function validateStateByZipCode(){
	var returnVal = false;
	
	var zipCode = document.getElementById('uc_customer_zip_code').value;
	var state = document.getElementById('uc_customer_state').value;
	
	if(document.getElementById('uc_customer_country').value != 'US'){
		return true;
	}
	
	if(state == null || state == undefined || state == ''){
		return true;
	}
	
	if(zipCode == null || zipCode == undefined || zipCode == ''){
		return true;
	}
	var requestURL = "validateAjax.do?"
		 	+ "method=validateState"
		 	+"&recipient_state=" + state
		    + "&recip_zip_code=" + zipCode
		    +"&recipient_city=" + ''
		    + "&securitytoken=" + document.getElementById("securitytoken").value
		    + "&context=" + document.getElementById("context").value;
	 
	var data = getSynchronousResponse(requestURL);
	if(eval(data.responseText) == null || eval(data.responseText) == undefined){
	      window.location = "http://" + window.location.host + "/secadmin";
	      return;
	 }else{
	      var outValue = eval(data.responseText);
	      
	      if(outValue == 'YES'){
	    	  returnVal = true;
	      }else{
	    	  returnVal = false;
	      }
	 }
	return returnVal;
}