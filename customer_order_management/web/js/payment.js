// The following js file is a utility file for the modify order payment.xsl page

var hasShownAlert = false;
var pinFieldName = "gcc_pin";
var gccnumFieldName = "gcc_num";

  //INTIALIZATION METHODS //

  function init()
  {
    document.oncontextmenu = contextMenuHandler;
    document.onkeydown = pBackKeyHandler;
          
    // credit card //
    if (document.getElementById("billingPrompt").value == "")
    {    
    	buildCCYearList('exp_year');
    	if (document.getElementById("lastCCExp"))
    	{
    		var expDate = document.getElementById("lastCCExp").value;
    		var month = expDate.substring(0,2);
    		document.getElementById("lastCCExpMonth").value = month;
    		document.getElementById("exp_month").selectedIndex = month;
    	}
    	onPayMethodChange();
    }
  }
  

  function clearOutInputs()
  {
    document.forms[0].gcc_num.value = "";
    document.forms[0].gcc_pin.value = "";
    document.forms[0].auth_code.value = "";
    document.forms[0].aafes_code.value = "";
    document.forms[0].approval_id.value = "";
    document.forms[0].approval_passwd.value = "";
  }

  function clearOutNonGCInputs()
  {
    document.forms[0].auth_code.value = "";
    document.forms[0].aafes_code.value = "";
    document.forms[0].approval_id.value = "";
    document.forms[0].approval_passwd.value = "";
  }

  function onPayMethodChange()
  {
    var payMethod = document.forms[0].pay_method.value;
    if (payMethod == 'GC')
    {
      document.getElementById('card_num').disabled = true;
      document.getElementById('exp_month').disabled = true;
      document.getElementById('exp_year').disabled = true;
      document.getElementById('giftCertificateRow').style.display = "block";
      document.getElementById('invoiceRow').style.display = "none";
      document.getElementById('authDIV').style.display = "none";
      document.getElementById('noChargeDIV').style.display = "none";
      clearOutNonGCInputs();
    }
    else if (payMethod == 'NC')
    {
      document.getElementById('card_num').disabled = true;
      document.getElementById('exp_month').disabled = true;
      document.getElementById('exp_year').disabled = true;
      document.getElementById('giftCertificateRow').style.display = "none";
      document.getElementById('invoiceRow').style.display = "none";
      document.getElementById('authDIV').style.display = "none";
      document.getElementById('noChargeDIV').style.display = "block";
      clearOutInputs();
    }
    else if (payMethod == 'IN')
    {
      document.getElementById('card_num').disabled = true;
      document.getElementById('exp_month').disabled = true;
      document.getElementById('exp_year').disabled = true;
      document.getElementById('giftCertificateRow').style.display = "none";
      document.getElementById('invoiceRow').style.display = "block";
      document.getElementById('authDIV').style.display = "none";
      document.getElementById('noChargeDIV').style.display = "none";
      clearOutInputs();
    }
    else
    {
      document.getElementById('card_num').disabled = false;
      document.getElementById('exp_month').disabled = false;
      document.getElementById('exp_year').disabled = false;
      document.getElementById('giftCertificateRow').style.display = "none";
      document.getElementById('invoiceRow').style.display = "none";
      document.getElementById('authDIV').style.display = "none";
      document.getElementById('noChargeDIV').style.display = "none";
      clearOutInputs();
    }
  }

  function switchSelected(selectName, selectedId)
  {
    var element = document.getElementById(selectName);
    for (var i = 0; i< element.length; i++)
    {
      if (selectedId == element.options[i].value)
      {
        element.selectedIndex = i;
        return;
      }
    }
    element.selectIndex = 0;
  }


  function removeFromSelect(selectName, selectedId)
  {
    var element = document.getElementById(selectName);
    for (var i = 0; i< element.length; i++)
    {
      if (selectedId == element.options[i].value)
      {
        var child = element.children(i);
        element.removeChild(child);
        return;
      }
    }
    element.selectedIndex = 0;
  }

  function addToSelect(selectName, selectText, selectValue)
  {
    var element = document.getElementById(selectName);
    for (var i = 1; i< element.length; i++)
    {
			var child = element.children(i);
			element.removeChild(child);
    }
		document.getElementById(selectName).options[1] = new Option(selectText, selectValue);
    element.selectedIndex = 1;
  }


  // PAGE SPECIFIC VALIDATION //

  function validate() 
  {
    hasShownAlert = false;
    clearErrors();
    var isValid = true;
    var validBeforeCC = true;
    if (document.getElementById('billingPrompt').value == "")
    {
      var payMethod = document.getElementById('pay_method').value;
      isValid = fieldValidationSelect ('pay_method', isValid);
      if (payMethod == 'GC')
      {
        // if gctype = gd, pin must be populated
        var gd_or_gc = document.getElementById('gd_or_gc').value;
        // if gd was prepopulated and user clicked place order, gd_or_gc will not be evaluated yet, only defaulted 
        if (gd_or_gc == 'GD')
        {
        	if (isGDDirty())
        	{
        		isValid = fieldValidationText (gccnumFieldName, -1, isValid, true, 'A', " ");
        		isValid = fieldValidationText(pinFieldName, -1, isValid, true, 'A', " ");
        	}
    	}
        else
        {
            isValid = fieldValidationText (gccnumFieldName, -1, isValid, true, 'A', " ");
        }
      }
      else if (payMethod == 'NC')
      {
        isValid = fieldValidationText ('approval_id', -1, isValid, true, 'A', " ");
        isValid = fieldValidationText ('approval_passwd', -1, isValid, true, 'A', " ");
      }
      else if (payMethod == 'IN')
      {
				return isValid;
      }
      else
      {
        var lastIndex = document.getElementById("card_num").value.lastIndexOf("*");
        var isOrigCC = false;
        if ( lastIndex !=  -1)
        {
          var ccNum = document.getElementById("card_num").value.substr(lastIndex+1, 4);
          if (document.forms[0].lastCCType.value == document.forms[0].pay_method.value &&
              document.forms[0].lastCC.value == ccNum)
            {
              document.forms[0].is_orig_cc.value = "Y";
              isOrigCC = true;
            }
          else
          {
            document.forms[0].is_orig_cc.value = "N";
          }
        }
        if (!isOrigCC)
        {
          isValid = fieldValidationSelect ('exp_month', isValid);
          isValid = fieldValidationSelect ('exp_year', isValid);
          isValid = validateCC(isValid);
        }
      }
    }
    // Dynamic Fields Validation //
    if (document.getElementById("promptListRow")) 
    {
      var promptArr = document.getElementsByName("promptListRow");
      for (var i=0; i< promptArr.length; i++)
      {
        var row = promptArr[i];
        var inputArr = row.getElementsByTagName("INPUT");
        for (var j=0; j < inputArr.length; j++)
        {
          if (inputArr[j].disabled == false &&  inputArr[j].type.toUpperCase() != "HIDDEN")
          {
            inputArr[j].className="";
            if (!matchDynamicField(inputArr[j], i+1))
            {
              if (isValid == true)
              {
                inputArr[j].focus();
                isValid = false;
              }
              inputArr[j].className="errorField";
            }
          }
        }
      }
    }
    return isValid;
  }
  
  
  function isGDDirty()
  {
	  // determine if original gd# or not
	  if (document.getElementById("is_gd_dirty").value == "Y")
	  {
		  return true;
	  }
	  else
	  {
		  return false;
	  }
  }
  
  
  // PLACING THE ORDER METHODS //
  
  function doPlaceOrder()
  {
    document.getElementById("place_order_b").disabled = true;
    var isValid = validate();
    if (isValid)
		{
    	showWaitMessage("mainContent", "wait", "Processing...");
    	doRecordLock('MODIFY_ORDER','check','','paymentForm');
    }
		else if (!hasShownAlert)
		{
			showModalAlert("Please correct marked fields.");

			// Payment requires additional user interaction.  Enable the Place
			// Order button.
      document.getElementById("place_order_b").disabled = false;
		}
		else
		{
			// Payment requires additional user interaction.  Enable the Place
			// Order button.
      document.getElementById("place_order_b").disabled = false;
		}
  }

// required method, when implementing locking


function checkAndForward(forwardAction)
{
	//set gd_pin field based on gcc_pin (if we have one) every time
	if ($('#gcc_pin').val().length > 0) {
		$('#gd_pin').val($('#gcc_pin').val());
	}

	var url = 'updatePayment.do';
	var parameters = getFormNameValueQueryString( document.getElementById('paymentForm') );    
	parameters = parameters.substring(1);
	xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
	xmlHttp.onreadystatechange = evaluateStatus; 
	xmlHttp.open("POST", url , true); 
	xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlHttp.setRequestHeader("Content-length", parameters.length);
	xmlHttp.send(parameters);
}


function evaluateStatus()
{
	if (xmlHttp.readyState == 4 || xmlHttp.readyState == "complete")
	{ 
		var xml = getXMLobjXML(xmlHttp.responseText);

		// If there was an error display the error page
		var isError = xml.selectSingleNode("root/is_error").text;
		if(isError == "Y"){
			hideWaitMessage("mainContent", "wait");
			showModalAlert($(xml).filter("error_message").text());
			//enable the place order button again
			$("#place_order_b").removeAttr('disabled');
		}

		var messageAction = xml.selectSingleNode("root/status/data/message_action").text;
		var messageText = xml.selectSingleNode("root/status/data/message_text").text;
		var messageStatus = xml.selectSingleNode("root/status/data/message_status").text;

		if (messageStatus == "invalid")
		{
			hideWaitMessage("mainContent", "wait");
			if (messageAction == "decline_nc")
			{
				if (messageText != "")
					showModalAlert(messageText);
				document.getElementById("approval_id").style.display = "block";
				document.getElementById("approval_passwd").style.display = "block";
				document.getElementById("approval_id").className = "errorField";
				document.getElementById("approval_passwd").className = "errorField";
			}
			if (messageAction == "decline_credit_ms")
			{
				if (messageText != "")
					showModalAlert(messageText);
				document.getElementById("authDIV").style.display = "block";
				document.getElementById("aafesRow").style.display = "block";
				document.getElementById("auth_code").className = "errorField";
				document.getElementById("aafes_code").className = "errorField";
			}
			else if (messageAction == "decline_credit")
			{
				if (messageText != "")
					showModalAlert(messageText);
				document.getElementById("authDIV").style.display = "block";
				document.getElementById("auth_code").className = "errorField";
			}
			else if ((messageAction == "error_credit") || (messageAction == "error_credit_ms"))
			{
				if (messageText != "") {
					if (confirm(messageText)) {
						// If here, then there was a CC auth error (not to be confused with decline)
						// and user selected continue from confirmation popup.  This will set flag
						// to skip auth and re-submit form so it can continue without authorization.
						document.getElementById("cc_skip_auth").value = "Y";
						checkAndForward("");  // re-submit form
						return;
					}
				}
				document.getElementById("authDIV").style.display = "block";
				document.getElementById("auth_code").className = "errorField";
				if (messageAction == "error_credit_ms") {
					document.getElementById("aafesRow").style.display = "block";
					document.getElementById("aafes_code").className = "errorField";
				}
			}
			else if (messageAction == "decline_gcc")
			{
				if (messageText != "")
					showModalAlert(messageText);
				document.getElementById("gcc_num").className = "errorField";
			}
			else if (messageAction == "decline_icc")
			{
				if (messageText != "")
					showModalAlert(messageText);
			}

			// Payment requires additional user interaction.  Enable the Place
			// Order button.
			document.getElementById("place_order_b").disabled = false;
		}
		else if (messageStatus == "valid")
		{
			if (messageAction == "amount_owed")
			{
				hideWaitMessage("mainContent", "wait");
				if (messageText != "")
					showModalAlert(messageText);
				var amountOwed = $(xml).find("amount_owed").text();//xml.selectSingleNode("root/status/data/amount_owed").text;
				var giftCertTotal = $(xml).find("gc_amount").text();//xml.selectSingleNode("root/status/data/gc_amount").text; //// what is this for gift cards??? effective balance
				document.getElementById("gcTotal").innerHTML = giftCertTotal; /// what's this for gd?
				document.getElementById("remainingTotal").innerHTML = amountOwed;
				document.getElementById("giftCertId").innerHTML = document.getElementById("gcc_num").value;
				document.getElementById("appr_gcc_num").value = document.getElementById("gcc_num").value;
				document.getElementById("appr_gcc_amt").value = giftCertTotal;
				$('#gd_pin').val($('#'+pinFieldName).val());  //setting the value of gd_pin with the pin_field before we blank it out
				document.getElementById(gccnumFieldName).value = "";
				document.getElementById(pinFieldName).value = "";
				document.getElementById(gccnumFieldName).disabled = "true";
				document.getElementById(pinFieldName).disabled = "true";
				document.getElementById("charge_amt").value = amountOwed;
				$('#charge_amt_span').html(amountOwed);
				removeFromSelect("pay_method", "GC");
				// if original order had a CC payment, preset the payment method to that payment type
				if (document.getElementById("card_num").value != "")
				{
					var payfield = document.getElementById("pay_method");
					for(var i = 0; i < payfield.options.length; ++i)
					{
						if (payfield.options[i].value == document.forms[0].lastCCType.value)
						{
							payfield.selectedIndex = i;
							document.getElementById('card_num').disabled = false;
							document.getElementById('exp_month').disabled = false;
							document.getElementById('exp_year').disabled = false;
						}
					}    			  
				}
				else
				{
					document.getElementById("pay_method").selectedIndex = 0;
				}

				// show the amount still owed msg, hide the GD div
				document.getElementById("giftCertMessageRow").style.display = "block";
				document.getElementById('giftCertificateRow').style.display = "none";

				// If the payment type is Invoice and an additional amount is still 
				// owed the user can only apply the additional amount as an invoice
				var payType = document.getElementById("pay_type").value;
				if(payType == "I")
				{
					addToSelect("pay_method", "Invoice", "IN")
					document.getElementById("pay_method").disabled = "true";
				}

				// Payment requires additional user interaction.  Enable the Place
				// Order button.
				document.getElementById("place_order_b").disabled = false;
			}
			else
			{
				//THIS IS SUCCESS.  submit back to the order detail screen for this order
				document.getElementById('paymentForm').action = 'customerOrderSearch.do';
				$('#recipient_flag').val('y');
				$('#in_order_number').val($('#external_order_number').val());
				
				// dynamically adding this variable because inputs with name="action" totally mess up the javascript when doing form.action="someURL"
				$('#paymentForm').append('<input type="hidden" name="action" value="search" />');
				
				//this is a decision_result related method
				updateManditoryFlagForEntity($('#order_detail_id').val(), "true");

				
				$('#paymentForm').submit();
			}
		}	
	}
}

function getXMLobjXML(xmlToLoad)
{
	var xml = new ActiveXObject("Msxml2.FreeThreadedDOMDocument");
	xml.async = false;
	xml.resolveExternals = false;
	try
	{
		xml.loadXML(xmlToLoad);
		return(xml)
	}
	catch(e)
	{
		throw(xml.parseError.reason)
	}
}



// CANCEL THE ORDER METHODS //

  // makes the requst to cancel and return the user to another page
  function doCancelUpdateAction(){
    
    // Defect 1240: PCI Compliance.  Clear the cc number field.
    document.getElementById("card_num").value = "";
  
    if ( hasFormChanged() ) 
    {
    	if ( doExitPageAction(WARNING_MSG_4) ) {
    		baseDoCancelUpdateAction("paymentForm");
    	}
    }
    else {
    	baseDoCancelUpdateAction("paymentForm");
    }
  }

  function buildCCYearList(list){
    var thisYear = new Date();   
    var theList = document.getElementById(list);
    var year = thisYear.getYear().toString();
    var expDate = document.getElementById("lastCCExp").value;
    var lastYear = "20"+expDate.substring(3,5);
    document.getElementById("lastCCExpYear").value = expDate.substring(3,5);
    var selectedIndex = 0;
    
    theList.options.length = 0; //clear the list
    theList.add(new Option("","",false,false));
    for(var i = 0; i <= 10; i++){ //add 10 years to current year create new options in list   
        if (lastYear == (thisYear.getYear() + i)){
          selectedIndex = i;
        }
        theList.add(new Option((thisYear.getYear() + i), (thisYear.getYear() + i).toString().substr(2,2),false,false));
     }
    theList.selectedIndex = selectedIndex+1;
  }

  // GENERAL FIELD VALIDATION //


  function fieldValidationSelect (fieldName, hasValidFields)
  {
    var element = document.getElementById(fieldName);
  	element.className="";
  	if (element.value == "")
	  {
      if (hasValidFields == true)
	    {
	    			element.focus();
	    			hasValidFields = false;
	   	}
	    element.className="errorField";
	  }
    return hasValidFields;
  }

  function fieldValidationText (fieldName, fieldLength, hasValidFields, required, fieldType, bag)
  {
    var element = document.getElementById(fieldName);
    element.className = "";
    var newValue = stripCharsInBag(element.value, bag);
    var newValue = stripWhitespace(newValue);
    if (required == true)
    {
      if (newValue.length == 0)
      {
          if (hasValidFields == true)
          {
              element.focus();
              hasValidFields = false;
          }
          element.className="errorField";
          return hasValidFields;
      }
    }
    if (fieldType == "A")
    {
      if ((newValue.length > 0 && !isAlphanumeric(newValue)) || (fieldExceedsMaxLength(element.value, fieldLength)))
      {
          if (hasValidFields == true)
          {
              element.focus();
              hasValidFields = false;
          }
          element.className="errorField";
          return hasValidFields;
      }
    }
    else if (fieldType == "I")
    {
      if ((newValue.length > 0 && !isInteger(newValue)) || (fieldExceedsMaxLength(element.value, fieldLength)))
      {
          if (hasValidFields == true)
          {
              element.focus();
              hasValidFields = false;
          }
          element.className="errorField";
          return hasValidFields;
      }
    }
    return hasValidFields;
  }

	function fieldExceedsMaxLength(fieldValue, fieldLength)
	{
    if (fieldLength == -1)
    {
      return false;
    }
    else if (fieldValue.length > fieldLength) 
    {
      return true;	
    } 
    else
    {
      return false;
    }
	}




  function validateCC(isValid)
  {
    var ccType = document.getElementById("pay_method").value;
    var ccNumber = document.getElementById("card_num").value;
    var validCC = true;
    if (ccType && ccNumber)
    {
      if (ccType == "VI")
      {
        validCC = isVisa(ccNumber);
      }
      else if (ccType == "MC")
      {
        validCC = isMasterCard(ccNumber);
      }
      else if (ccType == "DI")
      {
        validCC = isDiscover(ccNumber);
      }
      else if (ccType == "AX")
      {
        validCC = isAmericanExpress(ccNumber);
      }
      else if (ccType == "DC")
      {
        validCC = isDinersClub(ccNumber);
      }
      else if (ccType == "JP")
      {
        validCC = (ccNumber.length == 11);
      }
      else
      {
        validCC = (ccNumber.length == 16);
      }
    }
    else
    {
      validCC = false;
    }
    if (!validCC)
    {
      if (isValid == true)
      {
        document.getElementById("card_num").focus();
        showModalAlert("Invalid credit card number.  Please correct.");
        hasShownAlert = true;
        isValid = false;
      }
      document.getElementById("card_num").className="errorField";
    }
    return isValid;
  
  }
  
  
  function checkGCType()
  {
	// mark gccnum field as dirty
	  document.getElementById("is_gd_dirty").value = "Y"; 
	  var url = 'checkGCType.do';
	  var parameters = getFormNameValueQueryString( document.getElementById('paymentForm') );    
	  parameters = parameters.substring(1);
	  xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
	  xmlHttp.onreadystatechange = checkGCTypeCallBack; 
	  xmlHttp.open("POST", url , true); 
	  xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	  xmlHttp.setRequestHeader("Content-length", parameters.length);
	  xmlHttp.send(parameters);
  }
  
  function checkGCTypeCallBack(data)
  {	  
	  if (xmlHttp.readyState == 4 || xmlHttp.readyState == "complete")
	  { 
		  var xml = getXMLobjXML(xmlHttp.responseText);

		  var pinField = $('#'+pinFieldName)[0];
		  var gccField = $('#'+gccnumFieldName)[0];
		  //var xml = getXMLobjXML(xmlHttp.responseText);
		  var gdorgc = xml.selectSingleNode("root/gd_or_gc").text;

		  if (gdorgc == 'GD')
		  {
			  $('#gd_or_gc').val("GD")
			  pinField.disabled = false;
			  pinField.focus();
		  }
		  else
		  {
			  $('#gd_or_gc').val("GC");
			  fieldBlurByElement(pinField);
			  pinField.disabled = true;	    	
		  }
		  //pinField.value = "";
		  if (gccField != undefined) {
			  fieldBlurByElement(gccField);
		  }
	  }
  }
  
  function clearPin(e)
  {
	  var keyCd = e.keyCode;
	  var pinField = $('#'+pinFieldName)[0];
	  var gccField = $('#'+gccnumFieldName)[0];
          //alert("Key Code : "+keyCd);
	  if (window.event && keyCd != 9 && keyCd != 13 && keyCd != 27){
		  pinField.value = "";
		  gccField.focus();
		  return true;
	  }else if(window.event && keyCd == 27){
		  gccField.focus();
		  return true;

	  }else if(window.event && keyCd == 9){
		  pinField.focus();
		  return false;
	  }
	  gccField.focus();
	  return true;
  }

  function clearErrors()
  {
    var arr1 = document.getElementsByTagName("INPUT");
    for (var i = 0; i < arr1.length; i++)
    {
      if (arr1[i].className )
      {
        arr1[i].className = '';
      }
    }
    var arr2 = document.getElementsByTagName("SELECT");
    for (var j = 0; j < arr2.length; j++)
    {
      if (arr1[j].className )
      {
        arr1[j].className = '';
      }
    }
  }

	/*
	 Called if there are any errors
	*/
	function doCallErrorPage(){
		 var url = "Error.do?";
		 document.getElementById('paymentForm').action = url;
		 document.getElementById('paymentForm').submit();
	}
  
 /*
 *  Disables the various navigation keyboard commands.
 */
function pBackKeyHandler() {
   // backspace
   if (window.event && window.event.keyCode == 8){
      var formElement = false;

      for(i = 0; i < document.forms['paymentForm'].elements.length; i++){
         if(document.forms['paymentForm'].elements[i].name == document.activeElement.name){
            formElement = true;
            break;
         }
      }

      if(formElement == false || (document.activeElement.type != "text" && document.activeElement.type != "textarea" && document.activeElement.type != "password")){
         window.event.cancelBubble = true;
         window.event.returnValue = false;
         return false;
      }
   }

   // F5 and F11
   if (window.event.keyCode == 122 || window.event.keyCode == 116){
      window.event.keyCode = 0;
      event.returnValue = false;
      return false;
   }

   // Alt + Left Arrow
   if(window.event.altLeft){
      window.event.returnValue = false;
      return false;
   }

   // Alt + Right Arrow
   if(window.event.altKey){
      window.event.returnValue = false;
      return false;
   }
}

