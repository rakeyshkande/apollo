
var inputFileName;
var ppCnt = 0;
var ppArray = [];

//***************************************************************************************
//  init()
//***************************************************************************************
function init()
{
  setNavigationHandlers();
  doClearFields(); 
  document.getElementById('inputExcelFile').focus();
  document.getElementById("sDeleteOtherQueues").selectedIndex = -1;  
  document.getElementById("sDeleteOtherQueues").value = "";
  document.getElementById("sAnspOverride").disabled  = true;
}


function validateBatchDescription(){
  
 if(document.theForm.batch_description.value == null || document.theForm.batch_description.value == ""){
  return false;
 }
 return true; 

}

function isAlphanumeric(thisValue){
	var result = false;
	if(getActualOrder(thisValue).length>0){
		result = /^[A-Za-z0-9\r|\n]+$/.test(thisValue);
		
	}
	return result;
}

function isNumeric(thisValue){
	var result = false;
	if(getActualOrder(thisValue).length>0){
		result = /^[0-9\r|\n]+$/.test(thisValue);
		
	}
	return result;
}

//***************************************************************************************
//  FormValidator()
//***************************************************************************************
function FormValidator() 
{
  this.focusOnField = "";
  this.errorMsg = "";
  this.minTabIndex = 99999;

  initInputFieldStyle();

  var msg = "";

  //***INPUT FILE (.XLS)
  if (document.theForm.inputExcelFile.value == "" && getRadioValue("inputType") == "FILE") 
  {
    msg = msg + "Input File is required.\n";
    if (document.theForm.inputExcelFile.tabIndex < this.minTabIndex) 
    {
      this.minTabIndex = document.theForm.inputExcelFile.tabIndex;
      this.focusOnField = document.theForm.inputExcelFile.name;
    }
    document.theForm.inputExcelFile.className="ErrorField";
  }


  //***INPUT QUEUE MESSAGE IDS
  if (getRadioValue("inputType") == "INPUTBOX" && !isNumeric(document.theForm.inputTextArea.value))
  {
    msg = msg + "Queue Message ID(s) required (Only Numeric values are allowed).\n";
    if (document.theForm.inputExcelFile.tabIndex < this.minTabIndex) 
    {
      this.minTabIndex = document.theForm.inputTextArea.tabIndex;
      this.focusOnField = document.theForm.inputTextArea.name;
    }
    document.theForm.inputTextArea.className="ErrorField";
  }


  //***INPUT EXTERNAL ORDER NUMBERS
  if (getRadioValue("inputType") == "EXTERNAL_ORDER_NUMBERS_BOX"  && !isAlphanumeric(document.theForm.inputExternalOrderNumbersTextArea.value))
  {
    msg = msg + "External Order Number(s) required (Only Alphanumeric values are allowed).\n";
    if (document.theForm.inputExcelFile.tabIndex < this.minTabIndex) 
    {
      this.minTabIndex = document.theForm.inputExternalOrderNumbersTextArea.tabIndex;
      this.focusOnField = document.theForm.inputExternalOrderNumbersTextArea.name;
    }
    document.theForm.inputExternalOrderNumbersTextArea.className="ErrorField";
  }


  //***DELETE ASSOCIATED QUEUE MESSAGES
  if (document.theForm.cbDeleteOtherQueues.checked && deleteOtherQueuesSelected() == false)
  {
    msg = msg + "Queue Type(s) must be selected.\n";
    if (document.theForm.inputExcelFile.tabIndex < this.minTabIndex) 
    {
      this.minTabIndex = document.theForm.inputExternalOrderNumbersTextArea.tabIndex;
      this.focusOnField = document.theForm.inputExternalOrderNumbersTextArea.name;
    }
    document.theForm.sDeleteOtherQueues.className="ErrorField";
  }

  //validate batch description
  if(!validateBatchDescription()){
    msg = msg + "Please enter Batch Description.\n";
    if (document.theForm.batch_description.tabIndex < this.minTabIndex) 
    {
      this.minTabIndex = document.theForm.batch_description.tabIndex;
      this.focusOnField = document.theForm.batch_description.name;
    }
    document.theForm.batch_description.className="ErrorField";
  }

  //***SEND MESSAGE 
  if (document.theForm.cbSendMessage.checked) 
  {
    var newMessage = document.getElementById("newMessage");
    var newMessageVal = "";

    if(newMessage != null) 
      newMessageVal = newMessage.value;  

    if (document.theForm.sSendMessage.value == "")
    {
      msg = msg + "You must select the message type that you want to send.\n";
      if (document.theForm.sSendMessage.tabIndex < this.minTabIndex) 
      {
        this.minTabIndex = document.theForm.sSendMessage.tabIndex;
        this.focusOnField = document.theForm.sSendMessage.name;
      }
      document.theForm.sSendMessage.className="ErrorField";
    }
    

    if (document.theForm.sSendMessage.value == "ANS" && document.getElementById("sAnspOverride").checked == false )
    {

       if (document.theForm.sAnspAmount.value  != "" && (!(document.theForm.sAnspAmount.value > 0.00)))
       {
        this.focusOnField = document.theForm.sAnspAmount.name ;
        msg = msg + "Please specify ANSP amount.\n";
        document.theForm.sAnspAmount.className="ErrorField";
       }
       else if(!formatDollarAmount(document.theForm.sAnspAmount))
       {
        this.focusOnField = document.theForm.sAnspAmount.name ;
        document.theForm.sAnspAmount.className="ErrorField";
        msg = msg + "Please specify ANSP amount.\n";
       }
       else if (!validateDollarAmountToBeLessThanAnspAmount(document.theForm.sAnspAmount, maxAnspAmount))
       {
        this.focusOnField = document.theForm.sAnspAmount.name ;
        document.theForm.sAnspAmount.className="ErrorField";
        msg = msg + "Max ANSP amount not allowed is "+ maxAnspAmount ;
       }
   

    }
 
    if (document.theForm.sSendMessage.value == "CAN" && document.theForm.sCancelReasonCode.value == "")
    {
      msg = msg + "You must select the cancel reason code.\n";
      if (document.theForm.sCancelReasonCode.tabIndex < this.minTabIndex) 
      {
        this.minTabIndex = document.theForm.sCancelReasonCode.tabIndex;
        this.focusOnField = document.theForm.sCancelReasonCode.name;
      }
      document.theForm.sCancelReasonCode.className="ErrorField";
    }


    if(Trim(newMessageVal) == "")
    {
      msg = msg + "Please enter the text for Send Message.\n";
      if (document.theForm.newMessage.tabIndex < this.minTabIndex) 
      {
        this.minTabIndex = document.theForm.newMessage.tabIndex;
        this.focusOnField = document.theForm.newMessage.name;
      }
      document.theForm.newMessage.className="ErrorField";
    }

    
  }


  //***ADD ORDER COMMENT
  if (document.theForm.isCommentAdded.checked) 
  {
    var commentText = document.getElementById('comment_text');
    var commentTextVal = "";

    if(commentText != null) 
      commentTextVal = commentText.value;  

    if(Trim(commentTextVal) == "")
    {
      msg = msg + "Please enter Order Comments.\n";
      if (document.theForm.comment_text.tabIndex < this.minTabIndex) 
      {
        this.minTabIndex = document.theForm.comment_text.tabIndex;
        this.focusOnField = document.theForm.comment_text.name;
      }
      document.theForm.comment_text.className="ErrorField";
    }
  } 


  //***SEND CUSTOMER EMAIL
  if (document.theForm.isEmailAdded.checked) 
  {
    // START Validate subject and message body exists
    var subject = document.getElementById('newSubject');
    var newBody = document.getElementById('newBody');

    // Initialize as empty string to avoid any exceptions when testing the 
    // object
    var subjectVal = "";
    var newBodyVal = "";

    if(newBody != null) 
      newBodyVal = newBody.value;  

    if(subject != null) 
      subjectVal = subject.value;  

    if(Trim(subjectVal) == "")
    {
      msg = msg + "Please enter a Customer Email subject.\n";
      if (document.theForm.newSubject.tabIndex < this.minTabIndex) 
      {
        this.minTabIndex = document.theForm.newSubject.tabIndex;
        this.focusOnField = document.theForm.newSubject.name;
      }
      document.theForm.newSubject.className="ErrorField";
    }

    if(Trim(newBodyVal) == "")
    {
      msg = msg + "Please enter a Customer Email body.\n";
      if (document.theForm.newBody.tabIndex < this.minTabIndex) 
      {
        this.minTabIndex = document.theForm.newBody.tabIndex;
        this.focusOnField = document.theForm.newBody.name;
      }
      document.theForm.newBody.className="ErrorField";
    }

  }  // END Validate subject and message body exists


  //***REFUND ORDER 
  if (document.theForm.isOrderRefunded.checked) 
  {
    if (document.theForm.refundDisposition.value == "") 
    {
      msg = msg + "Refund Disposition is required.\n";
      if (document.theForm.refundDisposition.tabIndex < this.minTabIndex) 
      {
        this.minTabIndex = document.theForm.refundDisposition.tabIndex;
        this.focusOnField = document.theForm.refundDisposition.name;
      }
      document.theForm.refundDisposition.className="ErrorField";
    }

    if (document.theForm.responsibleParty.value == "") 
    {
      msg = msg + "Responsible Party is required.\n";
      if (document.theForm.responsibleParty.tabIndex < this.minTabIndex) 
      {
        this.minTabIndex = document.theForm.responsibleParty.tabIndex;
        this.focusOnField = document.theForm.responsibleParty.name;
      }
      document.theForm.responsibleParty.className="ErrorField";
    }
  }

  this.errorMsg = msg;

}
//***************************************************************************************
//use formatDollarAmount from Utils  to validate and add decimals and then validate it bo less than MaxAnspAMount()
//***************************************************************************************

function formatDollarAmountWithMaxLimit(textbox, maxANSPAmount)
{
  if(!formatDollarAmount(textbox))
  {

    textbox.className = "ErrorField";
   
    alert("Please specify ANSP amount.");
    textbox.focus();
    
  }
  else if (!validateDollarAmountToBeLessThanAnspAmount(textbox, maxANSPAmount))
  {
    textbox.className = "ErrorField";
   
    alert("Max ANSP amount allowed is "+ maxANSPAmount.toFixed(2));
    textbox.focus();
    
  }
  else
  {
      textbox.className = "" ;
  }
}

function validateDollarAmountToBeLessThanAnspAmount(textbox, maxANSPAmount)
{

   if(textbox.value > maxANSPAmount )
   {
     
        return false;
    }
    else 
    {
      return true
    }
}

function useASKPPrice(checkbox, texbox)
{

   if(checkbox.checked == true )
   {
     texbox.value = "" ;
     texbox.disabled = true;
   }
    else 
    {
     texbox.disabled = false;
    }
}
//***************************************************************************************
//  getRadioValue()
//***************************************************************************************
function getRadioValue(buttonName)
{
  var list = document.getElementsByName(buttonName);
  for (var i=0; i < list.length; i++)
  {
    if (list[i].checked)
    {
      return list[i].value;
    }
  }

}


//***************************************************************************************
//  doQueueMessageDeleteAction()
//***************************************************************************************
function doQueueMessageDeleteAction()
{
	
  var form=document.theForm;

  var formValidator = new FormValidator();
  if (formValidator.errorMsg != "") 
  {
    alert(formValidator.errorMsg);
    document.theForm[formValidator.focusOnField].focus();
  } 
  else 
  {
    //remove special characters from batch description field
    var bag = ",/.<>?;:\|[]{}~!@#$%^&*()-_=+`" + "\\\'";
    document.getElementById("batch_description").value = stripCharsInBag( document.getElementById("batch_description").value, bag);
    var cont = true;
    var modalArguments = new Object();
    var anspAmountToBeSubmitted ;
    /*if (document.getElementById("cbSendMessage").checked == true && document.theForm.sSendMessage.value=='ANS')
    {
      if(document.getElementById("sAnspOverride").checked == true)
      {
        modalArguments.modalText = 'You are about to submit an ANSP to give all florists the amount they requested.  Click Continue to submit, or click Cancel to go back.';
        cont = window.showModalDialog("continue.html", modalArguments, 'dialogWidth:300px; dialogHeight:200px; center:yes; status=no; help=no; resizable:no; scroll:yes');
      }
      else
      {
        if (document.theForm.sAnspAmount.value  != "")
        {
          anspAmountToBeSubmitted = document.getElementById("sAnspAmount").value;
          modalArguments.modalText = 'You are about to submit an ANSP for $' + anspAmountToBeSubmitted + ' .  Click Continue to submit, or click Cancel to go back.';    
          cont = window.showModalDialog("continue.html", modalArguments, 'dialogWidth:300px; dialogHeight:200px; center:yes; status=no; help=no; resizable:no; scroll:yes');
        }
      }
    }*/


    if(cont)
    {
    
      form.action_type.value = "delete";
      document.getElementById("cbDeleteOtherQueues").disabled = false; 
      if (document.getElementById("cbDeleteOtherQueues").checked)
        document.getElementById("delete_other_queues_selected").value = getDeleteOtherQueuesSelected(); 



      // Note that the URL is composed for SecurityFilter multipart/form-data submission.
      form.action = "queueMessageDelete.do?&context="+form.context.value+"&securitytoken="+form.securitytoken.value;

      form.message_id.value = form.stockEmail.options[form.stockEmail.selectedIndex].value;
      form.message_title.value = form.stockEmail.options[form.stockEmail.selectedIndex].text;
      form.message_subject.value = form.newSubject.value;     
      form.message_content.value = form.newBody.value;
      //showWaitMessage("content", "wait");
      
      showDialogMessage(form);
      
      
      
    }
  }

}


function getActualOrder(givenInput){
	var res = givenInput.split("\n");
    var finalOrders = [];
    for(i=0;i<res.length;i++){
      
     if(res[i].length>1){
    	 finalOrders.push(res[i])
     }
    }
    return finalOrders;
}




function showDialogMessage(form){
	
	var displayMessage = '';
	var warning = true;
	var radioButtonClicked = getRadioValue("inputType"); 
	if(document.getElementById('batch_description').value!=''){
		
		displayMessage+= 'Batch description : '+document.getElementById('batch_description').value+"\n\n";
	}
	
	var ppCheckedCnt = 0;
	var ppCheckedArray = [];
	for (p=0; p<ppArray.length; p++) {
		if (document.getElementById('cb'+ppArray[p]).checked) {
			ppCheckedArray[ppCheckedCnt] = ppArray[p];
			ppCheckedCnt = ppCheckedCnt + 1;
		}
	}
	var tempMsg1 = '';
	var tempMsg2 = '';
	if (ppCheckedCnt > 0) {
		tempMsg1 = ' and all associated messages in the ';
		tempMsg2 = ' which includes ';
		for (q=0; q<ppCheckedArray.length; q++) {
			if (q == 0) {
				tempMsg1 += ppCheckedArray[q];
				tempMsg2 += ppCheckedArray[q];
			} else if (q == (ppCheckedArray.length - 1)) {
				tempMsg1 += ' and ' + ppCheckedArray[q];
				tempMsg2 += ' and ' + ppCheckedArray[q];
			} else {
				tempMsg1 += ', ' + ppCheckedArray[q];
				tempMsg2 += ', ' + ppCheckedArray[q];
			}
		}
		tempMsg1 += ' queue.\n\n';
		tempMsg2 += '.\n\n';
	} else {
		tempMsg1 = '.\n\n';
		tempMsg2 = '.\n\n';
	}
		
	if(radioButtonClicked=='EXTERNAL_ORDER_NUMBERS_BOX' && document.getElementById('cbDeleteOtherQueues').checked){
			var numberOfExternalOrders = document.getElementById('inputExternalOrderNumbersTextArea').value;
			var actualOrders = getActualOrder(numberOfExternalOrders);
			var externalOrderCount = actualOrders.length;
			var deleteQueues = getDeleteOtherQueuesSelected();
						
			displayMessage += 'You are deleting '+deleteQueues+' messages from '+externalOrderCount+' orders' + tempMsg1;
			
	}
	else{
		var deleteQueues =getDeleteOtherQueuesSelected();
		var numberOfOrders = document.getElementById('inputTextArea').value;
		var actualOrders = getActualOrder(numberOfOrders);
		if(radioButtonClicked=="FILE"){
			
			var orderCount ="";
		}
		else{
			
			var orderCount = actualOrders.length;
		}
		
		if(document.getElementById('cbDeleteOtherQueues').checked){
			displayMessage+='You are deleting '+orderCount+' queue messages, and all associated messages in the '+deleteQueues+' queue(s)' + tempMsg2;
		}
		else{
			displayMessage+='You are deleting '+orderCount+' queue messages' + tempMsg1;
		}
		
	}
	
	
	
	
	if(document.getElementById('cbSendMessage').checked){
		warning = false;
		var newMessage = document.getElementById('newMessage').value;
		var sSendMessage = document.getElementById('sSendMessage').value;
		if(sSendMessage == 'ANS'){
			var sAnspAmount =  document.getElementById("sAnspAmount").value;
			
			if(sAnspAmount.length>0){
				displayMessage+='You are sending a ANS message and an additional $'+sAnspAmount+' to the florist with the following text or reason:'+newMessage+'.\n\n ';
			}
			else if(document.getElementById('sAnspOverride').checked){
				displayMessage+='You are sending a ANS message and an additional amount as requested to the florist with the following text or reason: '+newMessage+'.\n\n';
			}
			else{
				displayMessage+='You are sending a ANS message with the following text or reason: '+newMessage+'.\n\n';
			}
		}
		else if(sSendMessage == 'CAN'){
			var sCancelReasonCode = document.getElementById('sCancelReasonCode').value;
			var reasonCode;
			
			switch(sCancelReasonCode){
				case 'CHG':
					reasonCode = 'Customer Changed Order/Recipient Information';
					break;
				case 'QUA':
					reasonCode = 'Poor Quality/Broken Vase';
					break;	
				case 'SUB':
					reasonCode = 'Substitution';
					break;
				case 'CCN':
					reasonCode = 'Customer Cancelled';
					break;
				case 'FRD':
					reasonCode = 'Fraud';
					break;
				case 'RCP':
					reasonCode = 'Hospital Order/Recipient Discharged';
					break;
				case 'SHP':
					reasonCode = 'Carrier Delivery/Shipping Issue';
					break;
				case 'VCN':
					reasonCode = 'Vendor Cancelled';
					break;
				case 'SCN':
					reasonCode = 'Non-Scan Order';
					break;	
			}
			displayMessage+='You are sending a CAN message to the florist with '+sCancelReasonCode+'-'+reasonCode+' reason code and the following text or reason: '+newMessage+'.\n\n';
		}
	}
	
	if(document.getElementById('isOrderReprocessed').checked){
		warning = false;
		displayMessage+= 'You are attempting to reprocess '+orderCount+' order(s).\n\n';
	}
	
	if(document.getElementById('isCommentAdded').checked){
		warning = false;
		var comment_text = document.getElementById('comment_text').value;
		displayMessage+= 'The following order comment will be appended to the order associated with the deleted message: '+comment_text+'\n\n';
	}
	
	if(document.getElementById('isEmailAdded').checked){
		 warning = false;
		var newSubject = document.getElementById('newSubject').value;
		displayMessage+= 'A email will be sent to the customer with the subject: '+newSubject+'\n\n';
		
		
	}
	
	 if (document.getElementById("isOrderRefunded").checked){
		 warning = false;
		 var refundDisposition = document.getElementById('refundDisposition').value;
		 var responsibleParty = document.getElementById('responsibleParty').value;
		 displayMessage+= 'A refund will be applied to order with a '+refundDisposition+' refund disposition and '+responsibleParty+' as the responsible party.\n\n';
	 }
	
	 if(warning){
		 displayMessage+= 'WARNING: You are performing no other action with the order associated to message.\n\n';
	 }
	 
	displayMessage+= 'Are you sure you want to continue?';
	var finalSubmit= confirm(displayMessage);
	
	if(finalSubmit == true){
		//alert("Confirmed");
		showWaitMessage("content", "wait");
		form.submit();
	}
	else{
		//alert("declined");
	}
}





//***************************************************************************************
//  initInputFieldStyle()
//***************************************************************************************
function initInputFieldStyle() 
{
  document.theForm.inputExcelFile.className="input";
  document.theForm.inputTextArea.className="input";
  document.theForm.inputExternalOrderNumbersTextArea.className="input";
  document.theForm.sDeleteOtherQueues.className="input";
  document.theForm.sSendMessage.className="input";
  document.theForm.sCancelReasonCode.className="input";
  document.theForm.newMessage.className="input";
  document.theForm.comment_text.className="input";
  document.theForm.newSubject.className="input";
  document.theForm.newBody.className="input";
  document.theForm.refundDisposition.className="input";
  document.theForm.responsibleParty.className="input";
  document.theForm.batch_description.className="input";
  document.getElementById('inputExcelFile').focus();

}


//***************************************************************************************
//  doClearFields()
//***************************************************************************************
function doClearFields()
{
  document.forms[0].reset();
  initInputFieldStyle();

  for(var i = 0; i < document.forms[0].elements.length; i++)
  {
    if( (document.forms[0].elements[i].type == 'text') ||
        (document.forms[0].elements[i].type == 'file') ||
        (document.forms[0].elements[i].type == 'textarea')
      )
    {
      if (containsErrors &&  document.forms[0].elements[i].name == "errorMessageTextArea")
      {
        //bypass - we do not want to clear the error text area
      }
      else
      {
        document.forms[0].elements[i].value = "";
        document.forms[0].elements[i].disabled = true;  
      }
    }
    else if ( (document.forms[0].elements[i].type == 'select-one') || (document.forms[0].elements[i].type == 'select-multiple') )
    {
      document.forms[0].elements[i].selectedIndex = 0;  
      document.forms[0].elements[i].value = "";
      document.forms[0].elements[i].disabled = true;  
    }
    else if(document.forms[0].elements[i].type == 'checkbox')
    {
      document.forms[0].elements[i].checked = false;  
      document.forms[0].elements[i].disabled = false;  
    }
    else if(document.forms[0].elements[i].type == 'radio')
      document.forms[0].elements[i].checked = false;
  }

  document.getElementById("inputType").checked = true; 
  document.getElementById("inputExcelFile").disabled = false; 
  document.getElementById("inputTextArea").disabled = false; 
  document.getElementById("inputExternalOrderNumbersTextArea").disabled = false;
  document.getElementById("batch_description").disabled=false;

    

}


//***************************************************************************************
//  doBackAction()
//***************************************************************************************
function doBackAction() 
{
  document.theForm.action = "/secadmin/security/Main.do?adminAction=customerService";
  document.theForm.target = "_top";
  document.theForm.submit();

}





//***************************************************************************************
//  doRefreshEmailContentAction() - functionality via AJAX
//***************************************************************************************
function doRefreshEmailContentAction() 
{
  form = document.theForm;
  form.action_type.value = "load_content";
  form.message_id.value = form.stockEmail.options[form.stockEmail.selectedIndex].value;
  form.message_title.value = form.stockEmail.options[form.stockEmail.selectedIndex].text;

  //  Send a POST request via AJAX to recalculate the order
  var url = 'queueMessageDelete.do';
  var parameters = getFormNameValueQueryString(form);   
  parameters = parameters.substring(1);
  xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
  xmlHttp.onreadystatechange = evalRefreshEmailContentAction; 
  xmlHttp.open("POST", url , true); 
  xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xmlHttp.setRequestHeader("Content-length", parameters.length);
  xmlHttp.send(parameters);

}


//***************************************************************************************
//  evalRefreshEmailContentAction() - Evaluates response from functionality via AJAX
//***************************************************************************************
function evalRefreshEmailContentAction() 
{
  if (xmlHttp.readyState == 4 || xmlHttp.readyState == "complete")
  {
    // Obtain response data
    var xml = getXMLobjXML(xmlHttp.responseText);

    var emailSubject = xml.selectSingleNode("PointOfContactVO/emailSubject").text;
    var emailBody = xml.selectSingleNode("PointOfContactVO/body").text;

    // Display refreshed content
    document.getElementById('newSubject').value = emailSubject;
    document.getElementById('newBody').value = emailBody;
  }

}


//***************************************************************************************
//  getXMLobjXML()
//***************************************************************************************
function getXMLobjXML(xmlToLoad)
{
  var xml = new ActiveXObject("Msxml2.FreeThreadedDOMDocument");
  xml.async = false;
  xml.resolveExternals = false;
  try
  {
    xml.loadXML(xmlToLoad);
    return(xml)
  }
  catch(e)
  {
    throw(xml.parseError.reason)
  }

}


//***************************************************************************************
//  doEmailCheckboxChangeAction()
//***************************************************************************************
function doEmailCheckboxChangeAction() 
{
  form = document.theForm;
  if (form.isEmailAdded.checked) 
  {
    // Enable the fields.
    form.newSubject.disabled = false;
    form.newBody.disabled = false;
    form.stockEmail.disabled = false;
    form.stockEmail.focus();
  } 
  else 
  {
    // Disable the fields.
    form.newSubject.disabled = true;
    form.newBody.disabled = true;
    form.stockEmail.disabled = true;

    // Clear the fields.
    form.newSubject.value = "";
    form.newBody.value = "";
    form.stockEmail.selectedIndex = 0;
  }

}


//***************************************************************************************
//  doCommentCheckboxChangeAction()
//***************************************************************************************
function doCommentCheckboxChangeAction() 
{
  form = document.theForm;
  if (form.isCommentAdded.checked) 
  {
    // Enable the fields.
    form.comment_text.disabled = false;
    form.comment_text.focus();
  } 
  else 
  {
    // Clear the fields.
    form.comment_text.value = "";

    // Disable the fields.
    form.comment_text.disabled = true;
  }

}


//***************************************************************************************************
// deleteOtherQueuesSelected()
//***************************************************************************************************/
function deleteOtherQueuesSelected()
{
  var qt = document.getElementById("sDeleteOtherQueues");
  var selectedFlag = true; 
  var qtIndexSelected = qt.selectedIndex; 

  //if the user has not selected anything, the selectedIndex will be -1.  If the user has selected 1 item, the selectedIndex will be 
  //the index of the selected item. If the user has selected >1 items, selectedIndex will be 0. 
  //Thus, if the value is < 0, user has not selected anything.
  if (qtIndexSelected < 0)
  {
    selectedFlag = false; 
  }

  return selectedFlag; 

}


//***************************************************************************************************
// checkDeleteOtherQueues()
//***************************************************************************************************/
function checkDeleteOtherQueues()
{
  if (document.getElementById("cbDeleteOtherQueues").checked)
  {
    document.getElementById("sDeleteOtherQueues").disabled = false; 
    document.getElementById("sDeleteOtherQueues").selectedIndex = -1;  
    document.getElementById("sDeleteOtherQueues").value = "";
  }
  else
  {
    document.getElementById("sDeleteOtherQueues").disabled = true; 
    document.getElementById("sDeleteOtherQueues").selectedIndex = -1;  
    document.getElementById("sDeleteOtherQueues").value = "";
  }

}


//***************************************************************************************************
// getDeleteOtherQueuesSelected()
//***************************************************************************************************/
function getDeleteOtherQueuesSelected()
{
  var qt = document.getElementById("sDeleteOtherQueues");
  var deleteOtherQueuesSelected = ""; 
  
  for (var i = 0; i < qt.length; i++)
  {
    if ( qt[i].selected ) 
    {
      deleteOtherQueuesSelected += qt[i].value + ","; 
    }
  }

  deleteOtherQueuesSelected = deleteOtherQueuesSelected.substring(0,deleteOtherQueuesSelected.length - 1)
  return deleteOtherQueuesSelected; 
  
}



//***************************************************************************************
//  doRefund()
//***************************************************************************************
function doRefund() 
{
  checkCbRefund(true); 

  if (document.getElementById("isOrderRefunded").checked)
  {
    //***RESUBMIT ORDER FOR PROCESSING
    document.getElementById("isOrderReprocessed").checked = false; 
    document.getElementById("isOrderReprocessed").disabled = true; 
  }
  else
  {
    //***RESUBMIT ORDER FOR PROCESSING
    document.getElementById("isOrderReprocessed").checked = false; 
    document.getElementById("isOrderReprocessed").disabled = false; 
  }

}

//***************************************************************************************
//  checkCbRefund()
//***************************************************************************************
function checkCbRefund(giveFocus) 
{
  form = document.theForm;
  if (form.isOrderRefunded.checked) 
  {
    // Enable the fields.
    form.refundDisposition.disabled = false;
    form.responsibleParty.disabled = false;
    if (giveFocus)
      form.refundDisposition.focus();
  } 
  else 
  {
    // Disable the fields.
    form.refundDisposition.disabled = true;
    form.refundDisposition.selectedIndex = 0;
    form.responsibleParty.disabled = true;
    form.responsibleParty.selectedIndex = 0;
  }

}


//***************************************************************************************
//  doPrefPartnerCB
//***************************************************************************************
function doPrefPartnerCB(obj)
{
      form = document.theForm;
  if (obj.checked) 
  {
    // Enable the fields.
    obj.disabled = false;
  } 
}

//***************************************************************************************************
// doSendMessage()
//***************************************************************************************************/
function doSendMessage()
{
  checkCbSendMessage(true); 
  doMasterCheck(false); 
}


//***************************************************************************************************
// checkCbSendMessage()
//***************************************************************************************************/
function checkCbSendMessage(giveFocus)
{
  if (document.getElementById("cbSendMessage").checked)
  {
    // Enable the fields.
    document.getElementById("sSendMessage").disabled = false; 
    if (giveFocus)
      document.getElementById("sSendMessage").focus();
  }
  else
  {
    // Disable the fields.
    document.getElementById("sSendMessage").disabled = true; 
    document.getElementById("newMessage").disabled = true; 

    // Clear the fields.
    document.getElementById("sSendMessage").selectedIndex = 0;
    document.getElementById("newMessage").value = ""; 

    document.getElementById("sAnspAmount").disabled = true; 
    document.getElementById("sAnspOverride").checked = false; 
    document.getElementById("sAnspOverride").disabled = true; 

 
    document.getElementById("sCancelReasonCode").selectedIndex = 0;
    document.getElementById("sCancelReasonCode").disabled = true; 
  }

}


//***************************************************************************************
//  doMasterCheck
//***************************************************************************************
function doMasterCheck(giveFocus) 
{
  var radioButtonClicked = getRadioValue("inputType"); 
  var sSendMessageSelectedIndex = document.getElementById("sSendMessage").selectedIndex; 
  var sSendMessageSelected = document.getElementById("sSendMessage").options[sSendMessageSelectedIndex].value; 

  if (radioButtonClicked == 'EXTERNAL_ORDER_NUMBERS_BOX')
  {
    //***DELETE ASSOCIATED QUEUE MESSAGES
    document.getElementById("cbDeleteOtherQueues").checked = true; 
    document.getElementById("cbDeleteOtherQueues").disabled = true; 
    document.getElementById("sDeleteOtherQueues").disabled = false; 

  
    //***SEND MESSAGE
    document.getElementById("cbSendMessage").disabled = true; 
    document.getElementById("cbSendMessage").checked = false; 

    document.getElementById("sSendMessage").disabled = true; 
    document.getElementById("sSendMessage").selectedIndex = 0;

    document.getElementById("sAnspAmount").disabled = true; 
    document.getElementById("sAnspOverride").disabled = true; 
    
    document.getElementById("sCancelReasonCode").disabled = true; 
    document.getElementById("sCancelReasonCode").selectedIndex = 0;

    document.getElementById("newMessage").disabled = true; 
    document.getElementById("newMessage").value = ""; 

    //***RESUBMIT ORDER FOR PROCESSING
    document.getElementById("isOrderReprocessed").checked = false; 
    document.getElementById("isOrderReprocessed").disabled = true; 

    document.getElementById("isOrderRefunded").disabled = false; 

  }
  else
  {
    document.getElementById("cbDeleteOtherQueues").disabled = false; 
    document.getElementById("cbSendMessage").disabled = false; 
    document.getElementById("isOrderReprocessed").disabled = false; 
    document.getElementById("isCommentAdded").disabled = false; 
    document.getElementById("isEmailAdded").disabled = false; 
    document.getElementById("isOrderRefunded").disabled = false; 

    checkCbSendMessage(false); 
    checkCbRefund(false); 
    
  
    if (sSendMessageSelected == 'ANS')
    {

      document.getElementById("isOrderReprocessed").checked = false; 
      document.getElementById("isOrderReprocessed").disabled = true; 
      if (document.getElementById("sAnspOverride").checked == false )
      {
         document.getElementById("sAnspAmount").disabled = false; 
         document.getElementById("sAnspOverride").disabled = false; 
       
      }
     
      
      if (giveFocus)
      document.getElementById("sAnspAmount").focus();
      document.getElementById("sCancelReasonCode").selectedIndex = 0;
      document.getElementById("sCancelReasonCode").disabled = true; 
      document.getElementById("newMessage").disabled = false; 

      document.getElementById("isOrderRefunded").checked = false; 
      document.getElementById("isOrderRefunded").disabled = true; 
      document.getElementById("refundDisposition").disabled = true;
      document.getElementById("refundDisposition").selectedIndex = 0;
      document.getElementById("responsibleParty").disabled = true;
      document.getElementById("responsibleParty").selectedIndex = 0;

    }
    else if (sSendMessageSelected == 'CAN')
    {
      document.getElementById("isOrderReprocessed").checked = true; 
      document.getElementById("isOrderReprocessed").disabled = false; 
    document.getElementById("sAnspAmount").disabled = true; 
    document.getElementById("sAnspOverride").checked = false; 
    document.getElementById("sAnspOverride").disabled = true; 
      document.getElementById("sCancelReasonCode").disabled = false; 
      if (giveFocus)
        document.getElementById("sCancelReasonCode").focus();
      document.getElementById("newMessage").disabled = false; 
      document.getElementById("newMessage").value = "PLEASE CANCEL ORDER"; 

      document.getElementById("isEmailAdded").disabled = false; 
      document.getElementById("isOrderRefunded").checked = false; 
      document.getElementById("isOrderRefunded").disabled = true; 
      document.getElementById("refundDisposition").disabled = true;
      document.getElementById("refundDisposition").selectedIndex = 0;
      document.getElementById("responsibleParty").disabled = true;
      document.getElementById("responsibleParty").selectedIndex = 0;

    }
    else    
    {
      document.getElementById("isOrderRefunded").disabled = false; 
      document.getElementById("isOrderReprocessed").disabled = false; 

      if (document.getElementById("isOrderRefunded").checked)
      {
        document.getElementById("isOrderReprocessed").checked = false; 
        document.getElementById("isOrderReprocessed").disabled = true; 
      }
      if (document.getElementById("isOrderReprocessed").checked)
      {
        document.getElementById("isOrderRefunded").checked = false; 
        document.getElementById("isOrderRefunded").disabled = true; 
        document.getElementById("refundDisposition").disabled = true;
        document.getElementById("refundDisposition").selectedIndex = 0;
        document.getElementById("responsibleParty").disabled = true;
        document.getElementById("responsibleParty").selectedIndex = 0;
      }
      
      document.getElementById("sAnspAmount").disabled = true; 
      document.getElementById("sAnspOverride").disabled = true; 
      document.getElementById("sCancelReasonCode").selectedIndex = 0;
      document.getElementById("sCancelReasonCode").disabled = true; 
      document.getElementById("newMessage").disabled = true; 
      document.getElementById("isEmailAdded").disabled = false; 

    }

  }  



}



//***************************************************************************************************
// doResubmit()
//***************************************************************************************************/
function doResubmit()
{
  if (document.getElementById("isOrderReprocessed").checked)
  {
    //***REFUND ORDER
    document.getElementById("isOrderRefunded").checked = false; 
    document.getElementById("isOrderRefunded").disabled = true; 
    document.getElementById("refundDisposition").disabled = true;
    document.getElementById("refundDisposition").selectedIndex = 0;
    document.getElementById("responsibleParty").disabled = true;
    document.getElementById("responsibleParty").selectedIndex = 0;
  }
  else
  {
    //***REFUND ORDER
    document.getElementById("isOrderRefunded").checked = false; 
    document.getElementById("isOrderRefunded").disabled = false; 

  }

}