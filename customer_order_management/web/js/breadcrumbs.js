/**
 * Submits a breadcrumb link.  Creates HTML elements for the required parameters needed
 * to load the requested link.
 *
 * @param bcNum  The breadcrumb number 1-n
 */
function doBreadcrumbsAction(bcNum){
  var doAction = true;
  if ( hasFormChanged() ) {
    doAction = doExitPageAction(WARNING_MSG_4);
  }
  if ( doAction ) {
    showWaitMessage("mainContent", "wait", "Retrieving");
    var form = document.getElementById("breadcrumbsForm");
    var paramCount = document.getElementById("breadcrumb_param_count_" + bcNum).value;
    var formAction = document.getElementById("breadcrumb_url_" + bcNum).value;
    form.action = formAction;

    var actionValue = document.getElementById("breadcrumb_action_" + bcNum).value;
    addBreadcrumbElement(form, "action", actionValue);

    var name, value;
    for (var i = 0; i < paramCount; i++){
       name = document.getElementById("breadcrumb_param_name_" + bcNum + "_" + i).value;
       value = document.getElementById("breadcrumb_param_value_" + bcNum + "_" + i).value;
       addBreadcrumbElement(form, name, value);
    }
    form.submit();
  }
}

/**
 * Creates a new HTML input element and adds it to the form using the supplied values.
 *
 * NOTE:  I tried creating a "hidden" type input but could not get that to work.  Instead, I had to
 *        create a text input and just hide it by setting its display to "none".
 *
 * @param form  The form to add the element to
 * @param name  The name attribute of the text input created
 * @param value The value attribute of the text input created
 */
function addBreadcrumbElement(form, name, value) {
  var el = document.createElement("input");
  el.setAttribute("id", name);
  el.setAttribute("name", name);
  el.setAttribute("value", value);
  el.setAttribute("TYPE", "text");
  el.style.display = "none";
  form.appendChild(el);
}