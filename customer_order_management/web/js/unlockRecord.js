/*************************************************************************************
* Initialization()
**************************************************************************************/
function init(){
    setNavigationHandlers();
    addDefaultListeners();
    resetErrorFields();
    checkForErrorMessage();
    document.getElementById('in_csr_id').focus();
}


/*------------------------------------------------------------------------------------
* Global Variables
-------------------------------------------------------------------------------------*/
var fieldNames = new Array("in_csr_id");
var alphaNumFields = new Array("in_csr_id");


/*------------------------------------------------------------------------------------
* Actions
-------------------------------------------------------------------------------------*/

/*************************************************************************************
* Validate the form before the submit is allowed.
**************************************************************************************/
function doValidateForm() {
  resetErrorFields();
  clearErrorMessage();
  var isValid =  checkForEmptyFields();
  isValid = isValid && doAlphaNumFields();
  return isValid;
}

/*************************************************************************************
* checkForErrorMessage()
**************************************************************************************/
function checkForErrorMessage() {
  var message = document.forms[0].error_message.value;
  if (message.length > 0) {
    alert(document.forms[0].error_message.value);
  }
}  

/*************************************************************************************
* checkForErrorMessage() 
* Need to remove the server side error if we are attempting a search
**************************************************************************************/
function clearErrorMessage() {
  if (document.forms[0].error_message.value != null) {
    document.forms[0].error_message.value = '';
  }
}  


/*************************************************************************************
* resetErrorFields()
**************************************************************************************/
function resetErrorFields(){
  var spans = document.getElementsByTagName('span');
  for(var i = 0; i < spans.length; i++){
    if(spans[i].id != null && spans[i].id != ''){
      if(spans[i].id.substr(0,5) == 'error')
        spans[i].style.display = "none";
    }
  }
}


/*************************************************************************************
* Iterate through all fields that must contain only alphanumeric (i.e. ABC123)
* characters.  If any field is invalid do not submit the form.
**************************************************************************************/
function doAlphaNumFields(){
  var bool = true;
  for(var i = 0; i < alphaNumFields.length; i++) {
    var field = document.getElementById(alphaNumFields[i]);
    var value = stripInitialWhitespace(field.value);
    
    if( isAlphanumeric(value,true)) {
      continue;
    }
    else {
      if(field.id == 'in_csr_id')
        document.getElementById('errorCsrId').style.display = "block";
      bool = false;
    }
  }
  return bool;
}

function checkForEmptyFields(){
  var count = 0;
  var fieldCount = 0;
  var form = document.forms[0];
  for(var i = 0; i < form.elements.length; i++){
    if(form.elements[i].type == 'text'){
      ++count;
    if(form.elements[i].value == '')
      ++fieldCount;
    }
  }
  if(fieldCount == count) {
    alert("You must enter a csr id. ");
    document.getElementById('in_csr_id').style.background = 'pink';
    document.getElementById('in_csr_id').focus();
    return false;
  }
 return true;
}


/*************************************************************************************
* Clear all form fields
**************************************************************************************/
function doClearFields(){
  clearAllFields(document.forms[0]);
  resetErrorFields();
   document.getElementById('in_csr_id').focus();
}



/*
  Checks to see if the csr hit the enter key
  and submits the form.
*/
function doGetCsrLockedRecords(){
 if (window.event)
        if (window.event.keyCode)
           if (window.event.keyCode != 13)
             return false;
  var otype = event.srcElement.id;
  if(otype == 'clear')
    return;
  if(otype == 'mainMenu')
    return;
      var url = "unlockRecord.do" +
            "?action=get_locked_records";
       if(doValidateForm())
        performAction(url);
}


/*************************************************************************************
* Using the specified URL submit the form
**************************************************************************************/
function performAction(url){
  showWaitMessage("content", "wait", "Searching");
  document.forms[0].action = url;
  document.forms[0].submit();
}

/*************************************************************************************
* Performs the Back button functionality
**************************************************************************************/
function doBackAction() {
  doMainMenuAction();
}