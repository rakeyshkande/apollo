/*
 * Constructor
 */
InstitutionPopup = function(params) {
  this.focusObj = null;
  this.institution = params.institution;
  this.institutionType = params.institutionType;
  this.address1Name = params.address1;
  this.address2Name = params.address2;
  this.cityName = params.city;
  this.stateName = params.state;
  this.zipName = params.zip;
  this.countryName = params.country;
  this.phoneName = params.phone;
};

InstitutionPopup._I = null;

/*
 * Class functions
 */
InstitutionPopup.setup = function(params) {
   window.popup = Popup.setup(params, new InstitutionPopup(params));
   window.popup.create();
   window.popup.showAtElement(params.displayArea);
};

InstitutionPopup.enterInstitutionSearch = function(ev) {
   if (ev.keyCode == 13)
      InstitutionPopup.searchInstitution(ev);
};


InstitutionPopup.pressHide = function () {
  if (window.event.keyCode == 13)
      Popup.hide();
  // if tab on close button, return to first button
  if (window.event.keyCode == 9){
     var input = document.getElementById("institutionInput");
      input.focus();
      return false;
  }
};


InstitutionPopup.searchInstitution = function(ev) {
   //First validate the inputs
   var check = true;
   var institutionVal = document.getElementById("institutionInput").value;
   var city = document.getElementById("cityInput");
   var state = stripWhitespace(document.getElementById("stateInput").value);
   var country = stripWhitespace(document.getElementById(InstitutionPopup._I.countryName).value);

   //City is required
   if((stripWhitespace(city.value).length == 0) ) {
     if (check == true) {
       city.focus();
       check = false;
     }
     city.style.backgroundColor = 'pink';
   }

   if (!check) {
     alert("Please correct the marked fields")
     return false;
   }

   //Now that everything is valid, open the popup after removing special characters
   var bag2 = ",/.<>?;:\|[]{}~!@#$%^&*()-_=+`" + "\\\'";
   var cityVal = stripCharsInBag(city.value, bag2);

   var url_source="lookupInstitution.do" +
   "?institutionInput=" + institutionVal +
   "&cityInput=" + cityVal +
   "&stateInput=" + state +
   "&countryInput=" + country +
   "&institutionTypeInput=" + InstitutionPopup._I.institutionType +
   getSecurityParams(false);

   //Open the window
   var modal_dim = "dialogWidth:800px; dialogHeight:500px; center:yes; status=0";
   var ret = showModalDialogIFrame(url_source, "", modal_dim);

  // Set the new city, state, and zip
  if (ret && ret != null && ret[0] != ""){
    document.getElementById(InstitutionPopup._I.institution).value = ret[0];
    document.getElementById(InstitutionPopup._I.address1Name).value = ret[1];
    document.getElementById(InstitutionPopup._I.address2Name).value = ret[2];
    document.getElementById(InstitutionPopup._I.cityName).value = ret[3];
    populateStates(InstitutionPopup._I.countryName, InstitutionPopup._I.stateName, ret[4]);
    document.getElementById(InstitutionPopup._I.zipName).value = ret[5];
    document.getElementById(InstitutionPopup._I.phoneName).value = ret[6];
  }
  Popup.hide();
  InstitutionPopup._I = null;
};


/*
 * Member Functions
 */
InstitutionPopup.prototype.renderContent = function(div) {
   InstitutionPopup._I = this;

   var table = Popup.createElement("table");
   table.setAttribute("cellSpacing", "2");
   table.setAttribute("cellPadding", "2");
   table.setAttribute("className", "LookupTable");
   div.appendChild(table);

   var thead, tbody, tfoot, row, cell, input, image;
   thead = Popup.createElement("thead", table);
   row = Popup.createElement("tr", thead);

   cell = Popup.createElement("td", row);
   cell.setAttribute("className", "PopupHeader");
   cell.setAttribute("colSpan", "2");
   cell.style.cursor = "move";
   cell.popup = window.popup;
   cell.appendChild(document.createTextNode("Business Lookup"));
   Popup.addDragListener(cell);

   tbody = Popup.createElement("tbody", table);
   row = Popup.createElement("tr", tbody);
   cell = Popup.createElement("td", row);

   // Institution name
   row = Popup.createElement("tr", tbody);
   cell = Popup.createElement("td", row);
   cell.setAttribute("className", "labelRight");
   cell.appendChild(document.createTextNode("Institution:"));

   cell = Popup.createElement("td", row);
   input = Popup.createElement("input", cell);
   this.focusObj = input;
   input.setAttribute("id", "institutionInput");
   input.setAttribute("tabIndex", "105");
   input.setAttribute("TYPE", "text");
   Popup.addEvent(input, "blur", fieldBlur);
   Popup.addEvent(input, "focus", fieldFocus);
   Popup.addEvent(input, "keydown", InstitutionPopup.enterInstitutionSearch);

   // City
   row = Popup.createElement("tr", tbody);
   cell = Popup.createElement("td", row);
   cell.setAttribute("className", "labelRight");
   cell.appendChild(document.createTextNode("City:"));

   cell = Popup.createElement("td", row);
   input = Popup.createElement("input", cell);
   input.setAttribute("id", "cityInput");
   input.setAttribute("tabIndex", "106");
   input.setAttribute("TYPE", "text");
   Popup.addEvent(input, "blur", fieldBlur);
   Popup.addEvent(input, "focus", fieldFocus);
   Popup.addEvent(input, "keydown", InstitutionPopup.enterInstitutionSearch);

   // States
   row = Popup.createElement("tr", tbody);
   cell = Popup.createElement("td", row);
   cell.setAttribute("className", "labelRight");
   cell.appendChild(document.createTextNode("State:"));

   cell = Popup.createElement("td", row);
   input = Popup.createElement("select", cell);
   input.setAttribute("id", "stateInput");
   input.setAttribute("tabIndex", "107");

   // Buttons
   tfoot = Popup.createElement("tfoot", table);
   row = Popup.createElement("tr", tfoot);
   cell = Popup.createElement("td", row);
   cell.setAttribute("colSpan", "2");
   cell.setAttribute("align", "right");

   button = Popup.createElement("button", cell);
   button.setAttribute("value", "Search");
   button.setAttribute("className", "BlueButton");
   button.setAttribute("tabIndex", "109");
   button.setAttribute("accessKey", "s");
   Popup.addEvent(button, "click", InstitutionPopup.searchInstitution);
   Popup.addEvent(button, "keydown", InstitutionPopup.enterInstitutionSearch);

   button = Popup.createElement("button", cell);
   button.setAttribute("value", "Close screen");
   button.setAttribute("className", "BlueButton");
   button.setAttribute("tabIndex", "110");
   button.setAttribute("accessKey", "c");
   Popup.addEvent(button, "click", Popup.hide);
   Popup.addEvent(button, "keydown", InstitutionPopup.pressHide);
};

InstitutionPopup.prototype.setFocus = function() {
   if (this.focusObj) {
      this.focusObj.focus();
   }
};

InstitutionPopup.prototype.setValues = function() {
  var state = document.getElementById(InstitutionPopup._I.stateName);
  document.getElementById("institutionInput").value = document.getElementById(InstitutionPopup._I.institution).value;
  document.getElementById("cityInput").value = document.getElementById(InstitutionPopup._I.cityName).value;
  populateStates(InstitutionPopup._I.countryName, "stateInput", state[state.selectedIndex].value);
};
