var FLORIST_TYPE = "";
var FLORIST_TYPE_AUTO = "auto";
var FLORIST_TYPE_MAN = "man";
var FLORIST_TYPE_AUTO_VALUE = "";
var COMPLETE_ACTION  = "validate"
var CUSTOM_ACTION = "custom";
var FLORIST_LOOKUP = "florist_lookup";
var callingAction = "";
var validationMatrix = new Array
     (//input attribute name      error message       validation function     additional function parameters
      ["page_florist_id",              "Invalid format",   validateFloristCode]
     );

/*
 * Called when the page loads.
 */
function init(){
  document.oncontextmenu = contextMenuHandler;
  document.onkeydown = udcBackKeyHandler;
  addDefaultListeners();
  initErrorMessages();
}

/*
 * Called when the page loads, this function displays any error messages
 * generated on load or from validation failures.
 */
function initErrorMessages() {
  if ( containsErrors ) {
    var array = doDisplayServerSideValidationMessages();

    if ( array.goToPage != null && array.goToPage != "" ) {
      doCustomAction( array.goToPage );
    }
    else if ( array.jsFunction != null) {
      array.jsFunction();
    }
  }
}

/*
 * Begins the Florist Lookup process.
 */
function doFloristLookup() {
  var vendorFlag = document.getElementById("vendor_flag").value;
  var cty = document.getElementById("recipient_country").value;
  var zip = document.getElementById("recipient_zip_code").value;
  if ( cty == 'US' || cty == 'USA' ){
  	if ( zip.length > 5 ){
  	    zip = zip.substring(0,5);
  	}
  }
  else if ( cty == 'CA' ){
  	if (zip.length > 3){
  	    zip = zip.substring(0,3);
  	}
  }
  var messageType = (vendorFlag == 'N' || vendorFlag == 'n') ? "Mercury" : "Venus";
  var args = new Array();
  args.action = "floristLookup.do";
  args[args.length] = new Array("order_detail_id", document.getElementById("order_detail_id").value);
  args[args.length] = new Array("msg_message_order_number", document.getElementById("message_order_number").value);
  args[args.length] = new Array("msg_message_type", messageType);
  args[args.length] = new Array("recipient_city", document.getElementById("recipient_city").value);
  args[args.length] = new Array("recipient_state", document.getElementById("recipient_state").value);
  args[args.length] = new Array("recipient_zip", zip);
  args[args.length] = new Array("order_delivery_timestamp", document.getElementById("delivery_date").value);
  args[args.length] = new Array("product_id", document.getElementById("product_id").value);
  args[args.length] = new Array("source_code", document.getElementById("source_code").value);
  args[args.length] = new Array("florist_prd_price", document.getElementById("florist_prd_price").value);
  args[args.length] = new Array("securitytoken", document.getElementById("securitytoken").value);
  args[args.length] = new Array("context", document.getElementById("context").value);
  args[args.length] = new Array("destination", "recipient_order_page");
  args[args.length] = new Array("modify_order_flag", "Y");  //???

  var modal_dim = "dialogWidth:750px; dialogHeight:400px; dialogLeft:125; dialogTop: 190; center:yes; status=0; help:no; scroll:no";
  var floristInfo = window.showModalDialog("html/waitModalDialog.html", args, modal_dim);
  if ( floristInfo && floristInfo[0] != '') {
    document.getElementById("page_florist_id").value = floristInfo[0];
    FLORIST_TYPE_AUTO_VALUE = floristInfo[0];
    FLORIST_TYPE = FLORIST_TYPE_AUTO;
  }
}

/*
 * Checks the lock before completing the update.
 */
function doCompleteUpdateAction(){
  doLinkAction('loadPayment.do');
}

/*
 * Called after lock checking.
 */
function doCompleteUpdatePostAction(){
  showWaitMessage("mainContent", "wait", "Updating");
  prepareSubmit();
  var toSubmit = document.getElementById("udcForm");
  toSubmit.action += "?action=" + COMPLETE_ACTION;
  toSubmit.submit();
}

/*
 * Checks the lock before canceling the update.
 */
function doCancelUpdateAction(){
  if ( hasFormChanged() ) {
    if ( doExitPageAction(WARNING_MSG_4) ) {
      baseDoCancelUpdateAction("udcForm");
    }
  }
  else {
    baseDoCancelUpdateAction("udcForm");
  }
}

/*
 * Custom action determined by server side processing.
 */
function doCustomAction(action){
  document.getElementById("udcForm").action = action;
  callingAction = CUSTOM_ACTION;
  doRecordLock('MODIFY_ORDER', 'check', null, "udcForm");
}

/*
 * Submits the custom action after retrieving the lock.
 */
function doCustomPostAction(){
  showWaitMessage("mainContent", "wait", "Retrieving");
  document.getElementById("udcForm").submit();
}

/*
 * Calls post action after checking the lock.
 */
function checkAndForward(forwardAction){
  if ( callingAction == COMPLETE_ACTION ) {
    doCompleteUpdatePostAction();
  }
  else if ( callingAction == CUSTOM_ACTION  ) {
    doCustomPostAction();
  }
}


/*
 *  Disables the various navigation keyboard commands.
 */
function udcBackKeyHandler() {
   // backspace
   if (window.event && window.event.keyCode == 8){
      var formElement = false;

      for(i = 0; i < document.forms['udcForm'].elements.length; i++){
         if(document.forms['udcForm'].elements[i].name == document.activeElement.name){
            formElement = true;
            break;
         }
      }

      if(formElement == false || (document.activeElement.type != "text" && document.activeElement.type != "textarea" && document.activeElement.type != "password")){
         window.event.cancelBubble = true;
         window.event.returnValue = false;
         return false;
      }
   }

   // F5 and F11
   if (window.event.keyCode == 122 || window.event.keyCode == 116){
      window.event.keyCode = 0;
      event.returnValue = false;
      return false;
   }

   // Alt + Left Arrow
   if(window.event.altLeft){
      window.event.returnValue = false;
      return false;
   }

   // Alt + Right Arrow
   if(window.event.altKey){
      window.event.returnValue = false;
      return false;
   }
}


/*
 * Performs the submit for the links on the page.
 */
function doLinkAction(url) {
  if ((document.getElementById("page_florist_id")!= null) && (document.getElementById("page_florist_id").value != document.getElementById("page_florist_id").originalValue)) {
    if ( doExitPageAction(WARNING_MSG_4) ) {
      var toSubmit = document.getElementById("udcForm");
      toSubmit.action = url;
      toSubmit.submit();
    }
  }
  else {
    var toSubmit = document.getElementById("udcForm");
    toSubmit.action = url;
    if (url == "paymentPage.do" || url == "loadPayment.do") {
        toSubmit.action = "https://" + siteNameSsl + applicationContext + "/" + url;
    }
    toSubmit.submit();
  }
}


/*
 * Validates for valid florist code format.
 */
function validateFloristCode() {
  var id = document.getElementById("page_florist_id");
  if ( id ) {
    var validFloristCode = /\d{2}-\d{4}[A-Z]{2}/;
    if ( id && !isWhitespace(id.value)) {
      return validFloristCode.test( id.value );
    }
    return true;
  }
  else {
    return true;
  }
}


/*
 * Called before a submit to prepare all calculated values
 */
function prepareSubmit() {
  prepareFloristType();
}

/*
 * Sets the florist_type value.
 */
function prepareFloristType() {
  var pageId = document.getElementById("page_florist_id");
  if ( pageId ) {
    var pageIdValue = pageId.value;
    var id = document.getElementById("florist_id").value;
    var type = document.getElementById("florist_type");
    if ( isWhitespace(pageIdValue) ) {
      type.value = "";
    }
    else if ( FLORIST_TYPE == FLORIST_TYPE_AUTO && pageIdValue != FLORIST_TYPE_AUTO_VALUE ) {
      type.value = FLORIST_TYPE_MAN;
    }
    else if ( FLORIST_TYPE != FLORIST_TYPE_AUTO && id != pageIdValue) {
      type.value = FLORIST_TYPE_MAN;
    }
    else {
      type.value = FLORIST_TYPE;
    }
  }
}

function doLargeImagePopup()
{
   largeImage = document.getElementById("showLargeImage").style;
   scroll_top = document.body.scrollTop;
   largeImage.top = scroll_top + document.body.clientHeight/2 - 200;
   largeImage.width = 340;
   largeImage.height = 400;
   largeImage.left = 200;
   largeImage.display = "block";
   hideShowCovered(showLargeImage);
}

function closeLargeImage(){
  largeImage.display = "none";
  hideShowCovered(showLargeImage);
}


/*
 * Displays server side validation messages.
 */
function doDisplayServerSideValidationMessages() {
  var ID = 0;
  var TEXT = 1;
  var YES = 2;
  var NO = 3;
  var toReturn = new Array();
  var urlOrId = "";

  // highlight errors
  if ( fieldErrors != null && fieldErrors.length > 0 ) {
    validateForm(fieldErrors);
  }

  // error messages requiring an 'Ok' popup
  if ( okErrors != null && okErrors.length > 0 ) {
    // concatenate error messages
    var messages = "";
    for (var i = 0; i < okErrors.length; i++) {
      messages += okErrors[i][TEXT] + "<br><br>";
    }

    // modal arguments
    var modalArguments = new Object();
    modalArguments.modalText = messages;

    // modal features
    var modalFeatures = 'dialogWidth:300px; dialogHeight:200px; center:yes; status=no; help=no; resizable:no; scroll:yes';
    toReturn.okErrorReturnValue = window.showModalDialog("alert.html", modalArguments, modalFeatures);
    if ( toReturn.okErrorReturnValue ) {
      urlOrId = okErrors[0][YES];
    }
  }

  // error messages requiring a 'Yes/No' popup
  if ( confirmErrors != null && confirmErrors.length > 0 ) {
    for ( var i = 0; i < confirmErrors.length; i++) {
      if ( urlOrId == '' || urlOrId == confirmErrors[i][ID] ) {
        // concatenate error messages
        var messages = confirmErrors[i][TEXT];

        // modal arguments
        var modalArguments = new Object();
        modalArguments.modalText = messages;

        // modal features
        var modalFeatures = 'dialogWidth:300px; dialogHeight:200px; center:yes; status=no; help=no; resizable:no; scroll:yes';
        toReturn.confirmErrorReturnValue = window.showModalDialog("confirm.html", modalArguments, modalFeatures);
        if ( toReturn.confirmErrorReturnValue ) {
          urlOrId = confirmErrors[i][YES];
        }
        else {
          urlOrId = confirmErrors[i][NO];
        }
      }
    }
  }

  // a JS function may need to be called, if so check wether or not the
  // urlOrId field contains an action or not
  if ( urlOrId.indexOf(".do") < 0 ) {
    if ( urlOrId == FLORIST_LOOKUP ) {
      toReturn.jsFunction = doFloristLookup;
    }
  }
  else {
    toReturn.goToPage = urlOrId;
  }
  return toReturn;
}
