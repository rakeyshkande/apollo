/***************************************************************************************************
 * VARIABLES
 ***************************************************************************************************/
var validationMatrix = new Array
     (//input attribute name           error message                          validation function   additional function parameters
      ["page_source_code",             "Required Field",                      isNotWhitespaceObj],
      ["recipient_phone_number",       "Invalid Format - Phone Number",       validatePhone,        true],
      ["recipient_extension",          "Invalid Format - Phone Extension",    isIntegerObj,         true],
      ["recipient_first_name",         "Required Field",                      isNotWhitespaceObj],
      ["recipient_last_name",          "Required Field",                      isNotWhitespaceObj],
      ["recipient_business_name",      "Required Field",                      validateBusinessName],
      ["recipient_address_1",          "Required Field",                      isNotWhitespaceObj],
      ["recipient_address_1",          "PO Box or APO address not allowed",   validateRecipientAddress],
      ["recipient_address_2",          "PO Box or APO address not allowed",   validateRecipientAddress],
      ["recipient_city",               "Required Field",                      isNotWhitespaceObj],
      ["card_message",                 "",                                    validateCardMessage],
      ["card_signature",               "",                                    validateCardMessage],
      ["recipient_zip_code",           "Invalid Format",                      udiValidateZipCode,   false],
      ["alt_contact_phone_number",     "Invalid Format - Phone Number",       validatePhone,        true],
      ["alt_contact_extension",        "Invalid Format - Phone Extension",    isIntegerObj,         true],
      ["recipient_state",			   "Required Field",					  isNotWhitespaceObj],
      ["recipient_state",			   "Invalid state for zip code",	  validateStateByZipCode]					

     );

var specialCharacters 				= ",/.<>?;:\|[]{}~!@#$%^&*()-_=+`" + "\\\'";
var specialCharactersForName 	= ",/.<>?;:\|[]{}~!@#$%^*()-_=+`" + "\\\'";

// Globals for AJAX Library
var securitytoken;
var applicationcontext;
var context;

/***************************************************************************************************
 * Validates the phone.  Overwrites the method of the same name in FormChek.js
 ***************************************************************************************************/
function  validatePhone(inputName, emptyOk){
  emptyOk = (validatePhone.arguments.length == 1) ? false : emptyOk;
  var phone = document.getElementById(inputName);
  var newCountry = document.getElementById("recipient_country").value;
  if (newCountry == "US" || newCountry == "CA")
    return checkUSPhone(phone, emptyOk);
  else
    return checkInternationalPhone(phone, emptyOk);
}


/**************************************************************************************************
* Sets indexes
**************************************************************************************************/

function setUpdateDeliveryTabIndexes()
{
  untabAllElements();
  var tabArray = new Array("custUpdateProduct", "custComplete", "custCancel", "page_source_code",
    "sourceCodeLink", "delivery_date_select", "getMaxButton", "membership_number",
    "membership_first_name", "membership_last_name", "page_recipient_address_type",
    "recipient_phone_number", "recipient_extension", "recipientLookupLink", "institutionLookupLink",
    "recipient_first_name", "recipient_last_name", "recipient_business_name", "special_instructions",
    "special_instructions_start_time", "special_instructions_end_time", "recipient_address_1",
    "recipient_address_2", "recipient_city", "recipient_state", "recipient_zip_code",
    "cityLookupLink", "recipient_country", "card_message", "arrowButton", "stock_messages",
    "spellCheckButton", "card_signature", "alt_contact_last_name", "release_info_indicator",
    "alt_contact_phone_number", "alt_contact_extension", "alt_contact_email", "custUpdateProductBottom",
    "custCompleteBottom", "custCancelBottom" );
  var begIndex = setTabIndexes(tabArray, begIndex);
  // set focus to the first element in the array that exists //
  setFocusForArray(tabArray);
  return begIndex;
}


/***************************************************************************************************
 * Alternates the recipient label depending on the delivery location type.
 ***************************************************************************************************/
function doDeliveryTypeChange(){
  var dd = document.getElementById("page_recipient_address_type");
  var selected = dd[dd.selectedIndex].value;

  // Recipient/Deceased
  var fn = document.getElementById("recipient_first_name_cell");
  var ln = document.getElementById("recipient_last_name_cell");
  var rs = document.getElementById("release_info_indicator_cell");
  if ( selected == "F" ) {
    fn.innerHTML = "Deceased's First Name:";
    ln.innerHTML = "Deceased's Last Name:";
  }
  else {
    fn.innerHTML = "Recipient First Name:";
    ln.innerHTML = "Recipient Last Name:";
  }

  // Business/Institution Name
  
  var bn = document.getElementById("recipient_business_name_cell");
  
  if (ADDRESS_TYPES[selected].promptForBusiness == "Y") {
    bn.style.display = "block";
  } else {
    bn.style.display = "none";
  }

  

  // Time of Service & Room Number or Special instructions
  var rn = document.getElementById("room_number_cell");   
  if ( !containsSpecialInstructions ) {
    if (ADDRESS_TYPES[selected].promptForRoom == "Y") {
      document.getElementById("room_number_label").innerText=ADDRESS_TYPES[selected].roomLabelTxt;
      rn.style.display = "block";
    } else {
      rn.style.display = "none";
    }
  
    var ts = document.getElementById("time_of_service_cell");
    if ( ADDRESS_TYPES[selected].promptForHours == "Y" ) {
      document.getElementById("time_of_service_label").innerText=ADDRESS_TYPES[selected].hoursLabelTxt;
      ts.style.display = "block";
    }
    else {
      ts.style.display = "none";
    }
  } else {
    if (ADDRESS_TYPES[selected].promptForRoom == "Y") {
      rn.style.display = "block";
    } else {
      rn.style.display = "none";
    }
  }
}


/***************************************************************************************************
 * Validates that business name contains non-whitespace characters.
 ***************************************************************************************************/
function validateBusinessName(){
  var dd = document.getElementById("page_recipient_address_type");
  var selected = dd[dd.selectedIndex].value;
  var bn = document.getElementById("recipient_business_name");
  if ( ADDRESS_TYPES[selected].promptForBusiness == "Y" ) {
    return !isWhitespace(bn.value);
  }
  return true;
}


/***************************************************************************************************
 * Validates card message and signature for 240 total character limit.
 ***************************************************************************************************/
function validateCardMessage(){
  var cm = document.getElementById("card_message");
  var sig = document.getElementById("card_signature");
  if (cm.value.length + sig.value.length > 240){
    var cmm = document.getElementById("card_message_validation_message_cell");
    cmm.innerHTML = "Card message and signature cannot exceed 240 combined characters.  Card message contains " + cm.value.length + " characters.";

    var sigm = document.getElementById("card_signature_validation_message_cell");
    sigm.innerHTML = "Card message and signature cannot exceed 240 combined characters.  Signature contains " + sig.value.length + " characters.";
    return false;
  }
  return true;
}

/***************************************************************************************************
 * Validates recipient zip code.
 ***************************************************************************************************/
function udiValidateZipCode(inputName, emptyOk) {
  return validateZipCode(inputName, "recipient_country", emptyOk);
}


/***************************************************************************************************
 * Validates recipient address 1 to check for Post Office or APO in a string
 ***************************************************************************************************/
function validateRecipientAddress(inputName)
{
	var POAPO = new Array("PO BOX",
												"P.O.",
												"POSTOFFICE",
												"APO",
												"A.P.O.",
												"FPO",
												"F.P.O");

  //note that this is a custom trim function
  var recipientAddress = Trim(document.getElementById(inputName).value);

	for (var i = (POAPO.length - 1); i >= 0; i--)
	{
		if ((recipientAddress.toUpperCase()).indexOf(POAPO[i]) >= 0)
		{
			return false;
		}
	}

	return true;

}


/***************************************************************************************************
 * Opens the source code lookup div.
 ***************************************************************************************************/
function doSourceCodeLookup(){
  SourceCodePopup.setup(
	{
	  displayArea: "sourceCodeLink",
	  sourceCode: "page_source_code"
	}
  );
}


/***************************************************************************************************
 * Opens the recipient lookup div.
 ***************************************************************************************************/
function doRecipientLookup(){
  RecipientPopup.setup(
	{
	 displayArea:"recipientLookupLink",
	 phone: "recipient_phone_number",
	 first_name: "recipient_first_name",
	 last_name: "recipient_last_name",
	 address1: "recipient_address_1",
	 address2: "recipient_address_2",
	 city: "recipient_city",
	 state: "recipient_state",
	 zip: "recipient_zip_code",
	 country: "recipient_country"
	}
  );
}


/***************************************************************************************************
 * Opens the city lookup lookup div.
 ***************************************************************************************************/
function doCityLookup(){
  CityPopup.setup(
	{
	 displayArea:"cityLookupLink",
	 city: "recipient_city",
	 state: "recipient_state",
	 zip: "recipient_zip_code",
	 country: "recipient_country"
	}
  );
}

/***************************************************************************************************
 * Opens the institution lookup lookup div.
 ***************************************************************************************************/
function doInstitutionLookup(){
  var dd = document.getElementById("page_recipient_address_type");
  InstitutionPopup.setup(
  {
   displayArea:"institutionLookupLink",
   institution: "recipient_business_name",
   institutionType: dd[dd.selectedIndex].value,
   address1: "recipient_address_1",
   address2: "recipient_address_2",
   city: "recipient_city",
   state: "recipient_state",
   zip: "recipient_zip_code",
   country: "recipient_country",
   phone: "recipient_phone_number"
  }
  );
}


/***************************************************************************************************
 * Moves a Stock Message into the Card Message text area.
 ***************************************************************************************************/
function moveMessage(){
  var cm = document.getElementById("card_message");
  var sm = document.getElementById("stock_messages");
  for (var i = 0; i < sm.length; i++){
    if ( sm[i].selected ) {
      cm.value += " " + sm[i].text;
    }
  }
}


/***************************************************************************************************
 * Performs country select box onchange event.  A popup is needed to confirm
 * changing countries from US or Canada to International and vice versa.
 ***************************************************************************************************/
function doCountryChange(event){
  var newCountry = event.srcElement[event.srcElement.selectedIndex].value;
  var countryChanged = false;

  // INTL -> (US || CA)
  if ( (ORIG_COUNTRY == "US" || ORIG_COUNTRY == "CA") && !(newCountry == "US" || newCountry == "CA") )
  {
    countryChanged = true;
  }
  // (US || CA) -> INTL
  else if ( !(ORIG_COUNTRY == "US" || ORIG_COUNTRY == "CA") && (newCountry == "US" || newCountry == "CA") )
  {
    countryChanged = true;
  }

	//country was changed
  if (countryChanged)
  {
		document.getElementById("recipient_zip_code").value = "";
		document.getElementById("recipient_city").value = "";
  }

  populateStates("recipient_country", "recipient_state");

}


/***************************************************************************************************
 * Calls the spell check service.
 *
 * @param value The text to spell check.
 * @param name  The name attribute of the input that contains the text to spell check.
 ***************************************************************************************************/
function doSpellCheckAction(value, name) {
  value = escape(value).replace(/\+/g,"%2B");
  var url = "spellCheck.do?content=" + value +
            "&callerName=" + name +
            getSecurityParams(false);

  window.open(url,"Spell_Check","toolbar=no,resizable=yes,scrollbars=yes,width=500,height=300");
}


/***************************************************************************************************
 * Displays server side validation messages.
 *
 * @param genericErrors messages to be displayed in a popup
 * @param fieldErrors   messages to be displayed under the invalid input
 ***************************************************************************************************/
function doDisplayServerSideValidationMessages(confirmErrors, okErrors, fieldErrors) {
  hideWaitMessage("mainContent", "wait");
  var toReturn = new Array();

  // highlight errors
  if ( fieldErrors != null && fieldErrors.length > 0 ) {
    validateForm(fieldErrors);
  }

  // error messages requiring an 'Ok' popup
  if ( okErrors != null && okErrors.length > 0 ) {
    // concatenate error messages
    var messages = "";
    for (var i = 0; i < okErrors.length; i++) {
      messages += okErrors[i][0] + "<br><br>";
    }

    // modal arguments
    var modalArguments = new Object();
    modalArguments.modalText = messages;

    // modal features
    var modalFeatures = 'dialogWidth:300px; dialogHeight:300px; center:yes; status=no; help=no; resizable:no; scroll:yes';
    toReturn.okErrorReturnValue = window.showModalDialog("alert.html", modalArguments, modalFeatures);
    toReturn.okGoToPage = okErrors[0][1];
  }

  // error messages requiring a 'Yes/No' popup
  if ( confirmErrors != null && confirmErrors.length > 0 ) {
    // concatenate error messages
    var messages = confirmErrors[0][0];

    // modal arguments
    var modalArguments = new Object();
    modalArguments.modalText = messages;

    // modal features
    var modalFeatures = 'dialogWidth:300px; dialogHeight:200px; center:yes; status=no; help=no; resizable:no; scroll:yes';
    toReturn.confirmErrorReturnValue = window.showModalDialog("confirm.html", modalArguments, modalFeatures);
    toReturn.confirmGoToPage = confirmErrors[0][1];
  }

  return toReturn;
}

/****************************************************************************************************
 * ACTIONS
 ***************************************************************************************************/
var SAVE_ACTION      = "save";
var CONTINUE_ACTION  = "continue";
var COMPLETE_ACTION  = "save_and_complete"
var CANCEL_ACTION    = "cancel_update";
var CUSTOM_ACTION = "custom";
var callingAction = "";


var callingAction = "";


/**************************************************************************************************
**************************************************************************************************/
function doContinueUpdateAction()
{
	
	if(isGDOrder)
	{
		validateGiftCardSourceCode();
	}
	else
	{
		doContinueUpdateActionNoSrcCheck();
	}
}

function doContinueUpdateActionNoSrcCheck()
{
	
	if ( validateForm(validationMatrix) )
	{
  var freeShippingActiveMember = document.getElementById("free_Shipping_Active_Member").value;

  if (freeShippingActiveMember == 'Y' ) 
  {
    var sourceCodeUsed = document.getElementById("page_source_code").value ;

    var requestURL = "sourceCodeLookUpAjaxAction.do?"
    + "sourceCodeInput=" + sourceCodeUsed
    + "&securitytoken=" + document.getElementById("securitytoken").value
    + "&context=" + document.getElementById("context").value;
        
    jQuery.ajax(
     {     
        type: "POST",     
        url: requestURL,     
        dataType: "json",        
        contentType: "application/json; charset=utf-8",     
        success: getFreeShipFlagFromSource,
        error: function(xhr){ alert(xhr.statusText);}                 
    }); 

    return ;
  }
    callingAction = SAVE_ACTION;
		doRecordLock('MODIFY_ORDER', 'check', null, "deliveryInfoForm");

	}
}

function getFreeShipFlagFromSource(data)
{
    var sourceCode   = data;

    if (sourceCode.allowFreeShippingFlag != 'Y' )
    {
        var freeShippingProgramName = document.getElementById("free_Shipping_Program_Name").value;

        if(!doConfirmPageActionSetSize("You are attempting to change from a " + freeShippingProgramName + " qualifying source code to a non-"+ freeShippingProgramName + " qualifying source code. This " + freeShippingProgramName + " member will not recieve their benefits.<BR> Are you sure you want to continue? ", '400px', '150px'))
        {
          return;
        }
     }
    callingAction = SAVE_ACTION;
		doRecordLock('MODIFY_ORDER', 'check', null, "deliveryInfoForm");
}

/**************************************************************************************************
**************************************************************************************************/
function doContinueUpdatePostAction(){
  showWaitMessage("mainContent", "wait", "Updating");
  prepareFieldsForSubmit(); // defined in vendorDeliveryDates.xsl && floristDeliveryDates.xsl
  document.getElementById("deliveryInfoForm").action += "?action="+SAVE_ACTION;
  document.getElementById("deliveryInfoForm").submit()
}


/**************************************************************************************************
**************************************************************************************************/
function doCompleteUpdateAction()
{
  if ( validateForm(validationMatrix) )
  {
    callingAction = COMPLETE_ACTION;
    doRecordLock('MODIFY_ORDER', 'check', null, "deliveryInfoForm");
  }
}

/**************************************************************************************************
**************************************************************************************************/
function doCompleteUpdatePostAction(){
   showWaitMessage("mainContent", "wait", "Updating");
   prepareFieldsForSubmit(); // defined in vendorDeliveryDates.xsl && floristDeliveryDates.xsl
   document.getElementById("deliveryInfoForm").action += "?action="+COMPLETE_ACTION;
   document.getElementById("deliveryInfoForm").submit()
}


/**************************************************************************************************
**************************************************************************************************/
function doCancelUpdateAction(){
  if ( hasFormChanged() ) {
    if ( doExitPageAction(WARNING_MSG_4) ) {
      baseDoCancelUpdateAction("deliveryInfoForm");
    }
  }
  else {
    baseDoCancelUpdateAction("deliveryInfoForm");
  }
}

/***************************************************************************************************
 * Calls post action after checking the lock.
 ***************************************************************************************************/
function checkAndForward(forwardAction){
  if ( callingAction == SAVE_ACTION ) {
    doContinueUpdatePostAction();
  }
  else if ( callingAction == COMPLETE_ACTION ) {
    doCompleteUpdatePostAction();
  }
  else if ( callingAction == CUSTOM_ACTION  ) {
    showWaitMessage("mainContent", "wait", "Retrieving");
    document.getElementById("deliveryInfoForm").submit();
  }
}


/***************************************************************************************************
 * Calls any functions that must be exectued prior to submitting the form.
 ***************************************************************************************************/
function prepareFieldsForSubmit() {
  prepareSourceCode();
  prepareDeliveryLocation();
  preparePhoneNumbers();
  removeSpecialCharacters();
}


/***************************************************************************************************
 * Removes special characters from these fields
 ***************************************************************************************************/

function removeSpecialCharacters()
{
  document.getElementById("recipient_first_name").value = stripCharsInBag(document.getElementById("recipient_first_name").value, specialCharactersForName);
  document.getElementById("recipient_last_name").value = stripCharsInBag(document.getElementById("recipient_last_name").value, specialCharacters);
  document.getElementById("recipient_city").value = stripCharsInBag(document.getElementById("recipient_city").value, specialCharacters);

/*Following two lines commented out do to the results of stripping out decimals in addresses.
  This causes the screen to detect a change which results in inprotecting the florist hyperlink going forward.  Issue 2427.
  document.getElementById("recipient_address_1").value = stripCharsInBag(document.getElementById("recipient_address_1").value, specialCharacters);
  document.getElementById("recipient_address_2").value = stripCharsInBag(document.getElementById("recipient_address_2").value, specialCharacters);

*/
  document.getElementById("alt_contact_last_name").value = stripCharsInBag(document.getElementById("alt_contact_last_name").value, specialCharacters);
  if (document.getElementById("membership_first_name") != null)
    document.getElementById("membership_first_name").value = stripCharsInBag(document.getElementById("membership_first_name").value, specialCharacters);
  if (document.getElementById("membership_last_name"))
    document.getElementById("membership_last_name").value = stripCharsInBag(document.getElementById("membership_last_name").value, specialCharacters);
}


/***************************************************************************************************
 * Places the page_source_code into source_code.  Puts the value into the field the
 * action will be checking.
 ***************************************************************************************************/
function prepareSourceCode() {
  document.getElementById("source_code").value = document.getElementById("page_source_code").value;
}


/***************************************************************************************************
 * Takes the text from the Delivery Location drop down and places it into a hidden
 * field for submission.
 ***************************************************************************************************/
function prepareDeliveryLocation() {
  var dd = document.getElementById("page_recipient_address_type");
  document.getElementById("recipient_address_code").value = dd[dd.selectedIndex].value;
  document.getElementById("recipient_address_type").value = dd[dd.selectedIndex].type;
}


/***************************************************************************************************
 *  Reformats the phone number for submission.
 *
 ***************************************************************************************************/
function preparePhoneNumbers() {
  var recipPhoneNumber = document.getElementById("recipient_phone_number").value;
  var altPhoneNumber = document.getElementById("alt_contact_phone_number").value;
  if (recipPhoneNumber != "") {
    document.getElementById("recipient_phone_number").value = stripCharsInBag(recipPhoneNumber, phoneNumberDelimiters);
  }
  if (altPhoneNumber != "") {
    document.getElementById("alt_contact_phone_number").value = stripCharsInBag(altPhoneNumber, phoneNumberDelimiters);
  }
}


/***************************************************************************************************
 * Called when the page loads, this function displays any error messages
 * generated on load or from validation failures.
 ***************************************************************************************************/
function initErrorMessages() {
  if ( containsErrors ) {

    // display errors
    var array = doDisplayServerSideValidationMessages(confirmErrors, okErrors, fieldErrors);

    // if the skip_florist_validation flag was 'y' in pageData and the user
    // elects to update florist data on order confirmation page, set flag to
    // skip florist validation and resubmit the form
    if ( array.confirmErrorReturnValue && array.confirmGoToPage != "") {
      document.getElementById("skip_florist_validation").value = (isSkipFloristValidation) ? "Y" : "N";
      document.getElementById("deliveryInfoForm").action = array.confirmGoToPage;
      callingAction = CUSTOM_ACTION;
      doRecordLock('MODIFY_ORDER', 'check', null, "deliveryInfoForm");
    }
    else if ( array.okErrorReturnValue && array.okGoToPage != "" ) {
      doRecordLock('MODIFY_ORDER', 'check', null, "deliveryInfoForm");
      document.getElementById("deliveryInfoForm").action = array.okGoToPage;
      document.getElementById("deliveryInfoForm").submit();
    }
  }
}


/***************************************************************************************************
* Disables the various navigation keyboard commands.
***************************************************************************************************/
function udiBackKeyHandler()
{
   // backspace
   if (window.event && window.event.keyCode == 8){
      var formElement = false;

      for(i = 0; i < document.forms['deliveryInfoForm'].elements.length; i++){
         if(document.forms['deliveryInfoForm'].elements[i].name == document.activeElement.name){
            formElement = true;
            break;
         }
      }
      var searchBox = document.getElementById('numberEntry');
      if(searchBox != null){
        if(document.getElementById('numberEntry').id == document.activeElement.id){
          formElement = true;
        }
      }

      if(formElement == false || (document.activeElement.type != "text" && document.activeElement.type != "textarea" && document.activeElement.type != "password")){
         window.event.cancelBubble = true;
         window.event.returnValue = false;
         return false;
      }
   }

   // F5 and F11
   if (window.event.keyCode == 122 || window.event.keyCode == 116){
      window.event.keyCode = 0;
      event.returnValue = false;
      return false;
   }

   // Alt + Left Arrow
   if(window.event.altLeft){
      window.event.returnValue = false;
      return false;
   }

   // Alt + Right Arrow
   if(window.event.altKey){
      window.event.returnValue = false;
      return false;
   }
}


function validateStateByZipCode(){
	var returnVal = false;
	
	var zipCode = document.getElementById('recipient_zip_code').value;
	var state = document.getElementById('recipient_state').value;
	
	if(document.getElementById('recipient_country').value != 'US'){
		return true;
	}
	
	if(state == null || state == undefined || state == ''){
		return true;
	}
	
	if(zipCode == null || zipCode == undefined || zipCode == ''){
		return true;
	}
	var requestURL = "validateAjax.do?"
		 	+ "method=validateState"
		 	+"&recipient_state=" + state
		    + "&recip_zip_code=" + zipCode
		    +"&recipient_city=" + ''
		    + "&securitytoken=" + document.getElementById("securitytoken").value
		    + "&context=" + document.getElementById("context").value;
	 
	var data = getSynchronousResponse(requestURL);
	if(eval(data.responseText) == null || eval(data.responseText) == undefined){
	      window.location = "http://" + window.location.host + "/secadmin";
	      return;
	 }else{
	      var outValue = eval(data.responseText);
	      
	      if(outValue == 'YES'){
	    	  returnVal = true;
	      }else{
	    	  returnVal = false;
	      }
	 }
	return returnVal;
}

function validateGiftCardSourceCode()
{
	var sourceCode = document.getElementById('page_source_code').value;
	if(isGDOrder){
		//check if the given source code is valid for gift card.
		var requestURL = "validateAjax.do?"
		 	+ "method=validateGDSrcCd"
		 	+"&source_code=" + sourceCode
		    + "&securitytoken=" + document.getElementById("securitytoken").value
		    + "&context=" + document.getElementById("context").value;
		jQuery.ajax(
			     {     
			        type: "POST",     
			        url: requestURL,     
			        dataType: "json",        
			        contentType: "application/json; charset=utf-8",     
			        success: gdSrcCdCallback,
			        error: function(xhr){ 
			        	alert(xhr.statusText);
			        	window.location = "http://" + window.location.host + "/secadmin";
			  	      	return;
			        }                 
			    }); 
	}		
}


function gdSrcCdCallback(data)
{
  if(data == 'YES'){
	  //Continue update
	  doContinueUpdateActionNoSrcCheck();
  }else{
	  //Display warning message dialog with yes/no buttons
	  if ( doExitPageAction(WARNING_GD_SOURCE_CODE) ) {
		  doContinueUpdateActionNoSrcCheck();
	  }
  }
	
}

