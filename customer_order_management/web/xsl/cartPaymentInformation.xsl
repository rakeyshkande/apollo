<!DOCTYPE ACDemo[
	<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="securityanddata.xsl"/>
	<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>
	<xsl:key name="security" match="/root/security/data" use="name"/>
		<!-- decimal format used when a value is not given -->
<xsl:decimal-format NaN="0.00" name="notANumber"/>
<xsl:decimal-format NaN="(0.00)"  name="noRefund"/>
<xsl:output method="html"/>
     <xsl:template match="/root">
     <html>
     	<title>FTD - Cart Payment Information</title>
		<head>
			<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
			<script type="text/javascript">
				<![CDATA[
					function doCloseAction(){
						top.close();
					}
				]]>
			</script>
		</head>
		<body>
    <xsl:call-template name="securityanddata"/>
    <xsl:call-template name="decisionResultData"/>
	<table border="0" align="center" cellpadding="1" width="98%" cellspacing="1" class="mainTable">
		<tr>
			<td class="banner">
				Payment Information
			</td>
		</tr>
			<tr>
				<td>
					<table border="0"  width="100%" class="innerTable" cellpadding="0" cellspacing="0">
					  <tr>
						<td>
							<table border="0" cellpadding="0" cellspacing="0" width="100%">
								<tr style="text-align:left;">
									<td width="11%">&nbsp;</td>
									<td class="Label" width="8%">Trans&nbsp;ID</td>
                  <td class="Label" width="13%">Payment&nbsp;Type&nbsp;</td>
									<td class="Label" width="12%">Card Number</td>
									<td class="Label" width="13%">Authorization&nbsp;#</td>
                  <td class="Label" width="5%">AVS</td>
                  <td class="Label" width="5%">CSC</td>
									<td class="Label" width="14%">Transaction&nbsp;Date</td>
									<td class="Label" width="9%">Charged</td>
									<td class="Label" width="10%">Billed</td>
								</tr>
								<tr >
									<td colspan="10">
									  <div id="cartPaymentDiv"  style="overflow:auto; width:100%;height:120px; padding:0px; margin:0px;word-break:break-all;">
                      <table border="0" id="cartPayment" cellpadding="0" cellspacing="0" width="100%">

<!-- Payment Indicator = Original -->
                        <xsl:for-each select="CART_PAYMENTS/CART_PAYMENT/payment_indicator[contains(.,'O')]">
                          <tr>
                            <td align="left" style="font:bold" width="11%" ><xsl:value-of select="text()"/></td>
                            <td width="8%"><xsl:value-of select="../payment_transaction_id"/></td>
                            <td width="13%">
                              <table>
                                <tr>
                                  <td><xsl:value-of select="../payment_type"/></td>
                                  <td>&nbsp;</td>
                                </tr>
                              </table>
                            </td>
                            <td width="12%">
															<xsl:choose>
																<xsl:when test="../payment_type = 'GC' or ../payment_type = 'IN' or ../payment_type = 'NC' or ../payment_type = 'PP' or ../payment_type = 'BM' or ../payment_type = 'UA'">
																	<xsl:value-of select="../card_number"/>
																</xsl:when>
																<xsl:when test="../payment_type = 'GD'">
																	***************<xsl:value-of select="../card_number"/>
																</xsl:when>
																<xsl:otherwise>
																	************<xsl:value-of select="../card_number"/>
																</xsl:otherwise>
															</xsl:choose>
														</td>
                            <td width="13%"><xsl:value-of select="../auth_number"/></td>
                            <td width="5%"><xsl:value-of select="../avs_code"/></td>
                            <td width="5%"><xsl:value-of select="../csc_validated_flag"/></td>
                            <td width="14%">
                              <xsl:call-template name="formatDate">
                                <xsl:with-param name="dateValue" select="../created_on"/>
                              </xsl:call-template>
                            </td>
                            <td width="9%">
                              <xsl:value-of select="format-number(../credit_amount, '#,###,##0.00;(#,###,##0.00)','notANumber')"/>
                            </td>
                            <td width="10%"><xsl:value-of select="../bill_date"/></td>
                          </tr>

                          <xsl:if test="../display_credit_debit_ind = 'Y'">
                            <tr>
                              <td class="Label">Refund</td>
                              <td>&nbsp;</td>
                              <td colspan="2">
                                <table>
                                  <tr>
                                    <td><xsl:value-of select="../payment_type"/></td>
                                    <td style="color:#FF0000;">
                                      <xsl:value-of select="format-number(../child_refund_amount, '(#,###,##0.00)','noRefund')"/>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                              <td colspan="6">&nbsp;</td>
                            </tr>
                            <tr>
                              <td class="Label">Add Bill</td>
                              <td>&nbsp;</td>
                              <td colspan="2">
                                <table>
                                  <tr>
                                    <td><xsl:value-of select="../payment_type"/></td>
                                    <td>
                                      <xsl:value-of select="format-number(../child_payment_amount, '#,###,##0.00;(#,###,##0.00)','notANumber')"/>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                              <td colspan="6">&nbsp;</td>
                            </tr>
                          </xsl:if>
                        </xsl:for-each>

<!-- Payment Indicator = Add Bill -->
                        <xsl:for-each select="CART_PAYMENTS/CART_PAYMENT/payment_indicator[contains(.,'Add')]">
                          <tr >
                            <td align="left" style="font:bold;"><xsl:value-of select="text()"/></td>
                            <td><xsl:value-of select="../payment_transaction_id"/></td>
                            <td>
                              <table>
                                <tr>
                                  <td><xsl:value-of select="../payment_type"/></td>
                                  <td>&nbsp;</td>
                                </tr>
                              </table>
                            </td>
														<td>
															<xsl:choose>
																<xsl:when test="../payment_type = 'GC' or ../payment_type = 'IN' or ../payment_type = 'NC' or ../payment_type = 'PP' or ../payment_type = 'BM' or ../payment_type = 'UA'">
																	<xsl:value-of select="../card_number"/>
																</xsl:when>
																<xsl:when test="../payment_type = 'GD'">
																	***************<xsl:value-of select="../card_number"/>
																</xsl:when>
																<xsl:otherwise>
																	************<xsl:value-of select="../card_number"/>
																</xsl:otherwise>
															</xsl:choose>
														</td>
                            <td><xsl:value-of select="../auth_number"/></td>
                            <td><xsl:value-of select="../avs_code"/></td>
                            <td><xsl:value-of select="../csc_validated_flag"/></td>
                            <td>
                               <xsl:call-template name="formatDate">
                                 <xsl:with-param name="dateValue" select="../created_on"/>
                               </xsl:call-template>
                             </td>
                            <td>
                              <xsl:value-of select="format-number(../credit_amount, '#,###,##0.00;(#,###,##0.00)','notANumber')"/>
                            </td>
                            <td><xsl:value-of select="../bill_date"/></td>
                          </tr>
                          <xsl:if test="../display_credit_debit_ind = 'Y'">
                            <tr>
                              <td class="Label">Refund</td>
                              <td>&nbsp;</td>
                              <td colspan="2">
                                <table>
                                  <tr>
                                    <td><xsl:value-of select="../payment_type"/></td>
                                    <td style="color:#FF0000;">
                                      <xsl:value-of select="format-number(../child_refund_amount, '(#,###,##0.00)','noRefund')"/>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                              <td colspan="6">&nbsp;</td>
                            </tr>
                            <tr>
                              <td class="Label">Add Bill</td>
                              <td>&nbsp;</td>
                              <td colspan="2">
                                <table>
                                  <tr>
                                    <td><xsl:value-of select="../payment_type"/></td>
                                    <td>
                                      <xsl:value-of select="format-number(../child_payment_amount, '#,###,##0.00;(#,###,##0.00)','notANumber')"/>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                              <td colspan="6">&nbsp;</td>
                            </tr>
                          </xsl:if>
                        </xsl:for-each>
<!-- Payment Indicator = Refund -->
                        <xsl:for-each select="CART_PAYMENTS/CART_PAYMENT/payment_indicator[contains(.,'Ref')]">
                          <tr >
                            <td align="left" style="font:bold;" ><xsl:value-of select="text()"/></td>
                            <td><xsl:value-of select="../payment_transaction_id"/></td>
                            <td>
                              <table>
                                <tr>
                                  <td><xsl:value-of select="../payment_type"/></td>
                                  <td>&nbsp;</td>
                                </tr>
                              </table>
                            </td>
														<td>
															<xsl:choose>
																<xsl:when test="../payment_type = 'GC' or ../payment_type = 'IN' or ../payment_type = 'NC' or ../payment_type = 'PP' or ../payment_type = 'BM' or ../payment_type = 'UA'">
																	<xsl:value-of select="../card_number"/>
																</xsl:when>
																<xsl:when test="../payment_type = 'GD'">
																	***************<xsl:value-of select="../card_number"/>
																</xsl:when>
																<xsl:otherwise>
																	************<xsl:value-of select="../card_number"/>
																</xsl:otherwise>
															</xsl:choose>
														</td>
                            <td><xsl:value-of select="../auth_number"/></td>
                            <td><xsl:value-of select="../avs_code"/></td>
                            <td><xsl:value-of select="../csc_validated_flag"/></td>
                            <td>
                              <xsl:call-template name="formatDate">
                                <xsl:with-param name="dateValue" select="../created_on"/>
                              </xsl:call-template>
                            </td>
                            <td style="color:#FF0000;">
                              <xsl:value-of select="format-number(../credit_amount, '(#,###,##0.00)','noRefund')"/>
                            </td>
                            <td><xsl:value-of select="../bill_date"/></td>
                          </tr>

                          <xsl:if test="../display_credit_debit_ind = 'Y'">
                            <tr>
                              <td class="Label">Refund</td>
                              <td>&nbsp;</td>
                              <td colspan="2">
                                <table>
                                  <tr>
                                    <td><xsl:value-of select="../payment_type"/></td>
                                    <td style="color:#FF0000;">
                                      <xsl:value-of select="format-number(../child_refund_amount, '(#,###,##0.00)','noRefund')"/>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                              <td colspan="6">&nbsp;</td>
                            </tr>
                            <tr>
                              <td class="Label">Add Bill</td>
                              <td>&nbsp;</td>
                              <td colspan="2">
                                <table>
                                  <tr>
                                    <td><xsl:value-of select="../payment_type"/></td>
                                    <td>
                                      <xsl:value-of select="format-number(../child_payment_amount, '#,###,##0.00;(#,###,##0.00)','notANumber')"/>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                              <td colspan="6">&nbsp;</td>
                            </tr>
                          </xsl:if>
                        </xsl:for-each>
                      </table>
									  </div>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>

	</table>
		<br/>
		<table border="0" width="98%" align="center" cellpadding="0" cellspacing="0">
			<tr>
				<td align="center">
						<button type="button" class="BlueButton" accesskey="C" onclick="javascript:doCloseAction();">(C)lose</button>
				</td>
			</tr>
		</table>

		</body>
     </html>


	</xsl:template>

 <xsl:template name="formatDate">
  <xsl:param name="dateValue"/>
  <xsl:param name="tempDate" select="substring($dateValue,1,10)"/>
   <xsl:value-of select="concat(substring($tempDate,6,2),'/',substring($tempDate,9,2),'/',substring($dateValue,1,4))"/>
</xsl:template>



</xsl:stylesheet>