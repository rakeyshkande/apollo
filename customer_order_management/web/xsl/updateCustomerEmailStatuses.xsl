<!DOCTYPE ACDemo[
  <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="header.xsl"/>
<xsl:import href="footer.xsl"/>
<xsl:import href="securityanddata.xsl"/>
<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>

<xsl:output doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"/>
<xsl:output indent="yes" method="html"/>
<xsl:key name="pageData" match="/root/pageData/data" use="name"/>
<xsl:key name="security" match="/root/security/data" use="name"/>

<!--
  This variable is used to indicate which 'mode' the page is currently in.
  We can be in ADD or UPDATE mode. Certain features are enabled/disabled
  based on the mode we are currently in.
-->
<xsl:variable name="isUpdateMode" select="boolean(key('pageData','page_action')/value = 'update')"/>
<xsl:variable name="brandCount" select="count(/root/EMAILS_INFO/EMAIL_INFO)"/>

<xsl:template match="/root">
<html>
  <head>
  <title>FTD - View all Email Addresses</title>
  <base target="_self"/>
  <script type="text/javascript" src="js/tabIndexMaintenance.js"/>
  <script type="text/javascript" src="js/util.js"/>
  <script type="text/javascript" src="js/FormChek.js"/>
  <script type="text/javascript">
  var returnParms = new Array('uc_customer_id','uc_master_order_number',
                              'uc_order_guid','display_page');

  var isUpdateMode = '<xsl:value-of select="$isUpdateMode"/>';
  var brandCount = '<xsl:value-of select="$brandCount"/>';
  var pageAction;
  var dataChanged = false;
  var _sub = 'Subscribe';
  var _unsub = 'Unsubscribe';
  <!-- apply tab indexes-->

<![CDATA[

function init(){
  setNavigationHandlers();
  addDefaultListenersArray(new Array('uce_email_address'));
  setUpdateCustomerEmailIndexes();
  //setAddUpdateTabIndex(isUpdateMode);
  if(isUpdateMode == 'false')
    document.getElementById('uce_email_address2').focus();

}
/*******************************************************************************
*  setUpdateCustomerEmailIndexes
*  Sets tabs for Update Customer Email Statues
*  See tabIndexMaintenance.js
*******************************************************************************/
function setUpdateCustomerEmailIndexes(){
  untabAllElements();
  var begIndex = findBeginIndex();
  var tabArray1 = new Array('uce_email_address2');
  begIndex = setTabIndexes(tabArray1, begIndex);
  var tabArray2 = $('#cb');
  for(var x = 0; x < tabArray2.length; x++)
  {
      var element1 = tabArray2[x];
      if ((element1 != null))
      {
        element1.tabIndex = begIndex;
        begIndex++;
      }
   }
  var tabArray3 = new Array('submitChanges', 'close','backButtonHeader','printer');
  begIndex = setTabIndexes(tabArray3, begIndex);
  return begIndex;
}

function doCloseAction(){
  var ret = new Array('EXIT');
  top.returnValue = ret;
  top.close();
}
/*
 Changes submitted successfully
 close the window
*/
function doCompleteSubmit(){
  top.close();
}

/*************************************************************************************
* Perorms the Back button functionality
**************************************************************************************/
function doBackAction()
{
  doCloseAction();
}


function doSubmitChangesAction(){
  if(!dataChanged) {
    alert('Customer email statuses were not updated, no changes were made.');
    return;
  }

  if(isUpdateMode != 'true')
  {
    var emailAddress = stripWhitespace(document.getElementById('uce_email_address2').value);
    if ( emailAddress.length > 55 )
    {
      alert('Input exceeds field length of 55 characters');
      return;
    }
    if (!isEmail(emailAddress))
    {
      alert('You must enter a valid email address');
      return;
    }
  }


  pageAction = 'checkSubmit';
  var url = 'updateCustomerEmail.do?action=check_customer_email';
  var frame = document.getElementById('updateEmailsFrame');
  url += retrieveParams() + getSecurityParams(false);
	url += "&submittedViaIFrame=y";

  url += "&uce_email_address=";

  if(isUpdateMode == 'true')
  {
    url += document.getElementById('uce_email_address1').value;
    url += '&page_action=update';
  }
  else
  {
    url += document.getElementById('uce_email_address2').value;
    url += '&page_action=add';
  }

  frame.src = url;

}

function doError()
{
  var waitDiv = document.getElementById("waitDiv").style;
  var errorDiv = document.getElementById("errorDiv").style;
  waitDiv.display = "none";
	errorDiv.display = "block";

}


function doSubmitAction(){

 pageAction = 'checkComplete';
  var url;
  url = 'updateCustomerEmail.do?action=save_customer_emails' +
     retrieveParams() + getSecurityParams(false);
	url += "&submittedViaIFrame=y";

  url += "&uce_email_address=";
  if(isUpdateMode == 'true')
  {
    url += document.getElementById('uce_email_address1').value;
    url += '&page_action=update';
  }
  else
  {
    url += document.getElementById('uce_email_address2').value;
    url += '&page_action=add';
  }

	showWaitMessage("innerContent", "wait", "Please wait validating input");

  var frame = document.getElementById('updateEmailsFrame');
  frame.src = url;
}


/*
	Used to trip the value of the dataChanged flag
*/
  function doCBChanged()
  {
    dataChanged = true;
  }



/*
  Used to catch the event
*/
	function checkKey()
	{
		var tempKey = window.event.keyCode;

		if (tempKey == 13)
			doSubmitChangesAction();
	}



/*
  Used to attache parameters to
  the url for processing
*/
function retrieveParams(){
 var url = '';
  /*return checkbox parms*/
  var cb;

  for(var i = 1; i <= brandCount; i++)
  {
    cb_unsub = $('input[name="cb_unsub_'+i+'"]')[0];
    cb_sub = $('input[name="cb_sub_'+i+'"]')[0];
    url += '&uce_subscribe_status_' + i + '=';
    if(cb_unsub.checked){
    url += 'Unsubscribe';
    }
    if(cb_sub.checked){
    url += 'Subscribe';
    }
     url = attachCompanyAndEmailParms(url,i);
    }
   url += '&uce_company_id_count=' + brandCount;

  /*
    return the page's hidden fields
  */
  for(var m = 0; m < returnParms.length; m++){
    var ele = document.getElementById(returnParms[m]);
    url += '&' + ele.name + '=' + ele.value;
  }
  return url;
}
/*
  Used to attach the company id and
  email id for the Sub/Unsub choice of the user
*/
function attachCompanyAndEmailParms(url,pos){
  var emailId = document.getElementById('uce_email_id_' + pos);
  var companyId = document.getElementById('uce_company_id_' + pos);
  var companyName = document.getElementById('uce_company_name_' + pos); //lakhani
  url += '&' + emailId.name + '=' + emailId.value +
    '&' + companyId.name + '=' + companyId.value  +
    '&' + companyName.name + '=' + companyName.value;

  return url;
}

/*
  This function ensures that only one check box is
  selected at time.
*/
function doBoxValidation(obj,pos){
  if(!isUpdateMode){
  if(obj.checked && (obj.name.substr(0,8) == 'cb_unsub')){
    var otherObj = $('input[name="cb_sub_'+pos+'"]')[0];
    otherObj.checked = false;
  }else{
    var otherObj = $('input[name="cb_unsub_'+pos+'"]')[0];
    otherObj.checked = false;
  }
  }else{
    if(!obj.checked && (obj.name.substr(0,8) == 'cb_unsub')){
      var otherObj = $('input[name="cb_sub_'+pos+'"]')[0];
    otherObj.checked = true;
    }
    if(!obj.checked &&  (obj.name.substr(0,6) == 'cb_sub')){
      var otherObj = $('input[name="cb_unsub_'+pos+'"]')[0];
      if(otherObj.disabled)
        return;
      otherObj.checked = true;
    }
    if(obj.checked && (obj.name.substr(0,8) == 'cb_unsub')){
      var otherObj = $('input[name="cb_sub_'+pos+'"]')[0];
    otherObj.checked = false;
    }
    if(obj.checked && (obj.name.substr(0,6) == 'cb_sub')){
      var otherObj = $('input[name="cb_unsub_'+pos+'"]')[0];
    otherObj.checked = false;
    }
  }
}


  ]]>
  </script>
  <link rel="stylesheet" href="css/ftd.css"/>
  <style type="text/css">
     /* Scrolling table overrides for the order/shopping cart table */
      div.tableContainer table {
        width: 97%;
      }
      div#messageContainer {
        height: 130px;
        width: 100%;
      }
  </style>
  </head>
  <body onload="javascript:init();" onkeypress="checkKey();">
  <xsl:choose>
    <xsl:when test="$isUpdateMode">
        <xsl:call-template name="addHeader">
        <xsl:with-param name="header" select="'Update Email Status'"/>
      </xsl:call-template>
    </xsl:when>
    <xsl:otherwise>
      <xsl:call-template name="addHeader">
        <xsl:with-param name="header" select="'Add Email Address'"/>
      </xsl:call-template>
    </xsl:otherwise>
  </xsl:choose>


		<xsl:call-template name="securityanddata"/>
		<xsl:call-template name="decisionResultData"/>

		<input type="hidden" name="uc_customer_id" id="uc_customer_id" value="{key('pageData','customer_id')/value}" />
		<input type="hidden" name="uc_master_order_number" id="uc_master_order_number" value="{key('pageData','master_order_number')/value}" />
		<input type="hidden" name="uc_order_guid" id="uc_order_guid" value="{key('pageData','order_guid')/value}" />
		<input type="hidden" name="display_page" id="display_page" value="{key('pageData','display_page')/value}" />

		<table class="mainTable" border="0" align="center" cellpadding="0" cellspacing="1" width="98%">
			<tr>
				<td>
					<div id="innerContent" >
						<table class="innerTable" border="0" cellpadding="0" cellspacing="1" width="100%" >
							<tr>
								<td>
									<!-- table to hold page fields -->
									<table border="0" cellpadding="0" cellspacing="0" width="100%" >
										<tr>
											<td>
												<div style="float:left;">
													<span class="Label" style="padding-left:5em;">Email Address:</span>&nbsp;
													<xsl:choose>
														<xsl:when test="$isUpdateMode">
															<xsl:value-of select="key('pageData','uce_email_address')/value" />
															<input type="hidden" name="uce_email_address1" id="uce_email_address1" size="40" value="{key('pageData','uce_email_address')/value}" />
														</xsl:when>
														<xsl:otherwise>
															<input type="text" name="uce_email_address2" onkeypress="javascript:doCBChanged();" id="uce_email_address2" size="40" value="{key('pageData','uce_email_address')/value}" />
														</xsl:otherwise>
													</xsl:choose>
												</div>
												<xsl:if test="$isUpdateMode and EMAILS_INFO/EMAIL_INFO[1]/updated_on != ''">
													<div style="padding-left:10em;float:right;padding-right:5em;">
														<span class="Label">Modified Date:</span>&nbsp;
														<xsl:call-template name="modDate">
															<xsl:with-param name="dateValue" select="EMAILS_INFO/EMAIL_INFO[1]/updated_on"/>
														</xsl:call-template>
													</div>
												</xsl:if>
											</td>
										</tr>
									</table><br /><br />
									<!-- This table contains the fields of the page -->
									<div class="tableContainer" id="messageContainer" >
										<table width="100%" class="scrollTable" border="0" cellpadding="0"  align="center" cellspacing="2">
											<thead class="fixedHeader">
												<tr style="text-align:center">
													<td width="10%">&nbsp;</td>
													<xsl:if test="$isUpdateMode"><td class="Label" width="30%">Current Status</td></xsl:if>
													<td class="Label" width="30%">Subscribe</td>
													<td class="Label" width="30%">Unsubscribe</td>
												</tr>
											</thead>
											<tbody class="scrollContent">
												<xsl:for-each select="EMAILS_INFO/EMAIL_INFO">
													<tr>
														<td id="nav_{position()}">
															<input type="hidden" name="uce_email_id_{position()}" id="uce_email_id_{position()}" value="{email_id}"/>
														</td>
														<input type="hidden" name="uce_company_name_{position()}" id="uce_company_name_{position()}" value="{company_name}"/>
														<input type="hidden" name="uce_company_id_{position()}"   id="uce_company_id_{position()}"   value="{company_id}"/>
														<xsl:if test="$isUpdateMode">
															<td>
																<xsl:choose>
																	<xsl:when test="subscribe_status = ''">
																		<xsl:value-of select="'Never Opted'"/>
																	</xsl:when>
																	<xsl:otherwise>
																		<xsl:value-of select="subscribe_status"/>
																	</xsl:otherwise>
																</xsl:choose>
															</td>
														</xsl:if>
														<td>
															<input type="checkbox" tabindex="{position() + 1}" name="cb_sub_{position()}" id="cb" onchange="javascript:doCBChanged();" onclick="javascript:doBoxValidation(this,{position()});">
																<xsl:if test="subscribe_status != '' and subscribe_status = 'Subscribe'">
																	<xsl:attribute name="checked">checked</xsl:attribute>
																</xsl:if>
															</input>
														</td>
														<td>
															<input type="checkbox" tabindex="{position() + 2}" name="cb_unsub_{position()}" id="cb" onchange="javascript:doCBChanged();" onclick="javascript:doBoxValidation(this,{position()});" >
																<xsl:if test="not($isUpdateMode)">
																	<xsl:attribute name="checked">checked</xsl:attribute>
																</xsl:if>
																<xsl:if test="subscribe_status = '' and $isUpdateMode">
																	<xsl:attribute name="disabled">true</xsl:attribute>
																</xsl:if>
																<xsl:if test="subscribe_status != '' and subscribe_status = 'Unsubscribe'">
																	<xsl:attribute name="checked">checked</xsl:attribute>
																</xsl:if>
															</input>
														</td>
													</tr>
												</xsl:for-each>
											</tbody>
										</table>
									</div>
									<table border="0" align="center" cellpadding="0" cellspacing="0" width="98%">
										<tr>
											<td align="center">
												<button type="button" class="BlueButton" id="submitChanges" accesskey="G" onclick="javascript:doSubmitChangesAction();">Submit Chan(g)es</button>&nbsp;
												<button type="button" class="BlueButton" id="close" accesskey="C" onclick="javascript:doCloseAction();">(C)lose</button>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</div>
				</td>
			</tr>
		</table>

		<div id="waitDiv" style="display:none">
			<table id="waitTable" class="mainTable" width="98%" height="195px" align="center" cellpadding="0" cellspacing="1" >
				<tr>
					<td width="100%">
						<table class="innerTable" style="width:100%;height:195px" cellpadding="30" cellspacing="1">
							<tr>
								<td>
									<table width="100%" cellspacing="0" cellpadding="0" border="0">
										<tr>
											<td id="waitMessage" align="right" width="60%" class="WaitMessage"></td>
											<td id="waitTD"  width="40%" class="WaitMessage"></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div><!-- TO remove put and end-comment marker here -->



		<div id="errorDiv" style="display:none">
			<center>
					<!-- Main table -->
				<table width="98%" border="3" bordercolor="#006699" cellspacing="1" cellpadding="1">
					<tr><td align="center" class="ErrorMessage">An unexpected system error has occured.</td></tr>
				</table>
			</center>
		</div><!-- TO remove put and end-comment marker here -->

		<xsl:call-template name="footer"/>
		<iframe name="updateEmailsFrame" id="updateEmailsFrame" width="0px" height="0px" border="0"/>




 </body>
</html>
</xsl:template>
<xsl:template name="addHeader">
<xsl:param name="header"/>
  <xsl:call-template name="header">
    <xsl:with-param name="showPrinter" select="true()"/>
    <xsl:with-param name="headerName" select="$header"/>
    <xsl:with-param name="showTime" select="true()"/>
    <xsl:with-param name="showBackButton" select="true()"/>
    <xsl:with-param name="showCSRIDs" select="true()"/>
      <xsl:with-param name="dnisNumber" select="$call_dnis"/>
      <xsl:with-param name="cservNumber" select="$call_cs_number"/>
      <xsl:with-param name="indicator" select="$call_type_flag"/>
      <xsl:with-param name="brandName" select="$call_brand_name"/>
      <xsl:with-param name="backButtonLabel" select="'(B)ack'"/>
      <xsl:with-param name="cdispEnabledFlag" select="$cdisp_enabledFlag"/>
  </xsl:call-template>
</xsl:template>

<xsl:template name="modDate">
  <xsl:param name="dateValue"/>
  <xsl:param name="hackDate" select="substring($dateValue,1,10)"/>
   <xsl:value-of select="concat(substring($hackDate,6,2),'/',substring($hackDate,9,2),'/',substring($dateValue,1,4))"/>
</xsl:template>
</xsl:stylesheet>