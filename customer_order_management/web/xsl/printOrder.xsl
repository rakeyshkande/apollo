<!DOCTYPE ACDemo[
	<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN="0.00" name="notANumber"/>
<xsl:variable name="PRINT_ORDER" select="root/PRINT_ORDERS/PRINT_ORDER"/>
<xsl:variable name="PRINT_CUSTOMER" select="root/PRINT_CUSTOMERS/PRINT_CUSTOMER"/>
<xsl:output method="html" indent="yes"/>
<xsl:template match="/">
<html>
	<head>
		<title></title>
		<script type="text/javascript">
function init(){
	this.focus();
	self.print();
}

function closeMe(){
	window.close();
}

</script>
<link rel="stylesheet" href="css/ftd.css"/>
<link rel="stylesheet" type="text/css" media="print" href="print.css"/>
<script type="text/javascript" src="js/FormChek.js"/>
<style type="text/css">
	td.Label{
		vertical-align:top;
		text-align:left;
		font-size:9pt;
	}
	span.Label{
		vertical-align:top;
		text-align:left;
		font-size:9pt;
	}
	td {
		padding-left:.8em;
		vertical-align:top;
		font-size:8pt;
	}
	body{
		background:white;
		color:black;
		font-family: arial,helvetica,sans-serif;
	}

	#orderBills td {
		text-align:left;
	}
	#paymentInformation td{
		text-align:left;
	}
	.printedBanner{
		text-align:left;
		padding-left:0px;
		font-size:11pt;
		font-weight:bold;
		border-bottom: 1px solid gray;	}

	#orderHeader{
		font-size:11pt;

	}
	#sourceHeader{
		font-size:11pt;
		vertical-align:bottom;
	}
</style>
	</head>
	<body onload="javascript:init();">
	<table width="100%" border="0" cellpadding="0" cellspacing="2" align="center" style="word-break:break-all;">
	<tr>
		<td colspan="8">
			<table width="100%" cellpadding="0" cellspacing="0" border="0" style="word-break:break-all;">
				<tr>
					<td class="Label" width="12%">
						<span id="orderHeader">Order #</span>&nbsp;&nbsp;
					</td>
					<td width="30%" style="vertical-align:bottom;"><xsl:value-of select="$PRINT_ORDER/external_order_number"/></td>
					<td class="Label" width="19%">
						<span id="sourceHeader">Source Code #</span>&nbsp;&nbsp;

					</td>
					<td colspan="4" style="vertical-align:bottom;">
						<xsl:value-of select="$PRINT_ORDER/source_code"/>
					</td>
				</tr>
			</table>
			<br />
		</td>
	</tr>
	<tr>
		<tr>
			<td colspan="8" class="printedBanner" >Customer Information:</td>
		</tr>
		<tr>
		<td colspan="8" style="padding-left:0px;">
			<table border="0" width="100%" cellpadding="0" cellspacing="0">
				<tr>
					<td width="26%">
							<span class="Label">Name:</span><br />
							<xsl:value-of select="$PRINT_CUSTOMER/first_name"/><br /><xsl:value-of select="$PRINT_CUSTOMER/last_name"/>
					</td>
					<td width="30%">
						<span class="Label">Home Phone:</span><br />

						<xsl:if test="root/PRINT_CUSTOMER_PHONES/PRINT_CUSTOMER_PHONE/phone_type[. = 'Evening']/../phone_number != ''">
							<script type="text/javascript">
							if(isUSPhoneNumber('<xsl:value-of select="root/PRINT_CUSTOMER_PHONES/PRINT_CUSTOMER_PHONE//phone_type[. = 'Evening']/../phone_number"/>')){
								document.write(reformatUSPhone('<xsl:value-of select="root/PRINT_CUSTOMER_PHONES/PRINT_CUSTOMER_PHONE//phone_type[. = 'Evening']/../phone_number"/>'));
							}else{
								document.write('<xsl:value-of select="root/PRINT_CUSTOMER_PHONES/PRINT_CUSTOMER_PHONE//phone_type[. = 'Evening']/../phone_number"/>');
							}
							</script>
						 </xsl:if>
					</td>
					<td >
						<span class="Label">Work Phone:</span><br />
						<xsl:if test="root/PRINT_CUSTOMER_PHONES/PRINT_CUSTOMER_PHONE/phone_type[. = 'Day']/../phone_number != ''">
							<script type="text/javascript">
							if(isUSPhoneNumber('<xsl:value-of select="root/PRINT_CUSTOMER_PHONES/PRINT_CUSTOMER_PHONE//phone_type[. = 'Day']/../phone_number"/>')){
								document.write(reformatUSPhone('<xsl:value-of select="root/PRINT_CUSTOMER_PHONES/PRINT_CUSTOMER_PHONE//phone_type[. = 'Evening']/../phone_number"/>'));
							}else{
								document.write('<xsl:value-of select="root/PRINT_CUSTOMER_PHONES/PRINT_CUSTOMER_PHONE//phone_type[. = 'Day']/../phone_number"/>');
							}
							</script>
						 </xsl:if>
					</td>
				</tr>
				<tr>
					<td>
						<span class="Label">Address 1:</span><br />
						<xsl:value-of select="$PRINT_CUSTOMER/address_1"/>
					</td>
					<td>
						<span class="Label">Address 2:</span><br />
						<xsl:value-of select="$PRINT_CUSTOMER/address_2"/>
					</td>
					<td>
						<span class="Label">City,State</span><br />
						<xsl:choose>
							<xsl:when test="$PRINT_CUSTOMER/state != ''">
								<xsl:value-of select="$PRINT_CUSTOMER/city"/>, <xsl:value-of select="$PRINT_CUSTOMER/state"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="$PRINT_CUSTOMER/city"/>
							</xsl:otherwise>
						</xsl:choose>
					</td>
				</tr>
				<tr>
					<td>
						<span class="Label">Zip Code:</span><br />
						<xsl:value-of select="$PRINT_CUSTOMER/zip_code"/>
					</td>
					<td>
						<span class="Label">Membership Id:</span><br />
						<xsl:value-of select="root/PRINT_MEMBERSHIPS/PRINT_MEMBERSHIP/membership_number"/>
					</td>
				</tr>
			</table>
		</td>
		</tr><tr><td><br /></td></tr>
	</tr>
	<tr>
		<td colspan="8" class="printedBanner">
			 Recipient Information:
		</td>
	</tr>
	<tr>
		<td width="24%">
		<span class="Label">Name:</span><br />

			<xsl:value-of select="$PRINT_ORDER/first_name"/><br/>
			<xsl:value-of select="$PRINT_ORDER/last_name"/>

		</td>
		<td width="30%">
			<span class="Label">Address 1:</span><br />
			<xsl:value-of select="$PRINT_ORDER/address_1"/>
		</td>
		<td width="30%">
			<span class="Label">Phone Number:</span><br />
			<xsl:if test="$PRINT_ORDER/recipient_phone_number != ''">
				<script type="text/javascript">
				if(isUSPhoneNumber('<xsl:value-of select="$PRINT_ORDER/recipient_phone_number"/>')){
					document.write(reformatUSPhone('<xsl:value-of select="$PRINT_ORDER/recipient_phone_number"/>'));
				}else{
					document.write('<xsl:value-of select="$PRINT_ORDER/recipient_phone_number"/>');
				}
				</script>
			 </xsl:if>
		</td>

		<td width="16%" colspan="5">
			<span class="Label">Ext:</span><br />
				<xsl:value-of select="$PRINT_ORDER/recipient_extension"/>
		</td>
	</tr>
	<tr>
		<td >
			<span class="Label">Delivery Location:</span><br />
				<xsl:value-of select="$PRINT_ORDER/address_type_description"/>

		</td>

		<td >
			<span class="Label">Address 2:</span><br />
				<xsl:value-of select="$PRINT_ORDER/address_2"/>
		</td>
		<td colspan="6">
			<span class="Label">Recipient Number:</span><br />
			<xsl:value-of select="$PRINT_ORDER/recipient_id"/>
		</td>
	</tr>
	<tr>
		<td >
			<span class="Label">Business Name:</span><br />
			<xsl:value-of select="$PRINT_ORDER/business_name"/>
		</td>
		<td >
			<span class="Label">City,State</span><br />
			<xsl:choose>
				<xsl:when test="$PRINT_ORDER/state != ''">
						<xsl:value-of select="$PRINT_ORDER/city"/>, <xsl:value-of select="$PRINT_ORDER/state"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$PRINT_ORDER/city"/>
				</xsl:otherwise>
			</xsl:choose>
		</td>
		<td >
			<span class="Label">Zip/Postal Code:</span><br />
				<xsl:value-of select="$PRINT_ORDER/zip_code"/>&nbsp;&nbsp;

		</td>
		<td colspan="4">
			<span class="Label">Country:</span><br />
			<xsl:value-of select="$PRINT_ORDER/country"/>
		</td>
	</tr>
	<tr>

		<td colspan="8" >
			<span class="Label">Hold Disposition</span><br />
			<xsl:value-of select="$PRINT_ORDER/order_disp_code"/>
		</td>
	</tr>
	 <tr>
	  <td><br /></td>
	</tr>
	<tr>
		<td class="printedBanner" colspan="8" >
			Product Summary:
		</td>
	</tr>
	<tr>
		<xsl:if test="$PRINT_ORDER/vendor_flag = 'Y'">
			<xsl:call-template name="addVendorSummary"/>
		</xsl:if>
		<xsl:if test="$PRINT_ORDER/vendor_flag = 'N'">
			<xsl:call-template name="addRecipientSummary"/>
		</xsl:if>
	</tr>
	 <tr>
	  <td><br /></td>
	</tr>
	<tr>
		<td class="printedBanner" colspan="8" >Totals:
		</td>
	</tr>
	<tr>
		<xsl:call-template name="printOrderBills"/>
	</tr>
	 <tr>
	  <td><br /></td>
	</tr>
	<tr>
		<td class="printedBanner" colspan="8" >Payment Information:
</td>
	</tr>
	<tr>
		<xsl:call-template name="paymentInformation"/>
	</tr><tr><td><br /></td></tr>
	<tr>
		<td colspan="8" class="printedBanner">
			<xsl:choose>
				<xsl:when test="$PRINT_ORDER/vendor_flag = 'Y'">
					 Vendor Information:
				</xsl:when>
				<xsl:otherwise>
					Florist Information:
				</xsl:otherwise>
			</xsl:choose>

		</td>
	</tr>
	<tr>
		<td colspan="8">
			<xsl:choose>
				<xsl:when test="$PRINT_ORDER/vendor_flag = 'Y'">
					<xsl:call-template name="addVendorInformation"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="addFloristInformation"/>
				</xsl:otherwise>
			</xsl:choose>
		</td>

	</tr><tr><td><br /></td></tr>
	<tr>
		<td colspan="8" class="printedBanner">
				Enclosure Card:
		</td>
	</tr>
	<tr>
		<td class="Label">Ocassion</td>
		<td  class="Label" colspan="1">Signature line</td>
		<td  class="Label" colspan="6">Gift Card Message</td>
	</tr>
	<tr>
		<td><xsl:value-of select="$PRINT_ORDER/occasion_description"/></td>
		<td><xsl:value-of select="$PRINT_ORDER/card_signature"/></td>
		<td colspan="6"><xsl:value-of select="$PRINT_ORDER/card_message"/></td>
	</tr>
		<tr><td><br /></td></tr>
	<tr>
		<td colspan="8" class="printedBanner">Miscellaneous:</td>
	</tr>
	<tr>
   <xsl:if test="$PRINT_ORDER/address_type = 'FUNERAL HOME'">
		<td class="Label">OK to Release</td>
   </xsl:if>
		<td colspan="7" class="Label">Special Instructions</td>
	</tr>
	<tr>
		<td>
      <xsl:if test="$PRINT_ORDER/address_type = 'FUNERAL HOME'">
        <xsl:choose>
          <xsl:when test="$PRINT_ORDER/release_info_indicator = 'Y'">
            <xsl:value-of select="'YES'"/>
          </xsl:when>
          <xsl:when test="$PRINT_ORDER/release_info_indicator = 'N' or $PRINT_ORDER/release_info_indicator = '' ">
            <xsl:value-of select="'NO'"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="'NO'"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:if>
		</td>
		<td colspan="7" style="padding-right:20em;">
			<xsl:value-of select="$PRINT_ORDER/special_instructions"/>
		</td>
	</tr><tr><td><br /></td></tr>
	<tr>
		<td class="printedBanner" colspan="8">Comments:</td>
	</tr>
	<tr>
		<td colspan="8">
			<xsl:if test="$PRINT_ORDER/order_comment_indicator[. = 'Y']">
				<xsl:call-template name="printComments"/>
			</xsl:if>
		</td>

	</tr>
</table>
	</body>
</html>

</xsl:template>

<!-- Vendor Summmary Information -->
<xsl:template name="addVendorSummary">
	<td colspan="8" style="padding-left:0px;">
		<table border="0" width="100%" cellpadding="0" cellspacing="0">
			<tr>
				<td width="20%">
					<span class="Label" >Delivery Date:</span><br />
					<xsl:value-of select="$PRINT_ORDER/delivery_date"/>
				</td>
				<td width="25%">
					<span class="Label">Ship Method:</span><br />
					<xsl:value-of select="$PRINT_ORDER/ship_method_desc"/>
				</td>
				<td>
					<span class="Label">Ship Date:</span>
					<br />
					<xsl:choose>
						<xsl:when test="$PRINT_ORDER/ship_date != ''">
							<xsl:call-template name="modDate">
								<xsl:with-param name="dateValue" select="$PRINT_ORDER/ship_date"/>
							</xsl:call-template>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$PRINT_ORDER/ship_date"/>
						</xsl:otherwise>
					</xsl:choose>
				</td>
				<td >
					<span class="Label">Item:</span>
					<br />
					<xsl:choose>
						<xsl:when test="$PRINT_ORDER/subcode != ''">
							<xsl:value-of select="$PRINT_ORDER/subcode"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$PRINT_ORDER/product_id"/>
						</xsl:otherwise>
					</xsl:choose>
				</td>
			</tr>
			  <tr>
					<td>
						<span class="Label">Substitution Allowed:</span><br />

							<xsl:if test="$PRINT_ORDER/substitution_indicator = 'N'">
								<xsl:value-of select="'No'"/>
							</xsl:if>
							<xsl:if test="$PRINT_ORDER/substitution_indicator = 'Y'">
								<xsl:value-of select="'Yes'"/>
							</xsl:if>

					</td>
					<td >
						<span class="Label">Color:</span><br />
						<xsl:choose>
							<xsl:when test="$PRINT_ORDER/color1_description != '' and $PRINT_ORDER/color2_description != ''">
								<xsl:value-of select="$PRINT_ORDER/color1_description"/>&nbsp;|&nbsp; <xsl:value-of select="$PRINT_ORDER/color2_description"/>
							</xsl:when>
							<xsl:when test="$PRINT_ORDER/color1_description = '' and $PRINT_ORDER/color2_description != ''">
								 <xsl:value-of select="$PRINT_ORDER/color2_description"/>
							</xsl:when>
							 <xsl:when test="$PRINT_ORDER/color1_description != '' and $PRINT_ORDER/color2_description = ''">
								 <xsl:value-of select="$PRINT_ORDER/color1_description"/>
							</xsl:when>
						</xsl:choose>
					</td>
					<td colspan="2">
						<span class="Label">Price:</span><br />
						<xsl:choose>
							<xsl:when test="$PRINT_ORDER/discount_product_price &lt; 0">
								<xsl:value-of select="'('"/>
                  <xsl:value-of select="format-number($PRINT_ORDER/discount_product_price, '#,###,##0.00;(#,###,##0.00)','notANumber')"/>
								<xsl:value-of select="')'"/>
							</xsl:when>
							<xsl:otherwise>
                  <xsl:value-of select="format-number($PRINT_ORDER/discount_product_price, '#,###,##0.00;(#,###,##0.00)','notANumber')"/>
							</xsl:otherwise>
						</xsl:choose>
					</td>
			  </tr>
			  <tr>
					<td colspan="8">
						<span class="Label">Description:</span>
						<br />
						<xsl:choose>
							<xsl:when test="$PRINT_ORDER/subcode != ''">
								<xsl:value-of select="$PRINT_ORDER/subcode_description"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="$PRINT_ORDER/product_name"/>
							</xsl:otherwise>
						</xsl:choose>
					</td>
			  </tr>
                          <tr>
					<td colspan="8">
						<span class="Label">Add-On:</span><br />
						 	<xsl:for-each select="root/PRINT_ORDER_ADDONS/PRINT_ORDER_ADDON">
						 			<xsl:if test="position() > 1">, </xsl:if>
					 					<xsl:value-of select="add_on_quantity"/> &nbsp; <xsl:value-of select="description" disable-output-escaping="yes"/>
						 	</xsl:for-each>
					</td>
			 </tr>
		</table>
	</td>
</xsl:template>

<!-- Recipient Type Product Summary -->

<xsl:template name="addRecipientSummary">

	<td colspan="8" style="padding-left:0px">
		<table border="0" cellpadding="0" cellspacing="0" width="100%">
			<tr>
			   <td width="20%">
						<span class="Label">Delivery Date:</span><br />
							<xsl:value-of select="$PRINT_ORDER/delivery_date"/>
			   </td>
				<td width="30%">
					<span class="Label">Item:</span>
					<br />
					<xsl:choose>
						<xsl:when test="$PRINT_ORDER/subcode != ''">
							<xsl:value-of select="$PRINT_ORDER/subcode"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$PRINT_ORDER/product_id"/>
						</xsl:otherwise>
					</xsl:choose>
				</td>
				<td width="20%">
					<span class="Label">Price:</span><br />
						<xsl:choose>
							<xsl:when test="$PRINT_ORDER/discount_product_price &lt; 0">
								<xsl:value-of select="'('"/>
                  <xsl:value-of select="format-number($PRINT_ORDER/discount_product_price, '#,###,##0.00;(#,###,##0.00)','notANumber')"/>
								<xsl:value-of select="')'"/>
							</xsl:when>
							<xsl:otherwise>
                  <xsl:value-of select="format-number($PRINT_ORDER/discount_product_price, '#,###,##0.00;(#,###,##0.00)','notANumber')"/>
							</xsl:otherwise>
						</xsl:choose>
				</td>
				<td colspan="5">
					<span class="Label">Substitution Allowed:</span><br />
					<xsl:if test="$PRINT_ORDER/substitution_indicator = 'N'">
							<xsl:value-of select="'No'"/>
						</xsl:if>
						<xsl:if test="$PRINT_ORDER/substitution_indicator = 'Y'">
							<xsl:value-of select="'Yes'"/>
						</xsl:if>
				</td>
			</tr>
				<tr>
					<td colspan="2">
						<span class="Label">Add-On:</span><br />

						 	<xsl:for-each select="root/PRINT_ORDER_ADDONS/PRINT_ORDER_ADDON">
						 			<xsl:if test="position() > 1">, </xsl:if>
					 					<xsl:value-of select="add_on_quantity"/> &nbsp; <xsl:value-of select="description" disable-output-escaping="yes"/>
						 	</xsl:for-each>

					</td>
					<td colspan="6">
						<span class="Label">Color:</span><br />
						<xsl:choose>
							<xsl:when test="$PRINT_ORDER/color1_description != '' and $PRINT_ORDER/color2_description != ''">
								<xsl:value-of select="$PRINT_ORDER/color1_description"/>&nbsp;|&nbsp; <xsl:value-of select="$PRINT_ORDER/color2_description"/>
							</xsl:when>
							<xsl:when test="$PRINT_ORDER/color1_description = '' and $PRINT_ORDER/color2_description != ''">
								 <xsl:value-of select="$PRINT_ORDER/color2_description"/>
							</xsl:when>
							 <xsl:when test="$PRINT_ORDER/color1_description != '' and $PRINT_ORDER/color2_description = ''">
								 <xsl:value-of select="$PRINT_ORDER/color1_description"/>
							</xsl:when>
						</xsl:choose>
					</td>
				</tr>
				<tr>
					<td colspan="8">
						<span class="Label">Description:</span><br />
						<xsl:choose>
							<xsl:when test="$PRINT_ORDER/subcode != ''">
								<xsl:value-of select="$PRINT_ORDER/subcode_description"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="$PRINT_ORDER/product_name"/>
							</xsl:otherwise>
						</xsl:choose>
					</td>
				</tr>
		</table>
	</td>
</xsl:template>
<xsl:template name="printOrderBills">
	<td colspan="8" style="padding-left:0px">
		<table id="orderBills" width="100%"  border="0" cellpadding="0" cellspacing="2">
			<tr>	<td width="16%"></td>
				<td class="Label" width="13%">
					Merchandise
				</td>
				<xsl:if test="$PRINT_ORDER/vendor_flag = 'N'">
					<td class="Label" width="11%">
						Add-Ons
					</td>
				</xsl:if>
				<td class="Label" width="19%">
					Service/Shipping Fee
				</td>
				<td class="Label" width="11%">
					Discount
				</td>
				<td class="Label" width="10%">
					Tax
				</td>
				<td class="Label" width="10%">
					Total
				</td>
			</tr>
			<xsl:for-each select="root/PRINT_ORDER_BILLS/PRINT_ORDER_BILL/total_type[contains(.,'O') ]">
				<tr>

					<td class="Label" style="text-align:right"><xsl:value-of select="text()"/></td>
					<td>
            		<xsl:value-of select="format-number(../product_amount, '#,###,##0.00;(#,###,##0.00)','notANumber')"/>
					</td>
					<xsl:if test="$PRINT_ORDER/vendor_flag = 'N'">
						<td>
                	<xsl:value-of select="format-number(../add_on_amount, '#,###,##0.00;(#,###,##0.00)','notANumber')"/>
					  </td>
					</xsl:if>
					<td>
            		 <xsl:value-of select="format-number(../serv_ship_fee, '#,###,##0.00;(#,###,##0.00)','notANumber')"/>
					</td>
					<td>
           			 <xsl:value-of select="format-number(../discount_amount, '#,###,##0.00;(#,###,##0.00)','notANumber')"/>
					</td>
					<td>
            		<xsl:value-of select="format-number(../tax, '#,###,##0.00;(#,###,##0.00)','notANumber')"/>
					</td>
					<td>
         			 <xsl:value-of select="format-number(../bill_total, '#,###,##0.00;(#,###,##0.00)','notANumber')"/>
					</td>
				</tr>
			</xsl:for-each>
			<xsl:for-each select="root/PRINT_ORDER_BILLS/PRINT_ORDER_BILL/total_type[contains(.,'A')]">
				<tr>
					<td class="Label" style="text-align:right"><xsl:value-of select="'Additional Bill(s)'"/></td>
					<td>
					 <xsl:value-of select="format-number(../product_amount, '#,###,##0.00;(#,###,##0.00)','notANumber')"/>
					</td>
					<xsl:if test="$PRINT_ORDER/vendor_flag = 'N'">
						<td>
						 <xsl:value-of select="format-number(../add_on_amount, '#,###,##0.00;(#,###,##0.00)','notANumber')"/>
						</td>
					</xsl:if>
					<td>
					<xsl:value-of select="format-number(../serv_ship_fee, '#,###,##0.00;(#,###,##0.00)','notANumber')"/>
					</td>
					<td>
					<xsl:value-of select="format-number(../discount_amount, '#,###,##0.00;(#,###,##0.00)','notANumber')"/>
					</td>
					<td>
					<xsl:value-of select="format-number(../tax, '#,###,##0.00;(#,###,##0.00)','notANumber')"/>
					</td>
					<td>
					<xsl:value-of select="format-number(../bill_total, '#,###,##0.00;(#,###,##0.00)','notANumber')"/>
					</td>
				</tr>
			</xsl:for-each>
			<xsl:for-each select="root/PRINT_ORDER_REFUNDS/PRINT_ORDER_REFUND/refund_type[contains(.,'R')]">
				<tr>
					<td class="Label" style="text-align:right"><xsl:value-of select="'Refund(s)'"/></td>
					<td>
					<xsl:value-of select="format-number(../product_amount, '#,###,##0.00;(#,###,##0.00)','notANumber')"/>
					</td>
					<xsl:if test="$PRINT_ORDER/vendor_flag = 'N'">
						<td>
							<xsl:value-of select="format-number(../addon_amount, '#,###,##0.00;(#,###,##0.00)','notANumber')"/>
						</td>
					</xsl:if>
					<td>
					<xsl:value-of select="format-number(../discount_amount, '#,###,##0.00;(#,###,##0.00)','notANumber')"/>
					</td>
					<td>
					<xsl:value-of select="format-number(../serv_ship_fee, '#,###,##0.00;(#,###,##0.00)','notANumber')"/>
					</td>
					<td>
						<xsl:value-of select="format-number(../tax, '#,###,##0.00;(#,###,##0.00)','notANumber')"/>
					</td>
					<td>
					<xsl:value-of select="format-number(../refund_total, '#,###,##0.00;(#,###,##0.00)','notANumber')"/>
					</td>
				</tr>
			</xsl:for-each>
			<xsl:for-each select="root/PRINT_ORDER_TOTALS/PRINT_ORDER_TOTAL/total_type[contains(.,'M')]">
				<tr>
					<td class="Label" style="text-align:right"><xsl:value-of select="text()"/></td>
					<td>
					<xsl:value-of select="format-number(../product_amount, '#,###,##0.00;(#,###,##0.00)','notANumber')"/>
					</td>
					<xsl:if test="$PRINT_ORDER/vendor_flag = 'N'">
						<td>
							<xsl:value-of select="format-number(../add_on_amount, '#,###,##0.00;(#,###,##0.00)','notANumber')"/>
						</td>
					</xsl:if>
					<td>
					<xsl:value-of select="format-number(../serv_ship_fee, '#,###,##0.00;(#,###,##0.00)','notANumber')"/>
					</td>
					<td>
					<xsl:value-of select="format-number(../discount_amount, '#,###,##0.00;(#,###,##0.00)','notANumber')"/>
					</td>
					<td>
					<xsl:value-of select="format-number(../tax, '#,###,##0.00;(#,###,##0.00)','notANumber')"/>
					</td>
					<td>
						<xsl:value-of select="format-number(../order_total, '#,###,##0.00;(#,###,##0.00)','notANumber')"/>
					</td>
				</tr>
			</xsl:for-each>
		</table>
	</td>
</xsl:template>

<xsl:template name="paymentInformation">
	<td colspan="8" style="padding-left:0px">
		<table id="paymentInformation" border="0" width="100%" cellpadding="0" cellspacing="1">
			<tr>
				<td width="13%"></td>
				<td class="Label" width="13%" >Payment Type</td>
				<td class="Label" width="16%" >Card Number</td>
				<td class="Label" width="12%">Auth#</td>
				<td class="Label" width="10%">Charged</td>
				<td class="Label" width="10%">Billed</td>
			</tr>
			<xsl:for-each select="root/PRINT_ORDER_PAYMENTS/PRINT_ORDER_PAYMENT/payment_indicator[contains(.,'O')]">
				<tr>
					<td class="Label" style="text-align:right"><xsl:value-of select="text()"/></td>
					<td><xsl:value-of select="../payment_type/text()"/></td>
					<td>
						<xsl:choose>
							<xsl:when test="../payment_type = 'GC' or ../payment_type = 'IN' or ../payment_type = 'NC'">
								<xsl:value-of select="../card_number"/>
							</xsl:when>
							<xsl:otherwise>
								************<xsl:value-of select="../card_number"/>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td><xsl:value-of select="../auth_number/text()"/></td>
          <td><xsl:value-of select="format-number(../credit_amount/text(), '#,###,##0.00;(#,###,##0.00)','notANumber')"/></td>
					<td><xsl:value-of select="../bill_date/text()"/></td>
				</tr>
			</xsl:for-each>
			<xsl:for-each select="root/PRINT_ORDER_PAYMENTS/PRINT_ORDER_PAYMENT/payment_indicator[contains(.,'Add')]">
				<tr>
					<td class="Label" style="text-align:right"><xsl:value-of select="text()"/></td>
					<td><xsl:value-of select="../payment_type/text()"/></td>
					<td>
						<xsl:choose>
							<xsl:when test="../payment_type = 'GC' or ../payment_type = 'IN' or ../payment_type = 'NC'">
								<xsl:value-of select="../card_number"/>
							</xsl:when>
							<xsl:otherwise>
								************<xsl:value-of select="../card_number"/>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td><xsl:value-of select="../auth_number/text()"/></td>
          <td><xsl:value-of select="format-number(../credit_amount/text(), '#,###,##0.00;(#,###,##0.00)','notANumber')"/></td>
					<td><xsl:value-of select="../bill_date/text()"/></td>
				</tr>
			</xsl:for-each>
			<xsl:for-each select="root/PRINT_ORDER_PAYMENTS/PRINT_ORDER_PAYMENT/payment_indicator[contains(.,'Ref')]">
				<tr>
					<td class="Label" style="text-align:right"><xsl:value-of select="text()"/></td>
					<td><xsl:value-of select="../payment_type/text()"/></td>
					<td>
						<xsl:choose>
							<xsl:when test="../payment_type = 'GC' or ../payment_type = 'IN' or ../payment_type = 'NC'">
								<xsl:value-of select="../card_number"/>
							</xsl:when>
							<xsl:otherwise>
								************<xsl:value-of select="../card_number"/>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td><xsl:value-of select="../auth_number/text()"/></td>
          <td><xsl:value-of select="format-number(../credit_amount/text(), '#,###,##0.00;(#,###,##0.00)','notANumber')"/></td>
					<td><xsl:value-of select="../bill_date/text()"/></td>
				</tr>
			</xsl:for-each>
		</table>
	</td>
</xsl:template>
<!-- Print order comments -->
<xsl:template name="printComments">
	<tr>
		<td colspan="8">
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
			 <tr>
				<td class="Label" width="12%">User ID</td>
				<td class="Label" width="12%">Date</td>
				<td class="Label" width="12%">Time</td>
				<td class="Label" width="64%">Comment</td>
			</tr>
			<xsl:for-each select="root/PRINT_ORDER_COMMENTS/PRINT_ORDER_COMMENT">
				<tr >
					<td><xsl:value-of select="created_by"/></td>
					<td><xsl:value-of select="substring(created_on,0,11)"/></td>
					<td><xsl:value-of select="substring(created_on,11)"/></td>
					<td><xsl:value-of select="comment_text"/></td>
				</tr>
			</xsl:for-each>
			</table>
		</td>
	</tr>
</xsl:template>

<!-- addFloristInformation -->

<xsl:template name="addFloristInformation">
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
		<tr>
			<td class="Label" width="15%">Florist Name</td>
			<td class="Label" width="15%">Florist Code</td>
			<td class="Label" width="20%">Florist Phone Number</td>
		</tr>
		<tr>
			<td width="15%">
				<xsl:choose>
					<xsl:when test="$PRINT_ORDER/florist_id = ''">
						<xsl:value-of select="'UNASSIGNED'"/>
					</xsl:when>
					<xsl:otherwise>
							<xsl:value-of select="$PRINT_ORDER/florist_name"/><br />
					</xsl:otherwise>
				</xsl:choose>
				</td>
			<td width="15%"><xsl:value-of select="$PRINT_ORDER/florist_id"/></td>
			<td width="20%">
				<xsl:if test="$PRINT_ORDER/florist_phone_number != ''">
					<script type="text/javascript">
						if(isUSPhoneNumber('<xsl:value-of select="$PRINT_ORDER/florist_phone_number"/>')){
							document.write(reformatUSPhone('<xsl:value-of select="$PRINT_ORDER/florist_phone_number"/>'));
						}else{
							document.write('<xsl:value-of select="$PRINT_ORDER/florist_phone_number"/>');
						}
					</script>
				</xsl:if>
			</td>
		</tr>
	</table>
</xsl:template>

<!-- addVendorInformation -->
<xsl:template name="addVendorInformation">
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
		<tr>
			<td  class="Label" style="width:30%">
				Vendor Name
				<xsl:value-of select="florist_name"/>
			</td>
			<td  class="Label" style="width:15%">
				Vendor Code
				<xsl:value-of select="florist_id"/>
			</td>
			<td  class="Label" style="width:25%">
				Tracking Number
			</td>
		</tr>
		<tr>
			<td style="width:30%"><xsl:value-of select="$PRINT_ORDER/florist_name"/><br /></td>
			<td style="width:15%"><xsl:value-of select="$PRINT_ORDER/florist_id"/></td>
			<td style="width:25%"><xsl:value-of select="$PRINT_ORDER/tracking_number"/></td>
		</tr>
	</table>
</xsl:template>

<xsl:template name="modDate">
	<xsl:param name="dateValue"/>
	<xsl:param name="hackDate" select="substring($dateValue,1,10)"/>
	<xsl:value-of select="concat(substring($hackDate,6,2),'/',substring($hackDate,9,2),'/',substring($dateValue,1,4))"/>
</xsl:template>

</xsl:stylesheet>