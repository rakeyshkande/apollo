<!DOCTYPE ACDemo[ 
	<!ENTITY nbsp "&#160;">
	<!ENTITY reg "&#169;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
     <xsl:template match="/root">
		<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
		<style type="text/css">

		</style>
		<script type="text/javascript">
			<![CDATA[
				function doCloseAction(){
					top.close();
				}
			]]>
		</script>
		<table border="0" width="98%" class="mainTable" cellpadding="0" cellspacing="0">
			<tr>
				<td class="banner">
					Alternate Contact Information
				</td>
			</tr>
			<tr>
				<td>
					<table id="altContactInfo" border="0" style="word-break:break-all;"  cellpadding="0" cellspacing="3" width="100%">
						<tr>
							<td class="Label" width="7%">
								Name:
							</td>
							<td><xsl:value-of select="MEMBERSHIPS/MEMBERSHIP_INFO/membership_id"/></td>
						</tr>
						<tr>
							<td class="Label">Email:</td>
							<td><xsl:value-of select="MEMBERSHIPS/MEMBERSHIP_INFO/date_miles_posted"/></td>
						</tr>
						<tr>
							<td class="Label">Phone:</td>
							<td><xsl:value-of select="MEMBERSHIPS/MEMBERSHIP_INFO/membership_first_name"/></td>
						</tr>
						<tr>
							<td class="Label">Ext:</td>
							<td><xsl:value-of select="MEMBERSHIPS/MEMBERSHIP_INFO/membership_last_name"/></td>
						</tr>
					</table>
				</td>
			</tr>
		</table><br/>
		<div style="width:98%;text-align:center"><button type="button" class="BlueButton" accesskey="C" onclick="javascript:doCloseAction();">(C)lose</button></div>
	</xsl:template>
</xsl:stylesheet>