<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="securityanddata.xsl"/>
<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>

<xsl:key name="pageData" match="/root/pageData/data" use="name"/>
<xsl:key name="security" match="/root/security/data" use="name"/>

<xsl:output method="html" indent="yes"/>
<xsl:template match="/root">
<html>
<head>
<script type="text/javascript" language="javascript">
var message = '<xsl:value-of select="key('pageData','message_display')/value"/>';
var updated = '<xsl:value-of select="key('pageData','updated')/value"/>';
var updateable = '<xsl:value-of select="key('pageData','updateable')/value"/>';

<![CDATA[
	function processCustomer()
	{

    if(message == '')
    {
        if(parent.pageAction == 'checkSubmit' && updateable.toUpperCase() == 'Y')
        {
          parent.doSubmitAction()
        }
        if(parent.pageAction == 'checkComplete' && updated.toUpperCase() == 'Y')
        {
          parent.doCompleteSubmit();
        }
    }
    else if (message == 'exception found')
    {
    		parent.doError();
    }
    else
    {
        alert(message);
    }
	}
]]>
</script>
</head>
<body onload="javascript:processCustomer();">
    <xsl:call-template name="securityanddata"/>
    <xsl:call-template name="decisionResultData"/>
</body>
</html>
</xsl:template>
</xsl:stylesheet>