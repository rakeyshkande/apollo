<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
						 doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
						 indent="yes"/>


<xsl:template name="mercMessButtons">

	<xsl:if test="$destination ='' ">
	<input type="hidden" name="load_member_data_url" id="load_member_data_url" value="{$xvLoadMemberDataUrl}"/>

    	<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" style="margin-top: 6px;">
    		<tr>
    			<xsl:choose>
                            <xsl:when test="$bypass_com = 'Y' ">
                                <td>
                                </td>
                            </xsl:when>
                            <xsl:otherwise>                        
                                <td>
                                    <button type="button"   id="recipientOrderButton" class="BigBlueButton ButtonTextBox2" accesskey="O" onclick="openRecipientOrder();">
                                            Recipient/<br/>(O)rder
                                    </button>
    
                                    <button type="button"   id="historyButton" class="BigBlueButton ButtonTextBox1" accesskey="H" onclick="openHistory();">
                                            (H)istory
                                    </button>
    
                                    <button type="button"   id="orderCommentsButton" class="BigBlueButton ButtonTextBox2" accesskey="C" onclick="openOrderComments();">
                                            Order<br/>(C)omments
                                    </button>
    
                                    <button type="button"   id="emailButton" class="BigBlueButton" accesskey="E" onclick="openEmail();">
                                         <xsl:if test="$msg_email_indicator = 'true'">
									          <xsl:attribute name="style">font-weight:bold</xsl:attribute>
									     </xsl:if>
									     (E)mail
                                    </button>
    
                                    <!-- <button type="button"   id="lettersButton" class="BigBlueButton" accesskey="L" onclick="openLetters();">
                                            <xsl:choose>
                                                            <xsl:when test="$msg_letter_indicator = 'true'">
                                                                    <b>(L)etters</b>
                                                            </xsl:when>
                                                            
                                                            <xsl:otherwise>
                                                                    (L)etters
                                                            </xsl:otherwise>
                                                    </xsl:choose>
                                            
                                    </button> -->
                            
                                    <button type="button"   id="refundButton" class="BigBlueButton" accesskey="R" onclick="openRefund();">
                                            (R)efund
                                    </button>
    
                                    <button type="button"   id="floristInquiryButton" class="BigBlueButton" accesskey="F" onclick="openFloristInquiry();">
                                            (F)lorist<br/>Inquiry
                                    </button>
                                </td>
                            </xsl:otherwise>
                        </xsl:choose>
        		

    		<td align="right">
<!--    
        			<button type="button" id="searchButton" class="BlueButton" accesskey="S" onclick="doSearchAction();">
        				(S)earch
        			</button>
              -->

        			<button type="button"   id="backButton" class="BlueButton" accesskey="B" onclick="doBackAction();">
        				(B)ack to Communication
        			</button>

        			<br/>

       				<button type="button"   id="mainMenuButton" class="BlueButton" accesskey="M" onclick="doMainMenuAction();">
        				(M)ain Menu
        			</button>
        		</td>
        	</tr>
        </table>
        </xsl:if>
	<script language="javascript" type="text/javascript">
		         
		
		window.onerror = handleError;
		
		 
		
		function handleError (err, url, line)
		
		{
		
		    if (err.indexOf('focus') != -1)
		
		      return true; // error is handled
		
		    else
		
		      alert('Error - ' + err);
		
		      return false; // let the browser handle the error
		
		}
              

	</script>

</xsl:template>

</xsl:stylesheet>