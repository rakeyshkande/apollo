<!DOCTYPE stylesheet [
	<!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:import href="../header.xsl"/>
<xsl:import href="../footer.xsl"/>
<xsl:import href="mercMessButtons.xsl"/>
<xsl:import href="mercMessDropDown.xsl"/>
<xsl:import href="../securityanddata.xsl"/>
<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>
<xsl:import href="messagingSecurityAndData.xsl"/>
<xsl:import href="formattingTemplates.xsl"/>
<xsl:key name="security" match="/message/security/data" use="name"/>
<xsl:variable name="messageId" select="normalize-space(message/messageId)"/>

<!-- Case comparision values -->
<xsl:variable name="upper_case">ABCDEFGHIJKLMNOPQRSTUVWXYZ</xsl:variable>
<xsl:variable name="lower_case">abcdefghijklmnopqrstuvwxyz</xsl:variable>
<xsl:variable name="shippingSystem" select="translate(message/shippingSystem, $lower_case, $upper_case)"/>

<xsl:variable name="xvOrderNumber" select="normalize-space($msg_external_order_number)"/>


<xsl:output method="html"
			doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
			doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
			indent="yes"/>

<xsl:include href="mercMessFields.xsl"/>

<xsl:template match="/">


	<html>
		<head>
			<title><xsl:value-of select="$xvHeaderName"/></title>

			<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
			<meta http-equiv="language" content="en-us"/>

			<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
			<link rel="stylesheet" type="text/css" href="css/mercuryMessage.css"/>
			<script type="text/javascript" src="js/commonUtil.js">;</script>
			<script type="text/javascript" src="js/clock.js">;</script>

			<script type="text/javascript" src="js/mercuryMessage.js">;</script>
			<script type="text/javascript" src="js/FormChek.js">;</script>
			<script>
				function init()
				{
					document.oncontextmenu = contextMenuHandler;
					untabHeader();
				}
				<![CDATA[	
				function openCOMScreen(){
				
					var ordNum =  document.getElementById("msg_external_order_number").value;
					var ordGuid = document.getElementById("msg_guid").value;
					var cusId = document.getElementById("msg_customer_id").value;
					
					var url = "customerOrderSearch.do?action=customer_account_search&order_number="+ordNum+"&order_guid="+ordGuid+"&customer_id="+cusId;
					document.forms[0].action = url;
					document.forms[0].submit();
				}
				]]>
				
			</script>

			<style>
        		td.label {
        			width: 225px;
        		}
			</style>
		</head>

		<body onload="init();">
			<form id="viewFTDMessageForm" method="post">
				<xsl:call-template name="securityanddata"/>
				<xsl:call-template name="decisionResultData"/>
				<xsl:call-template name="messagingSecurityAndData"/>
				<input type="hidden" name="ftd_allowed" id="ftd_allowed" value="{$ftd_allowed}"/>
                <input type="hidden" name="personalized" id="personalized" value="{$personalized}"/>
                <input type="hidden" name="in_transit" id="in_transit" value="{$in_transit}"/>
                <input type="hidden" name="msgExternalOrderNumber" id="msgExternalOrderNumber" value="{$msgExternalOrderNumber}"/>
                <input type="hidden" name="msgOrderGuid" id="msgOrderGuid" value="{$msgOrderGuid}"/>
                <input type="hidden" name="msgCustomerId" id="msgCustomerId" value="{$msgCustomerId}"/>
				<input type="hidden" name="subsequent_action" id="subsequent_action"/>
				<input type="hidden" name="ftd_message_id" id="ftd_message_id" value="{normalize-space(message/messageId)}"/>
				<input type="hidden" name="ship_date" id="ship_date" value="{$xvShipDate}"/>
                <iframe name="unlockFrame" id="unlockFrame" height="0" width="0" border="0"></iframe>
				<iframe id="checkLock" width="0px" height="0px" border="0"/>
			</form>

			<xsl:call-template name="header">
				<xsl:with-param name="showSearchBox" select="false()"/>
				<xsl:with-param name="showPrinter" select="true()"/>
				<xsl:with-param name="showTime" select="true()"/>
				<xsl:with-param name="headerName" select="$xvHeaderName"/>
				<xsl:with-param name="showBackButton" select="true()"/>
				<xsl:with-param name="cservNumber" select="$xvCservNumber"/>
				<xsl:with-param name="indicator" select="$xvIndicator"/>
				<xsl:with-param name="brandName" select="$xvBrandName"/>
				<xsl:with-param name="dnisNumber" select="$xvDnisNumber"/>
				<xsl:with-param name="backButtonLabel" select="'(B)ack to Communication'"/>
				<xsl:with-param name="showCSRIDs" select="true()"/>
				<xsl:with-param name="cdispEnabledFlag" select="$cdisp_enabledFlag"/>
			</xsl:call-template>

			<script>updateCurrentUsers();</script>

			<div id="content">
				<table class="mainTable" width="98%" align="center" cellpadding="0" cellspacing="1">
					<tr style="font-weight: bold;">
										<td id="orderNumber">
											Order Number:&nbsp;
											<a href="javascript: openCOMScreen();" id="ext_order_number" tabindex="-1">
												<xsl:value-of select="$xvOrderNumber"/> 
											</a>
										</td>
					</tr>
					<tr>
						<td>
							<table class="PaneSection">
								<tr>
									<td class="banner">
										Message Detail
										<xsl:if test="$xvMercuryMessageType">
											&nbsp;-&nbsp;<xsl:value-of select="$xvMercuryMessageType"/>
										</xsl:if>
									</td>
								</tr>
							</table>

							<div style="overflow: auto; padding-right: 16px;">
								<br></br>
								<div style = "FONT-SIZE: 11pt; MARGIN-LEFT: 25px; FONT-WEIGHT: bold;" id = "vm_ftd_product_info">Product Information:</div>
								<br></br>
								<table width="50%" border="0" cellpadding="0" cellspacing="0" style="TABLE-LAYOUT: fixed; DISPLAY: inline-block; float: left">

									<tr>
										<td class="label" id = "vm_ftd_delivery_date">
											Delivery Date:
										</td>

										<td class="datum">
											<xsl:value-of select="$xvOrderDeliveryDate"/>
										</td>
									</tr>
									
									<tr>
										<td class="label" id = "vm_ftd_product">
											Product:
										</td>
										<td class="datum">
										    <xsl:call-template name="replace">
        										<xsl:with-param name="string" select="$xvFirstChoice"/>
    										</xsl:call-template>
										</td>
									</tr>

									<tr>
										<td class="label" id = "vm_ftd_allowed_sub">
											Allowed Substitution:
										</td>

										<td class="datum">
											<xsl:call-template name="replace">
        										<xsl:with-param name="string" select="$xvSecondChoice"/>
    										</xsl:call-template>

										</td>
									</tr>

									<tr>
										<td class="label" id = "vm_ftd_price">
											Price Including Delivery:
										</td>

										<td class="datum">
											<xsl:value-of select="$xvNewPrice"/>
										</td>
									</tr>
									
									<tr>
										<td class="label" id = "vm_ftd_spl_ins">
											Special Instructions:
										</td>

										<td class="datum">
											<xsl:choose>
												<xsl:when test="$xvInstructions = ''">
													NONE
												</xsl:when>

												<xsl:otherwise>
													<xsl:call-template name="replace">
														<xsl:with-param name="string" select="$xvInstructions"/>
													</xsl:call-template>
												</xsl:otherwise>
											</xsl:choose>
										</td>
									</tr>
									
									<tr>
										<td class="label" id = "vm_ftd_occasion">
											Occasion:
										</td>

										<td class="datum">
											<xsl:value-of select="$xvOccasionDescription"/>
										</td>
									</tr>
									
									<tr>
										<td class="label" id = "vm_ftd_card_msg">
											Card Message:
										</td>

										<td class="datum">
											<xsl:choose>
												<xsl:when test="$xvCardMessage = ''">
													NO MESSAGE ENTERED
												</xsl:when>

												<xsl:otherwise>
													<xsl:call-template name="replace">
														<xsl:with-param name="string" select="$xvCardMessage"/>
													</xsl:call-template>
												</xsl:otherwise>
											</xsl:choose>
										</td>
									</tr>
								</table>	
							
								
								<table width="390px" cellpadding="0" cellspacing="0" style="FLOAT: right; MARGIN-TOP: -33px; MARGIN-RIGHT: -16px" >
									<tr>
										<td class="label" id = "vm_ftd_status">
											Status:
										</td>
				
										<td class="datum">
											<xsl:value-of select="$xvStatus"/>
										</td>
									</tr>
									
									<tr>
										<td class="label" id = "vm_ftd_msg_time">
											Message Time:
										</td>
				
										<td class="datum">
											<xsl:value-of select="$xvStatusTimestamp"/>
										</td>
									</tr>
									
									<tr>
										<td class="label">
											&nbsp;
										</td>
				
										<td class="datum">
											&nbsp;
										</td>
									</tr>
									
									<tr>
										<td class="label" id = "vm_ftd_mercury_order">
											Mercury Order:
										</td>
				
										<td class="datum">
											<xsl:value-of select="$xvMercuryOrderNumber"/>
										</td>
									</tr>
									
									<tr>
										<td class="label">
											&nbsp;
										</td>
				
										<td class="datum">
											&nbsp;
										</td>
									</tr>

									<tr>
										<td class="label"  id = "vm_ftd_filler_code">
											Filler Code:
										</td>

										<td class="datum">
											<xsl:value-of select="$xvFillingFloristCode"/>
											<input type="hidden" name="filling_florist_code" id="filling_florist_code" value="{$xvFillingFloristCode}"/>
							            </td>
									</tr>

									<tr>
										<td class="label"  id = "vm_ftd_sender_code">
											Sender Code:
										</td>

										<td class="datum">
											<xsl:value-of select="$xvSendingFloristCode"/>
											<input type="hidden" name="sending_florist_code" id="sending_florist_code" value="{$xvSendingFloristCode}"/>
										</td>
									</tr>
									<tr>
										<td class="label"  id = "vm_ftd_operator">
											Operator:
										</td>

										<td class="datum">
											<xsl:value-of select="$xvOperatorCode"/>
										</td>
									</tr>
									
									<xsl:if test="$shippingSystem='SDS' or $shippingSystem='FTD WEST'">
										<tr>
											<td class="label"  id = "vm_ftd_sds_transaction">
												&nbsp;
											</td>
											
											<td class="datum">
									            <a id="viewSDSTransaction" href="javascript:doViewSDSTransaction('{$xvMercuryOrderNumber}','{$messageId}');" class="textlink">
									              	View SDS Transaction
									            </a>
											</td>
										</tr>
									</xsl:if>

								</table> <!-- End of order -->
								
								<div style = "float: left; FONT-SIZE: 11pt; MARGIN-LEFT: 25px; WIDTH: 50%; FONT-WEIGHT: bold; margin-top: 25px" id = "vm_ftd_delivery_info">Delivery Information:</div>
								<br></br>
								<table width="55%" cellpadding="0" cellspacing="0" style="TABLE-LAYOUT: fixed; DISPLAY: inline-block; float: left; ">
									<tr>
										<td class="label"  id = "vm_ftd_name">
											Name:
										</td>

										<td class="datum">
											<xsl:value-of select="$xvRecipientName"/>
										</td>
									</tr>
									
									<tr>
										<td class="label"  id = "vm_ftd_address">
											Address:
										</td>

										<td class="datum">
											<xsl:value-of select="$xvRecipientStreet"/>
										</td>
									</tr>
									
									<tr>
										<td class="label"  id = "vm_ftd_city_zip">
											City, State, Zip:
										</td>

										<td class="datum">
											<xsl:value-of select="$xvRecipientCityStateZip"/>
										</td>
									</tr>
									
									
									<tr>
										<td class="label"  id = "vm_ftd_phone">
											Phone:
										</td>

										<td class="datum">
											<script type="text/javascript"> 
											  document.write(reformatUSPhoneWithSlash('<xsl:value-of select="$xvRecipientPhone"/>'));
										    </script>
										</td>
									</tr>
										<tr>
											<td class="label"> &nbsp;
											</td>
											<td class="datum"> &nbsp;
											</td>
										</tr>
										<xsl:if test="$xvIsSubsequentMessageAllowed = 'true'">
										<tr>
											<td class="label">
												&nbsp;
											</td>
					
											<td class="datum">
												&nbsp;
											</td>
										</tr>
										
										<tr>
											<td class="label" style = "FONT-SIZE: 14px"  id = "vm_ftd_dropdown">
												I WOULD LIKE TO:
											</td>
											<td class="datum">
												<xsl:call-template name="mercMessDropDown"/>
											</td>
										</tr>
										<tr>
											<td class="label">
												&nbsp;
											</td>
					
											<td class="datum">
												&nbsp;
											</td>
										</tr>
									 </xsl:if>
								</table>
                <DIV id="compOrderDIV" style="display:none">
                <TABLE width="100%" border="0" cellpadding="0" cellspacing="0" style="margin-right: -16px;">
                  <TR>
                    <TD colspan="2">
                      <HR/>
                    </TD>
                  </TR>
                  <TR>
                    <TD class="label" id = "vm_ftd_approval_id"> Approval ID: </TD>
                    <TD class="datum">
                      <input tabindex="6" type="text" name="approval_id" id="approval_id" size="10" maxlength="30"/>
          &nbsp;
                      <SPAN style="color:red"> *** </SPAN>&nbsp;
                      <SPAN style="color:green"> User ID of manager approving Comp Order. </SPAN>&nbsp;
                    </TD>
                  </TR>
                  <TR>
                    <TD class="label"  id = "vm_ftd_approval_pwd"> Approval Password: </TD>
                    <TD class="datum">
                      <input tabindex="7" type="password" name="approval_passwd" id="approval_passwd" size="10" maxlength="10"/>
          &nbsp;
                      <SPAN style="color:red"> *** </SPAN>&nbsp;
                      <SPAN style="color:green"> Password of manager approving Comp Order. </SPAN>
                    </TD>
                  </TR>
            	<tr>
					<td class="label">
						&nbsp;
					</td>

					<td class="datum">
						&nbsp;
					</td>
				</tr>
                </TABLE>
              </DIV>
							</div> <!-- End of scrolling div -->
						</td>
					</tr>
				</table> <!-- End of mainTable -->
			</div>

			<div id="waitDiv" style="display: none;">
				<table id="waitTable" class="mainTable" width="98%" height="100%" align="center" cellpadding="2" cellspacing="1" >
					<tr>
						<td width="100%">
							<table width="100%" cellspacing="0" cellpadding="0" border="0">
								<tr>
									<td id="waitMessage" align="right" width="50%" class="WaitMessage"></td>
									<td id="waitTD"  align="left" width="50%" class="WaitMessage"></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>

			<xsl:call-template name="mercMessButtons"/>
			<xsl:call-template name="footer"/>
		</body>
	</html>

</xsl:template>
<!--
<xsl:template name="replace">
    <xsl:param name="string"/>
    <xsl:choose>
        <xsl:when test="contains($string,'&#10;')">
            <xsl:value-of select="substring-before($string,'&#10;')"/>
            <br/>
            <xsl:call-template name="replace">
                <xsl:with-param name="string" select="substring-after($string,'&#10;')"/>
            </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
            <xsl:value-of select="$string"/>
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>
-->
</xsl:stylesheet><!-- Stylus Studio meta-information - (c) 2004-2005. Progress Software Corporation. All rights reserved.
<metaInformation>
<scenarios ><scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="ftd.xml" htmlbaseurl="" outputurl="" processortype="internal" profilemode="0" profiledepth="" profilelength="" urlprofilexml="" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext=""/></scenarios><MapperMetaTag><MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/><MapperBlockPosition></MapperBlockPosition></MapperMetaTag>
</metaInformation>
-->