
<!DOCTYPE stylesheet [
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:import href="../securityanddata.xsl"/>
	<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>
	<xsl:import href="messagingSecurityAndData.xsl"/>
	<xsl:import href="mercMessFormatDate.xsl"/>
	<xsl:import href="../header.xsl"/>
	<xsl:import href="../footer.xsl"/>
<xsl:key name="security" match="/root/security/data" use="name"/>
	<xsl:output method="html" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" indent="yes"/>

	<xsl:template match="/">
		<html>
			<head>
				<title>Update Florist</title>
				<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
				<link rel="stylesheet" type="text/css" href="css/mercuryMessage.css"/>

				<script type="text/javascript" src="js/mercuryMessage.js">;</script>
				<script>
                                var recipZipCode = '<xsl:value-of select="key('security','recip_zip_code')/value"/>';

//*********************************************************************************************************************
//* init
//*********************************************************************************************************************
function init()
{
  document.oncontextmenu = contextMenuHandler;
  document.onkeydown = backKeyHandler;
  untabHeader();
  window.name = "updateFlorist";
  if(document.getElementById("filling_florist_code"))
  {
    if(document.getElementById("filling_florist_code").type=="text"){
      document.getElementById("filling_florist_code").focus();
    }
  }
}
				
<![CDATA[
//*********************************************************************************************************************
//* exit
//*********************************************************************************************************************
function exit()
{
//	document.getElementById("unlockFrame").src="messagingLock.do?action_type=unlock&lock_app=MODIFY_ORDER&lock_id="+document.getElementById("order_detail_id").value+getSecurityParams(false);

}
				
				
//*********************************************************************************************************************
//* submitFormAction
//*********************************************************************************************************************
function submitFormAction(floristSelectionLogId, zipCityFlag){
  showWaitMessage("content", "wait");
  var valid=checkFloristCode("filling_florist_code", "invalidFloristCode");
  if(valid){
    checkLockProcessRequest('update_florist', floristSelectionLogId, zipCityFlag);
  }else{
    hideWaitMessage("content", "wait");
  }

}

function submitFormActionNew() {
  showWaitMessage("content", "wait");
  var valid=checkFloristCode("filling_florist_code", "invalidFloristCode");
  var floristSelectionLogId = '';
  var zipCityFlag = 'Manual';
  if(valid){
    checkLockProcessRequest('update_florist', floristSelectionLogId, zipCityFlag);
  }else{
    hideWaitMessage("content", "wait");
  }

}
				

//*********************************************************************************************************************
//* doUpdateFlorist
//*********************************************************************************************************************
function doUpdateFlorist(){


  var url = "updateFlorist.do?filling_florist_code="+ document.forms[0].filling_florist_code.value
        + "&order_detail_id="
        + document.forms[0].order_detail_id.value
        + "&msg_message_type="
        + document.forms[0].msg_message_type.value
        + "&destination="
        + document.forms[0].destination.value
        + "&queue_id="
        + document.forms[0].queue_id.value
        + getSecurityParams(false);

  FloristStatusIFrame.location.href = url;

 
}
				
function doUpdateFlorist(floristSelectionLogId, zipCityFlag){

  var url = "updateFlorist.do?filling_florist_code="+ document.forms[0].filling_florist_code.value
        + "&order_detail_id="
        + document.forms[0].order_detail_id.value
        + "&msg_message_type="
        + document.forms[0].msg_message_type.value
        + "&destination="
        + document.forms[0].destination.value
        + "&queue_id="
        + document.forms[0].queue_id.value
        + "&florist_selection_log_id="
        + floristSelectionLogId
        + "&zip_city_flag="
        + zipCityFlag
        + getSecurityParams(false);

  FloristStatusIFrame.location.href = url;

 
}				
//*********************************************************************************************************************
//* redirectForm
//*********************************************************************************************************************
function redirectForm(forwardAction){

  var url;
  if(forwardAction.indexOf("prepareCallout")!=-1){
      url   =forwardAction+"?order_detail_id="+ document.forms[0].order_detail_id.value
            + "&msg_message_type="
            + document.forms[0].msg_message_type.value
            +"&filling_florist_code="+document.forms[0].filling_florist_code.value
            + "&destination="
            + document.forms[0].destination.value
            +"&queue_id="
            + document.forms[0].queue_id.value
  }else{
     url =forwardAction+document.forms[0].msg_external_order_number.value
    +"&order_guid="+document.forms[0].msg_guid.value
    +"&customer_id="+ document.forms[0].msg_customer_id.value;
  }

  top.returnValue=url;
  top.close();

}
				
								
//*********************************************************************************************************************
//* doClose
//*********************************************************************************************************************
function doClose(){


  top.close();

}
				

//*********************************************************************************************************************
//* doFloristLookupAction
//*********************************************************************************************************************
function doFloristLookupAction(){					
				
  var args = new Array();

  args.action = "floristLookup.do";

  args[args.length] = new Array("order_detail_id", document.getElementById("order_detail_id").value);
  args[args.length] = new Array("zip_code", recipZipCode);
  args[args.length] = new Array("securitytoken", document.getElementById("securitytoken").value);
  args[args.length] = new Array("context", document.getElementById("context").value);
                                args[args.length] = new Array("lookup_by_order", "true");


  var modal_dim = "dialogWidth:750px; dialogHeight:400px; dialogLeft:125; dialogTop: 190; center:yes; status=0; help:no; scroll:no";
  var floristInfo = window.showModalDialog("html/waitModalDialog.html", args, modal_dim);

  if(floristInfo != null)
  {
    document.forms[0].filling_florist_code.value = floristInfo[0];
    submitFormActionNew();
  }

}	
                                
//*********************************************************************************************************************
//* doSendNewFTDMessageAction
//*********************************************************************************************************************
function doSendNewFTDMessageAction(actionType)
{
  showWaitMessage("content", "wait");
  if(actionType == "auto_select_send")
  {
    var url = "updateFloristAutoSelect.do?action="
            + actionType
            + "&lookup_by_order=true"
            + "&order_detail_id="
            + document.forms[0].order_detail_id.value
            + "&msg_message_type="
            + document.forms[0].msg_message_type.value
            + "&source_code="
            + document.forms[0].source_code.value
            + getSecurityParams(false);
    FloristStatusIFrame.location.href = url;
   }
}              

function doSendNewFTDMessageAction(actionType, floristSelectionLogId, zipCityFlag)
{
  showWaitMessage("content", "wait");
  if(actionType == "auto_select_send")
  {
    var url = "updateFloristAutoSelect.do?action="
            + actionType
            + "&lookup_by_order=true"
            + "&order_detail_id="
            + document.forms[0].order_detail_id.value
            + "&msg_message_type="
            + document.forms[0].msg_message_type.value
            + "&source_code="
            + document.forms[0].source_code.value
            + "&florist_selection_log_id="
            + floristSelectionLogId
        	+ "&zip_city_flag="
       	    + zipCityFlag
            + getSecurityParams(false);
    FloristStatusIFrame.location.href = url;
   }
}                                
]]></script>
			</head>
			<body onload="init();" onunload="exit();">

				<form name="updateFloristForm" id="updateFloristForm" method="post">

					<iframe id="FloristStatusIFrame" name="FloristStatusIFrame" width="0" height="0" border="0" style="display: none"/>
					<iframe name="unlockFrame" id="unlockFrame" height="0" width="0" border="0"></iframe>
					<xsl:call-template name="securityanddata"/>
					<xsl:call-template name="decisionResultData"/>
					<xsl:call-template name="messagingSecurityAndData"/>
					<!-- header -->
					<xsl:call-template name="header">
						<xsl:with-param name="showSearchBox" select="false()"/>
						<xsl:with-param name="showPrinter" select="false()"/>
						<xsl:with-param name="showTime" select="true()"/>
						<xsl:with-param name="headerName" select="'Update Florist'"/>
						<xsl:with-param name="showBackButton" select="false()"/>
						<xsl:with-param name="showCSRIDs" select="false()"/>
						<xsl:with-param name="cdispEnabledFlag" select="$cdisp_enabledFlag"/>
					</xsl:call-template>
					<!-- end of header -->
					<div id="content">

						<iframe id="checkLock" width="0px" height="0px" border="0"/>
						<table class="mainTable" cellpadding="2" cellspacing="2" width="98%">
							<tr>
								<td>
									<table border="0" class="innerTable" cellpadding="2" cellspacing="2" width="100%">
										<tr align="center">
											<td colspan="3">
												<table border="0" cellpadding="2" cellspacing="2" width="100%">
													<tr>
														<td class="Label" align="right">Florist Number:</td>
														<td colspan="1" style="padding-left:1em;" align="left">
														<input type="text" name="filling_florist_code" value="" id="filling_florist_code" onKeyPress="return noEnter();" size="20"  tabindex="1"/>
														<span id="invalidFloristCode" class="RequiredFieldTxt" style="display: none;"></span>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr align="center">
											<td width="30%">
												<br/>
												<br/>
												<button type="button" class="BlueButton" onclick="submitFormActionNew();" accesskey="G"  tabindex="2">Submit Chan(g)es</button>
											</td>
											<td width="30%">
												<br/>
												<br/>
												<button type="button" class="BlueButton" accesskey="U" id="lookupButton" onclick="doFloristLookupAction();" style="margin-right: 10px;" tabindex="3">Florist Look(U)p</button>
											</td>
											<td width="30%">
												<br/>
												<br/>
                                                                                  <button type="button" class="BlueButton" accesskey="A" id="autoSelectButton" onclick="checkLockProcessRequest('auto_select_send');" style="margin-right: 10px;" tabindex="4">(A)uto Select &amp; Send</button>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<table border="0" cellpadding="0" cellspacing="0" width="98%">
							<tr>
								<td width="100%" align="right">
									&nbsp;
									<button type="button" class="BlueButton" accesskey="C" onclick="releaseLock('do_close', 'MODIFY_ORDER');"  tabindex="5">(C)lose</button>
								</td>
							</tr>
						</table>
					</div>
					<div id="waitDiv" style="display: none;">
						<table id="waitTable" class="mainTable" width="98%" height="100%" align="center" cellpadding="2" cellspacing="1">
							<tr>
								<td width="100%">
									<table width="100%" cellspacing="0" cellpadding="0" border="0">
										<tr>
											<td id="waitMessage" align="right" width="50%" class="WaitMessage"></td>
											<td id="waitTD" align="left" width="50%" class="WaitMessage"></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</div>
				</form>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>