<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:output method="html" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" indent="yes"/>


	<xsl:template name="mercFTDFillerCode">
		<xsl:choose>
											<xsl:when test="$xvMessageCategory = 'Venus' or $new_florist_id !=''">
												<input type="hidden" name="call_out" id="call_out" value="false"/>
										<tr>
											<td class="label" style="vertical-align: middle;">
												FILLER CODE:
											</td>
	
											<td class="datum">
												
														<xsl:value-of select="$xvFillingFloristCode"/>		
														<input type="hidden" name="filling_florist_code" id="filling_florist_code" value="{$xvFillingFloristCode}"/>
															
														<span id="invalidFloristCode" class="RequiredFieldTxt" style="display: none;">
														</span>
													
													
												
											</td>
										</tr>
											</xsl:when>
											
											<xsl:otherwise>
										<tr>
											<td class="label" style="vertical-align: middle;">
												MANUAL CALL OUT ORDER:
											</td>
	
											<td class="datum">
												
												<input type="checkbox" name="call_out" id="call_out" value="true">
													<xsl:if test="$xvMercuryMessageType='FTDM'">
														<xsl:attribute name="checked">true</xsl:attribute>
													</xsl:if>														
												</input>
											</td>
										</tr>
										<tr>
											<td class="label" style="vertical-align: middle;">
												FILLER CODE:
											</td>
	
											<td class="datum">
												
														
														<input type="text" size="9" name="filling_florist_code" id="filling_florist_code" maxlength="9" onKeyPress="return noEnter();"/>
														<span id="invalidFloristCode" class="RequiredFieldTxt" style="display: none;">
														</span>
													
													
												
											</td>
										</tr>
											</xsl:otherwise>
										</xsl:choose>
	</xsl:template>
</xsl:stylesheet>