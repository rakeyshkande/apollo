<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
						 doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
						 indent="yes"/>


<xsl:param name="ftd_allowed"><xsl:value-of select="key('security','ftd_allowed')/value" /></xsl:param>
<xsl:param name="personalized"><xsl:value-of select="key('security','personalized')/value" /></xsl:param>
<xsl:param name="in_transit"><xsl:value-of select="key('security','in_transit')/value" /></xsl:param>
<xsl:param name="msgExternalOrderNumber"><xsl:value-of select="key('security','msgExternalOrderNumber')/value" /></xsl:param>
<xsl:param name="msgOrderGuid"><xsl:value-of select="key('security','msgOrderGuid')/value" /></xsl:param>
<xsl:param name="msgCustomerId"><xsl:value-of select="key('security','msgCustomerId')/value" /></xsl:param>
<xsl:template name="mercMessDropDown">
<xsl:if test="$xvStatus != 'OPEN' and $bypass_com != 'Y'">
	<iframe id="checkLock" width="0px" height="0px" border="0"/>	
	<input type="hidden" id="sending_florist_code" name="sending_florist_code" value="{$xvSendingFloristCode}"/>
	<input type="hidden" id="ftd_message_id" name="ftd_message_id" value="{normalize-space(message/ftdMessageID)}"/>
	<input type="hidden" id="ftd_can_rej_flag" name="ftd_can_rej_flag" value="{normalize-space(message/ftdCancelledOrRejected)}"/>
	<select id="would_like_to" name="action_type" onchange="javascript:onWouldLikeToChange();" > 
	<xsl:choose>
		<xsl:when test="$xvStatus = 'MANUAL'">
			<option value="cancel_order">Cancel Order</option>
			<option value="send_new_ftd">Send New FTD Order</option>
			<option value="cancel_original_send_new">Cancel Original Order &amp; Send New FTD</option>
			<option value="pay_original_send_new">Pay Original Florist &amp; Send New FTD</option>
			<option value="send_comp_order">Send COMP Order</option>
			<option value="callout">Call Out Manual Order</option>
			<option value="send_adjustment">Send Adjustment</option>
		</xsl:when>
		
		<xsl:when test="$xvStatus = 'REJECTED' or $xvStatus = 'ERROR' ">
			
			<option value="send_new_ftd">Send New FTD Order</option>
			
			<option value="pay_original_send_new">Pay Original Florist &amp; Send New FTD</option>
			<option value="send_comp_order">Send COMP Order</option>
			<option value="callout">Call Out Manual Order</option>
			
		</xsl:when>
		
		<xsl:otherwise>
			<option value="delivery_confirmation">Request Delivery Confirmation</option>
			<option value="ask_florist">Ask Florist A Question</option>
			<option value="cancel_order">Cancel Order</option>
			<option value="send_new_ftd">Send New FTD Order</option>
			<option value="cancel_original_send_new">Cancel Original Order &amp; Send New FTD</option>
			<option value="pay_original_send_new">Pay Original Florist &amp; Send New FTD</option>
			<option value="send_comp_order">Send COMP Order</option>
			<option value="send_price_change">Send Florist A Price Change</option>
			<option value="accept_price_change">Accept Florist Price Change</option>
			<option value="send_add_info">Send Florist Additional Information</option>
			<option value="answer_florist">Answer Florist</option>
			<option value="callout">Call Out Manual Order</option>
			<option value="send_adjustment">Send Adjustment</option>
			<option value="cost_of_goods">Send Florist Cost of Goods Sold</option>
		</xsl:otherwise>
	</xsl:choose>
	</select>
	
	
	
	<button type="button" class="BlueButton" id="dropDownGoButton" accesskey="G" onclick="optionClicked();">
		(G)o$
	</button>

	<iframe id="MessageForwardFrame" name="MessageForwardFrame" width="0" height="0" border="0"/>
</xsl:if>	
	
</xsl:template>


<xsl:template name="mercMessDropDownManual">


  <select id="would_like_to" name="action_type">

  </select>

  <button type="button" class="BlueButton" id="dropDownGoButton" accesskey="G" onclick="checkLockProcessRequest('message_forward');">
    (G)o
  </button>

	<iframe id="MessageForwardFrame" name="MessageForwardFrame" width="0" height="0" border="0"/>


</xsl:template>

</xsl:stylesheet>