<!DOCTYPE ACDemo[
<!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0"  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:key name="security" match="/message/security/data" use="name"/>

	<xsl:param name="order_detail_id"><xsl:value-of select="key('security','order_detail_id')/value" /></xsl:param>
	<xsl:param name="msg_guid"><xsl:value-of select="key('security','msg_guid')/value" /></xsl:param>
    	<xsl:param name="msg_external_order_number"><xsl:value-of select="key('security','msg_external_order_number')/value" /></xsl:param>
    	<xsl:param name="msg_master_order_number"><xsl:value-of select="key('security','msg_master_order_number')/value" /></xsl:param>
	<xsl:param name="msg_email_indicator"><xsl:value-of select="key('security','msg_email_indicator')/value" /></xsl:param>
	<xsl:param name="msg_letter_indicator"><xsl:value-of select="key('security','msg_letter_indicator')/value" /></xsl:param>
	<xsl:param name="msg_customer_id"><xsl:value-of select="key('security','msg_customer_id')/value" /></xsl:param>
	
  	<xsl:output method="html" indent="yes"/>


	<xsl:template name="communicationData">
	 <!-- Messaging -->
	 <input type="hidden" name="order_detail_id"  id="order_detail_id" value="{$order_detail_id}"/>
     <input type="hidden" name="msg_guid" id="msg_guid" value="{$msg_guid}"/>
     <input type="hidden" name="external_order_number" id="external_order_number" value="{$msg_external_order_number}"/>
     <input type="hidden" name="customer_id" id="customer_id" value="{$msg_customer_id}"/>
     <input type="hidden" name="msg_external_order_number" id="msg_external_order_number" value="{$msg_external_order_number}"/>
     <input type="hidden" name="msg_master_order_number" id="" value="{$msg_master_order_number}"/>
     <input type="hidden" name="msg_email_indicator" id="msg_email_indicator" value="{$msg_email_indicator}"/>
	 <input type="hidden" name="msg_letter_indicator" id="msg_letter_indicator" value="{$msg_letter_indicator}"/>
	 <input type="hidden" name="msg_customer_id" id="msg_customer_id" value="{$msg_customer_id}"/>
			
	</xsl:template>
</xsl:stylesheet>