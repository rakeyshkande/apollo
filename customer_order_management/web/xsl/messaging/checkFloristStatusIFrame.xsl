<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:key name="security" match="/root/security/data" use="name"/>

<xsl:param name="msg_order_type"><xsl:value-of select="key('security','msg_order_type')/value" /></xsl:param>
<xsl:output method="html" indent="yes"/>

<xsl:template match="/root">

  <html>
    <head>
      <script type="text/javascript" language="javascript">
        forwardAction();

		function forwardAction()
		{
			//get error message
			var error = "<xsl:value-of select='/root/error_message'/>";
			
			if((error == null) || (error.length &lt; 1))
			{
				var forwardAction = "<xsl:value-of select='/root/forwarding_action'/>";
				var orderType = "<xsl:value-of select='$msg_order_type'/>";
				if(!forwardAction.match(/^\w/)){
				   
				    forwardAction=forwardAction.substr(1);
				}
								
				parent.resetFormActionAndSubmit(forwardAction, "", orderType, "", "", "");
			}
			else
			{
				parent.hideWaitMessage("content", "wait");
				alert(error);
			} 
        }
      </script>
    </head>
    <body></body>
  </html>
</xsl:template>

</xsl:stylesheet>