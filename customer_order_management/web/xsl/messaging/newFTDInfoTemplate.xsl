
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="mercFTDFillerCode.xsl"/>
	<xsl:import href="dateRangeSelect.xsl"/>
<xsl:key name="security" match="/message/security/data" use="name"/>
	<xsl:output method="html" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" indent="yes"/>
	<xsl:template name="newFTDInfoTemplate">
		<xsl:param name="destination"/>
		<xsl:param name="new_msg_message_type"/>
		<xsl:param name="msg_message_type"/>
		<xsl:variable name="xvRecipientCty" select="message/recipientCountry"/>
		<xsl:variable name="defaultOccasionCode">
			<xsl:for-each select="message/occasions/occasion">
				<xsl:if test="$xvOccasionCode = occasion_id">
					<xsl:value-of select="occasion_id"/>
				</xsl:if>
			</xsl:for-each>
		</xsl:variable>
		<xsl:variable name="defaultOtherOccasionCode">
			<xsl:for-each select="message/occasions/occasion">
				<xsl:if test="description = 'OTHER' or description = 'Other' or description = 'other'">
					<xsl:value-of select="occasion_id"/>
				</xsl:if>
			</xsl:for-each>
		</xsl:variable>
		
		<xsl:variable name="ftdType">

			<xsl:choose>
				<xsl:when test="$new_msg_message_type !=''">
					<xsl:value-of select="$new_msg_message_type"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$msg_message_type"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="$destination ='' ">
				<!-- COMP order created from Communication screen -->

				<xsl:choose>
					<xsl:when test="$msg_message_type ='Mercury'">
						<xsl:call-template name="mercFTDFillerCode"/>

						<tr>
							<td class="label">SENDER CODE:</td>

							<td class="datum">
								<xsl:value-of select="$xvSendingFloristCode"/>
								<input type="hidden" name="sending_florist_code" id="sending_florist_code" value="{$xvSendingFloristCode}"/>
							</td>
						</tr>

						<tr>
							<td class="label">DELIVER TO:</td>

							<td class="datum">
								<input type="text" size="50" maxlength="60" name="recipient_name" id="recipient_name" value="{$xvRecipientName}" onKeyPress="return noEnter();"/>
								<span id="invalidRecipientName" class="RequiredFieldTxt" style="display: none;">
								</span>
							</td>
						</tr>

						<tr>
							<td class="label">BUSINESS NAME:</td>

							<td class="datum">
								<input type="text" size="50" name="business_name" id="business_name" value="{normalize-space(message/businessName)}" onKeyPress="return noEnter();"/>
								<span id="invalidBusiness_name" class="RequiredFieldTxt" style="display: none;">
								</span>
							</td>
						</tr>

						<tr>
							<td class="label">STREET ADDRESS APT:</td>

							<td class="datum">
								<input type="text" size="50" maxlength="100" name="recipient_street" id="recipient_street" value="{$xvRecipientStreet}" onKeyPress="return noEnter();"/>
								<span id="invalidRecipientStreet" class="RequiredFieldTxt" style="display: none;">
								</span>
							</td>
						</tr>

						<tr>
							<td class="label">CITY, STATE, ZIP:</td>

							<td class="datum">
								<input type="text" size="30" maxlength="50" name="recipient_city" id="recipient_city" value="{$xvRecipientCity}" onKeyPress="return noEnter();"/>

								<select name="recipient_state" id="recipient_state">
									<xsl:for-each select="message/STATES/STATE">
										<option value="{id}">
											<xsl:if test="id =$xvRecipientState">
												<xsl:attribute name="selected">true</xsl:attribute>
											</xsl:if>
											<xsl:value-of select="name"/>
										</option>
									</xsl:for-each>
								</select>

								<input type="text" size="9" maxlength="6" name="recipient_zip" id="recipient_zip" value="{$xvRecipientZip}" onKeyPress="return noEnter();"/>
								<span id="invalidRecipientCityStateZip" class="RequiredFieldTxt" style="display: none;">
								</span>
							</td>
						</tr>

						<tr>
							<td class="label">COUNTRY:</td>

							<td class="datum">

								<xsl:choose>
									<xsl:when test="$xvRecipientCty='US' or $xvRecipientCty='CA'">
										<input type="text" size="2" maxlength="2" name="recipient_country" id="recipient_country" value="{$xvRecipientCty}" onKeyPress="return noEnter();"/>
									</xsl:when>
									<xsl:otherwise>
										<input type="hidden" name="recipient_country" id="recipient_country" value="{$xvRecipientCty}"/>
										<xsl:value-of select="$xvRecipientCty"/>
									</xsl:otherwise>
								</xsl:choose>
								<span id="invalidRecipientCityStateZip" class="RequiredFieldTxt" style="display: none;">
								</span>
							</td>
						</tr>

						<tr>
							<td class="label">PHONE:</td>

							<td class="datum">

								<input type="text" maxlength="3" size="3" name="recipient_phone_area" id="recipient_phone_area" value="{substring($xvRecipientPhone, 1, 3)}" onKeyPress="return noEnter();"/>/<input type="text" maxlength="3" size="3" name="recipient_phone_prefix" id="recipient_phone_prefix" value="{substring($xvRecipientPhone, 4, 3)}" onKeyPress="return noEnter();"/>
												-<input type="text" maxlength="4" size="4" name="recipient_phone_number" id="recipient_phone_number" value="{substring($xvRecipientPhone, 7)}" onKeyPress="return noEnter();"/>
								<span id="invalidRecipientPhone" class="RequiredFieldTxt" style="display: none;">
								</span>
							</td>
						</tr>


						<tr>
							<td class="label">
										DELIVERY DATE
								<xsl:if test="$xvOrderDeliveryDateEnd != ''">
									<xsl:text> RANGE</xsl:text>
								</xsl:if>
										:
							</td>
							<td class="datum">
								<select name="delivery_month" id="delivery_month">
									<option value="0">January</option>
									<option value="1">February</option>
									<option value="2">March</option>
									<option value="3">April</option>
									<option value="4">May</option>
									<option value="5">June</option>
									<option value="6">July</option>
									<option value="7">August</option>
									<option value="8">September</option>
									<option value="9">October</option>
									<option value="10">November</option>
									<option value="11">December</option>
								</select>

								<input type="text" name="delivery_date" id="delivery_date" maxlength="2" size="2" onKeyPress="return noEnter();"/>
								<span id="invalidOrderDeliveryDate" class="RequiredFieldTxt" style="display: none;"></span>
							</td>
						</tr>
						<xsl:if test="$xvOrderDeliveryDateEnd != ''">
							<tr>
								<td class="label"/>
								<td class="datum">
									<select name="delivery_month_end" id="delivery_month_end">
										<option value="0">
											<xsl:if test="starts-with($xvOrderDeliveryDateEnd, 'Jan')">
												<xsl:attribute name="selected">
																	selected
												</xsl:attribute>
											</xsl:if>
															January
										</option>
										<option value="1">
											<xsl:if test="starts-with($xvOrderDeliveryDateEnd, 'Feb')">
												<xsl:attribute name="selected">
																	selected
												</xsl:attribute>
											</xsl:if>
															February
										</option>
										<option value="2">
											<xsl:if test="starts-with($xvOrderDeliveryDateEnd, 'Mar')">
												<xsl:attribute name="selected">
																	selected
												</xsl:attribute>
											</xsl:if>
															March
										</option>
										<option value="3">
											<xsl:if test="starts-with($xvOrderDeliveryDateEnd, 'Apr')">
												<xsl:attribute name="selected">
																	selected
												</xsl:attribute>
											</xsl:if>
															April
										</option>
										<option value="4">
											<xsl:if test="starts-with($xvOrderDeliveryDateEnd, 'May')">
												<xsl:attribute name="selected">
																	selected
												</xsl:attribute>
											</xsl:if>
															May
										</option>
										<option value="5">
											<xsl:if test="starts-with($xvOrderDeliveryDateEnd, 'Jun')">
												<xsl:attribute name="selected">
																	selected
												</xsl:attribute>
											</xsl:if>
															June
										</option>
										<option value="6">
											<xsl:if test="starts-with($xvOrderDeliveryDateEnd, 'Jul')">
												<xsl:attribute name="selected">
																	selected
												</xsl:attribute>
											</xsl:if>
															July
										</option>
										<option value="7">
											<xsl:if test="starts-with($xvOrderDeliveryDateEnd, 'Aug')">
												<xsl:attribute name="selected">
																	selected
												</xsl:attribute>
											</xsl:if>
															August
										</option>
										<option value="8">
											<xsl:if test="starts-with($xvOrderDeliveryDateEnd, 'Sep')">
												<xsl:attribute name="selected">
																	selected
												</xsl:attribute>
											</xsl:if>
															September
										</option>
										<option value="9">
											<xsl:if test="starts-with($xvOrderDeliveryDateEnd, 'Oct')">
												<xsl:attribute name="selected">
																	selected
												</xsl:attribute>
											</xsl:if>
															October
										</option>
										<option value="10">
											<xsl:if test="starts-with($xvOrderDeliveryDateEnd, 'Nov')">
												<xsl:attribute name="selected">
																	selected
												</xsl:attribute>
											</xsl:if>
															November
										</option>
										<option value="11">
											<xsl:if test="starts-with($xvOrderDeliveryDateEnd, 'Dec')">
												<xsl:attribute name="selected">
																	selected
												</xsl:attribute>
											</xsl:if>
															December
										</option>
									</select>
									<input type="text" name="delivery_date_end" id="delivery_date_end" maxlength="2" size="2" value="{substring($xvOrderDeliveryDateEnd, 5, 2)}" onKeyPress="return noEnter();"/>
									<span id="invalidOrderDeliveryDateEnd" class="RequiredFieldTxt" style="display: none;"></span>
								</td>
							</tr>
						</xsl:if>


						<!--***********************************************-->
						<tr>
							<td class="label">FIRST CHOICE:</td>

							<td class="datum">
                  <xsl:value-of select="$xvFirstChoice" disable-output-escaping="yes"/>
                  <input type="hidden" name="first_choice" id="first_choice" value="{$xvFirstChoice}"/>
              </td>

						</tr>

						<tr>
							<td class="label">ALLOWED SUBSTITUTION:</td>

							<td class="datum">
                  <xsl:value-of select="$xvSecondChoice" disable-output-escaping="yes"/>
                  <input type="hidden" name="allowed_substitution" id="allowed_substitution" value="{$xvSecondChoice}"/>
              </td>

						</tr>
						<tr>
							<td class="label">PRICE INCLUDING DELIVERY:</td>

							<td class="datum">
                  <xsl:value-of select="$xvNewPrice"/>
                  <input type="hidden" name="florist_prd_price" id="florist_prd_price" value="{$xvNewPrice}"/>
              </td>
            </tr>

						<tr>
							<td class="label">CARD:</td>

							<td class="datum">
								<input type="text" name="card_message" id="card_message" size="70" maxlength="240" onKeyPress="return noEnter();">
									<xsl:attribute name="value">
										<xsl:choose>
											<xsl:when test="$xvCardMessage = ''">
												<xsl:text>NO MESSAGE ENTERED</xsl:text>
											</xsl:when>

											<xsl:otherwise>
												<xsl:value-of select="$xvCardMessage"/>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:attribute>
								</input>
								<span id="invalidCardMessage" class="RequiredFieldTxt" style="display: none;">
								</span>
							</td>
						</tr>
						<xsl:if test="$msg_message_type ='Mercury'">
							<tr>
								<td class="label">OCCASION CODE:</td>

								<td class="datum">
								<select name="occasion_code" id="occasion_code">
									<xsl:for-each select="message/occasions/occasion">
										<option value="{occasion_id}">
											<xsl:if test="$defaultOccasionCode = occasion_id">
												<xsl:attribute name="selected">selected</xsl:attribute>
											</xsl:if>
											<xsl:if test="string-length($defaultOccasionCode) = 0 and $defaultOtherOccasionCode = occasion_id">
												<xsl:attribute name="selected">selected</xsl:attribute>
											</xsl:if>
											<xsl:value-of select="occasion_id"/>&#160;&#160;<xsl:value-of select="description"/> 
										</option>
									</xsl:for-each>
								</select>
								</td>
							</tr>
						</xsl:if>
						<tr>
							<td class="label">SPECIAL INSTRUCTIONS:</td>

							<td class="datum">
								<input type="text" name="spec_instruction" id="spec_instruction" size="70" maxlength="240" onKeyPress="return noEnter();">
									<xsl:attribute name="value">
										<xsl:choose>
											<xsl:when test="$xvInstructions = ''">
												<xsl:text>NONE</xsl:text>
											</xsl:when>

											<xsl:otherwise>
												<xsl:value-of select="$xvInstructions"/>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:attribute>
								</input>
								<span id="invalidInstructions" class="RequiredFieldTxt" style="display: none;">
								</span>
							</td>
						</tr>
					</xsl:when>
					<xsl:otherwise>
						<!-- Venus COMP order -->

						<tr>
							<td class="label">FILLER CODE:</td>

							<td class="datum">
								<xsl:value-of select="$xvFillingFloristCode"/>
								<input type="hidden" name="filling_florist_code" id="filling_florist_code" value="{$xvFillingFloristCode}"/>
							</td>
						</tr>

						<tr>
							<td class="label">SENDER CODE:</td>

							<td class="datum">
								<xsl:value-of select="$xvSendingFloristCode"/>
								<input type="hidden" name="sending_florist_code" id="sending_florist_code" value="{$xvSendingFloristCode}"/>
							</td>
						</tr>

						<tr>
							<td class="label">DELIVER TO:</td>

							<td class="datum">
								<input type="text" size="50" name="recipient_name" id="recipient_name" value="{$xvRecipientName}" onKeyPress="return noEnter();"/>
								<span id="invalidRecipientName" class="RequiredFieldTxt" style="display: none;">
								</span>
							</td>
						</tr>

						<tr>
							<td class="label">BUSINESS NAME:</td>

							<td class="datum">
								<input type="text" size="50" name="business_name" id="business_name" value="{normalize-space(message/businessName)}" onKeyPress="return noEnter();"/>
								<span id="invalidBusiness_name" class="RequiredFieldTxt" style="display: none;">
								</span>
							</td>
						</tr>

						<tr>
							<td class="label">STREET ADDRESS APT:</td>

							<td class="datum">
								<input type="text" size="50" name="recipient_street" id="recipient_street" value="{$xvRecipientStreet}" onKeyPress="return noEnter();"/>
								<span id="invalidRecipientStreet" class="RequiredFieldTxt" style="display: none;">
								</span>
							</td>
						</tr>

						<tr>
							<td class="label">CITY, STATE, ZIP:</td>

							<td class="datum">
								<input type="text" size="30" name="recipient_city" id="recipient_city" value="{$xvRecipientCity}" onKeyPress="return noEnter();"/>

								<select name="recipient_state" id="recipient_state">
									<xsl:for-each select="message/STATES/STATE">
										<option value="{id}">
											<xsl:if test="id =$xvRecipientState">
												<xsl:attribute name="selected">true</xsl:attribute>
											</xsl:if>
											<xsl:value-of select="name"/>
										</option>
									</xsl:for-each>
								</select>

								<input type="text" size="9" name="recipient_zip" id="recipient_zip" value="{$xvRecipientZip}" onKeyPress="return noEnter();"/>
								<span id="invalidRecipientCityStateZip" class="RequiredFieldTxt" style="display: none;">
								</span>
							</td>
						</tr>

						<tr>
							<td class="label">PHONE:</td>

							<td class="datum">

								<input type="text" maxlength="3" size="3" name="recipient_phone_area" id="recipient_phone_area" value="{substring($xvRecipientPhone, 1, 3)}" onKeyPress="return noEnter();"/>/<input type="text" maxlength="3" size="3" name="recipient_phone_prefix" id="recipient_phone_prefix" value="{substring($xvRecipientPhone, 4, 3)}" onKeyPress="return noEnter();"/>
												-<input type="text" maxlength="4" size="4" name="recipient_phone_number" id="recipient_phone_number" value="{substring($xvRecipientPhone, 7)}" onKeyPress="return noEnter();"/>
								<span id="invalidRecipientPhone" class="RequiredFieldTxt" style="display: none;">
								</span>
							</td>
						</tr>

						<xsl:call-template name="dateRangeSelect"/>

						<tr>
							<td class="label">FIRST CHOICE:</td>

							<td class="datum">
								<xsl:value-of select="$xvFirstChoice" disable-output-escaping="yes"/>
								<input type="hidden" name="first_choice" id="first_choice" value="{$xvFirstChoice}"/>
							</td>
						</tr>

						<tr>
							<td class="label">ALLOWED SUBSTITUTION:</td>

							<td class="datum">NONE</td>
						</tr>


						<tr>
							<td class="label">Vendor Cost:</td>

							<td class="datum">
								<input type="text" name="florist_prd_price" id="florist_prd_price" size="8" maxlength="8" value="{$xvVendorCost}" onKeyPress="return noEnter();"/>
								<span id="invalidPrice" class="RequiredFieldTxt" style="display: none;">
								</span>
							</td>
						</tr>

						<tr>
							<td class="label">CARD:</td>

							<td class="datum">
								<input type="text" name="card_message" id="card_message" size="70" onKeyPress="return noEnter();">
									<xsl:attribute name="value">
										<xsl:choose>
											<xsl:when test="$xvCardMessage = ''">
												<xsl:text>NO MESSAGE ENTERED</xsl:text>
											</xsl:when>

											<xsl:otherwise>
												<xsl:value-of select="$xvCardMessage"/>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:attribute>
								</input>
								<span id="invalidCardMessage" class="RequiredFieldTxt" style="display: none;">
								</span>
							</td>
						</tr>



						<tr>
							<td class="label">SPECIAL INSTRUCTIONS:</td>

							<td class="datum">
								<input type="text" name="spec_instruction" id="spec_instruction" size="70" onKeyPress="return noEnter();">
									<xsl:attribute name="value">
										<xsl:choose>
											<xsl:when test="$xvInstructions = ''">
												<xsl:text>NONE</xsl:text>
											</xsl:when>

											<xsl:otherwise>
												<xsl:value-of select="$xvInstructions"/>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:attribute>
								</input>
								<span id="invalidInstructions" class="RequiredFieldTxt" style="display: none;">
								</span>
							</td>
						</tr>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<!-- Modify Order COMP  -->
				<xsl:call-template name="mercFTDFillerCode"/>

				<tr>
					<td class="label">SENDER CODE:</td>

					<td class="datum">
						<xsl:value-of select="$xvSendingFloristCode"/>
						<input type="hidden" name="sending_florist_code" id="sending_florist_code" value="{$xvSendingFloristCode}"/>
					</td>
				</tr>

				<tr>
					<td class="label">DELIVER TO:</td>

					<td class="datum">
						<xsl:value-of select="$xvRecipientName"/>
						<input type="hidden" name="recipient_name" id="recipient_name" value="{$xvRecipientName}"/>
						<span id="invalidRecipientName" class="RequiredFieldTxt" style="display: none;">
						</span>
					</td>
				</tr>

					<tr>
						<td class="label">BUSINESS NAME:</td>

						<td class="datum">
							<xsl:value-of select="normalize-space(message/businessName)"/>
							<input type="hidden" name="business_name" id="business_name" value="{normalize-space(message/businessName)}"/>
							<span id="invalidBusiness_name" class="RequiredFieldTxt" style="display: none;">
							</span>
						</td>
					</tr>

				<tr>
					<td class="label">STREET ADDRESS APT:</td>

					<td class="datum">
						<xsl:value-of select="$xvRecipientStreet"/>
						<input type="hidden" name="recipient_street" id="recipient_street" value="{$xvRecipientStreet}"/>
						<span id="invalidRecipientStreet" class="RequiredFieldTxt" style="display: none;">
						</span>
					</td>
				</tr>

				<tr>
					<td class="label">CITY, STATE, ZIP:</td>

					<td class="datum">
						<xsl:value-of select="$xvRecipientCity"/>
						<xsl:value-of select="$xvRecipientState"/>, <xsl:value-of select="$xvRecipientZip"/>
						<input type="hidden" name="recipient_city" id="recipient_city" value="{$xvRecipientCity}"/>
						<input type="hidden" name="recipient_state" id="recipient_state" value="{$xvRecipientState}"/>
						<input type="hidden" name="recipient_zip" id="recipient_zip" value="{$xvRecipientZip}"/>

						<span id="invalidRecipientCityStateZip" class="RequiredFieldTxt" style="display: none;">
						</span>
					</td>
				</tr>

				<tr>
					<td class="label">COUNTRY:</td>

					<td class="datum">


						<input type="hidden" name="recipient_country" id="recipient_country" value="{$xvRecipientCty}"/>
						<xsl:value-of select="$xvRecipientCty"/>

						<span id="invalidRecipientCityStateZip" class="RequiredFieldTxt" style="display: none;">
						</span>
					</td>
				</tr>

				<tr>
					<td class="label">PHONE:</td>

					<td class="datum">
						<xsl:value-of select="substring($xvRecipientPhone, 1, 3)"/>/<xsl:value-of select="substring($xvRecipientPhone, 4, 3)"/>-<xsl:value-of select="substring($xvRecipientPhone, 7)"/>
						<input type="hidden" maxlength="3" size="3" name="recipient_phone_area" id="recipient_phone_area" value="{substring($xvRecipientPhone, 1, 3)}"/>
						<input type="hidden" name="recipient_phone_prefix" id="recipient_phone_prefix" value="{substring($xvRecipientPhone, 4, 3)}"/>
						<input type="hidden" name="recipient_phone_number" id="recipient_phone_number" value="{substring($xvRecipientPhone, 7)}"/>
						<span id="invalidRecipientPhone" class="RequiredFieldTxt" style="display: none;">
						</span>
					</td>
				</tr>

				<tr>
					<td class="label">DELIVERY DATE:</td>
					<td class="datum">
						<xsl:value-of select="$xvOrderDeliveryDate"/>
						<span id="invalidOrderDeliveryDate" class="RequiredFieldTxt" style="display: none;">
						</span>
					</td>
				</tr>
				<!--***********************************************-->
				<tr>
					<td class="label">FIRST CHOICE:</td>

					<td class="datum">
						<xsl:value-of select="$xvFirstChoice" disable-output-escaping="yes"/>
						<input type="hidden" name="first_choice" id="first_choice" value="{$xvFirstChoice}"/>
						<span id="invalidFirstChoice" class="RequiredFieldTxt" style="display: none;">
						</span>
					</td>
				</tr>


				<tr>
					<td class="label">ALLOWED SUBSTITUTION:</td>
					<xsl:choose>
						<xsl:when test="$ftdType ='Mercury'">

							<td class="datum">
								<xsl:value-of select="$xvSecondChoice" disable-output-escaping="yes"/>
								<input type="hidden" name="allowed_substitution" id="allowed_substitution" value="{$xvSecondChoice}"/>
								<span id="invalidSecondChoice" class="RequiredFieldTxt" style="display: none;">
								</span>
							</td>
						</xsl:when>
						<xsl:otherwise>
							<td class="datum">NONE</td>
						</xsl:otherwise>
					</xsl:choose>
				</tr>

				<tr>

					<td class="label">
						<xsl:choose>
							<xsl:when test="$ftdType ='Mercury'">PRICE INCLUDING DELIVERY:</xsl:when>
							<xsl:otherwise>Vendor Cost:</xsl:otherwise>
						</xsl:choose>
					</td>

					<td class="datum">
						<xsl:value-of select="$xvNewPrice"/>
						<input type="hidden" name="florist_prd_price" id="florist_prd_price" value="{$xvNewPrice}"/>
						<span id="invalidPrice" class="RequiredFieldTxt" style="display: none;">
						</span>
					</td>
				</tr>

				<tr>
					<td class="label">CARD:</td>

					<td class="datum">
						<xsl:choose>
							<xsl:when test="$xvCardMessage = ''">
								<xsl:text>NO MESSAGE ENTERED</xsl:text>
							</xsl:when>

							<xsl:otherwise>
								<xsl:value-of select="$xvCardMessage"/>
							</xsl:otherwise>
						</xsl:choose>
						<input type="hidden" name="card_message" id="card_message">
							<xsl:attribute name="value">
								<xsl:choose>
									<xsl:when test="$xvCardMessage = ''">
										<xsl:text>NO MESSAGE ENTERED</xsl:text>
									</xsl:when>

									<xsl:otherwise>
										<xsl:value-of select="$xvCardMessage"/>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:attribute>
						</input>
						<span id="invalidCardMessage" class="RequiredFieldTxt" style="display: none;">
						</span>
					</td>
				</tr>

				<tr>
					<td class="label">OCCASION CODE:</td>

					<td class="datum">
						<xsl:value-of select="message/occasionDesc"/>
						<input type="hidden" value="{$xvOccasionCode}"/>
					</td>
				</tr>

				<tr>
					<td class="label">SPECIAL INSTRUCTIONS:</td>

					<td class="datum">

						<xsl:choose>
							<xsl:when test="$xvInstructions = ''">
								<xsl:text>NONE</xsl:text>
							</xsl:when>

							<xsl:otherwise>
								<xsl:value-of select="$xvInstructions"/>
							</xsl:otherwise>
						</xsl:choose>
						<input type="hidden" name="spec_instruction" id="spec_instruction">
							<xsl:attribute name="value">
								<xsl:choose>
									<xsl:when test="$xvInstructions = ''">
										<xsl:text>NONE</xsl:text>
									</xsl:when>

									<xsl:otherwise>
										<xsl:value-of select="$xvInstructions"/>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:attribute>
						</input>
						<span id="invalidInstructions" class="RequiredFieldTxt" style="display: none;">
						</span>
					</td>
				</tr>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>