
<!DOCTYPE stylesheet [
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:import href="../securityanddata.xsl"/>
	<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>
	<xsl:import href="messagingSecurityAndData.xsl"/>
	<xsl:import href="mercMessFormatPhone.xsl"/>
        <xsl:key name="security" match="/root/security/data" use="name"/>

	<xsl:output method="html" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" indent="yes"/>

	<xsl:variable name="xvZipCode" select="normalize-space(/root/zip_code)"/>
	<xsl:variable name="xvLoadMemberDataUrl" select="normalize-space(/root/load_member_data_url)"/>
	<xsl:variable name="xvModifyOrderFlag" select="normalize-space(/root/modify_order_flag)"/>
        
	<xsl:template match="/">

		<html>
			<head>
				<title>Florist Lookup</title>

				<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
				<meta http-equiv="language" content="en-us"/>

				<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
				<link rel="stylesheet" type="text/css" href="css/mercuryMessage.css"/>
				<link rel="stylesheet" type="text/css" href="css/floristLookup.css"/>
				<script type="text/javascript" src="js/commonUtil.js">;</script>
				<script type="text/javascript" src="js/mercuryMessage.js">;</script>
				<script><![CDATA[
				var isChecked;
				var checkedBox;

				function init()
				{
					document.oncontextmenu = contextMenuHandler;
					document.onkeydown = backKeyHandler;

					isChecked = false;
					checkedBoxId = null;
					document.getElementById("send1").disabled = true;
					document.getElementById("send2").disabled = true;
				}

				function toggleFloristCheckbox()
				{
					if(isChecked == false)
					{
						isChecked = true;
						checkedBox = event.srcElement;
						event.srcElement.checked = true;
						document.getElementById("send1").disabled = false;
						document.getElementById("send2").disabled = false;
					}
					else
					{
						checkedBox.checked = false;
						if(event.srcElement == checkedBox)
						{
							isChecked = false;
							document.getElementById("send1").disabled = true;
							document.getElementById("send2").disabled = true;
						}
						else
						{
							event.srcElement.checked = true;
							checkedBox = event.srcElement;
						}
					}
				}

				function sendFTD()
				{
					var returnVal=checkedBox.value.split("|");
					//alert("returnVal="+returnVal);
					top.returnValue = returnVal;
					top.close();
				}
			]]>
				</script>
			</head>

			<body onload="init();" style="margin: 0; padding: 0;">
				<form id="floristLookupForm" method="post">
					<input type="hidden" name="load_member_data_url" id="load_member_data_url" value="{$xvLoadMemberDataUrl}"/>
					<xsl:call-template name="securityanddata"/>
					<xsl:call-template name="decisionResultData"/>
					<xsl:call-template name="messagingSecurityAndData"/>

					<table style="border: 0;" class="mainTable" width="98%" align="center" cellpadding="0" cellspacing="1">
						<tr>
							<td>
								<div class="banner" style="margin-bottom: 5px; margin-top: 1px;">FLORIST LOOKUP</div>

								<div class="floatleft">
									<img border="0" src="images/wwwftdcom_131x32.gif" width="131" height="32"/>
								</div>

								<br/>

                <span class="PopupHeader" style="float: left; margin-left: 20%">Available Florists in Zip Code <xsl:value-of select="$xvZipCode"/></span>

								<div class="floatright" style="margin-bottom: 5px;">
									<button type="button" class="BlueButton" id="send1" tabindex="-1" accesskey="S" onclick="sendFTD();" style="margin-right: 5px;">
										<xsl:choose>
											<xsl:when test="$xvModifyOrderFlag='Y'">(S)ave</xsl:when>
											<xsl:otherwise>(S)end</xsl:otherwise>
										</xsl:choose>
									</button>

									<button type="button" class="BlueButton" accesskey="C" tabindex="-1" onclick="top.close();" style="margin-left: 5px;">(C)lose</button>
								</div>

								<div style="clear: both; padding-right: 16px; background: #006699;">
									<table width="100%" cellspacing="0" cellpadding="0">
										<tr class="ColHeader">
											<th class="selected">&nbsp;</th>

                      <th class="primaryBackupFlorist">&nbsp;</th>

											<th class="memberNumber">Member #</th>

											<th class="name">Name</th>

											<th class="phone">Phone</th>

											<th class="sunday">Sun</th>

											<th class="mercury">Merc</th>

											<th class="goTo">GoTo</th>

											<th class="cutOff">Cutoff</th>

											<th class="alreadyReceived">Recv'd</th>

											<th class="status">Status</th>
										</tr>
									</table>
									<!-- End of table headings. -->
								</div>

								<div style="overflow: auto; height: 226px; border: 1px solid #006699; padding-right: 16px;">
									<table cellpadding="0" cellspacing="0" style="width: 100%; margin-right: -16px;">
										<tr align="center">
												<xsl:if test="count(/root/floristList/item) = 0">
		                                        	 <td class="ErrorMessage">All florists in this area are currently unavailable to fulfill this order.</td>
												</xsl:if>  
										</tr>
																				
										<xsl:for-each select="/root/floristList/item">
											<xsl:variable name="floristCode" select="normalize-space(floristId)"/>
											<xsl:variable name="floristStatus" select="normalize-space(blockStatus)"/>
											<xsl:variable name="mercuryStatus" select="normalize-space(mercuryFlag)"/>
											
											<tr>
											    <xsl:attribute name="bgcolor">
													<xsl:choose>
													  <xsl:when test="normalize-space(floristSuspended) = 'N'">
													    <xsl:text>ffffff</xsl:text>
													  </xsl:when>
													  <xsl:otherwise>
													    <xsl:text>cccccc</xsl:text>
													  </xsl:otherwise>
													</xsl:choose>
											    </xsl:attribute>
                                                                                                                                                                              
												<td class="selected">
													<xsl:variable name="floristInfo" select="concat($floristCode, '|', $floristStatus, '|', $mercuryStatus)"/>
													     <input type="checkbox" onclick="toggleFloristCheckbox();" name="{$floristCode}" id="{$floristCode}" value="{$floristInfo}"/>
                                                                                                </td>

                        <td class="primaryBackupFlorist">
                          <xsl:choose>
                            <xsl:when test="normalize-space(priority) = '1'"><img border="0" src="images/primaryFlorist.gif"/></xsl:when>
                            <xsl:when test="normalize-space(priority) = '2'"><img border="0" src="images/backupFlorist.gif"/></xsl:when>
                            <xsl:otherwise>&nbsp;</xsl:otherwise>
                          </xsl:choose>
                        </td>

                        <td class="memberNumber">
													<a href="#" class="memberLink" onclick="javascript:openFlorist('{$floristCode}');">
														<xsl:value-of select="$floristCode"/>
													</a>
												</td>

                        <td class="name">
													<xsl:value-of select="normalize-space(floristName)"/>
                         <!--Fix for Defect. No. 7470 -->
													<xsl:if test="normalize-space(codification) != ''">
														<strong>
														&nbsp;<xsl:value-of select="normalize-space(codification)"/>
                            </strong>
														<xsl:if test="normalize-space(codificationBlockFlag) = 'true'">
															<xsl:text> - blocked</xsl:text>
														</xsl:if>
													</xsl:if>
												</td>

                        <td class="phone">
													<xsl:call-template name="mercMessFormatPhone">
														<xsl:with-param name="phoneNumber" select="normalize-space(phoneNumber)"/>
													</xsl:call-template>
												</td>

                        <td class="sunday">													
													<xsl:if test="normalize-space(sundayDeliveryFlag) = 'Y'">
														<xsl:value-of select="normalize-space(sundayDeliveryFlag)"/>
													</xsl:if>
												</td>

                        <td class="mercury">
													<xsl:if test="$mercuryStatus = 'M'">
														<xsl:text>Merc</xsl:text>
													</xsl:if>
												</td>

                        <td class="goTo">
													<xsl:if test="normalize-space(superFloristFlag) = 'Y'">
														<xsl:text>Y</xsl:text>
													</xsl:if>
												</td>

                        <td class="cutOff">
													<xsl:call-template name="extractTime">
														<xsl:with-param name="timestamp" select="normalize-space(cutoffTime)"/>
													</xsl:call-template>
												</td>

                        <td class="alreadyReceived">

													<xsl:value-of select="normalize-space(usedSequence)"/>
												</td>

                        <td class="status">
													<xsl:value-of select="$floristStatus"/>
												</td>
											</tr>

                      <tr><td colspan="11"><hr/></td></tr>
                      
										</xsl:for-each>
									</table>
								</div>
								<!-- End of scrolling div for table -->

								<table class="PaneSection" cellspacing="0" cellpadding="0" style="margin-top: 5px; ">
									<tr>
										<td colspan="2" style="text-align: right;">
											<button type="button" class="BlueButton" id="send2" accesskey="S" onclick="sendFTD();" style="margin-right: 5px;">
												<xsl:choose>
													<xsl:when test="$xvModifyOrderFlag='Y'">(S)ave</xsl:when>
													<xsl:otherwise>(S)end</xsl:otherwise>
												</xsl:choose>
											</button>

											<button type="button" class="BlueButton" accesskey="C" onclick="top.close();" style="margin-left: 5px;">(C)lose</button>
										</td>
									</tr>

                  <tr>
                    <td style="vertical-align:center; width:4%;"><img border="0" src="images/primaryFlorist.gif"/></td>
                    <td style="vertical-align:center; text-align:left; width:96%;">&nbsp;=&nbsp;Primary Florist assigned to source code and zip code</td>
                  </tr>

                  <tr><td>&nbsp;</td></tr>

                  <tr>
                    <td style="vertical-align:center; width:4%;"><img border="0" src="images/backupFlorist.gif"/></td>
                    <td style="vertical-align:center; text-align:left; width:96%;">&nbsp;=&nbsp;Backup Florist assigned to source code and zip code</td>
                  </tr>

								</table>
							</td>
						</tr>
					</table>
					<!-- End of mainTable -->
				</form>
			</body>
		</html>
	</xsl:template>


	<xsl:template name="extractTime">
		<xsl:param name="timestamp"/>

		<xsl:if test="$timestamp != ''">
			<xsl:variable name="militaryHours" select="substring($timestamp, 1, 2)"/>

			<xsl:variable name="hours">
				<xsl:choose>
					<xsl:when test="$militaryHours = 0">
						<xsl:text>12</xsl:text>
					</xsl:when>

					<xsl:when test="$militaryHours &gt; 12">
						<xsl:value-of select="$militaryHours - 12"/>
					</xsl:when>

					<xsl:otherwise>
						<xsl:value-of select="$militaryHours"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>

			<xsl:variable name="meridian">
				<xsl:choose>
					<xsl:when test="$militaryHours &lt; 12">
						<xsl:text>AM</xsl:text>
					</xsl:when>

					<xsl:otherwise>
						<xsl:text>PM</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>

			<xsl:variable name="minutes" select="substring($timestamp, 3, 2)"/>

			<xsl:value-of select="concat($hours, ':', $minutes, $meridian)"/>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c) 2004-2005. Progress Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperMetaTag><MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/><MapperBlockPosition></MapperBlockPosition></MapperMetaTag>
</metaInformation>
-->