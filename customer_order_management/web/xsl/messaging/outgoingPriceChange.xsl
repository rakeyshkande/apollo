<!DOCTYPE stylesheet [
	<!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:import href="../header.xsl"/>
<xsl:import href="../footer.xsl"/>
<xsl:import href="mercMessButtons.xsl"/>
<xsl:import href="../securityanddata.xsl"/>
<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>
<xsl:import href="messagingSecurityAndData.xsl"/>
<xsl:import href="mercMessCommentTable.xsl"/>
<xsl:import href="mercMessReasonText.xsl"/>
<xsl:import href="formattingTemplates.xsl"/>
<xsl:import href="mercMessSendButton.xsl"/>
<xsl:key name="security" match="/message/security/data" use="name"/>

<xsl:output method="html"
			doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
			doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
			indent="yes"/>

<xsl:include href="mercMessFields.xsl"/>

<xsl:template match="/">

	<html>
		<head>
			<title><xsl:value-of select="$xvHeaderName"/></title>

			<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
			<meta http-equiv="language" content="en-us"/>

			<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
			<link rel="stylesheet" type="text/css" href="css/mercuryMessage.css"/>
			<script type="text/javascript" src="js/commonUtil.js">;</script>
			<script type="text/javascript" src="js/clock.js">;</script>
			
			<script type="text/javascript" src="js/mercuryMessage.js">;</script>
			<script type="text/javascript" src="js/FormChek.js">;</script>
			<script>
				<![CDATA[
				var actions = new Array();
				function init()
				{
					document.oncontextmenu = contextMenuHandler;
					document.onkeydown = backKeyHandler;
					var buttons = document.getElementsByTagName("BUTTON");
					
					for(var i = 0; i < buttons.length; i++)
					{
						if(buttons[i].id != "mainMenuButton" && buttons[i].id != "sendButton")
						{
							actions[buttons[i].id] = buttons[i].onclick;
							buttons[i].onclick = checkButtons;
						}else if(buttons[i].id == "mainMenuButton"){
							buttons[i].onclick=mainMenuAction;
						}
					}
					untabHeader();
				}		
			
				
				 //Determine if a new price increase requires authorization
				 function checkAuthorizationRequiredForPriceIncrease(oldPriceElementName, newPriceElementName, thresholdElementName)
				 {
				 	var oldprice = document.getElementById(oldPriceElementName).value;
				 	var newprice = document.getElementById(newPriceElementName).value;
				 	var threshold = $("#"+thresholdElementName)[0].value;
				 	
				 	// play it safe when comparing floating point numbers
				 	var oldp = parseFloat(oldprice) * 100;
				 	var newp = parseFloat(newprice) * 100;
				 	var thresh = parseFloat(threshold) * 100;
				 	var diffp = newp - oldp;
				 	
				 	if (diffp > thresh)
				 		return true;
				 	else
				 		return false;
				 }
				
				 
				 function hideAuthorization()
				 {
				     document.getElementById('ASKPDIV').style.display = "none";
				     parent.document.getElementById("manager_code").value = "";
				     parent.document.getElementById("manager_passwd").value = "";
				     parent.document.getElementById("manager_code").style.backgroundColor='white';
				     parent.document.getElementById("manager_passwd").style.backgroundColor='white';
				 }
				
				 function showAuthorization(color)
				 {
				     document.getElementById('ASKPDIV').style.display = "block";
				     parent.document.getElementById("manager_code").value = "";
				     parent.document.getElementById("manager_passwd").value = "";
				     parent.document.getElementById("manager_code").style.backgroundColor=color;
				     parent.document.getElementById("manager_passwd").style.backgroundColor=color;
				     parent.document.getElementById("manager_code").focus();
				 }
				 
				 function hasAuthorized() {
				  if (document.getElementById('ASKPDIV').style.display == "block") {
					  return true;
				  }
				  else 
					  return false
				 }
				 
				 function validatePriceIncreaseAuthorizeFields(approvalId, approvalPassword) {
				  var invalidManagerInfo = false;
				  if(isWhitespace(approvalId))
				  {
					  invalidManagerInfo = true;
				  }
				  if(isWhitespace(approvalPassword))
				  {
					  invalidManagerInfo = true;
				  }
				
				  if(invalidManagerInfo)
				  {
					  return false;
				  }
				  else
					  return true;
				 }
				 
				 function authorizePriceIncrease() {
					 var form = document.forms[0];
					
					 var approvalId = document.getElementById("manager_code").value;
					 var approvalPassword = document.getElementById("manager_passwd").value;
					 
					 if (!validatePriceIncreaseAuthorizeFields(approvalId, approvalPassword)) {
						 alert("Incorrect manager login and/or password.  Please correct marked fields.");
						 hideWaitMessage("content", "wait");
						 showAuthorization('pink');
						 return false;
					 }
					
					 var url = "sendPriceMessage.do";
					 var data = $('form').serialize() + "&authorize_price_increase=true";
					 
					 $.ajax({
						 type: "POST",
						 url: url,
						 data: data,
						 dataType: "xml",
						 async: false,
						 cache: false,
						 success: function(xml) {
							 //enable the comment box if we got the lock
							 var authorized = $(xml).find("AUTHORIZED").text();
							 
							 if (authorized == "Y") {
							 	form.submit();
							 } else {
							 	alert("Incorrect manager login and/or password.  Please correct marked fields.");
							 	hideWaitMessage("content", "wait");
							 	showAuthorization('pink');
							 }
						 },
						 error: function(jqXHR, textStatus, errorThrown) {
							alert('Server error while trying to authorize price increase. ' + errorThrown);
						 }
					 });
				 };


				function validateMessageForm()
				{		
					var type=document.getElementById("msg_message_detail_type").value;
									
					if(type=="ASKP")
					{
						return checkPrice("new_price", "invalidPrice");
					}
					else
					{
						return true;
					}
				}

				function authRequiredForPriceIncrease()
				{
					var type=document.getElementById("msg_message_detail_type").value;
									
					if(type=="ASKP" || type == "ANSP")
					{
						var authRequired = checkAuthorizationRequiredForPriceIncrease("message_old_price", "new_price", "PRICE_INCREASE_AUTH_THRESHOLD");
						
						return authRequired;
					}
					else
					{
						return true;
					}
				}


				function doSendPriceMessageAction()
				{
					showWaitMessage("content", "wait");

					if(validateMessageForm())
					{
						if (authRequiredForPriceIncrease())
						{
							if (hasAuthorized()) {
								authorizePriceIncrease();
							}
							else {
								hideWaitMessage("content", "wait");
								showAuthorization('white');
							}
						}
						else
						{
							hideAuthorization();
							document.forms[0].submit();							
						}
					}
					else
					{
						hideWaitMessage("content", "wait");
					}
				}
				]]>
			</script>

			<style>
				td.label {
					width: 128px;
				}
			</style>
			

		</head>

		<body onload="init();">
			<form id="outgoingPriceChangeForm" method="post" action="sendPriceMessage.do">

				<input type="hidden" name="delivery_date" id="delivery_date" value="{$xvOrderDeliveryDateTimestamp}"/>

				<xsl:call-template name="securityanddata"/>
				<xsl:call-template name="decisionResultData"/>
				<xsl:call-template name="messagingSecurityAndData"/>

				<input type="hidden" name="sending_florist_code" id="sending_florist_code" value="{$xvSendingFloristCode}"/>
				<input type="hidden" name="filling_florist_code" id="filling_florist_code" value="{$xvFillingFloristCode}"/>
				<input type="hidden" name="ftd_message_id" id="ftd_message_id" value="{$xvFtdMessageID}"/>
				<input type="hidden" name="PRICE_INCREASE_AUTH_THRESHOLD" id="PRICE_INCREASE_AUTH_THRESHOLD" value="{message/PRICE_INCREASE_AUTH_THRESHOLD}"/>
				<iframe name="unlockFrame" id="unlockFrame" height="0" width="0" border="0"></iframe>
				<iframe id="checkLock" width="0px" height="0px" border="0"/>
				<xsl:choose>				
				<xsl:when test="$destination ='' ">
				    <xsl:call-template name="header">
					<xsl:with-param name="showSearchBox" select="false()"/>
					<xsl:with-param name="showPrinter" select="true()"/>
					<xsl:with-param name="showTime" select="true()"/>
					<xsl:with-param name="headerName" select="$xvHeaderName"/>
					
					<xsl:with-param name="cservNumber" select="$xvCservNumber"/>
					<xsl:with-param name="indicator" select="$xvIndicator"/>
					<xsl:with-param name="brandName" select="$xvBrandName"/>
					<xsl:with-param name="dnisNumber" select="$xvDnisNumber"/>
					<xsl:with-param name="showCSRIDs" select="true()"/>
					<xsl:with-param name="showBackButton" select="true()"/>
					<xsl:with-param name="backButtonLabel" select="'(B)ack to Communication'"/>
					<xsl:with-param name="cdispEnabledFlag" select="$cdisp_enabledFlag"/>
				    </xsl:call-template>
				</xsl:when>
				<xsl:otherwise>
				    <xsl:call-template name="header">
					<xsl:with-param name="showSearchBox" select="false()"/>
					<xsl:with-param name="showPrinter" select="true()"/>
					<xsl:with-param name="showTime" select="true()"/>
					<xsl:with-param name="headerName" select="$xvHeaderName"/>
					
					<xsl:with-param name="cservNumber" select="$xvCservNumber"/>
					<xsl:with-param name="indicator" select="$xvIndicator"/>
					<xsl:with-param name="brandName" select="$xvBrandName"/>
					<xsl:with-param name="dnisNumber" select="$xvDnisNumber"/>
					<xsl:with-param name="showCSRIDs" select="true()"/>
					<xsl:with-param name="cdispEnabledFlag" select="$cdisp_enabledFlag"/>
				    </xsl:call-template>
				</xsl:otherwise>
				</xsl:choose>
				
				<script>updateCurrentUsers();</script>

				<div id="content">
					<table class="mainTable" width="98%" align="center" cellpadding="0" cellspacing="1">
						<tr>
							<td>
								<table class="PaneSection">
									<tr>
										<td class="banner">
											Message Detail
											<xsl:if test="$xvMercuryMessageType">
												&nbsp;-&nbsp;<xsl:value-of select="$xvOutboundMercuryMessageType"/>
											</xsl:if>
										</td>
									</tr>
								</table>
			
								
								<br></br>
									<div style = "FONT-SIZE: 10pt; MARGIN-LEFT: 25px; FONT-WEIGHT: bold;" id = "pc_delivery_info">Delivery Information:</div>
								<br></br>
								
								<table width="48%" cellpadding="0" cellspacing="0" style="DISPLAY: inline-block; margin-right: -16px; float: left" class = "deliveryInfo">
									<tr>
										<td class="label" id="pc_recipient_name">
											Name:
										</td>

										<td class="datum">
											<xsl:value-of select="$xvRecipientName"/>
										</td>
									</tr>
									
									 <tr>
										<td class="label" id="pc_recipient_address">
											Address:
										</td>

										<td class="datum" >
											<xsl:value-of select="$xvRecipientStreet"/>
										</td>
									</tr>
									
									<tr>
										<td class="label" id="pc_recipient_city_state_zip">
											City, State, Zip:
										</td>

										<td class="datum">
											<xsl:value-of select="$xvRecipientCityStateZip"/>
										</td>
									</tr>
									
									<tr>
										<td class="label" id="pc_recipient_phone">
											Phone:
										</td>

										<td class="datum">
										 <script type="text/javascript">
											document.write(reformatUSPhoneWithSlash('<xsl:value-of select="$xvRecipientPhone"/>'));
										</script>	
										</td>
									</tr>
									
									<tr>
										<td class="label" id="pc_delivery_date">
											Delivery Date:
										</td>

										<td class="datum">
											<xsl:value-of select="$xvOrderDeliveryDate"/>
										</td>
									</tr>
									
									<tr>
										<td class="label" id="pc_order_date">
											Order Date:
										</td>

										<td class="datum">
											<xsl:value-of select="$xvOrderDate"/>
										</td>
									</tr>
									
									 <tr>
										<td class="label" id="pc_original_price">
											Original Price:
										</td>

									<td class="datum">
															<xsl:choose>
																<xsl:when test="$xvOutboundMercuryMessageType = 'ASKP'">
																	<xsl:value-of select="$xvNewPrice"/>
																	<input type="hidden" name="message_old_price" id="message_old_price" value="{$xvNewPrice}"/>
																</xsl:when>
																
																<xsl:otherwise>
																	<xsl:value-of select="$xvOldPrice"/>
																	<input type="hidden" name="message_old_price" id="message_old_price" value="{$xvOldPrice}"/>
																</xsl:otherwise>
															</xsl:choose>
									</td>						
									</tr>
									
									<tr>
										<td class="label" id="pc_new_price">
											New Price:
										</td>

										<td class="datum">
												<xsl:choose>
																<xsl:when test="$xvOutboundMercuryMessageType = 'ASKP'">
																	<xsl:variable name="xvCostOfGoods" select="normalize-space(message/cogs)"/>
																	<xsl:choose>
																	<xsl:when test="$xvCostOfGoods='' or $xvCostOfGoods='0.00'">
																		<input type="text" name="new_price" id="new_price" size="6" maxlength="8" onKeyPress="return noEnter();"/>
																		
																         </xsl:when>
																         <xsl:otherwise>
																			<input type="text" size="6" maxlength="8" name="new_price" id="new_price" value="{$xvCostOfGoods}"/>
																         </xsl:otherwise>
																         </xsl:choose>
																         <span id="invalidPrice" class="RequiredFieldTxt" style="display: none;">
																	 </span>
																</xsl:when>
	
																<xsl:otherwise>
																	<xsl:value-of select="$xvNewPrice"/>
																	<input type="hidden" name="new_price" id="new_price" value="{$xvNewPrice}"/>
																</xsl:otherwise>
															</xsl:choose>
										</td>
									</tr>
									
									<tr>
										<td class="label">
											&nbsp;
										</td>
				
										<td class="datum">
											&nbsp;
										</td>
									</tr>
									
									
									<xsl:call-template name="mercMessReasonText"/>
									
									<tr>
										<td class="label">
											&nbsp;
										</td>
				
										<td class="datum">
											&nbsp;
										</td>
									</tr>
									
									<tr>
									<td class="label">
											&nbsp;
										</td>
										
										<td class="datum">
											<xsl:call-template name="mercMessSendButton">
												<xsl:with-param name="action_type" select="'send_price_message'"/>
											</xsl:call-template>
										</td>
									</tr>
									
								</table>
								<div width = "52%" >
								<table width="300px" cellpadding="0" cellspacing="0" style="MARGIN-TOP: -40px; MARGIN-RIGHT: -16px; FLOAT: left;" class = "MessageTable" >
								  	<tr>
								  	<td>
											<xsl:call-template name="mercMessCommentTable"/>
										</td>
									</tr>
								  	
								 </table>

							 	<table style="FLOAT: right; MARGIN-TOP: -40px; width: 225px" cellpadding="0" cellspacing="0"  class = "mercuryInfo" >
							
									
										
										<tr>
											<td class="label" id="pc_mercury_order">Mercury Order:</td>
											<td class="datum"><xsl:value-of select="$xvMercuryOrderNumber"/></td>
										</tr>
										<tr>
											<td class="label">&nbsp;</td>
											<td class="datum">&nbsp;</td>
										</tr>
										
										<tr>
										<td class="label" id="pc_filler_code">Filler Code:</td>
				
										<td class="datum">
											<xsl:value-of select="$xvFillingFloristCode"/>
										</td>
									</tr>
				
									<tr>
										<td class="label" id="pc_sender_code">Sender Code:</td>
				
										<td class="datum">
											<xsl:value-of select="$xvSendingFloristCode"/>
										</td>
									</tr>
									<tr>
										<td class="label" id="pc_operator">Operator:</td>
										<td class="datum">
											<xsl:value-of select="$xvOperatorCode"/>
										</td>
										<input type="hidden" name="message_operator" id="message_operator" value="{$xvOperatorCode}"/>
									</tr>
								  </table>
													 
								</div> 	
								  	
								  	
								<DIV id="ASKPDIV" style="display:none">
				                <TABLE width="100%" border="0" cellpadding="0" cellspacing="0" style="margin-right: -16px;">
				                  <TR>
				                    <TD colspan="2">
				                      <HR/>
				                    </TD>
				                  </TR>
				                  <TR>
				                    <TD class="label"> Approval ID: </TD>
				                    <TD class="datum">
				                      <input tabindex="6" type="text" name="manager_code" id="manager_code" size="10" maxlength="30"/>
				          &nbsp;
				                      <SPAN style="color:red"> *** </SPAN>&nbsp;
				                      <SPAN style="color:green"> User ID of manager approving asking for new price. </SPAN>&nbsp;
				                    </TD>
				                  </TR>
				                  <TR>
				                    <TD class="label"> Approval Password: </TD>
				                    <TD class="datum">
				                      <input tabindex="7" type="password" name="manager_passwd" id="manager_passwd" size="10" maxlength="10"/>
				          &nbsp;
				                      <SPAN style="color:red"> *** </SPAN>&nbsp;
				                      <SPAN style="color:green"> Password of manager approving asking for new price. </SPAN>
				                    </TD>
				                  </TR>
				                </TABLE>
				              </DIV>									
							</td>
						</tr>
					</table> <!-- End of mainTable -->
				</div>

				<div id="waitDiv" style="display: none;">
					<table id="waitTable" class="mainTable" width="98%" height="100%" align="center" cellpadding="2" cellspacing="1" >
						<tr>
							<td width="100%">
								<table width="100%" cellspacing="0" cellpadding="0" border="0">                      
									<tr>
										<td id="waitMessage" align="right" width="50%" class="WaitMessage"></td>
										<td id="waitTD"  align="left" width="50%" class="WaitMessage"></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</div>

				<xsl:call-template name="mercMessButtons"/>
				<xsl:call-template name="footer"/>
			</form>
		</body>
	</html>

</xsl:template>

</xsl:stylesheet>
