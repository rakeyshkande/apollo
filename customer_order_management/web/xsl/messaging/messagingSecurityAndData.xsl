<!DOCTYPE ACDemo[
<!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0"  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:param name="order_detail_id"><xsl:value-of select="key('security','order_detail_id')/value" /></xsl:param>
	<xsl:param name="msg_message_order_number"><xsl:value-of select="key('security','msg_message_order_number')/value" /></xsl:param>
	<xsl:param name="msg_message_type"><xsl:value-of select="key('security','msg_message_type')/value" /></xsl:param>
	<xsl:param name="msg_message_detail_type"><xsl:value-of select="key('security','msg_message_detail_type')/value" /></xsl:param>
	<xsl:param name="msg_order_type"><xsl:value-of select="key('security','msg_order_type')/value" /></xsl:param>
  <xsl:param name="msg_guid"><xsl:value-of select="key('security','msg_guid')/value" /></xsl:param>
  <xsl:param name="msg_external_order_number"><xsl:value-of select="key('security','msg_external_order_number')/value" /></xsl:param>
  <xsl:param name="msg_master_order_number"><xsl:value-of select="key('security','msg_master_order_number')/value" /></xsl:param>
	<xsl:param name="msg_email_indicator"><xsl:value-of select="key('security','msg_email_indicator')/value" /></xsl:param>
	<xsl:param name="msg_letter_indicator"><xsl:value-of select="key('security','msg_letter_indicator')/value" /></xsl:param>
	<xsl:param name="msg_order_status"><xsl:value-of select="key('security','msg_order_status')/value" /></xsl:param>
	<xsl:param name="msg_customer_id"><xsl:value-of select="key('security','msg_customer_id')/value" /></xsl:param>
	<xsl:param name="comp_order"><xsl:value-of select="key('security','comp_order')/value" /></xsl:param>
	<xsl:param name="subsequent_action"><xsl:value-of select="key('security','subsequent_action')/value" /></xsl:param>
	<xsl:param name="msg_ftd_message_status"><xsl:value-of select="key('security','msg_ftd_message_status')/value" /></xsl:param>
	<xsl:param name="msg_ftd_message_cancelled"><xsl:value-of select="key('security','msg_ftd_message_cancelled')/value" /></xsl:param>
	<xsl:param name="destination"><xsl:value-of select="key('security','destination')/value" /></xsl:param>
	<xsl:param name="queue_id"><xsl:value-of select="key('security','queue_id')/value" /></xsl:param>
	<xsl:param name="new_florist_id"><xsl:value-of select="key('security','new_florist_id')/value" /></xsl:param>
	<xsl:param name="new_msg_message_type"><xsl:value-of select="key('security','new_msg_message_type')/value" /></xsl:param>
  <xsl:param name="source_code"><xsl:value-of select="key('security','source_code')/value" /></xsl:param>
  	<xsl:param name="florist_selection_log_id"><xsl:value-of select="key('security','florist_selection_log_id')/value" /></xsl:param>
  	<xsl:param name="zip_city_flag"><xsl:value-of select="key('security','zip_city_flag')/value" /></xsl:param>
  
 	<xsl:output method="html" indent="yes"/>
 

	<xsl:template name="messagingSecurityAndData">
	 <!-- Messaging -->
		<input type="hidden" name="order_detail_id" 		id="order_detail_id" 		value="{$order_detail_id}"/>
		<input type="hidden" name="msg_message_order_number" 	id="msg_message_order_number" 	value="{$msg_message_order_number}"/>
		<input type="hidden" name="msg_message_type" 		id="msg_message_type" 		value="{$msg_message_type}"/>
		<input type="hidden" name="msg_message_detail_type" 	id="msg_message_detail_type" 	value="{$msg_message_detail_type}"/>
		<input type="hidden" name="msg_order_type" 		id="msg_order_type" 		value="{$msg_order_type}"/>
		<input type="hidden" name="msg_guid" 			id="msg_guid" 			value="{$msg_guid}"/>
		<input type="hidden" name="msg_external_order_number" 	id="msg_external_order_number" 	value="{$msg_external_order_number}"/>
		<input type="hidden" name="msg_master_order_number" 	id="msg_master_order_number" 	value="{$msg_master_order_number}"/>
		<input type="hidden" name="msg_email_indicator" 	id="msg_email_indicator" 	value="{$msg_email_indicator}"/>
		<input type="hidden" name="msg_letter_indicator" 	id="msg_letter_indicator" 	value="{$msg_letter_indicator}"/>
		<input type="hidden" name="msg_order_status" 		id="msg_order_status" 		value="{$msg_order_status}"/>
		<input type="hidden" name="msg_customer_id" 		id="msg_customer_id" 		value="{$msg_customer_id}"/>
		<input type="hidden" name="comp_order" 			id="comp_order" 		value="{$comp_order}"/>
		<input type="hidden" name="subsequent_action" 		id="subsequent_action" 		value="{$subsequent_action}"/>
		<input type="hidden" name="msg_ftd_message_status" 	id="msg_ftd_message_status" 	value="{$msg_ftd_message_status}"/>
		<input type="hidden" name="msg_ftd_message_cancelled" 	id="msg_ftd_message_cancelled" 	value="{$msg_ftd_message_cancelled}"/>
		<input type="hidden" name="destination" 		id="destination" 		value="{$destination}"/>
		<input type="hidden" name="queue_id" 			id="queue_id" 			value="{$queue_id}"/>
		<input type="hidden" name="new_florist_id" 		id="new_florist_id" 		value="{$new_florist_id}"/>
		<input type="hidden" name="new_msg_message_type" 	id="new_msg_message_type" 	value="{$new_msg_message_type}"/>
		<input type="hidden" name="source_code" 		id="source_code" 		value="{$source_code}"/>
        <!-- need to get back from order history -->
		<input type="hidden" name="external_order_number" 	id="external_order_number" 	value="{$msg_external_order_number}"/>
		<input type="hidden" name="customer_id" 		id="customer_id" 		value="{$msg_customer_id}"/>
		<input type="hidden" name="florist_selection_log_id" 		id="florist_selection_log_id" 		value="{$florist_selection_log_id}"/>
		<input type="hidden" name="zip_city_flag" 		id="zip_city_flag" 		value="{$zip_city_flag}"/>
		
		
	</xsl:template>
</xsl:stylesheet>