<!DOCTYPE stylesheet [
	<!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:import href="../header.xsl"/>
<xsl:import href="../footer.xsl"/>
<xsl:import href="mercMessButtons.xsl"/>
<xsl:import href="../securityanddata.xsl"/>
<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>
<xsl:import href="messagingSecurityAndData.xsl"/>
<xsl:import href="mercMessCommentTable.xsl"/>
<xsl:import href="mercMessReasonText.xsl"/>
<xsl:import href="formattingTemplates.xsl"/>
<xsl:import href="mercMessSendButton.xsl"/>
<xsl:key name="security" match="/message/security/data" use="name"/>

<xsl:output method="html"
			doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
			doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
			indent="yes"/>


<xsl:include href="mercMessFields.xsl"/>

<xsl:template match="/">

	<html>
		<head>
			<title><xsl:value-of select="$xvHeaderName"/></title>

			<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
			<meta http-equiv="language" content="en-us"/>

			<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
			<link rel="stylesheet" type="text/css" href="css/mercuryMessage.css"/>
			<script type="text/javascript" src="js/commonUtil.js">;</script>
			<script type="text/javascript" src="js/clock.js">;</script>
			
			<script type="text/javascript" src="js/mercuryMessage.js">;</script>
			<script type="text/javascript" src="js/FormChek.js">;</script>
			<script>
				<![CDATA[
				var actions = new Array();
				function init()
				{
					document.oncontextmenu = contextMenuHandler;
					document.onkeydown = backKeyHandler;
					var buttons = document.getElementsByTagName("BUTTON");
					
					for(var i = 0; i < buttons.length; i++)
					{
						if(buttons[i].id != "mainMenuButton" && buttons[i].id != "sendButton")
						{
							actions[buttons[i].id] = buttons[i].onclick;
							buttons[i].onclick = checkButtons;
						}else if(buttons[i].id == "mainMenuButton"){
							buttons[i].onclick=mainMenuAction;
						}
					}
					untabHeader();
				}
				
			
				

				function doSendMessagingAction()
				{
					showWaitMessage("content", "wait");
					
					document.forms[0].submit();
				       
				}
				]]>
			</script>

			<style>
				td.label {
					width: 128px;
				}
			</style>
		</head>

		<body onload="init();">
			<form id="outgoingMessageForm" method="post" action="sendAskAnsMessage.do">

				<xsl:call-template name="securityanddata"/>
				<xsl:call-template name="decisionResultData"/>
				<xsl:call-template name="messagingSecurityAndData"/>

				<input type="hidden" name="sending_florist_code" id="sending_florist_code" value="{$xvSendingFloristCode}"/>
				<input type="hidden" name="filling_florist_code" id="filling_florist_code" value="{$xvFillingFloristCode}"/>
				<input type="hidden" name="ftd_message_id" id="ftd_message_id" value="{$xvFtdMessageID}"/>
				<iframe name="unlockFrame" id="unlockFrame" height="0" width="0" border="0"></iframe>
				<iframe id="checkLock" width="0px" height="0px" border="0"/>
				<xsl:choose>				
				<xsl:when test="$destination ='' ">
				    <xsl:call-template name="header">
					<xsl:with-param name="showSearchBox" select="false()"/>
					<xsl:with-param name="showPrinter" select="true()"/>
					<xsl:with-param name="showTime" select="true()"/>
					<xsl:with-param name="headerName" select="$xvHeaderName"/>
					
					<xsl:with-param name="cservNumber" select="$xvCservNumber"/>
					<xsl:with-param name="indicator" select="$xvIndicator"/>
					<xsl:with-param name="brandName" select="$xvBrandName"/>
					<xsl:with-param name="dnisNumber" select="$xvDnisNumber"/>
					<xsl:with-param name="showCSRIDs" select="true()"/>
					<xsl:with-param name="showBackButton" select="true()"/>
					<xsl:with-param name="backButtonLabel" select="'(B)ack to Communication'"/>
					<xsl:with-param name="cdispEnabledFlag" select="$cdisp_enabledFlag"/>
				    </xsl:call-template>
				</xsl:when>
				<xsl:otherwise>
				    <xsl:call-template name="header">
					<xsl:with-param name="showSearchBox" select="false()"/>
					<xsl:with-param name="showPrinter" select="true()"/>
					<xsl:with-param name="showTime" select="true()"/>
					<xsl:with-param name="headerName" select="$xvHeaderName"/>
					
					<xsl:with-param name="cservNumber" select="$xvCservNumber"/>
					<xsl:with-param name="indicator" select="$xvIndicator"/>
					<xsl:with-param name="brandName" select="$xvBrandName"/>
					<xsl:with-param name="dnisNumber" select="$xvDnisNumber"/>
					<xsl:with-param name="showCSRIDs" select="true()"/>
					<xsl:with-param name="cdispEnabledFlag" select="$cdisp_enabledFlag"/>
				    </xsl:call-template>
				</xsl:otherwise>
				</xsl:choose>
				<script>updateCurrentUsers();</script>

					<div id="content">
					
					<table class="mainTable" width="98%" align="center" cellpadding="0" cellspacing="1">
						<tr>
							<td>
								<table class="PaneSection">
									<tr>
										<td class="banner">
											Message Detail
											<xsl:if test="$xvMercuryMessageType">
												&nbsp;-&nbsp;<xsl:value-of select="$xvMercuryMessageType"/>
											</xsl:if>
										</td>
									</tr>
								</table>
								<br></br>
									<div style = "FONT-SIZE: 10pt; MARGIN-LEFT: 25px; FONT-WEIGHT: bold;" id = "om_delivery_info">Delivery Information:</div>
								<br></br>
								
								<table width="48%" cellpadding="0" cellspacing="0" style="DISPLAY: inline-block; margin-right: -16px; float: left" class = "deliveryInfo">
									<tr>
										<td class="label" id="om_recipient_name">
											Name:
										</td>

										<td class="datum">
											<xsl:value-of select="$xvRecipientName"/>
										</td>
									</tr>
									
									 <tr>
										<td class="label" id="om_recipient_address">
											Address:
										</td>

										<td class="datum">
											<xsl:value-of select="$xvRecipientStreet"/>
										</td>
									</tr>
									
									<tr>
										<td class="label" id="om_recipient_city_state_zip">
											City, State, Zip:
										</td>

										<td class="datum">
											<xsl:value-of select="$xvRecipientCityStateZip"/>
										</td>
									</tr>
									
									<tr>
										<td class="label" id="om_recipient_phone">
											Phone:
										</td>

										<td class="datum">
										 <script type="text/javascript">
											document.write(reformatUSPhoneWithSlash('<xsl:value-of select="$xvRecipientPhone"/>'));
										</script>	
										</td>
									</tr>
									
									<tr>
										<td class="label" id="om_delivery_date">
											Delivery Date:
										</td>

										<td class="datum">
											<xsl:value-of select="$xvOrderDeliveryDate"/>
										</td>
									</tr>
									
									<tr>
										<td class="label" id="om_order_date">
											Order Date:
										</td>

										<td class="datum">
											<xsl:value-of select="$xvOrderDate"/>
										</td>
									</tr>
																		
									<tr>
										<td class="label">
											&nbsp;
										</td>
				
										<td class="datum">
											&nbsp;
										</td>
									</tr>
									
									
									<xsl:call-template name="mercMessReasonText"/>
									
									<tr>
										<td class="label">
											&nbsp;
										</td>
				
										<td class="datum">
											&nbsp;
										</td>
									</tr>
									
									<tr>
									<td class="label">
											&nbsp;
										</td>
										
										<td class="datum">
											<xsl:call-template name="mercMessSendButton">
												<xsl:with-param name="action_type" select="'send_message'"/>
											</xsl:call-template>
										</td>
									</tr>
									
								</table>
								<div width = "52%" >
								<table width="300px" cellpadding="0" cellspacing="0" style="MARGIN-TOP: -40px; MARGIN-RIGHT: -16px; FLOAT: left;" class = "MessageTable" >
								  	<tr>
								  	<td>
											<xsl:call-template name="mercMessCommentTable"/>
										</td>
									</tr>
								  	
								 </table>

							 	<table style="FLOAT: right; MARGIN-TOP: -40px; width: 225px" cellpadding="0" cellspacing="0"  class = "mercuryInfo" >
							
									
										
										<tr>
											<td class="label" id="om_mercury_order">Mercury Order:</td>
											<td class="datum"><xsl:value-of select="$xvMercuryOrderNumber"/></td>
										</tr>
										<tr>
											<td class="label">&nbsp;</td>
											<td class="datum">&nbsp;</td>
										</tr>
										
										<tr>
										<td class="label" id="om_filler_code">Filler Code:</td>
				
										<td class="datum">
											<xsl:value-of select="$xvFillingFloristCode"/>
										</td>
									</tr>
				
									<tr>
										<td class="label" id="om_sender_code">Sender Code:</td>
				
										<td class="datum">
											<xsl:value-of select="$xvSendingFloristCode"/>
										</td>
									</tr>
									<tr>
										<td class="label" id="om_operator">Operator:</td>
										<td class="datum">
											<xsl:value-of select="$xvOperatorCode"/>
										</td>
										<input type="hidden" name="message_operator" id="message_operator" value="{$xvOperatorCode}"/>
									</tr>
								  </table>
													 
								</div> 	
							</td>
						</tr>
					</table> <!-- End of mainTable -->
				</div>

				<div id="waitDiv" style="display: none;">
					<table id="waitTable" class="mainTable" width="98%" height="100%" align="center" cellpadding="2" cellspacing="1" >
						<tr>
							<td width="100%">
								<table width="100%" cellspacing="0" cellpadding="0" border="0">                      
									<tr>
										<td id="waitMessage" align="right" width="50%" class="WaitMessage"></td>
										<td id="waitTD"  align="left" width="50%" class="WaitMessage"></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</div>

				<xsl:call-template name="mercMessButtons"/>
				<xsl:call-template name="footer"/>
			</form>
		</body>
	</html>

</xsl:template>

</xsl:stylesheet><!-- Stylus Studio meta-information - (c) 2004-2005. Progress Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperMetaTag><MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/><MapperBlockPosition></MapperBlockPosition></MapperMetaTag>
</metaInformation>
-->