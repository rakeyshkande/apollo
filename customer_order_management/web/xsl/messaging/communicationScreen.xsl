<!DOCTYPE stylesheet [
	<!ENTITY  nbsp "&#160;">

]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:import href="../header.xsl"/>
<xsl:import href="../footer.xsl"/>
<xsl:import href="mercMessButtons.xsl"/>
<xsl:import href="../securityanddata.xsl"/>
<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>
<xsl:import href="communicationData.xsl"/>
<xsl:import href="mercMessFormatDate.xsl"/>
<xsl:import href="messagingSecurityAndData.xsl"/>

<xsl:key name="security" match="/root/security/data" use="name"/>

<xsl:output method="html"
			doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
			doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
			indent="yes"/>




<!-- Variables for the messages and emails tables -->
<xsl:param name="read_only_flag"><xsl:value-of select="key('security','read_only_flag')/value" /></xsl:param>
<xsl:param name="error_on_send_str"><xsl:value-of select="key('security','ERROR_ON_SEND')/value" /></xsl:param>
<xsl:param name="msg_status_url"><xsl:value-of select="key('security','msg_status_url')/value"/></xsl:param>
<xsl:variable name="xvMessages" select="root/messages"/>
<xsl:variable name="xvEmailLetters" select="root/email-letters"/>
<xsl:variable name="xvCurrentPage" select="normalize-space(root/CurrentPage)"/>
<xsl:variable name="xvTotalPages" select="normalize-space(root/NumberOfPages)"/>
<xsl:variable name="order_tagged_indicator" select="normalize-space(root/out-parameters/v_tagged)"/>
<xsl:variable name="xvHeaderName" select="normalize-space(message/page_title)"/>
<xsl:variable name="xvCservNumber" select="normalize-space($call_cs_number)"/>
<xsl:variable name="xvIndicator" select="normalize-space($call_type_flag)"/>
<xsl:variable name="xvDnisNumber" select="normalize-space($call_dnis)"/>
<xsl:variable name="xvBrandName">
<xsl:variable name="temp" select="normalize-space($call_brand_name)"/>
<xsl:value-of select="substring($temp, 1, 50)"/>
</xsl:variable>
<xsl:variable name="xvLoadMemberDataUrl" select="normalize-space(//load_member_data_url)"/>

<!-- Variables for queue information -->
<xsl:variable name="xvOrderNumber" select="normalize-space($msg_external_order_number)"/>


<xsl:template match="/">

	<html>
		<head>
			<title>Communication</title>

			<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
			<meta http-equiv="language" content="en-us"/>

			<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
			<link rel="stylesheet" type="text/css" href="css/mercuryMessage.css"/>
			<link rel="stylesheet" type="text/css" href="css/communication.css"/>
			<link rel="stylesheet" type="text/css" href="css/tooltip.css"/>

			<script type="text/javascript" src="js/tabIndexMaintenance.js">;</script>
			<script type="text/javascript" src="js/clock.js">;</script>
			<script type="text/javascript" src="js/commonUtil.js">;</script>
			<script type="text/javascript" src="js/util.js">;</script>
			<script type="text/javascript" src="js/FormChek.js">;</script>
			<script>
				function init()
				{
                                        setCommunicationScreenIndexes();
					document.oncontextmenu = contextMenuHandler;

					<xsl:if test="$xvTotalPages &gt; 1">
						document.attachEvent("onkeydown", arrowKeyHandler);
					</xsl:if>

                    <xsl:if test="string-length($error_on_send_str) &gt; 1">
                    alert('<xsl:value-of select="$error_on_send_str"/>');
                    </xsl:if>

					document.attachEvent("onkeydown", backKeyHandler);
					document.attachEvent("onkeydown", evaluateKeyPress);
					document.getElementById("numberEntry").focus();
				}

				<xsl:if test="$xvTotalPages &gt; 1">
					function arrowKeyHandler()
					{
						var keypress = window.event.keyCode;

						<xsl:if test="$xvCurrentPage &gt; 1">
							//go to 1st page(left arrow)
							if(window.event.altKey &amp;&amp; keypress == 37)
								newPage(1);

							//go to previous page(up arrow)
							if(window.event.altKey &amp;&amp; keypress == 38)
								newPage(<xsl:value-of select="$xvCurrentPage - 1"/>);
						</xsl:if>

						<xsl:if test="number($xvCurrentPage) != number($xvTotalPages)">
							//go to the last page(right arrow)
							if(window.event.altKey &amp;&amp; keypress == 39)
								newPage(<xsl:value-of select="$xvTotalPages"/>);

							//go to the next page(down arrow)
							if(window.event.altKey &amp;&amp; keypress == 40)
								newPage(<xsl:value-of select="$xvCurrentPage + 1"/>);
						</xsl:if>
					}
				</xsl:if>
<![CDATA[				

				var isRecordLocked = false;

/*******************************************************************************
*  setCommunicationScreenIndexes()
*  Sets tabs for Communication Screen
*  See tabIndexMaintenance.js
*******************************************************************************/
function setCommunicationScreenIndexes()
{
  untabAllElements();
	var begIndex = findBeginIndex();
  // set elements tabs//
 	var tabArray1 = new Array('numberEntry', 'floristDashboardButton', 'qDeleteButton',
  			'recipientOrderButton', 'historyButton', 'orderCommentsButton', 
        'emailButton', 'lettersButton', 'refundButton', 'floristInquiryButton','tagOrder');
  begIndex = setTabIndexes(tabArray1, begIndex);
  // set tabs for florist lists //
  var tabArray2 = document.getElementsByName('floristLink');
	for(var x = 0; x < tabArray2.length; x++)
  {
	  var element = tabArray2[x];
    if (element != null)
    {
      element.tabIndex = begIndex;
      begIndex++;
    }
	}
  begIndex = setTabIndexes(tabArray2, begIndex);
  // set more elements tabs//
  var tabArray3 = new Array('backButton', 'mainMenu', 'printer');
  begIndex = setTabIndexes(tabArray3, begIndex);
  return begIndex;
}

				function contextMenuHandler()
				{
				   return false;
				}
				
				function doBackAction() {
					openRecipientOrder();
				}

				function backKeyHandler()
				{
				   // backspace
				   if (window.event && window.event.keyCode == 8){
				      var formElement = false;
				
				      for(i = 0; i < document.forms[0].elements.length; i++){
				         if(document.forms[0].elements[i].name == document.activeElement.name){
				            formElement = true;
				            break;
				         }
				      }
				      var searchBox = document.getElementById('numberEntry');
				      if(searchBox != null){
				        if(document.getElementById('numberEntry').id == document.activeElement.id){
				          formElement = true;
				        }
				      }
				
				      if(formElement == false || (document.activeElement.type != "text" && document.activeElement.type != "textarea" && document.activeElement.type != "password")){
				         window.event.cancelBubble = true;
				         window.event.returnValue = false;
				         return false;
				      }
				   }
				
				   // F5 and F11
				   if (window.event.keyCode == 122 || window.event.keyCode == 116){
				      window.event.keyCode = 0;
				      event.returnValue = false;
				      return false;
				   }
				
				   // Alt + Left Arrow
				   if(window.event.altLeft){
				      window.event.returnValue = false;
				      return false;
				   }

				   // Alt + Right Arrow
				   if(window.event.altKey){
					      window.event.returnValue = false;
					      return false;
					   }
					}

					/*
						Catch all numeric keypresses and send them to the search box.
					*/
					function evaluateKeyPress()
					{
						var a;
						var keypress = window.event.keyCode;
					
						if (keypress == 48||keypress==96)
							a = 0;
						if (keypress == 49||keypress==97)
							a = 1;
						if (keypress == 50||keypress==98)
							a = 2;
						if (keypress == 51||keypress==99)
							a = 3;
						if (keypress == 52||keypress==100)
							a = 4;
						if (keypress == 53||keypress==101)
							a = 5;
						if (keypress == 54||keypress==102)
							a = 6;
						if (keypress == 55||keypress==103)
							a = 7;
						if (keypress == 56||keypress==104)
							a = 8;
						if (keypress == 57||keypress==105)
							a = 9;
					
						if (a >= 0 && a <= 9)
						{
							document.getElementById("numberEntry").focus();
						}
					}
					
					/*
					   This is a helper function to return the two security parameters
					   required by the security framework.
					
					   areOnlyParams -   is used to change the url string (? vs. &) depending if these
										 are the only parameters in the url
					*/
					function getSecurityParams(areOnlyParams)
					{
						return ((areOnlyParams) ? "?" : "&")
								+ "securitytoken=" + document.getElementById("securitytoken").value
								+ "&context=" + document.getElementById("context").value;
					}
					
					
					/*
					This function is executed when the csr
					uses the search box. If the csr hits the 'enter' key
					the associated td with the same id will be located and the customer
					id is grabbed from the hidden field and a search is performed.
					*/
					function doEntryAction()
					{
						if(window.event.keyCode != 13)
						{
							return false;
						}
					
						if(isWhitespace(event.srcElement.value))
						{
							window.event.returnValue = false;
							return false;
						}
					
					
						var messageNumber = stripZeros(stripWhitespace(event.srcElement.value)).toUpperCase();
						var messageNameRegex = /^\d+E*$/;
						if(messageNameRegex.test(messageNumber))
						{
							openMessage(messageNumber);
							window.event.returnValue = false;
							return false;
						}
						else
						{
							alert(messageNumber + " isn't a valid message number.");
							window.event.returnValue = false;
							return false;
						}
					}

					/*
					Using the specified URL submit the form
					*/
					function performAction(url)
					{
					//	showWaitMessage("content", "wait", "Searching");
						document.forms[0].action = url;
						document.forms[0].submit();
					}
					
					
					/*
					Handlers for the mercMessButtons.xsl footer buttons.
					*/
					function openRecipientOrder()
					{
						var url = "customerOrderSearch.do?action=customer_account_search"
									+ "&order_number=" + document.getElementById("msg_external_order_number").value
									+ "&order_guid=" + document.getElementById("msg_guid").value
									+ "&customer_id=" + document.getElementById("msg_customer_id").value;
					
						performAction(url);
					}
					
					function openHistory()
					{
						var url = "loadHistory.do?history_type=Order&page_position=1"
									+ "&order_detail_id=" + document.getElementById("order_detail_id").value;
					
						performAction(url);
					}
					
					function doTagOrderAction(tag,detailId){
					  if(tag == '2Y' || tag == '2N'){
					    alert('You cannot tag this order, there are two users already tagged');
					    return;
					  }
					  if(tag == '1Y'){
					   alert('You already have this order tagged');
					   return;
					  }
					 var url = "tagOrder.do?order_detail_id=" + detailId   + getSecurityParams(false);
					  var sFeatures="dialogHeight:500px;dialogWidth:800px;";
					    sFeatures +=  "status:1;scroll:0;resizable:0";
					
					  var shouldReload=showModalDialogIFrame(url,"" , sFeatures);
					  if(shouldReload=='Y'){
					  	reloadComm();   
					  
					  }
					  
					
					// performAction(url);

					}
                                        
                                        function openOrderComments()
					{
						var url = "loadComments.do"
									+ "?page_position=1"
									+ "&comment_type=Order"
									+ "&order_detail_id=" + document.getElementById("order_detail_id").value
									+ "&order_guid="
									+ document.getElementById("msg_guid").value
									+ "&customer_id="
									+ document.getElementById("msg_customer_id").value;
					
						performAction(url);
					}
					
					function openEmail()
					{
						if(document.getElementById("msg_email_indicator").value == "true")
						{
							var url = "loadSendMessage.do?message_type=Email&action_type=load_titles&external_order_number="
										+ document.getElementById("msg_external_order_number").value;
						
							performAction(url);
						}
						else
						{
							alert("There is no email address on file for this customer.");
						}
					}

					function openLetters()
					{
						var url = "loadSendMessage.do?message_type=Letter&action_type=load_titles&external_order_number="
									+ document.getElementById("msg_external_order_number").value;
					
						performAction(url);
					}
					
					function openRefund()
					{
						var url = "loadRefunds.do?order_detail_id="
									+ document.getElementById("order_detail_id").value;
					
						performAction(url);
					}
					
					function openFloristInquiry()
					{
						  var sURL = "incorrectOrderDisposition.html";
						  var objFlorist = new Object();
						
						  var   url = document.getElementById("load_member_data_url").value;
						        url += "action=loadFloristSearch" + getSecurityParams(false);
						
						  objFlorist.scrubURL = url;
						  objFlorist.orderStatus = '';
						  objFlorist.callingPage = 'customerShoppingCart';
						  //Features
						  var sFeatures="dialogHeight:500px;dialogWidth:950px;";
						    sFeatures +=  "status:1;scroll:1;resizable:0";
						
						  window.showModalDialog(sURL, objFlorist, sFeatures);
					}
					
					/*
					Handler for the communicationScreen.xsl and floristLookup.xsl functionality of clicking on a
					florist's number in the table of Mercury messages, being taken to that florist's information
					page.
					*/
					function openFlorist(floristCode)
					{
						var sURL = "incorrectOrderDisposition.html";
						  var objFlorist = new Object();
						
						  /* build url used to access load member data */
						   var  url = document.forms[0].load_member_data_url.value;
						         url += "action=searchFlorist" + "&sc_floristnum=" + floristCode + getSecurityParams(false);
						
						  objFlorist.scrubURL = url;
						  objFlorist.orderStatus = '';
						  objFlorist.callingPage = 'customerShoppingCart';
						
						 //Features
						           var sFeatures="dialogHeight:500px;dialogWidth:950px;";
						          sFeatures +=  "status:0;scroll:1;resizable:0";
						
						  window.showModalDialog(sURL, objFlorist, sFeatures);
					}
										
					/*
					Handler for the communicationScreen.xsl "(Q)Delete" button in the Mercury
					message table. This button creates a popup window listing the messages in the queue, as
					created by the qDelete.xsl.
					*/
					function openQDelete()
					{
						
						var args = new Array();
					
						args.action = "displayQDelete.do";
						
					  /*
						The action needs a list of all the messages on the communication screen. The list will
						consist of all the messages' mercury ids. All the message hidden input
						fields are named "n" where n is that message's line number. Their values are the
						concatenation of their mercury ids and their message types(mercury, venus, email or
						letter), separated by a "|" character. We use an appropriate regular expression to select
						the message hidden fields, split off just the message id portions of those fields'	values,
						and use them to make a list of ids we pass to the displayQDelete action.
					  */
						var formFields = document.getElementById("communicationScreenForm").elements;
						var messageNameRegex = /^\d+E*$/;
						
						var messageId;
					
						for(var i = 0; i < formFields.length; i++)
						{
							//alert("name="+formFields[i].name+" value="+formFields[i].value);
							if(messageNameRegex.test(formFields[i].name))
							{
								//alert("name="+formFields[i].name);
								messageId = formFields[i].value.split("|");
								
								args[args.length] = new Array(formFields[i].name, messageId[0]);
							}
						}
					
						args[args.length] = new Array("order_detail_id", document.getElementById("order_detail_id").value);
						args[args.length] = new Array("securitytoken", document.getElementById("securitytoken").value);
						args[args.length] = new Array("context", document.getElementById("context").value);
					
						var modal_dim = "dialogWidth:600px; dialogHeight:350px; dialogLeft:125; dialogTop: 190; center:yes; status=0; help:no; scroll:yes;";
						var shouldReload = window.showModalDialog("html/waitModalDialog.html", args, modal_dim);
					
						if(shouldReload == "Y")
						{
							reloadComm();
							
						}
					}
					
					
					function reloadComm(){
					
							url = "displayCommunication.do?";
					
							//We need to refresh the communications window to reflect any deletions that were just made
							//in the qdelete popup screen, and so we must append the relevant hidden field values to the
							//url so they'll be preserved.
							var inputElements = document.getElementsByTagName("input");
							var type;
							
							//We'll need a regular expression matching purely numeric strings to weed out the message
							//fields.
							var numberRegex = /^\d+E*$/;
							var isNumber;
							for(var i = 0; i < inputElements.length; i++)
							{
								type = inputElements[i].getAttribute("type");
								//We don't bother sending along hidden field values which are null, and we don't want to
								//send the message hidden fields. Checking for null values is simple, and all the
								//message hidden fields have purely numeric names, unlike all the other hidden fields,
								//so we can use our simple numeric regular expression to identify them.
								if((type == "hidden") && (inputElements[i].value) && (inputElements[i].value != ""))
								{
									isNumber = numberRegex.test(inputElements[i].name)
									if(!isNumber)
									{
										url += "&" + inputElements[i].name + "=" + inputElements[i].value;
									}
								}
							}
							
							window.location = url;
					
					
					}
					
					

					/*
					Handler for the communicationScreen.xsl "Florist (D)ashboard" button in the Mercury
					message table. This button creates a popup window providing florist data relating to
					the order, as created by the floristDashboard.xsl.
					*/
					function openDashboard()
					{
						var args = new Array();
					
						args.action = "displayFloristDashboard.do";
					
						args[0] = new Array("action_type", "florist_dashboard");
						args[1] = new Array("order_detail_id", document.getElementById("order_detail_id").value);
						args[2] = new Array("securitytoken", document.getElementById("securitytoken").value);
						args[3] = new Array("context", document.getElementById("context").value);
					
						var modal_dim = "dialogWidth:570px; dialogHeight:245px; dialogLeft:125; dialogTop: 190; center:yes; status=0; help:no; scroll:no";
					
						window.showModalDialog("html/waitModalDialog.html", args, modal_dim);
					}
					
					
					/*
					Handler for the message type links in communicationScreen.xsl. This function accepts an
					integer parameter representing the message number whose details it will attempt to bring up.
					*/
					function openMessage(recordNumber)
					{
						
						var hiddenMessageField = document.getElementById(recordNumber.toString());
						if(hiddenMessageField == null)
						{
							alert("There is no message " + recordNumber + " on this page.");
							window.event.returnValue = false;
							return false;
						}
						else
						{
							document.forms[0].message_info.value = hiddenMessageField.value;
							document.forms[0].message_line_number.value = recordNumber;
							document.forms[0].ext_order_number.value = document.getElementById("msg_external_order_number").value;
							document.forms[0].ord_guid.value = document.getElementById('msg_guid').value;
							document.forms[0].cus_id.value = document.getElementById("msg_customer_id").value;
							document.forms[0].submit();
						}
					}
					
					
					
									
					
                                        function resetFormActionAndSubmit(forwardAction, subsequentAction, orderType, orderStatus, cancelled, cdispCancelled)
                                        {
                                                //set parent window's forward action field.
                                                if(document.forms[0] && forwardAction != "")
                                                {
                                                        document.forms[0].action = forwardAction;
                                                }
                                        
                                                //set parent window's subsequent action field, if it has one.
                                                if(document.forms[0].subsequent_action && subsequentAction != "")
                                                {
                                                        document.forms[0].subsequent_action.value = subsequentAction;
                                                }
                                        
                                                //set parent window's msg_order_type field, if it has one.
                                                if(document.forms[0].msg_order_type && orderType != "")
                                                {
                                                        document.forms[0].msg_order_type.value = orderType;
                                                }
                                        
                                                //set parent window's msg_order_status field, if it has one.
                                                if(document.forms[0].msg_order_status && orderStatus != "")
                                                {
                                                        document.forms[0].msg_order_status.value = orderStatus;
                                                }
                                        
                                                //set parent window's msg_ftd_message_cancelled field, if it has one.
                                                if(document.forms[0].msg_ftd_message_cancelled && cancelled != "")
                                                {
                                                        document.forms[0].msg_ftd_message_cancelled.value = cancelled;
                                                }
                                        
                                                if(document.forms[0].cdisp_cancelledOrder && cdispCancelled != "")
                                                {
                                                        document.forms[0].cdisp_cancelledOrder.value = cdispCancelled;
                                                }
                                        
                                                document.forms[0].submit();
                                        }

					/*
					Handler for the "(Q)Delete" button in the qDelete.xsl.
					*/
					function doQDeleteAction()
					{
						//alert("in doQDeleteAction");
						var toBeDeleted = "";
						if(qDeleteForm.messageCheckboxes != null){
							var len= qDeleteForm.messageCheckboxes.length
							if(len==null&&qDeleteForm.messageCheckboxes.checked){
								toBeDeleted=qDeleteForm.messageCheckboxes.value;
							}else if(len > 0)
							{
								//alert("in doQDeleteAction checkboxes");
								
								for(var i = 0; i < qDeleteForm.messageCheckboxes.length; i++)
								{
									if(qDeleteForm.messageCheckboxes[i].checked)
									{
										toBeDeleted += qDeleteForm.messageCheckboxes[i].value + ',';
									}
								}
								//remove the last ","
								toBeDeleted = toBeDeleted.slice(0, -1);
								
							}
						}
						//alert("toBeDeleted="+toBeDeleted);
						if(toBeDeleted != "")
						{
							
					
							var url = "deleteQueue.do?action_type=delete"
										+ "&order_detail_id=" + document.getElementById("order_detail_id").value
										+ "&queue_ids=" + toBeDeleted;
					
							
							performAction(url);
						}
						else
						{
							alert("You must select at least one message to delete.");
						}
					}
					
					/*
					Handler for the "(C)lose" buttons in the qDelete screen. The qDelete modal popup window
					must be closed, and the parent window, the Communication screen, must be updated, since
					it's possible that deletions made on the qDelete screen have caused some of the messages
					on the Communications screen to cease to exist.
					*/
					function closeQDelete()
					{
						top.returnValue = shouldReload;
					
						top.close();
					}
					
					/*
					Handler for the "QDelete(A)ll" button in the qDelete.xsl. This action checks all the boxes
					in the table, and then invokes the doQDeleteAction() function to have them all deleted.
					*/
					function checkAll()
					{
						if(qDeleteForm.messageCheckboxes != null)
						{
							//alert("qDeleteForm.messageCheckboxes = "+qDeleteForm.messageCheckboxes.length);
							var msgLength=qDeleteForm.messageCheckboxes.length;
							//Check all the messages in the table
							if(msgLength!=null){//multiple checkboxes
								for(var i = 0; i < qDeleteForm.messageCheckboxes.length; i++)
								{
									if(!qDeleteForm.messageCheckboxes[i].checked)
									{
										qDeleteForm.messageCheckboxes[i].checked = true;
									}
								}
							}else{ //only one checkbox.
								qDeleteForm.messageCheckboxes.checked = true;
							}
					
							doQDeleteAction();
						}
					}
					
					
										
					
					function writeError(errorFieldName, errorMessage)
					{
						var span = document.getElementById(errorFieldName);
						var newMessage = document.createTextNode("  " + errorMessage);
					
						if(span.childNodes)
						{
							var kids = span.childNodes;
							for(var i = 0; i < kids.length; i++)
							{
								span.removeChild(kids[i]);
							}
						}
						span.appendChild(newMessage);
						span.style.display = "inline";
					}
					
					
					
					
					
					
					
					
					function newPage(pageNumber)
					{
						var url = "displayCommunication.do?order_detail_id="
												+ document.forms[0].order_detail_id.value
												+ "&page_number="
												+ pageNumber
												+"&read_only_flag="+document.forms[0].read_only_flag.value;
					
						performAction(url);
					}
					
					
					
					/*****
					Low-level validation and formatting functions.
					*****/
					function isEmpty(s)
					{
						return ((s == null) || (s.length == 0));
					}
					
					function isDigit(c)
					{
						return ((c >= "0") && (c <= "9"));
					}
					
					
					var defaultEmptyOK = false;
					
					function isInteger(s)
					{
						var i;
					
						if(isEmpty(s))
						{
							if(isInteger.arguments.length == 1)
							{
								return defaultEmptyOK;
							}
							else
							{
								return (isInteger.arguments[1] == true);
							}
						}
					
						// Search through string's characters one by one
						// until we find a non-numeric character.
						// When we do, return false; if we don't, return true.
					
						for(i = 0; i < s.length; i++)
						{
							// Check that current character is number.
							var c = s.charAt(i);
					
							if(!isDigit(c))
							{
								return false;
							}
						}
					
						// All characters are numbers.
						return true;
					}
					
					function stripCharsInBag(s, bag)
					{
						var i;
						var returnString = "";
					
						// Search through string's characters one by one.
						// If character is not in bag, append to returnString.
						for(i = 0; i < s.length; i++)
						{
							// Check that current character isn't whitespace.
							var c = s.charAt(i);
							if (bag.indexOf(c) == -1)
							{
								returnString += c;
							}
						}
					
						return returnString;
					}
					
					/*
					Remove any leading zeros from the search number
					*/
					function stripZeros(num)
					{
						var num, newTerm;
					
						while(num.charAt(0) == "0")
						{
							newTerm = num.substring(1, num.length);
							num = newTerm;
						}
					
						if(num == "")
							num = "0";
					
						return num;
					}
					
					// whitespace characters
					var whitespace = " \t\n\r";
					
					function stripWhitespace(s)
					{
						return stripCharsInBag(s, whitespace);
					}
					
					function isWhitespace(s)
					{
					    // Is s empty?
					    if((s == null) || (s.length == 0))
					    	return true;
					
					    // Search through string's characters one by one
					    // until we find a non-whitespace character.
					    // When we do, return false; if we don't, return true.
					    var whitespace = " \t\n\r";
					
					    for (var i = 0; i < s.length; i++)
					    {
					        // Check that current character isn't whitespace.
					        var c = s.charAt(i);
					
					        if (whitespace.indexOf(c) == -1) return false;
					    }
					
					    // All characters are whitespace.
					    return true;
					}
					
					
					
					function showModalAlert(message)
					{
						//Modal_URL
						modalUrl = "alert.html";
					
						//Modal_Arguments
						var modalArguments = new Object();
						modalArguments.modalText = message;
					
						//Modal_Features
						var modalFeatures = 'dialogWidth:200px; dialogHeight:150px; center:yes; status=no; help=no;';
						modalFeatures += 'resizable:no; scroll:no';
					
						window.showModalDialog(modalUrl, modalArguments, modalFeatures);
					}
					
					
					function updateCurrentUsers()
					{
						var iframe = document.getElementById('CSRFrame');
					
						var url = "customerOrderSearch.do?action=refresh_ids"
									+ "&entity_type=ORDER_DETAILS"
									+ "&entity_id="
									+ document.getElementById('order_detail_id').value
									+ "&order_guid="
									+ document.getElementById('msg_guid').value
									+ getSecurityParams(false);
					
						iframe.src = url;
					}
					
					function noEnter()
					{
						if(window.event && window.event.keyCode == 13)
						{
							window.event.returnValue = false;
							return false;
						}
						else
						{
							return true;
						}
					}
					
					
					/*
					  The following methods are used to display a wait message to the user
					  while an action is being executed.  The following coding convention should
					  be used for the wait div's table structure:
					
					  waitDiv - id of the wait div
					  waitMessage - id of the TD which will contain the message
					  waitTD - id of the TD which will contain the dots (...)
					
					  Parameters
					  content - The div id containing the content to be hidden
					  wait - The div id containing the wait message
					  message - Optional, if provided the message to be displayed, otherwise, "Processing"
					            will be the message
					*/
					var _waitTD = "";
					var clearMessageTimerID = "";
					var showPeriodTimerID = "";
					
					function showWaitMessage(content, wait, message)
					{
						var content = document.getElementById(content);
						var height = content.offsetHeight;
						var waitDiv = document.getElementById(wait + "Div");
						var waitMessage = document.getElementById(wait + "Message");
						_waitTD = document.getElementById(wait + "TD");
					
						content.style.display = "none";
						waitDiv.style.display = "block";
						waitDiv.style.height = height;
						waitMessage.innerHTML = (message) ? message : "Processing";
					
						clearMessageTimerID = window.setInterval("clearWaitMessage()", 5000);
						showPeriodTimerID = window.setInterval("showPeriod()", 1000);
					}
					
					function hideWaitMessage(content, wait)
					{
						var content = document.getElementById(content);
						var waitDiv = document.getElementById(wait + "Div");
						
						content.style.display = "block";
						waitDiv.style.display = "none";
						
						window.clearInterval(clearMessageTimerID);
						window.clearInterval(showPeriodTimerID);
					}

					function clearWaitMessage()
					{
						_waitTD.innerHTML = "";
					}
					
					function showPeriod()
					{
						_waitTD.innerHTML += "&nbsp;.";
					}
					
					function noFlorists(error)
					{
						writeError("invalidFloristCode", error);
					}
					
					function submitNewFtd(forwardAction, floristCode, floristSelectionLogId, zipCityFlag)
					{
						document.forms[0].filling_florist_code.value = floristCode;
						document.forms[0].florist_selection_log_id.value = floristSelectionLogId;
						document.forms[0].zip_city_flag.value = zipCityFlag;
						document.forms[0].action = forwardAction;
						document.forms[0].submit();
					}
					
					
					
					
					
					function isExitSafe(pageText)
					{
						elementClicked = event.srcElement;
					
						var modalUrl = "confirm.html";
					
						//Modal_Arguments
						var modalArguments = new Object();
						modalArguments.modalText = pageText;
					
						//Modal_Features
						var modalFeatures = 'dialogWidth:200px; dialogHeight:150px; center:yes; status=no;';
						modalFeatures += 'help=no; resizable:no; scroll:no';
					
						//get a true/false value from the modal dialog.
						return window.showModalDialog(modalUrl, modalArguments, modalFeatures);
					
						
					}
					
					function checkButtons()
					{
						
					
						if(isExitSafe("Are you sure you want to leave without sending the message?")==true)
						{
							document.getElementById("unlockFrame").src="lock.do?action_type=unlock&lock_app=COMMUNICATION&lock_id="+document.getElementById("order_detail_id").value+getSecurityParams(false);
					           
							actions[elementClicked.id]();
						}
					}
					
					
					function mainMenuAction()
					{
					  if(isExitSafe("Are you sure you want to leave without sending the message?")==true)
					  {
					  	
					           document.getElementById("unlockFrame").src="lock.do?action_type=unlock&lock_app=COMMUNICATION&lock_id="+document.getElementById("order_detail_id").value+getSecurityParams(false);
					           doMainMenuAction();
						
					  }
					}

                                        function openLifecycle(mercuryId) {
                                            var args = new Array();
                                            args.action = "loadLifecycle.do";

                                            args[args.length] = new Array("mercury_id", mercuryId);
                                            args[args.length] = new Array("securitytoken", document.getElementById("securitytoken").value);
                                            args[args.length] = new Array("context", document.getElementById("context").value);

                                            var modal_dim = "dialogWidth:600px; dialogHeight:350px; dialogLeft:125; dialogTop: 190; center:yes; status=0; help:no; scroll:yes;";
                                            var shouldReload = window.showModalDialog("html/waitModalDialog.html", args, modal_dim);
                                        }

                    function showLifecyclePopup() {
                        //Modal_Arguments
                        var modalArguments = new Object();

                        //Modal_Features
                        var modalFeatures = "dialogWidth:600px; dialogHeight:400px; resizable:yes; scroll:yes";

                        var myLink = document.getElementById("msg_status_url").value;
                        window.showModalDialog(myLink, modalArguments, modalFeatures);
                    }
                    
                    function openOriginalView(){
					 var url = 'originalView.do?'
					    + "&master_order_number=" + document.getElementById("msg_master_order_number").value
					    + "&display_order_number=" + document.getElementById("msg_external_order_number").value
					    + "&action=original_view_load";
					 performAction(url);
                    }
                    
                     function doRefresh(){
					 var url = 'displayCommunication.do?'
					    + "&order_detail_id=" + document.getElementById("order_detail_id").value
					    + "&master_order_number=" + document.getElementById("msg_master_order_number").value
					    + "&bypass_com=" + document.getElementById("bypass_com").value;
					 performAction(url);
                    }

]]>
			</script>
		</head>

		<body onload="init();">
			<form id="communicationScreenForm" name="communicationScreenForm" action="viewMessageSwitch.do" method="post">

				<input type="hidden" name="message_info" id="message_info"/>
				<input type="hidden" name="message_line_number" id="message_line_number"/>
				<input type="hidden" name="read_only_flag" id="read_only_flag" value="{$read_only_flag}"/>
				<input type="hidden" name="msg_status_url" value="{$msg_status_url}"/>
				<input type="hidden" name="ext_order_number" id="ext_order_number"/>
				<input type="hidden" name="ord_guid" id="ord_guid"/>
				<input type="hidden" name="cus_id" id="cus_id"/>
				
				<xsl:call-template name="securityanddata"/>
				<xsl:call-template name="decisionResultData"/>
				<xsl:call-template name="communicationData"/>

				<xsl:for-each select="$xvMessages/message">
					<xsl:variable name="line" select="normalize-space(row_number)"/>
					<xsl:variable name="id" select="normalize-space(message_id)"/>
					<xsl:variable name="msgType" select="normalize-space(group_indicator)"/>
							
					<input type="hidden" name="{$line}" id="{$line}" value="{concat($id, '|', $msgType)}"/>
				</xsl:for-each>


				<xsl:for-each select="$xvEmailLetters/email-letter">
					<xsl:variable name="line" select="concat(normalize-space(row_number), 'E')"/>
					<xsl:variable name="id" select="normalize-space(message_id)"/>
					<xsl:variable name="msgType" select="normalize-space(msg_type)"/>
							
					<input type="hidden" name="{$line}" id="{$line}" value="{concat($id, '|', $msgType)}"/>
				</xsl:for-each>

				<xsl:call-template name="header">
					<xsl:with-param name="showSearchBox" select="true()"/>
					<xsl:with-param name="showPrinter" select="true()"/>
					<xsl:with-param name="showTime" select="true()"/>
					<xsl:with-param name="headerName" select="'Communication'"/>
					<xsl:with-param name="showBackButton" select="true()"/>
					<xsl:with-param name="cservNumber" select="$xvCservNumber"/>
					<xsl:with-param name="indicator" select="$xvIndicator"/>
					<xsl:with-param name="brandName" select="$xvBrandName"/>
					<xsl:with-param name="dnisNumber" select="$xvDnisNumber"/>
					<xsl:with-param name="showCSRIDs" select="true()"/>
					<xsl:with-param name="backButtonLabel" select="'(B)ack to Order'"/>
					<xsl:with-param name="refreshButtonLabel" select="'Refresh'"/>
					<xsl:with-param name="cdispEnabledFlag" select="$cdisp_enabledFlag"/>
					<xsl:with-param name="showRefreshButton" select="true()"/>
				</xsl:call-template>
                                
				<script>updateCurrentUsers();</script>

				<div id="content">
					
					<table class="mainTable" width="98%" align="center" cellpadding="0" cellspacing="1">
						
						<tr>
							<td>
								<!--Order information-->
								<table class="PaneSection">
									<tr style="font-weight: bold;">
										<td id="orderNumber">
											Order Number:&nbsp;
											<a href="javascript: openRecipientOrder();" id="order_number" tabindex="-1">
												<xsl:value-of select="$xvOrderNumber"/>
											</a>
											<xsl:if test="key('security','msg_lifecycle_delivered_status')/value = 'Y'">
												&nbsp;&nbsp;
												<xsl:choose>
													<xsl:when test="key('security','msg_status_url')/value != ''">
														<xsl:choose>
															<xsl:when test="key('security','msg_status_url_active')/value = 'Y'">
																<a href="javascript:showLifecyclePopup();">
																	<img src="images/green-check-mark.png" style="width:14px;height:14px;border:0;"/>
																</a>
															</xsl:when>
															<xsl:otherwise>
																<img src="images/green-check-mark.png" style="width:14px;height:14px;">
																	<xsl:attribute name="alt"><xsl:value-of select="key('security','msg_status_url_expired_msg')/value"/></xsl:attribute>
																</img>
															</xsl:otherwise>
														</xsl:choose>
													</xsl:when>
													<xsl:otherwise>
														<img src="images/green-check-mark.png" style="width:14px;height:14px;"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:if>
										</td>

										<xsl:for-each select="root/queues/queue">
	
										<td id="creditQueue">
											<xsl:value-of select="queue_item"/>
										</td>
										</xsl:for-each>
										
									</tr>
								</table>
	
								<!--Messages banner-->
								<table class="PaneSection">
									<tr>
										<td align="center" class="banner">
											Messages
										</td>
									</tr>
								</table>
	
								<!-- Mercury messages -->
								<div class="messageContainer" style="padding-right: 16px;">
									<table width="100%" cellpadding="0" cellspacing="0">
										<tr>
											<th class="number">
											</th>
	
											<th class="date">
												Date
											</th>
	
											<th class="time">
												Time
											</th>
	
											<th class="type">
												Type
											</th>
	
											<th class="florist">
												Filler Code
											</th>
	
											<th class="amount">
												Merc Value
											</th>
	
											<th class="status">
												Status
											</th>
	
											<th class="id">
												Mercury ID
											</th>
										</tr>
									</table>
								</div>
									
								<div class="messageContainer" style="overflow: visible; padding-right: 16px; height: 133px;">
									<table cellpadding="0" cellspacing="0" style="width: 100%; margin-right: -16px;">
										<xsl:variable name="mercuryMessages" select="$xvMessages/message[normalize-space(group_indicator) = 'Mercury']"/>
										<xsl:variable name="venusMessages" select="$xvMessages/message[normalize-space(group_indicator) = 'Venus']"/>

	
										<xsl:for-each select="$mercuryMessages | $venusMessages">
											<tr>
											<xsl:attribute name="bgcolor">
											    <xsl:if test="live_ftd = 'Y'">
											        <xsl:text>99CC99</xsl:text>
											    </xsl:if>
											</xsl:attribute>
											
												<td class="number">
													<xsl:value-of select="normalize-space(row_number)"/>.
												</td>
	
												<td class="date">
													<xsl:call-template name="mercMessFormatDate">
														<xsl:with-param name="dateString" select="normalize-space(message_date)"/>
													</xsl:call-template>
												</td>
	
												<td class="time">
													<xsl:value-of select="normalize-space(message_time)"/>
												</td>
	
												<td class="type">
													<xsl:variable name="messageType" select="normalize-space(msg_type)"/>
													<xsl:if test="not(starts-with($messageType, 'FTD'))">
														&nbsp;&nbsp;
													</xsl:if>
													<a class="tooltip" href="javascript: openMessage({row_number});" tabindex="-1">
														<xsl:value-of select="substring($messageType, 1, 28)"/>
                                                                                                                <span><xsl:choose>
                                                                                                                <xsl:when test="string-length(comments) > 0"><xsl:value-of select="normalize-space(comments)" disable-output-escaping="yes"/></xsl:when>
                                                                                                                <xsl:otherwise>Click to view contents. (Popup feature temporarily disabled.)</xsl:otherwise>
                                                                                                                </xsl:choose></span>
													</a>
												</td>
	
												<td class="florist">
													<xsl:variable name="florist">
														<xsl:choose>
															<xsl:when test="message_direction = 'OUTBOUND'">
																<xsl:variable name="temp2" select="normalize-space(filling_florist)"/>
																<xsl:value-of select="substring($temp2,1,9)"/>
															</xsl:when>
															<xsl:when test="contains(sending_florist,'ADMIN SENDER')">
																<xsl:variable name="temp3" select="normalize-space('EFOS')"/>
																<xsl:value-of select="substring($temp3,1,9)"/>
															</xsl:when>
															<xsl:otherwise>
																<xsl:variable name="temp4" select="normalize-space(sending_florist)"/>
																<xsl:value-of select="substring($temp4,1,9)"/>
															</xsl:otherwise>
														</xsl:choose>				
													</xsl:variable>
	
													<xsl:choose>
														<xsl:when test="group_indicator = 'Venus'">
															<xsl:text>VENDOR</xsl:text>
														</xsl:when>
	
														<xsl:when test="$florist = ''">
															<xsl:text>UNASSIGNED</xsl:text>
														</xsl:when>
	
														<xsl:when test="$florist = 'EFOS'">
															<xsl:text>EFOS</xsl:text>
														</xsl:when>
	
														<xsl:otherwise>
															<a class="tooltip" id = "floristLink" href="javascript:openFlorist('{$florist}');">
																<xsl:value-of select="$florist"/>
																<xsl:if test="starts-with($messageType, 'FTD')">
																		<span style = "width : 150px"><xsl:choose>
	                                                                   	<xsl:when test="string-length(filler_name) > 0">
	                                                                   		<xsl:value-of select='normalize-space(filler_name)'/>
	                                                                   		<script type="text/javascript"> 
	                                                                   		  document.write(returnNewLine());
	                                                                   		  document.write(reformatUSPhone('<xsl:value-of select="normalize-space(filler_phone)"/>'));
																		    </script>
	                                                                   	</xsl:when>
	                                                                  	<xsl:otherwise>Click to view contents. (Popup feature temporarily disabled.)</xsl:otherwise>
	                                                                  	</xsl:choose></span>
																</xsl:if>												
															</a>
															
														</xsl:otherwise>
													</xsl:choose>
												</td>
	
												<td class="amount">
													<xsl:variable name="messageType2" select="normalize-space(msg_type)"/>
                          <xsl:choose>
                            <xsl:when test="starts-with($messageType2, 'ADJ')">
	  												  <xsl:variable name="ftdPrice" select="normalize-space(over_under_charge)"/>
 														  <xsl:value-of select='format-number($ftdPrice, "#,###,##0.00")'/>
                            </xsl:when>
                            <xsl:otherwise>
	  												  <xsl:variable name="ftdPrice" select="normalize-space(price)"/>
   			  										<xsl:if test="$ftdPrice &gt; 0">
	  			  										<xsl:value-of select='format-number($ftdPrice, "##,###,##0.00")'/>
		    											</xsl:if>
                            </xsl:otherwise>
                          </xsl:choose>
												</td>
	
												<td class="status">
													<xsl:value-of select="normalize-space(message_status)"/>
												</td>
	
                                                                                                <td class="id">
                                                                                                    <xsl:choose>
                                                                                                        <xsl:when test="lifecycle_status != ''">
                                                                                                            <a class="tooltipLS" id="lifecycleLink" href="javascript:openLifecycle('{message_id}');">
                                                                                                                <xsl:value-of select="normalize-space(message_number)"/>
                                                                                                                <span>
                                                                                                                    <xsl:value-of select="normalize-space(lifecycle_status)"/>
                                                                                                                </span>
                                                                                                            </a>
                                                                                                        </xsl:when>
                                                                                                        <xsl:otherwise>
                                                                                                            <xsl:value-of select="normalize-space(message_number)"/>
                                                                                                        </xsl:otherwise>
                                                                                                    </xsl:choose>
                                                                                                </td>
											</tr>
										</xsl:for-each>
										
									</table> <!-- End of messageTable -->
								</div> <!-- End of messageContainer -->
	
								<xsl:if test="$bypass_com != 'Y'">
                                                                    <!-- Messages footer -->
                                                                    <table class="PaneSection">
                                                                            <tr>
                                                                                    <td align="center">
                                                                                            <button type="button"  class="BlueButton" name ="floristDashboardButton" accesskey="S" onclick="openDashboard();">
                                                                                                    Florist Da(s)hboard
                                                                                            </button>
                                                                                    </td>
                                                                            </tr>
                                                                    </table>
                                                                
            
                                                                    <!-- Email banner-->
                                                                    <table class="PaneSection">
                                                                            <tr>
                                                                                    <td align="center" class="banner">
                                                                                            Email / Letters
                                                                                    </td>
                                                                            </tr>
                                                                    </table>
            
                                                                    <!-- Emails and letters -->
                                                                    <div class="emailContainer" style="padding-right: 16px;">
                                                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                                                    <tr>
                                                                                            <th class="number">
                                                                                            </th>
            
                                                                                            <th class="date">
                                                                                                    Date
                                                                                            </th>
            
                                                                                            <th class="time">
                                                                                                    Time
                                                                                            </th>
            
                                                                                            <th class="type">
                                                                                                    Type
                                                                                            </th>
            
                                                                                            <th class="email">
                                                                                                    Email Address / Letter Type
                                                                                            </th>
            
                                                                                            <th class="subject">
                                                                                                    Subject
                                                                                            </th>
                                                                                    </tr>
                                                                            </table>
                                                                    </div>
            
                                                                    <div class="emailContainer" style="overflow: visible; padding-right: 16px; height: 105px;">
                                                                            <table cellpadding="0" cellspacing="0" style="width: 100%; margin-right: -16px;">
                                                                                    <xsl:for-each select="$xvEmailLetters/email-letter">
                                                                                            <xsl:variable name="lineNum" select="concat(normalize-space(row_number),'E')"/>
                                                                                            
                                                                                            <tr>
                                                                                                    <td class="number">
                                                                                                            <xsl:value-of select="$lineNum"/>.
                                                                                                    </td>
            
                                                                                                    <td class="date">
                                                                                                            <xsl:call-template name="mercMessFormatDate">
                                                                                                                    <xsl:with-param name="dateString" select="normalize-space(message_date)"/>
                                                                                                            </xsl:call-template>
                                                                                                    </td>
            
                                                                                                    <td class="time">
                                                                                                            <xsl:value-of select="normalize-space(message_time)"/>
                                                                                                    </td>
            
                                                                                                    <td class="type">
                                                                                                            <a class="tooltip" href="javascript: openMessage('{$lineNum}');" tabindex="-1">
                                                                                                                    <xsl:variable name="temp5" select="normalize-space(msg_type)"/>
                                                                                                                    <xsl:value-of select="substring($temp5, 1, 12)"/>
                                                                                                                    <span><xsl:choose>
                                                                                                                    <xsl:when test="string-length(message_body) > 0"><xsl:value-of select="normalize-space(message_body)"/></xsl:when>
                                                                                                                    <xsl:otherwise>Click to view contents. (Popup feature temporarily disabled.)</xsl:otherwise>
                                                                                                                    </xsl:choose></span>
                                                                                                            </a>
                                                                                                    </td>
            
                                                                                                    <td class="email">
                                                                                                                    <xsl:variable name="temp6" select="normalize-space(sender_email_address)"/>
                                                                                                                    <xsl:value-of select="substring($temp6, 1, 55)"/>
                                                                                                    </td>
            
                                                                                                    <td class="subject">
                                                                                                                    <xsl:variable name="temp7" select="normalize-space(subject)"/>
                                                                                                                    <xsl:value-of select="substring($temp7, 1, 26)"/>
                                                                                                    </td>
                                                                                            </tr>
                                                                                    </xsl:for-each>
                                                                            </table> <!-- End of emailTable -->
                                                                    </div> <!-- End of emailContainer -->
                                                                </xsl:if>
	
								<table class="PaneSection">
									<tr>
										<xsl:choose>
                                                                                    <xsl:when test="$bypass_com = 'Y'">
                                                                                        <td width="33%"/>
                                                                                        <td width="33%"/>
                                                                                    </xsl:when>
                                                                                    <xsl:otherwise>
                                                                                        <td width="33%"/>
                
                                                                                        <td align="center" width="33%">
                                                                                                <button type="button"  name = "qDeleteButton" class="BlueButton" accesskey="D" onclick="openQDelete();">
                                                                                                        Queue (D)elete
                                                                                                </button>
                                                                                        </td>
                                                                                    </xsl:otherwise>
                                                                                </xsl:choose>
        
										<td align="right" width="33%">
											<xsl:if test="$xvTotalPages &gt; 1">
												<xsl:choose>
													<xsl:when test="$xvCurrentPage = 1">
														<button type="button" class="arrowButton" disabled="disabled">
															7
														</button>
	
														<button type="button" class="arrowButton" disabled="disabled">
															3
														</button>
													</xsl:when>
	
													<xsl:otherwise>
														<button type="button" class="arrowButton" onclick="newPage(1);">
															7
														</button>
	
														<button type="button" class="arrowButton" onclick="newPage({$xvCurrentPage - 1})">
															3
														</button>
													</xsl:otherwise>
												</xsl:choose>
	
												<xsl:choose>
													<xsl:when test="number($xvCurrentPage) = number($xvTotalPages)">
														<button type="button"  class="arrowButton" disabled="disabled">
															4
														</button>
	
														<button type="button" class="arrowButton" disabled="disabled">
															8
														</button>
													</xsl:when>
	
													<xsl:otherwise>
														<button type="button" class="arrowButton" onclick="newPage({$xvCurrentPage + 1});">
															4
														</button>
	
														<button type="button" class="arrowButton" onclick="newPage({$xvTotalPages});">
															8
														</button>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:if>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table> <!-- End of mainTable -->
				</div>

				<div id="waitDiv" style="display: none;">
					<table id="waitTable" class="mainTable" width="98%" height="100%" align="center" cellpadding="2" cellspacing="1" >
						<tr>
							<td width="100%">
								<table width="100%" cellspacing="0" cellpadding="0" border="0">                      
									<tr>
										<td id="waitMessage" align="right" width="50%" class="WaitMessage"></td>
										<td id="waitTD"  align="left" width="50%" class="WaitMessage"></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</div>


				
				<input type="hidden" name="load_member_data_url" id="load_member_data_url" value="{$xvLoadMemberDataUrl}"/>
			
			    	<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" style="margin-top: 6px;">
			    		<tr>    
			    			<xsl:choose>
                                                    <xsl:when test="$bypass_com = 'Y'">
                                                        <td>
                          
                                                                <button type="button"  id="recipientOrderButton" class="BigBlueButton" style="WIDTH: 115px" accesskey="O" onclick="openRecipientOrder();">
                                                                        Recipient/<br/>(O)rder
                                                                </button>
                                
                                                                <button type="button"  id="orderCommentsButton" class="BigBlueButton" style="WIDTH: 115px" accesskey="C" onclick="openOrderComments();">
                                                                		Order<br/>(C)omments
                                                                </button><br/>
                                                                <button type="button"  id="historyButton" class="BigBlueButton ButtonTextBox1" accesskey="H" onclick="openHistory();">
                                                                		Order<br/> (H)istory
                                                                </button>
                                                                <button type="button" id="originalViewButton" class="BigBlueButton ButtonTextBox1" accesskey="V" onclick="openOriginalView();">Original<br/>(V)iew</button>
                                                        </td>
                                
                                                        <td align="right">
                                                                <button type="button"  id="backButton" style="width:93px;" class="BlueButton" accesskey="B" onclick="doBackAction();">
                                                                        (B)ack to Order
                                                                </button>
                                
                                                                <br/>
                                
                                                                <button type="button"   id="mainMenuButton" style="width:93px;" class="BlueButton" accesskey="M" onclick="doMainMenuAction();">
                                                                        (M)ain Menu
                                                                </button>
                                                        </td>   
                                                    </xsl:when>
                                                    <xsl:otherwise>
                                                
                                                        <td>
                          
                                                                <button type="button"   id="recipientOrderButton" class="BigBlueButton" style="WIDTH: 115px" accesskey="O" onclick="openRecipientOrder();">
                                                                        Recipient/<br/>(O)rder
                                                                </button>
                                
                                                                <button type="button"   id="orderCommentsButton" class="BigBlueButton" style="WIDTH: 115px" accesskey="C" onclick="openOrderComments();">
                                                                 Order<br/>(C)omments
                                                                </button>
                                                                <button type="button"   id="refundButton" class="BigBlueButton ButtonTextBox1" accesskey="R" onclick="openRefund();">
                                                                 (R)efund
                                                                </button>
                                                                <button type="button"   id="emailButton" class="BigBlueButton ButtonTextBox1" accesskey="E" onclick="openEmail();" >
                                                                        <xsl:if test="$msg_email_indicator = 'true'">
													                      <xsl:attribute name="style">font-weight:bold</xsl:attribute>
													                    </xsl:if>
                                                                  (E)mail
                                                                </button>
                                                                <br/>
                                                                <button type="button"   id="historyButton" class="BigBlueButton ButtonTextBox1" accesskey="H" onclick="openHistory();">
                                                                  Order <br/> (H)istory
                                                                </button>
                                								<button type="button" id="originalViewButton" class="BigBlueButton ButtonTextBox1" accesskey="V" onclick="openOriginalView();">
													               Original<br/>(V)iew
												                </button>
                                                                
        
                                                               <!--  <button type="button"   id="lettersButton" class="BigBlueButton" accesskey="L" onclick="openLetters();">
                                                                        <xsl:choose>
                                                                                        <xsl:when test="$msg_letter_indicator = 'true'">
                                                                                                <b>(L)etters</b>
                                                                                        </xsl:when>
                                                                                        
                                                                                        <xsl:otherwise>
                                                                                                (L)etters
                                                                                        </xsl:otherwise>
                                                                                </xsl:choose>
                                                                        
                                                                </button> -->
                                
                                                                <button type="button"   id="floristInquiryButton" class="BigBlueButton ButtonTextBox1" accesskey="F" onclick="openFloristInquiry();">
                                                                        (F)lorist<br/>Inquiry
                                                                </button>
                                                                
                                                                <button type="button"   id="tagOrder{@num}" class="BigBlueButton ButtonTextBox1" accesskey="T" onclick="javascript:doTagOrderAction('{$order_tagged_indicator}','{$order_detail_id}');">
                                                                                
                                                                                        <xsl:if test="$order_tagged_indicator != '0'">
                                                                                        <xsl:attribute name="style">font-weight:bold</xsl:attribute>
                                                                                        </xsl:if>
                                                                                
                                                                        (T)ag<br />Order</button>
                                        
                                                        </td>
                                
                                                        <td align="right">
                                                                <button type="button"   id="backButton" style="width:93px;" class="BlueButton" accesskey="B" onclick="doBackAction();">
                                                                        (B)ack to Order
                                                                </button>
                                
                                                                <br/>
                                
                                                                <button type="button"   id="mainMenuButton" style="width:93px;" class="BlueButton" accesskey="M" onclick="doMainMenuAction();">
                                                                        (M)ain Menu
                                                                </button>
                                                        </td>
                                                    </xsl:otherwise>
                                                </xsl:choose>
			        	</tr>   
			        </table>
			       
				
				
				
				<xsl:call-template name="footer"/>
			</form>
		</body>
	</html>

</xsl:template>

</xsl:stylesheet>