<?xml version='1.0'?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html"
			doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
			doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
			indent="yes"/>
<xsl:template name="dateRangeSelect">
	<tr>
											<td class="label">
												DELIVERY DATE
												<xsl:if test="$xvOrderDeliveryDateEnd != ''">
													<xsl:text> RANGE</xsl:text>
												</xsl:if>
												:
											</td>
	
											<td class="datum">
												<select name="delivery_month" id="delivery_month">
													<option value="0">
														<xsl:if test="starts-with($xvOrderDeliveryDate, 'JAN')">
															<xsl:attribute name="selected">
																selected
															</xsl:attribute>
														</xsl:if>
														January
													</option>
	
													<option value="1">
														<xsl:if test="starts-with($xvOrderDeliveryDate, 'FEB')">
															<xsl:attribute name="selected">
																selected
															</xsl:attribute>
														</xsl:if>
														February
													</option>
	
													<option value="2">
														<xsl:if test="starts-with($xvOrderDeliveryDate, 'MAR')">
															<xsl:attribute name="selected">
																selected
															</xsl:attribute>
														</xsl:if>
														March
													</option>
	
													<option value="3">
														<xsl:if test="starts-with($xvOrderDeliveryDate, 'APR')">
															<xsl:attribute name="selected">
																selected
															</xsl:attribute>
														</xsl:if>
														April
													</option>
	
													<option value="4">
														<xsl:if test="starts-with($xvOrderDeliveryDate, 'MAY')">
															<xsl:attribute name="selected">
																selected
															</xsl:attribute>
														</xsl:if>
														May
													</option>
	
													<option value="5">
														<xsl:if test="starts-with($xvOrderDeliveryDate, 'JUN')">
															<xsl:attribute name="selected">
																selected
															</xsl:attribute>
														</xsl:if>
														June
													</option>
	
													<option value="6">
														<xsl:if test="starts-with($xvOrderDeliveryDate, 'JUL')">
															<xsl:attribute name="selected">
																selected
															</xsl:attribute>
														</xsl:if>
														July
													</option>
	
													<option value="7">
														<xsl:if test="starts-with($xvOrderDeliveryDate, 'AUG')">
															<xsl:attribute name="selected">
																selected
															</xsl:attribute>
														</xsl:if>
														August
													</option>
	
													<option value="8">
														<xsl:if test="starts-with($xvOrderDeliveryDate, 'SEP')">
															<xsl:attribute name="selected">
																selected
															</xsl:attribute>
														</xsl:if>
														September
													</option>
	
													<option value="9">
														<xsl:if test="starts-with($xvOrderDeliveryDate, 'OCT')">
															<xsl:attribute name="selected">
																selected
															</xsl:attribute>
														</xsl:if>
														October
													</option>
	
													<option value="10">
														<xsl:if test="starts-with($xvOrderDeliveryDate, 'NOV')">
															<xsl:attribute name="selected">
																selected
															</xsl:attribute>
														</xsl:if>
														November
													</option>
	
													<option value="11">
														<xsl:if test="starts-with($xvOrderDeliveryDate, 'DEC')">
															<xsl:attribute name="selected">
																selected
															</xsl:attribute>
														</xsl:if>
														December
													</option>
												</select>
	
												<input type="text" name="delivery_date" id="delivery_date" maxlength="2" size="2" value="{substring($xvOrderDeliveryDate, 5, 2)}" onKeyPress="return noEnter();"/>
												<span id="invalidOrderDeliveryDate" class="RequiredFieldTxt" style="display: none;">
												</span>
											</td>
										</tr>
	<!--***********************************-->
										<xsl:if test="$xvOrderDeliveryDateEnd != ''">
											<tr>
												<td class="label"/>
	
												<td class="datum">
													<select name="delivery_month_end" id="delivery_month_end">
														<option value="0">
															<xsl:if test="starts-with($xvOrderDeliveryDateEnd, 'Jan')">
																<xsl:attribute name="selected">
																	selected
																</xsl:attribute>
															</xsl:if>
															January
														</option>
		
														<option value="1">
															<xsl:if test="starts-with($xvOrderDeliveryDateEnd, 'Feb')">
																<xsl:attribute name="selected">
																	selected
																</xsl:attribute>
															</xsl:if>
															February
														</option>
		
														<option value="2">
															<xsl:if test="starts-with($xvOrderDeliveryDateEnd, 'Mar')">
																<xsl:attribute name="selected">
																	selected
																</xsl:attribute>
															</xsl:if>
															March
														</option>
		
														<option value="3">
															<xsl:if test="starts-with($xvOrderDeliveryDateEnd, 'Apr')">
																<xsl:attribute name="selected">
																	selected
																</xsl:attribute>
															</xsl:if>
															April
														</option>
		
														<option value="4">
															<xsl:if test="starts-with($xvOrderDeliveryDateEnd, 'May')">
																<xsl:attribute name="selected">
																	selected
																</xsl:attribute>
															</xsl:if>
															May
														</option>
		
														<option value="5">
															<xsl:if test="starts-with($xvOrderDeliveryDateEnd, 'Jun')">
																<xsl:attribute name="selected">
																	selected
																</xsl:attribute>
															</xsl:if>
															June
														</option>
		
														<option value="6">
															<xsl:if test="starts-with($xvOrderDeliveryDateEnd, 'Jul')">
																<xsl:attribute name="selected">
																	selected
																</xsl:attribute>
															</xsl:if>
															July
														</option>
		
														<option value="7">
															<xsl:if test="starts-with($xvOrderDeliveryDateEnd, 'Aug')">
																<xsl:attribute name="selected">
																	selected
																</xsl:attribute>
															</xsl:if>
															August
														</option>
		
														<option value="8">
															<xsl:if test="starts-with($xvOrderDeliveryDateEnd, 'Sep')">
																<xsl:attribute name="selected">
																	selected
																</xsl:attribute>
															</xsl:if>
															September
														</option>
		
														<option value="9">
															<xsl:if test="starts-with($xvOrderDeliveryDateEnd, 'Oct')">
																<xsl:attribute name="selected">
																	selected
																</xsl:attribute>
															</xsl:if>
															October
														</option>
		
														<option value="10">
															<xsl:if test="starts-with($xvOrderDeliveryDateEnd, 'Nov')">
																<xsl:attribute name="selected">
																	selected
																</xsl:attribute>
															</xsl:if>
															November
														</option>
		
														<option value="11">
															<xsl:if test="starts-with($xvOrderDeliveryDateEnd, 'Dec')">
																<xsl:attribute name="selected">
																	selected
																</xsl:attribute>
															</xsl:if>
															December
														</option>
													</select>
		
													<input type="text" name="delivery_date_end" id="delivery_date_end" maxlength="2" size="2" value="{substring($xvOrderDeliveryDateEnd, 5, 2)}" onKeyPress="return noEnter();"/>
													<span id="invalidOrderDeliveryDateEnd" class="RequiredFieldTxt" style="display: none;">
													</span>
												</td>
											</tr>
										</xsl:if>
</xsl:template>

</xsl:stylesheet><!-- Stylus Studio meta-information - (c) 2004-2005. Progress Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperMetaTag><MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/><MapperBlockPosition><template match="/"></template><template match="dateRangeSelect"></template></MapperBlockPosition></MapperMetaTag>
</metaInformation>
-->