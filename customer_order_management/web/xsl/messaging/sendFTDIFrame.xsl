<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:key name="security" match="/root/security/data" use="name"/>

<xsl:param name="msg_order_type"><xsl:value-of select="key('security','msg_order_type')/value" /></xsl:param>
<xsl:output method="html" indent="yes"/>

<xsl:template match="/root">

  <html>
    <head>
      <script type="text/javascript" language="javascript">
        forwardAction();
	
		function forwardAction()
		{
			//get error message
			var error = "<xsl:value-of select='/root/error_message'/>";
					
			if((error == null) || (error.length &lt; 1))
			{
				var forwardAction = "<xsl:value-of select='/root/forwarding_action'/>";
				var floristCode = "<xsl:value-of select='/root/filling_florist_code'/>";
				var floristSelectionLogId = "<xsl:value-of select='/root/florist_selection_log_id'/>";
				var zipCityFlag = "<xsl:value-of select='/root/zip_city_flag'/>";
				if(!forwardAction.match(/^\w/)){
				   
				    forwardAction=forwardAction.substr(1);
				}
				parent.submitNewFtd(forwardAction, floristCode, floristSelectionLogId, zipCityFlag);
			}
			else
			{
				//If there are no more florists available for auto selection, 
				//the user will get the following message next to the textbox in red font:
				//"There are no more florists available for auto selection."
				parent.hideWaitMessage("content", "wait");
				parent.noFlorists(error);
			} 
        }
      </script>
    </head>
    <body></body>
  </html>
</xsl:template>

</xsl:stylesheet><!-- Stylus Studio meta-information - (c) 2004-2005. Progress Software Corporation. All rights reserved.
<metaInformation>
<scenarios ><scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="..\..\..\..\..\..\tmp\iframe.xml" htmlbaseurl="" outputurl="" processortype="internal" profilemode="0" profiledepth="" profilelength="" urlprofilexml="" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext=""/></scenarios><MapperMetaTag><MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/><MapperBlockPosition><template name="MessageForwardIFrame"></template></MapperBlockPosition></MapperMetaTag>
</metaInformation>
-->