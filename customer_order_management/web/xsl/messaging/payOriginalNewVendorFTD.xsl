<!DOCTYPE stylesheet [
	<!ENTITY  nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:import href="../header.xsl"/>
<xsl:import href="../footer.xsl"/>
<xsl:import href="mercMessButtons.xsl"/>
<xsl:import href="../securityanddata.xsl"/>
<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>
<xsl:import href="messagingSecurityAndData.xsl"/>
<xsl:import href="mercMessFormatPhone.xsl"/>
<xsl:import href="mercMessSendButton.xsl"/>
<xsl:import href="dateRangeSelect.xsl"/>
<xsl:import href="compVendorDeliveryDates.xsl"/>
<xsl:key name="security" match="/message/security/data" use="name"/>
<xsl:output method="html"
			doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
			doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
			indent="yes"/>


<xsl:include href="mercMessFields.xsl"/>
<xsl:variable name="xvVendorCost" select="format-number(normalize-space(message/vendorCost), '#,###,##0.00')"/>

<xsl:template match="/">

	<html>
		<head>
			<title><xsl:value-of select="$xvHeaderName"/></title>

			<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
			<meta http-equiv="language" content="en-us"/>

			<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
			<link rel="stylesheet" type="text/css" href="css/mercuryMessage.css"/>
			<script type="text/javascript" src="js/commonUtil.js">;</script>
			<script type="text/javascript" src="js/util.js">;</script>
			
			<script type="text/javascript" src="js/mercuryMessage.js">;</script>
			<script>
				<![CDATA[
					var actions = new Array();
					function init()
					{
						document.oncontextmenu = contextMenuHandler;
						document.onkeydown = backKeyHandler;
						var buttons = document.getElementsByTagName("BUTTON");
					
						for(var i = 0; i < buttons.length; i++)
						{
							if(buttons[i].id != "mainMenuButton" && buttons[i].id != "sendButton")
							{
								actions[buttons[i].id] = buttons[i].onclick;
								buttons[i].onclick = checkButtons;
							}else if(buttons[i].id == "mainMenuButton"){
								buttons[i].onclick=mainMenuAction;
							}
						}
						untabHeader();
					}
					
					
				
					
	
					function validateMessageForm()
					{						
				]]>
					
				<![CDATA[
						var isValidRecipientName = checkRecipientName("recipient_name", "invalidRecipientName");
						var isValidRecipientStreet = checkRecipientStreet("recipient_street", "invalidRecipientStreet");
						var isValidRecipientCity = checkRecipientCity("recipient_city", "invalidRecipientCityStateZip");
						var isValidRecipientState = checkRecipientState("recipient_state", "invalidRecipientCityStateZip");
						var isValidRecipientZip = checkRecipientZip("recipient_zip", "invalidRecipientCityStateZip");
						var isValidRecipientPhone = checkRecipientPhone("recipient_phone", "invalidRecipientPhone");
						prepareDeliveryDate("payOriginalNewFTDForm");
						var isProductDelivarable = checkProductDeliverability();				            			
						var isValidDate = validateShipMethodAndDeliveryDate();
	   					var orderHasDeliveryRestrictions = checkOrderHasDeliveryRestrictions();
						return (
				]]>
				
				
				
				<![CDATA[
						isValidRecipientName
						&& isValidRecipientStreet
						&& isValidRecipientCity
						&& isValidRecipientZip
						&& isValidRecipientPhone
            && isValidDate
            && orderHasDeliveryRestrictions
            && isProductDelivarable
						);
					}

					function doSendNewFTDMessageAction(actionType)
					{
						showWaitMessage("content", "wait");

						if(validateMessageForm())
						{
							// var tempDelDate = new Date(document.forms["payOriginalNewFTDForm"].delivery_date.value);
							var requestURL = "validateAjax.do?"
								+ "method=validateDeliveryDate"
								+ "&delivery_date=" + document.forms["payOriginalNewFTDForm"].delivery_date.value
								+ "&zip_code=" + document.getElementById('recipient_zip').value
								+ "&product_id=" + document.getElementById('product_id').value
								+ "&country=" + document.getElementById('recipient_country').value
								+ "&order_add_ons=" + document.getElementById('order_add_ons').value
								+ "&morning_delivery=" + document.getElementById('order_has_morning_delivery').value
								+ "&source_code=" + document.getElementById('source_code').value
								+ "&securitytoken=" + document.getElementById("securitytoken").value
								+ "&context=" + document.getElementById("context").value;

							var data = getSynchronousResponse(requestURL);
							if (eval(data.responseText) == null || eval(data.responseText) == undefined) {
								window.location = "http://" + window.location.host + "/secadmin";
								return;
							} else {
								var outValue = eval(data.responseText);
      
								if(outValue == 'YES'){
									document.forms[0].recipient_phone.value = document.forms[0].recipient_phone_area.value + document.forms[0].recipient_phone_prefix.value + document.forms[0].recipient_phone_number.value;
 		             						document.forms[0].submit();
								} else {
									hideWaitMessage("content", "wait");
									if (outValue == 'NO MD') {
									    alert('Morning Delivery is not available for this delivery date');
									} else if (outValue == 'NO ADDON') {
									    alert('One or more of the previously selected Addons are no longer available.');
									} else {
								 	    alert('Product is not available or can no longer be delivered by selected vendor.');
									}
									return;
								}
							}
						
						}
						else
						{
							var delDate = new Date(document.forms["payOriginalNewFTDForm"].delivery_date.value);
            				var delDay = delDate.getDay()
            				if (delDay != null){
            				
            					var isFTDWOrder = document.forms["payOriginalNewFTDForm"].is_ftdw_order.value;
            				
	            				if(document.forms["payOriginalNewFTDForm"].order_has_morning_delivery.value == 'Y' && delDay == 1 && isFTDWOrder == 'false')
	            				{
				 				 	alert("Morning Delivery Orders cannot be delivered on Monday. Please contact the customer for an alternate delivery date.");
	              					hideWaitMessage("content", "wait");                				
								}
								else if(!checkProductDeliverability()) {
								  alert('Product is not available or can no longer be delivered by selected vendor.');
								  hideWaitMessage("content", "wait");    
								}
								else
								{	
									alert("Correct fields in error.");
              						hideWaitMessage("content", "wait");
              					}
							}
							else
							{
								alert("Correct fields in error.");
              					hideWaitMessage("content", "wait");
              				}
						}
					}
				]]>
			</script>

			<style>
        		td.label {
        			width: 225px;
        			vertical-align: middle;
        		}
			</style>
		</head>

		<body onload="init();">
			<form id="payOriginalNewFTDForm" method="post" action="sendFTDMessage.do" onsubmit="return validateMessageForm();">

				<xsl:call-template name="securityanddata"/>
				<xsl:call-template name="decisionResultData"/>
				<xsl:call-template name="messagingSecurityAndData"/>

				<input type="hidden" name="order_delivery_date" id="order_delivery_date" value="{$xvOrderDeliveryDate}"/>
				<input type="hidden" name="order_delivery_timestamp" id="order_delivery_timestamp" value="{$xvOrderDeliveryDateTimestamp}"/>
				<input type="hidden" name="order_delivery_date_end" id="order_delivery_date_end" value="{$xvOrderDeliveryDateEnd}"/>
				
                <input type="hidden" name="product_type" id="product_type" value="{normalize-space(message/product/product_info/productType)}"/>
                
				<input type="hidden" name="order_date_text" id="order_date_text" value="{$xvOrderDate}"/>
				<input type="hidden" name="order_date" id="order_date" value="{$xvOrderDateTimestamp}"/>
        
        <input type="hidden" name="ship_method" id="ship_method" value=""/>
        <input type="hidden" name="delivery_date" id="delivery_date" value=""/>
        <input type="hidden" name="recipient_phone" id="recipient_phone" value=""/>

				<input type="hidden" name="ftd_message_id" id="ftd_message_id" value="{$xvFtdMessageID}"/>
				<input type="hidden" name="product_id" id="product_id" value="{normalize-space(message/productId)}"/>
				<input type="hidden" name="order_has_morning_delivery" id="order_has_morning_delivery" value="{normalize-space(message/orderHasMorningDelivery)}"/>
				<input type="hidden" name="order_add_ons" id="order_add_ons" value="{normalize-space(message/orderAddOns)}"/>
				<input type="hidden" name="is_ftdw_order" id="is_ftdw_order" value="{normalize-space(message/isFTDWOrder)}"/>
				<input type="hidden" name="source_code" id="source_code" value="{normalize-space(message/sourceCode)}"/>

				<iframe id="FloristStatusIFrame" name="FloristStatusIFrame" width="0" height="0" border="0" style="display: none"/>
				<iframe name="unlockFrame" id="unlockFrame" height="0" width="0" border="0"></iframe>
				<iframe id="checkLock" width="0px" height="0px" border="0"/>
				<xsl:choose>				
				<xsl:when test="$destination ='' ">
				    <xsl:call-template name="header">
					<xsl:with-param name="showSearchBox" select="false()"/>
					<xsl:with-param name="showPrinter" select="true()"/>
					<xsl:with-param name="showTime" select="true()"/>
					<xsl:with-param name="headerName" select="$xvHeaderName"/>
					
					<xsl:with-param name="cservNumber" select="$xvCservNumber"/>
					<xsl:with-param name="indicator" select="$xvIndicator"/>
					<xsl:with-param name="brandName" select="$xvBrandName"/>
					<xsl:with-param name="dnisNumber" select="$xvDnisNumber"/>
					<xsl:with-param name="showCSRIDs" select="true()"/>
					<xsl:with-param name="showBackButton" select="true()"/>
					<xsl:with-param name="backButtonLabel" select="'(B)ack to Communication'"/>
					<xsl:with-param name="cdispEnabledFlag" select="$cdisp_enabledFlag"/>
				    </xsl:call-template>
				</xsl:when>
				<xsl:otherwise>
				    <xsl:call-template name="header">
					<xsl:with-param name="showSearchBox" select="false()"/>
					<xsl:with-param name="showPrinter" select="true()"/>
					<xsl:with-param name="showTime" select="true()"/>
					<xsl:with-param name="headerName" select="$xvHeaderName"/>
					
					<xsl:with-param name="cservNumber" select="$xvCservNumber"/>
					<xsl:with-param name="indicator" select="$xvIndicator"/>
					<xsl:with-param name="brandName" select="$xvBrandName"/>
					<xsl:with-param name="dnisNumber" select="$xvDnisNumber"/>
					<xsl:with-param name="showCSRIDs" select="true()"/>
					<xsl:with-param name="cdispEnabledFlag" select="$cdisp_enabledFlag"/>
				    </xsl:call-template>
				</xsl:otherwise>
				</xsl:choose>
				
				<script>updateCurrentUsers();</script>

				<div id="content">
					 <table class="mainTable" width="98%" align="center" cellpadding="0" cellspacing="1">
              <tr>
                <td>
                  <table class="PaneSection">
                    <tr>
                      <td class="banner">
                        Message Detail
                        <xsl:if test="$xvMercuryMessageType">
                          &nbsp;-&nbsp;<xsl:value-of select="$xvMercuryMessageType"/>
                        </xsl:if>
                      </td>
                    </tr>
                  </table>
    
                  <div style="overflow: auto; height: 329px; padding-right: 16px;">
                    <table width="100%" cellspacing="0" cellpadding="0" style="margin-right: -16px;">
                      <tr>
                        <td class="label">
                          FILLER CODE:
                        </td>
    
                        <td class="datum">
                            <xsl:value-of select="$xvFillingFloristCode"/>
                            <input type="hidden" name="filling_florist_code" id="filling_florist_code" value="{$xvFillingFloristCode}"/>
                            
                        </td>
                      </tr>
    
                      <tr>
                        <td class="label">
                          SENDER CODE:
                        </td>
    
                        <td class="datum">
                          <xsl:value-of select="$xvSendingFloristCode"/>
                          <input type="hidden" name="sending_florist_code" id="sending_florist_code" value="{$xvSendingFloristCode}"/>
                        </td>
                      </tr>
    
                      <tr>
                        <td class="label">
                          DELIVER TO:
                        </td>
    
                        <td class="datum">
                          <input type="text" size="50" name="recipient_name" id="recipient_name" value="{$xvRecipientName}" onKeyPress="return noEnter();"/>
                          <span id="invalidRecipientName" class="RequiredFieldTxt" style="display: none;">
                          </span>
                        </td>
                      </tr>
    
                      <tr>
                        <td class="label">
                          BUSINESS NAME:
                        </td>
    
                        <td class="datum">
                          <input type="text" size="50" name="business_name" id="business_name" value="{normalize-space(message/businessName)}" onKeyPress="return noEnter();"/>
                          <span id="invalidBusiness_name" class="RequiredFieldTxt" style="display: none;">
                          </span>
                        </td>
                      </tr>
                      
                      <tr>
                        <td class="label">
                          STREET ADDRESS APT:
                        </td>
    
                        <td class="datum">
                          <input type="text" size="50" name="recipient_street" id="recipient_street" value="{$xvRecipientStreet}" onKeyPress="return noEnter();"/>
                          <span id="invalidRecipientStreet" class="RequiredFieldTxt" style="display: none;">
                          </span>
                        </td>
                      </tr>
    
                      <tr>
                        <td class="label">
                          CITY, STATE, ZIP:
                        </td>
    
                        <td class="datum">
                          <input type="text" size="30" name="recipient_city" id="recipient_city" value="{$xvRecipientCity}" onKeyPress="return noEnter();"/>
    
                          <select name="recipient_state" id="recipient_state">
                            <xsl:for-each select="message/STATES/STATE">
                            <option value="{id}">
                              <xsl:if test="id =$xvRecipientState">
                                <xsl:attribute name="selected">true</xsl:attribute>
                              </xsl:if>
                              <xsl:value-of select="name"/>
                            </option>
                            </xsl:for-each>
                          </select>
    
                          <input type="text" size="9" name="recipient_zip" id="recipient_zip" value="{$xvRecipientZip}" onKeyPress="return noEnter();"/>
                          <span id="invalidRecipientCityStateZip" class="RequiredFieldTxt" style="display: none;">
                          </span>
                        </td>
                      </tr>
    
                      <tr>
                        <td class="label">
                          COUNTRY:
                        </td>
    
                        <td class="datum">
                            <xsl:value-of select="message/recipientCountry"/>
                            <input type="hidden" name="recipient_country" id="recipient_country" value="{message/recipientCountry}"/>
                        </td>
                      </tr> 
    
                      <tr>
                        <td class="label">
                          PHONE:
                        </td>
    
                        <td class="datum">
                                                  
                          <input type="text" maxlength="3" size="3" name="recipient_phone_area" id="recipient_phone_area" value="{substring($xvRecipientPhone, 1, 3)}" onKeyPress="return noEnter();"/>
                          /<input type="text" maxlength="3" size="3" name="recipient_phone_prefix" id="recipient_phone_prefix" value="{substring($xvRecipientPhone, 4, 3)}" onKeyPress="return noEnter();"/>
                          -<input type="text" maxlength="4" size="4" name="recipient_phone_number" id="recipient_phone_number" value="{substring($xvRecipientPhone, 7)}" onKeyPress="return noEnter();"/>
                          <span id="invalidRecipientPhone" class="RequiredFieldTxt" style="display: none;">
                          </span>
                        </td>
                      </tr>
  
                      <tr>
                        <td colspan="3">
                          <xsl:call-template name="vendorDeliveryDates">
                            <xsl:with-param name="deliveryDates" select="deliveryDates"/>
                            <xsl:with-param name="origShipMethod" select="shipMethod"/>
                          </xsl:call-template>
                        </td>
                      </tr>
      
                      <tr>
                        <td class="label">
                          FIRST CHOICE:
                        </td>
    
                        <td class="datum">
                              <xsl:value-of select="$xvFirstChoice" disable-output-escaping="yes"/>
                              <input type="hidden" name="first_choice" id="first_choice" value="{$xvFirstChoice}"/>
                            
                        </td>
                      </tr>
                      
                      <tr>
                        <td class="label">
                          ALLOWED SUBSTITUTION:
                        </td>
    
                        <td class="datum">
                          NONE
                        </td>
                      </tr>
    
                        
                      <tr>
                        <td class="label">
                          Vendor Cost:
                        </td>
    
                        <td class="datum">
                            <xsl:value-of select="$xvVendorCost"/>
                            <input type="hidden" name="florist_prd_price" id="florist_prd_price" value="{$xvVendorCost}"/>
                        </td>
                      </tr>                  
    
                      <tr>
                      <td class="label">
                          CARD:
                        </td>
    
                        <td class="datum">
                          <input type="text" name="card_message" id="card_message" size="70" onKeyPress="return noEnter();">
                            <xsl:attribute name="value">
                              <xsl:choose>
                                <xsl:when test="$xvCardMessage = ''">
                                  <xsl:text>NO MESSAGE ENTERED</xsl:text>
                                </xsl:when>
    
                                <xsl:otherwise>
                                  <xsl:value-of select="$xvCardMessage"/>
                                </xsl:otherwise>
                              </xsl:choose>
                            </xsl:attribute>
                          </input>
                          <span id="invalidCardMessage" class="RequiredFieldTxt" style="display: none;">
                          </span>
                        </td>
                      </tr>
    
                      <tr>
                        <td class="label">
                          Occasion:
                        </td>
    
                        <td class="datum">
                            <xsl:value-of select="$xvOccasionDescription"/>
                            <input type="hidden" name="occasion_desc" id="occasion_desc" value="{$xvOccasionDescription}"/>
                        </td>
                      </tr>    
    
                      <tr>
                        <td class="label">
                          SPECIAL INSTRUCTIONS:
                        </td>
    
                        <td class="datum">
                            <xsl:value-of select="$xvInstructions"/>
                            <input type="hidden" name="spec_instruction" id="spec_instruction" value="{$xvInstructions}"/>
                        </td>
                      </tr>
    
                      <tr>
                        <td class="label" style="vertical-align: top;">
                          OPERATOR:
                        </td>
    
                        <td class="datum">
                          <xsl:value-of select="$xvOperatorCode"/>
                        </td>
                        <input type="hidden" name="message_operator" id="message_operator" value="{$xvOperatorCode}"/>
                      </tr>
                    </table> <!-- End of order -->
                  </div> <!-- End of scrolling div -->
                  <xsl:call-template name="mercMessSendButton">
                    <xsl:with-param name="action_type" select="'pay_original_send_new'"/>
                  </xsl:call-template>
                  
                </td>
              </tr>
            </table> <!-- End of mainTable -->
   			</div>

				<div id="waitDiv" style="display: none;">
					<table id="waitTable" class="mainTable" width="98%" height="100%" align="center" cellpadding="2" cellspacing="1" >
						<tr>
							<td width="100%">
								<table width="100%" cellspacing="0" cellpadding="0" border="0">                      
									<tr>
										<td id="waitMessage" align="right" width="50%" class="WaitMessage"></td>
										<td id="waitTD"  align="left" width="50%" class="WaitMessage"></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</div>

				<xsl:call-template name="mercMessButtons"/>
				<xsl:call-template name="footer"/>
			</form>
		</body>
	</html>

</xsl:template>

</xsl:stylesheet>