
<!DOCTYPE ACDemo[
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0"  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="header.xsl"/>
	<xsl:import href="footer.xsl"/>
	<xsl:import href="securityanddata.xsl"/>
	<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>
	<xsl:key name="security" match="/root/security/data" use="name"/>
		<xsl:output method="html" indent="yes"/>

	<xsl:template match="/">
		<html>
			<head>
				<base target="_self"/>
				<title>Credit Card</title>
				<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
				<script type="text/javascript" src="js/clock.js"></script>
				<script type="text/javascript" src="js/util.js"></script>
				<script type="text/javascript" language="JavaScript">


<![CDATA[

/***********************************************************************************
* enterAuth
* Go to Authorization action
************************************************************************************/
      function enterAuth() {
      
		var url = "enterAuth.do";
		performAction(url);
				
      }

/***********************************************************************************
* returnToBilling
* close page 
************************************************************************************/
      function returnToBilling() {

		//build array to return
		var returnArray= new Array()
		returnArray["page_action"]="returntobilling";

        top.returnValue = returnArray;
        top.close();
      }

/***********************************************************************************
* cancelBilling
* close page and return cancel string
************************************************************************************/
      function cancelBilling() {

		//build array to return
		var returnArray= new Array()
		returnArray["page_action"]="cancel";

        top.returnValue = returnArray;
        top.close();
      }

/***********************************************************************************
* performAction
* forward to a page
************************************************************************************/
function performAction(url)
{
  document.forms[0].action = url;
  document.forms[0].submit();
}


       ]]></script>
			</head>
			<body>
				<form method="post">
					<xsl:call-template name="securityanddata"/>
					<xsl:call-template name="decisionResultData"/>
          <input type="hidden" name="pay_type" id="pay_type" value="{root/pay_type}"/>
					<!-- Header-->
					<xsl:call-template name="header">
						<xsl:with-param name="headerName" select="''" />
						<xsl:with-param name="dnisNumber" select="$call_dnis"/>
						<xsl:with-param name="brandName" select="$call_brand_name" />
						<xsl:with-param name="indicator" select="$call_type_flag" />
						<xsl:with-param name="cservNumber" select="$call_cs_number" />
						<xsl:with-param name="showBackButton" select="false()"/>
						<xsl:with-param name="showTime" select="true()"/>
						<xsl:with-param name="cdispEnabledFlag" select="$cdisp_enabledFlag"/>
					</xsl:call-template>
					<!-- Display table-->
					<table width="98%" align="center" cellspacing="1" class="mainTable">
						<tr>
							<td>
								<table width="100%" align="center" class="innerTable">
									<tr><td class="TopHeading" align="center">
									<xsl:value-of select="root/message"/>
									</td></tr>
								</table>
							</td>
						</tr>
					</table>
					<table width="98%" align="center" cellspacing="1" class="mainTable">
						<tr>
							<td>
								<div align="center">
								     <button type="button" class="BlueButton" accesskey="E" onclick="enterAuth()">(E)nter Authorization Code</button>
									 &nbsp;
								     <button type="button" class="BlueButton" accesskey="C" onclick="cancelBilling()">(C)ancel Billing</button>
									&nbsp;
								     <button type="button" class="BlueButton" accesskey="R" onclick="returnToBilling();">(R)eturn to Billing</button>
								</div>
							</td>
						</tr>
					</table>
					<!--Copyright bar-->
					<xsl:call-template name="footer"/>
				</form>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>