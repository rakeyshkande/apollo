<!DOCTYPE ACDemo[
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0"  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html" indent="yes"/>
<xsl:template name="tag_frame" match="/">

<html>
<head>
<script type="text/javascript">
<![CDATA[

function tagAction()
{
parent.performcheck();
}

]]>
</script>

</head>
<body onload="tagAction();">
<form name="iframeform">
<input type="hidden" name="multiple_tags" id="multiple_tags" value="{root/multiple_tags}"/>
<input type="hidden" name="order_already_tagged_to_csr" id="order_already_tagged_to_csr" value="{root/order_already_tagged_to_csr}"/>
</form>
</body>
</html>
</xsl:template>
</xsl:stylesheet>