<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" indent="no"/>
	<xsl:output indent="yes"/>
	<xsl:key name="letterDetail" match="/root/letter/detail" use="name" />
	
	<!-- decimal format used when a value is not given -->
	<xsl:decimal-format NaN="0.00" name="notANumber"/>
	<xsl:decimal-format NaN="(0.00)"  name="noRefund"/>
	
	<xsl:template match="/root">
	
		<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
		  <fo:layout-master-set>
		    <fo:simple-page-master master-name="simpleA4" page-height="29.7cm" page-width="21cm" margin-top="2cm" margin-bottom="2cm" margin-left="2cm" margin-right="2cm">
		      <fo:region-body/>
		    </fo:simple-page-master>
		  </fo:layout-master-set>
		  
		  <fo:page-sequence master-reference="simpleA4">
		    <fo:flow flow-name="xsl-region-body">
		    
		    <fo:block text-align="center" space-after.optimum='15pt'>
           		<fo:external-graphic src="{key('letterDetail','logo')/value}"/>
			</fo:block>
			
			<fo:block font-size="10pt" space-after.optimum='35pt'><xsl:value-of select="key('letterDetail','current_date')/value"/></fo:block>
			<fo:block font-size="10pt"><xsl:value-of select="/root/ORDER/PRINT_CUSTOMERS/PRINT_CUSTOMER/first_name"/><xsl:text>&#160;</xsl:text><xsl:value-of select="/root/ORDER/PRINT_CUSTOMERS/PRINT_CUSTOMER/last_name"/></fo:block>
			<fo:block font-size="10pt"><xsl:value-of select="/root/ORDER/PRINT_CUSTOMERS/PRINT_CUSTOMER/address_1"/></fo:block>
			<fo:block font-size="10pt"><xsl:value-of select="/root/ORDER/PRINT_CUSTOMERS/PRINT_CUSTOMER/address_2"/></fo:block>
			<fo:block font-size="10pt"><xsl:value-of select="/root/ORDER/PRINT_CUSTOMERS/PRINT_CUSTOMER/city"/>,<xsl:text>&#160;</xsl:text><xsl:value-of select="/root/ORDER/PRINT_CUSTOMERS/PRINT_CUSTOMER/state"/><xsl:text>&#160;</xsl:text><xsl:value-of select="/root/ORDER/PRINT_CUSTOMERS/PRINT_CUSTOMER/zip_code"/></fo:block>
			
			
			<xsl:for-each select="/root/ORDER">
			<xsl:sort select="PRINT_ORDERS/PRINT_ORDER/external_order_number"/>
			<fo:block space-before.optimum='15pt' space-after.optimum='15pt' background-color='grey' padding='.15pt'></fo:block>
			<fo:table table-layout="fixed">
				<fo:table-column column-width="6cm"/>
				<fo:table-column column-width="6cm"/>
				<fo:table-column column-width="7cm"/>
				<fo:table-body>
				  <fo:table-row>
				    <fo:table-cell>
				      <fo:block font-size="10pt" text-align="left" vertical-align="middle">
				      Order Number: <xsl:value-of select="PRINT_ORDERS/PRINT_ORDER/external_order_number"/>
				      </fo:block>
				    </fo:table-cell>
				    <fo:table-cell>
				      <fo:block font-size="10pt" text-align="left" vertical-align="middle">
				      Order Date: <xsl:value-of select="PRINT_ORDERS/PRINT_ORDER/order_date"/>
				      </fo:block>
				    </fo:table-cell>
				    <fo:table-cell>
				      <fo:block font-size="10pt" text-align="left" vertical-align="middle">
				      Delivery Date: <xsl:value-of select="PRINT_ORDERS/PRINT_ORDER/delivery_date"/><xsl:if test="PRINT_ORDERS/PRINT_ORDER/delivery_date_range_end != '' and PRINT_ORDERS/PRINT_ORDER/delivery_date != PRINT_ORDERS/PRINT_ORDER/delivery_date_range_end"><xsl:text>&#160;</xsl:text>-<xsl:text>&#160;</xsl:text><xsl:value-of select="PRINT_ORDERS/PRINT_ORDER/delivery_date_range_end"/></xsl:if>
				      </fo:block>
				    </fo:table-cell>
				  </fo:table-row>
				  <fo:table-row>
				    <fo:table-cell>
				      <fo:block padding='8pt' text-align="left" vertical-align="middle"></fo:block>
				    </fo:table-cell>
				  </fo:table-row>
				  <fo:table-row>
				    <fo:table-cell>
				      <fo:block font-size="10pt" text-align="left" vertical-align="middle">
				      Recipient Information:
				      </fo:block>
				    </fo:table-cell>
				    <fo:table-cell>
				      <fo:block font-size="10pt" text-align="left" vertical-align="middle">
				      	<fo:block><xsl:value-of select="PRINT_ORDERS/PRINT_ORDER/first_name"/><xsl:text>&#160;</xsl:text><xsl:value-of select="PRINT_ORDERS/PRINT_ORDER/last_name"/></fo:block>
						<xsl:if test="PRINT_ORDERS/PRINT_ORDER/business_name != '' and PRINT_ORDERS/PRINT_ORDER/business_name != 'n/a'">
							<fo:block><xsl:value-of select="PRINT_ORDERS/PRINT_ORDER/business_name"/></fo:block>
						</xsl:if>
						<fo:block><xsl:value-of select="PRINT_ORDERS/PRINT_ORDER/address_1"/></fo:block>
						<fo:block><xsl:value-of select="PRINT_ORDERS/PRINT_ORDER/address_2"/></fo:block>
						<xsl:choose>
							<xsl:when test="PRINT_ORDERS/PRINT_ORDER/international_flag = 'D'">
								<fo:block space-after.optimum='30pt'><xsl:value-of select="PRINT_ORDERS/PRINT_ORDER/city"/>,<xsl:text>&#160;</xsl:text><xsl:value-of select="PRINT_ORDERS/PRINT_ORDER/state"/><xsl:text>&#160;</xsl:text><xsl:value-of select="PRINT_ORDERS/PRINT_ORDER/zip_code"/>
								<xsl:if test="PRINT_ORDERS/PRINT_ORDER/country != 'US'">
									<xsl:text>&#160;</xsl:text><xsl:value-of select="PRINT_ORDERS/PRINT_ORDER/country_name"/>
								</xsl:if>
								</fo:block>
							</xsl:when>
							<xsl:otherwise>
								<fo:block space-after.optimum='30pt'><xsl:value-of select="PRINT_ORDERS/PRINT_ORDER/city"/>, <xsl:text>&#160;</xsl:text><xsl:value-of select="PRINT_ORDERS/PRINT_ORDER/country_name"/></fo:block>
							</xsl:otherwise>
						</xsl:choose>
				      </fo:block>
				    </fo:table-cell>
				    <fo:table-cell>
				      <fo:block font-size="10pt" text-align="left" vertical-align="middle"></fo:block>
				    </fo:table-cell>
				  </fo:table-row>
				</fo:table-body>
			</fo:table>
			
			<fo:table table-layout="fixed">
				<fo:table-column column-width="13cm"/>
				<fo:table-column column-width="1.5cm"/>
				<fo:table-body>
				  <fo:table-row>
				    <fo:table-cell>
				      <fo:block font-size="10pt" text-align="left" vertical-align="middle">
				      	<fo:block><xsl:value-of select="PRINT_ORDERS/PRINT_ORDER/product_name"/><xsl:text>&#160;</xsl:text>-<xsl:text>&#160;</xsl:text><xsl:value-of select="PRINT_ORDERS/PRINT_ORDER/product_id"/></fo:block>
				      </fo:block>
				    </fo:table-cell>
				    <fo:table-cell>
				      <fo:block font-size="10pt" text-align="left" vertical-align="middle">
						<fo:block>$<xsl:value-of select="format-number(PRINT_ORDER_BILLS/PRINT_ORDER_BILL/product_amount, '#,###,##0.00')"/></fo:block>
				      </fo:block>
				    </fo:table-cell>
				  </fo:table-row>
                                  <xsl:for-each select="PRINT_ORDER_ADDONS/PRINT_ORDER_ADDON">
				  <xsl:sort select="add_on_type"/>
				  <fo:table-row>
				    <fo:table-cell>
				      <fo:block font-size="10pt" text-align="left" vertical-align="middle">
                                        <xsl:if test="addon_type = '7'">
                                            <fo:block><xsl:value-of select="description"/>
                                                <xsl:text>&#160;</xsl:text>Quantity<xsl:text>&#160;</xsl:text>-<xsl:text>&#160;</xsl:text><xsl:value-of select="add_on_quantity"/>
                                            </fo:block>
                                         </xsl:if>
				      </fo:block>
				    </fo:table-cell>
				    <fo:table-cell>
				      <fo:block font-size="10pt" text-align="left" vertical-align="middle">
                                        <xsl:if test="addon_type = '7'">
                                            <fo:block>
                                            	<xsl:choose>
									           		<xsl:when test="(string-length(add_on_sale_price) != 0 and add_on_sale_price != '' and (add_on_sale_price &gt; 0)) or (string-length(add_on_discount_amount) != 0 and add_on_discount_amount != '' and (add_on_discount_amount &gt; 0))">
									           			<xsl:choose>
									           				<xsl:when test="string-length(add_on_discount_amount) != 0 and add_on_discount_amount != '' and (add_on_discount_amount &gt; 0)">
									           					$<xsl:value-of select="format-number((add_on_sale_price * add_on_quantity) + (add_on_discount_amount * add_on_quantity), '#,###,##0.00;(#,###,##0.00)', 'notANumber')"/>
									           				</xsl:when>
									           				<xsl:otherwise>
									           					$<xsl:value-of select="format-number((add_on_sale_price * add_on_quantity), '#,###,##0.00;(#,###,##0.00)', 'notANumber')"/>
									           				</xsl:otherwise>
									           			</xsl:choose>
													</xsl:when>
													<xsl:otherwise>
														$<xsl:value-of select="format-number((price * add_on_quantity), '#,###,##0.00;(#,###,##0.00)','notANumber')"/>
													</xsl:otherwise>
											  	</xsl:choose>
                                            </fo:block>
                                        </xsl:if>    
				      </fo:block>
				    </fo:table-cell>
				  </fo:table-row>
                                 
				  </xsl:for-each>
				  <xsl:for-each select="PRINT_ORDER_ADDONS/PRINT_ORDER_ADDON">
				  <xsl:sort select="add_on_type"/>
				  <fo:table-row>
				    <fo:table-cell>
				      <fo:block font-size="10pt" text-align="left" vertical-align="middle">
				      	<xsl:if test="addon_type != '7'">
                                            <fo:block><xsl:value-of select="description"/>
                                                <xsl:choose>
                                                    <xsl:when test="addon_type != '4'">
                                                            <xsl:text>&#160;</xsl:text>Quantity<xsl:text>&#160;</xsl:text>-<xsl:text>&#160;</xsl:text><xsl:value-of select="add_on_quantity"/>
                                                    </xsl:when>
                                                    <xsl:otherwise>
                                                            &#160;Greeting Card
                                                    </xsl:otherwise>
                                                </xsl:choose>
                                            </fo:block>
                                        </xsl:if>
				      </fo:block>
				    </fo:table-cell>
				    <fo:table-cell>
				      <fo:block font-size="10pt" text-align="left" vertical-align="middle">
                                        <xsl:if test="addon_type != '7'">
                                            <fo:block>
                                            	<xsl:choose>
									           		<xsl:when test="(string-length(add_on_sale_price) != 0 and add_on_sale_price != '' and (add_on_sale_price &gt; 0)) or (string-length(add_on_discount_amount) != 0 and add_on_discount_amount != '' and (add_on_discount_amount &gt; 0))">
									           			<xsl:choose>
									           				<xsl:when test="string-length(add_on_discount_amount) != 0 and add_on_discount_amount != '' and (add_on_discount_amount &gt; 0)">
									           					$<xsl:value-of select="format-number((add_on_sale_price * add_on_quantity) + (add_on_discount_amount * add_on_quantity), '#,###,##0.00;(#,###,##0.00)', 'notANumber')"/>
									           				</xsl:when>
									           				<xsl:otherwise>
									           					$<xsl:value-of select="format-number((add_on_sale_price * add_on_quantity), '#,###,##0.00;(#,###,##0.00)', 'notANumber')"/>
									           				</xsl:otherwise>
									           			</xsl:choose>
													</xsl:when>
													<xsl:otherwise>
														$<xsl:value-of select="format-number((price * add_on_quantity), '#,###,##0.00;(#,###,##0.00)','notANumber')"/>
													</xsl:otherwise>
											  	</xsl:choose>
                                            </fo:block>
                                        </xsl:if>
				      </fo:block>
				    </fo:table-cell>
				  </fo:table-row>
				  </xsl:for-each>
          <xsl:if test="PRINT_ORDER_BILLS/PRINT_ORDER_BILL/discount_amount &gt; 0 or (string-length(PRINT_ORDER_BILLS/PRINT_ORDER_BILL/add_on_discount_amount) != 0 and PRINT_ORDER_BILLS/PRINT_ORDER_BILL/add_on_discount_amount != '' and PRINT_ORDER_BILLS/PRINT_ORDER_BILL/add_on_discount_amount &gt; 0)">
            <fo:table-row>
              <fo:table-cell>
                <fo:block font-size="10pt" text-align="left" vertical-align="middle">
                  <fo:block>Discount</fo:block>
                </fo:block>
              </fo:table-cell>
              <fo:table-cell>
                <fo:block font-size="10pt" text-align="left" vertical-align="middle">
                  <fo:block>
                  	<xsl:choose>
		           		<xsl:when test="string-length(PRINT_ORDER_BILLS/PRINT_ORDER_BILL/add_on_discount_amount) != 0 and PRINT_ORDER_BILLS/PRINT_ORDER_BILL/add_on_discount_amount != ''">
							$(<xsl:value-of select="format-number((PRINT_ORDER_BILLS/PRINT_ORDER_BILL/discount_amount) + (PRINT_ORDER_BILLS/PRINT_ORDER_BILL/add_on_discount_amount), '#,###,##0.00;(#,###,##0.00)', 'notANumber')"/>)
						</xsl:when>
						<xsl:otherwise>
							$(<xsl:value-of select="format-number(PRINT_ORDER_BILLS/PRINT_ORDER_BILL/discount_amount, '#,###,##0.00;(#,###,##0.00)','notANumber')"/>)
						</xsl:otherwise>
				  	</xsl:choose>
                  </fo:block>
                </fo:block>
              </fo:table-cell>
            </fo:table-row>
          </xsl:if>
				  <xsl:choose>
					<xsl:when test="PRINT_ORDERS/PRINT_ORDER/vendor_flag = 'Y' and (PRINT_ORDERS/PRINT_ORDER/state = 'HI' or PRINT_ORDERS/PRINT_ORDER/state = 'AK')">
					  <fo:table-row>
					    <fo:table-cell>
					      <fo:block font-size="10pt" text-align="left" vertical-align="middle">
					      	<xsl:choose>
								<xsl:when test="PRINT_ORDERS/PRINT_ORDER/vendor_flag = 'Y'">
									<fo:block><xsl:value-of select="PRINT_ORDERS/PRINT_ORDER/ship_method_desc"/></fo:block>								
								</xsl:when>
								<xsl:otherwise>
									<fo:block>Service Fee</fo:block>
								</xsl:otherwise>
							</xsl:choose>
					      </fo:block>
					    </fo:table-cell>
					    <fo:table-cell>
					      <fo:block font-size="10pt" text-align="left" vertical-align="middle">
							<fo:block>$<xsl:value-of select="format-number(PRINT_ORDER_BILLS/PRINT_ORDER_BILL/serv_ship_fee - key('letterDetail','special_service_charge')/value, '#,###,##0.00;(#,###,##0.00)','notANumber')"/></fo:block>
					      </fo:block>
					    </fo:table-cell>
					  </fo:table-row>
					  <fo:table-row>
					    <fo:table-cell>
					      <fo:block font-size="10pt" text-align="left" vertical-align="middle">
					      		<fo:block font-size="10pt">Special Service Charge</fo:block>
					      </fo:block>
					    </fo:table-cell>
					    <fo:table-cell>
					      <fo:block font-size="10pt" text-align="left" vertical-align="middle">
							<fo:block>$<xsl:value-of select="format-number(key('letterDetail','special_service_charge')/value, '#,###,##0.00;(#,###,##0.00)','notANumber')"/></fo:block>
					      </fo:block>
					    </fo:table-cell>
					  </fo:table-row>
				  </xsl:when>
				  <xsl:otherwise>
				  	  <fo:table-row>
					    <fo:table-cell>
					      <fo:block font-size="10pt" text-align="left" vertical-align="middle">
					      	<xsl:choose>
								<xsl:when test="PRINT_ORDERS/PRINT_ORDER/vendor_flag = 'Y'">
									<fo:block><xsl:value-of select="PRINT_ORDERS/PRINT_ORDER/ship_method_desc"/></fo:block>								
								</xsl:when>
								<xsl:otherwise>
									<fo:block>Service Fee</fo:block>
								</xsl:otherwise>
							</xsl:choose>
					      </fo:block>
					    </fo:table-cell>
					    <fo:table-cell>
					      <fo:block font-size="10pt" text-align="left" vertical-align="middle">
							<xsl:choose>
								<xsl:when test="PRINT_ORDERS/PRINT_ORDER/vendor_flag = 'Y' and PRINT_ORDER_BILLS/PRINT_ORDER_BILL/morning_delivery_fee != ''">
									<fo:block>$<xsl:value-of select="format-number(PRINT_ORDER_BILLS/PRINT_ORDER_BILL/serv_ship_fee - PRINT_ORDER_BILLS/PRINT_ORDER_BILL/morning_delivery_fee, '#,###,##0.00;(#,###,##0.00)','notANumber')"/></fo:block>
					        	</xsl:when>
								<xsl:otherwise>
									<fo:block>$<xsl:value-of select="format-number(PRINT_ORDER_BILLS/PRINT_ORDER_BILL/serv_ship_fee, '#,###,##0.00;(#,###,##0.00)','notANumber')"/></fo:block>
								</xsl:otherwise>
							</xsl:choose>
					      </fo:block>
					    </fo:table-cell>
					  </fo:table-row>
					  
				  
				  </xsl:otherwise>
				  </xsl:choose>
				  
				  <xsl:if test="PRINT_ORDERS/PRINT_ORDER/vendor_flag = 'Y' and PRINT_ORDER_BILLS/PRINT_ORDER_BILL/morning_delivery_fee != ''">
					  <fo:table-row>
					    <fo:table-cell>
					      <fo:block font-size="10pt" text-align="left" vertical-align="middle">
					      		<fo:block font-size="10pt"><xsl:value-of select="key('letterDetail','morningDeliveryLabel')/value"/></fo:block>
					      </fo:block>
					    </fo:table-cell>
					    <fo:table-cell>
					      <fo:block font-size="10pt" text-align="left" vertical-align="middle">
							<fo:block>$<xsl:value-of select="format-number(PRINT_ORDER_BILLS/PRINT_ORDER_BILL/morning_delivery_fee, '#,###,##0.00;(#,###,##0.00)','notANumber')"/></fo:block>
					      </fo:block>
					    </fo:table-cell>
					  </fo:table-row>
				  </xsl:if>
				  <xsl:for-each select="PRINT_ORDER_TAXES/PRINT_ORDER_TAX">
				  	<xsl:if test="display_order!='0'"> 
				  	  <fo:table-row>
					    <fo:table-cell>
					      <fo:block font-size="10pt" text-align="left" vertical-align="middle">
					      	<fo:block><xsl:value-of select="tax_description"/></fo:block>
					      </fo:block>
					    </fo:table-cell>
					    <fo:table-cell>
					      <fo:block font-size="10pt" text-align="left" vertical-align="middle">
							<fo:block>$<xsl:value-of select="format-number(tax_amount, '#,###,##0.00;(#,###,##0.00)','notANumber')"/></fo:block>
					      </fo:block>
					    </fo:table-cell>
					  </fo:table-row>
					  </xsl:if>				  
				  </xsl:for-each>
				  
				  <fo:table-row>
				    <fo:table-cell>
				    </fo:table-cell>
				    <fo:table-cell>
				    <fo:block space-after.optimum='2pt' background-color='grey' padding='.05pt'></fo:block>
				    </fo:table-cell>
				  </fo:table-row>
				  <fo:table-row>
				    <fo:table-cell>
				      <fo:block font-size="10pt" text-align="left" vertical-align="middle">
				      	<fo:block>Subtotal</fo:block>
				      </fo:block>
				    </fo:table-cell>
				    <fo:table-cell>
				      <fo:block font-size="10pt" text-align="left" vertical-align="middle">
						<fo:block>$<xsl:value-of select="format-number(PRINT_ORDER_BILLS/PRINT_ORDER_BILL/bill_total, '#,###,##0.00;(#,###,##0.00)','notANumber')"/></fo:block>
				      </fo:block>
				    </fo:table-cell>
				  </fo:table-row>
				</fo:table-body>
			</fo:table>
			</xsl:for-each>

			<fo:block space-before.optimum='15pt' space-after.optimum='15pt' background-color='grey' padding='.15pt'></fo:block>
					
			<fo:table table-layout="fixed">
				<fo:table-column column-width="13cm"/>
				<fo:table-column column-width="1.5cm"/>
				<fo:table-body>
				<xsl:for-each select="ORDER[1]/OUT_ROLLED_PAYMENTS/OUT_ROLLED_PAYMENT">
				  <fo:table-row>
				    <fo:table-cell>
				      <fo:block font-size="10pt" text-align="left" vertical-align="middle">
				      	<fo:block>		
				      	<xsl:choose>
							<xsl:when test="payment_type = 'GC'">
								<xsl:value-of select="payment_type_description"/><xsl:text>&#160;</xsl:text><xsl:value-of select="card_number"/>
							</xsl:when>
							<xsl:when test="payment_method = 'P'">
								Total Charge: <xsl:value-of select="payment_type_description"/>
							</xsl:when>
							<xsl:otherwise>
								Total Charge: <xsl:value-of select="payment_type_description"/><xsl:text>&#160;</xsl:text><xsl:text>**************</xsl:text><xsl:value-of select="card_number"/>
							</xsl:otherwise>
						</xsl:choose>
				      	</fo:block>
				      </fo:block>
				    </fo:table-cell>
				    <fo:table-cell>
				      <fo:block font-size="10pt" text-align="left" vertical-align="middle">
						<fo:block>
						<xsl:choose>
							<xsl:when test="payment_type = 'GC'">
								($<xsl:value-of select="format-number(payment_amount, '#,###,##0.00;(#,###,##0.00)','notANumber')"/>)
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>$<xsl:value-of select="format-number(payment_amount, '#,###,##0.00;(#,###,##0.00)','notANumber')"/>
							</xsl:otherwise>
						</xsl:choose>
						</fo:block>
				      </fo:block>
				    </fo:table-cell>
				  </fo:table-row>
				</xsl:for-each>
				</fo:table-body>
			</fo:table>
			
			<fo:block font-size="10pt" font-weight="bold" space-before.optimum='15pt'>
			All Prices are in US dollars
			</fo:block>
			
			
			<xsl:if test="key('letterDetail','surchargeExplanation')/value != ''">
				<fo:block font-size="10pt" space-before.optimum='15pt'>
				<xsl:value-of select="key('letterDetail','surchargeExplanation')/value"
						 		 disable-output-escaping="yes"/>				  
				</fo:block>
			</xsl:if>
			
			<fo:block font-size="10pt" space-before.optimum='20pt'>
			Thank you for your order,
			</fo:block>
			
			<fo:block font-size="10pt" space-before.optimum='2pt'>
			<xsl:value-of select="key('letterDetail','company_name')/value"/>
			</fo:block>
			
		    
		    </fo:flow>
		  </fo:page-sequence>
		</fo:root>
		
	</xsl:template>
	
</xsl:stylesheet>