<!DOCTYPE ACDemo[
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0"  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:param name="securitytoken"><xsl:value-of select="key('security','securitytoken')/value" /></xsl:param>
  <xsl:param name="context"><xsl:value-of select="key('security','context')/value" /></xsl:param>
  <xsl:param name="adminAction"><xsl:value-of select="key('security','adminAction')/value" /></xsl:param>
  <xsl:param name="applicationcontext"><xsl:value-of select="key('security','applicationcontext')/value" /></xsl:param>
  <xsl:param name="sitename"><xsl:value-of select="key('security','sitename')/value" /></xsl:param>
  <xsl:param name="sitenamessl"><xsl:value-of select="key('security','sitenamessl')/value" /></xsl:param>
  <xsl:param name="isclickthroughsecure"><xsl:value-of select="key('security','isclickthroughsecure')/value" /></xsl:param>
  <xsl:param name="sc_cust_ind"><xsl:value-of select="key('security','sc_cust_ind')/value" /></xsl:param>
  <xsl:param name="sc_tracking_number"><xsl:value-of select="key('security','sc_tracking_number')/value" /></xsl:param>
  <xsl:param name="sc_cust_number"><xsl:value-of select="key('security','sc_cust_number')/value" /></xsl:param>
  <xsl:param name="sc_phone_number"><xsl:value-of select="key('security','sc_phone_number')/value" /></xsl:param>
  <xsl:param name="sc_rewards_number"><xsl:value-of select="key('security','sc_rewards_number')/value" /></xsl:param>
  <xsl:param name="sc_pc_membership"><xsl:value-of select="key('security','sc_pc_membership')/value" /></xsl:param>
  <xsl:param name="sc_recip_ind"><xsl:value-of select="key('security','sc_recip_ind')/value" /></xsl:param>
  <xsl:param name="sc_order_number"><xsl:value-of select="key('security','sc_order_number')/value" /></xsl:param>
  <xsl:param name="sc_pro_order_number"><xsl:value-of select="key('security','sc_pro_order_number')/value" /></xsl:param>
  <xsl:param name="sc_formatted_ord_number"><xsl:value-of select="key('security','sc_formatted_ord_number')/value" /></xsl:param>
  <xsl:param name="sc_email_address"><xsl:value-of select="key('security','sc_email_address')/value" /></xsl:param>
  <xsl:param name="sc_zip_code"><xsl:value-of select="key('security','sc_zip_code')/value" /></xsl:param>
  <xsl:param name="sc_last_name"><xsl:value-of select="key('security','sc_last_name')/value" /></xsl:param>
  <xsl:param name="call_cs_number"><xsl:value-of select="key('security','call_cs_number')/value" /></xsl:param>
  <xsl:param name="call_type_flag"><xsl:value-of select="key('security','call_type_flag')/value" /></xsl:param>
  <xsl:param name="call_brand_name"><xsl:value-of select="key('security','call_brand_name')/value" /></xsl:param>
  <xsl:param name="call_dnis"><xsl:value-of select="key('security','call_dnis')/value" /></xsl:param>
  <xsl:param name="t_call_log_id"><xsl:value-of select="key('security','t_call_log_id')/value" /></xsl:param>
  <xsl:param name="t_entity_history_id"><xsl:value-of select="key('security','t_entity_history_id')/value" /></xsl:param>
  <xsl:param name="t_comment_origin_type"><xsl:value-of select="key('security','t_comment_origin_type')/value" /></xsl:param>
  <xsl:param name="start_origin"><xsl:value-of select="key('security','start_origin')/value" /></xsl:param>
  <xsl:param name="uo_display_page"><xsl:value-of select="key('security','uo_display_page')/value" /></xsl:param>
  <xsl:param name="florist_cant_fulfill"><xsl:value-of select="key('security','florist_cant_fulfill')/value" /></xsl:param>
  <xsl:param name="rtq"><xsl:value-of select="key('security','rtq')/value" /></xsl:param>
  <xsl:param name="rtq_queue_type"><xsl:value-of select="key('security','rtq_queue_type')/value" /></xsl:param>
  <xsl:param name="rtq_queue_ind"><xsl:value-of select="key('security','rtq_queue_ind')/value" /></xsl:param>
  <xsl:param name="rtq_attached"><xsl:value-of select="key('security','rtq_attached')/value" /></xsl:param>
  <xsl:param name="rtq_tagged"><xsl:value-of select="key('security','rtq_tagged')/value" /></xsl:param>
  <xsl:param name="rtq_user"><xsl:value-of select="key('security','rtq_user')/value" /></xsl:param>
  <xsl:param name="rtq_group"><xsl:value-of select="key('security','rtq_group')/value" /></xsl:param>
  <xsl:param name="rtq_max_records"><xsl:value-of select="key('security','rtq_max_records')/value" /></xsl:param>
  <xsl:param name="rtq_position"><xsl:value-of select="key('security','rtq_position')/value" /></xsl:param>
  <xsl:param name="rtq_current_sort_direction"><xsl:value-of select="key('security','rtq_current_sort_direction')/value" /></xsl:param>
  <xsl:param name="rtq_sort_on_return"><xsl:value-of select="key('security','rtq_sort_on_return')/value" /></xsl:param>
  <xsl:param name="rtq_sort_column"><xsl:value-of select="key('security','rtq_sort_column')/value" /></xsl:param>
  <xsl:param name="rtq_received_date"><xsl:value-of select="key('security','rtq_received_date')/value" /></xsl:param>
  <xsl:param name="rtq_delivery_date"><xsl:value-of select="key('security','rtq_delivery_date')/value" /></xsl:param>
  <xsl:param name="rtq_tag_date"><xsl:value-of select="key('security','rtq_tag_date')/value" /></xsl:param>
  <xsl:param name="rtq_last_touched"><xsl:value-of select="key('security','rtq_last_touched')/value" /></xsl:param>
  <xsl:param name="rtq_type"><xsl:value-of select="key('security','rtq_type')/value" /></xsl:param>
  <xsl:param name="rtq_sys"><xsl:value-of select="key('security','rtq_sys')/value" /></xsl:param>
  <xsl:param name="rtq_timezone"><xsl:value-of select="key('security','rtq_timezone')/value" /></xsl:param>
  <xsl:param name="rtq_sdg"><xsl:value-of select="key('security','rtq_sdg')/value" /></xsl:param>
  <xsl:param name="rtq_disposition"><xsl:value-of select="key('security','rtq_disposition')/value" /></xsl:param>
  <xsl:param name="rtq_priority"><xsl:value-of select="key('security','rtq_priority')/value" /></xsl:param>
  <xsl:param name="rtq_new_item"><xsl:value-of select="key('security','rtq_new_item')/value" /></xsl:param>
  <xsl:param name="rtq_occasion"><xsl:value-of select="key('security','rtq_occasion')/value" /></xsl:param>
  <xsl:param name="rtq_location"><xsl:value-of select="key('security','rtq_location')/value" /></xsl:param>
  <xsl:param name="rtq_member_num"><xsl:value-of select="key('security','rtq_member_num')/value" /></xsl:param>
  <xsl:param name="rtq_zip"><xsl:value-of select="key('security','rtq_zip')/value" /></xsl:param>
  <xsl:param name="rtq_tag_by"><xsl:value-of select="key('security','rtq_tag_by')/value" /></xsl:param>
  <xsl:param name="rtq_filter"><xsl:value-of select="key('security','rtq_filter')/value" /></xsl:param>
  <xsl:param name="bypass_com"><xsl:value-of select="key('security','bypass_com')/value" /></xsl:param>
  <xsl:param name="loss_prevention_permission"><xsl:value-of select="key('security','loss_prevention_permission')/value" /></xsl:param>
  <xsl:param name="bypass_privacy_policy_verification"><xsl:value-of select="key('security','bypass_privacy_policy_verification')/value" /></xsl:param>

  <xsl:param name="modify_order_updated"><xsl:value-of select="key('security','modify_order_updated')/value" /></xsl:param>

  <xsl:param name="prev_page"><xsl:value-of select="key('security','prev_page')/value" /></xsl:param>
  <xsl:param name="prev_page_position"><xsl:value-of select="key('security','prev_page_position')/value" /></xsl:param>
  
  <xsl:param name="prev_sort_column"><xsl:value-of select="key('security','prev_sort_column')/value" /></xsl:param>
  <xsl:param name="prev_sort_status"><xsl:value-of select="key('security','prev_sort_status')/value" /></xsl:param>
  <xsl:param name="prev_current_sort_status"><xsl:value-of select="key('security','prev_current_sort_status')/value" /></xsl:param>
  <xsl:param name="prev_is_from_header"><xsl:value-of select="key('security','prev_is_from_header')/value" /></xsl:param>
  <xsl:param name="prev_userId_filter"><xsl:value-of select="key('security','prev_userId_filter')/value" /></xsl:param>
  <xsl:param name="prev_groupId_filter"><xsl:value-of select="key('security','prev_groupId_filter')/value" /></xsl:param>
  <xsl:param name="prev_codes_filter"><xsl:value-of select="key('security','prev_codes_filter')/value" /></xsl:param>

  <xsl:param name="cdisp_autoOpenedFlag"><xsl:value-of select="key('security','cdisp_autoOpenedFlag')/value" /></xsl:param>
  <xsl:param name="cdisp_reminderFlag"><xsl:value-of select="key('security','cdisp_reminderFlag')/value" /></xsl:param>
  <xsl:param name="cdisp_cancelledOrder"><xsl:value-of select="key('security','cdisp_cancelledOrder')/value" /></xsl:param>
  <xsl:param name="cdisp_enabledFlag"><xsl:value-of select="key('security','cdisp_enabledFlag')/value" /></xsl:param>
  <xsl:param name="cdisp_resetFlag"><xsl:value-of select="key('security','cdisp_resetFlag')/value" /></xsl:param>
  
  <xsl:output method="html" indent="yes"/>
  <xsl:template name="securityanddata">
    <!--security-->
    <input type="hidden" name="securitytoken"           id="securitytoken"          value="{$securitytoken}"/>
    <input type="hidden" name="context"                 id="context"                value="{$context}"/>
    <input type="hidden" name="applicationcontext"      id="applicationcontext"     value="{$applicationcontext}"/>
    <input type="hidden" name="adminAction"             id="adminAction"            value="{$adminAction}"/>

    <!-- searchCriteria -->
    <input type="hidden" name="sc_cust_ind"             id="sc_cust_ind"            value="{$sc_cust_ind}" />
    <input type="hidden" name="sc_tracking_number"      id="sc_tracking_number"     value="{$sc_tracking_number}" />
    <input type="hidden" name="sc_cust_number"          id="sc_cust_number"         value="{$sc_cust_number}" />
    <input type="hidden" name="sc_phone_number"         id="sc_phone_number"        value="{$sc_phone_number}" />
    <input type="hidden" name="sc_rewards_number"       id="sc_rewards_number"      value="{$sc_rewards_number}" />
    <input type="hidden" name="sc_pc_membership"        id="sc_pc_membership"       value="{$sc_pc_membership}" />
    <input type="hidden" name="sc_recip_ind"            id="sc_recip_ind"           value="{$sc_recip_ind}" />
    <input type="hidden" name="sc_order_number"         id="sc_order_number"        value="{$sc_order_number}" />
    <input type="hidden" name="sc_email_address"        id="sc_email_address"       value="{$sc_email_address}" />
    <input type="hidden" name="sc_zip_code"             id="sc_zip_code"            value="{$sc_zip_code}" />
    <input type="hidden" name="sc_last_name"            id="sc_last_name"           value="{$sc_last_name}" />
    <!-- header.xsl -->
    <input type="hidden" name="call_cs_number"          id="call_cs_number"         value="{$call_cs_number}"/>
    <input type="hidden" name="call_type_flag"          id="call_type_flag"         value="{$call_type_flag}"/>
    <input type="hidden" name="call_brand_name"         id="call_brand_name"        value="{$call_brand_name}"/>
    <input type="hidden" name="call_dnis"               id="call_dnis"              value="{$call_dnis}"/>

    <!-- call log-->
    <input type="hidden" name="t_call_log_id"           id="t_call_log_id"          value="{$t_call_log_id}"/>

    <!-- timer -->
    <input type="hidden" name="t_entity_history_id"     id="t_entity_history_id"    value="{$t_entity_history_id}"/>
    <input type="hidden" name="t_comment_origin_type"   id="t_comment_origin_type"  value="{$t_comment_origin_type}"/>

    <!-- start origin -->
    <input type="hidden" name="start_origin"     id="start_origin"    value="{$start_origin}"/>

    <!-- Update Order info -->
    <input type="hidden" name="uo_display_page"      id="uo_display_page"       value="{$uo_display_page}"/>
    <input type="hidden" name="florist_cant_fulfill" id="florist_cant_fulfill" value="{$florist_cant_fulfill}"/>
    <input type="hidden" name="modify_order_updated" id="modify_order_updated" value="{$modify_order_updated}"/>

    <!-- Queues Back Button -->
    <input type="hidden" name="rtq" id="rtq" value="{$rtq}"/>
    <input type="hidden" name="rtq_queue_type" id="rtq_queue_type" value="{$rtq_queue_type}"/>
    <input type="hidden" name="rtq_queue_ind" id="rtq_queue_ind" value="{$rtq_queue_ind}"/>
    <input type="hidden" name="rtq_attached" id="rtq_attached" value="{$rtq_attached}"/>
    <input type="hidden" name="rtq_tagged" id="rtq_tagged" value="{$rtq_tagged}"/>
    <input type="hidden" name="rtq_user" id="rtq_user" value="{$rtq_user}"/>
    <input type="hidden" name="rtq_group" id="rtq_group" value="{$rtq_group}"/>
    <input type="hidden" name="rtq_max_records" id="rtq_max_records" value="{$rtq_max_records}"/>
    <input type="hidden" name="rtq_position" id="rtq_position" value="{$rtq_position}"/>
    <input type="hidden" name="rtq_current_sort_direction" id="rtq_current_sort_direction" value="{$rtq_current_sort_direction}"/>
    <input type="hidden" name="rtq_sort_on_return" id="rtq_sort_on_return" value="{$rtq_sort_on_return}"/>
    <input type="hidden" name="rtq_sort_column" id="rtq_sort_column" value="{$rtq_sort_column}"/>
    
    <input type="hidden" name="rtq_received_date" id="rtq_received_date" value="{$rtq_received_date}"/>
    <input type="hidden" name="rtq_delivery_date" id="rtq_delivery_date" value="{$rtq_delivery_date}"/>
    <input type="hidden" name="rtq_tag_date" id="rtq_tag_date" value="{$rtq_tag_date}"/>
    <input type="hidden" name="rtq_last_touched" id="rtq_last_touched" value="{$rtq_last_touched}"/>
    <input type="hidden" name="rtq_type" id="rtq_type" value="{$rtq_type}"/>
    <input type="hidden" name="rtq_sys" id="rtq_sys" value="{$rtq_sys}"/>
    <input type="hidden" name="rtq_timezone" id="rtq_timezone" value="{$rtq_timezone}"/>
    <input type="hidden" name="rtq_sdg" id="rtq_sdg" value="{$rtq_sdg}"/>
    <input type="hidden" name="rtq_disposition" id="rtq_disposition" value="{$rtq_disposition}"/>
    <input type="hidden" name="rtq_priority" id="rtq_priority" value="{$rtq_priority}"/>
    <input type="hidden" name="rtq_new_item" id="rtq_new_item" value="{$rtq_new_item}"/>
    <input type="hidden" name="rtq_occasion" id="rtq_occasion" value="{$rtq_occasion}"/>
    <input type="hidden" name="rtq_location" id="rtq_location" value="{$rtq_location}"/>
    <input type="hidden" name="rtq_member_num" id="rtq_member_num" value="{$rtq_member_num}"/>
    <input type="hidden" name="rtq_zip" id="rtq_zip" value="{$rtq_zip}"/>
    <input type="hidden" name="rtq_tag_by" id="rtq_tag_by" value="{$rtq_tag_by}"/>
    <input type="hidden" name="rtq_filter" id="rtq_filter" value="{$rtq_filter}"/>

    <!-- Refunds Prev Page Data -->
    <input type="hidden" name="prev_page" id="prev_page" value="{$prev_page}"/>
    <input type="hidden" name="prev_page_position" id="prev_page_position" value="{$prev_page_position}"/>
    
    <input type="hidden" name="prev_sort_column" id="prev_sort_column" value="{$prev_sort_column}"/>
    <input type="hidden" name="prev_sort_status" id="prev_sort_status" value="{$prev_sort_status}"/>
    <input type="hidden" name="prev_current_sort_status" id="prev_current_sort_status" value="{$prev_current_sort_status}"/>
    <input type="hidden" name="prev_is_from_header" id="prev_is_from_header" value="{$prev_is_from_header}"/>
    <input type="hidden" name="prev_userId_filter" id="prev_userId_filter" value="{$prev_userId_filter}"/>
    <input type="hidden" name="prev_groupId_filter" id="prev_groupId_filter" value="{$prev_groupId_filter}"/>
    <input type="hidden" name="prev_codes_filter" id="prev_codes_filter" value="{$prev_codes_filter}"/>
    
    <!-- bypass com data -->
    <input type="hidden" name="loss_prevention_permission"  id="loss_prevention_permission" value="{$loss_prevention_permission}" />
    <input type="hidden" name="bypass_com"  id="bypass_com" value="{$bypass_com}" />

    <!-- bypass privacy policy validation -->
    <input type="hidden" name="bypass_privacy_policy_verification"  id="bypass_privacy_policy_verification" value="{$bypass_privacy_policy_verification}" />

    <!-- Call Disposition (decision_result) Data -->
    <input type="hidden" name="cdisp_autoOpenedFlag" id="cdisp_autoOpenedFlag" value="{$cdisp_autoOpenedFlag}"/>
    <input type="hidden" name="cdisp_reminderFlag" id="cdisp_reminderFlag" value="{$cdisp_reminderFlag}"/>
    <input type="hidden" name="cdisp_cancelledOrder" id="cdisp_cancelledOrder" value="{$cdisp_cancelledOrder}"/>
    <input type="hidden" name="cdisp_enabledFlag" id="cdisp_enabledFlag" value="{$cdisp_enabledFlag}"/>
    <input type="hidden" name="cdisp_resetFlag" id="cdisp_resetFlag" value="{$cdisp_resetFlag}"/>
    <input type="hidden" name="sc_pro_order_number"    id="sc_pro_order_number"    value="{$sc_pro_order_number}" />
    <input type="hidden" name="sc_formatted_ord_number"    id="sc_formatted_ord_number"    value="{$sc_formatted_ord_number}" />

  </xsl:template>
</xsl:stylesheet>