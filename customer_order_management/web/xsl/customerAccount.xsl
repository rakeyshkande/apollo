<!DOCTYPE ACDemo[
	<!ENTITY nbsp "&#160;">
	<!ENTITY reg "&#169;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<!-- external templates -->
<!--	<xsl:import href="header.xsl"/>-->
<xsl:import href="header.xsl"/>
	<xsl:import href="footer.xsl"/>
	<xsl:import href="orderShoppingCart.xsl"/>
	<xsl:import href="customerAccountDetail.xsl"/>
	<xsl:import href="securityanddata.xsl"/>
	<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>
	<xsl:key name="security" match="/root/security/data" use="name"/>
		
  <xsl:decimal-format NaN="0.00" name="notANumber"/>
  <xsl:decimal-format NaN="(0.00)"  name="noRefund"/>
  <!--
	 doctype-system and doctype-public must be included in order for the
     orderShoppingCart.xsl to work properly
    -->
	<xsl:output doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"/>
	<xsl:output method="html" omit-xml-declaration="yes"/>
	<xsl:output indent="yes"/>
	<!-- Keys -->
	<xsl:key name="security" match="/root/security/data" use="name"/>
		<xsl:key name="pageData" match="/root/pageData/data" use="name"/>
	<xsl:key name="searchCriteria" match="/root/searchCriteria/criteria" use="name"/>
	<!-- Page/Navigation variables -->
	<xsl:variable name="cai_number_of_orders" select="key('pageData','cai_number_of_orders')/value"/>
	<xsl:variable name="cai_current_page" select="key('pageData','cai_current_page')/value"/>
	<xsl:variable name="cai_total_pages" select="key('pageData','cai_total_pages')/value"/>
	<xsl:variable name="cai_start_position" select="key('pageData','cai_start_position')/value"/>
	<!-- Indicators -->
	<xsl:variable name="isUS" select="boolean(root/CAI_CUSTOMERS/CAI_CUSTOMER/customer_country = 'US')"/>
	<xsl:variable name="SCRUB" select="'S'"/>
	<xsl:variable name="PENDING" select="'P'"/>
	<xsl:variable name="REMOVED" select="'R'"/>
	<xsl:variable name="YES" select="'Y'"/>
	<xsl:variable name="NO" select="'N'"/>
	<xsl:variable name="id"  select="key('pageData','customer_id')/value"/>
	<xsl:variable name="cai" select="root/CAI_CUSTOMERS/CAI_CUSTOMER"/>
	<xsl:variable name="cai_phone" select="root/CAI_PHONES/CAI_PHONE"/>
        
        <xsl:variable name="bypassCOM" select="/root/pageData/data[name='bypass_com']/value"/>
        
	<xsl:template match="/root">
		<html>
			<head>
				<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
				<style type="text/css">
				 /* Scrolling table overrides for the order/shopping cart table */
					div.tableContainer table {
						width: 97%;
					}
					div#messageContainer {
						height: 130px;
						width: 100%;
					}
				</style>
				<script type="text/javascript" src="js/tabIndexMaintenance.js"/>
				<script type="text/javascript" src="js/commonUtil.js"/>
				<script type="text/javascript" src="js/util.js"/>
				<script type="text/javascript" src="js/FormChek.js"/>
				<script type="text/javascript">
				  var cust_id = '<xsl:value-of select="$id"/>';
				  var pageAction = new Array("ca_first","ca_previous","ca_next","ca_last");
                                  var cai_show_previous = "<xsl:value-of select="key('pageData','cai_show_previous')/value"/>";
                                  var cai_show_next     = "<xsl:value-of select="key('pageData','cai_show_next')/value"/>";
                                  var cai_show_first    = "<xsl:value-of select="key('pageData','cai_show_first')/value"/>";
                                  var cai_show_last     = "<xsl:value-of select="key('pageData','cai_show_last')/value"/>";
                  
                  var siteNameSsl = '<xsl:value-of select="$sitenamessl"/>';
                  var applicationContext = '<xsl:value-of select="$applicationcontext"/>';

  <!-- Privacy Policy Verification -->
  var customerValidationRequiredFlag = 'Y'; 

  var customersValidated = new Array( <xsl:for-each select="PPV_VALIDATED_CUSTOMERS/VALIDATED_CUSTOMER">
                                        ["<xsl:value-of select="customer_id"/>"]
                                        <xsl:choose>
                                          <xsl:when test="position()!=last()">,</xsl:when>
                                        </xsl:choose>
                                      </xsl:for-each>
                                    );
  <!-- Privacy Policy -->
  var pdBypassPrivacyPolicyVerification = 'N';
  <xsl:if test="key('pageData', 'bypass_privacy_policy_verification')/value = 'Y'">
    pdBypassPrivacyPolicyVerification = 'Y';
  </xsl:if>


		   <![CDATA[
/***********************************************************************************
* init()
************************************************************************************/
	  function init(){
            setNavigationHandlers();
            setCSCustomerAccountIndexes();

            //For the first invokation of customerOrderSearch, bypass_privacy_policy_verification in the dataFilter maybe empty.  Thus,
            //we have to rely on the bypass_privacy_policy_verification that is being passed back in the pageData. 
            //Value from dataFilter should be ok for subsequent invokations. 
            if (document.getElementById('bypass_privacy_policy_verification') == null ||
                document.getElementById('bypass_privacy_policy_verification').value == '' ||
                document.getElementById('bypass_privacy_policy_verification').value == 'N')
              document.getElementById('bypass_privacy_policy_verification').value = pdBypassPrivacyPolicyVerification;

            <!-- Privacy Policy Verification -->
            if (document.getElementById("t_call_log_id").value != null &&
                document.getElementById("t_call_log_id").value != "")
            {
              //if bypass flag is set to Y, nothing is required.  Else, check to see the current tab
              if (document.getElementById('bypass_privacy_policy_verification').value != 'Y')
              {

                //loop thru customersValidated.  If customer has already been validated, set customerValidationRequiredFlag = V
                for(var i=0;i<customersValidated.length;i++)
                {
                  if (document.getElementById('customer_id').value == customersValidated[i])
                  {
                    customerValidationRequiredFlag = 'V';
                    break;
                  }
                }

                //if either customerValidationRequiredFlag or orderValidationRequiredFlag equals Y, invoke privacy policy popup
                if (customerValidationRequiredFlag == 'Y')
                {
                  //display PPV popup
                  displayPrivacyPolicy(document.getElementById('customer_id').value, customerValidationRequiredFlag, '', 'N'); 

                }
              }
            }

	  }


/*******************************************************************************
*  setCSCustomerAccountIndexes()
*  Sets tabs for Customer Service Customer Account Page.
*  See tab.xsl
*******************************************************************************/
function setCSCustomerAccountIndexes()
{
  //untabAllElements();
	var begIndex = findBeginIndex();
  var externalAnchors = document.getElementsByName('external');
  if((externalAnchors != null && externalAnchors.length > 0)){
			for(var k = 0; k < externalAnchors.length ; k++){
				externalAnchors[k].tabIndex = begIndex++;
			}
	}
	var caElements = new Array('nextItem','nextItem2', 'backItem','backItem2', 
  'firstItem','firstItem2', 'lastItem','lastItem2', 'customer_comments', 
  'update_customer', 'hold','loss_prevention', 'search', 'main_menu',
  'caDirectMail', 'membershipInfoLink', 'email_address','caMoreLink','printer');
  begIndex = setTabIndexes(caElements, begIndex);
  return begIndex;
}


/***********************************************************************************
* getAllParameters()
************************************************************************************/
			function getAllParameters(){
        		var form = document.forms[0];
				var url = '';
				for(var i = 0; i < form.elements.length; i++){
					if(form.elements[i].name.indexOf('sc_') > -1)
					{
						url += "&" + form.elements[i].name +
							"=" + form.elements[i].value;
					}
				}
				return url;
			}
/***********************************************************************************
* doPageAction()
************************************************************************************/
		 	function doPageAction(val){
				var url = "customerOrderSearch.do" + "?action=" +
						pageAction[val];
				performAction(url);
		 	}
		 	/*
		 	Button Scripts
		 	*/

/***********************************************************************************
* performAction()
************************************************************************************/
		 	function performAction(url){
            scroll(0,0);
            showWaitMessage("customerAccountDiv", "wait", "Processing");
	 	        document.forms[0].action = url;
	 		      document.forms[0].submit();
		 	}
      
/***********************************************************************************
* checkKey()
************************************************************************************/
function checkKey()
{
  var tempKey = window.event.keyCode;

  //Up arrow pressed
  if (window.event.altKey && tempKey == 38 && cai_show_first == 'y')
    doPageAction(1);

  //Down arrow pressed
  if (window.event.altKey && tempKey == 40 && cai_show_last == 'y')
    doPageAction(2);

  //Right arrow pressed
  if (window.event.altKey && tempKey == 39 && cai_show_next == 'y')
    doPageAction(3);

  //Left arrow pressed
  if (window.event.altKey && tempKey == 37 && cai_show_previous == 'y')
    doPageAction(0);

} 


/*************************************************************************************
*	Perorms the Back button functionality
**************************************************************************************/
function doBackAction()
{
    if (!cdisp_showWidgetIfRequired()) {   // Display Call Disposition widget if required, otherwise perform action
        var url = "customerOrderSearch.do?action=load_page";
        performAction(url);
    }
}


]]>
       </script>
      </head>
			<body onload="javascript:init();"  onkeydown="javascript:checkKey();">        
                <xsl:call-template name="customerAccountDetail"/>
       
              	<!-- Used to dislay Processing... message to user -->
            <div id="waitDiv" style="display:none">
              <xsl:call-template name="addHeader"/>
               <table id="waitTable" class="mainTable" width="98%" height="325px" align="center" cellpadding="0" cellspacing="2" >
                  <tr>
                    <td width="100%">
                      <table class="innerTable" width="100%" height="325px" cellpadding="0" cellspacing="2">
                       <tr>
                          <td>
                             <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                 <tr>
                                   <td id="waitMessage" align="right" width="50%" class="WaitMessage"></td>
                                   <td id="waitTD"  width="50%" class="WaitMessage"></td>
                                 </tr>
                             </table>
                           </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
              </table>
              <xsl:call-template name="footer"/>
          </div>
			</body>
		</html>
	</xsl:template>
	<xsl:template name="addHeader">
		<xsl:call-template name="header">
			<xsl:with-param name="showPrinter" select="true()"/>
			<xsl:with-param name="showTime" select="true()"/>
			<xsl:with-param name="showSearchBox" select="false()"/>
			<xsl:with-param name="headerName" select="'Customer Account'"/>
			<xsl:with-param name="showBackButton" select="true()"/>
			<xsl:with-param name="dnisNumber" select="$call_dnis"/>
			<xsl:with-param name="cservNumber" select="$call_cs_number"/>
			<xsl:with-param name="indicator" select="$call_type_flag"/>
			<xsl:with-param name="brandName" select="$call_brand_name"/>
			<xsl:with-param name="showCSRIDs" select="true()"/>
			<xsl:with-param name="backButtonLabel" select="'(B)ack to Search'"/>
			<xsl:with-param name="cdispEnabledFlag" select="$cdisp_enabledFlag"/>
		</xsl:call-template>
	</xsl:template>
</xsl:stylesheet>