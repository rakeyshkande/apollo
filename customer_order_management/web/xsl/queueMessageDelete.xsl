<!DOCTYPE ACDemo [
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:import href="header.xsl"/>
  <xsl:import href="footer.xsl"/>
  <xsl:import href="securityanddata.xsl"/>
  <xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>
  <xsl:output method="html" indent="no"/>
  <xsl:output indent="yes"/>
  <xsl:key name="pagedata" match="/root/pageData/data" use="name"/>
  <xsl:key name="security" match="/root/security/data" use="name"/>
  <xsl:variable name="msg_id" select="key('pagedata', 'message_id')/value"/>
  <xsl:variable name="contains_errors" select="boolean( (/root/unableDeleteQueues) != '')"/>
  <xsl:template match="/">

    <html>
      <head>
        <META http-equiv="Content-Type" content="text/html"/>
        <title>Queue Message Delete</title>
        <link rel="stylesheet" type="text/css" href="css/ftd.css"/>
        <link rel="stylesheet" type="text/css" href="css/calendar.css"/>
        <script type="text/javascript" src="js/util.js"></script>
        <script type="text/javascript" src="js/tabIndexMaintenance.js"/>
        <script type="text/javascript" src="js/calendar.js"></script>
        <script type="text/javascript" src="js/clock.js"></script>
        <script type="text/javascript" src="js/commonUtil.js"></script>
        <script type="text/javascript" src="js/mercuryMessage.js"></script>
  		  <script type="text/javascript" src="js/queueDelete.js"/>
  		<script type="text/javascript" >
			var maxAnspAmount = <xsl:value-of select = "key('pagedata','maxAnspAmount')/value"/>
			var containsErrors = <xsl:value-of select="$contains_errors"/>;

		</script>
      </head>
      <body onload="init();">
        <xsl:call-template name="header">
          <xsl:with-param name="headerName" select="'Queue Message Maintenance'"/>
          <xsl:with-param name="showTime" select="true()"/>
          <xsl:with-param name="showBackButton" select="false()"/>
          <xsl:with-param name="showPrinter" select="true()"/>
          <xsl:with-param name="showSearchBox" select="false()"/>
          <xsl:with-param name="dnisNumber" select="$call_dnis"/>
          <xsl:with-param name="cservNumber" select="$call_cs_number"/>
          <xsl:with-param name="indicator" select="$call_type_flag"/>
          <xsl:with-param name="brandName" select="$call_brand_name"/>
          <xsl:with-param name="showCSRIDs" select="true()"/>
          <xsl:with-param name="cdispEnabledFlag" select="$cdisp_enabledFlag"/>
        </xsl:call-template>

        <!-- content to be hidden once the search begins -->
        <div id="content" style="display:block">
          <form name="theForm" method="post" action="" enctype="multipart/form-data">
            <xsl:call-template name="securityanddata"/>
            <xsl:call-template name="decisionResultData"/>
            <input type="hidden" name="action_type"                  id="action_type"                  value="{key('pagedata','action_type')/value}"/>
            <input type="hidden" name="customer_last_name"           id="customer_last_name"           value="{key('pagedata', 'customer_last_name')/value}"/>
            <input type="hidden" name="order_guid"                   id="order_guid"                   value="{key('pagedata','order_guid')/value}"/>
            <input type="hidden" name="master_order_number"          id="master_order_number"          value="{key('pagedata','master_order_number')/value}"/>
            <input type="hidden" name="external_order_number"        id="external_order_number"        value="{key('pagedata', 'external_order_number')/value}"/>
            <input type="hidden" name="order_detail_id"              id="order_detail_id"              value="{key('pagedata', 'order_detail_id')/value}"/>
            <input type="hidden" name="customer_email_address"       id="customer_email_address"       value="{key('pagedata', 'customer_email_address')/value}"/>
            <input type="hidden" name="sender_email_address"         id="sender_email_address"         value="{key('pagedata', 'sender_email_address')/value}"/>
            <input type="hidden" name="message_type"                 id="message_type"                 value="{key('pagedata', 'message_type')/value}"/>
            <input type="hidden" name="message_id"                   id="message_id"                   value="{key('pagedata', 'message_id')/value}"/>
            <input type="hidden" name="message_title"                id="message_title"                value=""/>
            <input type="hidden" name="origin_id"                    id="origin_id"                    value="{key('pagedata','origin_id')/value}"/>
            <input type="hidden" name="change_flag"                  id="change_flag"                  value="N"/>
            <input type="hidden" name="poc_id"                       id="poc_id"                       value="{key('pagedata', 'poc_id')/value}"/>
            <input type="hidden" name="message_subject"              id="message_subject"              value="{root/PointOfContactVO/emailSubject}"/>
            <input type="hidden" name="message_content"              id="message_content"              value="{root/PointOfContactVO/body}"/> 
            <input type="hidden" name="delete_other_queues_selected" id="delete_other_queues_selected" />

            <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
              <tr>
                <td>
                  <table width="100%" border="0" align="center" cellpadding="2" cellspacing="2">
                    <tr>
                      <td align="center">
                        <div>
                          <table width="100%" border="0" cellpadding="2" cellspacing="0">
                            

                            <!--************* USER COMMUNICATION AREA - SUCCESS & ERROR MESSAGES WILL BE DISPLAYED HERE *************-->
                            <tr>
                              <td>
                                <table>
                                  <tr>
                                    <xsl:if test = "key('pagedata','validation_error')/value != ''">
                                      <td class = "errorMessage">
                                        NO QUEUE MESSAGES WERE PROCESSED.&nbsp;INPUT FILE VALIDATION ERROR:&nbsp;
                                        <xsl:value-of select = "key('pagedata','validation_error')/value"/>
                                      </td>
                                    </xsl:if>
                                  </tr>
                                  <tr>
                                    <xsl:if test = "key('pagedata','success_count')/value != ''">
                                      <td class = "OperationSuccess">
                                        <xsl:value-of select = "key('pagedata','success_count')/value"/>&nbsp;QUEUE MESSAGE(S) SUCCESSFULLY PROCESSED
                                      </td>
                                    </xsl:if>
                                  </tr>
                                  <tr>
                                    <xsl:if test = "/root/unableDeleteQueues != ''">
                                      <td class = "errorMessage">
                                        THE FOLLOWING QUEUE MESSAGES FAILED:&nbsp;
                                      </td>
                                    </xsl:if>
                                  </tr>
                                </table>
                              </td>
                              <td colspan="2">
                                <xsl:if test = "/root/unableDeleteQueues != ''">
                                  <textarea name="errorMessageTextArea" id="errorMessageTextArea" rows="3" cols="150" tabindex="5">
                                    <xsl:value-of select = "/root/unableDeleteQueues"/>
                                  </textarea>
                                </xsl:if>
                              </td>
                            </tr>

                            <tr><td colspan="3">&nbsp;</td></tr>




                            <!--************* RECORD INPUT AREA *************-->
                            <tr valign="top">
                              <td colspan="3">
                                <table width="100%" border="0" cellpadding="2" cellspacing="0">


                                  <!--************* RADIO BUTTONS *************-->
                                  <tr>
                                    <!-- Input File (.Xls) -->
                                    <td class="label" width="23%" align="left" valign="top">
                                      <input type="radio" name="inputType" id="inputType" value="FILE" checked="true" tabindex="5" onclick="doMasterCheck(false);"/> 
                                      Input File (.xls)<font color="red">&nbsp;*&nbsp;</font>:&nbsp;
                                    </td>

                                    <td class="Instruction" style="font-size:12pt; font-weight: bold;" width="10%" align="left">
                                      <center>OR</center>
                                    </td>

                                    <!-- Input Queue Message Ids -->
                                    <td class="label" width="23%" align="left" valign="top">
                                      <input type="radio" name="inputType" id="inputType" value="INPUTBOX" tabindex="5" onclick="doMasterCheck(false);"/> 
                                      Input Queue Message IDs<font color="red">&nbsp;*&nbsp;</font>:&nbsp;
                                    </td>

                                    <td class="Instruction" style="font-size:12pt; font-weight: bold;" width="10%" align="left">
                                      <center>OR</center>
                                    </td>

                                    <!-- Input External Order Numbers -->
                                    <td class="label" width="33%" align="left" valign="top">
                                      <input type="radio" name="inputType" id="inputType" value="EXTERNAL_ORDER_NUMBERS_BOX" tabindex="5" onclick="doMasterCheck(false);"/> 
                                      Input External Order Numbers<font color="red">&nbsp;*&nbsp;</font>:&nbsp;
                                    </td>
                                  </tr>

                                  <!--************* TEXTAREA *************-->
                                  <tr>
                                    <!-- Input File (.Xls) -->
                                    <td colspan="2" class="label" width="33%" align="left" valign="top">
                                      <input type="file" name="inputExcelFile" id="inputExcelFile" tabindex="5"/>
                                    </td>

                                    <!-- Input Queue Message Ids -->
                                    <td  colspan="2" class="label" width="33%" align="left" valign="top">
                                      <textarea name="inputTextArea" id="inputTextArea" rows="3" cols="28" tabindex="5"/>
                                    </td>

                                    <!-- Input External Order Numbers -->
                                    <td class="label" width="33%" align="left" valign="top">
                                      <textarea name="inputExternalOrderNumbersTextArea" id="inputExternalOrderNumbersTextArea" rows="3" cols="28" tabindex="5"/>
                                    </td>
                                  </tr>

                                  <!--************* Instructions *************-->
                                  <tr>
                                    <!-- INPUT FILE (.XLS) -->
                                    <td colspan="2" class="Instruction" width="33%" align="left">
                                      The MS Excel input file of queue messages as generated by "Batch<br/> Queue Management -- Delete" Customer Service menu option
                                    </td>

                                    <!-- Input Queue Message Ids -->
                                    <td colspan="2" class="Instruction"  width="33%" align="left">
                                      A list of Queue Message IDs (one payload per line)
                                    </td>

                                    <!-- Input External Order Numbers -->
                                    <td colspan="2" class="Instruction"  width="33%" align="left">
                                      A list of External Order Numbers (one per line)
                                    </td>
                                  </tr>
                                </table>
                              </td>
                            </tr>


                            <tr><td colspan="3">&nbsp;</td></tr>
                            <tr><td colspan="3">&nbsp;</td></tr>
                            <!--Batch Description-->
                            <tr>
                              <td class="label" width="23%" align="left" valign="top">Batch Description <font color="red">&nbsp;*&nbsp;</font>:</td>
                              <td class="label" width="23%" align="left" valign="top"><input type="text" name="batch_description" id="batch_description" tabindex="5" maxlength="25"/></td>
                              <td class="label" width="23%" align="left" valign="top">&nbsp;</td>
                              <td class="label" width="23%" align="left" valign="top">&nbsp;</td>
                              <td class="label" width="23%" align="left" valign="top">&nbsp;</td>
                            </tr>

                            <!--************* PREFERRED PARTNE FILTERS *************-->
                            <xsl:for-each select="/root/preferredPartners/preferredPartner">
                            <tr>
                                <td class="label" width="22%" align="left" valign="top">
                                        <input name="cb{name}" id="cb{name}" type="checkbox" tabindex="5" onclick="doPrefPartnerCB(this);"/>
                                        Include <xsl:value-of select="value"/>    
                                </td>
                                <td>&nbsp;</td>
                                <td class="Instruction" width="43%" align="left" valign="top">
                                (Optional) If checked, associated <xsl:value-of select="name"/> order queue messages will be deleted from selected queues.
                                <script type="text/javascript">
                                    ppArray[ppCnt] = "<xsl:value-of select="name"/>";
                                    ppCnt = ppCnt + 1;
                                </script>
                                </td>
                            </tr>
                            </xsl:for-each>
                            <tr><td colspan="3">&nbsp;</td></tr>
                            
                            <!--************* DELETE ASSOCIATED QUEUE MESSAGES *************-->
                            <tr valign="top">
                              <td class="label" width="22%" align="left" valign="top">
                                <input type="checkbox" name="cbDeleteOtherQueues" id="cbDeleteOtherQueues" tabindex="5" onclick="checkDeleteOtherQueues();"/> 
                                Delete Associated Queue Messages:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              </td>
                              <td width="35%" align="left" valign="top">
                                <select name="sDeleteOtherQueues" id="sDeleteOtherQueues" tabindex="5" size="3">
                                  <xsl:attribute name="multiple"/>
                                  <xsl:attribute name="disabled"/>
                                  <xsl:for-each select="/root/queueTypeList/queueType">
                                    <option value="{@queue_type}">
                                      <xsl:value-of select="@description"/>
                                    </option>
                                  </xsl:for-each>
                                </select>
                              </td>
                              <td class="Instruction" width="43%" align="left" valign="top">
                                (Optional) If checked, associated queue messages will be deleted from selected queues.  If deleting by external order number, this is required.
                              </td>
                            </tr>

                            <tr><td colspan="3">&nbsp;</td></tr>


                            <!--************* SEND MESSAGE *************-->
                            <tr valign="top">
                              <td class="label" width="22%" align="left">
                                <input type="checkbox" name="cbSendMessage" id="cbSendMessage" tabindex="5" onclick="doSendMessage();"/> 
                                Send Message:&nbsp;
                              </td>
                              <td width="35%" align="left">
                                <select name="sSendMessage" id="sSendMessage" onchange="doMasterCheck(true);" tabindex="5">
                                  <xsl:attribute name="disabled"/>
                                  <option value="">-select one-</option>
                                  <option value="ANS">ANS</option>
                                  <option value="CAN">CAN</option>
                                </select>
                              </td>
                              <td class="Instruction" width="43%" align="left" valign="top">
                               (Optional) If checked, an ANS message or a CAN message will be sent to the florist/vendor.
                              </td>
                            </tr>

                            <tr>
                              <td width="22%" align="right">
                                Give florists more money (ANSP):&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$
                              </td>
                              <td width="36%" align="left" >
                                 <input type="text" name="sAnspAmount" id="sAnspAmount" tabindex="5" disabled = "true"  onblur = "formatDollarAmountWithMaxLimit(this,maxAnspAmount);"/>                                
                              </td>
                              <td class="Instruction" width="48%" align="left" valign="top">
                               The amount of additional money to be added to the mercury value for the florists. Maximum: $<xsl:value-of select = "key('pagedata','maxAnspAmount')/value"/>
                              </td>
                            </tr>
                            <tr>
                            <td width="22%" align="right">
                                Give florists amount they requested (ANSP):&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              </td>
                              <td width="35%" align="left" >
                                 <input type="checkbox" name="sAnspOverride" id="sAnspOverride"  disabled = "true"  tabindex="5" onclick = "useASKPPrice(this,sAnspAmount);"/>                                
                              </td>
                              <td class="Instruction" width="43%" align="left" valign="top">
                             
                              </td>
                            </tr>

                            <tr>
                              <td width="22%" align="right">
                                Reason Code:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              </td>
                              <td width="35%" align="left" >
                                <select name="sCancelReasonCode" id="sCancelReasonCode" tabindex="5">
                                  <xsl:attribute name="disabled"/>
                                  <option value="">-select one-</option>
                                  <xsl:for-each select="/root/CancelCodes/CancelCode">
                                    <option value="{@name}">
                                      <xsl:value-of select="@value"/>
                                    </option>
                                  </xsl:for-each>
                                </select>
                              </td>

                              <td class="Instruction" width="43%" align="left" valign="top">
                                Reason Code defines the reason for the CAN.  It will only be used on vendor orders.
                              </td>

                            </tr>

                            <tr>
                              <td width="22%" align="right">
                                Text or Reason:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              </td>
                              <td width="35%" align="left" >
                                <textarea name="newMessage" id="newMessage" rows="3" cols="76" disabled="true" tabindex="5"/>
                              </td>
                              <td class="Instruction" width="43%" align="left" valign="top">
                                Text or Reason is the message that will be sent to the vendor/florist.
                              </td>
                            </tr>

                            <tr><td colspan="3">&nbsp;</td></tr>


                            <!--************* RESUBMIT ORDER FOR PROCESSING *************-->
                            <tr>
                              <td colspan="2" class="label" width="57%" align="left" valign="center">
                                <input type="checkbox" name="isOrderReprocessed" id="isOrderReprocessed" tabindex="5" onclick="doResubmit();" >
                                  <xsl:if test="key('pagedata','isOrderReprocessed')/value = 'on'">
                                    <xsl:attribute name="checked">true</xsl:attribute>
                                  </xsl:if>
                                </input>
                                Resubmit Order For Processing
                              </td>
                              <td class="Instruction" width="43%" align="left" valign="top">
                                (Optional) If checked, the order associated with the deleted message will be resubmitted through Order Processing.
                              </td>
                            </tr>

                            <tr><td colspan="3">&nbsp;</td></tr>


                            <!--************* ADD ORDER COMMENT *************-->
                            <tr>
                              <td class="label" width="22%" align="left" valign="top">
                                <input type="checkbox" name="isCommentAdded" id="isCommentAdded" tabindex="5" onclick="javascript:doCommentCheckboxChangeAction();">
                                  <xsl:if test="key('pagedata','isCommentAdded')/value = 'on'">
                                    <xsl:attribute name="checked">true</xsl:attribute>
                                  </xsl:if>
                                </input>
                                Add Order Comment:
                              </td>
                              <td width="35%" align="left">
                                <textarea name="comment_text" id="comment_text" cols="77" rows="3"  disabled="true" tabindex="5">
                                  <xsl:value-of select="key('pagedata','comment_text')/value"/>
                                </textarea>
                              </td>
                              <td class="Instruction" width="43%" align="left" valign="top">
                                (Optional) If checked, the order associated with the deleted message will be appended with the custom comment text entered here.
                              </td>
                            </tr>


                            <!--************* SEND CUSTOMER EMAIL *************-->
                            <tr>
                              <td colspan="2" class="label" width="57%" align="left" valign="center">
                                <input type="checkbox" name="isEmailAdded" id="isEmailAdded" tabindex="5" onclick="javascript:doEmailCheckboxChangeAction();">
                                  <xsl:if test="key('pagedata','isEmailAdded')/value = 'on'">
                                    <xsl:attribute name="checked">true</xsl:attribute>
                                  </xsl:if>
                                </input>
                                Send Customer Email:
                              </td>
                              <td class="Instruction" width="43%" align="left" valign="top">
                                (Optional) If checked, the order customer associated with the deleted message will be sent the custom email entered here.
                              </td>
                            </tr>

                            <!-- Title -->
                            <tr>
                              <td width="22%" align="right">
                                Title:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              </td>
                              <td width="35%" align="left" >
                                <select name="stockEmail" id="stockEmail" onchange="doRefreshEmailContentAction()" style="width:300px;" tabindex="5">
                                  <xsl:attribute name="disabled"/>
                                  <option value="">-select one-</option>
                                  <xsl:for-each select="/root/StockMessageTitles/Title">
                                    <xsl:choose>
                                      <xsl:when test="$msg_id=stock_email_id">
                                        <option value="{stock_email_id}" selected="selected">
                                          <xsl:value-of select="title"/>
                                          - 
                                          <xsl:value-of select="subject"/>
                                        </option>
                                      </xsl:when>
                                      <xsl:otherwise>
                                        <option value="{stock_email_id}">
                                          <xsl:value-of select="title"/>
                                          - 
                                          <xsl:value-of select="subject"/>
                                        </option>
                                      </xsl:otherwise>
                                    </xsl:choose>
                                  </xsl:for-each>
                                </select>
                              </td>
                              <td class="Instruction" width="43%" align="left" valign="top">
                                &nbsp;
                              </td>
                            </tr>

                            <!-- Subject -->
                            <tr>
                              <td width="22%" align="right">
                                Subject:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              </td>
                              <td width="35%" align="left" >
                                <input name="newSubject" id="newSubject" size="75" type="text" value="{key('pagedata', 'message_subject')/value}" disabled="true" tabindex="5"/>
                              </td>
                              <td class="Instruction" width="43%" align="left" valign="top">
                                &nbsp;
                              </td>
                            </tr>

                            <!-- Message -->
                            <tr>
                              <td width="22%" align="right">
                                Message:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              </td>
                              <td width="35%" align="left" >
                                <textarea name="newBody" id="newBody" rows="3" cols="76" disabled="true" tabindex="5">
                                  <xsl:value-of select="key('pagedata', 'message_content')/value"/>
                                </textarea>
                              </td>
                              <td class="Instruction" width="43%" align="left" valign="top">
                                &nbsp;
                              </td>
                            </tr>


                            <!--************* REFUND ORDER *************-->
                            <tr>
                              <td colspan="2" class="label" width="57%" align="left" valign="center">
                                <input type="checkbox" name="isOrderRefunded" id="isOrderRefunded" tabindex="5" onclick="javascript:doRefund();">
                                  <xsl:if test="key('pagedata','isOrderRefunded')/value = 'on'">
                                    <xsl:attribute name="checked">true</xsl:attribute>
                                  </xsl:if>
                                </input>
                                Refund Order
                              </td>
                              <td class="Instruction" width="43%" align="left" valign="top">
                                (Optional) If checked, the order associated with the deleted message will be fully refunded.
                              </td>
                            </tr>
                            
                            <!-- Refund Disposition -->
                            <tr>
                              <td width="22%" align="right">
                                Refund Disposition:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              </td>
                              <td width="35%" align="left" >
                                <select name="refundDisposition" id="refundDisposition" tabindex="5">
                                  <xsl:attribute name="disabled"/>
                                  <option value="">-select one-</option>
                                  <xsl:for-each select="/root/RefundDispositions/Disposition">
                                    <option value="{refund_disp_code}">
                                      <xsl:value-of select="refund_disp_code"/>&nbsp;
                                      -
                                      <xsl:value-of select="short_description"/>
                                    </option>
                                  </xsl:for-each>
                                </select>
                              </td>
                              <td class="Instruction" width="43%" align="left" valign="top">
                                &nbsp;
                              </td>
                            </tr>

                            <!-- Responsible Party -->
                            <tr>
                              <td width="22%" align="right">
                                Responsible Party:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              </td>
                              <td width="35%" align="left" >
                                <select name="responsibleParty" id="responsibleParty" tabindex="5">
                                  <xsl:attribute name="disabled"/>
                                  <option value="">-select one-</option>
                                  <option value="FTD">FTD.COM</option>
                                  <option value="Other">Other</option>
                                </select>
                              </td>
                              <td class="Instruction" width="43%" align="left" valign="top">
                                &nbsp;
                              </td>
                            </tr>


                          </table>
                        </div>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </form>
  
          <table align="center" width="98%" border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td colspan = "3">&nbsp;</td>
            </tr>
            <tr>
              <td width="20%"></td>
              <td valign="top" align="center" width="45%">
                <button type="button" id="buttonQDelete" class="bluebutton" accesskey="s" tabindex="5" onclick="javascript:doQueueMessageDeleteAction();">(S)ubmit</button>
                <button type="button" id="buttonClearAllFields" class="bluebutton" accesskey="c" tabindex="5" onclick="javascript:doClearFields();">(C)lear all Fields</button>
              </td>
              <td width="35%" align="right">
                <button type="button" id="mainMenu" class="bluebutton" accesskey="b" tabindex="5" onclick="javascript:doBackAction();">(B)ack</button>
              </td>
            </tr>
          </table>
        </div>

        <div id="waitDiv" style="display:none">
          <table id="waitTable" width="98%" border="3" height="100%" align="center" cellpadding="30" cellspacing="1" bordercolor="#006699">
            <tr>
              <td width="100%">
                <table width="100%" cellspacing="0" cellpadding="0">
                  <tr>
                    <td id="waitMessage" align="right" width="50%" class="waitMessage"></td>
                    <td id="waitTD" width="50%" class="waitMessage"></td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
        </div>

        <xsl:call-template name="footer"></xsl:call-template>

      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>