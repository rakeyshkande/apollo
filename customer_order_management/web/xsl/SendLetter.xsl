
<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="header.xsl"/>
	<xsl:import href="footer.xsl"/>
  <xsl:import href="securityanddata.xsl" />
  <xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>
	<xsl:output method="html" indent="no"/>
	<xsl:output indent="yes"/>
	<xsl:key name="pagedata" match="root/pageData/data" use="name" />
    <xsl:key name="security" match="/root/security/data" use="name"/>
	<xsl:variable name="msg_id" select="key('pagedata', 'message_id')/value"/>
	<xsl:template match="/">
		<html>
			<head>
				<META http-equiv="Content-Type" content="text/html"/>
				<title>Send Letter</title>
				<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
				<link rel="stylesheet" type="text/css" href="css/calendar.css"/>
        <script type="text/javascript" src="js/tabIndexMaintenance.js"/>
				<script type="text/javascript" src="js/util.js"></script>
				<script type="text/javascript" src="js/calendar.js"></script>
				<script type="text/javascript" src="js/clock.js"></script>
			  <script type="text/javascript" src="js/commonUtil.js"></script>
				<script type="text/javascript" src="js/mercuryMessage.js"></script>
				<script type="text/javascript"><![CDATA[
// init code
function init()
{
  setSendLetterIndexes();
  if (document.getElementById('newBody').value != '')
    textChange();
}

/*******************************************************************************
*  setSendLetterIndexes()
*  Sets tabs for View Email Page. 
*  See tabIndexMaintenance.js
*******************************************************************************/
function setSendLetterIndexes()
{
  untabAllElements();
	var begIndex = findBeginIndex();
 	var tabArray = new Array('stockLetter', 'refreshButton', 'searchAction', 
    'spellCheckButton', 'sendButton', 'commButton', 'backButton', 'mainMenu',
    'masterOrderNumberLink', 'printer');
  var newBegIndex = setTabIndexes(tabArray, begIndex);
  return newBegIndex;
}

function doOrderSearchAction(orderNumberType)
{
  var form = document.forms[0];
  var order_number = null;
  if(orderNumberType == "Master Order")
    order_number = form.master_order_number.value;
  else
    order_number = form.external_order_number.value;
	
	if(document.sendLetter.change_flag.value=="Y"){
		var exitConfirmed = doExitPageAction('Your changes have not been saved. Do you wish to continue?');
		if (exitConfirmed==true){
  			form.action = "customerOrderSearch.do" +
                 "?action=customer_account_search" +
                 "&order_number="+order_number;
  			form.target = "_self";
  			form.submit();
  		}
  	}else{
		form.action = "customerOrderSearch.do" +
                 "?action=customer_account_search" +
                 "&order_number="+order_number;
  		form.target = "_self";
  		form.submit();
  	}
}

// perform communication button action
function commAction()
{
  if(document.sendLetter.change_flag.value=="Y"){
		var exitConfirmed = doExitPageAction('Your changes have not been saved. Do you wish to continue?');
		if (exitConfirmed==true){
      document.forms[0].action = "displayCommunication.do";
      document.forms[0].action_type.value = "display_communication";
      document.forms[0].submit();
		}
	}else{
    document.forms[0].action = "displayCommunication.do";
    document.forms[0].action_type.value = "display_communication";
    document.forms[0].submit();
	}
}

function textChange(){
	document.sendLetter.change_flag.value="Y";
	document.sendLetter.spellCheckFlag.value="Y";
}

function doRefresh(){
  document.sendLetter.action_type.value = "load_content";
  document.sendLetter.action = "loadSendMessage.do";
  document.sendLetter.message_id.value = document.sendLetter.stockLetter.options[document.sendLetter.stockLetter.selectedIndex].value;
  if (document.sendLetter.message_id.value==0) {
    alert("Select a Stock Letter and then Refresh");
    return;
  }
  document.sendLetter.message_title.value = document.sendLetter.stockLetter.options[document.sendLetter.stockLetter.selectedIndex].text;
  document.sendLetter.target = "_self";
  document.sendLetter.submit();
}

function mainMenuClick(){
	if(document.sendLetter.change_flag.value=="Y"){
		var exitConfirmed = doExitPageAction('Your changes have not been saved. Do you wish to continue?');
		if (exitConfirmed==true){
			doMainMenuAction();
		}
	}else{
		doMainMenuAction();
	}
}

function spellCheck()
{
  var form = document.sendLetter;
    var value = escape(form.newBody.value).replace(/\+/g,"%2B");
    var url = "spellCheck.do?content=" + value + "&callerName=newBody"+
              "&securitytoken="+form.securitytoken.value+
              "&context="+form.context.value;

    window.open(url,"Spell_Check","toolbar=no,resizable=yes,scrollbars=yes,width=500,height=300,help=no");
   form.spellCheckFlag.value = "N";
}

/*************************************************************************************
*	Perorms the Back button functionality
**************************************************************************************/
function doBackAction()
{
   doOrderSearchAction('External Order');
}

function sendAction(){
	var form=document.sendLetter;
  // took out the madatory spell check due to speed problems...will put it back when the process is sped up
  form.spellCheckFlag.value = "N";
	if(form.spellCheckFlag.value == "N")
	{
		var ran = Math.floor(Math.random()*100);
		
		// reset change flag
		document.sendLetter.change_flag.value="N";
		
		// window.open approach to preview the letter
	  	var url = "sendMessage.do?action_type=send_message"+
		            "&message_id="+form.stockLetter.options[form.stockLetter.selectedIndex].value+
		            "&message_title="+form.stockLetter.options[form.stockLetter.selectedIndex].text+
		            "&message_content="+escape(form.newBody.value).replace(/\+/g,"%2B")+
		            "&company_id="+form.company_id.value+
		            "&order_detail_id="+form.order_detail_id.value+
		            "&message_type="+form.message_type.value+
		            "&origin_id="+form.origin_id.value+
		            "&securitytoken="+form.securitytoken.value+
		            "&context="+form.context.value+
		            "&rand="+ran;
	  	
		var sURL = "letterPreview.html";
		var objFlorist = new Object();
		objFlorist.letterURL = url;
		
		var sFeatures="dialogHeight:500px;dialogWidth:800px;status:1;scroll:0;resizable:0";
		window.showModalDialog(sURL, objFlorist, sFeatures);
	}
	else
	{
		alert("You must spell check the letter before sending");
	}
  
}

]]>
	</script>
</head>
<body onload="init();">
	<xsl:call-template name="header">
		<xsl:with-param name="headerName"  select="'Send Letter'" />
		<xsl:with-param name="showTime" select="true()"/>
		<xsl:with-param name="showBackButton" select="true()"/>
		<xsl:with-param name="showPrinter" select="true()"/>
		<xsl:with-param name="showSearchBox" select="false()"/>
		<xsl:with-param name="dnisNumber" select="$call_dnis"/>
		<xsl:with-param name="cservNumber" select="$call_cs_number"/>
		<xsl:with-param name="indicator" select="$call_type_flag"/>
		<xsl:with-param name="brandName" select="$call_brand_name"/>
		<xsl:with-param name="showCSRIDs" select="true()"/>
		<xsl:with-param name="backButtonLabel" select="'(B)ack to Order'"/>
		<xsl:with-param name="cdispEnabledFlag" select="$cdisp_enabledFlag"/>
	</xsl:call-template>
	<form name="sendLetter" method="post" action="">
	
    <xsl:call-template name="securityanddata"/>
    <xsl:call-template name="decisionResultData"/>
		<input type="hidden" name="action_type" id="action_type" value="{key('pagedata','action_type')/value}"/>
		<input type="hidden" name="message_id" id="message_id" value=""/>
		<input type="hidden" name="message_title" id="message_title" value=""/>
		<input type="hidden" name="spellCheckFlag" id="spellCheckFlag" value="N"/>
		<input type="hidden" name="company_id" id="company_id" value="{key('pagedata','company_id')/value}"/>
		<input type="hidden" name="customer_id" id="customer_id" value="{key('pagedata','customer_id')/value}"/>
		<input type="hidden" name="customer_first_name" id="customer_first_name" value="{key('pagedata', 'customer_first_name')/value}" />
		<input type="hidden" name="customer_last_name" id="customer_last_name" value="{key('pagedata', 'customer_last_name')/value}" />
		<input type="hidden" name="order_guid" id="order_guid" value="{key('pagedata','order_guid')/value}"/>
		<input type="hidden" name="master_order_number" id="master_order_number" value="{key('pagedata','master_order_number')/value}" />
		<input type="hidden" name="external_order_number" id="external_order_number" value="{key('pagedata', 'external_order_number')/value}" />
		<input type="hidden" name="order_detail_id" id="order_detail_id" value="{key('pagedata', 'order_detail_id')/value}" />
		<input type="hidden" name="customer_email_address" id="customer_email_address" value="key('pagedata', 'customer_email_address')/value"/>
		<input type="hidden" name="message_type" id="message_type" value="{key('pagedata', 'message_type')/value}"/>
		<input type="hidden" name="origin_id" id="origin_id" value="{key('pagedata','origin_id')/value}"/>		
		<input type="hidden" name="message_content" id="message_content" value="{root/PointOfContactVO/body}" />
		<input type="hidden" name="change_flag" id="change_flag" value="N"/>
		
		<table border="0" cellpadding="0" cellspacing="0" width="98%" align="center">
			<tr>
				<td id="lockError" class="ErrorMessage"></td>
			</tr>
		</table>
		<table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
			<tr>
				<td>
					<table width="100%" border="0" align="center" cellpadding="2" cellspacing="2">
						<tr>
							<td align="center">
								<div style="overflow:auto; width:80%; height=370;">
									<table width="100%" border="0" align="center" cellpadding="2" cellspacing="0">
										
										<tr>
											<td class="label" width="25%" align="right">Master Order Number:</td>
											<td align="left"><a name="masterOrderNumberLink" href="javascript:doOrderSearchAction('Master Order');"><xsl:value-of select="key('pagedata','master_order_number')/value"/></a></td>
											<td class="label" width="25%" align="right">Customer:</td>
											<td align="left"><xsl:value-of select="key('pagedata','customer_first_name')/value"/>&nbsp;<xsl:value-of select="key('pagedata','customer_last_name')/value"/></td>
										</tr>
										<tr>
											<td class="label" width="25%" align="right">Order Number:</td>
											<td align="left"><a href="javascript:doOrderSearchAction('External Order');"><xsl:value-of select="key('pagedata','external_order_number')/value"/></a></td>
										</tr>
										<tr><td colspan="7">&nbsp;</td></tr>
										<tr>
											<td class="label" align="right">Stock Letter:</td>
											<td colspan="3" align="left">
												<select name="stockLetter" id="stockLetter">
													<option value="0">-select one-</option>
													<xsl:for-each select="/root/StockMessageTitles/Title">
													<xsl:choose>
													<xsl:when test="$msg_id=stock_letter_id">
														<option value="{stock_letter_id}" selected="selected"><xsl:value-of select="title"/></option>
													</xsl:when>
													<xsl:otherwise>
														<option value="{stock_letter_id}"><xsl:value-of select="title"/></option>
													</xsl:otherwise>
													</xsl:choose>
													</xsl:for-each>
												</select>
												&nbsp;
												<button type="button" class="blueButton" accesskey="R" name="refreshButton" tabindex="9" onclick="doRefresh()" style="width:55px">(R)efresh</button>
											</td>
										</tr>
										<tr><td colspan="7">&nbsp;</td></tr>
										<tr>
											<td align="center" rowspan="13" colspan="6"><textarea onchange="textChange()" name="newBody" rows="15" cols="110"><xsl:value-of select="root/PointOfContactVO/body"/></textarea></td>
										</tr>
									</table>
								</div>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	
	<iframe id="lockFrame" src="" width="0" height="0" frameborder="0"/>
	</form>
	<table width="98%" align="center">
		<tr>
			<td width="75%" valign="top">
				<button type="button" style="width:90" accesskey="R" class="bigBlueButton" name="searchAction" tabindex="6" onClick="doOrderSearchAction('External Order');">
					Recipien(t)/<br/>Order
				</button>
				<button type="button" style="width:90" accesskey="O" class="bigBlueButton" name="commButton" id="commButton" onClick="commAction();">
					C(o)mmunication
			    </button>
			    <button type="button"   id="orderComments" class="BigBlueButton ButtonTextBox2" accesskey="C" onclick="javascript:openOrderComments();">
                 Order<br/>(C)omments
                </button> 
			    <button type="button"   id="refund" class="BigBlueButton ButtonTextBox1" accesskey="R" onclick="javascript:openRefunds();" >
                (R)efund
                </button>
			    <button type="button" style="width:90" accesskey="P" class="bigBlueButton" name="spellCheck" id="spellCheck" onClick="spellCheck();">
				S(p)ell<br/>Check
			   </button>
			   <button type="button" style="width:90" accesskey="S" class="bigBlueButton" name="sendButton" id="sendButton" onClick="sendAction();">
			   (S)end
			   </button>

			</td>
			<td width="20%" align="right">
				<button type="button" class="blueButton" accesskey="B" name="backButton" tabindex="9" onclick="javascript:doBackAction();">(B)ack to Order</button>
  				<br />
 				<button type="button" class="bigBlueButton" accesskey="M" name="mainMenu" tabindex="10" onclick="mainMenuClick()">(M)ain<br/>Menu</button>
  			</td>
  		</tr>
	</table>
	<div id="waitDiv" style="display:none">
		<table id="waitTable" width="98%" border="3" height="100%" align="center" cellpadding="30" cellspacing="1" bordercolor="#006699">
			<tr>
				<td width="100%">
					<table width="100%" cellspacing="0" cellpadding="0">
						<tr>
							<td id="waitMessage" align="right" width="50%" class="waitMessage"></td>
							<td id="waitTD" width="50%" class="waitMessage"></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>
	<xsl:call-template name="footer"></xsl:call-template>
</body>
</html>
</xsl:template>
</xsl:stylesheet>
<!-- Stylus Studio meta-information - (c) 2004-2005. Progress Software Corporation. All rights reserved.
<metaInformation>
<scenarios ><scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="..\..\..\..\..\dnisMaint.xml" htmlbaseurl="" outputurl="" processortype="internal" profilemode="0" profiledepth="" profilelength="" urlprofilexml="" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext=""/></scenarios><MapperMetaTag><MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/><MapperBlockPosition></MapperBlockPosition></MapperMetaTag>
</metaInformation>
-->