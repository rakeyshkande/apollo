<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">

	<PRODUCTS>
		<xsl:for-each select="PRODUCTS/PRODUCT">
			<xsl:sort select="@standardprice" data-type="number"/>
        <xsl:copy-of select="."/>
		</xsl:for-each>

<!--
		<xsl:copy-of select="PRODUCTS/OEParameters"/>
    <xsl:copy-of select="PRODUCTS/HOLIDAYS"/>
    <xsl:copy-of select="PRODUCTS/SHIPPINGMETHODS"/>
    <xsl:copy-of select="PRODUCTS/DATERANGES"/>
-->

	</PRODUCTS>

</xsl:template>
</xsl:stylesheet>