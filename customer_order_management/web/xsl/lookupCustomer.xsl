<!DOCTYPE ACDemo[
   <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:import href="securityanddata.xsl"/>
<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>
<xsl:output method="html" indent="no"/>
<xsl:output indent="yes"/>

<xsl:key name="pageData" match="/root/pageData/data" use="name"/>
<xsl:key name="security" match="/root/security/data" use="name"/>
<xsl:template match="/root">
<html>
	<head>
		<title> Customer Lookup </title>
		<link rel="stylesheet" type="text/css" href="css/ftd.css"></link>
		<script language="JavaScript" src="js/FormChek.js"/>
		<script language="JavaScript" src="js/util.js"/>
		<script>
   <![CDATA[

function onKeyDown(rowNum)
{
	if (window.event.keyCode == 13)
	{
		populatePage(rowNum)
	}

}

function populatePage(rowNum)
{
	var ret = new Array();
	var trArray = document.getElementsByName("customerRow");
	for (var i = 0; i < trArray.length; i++)
	{
		if (trArray[i].rowNum == rowNum)
		{
			ret[0] = trArray[i].firstName;
			ret[1] = trArray[i].lastName;
			ret[2] = trArray[i].address1;
			ret[3] = trArray[i].address2;
			ret[4] = trArray[i].city;
			ret[5] = trArray[i].state;
			ret[6] = trArray[i].zipCode;
			ret[7] = trArray[i].country;
			ret[8] = document.forms[0].phoneInput.value;
			top.returnValue = ret;
			top.close();
		}
	}
	top.returnValue = ret;
	top.close();
}

function reopenPopup()
{
	var check = true;
	var bag = ",/.<>?;:\|[]{}~!@#$%^&*()-_=+`" + "\\\'";
	var phoneInput = document.forms[0].phoneInput.value;
	phoneInput = stripWhitespace(phoneInput);
	phoneInput = stripCharsInBag(phoneInput, bag);
	document.forms[0].phoneInput.value = phoneInput;


	if(phoneInput == "") {
		 document.forms[0].phoneInput.focus();
		 document.forms[0].phoneInput.style.backgroundColor='pink'
		 check = false;
		 alert("Please correct the marked fields");
	}

	if (check) {
		 var url_source="";
		 document.forms[0].action = url_source;
		 document.forms[0].method = "get";
		 document.forms[0].target = "VIEW_CUSTOMER_LOOKUP";

		 document.forms[0].submit();
	}
}

function init()
{
	addDefaultListeners();
	window.name = "VIEW_CUSTOMER_LOOKUP";
	document.forms[0].phoneInput.focus()
}
   ]]>
		</script>
	</head>
	<body onLoad="javascript:window.focus(); init();">
		<form name="form" method="post" action="lookupCustomer.do">
			<xsl:call-template name="securityanddata"/>
			<xsl:call-template name="decisionResultData"/>
			<input type="hidden" name="customerInput" id="customerInput" value="{pageData/data[name='customerInput']/value}"/>
			<table width="100%" border="0" cellspacing="2" cellpadding="2">
				<tr>
					<td colspan="3" align="center">
						 <h1>Recipient Lookup</h1>
					</td>
				</tr>
				<tr>
					<td width="100%" valign="top">
						<center>
							<table width="100%" border="0" cellpadding="2" cellspacing="2">
								<tr>
									<td>
										<table width="98%" border="0" cellpadding="1" cellspacing="1">
											<tr>
												<td nowrap="true" colspan="3" align="left">
													<span class="instruction"> Enter Phone Number </span> &nbsp;
													<input tabindex="1" type="text" name="phoneInput" id="phoneInput" size="30" maxlength="50" value="{key('pageData', 'phoneInput')/value}"/> &nbsp;
													<span class="instruction"> and press </span>
													<button type="button" class="BlueButton" tabindex="2" border="0" onclick="javascript:reopenPopup()">Search</button>
												</td>
											</tr>
											<tr><td colspan="10"><hr></hr></td></tr>
											<tr>
												<td class="instruction">
													 &nbsp;Please click on an arrow to select a customer.
												</td>
												<td colspan="10" align="right">
													<button type="button" class="BlueButton" tabindex="3" border="0" onclick="javascript:populatePage('None');">Close screen</button>
												</td>
											</tr>
										</table>
										<table width="98%" border="0" cellpadding="2" cellspacing="2">
											<tr>
												<td>
													<table class="LookupTable" width="100%" border="1" cellpadding="2" cellspacing="2">
														<tr>
															<td width="5%" class="label"> &nbsp; </td>
															<td class="label" valign="bottom"> Name </td>
															<td class="label" valign="bottom"> Address </td>
															<td class="label" valign="bottom"> City </td>
															<td class="label" valign="bottom"> State </td>
															<td class="label" valign="bottom"> Zip/Postal Code</td>
															<td class="label" valign="bottom"> Phone </td>
															<td>
																<xsl:for-each select="customerList/customer">
																	<!-- put the values in the row to avoid js error in populatePage method-->
																	<tr id="customerRow" rowNum="{position()}" firstName="{first_name}" lastName="{last_name}" address1="{address_1}" address2 = "{address_2}" city="{city}" state="{state}" zipCode="{zip_code}" country="{country}">
																		<td>
																			<!-- need onkeydown because it's a gif. having focus and pressing enter will not suffice in this case -->
																			<img src="images/selectButtonRight.gif" tabindex="4" border="0" onclick="javascript:populatePage('{position()}');" onkeydown="javascript:onKeyDown('{position()}');"></img>
																		</td>
																		<td align="left"><xsl:value-of select="first_name"/> &nbsp;<xsl:value-of select="last_name"/></td>
																		<td align="left"><xsl:value-of select="address_1"/> &nbsp;<xsl:value-of select="address_2"/></td>
																		<td align="left"><xsl:value-of select="city"/></td>
																		<td align="left"><xsl:value-of select="state"/></td>
																		<td align="left"><xsl:value-of select="zip_code"/></td>
																		<td align="left"><xsl:value-of select="key('pageData', 'phoneInput')/value"/>&nbsp;</td>
																	</tr>
																</xsl:for-each>
															</td>
														</tr>
														<tr id="customerRow" rowNum="None" firstName="{first_name}" lastName="" address1="" address2 = "" city="" state="" zipCode="" country="">
															<td>
																<!-- need onkeydown because it's a gif. having focus and pressing enter will not suffice in this case -->
																<img src="images/selectButtonRight.gif" tabindex="5" border="0" onclick="javascript:populatePage('None')" onkeydown="javascript:onKeyDown('None')"></img>
															</td>
															<td colspan="6"> None of the above </td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
										<table width="98%">
											<tr>
												<td align="right">
													<button type="button" class="BlueButton" tabindex="6" border="0" onclick="javascript:populatePage('None')">Close screen</button>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</center>
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>
</xsl:template>
</xsl:stylesheet>