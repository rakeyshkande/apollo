<!DOCTYPE ACDemo[
	<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="securityanddata.xsl" />
	<xsl:key name="security" match="/root/security/data" use="name" />
	<xsl:key name="pageData" match="/root/pageData/data" use="name" />
	<xsl:param name="showSummaryAndFsHistory"><xsl:value-of select="key('pageData','SHOW_SUMMARY_AND_FS_HISTORY')/value" />	</xsl:param>
	<xsl:param name="showEmailList"><xsl:value-of select="key('pageData','SHOW_EMAIL_LIST')/value" /></xsl:param>
	<xsl:param name="searchedEmailAddress"><xsl:value-of select="key('pageData','SEARCHED_EMAIL_ADDRESS')/value" /></xsl:param>
	<xsl:param name="customerId"><xsl:value-of select="key('pageData','customer_id')/value" /></xsl:param>

	<xsl:decimal-format NaN="" name="notANumber" />
	<xsl:template match="/root">
		<html>
			<title>Services Information</title>

			<head>
				<link rel="stylesheet" type="text/css" href="css/ftd.css" />
				<script type="text/javascript">
					var searchedEmail = '<xsl:value-of select="$searchedEmailAddress" />';
					var showEmail = '<xsl:value-of select="$showEmailList" />';
					var custId = '<xsl:value-of select="$customerId" />';	
     				<![CDATA[
					
					function doCloseAction(){
						top.close();
					}
					
					function init() {						
						document.forms[0].popup_buyer_email_address.value = searchedEmail;
					}
					
					function searchWithEmail() {						
						var buyerEmailAddress = document.getElementById("popup_buyer_email_address").value;												
						submitServicesInfo(buyerEmailAddress);
					}	
					
					function submitServicesInfo(emailAddress) {
						document.forms[0].email_address.value = emailAddress;						
						document.forms[0].action.value = "get_fsm_details";
						document.forms[0].showEmailDetails.value=showEmail;
						document.forms[0].customer_id.value=custId;
					    document.forms[0].submit();
					}
					function searchFromDropDown() {
						var buyerEmailAddress = document.forms[0].drop_down_email_address.value;												
						submitServicesInfo(buyerEmailAddress);						 						 
					}
					
					function processKeyPress(){
						if (window.event){
        					if (window.event.keyCode){
          						if (window.event.keyCode == 13){
          							searchWithEmail();
            					}
            				}
            			}
					}
					]]>
				</script>
			</head>
			<body onload="javascript:init();">

				<form name="servicesInfoForm" action="customerOrderSearch.do">
					<input type="hidden" name="email_address" />
					<input type="hidden" name="securitytoken" value="{key('security', 'securitytoken')/value}" />
					<input type="hidden" name="context" value="{key('security', 'context')/value}" />
					<input type="hidden" name="adminAction"	value="{key('security', 'adminAction')/value}" />
					<input type="hidden" name="action" />
					<input type="hidden" name="showEmailDetails" />
					<input type="hidden" name="customer_id" />
					<xsl:call-template name="securityanddata" />
					
					<table class="mainTable" align="center" cellpadding="1"	cellspacing="1" width="98%">
						
						<tr><td class="banner">Services Information</td></tr>
						<tr><td>&nbsp;</td></tr>						
						<tr>
							<td align="center">
								<table border="0" cellpadding="2" cellspacing="2" align="center">
									<tr align="center">
										<td align="right" class="label">Email Address:&nbsp;
										</td>
										<td align="left">
											<input type="text" size="30" maxlength="55" id="popup_buyer_email_address" name="popup_buyer_email_address"  onkeypress="processKeyPress();"/>
										</td>
										<td align="left">
											<input type="button" value="Search"	onclick="javascript:searchWithEmail();" />
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr><td>&nbsp;</td></tr>
						<xsl:if test="$showEmailList = 'Yes'">
							<tr>
								<td align="center">
									<select id="drop_down_email_address" name="drop_down_email_address"
										onchange="searchFromDropDown()">
										<option value="default" selected="selected">Select email address</option>
										<xsl:for-each select="EMAIL_ADDRESSES/EMAIL_INFO">
											<option value="{EMAIL_ADDRESS}">
												<xsl:value-of select="EMAIL_ADDRESS" />
											</option>
										</xsl:for-each>
									</select>
								</td>
							</tr>
						</xsl:if>
						<tr><td>&nbsp;</td></tr>						
						<xsl:choose>
							<xsl:when test="$showSummaryAndFsHistory = 'Yes'">
								<tr>
									<td>
										<table align="center" width="100%" class="innerTable"
											cellpadding="0" cellspacing="0">																						 											
											<tr>
												<td>
													<table id="servicesInfoPopup" style="word-break:break-all;"
														cellpadding="0" cellspacing="3" width="100%">
														<xsl:choose>
															<xsl:when test="count(FSM_SUMMARY/SUMMARY_INFO/display_name) >0 and count(SERVICES/SERVICES_INFO/email_address) > 0 ">
																
															 <xsl:for-each select="FSM_SUMMARY/SUMMARY_INFO">
													          <tr>
													            <td align="right" class="label" width="25%">Service:&nbsp;</td>
													            <td align="left"><xsl:value-of disable-output-escaping="yes" select="display_name"/></td>
													          </tr>													          
													          <tr>
													            <td align="right" class="label">Status:&nbsp;</td>
													            <td align="left">
													            <xsl:value-of select="account_program_status"/>
													            </td>
													          </tr>
													          <tr>
													            <td align="right" class="label">Original Join Date:&nbsp;</td>													            
													            <td align="left">
													            	<xsl:value-of select="start_date"/>
													            </td>
													          </tr>
													          <tr>
													          	<xsl:choose>
													          		<xsl:when test="account_program_status = 'Cancelled'">
													            		<td align="right" class="label">Cancellation Date:&nbsp;</td>
													            	</xsl:when>
													            	<xsl:otherwise>
													            		<td align="right" class="label">Expiration Date:&nbsp;</td>
													            	</xsl:otherwise>
													            </xsl:choose>            
													            <td align="left">
													              <xsl:value-of select="expiration_date"/>
													            </td>
													          </tr>
													          <tr>
													            <td align="right" class="label">Auto-renewal Status:&nbsp;</td>
													            <td align="left"><xsl:value-of select="auto_renewal_status"/></td>
													          </tr>
													          <tr>
													            <td align="right" class="label">Program Information:&nbsp;</td>
													            <td align="left">
													              <xsl:variable name="tempUrl" select="program_url"/>
													              <a class="link" href="#" onclick="javascript:doProgramInfoPopup('{$tempUrl}');">
													                <xsl:value-of select="program_url"/>
													              </a>
													            </td>
													          </tr>
													          <!-- <tr>
													            <td align="right" class="label">Services Purchase Order:&nbsp;</td>
													            <td align="left"><xsl:value-of select="external_order_number"/></td>
													          </tr> -->
													          <tr>
													            <td colspan="2"><hr/></td>
													          </tr>
													          <tr>
																		<td class="Label" colspan="2">
																			<xsl:if test="not (position()=last())">
																				<hr width="100%" />
																			</xsl:if>
																		</td>
																	</tr>
											        		</xsl:for-each>
															<xsl:for-each select="SERVICES/SERVICES_INFO">
																	<tr>
																		<td class="Label" width="25%" align="right">Services Product: </td>
																		<td align="left">
																			<xsl:value-of select="display_name"/>
																		</td>
																	</tr>
																	<tr>
																		<td class="Label" width="25%" align="right">Email Address: </td>
																		<td align="left">
																			<xsl:value-of select="email_address" />
																		</td>
																	</tr>
																	<tr>
																		<td class="Label" width="25%" align="right">Status: </td>
																		<td align="left">
																			<xsl:value-of select="account_program_status" />
																		</td>
																	</tr>
																	<tr>
																		<td class="Label" width="25%" align="right">Start Date: </td>
																		<td align="left">
																			<xsl:value-of select="start_date" />
																		</td>
																	</tr>
																	<tr>
																		<xsl:choose>
															          		<xsl:when test="account_program_status = 'Cancelled'">
															            		<td align="right" class="label">Cancellation Date:&nbsp;</td>
															            	</xsl:when>
															            	<xsl:otherwise>
															            		<td align="right" class="label">Expiration Date:&nbsp;</td>
															            	</xsl:otherwise>
															            </xsl:choose>            
																		<td align="left">
																			<xsl:value-of select="expiration_date" />
																		</td>
																	</tr>
																	<!-- <tr>
																		<td class="Label" width="25%" align="right">Program Information: </td>
																		<td align="left">
																			<a>
																				<xsl:attribute name="target">_blank</xsl:attribute>
																				<xsl:attribute name="href"><xsl:value-of
																					select="program_url" /></xsl:attribute>
																				<xsl:value-of select="program_url" />
																			</a>
																		</td>
																	</tr> -->
																	<tr>
																		<td class="Label" width="25%" align="right">Services Purchase Order: </td>
																		<td align="left">
																			<xsl:value-of select="external_order_number" />
																		</td>
																	</tr>
																	<tr>
																		<td class="Label" colspan="2">
																			<xsl:if test="not (position()=last())">
																				<hr width="100%" />
																			</xsl:if>
																		</td>
																	</tr>
																</xsl:for-each>
															</xsl:when>
															<xsl:otherwise>
																<tr>
																	<td colspan="2" align="center" class="label">
																	Email address is not associated with any services
																	</td>
																</tr>
															</xsl:otherwise>
														</xsl:choose>
													</table><!-- End of servicesInfoPopup table -->
												</td>
											</tr>
										</table><!-- End of innerTable table -->
									</td>
								</tr>
							</xsl:when>
						</xsl:choose>
					</table><!-- End of mainTable table -->
					<br />

					<table border="0" width="98%" align="center" cellpadding="0"
						cellspacing="0">
						<tr>
							<td align="center">
								<button type="button" class="BlueButton" accesskey="C"
									onclick="javascript:doCloseAction();">(C)lose</button>
							</td>
						</tr>
					</table><!-- End of button table -->
				</form>
			</body>
		</html>
	</xsl:template>
	<xsl:template name="modDate">
		<xsl:param name="dateValue" />
		<xsl:param name="hackDate" select="substring($dateValue,1,10)" />
		<xsl:value-of
			select="concat(substring($hackDate,6,2),'/',substring($hackDate,9,2),'/',substring($dateValue,1,4))" />
	</xsl:template>
</xsl:stylesheet>