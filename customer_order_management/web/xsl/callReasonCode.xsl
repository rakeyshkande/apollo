
<!DOCTYPE ACDemo[
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0"  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="header.xsl"/>
	<xsl:import href="footer.xsl"/>
	<xsl:import href="securityanddata.xsl"/>
	<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>
	<xsl:output method="html" indent="yes"/>
	<xsl:variable name="call_reason_type" select="crc_data/reason_code/call_reason_type"/>
    <xsl:key name="security" match="/crc_data/security/data" use="name"/>
	
	<xsl:template match="/">
		<html>
			<head>
				<META http-equiv="Content-Type" content="text/html"/>
				<title>Call Reason Code</title>
				<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
				<script type="text/javascript" src="js/clock.js"></script>
				<script type="text/javascript" src="js/util.js"></script>
				<script type="text/javascript" language="JavaScript">
				
				var totalNumber = 0;
<![CDATA[
function init()
{
setNavigationHandlers();
document.getElementById('codeNumber').focus();
}
/*******************************************************
Remove any leading zeros from the code number
********************************************************/
function stripZeros(num){
   var num,newTerm
   while (num.charAt(0) == "0") {
         newTerm = num.substring(1, num.length);
         num = newTerm;
   }
   if (num == "")
       num = "0";
       return num;
}


function doCallReasonCodeAction()
{
  var eve = document.getElementById("codeNumber").value;
  var neweve= stripZeros(eve);
  var flag = 'true';
  if (eve == '')
  {
    alert ('Please Enter a Reason Code');
    document.getElementById('codeNumber').focus();
    flag = 'false';
  }
  else if(neweve <= totalNumber && neweve > 0)
  {
    var x = "call_reason_code" + neweve ;
    var y = "call_reason_code_type" + neweve;
    document.getElementById("call_reason_code").value = document.getElementById(x).value;
    document.getElementById("call_reason_code_type").value = document.getElementById(y).value;
    flag = 'true';
  }
  else
  {
    alert ('This Reason Code does not exist');
    document.getElementById('codeNumber').focus();
    flag = 'false'
  }
  return flag;
}



function Submit()
{
var flag1 = 'true';
var flag = 'true'

flag = doCallReasonCodeAction();
if (flag == 'false' && flag1 == 'true')
flag1 = 'false';

if (flag1 == 'true')
	  {
          document.forms[0].action = "CallReasonCode.do" ;
          document.forms[0].submit();
          }
}

 function entrykey()
    {
            if(window.event.keyCode != 13)
	            return false;
	        else
	        {
	        Submit();
	        }
	 }

function checknum()
     {
     if (event.keyCode < 48 || event.keyCode > 57) event.returnValue = false;
     }

function totalnumber()
   {
    document.getElementById('number').value = totalNumber;
    return totalNumber;
   }


       ]]></script>
			</head>
			<body onload="init();" onkeypress="entrykey();">
				<form >
					<xsl:call-template name="securityanddata"/>
					<xsl:call-template name="decisionResultData"/>
					<input type="hidden" name="call_reason_code" id="call_reason_code" value="{crc_data/reason_code/call_reason_code}"/>
					<input type="hidden" name="call_reason_code_type" id="call_reason_code_type" value="{crc_data/reason_code/call_reason_type}"/>
					<!-- Header-->
					<xsl:call-template name="header">
						<xsl:with-param name="headerName" select="'Call Reason Code'" />
						<xsl:with-param name="dnisNumber" select="$call_dnis"/>
						<xsl:with-param name="brandName" select="$call_brand_name" />
						<xsl:with-param name="indicator" select="$call_type_flag" />
						<xsl:with-param name="cservNumber" select="$call_cs_number" />
						<xsl:with-param name="showBackButton" select="false()"/>
						<xsl:with-param name="showTime" select="true()"/>
						<xsl:with-param name="cdispEnabledFlag" select="$cdisp_enabledFlag"/>
					</xsl:call-template>
					<!-- Display table-->
					<table width="98%" align="center" cellspacing="1" class="mainTable">
						<tr>
							<td>
								<table width="100%" align="center" class="innerTable">
									<tr class="script">
										<td width="48%" align="center"></td>
										<td width="52%" align="center">
											<p>Customer : 
												<xsl:value-of select="crc_data/customer_name"/>
											</p>
											<p>&nbsp;</p>
										</td>
									</tr>
									<tr class="script">
										<td align="center"></td>
										<td align="center"></td>
									</tr>
									<tr valign="top" class="Label">
										<td>
											<table width="100%"  border="0">
												<tr>
													<td width="14%">&nbsp;</td>
													<td colspan="2" class="TopHeading">Customer / Recipient Reason Codes </td>
												</tr>
												<xsl:for-each select="crc_data/reason_code">
													<script>
                                                       totalNumber += 1;
													</script>
													<xsl:if test="call_reason_type = 'Customer'">
														<tr class="script">
															<td>&nbsp;</td>
															<input type="hidden" name="call_reason_code{@num}" id="call_reason_code{@num}" value="{call_reason_code}"/>
															<input type="hidden" name="call_reason_code_type{@num}" id="call_reason_code_type{@num}" value="{call_reason_type}"/>
															<td width="25%">
																<div align="right">
																	<xsl:value-of select="@num"/>&nbsp;&nbsp;
																</div>
															</td>
															<td width="61%">
																<xsl:value-of select="description"/>
															</td>
														</tr>
													</xsl:if>
												</xsl:for-each>
											</table>
											<td>
												<table width="100%"  border="0">
													<tr>
														<td width="4%">&nbsp;</td>
														<td colspan="2" class="TopHeading">&nbsp;&nbsp;&nbsp;Florist Reason Codes</td>
													</tr>
													<xsl:for-each select="crc_data/reason_code">
														<xsl:if test="call_reason_type = 'Florist'">
															<tr class="script">
																<td>&nbsp;</td>
																<input type="hidden" name="call_reason_code{@num}" id="call_reason_code{@num}" value="{call_reason_code}"/>
																<input type="hidden" name="call_reason_code_type{@num}" id="call_reason_code_type{@num}" value="{call_reason_type}"/>
																<td width="13%">
																	<div align="right">
																		<xsl:value-of select="@num"/>&nbsp;&nbsp;
																	</div>
																</td>
																<td width="83%">
																	<xsl:value-of select="description"/>
																</td>
															</tr>
														</xsl:if>
													</xsl:for-each>
												</table>
												<input type="hidden" id="number" name="number" value="" />
												<script>
                                                  totalnumber();
												</script>
											</td>
											<tr class="Label">
												<td colspan="3" align="center">
													<p>&nbsp;</p>
													<p>&nbsp;</p>
													<span class="label">Enter Reason Code Here: </span>&nbsp;&nbsp;&nbsp;&nbsp;
													<input name="textfield" type="text" id="codeNumber" size="5" maxlength="3" onkeypress="checknum()"/>
												</td>
											</tr>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					<table width="98%" border="0" align="center" cellpadding="0" cellspacing="1">
						<tr>
							<td width="10%">
								<div align="center"></div>
							</td>
							<td width="80%">
								<div align="center">
								     <button type="button" class="BlueButton" accesskey="S" onclick="Submit();">(S)ubmit</button>
								</div>
							</td>
							<td width="10%" align="right">
							</td>
						</tr>
					</table>
					<!--Copyright bar-->
					<xsl:call-template name="footer"/>
				</form>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>