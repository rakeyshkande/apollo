<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="securityanddata.xsl"/>
<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>

<xsl:key name="pageData" match="/root/pageData/data" use="name"/>
<xsl:key name="security" match="/root/security/data" use="name"/>
	
<xsl:output method="html" indent="yes"/>
<xsl:template match="/root">
<html>
<head>
<script type="text/javascript" language="javascript">
var message = '<xsl:value-of select="key('pageData','message_display')/value"/>';
var queueId = '<xsl:value-of select="key('pageData','queue_id')/value"/>';
var recipZipCode = '<xsl:value-of select="key('pageData','recip_zip_code')/value"/>';

<![CDATA[
   function queueCheck()
   {	
    if(message == '' )
    {    /* This function must be implemented on any xsl that will use the checkLockIFrame.xsl */
      parent.doObtainLockAndProceedToQueue(queueId, recipZipCode);
    }
    else
    {
       alert(message);
    }
   }
]]>
</script>
</head>
<body onload="javascript:queueCheck();">
    <xsl:call-template name="securityanddata"/>   
    <xsl:call-template name="decisionResultData"/>
</body>
</html>
</xsl:template>
</xsl:stylesheet>