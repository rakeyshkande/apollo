<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="securityanddata.xsl"/>
<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>
<xsl:key name="security" match="/root/security/data" use="name"/>
<xsl:output method="html" indent="yes"/>
<xsl:template match="/root">
<html>

<head>
<script type="text/javascript" language="javascript">
function reloadCart()
{
	var ret = new Array();
	ret[0] = 'REFRESH';
	top.returnValue = ret;
  	top.close();
}
</script>
</head>

<body onload="javascript:reloadCart();">
</body>
</html>
</xsl:template>
</xsl:stylesheet>