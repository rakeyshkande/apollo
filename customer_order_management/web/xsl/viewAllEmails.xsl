<!DOCTYPE ACDemo[
	<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="header.xsl"/>
<xsl:import href="footer.xsl"/>
<xsl:import href="securityanddata.xsl"/>
<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>

<xsl:variable name="total_records" select="key('pageData','uce_customer_email_count')/value"/>
<xsl:variable name="total_pages" select="key('pageData','uce_total_pages')/value"/>
<xsl:variable name="current_page" select="key('pageData','uce_current_page')/value"/>
<xsl:variable name="start_position" select="key('pageData','uce_start_position')/value"/>
<xsl:output doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"/>

<xsl:key name="pageData" match="/root/pageData/data" use="name"/>
<xsl:key name="security" match="/root/security/data" use="name"/>
<xsl:output indent="yes" method="html"/>
<xsl:template match="/root">
<base target="_self"/>
<html>
  <head>
	<title>FTD - View Email Addresses</title>
	<script type="text/javascript" src="js/util.js"/>
	<script type="text/javascript" src="js/FormChek.js"/>
	<link rel="stylesheet" href="css/ftd.css"/>
	<style type="text/css">
		.numberPosition{
			text-align:right;
			padding-right:.7em;
		}
		 /* Scrolling table overrides for the order/shopping cart table */
			div.tableContainer table {
				width: 97%;
			}
			div#messageContainer {
				height: 130px;
				width: 100%;
			}
			tbody.scrollContent td{
				text-align:left;
			}
	</style>
	<script type="text/javascript">
	var cPage = '<xsl:value-of select="key('pageData','uce_current_page')/value"/>';
	var pageAction = new Array("uce_first","uce_previous","uce_next","uce_last");
	var mysteryBox = new Array('numberentry');
    var show_previous = "<xsl:value-of select="key('pageData','uce_show_previous')/value"/>";
    var show_next     = "<xsl:value-of select="key('pageData','uce_show_next')/value"/>";
    var show_first    = "<xsl:value-of select="key('pageData','uce_show_first')/value"/>";
    var show_last     = "<xsl:value-of select="key('pageData','uce_show_last')/value"/>";
   	var pos = "<xsl:value-of select="$start_position"/>";
    var returnParms = new Array('uce_current_page','display_page','uce_max_records_allowed',
    			'uce_total_records','uce_start_position','uce_total_pages');
    var ret;
	<![CDATA[
  
/*
   Disables the various navigation keyboard commands.  Overwrites backKeyHandler
   in util.js because we need to run the checkKey event.
*/
function backKeyHandler()
{
    checkKey();
   // backspace
   if (window.event && window.event.keyCode == 8)
   {
			//when the control is on the page instead of an element like text box, we want to suppress the
			//back space.  This would prevent the users from hitting Alt+Backspace and would force them to
			//use the BACK button provided.
      var formElement = false;

			//get the form
			var testForm = document.forms[0];

			//if the page actually had a form, go in the loop.
			if (testForm != null)
			{
				for(i = 0; i < document.forms[0].elements.length; i++)
				{
					 if(document.forms[0].elements[i].name == document.activeElement.name)
					 {
							formElement = true;
							break;
					 }
				}
			}
			//else check if the activeElement is a Text or TextArea that is part of the body
			else if (document.activeElement.type == "text" || document.activeElement.type == "textarea")
			{
				formElement = true;
			}


      var searchBox = document.getElementById('numberEntry');
      if(searchBox != null){
        if(document.getElementById('numberEntry').id == document.activeElement.id){
          formElement = true;
        }
      }

      if(formElement == false || (document.activeElement.type != "text" && document.activeElement.type != "textarea" && document.activeElement.type != "password")){
         window.event.cancelBubble = true;
         window.event.returnValue = false;
         return false;
      }
   }

   // F5 and F11
   if (window.event.keyCode == 122 || window.event.keyCode == 116){
      window.event.keyCode = 0;
      event.returnValue = false;
      return false;
   }

    
  /*
   // Alt + Left Arrow
   if(window.event.altLeft){
      window.event.returnValue = false;
      return false;
   }

   // Alt + Right Arrow
   if(window.event.altKey){
      window.event.returnValue = false;
      return false;
   }
   */
}
  
function init(){
   setNavigationHandlers();
   addDefaultListenersArray(mysteryBox);
   setAllEmailsTabIndexes();
}

function setAllEmailsTabIndexes(){
 var count = document.getElementById('uce_customer_email_count').value;
 
  document.getElementById('numberEntry').tabIndex = 1;
  document.getElementById('numberEntry').focus();
  document.getElementById('updateEmail').tabIndex = (count + 1);
  document.getElementById('addEmail').tabIndex = (count + 2);
  document.getElementById('close').tabIndex = (count + 3);
  
  var buttons = new Array('firstItem','backItem','nextItem','lastItem');
  for(var i = 4; i < buttons.length; i++){
    var but =  document.getElementById(buttons[i]);
    if(but)
      but.tabIndex = (count + i);
  }
}

/*
 Handles the page navigation for this page.
*/
function doPageAction(val){
 var url = "updateCustomerEmail.do" + "?action=" +
		pageAction[val] + getSecurityParams(false);
	url += retrieveParms();
performAction(url);
}

/*
	Opens the 'Update Email Status' modal dialog window
*/
function doUpdateEmailStatusAction(pos){
   if(pos == -1){
		if(document.getElementById('numberEntry').value == ''){
			alert('Please enter a line number.');
			return;
		}
 	  doEntryAction();
	}
	if(isInteger(pos) && pos != -1){
		var td = document.getElementById('td_' + pos);
		doUpdateEmailStatus(td.childNodes(1).value, pos);
	}
}

/*
 This function handles the actual call(request) to
 the server in order to call the Update Email Status page
*/
function doUpdateEmailStatus(id,num){
 var emailAddress = getEmailAddress(num);
  if(emailAddress != null && emailAddress != '') {
	 var url = '';
	 url = 'updateCustomerEmail.do?action=get_email_info' +
	 		'&uc_customer_id=' + id +
	 		'&uce_email_id='    + document.getElementById('email_id_'+ num).value +
	 		'&uce_email_address=' +  emailAddress  + '&page_action=update' +
	 		 getSecurityParams(false);
	 var modalFeatures  =  'dialogWidth:800px;dialogHeight:420px;center:yes;status:no;help:no;';
	      modalFeatures +=  'resizable:no;scroll:no';
 	url + retrieveParms();
	  ret =  showModalDialogIFrame(url, "", modalFeatures);
      refreshPage();
  }else{
  	alert('Please provided an email address.');
  }
}


function refreshPage(){
	var url = 'updateCustomerEmail.do?action=more_customer_emails' +
  			 getSecurityParams(false) + reloadParms();
  performAction(url);
}

function reloadParms(){
  var url = '';
	for(var i=0; i < returnParms.length; i++){
		url += '&uc_' + document.getElementById(returnParms[i]).name + '=' +
			document.getElementById(returnParms[i]).value;
	}

  url += '&uc_master_order_number=' + document.getElementById('master_order_number').value +
         '&uc_order_guid=' + document.getElementById('order_guid').value +
         '&uc_customer_id=' + document.getElementById('uc_customer_id').value;
  return url;

}

/*
 Returns parameters to the server based
 on the fields included in the returnParms array
*/
function retrieveParms(){
 var url = '';
	for(var i=0; i < returnParms.length; i++){
		url += '&' + document.getElementById(returnParms[i]).name + '=' +
			document.getElementById(returnParms[i]).value;
	}
  return url;
}

/*

*/
function getEmailAddress(num){
 var td = document.getElementById('email' + num);
  if(td != null)
  	var mail = td.childNodes[0].nodeValue;
 return (mail);
}

function doAddEmailAddress(){
	var url = '';
	url = 'updateCustomerEmail.do?action=add_customer_email&page_action=add' +
		'&uc_customer_id=' + document.getElementById('uc_customer_id').value +
	    retrieveParms() + getSecurityParams(false);
	 var modalFeatures  =  'dialogWidth:800px;dialogHeight:420px;center:yes;status:no;help:no;';
	      modalFeatures +=  'resizable:no;scroll:no';

  ret =  showModalDialogIFrame(url, "", modalFeatures);
   refreshPage();
}

/*
 This function recieves the information
 associated with a csr typing in the mystery box
*/
function doSearchAction(id,num){
	doUpdateEmailStatus(id,num);
}


function checkKey()
{
document.getElementById('numberentry').focus()
var tempKey = window.event.keyCode;
  //Left arrow pressed
  if (window.event.altKey && tempKey == 37 && show_first == 'y')
  {
    doNavigationAction(0);
  }
  //Right arrow pressed
  if (window.event.altKey && tempKey == 39 && show_last == 'y')
  {
    doNavigationAction(3);
  }
  //Down arrow pressed
  if (window.event.altKey && tempKey == 40 && show_next == 'y')
  {
    doNavigationAction(2);
  }

  //Up arrow pressed
  if (window.event.altKey && tempKey == 38 && show_previous == 'y')
  {
    doNavigationAction(1);
  }

if (event.srcElement.value == '' && tempKey == 13)
    alert('Please enter a line number');

}

function doNavigationAction(val){
	var url = "updateCustomerEmail.do" + "?action=" + pageAction[val];
 url += retrieveParms();
performAction(url);
}

function doCloseAction(){
  ret = new Array();
  top.returnValue = ret
  top.close();
}
/*
This function is used to retrieve the fields
from security and data which have df_ pre-pended to them.
These fields are used to correctly refresh the
updateCustomerAccount page
*/
/*
function getSADRefreshValues(){
 var arr = new Array();
 var elems = new Array();
 var count = 0;

 var form = document.forms[0];
 for(var i = 0; i < form.elements.length; i++)
 {
  if(form.elements[i].type == 'hidden' && form.elements[i].id.substr(0,3) == 'df_' )
  {
  	elems[count] = form.elements[i].id;
  	count++;
  }
 }
 for(var k = 0; k < elems.length; k++)
 {
   arr[k] = new Array();
   arr[k][0] = elems[k];
   arr[k][1] = document.getElementById(elems[k]).value;
 }
 return arr;
}
*/
function performAction(url){
 document.forms[0].action = url;
 document.forms[0].submit();
}
		]]>
	</script>
  </head>
  <body onload="javascript:init();" onkeypress="javascript:checkKey();" >
	<xsl:call-template name="addHeader"/>
	<form name="vaea" method="post" action="">
		<iframe id="uceStatus" width="0px" height="0px" border="0" />
		<xsl:call-template name="securityanddata" />

	<input type="hidden" name="uce_max_records_allowed" 	id="uce_max_records_allowed" 		value="{key('pageData','max_records_allowed')/value}"/>
	<input type="hidden" name="display_page" 				id="display_page" 					value="{key('pageData','display_page')/value}"/>
	<input type="hidden" name="uce_current_page" 			id="uce_current_page" 				value="{key('pageData','uce_current_page')/value}"/>
	<input type="hidden" name="uce_total_records" 			id="uce_total_records" 				value="{key('pageData','uce_total_records')/value}"/>
	<input type="hidden" name="uce_start_position" 			id="uce_start_position" 			value="{key('pageData','uce_start_position')/value}"/>
	<input type="hidden" name="uce_total_pages" 			id="uce_total_pages" 				value="{key('pageData','uce_total_pages')/value}"/>
	<input type="hidden" name="master_order_number" 		id="master_order_number" 			value="{key('pageData','master_order_number')/value}"/>
	<input type="hidden" name="order_guid" 					id="order_guid" 					value="{key('pageData','order_guid')/value}"/>
	<input type="hidden" name="uc_customer_id" 				id="uc_customer_id" 				value="{key('pageData','customer_id')/value}" />
	<input type="hidden" name="uce_customer_email_count"	id="uce_customer_email_count"		value="{key('pageData','uce_customer_email_count')/value}"/>

		 <xsl:if test="key('pageData','message_display')/value != ''">
		   <table border="0" cellpadding="0" cellspacing="0" width="98%" align="center">
			   <tr>
				   <td class="largeErrorMsgTxt">
						<xsl:value-of select="key('pageData','message_display')/value"/>
				   </td>
			   </tr>
		   </table>
		   </xsl:if>
		<table class="mainTable" align="center" border="0" cellpadding="0" cellspacing="1" width="98%" height="50%">
			<tr>
				<td>
					<table border="0" align="center" class="innerTable"  cellpadding="0" cellspacing="1" width="100%" height="100%">
						<tr>
							<td valign="top" colspan="2">
								<!-- This table contains the fields of the page -->
								 <div class="tableContainer" id="messageContainer" >
									   <table width="100%" class="scrollTable" border="0" cellpadding="0"  align="center" cellspacing="2">
										 <thead class="fixedHeader">
						                  	<tr style="text-align:left">
	                                            <td width="18%" colspan="2"></td>
												<td class="Label" >	Email Address</td>
												<td class="Label">Modified Date</td>
								            </tr>
								            </thead>
								             <tbody class="scrollContent" >
												<xsl:for-each select="UCE_CUSTOMER_EMAILS/UCE_CUSTOMER_EMAIL">
													<tr>
														<td style="text-align:right;padding-right:.5em;" >
												 	          <script type="text/javascript">
															 	<![CDATA[
															 	  document.write(pos++ + ".");
													 			]]>
													 			</script>

														</td>
														<td id="td_{position()}" >
															<input type="hidden" />
															<input type="hidden" name="uc_customer_{position()}" id="uc_customer_{position()}" value="{key('pageData','customer_id')/value}" />
														</td>
														<td id="emaiLink_{position()}">
															<a id="email{position()}" tabindex="{position() + 1}" href="javascript:doUpdateEmailStatusAction({position()});" class="textlink"><xsl:value-of select="email_address"/></a>
															<input type="hidden" name="email_id_{position()}" id="email_id_{position()}" value="{email_id}"/>
														</td>
														<td align="left"><xsl:value-of select="updated_on"/></td>
													</tr>
												</xsl:for-each>
											</tbody>
										</table>
									 </div>

							</td>
						</tr>
						<tr>
							<td class="Label" width="50%" >
					       		&nbsp;Page &nbsp; <xsl:value-of select="$current_page"/>&nbsp; of &nbsp;
					       		<xsl:choose>
					       			<xsl:when test="$total_pages = '0'">
					       				<xsl:value-of select="'1'"/>
					       			</xsl:when>
					       			<xsl:otherwise>
					       				<xsl:value-of select="$total_pages"/>
					       			</xsl:otherwise>
					       		</xsl:choose>
					       </td>
					       <td class="Label" align="right" width="50%">
								<xsl:value-of select="$total_records"/>&nbsp; Records Found&nbsp;
					       </td>
						</tr>
					</table> <!-- inner table -->
				</td>
			</tr>
		</table><!-- main table -->
		<table border="0" align="center" cellpadding="0" cellspacing="0" width="98%">
			<tr>
				<td width="80%" align="center">
				<button type="button" class="BigBlueButton" id="updateEmail" accesskey="U" onclick="javascript:doUpdateEmailStatusAction(-1);">(U)pdate Email<br />Status</button>&nbsp;
				<button type="button" class="BigBlueButton" id="addEmail" accesskey="A" onclick="javascript:doAddEmailAddress();">(A)dd New Email<br />Address</button>&nbsp;

					<button type="button" class="BigBlueButton" id="close" accesskey="C" onclick="javascript:doCloseAction();">(C)lose</button>
				</td>


				<td align="right" style="vertical-align:top;">
 				 <xsl:choose>
                   <xsl:when test="key('pageData','uce_show_first')/value ='y'">
                     <button type="button" name="firstItem" tabindex="3"  class="arrowButton" id="firstItem" onclick="doPageAction(0);">
                      7</button>
                   </xsl:when>
                   <xsl:when test="key('pageData','uce_show_first')/value ='n'">
                     <button type="button" name="firstItem2" tabindex="3"  class="arrowButton" id="firstItem2" onclick="">
                     	<xsl:attribute name="disabled">true</xsl:attribute>
                     7</button>
                   </xsl:when>
                  </xsl:choose>


                  <xsl:choose>
                     <xsl:when test="key('pageData','uce_show_previous')/value ='y'">
                         <button type="button" name="backItem" tabindex="1" class="arrowButton"  id="backItem" onclick="doPageAction(1);">
                         3</button>
                     </xsl:when>
                     <xsl:when test="key('pageData','uce_show_previous')/value ='n'">
                         <button type="button" name="backItem2" tabindex="1" class="arrowButton" id="backItem2" onclick="">
                        	<xsl:attribute name="disabled">true</xsl:attribute>
                         3</button>
                     </xsl:when>
                   </xsl:choose>


			       <xsl:choose>
                       <xsl:when test="key('pageData','uce_show_next')/value ='y'">
                          <button type="button" name="nextItem" tabindex="2" class="arrowButton"  id="nextItem" onclick="doPageAction(2);">
                          4</button>
                       </xsl:when>
                       <xsl:when test="key('pageData','uce_show_next')/value ='n'">
                          <button type="button" name="nextItem2" tabindex="2"  class="arrowButton" id="nextItem2" onclick="">
                          	<xsl:attribute name="disabled">true</xsl:attribute>
                          4</button>
                        </xsl:when>
                  </xsl:choose>

				    <xsl:choose>
                       <xsl:when test="key('pageData','uce_show_last')/value ='y'">
                         <button type="button" name="lastItem" tabindex="4" class="arrowButton"  id="lastItem" onclick="doPageAction(3);">
                         8</button>
                       </xsl:when>
                       <xsl:when test="key('pageData','uce_show_last')/value ='n'">
                         <button type="button" name="lastItem2" tabindex="4" class="arrowButton"  id="lastItem2" onclick="">
                          	<xsl:attribute name="disabled">true</xsl:attribute>
                         8</button>
                       </xsl:when>
                   </xsl:choose>
				</td>
		   </tr>
       </table>
	</form>
  </body>
</html>

</xsl:template>
<xsl:template name="addHeader">
	<xsl:call-template name="header">
		<xsl:with-param name="showPrinter" select="true()"/>
		<xsl:with-param name="headerName" select="'Email Addresses'"/>
		<xsl:with-param name="showTime" select="true()"/>
		<xsl:with-param name="showBackButton" select="false()"/>
		<xsl:with-param name="showSearchBox" select="true()"/>
		<xsl:with-param name="searchLabel" select="'Enter Line#'"/>
		<xsl:with-param name="showCSRIDs" select="true()"/>
    <xsl:with-param name="dnisNumber" select="$call_dnis"/>
    <xsl:with-param name="cservNumber" select="$call_cs_number"/>
    <xsl:with-param name="indicator" select="$call_type_flag"/>
    <xsl:with-param name="brandName" select="$call_brand_name"/>
    <xsl:with-param name="cdispEnabledFlag" select="$cdisp_enabledFlag"/>
	</xsl:call-template>
</xsl:template>
</xsl:stylesheet>