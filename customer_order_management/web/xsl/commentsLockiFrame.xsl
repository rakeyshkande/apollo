<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" indent="yes"/>

<xsl:template match="/">

<html>
<head>
<script type="text/javascript" language="javascript">

  loadLockContent();

  function loadLockContent() {
    <xsl:if test="/root/out-parameters/OUT_LOCK_OBTAINED = 'Y'">
      parent.document.getElementById("comment_text").disabled=false;     
      parent.document.getElementById("comment_text").focus();   
      parent.document.getElementById("saveAction").disabled=false;
      parent.document.getElementById("addComment").disabled=true;
      if(parent.document.getElementById("shoppingCart")!=null)
        parent.document.getElementById("shoppingCart").disabled=false;
        replaceText("lockError", "");
    </xsl:if>
    <xsl:if test="/root/out-parameters/OUT_LOCK_OBTAINED = 'N'">
      parent.document.getElementById("comment_text").disabled=true;
      replaceText("lockError", "Cannot add new comment. Current record is locked by <xsl:value-of select='/root/out-parameters/OUT_LOCKED_CSR_ID'/>");
    </xsl:if>
  }
  
function replaceText( elementId, newString )
  {
   var node = parent.document.getElementById( elementId );   
   var newTextNode = parent.document.createTextNode( newString );
   if(node.childNodes[0]!=null)
   {
    node.removeChild( node.childNodes[0] );
   }
   node.appendChild( newTextNode );
  }
</script>
</head>
<body>
</body>
</html>

</xsl:template>
</xsl:stylesheet>