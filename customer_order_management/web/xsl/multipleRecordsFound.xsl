<!DOCTYPE ACDemo [
 	<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<!--External Templates -->
<xsl:import href="header.xsl"/>
<xsl:import href="footer.xsl"/>
<xsl:import href="securityanddata.xsl"/>
<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>
<xsl:output doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"/>
<xsl:output method="html" indent="yes"/>

<!-- Keys -->
<xsl:key name="security" match="/root/security/data" use="name"/>
<xsl:key name="pageData" match="/root/pageData/data" use="name"/>
<xsl:key name="searchCriteria" match="/root/searchCriteria/criteria" use="name"/>

<xsl:variable name="total_records" select="key('pageData','cos_total_records_found')/value"/>
<xsl:variable name="cos_current_page" select="key('pageData','cos_current_page')/value"/>
<xsl:variable name="cos_total_pages" select="key('pageData','cos_total_pages')/value"/>
<xsl:variable name="cos_start_position" select="key('pageData','cos_start_position')/value"/>


<xsl:variable name="YES" select="'Y'"/>
<xsl:variable name="NO" select="'N'"/>

 <xsl:template match="/">
	<html>
		<head>
			<title>FTD - Multiple Records Found</title>
			<link rel="stylesheet" href="css/ftd.css"/>
			<style type="text/css">
		    /* Scrolling table overrides for the order/shopping cart table */
			div.tableContainer table {
				width: 97%;
			}
			div#messageContainer {
				height: 130px;
				width: 100%;
			}
			</style>
			<script type="text/javascript" src="js/tabIndexMaintenance.js"/>
			<script type="text/javascript" src="js/util.js"/>
			<script type="text/javascript" src="js/clock.js"/>
			<script type="text/javascript" src="js/commonUtil.js"/>
			<script type="text/javascript" src="js/FormChek.js"/>
			<script type="text/javascript" language="javascript">

			var pageAction = new Array("first_page","previous_page","next_page","last_page");
			var pos = "<xsl:value-of select="$cos_start_position"/>";
			var cPage = "<xsl:value-of select="$cos_current_page"/>";
                        var cos_show_previous = "<xsl:value-of select="key('pageData','cos_show_previous')/value"/>";
                        var cos_show_next     = "<xsl:value-of select="key('pageData','cos_show_next')/value"/>";
                        var cos_show_first    = "<xsl:value-of select="key('pageData','cos_show_first')/value"/>";
                        var cos_show_last     = "<xsl:value-of select="key('pageData','cos_show_last')/value"/>";
                        var transfer_number  = "<xsl:value-of select="key('pageData','transferNumber')/value"/>";
                        mysteryBox = new Array('numberEntry');

			<![CDATA[

/***********************************************************************************
* init()
************************************************************************************/
 			function init(){
                            setNavigationHandlers();
                            setMultipleRecordsFoundIndexes();
                            addDefaultListenersArray(mysteryBox);
                            document.getElementById('numberEntry').focus();
 			}


/*******************************************************************************
*  setMultipleRecordsFoundIndexes()
*  Sets tabs for Multiple Records Found Page
*  See tabIndexMaintenance.js
*******************************************************************************/

      function  setMultipleRecordsFoundIndexes()
      {
       untabAllElements();
        var tabOrder = new Array('numberEntry', 'nextItem', 'backItem', 
          'firstItem', 'lastItem', 'backSearchButton', 'mainMenuButton', 
          'printer');
        setTabIndexes(tabOrder, findBeginIndex());
      }

/***********************************************************************************
* doPageAction()
************************************************************************************/
      function doPageAction(val){
        if(window.event)
         if(window.event.keyCode)
          if(window.event.keyCode != 13)
            return false;
        var url = "customerOrderSearch.do" + "?action=" + pageAction[val];
        performAction(url);
      }

/***********************************************************************************
* doNavigationAction()
************************************************************************************/
			function doNavigationAction(val){
				var url = "customerOrderSearch.do" + "?action=" + pageAction[val];
				performAction(url);
			}


/***********************************************************************************
* canRepAccessOrder()
************************************************************************************/
			function canRepAccessOrder(id,num,preferredCustomer,ftdCustomer,preferredRecipient,ftdRecipient,preferredWarningMessage){
                            var repHasPermission = document.getElementById('rep_has_access'+num).value;
                            if ( preferredCustomer == 'Y' && ftdCustomer == 'N' && repHasPermission == 'N')
                            {
                                 var modalArguments = new Object();
                                 modalArguments.modalText = preferredWarningMessage;
                                 window.showModalDialog("alert.html", modalArguments, 'dialogWidth:300px; dialogHeight:200px; center:yes; status=no; help=no; resizable:no; scroll:yes');
                            }
                            else if ( preferredRecipient == 'Y' && ftdRecipient == 'N' && repHasPermission == 'N')
                            {
                                 var modalArguments = new Object();
                                 modalArguments.modalText = preferredWarningMessage;
                                 window.showModalDialog("alert.html", modalArguments, 'dialogWidth:300px; dialogHeight:200px; center:yes; status=no; help=no; resizable:no; scroll:yes');
                            }
                            else
                            {
                                doCustomerSearchAction(id,num);
                            }
			}

/***********************************************************************************
* doCustomerSearchAction()
************************************************************************************/
			function doCustomerSearchAction(id,num){
				var url = "customerOrderSearch.do" +
							"?action=customer_search" + "&customer_id=" + id;
				var lastODate = document.getElementById('lastOrderDate'+num).value;
				if(lastODate != '') url += "&last_order_date=" + lastODate;
				var recipOrderNum = document.getElementById('recip_order_number'+num).value;
				if(recipOrderNum != '') url += "&order_number=" + recipOrderNum;

			  performAction(url);
			}

/***********************************************************************************
* performAction()
************************************************************************************/
			function performAction(url){
        scroll(0,0);
        showWaitMessage("content", "wait", "Searching");
				document.forms[0].action = url;
			  document.forms[0].submit();
			}

/***********************************************************************************
* checkKey()
************************************************************************************/
function checkKey()
{
  var tempKey = window.event.keyCode;

  //Left arrow pressed
  if (window.event.altKey && tempKey == 37 && cos_show_first == 'y')
    doNavigationAction(0);


  //Right arrow pressed
  if (window.event.altKey && tempKey == 39 && cos_show_last == 'y')
    doNavigationAction(3);


  //Down arrow pressed
  if (window.event.altKey && tempKey == 40 && cos_show_next == 'y')
    doNavigationAction(2);


  //Up arrow pressed
  if (window.event.altKey && tempKey == 38 && cos_show_previous == 'y')
    doNavigationAction(1);


}

function doLossPreventionSearchAction() {
    var url = "customerOrderSearch.do?action=load_lpsearch_page";
        performAction(url);
}


function doEntryAction()
{
  if(window.event)
    if(window.event.keyCode)
      if(window.event.keyCode != 13)
        return false;

  var eve = event.srcElement.value;

  if(isInteger(eve))
  {
    var searchNumber = doRowCalculation(eve);

    if(document.getElementById('recip_order_number' + searchNumber)==null)
    {
      alert('Invalid line number entered.' + '\n' +
      'Please only enter a line number that is currently being displayed on the page.');
    }
    else
    {
      canRepAccessOrder(document.getElementById('customer_id'+searchNumber).value, searchNumber, document.getElementById('preferred_customer'+searchNumber).value, document.getElementById('ftd_customer'+searchNumber).value, document.getElementById('preferred_recipient'+searchNumber).value, document.getElementById('ftd_recipient'+searchNumber).value, document.getElementById('preferred_warning_message'+searchNumber).value);
    }
  }
}

		/*************************************************************************************
		*	Perorms the Back button functionality
		**************************************************************************************/
		function doBackAction()
		{
		  	var url = "customerOrderSearch.do?action=load_page";
			performAction(url);
		}

			]]></script>
		</head>
    <body onload="javascript:init();" onkeypress="javascript:evaluateKeyPress();"  onkeydown="javascript:checkKey();">
			<xsl:call-template name="addHeader" />
		  <form name="multRec" method="post">
			<input type="hidden" name="cos_current_page" id="cos_current_page" value="{key('pageData','cos_current_page')/value}"/>
			<input type="hidden" name="cos_total_pages" id="cos_total_pages" value="{key('pageData','cos_total_pages')/value}"/>
			<input type="hidden" name="total_records_found" id="total_records_found" value="{key('pageData','cos_total_records_found')/value}"/>
			<input type="hidden" name="begin_index" id="begin_index" value="{key('pageData','begin_index')/value}"/>
			<input type="hidden" name="end_index" id="end_index" value="{key('pageData','end_index')/value}"/>
			<input type="hidden" name="max_search_results" id="max_search_results" value="{key('pageData','max_search_results')/value}"/>
			
			<input type="hidden" name="max_records_allowed" id="max_records_allowed" value="{key('pageData','max_records_allowed')/value}"/>
  		<input type="hidden" name="master_order_number" id="master_order_number" value="{key('pageData','master_order_number')/value}"/>
      <xsl:call-template name="securityanddata"/>
      <xsl:call-template name="decisionResultData"/>
		<div id="content" style="display:block">
			<!--  Main Table thick blue border -->
			   <table class="mainTable" align="center" width="98%" cellpadding="0" cellspacing="1">
				   <tr>
					   <td><!-- Inner table used to show thin blue border -->
						   <table class="innerTable" cellpadding="0" cellspacing="0" width="100%" border="0">
							   <tr>
								   <td colspan="2">
								     <div class="tableContainer" id="messageContainer" >
									   <table width="100%" class="scrollTable" border="0" cellpadding="0"  align="center" cellspacing="2">
										 <thead class="fixedHeader">
						                  	<tr style="text-align:left">
								              <td  class="ColHeader" width="2%"  ></td>
								              <td  class="ColHeader" width="23%">&nbsp;Last Name</td>
								              <td  class="ColHeader" width="19%">&nbsp;First Name</td>
								              <td  class="ColHeader" width="25%">&nbsp;Address</td>
								              <td  class="ColHeader" width="15%">&nbsp;City</td>
								              <td  class="ColHeader" width="5%">&nbsp;State</td>
								              <td  class="ColHeader" width="12%">&nbsp;Last Order<br/>&nbsp;Date</td>
								            </tr>
								            </thead>
								             <tbody class="scrollContent" >
												<xsl:for-each select="root/SC_SEARCH_CUSTOMERS/SC_SEARCH_CUSTOMER">
                                                                                                    <tr style="vertical-align:text-top;" id="row">
													 <td width="23px" style="text-align:left;">
													 	<script type="text/javascript">
													 	<![CDATA[
													 	  document.write(pos++ + ".");

											 			]]></script>
												 	 </td>
                                                                                                         <td width="147pt" id="td_{@num}" style="text-align:left;">
                                                                                                                 <xsl:choose>
                                                                                                                        <xsl:when test="preferred_customer = 'Y' and ftd_customer = 'N' and preferred_masked_name != '' and key('pageData', preferred_resource)/value = 'N'">
                                                                                                                                <a href="javascript:canRepAccessOrder('{customer_id}','{@num}','{preferred_customer}','{ftd_customer}','{preferred_recipient}','{ftd_recipient}','{preferred_warning_message}');" class="textlink" style="color:blue;font-weight:bold">
                                                                                                                                    <xsl:value-of select="preferred_masked_name"/>
                                                                                                                                </a>
                                                                                                                        </xsl:when>
                                                                                                                        <xsl:when test="preferred_recipient = 'Y' and ftd_recipient = 'N' and preferred_masked_name != '' and key('pageData', preferred_resource)/value = 'N'">
                                                                                                                                <a href="javascript:canRepAccessOrder('{customer_id}','{@num}','{preferred_customer}','{ftd_customer}','{preferred_recipient}','{ftd_recipient}','{preferred_warning_message}');" class="textlink" style="color:blue;font-weight:bold">
                                                                                                                                    <xsl:value-of select="preferred_masked_name"/>
                                                                                                                                </a>
                                                                                                                        </xsl:when>
                                                                                                                        <xsl:otherwise>
                                                                                                                                <a href="javascript:doCustomerSearchAction('{customer_id}','{@num}');" class="textlink">
                                                                                                                                    <xsl:value-of select="last_name"/>
                                                                                                                                </a>
                                                                                                                        </xsl:otherwise>
                                                                                                                </xsl:choose>
                                                                                                                <input type="hidden" id="customer_id{@num}" value="{customer_id}"/>
                                                                                                                <input type="hidden" id="recip_order_number{@num}" value="{order_number}"/>
                                                                                                                <input type="hidden" id="preferred_customer{@num}" value="{preferred_customer}"/>
														<input type="hidden" id="ftd_customer{@num}" value="{ftd_customer}"/>
                                                                                                                <input type="hidden" id="preferred_recipient{@num}" value="{preferred_recipient}"/>
														<input type="hidden" id="ftd_recipient{@num}" value="{ftd_recipient}"/>
                                                                                                                <input type="hidden" id="preferred_warning_message{@num}" value="{preferred_warning_message}"/>
                                                                                                                <input type="hidden" id="rep_has_access{@num}" value="{key('pageData', preferred_resource)/value}"/>
												  	  </td>
                                                                                                                <xsl:choose>
                                                                                                                        <xsl:when test="preferred_customer = 'Y' and ftd_customer = 'N' and preferred_masked_name != '' and key('pageData', preferred_resource)/value = 'N'">
                                                                                                                                <td width="142pt" style="text-align:left;color:blue;font-weight:bold"><xsl:value-of select="preferred_masked_name"/></td>
                                                                                                                        </xsl:when>
                                                                                                                        <xsl:when test="preferred_recipient = 'Y' and ftd_recipient = 'N' and preferred_masked_name != '' and key('pageData', preferred_resource)/value = 'N'">
                                                                                                                                <td width="142pt" style="text-align:left;color:blue;font-weight:bold"><xsl:value-of select="preferred_masked_name"/></td>
                                                                                                                        </xsl:when>
                                                                                                                        <xsl:otherwise>
                                                                                                                                 <td width="142pt" style="text-align:left;"><xsl:value-of select="first_name"/></td>
                                                                                                                        </xsl:otherwise>
                                                                                                                </xsl:choose>
                                                                                                         
													  <td width="180pt" style="text-align:left;"><xsl:value-of select="address"/></td>
													  <td width="135pt" style="text-align:left;"><xsl:value-of select="city"/></td>
													  <td width="22pt"  style="text-align:left;">&nbsp;<xsl:value-of select="state"/></td>
													  <td width="60pt"  style="text-align:left;"><xsl:value-of select="last_order_date"/></td>
													  <input type="hidden" id="lastOrderDate{@num}" value="{last_order_date}"/>
												  </tr>
												 </xsl:for-each>
											</tbody>
										</table>
									 </div>
								   </td>
							   </tr>
						       <tr>
							       <td class="Label" width="50%" >
							       		&nbsp;Page &nbsp; <xsl:value-of select="$cos_current_page"/>&nbsp; of &nbsp;
							       		<xsl:choose>
							       			<xsl:when test="$cos_total_pages = '0'">
							       				<xsl:value-of select="'1'"/>
							       			</xsl:when>
							       			<xsl:otherwise>
							       				<xsl:value-of select="$cos_total_pages"/>
							       			</xsl:otherwise>
							       		</xsl:choose>
							       </td>
							       <td class="Label" align="right" width="50%">
										<xsl:value-of select="$total_records"/>&nbsp; Records Found&nbsp;
							       </td>
    						   </tr>
						   </table><!-- end Inner -->
					   </td>
				   </tr>
			   </table><!-- end Main-->
			   <table border="0" width="98%" align="center" cellpadding="0" cellspacing="1" >
					<tr valign="top">
						<td width="1%">
<!--
					    <a href="#" tabindex="3"><img src="images/firstItem.gif" border="0" name="firstItem" id="firstItem" onmousedown="imgPressed(0);" onmouseup="imgUnPressed(0);" onclick="doPageAction(0);"/></a>
-->
              <xsl:choose>
                <xsl:when test="key('pageData','cos_show_first')/value ='y'">
                  <button type="button" name="firstItem" tabindex="3" class="arrowButton"  id="firstItem" onclick="doPageAction(0);">                  
                   7</button>
                </xsl:when>
                <xsl:when test="key('pageData','cos_show_first')/value ='n'">
                  <button type="button" name="firstItem2" tabindex="3" class="arrowButton"  id="firstItem2" onclick="">
                    	<xsl:attribute name="disabled">true</xsl:attribute>
                  7</button>
                </xsl:when>
              </xsl:choose>
						</td>
						<td width="1%">
<!--
							<a href="#" tabindex="2"><img src="images/backItem.gif"   border="0" name="backItem" id="backItem" onmousedown="imgPressed(1);" onmouseup="imgUnPressed(1);" onclick="doPageAction(1);"/></a>
-->
              <xsl:choose>
                <xsl:when test="key('pageData','cos_show_previous')/value ='y'">
                  <button type="button" name="backItem" tabindex="1" class="arrowButton" id="backItem" onclick="doPageAction(1);">                   
                   3</button>
                </xsl:when>
                <xsl:when test="key('pageData','cos_show_previous')/value ='n'">
                  <button type="button" name="backItem2" tabindex="1" class="arrowButton" id="backItem2" onclick="">
                       	<xsl:attribute name="disabled">true</xsl:attribute>
                 3 </button>
                </xsl:when>
              </xsl:choose>
						</td>
						<td width="1%">
<!--
              <a href="#" tabindex="1"><img src="images/nextItem.gif"   border="0" name="nextItem" id="nextItem" onmousedown="imgPressed(2);" onmouseup="imgUnPressed(2);" onclick="doPageAction(2);"/></a>
-->
              <xsl:choose>
                <xsl:when test="key('pageData','cos_show_next')/value ='y'">
                  <button type="button" name="nextItem" tabindex="2" class="arrowButton" id="nextItem" onclick="doPageAction(2);">                    
                   4</button>
                </xsl:when>
                <xsl:when test="key('pageData','cos_show_next')/value ='n'">
                  <button type="button" name="nextItem2" tabindex="2" class="arrowButton" id="nextItem2" onclick="">
                     	<xsl:attribute name="disabled">true</xsl:attribute>
                  4</button>
                </xsl:when>
              </xsl:choose>
						</td>
						<td width="1%">
<!--
							<a href="#" tabindex="4"><img src="images/lastItem.gif"   border="0" name="lastItem" id="lastItem" onmousedown="imgPressed(3);" onmouseup="imgUnPressed(3);" onclick="doPageAction(3);"/></a>
-->
              <xsl:choose>
                <xsl:when test="key('pageData','cos_show_last')/value ='y'">
                  <button type="button" name="lastItem" tabindex="4" class="arrowButton" id="lastItem" onclick="doPageAction(3);">                   
                  8</button>
                </xsl:when>
                <xsl:when test="key('pageData','cos_show_last')/value ='n'">
                  <button type="button" name="lastItem2" tabindex="4" class="arrowButton" id="lastItem2" onclick="">
                      <xsl:attribute name="disabled">true</xsl:attribute>
                  8</button>
                </xsl:when>
              </xsl:choose>
						</td>

						<td align="right">
                                                        <xsl:if test="//data[name='lossPreventionPermission']/value = $YES">
                                                            <button type="button" class="BigBlueButton" accesskey="V" id="lpsearch" onclick="javascript:doLossPreventionSearchAction();">
                                                                Back to Loss<br />Pre(v)ention Search
                                                            </button>
                                                        </xsl:if>
							<button type="button" class="BigBlueButton" tabindex="5" accesskey="b" onclick="javascript:doBackAction();" id="backSearchButton">(B)ack to Search</button>
						</td>
					</tr>
					<tr>
						<td colspan="5" align="right">
						   <button type="button" class="BigBlueButton" tabindex="6" onclick="javascript:doMainMenuAction();" style="width:55px;" accesskey="m"  id = "mainMenuButton" >(M)ain<br/>Menu</button>
						</td>
					</tr>
				</table>
        <!-- These buttons will provide the same functionality as the navigational links.  Buttons were
             implemented to use the acesskey functionality-->				
       </div>
       
       <!-- Used to dislay Processing... message to user -->
		<div id="waitDiv" style="display:none">
		   <table id="waitTable" class="mainTable" width="98%" height="300px" align="center" cellpadding="0" cellspacing="2" >
		      <tr>
		        <td width="100%">
              <table class="innerTable" width="100%" height="300px" cellpadding="0" cellspacing="2">
               <tr>
                  <td>
                     <table width="100%" cellspacing="0" cellpadding="0" border="0">
                         <tr>
                           <td id="waitMessage" align="right" width="50%" class="WaitMessage"></td>
                           <td id="waitTD"  width="50%" class="WaitMessage"></td>
                         </tr>
                     </table>
                   </td>
                </tr>
              </table>
		        </td>
		      </tr>
	      </table>
		   </div>
       <xsl:call-template name="footer"/>
			</form>
		</body>
	</html>
 </xsl:template>
  <!-- Actual call to header w/ params-->
  <xsl:template name="addHeader">
	<xsl:call-template name="header">
		<xsl:with-param name="showPrinter" select="true()"/>
		<xsl:with-param name="showBackButton" select="true()"/>
		<xsl:with-param name="showSearchBox" select="true()"/>
		<xsl:with-param name="showTime" select="true()"/>
		<xsl:with-param name="searchLabel" select="'Enter Line#'"/>
		<xsl:with-param name="headerName" select="'Multiple Records Found'"/>
                <xsl:with-param name="dnisNumber" select="$call_dnis"/>
                <xsl:with-param name="cservNumber" select="$call_cs_number"/>
                <xsl:with-param name="indicator" select="$call_type_flag"/>
                <xsl:with-param name="brandName" select="$call_brand_name"/>
		<xsl:with-param name="entryAction" select="true()"/>
		<xsl:with-param name="backButtonLabel" select="'(B)ack to Search'"/>
		<xsl:with-param name="cdispEnabledFlag" select="$cdisp_enabledFlag"/>
	</xsl:call-template>
  </xsl:template>
</xsl:stylesheet>