<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="securityanddata.xsl"/>
<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>

<xsl:key name="pageData" match="/root/pageData/data" use="name"/>
<xsl:key name="security" match="/root/security/data" use="name"/>
	
<xsl:output method="html" indent="yes"/>
<xsl:template match="/root">
<html>
<head>
<script type="text/javascript" language="javascript">
var statusValue = '<xsl:value-of select="key('pageData','message_display')/value"/>';
<![CDATA[
	function checkStatus()
	{
    parent.document.getElementById('process_original_view').value = statusValue;
    if(statusValue == '')
    {    
      parent.doProcessOriginalView();
    }
    else
    {
      alert('Original View is not available for this order.');
    }
	}
]]>
</script>
</head>
<body onload="javascript:checkStatus();">
    <xsl:call-template name="securityanddata"/>   
    <xsl:call-template name="decisionResultData"/>
</body>
</html>
</xsl:template>
</xsl:stylesheet>