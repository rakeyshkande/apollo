<!DOCTYPE ACDemo[
  <!ENTITY nbsp "&#160;">
  <!ENTITY reg "&#xAE;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<!-- External templates -->
<xsl:import href="header.xsl"/>
<xsl:import href="footer.xsl"/>
<xsl:import href="securityanddata.xsl"/>
<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>
<xsl:output method="html" indent="yes"/>

<xsl:variable name="SCRUB" select="'S'"/>
<xsl:variable name="PENDING" select="'P'"/>
<xsl:variable name="REMOVED" select="'R'"/>
<xsl:variable name="YES" select="'Y'"/>
<xsl:variable name="INDICATOR" select="/root/SC_SEARCH_CUSTOMERS/SC_SEARCH_CUSTOMER[position() = 1]/scrub_status"/>

<!-- Bollean modes -->
<xsl:variable name="SCRUB_MODE" select="boolean($SCRUB = $INDICATOR)"/>
<xsl:variable name="PENDING_MODE" select="boolean($PENDING = $INDICATOR)"/>
<xsl:variable name="REMOVED_MODE" select="boolean($REMOVED = $INDICATOR)"/>

<!-- Keys -->
<xsl:key name="security" match="/root/security/data" use="name"/>
<xsl:key name="pageData" match="/root/pageData/data" use="name"/>


<xsl:template match="/">
<html>
  <head>
     <title>FTD - Loss Prevention Search</title>
        <link rel="stylesheet" type="text/css" href="css/ftd.css"/>

    <script type="text/javascript" src="js/clock.js"/>
    <script type="text/javascript" src="js/commonUtil.js"/>
    <script type="text/javascript" src="js/util.js"/>
    <script type="text/javascript" src="js/FormChek.js"/>
    <script language="javascript" type="text/javascript">
    var orderStatus = '';
    var sc_mode = '';
    var action = '';
    var sc_confirmation_number = '<xsl:value-of select="$sc_order_number"/>';
    var scrub_url = '<xsl:value-of select="key('pageData','scrub_url')/value"/>';
    var order_access_denied_message = '<xsl:value-of select="key('pageData','orderAccessDeniedMessage')/value"/>';

    <xsl:variable name="popUp">
      <xsl:choose>
      <xsl:when test="$SCRUB_MODE">
        orderStatus = 'SCRUB';
        sc_mode = 'S';
        <xsl:value-of select="true()"/>
      </xsl:when>
      <xsl:when test="$PENDING_MODE">
        orderStatus = 'PENDING';
        sc_mode = 'P';
        <xsl:value-of select="true()"/>
      </xsl:when>
      <xsl:when test="$REMOVED_MODE">
        orderStatus = 'REMOVED';
        sc_mode = 'R';
        <xsl:value-of select="true()"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="false()"/>
      </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    var showPopup = <xsl:value-of select="$popUp"/>;
    
    <xsl:variable name="preferredPartnerPopUp">
      <xsl:choose>
      <xsl:when test="key('pageData', 'repCanAccessOrder')/value = 'N'">
        <xsl:value-of select="true()"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="false()"/>
      </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    var showPreferredPartnerPopUp = <xsl:value-of select="$preferredPartnerPopUp"/>;

/*************************************************************************************
* Initilization()
**************************************************************************************/
      function init(){
          clearReturnToRefundReview();
          clearBypassComValues();
          setNavigationHandlers();
          addDefaultListeners();
          resetErrorFields();
          document.getElementById('in_order_number').focus();
          document.getElementById('printer').tabindex = 17;
          document.getElementById('backButtonHeader').tabindex = 18;
          document.getElementById('bypass_privacy_policy_verification').value = 'Y';

          if(showPopup){
            doPopUpAction();
          }
          if(showPreferredPartnerPopUp){
            doPreferredPartnerPopUpAction();
          }     
      }
    <![CDATA[

      // Deletes values in the data filter so that the "Return to Refund Review"
      // button is no longer displayed for sessions initiated from the Search
      // screen. See defects 1067, 1212 and 1421.
      function clearReturnToRefundReview()
      {
        document.getElementById("prev_page").value = "";
        document.getElementById("prev_page_position").value = "";
      }
      
      // Deletes values in the data filter associated with the bypassing on com
      function clearBypassComValues()
      {
        document.getElementById("bypass_com").value = "";
        document.getElementById("loss_prevention_permission").value = "";
      }

/*------------------------------------------------------------------------------------
* Global Variables
-------------------------------------------------------------------------------------*/
      var fieldNames = new Array("in_order_number","in_last_name",
                                 "in_phone_number","in_email_address","in_zip_code","in_cc_number",
                                 "in_cust_number","in_tracking_number","in_rewards_number","in_ap_account");
      var alphaFields = new Array("in_last_name");
      var alphaNumFields = new Array("in_cc_number","in_tracking_number","in_rewards_number");
      var digitFields = new Array("in_cust_number");
      var uniqueFields = new Array("in_order_number", "in_cc_number", "in_cust_number", "in_tracking_number", "in_rewards_number", "in_ap_account");
      var nonUniqueFields = new Array("in_last_name", "in_phone_number", "in_email_address", "in_zip_code");

    // validate that only one unique field has been filled in or
    // that a valid combination of non-unique fields have been filled in
    function doValidation(){
      document.getElementById("errorEmail2").style.display = "none";
      var uniqueCount = 0;
      for ( var i = 0; i < uniqueFields.length; i++) {
        var toCheck = document.getElementById(uniqueFields[i]);
        if ( !isWhitespace(toCheck.value) ) {
          uniqueCount++;
        }
      }
      if ( uniqueCount > 1 ) {
        alert("Invalid combination of search criteria");
        return false;
      }

      var nonUniqueCount = 0;
      for ( var i = 0; i < nonUniqueFields.length; i++) {
        var toCheck = document.getElementById(nonUniqueFields[i]);
        if ( !isWhitespace(toCheck.value) ) {
          nonUniqueCount++;
        }
      }
      if ( uniqueCount == 1 && nonUniqueCount > 0 ) {
        alert("Invalid combination of search criteria");
        return false;
      }
      return true;
    }
    function doNonUniqueValidation(){
      document.getElementById("errorEmail2").style.display = "none";
      var cust = document.getElementById("in_cust_ind");
      var recip = document.getElementById("in_recip_ind");
      if (!cust.checked && recip.checked) {
        var email = document.getElementById("in_email_address");
        if ( !isWhitespace(email.value) ) {
          document.getElementById("errorEmail2").style.display = "block";
          return false;
        }
      }
      return checkSearchType();
    }
    function checkSearchType() {
      document.getElementById("errorSearchType").style.display = "none";

      var cust = document.getElementById("in_cust_ind");
      var recip = document.getElementById("in_recip_ind");

      // move checkbox values to hidden values //
      if (cust.checked == true){
        document.getElementById("sc_cust_ind").value = 'Y';
      }
      else{
        document.getElementById("sc_cust_ind").value = 'N';
      }

      if (recip.checked == true){
        document.getElementById("sc_recip_ind").value = 'Y';
      }
      else{
        document.getElementById("sc_recip_ind").value = 'N';
      }


      var uniqueCount = 0;
      var toCheck;
      for ( var i = 0; i < uniqueFields.length; i++) {
        toCheck = document.getElementById(uniqueFields[i]);
        if ( !isWhitespace(toCheck.value) ) {
          uniqueCount++;
        }
      }

      if (!cust.checked && !recip.checked && uniqueCount==0)
      {
				document.getElementById("errorSearchType").style.display = "block";
				return false;
      }
      return true;
    }

/*------------------------------------------------------------------------------------
* Actions
-------------------------------------------------------------------------------------*/

/*************************************************************************************
* Validate the form before the submit is allowed.
**************************************************************************************/
      function doValidateForm() {
        resetErrorFields();
        var isValid =  checkForEmptyFields();
        isValid = isValid && doAlphaFields();
        isValid = isValid && doAlphaNumFields();
        isValid = isValid && doDigitFields();
        isValid = isValid && checkEmailAddress();
        isValid = isValid && checkZipCode();
        isValid = isValid && stripPhone(stripInitialWhitespace(document.getElementById('in_phone_number').value));
        isValid = isValid && doValidation();
        isValid = isValid && doNonUniqueValidation();
        return isValid;
      }

/*************************************************************************************
* checkEmailAddress()
**************************************************************************************/
      function checkEmailAddress(){
        if(isEmail(document.getElementById('in_email_address').value,true))
        {
          document.getElementById("errorEmail").style.display = "none";
          return true;
        }else{
          document.getElementById("errorEmail").style.display = "block";
          return false;
        }
      }


/*************************************************************************************
  checkZipCode()
  Check for Zip Codes
**************************************************************************************/
      function checkZipCode(){
        var zipCode = document.getElementById('in_zip_code').value;

        var zip = stripInitialWhitespace(zipCode);
        if( zip.length > 0 ) {

          var re = /^\d{5}([\-])?(\d{4})?$/;
          var can = /^\s*[a-ceghj-npr-tvxy]\d[a-z](\s)?\d[a-z]\d\s*$/i;
          var inter = /[a-z]|[A-Z]|[0-9](\s+)?/;

          if( !re.test(zipCode) && !can.test(zipCode) && !inter.test(zipCode) ) {
            document.getElementById('errorZip').style.display = "block";
            return false;
          }
        }
        return true;
      }


/*************************************************************************************
* resetErrorFields()
**************************************************************************************/
      function resetErrorFields(){
        var spans = document.getElementsByTagName('span');
        for(var i = 0; i < spans.length; i++){
          if(spans[i].id != null && spans[i].id != ''){
            if(spans[i].id.substr(0,5) == 'error')
              spans[i].style.display = "none";
          }
        }
      }


/*************************************************************************************
* Iterate through all fields that must contain only alphanumeric (i.e. ABC123)
* characters.  If any field is invalid do not submit the form.
**************************************************************************************/
      function doAlphaNumFields(){
        var bool = true;
        for(var i = 0; i < alphaNumFields.length; i++) {
          var field = document.getElementById(alphaNumFields[i]);
          var value = stripInitialWhitespace(field.value);
          if (field.id == 'in_cc_number') {
            if ( !isCreditCard(value) )
              document.getElementById('errorCC').style.display = "block";
          }

          if( isAlphanumeric(value,true)) {
            continue;
          }
          else {
            if(field.id == 'in_tracking_number')
              document.getElementById('errorTrackingNum').style.display = "block";
            if(field.id == 'in_rewards_number')
              document.getElementById('errorRewardNum').style.display = "block";
            if(field.id == 'in_phone_number')
              document.getElementById('errorPhone').style.display = "block";
            bool = false;
          }
        }
        return bool;
      }


/*************************************************************************************
* Iterate through all fields that must contain only alphabetic characters.
* If any field is invalid do not submit the form.
**************************************************************************************/
      function doAlphaFields(){
        var bool = true;
        for(var i = 0; i < alphaFields.length; i++){
          var field = document.getElementById(alphaFields[i]);
          if(checkExtLastName(field.value)){
            continue;
          }else{
            bool = false;
            break;
          }
        }
        return bool;
      }


/*************************************************************************************
* doDigitFields()
**************************************************************************************/
      function doDigitFields(){
        var bool = true;
        var field = stripInitialWhitespace(document.getElementById(digitFields[0]).value);
        if(isInteger(field,true)){return true;
        } else{
          document.getElementById('errorCustNumber').style.display = "block";
           return false;
         }
      }


/*************************************************************************************
* Used to break up a lastname,firstname search combonation and ensure lastname and
* firstname are alphabetic. Otherwise, just check field if no comma is in the string
* to ensure it is alphabetic.
**************************************************************************************/
      function checkExtLastName(field){
        var bool = false;
        var i;
        field = Trim(field);
        var index = field.search(',');
        var lastNameFirstSec = '';
        var lastNameSecondSec = '';

        if( index > -1 )
        {
          //Get the Last Name
          var lastName =  Trim(field.substr(0,index));
          var lastNameEditted = "";
          for (i = 0; i < lastName.length; i++)
          {
              // remove all the whitespace from the last name and then check if the CSR entered
              // valid alphabets
              var c = lastName.charAt(i);
              if (isWhitespace(c) || c == ".")
              {}
              else
              {
                lastNameEditted += c;
              }
          }

          //Get the First Name
          var firstName = Trim(field.substr((index + 1),field.length));
          var firstNameEditted = "";
          for (i = 0; i < firstName.length; i++)
          {
              // remove all the whitespace from the first name and then check if the CSR entered
              // valid alphabets
              var c = firstName.charAt(i);
              if (isWhitespace(c) || c == ".")
              {}
              else
              {
                firstNameEditted += c;
              }
          }

            /* deal with hyphenated chars*/
          if( isHyphenated(lastNameEditted) )
          {
            var index = lastNameEditted.search('-');
            lastNameFirstSec = Trim( lastNameEditted.substr( 0,(index) ) );
            lastNameSecondSec = Trim( lastNameEditted.substr( (index +1),lastNameEditted.length ) );
          }

          //Check if the Last Name and the First Name were entered correctly
          if ( (lastNameFirstSec != '' && lastNameSecondSec != '') ? isAlphabetic( lastNameFirstSec,true) && isAlphabetic(lastNameSecondSec,true):isAlphabetic(lastNameEditted,true) && isAlphabetic(firstNameEditted,true) )
          {
            bool = true;
          }
          else
          {
            document.getElementById("errorName").style.display = "block";
          }
        }
        else
        {
          var field2 = "";
          for (i = 0; i < field.length; i++)
          {
              // remove all the whitespace from the last name and then check if the CSR entered
              // valid alphabets
              var c = field.charAt(i);
              if (isWhitespace(c) || c == ".")
              {}
              else
              {
                field2 += c;
              }
          }

            /* deal with hyphenated chars*/
          if( isHyphenated(field2) )
          {
            var index = field2.search('-');
            lastNameFirstSec = Trim( field2.substr( 0,(index) ) );
            lastNameSecondSec = Trim( field2.substr( (index +1),field2.length ) );
          }

          if( (lastNameFirstSec != '' && lastNameSecondSec != '') ? !isAlphabetic( lastNameFirstSec,true) && !isAlphabetic(lastNameSecondSec,true): !isAlphabetic(field2,true) )
          {
            document.getElementById('errorName').style.display = "block";
            return false;
          }
          else
          {
            bool = true;
          }
        }
        return bool;
      }

/*
 returns true if the value
 contains a '-' char
*/
function isHyphenated(value){
    var reg = '-';
    var val  = (value.match(reg) == null ? false:true);
  return val;
}
/*************************************************************************************
* Iterate through form fields to ensure the csr has entered data to search on.
**************************************************************************************/
      function checkForEmptyFields(){
        var count = 0;
        var fieldCount = 0;
        var form = document.forms[0];
        for(var i = 0; i < form.elements.length; i++){
          if(form.elements[i].type == 'text'){
            ++count;
          if(Trim(form.elements[i].value) == '')
            ++fieldCount;
          }
        }
        if(fieldCount == count) {alert("You must supply at least one search parameter. ");return false;}
       return true;
      }


/*************************************************************************************
* Remove all '-' characters form the phone number before passing it to the FormCheck.js
**************************************************************************************/
      function stripPhone(field){
        var re = /[()-]+/g;
        var newField = field.replace(re,"");
        if(isUSPhoneNumber(newField,true) || isInternationalPhoneNumber(newField,true))
        {
          return true;
        }
        else
        {
          document.getElementById('errorPhone').style.display = "block";
          return false;
        }
      }


/*************************************************************************************
* Clear all form fields
**************************************************************************************/
      function doClearFields(){
        clearAllFields(document.forms[0]);
        resetErrorFields();
        document.getElementById('in_cust_ind').checked = true;
        document.getElementById('in_recip_ind').checked = true;
        document.getElementById('in_order_number').focus();
      }


/*************************************************************************************
* If the order is currently in SCRUB,PENDING,REINSTATE provide a popup screen
* to allow the user to go there and view the order
**************************************************************************************/
      function doPopUpAction()
      {

          /* //if( scrub_url.localeCompare('http://site_name/scrub/servlet/ScrubSearchServlet') != 0 )
          //{
            //alert("Invalid site_name conversion... defaulting to Apollo..  url passed back = " + scrub_url);

            //APOLLO
            //scrub_url = "http://apollo.ftdi.com:7778/scrub/servlet/ScrubSearchServlet";

            //MY MACHINE
                  //scrub_url = "http://192.168.111.13:8988/scrub/servlet/ScrubSearchServlet";

            //BRIAN'S MACHINE
                  //scrub_url = "http://192.168.111.95:8988/scrub/servlet/ScrubSearchServlet";
         // }*/

          scrub_url += "?sc_email_address=";
          scrub_url += "&sc_product_code=";
          scrub_url += "&sc_ship_to_type=";
          scrub_url += "&sc_origin=";
	      // scrub_url += "&servlet_action=result_list";
          scrub_url += "&sc_mode=";
	   	  scrub_url += sc_mode;
	   	  scrub_url += "&selected_master_order_number=" + document.forms[0].sc_order_number.value;
          scrub_url += "&sc_confirmation_number=";
          scrub_url += document.forms[0].sc_order_number.value;
          scrub_url += "&sc_search_type=";
          scrub_url += sc_mode;
          scrub_url += getSecurityParams(false);

          //Modal Dial takes 3 parameters: URL, arguments, and features.

          //Modal_URL
          var modalUrl = "incorrectOrderDisposition.html";

          //Modal_Arguments
          var modalArguments = new Object();
          modalArguments.orderStatus = orderStatus;
          modalArguments.scrubURL = scrub_url;

          //Modal_Features
          var modalFeatures  =  'dialogWidth:800px,dialogHeight:400px,dialogLeft:0px,dialogTop:90px,';
          modalFeatures +=  'status:1,scroll:1,resizable:0';

          window.showModalDialog(modalUrl, modalArguments, modalFeatures);

          var frame = document.getElementById('checkLock');
		  var url = "lock.do?action_type=reset_scrub_lock&lock_id=" + document.forms[0].sc_order_number.value + getSecurityParams(false);

		  frame.src = url;

      }

    function checkKey()
	{
  		var tempKey = window.event.keyCode;

	  	//Up arrow pressed
	  	if (window.event.altKey && (tempKey == 53 || tempKey == 101))
	  	{
	  		doLast50RecordsAccessed();
	  	}
  	}
        
    /*************************************************************************************
    * If the order is a preferred partner order and the rep doesn't have security permission
    * to view this order, then display a warning message
    **************************************************************************************/
    function doPreferredPartnerPopUpAction()
    {
       var modalArguments = new Object();
       modalArguments.modalText = order_access_denied_message;
       window.showModalDialog("alert.html", modalArguments, 'dialogWidth:300px; dialogHeight:200px; center:yes; status=no; help=no; resizable:no; scroll:yes');
    }

      /*
        Checks to see if the csr hit the enter key
        and submits the form.
      */
      function doCustomerSearchAction(){
         if (window.event)
              if (window.event.keyCode)
                 if (window.event.keyCode != 13)
                   return false;
        var otype = event.srcElement.id;
        if(otype == 'lastRecords')
          return;
        if(otype == 'last50')
          return;
        if(otype == 'clear')
          return;
        if(otype == 'mainMenu')
          return;
            var url = "customerOrderSearch.do" +
                  "?action=lpsearch" + getIndicators();
             if(doValidateForm())
              performAction(url);
      }

/*************************************************************************************
* Iterate over the checkboxes - if checked append to the url.
**************************************************************************************/
      function getIndicators(){
        var form = document.forms[0];
        var url = "";
        for(var i = 0; i < form.elements.length; i++)
        {
          if(form.elements[i].type == 'checkbox' && form.elements[i].checked){
            url += "&" + form.elements[i].name + "=Y";
          }
        }
        return url;
      }

/*************************************************************************************
* doLastRecordAccessed
**************************************************************************************/
      function doLastRecordAccessed() {
        doClearFields();
        performAction("customerOrderSearch.do?action=last");
      }

/*************************************************************************************
* doLast50RecordsAccessed
**************************************************************************************/
      function doLast50RecordsAccessed() {
        doClearFields();
        performAction("customerOrderSearch.do?action=last_50");
      }

/*************************************************************************************
* Using the specified URL submit the form
**************************************************************************************/
      function performAction(url){
        showWaitMessage("content", "wait", "Searching");
        document.forms[0].action = url;
        document.forms[0].submit();
      }

/*************************************************************************************
* Perorms the Back button functionality
**************************************************************************************/
      function doBackAction() {
        doMainMenuAction();
      }
    ]]>
    </script>
  </head>
  <body id="body" onload="javascript:init();" onkeydown="javascript:checkKey();">

  <xsl:call-template name="addHeader"/>
  <form name="orderSearch" method="post" onkeypress="doCustomerSearchAction();" >
  	<iframe id="checkLock" name="checkLock" height="0" width="0" border="0"></iframe>
    <xsl:call-template name="securityanddata"/>
    <xsl:call-template name="decisionResultData"/>

    <!-- content to be hidden once the search begins -->
    <div id="content" style="display:block">
     <xsl:if test="key('pageData','message_display')/value != ''">
       <table border="0" cellpadding="0" cellspacing="0" width="98%" align="center">
         <tr>
           <td class="largeErrorMsgTxt">
            <xsl:value-of select="key('pageData','message_display')/value"/>
           </td>
         </tr>
       </table>
       </xsl:if>
       <table class="mainTable" align="center" cellpadding="1" cellspacing="1" width="98%">
          <tr>
          <td><!--Content table-->
            <table border="0" class="innerTable" width="100%" align="center">
              <tr><td colspan="6">&nbsp;</td></tr>
              <tr>
                <td class="labelpad" width="24%" >
                  ** Order Number:
                </td>
                <td >
                  <input type="text" value="" tabindex="1" maxlength="25" id="in_order_number" name="in_order_number"/>
                </td>
                <td width="25%" align="right">
                  <label for="in_cust_ind">
                    <input type="checkbox" tabindex="10" name="in_cust_ind" id="in_cust_ind">
                      <xsl:if test="$sc_cust_ind = 'Y'"><xsl:attribute name="CHECKED"/></xsl:if>
                    </input>
                    Customer
                  </label>
                  &nbsp;&nbsp;&nbsp;&nbsp;
                  <label for="in_recip_ind">
                    <input type="checkbox" tabindex="11" name="in_recip_ind" id="in_recip_ind">
                      <xsl:if test="$sc_recip_ind = 'Y'"><xsl:attribute name="CHECKED"/></xsl:if>
                    </input>
                    Recipient
                  </label>
                </td>
                <td>
                  <span id="errorSearchType" class="RequiredFieldTxt" style="display:none;text-align:left">
                    &nbsp;&nbsp;Invalid search type.
                  </span>
                </td>

              </tr>
              <tr>
                <td class="labelpad">
                   Last Name:
                </td>
                <td>
                  <input type="text" value="" tabindex="2" maxlength="20" id="in_last_name" name="in_last_name"/>
                </td>
                <td colspan="3" >
                <span id="errorName" class="RequiredFieldTxt" style="display:none;text-align:left">
                    &nbsp;&nbsp;Invalid Format Last Name, First Name
                  </span>
                </td>
              </tr>

              <tr>
                <td class="labelpad">
                  Phone Number:
                </td>
                <td >
                  <input type="text" value="" tabindex="3" maxlength="20" id="in_phone_number" name="in_phone_number"/>
                </td>
                <td colspan="3">
                  <span id="errorPhone" class="RequiredFieldTxt" style="display:none">
                    &nbsp;&nbsp;Invalid Format
                  </span>
                </td>
              </tr>
              <tr>
                <td class="labelpad">
                  Email Address:
                </td>
                <td >
                  <input type="text" tabindex="4" maxlength="55" id="in_email_address" name="in_email_address" value=""/>
                </td>
                <td colspan="3">
                  <span id="errorEmail" class="RequiredFieldTxt" style="display:none">
                    &nbsp;&nbsp;Invalid Format xxxxx@xxxx.com
                  </span>
                  <span id="errorEmail2" class="RequiredFieldTxt" style="display:none">
                    &nbsp;&nbsp;Invalid search criteria: Recipient Email
                  </span>
                </td>
              </tr>
              <tr>
                <td class="labelpad">
                  Zip Code:
                </td>
                <td>
                  <input type="text" tabindex="5" maxlength="12" id="in_zip_code" name="in_zip_code" value=""/>
                </td>
                <td colspan="3">
                  <span id="errorZip" class="RequiredFieldTxt" style="display:none">
                    &nbsp;&nbsp;Invalid Format
                  </span>
                  <span id="errorZipInter" class="RequiredFieldTxt" style="display:none">
                    &nbsp;&nbsp;Invalid Format A9AA9A
                  </span>
                </td>
              </tr>
              <tr>
                <td class="labelpad">
                  ** Credit Card Number:
                </td>
                <td >
                  <input type="text" value="" tabindex="6" maxlength="20" id="in_cc_number" name="in_cc_number"/>
                </td>
                <td colspan="3">
                  <span id="errorCC" class="RequiredFieldTxt" style="display:none">
                    &nbsp;&nbsp;Invalid Format
                  </span>

                </td>
              </tr>
              <tr>
                <td class="labelpad">
                  ** Alternate Payment Account:
                </td>
                <td>
                  <input type="text" value="" tabindex="6" maxlength="200" id="in_ap_account" name="in_ap_account"/>
                </td>
                <td class="Instruction" width="25%" align="right" colspan="3">
                   * This field allows alternate payment account types such as PayPal, BillMeLater, Miles / Points Redemption account number...
                </td>
              </tr>
              <tr>
                <td class="labelpad">
                  ** Customer Number:
                </td>
                <td >
                  <input type="text" value="" tabindex="7" maxlength="20" id="in_cust_number" name="in_cust_number"/>
                </td>
                <td colspan="3">
                  <span id="errorCustNumber" class="RequiredFieldTxt" style="display:none">
                    &nbsp;&nbsp;Invalid Format
                  </span>

                </td>
              </tr>
              <tr>
                <td class="labelpad">
                  ** Tracking Number:
                </td>
                <td>
                  <input type="text" value="" tabindex="8" maxlength="100" id="in_tracking_number" name="in_tracking_number"/>
                </td>
                <td colspan="3">
                  <span id="errorTrackingNum" class="RequiredFieldTxt" style="display:none">
                    &nbsp;&nbsp;Invalid Format
                  </span>
                </td>
              </tr>
              <tr>
                <td class="labelpad">
                  ** Rewards Number:
                </td>
                <td>
                  <input type="text" value="" tabindex="9" maxlength="55" id="in_rewards_number" name="in_rewards_number"/>
                </td>
                <td colspan="3">
                  <span id="errorRewardNum" class="RequiredFinumberpad keycodeeldTxt" style="display:none">
                    &nbsp;&nbsp;Invalid Format
                  </span>
                </td>

              </tr>
              <tr>
                <td class="labelpad" colspan="3">
                  ** Designates Unique Search Criteria
                </td>
                <td  class="label" align="center">
                  QUICK SEARCH <br/>
                  <button type="button" id="lastRecords" class="bluebutton" tabindex="12" accesskey="L" onclick="javascript:doLastRecordAccessed();">(L)ast Record Accessed</button><br/>
                  OR <br/>
                  <button type="button" id="last50" class="bluebutton"  tabindex="13" onclick="javascript:doLast50RecordsAccessed();">Last (5)0 Records Accessed</button>
                </td>
              </tr>
            </table>
          </td><!--Content table-->
           </tr>
       </table>
      <table align="center" width="98%" border="0" cellpadding="0" cellspacing="0">
        <tr><td colspan = "3">&nbsp;</td></tr>
        <tr>
          <td width="25%"></td>
          <td valign="top" align="center" width="49%">
            <button type="button" id="searchButton" class="bluebutton" accesskey="S" tabindex="14" onclick="javascript:doCustomerSearchAction();">(S)earch</button>
            <button type="button" id="clear" class="bluebutton" accesskey="C" tabindex="15" onclick="doClearFields();">(C)lear all Fields</button>
          </td>
          <td width="49%" align="right">
            <button type="button" id="mainMenu" class="bluebutton" accesskey="B" tabindex="16" onclick="javascript:doMainMenuAction();">(B)ack to Main Menu</button>
          </td>
        </tr>
      </table>
    </div>

    <!-- Used to dislay Processing... message to user -->
    <div id="waitDiv" style="display:none">
       <table id="waitTable" class="mainTable" width="98%" height="100%" align="center" cellpadding="2" cellspacing="1" >
          <tr>
            <td width="100%">
              <table class="innerTable" width="100%" height="100%" cellpadding="30" cellspacing="1">
               <tr>
                  <td>
                     <table width="100%" cellspacing="0" cellpadding="0" border="0">
                         <tr>
                           <td id="waitMessage" align="right" width="50%" class="waitMessage"></td>
                           <td id="waitTD"  width="50%" class="waitMessage"></td>
                         </tr>
                     </table>
                   </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
    </div>
    <!-- call footer template-->
    <xsl:call-template name="footer"/>
    </form>
  </body>
</html>
</xsl:template>
   <!--Actual Call to the Header.xsl page -->
     <xsl:template name="addHeader">
      <xsl:call-template name="header">
        <xsl:with-param name="showPrinter" select="true()"/>
        <xsl:with-param name="showBackButton" select="true()"/>
        <xsl:with-param name="showSearchBox" select="false()"/>
        <xsl:with-param name="showTime" select="true()"/>
        <xsl:with-param name="headerName" select="'Loss Prevention Search'"/>
        <xsl:with-param name="dnisNumber" select="$call_dnis"/>
        <xsl:with-param name="cservNumber" select="$call_cs_number"/>
        <xsl:with-param name="indicator" select="$call_type_flag"/>
        <xsl:with-param name="brandName" select="$call_brand_name"/>
        <xsl:with-param name="backButtonLabel" select="'(B)ack to Main Menu'"/>
        <xsl:with-param name="cdispEnabledFlag" select="$cdisp_enabledFlag"/>
      </xsl:call-template>
    </xsl:template>
</xsl:stylesheet>