<!DOCTYPE ACDemo[
  <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:import href="header.xsl"/>
<xsl:import href="footer.xsl"/>
<xsl:import href="securityanddata.xsl"/>
<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>
<xsl:output method="html" indent="yes"/>

<xsl:key name="pageData" match="/root/pageData/data" use="name"/>
<xsl:key name="security" match="/root/security/data" use="name"/>
<xsl:variable name="productImages" select="key('pageData', 'productImages')/value"/>
<xsl:variable name="selected_product" select="/root/searchResults/searchResult[@selectedaddonid=@addonid]"/>
<xsl:template match="/root">

<html>
<head>
  <title>FTD - Greeting Card Detail</title>
  <link rel="stylesheet" type="text/css" href="css/ftd.css"/>
  <script language="javascript" src="js/FormChek.js"/>
  <script language="javascript" src="js/util.js"/>
  <script language="javascript"><![CDATA[
  /*
   *  Initialization
   */
    function init(){
      window.name = "VIEW_GREETING_CARD_DETAIL";
    }

  /*
   *  Actions
   */
    function doCloseAction(retValue){
      if (window.event)
        if (window.event.keyCode)
          if (window.event.keyCode != 13)
            return false;

      top.returnValue = retValue;
      top.close();
    }

    //This function passes parameters from this page to a function on the calling page
    function goBack(){
      if (window.event)
        if (window.event.keyCode)
          if (window.event.keyCode != 13)
            return false;

      doCloseAction("BACK");
    }

    //This function closes both the current window and the lookup window
    function closePage(){
      doCloseAction("");
    }

    function processClosePage(){
      if (window.event.keyCode == 13)
        closePage();
    }

    function populatePage(cardId){
      if (window.event)
        if (window.event.keyCode)
          if (window.event.keyCode != 13)
            return false;

      doCloseAction(cardId);
    }

    //This function refreshes the current page with the selected card
    //Or reopens the list page with the proper occasion
    function openPopup(){
      var toSubmit = document.getElementById("lookupGreetingDetailForm");
      var id = toSubmit.cardIdInput[toSubmit.cardIdInput.selectedIndex].value;
      if (id != "") {
        toSubmit.target = window.name;
        toSubmit.submit();
      }
    }]]>
  </script>
</head>
<body onload="javascript:init();">
  <form name="lookupGreetingDetailForm" method="post" action="lookupGreeting.do">
    <xsl:call-template name="securityanddata"/>
    <xsl:call-template name="decisionResultData"/>
    <input type="hidden" name="action" id="action" value="detail"/>
    <input type="hidden" name="occasionInput" id="occasionInput" value="{key('pageData', 'occasionInput')/value}"/>

    <!-- Header template -->
    <xsl:call-template name="header">
      <xsl:with-param name="headerName" select="'Greeting Card Detail'"/>
      <xsl:with-param name="showExitButton" select="'true'"/>
      <xsl:with-param name="cdispEnabledFlag" select="$cdisp_enabledFlag"/>
    </xsl:call-template>

    <table width="98%" border="0" cellpadding="2" cellspacing="2" align="center">
      <tr>
        <td>
          <select tabindex="1" name="cardIdInput" id="cardIdInput" onchange="javascript:openPopup()">
            <option value="">---Select a card---</option>
            <xsl:for-each select="searchResults/searchResult">
              <option value="{@addonid}"> <xsl:value-of select="@description"/> </option>
            </xsl:for-each>
          </select>
        </td>
        <td align="right">
          <button type="button" class="BlueButton" tabindex="2" border="0" onclick="javascript:goBack()" onkeydown="javascript:goBack()">Back to Card Selections</button>&nbsp;
          <button type="button" class="BlueButton" tabindex="3" border="0" onclick="javascript:closePage()" onkeydown="javascript:processClosePage()">Close screen</button>
        </td>
      </tr>
    </table>

    <table width="98%" class="LookupTable" cellpadding="2" cellspacing="2" align="center">
      <tr>
        <td width="34%">
          <table width="100%" border="1" cellpadding="2" cellspacing="2">
            <tr>
              <td>
                <img width="280" height="260" src="{$productImages}{$selected_product/@addonid}.jpg"/>
              </td>
            </tr>
          </table>
        </td>
        <td width="66%" valign="top">
          <table width="98%" border="0" cellpadding="2" cellspacing="2">
            <tr>
              <td class="label">Greeting Card:</td>
              <td><xsl:value-of select="$selected_product/@description"/></td>
            </tr>
            <tr>
              <td class="label">Front:</td>
              <td><xsl:value-of select="$selected_product/@description"/></td>
            </tr>
            <tr>
              <td class="label">Inside:</td>
              <td><xsl:value-of select="$selected_product/@inside" disable-output-escaping="yes"/></td>
            </tr>
            <tr>
              <td class="label">Price:</td>
              <td>
                <script language="javascript">
                  //format the price
                  var price = "<xsl:value-of select="$selected_product/@price"/>";<![CDATA[
                  var place = price.indexOf('.');
                  var rightOf = price.substring(place, 10);
                  if ((rightOf.length) == 2)
                    price +="0";
                  document.write("&nbsp;$"+price+"")]]>
                </script>
              </td>
            </tr>
            <tr>
              <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="2" align="center">
                <button type="button" class="BlueButton" onclick="javascript:populatePage('{$selected_product/@addonid}');" onkeydown="javascript:populatePage('{$selected_product/@addonid}');" tabindex="4">Select card</button>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>

    <table width="98%" border="0" cellpadding="2" cellspacing="2" align="center">
      <tr>
        <td align="right">
          <button type="button" class="BlueButton" tabindex="5" border="0" onclick="javascript:goBack()" onkeydown="javascript:goBack()">Back to Card Selections</button>&nbsp;
          <button type="button" class="BlueButton" tabindex="6" border="0" onclick="javascript:closePage()" onkeydown="javascript:processClosePage()">Close screen</button>
        </td>
      </tr>
    </table>

    <!-- Footer template -->
    <xsl:call-template name="footer"/>
  </form>
</body>
</html>

</xsl:template>
</xsl:stylesheet>