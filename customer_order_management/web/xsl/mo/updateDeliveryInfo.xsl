<!DOCTYPE ACDemo[
   <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="../header.xsl"/>
<xsl:import href="../footer.xsl"/>
<xsl:import href="../securityanddata.xsl"/>
<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>
<xsl:import href="cusProduct.xsl"/>

<xsl:output indent="yes" method="html"/>

<xsl:key name="pageData" match="/root/pageData/data" use="name"/>
<xsl:key name="security" match="/root/security/data" use="name"/>
<xsl:variable name="deliveryData" select="/root/PD_ORDER_PRODUCTS/PD_ORDER_PRODUCT"/>
<xsl:variable name="productData" select="/root/PD_ORIG_PRODUCTS/PD_ORIG_PRODUCT"/>
<xsl:variable name="selectedState" select="$deliveryData/recipient_state"/>
<xsl:variable name="selectedCountry" select="$deliveryData/recipient_country"/>
<xsl:variable name="OK" select="'O'"/>
<xsl:variable name="CONFIRM" select="'C'"/>
<xsl:variable name="WALMART_ORDER" select="'WLMTI'"/>
<xsl:variable name="CSPI_ORDER" select="'CSPI'"/>
<xsl:variable name="membership_required" select="key('pageData', 'membership_required')/value"/>


<xsl:template match="/root">
<html>
  <head>
    <title>FTD - Update Delivery Information</title>
    <link rel="stylesheet" type="text/css" href="css/ftd.css"/>
<!--
-->
      <script type="text/javascript" src="js/commonUtil.js"></script>
      <script type="text/javascript" src="js/util.js"></script>
      <script type="text/javascript" src="js/FormChek.js"/>
      <script type="text/javascript" src="js/popup.js"/>
      <script type="text/javascript" src="js/sourceCodeLookup.js"/>
      <script type="text/javascript" src="js/cityLookup.js"/>
      <script type="text/javascript" src="js/recipientLookup.js"/>
      <script type="text/javascript" src="js/institutionLookup.js"/>
      <script type="text/javascript" src="js/tabIndexMaintenance.js"/>
      <script type="text/javascript" src="js/updateDeliveryInfo.js"/>
    
    <script type="text/javascript" language="javascript">

    <!--
      GLOBAL VARIABLES
    -->
    var usStates = new Array( <xsl:for-each select="STATES/STATE[countryCode='']">    ["<xsl:value-of select="id"/>","<xsl:value-of select="name"/>"]<xsl:choose><xsl:when test="position()!=last()">,</xsl:when></xsl:choose></xsl:for-each> );
    var caStates = new Array( <xsl:for-each select="STATES/STATE[countryCode='CAN']"> ["<xsl:value-of select="id"/>","<xsl:value-of select="name"/>"]<xsl:choose><xsl:when test="position()!=last()">,</xsl:when></xsl:choose></xsl:for-each> );
    var viStates = new Array( <xsl:for-each select="STATES/STATE[id='VI']"> ["<xsl:value-of select="id"/>","<xsl:value-of select="name"/>"]<xsl:choose><xsl:when test="position()!=last()">,</xsl:when></xsl:choose></xsl:for-each> );
    var prStates = new Array( <xsl:for-each select="STATES/STATE[id='PR']"> ["<xsl:value-of select="id"/>","<xsl:value-of select="name"/>"]<xsl:choose><xsl:when test="position()!=last()">,</xsl:when></xsl:choose></xsl:for-each> );

    var ADDRESS_TYPES = new Array();
    <xsl:for-each select="ADDRESS_TYPES/ADDRESS_TYPE">
      ADDRESS_TYPES["<xsl:value-of select="CODE"/>"] = {};
      ADDRESS_TYPES["<xsl:value-of select="CODE"/>"].promptForBusiness="<xsl:value-of select="PROMPT_FOR_BUSINESS_FLAG"/>";
      ADDRESS_TYPES["<xsl:value-of select="CODE"/>"].promptForRoom="<xsl:value-of select="PROMPT_FOR_ROOM_FLAG"/>";
      ADDRESS_TYPES["<xsl:value-of select="CODE"/>"].roomLabelTxt="<xsl:value-of select="ROOM_LABEL_TXT"/>";  
      ADDRESS_TYPES["<xsl:value-of select="CODE"/>"].promptForHours="<xsl:value-of select="PROMPT_FOR_HOURS_FLAG"/>";
      ADDRESS_TYPES["<xsl:value-of select="CODE"/>"].hoursLabelTxt="<xsl:value-of select="HOURS_LABEL_TXT"/>";  
    </xsl:for-each>
  
    var naStates = new Array(["NA", "N/A"]);
    var containsErrors = <xsl:value-of select="boolean(ERROR_MESSAGES/ERROR_MESSAGE)"/>;
    var fieldErrors = new Array( <xsl:for-each select="ERROR_MESSAGES/ERROR_MESSAGE[FIELDNAME!='']">["<xsl:value-of select="FIELDNAME"/>","<xsl:value-of select="TEXT"/>",validateFormHelper,false]<xsl:choose><xsl:when test="position()!=last()">,</xsl:when></xsl:choose></xsl:for-each> );
    var okErrors = new Array( <xsl:for-each select="ERROR_MESSAGES/ERROR_MESSAGE[FIELDNAME='' and POPUP_INDICATOR=$OK]">["<xsl:value-of select="TEXT"/>", "<xsl:value-of select="POPUP_GOTO_PAGE"/>"]<xsl:choose><xsl:when test="position()!=last()">,</xsl:when></xsl:choose></xsl:for-each> );
    var confirmErrors = new Array( <xsl:for-each select="ERROR_MESSAGES/ERROR_MESSAGE[FIELDNAME='' and POPUP_INDICATOR=$CONFIRM]">["<xsl:value-of select="TEXT"/>", "<xsl:value-of select="POPUP_GOTO_PAGE"/>"]<xsl:choose><xsl:when test="position()!=last()">,</xsl:when></xsl:choose></xsl:for-each> );
    var isWalmartOrder = <xsl:value-of select="boolean($deliveryData/origin_id = $WALMART_ORDER)"/>;
    var isSkipFloristValidation = <xsl:value-of select="boolean(key('pageData', 'skip_florist_validation')/value = 'y')"/>;
    var containsSpecialInstructions = <xsl:value-of select="boolean($deliveryData/special_instructions != '')"/>;
    var isGDOrder = <xsl:value-of select="boolean(/root/ORDER_PAYMENTS/ORDER_PAYMENT/payment_type[. = 'GD'])"/>;

    <!--
      FUNCTIONS
    -->
    function init(){
      document.oncontextmenu = contextMenuHandler;
      document.onkeydown = udiBackKeyHandler;
      addDefaultListeners();
      doDeliveryTypeChange();
      populateStates('recipient_country', 'recipient_state', '<xsl:value-of select="$selectedState"/>');
      initErrorMessages();
      setUpdateDeliveryTabIndexes();
    }
    </script>
  </head>
<body onload="javascript:init();">
   <xsl:call-template name="addHeader"/>

   <!-- Locking IFrame -->
   <iframe id="checkLock" width="0px" height="0px" border="0"/>

   <!-- Cancel Update IFrame -->
   <iframe id="cancelUpdateFrame" width="0px" height="0px" border="0"/>

   <!-- Main Content Div used to hide content when submitting requests -->
   <div id="mainContent" style="display:block">

   <!-- Customer Product data -->
   <xsl:call-template name="cusProduct">
     <xsl:with-param name="subheader" select="subheader"/>
     <xsl:with-param name="displayContinue" select="true()"/>
     <xsl:with-param name="displayComplete" select="false()"/>
     <xsl:with-param name="displayCancel" select="true()"/>
   </xsl:call-template>

   
   <form name="deliveryInfoForm" id="deliveryInfoForm" action="updateDeliveryInfo.do" method="post">
      <xsl:call-template name="securityanddata"/>
      <xsl:call-template name="decisionResultData"/>
      <input type="hidden" name="uc_change_flag" id="uc_change_flag" value="N"/>
      <input type="hidden" name="display_page" id="display_page" value=""/>
      <input type="hidden" name="vendor_flag" id="vendor_flag" value="{$deliveryData/vendor_flag}"/>
      <input type="hidden" name="order_detail_id" id="order_detail_id" value="{$deliveryData/order_detail_id}"/>
      <input type="hidden" name="order_guid" id="order_guid" value="{$deliveryData/order_guid}"/>
      <input type="hidden" name="category_index" id="category_index" value="{$deliveryData/occasion_category_index}"/>
      <input type="hidden" name="company_id" id="company_id" value="{$deliveryData/company_id}"/>
      <input type="hidden" name="master_order_number" id="master_order_number" value="{$deliveryData/master_order_number}"/>
      <input type="hidden" name="occasion" id="occasion" value="{$deliveryData/occasion}"/>
      <input type="hidden" name="occasion_description" id="occasion_description" value="{$deliveryData/occasion_description}"/>
      <input type="hidden" name="price_point_id" id="price_point_id" value="{$deliveryData/size_indicator}"/>
      <input type="hidden" name="product_id" id="product_id" value="{$deliveryData/product_id}"/>
      <input type="hidden" name="orig_product_id" id="orig_product_id" value="{subheader/subheader_info/sh_product_id}"/>
      <input type="hidden" name="ship_method" id="ship_method" value="{$deliveryData/ship_method}"/>
      <input type="hidden" name="ship_date" id="ship_date" value="{$deliveryData/ship_date}"/>
      <input type="hidden" name="shipping_fee" id="shipping_fee" value="{$deliveryData/shipping_fee}"/>
      <input type="hidden" name="delivery_date" id="delivery_date" value="{$productData/delivery_date}"/>
      <input type="hidden" name="delivery_date_range_end" id="delivery_date_range_end" value="{$productData/delivery_date_range_end}"/>
      <input type="hidden" name="skip_florist_validation" id="skip_florist_validation" value="N"/>
      <input type="hidden" name="buyer_full_name" id="buyer_full_name" value="{$deliveryData/customer_first_name} {$deliveryData/customer_last_name}"/>
      <input type="hidden" name="page_number" id="page_number" value="1"/>
      <input type="hidden" name="origin_id" id="origin_id" value="{$deliveryData/origin_id}"/>
      <input type="hidden" name="can_change_order_source" id="can_change_order_source" value="{$deliveryData/can_change_order_source}"/>
      <input type="hidden" name="florist_id" id="florist_id" value="{$deliveryData/florist_id}"/>
      <input type="hidden" name="alt_contact_info_id" id="alt_contact_info_id" value="{$deliveryData/alt_contact_info_id}"/>
      <input type="hidden" name="recipient_address_type" id="recipient_address_type" value="{$deliveryData/recipient_address_type}"/>
      <input type="hidden" name="customer_id" id="customer_id" value="{$deliveryData/customer_id}"/>
      <input type="hidden" name="customer_first_name" id="customer_first_name" value="{$productData/customer_first_name}"/>
      <input type="hidden" name="customer_last_name" id="customer_last_name" value="{$productData/customer_last_name}"/>
      <input type="hidden" name="external_order_number" id="external_order_number" value="{subheader/subheader_info/sh_external_order_number}"/>
      <input type="hidden" name="product_amount" id="product_amount" value="{$deliveryData/product_amount}"/>
      <input type="hidden" name="orig_product_amount" id="orig_product_amount" value="{$deliveryData/product_amount}"/>
      <input type="hidden" name="reward_type" id="reward_type" value="{key('pageData', 'reward_type')/value}"/>
      <input type="hidden" name="eod_delivery_indicator" id="eod_delivery_indicator" value="{$deliveryData/eod_delivery_indicator}"/>
      <input type="hidden" name="ship_method_carrier" id="ship_method_carrier" value="{$deliveryData/ship_method_carrier}"/>
      <input type="hidden" name="ship_method_vendor" id="ship_method_vendor" value="{$deliveryData/ship_method_vendor}"/>
      <input type="hidden" name="ship_method_desc" id="ship_method_desc" value="{$deliveryData/ship_method_desc}"/>
      <input type="hidden" name="membership_required" id="membership_required" value="{key('pageData', 'membership_required')/value}"/>

      <input type="hidden" name="exception_start_date" id="exception_start_date" value="{PD_ORDER_PRODUCTS/PD_ORDER_PRODUCT/exception_start_date}"/>
      <input type="hidden" name="exception_end_date" id="exception_end_date" value="{PD_ORDER_PRODUCTS/PD_ORDER_PRODUCT/exception_end_date}"/>


      <input type="hidden" name="updated_on" id="updated_on" value="{$deliveryData/updated_on}"/>
      <input type="hidden" name="recipient_id" id="recipient_id" value="{$deliveryData/recipient_id}"/>
      <input type="hidden" name="source_code_description" id="source_code_description" value="{$deliveryData/source_code_description}"/>
      <input type="hidden" name="eod_delivery_date" id="eod_delivery_date" value="{$deliveryData/eod_delivery_date}"/>
      <input type="hidden" name="customer_city" id="customer_city" value="{$deliveryData/customer_city}"/>
      <input type="hidden" name="customer_state" id="customer_state" value="{$deliveryData/customer_state}"/>
      <input type="hidden" name="customer_zip_code" id="customer_zip_code" value="{$deliveryData/customer_zip_code}"/>
      <input type="hidden" name="customer_country" id="customer_country" value="{$deliveryData/customer_country}"/>
      <input type="hidden" name="recipient_address_code" id="recipient_address_code"/>

      <input type="hidden" name="free_Shipping_Active_Member" id="free_Shipping_Active_Member" value="{key('pageData', 'freeShippingActiveMember')/value}"/>
      <input type="hidden" name="free_Shipping_Program_Name" id="free_Shipping_Program_Name" value="{key('pageData', 'freeShippingProgramName')/value}"/>
      <input type="hidden" name="free_shipping_flag_from_source" id="free_shipping_flag_from_source" />

       <!-- Customer Product params -->
      <xsl:call-template name="cusProduct-params">
        <xsl:with-param name="subheader" select="subheader"/>
      </xsl:call-template>
	<table class="mainTable" align="center" border="0" cellpadding="0" cellspacing="1" width="98%">
      <tr>
         <td>
            <table class="innerTable" align="center" border="0" cellpadding="0" cellspacing="1" width="100%">
               <tr>
                  <td>
                    <span style="padding-left:1em;color:#000099;font-weight:bold;font-size:12pt">
                      <xsl:value-of select="$productData/product_name"/>
                    </span>
                    <xsl:if test="$deliveryData/vendor_flag = 'Y' and $deliveryData/shipping_fee > 0">
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      <strong>Current Delivery Method&nbsp;&amp;&nbsp;Charges:&nbsp;</strong>
                      <xsl:value-of select="$deliveryData/ship_method_desc"/>
                      &nbsp;-&nbsp;
                      $<xsl:value-of select="format-number($deliveryData/shipping_fee, '#,###,##0.00')"/>
                    </xsl:if>
                    <br/>
                    <span style="padding-left:1em;font-weight:bold;font-size:12pt;"><xsl:value-of select="$productData/product_id"/></span><br/>
                    <hr align="center" width="98%" size="4px" style="color:#000099;"/><br/>
                    <hr align="center" width="98%" size="1px" style="color:#000099;"/>
                    
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                      <!-- Source Code/Membership Number -->
                      <tr>
                        <td width="60%" valign="top">
                          <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                              <td width="20%" class="Label" align="right">Source Code is: </td>
                              <td width="10%" style="padding-left:.5em;">
                                <input name="source_code" id="source_code" type="hidden" value="{$deliveryData/source_code}"/>
                                <input name="page_source_code" id="page_source_code" type="text" size="10" maxlength="15" value="{$deliveryData/source_code}">
                                  <xsl:if test="$CSPI_ORDER = $deliveryData/origin_id">
                                     <xsl:attribute name="disabled"><xsl:value-of select="'true'"/></xsl:attribute>
                                  </xsl:if>
                                </input>
                              </td>
                              <td width="30%" class="Label">&nbsp;<xsl:value-of select="$deliveryData/source_code_description"/></td>
                            </tr>
                            <tr id="page_source_code_validation_message_row" style="display:none">
                              <td/>
                              <td colspan="2" id="page_source_code_validation_message_cell" class="RequiredFieldTxt" style="padding-left:.5em;"/>
                            </tr>
                            <tr>
                              <td/>
                              <td colspan="2" style="padding-left:.5em;">   
                                  <xsl:if test="not($CSPI_ORDER = $deliveryData/origin_id)">
                                    <a id="sourceCodeLink" class="textlink" onclick="javascript:doSourceCodeLookup();" onkeypress="javascript:doEnterKeyAction(doSourceCodeLookup);">Lookup Source Code</a>
                                  </xsl:if>
                              </td>
                            </tr>
                          </table>
                        </td>
                        <td width="40%" valign="top">
                          <xsl:if test="$membership_required = 'Y'">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                              <tr>
                                <td class="Label" align="right" width="28%">Membership Number:</td>
                                <td style="padding-left:.5em;"><input name="membership_number" id="membership_number" type="text" size="30" maxlength="50" value="{$deliveryData/membership_number}"/></td>
                              </tr>
                              <tr id="membership_number_validation_message_row" style="display:none">
                                <td/>
                                <td id="membership_number_validation_message_cell" class="RequiredFieldTxt" style="padding-left:.5em;"/>
                              </tr>
                              <tr>
                                <td class="Label" align="right">Membership<br/>First Name:</td>
                                <td style="padding-left:.5em;"><input name="membership_first_name" id="membership_first_name" type="text" size="30" maxlength="20" value="{$deliveryData/membership_first_name}"/></td>
                              </tr>
                              <tr id="membership_first_name_validation_message_row" style="display:none">
                                <td/>
                                <td id="membership_first_name_validation_message_cell" class="RequiredFieldTxt" style="padding-left:.5em;"/>
                              </tr>
                              <tr>
                                <td class="Label" align="right">Membership<br/>Last Name:</td>
                                <td style="padding-left:.5em;"><input name="membership_last_name" id="membership_last_name" type="text" size="30" maxlength="20" value="{$deliveryData/membership_last_name}"/></td>
                              </tr>
                              <tr id="membership_last_name_validation_message_row" style="display:none">
                                <td/>
                                <td id="membership_last_name_validation_message_cell" class="RequiredFieldTxt" style="padding-left:.5em;"/>
                              </tr>
                            </table>
                          </xsl:if>
                        </td>
                      </tr>
                    </table>

                    <table border="0" cellpadding="0" cellspacing="2" width="100%">

                        <!-- Delivery Location -->
                        <tr>
                           <td width="20%" class="Label" align="right">Delivery Location Type:</td>
                           <td width="30%" style="padding-left:.5em;">
                              <select name="page_recipient_address_type" id="page_recipient_address_type" onchange="javascript:doDeliveryTypeChange();">
                                <xsl:for-each select="ADDRESS_TYPES/ADDRESS_TYPE">
                                <option value="{CODE}" type="{TYPE}"><xsl:if test="TYPE = $deliveryData/recipient_address_type"><xsl:attribute name="selected"/></xsl:if><xsl:value-of select="DESCRIPTION"/></option>
                                </xsl:for-each>
                              </select>
                           </td>
                           <td width="50%"><span style="color:green">Select delivery location from drop down menu</span></td>
                        </tr>

                        <!-- Phone -->
                        <tr>
                          <td width="20%" class="Label" align="right">Phone Number:</td>
                          <td width="30%" style="padding-left:.5em;">
                            <input name="recipient_phone_number" id="recipient_phone_number" type="text" size="20" maxlength="20" value="{$deliveryData/recipient_phone_number}" onchangeUnformat="true"/>&nbsp;
                              <script type="text/javascript">
                                if(isUSPhoneNumber('<xsl:value-of select="$deliveryData/recipient_phone_number"/>')){
                                  document.getElementById('recipient_phone_number').value = reformatUSPhone('<xsl:value-of select="$deliveryData/recipient_phone_number"/>');

                                }else{
                                  document.getElementById('recipient_phone_number').value = '<xsl:value-of select="$deliveryData/recipient_phone_number"/>';
                                }
                              </script>
                              <span class="Label">
                                 Ext:&nbsp;<input name="recipient_extension" id="recipient_extension" type="text" size="10" maxlength="10" value="{$deliveryData/recipient_extension}"/>
                                 &nbsp;
                              </span>
                              <a id="recipientLookupLink" class="textlink" onclick="javascript:doRecipientLookup();" onkeypress="javascript:doEnterKeyAction(doRecipientLookup);">Search</a>


                            <button type="button" id="hiddenSearch" class="BigBlueButton" tabindex="9999"  style="width:1px;height:1px" accesskey="S" onclick="javascript:doRecipientLookup();"></button>

                           </td>
                           <td width="50%"><span style="color:green">Click on link to select an existing recipient or location</span></td>
                        </tr>
                        <tr id="recipient_phone_number_validation_message_row" style="display:none">
                          <td/>
                          <td id="recipient_phone_number_validation_message_cell" class="RequiredFieldTxt" style="padding-left:.5em;"/>
                        </tr>
                        <tr id="recipient_extension_validation_message_row" style="display:none">
                          <td/>
                          <td id="recipient_extension_validation_message_cell" class="RequiredFieldTxt" style="padding-left:.5em;"/>
                        </tr>

                        <!-- Recip/Deceased first name
                             Text will be set dynamically
                        -->
                        <tr>
                           <td width="20%" id="recipient_first_name_cell" class="Label" align="right"/>
                           <td width="30%" style="padding-left:.5em;">
                              <input name="recipient_first_name" id="recipient_first_name" type="text" size="20" maxlength="20" value="{$deliveryData/recipient_first_name}"/>
                              <span style="color:red">***</span>
                           </td>
                           <td width="50%">
                              <span style="color:green;">Fill in the recipient's first name; Fill in the deceased name if funeral</span>
                           </td>
                        </tr>
                        <tr id="recipient_first_name_validation_message_row" style="display:none">
                          <td/>
                          <td id="recipient_first_name_validation_message_cell" class="RequiredFieldTxt" style="padding-left:.5em;"/>
                        </tr>

                        <!-- Recip/Deceased last name
                             Text will be set dynamically
                        -->
                        <tr>
                           <td width="20%" id="recipient_last_name_cell" class="Label" align="right"/>
                           <td width="80%" colspan="2" style="padding-left:.5em;">
                              <input name="recipient_last_name" id="recipient_last_name" type="text" size="20" maxlength="20" value="{$deliveryData/recipient_last_name}"/>
                              <span style="color:red">***</span>
                           </td>
                        </tr>
                        <tr id="recipient_last_name_validation_message_row" style="display:none">
                          <td/>
                          <td id="recipient_last_name_validation_message_cell" class="RequiredFieldTxt" style="padding-left:.5em;"/>
                        </tr>

                        <!-- Business/Institution
                             Only shown if delivery location type is: Business, Funeral Home, Hospital, or Nursing Home
                        -->
                        <tr id="recipient_business_name_cell" class="Label" style="display:none">
                           <td width="20%"  align="right"  style="padding-left:.5em;">Business/Institution Name:</td>
                           <td width="80%"  colspan="2" style="padding-left:.5em;">
                              <input name="recipient_business_name" id="recipient_business_name" type="text" size="20" maxlength="50" value="{$deliveryData/recipient_business_name}"/>
                              <span style="color:red">***</span>&nbsp;&nbsp;<a id="institutionLookupLink" class="textlink" onclick="javascript:doInstitutionLookup();" onkeypress="javascript:doEnterKeyAction(doInstitutionLookup);">Lookup Institution</a>
                           </td>
                        </tr>
                        <tr id="recipient_business_name_validation_message_row" style="display:none">
                          <td/>
                          <td id="recipient_business_name_validation_message_cell" class="RequiredFieldTxt" style="padding-left:.5em;"/>
                        </tr>

                        <xsl:choose>
                          <xsl:when test="$deliveryData/special_instructions != ''">
                            <!-- Special Instructions
                                 Only shown if delivery location type is: Business, Funeral Home, Hospital, or Nursing Home
                                 and if existing special instructions exist
                            -->
                            <tr id="room_number_cell" >
                               <td width="20%" class="Label" align="right">Special Instructions:</td>
                               <td width="80%" colspan="2" style="padding-left:.5em;">
                                  <input name="special_instructions" id="special_instructions" type="text" size="50" maxlength="4000" value="{$deliveryData/special_instructions}"/>
                               </td>
                            </tr>
                          </xsl:when>
                          <xsl:otherwise>
                            <!-- Room Number/Ward
                                 Only shown if delivery location type is: Business, Funeral Home, Hospital, or Nursing Home
                            -->
                            <tr id="room_number_cell" >
                               <td width="20%" class="Label" align="right" id="room_number_label">Room Number/Ward:</td>
                               <td width="80%" colspan="2" style="padding-left:.5em;">
                                  <input name="special_instructions_room_number" id="special_instructions_room_number" type="text" size="50" maxlength="20" value=""/>
                               </td>
                            </tr>

                            <!-- Time of Service/Working Hours
                                 Only shown if delivery location type is: Business or Funeral Home
                            -->
                            <tr id="time_of_service_cell" >
                               <td width="20%" align="right" class="Label" id="time_of_service_label">Time of Service/Working Hours:</td>
                               <td width="80%" colspan="2" style="padding-left:.5em;">
                                  <select name="special_instructions_start_time" id="special_instructions_start_time" >
                                    <option value=""></option>
                                    <option value="5:00 AM">5:00 AM</option>
                                    <option value="6:00 AM">6:00 AM</option>
                                    <option value="7:00 AM">7:00 AM</option>
                                    <option value="8:00 AM">8:00 AM</option>
                                    <option value="9:00 AM">9:00 AM</option>
                                    <option value="10:00 AM">10:00 AM</option>
                                    <option value="11:00 AM">11:00 AM</option>
                                    <option value="12:00 PM">12:00 PM</option>
                                    <option value="1:00 PM">1:00 PM</option>
                                    <option value="2:00 PM">2:00 PM</option>
                                    <option value="3:00 PM">3:00 PM</option>
                                    <option value="4:00 PM">4:00 PM</option>
                                    <option value="5:00 PM">5:00 PM</option>
                                    <option value="6:00 PM">6:00 PM</option>
                                    <option value="7:00 PM">7:00 PM</option>
                                    <option value="8:00 PM">8:00 PM</option>
                                    <option value="9:00 PM">9:00 PM</option>
                                    <option value="10:00 PM">10:00 PM</option>
                                    <option value="11:00 PM">11:00 PM</option>
                                    <option value="12:00 AM">12:00 AM</option>
                                  </select>
                                  &nbsp;to
                                  <select name="special_instructions_end_time" id="special_instructions_end_time" >
                                    <option value=""></option>
                                    <option value="5:00 AM">5:00 AM</option>
                                    <option value="6:00 AM">6:00 AM</option>
                                    <option value="7:00 AM">7:00 AM</option>
                                    <option value="8:00 AM">8:00 AM</option>
                                    <option value="9:00 AM">9:00 AM</option>
                                    <option value="10:00 AM">10:00 AM</option>
                                    <option value="11:00 AM">11:00 AM</option>
                                    <option value="12:00 PM">12:00 PM</option>
                                    <option value="1:00 PM">1:00 PM</option>
                                    <option value="2:00 PM">2:00 PM</option>
                                    <option value="3:00 PM">3:00 PM</option>
                                    <option value="4:00 PM">4:00 PM</option>
                                    <option value="5:00 PM">5:00 PM</option>
                                    <option value="6:00 PM">6:00 PM</option>
                                    <option value="7:00 PM">7:00 PM</option>
                                    <option value="8:00 PM">8:00 PM</option>
                                    <option value="9:00 PM">9:00 PM</option>
                                    <option value="10:00 PM">10:00 PM</option>
                                    <option value="11:00 PM">11:00 PM</option>
                                    <option value="12:00 AM">12:00 AM</option>
                                  </select>
                               </td>
                            </tr>
                          </xsl:otherwise>
                        </xsl:choose>

                        <!-- Recipient Address 1 -->
                        <tr>
                           <td width="20%" class="Label" align="right">Street Address:</td>
                           <td width="30%" style="padding-left:.5em;">
                              <input name="recipient_address_1" id="recipient_address_1" type="text" size="30" maxlength="30" value="{$deliveryData/recipient_address_1}"/>
                              <span style="color:red">***</span>
                           </td>
                           <td width="50%"><span style="color:green">No PO Box or APO addresses</span></td>
                        </tr>
                        <tr id="recipient_address_1_validation_message_row" style="display:none">
                          <td/>
                          <td id="recipient_address_1_validation_message_cell" class="RequiredFieldTxt" style="padding-left:.5em;"/>
                        </tr>
                        <!-- Recipient Address 2 -->
                        <tr>
                           <td width="20%"/>
                           <td width="80%" colspan="2" style="padding-left:.5em;"><input name="recipient_address_2" id="recipient_address_2" type="text" size="30" maxlength="30" value="{$deliveryData/recipient_address_2}"/></td>
                        </tr>
                        <tr id="recipient_address_2_validation_message_row" style="display:none">
                          <td/>
                          <td id="recipient_address_2_validation_message_cell" class="RequiredFieldTxt" style="padding-left:.5em;"/>
                        </tr>

                        <!-- City -->
                        <tr>
                           <td width="20%" class="Label" align="right">City:</td>
                           <td width="80%" colspan="2" style="padding-left:.5em;">
                              <input name="recipient_city" id="recipient_city" type="text" size="30" maxlength="30" value="{$deliveryData/recipient_city}"/>
                              <span style="color:red">***</span>
                           </td>
                        </tr>
                        <tr id="recipient_city_validation_message_row" style="display:none">
                          <td/>
                          <td id="recipient_city_validation_message_cell" class="RequiredFieldTxt" style="padding-left:.5em;"/>
                        </tr>

                        <tr><td colspan="3">  <hr align="center" width="98%" size="1px" style="color:#000099;" /></td></tr>

                        <!-- State -->
                        <tr>
                           <td width="20%" align="right" class="Label">State/Province:</td>
                           <td width="80%" colspan="2" style="padding-left:.5em;">
                              <select name="recipient_state" id="recipient_state"/><span style="color:red">***</span>
                           </td>
                        </tr>
						<tr id="recipient_state_validation_message_row" style="display:none">
                          <td/>
                          <td id="recipient_state_validation_message_cell" class="RequiredFieldTxt" style="padding-left:.5em;"/>
                        </tr>
                        <!-- Zip Code -->
                        <tr>
                           <td width="20%"></td>
                           <td width="80%" colspan="2" style="padding-left:.5em;"><span style="color:green">If zipcode has changed or the field is empty, enter the<br/>recipient's zipcode and click button to show valid delivery dates</span></td>
                        </tr>
                        <tr>
                           <td width="20%" align="right" class="Label">Zip/Postal Code:</td>
                           <td width="80%" colspan="2" style="padding-left:.5em;">
                              <input name="recipient_zip_code" id="recipient_zip_code" type="text" size="12" maxlength="12" value="{$deliveryData/recipient_zip_code}"/>
                              <span style="color:red">***</span>
                           </td>
                        </tr>
                        <tr id="recipient_zip_code_validation_message_row" style="display:none">
                          <td/>
                          <td id="recipient_zip_code_validation_message_cell" class="RequiredFieldTxt" style="padding-left:.5em;"/>
                        </tr>

                        <!-- City Lookup -->
                        <tr>
                           <td width="20%"></td>
                           <td width="80%" colspan="2" style="padding-left:.5em;"><a name="cityLookupLink" class="textlink" onclick="javascript:doCityLookup();" onkeypress="javascript:doEnterKeyAction(doCityLookup);">Lookup by City</a></td>
                        </tr>

                        <!-- Country -->
                        <tr>
                           <td width="20%" align="right" class="Label">Country:</td>
                           <td width="80%" colspan="2" style="padding-left:.5em;">
                             <select name="recipient_country" id="recipient_country" onchange="javascript:doCountryChange(event);">
                               <xsl:for-each select="COUNTRIES/COUNTRY">
                                 <option value="{id}"><xsl:if test="id = $selectedCountry"><xsl:attribute name="SELECTED"/></xsl:if><xsl:value-of select="description"/></option>
                               </xsl:for-each>
                             </select>
                           </td>
                        </tr>

                        <tr><td colspan="3"><hr align="center" width="98%" size="1px" style="color:#000099;"/></td></tr>

                        <!-- Card Message -->
                        <tr>
                           <td width="20%" class="Label" align="right" style="vertical-align:top">Card Message:</td>
                           <td width="30%" style="padding-left:.5em;"><textarea name="card_message" id="card_message" rows="3" cols="35" ><xsl:value-of select="$deliveryData/card_message"/></textarea></td>
                           <td width="50%">
                              <div style="vertical-align:middle">
                                 <span style="float:left"><button type="button" class="arrowbutton" id="arrowButton" onclick="javascript:moveMessage();">3</button></span>
                                 <select name="stock_messages" id="stock_messages" size="3" ondblclick="javascript:moveMessage();" onchangeIgnore="true">
                                   <xsl:attribute name="multiple"/>
                                   <xsl:for-each select="GIFT_MESSAGES/GIFT_MESSAGE">
                                   <option value="{TEXT}"><xsl:value-of select="TEXT"/></option>
                                   </xsl:for-each>
                                 </select>
                              </div>
                           </td>
                        </tr>
                        <tr id="card_message_validation_message_row" style="display:none">
                          <td width="20%"/>
                          <td width="80%" id="card_message_validation_message_cell" class="RequiredFieldTxt" style="padding-left:.5em;"/>
                        </tr>
                        <tr>
                           <td colspan="3" align="center"><button type="button" class="BlueButton" style="width:150px;" id="spellCheckButton" onclick="javascript:doSpellCheckAction(card_message.value, 'card_message');">Spell Check Gift Message</button></td>
                        </tr>

                        <!-- Signature -->
                        <tr>
                           <td width="20%" align="right" class="Label">Signature:</td>
                           <td width="80%" colspan="2" style="padding-left:.5em;"><input name="card_signature" id="card_signature" type="text" size="30" maxlength="240" value="{$deliveryData/card_signature}"/></td>
                        </tr>
                        <tr id="card_signature_validation_message_row" style="display:none">
                          <td/>
                          <td id="card_signature_validation_message_cell" class="RequiredFieldTxt" style="padding-left:.5em;"/>
                        </tr>

                        <!-- Release Sender's Name
                             Only shown if delivery location type is: Funeral Home or Hospital
                        -->
                        <tr id="release_info_indicator_cell" style="display:none">
                           <td width="20%" class="Label" align="right">Release Sender's Name:</td>
                           <td width="80%" colspan="2" style="padding-left:.5em;">
                              <input name="release_info_indicator" id="release_info_indicator" type="checkbox" value="Y">
                                <xsl:if test="$deliveryData/release_info_indicator = 'Y'">
                                  <xsl:attribute name="checked"><xsl:value-of select="'true'"/></xsl:attribute>
                                </xsl:if>
                              </input>
                           </td>
                        </tr>
                        <tr>
                           <td colspan="3"><hr align="center" width="98%" size="1px" style="color:#000099;" /></td>
                        </tr>

                        <!-- Alt Contact -->
                        <tr>
                           <td width="20%" align="right" class="Label">Alternate Contact Name:</td>
                           <td width="80%" colspan="2" style="padding-left:.5em;"><input name="alt_contact_last_name" id="alt_contact_last_name" type="text" size="30" maxlength="40" value="{$deliveryData/alt_contact_last_name}"/></td>
                        </tr>

                        <!-- Alt Phone -->
                        <tr>
                           <td width="20%" align="right" class="Label">Alternate Phone Number:</td>
                           <td width="80%" colspan="2" style="padding-left:.5em;">
                           <input name="alt_contact_phone_number" id="alt_contact_phone_number" type="text" size="20" maxlength="20" value="{$deliveryData/alt_contact_phone_number}" onchangeUnformat="true"/>&nbsp;
                           <script type="text/javascript">
                              if(isUSPhoneNumber('<xsl:value-of select="$deliveryData/alt_contact_phone_number"/>')){
                                document.getElementById('alt_contact_phone_number').value = reformatUSPhone('<xsl:value-of select="$deliveryData/alt_contact_phone_number"/>');
                               }else{
                                document.getElementById('alt_contact_phone_number').value = '<xsl:value-of select="$deliveryData/alt_contact_phone_number"/>';
                               }
                              </script>
                              <span class="Label">Ext:&nbsp;</span>
                              <input name="alt_contact_extension" id="alt_contact_extension" type="text" size="10" maxlength="10" value="{$deliveryData/alt_contact_extension}"/>
                           </td>
                        </tr>
                        <tr id="alt_contact_phone_number_validation_message_row" style="display:none">
                          <td/>
                          <td id="alt_contact_phone_number_validation_message_cell" class="RequiredFieldTxt" style="padding-left:.5em;"/>
                        </tr>
                        <tr id="alt_contact_extension_validation_message_row" style="display:none">
                          <td/>
                          <td id="alt_contact_extension_validation_message_cell" class="RequiredFieldTxt" style="padding-left:.5em;"/>
                        </tr>
                        
                     </table>
                  </td>
               </tr>
            </table>
         </td>
      </tr>
    </table>
   </form>


   <!-- Bottom buttons -->
   <div style="margin-left:7px;margin-top:0px;width:99%;align:center;">
      <div style="text-align:right;padding-top:.5em;">
         <span style="padding-right:2em;">
          <button type="button" id="custUpdateProductBottom" class="BigBlueButton" style="width:110px;" accesskey="t" onclick="javascript:doContinueUpdateAction();">Con(t)inue Update<br/>Product</button>
         </span>
         <button type="button" id="custCancelBottom" class="BigBlueButton" style="width:110px;" accesskey="C" onclick="javascript:doCancelUpdateAction();">(C)ancel Update</button>
      </div>
   </div>

  <!-- Footer -->
  <xsl:call-template name="footer"/>

  <!-- end mainContent Div -->
  </div>

  <!-- Processing message div -->
  <div id="waitDiv" style="display:none">
    <table id="waitTable" width="98%" border="3" height="300px" align="center" cellpadding="30" cellspacing="1" bordercolor="#006699">
      <tr>
        <td width="100%">
          <table cellspacing="0" cellpadding="0" style="width:100%;height:300px">
            <tr>
              <td id="waitMessage" align="right" width="50%" class="waitMessage"></td>
              <td id="waitTD" width="50%" class="waitMessage"></td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
    <xsl:call-template name="footer"/>
  </div>
  </body>
</html>

</xsl:template>
<xsl:template name="addHeader">
   <xsl:call-template name="header">
      <xsl:with-param name="headerName" select="'Update Delivery Information'"/>
      <xsl:with-param name="showTime" select="true()"/>
      <xsl:with-param name="showCSRIDs" select="true()"/>
      <xsl:with-param name="dnisNumber" select="$call_dnis"/>
      <xsl:with-param name="cservNumber" select="$call_cs_number"/>
      <xsl:with-param name="indicator" select="$call_type_flag"/>
      <xsl:with-param name="brandName" select="$call_brand_name"/>
      <xsl:with-param name="overrideRTQ" select="Y"/>
      <xsl:with-param name="cdispEnabledFlag" select="$cdisp_enabledFlag"/>
   </xsl:call-template>
</xsl:template>
</xsl:stylesheet>
