<!DOCTYPE ACDemo [
  <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<!-- External templates -->
	<xsl:import href="../securityanddata.xsl" />
	<xsl:import href="../header.xsl" />
	<xsl:import href="../footer.xsl" />
	<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>
	<!-- Keys -->
	<xsl:key name="pageData" match="/root/pageData/data" use="name" />
	<xsl:key name="security" match="/root/security/data" use="name" />
	<xsl:key name="deliveryDates" match="/root/deliveryDatesList/date" use="name" />
	
	<xsl:variable name="deliveryDateExistsInPASVar" select="key('pageData','deliveryDateExistsInPAS')/value"/>
	<!-- <xsl:variable name="selectedDeliveryDate" select="key('pageData','selectedDeliveryDate')/value"/> 
	<xsl:variable name="selectedCancelId" select="key('pageData','selectedCancelId')/value"/>-->
	<xsl:variable name="liveFTD" select="key('pageData','liveFTD')/value"/>
	<xsl:variable name="successMsg" select="key('pageData','success_message')/value"/>
	<xsl:variable name="isOrigNewProdSame" select="key('pageData','isOrigNewProdSame')/value"/>
	
	<xsl:output method="html" indent="yes" />
	<xsl:template match="/root">
		<html>
			<head>
				<title>FTD - Phoenix Order</title>
				<link rel="stylesheet" type="text/css" href="css/ftd.css" />
				<link rel="stylesheet" type="text/css" href="css/calendar.css" />
				<style type="text/css">
					.blueBanner{
					color: #006699;
					height:5px;
					}
					img{cursor:hand;}
				</style>
		<script language="javascript" src="js/FormChek.js" />
		<script language="javascript" src="js/util.js" />
		<script language="javascript" src="js/commonUtil.js" />
		<script type="text/javascript" language="javascript">
		
			var liveFTD = '<xsl:value-of select="$liveFTD"/>';
			var deliveryDateExistsInPAS = '<xsl:value-of select="$deliveryDateExistsInPASVar"/>'
<![CDATA[			
			function doSubmitPhoenixOrderAction(){
			
			var form = document.phoenixOderForm;
			
				onDeliveryDateChange();
				
				var emailType = document.getElementById("emailType").value;
				
				if(emailType == 'stock_email'){
					if(Trim(document.getElementById("newSubject").value) == "")
					{
						alert("Please enter a subject");
					  return false;
					}
					else if(Trim(document.getElementById("newBody").value) == "")
					{
						alert("Please enter message body");
					  return false;
					}
				}
				
							
				if(document.getElementById("newBody").value.indexOf('~') > 0 || document.getElementById("newSubject").value.indexOf('~') > 0)
				    {
				    	alert("Please replace all tokens before sending.");
				    	return false;
				    }
				
				disableButtons();
				if(validatePhoenixForm()){
					
					var zipCode = document.getElementById("recip_zip_code").value;
					var prodId = document.getElementById("phoenix_product_id").value;
					var delDateElement = document.getElementById("deliveryDates");
					var addons = document.getElementById("addons").value;
					var sourceCode = document.getElementById("sourceCode").value;
					
					var delDate = null;
					var submitFlag = true;
					if(deliveryDateExistsInPAS != 'Y'){
						delDate = delDateElement.options[delDateElement.selectedIndex].value;
					}else{
						delDate = delDateElement.value;
					}
					
					var data = validateShipDate(zipCode, prodId, delDate, addons, sourceCode);
					
					if(data == null){
						submitFlag = false;
					}else{
						var shipDate = data.ship_date;
						var shipMethod = data.ship_method;
						if(shipDate == null || shipMethod == null){
							submitFlag = false;							
						}else{
							document.getElementById("ship_date").value = shipDate;
							document.getElementById("ship_method").value = shipMethod;
						}
						
					}
					
					
					if(!submitFlag){
						var modalArguments = new Object();
						modalArguments.modalText = "The order is not Phoenix Eligible";
    						window.showModalDialog("alert.html", modalArguments, 'dialogWidth:300px; dialogHeight:200px; center:yes; status=no; help=no; resizable:no; scroll:yes');
    						enableButtons();
					}else{
						var emailType;
						var cancelId;
						var cancelText;
						if(document.getElementById("cancelId") != null){
							cancelId = document.getElementById("cancelId").options[document.getElementById("cancelId").selectedIndex].value;
							cancelText = document.getElementById("cancelText").value;
						}
						if(document.getElementById("emailType") != null){
							emailType = document.getElementById("emailType").options[document.getElementById("emailType").selectedIndex].value;
						}
						
						var emailSubject = document.getElementById('newSubject').value;
			    		var emailBody =  document.getElementById('newBody').value;
			    		
			    		var url= "phoenixOrder.do?action=submit"
						+"&deliveryDate="+delDate
						+"&cancelId="+cancelId
						+"&cancelText="+cancelText
						+"&emailType="+emailType
						+"&emailSubject="+emailSubject
						+"&emailTitle="+document.phoenixOderForm.stockEmail.options[document.phoenixOderForm.stockEmail.selectedIndex].text
						+getSecurityParams(false);
						
						var toSubmit = document.getElementById("phoenixOderForm");
					
						toSubmit.action = url;
				
						toSubmit.submit();	
						
						
						
						
					} 
					
				}else{
					enableButtons();
					return true;
				}
				
			}
			
	    	function validatePhoenixForm(){
	    		if(deliveryDateExistsInPAS != 'Y'){
	    			document.getElementById("deliverydate_error_msg").innerHTML  = "";
	    		}
	    		if(liveFTD == "Y"){
	    			document.getElementById("cancelid_error_msg").innerHTML="";
	    			document.getElementById("canceltxt_error_msg").innerHTML="";
	    		}
	    		document.getElementById("emailtype_error_msg").innerHTML="";
	    		
				var delDateElement = document.getElementById("deliveryDates");
				
				if(deliveryDateExistsInPAS != 'Y'){
					var delDate = delDateElement.options[delDateElement.selectedIndex].value;
				}else{
					var delDate = delDateElement.value;
				}
				
				var flag = true;
				if(delDate == "0" || delDate == null || delDate == ""){
	    			document.getElementById("deliverydate_error_msg").innerHTML  = "Select a Delivery date";
	    			flag = false;
	    		}
	    		if(liveFTD == "Y"){
	    			var cancelFlagElement = document.getElementById("cancelId");
					var cancelFlag = cancelFlagElement.options[cancelFlagElement.selectedIndex].value;
	    			var cancelTxt = document.getElementById("cancelText").value;
	    			if(cancelFlag == ""){
	    				document.getElementById("cancelid_error_msg").innerHTML = "Cancel Florist Order Selection is Required";
	    				flag = false;
	    			}
	    			else if(cancelFlag == "Yes"){
		    			if(cancelTxt == null || cancelTxt == "" ){
		    				document.getElementById("canceltxt_error_msg").innerHTML = "Cancel Message Text is Required";
		    				flag = false;
		    			} 
	    			}
	    		}
	    		var emailTypeElement = document.getElementById("emailType");
	    		var emailType = emailTypeElement.options[emailTypeElement.selectedIndex].value;
	    		if(emailType == "" || emailType == null){
	    			document.getElementById("emailtype_error_msg").innerHTML = "Choose an email option";
	    			flag = false;
	    		}
	    		
	    		return flag;
	    	}
	    	
	    	function doSubmitCancelPhoenixOrder(eon, guid, cust_id){
				var url = "customerOrderSearch.do?action=customer_account_search"
			    + "&order_number=" + eon
			    + "&order_guid=" + guid
			    + "&customer_id=" + cust_id
			    + getSecurityParams(false);
					
				var toSubmit = document.getElementById("phoenixOderForm");
					
				toSubmit.action = url;
				
				toSubmit.submit();	
			
			
			} 	
			
			function textChange(){
				document.phoenixOderForm.change_flag.value="Y";
				document.phoenixOderForm.spellCheckFlag.value="Y";
			}
			
			function doSpellCheck()
			{
					var form = document.phoenixOderForm;
					var value = escape(form.newBody.value).replace(/\+/g,"%2B");
					var url = "spellCheck.do?content=" + value + "&callerName=newBody"+
              					"&securitytoken="+form.securitytoken.value+
              					"&context="+form.context.value;
              		
    			 	window.open(url,"Spell_Check","toolbar=no,resizable=yes,scrollbars=yes,width=500,height=300,help=no");
   					form.spellCheckFlag.value = "N"; 
			}
			
			
			//***************************************************************************************
			//  doRefreshEmailContentAction() - functionality via AJAX
			//***************************************************************************************
			function doRefreshEmailContentAction() 
			{
			  form = document.phoenixOderForm;
			  form.action_type.value = "load_content_ajax";
			  form.message_id.value = document.phoenixOderForm.stockEmail.options[document.phoenixOderForm.stockEmail.selectedIndex].value;
			  form.message_title.value = document.phoenixOderForm.stockEmail.options[document.phoenixOderForm.stockEmail.selectedIndex].text;
			  form.order_detail_id.value = document.getElementById("order_detail_id").value;
			  form.company_id.value = document.getElementById("company_id").value;
			  form.message_type.value = "Email";
				 if(form.message_id.value==0)
				  {
				    alert("Select a Stock Email and then Refresh");
				    return;
				  }
			
			  //  Send a POST request via AJAX to recalculate the order
			  var url = 'loadSendMessage.do';
			  var parameters = getFormNameValueQueryString(form);   
			  parameters = parameters.substring(1);
			  xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
			  xmlHttp.onreadystatechange = evalRefreshEmailContentAction; 
			  xmlHttp.open("POST", url , true); 
			  xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			  xmlHttp.setRequestHeader("Content-length", parameters.length);
			  xmlHttp.send(parameters);
			
			}
			
			
			//***************************************************************************************
			//  evalRefreshEmailContentAction() - Evaluates response from functionality via AJAX
			//***************************************************************************************
			function evalRefreshEmailContentAction() 
			{
			  if (xmlHttp.readyState == 4 || xmlHttp.readyState == "complete")
			  {
			    // Obtain response data
			    var xml = getXMLobjXML(xmlHttp.responseText);
			    
			    var emailSubject = xml.selectSingleNode("/PointOfContactVO/emailSubject").text;
			    var emailBody = xml.selectSingleNode("/PointOfContactVO/body").text;
			
			    // Display refreshed content
			    document.getElementById('newSubject').value = emailSubject;
			    document.getElementById('newBody').value = emailBody;
			  }
			
			}
			
			function getXMLobjXML(xmlToLoad)
				{
				  var xml = new ActiveXObject("Msxml2.FreeThreadedDOMDocument");
				  xml.async = false;
				  xml.resolveExternals = false;
				  try
				  {
				    xml.loadXML(xmlToLoad);
				    return(xml)
				  }
				  catch(e)
				  {
				    throw(xml.parseError.reason)
				  }
				
				}

			function emailTypeChange(){
				var emailType = document.getElementById("emailType").value;
				
				if(emailType == 'stock_email'){
					document.getElementById("newBody").disabled = false;
					document.getElementById("newSubject").disabled = false;
					document.getElementById("stockEmail").disabled = false;
					document.getElementById("spellCheck").disabled = false;
					document.getElementById("refreshButton").disabled = false;
				}else{
					document.getElementById("stockEmail").selectedIndex = 0;
					document.getElementById("newBody").value = "";
					document.getElementById("newSubject").value = "";					
					document.getElementById("newBody").disabled = true;
					document.getElementById("newSubject").disabled = true;
					document.getElementById("stockEmail").disabled = true;
					document.getElementById("spellCheck").disabled = true;
					document.getElementById("refreshButton").disabled = true;
				}
			}
			
			function onDeliveryDateChange(){
				var date = document.getElementById("deliveryDates").value;
				if(document.getElementById(date) != null){
					var productString = document.getElementById(date).value;
					var products = productString.split(",");
					document.getElementById("phoenix_product_id").value = products[0]; 
				}
			}
			
			
	    	
	    		
]]>	    			 
	    </script>
		
		</head>
			
			<body>
				<xsl:call-template name="addHeader" />
				<iframe id="cancelPhoenixFrame" width="0px" height="0px" border="0"/>
				<iframe id="submitPhoenixFrame" width="0px" height="0px" border="0"/>
				<form name="phoenixOderForm" id="phoenixOderForm" action="phoenixOrder.do" action_type=""	method="post">
				
				 <!-- Cancel Update IFrame -->
   				
   				<xsl:call-template name="securityanddata" />
				<xsl:call-template name="decisionResultData" />
				
				<input type="hidden" name="customer_id" id="customer_id" value="{key('pageData','customer_id')/value}"/>
  				<input type="hidden" name="external_order_number" id="external_order_number" value="{key('pageData','external_order_number')/value}"/>
  				<input type="hidden" name="master_order_number" id="master_order_number" value="{key('pageData','master_order_number')/value}"/>
  				<input type="hidden" name="order_guid" id="order_guid" value="{key('pageData','order_guid')/value}"/>
  				<input type="hidden" name="order_detail_id" id="order_detail_id" value="{key('pageData','order_detail_id')/value}"/>
  				<input type="hidden" name="live_ftd" id="live_ftd" value="{key('pageData','liveFTD')/value}"/>
  				<input type="hidden" name="phoenix_product_id" id="phoenix_product_id" value="{key('pageData','phoenix_product_id')/value}"/>
  				<input type="hidden" name="recip_zip_code" id="recip_zip_code" value="{key('pageData','recip_zip_code')/value}"/>
  				<input type="hidden" name="ship_method" id="ship_method" value="{key('pageData','ship_method')/value}"/>
  				<input type="hidden" name="ship_date" id="ship_date" value="{key('pageData','ship_date')/value}"/>
  				<input type="hidden" name="addons" id="addons" value="{key('pageData','addons')/value}"/>
  				<input type="hidden" name="message_title" id="message_title" value=""/>
  				<input type="hidden" name="message_type" id="message_type" value=""/>
            	<input type="hidden" name="message_id" id="message_id" value=""/>
            	<input type="hidden" name="action_type" id="action_type" value=""/>
            	<input type="hidden" name="spellCheckFlag" id="spellCheckFlag" value="N"/>
            	<input type="hidden" name="change_flag" id="change_flag" value="N"/>
            	<input type="hidden" name="company_id" id="company_id" value="{key('pageData','companyId')/value}"/>
            	<input type="hidden" name="sourceCode" id="sourceCode" value="{key('pageData','sourceCode')/value}"/>
  				
  				<xsl:for-each select="/root/deliveryDatesList/dDate">
					<input type="hidden" name="{name}"  id="{name}" value="{value}"/>
				</xsl:for-each>
  				
				<!-- /Top buttons -->
   					<div style="margin-left:7px;margin-top:0px;width:99%;align:center;">
						<div style="text-align:right">
							<span style="padding-right:2em;">
								<button type="button" tabindex="-1" id="submitButton" accesskey="S" class="BigBlueButton" style="width:110px;" onclick="javascript:doSubmitPhoenixOrderAction();">(S)ubmit</button>
							</span>
								<button type="button" tabindex="-1" id="cancelButton" accesskey="C" class="BigBlueButton" style="width:110px;" onclick="javascript:doCancelPhoenixOrderAction();">(C)ancel</button>
						</div>
					</div>
				<!-- /Top buttons -->
				<br/>
				<br/>
				
					<table width="90%" border="1" align="center" cellpadding="1"
						cellspacing="1" bordercolor="#006699">
						<tr>
							<td>
								<table>
									<tr>
										<td width="23%" align="Left"
											style="font-size: 10pt; font-weight: bold; color:green;">This order is Phoenix Eligible</td>
									</tr>
									<!-- <xsl:for-each select="pageData"> -->									
									<xsl:if test="$deliveryDateExistsInPASVar='Y'">	
									<tr>
										<td width="23%" align="Left"
											style="font-size: 10pt; font-weight: bold; color:#003366;">Original Delivery Date : <xsl:value-of select="key('pageData','deliveryDate')/value"/></td>
										<input type="hidden" name="deliveryDates" id="deliveryDates" value="{key('pageData','deliveryDate')/value}"/>
											
									</tr>
									</xsl:if>	
									<xsl:if test="$isOrigNewProdSame='Y'">	
										<tr>
										<td width="23%" align="Left"
											style="font-size: 10pt; font-weight: bold; color:red;">This order will be resent as the exact same item with no upgrade via a vendor delivery. No email will be sent.</td>										
										</tr>
									</xsl:if>	
									<!-- </xsl:for-each> -->							
								</table>
							</td>
						</tr>

						<tr>
							<td colspan="5" width="100%" align="center">
								<table width="100%" cellpadding="1" cellspacing="1">
								<!-- <xsl:for-each select="/root/pageData/data"> -->								 	
   									<xsl:if test="$deliveryDateExistsInPASVar='N'">					
									<tr>
										<td width="23%" align="left" style="font-size: 10pt; font-weight: bold; color:#003366;">
											Choose a New Delivery Date : 
										</td>
										<td>&nbsp;</td>
										
										<td align="left" style="font-size: 16px;">
										
										<select name="deliveryDates" size="1" id="deliveryDates" onChange="onDeliveryDateChange()">
					                      <option value="0">-Select a Date-</option>
					                      	<xsl:for-each select="/root/deliveryDatesList/dDate">
					                      		<option value="{name}"><xsl:value-of select="name"/></option>
					                      	</xsl:for-each>
					                    </select>
										
										</td>
										<td id="deliverydate_error_msg" class="RequiredFieldTxt" style="padding-left:.5em;"/>
									</tr>									
									</xsl:if>
									
										 	
   									<!-- <xsl:if test="name='liveFTD'and value='Y'"> -->
   									<xsl:if test = "key('pageData','liveFTD')/value='Y'">		
									<tr>
										<td width="23%" align="left" style="font-size: 10pt; font-weight: bold; color:#003366;">
										 	Do you want to Cancel Florist Order? : 
										</td>
										<td>&nbsp;</td>
										<td align="left" style="font-size: 16px;">
											<select name="cancelId" size="1" id="cancelId">
												<option value="">-Select One-</option>
												<option value="Yes">Yes</option>
												<option value="No">No</option>
											</select>
										</td>
										<td id="cancelid_error_msg" class="RequiredFieldTxt" style="padding-left:.5em;"/>
									</tr>
									<tr>
										<td width="23%" align="left" style="font-size: 10pt; font-weight: bold; color:#003366;">
									 		Send Cancel Text : 
										</td>
										<td>&nbsp;</td>
										<td align="left">
											<textarea name="cancelText" id="cancelText"  rows="4" cols="50"><xsl:value-of select="key('pageData','cancelText')/value"/></textarea>
										</td>
										<td id="canceltxt_error_msg" class="RequiredFieldTxt" style="padding-left:.5em;"/>
									</tr>
									</xsl:if>
									<!-- </xsl:for-each> -->				
									<tr>
										<td width="23%" align="left" style="font-size: 10pt; font-weight: bold; color:#003366;">
									 		Select Email to Send to the Customer : 
										</td>
										
										<td>&nbsp;</td>
										<td align="left">
											<select name="emailType" size="1" id="emailType" onchange="emailTypeChange()">
												<option value="">-Select One-</option>
					                      			<xsl:for-each select="/root/emailTypeList/emailType">
					                      		<option value="{name}"><xsl:value-of select="value"/></option>
					                      	</xsl:for-each>
					                    	</select>
										</td>
										<td id="emailtype_error_msg" class="RequiredFieldTxt" style="padding-left:.5em;"/>	
									</tr>
									<tr>
										<td colspan="7">&nbsp;</td>
									</tr>
									<tr>
										<td width="23%" align="left" style="font-size: 10pt; font-weight: bold; color:#003366;">
									 		Send Stock Email : 
										</td>
										<td width="15%" align="right" style="font-weight:bold">Stock Email:</td>
										<td colspan="3" align="left">
											 <select name="stockEmail" id="stockEmail" disabled="true" style="width:300px;">
												<option value="0">-select one-</option>
													<xsl:for-each select="/root/StockMessageTitles/Title">
															<option value="{stock_email_id}">
															<xsl:value-of select="title"/>- <xsl:value-of select="subject"/></option>
													</xsl:for-each>
												</select> 
													&nbsp;
												<button type="button" id="refreshButton"  class="blueButton" accesskey="R" disabled="true" name="refreshButton" onclick="doRefreshEmailContentAction()" style="width:55px">(R)efresh</button>
										</td>
										<td id="emailtype_error_msg" class="RequiredFieldTxt" style="padding-left:.5em;"/>	
									</tr>
									<tr>
										<td width="23%" align="left" style="font-size: 10pt; font-weight: bold; color:#003366;">
									 		&nbsp; 
										</td>
										<td width="15%" align="right" style="font-weight:bold">Subject:</td>
										<!-- <td class="label" align="right">Subject:</td> -->
										<td colspan="3" align="left">
											<input size="85" id="newSubject" type="text" value="" disabled="true"/>
											<!-- Subject:&nbsp;<input size="85" onchange="textChange()" name="newSubject" id="newSubject" type="text" value="{root/PointOfContactVO/emailSubject}"/> -->
										</td>
									</tr>
									
									<tr>
										<td width="23%" align="left" style="font-size: 10pt; font-weight: bold; color:#003366;">
									 		&nbsp; 
										</td>
										<td>&nbsp;</td>
										<td colspan="3" align="left">
											<textarea onchange="textChange()" name="newBody" rows="9" cols="100" disabled="true">
												
											</textarea>
											&nbsp;
											<button type="button" id="spellCheck" accesskey="P" disabled="true" class="BigBlueButton" style="width:90px;" onclick="javascript:doSpellCheck();">S(p)ell Check</button>
										</td>
									</tr>
									<tr>
										<td width="23%" align="left" style="font-size: 10pt; font-weight: bold; color:#003366;">
									 		&nbsp; 
										</td>
										<td width="15%" align="right" style="font-weight:bold">Add Order Comment(s):<br/>(optional)</td>
										<!-- <input type="hidden" name="orderComments" id="orderComments" value=""/> -->
										<td colspan="3" align="left">
											<textarea  name="orderComments" id="orderComments" rows="9" cols="100">
												
											</textarea>
										</td>
									</tr>
									
								</table>
							</td>
						</tr>
						
					</table>
					<br />
					<br />
					<table  width="90%" align="center">
					<tr><td width="90%" align="center" style="font-size: 10pt; font-weight: bold; color:red;">
						Once submitted, please allow several minutes for Phoenix processing to complete
					</td></tr>
					</table>
					<br />
					<br />

					<!-- /bottom buttons -->
   					<div style="margin-left:7px;margin-top:0px;width:99%;align:center;">
						<div style="text-align:right">
							<span style="padding-right:2em;">
								<button type="button" tabindex="-1" id="submitButton" accesskey="S" class="BigBlueButton" style="width:110px;" onclick="javascript:doSubmitPhoenixOrderAction();">(S)ubmit</button>
							</span>
								<button type="button" tabindex="-1" id="cancelButton" accesskey="C" class="BigBlueButton" style="width:110px;" onclick="javascript:doCancelPhoenixOrderAction();">(C)ancel</button>
						</div>
					</div>
					<!-- /bottom buttons -->

				</form>

				<!-- Footer -->
				<xsl:call-template name="footer" />

				<div id="waitDiv" style="display:none">
					<table id="waitTable" width="98%" border="3" height="300px"
						align="center" cellpadding="30" cellspacing="1" bordercolor="#006699">
						<tr>
							<td width="100%">
								<table cellspacing="0" cellpadding="0"
									style="width:100%;height:300px">
									<tr>
										<td id="waitMessage" align="right" width="50%" class="waitMessage"></td>
										<td id="waitTD" width="50%" class="waitMessage"></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					<xsl:call-template name="footer" />
				</div>
			</body>
		</html>


	</xsl:template>
	<xsl:template name="addHeader">
		<xsl:call-template name="header">
			<xsl:with-param name="headerName" select="'Phoenix Order'" />
			<xsl:with-param name="showTime" select="true()" />
		</xsl:call-template>
	</xsl:template>
</xsl:stylesheet>