<!DOCTYPE ACDemo [
  <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<!-- External templates -->
<xsl:import href="../header.xsl"/>
<xsl:import href="../footer.xsl"/>
<xsl:import href="../securityanddata.xsl"/>
<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>
<xsl:import href="cusProduct.xsl"/>

<!-- Keys -->
<xsl:key name="addOn" match="/root/Addons/Addon" use="addon_id"/>
<xsl:key name="pageData" match="/root/pageData/data" use="name"/>
<xsl:key name="security" match="/root/security/data" use="name"/>

<xsl:variable name="product" select="/root/PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT"/>
<xsl:variable name="rewardType" select="key('pageData','reward_type')/value"/>
<xsl:variable name="requesteddeliverydate" select="key('pageData','delivery_date')/value"/>

<xsl:variable name="standardLabel" select="key('pageData','floralLabelStandard')/value"/>
<xsl:variable name="deluxeLabel" select="key('pageData','floralLabelDeluxe')/value"/>
<xsl:variable name="premiumLabel" select="key('pageData','floralLabelPremium')/value"/>

<xsl:output method="html" indent="yes"/>
<xsl:template match="/root">

<html>
<head>
  <title>FTD - Product List</title>
  <link rel="stylesheet" type="text/css" href="css/ftd.css"/>
  <script language="javascript" src="js/tabIndexMaintenance.js"/>
  <script type="text/javascript" language="javascript" src="js/commonUtil.js"/>
  <script type="text/javascript" language="javascript" src="js/util.js"></script>
  <script type="text/javascript" language="javascript" src="js/updateProductList.js"/>
  <script type="text/javascript" language="javascript">
  /*
   *  Global variables
   */
    var product_images = "<xsl:value-of select="key('pageData', 'product_images')/value"/>";
    var hasProducts = <xsl:value-of select="boolean(count(PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT)!=0)"/>;
    var totalProducts = <xsl:value-of select="count(PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT)"/>
    <![CDATA[
    ]]>
  </script>
  <style type="text/css">
  img {
    cursor:hand;
  }
  .colHeaderCenter{
    background-color:#4169E1;
    color:#FFFFFF;
    font-weight:bold;
  }
  </style>
</head>
<!---->
<body onload="javascript:init();">
 <!-- Header template -->
    <xsl:call-template name="header">
      <xsl:with-param name="headerName" select="'Update Product List'"/>
      <xsl:with-param name="showExitButton" select="true()"/>
      <xsl:with-param name="showTime" select="true()"/>
      <xsl:with-param name="showCSRIDs" select="true()"/>
      <xsl:with-param name="dnisNumber" select="$call_dnis"/>
      <xsl:with-param name="cservNumber" select="$call_cs_number"/>
      <xsl:with-param name="indicator" select="$call_type_flag"/>
      <xsl:with-param name="brandName" select="$call_brand_name"/>
      <xsl:with-param name="overrideRTQ" select="Y"/>
      <xsl:with-param name="cdispEnabledFlag" select="$cdisp_enabledFlag"/>
    </xsl:call-template>

    <!-- Content div -->
    <div id="mainContent" style="display:block">

    <!-- Customer Product data -->
    <xsl:call-template name="cusProduct">
      <xsl:with-param name="subheader" select="subheader"/>
      <xsl:with-param name="displayContinue" select="false()"/>
      <xsl:with-param name="displayComplete" select="false()"/>
      <xsl:with-param name="displayCancel" select="true()"/>
    </xsl:call-template>

    <form name="ProductListForm" method="post" action="">
    <xsl:call-template name="securityanddata"/>
    <xsl:call-template name="decisionResultData"/>

    <!-- Customer Product params -->
    <xsl:call-template name="cusProduct-params">
      <xsl:with-param name="subheader" select="subheader"/>
    </xsl:call-template>

    <iframe id="checkLock" width="0px" height="0px" border="0"/>
    <iframe id="cancelUpdateFrame" width="0px" height="0px" border="0"/>

    <!-- old fields left over from scrub -->
    <input type="hidden" name="search_type" id="search_type" value="{key('pageData', 'search_type')/value}"/>
    <input type="hidden" name="sort_type" id="sort_type" value="{key('pageData', 'sort_type')/value}"/>
    <input type="hidden" name="jcp_flag" id="jcp_flag" value="{key('pageData', 'jcp_flag')/value}"/>
    <input type="hidden" name="orig_bear" id="orig_bear" value="{key('addOn', 'B')/addon_id}"/>
    <input type="hidden" name="orig_bear_quantity" id="orig_bear_quantity" value="{key('addOn', 'B')/addon_qty}"/>
    <input type="hidden" name="orig_baloon" id="orig_baloon" value="{key('addOn', 'A')/addon_id}"/>
    <input type="hidden" name="orig_baloon_quantity" id="orig_baloon_quantity" value="{key('addOn', 'A')/addon_qty}"/>
    <input type="hidden" name="orig_chocolate" id="orig_chocolate" value="{key('addOn', 'C')/addon_id}"/>
    <input type="hidden" name="orig_chocolate_quantity" id="orig_chocolate_quantity" value="{key('addOn', 'C')/addon_qty}"/>
    <input type="hidden" name="orig_funeral_banner" id="orig_funeral_banner" value="{key('addOn', 'F')/addon_id}"/>
    <input type="hidden" name="orig_funeral_banner_quantity" id="orig_funeral_banner_quantity" value="{key('addOn', 'F')/addon_qty}"/>
    <input type="hidden" name="orig_greeting_card" id="orig_greeting_card" value="{Addons/Addon[starts-with(addon_id, 'RC')]/addon_id}"/>
    <input type="hidden" name="list_flag" id="list_flag" value="Y"/>
 
    <!-- new fields for modify order -->
    <input type="hidden" name="action_type" id="action_type" value=""/>
    <input type="hidden" name="category_index" id="category_index" value="{key('pageData', 'category_index')/value}"/>
    <input type="hidden" name="total_pages" id="total_pages" value="{key('pageData', 'total_pages')/value}"/>
    <input type="hidden" name="buyer_full_name" id="buyer_full_name" value="{key('pageData','buyer_full_name')/value}"/>
    <input type="hidden" name="recipient_city" id="recipient_city" value="{key('pageData','recipient_city')/value}"/>
    <input type="hidden" name="company_id" id="company_id" value="{key('pageData','company_id')/value}"/>
    <input type="hidden" name="recipient_country" id="recipient_country" value="{key('pageData', 'recipient_country')/value}"/>
    <input type="hidden" name="item_order_number" id="item_order_number" value="{key('pageData', 'item_order_number')/value}"/>
    <input type="hidden" name="external_order_number" id="external_order_number" value="{key('pageData', 'external_order_number')/value}"/>
    <input type="hidden" name="master_order_number" id="master_order_number" value="{key('pageData', 'master_order_number')/value}"/>
    <input type="hidden" name="occasion" id="occasion" value="{key('pageData', 'occasion')/value}"/>
    <input type="hidden" name="order_guid" id="order_guid" value="{key('pageData', 'order_guid')/value}"/>
    <input type="hidden" name="page_name" id="page_name" value="{key('pageData', 'page_name')/value}"/>
    <input type="hidden" name="page_number" id="page_number" value="{key('pageData', 'page_number')/value}"/>
    <input type="hidden" name="recipient_zip_code" id="recipient_zip_code" value="{key('pageData', 'recipient_zip_code')/value}"/>
    <input type="hidden" name="price_point_id" id="price_point_id" value="{key('pageData', 'price_point_id')/value}"/>
    <input type="hidden" name="source_code" id="source_code" value="{key('pageData','source_code')/value}"/>
    <input type="hidden" name="recipient_state" id="recipient_state" value="{key('pageData', 'recipient_state')/value}"/>
    <input type="hidden" name="domestic_international_flag" id="domestic_international_flag" value="{key('pageData', 'domestic_international_flag')/value}"/>
    <input type="hidden" name="domestic_srvc_fee" id="domestic_srvc_fee" value="{key('pageData', 'domestic_srvc_fee')/value}"/>
    <input type="hidden" name="international_srvc_fee" id="international_srvc_fee" value="{key('pageData', 'international_srvc_fee')/value}"/>
    <input type="hidden" name="line_number" id="line_number" value="{key('pageData', 'line_number')/value}"/>
    <input type="hidden" name="occasion_description" id="occasion_description" value="{key('pageData', 'occasion_description')/value}"/>
    <input type="hidden" name="partner_id" id="partner_id" value="{key('pageData','partner_id')/value}"/>
    <input type="hidden" name="pricing_code" id="pricing_code" value="{key('pageData','pricing_code')/value}"/>
    <input type="hidden" name="snh_id" id="snh_id" value="{key('pageData','snh_id')/value}"/>
    <input type="hidden" name="order_detail_id" id="order_detail_id" value="{key('pageData','order_detail_id')/value}"/>
    <input type="hidden" name="delivery_date" id="delivery_date" value="{key('pageData','delivery_date')/value}"/>
    <input type="hidden" name="delivery_date_range_end" id="delivery_date_range_end" value="{key('pageData','delivery_date_range_end')/value}"/>
    <input type="hidden" name="product_id " id="product_id" value="{key('pageData','product_id')/value}"/>
    <input type="hidden" name="orig_product_id" id="orig_product_id" value="{key('pageData','orig_product_id')/value}"/>
    <input type="hidden" name="product_amount" id="product_amount" value="{key('pageData','product_amount')/value}"/>
    <input type="hidden" name="orig_product_amount" id="orig_product_amount" value="{key('pageData','orig_product_amount')/value}"/>
    <input type="hidden" name="origin_id" id="origin_id" value="{key('pageData','origin_id')/value}"/>
    <input type="hidden" name="customer_id" id="customer_id" value="{key('pageData','customer_id')/value}"/>
    <input type="hidden" name="reward_type" id="reward_type" value="{key('pageData','reward_type')/value}"/>
    <input type="hidden" name="ship_method" id="ship_method" value="{key('pageData', 'ship_method')/value}"/>

    <table border="0" align="center" cellpadding="0" cellspacing="0" width="98%">
      <tr>
        <td>
          <button type="button" class="BlueButton" onclick="javascript:doUpdateDeliveryInfo()">Update Delivery Information</button>
          &nbsp;&nbsp;
          <button type="button" class="BlueButton" onclick="javascript:doUpdateProductCategory()">Update Product Category</button>
        </td>
      </tr>
    </table>

    <!-- Main table -->
    <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
      <tr>
        <td>
          <!-- Product list or no matching products message -->
          <xsl:choose>
            <xsl:when test="count(PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT)!=0">

            <!-- Filters -->
            <table width="100%" border="0" cellpadding="2" cellspacing="2">
              <tr>
                <td align="right" >
                  <span class="textLink" id="sortLink" style="padding-right:5em;" onclick="javascript:doSortByPrice();">Sort By Price</span>
                  Filter By Price:&nbsp;
                  <select name="pricepointfilter" id="pricepointfilter" onchange="javascript:doPriceFilterAction(this);" >
                    <option value="0"/>
                    <xsl:for-each select="PRODUCTRESULTS/PRICEPOINTS/PRICEPOINT">
                      <option value="{@pricepointsid}"><xsl:value-of select="@pricepointdescription"/></option>
                    </xsl:for-each>
                  </select>
                <br /><br />

                  Page&nbsp;
                  <span id="PrevPageSection" class="textLink" onclick="javascript:doPrevPageAction();">Previous</span>
                 &nbsp;&nbsp;
                  <script language="javascript"><![CDATA[
                    for (var i = 0, x = 1; i < document.getElementById('ProductListForm').total_pages.value; i++, x++){
                      if (x == document.getElementById('ProductListForm').page_number.value)
                        document.write(x + "&nbsp;");
                      else
                        document.write("<span id='pageLink_" + x + "' class=\"textLink\" onclick=\"javascript:doViewPageAction(" + x + ")\">" + x + "</span>&nbsp;");
                    }]]>
                  </script>
                  &nbsp;
                  <span id="NextPageSection" class="textLink" onclick="javascript:doNextPageAction();">Next</span>
                </td>
              </tr>
            </table>

              <!-- Product List -->
              <table width="100%" border="0" cellpadding="2" cellspacing="2">
                <xsl:variable name="rewardType" select="$rewardType"/>
                <tr>
                  <td class="colHeaderCenter" width="15%">&nbsp;</td>
                  <td class="colHeaderCenter" width="20%">
                    Price
                    <xsl:if test="$rewardType !='' and $rewardType = 'P'">(Percentage)	</xsl:if>
                    <xsl:if test="$rewardType !='' and $rewardType = 'D'">(Dollars)			</xsl:if>
                    <xsl:if test="$rewardType !='' and $rewardType = 'Miles'">(Miles)		</xsl:if>
                    <xsl:if test="$rewardType !='' and $rewardType = 'Points'">(Points)	</xsl:if>
                  </td>
                  <td class="colHeaderCenter" width="70%">Name / Description</td>
                </tr>
                <tr>
                  <td colspan="3" width="100%" align="center">

                  <!-- Scrolling div contiains product detail -->
                  <div id="productListing" style="overflow:auto; width:100%; padding:0px; margin:0px;">
                    <table width="100%" border="0" cellpadding="2" cellspacing="2">
                      <xsl:for-each select="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT">
                        <tr>

                          <!-- Product picture -->
                          <td width="15%" align="center" valign="top">
                            <table width="100%" border="0" cellpadding="1" cellspacing="1">
                              <tr>
                                <td align="center">
                                  <script language="javascript">
                                    // Exception display trigger variables
                                    var exceptionMessageTrigger = false;
                                    var selectedDate = new Date("<xsl:value-of select="$requesteddeliverydate"/>");
                                    var exceptionStart = new Date("<xsl:value-of select="@exceptionstartdate"/>");
                                    var exceptionEnd = new Date("<xsl:value-of select="@exceptionenddate"/>");
                                    var novator_id = "<xsl:value-of select="@novatorid"/>";<![CDATA[
                                    document.write("<img onclick=\"javascript: viewLargeImage(\'" + novator_id + "\')\" ");
                                    document.write("ONERROR=\"var imag = this.src.substring(this.src.length-9, this.src.length); ");
                                    document.write("if(imag != 'npi_1.gif'){ ");
                                    document.write("this.src ='" + product_images + "npi_1.gif'}\" ");
                                    document.write("src='" + product_images + novator_id + "_1.gif'/> ");
                                    ]]>
                                  </script>
                                </td>
                              </tr>
                              <tr>
                                <td align="center">
                                  <span class="textLink" id="orderLink_{position()}" onclick="javascript:doOrderNowAction('{@productid}');" onkeypress="javascript:doOrderNowAction('{@productid}');" tabindex="{position()}">Order Now</span>&nbsp;&nbsp;
                                </td>
                              </tr>
                              <xsl:if test="@firstdeliverymethod='florist'">
                                <tr>
                                  <td align="center" bgcolor="#C1DEF3">
                                    FTD <script><![CDATA[document.write("&#174;")]]></script> Florist
                                  </td>
                                </tr>
                              </xsl:if>
                              <xsl:if test="@firstdeliverymethod='carrier'">
                                <tr>
                                  <td align="center" bgcolor="#FF9933"><xsl:value-of select="@carrier"/></td>
                                </tr>
                              </xsl:if>
                            </table>
                          </td>

                          <!-- Pricing -->
                          <td width="20%" valign="top">
                            <table width="100%" border="0" cellpadding="1" cellspacing="1">
                            <xsl:if test="@standardprice > 0">
                              <tr>
                                <td valign="top">
                                  <xsl:choose>
                                    <xsl:when test="@standarddiscountprice>0 and @standarddiscountprice!=@standardprice">
                                      <span style="color='red'"><strike>$<xsl:value-of select="@standardprice"/></strike></span>
                                      $<xsl:value-of select="@standarddiscountprice"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                      $<xsl:value-of select="@standardprice"/>
                                    </xsl:otherwise>
                                  </xsl:choose>
                                  <xsl:choose>
                                    <xsl:when test="$standardLabel != ''">
                                      &nbsp; - <xsl:value-of select="$standardLabel"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                      &nbsp; - Shown
                                    </xsl:otherwise>
                                  </xsl:choose>
                                  <xsl:if test="@standardrewardvalue>0">
                                    <xsl:if test="$rewardType!='P' and $rewardType!='D'">
                                      (<xsl:value-of select="@standardrewardvalue"/>)
                                    </xsl:if>
                                  </xsl:if>
                                </td>
                              </tr>
                            </xsl:if>

                            <xsl:if test="@deluxeprice>0">
                              <tr>
                                <td valign="top">
                                  <xsl:choose>
                                    <xsl:when test="@deluxediscountprice>0 and @deluxediscountprice!=@deluxeprice">
                                      <span style="color='red'"><strike>$<xsl:value-of select="@deluxeprice"/></strike></span>
                                      $<xsl:value-of select="@deluxediscountprice"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                      $<xsl:value-of select="@deluxeprice"/>
                                    </xsl:otherwise>
                                  </xsl:choose>
                                  <xsl:choose>
                                    <xsl:when test="$deluxeLabel != ''">
                                      &nbsp; - <xsl:value-of select="$deluxeLabel"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                      &nbsp; - Deluxe
                                    </xsl:otherwise>
                                  </xsl:choose>
                                  <xsl:if test="@deluxerewardvalue>0">
                                    <xsl:if test="$rewardType!='P' and $rewardType!='D'">
                                      (<xsl:value-of select="@deluxerewardvalue"/>)
                                    </xsl:if>
                                  </xsl:if>
                                </td>
                              </tr>
                            </xsl:if>
                            <xsl:if test="@premiumprice>0">
                              <tr>
                                <td valign="top">
                                  <xsl:choose>
                                    <xsl:when test="@premiumdiscountprice>0 and @premiumdiscountprice!=@premiumprice">
                                      <span style="color='red'"><strike>$<xsl:value-of select="@premiumprice"/></strike></span>
                                      $<xsl:value-of select="@premiumdiscountprice"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                      $<xsl:value-of select="@premiumprice"/>
                                    </xsl:otherwise>
                                  </xsl:choose>
                                  <xsl:choose>
                                    <xsl:when test="$premiumLabel != ''">
                                      &nbsp; - <xsl:value-of select="$premiumLabel"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                      &nbsp; - Premium
                                    </xsl:otherwise>
                                  </xsl:choose>
                                  <xsl:if test="@premiumrewardvalue>0">
                                    <xsl:if test="$rewardType!='P' and $rewardType!='D'">
                                      (<xsl:value-of select="@premiumrewardvalue"/>)
                                    </xsl:if>
                                  </xsl:if>
                                </td>
                              </tr>
                            </xsl:if>
                          </table>
                        </td>

                        <!-- Product description -->
                        <td width="65%" valign="top">
                          <table width="100%" border="0" cellpadding="1" cellspacing="1">
                            <tr>
                              <td colspan="2">
                                <span class="label"><xsl:value-of disable-output-escaping="yes" select="@productname"/>&nbsp;&nbsp;&nbsp;&nbsp; #<xsl:value-of select="@productid"/>
                                  <xsl:if test="@upsellflag='Y'">
                                    &nbsp;&nbsp;&nbsp;&nbsp;<font color="red">This is an Upsell Product</font>
                                  </xsl:if>
                                </span><br/>
                                <xsl:value-of disable-output-escaping="yes" select="@longdescription"/><br/>
                                <xsl:value-of select="@arrangementsize"/>
                              </td>
                            </tr>
                            <tr>
                              <td width="22%" align="left" valign="top">
                                <b>Delivery Method:</b>
                              </td>
                              <td width="78%">We can deliver this product as early as
                                <xsl:value-of select="@deliverydate"/> &nbsp;
                                <xsl:if test="@firstdeliverymethod='florist'">using an FTD Florist</xsl:if>
                                <xsl:if test="@firstdeliverymethod='carrier'">using <xsl:value-of select="@carrier"/>.</xsl:if>
                              </td>
                            </tr>
                            <xsl:if test="@discountpercent>0">
                              <tr>
                                <td width="22%" align="left">
                                  <br>Discount:</br>
                                </td>
                                <td width="78%" align="left">
                                  <xsl:value-of select="@discountpercent"/>%
                                </td>
                              </tr>
                            </xsl:if>

                            <xsl:choose>
                              <xsl:when test="$requesteddeliverydate">
                                <xsl:if test="@exceptioncode='U'">
                                  <script language="javascript"><![CDATA[
                                    if (selectedDate >= exceptionStart && selectedDate <= exceptionEnd) {
                                      exceptionMessageTrigger = true;
                                      document.write("<tr><td colspan=\"2\" align=\"left\"><font color=\"red\">This product is not available for delivery from&nbsp;]]><xsl:value-of select="@exceptionstartdate"/><![CDATA[&nbsp;to&nbsp;]]><xsl:value-of select="@exceptionenddate"/><![CDATA[</font></td></tr>");
                                    }]]>
                                  </script>
                                </xsl:if>
                                <xsl:if test="@exceptioncode='A'">
                                  <script language="javascript"><![CDATA[
                                    if (selectedDate < exceptionStart || selectedDate > exceptionEnd) {
                                      exceptionMessageTrigger = true;
                                      document.write("<tr><td colspan=\"2\" align=\"left\"><font color=\"red\">This product is only available for delivery from&nbsp;]]><xsl:value-of select="@exceptionstartdate"/><![CDATA[&nbsp;to&nbsp;]]><xsl:value-of select="@exceptionenddate"/><![CDATA[</font></td></tr>");
                                    }]]>
                                  </script>
                                </xsl:if>
                                <script language="javascript"><![CDATA[
                                  if (exceptionMessageTrigger){
                                    document.write("<tr><td colspan=\"2\" align=\"left\"> <font color=\"red\">]]><xsl:value-of select="@exceptionmessage"/><![CDATA[</font></td></tr>");
                  }
                                  else{
                                    document.write("<tr><td colspan=\"2\" align=\"left\"> <font color=\"blue\">]]><xsl:value-of select="@exceptionmessage"/><![CDATA[</font></td></tr>");
                                    }]]>

                                </script>
                              </xsl:when>
                              <xsl:otherwise>
                                <xsl:if test="@exceptioncode='A'">
                                  <tr>
                                    <td colspan="2" align="left"><font color="red">This product is only available for delivery from&nbsp;<xsl:value-of select="@exceptionstartdate"/>&nbsp;to&nbsp;<xsl:value-of select="@exceptionenddate"/></font> </td>
                                  </tr>
                                </xsl:if>
                                <xsl:if test="@exceptioncode='U'">
                                  <tr>
                                    <td colspan="2" align="left"><font color="red">This product is not available for delivery from&nbsp;<xsl:value-of select="@exceptionstartdate"/>&nbsp;to&nbsp;<xsl:value-of select="@exceptionenddate"/></font> </td>
                                  </tr>
                                </xsl:if>
                                <tr>
                                  <td colspan="2" align="left"><font color="red"><xsl:value-of select="@exceptionmessage"/></font></td>
                                </tr>
                              </xsl:otherwise>
                            </xsl:choose>
                            <xsl:if test="@displayspecialfee='Y'">
                              <tr>
                                <td colspan="2" align="left"><font color="red">A $12.99 surcharge will  be added to deliver this item to Alaska or Hawaii</font></td>
                              </tr>
                            </xsl:if>
                            <xsl:if test="@displaycablefee='Y'">
                              <tr>
                                <td colspan="2" align="left"><font color="red">A $12.99 cable fee will  be added to deliver this item to an International country</font></td>
                              </tr>
                            </xsl:if>
                          </table>
                        </td>
                      </tr>
                      <xsl:if test="position()!=last()">
                        <tr>
                          <td colspan="3"><hr/></td>
                        </tr>
                      </xsl:if>
                      </xsl:for-each>
                    </table>
                  </div>
                </td>
              </tr>
              </table>
            </xsl:when>

            <!-- No products found -->
            <xsl:otherwise>
              <table align="center" width="75%" height="25%" border="0" cellspacing="2" cellpadding="2">
                      <tr>
                        <td align="center" class="waitMessage">
                          No products were found matching your criteria.
                        </td>
                      </tr>
                      <tr>
                        <td valign="top" align="right">
                          <button type="button" tabindex="-1" class="BigBlueButton" style="width:160px;" onclick="javascript:doUpdateProductCategoryAction();">Update Product Category</button>
                        </td>
                      </tr>
                    </table>
            </xsl:otherwise>
          </xsl:choose>

        </td>
      </tr>
    </table>
    <!-- End Main table -->

    <!-- Action Buttons -->
    <table width="98%" border="0" align="center" cellpadding="2" cellspacing="2">
      <tr>
        <td width="90%" align="center">
        </td>
        <td width="10%" >
       <button type="button" tabindex="-1" accesskey="C" class="BigBlueButton" style="width:110px;" onclick="javascript:doCancelUpdateAction();">(C)ancel Update</button>
        </td>
      </tr>
    </table>
  </form>

  <!-- Footer template -->
  <xsl:call-template name="footer"/>
  </div>

  <!-- Processing message div -->
  <div id="waitDiv" style="display:none">
    <table id="waitTable" width="98%" border="3" height="300px" align="center" cellpadding="30" cellspacing="1" bordercolor="#006699">
      <tr>
        <td width="100%">
          <table cellspacing="0" cellpadding="0" style="width:100%;height:300px">
            <tr>
              <td id="waitMessage" align="right" width="50%" class="waitMessage"></td>
              <td id="waitTD" width="50%" class="waitMessage"></td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
    <!-- Footer template -->
    <xsl:call-template name="footer"/>
  </div>

</body>
</html>

</xsl:template>
</xsl:stylesheet>
