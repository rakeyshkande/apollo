<!DOCTYPE ACDemo[
  <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<!-- External templates -->
<xsl:import href="../header.xsl"/>
<xsl:import href="../footer.xsl"/>
<xsl:import href="../securityanddata.xsl"/>
<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>
<xsl:import href="cusProduct.xsl"/>

<!-- Keys -->
<xsl:key name="addOn" match="/root/Addons/Addon" use="addon_id"/>
<xsl:key name="pageData" match="/root/pageData/data" use="name"/>
<xsl:key name="security" match="/root/security/data" use="name"/>
<!-- Variables -->
<xsl:variable name="product" select="/root/PRODUCTS/PRODUCT"/>
<xsl:variable name="WALMART_ORDER" select="'WLMTI'"/>

<xsl:output method="html" indent="yes"/>

<xsl:template match="/root">

<html>
<head>
  <title> FTD - Product Categories</title>
  <link rel="stylesheet" type="text/css" href="css/ftd.css"/>
  <script language="javascript" src="js/util.js"/>
  <script language="javascript" src="js/commonUtil.js"/>
  <script type="text/javascript" src="js/updateProductCategory.js"/>
  <style type="text/css">
  #categoryHeader{
    font-weight:bold;
    font-size:small;
  }
  .EditProductCellHighlight{
    font-weight:bold;
  }
  </style>
</head>
<body onload="javascript:init();">
  <!-- Header template -->
    <xsl:call-template name="header">
      <xsl:with-param name="headerName" select="'Update Product Category'"/>
      <xsl:with-param name="showExitButton" select="true()"/>
      <xsl:with-param name="showTime" select="true()"/>
      <xsl:with-param name="showCSRIDs" select="true()"/>
      <xsl:with-param name="dnisNumber" select="$call_dnis"/>
      <xsl:with-param name="cservNumber" select="$call_cs_number"/>
      <xsl:with-param name="indicator" select="$call_type_flag"/>
      <xsl:with-param name="brandName" select="$call_brand_name"/>
      <xsl:with-param name="overrideRTQ" select="Y"/>
      <xsl:with-param name="cdispEnabledFlag" select="$cdisp_enabledFlag"/>
    </xsl:call-template>

  <!-- Content div -->
  <div id="mainContent" style="display:block">

   <!-- Customer Product data -->
   <xsl:call-template name="cusProduct">
     <xsl:with-param name="subheader" select="subheader"/>
     <xsl:with-param name="displayContinue" select="false()"/>
     <xsl:with-param name="displayComplete" select="false()"/>
     <xsl:with-param name="displayCancel" select="true()"/>
   </xsl:call-template>

  <table border="0" align="center" cellpadding="0" cellspacing="0" width="98%">
    <tr>
      <td id="categoryHeader">
        Categories for: '<xsl:value-of select="key('pageData', 'occasion_description')/value"/>'
      </td>
    </tr>
  </table>
  <table border="0" align="center" cellpadding="0" cellspacing="0" width="98%">
    <tr>
      <td>
        <button type="button" class="BlueButton" tabindex="4" onclick="javascript:doUpdateDeliveryInformation()">Update Delivery Information</button>
      </td>
    </tr>
  </table>
    <!-- Main table -->
    <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
      <tr>
        <td>

        <!-- Basic Search Form -->
        <form name="upcForm" id="upcForm" method="post">
        <script>
        	// invoke doSearchAction when form with id="upcForm" is submitted
        	$('#upcForm').submit(function() {
        		doSearchAction();
        	});
        </script>
            <xsl:call-template name="securityanddata"/>
            <xsl:call-template name="decisionResultData"/>

              <!-- Customer Product params -->
              <xsl:call-template name="cusProduct-params">
                <xsl:with-param name="subheader" select="subheader"/>
              </xsl:call-template>

              <!-- New pageData fields  -->
              <input type="hidden" name="action_type" id="action_type" value=""/>
              <input type="hidden" name="product_images" id="product_images" value="{key('pageData','product_images')/value}"/>
              <input type="hidden" name="buyer_full_name" id="buyer_full_name" value="{key('pageData','buyer_full_name')/value}"/>
              <input type="hidden" name="recipient_city" id="recipient_city" value="{key('pageData','recipient_city')/value}"/>
              <input type="hidden" name="company_id" id="company_id" value="{key('pageData','company_id')/value}"/>
              <input type="hidden" name="recipient_country" id="recipient_country" value="{key('pageData', 'recipient_country')/value}"/>
              <input type="hidden" name="item_order_number" id="item_order_number" value="{key('pageData', 'item_order_number')/value}"/>
              <input type="hidden" name="external_order_number" id="external_order_number" value="{key('pageData', 'external_order_number')/value}"/>
              <input type="hidden" name="master_order_number" id="master_order_number" value="{key('pageData', 'master_order_number')/value}"/>
              <input type="hidden" name="occasion" id="occasion" value="{key('pageData', 'occasion')/value}"/>
              <input type="hidden" name="order_guid" id="order_guid" value="{key('pageData', 'order_guid')/value}"/>
              <input type="hidden" name="page_name" id="page_name" value="{key('pageData', 'page_name')/value}"/>
              <input type="hidden" name="page_number" id="page_number" value="{key('pageData', 'page_number')/value}"/>
              <input type="hidden" name="recipient_zip_code" id="recipient_zip_code" value="{key('pageData', 'recipient_zip_code')/value}"/>
              <input type="hidden" name="price_point_id" id="price_point_id" value="{key('pageData', 'price_point_id')/value}"/>
              <input type="hidden" name="source_code" id="source_code" value="{key('pageData','source_code')/value}"/>
              <input type="hidden" name="recipient_state" id="recipient_state" value="{key('pageData', 'recipient_state')/value}"/>
              <input type="hidden" name="domestic_international_flag" id="domestic_international_flag" value="{key('pageData', 'domestic_international_flag')/value}"/>
              <input type="hidden" name="domestic_srvc_fee" id="domestic_srvc_fee" value="{key('pageData', 'domestic_srvc_fee')/value}"/>
              <input type="hidden" name="international_srvc_fee" id="international_srvc_fee" value="{key('pageData', 'international_srvc_fee')/value}"/>
              <input type="hidden" name="line_number" id="line_number" value="{key('pageData', 'line_number')/value}"/>
              <input type="hidden" name="occasion_description" id="occasion_description" value="{key('pageData', 'occasion_description')/value}"/>
              <input type="hidden" name="partner_id" id="partner_id" value="{key('pageData','partner_id')/value}"/>
              <input type="hidden" name="pricing_code" id="pricing_code" value="{key('pageData','pricing_code')/value}"/>
              <input type="hidden" name="snh_id" id="snh_id" value="{key('pageData','snh_id')/value}"/>
              <input type="hidden" name="category_index" id="category_index" value="{key('pageData','category_index')/value}"/>
              <input type="hidden" name="order_detail_id" id="order_detail_id" value="{key('pageData','order_detail_id')/value}"/>
              <input type="hidden" name="delivery_date" id="delivery_date" value="{key('pageData','delivery_date')/value}"/>
              <input type="hidden" name="delivery_date_range_end" id="delivery_date_range_end" value="{key('pageData','delivery_date_range_end')/value}"/>
              <input type="hidden" name="orig_product_id" id="orig_product_id" value="{key('pageData','orig_product_id')/value}"/>
              <input type="hidden" name="orig_product_amount" id="orig_product_amount" value="{key('pageData','orig_product_amount')/value}"/>
              <input type="hidden" name="origin_id" id="origin_id" value="{key('pageData','origin_id')/value}"/>
              <input type="hidden" name="customer_id" id="customer_id" value="{key('pageData','customer_id')/value}"/>
					    <input type="hidden" name="reward_type" id="reward_type" value="{key('pageData','reward_type')/value}"/>
              <input type="hidden" name="ship_method" id="ship_method" value="{key('pageData', 'ship_method')/value}"/>

              <!-- Category and Search Filters -->
              <table width="100%" border="0" cellpadding="2" cellspacing="2">
                <tr>
                  <td class="Instruction" style="font-size:x-small;">
                    Use drop down menu to search by Item or Keyword
                  </td>
                </tr>
                <tr>
                  <td width="100%" class="label">
                    Product Search:
                    &nbsp;&nbsp;&nbsp;
                    <select tabindex="1" name="searchSelect" id="searchSelect">
                         <OPTION value="simpleproductsearch">Item Number</OPTION>
                         <OPTION value="keywordsearch">Keyword</OPTION>
                    </select> &nbsp;&nbsp;

                    <input type="text" id="search_input" name="search_input" maxlength="50" tabindex="2" value="{/root/PD_ORDER_PRODUCTS/PD_ORDER_PRODUCT/product_id}"/>
                    &nbsp;&nbsp;&nbsp;
                    <button type="button" id="goButton" class="BlueButton" tabindex="3" onkeydown="javascript:doSearchAction();" onclick="javascript:doSearchAction();">Go</button>
                  </td>
                </tr>
              </table>

              <!-- Scrolling div contiains categories -->
              <div id="productListing" style="overflow:auto; width:100%; height:600; padding:0px; margin:0px">

                <!-- Occasion, Product, Price -->
                <table id="basicSearchTable" width="100%" border="0" cellpadding="2" cellspacing="2">
                  <tr>
                    <td width="100%" >
                      <table width="80%"  border="0" cellpadding="0" cellspacing="0">
                        <tr>

                          <!-- Occasion column -->
                          <td id="occasionLayer" width="33%" valign="top" style="border-left: thin solid #F0E68C; border-bottom: thin solid #F0E68C;" onmouseover="javascript:doShowByOccasion();">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td width="33%" id="occasionCol" class="EditProductColHeaderDeselect">Occasion</td>
                              </tr>
                              <xsl:for-each select="/root/OCCASION_INDEXES/INDEX">
                                <xsl:variable name="occasionindexid" select="@indexid"/>
                                <tr>
                                  <xsl:if test="@subindexesexist='N'">
                                    <td id="{@name}"  tabindex="6"  onclick="javascript:doCategorySearchAction('{@indexid}');" onkeypress="javascript:doCategorySearchAction('{@indexid}');" onfocus="javascript:doCategoryOverEvent(this);" onblur="javascript:doCategoryOutEvent(this);" onmouseover="javascript:doCategoryOverEvent(this);" onmouseout="javascript:doCategoryOutEvent(this);">
                                      &nbsp;&nbsp;<u><xsl:value-of disable-output-escaping="yes" select="@name"/></u>
                                    </td>
                                  </xsl:if>
                                  <xsl:if test="@subindexesexist='Y'">
                                    <td id="{@name}"   tabindex="6"  onclick="javascript:doShowCategory(this);" onkeypress="javascript:doShowCategory(this);" onfocus="javascript:doCategoryOverEvent(this);" onblur="javascript:doCategoryOutEvent(this);" onmouseover="javascript:doCategoryOverEvent(this);" onmouseout="javascript:doCategoryOutEvent(this);">
                                      &nbsp;&nbsp;<xsl:value-of disable-output-escaping="yes" select="@name"/>
                                      <script type="text/javascript">
                                        startSubCategoryHTML("<xsl:value-of disable-output-escaping="yes" select="@name"/>");
                                        <xsl:for-each select="/root/SUBINDEXES/INDEX[@parentindexid=$occasionindexid]">
                                          addSubCategoryHTML("<xsl:value-of disable-output-escaping="yes" select="@name"/>", "<xsl:value-of select="@indexid"/>");
                                        </xsl:for-each>
                                        endSubCategoryHTML();
                                      </script>
                                    </td>
                                  </xsl:if>
                                </tr>
                              </xsl:for-each>
                            </table>
                          </td>

                          <!-- Product column -->
                          <td id="productLayer" width="33%" valign="top" style="border-left: thin solid #F0E68C; border-right: thin solid #F0E68C; border-bottom: thin solid #F0E68C;" onmouseover="javascript:doShowByProduct();">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td width="33%" id="productCol" class="EditProductColHeaderDeselect">Product</td>
                              </tr>
                              <xsl:for-each select="/root/PRODUCT_INDEXES/INDEX">
                                <xsl:variable name="productindexid" select="@indexid"/>
                                <tr>
                                  <xsl:if test="@subindexesexist='N'">
                                    <td id="{@name}"  tabindex="6"  onclick="javascript:doCategorySearchAction('{@indexid}');" onkeypress="javascript:doCategorySearchAction('{@indexid}');" onfocus="javascript:doCategoryOverEvent(this);" onblur="javascript:doCategoryOutEvent(this);" onmouseover="javascript:doCategoryOverEvent(this);" onmouseout="javascript:doCategoryOutEvent(this);">
                                      &nbsp;&nbsp;<u><xsl:value-of disable-output-escaping="yes" select="@name"/></u>
                                    </td>
                                  </xsl:if>
                                  <xsl:if test="@subindexesexist='Y'">
                                    <td id="{@name}"  tabindex="6"   onclick="javascript:doShowCategory(this);" onkeypress="javascript:doShowCategory(this);" onfocus="javascript:doCategoryOverEvent(this);" onblur="javascript:doCategoryOutEvent(this);" onmouseover="javascript:doCategoryOverEvent(this);" onmouseout="javascript:doCategoryOutEvent(this);">
                                      &nbsp;&nbsp;<xsl:value-of disable-output-escaping="yes" select="@name"/>
                                      <script type="text/javascript">
                                        startSubCategoryHTML("<xsl:value-of disable-output-escaping="yes" select="@name"/>");
                                        <xsl:for-each select="/root/SUBINDEXES/INDEX[@parentindexid=$productindexid]">
                                          addSubCategoryHTML("<xsl:value-of disable-output-escaping="yes" select="@name"/>", "<xsl:value-of select="@indexid"/>");
                                        </xsl:for-each>
                                        endSubCategoryHTML();
                                      </script>
                                    </td>
                                  </xsl:if>
                                </tr>
                              </xsl:for-each>
                            </table>
                          </td>
                        </tr>
                        <tr>
                          <td>&nbsp;</td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
                <table width="100%" border="0" cellpadding="1" cellspacing="1">
          <tr>
            <td width="80%">

            </td>

            <td width="20%" align="right">
               <xsl:if test="key('pageData','origin_id')/value != $WALMART_ORDER">
                <button type="button" class="BlueButton" tabindex="5" onclick="javascript:doCustomOrder()">Custom Order</button>
                &nbsp;&nbsp;
                  </xsl:if>
            </td>
          </tr>
        </table>
              </div>

              <!--  Subcategory Div Popup -->
              <div id="subcategory" style="display:none; position:absolute; border:1px solid black; border-color:black; background:#FFF8DC; cursor:default;">
                <button type="button" id="moveBtn" style="border: none; font-family: Verdana; font-style: normal; width: 99px; height: 22; text-align: left; font-weight: normal; background-color: #808080; text-decoration: none;" onClick="javascript:moveBtn_onmouseclick(this)"><font color="#FFFFFF">Move Window</font></button>
                <button type="button" id="closeBtn" style="border: none; font-family: Verdana; font-style: normal; width: 99px; height: 22; text-align: left; font-weight: normal; background-color: #808080; text-decoration: none;"  onClick="javascript:dismisssubcatbox();"><font color="#FFFFFF">Close Window</font></button>
                <table cellspacing="20" border="0">
                  <tr>
                    <td>
                      <div id="subcategorydetail"/>
                    </td>
                  </tr>
                </table>
              </div>
            </form>
            <!-- End Basic Search form -->


        </td>
      </tr>
    </table>

    <!-- Footer template -->
    <xsl:call-template name="footer"/>
    <iframe id="cancelUpdateFrame" width="0px" height="0px" border="0"/>
    <iframe id="checkLock" width="0px" height="0px" border="0"/>

  </div>

  <!-- Processing message div -->
  <div id="waitDiv" style="display:none">
    <table id="waitTable" width="98%" border="3" height="300px" align="center" cellpadding="30" cellspacing="1" bordercolor="#006699">
      <tr>
        <td width="100%">
          <table cellspacing="0" cellpadding="0" style="width:100%;height:300px">
            <tr>
              <td id="waitMessage" align="right" width="50%" class="waitMessage"></td>
              <td id="waitTD" width="50%" class="waitMessage"></td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
    <xsl:call-template name="footer"/>
  </div>

</body>
</html>
</xsl:template>
</xsl:stylesheet>
