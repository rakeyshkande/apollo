<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">

  <productList>
		<PRODUCTS>
			<xsl:for-each select="PRODUCTS/PRODUCT">
				<xsl:sort select="@displayorder" data-type="number"/>
				<xsl:copy-of select="."/>
			</xsl:for-each>
		</PRODUCTS>
    <xsl:copy-of select="PRODUCTS/OEParameters"/>
    <xsl:copy-of select="PRODUCTS/SHIPPINGMETHODS"/>
	</productList>

</xsl:template>
</xsl:stylesheet>