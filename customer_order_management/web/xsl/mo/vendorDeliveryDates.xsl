<!DOCTYPE ACDemo[
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" indent="yes"/>
<xsl:template name="vendorDeliveryDates" >
  <xsl:param name="floristServiceCharges"/>
  <xsl:param name="origShipMethod"/>
  <xsl:param name="shipCosts"/>
  <xsl:param name="requestedDeliveryDate"/>
  <xsl:param name="requestedDeliveryDateRangeEnd"/>
  <xsl:param name="deliveryDates"/>
  <xsl:param name="displayGetMax"/>
  <xsl:param name="defaultDates"/>
  <xsl:param name="productType"/>
  <xsl:param name="containsErrors"/>
  <xsl:param name="fuelSurcharge"/>


  <script type="text/javascript" language="javascript">
  <![CDATA[
  /*
   * The prepareDeliveryDate function is dynamic because delivery date submission preperation
   * is different for florist and vendor items.  This version of prepareDeliveryDate gets the selected
   * ship method and the corresponding selected delivery date.
   */
  function prepareDeliveryDate(formName)
  {
    var radio = document.forms[formName].page_ship_method;
    var radioSelected;

		//incase the product is not available for the delivery date/address selected, we will not
		//get the delivery dates/shipping methods at all.
		//In this case, use the default delivery date, delivery date range, and the shipping method.
		if (radio != null)
		{
			if (radio.length == null)
			{
				if (radio.checked)
					radioSelected = radio;
				else
					radioSelected = null;
			}
			else
			{
				for ( var i = 0; i < radio.length; i++){
					if ( radio[i].checked ){
						radioSelected = radio[i];
					}
				}
			}

			// continue only if a ship method was selected
			if ( radioSelected != null )
			{
				var shipMethod = radioSelected.value;
				var dd = $('select[name="'+shipMethod + '_delivery_date"]')[0];
				var ddSelected = dd[dd.selectedIndex];
		    var startDate = ddSelected.value;
		    var endDate = $(ddSelected).attr('endDate');
		    var outEndDate = ( endDate != startDate ) ?  endDate : "";

				document.forms[formName].ship_method.value = shipMethod;
				document.forms[formName].delivery_date.value = startDate;
				document.forms[formName].delivery_date_range_end.value = outEndDate;
			}
			else
			{
				document.forms[formName].ship_method.value = document.forms[formName].origShipMethod.value;
				document.forms[formName].delivery_date.value = document.forms[formName].requestedDeliveryDate.value;
				document.forms[formName].delivery_date_range_end.value = document.forms[formName].requestedDeliveryDateRangeEnd.value;
			}
		}
  }


function validateShipMethodAndDeliveryDate()
{
  var radio = document.forms["productDetailForm"].page_ship_method;
	var radioSelected;

	//incase the product is not available for the delivery date/address selected, we will not
	//get the delivery dates/shipping methods at all.
	//In this case, use the default delivery date, delivery date range, and the shipping method.
	if (radio != null)
	{
		if (radio.length == null)
		{
			if (radio.checked)
			{
				radioSelected = radio;
			}
		}
		else
		{
			for ( var i = 0; i < radio.length; i++)
			{
				radio[i].style.backgroundColor='white';
				if ( radio[i].checked )
				{
					radioSelected = radio[i];
				}
			}
		}

		if ( radioSelected != null )
		{
			return true;
		}
		else
		{
			for ( var i = 0; i < radio.length; i++)
			{
				radio[i].style.backgroundColor='pink';
			}
			return false;
		}
	}
	else
	{
		return false;
	}
}


  ]]>
  </script>
  <tr>
    <input type="hidden" name="origShipMethod" id="origShipMethod" value="{$origShipMethod}"/>
    <input type="hidden" name="requestedDeliveryDate" id="requestedDeliveryDate" value="{$requestedDeliveryDate}"/>
    <input type="hidden" name="requestedDeliveryDateRangeEnd" id="requestedDeliveryDateRangeEnd" value="{$requestedDeliveryDateRangeEnd}"/>
    <td colspan="3">
      <table border="0" cellpadding="0" cellspacing="2" width="100%">
      <!-- There are four shipping methods for the vendor, plus the ship method of Florist
					<shippingMethod>
						<method cost="0" description="Saturday Delivery" code="SA"/>
						<method cost="0" description="Standard Delivery" code="GR"/>
						<method cost="0" description="Next Day Delivery" code="ND"/>
						<method cost="0" description="Two Day Delivery"  code="2F"/>
					</shippingMethod>
					Note that the florist delivery method can be '' or 'FL'.
			-->
        <xsl:for-each select="/root/shippingData/shippingMethods/shippingMethod/method">

<!--
requestedDeliveryDate&nbsp;=&nbsp;		 					<xsl:value-of select="$requestedDeliveryDate"/>
&nbsp;requestedDeliveryDateRangeEnd&nbsp;=&nbsp;<xsl:value-of select="$requestedDeliveryDateRangeEnd"/>
-->
          <tr>

	          <!-- ******************************************************************************* -->
	          <!-- Radio Button -->
	          <!-- ******************************************************************************* -->
            <td class="Label" align="right" colspan="30%">
              <xsl:choose>
                <xsl:when test="@code = $origShipMethod">
                  <input type="radio" name="page_ship_method" id="page_ship_method" value="{@code}">
										<!-- determine which, if any, radio button to select -->
										<xsl:choose>
											<xsl:when test="@code = 'ND'">
												<xsl:for-each select="/root/shippingData/deliveryDates/deliveryDate">
													<xsl:if test="contains(types, 'ND') and
																				startDate = $requestedDeliveryDate and
																			  (	$requestedDeliveryDateRangeEnd = '' or
																			  	endDate = $requestedDeliveryDateRangeEnd
																			  )">
														<xsl:attribute name="checked"/>
													</xsl:if>
												</xsl:for-each>
											</xsl:when>
											<xsl:when test="@code = 'SA'">
												<xsl:for-each select="/root/shippingData/deliveryDates/deliveryDate">
													<xsl:if test="contains(types, 'SA') and
																				startDate = $requestedDeliveryDate and
																			  (	$requestedDeliveryDateRangeEnd = '' or
																			  	endDate = $requestedDeliveryDateRangeEnd
																			  )">
														<xsl:attribute name="checked"/>
													</xsl:if>
												</xsl:for-each>
											</xsl:when>
											<xsl:when test="@code = 'GR'">
												<xsl:for-each select="/root/shippingData/deliveryDates/deliveryDate">
													<xsl:if test="contains(types, 'GR') and
																				startDate = $requestedDeliveryDate and
																			  (	$requestedDeliveryDateRangeEnd = '' or
																			  	endDate = $requestedDeliveryDateRangeEnd
																			  )">
														<xsl:attribute name="checked"/>
													</xsl:if>
												</xsl:for-each>
											</xsl:when>
											<xsl:when test="@code = '2F'">
												<xsl:for-each select="/root/shippingData/deliveryDates/deliveryDate">
													<xsl:if test="contains(types, '2F') and
																				startDate = $requestedDeliveryDate and
																			  (	$requestedDeliveryDateRangeEnd = '' or
																			  	endDate = $requestedDeliveryDateRangeEnd
																			  )">
														<xsl:attribute name="checked"/>
													</xsl:if>
												</xsl:for-each>
											</xsl:when>

											<xsl:otherwise>
											</xsl:otherwise>
										</xsl:choose>
                  </input>
                </xsl:when>
                <xsl:when test="@code = 'FL' and $origShipMethod = 'SD'">
                  <input type="radio" name="page_ship_method" id="page_ship_method" value="SD">
										<xsl:choose>
											<xsl:when test="@code = 'FL'">
												<xsl:for-each select="/root/shippingData/deliveryDates/deliveryDate">
													<xsl:if test="contains(types, 'FL') and
																				startDate = $requestedDeliveryDate and
																			  (	$requestedDeliveryDateRangeEnd = '' or
																			  	endDate = $requestedDeliveryDateRangeEnd
																			  )">
														<xsl:attribute name="checked"/>
													</xsl:if>
												</xsl:for-each>
											</xsl:when>
											<xsl:otherwise>
											</xsl:otherwise>
										</xsl:choose>
                  </input>
                </xsl:when>
                <xsl:when test="@code = 'FL'">
                  <input type="radio" name="page_ship_method" id="page_ship_method" value="SD"/>
                </xsl:when>
                <xsl:otherwise>
                  <input type="radio" name="page_ship_method" id="page_ship_method" value="{@code}"/>
                </xsl:otherwise>
              </xsl:choose>
            </td>


	          <!-- ******************************************************************************* -->
            <!-- Description -->
	          <!-- ******************************************************************************* -->
            <td colspan="10%">
              <xsl:value-of select="@description"/>
            </td>


	          <!-- ******************************************************************************* -->
            <!-- Cost -->
	          <!-- ******************************************************************************* -->
            <td colspan="10%">
              &nbsp;
            </td>


	          <!-- ******************************************************************************* -->
            <!-- blank column -->
	          <!-- ******************************************************************************* -->
            <td colspan="40%"/>


	          <!-- ******************************************************************************* -->
	          <!-- Arrives No Later than / Will be Delivered On -->
	          <!-- ******************************************************************************* -->
            <td >
            <xsl:choose>
              <xsl:when test="@code = 'ND' or @code = 'SA' or @code= '' or @code = 'FL' ">
                Will be delivered on &nbsp; &nbsp;
              </xsl:when>
              <xsl:otherwise>
                Arrives no later than &nbsp;
              </xsl:otherwise>
            </xsl:choose>
            </td>


	          <!-- ******************************************************************************* -->
	          <!-- Day/Date Drop down -->
	          <!-- ******************************************************************************* -->
            <td>
              <xsl:choose>
                <xsl:when test="@code = 'ND'">
                  <select name="{@code}_delivery_date" id="delivery_date_select">
                    <xsl:for-each select="/root/shippingData/deliveryDates/deliveryDate">
                      <xsl:if test="contains(types, 'ND')">
                        <option value="{startDate}" endDate="{endDate}">
                          <xsl:if test="$containsErrors and
                          							startDate = $requestedDeliveryDate and
																			  (	$requestedDeliveryDateRangeEnd = '' or
																			  	endDate = $requestedDeliveryDateRangeEnd
																			  )">
                            <xsl:attribute name="selected"/>
                          </xsl:if>
                          <xsl:value-of select="displayDate"/>
                        </option>
                      </xsl:if>
                    </xsl:for-each>
                  </select>
                </xsl:when>

                <xsl:when test="@code = '2F'">
                  <select name="{@code}_delivery_date" id="delivery_date_select">
                    <xsl:for-each select="/root/shippingData/deliveryDates/deliveryDate">
                      <xsl:if test="contains(types, '2F')">
                        <option value="{startDate}" endDate="{endDate}">
                          <xsl:if test="$containsErrors and
                          							startDate = $requestedDeliveryDate and
																			  (	$requestedDeliveryDateRangeEnd = '' or
																			  	endDate = $requestedDeliveryDateRangeEnd
																			  )">
                            <xsl:attribute name="selected"/>
                          </xsl:if>
                          <xsl:value-of select="displayDate"/>
                        </option>
                      </xsl:if>
                    </xsl:for-each>
                  </select>
                </xsl:when>

                <xsl:when test="@code = 'GR'">
                  <select name="{@code}_delivery_date" id="delivery_date_select">
                    <xsl:for-each select="/root/shippingData/deliveryDates/deliveryDate">
                      <xsl:if test="contains(types, 'GR')">
                        <option value="{startDate}" endDate="{endDate}">
                          <xsl:if test="$containsErrors and
                          							startDate = $requestedDeliveryDate and
																			  (	$requestedDeliveryDateRangeEnd = '' or
																			  	endDate = $requestedDeliveryDateRangeEnd
																			  )">
                            <xsl:attribute name="selected"/>
                          </xsl:if>
                          <xsl:value-of select="displayDate"/>
                        </option>
                      </xsl:if>
                    </xsl:for-each>
                  </select>
                </xsl:when>

                <xsl:when test="@code = 'SA'">
                  <select name="{@code}_delivery_date" id="delivery_date_select">
                    <xsl:for-each select="/root/shippingData/deliveryDates/deliveryDate">
                      <xsl:if test="contains(types, 'SA')">
                        <option value="{startDate}" endDate="{endDate}">
                          <xsl:if test="$containsErrors and
                          							startDate = $requestedDeliveryDate and
																			  (	$requestedDeliveryDateRangeEnd = '' or
																			  	endDate = $requestedDeliveryDateRangeEnd
																			  )">
                            <xsl:attribute name="selected"/>
                          </xsl:if>
                          <xsl:value-of select="displayDate"/>
                        </option>
                      </xsl:if>
                    </xsl:for-each>
                  </select>
                </xsl:when>

                <xsl:when test="@code = 'FL'">
                  <select name="SD_delivery_date" id="delivery_date_select">
                    <xsl:for-each select="/root/shippingData/deliveryDates/deliveryDate">
                      <xsl:if test="contains(types, 'FL')">
                        <option value="{startDate}" endDate="{endDate}">
                          <xsl:if test="$containsErrors and
                          							startDate = $requestedDeliveryDate and
																			  (	$requestedDeliveryDateRangeEnd = '' or
																			  	endDate = $requestedDeliveryDateRangeEnd
																			  )">
                            <xsl:attribute name="selected"/>
                          </xsl:if>
                          <xsl:value-of select="displayDate"/>
                        </option>
                      </xsl:if>
                    </xsl:for-each>
                  </select>
                </xsl:when>

                <xsl:otherwise>
                  <select name="{@code}_delivery_date" id="delivery_date_select">
                    <xsl:for-each select="/root/shippingData/deliveryDates/deliveryDate">
                        <option value="{startDate}" endDate="{endDate}">
                          <xsl:if test="$containsErrors and
                          							startDate = $requestedDeliveryDate and
																			  (	$requestedDeliveryDateRangeEnd = '' or
																			  	endDate = $requestedDeliveryDateRangeEnd
																			  )">
                            <xsl:attribute name="selected"/>
                          </xsl:if>
                          <xsl:value-of select="displayDate"/>
                        </option>
                    </xsl:for-each>
                  </select>
                </xsl:otherwise>
              </xsl:choose>
            </td>
          </tr>
        </xsl:for-each>



	      <!-- ******************************************************************************* -->
        <!-- Issue 2134 -->
        <!-- ******************************************************************************* -->
        <!-- Section for fringe condition where florist delivery on original order
             but floral delivery no longer available.  Whatta pain :-(
        -->
        <xsl:if test="$deliveryDates != ''">
          <tr>
            <td class="Label" align="right" colspan="30%">
              <input type="radio" name="page_ship_method" id="page_ship_method" value="SD">
							<xsl:attribute name="checked"/>
              </input>
            </td>
            <td colspan="10%">
              Florist Delivery
            </td>
            <!-- Cost -->
            <td colspan="10%">
							$<xsl:value-of select="format-number($floristServiceCharges, '#,###,##0.00')"/>
            </td>
            <td colspan="40%"/>
            <td >
                Will be delivered on &nbsp; &nbsp;
            </td>
            <td>
                <select name="SD_delivery_date" id="delivery_date_select">
                  <xsl:for-each select="$deliveryDates/deliveryDate">
                  <option value="{startDate}" endDate="{endDate}">
                    <xsl:if test="$defaultDates">
                      <xsl:if test="startDate = $requestedDeliveryDate and ($requestedDeliveryDateRangeEnd = '' or endDate = $requestedDeliveryDateRangeEnd)"><xsl:attribute name="selected"/></xsl:if>
                    </xsl:if>
                    <xsl:value-of select="displayDate"/>
                  </option>
                  </xsl:for-each>
                </select>
            </td>
          </tr>
        </xsl:if>
        <!-- End of fringe condition section -->
      </table>
    </td>
  </tr>
  <tr id="page_ship_method_validation_message_row" style="display:none">
    <td/>
    <td id="page_ship_method_validation_message_cell" colspan="10" class="RequiredFieldTxt" style="padding-left:4.5em;"/>
  </tr>


</xsl:template>
</xsl:stylesheet>