

<!DOCTYPE ACDemo [
  <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<!-- External templates -->
	<xsl:import href="../securityanddata.xsl"/>
<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>
	<xsl:import href="../header.xsl"/>
	<xsl:import href="../footer.xsl"/>
	<!-- Keys -->
	<xsl:key name="pageData" match="/root/pageData/data" use="name"/>
	<xsl:key name="security" match="/root/security/data" use="name"/>
		<xsl:variable name="isFloristOrder" select="boolean(key('pageData','display_format')/value = 'F')"/>
	<!-- Constants -->
	<!-- Variables -->
	<xsl:variable name="containsErrors" select="boolean(/root/ERROR_MESSAGES/ERROR_MESSAGE)"/>
	<xsl:variable name="requestedDeliveryDate" select="key('pageData', 'delivery_date')/value"/>
	<xsl:variable name="requestedDeliveryDateRangeEnd" select="key('pageData', 'delivery_date_range_end')/value"/>
	<xsl:variable name="defaultDates" select="true()"/>
        <xsl:variable name="origShipMethod" select="key('pageData','ship_method')/value"/>
	<xsl:variable name="displayGetMax" select="boolean(key('pageData', 'max_delivery_dates_retrieved')/value = 'n')"/>
        <xsl:variable name="message_display" select="key('pageData','message_display')/value"/>
        <xsl:variable name="success" select="key('pageData','success')/value"/>
        <xsl:variable name="currentDate" select="key('pageData','current_date')/value"/>
	<xsl:output method="html" indent="yes"/>
	<xsl:template match="/root">
		<html>
			<head>
				<title>FTD - Update Delivery Date</title>
				<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
				<link rel="stylesheet" type="text/css" href="css/calendar.css"/>
				<style type="text/css">
                                    .blueBanner{
                                     color: #006699;
                                     height:5px;
                                    }
                                    img{cursor:hand;}
				</style>
				<!-- main calendar program -->
				<script type="text/javascript" src="js/calendar.js"/>
				<!-- language for the calendar -->
				<script type="text/javascript" src="js/calendar-en.js"/>
				<!-- the following script defines the Calendar.setup helper function, which makes
                                        adding a calendar a matter of 1 or 2 lines of code. -->
				<script type="text/javascript" src="js/calendar-setup.js"/>
				<script language="javascript" src="js/tabIndexMaintenance.js"/>
				<script language="javascript" src="js/FormChek.js"/>
				<script language="javascript" src="js/util.js"/>
				<script language="javascript" src="js/commonUtil.js"/>
				<script language="javascript" src="js/updateDeliveryDate.js"/>
			</head>
			<body onload="javascript:init();">
				<!-- Iframe used to get max delivery dates -->
				<iframe id="updateDeliveryDateActions" width="0px" height="0px" border="0"/>
				<!-- Header template -->
				<xsl:call-template name="header">
					<xsl:with-param name="headerName" select="'Update Delivery Date'"/>
					<xsl:with-param name="showExitButton" select="true()"/>
					<xsl:with-param name="showTime" select="true()"/>
					<xsl:with-param name="showCSRIDs" select="true()"/>
					<xsl:with-param name="dnisNumber" select="$call_dnis"/>
					<xsl:with-param name="cservNumber" select="$call_cs_number"/>
					<xsl:with-param name="indicator" select="$call_type_flag"/>
					<xsl:with-param name="brandName" select="$call_brand_name"/>
					<xsl:with-param name="overrideRTQ" select="Y"/>
					<xsl:with-param name="cdispEnabledFlag" select="$cdisp_enabledFlag"/>
				</xsl:call-template>
				<!-- Content div -->
				<div id="mainContent" style="display:block">
					<!-- Top buttons -->
					<div style="margin-left:7px;margin-top:0px;width:99%;align:center;">
						<div style="text-align:right">
							<span style="padding-right:2em;">
								<button type="button" tabindex="-1" id="topCompleteButton" accesskey="p" style="width:110px;" class="BigBlueButton" onclick="javascript:doCompleteUpdateAction();">Com(p)lete Update</button>
							</span>
							<button type="button" tabindex="-1" id="topCancelButton" accesskey="C" class="BigBlueButton" style="width:110px;" onclick="javascript:doCancelUpdateAction();">(C)ancel Update</button>
						</div>
					</div>
					<!-- /Top buttons -->
					<form name="deliveryDateForm" method="post" action="updateDeliveryDate.do">
						<xsl:call-template name="securityanddata"/>
						<xsl:call-template name="decisionResultData"/>
                                                <input type="hidden" name="order_guid" id="order_guid" value="{key('pageData','order_guid')/value}"/>                                                
                                                <input type="hidden" name="order_detail_id" id="order_detail_id" value="{key('pageData','order_detail_id')/value}"/>
                                                <input type="hidden" name="customer_id" id="customer_id" value="{key('pageData','customer_id')/value}"/>                                                
                                                <input type="hidden" name="master_order_number" id="master_order_number" value="{key('pageData', 'master_order_number')/value}"/>
                                                <input type="hidden" name="external_order_number" id="external_order_number" value="{key('pageData', 'external_order_number')/value}"/> 
                                                <input type="hidden" name="source_code" id="source_code" value="{key('pageData', 'source_code')/value}"/>
						<input type="hidden" name="recipient_city" id="recipient_city" value="{key('pageData', 'recipient_city')/value}"/>
                                                <input type="hidden" name="recipient_country" id="recipient_country" value="{key('pageData', 'recipient_country')/value}"/>
						<input type="hidden" name="recipient_state" id="recipient_state" value="{key('pageData', 'recipient_state')/value}"/>
						<input type="hidden" name="recipient_zip_code" id="recipient_zip_code" value="{key('pageData', 'recipient_zip_code')/value}"/>
						<input type="hidden" name="occasion" id="occasion" value="{key('pageData', 'occasion')/value}"/>
       						<input type="hidden" name="order_disp_code" id="order_disp_code" value="{key('pageData', 'order_disp_code')/value}"/>
						<input type="hidden" name="product_id" id="product_id" value="{key('pageData', 'product_id')/value}"/>
                                                <input type="hidden" name="florist_id" id="florist_id" value="{key('pageData', 'florist_id')/value}"/>
						<input type="hidden" name="origin_id" id="origin_id" value="{key('pageData','origin_id')/value}"/>
                                                <input type="hidden" name="ship_method" id="origin_id" value="{key('pageData','ship_method')/value}"/>
						<input type="hidden" name="orig_product_id" id="orig_product_id" value="{key('pageData', 'orig_product_id')/value}"/>
						<input type="hidden" name="vendor_flag" id="vendor_flag" value="{key('pageData', 'vendor_flag')/value}"/>
						<input type="hidden" name="delivery_date" id="delivery_date" value="{key('pageData','delivery_date')/value}"/>
						<input type="hidden" name="delivery_date_range_end" id="delivery_date_range_end" value="{key('pageData','delivery_date_range_end')/value}"/>
                                                <input type="hidden" name="orig_delivery_date" id="orig_delivery_date" value="{key('pageData','delivery_date')/value}"/>
						<input type="hidden" name="orig_delivery_date_range_end" id="orig_delivery_date_range_end" value="{key('pageData','delivery_date_range_end')/value}"/>
						<input type="hidden" name="orig_ship_date" id="orig_ship_date" value="{key('pageData','ship_date')/value}"/>
						<input type="hidden" name="addons" id="addons" value="{key('pageData', 'addons')/value}"/>
                                                <!-- Cancel Update IFrame -->
						<iframe id="cancelUpdateFrame" width="0px" height="0px" border="0"/>
						<!-- Locking IFrame -->
						<iframe id="checkLock" width="0px" height="0px" border="0"/>
						<!-- Main table -->
						<table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
							<tr>
								<td>
									<table width="100%" border="0" cellpadding="2" cellspacing="2">
										<tr>
											<td colspan="3" width="100%" align="center">
												<!-- Scrolling div contains product detail or no products found message -->
												<div id="deliveryDateDetail" style="overflow:auto; width:100%; height:600; padding:0px; margin:0px">
													<table width="100%" border="0" cellspacing="2" cellpadding="2">
														<tr>
															<td colspan="2" width="100%" align="left">
																<span style="color:#FF0000">
                                                                                                                                This function will send either an ASK message or CAN message and a new FTD message on the order.  It is not to be used after the delivery date on orders that are “make good” that is not the fault of the vendor (e.g. carrier delays) It can be used for “make good” orders that are the fault of the Vendor or Florist.
																</span>
															</td>
														</tr>
                                                                                                                <tr>
															<td colspan="2" width="100%" align="left">
																&nbsp;
															</td>
														</tr>
                                                                                                                <tr>
															<td colspan="2" width="100%" align="left">
																&nbsp;
															</td>
														</tr>
														<xsl:choose>
															<xsl:when test="boolean($isFloristOrder)">															
																 <xsl:if test="deliveryDates != ''">
																<tr>
                                                                                                                                        <td class="alignright" width="18%">
                                                                                                                                          <span class="Label">Original Delivery Date:</span>
                                                                                                                                        </td>
                                                                                                                                        <td class="alignleft">
                                                                                                                                          <xsl:value-of select="$requestedDeliveryDate"/>
                                                                                                                                          <xsl:if test="$requestedDeliveryDateRangeEnd != ''">
                                                                                                                                             &nbsp;-&nbsp;<xsl:value-of select="$requestedDeliveryDateRangeEnd"/>
                                                                                                                                         </xsl:if>
                                                                                                                                        </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                        <td colspan="2" width="100%" align="left">
                                                                                                                                                &nbsp;
                                                                                                                                        </td>
                                                                                                                                </tr>
                                                                                                                                
                                                                                                                                <tr>
																	<td class="Label" align="right" width="25%">Delivery Date:</td>
																	<td style="padding-left:.5em;">
																		<select name="page_delivery_date" id="delivery_date_select" onchange="doCheckSelection(this);">
																			<option value="" endDate="">																				
                                                                                                                                                                <xsl:if test="$containsErrors = false">
																					<xsl:attribute name="selected"/>
																				</xsl:if>                                                                                                                                                             
																			</option>
																			<xsl:for-each select="deliveryDates/deliveryDate">
																				<option value="{startDate}" endDate="{endDate}">
																					<xsl:if test="$defaultDates">
																						<xsl:if test="$containsErrors and startDate = $requestedDeliveryDate and ($requestedDeliveryDateRangeEnd = '' or endDate = $requestedDeliveryDateRangeEnd)">
																							<xsl:attribute name="selected"/>
																						</xsl:if>
																					</xsl:if>
																					<xsl:choose>
																						<xsl:when test="startDate = $currentDate and startDate = endDate">
																							<xsl:value-of select="concat('Today ',startDate)"/>
																						</xsl:when>
																						<xsl:otherwise>
																							<xsl:value-of select="displayDate"/>
																						</xsl:otherwise>
																					</xsl:choose> 
																				</option>
																			</xsl:for-each>
																		</select>
																		<span style="padding-left:1em;">&nbsp;</span>
																		<img id="delivery_date_img" name="DateCalendar" src="images\calendar.gif"></img>
																		<xsl:if test="boolean($displayGetMax)">
																			<span style="padding-left:3em;">
																				<button type="button" id="getMaxButton" class="bluebutton" onclick="javascript:doGetMaxDeliveryDateAction();">Get Max</button>
																			</span>
																		</xsl:if>
																	</td>
																</tr>
                                                                                                                       <tr>
                                                                                                                                  <td></td>
                                                                                                                                  <td>
                                                                                                                                     <span id="required" class="RequiredFieldTxt" style="display:none">Required Field</span>
                                                                                                                                  </td>
                                                                                                                                </tr>     
                                                                </xsl:if>                                                                                                                                                                      
															</xsl:when>
															<xsl:otherwise>
																<tr>
																	<td colspan="3">
																		<table border="0" cellpadding="0" cellspacing="2" width="100%">
																			<xsl:for-each select="/root/shippingData/shippingMethods/shippingMethod/method">
                                                                                                                                                        <xsl:if test="$origShipMethod = @code">
																				<tr>
                                                                                                                                                                        <td class="alignright" width="18%">
                                                                                                                                                                          <span class="Label">Original Delivery Date:</span>
                                                                                                                                                                        </td>
                                                                                                                                                                        <td class="alignleft">
                                                                                                                                                                          <xsl:value-of select="$requestedDeliveryDate"/>
                                                                                                                                                                        </td>
                                                                                                                                                                </tr>
                                                                                                                                                                <tr>
                                                                                                                                                                        <td colspan="2" width="100%" align="left">
                                                                                                                                                                                &nbsp;
                                                                                                                                                                        </td>
                                                                                                                                                                </tr>
                                                                                                                                                                <tr>
                                                                                                                                                                          <!-- ******************************************************************************* -->
                                                                                                                                                                              <!-- Arrives No Later than / Will be Delivered On -->
                                                                                                                                                                              <!-- ******************************************************************************* -->
                                                                                                                                                                        <td class="alignright" width="25%">
                                                                                                                                                                        <xsl:choose>
                                                                                                                                                                          <xsl:when test="@code = 'ND' or @code = 'SA'  or @code = 'SU' or @code= '' or @code = 'FL' ">
                                                                                                                                                                            Will be delivered on &nbsp; &nbsp;
                                                                                                                                                                          </xsl:when>
                                                                                                                                                                          <xsl:otherwise>
                                                                                                                                                                            Arrives no later than &nbsp;
                                                                                                                                                                          </xsl:otherwise>
                                                                                                                                                                        </xsl:choose>
                                                                                                                                                                        </td>
                                                                                                                                                            
                                                                                                                                                            
                                                                                                                                                                              <!-- ******************************************************************************* -->
                                                                                                                                                                              <!-- Day/Date Drop down -->
                                                                                                                                                                              <!-- ******************************************************************************* -->
                                                                                                                                                                        <td class="alignleft">
                                                                                                                                                                          <xsl:choose>
                                                                                                                                                                            <xsl:when test="@code = 'ND'">
                                                                                                                                                                              <select name="{@code}_delivery_date" id="delivery_date_select" onchange="doCheckSelection(this);">
                                                                                                                                                                                <xsl:for-each select="/root/shippingData/deliveryDates/deliveryDate">
                                                                                                                                                                                  <xsl:if test="contains(types, 'ND')">
                                                                                                                                                                                    <option value="{startDate}" endDate="{endDate}">
                                                                                                                                                                                      <xsl:if test="$containsErrors and
                                                                                                                                                                                                                                            startDate = $requestedDeliveryDate and
                                                                                                                                                                                                                                                                                                                      (	$requestedDeliveryDateRangeEnd = '' or
                                                                                                                                                                                                                                                                                                                            endDate = $requestedDeliveryDateRangeEnd
                                                                                                                                                                                                                                                                                                                      )">
                                                                                                                                                                                        <xsl:attribute name="selected"/>
                                                                                                                                                                                      </xsl:if>
                                                                                                                                                                                      <xsl:value-of select="displayDate"/>
                                                                                                                                                                                    </option>
                                                                                                                                                                                  </xsl:if>
                                                                                                                                                                                </xsl:for-each>
                                                                                                                                                                              </select>
                                                                                                                                                                            </xsl:when>
                                                                                                                                                            
                                                                                                                                                                            <xsl:when test="@code = '2F'">
                                                                                                                                                                              <select name="{@code}_delivery_date" id="delivery_date_select" onchange="doCheckSelection(this);">
                                                                                                                                                                                <xsl:for-each select="/root/shippingData/deliveryDates/deliveryDate">
                                                                                                                                                                                  <xsl:if test="contains(types, '2F')">
                                                                                                                                                                                    <option value="{startDate}" endDate="{endDate}">
                                                                                                                                                                                      <xsl:if test="$containsErrors and
                                                                                                                                                                                                                                            startDate = $requestedDeliveryDate and
                                                                                                                                                                                                                                                                                                                      (	$requestedDeliveryDateRangeEnd = '' or
                                                                                                                                                                                                                                                                                                                            endDate = $requestedDeliveryDateRangeEnd
                                                                                                                                                                                                                                                                                                                      )">
                                                                                                                                                                                        <xsl:attribute name="selected"/>
                                                                                                                                                                                      </xsl:if>
                                                                                                                                                                                      <xsl:value-of select="displayDate"/>
                                                                                                                                                                                    </option>
                                                                                                                                                                                  </xsl:if>
                                                                                                                                                                                </xsl:for-each>
                                                                                                                                                                              </select>
                                                                                                                                                                            </xsl:when>
                                                                                                                                                            
                                                                                                                                                                            <xsl:when test="@code = 'GR'">
                                                                                                                                                                              <select name="{@code}_delivery_date" id="delivery_date_select" onchange="doCheckSelection(this);">
                                                                                                                                                                                <xsl:for-each select="/root/shippingData/deliveryDates/deliveryDate">
                                                                                                                                                                                  <xsl:if test="contains(types, 'GR')">
                                                                                                                                                                                    <option value="{startDate}" endDate="{endDate}">
                                                                                                                                                                                      <xsl:if test="$containsErrors and
                                                                                                                                                                                                                                            startDate = $requestedDeliveryDate and
                                                                                                                                                                                                                                                                                                                      (	$requestedDeliveryDateRangeEnd = '' or
                                                                                                                                                                                                                                                                                                                            endDate = $requestedDeliveryDateRangeEnd
                                                                                                                                                                                                                                                                                                                      )">
                                                                                                                                                                                        <xsl:attribute name="selected"/>
                                                                                                                                                                                      </xsl:if>
                                                                                                                                                                                      <xsl:value-of select="displayDate"/>
                                                                                                                                                                                    </option>
                                                                                                                                                                                  </xsl:if>
                                                                                                                                                                                </xsl:for-each>
                                                                                                                                                                              </select>
                                                                                                                                                                            </xsl:when>
                                                                                                                                                            
                                                                                                                                                                            <xsl:when test="@code = 'SA'">
                                                                                                                                                                              <select name="{@code}_delivery_date" id="delivery_date_select" onchange="doCheckSelection(this);">
                                                                                                                                                                                <xsl:for-each select="/root/shippingData/deliveryDates/deliveryDate">
                                                                                                                                                                                  <xsl:if test="contains(types, 'SA')">
                                                                                                                                                                                    <option value="{startDate}" endDate="{endDate}">
                                                                                                                                                                                      <xsl:if test="$containsErrors and
                                                                                                                                                                                                                                            startDate = $requestedDeliveryDate and
                                                                                                                                                                                                                                                                                                                      (	$requestedDeliveryDateRangeEnd = '' or
                                                                                                                                                                                                                                                                                                                            endDate = $requestedDeliveryDateRangeEnd
                                                                                                                                                                                                                                                                                                                      )">
                                                                                                                                                                                        <xsl:attribute name="selected"/>
                                                                                                                                                                                      </xsl:if>
                                                                                                                                                                                      <xsl:value-of select="displayDate"/>
                                                                                                                                                                                    </option>
                                                                                                                                                                                  </xsl:if>
                                                                                                                                                                                </xsl:for-each>
                                                                                                                                                                              </select>
                                                                                                                                                                            </xsl:when>
                                                                                                                                                            
                                                                                                                                                                            <xsl:when test="@code = 'FL'">
                                                                                                                                                                              <select name="SD_delivery_date" id="delivery_date_select" onchange="doCheckSelection(this);">
                                                                                                                                                                                <xsl:for-each select="/root/shippingData/deliveryDates/deliveryDate">
                                                                                                                                                                                  <xsl:if test="contains(types, 'FL')">
                                                                                                                                                                                    <option value="{startDate}" endDate="{endDate}">
                                                                                                                                                                                      <xsl:if test="$containsErrors and
                                                                                                                                                                                                                                            startDate = $requestedDeliveryDate and
                                                                                                                                                                                                                                                                                                                      (	$requestedDeliveryDateRangeEnd = '' or
                                                                                                                                                                                                                                                                                                                            endDate = $requestedDeliveryDateRangeEnd
                                                                                                                                                                                                                                                                                                                      )">
                                                                                                                                                                                        <xsl:attribute name="selected"/>
                                                                                                                                                                                      </xsl:if>
                                                                                                                                                                                      <xsl:value-of select="displayDate"/>
                                                                                                                                                                                    </option>
                                                                                                                                                                                  </xsl:if>
                                                                                                                                                                                </xsl:for-each>
                                                                                                                                                                              </select>
                                                                                                                                                                            </xsl:when>
                                                                                                                                                            
                                                                                                                                                                            <xsl:when test="@code = 'SU'">
                                                                                                                                                                              <select name="{@code}_delivery_date" id="delivery_date_select" onchange="doCheckSelection(this);">
                                                                                                                                                                                <xsl:for-each select="/root/shippingData/deliveryDates/deliveryDate">
                                                                                                                                                                                  <xsl:if test="contains(types, 'SU')">
                                                                                                                                                                                    <option value="{startDate}" endDate="{endDate}">
                                                                                                                                                                                      <xsl:if test="$containsErrors and
                                                                                                                                                                                                                                            startDate = $requestedDeliveryDate and
                                                                                                                                                                                                                                                                                                                      (	$requestedDeliveryDateRangeEnd = '' or
                                                                                                                                                                                                                                                                                                                            endDate = $requestedDeliveryDateRangeEnd
                                                                                                                                                                                                                                                                                                                      )">
                                                                                                                                                                                        <xsl:attribute name="selected"/>
                                                                                                                                                                                      </xsl:if>
                                                                                                                                                                                      <xsl:value-of select="displayDate"/>
                                                                                                                                                                                    </option>
                                                                                                                                                                                  </xsl:if>
                                                                                                                                                                                </xsl:for-each>
                                                                                                                                                                              </select>
                                                                                                                                                                            </xsl:when>
                                                                                                                                                            
                                                                                                                                                                            <xsl:otherwise>
                                                                                                                                                                              <select name="{@code}_delivery_date" id="delivery_date_select" onchange="doCheckSelection(this);">
                                                                                                                                                                                <xsl:for-each select="/root/shippingData/deliveryDates/deliveryDate">
                                                                                                                                                                                    <option value="{startDate}" endDate="{endDate}">
                                                                                                                                                                                      <xsl:if test="$containsErrors and startDate = $requestedDeliveryDate and ($requestedDeliveryDateRangeEnd = '' or endDate = $requestedDeliveryDateRangeEnd )">
                                                                                                                                                                                        <xsl:attribute name="selected"/>
                                                                                                                                                                                      </xsl:if>
                                                                                                                                                                                      <xsl:value-of select="displayDate"/>
                                                                                                                                                                                    </option>
                                                                                                                                                                                </xsl:for-each>
                                                                                                                                                                              </select>
                                                                                                                                                                            </xsl:otherwise>
                                                                                                                                                                          </xsl:choose>
                                                                                                                                  
                                                                                                                                                                          <xsl:choose>
                                                                                                                                                                              <xsl:when test="boolean($isFloristOrder)">
                                                                                                                                                                                  <span style="padding-left:1em;">&nbsp;</span>
                                                                                                                                                                                  <img id="delivery_date_img" name="DateCalendar" src="images\calendar.gif"></img>
                                                                                                                                                                                  <xsl:if test="boolean($displayGetMax)">
                                                                                                                                                                                          <span style="padding-left:3em;">
                                                                                                                                                                                                  <button type="button" id="getMaxButton" class="bluebutton" onclick="javascript:doGetMaxDeliveryDateAction();">Get Max</button>
                                                                                                                                                                                          </span>
                                                                                                                                                                                  </xsl:if> 
                                                                                                                                                                              </xsl:when>
                                                                                                                                                                              <xsl:otherwise>
                                                                                                                                                                                  <span style="padding-left:1em;">&nbsp;</span>
                                                                                                                                                                                  <img id="delivery_date_img" name="DateCalendar" src="images\calendar.gif"></img>
                                                                                                                                                                              </xsl:otherwise>
                                                                                                                                                                          </xsl:choose>
                                                                                                                                                                        </td>
																				</tr>
                                                                                                                                                                </xsl:if>
																			</xsl:for-each>
                                                                                                                                                        
                                                                                                                                                        

                                                                                                                                                        <!-- ******************************************************************************* -->
                                                                                                                                                        <!-- Issue 2134 -->
                                                                                                                                                        <!-- ******************************************************************************* -->
                                                                                                                                                        <!-- Section for fringe condition where florist delivery on original order
                                                                                                                                                             but floral delivery no longer available.  Whatta pain :-(
                                                                                                                                                        -->
                                                                                                                                                        <xsl:if test="deliveryDates != ''">
                                                                                                                                                          <tr>
                                                                                                                                                            <td >
                                                                                                                                                                Will be delivered on &nbsp; &nbsp;
                                                                                                                                                            </td>
                                                                                                                                                            <td>
                                                                                                                                                                <select name="SD_delivery_date" id="delivery_date_select" onchange="doCheckSelection(this);">
                                                                                                                                                                  <xsl:for-each select="deliveryDates/deliveryDate">
                                                                                                                                                                  <option value="{startDate}" endDate="{endDate}">
                                                                                                                                                                    <xsl:if test="$defaultDates">
                                                                                                                                                                      <xsl:if test="startDate = $requestedDeliveryDate and ($requestedDeliveryDateRangeEnd = '' or endDate = $requestedDeliveryDateRangeEnd)"><xsl:attribute name="selected"/></xsl:if>
                                                                                                                                                                    </xsl:if>
                                                                                                                                                                    <xsl:value-of select="displayDate"/>
                                                                                                                                                                  </option>
                                                                                                                                                                  </xsl:for-each>
                                                                                                                                                                </select>
                                                                                                                                                            </td>
                                                                                                                                                          </tr>
                                                                                                                                                        </xsl:if>
                                                                                                                                                        <!-- End of fringe condition section -->
																		</table>
																	</td>
																</tr>
															</xsl:otherwise>
														</xsl:choose>
													</table>
												</div>
												<!-- end scrolling div -->
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<br/>
						<!-- /main table -->
						<!-- bottom buttons -->
						<div style="margin-left:7px;margin-top:0px;width:99%;align:center;">
							<div style="text-align:right">
								<span style="padding-right:2em;">
									<button type="button" tabindex="-1" id="updCompleteButton" accesskey="p" style="width:110px;" class="BigBlueButton" onclick="javascript:doCompleteUpdateAction();">Com(p)lete Update</button>
								</span>
								<button type="button" tabindex="-1" id="updCancelButton" accesskey="C" class="BigBlueButton" style="width:110px;" onclick="javascript:doCancelUpdateAction();">(C)ancel Update</button>
							</div>
						</div>
						<!-- /bottom buttons -->
					</form>
					<!-- Footer template -->
					<xsl:call-template name="footer"/>
					<!-- end main content div -->
				</div>
				<!-- Processing message div -->
				<div id="waitDiv" style="display:none">
					<table id="waitTable" width="98%" border="3" height="300px" align="center" cellpadding="30" cellspacing="1" bordercolor="#006699">
						<tr>
							<td width="100%">
								<table cellspacing="0" cellpadding="0" style="width:100%;height:300px">
									<tr>
										<td id="waitMessage" align="right" width="50%" class="waitMessage"></td>
										<td id="waitTD" width="50%" class="waitMessage"></td>
									</tr>
								</table></td>
						</tr>
					</table>
					<xsl:call-template name="footer"/>
				</div>
			</body>
		</html>
		<script type="text/javascript" language="javascript">
                var updateValidationFail = '<xsl:value-of select="$success"/>';
                var updateValidationMessage = '<xsl:value-of select="$message_display"/>';
                var isFloristOrder = <xsl:value-of select="$isFloristOrder"/>;
 <![CDATA[  
  
  /* function to disable dates in the dthml calendar. Only the available delivery dates
     from the delivery date select box should be enabled.
  */
  function isDeliveryDate(date, y, m, d) {
        var dd = document.getElementById("delivery_date_select");
        for (var i = 0; dd.options.length > i; i++) {
            var option = dd[i];
            var startDate = option.value;
            var date2 = new Date(startDate);
            if(date.getFullYear() == date2.getFullYear() &&
                date.getMonth() == date2.getMonth() &&
                date.getDate() == date2.getDate()) {
                
                    return false;
            }
        }
          return true;
          // return true if you want to disable other dates
    }

    /* this function is to update the delivery date select box.
       automatically gets called by dhtml calendar to update the delivery date.
    */
    function updateDeliveryDateSelect(cal) {
        var date = cal.date;
        var ddSelect = document.getElementById("delivery_date_select");
        var optLength = ddSelect.options.length;
        for (var i = 0; optLength > i; i++) {
            var option = ddSelect.options[i];
            var startDate = option.value;
            var date2 = new Date(startDate);
                if(date.getFullYear() == date2.getFullYear() &&
                    date.getMonth() == date2.getMonth() &&
                    date.getDate() == date2.getDate() )  {
                    
                        option.selected = true;
                }
        }
    }
    
   
  var ddSelect = document.getElementById("delivery_date_select");

  if(ddSelect != null ){ 
    Calendar.setup({
        inputField     :    "delivery_date_select",     // id of the input field
        ifFormat       :    "%m/%d/%Y",  // the date format
        button         :    "delivery_date_img",  // trigger for the calendar (button ID)
        align          :    "Tl",           // alignment (defaults to "Bl")
        singleClick    :    true,
        dateStatusFunc :    isDeliveryDate,
        onUpdate       :    updateDeliveryDateSelect
    });
    
    }else{
       alert("No additional delivery dates available for selected product, please use Update Order to select a different product.");
    }
  
    ]]></script>
	</xsl:template>
</xsl:stylesheet>