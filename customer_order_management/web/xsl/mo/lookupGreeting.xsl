<!DOCTYPE ACDemo[
  <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:import href="../header.xsl"/>
<xsl:import href="../footer.xsl"/>
<xsl:import href="../securityanddata.xsl"/>
<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>
<xsl:output method="html" indent="yes"/>

<xsl:key name="pageData" match="/root/pageData/data" use="name"/>
<xsl:key name="security" match="/root/security/data" use="name"/>
<xsl:variable name="productImages" select="key('pageData', 'productImages')/value"/>
<xsl:template match="/root">

<html>
<head>
  <title>FTD - Lookup Greeting Card</title>
  <link rel="stylesheet" type="text/css" href="css/ftd.css"/>
  <script language="javascript" src="js/FormChek.js"/>
  <script language="javascript" src="js/util.js"/>
  <script language="javascript">
  /*
   *  Global variables
   */
    var productImages = "<xsl:value-of select="$productImages"/>";<![CDATA[

  /*
   *  Actions
   */
    function doCloseAction(cardId, cardPrice){
      if (window.event)
        if (window.event.keyCode)
          if (window.event.keyCode != 13)
            return false;

      var retValue = new Array();
      retValue[0] = cardId;
      retValue[1] = cardPrice;
      top.returnValue = retValue;
      top.close();
    }

    function doShowDetailAction(cardId){
      if (window.event.keyCode == 13)
        openDetailPopup(cardId);
    }

    function doReturnToMainMenuAction(){
        doCloseAction("","");
    }

  /*
   *  Popups
   */
    function openDetailPopup(cardId){
      var form = document.getElementById("lookupGreetingForm");
      var url_source =  "lookupGreeting.do" +
                        "?action=detail" +
                        "&cardIdInput=" + cardId +
                        "&occasionInput=" + form.occasionInput.value +
                        getSecurityParams(false);

      var modal_dim="dialogWidth:600px; dialogHeight:625px; center:yes; scroll:no";
      var ret = showModalDialogIFrame(url_source, "", modal_dim);

      //if the return value is BACK then the back button was clicked, so keep this window open
      if (ret && ret != null && ret[0] == "BACK") {
        return false;
      }

      //if a return value is received from the detail page, then populate the main page
      if (ret && ret != null && ret[0] != '') {
        doCloseAction(ret[0], ret[1]);
      }

      //if a blank return value is received from the detail page then also close this window
      if(ret && ret[0] == '') {
        doCloseAction("","");
      }

      //the X icon was clicked, so keep this window open
      if (!ret) {
        return false;
      }
    }]]>
  </script>
</head>

<body>
  <form name="lookupGreetingForm" method="get">
    <xsl:call-template name="securityanddata"/>
    <xsl:call-template name="decisionResultData"/>
    <input type="hidden" name="occasionInput" id="occasionInput" value="{key('pageData', 'occasionInput')/value}"/>

    <!-- Header template -->
    <xsl:call-template name="header">
      <xsl:with-param name="headerName" select="'Lookup Greeting Card'"/>
      <xsl:with-param name="cdispEnabledFlag" select="$cdisp_enabledFlag"/>
    </xsl:call-template>

    <table width="98%" border="0" cellpadding="2" cellspacing="2" align="center">
      <tr>
        <td class="instruction" colspan="3">
          Click on image or link for larger photo &amp; details or click on the button to select the card.<br/>
          The card you select will be filled in automatically on the order checkout form.
        </td>
      </tr>
      <tr>
        <td align="right">
          <button type="button" class="BlueButton" tabindex="501" border="0" onclick="javascript:doCloseAction('','');" onkeydown="javascript:doCloseAction('','')">Close screen</button>
        </td>
      </tr>
    </table>

    
    <table width="98%" height="60%" class="LookupTable" align="center">
      <tr valign="top">
        <td>
          <div id="cardtable" style="overflow:auto; width:100%; height:100%; padding:0px; margin:0px">
          <table width="98%" border="1" class="scrollTable" cellpadding="2" cellspacing="2">
            <tr valign="top">
              <script language="JavaScript">
                var counter = 0;
                var price = "";
              </script>
              <xsl:for-each select="searchResults/searchResult">
                <xsl:variable name="curPrice" select="format-number(@price, '#0.00')"/>
                <td align="center" width="33%" valign="top">
                  <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                      <td width="40%" align="center" valign="top">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                          <tr>
                            <td align="center" valign="top">
                              <img onclick="javascript:openDetailPopup('{@addonid}');" onkeydown="javascript:doShowDetailAction('{@addonid}');" tabindex="2" src="{$productImages}{@addonid}_1.gif"/>
                            </td>
                          </tr>
                          <tr>
                            <td align="center" valign="top">
                              <a style="cursor:hand;" onclick="javascript:openDetailPopup('{@addonid}');" onkeydown="javascript:doShowDetailAction('{@addonid}');" tabindex="2">
                                <span class="textLink"><xsl:value-of disable-output-escaping="yes" select="@description"/></span>
                              </a>
                            </td>
                          </tr>
                          <tr>
                            <td align="center" valign="top">
                              <button type="button" class="BlueButton" onclick="javascript:doCloseAction('{@addonid}','{$curPrice}');" onkeydown="javascript:doCloseAction('{@addonid}','{$curPrice}');" tabindex="2">Select</button>
                            </td>
                          </tr>
                        </table>
                      </td>
                      <td rowspan="3" valign="top">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                          <tr><td colspan="2" class="label">Frontx:</td></tr>
                          <tr><td colspan="2"><xsl:value-of disable-output-escaping="yes" select="@description"/></td></tr>
                          <tr><td colspan="2" class="label">&nbsp;</td></tr>
                          <tr><td colspan="2" class="label">Inside:</td></tr>
                          <tr><td colspan="2"><xsl:value-of disable-output-escaping="yes" select="@inside"/><br/></td></tr>
                          <tr><td colspan="2" class="label">&nbsp;</td></tr>
                          <tr>
                            <td width="20%" class="label">Price:</td>
                            <td width="80%">
                              <script language="javascript">
                                price = "<xsl:value-of select="@price"/>";<![CDATA[
                                var place = price.indexOf(".");
                                var rightOf = price.substring(place, 10);
                                if ((rightOf.length) == 2)
                                  price += "0";

                                document.write("&nbsp;$" + price);]]>
                               </script>
                            </td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </table>
                </td>
                <script language="javascript"><![CDATA[
                  //start a new row with each 4th card
                  counter = counter + 1;
                  if ((counter % 3) == 0)
                    document.write("</tr><tr>");]]>
                </script>
              </xsl:for-each>
            </tr>
          </table>
          </div>
        </td>
      </tr>
    </table>
    

    <table width="98%" border="0" cellpadding="2" cellspacing="2" align="center">
      <tr>
        <td align="right">
          <button type="button" class="BlueButton" border="0" tabindex="500" onclick="javascript:doCloseAction('','');" onkeydown="javascript:doCloseAction('','');">Close screen</button>
        </td>
      </tr>
    </table>

    <!-- Footer template -->
    <xsl:call-template name="footer"/>
  </form>
</body>
</html>
</xsl:template>
</xsl:stylesheet>