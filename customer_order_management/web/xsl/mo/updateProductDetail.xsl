<!DOCTYPE ACDemo [
  <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<!-- External templates -->
<xsl:import href="../header.xsl"/>
<xsl:import href="../footer.xsl"/>
<xsl:import href="../securityanddata.xsl"/>
<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>
<xsl:import href="cusProduct.xsl"/>
<xsl:import href="floristDeliveryDates.xsl"/>
<xsl:import href="vendorDeliveryDates.xsl"/>

  <xsl:decimal-format NaN="0.00" name="notANumber"/>

<!-- Keys -->
<xsl:key name="addOn" match="/root/PRODUCTRESULTS/EXTRAINFO/ADDONS/ADDON" use="@addonid"/>
<xsl:key name="pageData" match="/root/pageData/data" use="name"/>
<xsl:key name="security" match="/root/security/data" use="name"/>
<xsl:variable name="isFloristOrder" select="boolean(key('pageData','display_format')/value = 'F')"/>


<!-- Constants -->
<xsl:variable name="OK" select="'O'"/>
<xsl:variable name="CONFIRM" select="'C'"/>
<xsl:variable name="YES" select="'Y'"/>
<xsl:variable name="upper_case">ABCDEFGHIJKLMNOPQRSTUVWXYZ</xsl:variable>
<xsl:variable name="lower_case">abcdefghijklmnopqrstuvwxyz</xsl:variable>


<!-- Variables -->
<xsl:variable name="deliveryData" select="/root/deliveryDates/deliveryDate"/>


<!-- product data -->
	<!-- Standard-->
		<xsl:variable name="standardDiscountPrice"					select="format-number(/root/PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@standarddiscountprice, 	'#0.00','notANumber')"/>
		<xsl:variable name="standardPrice"						select="format-number(/root/PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@standardprice, 		'#0.00','notANumber')"/>
		<xsl:variable name="standardRewardValue"					select="/root/PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@standardrewardvalue"/>
	<!-- Deluxe-->
		<xsl:variable name="deluxeDiscountPrice"					select="format-number(/root/PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@deluxediscountprice, 		'#0.00','notANumber')"/>
		<xsl:variable name="deluxePrice"						select="format-number(/root/PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@deluxeprice, 			'#0.00','notANumber')"/>
		<xsl:variable name="deluxeRewardValue"						select="/root/PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@deluxerewardvalue"/>
	<!-- Premium-->
		<xsl:variable name="premiumDiscountPrice" 					select="format-number(/root/PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@premiumdiscountprice, 		'#0.00','notANumber')"/>
		<xsl:variable name="premiumPrice" 						select="format-number(/root/PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@premiumprice, 			'#0.00','notANumber')"/>
		<xsl:variable name="premiumRewardValue"						select="/root/PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@premiumrewardvalue"/>
	<!-- Variable-->
		<xsl:variable name="variablePriceFlag"						select="/root/PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@variablepriceflag"/>
		<xsl:variable name="variablePriceMax" 						select="format-number(/root/PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@variablepricemax, 		'#0.00','notANumber')"/>
		<xsl:variable name="weboeFlag" 						        select="/root/PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@weboeblocked"/>
                <xsl:variable name="productType" 						select="/root/PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@producttype"/>
        <!-- Personal Greeting -->
		<xsl:variable name="personalGreetingFlag"					select="/root/PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@personalgreetingflag"/>
                <xsl:variable name="personalGreetingId"		        			select="/root/PD_ORIG_PRODUCTS/PD_ORIG_PRODUCT/personal_greeting_id"/>


<!-- this is the data that the CSR selected before being re-routed to this page -->
<xsl:variable name="requestedSizeDescription"				select="key('pageData', 'requested_size_description')/value"/>
<xsl:variable name="requestedPrice"          	  		select="format-number(/root/requestOrderDetailVO/OrderDetailVO/OrderBillVO/productAmount,							'#,###,##0.00','notANumber')"/>
<xsl:variable name="requestedSubstitutionIndicator"	select="/root/requestOrderDetailVO/OrderDetailVO/substitution_indicator"/>

<!-- original order/product data -->
<xsl:variable name="ORIG_product_id" 								select="key('pageData', 'orig_product_id')/value"/>
<xsl:variable name="ORIG_product_amount" 						select="format-number(key('pageData',	'orig_product_amount')/value, 															'#,###,##0.00','notANumber')"/>


<!-- new order/product data -->
<xsl:variable name="NEW_product_id" 								select="key('pageData', 'product_id')/value"/>
<xsl:variable name="NEW_product_amount"							select="format-number(/root/PD_ORDER_PRODUCTS/PD_ORDER_PRODUCT/product_amount,										'#,###,##0.00','notANumber')"/>

<!-- other data -->
<xsl:variable name="floristServiceCharges" 					select="key('pageData', 'floral_service_charges')/value"/>
<xsl:variable name="containsErrors"     						select="boolean(/root/ERROR_MESSAGES/ERROR_MESSAGE)"/>
<xsl:variable name="agricultureRestrictionFound"		select="key('pageData','agricultureRestrictionFound')/value"/>
<xsl:variable name="compAddonText"  select="key('pageData','compAddonText')/value"/>

<xsl:variable name="standardLabel" select="key('pageData','floralLabelStandard')/value"/>
<xsl:variable name="deluxeLabel" select="key('pageData','floralLabelDeluxe')/value"/>
<xsl:variable name="premiumLabel" select="key('pageData','floralLabelPremium')/value"/>

<xsl:param name="addOnFlag" select="false()"/>

<xsl:output method="html" indent="yes"/>
<xsl:template match="/root">
<html>
<head>
 <title>FTD - Product Detail</title>
 <link rel="stylesheet" type="text/css" href="css/ftd.css"/>
  <style type="text/css">
    .blueBanner{
     color: #006699;
     height:5px;
    }
  	img{cursor:hand;}
  </style>
 <script language="javascript" src="js/tabIndexMaintenance.js"/>
 <script language="javascript" src="js/FormChek.js"/>
 <script language="javascript" src="js/util.js"/>
 <script language="javascript" src="js/commonUtil.js"/>
 <script language="javascript" src="js/updateProductDetail.js"/>
 <script language="javascript">
 
/*
 *  Global variables
 */
var containsErrors = <xsl:value-of select="boolean(ERROR_MESSAGES/ERROR_MESSAGE)"/>;
var fieldErrors = new Array( <xsl:for-each select="ERROR_MESSAGES/ERROR_MESSAGE[FIELDNAME!='']">["<xsl:value-of select="FIELDNAME"/>","<xsl:value-of select="TEXT"/>",parent.validateFormHelper,false]<xsl:choose><xsl:when test="position()!=last()">,</xsl:when></xsl:choose></xsl:for-each> );
var okErrors = new Array( <xsl:for-each select="ERROR_MESSAGES/ERROR_MESSAGE[POPUP_INDICATOR=$OK]">["<xsl:value-of select="@id"/>", "<xsl:value-of select="TEXT"/>", "<xsl:value-of select="POPUP_GOTO_PAGE"/>" , "<xsl:value-of select="POPUP_CANCEL_PAGE"/>"]<xsl:choose><xsl:when test="position()!=last()">,</xsl:when></xsl:choose></xsl:for-each> );
var confirmErrors = new Array( <xsl:for-each select="ERROR_MESSAGES/ERROR_MESSAGE[POPUP_INDICATOR=$CONFIRM]">["<xsl:value-of select="@id"/>", "<xsl:value-of select="TEXT"/>", "<xsl:value-of select="POPUP_GOTO_PAGE"/>" , "<xsl:value-of select="POPUP_CANCEL_PAGE"/>"]<xsl:choose><xsl:when test="position()!=last()">,</xsl:when></xsl:choose></xsl:for-each> );
var isAvailable = true;
var variablePriceMax = "<xsl:value-of select="format-number(/root/PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@variablepricemax, 				'#0.00','notANumber')"/>";
<!-- Product is unavailable and it's not because of agriculture restrictions -->
<xsl:if test="key('pageData', 'displayProductUnavailable')/value = 'Y' and $agricultureRestrictionFound = 'N'">
  <xsl:choose>
    <xsl:when test="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@status = 'A'">
      okErrors.push([null, "The product is not currently available in your delivery area.<br/><br/>Do you wish to continue?", "loadProductCategory.do", null]);
    </xsl:when>
    <xsl:otherwise>
      isAvailable = false;
    </xsl:otherwise>
  </xsl:choose>
</xsl:if>


<!-- Drop Ship product unavailable for PR, VI, CAN-->
<xsl:if test="key('pageData', 'dropShipAllowed')/value = 'N'">
  okErrors.push([null, "Drop Ship products cannot be delivered outside of the US.", "loadProductCategory.do", null]);
</xsl:if>


<!-- Codified Special -->
<xsl:if test="key('pageData', 'displayCodifiedSpecial')/value = 'Y'">
  okErrors.push([null, "We're sorry the product you've selected is currently not available in the zip code you entered.  However, the majority of our flowers can be delivered to this zip code.<br/><br/>Would you like to shop for another item?", "loadProductCategory.do", null]);
</xsl:if>

<!-- No Product -->
<xsl:if test="key('pageData', 'displayNoProductPopup')/value = 'Y'">
  okErrors.push([null, "I'm sorry but the zip code you have provided is not currently serviced by an FTD florist or FedEx.<br/><br/>Would you like to shop for another item?", "loadProductCategory.do", null]);
</xsl:if>

<!-- No Floral -->
<xsl:if test="key('pageData', 'displaySpecGiftPopup')/value = 'Y'">
	confirmErrors.push([null, "There are currently no florists available to deliver to this zip/postal code.  If you continue you will need to enter in a filling florist code number on the Update Order confirmation Screen.  Do you wish to continue?", null, "loadProductCategory.do"]);
</xsl:if>

<!-- GNADD Special -->
<xsl:if test="key('pageData', 'displayUpsellGnaddSpecial')/value = 'Y'">
  <xsl:variable name="name" select="key('pageData', 'upsellBaseName')/value"/>
  <xsl:variable name="date" select="key('pageData', 'upsellBaseDeliveryDate')/value"/>
  okErrors.push([null, "NEED IT THERE SOONER?<xsl:value-of select='$name'/>&nbsp;is available for delivery as soon as&nbsp;<xsl:value-of select='$date'/>.", null, null]);
</xsl:if>

<!-- Special Fee -->
<xsl:if test="key('pageData', 'displaySpecialFee')/value = 'Y'">
  okErrors.push([null, "A $12.99 surcharge will be added to deliver this item to Alaska or Hawaii.", null, null]);
</xsl:if>


<!-- Personal Greeting -->
<xsl:if test="$personalGreetingFlag = 'N' and $personalGreetingId != '' ">
  okErrors.push([null, "You are changing a Personal Greeting order to a non-Personal Greeting order.  Any personalized greeting created by customer will not be associated with this product.", null, null]);
</xsl:if>

<xsl:if test="string-length(key('pageData', 'product_attribute_exclusions')/value) > 0 ">
  okErrors.push([null, '<xsl:value-of select="key('pageData', 'product_attribute_exclusions')/value"/>', "loadProductCategory.do", null]);
</xsl:if>

var product_images = "<xsl:value-of select="key('pageData', 'product_images')/value"/>";
var novator_id = "<xsl:value-of select="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@novatorid"/>";
var shipMethodFlorist = "<xsl:value-of select="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/PRODUCT/@shipmethodflorist"/>";
var shipMethodCarrier = "<xsl:value-of select="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/PRODUCT/@shipmethodcarrier"/>";

// variable price min is the shown price of the product searched on
var variablePriceMinStandard = "<xsl:value-of select="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@standardprice"/>";
var variablePriceMinText = "<xsl:value-of select="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@standardprice"/>";
var productCount = <xsl:value-of select="count(PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT)"/>;
var productStatus = "<xsl:value-of select="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@status"/>";
var isFlorist = <xsl:value-of select="$isFloristOrder"/>;

</script>
</head>
<body onload="javascript:init();">

  <!-- Iframe used to get max delivery dates -->
  <iframe id="updateProductDetailActions" width="0px" height="0px" border="0" src="blank.html"/>


 <!-- Header template -->
 <xsl:call-template name="header">
    <xsl:with-param name="headerName" select="'Update Product Detail'"/>
    <xsl:with-param name="showExitButton" select="true()"/>
    <xsl:with-param name="showTime" select="true()"/>
    <xsl:with-param name="showCSRIDs" select="true()"/>
    <xsl:with-param name="dnisNumber" select="$call_dnis"/>
    <xsl:with-param name="cservNumber" select="$call_cs_number"/>
    <xsl:with-param name="indicator" select="$call_type_flag"/>
    <xsl:with-param name="brandName" select="$call_brand_name"/>
    <xsl:with-param name="overrideRTQ" select="Y"/>
    <xsl:with-param name="cdispEnabledFlag" select="$cdisp_enabledFlag"/>
 </xsl:call-template>

  <!-- Content div -->
  <div id="mainContent" style="display:block">

 <!-- Customer Product data : display if 1) product is found and 2a) its available or 2b)it's unavailable due to
 															agriculture restrictions-->
 <xsl:if test="count(PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT)> 0 and
 			(	key('pageData', 'displayProductUnavailable')/value = 'N' 			or
 				(key('pageData', 'displayProductUnavailable')/value = 'Y' and $agricultureRestrictionFound = 'Y')
		 			) and
                               key('pageData', 'displayPersonalizedProduct')/value = 'N'">
   <xsl:call-template name="cusProduct">
     <xsl:with-param name="subheader" select="subheader"/>
     <xsl:with-param name="displayContinue" select="false()"/>
     <xsl:with-param name="displayComplete" select="true()"/>
     <xsl:with-param name="displayCancel" select="true()"/>
   </xsl:call-template>
  </xsl:if>

  <form name="productDetailForm" id="productDetailForm" method="post" action="updateProductDetail.do">
    <xsl:call-template name="securityanddata"/>
    <xsl:call-template name="decisionResultData"/>
    <input type="hidden" name="display_page" id="display_page" value=""/>
    <input type="hidden" name="greeting_card" id="greeting_card" value=""/>
    <input type="hidden" name="mo_update_flag" id="mo_update_flag" value="N"/>
    <input type="hidden" name="order_detail_id" id="order_detail_id" value="{key('pageData','order_detail_id')/value}"/>
    <input type="hidden" name="customer_id" id="customer_id" value="{key('pageData','customer_id')/value}"/>
    <input type="hidden" name="domestic_service_fee" id="domestic_service_fee" value="{key('pageData', 'domestic_service_fee')/value}"/>
    <input type="hidden" name="master_order_number" id="master_order_number" value="{key('pageData', 'master_order_number')/value}"/>
    <input type="hidden" name="external_order_number" id="external_order_number" value="{key('pageData', 'external_order_number')/value}"/>
    <input type="hidden" name="item_order_number" id="item_order_number" value="{key('pageData', 'item_order_number')/value}"/>
    <input type="hidden" name="buyer_full_name" id="buyer_full_name" value="{key('pageData', 'buyer_full_name')/value}"/>
    <input type="hidden" name="price_point_id" id="price_point_id" value="{key('pageData', 'price_point_id')/value}"/>
    <input type="hidden" name="category_index" id="category_index" value="{key('pageData', 'category_index')/value}"/>
    <input type="hidden" name="occasion_description" id="occasion_description" value="{key('pageData', 'occasion_description')/value}"/>
    <input type="hidden" name="domestic_flag" id="domestic_flag" value="{key('pageData', 'domestic_flag')/value}"/>
    <input type="hidden" name="occasion" id="occasion" value="{key('pageData', 'occasion')/value}"/>
    <input type="hidden" name="recipient_zip_code" id="recipient_zip_code" value="{key('pageData', 'recipient_zip_code')/value}"/>
    <input type="hidden" name="source_code" id="source_code" value="{key('pageData', 'source_code')/value}"/>
    <input type="hidden" name="order_guid" id="order_guid" value="{key('pageData','order_guid')/value}"/>
    <input type="hidden" name="script_code" id="script_code" value="{key('pageData', 'script_code')/value}"/>
    <input type="hidden" name="search_type" id="search_type" value="{key('pageData', 'search_type')/value}"/>
    <input type="hidden" name="total_pages" id="total_pages" value="{key('pageData', 'total_pages')/value}"/>
    <input type="hidden" name="page_number" id="page_number" value="{key('pageData', 'page_number')/value}"/>
    <input type="hidden" name="upsell_flag" id="upsell_flag" value="{key('pageData', 'upsell_flag')/value}"/>
    <input type="hidden" name="company_id" id="company_id" value="{key('pageData', 'company_id')/value}"/>
    <input type="hidden" name="partner_id" id="partner_id" value="{key('pageData', 'partner_id')/value}"/>
    <input type="hidden" name="product_id" id="product_id" value="{key('pageData', 'product_id')/value}"/>
    <input type="hidden" name="orig_product_id" id="orig_product_id" value="{key('pageData', 'orig_product_id')/value}"/>
    <input type="hidden" name="list_flag" id="list_flag" value="{key('pageData', 'list_flag')/value}"/>
    <input type="hidden" name="sort_type" id="sort_type" value="{key('pageData', 'sort_type')/value}"/>
    <input type="hidden" name="page_name" id="page_name" value="{key('pageData', 'page_name')/value}"/>
    <input type="hidden" name="upsell_id" id="upsell_id" value="{key('pageData', 'upsell_id')/value}"/>
    <input type="hidden" name="jcp_flag" id="jcp_flag" value="{key('pageData', 'jcp_flag')/value}"/>
    <input type="hidden" name="recipient_country" id="recipient_country" value="{key('pageData', 'recipient_country')/value}"/>
    <input type="hidden" name="recipient_state" id="recipient_state" value="{key('pageData', 'recipient_state')/value}"/>
    <input type="hidden" name="recipient_city" id="recipient_city" value="{key('pageData', 'recipient_city')/value}"/>
    <input type="hidden" name="delivery_date" id="delivery_date" value="{key('pageData','delivery_date')/value}"/>
    <input type="hidden" name="delivery_date_range_end" id="delivery_date_range_end" value="{key('pageData','delivery_date_range_end')/value}"/>
    <input type="hidden" name="orig_product_amount" id="orig_product_amount" value="{key('pageData','orig_product_amount')/value}"/>
    <input type="hidden" name="product_amount" id="product_amount" value="{key('pageData','product_amount')/value}"/>
    <input type="hidden" name="origin_id" id="origin_id" value="{key('pageData','origin_id')/value}"/>
    <input type="hidden" name="size_indicator" id="size_indicator" value=""/>
    <input type="hidden" name="card_quantity" id="card_quantity"  value=""/>
    <input type="hidden" name="ship_method" id="ship_method" value=""/>
    <input type="hidden" name="subcode" id="subcode"  value=""/>
    <input type="hidden" name="product_sub_type" id="product_sub_type" value="{PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@productsubtype}"/>
    <input type="hidden" name="reward_type" id="reward_type" value="{key('pageData','reward_type')/value}"/>
    <input type="hidden" name="zip_sunday_flag" id="zip_sunday_flag" value="{PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@zipsundayflag}"/>
    <input type="hidden" name="exception_start_date" id="exception_start_date" value="{PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@exceptionstartdate}"/>
    <input type="hidden" name="exception_end_date" id="exception_end_date" value="{PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@exceptionenddate}"/>
    <input type="hidden" name="product_type" id="product_type" value="{PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@producttype}"/>
    <input type="hidden" name="ship_method_carrier" id="ship_method_carrier" value="{PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@shipmethodcarrier}"/>
    <input type="hidden" name="ship_method_florist" id="ship_method_florist" value="{PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@shipmethodflorist}"/>
    <input type="hidden" name="ignore_error" id="ignore_error" value="N"/>
    <input type="hidden" name="special_instructions" id="special_instructions" value="{/root/PD_ORIG_PRODUCTS/PD_ORIG_PRODUCT/special_instructions}"/>
    <input type="hidden" name="card_add_on_id_to_be_deleted" id="card_add_on_id_to_be_deleted" value="{key('pageData','card_add_on_id_to_be_deleted')/value}"/>
    <input type="hidden" name="variable_price_selected" id="variable_price_selected" value="n"/>
    <input type="hidden" name="personal_greeting_id" id="personal_greeting_id" value="{/root/PD_ORIG_PRODUCTS/PD_ORIG_PRODUCT/personal_greeting_id}"/>
    <input type="hidden" name="personal_greeting_flag" id="personal_greeting_flag" value="{/root/PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@personalgreetingflag}"/>
   
    
    <!-- Customer Product params -->
    <xsl:call-template name="cusProduct-params">
      <xsl:with-param name="subheader" select="subheader"/>
    </xsl:call-template>

    <!-- Cancel Update IFrame -->
    <iframe id="cancelUpdateFrame" width="0px" height="0px" border="0"/>

    <!-- Locking IFrame -->
    <iframe id="checkLock" width="0px" height="0px" border="0"/>

    <table border="0" align="center" cellpadding="0" cellspacing="0" width="98%">
      <tr>
        <td>
          <button type="button" class="BlueButton" onclick="javascript:doUpdateDeliveryInformation()">Update Delivery Information</button>
          &nbsp;&nbsp;
          <button type="button" class="BlueButton" onclick="javascript:doUpdateProductCategory()">Update Product Category</button>
        </td>
      </tr>
    </table>

    <!-- Main table -->
    <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
      <tr>
        <td>
          <table width="100%" border="0" cellpadding="2" cellspacing="2">
            <tr>
              <td colspan="3" width="100%" align="center">
                <!-- Scrolling div contiains product detail or no products found message -->
			            <div id="productDetail" style="overflow:auto; width:100%; height:600; padding:0px; margin:0px">
                  <!-- Product detail information or No Products Found -->
                  <xsl:choose>
	            <!-- Web OE Blocked flag is on-->
                    <xsl:when test="$weboeFlag = 'Y'">
                      <table align="center" width="75%" height="25%" border="0" cellspacing="2" cellpadding="2">
                        <tr>
                          <td align="center" class="waitMessage">
                            I'm sorry but this product is unavailable for phone orders.  Please select another item.
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" align="right">
                            <button type="button" tabindex="-1" class="BigBlueButton" style="width:160px;" onclick="javascript:doUpdateProductCategoryAction();">Update Product Category</button>
                          </td>
                        </tr>
                      </table>
                    </xsl:when>
                    
                    <!-- User is trying to select a Personal Greeting Product on a non-Personal Greeting order-->
                    <xsl:when test="$personalGreetingFlag = 'Y' and $personalGreetingId = '' ">
                      <table align="center" width="75%" height="25%" border="0" cellspacing="2" cellpadding="2">
                        <tr>
                          <td align="center" class="waitMessage">
                            Cannot change to Personal Greeting Product, please ask customer to choose another product.
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" align="right">
                            <button type="button" tabindex="-1" class="BigBlueButton" style="width:160px;" onclick="javascript:doUpdateProductCategoryAction();">Update Product Category</button>
                          </td>
                        </tr>
                      </table>
                    </xsl:when>               

                    <!-- display if 1)prod-id exist and 2)prod count > 0 and 3a)prod avlble or 3b)prod unavail because of agriculture restrictions -->
                    <xsl:when test="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT[@productid!=''] and
                    								count(PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT) > 0 and
                    								(	key('pageData', 'displayProductUnavailable')/value = 'N' or
                    									(key('pageData', 'displayProductUnavailable')/value = 'Y' and $agricultureRestrictionFound = 'Y')
                    								) and
                                                                                key('pageData', 'displayPersonalizedProduct')/value = 'N'">
                      <table width="100%" border="0" cellspacing="2" cellpadding="2">
                        <!-- Product name and id -->
                        <tr>
                          <td colspan="3">
                            <span class="header"><xsl:value-of disable-output-escaping="yes" select="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@novatorname"/></span>
                            <span class="label">&nbsp;&nbsp;#<xsl:value-of select="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@productid"/></span>
                          </td>
                        </tr>
                        <!-- Product image, description, delivery policy, etc. -->
                        <tr valign="top">
                          <td align="center">
                            <script language="javascript">
                            // Exception display trigger variables
                            var exceptionMessageTrigger = false;
                            var selectedDate = new Date("<xsl:value-of select="key('pageData', 'requestedDeliveryDate')/value"/>");
                            var exceptionStart = new Date("<xsl:value-of select="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@exceptionstartdate"/>");
                            var exceptionEnd = new Date("<xsl:value-of select="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@exceptionenddate"/>");<![CDATA[
                            document.write("<img tabindex=\"-1\" border=\"0\" height=\"60\" width=\"60\" ");
                            document.write("ONERROR=\"var imag = this.src.substring(this.src.length-9, this.src.length); ");
                            document.write("if (imag!='npi_1.gif'){ ");
                            document.write("this.src='" + product_images + "npi_1.gif\'}\" ");
                            document.write("src='" + product_images + novator_id + "_1.gif' /> ");
                            ]]>
                            </script>
                            <br />
                            <a tabindex="-1" class="textlink" href="#" onclick="javascript:doLargeImagePopup();">View Larger Image</a>
                          </td>
                          <td colspan="2" align="left">
                            <table width="100%" border="0" cellpadding="2" cellspacing="2">
                              <tr>
                                <td class="description" colspan="2"><xsl:value-of select="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@longdescription"  disable-output-escaping="yes"/></td>
                              </tr>
                              <xsl:if test="$compAddonText != '' ">
                                <tr>
                                    <td colspan="2"><b>*<xsl:value-of select="$compAddonText"/></b></td>
                                </tr>
                              </xsl:if>
                              <xsl:choose>
                                <xsl:when test="key('pageData', 'requestedDeliveryDate')/value">
                                  <xsl:if test="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@exceptioncode = 'U'">
                                    <script language="JavaScript"><![CDATA[
                                      if (selectedDate >= exceptionStart && selectedDate <= exceptionEnd) {
                                        exceptionMessageTrigger = true;
                                        document.write("<tr><td colspan=\"2\" class=\"labelleft\" align=\"left\"><font color=\"red\">This product is not available for delivery from&nbsp;]]><xsl:value-of select="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@exceptionstartdate"/><![CDATA[&nbsp;to&nbsp;]]><xsl:value-of select="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@exceptionenddate"/><![CDATA[</font></td></tr>");
                                      }]]>
                                    </script>
                                  </xsl:if>
                                  <xsl:if test="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@exceptioncode = 'A'">
                                    <script language="javascript"><![CDATA[
                                      if (selectedDate < exceptionStart || selectedDate > exceptionEnd) {
                                        exceptionMessageTrigger = true;
                                        document.write("<tr><td colspan=\"2\" class=\"labelleft\" align=\"left\"><font color=\"red\">This product is only available for delivery from&nbsp;]]><xsl:value-of select="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@exceptionstartdate"/><![CDATA[&nbsp;to&nbsp;]]><xsl:value-of select="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@exceptionenddate"/><![CDATA[</font></td></tr>");
                                      }]]>
                                    </script>
                                  </xsl:if>

                                  <script language="javascript"><![CDATA[
                                    if (exceptionMessageTrigger == true)
                                      document.write("<tr><td colspan=\"2\" class=\"labelleft\" align=\"left\"> <font color=\"red\">]]><xsl:value-of select="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@exceptionmessage"/><![CDATA[</font></td></tr>");
                                    else
                                      document.write("<tr><td colspan=\"2\" class=\"labelleft\" align=\"left\"> <font color=\"blue\">]]><xsl:value-of select="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@exceptionmessage"/><![CDATA[</font></td></tr>");
                                    ]]>
                                  </script>
                                </xsl:when>
                                <xsl:otherwise>
                                  <xsl:if test="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@exceptioncode = 'A'">
                                    <tr>
                                      <td colspan="2" class="labelleft"><font color="red">This product is only available for delivery from&nbsp;<xsl:value-of select="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@exceptionstartdate"/>&nbsp;to&nbsp;<xsl:value-of select="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@exceptionenddate"/></font></td>
                                    </tr>
                                  </xsl:if>
                                  <xsl:if test="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@exceptionCode = 'U'">
                                    <tr>
                                      <td colspan="2" class="labelleft"><font color="red">This product is not available for delivery from&nbsp;<xsl:value-of select="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@exceptionstartdate"/>&nbsp;to&nbsp;<xsl:value-of select="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@exceptionenddate"/></font></td>
                                    </tr>
                                  </xsl:if>
                                  <tr>
                                    <td colspan="2" class="labelleft"><font color="red"><xsl:value-of select="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@exceptionmessage"/></font></td>
                                  </tr>
                                </xsl:otherwise>
                              </xsl:choose>

                              <xsl:if test="PRODUCTRESULTS/EXTRAINFO/SUBTYPES/PRODUCT">
                                <tr>
                                  <td width="20%" class="label"><xsl:value-of select="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@productname"/>:</td>
                                  <td align="left">
                                    <select name="subcode_id"  id="subcode_id">
                                      <option value="">Please Select</option>
                                        <xsl:for-each select="PRODUCTRESULTS/EXTRAINFO/SUBTYPES/PRODUCT">
                                          <option value="{@productsubcodeid}"><xsl:value-of select="@subcodedescription"/> - $<xsl:value-of select="@subcodeprice"/></option>
                                        </xsl:for-each>
                                    </select>
                                  </td>
                                </tr>
                              </xsl:if>

                              <xsl:if test="$isFloristOrder">
                                <xsl:call-template name="floristDeliveryDates">
                                  <xsl:with-param name="requestedDeliveryDate" select="key('pageData', 'delivery_date')/value"/>
                                  <xsl:with-param name="requestedDeliveryDateRangeEnd" select="key('pageData', 'delivery_date_range_end')/value"/>
                                  <xsl:with-param name="deliveryDates" select="deliveryDates"/>
                                  <xsl:with-param name="displayGetMax" select="boolean(key('pageData', 'max_delivery_dates_retrieved')/value = 'n')"/>
                                  <xsl:with-param name="defaultDates" select="true()"/>
                                  <xsl:with-param name="containsErrors" select="$containsErrors"/>
                                  <xsl:with-param name="currentDate" select="key('pageData', 'currentDate')/value"/>
                                </xsl:call-template>
                              </xsl:if>

															<xsl:if test="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@exceptioncode = 'U'">
																<tr>
																	<td>&nbsp;</td>
																	<td class="labelleft">
																		<font color="red">
																			This product is not available for delivery from&nbsp;<xsl:value-of select="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@exceptionstartdate"/>&nbsp;to&nbsp;<xsl:value-of select="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@exceptionenddate"/>.
																		</font>
																	</td>
																</tr>
															</xsl:if>
                              
                              <xsl:if test="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@allowfreeshippingflag != 'Y'">
                              <tr>
																	<td >
                                  	<img src="images/noFSicon.jpg"/>
																	</td>                              
                              </tr>
                              </xsl:if>
                            </table>
                          </td>
                        </tr>

                        <xsl:if test="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@shipmethodflorist = 'Y'">
                          <tr>
                            <td align="center" bgcolor="#C1DEF3">
                              FTD <script><![CDATA[document.write("&#174;")]]></script> Florist
                            </td>
                          </tr>
                        </xsl:if>

                        <xsl:if test="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@shipmethodcarrier = 'Y'">
                          <tr>
                            <td align="center" bgcolor="#FF9933"><xsl:value-of select="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@carrier"/></td>
                          </tr>
                        </xsl:if>

<!--
&nbsp;ORIG_product_id&nbsp;=&nbsp;			<xsl:value-of select="$ORIG_product_id"				/>
&nbsp;ORIG_product_amount&nbsp;=&nbsp;	<xsl:value-of select="$ORIG_product_amount"		/>
<br/>
<br/>
&nbsp;NEW_product_amount&nbsp;=&nbsp;		<xsl:value-of select="$NEW_product_amount"	/>
<br/>
<br/>
&nbsp;standardDiscountPrice&nbsp;=&nbsp;<xsl:value-of select="$standardDiscountPrice"	/>
&nbsp;standardPrice&nbsp;=&nbsp;				<xsl:value-of select="$standardPrice"				 	/>
&nbsp;standardRewardValue&nbsp;=&nbsp;	<xsl:value-of select="$standardRewardValue"		/>
<br/>
&nbsp;deluxeDiscountPrice&nbsp;=&nbsp;	<xsl:value-of select="$deluxeDiscountPrice"		/>
&nbsp;deluxePrice&nbsp;=&nbsp;					<xsl:value-of select="$deluxePrice"						/>
&nbsp;deluxeRewardValue&nbsp;=&nbsp;		<xsl:value-of select="$deluxeRewardValue"			/>
<br/>
&nbsp;premiumDiscountPrice&nbsp;=&nbsp; <xsl:value-of select="$premiumDiscountPrice" 	/>
&nbsp;premiumPrice&nbsp;=&nbsp; 				<xsl:value-of select="$premiumPrice" 					/>
&nbsp;premiumRewardValue&nbsp;=&nbsp;		<xsl:value-of select="$premiumRewardValue"		/>
<br/>
-->


												<!-- Product pricing -->

												<xsl:if test="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@standardprice > 0">
													<tr>
														<td colspan="3" ><hr class="blueBanner"/><hr /></td>
													</tr>
													<tr>
														<td class="labelright">Price:&nbsp;</td>
														<td align="left">
															<input type="radio" id="price_standard" name="price" onchangeIgnore="true" onclick="checkVariablePrice(value);" value="A" checked="true">
															</input>
															<input type="hidden" name="standard_price" id="standard_price" value="{$standardPrice}"/>
															<xsl:choose>
																<xsl:when test="$standardDiscountPrice > 0 and $standardPrice != $standardDiscountPrice">
																	<span class="strikePrice">
																	$<xsl:value-of select="format-number(/root/PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@standardprice, '#,###,##0.00','notANumber')"/>
																	</span>&nbsp;
																	$<xsl:value-of select="format-number(/root/PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@standarddiscountprice, '#,###,##0.00','notANumber')"/>
																</xsl:when>
																<xsl:otherwise>
																	$<xsl:value-of select="format-number(/root/PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@standardprice, '#,###,##0.00','notANumber')"/>
																</xsl:otherwise>
															</xsl:choose>

															<!-- Show Price -->
															<xsl:choose>
																<xsl:when test="$standardLabel != ''">
																	&nbsp;&nbsp; - &nbsp;&nbsp;<xsl:value-of select="$standardLabel"/>
																</xsl:when>
																<xsl:otherwise>
																	&nbsp;&nbsp; - &nbsp;&nbsp;Shown
																</xsl:otherwise>
															</xsl:choose>
															<xsl:if test="$standardRewardValue > 0">
																 <xsl:if test="key('pageData', 'reward_type')/value ='D'">
																		($<xsl:value-of select="format-number(/root/PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@standardrewardvalue, '#,###,##0.00','notANumber')"/>&nbsp;Off)
																 </xsl:if>
																 <xsl:if test="key('pageData', 'reward_type')/value ='Charity'">
																		($<xsl:value-of select="format-number(/root/PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@standardrewardvalue, '#,###,##0.00','notANumber')"/>&nbsp;Charity)
																 </xsl:if>
																 <xsl:if test="key('pageData', 'reward_type')/value ='Cash Back'">
																		($<xsl:value-of select="format-number(/root/PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@standardrewardvalue, '#,###,##0.00','notANumber')"/>&nbsp;Cash Back)
																 </xsl:if>
																 <xsl:if test="key('pageData', 'reward_type')/value ='P'">
																		(<xsl:value-of select="format-number(/root/PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@standardrewardvalue, '#,###,##0.00','notANumber')"/>%)
																 </xsl:if>
																 <xsl:if test="key('pageData', 'reward_type')/value ='Miles'">
																		(<xsl:value-of select="format-number(/root/PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@standardrewardvalue, '#,###,##0.00','notANumber')"/>&nbsp;Miles)
																 </xsl:if>
																 <xsl:if test="key('pageData', 'reward_type')/value ='Points'">
																		(<xsl:value-of select="format-number(/root/PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@standardrewardvalue, '#,###,##0.00','notANumber')"/>&nbsp;Points)
																</xsl:if>
															</xsl:if>
														</td>
														<td align="left" class="Instruction" valign="top">
															<xsl:value-of select="PRODUCTRESULTS/EXTRAINFO/SCRIPTING/SCRIPT[@fieldname='PRICE']/@instructiontext"/>
														</td>
													</tr>
												</xsl:if>

												<!-- upsell -->
												<xsl:if test="key('pageData', 'upsell_flag')/value != 'Y'">
													<!-- Deluxe Price -->
													<xsl:if test="$deluxePrice > 0">
														<tr>
															<td class="labelright">&nbsp;</td>
															<td align="left">
																<input type="radio" id="price_deluxe" onchangeIgnore="true" onclick="checkVariablePrice(value);" name="price" value="B">
																	<xsl:if test="$containsErrors and $requestedSizeDescription = 'deluxe'">
																		<xsl:attribute name="CHECKED"/>
																	</xsl:if>
																</input>
																<input type="hidden" name="deluxe_price" id="deluxe_price" value="{$deluxePrice}"/>
																<xsl:choose>
																	<xsl:when test="$deluxeDiscountPrice > 0 and $deluxePrice != $deluxeDiscountPrice">
																		<span class="strikePrice">
																		$<xsl:value-of select="format-number(/root/PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@deluxeprice, '#,###,##0.00','notANumber')"/>
																		</span>&nbsp;
																		$<xsl:value-of select="format-number(/root/PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@deluxediscountprice, '#,###,##0.00','notANumber')"/>
																	</xsl:when>
																	<xsl:otherwise>
																		$<xsl:value-of select="format-number(/root/PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@deluxeprice, '#,###,##0.00','notANumber')"/>
																	</xsl:otherwise>
																</xsl:choose>
																<xsl:choose>
																	<xsl:when test="$deluxeLabel != ''">
																		&nbsp;&nbsp; - &nbsp;&nbsp;<xsl:value-of select="$deluxeLabel"/>
																	</xsl:when>
																	<xsl:otherwise>
																		&nbsp;&nbsp; - &nbsp;&nbsp;Deluxe
																	</xsl:otherwise>
																</xsl:choose>
																<xsl:if test="$deluxeRewardValue > 0">
																	<xsl:if test="key('pageData', 'reward_type')/value != 'TotSavings'">
																		(<xsl:value-of select="format-number(/root/PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@deluxerewardvalue, '#,###,##0.00','notANumber')"/>)
																	</xsl:if>
																</xsl:if>
															</td>
														</tr>
													</xsl:if>

													<!-- Premium Price -->
													<xsl:if test="$premiumPrice > 0">
														<tr>
															<td class="labelright">&nbsp;</td>
															<td align="left">
																<input type="radio" id="price_premium" onchangeIgnore="true" name="price" onclick="checkVariablePrice(value);" value="C">
																	<xsl:if test="$containsErrors and $requestedSizeDescription = 'premium'">
																		<xsl:attribute name="CHECKED"/>
																	</xsl:if>
																</input>
																<input type="hidden" name="premium_price" id="premium_price" value="{$premiumPrice}"/>
																<xsl:choose>
																	<xsl:when test="$premiumDiscountPrice > 0 and $premiumPrice != $premiumDiscountPrice">
																		<span class="strikePrice">
																		$<xsl:value-of select="format-number(/root/PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@premiumprice, '#,###,##0.00','notANumber')"/>
																		</span>&nbsp;
																		$<xsl:value-of select="format-number(/root/PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@premiumdiscountprice, '#,###,##0.00','notANumber')"/>
																	</xsl:when>
																	<xsl:otherwise>
																		$<xsl:value-of select="format-number(/root/PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@premiumprice, '#,###,##0.00','notANumber')"/>
																	</xsl:otherwise>
																</xsl:choose>
																<xsl:choose>
																	<xsl:when test="$premiumLabel != ''">
																		&nbsp;&nbsp; - &nbsp;&nbsp;<xsl:value-of select="$premiumLabel"/>
																	</xsl:when>
																	<xsl:otherwise>
																		&nbsp;&nbsp; - &nbsp;&nbsp;Premium
																	</xsl:otherwise>
																</xsl:choose>
																<xsl:if test="$premiumRewardValue > 0">
																	<xsl:if test="key('pageData', 'reward_type')/value != 'TotSavings'">
																		(<xsl:value-of select="format-number(/root/PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@premiumrewardvalue, '#,###,##0.00','notANumber')"/>)
																	</xsl:if>
																</xsl:if>
															</td>
														</tr>
													</xsl:if>
												</xsl:if>
												<!-- /upsell -->

												<!-- Variable pricing -->
												<tr>
													<td/>
													<td>
														<xsl:if test="$isFloristOrder and
																					($variablePriceFlag = 'y' or $variablePriceFlag = 'Y')">

															<input type="radio" id="price_variable" name="price" value="variable" onchangeIgnore="true"  onclick="checkVariablePrice(value);" >
																<xsl:if test="$containsErrors and $requestedSizeDescription = 'variable'">
																	<xsl:attribute name="CHECKED"/>
																</xsl:if>
																Variable Price
															</input>
															&nbsp;&nbsp;
															$
															<input id="variable_price" name="variable_price" type="text" size="10" onselect="resetRadio(value);"
																onkeypress="resetRadio(value);" onclick="resetRadio(value);">
																	<xsl:if test="$containsErrors and $requestedSizeDescription = 'variable'">
																		<xsl:attribute name="VALUE">
																			<xsl:value-of select="$requestedPrice"/>
																		</xsl:attribute>
																	</xsl:if>
															</input>
														</xsl:if>
													</td>
												</tr>

                        <!-- floral vs. vendor -->
                        <xsl:choose>

                          <!-- florist order-->
                          <xsl:when test="$isFloristOrder">

                            <!-- color selection -->
                            <xsl:if test="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@color1 !=''">
                              <tr>
                                <td colspan="3"><hr /></td>
                              </tr>
                              <tr>
                                <td class="labelright">1st Choice Color:&nbsp;</td>
																<td align="left">
																	<select name="color_first_choice" id="color_first_choice" >
																			<option value=""> -- Please Select --</option>
																			<option value="{PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@color1}">
																				<xsl:if test="
																						translate(PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@color1,$lower_case, $upper_case) =
																							translate(requestOrderDetailVO/OrderDetailVO/color_1_description,$lower_case, $upper_case) and
																						$containsErrors
																				">
																					<xsl:attribute name="selected"/></xsl:if>
																				<xsl:value-of select="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@color1"/>
																			</option>
																		<xsl:if test="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@color2 != ''">
																			<option value="{PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@color2}">
																				<xsl:if test="
																						translate(PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@color2,$lower_case, $upper_case) =
																							translate(requestOrderDetailVO/OrderDetailVO/color_1_description,$lower_case, $upper_case) and
																						$containsErrors
																				">
																					<xsl:attribute name="selected"/>
																				</xsl:if>
																				<xsl:value-of select="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@color2"/>
																			</option>
																		</xsl:if>
																		<xsl:if test="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@color3 != ''">
																			<option value="{PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@color3}">
																				<xsl:if test="
																						translate(PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@color3,$lower_case, $upper_case) =
																							translate(requestOrderDetailVO/OrderDetailVO/color_1_description,$lower_case, $upper_case) and
																						$containsErrors
																				">
																					<xsl:attribute name="selected"/>
																				</xsl:if>
																				<xsl:value-of select="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@color3"/>
																			</option>
																		</xsl:if>
																		<xsl:if test="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@color4 != ''">
																			<option value="{PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@color4}">
																				<xsl:if test="
																						translate(PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@color4,$lower_case, $upper_case) =
																							translate(requestOrderDetailVO/OrderDetailVO/color_1_description,$lower_case, $upper_case) and
																						$containsErrors
																					">
																					<xsl:attribute name="selected"/>
																				</xsl:if>
																				<xsl:value-of select="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@color4"/>
																			</option>
																		</xsl:if>
																		<xsl:if test="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@color5 != ''">
																			<option value="{PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@color5}">
																				<xsl:if test="
																						translate(PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@color5,$lower_case, $upper_case) =
																							translate(requestOrderDetailVO/OrderDetailVO/color_1_description,$lower_case, $upper_case) and
																						$containsErrors
																				">
																					<xsl:attribute name="selected"/>
																				</xsl:if>
																				<xsl:value-of select="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@color5"/>
																			</option>
																		</xsl:if>
																		<xsl:if test="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@color6 != ''">
																			<option value="{PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@color6}">
																				<xsl:if test="
																						translate(PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@color6,$lower_case, $upper_case) =
																							translate(requestOrderDetailVO/OrderDetailVO/color_1_description,$lower_case, $upper_case) and
																						$containsErrors
																				">
																					<xsl:attribute name="selected"/>
																				</xsl:if>
																				<xsl:value-of select="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@color6"/>
																			</option>
																		</xsl:if>
																		<xsl:if test="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@color7 != ''">
																			<option value="{PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@color7}">
																				<xsl:if test="
																						translate(PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@color7,$lower_case, $upper_case) =
																							translate(requestOrderDetailVO/OrderDetailVO/color_1_description,$lower_case, $upper_case) and
																						$containsErrors
																				">
																					<xsl:attribute name="selected"/>
																				</xsl:if>
																				<xsl:value-of select="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@color7"/>
																			</option>
																		</xsl:if>
																		<xsl:if test="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@color8 != ''">
																			<option value="{PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@color8}">
																				<xsl:if test="
																						translate(PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@color8,$lower_case, $upper_case) =
																							translate(requestOrderDetailVO/OrderDetailVO/color_1_description,$lower_case, $upper_case) and
																						$containsErrors
																				">
																					<xsl:attribute name="selected"/>
																				</xsl:if>
																				<xsl:value-of select="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@color8"/>
																			</option>
																		</xsl:if>
																	</select>
																	&nbsp;<span class="requiredFieldTxt">***</span>
																</td>
                                <td class="instruction">&nbsp;</td>
                              </tr>
                            </xsl:if>

                            <xsl:if test="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@color2 !=''">
                              <tr>
                                <td class="labelright">2nd Choice Color:&nbsp;</td>
																<td align="left">
																	<select name="color_second_choice" id="color_second_choice">
																		<option value=""> -- Please Select --</option>
																		<xsl:if test="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@color1 != ''">
																			<option value="{PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@color1}">
																				<xsl:if test="
																						translate(PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@color1,$lower_case, $upper_case) =
																							translate(requestOrderDetailVO/OrderDetailVO/color_2_description,$lower_case, $upper_case) and
																						$containsErrors
																				">
																					<xsl:attribute name="selected"/>
																				</xsl:if>
																				<xsl:value-of select="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@color1"/>
																			</option>
																		</xsl:if>
																		<xsl:if test="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@color2 != ''">
																			<option value="{PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@color2}">
																				<xsl:if test="
																						translate(PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@color2,$lower_case, $upper_case) =
																							translate(requestOrderDetailVO/OrderDetailVO/color_2_description,$lower_case, $upper_case) and
																						$containsErrors
																				">
																					<xsl:attribute name="selected"/>
																				</xsl:if>
																				<xsl:value-of select="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@color2"/>
																			</option>
																		</xsl:if>
																		<xsl:if test="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@color3 != ''">
																			<option value="{PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@color3}">
																				<xsl:if test="
																						translate(PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@color3,$lower_case, $upper_case) =
																							translate(requestOrderDetailVO/OrderDetailVO/color_2_description,$lower_case, $upper_case) and
																						$containsErrors
																				">
																					<xsl:attribute name="selected"/>
																				</xsl:if>
																				<xsl:value-of select="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@color3"/>
																			</option>
																		</xsl:if>
																		<xsl:if test="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@color4 != ''">
																			<option value="{PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@color4}">
																				<xsl:if test="
																						translate(PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@color4,$lower_case, $upper_case) =
																							translate(requestOrderDetailVO/OrderDetailVO/color_2_description,$lower_case, $upper_case) and
																						$containsErrors
																				">
																					<xsl:attribute name="selected"/>
																				</xsl:if>
																				<xsl:value-of select="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@color4"/>
																			</option>
																		</xsl:if>
																		<xsl:if test="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@color5 != ''">
																			<option value="{PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@color5}">
																				<xsl:if test="
																						translate(PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@color5,$lower_case, $upper_case) =
																							translate(requestOrderDetailVO/OrderDetailVO/color_2_description,$lower_case, $upper_case) and
																						$containsErrors
																				">
																					<xsl:attribute name="selected"/>
																				</xsl:if>
																				<xsl:value-of select="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@color5"/>
																			</option>
																		</xsl:if>
																		<xsl:if test="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@color6 != ''">
																			<option value="{PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@color6}">
																				<xsl:if test="
																						translate(PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@color6,$lower_case, $upper_case) =
																							translate(requestOrderDetailVO/OrderDetailVO/color_2_description,$lower_case, $upper_case) and
																						$containsErrors
																				">
																					<xsl:attribute name="selected"/>
																				</xsl:if>
																				<xsl:value-of select="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@color6"/>
																			</option>
																		</xsl:if>
																		<xsl:if test="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@color7 != ''">
																			<option value="{PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@color7}">
																				<xsl:if test="
																						translate(PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@color7,$lower_case, $upper_case) =
																							translate(requestOrderDetailVO/OrderDetailVO/color_2_description,$lower_case, $upper_case) and
																						$containsErrors
																				">
																					<xsl:attribute name="selected"/>
																				</xsl:if>
																				<xsl:value-of select="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@color7"/>
																			</option>
																		</xsl:if>
																		<xsl:if test="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@color8 != ''">
																			<option value="{PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@color8}">
																				<xsl:if test="
																						translate(PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@color8,$lower_case, $upper_case) =
																							translate(requestOrderDetailVO/OrderDetailVO/color_2_description,$lower_case, $upper_case) and
																						$containsErrors
																				">
																					<xsl:attribute name="selected"/>
																				</xsl:if>
																				<xsl:value-of select="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@color8"/>
																			</option>
																		</xsl:if>
																	</select>
																	&nbsp;<span class="requiredFieldTxt">***</span>
																</td>
                              </tr>
                              <tr>
                                <td class="labelright">&nbsp;</td>
                                <td align="left" class="Instruction" valign="top">
                                  <xsl:value-of select="PRODUCTRESULTS/EXTRAINFO/SCRIPTING/SCRIPT[@fieldname='COLOR']/@instructiontext"/>
                                </td>
                              </tr>
                              <tr>
                                <td colspan="3">&nbsp;</td>
                              </tr>
                            </xsl:if>

                            <!-- Product second choice -->
                            <xsl:if test="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@secondchoicecode!='0' and PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@shipmethodflorist='Y'">
                              <tr>
                                <td colspan="3"><hr/></td>
                              </tr>
                              <tr>
                                <td><br/></td>
                              </tr>
                              <tr>
                              <!--
                                The Allow Substitutions Check box will only display
                                if there are no color 1 and 2 choices or if the
                                product is Domestic
                              -->
                              <xsl:if test="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@deliverytype = 'D' and PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@color1 = '' and PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@color2 =''">
                                <td class="labelright">Allow substitution:</td>
                                <td align="left">
																	<input tabindex="-1" type="checkbox" name="substitution_indicator" id="substitution_indicator" onclick="javascript:updateSubstitutionIndicator();" >
																		<xsl:choose>
																			<xsl:when test="$containsErrors">
																				<xsl:if test="$requestedSubstitutionIndicator = 'Y' or $requestedSubstitutionIndicator = 'y'">
																					<xsl:attribute name="CHECKED"/>
																				</xsl:if>

																				<xsl:attribute name="VALUE">
																					<xsl:value-of select="$requestedSubstitutionIndicator"/>
																				</xsl:attribute>
																			</xsl:when>

																			<xsl:otherwise>
																				<xsl:attribute name="CHECKED"/>
																				<xsl:attribute name="VALUE">
																					<xsl:value-of select="$YES"/>
																				</xsl:attribute>
																			</xsl:otherwise>
																		</xsl:choose>
																	</input>
																</td>
                              </xsl:if>
                              </tr>
                              <tr>
                                <td><br/></td>
                              </tr>
                            </xsl:if>

                          </xsl:when> <!-- end of <xsl:when test="$isFloristOrder"> -->


                          <!-- vendor -->
                          <xsl:otherwise>
                            <tr>
                              <td colspan="3">
                                <xsl:call-template name="vendorDeliveryDates">
																	<xsl:with-param name="defaultDates" select="true()"/>
																	<xsl:with-param name="deliveryDates" select="deliveryDates"/>
																	<xsl:with-param name="displayGetMax" select="boolean(key('pageData', 'max_delivery_dates_retrieved')/value = 'n')"/>
																	<xsl:with-param name="floristServiceCharges" select="$floristServiceCharges"/>
																	<xsl:with-param name="origShipMethod" select="key('pageData','ship_method')/value"/>
																	<xsl:with-param name="requestedDeliveryDate" select="key('pageData', 'delivery_date')/value"/>
																	<xsl:with-param name="requestedDeliveryDateRangeEnd" select="key('pageData', 'delivery_date_range_end')/value"/>
																	<xsl:with-param name="shipCosts" select="PD_SHIP_COSTS"/>
																	<xsl:with-param name="productType" select="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@producttype"/>
                                  <xsl:with-param name="containsErrors" select="$containsErrors"/>
																	<xsl:with-param name="fuelSurcharge" select="PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@fuel_surcharge"/>
                                </xsl:call-template>
                              </td>
                            </tr>
                          </xsl:otherwise>
                        </xsl:choose>

                        <!-- Handle addons -->
                        <xsl:if test="(count(PRODUCTRESULTS/EXTRAINFO/add_ons/vase/AddOnVO) > 0) or
                                       (count(PRODUCTRESULTS/EXTRAINFO/add_ons/add_on/AddOnVO) > 0) or
                                       (count(PRODUCTRESULTS/EXTRAINFO/add_ons/banner/AddOnVO) > 0) or
                                       (count(PRODUCTRESULTS/EXTRAINFO/add_ons/card/AddOnVO) > 0)">
                          <tr>
                            <td colspan="3"><hr/></td>
                          </tr>
                        </xsl:if>

                          <!-- vase addon -->
                          <xsl:if test="PRODUCTRESULTS/EXTRAINFO/add_ons/vase/AddOnVO">
                            <tr>
                              <td class="labelright">Vase:</td>
                              <td colspan="2">
                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                  <tr>
                                    <td width="50%">
                                      <select name="addonNew_optVase" id="addonNew_optVase">
                                        <xsl:for-each select="PRODUCTRESULTS/EXTRAINFO/add_ons/vase/AddOnVO">
                                          <xsl:variable name="curLoopAddonId" select="addOnId"/>
                                          <xsl:variable name="curAaPrice" select="format-number(addOnPrice, '#,###,##0.00')"/> 
                                          <option value="{addOnId}---{$curAaPrice}">
                                            <xsl:if test="$containsErrors and /root/requestOrderDetailVO/OrderDetailVO/AddOnVO[addOnCode = $curLoopAddonId]/addOnQuantity != 0"><xsl:attribute name="SELECTED"/></xsl:if>
                                            <script>document.write(convertHtmlEntity('<xsl:value-of select="concat(addOnDescription, ' - $', $curAaPrice)"/>'))</script>
                                          </option>
                                        </xsl:for-each>
                                      </select>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                          </xsl:if>
                      
                          <!-- normal addons -->
                          <xsl:if test="PRODUCTRESULTS/EXTRAINFO/add_ons/add_on/AddOnVO">
                             <xsl:for-each select="PRODUCTRESULTS/EXTRAINFO/add_ons/add_on/AddOnVO">
                                <xsl:variable name="curLoopAddonId" select="addOnId"/>
                                <xsl:variable name="curLoopMaxQuantity" select="maxQuantity"/>
                                <xsl:variable name="orderQuantity" select="/root/requestOrderDetailVO/OrderDetailVO/AddOnVO[addOnCode = $curLoopAddonId]/addOnQuantity"/>
                                <xsl:variable name="curPrice" select="format-number(addOnPrice, '#,###,##0.00')"/>
                                <xsl:variable name="curChecked" select="boolean(/root/requestOrderDetailVO/OrderDetailVO/AddOnVO[addOnCode = $curLoopAddonId]) or boolean($curLoopAddonId = 'FC')"/>
                                <tr>
                                  <td class="labelright"><xsl:value-of select="addOnDescription" disable-output-escaping="yes"/>:</td>
                                  <td colspan="2">
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                      <tr>
                                        <td width="50%">
                                          <input type="checkbox" name="addonNew_{addOnId}" id="addonNew_{addOnId}" value="{addOnId}" onclick="javascript:updateAddOnQuantity(this, 'addonNew_{addOnId}_quantity');">
                                            <xsl:if test="$curChecked"><xsl:attribute name="CHECKED"/></xsl:if>
                                          </input>
                                          $<xsl:value-of select="format-number(addOnPrice, '#,###,##0.00')"/>
                                          <input type="hidden" name="addonNew_{addOnId}_price" id="addonNew_{addOnId}_price" value="{$curPrice}"/>
                                        </td>
                                        <td width="20%" class="Labelright">Quantity:&nbsp;</td>
                                        <td width="30%">
                                          <select name="addonNew_{addOnId}_quantity" id="addonNew_{addOnId}_quantity" class="tblText">
                                            <xsl:if test="$curChecked = false"><xsl:attribute name="disabled"/></xsl:if>
                                            <xsl:call-template name="recursiveOptionList">
                                              <xsl:with-param name="start"    select="1"/>
                                              <xsl:with-param name="end"      select="$curLoopMaxQuantity"/>
                                              <xsl:with-param name="selected" select="$orderQuantity"/>
                                            </xsl:call-template>
                                          </select>
                                        </td>
                                      </tr>
                                    </table>
                                  </td>
                                </tr>
                             </xsl:for-each>
                          </xsl:if>
                      
                          <!-- banner addon -->
                          <xsl:if test="PRODUCTRESULTS/EXTRAINFO/add_ons/banner/AddOnVO">
                             <xsl:for-each select="PRODUCTRESULTS/EXTRAINFO/add_ons/banner/AddOnVO">
                                <xsl:variable name="curLoopAddonId" select="addOnId"/>
                                <xsl:variable name="curPrice" select="format-number(addOnPrice, '#,###,##0.00')"/> 
                                <tr>
                                  <td class="labelright"><xsl:value-of select="addOnDescription" disable-output-escaping="yes"/>:</td>
                                  <td colspan="2">
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                      <tr>
                                        <td width="50%">
                                          <input type="checkbox" name="addonNew_{addOnId}" id="addonNew_{addOnId}" value="{addOnId}">
                                            <xsl:if test="boolean(/root/requestOrderDetailVO/OrderDetailVO/AddOnVO[addOnCode = $curLoopAddonId])"><xsl:attribute name="CHECKED"/></xsl:if>
                                          </input>
                                          <input type="hidden" name="addonNew_{addOnId}_quantity" id="addonNew_{addOnId}_quantity" value="1"/>
                                          $<xsl:value-of select="format-number(addOnPrice, '#,###,##0.00')"/>
                                          <input type="hidden" name="addonNew_{addOnId}_price" id="addonNew_{addOnId}_price" value="{$curPrice}"/>
                                        </td>
                                        <td colspan="2">&nbsp;</td>
                                      </tr>
                                    </table>
                                  </td>
                                </tr>
                             </xsl:for-each>
                          </xsl:if>

                          <!-- card addon -->
                          <xsl:if test="PRODUCTRESULTS/EXTRAINFO/add_ons/card/AddOnVO">
                            <tr>
                              <td class="labelright">Card:</td>
                              <td colspan="2">
                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                  <tr>
                                    <td width="50%">
                                      <select name="addonNew_optCard" id="addonNew_optCard">
                                        <option value="noCard---0.00">No Card</option>
                                        <xsl:for-each select="PRODUCTRESULTS/EXTRAINFO/add_ons/card/AddOnVO">
                                          <xsl:variable name="curLoopAddonId" select="addOnId"/>
                                          <xsl:variable name="curAaPrice" select="format-number(addOnPrice, '#,###,##0.00')"/> 
                                          <option value="{addOnId}---{$curAaPrice}">
                                            <xsl:if test="$containsErrors and /root/requestOrderDetailVO/OrderDetailVO/AddOnVO[addOnCode = $curLoopAddonId]/addOnQuantity != 0"><xsl:attribute name="SELECTED"/></xsl:if>
                                            <script>document.write(convertHtmlEntity('<xsl:value-of select="concat(addOnDescription, ' - $', $curAaPrice)"/>'))</script>
                                          </option>
                                        </xsl:for-each>
                                      </select>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            <td>
                                <a id="greetingCardsLink" href="#" onclick="javascript:openGreetingCardPopup();">View and Select cards</a>
                            </td>
                                <td class="instruction">&nbsp;</td>
                            </tr>
                          </xsl:if>

                      </table>
                    </xsl:when>

                    <!-- No matching products -->
                    <xsl:otherwise>
                      <xsl:if test="count(PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT) = 0">
                        <table align="center" width="75%" height="25%" border="0" cellspacing="2" cellpadding="2">
                          <tr>
                            <td align="center" class="waitMessage">
                              No products were found matching your criteria.
                            </td>
                          </tr>
                          <tr>
                            <td valign="top" align="right">
                              <button type="button" tabindex="-1" class="BigBlueButton" style="width:160px;" onclick="javascript:doUpdateProductCategoryAction();">Update Product Category</button>
                            </td>
                          </tr>
                        </table>
                      </xsl:if>


                      <!-- No matching product found and the status is set to U (unavailable) and its not because of agriculture restrictions-->
                    <xsl:if test="(key('pageData', 'displayProductUnavailable')/value = 'Y' and $agricultureRestrictionFound = 'N') or key('pageData', 'displayPersonalizedProduct')/value = 'Y'">
                        <table align="center" width="75%" height="25%" border="0" cellspacing="2" cellpadding="2">
                          <tr>
                            <td align="center" class="waitMessage">
                              The product is currently unavailable. Please select another product.
                            </td>
                          </tr>
                          <tr>
                            <td valign="top" align="right">
                              <button type="button" tabindex="-1" class="BigBlueButton" style="width:160px;" onclick="javascript:doUpdateProductCategoryAction();">Update Product Category</button>
                            </td>
                          </tr>
                        </table>
                      </xsl:if>
                    </xsl:otherwise>

                  </xsl:choose>

                </div>
                <!-- /scrolling div -->

                <!-- Used to display a message and a button back to Load Product Category -->
                <div id="messageDiv" style="display:none">
                  <table align="center" width="75%" height="300px" border="0" cellspacing="2" cellpadding="2">
                    <tr>
                      <td id="messageCell" align="center" class="waitMessage"/>
                    </tr>
                    <tr>
                      <td valign="top" align="right">
                        <button type="button" tabindex="-1" class="BigBlueButton" style="width:160px;" onclick="javascript:doUpdateProductCategoryAction();">Update Product Category</button>
                      </td>
                    </tr>
                  </table>
                </div>

              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
    <!-- /main table -->

    <!-- bottom buttons -->
 		<!-- display if 1) product is found and 2a) its available or 2b)it's unavailable due to agriculture restrictions-->
		 <xsl:if test="count(PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT)> 0 and
		 			(	key('pageData', 'displayProductUnavailable')/value = 'N' 			or
		 				(key('pageData', 'displayProductUnavailable')/value = 'Y' and $agricultureRestrictionFound = 'Y')
		 			) and
                                        key('pageData', 'displayPersonalizedProduct')/value = 'N'">
      <div style="margin-left:7px;margin-top:0px;width:99%;align:center;">
        <div style="text-align:right">
          <span style="padding-right:2em;"><button type="button" tabindex="-1" id="updCompleteButton" accesskey="p" style="width:110px;" class="BigBlueButton" onclick="javascript:doCompleteUpdateAction();">Com(p)lete Update</button></span>
          <button type="button" tabindex="-1" id="updCancelButton" accesskey="C" class="BigBlueButton" style="width:110px;" onclick="javascript:doCancelUpdateAction();">(C)ancel Update</button>
        </div>
      </div>
    </xsl:if>
    <!-- /bottom buttons -->


    <table width="98%" border="0" align="center" cellpadding="2" cellspacing="2">
      <tr>
        <td width="20%">&nbsp;</td>
        <td width="60%" align="center">
          <xsl:choose>
            <xsl:when test="key('pageData', 'displayProductUnavailable')/value = 'N' or
                                                                        (key('pageData', 'displayProductUnavailable')/value = 'Y' and $agricultureRestrictionFound ='Y')">
              <xsl:choose>
                <!-- JCP Food Order -->
                <xsl:when test="/root/pageHeader/headerDetail[@name='jcp_flag']/@value = 'JP' and PRODUCTRESULTS/PRODUCTLIST/PRODUCTS/PRODUCT/@jcpCategory != 'A'">
                  <input type="button" name="backButton" id="backButton" value="Back" onclick="javascript:doLastPageAction();"/>
                </xsl:when>

                <xsl:otherwise>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:when>
            <xsl:otherwise>
            </xsl:otherwise>
          </xsl:choose>
          &nbsp;
        </td>
        <td width="20%" align="right">
        </td>
      </tr>
    </table>

  <!-- Large image popup -->
  <div id="showLargeImage" style="display:none; position:absolute; border:1px solid Black; border-color:Black; background:#FFF8DC;">
    <center>
    <br/><br/>
    <script language="JavaScript"><![CDATA[
      document.write("<img ")
      document.write("ONERROR=\"var imag = this.src.substring(this.src.length-9, this.src.length); ")
      document.write("if (imag!='npi_2.jpg'){ ")
      document.write("this.src='" + product_images + "npi_2.jpg'}\" ")
      document.write("src='" + product_images + novator_id + ".jpg'/> ");
      ]]>
    </script>
    <br/><br/>
    <a href="#" onclick="javascript:closeLargeImage();">close</a>
    </center>
  </div>

  <!--Begin no codified florist but has common carrier DIV -->
  <div id="noCodifiedFloristHasCommonCarrier" style="visibility: hidden; position: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background:#F0F8FF">
    <table border="3" class="mainTable" cellpadding="1" cellspacing="1">
      <tr>
        <td>
          <table width="100%" class="innerTable" border="0" cellpadding="2" cellspacing="2">
            <tr>
              <td align="center">
                I'm sorry but the product you have selected is not currently available in your delivery area.
                <br/><br/>
                This item can be delivered via FedEx as early as DATE. Would you like to select a shipping option or shop for another item?
              </td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td align="center">
              <!--  <img src="../images/button_ok.gif" onclick="javascript:noCodifiedFloristHasCommonCarrier();"/>-->
              <button type="button" class="BlueButton" onclick="javascript:noCodifiedFloristHasCommonCarrier();">&nbsp;Ok&nbsp;</button>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </div>

  <!--Begin no florist but has common carrier DIV -->
  <div id="noFloristHasCommonCarrier" style="visibility: hidden; position: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background:#F0F8FF;">
    <table border="3" class="mainTable" cellpadding="1" cellspacing="1">
      <tr>
        <td>
          <table width="100%" border="0" class="innerTable" cellpadding="2" cellspacing="2">
            <tr>
              <td align="center">
                I'm sorry but the zip code you have provided is not currently serviced by an FTD florist for floral deliveries.
                <br/><br/>
                This item can be delivered via FedEx as early as DATE. Would you like to select a shipping option or shop for another item?
              </td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td align="center">
                <button type="button" class="BlueButton" onclick="javascript:noFloristHasCommonCarrier();">&nbsp;Ok&nbsp;</button>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </div>

  <!--Begin Product Unavailable DIV -->
  <div id="productUnavailable" style="VISIBILITY:hidden; POSITION:absolute; TOP:225px; border:1px solid Black; border-color:Black; background:#F0F8FF;">
    <table border="0" class="mainTable" cellpadding="1" cellspacing="1">
      <tr>
        <td>
          <table width="100%" class="innerTable" border="0" cellpadding="2" cellspacing="2">
            <xsl:choose>
              <xsl:when test="key('pageData', 'displayProductUnavailable')/value = 'N' or
            								(key('pageData', 'displayProductUnavailable')/value = 'Y' and $agricultureRestrictionFound ='Y')">
                <tr>
                  <td align="center">
                    I'm sorry but the product you have selected is not currently available in your delivery area.
                    <br/><br/>
                    Would you like to shop for another item?
                  </td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td align="center">
                    <button type="button" class="BlueButton" onclick="javascript:productUnavailable()">&nbsp;Ok&nbsp;</button>
                  </td>
                </tr>
              </xsl:when>
              <xsl:otherwise>
                <tr>
                  <td align="center">
                    Product is currently unavailable.  Please select another product.
                    <br/>
                  </td>
                </tr>
                <tr>
                  <td>&nbsp;</td> <td> <br/> <br/></td>
                </tr>
                <tr>
                  <td align="center">
                  <button type="button" class="BlueButton" onclick="javascript:productUnavailable()">&nbsp;Ok&nbsp;</button>
                  </td>
                </tr>
              </xsl:otherwise>
            </xsl:choose>
          </table>
        </td>
      </tr>
    </table>
  </div>


  <!--Begin Codified Special DIV -->
  <div id="codifiedSpecial" style="visibility: hidden; position: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background:#F0F8FF">
    <table border="3" class="mainTable" cellpadding="1" cellspacing="1">
      <tr>
        <td>
          <table width="100%" class="innerTable" border="0" cellpadding="2" cellspacing="2">
            <tr>
              <td align="center">
                We're sorry the product you've selected is currently not available in the
                zip code you entered.  However, the majority of our flowers can be delivered
                to this zip code.
                <br/>
                If this is an incorrect zip code, please re-enter the zip code
                below and "Click to Accept".  Otherwise, please click the "Back" button to
                return to shopping.
              </td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td align="center">
               <!-- <img src="../images/button_ok.gif" onclick="javascript:codifiedSpecial()"/>-->
               <button type="button" class="BlueButton" onclick="javascript:codifiedSpecial()">&nbsp;Ok&nbsp;</button>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </div>

  <!--Begin JCP Popup DIV -->
  <div id="JCPPop" style="visibility: hidden; position: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
    <table border="3" bordercolor="maroon" cellpadding="1" cellspacing="1">
      <tr>
        <td>
          <table width="100%" border="0" cellpadding="2" cellspacing="2">
            <tr>
              <td align="center">
                At this time, JC Penney accepts orders going to or coming from the 50 United States
                only.  We can process your Order using any major credit card through FTD.com.
                <br/><br/>
                Would you like to proceed?
              </td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td align="center">
                <img src="../images/button_no.gif" onclick="javascript:JCPPopChoice('N')"/>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <img src="../images/button_yes.gif" onclick="javascript:JCPPopChoice('Y')"/>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </div>
  </form>

  <!-- Footer template -->
  <xsl:call-template name="footer"/>

  <!-- end main content div -->
  </div>


  <!-- Processing message div -->
  <div id="waitDiv" style="display:none">
    <table id="waitTable" width="98%" border="3" height="300px" align="center" cellpadding="30" cellspacing="1" bordercolor="#006699">
      <tr>
        <td width="100%">
          <table cellspacing="0" cellpadding="0" style="width:100%;height:300px">
            <tr>
              <td id="waitMessage" align="right" width="50%" class="waitMessage"></td>
              <td id="waitTD" width="50%" class="waitMessage"></td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
    <xsl:call-template name="footer"/>
  </div>

</body>
</html>
</xsl:template>

<!-- Internal template to help display select options for addons.  Needed because xsl is sooo lame -->
<xsl:template name="recursiveOptionList">
      <xsl:param name="start"    select="1"/>
      <xsl:param name="end"      select="1"/>
      <xsl:param name="selected" select="1"/>
      <xsl:if test="$end >= $start">
        <option value="{$start}">
        <xsl:if test="($start = $selected) or (($selected >= $end) and ($start = $end))"><xsl:attribute name="SELECTED"/></xsl:if>
        <xsl:value-of select="$start"/></option>
        <xsl:call-template name="recursiveOptionList">
          <xsl:with-param name="start"    select="$start + 1"/>
          <xsl:with-param name="end"      select="$end"/>
          <xsl:with-param name="selected" select="$selected"/>
        </xsl:call-template>
      </xsl:if>
</xsl:template>

</xsl:stylesheet>
