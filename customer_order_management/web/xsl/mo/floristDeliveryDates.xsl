<!DOCTYPE ACDemo[
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html" indent="yes"/>
<xsl:template name="floristDeliveryDates" >
  <xsl:param name="requestedDeliveryDate"/>
  <xsl:param name="requestedDeliveryDateRangeEnd"/>
  <xsl:param name="deliveryDates"/>
  <xsl:param name="displayGetMax"/>
  <xsl:param name="defaultDates"/>
  <xsl:param name="containsErrors"/>
  <xsl:param name="currentDate"/>
  
  <script type="text/javascript" language="javascript">
      
  <![CDATA[
  /*
   * The prepareDeliveryDate function is dynamic because delivery date submission preperation
   * is different for florist and vendor items.  This version of prepareDeliveryDate checks to
   * see if a range was selected by comparing start and end dates for equality, if they are
   * different, a range is assumed.
   */
  function prepareDeliveryDate(formName)
  {
    var deliveryDate = document.getElementById("delivery_date");
    var deliveryDateRangeEnd = document.getElementById("delivery_date_range_end");
    var dd = document.getElementById("page_delivery_date");
    var option = dd[dd.selectedIndex];
    var startDate = option.value;
       
    var endDate = option.endDate;
    var outEndDate = ( endDate != startDate ) ?  endDate : "";

		var productType = document.getElementById("product_type");
		if (productType != null && (productType.value == 'SDG' || productType.value == 'SDFC'))
		{
			document.getElementById("ship_method").value = 'SD';
		}

    deliveryDate.value = startDate;
    deliveryDateRangeEnd.value = outEndDate;
  }



	function validateShipMethodAndDeliveryDate()
	{
		var dd = document.getElementById("delivery_date_select");

		var field = document.getElementById('delivery_date_select');
    field.style.backgroundColor='white';
		if (dd.selectedIndex == 0)
		{
      field.focus();
      field.style.backgroundColor='pink';
			return false;
		}
		else
			return true;
	}




  ]]>
  </script>
  <tr>
     <td class="Label" align="right" width="25%">Delivery Date:</td>
     <td style="padding-left:.5em;">


<!--
reqDelDate&nbsp;=&nbsp;<xsl:value-of select="$requestedDeliveryDate"/>
reqDelDateRangeEnd&nbsp;=&nbsp;<xsl:value-of select="$requestedDeliveryDateRangeEnd"/>
checkIfDateFound&nbsp;=&nbsp;<xsl:value-of select="$checkIfDateFound"/>
defaultDates&nbsp;=&nbsp;<xsl:value-of select="$defaultDates"/>
-->

        <select name="page_delivery_date" id="delivery_date_select">

					<option value="" endDate="">
						<xsl:if test="$containsErrors = false">
							<xsl:attribute name="selected"/>
						</xsl:if>
					</option>

          <xsl:for-each select="$deliveryDates/deliveryDate">
						<option value="{startDate}" endDate="{endDate}">
							<xsl:if test="$defaultDates">
								<xsl:if test="$containsErrors and
															startDate = $requestedDeliveryDate and
														  ($requestedDeliveryDateRangeEnd = '' or endDate = $requestedDeliveryDateRangeEnd)">
									<xsl:attribute name="selected"/>
								</xsl:if>
							</xsl:if>
							<xsl:choose>
        						<xsl:when test="startDate = $currentDate and startDate = endDate">
    								Today <xsl:value-of select="startDate"/>
    							</xsl:when>
    							<xsl:otherwise>
        							<xsl:value-of select="displayDate"/>
    							</xsl:otherwise>
    						</xsl:choose>
						</option>
          </xsl:for-each>
        </select>
        <span style="color:#FF0000">***</span>
        <xsl:if test="boolean($displayGetMax)">
          <span style="padding-left:4em;">&nbsp;<button type="button" id="getMaxButton" class="bluebutton" onclick="javascript:doGetMaxDeliveryDateAction();">Get Max</button></span>
        </xsl:if>
     </td>
  </tr>

</xsl:template>
</xsl:stylesheet>