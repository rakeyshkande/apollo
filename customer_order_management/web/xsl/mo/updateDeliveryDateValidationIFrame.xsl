<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="../securityanddata.xsl"/>
<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>

<xsl:key name="pageData" match="/root/pageData/data" use="name"/>
<xsl:key name="security" match="/root/security/data" use="name"/>

<xsl:output method="html" indent="yes"/>
<xsl:template match="/root">
<html>
<head>
<script type="text/javascript" language="javascript">
var statusValue = '<xsl:value-of select="key('pageData','message_display')/value"/>';
var success = '<xsl:value-of select="key('pageData','success')/value"/>';
<!--
  forward_action-
    users can pass in a value for this in order to determine
    which method needs to be processed after this xsl executes
-->
<![CDATA[
	function init() {
    if ( statusValue == '' && success.toUpperCase() == 'Y' )
    {
      parent.updateDeliveryDateValidated();
    }
    else
    {
		  var modalArguments = new Object();
		  modalArguments.modalText = statusValue;
    	window.showModalDialog("alert.html", modalArguments, 'dialogWidth:300px; dialogHeight:200px; center:yes; status=no; help=no; resizable:no; scroll:yes');
    }
	}
]]>
</script>
</head>
<body onload="javascript:init();">
    <xsl:call-template name="securityanddata"/>
    <xsl:call-template name="decisionResultData"/>
</body>
</html>
</xsl:template>
</xsl:stylesheet>