<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="../securityanddata.xsl"/>
<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>

<xsl:key name="pageData" match="/root/pageData/data" use="name"/>
<xsl:key name="security" match="/root/security/data" use="name"/>

<xsl:output method="html" indent="yes"/>
<xsl:template match="/root">
<html>
<head>
<script type="text/javascript" src="js/util.js"/>

<script type="text/javascript" language="javascript">
var statusValue = '<xsl:value-of select="key('pageData','message_display')/value"/>';
var success = '<xsl:value-of select="key('pageData','success')/value"/>';
var orderDetailId = '<xsl:value-of select="key('pageData','orderDetailId')/value"/>';
var zipcode = '<xsl:value-of select="key('pageData','zipcode')/value"/>';
var deliveryDate = '<xsl:value-of select="key('pageData','deliveryDate')/value"/>';
var phoenixProductId = '<xsl:value-of select="key('pageData','phoenixProductId')/value"/>';
var shipMethod = '<xsl:value-of select="key('pageData','shipMethod')/value"/>';
var shipDate = '<xsl:value-of select="key('pageData','shipDate')/value"/>';
var isOrigNewProdSame = '<xsl:value-of select="key('pageData','isOrigNewProdsame')/value"/>';
var externalOrderNumber = '<xsl:value-of select="key('pageData','externalOrderNumber')/value"/>'; 
var addons = '<xsl:value-of select="key('pageData','addons')/value"/>';
var sourceCode = '<xsl:value-of select="key('pageData','sourceCode')/value"/>';
var companyId = '<xsl:value-of select="key('pageData','companyId')/value"/>';
var originId = '<xsl:value-of select="key('pageData','originId')/value"/>';
<!--
  forward_action-
    users can pass in a value for this in order to determine
    which method needs to be processed after this xsl executes
-->
<![CDATA[
	function init() 
	{		
    	if ( statusValue == '' && success.toUpperCase() == 'Y' )
   		{
   			parent.doPhoenixOrder(orderDetailId,phoenixProductId,deliveryDate,zipcode,shipMethod,shipDate,isOrigNewProdSame,externalOrderNumber,addons,sourceCode,companyId,originId);		
    	 }
    	 else
    	 {    	 
    	 	var modalArguments = new Object();
			modalArguments.modalText = statusValue;
    		window.showModalDialog("alert.html", modalArguments, 'dialogWidth:300px; dialogHeight:200px; center:yes; status=no; help=no; resizable:no; scroll:yes');
    	 }
  
	}
]]>
</script>
</head>
<body onload="javascript:init();">
    <xsl:call-template name="securityanddata"/>
    <xsl:call-template name="decisionResultData"/>
</body>
</html>
</xsl:template>
</xsl:stylesheet>