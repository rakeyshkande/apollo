<!DOCTYPE ACDemo[
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" indent="yes"/>
<xsl:template name="header" >

<xsl:param name="showBackButton"/>
<xsl:param name="showSearchBox"/>
<xsl:param name="searchLabel"/>
<xsl:param name="showCSRIDs"/>
<xsl:param name="cservNumber"/>
<xsl:param name="showPrinter"/>
<xsl:param name="dnisNumber"/>
<xsl:param name="headerName"/>
<xsl:param name="brandName"/>
<xsl:param name="indicator"/>
<xsl:param name="showTime"/>
<xsl:param name="backButtonLabel"/>
<xsl:param name="overrideRTQ"/>
<xsl:param name="cdispEnabledFlag"/>
<xsl:param name="showRefreshButton"/>
<xsl:param name="refreshButtonLabel"/>


<xsl:variable name="source-html" select="document('/decisionresult/includes/decisionResult.html')"></xsl:variable>  
<xsl:copy-of select="$source-html/node()"/>

  <!--
    If you plan to use the search box on your page you will need
    to define a doEntryAction() function on your page in order to
    catch and user actions. Also not that if you need to disable the
    onkeypress event you can use ->
    document.getElementById('numberEntry').onkeypress == '';
  -->
    <script type="text/javascript">
    <![CDATA[
    /* This function returns the user to the queue they came from */
    function doQueueReturnAction()
    {
      var url = "/queue/QueueRequest.do?queue_type=" + document.getElementById("rtq_queue_type").value + 
                "&queue_ind=" + document.getElementById("rtq_queue_ind").value + 
                "&attached=" + document.getElementById("rtq_attached").value + 
                "&tagged=" + document.getElementById("rtq_tagged").value + 
                "&user=" + document.getElementById("rtq_user").value + 
                "&group=" + document.getElementById("rtq_group").value + 
                "&max_records=" + document.getElementById("rtq_max_records").value + 
                "&position=" + document.getElementById("rtq_position").value + 
                "&current_sort_direction=" + document.getElementById("rtq_current_sort_direction").value + 
                "&sort_on_return=" + document.getElementById("rtq_sort_on_return").value + 
                "&sort_column=" + document.getElementById("rtq_sort_column").value  +
                "&received_date_filter=" + document.getElementById("rtq_received_date").value  +
                "&delivery_date_filter=" + document.getElementById("rtq_delivery_date").value  +
                "&tag_date_filter=" + document.getElementById("rtq_tag_date").value  +
                "&lasttouched_filter=" + document.getElementById("rtq_last_touched").value  +
                "&type_filter=" + document.getElementById("rtq_type").value  +
                "&sys_filter=" + document.getElementById("rtq_sys").value  +
                "&timezone_filter=" + document.getElementById("rtq_timezone").value  +
                "&sdg_filter=" + document.getElementById("rtq_sdg").value  +
                "&disposition_filter=" + document.getElementById("rtq_disposition").value  +
                "&priority_filter=" + document.getElementById("rtq_priority").value  +
                "&new_item_filter=" + document.getElementById("rtq_new_item").value  +
                "&occasion_filter=" + document.getElementById("rtq_occasion").value  +
                "&location_filter=" + document.getElementById("rtq_location").value  +
                "&member_number_filter=" + document.getElementById("rtq_member_num").value  +
                "&zip_code_filter=" + document.getElementById("rtq_zip").value  +
                "&tagby_filter=" + document.getElementById("rtq_tag_by").value + 
                "&filter=" + document.getElementById("rtq_filter").value;


      performAction(url);
    }
    ]]>
    </script>
     <script type="text/javascript" src="js/clock.js"/>
 
 
     <!-- includes for Decision Result Link -->
     <!-- Start -->
     <link rel="stylesheet" type="text/css" href="/decisionresult/css/decisionResult.css"/>
    <!--  <link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/> -->
     <link rel="stylesheet" type="text/css" href="/decisionresult/css/jquery-1.8.css"/>
 
     <script type="text/javascript" src="/decisionresult/js/jquery-1.4.4.js"></script>
     <script type="text/javascript" src="/decisionresult/js/jquery.blockUI.js"></script>
     <script type="text/javascript" src="/decisionresult/js/jquery_curvycorners_packed.js"></script> 
     <script type="text/javascript" src="/decisionresult/js/decisionResult.js"></script>
     <!-- <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script> -->
     <script type="text/javascript" src="/decisionresult/js/jquery-1.8.js"></script> 
     <!-- End -->
 

     <table width="98%" border="0" cellpadding="0" cellspacing="0" align="center">
    <tr>
      <td width="30%">
        <div class="floatleft" >
          <img border="0" src="images/wwwftdcom_131x32.gif" width="131" height="32"/>
        </div>
        <xsl:if test="$showSearchBox">
            <div class="floatright" id="searchBlock">
                 &nbsp;
                   <label for="numberEntry" id="numberLabel"><xsl:value-of select="$searchLabel"/><br/>
                     <input type="text" size="7" maxlength="20" name="numberEntry" id="numberEntry" onkeypress="doEntryAction();"/>&nbsp;&nbsp;
                   </label>
            </div>
        </xsl:if>
      </td>
       <td width="40%" align="center" class="Header" id="pageHeader" valign="top">
                <xsl:value-of select="$headerName"/>
           </td>
          <td>
          </td>
          <td width="30%" style="text-align:right;">
              
              <xsl:if test="$showRefreshButton"> 
            	 <span>
            	 <button type="button" class="BlueButton" style="text-align:center; margin-right: 70px; width: 100px" name="refreshButtonHeader" id="refreshButtonHeader" onclick="javascript:doRefresh();">
            	 	<xsl:value-of select="$refreshButtonLabel"/>
            	 </button>
            	 </span>
              </xsl:if>
            
            <xsl:if test="$rtq = 'Y' and
                          $overrideRTQ != 'Y'">
              <input type="button" class="BlueButton" accesskey="Q" name="backButtonHeader" id="backButtonHeader" value="Return to (Q)ueue" onclick="javascript:doQueueReturnAction();"/> &nbsp; &nbsp;
      </xsl:if>
      <xsl:if test="$showBackButton">
                                <button type="button" class="BlueButton" accesskey="B" name="backButtonHeader" id="backButtonHeader" onclick="javascript:doBackAction();">
                                <xsl:value-of select="$backButtonLabel"/>
                                </button>
            </xsl:if>
          </td>
    </tr>
    <tr>
      <td colspan="4">
        <div style="width:100%;float:left;">
          <xsl:if test="$dnisNumber != '0000'">
             <xsl:choose>
            <xsl:when test="$dnisNumber != '' and $brandName != ''">
              <span class="PopupHeader" style="float:left;"> <xsl:value-of select="$dnisNumber"/> - <xsl:value-of select="$brandName" /></span>
            </xsl:when>
            <xsl:when test="$dnisNumber != ''">
              <span class="PopupHeader"  style="float:left;"><xsl:value-of select="$dnisNumber"/></span>
            </xsl:when>
            <xsl:when test="$brandName != ''">
              <span class="PopupHeader"  style="float:left;"><xsl:value-of select="$brandName"/></span>
            </xsl:when>
             </xsl:choose>
          </xsl:if>
          <span style="float:right;vertical-align:middle;padding-top:.4em;">&nbsp;&nbsp;
            <span class="Label" id="time"><xsl:if test="$showTime"><script type="text/javascript">startClock();</script></xsl:if></span>
            <xsl:if test="$showPrinter">
                    &nbsp; &nbsp; <a href="javascript:printIt();"><img src="images/printer.jpg" width="25" height="25" name="printer" id="printer" border="0" /></a>
                </xsl:if>
          </span>
          <div class="PopupHeader"   style="float:right;">
            <xsl:value-of select="$cservNumber"/> <!--&nbsp;<xsl:value-of select="$indicator"/>-->
          </div>
        </div>
      </td>
    </tr>
    <tr>
      <td colspan="2"  id="selectBoxTd">
        <xsl:if test="$showCSRIDs and //CSR_VIEWINGS/CSR_VIEWING/csr_id != ''">
           <span class="ErrorMessage">Currently being viewed by:&nbsp;</span>
            <xsl:choose>
             <xsl:when test="count(//CSR_VIEWINGS/CSR_VIEWING/csr_id) > 1">
             <select id="currentCSRs" name="currentUsers" >
                       <xsl:for-each select="//CSR_VIEWINGS/CSR_VIEWING/csr_id">
                           <option>
                            <xsl:value-of select="node()"/>
                           </option>
                       </xsl:for-each>
                   </select>
            </xsl:when>
              <xsl:otherwise>
            <span id="csrLabel"><xsl:value-of select="//CSR_VIEWINGS/CSR_VIEWING/csr_id"/></span>
              </xsl:otherwise>
          </xsl:choose>
         </xsl:if>
      </td>
      <td colspan="2"  id="cdispId" align="right">
                            <xsl:if test="$cdispEnabledFlag = 'Y'">
                               <span class="Label" id="cdisplink"><a href="javascript:toggleCallDisp();">Call Disposition</a></span>
                            </xsl:if>
      </td>
    </tr>
    <tr>
      <td colspan="4">
        <hr/>
      </td>
    </tr>
     </table>
  <div id="iframe_div">
     <iframe id="CSRFrame" name="CSRFrame" width="0px" height="0px"  border="0">
         <!-- This iframe is initialized with a "dummy" src value to
         prevent the MS IE browser from detecting these as nonsecure resources. -->
         <xsl:if test="$isclickthroughsecure = 'true'">
             <xsl:attribute name="src">https://<xsl:value-of select="$sitenamessl"/><xsl:value-of select="$applicationcontext"/>/html/blank.html</xsl:attribute>
        </xsl:if>
     </iframe>
  </div>
  </xsl:template>
</xsl:stylesheet>
