<?xml version='1.0' encoding='windows-1252'?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="header.xsl"/>
	<xsl:import href="footer.xsl"/>
  <xsl:import href="securityanddata.xsl" />
  <xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>
	<xsl:output method="html" indent="no"/>
	<xsl:output indent="yes"/>
	<xsl:key name="pagedata" match="/root/pageData/data" use="name" />
	<xsl:key name="security" match="/root/security/data" use="name"/>
		<xsl:key name="errors" match="/root/validation/error" use="name" />
   <!-- Root template -->
   <xsl:template match="/">
<html>
   <head>
<base target="_self" />
<META http-equiv="Content-Type" content="text/html" />
      <title>FTD - DNIS Maintenance</title>
      <link rel="stylesheet" type="text/css" href="css/ftd.css" />
      <link rel="stylesheet" type="text/css" href="css/calendar.css" />
      <script type="text/javascript" src="js/util.js"></script>
      <script type="text/javascript" src="js/FormChek.js"></script>
      <script type="text/javascript" src="js/calendar.js"></script>
      <script type="text/javascript" src="js/clock.js"></script>
			<script type="text/javascript">				
				<![CDATA[
// init code
function init()
{
]]>
  //var isValid = <xsl:value-of select="boolean(key('errors', 'VALIDATION_SUCCESS')/value)"/>;
  var isValid = "<xsl:value-of select="key('errors', 'VALIDATION_SUCCESS')/value"/>";
  var isAfterSubmit = <xsl:value-of select="boolean(key('pagedata', 'SUBMIT_ACTION')/value)" />;
  //alert("isvalid="+isValid+"  isaftersubmit="+isAfterSubmit);
  <xsl:if test="/root/out-parameters/OUT_LOCK_OBTAINED = 'N'">
    replaceText("lockError", "This DNIS record is locked by <xsl:value-of select='/root/out-parameters/OUT_LOCKED_CSR_ID'/>");
    document.getElementById("save").disabled="true";
	</xsl:if>

  <![CDATA[
  if(isValid=="Y" && isAfterSubmit)
  {
    //alert("Operation successful!");
    top.returnValue = "RELOAD";
    top.close();
  }
  else // edit or add action failed
  if(isAfterSubmit)
  {
    //display error message
]]>
    var errMsg = "";
    <xsl:for-each select="/root/validation/error" >
    <xsl:choose>
    <xsl:when test="name != 'VALIDATION_SUCCESS'" >
    errMsg = errMsg + "<xsl:value-of select='name' />: <xsl:value-of select='value' />\n";
    </xsl:when>
    </xsl:choose>
    </xsl:for-each>
    <![CDATA[
    replaceText("lockError", errMsg+"Edit or Add DNIS not successful. Please correct the errors and re-submit.");
  }
}

function replaceText( elementId, newString )
  {
   var node = document.getElementById( elementId );   
   var newTextNode = document.createTextNode( newString );
   if(node.childNodes[0]!=null)
   {
    node.removeChild( node.childNodes[0] );
   }
   node.appendChild( newTextNode );
  }


function validateDnis()
{
  var form = document.getElementById("editDNIS");
  if(isWhitespace(form.dnis_id.value) || form.dnis_id.length>7 || !isPositiveInteger(form.dnis_id.value))
  {
    alert("DNIS_ID is missing or bad. Please correct it before submitting.");
    return false;
  }
  if(isWhitespace(form.default_source_code.value) || form.default_source_code.value.length>10 || !isAlphanumeric(form.default_source_code.value))
  {
    alert("Default Source Code is missing or bad. Please correct it before submitting.");
    return false;
  }
  if(isWhitespace(form.description.value))
  {
    alert("DNIS Description is missing. Please correct it before submitting.");
    return false;
  }
  return true;
}

function closePopup()
{
	top.returnValue = "NO_RELOAD";
	//document.getElementById("unlockFrame").src="lock.do?action_type=unlock&lock_app=DNIS Maintenance&lock_id="+document.forms[0].dnis_id.value+"&securitytoken="+document.forms[0].securitytoken.value+"&context="+document.forms[0].context.value+"&applicationcontext="+document.forms[0].applicationcontext.value;
  	top.close();
}

function editDnisAction()
{
  // first, validate all the input parameters
  if(validateDnis())
  {
    document.forms[0].action = "editDnis.do";
    document.forms[0].target = window.name;
    document.forms[0].submit();
  }
  else
  {
    // user has to fix errors and resubmit the form
    return;
  }
}
      ]]></script>
		 
 </head>

<body onload="init();">

					<table border="0" cellpadding="0" cellspacing="0" width="98%" align="center">
						<tr>
							<td align="center" id="lockError" class="ErrorMessage"></td>
						</tr>
					</table>
   <table width="55%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
    <tr><th>
    <xsl:choose>
     <xsl:when test="key('pagedata','action_type')/value='insert_dnis'">
      Add DNIS Data
     </xsl:when>
     <xsl:otherwise>
      Update DNIS Data
     </xsl:otherwise>
     </xsl:choose>
     </th></tr>
   	<tr><td colspan="3">
	<table align="center" width="100%" cellspacing="10">
   	<form name="editDNIS" method="post" >
          <xsl:call-template name="securityanddata"/>
          <xsl:call-template name="decisionResultData"/>
					<input type="hidden" name="action_type" id="action_type" value="{key('pagedata','action_type')/value}"/>
		<iframe id="unlockFrame" name="unlockFrame" height="0" width="0" border="0"></iframe>
    	<tr>
    	  <td align="right">DNIS</td>
        <xsl:choose>
          <xsl:when test="key('pagedata','action_type')/value='insert_dnis'">
            <td><input type="text" name="dnis_id" id="dnis_id" title="Number with 7 digits or less"/></td>
          </xsl:when>
          <xsl:otherwise>
					  <input type="hidden" name="dnis_id" id="dnis_id" value="{key('pagedata','dnis_id')/value}"/>
            <td><xsl:value-of select="key('pagedata','dnis_id')/value"/></td>
          </xsl:otherwise>
        </xsl:choose>
      </tr>
      <tr><td align="right"> Origin</td>
       <td><select name="origin" id="origin">
		 	 <xsl:for-each select="/root/OriginsList/Origin">
       <xsl:choose>
       <xsl:when test="company_id=/root/Dnis/dnis_data/origin">
         <option value="{company_id}" selected="selected"><xsl:value-of select="company_id"/></option>
        </xsl:when>
        <xsl:otherwise>
         <option value="{company_id}" ><xsl:value-of select="company_id"/></option>
        </xsl:otherwise>
        </xsl:choose>
       </xsl:for-each>
       </select>
	   </td>
       </tr>
		 <tr>
		   <td align="right">Status</td>
		   <td>
         <xsl:choose>
          <xsl:when test="/root/Dnis/dnis_data/status_flag = 'A'">
           <input type="radio" name="status_flag" value="A" checked="checked"/> Active
		 			 <input type="radio" name="status_flag" value="I" /> Inactive
          </xsl:when>
          <xsl:otherwise>
           <input type="radio" name="status_flag" value="A" /> Active
		 			 <input type="radio" name="status_flag" value="I" checked="checked"/> Inactive
          </xsl:otherwise>
         </xsl:choose>
       </td></tr>
		 <tr><td align="right"> OE</td>
		 <td>
         <xsl:choose>
          <xsl:when test="/root/Dnis/dnis_data/oe_flag = 'Y'">
           <input type="radio" name="oe_flag" value="Y" checked="checked"/> Yes
		 			 <input type="radio" name="oe_flag" value="N" /> No
          </xsl:when>
          <xsl:otherwise>
           <input type="radio" name="oe_flag" value="Y" /> Yes
		 			 <input type="radio" name="oe_flag" value="N" checked="checked"/> No
          </xsl:otherwise>
         </xsl:choose>
     </td></tr>                           
		 <tr><td align="right">Yellow Page</td>
     <td>
       <xsl:choose>
        <xsl:when test="/root/Dnis/dnis_data/yellow_pages_flag = 'Y'">
          <input type="radio" name="yellow_pages_flag" value="Y" checked="checked"/> Yes
		 			<input type="radio" name="yellow_pages_flag" value="N" /> No
        </xsl:when>
        <xsl:otherwise>
          <input type="radio" name="yellow_pages_flag" value="Y" /> Yes
		 			<input type="radio" name="yellow_pages_flag" value="N" checked="checked"/> No
        </xsl:otherwise>
       </xsl:choose>
     </td></tr>                           
     <tr><td align="right"> Script</td>
		 	 <td><select name="script_code" id="script_code">
        <xsl:for-each select="/root/CountryScriptList/CountryScript">
        <xsl:choose>
         <xsl:when test="script_code = /root/Dnis/dnis_data/script_code">
           <option value="{script_code}" selected="selected"><xsl:value-of select="script_code"/></option>
         </xsl:when>
         <xsl:otherwise>
           <option value="{script_code}"><xsl:value-of select="script_code"/></option>
         </xsl:otherwise>
         </xsl:choose>
        </xsl:for-each>
				</select></td></tr>
		<tr><td align="right">Defalt Source Code</td>
		 	 <td><input type="text" name="default_source_code" id="default_source_code" value="{/root/Dnis/dnis_data/default_source_code}"/></td></tr>
		<tr><td align="right">Description</td>
		 	 <td><input type="text" name="description" id="description" value="{/root/Dnis/dnis_data/description}"/></td></tr>
		<tr><td align="right">Script ID</td>
		 	 <td><select name="script_id" id="script_id">
        <xsl:for-each select="/root/ScriptIdList/ScriptId">
         <xsl:choose>
          <xsl:when test="script_master_id = /root/Dnis/dnis_data/script_id">
            <option value="{script_master_id}" selected="selected"><xsl:value-of select="script_master_id"/></option>
          </xsl:when>
          <xsl:otherwise>
            <option value="{script_master_id}"><xsl:value-of select="script_master_id"/></option>
          </xsl:otherwise>
         </xsl:choose>
        </xsl:for-each>
				</select></td></tr>
												
               <tr>
                  <td colspan="2" align="left">   
                  </td>
               </tr>
		</form>
		
    </table>
    </td></tr>
		    <table width="55%" border="0" align="center" cellpadding="2" cellspacing="2">
  			    <tr>
  			    	<td align="center"><input id="save" name="save" type="button" value="(S)ubmit" class="blueButton" onclick="return editDnisAction();"/>
	  	 		   	<input type="button" value="(C)ancel" class="blueButton" onclick="return closePopup();" /></td>
      			</tr>
		 	</table>
    </table>   
</body>
</html>
</xsl:template>
</xsl:stylesheet>
<!-- Stylus Studio meta-information - (c) 2004-2005. Progress Software Corporation. All rights reserved.
<metaInformation>
<scenarios ><scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="..\..\..\..\..\dnisinfo.xml" htmlbaseurl="" outputurl="" processortype="internal" profilemode="0" profiledepth="" profilelength="" urlprofilexml="" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext=""/></scenarios><MapperMetaTag><MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/><MapperBlockPosition></MapperBlockPosition></MapperMetaTag>
</metaInformation>
-->