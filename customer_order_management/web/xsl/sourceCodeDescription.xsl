<!DOCTYPE ACDemo[
	<!ENTITY nbsp "&#160;">
	<!ENTITY reg "&#xAE;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html"/>
 <xsl:template match="/root">
	<html>
		<head>
			<title>Source Code Description</title>
			<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
			<script type="text/javascript">
				<![CDATA[
					function doCloseAction(){
						top.close();
					}
				]]>
			</script>
		</head>
		<body>
<table width="98%" align="center" cellpadding="1" cellspacing="1" class="mainTable">
 <tr>
	<td class="banner" colspan="5">Source Code Description</td>
 </tr>
<tr>
	<td>
		<table class="innerTable" border="0" width="100%" align="center" cellpadding="1" cellspacing="2">

			<tr style="text-align:left;">
				<td class="Label">
						Name / Descritpion
				</td>
				<td class="Label">
					Offer
				</td>
				<td class="Label">
					Service Charge
				</td>
	        </tr>
	        <tr style="text-align:left;">
				<td>
					<xsl:value-of select="SOURCE_CODES/SOURCE_CODE/source_description"/>
				</td>
				<td>
					<xsl:value-of select="SOURCE_CODES/SOURCE_CODE/offer_description"/>
				</td>
				<td>
					<xsl:value-of select="SOURCE_CODES/SOURCE_CODE/service_charge"/>
				</td>
		    </tr>
		    <tr>
				<td class="Label" colspan="3">
					Co-Branded URL
				</td>
		    </tr>
		    <tr>
                <td  colspan="3">
                   <xsl:value-of select="SOURCE_CODES/SOURCE_CODE/co_brand_url"/>
                </td>
		    </tr>
		    <tr>
                <td class="Label" style="text-align:left" colspan="2">
                  Comments
                </td>
		    </tr>
		    <tr>
			   <td colspan="2">
	              <textarea rows="4" cols="50" wrap="soft" readonly="true">
                   <xsl:value-of select="SOURCE_CODES/SOURCE_CODE/comments" />
	              </textarea> 
				</td>
		    </tr>
      </table>
    </td>
  </tr>
</table>
  	<table border="0" cellpadding="2" cellspacing="1" width="98%" align="center">
  		<tr>
  			<td align="center">
  				<button type="button" id="closeButton" class="BlueButton" accesskey="C" onclick="javascript:doCloseAction();">(C)lose</button>			
  			</td>
  		</tr>
  	</table>
  
		</body>
	</html>
 </xsl:template>
</xsl:stylesheet>