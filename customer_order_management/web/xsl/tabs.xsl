<!DOCTYPE ACDemo[
	<!ENTITY nbsp "&#160;">
	<!ENTITY reg "&#169;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" indent="yes"/>
	<xsl:template name="tabs">
		<table id="itemTabsTable" border="0" cellpadding="0" cellspacing="0" width="100%">
			<tr>
				<td>
					<div id="tabs">
                     <button type="button" id="customerAccount" tabindex="1" title="{//CAI_CUSTOMERS/CAI_CUSTOMER/customer_first_name} {//CAI_CUSTOMERS/CAI_CUSTOMER/customer_last_name}" onkeypress="javascript:showItemAt(this);javascript:updateCurrentUsers(CUSTOMER_ACCT_INDEX,CUSTOMER_ACCT_INDEX);javascript:showOnLoadAlerts(CUSTOMER_ACCT_INDEX);"  content="customerAccountDiv" onmouseover="tabMouseOver(this);" onmouseout="tabMouseOut(this);" onclick="javascript:showItemAt(this);javascript:updateCurrentUsers(CUSTOMER_ACCT_INDEX,CUSTOMER_ACCT_INDEX);javascript:showOnLoadAlerts(CUSTOMER_ACCT_INDEX);">CUSTOMER ACCOUNT</button>
            <script type="text/javascript" src="js/tabIndexMaintenance.js"/>
						<script type="text/javascript" language="javascript">
	                  <!--document.write('<button type="button" id="customerAccount" tabindex="1" onkeypress="javascript:showItemAt(this);javascript:updateCurrentUsers(CUSTOMER_ACCT_INDEX,CUSTOMER_ACCT_INDEX);"  content="customerAccountDiv" onmouseover="tabMouseOver(this);" onmouseout="tabMouseOut(this);" onclick="javascript:showItemAt(this);javascript:updateCurrentUsers(CUSTOMER_ACCT_INDEX,CUSTOMER_ACCT_INDEX);">CUSTOMER ACCOUNT</button>');-->
	            			document.write('<button type="button" id="shoppingCart" tabindex="2" title="{//CAI_ORDERS/CAI_ORDER/external_order_number[. = $keyValue]/../master_order_number}"  content="shoppingCartDiv" onmouseover="tabMouseOver(this);" onmouseout="tabMouseOut(this);"  onclick="javascript:showItemAt(this);javascript:updateCurrentUsers(SHOPPING_CART_INDEX,SHOPPING_CART_INDEX);">CUSTOMER INFO</button>');
						</script>
						<!-- Item Tabs - need to be in sequential order. -->
						<script type="text/javascript" language="javascript">
                  var tabsArray = new Array(document.getElementById('customerAccount'),document.getElementById("shoppingCart"));
                  var _tabCount = 2;
                  var statusCount = 0;
						</script>
						<xsl:for-each select="CSCI_ORDERS/CSCI_ORDER">
							<xsl:call-template name="addTab"/>
						</xsl:for-each>

					</div>
				</td>
				<td style="text-align:left" width="5%">
					<div id="more_previous_Tabs" >
						<a href="javascript:showMoreAction();" id="moreLink" class="textlink">More...</a>
						<br/>
						<a href="javascript:showPreviousAction()" id="previousLink" class="textlink">Previous...</a>
					</div>
				</td>
			</tr>
		</table>
<script type="text/javascript"><![CDATA[
 var CUSTOMER_ACCT_INDEX = 0;
 var SHOPPING_CART_INDEX = 1;
 var TAB_WIDTH = 140;
 var MAX_TABS = 5;
 var MAX_ORDERS = 3;
 var static_items = 2;
 var minItem = 1;
 var currentTab;
 var currentContent;
 var currentTabIndex;
 var currentTabId;
 var hidden = false;
 var header = document.getElementById('pageHeader');
 var customerAccount = "customerAccount";
 var shoppingCart = "shoppingCart";
 var searchedOrder = '';
 var savedSearchOrder = '';
 var sc_confirmation_number = '';


/**************************************************************
*
*	tabMouseOut(tab)
*
**************************************************************/
function tabMouseOut(tab){
  if(tab.className != "selected")
  	tab.className = "button";
  if(searchedOrder != null){
     if(tab.id == searchedOrder.id)
        highLightOrange(tab);
  }
}
/**************************************************************
*
*	tabMouseOver(tab)
*
***************************************************************/
function tabMouseOver(tab){
  if(tab.className != "selected" )
      tab.className = "hover";
}

function highLightOrange(tab){
	if(tab.className != "selected")
		tab.className = "orange";
}
/****************************************************************
*
*	initializeTabs(tabToDisplay)
*
*****************************************************************/
function initializeTabs(tabToDisplay){
  var display_page = document.getElementById('display_page');

  if(tabToDisplay != '' && tabToDisplay != null)
  {
    checkOrderStatus(getInteger(tabToDisplay));
  }

  /*  display/hide the more link */
  if(checkMoreStatus()){
     showHideMore("visible");
  }else{
     showHideMore("hidden");
  }

  /* display/hide the previous link */
  if(checkPreviousStatus()){
     showHidePrevious("visible");
  }else{
     showHidePrevious("hidden");
  }

  /* Display the Customer Account tab */
  if( (actionName.substr(0,6) == 'ca_csc') || (display_page.value == 'ACCTINFO')){
       document.getElementById('searchBlock').style.visibility = "hidden";
       currentTab = document.getElementById('customerAccount');

        /* highlight the tab if the user has searched on an order at any point */
       searchedOrder = document.getElementById(tabToDisplay);
       if ((searchedOrder != '') && (searchedOrder != null)){
          savedSearchOrder =  searchedOrder;
          highLightOrange(searchedOrder);
       }
       showItemAt(currentTab);
      /* set the actual content visibilty */
      currentContent = document.getElementById(currentTab.content);
      return;
   }

  /* Display the Customer Shopping Cart tab (Customer Info) */
  if((tabToDisplay != '' && tabToDisplay != null) && (actionName != 'customer_account_search' && actionName != 'recipient_order_number_search' &&
  			 actionName.substr(0,6) != 'ca_csc' && display_page.value != 'ACCTINFO' && actionName == 'search') && (recipient_flag.toUpperCase() != 'Y'))
  {
     currentTab = document.getElementById('shoppingCart');
     searchedOrder = document.getElementById(tabToDisplay);
     savedSearchOrder =  searchedOrder;
     highLightOrange(searchedOrder);
     showItemAt(currentTab);
  }

  /*
    Display the Recipient/Order Information page and set the tab to highlight orange
    when the user navigates to another tab on the page.
  */
  if( ( (tabToDisplay != '' && tabToDisplay != null) && ( (actionName == 'customer_account_search' )  || (actionName == 'recipient_order_number_search') ||(actionName != 'search') || (recipient_flag.toUpperCase() == 'Y'))) )
  {
       /*set the tab(button) to the "selected" image */
      currentTab = document.getElementById(tabToDisplay);
      searchedOrder = currentTab;
      savedSearchOrder =  currentTab;
      showItemAt(currentTab);
   }

  /*
    Display the Customer Info page if no other
    criteria is met.
  */
  if( (tabToDisplay == null || tabToDisplay == '') && display_page.value != 'ACCTINFO'){
     currentTab = document.getElementById('shoppingCart');
     showItemAt(currentTab);
  }

    /* set the actual content visibilty */
    currentContent = document.getElementById(currentTab.content);

  //refresh ids - upon initial invokation from COS screen, if PPV is displayed from Customer Info tab,
  //we want the PPV updates (customer/order details + bypass variable) to propagate to the correct arraylists.
  //Since PPV updates are done as part of refresh, we will invoke updateCurrentUsers() and update the arrays.
  updateCurrentUsers(0,0);

}



/***********************************************************
*
*		showTab(tab)
*
************************************************************/
function showTab(tab){

  if((currentTab.id != savedSearchOrder.id) && currentTab){
      currentTab.className = "button";
  }
  else{
   	currentTab.className = "orange";
  }
    tab.className = "selected";
    currentTab = tab;
    showTabContent(tab);

  <!-- Privacy Policy Verification -->
  if (document.getElementById("t_call_log_id").value != null &&
      document.getElementById("t_call_log_id").value != "")
  {
    //if bypass flag is set to Y, nothing is required.  Else, check to see the current tab
    if (document.getElementById('bypass_privacy_policy_verification').value != 'Y')
    {
      //reset the value orderValidationRequiredFlag to Y
      orderValidationRequiredFlag = 'Y';

      //if current tab = "customer info" or "customer shopping cart", set the orderValidationRequiredFlag = N
      if(currentTab.id == "customerAccount" || currentTab.id == "shoppingCart")
      {
        orderValidationRequiredFlag = 'N';
      }


      //regardless of the tab, loop thru customersValidated.  If customer has already been validated, set customerValidationRequiredFlag = V
      for(var i=0;i<customersValidated.length;i++)
      {
        if (document.getElementById('customer_id').value == customersValidated[i])
        {
          customerValidationRequiredFlag = 'V';
          break;
        }
      }

      //if recipient order tab, loop thru ordersValidated.  If order has already been validated, set orderValidationRequiredFlag = V
      if(currentTab.id != "customerAccount" && currentTab.id != "shoppingCart")
      {
        for(var i=0;i<ordersValidated.length;i++)
        {
          if (document.getElementById('order_detail_id').value == ordersValidated[i])
          {
            orderValidationRequiredFlag = 'V';
            break;
          }
        }
      }

      //if either customerValidationRequiredFlag or orderValidationRequiredFlag equals Y, invoke privacy policy popup
      if (customerValidationRequiredFlag == 'Y' || orderValidationRequiredFlag == 'Y')
      {
        displayPrivacyPolicy(document.getElementById('customer_id').value, customerValidationRequiredFlag, document.getElementById('order_detail_id').value, orderValidationRequiredFlag);
      }

    }
  }

}
/**********************************************************
*
*	checkMoreStatus()
*
***********************************************************/
function checkMoreStatus(){
  if(show_more == 'Y' || show_more == 'y'){
   return  true;
  }
  return false;
}
/***********************************************************
*
*	checkPreviousStatus()
*
************************************************************/
function checkPreviousStatus(){
  if(show_previous == 'y' || show_previous == 'Y'){
    return true;
  }
  return false;
}
/**********************************************************
*
*	showTabContent(tab)
*
***********************************************************/
function showTabContent(tab){
  if(currentContent)
       currentContent.className = "hidden";
  currentContent = document.getElementById(tab.content);

  if(currentContent.id == "customerAccountDiv"){
    var order = document.getElementById('messageContainer');
    order.style.display = "block";
  }
  var int = getInteger(currentContent.id);
  hidden = false;

  if( (int != '' && int != null) )
  if(statusArray[int - 1] != ''){
    displayPageMessage();
  }else{
    hidePageMessage();
    var content = document.getElementById('content');
    content.style.display = 'block';
    currentContent.className = "visible";
  }
  if(!hidden){
    hidePageMessage();
    var content = document.getElementById('content');
    content.style.display = 'block';
  }
currentContent.className = "visible";
}
/********************************************************
*
*	getInteger(s)
*
*********************************************************/
function getInteger(s){
  var i;
  for (i = 0; i < s.length; i++){
      // Check that current character is number.
      var c = s.charAt(i);
      if (((c < "0") || (c > "9")))
        continue;
      return c;
  }
}
/**********************************************************
*
*	showHidePrevious(visibility)
*
***********************************************************/
function showHidePrevious(visibility){
  var previous = document.getElementById("previousLink");
  previous.style.visibility = visibility;
}
/**********************************************************
*
*	showHideMore(visibility)
*
***********************************************************/
function showHideMore(visibility){
  var more = document.getElementById("moreLink");
  more.style.visibility = visibility;
}
/**********************************************************
*
*	showItemAt(index)
*
**********************************************************/
function showItemAt(index){
 if(window.event)
 	if(window.event.keyCode)
 		if(window.event.keyCode != 13)
 			return;
  currentTabIndex = index;
  if( (currentTabIndex.id != "customerAccount") &&
               (currentTabIndex.id != "shoppingCart") )
  {
	    header.innerHTML = "Recipient / Order Information";
      setRecipTabIndexes();
	    document.getElementById('searchBlock').style.visibility = "visible";

	    /*find the tab to display*/
	    for(var z = 0; z < tabsArray.length; z++){
	      if(index.id == tabsArray[z].id){
	        showTab(tabsArray[z]);
	       }
	    }
  }else if(currentTabIndex.id == "customerAccount" ) {
    header.innerHTML = "Customer Account";
    setCSCustomerAccountIndexes();
    document.getElementById('searchBlock').style.visibility = "hidden";
    showTab(tabsArray[CUSTOMER_ACCT_INDEX]);
  }
  else{
    header.innerHTML = "Customer Shopping Cart";
    setCartIndexes();
    document.getElementById('searchBlock').style.visibility = "visible";
    showTab(tabsArray[SHOPPING_CART_INDEX]);
  }
}

/******************************************************
*   Set the necessary variables to help
*	determine wether or not to display a link to S,P,R
*	in lieu of the Recipient/Order Information page
*	checkOrderStatus(obj)
*
*******************************************************/
function checkOrderStatus(obj){
	if(obj > 0){
		obj -= 1;
	}
  if(statusArray[obj] != null && statusArray[obj] != ''){
    if(statusArray[obj] == 'S'){
      scrubStatus = 'Scrub';
      sc_mode = statusArray[obj];
      sc_confirmation_number = confirmations[obj];
      return true;
    }
    if(statusArray[obj] == 'P'){
      scrubStatus = 'Pending';
      sc_mode = statusArray[obj];
      sc_confirmation_number = confirmations[obj];
      return true;
    }
    if(statusArray[obj] == 'R'){
      scrubStatus = 'Removed';
      sc_mode = statusArray[obj];
      sc_confirmation_number = confirmations[obj];
      return true;
    }
  }
 /*
  Anytime a user clicks/enter keys a tab button
  we need to retrieve that tabs order detail id and
  populate a hidden field called order_detail_id
  this is used by the record locking mechanism in order
  to lock the record based on the tab clicked.
 */
  document.getElementById('order_detail_id').value  =
        document.getElementById('order_detail_id' + (obj + 1)).value;

  return false;
}

/********************************************************
* 	Display a message to the user
* 	indicating where the order is
* 	Scrub, Pending , Removed
*******************************************************/
function displayPageMessage(){

  var content = document.getElementById('content');
  var waitDiv = document.getElementById("waitDiv").style;
  var waitMessage = document.getElementById("waitMessage");
  var waitTD = document.getElementById("waitTD");
  var inTab = document.getElementById("inTable");
  var hiddenNav = document.getElementById('hiddenPageNavigation');


  content.style.display = "none";
  waitDiv.display = "block";
  waitDiv.height = "325px";
  hiddenNav.style.display = "block";
  //inTab.height="325px";
  var message = 'This order is currently  in ';
  message += '<a href="#" class="textLink" onclick="javascript:doShowScrubAction()">'+ scrubStatus +'</a>'
  waitMessage.innerHTML = message;

    hidden = true;
}
/*********************************************************
*
*		hidePageMessage()
*
*********************************************************/
function hidePageMessage(){

   var waitDiv = document.getElementById("waitDiv").style;
   var hiddenNav = document.getElementById('hiddenPageNavigation');

   waitDiv.display = "none";
   hiddenNav.style.display = "none";
   hidden = false;
}

 /*******************************************************************************
*  setRecipTabIndexes()
*  Sets tabs for Recipient / Order Information page
*  See tab.xsl
*******************************************************************************/
function setRecipTabIndexes(){
  //untabAllElements();
	var begIndex = findBeginIndex();
	for(var i = 0; i < (tabsArray.length - 2) ; i++){
	  var z = (i + 1);
  	var recipTabElements = new Array('sourceCodeLink'+z,'memberInfoLink'+z,
      'updateOrderInfo'+z, 'updatePaymentInfo'+z,'updateFlorist'+z,'orderPaymentInfoLink'+z,
      'floristNameLink'+z,'orderComments'+z,'email'+z,'letters'+z,
      'orderHistory'+z,'origView'+z,'floristInquiry'+z,'communiction'+z,'tagOrder'+z,
      'reconciliation'+z,'refund'+z,'hold'+z,'search'+z,'mainMenu'+z,'printOrder'+z,'printer');
      var newBegIndex = setTabIndexes(recipTabElements, begIndex);
	}
  return newBegIndex;
}
/*******************************************************************************
*  setCartIndexes()
*  Sets tabs for Customer Shopping Cart.
*  See tab.xsl
*******************************************************************************/
function setCartIndexes()
{
  //untabAllElements();
	var begIndex = findBeginIndex();
 	var cartElements = new Array('cartSourceCodeLink','cartPaymentInfoLink',
    'cartUpdateCustomer','cartCommentsAction','cartHistoryAction','cartHold',
    'cartLossPrevention','cartRefund','updateEmailStatus', 'viewBillingTrans',
    'cartSearch','cartBack','cartMainMenu');
  var newBegIndex = setTabIndexes(cartElements, begIndex);
  return newBegIndex;
}
/*******************************************************************************
*  setCSCustomerAccountIndexes()
*  Sets tabs for Customer Service Customer Account Page.
*  See tab.xsl
*******************************************************************************/
function setCSCustomerAccountIndexes()
{
  //untabAllElements();
	var begIndex = findBeginIndex();
  var externalAnchors = document.getElementsByName('external');
  if((externalAnchors != null && externalAnchors.length > 0)){
			for(var k = 0; k < externalAnchors.length ; k++){
				externalAnchors[k].tabIndex = begIndex++;
			}
	}
	var caElements = new Array('nextItem','nextItem2', 'backItem','backItem2',
  'firstItem','firstItem2', 'lastItem','lastItem2', 'customer_comments',
  'update_customer', 'hold','loss_prevention', 'search', 'main_menu',
  'caDirectMail', 'membershipInfoLink', 'email_address','caMoreLink','printer');
  begIndex = setTabIndexes(caElements, begIndex);
  return begIndex;
}

function showPopup(mylink) {
    //Modal_Arguments
    var modalArguments = new Object();

    //Modal_Features
    var modalFeatures = 'dialogWidth:600px; dialogHeight:400px; resizable:yes; scroll:yes';

    window.showModalDialog(mylink, modalArguments, modalFeatures);
    //return false;
}

 ]]></script>
	</xsl:template>
	<xsl:template name="addTab">
      <button type="button" id="item{@num}" tabindex="{@num + 2}"
          title="{first_name} {last_name} &#10;&#10;{order_disp_code}"
          onmouseover="tabMouseOver(this);" onmouseout="tabMouseOut(this);"
          onkeypress="javascript:checkOrderStatus( '{@num}' );javascript:showItemAt( this );javascript:updateCurrentUsers('{order_detail_id}','{order_guid}');"
          content="item{@num}Div" onclick="javascript:checkOrderStatus( '{@num}' );javascript:showItemAt( this );javascript:updateCurrentUsers('{order_detail_id}','{order_guid}');">
          <xsl:if test="scrub_status = ''">
            <img src="images/transparent-{order_detail_status}.png" style="width:10px;height:10px;"/>&nbsp;&nbsp;
          </xsl:if>
          <xsl:value-of select="external_order_number"/>
          <xsl:if test="lifecycle_delivered_status = 'Y'">
            &nbsp;&nbsp;<img src="images/green-check-mark.png" style="width:14px;height:14px;"/>
          </xsl:if>
      </button>
      <script type="text/javascript" language="javascript">
        tabsArray[_tabCount] = document.getElementById('item<xsl:value-of select="@num"/>');
        _tabCount++;
        statusCount++;
      </script>
	</xsl:template>
</xsl:stylesheet>
