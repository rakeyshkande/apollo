<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;"> 
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="securityanddata.xsl"/>
<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>

<xsl:key name="pageData" match="/root/pageData/data" use="name"/>
<xsl:key name="security" match="/root/security/data" use="name"/>
	
<xsl:output method="html" indent="yes"/>
<xsl:template match="/root">
<html>
<head>
<script type="text/javascript" language="javascript">
var message = '<xsl:value-of select="key('pageData','message_display')/value"/>';
var updated = '<xsl:value-of select="key('pageData','updated')/value"/>';
var updateable = '<xsl:value-of select="key('pageData','updateable')/value"/>';

<![CDATA[
	function processCustomer()
	{
	
    if(message == '')
    {    
    	
        /* decide which action we need to contine to  */
        if (parent.pageAction == 'comments'){
            parent.doRetrieveCustomerComments();
        }
        if (parent.pageAction == 'email_history'){
            
        }
        if (parent.pageAction == 'search'){
          parent.doSearchAction();
        }
        if (parent.pageAction == 'back'){
          parent.doBackAction();
        }
        if (parent.pageAction == 'main_menu'){
          parent.doMainMenuAction();
        } 
        if(parent.pageAction == 'submit' && updateable.toUpperCase() == 'Y' ){
          parent.doSubmitChangesAction();
        }
        if(parent.pageAction == 'complete' && updated.toUpperCase() == 'Y'){
          parent.doUpdateCustomerSearch();
        }
    }
    else
    {
        var ele = parent.document.getElementById('outterContent');
        var displayType = ele.style.display;
        if(displayType == 'none'){
          parent.displayCustomerPage();
        }
        parent.uc_change_flag = false;
        alert(message);
    }
	}
]]>
</script>
</head>
<body onload="javascript:processCustomer();">
    <xsl:call-template name="securityanddata"/>   
    <xsl:call-template name="decisionResultData"/>
</body>
</html>
</xsl:template>
</xsl:stylesheet>