<!DOCTYPE ACDemo [
  <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<!-- Keys -->
<xsl:key name="pageData" match="root/pageData/data" use="name"/>
 <xsl:template name="orderShoppingCart">
 <script type="text/javascript">


    <![CDATA[
    
                
                /* This function returns the user to the queue they came from */
    /***********************************************************************************
                * canRepAccessOrder()
                ************************************************************************************/
                function canRepAccessOrder(externalOrderNumber,customerId,preferredPartner,warningMessage,accessType,permission){
                    if ( preferredPartner != '' && permission == 'N' )
                    {
                        var modalArguments = new Object();
                        modalArguments.modalText = warningMessage;
                        window.showModalDialog("alert.html", modalArguments, 'dialogWidth:300px; dialogHeight:200px; center:yes; status=no; help=no; resizable:no; scroll:yes');
                    }
                    else
                    {
                        doAccountSearch(externalOrderNumber,customerId);
                    }
                }

function showPopup(mylink) {
    //Modal_Arguments
    var modalArguments = new Object();

    //Modal_Features
    var modalFeatures = 'dialogWidth:600px; dialogHeight:400px; resizable:yes; scroll:yes';

    window.showModalDialog(mylink, modalArguments, modalFeatures);
}

    ]]>
    </script>
<table border="0" width="100%" style="position:relative;" >
  <tr>
  <td>
    <div class="tableContainer" id="messageContainer"  style="position:static;">
      <table class="scrollTable" cellpadding="0" cellspacing="0" border="0"  style="width:97%;">
        <thead class="fixedHeader" id="fixedHeader">
        <tr>
          <th colspan="2" width="15%" style="text-align:left;">Shopping Cart Order#</th>
          <th colspan="2" width="25%" style="text-align:left;padding-left:2em;">Order #</th>
          <th style="text-align:left;">Recipient Name</th>
          <th>Delivery<br/>Date</th>
          <th>Order<br/>Date</th>
          <th>Total</th>
          <th>Refund</th>
          <th>Disposition</th>
        </tr>
        </thead>
            <tbody class="scrollContent" id="scrollContent">
          <xsl:for-each select="CAI_ORDERS/CAI_ORDER">
                <tr>
            <td width="1%">
              <xsl:if test="cart_hold_indicator = $YES">
                <img src="images/IconLock.gif"/>
              </xsl:if> 
            </td>
                                                <xsl:choose>
                                                    <xsl:when test="scrub_status = '' and partner_name != '' ">
                                                        <td style="text-align:left;padding-left:.2em;"><a href="javascript:canRepAccessOrder('{master_order_number}', '{customer_id}','{partner_name}', '{partner_warning_message}', 'Clean', '{key('pageData', partner_resource)/value}');" name="master"><xsl:value-of select="master_order_number"/></a>
                                                            <input type="hidden" name="guid_{master_order_number}" id="guid_{master_order_number}" value="{order_guid}"/>
                                                            <input type="hidden" name="rep_can_access_clean" id="rep_can_access_clean" value="{key('pageData', partner_resource)/value}"/>
                                                        </td>
                                                    </xsl:when>
                                                    <xsl:when test="scrub_status != '' and scrub_partner_display_name != '' ">
                                                        <td style="text-align:left;padding-left:.2em;"><a href="javascript:canRepAccessOrder('{master_order_number}', '{customer_id}','{scrub_partner_display_name}', '{scrub_partner_warning_message}', 'Scrub', '{key('pageData', scrub_partner_resource)/value}');" name="master"><xsl:value-of select="master_order_number"/></a>
                                                            <input type="hidden" name="guid_{master_order_number}" id="guid_{master_order_number}" value="{order_guid}"/>
                                                            <input type="hidden" name="rep_can_access_scrub" id="rep_can_access_scrub" value="{key('pageData', scrub_partner_resource)/value}"/>
                                                        </td>
                                                    </xsl:when>
                                                    <xsl:otherwise>
                                                        <td style="text-align:left;padding-left:.2em;"><a href="#" onclick="javascript:doAccountSearch('{master_order_number}', '{customer_id}');" name="master"><xsl:value-of select="master_order_number"/></a>
                                                            <input type="hidden" name="guid_{master_order_number}" id="guid_{master_order_number}" value="{order_guid}"/>
                                                        </td>
                                                    </xsl:otherwise>
                                                </xsl:choose>
            
            <td style="text-align:right;" width="2%">
              <xsl:choose>
                <xsl:when test="scrub_status = $SCRUB">
                  <img src="images/cancel.gif"/>&nbsp;
                </xsl:when>
                <xsl:when test="scrub_status = $PENDING">
                  <img src="images/warning.gif"/>&nbsp;
                </xsl:when>
                <xsl:when test="scrub_status = $REMOVED">
                  <img src="images/removed.gif"/>&nbsp;
                </xsl:when>
                <xsl:when test="order_hold_indicator = $YES">
                  <img src="images/IconLock.gif"/>&nbsp;
                  <xsl:if test="lifecycle_delivered_status = 'Y'">
                      <xsl:choose>
                          <xsl:when test="status_url != ''">
                              <xsl:choose>
                                  <xsl:when test="status_url_active = 'Y'">
                                      <a href="javascript:showPopup('{status_url}');">
                                          <img src="images/green-check-mark.png" style="width:14px;height:14px;border:0;"/>
                                      </a>
                                      &nbsp;
                                  </xsl:when>
                                  <xsl:otherwise>
                                      <img src="images/green-check-mark.png" style="width:14px;height:14px;">
                                      <xsl:attribute name="alt"><xsl:value-of select="normalize-space(status_url_expired_msg)"/></xsl:attribute>
                                      </img>
                                      &nbsp;
                                  </xsl:otherwise>
                              </xsl:choose>
                          </xsl:when>
                          <xsl:otherwise>
                              <img src="images/green-check-mark.png" style="width:14px;height:14px;"/>&nbsp;
                          </xsl:otherwise>
                      </xsl:choose>
                  </xsl:if>
                  <xsl:choose>
                    <xsl:when test="order_detail_status = 'red'">
                      <img src="images/transparent-red.png" style="width:10px;height:10px;"/>&nbsp;
                    </xsl:when>
                    <xsl:when test="order_detail_status = 'yellow'">
                      <img src="images/transparent-yellow.png" style="width:10px;height:10px;"/>&nbsp;
                    </xsl:when>
                    <xsl:when test="order_detail_status = 'green'">
                      <img src="images/transparent-green.png" style="width:10px;height:10px;"/>&nbsp;
                    </xsl:when>
                  </xsl:choose>
                </xsl:when>
                <xsl:when test="order_detail_status = 'red'">
                  <xsl:if test="lifecycle_delivered_status = 'Y'">
                      <xsl:choose>
                          <xsl:when test="status_url != ''">
                              <xsl:choose>
                                  <xsl:when test="status_url_active = 'Y'">
                                      <a href="javascript:showPopup('{status_url}');">
                                          <img src="images/green-check-mark.png" style="width:14px;height:14px;border:0;"/>
                                      </a>
                                      &nbsp;
                                  </xsl:when>
                                  <xsl:otherwise>
                                      <img src="images/green-check-mark.png" style="width:14px;height:14px;">
                                      <xsl:attribute name="alt"><xsl:value-of select="normalize-space(status_url_expired_msg)"/></xsl:attribute>
                                      </img>
                                      &nbsp;
                                  </xsl:otherwise>
                              </xsl:choose>
                          </xsl:when>
                          <xsl:otherwise>
                              <img src="images/green-check-mark.png" style="width:14px;height:14px;"/>&nbsp;
                          </xsl:otherwise>
                      </xsl:choose>
                  </xsl:if>
                  <img src="images/transparent-red.png" style="width:10px;height:10px;"/>&nbsp;
                </xsl:when>
                <xsl:when test="order_detail_status = 'yellow'">
                  <xsl:if test="lifecycle_delivered_status = 'Y'">
                      <xsl:choose>
                          <xsl:when test="status_url != ''">
                              <xsl:choose>
                                  <xsl:when test="status_url_active = 'Y'">
                                      <a href="javascript:showPopup('{status_url}');">
                                          <img src="images/green-check-mark.png" style="width:14px;height:14px;border:0;"/>
                                      </a>
                                      &nbsp;
                                  </xsl:when>
                                  <xsl:otherwise>
                                      <img src="images/green-check-mark.png" style="width:14px;height:14px;">
                                      <xsl:attribute name="alt"><xsl:value-of select="normalize-space(status_url_expired_msg)"/></xsl:attribute>
                                      </img>
                                      &nbsp;
                                  </xsl:otherwise>
                              </xsl:choose>
                          </xsl:when>
                          <xsl:otherwise>
                              <img src="images/green-check-mark.png" style="width:14px;height:14px;"/>&nbsp;
                          </xsl:otherwise>
                      </xsl:choose>
                  </xsl:if>
                  <img src="images/transparent-yellow.png" style="width:10px;height:10px;"/>&nbsp;
                </xsl:when>
                <xsl:when test="order_detail_status = 'green'">
                  <xsl:if test="lifecycle_delivered_status = 'Y'">
                      <xsl:choose>
                          <xsl:when test="status_url != ''">
                              <xsl:choose>
                                  <xsl:when test="status_url_active = 'Y'">
                                      <a href="javascript:showPopup('{status_url}');">
                                          <img src="images/green-check-mark.png" style="width:14px;height:14px;border:0;"/>
                                      </a>
                                      &nbsp;
                                  </xsl:when>
                                  <xsl:otherwise>
                                      <img src="images/green-check-mark.png" style="width:14px;height:14px;">
                                          <xsl:attribute name="alt"><xsl:value-of select="normalize-space(status_url_expired_msg)"/></xsl:attribute>
                                      </img>
                                      &nbsp;
                                  </xsl:otherwise>
                              </xsl:choose>
                          </xsl:when>
                          <xsl:otherwise>
                              <img src="images/green-check-mark.png" style="width:14px;height:14px;"/>&nbsp;
                          </xsl:otherwise>
                      </xsl:choose>
                  </xsl:if>
                  <img src="images/transparent-green.png" style="width:10px;height:10px;"/>&nbsp;
                </xsl:when>
              </xsl:choose>
            </td>
                                                <xsl:choose>
                                                    <xsl:when test="scrub_status = '' and partner_name != '' ">
                                                        <td style="text-align:left;padding-left:.2em;">
                                                        <xsl:choose>
                                                    		<xsl:when test="order_modified != '' and order_modified = 'Yes' ">
                                                        		<a  href="javascript:canRepAccessOrder('{external_order_number}','{customer_id}','{partner_name}', '{partner_warning_message}', 'Clean', '{key('pageData', partner_resource)/value}');" name="external"><xsl:value-of select="external_order_number"/></a>
                                                        		&nbsp;&nbsp;
                                                        		<a href="javascript:canRepAccessOrder('{new_external_order_number}','{customer_id}','{partner_name}', '{partner_warning_message}', 'Clean', '{key('pageData', partner_resource)/value}');" name="external" title="{new_external_order_number}">MOD</a>
	                                                        </xsl:when>
	                                                    	<xsl:otherwise>
	                                                    		<a href="javascript:canRepAccessOrder('{external_order_number}','{customer_id}','{partner_name}', '{partner_warning_message}', 'Clean', '{key('pageData', partner_resource)/value}');" name="external"><xsl:value-of select="external_order_number"/></a>
	                                                        </xsl:otherwise>
                                                		</xsl:choose>
                                                        &nbsp;&nbsp;<span style="color:blue;font-weight:bold">
                                                           <xsl:value-of select="partner_display_name"/>
                                                            <xsl:if test="pc_flag=$YES">
                                                            , Premier
                                                            </xsl:if> 
                                                            </span>
                                                        </td>
                                                        <input type="hidden" name="guid_{external_order_number}" id="guid_{external_order_number}" value="{order_guid}"/>
                                                        <input type="hidden" name="rep_can_access_clean" id="rep_can_access_clean" value="{key('pageData', partner_resource)/value}"/>
                                                    </xsl:when>
                                                    <xsl:when test="scrub_status != '' and scrub_partner_display_name != '' ">
                                                        <td style="text-align:left;padding-left:.2em;"><a href="javascript:canRepAccessOrder('{external_order_number}','{customer_id}','{scrub_partner_display_name}', '{scrub_partner_warning_message}', 'Scrub', '{key('pageData', scrub_partner_resource)/value}');" name="external"><xsl:value-of select="external_order_number"/></a>
                                                         &nbsp;&nbsp; <span style="color:blue;font-weight:bold">
                                                            <xsl:value-of select="scrub_partner_display_name"/> 
                                                             <xsl:if test="pc_flag=$YES">
                                                            , Premier
                                                            </xsl:if>
                                                            </span>
                                                        </td>
                                                        <input type="hidden" name="guid_{external_order_number}" id="guid_{external_order_number}" value="{order_guid}"/>
                                                        <input type="hidden" name="rep_can_access_scrub" id="rep_can_access_scrub" value="{key('pageData', scrub_partner_resource)/value}"/>
                                                    </xsl:when>
                                                    <xsl:otherwise>
                                                        <td style="text-align:left;padding-left:.2em;">
                                                        <xsl:choose>
                                                    		<xsl:when test="order_modified != '' and order_modified = 'Yes' ">
                                                    			<a href="javascript:doAccountSearch('{external_order_number}','{customer_id}');" name="external"><xsl:value-of select="external_order_number"/></a>
                                                    			&nbsp;&nbsp;
                                                    			<a href="javascript:doAccountSearch('{new_external_order_number}','{customer_id}');" name="external" title="{new_external_order_number}">MOD</a>
                                                    		</xsl:when>
	                                                    	<xsl:otherwise>	
	                                                    		<a href="#" onclick="javascript:doAccountSearch('{external_order_number}','{customer_id}');" name="external"><xsl:value-of select="external_order_number"/></a>
                                                    		</xsl:otherwise>
                                                		</xsl:choose>	
                                                           &nbsp;&nbsp;<span style="color:blue;font-weight:bold">
                                                        
                                                         <xsl:if test="pc_flag=$YES">
                                                            Premier
                                                            </xsl:if>
                                                            </span>
                                                        </td>
                                                        <input type="hidden" name="guid_{external_order_number}" id="guid_{external_order_number}" value="{order_guid}"/>
                                                    </xsl:otherwise>
                                                </xsl:choose>
                                                      
                                                <td style="text-align:left;">
                                                    <xsl:choose>
														<xsl:when test="string-length(concat(recipient_first_name, ' ' , recipient_last_name)) > 41">
															<span style="WIDTH: 50px; VERTICAL-ALIGN: top; white-space: nowrap;">
											                	<xsl:value-of select="recipient_first_name"/><br /><xsl:value-of select="recipient_last_name"/>
											                </span>
														</xsl:when>
														<xsl:otherwise>
															<span style="WIDTH: 50px; VERTICAL-ALIGN: top; white-space: nowrap;">
											                	<xsl:value-of select="recipient_first_name"/>&nbsp;<xsl:value-of select="recipient_last_name"/>
											                </span>
														</xsl:otherwise>
													</xsl:choose>
                                                </td>
            <td><xsl:value-of select="delivery_date"/></td>
            <td><xsl:value-of select="order_date"/></td>
            <td style="text-align:right;"> <xsl:value-of select="format-number(order_total, '#,###,##0.00;-#,###,##0.00','notANumber')"/></td>
            <td style="text-align:right;"><xsl:value-of select="format-number(refund_total, '-#,###,##0.00;-#,###,##0.00','notANumber')"/></td>
            <td><xsl:value-of select="refund_disp"/></td>
          </tr>
         </xsl:for-each>
            </tbody>
      </table>
    </div>
    </td>
    </tr>
  </table>
 </xsl:template>
</xsl:stylesheet>
