<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="header.xsl"/>
	<xsl:import href="footer.xsl"/>
	<xsl:import href="securityanddata.xsl" />
	<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>

  <!-- decimal format used when a value is not given -->
  <xsl:decimal-format NaN="0.00" name="notANumber"/>

	<xsl:output method="html" indent="no"/>
	<xsl:output indent="yes"/>
	<xsl:key name="pagedata" match="/root/pageData/data" use="name" />
	<xsl:key name="lppagedata" match="root/LP_PAGEDATA/data" use="name" />
	<xsl:key name="phonetype" match="root/CAI_PHONES/CAI_PHONE" use="customer_phone_type" />
	<xsl:key name="security" match="/root/security/data" use="name"/>
		<xsl:variable name="SCRUB" select="'S'"/>
	<xsl:variable name="PENDING" select="'P'"/>
	<xsl:variable name="REMOVED" select="'R'"/>
	<xsl:variable name="YES" select="'Y'"/>
	<xsl:variable name="NO" select="'N'"/>
	<xsl:template match="/">
		<html>
			<head>
				<META http-equiv="Content-Type" content="text/html"/>
				<title>Loss Prevention</title>
				<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
				<script type="text/javascript" src="js/clock.js"></script>
      				<script type="text/javascript" src="js/commonUtil.js"></script>
      				<script type="text/javascript" src="js/util.js"></script>
        <script type="text/javascript" src="js/tabIndexMaintenance.js"/>
				<script type="text/javascript">
var pageAction = new Array("ca_first","ca_previous","ca_next","ca_last");
var lpIndExists = '<xsl:value-of select="root/LP_DATA/loss_prevention_indicator_exists"/>';
var show_first = '<xsl:value-of select="key('pagedata','cai_show_first')/value"/>';
var show_previous = '<xsl:value-of select="key('pagedata','cai_show_previous')/value"/>';
var show_next = '<xsl:value-of select="key('pagedata','cai_show_next')/value"/>';
var show_last = '<xsl:value-of select="key('pagedata','cai_show_last')/value"/>';
var prev_cust = '<xsl:value-of select="key('lppagedata','show_previous')/value"/>';
var next_cust = '<xsl:value-of select="key('lppagedata','show_next')/value"/>';
var prevguid = '<xsl:value-of select="key('lppagedata','prev_order_guid')/value"/>';
var nextguid = '<xsl:value-of select="key('lppagedata','next_order_guid')/value"/>';
var prevcustid = '<xsl:value-of select="key('lppagedata','prev_customer_id')/value"/>';
var nextcustid = '<xsl:value-of select="key('lppagedata','next_customer_id')/value"/>';
var custid = '<xsl:value-of select="key('lppagedata','customer_id')/value"/>';
var guid = '<xsl:value-of select="key('lppagedata','order_guid')/value"/>';
var curpage = '<xsl:value-of select="key('pagedata','cai_current_page')/value"/>';
var count = 0;

var siteNameSsl = '<xsl:value-of select="$sitenamessl"/>';
var applicationContext = '<xsl:value-of select="$applicationcontext"/>';


					<![CDATA[
/***************************************
init function
***********************************/
					
function init()
{
  buttondisable();

  if (document.getElementById('master') != null)
  {
  document.getElementById('master').focus();
  }
  
  if (prev_cust == 'n'|| prev_cust == 'N')
  document.getElementById('previous_cust_but').disabled = true;
  else
  document.getElementById('previous_cust_but').disabled = false;
  if (next_cust == 'n'|| next_cust == 'N')
  document.getElementById('next_cust_but').disabled = true;
  else
  document.getElementById('next_cust_but').disabled = false;
  setLossPreventionIndexes();
  
  if(lpIndExists == 'N')
  {
    showModalAlert("There is no loss prevention indicator for this shopping cart.");
  }
}

/***********************************************************************************
* checkKey()
************************************************************************************/
function checkKey()
{
  var tempKey = window.event.keyCode;
	
	//left arrow pressed
  if (window.event.altKey && tempKey == 37 && show_first == 'y')
		changePagination(0);
  
	//right arrow pressed
  if (window.event.altKey && tempKey == 39 && show_last == 'y')
		changePagination(3);

  //Down arrow pressed
  if (window.event.altKey && tempKey == 40 && show_next == 'y')
		changePagination(2);

  //Up arrow pressed
  if (window.event.altKey && tempKey == 38 && show_previous == 'y')
		changePagination(1);

}


/*******************************************************************************
*  setLossPreventionIndexes()
*  Sets tabs for Original View Page.
*  See tabIndexMaintenance.js
*******************************************************************************/
function setLossPreventionIndexes()
{
  untabAllElements();
	var begIndex = findBeginIndex();
 	var tabArray1 = new Array('customerRecordLink');
  begIndex = setTabIndexes(tabArray1, begIndex);
  // set tabs for florist lists //
 
  var tabArray2 = document.getElementsByName('shoppingCartLink');
  var tabArray3 = document.getElementsByName('externalOrderNumberLink');
	for(var x = 0; x < tabArray2.length; x++)
  {
	  var element1 = tabArray2[x];
    var element2 = tabArray3[x];
    if ((element1 != null) && (element2 != null))
    {
      element1.tabIndex = begIndex;
      begIndex++;
      element2.tabIndex = begIndex;
      begIndex++;
    }
	}
  var tabArray4 = new Array('previous_cust_but', 'next_cust_but', 'hold_but', 
  'nextItem', 'backItem','firstItem', 'lastItem', 'back_but', 'main_menu_but',
  'printer');
  begIndex = setTabIndexes(tabArray4, begIndex);
  return begIndex;
}


/***************************************
this is when u click the printer button
***********************************/
function printIt()
{
window.print();
}
/******************
Common functions
*******************/
function doLossPreventionAction()
{
document.forms[0].action = "lossPrevention.do";
document.forms[0].submit();
}

function doCustomerOrderSearchAction()
{
document.forms[0].action = "customerOrderSearch.do";
document.forms[0].submit();
}

/**************************************************/
function changePagination(val)
{
document.getElementById('customer_id').value = custid;
document.getElementById('order_guid').value = guid;
document.getElementById('rec_page_position').value = curpage;

var test = document.getElementById('cai_total_pages').value;

document.forms[0].action = "lossPrevention.do" + "?action=" + pageAction[val];
document.forms[0].submit();		
}

function setPreviousCustomer()
{
document.getElementById('customer_id').value = prevcustid;
document.getElementById('order_guid').value = prevguid;
document.getElementById('rec_page_position').value = 1;
doLossPreventionAction();
}

function setNextCustomer()
{
document.getElementById('customer_id').value = nextcustid;
document.getElementById('order_guid').value = nextguid;
document.getElementById('rec_page_position').value = 1;
doLossPreventionAction();
}

function setCustomer(guid,id)
{
document.getElementById('customer_id').value = id;
document.getElementById('order_guid').value = guid;
document.getElementById('rec_page_position').value = 1;
doLossPreventionAction();
}

function setShoppingCartOrderNum(num,id)
{
document.getElementById('shopping_cart_ord_num').value = num;
document.getElementById('customer_id').value = id;
document.forms[0].action = "customerOrderSearch.do" + "?action=customer_account_search" + "&order_number=" + document.getElementById('shopping_cart_ord_num').value + "&customer_id=" + document.getElementById('customer_id').value;
document.forms[0].submit();
}

function setOrderNum(num,id)
{
document.getElementById('order_num').value = num;
document.getElementById('customer_id').value = id;
document.forms[0].action = "customerOrderSearch.do" + "?action=customer_account_search" + "&order_number=" + document.getElementById('order_num').value + "&customer_id=" + document.getElementById('customer_id').value;
document.forms[0].submit();
}

function doHoldAction()
{
document.forms[0].action = "https://" + siteNameSsl + applicationContext + "/CustomerHoldAction.do" + "?from_page=loss_prevention" + "&action_type=loadcustomer" + "&customer_id=" + custid + "&order_guid=" + guid;;
document.forms[0].submit();
}


function buttondisable()
{
setNavigationHandlers();

if (show_first == 'n' || show_first == 'N')
document.getElementById("firstpage").disabled= "true"

if (show_previous == 'n' || show_previous == 'N')
document.getElementById("previouspage").disabled= "true";

if (show_next == 'n' || show_next == 'N')
document.getElementById("nextpage").disabled= "true";

if (show_last == 'n' || show_last == 'N')
document.getElementById("lastpage").disabled= "true";

}

/*************************************************************************************
*	Perorms the Back button functionality
**************************************************************************************/
function doBackAction()
{
	var url =  "customerOrderSearch.do?action=customer_search&customer_id=" + document.getElementById('customer_id').value + "&last_order_date=Recipient";
	performAction(url);
}

/**********************************
This function is used to wrap the customer names on different rows
************************************/
function nextRow()
{
count++;
 if (count % 5 == 0)
 {
  document.write("</tr>");
  document.write("<tr>");
  document.write('<td width="8%">&nbsp;</td>');
 }
}

function performAction(url)
{
  document.forms[0].action = url;
  document.forms[0].submit();
}


]]></script>
			</head>
<body onload="init();" onkeydown="javascript:checkKey();">
				<form name="LPScreen" method="post" action="">
					<xsl:call-template name="securityanddata"/>
					<xsl:call-template name="decisionResultData"/>
					<input type="hidden" name="cust_position" id="cust_position" value="{key('pagedata', 'cust_position')/value}"/>
					<input type="hidden" name="rec_page_position" id="rec_page_position" value=""/>
					<input type="hidden" name="page_position" id="page_position" value="{key('pagedata','page_position')/value}"/>
					<input type="hidden" name="cai_total_pages" id="cai_total_pages" value="{key('pagedata','cai_total_pages')/value}"/>
					<input type="hidden" name="order_guid" id="order_guid" value=""/>
                    <input type="hidden" name="customer_id" id="customer_id" value="{key('lppagedata','customer_id')/value}"/>
					<!-- Header-->
					<xsl:call-template name="header">
						<xsl:with-param name="headerName"  select="'Loss Prevention'" />
						<xsl:with-param name="showTime" select="true()"/>
						<xsl:with-param name="showBackButton" select="true()"/>
						<xsl:with-param name="showPrinter" select="true()"/>
						<xsl:with-param name="showSearchBox" select="false()"/>
						<xsl:with-param name="dnisNumber" select="$call_dnis"/>
						<xsl:with-param name="cservNumber" select="$call_cs_number"/>
						<xsl:with-param name="indicator" select="$call_type_flag"/>
						<xsl:with-param name="brandName" select="$call_brand_name"/>
						<xsl:with-param name="showCSRIDs" select="true()"/>
						<xsl:with-param name="backButtonLabel" select="'(B)ack to Cust Acct'"/>
						<xsl:with-param name="cdispEnabledFlag" select="$cdisp_enabledFlag"/>
					</xsl:call-template>
					<!-- Display table-->
					<table width="98%" align="center" class="mainTable">
						<tr>
							<td>
								<table width="100%" border="0" align="center" cellpadding="1" cellspacing="2" class="innerTable">
									<tr>
									<td colspan="9" class="fixedHeader"><div align="left">Customer Record: &nbsp;<a href ="#" name = "customerRecordLink" onclick="doBackAction()"><xsl:value-of select="key('lppagedata','customer_id')/value"/></a></div></td>
									</tr>
									<tr>
										<td width="51" align="left" class="Label">&nbsp;</td>
										<td width="85" align="left" class="Label">Customer:</td>
										<td colspan="4" align="left" class="fixedHeader">
											<xsl:value-of select="root/CAI_CUSTOMERS/CAI_CUSTOMER/customer_first_name"/>&nbsp;
											<xsl:value-of select="root/CAI_CUSTOMERS/CAI_CUSTOMER/customer_last_name"/>
										</td>
										<td colspan="3" align="left" class="fixedHeader">Loss Prevention Indicator</td>
									</tr>
									<tr>
										<td align="left" class="Label">&nbsp;</td>
										<td align="left" class="Label">Address 1 :</td>
										<td colspan="4" align="left" class="fixedHeader">
											<xsl:value-of select="root/CAI_CUSTOMERS/CAI_CUSTOMER/customer_address_1"/>
										</td>
										<td colspan="3" align="left" class="fixedHeader">
											<xsl:value-of select="root/LP_DATA/loss_prevention_indicator"/>
										</td>
									</tr>
									<tr>
									<td align="left" class="Label">&nbsp;</td>
									<td align="left" class="Label">Address 2 :</td>
									<td colspan="4" align="left" class="fixedHeader">
									<xsl:value-of select="root/CAI_CUSTOMERS/CAI_CUSTOMER/customer_address_2"/>
									</td>
									<td colspan="3" align="left" class="fixedHeader"></td>
									</tr>
									<tr>
										<td align="left" class="Label">&nbsp;</td>
										<td align="left" class="Label">City, State:</td>
										<td colspan="4" align="left" class="fixedHeader">
											<xsl:value-of select="root/CAI_CUSTOMERS/CAI_CUSTOMER/customer_city"/>,
											<xsl:value-of select="root/CAI_CUSTOMERS/CAI_CUSTOMER/customer_state"/>
										</td>
										<td colspan="3" align="left" class="fixedHeader">Number of Customer #'s Associated with LP indicator</td>
									</tr>
									<tr>
										<td align="left" class="Label">&nbsp;</td>
										<td align="left" class="Label">Zip/Postal:</td>
										<td width="62" align="left" class="fixedHeader">
											<xsl:value-of select="root/CAI_CUSTOMERS/CAI_CUSTOMER/customer_zip_code"/>
										</td>
										<td width="44" align="left" class="Label">Country:</td>
										<td colspan="2" align="left" class="fixedHeader">
											<xsl:value-of select="root/CAI_CUSTOMERS/CAI_CUSTOMER/customer_country"/>
										</td>
										<td colspan="3" align="left" class="fixedHeader"><xsl:value-of select="root/LP_DATA/lp_customer_count"/></td>
									</tr>
									<tr>
										<td align="left" class="Label">&nbsp;</td>
										<td align="left" class="Label">(H) Phone:</td>
										<td colspan="2" align="left" class="fixedHeader">
										<xsl:value-of select="key('phonetype','Evening')/customer_phone_number"/>
										</td>
										<td width="29" align="left" class="Label">Ext:</td>
										<td width="139" align="left" class="fixedHeader"><xsl:value-of select="key('phonetype','Evening')/customer_extension"/></td>
										<td colspan="3" align="left" class="fixedHeader">Now displaying &nbsp; <xsl:value-of select="key('lppagedata','cust_position')/value"/>&nbsp; of &nbsp;<xsl:value-of select="root/LP_DATA/lp_customer_count"/></td>
									</tr>
									<tr>
										<td align="left" class="Label">&nbsp;</td>
										<td align="left" class="Label">(W) Phone:</td>
										<td colspan="2" align="left" class="fixedHeader">
											<xsl:value-of select="key('phonetype','Day')/customer_phone_number"/>
										</td>
										<td align="left" class="Label">Ext:</td>
										<td align="left" class="fixedHeader"><xsl:value-of select="key('phonetype','Day')/customer_extension"/></td>
										<td colspan="3" align="left" class="fixedHeader">&nbsp;</td>
									</tr>
									<tr>
										<td align="left" class="Label">&nbsp;</td>
										<td align="left" class="Label">Email Address:</td>
										<td colspan="4" align="left" class="fixedHeader">
											<xsl:value-of select="root/CAI_EMAILS/CAI_EMAIL/email_address"/>
										</td>
										<td colspan="3" align="left" class="fixedHeader">&nbsp;</td>
									</tr>
									<tr>
										<td colspan="9" class="TotalLine">
											<div align="center" class="TotalLine">
												<strong>Customers Associated with LP Indicator</strong>
											</div>
										</td>
									</tr>
									<tr>
										<td colspan="9">
											<div align="justify" class="floatingdiv">

												<table width="100%"  border="0">
													<tr>
													<td width="8%">&nbsp;</td>
													<xsl:variable name="lpcust" select="key('lppagedata','customer_id')/value"/>
													<xsl:for-each select="root/LP_CUSTOMERS/LP_CUSTOMER">
													<xsl:variable name="custid" select="customer_id"/>
													<xsl:choose>
														<xsl:when test="$custid = $lpcust">
														<td width="15%" align="left">
														<a href="javascript:setCustomer('{order_guid}','{customer_id}')"  class="highlightedlink" tabindex="-1"><xsl:value-of select="customer_name"/></a>
														</td>
														</xsl:when>
														<xsl:otherwise>
														<td width="15%" align="left">
														<a href="javascript:setCustomer('{order_guid}','{customer_id}')" tabindex="-1"><xsl:value-of select="customer_name"/></a>
														</td>
														</xsl:otherwise>
														</xsl:choose>
													<script>
													nextRow() </script>
													</xsl:for-each>
													</tr>
												</table>
											</div>
										</td>
									</tr>
									<tr>
										<td colspan="9" class="TotalLine">
											<div align="center">
												<strong>Recipient Information</strong>
											</div>
										</td>
									</tr>
									<tr class="Label">
										<td colspan="9">
											<table width="100%"  border="0" cellspacing="0">
												<tr class="Label">
													<td width="12%">
														<div align="center">Cart Number </div>
													</td>
													<td width="8%">
													    <div align="center">Pre-Auth</div>
													</td>
													<td width="15%">
													    <div align="center">Order Number</div>
													</td>
													<td width="19%">
														<div align="center">Recipient Name </div>
													</td>
													<td width="11%">
														<div align="center">Order Date </div>
													</td>
													<td width="8%">
														<div align="center">Total MSD</div>
													</td>
													<td width="8%">
														<div align="center">Refund</div>
													</td>
													<td width="9%">
														<div align="center">Refund&nbsp;&nbsp;Code</div>
													</td>
													<td width="10%">
														<div align="center">Total Amount</div>
													</td>
												</tr>
											</table>
											<div align="justify" class="floatingdiv">
												<table width="100%"  border="0" cellspacing="0">
													<xsl:variable name="lpindnew" select="root/LP_DATA/loss_prevention_indicator"/>
													<xsl:for-each select="root/CAI_ORDERS/CAI_ORDER">
														<xsl:variable name="lpind" select="loss_prevention_indicator"/>
														<xsl:choose>
															<xsl:when test="$lpindnew != '' and $lpind = $lpindnew">
																<tr bgcolor="silver">
																	<td width="12%" id="master" colspan="2" align="center" >
																		<a name = "shoppingCartLink" href="javascript:setShoppingCartOrderNum('{master_order_number}','{customer_id}')">
																		<xsl:value-of select="master_order_number"/>
																		<input type="hidden" name="shopping_cart_ord_num" id="shopping_cart_ord_num" value=""/>
																		</a>
																	</td>
																	<td width="8%" align="center">
																		<xsl:value-of select="cardinal_verified"/>
																	</td>
																	<td width="15%" id="external">
																		<table align="center">
																			<tr>
																				<td width="30%" align="right">
															    					<xsl:choose>
								                                   						<xsl:when test="scrub_status = $SCRUB">
																							<img src="images/cancel.gif"/>&nbsp;
																						</xsl:when>
																						<xsl:when test="scrub_status = $PENDING">
																							<img src="images/warning.gif"/>&nbsp;
																						</xsl:when>
																						<xsl:when test="scrub_status = $REMOVED">
																							<img src="images/removed.gif"/>&nbsp;
																						</xsl:when>
																						<xsl:when test="order_hold_indicator = $YES">
																							<img src="images/IconLock.gif"/>&nbsp;
                  																			<xsl:if test="lifecycle_delivered_status = 'Y'">
                      																			<xsl:choose>
                          																			<xsl:when test="status_url != ''">
                              																			<xsl:choose>
                                  																			<xsl:when test="status_url_active = 'Y'">
                                      																			<a href="javascript:showPopup('{status_url}');">
                                          																		<img src="images/green-check-mark.png" style="width:14px;height:14px;border:0;"/>
                                      																			</a>
                                      																			&nbsp;
                                  																			</xsl:when>
                                  																			<xsl:otherwise>
                                      																			<img src="images/green-check-mark.png" style="width:14px;height:14px;">
                                      																				<xsl:attribute name="alt"><xsl:value-of select="normalize-space(status_url_expired_msg)"/></xsl:attribute>
                                      																			</img>
                                      																			&nbsp;
                                  																			</xsl:otherwise>
                              																			</xsl:choose>
                          																			</xsl:when>
                          																			<xsl:otherwise>
                              																			<img src="images/green-check-mark.png" style="width:14px;height:14px;"/>&nbsp;
                          																			</xsl:otherwise>
                      																			</xsl:choose>
                  																			</xsl:if>
                  																			<xsl:choose>
                    																			<xsl:when test="order_detail_status = 'red'">
                      																				<img src="images/transparent-red.png" style="width:10px;height:10px;"/>&nbsp;
                    																			</xsl:when>
                    																			<xsl:when test="order_detail_status = 'yellow'">
                      																				<img src="images/transparent-yellow.png" style="width:10px;height:10px;"/>&nbsp;
                    																			</xsl:when>
                    																			<xsl:when test="order_detail_status = 'green'">
                      																				<img src="images/transparent-green.png" style="width:10px;height:10px;"/>&nbsp;
                    																			</xsl:when>
                  																			</xsl:choose>
                																		</xsl:when>
                																		<xsl:when test="order_detail_status = 'red'">
                  																			<xsl:if test="lifecycle_delivered_status = 'Y'">
                      																			<xsl:choose>
                          																			<xsl:when test="status_url != ''">
                              																			<xsl:choose>
                                  																			<xsl:when test="status_url_active = 'Y'">
                                      																			<a href="javascript:showPopup('{status_url}');">
                                          																			<img src="images/green-check-mark.png" style="width:14px;height:14px;border:0;"/>
                                      																			</a>
                                      																			&nbsp;
                                  																			</xsl:when>
                                  																			<xsl:otherwise>
                                      																			<img src="images/green-check-mark.png" style="width:14px;height:14px;">
                                      																				<xsl:attribute name="alt"><xsl:value-of select="normalize-space(status_url_expired_msg)"/></xsl:attribute>
                                      																			</img>
                                      																			&nbsp;
                                  																			</xsl:otherwise>
                              																			</xsl:choose>
                          																			</xsl:when>
                          																			<xsl:otherwise>
                              																			<img src="images/green-check-mark.png" style="width:14px;height:14px;"/>&nbsp;
                          																			</xsl:otherwise>
                      																			</xsl:choose>
                  																			</xsl:if>
                  																			<img src="images/transparent-red.png" style="width:10px;height:10px;"/>&nbsp;
                																		</xsl:when>
                																		<xsl:when test="order_detail_status = 'yellow'">
                  																			<xsl:if test="lifecycle_delivered_status = 'Y'">
                      																			<xsl:choose>
                          																			<xsl:when test="status_url != ''">
                              																			<xsl:choose>
                                  																			<xsl:when test="status_url_active = 'Y'">
                                      																			<a href="javascript:showPopup('{status_url}');">
                                          																			<img src="images/green-check-mark.png" style="width:14px;height:14px;border:0;"/>
                                      																			</a>
                                      																			&nbsp;
                                  																			</xsl:when>
                                  																			<xsl:otherwise>
                                      																			<img src="images/green-check-mark.png" style="width:14px;height:14px;">
                                      																				<xsl:attribute name="alt"><xsl:value-of select="normalize-space(status_url_expired_msg)"/></xsl:attribute>
                                      																			</img>
                                      																			&nbsp;
                                  																			</xsl:otherwise>
                              																			</xsl:choose>
                          																			</xsl:when>
                          																			<xsl:otherwise>
                              																			<img src="images/green-check-mark.png" style="width:14px;height:14px;"/>&nbsp;
                          																			</xsl:otherwise>
                      																			</xsl:choose>
                  																			</xsl:if>
                  																			<img src="images/transparent-yellow.png" style="width:10px;height:10px;"/>&nbsp;
                																		</xsl:when>
                																		<xsl:when test="order_detail_status = 'green'">
                  																			<xsl:if test="lifecycle_delivered_status = 'Y'">
                      																			<xsl:choose>
                          																			<xsl:when test="status_url != ''">
                              																			<xsl:choose>
                                  																			<xsl:when test="status_url_active = 'Y'">
                                      																			<a href="javascript:showPopup('{status_url}');">
                                          																			<img src="images/green-check-mark.png" style="width:14px;height:14px;border:0;"/>
                                      																			</a>
                                      																			&nbsp;
                                  																			</xsl:when>
                                  																			<xsl:otherwise>
                                      																			<img src="images/green-check-mark.png" style="width:14px;height:14px;">
                                          																			<xsl:attribute name="alt"><xsl:value-of select="normalize-space(status_url_expired_msg)"/></xsl:attribute>
                                      																			</img>
                                      																			&nbsp;
                                  																			</xsl:otherwise>
                              																			</xsl:choose>
                          																			</xsl:when>
                          																			<xsl:otherwise>
                              																			<img src="images/green-check-mark.png" style="width:14px;height:14px;"/>&nbsp;
                          																			</xsl:otherwise>
                      																			</xsl:choose>
                  																			</xsl:if>
                  																			<img src="images/transparent-green.png" style="width:10px;height:10px;"/>&nbsp;
                																		</xsl:when>
              																		</xsl:choose>
              																	</td>
           
																				<td>
																     				<a name = "externalOrderNumberLink" href="javascript:setOrderNum('{external_order_number}','{customer_id}')">
																						<xsl:value-of select="external_order_number"/>
																						<input type="hidden" name="order_num" id="order_num" value=""/>
																					</a>
																   				</td>
																   			</tr>
																   		</table>																 
																	</td>
																	<td width="20%">
																		<div align="center">
																			<xsl:value-of select="recipient_first_name"/>&nbsp;
																			<xsl:value-of select="recipient_last_name"/>
																		</div>
																	</td>
																	<td width="11%">
																		<div align="center">
																			<xsl:value-of select="order_date"/>
																		</div>
																	</td>
																	<td width="8%">
																		<div align="center">
																			<xsl:value-of select="format-number(msd_total, '#,###,##0.00;(#,###,##0.00)','notANumber')"/>
																		</div>
																	</td>
																	<td width="8%">
																		<div align="center">
																			<xsl:value-of select="format-number(refund_total, '#,###,##0.00;(#,###,##0.00)','notANumber')"/>
																		</div>
																	</td>
																	<td width="9%">
																		<div align="center">
																			<xsl:value-of select="lp_refund_code"/>
																		</div>
																	</td>
																	<td width="9%">
																		<div align="center">
																			<xsl:value-of select="format-number(order_total, '#,###,##0.00;(#,###,##0.00)','notANumber')"/>
																		</div>
																	</td>
																</tr>
															</xsl:when>
															<xsl:otherwise>
																<tr>														    
																	<td width="12%" id="master"  colspan="2"  align="center">
																		<a href="javascript:setShoppingCartOrderNum('{master_order_number}','{customer_id}')">
																			<xsl:value-of select="master_order_number"/>
																			<input type="hidden" name="shopping_cart_ord_num" id="shopping_cart_ord_num" value=""/>
																		</a>
																	</td>
																	<td width="8%" align="center">
																		<xsl:value-of select="cardinal_verified"/>
																	</td>
																	<td width="15%" id="external">
																		<table align="center">
															   				<tr>
															    				<td width="30%" align="right">
																    				<xsl:choose>
																						<xsl:when test="scrub_status = $SCRUB">
																							<img src="images/cancel.gif"/>&nbsp;
																						</xsl:when>
																						<xsl:when test="scrub_status = $PENDING">
																							<img src="images/warning.gif"/>&nbsp;
																						</xsl:when>
																						<xsl:when test="scrub_status = $REMOVED">
																							<img src="images/removed.gif"/>&nbsp;
																						</xsl:when>
																						<xsl:when test="order_hold_indicator = $YES">
																							<img src="images/IconLock.gif"/>&nbsp;
																		 					<xsl:if test="lifecycle_delivered_status = 'Y'">
                      																			<xsl:choose>
                          																			<xsl:when test="status_url != ''">
                              																			<xsl:choose>
                                  																			<xsl:when test="status_url_active = 'Y'">
                                      																			<a href="javascript:showPopup('{status_url}');">
                                          																			<img src="images/green-check-mark.png" style="width:14px;height:14px;border:0;"/>
                                      																			</a>
                                      																			&nbsp;
                                  																			</xsl:when>
                                  																			<xsl:otherwise>
                                      																			<img src="images/green-check-mark.png" style="width:14px;height:14px;">
                                      																				<xsl:attribute name="alt"><xsl:value-of select="normalize-space(status_url_expired_msg)"/></xsl:attribute>
                                      																			</img>
                                      																			&nbsp;
                                  																			</xsl:otherwise>
                              																			</xsl:choose>
                          																			</xsl:when>
                          																			<xsl:otherwise>
                              																			<img src="images/green-check-mark.png" style="width:14px;height:14px;"/>&nbsp;
                          																			</xsl:otherwise>
                      																			</xsl:choose>
                  																			</xsl:if>
                  																			<xsl:choose>
                    																			<xsl:when test="order_detail_status = 'red'">
                      																				<img src="images/transparent-red.png" style="width:10px;height:10px;"/>&nbsp;
                    																			</xsl:when>
                    																			<xsl:when test="order_detail_status = 'yellow'">
                      																				<img src="images/transparent-yellow.png" style="width:10px;height:10px;"/>&nbsp;
                    																			</xsl:when>
                    																			<xsl:when test="order_detail_status = 'green'">
                      																				<img src="images/transparent-green.png" style="width:10px;height:10px;"/>&nbsp;
                    																			</xsl:when>
                  																			</xsl:choose>
                																		</xsl:when>
                																		<xsl:when test="order_detail_status = 'red'">
                  																			<xsl:if test="lifecycle_delivered_status = 'Y'">
                      																			<xsl:choose>
                          																			<xsl:when test="status_url != ''">
                              																			<xsl:choose>
                                  																			<xsl:when test="status_url_active = 'Y'">
                                      																			<a href="javascript:showPopup('{status_url}');">
                                          																			<img src="images/green-check-mark.png" style="width:14px;height:14px;border:0;"/>
                                      																			</a>
                                      																			&nbsp;
                                  																			</xsl:when>
                                  																			<xsl:otherwise>
                                      																			<img src="images/green-check-mark.png" style="width:14px;height:14px;">
                                      																				<xsl:attribute name="alt"><xsl:value-of select="normalize-space(status_url_expired_msg)"/></xsl:attribute>
                                      																			</img>
                                      																			&nbsp;
                                  																			</xsl:otherwise>
                              																			</xsl:choose>
                          																			</xsl:when>
                          																			<xsl:otherwise>
                              																			<img src="images/green-check-mark.png" style="width:14px;height:14px;"/>&nbsp;
                          																			</xsl:otherwise>
                      																			</xsl:choose>
                  																			</xsl:if>
                  																			<img src="images/transparent-red.png" style="width:10px;height:10px;"/>&nbsp;
                																		</xsl:when>
                																		<xsl:when test="order_detail_status = 'yellow'">
                  																			<xsl:if test="lifecycle_delivered_status = 'Y'">
                      																			<xsl:choose>
                          																			<xsl:when test="status_url != ''">
                              																			<xsl:choose>
                                  																			<xsl:when test="status_url_active = 'Y'">
                                      																			<a href="javascript:showPopup('{status_url}');">
                                          																			<img src="images/green-check-mark.png" style="width:14px;height:14px;border:0;"/>
                                      																			</a>
                                      																			&nbsp;
                                  																			</xsl:when>
                                  																			<xsl:otherwise>
                                      																			<img src="images/green-check-mark.png" style="width:14px;height:14px;">
                                      																				<xsl:attribute name="alt"><xsl:value-of select="normalize-space(status_url_expired_msg)"/></xsl:attribute>
                                      																			</img>
                                      																			&nbsp;
                                  																			</xsl:otherwise>
                              																			</xsl:choose>
                          																			</xsl:when>
                          																			<xsl:otherwise>
                              																			<img src="images/green-check-mark.png" style="width:14px;height:14px;"/>&nbsp;
                          																			</xsl:otherwise>
                      																			</xsl:choose>
                  																			</xsl:if>
                  																			<img src="images/transparent-yellow.png" style="width:10px;height:10px;"/>&nbsp;
                																		</xsl:when>
                																		<xsl:when test="order_detail_status = 'green'">
                  																			<xsl:if test="lifecycle_delivered_status = 'Y'">
                      																			<xsl:choose>
                          																			<xsl:when test="status_url != ''">
                              																			<xsl:choose>
                                  																			<xsl:when test="status_url_active = 'Y'">
                                      																			<a href="javascript:showPopup('{status_url}');">
                                          																			<img src="images/green-check-mark.png" style="width:14px;height:14px;border:0;"/>
                                      																			</a>
                                      																			&nbsp;
                                  																			</xsl:when>
                                  																			<xsl:otherwise>
                                      																			<img src="images/green-check-mark.png" style="width:14px;height:14px;">
                                          																			<xsl:attribute name="alt"><xsl:value-of select="normalize-space(status_url_expired_msg)"/></xsl:attribute>
                                      																			</img>
                                      																			&nbsp;
                                  																			</xsl:otherwise>
                              																			</xsl:choose>
                          																			</xsl:when>
                          																			<xsl:otherwise>
                              																			<img src="images/green-check-mark.png" style="width:14px;height:14px;"/>&nbsp;
                          																			</xsl:otherwise>
                      																			</xsl:choose>
                  																			</xsl:if>
                  																			<img src="images/transparent-green.png" style="width:10px;height:10px;"/>&nbsp;
                																		</xsl:when>
              																		</xsl:choose>
              																	</td>
																   				<td width="70%">
																     				<div align="center">
																 	   					<a href="javascript:setOrderNum('{external_order_number}','{customer_id}')">
																							<xsl:value-of select="external_order_number"/>
																							<input type="hidden" name="order_num" id="order_num" value=""/>
																	   					</a>
																      				</div>
															       				</td>
															        		</tr>
																		</table>
																	</td>
																	<td width="20%">
																		<div align="center">
																			<xsl:value-of select="recipient_first_name"/>&nbsp;
																			<xsl:value-of select="recipient_last_name"/>
																		</div>
																	</td>
																	<td width="11%">
																		<div align="center">
																			<xsl:value-of select="order_date"/>
																		</div>
																	</td>
																	<td width="8%">
																		<div align="center">
																			<xsl:value-of select="format-number(msd_total, '#,###,##0.00;(#,###,##0.00)','notANumber')"/>
																		</div>
																	</td>
																	<td width="8%">
																		<div align="center">
																			<xsl:value-of select="format-number(refund_total, '#,###,##0.00;(#,###,##0.00)','notANumber')"/>
																		</div>
																	</td>
																	<td width="9%">
																		<div align="center">
																			<xsl:value-of select="lp_refund_code"/>
																		</div>
																	</td>
																	<td width="9%">
																		<div align="center">
																			<xsl:value-of select="format-number(order_total, '#,###,##0.00;(#,###,##0.00)','notANumber')"/>
																		</div>
																	</td>
																</tr>
															</xsl:otherwise>
														</xsl:choose>
													</xsl:for-each>
												</table>
											</div>
										</td>
									</tr>
									<tr class="fixedHeader">
										<td colspan="7">&nbsp;</td>
										<td width="257" colspan="2">
											<div align="right">
												<span class="LabelRight"> Page
													<xsl:for-each select="key('pagedata', 'cai_current_page')">
														<xsl:value-of select="value" />
													</xsl:for-each>
														&nbsp;of&nbsp;
													<xsl:for-each select="key('pagedata', 'cai_total_pages')">
														<xsl:if test="value = '0'">
   															1
														</xsl:if>
														<xsl:if test="value != '0'">
															<xsl:value-of select="value" />
														</xsl:if>
													</xsl:for-each>
												</span>
											</div>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					<!--buttons-->
					<table width="98%" border="0" align="center" cellpadding="0" cellspacing="1">
						<tr>
							<td width="75%" rowspan="3" align="right" valign="middle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<button type="button" style="width:100" name="previous_cust_but" class="bigBlueButton" accesskey="P" onclick="setPreviousCustomer()">(P)revious
									<br/>Customer
								</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<button type="button" style="width:100" name="next_cust_but" class="bigBlueButton" accesskey="N" onclick="setNextCustomer()">(N)ext
									<br/>Customer
								</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<button type="button" style="vertical-align:top; width:90" name="hold_but" class="bigBlueButton" accesskey="H" onclick="doHoldAction()">(H)old</button>
							</td>
							<td width="25%" height="21" align="right">
								<xsl:choose>
	                              							<xsl:when test="key('pagedata','cai_total_pages')/value !='0'">
	                              								<button type="button" name="firstItem" class="arrowButton"   id="firstpage" onclick="changePagination(0);">
		                              								7
	                         									</button>
	                              							</xsl:when>
	                          								<xsl:when test="key('pagedata','cai_total_pages')/value ='0'">
	                              								<button type="button" name="firstItem" class="arrowButton"  id="firstpage" onclick="">
	                        										7
	                              								</button>
	                              							</xsl:when>
                              							</xsl:choose>
                             							<xsl:choose>
						                              		<xsl:when test="key('pagedata','cai_total_pages')/value !='0'">
								                              	<button type="button" name="backItem" class="arrowButton" id="previouspage" onclick="changePagination(1);">
									                              	3
								                              	</button>
						                              		</xsl:when>
						                              		<xsl:when test="key('pagedata','cai_total_pages')/value ='0'">
								                              	<button type="button" name="backItem" class="arrowButton" id="previouspage" onclick="">
									                              	3
								                              	</button>
						                              		</xsl:when>
						                              		</xsl:choose>
                            								<xsl:choose>
								                              	<xsl:when test="key('pagedata','cai_total_pages')/value !='0'">
								                              		<button type="button" name="nextItem" class="arrowButton" id="nextpage" onclick="changePagination(2);">
									                              		4
								                              		</button>
							                              		</xsl:when>
								                              	<xsl:when test="key('pagedata','cai_total_pages')/value ='0'">
								                              		<button type="button" name="nextItem" class="arrowButton" id="nextpage" onclick="">
									                              		4
								                              		</button>
								                              	</xsl:when>
                              								</xsl:choose>
                             								<xsl:choose>
								                              	<xsl:when test="key('pagedata','cai_total_pages')/value !='0'">
								                              		<button type="button" name="lastItem" class="arrowButton" id="lastpage" onclick="changePagination(3);">
									                              		8
								                              		</button>
						                              			</xsl:when>
								                              	<xsl:when test="key('pagedata','cai_total_pages')/value ='0'">
								                              		<button type="button" name="lastItem" class="arrowButton" id="lastpage" onclick="">
									                              		8
								                              		</button>
								                              	</xsl:when>
								                              	</xsl:choose>
							</td>
						</tr>
						<tr>
							<td align="right">
								<button type="button" class="blueButton" name="back_but" onclick="doBackAction();">(B)ack to Cust Acct</button>
							</td>
						</tr>
						<tr>
							<td align="right">
								<button type="button" class="blueButton" name="main_menu_but" onclick="doMainMenuAction();" style="width:75px">(M)ain Menu</button>
							</td>
						</tr>
					</table>
				</form>
				<!--Copyright bar-->
				<xsl:call-template name="footer"/>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>
