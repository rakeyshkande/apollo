<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>
<!--
Notes:

This xsl is used on the billingReview.xsl page.
When the user clicks the Submit Billing button after the lock is checked
we call billingReview.do and this xsl is then transformed.
If the status = successful we finish submitting our page otherwise
we display the appropriate error message and/or pop-ups.

-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="securityanddata.xsl"/>
<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>
<xsl:key name="security" match="/root/security/data" use="name"/>

<xsl:output method="html" indent="yes"/>
<xsl:template match="/root">
<html>
<head>
<script type="text/javascript" language="javascript">
var status = '<xsl:value-of select="status/data/message_status"/>';
var msgText = "<xsl:value-of select="status/data/message_text"/>";
var msgReason = "<xsl:value-of select="status/data/message_action"/>";
var isError = "<xsl:value-of select="status/data/message_action"/>";

<![CDATA[
	function init()
	{    
       if(isError == 'Y'){
          parent.doCallErrorPage();
         return;
       }
       if(status == 'success')
       {
        
	      parent.deliveryConfirmationAction('send_message');
       }
       if(status == 'declined'){
          parent.invokeDeclinePopup(msgReason,msgText);
       }
      

	}
]]>
</script>
</head>
<body onload="javascript:init();">
    <xsl:call-template name="securityanddata"/>
    <xsl:call-template name="decisionResultData"/>
</body>
</html>
</xsl:template>
</xsl:stylesheet>