<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="header.xsl"/>
	<xsl:import href="footer.xsl"/>
	<xsl:import href="securityanddata.xsl" />
	<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>

<!-- Keys -->
<xsl:key name="security" match="/root/security/data" use="name"/>
<xsl:key name="pageData" match="/root/pageData/data" use="name"/>
<xsl:key name="searchCriteria" match="/root/searchCriteria/criteria" use="name"/>

<xsl:template match="/">
<html>
   <head>
      <META http-equiv="Content-Type" content="text/html"></META>
      <title>Customer Hold</title>
	  <link rel="stylesheet" type="text/css" href="css/ftd.css"/>
	  <link rel="stylesheet" type="text/css" href="css/calendar.css"/>
	  <script type="text/javascript" src="js/util.js"></script>
	  <script type="text/javascript" src="js/commonutil.js"></script>
	  <script type="text/javascript" src="js/calendar.js"></script>
	  <script type="text/javascript" src="js/clock.js"></script>
	  <script type="text/javascript" src="js/commonUtil.js"></script>
	  <script type="text/javascript" src="js/FormChek.js"></script>
    <script type="text/javascript" src="js/mercuryMessage.js"></script>
	<script type="text/javascript" src="js/copyright.js"></script>
	
	  <script>
    
    var siteName = '<xsl:value-of select="$sitename"/>';
    var applicationContext = '<xsl:value-of select="$applicationcontext"/>';

<![CDATA[
	
		
	

	  		function isChecked()
	  		{

	  			if(document.getElementById("cust_num_hold_chk").checked)
	  			{
	  				return true;
	  			}

	  			if(document.getElementById("cust_addr_hold_chk").checked)
	  			{
	  				return true;
	  			}

	  			if(document.getElementById("cust_name_hold_chk").checked)
	  			{
	  				return true;
	  			}

	  			var i = 0
	  			var obj = null;

	  			i = 1;
	  			obj =document.getElementById("cust_card_value" + i);
	  			while(obj != null && obj.value.length > 0)
	  			{
	  			
	  				if(document.getElementById("cust_card_hold_chk" + i).checked)
	  				{
	  			
	  					return true;
	  				}
	  				i++;
		  			obj =document.getElementById("cust_card_value" + i);
	  			}


	  			i = 1;
	  			obj =document.getElementById("cust_phone_value" + i);
	  			while(obj != null && obj.value.length > 0)
	  			{

	  				if(document.getElementById("cust_phone_hold_chk" + i).checked)
	  				{

	  					return true;
	  				}

	  				i++;
	  				obj =document.getElementById("cust_phone_value" + i);
	  			}


	  			i = 1;
	  			obj =document.getElementById("cust_email_value" + i);
	  			while(obj != null && obj.value.length > 0)
	  			{

	  				if(document.getElementById("cust_email_hold_chk" + i).checked)
	  				{

	  					return true;
	  				}
	  				i++;
   	  			    obj =document.getElementById("cust_email_value" + i);
	  			}

	  			i = 1;
	  			obj =document.getElementById("cust_lp_value" + i);
	  			while(obj != null && obj.value.length > 0)
	  			{
	  				if(document.getElementById("cust_lp_hold_chk" + i).checked)
	  				{
	  					return true;
	  				}
	  				i++;
  	  			    obj =document.getElementById("cust_lp_value" + i);
	  			}

	  		}


		function doSave(){


			if(isChecked())
				{ 

					var holdReason = document.getElementById("holdreason").value;

					if(holdReason.length == 0)
					{
						alert("Please select a hold reason.");
						return;
					}
				}
				else
				{
					
				}

				if(isLocked())
				{
					return;
				}


				var url = "CustomerHoldAction.do" +
						   "?action_type=savecustomer"  + getSecurityParams(false);
				performAction(url);
			}

			function hasChanged()
			{
			  if(document.getElementById("changedflag").value == "Y")
			  {
				  return true;
		      }
		      else
		      {
		          return false;
		      }

			}

			function changed()
			{ 
			  document.getElementById("changedflag").value = "Y";
			  
			  obj = document.getElementById("save").disabled = false;
			  
			}
			
			function ddChanaged(){
				if(isChecked()){
					changed()
				}
			}
			

			function doReleaseAction(){

				if(isLocked())
				{
					return;
				}

				if(!hasChanged() ||	doExitPageAction("Are you sure you want to leave without saving changes?"))
				{
					clearCreditCardFields();
          var url = "http://" + siteName + applicationContext + "/CustomerHoldAction.do" +
							   "?action_type=releaseorders"  + getSecurityParams(false);
					performAction(url);
				}


			}

			function doCancelAction(){
        // disable relevant buttons to prevent double clicking//
        if (document.getElementById('cancelAllHeldOrdersButton'))
          document.getElementById('cancelAllHeldOrdersButton').disabled = true;
        if (document.getElementById('cancelHeldOrderButton'))
          document.getElementById('cancelHeldOrderButton').disabled = true;
        if (document.getElementById('cancelHeldCartButton'))
          document.getElementById('cancelHeldCartButton').disabled = true;

				if(isLocked())
				{
          // reenable relevant buttons //
          if (document.getElementById('cancelAllHeldOrdersButton'))
            document.getElementById('cancelAllHeldOrdersButton').disabled = false;
          if (document.getElementById('cancelHeldOrderButton'))
            document.getElementById('cancelHeldOrderButton').disabled = false;
          if (document.getElementById('cancelHeldCartButton'))
            document.getElementById('cancelHeldCartButton').disabled = false;
					return;
				}

				if(!hasChanged() ||	doExitPageAction("Are you sure you want to leave without saving changes?"))
				{
					clearCreditCardFields();
          var url = "http://" + siteName + applicationContext + "/CustomerHoldAction.do" +
							   "?action_type=cancelorders"  + getSecurityParams(false);
					performAction(url);
				}
        else
        {
          // reenable relevant buttons //
          if (document.getElementById('cancelAllHeldOrdersButton'))
            document.getElementById('cancelAllHeldOrdersButton').disabled = false;
          if (document.getElementById('cancelHeldOrderButton'))
            document.getElementById('cancelHeldOrderButton').disabled = false;
          if (document.getElementById('cancelHeldCartButton'))
            document.getElementById('cancelHeldCartButton').disabled = false;
        }
			}

		 function executeSearch(){

				if(!hasChanged() ||	doExitPageAction("Are you sure you want to leave without saving changes?"))
				{
          clearCreditCardFields();
					var url = "http://" + siteName + applicationContext + "/CustomerHoldAction.do" +
							   "?action_type=search_action"  + getSecurityParams(false);

					performAction(url);
				   // doSearchAction();

				}
				}


		 function executeMenu(){

				if(!hasChanged() ||	doExitPageAction("Are you sure you want to leave without saving changes?"))
				{

					clearCreditCardFields();
          doMainMenuAction(siteName, applicationContext);

				}
     }


		 function executeCustomerAccount(){


				if(!hasChanged() ||	doExitPageAction("Are you sure you want to leave without saving changes?"))
				{
          clearCreditCardFields();
					var url = "http://" + siteName + applicationContext + "/CustomerHoldAction.do" +
							   "?action_type=goto_customer_account"  + getSecurityParams(false);

					performAction(url);

				}
				}

		 function executeLossPrevention()
     {
    		if(!hasChanged() ||	doExitPageAction("Are you sure you want to leave without saving changes?"))
				{
      			clearCreditCardFields();
            var url = "http://" + siteName + applicationContext + "/CustomerHoldAction.do" +
							   "?action_type=goto_loss_prevention&order_guid=" + document.getElementById("order_guid").value  + getSecurityParams(false);

				    performAction(url);

				}
		 }
		 function executeNoLossPrevention()
     {
        showModalAlert("There is no loss prevention indicator for this shopping cart.");
		 }
		 
		 function executeBackToOrder(){

				if(!hasChanged() ||	doExitPageAction("Are you sure you want to leave without saving changes?"))
				{
					clearCreditCardFields();
          var url = "http://" + siteName + applicationContext + "/customerOrderSearch.do?action=customer_account_search&order_number=" + document.getElementById("order_number").value + getSecurityParams(false);
					performAction(url);
				}
		 }
     
     function clearCreditCardFields(){
      var i = 1;
      var obj = document.getElementById("cust_card_value" + i);
      while(obj != null && obj.value.length > 0)
      {
       obj.value = "";
       i++;
       obj = document.getElementById("cust_card_value" + i);
      }
     }

]]>
		/*************************************************************************************
		*	Perorms the Back button functionality
		**************************************************************************************/
		function doBackAction()
		{
			
			<xsl:choose>
			<xsl:when test="//root/pagedata/from_page = 'order_detail'">
			
			executeBackToOrder();
		  	
		  	</xsl:when>
		  	<xsl:otherwise>
			
			executeCustomerAccount();
			
			</xsl:otherwise>
			</xsl:choose>
		}

		/*
		Using the specified URL submit the form
		*/
		function performAction(url)
		{
			document.forms[0].action = url;
			document.forms[0].submit();
		}

		function isLocked()
		{

		   var lockvalue = document.getElementById("lockobtained").value;

		   if(lockvalue == 'N'){
		   	   alert("Another user is editing this record.  Please try again later.");

		   	  return true;
		   	  }
		   	else
		   	{

		   	return false;
		   	}
		}


	</script>

   </head>
	<body>
	<form name="customerHold" method="post"  >
	<xsl:call-template name="securityanddata"/>
	<xsl:call-template name="decisionResultData"/>
	
	<input TYPE="HIDDEN" name="loss_prevention_ind" id="loss_prevention_ind" value='{//root/pagedata/loss_prevention_ind}'></input>
	<input TYPE="HIDDEN" name="from_page" id="from_page" value='{//root/pagedata/from_page}'></input>
	<input TYPE="HIDDEN" name="customer_id" id="customer_id" value='{//root/pagedata/customer_id}'></input>
	<input TYPE="HIDDEN" name="order_detail_id" id="order_detail_id" value='{//root/pagedata/order_detail_id}'></input>
	<input TYPE="HIDDEN" name="order_number" id="order_number" value='{//root/pagedata/external_order_number}'></input>
  <input TYPE="HIDDEN" name="external_order_number" id="external_order_number" value='{//root/pagedata/external_order_number}'></input>
	<input TYPE="HIDDEN" name="order_guid" id="order_guid" value='{//root/pagedata/order_guid}'></input>
	<input TYPE="HIDDEN" name="lockobtained" id="lockobtained" value='{//OUT_LOCK_OBTAINED}'></input>

	<input TYPE="HIDDEN" name="recipient_search" id="recipient_search" value='{//root/pagedata/recipient_search}'></input>

	<input TYPE="HIDDEN" name="lockedby" id="lockedby" value='{//OUT_LOCKED_CSR_ID}'></input>	
	<input TYPE="HIDDEN" name="master_order_number" id="master_order_number" value='{//root/pagedata/master_order_number}'></input>
    <input TYPE="HIDDEN" name="changedflag" id="changedflag" value='N'></input>
    <input TYPE="HIDDEN" name="updateAllowed" id="updateAllowed" value='{//root/pagedata/updateAllowed}'></input>

<xsl:call-template name="addHeader"/>

<!-- Display table-->


   <table width="98%" align="center" cellspacing="1" class="mainTable">
      <tr>
        <td><table width="100%" align="center" class="innerTable" >

          <tr>




        					        </tr>
							  	    <tr>
										<td align="center" height="10"></td>
									</tr>
									<tr>
										<td align="center">

										<table width="100%" border="0" align="center" cellpadding="2" cellspacing="0">
										<tr>
										<td valign="top" align="center" width="50%">
											<div style="overflow:auto; width:75%; height=400; border-left: 2px solid #000; border-right: 2px solid #000; border-bottom: 2px solid #000;border-top: 2px solid #000;">
											<table width="100%" border="0" align="center" cellpadding="2" cellspacing="0">

<xsl:choose>
<xsl:when test="root/out-parameters/OUT_LOCK_OBTAINED = 'N'">
<tr><td colspan="3" class="ErrorMessage">Locked by <xsl:value-of select="root/out-parameters/OUT_LOCKED_CSR_ID"/></td></tr>
</xsl:when>
<xsl:when test="string-length(//error_message) > 0">
<tr><td colspan="3" class="ErrorMessage"><xsl:value-of select="//error_message"/></td></tr>
</xsl:when>
</xsl:choose>
		<xsl:choose>
		<xsl:when test="root/pagedata/from_page = 'customer_account'">

  	    </xsl:when>
		<xsl:when test="root/pagedata/from_page = 'loss_prevention'">

  	    </xsl:when>
		<xsl:when test="root/pagedata/from_page = 'cart'">

  	    </xsl:when>
		<xsl:when test="root/pagedata/from_page = 'cart'">

  	    </xsl:when>

		<xsl:when test="root/pagedata/from_page = 'order_detail'">
			<tr><td colspan="3" align="center" class="Label">Order Number: <xsl:value-of select="//root/pagedata/external_order_number"/></td></tr>
  	    </xsl:when>
		</xsl:choose>


		<xsl:choose>
		<xsl:when test="root/pagedata/from_page = 'customer_account'">
		<tr><td class="banner" colspan="3">Customer Information</td></tr>
  	    </xsl:when>
		<xsl:when test="root/pagedata/from_page = 'loss_prevention'">
		<tr><td class="banner" colspan="3">Customer Information</td></tr>
  	    </xsl:when>
		<xsl:when test="root/pagedata/from_page = 'cart'">
			<tr><td class="banner" colspan="3">Customer Information</td></tr>
  	    </xsl:when>
		<xsl:when test="root/pagedata/from_page = 'charge_back'">
			<tr><td class="banner" colspan="3">Customer Information</td></tr>
  	    </xsl:when>
		<xsl:when test="root/pagedata/from_page = 'order_detail'">
			<tr><td class="banner" colspan="3">Recipient Information</td></tr>
  	    </xsl:when>
		</xsl:choose>


											<tr>
												


		<xsl:choose>
		<xsl:when test="root/pagedata/from_page = 'customer_account'">
												<td width="35%" align="right"><b>Customer Record Number:</b></td>
  	    </xsl:when>
		<xsl:when test="root/pagedata/from_page = 'charge_back'">
												<td width="35%" align="right"><b>Customer Record Number:</b></td>
  	    </xsl:when>
		<xsl:when test="root/pagedata/from_page = 'loss_prevention'">
												<td width="35%" align="right"><b>Customer Record Number:</b></td>
  	    </xsl:when>
		<xsl:when test="root/pagedata/from_page = 'cart'">
												<td width="35%" align="right"><b>Customer Record Number:</b></td>
  	    </xsl:when>
		<xsl:when test="root/pagedata/from_page = 'order_detail'">
												<td width="35%" align="right"><b>Recipient Record Number:</b></td>
  	    </xsl:when>
		</xsl:choose>
												
												
												<td  align="left"><xsl:value-of select="root/customers/customer/customer_id"/></td>
												<td  align="left">
													<input TYPE="HIDDEN" name="cust_num_hold_value" id="cust_num_hold_value" value='{root/customers/customer/id_reason}'></input>
													<input TYPE="HIDDEN" name="cust_num_value" id="cust_num_value" value='{root/customers/customer/customer_id}'></input>
													<input TYPE="HIDDEN" name="cust_num_hold_id" id="cust_num_hold_id" value='{root/customers/customer/entity_id_customer}'></input>
													<xsl:choose>
													<xsl:when test="string-length(root/customers/customer/id_reason) = 0">
													<input TYPE="CHECKBOX" name="cust_num_hold_chk" id="cust_num_hold_chk"  onclick="changed();" tabindex="2">Hold</input>
													</xsl:when>
													<xsl:otherwise>
													<input TYPE="CHECKBOX" name="cust_num_hold_chk" id="cust_num_hold_chk" onclick="changed();" checked="checked"  tabindex="2">Hold</input>
													</xsl:otherwise>
													</xsl:choose>
												</td>
											</tr>
											<tr>
											
		<xsl:choose>
		<xsl:when test="root/pagedata/from_page = 'customer_account'">
												<td  align="right"><b>Customer Name:</b></td>
  	    </xsl:when>
		<xsl:when test="root/pagedata/from_page = 'charge_back'">
												<td  align="right"><b>Customer Name:</b></td>
  	    </xsl:when>
		<xsl:when test="root/pagedata/from_page = 'loss_prevention'">
												<td  align="right"><b>Customer Name:</b></td>
  	    </xsl:when>
		<xsl:when test="root/pagedata/from_page = 'cart'">
												<td  align="right"><b>Customer Name:</b></td>
  	    </xsl:when>
		<xsl:when test="root/pagedata/from_page = 'order_detail'">
												<td  align="right"><b>Recipient Name:</b></td>
  	    </xsl:when>
		</xsl:choose>
											

												<td  align="left"><xsl:value-of select="root/customers/customer/last_name"/>,<xsl:value-of select="root/customers/customer/first_name"/></td>
													<input TYPE="HIDDEN" name="cust_name_hold_value" id="cust_name_hold_value" value='{root/customers/customer/name_reason}'></input>
													<input TYPE="HIDDEN" name="cust_name_value" id="cust_name_value" value='{root/customers/customer/first_name}|{root/customers/customer/last_name}'></input>
													<input TYPE="HIDDEN" name="cust_name_hold_id" id="cust_name_hold_id" value='{root/customers/customer/entity_id_name}'></input>
													<xsl:choose>
													<xsl:when test="string-length(root/customers/customer/name_reason) = 0">
													<td  align="left"><input TYPE="CHECKBOX" name="cust_name_hold_chk" id="cust_name_hold_chk" onclick="changed();" tabindex="3">Hold</input></td>
													</xsl:when>
													<xsl:otherwise>
													<td  align="left"><input TYPE="CHECKBOX" name="cust_name_hold_chk" id="cust_name_hold_chk" checked="checked" onclick="changed();" tabindex="3">Hold</input></td>
													</xsl:otherwise>
													</xsl:choose>

											</tr>
											<tr>
												<td  align="right"><b>Address Line 1:</b></td>
												<td  align="left"><xsl:value-of select="root/customers/customer/address_1"/></td>
													<input TYPE="HIDDEN" name="cust_addr_hold_value" id="cust_addr_hold_value" value='{root/customers/customer/address_reason}'></input>
													<input TYPE="HIDDEN" name="cust_addr_value" id="cust_addr_value" value='{concat(root/customers/customer/address_1,"|",substring(root/customers/customer/zip_code,1,5))}'></input>
													<input TYPE="HIDDEN" name="cust_addr_hold_id" id="cust_addr_hold_id" value='{root/customers/customer/entity_id_address}'></input>
													<xsl:choose>
													<xsl:when test="string-length(root/customers/customer/address_reason) = 0">
													<td  align="left"><input TYPE="CHECKBOX" name="cust_addr_hold_chk" id="cust_addr_hold_chk" onclick="changed();" tabindex="4">Hold</input></td>
													</xsl:when>
													<xsl:otherwise>
													<td  align="left"><input TYPE="CHECKBOX" name="cust_addr_hold_chk" id="cust_addr_hold_chk" checked="checked" onclick="changed();" tabindex="4">Hold</input></td>
													</xsl:otherwise>
													</xsl:choose>

											</tr>
											<tr>
												<td  align="right"><b>Address Line 2:</b></td>
												<td  align="left"><xsl:value-of select="root/customers/customer/address_2"/></td>
												<td  align="left"></td>
											</tr>
											<tr>
												<td  align="right"><b>City, State:</b></td>
												<td  align="left"><xsl:value-of select="root/customers/customer/city"/>, <xsl:value-of select="root/customers/customer/state"/></td>
												<td  align="left"></td>
											</tr>
											<tr>
												<td  align="right"><b>Zip/Postal Code:</b></td>
												<td  align="left"><xsl:value-of select="substring(root/customers/customer/zip_code,1,5)"/></td>
												<td  align="left"></td>
											</tr>
											<tr>
												<td  align="right"><b>Country:</b></td>
												<td  align="left"><xsl:value-of select="root/customers/customer/country"/></td>
												<td  align="left"></td>
											</tr>


											<xsl:for-each select="root/credit_cards/credit_card">
											<tr>
												<xsl:choose>
												<xsl:when test="count(//credit_card) > 1">
													<td width="35%" align="right"><b>Credit Card Number (<xsl:value-of select="position()"/>):</b></td>
												</xsl:when>
												<xsl:otherwise>
													<td width="30%" align="right"><b>Credit Card Number:</b></td>
												</xsl:otherwise>
												</xsl:choose>
												<td  align="left"><xsl:value-of select="cc_number"/></td>
												    <input TYPE="HIDDEN" name="cust_card_hold_value{position()}" id="cust_card_hold_value{position()}"  value="{cc_reason}"></input>
												    <input TYPE="HIDDEN" name="cust_card_value{position()}" id="cust_card_value{position()}"  value="{cc_number}"></input>
												    <input TYPE="HIDDEN" name="cust_card_hold_id{position()}" id="cust_card_hold_id{position()}" value='{customer_hold_entity_id}'></input>
													<xsl:choose>
													<xsl:when test="string-length(cc_reason) = 0">
													<td  align="left"><input TYPE="CHECKBOX" name="cust_card_hold_chk{position()}" id="cust_card_hold_chk{position()}" onclick="changed();"  tabindex="1{position()}">Hold</input></td>
													</xsl:when>
													<xsl:otherwise>
													<td  align="left"><input TYPE="CHECKBOX" name="cust_card_hold_chk{position()}" id="cust_card_hold_chk{position()}" checked="checked" onclick="changed();" tabindex="1{position()}">Hold</input></td>
													</xsl:otherwise>
													</xsl:choose>
											</tr>
                                                                                        <tr>
                                                                                            <td colspan="3" align="center">AVS Response: <xsl:value-of select="avs_response"/></td>
                                                                                        </tr>
											</xsl:for-each>
                                                                                        <xsl:for-each select="root/ap_accounts/ap_account">
                                                                                        <tr>
                                                                                            <td width="35%" align="right"><b><xsl:value-of select="ap_account_type_desc"/> Account:</b></td>
                                                                                            <td align="left"><xsl:value-of select="ap_account_txt"/></td>
                                                                                            <input type="hidden" name="ap_account_hold_value{position()}" id="ap_account_hold_value{position()}" value="{ap_reason}"/>
                                                                                            <input type="hidden" name="ap_account_value{position()}" id="ap_account_value{position()}" value="{ap_account}"/>
                                                                                            <input type="hidden" name="ap_account_hold_id{position()}" id="ap_account_hold_id{position()}" value="{customer_hold_entity_id}"/>
                                                                                            <xsl:choose>
                                                                                                <xsl:when test="string-length(ap_reason) = 0">
                                                                                                    <td align="left"><input type="checkbox" name="ap_account_hold_chk{position()}" id="ap_account_hold_chk{position()}" onclick="changed();" tabindex="1{position()}">Hold</input></td>
                                                                                                </xsl:when>
                                                                                                <xsl:otherwise>
                                                                                                    <td align="left"><input type="checkbox" name="ap_account_hold_chk{position()}" id="ap_account_hold_chk{position()}" checked="checked" onclick="changed();" tabindex="1{position()}">Hold</input></td>
                                                                                                </xsl:otherwise>
                                                                                            </xsl:choose>
                                                                                        </tr>
                                                                                        </xsl:for-each>
											<xsl:for-each select="root/phonenumbers/phonenumber">
												<tr>
												<xsl:choose>
												<xsl:when test="count(//phonenumber) > 1">
												<td  align="right"><b>Phone (<xsl:value-of select="position()"/>):</b></td>
												</xsl:when>
												<xsl:otherwise>
												<td  align="right"><b>Phone:</b></td>
												</xsl:otherwise>
												</xsl:choose>
												<td  align="left"><xsl:value-of select="phone_number"/></td>
													<input TYPE="HIDDEN" name="cust_phone_hold_value{position()}" id="cust_phone_hold_value{position()}"  value="{phone_reason}"></input>
													<input TYPE="HIDDEN" name="cust_phone_value{position()}" id="cust_phone_value{position()}"  value="{phone_number}"></input>
													<input TYPE="HIDDEN" name="cust_phone_hold_id{position()}" id="cust_phone_hold_id{position()}" value='{customer_hold_entity_id}'></input>
													<xsl:choose>
													<xsl:when test="string-length(phone_reason) = 0">
													<td  align="left"><input TYPE="CHECKBOX" name="cust_phone_hold_chk{position()}" id="cust_phone_hold_chk{position()}" onclick="changed();" tabindex="2{position()}">Hold</input></td>
													</xsl:when>
													<xsl:otherwise>
													<td  align="left"><input TYPE="CHECKBOX" name="cust_phone_hold_chk{position()}" id="cust_phone_hold_chk{position()}" checked="checked" onclick="changed();" tabindex="2{position()}">Hold</input></td>
													</xsl:otherwise>
													</xsl:choose>
												</tr>
											</xsl:for-each>

		<xsl:choose>
		<xsl:when test="root/pagedata/from_page != 'order_detail'">



											<xsl:for-each select="root/memberships/membership">
											<tr>
												<input TYPE="HIDDEN" name="cust_member_hold_value{position()}" id="cust_member_hold_value{position()}"  value="{membership_reason}"></input>
												<input TYPE="HIDDEN" name="cust_member_value{position()}" id="cust_member_value{position()}"  value="{membership_number}"></input>
												<input TYPE="HIDDEN" name="cust_member_hold_id{position()}" id="cust_member_hold_id{position()}" value='{customer_hold_entity_id}'></input>
												<xsl:choose>
												<xsl:when test="count(//membership) > 1">
												<td  align="right"><b>Membership ID (<xsl:value-of select="position()"/>):</b></td>
												</xsl:when>
												<xsl:otherwise>
												<td  align="right"><b>Membership ID:</b></td>
												</xsl:otherwise>
												</xsl:choose>
												<td  align="left"><xsl:value-of select="membership_type"/>:<xsl:value-of select="membership_number"/></td>
													<xsl:choose>
													<xsl:when test="string-length(membership_reason) = 0">
													<td  align="left"><input TYPE="CHECKBOX" name="cust_member_hold_chk{position()}" id="cust_member_hold_chk{position()}" onclick="changed();" tabindex="3{position()}">Hold</input></td>
													</xsl:when>
													<xsl:otherwise>
													<td  align="left"><input TYPE="CHECKBOX" name="cust_member_hold_chk{position()}" id="cust_member_hold_chk{position()}" checked="checked" onclick="changed();" tabindex="3{position()}">Hold</input></td>
													</xsl:otherwise>
													</xsl:choose>
											</tr>
											</xsl:for-each>
											
  	    </xsl:when>
		</xsl:choose>
											
											<xsl:for-each select="root/emails/email">
											<tr>
												<input TYPE="HIDDEN" name="cust_email_hold_value{position()}" id="cust_email_hold_value{position()}"  value="{email_reason}"></input>
												<input TYPE="HIDDEN" name="cust_email_value{position()}" id="cust_email_value{position()}"  value="{email_address}"></input>
												<input TYPE="HIDDEN" name="cust_email_hold_id{position()}" id="cust_email_hold_id{position()}" value='{customer_hold_entity_id}'></input>
												<xsl:choose>
												<xsl:when test="count(//email) > 1">
												<td  align="right"><b>Email Address (<xsl:value-of select="position()"/>):</b></td>
												</xsl:when>
												<xsl:otherwise>
												<td  align="right"><b>Email Address:</b></td>
												</xsl:otherwise>
												</xsl:choose>
												<td  align="left"><xsl:value-of select="email_address"/></td>
													<xsl:choose>
													<xsl:when test="string-length(email_reason) = 0">
													<td  align="left"><input TYPE="CHECKBOX" name="cust_email_hold_chk{position()}" id="cust_email_hold_chk{position()}" onclick="changed();" tabindex="4{position()}">Hold</input></td>
													</xsl:when>
													<xsl:otherwise>
													<td  align="left"><input TYPE="CHECKBOX" name="cust_email_hold_chk{position()}" id="cust_email_hold_chk{position()}" checked="checked" onclick="changed();" tabindex="4{position()}">Hold</input></td>
													</xsl:otherwise>
													</xsl:choose>
											</tr>
											</xsl:for-each>
											
											<xsl:choose>
											<xsl:when test="root/pagedata/from_page != 'order_detail'">
											
											<xsl:for-each select="root/lps/lp">
											<tr>
												<input TYPE="HIDDEN" name="cust_lp_hold_value{position()}" id="cust_lp_hold_value{position()}"  value="{loss_prevention_reason}"></input>
												<input TYPE="HIDDEN" name="cust_lp_value{position()}" id="cust_lp_value{position()}"  value="{loss_prevention_indicator}"></input>
												<input TYPE="HIDDEN" name="cust_lp_hold_id{position()}" id="cust_lp_hold_id{position()}" value='{customer_hold_entity_id}'></input>
												<xsl:choose>
												<xsl:when test="count(//lp) > 1">
												<td  align="right"><b>LP Indicator (<xsl:value-of select="position()"/>):</b></td>
												</xsl:when>
												<xsl:otherwise>
												<td  align="right"><b>LP Indicator:</b></td>
												</xsl:otherwise>
												</xsl:choose>
												<td  align="left"><xsl:value-of select="loss_prevention_indicator"/></td>
													<xsl:choose>
													<xsl:when test="string-length(loss_prevention_reason) = 0">
													<td  align="left"><input TYPE="CHECKBOX" name="cust_lp_hold_chk{position()}" id="cust_lp_hold_chk{position()}" onclick="changed();" tabindex="5{position()}">Hold</input></td>
													</xsl:when>
													<xsl:otherwise>
													<td  align="left"><input TYPE="CHECKBOX" name="cust_lp_hold_chk{position()}" id="cust_lp_hold_chk{position()}" checked="checked" onclick="changed();" tabindex="5{position()}">Hold</input></td>
													</xsl:otherwise>
													</xsl:choose>
											</tr>
											</xsl:for-each>
											
											</xsl:when>
											</xsl:choose>
											
											</table>
										</div>
										</td>
										</tr>

										<tr>
										<td>
																				<table width="75%" border="0" align="center" cellpadding="2" cellspacing="0">
												<tr>
						                          <td>
						                          	<div align="center">
 						                          		<input TYPE="HIDDEN" name="orig_hold_reason" id="orig_hold_reason"  value="{//root/pagedata/holdreason}"></input>
														<b>Hold Reason:</b><select name="holdreason" id="holdreason"  onchange="ddChanaged();" tabindex="60">

														<xsl:choose>
														<xsl:when test="string-length(//root/pagedata/holdreason) = 0">
															<OPTION VALUE=""  selected="true"></OPTION>
														</xsl:when>
														<xsl:otherwise>
															<OPTION VALUE=""  ></OPTION>
														</xsl:otherwise>
														</xsl:choose>

														<xsl:for-each select="root/holdreasons/holdreason">
														<xsl:choose>
														<xsl:when test="hold_reason_code = //root/pagedata/holdreason">
															<OPTION VALUE="{hold_reason_code}" selected="true" ><xsl:value-of select="description"/></OPTION>
														</xsl:when>
														<xsl:otherwise>
															<OPTION VALUE="{hold_reason_code}" ><xsl:value-of select="description" /></OPTION>
														</xsl:otherwise>
														</xsl:choose>
														</xsl:for-each>
														</select>
													</div>
						                          </td>
						                        </tr>
						                        <tr>
						                          <td>
						                            <div align="center">
							                            <xsl:choose>
							                            <xsl:when test="//root/pagedata/updateAllowed = 'Y'">						
							                              <button type="button" class="BlueButton" disabled="true" style="width:80;" name="save" tabindex="70"  accesskey="S" onclick="javascript:doSave();">(S)ave </button>
														</xsl:when>
														<xsl:otherwise>														
                             							 <button type="button" class="BlueButton" disabled="true" style="width:80;" name="save" tabindex="70"  accesskey="S" onclick="javascript:doSave();">(S)ave </button>
						                                </xsl:otherwise>														
														</xsl:choose>
						                            </div>
						                          </td>
						                        </tr>
											</table>
										</td>
										</tr>

										</table>
											</td>
												</tr>
										</table>


	</td>
	</tr>
</table>

   <table width="98%" border="0" align="center" cellpadding="0" cellspacing="1">
      <tr>
      	 <td>
			 <table>
			 	<tr>
				 <td  align="left">
           <xsl:if test="root/pagedata/from_page = 'order_detail'">
    				 <button type="button" class="bigBlueButton" style="width:80;" name="recipientOrderButton" tabindex="80"  accesskey="O" onclick="javascript:executeBackToOrder();">Recipient/<br/>(O)rder</button>
           </xsl:if>
					 <button type="button" class="bigBlueButton" style="width:80;" name="addNew" tabindex="80"  accesskey="C" onclick="javascript:executeCustomerAccount();">(C)ustomer<br></br>Account </button>
					 
           <xsl:choose>
             <xsl:when test="root/pagedata/from_page != 'customer_account'">
      			   <xsl:choose>
                 <xsl:when test="string-length(root/pagedata/loss_prevention_ind) > 0">
                   <button type="button" class="bigBlueButton" style="width:80;" name="addNew" tabindex="90"  accesskey="L" onclick="javascript:executeLossPrevention();">(L)oss<br></br>Prevention </button>
                 </xsl:when>
                 <xsl:otherwise>
                   <button type="button" class="bigBlueButton" style="width:80;" name="addNew" tabindex="90"  accesskey="L" onclick="javascript:executeNoLossPrevention();">(L)oss<br></br>Prevention </button>
                 </xsl:otherwise>
               </xsl:choose>
    				 </xsl:when>
  				 </xsl:choose>
					
					<xsl:choose>
					<xsl:when test="root/pagedata/from_page = 'customer_account'">
     				    <xsl:choose>
                      	<xsl:when test="//root/pagedata/updateAllowed = 'Y'">
<!--					 		<button type="button" class="bigBlueButton" style="width:80;" name="addNew" tabindex="100" onclick="javascript:doReleaseAction();">(R)elease All<br></br>Held Orders </button>-->
					 		<button type="button" id="cancelAllHeldOrdersButton" class="bigBlueButton" style="width:80;" name="addNew" tabindex="110"  accesskey="H" onclick="javascript:doCancelAction();">Cancel All<br></br>(H)eld Orders </button>
						</xsl:when>
						<xsl:otherwise>
<!--					 		<button type="button" class="bigBlueButton" style="width:80;" disabled="true" name="addNew" tabindex="100" accesskey="R" onclick="javascript:doReleaseAction();">(R)elease All<br></br>Held Orders </button>-->
					 		<button type="button" id="cancelAllHeldOrdersButton" class="bigBlueButton" style="width:80;" disabled="true" name="addNew" tabindex="110"  accesskey="H" onclick="javascript:doCancelAction();">Cancel All<br></br>(H)eld Orders </button>
						</xsl:otherwise>	
						</xsl:choose>				 							
			  	    </xsl:when>
					<xsl:when test="root/pagedata/from_page = 'charge_back'">
     				    <xsl:choose>
                      	<xsl:when test="//root/pagedata/updateAllowed = 'Y'">
					<!-- 		<button type="button" class="bigBlueButton" style="width:80;" name="addNew" tabindex="100" onclick="javascript:doReleaseAction();">(R)elease All<br></br>Held Orders </button>-->
					 		<button type="button" id="cancelAllHeldOrdersButton" class="bigBlueButton" style="width:80;" name="addNew" tabindex="110"  accesskey="H" onclick="javascript:doCancelAction();">Cancel All<br></br>(H)eld Orders </button>
						</xsl:when>
						<xsl:otherwise>
<!--					 		<button type="button" class="bigBlueButton" style="width:80;" disabled="true" name="addNew" tabindex="100" accesskey="R" onclick="javascript:doReleaseAction();">(R)elease All<br></br>Held Orders </button>-->
					 		<button type="button" id="cancelAllHeldOrdersButton" class="bigBlueButton" style="width:80;" disabled="true" name="addNew" tabindex="110"  accesskey="H" onclick="javascript:doCancelAction();">Cancel All<br></br>(H)eld Orders </button>
						</xsl:otherwise>
						</xsl:choose>
			  	    </xsl:when>
					<xsl:when test="root/pagedata/from_page = 'loss_prevention'">
     				    <xsl:choose>
                      	<xsl:when test="//root/pagedata/updateAllowed = 'Y'">
<!--					 		<button type="button" class="bigBlueButton" style="width:80;" name="addNew" tabindex="100" onclick="javascript:doReleaseAction();">(R)elease All<br></br>Held Orders </button>-->
					 		<button type="button" id="cancelAllHeldOrdersButton" class="bigBlueButton" style="width:80;" name="addNew" tabindex="110"  accesskey="H" onclick="javascript:doCancelAction();">Cancel All<br></br>(H)eld Orders </button>
						</xsl:when>
						<xsl:otherwise>
<!--					 		<button type="button" class="bigBlueButton" style="width:80;" disabled="true" name="addNew" tabindex="100"  accesskey="R" onclick="javascript:doReleaseAction();">(R)elease All<br></br>Held Orders </button>-->
					 		<button type="button" id="cancelAllHeldOrdersButton" class="bigBlueButton" style="width:80;" disabled="true" name="addNew" tabindex="110"  accesskey="H" onclick="javascript:doCancelAction();">Cancel All<br></br>(H)eld Orders </button>
						</xsl:otherwise>
						</xsl:choose>
			  	    </xsl:when>
					<xsl:when test="root/pagedata/from_page = 'cart'">

     				    <xsl:choose>
                      	<xsl:when test="//root/pagedata/updateAllowed = 'Y'">
					 		 <button type="button" class="bigBlueButton" style="width:80;" name="addNew" tabindex="100"  accesskey="R" onclick="javascript:doReleaseAction();">(R)elease <br></br>Held Cart </button>
							 <button type="button" id="cancelHeldCartButton" class="bigBlueButton" style="width:80;" name="addNew" tabindex="110"  accesskey="H" onclick="javascript:doCancelAction();">Cancel <br></br>(H)eld Cart </button>
						</xsl:when>
						<xsl:otherwise>
							 <button type="button" class="bigBlueButton" style="width:80;"   accesskey="R" disabled="true" name="addNew" tabindex="100" onclick="javascript:doReleaseAction();">(R)elease <br></br>Held Cart </button>
							 <button type="button" id="cancelHeldCartButton" class="bigBlueButton" style="width:80;"  disabled="true" name="addNew" tabindex="110"  accesskey="H" onclick="javascript:doCancelAction();">Cancel <br></br>(H)eld Cart </button>
						</xsl:otherwise>
						</xsl:choose>


			  	    </xsl:when>
					<xsl:when test="root/pagedata/from_page = 'order_detail'">

     				    <xsl:choose>
                      	<xsl:when test="//root/pagedata/updateAllowed = 'Y'">
					 <button type="button" class="bigBlueButton" style="width:80;" name="addNew"  accesskey="R" tabindex="100" onclick="javascript:doReleaseAction();">(R)elease <br></br>Held Order </button>
					 <button type="button" id="cancelHeldOrderButton" class="bigBlueButton" style="width:80;" name="addNew" tabindex="110"  accesskey="H" onclick="javascript:doCancelAction();">Cancel <br></br>(H)eld Order </button>
						</xsl:when>
						<xsl:otherwise>
					 <button type="button" class="bigBlueButton" style="width:80;"  disabled="true" name="addNew"  accesskey="R" tabindex="100" onclick="javascript:doReleaseAction();">(R)elease <br></br>Held Order </button>
					 <button type="button" id="cancelHeldOrderButton" class="bigBlueButton" style="width:80;"  disabled="true" name="addNew" tabindex="110"  accesskey="H" onclick="javascript:doCancelAction();">Cancel <br></br>(H)eld Order </button>
						</xsl:otherwise>
						</xsl:choose>

			  	    </xsl:when>
					</xsl:choose>

				 </td>
				 </tr>
			 </table>
		  </td>
		  <td align="right">
			 <table>
			 	<tr>
				 	<td  align="right" valign="top">
					 <button type="button" class="BlueButton" style="width:80;" name="mainMenu" accesskey="E" tabindex="120" onclick="javascript:executeSearch();">S(e)arch </button>
				 	</td>
				</tr>
				<tr>
					<td align="right" valign="top">
				 	<button type="button" class="BlueButton" style="width:80;" name="mainMenu"  accesskey="M" tabindex="140" onclick="javascript:executeMenu();">(M)ain Menu </button>
				 	</td>
				</tr>
			 </table>
		</td>
      </tr>
</table>

<!--Copyright bar-->
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
   <tr>
      <td></td>
   </tr>   
   <tr>
     <td class="disclaimer"></td><script>showCopyright();</script>
   </tr>
</table>
	</form>
	</body>
</html>
</xsl:template>
	<xsl:template name="addHeader">

		<xsl:choose>
		<xsl:when test="root/pagedata/from_page = 'customer_account'">
		<xsl:call-template name="header">
			<xsl:with-param name="showBackButton" select="true()"/>
			<xsl:with-param name="showPrinter" select="true()"/>
			<xsl:with-param name="showSearchBox" select="false()"/>
			<xsl:with-param name="searchLabel" select="'Order #'"/>
			<xsl:with-param name="showCSRIDs" select="true()"/>
			<xsl:with-param name="showTime" select="true()"/>
			<xsl:with-param name="headerName" select="'Customer Hold Detail'"/>
			<xsl:with-param name="dnisNumber" select="$call_dnis"/>
			<xsl:with-param name="cservNumber" select="$call_cs_number"/>
			<xsl:with-param name="indicator" select="$call_type_flag"/>
			<xsl:with-param name="brandName" select="$call_brand_name"/>
			<xsl:with-param name="backButtonLabel" select="'(B)ack to Cust Acct'"/>
			<xsl:with-param name="cdispEnabledFlag" select="$cdisp_enabledFlag"/>
		</xsl:call-template>
  	    </xsl:when>
		<xsl:when test="root/pagedata/from_page = 'charge_back'">
		<xsl:call-template name="header">
			<xsl:with-param name="showBackButton" select="true()"/>
			<xsl:with-param name="showPrinter" select="true()"/>
			<xsl:with-param name="showSearchBox" select="false()"/>
			<xsl:with-param name="searchLabel" select="'Order #'"/>
			<xsl:with-param name="showCSRIDs" select="true()"/>
			<xsl:with-param name="showTime" select="true()"/>
			<xsl:with-param name="headerName" select="'Customer Hold Detail'"/>
			<xsl:with-param name="dnisNumber" select="$call_dnis"/>
			<xsl:with-param name="cservNumber" select="$call_cs_number"/>
			<xsl:with-param name="indicator" select="$call_type_flag"/>
			<xsl:with-param name="brandName" select="$call_brand_name"/>
			<xsl:with-param name="backButtonLabel" select="'(B)ack to Cust Acct'"/>
			<xsl:with-param name="cdispEnabledFlag" select="$cdisp_enabledFlag"/>
		</xsl:call-template>
  	    </xsl:when>
		<xsl:when test="root/pagedata/from_page = 'cart'">
		<xsl:call-template name="header">
			<xsl:with-param name="showBackButton" select="true()"/>
			<xsl:with-param name="showPrinter" select="true()"/>
			<xsl:with-param name="showSearchBox" select="false()"/>
			<xsl:with-param name="searchLabel" select="'Order #'"/>
			<xsl:with-param name="showCSRIDs" select="true()"/>
			<xsl:with-param name="showTime" select="true()"/>
			<xsl:with-param name="headerName" select="'Customer Hold Detail'"/>
			<xsl:with-param name="dnisNumber" select="$call_dnis"/>
			<xsl:with-param name="cservNumber" select="$call_cs_number"/>
			<xsl:with-param name="indicator" select="$call_type_flag"/>
			<xsl:with-param name="brandName" select="$call_brand_name"/>
			<xsl:with-param name="backButtonLabel" select="'(B)ack to Cust Acct'"/>
			<xsl:with-param name="cdispEnabledFlag" select="$cdisp_enabledFlag"/>
		</xsl:call-template>
  	    </xsl:when>
		<xsl:when test="root/pagedata/from_page = 'loss_prevention'">
		<xsl:call-template name="header">
			<xsl:with-param name="showBackButton" select="true()"/>
			<xsl:with-param name="showPrinter" select="true()"/>
			<xsl:with-param name="showSearchBox" select="false()"/>
			<xsl:with-param name="searchLabel" select="'Order #'"/>
			<xsl:with-param name="showCSRIDs" select="true()"/>
			<xsl:with-param name="showTime" select="true()"/>
			<xsl:with-param name="headerName" select="'Customer Hold Detail'"/>
			<xsl:with-param name="dnisNumber" select="$call_dnis"/>
			<xsl:with-param name="cservNumber" select="$call_cs_number"/>
			<xsl:with-param name="indicator" select="$call_type_flag"/>
			<xsl:with-param name="brandName" select="$call_brand_name"/>
			<xsl:with-param name="backButtonLabel" select="'(B)ack to Cust Acct'"/>
			<xsl:with-param name="cdispEnabledFlag" select="$cdisp_enabledFlag"/>
		</xsl:call-template>
  	    </xsl:when>
		<xsl:when test="root/pagedata/from_page = 'order_detail'">
		<xsl:call-template name="header">
			<xsl:with-param name="showBackButton" select="true()"/>
			<xsl:with-param name="showPrinter" select="true()"/>
			<xsl:with-param name="showSearchBox" select="false()"/>
			<xsl:with-param name="searchLabel" select="'Order #'"/>
			<xsl:with-param name="showCSRIDs" select="true()"/>
			<xsl:with-param name="showTime" select="true()"/>
			<xsl:with-param name="headerName" select="'Recipient Hold Detail'"/>
			<xsl:with-param name="dnisNumber" select="$call_dnis"/>
			<xsl:with-param name="cservNumber" select="$call_cs_number"/>
			<xsl:with-param name="indicator" select="$call_type_flag"/>
			<xsl:with-param name="brandName" select="$call_brand_name"/>
			<xsl:with-param name="backButtonLabel" select="'(B)ack to Order'"/>
			<xsl:with-param name="cdispEnabledFlag" select="$cdisp_enabledFlag"/>
		</xsl:call-template>
  	    </xsl:when>
		</xsl:choose>


				
	</xsl:template>
</xsl:stylesheet>