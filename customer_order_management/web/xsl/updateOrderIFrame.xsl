<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="securityanddata.xsl"/>
<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>

<xsl:key name="pageData" match="/root/pageData/data" use="name"/>
<xsl:key name="security" match="/root/security/data" use="name"/>
	
<xsl:output method="html" indent="yes"/>
<xsl:template match="/root">
<html>
<head>
<script type="text/javascript" language="javascript">
var message = '<xsl:value-of select="key('pageData','update_order_error_msg')/value"/>';

<![CDATA[
	function processCustomer()
	{
	
    if(message == '')
    {    
    	parent.doUpdateOrderInfoAction();
       
    }
    else
    {
        alert(message);
    }
	}
]]>
</script>
</head>
<body onload="javascript:processCustomer();">
    <xsl:call-template name="securityanddata"/>   
    <xsl:call-template name="decisionResultData"/>
</body>
</html>
</xsl:template>
</xsl:stylesheet>