
<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="header.xsl"/>
	<xsl:import href="footer.xsl"/>
  	<xsl:import href="securityanddata.xsl"/>
  	<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>
	<xsl:output method="html"
				doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
				doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
				indent="yes"/>
	<xsl:key name="pagedata" match="root/pageData/data" use="name" />
	<xsl:key name="security" match="/root/security/data" use="name"/>
		<xsl:template match="/">
		<html>
			<head>
				<META http-equiv="Content-Type" content="text/html"/>
				<title>FTD - DNIS Maintenance</title>
				<link rel="stylesheet" type="text/css" href="css/calendar.css"/>
        <link rel="stylesheet" type="text/css" href="css/ftd.css"/>
 				<script type="text/javascript" src="js/tabIndexMaintenance.js"></script>
				<script type="text/javascript" src="js/util.js"></script>
				<script type="text/javascript" src="js/calendar.js"></script>
				<script type="text/javascript" src="js/clock.js"></script>
				<script type="text/javascript" src="js/commonUtil.js"></script>
				<script type="text/javascript" src="js/mercuryMessage.js"></script>
				<script type="text/javascript"><![CDATA[
				
var cos_show_previous = "true";
var cos_show_next = "true";
var cos_show_first = "true";
var cos_show_last = "true";

// init code
function init()
{
  untabAllElements();
  setDNISMaintenanceIndexes();  
  
]]>

    buttonDis(document.dnisMaintenance.page_position.value);
					<xsl:if test="/root/out-parameters/OUT_LOCK_OBTAINED = 'N'">
      replaceText("lockError", "Current record is locked by <xsl:value-of select='/root/out-parameters/OUT_LOCKED_CSR_ID'/>");
					</xsl:if>
					<xsl:if test="/root/out-parameters/OUT_LOCK_OBTAINED = 'Y'">
        //document.getElementById("dnisMaintenance").src = "loadDnisEdit.do?action_type=insert_dnis";
					</xsl:if><![CDATA[
}

function setDNISMaintenanceIndexes()
{
  untabAllElements();
	var begIndex = findBeginIndex();
 	var tabArray1 = new Array('nextItem','backItem','firstItem','lastItem', 
  'addNew', 'backButton', 'printer');
  begIndex = setTabIndexes(tabArray1, begIndex);
  return begIndex;
}


function replaceText( elementId, newString )
  {
   var node = document.getElementById( elementId );
   var newTextNode = document.createTextNode( newString );
   if(node.childNodes[0]!=null)
   {
    node.removeChild( node.childNodes[0] );
   }
   node.appendChild( newTextNode );
  }

//Disable buttons
function buttonDis(page)
{
  setNavigationHandlers();
 	if (dnisMaintenance.total_pages.value <= 1)
 	{
    cos_show_previous = "false";
  	cos_show_first = "false";
    cos_show_next = "false";
    cos_show_last = "false";
    document.getElementById("firstItem").disabled= "true";
    document.getElementById("backItem").disabled= "true";
    document.getElementById("nextItem").disabled= "true";
    document.getElementById("lastItem").disabled= "true";
    return;
  }
 	if (page < 2)
 	{
    cos_show_previous = "false";
  	cos_show_first = "false";
    document.getElementById("firstItem").disabled= "true";
    document.getElementById("backItem").disabled= "true";
    return;
  }
  if (page == dnisMaintenance.total_pages.value)
  {
    cos_show_next = "false";
    cos_show_last = "false";
    document.getElementById("nextItem").disabled= "true";
    document.getElementById("lastItem").disabled= "true";
    return;
  }
}

// refresh the page with new data
function loadDnisMaintenanceAction()
{
		document.dnisMaintenance.action = "loadDnisMaintenance.do";
   	document.dnisMaintenance.target = "";
	 	document.dnisMaintenance.submit();
}

function editDnisAction(dnisid)
{
    document.dnisMaintenance.dnis_id.value = dnisid;
    document.dnisMaintenance.action_type.value = "update_dnis";
    openPopup();
}

function addDnisAction()
{
    document.dnisMaintenance.action_type.value = "insert_dnis";
    document.dnisMaintenance.dnis_id.value = "0";
    openPopup();
    //document.dnisMaintenance.action = "loadDnisEdit.do";
    //document.dnisMaintenance.submit();
}

function openPopup()
{
  var modal_dim = "dialogWidth:390px; dialogHeight:525px; center:yes; status:0; help:no; scroll:no";
  var ran = Math.floor(Math.random()*100);
  var form = document.dnisMaintenance;
  var urlEditDnis = "loadDnisEdit.do?dnis_id="+form.dnis_id.value+
                    "&action_type="+form.action_type.value+
                    "&securitytoken="+form.securitytoken.value+
                    "&context="+form.context.value+
                    "&rand="+ran;

  var ret = showModalDialogIFrame(urlEditDnis, document.dnisMaintenance.action_type.value, modal_dim);

  if (ret && ret != null)
  {
    if(ret == "RELOAD")
    {
      loadDnisMaintenanceAction();
    }
    else
    {
      // just return
      document.getElementById("unlockFrame").src="lock.do?action_type=unlock&lock_app=DNIS&lock_id="+document.forms[0].dnis_id.value+"&securitytoken="+document.forms[0].securitytoken.value+"&context="+document.forms[0].context.value+"&applicationcontext="+document.forms[0].applicationcontext.value;
      return;
    }
  }
  else
  {
    // some error may have occurred
   // alert("Edit/Add DNIS not performed.");
    return;
  }
}
function checkKey()
{
  var tempKey = window.event.keyCode;

  //Up arrow pressed
  if (window.event.altKey && tempKey == 38 && cos_show_previous == "true")
  {
    loadDnisMaintenanceAction(--dnisMaintenance.page_position.value);
  }

  //Down arrow pressed
  if (window.event.altKey && tempKey == 40 && cos_show_next == "true")
  {
    loadDnisMaintenanceAction(++dnisMaintenance.page_position.value);
  }

  //Right arrow pressed
  if (window.event.altKey && tempKey == 39 && cos_show_last == "true")
  {
    loadDnisMaintenanceAction(dnisMaintenance.page_position.value = dnisMaintenance.total_pages.value);
  }

  //Left arrow pressed
  if (window.event.altKey && tempKey == 37 && cos_show_first == "true")
  {
    loadDnisMaintenanceAction(dnisMaintenance.page_position.value = 1);
  }
}

]]>

/*************************************************************************************
* Perorms the Back button functionality
**************************************************************************************/
      function doBackAction() {
        doMainMenuActionNoPopup();
      }
				</script>
			<style>
				.messageContainer .lineNumber {
					width: 5%;
				}

				.messageContainer .dnisId {
					width: 8%;
					text-align: left;
				}
				.messageContainer .origin {
					width: 6%;
					text-align: left;
				}

				.messageContainer .statusFlag {
					width: 6%;
					text-align: center;
				}

				.messageContainer .oeFlag {
					width: 6%;
					text-align: center;
				}

				.messageContainer .description {
					width: 29%;
					text-align: left;
				}

				.messageContainer .ypFlag {
					width: 8%;
					text-align: center;
				}

				.messageContainer .scriptCode {
					width: 8%;
					text-align: center;
				}

				.messageContainer .defaultSource {
					width: 8%;
					text-align: left;
				}

				.messageContainer .scriptId {
					width: 8%;
					text-align: left;
				}

				.messageContainer .editButton {
					width: 8%;
				}
			</style>
			</head>
			<body onload="init();" onkeydown="javascript:checkKey();">
				<xsl:call-template name="header">
					<xsl:with-param name="headerName"  select="'DNIS MAINTENANCE'" />
					<xsl:with-param name="showTime" select="true()"/>
					<xsl:with-param name="showBackButton" select="true()"/>
					<xsl:with-param name="showPrinter" select="true()"/>
					<xsl:with-param name="showSearchBox" select="false()"/>
          <xsl:with-param name="backButtonLabel" select="'(B)ack to Main Menu'"/>
          <xsl:with-param name="cdispEnabledFlag" select="$cdisp_enabledFlag"/>
				</xsl:call-template>
				<form name="dnisMaintenance" method="post" action="">
				<iframe id="unlockFrame" name="unlockFrame" height="0" width="0" border="0"></iframe>
          <xsl:call-template name="securityanddata"/>
          <xsl:call-template name="decisionResultData"/>
					<input type="hidden" name="action_type" id="action_type" value="{key('pagedata','action_type')/value}"/>
					<input type="hidden" name="page_position" id="page_position" value="{key('pagedata','page_position')/value}"/>
					<input type="hidden" name="total_pages" id="total_pages" value="{key('pagedata', 'total_pages')/value}"/>
					<input type="hidden" name="start_position" id="start_position" value="{key('pagedata', 'start_position')/value}"/>
					<input type="hidden" name="dnis_id" id="dnis_id" value="" />
					<table border="0" cellpadding="0" cellspacing="0" width="98%" align="center">
						<tr>
							<td id="lockError" class="ErrorMessage"></td>
						</tr>
					</table>
					<table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
						<tr>
							<td>
								<table width="100%" border="0" align="center" cellpadding="2" cellspacing="2">
									<tr>
										<td colspan="3" align="left">
											<table width="100%" border="0" align="left" cellpadding="2" cellspacing="0">
												<tr>
													<td>
														<div class="messageContainer" style="padding-right: 16px; width: 99%;">
															<table width="100%" cellpadding="1" cellspacing="1">
																<tr>
																	<th class="lineNumber">&nbsp;</th>
																	<th class="dnisId">DNIS</th>
																	<th class="origin">Origin</th>
																	<th class="statusFlag">Status</th>
																	<th class="oeFlag">OE</th>
																	<th class="description">Description</th>
																	<th class="ypFlag">YP&nbsp;Flag</th>
																	<th class="scriptCode">Script</th>
																	<th class="defaultSource">Dflt&nbsp;Src</th>
																	<th class="scriptId">Script&nbsp;ID</th>
																	<th class="editButton">&nbsp;</th>
																</tr>
															</table>
														</div>
														<table width="100%" cellpadding="0" border="0" cellspacing="0">
															<tr>
																<td>
																	<hr/>
																</td>
															</tr>
														</table>
														<div class="messageContainer" style="width: 99%; overflow: auto; padding-right: 16px; height: 270px;">
															<table width="100%" cellpadding="1" cellspacing="1" style="margin-right: -16px;">
																<xsl:for-each select="root/Dnis/dnis_data">
																	<tr>
																		<td class="lineNumber">
																			<script type="text/javascript">document.write(dnisMaintenance.start_position.value); dnisMaintenance.start_position.value++</script>.
																		</td>
																		<td class="dnisId">
																			<xsl:value-of select="dnis_id"/>
																		</td>
																		<td class="origin">
																			<xsl:value-of select="origin"/>
																		</td>
																		<td class="statusFlag">
																			<xsl:value-of select="status_flag"/>
																		</td>
																		<td class="oeFlag">
																			<xsl:value-of select="oe_flag"/>
																		</td>
																		<td class="description">
																			<xsl:value-of select="description"/>
																		</td>
																		<td class="ypFlag">
																			<xsl:value-of select="yellow_pages_flag"/>
																		</td>
																		<td class="scriptCode">
																			<xsl:value-of select="script_code"/>
																		</td>
																		<td class="defaultSource">
																			<xsl:value-of select="default_source_code"/>
																		</td>
																		<td class="scriptId">
																			<xsl:value-of select="script_id"/>
																		</td>
																		<td class="editButton">
																			<xsl:variable name="dnsid" select="dnis_id"/>
																			<button type="button" class="BlueButton" name="editDnis" tabIndex = "-1" onClick="editDnisAction({$dnsid});">Edit</button>&nbsp;
																		</td>
																	</tr>
																</xsl:for-each>
															</table>
														</div>
													</td>
												</tr>
												<tr>
													<td colspan="11"><hr/></td>
												</tr>
												<tr>
													<td colspan="5" align="right" >
														<table border="0">
															<tr>
																<td class="label" align="right">Page&nbsp;
																	<xsl:for-each select="key('pagedata', 'page_position')">
																		<xsl:value-of select="value" />
																	</xsl:for-each>
																	&nbsp;of&nbsp;
																	<xsl:for-each select="key('pagedata', 'total_pages')">
									                              		<xsl:if test="value = '0'">
									                               			1
									                              		</xsl:if>
									                              		<xsl:if test="value != '0'">
																 			<xsl:value-of select="value" />
									                              		</xsl:if>
																	</xsl:for-each>
																</td>
															</tr>
															<xsl:for-each select="key('pagedata', 'page_position')">
																<tr>
																	<td align="right">
																			<xsl:choose>
										                              				<xsl:when test="key('pagedata','total_pages')/value !='0'">
										                              					<button type="button" name="firstItem" class="arrowButton" id="firstItem" onclick="loadDnisMaintenanceAction(dnisMaintenance.page_position.value = 1);">
									                              						7</button>
										                              				</xsl:when>
										                              				<xsl:when test="key('pagedata','total_pages')/value ='0'">
										                              					<button type="button" name="firstItem" class="arrowButton" id="firstItem" onclick="">
									                              						7</button>
										                              				</xsl:when>
										                              				</xsl:choose>
										                             			<xsl:choose>
										                              				<xsl:when test="key('pagedata','total_pages')/value !='0'">
										                              					<button type="button" name="backItem" class="arrowButton" id="backItem" onclick="loadDnisMaintenanceAction(--dnisMaintenance.page_position.value);">
										                              						3
										                              					</button>
										                              				</xsl:when>
										                              				<xsl:when test="key('pagedata','total_pages')/value ='0'">
										                              					<button type="button" name="backItem" class="arrowButton" id="backItem" onclick="">
											                              					3
										                              					</button>
										                              				</xsl:when>
										                              				</xsl:choose>
										                           				<xsl:choose>
										                              				<xsl:when test="key('pagedata','total_pages')/value !='0'">
										                              					<button type="button" name="nextItem" class="arrowButton" id="nextItem" onclick="loadDnisMaintenanceAction(++dnisMaintenance.page_position.value);">
											                              					4
										                              					</button>
										                              				</xsl:when>
										                              					<xsl:when test="key('pagedata','total_pages')/value ='0'">
										                              						<button type="button" name="nextItem" class="arrowButton" id="nextItem" onclick="">
											                              						4
										                              						</button>
										                              					</xsl:when>
										                              				</xsl:choose>
										                           				<xsl:choose>
										                              				<xsl:when test="key('pagedata','total_pages')/value !='0'">
											                              				<button type="button" name="lastItem" class="arrowButton" id="lastItem" onclick="loadDnisMaintenanceAction(dnisMaintenance.page_position.value = dnisMaintenance.total_pages.value);">
												                              				8
											                              				</button>
										                              				</xsl:when>
										                              				<xsl:when test="key('pagedata','total_pages')/value ='0'">
										                              					<button type="button" name="lastItem" class="arrowButton" id="lastItem" onclick="">
											                              					8
										                              					</button>
										                          					</xsl:when>
										                           					</xsl:choose>
										                             			
																	</td>
																</tr>
															</xsl:for-each>
														</table>
													</td>
												</tr>
											</table>
										</td>

									</tr>
									<tr>
										<td colspan="3" align="left"></td>
									</tr>
								</table></td>
						</tr>
					</table>
					<iframe id="lockFrame" src="" width="0" height="0" frameborder="0"/>
				</form>
				<table width="98%" align="center">
					<tr>
						<td width="75%" valign="top">
							<input name="addNew" id="addNew" accesskey="A" class="BlueButton" type="button" value="(A)dd New" onClick="return addDnisAction();" />
						</td>
						<td width="20%" align="right">
				  			<button type="button" accesskey="B" class="BlueButton" name="backButton" onclick="javascript:doBackAction();">(B)ack to Main Menu</button>
<!--  							<br />
  							<button type="button" accesskey="M" class="bluebutton" name="mainMenu" tabindex="10" onclick="javascript:doMainMenuActionNoPopup();">(M)ain<br/>Menu</button>-->
  						</td>
  					</tr>
				</table>
				<div id="waitDiv" style="display:none">
					<table id="waitTable" width="98%" border="3" height="100%" align="center" cellpadding="30" cellspacing="1" bordercolor="#006699">
						<tr>
							<td width="100%">
								<table width="100%" cellspacing="0" cellpadding="0">
									<tr>
										<td id="waitMessage" align="right" width="50%" class="waitMessage"></td>
										<td id="waitTD" width="50%" class="waitMessage"></td>
									</tr>
								</table></td>
						</tr>
					</table>
				</div>
				<xsl:call-template name="footer"></xsl:call-template>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>
<!-- Stylus Studio meta-information - (c) 2004-2005. Progress Software Corporation. All rights reserved.
<metaInformation>
<scenarios ><scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="..\..\..\..\..\dnisMaint.xml" htmlbaseurl="" outputurl="" processortype="internal" profilemode="0" profiledepth="" profilelength="" urlprofilexml="" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext=""/></scenarios><MapperMetaTag><MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/><MapperBlockPosition></MapperBlockPosition></MapperMetaTag>
</metaInformation>
-->