<!DOCTYPE ACDemo[
	<!ENTITY nbsp "&#160;">
	<!ENTITY reg "&#169;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<!-- external templates -->
<xsl:import href="header.xsl"/>
<xsl:import href="footer.xsl"/>
<xsl:import href="securityanddata.xsl"/>
<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>



<xsl:key name="pageData" match="/root/pageData/data" use="name"/>
<xsl:key name="searchCriteria" match="/root/searchCriteria/criteria" use="name"/>
<xsl:key name="security" match="/root/security/data" use="name"/>

<!-- decimal format used when a value is not given -->
<xsl:decimal-format NaN="0.00" name="notANumber"/>
<xsl:decimal-format NaN="(0.00)"  name="noRefund"/>
<!-- Case comparision values -->
<xsl:variable name="upper_case">ABCDEFGHIJKLMNOPQRSTUVWXYZ</xsl:variable>
<xsl:variable name="lower_case">abcdefghijklmnopqrstuvwxyz</xsl:variable>

<xsl:variable name="Residence" select="'R'"/>
<xsl:variable name="Business" select="'B'"/>
<xsl:variable name="FuneralHome" select="'F'"/>
<xsl:variable name="Hospital" select="'H'"/>
<xsl:variable name="Other" select="'O'"/>

<xsl:variable name="R" select="'Residence'"/>
<xsl:variable name="B" select="'Business'"/>
<xsl:variable name="F" select="'Funeral Home'"/>
<xsl:variable name="H" select="'Hospital'"/>
<xsl:variable name="O" select="'Other'"/>

<xsl:variable name="displayOrderNumber" select="/root/pageData/data[name='display_order_number']/value"/>

<xsl:variable name="newWebsiteFees" select="/root/pageData/data[name='total_fees']/value"/>

<xsl:output doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"/>
<xsl:output method="html" indent="yes" encoding="ISO-8859-1"/>
<xsl:template match="/root">
<html>
 <head>
	<title>FTD - Customer Shopping Cart</title>
	<link rel="stylesheet" href="css/ftd.css"/>
	<style type="text/css">
		
		 /* Scrolling table overrides for the order/shopping cart table */
			div.tableContainer table {
				width: 97%;
			}
			div#messageContainer {
				height: 50px;
				width: 98%;
			}
	</style>
	<style type="text/css">
        
        .tooltip{ 
            display:none;
            position:fixed;
            top:2em; left:2em; width:12em;
            border:1px solid #3669AC;
            background-color: #CCEFFF;
            color:#000000;
            text-align: left;
            padding-left: 4px;
            padding-right: 4px;
            font-family: arial,helvetica,sans-serif;
            font-size: 8pt;
            text-decoration:none;
            filter: progid:DXImageTransform.Microsoft.Shadow(color=gray,strength=4,direction=135);
        }

        </style>
<script type="text/javascript" src="js/commonUtil.js"/>
<script type="text/javascript" src="js/tabIndexMaintenance.js"/>
<script type="text/javascript" src="js/util.js"/>
<script type="text/javascript" src="js/FormChek.js"/>
<script type="text/javascript" language="javascript">
	var contactFirstName = '<xsl:value-of select="order/header/contact-first-name"/>';
	var contactLastName = '<xsl:value-of select="order/header/contact-last-name"/>';
	var contactPhone = '<xsl:value-of select="order/header/contact-phone"/>';		
 	var contactExt = '<xsl:value-of select="order/header/contact-ext"/>';
 	var contactEmail = '<xsl:value-of select="order/header/contact-email-address"/>';
 	
 	var taxArray = new Array( <xsl:for-each select="order/taxes/tax">
                                        ["<xsl:value-of select="tax_description"/>",
                                        "<xsl:value-of select="tax_amount"/>",
                                        "<xsl:value-of select="tax_display_order"/>"]
                                        <xsl:choose>
                                          <xsl:when test="position()!=last()">,</xsl:when>
                                        </xsl:choose>
                                      </xsl:for-each>
                                    );
	function sortByDisplayOrder(a,b) {
		return a[2] - b[2];
	}                              
    taxArray.sort(sortByDisplayOrder);

<![CDATA[

/*******************************************************************************
Intilization

init()

*******************************************************************************/
function init(){
  setNavigationHandlers();
  setOriginalViewIndexes();
  parsePDPString();
}

function doAlternateContact(){
 	var sURL = "alternateContactPopup.html";
	var obj = new Object();
	
  obj.first_name = contactFirstName;
  obj.last_name = contactLastName;
  obj.email = contactEmail;
  obj.phone = contactPhone;
  obj.ext = contactExt;
	
	 var sFeatures="dialogHeight:200px;dialogWidth:700px;";
      sFeatures +=  "status:0;scroll:0;resizable:0";
   window.showModalDialog(sURL, obj, sFeatures);
}
/*******************************************************************************

performAction()

*******************************************************************************/
function performAction(url){
 scroll(0,0);
 showWaitMessage("content", "wait", "Processing");
 document.forms[0].action = url;
 document.forms[0].submit();
}

/*************************************************************************************
*	Performs the Back button functionality
**************************************************************************************/
function doBackAction()
{
	var form = document.forms[0];
	var url = "customerOrderSearch.do?action=customer_account_search&order_number=" + form.order_number.value +"&securitytoken="+form.securitytoken.value+"&context="+form.context.value+"&applicationcontext="+form.applicationcontext.value;
	form.action = url;
	form.submit();
}



/*******************************************************************************
*  setOriginalViewIndexes()
*  Sets tabs for Original View Page.
*  See tabIndexMaintenance.js
*******************************************************************************/
function setOriginalViewIndexes()
{
  untabAllElements();
	var begIndex = findBeginIndex();
 	var tabArray = new Array('ovRecipientOrder','ovBackToOrder', 'ovMainMenu','printer');
  var newBegIndex = setTabIndexes(tabArray, begIndex);
  return newBegIndex;
}

function displayTaxesOnMouseOver(xpos,ypos) {
    var mouseOverString = "";
    for (var i=0; i<taxArray.length; i++) {
        if (taxArray[i][1] != '' && taxArray[i][1] > 0) {
            mouseOverString = mouseOverString + taxArray[i][0] + ': ';
         	mouseOverString = mouseOverString + formatDollarAmt(taxArray[i][1]) + '<br>';
     	}
	}
    if (mouseOverString != "" ) {
        $("#spanTaxMouseOver").html(mouseOverString);
        $("#spanTaxMouseOver")[0].style.top=ypos+15;
        $("#spanTaxMouseOver")[0].style.left=xpos-5;
        $("#spanTaxMouseOver").show();
    }
   // hack for IE7 issue where space appears under the tabs when you mouse over
   $("iframe.hidden").attr("style", "display:none");
   $("div.visible").attr("style", "margin:0px");

}

// Parses the PDP personalization string.
// Since the PDP personalization information is psuedo-XML within XML, we have
// to resort to this kludge to display the name/value pairs.  Yuck.
// "pdpRawString" is the hidden textarea where the XSL will put the raw personalization data
// that this function pulls from to parse the name/value pairs.
// "pdpFormattedString" is the TD element where the formated string will be displayed.
//
function parsePDPString() {
	
   var pString = "";
   var pdpRawStr = "";
   
   var piStart = /&lt;PersonalizationItems&gt;/;
   var nameStart = /&lt;DisplayName&gt;/;
   var nameEndMatch = /(.*)&lt;\/DisplayName&gt;/;
   var valueStart = /&lt;Value&gt;/;
   var valueEndMatch = /(.*)&lt;\/Value&gt;/;
   var sortStart = /&lt;SortOrder&gt;/;
   var sortEndMatch = /(.*)&lt;\/SortOrder&gt;/;
   var tmp;
	
   try {	
      pdpRawStr = document.getElementById("pdpRawString").innerHTML;
      if (pdpRawStr == null || pdpRawStr.length <= 1) {
        return;
      }
      var piArray = pdpRawStr.split(piStart);
      if (piArray == null || piArray.length <=1) {
         pString = pdpRawStr;
      } else {
         for (pi=0; pi < piArray.length; pi++) {      
            if (piArray[pi] != '') {
            
               // DisplayName
               var nameArray = piArray[pi].split(nameStart);
               for (n=0; n < nameArray.length; n++) {
                  if (nameArray[n] != '') {
                    tmp = nameArray[n].match(nameEndMatch);
                    if (tmp != null) {
                      pString += " Name: \"" + tmp[1] + "\"";          
                    }
                  }
               }
               
               // Value
               var valueArray = piArray[pi].split(valueStart);
               for (vl=0; vl < valueArray.length; vl++) {
                  if (valueArray[vl] != '') {
                    tmp = valueArray[vl].match(valueEndMatch);
                    if (tmp != null) {
                      pString += " Value: \"" + tmp[1] + "\"";          
                    }
                  }
               }
               
               // SortOrder
               var sortArray = piArray[pi].split(sortStart);
               for (s=0; s < sortArray.length; s++) {
                  if (sortArray[s] != '') {
                    tmp = sortArray[s].match(sortEndMatch);
                    if (tmp != null) {
                      pString += " Sort-order: " + tmp[1];          
                    }
                  }
               }
            }
         }  // End for
      }
   } catch (err) {
      pString = pdpRawStr;
   }
   document.getElementById("pdpFormattedString").innerHTML = pString;
}


]]>
  </script>
</head>
<body onload="init()">
<xsl:call-template name="addHeader"/>
<form name="originalView" method="post">
 <!-- Call template to import security params and searchCriteria -->
	<xsl:call-template name="securityanddata"/>
	<xsl:call-template name="decisionResultData"/>
        <input type="hidden" name="customer_id" id="customer_id" value="{/root/pageData/data[name='customer_id']/value}"/>
        <input type="hidden" name="order_number" id="order_number" value="{order/items/item[order-number=$displayOrderNumber]/order-number}"/>
<xml id="laura">
  <xsl:copy-of select="node()" /> 
</xml>
<div id="content" style="display:block">
<table class="mainTable" align="center" cellpadding="1" cellspacing="1" width="98%">
	<tr>
		<td>
			<table  class="innerTable" cellpadding="1" cellspacing="2" width="100%">
				<tr>
					<td>
						<table id="ovHeaderTable" border="0" width="100%" cellpadding="0" cellspacing="0">
							<tr>
								<td>
									<span class="Label">Confirmation Number:</span>&nbsp;
									<xsl:value-of select="$displayOrderNumber" />
								</td>
                <td>
									<span class="Label">Membership Number:</span>&nbsp;
									<xsl:value-of select="//membershipInfo/membershipNumber" />
                </td>
								<td>
									<span class="Label">Source Code:</span>&nbsp;
									<a href="javascript:doSourceCodeAction('{order/header/source-code}')" class="textLink"><xsl:value-of select="order/header/source-code"/></a>
								</td>
							</tr>
							<tr>
								<td>
									<span class="Label">Order Date/Time:</span>
									&nbsp;
									<xsl:value-of select="order/header/transaction-date"/>
								</td>
                 <td>
									<span class="Label">Member Name:</span>&nbsp;
									<xsl:value-of select="//membershipInfo/firstName" />&nbsp;<xsl:value-of select="//membershipInfo/lastName" />
								</td>
                <td>
								<span class="Label">Program Name:</span>&nbsp;
									<xsl:value-of select="//membershipInfo/programName" />
								</td>
							</tr>
                                                        <xsl:if test="order/header/order-created-by-id != ''">
                                                          <tr>
                                                                  <td colspan="3">
                                                                          <span class="Label">Order Created By:</span>
                                                                          &nbsp;
                                                                          <xsl:value-of select="order/header/order-created-by-id"/>
                                                                  </td>
                                                          </tr>
                                                        </xsl:if>
                                                          <xsl:if test="fraud-description != ''">
                                                              <tr>
                                                                        <td colspan="3">
                                                                                <span class="Label">Fraud Disposition:</span>
                                                                                &nbsp;
                                                                                <xsl:value-of select="fraud-description"/>
                                                                        </td>
                                                              </tr>
                                                          </xsl:if>
                                                          <xsl:if test="order/header/fraud-comments != ''">
                                                              <tr>
                                                                      <td colspan="3">
                                                                              <span class="Label">Fraud Comments:</span>
                                                                              &nbsp;
                                                                                <xsl:value-of select="order/header/fraud-comments"/>
                                                                      </td>
                                                              </tr>
                                                          </xsl:if>
                                                          <xsl:if test="novator-fraud-description != ''">
                                                              <tr>
                                                                        <td colspan="3">
                                                                                <span class="Label">Novator Fraud Comments:</span>
                                                                                &nbsp;
                                                                                <xsl:value-of select="novator-fraud-description"/>
                                                                        </td>
                                                              </tr>
                                                          </xsl:if>
							<xsl:if test="order/header/co-brands/co-brand/child::* > 0">
							<tr><td colspan="3" align="left" class="Label"><br />Partner Information:<hr /></td></tr>
							<tr>
								<td colspan="3">
						          <div class="tableContainer" id="messageContainer" >
									   <table width="100%" class="scrollTable" border="0" cellpadding="0"  align="center" cellspacing="2">
										 <thead class="fixedHeader">
						                  	<tr style="text-align:left">
								              <td  class="Label">Name</td>
								              <td  class="Label">Value</td>
								            </tr>
								            </thead>
								             <tbody class="scrollContent">
								             	<xsl:for-each select="order/header/co-brands/co-brand">
								             		<tr >
                                    <td  style="text-align:left"><xsl:value-of select="name"/></td>
                                    <td  style="text-align:left"><xsl:value-of select="data"/></td>
                                </tr>
                              </xsl:for-each>
											</tbody>
										</table>
									 </div>
								</td>
							</tr>
							</xsl:if>
						</table>
						
					</td>
				</tr>
				<tr>
					<td class="banner" style="text-align:left">Customer Information</td>
				</tr>
				<tr>
					<td>
						<table id="ovCustomerInfo" border="0" cellpadding="0" cellspacing="1" width="100%">
							<tr>
								<td width="10%" align="right" >
									<span class="Label">Name:</span>
								</td>
								<td width="37%" ><xsl:value-of select="order/header/buyer-first-name"/>&nbsp;<xsl:value-of select="order/header/buyer-last-name"/></td>
								<td align="right" width="10%">
									<span class="Label">Address 1:</span>
								</td>
								<td><xsl:value-of select="order/header/buyer-address1"/></td>
							</tr>
							<tr>
								<td align="right">
									<span class="Label">Bus. Name:</span>
									
								</td>
								<td><xsl:value-of select="order/header/buyer-business"/></td>
								<td align="right">
									<span class="Label">Address 2:</span>
								</td>
								<td><xsl:value-of select="order/header/buyer-address2"/></td>
							</tr>
							<tr>
								<td align="right">
									<span class="Label">Birthdate:</span>
								</td>
								<td><xsl:value-of select="order/items/item[order-number=$displayOrderNumber]/item-extensions/extension/name[. = 'Customer Birthdate']/../data"/></td>
								<td align="right">
									<span class="Label">Email Address:</span>
								</td>
								<td>
									<xsl:value-of select="order/header/buyer-email-address"/>
								</td>
								
							</tr>
							<tr>
								<td align="right">
									<span class="Label">H Number:</span>
								</td>
								<td>
									<xsl:if test="order/header/buyer-secondary-phone != ''">
										<script type="text/javascript">
											if(isUSPhoneNumber('<xsl:value-of select="order/header/buyer-secondary-phone"/>')){
												document.write(reformatUSPhone('<xsl:value-of select="order/header/buyer-secondary-phone"/>'));
											}else{
												document.write('<xsl:value-of select="order/header/buyer-secondary-phone"/>');
											}
										</script>
									</xsl:if>
								</td>
								<td colspan="2">
									<xsl:if test="order/header/contact-first-name != ''">
									    <a href="javascript:doAlternateContact();" class="textLink">Alternate Contact</a>
									</xsl:if>
								</td>
							</tr>
							<tr>
								<td align="right">
									<span class="Label">W Number:</span>
								</td>
								<td>
									<xsl:if test="order/header/buyer-primary-phone != ''">
										<script type="text/javascript">
											if(isUSPhoneNumber('<xsl:value-of select="order/header/buyer-primary-phone"/>')){
												document.write(reformatUSPhone('<xsl:value-of select="order/header/buyer-primary-phone"/>'));
											}else{
												document.write('<xsl:value-of select="order/header/buyer-primary-phone"/>');
											}
										</script>
									</xsl:if>
									 <span style="padding-left:4em;" class="Label">Ext:</span>&nbsp;
									 <xsl:value-of select="order/header/buyer-primary-phone-ext"/>
								</td>
							</tr>
							<tr>
								<td align="right"><span class="Label">City,State:</span></td>
								<td>
									<xsl:choose>
										<xsl:when test="order/header/buyer-state != ''">
											<xsl:value-of select="order/header/buyer-city"/>, <xsl:value-of select="order/header/buyer-state"/>
										</xsl:when>
										<xsl:otherwise>
												<xsl:value-of select="order/header/buyer-city"/>
										</xsl:otherwise>
									</xsl:choose>
								</td>
							</tr>
							<tr>
								<td align="right"><span class="Label">Zip Code:</span></td>
								<td>
									<xsl:value-of select="order/header/buyer-postal-code"/>
									<span style="padding-left:4em;" class="Label">Country:</span>&nbsp;<xsl:value-of select="order/header/buyer-country"/>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="banner" style="text-align:left;">Recipient Information</td>
				</tr>
				<tr>
					<td>
						<table id="ovRecipientInfo" border="0" cellpadding="0" cellspacing="0" width="100%">
							<tr>
								<td width="11%" align="right">
									<span class="Label">Name:</span>
								</td>
								<td width="37%">
									<xsl:value-of select="order/items/item[order-number=$displayOrderNumber]/recip-first-name"/>&nbsp;<xsl:value-of select="order/items/item[order-number=$displayOrderNumber]/recip-last-name"/>
								</td>
								<td align="right" width="10%">
									<span class="Label">Address 1:</span>
								</td>
								<td><xsl:value-of select="order/items/item[order-number=$displayOrderNumber]/recip-address1"/></td>
							</tr>
							<tr>
								<td align="right"><span class="Label">Del. Location:</span></td>
								<td>
                  <xsl:variable name="shipToType" select="order/items/item[order-number=$displayOrderNumber]/ship-to-type"/>
                  <xsl:value-of select="//ADDRESS_TYPES/ADDRESS_TYPE[CODE = $shipToType]/DESCRIPTION"/>
<!--
									<xsl:choose>
                  
										<xsl:when test="order/items/item[order-number=$displayOrderNumber]/ship-to-type = $Residence">
											<xsl:value-of select="$R"/>
										</xsl:when>
										<xsl:when test="order/items/item[order-number=$displayOrderNumber]/ship-to-type = $Business">
											<xsl:value-of select="$B"/>
										</xsl:when>
										<xsl:when test="order/items/item[order-number=$displayOrderNumber]/ship-to-type = $FuneralHome">
											<xsl:value-of select="$F"/>
										</xsl:when>
										<xsl:when test="order/items/item[order-number=$displayOrderNumber]/ship-to-type = $Hospital">
											<xsl:value-of select="$H"/>
										</xsl:when>
										<xsl:when test="order/items/item[order-number=$displayOrderNumber]/ship-to-type = $Other">
											<xsl:value-of select="$O"/>
										</xsl:when>
									</xsl:choose>
		-->							
								</td>
								<td align="right"><span class="Label">Address 2:</span></td>
								<td><xsl:value-of select="order/items/item[order-number=$displayOrderNumber]/recip-address2"/></td>
							</tr>
							<tr>
								<td align="right">
									<span class="Label">Bus. Name:</span>
								</td>
								<td><xsl:value-of select="order/items/item[order-number=$displayOrderNumber]/ship-to-type-name"/></td>
							</tr>
							<tr>
								<td align="right"><span class="Label">Phone:</span></td>
								<td>
									<xsl:if test="order/items/item[order-number=$displayOrderNumber]/recip-phone != ''">
										<script type="text/javascript">
											if(isUSPhoneNumber('<xsl:value-of select="order/items/item[order-number=$displayOrderNumber]/recip-phone"/>')){
												document.write(reformatUSPhone('<xsl:value-of select="order/items/item[order-number=$displayOrderNumber]/recip-phone"/>'));
											}else{
												document.write('<xsl:value-of select="order/items/item[order-number=$displayOrderNumber]/recip-phone"/>');
											}
										</script>
									</xsl:if>
                 	 <span style="padding-left:4em;" class="Label">Ext:</span>&nbsp;
  									 <xsl:value-of select="order/items/item[order-number=$displayOrderNumber]/recip-phone-ext"/>
								</td>
							</tr>
							
							<tr>
								<td align="right"><span class="Label">City,State:</span></td>
								<td><xsl:choose>
										<xsl:when test="order/items/item[order-number=$displayOrderNumber]/recip-state != ''">
											<xsl:value-of select="order/items/item[order-number=$displayOrderNumber]/recip-city"/>, <xsl:value-of select="order/items/item[order-number=$displayOrderNumber]/recip-state"/>
										</xsl:when>
										<xsl:otherwise>
												<xsl:value-of select="order/items/item[order-number=$displayOrderNumber]/recip-city"/>
										</xsl:otherwise>
									</xsl:choose>
								</td>
							</tr>
							<tr>
								<td align="right"><span class="Label">Zip Code:</span></td>
								<td>
									<xsl:value-of select="order/items/item[order-number=$displayOrderNumber]/recip-postal-code"/>
									<span style="padding-left:4em;" class="Label">Country:</span>&nbsp;<xsl:value-of select="order/items/item/recip-country"/>
								</td>
							</tr>
                                                        <xsl:if test="order/items/item[order-number=$displayOrderNumber]/qms-result-code != ''">
                                                          <tr>
                                                                  <td align="right"><span class="Label">QMS Result Code:</span></td>
                                                                  <td>
                                                                          <xsl:value-of select="order/items/item[order-number=$displayOrderNumber]/qms-result-code"/>
                                                                  </td>
                                                          </tr>
                                                        </xsl:if>
						</table>
					</td>
				</tr>
				<tr>
					<td class="banner" style="text-align:left">Product Summary
						<span style="padding-left:25em;">Totals</span>
					</td>
				</tr>
				<tr>
					<td>
						<table  border="0" cellpadding="0" cellspacing="0" width="100%">
							<tr>
								<td width="50%">
									<table id="ovProductSummary" border="0" cellpadding="0" cellspacing="0" width="100%">
										<tr>
											<td class="Label" align="right" width="25%">
												Delivery Date:
											</td>
											<td><xsl:value-of select="order/items/item[order-number=$displayOrderNumber]/delivery-date"/></td>
										</tr>
										<tr>
											<td class="Label" align="right">Item:</td>
											<td>
                                                                                            <xsl:choose>
                                                                                                <xsl:when test="order/items/item[order-number=$displayOrderNumber]/product-subcode-id != ''">
                                                                                                    <xsl:value-of select="order/items/item[order-number=$displayOrderNumber]/product-subcode-id"/>
                                                                                                </xsl:when>
                                                                                                <xsl:otherwise>
                                                                                                    <xsl:value-of select="order/items/item[order-number=$displayOrderNumber]/productid"/>
                                                                                                </xsl:otherwise>
                                                                                            </xsl:choose>
                                                                                        </td>
										</tr>
										<tr>
											<td class="Label" align="right">Description:</td>
											<td><xsl:value-of select="ITEM_INFO/DETAIL/product_name"/>&nbsp; <xsl:value-of select="ITEM_INFO/DETAIL/subcode_description"/></td>
										</tr>
                    <xsl:variable name="numberOfAddOns" select="count(order/items/item[order-number=$displayOrderNumber]/add-ons/child::*)"/>
										<xsl:if test="$numberOfAddOns != 0">
										<tr>
											<td class="Label" align="right">Add-On:<xsl:value-of select="$numberOfAddOns"/></td>
											<td>
												<xsl:choose>
													<xsl:when test="$numberOfAddOns > 1">
														 <select>
														    <xsl:for-each select="order/items/item[order-number=$displayOrderNumber]/add-ons/add-on">
														   	  <option>
													    	    <xsl:value-of select="quantity"/>&nbsp;
                                    <xsl:variable name="id" select="id"/>
                                    <xsl:value-of select="//addOnId[id = $id]/description" disable-output-escaping="yes"/>
														      </option>
														   </xsl:for-each>
														</select>
													</xsl:when>
													<xsl:when test="$numberOfAddOns = 1">
												 	        <xsl:value-of select="order/items/item[order-number=$displayOrderNumber]/add-ons/add-on/quantity"/>&nbsp;
											            <xsl:variable name="id" select="order/items/item[order-number=$displayOrderNumber]/add-ons/add-on/id"/>
                                  <xsl:value-of select="//addOnId[id = $id]/description" disable-output-escaping="yes"/>
													</xsl:when>
												</xsl:choose>
											</td>
										</tr>
                    </xsl:if>
										<tr>
											<td class="Label" align="right">Substitution:</td>
											<td>
												<xsl:choose>
													<xsl:when test="order/items/item[order-number=$displayOrderNumber]/product-substitution-acknowledgement = 'Y'">
														<xsl:value-of select="'YES'"/>
													</xsl:when>
													<xsl:when test="order/items/item[order-number=$displayOrderNumber]/product-substitution-acknowledgement = 'N'">
														<xsl:value-of select="'NO'"/>
													</xsl:when>
                          <xsl:otherwise>
														<xsl:value-of select="'YES'"/>
                          </xsl:otherwise>
												</xsl:choose>
											</td>
										</tr>
										<tr>
											<td class="Label" align="right">Color:</td>
											<td><xsl:value-of select="order/items/item[order-number=$displayOrderNumber]/first-color-choice"/>&nbsp;<xsl:value-of select="order/items/item[order-number=$displayOrderNumber]/second-color-choice"/></td>
										</tr>
										<tr>
											<td class="Label" align="right">Ship Method:</td>
											<td>
                        <xsl:variable name="shippingMethod" select="order/items/item[order-number=$displayOrderNumber]/shipping-method"/>
                        <xsl:value-of select="//SHIPPINGMETHODS/SHIPPINGMETHOD[@shipmethodid=$shippingMethod]/@description"/>
                      </td>
										</tr>
										<xsl:if test="order/items/item[order-number=$displayOrderNumber]/florist != ''">
                                                                                  <tr>
                                                                                          <td class="Label" align="right">Florist ID:</td>
                                                                                          <td><xsl:value-of select="order/items/item[order-number=$displayOrderNumber]/florist"/>
                        </td>
                                                                                  </tr>
                                                                                </xsl:if>
										<xsl:if test="order/items/item[order-number=$displayOrderNumber]/order-comments != ''">
                                                                                  <tr>
                                                                                          <td class="Label" align="right">Order Comments:</td>
                                                                                          <td><xsl:value-of select="order/items/item[order-number=$displayOrderNumber]/order-comments"/>
                        </td>
                                                                                  </tr>
                                                                                </xsl:if>
                                                                            <xsl:if test="order/items/item[order-number=$displayOrderNumber]/personalizations/personalization != ''">
                                                                                <tr>
                                                                                        <td class="Label" style="vertical-align:top" align="right">Personalization:</td>
                                                                                        <td id="pdpFormattedString">
                                                                                        <textarea id="pdpRawString" style="display:none;"><xsl:value-of select="order/items/item[order-number=$displayOrderNumber]/personalizations"/></textarea>
                                                                                        </td>
                                                                                </tr>
                                                                            </xsl:if>
                                                                            <xsl:if test="order/items/item[order-number=$displayOrderNumber]/personal-greeting-id != ''">
                                                                                  <tr>
                                                                                          <td class="Label" align="right">Personal Greeting Code:</td>
                                                                                          <td><xsl:value-of select="order/items/item[order-number=$displayOrderNumber]/personal-greeting-id"/>
                        </td>
                                                                                  </tr>
                                                                                </xsl:if>
                                                                        </table>
								</td>
								<td width="50%" style="vertical-align:top">
									<table border="0" cellpadding="0" cellspacing="0" width="100%" style="text-align:center">
										<tr class="Label" align="center">
											<td>Total MSD</td>
											<td>Shipping/Service Fee</td>
											<td>Total Tax</td>
										</tr>
										<tr><!--format-number(product_amount, '#,###,##0.00;(#,###,##0.00)','notANumber') -->
											<td><xsl:value-of select="format-number(order/items/item[order-number=$displayOrderNumber]/product-price,'#,###,##0.00;(#,###,##0.00)' ,'notANumber')"/></td>
											
											<td>
												<xsl:choose>
													<xsl:when test="order/header/origin = 'Web' or order/header/origin = 'Mobile' or order/header/origin = 'Tablet'">
														<xsl:value-of select="format-number($newWebsiteFees, '#,###,##0.00;(#,###,##0.00)' ,'notANumber')"/>
													</xsl:when>
													<xsl:when test="string(number(order/items/item[order-number=$displayOrderNumber]/service-fee))='NaN' and string(number(order/items/item[order-number=$displayOrderNumber]/extra-shipping-fee))='NaN' and string(number(order/items/item[order-number=$displayOrderNumber]/drop-ship-charges))='NaN' ">
														<xsl:value-of select="'0.00'"/>
													</xsl:when>
													<xsl:when test="string(number(order/items/item[order-number=$displayOrderNumber]/service-fee)) !='NaN' and string(number(order/items/item[order-number=$displayOrderNumber]/extra-shipping-fee))='NaN' and string(number(order/items/item[order-number=$displayOrderNumber]/drop-ship-charges)) ='NaN'">
														<xsl:value-of select="format-number(order/items/item[order-number=$displayOrderNumber]/service-fee ,'#,###,##0.00;(#,###,##0.00)' ,'notANumber')"/>
													</xsl:when>
													<xsl:when test="string(number(order/items/item[order-number=$displayOrderNumber]/service-fee))='NaN' and string(number(order/items/item[order-number=$displayOrderNumber]/extra-shipping-fee))!='NaN' and string(number(order/items/item[order-number=$displayOrderNumber]/drop-ship-charges)) ='NaN'">
														<xsl:value-of select="format-number(order/items/item[order-number=$displayOrderNumber]/extra-shipping-fee ,'#,###,##0.00;(#,###,##0.00)' ,'notANumber')"/>
													</xsl:when>
													<xsl:when test="string(number(order/items/item[order-number=$displayOrderNumber]/service-fee))='NaN' and string(number(order/items/item[order-number=$displayOrderNumber]/extra-shipping-fee))='NaN' and string(number(order/items/item[order-number=$displayOrderNumber]/drop-ship-charges))!='NaN'  ">
														<xsl:value-of select="format-number(order/items/item[order-number=$displayOrderNumber]/drop-ship-charges ,'#,###,##0.00;(#,###,##0.00)' ,'notANumber')"/>
													</xsl:when>
													<xsl:when test="string(number(order/items/item[order-number=$displayOrderNumber]/service-fee))!='NaN' and string(number(order/items/item[order-number=$displayOrderNumber]/extra-shipping-fee))!='NaN' and string(number(order/items/item[order-number=$displayOrderNumber]/drop-ship-charges))='NaN'  ">
														<xsl:value-of select="format-number(order/items/item[order-number=$displayOrderNumber]/service-fee + order/items/item[order-number=$displayOrderNumber]/extra-shipping-fee,'#,###,##0.00;(#,###,##0.00)' ,'notANumber')"/>
													</xsl:when>
													<xsl:when test="string(number(order/items/item[order-number=$displayOrderNumber]/service-fee))='NaN' and string(number(order/items/item[order-number=$displayOrderNumber]/extra-shipping-fee))!='NaN' and string(number(order/items/item[order-number=$displayOrderNumber]/drop-ship-charges))!='NaN'  ">
														<xsl:value-of select="format-number(order/items/item[order-number=$displayOrderNumber]/extra-shipping-fee + order/items/item[order-number=$displayOrderNumber]/drop-ship-charges,'#,###,##0.00;(#,###,##0.00)' ,'notANumber')"/>
													</xsl:when>
													<xsl:when test="string(number(order/items/item[order-number=$displayOrderNumber]/service-fee))!='NaN' and string(number(order/items/item[order-number=$displayOrderNumber]/extra-shipping-fee))='NaN' and string(number(order/items/item[order-number=$displayOrderNumber]/drop-ship-charges))!='NaN'  ">
														<xsl:value-of select="format-number(order/items/item[order-number=$displayOrderNumber]/service-fee + order/items/item[order-number=$displayOrderNumber]/drop-ship-charges,'#,###,##0.00;(#,###,##0.00)' ,'notANumber')"/>
													</xsl:when>
												</xsl:choose>
												
											</td>
											<td>
                        <xsl:variable name ="shippingTax" select="format-number(order/items/item[order-number=$displayOrderNumber]/shipping-tax-amount,'#,###,##0.00;(#,###,##0.00)' ,'notANumber')"/>
                        <xsl:variable name ="taxTotal" select="format-number(order/items/item[order-number=$displayOrderNumber]/tax-amount,'#,###,##0.00;(#,###,##0.00)' ,'notANumber')"/>
                        <span class="taxValue" style="text-decoration: none; cursor:default; color: black;">
							<xsl:value-of select="format-number($shippingTax+$taxTotal, '#,###,##0.00;(#,###,##0.00)','notANumber')"/>
						</span>
						<span id="spanTaxMouseOver" class="tooltip" ></span>
						<script>
						$(".taxValue").mouseover(function(e) {displayTaxesOnMouseOver(e.pageX, e.pageY); });
						$(".taxValue").mouseout(function() {$("#spanTaxMouseOver").hide(); });
						</script>
                        
											</td>
										</tr>
										<tr><td colspan="3"><br /></td></tr>
										<tr><td colspan="3"><br /></td></tr>
                                                                                <xsl:choose>
                                                                                  <xsl:when test="order/header/alt-pay-method/payment-type = 'PP'">
                                                                                    <tr>
                                                                                      <td colspan="3" align="left" style="padding-left:3em;">
                                                                                            <span class="Label">Payment Type: </span>&nbsp;PayPal
                                                                                      </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                      <td colspan="3" align="left" style="padding-left:3em;">
                                                                                      </td>
                                                                                    </tr>
                                                                                  </xsl:when>

                                                                                  <xsl:when test="order/header/no-charge/approval-id != ''">
                                                                                    <tr>
                                                                                      <td colspan="3" align="left" style="padding-left:3em;">
                                                                                            <span class="Label">Payment Type: </span>&nbsp;No Charge
                                                                                      </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                      <td colspan="3" align="left" style="padding-left:3em;">
                                                                                        <span class="Label">Manager Approval ID: </span>&nbsp;<xsl:value-of select="order/header/no-charge/approval-id"/>
                                                                                      </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                      <td colspan="3" align="left" style="padding-left:3em;">
                                                                                        <span class="Label">NC Type: </span>&nbsp;<xsl:value-of select="order/header/no-charge/type"/>
                                                                                      </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                      <td colspan="3" align="left" style="padding-left:3em;">
                                                                                        <span class="Label">NC Reason: </span>&nbsp;<xsl:value-of select="order/header/no-charge/reason"/>
                                                                                      </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                      <td colspan="3" align="left" style="padding-left:3em;">
                                                                                        <span class="Label">NC Order Reference: </span>&nbsp;<xsl:value-of select="nc-external-order-number"/>
                                                                                      </td>
                                                                                    </tr>
                                                                                  </xsl:when>

                                                                                  <xsl:when test="order/header/alt-pay-method/payment-type = 'BM'">
                                                                                    <tr>
                                                                                      <td colspan="3" align="left" style="padding-left:3em;">
                                                                                            <span class="Label">Payment Type: </span>&nbsp;Bill Me Later
                                                                                      </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                      <td colspan="3" align="left" style="padding-left:3em;">
                                                                                      </td>
                                                                                    </tr>
                                                                                  </xsl:when>                                                                                  

                                                                                  <xsl:when test="order/header/alt-pay-method/payment-type = 'UA'">
                                                                                    <tr>
                                                                                      <td colspan="3" align="left" style="padding-left:3em;">
                                                                                            <span class="Label">Payment Type: </span>&nbsp;United Mileage Plus
                                                                                      </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                      <td colspan="3" align="left" style="padding-left:3em;">
                                                                                      </td>
                                                                                    </tr>
                                                                                  </xsl:when>
                                                                                                                                                           
                                                                                  <xsl:otherwise>
                                                                                 
                                                                                  <!-- Either GD or GC paymenttype -->
                                                                                  	<xsl:if test="order/header/gift-certificates/gift-certificate/number">
                                                                                  		<!-- For GC payment-type is null -->
                                                                                  		<tr>
	                                                                                      	<td colspan="3" align="left" style="padding-left:3em;">
	                                                                                            <span class="Label">Payment Type: </span>&nbsp;
	                                                                                            <xsl:choose>
																									<xsl:when test="not(order/header/gift-certificates/gift-certificate/payment-type)">GC</xsl:when>
																									<xsl:otherwise><xsl:value-of select="order/header/gift-certificates/gift-certificate/payment-type"/></xsl:otherwise>
																								</xsl:choose>	                                                                                            
	                                                                                      	</td>
                                                                                    	</tr>
	                                                                                    <tr>
	                                                                                      <td colspan="3" align="left" style="padding-left:3em;">
	                                                                                            <span class="Label">Gift Card/Certificate #: </span>&nbsp;<xsl:if test="order/header/gift-certificates/gift-certificate/number != ''"><xsl:value-of select="order/header/gift-certificates/gift-certificate/number"/></xsl:if>
	                                                                                      </td>
	                                                                                    </tr>
                                                                                  	</xsl:if>

	                                                                                  <xsl:if test="order/header/cc-type!=''">
	                                                                                    <tr>
	                                                                                      <td colspan="3" align="left" style="padding-left:3em;">
	                                                                                            <span class="Label">Payment Type: </span>&nbsp;<xsl:value-of select="order/header/cc-type"/>
	                                                                                      </td>
	                                                                                    </tr>
	                                                                                    <tr>
	                                                                                      <td colspan="3" align="left" style="padding-left:3em;">
	                                                                                            <span class="Label">Credit Card #: </span>&nbsp;<xsl:if test="order/header/cc-number != ''"><xsl:value-of select="'*****'"/><xsl:value-of select="substring(order/header/cc-number,13,16)"/></xsl:if>&nbsp;&nbsp;
	                                                                                            <span class="Label">Exp:</span>&nbsp;&nbsp;<xsl:value-of select="order/header/cc-exp-date"/>
	                                                                                      </td>
	                                                                                    </tr>
	                                                                                    </xsl:if>                                                                      
                                                                                   
                                                                                  </xsl:otherwise>
                                                                                </xsl:choose>

									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="banner" style="text-align:left;">Enclosure Card
						<span style="padding-left:25em;">Miscellaneous</span>
					</td>
				</tr>
				<tr>
					<td>
						<table  border="0" cellpadding="0" cellspacing="0" width="100%">
							<tr>
								<td width="44%">
									<table id="ovEnclosureCard" border="0" cellpadding="0" cellspacing="0" width="100%">
										<tr>
											<td class="Label" width="18%" align="right">Occasion:</td>
											<td>
                        <xsl:variable name="occasionId" select="order/items/item[order-number=$displayOrderNumber]/occassion"/>
                        <xsl:value-of select="//OCCASIONLIST/OCCASION[occasionid = $occasionId]/description"/>
                      </td>
										</tr>
										<tr>
											<td class="Label" align="right">Gift Card<br />Message:</td>
											<td>
												<textarea  rows="2" cols="35" wrap="soft" >
													<xsl:value-of select="order/items/item[order-number=$displayOrderNumber]/card-message"/>
												</textarea>
											</td>
										</tr>
										<tr>
											<td colspan="2" ><span class="Label">Signature Line:</span>&nbsp;<xsl:value-of select="order/items/item[order-number=$displayOrderNumber]/card-signature"/></td>
											
										</tr>
									</table>
								</td>
								<td width="56%" style="vertical-align:top">
									<table id="ovMisc" border="0" cellpadding="0" cellspacing="0" width="100%">
										<tr style="vertical-align:top">
                     <xsl:if test="order/items/item[order-number=$displayOrderNumber]/ship-to-type = $FuneralHome">
											  <td align="center"><span class="Label">OK to Release</span>
												  <br />
                          <xsl:if test="order/items/item[order-number=$displayOrderNumber]/sender-release-flag = 'Y'">
												   <xsl:value-of select="'YES'"/>
                          </xsl:if>
                          <xsl:if test="order/items/item[order-number=$displayOrderNumber]/sender-release-flag = 'N' or order/items/item[order-number=$displayOrderNumber]/sender-release-flag = ''">
												   <xsl:value-of select="'NO'"/>
                          </xsl:if>
											  </td>
                      </xsl:if>
											<td class="Label" align="right">
												Special<br />Instructions:
											</td>
											<td>
												<textarea  rows="3" cols="35" wrap="soft" >
													<xsl:value-of select="order/items/item[order-number=$displayOrderNumber]/special-instructions"/>
												</textarea>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	 </tr>
  </table>
  <table align="center" border="0" width="98%" cellpadding="0" cellspacing="0">
	<tr>
	        <td align="left" valign="top">
                        <button type="button" style="width:120" accesskey="O" class="BigBlueButton" id = "ovRecipientOrder" name="searchAction" onClick="doBackAction();">Recipient/<br/>(O)rder</button>
                		<xsl:if test="key('pageData','surchargeExplanation')/value != ''">
						<br/>
					             <xsl:value-of select="key('pageData','surchargeExplanation')/value"
					              				disable-output-escaping="yes"/>
				    	</xsl:if>
                
                </td>
                <td align="right">
                        <button type="button" id="ovBackToOrder" class="BlueButton" style="width:90px;" onclick="javascript:doBackAction();">(B)ack to Order</button><br />
                        <button type="button" id="ovMainMenu" class="BigBlueButton" accesskey="M" name="mainMenu" style="width:90px;" onclick="javascript: doMainMenuAction();">(M)ain<br />Menu</button>
                </td>
	</tr>
  </table>	
    <xsl:call-template name="footer"/>
  </div>
  <!-- Used to dislay Processing... message to user -->
		<div id="waitDiv" style="display:none">
		   <table id="waitTable" class="mainTable" width="98%" height="320px" align="center" cellpadding="0" cellspacing="2" >
		      <tr>
		        <td width="100%">
	              <table class="innerTable" width="100%" height="320px" cellpadding="1" cellspacing="2">
	               <tr>
	                  <td>
	                     <table width="100%" cellspacing="0" cellpadding="0" border="0">
	                         <tr>
	                           <td id="waitMessage" align="right" width="50%" class="WaitMessage"></td>
	                           <td id="waitTD"  width="50%" class="WaitMessage"></td>
	                         </tr>
	                     </table>
	                   </td>
	                </tr>
	              </table>
		        </td>
		      </tr>
	      </table>
	      <xsl:call-template name="footer"/>
	 </div>
 </form>
</body>
</html>
	</xsl:template>
	<xsl:template name="addHeader">
            <xsl:call-template name="header">
                    <xsl:with-param name="showBackButton" select="true()"/>
                    <xsl:with-param name="showPrinter" select="true()"/>
                    <xsl:with-param name="showSearchBox" select="false()"/>
                    <xsl:with-param name="showCSRIDs" select="true()"/>
                    <xsl:with-param name="showTime" select="true()"/>
                    <xsl:with-param name="headerName" select="'Original View'"/>
                    <xsl:with-param name="dnisNumber" select="$call_dnis"/>
                    <xsl:with-param name="cservNumber" select="$call_cs_number"/>
                    <xsl:with-param name="indicator" select="$call_type_flag"/>
                    <xsl:with-param name="brandName" select="$call_brand_name"/>
                    <xsl:with-param name="backButtonLabel" select="'(B)ack to Order'"/>
                    <xsl:with-param name="cdispEnabledFlag" select="$cdisp_enabledFlag"/>
            </xsl:call-template>
	</xsl:template>
</xsl:stylesheet>