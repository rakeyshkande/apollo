<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:import href="securityanddata.xsl"/>
<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>
<xsl:output method="html" indent="no"/>
<xsl:output indent="yes"/>
<xsl:key name="security" match="/root/security/data" use="name"/>

<xsl:variable name="cityInput" select="root/pageData/data[name='cityInput']/value"/>
<xsl:variable name="stateInput" select="root/pageData/data[name='stateInput']/value"/>
<xsl:variable name="zipCodeInput" select="root/pageData/data[name='zipCodeInput']/value"/>
<xsl:variable name="countryInput" select="root/pageData/data[name='countryInput']/value"/>

<xsl:template match="/root">

<html>
<head>
  <title>FTD - City Lookup</title>
  <link rel="stylesheet" TYPE="text/css" href="css/ftd.css"></link>

  <script type="text/javascript" src="js/FormChek.js"/>
  <script type="text/javascript" src="js/util.js"/>
  <script type="text/javascript">
      <![CDATA[
      var fieldNames = new Array("cityInput", "stateInput", "zipCodeInput");

      function init() {
        addDefaultListenersArray(fieldNames);
        window.name = "VIEW_CITY_LOOKUP";
        window.focus();
        document.forms[0].cityInput.focus();
      }

      function onKeyDown() {
        if (window.event.keyCode == 13)
          reopenPopup();
      }

      function closeCityLookup(city, zip, state) {
        if (window.event.keyCode == 13)
          populatePage(city, zip, state);
      }

      //This function passes parameters from this page to a function on the calling page
      function populatePage(city, zip, state) {
          top.returnValue = new Array(city, state, zip);
          top.close();
      }

      function reopenPopup() {
        var form = document.forms[0];

        //initialize the background colors
        form.cityInput.style.backgroundColor = 'white';
        form.stateInput.style.backgroundColor = 'white';
        form.zipCodeInput.style.backgroundColor = 'white';

        //First validate the inputs
        check = true;

        state = stripWhitespace(form.stateInput.value);
        city = stripWhitespace(form.cityInput.value);
        zip = stripWhitespace(form.zipCodeInput.value);

        //State is required if zip is empty
        if((state.length == 0)  ]]>&amp; <![CDATA[ (zip.length == 0))
        {
          if (check == true) {
            document.all.stateInput.focus();
            check = false;
          }
          form.stateInput.style.backgroundColor = "pink";
        }

        //City is required if zip is empty
        if((city.length == 0)  ]]>&amp; <![CDATA[ (zip.length == 0))
        {
          if (check == true) {
            document.all.cityInput.focus();
            check = false;
          }
          form.cityInput.style.backgroundColor = "pink";
        }

        if (!check) {
            alert("Please correct the marked fields");
            return false;
        }
        else {
          form.target = window.name;
          form.submit();
        }
      }
      ]]>
  </script>
</head>

<body onload="javascript:init();">
  <form name="form" method="post" action="lookupLocation.do">
  <xsl:call-template name="securityanddata"/>
  <xsl:call-template name="decisionResultData"/>
  <input type="hidden" name="countryInput" id="countryInput" value="{$countryInput}"/>

  <table width="98%" border="0" cellspacing="0" cellpadding="2">
    <tr>
      <td width="100%" align="center" class="header">City Lookup</td>
    </tr>
    <tr>
      <td>
        <table width="100%" border="0" cellpadding="2" cellspacing="2">
          <tr>
            <td width="50%" class="label" align="right">City:</td>
            <td width="50%">
              <input type="text" name="cityInput" id="cityInput" class="TblText" tabindex="1" size="20" maxlength="50" value="{$cityInput}"/>
            </td>
          </tr>
          <tr>
            <td align="right" class="label">State:</td>
            <td>
              <xsl:choose>
                <xsl:when test="$countryInput='CA'">
                  <select name="stateInput" id="stateInput" class="TblText" tabindex="2">
                    <option value=""></option>
                    <xsl:for-each select="STATES/STATE[countrycode='CAN']">
                      <option value="{statemasterid}">
                        <xsl:if test="statemasterid=$stateInput"><xsl:attribute name="SELECTED"/></xsl:if>
                        <xsl:value-of select="statename"/>
                      </option>
                    </xsl:for-each>
                  </select>
                </xsl:when>
                <xsl:when test="$countryInput='US'">
                  <select name="stateInput" id="stateInput" class="tblText" tabindex="2">
                    <option value=""></option>
                    <xsl:for-each select="STATES/STATE[countrycode='']">
                      <option value="{statemasterid}">
                        <xsl:if test="statemasterid=$stateInput"><xsl:attribute name="SELECTED"/></xsl:if>
                        <xsl:value-of select="statename"/>
                      </option>
                    </xsl:for-each>
                  </select>
                </xsl:when>
                <xsl:otherwise>
                  <input class="TblText" type="text" name="stateInput" id="stateInput" tabindex="2" size="20" maxlength="50" value=""/>
                </xsl:otherwise>
              </xsl:choose>
            </td>
          </tr>
          <tr>
            <td align="right" class="label">Zip/Postal Code:</td>
            <td>
              <input type="text" name="zipCodeInput" id="zipCodeInput" class="TblText" tabindex="3" size="5" maxlength="10" value="{$zipCodeInput}"/>
            </td>
          </tr>
          <tr>
            <td></td>
            <td><button type="button" class="BlueButton" style="width:110px;" tabindex="4" border="0" onclick="javascript:reopenPopup()" onkeydown="javascript:onKeyDown()">Search</button></td>
          </tr>
          <tr>
            <td colspan="10"><hr/></td>
          </tr>
          <tr>
            <td class="instruction">&nbsp;Please click on an arrow to select a city.</td>
            <td align="right"><button type="button" class="BlueButton" style="width:110px;" id="cityCloseTop" tabindex="5" border="0" onclick="javascript:populatePage('')" onkeydown="javascript:closeCityLookup('')">Close screen</button></td>
          </tr>
          <tr>
            <table class="LookupTable" width="100%" border="1" cellpadding="2" cellspacing="2">
              <tr>
                <td width="5%" class="label"> &nbsp; </td>
                <td class="label" valign="bottom">City</td>
                <td class="label" valign="bottom">State</td>
                <td width="65%" class="label" valign="bottom">Zip/Postal Code</td>
              </tr>
              <xsl:for-each select="zipList/zip">
                <tr>
                  <td width="5%">
                    <img tabindex="6" src="images/selectButtonRight.gif" onclick="javascript:populatePage('{city}', '{zipcode}', '{state}');" onkeydown="javascript:closeCityLookup('{city}', '{zipcode}', '{state}');"/>
                  </td>
                  <td align="left"><xsl:value-of select="city"/></td>
                  <td><xsl:value-of select="state"/></td>
                  <td><xsl:value-of select="zipcode"/></td>
                </tr>
              </xsl:for-each>
              <tr>
                <td>
                  <img tabindex="7" src="images/selectButtonRight.gif" border="0" onclick="javascript:populatePage('');" onkeydown="javascript:closeCityLookup('','','')"></img>
                </td>
                <td colspan="8"> None of the above </td>
              </tr>
            </table>
          </tr>
          <tr>
            <td align="right">
              <button type="button" class="BlueButton" style="width:110px;" border="0" onclick="javascript:populatePage('')" onkeydown="javascript:closeCityLookup('','','')">Close screen</button>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  </form>

</body>
</html>

</xsl:template>
</xsl:stylesheet>