
<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="header.xsl"/>
	<xsl:import href="footer.xsl"/>
  <xsl:import href="securityanddata.xsl" />
  <xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>
	<xsl:output method="html" indent="no"/>
	<xsl:output indent="yes"/>
	<xsl:key name="pagedata" match="root/pageData/data" use="name" />
    <xsl:key name="security" match="/root/security/data" use="name"/>
	<xsl:template match="/">
		<html>
			<head>
				<META http-equiv="Content-Type" content="text/html"/>
				<title>View Letter</title>
				<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
				<link rel="stylesheet" type="text/css" href="css/calendar.css"/>
				<script type="text/javascript" src="js/util.js"></script>
        <script type="text/javascript" src="js/tabIndexMaintenance.js"></script>
				<script type="text/javascript" src="js/calendar.js"></script>
				<script type="text/javascript" src="js/clock.js"></script>
			  <script type="text/javascript" src="js/commonUtil.js"></script>
				<script type="text/javascript" src="js/mercuryMessage.js"></script>
				<script type="text/javascript">
// init code
function init(){
  setViewLetterIndexes();
}

/*******************************************************************************
*  setViewEmailIndexes()
*  Sets tabs for View Email Page. 
*  See tabIndexMaintenance.js
*******************************************************************************/
function setViewLetterIndexes()
{
  untabAllElements();
	var begIndex = findBeginIndex();
 	var tabArray = new Array('searchAction', 'sendBtn', 'commBtn', 'backButton',
  'mainMenu', 'printer');
  var newBegIndex = setTabIndexes(tabArray, begIndex);
  return newBegIndex;
}

<![CDATA[
function doBackAction() {	
	doOrderSearchAction();
}

function doOrderSearchAction()
{
  var form = document.forms[0];
  form.action = "customerOrderSearch.do" + 
                   "?action=customer_account_search" +
                   "&order_number="+form.external_order_number.value;
                   "&customer_id=" + form.customer_id.value;
             
  document.forms[0].action_type.value="";
  document.forms[0].order_detail_id.value="";       
  
  form.submit();
}

function sendLetter(){
	document.forms[0].action="loadSendMessage.do";
	document.forms[0].action_type.value="load_titles";
	document.forms[0].submit();
}

function doCommunication() {
	var form = document.forms[0];
	form.action = "displayCommunication.do?order_detail_id=" + form.order_detail_id.value;
	document.forms[0].action_type.value="display_communication";
	
	form.submit();
}

]]>
	</script>
</head>
<body onload="init();">
	<xsl:call-template name="header">
		<xsl:with-param name="headerName"  select="'View Letter'" />
		<xsl:with-param name="showTime" select="true()"/>
		<xsl:with-param name="showBackButton" select="true()"/>
		<xsl:with-param name="showPrinter" select="true()"/>
		<xsl:with-param name="showSearchBox" select="false()"/>
		<xsl:with-param name="dnisNumber" select="$call_dnis"/>
		<xsl:with-param name="cservNumber" select="$call_cs_number"/>
		<xsl:with-param name="indicator" select="$call_type_flag"/>
		<xsl:with-param name="brandName" select="$call_brand_name"/>
		<xsl:with-param name="showCSRIDs" select="true()"/>
		<xsl:with-param name="backButtonLabel" select="'(B)ack to Order'"/>
		<xsl:with-param name="cdispEnabledFlag" select="$cdisp_enabledFlag"/>
	</xsl:call-template>
	<form name="viewLetter" method="post" action="">
    <xsl:call-template name="securityanddata"/>
    <xsl:call-template name="decisionResultData"/>
		<input type="hidden" name="action_type" id="action_type" value="{key('pagedata','action_type')/value}"/>
		<input type="hidden" name="message_type" id="message_type" value="{key('pagedata','message_type')/value}"/>
		<input type="hidden" name="order_detail_id" id="order_detail_id" value="{root/PointOfContact/poc/order_detail_id}"/>
		<input type="hidden" name="master_order_number" id="master_order_number" value="{root/PointOfContact/poc/master_order_number}"/>
		<input type="hidden" name="external_order_number" id="external_order_number" value="{root/PointOfContact/poc/external_order_number}"/>
		<input type="hidden" name="customer_first_name" id="customer_first_name" value="{root/PointOfContact/poc/first_name}"/>
		<input type="hidden" name="customer_last_name" id="customer_last_name" value="{root/PointOfContact/poc/last_name}"/>
		<input type="hidden" name="customer_email_address" id="customer_email_address" value=""/>
		<input type="hidden" name="customer_id" id="customer_id" value="{key('pagedata', 'customer_id')/value}"/>
		<input type="hidden" name="message_title" id="message_title" value="{root/PointOfContact/poc/letter_title}"/>
		<input type="hidden" name="message_content" id="message_content" value="{root/PointOfContact/poc/body}"/>
		
		<table border="0" cellpadding="0" cellspacing="0" width="98%" align="center">
			<tr>
				<td id="lockError" class="ErrorMessage"></td>
			</tr>
		</table>
		<table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
			<tr>
				<td>
					<table width="100%" border="0" align="center" cellpadding="2" cellspacing="2">
						<tr>
							<td>
								<div style="overflow:auto; width:100%; height=265;">
									<table width="100%" border="0" align="left" cellpadding="2" cellspacing="0">
										<xsl:for-each select="root/PointOfContact/poc">
										<tr>
											<th class="label" width="10%" align="center">User&nbsp;ID</th>
											<th class="label" width="10%" align="center">Line&nbsp;#</th>
											<th class="label" width="10%" align="center">Date</th>
											<th class="label" width="10%" align="center">Time</th>
											<th class="label" width="30%" align="center">Title</th>
										</tr>
										<tr>
											<td align="center"><xsl:value-of select="created_by"/></td>
											<td align="center"><xsl:value-of select="key('pagedata','message_line_number')/value"/></td>
											<td align="center"><xsl:value-of select="substring(created_on, 0, 11)"/></td>
											<td align="center"><xsl:value-of select="substring(created_on, 11, 9)"/></td>
											<td align="center"><xsl:value-of select="letter_title"/></td>
										</tr>
										<tr><td colspan="7">&nbsp;</td></tr>
										<tr>
											<td align="center" rowspan="13" colspan="6"><textarea readonly="readonly" rows="14" cols="130"><xsl:value-of select="body"/></textarea></td>
										</tr>
										</xsl:for-each>	
									</table>
								</div>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	
	<iframe id="lockFrame" src="" width="0" height="0" frameborder="0"/>
	</form>
	<table width="98%" align="center">
		<tr>
			<td width="75%" valign="top">
        <button type="button" style="width:90" accesskey="" class="bigBlueButton" name="searchAction" onClick="javascript:doOrderSearchAction();">Recipient/<br/>Order</button>
				<input name="sendBtn" id="sendBtn" style="width:90" accesskey="S" class="bigBlueButton" type="button" value="(S)end Letter" onClick="sendLetter()" />
				<input name="commBtn" id="commBtn" style="width:90" accesskey="O" class="bigBlueButton" type="button" value="C(o)mmunication" onClick="doCommunication()" />
			</td>
			<td width="20%" align="right">
				<button type="button" class="blueButton" accesskey="B" name="backButton" tabindex="9" onclick="javascript:doBackAction();">(B)ack to Order</button>
  				<br />
 				<button type="button" class="bigBlueButton" accesskey="M" name="mainMenu" tabindex="10" onclick="javascript:doMainMenuAction();">(M)ain<br/>Menu</button>
  			</td>
  		</tr>
	</table>
	<div id="waitDiv" style="display:none">
		<table id="waitTable" width="98%" border="3" height="100%" align="center" cellpadding="30" cellspacing="1" bordercolor="#006699">
			<tr>
				<td width="100%">
					<table width="100%" cellspacing="0" cellpadding="0">
						<tr>
							<td id="waitMessage" align="right" width="50%" class="waitMessage"></td>
							<td id="waitTD" width="50%" class="waitMessage"></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>
	<xsl:call-template name="footer"></xsl:call-template>
</body>
</html>
</xsl:template>
</xsl:stylesheet>
<!-- Stylus Studio meta-information - (c) 2004-2005. Progress Software Corporation. All rights reserved.
<metaInformation>
<scenarios ><scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="..\..\..\..\..\dnisMaint.xml" htmlbaseurl="" outputurl="" processortype="internal" profilemode="0" profiledepth="" profilelength="" urlprofilexml="" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext=""/></scenarios><MapperMetaTag><MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/><MapperBlockPosition></MapperBlockPosition></MapperMetaTag>
</metaInformation>
-->