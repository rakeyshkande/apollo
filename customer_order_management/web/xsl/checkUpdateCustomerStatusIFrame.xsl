<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="securityanddata.xsl"/>
<xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>

<xsl:key name="pageData" match="/root/pageData/data" use="name"/>
<xsl:key name="security" match="/root/security/data" use="name"/>
	
<xsl:output method="html" indent="yes"/>
<xsl:template match="/root">
<html>
<head>
<script type="text/javascript" language="javascript">
var displayValue = '<xsl:value-of select="key('pageData','message_display')/value"/>';
<![CDATA[
	function checkCustomerStatus()
	{
    if(displayValue == '')
    {    
      parent.doProcessUpdateCustomer();
    }
    else
    {
      alert(displayValue);
    }
	}
]]>
</script>
</head>
<body onload="javascript:checkCustomerStatus();">
    <xsl:call-template name="securityanddata"/>   
    <xsl:call-template name="decisionResultData"/>
</body>
</html>
</xsl:template>
</xsl:stylesheet>