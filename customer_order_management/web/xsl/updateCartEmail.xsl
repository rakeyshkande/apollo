<!DOCTYPE ACDemo[
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:import href="header.xsl"/>
  <xsl:import href="footer.xsl"/>
  <xsl:import href="securityanddata.xsl"/>
  <xsl:import href="/decisionresult/includes/decisionResultData.xsl"/>
  <xsl:output method="html" indent="yes"/>
  <xsl:key name="pagedata" match="root/pageData/data" use="name"/>
  <xsl:key name="security" match="/root/security/data" use="name"/>
  <xsl:template match="/">
    <html>
      <head>
        <META http-equiv="Content-Type" content="text/html"/>
        <title>TAG Screen</title>
        <link rel="stylesheet" type="text/css" href="css/ftd.css"/>
        <base target="_self"/>
        <script type="text/javascript" src="js/clock.js">
        </script>
        <script type="text/javascript" src="js/util.js">
        </script>
        <script type="text/javascript" src="js/commonUtil.js">
        </script>
        <script type="text/javascript" language="JavaScript">
		
		var count = 0;
				
		
          
          
          
          <![CDATA[
          		
          		
          		var cos_show_previous = "true";
          		var cos_show_next = "true";
          		var cos_show_first = "true";
          		var cos_show_last = "true";
          		
          		function checkKey()
          		{
          		  var tempKey = window.event.keyCode;
          		
          		  //Up arrow pressed
          		  if (window.event.altKey && tempKey == 38 && cos_show_first == "true")
          		  {
          		    loadEmailsAction(--cartEmails.page_position.value);
          		  }
          		
          		  //Down arrow pressed
          		  if (window.event.altKey && tempKey == 40 && cos_show_last == "true")
          		  {
          		    loadEmailsAction(++cartEmails.page_position.value);
          		  }
          		
          		  //Right arrow pressed
          		  if (window.event.altKey && tempKey == 39 && cos_show_next == "true")
          		  {
          		    loadEmailsAction(cartEmails.page_position.value = cartEmails.total_pages.value);
          		  }
          		
          		  //Left arrow pressed
          		  if (window.event.altKey && tempKey == 37 && cos_show_previous == "true")
          		  {
          		    loadEmailsAction(cartEmails.page_position.value = 1);
          		  }
          		}
          		
          		
          		
          		function init()
          		{
          			document.getElementById('printer').tabIndex = 1007;
          			
          			]]>
          <xsl:if test="/root/out-parameters/OUT_LOCK_OBTAINED = 'N'">
		      	document.getElementById("saveAction").disabled=true;
		      	replaceText("lockError", "Current record is locked by 
            
            
            
            <xsl:value-of select="/root/out-parameters/OUT_LOCKED_CSR_ID"/>



");
			</xsl:if>
          <![CDATA[
          		}
          		
          		// replace an html element value with new text
          		function replaceText( elementId, newString ){
          		   	var node = document.getElementById( elementId );
          		   	var newTextNode = document.createTextNode( newString );
          		   	if(node.childNodes[0]!=null){
          		    	node.removeChild( node.childNodes[0] );
          		   	}
          		   	node.appendChild( newTextNode );
          		}
          		
          		function updateEmail()
          		{
          			var doaction;
          	
          			if(document.forms[0].change_flag.value=="Y"){
          				doaction = doExitPageAction("Are you sure you want to add the new email address and remove the old email address?");
          		  	}else{
          		    	doaction = true;
          		    }
          				


          			if(doaction == true)
          			{	    
          				var selectedEmail = '';			

          				for(var i=0;i<document.forms[0].email_address.length;i++)
          				{
          					if(document.forms[0].email_address[i].checked)
          					{
          						selectedEmail = document.forms[0].email_address[i].value;
          					}
          				}
          				var url = "cartEmailAction.do?action_type=update&email_address=" + selectedEmail + "&order_guid=" + document.forms[0].order_guid.value + "&premier_circle_membership=" + document.forms[0].premier_circle_membership.value + "&securitytoken="+document.forms[0].securitytoken.value+"&context="+document.forms[0].context.value+"&applicationcontext="+document.forms[0].applicationcontext.value;
          				document.getElementById("updateEmailFrame").src=url;
          			}
          		}
          		
          		//Disable buttons
          		function buttonDis(page){
          		
          			setNavigationHandlers();
          			
          		   	if (cartEmails.total_pages.value <= 1){
          		   		cos_show_next = "false";
          				cos_show_last = "false";
          				cos_show_previous = "false";
            				cos_show_first = "false";
          		      	document.getElementById("firstItem").disabled= "true";
          		      	document.getElementById("backItem").disabled= "true";
          		      	document.getElementById("nextItem").disabled= "true";
          		      	document.getElementById("lastItem").disabled= "true";
          		      	return;
          		    }
          		   	if (page < 2){
          		   		cos_show_previous = "false";
            				cos_show_first = "false";
          		      	document.getElementById("firstItem").disabled= "true";
          		      	document.getElementById("backItem").disabled= "true";
          		      	return;
          		    }
          			if (page == cartEmails.total_pages.value){
          				cos_show_next = "false";
          				cos_show_last = "false";
          		    	document.getElementById("nextItem").disabled= "true";
          		   		document.getElementById("lastItem").disabled= "true";
          		    	return;
          		  	}
          		}
          		
          		function loadEmailsAction(newPagePosition)
          		{
          			  var form = document.forms[0];
          			  if(isExitSafe()==true)
          			  {
          				  	form.page_position.value = newPagePosition;
          				    var url = 'cartEmailAction.do?action_type=load';
          					form.action = url;
          					form.submit();
          			  }
          		}
          		
          		function isExitSafe()
          		{
          			var form = document.forms[0];
          		  	if(form.change_flag.value=="Y"){
          		    	doaction = doExitPageAction("Your changes have not been saved. Do you wish to continue?");
          		  	}else{
          		    	doaction = true;
          		    }
          		  	return doaction;
          		}
          		
          		function emailChange()
          		{
          			document.cartEmails.change_flag.value="Y";
          		}
          		
          		function cancelUpdate()
          		{
          			if(isExitSafe()==true)
          			{
          			  top.close();
          			}
          		}
           		]]>
        </script>
      </head>
      <body onload="javascript:init();buttonDis(cartEmails.page_position.value);"
            onkeydown="javascript:checkKey();">
        <form name="cartEmails" method="post" action="">
          <iframe id="updateEmailFrame" name="updateEmailFrame" height="0"
                  width="0" border="0">
          </iframe>
          <xsl:call-template name="securityanddata"/>
          <xsl:call-template name="decisionResultData"/>
          
          
          <input type="hidden" name="email_address" id="email_address"
                 value="place holder for email address"/>          
          
          
          <input type="hidden" name="order_guid" id="order_guid"
                 value="{/root/pageData/data[name='order_guid']/value}"/>
          <input type="hidden" name="page_position" id="page_position"
                 value="{key('pagedata','page_position')/value}"/>
          <input type="hidden" name="total_pages" id="total_pages"
                 value="{key('pagedata', 'total_pages')/value}"/>
          <input type="hidden" name="start_position" id="start_position"
                 value="{key('pagedata', 'start_position')/value}"/>
          <input type="hidden" name="change_flag" id="change_flag" value="N"/>
          <!-- premier circle membership -->
  		  <input type="hidden" id="premier_circle_membership" name="premier_circle_membership" value="{key('pagedata','premier_circle_membership')/value}"/>
          <!-- Header-->
          <xsl:call-template name="header">
            <xsl:with-param name="headerName"
                            select="'Change Shopping Cart Email Address'"/>
            <xsl:with-param name="dnisNumber" select="$call_dnis"/>
            <xsl:with-param name="brandName" select="$call_brand_name"/>
            <xsl:with-param name="indicator" select="$call_type_flag"/>
            <xsl:with-param name="cservNumber" select="$call_cs_number"/>
            <xsl:with-param name="showPrinter" select="true()"/>
            <xsl:with-param name="showTime" select="true()"/>
            <xsl:with-param name="cdispEnabledFlag" select="$cdisp_enabledFlag"/>
          </xsl:call-template>
          <!-- Display table-->
          <table width="98%" align="center" cellspacing="1" class="mainTable">
            <tr>
              <td>
                <table width="100%" align="center" class="innerTable">
                  <tr>
                    <td>
                      <div style="overflow:auto; width:100%;height:150px; padding:0px; margin:0px;word-break:break-all;">
                        <table width="100%" align="center">
                          <tr>
                            <td id="lockError" class="ErrorMessage"/>
                          </tr>
                          <tr class="script">
                            <td align="center">
                              <table border="0" cellspacing="0" cellpadding="0">
                                <tr class="Label">
                                  <td>
                                  </td>
                                  <td width="8">
                                  </td>
                                  <td>Email Address</td>
                                </tr>
                                <tr height="4">
                                  <td>
                                  </td>
                                </tr>
                                <xsl:for-each select="root/email_addresses/email">
                                  <tr>
                                    <td>
                                      <xsl:choose>
                                        <xsl:when test="current_order_email = 'Y'">
                                          <input type="radio"
                                                 name="email_address"
                                                 value="{email_address}"
                                                 checked="checked"
                                                 onChange="emailChange();"
                                                 tabindex="{position()}"/>
                                        </xsl:when>
                                        <xsl:otherwise>
                                          <input type="radio"
                                                 name="email_address"
                                                 value="{email_address}"
                                                 onChange="emailChange();"
                                                 tabindex="{position()}"/>
                                        </xsl:otherwise>
                                      </xsl:choose>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                      <xsl:value-of select="email_address"/>
                                    </td>
                                  </tr>
                                </xsl:for-each>
                              </table>
                            </td>
                          </tr>
                        </table>
                      </div>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
          <table width="98%" border="0" align="center" cellpadding="0"
                 cellspacing="1">
            <tr>
              <td align="center">
                <xsl:if test="count(//email_addresses/email) != 0">
                  <button type="button" class="BlueButton" name="saveAction" accesskey="H"
                          tabindex="1001" onclick="updateEmail();">C(h)ange Email</button>
                </xsl:if>
          			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                
                <button type="button" class="BlueButton" accesskey="C" tabindex="1002"
                        onclick="cancelUpdate();">(C)ancel</button>
              </td>
            </tr>
            <tr>
              <td colspan="6" align="right">
                <xsl:choose>
                  <xsl:when test="key('pagedata','total_pages')/value !='0'">
                    <button type="button" name="firstItem" tabindex="1005" class="arrowButton"
                            id="firstItem" onclick="loadEmailsAction(1);">
                          											7
                              		</button>
                  </xsl:when>
                  <xsl:when test="key('pagedata','total_pages')/value ='0'">
                    <button type="button" name="firstItem" tabindex="1005" class="arrowButton"
                            id="firstItem" onclick="">
                              			7
                              		</button>
                  </xsl:when>
                </xsl:choose>
                <xsl:choose>
                  <xsl:when test="key('pagedata','total_pages')/value !='0'">
                    <button type="button" name="backItem" tabindex="1003" class="arrowButton"
                            id="backItem"
                            onclick="loadEmailsAction(cartEmails.page_position.value-1);">
                              				3
                              			</button>
                  </xsl:when>
                  <xsl:when test="key('pagedata','total_pages')/value ='0'">
                    <button type="button" name="backItem" tabindex="1003" class="arrowButton"
                            id="backItem" onclick="">
                              				3
                              			</button>
                  </xsl:when>
                </xsl:choose>
                <xsl:choose>
                  <xsl:when test="key('pagedata','total_pages')/value !='0'">
                    <button type="button" name="nextItem" tabindex="1004" class="arrowButton"
                            id="nextItem"
                            onclick="x=cartEmails.page_position.value;loadEmailsAction(++x);">
                              				4
                              				</button>
                  </xsl:when>
                  <xsl:when test="key('pagedata','total_pages')/value ='0'">
                    <button type="button" name="nextItem" tabindex="1004" class="arrowButton"
                            id="nextItem" onclick="">
                              				4
                              			</button>
                  </xsl:when>
                </xsl:choose>
                <xsl:choose>
                  <xsl:when test="key('pagedata','total_pages')/value !='0'">
                    <button type="button" name="lastItem" tabindex="1006" class="arrowButton"
                            id="lastItem"
                            onclick="loadEmailsAction(cartEmails.total_pages.value);">
                            			8
                            		</button>
                  </xsl:when>
                  <xsl:when test="key('pagedata','total_pages')/value ='0'">
                    <button type="button" name="lastItem" tabindex="10069" class="arrowButton"
                            id="lastItem" onclick="">
                             				8
                              			</button>
                  </xsl:when>
                </xsl:choose>
              </td>
            </tr>
          </table>
          <!--Copyright bar-->
          <xsl:call-template name="footer"/>
        </form>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
