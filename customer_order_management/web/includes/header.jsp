<%@ taglib uri="http://displaytag.sf.net" prefix="display" %> 
<%@ taglib uri = "http://java.sun.com/jstl/core" prefix = "c"%>
<%@ page contentType="text/html;charset=windows-1252"%>
<html>     
<head>    
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">         
<meta http-equiv="expires" content="-1">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">     
<title>Queue Message Processing Status</title>     
<script type="text/javascript" src="js/clock.js"></script>    
<script type="text/javascript" src="js/prototype.js"></script>
<script type="text/javascript" src="js/queueMsgProcessingStatus.js"></script>
<script type="text/javascript" src="js/util.js"></script>
<style type="text/css" media="all">
      @import url("css/display.css");@import url("css/pages.css");
</style>
</head>     
<body onLoad="doOnLoad();">

<div id="headerDiv" class="header">
  <table width="100%" border="0" cellspacing="2" cellpadding="0">
    <tbody>
      <tr> 
        <td width="150px" align="left" style="float:left;vertical-align:middle;padding-top:.4em;"><img src="images/wwwftdcom_131x32.gif" alt="ftd.com" border="0" height="32"></td>
        <td align="center" class="pageHeader">
          Queue Message Processing Status
        </td>
        <td width="150px" align="right" style="vertical-align:middle;padding-top:.4em;" class="clock">
            <span id="time"> <script type="text/javascript">startClock();</script></span>
        </td>
      </tr>			
      <tr>
        <td colspan="3"><hr></td>
      </tr>
    </tbody>
  </table>
</div>
