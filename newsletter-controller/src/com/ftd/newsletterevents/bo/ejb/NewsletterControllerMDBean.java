package com.ftd.newsletterevents.bo.ejb;

import javax.ejb.EJBException;
import javax.ejb.MessageDrivenBean;
import javax.ejb.MessageDrivenContext;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import javax.naming.InitialContext;

import com.ftd.newsletterevents.core.ErrorService;
import com.ftd.newsletterevents.core.IConstants;
import com.ftd.osp.framework.dispatcher.Dispatcher;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.MessageToken;

public class NewsletterControllerMDBean implements MessageDrivenBean, MessageListener
{
    public NewsletterControllerMDBean() {
    	try
    	{
    		logger = new Logger(NewsletterControllerMDBean.class.getName());
    	} catch (Throwable t) {
    		System.out.println(t.getMessage());
    		t.printStackTrace();
    		
    	}
	}

	private static Logger logger;
    protected final static String ORIGIN_APP_COM = "customerordermanagement";
    protected final static String ORIGIN_APP_NN  = "newsletternovator";
    protected final static String MESSAGE_STATUS_COM = "NEWS SUB COM";
    protected final static String MESSAGE_STATUS_NN  = "NEWS SUB NN";
    
    private MessageDrivenContext context;

    public void ejbCreate()
    {
    }

    public void onMessage(Message msg)
    {
        logger.debug("onMessage: message received.");

        try
        {
            // Filter the message based on metadata
            String msgOrigin = msg.getStringProperty(IConstants.SELECTOR_KEY_ORIGIN_APP_NAME);
            if (!(msg instanceof TextMessage))
            {
                throw new IllegalArgumentException("Invalid Message type " + msg.getClass().getName() + " expecting TextMessage");
            }
            TextMessage textMessage = (TextMessage) msg;
            
            boolean sendNN = false;
            boolean sendCOM = false;
            
            if (msgOrigin.equals(ORIGIN_APP_COM))
            {
                sendNN = true;
            }
            else if (msgOrigin.equals(ORIGIN_APP_NN))
            {
                sendCOM = true;
            }
            else
            {
                sendNN = true;
                sendCOM = true;
            }
            if (logger.isDebugEnabled())
            {
                StringBuffer sb = new StringBuffer();
                sb.append("Sending message from ");
                sb.append(msgOrigin);
                sb.append(" to ");
                if (sendNN)
                    sb.append("NN ");
                if (sendCOM)
                    sb.append("COM ");
                if (!sendNN && !sendCOM)
                {
                    sb.append("Nowhere! ");
                }
                logger.debug(sb.toString());
            }

            if (sendNN)
            {
                sendMessage(textMessage,MESSAGE_STATUS_NN);
            }
            if (sendCOM)
            {
                sendMessage(textMessage,MESSAGE_STATUS_COM);
            }
        } catch (Throwable e)
        {
            logger.error("onMessage: Failed.", e);

            // Handle the error.
            ErrorService.process(ErrorService.ERROR_TYPE_SUBSCRIBE_FAILURE, e);
            context.setRollbackOnly();
           	throw new RuntimeException("Rollback detected, exception thrown to force rollback.");
        }
    }

    public void ejbRemove()
    {
    }

    public void setMessageDrivenContext(MessageDrivenContext ctx)
    {
        this.context = ctx;
    }

    protected void sendMessage(TextMessage msg, String status ) throws Exception
    {
        MessageToken messageToken = new MessageToken();
        messageToken.setMessage(msg.getText());          
        messageToken.setStatus(status);
        Dispatcher dispatcher = Dispatcher.getInstance();       
        dispatcher.dispatchTextMessage(new InitialContext(),messageToken);  
    }
}
