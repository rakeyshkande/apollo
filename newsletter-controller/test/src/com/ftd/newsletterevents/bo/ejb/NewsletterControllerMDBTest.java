package com.ftd.newsletterevents.bo.ejb;

import static junit.framework.Assert.assertTrue;
import static org.easymock.EasyMock.anyObject;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;

import javax.jms.TextMessage;
import javax.naming.InitialContext;

import org.easymock.EasyMock;
import org.easymock.IAnswer;
import org.junit.Test;

import com.ftd.newsletterevents.core.IConstants;
import com.ftd.osp.framework.dispatcher.Dispatcher;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.MessageToken;

public class NewsletterControllerMDBTest {
	
	Logger log = new Logger(NewsletterControllerMDBTest.class.getName());
	
	private String message = "test message";
	
	private String outputStatus = null;
	
	NewsletterControllerMDBean newsletterController;
	
	private void setup() throws Exception {
		
		//we are overriding the sendMessage method of this class
		// so that it doesn't invoke the dispatcher.
		// we will take the output status and save it so we can test it.
		this.newsletterController = new NewsletterControllerMDBean(){
			protected void sendMessage(TextMessage msg, String status ) {
				outputStatus = status;	
			}
		};
	}
	
	@Test
	public void testReceiveCOMMessage() throws Exception {
		log.info("testing receipt of customer order mgmt message");
		
		setup();

		//create the message and send it
		TextMessage txt = EasyMock.createMock(TextMessage.class);
		expect(txt.getStringProperty(IConstants.SELECTOR_KEY_ORIGIN_APP_NAME))
			.andReturn(NewsletterControllerMDBean.ORIGIN_APP_COM)
			.anyTimes();
		expect(txt.getText()).andReturn("hey").anyTimes();
		replay(txt);
		newsletterController.onMessage(txt);
		
		assertTrue(this.outputStatus.equals(NewsletterControllerMDBean.MESSAGE_STATUS_NN));
	}
	
	@Test
	public void testReceiveNovatorMessage() throws Exception {
		log.info("testing receipt of Newsletter Novator message");
		
		setup();
		
		//create the message and send it
		TextMessage txt = EasyMock.createMock(TextMessage.class);
		expect(txt.getStringProperty(IConstants.SELECTOR_KEY_ORIGIN_APP_NAME))
			.andReturn(NewsletterControllerMDBean.ORIGIN_APP_NN)
			.anyTimes();
		expect(txt.getText()).andReturn("hey").anyTimes();
		replay(txt);
		newsletterController.onMessage(txt);
		
		assertTrue(this.outputStatus.equals(NewsletterControllerMDBean.MESSAGE_STATUS_COM));
	}

}
