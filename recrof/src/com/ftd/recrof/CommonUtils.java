package com.ftd.recrof;

import com.ftd.osp.utilities.ConfigurationUtil; 
import com.ftd.osp.utilities.plugins.Logger; 
import com.ftd.osp.utilities.systemmessenger.SystemMessenger; 
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO; 
import com.ftd.osp.utilities.vo.MessageToken; 
import java.sql.Connection; 
import java.util.Calendar; 
import java.util.Date; 
import javax.naming.Context; 
import javax.naming.InitialContext; 
import javax.naming.NamingException; 
import javax.sql.DataSource;  

/**  *   * @author Jason Weiss  */ 

public class CommonUtils  {   
    private static final String CONFIG_FILE = "recrof-config.xml";
    
    public CommonUtils(){        
    }
    
    private static String LOGGER_CATEGORY = "com.ftd.recrof.CommonUtilites";
    
    /*     * Get database connection    */   
    
    static public Connection getConnection() throws Exception   {     
        return getConnection("DATABASE_CONNECTION");   
    }
    
    static public Connection getConnection(String connectionName) throws Exception   {          
        ConfigurationUtil config = ConfigurationUtil.getInstance();                 
        String dbConnection = config.getProperty(CONFIG_FILE,connectionName);                  
        //get DB connection     
        DataSource dataSource = (DataSource)lookupResource(dbConnection);     
        return dataSource.getConnection();     
    }             
    
    static public DataSource getDataSource() throws Exception    {     
        ConfigurationUtil config = ConfigurationUtil.getInstance();                 
        String dbConnection = config.getProperty(CONFIG_FILE,"DATABASE_CONNECTION");                  
        //get DB connection     
        DataSource dataSource = (DataSource)lookupResource(dbConnection);     
        return dataSource;        
    }        
    
    /**    * This method sends a message to the System Messages table.    *     
     * @param message    
     * @return message id    
     */   
     
     static public String sendSystemMessage(String message) throws Exception   {
        Logger logger =  new Logger(LOGGER_CATEGORY);
        logger.error("Sending System Message:" + message);
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
        String messageSource = configUtil.getProperty(CONFIG_FILE,"MESSAGE_SOURCE");
        String messageID = "";        
        //build system vo     
        SystemMessengerVO sysMessage = new SystemMessengerVO();
        sysMessage.setLevel(SystemMessengerVO.LEVEL_PRODUCTION);
        sysMessage.setSource(messageSource);
        sysMessage.setType("ERROR");
        sysMessage.setMessage(message);
        SystemMessenger sysMessenger = SystemMessenger.getInstance();
        Connection conn = getConnection();     
        try {       
            messageID = sysMessenger.send(sysMessage,conn);
            if(messageID == null) {
                String msg = "Error occured while attempting to send out a system message. Msg not sent: " + message;
                logger.error(msg);         
                System.out.println(msg);
            }
        } finally {
            conn.close();     
        }        
        return messageID;     
    }          
    
    /**    
     * Returns a transactional resource from the EJB container.    
     *     
     * @param jndiName    
     * @return     
     * @throws javax.naming.NamingException    
     */   
     
     public static Object lookupResource(String jndiName)
            throws NamingException   {  
            
        InitialContext initContext = null;     
        try     {
            initContext = new InitialContext(); 
            //      Context myenv = (Context) initContext.lookup("java:comp/env"); 
            //      System.out.println("Resource lookup env:" + myenv.getEnvironment());
            return initContext.lookup(jndiName);
        } finally  {
            try  {
                initContext.close();       
            } catch (Exception ex)  {
                // do nothing
            } 
            finally  {
            }     
        }   
    }     
    
}
