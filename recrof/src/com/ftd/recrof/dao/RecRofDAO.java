package com.ftd.recrof.dao;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.recrof.vo.CustomerVO;
import com.ftd.recrof.vo.MercuryVO;
import com.ftd.recrof.vo.OrderDetailVO;

import java.sql.Connection;

import java.sql.Timestamp;

import java.text.DateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class RecRofDAO {

    private Connection conn;
    private String status;
    private String message;
    private Logger logger;  
    
    public RecRofDAO(Connection connParm) {
        conn = connParm;
        logger = new Logger("com.ftd.recrof.dao.RecRofDAO");
    }

    /**
     * This method is a wrapper for the SP_GET_CUSTOMER SP.   
     * It populates a customer VO based on the returned record set.
     * 
     * @param String - customerId
     * @return CustomerVO
     */
    public CustomerVO getCustomer(long customerId) throws Exception
    {
      DataRequest dataRequest = new DataRequest();
      CustomerVO customer = new CustomerVO();
      boolean isEmpty = true;
      CachedResultSet outputs = null;
      CachedResultSet outputs2 = null;
      
      try
      {
        logger.debug("getCustomer (long customerId ("+customerId+")) :: CustomerVO");
        
        /* setup store procedure input parameters */
        HashMap inputParams = new HashMap();
        inputParams.put("CUSTOMER_ID", new Long(customerId));
        
        /* build DataRequest object */
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_CUSTOMER");
        dataRequest.setInputParams(inputParams);
        
        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        outputs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
        
        /* populate object */
        customer.setCustomerId(new Long(customerId).longValue());     
        while (outputs.next())
        {
          isEmpty = false;
          customer.setConcatId(outputs.getString("CONCAT_ID"));
          customer.setFirstName(outputs.getString("CUSTOMER_FIRST_NAME"));
          customer.setLastName(outputs.getString("CUSTOMER_LAST_NAME"));
          customer.setBusinessName(outputs.getString("BUSINESS_NAME"));
          customer.setAddress1(outputs.getString("CUSTOMER_ADDRESS_1"));
          customer.setAddress2(outputs.getString("CUSTOMER_ADDRESS_2"));
          customer.setCity(outputs.getString("CUSTOMER_CITY"));
          customer.setState(outputs.getString("CUSTOMER_STATE"));
          
          // fixed to deal with international zip_code of N/A
          
          String zip_code = outputs.getString("CUSTOMER_ZIP_CODE");
          if(zip_code != null) {
            zip_code = zip_code.substring(0, zip_code.length() >= 5 ? 5 : zip_code.length());
            customer.setZipCode(zip_code);
          } 
          
          customer.setCountry(outputs.getString("CUSTOMER_COUNTRY"));       
          customer.setAddressType(outputs.getString("address_type"));
        }
        
        /* setup store procedure input parameters */
        HashMap inputParams2 = new HashMap();
        inputParams2.put("CUSTOMER_ID", new Long(customerId).toString());
        
      }
      catch (Exception e) 
      {            
          logger.error(e);
          throw e;
      } 
      if (isEmpty)
        return null;
      else
        return customer;
    }

    /**
     * This method is a wrapper for the GET_ORDER_DETAILS SP.  
     * It populates an Order Detail VO based on the returned record set.
     * 
     * @param String - order detail id
     * @return OrderDetailVO 
     */
    public OrderDetailVO getOrderDetail(String orderDetailID)throws Exception    
    {
      DataRequest dataRequest = new DataRequest();
      OrderDetailVO orderDetail = new OrderDetailVO();
      boolean isEmpty = true;
      CachedResultSet outputs = null;
      DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT);
      
      try
      {
        logger.debug("getOrderDetail (String orderDetailID("+orderDetailID+")) :: OrderDetailVO");
        /* setup store procedure input parameters */
        HashMap inputParams = new HashMap();
        inputParams.put("ORDER_DETAIL_ID", new Long(orderDetailID));

        /* build DataRequest object */
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_ORDER_DETAILS");
        dataRequest.setInputParams(inputParams);
        
        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        outputs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
        
        /* populate object */
        while (outputs.next())
        {        
          isEmpty = false;
          orderDetail.setOrderDetailId(outputs.getLong("ORDER_DETAIL_ID"));
          orderDetail.setDeliveryDate(outputs.getString("DELIVERY_DATE")==null?null:df.parse(outputs.getString("DELIVERY_DATE")));        
          orderDetail.setRecipientId(outputs.getLong("RECIPIENT_ID"));
          orderDetail.setProductId(outputs.getString("PRODUCT_ID"));
          orderDetail.setQuantity(outputs.getLong("QUANTITY"));
          orderDetail.setExternalOrderNumber(outputs.getString("EXTERNAL_ORDER_NUMBER"));
          orderDetail.setColor1(outputs.getString("COLOR_1"));
          orderDetail.setColor2(outputs.getString("COLOR_2"));
          orderDetail.setSubstitutionIndicator(outputs.getString("SUBSTITUTION_INDICATOR"));
          orderDetail.setSameDayGift(outputs.getString("SAME_DAY_GIFT"));
          orderDetail.setOccasion(outputs.getString("OCCASION"));
          orderDetail.setCardMessage(outputs.getString("CARD_MESSAGE"));
          orderDetail.setCardSignature(outputs.getString("CARD_SIGNATURE"));
          orderDetail.setSpecialInstructions(outputs.getString("SPECIAL_INSTRUCTIONS"));
          orderDetail.setReleaseInfoIndicator(outputs.getString("RELEASE_INFO_INDICATOR"));
          orderDetail.setFloristId(outputs.getString("FLORIST_ID"));
          orderDetail.setShipMethod(outputs.getString("SHIP_METHOD"));
          orderDetail.setShipDate(outputs.getDate("SHIP_DATE"));                        
          orderDetail.setOrderDispCode(outputs.getString("ORDER_DISP_CODE"));
          orderDetail.setDeliveryDateRangeEnd(outputs.getString("DELIVERY_DATE_RANGE_END")==null?null:df.parse(outputs.getString("DELIVERY_DATE_RANGE_END")));
          orderDetail.setScrubbedOn(outputs.getDate("SCRUBBED_ON_DATE"));
          orderDetail.setScrubbedBy(outputs.getString("USER_ID"));
          orderDetail.setOrderGuid(outputs.getString("ORDER_GUID"));                       
          orderDetail.setSourceCode(outputs.getString("SOURCE_CODE"));
          orderDetail.setSecondChoiceProduct(outputs.getString("SECOND_CHOICE_PRODUCT"));
          orderDetail.setRejectRetryCount(outputs.getLong("REJECT_RETRY_COUNT"));
          orderDetail.setSizeIndicator(outputs.getString("SIZE_INDICATOR"));
          orderDetail.setSubcode(outputs.getString("SUBCODE"));
          orderDetail.setOpStatus(outputs.getString("OP_STATUS"));
          orderDetail.setCarrierDelivery(outputs.getString("CARRIER_DELIVERY"));
          orderDetail.setCarrierId(outputs.getString("CARRIER_ID"));
          orderDetail.setVenusMethodOfPayment(outputs.getString("METHOD_OF_PAYMENT"));
        }        
      }
      catch (Exception e) 
      {            
        logger.error(e);
        throw e;
      } 
      if (isEmpty)
        return null;
      else
        return orderDetail;
    }

    /**
    * This method serves as a wrapper for the SP_GET_MERCURY_MESSAGE stored
    * procedure.
    *
    * @param messageId String
    * @return vo MercuryVO
    */

    public MercuryVO getMessage(String messageId) throws Exception  {
        DataRequest dataRequest = new DataRequest();
        MercuryVO vo = new MercuryVO();
        boolean isEmpty = true;
        CachedResultSet outputs = null;
        logger.debug("getMessage:: MercuryVO");
        /* setup store procedure input parameters */
        HashMap inputParams = new HashMap();
        inputParams.put("IN_MERCURY_ID", messageId);
             
        /* build DataRequest object */
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_MERCURY_BY_ID");
        dataRequest.setInputParams(inputParams);
        
        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();       
        outputs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
        while (outputs.next())
        {
          isEmpty = false;
          vo.setMercuryId(messageId);
          vo.setMercuryMessageNumber(outputs.getString("MERCURY_MESSAGE_NUMBER"));
          vo.setMercuryOrderNumber(outputs.getString("MERCURY_ORDER_NUMBER"));
          vo.setMercuryStatus(outputs.getString("MERCURY_STATUS"));
          vo.setMessageType(outputs.getString("MSG_TYPE"));
          vo.setOutboundId(outputs.getString("OUTBOUND_ID"));
          vo.setSendingFlorist(outputs.getString("SENDING_FLORIST"));
          vo.setFillingFlorist(outputs.getString("FILLING_FLORIST"));
          vo.setOrderDate(outputs.getDate("ORDER_DATE"));
          vo.setRecipient(outputs.getString("RECIPIENT"));
          vo.setAddress(outputs.getString("ADDRESS"));
          vo.setCityStateZip(outputs.getString("CITY_STATE_ZIP"));
          vo.setPhoneNumber(outputs.getString("PHONE_NUMBER"));
          vo.setDeliveryDate(outputs.getDate("DELIVERY_DATE"));
          vo.setDeliveryDateText(outputs.getString("DELIVERY_DATE_TEXT"));
          vo.setFirstChoice(outputs.getString("FIRST_CHOICE"));
          vo.setSecondChoice(outputs.getString("SECOND_CHOICE"));
          vo.setPrice(new Double(outputs.getDouble("PRICE")));
          vo.setCardMessage(outputs.getString("CARD_MESSAGE"));
          vo.setOccasion(outputs.getString("OCCASION"));
          vo.setSpecialInstructions(outputs.getString("SPECIAL_INSTRUCTIONS"));
          vo.setPriority(outputs.getString("PRIORITY"));
          vo.setOperator(outputs.getString("OPERATOR"));
          vo.setComments(outputs.getString("COMMENTS"));
          vo.setSakText(outputs.getString("SAK_TEXT"));
          vo.setCtseq(outputs.getInt("CTSEQ"));
          vo.setCrseq(outputs.getInt("CRSEQ"));
          vo.setTransmissionDate(outputs.getDate("TRANSMISSION_TIME"));
          vo.setReferenceNumber(outputs.getString("REFERENCE_NUMBER"));
          vo.setProductId(outputs.getString("PRODUCT_ID"));
          vo.setZipCode(outputs.getString("ZIP_CODE"));
          vo.setAskAnswerCode(outputs.getString("ASK_ANSWER_CODE"));
          vo.setSortValue(outputs.getString("SORT_VALUE"));
          vo.setRetrievalFlag(outputs.getString("RETRIEVAL_FLAG"));
          vo.setFromMessageNumber(outputs.getString("FROM_MESSAGE_NUMBER"));
          vo.setToMessageNumber(outputs.getString("TO_MESSAGE_NUMBER"));
          vo.setFromMessageDate(outputs.getDate("FROM_MESSAGE_DATE"));
          vo.setToMessageDate(outputs.getDate("TO_MESSAGE_DATE"));
          vo.setViewQueue(outputs.getString("VIEW_QUEUE"));
          vo.setSuffix(outputs.getString("SUFFIX"));
          vo.setDirection(outputs.getString("MESSAGE_DIRECTION"));
          vo.setOrderSequence(outputs.getString("ORDER_SEQ"));
          vo.setAdminSequence(outputs.getString("ADMIN_SEQ"));
          vo.setCombinedReportNumber(outputs.getString("COMBINED_REPORT_NUMBER"));
          vo.setRofNumber(outputs.getString("ROF_NUMBER"));
          vo.setOverUnderCharge(new Double(outputs.getDouble("OVER_UNDER_CHARGE")));
          vo.setAdjReasonCode(outputs.getString("ADJ_REASON_CODE"));
          vo.setCompOrder(outputs.getString("COMP_ORDER"));
          vo.setRequireConfirmation(outputs.getString("REQUIRE_CONFIRMATION"));
          vo.setOldPrice(new Double(outputs.getDouble("OLD_PRICE")));
        }
        if (isEmpty)
          return null;
        else
          return vo;
    }
    public void markReconciled(String mercuryID) throws Exception
    {
       DataRequest dataRequest = new DataRequest();
       dataRequest.setConnection(conn);
      
       dataRequest.setStatementID("MARK_RECONCILED");
       Map paramMap = new HashMap();   
       
       paramMap.put("IN_MERCURY_ID",mercuryID);

       dataRequest.setInputParams(paramMap);
       DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
       Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
       String status = (String) outputs.get("OUT_STATUS");
       if(status.equals("N"))
       {
           String message = (String) outputs.get("OUT_MESSAGE");
           throw new Exception(message);
       }
    }

    public LinkedList getManualReconcileList(Date deliveryDate) throws Exception 
    {
       LinkedList output = new LinkedList();
       DataRequest dataRequest = new DataRequest();
       dataRequest.setConnection(conn);
       
       dataRequest.setStatementID("GET_MERCURY_RECONCILES");
       Map paramMap = new HashMap();   
       
       paramMap.put("IN_DELIVERY_DATE", new Timestamp(deliveryDate.getTime()));

       dataRequest.setInputParams(paramMap);
       DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
       CachedResultSet outputs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
       boolean hasOutput = false;
       while (outputs.next()){
        output.add((String)outputs.getObject(1));
        hasOutput = true;
       }
       if (hasOutput) {
        return output;
       } else 
       {
         return null;
       }
    } 
}
