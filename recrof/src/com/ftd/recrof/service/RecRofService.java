package com.ftd.recrof.service;

import com.enterprisedt.net.ftp.FTPClient;
import com.enterprisedt.net.ftp.FTPConnectMode;
import com.enterprisedt.net.ftp.FTPException;
import com.enterprisedt.net.ftp.FTPTransferType;
import com.ftd.recrof.CommonUtils;
import com.ftd.recrof.dao.RecRofDAO;
import com.ftd.recrof.vo.MercuryVO;
import com.ftd.recrof.vo.CustomerVO;
import com.ftd.recrof.vo.OrderDetailVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.j2ee.NotificationUtil;
import com.ftd.osp.utilities.vo.NotificationVO;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.math.BigDecimal;
import java.sql.Connection;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;

public class RecRofService
{
  private Connection conn;
  private RecRofDAO recrofDao;
  
  // trailer record constants
  private static final int TRAILER_BUFFER_LENGTH = 68;
  private static final int TRAILER_BILLING = 0;
  private static final int TRAILER_START_DATE = 9;
  private static final int TRAILER_END_DATE = 19;
  private static final int TRAILER_TOTAL_LABEL = 29;
  private static final int TRAILER_COUNT = 59;
  
  // detail record constants
  private static final int DETAIL_BUFFER_LENGTH = 150;
  private static final int DETAIL_SENDING_MEMBER_NUM = 2; 
  private static final int DETAIL_DELIVERY_DATE = 8;
  private static final int DETAIL_RECIPIENT = 12;
  private static final int DETAIL_DOLLAR_AMT = 21;
  private static final int DETAIL_FILLING_MEMBER_NUM = 35;
  private static final int DETAIL_OPERATOR = 41;
  private static final int DETAIL_OCCASION = 80;
  private static final int DETAIL_ORDER_NUMBER = 81;  

  //private static final String CONFIG_FILE = "order-processing-config.xml";
  private static final String RECROF_CONFIG_FILE = "recrof-config.xml";
  private static final String RECROF_CONFIG_CONTEXT = "RECROF_CONFIG";

  // Secure Configuration Context
  private static final String SECURE_CONFIG_CONTEXT = "order_processing";
  
  private Logger logger;
  private static final String LOG_CONTEXT = "com.ftd.recrof.service.RecRofService";

    public RecRofService(Connection connParm)
  {
    conn = connParm;
    recrofDao = new RecRofDAO(connParm);
    logger = new Logger(LOG_CONTEXT);
  }

  private void sendEmail(String content, Date runDate) throws Exception
  {

        ConfigurationUtil config = ConfigurationUtil.getInstance();            

    	// get email config data

	String emailRecipients = 
		config.getFrpGlobalParm(RECROF_CONFIG_CONTEXT, 
				            "RECROF_EMAIL_RECIPIENTS");
    	String emailServer = config.getFrpGlobalParm(RECROF_CONFIG_CONTEXT,
		                                 	 "RECROF_EMAIL_SERVER");
    
        NotificationVO nvo = new NotificationVO();
        nvo.setMessageFromAddress("noreply@ftdi.com");
        nvo.setMessageContent(content);
        nvo.setMessageSubject("RECROF output for run date: " + runDate.toString());
        nvo.setMessageTOAddress(emailRecipients);
        nvo.setSMTPHost(emailServer);

        NotificationUtil mail = NotificationUtil.getInstance();
        mail.notify(nvo);
  }

  private String generateFile(Date inDate) throws Exception
  { 

    ConfigurationUtil config = ConfigurationUtil.getInstance();            

    SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
    String localDir = config.getProperty(RECROF_CONFIG_FILE,"RECROF_LOCAL_DIRECTORY");                              
    String localFileName = config.getProperty(RECROF_CONFIG_FILE,"RECROF_LOCAL_FILENAME");                            


    File outFile = new File(localDir + File.separator + localFileName);
    outFile.createNewFile();
    OutputStreamWriter output = new OutputStreamWriter(new FileOutputStream(outFile));
    int count = 0;
    LinkedList mercuryList = recrofDao.getManualReconcileList(inDate);    
    if(mercuryList != null) {
      Iterator iter = mercuryList.iterator();
      while(iter.hasNext()) 
      {
        String mercId = (String)iter.next();
        output.write(this.buildDetailLine(mercId));
        output.write(System.getProperty("line.separator"));
        count++;

      }
    }
    
    Calendar cal = Calendar.getInstance();    

    String trailerLine = this.buildTrailerLine(cal.getTime(), inDate, count);

    output.write(trailerLine);
    output.write(System.getProperty("line.separator"));
    output.flush();
    
    // only mark the records as reconciled once the file is created successfully
    if(mercuryList != null) {
      Iterator iter2 = mercuryList.iterator();
      while(iter2.hasNext()) 
      {
        String mercId = (String)iter2.next();
        recrofDao.markReconciled(mercId);
      }
    }
    return trailerLine;
  }
  public void invoke() throws Throwable {
        logger.info("Beginning RECROF file generation.");
        String trailer = generateFile(new Date());
	logger.info("RECROF file generated.  Attempting to FTP");
        sendRecRofFile();
	logger.info("RECROF completed successfully");
	sendEmail(trailer, new Date());
	logger.info("RECROF email sent successfully");

  }

  private String calcOccasion(OrderDetailVO orderDetailVO, CustomerVO customerVO) 
  {
      String occasionString = "";
      if ((orderDetailVO.getOccasion()== null) || orderDetailVO.getOccasion().equals("8") || (new Integer(orderDetailVO.getOccasion()).intValue() <= 0))
      {
        if ((customerVO.getAddressType() != null) && (customerVO.getAddressType().trim().equalsIgnoreCase("FUNERAL HOME")))
        {
          occasionString = new String("1"); // FUNERAL/SYMPATHY
        }
        else 
        {
          occasionString = new String("8"); // OTHER
        }
      }
      else
      {
        int occasionInt = new Integer(orderDetailVO.getOccasion()).intValue();
        if (occasionInt > 13)
        {
          occasionString = new String("5"); // HOLIDAY
        }
        else if ((occasionInt <= 13)&&(occasionInt >= 8))
        {
          occasionString = new String("8"); // OTHER
        }
        else if (occasionInt < 8)
        {
          occasionString = new Integer(occasionInt).toString(); // HOLIDAY
        }
      } 
      return occasionString;
  }
  private String buildTrailerLine(Date startDate, Date endDate, int numRecords) 
  {
    StringBuffer buffer = new StringBuffer();
    buffer.setLength(TRAILER_BUFFER_LENGTH);
    for(int i = 0; i < TRAILER_BUFFER_LENGTH; i++) 
    {
      buffer.setCharAt(i, ' ');
    }
    SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
    DecimalFormat decimalFormat = new DecimalFormat();
    decimalFormat.setGroupingUsed(false);
    decimalFormat.setMinimumIntegerDigits(10);
    
    buffer.insert(TRAILER_BILLING, "Billing");
    buffer.insert(TRAILER_START_DATE, dateFormat.format(startDate));
    buffer.insert(TRAILER_END_DATE, dateFormat.format(endDate));
    buffer.insert(TRAILER_TOTAL_LABEL, "Total Records");
    buffer.insert(TRAILER_COUNT, decimalFormat.format(numRecords));
    return buffer.toString();
  }
  private String buildDetailLine(String mercuryId) throws Exception
  {
    MercuryVO mercury = recrofDao.getMessage(mercuryId);
    
    OrderDetailVO detail = recrofDao.getOrderDetail(mercury.getReferenceNumber());
    CustomerVO recipient = recrofDao.getCustomer(detail.getRecipientId());
    
    
    StringBuffer buffer = new StringBuffer();
    
    // init string to all blanks
    buffer.setLength(DETAIL_BUFFER_LENGTH);
    for(int i = 0; i < DETAIL_BUFFER_LENGTH; i++) {
      buffer.setCharAt(i, ' ');
    }
    SimpleDateFormat dateFormat = new SimpleDateFormat("MMdd");
    
    //System.out.println("Sending florist: " + mercury.getSendingFlorist());
    buffer.insert(DETAIL_SENDING_MEMBER_NUM, mercury.getSendingFlorist().substring(0, 2) + mercury.getSendingFlorist().substring(3,7));
    buffer.insert(DETAIL_DELIVERY_DATE, dateFormat.format(mercury.getDeliveryDate()));
    buffer.insert(DETAIL_RECIPIENT, new String((recipient.getLastName() + "   ")).substring(0,3).toUpperCase());
    buffer.insert(DETAIL_DOLLAR_AMT, transformPrice(mercury.getPrice().toString()));
    //System.out.println("Filling florist: " + mercury.getFillingFlorist());
    buffer.insert(DETAIL_FILLING_MEMBER_NUM, mercury.getFillingFlorist().substring(0,2) + mercury.getFillingFlorist().substring(3,7));
    buffer.insert(DETAIL_OPERATOR, "##");
    buffer.insert(DETAIL_OCCASION, calcOccasion(detail, recipient));
    buffer.insert(DETAIL_ORDER_NUMBER, mercury.getMercuryMessageNumber());
    return buffer.toString();
    
  }
  
  public void sendRecRofFile() throws Exception
  {
    //get ftp config data
    ConfigurationUtil configUtil = ConfigurationUtil.getInstance();         
    String remoteLocation = configUtil.getFrpGlobalParm(RECROF_CONFIG_CONTEXT,"RECROF_REMOTE_LOCATION");                      
    String remoteFileName = configUtil.getProperty(RECROF_CONFIG_FILE,"RECROF_REMOTE_FILENAME");                      
    String remoteDir = configUtil.getProperty(RECROF_CONFIG_FILE,"RECROF_REMOTE_DIRECTORY");                      
    String localDir = configUtil.getProperty(RECROF_CONFIG_FILE,"RECROF_LOCAL_DIRECTORY");                              
    String localFileName = configUtil.getProperty(RECROF_CONFIG_FILE,"RECROF_LOCAL_FILENAME");                            


    // Obtain Secure Configuration properties
    String ftpLogon = configUtil.getSecureProperty(SECURE_CONFIG_CONTEXT,"RECROF_FTP_LOGON");                      
    String ftpPassword = configUtil.getSecureProperty(SECURE_CONFIG_CONTEXT,"RECROF_FTP_PASSWORD");                              
                    
    //get list of files in directory       
    FTPClient ftpClient = new FTPClient(remoteLocation);
    ftpClient.login(ftpLogon,ftpPassword);
        
    //get the file from the remote location
    
     String localFile = localDir + File.separator + localFileName;
     String remoteFile = remoteFileName;

     System.out.println(remoteFile);
     System.out.println(localFile);
     ftpClient.setConnectMode(FTPConnectMode.PASV);
     ftpClient.setType(FTPTransferType.ASCII);
     ftpClient.chdir(remoteDir);
     ftpClient.put(localFile, remoteFile); 
  }
  private String transformPrice(String inPrice) 
  {
    BigDecimal decPrice = new BigDecimal(inPrice);
    
    String out = decPrice.movePointRight(2).toString();
    while(out.length() < 6)
    {
      out = "0" + out;
    }
    return out;
  }
}
