package com.ftd.osp.utilities.cacheMgr.handlers;

import java.util.ArrayList;
import java.util.List;

import com.ftd.osp.test.AbstractDBTest;
import com.ftd.osp.test.TestUtilities;
import com.ftd.osp.utilities.cacheMgr.vo.CacheHandlerBase;


public class CacheManagerHandlerTest extends AbstractDBTest
{

//    protected String getPassword()
//    {
//      return "...";
//    }
//
//    public String getConnectionURL()
//    {
//      return "jdbc:mysql://BIAPP1.ftdi.com:3306/osp";
//    }
//
//    protected String getDriver()
//    {
//      return "com.mysql.jdbc.Driver";
//    }    
    
    
    /**
     * Scans the Cache Manager Class Path for Handlers and attempts to test the
     * load and setCachedObject methods in them.
     * 
     * The purpose of this test is to verify that the Cache can work in Apollo.
     * 
     * Note: This assumes all statements are in utilities.xml, and that AbstractDBTest
     * is pointing to the right database.
     * 
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
    public void testAllHandlers() throws Exception
    {
        List<Class> packageClassList = TestUtilities.getClasses("com.ftd.osp.utilities.cacheMgr.handlers");
        List<String> errors = new ArrayList<String>();
        
        int iTotalTested = 0;
        for(Class clazz: packageClassList)
        {
            // If ths is a Cache Handler
            if(CacheHandlerBase.class.isAssignableFrom(clazz))
            {
                iTotalTested ++;
                System.out.println("Testing Handler: " + clazz.getName());                
                long startTime = System.currentTimeMillis();
                try
                {
                    performTestHandler(clazz);
                    System.out.println("Success Handler: " + clazz.getName() + " Time: " + (System.currentTimeMillis() - startTime) );
                } catch (Exception e)
                {
                    System.out.println("Failed Handler: " + clazz.getName() + " Time: " + (System.currentTimeMillis() - startTime) );
                    System.out.println("Failed Handler: " + clazz.getName() + "->" + exceptionToString(e));
                    
                    errors.add("Failed Handler: " + clazz.getName() + "->" + exceptionToString(e));
                }
            }
        }
        
        System.out.println("\n\n");
        System.out.println("Completed Testing Handler: " + iTotalTested);
        
        if(errors.size() > 0)
        {
            System.out.println("Failed Handlers: " + errors.size());
            for(String err: errors)
            {
                System.out.println("\t" + err);
            }
        }
    }
    
    String exceptionToString(Throwable t)
    {
        StringBuffer sb = new StringBuffer(t.getMessage() + ":" + t.getClass().getName());
        
        StackTraceElement[] st = t.getStackTrace();
        for(StackTraceElement ste: st)
        {
            sb.append(";").append(ste.toString());
        }
        
        
        return sb.toString();
    }
    
    
    /**
     * Load the handler and test it
     * @throws Exception
     */
    public void performTestHandler(Class<CacheHandlerBase> cacheHandlerClazz) throws Exception
    {
        CacheHandlerBase loader = cacheHandlerClazz.newInstance();
        Object cache = loader.load(getConnection());
    
        CacheHandlerBase handler = cacheHandlerClazz.newInstance();
        handler.setCachedObject(cache);
    }
}
