package com.ftd.osp.utilities.order.dao;

import com.ftd.osp.utilities.vo.ProductVO;

import java.sql.Connection;

import java.util.Set;


/**
 * This class tests the RecalculateOrderDAO
 */
public class RecalculateOrderDAOTest
  extends AbstractDAOTest
{
  private RecalculateOrderDAO recalculateOrderDAO;
  
  /**
   * Execute with a Text Test Runner
   * @param args
   */
  public static void main(String[] args)
  {
    AbstractDAOTest.runTest(RecalculateOrderDAOTest.class);
  }

  /**
   * Create a constructor that take a String parameter and passes it
   * to the super class
   */
  public RecalculateOrderDAOTest(String name)
  {
    super(name);
  }

  /**
   * Override setup() to initialize variables
   */
  protected void setUp()
  {
    recalculateOrderDAO = new RecalculateOrderDAO();
  }


  /**
   * Tests the DAO method for retrieving Active Accounts for an Account.
   * 
   * Ensure the appropriate accounts, records exist in the database
   */
  public void testGetActiveProgramsForAccount()
  {
    // test invalid/unknown accounts
    assertEquals(0, recalculateOrderDAO.getActiveProgramsForAccount(getConnection(), "nobody", getDate("2011-02-02 12:13:00")).size());
    assertEquals(0, recalculateOrderDAO.getActiveProgramsForAccount(getConnection(), "arajoo@ftdi.com", getDate("2011-02-02 12:13:00")).size());
    assertEquals(0, recalculateOrderDAO.getActiveProgramsForAccount(getConnection(), "nobody", getDate("2011-02-02 12:13:00")).size());
    
    assertEquals(0, recalculateOrderDAO.getActiveProgramsForAccount(getConnection(), "nobody", getDate("2012-02-11 00:00:00")).size());
    
    // Check valid Account with Free Shipping
    Set<String> values =  recalculateOrderDAO.getActiveProgramsForAccount(getConnection(), "ARajoo@ftdi.com", getDate("2011-02-02 12:13:00"));
    assertEquals(1, values.size());
    assertTrue(values.contains("FREESHIP"));
    
    // Check Expirations
    assertEquals(0, recalculateOrderDAO.getActiveProgramsForAccount(getConnection(), "arajoo01@ftdi.com", getDate("2011-03-15 00:00:00")).size());
    assertEquals(0, recalculateOrderDAO.getActiveProgramsForAccount(getConnection(), "arajoo01@ftdi.com", getDate("2011-03-15 23:59:59")).size());    
    assertEquals(0, recalculateOrderDAO.getActiveProgramsForAccount(getConnection(), "arajoo01@ftdi.com", getDate("2012-03-17 00:00:00")).size());
    
    assertEquals(1, recalculateOrderDAO.getActiveProgramsForAccount(getConnection(), "arajoo01@ftdi.com", getDate("2011-03-16 00:00:00")).size());
    assertEquals(1, recalculateOrderDAO.getActiveProgramsForAccount(getConnection(), "arajoo01@ftdi.com", getDate("2012-03-16 00:00:00")).size());    
    assertEquals(1, recalculateOrderDAO.getActiveProgramsForAccount(getConnection(), "arajoo01@ftdi.com", getDate("2012-03-16 23:59:59")).size());        
  }


  /**
   * Tests the DAO method for retrieving Active Accounts for an Account
   */
  public void testGetProductById()
  {
    // test invalid/unknown accounts
    ProductVO productVO = recalculateOrderDAO.getProduct(getConnection(), "T145");
    assertEquals("T145", productVO.getProductId());
    assertEquals("Y", productVO.getShipMethodCarrier());

    productVO = recalculateOrderDAO.getProduct(getConnection(), "7030");
    assertEquals("7030", productVO.getProductId());
    assertEquals("N", productVO.getShipMethodCarrier());
    
    productVO = recalculateOrderDAO.getProduct(getConnection(), "1202");
    assertEquals("1202", productVO.getProductId());
    assertEquals("N", productVO.getShipMethodCarrier());    
  }
}
