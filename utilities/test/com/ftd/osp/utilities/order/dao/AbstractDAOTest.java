package com.ftd.osp.utilities.order.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.Set;

import junit.framework.TestCase;
import junit.framework.TestSuite;

import junit.textui.TestRunner;

/**
 * This class tests provides abstract methods for DAO testing, including
 * initializing the Database etc.
 */
public class AbstractDAOTest
  extends TestCase
{

  private Connection connection;


  public static void runTest(Class clazz)
  {
    TestRunner runner = new TestRunner();
    runner.run(new TestSuite(clazz));
  }
  
  /**
   * Execute with a Text Test Runner
   * @param args
   */
  public static void main(String[] args)
  {
    TestRunner runner = new TestRunner();
    runner.run(new TestSuite(AbstractDAOTest.class));
  }

  /**
   * Create a constructor that take a String parameter and passes it
   * to the super class
   */
  public AbstractDAOTest(String name)
  {
    super(name);
  }

  /**
   * @return Retrieve a connection for DB Testing
   */
  protected Connection getConnection()
  {
    if (connection == null)
    {
      try
      {
        String driver_ = "oracle.jdbc.driver.OracleDriver";
        String database_ = "jdbc:oracle:thin:@adonis.ftdi.com:1521:dev49j";
        String user_ = "osp";
        String password_ = "osp";

        Class.forName(driver_);
        connection = DriverManager.getConnection(database_, user_, password_);
      }
      catch (Exception e)
      {
        throw new RuntimeException(e);
      }
    }

    return connection;
  }

  /**
   * Override tearDown() to release any permanent resources you allocated in setUp
   */
  public void tearDown()
  {
    if (connection != null)
    {
      try
      {
        connection.close();
      }
      catch (SQLException e)
      {
        // NOP
      }
    }
  }


  /**
   * Used for generating Date Strings.
   * Format: YYYY-MM-DD HH:MM:SS
   * @param date
   * @return
   */
  protected Date getDate(String date)
  {
    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    try
    {
      return df.parse(date);
    }
    catch (ParseException e)
    {
      throw new RuntimeException(e);
    }
  }


}
