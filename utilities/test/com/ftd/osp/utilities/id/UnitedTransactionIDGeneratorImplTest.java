/**
 * 
 */
package com.ftd.osp.utilities.id;

import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.DriverManager;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ftd.osp.utilities.id.vo.IdRequestVO;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * @author cjohnson
 *
 */
public class UnitedTransactionIDGeneratorImplTest {
	
    protected static Connection connection;
    static String database_ = "jdbc:oracle:thin:@adonis.ftdi.com:1521:fozzie";
    
    Logger logger = new Logger(UnitedTransactionIdGeneratorImpl.class.getName());

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		connection = setupConnection();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		connection.close();
	}

	/**
	 * Test method for {@link com.ftd.osp.utilities.id.UnitedTransactionIdGeneratorImpl#execute(com.ftd.osp.utilities.id.vo.IdRequestVO, java.sql.Connection)}.
	 * @throws Exception 
	 */
	@Test
	public void testExecute() throws Exception {
		UnitedTransactionIdGeneratorImpl idGenerator = new UnitedTransactionIdGeneratorImpl();
		
		IdRequestVO req = new IdRequestVO("something", IdGeneratorFactory.UNITED_REQUEST_TYPE);
		
		String id = idGenerator.execute(req, connection);
		
		assertTrue(id != null);
		
		System.out.print("id is " + id);
		
	}
	
	
    protected static Connection setupConnection()
    {
        if (connection == null)
        {
            try
            {
                String driver_ = "oracle.jdbc.driver.OracleDriver";
                String user_ = "osp";
                String password_ = "osp";
    
                Class.forName(driver_);                                                           
                connection = DriverManager.getConnection(database_, user_, password_);
            }
            catch (Exception e)
            {
                throw new RuntimeException(e);
            }
        }
    
        return connection;
    }

}
