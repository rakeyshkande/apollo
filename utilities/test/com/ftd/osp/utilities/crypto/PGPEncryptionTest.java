package com.ftd.osp.utilities.crypto;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import junit.framework.TestCase;

import org.w3c.dom.Document;

import com.ftd.osp.utilities.xml.DOMUtil;

/**
 * This unit test executes tests of the PGPEncryption utility utilizing 
 * sample test files secret key files
 */
public class PGPEncryptionTest extends TestCase
{
    /**
     * The crypto utility
     */
    PGPEncryption pgpEncryption = new PGPEncryption();
    
    
    /**
     * Retrieves a test file as a resource.
     * the file must be in the same folder as this class.
     * @param testFile
     * @return
     */
    private InputStream getTestData(String testFile)
    {
        
        String resourcePath = "/" + getClass().getPackage().getName().replace(".", "/") + "/" + testFile;
        
        InputStream retval =  getClass().getResourceAsStream(resourcePath);
        
        return retval;
    }
    
    /**
     * Retrieves the Test Order XML file from the resource, and extracts the PGP
     * payload stored as Text within the <order> tag.
     * @param testFile
     * @return
     * @throws Exception
     */
    private InputStream getTestOrderData(String testFile) throws Exception
    {
        InputStream encryptedOrderXML = getTestData(testFile);
        
        Document orderXMLDocument = DOMUtil.getDocument(encryptedOrderXML);
        String encryptedData = DOMUtil.getNodeValue(orderXMLDocument.getDocumentElement());
        
        return new ByteArrayInputStream(encryptedData.getBytes("utf-8"));
    }        
    
    
    /**
     * Test decrypting a Base64 encoded order which was encrypted with GPG
     */
    public void test_GPG_DecryptBase64Encoded() throws Exception
    {
        InputStream encryptedData = getTestOrderData("gpg_test_order_encrypted_base64.txt");
        InputStream keyIn = getTestData("orderxmltest-secret.asc");
        
        byte[] decryptedData = pgpEncryption.decryptStream(encryptedData, keyIn, "orderxmlpcitest");
        String orderData = new String(decryptedData, "utf-8");
        
        System.out.println(orderData);
    }
    
    
    /**
     * Test decrypting an armored ZIP compressed order which was encrypted with GPG
     */    
    public void test_GPG_DecryptZipArmored() throws Exception
    {
        InputStream encryptedData = getTestOrderData("gpg_test_order_encrypted_zip_armored.txt");
        InputStream keyIn = getTestData("orderxmltest-secret.asc");
        
        byte[] decryptedData = pgpEncryption.decryptStream(encryptedData, keyIn, "orderxmlpcitest");
        String orderData = new String(decryptedData, "utf-8");
        
        System.out.println(orderData);
    }    
    
    /**
     * Test decrypting invalid data. We expect exceptions here. generally, this is why we get
     * NullPointerExceptions from the utility.
     */    
    public void testDecryptInvalidData() throws Exception
    {
        InputStream encryptedData = new ByteArrayInputStream("".getBytes("utf-8"));
        InputStream keyIn = getTestData("orderxmltest-secret.asc");
        
        try
        {
            pgpEncryption.decryptStream(encryptedData, keyIn, "orderxmlpcitest");
        } catch (Exception ex)
        {
            return;
        }
        
        fail("Expected Exception, usually an NPE");
    }    
    
    /**
     * Test decrypting an armored uncompressed order which was encrypted with GPG
     */    
    public void test_GPG_DecryptUncompresseArmored() throws Exception
    {
        InputStream encryptedData = getTestOrderData("gpg_test_order_encrypted_uncompressed_armored.txt");
        InputStream keyIn = getTestData("orderxmltest-secret.asc");
        
        byte[] decryptedData = pgpEncryption.decryptStream(encryptedData, keyIn, "orderxmlpcitest");
        String orderData = new String(decryptedData, "utf-8");
        
        System.out.println(orderData);
    }    
    
    /**
     * Test decrypting an armored uncompressed order which was encrypted by the Website
     */    
    public void test_Website_DecryptUncompresseArmored() throws Exception
    {
        InputStream encryptedData = getTestOrderData("website_test_order_encrypted_uncompressed_armored.txt");
        InputStream keyIn = getTestData("orderxmltest-secret.asc");
        
        byte[] decryptedData = pgpEncryption.decryptStream(encryptedData, keyIn, "orderxmlpcitest");
        String orderData = new String(decryptedData, "utf-8");
        
        System.out.println(orderData);
    }       
    
    /**
     * Extracts the order xml data from a testFile in the file system.
     * This is a helper method for testExtractedFiles()
     * @param testFile
     * @return
     * @throws Exception
     */
//    private String getTestOrderDataFromFile(File testFile) throws Exception
//    {
//        InputStream encryptedOrderXML = new FileInputStream(testFile);
//        
//        Document orderXMLDocument = DOMUtil.getDocument(encryptedOrderXML);
//        String encryptedData = DOMUtil.getNodeValue(orderXMLDocument.getDocumentElement());
//        
//        return encryptedData;
//    } 
    
    /**
     * Test all files in a specific folder with the name order_*.txt file pattern.
     * Generally this test is commented out. This is a helper test when debugging
     * data retrieved from the server.
     * 
     * @throws Exception
     */
//    public void testExtractedFiles() throws Exception
//    {
//        File targetFolder = new File("C:/Temp/gathererrequests");
//        
//        File[] testFiles = targetFolder.listFiles();
//        
//        for(File testFile: testFiles)
//        {
//            if(testFile.isDirectory())
//            {
//                continue;
//            }
//            
//            if(testFile.getName().startsWith("order_") && testFile.getName().endsWith(".txt"))
//            {
//                String encryptedDataString = ""; 
//                try
//                {                
//                    encryptedDataString = getTestOrderDataFromFile(testFile);
//                    InputStream keyIn = getTestData("orderxmltest-secret.asc");
//                    
//                    InputStream encryptedData = new ByteArrayInputStream(encryptedDataString.getBytes("utf-8"));
//
//                    byte[] decryptedData = pgpEncryption.decryptStream(encryptedData, keyIn, "orderxmlpcitest");
//                    String orderData = new String(decryptedData, "utf-8");
//                    System.out.println("OK: " + testFile.getName());
//                    System.out.println(orderData);
//                } catch (Exception ex)
//                {
//                    System.out.println("------------------------------------------------------------------");                    
//                    System.out.println("FAILED: " + testFile.getName() + "->" + ex.toString());
//                    ex.printStackTrace(System.out);
//                    System.out.println();                    
//                    // System.out.println(encryptedDataString);
//                    
//                    // analyzeString(encryptedDataString);
//
//                    System.out.println("------------------------------------------------------------------");
//                }
//                
//            }
//        }
//    }

}
