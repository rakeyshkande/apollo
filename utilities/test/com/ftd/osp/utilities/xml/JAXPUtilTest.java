package com.ftd.osp.utilities.xml;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.ParserConfigurationException;

import junit.framework.TestCase;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

public class JAXPUtilTest extends TestCase 
{
    
    public void testCreateDocument() throws Exception
    {
        // tests the various create Document Permutations
        assertNotNull(JAXPUtil.createDocument(false, false));
        assertNotNull(JAXPUtil.createDocument(false, true));
        assertNotNull(JAXPUtil.createDocument(true, false));
        assertNotNull(JAXPUtil.createDocument(true, true));
    }

    public void testSelectSingleNode() throws Exception
    {
        String xml="<node><fred>Freddy</fred></node>";
        Document doc = JAXPUtil.parseDocument(xml);        
        assertNotNull(JAXPUtil.selectSingleNode(doc, "//fred"));

        assertEquals("Freddy", JAXPUtil.selectSingleNodeText(doc, "//fred", null, null));
    }
    
    public void testSelectNodes() throws Exception
    {
        String xml="<node><fred>Freddy</fred></node>";
        Document doc = JAXPUtil.parseDocument(xml);        
        assertNotNull(JAXPUtil.selectNodes(doc, "//fred"));

        assertEquals(1, JAXPUtil.selectNodes(doc, "//fred", null, null).getLength());        
    }
    
    
    
    public void testPrintElement() throws Exception
    {
 
        assertTrue(printElementImpl("root1", true).startsWith("<root1>"));
        assertTrue(printElementImpl("root2", true).startsWith("<root2>"));

        assertTrue(printElementImpl("root1", false).startsWith("<One>"));
        assertTrue(printElementImpl("root2", false).startsWith("<One>"));                
    }
    
    private String printElementImpl(String rootNode, boolean addToDoc) throws Exception
    {
        Document testDocument = JAXPUtil.createDocument();
        Element dRoot = testDocument.createElement(rootNode);
        
        if(addToDoc)
        {
            testDocument.appendChild(dRoot);
        }
        
        Element testElement = testDocument.createElement("One");
        testElement.appendChild(JAXPUtil.buildSimpleXmlNode(testDocument,"FTDOrderNumber","1"));
        testElement.appendChild(JAXPUtil.buildSimpleXmlNode(testDocument,"FTDProductID","2"));
        dRoot.appendChild(testElement);        
        
        testElement = testDocument.createElement("Two");
        testElement.appendChild(JAXPUtil.buildSimpleXmlNode(testDocument,"FTDOrderNumber","1"));
        testElement.appendChild(JAXPUtil.buildSimpleXmlNode(testDocument,"FTDProductID","2"));
        dRoot.appendChild(testElement);            
        
        String dynamicDataString = JAXPUtil.toString(dRoot);
        
        System.out.println("Root=:" + rootNode + "Add to Doc=" + addToDoc); 
        System.out.println(dynamicDataString);
        
        return dynamicDataString ;
        
    }

    
    public void testParseEncodedInputStreamTM() throws IOException, JAXPException {
    	String fileContents = encodedTMString;
		verifyParseEncodedInputStream(fileContents);
    }

    public void testParseEncodedInputStreamR() throws IOException, JAXPException {
    	String fileContents = encodedRString;
		verifyParseEncodedInputStream(fileContents);
    }

    public void testParseEncodedInputStreamUmlaut() throws IOException, JAXPException {
    	String fileContents = encodedUmlautString;
		verifyParseEncodedInputStream(fileContents);
    }

    public void testParseSampleFile() throws JAXPException, IOException  {
    	InputStream is = getClass().getClassLoader().getResourceAsStream("sample_encoded_file.xml");
    	assertNotNull(is);
    	Document doc = JAXPUtil.parseDocument(is);
    	assertNotNull(doc);
    }

    private void verifyParseEncodedInputStream(String fileContents)
			throws IOException, FileNotFoundException, JAXPException {
		File tempFile = createTempFile(fileContents);
    	InputStream is = new FileInputStream(tempFile);
    	// parse the stream, verify it returns a document
    	Document doc = JAXPUtil.parseDocument(is);
    	assertNotNull(doc);
	}
    
    
	private File createTempFile(String fileContents) throws IOException {
		String filename = "encodetest" + System.currentTimeMillis();
    	File tempFile = File.createTempFile(filename, ".xml");
    	tempFile.deleteOnExit();

    	FileWriter fw = new FileWriter(tempFile);
    	fw.write(fileContents);
    	fw.close();
		return tempFile;
	}

    private final String encodedTMString = "<index><index-description><![CDATA[Say It Your Way� ]]></index-description></index>\n";
    private final String encodedRString = "<index><index-description><![CDATA[FTD� Good Neighbor Day� 2006]]></index-description></index>\n";
    private final String encodedUmlautString = "<index><index-description><![CDATA[Guy Umlaut ���]]></index-description></index>\n";

}
