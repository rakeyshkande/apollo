package com.ftd.osp.utilities.xml;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

import com.ftd.osp.utilities.j2ee.ResourceUtil;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.JAXPUtil;

import junit.framework.TestCase;
import org.apache.commons.io.IOUtils;

public class DOMUtilTest extends TestCase {

	public void testRestoreEmptyTextNode0Nodes() {
		String xml="<node></node>", 
			expectedXML="<node></node>";
		verifyRestore(xml, expectedXML);
	}

	public void testRestoreEmptyTextNode0NodesWithText() {
		String xml="<node>Fred</node>", 
			expectedXML="<node>Fred</node>";
		verifyRestore(xml, expectedXML);
	}

	public void testRestoreEmptyTextNode1Node() {
		String xml="<node/>", 
			expectedXML="<node></node>";
		verifyRestore(xml, expectedXML);
	}

	public void testRestoreEmptyTextNode2Nodes() {
		String xml="<node/><child/>",
			expectedXML="<node></node><child></child>";
		verifyRestore(xml, expectedXML);
	}
	
	public void testRestoreEmptyTextNode1NodeWithData() {
		String xml="<node/><fred>Freddy</fred>", 
			expectedXML="<node></node><fred>Freddy</fred>";
		verifyRestore(xml, expectedXML);
	}
	
	public void testRestoreEmptyTextNode1NodeWithAssortedData() {
		String xml="<node/><fred>\"Fredrick &quot;James&quot; 01-/02$03 Smith\"</fred>", 
			expectedXML="<node></node><fred>\"Fredrick &quot;James&quot; 01-/02$03 Smith\"</fred>";
		verifyRestore(xml, expectedXML);
	}
	public void testRestoreEmptyTextNode1NodeWithNewLines() {
		String xml="<node/>\n<fred>Freddy</fred>", 
			expectedXML="<node></node>\n<fred>Freddy</fred>";
		verifyRestore(xml, expectedXML);
	}

	public void testRestoreEmptyTextNodeNodeWithAttribute() {
		String xml="<node name=\"Fredrick James Smith\"/>", 
			expectedXML="<node name=\"Fredrick James Smith\"></node>";
		verifyRestore(xml, expectedXML);
	}

	public void testRestoreEmptyTextNodeNodeWithAttributes() {
		String xml="<node name=\"Fredrick James Smith\" home=\"Hometown, Usa\"/>", 
			expectedXML="<node name=\"Fredrick James Smith\" home=\"Hometown, Usa\"></node>";
		verifyRestore(xml, expectedXML);
	}
	public void testRestoreEmptyTextNodeNodeWithAttributeAndTabsAndSpaces() {
		String xml="<node \t\t name\t=  \"Fredrick James Smith\"/>", 
			expectedXML="<node \t\t name\t=  \"Fredrick James Smith\"></node>";
		verifyRestore(xml, expectedXML);
	}

	public void testRestoreEmptyTextNodeNodeWithAttributeWithQuotes() {
		String xml="<node name=\'Fredrick \"James\" Smith\'/>", 
			expectedXML="<node name=\'Fredrick \"James\" Smith\'></node>";
		verifyRestore(xml, expectedXML);
	}
	
	public void testRestoreEmptyTextNodeNodeWithAttributeWithCharacterentities() {
		String xml="<node name=\"Fredrick &quot;James&quot; Smith\"/>", 
			expectedXML="<node name=\"Fredrick &quot;James&quot; Smith\"></node>";
		verifyRestore(xml, expectedXML);
	}
	
	
	public void testRestoreEmptyTextNodeNodeWithAttributeAndOtherChars() {
		String xml="<node name=\"Fredrick 01-02-03 James ## Smith\"/>", 
			expectedXML="<node name=\"Fredrick 01-02-03 James ## Smith\"></node>";
		verifyRestore(xml, expectedXML);
	}
	
	
	/**
	 * This test is to ensure that empty tags within a CDATA section are not expanded.
	 * 
	 * Currently, the utilization of Regular Expressions in the implementation will cause
	 * expansion, and this test will fail. 
	 * 
	 * Currently, this test fails. Until an alternate implementation is in place. As this is
	 * not a likely scenario, this remains unimplemented.
	 */
	public void testRestoreEmptyTextNodeCData() {
		String xml="<node/><script><![CDATA[\nDont find this <fred/>\n]]>\n</script>", 
			expectedXML="<node></node>\n<script><![CDATA[\nDont find this <fred/>\n]]>\n</script>";
		
		    verifyRestore(xml, expectedXML);
	}

	private void verifyRestore(String xml, String expectedXML) {
		String newXML=DOMUtil.expandEmptyTextNode(xml);
		assertEquals(expectedXML, newXML);
	}

	
//	public void testSchemaValidation() throws Exception {
//		String textMessage = "<?xml version='1.0' encoding='utf-8'?><event xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xsi:noNamespaceSchemaLocation='event.xsd'><event-name>RUN-EOD</event-name><context>ACCOUNTING</context><payload></payload></event>";
//		StringReader sr = new StringReader(textMessage);
//		InputSource inputSource = new InputSource(sr);
//		
//		Schema schema;		
//		schema = DOMUtil.getSchema(new StreamSource(getClass().getClassLoader().getResourceAsStream("event.xsd"))); 
//
//        XMLErrorHandler errorHandler = new XMLErrorHandler();
//        
//		//should use this for parsing: com.sun.org.apache.xerces.internal.jaxp.DocumentBuilderFactoryImpl
//        DOMUtil.getDocument(inputSource, schema, errorHandler);
//                
//        assertTrue(errorHandler.isValid());
//	}
	
	
	/**
	 * Creates a temp file with the passed in name, and InputStream contents
	 * @param inputStream
	 * @param name
	 * @return
	 */
	public static File createTempFile(InputStream inputStream, String name)
	{
		try {
			File tempFile = File.createTempFile(name, "");
			tempFile.deleteOnExit();
			
			BufferedInputStream bis = new BufferedInputStream(inputStream, 20000);
			
			// FileOutputStream fos = new FileOutputStream(tempFile);
			BufferedOutputStream fos = new BufferedOutputStream(new FileOutputStream(tempFile), 20000);
			
			int next = bis.read();
			while(next >= 0)
			{
				fos.write(next);
				next = bis.read();
			}
			
			fos.close();
			return tempFile;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	
	

	class XMLErrorHandler extends DefaultHandler {
	    private boolean valid = true;
	    private SAXParseException parseException;

	    //Receive notification of a recoverable error.
	    public void error(SAXParseException se) {
	        setError(false, se);
	    }

	    //Receive notification of a non-recoverable error.
	    public void fatalError(SAXParseException se) {
	        setError(false, se);
	    }

	    //Receive notification of a warning.
	    public void warning(SAXParseException se) {
	        setError(false, se);
	    }


	    public void setValid(boolean valid) {
	        this.valid = valid;
	    }

	    private void setError(boolean valid, SAXParseException spe) {
	        setValid(valid);
	        setParseException(spe);
	    }

	    public boolean isValid() {
	        return valid;
	    }


	    public void setParseException(SAXParseException parseException) {
	        this.parseException = parseException;
	    }


	    public SAXParseException getParseException() {
	        return parseException;
	    }
	}
	
	
    public void testPrintElement() throws Exception
    {
        assertTrue(printElementImpl("root1", true).startsWith("<root1>"));
        assertTrue(printElementImpl("root2", true).startsWith("<root2>"));

        assertTrue(printElementImpl("root1", false).startsWith("<One>"));
        assertTrue(printElementImpl("root2", false).startsWith("<One>"));
    }
	    
    private String printElementImpl(String rootNode, boolean addToDoc) throws Exception
    {
        Document testDoc = DOMUtil.getDefaultDocument();
        Element dRoot = testDoc.createElement(rootNode);
        
        if(addToDoc)
        {
            testDoc.appendChild(dRoot);
        }
                
        Element testElement = testDoc.createElement("One");
        testElement.appendChild(JAXPUtil.buildSimpleXmlNode(testDoc,"FTDOrderNumber","1"));
        testElement.appendChild(JAXPUtil.buildSimpleXmlNode(testDoc,"FTDProductID","2"));
        dRoot.appendChild(testElement);        
        
        testElement = testDoc.createElement("Two");
        testElement.appendChild(JAXPUtil.buildSimpleXmlNode(testDoc,"FTDOrderNumber","1"));
        testElement.appendChild(JAXPUtil.buildSimpleXmlNode(testDoc,"FTDProductID","2"));
        dRoot.appendChild(testElement);            
        
        StringWriter writer = new StringWriter();
        DOMUtil.print(dRoot, writer);
        String dynamicDataString = writer.toString();
        
        System.out.println("Root=:" + rootNode + "Add to Doc=" + addToDoc); 
        System.out.println(dynamicDataString);
        
        return dynamicDataString ;
        
    }	
    
    
    public void testParseEncodedFileRegistered() throws Exception {
    	verifyParseEncodedFile(encodedRString);
    }

    public void testParseEncodedFileUmlaut() throws Exception {
    	verifyParseEncodedFile(encodedUmlautString);
    }

    public void testParseEncodedFileTM() throws Exception { 	
    	verifyParseEncodedFile(encodedTMString);
    }

    public void testParseEncodedFileDocumentBuilderRegistered() throws Exception {
    	verifyParseEncodedFileDocumentBuilder(encodedRString);
    }

    public void testParseEncodedFileDocumentBuilderUmlaut() throws Exception {
    	verifyParseEncodedFileDocumentBuilder(encodedUmlautString);
    }
    
	public void testParseEncodedFileDocumentBuilderTM() throws Exception{
		String fileContents = encodedTMString;
		
		verifyParseEncodedFileDocumentBuilder(fileContents);
	}

    public void testParseEncodedStringR() throws Exception {      	    	    	
    	Document doc = DOMUtil.getDocument(encodedRString);
    	assertNotNull(doc);
    }

    public void testParseEncodedStringUmlaut() throws Exception {      	    	    	
    	Document doc = DOMUtil.getDocument(encodedUmlautString);
    	assertNotNull(doc);
    }

    public void testParseEncodedStringTM() throws Exception {      	    	    	
    	Document doc = DOMUtil.getDocument(encodedTMString);
    	assertNotNull(doc);
    }

    public void testParseEncodedStringDocumentBuilderR() throws Exception {      	    	    	
    	DocumentBuilder docBuilder = DOMUtil.getDocumentBuilder();
    	Document doc = DOMUtil.getDocument(docBuilder, encodedRString);
    	assertNotNull(doc);
    }

    public void testParseEncodedStringDocumentBuilderUmlaut() throws Exception {      	    	    	
    	DocumentBuilder docBuilder = DOMUtil.getDocumentBuilder();
    	Document doc = DOMUtil.getDocument(docBuilder, encodedUmlautString);
    	assertNotNull(doc);
    }

    public void testParseEncodedStringDocumentBuilderTM() throws Exception {      	    	    	
    	DocumentBuilder docBuilder = DOMUtil.getDocumentBuilder();
    	Document doc = DOMUtil.getDocument(docBuilder, encodedTMString);
    	assertNotNull(doc);
    }
    
    public void testParseSampleFile() throws SAXException, IOException, ParserConfigurationException {
    	InputStream is = getClass().getClassLoader().getResourceAsStream("sample_encoded_file.xml");
    	assertNotNull(is);
    	Document doc = DOMUtil.getDocument(is);
    	assertNotNull(doc);
    }

    
    private void verifyParseEncodedFile(String fileContents) throws IOException, SAXException, ParserConfigurationException {
		File tempFile = createTempFile(fileContents);
    	    	
    	// parse the file, verify it returns a document
    	Document doc = DOMUtil.getDocument(tempFile);
    	assertNotNull(doc);
	}

	private void verifyParseEncodedFileDocumentBuilder(String fileContents)
			throws IOException, ParserConfigurationException, SAXException {
		// create file with special UTF-8 characters
    	File tempFile = createTempFile(fileContents);
    	    	
    	// parse the file, verify it returns a document
    	DocumentBuilder docBuilder = DOMUtil.getDocumentBuilder();
    	Document doc = DOMUtil.getDocument(docBuilder, tempFile);
    	assertNotNull(doc);
	}

	private File createTempFile(String fileContents) throws IOException {
		String filename = "encodetest" + System.currentTimeMillis();
    	File tempFile = File.createTempFile(filename, ".xml");
    	tempFile.deleteOnExit();

    	FileWriter fw = new FileWriter(tempFile);
    	fw.write(fileContents);
    	fw.close();
		return tempFile;
	}

    private final String encodedTMString = "<index><index-description><![CDATA[Say It Your Way� ]]></index-description></index>\n";
    private final String encodedRString = "<index><index-description><![CDATA[FTD� Good Neighbor Day� 2006]]></index-description></index>\n";
    private final String encodedUmlautString = "<index><index-description><![CDATA[Guy Umlaut ���]]></index-description></index>\n";

    
    // Verify that a valid file can be validated and parsed cleanly 
    public void testGetDocumentValid() throws Exception {
        StringBuffer errors = new StringBuffer();
    	Document doc;

    	String fileContents = convertFileToString(testFileNameValid);
        
        // verify that the file contents can be validated and parsed
        doc = DOMUtil.getDocument(fileContents, dtdFileNameValid, true, errors);
        assertNotNull(doc);
        assertEquals(0, errors.length());
    }

    // Verify that an invalid file will generate validation errors when validated and parsed
    public void testGetDocumentInvalid() throws Exception {
        StringBuffer errors = new StringBuffer();
    	Document doc;

    	String fileContents = convertFileToString(testFileNameInvalid);
        
        // verify that the file contents can be validated and parsed
        doc = DOMUtil.getDocument(fileContents, dtdFileNameValid, true, errors);
        assertNotNull(doc);
        assertNotSame(0, errors.length());
    }

    // Verify that an invalid file can be "validated" and parsed cleanly when validation is turned off
    public void testGetDocumentInvalidValidationOff() throws Exception {
        StringBuffer errors = new StringBuffer();
    	Document doc;

    	String fileContents = convertFileToString(testFileNameInvalid);
        
        // verify that the file contents can be parsed without validation
        doc = DOMUtil.getDocument(fileContents, dtdFileNameValid, false, errors);
        assertNotNull(doc);
        assertEquals(0, errors.length());
    }

    // Verify that an invalid file with no DTD can be "validated" and parsed cleanly when validation is turned off
    public void testGetDocumentNoDTDInvalidValidationOff() throws Exception {
        StringBuffer errors = new StringBuffer();
    	Document doc;

    	String fileContents = convertFileToString(testFileNameInvalidNoDTD);
        
        // verify that the file contents can be parsed without validation
        doc = DOMUtil.getDocument(fileContents, dtdFileNameValid, false, errors);
        assertNotNull(doc);
        assertEquals(0, errors.length());
    }

    // Verify that an invalid file with no DTD will return errors when validation is turned on
    public void testGetDocumentNoDTDInvalid() throws Exception {
        StringBuffer errors = new StringBuffer();
    	Document doc;

    	String fileContents = convertFileToString(testFileNameInvalidNoDTD);
        
        // verify that the file contents can be parsed without validation
        doc = DOMUtil.getDocument(fileContents, dtdFileNameValid, true, errors);
        assertNotNull(doc);
        assertNotSame(0, errors.length());
    }

    // Verify that a file can be validated and parsed with different internal and external DTDs, where 
    // the document is valid against internal DTD but invalid against the external.  
    public void testGetDocument2DTDs() throws Exception {
    	Document doc;

        StringBuffer errors = new StringBuffer();
    	
    	String fileContents = convertFileToString(testFileNameValid);
        
        // verify that the file contents can be validated and parsed - regression test to prove test is valid
        doc = DOMUtil.getDocument(fileContents, dtdFileNameValid, true, errors);
        assertNotNull(doc);
        assertEquals(0, errors.length());
        
        // Now, validate against bad DTD - should produce errors
        doc = DOMUtil.getDocument(fileContents, dtdFileNameInvalid, true, errors);
        assertNotNull(doc);
        assertNotSame(0, errors.length());
    }

    // Verify that, when validation is turned off, a file can be validated and parsed cleanly 
    // with different internal and external DTDs, where the document is valid against internal 
    // DTD but invalid against the external.  
    public void testGetDocument2DTDsValidationOff() throws Exception {
    	Document doc;

        StringBuffer errors = new StringBuffer();
    	
    	String fileContents = convertFileToString(testFileNameValid);
        
        // Now, validate against bad DTD - should produce errors - regression to validate test
        doc = DOMUtil.getDocument(fileContents, dtdFileNameInvalid, true, errors);
        assertNotNull(doc);
        assertNotSame(0, errors.length());

        errors = new StringBuffer();
        // now turn off validation - should be clean
        doc = DOMUtil.getDocument(fileContents, dtdFileNameInvalid, false, errors);
        assertNotNull(doc);
        assertEquals(0, errors.length());
    }

	private String convertFileToString(String testFileName)
			throws UnsupportedEncodingException, IOException {
		// get file and convert to string
    	InputStream is = ResourceUtil.getInstance().getResourceFileAsStream(testFileName);
    	assertNotNull(is);
    	
        StringBuilder sb = new StringBuilder();
        String line;
        try {
            BufferedReader r1 = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            while ((line = r1.readLine()) != null) {
                sb.append(line).append("\n");
            }
        } finally {
            is.close();
        }
		return sb.toString();
	}
	
	private static String testFileNameValid = "validTestDocument.xml";
	private static String testFileNameInvalid = "invalidTestDocument.xml";
	private static String testFileNameInvalidNoDTD = "invalidTestDocumentNoDTD.xml";
	private static String dtdFileNameValid = "850X-01-001-000.dtd";
	private static String dtdFileNameInvalid = "850X-01-001-000Invalid.dtd";

}
