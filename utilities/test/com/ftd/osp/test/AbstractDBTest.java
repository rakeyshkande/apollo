package com.ftd.osp.test;



import java.sql.Connection;
import java.sql.DriverManager;

import junit.framework.TestCase;

/**
 * Sets up Connections for JDBC Testing
 */
public class AbstractDBTest extends TestCase
{
  private TransactionalConnection _connection;
  private String _userName = "osp";
  private String _password = "osp";
  private String _connectionURL = "jdbc:oracle:thin:@adonis.ftdi.com:1521:dev49j";
  private String _driver = "oracle.jdbc.driver.OracleDriver";

  private boolean transactionalTest = true;

  public AbstractDBTest()
  {
  }

  /**
   * base setup, create connection object. Subclasses must call the superclass method
   * @throws Exception
   */
  public void setUp()
    throws Exception
  {
    Class.forName(getDriver());
    Connection con = DriverManager.getConnection(getConnectionURL(), getUserName(), getPassword());
    
    if(transactionalTest)
    {
      con.setAutoCommit(false);
    }
    
    _connection = new TransactionalConnection(con);
    
  }

/**
   * base setup, closes connection object, rolls back transaction. Subclasses must call the superclass method
   * @throws Exception
   */
  public void tearDown()
    throws Exception
  {
  
    if(transactionalTest)
    {
      _connection.rollback();
    }
    
    _connection.close();
  }

  public Connection getConnection()
  {
    return _connection;
  }

  protected String getUserName()
  {
    return _userName;
  }

  protected String getPassword()
  {
    return _password;
  }

  public String getConnectionURL()
  {
    return _connectionURL;
  }

  protected String getDriver()
  {
    return _driver;
  }
  
  /**
   * Override transacton mode, call this in the setup of the sub-class to turn off the test transaction
   * @param transactionalTest
   */
  public void setTransactionalTest(boolean transactionalTest)
  {
    this.transactionalTest = transactionalTest;
  }
}
