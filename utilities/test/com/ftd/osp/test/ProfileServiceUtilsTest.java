package com.ftd.osp.test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.junit.Ignore;
import org.junit.Test;

import com.ftd.ftdutilities.ProfileServiceUtils;
import com.ftd.osp.utilities.vo.CustomerCCandFSMDetails;
import com.ftd.osp.utilities.vo.CustomerFSHistory;
import com.ftd.osp.utilities.vo.CustomerFSMDetails;
import com.ftd.osp.utilities.vo.MembershipDetailsVO;


public class ProfileServiceUtilsTest { 
	
	@Ignore
	@Test
	public void testGetMembershipDataWithTimeoutException() {
		
		List<String> emailList = new ArrayList<String>();
		emailList.add("msalla@ftdi.com");
		Map<String, MembershipDetailsVO> response = ProfileServiceUtils.getMembershipDataWithTimeoutException(emailList, new Date());
		System.out.println("testGetMembershipData: " + response);
	}
	
	@Ignore
	@Test
	public void testGetFSMDetails() { 
		CustomerFSMDetails response = null;
		try {
			response = ProfileServiceUtils.getFSMDetails("msalla@ftdi.com");
		} catch (Exception e) {
			System.out.println(e);
		}
		System.out.println("testGetFSMDetails: " + response);
	}
	
	@Ignore
	@Test
	public void testGetCustomerFSHistory() { 
		List<CustomerFSHistory> response = null;
		try {
			List<String> emailList = new ArrayList<String>();
			emailList.add("msalla@ftdi.com");			
			emailList.add("madhan@ftdi.com");
			List<String> eventList = new ArrayList<String>();
			eventList.add("fsm_order");			
			eventList.add("fs_auto_renew");
			
			response = ProfileServiceUtils.getCustomerFSHistory(emailList, eventList);
		} catch (Exception e) {
			System.out.println(e);
		}
		System.out.println("testGetCustomerFSHistory: " + response);
	}
	
	@Ignore
	@Test
	public void testUpdateProgramStatusInProfile() { 
		Boolean response = null;
		try {
			response = ProfileServiceUtils.updateProgramStatusInProfile("msalla@ftdi.com", "690657897", "A", 12, true, null);
		} catch (Exception e) {
			System.out.println(e);
		}
		System.out.println("testUpdateProgramStatusInProfile: " + response);
	}
	
	
	@Ignore
	@Test
	public void testGetExpiredEmailsByExpireDate() throws Exception {		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String startDate = "2018-06-18";
		String endDate = "2019-06-18";
		List<String> response = ProfileServiceUtils.getExpiredEmailsByExpireDate(sdf.parse(startDate), sdf.parse(endDate));
		System.out.println("testGetExpiredEmailsByExpireDate: " + response);
		
	}
	
	//@Ignore
	@Test
	public void testGetFSAccount() throws Exception {
		CustomerCCandFSMDetails response = ProfileServiceUtils.getFSAccount("skadar5@ftdi.com");
		System.out.println("testGetFSAccount: " + response);
	}
	
	@Ignore
	@Test
	public void testSendCreditCardStatusToProfile() { 
		boolean response = ProfileServiceUtils.sendCreditCardStatusToProfile("Success", "Billing", "msalla@ftdi.com", "11", "2023","1234");
		System.out.println("testSendCreditCardStatusToProfile: "+ response);
	}
	
}
