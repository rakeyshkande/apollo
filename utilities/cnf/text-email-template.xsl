<!DOCTYPE xsl:stylesheet[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<!--xsl:param name="header-content" />
	<xsl:param name="guarantee-content" />
	<xsl:param name="marketing-content" />
	<xsl:param name="contact-content" /-->
	<xsl:output method="text" />
	<xsl:variable name="vlength" select="25"/>
	<xsl:variable name="vpad-character" select="'&nbsp;'" />

	<!-- Root template -->
	<xsl:template match="/">
		<xsl:text disable-output-escaping="yes">&#xD;&#xA;</xsl:text>
		<xsl:value-of select="order/text_email_section/header_content" />
		<xsl:text disable-output-escaping="yes">&#xD;&#xA;</xsl:text>
		<xsl:for-each select="order/items/item">
			<xsl:text disable-output-escaping="yes">&#xD;&#xA;</xsl:text>
			<xsl:call-template name="TEXT-PADDER-TEMPLATE">
				<xsl:with-param name="ptext" select="'-'"/>
				<xsl:with-param name="ppad-character" select="'-'"/>
				<xsl:with-param name="plength" select="80"/>
			</xsl:call-template>
			<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
			<xsl:call-template name="TEXT-PADDER-TEMPLATE">
				<xsl:with-param name="ptext" select="$vpad-character"/>
				<xsl:with-param name="ppad-character" select="$vpad-character"/>
				<xsl:with-param name="plength" select="$vlength"/>
			</xsl:call-template>
			<xsl:text disable-output-escaping="yes">&#xD;&#xA;</xsl:text>
			<xsl:call-template name="TEXT-PADDER-TEMPLATE">
				<xsl:with-param name="ptext" select="'Order Number:'"/>
				<xsl:with-param name="ppad-character" select="$vpad-character"/>
				<xsl:with-param name="plength" select="$vlength"/>
			</xsl:call-template>
			<xsl:value-of select="/order/header/master_order_number"/>
			<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
			<xsl:call-template name="TEXT-PADDER-TEMPLATE">
				<xsl:with-param name="ptext" select="$vpad-character"/>
				<xsl:with-param name="ppad-character" select="$vpad-character"/>
				<xsl:with-param name="plength" select="$vlength"/>
			</xsl:call-template>
			<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
			<!--xsl:call-template name="TEXT-PADDER-TEMPLATE">
				<xsl:with-param name="ptext" select="'Reference Number:'"/>
				<xsl:with-param name="ppad-character" select="$vpad-character"/>
				<xsl:with-param name="plength" select="$vlength"/>
			</xsl:call-template>
			<xsl:value-of select="'REFNBR'"/>
			<xsl:text disable-output-escaping="yes">&#xD;&#xA;</xsl:text-->
			<xsl:call-template name="TEXT-PADDER-TEMPLATE">
				<xsl:with-param name="ptext" select="'Order Date:'"/>
				<xsl:with-param name="ppad-character" select="$vpad-character"/>
				<xsl:with-param name="plength" select="$vlength"/>
			</xsl:call-template>
			<xsl:value-of select="substring(/order/header/order_date,1,10)"/>
			<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
			<xsl:call-template name="DELIVERY-DATE-TEMPLATE" />
			<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
			<xsl:call-template name="TEXT-PADDER-TEMPLATE">
				<xsl:with-param name="ptext" select="'Delivery to:'"/>
				<xsl:with-param name="ppad-character" select="$vpad-character"/>
				<xsl:with-param name="plength" select="$vlength"/>
			</xsl:call-template>
			<xsl:value-of select="recip_first_name"/>&nbsp;<xsl:value-of select="recip_last_name"/>
			<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
			<xsl:call-template name="TEXT-PADDER-TEMPLATE">
				<xsl:with-param name="ptext" select="$vpad-character"/>
				<xsl:with-param name="ppad-character" select="$vpad-character"/>
				<xsl:with-param name="plength" select="$vlength"/>
			</xsl:call-template>
			<xsl:call-template name="ADDRESS-TYPE-TEMPLATE" />
			<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
			<xsl:call-template name="TEXT-PADDER-TEMPLATE">
				<xsl:with-param name="ptext" select="$vpad-character"/>
				<xsl:with-param name="ppad-character" select="$vpad-character"/>
				<xsl:with-param name="plength" select="$vlength"/>
			</xsl:call-template>
			<xsl:value-of select="recip_address1"/>
			<xsl:call-template name="ADDRESS-2-TEMPLATE" />
			<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
			<xsl:call-template name="TEXT-PADDER-TEMPLATE">
				<xsl:with-param name="ptext" select="$vpad-character"/>
				<xsl:with-param name="ppad-character" select="$vpad-character"/>
				<xsl:with-param name="plength" select="$vlength"/>
			</xsl:call-template>
			<xsl:value-of select="recip_city"/>, <xsl:value-of select="recip_state"/> <xsl:value-of select="recip_postal_code"/>
			<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
			<xsl:call-template name="TEXT-PADDER-TEMPLATE">
				<xsl:with-param name="ptext" select="$vpad-character"/>
				<xsl:with-param name="ppad-character" select="$vpad-character"/>
				<xsl:with-param name="plength" select="$vlength"/>
			</xsl:call-template>
			<xsl:value-of select="recip_country"/>
			<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
			<xsl:call-template name="TEXT-PADDER-TEMPLATE">
				<xsl:with-param name="ptext" select="$vpad-character"/>
				<xsl:with-param name="ppad-character" select="$vpad-character"/>
				<xsl:with-param name="plength" select="$vlength"/>
			</xsl:call-template>
			<xsl:value-of select="recip_phone"/>
			<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
			<xsl:call-template name="TEXT-PADDER-TEMPLATE">
				<xsl:with-param name="ptext" select="$vpad-character"/>
				<xsl:with-param name="ppad-character" select="$vpad-character"/>
				<xsl:with-param name="plength" select="$vlength"/>
			</xsl:call-template>
			<xsl:text disable-output-escaping="yes">&#xD;&#xA;</xsl:text>
			<xsl:call-template name="TEXT-PADDER-TEMPLATE">
				<xsl:with-param name="ptext" select="'Gift Message:'"/>
				<xsl:with-param name="ppad-character" select="$vpad-character"/>
				<xsl:with-param name="plength" select="$vlength"/>
			</xsl:call-template>
			<xsl:call-template name="TEXT-WRAPPER-TEMPLATE">
				<xsl:with-param name="ptext" select="concat(card_message,' ',card_signature)"/>
				<xsl:with-param name="plength" select="55"/>
				<xsl:with-param name="pindent-first-line-with-spaces" select="'false'"/>
				<xsl:with-param name="pindent-wrapped-lines-with-spaces" select="'true'"/>
				<xsl:with-param name="pindent-length" select="25"/>
				<xsl:with-param name="pbegin-at-newline" select="'false'"/>
			</xsl:call-template>
			<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
			<!-- Blank Line -->
			<xsl:call-template name="TEXT-PADDER-TEMPLATE">
				<xsl:with-param name="ptext" select="$vpad-character"/>
				<xsl:with-param name="ppad-character" select="$vpad-character"/>
				<xsl:with-param name="plength" select="$vlength"/>
			</xsl:call-template>
			<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
			<xsl:call-template name="SPECIAL-INSTRUCTIONS-TEMPLATE" />
			<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
			<!-- Blank Line -->
			<xsl:call-template name="TEXT-PADDER-TEMPLATE">
				<xsl:with-param name="ptext" select="$vpad-character"/>
				<xsl:with-param name="ppad-character" select="$vpad-character"/>
				<xsl:with-param name="plength" select="$vlength"/>
			</xsl:call-template>
			<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
			<xsl:call-template name="TEXT-PADDER-TEMPLATE">
				<xsl:with-param name="ptext" select="substring(product_description,1,20)"/>
				<xsl:with-param name="ppad-character" select="$vpad-character"/>
				<xsl:with-param name="plength" select="50"/>
      </xsl:call-template>$<xsl:value-of select="format-number(product_price, '#,###,##0.00')"/>
			<xsl:call-template name="TEXT-WRAPPER-TEMPLATE">
				<xsl:with-param name="ptext" select="substring(product_description,21,string-length(product_description))"/>
				<xsl:with-param name="plength" select="20"/>
				<xsl:with-param name="pindent-first-line-with-spaces" select="'false'"/>
				<xsl:with-param name="pindent-wrapped-lines-with-spaces" select="'false'"/>
				<xsl:with-param name="pindent-length" select="0"/>
			</xsl:call-template>
			<xsl:call-template name="COLOR-SIZE-TEMPLATE" />
			<xsl:call-template name="ADD-ON-TEMPLATE" />
			<xsl:call-template name="DISCOUNT-AMOUNT-TEMPLATE" />
			<xsl:call-template name="SERVICE-FEE-TEMPLATE" />
			<xsl:call-template name="SHIPPING-FEE-TEMPLATE" />
			<xsl:call-template name="TAX-TEMPLATE" />
			<xsl:call-template name="SUBTOTAL-TEMPLATE" />
			<xsl:call-template name="LAST-MINUTE-GIFT-EMAIL-TEMPLATE" />
		</xsl:for-each>
		<xsl:call-template name="TOTAL-TEMPLATE" />
		<xsl:call-template name="CREDIT-CARD-TEMPLATE" />
		<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
		<xsl:value-of select="'All prices are in US dollars'" />
		<!-- Blank Line -->
		<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
		<xsl:call-template name="TEXT-PADDER-TEMPLATE">
			<xsl:with-param name="ptext" select="$vpad-character"/>
			<xsl:with-param name="ppad-character" select="$vpad-character"/>
			<xsl:with-param name="plength" select="$vlength"/>
		</xsl:call-template>
		<xsl:call-template name="PARTNER-INFO-TEMPLATE" />
		<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
		<xsl:value-of select="order/text_email_section/guarantee_content" />
		<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
		<xsl:value-of select="order/text_email_section/marketing_content" />
		<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
		<xsl:value-of select="order/text_email_section/contact_content" />
	</xsl:template>

	<xsl:template name="PARTNER-INFO-TEMPLATE">
		<xsl:choose>
			<xsl:when test="string-length(/order/header/partner_id)">
				<xsl:if test="contains(/order/items/item/discount_type, 'CHARITY')">
					<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
					<xsl:value-of select="concat('FTD.COM will donate 5% of the product price to',' ', /order/additional_order_information/partner/partner_information/@partner_name)" />
				</xsl:if>
				<xsl:if test="contains(/order/header/partner_id,'DISC1')">
					<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
          <xsl:value-of select="concat('Your purchase from FTD.COM has earned you $'format-number(,/order/items/item/miles_points, '#,###,##0.00') ' ','towards your Cash Back Bonus with Discover')" />
				</xsl:if>
				<xsl:if test="string-length(/order/items/item/miles_points)">
					<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
					<xsl:value-of select="concat('You will earn',' ', /order/items/item/miles_points, ' ',/order/items/item/discount_type)" />
					<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
					<xsl:value-of select="'Please allow 6-8 weeks for miles/points to post to your account'" />
				</xsl:if>
			</xsl:when>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="COLOR-SIZE-TEMPLATE">
		<xsl:choose>
			<xsl:when test="contains(size,'C')">
				<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
				<xsl:value-of select="productid"/>
				<xsl:value-of select="first_color_choice"/>
				<xsl:call-template name="TEXT-PADDER-TEMPLATE">
					<xsl:with-param name="ptext" select="$vpad-character"/>
					<xsl:with-param name="ppad-character" select="$vpad-character"/>
					<xsl:with-param name="plength" select="12"/>
				</xsl:call-template>2nd Choice - <xsl:value-of select="second_color_choice"/>
				<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
				<xsl:call-template name="TEXT-PADDER-TEMPLATE">
					<xsl:with-param name="ptext" select="$vpad-character"/>
					<xsl:with-param name="ppad-character" select="$vpad-character"/>
					<xsl:with-param name="plength" select="$vlength"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
				<xsl:value-of select="productid"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="ADD-ON-TEMPLATE">
		<xsl:for-each select="./add_ons/add_on">
			<xsl:choose>
				<xsl:when test="starts-with(./code,'RC')">
					<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
					<xsl:call-template name="TEXT-PADDER-TEMPLATE">
						<xsl:with-param name="ptext" select="concat('Greeting Card Qty',' ',./quantity)"/>
						<xsl:with-param name="ppad-character" select="$vpad-character"/>
						<xsl:with-param name="plength" select="50"/>
          </xsl:call-template>$<xsl:value-of select="format-number((number(./quantity) * number(./price)), '#,###,##0.00')"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
					<xsl:call-template name="TEXT-PADDER-TEMPLATE">
						<xsl:with-param name="ptext" select="concat(./description,' ',./quantity)"/>
						<xsl:with-param name="ppad-character" select="$vpad-character"/>
						<xsl:with-param name="plength" select="50"/>
          </xsl:call-template>$<xsl:value-of select="format-number((number(./quantity) * number(./price)), '#,###,##0.00')"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>
	</xsl:template>

	<xsl:template name="CREDIT-CARD-TEMPLATE">
		<xsl:if test="contains(order/header/payment_method,'C')" >
			<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
			<xsl:call-template name="TEXT-PADDER-TEMPLATE">
				<xsl:with-param name="ptext" select="'Credit Card Type:'"/>
				<xsl:with-param name="ppad-character" select="$vpad-character"/>
				<xsl:with-param name="plength" select="$vlength"/>
			</xsl:call-template>
	        <xsl:call-template name="TRANSFORM-CREDIT-CARD-TYPE">
	    		<xsl:with-param name="credit-card-type" select="order/header/payment_method_id"/>
	        </xsl:call-template>
			<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
			<xsl:call-template name="TEXT-PADDER-TEMPLATE">
				<xsl:with-param name="ptext" select="$vpad-character"/>
				<xsl:with-param name="ppad-character" select="$vpad-character"/>
				<xsl:with-param name="plength" select="$vlength"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>
	<xsl:template name="TOTAL-TEMPLATE">
		<xsl:text disable-output-escaping="yes">&#xD;&#xA;</xsl:text>
		<xsl:call-template name="TEXT-PADDER-TEMPLATE">
			<xsl:with-param name="ptext" select="'-'"/>
			<xsl:with-param name="ppad-character" select="'-'"/>
			<xsl:with-param name="plength" select="80"/>
		</xsl:call-template>
		<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
		<xsl:call-template name="TEXT-PADDER-TEMPLATE">
			<xsl:with-param name="ptext" select="$vpad-character"/>
			<xsl:with-param name="ppad-character" select="$vpad-character"/>
			<xsl:with-param name="plength" select="$vlength"/>
		</xsl:call-template>
		<xsl:choose>
			<xsl:when test="contains(/order/additional_order_information/jcpenney_flag/text(),'Y')" >
			    <xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
				<xsl:call-template name="TEXT-PADDER-TEMPLATE">
					<xsl:with-param name="ptext" select="'Approximate Total:'"/>
					<xsl:with-param name="ppad-character" select="$vpad-character"/>
					<xsl:with-param name="plength" select="$vlength"/>
        </xsl:call-template>$<xsl:value-of select="format-number(order/header/order_amount, '#,###,##0.00')"/>
			</xsl:when>
			<xsl:otherwise>
			    <xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
				<xsl:call-template name="TEXT-PADDER-TEMPLATE">
					<xsl:with-param name="ptext" select="'Total Charge:'"/>
					<xsl:with-param name="ppad-character" select="$vpad-character"/>
					<xsl:with-param name="plength" select="$vlength"/>
        </xsl:call-template>$<xsl:value-of select="format-number(order/header/order_amount, '#,###,##0.00')"/>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
		<xsl:call-template name="TEXT-PADDER-TEMPLATE">
			<xsl:with-param name="ptext" select="$vpad-character"/>
			<xsl:with-param name="ppad-character" select="$vpad-character"/>
			<xsl:with-param name="plength" select="$vlength"/>
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="SHIPPING-FEE-TEMPLATE">
		<xsl:variable name="vshipping_method" select="shipping_method" />
		<xsl:choose>
			<xsl:when test="number(service_fee) &gt; 0">
				<xsl:choose>
					<xsl:when test="contains(shipping_method,'SD')">
						<xsl:text disable-output-escaping="yes">&#xD;&#xA;</xsl:text>
						<xsl:call-template name="TEXT-PADDER-TEMPLATE">
							<xsl:with-param name="ptext" select="'Service Fee:'"/>
							<xsl:with-param name="ppad-character" select="$vpad-character"/>
							<xsl:with-param name="plength" select="$vlength"/>
						</xsl:call-template>
						<xsl:call-template name="TEXT-PADDER-TEMPLATE">
							<xsl:with-param name="ptext" select="$vpad-character"/>
							<xsl:with-param name="ppad-character" select="$vpad-character"/>
							<xsl:with-param name="plength" select="$vlength"/>
            </xsl:call-template>$<xsl:value-of select="format-number(drop_ship_charges, '#,###,##0.00')"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text disable-output-escaping="yes">&#xD;&#xA;</xsl:text>
						<xsl:call-template name="TEXT-PADDER-TEMPLATE">
							<xsl:with-param name="ptext" select="/order/additional_order_information/ship_method/ship_method_information[@shipmethodid=$vshipping_method]/@description"/>
							<xsl:with-param name="ppad-character" select="$vpad-character"/>
							<xsl:with-param name="plength" select="$vlength"/>
						</xsl:call-template>
						<xsl:call-template name="TEXT-PADDER-TEMPLATE">
							<xsl:with-param name="ptext" select="$vpad-character"/>
							<xsl:with-param name="ppad-character" select="$vpad-character"/>
							<xsl:with-param name="plength" select="$vlength"/>
            </xsl:call-template>$<xsl:value-of select="format-number(drop_ship_charges, '#,###,##0.00')"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="SUBTOTAL-TEMPLATE">
			<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
			<xsl:call-template name="TEXT-PADDER-TEMPLATE">
				<xsl:with-param name="ptext" select="'-'"/>
				<xsl:with-param name="ppad-character" select="'-'"/>
				<xsl:with-param name="plength" select="9"/>
			</xsl:call-template>
			<xsl:call-template name="TEXT-PADDER-TEMPLATE">
				<xsl:with-param name="ptext" select="$vpad-character"/>
				<xsl:with-param name="ppad-character" select="$vpad-character"/>
				<xsl:with-param name="plength" select="39"/>
			</xsl:call-template>
			<xsl:call-template name="TEXT-PADDER-TEMPLATE">
				<xsl:with-param name="ptext" select="'-'"/>
				<xsl:with-param name="ppad-character" select="'-'"/>
				<xsl:with-param name="plength" select="9"/>
			</xsl:call-template>
			<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
			<xsl:call-template name="TEXT-PADDER-TEMPLATE">
				<xsl:with-param name="ptext" select="'Subtotal:'"/>
				<xsl:with-param name="ppad-character" select="$vpad-character"/>
				<xsl:with-param name="plength" select="$vlength"/>
			</xsl:call-template>
			<xsl:call-template name="TEXT-PADDER-TEMPLATE">
				<xsl:with-param name="ptext" select="$vpad-character"/>
				<xsl:with-param name="ppad-character" select="$vpad-character"/>
				<xsl:with-param name="plength" select="$vlength"/>
      </xsl:call-template>$<xsl:value-of select="format-number(order_total, '#,###,##0.00')"/>
			<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
			<xsl:call-template name="TEXT-PADDER-TEMPLATE">
				<xsl:with-param name="ptext" select="$vpad-character"/>
				<xsl:with-param name="ppad-character" select="$vpad-character"/>
				<xsl:with-param name="plength" select="$vlength"/>
			</xsl:call-template>
	</xsl:template>
	<xsl:template name="LAST-MINUTE-GIFT-EMAIL-TEMPLATE">
		<xsl:if test="string-length(lmg_flag)">
			<xsl:text disable-output-escaping="yes">&#xD;&#xA;</xsl:text>
		    <xsl:value-of select="'An E-mail will be sent to '" /><xsl:value-of select="lmg_email_address" /><xsl:value-of select="' from '" /><xsl:value-of select="lmg_email_signature" />
			<xsl:text disable-output-escaping="yes">&#xD;&#xA;</xsl:text>
			<xsl:call-template name="TEXT-PADDER-TEMPLATE">
				<xsl:with-param name="ptext" select="$vpad-character"/>
				<xsl:with-param name="ppad-character" select="$vpad-character"/>
				<xsl:with-param name="plength" select="$vlength"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>
	<xsl:template name="SERVICE-FEE-TEMPLATE">
		<xsl:choose>
			<xsl:when test="number(service_fee) &gt; 0">
				<xsl:text disable-output-escaping="yes">&#xD;&#xA;</xsl:text>
				<xsl:call-template name="TEXT-PADDER-TEMPLATE">
					<xsl:with-param name="ptext" select="'Service Charges:'"/>
					<xsl:with-param name="ppad-character" select="$vpad-character"/>
					<xsl:with-param name="plength" select="$vlength"/>
				</xsl:call-template>
				<xsl:call-template name="TEXT-PADDER-TEMPLATE">
					<xsl:with-param name="ptext" select="$vpad-character"/>
					<xsl:with-param name="ppad-character" select="$vpad-character"/>
					<xsl:with-param name="plength" select="$vlength"/>
        </xsl:call-template>$<xsl:value-of select="format-number(service_fee, '#,###,##0.00')"/>
			</xsl:when>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="TAX-TEMPLATE">
		<xsl:choose>
			<xsl:when test="number(tax_amount) &gt; 0">
				<xsl:text disable-output-escaping="yes">&#xD;&#xA;</xsl:text>
				<xsl:call-template name="TEXT-PADDER-TEMPLATE">
					<xsl:with-param name="ptext" select="'Tax:'"/>
					<xsl:with-param name="ppad-character" select="$vpad-character"/>
					<xsl:with-param name="plength" select="$vlength"/>
				</xsl:call-template>
				<xsl:call-template name="TEXT-PADDER-TEMPLATE">
					<xsl:with-param name="ptext" select="$vpad-character"/>
					<xsl:with-param name="ppad-character" select="$vpad-character"/>
					<xsl:with-param name="plength" select="$vlength"/>
        </xsl:call-template>$<xsl:value-of select="format-number(tax_amount, '#,###,##0.00')"/>
			</xsl:when>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="DISCOUNT-AMOUNT-TEMPLATE">
		<xsl:choose>
			<xsl:when test="number(discount_amount) &gt; 0">
				<xsl:text disable-output-escaping="yes">&#xD;&#xA;</xsl:text>
				<xsl:call-template name="TEXT-PADDER-TEMPLATE">
					<xsl:with-param name="ptext" select="'Discount:'"/>
					<xsl:with-param name="ppad-character" select="$vpad-character"/>
					<xsl:with-param name="plength" select="$vlength"/>
				</xsl:call-template>
				<xsl:call-template name="TEXT-PADDER-TEMPLATE">
					<xsl:with-param name="ptext" select="$vpad-character"/>
					<xsl:with-param name="ppad-character" select="$vpad-character"/>
					<xsl:with-param name="plength" select="$vlength"/>
        </xsl:call-template>$<xsl:value-of select="format-number(discount_amount, '#,###,##0.00')"/>
			</xsl:when>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="ADDRESS-2-TEMPLATE">
		<xsl:if test="string-length(recip_address2)">
			<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
			<xsl:call-template name="TEXT-PADDER-TEMPLATE">
				<xsl:with-param name="ptext" select="$vpad-character"/>
				<xsl:with-param name="ppad-character" select="$vpad-character"/>
				<xsl:with-param name="plength" select="$vlength"/>
			</xsl:call-template>
			<xsl:value-of select="recip_address2"/>
		</xsl:if>
	</xsl:template>
	<xsl:template name="ADDRESS-TYPE-TEMPLATE">
		<xsl:value-of select="ship_to_type"/>
		<xsl:choose>
			<xsl:when test="not(contains(ship_to_type,'HOME'))">
				<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
				<xsl:call-template name="TEXT-PADDER-TEMPLATE">
					<xsl:with-param name="ptext" select="$vpad-character"/>
					<xsl:with-param name="ppad-character" select="$vpad-character"/>
					<xsl:with-param name="plength" select="$vlength"/>
				</xsl:call-template>
				<xsl:value-of select="ship_to_type_name"/>
			</xsl:when>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="SPECIAL-INSTRUCTIONS-TEMPLATE">
		<xsl:choose>
			<xsl:when test="string-length(special_instructions)">
				<xsl:if test="not(contains(ship_to_type,'FUNERAL HOME'))" >
					<xsl:call-template name="TEXT-PADDER-TEMPLATE">
						<xsl:with-param name="ptext" select="'Additional Recipient'"/>
						<xsl:with-param name="ppad-character" select="$vpad-character"/>
						<xsl:with-param name="plength" select="$vlength"/>
					</xsl:call-template>
					<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
					<xsl:call-template name="TEXT-PADDER-TEMPLATE">
						<xsl:with-param name="ptext" select="'Information:'"/>
						<xsl:with-param name="ppad-character" select="$vpad-character"/>
						<xsl:with-param name="plength" select="$vlength"/>
					</xsl:call-template>
					<xsl:call-template name="TEXT-WRAPPER-TEMPLATE">
						<xsl:with-param name="ptext" select="'PLEASE NOTE: We cannot honor requests for delivery at specific times of day.'"/>
						<xsl:with-param name="plength" select="54"/>
						<xsl:with-param name="pindent-first-line-with-spaces" select="'false'"/>
						<xsl:with-param name="pindent-wrapped-lines-with-spaces" select="'true'"/>
						<xsl:with-param name="pindent-length" select="25"/>
						<xsl:with-param name="pbegin-at-newline" select="'false'"/>
					</xsl:call-template>
					<xsl:call-template name="TEXT-WRAPPER-TEMPLATE">
						<xsl:with-param name="ptext" select="special_instructions"/>
						<xsl:with-param name="plength" select="55"/>
						<xsl:with-param name="pindent-first-line-with-spaces" select="'true'"/>
						<xsl:with-param name="pindent-wrapped-lines-with-spaces" select="'true'"/>
						<xsl:with-param name="pindent-length" select="25"/>
						<xsl:with-param name="pbegin-at-newline" select="'true'"/>
					</xsl:call-template>
				</xsl:if>
			</xsl:when>
			<xsl:otherwise></xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="DELIVERY-DATE-TEMPLATE">
		<xsl:choose>
			<xsl:when test="string-length(shipping_method) = 2 and contains(shipping_method,'GR')">
					<xsl:call-template name="TEXT-PADDER-TEMPLATE">
						<xsl:with-param name="ptext" select="'Delivery On or Before:'"/>
						<xsl:with-param name="ppad-character" select="$vpad-character"/>
						<xsl:with-param name="plength" select="$vlength"/>
					</xsl:call-template>
					<xsl:value-of select="substring(delivery_date,1,10)"/>
			</xsl:when>
			<xsl:when test="string-length(delivery_date_range_end)">
					<xsl:call-template name="TEXT-PADDER-TEMPLATE">
						<xsl:with-param name="ptext" select="'Delivery Date:'"/>
						<xsl:with-param name="ppad-character" select="$vpad-character"/>
						<xsl:with-param name="plength" select="$vlength"/>
					</xsl:call-template>
					<xsl:value-of select="substring(delivery_date_range_end,1,10)"/>
			</xsl:when>
			<xsl:otherwise>
					<xsl:call-template name="TEXT-PADDER-TEMPLATE">
						<xsl:with-param name="ptext" select="'Delivery On:'"/>
						<xsl:with-param name="ppad-character" select="$vpad-character"/>
						<xsl:with-param name="plength" select="$vlength"/>
					</xsl:call-template>
					<xsl:value-of select="substring(delivery_date,1,10)"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!--this will display the first n chars of $vtext then pass the rest to wrapper-helper if no string is left it will stop-->
	<xsl:template name="TEXT-WRAPPER-TEMPLATE">
		<xsl:param name="ptext"/>
		<xsl:param name="plength"/>
		<xsl:param name="pindent-first-line-with-spaces"/>
		<xsl:param name="pindent-wrapped-lines-with-spaces"/>
		<xsl:param name="pindent-length"/>
		<xsl:param name="pbegin-at-newline" />
		<xsl:choose>
			<xsl:when test="string-length($ptext)">
				<xsl:if test="contains($pbegin-at-newline,'true')" >
					<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
				</xsl:if>
				<xsl:if test="contains($pindent-first-line-with-spaces,'true')" >
					<xsl:call-template name="TEXT-PADDER-TEMPLATE">
						<xsl:with-param name="ptext" select="$vpad-character"/>
						<xsl:with-param name="ppad-character" select="$vpad-character"/>
						<xsl:with-param name="plength" select="$pindent-length"/>
					</xsl:call-template>
				</xsl:if>
				<xsl:value-of select="substring($ptext,1,number($plength))"/>
				<br/>
				<xsl:call-template name="TEXT-WRAPPER-HELPER-TEMPLATE">
					<xsl:with-param name="ptext" select="substring($ptext,(number($plength)+1))"/>
					<xsl:with-param name="plength" select="$plength"/>
					<xsl:with-param name="pindent-wrapped-lines-with-spaces" select="$pindent-wrapped-lines-with-spaces"/>
					<xsl:with-param name="pindent-length" select="$pindent-length"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise></xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!--this will also display n chars of the string, and pass the rest back to text-wrapper-->
	<xsl:template name="TEXT-WRAPPER-HELPER-TEMPLATE">
		<xsl:param name="ptext"/>
		<xsl:param name="plength"/>
		<xsl:param name="pindent-wrapped-lines-with-spaces"/>
		<xsl:param name="pindent-length"/>
		<xsl:param name="pbegin-at-newline" />
		<xsl:text disable-output-escaping="yes">&#xA;</xsl:text>
		<xsl:choose>
			<xsl:when test="contains($pindent-wrapped-lines-with-spaces,'true')">
				<xsl:call-template name="TEXT-PADDER-TEMPLATE">
					<xsl:with-param name="ptext" select="$vpad-character"/>
					<xsl:with-param name="ppad-character" select="$vpad-character"/>
					<xsl:with-param name="plength" select="$pindent-length"/>
				</xsl:call-template>
			</xsl:when>
		</xsl:choose>
		<xsl:value-of select="substring($ptext,1,number($plength))"/>
		<xsl:call-template name="TEXT-WRAPPER-TEMPLATE">
			<xsl:with-param name="ptext" select="substring($ptext,(number($plength)+1))"/>
			<xsl:with-param name="plength" select="$vlength"/>
			<xsl:with-param name="pindent-wrapped-lines-with-spaces" select="$pindent-wrapped-lines-with-spaces"/>
			<xsl:with-param name="pindent-length" select="$pindent-length"/>
			<xsl:with-param name="pbegin-at-newline" select="$pbegin-at-newline"/>
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="TEXT-PADDER-TEMPLATE">
		<xsl:param name="ptext"/>
		<xsl:param name="ppad-character"/>
		<xsl:param name="plength"/>
		<xsl:choose>
			<xsl:when test="string-length($ptext) &lt; number($plength)">
				<xsl:call-template name="TEXT-PADDER-HELPER-TEMPLATE">
					<xsl:with-param name="ptext" select="$ptext"/>
					<xsl:with-param name="ppad-character" select="$ppad-character"/>
					<xsl:with-param name="plength" select="$plength"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$ptext"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="TEXT-PADDER-HELPER-TEMPLATE">
		<xsl:param name="ptext"/>
		<xsl:param name="ppad-character"/>
		<xsl:param name="plength"/>
		<xsl:call-template name="TEXT-PADDER-TEMPLATE">
			<xsl:with-param name="ptext" select="concat($ptext,$ppad-character)"/>
			<xsl:with-param name="ppad-character" select="$ppad-character"/>
			<xsl:with-param name="plength" select="$plength"/>
		</xsl:call-template>
	</xsl:template>
  <xsl:template name="TRANSFORM-CREDIT-CARD-TYPE">
    <xsl:param name="credit-card-type"/>
    <xsl:choose>
      <!-- CC Type -->
      <xsl:when test="contains($credit-card-type, 'VI')">
        <xsl:value-of select="'Visa'" />
      </xsl:when>
      <xsl:when test="contains($credit-card-type, 'MC')">
        <xsl:value-of select="'Master'"/>
      </xsl:when>
      <xsl:when test="contains($credit-card-type, 'AX')">
        <xsl:value-of select="'Amex'" />
      </xsl:when>
      <xsl:when test="contains($credit-card-type, 'DI')">
        <xsl:value-of select="'Discover'"/>
      </xsl:when>
      <xsl:when test="contains($credit-card-type, 'DC')">
        <xsl:value-of select="'Diners'"/>
      </xsl:when>
      <xsl:when test="contains($credit-card-type, 'CB')">
        <xsl:value-of select="'Carte'"/>
      </xsl:when>
      <xsl:otherwise>
      	<xsl:value-of select="$credit-card-type"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

</xsl:stylesheet>