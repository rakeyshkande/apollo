<!DOCTYPE xsl:stylesheet[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	 <xsl:key name="item_ndx" match="order/items/item" use="concat(productid,' ',substring(delivery_date,1,10))" />

	<xsl:output method="html" encoding="utf-8"/>
	<xsl:variable name="vlength" select="25"/>
	<xsl:variable name="vpad-character" select="'&nbsp;'" />
	<!--xsl:param name="header-content" />
	<xsl:param name="guarantee-content" />
	<xsl:param name="marketing-content" />
	<xsl:param name="contact-content" /-->
	<!-- Root template -->
	<xsl:template match="/">
		<html>
			<head></head>
			<body>
				<table border="0">
					<tr>
						<td colspan="2">
							<xsl:value-of select="order/html_email_section/header_content" disable-output-escaping="yes"/>
						</td>
					</tr>
				 </table>
				<table border="0">
				 	<tr>
				 		<td>Customer:</td>
				 		<td><xsl:value-of select="/order/header/buyer_first_name"/>&nbsp;<xsl:value-of select="/order/header/buyer_last_name"/></td>
				 		<td><xsl:value-of select="/order/header/buyer_daytime_phone"/></td>
				 	</tr>
				 	<tr>
				 		<td	colspan="3">&nbsp;</td>
				 	</tr>
				 	<tr>
				 		<td>Order Date:</td>
				 		<td><xsl:value-of select="substring(/order/header/order_date,1,10)"/></td>
				 		<td>&nbsp;</td>
				 	</tr>
				 	<tr>
				 		<td>Master Order Number:</td>
				 		<td><xsl:value-of select="/order/header/master_order_number"/></td>
				 		<td>&nbsp;</td>
				 	</tr>
				 	<tr>
				 		<td	colspan="3">&nbsp;</td>
				 	</tr>
				</table>
				<table border="0">
						<tr>
							<td>QTY</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>Delivery Date</td>
							<td>Ship Method</td>
							<td>&nbsp;</td>
						</tr>
					<xsl:for-each select="order/items/item[generate-id()=generate-id(key('item_ndx',concat(productid,' ',substring(delivery_date,1,10))))]">
		              <xsl:for-each select="key('item_ndx',concat(productid,' ',substring(delivery_date,1,10)))[generate-id()=generate-id(key('item_ndx',concat(productid,' ',substring(delivery_date,1,10))))]">
						<tr>
							<td><xsl:value-of select="count(key('item_ndx',concat(productid,' ',substring(delivery_date,1,10))))"/></td>
							<td><xsl:value-of select="product_description"/>&nbsp;</td>
              <td>@$<xsl:value-of select="format-number(product_price, '#,###,##0.00')"/></td>
							<td><xsl:value-of select="substring(delivery_date,1,10)"/></td>
							<td>
								<xsl:call-template name="TRANSFORM-SHIPPING-METHOD">
									<xsl:with-param name="vshipping_method" select="shipping_method"/>
								</xsl:call-template>
							</td>
              <td>$<xsl:value-of select="format-number(sum(key('item_ndx',concat(productid,' ',substring(delivery_date,1,10)))/order_total),'#,###,##0.00')"/></td>
						</tr>
						<xsl:call-template name="COLOR-SIZE-TEMPLATE" />
					  </xsl:for-each>
					</xsl:for-each>
						<tr>
						<xsl:call-template name="SUBTOTAL-TEMPLATE" />
						</tr>
						<tr><td colspan="6">&nbsp;</td></tr>
						<tr><td colspan="6">&nbsp;</td></tr>
				</table>
			 	<table border="0">
					<tr>
					    <xsl:call-template name="DISCOUNT-AMOUNT-TEMPLATE" />
					</tr>
					<tr>
					    <xsl:call-template name="SERVICE-FEE-TEMPLATE" />
					</tr>
					<tr>
					<xsl:call-template name="SHIPPING-FEE-TEMPLATE" />
					</tr>
					<tr>
					    <xsl:call-template name="TAX-TEMPLATE" />
					</tr>
					<tr>
					<xsl:call-template name="TOTAL-TEMPLATE" />
					</tr>
					<tr>
					    <xsl:call-template name="CREDIT-CARD-TEMPLATE" />
					</tr>
					<tr>
						<td colspan="2">
						<xsl:value-of select="'All prices are in US dollars'" />
						</td>
					</tr>
				</table>
				<table border="0">
				 	<tr>
				 		<td	>&nbsp;</td>
				 	</tr>
					<xsl:call-template name="PARTNER-INFO-TEMPLATE" />
				 	<tr>
				 		<td	>&nbsp;</td>
				 	</tr>
				 	<tr>
				    	<td	>
				    	<xsl:value-of select="order/html_email_section/guarantee_content" disable-output-escaping="yes"/>
				    	</td>
					</tr>
				 	<tr>
				 		<td	>&nbsp;</td>
				 	</tr>
				 	<tr>
				    	<td	>
						<xsl:value-of select="order/html_email_section/marketing_content" disable-output-escaping="yes"/>
				    	</td>
					</tr>
				 	<tr>
				 		<td	>&nbsp;</td>
				 	</tr>
				 	<tr>
				    	<td	>
						<xsl:value-of select="order/html_email_section/contact_content" disable-output-escaping="yes"/>
				    	</td>
					</tr>
				</table>
			</body>
		</html>
	</xsl:template>
	<xsl:template name="PARTNER-INFO-TEMPLATE">
		<xsl:choose>
			<xsl:when test="string-length(/order/header/partner_id)">
				<xsl:if test="contains(/order/items/item/discount_type, 'CHARITY')">
				 	<tr>
				 		<td ><xsl:value-of select="concat('FTD.COM will donate 5% of the product price to',' ', /order/additional_order_information/partner/partner_information/@partner_name)" /></td>
				 	</tr>
				</xsl:if>
				<xsl:if test="contains(/order/header/partner_id,'DISC1')">
				 	<tr>
            <td ><xsl:value-of select="concat('Your purchase from FTD.COM has earned you $'format-number(,/order/items/item/miles_points, '#,###,##0.00') ' ','towards your Cash Back Bonus with Discover')" /></td>
				 	</tr>
				</xsl:if>
				<xsl:if test="string-length(/order/items/item/miles_points)">
				 	<tr>
				 		<td ><xsl:value-of select="concat('You will earn',' ', /order/items/item/miles_points, ' ',/order/items/item/discount_type)" /></td>
				 	</tr>
				 	<tr>
				 		<td ><xsl:value-of select="'Please allow 6-8 weeks for miles/points to post to your account'" /></td>
				 	</tr>
				</xsl:if>
			</xsl:when>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="COLOR-SIZE-TEMPLATE">
     <tr>
	   <td colspan="6">
		<table border="0">
		<xsl:choose>
			<xsl:when test="contains(size,'C')">
			 	<tr>
			 		<td><xsl:value-of select="productid"/>&nbsp;</td>
			 		<td><xsl:value-of select="first_color_choice"/>&nbsp;</td>
			 		<td>2nd Choice -&nbsp;</td>
			 		<td><xsl:value-of select="second_color_choice"/>&nbsp;</td>
				</tr>
			</xsl:when>
			<xsl:otherwise>
				<tr>
					<td colspan="4"><xsl:value-of select="productid"/></td>
				</tr>
			</xsl:otherwise>
		</xsl:choose>
		</table>
       </td>
	 </tr>
	</xsl:template>
	<xsl:template name="ADD-ON-TEMPLATE">
		<xsl:for-each select="./add_ons/add_on">
			<xsl:choose>
				<xsl:when test="starts-with(./code,'RC')">
					<tr>
					  <td><xsl:value-of select="concat('Greeting Card Qty',' ',./quantity)"/></td>
					  <td>&nbsp;</td>
            <td>$<xsl:value-of select="format-number((number(./quantity) * number(./price)), '#,###,##0.00')"/></td>
					</tr>
				</xsl:when>
				<xsl:otherwise>
					<xsl:if test="string-length(./quantity)">
					<tr>
					  <td><xsl:value-of select="concat(./description,' ',./quantity)"/></td>
					  <td>&nbsp;</td>
            <td>$<xsl:value-of select="format-number((number(./quantity) * number(./price)), '#,###,##0.00')"/></td>
					</tr>
					</xsl:if>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>
	</xsl:template>
	
	<xsl:template name="CREDIT-CARD-TEMPLATE">
		<xsl:if test="contains(order/header/payment_method,'C')" >
		 <tr>
		    <td>Credit Card Type:</td>
		    <td>
		        <xsl:call-template name="TRANSFORM-CREDIT-CARD-TYPE">
		    		<xsl:with-param name="credit-card-type" select="order/header/payment_method_id"/>
		        </xsl:call-template>
            </td>
		 </tr>
		</xsl:if>
	</xsl:template>
	
	<xsl:template name="TOTAL-TEMPLATE">
		<tr>
			<td colspan="2">
				<xsl:call-template name="TEXT-PADDER-TEMPLATE">
					<xsl:with-param name="ptext" select="'-'"/>
					<xsl:with-param name="ppad-character" select="'-'"/>
					<xsl:with-param name="plength" select="80"/>
				</xsl:call-template>
			</td>
		</tr>
		<xsl:choose>
			<xsl:when test="contains(/order/additional_order_information/jcpenney_flag/text(),'Y')" >
				 <tr>
				    <td >Approximate Total:</td>
              <td>$<xsl:value-of select="format-number(order/header/order_amount, '#,###,##0.00')"/></td>
				 </tr>
			</xsl:when>
			<xsl:otherwise>
				 <tr>
				    <td>Total Charge:</td>
              <td>$<xsl:value-of select="format-number(order/header/order_amount, '#,###,##0.00')"/></td>
				 </tr>
			</xsl:otherwise>
		</xsl:choose>
		<tr><td colspan="2">&nbsp;</td></tr>
	</xsl:template>
	<xsl:template name="SHIPPING-FEE-TEMPLATE">
		<xsl:variable name="vshipping_method" select="shipping_method" />
		<xsl:choose>
			<xsl:when test="number(service_fee) &gt; 0">
				<xsl:choose>
					<xsl:when test="contains(shipping_method,'SD')">
						 <tr>
						    <td>Service Fee:</td>
                  <td>$<xsl:value-of select="format-number(drop_ship_charges, '#,###,##0.00')"/></td>
						 </tr>
					</xsl:when>
					<xsl:otherwise>
						 <tr>
						    <td><xsl:value-of select="/order/additional_order_information/ship_method/ship_method_information[@shipmethodid=$vshipping_method]/@description"/></td>
                  <td>$<xsl:value-of select="format-number(drop_ship_charges, '#,###,##0.00')"/></td>
						 </tr>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="SUBTOTAL-TEMPLATE">
		<tr>
			<td >
			<xsl:call-template name="TEXT-PADDER-TEMPLATE">
				<xsl:with-param name="ptext" select="'-'"/>
				<xsl:with-param name="ppad-character" select="'-'"/>
				<xsl:with-param name="plength" select="9"/>
			</xsl:call-template>
			</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>
			<xsl:call-template name="TEXT-PADDER-TEMPLATE">
				<xsl:with-param name="ptext" select="'-'"/>
				<xsl:with-param name="ppad-character" select="'-'"/>
				<xsl:with-param name="plength" select="9"/>
			</xsl:call-template>
			</td>
		</tr>
		<tr>
			<td>
			<xsl:call-template name="TEXT-PADDER-TEMPLATE">
				<xsl:with-param name="ptext" select="'Subtotal:'"/>
				<xsl:with-param name="ppad-character" select="$vpad-character"/>
				<xsl:with-param name="plength" select="$vlength"/>
			</xsl:call-template>
			</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>
			<xsl:call-template name="TEXT-PADDER-TEMPLATE">
				<xsl:with-param name="ptext" select="$vpad-character"/>
				<xsl:with-param name="ppad-character" select="$vpad-character"/>
				<xsl:with-param name="plength" select="$vlength"/>
			</xsl:call-template>
			</td>
			<td>
				$<xsl:value-of select="format-number(sum(order/items/item/order_total), '#,###,##0.00')"/>
			</td>
		</tr>
	</xsl:template>
	<xsl:template name="LAST-MINUTE-GIFT-EMAIL-TEMPLATE">
		<xsl:if test="string-length(lmg_flag)">
			<tr>
				<td colspan="3">
				<xsl:value-of select="'An E-mail will be sent to '" />
				<xsl:value-of select="lmg_email_address" />
				<xsl:value-of select="' from '" />
				<xsl:value-of select="lmg_email_signature" />
				</td>
			</tr>
		</xsl:if>
	</xsl:template>
	<xsl:template name="SERVICE-FEE-TEMPLATE">
		<xsl:choose>
			<xsl:when test="sum(order/items/item/service_fee) &gt; 0">
				<tr>
					<td>
					<xsl:call-template name="TEXT-PADDER-TEMPLATE">
						<xsl:with-param name="ptext" select="'Service Charges:'"/>
						<xsl:with-param name="ppad-character" select="$vpad-character"/>
						<xsl:with-param name="plength" select="$vlength"/>
					</xsl:call-template>
					</td>
					<td>
          $<xsl:value-of select="format-number(sum(order/items/item/service_fee), '#,###,##0.00')"/>
					</td>
			   </tr>
			</xsl:when>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="TAX-TEMPLATE">
		<xsl:choose>
			<xsl:when test="sum(order/items/item/tax_amount) &gt; 0">
				<tr>
					<td>
					<xsl:call-template name="TEXT-PADDER-TEMPLATE">
						<xsl:with-param name="ptext" select="'Tax:'"/>
						<xsl:with-param name="ppad-character" select="$vpad-character"/>
						<xsl:with-param name="plength" select="$vlength"/>
					</xsl:call-template>
					</td>
					<td>
          $<xsl:value-of select="format-number(sum(order/items/item/tax_amount), '#,###,##0.00')"/>
					</td>
				</tr>
			</xsl:when>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="DISCOUNT-AMOUNT-TEMPLATE">
		<xsl:choose>
			<xsl:when test="sum(order/items/item/discount_amount) &gt; 0">
				<tr>
					<td>
					<xsl:call-template name="TEXT-PADDER-TEMPLATE">
						<xsl:with-param name="ptext" select="'Discount:'"/>
						<xsl:with-param name="ppad-character" select="$vpad-character"/>
						<xsl:with-param name="plength" select="$vlength"/>
					</xsl:call-template>
					</td>
					<td>
          $<xsl:value-of select="format-number(sum(order/items/item/discount_amount), '#,###,##0.00')"/>
				    </td>
				</tr>
			</xsl:when>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="ADDRESS-2-TEMPLATE">
		<xsl:if test="string-length(recip_address2)">
			<tr>
				<td>
				<xsl:call-template name="TEXT-PADDER-TEMPLATE">
					<xsl:with-param name="ptext" select="$vpad-character"/>
					<xsl:with-param name="ppad-character" select="$vpad-character"/>
					<xsl:with-param name="plength" select="$vlength"/>
				</xsl:call-template>
				</td>
				<td>
				<xsl:value-of select="recip_address2"/>
				</td>
			</tr>
		</xsl:if>
	</xsl:template>
	<xsl:template name="ADDRESS-TYPE-TEMPLATE">
		<xsl:value-of select="ship_to_type"/>
		<xsl:choose>
			<xsl:when test="not(contains(ship_to_type,'HOME'))">
				<tr>
					<td>&nbsp;</td>
					<td>
					<xsl:value-of select="ship_to_type_name"/>
					</td>
				</tr>
			</xsl:when>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="SPECIAL-INSTRUCTIONS-TEMPLATE">
		<xsl:choose>
			<xsl:when test="string-length(special_instructions)">
				<xsl:if test="not(contains(ship_to_type,'FUNERAL HOME'))" >
					<tr>
						<td>
						<xsl:call-template name="TEXT-PADDER-TEMPLATE">
							<xsl:with-param name="ptext" select="'Additional Recipient Information:'"/>
							<xsl:with-param name="ppad-character" select="$vpad-character"/>
							<xsl:with-param name="plength" select="$vlength"/>
						</xsl:call-template>
						</td>
						<td>
						<xsl:call-template name="TEXT-WRAPPER-TEMPLATE">
							<xsl:with-param name="ptext" select="'PLEASE NOTE: We cannot honor requests for delivery at specific times of day.'"/>
							<xsl:with-param name="plength" select="50"/>
							<xsl:with-param name="pindent-first-line-with-spaces" select="'false'"/>
							<xsl:with-param name="pindent-wrapped-lines-with-spaces" select="'true'"/>
							<xsl:with-param name="pindent-length" select="25"/>
							<xsl:with-param name="pbegin-at-newline" select="'false'"/>
						</xsl:call-template>
						</td>
						<td>
						<xsl:call-template name="TEXT-WRAPPER-TEMPLATE">
							<xsl:with-param name="ptext" select="special_instructions"/>
							<xsl:with-param name="plength" select="55"/>
							<xsl:with-param name="pindent-first-line-with-spaces" select="'true'"/>
							<xsl:with-param name="pindent-wrapped-lines-with-spaces" select="'true'"/>
							<xsl:with-param name="pindent-length" select="25"/>
							<xsl:with-param name="pbegin-at-newline" select="'false'"/>
						</xsl:call-template>
						</td>
					</tr>
				</xsl:if>
			</xsl:when>
			<xsl:otherwise></xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="DELIVERY-DATE-TEMPLATE">
		<xsl:choose>
			<xsl:when test="string-length(shipping_method) = 2 and contains(shipping_method,'GR')">
				<tr>
					<td>
					<xsl:call-template name="TEXT-PADDER-TEMPLATE">
						<xsl:with-param name="ptext" select="'Delivery On or Before:'"/>
						<xsl:with-param name="ppad-character" select="$vpad-character"/>
						<xsl:with-param name="plength" select="$vlength"/>
					</xsl:call-template>
					</td>
					<td>
					<xsl:value-of select="substring(delivery_date,1,10)"/>
					</td>
				</tr>
			</xsl:when>
			<xsl:when test="string-length(delivery_date_range_end)">
				<tr>
					<td>
					<xsl:call-template name="TEXT-PADDER-TEMPLATE">
						<xsl:with-param name="ptext" select="'Delivery Date:'"/>
						<xsl:with-param name="ppad-character" select="$vpad-character"/>
						<xsl:with-param name="plength" select="$vlength"/>
					</xsl:call-template>
					</td>
					<td><xsl:value-of select="substring(delivery_date_range_end,1,10)"/></td>
				</tr>
			</xsl:when>
			<xsl:otherwise>
				<tr>
					<td>
					<xsl:call-template name="TEXT-PADDER-TEMPLATE">
						<xsl:with-param name="ptext" select="'Delivery On:'"/>
						<xsl:with-param name="ppad-character" select="$vpad-character"/>
						<xsl:with-param name="plength" select="$vlength"/>
					</xsl:call-template>
					</td>
					<td><xsl:value-of select="substring(delivery_date,1,10)"/></td>
				</tr>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!--this will display the first n chars of $vtext then pass the rest to wrapper-helper if no string is left it will stop-->
	<xsl:template name="TEXT-WRAPPER-TEMPLATE">
		<xsl:param name="ptext"/>
		<xsl:param name="plength"/>
		<xsl:param name="pindent-first-line-with-spaces"/>
		<xsl:param name="pindent-wrapped-lines-with-spaces"/>
		<xsl:param name="pindent-length"/>
		<xsl:param name="pbegin-at-newline" />
		<xsl:choose>
			<xsl:when test="string-length($ptext)">
				<xsl:if test="contains($pbegin-at-newline,'true')" >
				</xsl:if>
				<xsl:choose>
					<xsl:when test="contains($pindent-first-line-with-spaces,'true')" >
					    <tr>
							<td>
							<xsl:call-template name="TEXT-PADDER-TEMPLATE">
								<xsl:with-param name="ptext" select="$vpad-character"/>
								<xsl:with-param name="ppad-character" select="$vpad-character"/>
								<xsl:with-param name="plength" select="$pindent-length"/>
							</xsl:call-template>
							</td>
							<td>
							<xsl:value-of select="substring($ptext,1,number($plength))"/>
							</td>
				    	</tr>
					</xsl:when>
					<xsl:when test="contains($pindent-first-line-with-spaces,'false')" >
							<xsl:value-of select="substring($ptext,1,number($plength))"/>
					</xsl:when>
					<xsl:otherwise>
					    <tr>
							<td>
							<xsl:value-of select="substring($ptext,1,number($plength))"/>
							</td>
					    </tr>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:call-template name="TEXT-WRAPPER-HELPER-TEMPLATE">
					<xsl:with-param name="ptext" select="substring($ptext,(number($plength)+1))"/>
					<xsl:with-param name="plength" select="$plength"/>
					<xsl:with-param name="pindent-wrapped-lines-with-spaces" select="$pindent-wrapped-lines-with-spaces"/>
					<xsl:with-param name="pindent-length" select="$pindent-length"/>
				</xsl:call-template>
			</xsl:when>
		</xsl:choose>
	</xsl:template>
	<!--this will also display n chars of the string, and pass the rest back to text-wrapper-->
	<xsl:template name="TEXT-WRAPPER-HELPER-TEMPLATE">
		<xsl:param name="ptext"/>
		<xsl:param name="plength"/>
		<xsl:param name="pindent-wrapped-lines-with-spaces"/>
		<xsl:param name="pindent-length"/>
		<xsl:param name="pbegin-at-newline" />
		<xsl:choose>
			<xsl:when test="contains($pindent-wrapped-lines-with-spaces,'true')">
				<tr>
					<td>
					<xsl:call-template name="TEXT-PADDER-TEMPLATE">
						<xsl:with-param name="ptext" select="$vpad-character"/>
						<xsl:with-param name="ppad-character" select="$vpad-character"/>
						<xsl:with-param name="plength" select="$pindent-length"/>
					</xsl:call-template>
					</td>
					<td>
					<xsl:value-of select="substring($ptext,1,number($plength))"/>
					</td>
				</tr>
			</xsl:when>
			<xsl:otherwise>
				<tr>
					<td>
					<xsl:value-of select="substring($ptext,1,number($plength))"/>
					</td>
				</tr>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:call-template name="TEXT-WRAPPER-TEMPLATE">
			<xsl:with-param name="ptext" select="substring($ptext,(number($plength)+1))"/>
			<xsl:with-param name="plength" select="$vlength"/>
			<xsl:with-param name="pindent-wrapped-lines-with-spaces" select="$pindent-wrapped-lines-with-spaces"/>
			<xsl:with-param name="pindent-length" select="$pindent-length"/>
			<xsl:with-param name="pbegin-at-newline" select="$pbegin-at-newline"/>
		</xsl:call-template>
	</xsl:template>
	
	<xsl:template name="TEXT-PADDER-TEMPLATE">
		<xsl:param name="ptext"/>
		<xsl:param name="ppad-character"/>
		<xsl:param name="plength"/>
		<xsl:choose>
			<xsl:when test="string-length($ptext) &lt; number($plength)">
				<xsl:call-template name="TEXT-PADDER-HELPER-TEMPLATE">
					<xsl:with-param name="ptext" select="$ptext"/>
					<xsl:with-param name="ppad-character" select="$ppad-character"/>
					<xsl:with-param name="plength" select="$plength"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$ptext"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="TEXT-PADDER-HELPER-TEMPLATE">
		<xsl:param name="ptext"/>
		<xsl:param name="ppad-character"/>
		<xsl:param name="plength"/>
		<xsl:call-template name="TEXT-PADDER-TEMPLATE">
			<xsl:with-param name="ptext" select="concat($ptext,$ppad-character)"/>
			<xsl:with-param name="ppad-character" select="$ppad-character"/>
			<xsl:with-param name="plength" select="$plength"/>
		</xsl:call-template>
	</xsl:template>
	
  <xsl:template name="TRANSFORM-CREDIT-CARD-TYPE">
    <xsl:param name="credit-card-type"/>
    <xsl:choose>
      <!-- CC Type -->
      <xsl:when test="contains($credit-card-type, 'VI')">
        <xsl:value-of select="'Visa'" />
      </xsl:when>
      <xsl:when test="contains($credit-card-type, 'MC')">
        <xsl:value-of select="'Master'"/>
      </xsl:when>
      <xsl:when test="contains($credit-card-type, 'AX')">
        <xsl:value-of select="'Amex'" />
      </xsl:when>
      <xsl:when test="contains($credit-card-type, 'DI')">
        <xsl:value-of select="'Discover'"/>
      </xsl:when>
      <xsl:when test="contains($credit-card-type, 'DC')">
        <xsl:value-of select="'Diners'"/>
      </xsl:when>
      <xsl:when test="contains($credit-card-type, 'CB')">
        <xsl:value-of select="'Carte'"/>
      </xsl:when>
      <xsl:otherwise>
      	<xsl:value-of select="$credit-card-type"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  <xsl:template name="TRANSFORM-SHIPPING-METHOD">
  	<xsl:param name="vshipping_method"/>
  	<xsl:value-of select="/order/additional_order_information/ship_method/ship_method_information[@shipmethodid=$vshipping_method]/@description"/>
  </xsl:template>
</xsl:stylesheet>