package com.ftd.pg.client.common.utils;

import org.apache.commons.httpclient.protocol.SSLProtocolSocketFactory;
import org.apache.commons.httpclient.protocol.SecureProtocolSocketFactory;

public class TLSSocketFactory {
	
	public SecureProtocolSocketFactory getSecureProtocolSocketFactory() {
		return new SSLProtocolSocketFactory();
	}

}
