package com.ftd.pg.client.common;

/**
 * @author nkatle
 *
 */
public class CreditCardValidationException extends Exception {
	
	public CreditCardValidationException() {
	}

	public CreditCardValidationException(String message) {
		super(message);
	}

	public CreditCardValidationException(Throwable cause) {
		super(cause);
	}

	public CreditCardValidationException(String message, Throwable cause) {
		super(message, cause);
	}

}
