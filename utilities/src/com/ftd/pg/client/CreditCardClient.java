package com.ftd.pg.client;


import java.util.logging.Logger;

import com.ftd.pg.client.creditcard.authorize.request.AuthorizeRequest;
import com.ftd.pg.client.creditcard.authorize.request.BillTo;
import com.ftd.pg.client.creditcard.authorize.request.CardinalCommerce;
import com.ftd.pg.client.creditcard.authorize.request.CreditCard;
import com.ftd.pg.client.creditcard.authorize.response.AuthorizeResponse;
import com.ftd.pg.client.creditcard.refund.request.RefundRequest;
import com.ftd.pg.client.creditcard.refund.response.RefundResponse;
import com.ftd.pg.client.creditcard.settlement.request.SettlementRequest;
import com.ftd.pg.client.creditcard.settlement.response.SettlementResponse;


//TODO Check for TLSv1.2 compatible
public class CreditCardClient {
	
	private static final Logger logger = Logger.getLogger(CreditCardClient.class.getName());

	private final String CC_WS_SOCKET_TIMEOUT;

	public CreditCardClient(String CC_WS_SOCKET_TIMEOUT) {
		this.CC_WS_SOCKET_TIMEOUT = CC_WS_SOCKET_TIMEOUT;
	}

	public AuthorizeResponse authorizeCC(AuthorizeRequest request, String authorizeCCWsUrl)
			throws Exception {
		
		if (authorizeCCWsUrl == null || authorizeCCWsUrl.isEmpty()) {
			logger.severe("Empty/null webservice url retrieved");
			throw new Exception("Empty/null webservice url retrieved");
		}
		
		String requestJsonString = PaymentGatewayUtils.convertToJsonString(request);
		return PaymentGatewayUtils.postPGAutorizeRequest(request.getMerchantReferenceId(), authorizeCCWsUrl, CC_WS_SOCKET_TIMEOUT, requestJsonString, AuthorizeResponse.class);
	}

	public RefundResponse refundCC(RefundRequest request, String refundWsUrl)
			throws Exception {
		
		if (refundWsUrl == null || refundWsUrl.isEmpty()) {
			logger.severe("Empty/null webservice url retrieved");
			throw new Exception("Empty/null webservice url retrieved");
		}
		String requestJsonString = PaymentGatewayUtils.convertToJsonString(request);
		return PaymentGatewayUtils.postPGRequest(request.getMerchantReferenceId(), refundWsUrl, CC_WS_SOCKET_TIMEOUT, requestJsonString, RefundResponse.class);
	}
	
	public SettlementResponse settleCC(SettlementRequest request, String settlementWsUrl) throws Exception {
		
		if (settlementWsUrl == null || settlementWsUrl.isEmpty()) {
			logger.severe("Empty/null webservice url retrieved");
			throw new Exception("Empty/null webservice url retrieved");
		}
		String requestJsonString = PaymentGatewayUtils.convertToJsonString(request);
		return PaymentGatewayUtils.postPGRequest(request.getMerchantReferenceId(), settlementWsUrl, CC_WS_SOCKET_TIMEOUT,
				requestJsonString, SettlementResponse.class);
	}
	
	
	
}
