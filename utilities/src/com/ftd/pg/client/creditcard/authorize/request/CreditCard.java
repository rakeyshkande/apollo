
package com.ftd.pg.client.creditcard.authorize.request;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;

@JsonPropertyOrder({
    "cardNumber",
    "cvvNumber",
    "expirationYear",
    "expirationMonth",
    "cardType",
    "cardinalCommerce"
})
public class CreditCard {

    /**
     * Credit Card number
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("cardNumber")
    private String cardNumber;
    /**
     * Credit Card Security Code(cvv)
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("cvvNumber")
    private String cvvNumber;
    /**
     * Credit Card expiration year YYYY
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("expirationYear")
    private String expirationYear;
    /**
     * Credit Card expiration month MM
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("expirationMonth")
    private String expirationMonth;
    /**
     * Credit Card Type. i.e Visa, MasterCard, Discover, Diners, AmericanExpress
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("cardType")
    private Object cardType;
    /**
     * Cardinal ecommerce Secure data
     * <p>
     * 
     * 
     */
    @JsonProperty("cardinalCommerce")
    private CardinalCommerce cardinalCommerce;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * Credit Card number
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("cardNumber")
    public String getCardNumber() {
        return cardNumber;
    }

    /**
     * Credit Card number
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("cardNumber")
    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    /**
     * Credit Card Security Code(cvv)
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("cvvNumber")
    public String getCvvNumber() {
        return cvvNumber;
    }

    /**
     * Credit Card Security Code(cvv)
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("cvvNumber")
    public void setCvvNumber(String cvvNumber) {
        this.cvvNumber = cvvNumber;
    }

    /**
     * Credit Card expiration year YYYY
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("expirationYear")
    public String getExpirationYear() {
        return expirationYear;
    }

    /**
     * Credit Card expiration year YYYY
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("expirationYear")
    public void setExpirationYear(String expirationYear) {
        this.expirationYear = expirationYear;
    }

    /**
     * Credit Card expiration month MM
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("expirationMonth")
    public String getExpirationMonth() {
        return expirationMonth;
    }

    /**
     * Credit Card expiration month MM
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("expirationMonth")
    public void setExpirationMonth(String expirationMonth) {
        this.expirationMonth = expirationMonth;
    }

    /**
     * Credit Card Type. i.e Visa, MasterCard, Discover, Diners, AmericanExpress
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("cardType")
    public Object getCardType() {
        return cardType;
    }

    /**
     * Credit Card Type. i.e Visa, MasterCard, Discover, Diners, AmericanExpress
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("cardType")
    public void setCardType(Object cardType) {
        this.cardType = cardType;
    }

    /**
     * Cardinal ecommerce Secure data
     * <p>
     * 
     * 
     */
    @JsonProperty("cardinalCommerce")
    public CardinalCommerce getCardinalCommerce() {
        return cardinalCommerce;
    }

    /**
     * Cardinal ecommerce Secure data
     * <p>
     * 
     * 
     */
    @JsonProperty("cardinalCommerce")
    public void setCardinalCommerce(CardinalCommerce cardinalCommerce) {
        this.cardinalCommerce = cardinalCommerce;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(cardNumber).append(cvvNumber).append(expirationYear).append(expirationMonth).append(cardType).append(cardinalCommerce).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof CreditCard) == false) {
            return false;
        }
        CreditCard rhs = ((CreditCard) other);
        return new EqualsBuilder().append(cardNumber, rhs.cardNumber).append(cvvNumber, rhs.cvvNumber).append(expirationYear, rhs.expirationYear).append(expirationMonth, rhs.expirationMonth).append(cardType, rhs.cardType).append(cardinalCommerce, rhs.cardinalCommerce).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
