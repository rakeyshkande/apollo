
package com.ftd.pg.client.creditcard.authorize.response;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;


/**
 * Credit Card response
 * <p>
 * 
 * 
 */
@JsonPropertyOrder({
    "amount",
    "status",
    "message",
    "processAccount",
    "requestId",
    "authCode",
    "authorizationTransactionId",
    "avsResultCode",
    "cardSpcGrp",
    "ccvResultCode",
    "currency",
    "posCondCode",
    "refNum",
    "reasonCode",
    "stan",
    "termCatCode",
    "transmissionDateTime"
})
public class CreditCardResponse {

    /**
     * Authorize Amount
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("amount")
    private String amount;
    /**
     * Authorize status
     * <p>
     * 
     * 
     */
    @JsonProperty("status")
    private String status;
    /**
     * Response message from gateway
     * <p>
     * 
     * 
     */
    @JsonProperty("message")
    private String message;
    /**
     * Process Account
     * <p>
     * 
     * 
     */
    @JsonProperty("processAccount")
    private String processAccount;
    /**
     * Request Id for AMEX
     * <p>
     * 
     * 
     */
    @JsonProperty("requestId")
    private String requestId;
    /**
     * AuthCode (Map to AuthCode for JCCAS and AMEX)
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("authCode")
    private String authCode;
    /**
     * Authorization ID (Map to AuthId for JCCAS and AMEX)
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("authorizationTransactionId")
    private String authorizationTransactionId;
    /**
     * AVS response code
     * <p>
     * 
     * 
     */
    @JsonProperty("avsResultCode")
    private String avsResultCode;
    /**
     * Card Specific Group details
     * <p>
     * 
     * 
     */
    @JsonProperty("cardSpcGrp")
    private CardSpcGrp cardSpcGrp;
    /**
     * CCV response code
     * <p>
     * 
     * 
     */
    @JsonProperty("ccvResultCode")
    private String ccvResultCode;
    /**
     * Currency Code
     * <p>
     * 
     * 
     */
    @JsonProperty("currency")
    private String currency;
    /**
     * POS Cond Code
     * <p>
     * 
     * 
     */
    @JsonProperty("posCondCode")
    private String posCondCode;
    /**
     * Payment reference number
     * <p>
     * 
     * 
     */
    @JsonProperty("refNum")
    private String refNum;
    /**
     * Respone Code (map to reason code for PS)
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("reasonCode")
    private String reasonCode;
    /**
     * Processor unique reference number
     * <p>
     * 
     * 
     */
    @JsonProperty("stan")
    private String stan;
    /**
     * Terminal Cat Code
     * <p>
     * 
     * 
     */
    @JsonProperty("termCatCode")
    private String termCatCode;
    /**
     * Transmission DateTime
     * <p>
     * 
     * 
     */
    @JsonProperty("transmissionDateTime")
    private String transmissionDateTime;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * Authorize Amount
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("amount")
    public String getAmount() {
        return amount;
    }

    /**
     * Authorize Amount
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("amount")
    public void setAmount(String amount) {
        this.amount = amount;
    }

    /**
     * Authorize status
     * <p>
     * 
     * 
     */
    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    /**
     * Authorize status
     * <p>
     * 
     * 
     */
    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * Response message from gateway
     * <p>
     * 
     * 
     */
    @JsonProperty("message")
    public String getMessage() {
        return message;
    }

    /**
     * Response message from gateway
     * <p>
     * 
     * 
     */
    @JsonProperty("message")
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * Process Account
     * <p>
     * 
     * 
     */
    @JsonProperty("processAccount")
    public String getProcessAccount() {
        return processAccount;
    }

    /**
     * Process Account
     * <p>
     * 
     * 
     */
    @JsonProperty("processAccount")
    public void setProcessAccount(String processAccount) {
        this.processAccount = processAccount;
    }

    /**
     * Request Id for AMEX
     * <p>
     * 
     * 
     */
    @JsonProperty("requestId")
    public String getRequestId() {
        return requestId;
    }

    /**
     * Request Id for AMEX
     * <p>
     * 
     * 
     */
    @JsonProperty("requestId")
    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    /**
     * Authorization ID (Map to AuthId for JCCAS)
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("authorizationTransactionId")
    public String getAuthorizationTransactionId() {
        return authorizationTransactionId;
    }

    /**
     * Authorization ID (Map to AuthId for JCCAS)
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("authCode")
    public void setAuthCode(String authCode) {
        this.authCode = authCode;
    }
    /**
     * AuthCode (Map to AuthCode for JCCAS and AMEX)
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("authCode")
    public String getAuthCode() {
        return authCode;
    }

    /**
     * AuthCode (Map to AuthCode for JCCAS and AMEX)
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("authorizationTransactionId")
    public void setAuthorizationTransactionId(String authorizationTransactionId) {
        this.authorizationTransactionId = authorizationTransactionId;
    }

    /**
     * AVS response code
     * <p>
     * 
     * 
     */
    @JsonProperty("avsResultCode")
    public String getAvsResultCode() {
        return avsResultCode;
    }

    /**
     * AVS response code
     * <p>
     * 
     * 
     */
    @JsonProperty("avsResultCode")
    public void setAvsResultCode(String avsResultCode) {
        this.avsResultCode = avsResultCode;
    }

    /**
     * Card Specific Group details
     * <p>
     * 
     * 
     */
    @JsonProperty("cardSpcGrp")
    public CardSpcGrp getCardSpcGrp() {
        return cardSpcGrp;
    }

    /**
     * Card Specific Group details
     * <p>
     * 
     * 
     */
    @JsonProperty("cardSpcGrp")
    public void setCardSpcGrp(CardSpcGrp cardSpcGrp) {
        this.cardSpcGrp = cardSpcGrp;
    }

    /**
     * CCV response code
     * <p>
     * 
     * 
     */
    @JsonProperty("ccvResultCode")
    public String getCcvResultCode() {
        return ccvResultCode;
    }

    /**
     * CCV response code
     * <p>
     * 
     * 
     */
    @JsonProperty("ccvResultCode")
    public void setCcvResultCode(String ccvResultCode) {
        this.ccvResultCode = ccvResultCode;
    }

    /**
     * Currency Code
     * <p>
     * 
     * 
     */
    @JsonProperty("currency")
    public String getCurrency() {
        return currency;
    }

    /**
     * Currency Code
     * <p>
     * 
     * 
     */
    @JsonProperty("currency")
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    /**
     * POS Cond Code
     * <p>
     * 
     * 
     */
    @JsonProperty("posCondCode")
    public String getPosCondCode() {
        return posCondCode;
    }

    /**
     * POS Cond Code
     * <p>
     * 
     * 
     */
    @JsonProperty("posCondCode")
    public void setPosCondCode(String posCondCode) {
        this.posCondCode = posCondCode;
    }

    /**
     * Payment reference number
     * <p>
     * 
     * 
     */
    @JsonProperty("refNum")
    public String getRefNum() {
        return refNum;
    }

    /**
     * Payment reference number
     * <p>
     * 
     * 
     */
    @JsonProperty("refNum")
    public void setRefNum(String refNum) {
        this.refNum = refNum;
    }

    /**
     * Respone Code (map to reason code for PS)
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("reasonCode")
    public String getReasonCode() {
        return reasonCode;
    }

    /**
     * Respone Code (map to reason code for PS)
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("reasonCode")
    public void setReasonCode(String reasonCode) {
        this.reasonCode = reasonCode;
    }

    /**
     * Processor unique reference number
     * <p>
     * 
     * 
     */
    @JsonProperty("stan")
    public String getStan() {
        return stan;
    }

    /**
     * Processor unique reference number
     * <p>
     * 
     * 
     */
    @JsonProperty("stan")
    public void setStan(String stan) {
        this.stan = stan;
    }

    /**
     * Terminal Cat Code
     * <p>
     * 
     * 
     */
    @JsonProperty("termCatCode")
    public String getTermCatCode() {
        return termCatCode;
    }

    /**
     * Terminal Cat Code
     * <p>
     * 
     * 
     */
    @JsonProperty("termCatCode")
    public void setTermCatCode(String termCatCode) {
        this.termCatCode = termCatCode;
    }

    /**
     * Transmission DateTime
     * <p>
     * 
     * 
     */
    @JsonProperty("transmissionDateTime")
    public String getTransmissionDateTime() {
        return transmissionDateTime;
    }

    /**
     * Transmission DateTime
     * <p>
     * 
     * 
     */
    @JsonProperty("transmissionDateTime")
    public void setTransmissionDateTime(String transmissionDateTime) {
        this.transmissionDateTime = transmissionDateTime;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(amount).append(status).append(message).append(processAccount).append(requestId).append(authorizationTransactionId).append(avsResultCode).append(cardSpcGrp).append(ccvResultCode).append(currency).append(posCondCode).append(refNum).append(reasonCode).append(stan).append(termCatCode).append(transmissionDateTime).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof CreditCardResponse) == false) {
            return false;
        }
        CreditCardResponse rhs = ((CreditCardResponse) other);
        return new EqualsBuilder().append(amount, rhs.amount).append(status, rhs.status).append(message, rhs.message).append(processAccount, rhs.processAccount).append(requestId, rhs.requestId).append(authorizationTransactionId, rhs.authorizationTransactionId).append(avsResultCode, rhs.avsResultCode).append(cardSpcGrp, rhs.cardSpcGrp).append(ccvResultCode, rhs.ccvResultCode).append(currency, rhs.currency).append(posCondCode, rhs.posCondCode).append(refNum, rhs.refNum).append(reasonCode, rhs.reasonCode).append(stan, rhs.stan).append(termCatCode, rhs.termCatCode).append(transmissionDateTime, rhs.transmissionDateTime).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
