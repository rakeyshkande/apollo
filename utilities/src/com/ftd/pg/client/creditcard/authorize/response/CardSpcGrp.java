
package com.ftd.pg.client.creditcard.authorize.response;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;


/**
 * Card Specific Group details
 * <p>
 * 
 * 
 */
@JsonPropertyOrder({
    "cardSpcDtl",
    "type"
})
public class CardSpcGrp {

    @JsonProperty("cardSpcDtl")
    private List<CardSpcDtl> cardSpcDtl = new ArrayList<CardSpcDtl>();
    @JsonProperty("type")
    private String type;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("cardSpcDtl")
    public List<CardSpcDtl> getCardSpcDtl() {
        return cardSpcDtl;
    }

    @JsonProperty("cardSpcDtl")
    public void setCardSpcDtl(List<CardSpcDtl> cardSpcDtl) {
        this.cardSpcDtl = cardSpcDtl;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(cardSpcDtl).append(type).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof CardSpcGrp) == false) {
            return false;
        }
        CardSpcGrp rhs = ((CardSpcGrp) other);
        return new EqualsBuilder().append(cardSpcDtl, rhs.cardSpcDtl).append(type, rhs.type).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
