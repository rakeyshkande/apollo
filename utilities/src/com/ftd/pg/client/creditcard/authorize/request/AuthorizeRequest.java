
package com.ftd.pg.client.creditcard.authorize.request;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;


/**
 * Payment-Gateway Authorize Request
 * <p>
 * 
 * 
 */
@JsonPropertyOrder({
	"paymentType",
    "merchantReferenceId",
    "currency",
    "isRetry",
    "amount",
    "creditCard",
    "billTo"
})
public class AuthorizeRequest {

    /**
     * Card type used by the customer
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("paymentType")
    private String paymentType;
    
    /**
     * Client reference id like OrderId, paymentId or unique Id
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("merchantReferenceId")
    private String merchantReferenceId;
    /**
     * Currency used for paymentservice - CURRETNLY SUPPOSRTS USD ONLY - Required for AMEX
     * <p>
     * 
     * 
     */
    @JsonProperty("currency")
    private Object currency;
    /**
     * Retry same authorization - default to false
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("isRetry")
    private boolean isRetry;
    /**
     * amount - must not have any decimals; if transaction is 50.98, value should be 5098
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("amount")
    private String amount;
    @JsonProperty("creditCard")
    private CreditCard creditCard;
    @JsonProperty("billTo")
    private BillTo billTo;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();


    @JsonProperty("paymentType")
    public String getPaymentType() {
        return paymentType;
    }

    @JsonProperty("paymentType")
    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }


    /**
     * Client reference id like OrderId, paymentId or unique Id
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("merchantReferenceId")
    public String getMerchantReferenceId() {
        return merchantReferenceId;
    }

    /**
     * Client reference id like OrderId, paymentId or unique Id
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("merchantReferenceId")
    public void setMerchantReferenceId(String merchantReferenceId) {
        this.merchantReferenceId = merchantReferenceId;
    }

    /**
     * Currency used for paymentservice - CURRETNLY SUPPOSRTS USD ONLY - Required for AMEX
     * <p>
     * 
     * 
     */
    @JsonProperty("currency")
    public Object getCurrency() {
        return currency;
    }

    /**
     * Currency used for paymentservice - CURRETNLY SUPPOSRTS USD ONLY - Required for AMEX
     * <p>
     * 
     * 
     */
    @JsonProperty("currency")
    public void setCurrency(Object currency) {
        this.currency = currency;
    }

    /**
     * Retry same authorization - default to false
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("isRetry")
    public boolean isRetry() {
        return isRetry;
    }

    /**
     * Retry same authorization - default to false
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("isRetry")
    public void setRetry(boolean isRetry) {
        this.isRetry = isRetry;
    }

    /**
     * amount - must not have any decimals; if transaction is 50.98, value should be 5098
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("amount")
    public String getAmount() {
        return amount;
    }

    /**
     * amount - must not have any decimals; if transaction is 50.98, value should be 5098
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("amount")
    public void setAmount(String amount) {
        this.amount = amount;
    }

    @JsonProperty("creditCard")
    public CreditCard getCreditCard() {
        return creditCard;
    }

    @JsonProperty("creditCard")
    public void setCreditCard(CreditCard creditCard) {
        this.creditCard = creditCard;
    }

    @JsonProperty("billTo")
    public BillTo getBillTo() {
        return billTo;
    }

    @JsonProperty("billTo")
    public void setBillTo(BillTo billTo) {
        this.billTo = billTo;
    }

    @Override
    public String toString() {
    	String tempCCNumber = this.getCreditCard().getCardNumber();
    	String returnStr = "";
    	if(StringUtils.isNotBlank(tempCCNumber) && tempCCNumber.length() > 5) {
    		this.getCreditCard().setCardNumber("************" + tempCCNumber.substring(tempCCNumber.length() - 4));
    		returnStr = ToStringBuilder.reflectionToString(this);
            this.getCreditCard().setCardNumber(tempCCNumber);
    	} else {
    		returnStr = ToStringBuilder.reflectionToString(this);
    	}
        return returnStr;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(merchantReferenceId).append(currency).append(isRetry).append(amount).append(creditCard).append(billTo).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof AuthorizeRequest) == false) {
            return false;
        }
        AuthorizeRequest rhs = ((AuthorizeRequest) other);
        return new EqualsBuilder().append(merchantReferenceId, rhs.merchantReferenceId).append(currency, rhs.currency).append(isRetry, rhs.isRetry).append(amount, rhs.amount).append(creditCard, rhs.creditCard).append(billTo, rhs.billTo).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
