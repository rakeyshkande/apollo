
package com.ftd.pg.client.creditcard.authorize.request;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;


/**
 * Cardinal ecommerce Secure data
 * <p>
 * 
 * 
 */
@JsonPropertyOrder({
    "cavv",
    "eci",
    "ucafCollectInd",
    "xid"
})
public class CardinalCommerce {

    /**
     * Cardholder Authentication Verification Value
     * <p>
     * 
     * 
     */
    @JsonProperty("cavv")
    private String cavv;
    /**
     * E-commerce Indicator
     * <p>
     * 
     * 
     */
    @JsonProperty("eci")
    private String eci;
    /**
     * Universal Cardholder Authentication Field
     * <p>
     * 
     * 
     */
    @JsonProperty("ucafCollectInd")
    private String ucafCollectInd;
    /**
     * Transaction Id
     * <p>
     * 
     * 
     */
    @JsonProperty("xid")
    private String xid;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * Cardholder Authentication Verification Value
     * <p>
     * 
     * 
     */
    @JsonProperty("cavv")
    public String getCavv() {
        return cavv;
    }

    /**
     * Cardholder Authentication Verification Value
     * <p>
     * 
     * 
     */
    @JsonProperty("cavv")
    public void setCavv(String cavv) {
        this.cavv = cavv;
    }

    /**
     * E-commerce Indicator
     * <p>
     * 
     * 
     */
    @JsonProperty("eci")
    public String getEci() {
        return eci;
    }

    /**
     * E-commerce Indicator
     * <p>
     * 
     * 
     */
    @JsonProperty("eci")
    public void setEci(String eci) {
        this.eci = eci;
    }

    /**
     * Universal Cardholder Authentication Field
     * <p>
     * 
     * 
     */
    @JsonProperty("ucafCollectInd")
    public String getUcafCollectInd() {
        return ucafCollectInd;
    }

    /**
     * Universal Cardholder Authentication Field
     * <p>
     * 
     * 
     */
    @JsonProperty("ucafCollectInd")
    public void setUcafCollectInd(String ucafCollectInd) {
        this.ucafCollectInd = ucafCollectInd;
    }

    /**
     * Transaction Id
     * <p>
     * 
     * 
     */
    @JsonProperty("xid")
    public String getXid() {
        return xid;
    }

    /**
     * Transaction Id
     * <p>
     * 
     * 
     */
    @JsonProperty("xid")
    public void setXid(String xid) {
        this.xid = xid;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(cavv).append(eci).append(ucafCollectInd).append(xid).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof CardinalCommerce) == false) {
            return false;
        }
        CardinalCommerce rhs = ((CardinalCommerce) other);
        return new EqualsBuilder().append(cavv, rhs.cavv).append(eci, rhs.eci).append(ucafCollectInd, rhs.ucafCollectInd).append(xid, rhs.xid).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
