
package com.ftd.pg.client.creditcard.authorize.response;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;

import com.ftd.pg.client.creditcard.settlement.response.Errors;


/**
 * Credit Card authorize response
 * <p>
 * 
 * 
 */
@JsonPropertyOrder({
    "paymentType",
    "siteId",
    "merchantReferenceId",
    "channel",
    "company",
    "route",
    "creditCardResponse",
    "status",
    "errors"
})
public class AuthorizeResponse {

    /**
     * Payment Type - CreditCard, GiftCode, PayPal
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("paymentType")
    private String paymentType;
    /**
     * SiteId
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("siteId")
    private String siteId;
    /**
     * Client reference id like OrderId, paymentId or unique Id
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("merchantReferenceId")
    private String merchantReferenceId;
    /**
     * Channel WEB,APOLLO
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("channel")
    private String channel;
    /**
     * Company FTD, FTDCA
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("company")
    private String company;
    /**
     * Payment route PG-JCCAS,PG-PS
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("route")
    private String route;
    /**
     * Credit Card response
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("creditCardResponse")
    private CreditCardResponse creditCardResponse;
    
    @JsonProperty("errors")
    private List<Errors> errorMessages;
    
    @JsonProperty("status")
    private String status;
    
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * Payment Type - CreditCard, GiftCode, PayPal
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("paymentType")
    public String getPaymentType() {
        return paymentType;
    }

    /**
     * Payment Type - CreditCard, GiftCode, PayPal
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("paymentType")
    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    /**
     * SiteId
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("siteId")
    public String getSiteId() {
        return siteId;
    }

    /**
     * SiteId
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("siteId")
    public void setSiteId(String siteId) {
        this.siteId = siteId;
    }

    /**
     * Client reference id like OrderId, paymentId or unique Id
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("merchantReferenceId")
    public String getMerchantReferenceId() {
        return merchantReferenceId;
    }

    /**
     * Client reference id like OrderId, paymentId or unique Id
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("merchantReferenceId")
    public void setMerchantReferenceId(String merchantReferenceId) {
        this.merchantReferenceId = merchantReferenceId;
    }

    /**
     * Channel WEB,APOLLO
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("channel")
    public String getChannel() {
        return channel;
    }

    /**
     * Channel WEB,APOLLO
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("channel")
    public void setChannel(String channel) {
        this.channel = channel;
    }

    /**
     * Company FTD, FTDCA
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("company")
    public String getCompany() {
        return company;
    }

    /**
     * Company FTD, FTDCA
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("company")
    public void setCompany(String company) {
        this.company = company;
    }

    /**
     * Payment route PG-JCCAS,PG-PS
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("route")
    public String getRoute() {
        return route;
    }

    /**
     * Payment route PG-JCCAS,PG-PS
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("route")
    public void setRoute(String route) {
        this.route = route;
    }

    /**
     * Credit Card response
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("creditCardResponse")
    public CreditCardResponse getCreditCardResponse() {
        return creditCardResponse;
    }

    /**
     * Credit Card response
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("creditCardResponse")
    public void setCreditCardResponse(CreditCardResponse creditCardResponse) {
        this.creditCardResponse = creditCardResponse;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

	public List<Errors> getErrorMessages() {
		return errorMessages;
	}

	public void setErrorMessages(List<Errors> errorMessages) {
		this.errorMessages = errorMessages;
	}

	public String getStatus() {
		return status;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((additionalProperties == null) ? 0 : additionalProperties
						.hashCode());
		result = prime * result + ((channel == null) ? 0 : channel.hashCode());
		result = prime * result + ((company == null) ? 0 : company.hashCode());
		result = prime
				* result
				+ ((creditCardResponse == null) ? 0 : creditCardResponse
						.hashCode());
		result = prime * result
				+ ((errorMessages == null) ? 0 : errorMessages.hashCode());
		result = prime
				* result
				+ ((merchantReferenceId == null) ? 0 : merchantReferenceId
						.hashCode());
		result = prime * result
				+ ((paymentType == null) ? 0 : paymentType.hashCode());
		result = prime * result + ((route == null) ? 0 : route.hashCode());
		result = prime * result + ((siteId == null) ? 0 : siteId.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AuthorizeResponse other = (AuthorizeResponse) obj;
		if (additionalProperties == null) {
			if (other.additionalProperties != null)
				return false;
		} else if (!additionalProperties.equals(other.additionalProperties))
			return false;
		if (channel == null) {
			if (other.channel != null)
				return false;
		} else if (!channel.equals(other.channel))
			return false;
		if (company == null) {
			if (other.company != null)
				return false;
		} else if (!company.equals(other.company))
			return false;
		if (creditCardResponse == null) {
			if (other.creditCardResponse != null)
				return false;
		} else if (!creditCardResponse.equals(other.creditCardResponse))
			return false;
		if (errorMessages == null) {
			if (other.errorMessages != null)
				return false;
		} else if (!errorMessages.equals(other.errorMessages))
			return false;
		if (merchantReferenceId == null) {
			if (other.merchantReferenceId != null)
				return false;
		} else if (!merchantReferenceId.equals(other.merchantReferenceId))
			return false;
		if (paymentType == null) {
			if (other.paymentType != null)
				return false;
		} else if (!paymentType.equals(other.paymentType))
			return false;
		if (route == null) {
			if (other.route != null)
				return false;
		} else if (!route.equals(other.route))
			return false;
		if (siteId == null) {
			if (other.siteId != null)
				return false;
		} else if (!siteId.equals(other.siteId))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		return true;
	}

	public void setStatus(String status) {
		this.status = status;
	}

    

}
