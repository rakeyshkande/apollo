
package com.ftd.pg.client.creditcard.authorize.request;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;

@JsonPropertyOrder({
    "firstName",
    "lastName",
    "middleName",
    "address1",
    "address2",
    "city",
    "state",
    "zipCode",
    "country",
    "phone",
    "email",
    "company"
})
public class BillTo {

    /**
     * Card holder first name - Required for AMEX
     * <p>
     * 
     * 
     */
    @JsonProperty("firstName")
    private String firstName;
    /**
     * Card holder first name - Required for AMEX
     * <p>
     * 
     * 
     */
    @JsonProperty("lastName")
    private String lastName;
    /**
     * Card holder first name - Optional for AMEX
     * <p>
     * 
     * 
     */
    @JsonProperty("middleName")
    private String middleName;
    /**
     * Card holder billing address1
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("address1")
    private String address1;
    /**
     * Card holder billing address2
     * <p>
     * 
     * 
     */
    @JsonProperty("address2")
    private String address2;
    /**
     * Card holder billing city - Required for AMEX
     * <p>
     * 
     * 
     */
    @JsonProperty("city")
    private String city;
    /**
     * Card holder billing state - Required for AMEX
     * <p>
     * 
     * 
     */
    @JsonProperty("state")
    private String state;
    /**
     * Card holder billing zipCode
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("zipCode")
    private String zipCode;
    /**
     * Card holder billing country - Required for AMEX
     * <p>
     * 
     * 
     */
    @JsonProperty("country")
    private String country;
    /**
     * Card holder phone
     * <p>
     * 
     * 
     */
    @JsonProperty("phone")
    private String phone;
    /**
     * Card holder email address - Required for AMEX
     * <p>
     * 
     * 
     */
    @JsonProperty("email")
    private String email;
    /**
     * Corporate account - company
     * <p>
     * 
     * 
     */
    @JsonProperty("company")
    private String company;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * Card holder first name - Required for AMEX
     * <p>
     * 
     * 
     */
    @JsonProperty("firstName")
    public String getFirstName() {
        return firstName;
    }

    /**
     * Card holder first name - Required for AMEX
     * <p>
     * 
     * 
     */
    @JsonProperty("firstName")
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Card holder first name - Required for AMEX
     * <p>
     * 
     * 
     */
    @JsonProperty("lastName")
    public String getLastName() {
        return lastName;
    }

    /**
     * Card holder first name - Required for AMEX
     * <p>
     * 
     * 
     */
    @JsonProperty("lastName")
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Card holder first name - Optional for AMEX
     * <p>
     * 
     * 
     */
    @JsonProperty("middleName")
    public String getMiddleName() {
        return middleName;
    }

    /**
     * Card holder first name - Optional for AMEX
     * <p>
     * 
     * 
     */
    @JsonProperty("middleName")
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    /**
     * Card holder billing address1
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("address1")
    public String getAddress1() {
        return address1;
    }

    /**
     * Card holder billing address1
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("address1")
    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    /**
     * Card holder billing address2
     * <p>
     * 
     * 
     */
    @JsonProperty("address2")
    public String getAddress2() {
        return address2;
    }

    /**
     * Card holder billing address2
     * <p>
     * 
     * 
     */
    @JsonProperty("address2")
    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    /**
     * Card holder billing city - Required for AMEX
     * <p>
     * 
     * 
     */
    @JsonProperty("city")
    public String getCity() {
        return city;
    }

    /**
     * Card holder billing city - Required for AMEX
     * <p>
     * 
     * 
     */
    @JsonProperty("city")
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * Card holder billing state - Required for AMEX
     * <p>
     * 
     * 
     */
    @JsonProperty("state")
    public String getState() {
        return state;
    }

    /**
     * Card holder billing state - Required for AMEX
     * <p>
     * 
     * 
     */
    @JsonProperty("state")
    public void setState(String state) {
        this.state = state;
    }

    /**
     * Card holder billing zipCode
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("zipCode")
    public String getZipCode() {
        return zipCode;
    }

    /**
     * Card holder billing zipCode
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("zipCode")
    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    /**
     * Card holder billing country - Required for AMEX
     * <p>
     * 
     * 
     */
    @JsonProperty("country")
    public String getCountry() {
        return country;
    }

    /**
     * Card holder billing country - Required for AMEX
     * <p>
     * 
     * 
     */
    @JsonProperty("country")
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * Card holder phone
     * <p>
     * 
     * 
     */
    @JsonProperty("phone")
    public String getPhone() {
        return phone;
    }

    /**
     * Card holder phone
     * <p>
     * 
     * 
     */
    @JsonProperty("phone")
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * Card holder email address - Required for AMEX
     * <p>
     * 
     * 
     */
    @JsonProperty("email")
    public String getEmail() {
        return email;
    }

    /**
     * Card holder email address - Required for AMEX
     * <p>
     * 
     * 
     */
    @JsonProperty("email")
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Corporate account - company
     * <p>
     * 
     * 
     */
    @JsonProperty("company")
    public String getCompany() {
        return company;
    }

    /**
     * Corporate account - company
     * <p>
     * 
     * 
     */
    @JsonProperty("company")
    public void setCompany(String company) {
        this.company = company;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(firstName).append(lastName).append(middleName).append(address1).append(address2).append(city).append(state).append(zipCode).append(country).append(phone).append(email).append(company).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof BillTo) == false) {
            return false;
        }
        BillTo rhs = ((BillTo) other);
        return new EqualsBuilder().append(firstName, rhs.firstName).append(lastName, rhs.lastName).append(middleName, rhs.middleName).append(address1, rhs.address1).append(address2, rhs.address2).append(city, rhs.city).append(state, rhs.state).append(zipCode, rhs.zipCode).append(country, rhs.country).append(phone, rhs.phone).append(email, rhs.email).append(company, rhs.company).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
