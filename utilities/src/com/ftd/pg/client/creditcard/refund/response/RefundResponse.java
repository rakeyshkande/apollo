
package com.ftd.pg.client.creditcard.refund.response;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;

@JsonPropertyOrder({
    "refundTransactionId",
    "refundDate",
    "status",
    "reasonCode",
    "message",
    "errorMessages",
    "requestId",
    "merchantReferenceId",
    "processorAccount"
})
public class RefundResponse {

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("refundTransactionId")
    private String refundTransactionId;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("status")
    private String status;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("reasonCode")
    private String reasonCode;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("message")
    private String message;
    @JsonProperty("errorMessages")
    private Object errorMessages;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("requestId")
    private String requestId;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("merchantReferenceId")
    private String merchantReferenceId;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("refundTransactionId")
    public String getRefundTransactionId() {
        return refundTransactionId;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("refundTransactionId")
    public void setRefundTransactionId(String refundTransactionId) {
        this.refundTransactionId = refundTransactionId;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("reasonCode")
    public String getReasonCode() {
        return reasonCode;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("reasonCode")
    public void setReasonCode(String reasonCode) {
        this.reasonCode = reasonCode;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("message")
    public String getMessage() {
        return message;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("message")
    public void setMessage(String message) {
        this.message = message;
    }

    @JsonProperty("errorMessages")
    public Object getErrorMessages() {
        return errorMessages;
    }

    @JsonProperty("errorMessages")
    public void setErrorMessages(Object errorMessages) {
        this.errorMessages = errorMessages;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("requestId")
    public String getRequestId() {
        return requestId;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("requestId")
    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("merchantReferenceId")
    public String getMerchantReferenceId() {
        return merchantReferenceId;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("merchantReferenceId")
    public void setMerchantReferenceId(String merchantReferenceId) {
        this.merchantReferenceId = merchantReferenceId;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(refundTransactionId).append(status).append(reasonCode).append(message).append(errorMessages).append(requestId).append(merchantReferenceId).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof RefundResponse) == false) {
            return false;
        }
        RefundResponse rhs = ((RefundResponse) other);
        return new EqualsBuilder().append(refundTransactionId, rhs.refundTransactionId).append(status, rhs.status).append(reasonCode, rhs.reasonCode).append(message, rhs.message).append(errorMessages, rhs.errorMessages).append(requestId, rhs.requestId).append(merchantReferenceId, rhs.merchantReferenceId).append(additionalProperties, rhs.additionalProperties).isEquals();
    }
    
    @JsonProperty("refundDate")
    private Object refundDate;
    
    @JsonProperty("refundDate")
	public Object getRefundDate() {
		return refundDate;
	}
    
    @JsonProperty("refundDate")
	public void setRefundDate(Object refundDate) {
		this.refundDate = refundDate;
	}
    
    
    
    
    
}
