
package com.ftd.pg.client.creditcard.refund.request;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;


/**
 * Payment-Gateway refund Request
 * <p>
 * 
 * 
 */
@JsonPropertyOrder({
    "amount",
    "isRetry",
    "merchantReferenceId",
    "requestToken",
    "settlementTransactionId",
    "paymentType"
})
public class RefundRequest {

    /**
     * Amount - must not have any decimals; if transaction is 50.98, value should be 5098.
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("amount")
    private Object amount;
    /**
     * Client reference id like OrderId, paymentId or unique Id.
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("merchantReferenceId")
    private String merchantReferenceId;
    /**
     * Authorization settlement Id.
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("settlementTransactionId")
    private Object settlementTransactionId;
    /**
     * Retry same refund - default to false
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("isRetry")
    private boolean isRetry;
    
    @JsonProperty("paymentType")
    private String paymentType;
    
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    
    
    /**
     * responseToken.
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("requestToken")
    private String requestToken;
    
    /**
     * Amount - must not have any decimals; if transaction is 50.98, value should be 5098.
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("amount")
    public Object getAmount() {
        return amount;
    }

    /**
     * Amount - must not have any decimals; if transaction is 50.98, value should be 5098.
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("amount")
    public void setAmount(Object amount) {
        this.amount = amount;
    }

    /**
     * Client reference id like OrderId, paymentId or unique Id.
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("merchantReferenceId")
    public String getMerchantReferenceId() {
        return merchantReferenceId;
    }

    /**
     * Client reference id like OrderId, paymentId or unique Id.
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("merchantReferenceId")
    public void setMerchantReferenceId(String merchantReferenceId) {
        this.merchantReferenceId = merchantReferenceId;
    }

    /**
     * Authorization settlement Id.
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("settlementTransactionId")
    public Object getSettlementTransactionId() {
        return settlementTransactionId;
    }

    /**
     * Authorization settlement Id.
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("settlementTransactionId")
    public void setSettlementTransactionId(Object settlementTransactionId) {
        this.settlementTransactionId = settlementTransactionId;
    }

    @JsonProperty("isRetry")
    public boolean isRetry() {
		return isRetry;
	}
    
    @JsonProperty("isRetry")
	public void setRetry(boolean isRetry) {
		this.isRetry = isRetry;
	}
    
    @JsonProperty("paymentType")
  	public String getPaymentType() {
  		return paymentType;
  	}
      
    @JsonProperty("paymentType")
  	public void setPaymentType(String paymentType) {
  		this.paymentType = paymentType;
  	}
    
	@Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
    
    @JsonProperty("requestToken")
	public String getRequestToken() {
		return requestToken;
	}
	
	@JsonProperty("requestToken")
	public void setRequestToken(String requestToken) {
		this.requestToken = requestToken;
	}

   
    
    
}
