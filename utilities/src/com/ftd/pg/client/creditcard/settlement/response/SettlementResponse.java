package com.ftd.pg.client.creditcard.settlement.response;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;

@JsonPropertyOrder({
    "settlementTransactionId",
    "settlementDate",
    "status",
    "reasonCode",
    "message",
    "errors",
    "requestId",
    "merchantReferenceId",
    "requestToken",
    "processorAccount"
})
public class SettlementResponse {

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("settlementTransactionId")
    private String settlementTransactionId;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("settlementDate")
    private Object settlementDate;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("status")
    private String status;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("reasonCode")
    private String reasonCode;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("message")
    private String message;
    @JsonProperty("errors")
    private List<Errors> errorMessages;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("requestId")
    private String requestId;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("merchantReferenceId")
    private String merchantReferenceId;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("requestToken")
    private String requestToken;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("processAccount")
    private String processAccount;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("settlementTransactionId")
    public String getSettlementTransactionId() {
        return settlementTransactionId;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("settlementTransactionId")
    public void setSettlementTransactionId(String settlementTransactionId) {
        this.settlementTransactionId = settlementTransactionId;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("settlementDate")
    public Object getSettlementDate() {
        return settlementDate;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("settlementDate")
    public void setSettlementDate(Object settlementDate) {
        this.settlementDate = settlementDate;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("reasonCode")
    public String getReasonCode() {
        return reasonCode;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("reasonCode")
    public void setReasonCode(String reasonCode) {
        this.reasonCode = reasonCode;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("message")
    public String getMessage() {
        return message;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("message")
    public void setMessage(String message) {
        this.message = message;
    }

    @JsonProperty("errorMessages")
    public List<Errors> getErrorMessages() {
        return errorMessages;
    }

    @JsonProperty("errorMessages")
    public void setErrorMessages(List<Errors> errorMessages) {
        this.errorMessages = errorMessages;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("requestId")
    public String getRequestId() {
        return requestId;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("requestId")
    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("merchantReferenceId")
    public String getMerchantReferenceId() {
        return merchantReferenceId;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("merchantReferenceId")
    public void setMerchantReferenceId(String merchantReferenceId) {
        this.merchantReferenceId = merchantReferenceId;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("requestToken")
    public String getRequestToken() {
        return requestToken;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("requestToken")
    public void setRequestToken(String requestToken) {
        this.requestToken = requestToken;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
    
    @JsonProperty("processAccount")
	public String getProcessAccount() {
		return processAccount;
	}

    @JsonProperty("processAccount")
	public void setProcessAccount(String processAccount) {
		this.processAccount = processAccount;
	}

    

}
