
package com.ftd.pg.client.creditcard.settlement.request;

import java.util.HashMap;

import java.util.Map;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;


/**
 * Payment-Gateway settlement Request
 * <p>
 * 
 * 
 */
@JsonPropertyOrder({
    "amount",
    "authorizationTransactionId",
    "isRetry",
    "merchantReferenceId",
    "paymentType"
})
public class SettlementRequest {

    /**
     * Amount - must not have any decimals; if transaction is 50.98, value should be 5098.
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("amount")
    private String amount;
    /**
     * Client reference id like OrderId, paymentId or unique Id.
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("merchantReferenceId")
    private String merchantReferenceId;
    /**
     * Authorization transaction Id.
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("authorizationTransactionId")
    private Object authorizationTransactionId;
    /**
     * Retry same authorization - default to false
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("isRetry")
    private boolean isRetry;
    
    @JsonProperty("paymentType")
    private String paymentType;
    
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * Amount - must not have any decimals; if transaction is 50.98, value should be 5098.
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("amount")
    public String getAmount() {
        return amount;
    }

    /**
     * Amount - must not have any decimals; if transaction is 50.98, value should be 5098.
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("amount")
    public void setAmount(String amount) {
        this.amount = amount;
    }

    /**
     * Client reference id like OrderId, paymentId or unique Id.
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("merchantReferenceId")
    public String getMerchantReferenceId() {
        return merchantReferenceId;
    }

    /**
     * Client reference id like OrderId, paymentId or unique Id.
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("merchantReferenceId")
    public void setMerchantReferenceId(String merchantReferenceId) {
        this.merchantReferenceId = merchantReferenceId;
    }

    /**
     * Authorization transaction Id.
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("authorizationTransactionId")
    public Object getAuthorizationTransactionId() {
        return authorizationTransactionId;
    }

    /**
     * Authorization transaction Id.
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("authorizationTransactionId")
    public void setAuthorizationTransactionId(Object authorizationTransactionId) {
        this.authorizationTransactionId = authorizationTransactionId;
    }

   
    @JsonProperty("isRetry")
    public boolean isRetry() {
		return isRetry;
	}
    
    @JsonProperty("isRetry")
	public void setRetry(boolean isRetry) {
		this.isRetry = isRetry;
	}
    
    @JsonProperty("paymentType")
    public String getPaymentType() {
		return paymentType;
	}
    
    @JsonProperty("paymentType")
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	@Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(amount).append(merchantReferenceId).append(authorizationTransactionId).append(isRetry).append(paymentType)
        		.append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof SettlementRequest) == false) {
            return false;
        }
        SettlementRequest rhs = ((SettlementRequest) other);
        return new EqualsBuilder().append(amount, rhs.amount).append(merchantReferenceId, rhs.merchantReferenceId).append(authorizationTransactionId, rhs.authorizationTransactionId).append(isRetry, rhs.isRetry).append(paymentType,rhs.paymentType)
        		.append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
