
package com.ftd.pg.client.paypal.settlement.response;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;

import com.ftd.pg.client.creditcard.settlement.response.Errors;


/**
 * Payment-Gateway Settlement Response
 * <p>
 * 
 * 
 */
@JsonPropertyOrder({
    "settlementTransactionId",
    "settlementDate",
    "status",
    "reasonCode",
    "message",
    "errors",
    "requestId",
    "merchantReferenceId",
    "processorAccount",
    "requestToken"
})
public class SettlementResponse {

    /**
     * Authorization response transactionId(authorization token)
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("settlementTransactionId")
    private String settlementTransactionId;
    /**
     * Settlement Date
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("settlementDate")
    private Object settlementDate;
    /**
     * Authorization response status
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("status")
    private String status;
    /**
     * Authorization response reason code
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("reasonCode")
    private String reasonCode;
    /**
     * Response message from gateway
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("message")
    private String message;
    /**
     * List Settlement response error messages(if any)
     * <p>
     * 
     * 
     */
    @JsonProperty("errors")
    private List<Errors> errorMessages;
    /**
     * The same as Transaction Id for cybersouce transactions/ Corrleation Id for PayPal transactions
     * <p>
     * 
     * 
     */
    @JsonProperty("requestId")
    private String requestId;
    /**
     * Client reference id like OrderId, paymentId or unique Id.
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("merchantReferenceId")
    private String merchantReferenceId;
    /**
     * Merchant Account against which we process the transaction
     * <p>
     * 
     * 
     */
    @JsonProperty("processorAccount")
    private String processorAccount;
    
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("requestToken")
    private String requestToken;
    
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * Authorization response transactionId(authorization token)
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("settlementTransactionId")
    public String getSettlementTransactionId() {
        return settlementTransactionId;
    }

    /**
     * Authorization response transactionId(authorization token)
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("settlementTransactionId")
    public void setSettlementTransactionId(String settlementTransactionId) {
        this.settlementTransactionId = settlementTransactionId;
    }

    /**
     * Settlement Date
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("settlementDate")
    public Object getSettlementDate() {
        return settlementDate;
    }

    /**
     * Settlement Date
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("settlementDate")
    public void setSettlementDate(Object settlementDate) {
        this.settlementDate = settlementDate;
    }

    /**
     * Authorization response status
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    /**
     * Authorization response status
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * Authorization response reason code
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("reasonCode")
    public String getReasonCode() {
        return reasonCode;
    }

    /**
     * Authorization response reason code
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("reasonCode")
    public void setReasonCode(String reasonCode) {
        this.reasonCode = reasonCode;
    }

    /**
     * Response message from gateway
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("message")
    public String getMessage() {
        return message;
    }

    /**
     * Response message from gateway
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("message")
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * List Settlement response error messages(if any)
     * <p>
     * 
     * 
     */
    @JsonProperty("errors")
    public List<Errors> getErrorMessages() {
        return errorMessages;
    }

    /**
     * List Settlement response error messages(if any)
     * <p>
     * 
     * 
     */
    @JsonProperty("errorMessages")
    public void setErrorMessages(List<Errors> errorMessages) {
        this.errorMessages = errorMessages;
    }

    /**
     * The same as Transaction Id for cybersouce transactions/ Corrleation Id for PayPal transactions
     * <p>
     * 
     * 
     */
    @JsonProperty("requestId")
    public String getRequestId() {
        return requestId;
    }

    /**
     * The same as Transaction Id for cybersouce transactions/ Corrleation Id for PayPal transactions
     * <p>
     * 
     * 
     */
    @JsonProperty("requestId")
    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    /**
     * Client reference id like OrderId, paymentId or unique Id.
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("merchantReferenceId")
    public String getMerchantReferenceId() {
        return merchantReferenceId;
    }

    /**
     * Client reference id like OrderId, paymentId or unique Id.
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("merchantReferenceId")
    public void setMerchantReferenceId(String merchantReferenceId) {
        this.merchantReferenceId = merchantReferenceId;
    }

    /**
     * Merchant Account against which we process the transaction
     * <p>
     * 
     * 
     */
    @JsonProperty("processorAccount")
    public String getProcessorAccount() {
        return processorAccount;
    }

    /**
     * Merchant Account against which we process the transaction
     * <p>
     * 
     * 
     */
    @JsonProperty("processorAccount")
    public void setProcessorAccount(String processorAccount) {
        this.processorAccount = processorAccount;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(settlementTransactionId).append(settlementDate).append(status).append(reasonCode).append(message).append(errorMessages).append(requestId).append(merchantReferenceId).append(processorAccount).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof SettlementResponse) == false) {
            return false;
        }
        SettlementResponse rhs = ((SettlementResponse) other);
        return new EqualsBuilder().append(settlementTransactionId, rhs.settlementTransactionId).append(settlementDate, rhs.settlementDate).append(status, rhs.status).append(reasonCode, rhs.reasonCode).append(message, rhs.message).append(errorMessages, rhs.errorMessages).append(requestId, rhs.requestId).append(merchantReferenceId, rhs.merchantReferenceId).append(processorAccount, rhs.processorAccount).append(additionalProperties, rhs.additionalProperties).isEquals();
    }
    
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("requestToken")
    public String getRequestToken() {
        return requestToken;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("requestToken")
    public void setRequestToken(String requestToken) {
        this.requestToken = requestToken;
    }
    
}
