
package com.ftd.pg.client.paypal.settlement.request;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;


@JsonPropertyOrder({
    "amount",
	"authorizationTransactionId",
    "merchantReferenceId",
    "isRetry",
    "paymentType"
})
public class SettlementRequest {

    /**
     * authorization transaction Id
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("authorizationTransactionId")
    private String payPalAuthorizationTransactionId;
    /**
     * Client reference id like OrderId, paymentId or unique Id
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("merchantReferenceId")
    private String merchantReferenceId;
    /**
     * flag to determine if the request is new
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("isRetry")
    private boolean isRetry;
    /**
     * The amount to settle through payment service
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("amount")
    private Object amount;
   
    @JsonProperty("paymentType")
    private String paymentType;
    
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * authorization transaction Id
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("authorizationTransactionId")
    public String getPayPalAuthorizationTransactionId() {
        return payPalAuthorizationTransactionId;
    }

    /**
     * authorization transaction Id
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("authorizationTransactionId")
    public void setPayPalAuthorizationTransactionId(String payPalAuthorizationTransactionId) {
        this.payPalAuthorizationTransactionId = payPalAuthorizationTransactionId;
    }

    /**
     * Client reference id like OrderId, paymentId or unique Id
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("merchantReferenceId")
    public String getMerchantReferenceId() {
        return merchantReferenceId;
    }

    /**
     * Client reference id like OrderId, paymentId or unique Id
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("merchantReferenceId")
    public void setMerchantReferenceId(String merchantReferenceId) {
        this.merchantReferenceId = merchantReferenceId;
    }

    /**
     * flag to determine if the request is new
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("isRetry")
    public boolean isIsRetry() {
        return isRetry;
    }

    /**
     * flag to determine if the request is new
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("isRetry")
    public void setIsRetry(boolean isRetry) {
        this.isRetry = isRetry;
    }
    
    
    @JsonProperty("paymentType")
  	public String getPaymentType() {
  		return paymentType;
  	}
      
    @JsonProperty("paymentType")
  	public void setPaymentType(String paymentType) {
  		this.paymentType = paymentType;
  	}
    
    /**
     * The amount to settle through payment service
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("amount")
    public Object getAmount() {
        return amount;
    }

    /**
     * The amount to settle through payment service
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("amount")
    public void setAmount(Object amount) {
        this.amount = amount;
    }


    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((additionalProperties == null) ? 0 : additionalProperties
						.hashCode());
		result = prime * result + ((amount == null) ? 0 : amount.hashCode());
		result = prime * result + (isRetry ? 1231 : 1237);
		result = prime
				* result
				+ ((merchantReferenceId == null) ? 0 : merchantReferenceId
						.hashCode());
		result = prime
				* result
				+ ((payPalAuthorizationTransactionId == null) ? 0
						: payPalAuthorizationTransactionId.hashCode());
		result = prime * result
				+ ((paymentType == null) ? 0 : paymentType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SettlementRequest other = (SettlementRequest) obj;
		if (additionalProperties == null) {
			if (other.additionalProperties != null)
				return false;
		} else if (!additionalProperties.equals(other.additionalProperties))
			return false;
		if (amount == null) {
			if (other.amount != null)
				return false;
		} else if (!amount.equals(other.amount))
			return false;
		if (isRetry != other.isRetry)
			return false;
		if (merchantReferenceId == null) {
			if (other.merchantReferenceId != null)
				return false;
		} else if (!merchantReferenceId.equals(other.merchantReferenceId))
			return false;
		if (payPalAuthorizationTransactionId == null) {
			if (other.payPalAuthorizationTransactionId != null)
				return false;
		} else if (!payPalAuthorizationTransactionId
				.equals(other.payPalAuthorizationTransactionId))
			return false;
		if (paymentType == null) {
			if (other.paymentType != null)
				return false;
		} else if (!paymentType.equals(other.paymentType))
			return false;
		return true;
	}

	

}
