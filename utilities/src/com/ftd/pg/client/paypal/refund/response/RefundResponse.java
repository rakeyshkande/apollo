
package com.ftd.pg.client.paypal.refund.response;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;


/**
 * Payment-Gateway refund response 
 * <p>
 * 
 * 
 */
@JsonPropertyOrder({
    "refundTransactionId",
    "refundDate",
    "status",
    "reasonCode",
    "message",
    "errorMessages",
    "requestId",
    "merchantReferenceId",
    "processorAccount"
})
public class RefundResponse {

    /**
     * Authorization response transactionId(authorization token)
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("refundTransactionId")
    private String refundTransactionId;
    /**
     * just a refund date
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("refundDate")
    private Object refundDate;
    /**
     * Authorization response status
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("status")
    private String status;
    /**
     * Authorization response reason code
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("reasonCode")
    private String reasonCode;
    /**
     * Response message from gateway
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("message")
    private String message;
    /**
     * List of response error messages(if any)
     * <p>
     * 
     * 
     */
    @JsonProperty("errorMessages")
    private Object errorMessages;
    /**
     * The same as Transaction Id for cybersouce transactions/ Corrleation Id for PayPal transactions
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("requestId")
    private String requestId;
    /**
     * Client reference id like OrderId, paymentId or unique Id.
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("merchantReferenceId")
    private String merchantReferenceId;
    /**
     * Merchant Account against which we process the transaction
     * <p>
     * 
     * 
     */
    @JsonProperty("processorAccount")
    private String processorAccount;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * Authorization response transactionId(authorization token)
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("refundTransactionId")
    public String getRefundTransactionId() {
        return refundTransactionId;
    }

    /**
     * Authorization response transactionId(authorization token)
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("refundTransactionId")
    public void setRefundTransactionId(String refundTransactionId) {
        this.refundTransactionId = refundTransactionId;
    }

    /**
     * just a refund date
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("refundDate")
    public Object getRefundDate() {
        return refundDate;
    }

    /**
     * just a refund date
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("refundDate")
    public void setRefundDate(Object refundDate) {
        this.refundDate = refundDate;
    }

    /**
     * Authorization response status
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    /**
     * Authorization response status
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * Authorization response reason code
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("reasonCode")
    public String getReasonCode() {
        return reasonCode;
    }

    /**
     * Authorization response reason code
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("reasonCode")
    public void setReasonCode(String reasonCode) {
        this.reasonCode = reasonCode;
    }

    /**
     * Response message from gateway
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("message")
    public String getMessage() {
        return message;
    }

    /**
     * Response message from gateway
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("message")
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * List of response error messages(if any)
     * <p>
     * 
     * 
     */
    @JsonProperty("errorMessages")
    public Object getErrorMessages() {
        return errorMessages;
    }

    /**
     * List of response error messages(if any)
     * <p>
     * 
     * 
     */
    @JsonProperty("errorMessages")
    public void setErrorMessages(Object errorMessages) {
        this.errorMessages = errorMessages;
    }

    /**
     * The same as Transaction Id for cybersouce transactions/ Corrleation Id for PayPal transactions
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("requestId")
    public String getRequestId() {
        return requestId;
    }

    /**
     * The same as Transaction Id for cybersouce transactions/ Corrleation Id for PayPal transactions
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("requestId")
    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    /**
     * Client reference id like OrderId, paymentId or unique Id.
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("merchantReferenceId")
    public String getMerchantReferenceId() {
        return merchantReferenceId;
    }

    /**
     * Client reference id like OrderId, paymentId or unique Id.
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("merchantReferenceId")
    public void setMerchantReferenceId(String merchantReferenceId) {
        this.merchantReferenceId = merchantReferenceId;
    }

    /**
     * Merchant Account against which we process the transaction
     * <p>
     * 
     * 
     */
    @JsonProperty("processorAccount")
    public String getProcessorAccount() {
        return processorAccount;
    }

    /**
     * Merchant Account against which we process the transaction
     * <p>
     * 
     * 
     */
    @JsonProperty("processorAccount")
    public void setProcessorAccount(String processorAccount) {
        this.processorAccount = processorAccount;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(refundTransactionId).append(refundDate).append(status).append(reasonCode).append(message).append(errorMessages).append(requestId).append(merchantReferenceId).append(processorAccount).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof RefundResponse) == false) {
            return false;
        }
        RefundResponse rhs = ((RefundResponse) other);
        return new EqualsBuilder().append(refundTransactionId, rhs.refundTransactionId).append(refundDate, rhs.refundDate).append(status, rhs.status).append(reasonCode, rhs.reasonCode).append(message, rhs.message).append(errorMessages, rhs.errorMessages).append(requestId, rhs.requestId).append(merchantReferenceId, rhs.merchantReferenceId).append(processorAccount, rhs.processorAccount).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
