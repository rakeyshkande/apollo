
package com.ftd.pg.client.paypal.refund.request;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;


/**
 * Payment-Gateway refund Request
 * <p>
 * 
 * 
 */
@JsonPropertyOrder({
    "amount",
    "merchantReferenceId",
    "settlementTransactionId",
    "paymentType"
})
public class RefundRequest {

    /**
     * The amount to refund through payment service.
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("amount")
    private Object amount;
    /**
     * Client reference id like OrderId, paymentId or unique Id.
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("merchantReferenceId")
    private Object merchantReferenceId;
    /**
     * Authorization settlement Id.
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("settlementTransactionId")
    private Object settlementTransactionId;
    
    @JsonProperty("paymentType")
    private String paymentType;
    
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * The amount to refund through payment service.
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("amount")
    public Object getAmount() {
        return amount;
    }

    /**
     * The amount to refund through payment service.
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("amount")
    public void setAmount(Object amount) {
        this.amount = amount;
    }

    /**
     * Client reference id like OrderId, paymentId or unique Id.
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("merchantReferenceId")
    public Object getMerchantReferenceId() {
        return merchantReferenceId;
    }

    /**
     * Client reference id like OrderId, paymentId or unique Id.
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("merchantReferenceId")
    public void setMerchantReferenceId(Object merchantReferenceId) {
        this.merchantReferenceId = merchantReferenceId;
    }

   
    @JsonProperty("settlementTransactionId")
    public Object getSettlementTransactionId() {
		return settlementTransactionId;
	}

    @JsonProperty("settlementTransactionId")
	public void setSettlementTransactionId(Object settlementTransactionId) {
		this.settlementTransactionId = settlementTransactionId;
	}
    
    @JsonProperty("paymentType")
  	public String getPaymentType() {
  		return paymentType;
  	}
      
    @JsonProperty("paymentType")
  	public void setPaymentType(String paymentType) {
  		this.paymentType = paymentType;
  	}
    
	@Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(amount).append(merchantReferenceId).append(settlementTransactionId).append(paymentType).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof RefundRequest) == false) {
            return false;
        }
        RefundRequest rhs = ((RefundRequest) other);
        return new EqualsBuilder().append(amount, rhs.amount).append(merchantReferenceId, rhs.merchantReferenceId).append(settlementTransactionId, rhs.settlementTransactionId).append(additionalProperties, rhs.additionalProperties).append(paymentType,rhs.paymentType)
        		.isEquals();
    }

}
