package com.ftd.pg.client;

import java.util.logging.Logger;

import com.ftd.pg.client.paypal.refund.request.RefundRequest;
import com.ftd.pg.client.paypal.refund.response.RefundResponse;
import com.ftd.pg.client.paypal.settlement.request.SettlementRequest;
import com.ftd.pg.client.paypal.settlement.response.SettlementResponse;

public class PaypalClient {

	private static final Logger logger = Logger.getLogger(PaypalClient.class.getName());

	private final String PP_WS_SOCKET_TIMEOUT;

	public PaypalClient(String PP_WS_SOCKET_TIMEOUT) {
		this.PP_WS_SOCKET_TIMEOUT = PP_WS_SOCKET_TIMEOUT;
	}

	public RefundResponse refundPaypalTrans(RefundRequest request, String refundWsUrl) throws Exception {
		//logger.info("refundCC called: URL:" + refundWsUrl);
		if (refundWsUrl == null || refundWsUrl.isEmpty()) {
			logger.severe("Empty/null webservice url retrieved");
			throw new Exception("Empty/null webservice url retrieved");
		}
		String requestJsonString = PaymentGatewayUtils.convertToJsonString(request);
		logger.info("PG PayPal RefundRequest:: "+requestJsonString);
		return PaymentGatewayUtils.postPGRequest((String) request.getMerchantReferenceId(), refundWsUrl,
				PP_WS_SOCKET_TIMEOUT, requestJsonString, RefundResponse.class);
	}

	public SettlementResponse settlePaypalTrans(SettlementRequest request, String settlementWsUrl) throws Exception {
		//logger.info("Payment Gateway Settlment called URL::" + settlementWsUrl);
		if (settlementWsUrl == null || settlementWsUrl.isEmpty()) {
			logger.severe("Empty/null webservice url retrieved");
			throw new Exception("Empty/null webservice url retrieved");
		}
		String requestJsonString = PaymentGatewayUtils.convertToJsonString(request);
		logger.info("PG PayPal SettlementRequest ::" + requestJsonString);
		return PaymentGatewayUtils.postPGRequest(request.getMerchantReferenceId(), settlementWsUrl, PP_WS_SOCKET_TIMEOUT,
				requestJsonString, SettlementResponse.class);
	}

}
