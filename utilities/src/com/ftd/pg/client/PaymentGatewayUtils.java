/**
 * 
 */
package com.ftd.pg.client;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;

import javax.ws.rs.core.HttpHeaders;

import org.apache.commons.io.IOUtils;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.ftd.osp.utilities.RESTUtils;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pg.client.common.CreditCardValidationException;
import com.ftd.pg.client.creditcard.authorize.request.AuthorizeRequest;
import com.ftd.pg.client.creditcard.authorize.request.BillTo;
import com.ftd.pg.client.creditcard.authorize.request.CardinalCommerce;
import com.ftd.pg.client.creditcard.authorize.request.CreditCard;
import com.ftd.pg.client.creditcard.authorize.response.AuthorizeResponse;
import com.ftd.pg.client.creditcard.settlement.request.SettlementRequest;

/**
 * Utility class for Credit card client
 * 
 * @author nkatle
 *
 */
public class PaymentGatewayUtils {
	
	
	private static final Logger logger = new Logger(PaymentGatewayUtils.class.getName());
	private static String APPLICATION_JSON = "application/json";
	private static final String PAYMENT_GATEWAY_SERVICE_SECRET  ="PAYMENT_GATEWAY_SERVICE_SECRET"; 
	
	/**
	 * It validates the credit card authorize request and throws exception in case of any validation error
	 * 
	 * @param request
	 * @throws CreditCardValidationException
	 */
	public static void validateRequest(AuthorizeRequest request) throws CreditCardValidationException {
		StringBuilder errorMessage = new StringBuilder();
		
		CreditCard creditCard = request.getCreditCard();
		String tempCCNumber = creditCard.getCardNumber();
        if (tempCCNumber.length() > 24)
        {
        	errorMessage.append("Invalid Credicard number;");            
        }
        
        String tempExpMonth = creditCard.getExpirationMonth();
        String tempExpYear = creditCard.getExpirationYear();
        if(tempExpMonth == null || tempExpMonth.length() != 2 || tempExpYear == null)
        {
        	errorMessage.append("Invalid credit card expiry date;");        	
        }
        
        String tempAmount = request.getAmount() + "";
        if (tempAmount.length() > 13)
        {
        	errorMessage.append("Invalid credit card amount;");            
        }
        
        if(errorMessage.length() > 0){
        	throw new CreditCardValidationException(errorMessage.toString());
        }
	}


	/**
	 * Checks the given ccType present in comma separated list of credit cards. Return true if present other wise false
	 * 
	 * @param ccType
	 * @param csCcList
	 * @return
	 */
	public static boolean checkValidBamsCC(String ccType, String csCcList) {
		String[] ccList = csCcList.split(",");
		for(String cardType : ccList) {
			if(ccType.equals(cardType)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Converts given java object into json string
	 * 
	 * @param reqObject
	 * @param reqClass
	 * @return
	 * @throws Exception
	 */
	public static String convertToJsonString(Object reqObject) throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		String json = null;
		try {
			json = mapper.writeValueAsString(reqObject);
			logger.info("Request object parsed successfully");
		} catch (JsonGenerationException e) {
			logger.error("Exception occured while parsing Json string", e);
			throw e;
		} catch (JsonMappingException e) {
			logger.error("Exception occured while parsing Json string", e);
			throw e;
		} catch (IOException e) {
			logger.error("Exception occured while parsing Json string", e);
			throw e;
		}
		return json;
	}
	
	/**
	 * Converts given java object into json string
	 * 
	 * @param reqObject
	 * @param reqClass
	 * @return
	 * @throws Exception
	 */
	public static <E> E convertJsonStringToJavaResponse(InputStream jsonStream, Class<E> respClass) throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		E jsonObj = null;
		try {
			jsonObj = mapper.readValue(jsonStream, respClass);
		} catch (JsonGenerationException e) {
			logger.error("Exception occured while parsing Json string to pojo classes", e);
			throw e;
		} catch (IOException e) {
			logger.error("Exception occured while parsing Json string to pojo classes", e);
			throw e;
		}
		return jsonObj;
	}
	
	public static <E> E postPGRequest(String merchantRefId, String postUrl, String socketTimeout, String jsonReqData, Class<E> classType) throws Exception {
	  logger.debug("Payment gateway request started at " + System.currentTimeMillis()); 
	  E  responseObject = null;

	  HttpPost httpPost = null;
	  StringEntity jsonData = new StringEntity(jsonReqData, "utf-8");

	  new DefaultHttpClient();
	  httpPost = new HttpPost(postUrl);
	  httpPost.setEntity(jsonData);

	  RESTUtils utils = new RESTUtils("Payment Gateway", true, true);
	  String secret = RESTUtils.getConfigParamValue(PAYMENT_GATEWAY_SERVICE_SECRET, true);
	  httpPost.addHeader(HttpHeaders.ACCEPT_ENCODING, APPLICATION_JSON);
	  int timeOut = formatSocketTimeOut(socketTimeout);
	  String responseString = utils.executeRequest(httpPost, timeOut, secret);
	  InputStream stream = IOUtils.toInputStream(responseString);
	  responseObject = convertJsonStringToJavaResponse(stream, classType);


	  return responseObject;
	}
	
	private static int formatSocketTimeOut(String socketTimeout) {
		int timeout = 10 * 1000; //ms
		try {
			if (socketTimeout != null && !socketTimeout.isEmpty())
				timeout = Integer.parseInt(socketTimeout) * 1000; // Convert
																	// seconds
																	// to
																	// millisec
		} catch (NumberFormatException e) {
			logger.error("Exception during Formating the Socket Time out" + e.getMessage());
		}
		return timeout;
	}


	public static int getPort(String baseURL){
		int port;
		URI uri = null;
		try
		{
			uri = new URI(baseURL);
			port = uri.getPort();
		}
		catch(URISyntaxException e)
		{
			e.printStackTrace();
			port = 443;
		}
		if(port <= 0)
			port = 443;
		return port;

	}

	public static String getHost(String baseURL){
		String host = null;
		URI uri = null;
		try
		{
			uri = new URI(baseURL);
			host = uri.getHost();
		}
		catch(URISyntaxException e)
		{
			e.printStackTrace();
			logger.error("This is an invalid URL:" + baseURL);
		}
		return host;
	}
	
	public static String getPath(String baseURL){
		String path = null;
		URI uri = null;
		try
		{
			uri = new URI(baseURL);
			path = uri.getPath();
		}
		catch(URISyntaxException e)
		{
			e.printStackTrace();
			logger.error("This is an invalid URL:" + baseURL);
		}
		return path;
	}
	
	public static void main(String[] args) {
		CreditCardClient clientUtil =  new CreditCardClient("10000");
		
		AuthorizeRequest authorizeRequest = new AuthorizeRequest();
		authorizeRequest.setMerchantReferenceId("123456789");
		authorizeRequest.setCurrency("usd");
		authorizeRequest.setRetry(false);
		authorizeRequest.setAmount("6298");
		
		CreditCard creditCard = new CreditCard();
		creditCard.setCardNumber("4264281555555555");
		creditCard.setCvvNumber("123");
		creditCard.setExpirationMonth("01");
		creditCard.setExpirationYear("2019");
		creditCard.setCardType("VISA");
		CardinalCommerce cardinalCommerce = new CardinalCommerce();
		creditCard.setCardinalCommerce(cardinalCommerce);
		
		authorizeRequest.setCreditCard(creditCard);
		
		BillTo billTo =  new BillTo();
		billTo.setFirstName("John");
		billTo.setLastName("Doe");
		billTo.setMiddleName("M");
		billTo.setAddress1("3113 Woodcreek Dr");
		billTo.setAddress2("Suite# 1");
		billTo.setCity("Downers Grove");
		billTo.setState("IL");
		billTo.setZipCode("60515");
		billTo.setCountry("US");
		billTo.setPhone("630-555-1212");
		billTo.setEmail("jdoe@example.com");
		billTo.setCompany("abc");		
		authorizeRequest.setBillTo(billTo);
		
		
		SettlementRequest settlementReq = new SettlementRequest();
		settlementReq.setAmount("6000");
		settlementReq.setAuthorizationTransactionId("2342");
		settlementReq.setMerchantReferenceId("12321");
		settlementReq.setRetry(false);
		try {
			System.out.println(clientUtil.authorizeCC(authorizeRequest, "http://gcp-nonprod1-dev2-proxy.ftdi.com/payment-gateway-service/ftd/api/payment/creditcard/authorize"));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}


	public static <E> E postPGAutorizeRequest(String merchantRefId, String postUrl, String socketTimeout, String jsonReqData, Class<E> classType) throws Exception {
		  logger.debug("Payment gateway request for Auth started at " + System.currentTimeMillis()); 
		  E  responseObject = null;

		  HttpPost httpPost = null;
		  StringEntity jsonData = new StringEntity(jsonReqData, "utf-8");
		  httpPost = new HttpPost(postUrl);
		  httpPost.setEntity(jsonData);

		  RESTUtils utils = new RESTUtils("Payment Gateway", true, true);
		  String secret = RESTUtils.getConfigParamValue(PAYMENT_GATEWAY_SERVICE_SECRET, true);
		  httpPost.addHeader(HttpHeaders.ACCEPT_ENCODING, APPLICATION_JSON);
		  int timeOut = formatSocketTimeOut(socketTimeout);
		  String responseString = utils.executeRequest(httpPost, timeOut, secret);
		  InputStream stream = IOUtils.toInputStream(responseString);
		  responseObject = convertJsonStringToJavaResponse(stream, classType);


		  return responseObject;
		}
}