package com.ftd.osp.utilities.crypto;

import com.ftd.osp.utilities.crypto.vo.PGPPrivateKeyInfoVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;

import com.ftd.osp.utilities.plugins.Logger;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import java.security.NoSuchProviderException;
import java.security.Security;

import java.sql.Connection;

import java.util.HashMap;
import java.util.Iterator;

import java.util.Map;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openpgp.PGPCompressedData;
import org.bouncycastle.openpgp.PGPEncryptedDataList;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPLiteralData;
import org.bouncycastle.openpgp.PGPObjectFactory;
import org.bouncycastle.openpgp.PGPOnePassSignatureList;
import org.bouncycastle.openpgp.PGPPrivateKey;
import org.bouncycastle.openpgp.PGPPublicKeyEncryptedData;
import org.bouncycastle.openpgp.PGPSecretKey;
import org.bouncycastle.openpgp.PGPSecretKeyRingCollection;
import org.bouncycastle.openpgp.PGPUtil;


/**
 * Provides Encryption and Decryption functionalty based on the PGP standards.
 * Uses the Bouncy Castle library
 * <br/>
 * Note: Any modules linking to this class requires the following <a href="http://www.bouncycastle.org/">Bouncy Castle</a> libraries
 * to be in the dependencies:
 *
 * <ol>
 * <li>ftd/lib/bouncy-castle/1.3/bcpg-jdk14-133.jar</li>
 * <li>ftd/lib/bouncy-castle/1.3/bcprov-jdk14-133.jar</li>
 * </ol>
 *
 * Key information is stored in a Database Table.
 * 
 * Note: For reference, the Web Site is using the following library/options for Encryption:
 * Crypt::OpenPGP 1.03
 * Pubkey: RSA,
 * Cipher: 3DES,
 * Hash: MD5,
 * Compression: ZIP
 */
public class PGPEncryption
{
   private static Logger logger = new Logger(PGPEncryption.class.getName());
   
  static 
  {
    // Register the Bouncy Castle Security Provider
    Security.addProvider(new BouncyCastleProvider());
  }

  /**
   * Decrypts the passed in input stream and returns the results as a byte[]
   * @param is
   * @param keyCode The Code identifying the Private Key Information
   * @param connection The database connection to utilize to retrieve encryption record information
   * @return Decrypted data.
   * @throws Exception On Errors retrieving Key information or decrypting the data
   */
  public byte[] decryptStream(InputStream encryptedData, String keyCode, Connection connection)
    throws Exception
  {
    // Retrieve the path to the Key File and Passphrase from the database
    PGPPrivateKeyInfoVO privateKeyInfo = getPrivateKeyInformation(keyCode, connection);

    // Decrypt the Stream
    return decryptStream(encryptedData, getKeyInputStream(keyCode, privateKeyInfo.getKeyFilePath()), privateKeyInfo.getPassPhrase());
  }

  /**
   * Decrypt the passed in stream, and returns a byte[] of the data.
   * @param in Encrypted Input Stream to read
   * @param keyIn InputStream for reading the Private Key File
   * @param passPhrase Pass Phrase for the Private Key File
   * @return Decrypted data.
   * @throws Exception
   */
  public byte[] decryptStream(InputStream encryptedData, InputStream keyIn, String passPhrase)
    throws Exception
  {

    try
    {
      InputStream in = PGPUtil.getDecoderStream(encryptedData); // Wrap the input stream so it supports binary and base64
      PGPObjectFactory pgpF = new PGPObjectFactory(in);
      PGPEncryptedDataList enc;

      Object o = pgpF.nextObject();

      //
      // the first object might be a PGP marker packet.
      //
      if (o instanceof PGPEncryptedDataList)
      {
        enc = (PGPEncryptedDataList) o;
      }
      else
      {
        enc = (PGPEncryptedDataList) pgpF.nextObject();
      }

      //
      // find the secret key
      //
      Iterator it = enc.getEncryptedDataObjects();
      PGPPrivateKey sKey = null;
      PGPPublicKeyEncryptedData pbe = null;

      while ((sKey == null) && it.hasNext())
      {
        pbe = (PGPPublicKeyEncryptedData) it.next();

        sKey = findSecretKey(keyIn, pbe.getKeyID(), passPhrase.toCharArray());
      }

      if (sKey == null)
      {
        throw new IllegalArgumentException("secret key for message not found.");
      }

      InputStream clear = pbe.getDataStream(sKey, "BC");

      PGPObjectFactory plainFact = new PGPObjectFactory(clear);
      Object thing = plainFact.nextObject();
      Object message = null;
      InputStream compressedStream = null;
      
      // Handle Uncompressed or Compressed Data
      if(thing instanceof PGPLiteralData)
      {
          // data is uncompressed
          message = (PGPLiteralData) thing;
      } else
      {
          // Data is compressed, retrieve decompressing message
          PGPCompressedData cData = (PGPCompressedData) thing;
    
          compressedStream = new BufferedInputStream(cData.getDataStream());
          PGPObjectFactory pgpFact = new PGPObjectFactory(compressedStream);
          message = pgpFact.nextObject();
      
      }
      
      InputStream unc = null;
      ByteArrayOutputStream out = new ByteArrayOutputStream();   
      try
      {
        if (message instanceof PGPLiteralData)
        {
          PGPLiteralData ld = (PGPLiteralData) message;

          unc = ld.getInputStream();

          int ch;

          while ((ch = unc.read()) >= 0)
          {
            out.write(ch);
          }
        }
        else if (message instanceof PGPOnePassSignatureList)
        {
          throw new PGPException("encrypted message contains a signed message - not literal data.");
        }
        else
        {
          throw new PGPException("message is not a simple encrypted file - type unknown.");
        }
      }
      finally
      {
        if (unc != null)
        {
          unc.close();
        }

        if (out != null)
        {
          out.close();
        }

        if(compressedStream != null)
        {
            compressedStream.close();
        }
      }

      if (pbe.isIntegrityProtected())
      {
        if (!pbe.verify())
        {
          throw new PGPException("message failed integrity check");
        }
      }

      return out.toByteArray();
    }
    catch (PGPException e)
    {
      if (e.getUnderlyingException() != null)
      {
        throw e.getUnderlyingException();
      }
      else
      {
        throw e;
      }
    }
    finally
    {
      if (keyIn != null)
      {
        keyIn.close();
      }

      if (encryptedData != null)
      {
        encryptedData.close();
      }
    }
  }

  /**
   * Load a secret key ring collection from keyIn and find the secret key corresponding to
   * keyID if it exists.
   *
   * @param keyIn input stream representing a key ring collection.
   * @param keyID keyID we want.
   * @param pass passphrase to decrypt secret key with.
   * @return
   * @throws IOException
   * @throws PGPException
   * @throws NoSuchProviderException
   */
  private PGPPrivateKey findSecretKey(InputStream keyIn, long keyID, char[] pass)
    throws IOException, PGPException, NoSuchProviderException
  {
    PGPSecretKeyRingCollection pgpSec = new PGPSecretKeyRingCollection(PGPUtil.getDecoderStream(keyIn));
    PGPSecretKey pgpSecKey = pgpSec.getSecretKey(keyID);

    if (pgpSecKey == null)
    {
      return null;
    }

    return pgpSecKey.extractPrivateKey(pass, "BC");
  }


  /**
   * Returns the Private Key Information for the passed in key code.
   * Returns null if it is unable to locate the key code information
   * @param keyCode The keycode to retrieve Private Key Information for
   * @param connection Connection to the Database
   * @return A VO containing the Private key information
   * @throws  Exception When there is no private key information for the key code.
   */
  private PGPPrivateKeyInfoVO getPrivateKeyInformation(String keyCode, Connection connection) throws Exception
  {
    HashMap inputParams = new HashMap();
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(connection);
    dataRequest.setStatementID("UTIL_STMT_CRYPTO_GET_PGP_PRIVATE_KEY_INFO");
    inputParams.put("IN_PGP_KEY_CODE", keyCode);
    dataRequest.setInputParams(inputParams);

    String privateKeyFilePath = null;
    String passPhrase = null;
    
    try 
    {
      Map outputs = (Map) DataAccessUtil.getInstance().execute(dataRequest);    
      privateKeyFilePath = (String) outputs.get("OUT_KEY_FILE_PATH");
      passPhrase = (String) outputs.get("OUT_PASS_PHRASE");
    } catch (Exception e)
    {
      logger.error("Failed to retrieve Key Information for Key Code: " + keyCode, e);
      throw new Exception("Failed to retrieve Key Information for Key Code: " + keyCode, e);
    }
    
    if(privateKeyFilePath == null || passPhrase == null)
    {
      logger.error("No Key Information configured for Key Code: " + keyCode);
      throw new Exception("No Key Information configured for Key Code: " + keyCode);
    }

    PGPPrivateKeyInfoVO retVal = new PGPPrivateKeyInfoVO();
    retVal.setKeyFilePath(privateKeyFilePath);
    retVal.setPassPhrase(passPhrase);
    return retVal;
  }
  
  
  /**
   * Opens the passed in file path and returns an Input Stream.
   * Formats appropriate exceptions if the file does not exist
   * @param keyFilePath
   * @return
   * @throws Exception
   */
  private InputStream getKeyInputStream(String keyCode, String keyFilePath)
  throws Exception
  {
    try 
    {
      return new FileInputStream(keyFilePath);
    } catch (Exception e)
    {
      logger.error("Failed to open Key File for Key Code: " + keyFilePath + " File Path: " + keyFilePath, e);
      throw new Exception("Failed to open Key File for Key Code: " + keyCode, e);
    }
  }
}
