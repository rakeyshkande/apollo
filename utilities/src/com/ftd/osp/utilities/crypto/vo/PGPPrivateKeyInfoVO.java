package com.ftd.osp.utilities.crypto.vo;

/**
 * Value object containing fields for the Private Key Information
 */
public class PGPPrivateKeyInfoVO
{
  /**
   * Path to the Key File
   */
  private String keyFilePath;
  
  /** 
   * Pass phrase for the private key file 
   */
  private String passPhrase;


  public void setKeyFilePath(String keyFilePath)
  {
    this.keyFilePath = keyFilePath;
  }

  public String getKeyFilePath()
  {
    return keyFilePath;
  }

  public void setPassPhrase(String passPhrase)
  {
    this.passPhrase = passPhrase;
  }

  public String getPassPhrase()
  {
    return passPhrase;
  }
}
