package com.ftd.osp.utilities.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 
 * @author sgupta
 * 
 */
public class CreateRequest implements Serializable {
	private static final long serialVersionUID = 7904385365588265739L;
	private BatchInfo batchInfo;
	private String batchNumber;
	private String createdOn;
	private String expirationDate;
	private FileFormat fileFormat;
	private String giftCodeId;
	private BigDecimal issueAmount;
	private String issueDate;
	private Meta meta;
	private BigDecimal paidAmount;
	private Integer quantity;

	private String redeemedRefNo;
	private BigDecimal redemptionAmount;
	private String redemptionDate;
	private String reinstateDate;
	private String siteId;
	private List<ReferenceCode> source;
	private String status;
	private String type;
	private String updatedOn;
	private String promotionId;

	public CreateRequest() {

	}

	public CreateRequest(BatchInfo batchInfo, String batchNumber,
			String createdOn, String expirationDate, FileFormat fileFormat,
			String giftCodeId, BigDecimal issueAmount, String issueDate,
			Meta meta, BigDecimal paidAmount, Integer quantity,
			String redeemedRefNo, BigDecimal redemptionAmount,
			String redemptionDate, String reinstateDate, String siteId,
			List<ReferenceCode> source, String status, String type,
			String updatedOn, String promotionId) {
		super();
		this.batchInfo = batchInfo;
		this.batchNumber = batchNumber;
		this.createdOn = createdOn;
		this.expirationDate = expirationDate;
		this.fileFormat = fileFormat;
		this.giftCodeId = giftCodeId;
		this.issueAmount = issueAmount;
		this.issueDate = issueDate;
		this.meta = meta;
		this.paidAmount = paidAmount;
		this.quantity = quantity;
		this.redeemedRefNo = redeemedRefNo;
		this.redemptionAmount = redemptionAmount;
		this.redemptionDate = redemptionDate;
		this.reinstateDate = reinstateDate;
		this.siteId = siteId;
		this.source = source;
		this.status = status;
		this.type = type;
		this.updatedOn = updatedOn;
		this.promotionId = promotionId;
	}

	public BatchInfo getBatchInfo() {
		return batchInfo;
	}

	public void setBatchInfo(BatchInfo batchInfo) {
		this.batchInfo = batchInfo;
	}

	public String getBatchNumber() {
		return batchNumber;
	}

	public void setBatchNumber(String batchNumber) {
		this.batchNumber = batchNumber;
	}

	public String getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	public String getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}

	public FileFormat getFileFormat() {
		return fileFormat;
	}

	public void setFileFormat(FileFormat fileFormat) {
		this.fileFormat = fileFormat;
	}

	public String getGiftCodeId() {
		return giftCodeId;
	}

	public void setGiftCodeId(String giftCodeId) {
		this.giftCodeId = giftCodeId;
	}

	public BigDecimal getIssueAmount() {
		return issueAmount;
	}

	public void setIssueAmount(BigDecimal issueAmount) {
		this.issueAmount = issueAmount;
	}

	public String getIssueDate() {
		return issueDate;
	}

	public void setIssueDate(String issueDate) {
		this.issueDate = issueDate;
	}

	public Meta getMeta() {
		return meta;
	}

	public void setMeta(Meta meta) {
		this.meta = meta;
	}

	public BigDecimal getPaidAmount() {
		return paidAmount;
	}

	public void setPaidAmount(BigDecimal paidAmount) {
		this.paidAmount = paidAmount;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public String getRedeemedRefNo() {
		return redeemedRefNo;
	}

	public void setRedeemedRefNo(String redeemedRefNo) {
		this.redeemedRefNo = redeemedRefNo;
	}

	public BigDecimal getRedemptionAmount() {
		return redemptionAmount;
	}

	public void setRedemptionAmount(BigDecimal redemptionAmount) {
		this.redemptionAmount = redemptionAmount;
	}

	public String getRedemptionDate() {
		return redemptionDate;
	}

	public void setRedemptionDate(String redemptionDate) {
		this.redemptionDate = redemptionDate;
	}

	public String getReinstateDate() {
		return reinstateDate;
	}

	public void setReinstateDate(String reinstateDate) {
		this.reinstateDate = reinstateDate;
	}

	public String getSiteId() {
		return siteId;
	}

	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}

	public List<ReferenceCode> getSource() {
		return source;
	}

	public void setSource(List<ReferenceCode> source) {
		this.source = source;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(String updatedOn) {
		this.updatedOn = updatedOn;
	}
	
	public String getPromotionId() {
		return promotionId;
	}

	public void setPromotionId(String promotionId) {
		this.promotionId = promotionId;
	}

	@Override
	public String toString() {
		return "CreateRequest [batchInfo=" + batchInfo + ", batchNumber="
				+ batchNumber + ", createdOn=" + createdOn
				+ ", expirationDate=" + expirationDate + ", fileFormat="
				+ fileFormat + ", giftCodeId=" + giftCodeId + ", issueAmount="
				+ issueAmount + ", issueDate=" + issueDate + ", meta=" + meta
				+ ", paidAmount=" + paidAmount + ", quantity=" + quantity
				+ ", redeemedRefNo=" + redeemedRefNo + ", redemptionAmount="
				+ redemptionAmount + ", redemptionDate=" + redemptionDate
				+ ", reinstateDate=" + reinstateDate + ", siteId=" + siteId
				+ ", source=" + source + ", status=" + status + ", type="
				+ type + ", updatedOn=" + updatedOn + ", promotionId="
				+ promotionId + "]";
	}

}
