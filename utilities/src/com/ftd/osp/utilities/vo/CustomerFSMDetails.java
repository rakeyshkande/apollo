package com.ftd.osp.utilities.vo;
/*
* Copyright 2012 by United Online, Inc.,
* 3113 Woodcreek Drive, Downers Grove, IL, 60515, U.S.A.
* All rights reserved.
* 
* This software is the confidential and proprietary information
* of United Online, Inc. ("Confidential Information").  You
* shall not disclose such Confidential Information and shall use
* it only in accordance with the terms of the license agreement
* you entered into with United Online.
*/
import java.util.Date;

/**
 * This class holds the customer free shipping details
 *
 */

public class CustomerFSMDetails {
	
	//Declare private class variables
	
	/**
	 * holds the modified date of the customer free shipping details
	 */
    private Date modifiedDate;

    
    /**
     * holds the created date
     */
    private Date createdDate;

    /**
     * holds the customer id
     */
    private Integer customerId;

    /**
     * holds the free shipping start date
     */
    private Date fsStartDate;

    /**
     * holds the free shippin end date
     */
    private Date fsEndDate;

    
    /**
     * holds the free shipping auto renew status
     */
    private String fsAutoRenew;

    /**
     * holds the free shipping join date
     */
    private Date fsJoinDate;

    
    /**
     * holds the free shipping member or not
     */
    private String fsMember;

    /**
     * holds the free shipping order number
     */
    private String fsmOrderNumber;
    
    /**
     * holds the fs membership term
     */
    private Integer fsTerm;
    
	/**
	 * @return the fsTerm
	 */
	public Integer getFsTerm() {
		return fsTerm;
	}

	/**
	 * @param fsTerm the fsTerm to set
	 */
	public void setFsTerm(Integer fsTerm) {
		this.fsTerm = fsTerm;
	}

	/**
	 * @return the modifiedDate
	 */
	public Date getModifiedDate() {
		return modifiedDate;
	}

	/**
	 * @param modifiedDate the modifiedDate to set
	 */
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	/**
	 * @return the createdDate
	 */
	public Date getCreatedDate() {
		return createdDate;
	}

	/**
	 * @param createdDate the createdDate to set
	 */
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 * @return the customerId
	 */
	public Integer getCustomerId() {
		return customerId;
	}

	/**
	 * @param customerId the customerId to set
	 */
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	/**
	 * @return the fsStartDate
	 */
	public Date getFsStartDate() {
		return fsStartDate;
	}

	/**
	 * @param fsStartDate the fsStartDate to set
	 */
	public void setFsStartDate(Date fsStartDate) {
		this.fsStartDate = fsStartDate;
	}

	/**
	 * @return the fsEndDate
	 */
	public Date getFsEndDate() {
		return fsEndDate;
	}

	/**
	 * @param fsEndDate the fsEndDate to set
	 */
	public void setFsEndDate(Date fsEndDate) {
		this.fsEndDate = fsEndDate;
	}

	/**
	 * @return the fsAutoRenew
	 */
	public String getFsAutoRenew() {
		return fsAutoRenew;
	}

	/**
	 * @param fsAutoRenew the fsAutoRenew to set
	 */
	public void setFsAutoRenew(String fsAutoRenew) {
		this.fsAutoRenew = fsAutoRenew;
	}

	/**
	 * @return the fsJoinDate
	 */
	public Date getFsJoinDate() {
		return fsJoinDate;
	}

	/**
	 * @param fsJoinDate the fsJoinDate to set
	 */
	public void setFsJoinDate(Date fsJoinDate) {
		this.fsJoinDate = fsJoinDate;
	}

	/**
	 * @return the fsMember
	 */
	public String getFsMember() {
		return fsMember;
	}

	/**
	 * @param fsMember the fsMember to set
	 */
	public void setFsMember(String fsMember) {
		this.fsMember = fsMember;
	}

	/**
	 * @return the fsmOrderNumber
	 */
	public String getFsmOrderNumber() {
		return fsmOrderNumber;
	}

	/**
	 * @param fsmOrderNumber the fsmOrderNumber to set
	 */
	public void setFsmOrderNumber(String fsmOrderNumber) {
		this.fsmOrderNumber = fsmOrderNumber;
	}

	@Override
	public String toString() {
		return "CustomerFSMDetails [modifiedDate=" + modifiedDate
				+ ", createdDate=" + createdDate + ", customerId=" + customerId
				+ ", fsStartDate=" + fsStartDate + ", fsEndDate=" + fsEndDate
				+ ", fsAutoRenew=" + fsAutoRenew + ", fsJoinDate=" + fsJoinDate
				+ ", fsMember=" + fsMember + ", fsmOrderNumber="
				+ fsmOrderNumber + ", fsTerm=" + fsTerm + "]";
	}

   
}