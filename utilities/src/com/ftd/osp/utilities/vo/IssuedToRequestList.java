package com.ftd.osp.utilities.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class IssuedToRequestList implements Serializable {
	public IssuedToRequestList() {
		super();
	}
	
	public IssuedToRequestList(List<UpdateIssuedToRequest> updateRecipientInfo, String batchNumber) {
		super();
		this.updateRecipientInfo = updateRecipientInfo;
		this.batchNumber = batchNumber;
	}
	public List<UpdateIssuedToRequest> getUpdateRecipientInfo() {
		return updateRecipientInfo;
	}
	public void setUpdateRecipientInfo(List<UpdateIssuedToRequest> updateRecipientInfo) {
		this.updateRecipientInfo = updateRecipientInfo;
	}
	public String getBatchNumber() {
		return batchNumber;
	}
	public void setBatchNumber(String batchNumber) {
		this.batchNumber = batchNumber;
	}
	private static final long serialVersionUID = 7282274293869058446L;
    private List<UpdateIssuedToRequest> updateRecipientInfo = new ArrayList<UpdateIssuedToRequest>();
    private String batchNumber;
}
