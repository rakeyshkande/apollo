package com.ftd.osp.utilities.vo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.ftd.util.adapters.DateTypeAdapter;
import com.ftd.util.adapters.MembershipTypeAdapter;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class MembershipDetailsVO {
	private String membershipId;
	private String membershipNumber;
	private String status;
	@XmlElement(name="atr_1")
	private String groupId;
	@XmlElement(name="identifier1")
	private String pc_card_id;
	private boolean isMembershipBenefitsApplicable;
	private String email;
	
	@XmlJavaTypeAdapter(value = MembershipTypeAdapter.class)
	private String membershipType;
	
	private List<String> errorList;
	
	private List<ErrorMessageVO> errorMessages;	
	
	@XmlJavaTypeAdapter(value = DateTypeAdapter.class)
	private Date startDate;
	
	@XmlJavaTypeAdapter(value = DateTypeAdapter.class)
	@XmlElement(name="expireDate")
	private Date expiryDate;
	
	@XmlJavaTypeAdapter(value = DateTypeAdapter.class)
	private Date joinDate;	
	
	public String getMembershipId() {
		return membershipId;
	}
	public void setMembershipId(String membershipId) {
		this.membershipId = membershipId;
	}
	public String getMembershipNumber() {
		return membershipNumber;
	}
	public void setMembershipNumber(String membershipNumber) {
		this.membershipNumber = membershipNumber;
	}
	public boolean isMembershipBenefitsApplicable() {
		return isMembershipBenefitsApplicable;
	}
	public void setMembershipBenefitsApplicable(
			boolean isMembershipBenefitsApplicable) {
		this.isMembershipBenefitsApplicable = isMembershipBenefitsApplicable;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}		
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getGroupId() {
		return groupId;
	}
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
	public String getMembershipType() {
		return membershipType;
	}
	public void setMembershipType(String membershipType) {
		this.membershipType = membershipType;
	}
	public List<String> getErrorList() {
		return errorList;
	}
	public void setErrorList(List<String> errorList) {
		this.errorList = errorList;
	}
	public List<ErrorMessageVO> getErrorMessages() {
		if(errorMessages == null) {
			errorMessages = new ArrayList<ErrorMessageVO>();
		}
		return errorMessages;
	}
	public void setErrorMessages(List<ErrorMessageVO> errorMessages) {
		this.errorMessages = errorMessages;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}
	public Date getJoinDate() {
		return joinDate;
	}
	public void setJoinDate(Date joinDate) {
		this.joinDate = joinDate;
	}	
}
