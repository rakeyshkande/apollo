package com.ftd.osp.utilities.vo;


public class ProductAttributeVO
{
  private String productAttributeId;
  private String productAttributeName;
  private String productAttributeOperator;
  private String productAttributeValue;
  private String productAttributeDesc;


    public void setProductAttributeId(String productAttributeId) {
        this.productAttributeId = productAttributeId;
    }

    public String getProductAttributeId() {
        return productAttributeId;
    }

    public void setProductAttributeName(String productAttributeName) {
        this.productAttributeName = productAttributeName;
    }

    public String getProductAttributeName() {
        return productAttributeName;
    }

    public void setProductAttributeOperator(String productAttributeOperator) {
        this.productAttributeOperator = productAttributeOperator;
    }

    public String getProductAttributeOperator() {
        return productAttributeOperator;
    }


    public void setProductAttributeDesc(String productAttributeDesc) {
        this.productAttributeDesc = productAttributeDesc;
    }

    public String getProductAttributeDesc() {
        return productAttributeDesc;
    }

    public void setProductAttributeValue(String productAttributeValue) {
        this.productAttributeValue = productAttributeValue;
    }

    public String getProductAttributeValue() {
        return productAttributeValue;
    }
}
