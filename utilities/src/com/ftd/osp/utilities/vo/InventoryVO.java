/**
 * 
 */
package com.ftd.osp.utilities.vo;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author smeka
 *
 */
public class InventoryVO {
	
	private String productId;
	private String productName;
	private String productType;
	private boolean isProdAvailable;
	private boolean isFloristDeliverable;
	private boolean isVendorDeliverable;
	
	private String vendorId;	
	private String vendorName;
	private boolean isVendorProdAvailable;
	
	private BigDecimal invTrkId;
	private String invStatus; // each inventory status
	private Date invStartDate;
	private Date invEndDate;	
	
	private int warningThreshold;
	private int shutdownThreshold;
	private long onHand;
	
	private long totalOrders;
	private long totalShipped;
	private long totalPending;	
	private long totalInventory;
	
	private boolean noInventory = false;
	
	public BigDecimal getInvTrkId() {
		return invTrkId;
	}
	public void setInvTrkId(BigDecimal invTrkId) {
		this.invTrkId = invTrkId;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getVendorId() {
		return vendorId;
	}
	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public boolean isProdAvailable() {
		return isProdAvailable;
	}
	public void setProdAvailable(boolean isProdAvailable) {
		this.isProdAvailable = isProdAvailable;
	}
	public String getVendorName() {
		return vendorName;
	}
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	public boolean isVendorProdAvailable() {
		return isVendorProdAvailable;
	}
	public void setVendorProdAvailable(boolean isVendorProdAvailable) {
		this.isVendorProdAvailable = isVendorProdAvailable;
	}
	public Date getInvStartDate() {
		return invStartDate;
	}
	public void setInvStartDate(Date invStartDate) {
		this.invStartDate = invStartDate;
	}
	public Date getInvEndDate() {
		return invEndDate;
	}
	public void setInvEndDate(Date invEndDate) {
		this.invEndDate = invEndDate;
	}
	public int getWarningThreshold() {
		return warningThreshold;
	}
	public void setWarningThreshold(int warningThreshold) {
		this.warningThreshold = warningThreshold;
	}
	public int getShutdownThreshold() {
		return shutdownThreshold;
	}
	public void setShutdownThreshold(int shutdownThreshold) {
		this.shutdownThreshold = shutdownThreshold;
	}
	public long getOnHand() {
		return onHand;
	}
	public void setOnHand(long onHand) {
		this.onHand = onHand;
	}
	public boolean isFloristDeliverable() {
		return isFloristDeliverable;
	}
	public void setFloristDeliverable(boolean isFloristDeliverable) {
		this.isFloristDeliverable = isFloristDeliverable;
	}
	public boolean isVendorDeliverable() {
		return isVendorDeliverable;
	}
	public void setVendorDeliverable(boolean isVendorDeliverable) {
		this.isVendorDeliverable = isVendorDeliverable;
	}
	public String getInvStatus() {
		return invStatus;
	}
	public void setInvStatus(String invStatus) {
		this.invStatus = invStatus;
	}
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	public long getTotalInventory() {
		return totalInventory;
	}
	public void setTotalInventory(long totalInventory) {
		this.totalInventory = totalInventory;
	}
	public long getTotalShipped() {
		return totalShipped;
	}
	public void setTotalShipped(long totalShipped) {
		this.totalShipped = totalShipped;
	}
	public long getTotalPending() {
		return totalPending;
	}
	public void setTotalPending(long totalPending) {
		this.totalPending = totalPending;
	}
	public long getTotalOrders() {
		return totalOrders;
	}
	public void setTotalOrders(long totalOrders) {
		this.totalOrders = totalOrders;
	}
	public boolean isNoInventory() {
		return noInventory;
	}
	public void setNoInventory(boolean noInventory) {
		this.noInventory = noInventory;
	}		
}
