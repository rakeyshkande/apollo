package com.ftd.osp.utilities.vo;

import java.io.Serializable;

public class UpdateIssuedToRequest implements Serializable{
	public UpdateIssuedToRequest() {
		super();
	}
	
	public UpdateIssuedToRequest(String giftCodeId, RecipientInfo recipientInfo) {
		super();
		this.recipientInfo = recipientInfo;
	}
	
	public RecipientInfo getRecipientInfo() {
		return recipientInfo;
	}
	public void setRecipientInfo(RecipientInfo recipientInfo) {
		this.recipientInfo = recipientInfo;
	}
	/**
	 * 
	 */
	private static final long serialVersionUID = -6587232376562912848L;
	RecipientInfo recipientInfo;

}
