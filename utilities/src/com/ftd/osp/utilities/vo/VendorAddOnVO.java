package com.ftd.osp.utilities.vo;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

public class VendorAddOnVO
{
    private String vendorId;
    private String addOnId;
    private String sKU;
    private String cost;
    private Boolean assignedFlag;

    public String getVendorId()
    {
        return vendorId;
    }

    public void setVendorId(String vendorId)
    {
        this.vendorId = vendorId;
    }

    public String getSKU()
    {
        return sKU;
    }

    public void setSKU(String sKU)
    {
        this.sKU = sKU;
    }

    public String getCost()
    {
        return cost;
    }

    public void setCost(String cost)
    {
        this.cost = cost;
    }

    public String getAddOnId()
    {
        return addOnId;
    }

    public void setAddOnId(String addOnId)
    {
        this.addOnId = addOnId;
    }
    public String toXML() throws Exception
    {
        StringBuffer sb = new StringBuffer();
    
        sb.append("<VendorAddOnVO>");
        Field[] fields = this.getClass().getDeclaredFields();
    
        for (int i = 0; i < fields.length; i++)
        {
            if (!Modifier.isStatic(fields[i].getModifiers()))
            {
                sb.append("<" + fields[i].getName() + ">");
                
                if (fields[i].get(this) != null)
                {
                    sb.append(fields[i].get(this));
                }
                sb.append("</" + fields[i].getName() + ">");
            }        
        }
        sb.append("</VendorAddOnVO>");
        
        return sb.toString();
    }

    public Boolean getAssignedFlag()
    {
        return assignedFlag;
    }

    public void setAssignedFlag(Boolean assignedFlag)
    {
        this.assignedFlag = assignedFlag;
    }
}
