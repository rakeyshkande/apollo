package com.ftd.osp.utilities.vo;

import java.io.Serializable;

public class BatchInfo implements Serializable {
	private static final long serialVersionUID = -8857339974427731297L;
	private String batchDescription;

	private String batchName;

	private String batchComment;

	private String orderSource;

	private String requestedBy;
	
	private String prefix;
	
	private String other;
	
	private Integer quantity;
	
	public BatchInfo(){
		
	}

	public BatchInfo(String batchDescription, String batchName,
			String batchComment, String orderSource, String requestedBy, String prefix, String other, Integer quantity) {
		super();
		this.batchDescription = batchDescription;
		this.batchName = batchName;
		this.batchComment = batchComment;
		this.orderSource = orderSource;
		this.requestedBy = requestedBy;
		this.prefix = prefix;
		this.other = other;
		this.quantity =quantity;
	}

	public String getBatchDescription() {
		return batchDescription;
	}

	public void setBatchDescription(String batchDescription) {
		this.batchDescription = batchDescription;
	}

	public String getBatchName() {
		return batchName;
	}

	public void setBatchName(String batchName) {
		this.batchName = batchName;
	}

	public String getBatchComment() {
		return batchComment;
	}

	public void setBatchComment(String batchComment) {
		this.batchComment = batchComment;
	}

	public String getOrderSource() {
		return orderSource;
	}

	public void setOrderSource(String orderSource) {
		this.orderSource = orderSource;
	}

	public String getRequestedBy() {
		return requestedBy;
	}

	public void setRequestedBy(String requestedBy) {
		this.requestedBy = requestedBy;
	}
	
	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getOther() {
		return other;
	}

	public void setOther(String other) {
		this.other = other;
	}
	

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	@Override
	public String toString() {
		return "BatchInfo [batchDescription=" + batchDescription
				+ ", batchName=" + batchName + ", batchComment=" + batchComment
				+ ", orderSource=" + orderSource + ", requestedBy="
				+ requestedBy + ", prefix=" + prefix + ", other=" + other
				+ ", quantity=" + quantity + "]";
	}

}
