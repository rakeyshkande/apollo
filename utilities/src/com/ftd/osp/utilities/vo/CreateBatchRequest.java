package com.ftd.osp.utilities.vo;

import java.io.Serializable;
import java.util.List;

public class CreateBatchRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2878336370199506460L;
	private CreateRequest createDetails;
	private List<RecipientInfo> recipientInfo;
	public CreateBatchRequest(){
		super();
	}
	public CreateBatchRequest(CreateRequest createDetails,
			List<RecipientInfo> recipientInfo) {
		super();
		this.createDetails = createDetails;
		this.recipientInfo = recipientInfo;
	}
	public CreateRequest getCreateDetails() {
		return createDetails;
	}
	public void setCreateDetails(CreateRequest createDetails) {
		this.createDetails = createDetails;
	}
	public List<RecipientInfo> getRecipientInfo() {
		return recipientInfo;
	}
	public void setRecipientInfo(List<RecipientInfo> recipientInfo) {
		this.recipientInfo = recipientInfo;
	}
	@Override
	public String toString() {
		return "CreateBatch [createDetails=" + createDetails
				+ ", recipientInfo=" + recipientInfo + "]";
	}	
}
