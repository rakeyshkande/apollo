package com.ftd.osp.utilities.vo;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import org.codehaus.jackson.map.ObjectMapper;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * This class is used to detokenize and get credit card information.
 *
 */
public class PaymentServiceRequest {

	private static Logger logger= new Logger("com.ftd.osp.utilities.vo.PaymentServiceRequest");
	private static final int SUCCESS_RESPONSE_CODE = 200;
	private static final String DETOKENIZATION_SERVICE_URL = "PAYMENT_GATEWAY_DETOKENIZATION_URL";
	
	public static PaymentDetokenizationResponse getCreditCardsFromToken(String tokenId, String merchantReferenceId) throws Exception {
		String response = sendGet(tokenId, merchantReferenceId);
		PaymentDetokenizationResponse creditCard = convertToRetrieveSingleObject(response);

		return creditCard;
	}

	private static String sendGet(String tokenId, String merchantReferenceId) throws Exception {
		logger.info("sendGet() to Detokenization started with tokenId: " + tokenId + ", merchantReferenceId: " + merchantReferenceId);
		ConfigurationUtil configurationUtil = ConfigurationUtil.getInstance();

		StringBuffer detokenizationURL = new StringBuffer(configurationUtil.getFrpGlobalParm("SERVICE", DETOKENIZATION_SERVICE_URL));
		//StringBuffer detokenizationURL = new StringBuffer("https://nonprod-gke-primary-1-proxy-dev2.gcp.ftdi.com/payment-gateway-service/FTD/api/payment/tokenize/get/");
		detokenizationURL.append(tokenId).append("/");
		detokenizationURL.append(merchantReferenceId);

		URL obj = new URL(detokenizationURL.toString());
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		int responseCode = con.getResponseCode();
		if (responseCode != SUCCESS_RESPONSE_CODE) {
			logger.error("Error while calling the token service");
		}

		logger.info("\nSending 'GET' request to Detokenization service : " + detokenizationURL.toString());
		logger.info("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();
		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
		con.disconnect();

		logger.info("Response : " + response.toString().replaceAll("\\d{12,19}", "XXXX"));

		return response.toString();
	}

	/**
	 * Converts Response String into Response Object
	 * @param jsonString
	 * @return RetrieveGiftCodeResponseVO
	 */
	public static PaymentDetokenizationResponse convertToRetrieveSingleObject(String jsonString) {
		logger.info("convertToRetrieveSingleObject() started  with input: " + jsonString.replaceAll("\\d{12,19}", "XXXX"));

		PaymentDetokenizationResponse paymentResponse = new PaymentDetokenizationResponse();
		ObjectMapper mapperObj = new ObjectMapper();
		try {
			paymentResponse = mapperObj.readValue(jsonString, PaymentDetokenizationResponse.class);
		} catch (IOException ex) {
			logger.info("Error while parsing response : " + ex.getMessage());
		}

		if(paymentResponse != null){
			logger.info("paymentResponse: " + paymentResponse.toString());
		}
		
		return paymentResponse;
	}

}
