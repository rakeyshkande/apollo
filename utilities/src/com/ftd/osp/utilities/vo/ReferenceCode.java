package com.ftd.osp.utilities.vo;

import java.io.Serializable;

public class ReferenceCode implements Serializable{
	private static final long serialVersionUID = 8494946998432022532L;

	private String refCode;

	public ReferenceCode(){
		
	}
	public ReferenceCode(String refCode) {
		super();
		this.refCode = refCode;
	}
	public String getRefCode() {
		return refCode;
	}
	public void setRefCode(String refCode) {
		this.refCode = refCode;
	}
	@Override
	public String toString() {
		return "ReferenceCode [refCode=" + refCode + "]";
	}
	
}
