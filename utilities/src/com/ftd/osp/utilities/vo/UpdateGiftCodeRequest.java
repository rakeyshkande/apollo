package com.ftd.osp.utilities.vo;

import java.math.BigDecimal;
import java.util.List;

public class UpdateGiftCodeRequest {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5387252491023199431L;
	private String giftCodeId;
	private String status;
	private BigDecimal totalRedemptionAmount;
	private String updatedBy;
	private List<RedemptionDetails> redemptionDetails;
	private String source;

	public UpdateGiftCodeRequest() {
		super();
	}

	public UpdateGiftCodeRequest(String giftCodeId, String status, BigDecimal totalRedemptionAmount, String updatedBy, List<RedemptionDetails> lRedemptionDetails, String source) {
		super();
		this.giftCodeId = giftCodeId;
		this.status = status;
		this.totalRedemptionAmount = totalRedemptionAmount;
		this.updatedBy = updatedBy;
		this.redemptionDetails = lRedemptionDetails;
		this.source = source;
	}

	public String getGiftCodeId() {
		return giftCodeId;
	}

	public void setGiftCodeId(String giftCodeId) {
		this.giftCodeId = giftCodeId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<RedemptionDetails> getRedemptionDetails() {
		return redemptionDetails;
	}
	public void setRedemptionDetails(List<RedemptionDetails> lRedemptionDetails) {
		this.redemptionDetails = lRedemptionDetails;
	}
	
	/**
	 * @return the totalRedemptionAmount
	 */
	public BigDecimal getTotalRedemptionAmount() {
		return totalRedemptionAmount;
	}

	/**
	 * @param totalRedemptionAmount the totalRedemptionAmount to set
	 */
	public void setTotalRedemptionAmount(BigDecimal totalRedemptionAmount) {
		this.totalRedemptionAmount = totalRedemptionAmount;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	@Override
	public String toString() {
		return "UpdateGiftCodeRequest [giftCodeId=" + giftCodeId + ", status="
				+ status + ", totalRedemptionAmount=" + totalRedemptionAmount
				+ ", updatedBy=" + updatedBy + ", redemptionDetails="
				+ redemptionDetails + ", source=" + source + "]";
	}
}