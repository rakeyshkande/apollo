package com.ftd.osp.utilities.vo;

import java.io.Serializable;

public class Meta implements Serializable {
	private static final long serialVersionUID = -766706423705186928L;

	private String createdBy;
	private String updatedBy;
	private String requestedBy;
	
	public Meta(){
		
	}

	public Meta(String createdBy, String updatedBy, String requestedBy) {
		super();
		this.createdBy = createdBy;
		this.updatedBy = updatedBy;
		this.requestedBy = requestedBy;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	

	public String getRequestedBy() {
		return requestedBy;
	}

	public void setRequestedBy(String requestedBy) {
		this.requestedBy = requestedBy;
	}

	@Override
	public String toString() {
		return "Meta [createdBy=" + createdBy + ", updatedBy=" + updatedBy
				+ ", requestedBy=" + requestedBy + "]";
	}

}
