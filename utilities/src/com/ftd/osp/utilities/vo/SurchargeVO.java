package com.ftd.osp.utilities.vo;

public class SurchargeVO
{

    private double surchargeAmount = 0;
    private String surchargeDescription = "";
    private String applySurchargeCode = "OFF";
    private String displaySurcharge = "N";
    private boolean applySurchargeFlag = false;
    private String sendSurchargeToFlorist = "N";

    
  public SurchargeVO()
  {
  }

  public void setSurchargeAmount(double surchargeAmount)
  {
    this.surchargeAmount = surchargeAmount;
  }

  public double getSurchargeAmount()
  {
    return surchargeAmount;
  }

  public void setSurchargeDescription(String surchargeDescription)
  {
    this.surchargeDescription = surchargeDescription;
  }

  public String getSurchargeDescription()
  {
    return surchargeDescription;
  }


  public void setApplySurchargeCode(String applySurchargeCode)
  {
    this.applySurchargeCode = applySurchargeCode;
  }

  public String getApplySurchargeCode()
  {
    return applySurchargeCode;
  }


  public void setSendSurchargeToFlorist(String sendSurchargeToFlorist)
  {
    this.sendSurchargeToFlorist = sendSurchargeToFlorist;
  }

  public String getSendSurchargeToFlorist()
  {
    return sendSurchargeToFlorist;
  }


  public void setApplySurchargeFlag(boolean applySurchargeFlag)
  {
    this.applySurchargeFlag = applySurchargeFlag;
  }

  public boolean isApplySurchargeFlag()
  {
    return applySurchargeFlag;
  }

  public void setDisplaySurcharge(String displaySurcharge)
  {
    this.displaySurcharge = displaySurcharge;
  }

  public String getDisplaySurcharge()
  {
    return displaySurcharge;
  }
}
