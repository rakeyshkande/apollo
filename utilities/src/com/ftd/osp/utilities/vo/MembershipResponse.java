package com.ftd.osp.utilities.vo;

import java.util.List;

public class MembershipResponse {
	
	String membershipEmail;
	
	MembershipData data;
	
	List<Errors> errors;

	public String getMembershipEmail() {
		return membershipEmail;
	}

	public void setMembershipEmail(String membershipEmail) {
		this.membershipEmail = membershipEmail;
	}

	public MembershipData getData() {
		return data;
	}

	public void setData(MembershipData data) {
		this.data = data;
	}

	public List<Errors> getErrors() {
		return errors;
	}

	public void setErrors(List<Errors> errors) {
		this.errors = errors;
	}

	@Override
	public String toString() {
		return "MembershipFields [membershipEmail=" + membershipEmail
				+ ", data=" + data + ", errors=" + errors + "]";
	}	
	
}
