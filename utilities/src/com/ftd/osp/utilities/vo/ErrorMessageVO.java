package com.ftd.osp.utilities.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author smeka
 *
 */
@XmlRootElement(name="error")
@XmlAccessorType(XmlAccessType.FIELD)
public class ErrorMessageVO {
	
	@XmlAttribute(name="code")
	private String errorCode;
	
	@XmlAttribute(name="message")
	private String errorMessage;
	
	@XmlAttribute
	private String severity;

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getSeverity() {
		return severity;
	}

	public void setSeverity(String severity) {
		this.severity = severity;
	}
	
}
