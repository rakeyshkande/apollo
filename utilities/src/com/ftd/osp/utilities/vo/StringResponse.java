package com.ftd.osp.utilities.vo;

import java.io.Serializable;

public class StringResponse implements Serializable{
	 /**
	 * 
	 */
	private static final long serialVersionUID = -806777357071364666L;
	private String response;
	public StringResponse(String response) {
		super();
		this.response = response;
	}
	
	public StringResponse(){
		
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}
}
