package com.ftd.osp.utilities.vo;
import java.sql.Connection;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class BusinessConfig  {
  private List outboundMessageTokens;
  private MessageToken inboundMessageToken;
  private Connection connection;

  /**
   * Constructor
   */
  public BusinessConfig() {
    outboundMessageTokens = new ArrayList();
  }

  /**
   * Returns the inbound message token
   */
  public MessageToken getInboundMessageToken(){
    return inboundMessageToken;
  }

  /**
   * Sets the inbound message token
   */
   public void setInboundMessageToken(MessageToken newMessageToken){
     inboundMessageToken = newMessageToken;
   }
   
  /**
   * Returns an iterator to the list of outbound message tokens
   * 
   */
  public Iterator getOutboundMessageTokens() {
    return outboundMessageTokens.iterator();
  }

  /**
   * Sets the message token in the list of outbound message tokens
   */
  public void setOutboundMessageToken(MessageToken newMessageToken) {
    outboundMessageTokens.add(newMessageToken);
  }

  /**
   * Returns a connection to the database, as specified in the ejb-jar.xml
   */
  public Connection getConnection() {
    return connection;
  }

  /**
   * Sets a connection to the database, as specified in the ejb-jar.xml
   */
  public void setConnection(Connection newConnection) {
    connection = newConnection;
  }
}