package com.ftd.osp.utilities.vo;

import java.io.File;
import java.io.Serializable;
import java.sql.Connection;

import com.ftd.ftdutilities.GeneralConstants;

import java.util.List;

public class NotificationVO implements Serializable  {

  // Initialize attributes to empty string since not all set methods are 
  // invoked before use (and we don't want any NullPointExceptions now do we?)
  private String SMTPHost = "";
  private String messageContent = "";
  private String messageSubject = "";
  private String messageMimeType = "";
  private String messageFromAddress = "";
  private String messageTOAddress = "";
  private String messageCCAddress = "";
  private String messageBCCAddress = "";
  private String addressSeparator = GeneralConstants.EMAIL_ADDRESS_SEPARATOR;
  private File fileAttachment;
  private List fileAttachments;
  private String messageContentHTML;
  private Connection connection = null;

    /**
     * The default constructor
     */
  public NotificationVO() {
    super();
  }

  public String getSMTPHost() {
    return SMTPHost;
  }

  public void setSMTPHost(String newSMTPHost) {
    SMTPHost = trim(newSMTPHost);
  }

  public String getMessageContent() {
    return messageContent;
  }

  public void setMessageContent(String newMessageContent) {
    messageContent = trim(newMessageContent);
  }

  public String getMessageSubject() {
    return messageSubject;
  }

  public void setMessageSubject(String newMessageSubject) {
    messageSubject = trim(newMessageSubject);
  }

  public String getMessageMimeType() {
    return messageMimeType;
  }

  public void setMessageMimeType(String newMessageMimeType) {
    messageMimeType = trim(newMessageMimeType);
  }

  public String getMessageFromAddress() {
    return messageFromAddress;
  }

  public void setMessageFromAddress(String newMessageFromAddress) {
    messageFromAddress = trim(newMessageFromAddress);
  }

  public String getMessageTOAddress() {
    return messageTOAddress;
  }

  public void setMessageTOAddress(String newMessageTOAddress) {
    messageTOAddress = trim(newMessageTOAddress);
  }

  public String getMessageCCAddress() {
    return messageCCAddress;
  }

  public void setMessageCCAddress(String newMessageCCAddress) {
    messageCCAddress = trim(newMessageCCAddress);
  }

  public String getMessageBCCAddress() {
    return messageBCCAddress;
  }

  public void setMessageBCCAddress(String newMessageBCCAddress) {
    messageBCCAddress = trim(newMessageBCCAddress);
  }

  public String getAddressSeparator() {
    return addressSeparator;
  }

  public void setAddressSeparator(String newAddressSeparator) {
    addressSeparator = trim(newAddressSeparator);
  }

  public File getFileAttachment() {
    return fileAttachment;
  }

  public void setFileAttachment(File newFileAttachment) {
    fileAttachment = newFileAttachment;
  }


  public boolean hasAttachment(){
    if (fileAttachment != null 
        || fileAttachments != null)  {
        return true;
    }
    return false;
  }

  public List getFileAttachments() {
    return fileAttachments;
  }

  public void setFileAttachments(List newFileAttachments) {
    fileAttachments = newFileAttachments;
  }

  public String getMessageContentHTML()
  {
    return messageContentHTML;
  }

  public void setMessageContentHTML(String newMessageContentHTML)
  {
    messageContentHTML = trim(newMessageContentHTML);
  }

  public void setConnection(Connection connection) 
  {
    this.connection = connection;
  }

  public Connection getConnection() 
  {
    return connection;
  }

  private String trim(String str)
  {
    return (str != null)?str.trim():str;
  }

}