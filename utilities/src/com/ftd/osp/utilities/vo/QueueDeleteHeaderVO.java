package com.ftd.osp.utilities.vo;


import java.io.Serializable;

import java.math.BigDecimal;

import java.util.Date;
import java.util.List;
/**
 * Value object for queue delete header info
 * 
 * Version History
 * @author           @Version      @Date                      @Comments
 * Swamy Patsa           1.0       11/29/2010           Initial Version.
 * Swamy Patsa           1.1       12/07/2010           Added emailSubject and emailBody.
 * 
 */
public class QueueDeleteHeaderVO implements Serializable
{

  private Long queueDeleteHeaderId;
  private String batchDescription;
  private Integer batchCount;
  private String batchProcessedFlag;
  private String deleteQueueMsgFlag;
  private String deleteQueueMsgType;
  private String sendMessageFlag;
  private String messageType;
  private BigDecimal anspAmount;
  private String giveFloristAskingPrice;
  private String cancelReasonCode;
  private String textOrReason;
  private String resubmitOrderFlag;
  private String includeCommentFlag;
  private String commentText;
  private String sendEmailFlag;
  private String emailSubject;
  private String emailBody;
  private String emailTitle;
  private String refundOrderFlag;
  private String refundDispCode;
  private String responsibleParty;
  private String deleteBatchByType;
  private String batchComment;
  private String includePartnerName;
  private String excludePartnerName;

  private Date createdOn;
  private String createdBy;
  private Date updatedOn;
  private String updatedBy;
  private List<QueueDeleteDetailVO> queueDeleteDetailVOList;


  public void setQueueDeleteHeaderId(Long queueDeleteHeaderId)
  {
    this.queueDeleteHeaderId = queueDeleteHeaderId;
  }

  public Long getQueueDeleteHeaderId()
  {
    return queueDeleteHeaderId;
  }

  public void setBatchDescription(String batchDescription)
  {
    this.batchDescription = batchDescription;
  }

  public String getBatchDescription()
  {
    return batchDescription;
  }

  public void setBatchCount(Integer batchCount)
  {
    this.batchCount = batchCount;
  }

  public Integer getBatchCount()
  {
    return batchCount;
  }

  public void setBatchProcessedFlag(String batchProcessedFlag)
  {
    this.batchProcessedFlag = batchProcessedFlag;
  }

  public String getBatchProcessedFlag()
  {
    return batchProcessedFlag;
  }

  public void setDeleteQueueMsgFlag(String deleteQueueMsgFlag)
  {
    this.deleteQueueMsgFlag = deleteQueueMsgFlag;
  }

  public String getDeleteQueueMsgFlag()
  {
    return deleteQueueMsgFlag;
  }

  public void setDeleteQueueMsgType(String deleteQueueMsgType)
  {
    this.deleteQueueMsgType = deleteQueueMsgType;
  }

  public String getDeleteQueueMsgType()
  {
    return deleteQueueMsgType;
  }

  public void setSendMessageFlag(String sendMessageFlag)
  {
    this.sendMessageFlag = sendMessageFlag;
  }

  public String getSendMessageFlag()
  {
    return sendMessageFlag;
  }

  public void setMessageType(String messageType)
  {
    this.messageType = messageType;
  }

  public String getMessageType()
  {
    return messageType;
  }

  public void setAnspAmount(BigDecimal anspAmount)
  {
    this.anspAmount = anspAmount;
  }

  public BigDecimal getAnspAmount()
  {
    return anspAmount;
  }

  public void setGiveFloristAskingPrice(String giveFloristAskingPrice)
  {
    this.giveFloristAskingPrice = giveFloristAskingPrice;
  }

  public String getGiveFloristAskingPrice()
  {
    return giveFloristAskingPrice;
  }

  public void setCancelReasonCode(String cancelReasonCode)
  {
    this.cancelReasonCode = cancelReasonCode;
  }

  public String getCancelReasonCode()
  {
    return cancelReasonCode;
  }

  public void setTextOrReason(String textOrReason)
  {
    this.textOrReason = textOrReason;
  }

  public String getTextOrReason()
  {
    return textOrReason;
  }

  public void setResubmitOrderFlag(String resubmitOrderFlag)
  {
    this.resubmitOrderFlag = resubmitOrderFlag;
  }

  public String getResubmitOrderFlag()
  {
    return resubmitOrderFlag;
  }

  public void setIncludeCommentFlag(String includeCommentFlag)
  {
    this.includeCommentFlag = includeCommentFlag;
  }

  public String getIncludeCommentFlag()
  {
    return includeCommentFlag;
  }

  public void setCommentText(String commentText)
  {
    this.commentText = commentText;
  }

  public String getCommentText()
  {
    return commentText;
  }

  public void setSendEmailFlag(String sendEmailFlag)
  {
    this.sendEmailFlag = sendEmailFlag;
  }

  public String getSendEmailFlag()
  {
    return sendEmailFlag;
  }

  public void setEmailTitle(String emailTitle)
  {
    this.emailTitle = emailTitle;
  }

  public String getEmailTitle()
  {
    return emailTitle;
  }

  public void setRefundOrderFlag(String refundOrderFlag)
  {
    this.refundOrderFlag = refundOrderFlag;
  }

  public String getRefundOrderFlag()
  {
    return refundOrderFlag;
  }

  public void setRefundDispCode(String refundDispCode)
  {
    this.refundDispCode = refundDispCode;
  }

  public String getRefundDispCode()
  {
    return refundDispCode;
  }

  public void setResponsibleParty(String responsibleParty)
  {
    this.responsibleParty = responsibleParty;
  }

  public String getResponsibleParty()
  {
    return responsibleParty;
  }

  public void setDeleteBatchByType(String deleteBatchByType)
  {
    this.deleteBatchByType = deleteBatchByType;
  }

  public String getDeleteBatchByType()
  {
    return deleteBatchByType;
  }

  public void setBatchComment(String batchComment)
  {
    this.batchComment = batchComment;
  }

  public String getBatchComment()
  {
    return batchComment;
  }

  public void setIncludePartnerName(String includePartnerName)
  {
    this.includePartnerName = includePartnerName;
  }

  public String getIncludePartnerName()
  {
    return includePartnerName;
  }

   public void setCreatedOn(Date createdOn)
  {
    this.createdOn = createdOn;
  }

  public Date getCreatedOn()
  {
    return createdOn;
  }

  public void setCreatedBy(String createdBy)
  {
    this.createdBy = createdBy;
  }

  public String getCreatedBy()
  {
    return createdBy;
  }

  public void setUpdatedOn(Date updatedOn)
  {
    this.updatedOn = updatedOn;
  }

  public Date getUpdatedOn()
  {
    return updatedOn;
  }

  public void setUpdatedBy(String updatedBy)
  {
    this.updatedBy = updatedBy;
  }

  public String getUpdatedBy()
  {
    return updatedBy;
  }

  public void setQueueDeleteDetailVOList(List<QueueDeleteDetailVO> queueDeleteDetailVOList)
  {
    this.queueDeleteDetailVOList = queueDeleteDetailVOList;
  }

  public List<QueueDeleteDetailVO> getQueueDeleteDetailVOList()
  {
    return queueDeleteDetailVOList;
  }

  public void setExcludePartnerName(String excludePartnerName)
  {
    this.excludePartnerName = excludePartnerName;
  }

  public String getExcludePartnerName()
  {
    return excludePartnerName;
  }


  public void setEmailSubject(String emailSubject)
  {
    this.emailSubject = emailSubject;
  }

  public String getEmailSubject()
  {
    return emailSubject;
  }


  public void setEmailBody(String emailBody)
  {
    this.emailBody = emailBody;
  }

  public String getEmailBody()
  {
    return emailBody;
  }
}
