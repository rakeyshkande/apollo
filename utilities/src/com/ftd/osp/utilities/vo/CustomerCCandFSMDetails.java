package com.ftd.osp.utilities.vo;

import java.util.List;
import java.util.Map;

public class CustomerCCandFSMDetails {
	
	public List<ProfileCCData> creditCards;
	public MembershipData fsmDetails;
	
	Map<String, String> errorMap;
	String timeStamp;
	String errorType;
	String status;
	String exception;
	
	public List<ProfileCCData> getCreditCards() {
		return creditCards;
	}
	public void setCreditCards(List<ProfileCCData> creditCards) {
		this.creditCards = creditCards;
	}
	public MembershipData getFsmDetails() {
		return fsmDetails;
	}
	public void setFsmDetails(MembershipData fsmDetails) {
		this.fsmDetails = fsmDetails;
	}
	public Map<String, String> getErrorMap() {
		return errorMap;
	}
	public void setErrorMap(Map<String, String> errorMap) {
		this.errorMap = errorMap;
	}
	public String getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}
	public String getErrorType() {
		return errorType;
	}
	public void setErrorType(String errorType) {
		this.errorType = errorType;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getException() {
		return exception;
	}
	public void setException(String exception) {
		this.exception = exception;
	}
	
	
}
