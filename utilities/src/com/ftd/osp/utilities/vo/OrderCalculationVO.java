package com.ftd.osp.utilities.vo;

import java.math.BigDecimal;

import java.util.Date;

/**
 * This class models product index value object.
 */
public class OrderCalculationVO extends BaseVO {
    
    private String sourceCode;
    private Date deliveryDate;
    private Date orderDate;
    private String zipCode;
    private String countryId;
    private String snhId;
    private String shipMethod;
    private String productId;
    private String shippingKey;
    private String productType;
    private String orderOrigin;
    private BigDecimal productPrice;
    private BigDecimal serviceFee;
    private BigDecimal shippingFee;
    private BigDecimal alaskaHawaiiFee;
    private BigDecimal fuelSurcharge;

    public void setServiceFee(BigDecimal serviceFee) {
        this.serviceFee = serviceFee;
    }

    public BigDecimal getServiceFee() {
        return serviceFee;
    }

    public void setShippingFee(BigDecimal shippingFee) {
        this.shippingFee = shippingFee;
    }

    public BigDecimal getShippingFee() {
        return shippingFee;
    }

    public void setAlaskaHawaiiFee(BigDecimal alaskaHawaiiFee) {
        this.alaskaHawaiiFee = alaskaHawaiiFee;
    }

    public BigDecimal getAlaskaHawaiiFee() {
        return alaskaHawaiiFee;
    }

    public void setFuelSurcharge(BigDecimal fuelSurcharge) {
        this.fuelSurcharge = fuelSurcharge;
    }

    public BigDecimal getFuelSurcharge() {
        return fuelSurcharge;
    }

    public void setSourceCode(String sourceCode) {
        this.sourceCode = sourceCode;
    }

    public String getSourceCode() {
        return sourceCode;
    }

    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setSnhId(String snhId) {
        this.snhId = snhId;
    }

    public String getSnhId() {
        return snhId;
    }

    public void setShipMethod(String shipMethod) {
        this.shipMethod = shipMethod;
    }

    public String getShipMethod() {
        return shipMethod;
    }

    public void setShippingKey(String shippingKey) {
        this.shippingKey = shippingKey;
    }

    public String getShippingKey() {
        return shippingKey;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductPrice(BigDecimal productPrice) {
        this.productPrice = productPrice;
    }

    public BigDecimal getProductPrice() {
        return productPrice;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductId() {
        return productId;
    }

    public void setOrderOrigin(String orderOrigin) {
        this.orderOrigin = orderOrigin;
    }

    public String getOrderOrigin() {
        return orderOrigin;
    }
}
