package com.ftd.osp.utilities.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class ReinstateDetail implements Serializable{
	public ReinstateDetail() {
		super();
	}
	public ReinstateDetail(String reinstateRefNo, BigDecimal reinstateAmount, String reinstateDate) {
		super();
		this.reinstateRefNo = reinstateRefNo;
		this.reinstateAmount = reinstateAmount;
		this.reinstateDate = reinstateDate;
	}
	@Override
	public String toString() {
		return "ReinstateDetail [reinstateRefNo=" + reinstateRefNo + ", reinstateAmount=" + reinstateAmount
				+ ", reinstateDate=" + reinstateDate + "]";
	}
	public String getReinstateRefNo() {
		return reinstateRefNo;
	}
	public void setReinstateRefNo(String reinstateRefNo) {
		this.reinstateRefNo = reinstateRefNo;
	}
	public BigDecimal getReinstateAmount() {
		return reinstateAmount;
	}
	public void setReinstateAmount(BigDecimal reinstateAmount) {
		this.reinstateAmount = reinstateAmount;
	}
	public String getReinstateDate() {
		return reinstateDate;
	}
	public void setReinstateDate(String reinstateDate) {
		this.reinstateDate = reinstateDate;
	}
	/**
	 * 
	 */
	private static final long serialVersionUID = 1235484884430733462L;
	private String reinstateRefNo;
    private BigDecimal reinstateAmount=BigDecimal.ZERO;
    private String reinstateDate;
}
