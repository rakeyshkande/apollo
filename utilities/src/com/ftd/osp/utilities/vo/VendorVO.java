package com.ftd.osp.utilities.vo;


public class VendorVO
{

  private String vendorId;
  private String vendorName;
  private String vendorType;
  private String memberNumber;

  public VendorVO()
  {
  }


  public void setVendorId(String vendorId)
  {
    this.vendorId = vendorId;
  }

  public String getVendorId()
  {
    return vendorId;
  }

  public void setVendorName(String vendorName)
  {
    this.vendorName = vendorName;
  }

  public String getVendorName()
  {
    return vendorName;
  }

  public void setVendorType(String vendorType)
  {
    this.vendorType = vendorType;
  }

  public String getVendorType()
  {
    return vendorType;
  }

  public void setMemberNumber(String memberNumber)
  {
    this.memberNumber = memberNumber;
  }

  public String getMemberNumber()
  {
    return memberNumber;
  }
}
