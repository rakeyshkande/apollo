package com.ftd.osp.utilities.vo;

import com.ftd.ftdutilities.FieldUtils;
import com.ftd.ftdutilities.XMLInterface;

import java.io.StringReader;

import java.lang.reflect.Field;

import java.lang.reflect.Modifier;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import org.w3c.dom.Element;

import org.xml.sax.InputSource;

public class AddOnVO
{
    //Constants
    public static final String ADD_ON_VO_VASE_KEY = "vase";
    public static final String ADD_ON_VO_ADD_ON_KEY = "add_on";
    public static final String ADD_ON_VO_BANNER_KEY = "banner";
    public static final String ADD_ON_VO_CARD_KEY = "card";
    
    public static final Integer ADD_ON_TYPE_VASE_ID = 7;
    public static final Integer ADD_ON_TYPE_BANNER_ID = 3;
    public static final Integer ADD_ON_TYPE_CARD_ID = 4;

    //Attributes
    String  addOnId;
    String  addOnTypeId;
    String  addOnDescription;
    String  addOnTypeDescription;
    String  addOnText;
    String  addOnUNSPSC;
    String  addOnPrice;
    String  addOnWeight;
    String  addOnDeliveryType;
    String  productId;
    Boolean addOnAvailableFlag;
    Boolean addOnTypeIncludedInProductFeedFlag;
    Boolean defaultPerTypeFlag;
    Boolean displayPriceFlag;
    String  maxQuantity;
    String  displaySequenceNumber;
    Integer orderQuantity;
    String occasionId;
    String occasionDescription;
    HashMap <String,VendorAddOnVO> vendorCostsMap = new HashMap <String,VendorAddOnVO> ();
    HashSet <String> productIdSet = new HashSet <String> ();
    String sLinkedOccasions;// Changes for Apollo.Q3.2015 - Addon-Occasion association
    /*
     * DI-4: Add-on Dashboard Enhancements: Added 2 new attributes to add-on : PQUAD_ACCESSORY_ID, FTD_WEST_ADDON.
     * Updated AddOnVO with the new attributes.
     */
    String sPquadAccsryId;
    boolean isFtdWestAddon;

	public String getAddOnId()
    {
        return addOnId;
    }

    public void setAddOnId(String addOnId)
    {
        this.addOnId = addOnId;
    }

    public String getAddOnTypeId()
    {
        return addOnTypeId;
    }

    public void setAddOnTypeId(String addOnTypeId)
    {
        this.addOnTypeId = addOnTypeId;
    }

    public String getAddOnDescription()
    {
        return addOnDescription;
    }

    public void setAddOnDescription(String addOnDescription)
    {
        this.addOnDescription = addOnDescription;
    }

    public String getAddOnPrice()
    {
        return addOnPrice;
    }

    public void setAddOnPrice(String addOnPrice)
    {
        this.addOnPrice = addOnPrice;
    }

    public Boolean getAddOnAvailableFlag()
    {
        return addOnAvailableFlag;
    }

    public void setAddOnAvailableFlag(Boolean addOnAvailableFlag)
    {
        this.addOnAvailableFlag = addOnAvailableFlag;
    }

    public Boolean getAddOnTypeIncludedInProductFeedFlag()
    {
        return addOnTypeIncludedInProductFeedFlag;
    }

    public void setAddOnTypeIncludedInProductFeedFlag(Boolean addOnTypeIncludedInProductFeedFlag)
    {
        this.addOnTypeIncludedInProductFeedFlag = addOnTypeIncludedInProductFeedFlag;
    }
    public String getMaxQuantity()
    {
        return maxQuantity;
    }

    public void setMaxQuantity(String maxQuantity)
    {
        this.maxQuantity = maxQuantity;
    }

    public String getDisplaySequenceNumber()
    {
        return displaySequenceNumber;
    }

    public void setDisplaySequenceNumber(String displaySequenceNumber)
    {
        this.displaySequenceNumber = displaySequenceNumber;
    }

    public String getAddOnTypeDescription()
    {
        return addOnTypeDescription;
    }

    public void setAddOnTypeDescription(String addOnTypeDescription)
    {
        this.addOnTypeDescription = addOnTypeDescription;
    }

    public Integer getOrderQuantity()
    {
        return orderQuantity;
    }

    public void setOrderQuantity(Integer orderQuantity)
    {
        this.orderQuantity = orderQuantity;
    }

    public HashMap<String, VendorAddOnVO> getVendorCostsMap()
    {
        return vendorCostsMap;
    }

    public void setVendorCostsMap(HashMap<String, VendorAddOnVO> vendorCostsMap)
    {
        this.vendorCostsMap = vendorCostsMap;
    }

    public Boolean getDisplayPriceFlag()
    {
        return displayPriceFlag;
    }

    public void setDisplayPriceFlag(Boolean displayPriceFlag)
    {
        this.displayPriceFlag = displayPriceFlag;
    }

    public Boolean getDefaultPerTypeFlag()
    {
        return defaultPerTypeFlag;
    }

    public void setDefaultPerTypeFlag(Boolean defaultPerTypeFlag)
    {
        this.defaultPerTypeFlag = defaultPerTypeFlag;
    }
    public String toXML(int index) throws Exception
    {
      StringBuilder sb = new StringBuilder();

    sb.append("<AddOnVO num = " + '"' + index + '"' + ">");
    Field[] fields = this.getClass().getDeclaredFields();

    for (int i = 0; i < fields.length; i++)
    {
        if (!Modifier.isStatic(fields[i].getModifiers()) && !fields[i].getType().equals(Class.forName("java.util.HashMap")) && !fields[i].getType().equals(Class.forName("java.util.HashSet")))
        {
            sb.append("<" + fields[i].getName() + ">");
            if (fields[i].get(this) != null)
            {
                StringBuilder element = new StringBuilder();
                element.append(fields[i].get(this));
                sb.append(fixHTMLEncoding(element.toString()));
            }
            sb.append("</" + fields[i].getName() + ">");
        }
        else if (StringUtils.equals(fields[i].getName(), "vendorCostsMap")) 
        {
            HashMap <String,VendorAddOnVO> vendorAddOnMap = (HashMap <String,VendorAddOnVO>) fields[i].get(this);
            if (vendorAddOnMap != null)
            {
                for (String key : vendorAddOnMap.keySet())
                {
                    VendorAddOnVO VendorAddOnVO = vendorAddOnMap.get(key);
                    String sXmlVO = VendorAddOnVO.toXML();
                    sb.append(sXmlVO);
                }
            }
        }        
    }
    sb.append("</AddOnVO>");
    
    return sb.toString();
  }

    public String getAddOnText()
    {
        return addOnText;
    }

    public void setAddOnText(String addOnText)
    {
        this.addOnText = addOnText;
    }

    public String getAddOnUNSPSC()
    {
        return addOnUNSPSC;
    }

    public void setAddOnUNSPSC(String addOnUNSPSC)
    {
        this.addOnUNSPSC = addOnUNSPSC;
    }

    public HashSet<String> getProductIdSet()
    {
        return productIdSet;
    }

    public void setProductIdSet(HashSet<String> productIdSet)
    {
        this.productIdSet = productIdSet;
    }

  public void setProductId(String productId)
  {
    this.productId = productId;
  }

  public String getProductId()
  {
    return productId;
  }

    public String getAddOnWeight()
    {
        return addOnWeight;
    }

    public void setAddOnWeight(String addOnWeight)
    {
        this.addOnWeight = addOnWeight;
    }
    
    /* #552 - FFlexible Fullfillment - Addon Dash board and Maintanance changes 
    added the getter and setter for the addon delivery type to fetch/set  the desired delivery type.
    */
    public void setAddOnDeliveryType(String addOnDeliveryType)
    {
        this.addOnDeliveryType = addOnDeliveryType;
    }
    
    public String getDeliveryType()
    {
      return addOnDeliveryType;
    }
    
    private static String fixHTMLEncoding(String before)
    {
        String fixed = FieldUtils.replaceAll(before, "&", "&amp;");
        fixed = FieldUtils.replaceAll(fixed, "<", "&lt;");
        fixed = FieldUtils.replaceAll(fixed, ">", "&gt;");
        fixed = FieldUtils.replaceAll(fixed, "'", "&apos;");
        fixed = FieldUtils.replaceAll(fixed, "\"", "&quot;");
        return fixed;
    }

    public void setOccasionId(String occasionId) {
        this.occasionId = occasionId;
    }

    public String getOccasionId() {
        return occasionId;
    }

    public void setOccasionDescription(String occasionDescription) {
        this.occasionDescription = occasionDescription;
    }

    public String getOccasionDescription() {
        return occasionDescription;
    }
    // Changes for Apollo.Q3.2015 - Addon-Occasion association
    public String getsLinkedOccasions() {
		return sLinkedOccasions;
	}

	public void setsLinkedOccasions(String sLinkedOccasions) {
		this.sLinkedOccasions = sLinkedOccasions;
	}
	
	/*
	 * DI-4: Add-on Dashboard Enhancements: Added 2 new attributes to add-on : PQUAD_ACCESSORY_ID, FTD_WEST_ADDON.
	 * Getter and setter methods for the same.
	 */
	public String getsPquadAccsryId() {
		return sPquadAccsryId;
	}

	public void setsPquadAccsryId(String sPquadAccsryId) {
		this.sPquadAccsryId = sPquadAccsryId;
	}

	public boolean isFtdWestAddon() {
		return isFtdWestAddon;
	}

	public void setFtdWestAddon(boolean isFtdWestAddon) {
		this.isFtdWestAddon = isFtdWestAddon;
	}

}
