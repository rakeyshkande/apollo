package com.ftd.osp.utilities.vo;

import java.io.Serializable;

public class FileFormat implements Serializable {
	private static final long serialVersionUID = 8832578217294655042L;
	private String fileName;
	private String format;
	
	public FileFormat(){
		
	}
	public FileFormat(String fileName, String format) {
		super();
		this.fileName = fileName;
		this.format = format;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getFormat() {
		return format;
	}
	public void setFormat(String format) {
		this.format = format;
	}
	@Override
	public String toString() {
		return "FileFormat [fileName=" + fileName + ", format=" + format + "]";
	}
}
