package com.ftd.osp.utilities.vo;

import java.io.Serializable;

public class GiftCodeUpdateBulkResp implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6672851210248627701L;
	private String response;
	private Integer giftCertCount;

	public GiftCodeUpdateBulkResp() {

	}

	public GiftCodeUpdateBulkResp(String response, Integer giftCertCount) {
		super();
		this.response = response;
		this.giftCertCount = giftCertCount;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public Integer getGiftCertCount() {
		return giftCertCount;
	}

	public void setGiftCertCount(Integer giftCertCount) {
		this.giftCertCount = giftCertCount;
	}

	@Override
	public String toString() {
		return "GiftCodeUpdateBatchResponseVO [response=" + response
				+ ", giftCertCount=" + giftCertCount + "]";
	}

}
