/**
 * 
 */
package com.ftd.osp.utilities.vo;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author smeka
 * 
 */
@XmlRootElement(name = "CustomerServiceResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class CustomerServiceResponse {

	List<MembershipDetailsVO> customerMemberships;

	List<ErrorMessageVO> errors;

	public List<MembershipDetailsVO> getCustomerMemberships() {
		if (customerMemberships == null) {
			customerMemberships = new ArrayList<MembershipDetailsVO>();
		}
		return customerMemberships;
	}

	public void setCustomerMemberships(
			List<MembershipDetailsVO> customerMemberships) {
		this.customerMemberships = customerMemberships;
	}

	public List<ErrorMessageVO> getErrors() {
		if (errors == null) {
			errors = new ArrayList<ErrorMessageVO>();
		}
		return errors;
	}

	public void setErrors(List<ErrorMessageVO> errors) {
		this.errors = errors;
	}

	/**
	 * Unmarshals the Object passed in as input parameter.
	 * 
	 * @param inStream
	 * @return CustomerServiceResponse
	 * @throws Exception
	 */
	public static CustomerServiceResponse unMarshall(InputStream inStream)
			throws Exception {
		CustomerServiceResponse custServiceResp = null;
		try {
			JAXBContext jaxbContext = JAXBContext
					.newInstance(CustomerServiceResponse.class);
			Unmarshaller unMarshaller = jaxbContext.createUnmarshaller();

			custServiceResp = (CustomerServiceResponse) unMarshaller
					.unmarshal(inStream);

		} catch (JAXBException e) {
			throw new Exception(e);
		}
		return custServiceResp;
	}
}
