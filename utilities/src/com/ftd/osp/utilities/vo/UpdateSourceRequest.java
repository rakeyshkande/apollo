package com.ftd.osp.utilities.vo;

import java.io.Serializable;
import java.util.List;

public class UpdateSourceRequest implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8264793142222088425L;
	private String giftCodeId;
	private String batchNumber;
	private List<ReferenceCode> source;
	private String updatedBy;

	public UpdateSourceRequest() {
		super();
	}

	public UpdateSourceRequest(String batchNumber, List<ReferenceCode> source,
			String updatedBy, String giftCodeId) {
		super();
		this.batchNumber = batchNumber;
		this.source = source;
		this.updatedBy = updatedBy;
		this.giftCodeId = giftCodeId;
	}

	public String getBatchNumber() {
		return batchNumber;
	}

	public void setBatchNumber(String batchNumber) {
		this.batchNumber = batchNumber;
	}

	public List<ReferenceCode> getSource() {
		return source;
	}

	public void setSource(List<ReferenceCode> source) {
		this.source = source;
	}	
	
	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getGiftCodeId() {
		return giftCodeId;
	}

	public void setGiftCodeId(String giftCodeId) {
		this.giftCodeId = giftCodeId;
	}

	@Override
	public String toString() {
		return "UpdateSourceRequest [giftCodeId=" + giftCodeId
				+ ", batchNumber=" + batchNumber + ", source=" + source
				+ ", updatedBy=" + updatedBy + "]";
	}

}
