package com.ftd.osp.utilities.vo;


import java.io.Serializable;

import java.math.BigDecimal;
import java.util.List;


public class RetrieveGiftCodeResponse implements Serializable{
	
	public RetrieveGiftCodeResponse(String giftCodeId, String status, BigDecimal remainingAmount, Boolean isRestricted,
			String siteId, String expirationDate, Boolean isReedemable, List<RedemptionDetails> redemptionDetails,
			List<ReinstateDetail> reinstateDetails, BigDecimal issueAmount) {
		super();
		this.giftCodeId = giftCodeId;
		this.status = status;
		this.remainingAmount = remainingAmount;
		this.isRestricted = isRestricted;
		this.siteId = siteId;
		this.expirationDate = expirationDate;
		this.isReedemable = isReedemable;
		this.redemptionDetails = redemptionDetails;
		this.reinstateDetails = reinstateDetails;
		this.issueAmount = issueAmount;
	}

	private static final long serialVersionUID = 1235484885630733461L;
	
	private String giftCodeId;
	private String status;
	private BigDecimal remainingAmount;
	private Boolean isRestricted;
	private String siteId;
	private String expirationDate;
	private Boolean isReedemable;
	private List<RedemptionDetails> redemptionDetails;
	private List<ReinstateDetail> reinstateDetails;
	private BigDecimal issueAmount;
	
	public String getGiftCodeId() {
		return giftCodeId;
	}

	public void setGiftCodeId(String giftCodeId) {
		this.giftCodeId = giftCodeId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public BigDecimal getRemainingAmount() {
		return remainingAmount;
	}

	public void setRemainingAmount(BigDecimal remainingAmount) {
		this.remainingAmount = remainingAmount;
	}

	public Boolean getIsRestricted() {
		return isRestricted;
	}

	public void setIsRestricted(Boolean isRestricted) {
		this.isRestricted = isRestricted;
	}

	public String getSiteId() {
		return siteId;
	}

	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}

	public String getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(String expirationDt) {
		this.expirationDate = expirationDt;
	}

	public Boolean getIsReedemable() {
		return isReedemable;
	}

	public void setIsReedemable(Boolean isReedemable) {
		this.isReedemable = isReedemable;
	}

	public List<RedemptionDetails> getRedemptionDetails() {
		return redemptionDetails;
	}

	public void setRedemptionDetails(List<RedemptionDetails> lRedemptionDetails) {
		this.redemptionDetails = lRedemptionDetails;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

   
	public RetrieveGiftCodeResponse(){
		
	}
    

	public List<ReinstateDetail> getReinstateDetails() {
		return reinstateDetails;
	}

	public void setReinstateDetails(List<ReinstateDetail> reinstateDetails) {
		this.reinstateDetails = reinstateDetails;
	}

	public BigDecimal getIssueAmount() {
		return issueAmount;
	}

	public void setIssueAmount(BigDecimal issueAmount) {
		this.issueAmount = issueAmount;
	}

	@Override
	public String toString() {
		return "RetrieveGiftCodeResponse [giftCodeId=" + giftCodeId
				+ ", status=" + status + ", remainingAmount=" + remainingAmount
				+ ", isRestricted=" + isRestricted + ", siteId=" + siteId
				+ ", expirationDate=" + expirationDate + ", isReedemable="
				+ isReedemable + ", redemptionDetails=" + redemptionDetails
				+ ", reinstateDetails=" + reinstateDetails + ", issueAmount="
				+ issueAmount + "]";
	}
}