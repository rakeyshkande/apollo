package com.ftd.osp.utilities.vo;


import java.io.Serializable;
import java.math.BigDecimal;

public class ExtendedFloristAvailabilityResponse implements Serializable{
	
	private static final long serialVersionUID = 4535384885630733461L;
	

	private Boolean isAvailable;
	private BigDecimal shippingCharge;

		
	public ExtendedFloristAvailabilityResponse() {
		super();
		// TODO Auto-generated constructor stub
	}


	public ExtendedFloristAvailabilityResponse(Boolean isAvailable, BigDecimal shippingCharge) {
		super();
		this.isAvailable = isAvailable;
		this.shippingCharge = shippingCharge;
	}


	public Boolean getIsAvailable() {
		return isAvailable;
	}


	public void setIsAvailable(Boolean isAvailable) {
		this.isAvailable = isAvailable;
	}


	public BigDecimal getShippingCharge() {
		return shippingCharge;
	}


	public void setShippingCharge(BigDecimal shippingCharge) {
		this.shippingCharge = shippingCharge;
	}


	@Override
	public String toString() {
		return "ExtendedFloristAvailabilityResponse [isAvailable=" + isAvailable + ", shippingCharge=" + shippingCharge
				+ "]";
	}


	

}