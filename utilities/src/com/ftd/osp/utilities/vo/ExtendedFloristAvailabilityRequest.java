package com.ftd.osp.utilities.vo;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import com.ftd.osp.utilities.plugins.Logger;

public class ExtendedFloristAvailabilityRequest {

	/**
	 * 
	 */
	private static Logger logger = new Logger("com.ftd.osp.utilities.vo.ExtendedFloristAvailabilityRequest");
	@SuppressWarnings("unused")
	private static final long serialVersionUID = -3872524910243199431L;
	private String productId;
	private String zipCode;
	private String channel;
	private String category;
	private String partnerCode;
	private String sourceCode;

	public ExtendedFloristAvailabilityRequest() {
		super();
	}

	public ExtendedFloristAvailabilityRequest(String productId, String zipCode, String channel, String category, String partnerCode) {
		super();
		this.productId = productId;
		this.zipCode = zipCode;
		this.channel = channel;
		this.category = category;
		this.partnerCode = partnerCode;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}
	
	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getPartnerCode() {
		return partnerCode;
	}

	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}	

	public String getSourceCode() {
		return sourceCode;
	}

	public void setSourceCode(String sourceCode) {
		this.sourceCode = sourceCode;
	}

	@Override
	public String toString() {
		return "ExtendedFloristAvailabilityRequest [productId=" + productId + ", zipCode=" + zipCode + ", channel="
				+ channel + ", category=" + category + ", partnerCode=" + partnerCode
				+ ", sourceCode=" + sourceCode + "]";
	}

}