package com.ftd.osp.utilities.vo;

import java.io.Serializable;

public class RecipientInfo implements Serializable {
	private static final long serialVersionUID = 8596977232795718165L;
	private String firstName;

	private String lastName;

	private String address1;

	private String address2;

	private String city;

	private String state;

	private String zipcode;

	private String country;

	private String phone;

	private String email;
	
	private String issuedReferenceNumber;
	
	private String giftCodeId;
	
	public RecipientInfo(){
		
	}

	public RecipientInfo(String firstName, String lastName, String address1,
			String address2, String city, String state, String zipcode,
			String country, String phone, String email, String issuedReferenceNumber,
			String giftCodeId) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.address1 = address1;
		this.address2 = address2;
		this.city = city;
		this.state = state;
		this.zipcode = zipcode;
		this.country = country;
		this.phone = phone;
		this.email = email;
		this.issuedReferenceNumber = issuedReferenceNumber;
		this.giftCodeId = giftCodeId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getIssuedReferenceNumber() {
		return issuedReferenceNumber;
	}

	public void setIssuedReferenceNumber(String issuedReferenceNumber) {
		this.issuedReferenceNumber = issuedReferenceNumber;
	}

	@Override
	public String toString() {
		return "RecipientInfo [firstName=" + firstName + ", lastName="
				+ lastName + ", address1=" + address1 + ", address2="
				+ address2 + ", city=" + city + ", state=" + state
				+ ", zipcode=" + zipcode + ", country=" + country + ", phone="
				+ phone + ", email=" + email + ", issuedReferenceNumber="
				+ issuedReferenceNumber + ", giftCodeId=" + giftCodeId + "]";
	}
	
	public String getGiftCodeId() {
		return giftCodeId;
	}
	public void setGiftCodeId(String giftCodeId) {
		this.giftCodeId = giftCodeId;
	}

}
