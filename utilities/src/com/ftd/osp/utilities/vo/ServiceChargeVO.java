package com.ftd.osp.utilities.vo;

import java.math.BigDecimal;

public class ServiceChargeVO
{
  private String snhId;
  private BigDecimal domesticCharge;
  private BigDecimal internationalCharge;
  private BigDecimal vendorCharge;
  private BigDecimal vendorSatUpcharge;
  private BigDecimal sameDayUpcharge;
  private BigDecimal sameDayUpchargeFS;
  private String overrideFlag;
  
  public String getSnhId()
  {
    return snhId;
  }

  public void setSnhId(String snhId)
  {
    this.snhId = snhId;
  }

  public BigDecimal getDomesticCharge()
  {
    return domesticCharge;
  }

  public void setDomesticCharge(BigDecimal domesticCharge)
  {
    this.domesticCharge = domesticCharge;
  }

  public BigDecimal getInternationalCharge()
  {
    return internationalCharge;
  }

  public void setInternationalCharge(BigDecimal internationalCharge)
  {
    this.internationalCharge = internationalCharge;
  }

  public BigDecimal getVendorCharge()
  {
    return vendorCharge;
  }

  public void setVendorCharge(BigDecimal vendorCharge)
  {
    this.vendorCharge = vendorCharge;
  }

  public BigDecimal getVendorSatUpcharge()
  {
    return vendorSatUpcharge;
  }

  public void setVendorSatUpcharge(BigDecimal vendorSatUpcharge)
  {
    this.vendorSatUpcharge = vendorSatUpcharge;
  }

    public void setOverrideFlag(String overrideFlag) {
        this.overrideFlag = overrideFlag;
    }

    public String getOverrideFlag() {
        return overrideFlag;
    }

	public void setSameDayUpcharge(BigDecimal sameDayUpcharge) {
		this.sameDayUpcharge = sameDayUpcharge;
	}

	public BigDecimal getSameDayUpcharge() {
		return sameDayUpcharge;
	}

	public BigDecimal getSameDayUpchargeFS() {
		return sameDayUpchargeFS;
	}

	public void setSameDayUpchargeFS(BigDecimal sameDayUpchargeFS) {
		this.sameDayUpchargeFS = sameDayUpchargeFS;
	}
}
