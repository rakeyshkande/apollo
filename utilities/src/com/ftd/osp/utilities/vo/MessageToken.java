package com.ftd.osp.utilities.vo;
import java.io.Serializable;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.Stack;
import java.util.Map;

public class MessageToken implements Serializable {
  private Stack progressionMessages;
  private String status;
  private Object message;
  public static final long serialVersionUID = 6788649795299930352L;
  private Map properties;
  private String JMSCorrelationID;
  private int JMSDeliveryMode;
  private long JMSExpiration;
  private int JMSPriority;

  /**
   * Constructor
   */
  public MessageToken() {
    progressionMessages = new Stack();
    properties = new HashMap();
  }

  /**
   * Returns an iterator to the collection of progression messages
   */
  public Iterator getProgressionMessages() {
    return progressionMessages.iterator();
  }

  /**
   * Sets a progression message in the list of progression messages
   */
  public void setProgressionMessage(String newProgressionMessage) {
    progressionMessages.push(newProgressionMessage);
  }

  /**
   * Removes the progression messages that was set last
   */
  public void removeProgressionMessage(){
    progressionMessages.pop();
  }

  /**
   * Returns the status on the message token
   */
  public String getStatus() {
    return status;
  }

  public void setStatus(String newStatus) {
    status = newStatus;
  }



  /**
   * Returns the message
   */
  public Object getMessage() {
    return message;
  }

  public void setMessage(Object newMessage) {
    message = newMessage;
  }

  /**
   * Returns a property
   *
   * @param name
   * @return
   */
  public MessageProperty getProperty(String name)
  {
    return (MessageProperty)properties.get(name);
  }

  /**
   * Set a property on the message
   *
   * @param name
   * @param value
   */
  public void setProperty(String name, String value, String type)
  {
    properties.put(name, new MessageProperty(name, value, type));
  }

  /**
   * Returns an iterator to the set of property name
   *
   * @return
   */
  public Set getPropertyNames()
  {
    return properties.keySet();
  }


  public String getJMSCorrelationID()
  {
    return JMSCorrelationID;
  }

  public void setJMSCorrelationID(String JMSCorrelationID)
  {
    this.JMSCorrelationID = JMSCorrelationID;
  }

  public int getJMSDeliveryMode()
  {
    return JMSDeliveryMode;
  }

  public void setJMSDeliveryMode(int JMSDeliveryMode)
  {
    this.JMSDeliveryMode = JMSDeliveryMode;
  }

  public long getJMSExpiration()
  {
    return JMSExpiration;
  }

  public void setJMSExpiration(long JMSExpiration)
  {
    this.JMSExpiration = JMSExpiration;
  }

  public int getJMSPriority()
  {
    return JMSPriority;
  }

  public void setJMSPriority(int JMSPriority)
  {
    this.JMSPriority = JMSPriority;
  }

public class MessageProperty implements Serializable
{
    public String name;
    public Object value;
    public String type;

    public MessageProperty(String name, Object value, String type)
    {
      this.name = name;
      this.value = value;
      this.type = type;
    }

}

}