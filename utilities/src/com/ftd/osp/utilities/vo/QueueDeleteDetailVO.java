package com.ftd.osp.utilities.vo;

import java.io.Serializable;

/**
 * Value object for queue delete detail info
 * 
 * Version History
 * Author Name                  Version             Date                      Comments
 * @author Swamy Patsa          @version 1.0        @Date 11/29/2010           Initial Version
 */
public class QueueDeleteDetailVO implements Serializable
{
  private Long queueDeleteDetailId;
  private Long queueDeleteHeaderId;
  private Long messageId;
  private String externalOrderNumber;
  private String processedStatus;
  private String errorText;
  private String createdBy;


  public void setQueueDeleteDetailId(Long queueDeleteDetailId)
  {
    this.queueDeleteDetailId = queueDeleteDetailId;
  }

  public Long getQueueDeleteDetailId()
  {
    return queueDeleteDetailId;
  }

  public void setQueueDeleteHeaderId(Long queueDeleteHeaderId)
  {
    this.queueDeleteHeaderId = queueDeleteHeaderId;
  }

  public Long getQueueDeleteHeaderId()
  {
    return queueDeleteHeaderId;
  }

  public void setMessageId(Long messageId)
  {
    this.messageId = messageId;
  }

  public Long getMessageId()
  {
    return messageId;
  }

  public void setExternalOrderNumber(String externalOrderNumber)
  {
    this.externalOrderNumber = externalOrderNumber;
  }

  public String getExternalOrderNumber()
  {
    return externalOrderNumber;
  }

  public void setProcessedStatus(String processedStatus)
  {
    this.processedStatus = processedStatus;
  }

  public String getProcessedStatus()
  {
    return processedStatus;
  }

  public void setErrorText(String errorText)
  {
    this.errorText = errorText;
  }

  public String getErrorText()
  {
    return errorText;
  }


  public void setCreatedBy(String createdBy)
  {
    this.createdBy = createdBy;
  }

  public String getCreatedBy()
  {
    return createdBy;
  }
}
