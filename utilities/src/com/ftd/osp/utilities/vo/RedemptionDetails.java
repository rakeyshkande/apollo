package com.ftd.osp.utilities.vo;

import java.io.Serializable;
import java.math.BigDecimal;

public class RedemptionDetails implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1235484885630733462L;
	public RedemptionDetails() {
		
	}
	public RedemptionDetails(String redeemedRefNo, BigDecimal redemptionAmount, String redemptionDate) {
		super();
		this.redeemedRefNo = redeemedRefNo;
		this.redemptionAmount = redemptionAmount;
		this.redemptionDate = redemptionDate;
	}
	@Override
	public String toString() {
		return "RedemptionDetails [redeemedRefNo=" + redeemedRefNo + ", redemptionAmount=" + redemptionAmount
				+ ", redemptionDate=" + redemptionDate + "]";
	}
	private String redeemedRefNo;
	public String getRedeemedRefNo() {
		return redeemedRefNo;
	}
	public void setRedeemedRefNo(String redeemedRefNo) {
		this.redeemedRefNo = redeemedRefNo;
	}
	public BigDecimal getRedemptionAmount() {
		return redemptionAmount;
	}
	public void setRedemptionAmount(BigDecimal redemptionAmount) {
		this.redemptionAmount = redemptionAmount;
	}
	public String getRedemptionDate() {
		return redemptionDate;
	}
	public void setRedemptionDate(String redemptionDate) {
		this.redemptionDate = redemptionDate;
	}
	private BigDecimal redemptionAmount=BigDecimal.ZERO;
	private String redemptionDate;
}
