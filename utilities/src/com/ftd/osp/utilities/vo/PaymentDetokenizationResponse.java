package com.ftd.osp.utilities.vo;

import java.io.Serializable;
import java.util.Map;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PaymentDetokenizationResponse implements Serializable {

	private static final long serialVersionUID = 4235214465663305803L;

	private String status;
    private String message;
    private String cardType;
    private String cardNumber;
    private String expirationMonth;
    private String expirationYear;
    private String merchantReferenceId;
    private String reasonCode;

    public PaymentDetokenizationResponse(String status, String message, String cardType, String cardNumber, String expirationMonth, String expirationYear, String merchantReferenceId, String reasonCode) {
		super();

		this.status = status;
		this.message = message;
		this.cardType = cardType;
		this.cardNumber = cardNumber;
		this.expirationMonth = expirationMonth;
		this.expirationYear = expirationYear;
		this.merchantReferenceId = merchantReferenceId;
		this.reasonCode = reasonCode;

	}

	public PaymentDetokenizationResponse(){

	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getExpirationMonth() {
		return expirationMonth;
	}

	public void setExpirationMonth(String expirationMonth) {
		this.expirationMonth = expirationMonth;
	}

	public String getExpirationYear() {
		return expirationYear;
	}

	public void setExpirationYear(String expirationYear) {
		this.expirationYear = expirationYear;
	}

	public String getMerchantReferenceId() {
		return merchantReferenceId;
	}

	public void setMerchantReferenceId(String merchantReferenceId) {
		this.merchantReferenceId = merchantReferenceId;
	}

	public String getReasonCode() {
		return reasonCode;
	}

	public void setReasonCode(String reasonCode) {
		this.reasonCode = reasonCode;
	}

    @JsonProperty("creditCard")
    private void unpackNested(Map<String,Object> creditCard) {
        this.cardType = (String) creditCard.get("cardType");
        this.cardNumber = (String) creditCard.get("cardNumber");
        this.expirationMonth = getExpirationMonthFromJSON((String) creditCard.get("expirationMonth"));
        this.expirationYear = getExpirationYearFromJSON((String) creditCard.get("expirationYear"));
    }

    @JsonProperty("billTo")
    private void unpackNestedBillTo(Map<String,Object> billTo) {

    }

	private String getMaskedCreditCardNumber() {
		if (cardNumber.length() < 4) {
			return "";
		}
		return cardNumber.substring(cardNumber.length() - 4);
	}

	private String getExpirationMonthFromJSON(String jsonResponse) {
		if (jsonResponse.length() == 1) {
			jsonResponse = "0" + jsonResponse;
        }

		return jsonResponse;
	}

	private String getExpirationYearFromJSON(String jsonResponse) {
		if (jsonResponse.length() > 2) {
        	return jsonResponse.substring(jsonResponse.length() - 2);
        }

		return jsonResponse;
	}

	@Override
	public String toString() {
		return "PaymentDetokenizationResponse [status=" + status + ", message=" + message + ", cardType=" + cardType + ", cardNumber=" + getMaskedCreditCardNumber() + ", expirationMonth=" + expirationMonth +
				", expirationYear=" + expirationYear + ", merchantReferenceId=" + merchantReferenceId + ", reasonCode=" + reasonCode + "]";
	}

	
}
