package com.ftd.osp.utilities.vo;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties({"billingAddress", "currency", "legacyCcId"})
public class ProfileCCData {
	
	String createdDate;
	String updatedDate;
	String creditCardId;
	String customerId;
	String cardNumber;
	String cardPurpose;
	boolean cvvIndicatorValue;
	Integer lastFourDigits;
	String merchantReferenceId;
	String expirationMonth;
	String expirationYear;
	String cardType;

	
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public String getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(String updatedDate) {
		this.updatedDate = updatedDate;
	}
	public String getCreditCardId() {
		return creditCardId;
	}
	public void setCreditCardId(String creditCardId) {
		this.creditCardId = creditCardId;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getCardPurpose() {
		return cardPurpose;
	}
	public void setCardPurpose(String cardPurpose) {
		this.cardPurpose = cardPurpose;
	}
	public boolean isCvvIndicatorValue() {
		return cvvIndicatorValue;
	}
	public void setCvvIndicatorValue(boolean cvvIndicatorValue) {
		this.cvvIndicatorValue = cvvIndicatorValue;
	}
	public Integer getLastFourDigits() {
		return lastFourDigits;
	}
	public void setLastFourDigits(Integer lastFourDigits) {
		this.lastFourDigits = lastFourDigits;
	}
	public String getMerchantReferenceId() {
		return merchantReferenceId;
	}
	public void setMerchantReferenceId(String merchantReferenceId) {
		this.merchantReferenceId = merchantReferenceId;
	}
	public String getExpirationMonth() {
		return expirationMonth;
	}
	public void setExpirationMonth(String expirationMonth) {
		this.expirationMonth = expirationMonth;
	}
	public String getExpirationYear() {
		return expirationYear;
	}
	public void setExpirationYear(String expirationYear) {
		this.expirationYear = expirationYear;
	}
	public String getCardType() {
		return cardType;
	}
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	
	public String getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
	
}
