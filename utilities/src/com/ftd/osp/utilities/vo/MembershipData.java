package com.ftd.osp.utilities.vo;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties({"legacyMembershipId", "savings"})
public class MembershipData {
	
	String membershipId;
	String membershipName;
	String joinDate;
	String startDate;
	String expireDate;
	String createdDate;
	String updatedDate;
	String status;
	String customerId;
	String term;
	String createdBy;
	String modifiedBy;
	String email;
	String eventType;
	boolean isAutoRenew;
	String orderNumber;
	
	public String getMembershipId() {
		return membershipId;
	}
	public void setMembershipId(String membershipId) {
		this.membershipId = membershipId;
	}
	public String getMembershipName() {
		return membershipName;
	}
	public void setMembershipName(String membershipName) {
		this.membershipName = membershipName;
	}
	public String getJoinDate() {
		return joinDate;
	}
	public void setJoinDate(String joinDate) {
		this.joinDate = joinDate;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getExpireDate() {
		return expireDate;
	}
	public void setExpireDate(String expireDate) {
		this.expireDate = expireDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCustomerId() {
		return customerId;
	}
	
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	
	public String getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(String updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getTerm() {
		return term;
	}
	public void setTerm(String term) {
		this.term = term;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getEventType() {
		return eventType;
	}
	public void setEventType(String eventType) {
		this.eventType = eventType;
	}
	
	public boolean getIsAutoRenew() {
		return isAutoRenew;
	}
	public void setIsAutoRenew(boolean isAutoRenew) {
		this.isAutoRenew = isAutoRenew;
	}
	
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	
	@Override
	public String toString() {
		return "MemberShipResponse [membershipId=" + membershipId
				+ ", membershipName=" + membershipName + ", joinDate="
				+ joinDate + ", startDate=" + startDate + ", expireDate="
				+ expireDate + ", status=" + status + ", customerId="
				+ customerId + ", term=" + term + ", createdBy=" + createdBy
				+ ", modifiedBy=" + modifiedBy + ", email=" + email
				+ ", isAutoRenew=" + isAutoRenew + ", orderNumber="
				+ orderNumber + "]";
	}
}
