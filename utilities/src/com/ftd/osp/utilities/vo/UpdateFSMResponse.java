package com.ftd.osp.utilities.vo;

import java.util.Map;

public class UpdateFSMResponse {

	Boolean success;
	String id;
	String message;
	
	Map<String, String> errorMap;
	String timeStamp;
	String errorType;
	String status;
	String exception;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Boolean getSuccess() {
		return success;
	}

	public void setSuccess(Boolean success) {
		this.success = success;
	}

	public Map<String, String> getErrorMap() {
		return errorMap;
	}

	public void setErrorMap(Map<String, String> errorMap) {
		this.errorMap = errorMap;
	}

	public String getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}

	public String getErrorType() {
		return errorType;
	}

	public void setErrorType(String errorType) {
		this.errorType = errorType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getException() {
		return exception;
	}

	public void setException(String exception) {
		this.exception = exception;
	}

	@Override
	public String toString() {
		return "UpdateFSMResponse [success=" + success + ", id=" + id
				+ ", message=" + message + ", errorMap=" + errorMap
				+ ", timeStamp=" + timeStamp + ", errorType=" + errorType
				+ ", status=" + status + ", exception=" + exception + "]";
	}
	
	
  
}
