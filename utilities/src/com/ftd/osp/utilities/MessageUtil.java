package com.ftd.osp.utilities;
import com.ftd.osp.utilities.j2ee.ResourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.io.File;
import java.io.IOException;

import java.net.URL;
import java.net.URLDecoder;

import java.util.HashMap;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.Element;

import org.xml.sax.SAXException;
import org.w3c.dom.*;

public class MessageUtil  {
  private Logger logger;
  private  Document messages;
  
  private static MessageUtil MESSAGEUTIL;
  private static final String configFile = "messages.xml";
  
  /** 
   * The private Constructor
  * @exception SAXException
  * @exception ParserConfigurationException

  * @exception IOExceptino
   **/
  private MessageUtil()  
    throws  IOException, SAXException, ParserConfigurationException{
      super();
      this.init();          
  }
 /**
  * The static initializer for the MessageUtil. It ensures that
  * at a given time there is only a single instance  
  * 
  * @exception SAXException
  * @exception ParserConfigurationException
  * @exception IOException

  * @return the MessageUtil
  **/
  public static synchronized MessageUtil getInstance() 
    throws  IOException, SAXException, ParserConfigurationException {
    if ( MESSAGEUTIL == null ){
      MESSAGEUTIL = new MessageUtil();
      return MESSAGEUTIL;
    }
    else {
      return MESSAGEUTIL;
    }
  }


 /**
  * Initialize 
  * @exception SAXException
  * @exception ParserConfigurationException
  * @exception IOException

    */
    private void init()           
        throws  IOException, SAXException, ParserConfigurationException{
      logger = new Logger("com.ftd.osp.utilities.MessageUtil");
      this.initConfig();
    }


  /**
   * Load the config file
   */
  private void initConfig()        
    throws IOException, SAXException, ParserConfigurationException {
    
    this.messages = DOMUtil.getDocument(ResourceUtil.getInstance().getResourceAsStream(this.configFile));   
    
  }

  /**
   * Returns the message content for the specified key
   * 
   * @param messageKey the message key
 
   * @return the message content
 * @throws XPathExpressionException 
   */
   public String getMessageContent(String messageKey) throws XPathExpressionException{
     Node messageContent = (Node) DOMUtil.selectSingleNode(messages,"/messages/message[@key='"+messageKey+"']/content/text()");
     if (messageContent != null)  {
        return messageContent.getNodeValue();
     } else {
       return "";
     }
     
     
   }
/*
public static void main(String[] args) {
try  {
  MessageUtil util = MessageUtil.getInstance();
  System.out.println(util.getMessageContent("UnableToPerformAction"));
 
} catch (Exception ex)  {
  ex.printStackTrace();
} finally  {
}

 }
*/   
}