package com.ftd.osp.utilities;

import com.ftd.osp.utilities.plugins.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;


/**
 * SendUpdateFeed
 * This thread send a socket message.
 */
public class SendUpdateFeed 
{
    
    private static final Logger logger = new Logger("com.ftd.osp.utilities.SendUpdateFeed");

    /**
    * Constructor for the SendUpdateFeed
    *
    */
    public SendUpdateFeed()
    {
    }

    
  /**
   * Send data over the socket.
   * @param serverName server at novator
   * @param portNumber http port
   * @param timeout socket timeout
   * @param transmitData data to be sent
   * @return java.lang.String - socket response
   */
    public static String send(String serverName, int portNumber, int timeout, String transmitData) throws Exception
    {
      String response = "";
      Socket socket = null;
      PrintWriter out = null;
      BufferedReader in = null;


      try 
      {
        logger.debug("SendUpdateFeed: Connecting to " + serverName + " on port " + portNumber + " for feed upload");
        socket = new Socket(serverName, portNumber);
        socket.setSoTimeout(timeout);

        out = new PrintWriter(socket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
      }
      catch (UnknownHostException e)
      {            
        logger.error(e);
        throw new Exception(e);
      }
      catch (IOException e)
      {
        logger.error(e);
        throw new Exception(e);
      }

      logger.debug("SendUpdateFeed: Sending " + transmitData + " to " + serverName + " on port " + portNumber);
      out.println(transmitData);
      out.flush();
        
      logger.debug("SendUpdateFeed: Data sent");
      
      try
      {
        logger.debug("SendUpdateFeed: Waiting for response");
        StringBuffer sb = new StringBuffer();
        response = in.readLine();
        while( response!=null && response.length()>0 ) 
        {
            sb.append(response);
            response = in.readLine();
        }
        response = sb.toString();
        logger.debug("Novator Response:" + response);
        
        //save feeds to store.
        if(logger.isDebugEnabled()) {
        	 logger.debug("Novator feed succesfully sent, Saving store feed.");
        }
        StoreFeedUtil storeFeedUtil = new StoreFeedUtil();
        storeFeedUtil.saveStoreFeed(transmitData);
      } 
      catch(SocketTimeoutException stoe)
      {
        logger.error(stoe);
        throw new Exception(stoe);
      }
      catch(IOException ioe)
      {
        logger.error(ioe);
        throw new Exception(ioe);   
      } 
      finally 
      {
        try
        {
          out.close();
          in.close();
          socket.close();
        }
        catch(IOException ioe)
        {
          logger.error(ioe);
          throw new Exception(ioe);   
        }
      }
        
      logger.debug("Ending send method");

      return response;

    }

}

