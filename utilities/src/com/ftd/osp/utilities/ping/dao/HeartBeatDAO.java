package com.ftd.osp.utilities.ping.dao;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;

import java.sql.SQLException;

import javax.swing.JPanel;

public class HeartBeatDAO
{
private static Logger logger = new Logger("com.ftd.osp.utilities.dataaccess.HeartBeatDAO");

  private Connection conn = null;
  
  // Stored procedure names
  private static final String IS_DB_UP = "IS_DB_UP";

  

  public HeartBeatDAO(Connection conn)
  {
    this.conn = conn;
  }
  
  public boolean checkDBStatus() throws SQLException, Exception
  {
    boolean flag = true;
    try{
      DataAccessUtil dau = DataAccessUtil.getInstance();
      DataRequest dataRequest = new DataRequest();
      dataRequest.setConnection(conn);
      dataRequest.setStatementID(IS_DB_UP);
    /* execute the stored procedure */
      String outputs = (String)dau.execute(dataRequest);
      if(outputs != null && outputs.equalsIgnoreCase("N")) {
        flag = false;
      }
      return flag;
    }
    catch(Exception e)
    {
      logger.error("Error occured in HeartBeatDAO --> checkDBStatus() method.");
      flag = false;
      return flag;
    }
    
  }
  
}
