package com.ftd.osp.utilities.ping.servlet;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.ping.dao.HeartBeatDAO;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;
import java.io.PrintWriter;

import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;


import org.xml.sax.SAXException;

public abstract class BaseHeartBeatServlet
  extends HttpServlet
{
  private Logger logger = 
        new Logger("com.ftd.osp.utilities.ping.servlet.BaseHeartBeatServlet");

  public void init(ServletConfig config)
    throws ServletException
  {
    super.init(config);
  }
  /**
   * 
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
  */
  public void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException
  {
    String returnVal = "FAILURE";
    Connection conn = null;
    HeartBeatDAO heartBeatDAO;
    try{
      conn = getDBConnection();
      heartBeatDAO = new  HeartBeatDAO(conn);
      boolean flag = heartBeatDAO.checkDBStatus();
       
       if(flag)
       {
        returnVal = "SUCCESS";
       }

        PrintWriter out = response.getWriter();
        out.println(returnVal);
        out.close();
      
    }catch(Exception e)
    { 
      logger.error("Error occured in CustomerOrderManagement.CheckPluseServlet");
      returnVal = "FAILURE";
      PrintWriter out = response.getWriter();
      out.println(returnVal);
      out.close();
    }finally{
    	try {
			if(conn != null)
				conn.close();
		} catch (SQLException e) {
			logger.error("Error occured while closing the connection");
		}
    }
    
  }
  
  
  /**
   * 
   * @return
   * @throws IOException
   * @throws ParserConfigurationException
   * @throws SAXException
   * @throws TransformerException
   * @throws XSLException
   * @throws Exception
   */
	public abstract Connection getDBConnection()
		throws Exception;

}
