package com.ftd.osp.utilities;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.SurchargeVO;

import java.sql.Connection;
import java.sql.Timestamp;

import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

public class FTDFuelSurchargeUtilities
{
  private static Logger logger = new Logger("com.ftd.osp.utilities.FTDFuelSurchargeUtilities");


  public FTDFuelSurchargeUtilities()
  {
  }


  /*
   * Retrieves fuel surcharge
   */

  public static String getFuelSurcharge(Connection conn)
    throws Exception
  {
    Calendar cal = null;
    String fuelSurchargeAmount = getFuelSurcharge(cal, conn);
    return fuelSurchargeAmount;
  } //end method getFuelSurcharge


  /*
   * Retrieves fuel surcharge
   */

  public static String getFuelSurcharge(java.util.Date utilOrderDate, Connection conn)
    throws Exception
  {
    Calendar cal = null;
    if (utilOrderDate != null)
    {
      cal = Calendar.getInstance();
      cal.setTime(utilOrderDate);
    }

    String fuelSurchargeAmount = getFuelSurcharge(cal, conn);
    return fuelSurchargeAmount;
  } //end method getFuelSurcharge


  /*
   * Retrieves fuel surcharge
   */

  public static String getFuelSurcharge(java.sql.Date sqlOrderDate, Connection conn)
    throws Exception
  {
    Calendar cal = null;
    if (sqlOrderDate != null)
    {
      cal = Calendar.getInstance();
      cal.setTime(sqlOrderDate);
    }

    String fuelSurchargeAmount = getFuelSurcharge(cal, conn);
    return fuelSurchargeAmount;
  } //end method getFuelSurcharge


  /*
   * Retrieves fuel surcharge
   */

  public static String getFuelSurcharge(Calendar cal, Connection conn)
    throws Exception
  {
    /* setup store procedure input parameters */
    HashMap inputParams = new HashMap();
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    if (cal != null)
      inputParams.put("IN_START_DATE", new Timestamp(cal.getTimeInMillis()));

    /* build DataRequest object */
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(conn);
    dataRequest.setStatementID("GET_FUEL_SURCHARGE");
    dataRequest.setInputParams(inputParams);

    /* execute the store prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    Map outputs = (Map) dataAccessUtil.execute(dataRequest);

    /* read store prodcedure output parameters to determine
     * if the procedure executed successfully */
    String status = (String) outputs.get("OUT_STATUS");
    if (status != null && status.equals("N"))
    {
      String message = (String) outputs.get("OUT_MESSAGE");
      throw new Exception(message);
    }

    //retrieve fuel surcharge
    String fuelSurchargeAmount = (String) outputs.get("OUT_FUEL_SURCHARGE_AMT");

    if (cal != null)
      logger.info("FTDFuelSurchargeUtilities - Date passed in = " + sdf.format(cal.getTime()) + 
                  " and Fuel Surcharge returned = " + fuelSurchargeAmount);
    else
      logger.info("FTDFuelSurchargeUtilities - Date passed in = NULL, and Fuel Surcharge returned = " + fuelSurchargeAmount);

    //and return it
    return fuelSurchargeAmount;
  } //end method getFuelSurcharge


  /*
   * Retrieves surchargeVO object similar to method above but returns full VO object rather than just fuel_surcharge_amount. Gets it from fuel_surcharge table.
   */

  private static SurchargeVO getGenericSurchargeVO(Calendar cal, Connection conn)
    throws Exception
  {
    /* setup store procedure input parameters */
    HashMap inputParams = new HashMap();


    if (cal != null)
      inputParams.put("IN_START_DATE", new Timestamp(cal.getTimeInMillis()));

    /* build DataRequest object */
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(conn);
    dataRequest.setStatementID("GET_GENERIC_SURCHARGE_VALUES");
    dataRequest.setInputParams(inputParams);

    /* execute the store prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

    CachedResultSet queryOutput = null;
    queryOutput = (CachedResultSet) dataAccessUtil.execute(dataRequest);

    SurchargeVO surchargeVO = null ;
    while (queryOutput.next())
    {
      surchargeVO = new SurchargeVO(); 
      surchargeVO.setApplySurchargeCode(queryOutput.getString("apply_surcharge_code"));
      surchargeVO.setSurchargeAmount(queryOutput.getDouble("fuel_surcharge_amt"));
      surchargeVO.setSurchargeDescription(queryOutput.getString("surcharge_description"));
      surchargeVO.setDisplaySurcharge(queryOutput.getString("display_surcharge_flag"));
      surchargeVO.setSendSurchargeToFlorist(queryOutput.getString("send_surcharge_to_florist_flag"));
      logger.debug("surcharge amount from fuel_master table is:" + surchargeVO.getSurchargeAmount());
      break;
    }

     if (surchargeVO == null)
     {
    // if surcharge_flag doesn't meet one of those above, then throw a exception as SurchargeVO is not available in db because this should never happen 
      logger.error("No data found for Generic surchargeVO from database, returning null from FTDFuelSurchargeUtilities");
       throw new Exception("No data found for Generic surchargeVO from database, returning null from FTDFuelSurchargeUtilities\")");    
     }

    //and return it
    return surchargeVO;
  } //end method getFuelSurcharge


  /*
   * Retrieves surchargeVO from fuel_surcharge table,  source_master$ table or source_master .
   */

  public static SurchargeVO getSurchargeVO(Calendar cal, String sourceCode, Connection conn)
    throws Exception
  {
   if (cal == null)
   {
     logger.debug("In getSurchargeVO Date passed in is null");
   }
   else
   {

     SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
     logger.debug("In getSurchargeVO Date passed in = " + sdf.format(cal.getTime())); 
   }
    logger.debug("In getSurchargeVO sourceCode passed in = " + sourceCode); 
    
    SurchargeVO sVO = null ;
    SurchargeVO gSVO = getGenericSurchargeVO(cal, conn);
    
    String genericSurchargeFlag = gSVO.getApplySurchargeCode();
    logger.debug("genericSurchargeFlag is : " + genericSurchargeFlag);
    logger.debug("send Surcharge to FLorist flag in FTDSurchargeUtilities is "+ gSVO.getSendSurchargeToFlorist());
    if (StringUtils.equalsIgnoreCase(genericSurchargeFlag, "OFF"))
    {
      logger.debug("returning Generic surchargeVO ");
      gSVO.setApplySurchargeFlag(false);
      return gSVO;
    }else if (StringUtils.equalsIgnoreCase(genericSurchargeFlag, "ON"))
    {
      gSVO.setApplySurchargeFlag(true);
    }
    else
    {
      gSVO.setApplySurchargeFlag(false);
    }

    if (sourceCode == null)
    {
       logger.debug("returning Generic surchargeVO ");
       return gSVO;
    }
    
    else if (sourceCode != null && cal == null)
    {
      sVO =  getSurchargeVOFromSourceMaster(sourceCode, conn);
    }
    else 
    {
       sVO = getSurchargeVOFromSourceShadowTable(cal, sourceCode, conn); // get SurchargeVO from SOURCE_master shadow table.     
    }
    
    if (StringUtils.equalsIgnoreCase(genericSurchargeFlag, "ON") || 
             StringUtils.equalsIgnoreCase(genericSurchargeFlag, "OFFC"))
    {
      String sourceCodeSurchargeFlag = sVO.getApplySurchargeCode();
      sVO.setSendSurchargeToFlorist(gSVO.getSendSurchargeToFlorist()); // We don't use this flag on source_maintenance screen so not in Source_master table , so need to get it from gsVO.

      logger.debug("sourceCodeSurchargeFlag is : " + sourceCodeSurchargeFlag);

      if (StringUtils.equalsIgnoreCase(sVO.getDisplaySurcharge(), "D")) 
      {
        sVO.setDisplaySurcharge(gSVO.getDisplaySurcharge());
     
      }

      if (StringUtils.equalsIgnoreCase(sourceCodeSurchargeFlag, "OFF")) 
      {
        sVO.setApplySurchargeFlag(false);
        return sVO;
      }
      else if (StringUtils.equalsIgnoreCase(sourceCodeSurchargeFlag, "ON"))
      {
        sVO.setApplySurchargeFlag(true);
        return sVO;
      }
      else if (StringUtils.equalsIgnoreCase(sourceCodeSurchargeFlag, "DEFAULT"))
      {
        if (StringUtils.equalsIgnoreCase(genericSurchargeFlag, "OFFC"))
        {
          gSVO.setApplySurchargeCode("OFF");
        }
        logger.debug("returning Generic surchargeVO ");
        return gSVO;
      }
      else
      {
      // if surcharge_flag doesn't meet one of those above, then throw a exception as SurchargeVO is not available in db because this should never happen 
      logger.error("No data found for surchargeVO from database, returning null from FTDFuelSurchargeUtilities");
        throw new Exception("No data found for surchargeVO from database, returning null from FTDFuelSurchargeUtilities\")");    
      }

    }
    else
    {
      logger.error("No data found for surchargeVO from database, returning null from FTDFuelSurchargeUtilities");
        throw new Exception("No data found for surchargeVO from database, returning null from FTDFuelSurchargeUtilities\")");    
      
    }
     //return null;
  }

  /*
   * Retrieves surchargeVO object from Source_Master$ shadow table.We get from shadow table because we need to fetch based on timestamp.
   */

  private static SurchargeVO getSurchargeVOFromSourceShadowTable(Calendar cal, String sourceCode, Connection conn)
    throws Exception
  {

    CachedResultSet searchResults = null;
    SurchargeVO surchargeVO = null; 

    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(conn);
    dataRequest.setStatementID("GET_SRC_SURCH_VAL_FRM_SRC_SHADOW_BY_ORD_TIME");

    dataRequest.addInputParam("IN_SOURCE_CODE", sourceCode);
  
    if (cal != null)
    {
    dataRequest.addInputParam("IN_ORDER_TIME", new Timestamp(cal.getTimeInMillis()));
    }
    else
    {
      dataRequest.addInputParam("IN_ORDER_TIME", 
                                new Timestamp(new java.util.Date().getTime())); // This is done because we get the data from SOURCE_MASTER$ table based on timestamp, which is never null so got to use present time for latest value.
    }

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

    searchResults = (CachedResultSet) dataAccessUtil.execute(dataRequest);


    while (searchResults.next())
    {
      surchargeVO = new SurchargeVO();
      surchargeVO.setApplySurchargeCode(searchResults.getString("apply_surcharge_code"));
      surchargeVO.setSurchargeAmount(searchResults.getDouble("surcharge_amount"));
      surchargeVO.setSurchargeDescription(searchResults.getString("surcharge_description"));
      surchargeVO.setDisplaySurcharge(searchResults.getString("display_surcharge_flag"));
      logger.debug("surcharge amount from Source_master$ shadow table is:" + surchargeVO.getSurchargeAmount());
      break;
    }
  
    if (surchargeVO == null)
    {
    // if surcharge_flag doesn't meet one of those above, then throw a exception as SurchargeVO is not available in db because this should never happen 
      logger.error("No data found for surchargeVO from SOURCE_MASTER$ shadow table, returning null from FTDFuelSurchargeUtilities");
      throw new Exception("No data found for surchargeVO from SOURCE_MASTER$ shadow table, returning null from FTDFuelSurchargeUtilities\")");    
    }
    return surchargeVO;
  }

  /*
   * Retrieves surchargeVO object from Source_Master table. This is used for source maintenance screen.
   */

  private static SurchargeVO getSurchargeVOFromSourceMaster(String sourceCode, Connection conn)
    throws Exception
  {

    CachedResultSet searchResults = null;
    SurchargeVO surchargeVO = null;

    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(conn);
    dataRequest.setStatementID("GET_SRC_SURCH_VAL_FRM_SRC_MASTER");

    dataRequest.addInputParam("IN_SOURCE_CODE", sourceCode);


    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

    searchResults = (CachedResultSet) dataAccessUtil.execute(dataRequest);


      while (searchResults.next())
      {
       surchargeVO = new SurchargeVO();

        surchargeVO.setApplySurchargeCode(searchResults.getString("apply_surcharge_code"));
        surchargeVO.setSurchargeAmount(searchResults.getDouble("surcharge_amount"));
        surchargeVO.setSurchargeDescription(searchResults.getString("surcharge_description"));
        surchargeVO.setDisplaySurcharge(searchResults.getString("display_surcharge_flag"));
        logger.debug("surcharge amount from Source_master table is:" + surchargeVO.getSurchargeAmount());
        break;
      }
     if (surchargeVO == null)
     {
    // if surcharge_flag doesn't meet one of those above, then throw a exception as SurchargeVO is not available in db because this should never happen 
      logger.error("No data found for surchargeVO from SOURCE_MASTER table, returning null from FTDFuelSurchargeUtilities");
      throw new Exception("No data found for surchargeVO from SOURCE_MASTER table, returning null from FTDFuelSurchargeUtilities\")");    
     }


    return surchargeVO;
  }
}
