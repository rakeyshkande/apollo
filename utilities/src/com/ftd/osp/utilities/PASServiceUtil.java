/**
 * 
 */
package com.ftd.osp.utilities;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.ftd.ftdutilities.GeneralConstants;
import com.ftd.ftdutilities.OEDeliveryDate;
import com.ftd.ftdutilities.ShippingMethod;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.stats.ServiceResponseTrackingUtil;
import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;
import com.ftd.osp.utilities.vo.ExtendedFloristAvailabilityRequest;
import com.ftd.osp.utilities.vo.ExtendedFloristAvailabilityResponse;
import com.ftd.osp.utilities.vo.PartnerMappingVO;
import com.ftd.pas.client.util.PASQueryUtil;
import com.ftd.pas.core.domain.PasShipDatesAvailVO;
import com.ftd.pas.core.domain.ProductAvailVO;
import com.ftd.pas.exception.PASClientException;
import com.ftd.pas.exception.PASTimeoutException;

/**
 * @author smeka
 *
 */
public class PASServiceUtil {
	
	private static Logger logger = new Logger(PASServiceUtil.class.getName());
	private static String PAS_NOPAGE_SUBJECT = "NO PAGE PAS Client Exception";
	private static String PAS_PAGE_SUBJECT = "PAS Client PAGE Exception";
	private static String PAS_CLIENT_SOURCE = "PAS_CLIENT_SERVICE";
	private static final long DEFAULT_PAS_SVC_CONN_TIMEOUT = 5000;
	private static final long DEFAULT_PAS_SVC_recv_TIMEOUT = 5000;

	private static final String PAS_SERVICE = "PAS Service";
	private static String GET_INTNL_PROD_AVAIL_DATES = "getInternationalProductAvailableDates";
	//private static String IS_PROD_LIST_AVAIL = "isProductListAvailable";
	private static String GET_FLORISTS = "getFlorists";
	private static String IS_ANY_PROD_AVAIL = "isAnyProductAvailable";
	private static String GET_AVAIL_SHIP_DATES = "getAvailableShipDates";
	private static String GET_MOST_POPULAR_PRODS = "getMostPopularProducts";
	private static String GET_NEXT_AVAIL_DATE = "getNextAvailableDeliveryDate";
	private static String GET_PROD_AVAIL = "getProductAvailability";
	private static String IS_INTNL_PROD_AVAIL = "isInternationalProductAvailable";
	private static String IS_PROD_AVAIL = "isProductAvailable";
	private static String GET_PROD_AVAIL_DATES = "getProductAvailableDates";
	//private static String IS_INTNL_PROD_LIST_AVAIL = "isInternationalProductListAvailable";
		
		
	/** Return non null pasWSDL URL for the flows when call_pas_svc_XXX is set to 'Y' 
	 * 
	 * @return
	 */
	private static PASQueryUtil getPASQueryUtil() {
		PASQueryUtil pasUtil = null;
		logger.info("PASQueryUtil initialization");
		try {
			ConfigurationUtil configUtil = new ConfigurationUtil(); 
			long serviceConnTimeoutMilli = DEFAULT_PAS_SVC_CONN_TIMEOUT;
			long serviceReceiveTimeoutMilli = DEFAULT_PAS_SVC_recv_TIMEOUT;
			
			try {
				serviceConnTimeoutMilli = Long.parseLong(configUtil.getFrpGlobalParm(GeneralConstants.PAS_CONFIG_CONTEXT, GeneralConstants.PAS_SVC_CONN_TIMEOUT_PARAM));
				serviceReceiveTimeoutMilli = Long.parseLong(configUtil.getFrpGlobalParm(GeneralConstants.PAS_CONFIG_CONTEXT, GeneralConstants.PAS_SVC_RECV_TIMEOUT_PARAM));
			} catch(Exception e) {
				logger.error("Invalid timeout value is provided to PAS Service timeout. Default timeout is configured.");
			}
			
			String pasServiceWSDL = configUtil.getFrpGlobalParm(GeneralConstants.SERVICE_CONTEXT, GeneralConstants.PAS_SVC_WSDL_PARAM);			  
			if(pasServiceWSDL == null || pasServiceWSDL.trim().length() == 0) { 
				throw new Exception("PAS Service WSDL in not configured. Cannot call PAS Service.");
			}							
			
			pasUtil = new PASQueryUtil(pasServiceWSDL, serviceConnTimeoutMilli, serviceReceiveTimeoutMilli);
			
		} catch(PASClientException e) {
			logger.error("Error caught while instantiating PASQueryUtil - configuration missing.", e);
			sendSystemMessage(PAS_PAGE_SUBJECT, "Error caught instantiating PASQueryUtil - configuration missing." + e);
		} catch (Exception e) {
			logger.error("Error caught instantiating PASQueryUtil - configuration missing.", e);
			sendSystemMessage(PAS_NOPAGE_SUBJECT, "Error caught instantiating PASQueryUtil - configuration missing." + getStackTrace(e));
		}
		return pasUtil;
	}
	


	/** 
	 * @param partnerMappingVO
	 * @param itemSku
	 * @param zipCode
	 * @param country
	 * @param sourceCode
	 * @param days
	 * @param shipMethodFlorist
	 * @param addons 
	 * @return
	 */
	public static Date getPartnerNextAvailableDate(PartnerMappingVO partnerMappingVO, String itemSku, String zipCode,
			String country, String sourceCode, int days, String shipMethodFlorist, List<String> addons) {
		
		logger.info("SKU:" + itemSku + ", Zip:" + zipCode + ", shipMethodFlorist:" + shipMethodFlorist);
		Date nextAvailableDate = null;
		String serviceMethod = null;
		long startTime = 0;
		
		boolean pasCallNeeded = true;
		int retryCount = 0;
		int maxRetries = 2;
		try {
			ConfigurationUtil configUtil = new ConfigurationUtil(); 
			maxRetries = Integer.valueOf(configUtil.getFrpGlobalParm("PAS_CONFIG", "TIMEOUT_MAX_RETRIES"));
		} catch (Exception e) {
			logger.error("Invalid retry count");
			maxRetries = 2;
		}

		while (pasCallNeeded) {
			
			pasCallNeeded = false;
			
			try { 
		 
				PASQueryUtil pasUtil = getPASQueryUtil();
			
				logger.info("partnerMappingVO.getSatDeliveryAllowedFloral(): " + partnerMappingVO.getSatDeliveryAllowedFloral());
				logger.info("partnerMappingVO.getSunDeliveryAllowedFloral(): " + partnerMappingVO.getSunDeliveryAllowedFloral());
		
				if(shipMethodFlorist.equalsIgnoreCase("Y")) {
					Calendar c = Calendar.getInstance(); 
					// As sat and sun not allowed get the first available weekday date
					if(partnerMappingVO.getSatDeliveryAllowedFloral().equalsIgnoreCase("N") && partnerMappingVO.getSunDeliveryAllowedFloral().equalsIgnoreCase("N")) {	
						serviceMethod = GET_PROD_AVAIL_DATES;
						startTime = (new Date()).getTime();
						ProductAvailVO paVO = pasUtil.getPASFirstAvailableWeekDayDate(itemSku, addons, zipCode, country, days);	
						if(paVO != null) {
							nextAvailableDate = paVO.getDeliveryDate();
						}
					} 
					// As sat not allowed get the first available date which is non sat
					else if (partnerMappingVO.getSatDeliveryAllowedFloral().equalsIgnoreCase("N")) {	
						serviceMethod = GET_PROD_AVAIL_DATES;
						startTime = (new Date()).getTime();
						ProductAvailVO paVO = pasUtil.getPASNoSATFirstAvailableDate(itemSku, addons, c, zipCode, country, sourceCode, days);
						if(paVO != null) {
							nextAvailableDate = paVO.getDeliveryDate();
						}
					}
					// As sun not allowed get the first available date which is non sun
					else if (partnerMappingVO.getSunDeliveryAllowedFloral().equalsIgnoreCase("N")) {	
						serviceMethod = GET_PROD_AVAIL_DATES;
						startTime = (new Date()).getTime();
						ProductAvailVO paVO = pasUtil.getPASNoSUNFirstAvailableDate(itemSku, addons, c, zipCode, country, sourceCode, days);
						if(paVO != null) {
							nextAvailableDate = paVO.getDeliveryDate();
						}
					}
					// As sat and sun allowed get the next available first date
					else {
						serviceMethod = GET_NEXT_AVAIL_DATE;
						startTime = (new Date()).getTime();
						nextAvailableDate = pasUtil.getNextAvailableDeliveryDate(itemSku, addons, zipCode, country, sourceCode);
					}
				} else {
					serviceMethod = GET_NEXT_AVAIL_DATE;
					startTime = (new Date()).getTime();
					nextAvailableDate = pasUtil.getNextAvailableDeliveryDate(itemSku, addons, zipCode, country, sourceCode);
				}
			
				logStats(null, serviceMethod, startTime);
			
			} catch (PASTimeoutException e) {
				logger.error("Timeout exception: " + e);
				retryCount++;
				logger.info("retryCount: " + retryCount + " maxRetries: " + maxRetries);
				if (retryCount < maxRetries) {
					pasCallNeeded = true;
				} else {
					logger.error("Max retries reached");
					sendSystemMessage(PAS_NOPAGE_SUBJECT, "getPartnerNextAvailableDate Exception: " + getStackTrace(e));
				}
			} catch(Exception e) {    	  
				logger.error("getPartnerNextAvailableDate Exception: ", e);
				sendSystemMessage(PAS_NOPAGE_SUBJECT, "getPartnerNextAvailableDate Exception: " + getStackTrace(e));
			}
		}
		
		return nextAvailableDate;
	}
	

	/** 
	 * @param itemSku
	 * @param deliveryDate
	 * @param zipCode
	 * @param country
	 * @param shipMethodFlorist
	 * @param addons
	 * @param sourceCode 
	 * @return
	 * @throws Exception
	 */
	public static String getShipMethod(String itemSku, Date deliveryDate, String zipCode, String country,
			String shipMethodFlorist, List<String> addons,String sourceCode) {		
		String shipMethod = null;
		String serviceMethod = null;
		long startTime = 0;
		
		boolean pasCallNeeded = true;
		int retryCount = 0;
		int maxRetries = 2;
		try {
			ConfigurationUtil configUtil = new ConfigurationUtil(); 
			maxRetries = Integer.valueOf(configUtil.getFrpGlobalParm("PAS_CONFIG", "TIMEOUT_MAX_RETRIES"));
		} catch (Exception e) {
			logger.error("Invalid retry count");
			maxRetries = 2;
		}

		while (pasCallNeeded) {
			
			pasCallNeeded = false;
			
			try {
			
				PASQueryUtil pasUtil = getPASQueryUtil();   
				serviceMethod = GET_PROD_AVAIL;
				startTime = (new Date()).getTime();
				ProductAvailVO shipDetail = pasUtil.getProductAvailShipData(itemSku, addons, deliveryDate, zipCode, country, sourceCode, null, shipMethodFlorist, false, false);
				if(shipDetail != null) {
					shipMethod = shipDetail.getShipMethod();
				}
		    
				logger.info("Ship Method: " + shipMethod);
				logStats(null, serviceMethod, startTime);
		    
			} catch (PASTimeoutException e) {
				logger.error("Timeout exception: " + e);
				retryCount++;
				logger.info("retryCount: " + retryCount + " maxRetries: " + maxRetries);
				if (retryCount < maxRetries) {
					pasCallNeeded = true;
				} else {
					logger.error("Max retries reached");
					sendSystemMessage(PAS_NOPAGE_SUBJECT, "Exception caught in getShipMethod: " + getStackTrace(e));
				}
			} catch(Exception e) { 			
				logger.error("Exception caught in getShipMethod: ", e); 
				sendSystemMessage(PAS_NOPAGE_SUBJECT, "Exception caught in getShipMethod: " + getStackTrace(e));
			}
		}
		return shipMethod;
	}
	
	/** 
	 * @param itemSku
	 * @param deliveryDate
	 * @param zipCode
	 * @param country
	 * @param shipMethod
	 * @param shipMethodFlorist
	 * @param addons
	 * @param sourceCode 
	 * @return
	 * @throws Exception
	 */
	public static ProductAvailVO getProductAvailShipData(String itemSku, Date deliveryDate, String zipCode, String country,
			String shipMethod, String shipMethodFlorist, List<String> addons, String sourceCode) {		
		ProductAvailVO shipDetail = null;
		String serviceMethod = null;
		long startTime = 0;
		
		boolean pasCallNeeded = true;
		int retryCount = 0;
		int maxRetries = 2;
		try {
			ConfigurationUtil configUtil = new ConfigurationUtil(); 
			maxRetries = Integer.valueOf(configUtil.getFrpGlobalParm("PAS_CONFIG", "TIMEOUT_MAX_RETRIES"));
		} catch (Exception e) {
			logger.error("Invalid retry count");
			maxRetries = 2;
		}

		while (pasCallNeeded) {
			
			pasCallNeeded = false;
			
			try {  			
				PASQueryUtil pasUtil = getPASQueryUtil();  
				serviceMethod = GET_PROD_AVAIL;
				startTime = (new Date()).getTime();
				shipDetail = pasUtil.getProductAvailShipData(itemSku, addons, deliveryDate, zipCode, country, sourceCode, shipMethod, shipMethodFlorist, false, false);
				logStats(null, serviceMethod, startTime);
		    
			} catch (PASTimeoutException e) {
				logger.error("Timeout exception: " + e);
				retryCount++;
				logger.info("retryCount: " + retryCount + " maxRetries: " + maxRetries);
				if (retryCount < maxRetries) {
					pasCallNeeded = true;
				} else {
					logger.error("Max retries reached");
					sendSystemMessage(PAS_NOPAGE_SUBJECT, "Exception caught in getProductAvailShipData: " + getStackTrace(e));
				}
			} catch(Exception e) { 			
				logger.error("Exception caught in getProductAvailShipData: ", e); 
				sendSystemMessage(PAS_NOPAGE_SUBJECT, "Exception caught in getProductAvailShipData: " + getStackTrace(e));
			}
		}
		
		if(shipDetail == null) {
			shipDetail = new ProductAvailVO();
		}
		return shipDetail;
	}
	
	/** 
	 * @param itemSku
	 * @param deliveryDate
	 * @param zipCode
	 * @param country
	 * @param shipMethodFlorist
	 * @param addons
	 * @param sourceCode 
	 * @return
	 * @throws Exception
	 */
	public static String getShipDate(String itemSku, Date deliveryDate, String zipCode, String country,
			String shipMethodFlorist, List<String> addons,String sourceCode) {		
		String shipDate = null;
		String serviceMethod = null;
		long startTime = 0;
		
		boolean pasCallNeeded = true;
		int retryCount = 0;
		int maxRetries = 2;
		try {
			ConfigurationUtil configUtil = new ConfigurationUtil(); 
			maxRetries = Integer.valueOf(configUtil.getFrpGlobalParm("PAS_CONFIG", "TIMEOUT_MAX_RETRIES"));
		} catch (Exception e) {
			logger.error("Invalid retry count");
			maxRetries = 2;
		}

		while (pasCallNeeded) {
			
			pasCallNeeded = false;
			
			try {  
				PASQueryUtil pasUtil = getPASQueryUtil(); 
				serviceMethod = GET_PROD_AVAIL;
				startTime = (new Date()).getTime();
				ProductAvailVO shipDetail = pasUtil.getProductAvailShipData(itemSku, addons, deliveryDate, zipCode, country, sourceCode, null, shipMethodFlorist, false, false);
				if(shipDetail != null) {
					shipDate = shipDetail.getShipDate();
				}
				logger.info("Ship Date: " + shipDate);
				logStats(null, serviceMethod, startTime);
		    
			} catch (PASTimeoutException e) {
				logger.error("Timeout exception: " + e);
				retryCount++;
				logger.info("retryCount: " + retryCount + " maxRetries: " + maxRetries);
				if (retryCount < maxRetries) {
					pasCallNeeded = true;
				} else {
					logger.error("Max retries reached");
					sendSystemMessage(PAS_NOPAGE_SUBJECT, "Exception caught in getShipDate: " + getStackTrace(e));
				}
			} catch(Exception e) { 			
				logger.error("Exception caught in getShipDate: ", e); 
				sendSystemMessage(PAS_NOPAGE_SUBJECT, "Exception caught in getShipDate: " + getStackTrace(e));
			}
		}
		return shipDate;
	}
	
	/** 
	 * @param itemSku
	 * @param deliveryDate
	 * @param zipCode
	 * @param country
	 * @param addons
	 * @param sourceCode
	 * @param includeCharges
	 * @param checkMDAvailable 
	 * @return
	 * @throws Exception
	 */
	public static ProductAvailVO getProductAvailability(String itemSku, Date deliveryDate, String zipCode, String country,
			List<String> addons, String sourceCode, boolean includeCharges, boolean checkMDAvailable) {
		ProductAvailVO paVO = null;
		String serviceMethod = null;
		long startTime = 0;

		boolean pasCallNeeded = true;
		int retryCount = 0;
		int maxRetries = 2;
		try {
			ConfigurationUtil configUtil = new ConfigurationUtil(); 
			maxRetries = Integer.valueOf(configUtil.getFrpGlobalParm("PAS_CONFIG", "TIMEOUT_MAX_RETRIES"));
		} catch (Exception e) {
			logger.error("Invalid retry count");
			maxRetries = 2;
		}

		while (pasCallNeeded) {
			
			pasCallNeeded = false;
			
			try {
				PASQueryUtil pasUtil = getPASQueryUtil();
				serviceMethod = GET_PROD_AVAIL;
				startTime = (new Date()).getTime();
			
				paVO = pasUtil.getProductAvailability(itemSku, addons,
					deliveryDate, zipCode, country, sourceCode, includeCharges, checkMDAvailable);
				logger.info("paVo: " + paVO.toString());
				logger.info("paVO.isIsAvailable(): " + paVO.isIsAvailable().toString());
				if(paVO != null  && !paVO.isIsAvailable()){
					//Check the call EFA flag. If it is Y, then call the Extended Florist Availability Service to see 
					//if there is a Non-Ftd Florist available to service this order.
					boolean callEFA = false;
					try {
						ConfigurationUtil configUtil = new ConfigurationUtil(); 
						if ((configUtil.getFrpGlobalParm("SERVICE", "CALL_EFA_SERVICE") != null)
			                    && (configUtil.getFrpGlobalParm("SERVICE", "CALL_EFA_SERVICE").equalsIgnoreCase("Y"))) {
							//check to see if efa source code check is set to Y.  If it is, retrieve the list of source codes
							//allowed to call EFA Service
							String efaSourceCodeCheck = configUtil.getFrpGlobalParm("SERVICE","EFA_SOURCE_CODE_CHECK");
							if ("Y".equalsIgnoreCase(efaSourceCodeCheck)) {
								String efaSourceCodesAllowed = configUtil.getFrpGlobalParm("SERVICE","EFA_SOURCE_CODES_ALLOWED");
								if (efaSourceCodesAllowed != null && efaSourceCodesAllowed.length() > 0) {
									String[] efaSourceCodes = efaSourceCodesAllowed.split(",");
									logger.info("Checking if source code is allowed to call EFA");
									for (String efaSourceCode : efaSourceCodes) {
										if(sourceCode.equalsIgnoreCase(efaSourceCode)){
											callEFA = true;
										}
									}
								}
							} else {
					              callEFA = true;
							}
			             } 
					} catch (Exception e) {
						logger.error("Invalid call efa flag");
					}
					logger.info("Call EFA Service: " + callEFA);
					if(callEFA){
						ExtendedFloristAvailabilityRequest extendedFloristAvailabilityRequest = new ExtendedFloristAvailabilityRequest();
						extendedFloristAvailabilityRequest.setProductId(itemSku);
						extendedFloristAvailabilityRequest.setZipCode( zipCode);
						extendedFloristAvailabilityRequest.setChannel("apollo");
						extendedFloristAvailabilityRequest.setSourceCode(sourceCode);
						ExtendedFloristAvailabilityResponse extendedFloristAvailabilityResponse = ExtendedFloristUtil.isAnyFloristAvailable(extendedFloristAvailabilityRequest);
						
						if (null != extendedFloristAvailabilityResponse) {
							logger.info("extendedFloristAvailabilityResponse.isAvailable(): " + extendedFloristAvailabilityResponse.getIsAvailable());
							paVO.setIsAvailable(extendedFloristAvailabilityResponse.getIsAvailable());
							paVO.setIsMorningDeliveryAvailable(false);
							paVO.setHasCharges(true);
							paVO.setCharges (extendedFloristAvailabilityResponse.getShippingCharge());
							//add some value to trigger florist availability
							paVO.setErrorMessage("EFA_FLORIST_AVAILABLE");
							logger.info("paVO after EFA call: " + paVO.toString());
						}
					}					
				}
				logStats(null, serviceMethod, startTime);			 
			} catch (PASTimeoutException e) {
				logger.error("Timeout exception: " + e);
				retryCount++;
				logger.info("retryCount: " + retryCount + " maxRetries: " + maxRetries);
				if (retryCount < maxRetries) {
					pasCallNeeded = true;
				} else {
					logger.error("Max retries reached");
					sendSystemMessage(PAS_NOPAGE_SUBJECT, "Exception caught in getProductAvailability: " + getStackTrace(e));
				}
			} catch (Exception e) {
				logger.error("Exception caught in getProductAvailability: ", e);
				sendSystemMessage(PAS_NOPAGE_SUBJECT, "Exception caught in getProductAvailability as string: " + getStackTrace(e));
			}
		}
		
		if(paVO == null) {
			paVO = new ProductAvailVO();
			logger.info("paVO is null");
		}
		
		return paVO;
	}
	
	/** 
	 * @param productId
	 * @param addons
	 * @param deliveryDate
	 * @param zipCode
	 * @param country
	 * @param sourceCode
	 * @param maxDeliveryDates 
	 * @return
	 */
	public static ProductAvailVO getPASNoSATFirstAvailableDate(String productId, List<String> addons, 
			Calendar deliveryDate, String zipCode, String country, String sourceCode, int maxDeliveryDates) {
		ProductAvailVO paVO = null;
		String serviceMethod = null;
		long startTime = 0;
		
		boolean pasCallNeeded = true;
		int retryCount = 0;
		int maxRetries = 2;
		try {
			ConfigurationUtil configUtil = new ConfigurationUtil(); 
			maxRetries = Integer.valueOf(configUtil.getFrpGlobalParm("PAS_CONFIG", "TIMEOUT_MAX_RETRIES"));
		} catch (Exception e) {
			logger.error("Invalid retry count");
			maxRetries = 2;
		}

		while (pasCallNeeded) {
			
			pasCallNeeded = false;
			
			try {
				PASQueryUtil pasUtil = getPASQueryUtil();
				serviceMethod = GET_PROD_AVAIL_DATES;
				startTime = (new Date()).getTime();
				paVO = pasUtil.getPASNoSATFirstAvailableDate(productId, addons, deliveryDate, zipCode, country, sourceCode, maxDeliveryDates);
				logStats(null, serviceMethod, startTime);
			
			} catch (PASTimeoutException e) {
				logger.error("Timeout exception: " + e);
				retryCount++;
				logger.info("retryCount: " + retryCount + " maxRetries: " + maxRetries);
				if (retryCount < maxRetries) {
					pasCallNeeded = true;
				} else {
					logger.error("Max retries reached");
					sendSystemMessage(PAS_NOPAGE_SUBJECT, "Exception caught in getPASNoSATFirstAvailableDate: " + getStackTrace(e));
				}
			} catch (Exception e) {
				logger.error("Exception caught in getPASNoSATFirstAvailableDate: ", e);
				sendSystemMessage(PAS_NOPAGE_SUBJECT, "Exception caught in getPASNoSATFirstAvailableDate: " + getStackTrace(e));
			}
		}
		if(paVO == null) {
			paVO = new ProductAvailVO();
		}	
		return paVO;
	}
	
	/** 
	 * @param productId
	 * @param addons
	 * @param deliveryDate
	 * @param zipCode
	 * @param country
	 * @param sourceCode
	 * @param maxDeliveryDates 
	 * @return
	 */
	public static ProductAvailVO getPASNoSUNFirstAvailableDate(String productId, List<String> addons, 
			Calendar deliveryDate, String zipCode, String country, String sourceCode, int maxDeliveryDates) {
		ProductAvailVO paVO = null;
		String serviceMethod = null;
		long startTime = 0;
		
		boolean pasCallNeeded = true;
		int retryCount = 0;
		int maxRetries = 2;
		try {
			ConfigurationUtil configUtil = new ConfigurationUtil(); 
			maxRetries = Integer.valueOf(configUtil.getFrpGlobalParm("PAS_CONFIG", "TIMEOUT_MAX_RETRIES"));
		} catch (Exception e) {
			logger.error("Invalid retry count");
			maxRetries = 2;
		}

		while (pasCallNeeded) {
			
			pasCallNeeded = false;
			
			try {
				PASQueryUtil pasUtil = getPASQueryUtil();
				serviceMethod = GET_PROD_AVAIL_DATES;
				startTime = (new Date()).getTime();
				paVO = pasUtil.getPASNoSUNFirstAvailableDate(productId, addons, deliveryDate, zipCode, country, sourceCode, maxDeliveryDates);
				logStats(null, serviceMethod, startTime);
			
			} catch (PASTimeoutException e) {
				logger.error("Timeout exception: " + e);
				retryCount++;
				logger.info("retryCount: " + retryCount + " maxRetries: " + maxRetries);
				if (retryCount < maxRetries) {
					pasCallNeeded = true;
				} else {
					logger.error("Max retries reached");
					sendSystemMessage(PAS_NOPAGE_SUBJECT, "Exception caught in getPASNoSUNFirstAvailableDate: " + getStackTrace(e));
				}
			} catch (Exception e) {
				logger.error("Exception caught in getPASNoSUNFirstAvailableDate: ", e);
				sendSystemMessage(PAS_NOPAGE_SUBJECT, "Exception caught in getPASNoSUNFirstAvailableDate: " + getStackTrace(e));
			}
		}
		if(paVO == null) {
			paVO = new ProductAvailVO();
		}
		return paVO;
	}
	
	/** 
	 * @param productId
	 * @param addons
	 * @param deliveryDate
	 * @param zipCode
	 * @param country
	 * @param sourceCode
	 * @param maxDeliveryDates 
	 * @return
	 */
	public static ProductAvailVO getPASNotSameDayFirstAvailableWeekDate(String productId, List<String> addons, 
			Calendar deliveryDate, String zipCode, String country, String sourceCode, int maxDeliveryDates) {
		ProductAvailVO paVO = null;
		String serviceMethod = null;
		long startTime = 0;
		
		boolean pasCallNeeded = true;
		int retryCount = 0;
		int maxRetries = 2;
		try {
			ConfigurationUtil configUtil = new ConfigurationUtil(); 
			maxRetries = Integer.valueOf(configUtil.getFrpGlobalParm("PAS_CONFIG", "TIMEOUT_MAX_RETRIES"));
		} catch (Exception e) {
			logger.error("Invalid retry count");
			maxRetries = 2;
		}

		while (pasCallNeeded) {
			
			pasCallNeeded = false;
			
			try {
				PASQueryUtil pasUtil = getPASQueryUtil();
				serviceMethod = GET_PROD_AVAIL_DATES;
				startTime = (new Date()).getTime();
				paVO = pasUtil.getPASNotSameDayFirstAvailableWeekDate(productId, addons, deliveryDate, zipCode, country, sourceCode, maxDeliveryDates);
				logStats(null, serviceMethod, startTime);
			
			} catch (PASTimeoutException e) {
				logger.error("Timeout exception: " + e);
				retryCount++;
				logger.info("retryCount: " + retryCount + " maxRetries: " + maxRetries);
				if (retryCount < maxRetries) {
					pasCallNeeded = true;
				} else {
					logger.error("Max retries reached");
					sendSystemMessage(PAS_NOPAGE_SUBJECT, "Exception caught in getPASNotSameDayFirstAvailableWeekDate: " + getStackTrace(e));
				}
			} catch (Exception e) {
				logger.error("Exception caught in getPASNotSameDayFirstAvailableWeekDate: ", e);
				sendSystemMessage(PAS_NOPAGE_SUBJECT, "Exception caught in getPASNotSameDayFirstAvailableWeekDate: " + getStackTrace(e));
			}
		}
		if(paVO == null) {
			paVO = new ProductAvailVO();
		}
		return paVO;
	}
	
	/** 
	 * @param productId
	 * @param addons
	 * @param deliveryDate
	 * @param zipCode
	 * @param country
	 * @param sourceCode
	 * @param maxDeliveryDates 
	 * @return
	 */
	public static ProductAvailVO getPASNotSameDayFirstAvailableDate(String productId, List<String> addons, 
			Calendar deliveryDate, String zipCode, String country, String sourceCode, int maxDeliveryDates) {
		ProductAvailVO paVO = null;
		String serviceMethod = null;
		long startTime = 0;
		
		boolean pasCallNeeded = true;
		int retryCount = 0;
		int maxRetries = 2;
		try {
			ConfigurationUtil configUtil = new ConfigurationUtil(); 
			maxRetries = Integer.valueOf(configUtil.getFrpGlobalParm("PAS_CONFIG", "TIMEOUT_MAX_RETRIES"));
		} catch (Exception e) {
			logger.error("Invalid retry count");
			maxRetries = 2;
		}

		while (pasCallNeeded) {
			
			pasCallNeeded = false;
			
			try {
				PASQueryUtil pasUtil = getPASQueryUtil();
				serviceMethod = GET_PROD_AVAIL_DATES;
				startTime = (new Date()).getTime();
			
				paVO = pasUtil.getPASNotSameDayFirstAvailableDate(productId, addons, deliveryDate, zipCode, country, sourceCode, maxDeliveryDates);
				logStats(null, serviceMethod, startTime);
			
			} catch (PASTimeoutException e) {
				logger.error("Timeout exception: " + e);
				retryCount++;
				logger.info("retryCount: " + retryCount + " maxRetries: " + maxRetries);
				if (retryCount < maxRetries) {
					pasCallNeeded = true;
				} else {
					logger.error("Max retries reached");
					sendSystemMessage(PAS_NOPAGE_SUBJECT, "Exception caught in getPASNotSameDayFirstAvailableDate: " + getStackTrace(e));
				}
			} catch (Exception e) {
				logger.error("Exception caught in getPASNotSameDayFirstAvailableDate: ", e);
				sendSystemMessage(PAS_NOPAGE_SUBJECT, "Exception caught in getPASNotSameDayFirstAvailableDate: " + getStackTrace(e));
			}
		}
		if(paVO == null) {
			paVO = new ProductAvailVO();
		}
		return paVO;
	}
	
	/** 
	 * @param productId
	 * @param addOns
	 * @param zipCode
	 * @param country
	 * @param sourceCode
	 *  
	 * @return 
	 */
	public static Date getNextAvailableDeliveryDate(String productId, List<String> addOns,
			String zipCode, String country, String sourceCode) {
		Date nextAvailDate = null;
		String serviceMethod = null;
		long startTime = 0;
		
		boolean pasCallNeeded = true;
		int retryCount = 0;
		int maxRetries = 2;
		try {
			ConfigurationUtil configUtil = new ConfigurationUtil(); 
			maxRetries = Integer.valueOf(configUtil.getFrpGlobalParm("PAS_CONFIG", "TIMEOUT_MAX_RETRIES"));
		} catch (Exception e) {
			logger.error("Invalid retry count");
			maxRetries = 2;
		}

		while (pasCallNeeded) {
			
			pasCallNeeded = false;
			
			try {
				PASQueryUtil pasUtil = getPASQueryUtil();
				serviceMethod = GET_NEXT_AVAIL_DATE;
				startTime = (new Date()).getTime();
				nextAvailDate = pasUtil.getNextAvailableDeliveryDate(productId, addOns, zipCode, country, sourceCode);
				logger.info("getNextAvailableDeliveryDate:: " + nextAvailDate);
				logStats(null, serviceMethod, startTime);
			
			} catch (PASTimeoutException e) {
				logger.error("Timeout exception: " + e);
				retryCount++;
				logger.info("retryCount: " + retryCount + " maxRetries: " + maxRetries);
				if (retryCount < maxRetries) {
					pasCallNeeded = true;
				} else {
					logger.error("Max retries reached");
					sendSystemMessage(PAS_NOPAGE_SUBJECT, "Exception caught in getNextAvailableDeliveryDate: " + getStackTrace(e));
				}
			} catch (Exception e) {
				logger.error("Exception caught in getNextAvailableDeliveryDate: ", e);
				sendSystemMessage(PAS_NOPAGE_SUBJECT, "Exception caught in getNextAvailableDeliveryDate: " + getStackTrace(e));
			}
		}
		
		return nextAvailDate;
	}
	
	/** 
	 * @param productId
	 * @param deliveryDate
	 * @param country 
	 * @return
	 */
	public static Boolean isInternationalProductAvailable(String productId, Date deliveryDate, String country) {
		Boolean isAvailable = null;
		String serviceMethod = null;
		long startTime = 0;
		
		try {
			PASQueryUtil pasUtil = getPASQueryUtil();
			serviceMethod = IS_INTNL_PROD_AVAIL;
			startTime = (new Date()).getTime();
			isAvailable = pasUtil.isInternationalProductAvailable(productId, deliveryDate, country);
			logger.info("isInternationalProductAvailable:: " + isAvailable);
			logStats(null, serviceMethod, startTime);
			
		} catch (Exception e) {
			logger.error("Exception caught in isInternationalProductAvailable: ", e);
			sendSystemMessage(PAS_NOPAGE_SUBJECT, "Exception caught in isInternationalProductAvailable: " + getStackTrace(e));
		}	
		if(isAvailable == null) {
			isAvailable = false;
		}
		return isAvailable;
	}
	
	
	/** 
	 * @param productId
	 * @param country
	 * @param days 
	 * @return
	 */
	public static ProductAvailVO[] getInternationalProductAvailableDates(String productId, String country, int days) {  
		String serviceMethod = null;
		long startTime = 0;
		
		try {
			PASQueryUtil pasUtil = getPASQueryUtil();
			serviceMethod = GET_INTNL_PROD_AVAIL_DATES;
			startTime = (new Date()).getTime();
			List<ProductAvailVO> productAvailVOs = pasUtil.getInternationalProductAvailableDates(productId, country, days);
			logStats(null, serviceMethod, startTime);
			
			if(productAvailVOs != null && productAvailVOs.size() > 0) {
				return productAvailVOs.toArray(new ProductAvailVO[productAvailVOs.size()]);
			}
		} catch (Exception e) {
			logger.error("Exception caught in getInternationalProductAvailableDates: ", e);
			sendSystemMessage(PAS_NOPAGE_SUBJECT, "Exception caught in getInternationalProductAvailableDates: " + getStackTrace(e));
		}	
		 
		return new ProductAvailVO[0];
	}
	
	/** 
	 * @param productId
	 * @param addOns
	 * @param zipCode
	 * @param country
	 * @param sourceCode
	 * @param days 
	 * @return
	 */
	public static List<ProductAvailVO> getProductAvailableDates(String productId, List<String> addOns, 
			String zipCode, String country, String sourceCode, int days) {
		List<ProductAvailVO> productAvailVOs = null;
		String serviceMethod = null;
		long startTime = 0;
		
		boolean pasCallNeeded = true;
		int retryCount = 0;
		int maxRetries = 2;
		try {
			ConfigurationUtil configUtil = new ConfigurationUtil(); 
			maxRetries = Integer.valueOf(configUtil.getFrpGlobalParm("PAS_CONFIG", "TIMEOUT_MAX_RETRIES"));
		} catch (Exception e) {
			logger.error("Invalid retry count");
			maxRetries = 2;
		}

		while (pasCallNeeded) {
			
			pasCallNeeded = false;
			
			try {
				PASQueryUtil pasUtil = getPASQueryUtil();
				serviceMethod = GET_PROD_AVAIL_DATES;
				startTime = (new Date()).getTime();
				productAvailVOs = pasUtil.getProductAvailableDates(productId, addOns, zipCode, country, sourceCode, days);			
				logStats(null, serviceMethod, startTime);
			
			} catch (PASTimeoutException e) {
				logger.error("Timeout exception: " + e);
				retryCount++;
				logger.info("retryCount: " + retryCount + " maxRetries: " + maxRetries);
				if (retryCount < maxRetries) {
					pasCallNeeded = true;
				} else {
					logger.error("Max retries reached");
					sendSystemMessage(PAS_NOPAGE_SUBJECT, "Exception caught in getProductAvailableDates: " + getStackTrace(e));
				}
			} catch (Exception e) {
				logger.error("Exception caught in getProductAvailableDates: ", e);
				sendSystemMessage(PAS_NOPAGE_SUBJECT, "Exception caught in getProductAvailableDates: " + getStackTrace(e));
			}
		}

		if(productAvailVOs == null) {
			productAvailVOs = new ArrayList<ProductAvailVO>();
		}
		return productAvailVOs;
	}
	
	/** 
	 * @param productId
	 * @param addOns
	 * @param zipCode
	 * @param country 
	 * @return
	 */
	public static ProductAvailVO getPASFirstAvailableDate(String productId, 
			List<String> addOns, String zipCode, String country) {
		ProductAvailVO productAvailVO = null;
		String serviceMethod = null;
		long startTime = 0;
		
		boolean pasCallNeeded = true;
		int retryCount = 0;
		int maxRetries = 2;
		try {
			ConfigurationUtil configUtil = new ConfigurationUtil(); 
			maxRetries = Integer.valueOf(configUtil.getFrpGlobalParm("PAS_CONFIG", "TIMEOUT_MAX_RETRIES"));
		} catch (Exception e) {
			logger.error("Invalid retry count");
			maxRetries = 2;
		}

		while (pasCallNeeded) {
			
			pasCallNeeded = false;
			
			try {
				PASQueryUtil pasUtil = getPASQueryUtil();
				serviceMethod = GET_PROD_AVAIL_DATES;
				startTime = (new Date()).getTime();
				productAvailVO = pasUtil.getPASFirstAvailableDate(productId, addOns, zipCode, country);			
				logStats(null, serviceMethod, startTime);
			
			} catch (PASTimeoutException e) {
				logger.error("Timeout exception: " + e);
				retryCount++;
				logger.info("retryCount: " + retryCount + " maxRetries: " + maxRetries);
				if (retryCount < maxRetries) {
					pasCallNeeded = true;
				} else {
					logger.error("Max retries reached");
					sendSystemMessage(PAS_NOPAGE_SUBJECT, "Exception caught in getPASFirstAvailableDate: " + getStackTrace(e));
				}
			} catch (Exception e) {
				logger.error("Exception caught in getPASFirstAvailableDate: ", e);
				sendSystemMessage(PAS_NOPAGE_SUBJECT, "Exception caught in getPASFirstAvailableDate: " + getStackTrace(e));
			}
		}
		
		if(productAvailVO == null) {
			productAvailVO = new ProductAvailVO();
		}
		return productAvailVO;
	}
	
	/** 
	 * @param deliveryDate
	 * @param zipCode
	 * @param companyId
	 * @param numberOfResults 
	 * @return
	 */
	public static ProductAvailVO[] getMostPopularProducts(Date deliveryDate, 
			String zipCode, String companyId, int numberOfResults) {
		ProductAvailVO[] paVOs = null;
		String serviceMethod = null;
		long startTime = 0;
		
		boolean pasCallNeeded = true;
		int retryCount = 0;
		int maxRetries = 2;
		try {
			ConfigurationUtil configUtil = new ConfigurationUtil(); 
			maxRetries = Integer.valueOf(configUtil.getFrpGlobalParm("PAS_CONFIG", "TIMEOUT_MAX_RETRIES"));
		} catch (Exception e) {
			logger.error("Invalid retry count");
			maxRetries = 2;
		}

		while (pasCallNeeded) {
			
			pasCallNeeded = false;
			
			try {
				PASQueryUtil pasUtil = getPASQueryUtil();
				serviceMethod = GET_MOST_POPULAR_PRODS;
				startTime = (new Date()).getTime();
				paVOs = pasUtil.getMostPopularProducts(deliveryDate, zipCode, companyId, numberOfResults);			
				logStats(null, serviceMethod, startTime);
			
			} catch (PASTimeoutException e) {
				logger.error("Timeout exception: " + e);
				retryCount++;
				logger.info("retryCount: " + retryCount + " maxRetries: " + maxRetries);
				if (retryCount < maxRetries) {
					pasCallNeeded = true;
				} else {
					logger.error("Max retries reached");
					sendSystemMessage(PAS_NOPAGE_SUBJECT, "Exception caught in getMostPopularProducts as stackTrace: " + getStackTrace(e));
				}
			} catch (Exception e) {
				logger.error("Exception caught in getMostPopularProducts: ", e);
				sendSystemMessage(PAS_NOPAGE_SUBJECT, "Exception caught in getMostPopularProducts as stackTrace: " + getStackTrace(e));
			}
		}
		if(paVOs == null) {
			paVOs = new ProductAvailVO[0];
		}
		return paVOs;		
	}
	
	/** 
	 * @param deliveryDate
	 * @param zipCode 
	 * @return
	 */
	public static String[] getAvailableFlorists(Date deliveryDate, String zipCode) {
		String[] florists = null;
		String serviceMethod = null;
		long startTime = 0;
		
		try {
			PASQueryUtil pasUtil = getPASQueryUtil();
			serviceMethod = GET_FLORISTS;
			startTime = (new Date()).getTime();
			florists = pasUtil.getAvailableFlorists(deliveryDate, zipCode);			
			logStats(null, serviceMethod, startTime);
			
		} catch (Exception e) {
			logger.error("Exception caught in getAvailableFlorists: ", e);
			sendSystemMessage(PAS_NOPAGE_SUBJECT, "Exception caught in getAvailableFlorists: " + getStackTrace(e));
		}
		if(florists == null) {
			florists = new String[0];
		}
		return florists;		
	}
	
	/** 
	 * @param deliveryDate
	 * @param zipCode 
	 * @return
	 */
	public static Boolean isAnyProductAvailable(Date deliveryDate, String zipCode) {
		Boolean isAnyProductAvailable = null;
		String serviceMethod = null;
		long startTime = 0;
		
		try {
			PASQueryUtil pasUtil = getPASQueryUtil();
			serviceMethod = IS_ANY_PROD_AVAIL;
			startTime = (new Date()).getTime();
			
			isAnyProductAvailable = pasUtil.isAnyProductAvailable(deliveryDate, zipCode);			
			logger.info("isAnyProductAvailable:: " + isAnyProductAvailable);
			logStats(null, serviceMethod, startTime);
			
		} catch (Exception e) {
			logger.error("Exception caught in isAnyProductAvailable: ", e);
			sendSystemMessage(PAS_NOPAGE_SUBJECT, "Exception caught in isAnyProductAvailable: " + getStackTrace(e));
		}
		if(isAnyProductAvailable == null) {
			isAnyProductAvailable = false;
		}
		return isAnyProductAvailable;
	}
	
	/** 
	 * @param deliveryDate
	 * @param zipCode 
	 * @return
	 */
	public static Boolean isProductAvailable(String productId, Date deliveryDate, String zipCode) {
		Boolean isProductAvailable = null;
		String serviceMethod = null;
		long startTime = 0;
		
		boolean pasCallNeeded = true;
		int retryCount = 0;
		int maxRetries = 2;
		try {
			ConfigurationUtil configUtil = new ConfigurationUtil(); 
			maxRetries = Integer.valueOf(configUtil.getFrpGlobalParm("PAS_CONFIG", "TIMEOUT_MAX_RETRIES"));
		} catch (Exception e) {
			logger.error("Invalid retry count");
			maxRetries = 2;
		}

		while (pasCallNeeded) {
			
			pasCallNeeded = false;
			
			try {
				PASQueryUtil pasUtil = getPASQueryUtil();
				serviceMethod = IS_PROD_AVAIL;
				startTime = (new Date()).getTime();
				isProductAvailable = pasUtil.isProductAvailable(productId, deliveryDate, zipCode);	
				logger.info("isProductAvailable:: " + isProductAvailable);	
				logStats(null, serviceMethod, startTime);
			
			} catch (PASTimeoutException e) {
				logger.error("Timeout exception: " + e);
				retryCount++;
				logger.info("retryCount: " + retryCount + " maxRetries: " + maxRetries);
				if (retryCount < maxRetries) {
					pasCallNeeded = true;
				} else {
					logger.error("Max retries reached");
					sendSystemMessage(PAS_NOPAGE_SUBJECT, "Exception caught in isProductAvailable: " + getStackTrace(e));
				}
			} catch (Exception e) {
				logger.error("Exception caught in isProductAvailable: ", e);
				sendSystemMessage(PAS_NOPAGE_SUBJECT, "Exception caught in isProductAvailable: " + getStackTrace(e));
			}
		}
		if(isProductAvailable == null) {
			isProductAvailable = false;
		}
		return isProductAvailable;
	}
	 
	/** 
	 * @param productId
	 * @param addOns
	 * @param zipCode
	 * @param sourceCode
	 * @param days 
	 * @return
	 */
	public static PasShipDatesAvailVO getAvailableShipDates(String productId, 
			List<String> addOns, String zipCode, String sourceCode, int days) {
		PasShipDatesAvailVO pasShipDatesAvailVO = null;
		String serviceMethod = null;
		long startTime = 0;
		
		boolean pasCallNeeded = true;
		int retryCount = 0;
		int maxRetries = 2;
		try {
			ConfigurationUtil configUtil = new ConfigurationUtil(); 
			maxRetries = Integer.valueOf(configUtil.getFrpGlobalParm("PAS_CONFIG", "TIMEOUT_MAX_RETRIES"));
		} catch (Exception e) {
			logger.error("Invalid retry count");
			maxRetries = 2;
		}

		while (pasCallNeeded) {
			
			pasCallNeeded = false;
			
			try {
				PASQueryUtil pasUtil = getPASQueryUtil();
				serviceMethod = GET_AVAIL_SHIP_DATES;
				startTime = (new Date()).getTime();
				pasShipDatesAvailVO = pasUtil.getAvailableShipDates(productId, addOns, zipCode, sourceCode, days);
				logger.info("pasShipDatesAvailVO: " + pasShipDatesAvailVO.toString());
				logStats(null, serviceMethod, startTime);
			
			} catch (PASTimeoutException e) {
				logger.error("Timeout exception: " + e);
				retryCount++;
				logger.info("retryCount: " + retryCount + " maxRetries: " + maxRetries);
				if (retryCount < maxRetries) {
					pasCallNeeded = true;
				} else {
					logger.error("Max retries reached");
					sendSystemMessage(PAS_NOPAGE_SUBJECT, "Exception caught in getAvailableShipDates: " + getStackTrace(e));
				}
			} catch (Exception e) {
				logger.error("Exception caught in getAvailableShipDates: ", e);
				sendSystemMessage(PAS_NOPAGE_SUBJECT, "Exception caught in getAvailableShipDates: " + getStackTrace(e));
			}
		}
		
		if(pasShipDatesAvailVO == null) {
			pasShipDatesAvailVO = new PasShipDatesAvailVO();
		}
		return pasShipDatesAvailVO;		
	}
	
	/**
	 * @param availableDates
	 * @return
	 */
	public static List<OEDeliveryDate> getOEDeliveryDates(ProductAvailVO[] availableDates) {
		List<OEDeliveryDate> dates = new ArrayList<OEDeliveryDate>();
		DateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		DateFormat sdfDay = new SimpleDateFormat("EEE");
		    
		if(availableDates != null) {
			for (int i = 0; i < availableDates.length; i++) {
				ProductAvailVO paVO = availableDates[i];
				OEDeliveryDate tempDate = new OEDeliveryDate();
				tempDate.setDeliveryDate(sdf.format(paVO.getDeliveryDate()
						.getTime()));
				tempDate.setDisplayDate(sdf
						.format(paVO.getDeliveryDate().getTime()));
				tempDate.setDayOfWeek(sdfDay.format(paVO.getDeliveryDate()
						.getTime()));
				tempDate.setDeliverableFlag("Y");
				tempDate.setInDateRange(false);
				tempDate.setHolidayText("");
				List<ShippingMethod> shipMethods = new ArrayList<ShippingMethod>();
				if (paVO.getShipDateND() != null) {
					shipMethods.add(getShippingMethodVO("ND", "Next Day Delivery"));
				}
				if (paVO.getShipDate2D() != null) {
					shipMethods.add(getShippingMethodVO("2F", "Two Day Delivery"));
				}
				if (paVO.getShipDateGR() != null) {
					shipMethods.add(getShippingMethodVO("GR", "Standard Delivery"));
				}
				if (paVO.getShipDateSA() != null) {
					shipMethods.add(getShippingMethodVO("SA", "Saturday Delivery"));
				}
				if (paVO.getShipDateSU() != null) {
					shipMethods.add(getShippingMethodVO("SU", "Sunday Delivery"));
				}
	
				tempDate.setShippingMethods(shipMethods);
				dates.add(tempDate);
			}
		}		
		return dates;
	}
	
	/**
	 * @param pasShipDates
	 * @return
	 */
	public static Set<String> getPASShipDates(PasShipDatesAvailVO pasShipDates) {
		Set<String> calDates = new HashSet<String>();
		DateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		
		if(pasShipDates != null) {
			Date[] calGD = pasShipDates.getGroundDeliveryDates();
			if (calGD != null) {
				for (int i = 0; i < calGD.length; i++) {
					calDates.add(sdf.format(calGD[i]));
				}
			}
			Date[] calND = pasShipDates.getNextDayDeliveryDates();
			if (calND != null) {
				for (int i = 0; i < calND.length; i++) {
					calDates.add(sdf.format(calND[i].getTime()));
				}
			}
			Date[] calSD = pasShipDates.getSaturdayDeliveryDates();
			if (calSD != null) {
				for (int i = 0; i < calSD.length; i++) {
					calDates.add(sdf.format(calSD[i].getTime()));
				}
			}
			Date[] calTD = pasShipDates.getTwoDayDeliveryDates();
			if (calTD != null) {
				for (int i = 0; i < calTD.length; i++) {
					calDates.add(sdf.format(calTD[i].getTime()));
				}
			}
			Date[] calSU = pasShipDates.getSundayDeliveryDates();
			if (calSU != null) {
				for (int i = 0; i < calSU.length; i++) {
					calDates.add(sdf.format(calSU[i].getTime()));
				}
			}
		}		
		return calDates;
	}
	
	/**
	 * @param smCode
	 * @param smDesc
	 * @return
	 */
	private static ShippingMethod getShippingMethodVO(String smCode, String smDesc) {
		ShippingMethod sm = new ShippingMethod();
		sm.setCode(smCode);
		sm.setDescription(smDesc);
		return sm;
	}
	
	/**
	 * Send System Messages 
	 * @param errorMessage
	 */
	private static void sendSystemMessage(String subject, String errorMessage) {
		logger.error("Sending system message. Message = " + errorMessage);
		DataRequest dataRequest = null;
		try {
			
			dataRequest = new DataRequest();
			dataRequest.setConnection(DataSourceUtil.getInstance().getConnection("ORDER SCRUB"));
			
			SystemMessengerVO sysMessage = new SystemMessengerVO();
			sysMessage.setLevel(SystemMessengerVO.LEVEL_PRODUCTION);
			sysMessage.setSource(PAS_CLIENT_SOURCE);
			sysMessage.setType("System Exception");
			sysMessage.setMessage(errorMessage);
			sysMessage.setSubject(subject);
			SystemMessenger.getInstance().send(sysMessage, dataRequest.getConnection());

		} catch (Throwable t) {
			logger.error("Sending system message failed.");
			logger.error(t);
		} finally {
			closeConnection(dataRequest);
		}
	}
	
	/**
	 * @param requestId
	 * @param serviceMethod
	 * @param startTime
	 */
	private static void logStats(String transactionId,  String serviceMethod, long startTime) { 
		if(serviceMethod == null) {
			return;
		}
		DataRequest dataRequest = null;
		try {
			logger.info("Logging stats for PAS Service");
			dataRequest = new DataRequest();
			dataRequest.setConnection(DataSourceUtil.getInstance().getConnection("ORDER SCRUB"));
			
			ServiceResponseTrackingUtil srtUtil = new ServiceResponseTrackingUtil();
			srtUtil.logServiceResponseTracking(transactionId, serviceMethod, startTime, dataRequest.getConnection(), PAS_SERVICE);
			
		} catch (Exception e) { 
			logger.error("Error caught inserting service tracking record for PAS Service, ", e);
		} finally {
			closeConnection(dataRequest);
		}
		
	}
	
	private static void closeConnection(DataRequest dataRequest) {
		try {
			if(dataRequest != null && dataRequest.getConnection() != null && !dataRequest.getConnection().isClosed()) {
				dataRequest.getConnection().close();
			}
		} catch (Exception e) {
			logger.error("Error caught closing the connection, " + e.getMessage());
		}
	}

	/**
	 * @param ex
	 * @return
	 */
	private static String getStackTrace(Exception ex) {
		StringWriter sw = new StringWriter();
	      PrintWriter pw = new PrintWriter(sw);
	      if (ex == null) return null;
	      ex.printStackTrace(pw);
	      return sw.toString();
	}
	
	
	
	
}

