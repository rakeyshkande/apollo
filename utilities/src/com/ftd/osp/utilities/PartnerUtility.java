package com.ftd.osp.utilities;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ftd.osp.ordervalidator.util.ValidationConstants;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.handlers.PartnerMappingsHandler;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;
import com.ftd.osp.utilities.vo.PartnerMappingVO;

public class PartnerUtility {
	private static Logger logger = new Logger(
			"com.ftd.osp.utilities.PartnerUtility");
	
	private static int PRINCIPAL_AMT_POS = 0;
	private static int ADDON_AMT_POS = 1;
	private static int SHIPPING_AMT_POS = 2;
	private static int SHIPPING_TAX_AMT_POS = 3;
	private static int TAX_AMT_POS = 4;
	private static int REF_PRNCPL_AMT_POS = 5;
	private static int REF_ADDON_AMT_POS = 6;

	public PartnerUtility() {
	}

	/*
	 * Creates an adjustment feed record for a single item
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void createAdjustmentFeedRecord(OrderDetailsVO item, String csrID,
			String adjustmentReason, Connection connection) throws Exception {
		BigDecimal products, shippingFee, taxAmount, shippingTax;

		// if shipping method is carrier delivered, then get shipping fee
		// from the shipping fee field on the item
		if (item.getShipMethod().equals(ValidationConstants.SAME_DAY_CARRIER)
				|| item.getShipMethod().equals(ValidationConstants.DROP_SHIP)) {
			shippingFee = (item.getShippingFeeAmount() == null) ? new BigDecimal(
					"0") : new BigDecimal(item.getShippingFeeAmount());
		}
		// if shipping method is florist, then get the service fee for this
		// item and set it to the shippingFee
		else {
			shippingFee = (item.getServiceFeeAmount() == null) ? new BigDecimal(
					"0") : new BigDecimal(item.getServiceFeeAmount());
		}

		// order values
		products = (item.getProductsAmount() == null) ? new BigDecimal("0")
				: new BigDecimal(item.getProductsAmount());
		shippingFee = (item.getShippingFeeAmount() == null) ? new BigDecimal(
				"0") : new BigDecimal(item.getShippingFeeAmount());
		taxAmount = (item.getTaxAmount() == null) ? new BigDecimal("0")
				: new BigDecimal(item.getTaxAmount());
		shippingTax = (item.getShippingTax() == null) ? new BigDecimal("0")
				: new BigDecimal(item.getShippingTax());
		try {
			DataAccessUtil dau = DataAccessUtil.getInstance();
			DataRequest request = new DataRequest();
			request.setConnection(connection);
			HashMap inputParams = new HashMap();
			inputParams.put("confirmation number",
					item.getExternalOrderNumber());
			inputParams.put("reason", adjustmentReason);
			inputParams.put("principal", products);
			inputParams.put("shipping", shippingFee);
			inputParams.put("tax", taxAmount);
			inputParams.put("shippingTax", shippingTax);
			inputParams.put("created by csr user name", csrID);
			inputParams.put("updated by csr user name", csrID);
			request.setInputParams(inputParams);
			request.setStatementID("insert_amazon_adjustment");

			Map outputParameters = (Map) dau.execute(request);

			// Integer id = (Integer) outputParameters.get("related Id");
			String status = (String) outputParameters.get("status");
			String message = (String) outputParameters.get("message");

			if (status.equalsIgnoreCase("N")) {
				throw new Exception("createOrderAdjustment caught: " + message);
			}
		} catch (SQLException se) {
			logger.error("exception thrown while executing query."
					+ se.toString());
			throw new Exception("Unable to access data.");
		} catch (Exception e) {
			logger.error("exception caught in createAdjustmentFeedRecord: " + e.toString());
			throw new Exception("createOrderAdjustment caught: " + e);
		}

	}// end method createAdjustmentFeedRecord(OrderVO)
	
	/** Method to insert the mercent order adjustment record - 
	 * This method is called when the order total are adjusted in scrub and adjusted 
	 * amounts are less than the actual order totals.
	 * @param item
	 * @param csrID
	 * @param adjustmentReason
	 * @param connection
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public void createMercentAdjustmentFeedRecord(OrderDetailsVO item, String csrID, 
			String adjustmentReason, boolean fullAdjustment, Connection connection) throws Exception {
		
		DataAccessUtil dau = DataAccessUtil.getInstance();
		DataRequest dataRequest = new DataRequest();
		dataRequest.setConnection(connection);
		Map<String, Object> inputParams;
				
		try {
			// #15754 - Get the original amounts on the Mercent order
			BigDecimal[] amtsToRefund = calculateAdjustedAmounts(connection, item, fullAdjustment);	
			if(amtsToRefund == null) {
				throw new Exception("Unable to calculated amounts to be refunded");
			}
			
			inputParams = new HashMap<String, Object>();
			inputParams.put("confirmation number", item.getExternalOrderNumber());
			inputParams.put("reason", adjustmentReason);
			
			inputParams.put("principal", amtsToRefund[PRINCIPAL_AMT_POS]);
			inputParams.put("shipping", amtsToRefund[SHIPPING_AMT_POS]);			
			inputParams.put("shippingTax", amtsToRefund[SHIPPING_TAX_AMT_POS]);	
			inputParams.put("tax", amtsToRefund[TAX_AMT_POS]);
			
			inputParams.put("created by csr user name", csrID);
			inputParams.put("updated by csr user name", csrID);
			
			dataRequest.setInputParams(inputParams);
			dataRequest.setStatementID("insert_mercent_adjustment");
			Map<String, Object> outputParameters = (Map<String, Object>) dau.execute(dataRequest);

			String status = (String) outputParameters.get("status");
			String message = (String) outputParameters.get("message");

			if (status.equalsIgnoreCase("N")) {
				logger.error("Unable to insert mercent order adjustment for the order " + item.getExternalOrderNumber() + " - " + message);
				throw new Exception("Error caught creating mercent order adjustment : " + message);
			}
			
		} catch (SQLException se) {
			logger.error("exception thrown while executing query." 	+ se.toString());
			throw new Exception("Unable to access data.");
		} catch (Exception e) {
			logger.error("exception caught in createMercentAdjustmentFeedRecord: " + e.toString());
			sendSystemMessage(e.getMessage());	
		} 
	}
	
	
	/**Calculate the amounts to be adjusted/ refunded	
	 * @param connection
	 * @param item
	 * @throws Exception
	 */
	private BigDecimal[] calculateAdjustedAmounts(Connection connection, OrderDetailsVO item, boolean fullAdjustment) throws Exception {
		BigDecimal[] originalOrderAmounts = null;
		BigDecimal[] amtsToRefund = null;
		
		try { 
			originalOrderAmounts = getOriginalOrderAmounts(connection, item, "PARTNER"); 
			
			if(originalOrderAmounts != null) {
				
				if(originalOrderAmounts[ADDON_AMT_POS] == null){
					originalOrderAmounts[ADDON_AMT_POS] = new BigDecimal("0");
				}
				
				amtsToRefund = new BigDecimal[7];	
				if (fullAdjustment) {
					amtsToRefund[PRINCIPAL_AMT_POS] = originalOrderAmounts[PRINCIPAL_AMT_POS];
					amtsToRefund[ADDON_AMT_POS] = originalOrderAmounts[ADDON_AMT_POS];
					amtsToRefund[SHIPPING_AMT_POS] = originalOrderAmounts[SHIPPING_AMT_POS];
					amtsToRefund[SHIPPING_TAX_AMT_POS] = originalOrderAmounts[SHIPPING_TAX_AMT_POS];
					amtsToRefund[TAX_AMT_POS] = originalOrderAmounts[TAX_AMT_POS];
				} else {
				    amtsToRefund[PRINCIPAL_AMT_POS] = 
					    originalOrderAmounts[PRINCIPAL_AMT_POS].subtract((item.getProductsAmount() == null) ? new BigDecimal("0") : new BigDecimal(item.getProductsAmount())						    
						    .subtract((item.getDiscountAmount() == null) ? new BigDecimal("0"): new BigDecimal(item.getDiscountAmount())));
				    
				  
				    amtsToRefund[ADDON_AMT_POS] = originalOrderAmounts[ADDON_AMT_POS].subtract(
				    		(item.getAddOnAmount() == null) ? new BigDecimal("0"): new BigDecimal(item.getAddOnAmount())
				    				.subtract((item.getAddOnDiscountAmount() == null ) ? new BigDecimal("0") :new BigDecimal(item.getAddOnDiscountAmount())));
				   
				   // setting the actual principal and addon refund values
				    amtsToRefund[REF_PRNCPL_AMT_POS] = amtsToRefund[PRINCIPAL_AMT_POS];
					amtsToRefund[REF_ADDON_AMT_POS] = amtsToRefund[ADDON_AMT_POS];
				    
					//Adjusting the refund principal and addon values
					//Applying refund to Addon when Product sale price has increased
					if(amtsToRefund[PRINCIPAL_AMT_POS].compareTo(new BigDecimal("0"))<0 && amtsToRefund[ADDON_AMT_POS].compareTo(new BigDecimal("0"))>0){  
						amtsToRefund[ADDON_AMT_POS] = amtsToRefund[ADDON_AMT_POS].add(amtsToRefund[PRINCIPAL_AMT_POS]);
				    	amtsToRefund[PRINCIPAL_AMT_POS] = new BigDecimal("0");
				    }
					//Applying refund to Product when Addon sale price has increased
					else if(amtsToRefund[ADDON_AMT_POS].compareTo(new BigDecimal("0"))<0 && amtsToRefund[PRINCIPAL_AMT_POS].compareTo(new BigDecimal("0"))>0){ 
				    	amtsToRefund[PRINCIPAL_AMT_POS] = amtsToRefund[ADDON_AMT_POS].add(amtsToRefund[PRINCIPAL_AMT_POS]);
				    	amtsToRefund[ADDON_AMT_POS] = new BigDecimal("0");
				    }
				    
					
				    BigDecimal serviceFeeAmt = (item.getServiceFeeAmount() == null) ? new BigDecimal("0") : new BigDecimal(item.getServiceFeeAmount());
				    BigDecimal shippingFeeAmt = (item.getShippingFeeAmount() == null) ? new BigDecimal("0") : new BigDecimal(item.getShippingFeeAmount());
				    amtsToRefund[SHIPPING_AMT_POS] = originalOrderAmounts[SHIPPING_AMT_POS].subtract(serviceFeeAmt.add(shippingFeeAmt));
				    
				    // Default to 0
				    amtsToRefund[SHIPPING_TAX_AMT_POS] = new BigDecimal(0);
				    amtsToRefund[TAX_AMT_POS]  = new BigDecimal(0);
				   
					amtsToRefund[TAX_AMT_POS] = originalOrderAmounts[TAX_AMT_POS]
						.subtract((item.getTaxAmount() == null) ? new BigDecimal("0") : new BigDecimal(item.getTaxAmount()))
						.subtract((item.getShippingTax() == null) ? new BigDecimal("0") : new BigDecimal(item.getShippingTax()))
						.subtract((item.getServiceFeeTax() == null) ? new BigDecimal("0") : new BigDecimal(item.getServiceFeeTax()));
	
				   
				}
			}
			logger.info("fullAdjustment: " + fullAdjustment);
			logger.info("Principal: " + amtsToRefund[PRINCIPAL_AMT_POS]);
			logger.info("Addon : "+amtsToRefund[ADDON_AMT_POS]);
			logger.info("Shipping: " + amtsToRefund[SHIPPING_AMT_POS]);
			logger.info("Shipping Tax: " + amtsToRefund[SHIPPING_TAX_AMT_POS]);
			logger.info("Tax: " + amtsToRefund[TAX_AMT_POS]);
		} catch (Exception e) {
			logger.error("exception caught in calculateAdjustedAmounts: " + e.toString());			
		}
		return amtsToRefund;
		
	}

	/** Get the original mercent order amounts
	 * @param connection
	 * @param item
	 * @throws Exception
	 */
	public BigDecimal[] getOriginalOrderAmounts(Connection connection, OrderDetailsVO item, String partnerType) throws Exception {
		
		BigDecimal[] originalAmts = null;
		
		try {
			if(logger.isDebugEnabled()) {
				logger.debug("Getting Original order amounts for partner - " + partnerType + ", order - " + item.getExternalOrderNumber());
			}
			
			DataAccessUtil dau = DataAccessUtil.getInstance();
			DataRequest dataRequest = new DataRequest();
			dataRequest.setConnection(connection);
						
			Map<String, Object> inputParams = new HashMap<String,Object>();
			inputParams.put("IN_CONFIRMATION_NUMBER", item.getExternalOrderNumber());
		
			dataRequest.setInputParams(inputParams);
			
			if("MERCENT".equals(partnerType)) {
				dataRequest.setStatementID("GET_MRCNT_ORD_ITEM_DETAIL");
			} else if("PARTNER".equals(partnerType)) {
				dataRequest.setStatementID("GET_PTN_ORD_ITEM_DETAIL");
			} else {
				logger.error("Invalid partner type");
				return null;
			}
						
			CachedResultSet ordAmounts = (CachedResultSet) dau.execute(dataRequest);
			
			if(ordAmounts == null) {
				logger.error("Unable to get the Original order amounts for partner - " + partnerType);	
				throw new Exception("Unable to get the Original order amounts for partner - " + partnerType);	
			}			
			
			if(ordAmounts.next()) {	
				originalAmts = new BigDecimal[5];
				if("PARTNER".equals(partnerType)) {					
					if(!isEmpty(ordAmounts.getString("SALE_PRICE"))) {
						originalAmts[PRINCIPAL_AMT_POS] = new BigDecimal(ordAmounts.getString("SALE_PRICE"));						
					}
					if(!isEmpty(ordAmounts.getString("ADD_ON_SALE_PRICE"))) {
						originalAmts[ADDON_AMT_POS] = new BigDecimal(ordAmounts.getString("ADD_ON_SALE_PRICE"));						
					}
					
					// there is no shipping/service tax for partner orders.
					originalAmts[SHIPPING_TAX_AMT_POS] = new BigDecimal(0);
				} else if("MERCENT".equals(partnerType)) {
					if(!isEmpty(ordAmounts.getString("PRINCIPAL_AMT"))) {				
						originalAmts[PRINCIPAL_AMT_POS] = new BigDecimal(ordAmounts.getString("PRINCIPAL_AMT"));
					}
					if(!isEmpty(ordAmounts.getString("SHIPPING_TAX_AMT"))) {
						originalAmts[SHIPPING_TAX_AMT_POS] = new BigDecimal(ordAmounts.getString("SHIPPING_TAX_AMT"));
					}
				}				
				if(!isEmpty(ordAmounts.getString("SHIPPING_AMT"))) {
					originalAmts[SHIPPING_AMT_POS] = new BigDecimal(ordAmounts.getString("SHIPPING_AMT"));
				}				
				if(!isEmpty(ordAmounts.getString("TAX_AMT"))) {
					originalAmts[TAX_AMT_POS] = new BigDecimal(ordAmounts.getString("TAX_AMT"));
				}								
			}
			
			return originalAmts;
			
		} catch (Exception e) {
			logger.error("Error caught obtaining original order amounts for partner : " + partnerType + ", error - " + e.getMessage());
			throw new Exception(e);
		}		
	}

	/** Convenient method to send a system message
	 * @param errorMessage
	 * @throws SQLException 
	 */
	private static void sendSystemMessage(String errorMessage) {
		Connection conn = null;
		try {
			conn = DataSourceUtil.getInstance().getConnection("ORDER SCRUB");
			SystemMessengerVO sysMessage = new SystemMessengerVO();
			sysMessage.setLevel(SystemMessengerVO.LEVEL_PRODUCTION);
			sysMessage.setSource("ORDER SCRUB - PARTNER UTILITY");
			sysMessage.setType("ERROR");
			sysMessage.setMessage(errorMessage);
			SystemMessenger.getInstance().send(sysMessage, conn);

		} catch (Throwable t) {
			logger.error("Sending system message failed. Message: " + errorMessage);
			logger.error(t);
		} finally {
			try {
				if(conn != null && !conn.isClosed()) {
					conn.close();
				}
			} catch (Exception e) {
				logger.error("Unable to close the connection.");
			}
		}
	}
	
	/**
	 * @param partnerOrderNum
	 * @param partnerOriginId
	 * @param connection
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public CachedResultSet getPartnerOrderInfoByOrderNumber(String partnerOrderNum, String partnerOriginId, Connection connection) {
		DataRequest dataRequest = new DataRequest();
		CachedResultSet crs = null;
		try {
			HashMap<String, String> inputParams = new HashMap<String, String>();
			inputParams.put("IN_PARTNER_ORDER_NUMBER", partnerOrderNum);
			inputParams.put("IN_PARTNER_ORIGIN_ID", partnerOriginId);
			dataRequest.setInputParams(inputParams);
			dataRequest.setConnection(connection);
			dataRequest.setStatementID("GET_PARTNER_ORDER_INFO");
			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
			Map result = (Map) dataAccessUtil.execute(dataRequest);
			if (result != null) {
				String status = (String) result.get("OUT_STATUS");
				if (!"Y".equalsIgnoreCase(status)) {
					String message = (String) result.get("OUT_MESSAGE");
					throw new Exception(message);
				}
				crs = (CachedResultSet) result.get("OUT_CUR");
			}
		} catch (Exception e) {
			logger.error("Error caught obtaining partner order info : " + e.getMessage());
		}
		return crs;
	}

	/**
	 * @param partnerOriginId
	 * @param connection
	 * @return
	 */
	public PartnerMappingVO getPartnerOriginsInfo(String partnerOriginId, String sourceCode, Connection connection) {		
		try {
			return getPartnerMappingVO(partnerOriginId, sourceCode);					
		} catch (Exception e) {
			logger.error("Error caught obtaining partner origins info from cache: " + e.getMessage() + ", checking the DB.");			//
			if(connection != null) {
				return getPartnerMappingInfo(connection, partnerOriginId, sourceCode);
			} else {
				logger.error("Unable to determine if origin is partner order origin, connection is null");
			}
		}
		return null;
	}
	
	/**
	 * @param partnerOriginId
	 * @param connection
	 * @return
	 */
	public boolean isPartnerOrder(String partnerOriginId, String sourceCode, Connection connection) {
		try {
			PartnerMappingVO partnerMapping = getPartnerOriginsInfo(partnerOriginId, sourceCode, connection);			
			if(partnerMapping != null && partnerMapping.isDefaultBehaviour()) {
				return true;
			}
		} catch (Exception e) {
			logger.error("Error caught obtaining partner origins info : " + e.getMessage());
		}
		return false;
	}
	
	/** Get the partner mapping info for a given order origin id/ ftd origin
	 * @param connection
	 * @param partnerOriginId
	 * @param sourceCode
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	private PartnerMappingVO getPartnerMappingInfo(Connection connection, String partnerOriginId, String sourceCode) {
		DataRequest dataRequest = new DataRequest();
		try {
			HashMap<String, String> inputParams = new HashMap<String, String>();
			inputParams.put("IN_PARTNER_ORIGIN_ID", partnerOriginId);
			inputParams.put("IN_SOURCE_CODE", sourceCode);
			dataRequest.setInputParams(inputParams);
			dataRequest.setConnection(connection);
			dataRequest.setStatementID("GET_PARTNER_ORIGIN_INFO");
			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
			Map result = (Map) dataAccessUtil.execute(dataRequest);
			if (result != null) {
				String status = (String) result.get("OUT_STATUS");
				if (!"Y".equalsIgnoreCase(status)) {
					String message = (String) result.get("OUT_MESSAGE");
					throw new Exception(message);
				}
				return new PartnerMappingsHandler().populatePartnerMappingVO((CachedResultSet) result.get("OUT_CUR"));
			}
		} catch (Exception e) {
			logger.error("Error caught obtaining partner origins info : " + e.getMessage());
		}
		return null;
	}
	
	/** Get the partner mapping VO for the given origin Id. 
	 * The object of parter mappings cache holds partner mapping VO mapped to partner id and not origin id. 
	 * So iterate through the values to get the partner mapping VO for a given origin id.
	 * @param originId
	 * @return
	 * @throws Exception
	 */
	private PartnerMappingVO getPartnerMappingVO(String originId, String sourceCode) throws Exception {
		try {
			if(isEmpty(originId)) {
				logger.debug("Cannot get the partner mapping vo for an empty originId");
				return null;
			}
			PartnerMappingsHandler partnerMappingHandler = 
				(PartnerMappingsHandler) CacheManager.getInstance().getHandler("CACHE_NAME_PARTNER_MAPPINGS");
			Map<String, PartnerMappingVO> partnerMappings = partnerMappingHandler.getPartnerMappings();

			for (PartnerMappingVO partnerMappingVO : partnerMappings.values()) {
				if (originId.equals(partnerMappingVO.getFtdOriginMapping())
						&& (sourceCode != null && sourceCode.equals(partnerMappingVO.getDefaultSourceCode()))) {
					return partnerMappingVO;
				}
			}
		} catch (Exception e) {
			logger.error("Error caught obtaining partner origins info : " + e.getMessage());
			throw e;
		}
		return null;
	}
	
	/** Get all partners mapping info.
	 * @return
	 */
	public List<PartnerMappingVO> getAllPartnersMappingInfo() {
		try {
			PartnerMappingsHandler partnerMappingHandler = 
				(PartnerMappingsHandler) CacheManager.getInstance().getHandler("CACHE_NAME_PARTNER_MAPPINGS");
			Map<String, PartnerMappingVO> partnerMappings = partnerMappingHandler.getPartnerMappings();
			if(partnerMappings != null && partnerMappings.size() > 0) {
				return new ArrayList<PartnerMappingVO>(partnerMappings.values());
			}
		} catch (Exception e) {
			logger.error("Error caught obtaining partner origins info : " + e.getMessage());			
		}
		return null;
	}
	
	/** Method to insert the partner order adjustment records - 
	 * This method is called when the order totals are adjusted in scrub and adjusted 
	 * amounts are less than the actual order totals.
	 * @param item
	 * @param csrID
	 * @param adjustmentReason
	 * @param connection
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public void createPartnerAdjustmentFeedRecord(OrderDetailsVO item, String csrID, 
			String adjustmentReason, boolean fullAdjustment, Connection connection) throws Exception {
		
		DataAccessUtil dau = DataAccessUtil.getInstance();
		DataRequest dataRequest = new DataRequest();
		dataRequest.setConnection(connection);
		Map<String, Object> inputParams;
				
		try {
			// Get the original amounts on the Partner order
			BigDecimal[] amtsToRefund = calculateAdjustedAmounts(connection, item, fullAdjustment);	
			if(amtsToRefund == null || amtsToRefund.length == 0) {
				throw new Exception("Unable to calculate amounts to be refunded for partner orders");
			}
						 			
			inputParams = new HashMap<String, Object>();
			
			inputParams.put("IN_CONFIRMATION_NUMBER", item.getExternalOrderNumber());
			inputParams.put("IN_ADJUSTMENT_TYPE", "REFUND");
			inputParams.put("IN_ADJUSTMENT_REASON", adjustmentReason);			
			inputParams.put("IN_PRINCIPAL_AMT", amtsToRefund[PRINCIPAL_AMT_POS]);
			inputParams.put("IN_ADDON_AMT",amtsToRefund[ADDON_AMT_POS]);
			inputParams.put("IN_SHIPPING_AMT", amtsToRefund[SHIPPING_AMT_POS]);				
			inputParams.put("IN_TAX_AMT", amtsToRefund[TAX_AMT_POS]);	
			inputParams.put("IN_REFUND_PRINCIPAL_AMT", amtsToRefund[REF_PRNCPL_AMT_POS]);
			inputParams.put("IN_REFUND_ADDON_AMT", amtsToRefund[REF_ADDON_AMT_POS]);
			inputParams.put("IN_CREATED_BY", csrID);
			inputParams.put("IN_UPDATED_BY", csrID);
			
			dataRequest.setInputParams(inputParams);
			dataRequest.setStatementID("INSERT_PARTNER_ADJUSTMENT");
			Map<String, Object> outputParameters = (Map<String, Object>) dau.execute(dataRequest);

			String status = (String) outputParameters.get("OUT_STATUS");
			
			if (!"Y".equalsIgnoreCase(status)) {
				String message = (String) outputParameters.get("OUT_MESSAGE");
				logger.error("Unable to insert partner order adjustment for the order " + item.getExternalOrderNumber() + " - " + message);
				throw new Exception("Error caught creating partner order adjustment records : " + message);
			}
			
		} catch (SQLException se) {
			logger.error("Exception thrown while executing query." 	+ se.getMessage());
			throw new Exception("Unable to access data.");
		} catch (Exception e) {
			logger.error("exception caught in createPartnerAdjustmentFeedRecord: " + e.getMessage());
			sendSystemMessage(e.getMessage());	
		} 
	}
	
	/**
	 * @param confirmationNumber
	 * @param partnerOriginId
	 * @param connection
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public CachedResultSet getPtnOrderDetailByConfNumber(String confirmationNumber, String partnerOriginId, Connection connection) {
		DataRequest dataRequest = new DataRequest();
		try {
			
			dataRequest.setConnection(connection);
			dataRequest.setStatementID("GET_PTN_ORD_ITEM_DETAIL");
			Map inputParams = new HashMap();
			inputParams.put("IN_CONFIRMATION_NUMBER", confirmationNumber);
			dataRequest.setInputParams(inputParams);
			
			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();	
			return (CachedResultSet) dataAccessUtil.execute(dataRequest);
			
		} catch (Exception e) {
			logger.error("Error caught obtaining partner order info : " + e.getMessage());
		}
		return null;
	}
	
	private boolean isEmpty(String str) {
		if(str == null || str.length() == 0) {
			return true;
		}
		return false;
	}
}
