package com.ftd.osp.utilities;


public class RosesDotComUtil {
    private static final String ROSES_INTERNET_ORIGIN = "ROSES";
    
    /**
     * Returns true if the passed origin represents a Roses.com Internet origin
     * @param origin
     * @return
     */
    public static boolean isRosesInternetOrder(String origin) {
        boolean isRoses = false;
        
        if (ROSES_INTERNET_ORIGIN.equalsIgnoreCase(origin)) {
        	isRoses = true;
        }
        return isRoses;
    }
    
}
