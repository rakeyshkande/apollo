package com.ftd.osp.utilities.dataaccess.util;

import com.ftd.osp.utilities.dataaccess.valueobjects.XMLFormat;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.sql.Clob;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * Converts a resultset object to a standard XML format
 *
 * @author Anshu Gaind
 **/
public class ResultSet2XML  
{
    private static Logger logger = new Logger("com.ftd.osp.utilities.dataaccess.util.ResultSet2XML");

    /**
    * Converts a Result Set to standard XML format string
    *
    * @param rset the result set object
    * @param xmlFormat specifies the format of the xml
    * @exception SQLException
    * @return the result set formatted as standard XML string. Returns a null if the
    *          result set is null
    */
    public static String convertToXMLString(ResultSet rset, XMLFormat xmlFormat) 
      throws SQLException
    {
        String xmlType = "attribute";
        String xmlTop = "rowset";
        String xmlBottom = "row";
        String newTextNodeData = null;
        
        if(xmlFormat != null) {
            xmlType = xmlFormat.getAttribute("type");
            xmlTop = xmlFormat.getAttribute("top");
            xmlBottom = xmlFormat.getAttribute("bottom");
        }

        if (rset == null)  {
            return null;
        }

        //begin tags
        StringBuffer rv = new StringBuffer();
    
        //rv.append("<?xml version=\"1.0\"?>");
        rv.append("<" + xmlTop + ">");

        // loop thru the result set and generate xml
        int i = 0;
        ResultSetMetaData meta = rset.getMetaData();
        int columnCount = meta.getColumnCount();
        while (rset.next())  
        {
            i++;
      
            if(xmlType.equals("attribute")) 
            {
                rv.append("<" + xmlBottom + " num=\"" + i + "\"");

                for (int j = 0; j < columnCount; )  
                {
                    rv.append(( " " + meta.getColumnName(++j).toLowerCase() ) + "=\"" + DOMUtil.encodeChars(rset.getString(j)!=null?rset.getString(j).trim():null) + "\"");
                }

                rv.append("/>");
            }
            else 
            {
                rv.append("<" + xmlBottom + " NUM=\"" + i + "\">");

                for (int j = 0; j < columnCount; )  
                {
                  newTextNodeData = rset.getString(j)!=null?rset.getString(j).trim():null;
                  if (newTextNodeData != null)  {
                    rv.append("<" + ( meta.getColumnName(++j).toLowerCase() ) + ">");
                    rv.append(newTextNodeData);
                    rv.append("</" + ( meta.getColumnName(j).toLowerCase() )  + ">");                      
                  } else {
                    rv.append("<" + ( meta.getColumnName(++j).toLowerCase() ) + " />");
                  }
                  
                }
        
                rv.append("<" + xmlBottom + "/>");
            }
      
        }

        rv.append("</" + xmlTop + ">");
    
        return rv.toString();
    }



    /**
    * Converts a Result Set to standard XML format document
    *
    * @param rset the result set object
    * @param xmlFormat specifies the format of the xml
    * @exception SQLException
    * @exception ParserConfigurationException
    * @return the result set formatted as standard XML Document. Returns a null if the
    *          result set is null
    */
    public static Document convertToXMLDOM(ResultSet rset, XMLFormat xmlFormat) 
      throws SQLException, ParserConfigurationException
    {
        String xmlType = "attribute";
        String xmlTop = "rowset";
        String xmlBottom = "row";

        if(xmlFormat != null) {
            xmlType = xmlFormat.getAttribute("type");
            xmlTop = xmlFormat.getAttribute("top");
            xmlBottom = xmlFormat.getAttribute("bottom");
        }

        if (rset == null)  {
            return null;
        }

        Document doc = null;

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setIgnoringElementContentWhitespace(true);
        factory.setNamespaceAware(false);
        DocumentBuilder builder = factory.newDocumentBuilder();
        doc = builder.newDocument();
        Element rowset = null, row = null, column = null;

        rowset = doc.createElement(xmlTop);

        // loop thru the result set and generate xml
        int i = 0;
        ResultSetMetaData meta = rset.getMetaData();
        int columnCount = meta.getColumnCount();
        String newTextNodeData = null;
        
        while (rset.next())  
        {
            i++;
      
            // create the row tag
            row = doc.createElement(xmlBottom);
            // set up the num attrib on the row tag
            row.setAttribute("num", String.valueOf(i));

            if(xmlType.equals("attribute")) 
            {
                for (int j = 0; j < columnCount; )  
                {
                    // JP Puzon 07-05-2005
                    // Removed encoding of attribute values since all XSLT
                    // processors will do this automatically as per the spec. 
                    // row.setAttribute(meta.getColumnName(++j).toLowerCase(), DOMUtil.encodeChars(rset.getString(j)));
                    row.setAttribute(meta.getColumnName(++j).toLowerCase(), rset.getString(j)!=null?rset.getString(j).trim():null);
                }
            }
            else 
            {
                // create a tag for each column name
                Clob bigVal = null;
                for (int j = 0; j < columnCount; )  
                {
                    column = doc.createElement(meta.getColumnName(++j).toLowerCase());
                    // new code to test clob type column
                    if(meta.getColumnTypeName(j).equalsIgnoreCase("clob"))
                    {
                      bigVal = rset.getClob(j);
                      if(bigVal!=null)
                      {
                        newTextNodeData = bigVal.getSubString((long)1, (int)bigVal.length());
                      }
                      else
                      {
                        newTextNodeData = null;
                      }
                    }
                    else
                      newTextNodeData = rset.getString(j)!=null?rset.getString(j).trim():null;
                    if (newTextNodeData != null)  {
                      // append the column data to the column node
                      if(meta.getColumnTypeName(j).equalsIgnoreCase("clob"))
                      {
                        column.appendChild(doc.createCDATASection(newTextNodeData));
                      }
                      else
                      {
                        column.appendChild(doc.createTextNode(newTextNodeData));
                      }                      
                    }
                    // append the column to the row tag
                    row.appendChild(column);
                }
            }
      
            rowset.appendChild(row);
            
        } // end while
        
        doc.appendChild(rowset);
        
        return doc;
    }


    /**
    * Converts a Result Set to standard XML format document for an AJAX front-end
    *
    * @param rset the result set object
    * @param xmlName specifies the name of the xml "rs" tag
    * @exception SQLException
    * @exception ParserConfigurationException
    * @return the result set formatted as standard XML Document. Returns a null if the
    *          result set is null
    */
    public static Element convertToXMLAjax(ResultSet rset, String xmlName, Document doc) 
      throws SQLException, ParserConfigurationException
    {
        String xmlTop = "record";

        if (rset == null)  {
            return null;
        }

        Element rowset = null, row = null, column = null;

        rowset = doc.createElement("rs");
        rowset.setAttribute("name", xmlName);

        // loop thru the result set and generate xml
        int i = 0;
        ResultSetMetaData meta = rset.getMetaData();
        int columnCount = meta.getColumnCount();
        String newTextNodeData = null;
        
        while (rset.next())  
        {
            i++;
      
            // create the row tag
            row = doc.createElement(xmlTop);
            // set up the num attrib on the row tag
            row.setAttribute("row", String.valueOf(i));

            // create a tag for each column name
            Clob bigVal = null;
            for (int j = 0; j < columnCount; )  
            {
                column = doc.createElement(meta.getColumnName(++j).toLowerCase());
                // new code to test clob type column
                if(meta.getColumnTypeName(j).equalsIgnoreCase("clob"))
                {
                    bigVal = rset.getClob(j);
                    if(bigVal!=null)
                    {
                        newTextNodeData = bigVal.getSubString((long)1, (int)bigVal.length());
                        column.appendChild(doc.createCDATASection(newTextNodeData));
                    }
                    else
                    {
                        newTextNodeData = null;
                    }
                }
                else
                {
                    newTextNodeData = rset.getString(j)!=null?rset.getString(j).trim():null;
                    column.appendChild(doc.createTextNode(newTextNodeData));
                }

                // append the column to the row tag
                row.appendChild(column);
            }
      
            rowset.appendChild(row);
            
        } // end while
        
        doc.appendChild(rowset);
        
        return rowset;
    }
}