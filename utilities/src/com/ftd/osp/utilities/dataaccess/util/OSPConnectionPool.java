package com.ftd.osp.utilities.dataaccess.util;

import java.sql.*;

public class OSPConnectionPool 
{
    private static OSPConnectionPool OSP_CONNECTION_POOL;

    public OSPConnectionPool()
    {
    }

    public synchronized static OSPConnectionPool getInstance() throws Exception
    {
        if (OSP_CONNECTION_POOL == null)
        {
            OSP_CONNECTION_POOL = new OSPConnectionPool();
        }

        return OSP_CONNECTION_POOL;
    }

    public Connection getConnection(String connectionString, String userName, String password) {
        Connection connection = null;
    
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            connection = DriverManager.getConnection(connectionString, userName, password);
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return connection;
    }
}