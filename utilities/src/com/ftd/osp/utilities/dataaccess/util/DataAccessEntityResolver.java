package com.ftd.osp.utilities.dataaccess.util;
import java.io.IOException;

import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * Allows the data access util to resolve external entities.
 * @author Anshu Gaind
 */
public class DataAccessEntityResolver implements EntityResolver {

  /**
   * Allows the data access util to resolve external entities
   * 
   * @param publicId  The public identifier of the external entity being referenced, or null if none was supplied.
   * @param systemId  The system identifier of the external entity being referenced.
   * @exception SAXException Any SAX exception, possibly wrapping another exception
   * @exception java.io.IOException  A Java-specific IO exception, possibly the result of creating a new InputStream or Reader for the InputSource.
   * @return An InputSource object describing the new input source, or null to request that the parser open a regular URI connection to the system identifier.
   */
   public InputSource resolveEntity (String publicId, String systemId) throws SAXException, IOException{
     if ( (systemId != null) && (systemId.endsWith(".xml")) ) {

         if(systemId.contains("file:")){
             systemId = systemId.substring(systemId.lastIndexOf("/")+1);
         }

        Class class_v = this.getClass();
        ClassLoader classLoader = class_v.getClassLoader();
        InputSource inputSource = new InputSource(classLoader.getResourceAsStream(systemId)); 
        return inputSource;
     } else {
       return null;
     }
   }
}