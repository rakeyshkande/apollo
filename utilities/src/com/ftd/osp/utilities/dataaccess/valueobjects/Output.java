package com.ftd.osp.utilities.dataaccess.valueobjects;

import com.ftd.osp.utilities.plugins.Logger;

import java.util.HashMap;

/**
 *  Represents an Output tag as defined in the configuration files
 *
 * @author Anshu Gaind
 */
public class Output {
    private HashMap attributes;
    private Logger logger;

    public Output() {
      logger = new Logger("com.ftd.osp.utilities.dataaccess.valueobjects.Output");
      attributes = new HashMap();
    }

    public String getAttribute(String name) {
        return (String)attributes.get(name);
    }

    public void setAttribute(String name, String value) {
      attributes.put(name.trim(), value.trim());
      logger.debug("Setting Attribute " + name);
    }

}