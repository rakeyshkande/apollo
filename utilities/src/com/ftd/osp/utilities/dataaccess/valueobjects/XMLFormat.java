package com.ftd.osp.utilities.dataaccess.valueobjects;

import com.ftd.osp.utilities.plugins.Logger;

import java.util.HashMap;

public class XMLFormat 
{
    private HashMap attributes;
    private Logger logger;

    /**
     * The default constructor
     */
    public XMLFormat() {
        logger = new Logger("com.ftd.osp.utilities.dataaccess.valueobjects.XMLFormat");
        attributes = new HashMap();
    }

    public String getAttribute(String name) {
        return (String)attributes.get(name);
    }

    public void setAttribute(String name, String value) {
        attributes.put(name.trim(), value.trim());
        logger.debug("Setting Attribute " + name + " to " + value);
    }
}