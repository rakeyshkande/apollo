package com.ftd.osp.utilities.dataaccess.valueobjects;

import com.ftd.osp.utilities.dataaccess.valueobjects.InParameter;
import com.ftd.osp.utilities.dataaccess.valueobjects.XMLFormat;
import com.ftd.osp.utilities.plugins.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *  Represents a statement configuration as defined in statements.xml
 *
 * @author Anshu Gaind
 */

public class StatementConfig {
    private ArrayList inParameters;
    private ArrayList outParameters;
    private Output output;
    private XMLFormat xmlFormat;
    private String SQL;
    private Logger logger;
    private HashMap attributes;

    
    public StatementConfig() {
        inParameters = new ArrayList();
        outParameters = new ArrayList();
        attributes = new HashMap();
        logger = new Logger("com.ftd.osp.utilities.dataaccess.valueobjects.StatementConfig");
    }

    public String getAttribute(String name) {
        return (String)attributes.get(name);
    }

    public void setAttribute(String name, String value) {
      attributes.put(name.trim(), value.trim());
      logger.debug("Setting Attribute " + name);
    }


    public void setInParameter(InParameter inParameter){
        this.inParameters.add(inParameter);
        logger.debug("Setting In Parameter " + inParameter.getAttribute("name") + " for Statement " + this.getAttribute("id"));
    }

    public InParameter getInParameter(int index){
        return (InParameter)this.inParameters.get(index);
    }

    public List getInParameters(){
      return this.inParameters;
    }

    public int getInputSize(){
      return this.inParameters.size();
    }

    public void setOutParameter(OutParameter outParameter){
        this.outParameters.add(outParameter);
        logger.debug("Setting Out Parameter " + outParameter.getAttribute("name") + " for Statement " + this.getAttribute("id"));
    }

    public OutParameter getOutParameter(int index){
        return (OutParameter)this.outParameters.get(index);
    }

    public List getOutParameters(){
      return this.outParameters;
    }

    public int getOutputSize(){
      return this.outParameters.size();
    }

    
    public void setOutput(Output output){
        this.output = output;
        logger.debug("Setting Output " + output.getAttribute("name") + " for Statement " + this.getAttribute("id"));
    }

    public Output getOutput(){
        return output;
    }

    public void setXMLFormat(XMLFormat xmlFormat){
        this.xmlFormat = xmlFormat;
        logger.debug("Setting XMLFormat Top " + xmlFormat.getAttribute("top") + " for Statement " + this.getAttribute("id"));
        logger.debug("Setting XMLFormat Bottom " + xmlFormat.getAttribute("bottom") + " for Statement " + this.getAttribute("id"));
        logger.debug("Setting XMLFormat Tag Case " + xmlFormat.getAttribute("tagCase") + " for Statement " + this.getAttribute("id"));
    }

    public XMLFormat getXMLFormat(){
        return xmlFormat;
    }

    public String getSQL() {
        return SQL;
    }

    public void setSQL(String newSQL) {
        SQL = newSQL.trim();
    }
}