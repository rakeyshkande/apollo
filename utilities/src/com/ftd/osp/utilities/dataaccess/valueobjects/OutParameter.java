package com.ftd.osp.utilities.dataaccess.valueobjects;

import com.ftd.osp.utilities.plugins.Logger;

import java.util.HashMap;

/**
 *  Represents an Out parameter tag as defined in the configuration files
 *
 * @author Anshu Gaind
 */
public class OutParameter {
    private HashMap attributes;
    private Logger logger;

    public OutParameter() {
        attributes = new HashMap();
        logger = new Logger("com.ftd.osp.utilities.dataaccess.valueobjects.OutParameter");
    }

    public String getAttribute(String name) {
        return (String)attributes.get(name);
    }

    public void setAttribute(String name, String value) {
      attributes.put(name.trim(), value.trim());
      logger.debug("Setting Attribute " + name);
    }

}