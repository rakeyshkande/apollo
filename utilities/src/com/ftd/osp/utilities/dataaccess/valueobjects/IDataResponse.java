package com.ftd.osp.utilities.dataaccess.valueobjects;

public interface IDataResponse
{
    public Object executeQuery(StatementConfig statementConfig, DataRequest dataRequest)
        throws Exception;
}