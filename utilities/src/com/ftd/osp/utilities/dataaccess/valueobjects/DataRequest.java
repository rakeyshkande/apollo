package com.ftd.osp.utilities.dataaccess.valueobjects;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.util.OSPConnectionPool;
import com.ftd.osp.utilities.plugins.Logger;

import oracle.sql.CLOB;

import java.sql.Clob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


/**
 * Represents a request made to the Data Access Service
 *
 * @author Anshu Gaind
 */
public class DataRequest {
    private Logger logger = new Logger(
            "com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest");
    Map inputParams;
    private String statementID;
    private Connection connection;
    private List tempClobList;

    /**
     * The default constructor
     */
    public DataRequest() {
        super();
    }

    public String getStatementID() {
        return statementID;
    }

    public void setStatementID(String newStatementID) {
        statementID = newStatementID;
    }

    public void setInputParams(Map newInputParams) {
        inputParams = newInputParams;
    }

    public Map getInputParams() {
        return inputParams;
    }

    public void addInputParam(String name, Object param) {
        if (inputParams == null) {
            inputParams = new HashMap();
        }

        inputParams.put(name, param);
    }

    public void addTempClob(Clob tempClob) {
        if (!(tempClob instanceof oracle.sql.CLOB)) {
            throw new IllegalArgumentException(
                "addTempClob: addTempClob() currently only supports oracle.sql.CLOB inputs, not " +
                tempClob.getClass().getName());
        }

        if (tempClobList == null) {
            tempClobList = new ArrayList();
        }

        tempClobList.add(tempClob);
    }

    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection newConnection) {
        connection = newConnection;
    }

    public void useGenericConnection(String connectionString, String userName,
        String password) {
        Connection genericConnection = null;

        try {
            genericConnection = OSPConnectionPool.getInstance().getConnection(connectionString,
                    userName, password);
        } catch (Exception e) {
            logger.error("useGenericConnection: Failed.", e);
        }

        this.setConnection(genericConnection);
    }

    public void closeGenericConnection() {
        try {
            if (this.getConnection() != null) {
                this.getConnection().close();
            }
        } catch (SQLException e) {
            logger.error("closeGenericConnection: Failed.", e);
        }
    }

    public void reset() {
        statementID = null;
        inputParams = null;

        if ((tempClobList != null) && (tempClobList.size() > 0)) {
            for (int index = 0; index < tempClobList.size(); index++) {
                CLOB tempClob = (CLOB) tempClobList.get(index);

                try {
                    if (tempClob.isOpen()) {
                        tempClob.close();
                    }
                } catch (SQLException e) {
                    logger.error("reset: tempClob.close() failed.", e);
                }

                try {
                    // Free the memory used by this CLOB
                    tempClob.freeTemporary();
                    tempClob = null;
                } catch (SQLException e) {
                    logger.error("reset: tempClob.freeTemporary() failed.", e);
                }
            }
            
            tempClobList.clear();
            tempClobList = null;
        }
    }

    protected void finalize() throws Throwable {
        reset();
    }
}
