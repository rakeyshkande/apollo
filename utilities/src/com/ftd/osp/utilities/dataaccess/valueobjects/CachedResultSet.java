package com.ftd.osp.utilities.dataaccess.valueobjects;

import java.io.StringWriter;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.Array;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import java.sql.Time;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Acts as a disconnected result set, using a similar API
 *
 * @author Anshu Gaind
 */

public class CachedResultSet {
    private ArrayList records;
    private ArrayList currentRecord;
    private ArrayList nameIndexMap; // name == key
    private Iterator iterator;
    
    /**
     * The default constructor
     */
     public CachedResultSet(){
        super();
        records = new ArrayList();
        nameIndexMap = new ArrayList();
     }

    /**
     * Populates itself from the contents of a result set
     *
     * @param resultSet the result set object
     */
    public void populate(ResultSet resultSet) throws SQLException{
        ArrayList record = null;
        ResultSetMetaData rsetMetaData = resultSet.getMetaData();
    
        // build the key for the local column map cache
        StringWriter sw = new StringWriter();
        for(int i=1; i <= rsetMetaData.getColumnCount(); i++) 
        {
          sw.write(rsetMetaData.getColumnName(i));
        }
        
        // couldn't find the key in the cache... build it and cache it
        nameIndexMap = new ArrayList(rsetMetaData.getColumnCount() + 1);
          
        // using a 1 based array to map directly to SQL column numbers
        nameIndexMap.add(0, null);
        for(int j=1; j <= rsetMetaData.getColumnCount(); j++) 
        {
          nameIndexMap.add(j, rsetMetaData.getColumnName(j).toUpperCase());
        }
        while (resultSet.next())  {
            // create a new record
            record = new ArrayList();

            //populate the record; result set starts at 1
            for (int i = 1; i <= rsetMetaData.getColumnCount(); i++)  {
              int type=rsetMetaData.getColumnType(i);
              String className =rsetMetaData.getColumnClassName(i);
              
              // We're using a defect in the java.sql.Date code
              // so that we can use it as the internal Date representation
              // This is being done so we don't break older apps, but 
              // it's somewhat unsafe to rely on java.sql.Date maintaining 
              // time values forever.
              
              // In an optimal world, we would use java.util.Date as
              // the internal representation for the date, but until
              // we go through and fix all of the other apps to use
              // the new getDate(), getTime(), and getTimestamp() methods
              // rather than the getObject() method (and then naively casting 
              // the output to java.sql.Date), we're stuck with this.
              // -- Jason Weiss 10/20/2005
							// Code was not checking for nulls.  Now a date is not created
							// if data does not exist.
							// -- Mike Kruger and Mark Moon 10/21/2005
              if(type==Types.TIME){  
                  java.util.Date temp = resultSet.getTime(i);
									if(temp != null)
										record.add(new java.sql.Date(temp.getTime()));
									else
										record.add(resultSet.getObject(i));   										
              }  else if(type==Types.TIMESTAMP || className.equalsIgnoreCase("datetime") || className.equalsIgnoreCase("timestamp")){
                  java.util.Date temp = resultSet.getTimestamp(i);
									if(temp != null)
										record.add(new java.sql.Date(temp.getTime()));
									else
										record.add(resultSet.getObject(i));   										
              }  else if(type==Types.DATE) {
                  java.util.Date temp = resultSet.getDate(i);
									if(temp != null)
										record.add(new java.sql.Date(temp.getTime()));
									else
										record.add(resultSet.getObject(i));   										
              } else {
                  record.add(resultSet.getObject(i));   
              }
            }
            //add it to the collection of records
            records.add(record);
        }
    }

    /**
     * Moves the cursor position to the next record
     */
    public boolean next() {
      if (iterator == null)  {
          iterator = records.iterator();
      }
      if (iterator.hasNext())  {
        currentRecord = (ArrayList) iterator.next();
        return true;
      } else {
        // reset the currentRecord
        currentRecord = null;
        return false;
      }
    }

    /**
     * Return the field from the indicated column position. The first column
     * begins at index 1, like in the java.sql.ResultSet object
     *
     * @param index the indicated column position
     * @return the field from the current record
     **/
    public Object getObject(int index){
      if (currentRecord != null)  {
         //
          return currentRecord.get(--index);
      } else {
        return null;
      }
    }

    public Object getObject(String fieldName)
    {
      if(fieldName == null) return null;
      try {
        int index = nameIndexMap.indexOf(fieldName.toUpperCase());
        return this.getObject(index);
      } catch (Exception e) 
      {
        return null;
      }
    }

    /**
     * Returns a count of the number of columns in the result set
     *
     **/
     public int getColumnCount(){
        if (records != null&&!records.isEmpty())  {
            ArrayList topRecord = (ArrayList)records.get(0);
            if (topRecord != null)  {
              return topRecord.size();
            } else {
              return 0;
            }
        } else {
            return 0;
        }
     }


    /**
     * Returns a count of the number of columns in the result set
     *
     **/
     public int getRowCount(){
        if (records != null)  {
            return records.size();
        } else {
            return 0;
        }
     }

    /**
     * Reset the cached result set to the beginning of the record set.
     */
    public void reset(){
      iterator = records.iterator();
    }

    public Array getArray(int i){
      return (Array)this.getObject(i);  
    }
    public Array getArray(String colName){
      return (Array)this.getObject(colName);
    }
    public BigDecimal getBigDecimal(int i){
      return (BigDecimal) this.getObject(i);
    }
    public BigDecimal getBigDecimal(String colName){
      return (BigDecimal) this.getObject(colName);
    }
    public Blob getBlob(int i) {
      return (Blob) this.getObject(i);
    }
    public Blob getBlob(String colName){
      return (Blob) this.getObject(colName);
    }
    public boolean getBoolean(int i){
      return ((Boolean)this.getObject(i)).booleanValue();
    }
    public boolean getBoolean(String colName){
      return((Boolean)this.getObject(colName)).booleanValue();
    }
    public Clob getClob(int i){
      return (Clob) this.getObject(i);
    }
    public Clob getClob(String colName){
      return (Clob) this.getObject(colName);
    }
    public Date getDate(int i){
      if (this.getObject(i) == null) return null;
      return new java.sql.Date(((java.util.Date)this.getObject(i)).getTime());
    }
    public Date getDate(String colName){
      if (this.getObject(colName) == null) return null;
      return new java.sql.Date(((java.util.Date)this.getObject(colName)).getTime());
    }
    public double getDouble(int i){
      Object o = this.getObject(i);
      if (o == null) return 0;
      return ((Number)o).doubleValue();
    }
    public double getDouble(String colName){
      Object o = this.getObject(colName);
      if (o == null) return 0;
      return ((Number)o).doubleValue();
    }
    public float getFloat(int i){
      Object o = this.getObject(i);
      if (o == null) return 0;
      return ((Number)o).floatValue();
    }
    public float getFloat(String colName){
      Object o = this.getObject(colName);
      if (o == null) return 0;
      return ((Number)o).floatValue();
    }
    public int getInt(int i){
      Object o = this.getObject(i);
      if (o == null) return 0;
      return ((Number)o).intValue();
    }
    public int getInt(String colName){
      Object o = this.getObject(colName);
      if (o == null) return 0;
      return ((Number)o).intValue();
    }
    public long getLong(int i){
      Object o = this.getObject(i);
      if (o == null) return 0;
      return ((Number)o).longValue();
    }
    public long getLong(String colName){
      Object o = this.getObject(colName);
      if (o == null) return 0;
      return ((Number)o).longValue();
    }
    public short getShort(int i){
      Object o = this.getObject(i);
      if (o == null) return 0;
      return ((Number)o).shortValue();
    }
    public short getShort(String colName){
      Object o = this.getObject(colName);
      if (o == null) return 0;
      return ((Number)o).shortValue();
    }
    public String getString(int i){
      Object o = this.getObject(i);
      if (o == null) return null;
      return o.toString();
    }
    public String getString(String colName){
      Object o = this.getObject(colName);
      if (o == null) return null;
      return o.toString();
    }
    public Time getTime(int i){
      if (this.getObject(i) == null) return null;
      return new java.sql.Time(((java.util.Date) this.getObject(i)).getTime());
    }
    public Time getTime(String colName){
      if (this.getObject(colName) == null) return null;
      return new java.sql.Time(((java.util.Date) this.getObject(colName)).getTime());
    }
    public Timestamp getTimestamp(int i){
      if (this.getObject(i) == null) return null;
      return new java.sql.Timestamp(((java.util.Date) this.getObject(i)).getTime());
    }
    public Timestamp getTimestamp(String colName){
      if (this.getObject(colName) == null) return null;
      return new java.sql.Timestamp(((java.util.Date) this.getObject(colName)).getTime());
    }
    public URL getURL(int i){
      return (URL) this.getObject(i);
    }
    public URL getURL(String colName){
      return (URL) this.getObject(colName);
    }

    public String getFieldName(int col) 
    {
      return (String)nameIndexMap.get(col);
    }
}