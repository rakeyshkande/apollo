package com.ftd.osp.utilities.dataaccess.valueobjects;

import com.ftd.osp.utilities.plugins.Logger;

import java.util.HashMap;

/**
 *  Represents an Input tag as defined in the configuration files
 *
 * @author Anshu Gaind
 */
public class InParameter {
    private HashMap attributes;
    private Logger logger;

    public InParameter() {
        attributes = new HashMap();
        logger = new Logger("com.ftd.osp.utilities.dataaccess.valueobjects.InParameter");
    }

    public String getAttribute(String name) {
        return (String)attributes.get(name);
    }

    public void setAttribute(String name, String value) {
      attributes.put(name.trim(), value.trim());
      logger.debug("Setting Attribute " + name);
    }

}