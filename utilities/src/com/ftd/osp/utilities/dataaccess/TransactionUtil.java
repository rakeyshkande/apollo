package com.ftd.osp.utilities.dataaccess;

import com.ftd.osp.utilities.ConfigurationUtil;

import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;

import javax.naming.InitialContext;

import javax.transaction.UserTransaction;

public class TransactionUtil
{
    private static Logger logger = new Logger("com.ftd.osp.utilities.dataaccess.TransactionUtil");
    
    public TransactionUtil()
    {
    }

    /**
     * Obtain database connection
     * @return Database connection
     * @throws Exception
     */
    public Connection getConnection(IDatabaseResourceProvider resourceProvider, String dataSource) throws Exception{
        Connection conn = resourceProvider.getDatabaseConnection(dataSource);
        if (conn == null) {
            throw new Exception("Unable to connect to database");
        }
        return conn;
    }

    /**
     * Close database connection
     * @param conn Database connection
     */
    public void closeConnection(Connection conn)
    {
        try
        {
            if(conn != null)
            {
                conn.close();
            }
        }
        catch(Throwable t)
        {
            logger.warn("Error occurred while closing connection:" + t);
        }
    }

    /**
     * Create a transaction
     * @return UserTransaction
     * @throws Exception
     */
    public UserTransaction createUserTransaction(String configFileName, String timeoutProperty, String jndiUserTransactionProperty) throws Exception
    {
        int transactionTimeout = Integer.parseInt(ConfigurationUtil.getInstance().getProperty(configFileName, timeoutProperty));
        String jndi_usertransaction_entry = ConfigurationUtil.getInstance().getProperty(configFileName, jndiUserTransactionProperty);
        return createUserTransaction(transactionTimeout,jndi_usertransaction_entry);
    }

    /**
     * Create a transaction
     * @return UserTransaction
     * @throws Exception
     */
    public UserTransaction createUserTransaction(int transactionTimeout, String jndiUserTransactionEntry) throws Exception
    {
        InitialContext context = new InitialContext();

        UserTransaction ut = (UserTransaction) context.lookup(jndiUserTransactionEntry);
        ut.setTransactionTimeout(transactionTimeout);

        return ut;
    }

    /**
     * Rollback a transaction
     * @param userTransaction Database transaction
     */
    public void rollback(UserTransaction userTransaction)
    {
        try  
        {
            if (userTransaction != null)  
            {
              // rollback the user transaction
              userTransaction.rollback();

              logger.debug("User transaction rolled back");
            }
        } 
        catch (Exception ex)  
        {
            logger.error(ex);
        } 
    }

    
}
