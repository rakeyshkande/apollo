package com.ftd.osp.utilities.dataaccess;


import com.ftd.osp.utilities.dataaccess.statements.CStmtReturnCRSet;
import com.ftd.osp.utilities.dataaccess.statements.CStmtReturnResultSet;
import com.ftd.osp.utilities.dataaccess.statements.CStmtReturnXMLDOM;
import com.ftd.osp.utilities.dataaccess.statements.CStmtReturnXMLAjax;
import com.ftd.osp.utilities.dataaccess.statements.CStmtReturnXMLString;
import com.ftd.osp.utilities.dataaccess.statements.ExecuteSQLStatement;
import com.ftd.osp.utilities.dataaccess.statements.PStmtReturnCRSet;
import com.ftd.osp.utilities.dataaccess.statements.PStmtReturnInteger;
import com.ftd.osp.utilities.dataaccess.statements.PStmtReturnResultSet;
import com.ftd.osp.utilities.dataaccess.statements.PStmtReturnXMLDOM;
import com.ftd.osp.utilities.dataaccess.statements.PStmtReturnXMLString;
import com.ftd.osp.utilities.dataaccess.statements.StmtReturnCRSet;
import com.ftd.osp.utilities.dataaccess.statements.StmtReturnInteger;
import com.ftd.osp.utilities.dataaccess.statements.StmtReturnResultSet;
import com.ftd.osp.utilities.dataaccess.statements.StmtReturnXMLDOM;
import com.ftd.osp.utilities.dataaccess.statements.StmtReturnXMLString;
import com.ftd.osp.utilities.dataaccess.util.DataAccessEntityResolver;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.dataaccess.valueobjects.InParameter;
import com.ftd.osp.utilities.dataaccess.valueobjects.OutParameter;
import com.ftd.osp.utilities.dataaccess.valueobjects.Output;
import com.ftd.osp.utilities.dataaccess.valueobjects.StatementConfig;
import com.ftd.osp.utilities.dataaccess.valueobjects.XMLFormat;
import com.ftd.osp.utilities.j2ee.ResourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import java.io.InputStream;
import java.net.URL;
import java.net.URLDecoder;

import java.sql.Clob;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.sql.Statement;
import java.sql.Types;

import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

import org.xml.sax.SAXException;
//import oracle.sql.CLOB;

/**
 * All statements are represented by one statement config object. Each
 * statement in turn is represented by a statement object. There can only be
 * one output for each statement. The valid types of output are 'java.lang.String',
 *  'org.w3c.dom.Document', 'com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet';,
 *  'java.lang.Integer'.<br/><p/>
 *
 *  Supports the following types of input parameters for PreparedStatement and
 *  CallableStatment.<br/>
 *         1. java.sql.Array <br/>
 *         2. java.math.BigDecimal <br/>
 *         3. java.sql.Blob <br/>
 *         4. boolean <br/>
 *         5. byte <br/>
 *         6. byte[] <br/>
 *         7. java.sql.Clob <br/>
 *         8. java.sql.Date <br/>
 *         9. double <br/>
 *         10. float <br/>
 *         11. int <br/>
 *         12. long <br/>
 *         13. null <br/>
 *         14. java.lang.Object <br/>
 *         15. short <br/>
 *         16. java.lang.String <br/>
 *         17. java.sql.Time <br/>
 *         18. java.sql.Timestamp <br/><p/>
 *         19. XML <br/><p/>
 *
 *  CallableStatements can register multiple out parameters. The framework supports
 *  the following out parameters.<p/>
 *           1. oracle.jdbc.OracleTypes.CURSOR<br/>
 *           2. java.sql.Types.CHAR<br/>
 *           3. java.sql.Types.VARCHAR<br/>
 *           4. java.sql.Types.NUMERIC<br/><p/>
 *           5. java.sql.Types.CLOB<br/><p/>
 *           
 *  If you wish to register an in-out parameter, include the parameter in both the 'in-parameter'
 *  and the 'out-parameter' lists and at the same position.
 *
 * @author Anshu Gaind
 */

public class DataAccessUtil
{
    private static Logger logger = new Logger("com.ftd.osp.utilities.dataaccess.DataAccessUtil");
    private static DataAccessUtil DATA_ACCESS_UTIL = new DataAccessUtil();
    private static HashMap STATEMENT_CONFIG_MAP;
    private final static String STATEMENT_CONFIG_FILE = "statements.xml";
    private static boolean SINGLETON_INITIALIZED;

    private StmtReturnXMLString stmtReturnXMLString = new StmtReturnXMLString();
    private StmtReturnXMLDOM stmtReturnXMLDOM = new StmtReturnXMLDOM();
    private StmtReturnCRSet stmtReturnCRSet = new StmtReturnCRSet();
    private StmtReturnResultSet stmtReturnResultSet = new StmtReturnResultSet();
    private StmtReturnInteger stmtReturnInteger = new StmtReturnInteger();
    
    private CStmtReturnXMLString cStmtReturnXMLString = new CStmtReturnXMLString();
    private CStmtReturnXMLDOM cStmtReturnXMLDOM = new CStmtReturnXMLDOM();
    private CStmtReturnCRSet cStmtReturnCRSet = new CStmtReturnCRSet();
    private CStmtReturnResultSet cStmtReturnResultSet = new CStmtReturnResultSet();
    private CStmtReturnXMLAjax cStmtReturnXMLAjax = new CStmtReturnXMLAjax();
    
    private PStmtReturnXMLString pStmtReturnXMLString = new PStmtReturnXMLString();
    private PStmtReturnXMLDOM pStmtReturnXMLDOM = new PStmtReturnXMLDOM();
    private PStmtReturnCRSet pStmtReturnCRSet = new PStmtReturnCRSet();
    private PStmtReturnResultSet pStmtReturnResultSet = new PStmtReturnResultSet();
    private PStmtReturnInteger pStmtReturnInteger = new PStmtReturnInteger();

  /**
   * The private Constructor
  * @exception SAXException
  * @exception ParserConfigurationException
  * @exception IOException
   **/
    private DataAccessUtil()
    {
      super();        
    }

 /**
  * The static initializer for the DataAccessUtil. It ensures that
  * at a given time there is only a single instance
  *
  * @exception SAXException
  * @exception ParserConfigurationException
  * @exception IOException
  * @return the DataAccessUtil
  **/
    public static DataAccessUtil getInstance()
       throws SAXException, ParserConfigurationException, IOException
    {
      if (! SINGLETON_INITIALIZED) 
      {
            initStatementConfigs();
      }
      
        return DATA_ACCESS_UTIL;
    }

  /**
   * Closes the result set and the associated statement, and the connection
   * 
   * @param rset
   * @param con
   * @throws java.sql.SQLException
   */
    public static void closeResource(ResultSet rset, Connection con) throws SQLException
    {
        if (rset != null ){
            Statement stmt = rset.getStatement();
            rset.close();
            if (stmt != null){
                stmt.close();
            }
        }
        if (con != null && (!con.isClosed()) )
        {
            con.close();
        }
    }
    
  
    /**
     * Execute the sql statement as specified in the DataRequest object.
     * 
     * It returns a null if it cannot find a statement handler or the statement definition
     *
     * @param dataRequest the data request object
     * @throws IOException
     * @throws SQLException
     * @throws ParserConfigurationException
     * @return the results of the sql statement
     */
    public Object execute(DataRequest dataRequest) throws IOException, SQLException, ParserConfigurationException
    {
        StatementConfig statementConfig = null;
        ExecuteSQLStatement dataResponse = null;
      
        try
        {
          if ( STATEMENT_CONFIG_MAP.containsKey(dataRequest.getStatementID()) )
          {
              statementConfig = (StatementConfig)STATEMENT_CONFIG_MAP.get(dataRequest.getStatementID());
              dataResponse = this.retrieveDataResponse(statementConfig);
              if (dataResponse == null) 
              {
                  // could not find the appropriate statement handler
                  return null;
              }
              
          }
          else
          {
              logger.error(dataRequest.getStatementID() + " not found in the statements xml file");
              return null;
          }
  
          return dataResponse.executeQuery(statementConfig, dataRequest);
        } 
        catch (ParserConfigurationException pce)  
        {
          logger.error(pce);
          throw pce;
        } 
        catch (SQLException ex)  
        {
          logger.error(ex);
          throw ex;
        } 
        catch (IOException ioe)  
        {
          logger.error(ioe);
          throw ioe;
        } 
        catch (Exception e)  
        {
          logger.error(e);
          throw new SQLException(e.toString());
        }


    }

    /**
     * Converts a clob to a string.  Checks for null clob value.
     * @param clob
     * @return convert string
     * @throws SQLException
     */
    public String clobToString(Clob clob) throws SQLException {
        String retval = null;
        
        if(clob!=null) {
            retval = clob.getSubString(1, (int)clob.length());
        }
        
        return retval;
    }

  /**
   * Converts a Clob containing XML data to an XML String
   * @param clob
   * @return the XML String
   * @throws java.sql.SQLException
   */
  public String toXMLString(Clob clob)
          throws SQLException
  {
    return clobToString(clob);
  }

  /**
   * Converts a Clob containing XML data to an XML Document
   * @param clob
   * @return the XML Document
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws org.xml.sax.SAXException
   * @throws java.io.IOException
   * @throws java.sql.SQLException
   */
  public Document toXMLDocument(Clob clob)
          throws ParserConfigurationException, SAXException, IOException, SQLException
  {
    return DOMUtil.getDocument(clob.getSubString(1, (int)clob.length()));
//    BufferedReader br = null;
//    Document doc = null;
//    try 
//    {
//      CLOB oracleCLOB = (CLOB)clob;
//      br = new BufferedReader(oracleCLOB.getCharacterStream());
//      String line = null; 
//      StringBuffer sb = new StringBuffer();
//      while ((line = br.readLine()) != null) 
//      {
//        sb.append(line);        
//      }
//      doc = DOMUtil.getDocument(sb.toString());
//    } finally 
//    {
//      if (br != null) 
//      {
//          br.close();
//      }
//    }
//    
//    return doc;
    
  }
  
  

  /**
   * Load the statement configuration
  * @exception SAXException
  * @exception ParserConfigurationException
  * @exception IOException
   */
    private synchronized static void initStatementConfigs() throws SAXException, ParserConfigurationException, 
                                                            IOException
    {
      if (! SINGLETON_INITIALIZED) 
      {
        STATEMENT_CONFIG_MAP = new HashMap();

        try
        {
            Element statementNode = null, sqlNode = null, parametersNode = null, 
              inParametersNode = null, inParameterNode = null,
              outParametersNode = null, outParameterNode = null,
              outputNode = null, xmlFormatNode;
            NodeList statement_nl = null, sql_nl = null, parameters_nl = null,
              inParameters_nl = null, inParameter_nl = null,
              outParameters_nl = null, outParameter_nl = null,
              input_nl = null, output_nl = null, xmlFormat_nl;
            Text sqlText = null;
            InParameter inParameter = null;
            OutParameter outParameter = null;
            Output output = null;
            XMLFormat xmlFormat = null;
            StatementConfig statementConfig = null;

            DocumentBuilder builder = DOMUtil.getDocumentBuilder(false, true, true, true, false, false);
            // set the entity resolver
            builder.setEntityResolver(new DataAccessEntityResolver());

            URL url = ResourceUtil.getInstance().getResource(STATEMENT_CONFIG_FILE);
            if (url == null) 
            {
                throw new IOException("The configuration file was not found."); 
            }

            Document doc = null;
            try {
              File f = new File(new URLDecoder().decode(url.getFile(), "UTF-8"));
              doc = DOMUtil.getDocument(builder, f);
            } catch (FileNotFoundException e) {
              // JP Puzon 07-29-2005
              // Added this functionality to accomodate resources which
              // are not physically located on the local filesystem.
              doc = DOMUtil.getDocument(builder, ResourceUtil.getInstance().getResourceAsStream(STATEMENT_CONFIG_FILE));
            }

            // get a list of all statements
            statement_nl = doc.getElementsByTagName("statement");

            // read all statements
            for (int i = 0; i < statement_nl.getLength(); i++)
            {
                statementNode = (Element)statement_nl.item(i);
                if (statementNode != null) {
                    statementConfig = new StatementConfig();

                    // add all statement attributes
                    statementConfig.setAttribute("id", statementNode.getAttribute("id"));
                    statementConfig.setAttribute("type", statementNode.getAttribute("type"));

                    //add the sql
                    sql_nl = statementNode.getElementsByTagName("sql");

                    // there can only be one sql node
                    sqlNode = (Element)sql_nl.item(0);
                    if (sqlNode != null)  {
                        sqlText = (Text)sqlNode.getFirstChild();
                        statementConfig.setSQL(sqlText.getData());

                        // process parameters
                        parameters_nl = statementNode.getElementsByTagName("parameters");
                        parametersNode = (Element) parameters_nl.item(0);

                        if (parametersNode != null)  {
                          // process in parameters
                          inParameters_nl = statementNode.getElementsByTagName("in-parameters");
                          inParametersNode = (Element) inParameters_nl.item(0);

                          if (inParametersNode != null)  {    
                          
                            inParameter_nl = statementNode.getElementsByTagName("in-parameter");
                            
                              for (int j = 0; j < inParameter_nl.getLength(); j++)
                              {
                                  inParameterNode = (Element)inParameter_nl.item(j);
                                  if (inParameterNode != null)  {
                                      inParameter = new InParameter();
                                      inParameter.setAttribute("name", inParameterNode.getAttribute("name"));
                                      inParameter.setAttribute("position", inParameterNode.getAttribute("position"));
                                      inParameter.setAttribute("type", inParameterNode.getAttribute("type"));
                                      statementConfig.setInParameter(inParameter);
                                  }
                              }
                          }
                          // process out parameters
                          outParameters_nl = statementNode.getElementsByTagName("out-parameters");
                          outParametersNode = (Element) outParameters_nl.item(0);

                          if (outParametersNode != null)  {    
                          
                            outParameter_nl = statementNode.getElementsByTagName("out-parameter");
                            
                              for (int j = 0; j < outParameter_nl.getLength(); j++)
                              {
                                  outParameterNode = (Element)outParameter_nl.item(j);
                                  if (outParameterNode != null)  {
                                      outParameter = new OutParameter();
                                      outParameter.setAttribute("name", outParameterNode.getAttribute("name"));
                                      outParameter.setAttribute("position", outParameterNode.getAttribute("position"));
                                      outParameter.setAttribute("type", outParameterNode.getAttribute("type"));
                                      outParameter.setAttribute("id", outParameterNode.getAttribute("id"));
                                      outParameter.setAttribute("emptyMessage", outParameterNode.getAttribute("emptyMessage"));
                                      statementConfig.setOutParameter(outParameter);
                                  }
                              }
                            }
                         } // end parameters

                        // add output for the sql
                        output_nl = statementNode.getElementsByTagName("output");
                        for (int k = 0; k < 1; k++)
                        {
                            // there can only be one output
                            outputNode = (Element)output_nl.item(0);
                            if (outputNode != null)
                            {
                                output = new Output();
                                output.setAttribute("name", outputNode.getAttribute("name"));
                                output.setAttribute("type", outputNode.getAttribute("type"));
                                output.setAttribute("source", outputNode.getAttribute("source"));
                                statementConfig.setOutput(output);
                            }
                        }

                        // add xml format for the sql
                        xmlFormat_nl = statementNode.getElementsByTagName("xmlformat");
                        if(xmlFormat_nl != null) {
                            for (int k = 0; k < 1; k++)
                            {
                                // there can only be one output
                                xmlFormatNode = (Element)xmlFormat_nl.item(0);
                                if (xmlFormatNode != null)
                                {
                                    xmlFormat = new XMLFormat();
                                    xmlFormat.setAttribute("type", xmlFormatNode.getAttribute("type"));
                                    xmlFormat.setAttribute("top", xmlFormatNode.getAttribute("top"));
                                    xmlFormat.setAttribute("bottom", xmlFormatNode.getAttribute("bottom"));
                                    xmlFormat.setAttribute("name", xmlFormatNode.getAttribute("name"));
                                    statementConfig.setXMLFormat(xmlFormat);
                                }
                            }
                        }
                    }
                }

                // add each statement config to the statement config object collection
                STATEMENT_CONFIG_MAP.put(statementConfig.getAttribute("id"), statementConfig);

            }
            SINGLETON_INITIALIZED = true;
        }
        finally {

        }
          
      }
      
    }

  /**
   * Returns the appropriate statement handler or null if it can't match any.
   * @param statementConfig
   * @return 
   */
    private ExecuteSQLStatement retrieveDataResponse(StatementConfig statementConfig)
    {

        if (statementConfig.getAttribute("type").equals("java.sql.CallableStatement"))
        {
            if (statementConfig.getOutput() == null) 
            {
                // default it to this
                return cStmtReturnCRSet;
            }
            else if (statementConfig.getOutput().getAttribute("type").equals("java.lang.String"))
            {
                return cStmtReturnXMLString;
            }
            else if (statementConfig.getOutput().getAttribute("type").equals("org.w3c.dom.Document"))
            {
                if (statementConfig.getOutput().getAttribute("source").equalsIgnoreCase("AJAX")) {
                    return cStmtReturnXMLAjax;
                } else {
                    return cStmtReturnXMLDOM;
                }
            }
            else if (statementConfig.getOutput().getAttribute("type").equals("com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet"))
            {
                return cStmtReturnCRSet;
            }
            else if (statementConfig.getOutput().getAttribute("type").equals("java.sql.ResultSet"))
            {
                return cStmtReturnResultSet;
            }
            else
            {
                logger.error("Return type not found for " + statementConfig.getOutput().getAttribute("type"));
            }
        }
        else if (statementConfig.getAttribute("type").equals("java.sql.PreparedStatement"))
        {
            if (statementConfig.getOutput() == null) 
            {
                // default it to this
                return pStmtReturnCRSet;
            }
            if (statementConfig.getOutput().getAttribute("type").equals("java.lang.String"))
            {
                return pStmtReturnXMLString;
            }
            else if (statementConfig.getOutput().getAttribute("type").equals("org.w3c.dom.Document"))
            {
                return pStmtReturnXMLDOM;
            }
            else if (statementConfig.getOutput().getAttribute("type").equals("com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet"))
            {
                return pStmtReturnCRSet;
            }
            else if (statementConfig.getOutput().getAttribute("type").equals("java.sql.ResultSet"))
            {
                return pStmtReturnResultSet;
            }
            else if (statementConfig.getOutput().getAttribute("type").equals("java.lang.Integer"))
            {
                return pStmtReturnInteger;
            }
            else
            {
                logger.error("Return type not found for " + statementConfig.getOutput().getAttribute("type"));
            }
        }
        else if (statementConfig.getAttribute("type").equals("java.sql.Statement"))
        {
            if (statementConfig.getOutput() == null) 
            {
                // default it to this
                return stmtReturnCRSet;
            }
            if (statementConfig.getOutput().getAttribute("type").equals("java.lang.String"))
            {
                return stmtReturnXMLString;
            }
            else if (statementConfig.getOutput().getAttribute("type").equals("org.w3c.dom.Document"))
            {
                return stmtReturnXMLDOM;
            }
            else if (statementConfig.getOutput().getAttribute("type").equals("com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet"))
            {
                return stmtReturnCRSet;
            }
            else if (statementConfig.getOutput().getAttribute("type").equals("java.sql.ResultSet"))
            {
                return stmtReturnResultSet;
            }
            else if (statementConfig.getOutput().getAttribute("type").equals("java.lang.Integer"))
            {
                return stmtReturnInteger;
            }
            else
            {
                logger.error("Return type not found for " + statementConfig.getOutput().getAttribute("type"));
            }            
        }
        else
        {
            logger.error("The type of statement " + statementConfig.getAttribute("type") + " does not exist");
        }
        
        return null;

    }

  
}
