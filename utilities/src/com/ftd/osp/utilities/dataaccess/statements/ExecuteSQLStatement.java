package com.ftd.osp.utilities.dataaccess.statements;

import com.ftd.osp.utilities.dataaccess.util.SQLGenerator;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.dataaccess.valueobjects.InParameter;
import com.ftd.osp.utilities.dataaccess.valueobjects.OutParameter;
import com.ftd.osp.utilities.dataaccess.valueobjects.StatementConfig;
import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.osp.utilities.xml.DOMUtil;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;


import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.xml.parsers.ParserConfigurationException;

import oracle.sql.CLOB;

import org.w3c.dom.Document;


public abstract class ExecuteSQLStatement
{
    private static String SQLSTRING_PLACEHOLDER_CONSTANT = "?";

    public abstract Object executeQuery(StatementConfig statementConfig, DataRequest dataRequest)
    throws SQLException, ParserConfigurationException, IOException, Exception;

     /**
      * Set parameters on the callable statement
      *
      * @param statementConfig the statement config object
      * @param inputParams the input parameters for the callable statement
      * @param cstmt the callable statement
      * @param logger the log4j logger
      * @throws SQLException
      * @throws IOException
      */
    public void setParameters(StatementConfig statementConfig,
                              DataRequest dataRequest,
                              CallableStatement cstmt,
                              Logger logger)
      throws SQLException, IOException, Exception{
        Map inputParams = dataRequest.getInputParams();
        InParameter inParameter = null;
        OutParameter outParameter = null;
        String inParameterName = null, inParameterPosition = null, inParameterType = null;
        String outParameterName = null, outParameterPosition = null, outParameterType = null;
        List registeredInputs = statementConfig.getInParameters();
        List registeredOutputs = statementConfig.getOutParameters();


        // register out parameters
        {
            StringBuffer paramListStr = new StringBuffer("Out Parameter List: ");
            for (Iterator iterator = registeredOutputs.iterator(); iterator.hasNext(); )
            {
                outParameter = (OutParameter)iterator.next();
                outParameterName = outParameter.getAttribute("name");
                outParameterPosition = outParameter.getAttribute("position");
                outParameterType = outParameter.getAttribute("type");

                if (outParameterType.equalsIgnoreCase("oracle.jdbc.OracleTypes.CURSOR"))  {
                    loggerStrAppend(paramListStr, outParameterName, "CURSOR", outParameterPosition, logger);
                    cstmt.registerOutParameter(Integer.valueOf(outParameterPosition).intValue(), oracle.jdbc.OracleTypes.CURSOR);
                } else if (outParameterType.equalsIgnoreCase("java.sql.Types.VARCHAR")) {
                    loggerStrAppend(paramListStr, outParameterName, "VARCHAR", outParameterPosition, logger);
                    cstmt.registerOutParameter(Integer.valueOf(outParameterPosition).intValue(), java.sql.Types.VARCHAR);
                } else if (outParameterType.equalsIgnoreCase("java.sql.Types.CHAR")){
                    loggerStrAppend(paramListStr, outParameterName, "CHAR", outParameterPosition, logger);
                    cstmt.registerOutParameter(Integer.valueOf(outParameterPosition).intValue(), java.sql.Types.CHAR);
                } else if (outParameterType.equalsIgnoreCase("java.sql.Types.NUMERIC")) {
                    loggerStrAppend(paramListStr, outParameterName, "NUMERIC", outParameterPosition, logger);
                    cstmt.registerOutParameter(Integer.valueOf(outParameterPosition).intValue(), java.sql.Types.NUMERIC);
                } else if (outParameterType.equalsIgnoreCase("java.sql.Types.CLOB")) {
                    logger.debug("Setting Out Parameter " + outParameterName +" as 'java.sql.Types.CLOB' at position " + Integer.valueOf(outParameterPosition).intValue());
                    cstmt.registerOutParameter(Integer.valueOf(outParameterPosition).intValue(), java.sql.Types.CLOB);
                }
                else if (outParameterType.equalsIgnoreCase("java.sql.Types.DATE")) 
                {
                    logger.debug("Setting Out Parameter " + outParameterName +" as 'java.sql.Types.DATE' at position " + Integer.valueOf(outParameterPosition).intValue());
                    cstmt.registerOutParameter(Integer.valueOf(outParameterPosition).intValue(), java.sql.Types.DATE);
                }
                else if (outParameterType.equalsIgnoreCase("java.sql.Types.TIMESTAMP")) 
                {
                    logger.debug("Setting Out Parameter " + outParameterName +" as 'java.sql.Types.TIMESTAMP' at position " + Integer.valueOf(outParameterPosition).intValue());
                    cstmt.registerOutParameter(Integer.valueOf(outParameterPosition).intValue(), java.sql.Types.TIMESTAMP);
                }
                else if (outParameterType.equalsIgnoreCase("java.sql.Types.REF"))
                {
                    logger.debug("Setting Out Parameter " + outParameterName + " as 'java.sql.Types.REF' at position " + Integer.valueOf(outParameterPosition).intValue());
                    cstmt.setNull(Integer.valueOf(outParameterPosition).intValue(), java.sql.Types.REF);
                    cstmt.registerOutParameter(Integer.valueOf(outParameterPosition).intValue(), java.sql.Types.REF);
                }else{
                
                    throw new IllegalArgumentException(outParameterType + "is not supported");
                }

            }
            if (outParameter != null) {
              logger.debug(paramListStr.toString());
            }
        }


        // register IN parameters
        {
           StringBuffer paramListStr = new StringBuffer("In Parameter List: ");
           //int Integer.valueOf(inParameterPosition).intValue() = 1;
           Iterator iterator = registeredInputs.iterator();

            for (int i = 0; iterator.hasNext() ; i++ )  {
                inParameter = (InParameter)iterator.next();
                inParameterName = inParameter.getAttribute("name");
                inParameterPosition = inParameter.getAttribute("position");
                inParameterType = inParameter.getAttribute("type");
  
                if (inputParams.get(inParameterName) == null) 
                {
                    loggerStrAppend(paramListStr, inParameterName, "NULL", inParameterPosition, logger);
                    cstmt.setNull(Integer.valueOf(inParameterPosition).intValue(), this.getJavaSQLType(inParameterType));
                } else if (inParameterType.equals("java.lang.String"))  {
                    loggerStrAppend(paramListStr, inParameterName, "String", inParameterPosition, logger);
                    cstmt.setString(Integer.valueOf(inParameterPosition).intValue(), (String)inputParams.get(inParameterName) );
                } else if (inParameterType.equals("java.sql.Array"))  {
                    loggerStrAppend(paramListStr, inParameterName, "Array", inParameterPosition, logger);
                    //cstmt.setArray(Integer.valueOf(inParameterPosition).intValue(), (java.sql.Array)inputParams.get(i) );
                    cstmt.setArray(Integer.valueOf(inParameterPosition).intValue(), (java.sql.Array)inputParams.get(inParameterName) );
                } else if (inParameterType.equals("java.math.BigDecimal"))  {
                    loggerStrAppend(paramListStr, inParameterName, "BigDecimal", inParameterPosition, logger);
                    cstmt.setBigDecimal(Integer.valueOf(inParameterPosition).intValue(), (java.math.BigDecimal)inputParams.get(inParameterName) );
                } else if (inParameterType.equals("java.sql.Blob"))  {
                    loggerStrAppend(paramListStr, inParameterName, "Blob", inParameterPosition, logger);
                    cstmt.setBlob(Integer.valueOf(inParameterPosition).intValue(), (java.sql.Blob)inputParams.get(inParameterName) );
                } else if (inParameterType.equals("boolean"))  {
                    loggerStrAppend(paramListStr, inParameterName, "boolean", inParameterPosition, logger);
                    cstmt.setBoolean(Integer.valueOf(inParameterPosition).intValue(), ((Boolean)inputParams.get(inParameterName)).booleanValue() );
                } else if (inParameterType.equals("byte"))  {
                    loggerStrAppend(paramListStr, inParameterName, "byte", inParameterPosition, logger);
                    cstmt.setByte(Integer.valueOf(inParameterPosition).intValue(), ((Byte)inputParams.get(inParameterName)).byteValue() );
                } else if (inParameterType.equals("byte[]"))  { // uses String
                    loggerStrAppend(paramListStr, inParameterName, "byte[]", inParameterPosition, logger);
                    cstmt.setBytes(Integer.valueOf(inParameterPosition).intValue(), ((String)inputParams.get(inParameterName)).getBytes() );
                } else if (inParameterType.equals("java.sql.Clob"))  {
                    loggerStrAppend(paramListStr, inParameterName, "Clob", inParameterPosition, logger);
                    cstmt.setClob(Integer.valueOf(inParameterPosition).intValue(), (java.sql.Clob)inputParams.get(inParameterName) );
                } else if (inParameterType.equals("java.sql.Date"))  {
                    loggerStrAppend(paramListStr, inParameterName, "Date", inParameterPosition, logger);
                    cstmt.setDate(Integer.valueOf(inParameterPosition).intValue(), (java.sql.Date)inputParams.get(inParameterName) );
                } else if (inParameterType.equals("double"))  {
                    loggerStrAppend(paramListStr, inParameterName, "double", inParameterPosition, logger);
                    cstmt.setDouble(Integer.valueOf(inParameterPosition).intValue(), ((Double)inputParams.get(inParameterName)).doubleValue() );
                } else if (inParameterType.equals("float"))  {
                    loggerStrAppend(paramListStr, inParameterName, "float", inParameterPosition, logger);
                    cstmt.setFloat(Integer.valueOf(inParameterPosition).intValue(), ((Float)inputParams.get(inParameterName)).floatValue() );
                } else if (inParameterType.equals("int"))  {
                    loggerStrAppend(paramListStr, inParameterName, "int", inParameterPosition, logger);
                    cstmt.setInt(Integer.valueOf(inParameterPosition).intValue(), ((Integer)inputParams.get(inParameterName)).intValue() );
                } else if (inParameterType.equals("long"))  {
                    loggerStrAppend(paramListStr, inParameterName, "long", inParameterPosition, logger);
                    cstmt.setLong(Integer.valueOf(inParameterPosition).intValue(), ((Long)inputParams.get(inParameterName)).longValue() );
                } else if (inParameterType.equals("null"))  { // uses java.sql.Types as the second parameter
                    loggerStrAppend(paramListStr, inParameterName, "null", inParameterPosition, logger);
                    cstmt.setNull(Integer.valueOf(inParameterPosition).intValue(), ((Integer)inputParams.get(inParameterName)).intValue() );
                } else if (inParameterType.equals("java.lang.Object"))  {
                    loggerStrAppend(paramListStr, inParameterName, "Object", inParameterPosition, logger);
                    cstmt.setObject(Integer.valueOf(inParameterPosition).intValue(), (Object)inputParams.get(inParameterName) );
                } else if (inParameterType.equals("short"))  {
                    loggerStrAppend(paramListStr, inParameterName, "short", inParameterPosition, logger);
                    cstmt.setShort(Integer.valueOf(inParameterPosition).intValue(), ((Short)inputParams.get(inParameterName)).shortValue() );
                } else if (inParameterType.equals("java.sql.Time"))  {
                    loggerStrAppend(paramListStr, inParameterName, "Time", inParameterPosition, logger);
                    cstmt.setTime(Integer.valueOf(inParameterPosition).intValue(), (java.sql.Time)inputParams.get(inParameterName) );
                } else if (inParameterType.equals("java.sql.Timestamp"))  {
                    loggerStrAppend(paramListStr, inParameterName, "Timestamp", inParameterPosition, logger);
                    cstmt.setTimestamp(Integer.valueOf(inParameterPosition).intValue(), (java.sql.Timestamp)inputParams.get(inParameterName) );
                } else if (inParameterType.equals("XML"))  { // to set XML data structures
                    logger.debug("Setting parameter " + inParameterName +" as 'java.lang.String' at position " + Integer.valueOf(inParameterPosition).intValue() );
                    Object xml = inputParams.get(inParameterName);
//                    String xmlData = (xml instanceof Document)?this.getDocumentAsXMLString((Document)xml):(String)xml;
//                    cstmt.setString(Integer.valueOf(inParameterPosition).intValue(), xmlData );
                    CLOB clob = (xml instanceof Document)?this.getCLOB(dataRequest.getConnection(),(Document)xml, logger):this.getCLOB(dataRequest.getConnection(),(String)xml, logger);
                    cstmt.setClob(Integer.valueOf(inParameterPosition).intValue(), clob );
                } else if (inParameterType.equals("oracle.sql.CLOB"))  {
                    loggerStrAppend(paramListStr, inParameterName, "CLOB", inParameterPosition, logger);
                    
                    // JP Puzon 03-30-2006
                    // The following logic compensates for JDBC clients that have submitted
                    // String values to CLOB database columns.
                    Object parmValue = inputParams.get(inParameterName);
                    CLOB clobValue = (parmValue instanceof java.lang.String) ? this.getCLOB(cstmt.getConnection(), (String) parmValue, logger) : (CLOB) parmValue;
                    JDBCUtil.getOraclePreparedStatement(cstmt).setCLOB(Integer.valueOf(inParameterPosition).intValue(), clobValue);
                    
                    // Place the CLOB value in a collection for later reference.
                    dataRequest.addTempClob(clobValue);
                }
            } //END FOR
            if (inParameter != null) {
              logger.debug(paramListStr.toString());
            }
        }
    }

     /**
      * Set parameters on the prepared statement
      *
      * @param statementConfig the statement config object
      * @param inputParams the input parameters for the prepared statement
      * @param pstmt the prepared statement
      * @param logger the log4j logger
      * @throws SQLException
      * @throws IOException
      */
    public void setParameters(StatementConfig statementConfig,
                                DataRequest dataRequest,
                                PreparedStatement pstmt,
                                Logger logger)
            throws SQLException, IOException, Exception {
        Map inputParams = dataRequest.getInputParams();
        InParameter inParameter = null;
        String inParameterName = null, inParameterPosition = null, inParameterType = null;
        List registeredInputs = statementConfig.getInParameters();
        StringBuffer paramListStr = new StringBuffer("Parameter list: ");

        for (Iterator iterator = registeredInputs.iterator(); iterator.hasNext() ;  )  {
            inParameter = (InParameter)iterator.next();
            inParameterName = inParameter.getAttribute("name");
            inParameterPosition = inParameter.getAttribute("position");
            inParameterType = inParameter.getAttribute("type");
            
            if (inputParams.get(inParameterName) == null) 
            {
                loggerStrAppend(paramListStr, inParameterName, "NULL", inParameterPosition, logger);
                pstmt.setNull(Integer.valueOf(inParameterPosition).intValue(), this.getJavaSQLType(inParameterType));
            } else if (inParameterType.equals("java.lang.String"))  {
                loggerStrAppend(paramListStr, inParameterName, "String", inParameterPosition, logger);
                pstmt.setString(Integer.valueOf(inParameterPosition).intValue(), (String)inputParams.get(inParameterName) );
            } else if (inParameterType.equals("java.sql.Array"))  {
                loggerStrAppend(paramListStr, inParameterName, "Array", inParameterPosition, logger);
                pstmt.setArray(Integer.valueOf(inParameterPosition).intValue(), (java.sql.Array)inputParams.get(inParameterName) );
            } else if (inParameterType.equals("java.math.BigDecimal"))  {
                loggerStrAppend(paramListStr, inParameterName, "BigDecimal", inParameterPosition, logger);
                pstmt.setBigDecimal(Integer.valueOf(inParameterPosition).intValue(), (java.math.BigDecimal)inputParams.get(inParameterName) );
            } else if (inParameterType.equals("java.sql.Blob"))  {
                loggerStrAppend(paramListStr, inParameterName, "Blob", inParameterPosition, logger);
                pstmt.setBlob(Integer.valueOf(inParameterPosition).intValue(), (java.sql.Blob)inputParams.get(inParameterName) );
            } else if (inParameterType.equals("boolean"))  {
                loggerStrAppend(paramListStr, inParameterName, "boolean", inParameterPosition, logger);
                pstmt.setBoolean(Integer.valueOf(inParameterPosition).intValue(), ((Boolean)inputParams.get(inParameterName)).booleanValue() );
            } else if (inParameterType.equals("byte"))  {
                loggerStrAppend(paramListStr, inParameterName, "byte", inParameterPosition, logger);
                pstmt.setByte(Integer.valueOf(inParameterPosition).intValue(), ((Byte)inputParams.get(inParameterName)).byteValue() );
            } else if (inParameterType.equals("byte[]"))  { // uses String
                loggerStrAppend(paramListStr, inParameterName, "byte[]", inParameterPosition, logger);
                pstmt.setBytes(Integer.valueOf(inParameterPosition).intValue(), ((String)inputParams.get(inParameterName)).getBytes() );
            } else if (inParameterType.equals("java.sql.Clob"))  {
                loggerStrAppend(paramListStr, inParameterName, "Clob", inParameterPosition, logger);
                pstmt.setClob(Integer.valueOf(inParameterPosition).intValue(), (java.sql.Clob)inputParams.get(inParameterName) );
            } else if (inParameterType.equals("java.sql.Date"))  {
                loggerStrAppend(paramListStr, inParameterName, "Date", inParameterPosition, logger);
                pstmt.setDate(Integer.valueOf(inParameterPosition).intValue(), (java.sql.Date)inputParams.get(inParameterName) );
            } else if (inParameterType.equals("double"))  {
                loggerStrAppend(paramListStr, inParameterName, "double", inParameterPosition, logger);
                pstmt.setDouble(Integer.valueOf(inParameterPosition).intValue(), ((Double)inputParams.get(inParameterName)).doubleValue() );
            } else if (inParameterType.equals("float"))  {
                loggerStrAppend(paramListStr, inParameterName, "float", inParameterPosition, logger);
                pstmt.setFloat(Integer.valueOf(inParameterPosition).intValue(), ((Float)inputParams.get(inParameterName)).floatValue() );
            } else if (inParameterType.equals("int"))  {
                loggerStrAppend(paramListStr, inParameterName, "int", inParameterPosition, logger);
                pstmt.setInt(Integer.valueOf(inParameterPosition).intValue(), ((Integer)inputParams.get(inParameterName)).intValue() );
            } else if (inParameterType.equals("long"))  {
                loggerStrAppend(paramListStr, inParameterName, "long", inParameterPosition, logger);
                pstmt.setLong(Integer.valueOf(inParameterPosition).intValue(), ((Long)inputParams.get(inParameterName)).longValue() );
            } else if (inParameterType.equals("null"))  { // uses java.sql.Types
                loggerStrAppend(paramListStr, inParameterName, "null", inParameterPosition, logger);
                pstmt.setNull(Integer.valueOf(inParameterPosition).intValue(), ((Integer)inputParams.get(inParameterName)).intValue() );
            } else if (inParameterType.equals("java.lang.Object"))  {
                loggerStrAppend(paramListStr, inParameterName, "Object", inParameterPosition, logger);
                pstmt.setObject(Integer.valueOf(inParameterPosition).intValue(), (Object)inputParams.get(inParameterName) );
            } else if (inParameterType.equals("short"))  {
                loggerStrAppend(paramListStr, inParameterName, "short", inParameterPosition, logger);
                pstmt.setShort(Integer.valueOf(inParameterPosition).intValue(), ((Short)inputParams.get(inParameterName)).shortValue() );
            } else if (inParameterType.equals("java.sql.Time"))  {
                loggerStrAppend(paramListStr, inParameterName, "Time", inParameterPosition, logger);
                pstmt.setTime(Integer.valueOf(inParameterPosition).intValue(), (java.sql.Time)inputParams.get(inParameterName) );
            } else if (inParameterType.equals("java.sql.Timestamp"))  {
                loggerStrAppend(paramListStr, inParameterName, "Timestamp", inParameterPosition, logger);
                pstmt.setTimestamp(Integer.valueOf(inParameterPosition).intValue(), (java.sql.Timestamp)inputParams.get(inParameterName) );
            } else if (inParameterType.equals("XML"))  { // to set XML data structures
                logger.debug("Setting parameter " + inParameterName +" as 'java.lang.String' at position " + Integer.valueOf(inParameterPosition).intValue() );
                Object xml = inputParams.get(inParameterName);
                //String xmlData = (xml instanceof Document)?this.getDocumentAsXMLString((Document)xml):(String)xml;
                //pstmt.setString(Integer.valueOf(inParameterPosition).intValue(), xmlData );
                CLOB clob = (xml instanceof Document)?this.getCLOB(dataRequest.getConnection(),(Document)xml,logger):this.getCLOB(dataRequest.getConnection(),(String)xml,logger);
                pstmt.setClob(Integer.valueOf(inParameterPosition).intValue(), clob );
            } else if (inParameterType.equals("oracle.sql.CLOB"))  {
                    loggerStrAppend(paramListStr, inParameterName, "CLOB", inParameterPosition, logger);
                    
                    // JP Puzon 03-30-2006
                    // The following logic compensates for JDBC clients that have submitted
                    // String values to CLOB database columns.
                    Object parmValue = inputParams.get(inParameterName);
                    CLOB clobValue = (parmValue instanceof java.lang.String) ? this.getCLOB(pstmt.getConnection(), (String) parmValue, logger) : (CLOB) parmValue;
                    JDBCUtil.getOraclePreparedStatement(pstmt).setCLOB(Integer.valueOf(inParameterPosition).intValue(), clobValue);
                    
                    // Place the CLOB value in a collection for later reference.
                    dataRequest.addTempClob(clobValue);
                }
        }
        logger.debug(paramListStr.toString());

    }
     /**
      * Set parameters on the statement
      *
      * @param statementConfig the statement config object
      * @param inputParams the input parameters for the statement
      * @param logger the log4j logger
      * @exception SQLException
      */
    public String generateSQL(StatementConfig statementConfig,
                                Map inputParams,
                                Logger logger)
        {
        StringTokenizer st = new StringTokenizer(statementConfig.getSQL(), SQLSTRING_PLACEHOLDER_CONSTANT);
        StringBuffer tempSQLString = new StringBuffer();
        String inParameterName = null;
        String inputParam = null;
        List registeredInputs = statementConfig.getInParameters();
        Iterator iterator = registeredInputs.iterator();
        StringBuffer paramListStr = new StringBuffer("Parameter list: ");

        for (int parameterIndex = 1; iterator.hasNext() && st.hasMoreTokens(); parameterIndex++ )  {
            inParameterName = ((InParameter)iterator.next()).getAttribute("name");

            inputParam = SQLGenerator.convertToSQLSafeString((String)inputParams.get(inParameterName));
            tempSQLString.append(st.nextToken());
            tempSQLString.append("'");
            tempSQLString.append(inputParam);
            tempSQLString.append("'");
            loggerStrAppend(paramListStr, inputParam, null, parameterIndex, logger);
        }
        logger.debug(paramListStr.toString());
        return tempSQLString.toString();
    }


    /**
     * Store ResultSet as a CachedResultSet in the output parameters Map.  
     * If no data exists create and empty CachedResultSet.
     * Key name for the CachedResultSet is RESULT_SET
     *
     * @param rset The ResultSet
     * @param outParametersMap Existing map of output parameters
     * @throws Exception
     */
    public void setResultSet(ResultSet rset, Map outParametersMap)
        throws Exception
    {
        CachedResultSet crs = new CachedResultSet();

        if(rset != null)  {
            crs.populate(rset);
            // close the associated result set
            if (rset != null)
            {
                rset.close();
            }
        }

        outParametersMap.put("RESULT_SET", crs);
    }


    /**
     * Returns an XML Document as a String
     *
     * @param doc
     * @return
     * @throws java.io.IOException
     */
  private String getDocumentAsXMLString(Document doc) throws IOException, Exception
  {
    String xmlString = null;
    StringWriter sw = null;
    PrintWriter pw = null;
    try
    {
      sw = new StringWriter();
      pw = new PrintWriter(sw, true);
      DOMUtil.print(doc, pw);
      xmlString = sw.toString();
    } finally
    {
      pw.close();
    }
    return xmlString;
  }


   /**
   * Creates an oracle.sql.CLOB object from the XML document
   * @param con
   * @param doc
   * @param logger
   * @return
   * @throws java.sql.SQLException
   * @throws java.io.IOException
   */
  private CLOB getCLOB(Connection con, Document doc, Logger logger) throws SQLException, IOException, Exception {
    CLOB tempClob = null;
    PrintWriter printWriter = null;
    try{
      tempClob = this.openCLOB(con, logger);
      // Write the data into the temporary CLOB
      printWriter = new PrintWriter(tempClob.getCharacterOutputStream());
      DOMUtil.print(doc, printWriter);

    } catch(SQLException se){
      tempClob.freeTemporary();
      logger.error(se);
      throw se;
    } catch(IOException ioe){
      tempClob.freeTemporary();
      logger.error(ioe);
      throw ioe;
    } finally
    {
      if (tempClob != null && tempClob.isOpen())
      {
        // Close the temporary CLOB
        tempClob.close();
        logger.debug("Temporary CLOB closed");
      }
      if (printWriter != null)
      {
          printWriter.close();
      }
    }
    return tempClob;
  }

   /**
   * Creates an oracle.sql.CLOB object from the XML string
   * @param con
   * @param xmlData
   * @param logger
   * @return
   * @throws java.sql.SQLException
   * @throws java.io.IOException
   */
  private CLOB getCLOB(Connection con, String xmlData, Logger logger) throws SQLException, IOException{
    CLOB tempClob = null;
    try{
      tempClob = this.openCLOB(con, logger);

      // Get the output stream to write
      Writer tempClobWriter = tempClob.getCharacterOutputStream();
      // Write the data into the temporary CLOB
      tempClobWriter.write(xmlData);

      // Flush and close the stream
      tempClobWriter.flush();
      tempClobWriter.close();

    } catch(SQLException se){
      // tempClob.freeTemporary();
      logger.error(se);
      throw se;
    } catch(IOException ioe){
      // tempClob.freeTemporary();
      logger.error(ioe);
      throw ioe;
    } finally
    {
      if (tempClob != null && tempClob.isOpen())
      {
        // Close the temporary CLOB
        tempClob.close();
        logger.debug("Temporary CLOB closed");
      }
    }
    return tempClob;
  }


  /**
   * Returns a CLOB
   *
   * @param con
   * @return
   * @throws java.sql.SQLException
   * @throws java.io.IOException
   */
  private CLOB openCLOB(Connection con, Logger logger)  throws SQLException, IOException
  {
    // the createTemporary() method expects an oracle.jdbc.driver.OracleConnection
    // but the application server provides an com.evermind.sql.OrclCMTConnection
    // If the temporary CLOB has not yet been created, create new
    // 8/1/06 Removed the work around, not needed in 10G
//    CLOB tempClob = CLOB.createTemporary(this.getOracleConnection(con, logger), true, CLOB.DURATION_SESSION);
    
    CLOB tempClob = CLOB.createTemporary(JDBCUtil.getOracleConnection(con), true, CLOB.DURATION_SESSION);

    // Open the temporary CLOB in readwrite mode to enable writing
    tempClob.open(CLOB.MODE_READWRITE);
    logger.debug("Temporary CLOB opened in read-write mode");
    return tempClob;
  }


     /**
      * Internal method to build list of parameter data for logger
      *
      * @param a_paramLogStr StringBuffer to append parameter to
      * @param a_paramName   Name of parameter
      * @param a_type        Parameter type
      * @param a_position    Position in parameter list
      * @param a_logger      The log4j logger
      */
    private void loggerStrAppend(StringBuffer a_paramLogStr, String a_paramName, String a_type, String a_position, Logger a_logger) {
      if (a_logger.isDebugEnabled()) {
        loggerStrAppend(a_paramLogStr, a_paramName, a_type, Integer.valueOf(a_position).intValue(), a_logger);
      }
    }
     /**
      * Internal method to build list of parameter data for logger
      *
      * @param a_paramLogStr StringBuffer to append parameter to
      * @param a_paramName   Name of parameter
      * @param a_type        Parameter type
      * @param a_position    Position in parameter list
      * @param a_logger      The log4j logger
      */
    private void loggerStrAppend(StringBuffer a_paramLogStr, String a_paramName, String a_type, int a_position, Logger a_logger) {
      if (a_logger.isDebugEnabled()) {
        a_paramLogStr.append(a_position);
        a_paramLogStr.append(":");
        a_paramLogStr.append(a_paramName);
        if (a_type != null) {
          a_paramLogStr.append("-");
          a_paramLogStr.append(a_type);
        }
        a_paramLogStr.append(" ");
      }

    }
    
  /**
   * Returns the associated Java SQL type value
   */
  private int getJavaSQLType(String inParameterType) {

      if (inParameterType.equals("java.lang.String"))  {  
          return Types.VARCHAR;            
      } else if (inParameterType.equals("java.sql.Array"))  {               
          return Types.ARRAY;                                               
      } else if (inParameterType.equals("java.math.BigDecimal"))  {        
          return Types.BIGINT;                                            
      } else if (inParameterType.equals("java.sql.Blob"))  {   
          return Types.BLOB;
      } else if (inParameterType.equals("boolean"))  {                    
          return Types.BOOLEAN;                                           
      } else if (inParameterType.equals("byte"))  {                    
          return Types.NUMERIC;                                        
      } else if (inParameterType.equals("byte[]"))  { // uses String   
          return Types.NUMERIC;                                        
      } else if (inParameterType.equals("java.sql.Clob"))  {           
          return Types.CLOB;                                           
      } else if (inParameterType.equals("java.sql.Date"))  {          
          return Types.DATE;                                          
      } else if (inParameterType.equals("double"))  {                 
          return Types.DOUBLE;                                        
      } else if (inParameterType.equals("float"))  {                  
          return Types.FLOAT;                                         
      } else if (inParameterType.equals("int"))  {                    
          return Types.INTEGER;                                      
      } else if (inParameterType.equals("long"))  {                  
          return Types.LONGVARBINARY;                                
      } else if (inParameterType.equals("null"))  { // uses java.sql.
          return Types.NULL;                                         
      } else if (inParameterType.equals("java.lang.Object"))  {      
          return Types.JAVA_OBJECT;                                  
      } else if (inParameterType.equals("short"))  {                 
          return Types.SMALLINT;                                    
      } else if (inParameterType.equals("java.sql.Time"))  {        
          return Types.TIME;  
      } else if (inParameterType.equals("java.sql.Timestamp"))  {     
          return Types.TIMESTAMP;                                   
      } else if (inParameterType.equals("XML"))  {                  
          return Types.CLOB;                                        
      } else if (inParameterType.equals("oracle.sql.CLOB"))  {                  
          return Types.CLOB;                                        
      } else                                                        
      {                                                             
        return -1;                                                  
      }                                                             

    }                          

}