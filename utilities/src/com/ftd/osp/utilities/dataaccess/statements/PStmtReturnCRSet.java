package com.ftd.osp.utilities.dataaccess.statements;

import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.dataaccess.valueobjects.StatementConfig;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.Map;

/**
 * Executes the SQL query and returns the results as a cached result set
 *
 * @author Anshu Gaind
 */

public class PStmtReturnCRSet extends ExecuteSQLStatement {
    private Logger logger;
    
    /**
     * The default constructor
     */
    public PStmtReturnCRSet(){
        logger = new Logger("com.ftd.osp.utilities.dataaccess.statements.PStmtReturnCRSet");
    }

    /**
     * Executes the SQL query and returns the results as a cached result set
     * 
     * @param statementConfig the statement configuration
     * @param dataRequest the data request object
     * @throws SQLException 
     * @throws IOException 
     * @return the cached rowset
     */
    public Object executeQuery(StatementConfig statementConfig, DataRequest dataRequest) 
      throws SQLException, IOException{
       Connection con = null;
       CachedResultSet crs = null;
       PreparedStatement pstmt = null;
       ResultSet rset = null;
       Map inputParams = dataRequest.getInputParams();

       try  {
            // request connection
            con = (Connection) dataRequest.getConnection();           
            
            pstmt = con.prepareStatement(statementConfig.getSQL());
            //logger.debug(statementConfig.getSQL());
            logger.debug(statementConfig.getSQL());
            // set input parameters
            super.setParameters(statementConfig, dataRequest, pstmt, logger);
            rset = pstmt.executeQuery();

            // return the cached result set
            crs = new CachedResultSet();
            crs.populate(rset);
            logger.debug("SQL Query Executed Successfully");

       } catch (SQLException ex)  {
            logger.error(ex);
            throw ex;
       } catch (IOException ioe)  {
            logger.error(ioe);
            throw ioe;
       } catch (Exception e)  {
            logger.error(e);
            throw new SQLException(e.toString());
       } finally {
            try{
                if (rset != null){
                    rset.close();
                }
                if (pstmt != null){
                    pstmt.close();
                }
            } catch (SQLException se){
                logger.error(se);
                throw se;
            }
       }
       return crs;
     }



}