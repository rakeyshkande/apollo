package com.ftd.osp.utilities.dataaccess.statements;

import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.dataaccess.valueobjects.StatementConfig;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import java.util.Map;

/**
 * Executes the SQL query and returns the results as an Integer. Used for 'inserts' and 'updates'.
 *
 * @author Anshu Gaind
 */

public class StmtReturnInteger extends ExecuteSQLStatement  {
    private Logger logger;

    /**
     * The default constructor
     */
    public StmtReturnInteger() {
        logger = new Logger("com.ftd.osp.utilities.dataaccess.statements.StmtReturnInteger");
    }
    
    /**
     * Executes the SQL query and returns the results as an Integer. Used for 'inserts' and 'updates'.
     * 
     * @param statementConfig the statement configuration
     * @param dataRequest the data request object
     * @exception SQLException 
     * @return the return value from the insert or the update
     */
    public Object executeQuery(StatementConfig statementConfig, DataRequest dataRequest) throws SQLException{
        Connection con = null;
        Integer rv = null;
        Statement stmt = null;
        Map inputParams = dataRequest.getInputParams();

        try  {
            // generate the SQL string
            String sql = super.generateSQL(statementConfig, inputParams, logger);
            logger.debug(sql);

            // request connection
            con = (Connection) dataRequest.getConnection();

            stmt = con.createStatement();
            rv = new Integer(stmt.executeUpdate(sql));
            logger.debug("SQL Query Executed Successfully");

        } catch (SQLException ex)  {
            logger.error(ex);
            throw ex;
        } finally {
            try{
                if (stmt != null){
                    stmt.close();
                }
            } catch (SQLException se){
                logger.error(se);
                throw se;
            }
        }
       
        return rv;
    }



}