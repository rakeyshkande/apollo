package com.ftd.osp.utilities.dataaccess.statements;

import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.Statement;

import oracle.jdbc.driver.OracleConnection;
import oracle.jdbc.OraclePreparedStatement;

import com.ftd.osp.utilities.plugins.Logger;

/**
 * Utility methods for working with JDBC APIs
 */
public class JDBCUtil
{
    static Logger logger = new Logger(JDBCUtil.class.getName());
    
    /**
     * Unwraps an Oracle Connection from the passed in Connection which may be
     * a ConnectionWrapper from JBOSS
     * @param connection
     * @return
     */
    public static OracleConnection getOracleConnection(Connection connection)
    {
        // If we are running in OC4J, this is already an OracleConnection
        if (connection == null || connection instanceof OracleConnection) {
            return (OracleConnection) connection;
        }         
        
        try
        {
            Method getUnderlyingConnectionMethod = connection.getClass().getMethod("getUnderlyingConnection", (Class[]) null);
            Connection retVal = (Connection) getUnderlyingConnectionMethod.invoke(connection, (Object[]) null);
            
            if(retVal instanceof OracleConnection)
            {
                return (OracleConnection) retVal;
            }
            
            logger.debug("Underlying connection not an Oracle connection: " + retVal.getClass().getName());
            
            // Recurse
            return getOracleConnection(retVal);
        }
        catch (Exception e)
        {
            logger.warn("Unable to unwrap connection. Not a JBOSS Wrapped Connection", e);
        }
        
        return null;
    }
    
    /**
     * Unwraps an Oracle PreparedStatement from the passed in Statement which may be
     * a StatementWrapper from JBOSS
     * @param statement
     * @return
     */
    public static OraclePreparedStatement getOraclePreparedStatement(Statement statement)
    {
        // If we are running in OC4J, this is already an OraclePreparedStatement
        if (statement == null || statement instanceof OraclePreparedStatement) {
            return (OraclePreparedStatement) statement;
        }         
        
        try
        {
            Method getUnderlyingStatementMethod = statement.getClass().getMethod("getUnderlyingStatement", (Class[]) null);
            Statement retVal = (Statement) getUnderlyingStatementMethod.invoke(statement, (Object[]) null);
            
            if(retVal instanceof OraclePreparedStatement)
            {
                return (OraclePreparedStatement) retVal;
            }
            
            logger.debug("Underlying statement not an Oracle statement: " + retVal.getClass().getName());
            
            // Recurse
            return getOraclePreparedStatement(retVal);
        }
        catch (Exception e)
        {
            logger.warn("Unable to unwrap statement. Not a JBOSS Wrapped Statement", e);
        }
        
        return null;        
        
        
    }

}
