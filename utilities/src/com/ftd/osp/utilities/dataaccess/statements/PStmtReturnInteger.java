package com.ftd.osp.utilities.dataaccess.statements;

import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.dataaccess.valueobjects.StatementConfig;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import java.util.Map;

/**
 * Executes the SQL query and returns the results as an Integer. Used for 'inserts' and 'updates'.
 *
 * @author Anshu Gaind
 */

public class PStmtReturnInteger extends ExecuteSQLStatement {
    private Logger logger;

    /**
     * The default constructor
     */
    public PStmtReturnInteger(){
        logger = new Logger("com.ftd.osp.utilities.dataaccess.statements.PStmtReturnInteger");
    }

    /**
     * Executes the SQL query and returns the results as an Integer.
     * Used for 'inserts' and 'updates'.
     * 
     * @param statementConfig the statement configuration
     * @param dataRequest the data request object
     * @throws SQLException 
     * @throws IOException
     * @return the return value from the insert or the update
     */
    public Object executeQuery(StatementConfig statementConfig, DataRequest dataRequest) 
      throws SQLException, IOException{
        Connection con = null;
        Integer rv = null;
        PreparedStatement pstmt = null;
        Map inputParams = dataRequest.getInputParams();

        try  {
            // request connection
            con = (Connection) dataRequest.getConnection();
           
            pstmt = con.prepareStatement(statementConfig.getSQL());
            logger.debug(statementConfig.getSQL());
            // set input parameters
            super.setParameters(statementConfig, dataRequest, pstmt, logger);
            rv = new Integer(pstmt.executeUpdate());

            logger.debug("SQL Query Executed Successfully");
        } catch (SQLException ex)  {
            logger.error(ex);
            throw ex;
        } catch (IOException ioe)  {
            logger.error(ioe);
            throw ioe;            
        } catch (Exception e)  {
            logger.error(e);
            throw new SQLException(e.toString());
        } finally {
            try{
                if (pstmt != null){
                    pstmt.close();
                }
            } catch (SQLException se){
                logger.error(se);
                throw se;
            }
        }
        return rv;
    }



}