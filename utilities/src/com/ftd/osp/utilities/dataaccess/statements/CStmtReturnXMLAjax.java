package com.ftd.osp.utilities.dataaccess.statements;

import com.ftd.osp.utilities.dataaccess.util.ResultSet2XML;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.dataaccess.valueobjects.OutParameter;
import com.ftd.osp.utilities.dataaccess.valueobjects.StatementConfig;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * Executes the SQL query and returns the results as an XML Document or a map
 * of all output parameters as /callXML documents.
 *
 * @author Anshu Gaind
 */

public class CStmtReturnXMLAjax  extends ExecuteSQLStatement {
    private Logger logger;
    
    /**
     * The default constructor
     */
    public CStmtReturnXMLAjax(){
        logger = new Logger("com.ftd.osp.utilities.dataaccess.statements.CStmtReturnXMLAjax");
    }

    /**
     * Executes the SQL query and returns the results as an XML Document or a map
     * of all output parameters as XML documents.
     * 
     * @param statementConfig the statement configuration
     * @param dataRequest the data request object
     * @throws SQLException 
     * @throws ParserConfigurationException
     * @throws IOException
     * @return the xml document
     */
    public Object executeQuery(StatementConfig statementConfig, DataRequest dataRequest)
        throws SQLException, ParserConfigurationException, IOException{
        Connection con = null;
        //ServiceManagerDelegate delegate = new ServiceManagerDelegate();
        CallableStatement cstmt = null;
        ResultSet rset = null;
        HashMap outParametersMap = new HashMap();
        Map resultSets = null;

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.newDocument();
        Element resultXML = document.createElement("result");
        String resultStatus = "Y";
        String resultMessage = "";
        String outStatus = "Y";
        String outMessage = "";
        String emptyMessageDefault = "Database query returned no results.";
        HashMap emptyMap = new HashMap();
        
        try  {
            // request connection
            con = dataRequest.getConnection();
            
            cstmt = con.prepareCall(statementConfig.getSQL());
            logger.debug(statementConfig.getSQL());
            // set input parameters
            super.setParameters(statementConfig, dataRequest, cstmt, logger);
            
            boolean cstmtResult = cstmt.execute();

            OutParameter outParameter = null;
            String outParameterName = null, outParameterPosition = null, outParameterType = null, outParameterId = null;
            List outParameters = statementConfig.getOutParameters();

            // build result set
            if (cstmtResult) {
                setResultSet(cstmt.getResultSet(), outParametersMap);
            }

            for (Iterator iterator = outParameters.iterator(); iterator.hasNext();) {
                outParameter = (OutParameter)iterator.next();
                outParameterName = outParameter.getAttribute("name");
                outParameterId = outParameter.getAttribute("id");
                outParameterPosition = outParameter.getAttribute("position");
                outParameterType = outParameter.getAttribute("type");
                if (outParameterId == null || outParameterId.equals("")) {
                    outParameterId = outParameterName.toLowerCase();
                }
                logger.debug(outParameterName + " " + outParameterId + " " + outParameterType);

                if (outParameterType.equals("oracle.jdbc.OracleTypes.CURSOR") || outParameterType.equals("java.sql.Types.REF")) 
                {
                    rset = (ResultSet)cstmt.getObject(Integer.valueOf(outParameterPosition).intValue());
                    if (resultSets == null)  
                    {
                        resultSets = new HashMap();
                    }
                    // add to the collection of result sets
                    resultSets.put(outParameterId, rset);
                    if (outParameter.getAttribute("emptyMessage") != null &&
                      !outParameter.getAttribute("emptyMessage").equals("")) {
                        emptyMap.put(outParameterId, outParameter.getAttribute("emptyMessage"));
                    }
                }  
                else 
                {
                    if (outParameterName.equalsIgnoreCase("OUT-STATUS")) {
                        outStatus = cstmt.getString(Integer.valueOf(outParameterPosition).intValue());
                    }
                    else if (outParameterName.equalsIgnoreCase("OUT-MESSAGE")) {
                        outMessage = cstmt.getString(Integer.valueOf(outParameterPosition).intValue());
                    }
                    else {
                        // add to the collection of the other out parameters
                        outParametersMap.put(outParameterName, cstmt.getString(Integer.valueOf(outParameterPosition).intValue()));
                    }
                }
            }
           
            logger.debug(statementConfig.getAttribute("id")+":: Using ResultSet2XML utility to build the XML document");

            // convert result sets to xml dom trees and add to document collection
            if (resultSets != null)  {
                ResultSet resultSet = null;
                String key = null;
                Element row = null;
                
                for (Iterator iter = resultSets.keySet().iterator(); iter.hasNext() ; )  {
                    key = (String)iter.next();
                    resultSet = (ResultSet)resultSets.get(key);

                    logger.debug("Calling convertToXMLAjax: " + key);
                    row = ResultSet2XML.convertToXMLAjax(resultSet, key, document);
                    // close result set
                    if (resultSet != null)  {
                        resultSet.close();
                    }
                    if (row.hasChildNodes()) {
                        row.setAttribute("status", outStatus);
                        row.setAttribute("message", outMessage != null ? outMessage : "");
                    } else {
                        row.setAttribute("status", "N");
                        String emptyMessage = (String) emptyMap.get(key);
                        if (emptyMessage == null) {
                            emptyMessage = emptyMessageDefault;
                        }
                        row.setAttribute("message", emptyMessage);
                    }
                    resultXML.appendChild(row);
                }
            }
            
            // convert registered Out Parameters to an xml dom tree and add to document collection
            if (outParametersMap.size() > 0)  {
                logger.debug(statementConfig.getAttribute("id")+":: Adding registered out parameters to the document tree");

                Element root = document.createElement("rs");
                root.setAttribute("name", "out-parameters");
                root.setAttribute("status", outStatus);
                root.setAttribute("message", outMessage != null ? outMessage : "");
                // append root to doc
                resultXML.appendChild(root);
                
                Element row = document.createElement("record");
                row.setAttribute("row", "1");
                    
                Element outParameter_E = null;
                String nodeName = null;
            
                for (Iterator iterator = outParametersMap.keySet().iterator(); iterator.hasNext(); )  {
                    nodeName = (String)iterator.next();
                    outParameter_E = document.createElement(nodeName);
                    if(outParametersMap.get(nodeName)!=null && !((String)outParametersMap.get(nodeName)).equals(""))
                    {
                        outParameter_E.appendChild(document.createTextNode((String)outParametersMap.get(nodeName)));
                    }
                    // append to root
                    row.appendChild(outParameter_E);
                }
                root.appendChild(row);
            }
            
            logger.debug(statementConfig.getAttribute("id")+":: SQL Query  Executed Successfully");
            
        } catch (ParserConfigurationException pce)  {
            logger.error(pce);
            resultStatus = "N";
            throw pce;
        } catch (SQLException ex)  {
            logger.error(ex);
            resultStatus = "N";
            throw ex;
        } catch (IOException ioe)  {
            logger.error(ioe);
            resultStatus = "N";
            throw ioe;
        } catch (Exception ex)  {
            logger.error(ex);
            resultStatus = "N";
            throw new SQLException(ex.toString());
        } finally {
            try{
                if (rset != null){
                    rset.close();
                }
                if (cstmt != null){
                    cstmt.close();
                }
            } catch (SQLException se){
                logger.error(se);
                resultStatus = "N";
                throw se;
            }
        }

        resultXML.setAttribute("status", resultStatus);
        resultXML.setAttribute("message", resultMessage);
        document.appendChild(resultXML);

        return document;
        
    }


}