package com.ftd.osp.utilities.dataaccess.statements;

import com.ftd.osp.utilities.dataaccess.util.ResultSet2XML;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.dataaccess.valueobjects.StatementConfig;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import java.util.Map;

/**
 * Executes the SQL query and returns the results as an XML String
 *
 * @author Anshu Gaind
 */

public class StmtReturnXMLString extends ExecuteSQLStatement {
    private Logger logger;

    /**
     * The default constructor
     */
    public StmtReturnXMLString(){
        logger = new Logger("com.ftd.osp.utilities.dataaccess.statements.StmtReturnXMLString");
    }

    /**
     * Executes the SQL query and returns the results as an XML String
     * 
     * @param statementConfig the statement configuration
     * @param dataRequest the data request object
     * @exception SQLException 
     * @return the xml string
     */
    public Object executeQuery(StatementConfig statementConfig, DataRequest dataRequest) throws SQLException {
        Connection con = null;
        String XMLString = null;
        Statement stmt = null;
        ResultSet rset = null;
        Map inputParams = dataRequest.getInputParams();
       
        try {
            // generate the SQL string
            String sql = super.generateSQL(statementConfig, inputParams, logger);
            logger.debug(sql);

            // request connection
            con = (Connection) dataRequest.getConnection();

            logger.debug("Using ResultSet2XML utility to build the XML string");
            // use the ResultSet2XMl utility to obtain the XML string
            stmt = con.createStatement();
            rset = stmt.executeQuery(sql);
            // return the xml string
            XMLString = ResultSet2XML.convertToXMLString(rset, statementConfig.getXMLFormat());
            
            logger.debug("SQL Query Executed Successfully");
        } 
        finally {
            try{
                if (rset != null){
                    rset.close();
                }
                if (stmt != null){
                    stmt.close();
                }
            } catch (SQLException se){
                logger.error(se);
                throw se;
            }
        }
        
        return XMLString;
    }



}