package com.ftd.osp.utilities.dataaccess.statements;

import com.ftd.osp.utilities.dataaccess.util.ResultSet2XML;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.dataaccess.valueobjects.StatementConfig;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.Map;

/**
 * Executes the SQL query and returns the results as an XML string
 *
 * @author Anshu Gaind
 */

public class PStmtReturnXMLString  extends ExecuteSQLStatement {
    private Logger logger;
   
    /**
     * The default constructor
     */
    public PStmtReturnXMLString(){
        logger = new Logger("com.ftd.osp.utilities.dataaccess.statements.PStmtReturnXMLString");
    }

    /**
     * Executes the SQL query and returns the results as an XML String
     * 
     * @param statementConfig the statement configuration
     * @param dataRequest the data request object
     * @throws SQLException 
     * @throws IOException
     * @return the xml string
     */
    public Object executeQuery(StatementConfig statementConfig, DataRequest dataRequest )
      throws SQLException, IOException {
        Connection con = null;
        String XMLString = null;
        PreparedStatement pstmt = null;
        ResultSet rset = null;
        Map inputParams = dataRequest.getInputParams();

        try  {
            // request connection
            con = (Connection) dataRequest.getConnection();       

            pstmt = con.prepareStatement(statementConfig.getSQL());
            logger.debug(statementConfig.getSQL());
            
            // set input parameters
            super.setParameters(statementConfig, dataRequest, pstmt, logger);
            rset = pstmt.executeQuery();

            logger.debug("Using ResultSet2XML utility to build the XML document");
            // use the ResultSet2DOM utility to obtain the XML string
            // return the xml string
            XMLString = ResultSet2XML.convertToXMLString(rset, statementConfig.getXMLFormat());

            logger.debug("SQL Query Executed Successfully");
            
        } catch (SQLException ex)  {
            logger.error(ex);
            throw ex;
        } catch (IOException ioe)  {
            logger.error(ioe);
            throw ioe;            
        } catch (Exception e)  {
            logger.error(e);
            throw new SQLException(e.toString());
        } finally {
            try{
                if (rset != null){
                    rset.close();
                }
                if (pstmt != null){
                    pstmt.close();
                }
            } catch (SQLException se){
                logger.error(se);
                throw se;
            }
        }
        return XMLString;
     }



}