package com.ftd.osp.utilities.dataaccess.statements;

import com.ftd.osp.utilities.dataaccess.util.ResultSet2XML;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.dataaccess.valueobjects.StatementConfig;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;

/**
 * Executes the SQL query and returns the results as an XML Document
 *
 * @author Anshu Gaind
 */

public class PStmtReturnXMLDOM  extends ExecuteSQLStatement {
    private Logger logger;
    
    /**
     * The default constructor
     */
    public PStmtReturnXMLDOM(){
        logger = new Logger("com.ftd.osp.utilities.dataaccess.statements.PStmtReturnXMLDOM");
    }

    /**
     * Executes the SQL query and returns the results as an XML Document
     * 
     * @param statementConfig the statement configuration
     * @param dataRequest the data request object
     * @throws SQLException 
     * @throws ParserConfigurationException
     * @throws IOException
     * @return the xml document
     */
    public Object executeQuery(StatementConfig statementConfig, DataRequest dataRequest) 
        throws SQLException, ParserConfigurationException, IOException {
        Connection con = null;
        Document doc =  null;
        PreparedStatement pstmt = null;
        ResultSet rset = null;
        Map inputParams = dataRequest.getInputParams();

        try {
            // request connection
            con = (Connection) dataRequest.getConnection();
            
            pstmt = con.prepareStatement(statementConfig.getSQL());
            logger.debug(statementConfig.getSQL());
            
            // set input parameters
            super.setParameters(statementConfig, dataRequest, pstmt, logger);
            rset = pstmt.executeQuery();

            logger.debug("Using logger.debug utility to build the XML document");
            
            // use the ResultSet2DOM utility to obtain the XML string
            // return the xml document
            doc = ResultSet2XML.convertToXMLDOM(rset, statementConfig.getXMLFormat());

            logger.debug("SQL Query Executed Successfully");
            
       } catch (ParserConfigurationException pce)  {
            logger.error(pce);
            throw pce;
       } catch (SQLException ex)  {
            logger.error(ex);
            throw ex;
       } catch (IOException ioe)  {
            logger.error(ioe);
            throw ioe;
        } catch (Exception e)  {
            logger.error(e);
            throw new SQLException(e.toString());
       } finally {
            try{
                if (rset != null){
                    rset.close();
                }
                if (pstmt != null){
                    pstmt.close();
                }
            } catch (SQLException se){
                logger.error(se);
                throw se;
            }
        }
        return doc;
    }


}