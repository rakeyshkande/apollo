package com.ftd.osp.utilities.dataaccess.statements;

import com.ftd.osp.utilities.dataaccess.util.ResultSet2XML;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.dataaccess.valueobjects.StatementConfig;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;

/**
 * Executes the SQL query and returns the results as an XML document
 *
 * @author Anshu Gaind
 */

public class StmtReturnXMLDOM extends ExecuteSQLStatement {
    private Logger logger;

    /**
     * The default constructor
     */
    public StmtReturnXMLDOM(){
        logger = new Logger("com.ftd.osp.utilities.dataaccess.statements.StmtReturnXMLDOM");
    }

    /**
     * Executes the SQL query and returns the results as an XML document
     * 
     * @param statementConfig the statement configuration
     * @param dataRequest the data request object
     * @exception SQLException 
     * @exception ParserConfigurationException
     * @return the xml document
     */
    public Object executeQuery(StatementConfig statementConfig, DataRequest dataRequest)
      throws SQLException, ParserConfigurationException {
        Connection con = null;
        Document doc = null;
        Statement stmt = null;
        ResultSet rset = null;
        Map inputParams = dataRequest.getInputParams();
       
        try  {
            // generate the SQL string
            String sql = super.generateSQL(statementConfig, inputParams, logger);
            logger.debug(sql);

            // request connection
            con = (Connection) dataRequest.getConnection();

            logger.debug("Using ResultSet2XML utility to build the XML document");
            // use the ResultSet2DOM utility to obtain the XML string
            stmt = con.createStatement();
            rset = stmt.executeQuery(sql);
            // return the xml document
            doc = ResultSet2XML.convertToXMLDOM(rset, statementConfig.getXMLFormat());

            logger.debug("SQL Query Executed Successfully");
        } finally {
            try{
                if (rset != null){
                    rset.close();
                }
                if (stmt != null){
                    stmt.close();
                }
            } catch (SQLException se){
                logger.error(se);
                throw se;
            }
        }
        
        return doc;
     }


}