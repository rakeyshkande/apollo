package com.ftd.osp.utilities.dataaccess.statements;

import com.ftd.osp.utilities.dataaccess.util.ResultSet2XML;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.dataaccess.valueobjects.OutParameter;
import com.ftd.osp.utilities.dataaccess.valueobjects.StatementConfig;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.io.ByteArrayOutputStream;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import oracle.jdbc.OracleTypes;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;


/**
 * Executes the SQL query and returns the results as an XML string
 *
 * @author Anshu Gaind
 */

public class CStmtReturnXMLString  extends ExecuteSQLStatement {
    private Logger logger;
    
    /**
     * The default constructor
     */
    public CStmtReturnXMLString(){
        logger = new Logger("com.ftd.osp.utilities.dataaccess.statements.CStmtReturnXMLString");
    }

    /**
     * Executes the SQL query and returns the results as an XML string
     * 
     * @param statementConfig the statement configuration
     * @param dataRequest the data request object
     * @throws SQLException 
     * @throws ParserConfigurationException 
     * @throws IOException 
     * @return the results as an xml string
     */
    public Object executeQuery(StatementConfig statementConfig, DataRequest dataRequest )  
      throws SQLException, ParserConfigurationException, IOException {
        Connection con = null;
        String XMLString = null;
        CallableStatement cstmt = null;
        ResultSet rset = null;
        Map inputParams = dataRequest.getInputParams();
        HashMap outParametersMap = new HashMap();
        Document doc =  null;
        ByteArrayOutputStream baos = null;
        Map resultSets = null, xmlStrings = new HashMap();
        
        try {
            // request connection
            con = (Connection) dataRequest.getConnection();

            cstmt = con.prepareCall(statementConfig.getSQL());
            logger.debug(statementConfig.getSQL());
            // set input parameters
            super.setParameters(statementConfig, dataRequest, cstmt, logger);
            
            boolean cstmtResult = cstmt.execute();
            // extract out parameters
            {
                OutParameter outParameter = null;
                String outParameterName = null, outParameterPosition = null, outParameterType = null;
                List outParameters = statementConfig.getOutParameters();
                
                // build result set
                if (cstmtResult) {
                    setResultSet(cstmt.getResultSet(), outParametersMap);
                }

                for (Iterator iterator = outParameters.iterator(); iterator.hasNext();) {
                    outParameter = (OutParameter)iterator.next();
                    outParameterName = outParameter.getAttribute("name");
                    outParameterPosition = outParameter.getAttribute("position");
                    outParameterType = outParameter.getAttribute("type");

                    if (outParameterType.equals("oracle.jdbc.OracleTypes.CURSOR")) 
                    {
                        rset = (ResultSet)cstmt.getObject(Integer.valueOf(outParameterPosition).intValue());
                        if (resultSets == null)  
                        {
                            resultSets = new HashMap();
                        }
                        // add to the collection of result sets
                        resultSets.put(outParameterName, rset);
                    }  
                    else 
                    {
                      // add to the collection of the other out parameters
                      outParametersMap.put(outParameterName, cstmt.getString(Integer.valueOf(outParameterPosition).intValue()));
                    }
                }
                logger.debug(statementConfig.getAttribute("id")+":: Using ResultSet2XML utility to build the XML document");

                // convert result sets to xml string and add to document collection
                if (resultSets != null)  {
                    ResultSet resultSet = null;
                    String key = null;
                
                    for (Iterator iter = resultSets.keySet().iterator(); iter.hasNext() ; )  {
                      key = (String)iter.next();
                      resultSet = (ResultSet)resultSets.get(key);
                  
                      XMLString = ResultSet2XML.convertToXMLString(resultSet, statementConfig.getXMLFormat());
                      // close result set
                      if (resultSet != null)  {
                          resultSet.close();
                      }
                      // add the document as an XML string
                      xmlStrings.put(key, XMLString);
                    }
                }
            
                // convert registered Out Parameters to an xml dom tree and add to document collection
                if (outParametersMap.size() > 0)  {
                    logger.debug(statementConfig.getAttribute("id")+":: Adding registered out parameters to the document tree");
                    Document outParametersDoc = DOMUtil.getDefaultDocument();
                    Element root = outParametersDoc.createElement("out-parameters");
                    // append root to doc
                    outParametersDoc.appendChild(root);
                    
                    Element outParameter_E = null;
                    String nodeName = null;
            
                    for (Iterator iterator = outParametersMap.keySet().iterator(); iterator.hasNext(); )  {
                      nodeName = (String)iterator.next();
                      outParameter_E = outParametersDoc.createElement(nodeName);
                      outParameter_E.appendChild(outParametersDoc.createTextNode((String)outParametersMap.get(nodeName)));
                      // append to root
                      root.appendChild(outParameter_E);
                    }

                      baos = new ByteArrayOutputStream();
                      DOMUtil.print(outParametersDoc, baos);
                      XMLString = new String(baos.toByteArray());
                      
                    // add to the collection as 'out-parameters'
                    xmlStrings.put("out-parameters", XMLString);
                }
            
              // Now all output parameters have been added to the xmlStrings collection
            } // end extracting out parameters
            
            logger.debug(statementConfig.getAttribute("id")+":: SQL Query  Executed Successfully");
            
        } catch (ParserConfigurationException pce)  {
            logger.error(pce);
            throw pce;
        } catch (SQLException ex)  {
            logger.error(ex);
            throw ex;
        } catch (IOException ioe)  {
            logger.error(ioe);
            throw ioe;
        } catch (Exception e)  {
            logger.error(e);
            throw new SQLException(e.toString());
        } finally {
            try{
                if (rset != null){
                    rset.close();
                }
                if (cstmt != null){
                    cstmt.close();
                }
            } catch (SQLException se){
                logger.error(se);
                throw se;
            }
        }
        // if there were no out parameters
        if (xmlStrings == null)  {
          return null;
        }
        
       // if there are multiple out parameters then return the collection
       if (xmlStrings.size() > 1)  
       {
          return xmlStrings;
       } 
       else 
       {
          // return the only document from the collection
          String key = (String) xmlStrings.keySet().iterator().next(); 
          return (String) xmlStrings.get(key);
       }
    }

}