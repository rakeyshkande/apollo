package com.ftd.osp.utilities.dataaccess.statements;

import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.dataaccess.valueobjects.StatementConfig;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import java.util.Map;

/**
 * Executes the SQL query and returns the results as a cached result set
 *
 * @author Anshu Gaind
 */

public class StmtReturnCRSet extends ExecuteSQLStatement  {
    private Logger logger;

    /**
     * The default constructor
     */
    public StmtReturnCRSet() {
        logger = new Logger("com.ftd.osp.utilities.dataaccess.statements.StmtReturnCRSet");
    }

    /**
     * Executes the SQL query and returns the results as a cached result set
     * 
     * @param statementConfig the statement configuration
     * @param dataRequest the data request object
     * @exception SQLException 
     * @return the cached result set
     */
    public Object executeQuery(StatementConfig statementConfig, DataRequest dataRequest) 
      throws SQLException {
        Connection con = null;
        CachedResultSet crs = null;
        Statement stmt = null;
        ResultSet rset = null;
        Map inputParams = dataRequest.getInputParams();

        try {
            // generate the SQL string
            String sql = super.generateSQL(statementConfig, inputParams, logger);
            logger.debug(sql);

            // request connection
            con = (Connection) dataRequest.getConnection();

            stmt = con.createStatement();
            rset = stmt.executeQuery(sql);

            // return the cached result set
            crs = new CachedResultSet();
            crs.populate(rset);

            logger.debug("SQL Query Executed Successfully");
            
        } catch (SQLException ex)  {
            logger.error(ex);
            throw ex;
        } finally {
            try{
                if (rset != null){
                    rset.close();
                }
                if (stmt != null){
                    stmt.close();
                }
            } catch (SQLException se){
                logger.error(se);
                throw se;
            }
        }
        return crs;
     }


}