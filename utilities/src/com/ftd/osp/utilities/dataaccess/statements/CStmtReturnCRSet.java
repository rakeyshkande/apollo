package com.ftd.osp.utilities.dataaccess.statements;

import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.dataaccess.valueobjects.OutParameter;
import com.ftd.osp.utilities.dataaccess.valueobjects.StatementConfig;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


/**
 * Executes the SQL query and returns the results as a cached result set or 
 * a map of all the output parameters
 *
 * @author Anshu Gaind
 */

public class CStmtReturnCRSet  extends ExecuteSQLStatement {
    private Logger logger;

    /**
     * The default constructor
     */    
    public CStmtReturnCRSet(){
        logger = new Logger("com.ftd.osp.utilities.dataaccess.statements.CStmtReturnCRSet");
    }

    /**
     * Executes the SQL query and returns the results as a cached result set or 
     * a map of all the output parameters
     * 
     * @param statementConfig the statement configuration
     * @param dataRequest the data request object
     * @throws SQLException 
     * @throws IOException 
     * @return the cached rowset
     */
    public Object executeQuery(StatementConfig statementConfig, 
                                DataRequest dataRequest) 
      throws SQLException, IOException{
       Connection con = null;
       CachedResultSet crs = null;
       CallableStatement cstmt = null;
       ResultSet rset = null;
       Map inputParams = dataRequest.getInputParams();
       Map outParametersMap = new HashMap();
       
       try  {
            // request connection
            con = (Connection) dataRequest.getConnection();
            
            cstmt = con.prepareCall(statementConfig.getSQL());
            logger.debug(statementConfig.getSQL());
            // set input and output parameters
            super.setParameters(statementConfig, dataRequest, cstmt, logger);
            
            
            boolean cstmtResult = cstmt.execute();
           
            // extract out parameters
            {
                OutParameter outParameter = null;
                String outParameterName = null, outParameterPosition = null, outParameterType = null;
                List outParameters = statementConfig.getOutParameters();

                // build result set
                if(cstmtResult)  {
                    setResultSet(cstmt.getResultSet(), outParametersMap);
                }

                for (Iterator iterator = outParameters.iterator(); iterator.hasNext();) {
                    outParameter = (OutParameter)iterator.next();
                    outParameterName = outParameter.getAttribute("name");
                    outParameterPosition = outParameter.getAttribute("position");
                    outParameterType = outParameter.getAttribute("type");

                    if (outParameterType.equals("oracle.jdbc.OracleTypes.CURSOR") || outParameterType.equals("java.sql.Types.REF")) 
                    {
                        // return the cached result set
                        crs = new CachedResultSet();
                        rset = (ResultSet)cstmt.getObject(Integer.valueOf(outParameterPosition).intValue());
                        crs.populate(rset); 
                        // close the associated result set
                        if (rset != null){
                            rset.close();
                        }
                        outParametersMap.put(outParameterName, crs);
                    }  
                    else 
                    {
                        // add to collection
                        if (outParametersMap == null)  
                        {
                            outParametersMap = new HashMap();
                        }
                            
                        outParametersMap.put(outParameterName, cstmt.getObject(Integer.valueOf(outParameterPosition).intValue()));
                    }
                }
            }
            
            logger.debug(statementConfig.getAttribute("id")+":: SQL Query  Executed Successfully");

      } catch (SQLException ex) {
        logger.error(ex);
        throw ex;
      } catch (IOException ioe) {
        logger.error(ioe);
        throw ioe;
      } catch (Exception e) {
        logger.error(e);
        throw new SQLException(e.toString());
      } finally {
            try{
                if (rset != null){
                    rset.close();
                }
                if (cstmt != null){
                    cstmt.close();
                }
            } catch (SQLException se){
                logger.error(se);
                throw se;
            }
      }
      // if there are multiple outputs then return the collection
      if ((outParametersMap != null) )  
      {
         if (outParametersMap.size() > 1)
         {
             return outParametersMap;
         }
         else if (outParametersMap.size() == 1)
         {
           // return the only output from the collection
           String key = (String) outParametersMap.keySet().iterator().next(); 
           return  outParametersMap.get(key);            
         }
         else
         {
           return null;
         }
       } 
      else
      {
         // no out parameters
         return null;
      }
   }


}

