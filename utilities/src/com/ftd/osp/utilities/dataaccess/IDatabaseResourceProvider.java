package com.ftd.osp.utilities.dataaccess;

import java.sql.Connection;

public interface IDatabaseResourceProvider
{
    public Connection getDatabaseConnection(String datasource) 
        throws Exception;
    
}
