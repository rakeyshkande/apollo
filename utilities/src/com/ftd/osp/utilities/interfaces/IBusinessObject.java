package com.ftd.osp.utilities.interfaces;
import com.ftd.osp.utilities.vo.BusinessConfig;

public interface IBusinessObject  {
  /**
   * Called by the Message Routing framework for each order that will be processed.
   * 
   * @param businessConfig the business config object
   */
  public void execute(BusinessConfig businessConfig);
}