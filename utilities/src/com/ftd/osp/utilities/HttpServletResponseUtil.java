package com.ftd.osp.utilities;

import java.io.Writer;

import javax.servlet.http.HttpServletResponse;

import com.ftd.osp.utilities.plugins.Logger;

/**
 * Provides utility methods for working with the Servlet Response
 * Object, specifically ensuring behavior compatibility across Web Servers (OC4J Vs Tomcat)
 * where such compatibility is necessary.
 */
public class HttpServletResponseUtil
{
    private static Logger logger = new Logger(HttpServletResponseUtil.class.getName());
    
    /*
     * Formats the Status Output in the manner compatible with what the Website Expects (Based on 
     * OC4J Web Server Behavior - Oracle-Application-Server-10g/10.1.3.4.0 Oracle-HTTP-Server).
     * Ensures that the formatting is done by the applications, and not dependent on the 
     * container type/configuration. This is because the Web site is parsing the response
     * and expecting it in a specific format.
     * 
     * Note from Web Site Dev Team:
     * 
     * Website always expects HTTP response. It checks for a status code of 200 and does 
     * a regex search for "Success" in the raw HTTP response content. If this check fails, 
     * it becomes a failure case.
     */
    public static void sendError(HttpServletResponse response, int statusCode, String message) throws Exception
    {
        response.reset();
        response.setStatus(statusCode);
        
        StringBuffer sb = new StringBuffer();

        sb.append("<HTML><HEAD><TITLE>");
        
        if(statusCode == HttpServletResponse.SC_OK)
        {
            sb.append(statusCode).append(" OK");
        } else
        {
            sb.append(statusCode).append(" ").append(message);
        }
        
        sb.append("</TITLE></HEAD><BODY><H1>");
        if(statusCode == HttpServletResponse.SC_OK)
        {
            sb.append(statusCode).append(" OK</H1>").append(message);
        } else
        {
            sb.append(statusCode).append(" ").append(message).append("</H1>").append(message);
        }

        sb.append("</BODY></HTML>");
        
        try {
            try {
                response.setContentType("text/html");
                response.setCharacterEncoding("ISO-8859-1");
            } catch (Throwable t) {
                if (logger.isDebugEnabled())
                    logger.debug("status.setContentType", t);
            }
            Writer writer = response.getWriter();
            if (writer != null) {
                // If writer is null, it's an indication that the response has
                // been hard committed already, which should never happen
                writer.write(sb.toString());
            }
        } catch (Exception e) {
            logger.error("sendError Failed: " + e.getMessage(), e);
            throw e;
        } 
    }
}
