package com.ftd.osp.utilities;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.ObjectMapper;

import com.ftd.osp.ordervalidator.util.ValidationConstants;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.PaymentGatewayUtil;
import com.ftd.osp.utilities.order.vo.CreditCardsVO;
import com.ftd.osp.utilities.order.vo.PaymentsVO;
import com.ftd.osp.utilities.order.vo.PaymentDetokenizationResponse;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * This class is used to detokenize and get credit card information.
 * @author Arman Kussainov
 *
 */
public class PaymentServiceRequest {

	private static Logger logger= new Logger("com.ftd.osp.ordervalidator.PaymentServiceRequest");

	private static final int SUCCESS_RESPONSE_CODE = 200;
	private static final String DETOKENIZATION_SERVICE_URL = "PAYMENT_GATEWAY_DETOKENIZATION_URL";
    private static final String GATHERER_CONFIG_FILE = "gatherer_config.xml";

	public static CreditCardsVO getCreditCardsFromToken(String tokenId, String merchantReferenceId) throws Exception {
		String response = PaymentGatewayUtil.getCreditCardDetailsFromToken(tokenId, merchantReferenceId);
		CreditCardsVO creditCard = parseDataFromResponse(response);

		return creditCard;
	}

	/**
	 * Converts Response String into Response Object
	 * @param jsonString
	 * @return RetrieveGiftCodeResponseVO
	 */
	public static PaymentDetokenizationResponse convertToRetrieveSingleObject(String jsonString) {
		logger.info("convertToRetrieveSingleObject() started  with input: " + jsonString.replaceAll("\\d{12,19}", "XXXX"));

		PaymentDetokenizationResponse retObj = new PaymentDetokenizationResponse();
		ObjectMapper mapperObj = new ObjectMapper();
		try {
			retObj = mapperObj.readValue(jsonString, PaymentDetokenizationResponse.class);
		} catch (IOException ex) {
			logger.info("Error while parsing response : " + ex.getMessage());
		}
		return retObj;
	}

	private static CreditCardsVO parseDataFromResponse(String inputData) {
		CreditCardsVO creditCard = new CreditCardsVO();
		PaymentDetokenizationResponse paymentResponse = convertToRetrieveSingleObject(inputData);

		logger.info("paymentResponse: " + paymentResponse.toString());
        logger.info("Response cardType : " + paymentResponse.getCardType());

        try {
        	logger.info("Converting credit card");
            String creditCardType = ConfigurationUtil.getInstance().getProperty(GATHERER_CONFIG_FILE, paymentResponse.getCardType());
            if (creditCardType != null && !creditCardType.isEmpty()) {
                creditCard.setCCType(creditCardType);
                logger.info("Converted cardType : " + creditCardType);
            } else {
            	logger.info("No mapping for creditType. Setting creditType to the value from response");
            	creditCard.setCCType(paymentResponse.getCardType());
            }
        } catch (Exception ex) {
            logger.error("Error mapping the xml to the credit card value object: " + ex.toString());
            creditCard.setCCType(paymentResponse.getCardType());
        }

        creditCard.setCCNumber(paymentResponse.getCardNumber());

        logger.info("ExpirationMonth : " + paymentResponse.getExpirationMonth());
        logger.info("ExpirationYear : " + paymentResponse.getExpirationYear());

        String ccExpiration = paymentResponse.getExpirationMonth() + "/" + paymentResponse.getExpirationYear();
        logger.info("Converted ccExpiration : " + ccExpiration);
        creditCard.setCCExpiration(ccExpiration);

		return creditCard;
	}
}
