package com.ftd.osp.utilities;

import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.constant.CacheMgrConstants;
import com.ftd.osp.utilities.cacheMgr.handlers.PartnerHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.PartnerVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.security.SecurityManager;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;


public class PreferredPartnerUtility
{
  private static Logger logger = new Logger("com.ftd.osp.utilities.PreferredPartnerUtility");

  public static final String PERMISSION_YES = "Yes";
  public static final String PERMISSION_VIEW = "View";
    
  public static final String RESOURCE_PREFERRED_PARTNER_CONTEXT = "Order Proc";
  
  public static final String PREFERRED_PARTNER_CONTEXT = "PREFERRED_PARTNER";
  public static final String PREFERRED_PARTNER_ORDER_ACCESS_RESTRICTION = "ORDER_ACCESS_RESTRICTION";
  public static final String PREFERRED_PARTNER_SOURCE_CODE_RESTRICTION = "SOURCE_CODE_RESTRICTION"; 
    
  public PreferredPartnerUtility()
  {
  }


    /**
     * Returns true if passed Preferred Partner Resource (e.g., USAA) is associated with the current CSR.
     * 
     * @param securityToken 
     * @param preferredResource
     * @return boolean
     */
    public static boolean checkPreferredPermissionsForUser(String securityToken, String preferredResource) {
        try {
            return SecurityManager.getInstance().assertPermission(RESOURCE_PREFERRED_PARTNER_CONTEXT, 
                                                                   securityToken, preferredResource, PERMISSION_VIEW);
        } catch (Exception e) {
            logger.error(e);
            return false;
        }
    }

    /**
     * Returns list of Preferred Partner names (e.g., USAA) that current CSR has permissions to access
     * 
     * @param securityToken
     * @return HashSet 
     * @throws Exception
     */
    public static HashSet getPreferredPartnersForUser(String securityToken) throws Exception {
        HashSet<String> retHash = null;
        
        PartnerHandler partnerHandler = (PartnerHandler)CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_PARTNER_HANDLER);
        HashMap ppMap = (HashMap)partnerHandler.getPreferredPartner();
        Iterator ppIterator = ppMap.keySet().iterator();
        while(ppIterator.hasNext()) {
            String ppName = (String)ppIterator.next();
            PartnerVO pvo = (PartnerVO)ppMap.get(ppName);
            if (checkPreferredPermissionsForUser(securityToken, pvo.getPreferredProcessingResource())) {
                if (retHash == null) {
                    retHash = new HashSet<String>();
                }
                retHash.add(pvo.getPreferredProcessingResource());
                logger.debug("User has permission for " + ppName + " via " + pvo.getPreferredProcessingResource());  
            }
        }
        return retHash;
    }
   
     /**
      * Returns HashMap containing all preferred partners and whether or not the rep has permission
      * to access 
      * 
      * @param securityToken
      * @return HashMap 
      * @throws Exception
      */
     public static HashMap getPreferredPartnersAccessForUser(String securityToken) throws Exception {
         HashMap repPermissions = new HashMap();
         
         PartnerHandler partnerHandler = (PartnerHandler)CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_PARTNER_HANDLER);
         HashMap ppMap = (HashMap)partnerHandler.getPreferredPartner();
         Iterator ppIterator = ppMap.keySet().iterator();
         while(ppIterator.hasNext()) {
             String ppName = (String)ppIterator.next();
             PartnerVO pvo = (PartnerVO)ppMap.get(ppName);
             if (checkPreferredPermissionsForUser(securityToken, pvo.getPreferredProcessingResource()))
             {
                 repPermissions.put(pvo.getPreferredProcessingResource(), "Y");
             }
             else 
             {
                 repPermissions.put(pvo.getPreferredProcessingResource(), "N");   
             }
         }
         return repPermissions;
     }
     
     /**
     * Returns list of Preferred Partner Resource names (e.g., USAA).
     * 
     * @param securityToken
     * @return HashSet 
     * @throws Exception
     */
    public static HashSet<String> getPreferredPartnerResources() throws Exception {
        HashSet<String> retHash = new HashSet<String>();
        
        PartnerHandler partnerHandler = (PartnerHandler)CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_PARTNER_HANDLER);
        HashMap ppMap = (HashMap)partnerHandler.getPreferredPartner();
        Iterator ppIterator = ppMap.values().iterator();
        while(ppIterator.hasNext()) {
            PartnerVO pvo = (PartnerVO) ppIterator.next();
            if(pvo.getPreferredProcessingResource() != null) 
            {
              retHash.add(pvo.getPreferredProcessingResource());
            }
        }
        
        return retHash;
    }
     
     /**
     * Returns list of Preferred Partner Names (e.g., USAA).
     * 
     * @return HashSet 
     * @throws Exception
     */
     public static HashSet<String> getPreferredPartnerNames() throws Exception {
        HashSet<String> retHash = new HashSet<String>();
        
        PartnerHandler partnerHandler = (PartnerHandler)CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_PARTNER_HANDLER);
        HashMap ppMap = (HashMap)partnerHandler.getPreferredPartner();
        Iterator ppIterator = ppMap.values().iterator();
        while(ppIterator.hasNext()) {
            PartnerVO pvo = (PartnerVO) ppIterator.next();
            if(pvo.getPartnerName() != null) 
            {
              retHash.add(pvo.getPartnerName());
            }
        }
        
        return retHash;
     }
     
     /**
     * Returns list of Preferred Partner Names (e.g., USAA).
     * 
     * @return HashSet 
     * @throws Exception
     */
     public static HashMap getPreferredPartnerDisplayNames() throws Exception {
         HashMap ppNames = new HashMap();
        
        PartnerHandler partnerHandler = (PartnerHandler)CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_PARTNER_HANDLER);
        HashMap ppMap = (HashMap)partnerHandler.getPreferredPartner();
        Iterator ppIterator = ppMap.values().iterator();
        while(ppIterator.hasNext()) {
            PartnerVO pvo = (PartnerVO) ppIterator.next();
            if(pvo.getPartnerName() != null) {
              ppNames.put(pvo.getPartnerName(), pvo.getDisplayValue());
            }
        }
        
        return ppNames;
     }
     
 }

