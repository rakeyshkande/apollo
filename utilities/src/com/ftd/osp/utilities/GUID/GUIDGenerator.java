package com.ftd.osp.utilities.GUID;

import java.util.Random;
import java.util.Stack;


/**
 * 
 * @author Sasha Secivan
 * @version 1.0 
 **/ 

public class GUIDGenerator  
{
	private static GUIDGenerator GUIDGENERATOR = new GUIDGenerator();
	private static long bootTime = System.currentTimeMillis();
	private long counter = 0x00000000;
  private static Stack guidsList = new Stack();
  
  /**
   * The private Constructor
   **/	
	private GUIDGenerator()
	{
    super();
	}

		
 /**
  * The static initializer for the GUIDGenerator. It ensures that
  * at a given time there is only a single instance  
  * 
  * @return the GUIDGenerator
  **/
	public static GUIDGenerator getInstance()
	{
    return GUIDGENERATOR;
	}

	/**
   * Returns a globally unique id or GUID
   * 
   * @return comments
   */
	public String getGUID()
	{
		String guid =  null;
    // The guids will be loaded on the first request
    // or when the stack is empty
    if(this.guidsList.empty())
		{
			this.loadGUIDS();	
		}

    synchronized(this){
      guid = (String) this.guidsList.pop();
    }
				
		return guid;
	}

	
	private synchronized void loadGUIDS()
	{
    // this will allow only one thread to load guids, once the stack is empty
    if(this.guidsList.empty())
		{
        short[] rawGUID = generateGUID();
        String tmpGUID = null;
        Random rand = new Random();
				
        for(int j = 0; j < 200; j++)
        {
          tmpGUID = new String();
          tmpGUID = "FTD_GUID_";
			
          for (int i = 0; i < rawGUID.length; i++)
          {
            tmpGUID = tmpGUID + rand.nextInt();	
            tmpGUID = tmpGUID + rawGUID[i];
          }

          this.guidsList.push(tmpGUID);
        }
    }
    
	}
	
	private short[] generateGUID()
	{
		short[] data = new short[16];
		int arrayIndex = 15;
		for (int i = 0; i < 8; i++)
		{
	
			long mask = 0X00000000000000FF;	
			mask = mask << (8 * i);
			long result = bootTime & mask;
			result = result >> (8 * i );
			data[arrayIndex--] = (short)result;
		}
		
		counter++;
		arrayIndex = 7;
		
		for (int i = 0; i < 8; i++)
		{
			long mask = 0X00000000000000FF;
			mask = mask << (8 * i);
			long result = counter & mask;		
			result = result >> (8 * i );
			data[arrayIndex--] = (short)result;		
		}		
		return data;	
	} 		 	
}