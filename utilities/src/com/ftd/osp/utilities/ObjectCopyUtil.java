package com.ftd.osp.utilities;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class ObjectCopyUtil 
{
    public ObjectCopyUtil()
    {
    }

    public synchronized Object deepCopy(Object oldObj) throws Exception
    {
        ObjectOutputStream out = null;
        ObjectInputStream in = null;
        Object newobj = null;

        try
        {
            ByteArrayOutputStream buf = new ByteArrayOutputStream();
            out = new ObjectOutputStream(buf);
            out.writeObject(oldObj);
            in = new ObjectInputStream(new ByteArrayInputStream(buf.toByteArray()));
            newobj = in.readObject();
            
            return newobj;
        }
        catch(Exception e)
        {
            throw(e);
        }
        finally
        {
            if(out != null)
            {
                out.close();
            }
            
            if(in != null)
            {
                in.close();
            }  
        }
    }
}