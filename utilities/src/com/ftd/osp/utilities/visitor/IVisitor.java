package com.ftd.osp.utilities.visitor;


/**
 * Visitor interface
 */
public interface IVisitor
{
    public boolean visit(OrderEntryElementVO e) throws Exception; 
 
}
