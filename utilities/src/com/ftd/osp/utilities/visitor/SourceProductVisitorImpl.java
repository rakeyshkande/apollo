package com.ftd.osp.utilities.visitor;

import com.ftd.osp.utilities.SourceProductUtility;


/**
 * Visitor implementation for source code product filtering check.
 */
public class SourceProductVisitorImpl implements IVisitor
{
    public boolean visit(OrderEntryElementVO e)  throws Exception {
        SourceProductUtility sourceProductUtil = new SourceProductUtility();
    
        
        String restrictionMsg = null;
        
        try {
            restrictionMsg = sourceProductUtil.getProductRestrictionMsg
                                (e.getConnection(),
                                 e.getSourceCode(),
                                 e.getProductId(),
                                 e.isSearchById(),
                                 e.getDeliveryType()
                                );
        } catch (Exception ex) {
            throw ex;
        }
        return restrictionMsg == null;

        }

} 
 

