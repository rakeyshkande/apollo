package com.ftd.osp.utilities.visitor;

import java.sql.Connection;


/**
 * Element/Visitable interface
 */
 public class OrderEntryElementVO implements IApplicationElement {
 
    String sourceCode;
    String productId;
    boolean searchById;
    String deliveryType;
    Connection connection;
    
    public boolean accept(IVisitor visitor) throws Exception {
        return visitor.visit(this);
    }

    public void setSourceCode(String sourceCode) {
        this.sourceCode = sourceCode;
    }

    public String getSourceCode() {
        return sourceCode;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductId() {
        return productId;
    }

    public void setSearchById(boolean searchById) {
        this.searchById = searchById;
    }

    public boolean isSearchById() {
        return searchById;
    }

    public void setDeliveryType(String deliveryType) {
        this.deliveryType = deliveryType;
    }

    public String getDeliveryType() {
        return deliveryType;
    }


    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    public Connection getConnection() {
        return connection;
    }
}

