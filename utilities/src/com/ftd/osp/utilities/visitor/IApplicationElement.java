package com.ftd.osp.utilities.visitor;


/**
 * Element/Visitable interface
 */
public interface IApplicationElement
{
    public boolean accept(IVisitor visitor) throws Exception; 
 
}
