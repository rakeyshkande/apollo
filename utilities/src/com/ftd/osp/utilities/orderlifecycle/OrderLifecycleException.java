/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ftd.osp.utilities.orderlifecycle;

/**
 *
 * @author tpeterson
 */
public class OrderLifecycleException extends Exception {

    /**
     *
     * @param message
     */
    public OrderLifecycleException(String message) {
        super(message);
    }

    /**
     *
     * @param e
     */
    public OrderLifecycleException(Exception e) {
        super(e);
    }

    /**
     * 
     * @param message
     * @param e
     */
    public OrderLifecycleException(String message, Exception e) {
        super(message,e);
    }
}
