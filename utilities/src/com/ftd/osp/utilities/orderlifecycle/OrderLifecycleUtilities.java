package com.ftd.osp.utilities.orderlifecycle;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.orderlifecycle.generated.OrderStatusStub;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.JAXPUtil;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * This class is a convenience wrapper around specific calls to
 * com.ftd.osp.utilities.orderlifecycle.generated.OrderLifecycleUtilities
 * @author tpeterson
 */
public class OrderLifecycleUtilities {

    private static final Matcher REGEX_MATCHER =  Pattern.compile("\\A[A-Z]\\d{4}[A-Z]-\\d{4}\\z").matcher("");
    private static final String CONFIG_CONTEXT = "ORDER_LIFECYCLE";
    private static final String MD_ACCOUNT_ID = "MD_ACCOUNT_ID";
    private static final String MD_USER_ID = "MD_USER_ID";
    private static final String MD_PASSWORD_ID = "MD_PASSWORD_ID";
    private static final String WS_URL = "WS_URL";
    private static Logger logger = new Logger("com.ftd.osp.utilities.orderlifecycle.OrderLifecycleUtilities");

    /**
     * Retrieves a list of unconfirmed status for a specific sending member code
     * @param sendingMemberCode - member code to check
     * @return a list of unconfirmed status for a specific sending member code
     * @throws com.ftd.osp.utilities.orderlifecycle.OrderLifecycleException
     */
    public static List<OrderStatus> GetNewOrderStatusList(String sendingMemberCode)
            throws OrderLifecycleException {
        List list = new ArrayList();

        try {
            ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
            String mnapiURL = configUtil.getFrpGlobalParm(CONFIG_CONTEXT, WS_URL);
            String mdAccountNumber = configUtil.getSecureProperty(CONFIG_CONTEXT, MD_ACCOUNT_ID);
            String mdUserId = configUtil.getSecureProperty(CONFIG_CONTEXT, MD_USER_ID);
            String mdPassword = configUtil.getSecureProperty(CONFIG_CONTEXT, MD_PASSWORD_ID);

            list = GetNewOrderStatusList(mnapiURL,mdAccountNumber,mdUserId,mdPassword,sendingMemberCode);
        } catch (OrderLifecycleException ole) {
            throw ole;
        } catch (Exception ex) {
            logger.error(ex);
        }

        return list;
    }

    /**
     * Retrieves a list of unconfirmed status for a specific sending member code
     * @param mnapiURL - URL of the web service (originates from global config)
     * @param mdAccountNumber - Mercury direct account number
     * @param mdUserId - Mercury direct user id
     * @param mdPassword - Mercury direct password
     * @param sendingMemberCode - member code to check
     * @return a list of unconfirmed status for a specific sending member code
     * @throws com.ftd.osp.utilities.orderlifecycle.OrderLifecycleException
     */
    public static List<OrderStatus> GetNewOrderStatusList(String mnapiURL, String mdAccountNumber, String mdUserId, String mdPassword, String sendingMemberCode)
            throws OrderLifecycleException {
        List<OrderStatus> list = new ArrayList<OrderStatus>();

        try {
            OrderStatusStub stub = new OrderStatusStub(mnapiURL);
            OrderStatusStub.MNAPIOrderStatusGetNewListRequest req = new OrderStatusStub.MNAPIOrderStatusGetNewListRequest();
            req.setSendingMemberCode(sendingMemberCode);
            OrderStatusStub.GetNewOrderStatusList s = new OrderStatusStub.GetNewOrderStatusList();
            s.setRequest(req);

            OrderStatusStub.MNAPIOrderStatusHeaderE header = new OrderStatusStub.MNAPIOrderStatusHeaderE();
            OrderStatusStub.MNAPIOrderStatusHeader h = new OrderStatusStub.MNAPIOrderStatusHeader();
            h.setAccountID(mdAccountNumber);
            h.setUserID(mdUserId);
            h.setPassword(mdPassword);
            header.setMNAPIOrderStatusHeader(h);

            OrderStatusStub.GetNewOrderStatusListResponse res = stub.getNewOrderStatusList(s, header);
            int responseCode = res.getGetNewOrderStatusListResult().getResponseCode();
            String responseText = res.getGetNewOrderStatusListResult().getResponseText();
            if (responseText != null) {
                logger.info("responseText : " + responseText);
            }

            if( responseCode==0 ) {
            	Document xmlDoc = JAXPUtil.parseDocument(responseText);
        	    NodeList nl = DOMUtil.selectNodes(xmlDoc, "//MNAPIOrderStatusDS/statusTable");

        	    for (int i = 0; i < nl.getLength(); i++) {
                    Element itemElement = (Element)nl.item(i);
                    NodeList nlOrder = itemElement.getChildNodes();
                    OrderStatus orderStatus = new OrderStatus();
                    for (int k=0; k<nlOrder.getLength(); k++) {
                    	Node rec = nlOrder.item(k);
                        String nodeName = rec.getNodeName();
                        String value = "";
                        if (rec.hasChildNodes()) {
                            Node n = rec.getFirstChild();
                            value = n.getNodeValue();
                	    	logger.info(nodeName + " " + value);
                	    	if (nodeName != null && value != null) {
                	    		if (nodeName.equalsIgnoreCase("id")) {
                	    			orderStatus.setStatusId(Integer.parseInt(value));
                	    		} else if (nodeName.equalsIgnoreCase("status_code")) {
                	    			orderStatus.setStatusCode(value);
                	    		} else if (nodeName.equalsIgnoreCase("status_date")) {
                	    			orderStatus.setStatusDate(value);
                	    		} else if (nodeName.equalsIgnoreCase("status_text")) {
                	    			orderStatus.setStatusText(value);
                	    		} else if (nodeName.equalsIgnoreCase("confirmed")) {
                	    			orderStatus.setConfirmed(value.equalsIgnoreCase("true"));
                	    		} else if (nodeName.equalsIgnoreCase("service_message_number")) {
                	    			orderStatus.setEfosNumber(value);
                	    		}
                	    	}
                        }
                    }
                    list.add(orderStatus);
        	    }
            } else {
                String errMsg = "Error code "+responseCode+" received from mnapiws.\r\n"+responseText;
                throw new OrderLifecycleException(errMsg);
            }
        } catch (OrderLifecycleException ole) {
            throw ole;
        } catch (Exception ex) {
            throw new OrderLifecycleException(ex);
        }

        return list;
    }

    /**
     * Retrieve a list of status for a specific efos number
     * @param sendingMemberCode - member code to check
     * @param efosNumber - efos number being inquired (with or without the dash and four digit suffix)
     * @param getAll - true to retrieve all status or false to just retrieve unconfirmed messages
     * @return a list of status for a specific efos number
     * @throws com.ftd.osp.utilities.orderlifecycle.OrderLifecycleException
     */
    public static List<OrderStatus> GetOrderStatus(String sendingMemberCode, String efosNumber, boolean getAll)
            throws OrderLifecycleException  {
        List<OrderStatus> list = new ArrayList<OrderStatus>();

        try {
            ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
            String mnapiURL = configUtil.getFrpGlobalParm(CONFIG_CONTEXT, WS_URL);
            String mdAccountNumber = configUtil.getSecureProperty(CONFIG_CONTEXT, MD_ACCOUNT_ID);
            String mdUserId = configUtil.getSecureProperty(CONFIG_CONTEXT, MD_USER_ID);
            String mdPassword = configUtil.getSecureProperty(CONFIG_CONTEXT, MD_PASSWORD_ID);

            list = GetOrderStatus(mnapiURL,mdAccountNumber,mdUserId,mdPassword,sendingMemberCode,efosNumber,getAll);
        } catch (OrderLifecycleException ole) {
            throw ole;
        } catch (Exception ex) {
            logger.error(ex);
        }

        return list;
    }

    /**
     * Retrieve a list of status for a specific efos number
     * @param mnapiURL - URL of the web service (originates from global config)
     * @param mdAccountNumber - Mercury direct account number
     * @param mdUserId - Mercury direct user id
     * @param mdPassword - Mercury direct password
     * @param sendingMemberCode - member code to check
     * @param efosNumber - efos number being inquired (with or without the dash and four digit suffix)
     * @param getAll - true to retrieve all status or false to just retrieve unconfirmed messages
     * @return a list of status for a specific efos number
     * @throws com.ftd.osp.utilities.orderlifecycle.OrderLifecycleException
     */
    public static List<OrderStatus> GetOrderStatus(String mnapiURL, String mdAccountNumber, String mdUserId, String mdPassword, String sendingMemberCode, String efosNumber, boolean getAll)
            throws OrderLifecycleException  {
        List<OrderStatus> list = new ArrayList<OrderStatus>();
        ByteArrayInputStream is = null;

        try {
            REGEX_MATCHER.reset(efosNumber);
            if( REGEX_MATCHER.matches() ) {
                efosNumber = efosNumber.substring(0, 6);
            }
            OrderStatusStub stub = new OrderStatusStub(mnapiURL);
            OrderStatusStub.MNAPIOrderStatusGetRequest req = new OrderStatusStub.MNAPIOrderStatusGetRequest();
            req.setSendingMemberCode(sendingMemberCode);
            req.setServiceMsgNum(efosNumber);
            req.setStatusType(getAll?OrderStatusStub.MNAPIOrderStatusType.All:OrderStatusStub.MNAPIOrderStatusType.New);
            OrderStatusStub.GetOrderStatus s = new OrderStatusStub.GetOrderStatus();
            s.setRequest(req);

            OrderStatusStub.MNAPIOrderStatusHeaderE header = new OrderStatusStub.MNAPIOrderStatusHeaderE();
            OrderStatusStub.MNAPIOrderStatusHeader h = new OrderStatusStub.MNAPIOrderStatusHeader();
            h.setAccountID(mdAccountNumber);
            h.setUserID(mdUserId);
            h.setPassword(mdPassword);
            header.setMNAPIOrderStatusHeader(h);

            OrderStatusStub.GetOrderStatusResponse res = stub.getOrderStatus(s, header);
            int responseCode = res.getGetOrderStatusResult().getResponseCode();
            String responseText = res.getGetOrderStatusResult().getResponseText();

            if( responseCode==0 ) {
                is = new ByteArrayInputStream(responseText.getBytes());
                SAXParserFactory factory = SAXParserFactory.newInstance();
                factory.setNamespaceAware(false);
                factory.setValidating(false);
                SAXParser saxParser = factory.newSAXParser();
                OrderStatusHandler oHandler = new OrderStatusHandler();
                saxParser.parse(is,oHandler);

                list = oHandler.getStatusList();
            } else {
                String errMsg = "Error code "+responseCode+" received from mnapiws.\r\n"+responseText;
                throw new OrderLifecycleException(errMsg);
            }
        } catch (OrderLifecycleException ole) {
            throw ole;
        } catch (Exception ex) {
            throw new OrderLifecycleException(ex);
        } finally {
            if( is!=null ) {
                try {
                    is.close();
                } catch (IOException ioe) {
                    //Eat the exception
                }
            }
        }

        return list;
    }

    /**
     * Inform the mnapiws that the specific status record has been received.
     * @param statusId
     * @throws com.ftd.osp.utilities.orderlifecycle.OrderLifecycleException
     */
    public static void ConfirmOrderStatus(int statusId)
            throws OrderLifecycleException {

        try {
            ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
            String mnapiURL = configUtil.getFrpGlobalParm(CONFIG_CONTEXT, WS_URL);
            String mdAccountNumber = configUtil.getSecureProperty(CONFIG_CONTEXT, MD_ACCOUNT_ID);
            String mdUserId = configUtil.getSecureProperty(CONFIG_CONTEXT, MD_USER_ID);
            String mdPassword = configUtil.getSecureProperty(CONFIG_CONTEXT, MD_PASSWORD_ID);

            ConfirmOrderStatus(mnapiURL,mdAccountNumber,mdUserId,mdPassword,statusId);
        } catch (OrderLifecycleException ole) {
            throw ole;
        } catch (Exception ex) {
            logger.error(ex);
        }
    }

    /**
     * Inform the mnapiws that the specific status record has been received.
     * @param mnapiURL - URL of the web service (originates from global config)
     * @param mdAccountNumber - Mercury direct account number
     * @param mdUserId - Mercury direct user id
     * @param mdPassword - Mercury direct password
     * @param statusId - status id to be confirmed
     * @throws com.ftd.osp.utilities.orderlifecycle.OrderLifecycleException
     */
    public static void ConfirmOrderStatus(String mnapiURL, String mdAccountNumber, String mdUserId, String mdPassword, int statusId)
            throws OrderLifecycleException {

        try {
            OrderStatusStub stub = new OrderStatusStub(mnapiURL);
            OrderStatusStub.MNAPIOrderStatusConfirmRequest req = new OrderStatusStub.MNAPIOrderStatusConfirmRequest();
            req.setStatusType(OrderStatusStub.MNAPIOrderStatusType.Confimred);
            req.setOrderStatusID(statusId);
            OrderStatusStub.ConfirmOrderStatus s = new OrderStatusStub.ConfirmOrderStatus();
            s.setRequest(req);

            OrderStatusStub.MNAPIOrderStatusHeaderE header = new OrderStatusStub.MNAPIOrderStatusHeaderE();
            OrderStatusStub.MNAPIOrderStatusHeader h = new OrderStatusStub.MNAPIOrderStatusHeader();
            h.setAccountID(mdAccountNumber);
            h.setUserID(mdUserId);
            h.setPassword(mdPassword);
            header.setMNAPIOrderStatusHeader(h);

            OrderStatusStub.ConfirmOrderStatusResponse res = stub.confirmOrderStatus(s, header);
            int responseCode = res.getConfirmOrderStatusResult().getResponseCode();
            String responseText = res.getConfirmOrderStatusResult().getResponseText();

            if( responseCode!=0 ) {
                String errMsg = "Error code "+responseCode+" received from mnapiws.\r\n"+responseText;
                throw new OrderLifecycleException(errMsg);
            }

        } catch (OrderLifecycleException ole) {
            throw ole;
        } catch (Exception ex) {
            throw new OrderLifecycleException(ex);
        } 
    }
}
