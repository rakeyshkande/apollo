/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ftd.osp.utilities.orderlifecycle;

import java.util.ArrayList;
import java.util.List;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author tpeterson
 */
public class OrderStatusHandler extends DefaultHandler {

    private List<OrderStatus> statusList = new ArrayList<OrderStatus>();
    private OrderStatus targetStatus;
    private final String ROOT = "MNAPIOrderStatusDS";
    private final String RECORD = "statusTable";
    private final String ID = "ID";
    private final String CODE = "status_code";
    private final String DATE = "status_date";
    private final String TEXT = "status_text";
    private final String CONFIRMED = "confirmed";
    private final String EFOS_NUMBER = "service_message_number";
    private String currentElement;

    @Override
    public void startElement(String namespaceURI, String localName, String qName, Attributes atts)
            throws SAXException {

        if (ROOT.equals(qName)) {
            statusList = new ArrayList<OrderStatus>();
        } else if (RECORD.equals(qName)) {
            targetStatus = new OrderStatus();
        }

        currentElement = qName;
    }

    @Override
    public void characters(char[] ch, int start, int length)
            throws SAXException {

        String strValue = new String(ch, start, length).trim();

        if( strValue.length()==0 )
            return;
        
        if (ID.equals(currentElement)) {
            targetStatus.setStatusId(Integer.parseInt(strValue));
        } else if (CODE.equalsIgnoreCase(currentElement)) {
            targetStatus.setStatusCode(strValue);
        } else if (DATE.equalsIgnoreCase(currentElement)) {
            targetStatus.setStatusDate(strValue);
        } else if (TEXT.equalsIgnoreCase(currentElement)) {
            targetStatus.setStatusText(strValue);
        } else if (CONFIRMED.equalsIgnoreCase(currentElement)) {
            targetStatus.setConfirmed(strValue.equalsIgnoreCase("true"));
        } else if (EFOS_NUMBER.equalsIgnoreCase(currentElement)) {
            targetStatus.setEfosNumber(strValue);
        }
    }

    @Override
    public void endElement(String namespaceURI, String localName, String qName) {
        if (RECORD.equals(qName)) {
            getStatusList().add(targetStatus);
        }
    }

    /**
     * @return the statusList
     */
    public List<OrderStatus> getStatusList() {
        return statusList;
    }
}
