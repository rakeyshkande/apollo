/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ftd.osp.utilities.orderlifecycle.test;

import com.ftd.osp.utilities.orderlifecycle.*;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author tpeterson
 */
public class OrderLifecycleUtilitiesTest {

    //Run SQL to unconfirm messages
    //jdbc:jtds:sqlserver://mtdevweb2.FTDI.COM:1433;DatabaseName=md2r1db
    //update dbo.ORDER_STATUS SET CONFIRMED=0 WHERE SENDING_MEMBER_CODE = '90-0134AQ'
    //update dbo.ORDER_STATUS SET CONFIRMED=0 WHERE SENDING_MEMBER_CODE = '90-8400AA'
    
    private String testURL = "http://mtdevweb1.ftdi.com:8093/orderstatus.asmx";
    private String testMnapiAcct = "10037";
    private String testMnapiUserId = "tpeterson";
    private String testMnapiPassword = "hellothere";
    private String testMemberCode = "90-0134AQ";
    private String testEfosNumber = "P6515P";
    private String testLongEfosNumber = "P6515P-1234";
    /*
    private String testMnapiAcct = "27";
    private String testMnapiUserId = "c0mstatus";
    private String testMnapiPassword = "passw0rd";
    private String testMemberCode = "90-8400AA";
    private String testEfosNumber = "N3405R";
    private String testLongEfosNumber = "N3405R-1234";
    */
    
    private static final ArrayList<Integer> confirmationArray = new ArrayList<Integer>();
    private static final String testSeperator = "****************************************************************************************************";

    public OrderLifecycleUtilitiesTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
        System.out.println(testSeperator);
    }

    @After
    public void tearDown() {
        System.out.println();
    }

    /**
     * Test of GetNewOrderStatusList method, of class OrderLifecycleUtilities.
     */
    @Test
    public void testGetNewOrderStatusListSuccess() throws Exception {
        System.out.println("GetNewOrderStatusList");
        String mnapiURL = testURL;
        String mdAccountNumber = testMnapiAcct;
        String mdUserId = testMnapiUserId;
        String mdPassword = testMnapiPassword;
        String sendingMemberCode = testMemberCode;

        List<OrderStatus> result = OrderLifecycleUtilities.GetNewOrderStatusList(mnapiURL, mdAccountNumber, mdUserId, mdPassword, sendingMemberCode);
        assertNotNull(result);
        System.out.println("Results returned: "+result.size());

        for (OrderStatus s : result) {
            confirmationArray.add(s.getStatusId());
            System.out.println("Status Id: " + s.getStatusId());
            System.out.println("Status Date: " + s.getStatusDate());
            System.out.println("Status Code: " + s.getStatusCode());
            System.out.println("Status Text: " + s.getStatusText());
            System.out.println("Efos Number: " + s.getEfosNumber());
            System.out.println("Confirmed: " + s.isConfirmed());
            System.out.println();
        }
    }

    /**
     * Test of GetOrderStatus method, of class OrderLifecycleUtilities.
     */
    @Test
    public void testGetOrderStatusSuccess() throws Exception {
        System.out.println("GetOrderStatus");
        String mnapiURL = testURL;
        String mdAccountNumber = testMnapiAcct;
        String mdUserId = testMnapiUserId;
        String mdPassword = testMnapiPassword;
        String sendingMemberCode = testMemberCode;
        String efosNumber = testEfosNumber;
        boolean getAll = false;
        List<OrderStatus> result = OrderLifecycleUtilities.GetOrderStatus(mnapiURL, mdAccountNumber, mdUserId, mdPassword, sendingMemberCode, efosNumber, getAll);
        assertNotNull(result);
        System.out.println("Results returned: "+result.size());

        for (OrderStatus s : result) {
            confirmationArray.add(s.getStatusId());
            System.out.println("Status Id: " + s.getStatusId());
            System.out.println("Status Date: " + s.getStatusDate());
            System.out.println("Status Code: " + s.getStatusCode());
            System.out.println("Status Text: " + s.getStatusText());
            System.out.println("Efos Number: " + s.getEfosNumber());
            System.out.println("Confirmed: " + s.isConfirmed());
            System.out.println();
        }
    }

    /**
     * Test of ConfirmOrderStatus method, of class OrderLifecycleUtilities.
     */
    @Test
    public void testConfirmOrderStatusSuccess() throws Exception {
        System.out.println("ConfirmOrderStatus");
        String mnapiURL = testURL;
        String mdAccountNumber = testMnapiAcct;
        String mdUserId = testMnapiUserId;
        String mdPassword = testMnapiPassword;
        //int statusId = 0;

        System.out.println(confirmationArray.size()+" status records to confirm.");
        System.out.println();
        for( Integer s : confirmationArray) {
            System.out.println("Confirming status id: "+s);
            OrderLifecycleUtilities.ConfirmOrderStatus(mnapiURL, mdAccountNumber, mdUserId, mdPassword, s);
        }
    }

    /**
     * Test of ConfirmOrderStatus method, of class OrderLifecycleUtilities.
     */
    @Test
    public void testConfirmOrderStatusFail() throws Exception {
        System.out.println("ConfirmOrderStatusFail");
        String mnapiURL = testURL;
        String mdAccountNumber = testMnapiAcct;
        String mdUserId = testMnapiUserId;
        String mdPassword = testMnapiPassword;
        int statusId = -9999;

        OrderLifecycleUtilities.ConfirmOrderStatus(mnapiURL, mdAccountNumber, mdUserId, mdPassword, statusId);
    }

    /**
     * Test of GetOrderStatus method, of class OrderLifecycleUtilities.
     */
    @Test
    public void testGetOrderStatusAllSuccess() throws Exception {
        System.out.println("GetOrderStatusAll");
        String mnapiURL = testURL;
        String mdAccountNumber = testMnapiAcct;
        String mdUserId = testMnapiUserId;
        String mdPassword = testMnapiPassword;
        String sendingMemberCode = testMemberCode;
        String efosNumber = testEfosNumber;
        boolean getAll = true;
        List<OrderStatus> result = OrderLifecycleUtilities.GetOrderStatus(mnapiURL, mdAccountNumber, mdUserId, mdPassword, sendingMemberCode, efosNumber, getAll);
        System.out.println("Results returned: "+result.size());
        assertNotNull(result);
        assertTrue(result.size()>0);
    }

    /**
     * Test of GetOrderStatus method, of class OrderLifecycleUtilities.
     */
    @Test
    public void testGetOrderStatusAllLongSuccess() throws Exception {
        System.out.println("GetOrderStatusAll");
        String mnapiURL = testURL;
        String mdAccountNumber = testMnapiAcct;
        String mdUserId = testMnapiUserId;
        String mdPassword = testMnapiPassword;
        String sendingMemberCode = testMemberCode;
        String efosNumber = testLongEfosNumber;
        boolean getAll = true;
        List<OrderStatus> result = OrderLifecycleUtilities.GetOrderStatus(mnapiURL, mdAccountNumber, mdUserId, mdPassword, sendingMemberCode, efosNumber, getAll);
        System.out.println("Results returned: "+result.size());
        assertNotNull(result);
        assertTrue(result.size()>0);
    }

    /**
     * Test of GetNewOrderStatusList method, of class OrderLifecycleUtilities.
     */
    @Test
    public void testGetNewOrderStatusListNoneAvailable() throws Exception {
        System.out.println("GetNewOrderStatusNoneAvailable");
        String mnapiURL = testURL;
        String mdAccountNumber = testMnapiAcct;
        String mdUserId = testMnapiUserId;
        String mdPassword = testMnapiPassword;
        String sendingMemberCode = testMemberCode;
        List<OrderStatus> result = OrderLifecycleUtilities.GetNewOrderStatusList(mnapiURL, mdAccountNumber, mdUserId, mdPassword, sendingMemberCode);
        assertNotNull(result);
        System.out.println("Results returned: "+result.size());
        assertEquals(0, result.size());
    }

    /**
     * Test of GetOrderStatus method, of class OrderLifecycleUtilities.
     */
    @Test
    public void testGetOrderStatusNoneAvailable() throws Exception {
        System.out.println("GetOrderStatusNoneAvailable");
        String mnapiURL = testURL;
        String mdAccountNumber = testMnapiAcct;
        String mdUserId = testMnapiUserId;
        String mdPassword = testMnapiPassword;
        String sendingMemberCode = testMemberCode;
        String efosNumber = testEfosNumber;
        boolean getAll = false;
        List<OrderStatus> result = OrderLifecycleUtilities.GetOrderStatus(mnapiURL, mdAccountNumber, mdUserId, mdPassword, sendingMemberCode, efosNumber, getAll);
        assertNotNull(result);
        System.out.println("Results returned: "+result.size());
        assertEquals(0, result.size());
    }

    /**
     * Test of ConfirmOrderStatus method, of class OrderLifecycleUtilities.
     */
    @Test
    public void testBadCredentials() throws Exception {
        System.out.println("Bad credentials");
        String mnapiURL = testURL;
        String mdAccountNumber = testMnapiAcct;
        String mdUserId = testMnapiUserId;
        String mdPassword = "hellothere";
        int statusId = 0;

        try {
            OrderLifecycleUtilities.ConfirmOrderStatus(mnapiURL, mdAccountNumber, mdUserId, mdPassword, statusId);
            fail("An exception should have been thrown!");
        } catch (OrderLifecycleException e) {

        }
    }

    /**
     * Test of ConfirmOrderStatus method, of class OrderLifecycleUtilities.
     */
    @Test
    public void testBadUrl() throws Exception {
        System.out.println("Bad URL");
        String mnapiURL = "http://google.com";
        String mdAccountNumber = testMnapiAcct;
        String mdUserId = testMnapiUserId;
        String mdPassword = testMnapiPassword;
        int statusId = 0;

        try {
            OrderLifecycleUtilities.ConfirmOrderStatus(mnapiURL, mdAccountNumber, mdUserId, mdPassword, statusId);
            fail("An exception should have been thrown!");
        } catch (OrderLifecycleException e) {
            System.out.println("Exception caught...test passed!");
        }
    }

    /**
     * Test of ConfirmOrderStatus method, of class OrderLifecycleUtilities.
     */
    @Test
    public void testProductionUrl() throws Exception {
        System.out.println("Production Url (Should fail because of bad credentials)");
        String mnapiURL = "http://md2.mercurynetwork.com/ws/orderstatus.asmx";
        String mdAccountNumber = testMnapiAcct;
        String mdUserId = testMnapiUserId;
        String mdPassword = "hellothere";
        int statusId = 0;

        try {
            OrderLifecycleUtilities.ConfirmOrderStatus(mnapiURL, mdAccountNumber, mdUserId, mdPassword, statusId);
            fail("An exception should have been thrown because of invalid credentials!");
        } catch (OrderLifecycleException e) {
            e.printStackTrace(System.out);
            System.out.println("Exception caught...test passed!");
        }
    }
}