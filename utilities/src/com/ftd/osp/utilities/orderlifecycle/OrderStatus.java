package com.ftd.osp.utilities.orderlifecycle;

import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.JAXPUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

import javax.xml.parsers.DocumentBuilder;

import org.w3c.dom.Document;
import org.w3c.dom.Element;


/**
 *
 * @author tpeterson
 */
public class OrderStatus {
    private Integer statusId;
    private String statusCode;
    private String statusDate;
    private String statusText;
    private Boolean confirmed;
    private String efosNumber;
    private String statusUrl;
    
    /**
     * Default constructor
     */
    public OrderStatus() {
        statusText = "";
    }

    /**
     * @return the statusId
     */
    public Integer getStatusId() {
        return statusId;
    }

    /**
     * @param statusId the statusId to set
     */
    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }

    /**
     * @return the statusCode
     */
    public String getStatusCode() {
        return statusCode;
    }

    /**
     * @param statusCode the statusCode to set
     */
    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    /**
     * @return the statusDate
     */
    public String getStatusDate() {
        return statusDate;
    }

    /**
     * @param statusDate the statusDate to set
     */
    public void setStatusDate(String statusDate) {
        this.statusDate = statusDate;
    }

    /**
     * @return the statusText
     */
    public String getStatusText() {
        return statusText;
    }

    /**
     * @param statusText the statusText to set
     */
    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    /**
     * @return the confirmed
     */
    public Boolean isConfirmed() {
        return confirmed;
    }

    /**
     * @param confirmed the confirmed to set
     */
    public void setConfirmed(Boolean confirmed) {
        this.confirmed = confirmed;
    }

    /**
     * @return the efosNumber
     */
    public String getEfosNumber() {
        return efosNumber;
    }

    /**
     * @param efosNumber the efosNumber to set
     */
    public void setEfosNumber(String efosNumber) {
        this.efosNumber = efosNumber;
    }
    
    /**
    * Returns an XML representation of the OrderStatus.
    * The XML tagname is 'orderstatus'. The attributes of the tag are the non-null field values
    * where the attribute names are the same as the field names.
    * 
    * @return String String representing the Order Status in XML
    * @throws java.lang.RuntimeException on any XML Generation Errors
    */
    public String toXML() {
      try {  
        DocumentBuilder documentBuilder = DOMUtil.getDocumentBuilder();
        return toXML(documentBuilder) ;
      } catch (Exception e) {
        throw new RuntimeException(e);
      }
    }
  
    /**
    * Returns an XML representation of the OrderStatus using the passed in DocumentBuilder.
    * The XML tagname is 'orderstatus'. The attributes of the tag are the non-null field values
    * where the attribute names are the same as the field names.
    * 
    *  
    *  This method is more efficient when converting a number of OrderStatus objects, as it 
    *  allows the DocumentBuilder to be passed in/Instantiated once.
    *  To obtain a DocumentBuilder instance, see {@link com.ftd.osp.utilities.xml.DOMUtil#getDocumentBuilder()}
    * 
    * @param documentBuilder the DOM Document Builder for creating a new Document
    * @return String String representing the Order Status in XML
    * @throws java.lang.RuntimeException on any XML Generation Errors
    */
    public String toXML(DocumentBuilder documentBuilder) {
      try {
        Document xmlDoc = documentBuilder.newDocument();
        Element osElement = xmlDoc.createElement("orderstatus");
        
        xmlDoc.appendChild(osElement);
        
        Field[] fields = OrderStatus.class.getDeclaredFields();
        
        for(int iLoop = 0; iLoop < fields.length; iLoop ++ ) {
          Field field = fields[iLoop];
          
          int modifiers = field.getModifiers();
          if(Modifier.isStatic(modifiers) || Modifier.isTransient(modifiers) || Modifier.isFinal(modifiers)) {
            continue;
          }
          
          Object value = field.get(this);
          if(value == null) {
            continue;
          }
         
          osElement.setAttribute(field.getName(), value.toString());
        }
                      
        return JAXPUtil.toStringNoFormat(xmlDoc);
      }
      catch (Exception e)
      {
        // Wrap the exception and throw it.
        throw new RuntimeException(e);
      }    
    }

    /**
    * Returns an OrderStatus initialized from XML.
    * Expects a single XML element with tagname 'orderstatus'. The attributes of the tag are mapped to the fields
    * where the attribute names are the same as the field names.
    * 
    * 
    * @param xml XML representation of the Order Status.
    * @return OrderStatus 
    * @throws java.lang.IllegalArgumentException If the xml is invalid
    */
    public static OrderStatus fromXML(String xml)  {
      try {
        Document xmlDoc = JAXPUtil.parseDocument(xml);
        Element osElement = xmlDoc.getDocumentElement();
        
        if(!"orderstatus".equalsIgnoreCase(osElement.getTagName())){
          throw new IllegalArgumentException("Expected tag=orderstatus, found: " + osElement.getTagName());
        }
        
        OrderStatus retVal = new OrderStatus();
        
        Field[] fields = OrderStatus.class.getDeclaredFields();
        
        for(int iLoop = 0; iLoop < fields.length; iLoop ++ ) {
          Field field = fields[iLoop];
          
          int modifiers = field.getModifiers();
          if(Modifier.isStatic(modifiers) || Modifier.isTransient(modifiers) || Modifier.isFinal(modifiers)) {
            continue;
          }
          
          if(!osElement.hasAttribute(field.getName())) {
            continue;
          }
          
          String value = osElement.getAttribute(field.getName());
          if(value == null) {
            continue;
          }
          
          if(field.getType() == String.class) {
            field.set(retVal, value);
          } else if (field.getType() == Integer.class) {
            field.set(retVal, new Integer(value));
          } else if (field.getType() == Boolean.class) {
            field.set(retVal, new Boolean(value));
          } else {
            throw new RuntimeException("Unsupported Field Type: " + field.getName() + "; " + field.getType());
          }
          
        }
                
        return retVal;        
      } catch (Exception e) {
        throw new IllegalArgumentException("Invalid xml: " + xml, e);
      }
    }

	public String getStatusUrl() {
		return statusUrl;
	}

	public void setStatusUrl(String statusUrl) {
		this.statusUrl = statusUrl;
	}
    
}
