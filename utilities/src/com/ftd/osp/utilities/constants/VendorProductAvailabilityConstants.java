/**
 * 
 */
package com.ftd.osp.utilities.constants;

/**
 * @author smeka
 * 
 */
public class VendorProductAvailabilityConstants {

	// Inventory/ Vendor/ product status. 
	public final static String AVAILABLE = "A";
	public final static String UNAVAILABLE = "U";
	public final static String SHUTDOWN = "X";
	public final static String INACTIVE = "N";
	public final static String ACTIVE = "Y";
	
	// Inventory being updated by
	public final static String SYS_INV_TRK_USER = "SYSTEM_INV_TRK";

	// Subjects/alert types for different inventory alerts.
	public final static String INVENTORY_WARNING_ALERT = "Inventory Alert";
	public final static String LOC_SHUTDOWN_ALERT = "Location Shutdown Alert";
	public final static String PERIOD_SHUTDOWN_ALERT = "Period Shutdown Alert";
	public final static String PROD_SHUTDOWN_ALERT = "Product Shutdown Alert";
	public final static String DROPSHIP_SHUTDOWN_ALERT= "Dropship Shutdown Alert";
	
	// Inventory event history types
	public final static String LOCATION_SHUTDOWN_EVENT = "LOCATION_SHUTDOWN";
	public final static String PRODUCT_SHUTDOWN_EVENT = "PRODUCT_SHUTDOWN";
	
	// PAS COMMANDS
	public final static String PAS_MODIFIED_PRODUCT = "MODIFIED_PRODUCT";
	public final static String PAS_VENDOR_PROD_STATE_DATE = "ADD_VENDOR_PRODUCT_STATE_DATE";
	
	public final static int DEFAULT_SHIP_DEL_DAYS_DIFF = 5;
	public final static int DEFAULT_INV_START_END_DIFF = 60;
	public final static String PAS_CONFIG = "PAS_CONFIG";
	public final static String MAX_DAYS_KEY = "MAX_DAYS";
	public final static String ADD_DAYS_KEY = "ADD_DAYS";
}
