/**
 * 
 */
package com.ftd.osp.utilities.constants;

/**
 * @author kvasant
 *
 */
public class PaymentExtensionConstants {

	public final static String TRANSMISSION_DATE_TIME = "TrnmsnDateTime";
	public final static String STAN = "STAN";	
	public final static String REFERENCE_NUMBER = "RefNum";	
	public final static String TERMINAL_CATEGORY_CODE = "TermCatCode";
	public final static String POS_CONDITION_CODE = "POSCondCode";
	public final static String ADDITIONAL_AMOUNT = "AddAmt";	
	public final static String TRANSACTION_CURRENCY = "TxnCrncy";	
	public final static String RESPONSE_CODE = "RespCode";	
	public final static String REQUEST_ID = "requestId";
	public final static String CARD_SPECIFIC_DETAIL = "CardSpcDtl";	
	public final static String CARD_SPECIFIC_GROUP = "CardSpcGrp";
	public final static String ADDITIONAL_RESP_DATA = "AddtlRespData";	
	public final static String TRANSACTION_AMOUNT = "TxnAmt";
	public final static String ECI = "Eci";	
	public final static String CAVV = "Cavv";	
	public final static String XID = "Xid";	
	public final static String UCAF = "Ucaf";
	public final static String MERCHANT_REF_ID = "merchantRefId";
}
