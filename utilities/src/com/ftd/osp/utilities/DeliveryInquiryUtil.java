package com.ftd.osp.utilities;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * Restricts from sending multiple ASK messages based on the configured global parms
 * <b>DCON_ASK_QTY</b>, <b>DCON_ASK_TIMEFRAME</b>, <b>DCON_ASK_TEXT</b>
 * 
 * @author kdatchan
 *
 */
public class DeliveryInquiryUtil {

	private static Logger logger = new Logger(
			"com.ftd.osp.utilities.DeliveryInquiryUtil");
    
	private static final String DCON_ASK_QTY = "DCON_ASK_QTY";
	private static final String DCON_ASK_TIMEFRAME = "DCON_ASK_TIMEFRAME";
    private static final String DCON_ASK_TEXT = "DCON_ASK_TEXT";
    private static final String ORDER_PROCESSING = "ORDER_PROCESSING";
    
    private Connection conn;
    
    public DeliveryInquiryUtil(Connection conn) {
    	this.conn = conn;
    }
    
    /**
     * Sends ASK messages based on the configured global parms
     * <b>DCON_ASK_QTY</b>, <b>DCON_ASK_TIMEFRAME</b>, <b>DCON_ASK_TEXT</b>
     * 
     * @param orderDetailId
     * @param connection
     * @return
     */
    public boolean sendASKMessage(String orderDetailId) {
		ConfigurationUtil configUtil;
		String keyText, timeFrame, askCount;
		try {
			configUtil = ConfigurationUtil.getInstance();
			askCount = configUtil.getFrpGlobalParm(ORDER_PROCESSING, DCON_ASK_QTY);
			timeFrame = configUtil.getFrpGlobalParm(ORDER_PROCESSING, DCON_ASK_TIMEFRAME);
			keyText = configUtil.getFrpGlobalParm(ORDER_PROCESSING, DCON_ASK_TEXT);

			// do not restrict the ASK messages even if one of the global parm
			// is null/empty
			if (askCount == null || askCount.isEmpty()
					|| timeFrame == null || timeFrame.isEmpty()
					|| keyText == null || keyText.isEmpty()) {
				return true;
			}

			if (Integer.parseInt(askCount) > 0 && Integer.parseInt(timeFrame) > 0
					&& getASKMessageCount(orderDetailId, timeFrame, keyText) < Integer.parseInt(askCount)) {
				return true;
			}

		} catch (NumberFormatException nfe) {
			// do not restrict the ASK messages if DCON_ASK_TIMEFRAME and/or DCON_ASK_QTY is non-numeric
			logger.error("exception caught in sendASKMessage: " + nfe.toString());
			return true;
		} catch (Exception e) {
			// do not restrict the ASK messages due to exceptions
			logger.error("exception caught in sendASKMessage: " + e.toString());
			return true;
		}

		if(logger.isDebugEnabled()) {
			logger.debug("ASK message for delivery confirmation request will not be sent");
		}
		return false;
	}

	/**
	 * Gets the count of ASK message sent for the given order 
	 * between the given time period.
	 * 
	 * 
	 * @param orderDetailId
	 * @param timeFrame
	 * @param keyText
	 * @param connection
	 * @return
	 * @throws Exception
	 */
	private int getASKMessageCount(String orderDetailId, String timeFrame,
			String keyText) throws Exception {
		Integer count = 0;
		try {
			DataAccessUtil dau = DataAccessUtil.getInstance();
			DataRequest request = new DataRequest();
			request.setConnection(conn);
			HashMap inputParams = new HashMap();
			inputParams.put("IN_ORDER_DETAIL_ID", orderDetailId);
			inputParams.put("IN_DCON_ASK_TIMEFRAME", Integer.parseInt(timeFrame));
			inputParams.put("IN_DCON_ASK_TEXT", keyText);
			request.setInputParams(inputParams);
			request.setStatementID("GET_ASK_FOR_DCON_COUNT");
			Map outputParameters = (Map) dau.execute(request);

			count = Integer.parseInt((String) outputParameters.get("OUT_COUNT"));
			String status = (String) outputParameters.get("OUT_STATUS");
			String message = (String) outputParameters.get("OUT_MESSAGE");

			if (status.equalsIgnoreCase("N")) {
				throw new Exception("getASKMessageCount caught: " + message);
			}

		} catch (Exception e) {
			throw new Exception("getASKMessageCount caught: " + e.getMessage());
		}

		return count;
	}

}
