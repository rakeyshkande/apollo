package com.ftd.osp.utilities;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;

import java.net.InetAddress;

import java.sql.Connection;

import java.util.HashMap;
import java.util.Stack;

public class NumberSequenceGenerator 
{
  private final static String CONFIGURATION_FILE = "sequence-config.xml";
  private static Stack sequenceNumbers = new Stack();
  private static NumberSequenceGenerator numberSequenceGenerator = new NumberSequenceGenerator();
  private static boolean initialized;
  private static long startNumber;
  private static String machineId;
  private static final String HP_SEQUENCE_MAX = "GET_MAX_SEQUENCE";

  private static final long STACK_SIZE = 100L;
  
  private NumberSequenceGenerator()
  {
      this.init();
      this.generateSequenceBulk();
  }
  
  public static NumberSequenceGenerator getInstance()
  {
     return numberSequenceGenerator;
  }

  public synchronized String getSequenceNumber()
  {
    String snum = (String)sequenceNumbers.pop();

    if(sequenceNumbers.isEmpty())
    {
      this.generateSequenceBulk();
    }
    return snum;
  }

  private synchronized void init()
  {
      if(!initialized)
      {
        String hostname = null;
        String ip_address = null; 
        Connection connection = null;

        try
        {
          hostname = InetAddress.getLocalHost().getHostName();
          ip_address = InetAddress.getLocalHost().getHostAddress();

          machineId = hostname.substring(0, 3); // + ip_address.substring(ip_address.length() - 3, ip_address.length()));
          this.initialized = true;
        }
        catch(Exception e)
        {
          machineId = "tmp";
        }

        try
        {
            connection = DataSourceUtil.getInstance().getConnection("ORDER SCRUB");
            DataRequest dataRequest = new DataRequest(); 
            HashMap inputParms = new HashMap();
            inputParms.put("MACHINE_ID", machineId);
            dataRequest.setInputParams(inputParms);
            dataRequest.setConnection(connection);
            dataRequest.setStatementID(HP_SEQUENCE_MAX);
            String  crs = (String)DataAccessUtil.getInstance().execute(dataRequest);

            if(crs == null || crs.equals(""))
            {
              startNumber = 0;
            }
            else
            {
              crs = crs.substring(3, crs.length());
              startNumber = (Long.parseLong(crs) + 1);
            }
        }
        catch(Exception e)
        {
          startNumber = 0;
          e.printStackTrace();
        }finally{ 
          if( connection != null)
          {
            try
            {
              connection.close();
            }
            catch(Exception e){
                  //Do nothing
            }
          } 
        }
        initialized = true;
      }
  }

  private synchronized void generateSequenceBulk()
  {
    long i = 0L;
    for (i = (startNumber + STACK_SIZE); i >= startNumber ; i--) 
    {
      sequenceNumbers.push((machineId + i));
    }
    startNumber = startNumber + STACK_SIZE + 1;
/*    
    for(i = startNumber; i < (startNumber + 10); i++)
    {
      sequenceNumbers.push((machineId + i));
    }
    startNumber = i;
*/    
  }

  

 /* public static void main(String[] args)
  {
    long start = System.currentTimeMillis();
    System.out.println("START: "  + start);
    NumberSequenceGenerator nsg = NumberSequenceGenerator.getInstance();
    System.out.println("TOTAL: " + (System.currentTimeMillis()- start));

    for(int i=0;i<120;i++)
    {
        System.out.println("i=" + i + " Seq:" + nsg.getSequenceNumber());  
    }
  }*/
  
}