package com.ftd.osp.utilities.feed;

import com.ftd.osp.utilities.feed.vo.INovatorFeedDataVO;
import com.ftd.osp.utilities.feed.vo.NovatorFeedDataUpsellDetailVO;
import com.ftd.osp.utilities.feed.vo.UpsellDetailVO;
import com.ftd.osp.utilities.feed.vo.UpsellMasterVO;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;


/**
 * Novator Fuel Surcharge feed implementation.
 */ 
public class NovatorFeedTransformerUpsellDetailImpl extends NovatorFeedTransformerBase{
    private static final Logger logger = new Logger("com.ftd.osp.utilities.feed.NovatorFeedTransformerUpsellDetailImpl");    

    /**
     * Converts an upsell object to feed XML 
     * @param conn
     * @param dataVO
     * @return xml
     * @throws Exception
     */
    public String objectToXML(Connection conn, INovatorFeedDataVO dataVO) throws Exception{
        String xml = null;

        UpsellMasterVO umvo = null;
        String upsellMasterId = null;
        NovatorFeedProductUtil prodUtil =  new NovatorFeedProductUtil();
        
        NovatorFeedDataUpsellDetailVO pDataVO = (NovatorFeedDataUpsellDetailVO)dataVO;
        upsellMasterId = pDataVO.getUpsellMasterId();

        if(logger.isDebugEnabled()){
            logger.debug("productId passed in is:" + upsellMasterId);
        }
        
        //Retrive from database.
        umvo = prodUtil.getUpsellMaster(conn, upsellMasterId);

        xml = umvo.toXML();

        return xml;
    }
    
    
    public String objectToXML(INovatorFeedDataVO dataVO) throws Exception{
        return objectToXML(null, dataVO);
    }
}
