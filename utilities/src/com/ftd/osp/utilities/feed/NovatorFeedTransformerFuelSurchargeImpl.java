package com.ftd.osp.utilities.feed;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.feed.vo.INovatorFeedDataVO;
import com.ftd.osp.utilities.feed.vo.NovatorFeedDataFuelSurchargeVO;
import com.ftd.osp.utilities.xml.JAXPUtil;

import java.math.BigDecimal;

import java.sql.Connection;

import java.text.DecimalFormat;

import org.apache.commons.lang.StringUtils;

import org.w3c.dom.Document;
import org.w3c.dom.Element;


/**
 * Novator Fuel Surcharge feed implementation.
 */ 
public class NovatorFeedTransformerFuelSurchargeImpl extends NovatorFeedTransformerBase{
    private static final Logger logger = new Logger("com.ftd.osp.utilities.feed.NovatorFeedTransformerFuelSurchargeImpl");    

    private static final String SURCHARGE = "surcharge";
    private static final String EXTERNAL_FEE = "external_fee";
    private static final String EXTERNAL_FEE_FLAG = "external_fee_flag";

    private static final DecimalFormat df = new DecimalFormat("0.00");

    /**
     * Converts a Novator fuel surcharge object to feed XML 
     * @param INovatorFeedDataVO Novator fuel surcharge information
     * @return Fuel surcharge xml
     * @throws Exception
     */
    public String objectToXML(INovatorFeedDataVO dataVO) throws Exception{
        Element node = null;
        NovatorFeedDataFuelSurchargeVO fsDataVO = (NovatorFeedDataFuelSurchargeVO)dataVO;

        if(logger.isDebugEnabled()){
            logger.debug("Fuel Surcharge values passed in are:");
            logger.debug("fsDataVO.getFuelSurchargeAmt(): " + fsDataVO.getFuelSurchargeAmt());
            logger.debug("fsDataVO.getOnOffFlag(): " + fsDataVO.getOnOffFlag());
        }

        // Test for valid data
        if(StringUtils.isBlank(fsDataVO.getOnOffFlag())){
            throw new Exception("Invalid on/off flag");            
        }
        
        // Convert the VO to XML
        Document doc = JAXPUtil.createDocument();
        Element root = doc.createElement(SURCHARGE);
        doc.appendChild(root);
        
        // Convert surcharge to the correct format
        String formattedSurcharge = "";
        if (fsDataVO.getFuelSurchargeAmt() != null && !StringUtils.isBlank(fsDataVO.getFuelSurchargeAmt())) {
            BigDecimal surcharge = new BigDecimal(fsDataVO.getFuelSurchargeAmt());
            formattedSurcharge = df.format(surcharge.doubleValue());
        }
        
        node = JAXPUtil.buildSimpleXmlNode(doc, EXTERNAL_FEE, formattedSurcharge);
        root.appendChild(node);
        node = JAXPUtil.buildSimpleXmlNode(doc, EXTERNAL_FEE_FLAG, fsDataVO.getOnOffFlag());
        root.appendChild(node);

        String xml = JAXPUtil.toStringNoFormat(doc);

        if(logger.isDebugEnabled()){
            logger.debug("Generated the following feed XML:\n" + xml);
        }

        return xml;
    }
    
    public String objectToXML(Connection conn, INovatorFeedDataVO dataVO) throws Exception{
        return objectToXML(dataVO);
    }
}
