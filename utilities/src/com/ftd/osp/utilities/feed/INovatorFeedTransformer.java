package com.ftd.osp.utilities.feed;

import com.ftd.osp.utilities.feed.vo.INovatorFeedDataVO;

import java.sql.Connection;

/**
 * Interface used by a Novator feed implemenation.
 */ 
public interface INovatorFeedTransformer {
    public String objectToXML(INovatorFeedDataVO dataVO) throws Exception;
    public String objectToXML(Connection conn, INovatorFeedDataVO dataVO) throws Exception;
}
