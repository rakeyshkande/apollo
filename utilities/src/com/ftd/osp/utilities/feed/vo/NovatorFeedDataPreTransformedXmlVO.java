package com.ftd.osp.utilities.feed.vo;

/**
 * This implementation of the NovatorFeedDataVO contains
 * pre-transformed Feed Data.
 * It contains XML that should be sent as is without change.
 */
public class NovatorFeedDataPreTransformedXmlVO
  implements INovatorFeedDataVO
{
  /**
   * The pre-transformed XML to be sent to the Web Site
   */
  String feedXML;

  public void setFeedXML(String feedXML)
  {
    this.feedXML = feedXML;
  }

  public String getFeedXML()
  {
    return feedXML;
  }
}
