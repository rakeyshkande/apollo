package com.ftd.osp.utilities.feed.vo;

/**
 * Contains Novator Fuel Surcharge feed information.
 */ 
public class NovatorFeedDataUpsellDetailVO implements INovatorFeedDataVO {
    private String upsellMasterId = null;

    public void setUpsellMasterId(String upsellMasterId) {
        this.upsellMasterId = upsellMasterId;
    }

    public String getUpsellMasterId() {
        return upsellMasterId;
    }
}
