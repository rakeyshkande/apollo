package com.ftd.osp.utilities.feed.vo;

/**
 * Contains Novator Fuel Surcharge feed information.
 */ 
public class NovatorFeedDataFuelSurchargeVO implements INovatorFeedDataVO {
    // If the feed is on or off
    private String onOffFlag = null;
    
    // The fuel surcharge amount
    private String fuelSurchargeAmt = null;

    public void setOnOffFlag(String onOffFlag) {
        this.onOffFlag = onOffFlag;
    }

    public String getOnOffFlag() {
        return onOffFlag;
    }

    public void setFuelSurchargeAmt(String fuelSurchargeAmt) {
        this.fuelSurchargeAmt = fuelSurchargeAmt;
    }

    public String getFuelSurchargeAmt() {
        return fuelSurchargeAmt;
    }
}
