package com.ftd.osp.utilities.feed.vo;

/**
 * Contains Novator Fuel Surcharge feed information.
 */ 
public class NovatorFeedDataProductVO implements INovatorFeedDataVO {
    private String productId = null;


    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductId() {
        return productId;
    }
}
