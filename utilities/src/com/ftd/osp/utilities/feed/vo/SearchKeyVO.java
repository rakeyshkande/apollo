package com.ftd.osp.utilities.feed.vo;

public class SearchKeyVO   extends FeedValueObjectBase
{
    private String searchKey;
    
    public SearchKeyVO() {
        super("com.ftd.osp.utilities.feed.vo.SearchKeyVO");
    }

    public String getSearchKey()
    {
        return searchKey;
    }

    public void setSearchKey(String newSearchKey)
    {
        searchKey = newSearchKey;
    }
}