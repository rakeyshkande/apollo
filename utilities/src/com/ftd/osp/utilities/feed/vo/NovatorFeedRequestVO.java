package com.ftd.osp.utilities.feed.vo;

/**
 * Contains Novator feed request data.
 */ 
public class NovatorFeedRequestVO {
    // Which environment the feed should be sent to 
    private String feedToEnv = null;
    
    // Feed information
    private INovatorFeedDataVO dataVO = null;
    
    public void setFeedToEnv(String feedToEnv) {
        this.feedToEnv = feedToEnv;
    }

    public String getFeedToEnv() {
        return feedToEnv;
    }

    public void setDataVO(INovatorFeedDataVO dataVO) {
        this.dataVO = dataVO;
    }

    public INovatorFeedDataVO getDataVO() {
        return dataVO;
    }
}
