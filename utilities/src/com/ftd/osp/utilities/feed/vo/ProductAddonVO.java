package com.ftd.osp.utilities.feed.vo;

public class ProductAddonVO
{
    String  addonId;
    String  addonTypeId;
    String  addonDescription;
    String  addonPrice;
    Boolean addonAvailableFlag;
    Boolean addonTypeIncludedInProductFeedFlag;
    Boolean activeFlag;
    String maxQuantity;
    String displaySequenceNumber;
    Boolean modified;
    String  addonPquadAccessoryId;
        
    
    
    public ProductAddonVO()
    {
        this.addonId="";
        this.addonTypeId="";
        this.addonDescription="";
        this.addonPrice="";
        this.addonAvailableFlag = false;
        this.addonTypeIncludedInProductFeedFlag = false;
        this.activeFlag = false;
        this.maxQuantity = "";
        this.displaySequenceNumber = "";
        this.modified = false;
        this.addonPquadAccessoryId="";
    }

    public String getAddonId()
    {
        return addonId;
    }

    public void setAddonId(String addonId)
    {
        this.addonId = addonId;
    }

    public String getAddonTypeId()
    {
        return addonTypeId;
    }

    public void setAddonTypeId(String addonTypeId)
    {
        this.addonTypeId = addonTypeId;
    }

    public String getAddonDescription()
    {
        return addonDescription;
    }

    public void setAddonDescription(String addonDescription)
    {
        this.addonDescription = addonDescription;
    }

    public String getAddonPrice()
    {
        return addonPrice;
    }

    public void setAddonPrice(String addonPrice)
    {
        this.addonPrice = addonPrice;
    }

    public Boolean getAddonAvailableFlag()
    {
        return addonAvailableFlag;
    }

    public void setAddonAvailableFlag(Boolean addonAvailableFlag)
    {
        this.addonAvailableFlag = addonAvailableFlag;
    }

    public Boolean getAddonTypeIncludedInProductFeedFlag()
    {
        return addonTypeIncludedInProductFeedFlag;
    }

    public void setAddonTypeIncludedInProductFeedFlag(Boolean addonTypeIncludedInProductFeedFlag)
    {
        this.addonTypeIncludedInProductFeedFlag = addonTypeIncludedInProductFeedFlag;
    }

    public Boolean getActiveFlag()
    {
        return activeFlag;
    }

    public void setActiveFlag(Boolean activeFlag)
    {
        this.activeFlag = activeFlag;
    }

    public String getMaxQuantity()
    {
        return maxQuantity;
    }

    public void setMaxQuantity(String maxQuantity)
    {
        this.maxQuantity = maxQuantity;
    }

    public String getDisplaySequenceNumber()
    {
        return displaySequenceNumber;
    }

    public void setDisplaySequenceNumber(String displaySequenceNumber)
    {
        this.displaySequenceNumber = displaySequenceNumber;
    }

    public Boolean isModified(Boolean activeFlag, String maxQuantity, String displaySequenceNumber)
    {
        if (this.activeFlag != activeFlag || this.maxQuantity == null ^ maxQuantity == null || 
            this.maxQuantity != null && maxQuantity != null && !this.maxQuantity.equals(maxQuantity) ||
            this.displaySequenceNumber == null ^ displaySequenceNumber == null ||
            this.displaySequenceNumber != null && displaySequenceNumber != null && !this.displaySequenceNumber.equals(displaySequenceNumber))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public Boolean isModified()
    {
        return this.modified;
    }

    public void setModified(Boolean modified)
    {
        this.modified = modified;
    }


	public String getAddonPquadAccessoryId() {
		return addonPquadAccessoryId;
	}

	public void setAddonPquadAccessoryId(String addonPquadAccessoryId) {
		this.addonPquadAccessoryId = addonPquadAccessoryId;
	}
}
