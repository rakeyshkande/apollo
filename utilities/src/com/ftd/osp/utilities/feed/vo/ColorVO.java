package com.ftd.osp.utilities.feed.vo;

public class ColorVO extends FeedValueObjectBase 
{
    private String id;
    private String description;
    private int orderBy;
    private String productId;
    
    public ColorVO() {
        super("com.ftd.osp.utilities.feed.vo.ColorVO");
    }

    public String getId()
    {
        return id;
    }

    public void setId(String newId)
    {
        id = newId;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String newDescription)
    {
        description = newDescription;
    }

    public int getOrderBy()
    {
        return orderBy;
    }

    public void setOrderBy(int newOrderBy)
    {
        orderBy = newOrderBy;
    }

    public String getProductId()
    {
        return productId;
    }

    public void setProductId(String newProductId)
    {
        productId = newProductId;
    }

    public boolean equals(Object obj)
    {
        boolean ret = false;
        if(id.equals(((ColorVO)obj).getId()))
        {
            ret = true;
        }

        return ret;
    }    
}