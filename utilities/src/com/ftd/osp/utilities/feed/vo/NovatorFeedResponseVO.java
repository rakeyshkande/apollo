package com.ftd.osp.utilities.feed.vo;

/**
 * Contains Novator feed response data.
 */ 
 public class NovatorFeedResponseVO {
    // If the feed was successful
    private boolean success;
    
    // Error message if the feed wasn't successful
    private String errorString = null;
    
    /**
     * Indicates that this response is due to an Exception when transmitting
     */
    private boolean isTransmitException = false;
    
    /**
     * The throwable if isTransmitException== true
     */
    private Throwable transmitThrowable = null;

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setErrorString(String errorString) {
        this.errorString = errorString;
    }

    public String getErrorString() {
        return errorString;
    }


  public void setIsTransmitException(boolean isTransmitException)
  {
    this.isTransmitException = isTransmitException;
  }

  public boolean getIsTransmitException()
  {
    return isTransmitException;
  }

  public void setTransmitThrowable(Throwable transmitThrowable)
  {
    this.transmitThrowable = transmitThrowable;
  }

  public Throwable getTransmitThrowable()
  {
    return transmitThrowable;
  }
}
