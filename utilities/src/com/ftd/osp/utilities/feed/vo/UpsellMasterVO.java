package com.ftd.osp.utilities.feed.vo;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.feed.FeedConstants;
import com.ftd.osp.utilities.feed.FeedXMLTags;
import com.ftd.osp.utilities.xml.JAXPException;
import com.ftd.osp.utilities.xml.JAXPUtil;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Element;


public class UpsellMasterVO  extends FeedValueObjectBase
{
    private String MasterID;
    private String MasterName;
    private String MasterDescription;
    private boolean MasterStatus;
    private List UpsellDetailList = new ArrayList();
    private List SearchKeyList;
    private String[] RecipientSearchList;
    private boolean CorporateSiteFlag;
    private String SearchPriority;
    private String masterType;
    private String[] companyList; 
    private boolean gbbPopoverFlag;
    private String gbbTitle;
    private String gbbUpsellDetailId1;
    private boolean gbbNameOverrideFlag1;
    private String gbbNameOverrideText1;
    private boolean gbbPriceOverrideFlag1;
    private String gbbPriceOverrideText1;
    private String gbbUpsellDetailId2;
    private boolean gbbNameOverrideFlag2;
    private String gbbNameOverrideText2;
    private boolean gbbPriceOverrideFlag2;
    private String gbbPriceOverrideText2;
    private String gbbUpsellDetailId3;
    private boolean gbbNameOverrideFlag3;
    private String gbbNameOverrideText3;
    private boolean gbbPriceOverrideFlag3;
    private String gbbPriceOverrideText3;
    private List upsellSourceList;
    private List websites;

	public UpsellMasterVO() {
        super("com.ftd.osp.utilities.feed.vo.UpsellMasterVO");
    }
    
    public List getWebsites() {
		return websites;
	}

	public void setWebsites(List websites) {
		this.websites = websites;
	}

    public String getMasterID()
    {
        return MasterID;
    }

    public void setMasterID(String newMasterID)
    {
        MasterID = newMasterID;
    }

    public String getMasterName()
    {
        return MasterName;
    }

    public void setMasterName(String newMasterName)
    {
        MasterName = newMasterName;
    }

    public String getMasterDescription()
    {
        return MasterDescription;
    }

    public void setMasterDescription(String newMasterDescription)
    {
        MasterDescription = newMasterDescription;
    }

    public boolean isMasterStatus()
    {
        return MasterStatus;
    }

    public void setMasterStatus(boolean newMasterStatus)
    {
        MasterStatus = newMasterStatus;
    }

    public List getUpsellDetailList()
    {
        return UpsellDetailList;
    }

    public void setUpsellDetailList(List newUpsellDetailList)
    {
        UpsellDetailList = newUpsellDetailList;
    }

    public List getSearchKeyList()
    {
        return SearchKeyList;
    }

    public void setSearchKeyList(List newSearchKeyList)
    {
        SearchKeyList = newSearchKeyList;
    }

    public String[] getRecipientSearchList()
    {
        return RecipientSearchList;
    }

    public void setRecipientSearchList(String[] newRecipientSearchList)
    {
        RecipientSearchList = newRecipientSearchList;
    }

    public boolean isCorporateSiteFlag()
    {
        return CorporateSiteFlag;
    }

    public void setCorporateSiteFlag(boolean newCorporateSiteFlag)
    {
        CorporateSiteFlag = newCorporateSiteFlag;
    }

    public String getSearchPriority()
    {
        return SearchPriority;
    }

    public void setSearchPriority(String newSearchPriority)
    {
        SearchPriority = newSearchPriority;
    }    

    public String getMasterType()
    {
        return masterType;
    }

    public void setMasterType(String newMasterType)
    {
        masterType = newMasterType;
    }

    public String[] getCompanyList()
    {
        return companyList;
    }

    public void setCompanyList(String[] newCompanyList)
    {
        companyList = newCompanyList;
    }
    

  	/**
    * Returns a string representation of the object in XML
  	*/  
    public String toXML() throws Exception
    {		
        String retval = null;
        
        try {
            retval = JAXPUtil.toStringNoFormat(toXMLDocument());
        } catch (JAXPException xpe) {
            throw new Exception(xpe.getMessage());
        }
        
        return retval;
    }

    /**
    * Returns a representation of the object in XML for transmission to
    * Novator
    */
    public Document toXMLDocument() throws Exception
    {
        // <upsell_master>
        //   <upsell_master_id>...
        //   <upsell_name>...
        //   <upsell_description>...
        //   <upsell_status>...
        //   <upsell_detail>...
        //   <keywords>
        //      <key>...
        //   </keywords>
        //   <recipient_search>
        //      <recipient_search_entity>...
        //   </recipient_search>
        //   <corporate_site>...
        //   <search_priority>...
        //   <type>...
        //   <companies>
        //      <company>...
        //   </companies>
        // </upsell_master>
        
        Document doc = null;
        
        try 
        {
            doc = JAXPUtil.createDocument();
            Element root = doc.createElement(FeedXMLTags.TAG_UPSELL_MASTER);
            root.appendChild(JAXPUtil.buildSimpleXmlNode(doc,FeedXMLTags.TAG_UPSELL_MASTER_ID, MasterID));
            root.appendChild(JAXPUtil.buildSimpleXmlNodeCDATA(doc,FeedXMLTags.TAG_UPSELL_MASTER_NAME, MasterName));
            root.appendChild(JAXPUtil.buildSimpleXmlNodeCDATA(doc,FeedXMLTags.TAG_UPSELL_DESCRIPTION, MasterDescription));
            root.appendChild(JAXPUtil.buildSimpleXmlNode(doc,FeedXMLTags.TAG_UPSELL_STATUS, MasterStatus));

            String defaultSku = "";
            int idx;
            Element retElemChild1 = doc.createElement(FeedXMLTags.TAG_UPSELL_DETAIL);
            for( idx=0; idx<UpsellDetailList.size(); idx++ )
            {
                UpsellDetailVO upsellDetailVO = (UpsellDetailVO)UpsellDetailList.get(idx);
                if( upsellDetailVO!=null )
                {   
                    Document subDoc = upsellDetailVO.toXMLDocument();
                    //TODO: what does true below indicate?
                    Element detailElement = (Element)subDoc.getFirstChild().cloneNode(true);
                    retElemChild1.appendChild(doc.importNode(detailElement,true));
                    if (upsellDetailVO.getDefaultSkuFlag() != null && upsellDetailVO.getDefaultSkuFlag().equalsIgnoreCase("Y")) {
                        defaultSku = upsellDetailVO.getDetailID();
                    }
                }
            }
            root.appendChild(retElemChild1);
            
            root.appendChild(JAXPUtil.buildSimpleXmlNode(doc, FeedXMLTags.TAG_UPSELL_DEFAULT_SKU, defaultSku));

            retElemChild1 = doc.createElement(FeedXMLTags.TAG_KEYWORDS);
            for( idx=0; idx<SearchKeyList.size(); idx++)
            {
                SearchKeyVO skvo = (SearchKeyVO)SearchKeyList.get(idx);
                if( skvo!=null ) {
                    retElemChild1.appendChild(JAXPUtil.buildSimpleXmlNode(doc,FeedXMLTags.TAG_KEY,skvo.getSearchKey()));
                }
            }
            root.appendChild(retElemChild1);

            retElemChild1 = doc.createElement(FeedXMLTags.TAG_RECIPIENT_SEARCH);
            if (RecipientSearchList!=null){
                for( idx=0; idx<RecipientSearchList.length; idx++)
                {
                    String recipient = RecipientSearchList[idx];
                    if( recipient!=null  )
                        retElemChild1.appendChild(JAXPUtil.buildSimpleXmlNode(doc,FeedXMLTags.TAG_RECIPIENT_SEARCH_ENTITY,recipient));
                }
            }
            root.appendChild(retElemChild1);

            root.appendChild(JAXPUtil.buildSimpleXmlNode(doc,FeedXMLTags.TAG_CORPORATE_SITE,CorporateSiteFlag));
            root.appendChild(JAXPUtil.buildSimpleXmlNode(doc,FeedXMLTags.TAG_SEARCH_PRIORITY,SearchPriority));
            root.appendChild(JAXPUtil.buildSimpleXmlNode(doc,FeedXMLTags.TAG_TYPE,masterType));

            retElemChild1 = doc.createElement(FeedXMLTags.TAG_COMPANIES);
            String[] companyList = this.getCompanyList();
            if(companyList != null)
            {
              for(int i = 0; i < companyList.length; i++)
              {
                retElemChild1.appendChild(JAXPUtil.buildSimpleXmlNode(doc,FeedXMLTags.TAG_COMPANY, companyList[i]));
              }
            }
            root.appendChild(retElemChild1);
            Element child1 = doc.createElement(FeedXMLTags.TAG_GBB);
            Element child2 = null;
            Element child3 = null;

            if (this.getGbbPopoverFlag()) {
                child2 = JAXPUtil.buildSimpleXmlNode(doc,FeedXMLTags.TAG_GBB_POPOVER_FLAG, "1");
            } else {
                child2 = JAXPUtil.buildSimpleXmlNode(doc,FeedXMLTags.TAG_GBB_POPOVER_FLAG, "");
            }
            child1.appendChild(child2);

            String gbbTitle = this.getGbbTitle();
            if (gbbTitle == null || gbbTitle.equals("")) {
                    ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
                    gbbTitle = configUtil.getFrpGlobalParmNoNull(FeedConstants.PDB_CONFIG_CONTEXT, FeedConstants.PDB_GBB_DEFAULT_TITLE);
                    logger.debug("Substituting default GBB title: " + gbbTitle);
            }
            child2 = JAXPUtil.buildSimpleXmlNodeCDATA(doc,FeedXMLTags.TAG_GBB_TITLE, gbbTitle);
            child1.appendChild(child2);

            child2 = doc.createElement(FeedXMLTags.TAG_GBB_POSITION_1);
            child3 = JAXPUtil.buildSimpleXmlNode(doc,FeedXMLTags.TAG_GBB_SKU, this.getGbbUpsellDetailId1());
            child2.appendChild(child3);
            if (this.getGbbNameOverrideFlag1()) {
                child3 = JAXPUtil.buildSimpleXmlNode(doc,FeedXMLTags.TAG_GBB_NAME_OVERRIDE_FLAG, "1");
            } else {
                child3 = JAXPUtil.buildSimpleXmlNode(doc,FeedXMLTags.TAG_GBB_NAME_OVERRIDE_FLAG, "");
            }
            child2.appendChild(child3);
            child3 = JAXPUtil.buildSimpleXmlNodeCDATA(doc,FeedXMLTags.TAG_GBB_NAME_OVERRIDE_TEXT, this.getGbbNameOverrideText1());
            child2.appendChild(child3);
            if (this.getGbbPriceOverrideFlag1()) {
                child3 = JAXPUtil.buildSimpleXmlNode(doc,FeedXMLTags.TAG_GBB_PRICE_OVERRIDE_FLAG, "1");
            } else {
                child3 = JAXPUtil.buildSimpleXmlNode(doc,FeedXMLTags.TAG_GBB_PRICE_OVERRIDE_FLAG, "");
            }
            child2.appendChild(child3);
            child3 = JAXPUtil.buildSimpleXmlNodeCDATA(doc,FeedXMLTags.TAG_GBB_PRICE_OVERRIDE_TEXT, this.getGbbPriceOverrideText1());
            child2.appendChild(child3);
            child1.appendChild(child2);

            child2 = doc.createElement(FeedXMLTags.TAG_GBB_POSITION_2);
            child3 = JAXPUtil.buildSimpleXmlNode(doc,FeedXMLTags.TAG_GBB_SKU, this.getGbbUpsellDetailId2());
            child2.appendChild(child3);
            if (this.getGbbNameOverrideFlag2()) {
                child3 = JAXPUtil.buildSimpleXmlNode(doc,FeedXMLTags.TAG_GBB_NAME_OVERRIDE_FLAG, "1");
            } else {
                child3 = JAXPUtil.buildSimpleXmlNode(doc,FeedXMLTags.TAG_GBB_NAME_OVERRIDE_FLAG, "");
            }
            child2.appendChild(child3);
            child3 = JAXPUtil.buildSimpleXmlNodeCDATA(doc,FeedXMLTags.TAG_GBB_NAME_OVERRIDE_TEXT, this.getGbbNameOverrideText2());
            child2.appendChild(child3);
            if (this.getGbbPriceOverrideFlag2()) {
                child3 = JAXPUtil.buildSimpleXmlNode(doc,FeedXMLTags.TAG_GBB_PRICE_OVERRIDE_FLAG, "1");
            } else {
                child3 = JAXPUtil.buildSimpleXmlNode(doc,FeedXMLTags.TAG_GBB_PRICE_OVERRIDE_FLAG, "");
            }
            child2.appendChild(child3);
            child3 = JAXPUtil.buildSimpleXmlNodeCDATA(doc,FeedXMLTags.TAG_GBB_PRICE_OVERRIDE_TEXT, this.getGbbPriceOverrideText2());
            child2.appendChild(child3);
            child1.appendChild(child2);

            child2 = doc.createElement(FeedXMLTags.TAG_GBB_POSITION_3);
            child3 = JAXPUtil.buildSimpleXmlNode(doc,FeedXMLTags.TAG_GBB_SKU, this.getGbbUpsellDetailId3());
            child2.appendChild(child3);
            if (this.getGbbNameOverrideFlag3()) {
                child3 = JAXPUtil.buildSimpleXmlNode(doc,FeedXMLTags.TAG_GBB_NAME_OVERRIDE_FLAG, "1");
            } else {
                child3 = JAXPUtil.buildSimpleXmlNode(doc,FeedXMLTags.TAG_GBB_NAME_OVERRIDE_FLAG, "");
            }
            child2.appendChild(child3);
            child3 = JAXPUtil.buildSimpleXmlNodeCDATA(doc,FeedXMLTags.TAG_GBB_NAME_OVERRIDE_TEXT, this.getGbbNameOverrideText3());
            child2.appendChild(child3);
            if (this.getGbbPriceOverrideFlag3()) {
                child3 = JAXPUtil.buildSimpleXmlNode(doc,FeedXMLTags.TAG_GBB_PRICE_OVERRIDE_FLAG, "1");
            } else {
                child3 = JAXPUtil.buildSimpleXmlNode(doc,FeedXMLTags.TAG_GBB_PRICE_OVERRIDE_FLAG, "");
            }
            child2.appendChild(child3);
            child3 = JAXPUtil.buildSimpleXmlNodeCDATA(doc,FeedXMLTags.TAG_GBB_PRICE_OVERRIDE_TEXT, this.getGbbPriceOverrideText3());
            child2.appendChild(child3);
            child1.appendChild(child2);
            root.appendChild(child1);
            
            // Website source codes
            child1 = doc.createElement(FeedXMLTags.TAG_WEBSITES);
            List upsellSourceList = this.getUpsellSourceList();
            if(upsellSourceList != null && upsellSourceList.size() > 0)
            {
                for (int i = 0 ; i < upsellSourceList.size(); i++) {
                    child2 = JAXPUtil.buildSimpleXmlNode(doc,FeedXMLTags.TAG_WEBSITE, (String)upsellSourceList.get(i));
                    child1.appendChild(child2);
                }
            }
            root.appendChild(child1);
                  
            doc.appendChild(root);
            logger.debug(JAXPUtil.toString(doc));
        }
        catch (Exception e) 
        {
            throw e;
        }
        
        return doc;        
    }
    
    public void setGbbPopoverFlag(boolean gbbPopoverFlag) {
        this.gbbPopoverFlag = gbbPopoverFlag;
    }

    public boolean getGbbPopoverFlag() {
        return gbbPopoverFlag;
    }

    public void setGbbTitle(String gbbTitle) {
        this.gbbTitle = gbbTitle;
    }

    public String getGbbTitle() {
        return gbbTitle;
    }

    public void setGbbUpsellDetailId1(String gbbUpsellDetailId1) {
        this.gbbUpsellDetailId1 = gbbUpsellDetailId1;
    }

    public String getGbbUpsellDetailId1() {
        return gbbUpsellDetailId1;
    }

    public void setGbbNameOverrideFlag1(boolean gbbNameOverrideFlag1) {
        this.gbbNameOverrideFlag1 = gbbNameOverrideFlag1;
    }

    public boolean getGbbNameOverrideFlag1() {
        return gbbNameOverrideFlag1;
    }

    public void setGbbNameOverrideText1(String gbbNameOverrideText1) {
        this.gbbNameOverrideText1 = gbbNameOverrideText1;
    }

    public String getGbbNameOverrideText1() {
        return gbbNameOverrideText1;
    }

    public void setGbbPriceOverrideFlag1(boolean gbbPriceOverrideFlag1) {
        this.gbbPriceOverrideFlag1 = gbbPriceOverrideFlag1;
    }

    public boolean getGbbPriceOverrideFlag1() {
        return gbbPriceOverrideFlag1;
    }

    public void setGbbPriceOverrideText1(String gbbPriceOverrideText1) {
        this.gbbPriceOverrideText1 = gbbPriceOverrideText1;
    }

    public String getGbbPriceOverrideText1() {
        return gbbPriceOverrideText1;
    }

    public void setGbbUpsellDetailId2(String gbbUpsellDetailId2) {
        this.gbbUpsellDetailId2 = gbbUpsellDetailId2;
    }

    public String getGbbUpsellDetailId2() {
        return gbbUpsellDetailId2;
    }

    public void setGbbNameOverrideFlag2(boolean gbbNameOverrideFlag2) {
        this.gbbNameOverrideFlag2 = gbbNameOverrideFlag2;
    }

    public boolean getGbbNameOverrideFlag2() {
        return gbbNameOverrideFlag2;
    }

    public void setGbbNameOverrideText2(String gbbNameOverrideText2) {
        this.gbbNameOverrideText2 = gbbNameOverrideText2;
    }

    public String getGbbNameOverrideText2() {
        return gbbNameOverrideText2;
    }

    public void setGbbPriceOverrideFlag2(boolean gbbPriceOverrideFlag2) {
        this.gbbPriceOverrideFlag2 = gbbPriceOverrideFlag2;
    }

    public boolean getGbbPriceOverrideFlag2() {
        return gbbPriceOverrideFlag2;
    }

    public void setGbbPriceOverrideText2(String gbbPriceOverrideText2) {
        this.gbbPriceOverrideText2 = gbbPriceOverrideText2;
    }

    public String getGbbPriceOverrideText2() {
        return gbbPriceOverrideText2;
    }

    public void setGbbUpsellDetailId3(String gbbUpsellDetailId3) {
        this.gbbUpsellDetailId3 = gbbUpsellDetailId3;
    }

    public String getGbbUpsellDetailId3() {
        return gbbUpsellDetailId3;
    }

    public void setGbbNameOverrideFlag3(boolean gbbNameOverrideFlag3) {
        this.gbbNameOverrideFlag3 = gbbNameOverrideFlag3;
    }

    public boolean getGbbNameOverrideFlag3() {
        return gbbNameOverrideFlag3;
    }

    public void setGbbNameOverrideText3(String gbbNameOverrideText3) {
        this.gbbNameOverrideText3 = gbbNameOverrideText3;
    }

    public String getGbbNameOverrideText3() {
        return gbbNameOverrideText3;
    }

    public void setGbbPriceOverrideFlag3(boolean gbbPriceOverrideFlag3) {
        this.gbbPriceOverrideFlag3 = gbbPriceOverrideFlag3;
    }

    public boolean getGbbPriceOverrideFlag3() {
        return gbbPriceOverrideFlag3;
    }

    public void setGbbPriceOverrideText3(String gbbPriceOverrideText3) {
        this.gbbPriceOverrideText3 = gbbPriceOverrideText3;
    }

    public String getGbbPriceOverrideText3() {
        return gbbPriceOverrideText3;
    }

    public void setUpsellSourceList(List upsellSourceList) {
        this.upsellSourceList = upsellSourceList;
    }

    public List getUpsellSourceList() {
        return upsellSourceList;
    }
}
