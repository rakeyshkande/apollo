package com.ftd.osp.utilities.feed.vo;

public class ProductSubCodeVO extends FeedValueObjectBase 
{
    private String productSubCodeId;
    private String productId;
    private String subCodeDescription;
    private float subCodePrice;
    private String subCodeHolidaySKU;
    private float subCodeHolidayPrice;
    private String subCodeRefNumber;
    private String dimWeight;
    private int displayOrder;
    private boolean activeFlag;

    public ProductSubCodeVO()
    {
        super("com.ftd.osp.utilities.feed.vo.ProductSubCodeVO");
        productSubCodeId = "";
        productId = "";
        subCodeDescription = "";
        subCodeHolidaySKU = "";
        subCodeRefNumber = "";
        activeFlag = false;
    }

    public String getProductSubCodeId()
    {
        return productSubCodeId;
    }

    public void setProductSubCodeId(String newProductSubCodeId)
    {
        productSubCodeId = newProductSubCodeId;
    }

    public String getProductId()
    {
        return productId;
    }

    public void setProductId(String newProductId)
    {
        productId = newProductId;
    }

    public String getSubCodeDescription()
    {
        return subCodeDescription;
    }

    public void setSubCodeDescription(String newSubCodeDescription)
    {
        subCodeDescription = newSubCodeDescription;
    }

    public float getSubCodePrice()
    {
        return subCodePrice;
    }

    public void setSubCodePrice(float newSubCodePrice)
    {
        subCodePrice = newSubCodePrice;
    }

    public float getSubCodeHolidayPrice()
    {
        return subCodeHolidayPrice;
    }

    public void setSubCodeHolidayPrice(float newSubCodeHolidayPrice)
    {
        subCodeHolidayPrice = newSubCodeHolidayPrice;
    }

    public String getSubCodeHolidaySKU()
    {
        return subCodeHolidaySKU;
    }

    public void setSubCodeHolidaySKU(String newSubCodeHolidaySKU)
    {
        subCodeHolidaySKU = newSubCodeHolidaySKU;
    }

    public String getSubCodeRefNumber()
    {
        return subCodeRefNumber;
    }

    public void setSubCodeRefNumber(String newSubCodeRefNumber)
    {
        subCodeRefNumber = newSubCodeRefNumber;
    }

    public String getDimWeight()
    {
        return dimWeight;
    }

    public void setDimWeight(String newDimWeight)
    {
        dimWeight = newDimWeight;
    }

    public int getDisplayOrder() 
    {
        return displayOrder;
    }

    public void setDisplayOrder(int newDisplayOrder)
    {
        displayOrder = newDisplayOrder;
    }

    public void setActiveFlag(boolean activeFlag) {
        this.activeFlag = activeFlag;
    }

    public boolean isActiveFlag() {
        return activeFlag;
    }
}
