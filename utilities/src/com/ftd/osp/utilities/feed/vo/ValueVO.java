package com.ftd.osp.utilities.feed.vo;

public class ValueVO extends FeedValueObjectBase
{
	private String id;
	private String description;
        
    public ValueVO() {
        super("com.ftd.osp.utilities.feed.vo.ValueVO");
    }

    public String getDescription() 
    { 
        return description; 
    }
    
    public String getId()
    {
        return id;
    }
    
    public void setDescription(String description) 
    {
        this.description = description; 
    }
    
    public void setId(String id)
    {
        this.id = id;
    }

    public boolean equals(Object obj)
    {
        boolean ret = false;
        if((id.equals(((ValueVO)obj).getId())) && (description.equals(((ValueVO)obj).getDescription())))
        {
            ret = true;
        }

        return ret;
    }
}