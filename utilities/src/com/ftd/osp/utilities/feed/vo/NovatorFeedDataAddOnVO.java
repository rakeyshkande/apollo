package com.ftd.osp.utilities.feed.vo;

import org.apache.commons.lang.StringEscapeUtils;

public class NovatorFeedDataAddOnVO implements INovatorFeedDataVO {
	public static final String FLORAL = "Floral";
	public static final String VENDOR = "Vendor";
	public static final String FLORALORVENDOR = "FloralOrVendor";

	String add_on_id;
	String add_on_description;
	String add_on_type_id;
	String add_on_type_description;
	String price;
	String add_on_text;
	String unspsc;
	String available_flag;
	String default_per_type_flag;
	String display_price_flag;
	String delivery_type;
	String sPquadAccsryId;// DI-4: Add-on Dashboard Enhancements: Added 2 new attributes to add-on : PQUAD_ACCESSORY_ID, IS_FTD_WEST_ADDON.
	String isFtdWestAddon;// DI-4: Add-on Dashboard Enhancements: Added 2 new attributes to add-on : PQUAD_ACCESSORY_ID, FTD_WEST_ADDON.
	
	public String getAdd_on_id() {
		return add_on_id;
	}

	public void setAdd_on_id(String add_on_id) {
		this.add_on_id = add_on_id;
	}

	public String getAdd_on_description() {
		return add_on_description;
	}

	public void setAdd_on_description(String add_on_description) {
		this.add_on_description = add_on_description;
	}

	public String getAdd_on_type_id() {
		return add_on_type_id;
	}

	public void setAdd_on_type_id(String add_on_type_id) {
		this.add_on_type_id = add_on_type_id;
	}

	public String getAdd_on_type_description() {
		return add_on_type_description;
	}

	public void setAdd_on_type_description(String add_on_type_description) {
		this.add_on_type_description = add_on_type_description;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getAdd_on_text() {
		return add_on_text;
	}

	public void setAdd_on_text(String add_on_text) {
		this.add_on_text = add_on_text;
	}

	public String getUnspsc() {
		return unspsc;
	}

	public void setUnspsc(String unspsc) {
		this.unspsc = unspsc;
	}

	public String getAvailable_flag() {
		return available_flag;
	}

	public void setAvailable_flag(String available_flag) {
		this.available_flag = available_flag;
	}

	public String getDefault_per_type_flag() {
		return default_per_type_flag;
	}

	public void setDefault_per_type_flag(String default_per_type_flag) {
		this.default_per_type_flag = default_per_type_flag;
	}

	public String getDisplay_price_flag() {
		return display_price_flag;
	}

	public void setDisplay_price_flag(String display_price_flag) {
		this.display_price_flag = display_price_flag;
	}

	/*
	 * #552 - Flexible Fullfillment - Addon Dash board and Maintanance changes
	 * added the getter and setter for the addon delivery type to get/set the
	 * desired delivery type. commneted the code as we are no longer sending the
	 * new feed to novator (delivery_type) Removing the comment as Store module
	 * need this flag.
	 */

	public String getDelivery_type() {
		return delivery_type;
	}

	public void setDelivery_type(String delivery_type) {
		if (delivery_type.equals("florist"))
			this.delivery_type = FLORAL;
		else if (delivery_type.equals("dropship")) {
			this.delivery_type = VENDOR;
		} else if (delivery_type.equals("both")) {
			this.delivery_type = FLORALORVENDOR;
		}
	}

	/**
	 * @return the sPquadAccsryId
	 */
	public String getsPquadAccsryId() {
		return sPquadAccsryId;
	}

	/**
	 * @param sPquadAccsryId the sPquadAccsryId to set
	 */
	public void setsPquadAccsryId(String sPquadAccsryId) {
		this.sPquadAccsryId = sPquadAccsryId;
	}

	/**
	 * @return the isFtdWestAddon
	 */
	public String isFtdWestAddon() {
		return isFtdWestAddon;
	}

	/**
	 * @param isFtdWestAddon the isFtdWestAddon to set
	 */
	public void setFtdWestAddon(String isFtdWestAddon) {
		this.isFtdWestAddon = isFtdWestAddon;
	}
}
