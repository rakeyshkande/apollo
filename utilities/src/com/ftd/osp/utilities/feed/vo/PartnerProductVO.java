package com.ftd.osp.utilities.feed.vo;

import java.math.BigDecimal;


/**
 * The purpose of this class is to hold values from the FTD_APPS.,PARTNER_PRODUCTS
 * table.
 *
 * @author Tim Peterson
 */
public class PartnerProductVO {

    private String productSubcodeId;
    private String partnerId;
    private BigDecimal wholesalePrice;
    private boolean availableFlag;
    /**
     * Default constructor
     */
    public PartnerProductVO() {
    }

    public void setProductSubcodeId(String productSubcodeId) {
        this.productSubcodeId = productSubcodeId;
    }

    public String getProductSubcodeId() {
        return productSubcodeId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setWholesalePrice(BigDecimal wholesalePrice) {
        this.wholesalePrice = wholesalePrice;
    }

    public BigDecimal getWholesalePrice() {
        return wholesalePrice;
    }

    public void setAvailableFlag(boolean availableFlag) {
        this.availableFlag = availableFlag;
    }

    public boolean isAvailableFlag() {
        return availableFlag;
    }
    
    public void copyTo(PartnerProductVO targetVO) {
        if( targetVO!=null ) {
            targetVO.setAvailableFlag(this.availableFlag);
            targetVO.setPartnerId(this.partnerId);
            targetVO.setProductSubcodeId(this.productSubcodeId);
            targetVO.setWholesalePrice(this.wholesalePrice);
        }
    }
}
