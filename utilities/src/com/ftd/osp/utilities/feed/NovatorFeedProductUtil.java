package com.ftd.osp.utilities.feed;

import com.ftd.ftdutilities.FieldUtils;
import com.ftd.osp.framework.dispatcher.Dispatcher;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.feed.vo.NovatorFeedDataProductVO;
import com.ftd.osp.utilities.feed.vo.NovatorFeedDataUpsellDetailVO;
import com.ftd.osp.utilities.feed.vo.NovatorFeedRequestVO;
import com.ftd.osp.utilities.feed.vo.NovatorFeedResponseVO;
import com.ftd.osp.utilities.feed.vo.ProductVO;
import com.ftd.osp.utilities.feed.vo.SearchKeyVO;
import com.ftd.osp.utilities.feed.vo.UpsellDetailVO;
import com.ftd.osp.utilities.feed.vo.UpsellMasterVO;
import com.ftd.osp.utilities.feed.vo.ValueVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;
import com.ftd.osp.utilities.vo.MessageToken;

import java.math.BigDecimal;
import java.sql.Connection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import java.util.Map;

import javax.naming.InitialContext;

import org.apache.commons.lang.StringUtils;


/**
 * Utility class for Novator product feed.
 * 1.  Send Product Feed
 * 2.  Send Upsell Feed if applies.
 */ 
public class NovatorFeedProductUtil {
    private static Logger logger = new Logger("com.ftd.osp.utilities.feed.NovatorProductFeedUtil");    
    private NovatorFeedUtil feedUtil = null;
    
    public NovatorFeedProductUtil() {
        if(feedUtil == null) {
            feedUtil = new NovatorFeedUtil();
        }
    }

    /**
     * Send product feed information to Novator.
     * @param conn        Database connection
     * @param productId   Product id
     * @param envKeys     List of environments to feed to
     * @return Feed response information
     * @throws Exception
     */
    public NovatorFeedResponseVO sendProductFeed(Connection conn, String productId, List<String> envKeys) throws Exception {
        NovatorFeedResponseVO responseVO = sendProductFeed(conn, productId, envKeys, true);
        return responseVO;
    }
    
    /**
     * Send product feed information to Novator.
     * @param conn            Database connection
     * @param productId       Product id
     * @param envKeys         List of environments to feed to
     * @param overwriteSentTo If true overwrite any existing "sent_to_novator" values.
     * @return Feed response information
     * @throws Exception
     */
    public NovatorFeedResponseVO sendProductFeed(Connection conn, String productId, List<String> envKeys, boolean overwriteSentTo) throws Exception {
        logger.info("NovatorProductFeedUtil sendProductFeed infos");
        NovatorFeedResponseVO responseVO = null;
        NovatorFeedResponseVO productFeedResponseVO = null;
        NovatorFeedResponseVO upsellFeedResponseVO = null;
        String environment = null;
        NovatorFeedRequestVO requestVO = null;
        StringBuffer response = new StringBuffer();
        ArrayList<String> sendToNovatorEnvKeys = new ArrayList(); 

        // Set the product feed vo
        NovatorFeedDataProductVO nfpvo = new NovatorFeedDataProductVO();
        nfpvo.setProductId(productId);
        
        // Obtain Novator environment keys if they don't exist
        if(envKeys == null || envKeys.size() == 0) {
           envKeys = feedUtil.getNovatorEnvironmentsAllowedAndChecked();
        }
        
        // Initialize response and set to true until proven otherwise
        responseVO = new NovatorFeedResponseVO();
        responseVO.setSuccess(true);

        // Send feeds for each environment
        requestVO = new NovatorFeedRequestVO();
        requestVO.setDataVO(nfpvo);
        for(Iterator<String> it = envKeys.iterator(); it.hasNext();) {
            try {
                environment = it.next();

                // Set the environment to feed to
                requestVO.setFeedToEnv(environment);

                logger.debug("Sending product feed..." + productId);

                // Product feed
                productFeedResponseVO = feedUtil.sendToNovator(conn, requestVO);

                logger.debug("Product feed sent. Result:" + productFeedResponseVO.isSuccess());

                // If Product feed was successful send the Upsell feed
                if(productFeedResponseVO.isSuccess()) {
                    try { 
                    	// Build list of upsell master IDs this product is a member of
                    	List upsellDetails = getUpsellDetailByProductID(conn, productId);
                    	List masterIDs = new ArrayList();
                    	Iterator detailI = upsellDetails.iterator();
                    	while (detailI.hasNext()) {
                    		UpsellDetailVO udvo = (UpsellDetailVO)detailI.next();
                    		if(udvo != null && udvo.getDetailMasterID() != null) {
                    			String masterid = udvo.getDetailMasterID();
                    			if (!masterIDs.contains(masterid)) {
                    				masterIDs.add(masterid);
                    				logger.debug("Sending upsell feed..." + productId);
                    				upsellFeedResponseVO = sendUpsellFeed(conn, masterid, envKeys);
                    				logger.debug(productId + " Upsell feed sent for upsell " + masterid + ". Result:" + upsellFeedResponseVO.isSuccess());
                    			}
                    		} else {
                    			// not an upsell product
                    		}                        
                    	}
                    } catch (Throwable t) {
                        logger.error(t);
                        upsellFeedResponseVO = new NovatorFeedResponseVO();
                        upsellFeedResponseVO.setSuccess(false);
                        upsellFeedResponseVO.setErrorString(t.getMessage());                        
                    }
                } 
            } catch(Throwable t) {
                logger.error(t);
                productFeedResponseVO = new NovatorFeedResponseVO();
                productFeedResponseVO.setSuccess(false);
                productFeedResponseVO.setErrorString(t.getMessage());
            } 

            // If response data already exists add newline
            if(response.length() > 0) {
                response.append("\n");
            } 
            response.append(environment + ":  ");              

            if(productFeedResponseVO.isSuccess()) {
                if(upsellFeedResponseVO == null || (upsellFeedResponseVO != null && upsellFeedResponseVO.isSuccess())) {
                    response.append("Feed was successful.");
                    sendToNovatorEnvKeys.add(environment);
                } else if(upsellFeedResponseVO != null && !upsellFeedResponseVO.isSuccess()) {
                    response.append("Product feed was successful.  Product Upsell feed failed:  " + upsellFeedResponseVO.getErrorString());
                    responseVO.setSuccess(false);
                }
            } else {
                response.append("Product feed failed:  " + productFeedResponseVO.getErrorString());                  
                responseVO.setSuccess(false);
            }            
        }
        
        // Update "sent_to_novator" values
        if(sendToNovatorEnvKeys.size() > 0){
            try{
                updateSentToNovator(conn, productId, sendToNovatorEnvKeys, overwriteSentTo);
            } catch(Throwable t) {
                logger.error(t);
                response.append("\nERROR:  Failed to update \"sent_to_novator\" values.");                  
                responseVO.setSuccess(false);            
            }
        }

        responseVO.setErrorString(response.toString());
        
        return responseVO;    
    }
    
    /**
     * This method provides peripheral processing when making a product unavailable, such as:
     * - updating any member upsell defaults
     * - updating any member upsell availability
     * - sending any member upsells and related products to Novator
     * 
     * This method assumes
     * 		1. the product has already been updated in the database to be unavailable
     * 		2. the product will be sent to Novator upon successful completion, which includes sending the upsells
     * @param conn
     * @param productID
     * @param envKeys
     * @return
     * @throws Exception 
     */
    public boolean updateUpsellsForDisabledProduct(Connection conn, String productID, List<String> envKeys) throws Exception{
    	List upsellWrappers;
    	
    	logger.debug("Processing upsells for changes for disabled product " + productID);
    	// build list of wrappers for the product - we'll use these objects to facilitate operations
    	try {
			upsellWrappers = buildUpsellWrappers(conn, productID);
		} catch (Exception e) {
			logger.error("Error disabling product " + productID + ". Upsells not changed. " + e);
			throw new Exception("Error disabling product " + productID + ". Upsells not changed. " + e);
		}
    	    	    	
    	// For each upsell see if the default sku and master availability has changed/apply and send those changes
    	Iterator wrapperI = upsellWrappers.iterator();
    	while (wrapperI.hasNext()) {
    		UpsellWrapper wrapper = (UpsellWrapper) wrapperI.next();
    		UpsellMasterVO master = wrapper.getUpsellMaster();
    		boolean upsellMasterChanged = false;
    		
    		// change upsell default if needed
    		upsellMasterChanged = updateUpsellDefault(wrapper, productID);
    	
    		// determine if upsell availability needs to change to be "unavailable"
        	if (master.isMasterStatus() && !wrapper.isAnyDetailsAvailable()) {        		
        		master.setMasterStatus(false);
        		upsellMasterChanged = true;
        		logger.info("Upsell " + master.getMasterID() + " disabled.");
        	}

    		// If default or availability has changed, save upsell changes to database and send changes to novator
        	if (upsellMasterChanged) {
        		if (!applyUpsellChanges(conn, productID, envKeys, master))
        			return false;
        	}
    	}
    	
    	return true;
    }

	private boolean applyUpsellChanges(Connection conn, String productID, List<String> envKeys, UpsellMasterVO master) {
		logger.debug("Updating Upsell " + master.getMasterID());
		
		try {
			updateMasterSKU(conn, master, envKeys);
		} catch (Exception e) {
			logger.error("Error disabling product "+ productID + ". Failed saving upsell " +  
					master.getMasterID() + " changes to database. " + e);
			return false;
		}
		logger.debug("Upsell updated successfully.");
		return true;
	}
	
    
    /**
     * 
     * @param wrapper
     * @param productID
     * @return true = the default changed
     */
    private boolean updateUpsellDefault(UpsellWrapper wrapper, String productID) {
    	// Changing the default requires changing 2 details, the current detail and the new one
    	boolean foundNewDefault = false;
    	List details = wrapper.getDetails();

    	if (!wrapper.getDefaultID().equals(productID)) {
    		// product is not the current default
    		
    		/* Default won't change unless this product becomes the new default, which happens only if
    		 * 1. all products for this upsell are unavailable
    		 * 2. this product is the first in the sequence
    		*/
    		UpsellDetailVO detail0 = (UpsellDetailVO)details.get(0);
    		if (detail0.getDetailID().equals(productID)) {   			
    			if (!wrapper.isAnyDetailsAvailable()) {
    				// make this product the default even though it was just made unavailable
    				wrapper.getDefaultDetail().setDefaultSkuFlag("N");
    				((UpsellDetailVO)details.get(0)).setDefaultSkuFlag("Y"); // it is already marked for send
    				foundNewDefault = true;
    				wrapper.setDefaultDetail(detail0);
    			}
    		}
    	}
    	else { // product is the default 
    		// change to the new default - gotta find who the new one should be first
    		UpsellDetailVO newDefault = null;
    		
    		// find where this product is in the list
    		int thisIndex = -1;
			for (int i=0; i<details.size(); i++) {
				if (((UpsellDetailVO)details.get(i)).getDetailID().equals(productID)) {
					thisIndex = i;
					break;
				}
			}

			// see if any less than this are available
			int newDefaultIndex = -1;
			for (int i=thisIndex-1; i>-1; i--) {
				if (((UpsellDetailVO)details.get(i)).isAvailable()) {
					newDefaultIndex = i;
					newDefault = (UpsellDetailVO)details.get(i);
					break;
				}
			}
			
			// if none below were available then search up
			if (newDefaultIndex == -1) {
				for (int i=thisIndex+1; i<details.size(); i++) {
					if (details.get(i) == null)
						break;
					if (((UpsellDetailVO)details.get(i)).isAvailable()) {
						newDefaultIndex = i;
						break;
					}				
				}
			}

			// See if we found a new available default - if not, use the first detail in the sequence 
			if (newDefaultIndex == -1) {
				// this is the default and the lowest detail and no others are available - therefore, default stays on this
				if (thisIndex == 0)
					foundNewDefault = false;
				else {
					newDefaultIndex = 0;
					foundNewDefault = true;
				}
			}
			else
				foundNewDefault = true;

    		// if we found a change condition then update the default setting and 
    		// add the new default to the list of details to send
    		if (foundNewDefault) {
    			newDefault = (UpsellDetailVO)details.get(newDefaultIndex);
        		logger.info("Default upsell detail changed from " + wrapper.getDefaultDetail().getDetailID() + " to " + newDefault.getDetailID());
    			wrapper.getDefaultDetail().setDefaultSkuFlag("N");
    			newDefault.setDefaultSkuFlag("Y");
    			wrapper.setDefaultDetail(newDefault);
    		}
    	}
    	
    	if (foundNewDefault) {
    		// reset details on the master so they get picked up when the upsell and details are saved
    		wrapper.getUpsellMaster().setUpsellDetailList(wrapper.getDetails());			
    	}
    	
    	return foundNewDefault;
	}

    /**
     * Build upsell wrappers for convenience of processing. This means finding all upsell details for this product and all 
     * upsell masters for those details
     * @param conn
     * @param productID
     * @param masters
     * @param details2send
     * @return
     * @throws Exception 
     */
	private List buildUpsellWrappers(Connection conn, String productID) throws Exception {
    	List upsellDetails;
    	List wrappers = new ArrayList();

    	// find all upsell details for this product
    	upsellDetails = getUpsellDetailByProductID(conn, productID);
    	
    	// get all upsell masters
    	Iterator detailI = upsellDetails.iterator();
    	while (detailI.hasNext()) {
    		UpsellMasterVO master;
    		String upsellMasterId = ((UpsellDetailVO)detailI.next()).getDetailMasterID();
    		master = getUpsellMaster(conn, upsellMasterId);
    		UpsellWrapper wrapper = new UpsellWrapper(master);
    		wrappers.add(wrapper);
    		
    		// now fully populate the wrapper with all of the subskus
    		List details4Master = master.getUpsellDetailList();
    		
        	// add all subskus to the wrapper
        	Iterator subskuDetailI = details4Master.iterator();
        	while (subskuDetailI.hasNext()) {
        		UpsellDetailVO detail;
        		detail = (UpsellDetailVO)subskuDetailI.next();
        		wrapper.addDetail(detail);
        	}    		
    	}
    	
    	return wrappers;
	}
    
    private class UpsellWrapper {
		String masterID;
    	boolean available, anyDetailsAvailable = false;
    	List details = new ArrayList();
    	UpsellDetailVO defaultDetail;
    	UpsellMasterVO upsellMaster;
    	
    	public UpsellWrapper(UpsellMasterVO master) {
    		masterID = master.getMasterID();
    		upsellMaster = master;
    		for (int i = 0; i < master.getUpsellDetailList().size(); i++) {
    			details.add(null);
    		}
    	}

		public UpsellMasterVO getUpsellMaster() {
			return upsellMaster;
		}

		public List getDetails() {
			return details;
		}

		public void setDefaultDetail(UpsellDetailVO defaultDetail) {
			this.defaultDetail = defaultDetail;
		}

		public UpsellDetailVO getDefaultDetail() {
			return defaultDetail;
		}

		public String getMasterID() {
			return masterID;
		}

		public String getDefaultID() {
			return defaultDetail.getDetailID();
		}

		public boolean isAvailable() {
			return available;
		}

		public void setAvailable(boolean available) {
			this.available = available;
		}
		
		public void addDetail(UpsellDetailVO upsellDetail) {
			String id = upsellDetail.getDetailID();
			boolean available = upsellDetail.isAvailable();
			if ("Y".equals(upsellDetail.getDefaultSkuFlag()))
				this.defaultDetail = upsellDetail;
			int seq = upsellDetail.getSequence();
			details.set(seq-1, upsellDetail); // 0-indexed
			
			anyDetailsAvailable = anyDetailsAvailable || available;
		}

		public boolean isAnyDetailsAvailable() {
			return anyDetailsAvailable;
		}

    }

	/**
     * Send upsell feed information to Novator
     * @param conn
     * @param upsellMasterId
     * @param envKeys
     * @return
     */
    public NovatorFeedResponseVO sendUpsellFeed(Connection conn, String upsellMasterId, List<String> envKeys) {
        NovatorFeedResponseVO responseVO = null;
        NovatorFeedDataUpsellDetailVO uvo = new NovatorFeedDataUpsellDetailVO();
        uvo.setUpsellMasterId(upsellMasterId);
        
        responseVO = feedUtil.sendToNovator(conn, uvo, envKeys);
        return responseVO;
    }

    
    /**
     * Returns an array of DetailUpsellVOs based on passed in product id, i.e. every upsell_detail for product = id
     * @param conn database connection
     * @return array of MasterUpsellVOs
     * @throws Exception
     **/
     public List getUpsellDetailByProductID(Connection conn, String id) throws Exception {
          ArrayList skus = new ArrayList();
          DataRequest dataRequest = new DataRequest();
          CachedResultSet rs = null;

          try
          {
              HashMap inputParams = new HashMap();
              inputParams.put("UPSELL_ID", id);
              
              /* build DataRequest object */
              dataRequest.setConnection(conn);
              dataRequest.setStatementID("PDB_GET_UPSELL_DETAIL_BY_ID");
              dataRequest.setInputParams(inputParams);
              
              /* execute the store prodcedure */
              DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance(); 
              rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
              while(rs.next())
              {
                  UpsellDetailVO upsellDetailVO = new UpsellDetailVO();
                  upsellDetailVO.setDetailMasterID(rs.getString(FeedConstants.PDB_UPSELLDETAIL_BYID_MASTER_ID));
                  upsellDetailVO.setDetailID(rs.getString(FeedConstants.PDB_UPSELLDETAIL_BYID_DETAIL_ID));
                  upsellDetailVO.setSequence(rs.getInt(FeedConstants.PDB_UPSELLDETAIL_BYID_SEQUENCE));
                  upsellDetailVO.setName(rs.getString(FeedConstants.PDB_UPSELLDETAIL_BYID_NAME));
                  upsellDetailVO.setDetailMasterName(rs.getString(FeedConstants.PDB_UPSELLDETAIL_BYID_MASTERNAME));
                  upsellDetailVO.setGbbNameOverrideFlag(StringUtils.equals("Y",rs.getString(FeedConstants.PDB_UPSELLDETAIL_BYID_GBB_NAME_OVERRIDE_FLAG)));
                  upsellDetailVO.setGbbNameOverrideText(rs.getString(FeedConstants.PDB_UPSELLDETAIL_BYID_GBB_NAME_OVERRIDE_TEXT));
                  upsellDetailVO.setGbbPriceOverrideFlag(StringUtils.equals("Y",rs.getString(FeedConstants.PDB_UPSELLDETAIL_BYID_GBB_PRICE_OVERRIDE_FLAG)));
                  upsellDetailVO.setGbbPriceOverrideText(rs.getString(FeedConstants.PDB_UPSELLDETAIL_BYID_GBB_PRICE_OVERRIDE_TEXT));
                  upsellDetailVO.setDefaultSkuFlag(rs.getString(FeedConstants.PDB_UPSELLDETAIL_BYID_DEFAULT_SKU_FLAG));
                  skus.add(upsellDetailVO);
              }
          }
          catch(Exception e)
          {
              String[] args = new String[1];
              args[0] = new String(e.getMessage());
              logger.error(e);
              throw e;
          }

          return skus;
      }
    
    
    
    
    /**
     * Retrive Upsell Master value object by upsell master id.
     * @param conn
     * @param sku
     * @return
     * @throws Exception
     */
    public UpsellMasterVO getUpsellMaster(Connection conn,String upsellMasterId) throws Exception
     {
         UpsellMasterVO upsellVO = null;
         DataRequest dataRequest = new DataRequest();
         CachedResultSet rs = null;

         try
         {
             HashMap inputParams = new HashMap();
             inputParams.put("IN_SKU_ID", upsellMasterId);
             
             /* build DataRequest object */
             dataRequest.setConnection(conn);
             dataRequest.setStatementID("PDB_GET_SKU_DETAILS");
             dataRequest.setInputParams(inputParams);
             
             /* execute the store prodcedure */
             DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance(); 
             HashMap outMap = (HashMap) dataAccessUtil.execute(dataRequest);
             rs = (CachedResultSet) outMap.get("OUT_SKU_CUR");

             if(rs.next())
             {
                 upsellVO = new UpsellMasterVO();
                 upsellVO.setMasterID(rs.getString(FeedConstants.PDB_UPSELLMASTER_MASTER_ID));
                 //upsellVO.setMasterName(FTDUtil.convertToXMLUnsafeCharacters(rs.getString(FeedConstants.PDB_UPSELLMASTER_MASTER_NAME)));
                 upsellVO.setMasterName(rs.getString(FeedConstants.PDB_UPSELLMASTER_MASTER_NAME));
                 //upsellVO.setMasterDescription(FTDUtil.convertToXMLUnsafeCharacters(rs.getString(FeedConstants.PDB_UPSELLMASTER_MASTER_DESCRIPTION)));
                 upsellVO.setMasterDescription(rs.getString(FeedConstants.PDB_UPSELLMASTER_MASTER_DESCRIPTION));
                 upsellVO.setSearchPriority(rs.getString(FeedConstants.PDB_UPSELLMASTER_SEARCH_PRIORITY ));
                 upsellVO.setCorporateSiteFlag(FieldUtils.convertStringToBoolean(rs.getString(FeedConstants.PDB_UPSELLMASTER_CORPORATE_SITE)));                
                 String status = rs.getString(FeedConstants.PDB_UPSELLMASTER_MASTER_STATUS);
                 if(status.equals(FeedConstants.SKU_AVAILABLE_KEY))
                 {
                     upsellVO.setMasterStatus(true);
                 }
                 else
                 {
                     upsellVO.setMasterStatus(false);
                 }
                 upsellVO.setGbbPopoverFlag(StringUtils.equals("Y",rs.getString(FeedConstants.PDB_UPSELLMASTER_GBB_POPOVER_FLAG)));
                 upsellVO.setGbbTitle(rs.getString(FeedConstants.PDB_UPSELLMASTER_GBB_TITLE));
                 
                 // Get the company list
                 rs = (CachedResultSet) outMap.get("OUT_COMPANY_CUR");
                 String[] companyList = new String[rs.getRowCount()];
                 int i = 0;
                 while(rs.next())
                 {   
                     companyList[i]= rs.getString(FeedConstants.OE_COMPANY_ID);
                     i++;
                 }
                 upsellVO.setCompanyList(companyList);

                 //populate the associated skus
                 List<UpsellDetailVO> upsellDetailList = getUpsellDetailList(conn,upsellVO);
                 upsellVO.setUpsellDetailList(upsellDetailList);
                 
                 //novator needs the product type of the first associate sku to be sent with master
                 if(upsellDetailList != null && upsellDetailList.size()>0) {
                     upsellVO.setMasterType(upsellDetailList.get(0).getType());
                 }

                 logger.debug("UpsellDAOImpl:getMasterSKU() Getting keywords");

                 // get the keywords
                 rs = (CachedResultSet) outMap.get("OUT_KEYWORD_CUR");
                 List keywords = new ArrayList();
                 while(rs.next())
                 {
                     SearchKeyVO keyVO = new SearchKeyVO();
                     keyVO.setSearchKey(rs.getString(FeedConstants.OE_PRODUCT_VALUE_ID));
                     keywords.add(keyVO);
                 }
                 upsellVO.setSearchKeyList(keywords);

                 logger.debug("ProductDAOImpl:getMasterSKU() Getting the product recipient search list");
                 
                 // Get the recipient search list
                 rs = (CachedResultSet) outMap.get("OUT_RECIPIENT_SEARCH_CUR");
                 String[] recipientList = new String[rs.getRowCount()];
                 i = 0;
                 while(rs.next())
                 {
                     recipientList[i] = rs.getString(FeedConstants.OE_PRODUCT_VALUE_ID);
                     i++;
                 }
                 upsellVO.setRecipientSearchList(recipientList);
                 
                 // Get the upsell source 
                 rs = (CachedResultSet) outMap.get("OUT_UPSELL_SOURCE_CUR");
                 List<String> upsellSourceList = new ArrayList();
                 while(rs.next())
                 {
                     String sourceCode = rs.getString(FeedConstants.OE_PRODUCT_VALUE_ID);
                     String orderSource = rs.getString(FeedConstants.OE_ORDER_SOURCE);
                     if (orderSource != null && orderSource.equalsIgnoreCase("I")) {
                         upsellSourceList.add(sourceCode);
                     }
                 }
                 upsellVO.setUpsellSourceList(upsellSourceList);
             }
         }
         catch(Exception e)
         {
             logger.error(e);
             throw e;
         }

         return upsellVO;
     }    
     
    /**
      * Returns an array of UpsellDetailVOs for the given master SKU
      * @param conn database connection
      * @return array of UpsellDetailVOs
      * @throws Exception
      **/
      private List getUpsellDetailList(Connection conn, UpsellMasterVO upsellVO)  throws Exception
       {
           ArrayList skus = new ArrayList();
           DataRequest dataRequest = new DataRequest();
           CachedResultSet rs = null;

           try
           {
               String sku = upsellVO.getMasterID();
               HashMap inputParams = new HashMap();
               inputParams.put("SKU", sku);
               
               /* build DataRequest object */
               dataRequest.setConnection(conn);
               dataRequest.setStatementID("PDB_GET_UPSELL_DETAIL_LIST");
               dataRequest.setInputParams(inputParams);
               
               /* execute the store prodcedure */
               DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance(); 
               rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
               while(rs.next())
               {
                   UpsellDetailVO upsellDetailVO = new UpsellDetailVO();
                   upsellDetailVO.setDetailID(rs.getString(FeedConstants.PDB_UPSELLDETAIL_DETAIL_ID));
                   upsellDetailVO.setSequence(rs.getInt(FeedConstants.PDB_UPSELLDETAIL_SEQUENCE));
                   //upsellDetailVO.setName(FTDUtil.convertToXMLUnsafeCharacters(rs.getString(FeedConstants.PDB_UPSELLDETAIL_NAME)));
                   upsellDetailVO.setName(rs.getString(FeedConstants.PDB_UPSELLDETAIL_NAME));
                   upsellDetailVO.setType(rs.getString(FeedConstants.PDB_UPSELLDETAIL_TYPE));
                   upsellDetailVO.setPrice(rs.getFloat(FeedConstants.PDB_UPSELLDETAIL_PRICE));                
                   upsellDetailVO.setNovatorID(rs.getString(FeedConstants.PDB_UPSELLDETAIL_NOVATOR_ID));
                   String status = rs.getString(FeedConstants.PDB_UPSELLDETAIL_AVAILABLE);
                   upsellDetailVO.setSentToNovatorProd(FieldUtils.convertStringToBoolean(rs.getString(FeedConstants.PDB_UPSELLDETAIL_SENT_TO_NOVATOR_PROD)));
                   upsellDetailVO.setSentToNovatorContent(FieldUtils.convertStringToBoolean(rs.getString(FeedConstants.PDB_UPSELLDETAIL_SENT_TO_NOVATOR_CONTENT)));
                   upsellDetailVO.setSentToNovatorUAT(FieldUtils.convertStringToBoolean(rs.getString(FeedConstants.PDB_UPSELLDETAIL_SENT_TO_NOVATOR_UAT)));
                   upsellDetailVO.setSentToNovatorTest(FieldUtils.convertStringToBoolean(rs.getString(FeedConstants.PDB_UPSELLDETAIL_SENT_TO_NOVATOR_TEST)));
                   if(status.equals(FeedConstants.PRODUCT_AVAILABLE_KEY))
                   {
                       upsellDetailVO.setAvailable(true);
                   }
                   else
                   {
                       upsellDetailVO.setAvailable(false);
                   }
                   String gbbSequence = rs.getString(FeedConstants.PDB_UPSELLDETAIL_GBB_SEQUENCE);
                   if (gbbSequence != null) {
                	   //set gbb sequence, name override and price override on detail
                	   upsellDetailVO.setGbbSequence(Integer.parseInt(gbbSequence));
                	   boolean gbbNameOverrideFlag = StringUtils.equals("Y",rs.getString(FeedConstants.PDB_UPSELLDETAIL_GBB_NAME_OVERRIDE_FLAG));
                	   String gbbNameOverrideText = rs.getString(FeedConstants.PDB_UPSELLDETAIL_GBB_NAME_OVERRIDE_TEXT);
                	   boolean gbbPriceOverrideFlag = StringUtils.equals("Y",rs.getString(FeedConstants.PDB_UPSELLDETAIL_GBB_PRICE_OVERRIDE_FLAG));
                	   String gbbPriceOverrideText = rs.getString(FeedConstants.PDB_UPSELLDETAIL_GBB_PRICE_OVERRIDE_TEXT);
                	   upsellDetailVO.setGbbNameOverrideFlag(gbbNameOverrideFlag);
                	   upsellDetailVO.setGbbNameOverrideText(gbbNameOverrideText);
                	   upsellDetailVO.setGbbPriceOverrideFlag(gbbPriceOverrideFlag);
                	   upsellDetailVO.setGbbPriceOverrideText(gbbPriceOverrideText);
                	   
                       if (gbbSequence.equals("1")) {
                           upsellVO.setGbbUpsellDetailId1(upsellDetailVO.getDetailID());
                           upsellVO.setGbbNameOverrideFlag1(gbbNameOverrideFlag);
                           upsellVO.setGbbNameOverrideText1(gbbNameOverrideText);
                           upsellVO.setGbbPriceOverrideFlag1(gbbPriceOverrideFlag);
                           upsellVO.setGbbPriceOverrideText1(gbbPriceOverrideText);
                       } else if (gbbSequence.equals("2")) {
                           upsellVO.setGbbUpsellDetailId2(upsellDetailVO.getDetailID());
                           upsellVO.setGbbNameOverrideFlag2(gbbNameOverrideFlag);
                           upsellVO.setGbbNameOverrideText2(gbbNameOverrideText);
                           upsellVO.setGbbPriceOverrideFlag2(gbbPriceOverrideFlag);
                           upsellVO.setGbbPriceOverrideText2(gbbPriceOverrideText);
                       } else if (gbbSequence.equals("3")) {
                           upsellVO.setGbbUpsellDetailId3(upsellDetailVO.getDetailID());
                           upsellVO.setGbbNameOverrideFlag3(gbbNameOverrideFlag);
                           upsellVO.setGbbNameOverrideText3(gbbNameOverrideText);
                           upsellVO.setGbbPriceOverrideFlag3(gbbPriceOverrideFlag);
                           upsellVO.setGbbPriceOverrideText3(gbbPriceOverrideText);
                       }
                   }
                   upsellDetailVO.setDefaultSkuFlag(rs.getString(FeedConstants.PDB_UPSELLDETAIL_DEFAULT_SKU_FLAG));

                   skus.add(upsellDetailVO);
               }//end while
           }
           catch(Exception e)
           {
               logger.error(e);
               throw e;
           }

           return skus;
      }     


    /**
     * Updates "sent_to_novator" database values for a given product id. 
     * @param conn            Database connection
     * @param productId       Product id
     * @param envKeys         List of environments that were successfully fed to Novator
     * @param overwriteSentTo If true overwrite any existing "sent_to_novator" values.
     * @throws Exception
     */
    public void updateSentToNovator(Connection conn, String productId, List<String> envKeys, boolean overwriteSentTo) throws Exception {        
        DataRequest dataRequest = new DataRequest();
        HashMap inputParams = new HashMap();
        String YES = "Y";
        String NO = "N";

        if(logger.isDebugEnabled()){
            logger.debug("CONTENT:  " + (envKeys.contains(NovatorFeedUtil.CONTENT)?YES:(overwriteSentTo?NO:null)));
            logger.debug("PRODUCTION:  " + (envKeys.contains(NovatorFeedUtil.PRODUCTION)?YES:(overwriteSentTo?NO:null)));
            logger.debug("TEST:  " + (envKeys.contains(NovatorFeedUtil.TEST)?YES:(overwriteSentTo?NO:null)));
            logger.debug("UAT:  " + (envKeys.contains(NovatorFeedUtil.UAT)?YES:(overwriteSentTo?NO:null)));
        }
        
        inputParams.put("IN_PRODUCT_ID", productId);
        inputParams.put("IN_SENT_TO_NOVATOR_CONTENT", (envKeys.contains(NovatorFeedUtil.CONTENT)?YES:(overwriteSentTo?NO:null)));
        inputParams.put("IN_SENT_TO_NOVATOR_PROD", (envKeys.contains(NovatorFeedUtil.PRODUCTION)?YES:(overwriteSentTo?NO:null)));
        inputParams.put("IN_SENT_TO_NOVATOR_TEST", (envKeys.contains(NovatorFeedUtil.TEST)?YES:(overwriteSentTo?NO:null)));
        inputParams.put("IN_SENT_TO_NOVATOR_UAT", (envKeys.contains(NovatorFeedUtil.UAT)?YES:(overwriteSentTo?NO:null)));
        
        /* build DataRequest object */
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("SP_UPDATE_PRODUCT_NOVATOR");
        dataRequest.setInputParams(inputParams);
        
        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        Map outputs = (Map)dataAccessUtil.execute(dataRequest);
        
        String status = (String)outputs.get("OUT_STATUS");
        if (status != null && status.equalsIgnoreCase("N")) {
            String message = (String)outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }
    }
    
    
    /**
     * Copied originally from PDB but with changes, most importantly only supports update to master and associated skus
     * @param conn
     * @param masterVO
     * @param novatorEnvKeys
     * @throws Exception
     */
    private void updateMasterSKU(Connection conn, UpsellMasterVO masterVO, List<String> novatorEnvKeys) 
    	throws Exception
    {
    	String dberror = "";
    	// update database for the upsell and associated skus
    	updateMasterSKUDB(conn, masterVO);
    	
    	String  myBuysFeedEnabled  = "N" ;  
    	try
    	{
    		ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
    		myBuysFeedEnabled  = configUtil.getFrpGlobalParm("MY_BUYS", "FEED_ENABLED_FLAG");

    	}
    	catch (Exception e)
    	{
    		logger.error("unable to get my buys feed from global param", e);
    	}

    	//Create new VO for Novator
    	UpsellMasterVO novatorMasterVO = new UpsellMasterVO();
    	novatorMasterVO.setCorporateSiteFlag(masterVO.isCorporateSiteFlag());              
    	//novatorMasterVO.setMasterDescription("<![CDATA[" + FTDUtil.convertNulls((masterVO.getMasterDescription())) + "]]>");
    	novatorMasterVO.setMasterDescription((String)FieldUtils.convertNulls((masterVO.getMasterDescription())));
    	novatorMasterVO.setCorporateSiteFlag(masterVO.isCorporateSiteFlag());
    	novatorMasterVO.setMasterStatus(masterVO.isMasterStatus());
    	novatorMasterVO.setMasterID(masterVO.getMasterID());
    	//novatorMasterVO.setMasterName( "<![CDATA[" + FTDUtil.convertNulls((masterVO.getMasterName())) + "]]>");
    	novatorMasterVO.setMasterName((String)FieldUtils.convertNulls((masterVO.getMasterName())));
    	novatorMasterVO.setMasterType(masterVO.getMasterType());
    	novatorMasterVO.setRecipientSearchList(masterVO.getRecipientSearchList());
    	novatorMasterVO.setSearchKeyList(masterVO.getSearchKeyList());
    	novatorMasterVO.setSearchPriority(masterVO.getSearchPriority());
    	novatorMasterVO.setCompanyList(masterVO.getCompanyList());
    	novatorMasterVO.setGbbPopoverFlag(masterVO.getGbbPopoverFlag());
    	novatorMasterVO.setGbbTitle(masterVO.getGbbTitle());
    	novatorMasterVO.setGbbUpsellDetailId1(masterVO.getGbbUpsellDetailId1());
    	novatorMasterVO.setGbbNameOverrideFlag1(masterVO.getGbbNameOverrideFlag1());
    	novatorMasterVO.setGbbNameOverrideText1(masterVO.getGbbNameOverrideText1());
    	novatorMasterVO.setGbbPriceOverrideFlag1(masterVO.getGbbPriceOverrideFlag1());
    	novatorMasterVO.setGbbPriceOverrideText1(masterVO.getGbbPriceOverrideText1());
    	novatorMasterVO.setGbbUpsellDetailId2(masterVO.getGbbUpsellDetailId2());
    	novatorMasterVO.setGbbNameOverrideFlag2(masterVO.getGbbNameOverrideFlag2());
    	novatorMasterVO.setGbbNameOverrideText2(masterVO.getGbbNameOverrideText2());
    	novatorMasterVO.setGbbPriceOverrideFlag2(masterVO.getGbbPriceOverrideFlag2());
    	novatorMasterVO.setGbbPriceOverrideText2(masterVO.getGbbPriceOverrideText2());
    	novatorMasterVO.setGbbUpsellDetailId3(masterVO.getGbbUpsellDetailId3());
    	novatorMasterVO.setGbbNameOverrideFlag3(masterVO.getGbbNameOverrideFlag3());
    	novatorMasterVO.setGbbNameOverrideText3(masterVO.getGbbNameOverrideText3());
    	novatorMasterVO.setGbbPriceOverrideFlag3(masterVO.getGbbPriceOverrideFlag3());
    	novatorMasterVO.setGbbPriceOverrideText3(masterVO.getGbbPriceOverrideText3());
    	novatorMasterVO.setWebsites(masterVO.getWebsites());


    	//loop through upsell detail list
    	List upsellList = masterVO.getUpsellDetailList();
    	List novatorList = new ArrayList();
    	if (upsellList != null){
    		for(int i=0;i<upsellList.size();i++){
    			UpsellDetailVO upsellDetailVO= (UpsellDetailVO)upsellList.get(i);
    			UpsellDetailVO novatorDetailVO= new UpsellDetailVO();
    			novatorDetailVO.setAvailable(upsellDetailVO.isAvailable());
    			novatorDetailVO.setSentToNovatorContent(upsellDetailVO.isSentToNovatorContent());
    			novatorDetailVO.setSentToNovatorProd(upsellDetailVO.isSentToNovatorProd());
    			novatorDetailVO.setSentToNovatorTest(upsellDetailVO.isSentToNovatorTest());
    			novatorDetailVO.setSentToNovatorUAT(upsellDetailVO.isSentToNovatorUAT());
    			novatorDetailVO.setAvailable(upsellDetailVO.isAvailable());
    			novatorDetailVO.setDetailID(upsellDetailVO.getDetailID());
    			novatorDetailVO.setDetailMasterID(upsellDetailVO.getDetailMasterID());
    			//novatorDetailVO.setDetailMasterName("<![CDATA[" + FTDUtil.convertNulls((upsellDetailVO.getDetailMasterName())) + "]]>");                    
    			novatorDetailVO.setDetailMasterName((String)FieldUtils.convertNulls((upsellDetailVO.getDetailMasterName())));
    			//novatorDetailVO.setName("<![CDATA[" + FTDUtil.convertNulls((upsellDetailVO.getName())) + "]]>");
    			novatorDetailVO.setName((String)FieldUtils.convertNulls((upsellDetailVO.getName())));
    			novatorDetailVO.setNovatorID(upsellDetailVO.getNovatorID());
    			novatorDetailVO.setPrice(upsellDetailVO.getPrice());
    			novatorDetailVO.setSequence(upsellDetailVO.getSequence());
    			novatorDetailVO.setType(upsellDetailVO.getType());
    			novatorDetailVO.setDefaultSkuFlag(upsellDetailVO.getDefaultSkuFlag());

    			//determine status value
    			String status = "";

    			if ( (!masterVO.isMasterStatus()) && StringUtils.equalsIgnoreCase(upsellDetailVO.getDefaultSkuFlag(), "Y") &&  StringUtils.equalsIgnoreCase(myBuysFeedEnabled, "Y") )
    			{
    				logger.debug("calling MyBuys feed method");
    				try{
    					sendMyBuys(upsellDetailVO.getDetailID());
    					logger.debug("Succesfull in sending MyBuys message() for order_processing for product id: "+ upsellDetailVO.getDetailID());
    				} catch (Exception e)
    				{
    					dberror = "Error in sending MyBuys message() for product id "+ e;
    					logger.error(dberror);
    				}
    			}

    			novatorList.add(novatorDetailVO);
    		}//end for
    		novatorMasterVO.setUpsellDetailList(novatorList);
    	}//end upsell list not null


    	//send to novator
    	if(novatorEnvKeys != null && novatorEnvKeys.size() > 0) {
    		NovatorFeedResponseVO resVO = sendUpsellFeed(conn, novatorMasterVO.getMasterID(), novatorEnvKeys);
            
    		String errorString = "";
            if(!resVO.isSuccess()) {
                errorString = resVO.getErrorString();
            }
            
            if (!errorString.equals("")){
                logger.info("Received error in feed:" + errorString);
                throw new RuntimeException(dberror + "\n" + errorString);
            }
    	}
    	if (!dberror.equals(""))
            throw new RuntimeException(dberror);
    }        

    /**
     * Update the passed in Master SKU and associate skus, no other associated objects, e.g. keywords or companies.
     * Associated skus are deleted then re-inserted.
     * @param conn database connection
     * @return void
     * @throws PDBApplicationException, PDBSystemException
     **/
     private void updateMasterSKUDB(Connection conn, UpsellMasterVO masterVO)
          throws Exception {
          DataRequest dataRequest = new DataRequest();

          try
          {
              HashMap inputParams = new HashMap();
              DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance(); 
              
              // delete all details - they will be inserted later. This is inplace of updating them.
              dataRequest.reset();
              dataRequest.setConnection(conn);
              inputParams.put("MASTER_ID", masterVO.getMasterID());
              dataRequest.setInputParams(inputParams);
              dataRequest.setStatementID("PDB_REMOVE_UPSELL_DETAIL");
              dataAccessUtil.execute(dataRequest);

              //determine status value
              String status = "";
              if (masterVO.isMasterStatus()) {
                  status = "Y";
              }
              else {
                  status = "N";
              }
              
              inputParams.put("MASTER_ID", masterVO.getMasterID());
              inputParams.put("NAME", masterVO.getMasterName());
              inputParams.put("DESCRIPTION", masterVO.getMasterDescription());
              inputParams.put("STATUS", status);
              inputParams.put("PRIORITY", masterVO.getSearchPriority());
              inputParams.put("CORP_FLAG", FieldUtils.convertBooleanToString(masterVO.isCorporateSiteFlag()));
              inputParams.put("GBB_POPOVER_FLAG", FieldUtils.convertBooleanToString(masterVO.getGbbPopoverFlag()));
              inputParams.put("GBB_TITLE_TXT", masterVO.getGbbTitle());

              //  build DataRequest object //
              dataRequest.reset();
              dataRequest.setConnection(conn);
              dataRequest.setStatementID("PDB_INSERT_UPSELL_MASTER");
              dataRequest.setInputParams(inputParams);
              
              // execute the store procedure //
              dataAccessUtil.execute(dataRequest);
              
              //insert associate skus
              List detailList = masterVO.getUpsellDetailList();
              for(int i = 0; i < detailList.size(); i++){
                  UpsellDetailVO detailVO = (UpsellDetailVO)detailList.get(i);
                  
                  inputParams = new HashMap();
                  inputParams.put("DETAIL_ID", detailVO.getDetailID());
                  inputParams.put("MASTER_ID", masterVO.getMasterID());
                  inputParams.put("SEQUENCE", new BigDecimal(detailVO.getSequence()));
                  inputParams.put("NAME", detailVO.getName());
                  inputParams.put("GBB_NAME_OVERRIDE_FLAG", FieldUtils.convertBooleanToString(detailVO.getGbbNameOverrideFlag()));
                  inputParams.put("GBB_NAME_OVERRIDE_TXT", detailVO.getGbbNameOverrideText());
                  inputParams.put("GBB_PRICE_OVERRIDE_FLAG", FieldUtils.convertBooleanToString(detailVO.getGbbPriceOverrideFlag()));
                  inputParams.put("GBB_PRICE_OVERRIDE_TXT", detailVO.getGbbPriceOverrideText());
                  inputParams.put("GBB_SEQUENCE", new BigDecimal(detailVO.getGbbSequence()));
                  inputParams.put("DEFAULT_SKU_FLAG", detailVO.getDefaultSkuFlag());
                  
                  // build DataRequest object //
                  dataRequest.reset();
                  dataRequest.setConnection(conn);
                  dataRequest.setStatementID("PDB_INSERT_UPSELL_DETAIL");
                  dataRequest.setInputParams(inputParams);
                  dataAccessUtil.execute(dataRequest);
                  
              }   //end assoc sku loop

          }
          catch(Exception e)
          {
              logger.error(e);
              throw e;
          }
      }
     
     /**
      * calls the MDB for sending JMS message to myBuys queue
      * @param String productID of object for whom messages need to be sent
      * @return void
      */
       
     public void sendMyBuys(String productId) throws Exception
     {
          MessageToken messageToken = new MessageToken();
          messageToken.setMessage(productId);
          messageToken.setStatus("MY BUYS");
          Dispatcher dispatcher = Dispatcher.getInstance();
          dispatcher.dispatchTextMessage(new InitialContext(),messageToken);         
     }

}
