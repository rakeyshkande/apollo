package com.ftd.osp.utilities.feed;

import com.ftd.osp.utilities.feed.vo.INovatorFeedDataVO;
import com.ftd.osp.utilities.feed.vo.NovatorFeedDataAddOnVO;
import com.ftd.osp.utilities.feed.vo.NovatorFeedDataPreTransformedXmlVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.JAXPUtil;

import java.sql.Connection;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * Pass through Transformer Implementation.
 * Expects the VO to be {@link com.ftd.osp.utilities.feed.vo.NovatorFeedDataPreTransformedXmlVO}
 */
public class NovatorFeedTransformerPreTransformedXML
  extends NovatorFeedTransformerBase
{
  private static final Logger logger = new Logger(NovatorFeedTransformerPreTransformedXML.class.getName());


  public String objectToXML(INovatorFeedDataVO dataVO)
    throws Exception
  {
    NovatorFeedDataPreTransformedXmlVO novatorFeedDataPreTransformedXmlVO = (NovatorFeedDataPreTransformedXmlVO) dataVO;
    return novatorFeedDataPreTransformedXmlVO.getFeedXML();
  }

  public String objectToXML(Connection conn, INovatorFeedDataVO dataVO)
    throws Exception
  {
    return objectToXML(dataVO);
  }
}
