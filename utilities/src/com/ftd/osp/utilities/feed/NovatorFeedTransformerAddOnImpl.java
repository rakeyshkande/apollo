package com.ftd.osp.utilities.feed;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.feed.vo.INovatorFeedDataVO;
import com.ftd.osp.utilities.feed.vo.NovatorFeedDataAddOnVO;
import com.ftd.osp.utilities.xml.JAXPUtil;

import java.sql.Connection;

import org.w3c.dom.Document;
import org.w3c.dom.Element;


/**
 * Novator AddOn feed implementation.
 */ 
public class NovatorFeedTransformerAddOnImpl extends NovatorFeedTransformerBase{
    private static final Logger logger = new Logger("com.ftd.osp.utilities.feed.NovatorFeedTransformerAddOnImpl");    

    private static final String ADD_ON = "add_on";
    private static final String ADD_ONS = "add_ons";
    private static final String ADD_ON_ID = "add_on_id";
    private static final String ADD_ON_DESCRIPTION = "add_on_description";
    private static final String ADD_ON_TYPE_ID = "add_on_type_id";
    private static final String ADD_ON_TYPE_DESCRIPTION = "add_on_type_description";
    private static final String PRICE = "price";
    private static final String ADD_ON_TEXT = "add_on_text";
    private static final String UNSPSC = "unspsc";
    private static final String AVAILABLE_FLAG = "available_flag";
    private static final String DEFAULT_PER_TYPE_FLAG = "default_per_type_flag";
    private static final String DISPLAY_PRICE_FLAG = "display_price_flag";
    private static final String DELIVERY_TYPE = "delivery_type";
    private static final String PQUAD_ACCESSORY_ID = "pquad_accessory_id";// DI-4: Add-on Dashboard Enhancements: Added 2 new attributes to add-on : PQUAD_ACCESSORY_ID, FTD_WEST_ADDON.
    private static final String FTDWEST_ADD_ON = "ftdwest_add_on";// DI-4: Add-on Dashboard Enhancements: Added 2 new attributes to add-on : PQUAD_ACCESSORY_ID, IS_FTD_WEST_ADDON.

    public String objectToXML(INovatorFeedDataVO dataVO) throws Exception{
        Element node = null;
        NovatorFeedDataAddOnVO fsDataVO = (NovatorFeedDataAddOnVO)dataVO;
       
        // Convert the VO to XML
        
        /* #552 - Flexible Fullfillment - Add-on Dash board and Maintenance changes 
        added the  delivery_type in the xml which is being sent to novator.
        Commented the code as per requirement.
        */
        Document doc = JAXPUtil.createDocument();
        Element root = doc.createElement(ADD_ONS);
        doc.appendChild(root);
        Element subRoot = doc.createElement(ADD_ON);
        root.appendChild(subRoot);
        
        node = JAXPUtil.buildSimpleXmlNode(doc, ADD_ON_ID, fsDataVO.getAdd_on_id());
        subRoot.appendChild(node);
        node = JAXPUtil.buildSimpleXmlNodeCDATA(doc, ADD_ON_DESCRIPTION, fsDataVO.getAdd_on_description());
        subRoot.appendChild(node);
        node = JAXPUtil.buildSimpleXmlNode(doc, ADD_ON_TYPE_ID, fsDataVO.getAdd_on_type_id());
        subRoot.appendChild(node);
        node = JAXPUtil.buildSimpleXmlNode(doc, ADD_ON_TYPE_DESCRIPTION, fsDataVO.getAdd_on_type_description());
        subRoot.appendChild(node);
        node = JAXPUtil.buildSimpleXmlNode(doc, PRICE, fsDataVO.getPrice());
        subRoot.appendChild(node);
        node = JAXPUtil.buildSimpleXmlNodeCDATA(doc, ADD_ON_TEXT, fsDataVO.getAdd_on_text());
        subRoot.appendChild(node);
        node = JAXPUtil.buildSimpleXmlNode(doc, UNSPSC, fsDataVO.getUnspsc());
        subRoot.appendChild(node);
        node = JAXPUtil.buildSimpleXmlNode(doc, AVAILABLE_FLAG, fsDataVO.getAvailable_flag());
        subRoot.appendChild(node);
        node = JAXPUtil.buildSimpleXmlNode(doc, DEFAULT_PER_TYPE_FLAG, fsDataVO.getDefault_per_type_flag());
        subRoot.appendChild(node);
        node = JAXPUtil.buildSimpleXmlNode(doc, DISPLAY_PRICE_FLAG, fsDataVO.getDisplay_price_flag());
        subRoot.appendChild(node);
        node = JAXPUtil.buildSimpleXmlNode(doc, DELIVERY_TYPE, fsDataVO.getDelivery_type());
        subRoot.appendChild(node);
        /*
         *  DI-4: Add-on Dashboard Enhancements: Added 2 new attributes to add-on : PQUAD_ACCESSORY_ID, FTD_WEST_ADDON.
         *  Adding the newly added attributes to the novator feed xml.
         */
        node = JAXPUtil.buildSimpleXmlNode(doc, PQUAD_ACCESSORY_ID, fsDataVO.getsPquadAccsryId());
        subRoot.appendChild(node);
        node = JAXPUtil.buildSimpleXmlNode(doc, FTDWEST_ADD_ON, fsDataVO.isFtdWestAddon());
        subRoot.appendChild(node);
        
        String xml = JAXPUtil.toStringNoFormat(doc);

        if(logger.isDebugEnabled()){
            logger.debug("Generated the following feed XML:\n" + xml);
        }
        return xml;
    }
    
    public String objectToXML(Connection conn, INovatorFeedDataVO dataVO) throws Exception{
        return objectToXML(dataVO);
    }
}
