package com.ftd.osp.utilities.feed;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.SendUpdateFeed;
import com.ftd.osp.utilities.feed.vo.INovatorFeedDataVO;
import com.ftd.osp.utilities.feed.vo.NovatorFeedRequestVO;
import com.ftd.osp.utilities.feed.vo.NovatorFeedResponseVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.JAXPUtil;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;


/**
 * Utility class for Novator feeds.
 * 1.  Obtains the Novator environment configuration which is typically used by the UI
 * 2.  Sends Novator feeds
 */ 
public class NovatorFeedUtil {
    private static final Logger logger = new Logger("com.ftd.osp.utilities.feed.NovatorFeedUtil");    

    // Novator environments
    public static final String CONTENT = "content";
    public static final String PRODUCTION = "production";
    public static final String TEST = "test";
    public static final String UAT = "uat";
    
    public static final String NOVATOR_UPDATE_SETUP = "novator_update_setup";
    // Context for Novator global parameters
    private static final String NOVATOR_CONFIG = "NOVATOR_CONFIG";

    // Global parameters for Novator environment IP and ports
    private static final String NOVATOR_PRODUCTION_FEED_IP = "production_feed_ip";
    private static final String NOVATOR_PRODUCTION_FEED_PORT = "production_feed_port";
    private static final String NOVATOR_CONTENT_FEED_IP = "content_feed_ip";
    private static final String NOVATOR_CONTENT_FEED_PORT = "content_feed_port";
    private static final String NOVATOR_TEST_FEED_IP = "test_feed_ip";
    private static final String NOVATOR_TEST_FEED_PORT = "test_feed_port";
    private static final String NOVATOR_UAT_FEED_IP = "uat_feed_ip";
    private static final String NOVATOR_UAT_FEED_PORT = "uat_feed_port";
    private static final String NOVATOR_FEED_TIMEOUT = "feed_timeout";
    
    // Global parameters for which Novator environments are enabled 
    public static final String NOVATOR_CONTENT_FEED_ALLOWED  = "content_feed_allowed";
    public static final String NOVATOR_PRODUCTION_FEED_ALLOWED  = "production_feed_allowed";
    public static final String NOVATOR_TEST_FEED_ALLOWED  = "test_feed_allowed";
    public static final String NOVATOR_UAT_FEED_ALLOWED  = "uat_feed_allowed";
    public static final String NOVATOR_CONTENT_FEED_CHECKED  = "content_feed_checked";
    public static final String NOVATOR_PRODUCTION_FEED_CHECKED  = "production_feed_checked";
    public static final String NOVATOR_TEST_FEED_CHECKED  = "test_feed_checked";
    public static final String NOVATOR_UAT_FEED_CHECKED  = "uat_feed_checked";
    
    private static final String COMMON_VAL_YES = "YES";

    /**
     * Send feed information to Novator.
     * @param requestVO
     * @return Feed information
     * @throws Exception
     */
    public NovatorFeedResponseVO sendToNovator(NovatorFeedRequestVO requestVO){
        return sendToNovator(null, requestVO);
    }
    
    public NovatorFeedResponseVO sendToNovator(Connection conn, NovatorFeedRequestVO requestVO){
        logger.debug("NovatorFeedUtil sendToNovator");
        NovatorFeedResponseVO responseVO = null;

        // Send information to Novator
        try{
            String response = null;
            String xml = null;
            
            xml = objectToXML(conn, requestVO);

            // Send to Novator based on the environment
            if (requestVO.getFeedToEnv().equals(CONTENT)) {
                response = sendNovatorXML(xml, NOVATOR_CONTENT_FEED_IP, NOVATOR_CONTENT_FEED_PORT);
            }
            if (requestVO.getFeedToEnv().equals(PRODUCTION)) {
                response = sendNovatorXML(xml, NOVATOR_PRODUCTION_FEED_IP, NOVATOR_PRODUCTION_FEED_PORT);
            }
            if (requestVO.getFeedToEnv().equals(TEST)) {
                response = sendNovatorXML(xml, NOVATOR_TEST_FEED_IP, NOVATOR_TEST_FEED_PORT);
            }
            if (requestVO.getFeedToEnv().equals(UAT)) {
                response = sendNovatorXML(xml, NOVATOR_UAT_FEED_IP, NOVATOR_UAT_FEED_PORT);
            }

            responseVO = parseResponse(response);
        } catch (Throwable t){
            logger.error(t);
            responseVO = new NovatorFeedResponseVO();
            responseVO.setSuccess(false);
            responseVO.setErrorString("An error occurred while sending the feed:\n" + t.toString());
            responseVO.setIsTransmitException(true);
            responseVO.setTransmitThrowable(t);
        }

        return responseVO;
    }    
    
    /**
     * Send feed information to Novator. If the environment key is sent in a list then feed to the list of
     * environment; otherwise send to the environment configured in the global parameters.
     * @param conn
     * @param dataVO
     * @param envKeys
     * @return Feed information
     * @throws Exception
     */
    public NovatorFeedResponseVO sendToNovator(Connection conn, INovatorFeedDataVO dataVO, List<String> envKeys){
        logger.debug("NovatorFeedUtil sendToNovator(Connection, INovatorFeedDataVO, List<String>)");
        NovatorFeedResponseVO resVO = new NovatorFeedResponseVO();
        Document novatorConfigDoc = null;
        NovatorFeedRequestVO reqVO = null;

        String response = "";
        String envKey = null;
        String successEnv = "";
        String failureEnv = "";

        try{
            // If the input envKeys list is empty then retrieve environment from NOVATOR_CONFIG global parm.
            resVO.setSuccess(true);
            if(envKeys == null || envKeys.size() == 0) {
                logger.debug("envKeys is null. Retrieving from global parm...");
                envKeys = getNovatorEnvironmentsAllowedAndChecked();                
            }
            
            reqVO = new NovatorFeedRequestVO();
            reqVO.setDataVO(dataVO);
            
            try {
            
                for (int i = 0; i<envKeys.size(); i++) {
                    envKey = envKeys.get(i);
                    logger.debug("Sending to:" + envKey);                    
                    reqVO.setFeedToEnv(envKey);           
                    resVO = sendToNovator(conn, reqVO);
                    
                    if(resVO.isSuccess()) {
                        successEnv = (successEnv.equals("")? "" : successEnv + ",") + envKey;
                    } else {
                        failureEnv = envKey;
                        throw new Exception(resVO.getErrorString());
                    }
                    
                }
            } catch (Exception e) {
                logger.error(e);
                if(successEnv.length()>0) {
                    response = "Update successfully sent to: " + successEnv + ". ";
                }
                response += "Update failed with " + e.getMessage() + "(" + failureEnv + ")";
            }
            
        } catch (Exception e) {
            logger.error(e);
            resVO.setSuccess(false); 
            response = e.getMessage();
        }
        if(resVO.isSuccess()) {
            resVO.setErrorString("Update successfully sent to: " + successEnv);
        } else {
            resVO.setErrorString(response);
        }
        logger.info("Send to Novator response:" + resVO.getErrorString());
        return resVO;
    }    
    
    

    /**
     * Convert the data object to XML.
     * @param conn
     * @param requestVO
     * @return Feed information
     * @throws Exception
     */
    private String objectToXML(Connection conn, NovatorFeedRequestVO requestVO)throws Exception{
        INovatorFeedTransformer transformer = NovatorFeedTransformerFactory.getTransformerImpl(requestVO.getDataVO()); 
        String xml = transformer.objectToXML(conn, requestVO.getDataVO());
        return xml;
    }
    
    /**
     * Send the feed information to Novator
     * @param xmlString Information to transmit
     * @param ipParamName IP configuration parameter
     * @param portParamName Port configuratin parameter
     * @return Novator response
     * @throws Exception
     */
    private String sendNovatorXML(String xmlString, String ipParamName, String portParamName) throws Exception {
        ConfigurationUtil cu = ConfigurationUtil.getInstance();
        String server = cu.getFrpGlobalParm(NOVATOR_CONFIG,ipParamName);
        String port = cu.getFrpGlobalParm(NOVATOR_CONFIG,portParamName);
        Integer portInt = Integer.parseInt(port);
        String timeout = cu.getFrpGlobalParm(NOVATOR_CONFIG,NOVATOR_FEED_TIMEOUT);
        Integer timeoutInt = Integer.parseInt(timeout);
        String response = SendUpdateFeed.send(server, portInt, timeoutInt, xmlString);
        
        return response;
    }

    /**
     * Parses and evaluates the feed response from Novator.
     * @param xmlResponse
     * @return Interpreted response information
     * @throws Exception
     */
    protected NovatorFeedResponseVO parseResponse(String xmlResponse) throws Exception {
        String originalXMLResponse = xmlResponse;
        NovatorFeedResponseVO responseVO = new NovatorFeedResponseVO();
        
        if(logger.isDebugEnabled())
            logger.debug("Novator response: " + xmlResponse);
        
        
        // Sometimes the response comes back with CON on the end.  Remove CON if present
        if(xmlResponse.endsWith("CON")) {
            xmlResponse = xmlResponse.substring(0,(xmlResponse.length() - 3));
        }

        try {
            Document doc = JAXPUtil.parseDocument(xmlResponse);

            // Attempt to obtain update node
            Element updateElement = JAXPUtil.selectSingleNode(doc, "//update");
            if(updateElement == null)
                updateElement = JAXPUtil.selectSingleNode(doc, "//create");
            if(updateElement == null)
                updateElement = JAXPUtil.selectSingleNode(doc, "//remove");
            

            if (updateElement == null ) {
                if(logger.isDebugEnabled())
                    logger.debug("No update element exists within the response.");

                // update was not found.  Look for a validation error
                Element errorElement = JAXPUtil.selectSingleNode(doc, "//validationError");
                if (errorElement ==  null ) {
                    // No validation error found...don't know what this message means
                    responseVO.setSuccess(false);
                    responseVO.setErrorString("Unable to parse the following Novator response: " + originalXMLResponse);
                } else {
                    // Obtain the validation error description
                    String description = "";
                    Element errorDescriptionElement = JAXPUtil.selectSingleNode(doc, "//validationError/error/description");
                    if(errorDescriptionElement != null) {
                        description = JAXPUtil.getTextValue(errorDescriptionElement);
                    }
                    responseVO.setSuccess(false);
                    responseVO.setErrorString("Novator validation error: " + description);
                }           
            } else {
                // Obtain the status of the update
                String status = "";
                Element statusElement = JAXPUtil.selectSingleNode(doc, "//update/status");
                if(statusElement == null)
                    statusElement = JAXPUtil.selectSingleNode(doc, "//create/status");
                if(statusElement == null)
                    statusElement = JAXPUtil.selectSingleNode(doc, "//remove/status");
                
                if(statusElement != null)
                    status = JAXPUtil.getTextValue(statusElement);

                if (status.equals("success")) {            
                    responseVO.setSuccess(true);
                } else{
                    responseVO.setSuccess(false);
                    responseVO.setErrorString("Novator feed was unsuccessful: " + status);
                }
            }        
        } catch (Throwable t) {
            // Could not parse Novator response or an error occurred           
            logger.warn(t);

            responseVO.setSuccess(false);
            responseVO.setErrorString("Novator feed error.  " + t.getMessage());
        }
        
        return responseVO;
    }

    /**
     * Creates an XML document containing Novator feed configuration values.
     * Values are typically used for display within a UI.
     * @return Document containing Novator feed configuration values
     * @throws Exception
     */
    public Document getNovatorEnvironmentSetup() throws Exception
    {
        Document doc = null;

        doc = JAXPUtil.createDocument();
        Element root = doc.createElement(NOVATOR_UPDATE_SETUP);
        doc.appendChild(root);

        addNovatorEnvironmentNode(doc, root, NOVATOR_CONTENT_FEED_ALLOWED);
        addNovatorEnvironmentNode(doc, root, NOVATOR_TEST_FEED_ALLOWED);
        addNovatorEnvironmentNode(doc, root, NOVATOR_PRODUCTION_FEED_ALLOWED);
        addNovatorEnvironmentNode(doc, root, NOVATOR_UAT_FEED_ALLOWED);

        addNovatorEnvironmentNode(doc, root, NOVATOR_CONTENT_FEED_CHECKED);
        addNovatorEnvironmentNode(doc, root, NOVATOR_TEST_FEED_CHECKED);
        addNovatorEnvironmentNode(doc, root, NOVATOR_PRODUCTION_FEED_CHECKED);
        addNovatorEnvironmentNode(doc, root, NOVATOR_UAT_FEED_CHECKED);
        
        return doc;
    }

    /**
     * Returns a list of Novator environments that are allowed and checked.
     * @return ArrayList containing a list of Novator environments
     * @throws Exception
     */
    public ArrayList getNovatorEnvironmentsAllowedAndChecked() throws Exception
    {
        ArrayList envKeys = new ArrayList();
        
        if(isNovatorEnvironmentAllowed(NovatorFeedUtil.PRODUCTION) && isNovatorEnvironmentChecked(NovatorFeedUtil.PRODUCTION)) {
            logger.debug("Prod is added.");
            envKeys.add(NovatorFeedUtil.PRODUCTION);
        }

        if(isNovatorEnvironmentAllowed(NovatorFeedUtil.CONTENT) && isNovatorEnvironmentChecked(NovatorFeedUtil.CONTENT)) {
            logger.debug("Content is added.");
            envKeys.add(NovatorFeedUtil.CONTENT);
        }
        
        if(isNovatorEnvironmentAllowed(NovatorFeedUtil.TEST) && isNovatorEnvironmentChecked(NovatorFeedUtil.TEST)) {
            logger.debug("Test is added.");
            envKeys.add(NovatorFeedUtil.TEST);
        }
        
        if(isNovatorEnvironmentAllowed(NovatorFeedUtil.UAT) && isNovatorEnvironmentChecked(NovatorFeedUtil.UAT)) {
            logger.debug("UAT is added.");
            envKeys.add(NovatorFeedUtil.UAT);
        }
        
        return envKeys;
    }

    /**
     * Create a Novator environment node and add it to the parent
     * node based on the name of the global parameter passed in.
     * @param Document Novator configuration document
     * @param Element Parent node
     * @param String Name of the global configuration parameter
     * @throws Exception
     */
    private void addNovatorEnvironmentNode(Document doc, Element parent, String name) throws Exception
    {
        String allowed = ConfigurationUtil.getInstance().getFrpGlobalParm(NOVATOR_CONFIG,name);
        Element node = JAXPUtil.buildSimpleXmlNode(doc, name, allowed);  
        parent.appendChild(node);
    }

  /**
   * Create a Novator environment node and add it to the parent
   * node based on the name of the global parameter passed in.
   * @param Document Novator configuration document
   * @param Element Parent node
   * @param String Name of the global configuration parameter
   * @throws Exception
   */
  private void addNovatorEnvironment(HashMap map, String name) throws Exception
  {
      String value = ConfigurationUtil.getInstance().getFrpGlobalParm(NOVATOR_CONFIG,name);
      map.put(name, value);
  }

  /**
   * Returns whether the Novator environment is allowed
   * @param name    Novator environment name
   * @return        true if the Novator environment is allowed and false if it is not
   * @throws Exception
   */
  public boolean isNovatorEnvironmentAllowed(String name) throws Exception
  {
      String value = null;
      if(name.equals(CONTENT))
      {
          value = ConfigurationUtil.getInstance().getFrpGlobalParm(NOVATOR_CONFIG,NOVATOR_CONTENT_FEED_ALLOWED);        
      }
      else if(name.equals(PRODUCTION))
      {
          value = ConfigurationUtil.getInstance().getFrpGlobalParm(NOVATOR_CONFIG,NOVATOR_PRODUCTION_FEED_ALLOWED);        
      }
      else if(name.equals(TEST))
      {
          value = ConfigurationUtil.getInstance().getFrpGlobalParm(NOVATOR_CONFIG,NOVATOR_TEST_FEED_ALLOWED);        
      }
      else if(name.equals(UAT))
      {
          value = ConfigurationUtil.getInstance().getFrpGlobalParm(NOVATOR_CONFIG,NOVATOR_UAT_FEED_ALLOWED);        
      }
      

      if(StringUtils.isNotBlank(value) && value.equalsIgnoreCase(COMMON_VAL_YES))
      {
        return true;
      }
      else
      {
        return false;
      }
  }

  /**
   * Returns whether the Novator environment is automatically checked/chosen
   * @param name    Novator environment name
   * @return        true if the Novator environment should be checked/chosen and false if it should not
   * @throws Exception
   */
  public boolean isNovatorEnvironmentChecked(String name) throws Exception
  {
      String value = null;
      if(name.equals(CONTENT))
      {
          value = ConfigurationUtil.getInstance().getFrpGlobalParm(NOVATOR_CONFIG,NOVATOR_CONTENT_FEED_CHECKED);        
      }
      else if(name.equals(PRODUCTION))
      {
          value = ConfigurationUtil.getInstance().getFrpGlobalParm(NOVATOR_CONFIG,NOVATOR_PRODUCTION_FEED_CHECKED);        
      }
      else if(name.equals(TEST))
      {
          value = ConfigurationUtil.getInstance().getFrpGlobalParm(NOVATOR_CONFIG,NOVATOR_TEST_FEED_CHECKED);        
      }
      else if(name.equals(UAT))
      {
          value = ConfigurationUtil.getInstance().getFrpGlobalParm(NOVATOR_CONFIG,NOVATOR_UAT_FEED_CHECKED);        
      }
      

      if(StringUtils.isNotBlank(value) && value.equalsIgnoreCase(COMMON_VAL_YES))
      {
        return true;
      }
      else
      {
        return false;
      }
  }

}
