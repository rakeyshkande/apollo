package com.ftd.osp.utilities.feed;

import com.ftd.osp.utilities.feed.vo.INovatorFeedDataVO;
import com.ftd.osp.utilities.feed.vo.NovatorFeedDataAddOnVO;
import com.ftd.osp.utilities.feed.vo.NovatorFeedDataFuelSurchargeVO;
import com.ftd.osp.utilities.feed.vo.NovatorFeedDataPreTransformedXmlVO;
import com.ftd.osp.utilities.feed.vo.NovatorFeedDataProductVO;
import com.ftd.osp.utilities.feed.vo.NovatorFeedDataUpsellDetailVO;

/**
 * Factory for Novator feed implementations.
 */ 
public class NovatorFeedTransformerFactory {
    public static INovatorFeedTransformer getTransformerImpl(INovatorFeedDataVO dataVO) throws Exception{
        if(dataVO instanceof NovatorFeedDataFuelSurchargeVO) {
            return new NovatorFeedTransformerFuelSurchargeImpl();
        } else if (dataVO instanceof NovatorFeedDataProductVO) {
            return new NovatorFeedTransformerProductImpl();
        } else if (dataVO instanceof NovatorFeedDataUpsellDetailVO) {
            return new NovatorFeedTransformerUpsellDetailImpl();
        } else if (dataVO instanceof NovatorFeedDataAddOnVO) {
            return new NovatorFeedTransformerAddOnImpl();
        } else if (dataVO instanceof NovatorFeedDataPreTransformedXmlVO) {
           return new NovatorFeedTransformerPreTransformedXML();
        } 
        else  
            throw new Exception("Unknown class type");
    }
}
