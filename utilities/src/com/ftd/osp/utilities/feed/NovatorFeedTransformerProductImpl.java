package com.ftd.osp.utilities.feed;

import com.ftd.ftdutilities.FieldUtils;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.feed.vo.ColorVO;
import com.ftd.osp.utilities.feed.vo.DeliveryOptionVO;
import com.ftd.osp.utilities.feed.vo.INovatorFeedDataVO;
import com.ftd.osp.utilities.feed.vo.NovatorFeedDataProductVO;
import com.ftd.osp.utilities.feed.vo.PartnerProductVO;
import com.ftd.osp.utilities.feed.vo.ProductAddonVO;
import com.ftd.osp.utilities.feed.vo.ProductSubCodeVO;
import com.ftd.osp.utilities.feed.vo.ProductVO;
import com.ftd.osp.utilities.feed.vo.StateDeliveryExclusionVO;
import com.ftd.osp.utilities.feed.vo.ValueVO;
import com.ftd.osp.utilities.feed.vo.VendorProductVO;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang.StringUtils;


/**
 * Novator Product feed implementation.
 */ 
public class NovatorFeedTransformerProductImpl extends NovatorFeedTransformerBase{
    private static final Logger logger = new Logger("com.ftd.osp.utilities.feed.NovatorFeedTransformerProductImpl");    

    /**
     * Converts a Novator fuel surcharge object to feed XML 
     * @param conn
     * @param dataVO
     * @return Fuel surcharge xml
     * @throws Exception
     */
    public String objectToXML(Connection conn, INovatorFeedDataVO dataVO) throws Exception{
        String xml = null;
        ProductVO pvo = null;
        String productId = null;
        
        NovatorFeedDataProductVO pDataVO = (NovatorFeedDataProductVO)dataVO;
        productId = pDataVO.getProductId();

        logger.info("productId passed in is:" + productId);
        
        //Retrive from database.
        pvo = getProductDetail(conn, pDataVO.getProductId());

        xml = pvo.toXML();
        logger.info("the xml data is "+xml);
        return xml;
    }
    
    
    /**
     * Returns a ProductVO based on the product id
     * @param conn database connection
     * @param productId to be returned
     * @return ProductVO
     * @exception Exception
     */    
    private ProductVO getProductDetail(Connection conn, String productId) throws Exception
    {
        ProductVO product = null;

        productId = productId.toUpperCase();
        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs = null;

        try
        {
            HashMap inputParams = new HashMap();
            inputParams.put("PRODUCT_ID", productId);
            
            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_GET_PRODUCT_DETAILS");
            dataRequest.setInputParams(inputParams);
            
            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance(); 
            HashMap outMap = (HashMap) dataAccessUtil.execute(dataRequest);
            rs = (CachedResultSet) outMap.get("OUT_PRODUCT_CUR");
            
            if(rs!=null && rs.next())
            {
                product = new ProductVO();
                product.setProductId(rs.getString(FeedConstants.OE_PRODUCT_MASTER_PRODUCT_ID));
                product.setNovatorId(rs.getString(FeedConstants.OE_PRODUCT_MASTER_NOVATOR_ID));
                product.setProductName(rs.getString(FeedConstants.OE_PRODUCT_MASTER_PRODUCT_NAME));                
                product.setNovatorName(rs.getString(FeedConstants.OE_PRODUCT_MASTER_NOVATOR_NAME));
                String status = rs.getString(FeedConstants.OE_PRODUCT_MASTER_STATUS);
                product.setDeliveryType(rs.getString(FeedConstants.OE_PRODUCT_MASTER_DELIVERY_TYPE));
                product.setCategory(rs.getString(FeedConstants.OE_PRODUCT_MASTER_CATEGORY));
                product.setProductType(rs.getString(FeedConstants.OE_PRODUCT_MASTER_PRODUCT_TYPE));
                product.setProductSubType(rs.getString(FeedConstants.OE_PRODUCT_MASTER_PRODUCT_SUB_TYPE));
                String colorSize = rs.getString(FeedConstants.OE_PRODUCT_MASTER_COLOR_SIZE_FLAG);
                product.setStandardPrice(rs.getFloat(FeedConstants.OE_PRODUCT_MASTER_STANDARD_PRICE));
                product.setDeluxePrice(rs.getFloat(FeedConstants.OE_PRODUCT_MASTER_DELUXE_PRICE));
                product.setPremiumPrice(rs.getFloat(FeedConstants.OE_PRODUCT_MASTER_PREMIUM_PRICE));
                product.setPreferredPricePoint(rs.getInt(FeedConstants.OE_PRODUCT_MASTER_PREFERRED_PRICE_POINT));
                product.setVariablePriceMax(rs.getFloat(FeedConstants.OE_PRODUCT_MASTER_VARIABLE_PRICE_MAX));
                product.setLongDescription(rs.getString(FeedConstants.OE_PRODUCT_MASTER_LONG_DESCRIPTION));
                product.setFloristReferenceNumber(rs.getString(FeedConstants.OE_PRODUCT_MASTER_FLORIST_REFERENCE_NUMBER));
                product.setMercuryDescription(rs.getString(FeedConstants.OE_PRODUCT_MASTER_MERCURY_DESCRIPTION));
                product.setItemComments(rs.getString(FeedConstants.OE_PRODUCT_MASTER_ITEM_COMMENTS));
                product.setAddOnBalloonsFlag(FieldUtils.convertStringToBoolean(rs.getString(FeedConstants.OE_PRODUCT_MASTER_ADD_ON_BALLOONS_FLAG)));
                product.setAddOnBearsFlag(FieldUtils.convertStringToBoolean(rs.getString(FeedConstants.OE_PRODUCT_MASTER_ADD_ON_BEARS_FLAG)));
                product.setAddOnGreetingCardsFlag(FieldUtils.convertStringToBoolean(rs.getString(FeedConstants.OE_PRODUCT_MASTER_ADD_ON_CARDS_FLAG)));
                product.setAddOnFuneralFlag(FieldUtils.convertStringToBoolean(rs.getString(FeedConstants.OE_PRODUCT_MASTER_ADD_ON_FUNERAL_FLAG)));
                product.setCodifiedFlag(rs.getString(FeedConstants.OE_PRODUCT_MASTER_CODIFIED_FLAG));
                product.setExceptionCode(rs.getString(FeedConstants.OE_PRODUCT_MASTER_EXCEPTION_CODE));
                product.setPersonalizationTemplate(rs.getString(FeedConstants.OE_PRODUCT_MASTER_PERSONALIZATION_TEMPLATE));
                product.setPersonalizationTemplateOrder(rs.getString(FeedConstants.OE_PRODUCT_MASTER_PERSONALIZATION_TEMPLATE_ORDER));
                product.setPersonalizationLeadDays(rs.getString(FeedConstants.OE_PRODUCT_MASTER_PERSONALIZATION_LEAD_DAYS));
                product.setPersonalizationCaseFlag(StringUtils.equals("Y", rs.getString("personalizationCaseFlag")));
                product.setAllAlphaFlag(StringUtils.equals("Y", rs.getString("allAlphaFlag")));
                product.setDepartmentCode(rs.getString(FeedConstants.OE_PRODUCT_MASTER_DEPARTMENT_CODE));
    //                product.setExceptionStartDate(rs.getDate(FeedConstants.OE_PRODUCT_MASTER_EXCEPTION_START_DATE));
    //                product.setExceptionEndDate(rs.getDate(FeedConstants.OE_PRODUCT_MASTER_EXCEPTION_END_DATE));
                Date utilDate = rs.getDate(FeedConstants.OE_PRODUCT_MASTER_EXCEPTION_START_DATE);
                if( utilDate!=null ) {
                  product.setExceptionStartDate(ProductVO.PDB_FORMAT.format(utilDate));
                } else {
                  product.setExceptionStartDate(null);
                }
                
                utilDate = rs.getDate(FeedConstants.OE_PRODUCT_MASTER_EXCEPTION_END_DATE);
                if( utilDate!=null ) {
                   product.setExceptionEndDate(ProductVO.PDB_FORMAT.format(utilDate));
                } else {
                   product.setExceptionEndDate(null);
                }
                product.setExceptionMessage(rs.getString(FeedConstants.OE_PRODUCT_MASTER_EXCEPTION_MESSAGE));
                product.setSecondChoice(rs.getString(FeedConstants.OE_PRODUCT_MASTER_SECOND_CHOICE));
                product.setHolidaySecondChoice(rs.getString(FeedConstants.OE_PRODUCT_MASTER_HOLIDAY_SECOND_CHOICE));
                product.setDropShipCode(rs.getString(FeedConstants.OE_PRODUCT_MASTER_DROP_SHIP_CODE));
                product.setDiscountAllowedFlag(FieldUtils.convertStringToBoolean(rs.getString(FeedConstants.OE_PRODUCT_MASTER_DISCOUNT_ALLOWED_FLAG)));
                product.setDeliveryIncludedFlag(FieldUtils.convertStringToBoolean(rs.getString(FeedConstants.OE_PRODUCT_MASTER_DELIVERY_INCLUDED_FLAG)));
                product.setTaxFlag(FieldUtils.convertStringToBoolean(rs.getString(FeedConstants.OE_PRODUCT_MASTER_TAX_FLAG)));
                product.setServiceFeeFlag(FieldUtils.convertStringToBoolean(rs.getString(FeedConstants.OE_PRODUCT_MASTER_SERVICE_FEE_FLAG)));
                product.setExoticFlag(FieldUtils.convertStringToBoolean(rs.getString(FeedConstants.OE_PRODUCT_MASTER_EXOTIC_FLAG)));
                product.setEGiftFlag(FieldUtils.convertStringToBoolean(rs.getString(FeedConstants.OE_PRODUCT_MASTER_EGIFT_FLAG)));
                product.setCountry(rs.getString(FeedConstants.OE_PRODUCT_MASTER_COUNTRY));
                product.setArrangementSize(rs.getString(FeedConstants.OE_PRODUCT_MASTER_ARRANGEMENT_SIZE));
                //product.setArrangementColors(rs.getString(FeedConstants.OE_PRODUCT_MASTER_ARRANGEMENT_COLORS));
                product.setDominantFlowers(rs.getString(FeedConstants.OE_PRODUCT_MASTER_DOMINANT_FLOWERS));
                product.setSearchPriority(rs.getString(FeedConstants.OE_PRODUCT_MASTER_SEARCH_PRIORITY));
                product.setRecipe(rs.getString(FeedConstants.OE_PRODUCT_MASTER_RECIPE));
                product.setSubcodeFlag(FieldUtils.convertStringToBoolean(rs.getString(FeedConstants.OE_PRODUCT_MASTER_SUB_CODE_FLAG)));
                product.setDimWeight(rs.getString(FeedConstants.OE_PRODUCT_MASTER_DIM_WEIGHT));
                product.setNextDayUpgrade(FieldUtils.convertStringToBoolean(rs.getString(FeedConstants.OE_PRODUCT_MASTER_NEXT_DAY_UPGRADE_FLAG)));
                product.setCorporateSiteFlag(FieldUtils.convertStringToBoolean(rs.getString(FeedConstants.OE_PRODUCT_MASTER_CORPORATE_SITE_FLAG)));
                product.setUnspscCode(rs.getString(FeedConstants.OE_PRODUCT_MASTER_UNSPSC_CODE));
                product.setStandardPriceRank(rs.getString(FeedConstants.OE_PRODUCT_MASTER_PRICE_RANK_1));
                product.setDeluxePriceRank(rs.getString(FeedConstants.OE_PRODUCT_MASTER_PRICE_RANK_2));
                product.setPremiumPriceRank(rs.getString(FeedConstants.OE_PRODUCT_MASTER_PRICE_RANK_3));
                product.setShipMethodCarrier(FieldUtils.convertStringToBoolean(rs.getString(FeedConstants.OE_PRODUCT_MASTER_SHIP_METHOD_CARRIER)));
                product.setShipMethodFlorist(FieldUtils.convertStringToBoolean(rs.getString(FeedConstants.OE_PRODUCT_MASTER_SHIP_METHOD_FLORIST)));
                product.setShippingKey(rs.getString(FeedConstants.OE_PRODUCT_MASTER_SHIPPING_KEY)); 
                product.setMondayDeliveryFreshCuts(FieldUtils.convertStringToBoolean(rs.getString(FeedConstants.OE_PRODUCT_MASTER_MONDAY_DELIVERY_FRESHCUT)));
                product.setTwoDaySaturdayShipFreshCuts(FieldUtils.convertStringToBoolean(rs.getString(FeedConstants.OE_PRODUCT_MASTER_TWO_DAY_SHIP_SAT_FRESHCUT)));
                product.setWeboeBlocked(FieldUtils.convertStringToBoolean(rs.getString(FeedConstants.OE_PRODUCT_MASTER_WEBOE_BLOCKED)));

                // OE_PRODUCT_MASTER_EXPRESS_SHIPPING New for FedEx home project
                product.setExpressShippingOnly(FieldUtils.convertStringToBoolean(rs.getString(FeedConstants.OE_PRODUCT_MASTER_EXPRESS_SHIPPING)));

                product.setVariablePricingFlag(FieldUtils.convertStringToBoolean(rs.getString(FeedConstants.OE_PRODUCT_MASTER_VARIABLE_PRICE_FLAG)));
                product.setNewSKU(rs.getString(FeedConstants.OE_PRODUCT_MASTER_HOLIDAY_SKU));
                product.setNewStandardPrice(rs.getFloat(FeedConstants.OE_PRODUCT_MASTER_HOLIDAY_PRICE));
                product.setCatelogFlag(FieldUtils.convertStringToBoolean(rs.getString(FeedConstants.OE_PRODUCT_MASTER_CATELOG_FLAG)));

                product.setNewDeluxePrice(rs.getFloat(FeedConstants.OE_PRODUCT_MASTER_HOLIDAY_DELUXE_PRICE));
                product.setNewPremiumPrice(rs.getFloat(FeedConstants.OE_PRODUCT_MASTER_HOLIDAY_PREMIUM_PRICE));
                //product.setNewStartDate(rs.getDate(FeedConstants.OE_PRODUCT_MASTER_NEW_START_DATE));
                utilDate = rs.getDate(FeedConstants.OE_PRODUCT_MASTER_NEW_START_DATE);
                if( utilDate!=null ) {
                    product.setNewStartDate(ProductVO.PDB_FORMAT.format(utilDate));
                } else {
                    product.setNewStartDate(null);
                }
                //product.setNewEndDate(rs.getDate(FeedConstants.OE_PRODUCT_MASTER_NEW_END_DATE));
                utilDate = rs.getDate(FeedConstants.OE_PRODUCT_MASTER_NEW_END_DATE);
                if( utilDate!=null ) {
                    product.setNewEndDate(ProductVO.PDB_FORMAT.format(utilDate));
                } else {
                    product.setNewEndDate(null);
                }
                product.setMercurySecondChoice(rs.getString(FeedConstants.OE_PRODUCT_MASTER_MERCURY_SECOND_CHOICE));
                product.setMercuryHolidaySecondChoice(rs.getString(FeedConstants.OE_PRODUCT_MASTER_MERCURY_HOLIDAY_SECOND_CHOICE));
                product.setAddOnChocolateFlag(FieldUtils.convertStringToBoolean(rs.getString(FeedConstants.OE_PRODUCT_MASTER_ADD_ON_CHOCOLATE_FLAG)));
                product.setCrossRefNovatorID(rs.getString(FeedConstants.OE_PRODUCT_MASTER_CROSS_REF_NOVATOR_ID));
                product.setGeneralComments(rs.getString(FeedConstants.OE_PRODUCT_MASTER_GENERAL_COMMENTS));
                product.setDefaultCarrier(rs.getString(FeedConstants.OE_PRODUCT_MASTER_DEFAULT_CARRIER));

                String holdUntilAvailable = rs.getString(FeedConstants.OE_PRODUCT_MASTER_HOLD_UNTIL_AVAILABLE);
                if(holdUntilAvailable.equals(FeedConstants.PRODUCT_HOLD_UNTIL_AVAILABLE_KEY_Y)){
                    product.setHoldUntilAvailable(true);
                }else{
                    product.setHoldUntilAvailable(false);
                }                
                if(status.equals(FeedConstants.PRODUCT_AVAILABLE_KEY)){
                    product.setStatus(true);
                }else{
                    product.setStatus(false);
                }
                if(colorSize.equals(FeedConstants.PRODUCT_COLOR_FLAG)){
                    product.setColorSizeFlag(true);
                }else{
                    product.setColorSizeFlag(false);
                }
                product.setLastUpdateUserId(rs.getString(FeedConstants.OE_PRODUCT_MASTER_LAST_UPDATE_USER_ID));
                product.setLastUpdateSystem(rs.getString(FeedConstants.OE_PRODUCT_MASTER_LAST_UPDATE_SYSTEM));
                product.setOver21Flag(StringUtils.equals("Y",rs.getString(FeedConstants.OE_PRODUCT_MASTER_OVER_21)));
                product.setShippingSystem(rs.getString(FeedConstants.OE_PRODUCT_MASTER_SHIPPING_SYSTEM));
                product.setCustomFlag(StringUtils.equals("Y",rs.getString(FeedConstants.OE_PRODUCT_MASTER_CUSTOM_FLAG)));
                product.setKeywords(rs.getString(FeedConstants.OE_PRODUCT_MASTER_KEYWORDS));
                product.setZoneJumpEligibleFlag(StringUtils.equals("Y",rs.getString(FeedConstants.OE_PRODUCT_MASTER_ZONE_JUMP_ELIGIBLE_FLAG)));
                product.setBoxId(rs.getString(FeedConstants.OE_PRODUCT_MASTER_BOX_ID));
                product.setPersonalGreetingFlag(StringUtils.equals("Y",rs.getString(FeedConstants.OE_PRODUCT_MASTER_PERSONAL_GREETING_FLAG)));
                product.setSupplyExpense(rs.getFloat(FeedConstants.OE_PRODUCT_MASTER_SUPPLY_EXPENSE));
                utilDate = rs.getDate(FeedConstants.OE_PRODUCT_MASTER_SUPPLY_EXPENSE_EFF_DATE);
                if( utilDate!=null ) {
                   product.setSupplyExpenseEffDate(ProductVO.PDB_FORMAT.format(utilDate));
                } else {
                   product.setSupplyExpenseEffDate(null);
                }
                product.setRoyaltyPercent(rs.getFloat(FeedConstants.OE_PRODUCT_MASTER_ROYALTY_PERCENT));
                utilDate = rs.getDate(FeedConstants.OE_PRODUCT_MASTER_ROYALTY_PERCENT_EFF_DATE);
                if( utilDate!=null ) {
                   product.setRoyaltyPercentEffDate(ProductVO.PDB_FORMAT.format(utilDate));
                } else {
                   product.setRoyaltyPercentEffDate(null);
                }
                product.setGbbPopoverFlag(StringUtils.equals("Y",rs.getString(FeedConstants.OE_PRODUCT_MASTER_GBB_POPOVER_FLAG)));
                product.setGbbTitle(rs.getString(FeedConstants.OE_PRODUCT_MASTER_GBB_TITLE));
                product.setGbbNameOverrideFlag1(StringUtils.equals("Y",rs.getString(FeedConstants.OE_PRODUCT_MASTER_GBB_NAME_OVERRIDE_FLAG_1)));
                product.setGbbNameOverrideText1(rs.getString(FeedConstants.OE_PRODUCT_MASTER_GBB_NAME_OVERRIDE_TEXT_1));
                product.setGbbPriceOverrideFlag1(StringUtils.equals("Y",rs.getString(FeedConstants.OE_PRODUCT_MASTER_GBB_PRICE_OVERRIDE_FLAG_1)));
                product.setGbbPriceOverrideText1(rs.getString(FeedConstants.OE_PRODUCT_MASTER_GBB_PRICE_OVERRIDE_TEXT_1));
                product.setGbbNameOverrideFlag2(StringUtils.equals("Y",rs.getString(FeedConstants.OE_PRODUCT_MASTER_GBB_NAME_OVERRIDE_FLAG_2)));
                product.setGbbNameOverrideText2(rs.getString(FeedConstants.OE_PRODUCT_MASTER_GBB_NAME_OVERRIDE_TEXT_2));
                product.setGbbPriceOverrideFlag2(StringUtils.equals("Y",rs.getString(FeedConstants.OE_PRODUCT_MASTER_GBB_PRICE_OVERRIDE_FLAG_2)));
                product.setGbbPriceOverrideText2(rs.getString(FeedConstants.OE_PRODUCT_MASTER_GBB_PRICE_OVERRIDE_TEXT_2));
                product.setGbbNameOverrideFlag3(StringUtils.equals("Y",rs.getString(FeedConstants.OE_PRODUCT_MASTER_GBB_NAME_OVERRIDE_FLAG_3)));
                product.setGbbNameOverrideText3(rs.getString(FeedConstants.OE_PRODUCT_MASTER_GBB_NAME_OVERRIDE_TEXT_3));
                product.setGbbPriceOverrideFlag3(StringUtils.equals("Y",rs.getString(FeedConstants.OE_PRODUCT_MASTER_GBB_PRICE_OVERRIDE_FLAG_3)));
                product.setGbbPriceOverrideText3(rs.getString(FeedConstants.OE_PRODUCT_MASTER_GBB_PRICE_OVERRIDE_TEXT_3));
                product.setServiceDuration(rs.getString(FeedConstants.OE_PRODUCT_SERVICE_DURATION));
                product.setAllowFreeShippingFlag(FieldUtils.convertStringToBoolean(rs.getString(FeedConstants.OE_ALLOW_FREE_SHIPPING_FLAG)));
                product.setMorningDeliveryFlag(FieldUtils.convertStringToBoolean(rs.getString(FeedConstants.OE_MORNING_DELIVERY_FLAG)));
                product.setPquadProductID(rs.getString(FeedConstants.OE_PRODUCT_MASTER_PQUAD_PRODUCT_ID));
                product.setPquadPersonalizationId(rs.getString(FeedConstants.PQUAD_PC_ID));
            }

            if(product != null)
            {
                // get the keywords
                rs = (CachedResultSet) outMap.get("OUT_KEYWORD_CUR");
                
                if( rs!=null ) {
                    List keywords = new ArrayList();
                    while(rs.next())
                    {
                        String keyword = rs.getString(FeedConstants.OE_PRODUCT_VALUE_ID);
                        keywords.add(keyword);
                    }
                    StringBuffer sb = new StringBuffer();
                    for(int i = 0; i < keywords.size(); i++)
                    {
                        sb.append(keywords.get(i));
                        sb.append(" ");
                    }
                    product.setKeywordSearch(sb.toString());
                }
                
                /*
                 * COLOR CODES NO LONGER USED FOR ORIGINAL PURPOSE - SO COLOR LOGIC THAT
                 * WAS HERE WAS COMMENTED OUT IN MARCH 2018. COLOR TABLES HAVE BEEN REPURPOSED AS 
                 * PRODUCT MARKERS FOR PROJECT FRESH.   
                // get the colors
                rs = (CachedResultSet) outMap.get("OUT_COLOR_CUR");
                if( rs!=null ) {
                    List colors = new ArrayList();
                    while(rs.next())
                    {
                        ColorVO color = new ColorVO();
                        color.setId(rs.getString(FeedConstants.OE_PRODUCT_VALUE_ID));
                        color.setOrderBy(rs.getInt(FeedConstants.OE_PRODUCT_VALUE_DESCRIPTION));
                        colors.add(color);
                    }
                    product.setColorsList(colors);
                }
                // set all colors list//
                product.setAllColorsList(getColorsList(conn));
                */
                
                // get the product component skus
                rs = (CachedResultSet) outMap.get("OUT_COMPONENT_CUR");
                if (rs != null) {
                    List componentSkus = new ArrayList();
                    while (rs.next()) {
                        ValueVO component = new ValueVO();
                        component.setId(rs.getString("componentSkuId"));
                        component.setDescription(rs.getString("componentSkuName"));
                        componentSkus.add(component);
                        logger.debug(component.getId());
                    }
                    product.setComponentSkuList(componentSkus);
                }
                product.setAllComponentSkuList(getComponentSkuList(conn));

                // Get the recipient search list
                rs = (CachedResultSet) outMap.get("OUT_RECIPIENT_SEARCH_CUR");
                
                if( rs!=null ) {
                    List recipientList = new ArrayList();
                    while(rs.next())
                    {
                        String recipient = rs.getString(FeedConstants.OE_PRODUCT_VALUE_ID);
                        recipientList.add(recipient);
                    }
                    String[] recipientArray = new String[recipientList.size()];
                    for( int idx=0; idx<recipientList.size(); idx++) {
                        recipientArray[idx]=(String)recipientList.get(idx);   
                    }
                    product.setRecipientSearch(recipientArray);
                }
                
                // Get the recipient search list
                rs = (CachedResultSet) outMap.get("OUT_COMPANY_CUR");
                
                if( rs!=null ) {
                    List companyList = new ArrayList();
                    while(rs.next())
                    {
                        String company = rs.getString(FeedConstants.OE_COMPANY_ID);
                        companyList.add(company);
                    }
                    String[] companyArray = new String[companyList.size()];
                    for( int idx=0; idx<companyList.size(); idx++) {
                        companyArray[idx]=(String)companyList.get(idx);   
                    }
                    product.setCompanyList(companyArray);
                }

                // Get all the excluded states
                rs = (CachedResultSet) outMap.get("OUT_EXCL_STATES_CUR");
                
                if( rs!=null ) {
                    List excludedStates = getStateDeliveryExceptionList(conn);
                    String tmpStateId;
                    StateDeliveryExclusionVO sdeVO;
                    while(rs.next())
                    {
                        tmpStateId = rs.getString(FeedConstants.EXCLUDED_STATE);
                        if( tmpStateId!=null ) {
                            for( int idx=0; idx<excludedStates.size(); idx++ ) {
                                sdeVO = (StateDeliveryExclusionVO)excludedStates.get(idx);
                                if( tmpStateId.equals(sdeVO.getExcludedState()) ) {
                                    sdeVO.setSunExcluded(FieldUtils.convertStringToBoolean(rs.getString(FeedConstants.SUNDAY)));
                                    sdeVO.setMonExcluded(FieldUtils.convertStringToBoolean(rs.getString(FeedConstants.MONDAY)));
                                    sdeVO.setTueExcluded(FieldUtils.convertStringToBoolean(rs.getString(FeedConstants.TUESDAY)));
                                    sdeVO.setWedExcluded(FieldUtils.convertStringToBoolean(rs.getString(FeedConstants.WEDNESDAY)));
                                    sdeVO.setThuExcluded(FieldUtils.convertStringToBoolean(rs.getString(FeedConstants.THURSDAY)));
                                    sdeVO.setFriExcluded(FieldUtils.convertStringToBoolean(rs.getString(FeedConstants.FRIDAY)));
                                    sdeVO.setSatExcluded(FieldUtils.convertStringToBoolean(rs.getString(FeedConstants.SATURDAY)));
                                    break;
                                }
                            }
                        }
                    }
                    
                    product.setExcludedDeliveryStates(excludedStates);
                        
                }               

                // Get the shipping methods for this product
                rs = (CachedResultSet) outMap.get("OUT_SHIP_METHODS_CUR");
                
                if( rs!=null ) {
                    List methodsList = new ArrayList();
                    while(rs.next())
                    {
                        String methodId = rs.getString(FeedConstants.GET_PRODUCT_SHIP_METHODS);
                        String defaultCarrier = rs.getString(FeedConstants.GET_PRODUCT_SHIP_METHOD_CARRIER);
                        String novatorTag = rs.getString(FeedConstants.GET_PRODUCT_SHIP_METHOD_NOVATOR_TAG);
                        DeliveryOptionVO vo = new DeliveryOptionVO();
                        vo.setShippingMethodId(methodId);
                        vo.setDefaultCarrier(defaultCarrier);
                        vo.setNovatorTag(novatorTag);
                        methodsList.add(vo);
                    }
                    product.setShipMethods(methodsList);
                }

                // Get the subcodes for this product
                rs = (CachedResultSet) outMap.get("OUT_SUB_CODES_CUR");
                
                if( rs!=null ) {
                    List subcodeList = new ArrayList();
                    while(rs.next())
                    {
                        ProductSubCodeVO vo = new ProductSubCodeVO();
                        vo.setProductId(rs.getString(FeedConstants.PDB_PRODUCT_SUBCODE_PRODUCT_ID));
                        vo.setProductSubCodeId(rs.getString(FeedConstants.PDB_PRODUCT_SUBCODE_SUBCODE_ID));
                        vo.setSubCodeDescription(rs.getString(FeedConstants.PDB_PRODUCT_SUBCODE_DESCRIPTION));
                        vo.setSubCodeRefNumber(rs.getString(FeedConstants.PDB_PRODUCT_SUBCODE_REF_NUMBER));
                        vo.setSubCodePrice(rs.getFloat(FeedConstants.PDB_PRODUCT_SUBCODE_PRICE));
                        vo.setSubCodeHolidaySKU(rs.getString(FeedConstants.PDB_PRODUCT_SUBCODE_HOLIDAY_SKU));
                        vo.setSubCodeHolidayPrice(rs.getFloat(FeedConstants.PDB_PRODUCT_SUBCODE_HOLIDAY_PRICE));
                        vo.setActiveFlag(FieldUtils.convertStringToBoolean(rs.getString(FeedConstants.PDB_PRODUCT_SUBCODE_AVAILABLE)));
                        vo.setDimWeight(rs.getString(FeedConstants.PDB_PRODUCT_SUBCODE_DIM_WEIGHT));
                        vo.setDisplayOrder(rs.getInt(FeedConstants.PDB_PRODUCT_SUBCODE_DISPLAY_ORDER));
    
                        subcodeList.add(vo);
                    }
                    
                    product.setSubCodeList(subcodeList);
                }
                
                // Get the vendor products
               List vendorList = new ArrayList();
                rs = (CachedResultSet) outMap.get("OUT_VENDORS_CUR");
                if( rs!=null ) {
                    while(rs.next()) {
                        VendorProductVO vo = new VendorProductVO();
                        vo.setAvailable(FieldUtils.convertStringToBoolean(rs.getString("AVAILABLE")));
                        vo.setProductSkuId(rs.getString("PRODUCT_SUBCODE_ID"));
                        vo.setRemoved(FieldUtils.convertStringToBoolean(rs.getString("REMOVED")));
                        vo.setVendorCost(rs.getFloat("VENDOR_COST"));
                        vo.setVendorId(rs.getString("VENDOR_ID"));
                        vo.setVendorName(rs.getString("VENDOR_NAME"));
                        vo.setVendorSku(rs.getString("VENDOR_SKU"));
                        vendorList.add(vo);
                    }
                }
                product.setVendorProductsList(vendorList);
                
                // Get the ship dates
                 rs = (CachedResultSet) outMap.get("OUT_SHIP_DATES_CUR");
                
                if( rs!=null ) {
                    while(rs.next())
                    {
                        product.setShipDayMonday(FieldUtils.convertStringToBoolean(rs.getString(FeedConstants.PDB_PRODUCT_SHIP_DATES_MONDAY)));
                        product.setShipDayTuesday(FieldUtils.convertStringToBoolean(rs.getString(FeedConstants.PDB_PRODUCT_SHIP_DATES_TUESDAY)));
                        product.setShipDayWednesday(FieldUtils.convertStringToBoolean(rs.getString(FeedConstants.PDB_PRODUCT_SHIP_DATES_WEDNESDAY)));
                        product.setShipDayThursday(FieldUtils.convertStringToBoolean(rs.getString(FeedConstants.PDB_PRODUCT_SHIP_DATES_THURSDAY)));
                        product.setShipDayFriday(FieldUtils.convertStringToBoolean(rs.getString(FeedConstants.PDB_PRODUCT_SHIP_DATES_FRIDAY)));
                        product.setShipDaySaturday(FieldUtils.convertStringToBoolean(rs.getString(FeedConstants.PDB_PRODUCT_SHIP_DATES_SATURDAY)));
                        product.setShipDaySunday(FieldUtils.convertStringToBoolean(rs.getString(FeedConstants.PDB_PRODUCT_SHIP_DATES_SUNDAY)));
                    }
                }
                
                List<PartnerProductVO> partnerProductsList = new ArrayList<PartnerProductVO>();
                rs = (CachedResultSet) outMap.get("OUT_PARTNER_PRODUCT_CUR");
                if( rs!=null ) {
                    while( rs.next() ) {
                        PartnerProductVO vo = new PartnerProductVO();
                        vo.setProductSubcodeId(rs.getString("PRODUCT_SUBCODE_ID"));
                        vo.setPartnerId(rs.getString("PARTNER_ID"));
                        vo.setAvailableFlag(StringUtils.equals("Y",rs.getString("AVAILABLE_FLAG")));
                        vo.setWholesalePrice(rs.getBigDecimal("WHOLESALE_PRICE_AMT"));
                        partnerProductsList.add(vo);
                    }
                }
                product.setPartnerProductsList(partnerProductsList);
                
                
                //This is to fix the feed so Novator will understand this specific type of product configuration
                if ("SDFC".equalsIgnoreCase(product.getProductType()) &&  !product.isShipMethodCarrier() && product.isShipMethodFlorist())
                {
                    product.setProductType("FLORAL");
                }       
                if ("SDG".equalsIgnoreCase(product.getProductType()) &&  !product.isShipMethodCarrier() && product.isShipMethodFlorist())
                {
                    product.setProductType("FLORAL");
                }  
                
                // get the website source codes
                rs = (CachedResultSet) outMap.get("OUT_PRODUCT_SOURCE_CUR");
                
                if( rs!=null ) {
                    List websites = new ArrayList();
                    
                    while(rs.next())
                    {
                        String sourceCode = rs.getString(FeedConstants.OE_PRODUCT_VALUE_ID);
                        String orderSource = rs.getString(FeedConstants.OE_ORDER_SOURCE);
                        if (orderSource != null && orderSource.equalsIgnoreCase("I")) {
                            websites.add(sourceCode);
                        }
                    }
                    StringBuffer sb = new StringBuffer();
                    for(int i = 0; i < websites.size(); i++)
                    {
                        sb.append(websites.get(i));
                        sb.append(" ");
                    }
                    product.setProductWebsites(sb.toString());
                }
                
                HashMap <String, ArrayList<ProductAddonVO>> productAddonMap = new HashMap <String, ArrayList<ProductAddonVO>>();
                ArrayList <ProductAddonVO> productAddonVOList = new ArrayList<ProductAddonVO>();
                rs = (CachedResultSet) outMap.get("OUT_PRODUCT_ADDON_CUR");
                if( rs!=null ) {
                    while( rs.next() ) {
                        ProductAddonVO productAddonVO = new ProductAddonVO();
                        productAddonVO.setAddonId(rs.getString("ADDON_ID"));
                        productAddonVO.setAddonTypeId(rs.getString("ADDON_TYPE_ID"));
                        productAddonVO.setAddonDescription(rs.getString("DESCRIPTION"));
                        productAddonVO.setAddonPrice(rs.getString("PRICE"));
                        productAddonVO.setActiveFlag(StringUtils.equals("Y",rs.getString("PRODUCT_ADDON_ACTIVE_FLAG")));
                        productAddonVO.setAddonAvailableFlag(StringUtils.equals("Y",rs.getString("ADDON_ACTIVE_FLAG")));
                        productAddonVO.setAddonTypeIncludedInProductFeedFlag(StringUtils.equals("Y",rs.getString("PRODUCT_FEED_FLAG")));
                        productAddonVO.setDisplaySequenceNumber(rs.getString("DISPLAY_SEQ_NUM"));
                        productAddonVO.setMaxQuantity(rs.getString("MAX_QTY"));
                        productAddonVO.setAddonPquadAccessoryId(rs.getString("PQUAD_ACCESSORY_ID"));
                        productAddonVOList.add(productAddonVO);
                        logger.debug("Addon found with id " + productAddonVO.getAddonId());
                    }
                    productAddonMap.put(FeedConstants.PDB_PRODUCT_ADDON_VO_ADDON_KEY,productAddonVOList);
                }
                productAddonVOList = new ArrayList<ProductAddonVO>();
                rs = (CachedResultSet) outMap.get("OUT_PRODUCT_ADDON_VASE_CUR");
                if( rs!=null ) {
                    while( rs.next() ) {
                        ProductAddonVO productAddonVO = new ProductAddonVO();
                        productAddonVO.setAddonId(rs.getString("ADDON_ID"));
                        productAddonVO.setAddonTypeId(rs.getString("ADDON_TYPE_ID"));
                        productAddonVO.setAddonDescription(rs.getString("DESCRIPTION"));
                        productAddonVO.setAddonPrice(rs.getString("PRICE"));
                        productAddonVO.setActiveFlag(StringUtils.equals("Y",rs.getString("PRODUCT_ADDON_ACTIVE_FLAG")));
                        productAddonVO.setAddonAvailableFlag(StringUtils.equals("Y",rs.getString("ADDON_ACTIVE_FLAG")));
                        productAddonVO.setAddonTypeIncludedInProductFeedFlag(StringUtils.equals("Y",rs.getString("PRODUCT_FEED_FLAG")));
                        productAddonVO.setDisplaySequenceNumber(rs.getString("DISPLAY_SEQ_NUM"));
                        productAddonVO.setMaxQuantity(rs.getString("MAX_QTY"));
                        productAddonVO.setAddonPquadAccessoryId(rs.getString("PQUAD_ACCESSORY_ID"));
                        productAddonVOList.add(productAddonVO);
                        logger.debug("Vase found with id " + productAddonVO.getAddonId());

                    }
                    productAddonMap.put(FeedConstants.PDB_PRODUCT_ADDON_VO_VASE_KEY,productAddonVOList);
                }
                product.setProductAddonMap(productAddonMap);
            }
        }
        catch(Exception e)
        {
            throw e;
        }


        logger.debug("ProductDAOImpl:getProduct() End");
        
        return product;
    }
    
    /**
    * Returns a list of states that can have delivery exceptions
    * @param conn database connection
    * @throws Exception
    **/
    public List getStateDeliveryExceptionList(Connection conn) throws Exception
    {
        List excludedStateList = new ArrayList();
        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs = null;
        try
        {
            // Get all the excluded states
            logger.debug("ProductDAOImpl:getProduct() Getting excluded states");
            
            HashMap inputParams = new HashMap();
            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_GET_DEL_EXCL_STATES");
            dataRequest.setInputParams(inputParams);
            
            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
            rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

            
            StateDeliveryExclusionVO sdeVO;
            while(rs.next())
            {
                sdeVO = new StateDeliveryExclusionVO();
                sdeVO.setExcludedState(rs.getString(FeedConstants.STATE_ID));
                sdeVO.setStateName(rs.getString(FeedConstants.STATE_NAME));
                sdeVO.setSunExcluded(false);
                sdeVO.setMonExcluded(false);
                sdeVO.setTueExcluded(false);
                sdeVO.setWedExcluded(false);
                sdeVO.setThuExcluded(false);
                sdeVO.setFriExcluded(false);
                sdeVO.setSatExcluded(false);
                excludedStateList.add(sdeVO);
            }
        }
        catch(Exception e)
        {
            logger.error(e);
            throw e;
        }
        
        return excludedStateList;
    }    
    
    /**
     * Get a list of  ValueVOs representing available colors
     * @param conn database connection
     * @return List of ValueVOs representing available colors
     * @exception PDBSystemException
     */
    public List getColorsList(Connection conn) throws Exception
    {
        List list = new ArrayList();
        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs = null;
        
        try
        {
            HashMap inputParams = new HashMap();
            
            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_GET_COLORS_LIST");
            dataRequest.setInputParams(inputParams);
            
            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
            rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

            while(rs.next())
            {
                ColorVO valueVO = new ColorVO();
                valueVO.setId(rs.getString(FeedConstants.OE_PRODUCT_VALUE_ID));
                valueVO.setDescription(rs.getString(FeedConstants.OE_PRODUCT_VALUE_DESCRIPTION));
                list.add(valueVO);
            }
        }  
        catch(Exception e)
        {
            logger.error(e);
            throw e;
        }

        return list;
    }       

    /**
       * Get a list of  ValueVOs representing available component skus
       * @param conn database connection
       * @return List of ValueVOs representing available component skus
       * @exception Exception
       */
    public List getComponentSkuList(Connection conn) throws Exception
    {
        List list = new ArrayList();
        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs = null;
        
        try
        {
            HashMap inputParams = new HashMap();
            
            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("PDB_GET_COMPONENT_SKU_LIST");
            inputParams.put("IN_COMPONENT_SKU_ID", null);
            inputParams.put("IN_SHOW_AVAILABLE", "Y");
            dataRequest.setInputParams(inputParams);
            
            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
            rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

            while(rs.next())
            {
                ValueVO valueVO = new ValueVO();
                valueVO.setId(rs.getString("component_sku_id"));
                valueVO.setDescription(rs.getString("component_sku_name"));
                list.add(valueVO);
                logger.debug(valueVO.getId() + " " + valueVO.getDescription());
            }
        }    
        catch(Exception e)
        {
            logger.error(e);
            throw e;
        }

        return list;
    }    
    
    public String objectToXML(INovatorFeedDataVO dataVO) throws Exception{
        return objectToXML(null, dataVO);
    }
}
