package com.ftd.osp.utilities;

import java.math.BigDecimal;
import java.sql.Connection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.ftd.ftdutilities.GeneralConstants;
import com.ftd.osp.utilities.constants.VendorProductAvailabilityConstants;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.feed.NovatorFeedProductUtil;
import com.ftd.osp.utilities.feed.vo.NovatorFeedResponseVO;
import com.ftd.osp.utilities.feed.NovatorFeedUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;
import com.ftd.osp.utilities.vo.InventoryVO;

public class VendorProductAvailabilityUtility
{
	private static Logger logger = new Logger("com.ftd.osp.utilities.VendorProductAvailabilityUtility");

	public VendorProductAvailabilityUtility()
	{
	}

	/**
	 * This method will clear associated exception dates / codes for product
	 * passed in.
	 * 
	 * @param productId
	 * @param connection
	 * @exception Exception
	 */
	public static void clearDates(String productId, Connection connection)
	throws Exception
	{
		clearExceptionDates(productId, connection);
		insertPASJMSMessage(connection,"MODIFIED_PRODUCT " + productId + " YES");

	} //end method clearDates

	/**
	 * This method will make product and/or vendor and/or dropship delivery available
	
	 */
	public static void makeAvailable(String productId, String vendorId, List<String> invTrkIds, boolean updateProduct, 
			String updatedBy, Connection connection) throws Exception {
		
		boolean hasVendorDelivery = false;
		boolean isProductAvailable = false;
		CachedResultSet crs = null;
		
			// Update all the inventory records assigned to a product_vendor as available.
		if(invTrkIds != null && invTrkIds.size() > 0) {
			for (String invTrkId : invTrkIds) {
				updateInventoryStatus(new BigDecimal(invTrkId), VendorProductAvailabilityConstants.AVAILABLE, connection);
			}  		
		}
		// Update the vendor-product as available.
		Map<String, String> vendProdFlags = getProductVendorFlags(productId, vendorId, connection);
		if (!VendorProductAvailabilityConstants.ACTIVE.equals(vendProdFlags.get("OUT_VP_AVAILABILITY"))) {				
			updateVendorProdAvailability(vendorId, productId, VendorProductAvailabilityConstants.ACTIVE, updatedBy, connection);
		} else {
			logger.debug("Vendor_product is in available state, not updating flag for vendor- " +vendorId + ", product- "
					+ productId);
		}
  		
		if(updateProduct) {
			crs = getProductInfo(productId, connection);
			if (crs.next()) {
			    isProductAvailable = StringUtils.equals(crs.getString("status"),"A") ? true : false;
			    hasVendorDelivery  = StringUtils.equals(crs.getString("shipMethodCarrier"),"Y") ? true : false;
			}
			logger.debug(productId + " isProductAvailable: " + isProductAvailable + " hasVendorDelivery: " + hasVendorDelivery);
			boolean sendPAS = false;
			if (!hasVendorDelivery) {				
				updateProdDropshipDelivery(productId, VendorProductAvailabilityConstants.ACTIVE, updatedBy, connection);
				sendPAS = true;
			}
			if (!isProductAvailable) {				
				updateProductStatus(productId,	VendorProductAvailabilityConstants.AVAILABLE, updatedBy, connection);
				sendPAS = true;
			}			
			if (sendPAS) {
			    insertPASJMSMessage(connection, "MODIFIED_PRODUCT " + productId + " Y N");
			}
		}
	}
	
	/**
	 * This method returns if the vendor is associated with a given product.
	 * 
	 * @param productId
	 * @param vendorId
	 * @return true if vendor is not associated with the product and vice versa
	 * @throws Exception 
	 */
	private static Map<String,String> getProductVendorFlags(String productId,
			String vendorId, Connection connection) throws Exception {
		DataRequest dataRequest = new DataRequest();
		String isRemoved = null;
		Map<String,String> outputs = null;
		try {
			logger.info("getProductVendorFlags(String productId) :: vendorId");
			HashMap inputParams = new HashMap();
			inputParams.put("IN_PRODUCT_SUBCODE_ID", productId);
			inputParams.put("IN_VENDOR_ID", vendorId);
			dataRequest.setConnection(connection);
			dataRequest.setStatementID("GET_VENDOR_PRODUCT_FLAGS");
			dataRequest.setInputParams(inputParams);

			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
			outputs = (Map<String,String>)dataAccessUtil.execute(dataRequest);
		} catch (Exception e) {
			logger.error(e.toString());
			throw e;
		}
		return outputs;
	}


	/**
	 * This method will make product and/or vendor and/or dropship delivery unavailable
	 * 
	 * @param productId
	 * @param vendorId
	 * @param connection
	 * @throws Exception
	 */ 
	public static void makeUnavailable(String productId, String vendorId, String invTrkId, String invStatus, String vpAvailable, String csrId, Connection connection) throws Exception {
		if(!VendorProductAvailabilityConstants.AVAILABLE.equals(invStatus)) {
			logger.debug("Inventory, " + invTrkId + " Status is not available. Do not make Unavailable.");
			return;
		}

		logger.debug("Recalculating Inventory status for: " + invTrkId);
		processMakeUnavailable(productId, vendorId, new BigDecimal(invTrkId), vpAvailable, csrId, connection);		

	} // end method makeUnavailable

	/**
	 * Clears exception dates / code set for product
	 * @param productId
	 * @param connection
	 * @throws Exception
	 */
	private static void clearExceptionDates(String productId, Connection connection)
	throws Exception
	{
		DataRequest dataRequest = new DataRequest();
		dataRequest.setConnection(connection);
		dataRequest.setStatementID("CLEAR_EXCEPTION_DATES");
		dataRequest.addInputParam("IN_PRODUCT_ID", productId);
		dataRequest.addInputParam("IN_UPDATED_BY", "SYSTEM_INV_TRK");

		DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();


		Map outputs = (Map) dataAccessUtil.execute(dataRequest);

		String status = (String) outputs.get("OUT_STATUS");
		if( StringUtils.equals(status, "N") )
		{
			String message = (String) outputs.get("OUT_MESSAGE");
			throw new Exception(message);
		}
		logger.debug("Clear Exception Dates Successful");

	} //end method clearExceptionDates

	/**
	 * This method will insert a record into the ftd_apps.inv_trk_event_history table.
	 * @param invTrkId
	 * @param eventType
	 * @param availStartDate
	 * @param availEndDate
	 * @param connection
	 * @throws Exception
	 */
	private static void insertInvTrkEventHistory(BigDecimal invTrkId, String eventType, java.util.Date availStartDate, 
			java.util.Date availEndDate, Connection connection) throws Exception
			{
		DataRequest dataRequest = new DataRequest();
		dataRequest.setConnection(connection);
		dataRequest.setStatementID("INSERT_INV_TRK_EVENT_HISTORY");
		dataRequest.addInputParam("IN_INV_TRK_ID", invTrkId.longValue());
		dataRequest.addInputParam("IN_EVENT_TYPE", eventType);
		dataRequest.addInputParam("IN_UPDATED_BY", "SYSTEM_INV_TRK");
		dataRequest.addInputParam("IN_PRODUCT_AVAIL_START_DATE", availStartDate != null ? new java.sql.Date(availStartDate.getTime()) : null);
		dataRequest.addInputParam("IN_PRODUCT_AVAIL_END_DATE", availEndDate != null ? new java.sql.Date(availEndDate.getTime()) : null);

		DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();


		Map outputs = (Map) dataAccessUtil.execute(dataRequest);

		String status = (String) outputs.get("OUT_STATUS");
		if( StringUtils.equals(status, "N") )
		{
			String message = (String) outputs.get("OUT_MESSAGE");
			throw new Exception(message);
		}
		logger.debug("insert Inv Trk Event History Successful");

	} //end method insertInvTrkEventHistory

	/**
	 * Obtain product information
	 */
	private static CachedResultSet getProductInfo(String productId, Connection connection) throws Exception
	{
		Map paramMap = new HashMap();
		paramMap.put("IN_PRODUCT_ID",productId);

		DataRequest dataRequest = new DataRequest();
		dataRequest.setConnection(connection);
		dataRequest.setStatementID("GET_PRODUCT_BY_ID");
		dataRequest.setInputParams(paramMap);

		DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
		CachedResultSet crs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

		return crs;
	}    

	/**
	 * Insert a message into the PAS JMS queue.
	 * @param connection
	 * @param payload message payload
	 * @throws Exception
	 */
	private static void insertPASJMSMessage(Connection connection, String payload) 
	throws Exception
	{
		logger.info("Inserting PAS JMS Message of : " + payload);
		DataRequest dataRequest = new DataRequest();
		dataRequest.setConnection(connection);
		dataRequest.setStatementID("PIF_POST_A_MESSAGE");

		HashMap inputParams = new HashMap();
		inputParams.put("IN_QUEUE_NAME", "OJMS.PAS_COMMAND");
		inputParams.put("IN_CORRELATION_ID", "");
		inputParams.put("IN_PAYLOAD", payload);
		inputParams.put("IN_DELAY_SECONDS", 0L);
		dataRequest.setInputParams(inputParams);

		DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
		Map outputs = (Map)dataAccessUtil.execute(dataRequest);

		String status = (String)outputs.get("OUT_STATUS");
		if (status != null && status.equalsIgnoreCase("N"))
		{
			String message = (String)outputs.get("OUT_MESSAGE");
			throw new Exception(message);
		}

	}

	/**
	 * This method sends a message to the System Messenger.
	 * @param message
	 * @param errorCause
	 * @return message id
	 */
	private static String sendSystemMessage(String message, String errorCause, Connection connection) throws Exception {		
		String messageID = "";

		try {
			logger.error("Sending System Message: " + message);
			String messageSource = "SHIP PROCESSING";

			//build system vo
			SystemMessengerVO sysMessage = new SystemMessengerVO();
			sysMessage.setLevel(SystemMessengerVO.LEVEL_PRODUCTION);
			sysMessage.setSource(messageSource);
			sysMessage.setType("ERROR");
			sysMessage.setMessage(message);
			sysMessage.setSubject("VENDOR PRODUCT AVAILABILITY UTILITY ERROR - " + errorCause);

			SystemMessenger sysMessenger = SystemMessenger.getInstance();
			// Passing the flag to not close connection.
			messageID = sysMessenger.send(sysMessage, connection, false);

			if (messageID == null) {
				String msg = 
					"Error occured while attempting to send out a system message. Msg not sent: " + 
					message;
				logger.error(msg);
			}
		} finally {
		}

		return messageID;
	}

	/**********************************#12863 - NEW SKU-IT, AUTO SHUT DOWN CHECK ***********************************/
	/** A utility method used to check the product availability for a given product, ship date and/or vendor.
	 * @param productId
	 * @param vendorId
	 * @param isVenusStatusPending
	 * @param shipDate
	 * @param connection
	 * @throws Exception
	 */
	public static void checkVendorProductAvailability(String productId,
			String vendorId, boolean isVenusStatusPending, Date shipDate, Connection connection) throws Exception {		
		logger.info("Entered checkVendorProductAvailability()");
		try {			
			if (vendorId != null) {
				logger.debug("Vendor Id is included in ship processing request. Perform inventory and vendor_product availability checks.");
			}
			processAutoShutdown(productId, vendorId, isVenusStatusPending, shipDate, connection);
		} catch (Exception e) {
			StringBuffer errorMessage = new StringBuffer("Unable to process Auto shutdown check for product:");
			errorMessage.append(productId).append(", vendor: ").append(vendorId).append(" and shipdate: ").append(shipDate).append(".").append(e);
			logger.error(errorMessage.toString());
			sendSystemMessage(errorMessage.toString(), " AUTOSHUTDOWN CHECK FAILED ", connection);
		}
	}


	/** If there is no vendor Id included in the request, perform Period and product shutdown checks alone.
	 * If vendor Id included, perform inventory, vendor_product availability checks before performing period and product shutdown checks.
	 * @param productId
	 * @param vendorId
	 * @param isVenusStatusPending
	 * @param shipDate
	 * @param connection
	 * @throws Exception
	 */
	private static void processAutoShutdown(String productId,
			String vendorId, boolean isVenusStatusPending, Date shipDate, Connection connection) throws Exception {
		logger.info("Entered processAutoShutdown()");		
		InventoryVO inventoryVO = null;	
		
		if(!StringUtils.isEmpty(vendorId)) {			
			inventoryVO = getInventoryDetail(productId, vendorId, shipDate, connection);
			if(inventoryVO == null) {
				throw new Exception("Unable to retrieve the inventory detail for a given product:"
						+ productId + ", vendor:" + vendorId + ", shipDate: " + shipDate);
			}			
			performInventoryShutdown(inventoryVO, connection, false, isVenusStatusPending);	
			return;
		} 		
		
		inventoryVO = getProductPeriodInventory(productId, shipDate, null, connection);
		if(inventoryVO == null) {
			logger.error("Unable to get the product period inventory details for product : " + productId + ", for ship date: " + shipDate);
		}
		performPeriodShutdown(inventoryVO, VendorProductAvailabilityConstants.SYS_INV_TRK_USER,connection, false, isVenusStatusPending);		
	}
	
	/** Update the inventory status to Unavailable for each inventory record in the list.
	 * @param invTrkIds
	 * @param connection
	 * @throws Exception
	 */
	public static void makeInvUnavailable(List<String> invTrkIds, Connection connection) throws Exception {
		logger.info("Entered makeInvUnavailable()");
		if(invTrkIds == null || invTrkIds.size() == 0) {
			return;
		}
		for (String invTrkId : invTrkIds) {
			logger.debug("Recalculating Inventory status for: " + invTrkId);
				try {
					updateInventoryStatus(new BigDecimal(invTrkId), VendorProductAvailabilityConstants.UNAVAILABLE, connection);					
				} catch (Exception e) {
					logger.error("Error caught updating inventory status for inv id : " + invTrkId);
			}
		}
	}
	
	/** Process Vendor product availability and product availability check.
	 * @param productId
	 * @param vendors
	 * @param performProdShutdownCheck
	 * @param updatedBy	
	 * @param connection
	 * @return
	 * @throws Exception
	 */
	public static boolean makeVendorProdUnavailable(String productId, String vendorId, boolean performProdShutdownCheck, String updatedBy, Connection connection) throws Exception {
	    boolean productShutdownPerformed = false;
		logger.info("Entered makeVendorProdUnavailable()");
		
		performVendorShutdown(isVendProdInvAvailable(vendorId, productId, connection),
					vendorId, productId, true, updatedBy, connection);

		// Check Product shutdown
		if(performProdShutdownCheck) {
			InventoryVO prodInvVO = getProductInventory(productId, connection);
			if (prodInvVO != null) {
				productShutdownPerformed = performProductShutdown(prodInvVO, updatedBy, connection, true);
			}
		}
		return productShutdownPerformed;
	}

	/** This Method makes an inventory Unavailable. If this is the last available inventory for a vendor/product, 
	 * performs Vendor shutdown. If this is last inventory for the product, performs product shutdown.
	 * @param productId
	 * @param vendorId
	 * @param invTrkId
	 * @param vpAvailable
	 * @param updatedBy 
	 * @param connection
	 * @throws Exception
	 */
	private static void processMakeUnavailable(String productId,
			String vendorId, BigDecimal invTrkId, String vpAvailable,String updatedBy, Connection connection) throws Exception {
		logger.info("Entered processMakeUnavailable()");
		updateInventoryStatus(invTrkId, VendorProductAvailabilityConstants.UNAVAILABLE, connection);	
		boolean isVPAvailable = VendorProductAvailabilityConstants.ACTIVE.equals(vpAvailable) ? true : false;
		performVendorShutdown(isVendProdInvAvailable(vendorId, productId, connection), vendorId, productId, isVPAvailable, updatedBy, connection);
		// If Inventory is shutdown, perform period shutdown
		InventoryVO prodPeriodInvVO = getProductPeriodInventory(productId, null, invTrkId, connection);
		if(prodPeriodInvVO == null) {
			logger.error("processMakeUnavailable: Unable to get the product period inventory detail for product : " + productId + ", for inventory tracking id: " + invTrkId);
			return;
		}
		// for alerts, populate the vendor detail into prodPeriodInventoryVO.
		prodPeriodInvVO.setInvTrkId(invTrkId);
		prodPeriodInvVO.setVendorId(vendorId);
		prodPeriodInvVO.setVendorName(null);
		performPeriodShutdown(prodPeriodInvVO, updatedBy, connection, true, false);
	}

	/** Perform Inventory Shutdown if the inventory on hand for a vendor+product+period has reached its shutdown threshold defined.
	 * Update the inventory status to 'X'. This triggers PAS, so no need to insert PAS JMS message.
	 * Only when the inventory is shutdown, we will check for the vendor shutdown.
	 * If inventory has reached warning threshold, send an inventory warning alert and return.
	 * @param inventoryVO
	 * @param connection
	 * @param isMadeUnavailable
	 * @param isVenusStatusPending
	 * @throws Exception
	 */
	private static void performInventoryShutdown(InventoryVO inventoryVO, Connection connection, boolean isMadeUnavailable, boolean isVenusStatusPending) throws Exception {
		logger.info(" Entered performInventoryShutdown()");

		if(inventoryVO.getOnHand() <= inventoryVO.getWarningThreshold()) {
			logger.debug("Inventory " + inventoryVO.getInvTrkId() + " reached warning shutdown.");
			sendInventoryAlerts(inventoryVO, VendorProductAvailabilityConstants.INVENTORY_WARNING_ALERT, connection);
		}

		if(inventoryVO.getOnHand() <= inventoryVO.getShutdownThreshold()) {	
			logger.debug("Inventory " + inventoryVO.getInvTrkId() + " reached shutdown.");
			updateInventoryStatus(inventoryVO.getInvTrkId(), VendorProductAvailabilityConstants.SHUTDOWN, connection);			
			if(!isMadeUnavailable) {
				insertInvTrkEventHistory(inventoryVO.getInvTrkId(), VendorProductAvailabilityConstants.LOCATION_SHUTDOWN_EVENT, null, null, connection);		
				sendInventoryAlerts(inventoryVO, VendorProductAvailabilityConstants.LOC_SHUTDOWN_ALERT, connection);
			}
			
			// If Inventory is shutdown, perform vendor shutdown
			boolean isVendInvAvailable = isVendProdInvAvailable(inventoryVO.getVendorId(), inventoryVO.getProductId(), connection);
			performVendorShutdown(isVendInvAvailable, inventoryVO.getVendorId(), inventoryVO.getProductId(), 
					inventoryVO.isVendorProdAvailable(), VendorProductAvailabilityConstants.SYS_INV_TRK_USER, connection);			
		} 
		
		// If Inventory is shutdown, perform period shutdown
		InventoryVO prodPeriodInvVO = getProductPeriodInventory(inventoryVO.getProductId(), null, inventoryVO.getInvTrkId(), connection);
		if(prodPeriodInvVO == null) {
			logger.error("Unable to get the product period inventory detail for product : " + inventoryVO.getProductId() + ", for inventory tracking id: " + inventoryVO.getInvTrkId());
			return;
		}
		// for alerts, populate the vendor detail into prodPeriodInventoryVO.
		prodPeriodInvVO.setInvTrkId(inventoryVO.getInvTrkId());
		prodPeriodInvVO.setVendorId(inventoryVO.getVendorId());
		prodPeriodInvVO.setVendorName(inventoryVO.getVendorName());
		performPeriodShutdown(prodPeriodInvVO, VendorProductAvailabilityConstants.SYS_INV_TRK_USER, connection, false, isVenusStatusPending);
	}

	/** Perform Vendor shutdown if the given vendor has reached shutdown threshold for a product for all periods.
	 * isInvAvailable is true, if there is at-least one inventory in 'A' status we will not have to perform Vendor Shutdown. 
	 *  If not, check if the vendor_product.available is not already in 'N' status to update the same.
	 *  There is no need to trigger PAS here, by this time, each individual inventory shutdown might have triggered PAS.
	 * @param isInvAvailable
	 * @param vendorId
	 * @param productId
	 * @param vpAvailable
	 * @param updatedBy
	 * @param connection
	 * @throws Exception
	 */
	private static void performVendorShutdown(boolean isInvAvailable, String vendorId, 
			String productId, boolean vpAvailable, String updatedBy, Connection connection) throws Exception {
		logger.info("Entered performVendorShutdown()");		
		if(!isInvAvailable && vpAvailable) {
			logger.debug("Vendor: " + vendorId + ", reached shutdown for product: "
					+ productId + ". Updating vendor_product availability to 'N'.");			
			updateVendorProdAvailability(vendorId, productId, VendorProductAvailabilityConstants.INACTIVE, updatedBy, connection);
		}		
	}

	/** Perform Period shutdown if the total on hand orders (pending + non-pending) have reached total inventory shutdown 
	 * for a product for a given period. There are NO DB changes to Vendor_product or product_master, 
	 * as the product may still be available for other periods.
	 * Only when the period is shutdown, we will check for the product shutdown.
	 * @param prodPeriodInvVO
	 * @param updatedBy 
	 * @param connection
	 * @param isMadeUnavailable
	 * @param isVenusStatusPending
	 * @throws Exception
	 */
	private static void performPeriodShutdown(InventoryVO prodPeriodInvVO, String updatedBy, Connection connection, boolean isMadeUnavailable, boolean isVenusStatusPending) throws Exception {
		logger.info("Entered performPeriodShutdown()"); 
		if(prodPeriodInvVO.getOnHand() <= prodPeriodInvVO.getShutdownThreshold()) {
			logger.info("Inventory Period: "
					+ prodPeriodInvVO.getInvStartDate() + " - "	+ prodPeriodInvVO.getInvEndDate()
					+ ", reached shutdown for product: " + prodPeriodInvVO.getProductId() + ". Triggering PAS.");
			
			if(!prodPeriodInvVO.isFloristDeliverable() && prodPeriodInvVO.isProdAvailable() && isVenusStatusPending) {
				logger.info("Product is vendor deliverable, performing period shutdown for product: " + prodPeriodInvVO.getProductId());
				insertPASJMSMessage(connection, constructPayLoad(VendorProductAvailabilityConstants.PERIOD_SHUTDOWN_ALERT, 
						prodPeriodInvVO.getInvStartDate(), prodPeriodInvVO.getProductId(), prodPeriodInvVO.getVendorId()));
			}
			
			if(!isMadeUnavailable) {
				sendInventoryAlerts(prodPeriodInvVO, VendorProductAvailabilityConstants.PERIOD_SHUTDOWN_ALERT, connection);
			}

			//Perform product shutdown					
			performProductShutdown(prodPeriodInvVO.getProductId(), prodPeriodInvVO, updatedBy, connection, isMadeUnavailable);
		} 		
	}

	/** Performs Product Shutdown if the total product inventory on hand reached total shutdown defined for a product (for all periods, for all vendors).
	 * If product is FLORIST deliverable turn off the drop ship deliverable flag on product if it is not ON already.
	 * If the product is not FLORIST deliverable, turn off the product availability status if it is not ON already.
	 * @param prodInventoryVO
	 * @param updatedBy 
	 * @param connection
	 * @param isMadeUnavailable
	 * @throws Exception
	 */
	private static boolean performProductShutdown(InventoryVO prodInventoryVO, String updatedBy, Connection connection, boolean isMadeUnavailable) throws Exception {
		logger.info("Entered performProductShutdown()");
		String alertType = null;
		boolean productShutdownPerformed = false;
		
		if(prodInventoryVO.isNoInventory() || prodInventoryVO.getOnHand() <= prodInventoryVO.getShutdownThreshold()) {
			logger.debug("Product: " + prodInventoryVO.getProductId()
					+ ", reached shutdown for all periods. Last Vendor Assigned to order is: " + prodInventoryVO.getVendorId());			
			if(prodInventoryVO.isFloristDeliverable() && prodInventoryVO.isVendorDeliverable()) {	
				if(GeneralConstants.OE_PRODUCT_TYPE_SDFC.equals(prodInventoryVO.getProductType()) || 
						GeneralConstants.OE_PRODUCT_TYPE_SDG.equals(prodInventoryVO.getProductType())) {
					logger.debug("Product type: " + prodInventoryVO.getProductType() +", performing Dropship shutdown on product: " + prodInventoryVO.getProductId());
					updateProdDropshipDelivery(prodInventoryVO.getProductId(), VendorProductAvailabilityConstants.INACTIVE, updatedBy, connection);
					alertType = VendorProductAvailabilityConstants.DROPSHIP_SHUTDOWN_ALERT;		
				} else {
					logger.debug("Product type: " + prodInventoryVO.getProductType() +", Dropship shutdown is not performed for " + prodInventoryVO.getProductId());
				}
			} else if(!prodInventoryVO.isFloristDeliverable() && prodInventoryVO.isProdAvailable()) {
				logger.debug("Product is vendor deliverable, performing product shutdown for product: " + prodInventoryVO.getProductId());
				updateProductStatus(prodInventoryVO.getProductId(), VendorProductAvailabilityConstants.UNAVAILABLE, updatedBy, connection);
				alertType = VendorProductAvailabilityConstants.PROD_SHUTDOWN_ALERT;								
			}

			if(alertType == null) {
				logger.debug("Product is already shut down. No changes made.");
				return productShutdownPerformed;
			} else {
			    productShutdownPerformed = true;
			}

			if(!isMadeUnavailable) {
				sendInventoryAlerts(prodInventoryVO, alertType, connection);
				if(prodInventoryVO.getInvTrkId() != null) {
					insertInvTrkEventHistory(prodInventoryVO.getInvTrkId(), VendorProductAvailabilityConstants.PRODUCT_SHUTDOWN_EVENT, null, null, connection);
				}

				insertPASJMSMessage(connection, constructPayLoad(alertType, null, prodInventoryVO.getProductId(), prodInventoryVO.getVendorId()));
				
				sendNovatorFeed(connection, prodInventoryVO.getProductId());	
			}					
		}		
		return productShutdownPerformed;
	}

	/** Gets the Available inventory Id(s) for a given product, vendor irrespective of periods.
	 * Calls Procedure FTD_APPS.INV_TRK_PKG.GET_AVAIL_VENDOR_PROD_INV
	 * @param vendorId
	 * @param productId
	 * @param connection
	 * @return
	 * @throws Exception
	 */
	private static boolean isVendProdInvAvailable(String vendorId, String productId, Connection connection) throws Exception {
		logger.info(" Entered getVendorProductInventory()");
		StringBuffer statusLogMsg = new StringBuffer("GET vendor product inventory for product, ").append(productId)
		.append(", vendorId, : ").append(vendorId);		
		try{
			DataRequest dataRequest = new DataRequest();
			dataRequest.setConnection(connection);		
			dataRequest.setStatementID("GET_AVAIL_VENDOR_PROD_INV");
			dataRequest.addInputParam("IN_PRODUCT_ID", productId);
			dataRequest.addInputParam("IN_VENDOR_ID", vendorId);			

			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
			CachedResultSet result = (CachedResultSet) dataAccessUtil.execute(dataRequest);
			if(result.next()) {
				return true;			
			}
		} catch(Exception e) {
			throw new Exception(statusLogMsg.append("-> failed for reason, ").append(e.getMessage()).toString());
		}
		return false;
	}

	/** Gets the inventory detail for a given product, vendor and ship date.
	 * Calls Procedure FTD_APPS.INV_TRK_PKG.GET_INVENTORY_DETAIL
	 * @param productId
	 * @param vendorId
	 * @param shipDate
	 * @param connection
	 * @return
	 * @throws Exception
	 */
	private static InventoryVO getInventoryDetail(String productId, String vendorId, Date shipDate, Connection connection) throws Exception {
		logger.info("Entered getInventoryDetail()");
		if(vendorId == null) {
			return getProductPeriodInventory(productId, shipDate, null, connection);
		}
		StringBuffer statusLogMsg = new StringBuffer("GET inventory detail for product, ").append(productId)
		.append(", vendorId, : ").append(vendorId).append(", for ship date,").append(shipDate);
		InventoryVO inventoryVO = null;
		try{
			DataRequest dataRequest = new DataRequest();
			dataRequest.setConnection(connection);		
			dataRequest.setStatementID("GET_INVENTORY_DETAIL");
			dataRequest.addInputParam("IN_PRODUCT_ID", productId);
			dataRequest.addInputParam("IN_VENDOR_ID", vendorId);			
			dataRequest.addInputParam("IN_SHIP_DATE", new java.sql.Date(shipDate.getTime()));

			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
			CachedResultSet result = (CachedResultSet) dataAccessUtil.execute(dataRequest);
			if(result.next()) {
				inventoryVO = populateInventoryDetail(result);				
			}
		} catch(Exception e) {
			throw new Exception(statusLogMsg.append("-> failed for reason, ").append(e.getMessage()).toString());
		}
		return inventoryVO;
	}


	/** Gets the cumulative count of the inventories defined for a product for a period irrespective of vendor.
	 * Calls Procedure FTD_APPS.INV_TRK_PKG.GET_PROD_PERIOD_INV_DETAIL
	 * @param productId
	 * @param shipDate
	 * @param invTrkId 
	 * @param connection
	 * @return
	 * @throws Exception
	 */
	private static InventoryVO getProductPeriodInventory(String productId, Date shipDate, BigDecimal invTrkId, Connection connection) throws Exception {
		logger.info("Entered getProductPeriodInventory()");
		StringBuffer statusLogMsg = new StringBuffer(
		"GET product inventory for a period. Product: ").append(productId).append(", shipDate: ").append(shipDate);
		InventoryVO prodPeriodInvVO = null;
		try{
			DataRequest dataRequest = new DataRequest();
			dataRequest.setConnection(connection);		
			dataRequest.setStatementID("GET_PROD_PERIOD_INV_DETAIL");			
			dataRequest.addInputParam("IN_PRODUCT_ID", productId);
			if (shipDate!=null)
				dataRequest.addInputParam("IN_SHIP_DATE", new java.sql.Date(shipDate.getTime()));
			else
				dataRequest.addInputParam("IN_SHIP_DATE", null);
			dataRequest.addInputParam("IN_INV_TRK_ID", invTrkId);

			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
			CachedResultSet result = (CachedResultSet) dataAccessUtil.execute(dataRequest);
			if(result.next()) {
				prodPeriodInvVO = populateInventoryDetail(result);				
			}
		} catch(Exception e) {
			throw new Exception(statusLogMsg.append("-> failed for reason, ").append(e.getMessage()).toString());
		}
		return prodPeriodInvVO;
	}

	/** Gets the cumulative count of the inventories defined for a product irrespective of a period and vendor.
	 * Calls Procedure FTD_APPS.INV_TRK_PKG.GET_PROD_INV_DETAIL
	 * @param productId
	 * @param connection
	 * @return
	 * @throws Exception
	 */
	public static InventoryVO getProductInventory(String productId, Connection connection) throws Exception {
		logger.info("Entered getProductPeriodInventory()");
		StringBuffer statusLogMsg = new StringBuffer("GET product inventory for all the periods. Product: ").append(productId);
		InventoryVO inventoryVO = null;
		try{
			DataRequest dataRequest = new DataRequest();
			dataRequest.setConnection(connection);		
			dataRequest.setStatementID("GET_PROD_INV_DETAIL");			
			dataRequest.addInputParam("IN_PRODUCT_ID", productId);

			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
			CachedResultSet result = (CachedResultSet) dataAccessUtil.execute(dataRequest);
			if(result.next()) {
				inventoryVO = populateInventoryDetail(result);				
			}
		} catch(Exception e) {
			throw new Exception(statusLogMsg.append("-> failed for reason, ").append(e.getMessage()).toString());
		}
		return inventoryVO;
	}

	/** Populate the inventory detail retrieved from DB into a VO
	 * @param result
	 * @return
	 * @throws Exception
	 */
	private static InventoryVO populateInventoryDetail(CachedResultSet result) throws Exception {
		logger.info("Entered populateInventoryDetail()");
		
		if(result.getString("tot_inventory") == null && result.getString("product_Id") == null) {			
			logger.debug("Product doesnt exist.Inavlid request.");
			return null;						
		} 
		
		InventoryVO inventoryVO = new InventoryVO();
		
		inventoryVO.setProductId(result.getString("product_Id"));
		inventoryVO.setProductName(result.getString("product_name"));
		inventoryVO.setProductType(result.getString("product_type"));
		inventoryVO.setProdAvailable(VendorProductAvailabilityConstants.AVAILABLE
				.equals(result.getString("prod_status")) ? true : false);		
		inventoryVO	.setFloristDeliverable(VendorProductAvailabilityConstants.ACTIVE
				.equalsIgnoreCase(result.getString("is_florist_deliverable")) ? true : false);
		inventoryVO.setVendorDeliverable(VendorProductAvailabilityConstants.ACTIVE
				.equalsIgnoreCase(result.getString("is_vendor_deliverable")) ? true : false);
		
		if(result.getString("tot_inventory") == null) {
			logger.debug("Product exist. No Inventory uploaded for the product.");
			inventoryVO.setNoInventory(true);
			return inventoryVO;
		}
		
		inventoryVO.setVendorId(result.getString("vendor_id"));
		inventoryVO.setVendorName(result.getString("vendor_name"));		
		inventoryVO.setVendorProdAvailable(VendorProductAvailabilityConstants.ACTIVE
				.equals(result.getString("vendor_prod_status")) ? true : false);
		
		inventoryVO.setInvTrkId(
				!StringUtils.isEmpty(result.getString("inv_trk_id")) ? new BigDecimal(result.getString("inv_trk_id")) : new BigDecimal(0));
		inventoryVO.setInvStartDate(result.getDate("start_date"));
		inventoryVO.setInvEndDate(result.getDate("end_date"));
		inventoryVO.setWarningThreshold(result.getInt("warning_threshold_qty"));
		inventoryVO.setShutdownThreshold(result.getInt("shutdown_threshold_qty"));		
		
		// Total count will be available for all the three inventory/ period/ product level queries
		inventoryVO.setTotalInventory(result.getInt("tot_inventory"));		
		inventoryVO.setTotalShipped(result.getLong("tot_shipped"));
				
		// get inventory detail - just check f the inv_status
		if(result.getString("inv_status") != null) {
			inventoryVO.setOnHand(inventoryVO.getTotalInventory() - inventoryVO.getTotalShipped() - inventoryVO.getTotalPending());
			return inventoryVO;
		}
		
		if(result.getString("tot_order_count") != null) {
			inventoryVO.setTotalOrders(result.getLong("tot_order_count"));
			inventoryVO.setTotalPending(inventoryVO.getTotalOrders() - inventoryVO.getTotalShipped());			
		}
		
		inventoryVO.setOnHand(result.getLong("on_hand"));
		
		return inventoryVO;	
	}

	/** Update Inventory availability status. Calls procedure FTD_APPS.INV_TRK_PKG.UPDATE_INVENTORY_STATUS
	 * @param invTrkId
	 * @param invStatus
	 * @param connection
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	private static void updateInventoryStatus(BigDecimal invTrkId, String invStatus, Connection connection) throws Exception {
		logger.info("Entered updateInventoryStatus()");
		StringBuffer statusLogMsg = new StringBuffer("Update inventory status for id, ").append(invTrkId)
		.append(", to set status flag to : ").append(invStatus);
		DataRequest dataRequest = new DataRequest();
		dataRequest.setConnection(connection);
		dataRequest.setStatementID("UPDATE_INVENTORY_STATUS");
		dataRequest.addInputParam("IN_INV_TRK_ID", invTrkId);
		dataRequest.addInputParam("IN_STATUS", invStatus);
		dataRequest.addInputParam("IN_UPDATED_BY", VendorProductAvailabilityConstants.SYS_INV_TRK_USER);

		DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
		Map<String, String> outputs = (Map<String, String>) dataAccessUtil.execute(dataRequest);

		String updatedStatus = outputs.get("OUT_STATUS");
		if (StringUtils.equals(updatedStatus, "N")) {
			String message = outputs.get("OUT_MESSAGE");
			throw new Exception(statusLogMsg.append("-> failed for reason, ").append(message).toString());
		}
		logger.debug(statusLogMsg.append("-> Successfull").toString());
	}

	/** Update Vendor Product availability status. Calls procedure FTD_APPS.MISC_PKG.UPDATE_VENDOR_PROD_AVAILABLE
	 * @param vendorId
	 * @param productId
	 * @param availableStatus
	 * @param updatedBy
	 * @param connection
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	private static void updateVendorProdAvailability(String vendorId,
			String productId, String availableStatus, String updatedBy, Connection connection) throws Exception {
		logger.info("Entered updateVendorProdAvailability()");
		StringBuffer statusLogMsg = new StringBuffer("Update Vendor_Product for product, ").append(productId)
		.append(" and vendor, ").append(vendorId).append(", to set available flag to : ").append(availableStatus);
		DataRequest dataRequest = new DataRequest();
		dataRequest.setConnection(connection);
		dataRequest.setStatementID("UPDATE_VENDOR_PROD_AVAILABLE");
		dataRequest.addInputParam("IN_VENDOR_ID", vendorId);
		dataRequest.addInputParam("IN_PRODUCT_ID", productId);
		dataRequest.addInputParam("IN_AVAILBLE_STATUS", availableStatus);
		dataRequest.addInputParam("IN_UPDATED_BY", updatedBy);

		DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
		Map<String, String> outputs = (Map<String, String>) dataAccessUtil.execute(dataRequest);

		String updatedStatus = outputs.get("OUT_STATUS");
		if (StringUtils.equals(updatedStatus, "N")) {
			String message = outputs.get("OUT_MESSAGE");
			throw new Exception(statusLogMsg.append("-> failed for reason, ").append(message).toString());
		}
		logger.debug(statusLogMsg.append("-> Successfull").toString());
	}

	/** Update Product Drop ship delivery flag. Calls procedure FTD_APPS.MISC_PKG.UPDATE_PROD_DROPSHIP_DELIVERY 
	 * @param productId
	 * @param dropshipAvailable
	 * @param csrId 
	 * @param connection
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public static void updateProdDropshipDelivery(String productId, String dropshipAvailable , String csrId, Connection connection) throws Exception {
		logger.info("Entered updateProdDropshipDelivery()");
		StringBuffer statusLogMsg = new StringBuffer("Update Product: ")
		.append(productId).append(", to set ship_method_carrier: ").append(dropshipAvailable);
		DataRequest dataRequest = new DataRequest();
		dataRequest.setConnection(connection);
		dataRequest.setStatementID("UPDATE_PROD_DROPSHIP_DELIVERY");
		dataRequest.addInputParam("IN_PRODUCT_ID", productId);
		dataRequest.addInputParam("IN_DROPSHIP_AVAILBLE", dropshipAvailable);
		dataRequest.addInputParam("IN_UPDATED_BY", csrId);

		DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
		Map<String, String> outputs = (Map<String, String>) dataAccessUtil.execute(dataRequest);

		String updatedStatus = outputs.get("OUT_STATUS");
		if (StringUtils.equals(updatedStatus, "N")) {
			String message = outputs.get("OUT_MESSAGE");
			throw new Exception(statusLogMsg.append("-> failed for reason, ").append(message).toString());
		}
		logger.debug(statusLogMsg.append("-> Successfull").toString());	
	}

	/** Update Product Availability Status. Calls procedure FTD_APPS.MISC_PKG.UPDATE_PROD_STATUS 
	 * @param productId
	 * @param status
	 * @param updatedBy 
	 * @param connection
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public static void updateProductStatus(String productId, String status, String updatedBy, Connection connection) throws Exception {
		logger.info("Entered updateProductStatus()");
		StringBuffer statusLogMsg = new StringBuffer("Update Product: ").append(productId).append(", to set status: ").append(status);
		DataRequest dataRequest = new DataRequest();
		dataRequest.setConnection(connection);
		dataRequest.setStatementID("UPDATE_PROD_STATUS");
		dataRequest.addInputParam("IN_PRODUCT_ID", productId);
		dataRequest.addInputParam("IN_STATUS", status);
		dataRequest.addInputParam("IN_UPDATED_BY", updatedBy);

		DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
		Map<String, String> outputs = (Map<String, String>) dataAccessUtil.execute(dataRequest);

		String updatedStatus = outputs.get("OUT_STATUS");
		if (StringUtils.equals(updatedStatus, "N")) {
			String message = outputs.get("OUT_MESSAGE");
			throw new Exception(statusLogMsg.append("-> failed for reason, ").append(message).toString());
		}
		logger.debug(statusLogMsg.append("-> Successfull").toString());
	}

	/** Send inventory alerts for different alert types. Calls procedure FTD_APPS.INV_TRK_PKG.SEND_INVENTORY_ALERTS
	 * @param inventoryVO
	 * @param alertType
	 * @param connection
	 */
	@SuppressWarnings("unchecked")
	private static void sendInventoryAlerts(InventoryVO inventoryVO, String alertType, Connection connection) {
		logger.info("Entered sendInventoryAlertEmail()");
		StringBuffer statusLogMsg = new StringBuffer("Sending an inventory alert for: ").append(alertType);
		try {			
			DataRequest dataRequest = new DataRequest();
			dataRequest.setConnection(connection);
			dataRequest.setStatementID("SEND_INVENTORY_ALERTS");
			dataRequest.addInputParam("IN_INV_TRK_ID", inventoryVO.getInvTrkId());
			dataRequest.addInputParam("IN_PRODUCT_ID", inventoryVO.getProductId());
			dataRequest.addInputParam("IN_VENDOR_ID", inventoryVO.getVendorId());
			dataRequest.addInputParam("IN_PRODUCT_NAME", inventoryVO.getProductName());
			if(inventoryVO.getInvStartDate() != null && inventoryVO.getInvEndDate() != null) {
				dataRequest.addInputParam("IN_INV_START_DATE", new java.sql.Date(inventoryVO.getInvStartDate().getTime()));
				dataRequest.addInputParam("IN_INV_END_DATE", new java.sql.Date(inventoryVO.getInvEndDate().getTime()));
			} else {
				dataRequest.addInputParam("IN_INV_START_DATE", null);
				dataRequest.addInputParam("IN_INV_END_DATE", null);
			}
			dataRequest.addInputParam("IN_VENDOR_NAME", inventoryVO.getVendorName());
			dataRequest.addInputParam("IN_ON_HAND", inventoryVO.getOnHand());
			dataRequest.addInputParam("IN_TOT_INV", inventoryVO.getTotalOrders());
			dataRequest.addInputParam("IN_SHUTDOWN", inventoryVO.getShutdownThreshold());
			dataRequest.addInputParam("IN_FTD_ORDER_CNT", inventoryVO.getTotalShipped());
			dataRequest.addInputParam("IN_PEND_ORDER_CNT", inventoryVO.getTotalPending());
			dataRequest.addInputParam("IN_ALERT_TYPE", alertType);


			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
			Map<String, String> outputs = (Map<String, String>) dataAccessUtil.execute(dataRequest);

			String updatedStatus = outputs.get("OUT_STATUS");
			if (StringUtils.equals(updatedStatus, "N")) {
				String message = outputs.get("OUT_MESSAGE");
				throw new Exception(statusLogMsg.append("-> failed for reason, ").append(message).toString());
			}
			logger.debug(statusLogMsg.append("-> Successfull").toString());
			} catch (Exception e) {
				DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
				logger.error(statusLogMsg.append(" failed -> ").append(e.getMessage())	.append(", continue shutdown check.").toString());
				
				if (inventoryVO.getProductId() != null) {
					statusLogMsg.append("\n").append(inventoryVO.getProductId()).append(" ");
				}
				
				if (inventoryVO.getProductName() != null) {
					statusLogMsg.append(inventoryVO.getProductName()).append("\n");
				}
				
				if (inventoryVO.getInvStartDate() != null && inventoryVO.getInvEndDate() != null) {
					statusLogMsg.append(df.format(inventoryVO.getInvStartDate())).append(" ").append(df.format(inventoryVO.getInvEndDate()))
						.append(" - ").append(alertType).append("\n");
				}
				if (String.valueOf(inventoryVO.getTotalInventory()) != null && String.valueOf(inventoryVO.getTotalShipped()) != null
						&& String.valueOf(inventoryVO.getTotalPending()) != null) {
					statusLogMsg.append("TOTAL INVENTORY: ").append(inventoryVO.getTotalInventory()).append(".  SHIPPED/SHIPPING: ") 
						.append(inventoryVO.getTotalShipped()).append(".  PENDING: ").append(inventoryVO.getTotalPending());
				}
			try {
				sendSystemMessage(statusLogMsg.toString(), " INVENTORY TRACKING ALERTS FAILED ", connection);
			} catch (Exception e1) {
				logger.error("Error caught sending system alert for INVENTORY TRACKING ALERTS FAILED: " + e1.getMessage());
			}
		}
	}

	/** Constructs PAS Command pay load string for a given alert type.
	 * @param alertType
	 * @param deliveryDate
	 * @param productId
	 * @param vendorId
	 * @return
	 */
	private static String constructPayLoad(String alertType, Date deliveryDate, String productId, String vendorId) {
		
		if(alertType == null || (alertType.trim()).length() == 0) {
			return null;
		}
		
		StringBuffer payLoad = new StringBuffer();
		// default to MODIFIED_PRODUCT, change as per alert type/ requirement
		payLoad.append(VendorProductAvailabilityConstants.PAS_MODIFIED_PRODUCT).append(" ").append(productId).append(" Y N"); 
		return payLoad.toString();
	}

	/** Send Novator Feed.
	 * @param connection
	 * @param productId
	 * @throws Exception
	 */
	private static void sendNovatorFeed(Connection connection, String productId) {
		StringBuffer errorMessage = null;
		try{ 
			NovatorFeedProductUtil novatorFeedProdUtil = new NovatorFeedProductUtil();
            try {
                // This step was lost during the SKU-IT rewrite.
            	logger.info("Checking Upsell default for product id: " + productId);
                NovatorFeedUtil novatorFeedUtil = null;                            
                novatorFeedUtil = new NovatorFeedUtil();
                List <String>envKeys = novatorFeedUtil.getNovatorEnvironmentsAllowedAndChecked();                
                novatorFeedProdUtil.updateUpsellsForDisabledProduct(connection, productId, envKeys);
            } catch (Exception e1) {
            	errorMessage = new StringBuffer("Error updating upsells during shutdown of product " + productId);
            	sendSystemMessage(errorMessage.toString(), " PRODUCT UPSELL FAILED ", connection);
            }

			NovatorFeedResponseVO novatorFeedResponse = novatorFeedProdUtil.sendProductFeed(connection, productId, null);

			if (!novatorFeedResponse.isSuccess()) {
				errorMessage = new StringBuffer("Feed to Novator failed for product:");
				errorMessage.append(productId).append(" in VendorProductAvailabilityUtility for the following reason:")
				.append(novatorFeedResponse.getErrorString());
				throw new Exception(errorMessage.toString());
			}	
		} catch (Exception e) {
			try{
				logger.error("Error caught sending novator feed: " + e.getMessage());
				if(errorMessage == null) {
					errorMessage = new StringBuffer("Feed to Novator failed for product:");
					errorMessage.append(e.getMessage());
				}
				sendSystemMessage(errorMessage.toString(), " FEED TO NOVATOR FAILED ", connection);
			} catch(Exception e1) {
				logger.error("Error caught sending system message for novator feed failure: " + e1.getMessage());
			}
		}
	}

	/** Perform Product shutdown as an independent task.
	 * @param products
	 * @param updatedBy
	 * @param connection
	 * @throws Exception
	 */
	public static void performPeriodShutdown(List<String> products, String updatedBy, Connection connection) throws Exception {	
		logger.info("Entered performPeriodShutdown(products())");
		if(products == null || products.size() == 0) {
			logger.error("No product to perform product shutdown check.");
			return;
		}
		for (String productId : products) {
			logger.info("Check product shutdown for: " + productId);			
			performProductShutdown(productId, null, updatedBy, connection, true);
		}
		
	}
	
	/** Perform product shutdown only when there is no inventory period available.
	 * @param productId
	 * @param invVO
	 * @param updatedBy
	 * @param connection
	 * @param isMadeUnavailable
	 * @throws Exception 
	 */
	private static void performProductShutdown(String productId, InventoryVO invVO, String updatedBy, Connection connection, boolean isMadeUnavailable) throws Exception {
		// Check if all the inventory periods for the given product are Unavailable/ shutdown. only then perform product shutdown.
		List<Date> startDates = getProductPeriods(productId, connection);		
		// check if at-least one inventory period is available
		InventoryVO periodInvVO = null;			
		boolean periodAvailable = false;
		
		for (Date startDate : startDates) {
			if(startDate != null) {
				periodInvVO = getProductPeriodInventory(productId, startDate, null, connection);
				if(periodInvVO.getOnHand() > periodInvVO.getShutdownThreshold()) {
					periodAvailable = true;
					logger.debug("Product, " + productId +", has atleast one inventory available. Do not perform Product shutdown");
					break;
				}
			}
		}
		
		if(!periodAvailable) {
			InventoryVO prodInvVO = getProductInventory(productId, connection);
			if(prodInvVO == null) {
				logger.error("Unable to get the product inventory detail for product : " + productId);
				return;
			}
			
			if(invVO != null) {
				// update the required details for shutdown alerts
				prodInvVO.setInvTrkId(invVO.getInvTrkId());
				prodInvVO.setInvStartDate(invVO.getInvStartDate());
				prodInvVO.setInvEndDate(invVO.getInvEndDate());
				prodInvVO.setVendorId(invVO.getVendorId());
				prodInvVO.setVendorName(invVO.getVendorName());
			}
			
			performProductShutdown(prodInvVO, updatedBy, connection, isMadeUnavailable);			
			
		}		
	}

	/** Get all the inventory periods (non-past) for a given product
	 * @param productId
	 * @param connection
	 * @return
	 * @throws Exception 
	 */
	private static List<Date> getProductPeriods(String productId, Connection connection) throws Exception {
		logger.info("Entered getProductPeriods()");
		
		StringBuffer statusLogMsg = new StringBuffer("GET product periods for product, ").append(productId);
		List<Date> periods = null;
		try{
			DataRequest dataRequest = new DataRequest();
			dataRequest.setConnection(connection);		
			dataRequest.setStatementID("GET_PRODUCT_PERIODS");
			dataRequest.addInputParam("IN_PRODUCT_ID", productId);
			
			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
			CachedResultSet result = (CachedResultSet) dataAccessUtil.execute(dataRequest);
			
			periods = new ArrayList<Date>();
			while(result.next()) {
				logger.debug("Prod Period: " + result.getDate("start_date") + " -- " + result.getDate("end_date"));
				periods.add(result.getDate("start_date")); 
			}
			
		} catch(Exception e) {
			throw new Exception(statusLogMsg.append("-> failed for reason, ").append(e.getMessage()).toString());
		}
		return periods;
	}
	
	/**	 
	 * @return
	 */
	private static int getShipNDelDaysDiff() {			
		int maxDays = VendorProductAvailabilityConstants.DEFAULT_SHIP_DEL_DAYS_DIFF;
		try {
			ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
			maxDays = Integer.parseInt(configUtil.getFrpGlobalParm(VendorProductAvailabilityConstants.PAS_CONFIG, VendorProductAvailabilityConstants.ADD_DAYS_KEY));
		} catch (Exception e) {
			logger.error("Error caught getting maximum difference between ship and delivery dates, " + e.getMessage());
			logger.error("Setting default value as " + VendorProductAvailabilityConstants.DEFAULT_SHIP_DEL_DAYS_DIFF);
		}
		return maxDays;
	}
	
	/**	
	 * @return
	 */
	private static int getInvStartNEndDaysDiff() {			
		int addDays = VendorProductAvailabilityConstants.DEFAULT_INV_START_END_DIFF;
		try {
			ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
			addDays = Integer.parseInt(configUtil.getFrpGlobalParm(VendorProductAvailabilityConstants.PAS_CONFIG, VendorProductAvailabilityConstants.MAX_DAYS_KEY));
		} catch (Exception e) {
			logger.error("Error caught getting difference between inventory start and end dates, " + e.getMessage());
			logger.error("Setting default value as " + VendorProductAvailabilityConstants.DEFAULT_INV_START_END_DIFF);
		}
		return addDays;
	}
	
	/**********************************#12863 - NEW SKU-IT, CHANGES END HERE ***********************************/
}


