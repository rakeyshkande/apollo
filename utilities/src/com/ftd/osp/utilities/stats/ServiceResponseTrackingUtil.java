package com.ftd.osp.utilities.stats;

import java.sql.Connection;
import java.util.Date;
import java.util.Map;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.ServiceResponseTrackingVO;

public class ServiceResponseTrackingUtil {

	private static final String INSERT_SERVICE_RESPONSE = "INSERT_SERVICE_RESPONSE";
    private static final String STATUS_PARAM = "OUT_STATUS";
    private static final String MESSAGE_PARAM = "OUT_MESSAGE"; 

    private static Logger logger = new Logger("com.ftd.osp.utilities.stats.ServiceResponseTracking");
    
    public ServiceResponseTrackingUtil() {
    }    

    public void insert(Connection connection, ServiceResponseTrackingVO srtVO) throws Exception
    {
        if(connection == null) {
            throw new Exception("Connection not set");
        }
    
        DataAccessUtil dau = DataAccessUtil.getInstance();
        DataRequest dataRequest = new DataRequest();
        
        try {
            dataRequest.setConnection(connection);
            dataRequest.addInputParam("IN_SERVICE_NAME", srtVO.getServiceName());
            dataRequest.addInputParam("IN_SERVICE_METHOD", srtVO.getServiceMethod());
            dataRequest.addInputParam("IN_TRANSACTION_ID", srtVO.getTransactionId());
            dataRequest.addInputParam("IN_RESPONSE_TIME", srtVO.getResponseTime());
            
            java.sql.Timestamp createdOn = new java.sql.Timestamp(srtVO.getCreatedOn().getTime());
            dataRequest.addInputParam("IN_CREATED_ON", createdOn);
            
            dataRequest.addInputParam(STATUS_PARAM, "");
            dataRequest.addInputParam(MESSAGE_PARAM, "");

            dataRequest.setStatementID(INSERT_SERVICE_RESPONSE);
            Map outputs = (Map) dau.execute(dataRequest);
            dataRequest.reset();
      
            String status = (String) outputs.get(STATUS_PARAM);
            String outMsg = null;
            if(status.equals("N")) {
                outMsg = (String) outputs.get(MESSAGE_PARAM);
                throw new Exception(outMsg);
            }
        } catch(Exception ex) {
        	logger.error("Error occurred: " + ex.getStackTrace());
            throw ex;
        }
    }
    
    public void insertWithRequest(Connection connection, ServiceResponseTrackingVO srtVO) throws Exception
    {
        if(connection == null) {
            throw new Exception("Connection not set");
        }
    
        DataAccessUtil dau = DataAccessUtil.getInstance();
        DataRequest dataRequest = new DataRequest();
        
        try {
            dataRequest.setConnection(connection);
            dataRequest.addInputParam("IN_SERVICE_NAME", srtVO.getServiceName());
            dataRequest.addInputParam("IN_SERVICE_METHOD", srtVO.getServiceMethod());
            dataRequest.addInputParam("IN_TRANSACTION_ID", srtVO.getTransactionId());
            dataRequest.addInputParam("IN_RESPONSE_TIME", srtVO.getResponseTime());
                       
            java.sql.Timestamp createdOn = new java.sql.Timestamp(srtVO.getCreatedOn().getTime());
            dataRequest.addInputParam("IN_CREATED_ON", createdOn);
            dataRequest.addInputParam("IN_REQUEST", srtVO.getRequest());
            dataRequest.addInputParam("IN_RESPONSE", srtVO.getResponse());
            
            dataRequest.addInputParam(STATUS_PARAM, "");
            dataRequest.addInputParam(MESSAGE_PARAM, "");

            dataRequest.setStatementID("INSERT_SERVICE_RESPONSE_REQUEST");
            Map outputs = (Map) dau.execute(dataRequest);
            dataRequest.reset();
      
            String status = (String) outputs.get(STATUS_PARAM);
            String outMsg = null;
            if(status.equals("N")) {
                outMsg = (String) outputs.get(MESSAGE_PARAM);
                throw new Exception(outMsg);
            }
        } catch(Exception ex) {
        	logger.error("Error occurred: " + ex.getStackTrace());
            throw ex;
        }
    }
    
	/**
	 * @param transactionId
	 * @param serviceMethod
	 * @param startTime
	 * @param connection
	 * @param service
	 */
	public void logServiceResponseTracking(String transactionId, String serviceMethod, 
			long startTime, Connection connection, String service) {
		try {
			
			long responseTime = System.currentTimeMillis() - startTime;
			ServiceResponseTrackingVO srtVO = new ServiceResponseTrackingVO();
			srtVO.setServiceName(service);
			srtVO.setServiceMethod(serviceMethod);
			if(transactionId == null) {
				transactionId = "NA";				
			}
			srtVO.setTransactionId(transactionId);
			srtVO.setResponseTime(responseTime);
			srtVO.setCreatedOn(new Date());
			insert(connection, srtVO);
			
		} catch (Exception e) {
			logger.error("Exception occured while persisting service tracking record for service: " + service);
		}

	}
    
}