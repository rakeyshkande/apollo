package com.ftd.osp.utilities.stats;

public class OrderDetailStates
{
    public static final int NEW = 3000;

    public static final int QMS_IN = 3001;
    public static final int QMS_OUT = 3002;
    public static final int OV_IN = 3003;
    public static final int OV_OUT = 3004;

    public static final int SCRUB_IN = 3005;
    public static final int SCRUB_OUT = 3006;
    public static final int PENDING_IN = 3007;
    public static final int PENDING_OUT = 3008;
    public static final int REINSTATE_IN = 3009;
    public static final int REINSTATE_OUT = 3010;

    public static final int COMPLETE = 3011;
}