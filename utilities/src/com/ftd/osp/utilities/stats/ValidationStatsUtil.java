package com.ftd.osp.utilities.stats;

import com.ftd.osp.utilities.dataaccess.*;
import org.xml.sax.*;
import javax.xml.parsers.*;
import java.io.*;
import java.sql.*;
import com.ftd.osp.utilities.systemmessenger.exception.SystemMessengerException;
import com.ftd.osp.utilities.dataaccess.valueobjects.*;
import java.util.*;
import com.ftd.osp.utilities.j2ee.*;
import java.text.*;
import com.ftd.osp.utilities.plugins.*;

public class ValidationStatsUtil 
{
    private static final String INSERT_UNIQUE_VALIDATION_STATS = "INSERT_UNIQUE_VALIDATION_STATS";
    private static final String INSERT_VALIDATION_REASON_STATS = "INSERT_VALIDATION_REASON_STATS";
    private static final String OUT_STATS_ID = "RegisterOutParameterId";
    private static final String STATUS_PARAM = "RegisterOutParameterStatus";
    private static final String MESSAGE_PARAM = "RegisterOutParameterMessage"; 

    private static Logger logger = new Logger("com.ftd.osp.utilities.stats.ValidationStatsUtil");

    public ValidationStatsUtil()
    {
    }

    public void insertReseaon(String orderDetailId, String guid, String reason, String value, Connection connection)
                //throws SAXException, ParserConfigurationException, 
                //IOException, SQLException, SystemMessengerException, Exception
    {
       
        try
        {
            DataAccessUtil dau = DataAccessUtil.getInstance();
            DataRequest dataRequest = new DataRequest();
            
            //conn.setAutoCommit(false);

            java.util.Date today = new java.util.Date();
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
            String time = Long.toString(today.getTime());
            java.sql.Date date = new java.sql.Date(today.getTime());

            dataRequest.setConnection(connection);
            dataRequest.addInputParam("IN_EXTERNAL_ORDER_NUMBER", orderDetailId);
            dataRequest.addInputParam("IN_RELATOR", guid);
            dataRequest.addInputParam(OUT_STATS_ID, "");
            dataRequest.addInputParam(STATUS_PARAM, "");
            dataRequest.addInputParam(MESSAGE_PARAM, "");
            dataRequest.setStatementID(INSERT_UNIQUE_VALIDATION_STATS);
            Map outputs = (Map) dau.execute(dataRequest);
            dataRequest.reset();
      
            String status = (String) outputs.get(STATUS_PARAM);
            String outValId = (String) outputs.get(OUT_STATS_ID);
            String outMsg = null;
            if(status != null && !status.equals("Y"))
            {
                outMsg = (String) outputs.get(MESSAGE_PARAM);
                throw new Exception(outMsg);
            }
            else
            {
                // insert reason row
                dataRequest = new DataRequest();
                dataRequest.setConnection(connection);
                dataRequest.addInputParam("IN_STATS_VALIDATION_ID", outValId);
                dataRequest.addInputParam("IN_REASON", reason);
                dataRequest.addInputParam("IN_VALUE", value);
                
                dataRequest.addInputParam(STATUS_PARAM, "");
                dataRequest.addInputParam(MESSAGE_PARAM, "");
                dataRequest.setStatementID(INSERT_VALIDATION_REASON_STATS);
                outputs = (Map) dau.execute(dataRequest);
                dataRequest.reset();
      
                status = (String) outputs.get(STATUS_PARAM);
                outMsg = null;
                if(status.equals("N"))
                {
                    outMsg = (String) outputs.get(MESSAGE_PARAM);
                    throw new Exception(outMsg);
                }                                
            }
        }
        catch(Exception e)
        {
            logger.error(e);
        }
     
    }
}