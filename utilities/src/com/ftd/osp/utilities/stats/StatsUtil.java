package com.ftd.osp.utilities.stats;

import com.ftd.osp.utilities.order.vo.*;
import com.ftd.osp.utilities.dataaccess.*;
import org.xml.sax.*;
import javax.xml.parsers.*;
import java.io.*;
import java.sql.*;
import com.ftd.osp.utilities.systemmessenger.exception.SystemMessengerException;
import com.ftd.osp.utilities.dataaccess.valueobjects.*;
import java.util.*;
import com.ftd.osp.utilities.j2ee.*;
import java.text.*;
import com.ftd.osp.utilities.plugins.*;
//import com.ftd.osp.utilities.dataaccess.util.*;

public class StatsUtil 
{
    private OrderVO order = null;
    private OrderDetailsVO item = null;
    private String relator = null;
    private int state = -1;
    

    private static final String INSERT_STATS = "INSERT_STATS";
    private static final String STATUS_PARAM = "RegisterOutParameterStatus";
    private static final String MESSAGE_PARAM = "RegisterOutParameterMessage"; 

    private static Logger logger = new Logger("com.ftd.osp.utilities.stats.StatsUtil");
    
    public StatsUtil()
    {
    }    

    public void setOrder(OrderVO order)
    {
        this.order = order;
    }

    public void setItem(OrderDetailsVO item)
    {
        this.item = item;
    }    

    public void setRelator(String relator)
    {
        this.relator = relator;
    }

    public void setState(int state)
    {
        this.state = state;
    }

    public void insert(Connection connection) throws SAXException, ParserConfigurationException, 
                IOException, SQLException, SystemMessengerException, Exception
    {
        if(connection == null)
        {
            throw new Exception("Connection not set in StatUtil");
        }
    
        if(this.order == null || this.state == -1)
        {
            throw new Exception("order and state cannot be null");
        }

        // If the credit card is 4111111111111111
        String testOrder = "N";
        if(order.getPayments() != null && order.getPayments().size() > 0)
        {
            PaymentsVO payment = null;
            CreditCardsVO creditCard = null;
            Iterator it = order.getPayments().iterator();

            while(it.hasNext())
            {
                payment = (PaymentsVO)it.next();
                if(payment.getCreditCards() != null && payment.getCreditCards().size() > 0)
                {
                     creditCard = (CreditCardsVO)payment.getCreditCards().get(0);

                     if(creditCard.getCCNumber() != null && creditCard.getCCNumber().equals("4111111111111111"))
                     {
                         testOrder = "Y";
                     }
                }
            }            
        }

        DataAccessUtil dau = DataAccessUtil.getInstance();
        DataRequest dataRequest = new DataRequest();
        

        java.util.Date today = new java.util.Date();
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        String time = Long.toString(today.getTime());
        Timestamp date = new Timestamp(today.getTime());

        try
        {
            dataRequest.setConnection(connection);
            dataRequest.addInputParam("IN_MASTER_ORDER_NUMBER", order.getMasterOrderNumber());
            dataRequest.addInputParam("IN_EXTERNAL_ORDER_NUMBER", item.getExternalOrderNumber());
            dataRequest.addInputParam("IN_STATUS", item.getStatus());
            dataRequest.addInputParam("IN_STATE", Integer.toString(state));
            dataRequest.addInputParam("IN_TIME", time);
            dataRequest.addInputParam("IN_CSR_ID", order.getCsrId());
            dataRequest.addInputParam("IN_ORIGIN", order.getOrderOrigin());
            dataRequest.addInputParam("IN_DATE", date);
            dataRequest.addInputParam("IN_RELATOR", relator);
            dataRequest.addInputParam("IN_TEST_ORDER_FLAG", testOrder);
            dataRequest.addInputParam(STATUS_PARAM, "");
            dataRequest.addInputParam(MESSAGE_PARAM, "");
            dataRequest.setStatementID(INSERT_STATS);
            Map outputs = (Map) dau.execute(dataRequest);
            dataRequest.reset();
      
            String status = (String) outputs.get(STATUS_PARAM);
            String outMsg = null;
            if(status.equals("N"))
            {
                outMsg = (String) outputs.get(MESSAGE_PARAM);
                throw new Exception(outMsg);
            }
        }
        catch(Exception ex)
        {
          throw ex;
        }
    }
}