/**
 * Common methods for all components making REST calls to microservices.
 */
package com.ftd.osp.utilities;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.io.IOException;
import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.net.ssl.SSLContext;

import org.apache.commons.lang.StringUtils;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.scheme.SchemeSocketFactory;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.PoolingClientConnectionManager;
import org.apache.http.impl.conn.SingleClientConnManager;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;

import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;

/**
 * @author gsaluja
 *
 */
public class RESTUtils {
	public static String ACCEPT = "Accept";
	public static String CONTENT_TYPE = "Content-Type";
	public static String APPLICATION_JSON = "application/json";
	public static final String SERVICE_CONTEXT = "SERVICE";
	public static final String API_AUTH_HEADER = "APIAuthorization";
	public String serviceName;
	public boolean throwExceptions;
	public boolean hasSensitiveContent;
	private UUID uuid;
	private static PoolingClientConnectionManager connectionManager;
	
	private static synchronized ClientConnectionManager getConnectionManager() {
		if (RESTUtils.connectionManager == null) {
			//Found something of interests.
			//https://stackoverflow.com/questions/28391798/how-to-set-tls-version-on-apache-httpclient
			SSLContext sslContext = null;
			try {
				sslContext = SSLContext.getInstance("TLSv1.2");
			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				logger.error(e.getMessage());
				logger.error("Could not create a pooling connection manager");
			}
			try {
				sslContext.init(null, null, new SecureRandom());
			} catch (KeyManagementException e) {
				// TODO Auto-generated catch block
				logger.error(e.getMessage());
				logger.error("Could not configure a pooling connection manager");
			}
	        SchemeSocketFactory sf = new SSLSocketFactory(sslContext);
	        Scheme httpsScheme = new Scheme("https", 443, sf);
	        SchemeRegistry schemeRegistry = new SchemeRegistry();
	        schemeRegistry.register(httpsScheme);
			RESTUtils.connectionManager = new PoolingClientConnectionManager(schemeRegistry);
			//TODO : make this from global parms
			connectionManager.setMaxTotal(100);
			connectionManager.setDefaultMaxPerRoute(100);
		}
		return RESTUtils.connectionManager;
	}

	
	private static Logger logger = new Logger(RESTUtils.class.getName());
	
	
	public RESTUtils(String serviceName, boolean throwExceptions, boolean hasSensitiveContent) {
		this.serviceName = serviceName;
		this.throwExceptions = throwExceptions;
		this.hasSensitiveContent = hasSensitiveContent;
	}
	
	
	
	/*
	 * Common things to do when executing Http Request
	 */
	public String executeRequest(HttpUriRequest request, int serviceTimeout, String secret) throws Exception{
		HttpClient httpclient = null;
		HttpResponse httpResponse = null;
		StringBuffer errorMessage = null;
		int responseCode = 0;
		String responseBody = null;
		
		try {
			uuid = UUID.randomUUID();
			logger.info("Trace ID for request: "+ uuid); 

			httpclient = new DefaultHttpClient(RESTUtils.getConnectionManager());

			request.addHeader(ACCEPT, APPLICATION_JSON);
			request.addHeader(CONTENT_TYPE, APPLICATION_JSON);
			request.addHeader(API_AUTH_HEADER, "Bearer " + RESTUtils.computeSignature(request, secret, "apollo"));

			HttpParams httpParams = httpclient.getParams();
			HttpConnectionParams.setConnectionTimeout(httpParams, serviceTimeout);
			HttpConnectionParams.setSoTimeout(httpParams, serviceTimeout);

			logRequestInfo(request);

			try {
			   httpResponse = httpclient.execute(request);
			} catch (SocketTimeoutException e) {			   
			   logger.error("Socket timeout - going to try one more time");
			   httpResponse = httpclient.execute(request);
			}

			int result = httpResponse.getStatusLine().getStatusCode();
			HttpEntity entity = httpResponse.getEntity();
			try {
				responseBody = EntityUtils.toString(entity);
			} catch (Exception e) {
				if (errorMessage == null) {
					errorMessage = new StringBuffer();
				}
				errorMessage = errorMessage
						.append(String.format("Exception caught reading Response from %s Service . ", serviceName))
						.append("URL: ")
						.append(request.getRequestLine())
						.append("ErrorCode: " + result);
			}
			if (result != HttpStatus.SC_OK) {
				//There might be more info for non critical Error Come up with a popup describing what went wrong
				if (responseBody != null) {
				    logger.info("responseBody: " + responseBody);
					ObjectMapper mapper = new ObjectMapper();
					JsonNode node = mapper.readTree(responseBody);

					if (node!=null) {
						if (errorMessage==null) {
							errorMessage = new StringBuffer();
						}
						String svcError = node.get("errors").get(0).get("message").toString();
						errorMessage.append(svcError.substring(1, svcError.length()-1));
						responseCode = result;
					}
					logger.error(String.format("%s Service was not OK :", serviceName)
							+ httpResponse.getStatusLine()
							+ responseBody);
					logger.error("Notified : " + errorMessage);
				}
			}
		} catch (UnknownHostException e) {
			errorMessage = new StringBuffer(
					String.format("Exception caught calling %s Service . Incorrect URI. ", serviceName))
					.append(e.getMessage());
			logger.error(errorMessage.toString(), e);
			handleExceptions(e);
		} catch (SocketTimeoutException e) {
			errorMessage = new StringBuffer(
					String.format("Exception caught calling %s Service . %s Service is taking more time to respond. Closing the connection. ",
							serviceName, serviceName)).append(e.getMessage());
			logger.error(errorMessage.toString(), e);
			handleExceptions(e);
		} catch (ConnectTimeoutException e) {
			errorMessage = new StringBuffer(
					String.format("Exception caught calling %s Service . Unable to connect to %s Service within the specified time.",
							serviceName, serviceName)).append(e.getMessage());
			logger.error(errorMessage.toString(), e);
			handleExceptions(e);
		} catch (ConnectException e) {
			errorMessage = new StringBuffer(
					String.format("Exception caught calling %s Service . Unable to connect to %s Service within the specified time.", 
							serviceName, serviceName)).append(e.getMessage());
			logger.error(errorMessage.toString(), e);
			handleExceptions(e);
		} catch (Exception e) {
			String detailMessage = e.getMessage() == null ? "Unknown" : e.getMessage();
			errorMessage = new StringBuffer(
					String.format("Exception caught calling %s Service : ", serviceName)).append(detailMessage);
			logger.error(errorMessage.toString(), e);
			handleExceptions(e);
		} finally {
			if (errorMessage != null
					&& !StringUtils.isEmpty(errorMessage.toString())) {
				//errorMessage.append(getCallerInformation());
				sendSystemMessage(String.format("%s%s%s%s%s%s%s", 
						errorMessage.toString(),
						System.getProperty("line.separator") + "Calling:" + System.getProperty("line.separator"), 
						request.getRequestLine(),
						System.getProperty("line.separator") + "Trace ID:" + System.getProperty("line.separator"), 
						uuid,
						System.getProperty("line.separator") + "Called From: " +  System.getProperty("line.separator"),
						getCallerInformation()
						));
				//Notify the caller of non critical error.
				Throwable cause = new Throwable(String.valueOf(responseCode));
				IllegalStateException ex = new IllegalStateException(errorMessage.toString(), cause);
				throw ex;
			}
			logger.info(String.format("%s Service Retrieve request Ended at %d", serviceName, System.currentTimeMillis()));
		}
		// logger.info("responseBody: " + responseBody);
		logger.info("Response : " + responseBody.toString().replaceAll("\\d{12,19}", "XXXX"));

		return responseBody;
	}
	


	
	private Object getCallerInformation() {
		StringBuilder frame = new StringBuilder();
		StackTraceElement[] sfe = Thread.currentThread().getStackTrace();
		for(int i=2; i< sfe.length; i++) {
			 frame.append(sfe[i].toString());
			 frame.append(System.getProperty("line.separator"));
		}
		return frame.toString();
	}

	private void handleExceptions(Exception e) throws Exception {
		if(throwExceptions) {
			throw e;
		}
	}
	
	/*
	 * Helper to retrieve values from global parm and secure properties.
	 */
	public static String getConfigParamValue(String param, boolean isSecure) {
		ConfigurationUtil configurationUtil = null;
		String serviceParam = null;
		try {
			configurationUtil = ConfigurationUtil.getInstance();
			if (isSecure) {
				serviceParam = configurationUtil.getSecureProperty(SERVICE_CONTEXT, param);
			} else {
				serviceParam = configurationUtil.getFrpGlobalParm(SERVICE_CONTEXT, param);
			}
		} catch (Exception e) {
			new StringBuffer("Parsing Exception fetching configuration values from DB.  ")
					.append(e.getMessage());
		}
		if(serviceParam == null) {
			logger.warn(String.format("Error fetching %s from config store", param));
		}
		return serviceParam;
	}
	
	private void logRequestInfo(HttpUriRequest request) {
		logger.info("Request :  " + request.getRequestLine());
		logger.info("Headers:");
		int count = 1;
		for (Header header : request.getAllHeaders()) {
			logger.info("[header" + count++ + "]" + header.getName() + ":"
					+ header.getValue());
		}
		if (request instanceof HttpPost) {
			logger.info(hasSensitiveContent? "Obfuscated Body" : "Body:");
			try {
				String content = EntityUtils.toString(((HttpPost) request).getEntity());
				logger.info(hasSensitiveContent ? StringUtils.repeat("*", content.length()) : content);
			} catch (Exception e) {
				logger.error(e.getMessage());
			}
		}
	}
	
	
	 /**
     * Convenient method to send a system message
     * 
     * @param errorMessage
     * @throws SQLException
     */
    private void sendSystemMessage(String errorMessage) {
        logger.info("Error encountered, sendSystemMessage(..)");

        Connection connection = null;
        try {
            connection = DataSourceUtil.getInstance().getConnection("ORDER SCRUB");
            SystemMessengerVO sysMessage = new SystemMessengerVO();
            sysMessage.setLevel(SystemMessengerVO.LEVEL_PRODUCTION);
            sysMessage.setSource(String.format("%s UTIL", serviceName.toUpperCase()));
            sysMessage.setType("ERROR");
            sysMessage.setMessage(errorMessage);
            SystemMessenger.getInstance().send(sysMessage, connection);

        } catch (Throwable t) {
            logger.error("Sending system message failed. Message: "
                    + errorMessage);
            logger.error(t);
        } finally {
            closeConnection(connection);
        }
    }
    
    /**
     * Close connection.
     * 
     * @param connection
     */
    public static void closeConnection(Connection connection) {
        try {
            if (connection != null && !connection.isClosed()) {
                connection.close();
            }
        } catch (Exception e) {
            logger.error("Unable to close the connection." + e.getMessage());
        }
    }
	
	/*
	 * Gnerate a jwt token on URL from /internal onwards as a subject.
	 */
	public static String computeSignature(HttpUriRequest request, String secret, String issuer)
			throws IOException {
		int contentLenght = -1;
		if (request instanceof HttpPost) {
			contentLenght = EntityUtils.toString(
					((HttpPost) request).getEntity()).length();
		} else if (request instanceof HttpPut) {
			contentLenght = EntityUtils.toString(
					((HttpPut) request).getEntity()).length();
		}

		String uri = request.getURI().toString();
		String relativeURI = uri.substring(uri.indexOf(request.getURI().getPath()), uri.length());
		logger.info("request PATH: "+ relativeURI);		
		
		
		String subject = relativeURI + ">" + contentLenght;
		logger.info("Subject : " + subject);
		//String secret = getConfigParamValue(GIFT_CODE_SERVICE_SECRET, true);
		String compactJws = Jwts.builder().setSubject(subject)
				.setIssuer(issuer).setIssuedAt(new Date())
				.signWith(SignatureAlgorithm.HS512, secret.getBytes())
				.compact();
		return compactJws;
	}
	
	
}
