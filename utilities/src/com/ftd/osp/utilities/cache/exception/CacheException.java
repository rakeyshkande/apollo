package com.ftd.osp.utilities.cache.exception;

public class CacheException extends oracle.ias.cache.CacheException 
{
    public CacheException()
    {
        super();
    }

   /**
   * Constructs a new CacheException with a message string
   * @param message
   */
    public CacheException(String message)
    {
        super(message);
    }
    
  /**
   * Constructs a CacheException with a message string, and a base exception
   * 
   * @param message
   * @param ex
   */
    public CacheException(String message, Exception ex)
    {
      super(message, ex);
    }
    
}