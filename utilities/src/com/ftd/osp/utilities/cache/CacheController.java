package com.ftd.osp.utilities.cache;


import com.ftd.osp.utilities.cache.exception.CacheException;
import com.ftd.osp.utilities.cache.vo.CacheConfigVO;
import com.ftd.osp.utilities.cache.vo.CacheHandlerBase;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;
import java.io.IOException;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;

import java.sql.SQLException;
import java.util.Iterator;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import oracle.ias.cache.Attributes;
import oracle.ias.cache.CacheAccess;
import oracle.ias.cache.CacheNotAvailableException;
import oracle.ias.cache.RegionNotFoundException;

import oracle.xml.parser.v2.XSLException;

import org.xml.sax.SAXException;

public class CacheController
{
  private static Logger logger = new Logger("com.ftd.osp.utilities.cache.CacheController");
  private static final String MSG_TYPE_EXCEPTION = "EXCEPTION";
  private static final String MSG_SOURCE = "CACHE CONTROLLER";
  private static final String CACHE_DATASOURCE = "ORDER SCRUB";
  private static CacheController CACHECONTROLLER = new CacheController();
  private static boolean SINGLETON_INITIALIZED;
  
  /**
   * The private constructor
   */
  private CacheController()
  {
    super();
  }

 /**
  * The static initializer for the CacheController. It ensures that
  * at a given time there is only a single instance
  *
  * @return the CacheController
  **/  
  public static CacheController getInstance()
  {          
    if (! SINGLETON_INITIALIZED )
    {
      initCache();
    }
    return CACHECONTROLLER;
  }


  /**
   * This process creates a cache, the name of which is specified in the 'application' 
   * attribute in cacheconfig.xml.
   * 
   * For each cache handler, it then creates a sub-region within the cache. 
   * Each sub-region can be individually configured to its refresh-rate, whether or not 
   * to preload, whether or not changes made to the sub-region need to be distributed
   * across all caches, etc.
   */
  private synchronized static void initCache()
  {
    CacheAccess region = null;
    CacheAccess subRegion = null;
    Attributes attributes = null;
    Connection conn = null;
    StringWriter stringWriter = null;
    PrintWriter printWriter = null; 
    CacheHandlerBase cacheHandlerBase = null;
    Class cacheHandlerBaseClass = null;
    Map cacheConfigProps = null;
    boolean globalRegionExists = true; // set to true
    
    if (! SINGLETON_INITIALIZED)
    {
      try 
      {
        CacheConfigVO cacheConfigVO = CacheUtil.getInstance().getCacheConfig();
        
        attributes = new Attributes();
        if ( cacheConfigVO.getDistributedMode() != null 
            && cacheConfigVO.getDistributedMode().equals("true") ) 
        {
            attributes.setFlags(Attributes.DISTRIBUTE);
            logger.debug("Setting global cache " + cacheConfigVO.getGlobalCacheName() + " as distributed."
                          + " Any user-defined objects that will be distributed among JVMs must be"
                           + " loaded by the system class loader. In order to do that, copy the"
                           + " class files of the user-defined objects to ORACLE_HOME/javacache/sharedobjects/classes or add it"
                           + " to the JAR file ORACLE_HOME/javacache/cachedobjects/share.jar");
        }
              
        try 
        {
          /*
           * check the existence of the global region. If  the region does not 
           * exist, a RegionNotFoundException will be thrown.
           */
          region = CacheAccess.getAccess(cacheConfigVO.getGlobalCacheName(), true);
          // destroy all objects associated with the region
          region.destroy();
          logger.debug(cacheConfigVO.getGlobalCacheName() + " HAS BEEN DESTROYED");
          globalRegionExists = false;
        } catch (RegionNotFoundException rnfe) 
        {
          logger.debug("The global cache region " 
                        + cacheConfigVO.getGlobalCacheName()
                        + " does not exist, a new global cache region will be created");
          globalRegionExists = false;
        } 
        
        // proceed only if the global region does not yet exist
        if (! globalRegionExists) 
        {
          // create the global region if it doesnt exist
          CacheAccess.defineRegion(cacheConfigVO.getGlobalCacheName(), attributes);
          logger.debug("Creating cache " + cacheConfigVO.getGlobalCacheName());
          // obtain an instance of CacheAccess object to a named region
          region = CacheAccess.getAccess(cacheConfigVO.getGlobalCacheName());
    
          
          // Create sub-regions
          for (Iterator iter = cacheConfigVO.getSubRegions().keySet().iterator(); iter.hasNext(); ) 
          {
            cacheConfigProps = cacheConfigVO.getSubRegion((String)iter.next());
            
            // configure the attributes for the sub-region
            attributes = new Attributes();
            attributes.setDefaultTimeToLive( Integer.parseInt( (String)cacheConfigProps.get("refresh-rate")) * 60 ); // convert minutes to seconds
            if ( cacheConfigProps.get("distributed-mode") != null 
                && ((String)cacheConfigProps.get("distributed-mode")).equals("true") ) 
            {
                attributes.setFlags(Attributes.DISTRIBUTE);
                logger.debug("Setting sub-region " + cacheConfigProps.get("name") + " as distributed."
                              + " Any user-defined objects that will be   distributed among JVMs must be"
                               + " loaded by the system class loader. In order to do that, copy the"
                               + " class files of the user-defined objects to ORACLE_HOME/javacache/sharedobjects/classes or add it"
                               + " to the JAR file ORACLE_HOME/javacache/cachedobjects/share.jar");
            }
            
            // set the appropriate cache loader
            try {
            cacheHandlerBaseClass = Class.forName((String)cacheConfigProps.get("cache-loader"));
            } catch (ClassNotFoundException e) 
            {
              // JP Puzon 08-23-05
              // Added the catch logic below to accomodate multiple class loaders.
              cacheHandlerBaseClass = Thread.currentThread().getContextClassLoader().loadClass((String)cacheConfigProps.get("cache-loader"));
            }
            cacheHandlerBase = (CacheHandlerBase)(cacheHandlerBaseClass.newInstance());
            attributes.setLoader(cacheHandlerBase);
            // define the sub-region
            region.defineSubRegion( (String)cacheConfigProps.get("name"), attributes ); 
            logger.debug("Created cache sub-region " + cacheConfigProps.get("name"));
            // preload a sub-region
            if ( cacheConfigProps.get("preload") != null 
                && ((String)cacheConfigProps.get("preload")).equals("true") ) 
            {
                subRegion = region.getSubRegion((String)cacheConfigProps.get("name"));
                subRegion.preLoad((String)cacheConfigProps.get("name"));
            }
          } // end for
            
        }
        
        SINGLETON_INITIALIZED = true;
        
      } catch (Exception ex) 
      {
        logger.error(ex);
        
        try
        {
            conn = DataSourceUtil.getInstance().getConnection(CACHE_DATASOURCE);
            SystemMessengerVO sysVo = new SystemMessengerVO();
            sysVo.setLevel(SystemMessengerVO.LEVEL_PRODUCTION);
            stringWriter = new StringWriter();
            printWriter = new PrintWriter(stringWriter);
            ex.printStackTrace(printWriter);
            sysVo.setMessage(stringWriter.toString());
            sysVo.setSource(MSG_SOURCE);
            sysVo.setType(MSG_TYPE_EXCEPTION);
            SystemMessenger.getInstance().send(sysVo, conn, false);
        }
        catch(Exception sme)
        {
            logger.error(ex);
        }
        finally
        {
            try
            {
                if(conn != null && !conn.isClosed())
                {
                    conn.close();
                }
                if(stringWriter != null)
                {
                    stringWriter.close();
                }
                if(printWriter != null)
                {
                    printWriter.close();
                }
            }
            catch(Exception e)
            {
                logger.error(e);
            }
        }
      } 
      finally
      {
        if (subRegion != null) 
        {
          // Close the CacheAccess object
          subRegion.close();          
        }
        if (region != null) 
        {
          // Close the CacheAccess object
          region.close();          
        }      
      }
      
    }// end if
  }
  

 /**
  * Returns a cache handler. 
  * If the cache handler is not found, a null is returned.
  * 
  * @param handlerName the name of the handler
  * @return returns the appropriate cache handler
  */
  public CacheHandlerBase getHandler(String handlerName) 
  {
      CacheHandlerBase cacheHandlerBase = null;
      CacheConfigVO cacheConfigVO = null;
      CacheAccess region = null;
      CacheAccess subRegion = null;
      
      try
      {
        this.checkCacheConfigChange();
        cacheConfigVO = CacheUtil.getInstance().getCacheConfig();
  
        // obtain an instance of CacheAccess object to the entire application
        region = CacheAccess.getAccess(cacheConfigVO.getGlobalCacheName());

        // obtain an instance of CacheAccess object to a named region
        subRegion = region.getSubRegion(handlerName);
        //cacheHandlerBase = (CacheHandlerBase)(subRegion.get(handlerName));
        // get a new instance of the cache handler
        Map subRegionProperties = cacheConfigVO.getSubRegion(handlerName);
        try {
        cacheHandlerBase = (CacheHandlerBase) Class.forName((String)subRegionProperties.get("cache-loader")).newInstance();
        } catch (ClassNotFoundException e) 
            {
              // JP Puzon 08-23-05
              // Added the catch logic below to accomodate multiple class loaders.
              cacheHandlerBase = (CacheHandlerBase) Thread.currentThread().getContextClassLoader().loadClass((String)subRegionProperties.get("cache-loader")).newInstance();
            }
        // set the cached object on the cache handler
        cacheHandlerBase.setCachedObject(subRegion.get(handlerName));
        logger.debug(handlerName +"::"+ "Cached data loaded into the cache handler");
      }
      catch(Exception e)
      {
        logger.error(e);
      } finally
      {
        if (subRegion != null) 
        {
          // close the CacheAccess handle
          subRegion.close();                                 
        }
        if (region != null) 
        {
          // close the CacheAccess handle
          region.close();
        }
      }

      return cacheHandlerBase;
  }


 /**
  * Returns a cache handler. 
  * If the cache handler is not found, a null is returned.
  * 
  * @param handlerName the name of the handler
  * @param con the connection to the database
  * @return returns the appropriate cache handler
  */
  public CacheHandlerBase getHandler(String handlerName, Connection con) 
  {
      CacheHandlerBase cacheHandlerBase = null;
      CacheConfigVO cacheConfigVO = null;
      CacheAccess region = null;
      CacheAccess subRegion = null;
      
      try
      {
        this.checkCacheConfigChange();
        cacheConfigVO = CacheUtil.getInstance().getCacheConfig();
  
        // obtain an instance of CacheAccess object to the entire application
        region = CacheAccess.getAccess(cacheConfigVO.getGlobalCacheName());

        // obtain an instance of CacheAccess object to a named region
        subRegion = region.getSubRegion(handlerName);
        //cacheHandlerBase = (CacheHandlerBase)(subRegion.get(handlerName, con));
        // get a new instance of the cache handler
        Map subRegionProperties = cacheConfigVO.getSubRegion(handlerName);
        try {
        cacheHandlerBase = (CacheHandlerBase) Class.forName((String)subRegionProperties.get("cache-loader")).newInstance();
        } catch (ClassNotFoundException e) 
            {
              // JP Puzon 08-23-05
              // Added the catch logic below to accomodate multiple class loaders.
              cacheHandlerBase = (CacheHandlerBase) Thread.currentThread().getContextClassLoader().loadClass((String)subRegionProperties.get("cache-loader")).newInstance();
            }
        // set the cached object on the cache handler
        cacheHandlerBase.setCachedObject(subRegion.get(handlerName, con));
        logger.debug(handlerName +"::"+ "Cached data loaded into the cache handler");
      }
      catch(Exception e)
      {
        logger.error(e);
      } finally
      {
        if (subRegion != null) 
        {
          // close the CacheAccess handle
          subRegion.close();                                 
        }
        if (region != null) 
        {
          // close the CacheAccess handle
          region.close();
        }
      }

      return cacheHandlerBase;
  }

  /**
   * Reloads the cache configuration on change
   * 
   * @throws java.io.IOException
   * @throws org.xml.sax.SAXException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws oracle.xml.parser.v2.XSLException   
   */
  private void checkCacheConfigChange()throws IOException, 
                                              SAXException, 
                                              ParserConfigurationException, 
                                              XSLException,
                                              CacheNotAvailableException,
                                              oracle.ias.cache.CacheException
  {
    CacheAccess region = null;
    try 
    {
      // check for cache invalidation
      if(CacheUtil.getInstance().hasConfigChanged())
      {
        synchronized(this)
        {
          if (CacheUtil.RELOAD_CACHE_CONFIG) 
          {
            // reload the cache config
            CacheConfigVO cacheConfigVO = CacheUtil.getInstance().reloadCacheConfig();
            // flag needs to be protected from other threads
            SINGLETON_INITIALIZED = false; 
            this.initCache();
            logger.debug(cacheConfigVO.getGlobalCacheName() + " HAS BEEN RE-INITIALIZED");
            CacheUtil.RELOAD_CACHE_CONFIG = false;
          }
        }

      }      
    } finally 
    {
        if (region != null) 
        {
          // close the CacheAccess handle
          region.close();
        }
    }    
  }

 /**
  * Marks all objects within the cache as invalid. Invalidate does not affect 
  * the region or groups within the region, just individual objects are invalidated.
  */
  public synchronized static void invalidate() throws CacheException
  {
    CacheAccess region = null;
    try 
    {
      CacheConfigVO cacheConfigVO = CacheUtil.getInstance().getCacheConfig();
      // obtain an instance of CacheAccess object to the entire application
      try 
      {
        /*
         * check the existence of the global region. If  the region does not 
         * exist, a RegionNotFoundException will be thrown.
         */
        region = CacheAccess.getAccess(cacheConfigVO.getGlobalCacheName(), true);
        region.invalidate();
        logger.debug("Global-Region " + cacheConfigVO.getGlobalCacheName()+" has been invalidated");          
      } catch (RegionNotFoundException rnfe) 
      {
        logger.error(rnfe);
        logger.error("Global-Region " + cacheConfigVO.getGlobalCacheName() + " could not be invalidated");
        logger.error("IGNORE THIS ERROR");
      } 
      
    } catch (Exception ex) 
    {
      throw new CacheException("Cache could not be invalidated", ex);
    } finally 
    {
        if (region != null) 
        {
          // close the CacheAccess handle
          region.close();
        }
    }
  }
  
  /**
   * Marks a specific cache handler as invalid. Invalidate does not affect 
   * the region or groups within the region, just individual objects are invalidated.
   * 
   * @param handlerName the name of the handler
   */
  public synchronized static void invalidate(String handlerName) throws CacheException
  {    
    CacheAccess region = null;
    CacheAccess subRegion = null;
    try 
    {
      CacheConfigVO cacheConfigVO = CacheUtil.getInstance().getCacheConfig();
      try 
      {
        /*
         * check the existence of the global region. If  the region does not 
         * exist, a RegionNotFoundException will be thrown.
         */
        // obtain an instance of CacheAccess object to the entire application
        region = CacheAccess.getAccess(cacheConfigVO.getGlobalCacheName(), true);
  
        // obtain an instance of CacheAccess object to a named region
        subRegion = region.getSubRegion(handlerName);
        // invalidate all objects associated with the sub-region
        subRegion.invalidate();           
        logger.debug("Sub-Region " + handlerName + " has been invalidated");
      } catch (RegionNotFoundException rnfe) 
      {
        logger.error(rnfe);
        logger.error("Sub-Region " + handlerName + " could not be invalidated");
        logger.error("IGNORE THIS ERROR");
      } 
      
    } catch (Exception ex) 
    {
      throw new CacheException("Sub-Region " + handlerName + " could not be invalidated", ex);
    } finally 
    {
        if (subRegion != null) 
        {
          // close the CacheAccess handle
          subRegion.close();                                 
        }
        if (region != null) 
        {
          // close the CacheAccess handle
          region.close();
        }
    }
  }
  
    /**
     * Destroys the existing cache instance and then reloads it
     *
     * @return the CacheController
     **/  
     public static CacheController reloadCache()
     {          
         SINGLETON_INITIALIZED = false;
         initCache();
         return CACHECONTROLLER;
     }


}