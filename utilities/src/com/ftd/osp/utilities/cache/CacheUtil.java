package com.ftd.osp.utilities.cache;

import com.ftd.osp.utilities.cache.vo.CacheConfigVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.io.File;
import java.io.IOException;

import java.net.URL;

import java.net.URLDecoder;
import java.util.HashMap;

import javax.xml.parsers.ParserConfigurationException;

import oracle.xml.parser.v2.XMLDocument;
import oracle.xml.parser.v2.XSLException;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import org.xml.sax.SAXException;

/**
 * Maintains a cache which can be divided into multiple domains. Each domain
 * can be independently configured to refresh itself periodically.
 *
 * @Anshu Gaind
 */
public class CacheUtil  {
  private static Logger logger = new Logger("com.ftd.osp.utilities.cache.CacheUtil");
  private static CacheUtil CACHEUTIL;
  private static CacheConfigVO cacheConfigVO;
  private static final String configFile = "cacheconfig.xml";
  private static long configLastModified = 0;
  private static File cacheConfigFile;
  
  public static boolean RELOAD_CACHE_CONFIG;
  

 /**
  * The static initializer for the CacheUtil. It ensures that
  * at a given time there is only a single instance
  *
  * @return the CacheUtil
  * @throws java.io.IOException
  * @throws org.xml.sax.SAXException
  * @throws javax.xml.parsers.ParserConfigurationException
  * @throws oracle.xml.parser.v2.XSLException
  **/
  public static CacheUtil getInstance()
    throws IOException, SAXException, ParserConfigurationException, XSLException
  {    
    if (CACHEUTIL == null) 
    {
      CACHEUTIL = new CacheUtil();
    }
    
    return CACHEUTIL;
  }

  /**
   * The private Constructor
   * 
   * @throws java.io.IOException
   * @throws org.xml.sax.SAXException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws oracle.xml.parser.v2.XSLException
   **/
  private CacheUtil() 
    throws IOException, SAXException, ParserConfigurationException, XSLException
  {
    super();
    cacheConfigVO = this.loadCacheConfig();
  }


  /**
   * Returns cache config information
   *
   * @return the cache config object
   */
  public CacheConfigVO getCacheConfig()
  {
    return this.cacheConfigVO;
  }

  /**
   * Loads cacheconfig.xml
   *
   * @return the cache config object
   * @throws java.io.IOException
   * @throws org.xml.sax.SAXException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws oracle.xml.parser.v2.XSLException
   */
  private CacheConfigVO loadCacheConfig() 
        throws IOException, SAXException, ParserConfigurationException, XSLException
  {
    URL url = this.getClass().getClassLoader().getResource(this.configFile);
       
    if (url == null)
    {
         throw new IOException("The configuration file was not found.");
    }

		cacheConfigFile = new File(new URLDecoder().decode(url.getFile(), "UTF-8"));
    XMLDocument config = (XMLDocument)DOMUtil.getDocument(cacheConfigFile);

    NodeList cacheNodeList = (NodeList) config.selectNodes("/cache-config/caches/cache");
    Element cacheElement;

    CacheConfigVO cacheConfigVO = new CacheConfigVO();

    cacheConfigVO.setGlobalCacheName( ((Element)config.selectSingleNode("/cache-config")).getAttribute("global-cache-name"));
    cacheConfigVO.setDataSource( ((Element)config.selectSingleNode("/cache-config")).getAttribute("data-source"));
    cacheConfigVO.setDistributedMode( ((Element)config.selectSingleNode("/cache-config")).getAttribute("distributed-mode"));
    cacheConfigVO.setDistributedSearchTimeout( ((Element)config.selectSingleNode("/cache-config")).getAttribute("distributed-search-timeout") );

    HashMap properties = null;

    for (int i = 0; i < cacheNodeList.getLength(); i++)
    {
        properties = new HashMap();
        cacheElement = (Element) (cacheNodeList.item(i));

        properties.put("name", cacheElement.getAttribute("name"));
        properties.put("cache-loader", cacheElement.getAttribute("cache-loader"));
        properties.put("refresh-rate", cacheElement.getAttribute("refresh-rate"));
        properties.put("preload", cacheElement.getAttribute("preload"));
        properties.put("distributed-mode", cacheElement.getAttribute("distributed-mode"));
        cacheConfigVO.setSubRegion(cacheElement.getAttribute("name"), properties);
     }
    configLastModified = cacheConfigFile.lastModified();
    return cacheConfigVO;
  }


  /**
   * Reloads the cache configuration
   *
   * @return the cache configuration
   * @throws java.io.IOException
   * @throws org.xml.sax.SAXException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws oracle.xml.parser.v2.XSLException
   */
  public CacheConfigVO reloadCacheConfig()
      throws IOException, SAXException, ParserConfigurationException, XSLException
  {
    this.cacheConfigVO = this.loadCacheConfig();
    return this.cacheConfigVO;
  }


  /**
   * Returns whether or not cacheconfig.xml file has been modified
   *
   * @return whether or not cacheconfig.xml file has been modified
   */
  public boolean hasConfigChanged()
  {
    if(configLastModified == 0 
        || configLastModified == cacheConfigFile.lastModified())
    {
      return false;
    }


    synchronized(this)
    {
      if(configLastModified != cacheConfigFile.lastModified())
      {
        logger.debug("CACHECONFIG.XML HAS BEEN MODIFIED");
        configLastModified = cacheConfigFile.lastModified();
        this.RELOAD_CACHE_CONFIG = true;
        return true;
      }
    }

    return false;
  }
}