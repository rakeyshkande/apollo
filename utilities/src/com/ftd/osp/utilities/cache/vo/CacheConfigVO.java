package com.ftd.osp.utilities.cache.vo;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class CacheConfigVO 
{
  private String globalCacheName;
  private Map subRegions = new HashMap();
  private String dataSource;
  private String distributedMode;
  private String distributedSearchTimeout;
  
  public CacheConfigVO()
  {}

  public String getGlobalCacheName()
  {
    return globalCacheName;
  }

  public void setGlobalCacheName(String globalCacheName)
  {
    this.globalCacheName = globalCacheName;
  }

  public Map getSubRegions()
  {
    return subRegions;
  }
  
  public Map getSubRegion(String subRegion)
  {
    return (Map) subRegions.get(subRegion);
  }

  public void setSubRegion(String subRegion, Map properties)
  {
    subRegions.put(subRegion, properties);
  }
  

  public String getDataSource()
  {
    return dataSource;
  }

  public void setDataSource(String dataSource)
  {
    this.dataSource = dataSource;
  }

  public String getDistributedMode()
  {
    return distributedMode;
  }

  public void setDistributedMode(String distributedMode)
  {
    this.distributedMode = distributedMode;
  }

  public String getDistributedSearchTimeout()
  {
    return distributedSearchTimeout;
  }

  public void setDistributedSearchTimeout(String distributedSearchTimeout)
  {
    this.distributedSearchTimeout = distributedSearchTimeout;
  }

}