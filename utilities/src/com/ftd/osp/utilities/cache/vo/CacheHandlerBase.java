package com.ftd.osp.utilities.cache.vo;

import com.ftd.osp.utilities.cache.CacheUtil;
import com.ftd.osp.utilities.cache.exception.CacheException;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;

import oracle.ias.cache.CacheLoader;
import oracle.ias.cache.ObjectNotFoundException;

/**
 * A cache handler is responsible for the following:
 * 
 * 1. Act as a cache loader, to load the data that needs to be cached.
 * 2. Provide application specific API, to enable access to the cached data.
 * 
 */
public abstract class CacheHandlerBase extends CacheLoader
{
  protected Logger logger =  new Logger("com.ftd.osp.utilities.cache.vo.CacheHandlerBase");

  /**
   * Return a connection to the datasource, as specified in the 'data-source'
   * attribute in cacheconfig.xml
   * 
   * @return 
   * @throws java.lang.Exception
   */
  protected Connection getConnection() throws Exception
  {
    CacheConfigVO cacheConfigVO = CacheUtil.getInstance().getCacheConfig();
    
    if (cacheConfigVO.getDataSource() == null 
        && (cacheConfigVO.getDataSource().equals("")) ) 
    {
      logger.error(cacheConfigVO.getDataSource() + " could not be located");
    }
    
    return DataSourceUtil.getInstance().getConnection(cacheConfigVO.getDataSource());        
  }

  /**
   * This method is called by the caching service when the requested object is not in the cache.
   * It should always return a reference to the Cache Handler.
   * 
   * @param handle An internal handle object is passed in from cache service. 
   *               It should only be used as the first parameter of the 
   *               netSearch(handle, timeout) or setAttributes(handle, attributes) API.
   * @param args It is an argument object for load method passed in from customer code. 
   *              This object is passed in as args either in get(String name, Object args), 
   *              or get(String name, String group, Object args)
   * @return In general, the object that is to be saved in the cache should be returned. 
   *          If createStream has been called the OutputStream object returned by 
   *          createStream should be returned. If createDiskObject is called the File 
   *          object returned by createDiskObject should be returned.
   * @throws com.ftd.osp.utilities.cache.exception.CacheException
   */
    public final Object load(Object handle, Object args) throws CacheException
    {
        Connection con = null;
        Object cachedObject = null;
        try
        {          
          CacheConfigVO cacheConfigVO = CacheUtil.getInstance().getCacheConfig();
          if (cacheConfigVO.getDistributedMode() != null && cacheConfigVO.getDistributedMode().equals("true")) 
          {
            // check if this object is loaded in another cache 
            try {
              CacheHandlerBase cacheHandlerBase = null;
              logger.debug("Searching the network for the object in other caches...will search for "+ cacheConfigVO.getDistributedSearchTimeout() + " seconds");
              cachedObject = netSearch(handle, (Integer.parseInt(cacheConfigVO.getDistributedSearchTimeout()) * 1000) );// wait for up to x scnds            
              logger.debug("Found the object in another cache, on the network");
              return cachedObject;
            } catch(ObjectNotFoundException ex)
            {
              logger.debug("The object could not be found in other caches, on the network");
            }              
          }
          
          //if the above fails then load from the database
          if (args != null) 
          {
            con = (Connection) args;
          }
          else
          {
            con = this.getConnection();
          }

          cachedObject = load(con);

        } catch(CacheException ce)
        {
          throw ce;
        } catch(Exception e)
        {
            logger.error(e);
            throw new CacheException("Could not load cache", e);
        } finally 
        {
          try 
          {
            if (con != null && (! con.isClosed())) 
            {
                con.close();
            }            
          } catch (Exception ex) 
          {
            logger.error(ex);
          }                     
        }
      //save the object in cache
      return cachedObject;
    }
   
   /**
   * Returns the object that needs to be cached.
   * 
   * The cache cannot be configured as distributable, if the cache handler 
   * returns a custom object. In order to utilize the benefits of a distributable 
   * cache, the return objects should only be a part JDK API.
   * 
   * @param con
   * @return the object to be cached
   * @throws com.ftd.osp.utilities.cache.exception.CacheException
   */
   abstract public Object load(Connection con) throws CacheException;
  
  
  /**
   * Set the cached object in the cache handler. The cache handler is then 
   * responsible for fulfilling all application level API calls, to access the
   * data in the cached object.
   * 
   * @param cachedObject
   * @throws com.ftd.osp.utilities.cache.exception.CacheException
   */
  abstract public void setCachedObject(Object cachedObject) throws CacheException;
}