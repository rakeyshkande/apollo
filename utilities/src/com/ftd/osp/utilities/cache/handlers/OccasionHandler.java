package com.ftd.osp.utilities.cache.handlers;

import com.ftd.osp.utilities.cache.exception.CacheException;
import com.ftd.osp.utilities.cache.vo.CacheHandlerBase;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * A cache handler is responsible for the following:
 * 
 * 1. Act as a cache loader, to load the data that needs to be cached.
 * 2. Provide application specific API, to enable access to the cached data.
 * 
 */

public class OccasionHandler extends CacheHandlerBase 
{
    // set by the setCachedObject method
    private Map occasionsById;
    private Map occasionsByAddon;
    private Map occasionsFtdWest;

    private static final String GET_OCCASION_LIST = "GET_OCCASION_LIST";
    private static final String GET_OCCASION_ADDON_LIST = "GET_OCCASION_ADDON_LIST";
    
    private String GET_OCCASIONS = "GET_OCCASIONS";
    private String GET_OCCASION_ADDONS = "GET_OCCASION_ADDONS";
    private String GET_FTD_WEST_OCCASIONS = "GET_FTD_WEST_OCCASIONS";
    
    public OccasionHandler()
    {        
    }

  
   /**
   * Returns the object that needs to be cached.
   * 
   * The cache cannot be configured as distributable, if the cache handler 
   * returns a custom object. In order to utilize the benefits of a distributable 
   * cache, the return objects should only be a part JDK API.
   * 
   * @param con
   * @return the object to be cached
   * @throws com.ftd.osp.utilities.cache.exception.CacheException
   */
    public Object load(Connection con) throws CacheException
    {  
        Map cachedObject = new HashMap();
        Map occasionHandlerCacheObject = new HashMap();
        Map occasionAddonCacheObject = new HashMap();
        Map occasionFtdWestCacheObject = new HashMap();
        
        try
        {
            super.logger.debug("BEGIN LOADING OCCASION HANDLER...");
            DataAccessUtil dau = DataAccessUtil.getInstance();
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(con);
            dataRequest.setStatementID(GET_OCCASION_LIST);
            CachedResultSet rs = (CachedResultSet)dau.execute(dataRequest);
            rs.reset();
            dataRequest.reset();
            Object occasionCode = null;
            String occasionDesc = null;
            String ftdWestOccasionCode=null;
            
            while(rs.next())
            {
                occasionCode = rs.getObject(1);
                occasionDesc = (String)rs.getObject(2);
                ftdWestOccasionCode= (String)rs.getObject(4);
                if(occasionCode != null)
                {
                    occasionHandlerCacheObject.put(occasionCode.toString(), occasionDesc);
                    occasionFtdWestCacheObject.put(occasionCode.toString(), ftdWestOccasionCode);
                }
            }
            
            cachedObject.put(GET_OCCASIONS, occasionHandlerCacheObject);
            
            // Occasion addons
            dataRequest.setConnection(con);
            dataRequest.setStatementID(GET_OCCASION_ADDON_LIST);
            rs = (CachedResultSet)dau.execute(dataRequest);
            String addonCode = null;
            List<String> occasionList = null;
            String prevAddonCode = null;
            
            while(rs != null && rs.next())
            {
                occasionCode = rs.getString(1);
                addonCode = (String)rs.getObject(2);

                if(!addonCode.equals(prevAddonCode)) {
                    if(prevAddonCode != null) {
                        occasionAddonCacheObject.put(prevAddonCode, occasionList);
                    } 
                    
                    occasionList = new ArrayList();
                }
                occasionList.add((String)occasionCode);

                prevAddonCode = addonCode;
            }
            
            if(prevAddonCode != null) {
                occasionAddonCacheObject.put(prevAddonCode, occasionList);
            }
            
            rs.reset();
            dataRequest.reset();
            cachedObject.put(GET_OCCASION_ADDONS, occasionAddonCacheObject);

            super.logger.debug("END LOADING OCCASION HANDLER...");
        }
        catch(Exception e)
        {
            e.printStackTrace();
            throw new CacheException("Could not load OccasionHandler cache");
        }

        logger.debug(occasionHandlerCacheObject.size() + " records loaded");
        return cachedObject;
    }


  /**
   * Set the cached object in the cache handler. The cache handler is then 
   * responsible for fulfilling all application level API calls, to access the
   * data in the cached object.
   * 
   * @param cachedObject
   * @throws com.ftd.osp.utilities.cache.exception.CacheException
   */    
    public void setCachedObject(Object cachedObject) throws CacheException
    {
      try 
      {
        this.occasionsById = (Map) ((Map)cachedObject).get(GET_OCCASIONS);  
        this.occasionsByAddon = (Map) ((Map)cachedObject).get(GET_OCCASION_ADDONS);
      } catch (Exception ex) 
      {
        super.logger.error(ex);
        throw new CacheException("Could not set the cached object.");
      } 
    }

  /**
   * 
   * @return 
   */
    public Map getOccasionIdMap()
    {
        return occasionsById;
    }
    
    public List<String> getOccasionListByAddonId(String addonId) throws CacheException
    {
        System.out.println("size of addon map:" + occasionsByAddon.size());
        return (List<String>)occasionsByAddon.get(addonId);
    }
    
    
}