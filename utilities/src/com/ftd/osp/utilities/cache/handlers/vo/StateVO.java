package com.ftd.osp.utilities.cache.handlers.vo;

import java.lang.reflect.Field;

import java.math.BigDecimal;


/*******************************************************************************************
 * ==============
 * ||Change Log||
 * ==============
 * 
 * Developer      Project                     Changes Made
 * ---------      -------                     ----------------------------------------------
 * Ali Lakhani    Modify Customers/Orders     Added toXML() method.
 * 
 *******************************************************************************************/


public class StateVO 
{
    private String id;
    private String name;
    private String countryCode;
    private String timeZone;
    private String dropShipAvailable;

    public StateVO()
    {
    }

    public String getId()
    {
        return id;
    }

    public void setId(String newId)
    {
        id = newId;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String newName)
    {
        name = newName;
    }

    public String getCountryCode()
    {
        return countryCode;
    }

    public void setCountryCode(String newCountryCode)
    {
        countryCode = newCountryCode;
    }

    public String getTimeZone()
    {
        return timeZone;
    }

    public void setTimeZone(String newTimeZone)
    {
        timeZone = newTimeZone;
    }

    public String getDropShipAvailable()
    {
        return dropShipAvailable;
    }
    public void setDropShipAvailable(String dropShipAvailable)
    {
        this.dropShipAvailable = dropShipAvailable;
    }

  /**
    * This method uses the Reflection API to generate an XML string that will be
    * passed back to the calling module.
    * The XML string will contain all the fields within this VO.
    *
    * @param  None
    * @return XML string
   **/
  public String toXML(int count)
  {
    StringBuffer sb = new StringBuffer();
    try
    {
      if (count == 0)
      {
        sb.append("<STATE>");
      }
      else
      {
        sb.append("<STATE num=" + '"' + count + '"' + ">");
      }
      Field[] fields = this.getClass().getDeclaredFields();
      int num = 0; 

      for (int i = 0; i < fields.length; i++)
      {
        count++;
        if (fields[i].get(this) == null)
        {
          sb.append("<" + fields[i].getName() + "/>");
        }
        else
        {
          sb.append("<" + fields[i].getName() + ">");
          sb.append(fields[i].get(this));
          sb.append("</" + fields[i].getName() + ">");
        }
      }
      sb.append("</STATE>");
    }

    catch (Exception e)
    {
      e.printStackTrace();
    }

    return sb.toString();
  }

}