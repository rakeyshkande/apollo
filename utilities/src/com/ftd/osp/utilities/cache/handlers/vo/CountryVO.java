package com.ftd.osp.utilities.cache.handlers.vo;
import java.lang.reflect.Field;

import java.math.BigDecimal;


/*******************************************************************************************
 * ==============
 * ||Change Log||
 * ==============
 * 
 * Developer      Project                     Changes Made
 * ---------      -------                     ----------------------------------------------
 * Ali Lakhani    Modify Customers/Orders     Added toXML() method.
 * 
 *******************************************************************************************/

public class CountryVO 
{
    private String id;
    private String description;
    private BigDecimal displayOrder;
    private String type;
    private String mondayClosed;
    private String tuesdayClosed;
    private String wednesdayClosed;
    private String thursdayClosed;
    private String fridayClosed;
    private String saturdayClosed;
    private String sundayClosed;
    private long   addonDays;
    private String threeCharacterId;  //ISO Alpha 3 code
    private String status;
    private String cutoffTime;
    private String mondayTransit;
    private String tuesdayTransit;
    private String wednesdayTransit;
    private String thursdayTransit;
    private String fridayTransit;
    private String saturdayTransit;
    private String sundayTransit;

    public CountryVO()
    {
    }

    public String getId()
    {
        return id;
    }

    public void setId(String newId)
    {
        id = newId;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String newDescription)
    {
        description = newDescription;
    }

    public BigDecimal getDisplayOrder()
    {
        return displayOrder;
    }

    public void setDisplayOrder(BigDecimal newDisplayOrder)
    {
        displayOrder = newDisplayOrder;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String newType)
    {
        type = newType;
    }

    public String getMondayClosed()
    {
        return mondayClosed;
    }

    public void setMondayClosed(String newMondayClosed)
    {
        mondayClosed = newMondayClosed;
    }

    public String getTuesdayClosed()
    {
        return tuesdayClosed;
    }

    public void setTuesdayClosed(String newTuesdayClosed)
    {
        tuesdayClosed = newTuesdayClosed;
    }

    public String getWednesdayClosed()
    {
        return wednesdayClosed;
    }

    public void setWednesdayClosed(String newWednesdayClosed)
    {
        wednesdayClosed = newWednesdayClosed;
    }

    public String getThursdayClosed()
    {
        return thursdayClosed;
    }

    public void setThursdayClosed(String newThursdayClosed)
    {
        thursdayClosed = newThursdayClosed;
    }

    public String getFridayClosed()
    {
        return fridayClosed;
    }

    public void setFridayClosed(String newFridayClosed)
    {
        fridayClosed = newFridayClosed;
    }

    public String getSaturdayClosed()
    {
        return saturdayClosed;
    }

    public void setSaturdayClosed(String newSaturdayClosed)
    {
        saturdayClosed = newSaturdayClosed;
    }

    public String getSundayClosed()
    {
        return sundayClosed;
    }

    public void setSundayClosed(String newSundayClosed)
    {
        sundayClosed = newSundayClosed;
    }

    public long getAddonDays()
    {
        return addonDays;
    }

    public void setAddonDays(long newAddonDays)
    {
        addonDays = newAddonDays;
    }


  public void setThreeCharacterId(String threeCharacterId)
  {
    this.threeCharacterId = threeCharacterId;
  }


  public String getThreeCharacterId()
  {
    return threeCharacterId;
  }
  
  public void setStatus(String status)
  {
      this.status = status;
  }

  public String getStatus()
  {
      return status;
  }
  
  public void setCutoffTime(String cutoffTime)
  {
      this.cutoffTime = cutoffTime;
  }
  
  public String getCutoffTime()
  {
      return cutoffTime;
  }

 /**
    * This method uses the Reflection API to generate an XML string that will be
    * passed back to the calling module.
    * The XML string will contain all the fields within this VO.
    *
    * @param  None
    * @return XML string
   **/
  public String toXML(int count)
  {
    StringBuffer sb = new StringBuffer();
    try
    {
      if (count == 0)
      {
        sb.append("<COUNTRY>");
      }
      else
      {
        sb.append("<COUNTRY num=" + '"' + count + '"' + ">");
      }
      
      Field[] fields = this.getClass().getDeclaredFields();

      for (int i = 0; i < fields.length; i++)
      {
        if (fields[i].get(this) == null)
        {
          sb.append("<" + fields[i].getName() + "/>");
        }
        else
        {
          sb.append("<" + fields[i].getName() + ">");
          sb.append(fields[i].get(this));
          sb.append("</" + fields[i].getName() + ">");
        }
      }
      sb.append("</COUNTRY>");
    }

    catch (Exception e)
    {
      e.printStackTrace();
    }

    return sb.toString();
  }


    public void setMondayTransit(String param)
    {
        this.mondayTransit = param;
    }

    public String getMondayTransit()
    {
        return mondayTransit;
    }

    public void setTuesdayTransit(String param)
    {
        this.tuesdayTransit = param;
    }

    public String getTuesdayTransit()
    {
        return tuesdayTransit;
    }

    public void setWednesdayTransit(String param)
    {
        this.wednesdayTransit = param;
    }

    public String getWednesdayTransit()
    {
        return wednesdayTransit;
    }

    public void setThursdayTransit(String param)
    {
        this.thursdayTransit = param;
    }

    public String getThursdayTransit()
    {
        return thursdayTransit;
    }

    public void setFridayTransit(String param)
    {
        this.fridayTransit = param;
    }

    public String getFridayTransit()
    {
        return fridayTransit;
    }

    public void setSaturdayTransit(String param)
    {
        this.saturdayTransit = param;
    }

    public String getSaturdayTransit()
    {
        return saturdayTransit;
    }

    public void setSundayTransit(String param)
    {
        this.sundayTransit = param;
    }

    public String getSundayTransit()
    {
        return sundayTransit;
    }
}
