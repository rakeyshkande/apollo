package com.ftd.osp.utilities.cache.handlers.vo;

public class PostalCodeVO 
{
    private String zipcode;
    private String city;
    private String state;

    public PostalCodeVO()
    {
    }

    public String getZipcode()
    {
        return zipcode;
    }

    public void setZipcode(String newZipcode)
    {
        zipcode = newZipcode;
    }

    public String getCity()
    {
        return city;
    }

    public void setCity(String newCity)
    {
        city = newCity;
    }

    public String getState()
    {
        return state;
    }

    public void setState(String newState)
    {
        state = newState;
    }
}