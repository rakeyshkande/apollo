package com.ftd.osp.utilities.cache.handlers;

import com.ftd.osp.utilities.cache.exception.CacheException;
import com.ftd.osp.utilities.cache.vo.CacheHandlerBase;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;

import java.sql.Connection;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


/**
 * A cache handler is responsible for the following:
 *
 * 1. Act as a cache loader, to load the data that needs to be cached.
 * 2. Provide application specific API, to enable access to the cached data.
 *
 */
public class PaymentMethodHandler  extends CacheHandlerBase
{
  /* Cache of payment method text types and associated text
   * Set by the setCachedObject method
   */ 
  private Map textTypeMap; 
  
  private static final String GET_PAYMENT_METHODS_TEXT = "GET_PAYMENT_METHODS_TEXT";

  /**
   * constructor
   */
  public PaymentMethodHandler()
  {
  }

  /**
  * Returns the object that needs to be cached.
  *
  * The cache cannot be configured as distributable, if the cache handler
  * returns a custom object. In order to utilize the benefits of a distributable
  * cache, the return objects should only be a part JDK API.
  *
  * @param con
  * @return the object to be cached
  * @throws com.ftd.osp.utilities.cache.exception.CacheException
  */
  public Object load(Connection con) throws CacheException
  {
    Map textTypeMap = new HashMap();
    Map textMap = null;
  
    try
    {
      super.logger.debug("BEGIN LOADING PaymentMethodHandler...");

      // Obtain payment method text information
      DataAccessUtil dau = DataAccessUtil.getInstance();
      DataRequest dataRequest = new DataRequest();
      dataRequest.setStatementID(GET_PAYMENT_METHODS_TEXT);
      dataRequest.addInputParam("IN_TYPE_TXT", null);
      dataRequest.setConnection(con);
      CachedResultSet crs = (CachedResultSet)dau.execute(dataRequest);

      String type = null;
      while(crs.next())
      {
        // Obtain the type
        type = crs.getString("type_txt");
        textMap = (HashMap)textTypeMap.get(type);
        
        // If type does not exist create a new HashMap
        if(textMap == null)
          textMap = new HashMap();  

        // Store the payment method and associated text
        textMap.put(crs.getString("payment_method_id"), crs.getString("text_txt"));

        // Store the HashMap for the given type
        textTypeMap.put(type, textMap);        
      }

      super.logger.debug("END LOADING PaymentMethodHandler...");
    }
    catch(Exception e)
    {
        super.logger.error(e);
        throw new CacheException("Could not load PaymentMethodHandler cache");
    }
  
    return textTypeMap;
  }

  /**
   * Set the cached object in the cache handler. The cache handler is then
   * responsible for fulfilling all application level API calls, to access the
   * data in the cached object.
   *
   * @param cachedObject
   * @throws com.ftd.osp.utilities.cache.exception.CacheException
   */
    public void setCachedObject(Object cachedObject) throws CacheException
    {
      try
      {
        this.textTypeMap = (Map)cachedObject;
      } catch (Exception ex)
      {
        super.logger.error(ex);
        throw new CacheException("Could not set the cached object.");
      }
    }

    public String getText(List paymentMethods, String type)
    {
      // Log data
      if(logger.isDebugEnabled())
      {
        if(!paymentMethods.isEmpty())
          for(Iterator it = paymentMethods.iterator(); it.hasNext();)
            logger.debug("paymentMethod is " + (String)it.next());

        logger.debug("type is " + type);
      }

      // If no payments methods exist
      if(paymentMethods.isEmpty())
      {
        logger.warn("There were no payment methods supplied!  Please correct or payment method text will be blank.");
        return "";
      }
      // If payment methods exist
      else
      {
        // Iterate through payment methods
        String paymentMethod = null;
        for(Iterator it = paymentMethods.iterator(); it.hasNext();)
        {
          paymentMethod = (String)it.next();

          // Use the first non gift certificate payment method.
          // If only one payment method exists, use it.
          if(paymentMethod != null && !paymentMethod.equalsIgnoreCase("GC"))
            break;
        }

        // Obtain the HashMap of payment method text data based on type.
        // If no type exists use a default type of 'generic'.
        if(type == null) type = "generic";
        HashMap textMap = (HashMap)textTypeMap.get(type);

        if(textMap == null)
        {
          logger.warn("No payment method text found for type " + type + "!  Please correct or payment method text will be blank.");
          return "";
        }

        // Return payment method text
        return (String)textMap.get(paymentMethod);
      }
    }

  /**
   *
   * @param args
   */
  public static void main(String[] args)
  {
    PaymentMethodHandler paymentMethodHandler = new PaymentMethodHandler();
  }
}