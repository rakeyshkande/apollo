package com.ftd.osp.utilities.cache.handlers.vo;


public class PartnerVO 
{
    private String partnerName;
    private String preferredProcessingResource;
    private String displayValue;
    private String sourceCode;
    private String defaultPhoneSourceCode;
    private String defaultWebSourceCode;
    private String replyEmailAddress;
    private String preferredPartnerFlag;
    private String binProcessingFlag;
    private String defaultPhoneSourceCodeDescription;
    private String defaultWebSourceCodeDescription;
    private boolean marketingInsertAllowed = true;
    private boolean floristResendAllowed = false;


    public void setPartnerName(String partnerName) {
        this.partnerName = partnerName;
    }

    public String getPartnerName() {
        return partnerName;
    }

    public void setPreferredProcessingResource(String preferredProcessingResource) {
        this.preferredProcessingResource = preferredProcessingResource;
    }

    public String getPreferredProcessingResource() {
        return preferredProcessingResource;
    }

    public void setDisplayValue(String displayValue) {
        this.displayValue = displayValue;
    }

    public String getDisplayValue() {
        return displayValue;
    }

    public void setSourceCode(String sourceCode) {
        this.sourceCode = sourceCode;
    }

    public String getSourceCode() {
        return sourceCode;
    }

    public void setDefaultPhoneSourceCode(String defaultPhoneSourceCode) {
        this.defaultPhoneSourceCode = defaultPhoneSourceCode;
    }

    public String getDefaultPhoneSourceCode() {
        return defaultPhoneSourceCode;
    }

    public void setReplyEmailAddress(String replyEmailAddress)
    {
        this.replyEmailAddress = replyEmailAddress;
    }

    public String getReplyEmailAddress()
    {
        return replyEmailAddress;
    }

    public void setDefaultWebSourceCode(String defaultWebSourceCode) {
        this.defaultWebSourceCode = defaultWebSourceCode;
    }

    public String getDefaultWebSourceCode() {
        return defaultWebSourceCode;
    }

    public void setPreferredPartnerFlag(String preferredPartnerFlag) {
        this.preferredPartnerFlag = preferredPartnerFlag;
    }

    public String getPreferredPartnerFlag() {
        return preferredPartnerFlag;
    }

    public void setBinProcessingFlag(String binProcessingFlag) {
        this.binProcessingFlag = binProcessingFlag;
    }

    public String getBinProcessingFlag() {
        return binProcessingFlag;
    }

    public void setDefaultPhoneSourceCodeDescription(String defaultPhoneSourceCodeDescription) {
        this.defaultPhoneSourceCodeDescription = defaultPhoneSourceCodeDescription;
    }

    public String getDefaultPhoneSourceCodeDescription() {
        return defaultPhoneSourceCodeDescription;
    }

    public void setDefaultWebSourceCodeDescription(String defaultWebSourceCodeDescription) {
        this.defaultWebSourceCodeDescription = defaultWebSourceCodeDescription;
    }

    public String getDefaultWebSourceCodeDescription() {
        return defaultWebSourceCodeDescription;
    }

  public void setMarketingInsertAllowed(String preferredPartnerFlag)
  {
    //if preferred partner, marketingInsert is not allowed
    if (preferredPartnerFlag != null && preferredPartnerFlag.equalsIgnoreCase("Y"))
      this.marketingInsertAllowed = false;
    else
      this.marketingInsertAllowed = true; 
  }

  public boolean isMarketingInsertAllowed()
  {
    return marketingInsertAllowed;
  }

  public void setFloristResendAllowed(String floristResendAllowedFlag)
  {
    if (floristResendAllowedFlag != null && floristResendAllowedFlag.equalsIgnoreCase("Y"))
      this.floristResendAllowed = true;
    else
      this.floristResendAllowed = false; 
  }

  public boolean isFloristResendAllowed()
  {
    return floristResendAllowed;
  }
}
