package com.ftd.osp.utilities.cache.handlers;

import com.ftd.osp.utilities.cache.exception.CacheException;
import com.ftd.osp.utilities.cache.handlers.vo.MessageTokenVO;
import com.ftd.osp.utilities.cache.vo.CacheHandlerBase;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;

import java.sql.Connection;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


/**
 * A cache handler is responsible for the following:
 *
 * 1. Act as a cache loader, to load the data that needs to be cached.
 * 2. Provide application specific API, to enable access to the cached data.
 *
 */
public class StockMessageTokenHandler  extends CacheHandlerBase
{
  // set by the setCachedObject method
   private Map tokenMap; // companyId is the key and the ArrayList of tokens is the value

   private static final String VIEW_MESSAGE_TOKENS = "VIEW_MESSAGE_TOKENS";
   private static final String ALL_COMPANIES = "ALL";

/**
 * constructor
 */
  public StockMessageTokenHandler()
  {
  }

   /**
   * Returns the object that needs to be cached.
   *
   * The cache cannot be configured as distributable, if the cache handler
   * returns a custom object. In order to utilize the benefits of a distributable
   * cache, the return objects should only be a part JDK API.
   *
   * @param con
   * @return the object to be cached
   * @throws com.ftd.osp.utilities.cache.exception.CacheException
   */
    public Object load(Connection con) throws CacheException
    {
        Map stockMessageTokenHandlerCacheObject = new HashMap();

        Map stockMessageTokenMap = new HashMap();
        try
        {
            super.logger.debug("BEGIN LOADING MESSAGE_TOKEN HANDLER...");

            DataAccessUtil dau = DataAccessUtil.getInstance();
            DataRequest dataRequest = new DataRequest();
            dataRequest.setStatementID("VIEW_MESSAGE_TOKENS");
            dataRequest.addInputParam("IN_COMPANY_ID", null);
            dataRequest.setConnection(con);
            ResultSet rs = (ResultSet)dau.execute(dataRequest);


            while(rs.next())
            {
              MessageTokenVO messageToken = new MessageTokenVO(rs.getString("token_id"),
                rs.getString("company_id"), rs.getString("token_type"),
                rs.getString("token_value"), rs.getString("default_value"));

              ArrayList tokenList = (ArrayList) stockMessageTokenMap.get(messageToken.getCompanyId());
              if(tokenList==null)
              {
                tokenList = new ArrayList();
                tokenList.add(messageToken);
                stockMessageTokenMap.put(messageToken.getCompanyId(), tokenList);
              }
              else
              {
                tokenList.add(messageToken);
              }

            }

            stockMessageTokenHandlerCacheObject.put(this.VIEW_MESSAGE_TOKENS, stockMessageTokenMap);

            rs.close();
            super.logger.debug("END LOADING MESSAGE_TOKEN HANDLER CACHE...");
        }
        catch(Exception e)
        {
            super.logger.error(e);
            throw new CacheException("Could not load StockMessageTokenHandler cache");
        }

        logger.debug(stockMessageTokenHandlerCacheObject.size() + " records loaded");
        return stockMessageTokenHandlerCacheObject;
    }


  /**
   * Set the cached object in the cache handler. The cache handler is then
   * responsible for fulfilling all application level API calls, to access the
   * data in the cached object.
   *
   * @param cachedObject
   * @throws com.ftd.osp.utilities.cache.exception.CacheException
   */
    public void setCachedObject(Object cachedObject) throws CacheException
    {
      try
      {
        this.tokenMap = (Map)((Map) cachedObject).get(this.VIEW_MESSAGE_TOKENS);
      } catch (Exception ex)
      {
        super.logger.error(ex);
        throw new CacheException("Could not set the cached object.");
      }
    }

    public ArrayList getTokensByCompany(String companyId)
    {
        ArrayList tokenList = new ArrayList();
        
        if(this.tokenMap!=null)
        {
            if(this.tokenMap.get(ALL_COMPANIES) != null)
            {
                tokenList.addAll((ArrayList) this.tokenMap.get(ALL_COMPANIES));
            }
            
            if(this.tokenMap.get(companyId) != null)
            {
                tokenList.addAll((ArrayList) this.tokenMap.get(companyId));
            }
        }
        
        return tokenList;
    }

  /**
   *
   * @param args
   */
  public static void main(String[] args)
  {
    StockMessageTokenHandler stockMessageTokenHandler = new StockMessageTokenHandler();
  }
}