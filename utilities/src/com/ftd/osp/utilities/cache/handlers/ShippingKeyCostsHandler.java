package com.ftd.osp.utilities.cache.handlers;

import com.ftd.osp.utilities.cache.exception.CacheException;
import com.ftd.osp.utilities.cache.handlers.vo.ShippingKeyCostsVO;
import com.ftd.osp.utilities.cache.vo.CacheHandlerBase;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;

import java.math.BigDecimal;

import java.sql.Connection;

import java.util.HashMap;
import java.util.Map;
/**
 * A cache handler is responsible for the following:
 * 
 * 1. Act as a cache loader, to load the data that needs to be cached.
 * 2. Provide application specific API, to enable access to the cached data.
 * 
 */

public class ShippingKeyCostsHandler extends CacheHandlerBase 
{
    // set by the setCachedObject method
    private Map keyCosts;
    private static final String SQL_STATEMENT = "GET_SHIPPING_KEY_COSTS";
    
    public ShippingKeyCostsHandler()
    {
    }

   /**
   * Returns the object that needs to be cached.
   * 
   * The cache cannot be configured as distributable, if the cache handler 
   * returns a custom object. In order to utilize the benefits of a distributable 
   * cache, the return objects should only be a part JDK API.
   * 
   * @param con
   * @return the object to be cached
   * @throws com.ftd.osp.utilities.cache.exception.CacheException
   */
    public Object load(Connection con) throws CacheException
    {
        Map shippingKeyCostsCacheObject = new HashMap();
        try
        {
            super.logger.debug("BEGIN LOADING SHIPPING KEY COSTS HANDLER...");
            DataAccessUtil dau = DataAccessUtil.getInstance();
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(con);
            dataRequest.setStatementID(SQL_STATEMENT);
            CachedResultSet rs = (CachedResultSet)dau.execute(dataRequest);

            ShippingKeyCostsVO shippingKeyCostsVO = null;
            while(rs.next())
            {
                shippingKeyCostsVO = new ShippingKeyCostsVO();
                shippingKeyCostsVO.setShippingKeyDetailId((String)rs.getObject(1));
                shippingKeyCostsVO.setShippingMethodId((String)rs.getObject(2));
                shippingKeyCostsVO.setShippingCost((BigDecimal)rs.getObject(3));

                String key = shippingKeyCostsVO.getShippingKeyDetailId() + ":" + shippingKeyCostsVO.getShippingMethodId();          
      
                shippingKeyCostsCacheObject.put(key, shippingKeyCostsVO);
            }
            super.logger.debug("END LOADING SHIPPING KEY COSTS HANDLER...");
            
        }
        catch(Exception e)
        {
            super.logger.error(e);
            throw new CacheException("Could not load Shipping Key Costs cache.");
        }
        
        logger.debug(shippingKeyCostsCacheObject.size() + " records loaded");
        return shippingKeyCostsCacheObject;
    }


  /**
   * Set the cached object in the cache handler. The cache handler is then 
   * responsible for fulfilling all application level API calls, to access the
   * data in the cached object.
   * 
   * @param cachedObject
   * @throws com.ftd.osp.utilities.cache.exception.CacheException
   */    
    public void setCachedObject(Object cachedObject) throws CacheException
    {
      try 
      {
        this.keyCosts = (Map) cachedObject;  
      } catch (Exception ex) 
      {
        super.logger.error(ex);
        throw new CacheException("Could not set the cached object.");
      } 
    }

  /**
   * 
   * @param shippingKeyDetailId
   * @param shippingMethodId
   * @return 
   */
    public ShippingKeyCostsVO getShippingKeyCosts(String shippingKeyDetailId, String shippingMethodId)
    {
        String key = shippingKeyDetailId + ":" + shippingMethodId;          

        return (this.keyCosts.containsKey(key))?(ShippingKeyCostsVO) this.keyCosts.get(key):null;

    }
}