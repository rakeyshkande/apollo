package com.ftd.osp.utilities.cache.handlers;

import com.ftd.osp.utilities.cache.exception.CacheException;
import com.ftd.osp.utilities.cache.vo.CacheHandlerBase;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.List;
/**
 * A cache handler is responsible for the following:
 * 
 * 1. Act as a cache loader, to load the data that needs to be cached.
 * 2. Provide application specific API, to enable access to the cached data.
 * 
 */

public class KeywordHandler extends CacheHandlerBase
{
    // set by the setCachedObject method
   private List keywords;

    private static final String GET_KEYWORDS = "GET KEYWORDS";
    
    public KeywordHandler()
    {        
    }

   
   /**
   * Returns the object that needs to be cached.
   * 
   * The cache cannot be configured as distributable, if the cache handler 
   * returns a custom object. In order to utilize the benefits of a distributable 
   * cache, the return objects should only be a part JDK API.
   * 
   * @param con
   * @return the object to be cached
   * @throws com.ftd.osp.utilities.cache.exception.CacheException
   */
    public Object load(Connection con) throws CacheException
    {          
        List keywordHandlerCacheObject = new ArrayList();
        try
        {
            super.logger.debug("BEGIN LOADING KEYWORD HANDLER...");
            
            DataAccessUtil dau = DataAccessUtil.getInstance();
            DataRequest dataRequest = new DataRequest();
            dataRequest.setStatementID(GET_KEYWORDS);
            dataRequest.setConnection(con);
            CachedResultSet rs = (CachedResultSet)dau.execute(dataRequest);
            rs.reset();
        
            while(rs.next())
            {
                keywordHandlerCacheObject.add(rs.getObject(1));
            }
            super.logger.debug("END LOADING KEYWORD HANDLER CACHE...");
        }
        catch(Exception e)
        {
            super.logger.error(e);
            throw new CacheException("Could not load KeywordHandler cache");
        }
        
        logger.debug(keywordHandlerCacheObject.size() + " records loaded");
        return keywordHandlerCacheObject;
    }


  /**
   * Set the cached object in the cache handler. The cache handler is then 
   * responsible for fulfilling all application level API calls, to access the
   * data in the cached object.
   * 
   * @param cachedObject
   * @throws com.ftd.osp.utilities.cache.exception.CacheException
   */    
    public void setCachedObject(Object cachedObject) throws CacheException
    {
      try 
      {
        this.keywords = (List) cachedObject;  
      } catch (Exception ex) 
      {
        super.logger.error(ex);
        throw new CacheException("Could not set the cached object.");
      } 
    }

  /**
   * 
   * @return 
   */
    public List getKeywords()
    {
        return keywords;
    }
}