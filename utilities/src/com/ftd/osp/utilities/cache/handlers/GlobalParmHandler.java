package com.ftd.osp.utilities.cache.handlers;

import com.ftd.osp.utilities.cache.exception.CacheException;
import com.ftd.osp.utilities.cache.handlers.vo.FTDAppsGlobalParmsVO;
import com.ftd.osp.utilities.cache.vo.CacheHandlerBase;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;

import java.math.BigDecimal;

import java.sql.Connection;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
/**
 * A cache handler is responsible for the following:
 * 
 * 1. Act as a cache loader, to load the data that needs to be cached.
 * 2. Provide application specific API, to enable access to the cached data.
 * 
 */

public class GlobalParmHandler extends CacheHandlerBase
{
    // set by the setCachedObject method
    private FTDAppsGlobalParmsVO ftdAppsGlobalParmsVO ;
    private Map frpGlobalParmMap;

    private static final String GET_FRP_GLOBAL_PARMS = "GET_FRP_GLOBAL_PARMS";
    private static final String GET_FTD_APPS_GLOBAL_PARMS = "GET_FTD_APPS_GLOBAL_PARMS";
    private static final String FTD_APPS_GLOBAL_PARMS_VO_KEY = "FTD_APPS_GLOBAL_PARMS_VO_KEY";
    private static final String FRP_GLOBAL_PARMS_MAP_KEY = "FRP_GLOBAL_PARMS_MAP_KEY";

    public GlobalParmHandler()
    {
        super();
    }

   
   /**
   * Returns the object that needs to be cached.
   * 
   * The cache cannot be configured as distributable, if the cache handler 
   * returns a custom object. In order to utilize the benefits of a distributable 
   * cache, the return objects should only be a part JDK API.
   * 
   * @param con
   * @return the object to be cached
   * @throws com.ftd.osp.utilities.cache.exception.CacheException
   */
    public Object load(Connection con) throws CacheException
    {  
        Map globalParmHandlerCacheObject = new HashMap();
        
        FTDAppsGlobalParmsVO ftdAppsGlobalParmsVOCacheObject = new FTDAppsGlobalParmsVO();
        Map frpGlobalParmMapCacheObject = new HashMap();
        try
        {
            // Load FTD Apps global parameters
            super.logger.debug("BEGIN LOADING GLOBAL PARAM HANDLER...");
            
            DataAccessUtil dau = DataAccessUtil.getInstance();
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(con);
            dataRequest.setStatementID(GET_FTD_APPS_GLOBAL_PARMS);
            CachedResultSet rs = (CachedResultSet)dau.execute(dataRequest);
            
            while(rs.next())
            {
                ftdAppsGlobalParmsVOCacheObject.setGnaddLevel(((BigDecimal) rs.getObject(1)).longValue());
                ftdAppsGlobalParmsVOCacheObject.setGnaddDate(this.formatStringToUtilDate((String) rs.getObject(2)));
                ftdAppsGlobalParmsVOCacheObject.setIntlOverrideDays(((BigDecimal) rs.getObject(3)).longValue());
                ftdAppsGlobalParmsVOCacheObject.setMondayCutoff((String) rs.getObject(4));
                ftdAppsGlobalParmsVOCacheObject.setTuesdayCutoff((String) rs.getObject(5));
                ftdAppsGlobalParmsVOCacheObject.setWednesdayCutoff((String) rs.getObject(6));
                ftdAppsGlobalParmsVOCacheObject.setThursdayCutoff((String) rs.getObject(7));
                ftdAppsGlobalParmsVOCacheObject.setFridayCutoff((String) rs.getObject(8));
                ftdAppsGlobalParmsVOCacheObject.setSaturdayCutoff((String) rs.getObject(9));
                ftdAppsGlobalParmsVOCacheObject.setSundayCutoff((String) rs.getObject(10));
                ftdAppsGlobalParmsVOCacheObject.setExoticCutoff((String) rs.getObject(11));
                ftdAppsGlobalParmsVOCacheObject.setFreshCutsCutoff((String) rs.getObject(12));
                ftdAppsGlobalParmsVOCacheObject.setSpecialtyGiftCutoff((String) rs.getObject(13));
                ftdAppsGlobalParmsVOCacheObject.setIntlCutoff((String) rs.getObject(14));
                ftdAppsGlobalParmsVOCacheObject.setDeliveryDaysOut(((BigDecimal) rs.getObject(15)).longValue());
                ftdAppsGlobalParmsVOCacheObject.setCheckJCPenney((String) rs.getObject(16));
                ftdAppsGlobalParmsVOCacheObject.setFreshCutsSvcCharge((BigDecimal) rs.getObject(17));
                ftdAppsGlobalParmsVOCacheObject.setSpecialSvcCharge((BigDecimal) rs.getObject(18));
                ftdAppsGlobalParmsVOCacheObject.setFreshCutsSvcChargerTrigger((String) rs.getObject(19));
                ftdAppsGlobalParmsVOCacheObject.setCanadianExchangeRate((BigDecimal) rs.getObject(20));
                ftdAppsGlobalParmsVOCacheObject.setFreshCutsSatCharge((BigDecimal) rs.getObject(21));
            }

            // Load FRP global parameters
            rs.reset();
            dataRequest.reset();
            dataRequest.setStatementID(GET_FRP_GLOBAL_PARMS);
            rs = (CachedResultSet)dau.execute(dataRequest);
            
            while(rs.next())
            {
                frpGlobalParmMapCacheObject.put((String) rs.getObject(1) + (String) rs.getObject(2), (String) rs.getObject(3));
            }
            super.logger.debug("END LOADING GLOBAL PARAM HANDLER...");
        }
        catch(Exception e)
        {
            super.logger.error(e);
            throw new CacheException("Could not load global parms data.");
        }
        globalParmHandlerCacheObject.put(this.FRP_GLOBAL_PARMS_MAP_KEY, frpGlobalParmMapCacheObject);
        globalParmHandlerCacheObject.put(this.FTD_APPS_GLOBAL_PARMS_VO_KEY, ftdAppsGlobalParmsVOCacheObject);
        
        logger.debug(frpGlobalParmMapCacheObject.size() + " records loaded");
        logger.debug(" FtdAppsGlobalParmsVOCacheObject loaded");
        return globalParmHandlerCacheObject;
    }


  /**
   * Set the cached object in the cache handler. The cache handler is then 
   * responsible for fulfilling all application level API calls, to access the
   * data in the cached object.
   * 
   * @param cachedObject
   * @throws com.ftd.osp.utilities.cache.exception.CacheException
   */    
    public void setCachedObject(Object cachedObject) throws CacheException
    {
      try 
      {
        
        Map cacheMap=(Map) cachedObject;
        this.frpGlobalParmMap = (Map)cacheMap.get(this.FRP_GLOBAL_PARMS_MAP_KEY);  
        if(cacheMap.get(this.FTD_APPS_GLOBAL_PARMS_VO_KEY)!=null){
          this.ftdAppsGlobalParmsVO=(FTDAppsGlobalParmsVO)cacheMap.get(this.FTD_APPS_GLOBAL_PARMS_VO_KEY);
	}else{
	 super.logger.warn("No vaule found for "+this.FTD_APPS_GLOBAL_PARMS_VO_KEY);
	}
          
      } catch (Exception ex) 
      {
        super.logger.error(ex);
        throw new CacheException("Could not set the cached object.");
      } 
    }

  /**
   * 
   * @return 
   */
    public FTDAppsGlobalParmsVO getFTDAppsGlobalParms()
    {
        return ftdAppsGlobalParmsVO;
    }

  /**
   * 
   * @param context
   * @param name
   * @return 
   */
    public String getFrpGlobalParm(String context, String name)
    {
       return (this.frpGlobalParmMap.containsKey(context + name))?(String) this.frpGlobalParmMap.get(context + name):null;
    }

  /**
   * 
   * @return 
   */
    public Map getFrpGlobalParmMap()
    {
        return frpGlobalParmMap;
    }
    
  /**
   * 
   * @param strDate
   * @return 
   * @throws java.lang.Exception
   */
    private Date formatStringToUtilDate(String strDate) throws Exception 
    {
        java.util.Date utilDate = null;
        String inDateFormat = "";
        int dateLength = 0;
        int firstSep = 0;
        int lastSep = 0;

        if ((strDate != null) && (!strDate.trim().equals(""))) {
            try {

                // set input date format
                dateLength = strDate.length();
                if ( dateLength > 10) {
                    inDateFormat = "yyyy-MM-dd hh:mm:ss";
                } else {
                    firstSep = strDate.indexOf("/");
                    lastSep = strDate.lastIndexOf("/");

                    switch ( dateLength ) {
                        case 10:
                            inDateFormat = "MM/dd/yyyy";
                            break;
                        case 9:
                            if ( firstSep == 1 ) {
                                inDateFormat = "M/dd/yyyy";
                            } else {
                                inDateFormat = "MM/d/yyyy";
                            }
                            break;
                        case 8:
                            if ( firstSep == 1 ) {
                                inDateFormat = "M/d/yyyy";
                            } else {
                                inDateFormat = "MM/dd/yy";
                            }
                            break;
                        case 7:
                            if ( firstSep == 1 ) {
                                inDateFormat = "M/dd/yy";
                            } else {
                                inDateFormat = "MM/d/yy";
                            }
                            break;
                        case 6:
                            inDateFormat = "M/d/yy";
                            break;
                        default:
                            break;
                    }
                }
                SimpleDateFormat sdfInput = new SimpleDateFormat( inDateFormat );
                SimpleDateFormat sdfOutput = new SimpleDateFormat ( "yyyy-MM-dd" );

                java.util.Date date = sdfInput.parse(strDate);
                String outDateString = sdfOutput.format(date);

                // now that we have no errors, use the string to make a Util date
                utilDate = sdfOutput.parse(outDateString);

                } catch (Exception e) {
                   super.logger.error(e);
                   throw new Exception();
            }
        }
        return utilDate;
    }

}