package com.ftd.osp.utilities.cache.handlers.vo;

import java.math.BigDecimal;

public class ShippingKeyCostsVO
{

  private String ShippingKeyDetailId;
  private String ShippingMethodId;
  private BigDecimal ShippingCost;

    public ShippingKeyCostsVO()
    {
    }

  public String getShippingKeyDetailId()
  {
    return ShippingKeyDetailId;
  }

  public void setShippingKeyDetailId(String newShippingKeyDetailId)
  {
    ShippingKeyDetailId = newShippingKeyDetailId;
  }

  public String getShippingMethodId()
  {
    return ShippingMethodId;
  }

  public void setShippingMethodId(String newShippingMethodId)
  {
    ShippingMethodId = newShippingMethodId;
  }

  public BigDecimal getShippingCost()
  {
    return ShippingCost;
  }

  public void setShippingCost(BigDecimal newShippingCost)
  {
    ShippingCost = newShippingCost;
  }











}