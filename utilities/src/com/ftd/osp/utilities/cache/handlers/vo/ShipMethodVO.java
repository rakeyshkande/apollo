package com.ftd.osp.utilities.cache.handlers.vo;

public class ShipMethodVO
{
    private String shipMethodId;
    private String description;
    private String novatorTag;
    private Long   maxTransitDays;
    private String sdsShipVia;
    private String sdsShipViaAir;
    
    public ShipMethodVO()
    {
    }

    public void setShipMethodId(String param)
    {
        this.shipMethodId = param;
    }

    public String getShipMethodId()
    {
        return shipMethodId;
    }


    public void setMaxTransitDays(Long param)
    {
        this.maxTransitDays = param;
    }

    public Long getMaxTransitDays()
    {
        return maxTransitDays;
    }

    public void setDescription(String param)
    {
        this.description = param;
    }

    public String getDescription()
    {
        return description;
    }

    public void setNovatorTag(String param)
    {
        this.novatorTag = param;
    }

    public String getNovatorTag()
    {
        return novatorTag;
    }

    public void setSdsShipVia(String param)
    {
        this.sdsShipVia = param;
    }

    public String getSdsShipVia()
    {
        return sdsShipVia;
    }

    public void setSdsShipViaAir(String param)
    {
        this.sdsShipViaAir = param;
    }

    public String getSdsShipViaAir()
    {
        return sdsShipViaAir;
    }
}
