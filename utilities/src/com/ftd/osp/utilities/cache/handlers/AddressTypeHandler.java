package com.ftd.osp.utilities.cache.handlers;

import com.ftd.osp.utilities.cache.exception.CacheException;
import com.ftd.osp.utilities.cache.vo.CacheHandlerBase;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.order.vo.AddressTypeVO;

import java.sql.Connection;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * A cache handler is responsible for the following:
 * 
 * 1. Act as a cache loader, to load the data that needs to be cached.
 * 2. Provide application specific API, to enable access to the cached data.
 * 
 */

public class AddressTypeHandler extends CacheHandlerBase 
{
    // set by the setCachedObject method
    private Map addressTypes;
    private Map voMap;

    private static final String GET_ADDRESS_TYPES = "GET_ADDRESS_TYPES";
    
    public AddressTypeHandler() {
      
    }
    
   /**
   * Returns the object that needs to be cached.
   * 
   * The cache cannot be configured as distributable, if the cache handler 
   * returns a custom object. In order to utilize the benefits of a distributable 
   * cache, the return objects should only be a part JDK API.
   * 
   * @param con
   * @return the object to be cached
   * @throws com.ftd.osp.utilities.cache.exception.CacheException
   */
    public Object load(Connection con) throws CacheException
    {
      Map addressTypes = new LinkedHashMap();
      Map addressTypesVO = new LinkedHashMap();
      Map cacheObject = new LinkedHashMap();      
      try
      {
        super.logger.debug("BEGIN LOADING ADDRESS TYPE HANDLER...");
        DataAccessUtil dau = DataAccessUtil.getInstance();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(con);
        dataRequest.setStatementID(GET_ADDRESS_TYPES);
        CachedResultSet rs = (CachedResultSet)dau.execute(dataRequest);
        String addressType = null;
        String typeCode = null;
        String description = null;
        AddressTypeVO addressVO = null;
        while(rs.next())
        {
            addressType = (String)rs.getObject(1);
            typeCode = (String)rs.getObject(2);
            description = (String)rs.getObject(3);
            addressTypes.put(addressType, typeCode);
            
            // build VO
            addressVO = new AddressTypeVO();
            addressVO.setCode( typeCode );
            addressVO.setType( addressType );
            addressVO.setDescription( description );
            addressVO.setPromptForBusiness("Y".equals((String) rs.getObject(4)));
            addressVO.setPromptForRoom("Y".equals((String) rs.getObject(5)));
            addressVO.setRoomLabelTxt(((String) rs.getObject(6)));
            addressVO.setPromptForHours("Y".equals((String) rs.getObject(7)));
            addressVO.setHoursLabelTxt(((String) rs.getObject(8)));

            addressTypesVO.put(addressType, addressVO);
        }  
        super.logger.debug("END LOADING ADDRESS TYPE HANDLER...");
      } catch(Exception e)
      {
          super.logger.error(e);
          throw new CacheException("Could not load address types data.");
      }
      cacheObject.put( "TYPES", addressTypes );
      cacheObject.put( "VOS", addressTypesVO );
      
      return cacheObject;      
    }
    
  /**
   * Set the cached object in the cache handler. The cache handler is then 
   * responsible for fulfilling all application level API calls, to access the
   * data in the cached object.
   * 
   * @param cachedObject
   * @throws com.ftd.osp.utilities.cache.exception.CacheException
   */    
    public void setCachedObject(Object cachedObject) throws CacheException
    {
      try 
      {
        this.addressTypes = (Map)((Map)cachedObject).get("TYPES");
        this.voMap = (Map)((Map)cachedObject).get("VOS");
      } catch (Exception ex) 
      {
        super.logger.error(ex);
        throw new CacheException("Could not set the cached object.");
      } 
    }
    
  /**
   * 
   * @return the address type map
   */
    public Map getAddressTypeMap() {
        return addressTypes;
    }


  /**
   * Returns a Map of AddressTypeVO
   * @return Map <key>FRP.ADDRESS_TYPES.CODE, <value>AddressTypeVO
   * @see com.ftd.osp.utilities.order.vo.AddressTypeVO
   */
    public Map getAddressTypeVOMap() {
        return voMap;
    }
    
    /**
   * Returns AddressTypeVO by the Address Type Code
   * @return Address Type
   * @see com.ftd.osp.utilities.order.vo.AddressTypeVO
   */
    public AddressTypeVO getAddressTypeByCode(String code) {
      AddressTypeVO res = null;
      if (code!=null && voMap!=null && addressTypes!=null) {
        Iterator it = addressTypes.keySet().iterator();
        while (it.hasNext()) {
          String addressType = (String) it.next();
          if (code.equals(addressTypes.get(addressType))) {
            res = (AddressTypeVO) voMap.get(addressType);
            break;
          }
        }
      }
      return res;
    }
    
}