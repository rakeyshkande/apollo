package com.ftd.osp.utilities.cache.handlers.vo;
import java.util.Date;
import java.util.List;

public class SourceVO 
{
    private String billingInfoLogic;
    private String billingInfoPrompt;
    private String binNumberCheckFlag;
    private String bonusPoints;
    private String bonusType;
    private String companyId;
    private String companyName;
    private String defaultSourceCodeFlag;
    private String departmentCode;
    private String description;
    private String discountAllowedFlag;
    private String emergencyTextFlag;
    private String enableLpProcessing;
    private Date   endDate;
    private String externalCallCenterFlag;
    private String fraudFlag;
    private String highlightDescriptionFlag;
    private String internetOrigin;
    private String jcpenneyFlag;
    private String listCodeFlag;
    private String marketingGroup;
    private String maximumPointsMiles;
    private String mileageBonus;
    private String mileageCalculationSource;
    private String oeDefaultFlag;
    private String orderSource;
    private String partnerId;
    private String pointsMilesPerDollar;
    private String pricingCode;
    private String promotionCode;
    private String requiresDeliveryConfirmation;
    private String sendToScrub;
    private String separateData;
    private String shippingCode;
    private String sourceCode;
    private String sourceType;
    private Date   startDate;
    private String validPayMethod;
    private String webloyaltyFlag;
    private String yellowPagesCode;
    private String primaryFlorist; 
    private List backupFloristsList;
    private String sourceCodeHasLimitIndex;
    private String relatedSourceCode;
    private String applySurchargeCode;
    private double surchargeAmount;
    private String surchargeDescription;
    private String displaySurcharge;
    private String partnerName;
    private String allowFreeShippingFlag;

    public String getSourceCode()
    {
        return sourceCode;
    }

    public void setSourceCode(String newSourceCode)
    {
        sourceCode = newSourceCode;
    }

    public String getJcpenneyFlag()
    {
        return jcpenneyFlag;
    }

    public void setJcpenneyFlag(String newJcpenneyFlag)
    {
        jcpenneyFlag = newJcpenneyFlag;
    }

    public String getPartnerId()
    {
        return partnerId;
    }

    public void setPartnerId(String newPartnerId)
    {
        partnerId = newPartnerId;
    }

    public Date getStartDate()
    {
        return startDate;
    }

    public void setStartDate(Date newStartDate)
    {
        startDate = newStartDate;
    }

    public Date getEndDate()
    {
        return endDate;
    }

    public void setEndDate(Date newEndDate)
    {
        endDate = newEndDate;
    }

    public String getValidPayMethod()
    {
        return validPayMethod;
    }

    public void setValidPayMethod(String newValidPayMethod)
    {
        validPayMethod = newValidPayMethod;
    }

    public String getSendToScrub()
    {
        return sendToScrub;
    }

    public void setSendToScrub(String newSendToScrub)
    {
        sendToScrub = newSendToScrub;
    }

    public String getPricingCode()
    {
        return pricingCode;
    }

    public void setPricingCode(String newPricingCode)
    {
        pricingCode = newPricingCode;
    }

    public String getShippingCode()
    {
        return shippingCode;
    }

    public void setShippingCode(String newShippingCode)
    {
        shippingCode = newShippingCode;
    }

    public String getCompanyName()
    {
        return companyName;
    }

    public void setCompanyName(String newCompanyName)
    {
        companyName = newCompanyName;
    }

    public String getCompanyId()
    {
        return companyId;
    }

    public void setCompanyId(String newCompanyId)
    {
        companyId = newCompanyId;
    }

    public String getInternetOrigin()
    {
        return internetOrigin;
    }

    public void setInternetOrigin(String newInternetOrigin)
    {
        internetOrigin = newInternetOrigin;
    }

    public String getFraudFlag()
    {
        return fraudFlag;
    }

    public void setFraudFlag(String newFraudFlag)
    {
        fraudFlag = newFraudFlag;
    }
/************************TARGET_2521*************************************/
  public String getBillingInfoLogic()
  {
    return billingInfoLogic;
  }

  public void setBillingInfoLogic(String newBillingInfoLogic)
  {
    billingInfoLogic = newBillingInfoLogic;
  }
  /************************TARGET_2521*************************************/

  public void setBillingInfoPrompt(String billingInfoPrompt)
  {
    this.billingInfoPrompt = billingInfoPrompt;
  }

  public String getBillingInfoPrompt()
  {
    return billingInfoPrompt;
  }

  public void setBinNumberCheckFlag(String binNumberCheckFlag)
  {
    this.binNumberCheckFlag = binNumberCheckFlag;
  }

  public String getBinNumberCheckFlag()
  {
    return binNumberCheckFlag;
  }

  public void setBonusPoints(String bonusPoints)
  {
    this.bonusPoints = bonusPoints;
  }

  public String getBonusPoints()
  {
    return bonusPoints;
  }

  public void setBonusType(String bonusType)
  {
    this.bonusType = bonusType;
  }

  public String getBonusType()
  {
    return bonusType;
  }

  public void setDefaultSourceCodeFlag(String defaultSourceCodeFlag)
  {
    this.defaultSourceCodeFlag = defaultSourceCodeFlag;
  }

  public String getDefaultSourceCodeFlag()
  {
    return defaultSourceCodeFlag;
  }

  public void setDepartmentCode(String departmentCode)
  {
    this.departmentCode = departmentCode;
  }

  public String getDepartmentCode()
  {
    return departmentCode;
  }

  public void setDescription(String description)
  {
    this.description = description;
  }

  public String getDescription()
  {
    return description;
  }

  public void setDiscountAllowedFlag(String discountAllowedFlag)
  {
    this.discountAllowedFlag = discountAllowedFlag;
  }

  public String getDiscountAllowedFlag()
  {
    return discountAllowedFlag;
  }

  public void setEmergencyTextFlag(String emergencyTextFlag)
  {
    this.emergencyTextFlag = emergencyTextFlag;
  }

  public String getEmergencyTextFlag()
  {
    return emergencyTextFlag;
  }

  public void setEnableLpProcessing(String enableLpProcessing)
  {
    this.enableLpProcessing = enableLpProcessing;
  }

  public String getEnableLpProcessing()
  {
    return enableLpProcessing;
  }

  public void setExternalCallCenterFlag(String externalCallCenterFlag)
  {
    this.externalCallCenterFlag = externalCallCenterFlag;
  }

  public String getExternalCallCenterFlag()
  {
    return externalCallCenterFlag;
  }

  public void setHighlightDescriptionFlag(String highlightDescriptionFlag)
  {
    this.highlightDescriptionFlag = highlightDescriptionFlag;
  }

  public String getHighlightDescriptionFlag()
  {
    return highlightDescriptionFlag;
  }

  public void setListCodeFlag(String listCodeFlag)
  {
    this.listCodeFlag = listCodeFlag;
  }

  public String getListCodeFlag()
  {
    return listCodeFlag;
  }

  public void setMarketingGroup(String marketingGroup)
  {
    this.marketingGroup = marketingGroup;
  }

  public String getMarketingGroup()
  {
    return marketingGroup;
  }

  public void setMaximumPointsMiles(String maximumPointsMiles)
  {
    this.maximumPointsMiles = maximumPointsMiles;
  }

  public String getMaximumPointsMiles()
  {
    return maximumPointsMiles;
  }

  public void setMileageBonus(String mileageBonus)
  {
    this.mileageBonus = mileageBonus;
  }

  public String getMileageBonus()
  {
    return mileageBonus;
  }

  public void setMileageCalculationSource(String mileageCalculationSource)
  {
    this.mileageCalculationSource = mileageCalculationSource;
  }

  public String getMileageCalculationSource()
  {
    return mileageCalculationSource;
  }

  public void setOeDefaultFlag(String oeDefaultFlag)
  {
    this.oeDefaultFlag = oeDefaultFlag;
  }

  public String getOeDefaultFlag()
  {
    return oeDefaultFlag;
  }

  public void setOrderSource(String orderSource)
  {
    this.orderSource = orderSource;
  }

  public String getOrderSource()
  {
    return orderSource;
  }

  public void setPointsMilesPerDollar(String pointsMilesPerDollar)
  {
    this.pointsMilesPerDollar = pointsMilesPerDollar;
  }

  public String getPointsMilesPerDollar()
  {
    return pointsMilesPerDollar;
  }

  public void setPromotionCode(String promotionCode)
  {
    this.promotionCode = promotionCode;
  }

  public String getPromotionCode()
  {
    return promotionCode;
  }

  public void setRequiresDeliveryConfirmation(String requiresDeliveryConfirmation)
  {
    this.requiresDeliveryConfirmation = requiresDeliveryConfirmation;
  }

  public String getRequiresDeliveryConfirmation()
  {
    return requiresDeliveryConfirmation;
  }

  public void setSeparateData(String separateData)
  {
    this.separateData = separateData;
  }

  public String getSeparateData()
  {
    return separateData;
  }

  public void setSourceType(String sourceType)
  {
    this.sourceType = sourceType;
  }

  public String getSourceType()
  {
    return sourceType;
  }

  public void setWebloyaltyFlag(String webloyaltyFlag)
  {
    this.webloyaltyFlag = webloyaltyFlag;
  }

  public String getWebloyaltyFlag()
  {
    return webloyaltyFlag;
  }

  public void setYellowPagesCode(String yellowPagesCode)
  {
    this.yellowPagesCode = yellowPagesCode;
  }

  public String getYellowPagesCode()
  {
    return yellowPagesCode;
  }

  public void setPrimaryFlorist(String newPrimaryFlorist)
  {
    this.primaryFlorist = newPrimaryFlorist;
  }

  public String getPrimaryFlorist()
  {
    return this.primaryFlorist;
  }

  public List getBackupFloristsList()
  {
    return this.backupFloristsList;
  }

  public void setBackupFloristsList(List newBackupFloristsList)
  {
    this.backupFloristsList = newBackupFloristsList;
  }


  public void setSourceCodeHasLimitIndex(String sourceCodeHasLimitIndex)
  {
    this.sourceCodeHasLimitIndex = sourceCodeHasLimitIndex;
  }

  public String getSourceCodeHasLimitIndex()
  {
    return sourceCodeHasLimitIndex;
  }

  public void setRelatedSourceCode(String relatedSourceCode)
  {
    this.relatedSourceCode = relatedSourceCode;
  }

  public String getRelatedSourceCode()
  {
    return relatedSourceCode;
  }

  public void setSurchargeAmount(double surchargeAmount)
  {
    this.surchargeAmount = surchargeAmount;
  }

  public double getSurchargeAmount()
  {
    return surchargeAmount;
  }

  public void setSurchargeDescription(String surchargeDescription)
  {
    this.surchargeDescription = surchargeDescription;
  }

  public String getSurchargeDescription()
  {
    return surchargeDescription;
  }

  public void setApplySurchargeCode(String applySurchargeCode)
  {
    this.applySurchargeCode = applySurchargeCode;
  }

  public String getApplySurchargeCode()
  {
    return applySurchargeCode;
  }

  public void setDisplaySurcharge(String displaySurcharge)
  {
    this.displaySurcharge = displaySurcharge;
  }

  public String getDisplaySurcharge()
  {
    return displaySurcharge;
  }

    public void setPartnerName(String partnerName) {
        this.partnerName = partnerName;
    }

    public String getPartnerName() {
        return partnerName;
    }

    public void setAllowFreeShippingFlag(String allowFreeShippingFlag) {
        this.allowFreeShippingFlag = allowFreeShippingFlag;
    }

    public String getAllowFreeShippingFlag() {
        return allowFreeShippingFlag;
    }
}
