package com.ftd.osp.utilities.cache.handlers;

import com.ftd.osp.utilities.cache.exception.CacheException;
import com.ftd.osp.utilities.cache.handlers.vo.CountryVO;
import com.ftd.osp.utilities.cache.handlers.vo.CountryVoComparator;
import com.ftd.osp.utilities.cache.vo.CacheHandlerBase;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;

import java.math.BigDecimal;

import java.sql.Connection;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
/**
 * A cache handler is responsible for the following:
 * 
 * 1. Act as a cache loader, to load the data that needs to be cached.
 * 2. Provide application specific API, to enable access to the cached data.
 * 
 */

public class CountryHandler extends CacheHandlerBase 
{
    // set by the setCachedObject method
    private List countryList;    

    private static final String GET_COUNTRY_MASTER = "GET_COUNTRY_MASTER";
    
    public CountryHandler()
    {
    }

  
   /**
   * Returns the object that needs to be cached.
   * 
   * The cache cannot be configured as distributable, if the cache handler 
   * returns a custom object. In order to utilize the benefits of a distributable 
   * cache, the return objects should only be a part JDK API.
   * 
   * @param con
   * @return the object to be cached
   * @throws com.ftd.osp.utilities.cache.exception.CacheException
   */
    public Object load(Connection con) throws CacheException
    {  
        List countryListCacheObject = new ArrayList();
        try
        {
            super.logger.debug("BEGIN LOADING COUNTRY HANDLER CACHE...");
            
            NumberFormat nf = new DecimalFormat("0000");
            DataAccessUtil dau = DataAccessUtil.getInstance();
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(con);
            dataRequest.setStatementID(GET_COUNTRY_MASTER);
            CachedResultSet rs = (CachedResultSet)dau.execute(dataRequest);
            //rs.reset();
            dataRequest.reset();            
            String countryId = null;
            String countryDesc = null;
            Object displayOrder = null;
            CountryVO countryVo = null;
            String countryType = null;
            String mondayClosed = null;
            String tuesdayClosed = null;
            String wednesdayClosed = null;
            String thursdayClosed = null;
            String fridayClosed = null;
            String saturdayClosed = null;
            String sundayClosed = null;
            Object addonDays = null;
            String threeCharacterId = null;
            String status = null;
            Object cutoffTime = null;
            String mondayTransit = null;
            String tuesdayTransit = null;
            String wednesdayTransit = null;
            String thursdayTransit = null;
            String fridayTransit = null;
            String saturdayTransit = null;
            String sundayTransit = null;
            while(rs.next())
            {
                countryId = (String)rs.getObject(1);
                countryDesc = (String)rs.getObject(2);
                countryType = (String)rs.getObject(3);
                displayOrder = rs.getObject(4);
                if(displayOrder == null) displayOrder = new BigDecimal("999");

                addonDays = rs.getObject(5);
                if(addonDays == null)
                {
                    addonDays = new BigDecimal("0");
                }
                cutoffTime = rs.getObject(6);
                mondayClosed = (String)rs.getObject(7);
                tuesdayClosed = (String)rs.getObject(8);
                wednesdayClosed = (String)rs.getObject(9);
                thursdayClosed = (String)rs.getObject(10);
                fridayClosed = (String)rs.getObject(11);
                saturdayClosed = (String)rs.getObject(12);
                sundayClosed = (String)rs.getObject(13);
                threeCharacterId = (String)rs.getObject(14);
                status = (String) rs.getObject(15);
                mondayTransit = (String)rs.getObject(16);
                tuesdayTransit = (String)rs.getObject(17);
                wednesdayTransit = (String)rs.getObject(18);
                thursdayTransit = (String)rs.getObject(19);
                fridayTransit = (String)rs.getObject(20);
                saturdayTransit = (String)rs.getObject(21);
                sundayTransit = (String)rs.getObject(22);
            
                if(countryId != null)
                {
                    countryId = countryId.toUpperCase();
                    countryDesc = countryDesc.toUpperCase();

                    countryVo = new CountryVO();
                    countryVo.setId(countryId);
                    countryVo.setType(countryType);
                    countryVo.setDisplayOrder((BigDecimal)displayOrder);
                    countryVo.setDescription(countryDesc);
                    countryVo.setMondayClosed(mondayClosed);
                    countryVo.setTuesdayClosed(tuesdayClosed);
                    countryVo.setWednesdayClosed(wednesdayClosed);
                    countryVo.setThursdayClosed(thursdayClosed);
                    countryVo.setFridayClosed(fridayClosed);
                    countryVo.setSaturdayClosed(saturdayClosed);
                    countryVo.setSundayClosed(sundayClosed);
                    countryVo.setAddonDays(((BigDecimal)addonDays).longValue());
                    countryVo.setThreeCharacterId(threeCharacterId);
                    countryVo.setStatus(status);
                    
                    countryVo.setCutoffTime(nf.format(((BigDecimal)cutoffTime).longValue()));
                    countryVo.setMondayTransit(mondayTransit);
                    countryVo.setTuesdayTransit(tuesdayTransit);
                    countryVo.setWednesdayTransit(wednesdayTransit);
                    countryVo.setThursdayTransit(thursdayTransit);
                    countryVo.setFridayTransit(fridayTransit);
                    countryVo.setSaturdayTransit(saturdayTransit);
                    countryVo.setSundayTransit(sundayTransit);
                    
                    countryListCacheObject.add(countryVo);
                }
            }

            Arrays.sort(countryListCacheObject.toArray(), new CountryVoComparator());
            logger.debug(countryListCacheObject.size() + " records loaded");
            super.logger.debug("END LOADING COUNTRY HANDLER CACHE...");
        }
        catch(Exception e)
        {
            super.logger.error(e);
            throw new CacheException("Could not refresh CountryHandler cache");
        }
        
        return countryListCacheObject;
    }


  /**
   * Set the cached object in the cache handler. The cache handler is then 
   * responsible for fulfilling all application level API calls, to access the
   * data in the cached object.
   * 
   * @param cachedObject
   * @throws com.ftd.osp.utilities.cache.exception.CacheException
   */    
    public void setCachedObject(Object cachedObject) throws CacheException
    {
      try 
      {
        this.countryList = (List) cachedObject;  
      } catch (Exception ex) 
      {
        super.logger.error(ex);
        throw new CacheException("Could not set the cached object.");
      } 
    }

  /**
   * Returns the CountryVO
   * 
   * @param id
   * @return 
   */
    public CountryVO getCountryById(String id)
    {
        CountryVO countryVo = null;

        Iterator it = countryList.iterator();
        CountryVO countryVoTmp = null; 
        while(it.hasNext())
        {
            countryVoTmp = (CountryVO)it.next();

            if(countryVoTmp.getId().equals(id))
            {
                countryVo = countryVoTmp;
                break;
            }
        }

        return countryVo;
    }

  /**
   * Returns the CountryVO
   * 
   * @param name
   * @return 
   */
    public CountryVO getCountryByName(String name)
    {
        CountryVO countryVo = null;

        Iterator it = countryList.iterator();
        CountryVO countryVoTmp = null; 
        while(it.hasNext())
        {
            countryVoTmp = (CountryVO)it.next();

            if(countryVoTmp.getDescription().equals(name))
            {
                countryVo = countryVoTmp;
                break;
            }
        }

        return countryVo;        
    }

  /**
   * 
   * @param countryId
   * @return 
   */
    public boolean countryExists(String countryId)
    {
        return (getCountryById(countryId) != null)?true:false;
    }

  /**
   * 
   * @return 
   */
    public List getCountryList()
    {
        return countryList;
    }
}