package com.ftd.osp.utilities.cache.handlers;

import com.ftd.osp.utilities.cache.dao.SecureConfigDAO;
import com.ftd.osp.utilities.cache.exception.CacheException;
import com.ftd.osp.utilities.cache.vo.CacheHandlerBase;

import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.HashMap;
import java.util.Iterator;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

/**
 * This cache handler is responsible for storing Secure System Configuration
 * properties.
 * 
 * A cache handler is responsible for the following:
 * 
 * 1. Act as a cache loader, to load the data that needs to be cached.
 * 2. Provide application specific API, to enable access to the cached data.
 * 
 */

public class SecureConfigHandler extends CacheHandlerBase 
{
  private static HashMap secureConfig = new HashMap();

  /**
  * Load the cache and returns the cache object.  Technically the loadContext
  * method loads the cache.
  * 
  * The cache cannot be configured as distributable, if the cache handler 
  * returns a custom object. In order to utilize the benefits of a distributable 
  * cache, the return objects should only be a part JDK API.
  * 
  * @param con - Database connection
  * @return the object to be cached
  * @throws com.ftd.osp.utilities.cache.exception.CacheException
  */
  public Object load(Connection con) throws CacheException
  {
    try
    {
      super.logger.debug("Loading the SecureConfig cache");
      
      /* Each key within the HashMap signifies a secure configuration context.
       * Itearate through the keys and reload the properties for each context.
       */
      for(Iterator it = secureConfig.keySet().iterator(); it.hasNext();)
      {
        loadContext((String)it.next(), con);
      }
    } catch(Exception e)
    {
      super.logger.error(e);
      throw new CacheException("Could not load secure system configuration data.");
    }
    
    return secureConfig;      
  }
    
  /**
   * Set the cached object in the cache handler. The cache handler is then 
   * responsible for fulfilling all application level API calls, to access the
   * data in the cached object.
   * 
   * @param cachedObject - Object to be cached
   * @throws com.ftd.osp.utilities.cache.exception.CacheException
   */    
  public void setCachedObject(Object cachedObject) throws CacheException
  {
    try 
    {
      this.secureConfig = (HashMap)cachedObject;
    } catch (Exception ex) 
    {
      super.logger.error(ex);
      throw new CacheException("Could not set the cached object.");
    } 
  }
    
  /**
   * Loads context properties into the cached object
   * @param context - Context of properties to obtain
   */
    public void loadContext(String context, Connection con)
			throws IOException, ParserConfigurationException, SAXException, 
			SQLException, Exception
		{
			// Obtain context properties
      SecureConfigDAO scDAO = new SecureConfigDAO(con);
			HashMap properties = (HashMap)scDAO.getSecureProperties(context);
			
			// Store properties in the cached object
			if(properties != null)
				secureConfig.put(context, properties);
    }


  /**
   * Returns the value of a property
   * @return String - property value
   */
  public String getProperty(String context, String name) 
  throws IOException, ParserConfigurationException, SAXException, 
  SQLException, Exception		
  {			
    Connection con = null;
    
    try
    {
      // If context does not exist within the cached object load the context.
      if(!secureConfig.containsKey(context))
      {
        con = super.getConnection();
        loadContext(context, con);				
      }
        
      // Obtain context properties from cached object
      HashMap properties = (HashMap)secureConfig.get(context);
      
      // If properties exist for the given context
      if(properties != null)
      {
        // Obtain property value
        String value = (String)properties.get(name);
        
        // Return the value if the property exists
        if(value != null)
          return value;
        // If the property does not exist throw an error
        else
          throw new Exception("Property does not exist within the" 
            + " \nSecureConfigHandler cache.  Check to make sure property exists"
            + " within the database."
            + "\nContext:" + context + "\nProperty:" + name);
      }
      // If context does not exist throw an error
      else
      {
          throw new Exception("Context does not exist within the" 
            + " \nSecureConfigHandler cache.  Check to make sure context exists"
            + " within the database."
            + "\nContext:" + context + "\nProperty:" + name);
      }
    }
    finally
    {
      if(con != null)
      {
        con.close();
        con = null;					
      }
    }
			
  }
}