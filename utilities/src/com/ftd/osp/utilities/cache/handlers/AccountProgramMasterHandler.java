package com.ftd.osp.utilities.cache.handlers;

import com.ftd.osp.utilities.cache.exception.CacheException;
import com.ftd.osp.utilities.cache.handlers.vo.AccountProgramMasterVO;
import com.ftd.osp.utilities.cache.vo.CacheHandlerBase;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * A cache handler is responsible for the following:
 * 
 * 1. Act as a cache loader, to load the data that needs to be cached.
 * 2. Provide application specific API, to enable access to the cached data.
 * 
 */

public class AccountProgramMasterHandler extends CacheHandlerBase 
{
    // set by the setCachedObject method
    private Map programMasterById;
    private Map programMasterByName;

    private static final String GET_PROGRAM_MASTER_LIST = "GET_PROGRAM_MASTER_LIST";
    private static final String KEY_PROGRAM_MASTER_ID = "KEY_PROGRAM_MASTER_ID";
    private static final String KEY_PROGRAM_MASTER_NAME = "KEY_PROGRAM_MASTER_NAME";
    
    public AccountProgramMasterHandler() {        
    }

   /**
   * Returns the object that needs to be cached.
   * 
   * The cache cannot be configured as distributable, if the cache handler 
   * returns a custom object. In order to utilize the benefits of a distributable 
   * cache, the return objects should only be a part JDK API.
   * 
   * @param con
   * @return the object to be cached
   * @throws com.ftd.osp.utilities.cache.exception.CacheException
   */
    public Object load(Connection con) throws CacheException
    {  
        Map programMasterHandlerCacheObject = new HashMap();
        Map programMasterIdObject = new HashMap();
        Map programMasterNameObject = new HashMap();

        try
        {
            super.logger.debug("BEGIN LOADING ACCOUNT PROGRAM MASTER HANDLER...");
            DataAccessUtil dau = DataAccessUtil.getInstance();
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(con);
            dataRequest.setStatementID(GET_PROGRAM_MASTER_LIST);
            CachedResultSet rs = (CachedResultSet)dau.execute(dataRequest);
            dataRequest.reset();
            while(rs.next()) {
                AccountProgramMasterVO apmVO = new AccountProgramMasterVO();
                apmVO.setProgramMasterId(rs.getInt("program_master_id"));
                apmVO.setProgramName(rs.getString("program_name"));
                apmVO.setProgramDescription(rs.getString("program_description"));
                apmVO.setActiveFlag(rs.getString("active_flag"));
                apmVO.setProgramUrl(rs.getString("program_url"));
                apmVO.setDisplayName(rs.getString("display_name"));

                programMasterIdObject.put(apmVO.getProgramMasterId(), apmVO);
                programMasterNameObject.put(apmVO.getProgramName(), apmVO);
            }

            programMasterHandlerCacheObject.put(this.KEY_PROGRAM_MASTER_ID, programMasterIdObject);
            programMasterHandlerCacheObject.put(this.KEY_PROGRAM_MASTER_NAME, programMasterNameObject);        

            super.logger.debug("END LOADING ACCOUNT PROGRAM MASTER HANDLER...");
        }
        catch(Exception e)
        {
            super.logger.error(e);
            throw new CacheException("Could not load AccountProgramMasterHandler cache");
        }

        logger.debug(programMasterIdObject.size() + " records loaded");
        return programMasterHandlerCacheObject;
    }


  /**
   * Set the cached object in the cache handler. The cache handler is then 
   * responsible for fulfilling all application level API calls, to access the
   * data in the cached object.
   * 
   * @param cachedObject
   * @throws com.ftd.osp.utilities.cache.exception.CacheException
   */    
    public void setCachedObject(Object cachedObject) throws CacheException {
        try {
            this.programMasterById = (Map)((Map) cachedObject).get(this.KEY_PROGRAM_MASTER_ID);  
            this.programMasterByName = (Map) ((Map) cachedObject).get(this.KEY_PROGRAM_MASTER_NAME);  
        } catch (Exception ex) {
            super.logger.error(ex);
            throw new CacheException("Could not set the cached object.");
        } 
    }

  /**
   * 
   * @return 
   */
    public AccountProgramMasterVO getAccountProgramMasterById(int programId) {
        if (this.programMasterById.containsKey(programId)) {
            if (this.programMasterById.get(programId) != null) {
                return (AccountProgramMasterVO) this.programMasterById.get(programId);
            }
        }
        return null;
    }

    /**
     * 
     * @return 
     */
    public AccountProgramMasterVO getAccountProgramMasterByName(String programName) {
        
        if (this.programMasterByName.containsKey(programName)) {
            if (this.programMasterByName.get(programName) != null) {
                return (AccountProgramMasterVO) this.programMasterByName.get(programName);
            }
        }
        return null;
    }
    
    
    /**
     * @return Returns all the program masters records in the cache in a list
     */
    public List<AccountProgramMasterVO> getProgramMasterList() {
      List<AccountProgramMasterVO> retVal = new ArrayList<AccountProgramMasterVO>(programMasterById.values());
      return retVal;      
    }
}