package com.ftd.osp.utilities.cache.handlers;

import com.ftd.osp.utilities.cache.exception.CacheException;
import com.ftd.osp.utilities.cache.handlers.vo.ShipMethodVO;
import com.ftd.osp.utilities.cache.vo.CacheHandlerBase;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;

import java.math.BigDecimal;

import java.sql.Connection;

import java.util.HashMap;
import java.util.Map;

public class ShipMethodHandler extends CacheHandlerBase
{
    // set by the setCachedObject method
    private Map shipMethodsById;

    private static final String GET_SHIP_METHODS = "GET_SHIP_METHODS";
    private static final String SHIP_METHODS_BY_ID_KEY = "SHIP_METHODS_BY_ID_KEY";

    public ShipMethodHandler()
    {
        super();
    }


    /**
     * Returns the object that needs to be cached.
     *
     * The cache cannot be configured as distributable, if the cache handler
     * returns a custom object. In order to utilize the benefits of a distributable
     * cache, the return objects should only be a part JDK API.
     *
     * @param con
     * @return the object to be cached
     * @throws com.ftd.osp.utilities.cache.exception.CacheException
     */
    public Object load(Connection con) throws CacheException
    {
        Map shipMethodsHandlerCacheObject = new HashMap();

        Map shipMethodsByIdCacheObject = new HashMap();
        try
        {
            super.logger.debug("BEGIN LOADING SHIP_METHODS HANDLER...");

            DataAccessUtil dau = DataAccessUtil.getInstance();
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(con);
            dataRequest.setStatementID(GET_SHIP_METHODS);
            CachedResultSet rs = (CachedResultSet)dau.execute(dataRequest);

            ShipMethodVO shipMethodVO = null;
            while (rs.next())
            {

                shipMethodVO = new ShipMethodVO();
                shipMethodVO.setShipMethodId(((String)rs.getObject(1)).toUpperCase());
                shipMethodVO.setDescription(((String)rs.getObject(2)));
                shipMethodVO.setNovatorTag(((String)rs.getObject(3)));
                shipMethodVO.setMaxTransitDays(((BigDecimal)rs.getObject(4)).longValue());
                shipMethodVO.setSdsShipVia(((String)rs.getObject(5)));
                shipMethodVO.setSdsShipViaAir(((String)rs.getObject(6)));

                shipMethodsByIdCacheObject.put(shipMethodVO.getShipMethodId(), shipMethodVO);
            }
            super.logger.debug("END LOADING SHIP_METHOD HANDLER...");
        } catch (Exception e)
        {
            super.logger.error(e);
            throw new CacheException("Could not load ship_method cache.");
        }
        shipMethodsHandlerCacheObject.put(this.SHIP_METHODS_BY_ID_KEY, shipMethodsByIdCacheObject);

        logger.debug(shipMethodsByIdCacheObject.size() + " records loaded in ship methods by id");
        return shipMethodsHandlerCacheObject;
    }


    /**
     * Set the cached object in the cache handler. The cache handler is then
     * responsible for fulfilling all application level API calls, to access the
     * data in the cached object.
     *
     * @param cachedObject
     * @throws com.ftd.osp.utilities.cache.exception.CacheException
     */
    public void setCachedObject(Object cachedObject) throws CacheException
    {
        try
        {
            this.shipMethodsById = (Map)((Map)cachedObject).get(this.SHIP_METHODS_BY_ID_KEY);
        } catch (Exception ex)
        {
            super.logger.error(ex);
            throw new CacheException("Could not set the cached object.");
        }
    }

    /**
     *
     * @param id
     * @return
     */
    public ShipMethodVO getShipMethodById(String id)
    {
        return (this.shipMethodsById.containsKey(id)) ? (ShipMethodVO)(this.shipMethodsById.get(id)) : null;
    }


    /**
     *
     * @return
     */
    public Map getShipMethodsByIdMap()
    {
        return shipMethodsById;
    }

}
