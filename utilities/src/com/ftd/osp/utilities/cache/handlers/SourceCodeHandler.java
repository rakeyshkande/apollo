package com.ftd.osp.utilities.cache.handlers;

import com.ftd.osp.utilities.cache.exception.CacheException;
import com.ftd.osp.utilities.cache.handlers.vo.SourceVO;
import com.ftd.osp.utilities.cache.vo.CacheHandlerBase;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;

import java.sql.Connection;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * A cache handler is responsible for the following:
 * 
 * 1. Act as a cache loader, to load the data that needs to be cached.
 * 2. Provide application specific API, to enable access to the cached data.
 * 
 */

public class SourceCodeHandler extends CacheHandlerBase 
{
    // set by the setCachedObject method
    private Map sourceById;
    private static final String GET_SOURCE = "GET_SOURCE";    
    
    public SourceCodeHandler()
    {
    }

   /**
   * Returns the object that needs to be cached.
   * 
   * The cache cannot be configured as distributable, if the cache handler 
   * returns a custom object. In order to utilize the benefits of a distributable 
   * cache, the return objects should only be a part JDK API.
   * 
   * @param con
   * @return the object to be cached
   * @throws com.ftd.osp.utilities.cache.exception.CacheException
   */
    public Object load(Connection con) throws CacheException
    {
        Map sourceCodeHandlerCacheObject = new HashMap();
        try
        {
            super.logger.debug("BEGIN LOADING SOURCE CODE HANDLER...");
            
            DataAccessUtil dau = DataAccessUtil.getInstance();
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(con);
            dataRequest.setStatementID(GET_SOURCE);
            CachedResultSet rs = (CachedResultSet)dau.execute(dataRequest);
            dataRequest.reset();
            SourceVO sourceVO = null;
            while(rs.next())
            {
                sourceVO = new SourceVO();
                sourceVO.setBillingInfoLogic(rs.getString("billing_info_logic"));
                sourceVO.setBillingInfoPrompt(rs.getString("billing_info_prompt"));
                sourceVO.setBinNumberCheckFlag(rs.getString("bin_number_check_flag"));
                sourceVO.setBonusPoints(rs.getString("bonus_points"));
                sourceVO.setBonusType(rs.getString("bonus_type"));
                sourceVO.setCompanyId(rs.getString("company_id"));
                sourceVO.setDefaultSourceCodeFlag(rs.getString("default_source_code_flag"));
                sourceVO.setDepartmentCode(rs.getString("department_code"));
                sourceVO.setDescription(rs.getString("description"));
                sourceVO.setDiscountAllowedFlag(rs.getString("discount_allowed_flag"));
                sourceVO.setEmergencyTextFlag(rs.getString("emergency_text_flag"));
                sourceVO.setEnableLpProcessing(rs.getString("enable_lp_processing"));
                sourceVO.setEndDate((Date)rs.getObject("end_date"));
                sourceVO.setExternalCallCenterFlag(rs.getString("external_call_center_flag"));
                sourceVO.setFraudFlag(rs.getString("fraud_flag"));
                sourceVO.setHighlightDescriptionFlag(rs.getString("highlight_description_flag"));
                sourceVO.setJcpenneyFlag(rs.getString("jcpenney_flag"));
                sourceVO.setListCodeFlag(rs.getString("list_code_flag"));
                sourceVO.setMarketingGroup(rs.getString("marketing_group"));
                sourceVO.setMaximumPointsMiles(rs.getString("maximum_points_miles"));
                sourceVO.setMileageBonus(rs.getString("mileage_bonus"));
                sourceVO.setMileageCalculationSource(rs.getString("mileage_calculation_source"));
                sourceVO.setOeDefaultFlag(rs.getString("oe_default_flag"));
                sourceVO.setOrderSource(rs.getString("order_source"));
                sourceVO.setPartnerId(rs.getString("partner_id"));
                sourceVO.setPointsMilesPerDollar(rs.getString("points_miles_per_dollar"));
                sourceVO.setPricingCode(rs.getString("pricing_code"));
                sourceVO.setPromotionCode(rs.getString("promotion_code"));
                sourceVO.setRequiresDeliveryConfirmation(rs.getString("requires_delivery_confirmation"));
                sourceVO.setSendToScrub(rs.getString("send_to_scrub"));
                sourceVO.setSeparateData(rs.getString("separate_data"));
                sourceVO.setShippingCode(rs.getString("shipping_code"));
                sourceVO.setSourceCode(rs.getString("source_code"));
                sourceVO.setSourceType(rs.getString("source_type"));
                sourceVO.setStartDate((Date)rs.getObject("start_date"));
                sourceVO.setValidPayMethod(rs.getString("valid_pay_method"));
                sourceVO.setWebloyaltyFlag(rs.getString("webloyalty_flag"));
                sourceVO.setYellowPagesCode(rs.getString("yellow_pages_code"));
                sourceVO.setCompanyName(rs.getString("company_name"));
                sourceVO.setInternetOrigin(rs.getString("internet_origin"));
                sourceVO.setPrimaryFlorist(rs.getString("primary_florist"));
                
                sourceVO.setApplySurchargeCode(rs.getString("apply_surcharge_code"));
                sourceVO.setSurchargeAmount(rs.getDouble("surcharge_amount"));
                sourceVO.setSurchargeDescription(rs.getString("surcharge_description"));
                sourceVO.setPartnerName(rs.getString("partner_name"));
                sourceVO.setDisplaySurcharge(rs.getString("display_surcharge_flag"));
                
                String backupFlorists = null; 
                List backupFloristsList = null; 
                if (rs.getString("backup_florists") != null &&  rs.getString("backup_florists") != "")
                {
                  backupFlorists = rs.getString("backup_florists");
                  backupFloristsList = new ArrayList(); 
                  
                  StringTokenizer st = new StringTokenizer(backupFlorists, " ");
                  while (st.hasMoreTokens())  
                  {
                    backupFloristsList.add(st.nextToken());
                  }
                }
                sourceVO.setBackupFloristsList(backupFloristsList);
                sourceVO.setSourceCodeHasLimitIndex(rs.getString("source_code_has_limit_index"));
                sourceVO.setRelatedSourceCode(rs.getString("related_source_code"));
                sourceVO.setAllowFreeShippingFlag(rs.getString("allow_free_shipping_flag"));
  
                sourceCodeHandlerCacheObject.put(sourceVO.getSourceCode(), sourceVO);
            }            
            super.logger.debug("END LOADING SOURCE CODE HANDLER...");
        }
        catch(Exception e)
        {
            super.logger.error(e);
            throw new CacheException("Could not load Source Code cache.");
        }
        
        logger.debug(sourceCodeHandlerCacheObject.size() + " records loaded");
        return sourceCodeHandlerCacheObject;
    }


  /**
   * Set the cached object in the cache handler. The cache handler is then 
   * responsible for fulfilling all application level API calls, to access the
   * data in the cached object.
   * 
   * @param cachedObject
   * @throws com.ftd.osp.utilities.cache.exception.CacheException
   */    
    public void setCachedObject(Object cachedObject) throws CacheException
    {
      try 
      {
        this.sourceById = (Map) cachedObject;  
      } catch (Exception ex) 
      {
        super.logger.error(ex);
        throw new CacheException("Could not set the cached object.");
      } 
    }

  /**
   * Returns the SourceVO
   * 
   * @param sourceCode
   * @return 
   */
    public SourceVO getSourceCodeDetails(String sourceCode)
    {
        super.logger.debug("SOURCE CODE="+sourceCode);
        if(this.sourceById.containsKey(sourceCode)){
           if(this.sourceById.get(sourceCode)!=null){
           	super.logger.debug("found SOURCE CODE VO");
           	return (SourceVO) this.sourceById.get(sourceCode);
           }else{
           	super.logger.debug("SOURCE CODE VO is NULL");
           	return null;
           }
	}else{
		return null;
	}
        
    }
}