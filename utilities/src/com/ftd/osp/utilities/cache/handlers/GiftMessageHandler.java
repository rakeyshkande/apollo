package com.ftd.osp.utilities.cache.handlers;

import com.ftd.osp.utilities.cache.exception.CacheException;
import com.ftd.osp.utilities.cache.vo.CacheHandlerBase;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;

import java.sql.Connection;

import java.util.HashMap;
import java.util.Map;
/**
 * A cache handler is responsible for the following:
 * 
 * 1. Act as a cache loader, to load the data that needs to be cached.
 * 2. Provide application specific API, to enable access to the cached data.
 * 
 */

public class GiftMessageHandler extends CacheHandlerBase 
{
    // set by the setCachedObject method
    private Map giftMessagesById;

    private static final String GET_GIFT_MESSAGE_LIST = "GET_GIFT_MESSAGE_LIST";
    
    public GiftMessageHandler()
    {        
    }

  
   /**
   * Returns the object that needs to be cached.
   * 
   * The cache cannot be configured as distributable, if the cache handler 
   * returns a custom object. In order to utilize the benefits of a distributable 
   * cache, the return objects should only be a part JDK API.
   * 
   * @param con
   * @return the object to be cached
   * @throws com.ftd.osp.utilities.cache.exception.CacheException
   */
    public Object load(Connection con) throws CacheException
    {  
        Map giftMsgHandlerCacheObject = new HashMap();
        try
        {
            super.logger.debug("BEGIN LOADING GIFT MESSAGE HANDLER...");
            DataAccessUtil dau = DataAccessUtil.getInstance();
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(con);
            dataRequest.setStatementID(GET_GIFT_MESSAGE_LIST);
            CachedResultSet rs = (CachedResultSet)dau.execute(dataRequest);
            //rs.reset();
            dataRequest.reset();
            Object giftMsgCode = null;
            String giftMsgDesc = null;
            while(rs.next())
            {
                giftMsgCode = rs.getObject(1);
                giftMsgDesc = (String)rs.getObject(2);

                if(giftMsgCode != null)
                {
                    giftMsgHandlerCacheObject.put(giftMsgCode.toString(), giftMsgDesc);
                }
            }
            super.logger.debug("END LOADING GIFT MESSAGE HANDLER...");
        }
        catch(Exception e)
        {
            super.logger.error(e);
            throw new CacheException("Could not load GiftMessageHandler cache");
        }

        logger.debug(giftMsgHandlerCacheObject.size() + " records loaded");
        return giftMsgHandlerCacheObject;
    }


  /**
   * Set the cached object in the cache handler. The cache handler is then 
   * responsible for fulfilling all application level API calls, to access the
   * data in the cached object.
   * 
   * @param cachedObject
   * @throws com.ftd.osp.utilities.cache.exception.CacheException
   */    
    public void setCachedObject(Object cachedObject) throws CacheException
    {
      try 
      {
        this.giftMessagesById = (Map) cachedObject;  
      } catch (Exception ex) 
      {
        super.logger.error(ex);
        throw new CacheException("Could not set the cached object.");
      } 
    }

  /**
   * 
   * @return 
   */
    public Map getGiftMessagesIdMap()
    {
        return giftMessagesById;
    }
}