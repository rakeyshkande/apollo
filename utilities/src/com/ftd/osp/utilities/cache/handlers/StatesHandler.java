package com.ftd.osp.utilities.cache.handlers;

import com.ftd.osp.utilities.cache.exception.CacheException;
import com.ftd.osp.utilities.cache.handlers.vo.StateVO;
import com.ftd.osp.utilities.cache.vo.CacheHandlerBase;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;

import java.math.BigDecimal;

import java.sql.Connection;

import java.util.HashMap;
import java.util.Map;
/**
 * A cache handler is responsible for the following:
 * 
 * 1. Act as a cache loader, to load the data that needs to be cached.
 * 2. Provide application specific API, to enable access to the cached data.
 * 
 */

public class StatesHandler extends CacheHandlerBase
{
    // set by the setCachedObject method
    private Map statesById;
    private Map statesByName;
    private Map usCanStatesById;

    private static final String GET_STATE_MASTER = "GET_STATE_MASTER";
    private static final String STATES_BY_ID_KEY = "STATES_BY_ID_KEY";
    private static final String STATES_BY_NAME_KEY = "STATES_BY_NAME_KEY";    
    private static final String US_CAN_STATES_BY_ID_KEY = "US_CAN_STATES_BY_ID_KEY";
    
    public StatesHandler()
    {        
    }
 
  
   /**
   * Returns the object that needs to be cached.
   * 
   * The cache cannot be configured as distributable, if the cache handler 
   * returns a custom object. In order to utilize the benefits of a distributable 
   * cache, the return objects should only be a part JDK API.
   * 
   * @param con
   * @return the object to be cached
   * @throws com.ftd.osp.utilities.cache.exception.CacheException
   */
    public Object load(Connection con) throws CacheException
    {  
      Map statesHandlerCacheObject = new HashMap();
      
      Map statesByIdCacheObject = new HashMap();
      Map statesByNameCacheObject = new HashMap();
      Map usCanStatesByIdCacheObject = new HashMap();
        try
        {
            super.logger.debug("BEGIN LOADING STATES HANDLER...");
            
            DataAccessUtil dau = DataAccessUtil.getInstance();
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(con);
            dataRequest.setStatementID(GET_STATE_MASTER);
            CachedResultSet rs = (CachedResultSet)dau.execute(dataRequest);

            StateVO stateVO = null;
            while(rs.next())
            {
                stateVO = new StateVO();
                stateVO.setId((rs.getString("state_master_id")).toUpperCase());
                stateVO.setName((rs.getString("state_name")).toUpperCase());
                stateVO.setCountryCode(rs.getString("country_code"));
                stateVO.setTimeZone(rs.getString("time_zone"));
                stateVO.setDropShipAvailable(rs.getString("drop_ship_available_flag"));
                
                statesByIdCacheObject.put(stateVO.getId(), stateVO);
                statesByNameCacheObject.put(stateVO.getName(), stateVO);

                if(stateVO.getCountryCode() == null || stateVO.getCountryCode().equalsIgnoreCase("CAN"))
                {
                    usCanStatesByIdCacheObject.put(stateVO.getId(), stateVO);
                }
            }
            super.logger.debug("END LOADING STATES HANDLER...");
        }
        catch(Exception e)
        {
            super.logger.error(e);
            throw new CacheException("Could not load State cache.");
        }   
      statesHandlerCacheObject.put(this.STATES_BY_ID_KEY, statesByIdCacheObject);
      statesHandlerCacheObject.put(this.STATES_BY_NAME_KEY, statesByNameCacheObject);
      statesHandlerCacheObject.put(this.US_CAN_STATES_BY_ID_KEY, usCanStatesByIdCacheObject);
      
      logger.debug(statesByIdCacheObject.size() + " records loaded in states by id");
      logger.debug(statesByNameCacheObject.size() + " records loaded in states by name");
      logger.debug(usCanStatesByIdCacheObject.size() + " records loaded in US Canada states by id");
      return statesHandlerCacheObject;      
    }


  /**
   * Set the cached object in the cache handler. The cache handler is then 
   * responsible for fulfilling all application level API calls, to access the
   * data in the cached object.
   * 
   * @param cachedObject
   * @throws com.ftd.osp.utilities.cache.exception.CacheException
   */    
    public void setCachedObject(Object cachedObject) throws CacheException
    {
      try 
      {
        this.statesById = (Map)((Map) cachedObject).get(this.STATES_BY_ID_KEY);  
        this.statesByName = (Map)((Map) cachedObject).get(this.STATES_BY_NAME_KEY);  
        this.usCanStatesById = (Map)((Map) cachedObject).get(this.US_CAN_STATES_BY_ID_KEY);  
      } catch (Exception ex) 
      {
        super.logger.error(ex);
        throw new CacheException("Could not set the cached object.");
      } 
    }

  /**
   * 
   * @param name
   * @return 
   */
    public StateVO getStateByName(String name)
    {
        return ( this.statesByName.containsKey(name) )?(StateVO)(this.statesByName.get(name)):null;
    }

  /**
   * 
   * @param id
   * @return 
   */
    public StateVO getStateById(String id)
    {
        return ( this.statesById.containsKey(id) )?(StateVO)(this.statesById.get(id)):null;
    }

  /**
   * 
   * @param id
   * @return 
   */
    public String getTimeZone(String id)
    {        
        return (this.statesById.containsKey(id))?( (StateVO)this.statesById.get(id) ).getTimeZone():null;
    }

  /**
   * 
   * @return 
   */
    public Map getStatesByIdMap()
    {
        return statesById;
    }

  /**
   * 
   * @return 
   */
    public Map getStatesByNameMap()
    {
        return statesByName;
    }
    
  /**
   * 
   * @return 
   */
    public Map getStatesByUsCanMap()
    {
        return usCanStatesById;
    }
}