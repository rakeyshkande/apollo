package com.ftd.osp.utilities.cache.handlers;

import com.ftd.osp.utilities.cache.exception.CacheException;
import com.ftd.osp.utilities.cache.handlers.vo.SNHVO;
import com.ftd.osp.utilities.cache.vo.CacheHandlerBase;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;

import java.math.BigDecimal;

import java.sql.Connection;

import java.util.HashMap;
import java.util.Map;
/**
 * A cache handler is responsible for the following:
 * 
 * 1. Act as a cache loader, to load the data that needs to be cached.
 * 2. Provide application specific API, to enable access to the cached data.
 * 
 */

public class SNHHandler extends CacheHandlerBase 
{
    // set by the setCachedObject method
    private Map snhById;
    private static final String GET_SNH = "GET_SNH";
    
    public SNHHandler()
    {
    }


   /**
   * Returns the object that needs to be cached.
   * 
   * The cache cannot be configured as distributable, if the cache handler 
   * returns a custom object. In order to utilize the benefits of a distributable 
   * cache, the return objects should only be a part JDK API.
   * 
   * @param con
   * @return the object to be cached
   * @throws com.ftd.osp.utilities.cache.exception.CacheException
   */
    public Object load(Connection con) throws CacheException
    {
       Map snhHandlerCacheObject = new HashMap();
        try
        {
            super.logger.debug("BEGIN LOADING SNH HANDLER...");
            
            DataAccessUtil dau = DataAccessUtil.getInstance();
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(con);
            dataRequest.setStatementID(GET_SNH);
            CachedResultSet rs = (CachedResultSet)dau.execute(dataRequest);

            SNHVO snhVO = null;
            while(rs.next())
            {
                snhVO = new SNHVO();
                snhVO.setId((String) rs.getObject(1));
                snhVO.setDescription((String) rs.getObject(2));
                snhVO.setFirstOrderDomestic((BigDecimal) rs.getObject(3));
                snhVO.setSecondOrderDomestic((BigDecimal) rs.getObject(4));
                snhVO.setThirdOrderDomestic((BigDecimal) rs.getObject(5));
                snhVO.setFirstOrderInternational((BigDecimal) rs.getObject(6));
                snhVO.setSecondOrderInternational((BigDecimal) rs.getObject(7));
                snhVO.setThirdOrderInternational((BigDecimal) rs.getObject(8));
                
                snhHandlerCacheObject.put(snhVO.getId(), snhVO);
            }
            super.logger.debug("END LOADING SNH HANDLER...");
        }
        catch(Exception e)
        {
            super.logger.error(e);
            throw new CacheException("Could not load SNH cache.");
        } 
        
        logger.debug(snhHandlerCacheObject.size() + " records loaded");
        return snhHandlerCacheObject;
    }


  /**
   * Set the cached object in the cache handler. The cache handler is then 
   * responsible for fulfilling all application level API calls, to access the
   * data in the cached object.
   * 
   * @param cachedObject
   * @throws com.ftd.osp.utilities.cache.exception.CacheException
   */    
    public void setCachedObject(Object cachedObject) throws CacheException
    {
      try 
      {
        this.snhById = (Map) cachedObject;  
      } catch (Exception ex) 
      {
        super.logger.error(ex);
        throw new CacheException("Could not set the cached object.");
      } 
    }

  /**
   * 
   * @param snhId
   * @return 
   */
    public SNHVO getSNHDetails(String snhId)
    {
        return (this.snhById.containsKey(snhId))?(SNHVO) this.snhById.get(snhId):null;
    }
}