package com.ftd.osp.utilities.cache.handlers.vo;

import java.util.Comparator;

public class CountryVoComparator implements Comparator
{
    public CountryVoComparator()
    {
    }

    public int compare(Object obj1, Object obj2)
    {
        int ret = 0;
        int int1 = ((CountryVO)obj1).getDisplayOrder().intValue();
        int int2 = ((CountryVO)obj2).getDisplayOrder().intValue();

        if(int1 < int2)
        {
            ret = -1;
        }
        else if(int1 > int2)
        {
            ret = 1;
        }

        return ret;
    }

    public boolean equals(Object obj1)
    {
        return false;
    }
}