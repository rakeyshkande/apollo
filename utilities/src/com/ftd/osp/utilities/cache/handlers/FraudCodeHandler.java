package com.ftd.osp.utilities.cache.handlers;

import com.ftd.osp.utilities.cache.exception.CacheException;
import com.ftd.osp.utilities.cache.vo.CacheHandlerBase;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;

import java.sql.Connection;

import java.util.HashMap;
import java.util.Map;
/**
 * A cache handler is responsible for the following:
 * 
 * 1. Act as a cache loader, to load the data that needs to be cached.
 * 2. Provide application specific API, to enable access to the cached data.
 * 
 */

public class FraudCodeHandler extends CacheHandlerBase
{
    // set by the setCachedObject method
    private Map fraudById;

    private static final String GET_NOVATOR_FRAUD_CODES = "GET_NOVATOR_FRAUD_CODES";
    
    public FraudCodeHandler()
    {        
    }

   
   /**
   * Returns the object that needs to be cached.
   * 
   * The cache cannot be configured as distributable, if the cache handler 
   * returns a custom object. In order to utilize the benefits of a distributable 
   * cache, the return objects should only be a part JDK API.
   * 
   * @param con
   * @return the object to be cached
   * @throws com.ftd.osp.utilities.cache.exception.CacheException
   */
    public Object load(Connection con) throws CacheException
    {  
       Map fraudCodeHandlerCacheObject = new HashMap();
       
        try
        {
            super.logger.debug("BEGIN LOADING FRAUD CODE HANDLER...");
            
            DataAccessUtil dau = DataAccessUtil.getInstance();
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(con);
            dataRequest.setStatementID(GET_NOVATOR_FRAUD_CODES);
            CachedResultSet rs = (CachedResultSet)dau.execute(dataRequest);

            while(rs.next())
            {
                fraudCodeHandlerCacheObject.put((String) rs.getObject(1), (String) rs.getObject(2));
            }
            
            logger.debug(fraudCodeHandlerCacheObject.size() + " records loaded");
            super.logger.debug("END LOADING FRAUD CODE HANDLER...");
        }
        catch(Exception e)
        {
            super.logger.error(e);
            throw new CacheException("Could not load Fraud cache.");
        }
        
        return fraudCodeHandlerCacheObject;
    }

  /**
   * Set the cached object in the cache handler. The cache handler is then 
   * responsible for fulfilling all application level API calls, to access the
   * data in the cached object.
   * 
   * @param cachedObject
   * @throws com.ftd.osp.utilities.cache.exception.CacheException
   */    
    public void setCachedObject(Object cachedObject) throws CacheException
    {
      try 
      {
        this.fraudById = (Map) cachedObject;  
      } catch (Exception ex) 
      {
        super.logger.error(ex);
        throw new CacheException("Could not set the cached object.");
      } 
    }

  /**
   * 
   * @param id
   * @return 
   */
    public String getFraudDescriptionById(String id)
    {
        return (this.fraudById.containsKey(id))?(String)this.fraudById.get(id):null;
    }

  /**
   * 
   * @param id
   * @return 
   */
    public Map getFraudMap(String id)
    {
        return fraudById;
    }

}