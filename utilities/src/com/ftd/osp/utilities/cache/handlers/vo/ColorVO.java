package com.ftd.osp.utilities.cache.handlers.vo;

public class ColorVO
{
    private String id;
    private String description;

    public ColorVO(String id, String description)
    {
        this.id = id;
        this.description = description;
    }
    
    public String getId()
    {
        return id;
    }

    public void setId(String newId)
    {
        id = newId;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String newDescription)
    {
        description = newDescription;
    }
    
}