package com.ftd.osp.utilities.cache.handlers;

import com.ftd.osp.utilities.cache.exception.CacheException;
import com.ftd.osp.utilities.cache.handlers.vo.ShippingKeyDetailVO;
import com.ftd.osp.utilities.cache.vo.CacheHandlerBase;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;

import java.math.BigDecimal;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
/**
 * A cache handler is responsible for the following:
 * 
 * 1. Act as a cache loader, to load the data that needs to be cached.
 * 2. Provide application specific API, to enable access to the cached data.
 * 
 */

public class ShippingKeyDetailsHandler extends CacheHandlerBase 
{
    // set by the setCachedObject method
    private List shippingKeyDetailList;
    private static final String GET_SHIPPING_KEY_DETAILS = "GET_SHIPPING_KEY_DETAILS";
    
    public ShippingKeyDetailsHandler()
    {
    }

   /**
   * Returns the object that needs to be cached.
   * 
   * The cache cannot be configured as distributable, if the cache handler 
   * returns a custom object. In order to utilize the benefits of a distributable 
   * cache, the return objects should only be a part JDK API.
   * 
   * @param con
   * @return the object to be cached
   * @throws com.ftd.osp.utilities.cache.exception.CacheException
   */
    public Object load(Connection con) throws CacheException
    {
        List shippingKeyDetailsHandlerCacheObject = new ArrayList();
        try
        {
            super.logger.debug("BEGIN LOADING SHIPPING KEY DETAILS HANDLER...");
            
            DataAccessUtil dau = DataAccessUtil.getInstance();
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(con);
            dataRequest.setStatementID(GET_SHIPPING_KEY_DETAILS);
            CachedResultSet rs = (CachedResultSet)dau.execute(dataRequest);

            ShippingKeyDetailVO shippingKeyDetailVO = null;
            while(rs.next())
            {
                shippingKeyDetailVO = new ShippingKeyDetailVO();
                shippingKeyDetailVO.setShippingDetailId((String) rs.getObject(1));
                shippingKeyDetailVO.setShippingKeyId((String) rs.getObject(2));
                shippingKeyDetailVO.setMinPrice((BigDecimal) rs.getObject(3));
                shippingKeyDetailVO.setMaxPrice((BigDecimal) rs.getObject(4));
                
                shippingKeyDetailsHandlerCacheObject.add(shippingKeyDetailVO);
            }
            super.logger.debug("END LOADING SHIPPING KEY DETAILS HANDLER...");
        }
        catch(Exception e)
        {
            super.logger.error(e);
            throw new CacheException("Could not load Source Code cache.");
        }
        
        logger.debug(shippingKeyDetailsHandlerCacheObject.size() + " records loaded");
        return shippingKeyDetailsHandlerCacheObject;
    }


  /**
   * Set the cached object in the cache handler. The cache handler is then 
   * responsible for fulfilling all application level API calls, to access the
   * data in the cached object.
   * 
   * @param cachedObject
   * @throws com.ftd.osp.utilities.cache.exception.CacheException
   */    
    public void setCachedObject(Object cachedObject) throws CacheException
    {
      try 
      {
        this.shippingKeyDetailList = (List) cachedObject;  
      } catch (Exception ex) 
      {
        super.logger.error(ex);
        throw new CacheException("Could not set the cached object.");
      } 
    }

  /**
   * 
   * @param shippingKeyId
   * @param price
   * @return 
   */
    public ShippingKeyDetailVO getShippingKeyDetails(String shippingKeyId, BigDecimal price)
    {
        ShippingKeyDetailVO shippingKeyDetailVO = null;
        Iterator shippingKeyDetailIterator = shippingKeyDetailList.iterator();
        while(shippingKeyDetailIterator.hasNext())
        {
            shippingKeyDetailVO = (ShippingKeyDetailVO) shippingKeyDetailIterator.next();
            if(shippingKeyId.equals(shippingKeyDetailVO.getShippingKeyId()) &&
                price.compareTo(shippingKeyDetailVO.getMinPrice()) >= 0  &&
                price.compareTo(shippingKeyDetailVO.getMaxPrice()) <= 0)
            {
                return shippingKeyDetailVO;  
            }
        }
        
        return null;
    }
}