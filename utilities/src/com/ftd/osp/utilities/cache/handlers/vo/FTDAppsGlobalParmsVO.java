package com.ftd.osp.utilities.cache.handlers.vo;

import java.util.Date;
import java.math.BigDecimal;

public class FTDAppsGlobalParmsVO 
{
    private long gnaddLevel;
    private Date gnaddDate;
    private long intlOverrideDays;
    private String mondayCutoff;
    private String tuesdayCutoff;
    private String wednesdayCutoff;
    private String thursdayCutoff;
    private String fridayCutoff;
    private String saturdayCutoff;
    private String sundayCutoff;
    private String freshCutsCutoff;
    private String specialtyGiftCutoff;
    private long deliveryDaysOut;
    private String intlCutoff;
    private String exoticCutoff;
    private String checkJCPenney;
    private BigDecimal freshCutsSvcCharge;
    private BigDecimal specialSvcCharge;
    private String freshCutsSvcChargerTrigger;
    private BigDecimal canadianExchangeRate;
    private String dispatchOrderFlag;
    private BigDecimal freshCutsSatCharge;

    // Modify Order Members
    private String MODcutoffFlag;
    private String MODcutoffTime;
    private Integer MODminDaysOut;
    private Integer MODmaxDaysOut;

    public FTDAppsGlobalParmsVO()
    {
    }

    public long getGnaddLevel()
    {
        return gnaddLevel;
    }

    public void setGnaddLevel(long newGnaddLevel)
    {
        gnaddLevel = newGnaddLevel;
    }

    public Date getGnaddDate()
    {
        return gnaddDate;
    }

    public void setGnaddDate(Date newGnaddDate)
    {
        gnaddDate = newGnaddDate;
    }

    public long getIntlOverrideDays()
    {
        return intlOverrideDays;
    }

    public void setIntlOverrideDays(long newIntlOverrideDays)
    {
        intlOverrideDays = newIntlOverrideDays;
    }

    public String getMondayCutoff()
    {
        return mondayCutoff;
    }

    public void setMondayCutoff(String newMondayCutoff)
    {
        mondayCutoff = newMondayCutoff;
    }

    public String getTuesdayCutoff()
    {
        return tuesdayCutoff;
    }

    public void setTuesdayCutoff(String newTuesdayCutoff)
    {
        tuesdayCutoff = newTuesdayCutoff;
    }

    public String getWednesdayCutoff()
    {
        return wednesdayCutoff;
    }

    public void setWednesdayCutoff(String newWednesdayCutoff)
    {
        wednesdayCutoff = newWednesdayCutoff;
    }

    public String getThursdayCutoff()
    {
        return thursdayCutoff;
    }

    public void setThursdayCutoff(String newThursdayCutoff)
    {
        thursdayCutoff = newThursdayCutoff;
    }

    public String getFridayCutoff()
    {
        return fridayCutoff;
    }

    public void setFridayCutoff(String newFridayCutoff)
    {
        fridayCutoff = newFridayCutoff;
    }

    public String getSaturdayCutoff()
    {
        return saturdayCutoff;
    }

    public void setSaturdayCutoff(String newSaturdayCutoff)
    {
        saturdayCutoff = newSaturdayCutoff;
    }

    public String getSundayCutoff()
    {
        return sundayCutoff;
    }

    public void setSundayCutoff(String newSundayCutoff)
    {
        sundayCutoff = newSundayCutoff;
    }

    public String getFreshCutsCutoff()
    {
        return freshCutsCutoff;
    }

    public void setFreshCutsCutoff(String newFreshCutsCutoff)
    {
        freshCutsCutoff = newFreshCutsCutoff;
    }

    public String getSpecialtyGiftCutoff()
    {
        return specialtyGiftCutoff;
    }

    public void setSpecialtyGiftCutoff(String newSpecialtyGiftCutoff)
    {
        specialtyGiftCutoff = newSpecialtyGiftCutoff;
    }

    public long getDeliveryDaysOut()
    {
        return deliveryDaysOut;
    }

    public void setDeliveryDaysOut(long newDeliveryDaysOut)
    {
        deliveryDaysOut = newDeliveryDaysOut;
    }

    public String getIntlCutoff()
    {
        return intlCutoff;
    }

    public void setIntlCutoff(String newIntlCutoff)
    {
        intlCutoff = newIntlCutoff;
    }

    public String getExoticCutoff()
    {
        return exoticCutoff;
    }

    public void setExoticCutoff(String newExoticCutoff)
    {
        exoticCutoff = newExoticCutoff;
    }

    public String getCheckJCPenney()
    {
        return checkJCPenney;
    }

    public void setCheckJCPenney(String newCheckJCPenney)
    {
        checkJCPenney = newCheckJCPenney;
    }

    public BigDecimal getFreshCutsSvcCharge()
    {
        return freshCutsSvcCharge;
    }

    public void setFreshCutsSvcCharge(BigDecimal newFreshCutsSvcCharge)
    {
        freshCutsSvcCharge = newFreshCutsSvcCharge;
    }

    public BigDecimal getSpecialSvcCharge()
    {
        return specialSvcCharge;
    }

    public void setSpecialSvcCharge(BigDecimal newSpecialSvcCharge)
    {
        specialSvcCharge = newSpecialSvcCharge;
    }

    public String getFreshCutsSvcChargerTrigger()
    {
        return freshCutsSvcChargerTrigger;
    }

    public void setFreshCutsSvcChargerTrigger(String newFreshCutsSvcChargerTrigger)
    {
        freshCutsSvcChargerTrigger = newFreshCutsSvcChargerTrigger;
    }

    public BigDecimal getCanadianExchangeRate()
    {
        return canadianExchangeRate;
    }

    public void setCanadianExchangeRate(BigDecimal newCanadianExchangeRate)
    {
        canadianExchangeRate = newCanadianExchangeRate;
    }

    public String getDispatchOrderFlag()
    {
        return dispatchOrderFlag;
    }

    public void setDispatchOrderFlag(String newDispatchOrderFlag)
    {
        dispatchOrderFlag = newDispatchOrderFlag;
    }

    public BigDecimal getFreshCutsSatCharge()
    {
        return freshCutsSatCharge;
    }

    public void setFreshCutsSatCharge(BigDecimal newFreshCutsSatCharge)
    {
        freshCutsSatCharge = newFreshCutsSatCharge;
    }


  public void setMODcutoffFlag(String MODcutoffFlag)
  {
    this.MODcutoffFlag = MODcutoffFlag;
  }


  public String getMODcutoffFlag()
  {
    return MODcutoffFlag;
  }


  public void setMODcutoffTime(String MODcutoffTime)
  {
    this.MODcutoffTime = MODcutoffTime;
  }


  public String getMODcutoffTime()
  {
    return MODcutoffTime;
  }


  public void setMODminDaysOut(Integer MODminDaysOut)
  {
    this.MODminDaysOut = MODminDaysOut;
  }


  public Integer getMODminDaysOut()
  {
    return MODminDaysOut;
  }


  public void setMODmaxDaysOut(Integer MODmaxDaysOut)
  {
    this.MODmaxDaysOut = MODmaxDaysOut;
  }


  public Integer getMODmaxDaysOut()
  {
    return MODmaxDaysOut;
  }
}