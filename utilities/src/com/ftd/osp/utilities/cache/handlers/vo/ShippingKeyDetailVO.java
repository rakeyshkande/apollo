package com.ftd.osp.utilities.cache.handlers.vo;
import java.math.BigDecimal;

public class ShippingKeyDetailVO 
{
    private String shippingDetailId;
    private String shippingKeyId;
    private BigDecimal minPrice;
    private BigDecimal maxPrice;

    public ShippingKeyDetailVO()
    {
    }

    public String getShippingDetailId()
    {
        return shippingDetailId;
    }

    public void setShippingDetailId(String newShippingDetailId)
    {
        shippingDetailId = newShippingDetailId;
    }

    public String getShippingKeyId()
    {
        return shippingKeyId;
    }

    public void setShippingKeyId(String newShippingKeyId)
    {
        shippingKeyId = newShippingKeyId;
    }

    public BigDecimal getMinPrice()
    {
        return minPrice;
    }

    public void setMinPrice(BigDecimal newMinPrice)
    {
        minPrice = newMinPrice;
    }

    public BigDecimal getMaxPrice()
    {
        return maxPrice;
    }

    public void setMaxPrice(BigDecimal newMaxPrice)
    {
        maxPrice = newMaxPrice;
    }
}