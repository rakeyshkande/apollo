package com.ftd.osp.utilities.cache.handlers;

import com.ftd.osp.utilities.cache.exception.CacheException;
import com.ftd.osp.utilities.cache.handlers.vo.StateVO;
import com.ftd.osp.utilities.cache.vo.CacheHandlerBase;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;

import java.math.BigDecimal;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SpellCheckHandler extends CacheHandlerBase {
    // set by the setCachedObject method
    private static List dictionaryList;
    private static Date lastUpdate = new Date();
    
    public SpellCheckHandler() {
    }

    /**
    * Returns the object that needs to be cached.
    * 
    * The cache cannot be configured as distributable, if the cache handler 
    * returns a custom object. In order to utilize the benefits of a distributable 
    * cache, the return objects should only be a part JDK API.
    * 
    * @param con
    * @return the object to be cached
    * @throws com.ftd.osp.utilities.cache.exception.CacheException
    */
    public Object load(Connection con) throws CacheException {
        
        List tempList = new ArrayList();
        
        try
        {
            super.logger.debug("BEGIN LOADING SPELL CHECK DICTIONARY HANDLER...");
            
            DataAccessUtil dau = DataAccessUtil.getInstance();
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(con);
            dataRequest.setStatementID("GET_DICTIONARY");
            CachedResultSet rs = (CachedResultSet)dau.execute(dataRequest);
            String word;
            
            while(rs.next())
            {
                word = rs.getString(1);
                if( word!=null ) {
                    tempList.add(word);
                }
            }
            
            SpellCheckHandler.dictionaryList = tempList;
            SpellCheckHandler.lastUpdate = new Date();
            super.logger.debug("END LOADING SPELL CHECK DICTIONARY HANDLER...");
        }
        catch(Exception e)
        {
          super.logger.error(e);
          throw new CacheException("Could not load SpellCheckHandler cache.");
        }   
          
        return SpellCheckHandler.dictionaryList;      
    }

    /**
     * Set the cached object in the cache handler. The cache handler is then 
     * responsible for fulfilling all application level API calls, to access the
     * data in the cached object.
     * 
     * @param cachedObject
     * @throws com.ftd.osp.utilities.cache.exception.CacheException
     */    
    public void setCachedObject(Object cachedObject) throws CacheException {
        try {
            SpellCheckHandler.dictionaryList = (List)cachedObject;  
        } catch (Exception ex) {
            super.logger.error(ex);
            throw new CacheException("Could not set the cached object.");
        } 
    }

  /**
   * 
   * @return the dictionary list
   */
    public List getDictionaryList()
    {
        return SpellCheckHandler.dictionaryList;
    }

    /**
     * Get the time of the last update from the database
     * @return
     */
    public Date getLastUpdate() {
          return SpellCheckHandler.lastUpdate;
      }
}
