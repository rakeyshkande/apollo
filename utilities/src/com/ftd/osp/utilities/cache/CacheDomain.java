package com.ftd.osp.utilities.cache;

import com.ftd.osp.utilities.plugins.Logger;
import java.util.HashMap;

/**
 *  Represents an Cache Domain 
 *
 * @author Anshu Gaind
 */
public class CacheDomain {
    private HashMap domainEntries;
    private String domainName;
    private Logger logger;

    public CacheDomain(String domainName) {
        this.domainName = domainName;
        domainEntries = new HashMap();
        logger = new Logger("com.ftd.osp.utilities.cache.CacheDomain");
    }

    public void setDomainEntry(String name, Object value) {
      domainEntries.put(name.trim(), value);
      logger.debug("Setting domain entry " + name);
    }

    public Object getDomainEntry(String name){
      return domainEntries.get(name);
    }
}