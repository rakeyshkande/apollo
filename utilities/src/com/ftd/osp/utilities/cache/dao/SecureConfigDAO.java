package com.ftd.osp.utilities.cache.dao;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.HashMap;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

public class SecureConfigDAO 
{
  private static Logger logger = new Logger("com.ftd.osp.utilities.dataaccess.SecureConfigDAO");

  private Connection con = null;
  
  // Stored procedure names
  private static final String GET_SECURE_CONTEXT = "GET_SECURE_CONTEXT";
  
  /**
   * Constructor which accepts a database connection 
   * @param con - Database connection
   */
  public SecureConfigDAO(Connection con)
  {
    this.con = con;
  }

  /**
   * Obtains secure configuration properties for a given context. 
   * @param context - Context
   * @return HashMap - Hashmap of secure configuration properties
   */
  public HashMap getSecureProperties(String context)
    throws IOException, ParserConfigurationException, SAXException, 
      SQLException, Exception
  {
    HashMap properties = null;
    
    DataAccessUtil dau = DataAccessUtil.getInstance();
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(con);
    dataRequest.setStatementID(GET_SECURE_CONTEXT);
    dataRequest.addInputParam("IN_CONTEXT", context);
    CachedResultSet rs = (CachedResultSet)dau.execute(dataRequest);
    
    if(rs.getRowCount() > 0)
      properties = new HashMap();
    
    String name = null;
    String value = null;
    while(rs.next())
    {
      name = rs.getString(2);
      value = rs.getString(3);
      
      properties.put(name, value);
    }  
    
    return properties;
  }
}
