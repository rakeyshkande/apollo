package com.ftd.osp.utilities;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.AddOnVO;

import com.ftd.osp.utilities.vo.VendorAddOnVO;

import com.ftd.osp.utilities.xml.DOMUtil;

import java.io.StringReader;

import java.sql.Connection;

import java.util.ArrayList;

import java.util.Date;
import java.util.HashMap;

import java.util.HashSet;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import org.xml.sax.InputSource;

public class AddOnUtility
{
    private static Logger logger = new Logger("com.ftd.osp.utilities.AddonUtility");

    //SQL Queries
    private final static String GET_ADDON_BY_ADDON_ID = "GET_ADDON_BY_ADDON_ID";
    private final static String GET_ADDON_CUR_BY_PROD_OCCA = "GET_ADDON_CUR_BY_PROD_OCCA";
    private final static String GET_ADDON_CUR_BY_PROD_OCCA_DTP = "GET_ADDON_CUR_BY_PROD_OCCA_DTP";
    private final static String GET_ATV_ADDON_CUR_BY_VENDOR_ID = "GET_ATV_ADDON_CUR_BY_VENDOR_ID";
    private final static String GET_ALL_ADDON_CUR_BY_VENDOR_ID = "GET_ALL_ADDON_CUR_BY_VENDOR_ID";
    private final static String GET_ADDON_CUR_BY_VENUS_ID = "GET_ADDON_CUR_BY_VENUS_ID";
    private final static String GET_ADDON_CUR_BY_DETAIL_ID = "GET_ADDON_CUR_BY_DETAIL_ID";
    private final static String GET_ALL_ADDON_CUR = "GET_ALL_ADDON_CUR";
    private final static String GET_VENDOR_BY_ADDON_PRODUCT = "GET_VENDOR_BY_ADDON_PRODUCT";
    private final static String ORDER_ADD_ON_AVAILABILITY = "ORDER_ADD_ON_AVAILABILITY";
    private final static String VENDOR_ADD_ON_AVAILABILITY = "VENDOR_ADD_ON_AVAILABILITY";
    private final static String GET_IS_ONLY_OF_TYPE_ASSIGNED = "GET_IS_ONLY_OF_TYPE_ASSIGNED";
    private final static String GET_IS_LAST_VENDOR_ASSIGNED = "GET_IS_LAST_VENDOR_ASSIGNED";
      
    
    //SQL IN Params
    private final static  String IN_ADD_ON_ID = "IN_ADD_ON_ID";
    private final static  String IN_PRODUCT_ID = "IN_PRODUCT_ID";
    private final static  String IN_OCCASION_ID = "IN_OCCASION_ID";  
    private final static  String IN_VENDOR_ID = "IN_VENDOR_ID";
    private final static  String IN_ORDER_DETAIL_ID = "IN_ORDER_DETAIL_ID";
    private final static  String IN_VENUS_ID = "IN_VENUS_ID";
    private final static  String IN_VENDOR_INFO_FLAG = "IN_VENDOR_INFO_FLAG";
    private final static  String IN_PRODUCT_INFO_FLAG = "IN_PRODUCT_INFO_FLAG";
    private final static  String IN_SOURCE_CODE = "IN_SOURCE_CODE";
    private final static  String IN_ADD_ON_LIST = "IN_ADD_ON_ID_STRING";
    private final static  String IN_ADD_ON_COUNT = "IN_ADD_ON_COUNT";
    private final static  String IN_FLORIST_AVAILABLE = "IN_FLORIST_AVAILABLE";
    private final static  String IN_DROPSHIP_AVAILABLE = "IN_DROPSHIP_AVAILABLE";
    

    //SQL OUT Params
    private final static  String OUT_CUR = "OUT_CUR";
    private final static  String OUT_VENDOR_CUR = "OUT_VENDOR_CUR";
    private final static  String OUT_PRODUCT_CUR = "OUT_PRODUCT_CUR";

    //SQL OUT Attributes
    private final static  String ADDON_ID = "ADDON_ID";
    private final static  String ADDON_TYPE = "ADDON_TYPE";
    private final static  String ADD_ON_DESCRIPTION = "ADD_ON_DESCRIPTION";
    private final static  String ADDON_TEXT = "ADDON_TEXT";
    private final static  String UNSPSC = "UNSPSC";
    private final static  String PRICE = "PRICE";
    private final static  String ADD_ON_ACTIVE_FLAG = "ADD_ON_ACTIVE_FLAG";
    private final static  String DEFAULT_PER_TYPE_FLAG = "DEFAULT_PER_TYPE_FLAG";
    private final static  String DISPLAY_PRICE_FLAG = "DISPLAY_PRICE_FLAG";
    private final static  String DISPLAY_SEQ_NUMBER = "DISPLAY_SEQ_NUM";
    private final static  String PRODUCT_FEED_FLAG = "PRODUCT_FEED_FLAG";
    private final static  String ADD_ON_TYPE_DESCRIPTION = "ADD_ON_TYPE_DESCRIPTION";
    private final static  String ADD_ON_QUANTITY = "ADD_ON_QUANTITY";    
    private final static  String VENDOR_ID = "VENDOR_ID";
    private final static  String VENDOR_SKU = "VENDOR_SKU";
    private final static  String VENDOR_COST = "VENDOR_COST";
    private final static  String VENDOR_ACTIVE_FLAG = "VENDOR_ACTIVE_FLAG";
    private final static  String PRODUCT_ID = "PRODUCT_ID";
    private final static  String MAX_QTY = "MAX_QTY";
    private final static  String ADDON_WEIGHT = "ADDON_WEIGHT";
    private final static  String ADD_ON_TYPE_COUNT = "ADD_ON_TYPE_COUNT";
    private final static  String OCCASION_ID = "OCCASION_ID";
    private final static  String OCCASION_DESCRIPTION = "OCCASION_DESCRIPTION";
    private final static  String ADDON_DELIVERY_TYPE = "ADDON_DELIVERY_TYPE";
    private final static  String LINKED_OCCASIONS = "LINKED_OCCASIONS";// Changes for Apollo.Q3.2015 - Addon-Occasion association
    /*
     * DI-4: Add-on Dashboard Enhancements: Added 2 new attributes to add-on : PQUAD_ACCESSORY_ID, IS_FTD_WEST_ADDON.
     */
    private final static  String PQUAD_ACCESSORY_ID = "PQUAD_ACCESSORY_ID";
    private final static  String IS_FTD_WEST_ADDON = "IS_FTD_WEST_ADDON";


    //XML tags
    private final static  String ADD_ON_XML_TOP_TAG = "add_ons";
    
    //Global Parm Infor
    private final static String ADDON_VALUES_CONTEXT = "ADDON_VALUES";
    private final static String MAX_DISPLAY_COUNT_NAME = "MAX_DISPLAY_COUNT";
    private final static String MAX_VASE_DISPLAY_COUNT_NAME = "MAX_VASE_DISPLAY_COUNT";

   
    /**
      * Returns the list of add-ons that apply to the given filter
      * @param productId The product Id to filter on
      * @param occasionId The occasion Id to filter on
      * @param sourceCode The source code to filter on
      * @param returnAllAddOnFlag True if all add-ons are desired, false if the list should be trimed to the 'ADDON_VALUES', 'MAX_DISPLAY_COUNT' global parm amount
      * @param conn Database connection
      * @return Map of add-ons that fit the search criteria
      * @throws Exception
      */
    
    public HashMap <String, ArrayList <AddOnVO>> getActiveAddonListByProductIdAndOccasionAndSourceCode
                                (String productId, Integer occasionId, String sourceCode, Boolean returnAllAddOnFlag, Connection  conn) throws Exception
    {
        if (returnAllAddOnFlag == null)
        {
            returnAllAddOnFlag = true;
        }
        
        HashMap inputParams = new HashMap();
        DataRequest dataRequest = new DataRequest();

        dataRequest.setConnection(conn);
        dataRequest.setStatementID(GET_ADDON_CUR_BY_PROD_OCCA);
        inputParams.put(IN_PRODUCT_ID, productId);
        inputParams.put(IN_OCCASION_ID, occasionId);
        inputParams.put(IN_SOURCE_CODE, sourceCode);

        dataRequest.setInputParams(inputParams);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        CachedResultSet rs = null;
        rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);

        HashMap <String, ArrayList<AddOnVO>> addonMap = new HashMap <String, ArrayList<AddOnVO>>();
        ArrayList <AddOnVO> addOnVOList = new ArrayList<AddOnVO>();
        ArrayList <AddOnVO> vaseAddOnVOList = new ArrayList<AddOnVO>();
        ArrayList <AddOnVO> bannerAddOnVOList = new ArrayList<AddOnVO>();
        ArrayList <AddOnVO> cardAddOnVOList = new ArrayList<AddOnVO>();

        if( rs!=null ) 
        {
            while( rs.next() ) 
            {
                AddOnVO addOnVO =  getAddonFromResultSet(rs);
                if (StringUtils.equals(addOnVO.getAddOnTypeId(),String.valueOf(AddOnVO.ADD_ON_TYPE_VASE_ID)))
                {
                    vaseAddOnVOList.add(addOnVO);
                }
                else if (StringUtils.equals(addOnVO.getAddOnTypeId(),String.valueOf(AddOnVO.ADD_ON_TYPE_BANNER_ID)))
                {
                    bannerAddOnVOList.add(addOnVO);
                }
                else if (StringUtils.equals(addOnVO.getAddOnTypeId(),String.valueOf(AddOnVO.ADD_ON_TYPE_CARD_ID)))
                {
                    cardAddOnVOList.add(addOnVO);
                }
                else
                {
                    addOnVOList.add(addOnVO);
                }
            }
            if (!returnAllAddOnFlag)
            {
                ConfigurationUtil configurationUtil = new ConfigurationUtil();
                String maxDisplayCountString = configurationUtil.getFrpGlobalParm(ADDON_VALUES_CONTEXT,MAX_DISPLAY_COUNT_NAME);
                if (maxDisplayCountString != null)
                {
                    Integer maxDisplayCount = Integer.parseInt(maxDisplayCountString);
                    if (cardAddOnVOList.size() > 0)
                    {
                        maxDisplayCount--;
                    }
    
                    if (addOnVOList.size() > maxDisplayCount)
                    {
                        ArrayList <AddOnVO> trimmedAddOnVOList = new ArrayList <AddOnVO> ();
                        for (int i = 0; i < maxDisplayCount; i++)
                        {
                            trimmedAddOnVOList.add(addOnVOList.get(i));
                        }
                        addOnVOList = trimmedAddOnVOList;
                    }
                }
                else
                {
                    logger.error("Missing max display count global parm for add-ons.  Context: " + ADDON_VALUES_CONTEXT +
                                 " Value: " + MAX_DISPLAY_COUNT_NAME);
                }
                
                maxDisplayCountString = configurationUtil.getFrpGlobalParm(ADDON_VALUES_CONTEXT,MAX_VASE_DISPLAY_COUNT_NAME);
                if (maxDisplayCountString != null)
                {
                    Integer maxDisplayCount = Integer.parseInt(maxDisplayCountString);
                    if (vaseAddOnVOList.size() > maxDisplayCount)
                    {
                        ArrayList <AddOnVO> trimmedVaseAddOnVOList = new ArrayList <AddOnVO> ();
                        for (int i = 0; i < maxDisplayCount; i++)
                        {
                            trimmedVaseAddOnVOList.add(vaseAddOnVOList.get(i));
                        }
                        vaseAddOnVOList = trimmedVaseAddOnVOList;
                    }
                }
                else
                {
                    logger.error("Missing max display count global parm for add-ons.  Context: " + ADDON_VALUES_CONTEXT +
                                 " Value: " + MAX_VASE_DISPLAY_COUNT_NAME);
                }                
            }        
            addonMap.put(AddOnVO.ADD_ON_VO_ADD_ON_KEY,addOnVOList);
            addonMap.put(AddOnVO.ADD_ON_VO_VASE_KEY,vaseAddOnVOList);
            addonMap.put(AddOnVO.ADD_ON_VO_BANNER_KEY,bannerAddOnVOList);
            addonMap.put(AddOnVO.ADD_ON_VO_CARD_KEY,cardAddOnVOList);
        }
        return addonMap; 
    }
    
    
    /**
     * Returns the list of add-ons that apply to the given filter
     * @param productId The product Id to filter on
     * @param occasionId The occasion Id to filter on
     * @param sourceCode The source code to filter on
     * @param sourceCode Delivery type ( florist / dropship ) to filter on
     * @param returnAllAddOnFlag True if all add-ons are desired, false if the list should be trimed to the 'ADDON_VALUES', 'MAX_DISPLAY_COUNT' global parm amount
     * @param conn Database connection
     * @return Map of add-ons that fit the search criteria
     * @throws Exception
     */
   
   public HashMap <String, ArrayList <AddOnVO>> getActiveAddonListByProductIdAndOccasionAndSourceCodeAndDeliveryType
                               (String productId, Integer occasionId, String sourceCode, Boolean returnAllAddOnFlag, String floristDelivery, String dropshipDelivery, Connection  conn) throws Exception
   {
       if (returnAllAddOnFlag == null)
       {
           returnAllAddOnFlag = true;
       }
       
       HashMap inputParams = new HashMap();
       DataRequest dataRequest = new DataRequest();

       dataRequest.setConnection(conn);
       dataRequest.setStatementID(GET_ADDON_CUR_BY_PROD_OCCA_DTP);
       inputParams.put(IN_PRODUCT_ID, productId);
       inputParams.put(IN_OCCASION_ID, occasionId);
       inputParams.put(IN_SOURCE_CODE, sourceCode);
       inputParams.put(IN_FLORIST_AVAILABLE, floristDelivery);
       inputParams.put(IN_DROPSHIP_AVAILABLE, dropshipDelivery);
       

       dataRequest.setInputParams(inputParams);

       DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
       CachedResultSet rs = null;
       rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);

       HashMap <String, ArrayList<AddOnVO>> addonMap = new HashMap <String, ArrayList<AddOnVO>>();
       ArrayList <AddOnVO> addOnVOList = new ArrayList<AddOnVO>();
       ArrayList <AddOnVO> vaseAddOnVOList = new ArrayList<AddOnVO>();
       ArrayList <AddOnVO> bannerAddOnVOList = new ArrayList<AddOnVO>();
       ArrayList <AddOnVO> cardAddOnVOList = new ArrayList<AddOnVO>();

       if( rs!=null ) 
       {
           while( rs.next() ) 
           {
               AddOnVO addOnVO =  getAddonFromResultSet(rs);
               if (StringUtils.equals(addOnVO.getAddOnTypeId(),String.valueOf(AddOnVO.ADD_ON_TYPE_VASE_ID)))
               {
                   vaseAddOnVOList.add(addOnVO);
               }
               else if (StringUtils.equals(addOnVO.getAddOnTypeId(),String.valueOf(AddOnVO.ADD_ON_TYPE_BANNER_ID)))
               {
                   bannerAddOnVOList.add(addOnVO);
               }
               else if (StringUtils.equals(addOnVO.getAddOnTypeId(),String.valueOf(AddOnVO.ADD_ON_TYPE_CARD_ID)))
               {	
            	   if (floristDelivery.equalsIgnoreCase("Y"))
            	   {
            	   cardAddOnVOList.add(addOnVO);
            	   }
               }
               else
               {
                   addOnVOList.add(addOnVO);
               }
           }
           if (!returnAllAddOnFlag)
           {
               ConfigurationUtil configurationUtil = new ConfigurationUtil();
               String maxDisplayCountString = configurationUtil.getFrpGlobalParm(ADDON_VALUES_CONTEXT,MAX_DISPLAY_COUNT_NAME);
               if (maxDisplayCountString != null)
               {
                   Integer maxDisplayCount = Integer.parseInt(maxDisplayCountString);
                   if (cardAddOnVOList.size() > 0)
                   {
                       maxDisplayCount--;
                   }
   
                   if (addOnVOList.size() > maxDisplayCount)
                   {
                       ArrayList <AddOnVO> trimmedAddOnVOList = new ArrayList <AddOnVO> ();
                       for (int i = 0; i < maxDisplayCount; i++)
                       {
                           trimmedAddOnVOList.add(addOnVOList.get(i));
                       }
                       addOnVOList = trimmedAddOnVOList;
                   }
               }
               else
               {
                   logger.error("Missing max display count global parm for add-ons.  Context: " + ADDON_VALUES_CONTEXT +
                                " Value: " + MAX_DISPLAY_COUNT_NAME);
               }
               
               maxDisplayCountString = configurationUtil.getFrpGlobalParm(ADDON_VALUES_CONTEXT,MAX_VASE_DISPLAY_COUNT_NAME);
               if (maxDisplayCountString != null)
               {
                   Integer maxDisplayCount = Integer.parseInt(maxDisplayCountString);
                   if (vaseAddOnVOList.size() > maxDisplayCount)
                   {
                       ArrayList <AddOnVO> trimmedVaseAddOnVOList = new ArrayList <AddOnVO> ();
                       for (int i = 0; i < maxDisplayCount; i++)
                       {
                           trimmedVaseAddOnVOList.add(vaseAddOnVOList.get(i));
                       }
                       vaseAddOnVOList = trimmedVaseAddOnVOList;
                   }
               }
               else
               {
                   logger.error("Missing max display count global parm for add-ons.  Context: " + ADDON_VALUES_CONTEXT +
                                " Value: " + MAX_VASE_DISPLAY_COUNT_NAME);
               }                
           }        
           addonMap.put(AddOnVO.ADD_ON_VO_ADD_ON_KEY,addOnVOList);
           addonMap.put(AddOnVO.ADD_ON_VO_VASE_KEY,vaseAddOnVOList);
           addonMap.put(AddOnVO.ADD_ON_VO_BANNER_KEY,bannerAddOnVOList);
           addonMap.put(AddOnVO.ADD_ON_VO_CARD_KEY,cardAddOnVOList);
       }
       return addonMap; 
   }
   
    
    /**
     * Returns list of the assigned addons for a vendor
     * @param vendorId The vendor id to search by
     * @param conn Database connection
     * @return A map of all of the add-ons
     * @throws Exception
     */
    
    public HashMap <String, ArrayList <AddOnVO>> getAssignedAddonListByVendor (String  vendorId, Connection conn) throws Exception
    {
        HashMap inputParams = new HashMap();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID(GET_ATV_ADDON_CUR_BY_VENDOR_ID);
        inputParams.put(IN_VENDOR_ID, vendorId);

        dataRequest.setInputParams(inputParams);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        CachedResultSet rs = null;
        rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);

        HashMap <String, ArrayList<AddOnVO>> addonMap = new HashMap <String, ArrayList<AddOnVO>>();
        HashMap <String,VendorAddOnVO> vendorAddOnVOMap = null;
        
        ArrayList <AddOnVO> addOnVOList = new ArrayList<AddOnVO>();
        ArrayList <AddOnVO> vaseAddOnVOList = new ArrayList<AddOnVO>();
        ArrayList <AddOnVO> bannerAddOnVOList = new ArrayList<AddOnVO>();
        ArrayList <AddOnVO> cardAddOnVOList = new ArrayList<AddOnVO>();

        if( rs!=null ) 
        {
            while( rs.next() ) 
            {
                AddOnVO addOnVO =  getAddonFromResultSet(rs);
                
                vendorAddOnVOMap = new HashMap <String,VendorAddOnVO>();
                VendorAddOnVO vendorAddOnVO =  getVendorFromResultSet(rs);
                vendorAddOnVOMap.put(vendorAddOnVO.getVendorId(),vendorAddOnVO);
                addOnVO.setVendorCostsMap(vendorAddOnVOMap);
                    
                if (StringUtils.equals(addOnVO.getAddOnTypeId(),String.valueOf(AddOnVO.ADD_ON_TYPE_VASE_ID)))
                {
                    vaseAddOnVOList.add(addOnVO);
                }
                else if (StringUtils.equals(addOnVO.getAddOnTypeId(),String.valueOf(AddOnVO.ADD_ON_TYPE_BANNER_ID)))
                {
                    bannerAddOnVOList.add(addOnVO);
                }
                else if (StringUtils.equals(addOnVO.getAddOnTypeId(),String.valueOf(AddOnVO.ADD_ON_TYPE_CARD_ID)))
                {
                    cardAddOnVOList.add(addOnVO);
                }
                else
                {
                    addOnVOList.add(addOnVO);
                }
            }
            addonMap.put(AddOnVO.ADD_ON_VO_ADD_ON_KEY,addOnVOList);
            addonMap.put(AddOnVO.ADD_ON_VO_VASE_KEY,vaseAddOnVOList);
            addonMap.put(AddOnVO.ADD_ON_VO_BANNER_KEY,bannerAddOnVOList);
            addonMap.put(AddOnVO.ADD_ON_VO_CARD_KEY,cardAddOnVOList);
        }
        return addonMap; 
    }
    
    /**
     * Returns all of the addons a vendor could provide
     * @param vendorId The vendor id to search by
     * @param conn Database connection
     * @return A map of all of the add-ons
     * @throws Exception
     */
    
    public HashMap <String, ArrayList <AddOnVO>> getAllAddonListByVendor (String  vendorId, Connection conn) throws Exception
    {
        HashMap inputParams = new HashMap();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID(GET_ALL_ADDON_CUR_BY_VENDOR_ID);
        inputParams.put(IN_VENDOR_ID, vendorId);

        dataRequest.setInputParams(inputParams);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        CachedResultSet rs = null;
        rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);

        HashMap <String, ArrayList<AddOnVO>> addonMap = new HashMap <String, ArrayList<AddOnVO>>();
        HashMap <String,VendorAddOnVO> vendorAddOnVOMap = null;
        
        ArrayList <AddOnVO> addOnVOList = new ArrayList<AddOnVO>();
        ArrayList <AddOnVO> vaseAddOnVOList = new ArrayList<AddOnVO>();
        ArrayList <AddOnVO> bannerAddOnVOList = new ArrayList<AddOnVO>();
        ArrayList <AddOnVO> cardAddOnVOList = new ArrayList<AddOnVO>();

        if( rs!=null ) 
        {
            while( rs.next() ) 
            {
                AddOnVO addOnVO =  getAddonFromResultSet(rs);              
                vendorAddOnVOMap = new HashMap <String,VendorAddOnVO>();
                VendorAddOnVO vendorAddOnVO =  getVendorFromResultSet(rs);
                vendorAddOnVOMap.put(vendorAddOnVO.getVendorId(),vendorAddOnVO);
                addOnVO.setVendorCostsMap(vendorAddOnVOMap);

                if (StringUtils.equals(addOnVO.getAddOnTypeId(),String.valueOf(AddOnVO.ADD_ON_TYPE_VASE_ID)))
                {
                    vaseAddOnVOList.add(addOnVO);
                }
                else if (StringUtils.equals(addOnVO.getAddOnTypeId(),String.valueOf(AddOnVO.ADD_ON_TYPE_BANNER_ID)))
                {
                    bannerAddOnVOList.add(addOnVO);
                }
                else if (StringUtils.equals(addOnVO.getAddOnTypeId(),String.valueOf(AddOnVO.ADD_ON_TYPE_CARD_ID)))
                {
                    cardAddOnVOList.add(addOnVO);
                }
                else
                {
                    addOnVOList.add(addOnVO);
                }
            }
            addonMap.put(AddOnVO.ADD_ON_VO_ADD_ON_KEY,addOnVOList);
            addonMap.put(AddOnVO.ADD_ON_VO_VASE_KEY,vaseAddOnVOList);
            addonMap.put(AddOnVO.ADD_ON_VO_BANNER_KEY,bannerAddOnVOList);
            addonMap.put(AddOnVO.ADD_ON_VO_CARD_KEY,cardAddOnVOList);
        }
        return addonMap; 
    }
    
    /**
     * Performs availability check for a vendor that can fill both the addons and product
     * @param productId The product Id to search for
     * @param addOnList ArrayList of add-on ids to search for
     * @param conn Database connection
     * @return ArrayList of strings containing the vendor ids that can fulfill the order
     * @throws Exception
     */
    
    public ArrayList <String> getVendorListByAddonListAndProduct (String productId, ArrayList <String> addOnList, Connection conn) throws Exception
    {
        
        StringBuffer addOnString = new StringBuffer();
        
        for (int i = 0; i < addOnList.size(); i++)
        {
            addOnString.append("\'" + addOnList.get(i) + "\'");
            if (i != addOnList.size() - 1 )
            {
                addOnString.append(",");
            }
        }
        
        ArrayList <String> vendorList = new ArrayList <String>();
        HashMap inputParams = new HashMap();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID(GET_VENDOR_BY_ADDON_PRODUCT);
        inputParams.put(IN_PRODUCT_ID, productId);
        inputParams.put(IN_ADD_ON_LIST, addOnString.toString());
        inputParams.put(IN_ADD_ON_COUNT, addOnList.size());

        dataRequest.setInputParams(inputParams);
 
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        CachedResultSet rs = null;
        rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);
        if (rs != null)
        {
            while (rs.next())
            {
                vendorList.add(rs.getString(VENDOR_ID));
            }
        }
        
        return vendorList;
    }

    /**
     * Will return all of the add-ons associated with a order detail Id
     * 
     * @param orderDetailId
     * @param returnVendorInformationFlag true if the caller wants the vendor related information such as SKU and vendor cost
     * @param conn Database connection
     * @return The list of add-ons associated with the order
     * @throws Exception
     */
    
    public ArrayList <AddOnVO> getOrderAddonListByDetailId (String orderDetailId, Boolean returnVendorInformationFlag, Connection conn) throws Exception
    {
        if (returnVendorInformationFlag == null)
        {
            returnVendorInformationFlag = false;
        }
        HashMap inputParams = new HashMap();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID(GET_ADDON_CUR_BY_DETAIL_ID);
        inputParams.put(IN_ORDER_DETAIL_ID, orderDetailId);
        inputParams.put(IN_VENDOR_INFO_FLAG, returnVendorInformationFlag.toString());

        dataRequest.setInputParams(inputParams);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        CachedResultSet addOnSet = null;
        CachedResultSet vendorSet = null;
        
        Map result = (Map)dataAccessUtil.execute(dataRequest);

        if(result != null)
        {
          addOnSet = (CachedResultSet)result.get(OUT_CUR);
          vendorSet = (CachedResultSet)result.get(OUT_VENDOR_CUR);
        }
        ArrayList <AddOnVO> addOnVOList = new ArrayList<AddOnVO>();
        if(addOnSet!=null) 
        {
            while( addOnSet.next() ) 
            {
                AddOnVO addOnVO =  getAddonFromResultSet(addOnSet);
                addOnVO.setOrderQuantity(new Integer(addOnSet.getString(ADD_ON_QUANTITY)));
                addOnVOList.add(addOnVO);
            }
        }
        
        HashMap <String,VendorAddOnVO> vendorAddOnVOMap = new HashMap <String,VendorAddOnVO>();
        if(vendorSet!=null) 
        {
            String currentAddOn = null;
            while( vendorSet.next() ) 
            {
                VendorAddOnVO vendorAddOnVO =  getVendorFromResultSet(vendorSet);
                if (StringUtils.equals(currentAddOn,vendorAddOnVO.getAddOnId()) || currentAddOn == null)
                {
                    vendorAddOnVOMap.put(vendorAddOnVO.getVendorId(),vendorAddOnVO);
                }
                else
                {
                    for (AddOnVO addOnVO : addOnVOList)
                    {
                        if (StringUtils.equals(addOnVO.getAddOnId(),vendorAddOnVO.getAddOnId()))
                        {
                            addOnVO.setVendorCostsMap(vendorAddOnVOMap);
                            break;
                        }
                    }              
                    vendorAddOnVOMap = new HashMap <String,VendorAddOnVO>();
                    vendorAddOnVOMap.put(vendorAddOnVO.getVendorId(),vendorAddOnVO);
                }            
            }
        }

        return addOnVOList;    
    }
    
    /**
     * Will return all of the add-ons associated with a venus Id
     * @param venusId The venus Id of the order
     * @param returnVendorInformationFlag true if the caller wants the vendor related information such as SKU and vendor cost
     * @param conn Database connection
     * @return The list of add-ons associated with the order
     * @throws Exception
     */
    
    public ArrayList <AddOnVO> getOrderAddonListByVenusId (String venusId, Boolean returnVendorInformationFlag, Connection conn) throws Exception
    {
        if (returnVendorInformationFlag == null)
        {
            returnVendorInformationFlag = false;
        }
        
        HashMap inputParams = new HashMap();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID(GET_ADDON_CUR_BY_VENUS_ID);
        inputParams.put(IN_VENUS_ID, venusId);
        inputParams.put(IN_VENDOR_INFO_FLAG, returnVendorInformationFlag.toString());

        dataRequest.setInputParams(inputParams);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        CachedResultSet addOnSet = null;
        CachedResultSet vendorSet = null;
        
        Map result = (Map)dataAccessUtil.execute(dataRequest);

        if(result != null)
        {
          addOnSet = (CachedResultSet)result.get(OUT_CUR);
          vendorSet = (CachedResultSet)result.get(OUT_VENDOR_CUR);
        }
        ArrayList <AddOnVO> addOnVOList = new ArrayList<AddOnVO>();
        if(addOnSet!=null) 
        {
            while( addOnSet.next() ) 
            {
                AddOnVO addOnVO =  getAddonFromResultSet(addOnSet);
                addOnVO.setOrderQuantity(new Integer(addOnSet.getString(ADD_ON_QUANTITY)));
                if (returnVendorInformationFlag)
                {
                    HashMap <String,VendorAddOnVO> vendorAddOnVOMap = new HashMap <String,VendorAddOnVO>();
                    
                    if(vendorSet!=null) 
                    {
                        String currentAddOn = addOnVO.getAddOnId();
                        while( vendorSet.next() ) 
                        {
                            VendorAddOnVO vendorAddOnVO =  getVendorFromResultSet(vendorSet);
                            if (StringUtils.equals(currentAddOn,vendorAddOnVO.getAddOnId()) )
                            {
                                vendorAddOnVOMap.put(vendorAddOnVO.getVendorId(),vendorAddOnVO);
                            }
                        }  
                    }
                    addOnVO.setVendorCostsMap(vendorAddOnVOMap);
                }
                vendorSet.reset();
                addOnVOList.add(addOnVO);
            }
        }
        
        return addOnVOList;     
    }
    
    /**
     * Returns all of the information for a single add-on
     * @param returnVendorInformationFlag true if the caller wants the vendor related information such as SKU and vendor cost
     * @param conn Database connection
     * @return Populated AddOnVO
     * @throws Exception
     */
    
    public AddOnVO getAddOnById(String addOnId, Boolean returnVendorInformationFlag,  Boolean returnProductInformationFlag, Connection conn) throws Exception
    {
        if (returnVendorInformationFlag == null)
        {
            returnVendorInformationFlag = false;
        }
        if (returnProductInformationFlag == null)
        {
            returnProductInformationFlag = false;
        }
        HashMap inputParams = new HashMap();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID(GET_ADDON_BY_ADDON_ID);
        inputParams.put(IN_ADD_ON_ID, addOnId);
        inputParams.put(IN_VENDOR_INFO_FLAG, returnVendorInformationFlag.toString());
        inputParams.put(IN_PRODUCT_INFO_FLAG, returnProductInformationFlag.toString());
        dataRequest.setInputParams(inputParams);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        CachedResultSet addOnSet = null;
        CachedResultSet vendorSet = null;
        CachedResultSet productSet = null;
        
        Map result = (Map)dataAccessUtil.execute(dataRequest);
        
        
        if(result != null)
        {
            addOnSet = (CachedResultSet)result.get(OUT_CUR);
            vendorSet = (CachedResultSet)result.get(OUT_VENDOR_CUR);
            productSet = (CachedResultSet)result.get(OUT_PRODUCT_CUR);
        }
        
        AddOnVO addOnVO = null;

        if (addOnSet != null)
        {
            while(addOnSet.next())
            {
                addOnVO = getAddonFromResultSet(addOnSet);
            }
        }
         
        HashMap <String,VendorAddOnVO> vendorAddOnVOMap = new HashMap <String,VendorAddOnVO>();

        if(vendorSet != null && addOnVO != null) 
        {
            while (vendorSet.next())
            {
                VendorAddOnVO vendorAddOnVO = getVendorFromResultSet(vendorSet);
                vendorAddOnVOMap.put(vendorAddOnVO.getVendorId(),vendorAddOnVO);
            }
            addOnVO.setVendorCostsMap(vendorAddOnVOMap);
            
        }
        
        HashSet <String> productIdSet = new HashSet <String> ();

        if(productSet != null && addOnVO != null)
        {
            while (productSet.next())
            {
                productIdSet.add(productSet.getString(PRODUCT_ID));
            }
            addOnVO.setProductIdSet(productIdSet);
        }
        return addOnVO;
    }
    
    /**
     * Returns all of the information for all add-ons
     * @param returnVendorInformationFlag true if the caller wants the vendor related information such as SKU and vendor cost
     * @param conn Database connection
     * @return List of add-ons
     * @throws Exception
     */
        
    public HashMap <String, ArrayList <AddOnVO>> getAllAddonList (Boolean returnVendorInformationFlag, Boolean oneAddOnList, Connection conn) throws Exception
    {
        if (returnVendorInformationFlag == null)
        {
            returnVendorInformationFlag = false;
        }
        if (oneAddOnList == null)
        {
            oneAddOnList = false;
        }
        HashMap inputParams = new HashMap();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID(GET_ALL_ADDON_CUR);
        inputParams.put(IN_VENDOR_INFO_FLAG, returnVendorInformationFlag.toString());
        dataRequest.setInputParams(inputParams);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

        CachedResultSet addOnSet = null;
        CachedResultSet vendorSet = null;

        Map result = (Map)dataAccessUtil.execute(dataRequest);

                
        HashMap <String, ArrayList<AddOnVO>> addonMap = new HashMap <String, ArrayList<AddOnVO>>();
        ArrayList <AddOnVO> addOnVOList = new ArrayList<AddOnVO>();
        ArrayList <AddOnVO> vaseAddOnVOList = new ArrayList<AddOnVO>();
        ArrayList <AddOnVO> bannerAddOnVOList = new ArrayList<AddOnVO>();
        ArrayList <AddOnVO> cardAddOnVOList = new ArrayList<AddOnVO>();
        
        if(result != null)
        {
          addOnSet = (CachedResultSet)result.get(OUT_CUR);
          vendorSet = (CachedResultSet)result.get(OUT_VENDOR_CUR);
        }

        if(addOnSet!=null) 
        {
            while( addOnSet.next() ) 
            {
                AddOnVO addOnVO =  getAddonFromResultSet(addOnSet);
                if (oneAddOnList)
                {
                    addOnVOList.add(addOnVO);
                }
                else
                {
                    if (StringUtils.equals(addOnVO.getAddOnTypeId(),String.valueOf(AddOnVO.ADD_ON_TYPE_VASE_ID)))
                    {
                        vaseAddOnVOList.add(addOnVO);
                    }
                    else if (StringUtils.equals(addOnVO.getAddOnTypeId(),String.valueOf(AddOnVO.ADD_ON_TYPE_BANNER_ID)))
                    {
                        bannerAddOnVOList.add(addOnVO);
                    }
                    else if (StringUtils.equals(addOnVO.getAddOnTypeId(),String.valueOf(AddOnVO.ADD_ON_TYPE_CARD_ID)))
                    {
                        cardAddOnVOList.add(addOnVO);
                    }
                    else
                    {
                        addOnVOList.add(addOnVO);
                    }
                }
            }
            if (oneAddOnList)
            {
                addonMap.put(AddOnVO.ADD_ON_VO_ADD_ON_KEY,addOnVOList);
            }
            else
            {
                addonMap.put(AddOnVO.ADD_ON_VO_ADD_ON_KEY,addOnVOList);
                addonMap.put(AddOnVO.ADD_ON_VO_VASE_KEY,vaseAddOnVOList);
                addonMap.put(AddOnVO.ADD_ON_VO_BANNER_KEY,bannerAddOnVOList);
                addonMap.put(AddOnVO.ADD_ON_VO_CARD_KEY,cardAddOnVOList);              
            }
        }
        
        HashMap <String,VendorAddOnVO> vendorAddOnVOMap = new HashMap <String,VendorAddOnVO>();
        if(vendorSet!=null) 
        {
            String currentAddOn = null;
            while( vendorSet.next() ) 
            {
                VendorAddOnVO vendorAddOnVO =  getVendorFromResultSet(vendorSet);
                if (StringUtils.equals(currentAddOn,vendorAddOnVO.getAddOnId()) || currentAddOn == null)
                {
                    vendorAddOnVOMap.put(vendorAddOnVO.getVendorId(),vendorAddOnVO);
                }
                else
                {
                    for (AddOnVO addOnVO : addOnVOList)
                    {
                        if (StringUtils.equals(addOnVO.getAddOnId(),vendorAddOnVO.getAddOnId()))
                        {
                            addOnVO.setVendorCostsMap(vendorAddOnVOMap);
                            break;
                        }
                    }              
                    vendorAddOnVOMap = new HashMap <String,VendorAddOnVO>();
                    vendorAddOnVOMap.put(vendorAddOnVO.getVendorId(),vendorAddOnVO);
                }            
            }
        }      
        return addonMap; 
    }
    
    /**
     * Private method that will obtain add-on information from a result set
     * @param queryOutput CachedResultSet that the information will be pulled from
     * @return AddOnVO that is populated with new information
     */
    
    
    /* #552 - Flexible Fullfillment - Addon Dash board and Maintanance changes 
    Set the delivery type in addonVo from the resultset.
    */
    
    private AddOnVO getAddonFromResultSet(CachedResultSet queryOutput)
    {
        AddOnVO addOnVO = new AddOnVO();

        addOnVO.setAddOnId(queryOutput.getString(ADDON_ID));
        addOnVO.setAddOnTypeId(queryOutput.getString(ADDON_TYPE));
        addOnVO.setAddOnText(queryOutput.getString(ADDON_TEXT));
        addOnVO.setAddOnUNSPSC(queryOutput.getString(UNSPSC));
        addOnVO.setAddOnDescription(queryOutput.getString(ADD_ON_DESCRIPTION));
        addOnVO.setAddOnPrice(queryOutput.getString(PRICE));
        addOnVO.setAddOnWeight(queryOutput.getString(ADDON_WEIGHT));
        addOnVO.setAddOnAvailableFlag(StringUtils.equals("Y",queryOutput.getString(ADD_ON_ACTIVE_FLAG)));
        addOnVO.setDefaultPerTypeFlag(StringUtils.equals("Y",queryOutput.getString(DEFAULT_PER_TYPE_FLAG)));
        addOnVO.setDisplayPriceFlag(StringUtils.equals("Y",queryOutput.getString(DISPLAY_PRICE_FLAG)));
        addOnVO.setAddOnTypeDescription(queryOutput.getString(ADD_ON_TYPE_DESCRIPTION));
        addOnVO.setDisplaySequenceNumber(queryOutput.getString(DISPLAY_SEQ_NUMBER));
        addOnVO.setAddOnTypeIncludedInProductFeedFlag(StringUtils.equals("Y",queryOutput.getString(PRODUCT_FEED_FLAG)));
        addOnVO.setProductId(queryOutput.getString(PRODUCT_ID));
        addOnVO.setMaxQuantity(queryOutput.getString(MAX_QTY));
        addOnVO.setOccasionId(queryOutput.getString(OCCASION_ID));
        addOnVO.setOccasionDescription(queryOutput.getString(OCCASION_DESCRIPTION));
        addOnVO.setAddOnDeliveryType(queryOutput.getString(ADDON_DELIVERY_TYPE));
        addOnVO.setsLinkedOccasions(queryOutput.getString(LINKED_OCCASIONS));// Changes for Apollo.Q3.2015 - Addon-Occasion association
        /*
         * DI-4: Add-on Dashboard Enhancements: Added 2 new attributes to add-on : PQUAD_ACCESSORY_ID, FTD_WEST_ADDON.
         * The attributes are retrieved from DB and displayed on Add / Edit Add-on page.
         */
        addOnVO.setsPquadAccsryId(queryOutput.getString(PQUAD_ACCESSORY_ID)); 
        addOnVO.setFtdWestAddon(StringUtils.equals("Y",queryOutput.getString(IS_FTD_WEST_ADDON)));
        
        return addOnVO;
    }
    
    /**
     * Private method that will obtain vendor information from a result set
     * @param queryOutput CachedResultSet that the information will be pulled from
     * @return VendorAddOnVO that is populated with the inforamtion that was stored in the queryOutput
     * @throws Exception
     */
    
    private VendorAddOnVO getVendorFromResultSet(CachedResultSet queryOutput) throws Exception
    {
        VendorAddOnVO vendorAddOnVO = new VendorAddOnVO();
        vendorAddOnVO.setVendorId(queryOutput.getString(VENDOR_ID));
        vendorAddOnVO.setAddOnId(queryOutput.getString(ADDON_ID));
        vendorAddOnVO.setAssignedFlag(StringUtils.equals("Y",queryOutput.getString(VENDOR_ACTIVE_FLAG)));
        vendorAddOnVO.setSKU(queryOutput.getString(VENDOR_SKU));
        vendorAddOnVO.setCost(queryOutput.getString(VENDOR_COST));

        return vendorAddOnVO;
    }
    
    /**
     * This method will convert an add-on representation into XML
     * @param addOnMap  A map containing the add-on vos to convert to XML
     * @return The XML representation of the object
     * @throws Exception
     */
    
    public Document convertAddOnMapToXML(HashMap <String, ArrayList <AddOnVO>> addOnMap)  throws Exception
    {
        //convert the requestOrderDetailVO in an XML format
    	Document parser;
        Document addOnXML = DOMUtil.getDefaultDocument();
    
        //generate the top node
        Element stateElement = addOnXML.createElement(ADD_ON_XML_TOP_TAG);
        //and append it
        addOnXML.appendChild(stateElement);
        
        if (addOnMap != null) 
        {
            for (String key : addOnMap.keySet())
            {
                Element addOnTypeElement = addOnXML.createElement(key);
                stateElement.appendChild(addOnTypeElement);

                for (int i = 0; i< addOnMap.get(key).size(); i++)
                {
                    AddOnVO addOnVO = addOnMap.get(key).get(i);
                    parser = DOMUtil.getDocument(addOnVO.toXML(i + 1));
                    addOnTypeElement.appendChild(addOnXML.importNode(parser.getFirstChild(), true));                
                } 
            }
        }      
        return addOnXML;
    }
    
    public boolean orderAddOnAvailable(String orderDetailID, Connection conn) throws Exception
    {
       boolean orderAddOnAvailable = true;
       DataRequest dataRequest = new DataRequest();
       dataRequest.setConnection(conn);
       dataRequest.setStatementID(ORDER_ADD_ON_AVAILABILITY);
       dataRequest.addInputParam("IN_ORDER_DETAIL_ID", orderDetailID);
      
       DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
       String searchResults = (String) dataAccessUtil.execute(dataRequest);
       
       if (searchResults.equalsIgnoreCase("N"))
       {
           orderAddOnAvailable = false;
       }
       return orderAddOnAvailable;
     
    }
    
    public boolean vendorAddOnAvailable(String venusId, String vendorId, Connection conn) throws Exception
    {
       boolean vendorAddOnAvailable = true;
       DataRequest dataRequest = new DataRequest();
       dataRequest.setConnection(conn);
       dataRequest.setStatementID(VENDOR_ADD_ON_AVAILABILITY);
       dataRequest.addInputParam("IN_VENUS_ID", venusId);
       dataRequest.addInputParam("IN_VENDOR_ID", vendorId);
   
       DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
       String searchResults = (String) dataAccessUtil.execute(dataRequest);
       
       if (searchResults.equalsIgnoreCase("N"))
       {
           vendorAddOnAvailable = false;
       }
       return vendorAddOnAvailable;
     
    }
    
    /**
     * This method will verify that an add-on is the only one of that add-on's type assigned to any product to which
     * the add-on is assigned
     * @param addOnId
     * @param conn
     * @return true if there are products in this state
     * @throws Exception
     */
    
    public Boolean isOnlyOfTypeAssigned(String addOnId, Connection conn) throws Exception
    {
        Boolean returnValue = false;
        HashMap inputParams = new HashMap();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID(GET_IS_ONLY_OF_TYPE_ASSIGNED);
        inputParams.put(IN_ADD_ON_ID, addOnId);
        dataRequest.setInputParams(inputParams);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        CachedResultSet productIdSet = (CachedResultSet) dataAccessUtil.execute(dataRequest);

        if (productIdSet != null)
        {
            if (productIdSet.next())
            {
                returnValue = true;
            }
        }
        return returnValue;
    }
    
    /**
     * This method will verify that an add-on is assigned to one and only one vendor and it is the vendor that is passed
     * that is assigned
     * @param vendorId
     * @param addOnId
     * @param conn
     * @return true if there are products in this state
     * @throws Exception
     */
    
    public Boolean isLastVendorAssignedToAddOn(String vendorId, String addOnId, Connection conn) throws Exception
    {
        Boolean returnValue = false;
        HashMap inputParams = new HashMap();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID(GET_IS_LAST_VENDOR_ASSIGNED);
        inputParams.put(IN_VENDOR_ID, vendorId);
        inputParams.put(IN_ADD_ON_ID, addOnId);
        dataRequest.setInputParams(inputParams);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        CachedResultSet vendorIdSet = (CachedResultSet) dataAccessUtil.execute(dataRequest);

        if (vendorIdSet != null)
        {
            if (vendorIdSet.next())
            {
                returnValue = true;
            }
        }
        return returnValue;
    }
}
