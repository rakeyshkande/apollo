package com.ftd.osp.utilities.QMS;

import qms.addressbroker.client.AddressBrokerException;
import qms.addressbroker.client.QMSAddressBroker;
import qms.addressbroker.client.QMSAddressBrokerFactory;
import com.ftd.osp.utilities.plugins.*;
import java.io.*;

public class QMSAddressBrokerWrapper
{
    private final static String QMS_INIT_LIST = "INIT_LIST";
    private final static String QMS_INPUTFIELDLIST = "INPUTFIELDLIST";
    private final static String QMS_OUTPUTFIELDLIST = "OUTPUTFIELDLIST";
    private final static String QMS_INPUT_MODE = "Input_Mode";
    private final static String QMS_KEEP_MULTIMATCH = "Keep_multimatch";
    private final static String QMS_BUFFER_RADIUS = "BUFFER RADIUS";
    private static Logger logger = new Logger("com.ftd.applications.osp.qmsconnectionpool.QMSAddressBrokerWrapper");

    private QMSAddressBroker broker;
    private boolean destroy;

    public QMSAddressBrokerWrapper(String serverString, String transport, boolean destroyAfterUse) throws Exception
    {
        destroy = destroyAfterUse;

        broker = QMSAddressBrokerFactory.make(serverString, transport, null, null);

        broker.setProperty(QMS_INIT_LIST, "Geostan|GeostanZ9");
        broker.setProperty(QMS_INPUTFIELDLIST, "firmname|addressline|lastline");
        broker.setProperty(QMS_OUTPUTFIELDLIST, "firmname|addressline|city|state|zip10|latitude|longitude|uspsrangerecordType|lowendhousenumber|highendhousenumber|lowunitnumber|highunitnumber|HouseNumber|UnitNumber|match_code");
        broker.setProperty(QMS_INPUT_MODE, "0");
        broker.setProperty(QMS_KEEP_MULTIMATCH,"true");
        broker.setProperty(QMS_BUFFER_RADIUS,"200");

        broker.validateProperties();
    }

    void close()
    {
        if(destroy)
        {
            try
            {
                broker.close();
            }
            catch(Exception e)
            {
                StringWriter sw = new StringWriter();
                e.printStackTrace(new PrintWriter(sw));
                logger.error(sw.toString());
            }
        }
    }

    public String getField(String name)
    {
        String field = null;
        try
        {
            field = broker.getField(name);
        }
        catch(Exception e)
        {            
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            logger.error(sw.toString());
        }
        
        return field; 
    }

    public void setRecord()
    {    
        try
        {
            broker.setRecord();
        }
        catch(Exception e)
        {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            logger.error(sw.toString());
        }            
    }

    public void processRecords() throws Exception
    {
        try
        {
            broker.processRecords();
        }
        catch(Exception e)
        {
            // We need to destroy this connection
            destroy = true;
            throw e;
        }
    }

    public void clear()
    {
        boolean ret = false;
        try
        {
            ret = broker.clear();

            if(ret == false)
            {
                throw new Exception();
            }
        }
        catch(Exception e)
        {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            logger.error(sw.toString());
        }
    }

    public void getRecord()
    {
        try
        {
            broker.getRecord();
        }
        catch(Exception e)
        {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            logger.error(sw.toString());
        }
    }

    public void setField(String name, String value)
    {
        try
        {
            broker.setField(name, value);
        }
        catch(Exception e)
        {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            logger.error(sw.toString());
        }
    }

    public boolean isDestroy()
    {
        return destroy;
    }

    public void setDestroy(boolean newDestroy)
    {
        destroy = newDestroy;
    }
}