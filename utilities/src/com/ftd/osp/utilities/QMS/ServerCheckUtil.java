package com.ftd.osp.utilities.QMS;
import java.net.*;

public class ServerCheckUtil 
{
    public ServerCheckUtil()
    {
    }

    /**
     * Checks a server port for connectivity
     * @author Jeff Penney
     *
     */
    public static boolean checkServerPort(String serverName, int serverPort)
    {
        boolean ret = true;
        try
        {
            Socket socket = new Socket(serverName, serverPort);

            socket.close();
        }
        catch(Exception e)
        {
            ret = false;
        }

        return ret;
    }
}