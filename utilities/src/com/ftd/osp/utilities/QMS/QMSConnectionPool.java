package com.ftd.osp.utilities.QMS;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.ConfigurationUtil;

import java.io.*;
import java.util.*;
import javax.xml.transform.*;
import org.xml.sax.*;
import javax.xml.parsers.*;

/**
 * This class implements a singleton pool of QMS Address Broker objects.
 *
 * @author Jeff Penney
 *
 */
public class QMSConnectionPool extends Thread
{
    private final static String QMS_CONTEXT = "QMS_CONFIG";
    private final static String QMS_SERVERNAME_1 = "qmsServerName1";
    private final static String QMS_SERVERPORT_1 = "qmsServerPort1";
    private final static String QMS_SERVERNAME_2 = "qmsServerName2";
    private final static String QMS_SERVERPORT_2 = "qmsServerPort2";

    private int maxConnections;
    private int numberConnections;
    private int dedicatedConections;
    private boolean waitIfBusy;
    private Stack availableConnections;
    private static Logger logger = new Logger("com.ftd.osp.utilities.QMS.QMSConnectionPool");
    private static ConfigurationUtil configUtil;

    private static  QMSConnectionPool _instance = new QMSConnectionPool();

    /**
     * A private constructor for the QMSConnectionPool class.  This sets the initial variables
     *
     */
    private QMSConnectionPool() 
    {
      try 
      {
        configUtil = ConfigurationUtil.getInstance();
      } catch (Exception ex) 
      {
        logger.error(ex);
      } 
      
    }

    /**
     * This function returns the instance of this singleton
     * @return Returns the instance of QMSConnectionPool
     *
     */
    public static QMSConnectionPool getInstance()
    {
        return _instance;
    }

    /**
     * Gets a QMSAddressBroker connection from the pool.  If no connections are available
     * it creates a new connection.
     * @exception Exception Throws an Exception if the connection limit is reached
     * @return a connection from the pool
     *
     */
    public QMSAddressBrokerWrapper getDedicatedConnection() throws Exception
    {
        return getConnection(true);
    }

    /**
     * Gets a QMSAddressBroker connection from the pool
     * @exception Exception Throws an Exception if the connection limit is reached
     * @return a connection from the pool
     *
     */
    public QMSAddressBrokerWrapper getConnection() throws Exception
    {
        return getConnection(false);
    }

    /**
     * Gets a QMSAddressBroker connection from the pool
     * @exception Exception Throws an Exception if the connection limit is reached
     * @return a connection from the pool
     *
     */
    private synchronized QMSAddressBrokerWrapper getConnection(boolean waitOverride) throws Exception
    {
        logger.debug("getConnection(" + waitOverride + ")");
        QMSAddressBrokerWrapper broker = null;
        
        broker = makeNewConnection(true);
        return broker;
    }

    /**
     *  This explicitly makes a new connection. Called in the foreground when initializing the pool, and called in the background when running.
     * @return An address broker connection
     *
     */
    private QMSAddressBrokerWrapper makeNewConnection(boolean destroyAfterUse) throws Exception
    {
        QMSAddressBrokerWrapper broker = null;
        QMSAddressBrokerFactory factory = QMSAddressBrokerFactory.getInstance();

        String serverString = getServerString();

        if(destroyAfterUse)
        {
            broker = factory.makeQMSAddressBrokerWrapper(serverString, "SOCKET", destroyAfterUse);
            logger.debug("Made new QMSAddressBrokerWrapper for a dedicated connection");
            dedicatedConections ++;            
        }
        else if(this.numberConnections < this.maxConnections)
        {
            broker = factory.makeQMSAddressBrokerWrapper(serverString, "SOCKET", destroyAfterUse);
            logger.debug("Made new QMSAddressBrokerWrapper and added it to the pool");
            numberConnections++;            
        }

        return broker;
    }

    /**
     * Frees a QMSAddressBroker connection from the busy pool
     * @param connection QMSAddressBroker to be freed
     *
     */
    public synchronized void free(QMSAddressBrokerWrapper connection)
    {
        connection.clear();
        connection.close();
        dedicatedConections--;
        logger.debug("Closed QMSAddressBrokerWrapper");
    }

    /**
     * Returns the total number of connections (availableConnections + busyConnections)
     * @return The total numbet of connections.
     *
     */
    public synchronized int totalConnections()
    {
        return(numberConnections + dedicatedConections);
    }

    /** Close all the connections. Use with caution:
    *  be sure no connections are in use before
    *  calling. Note that you are not <I>required</I> to
    *  call this when done with a qmsConnectionPool, since
    *  connections are guaranteed to be closed when
    *  garbage collected. But this method gives more control
    *  regarding when the connections are closed.
    */
    public synchronized void closeAllConnections()
    {
        closeConnections(availableConnections);
        availableConnections = new Stack();
    }

    /**
     *
     * Closes Address Boroker connections in the pool
     * @param connections A Vector of AddressBrokers
     *
     */
    private void closeConnections(Stack connections)
    {
        for(int i=0; i<connections.size(); i++)
        {
            QMSAddressBrokerWrapper connection = (QMSAddressBrokerWrapper)connections.pop();
            numberConnections--;
            if (connection != null)
            {
                connection.close();
                logger.debug("Connection closed");
            }
        }
    }

    /**
     *
     * Returns a String representing the current state of the pool
     * @return A String representing the pool
     *
     */
    public synchronized String toString()
    {
        String info = "maxConnections = " + maxConnections + "\n" +
                      "availableConnections.size() = " + availableConnections.size() + "\n" +
                      "numberConnections = " + numberConnections + "\n" +
                      "dedicatedConections = " + dedicatedConections + "\n" +
                      "waitIfBusy = " + waitIfBusy + "\n";
        return info;
    }

    public void finalize()
    {
        this.closeAllConnections();
    }

    public static String getServerString() throws TransformerException, IOException,
                  SAXException, ParserConfigurationException 
    {
        Map serverNames = new HashMap();
        Map serverPorts = new HashMap();
        StringBuffer sb = new StringBuffer();
        String key = null;
        int port;

        try {
            String serverName = configUtil.getFrpGlobalParm(QMS_CONTEXT, QMS_SERVERNAME_1);
            String serverPort = configUtil.getFrpGlobalParm(QMS_CONTEXT, QMS_SERVERPORT_1);
            serverNames.put("1", serverName);
            serverPorts.put("1", serverPort);
            logger.debug("Adding " + serverName + ":" + serverPort);

            serverName = configUtil.getFrpGlobalParm(QMS_CONTEXT, QMS_SERVERNAME_2);
            serverPort = configUtil.getFrpGlobalParm(QMS_CONTEXT, QMS_SERVERPORT_2);
            serverNames.put("2", serverName);
            serverPorts.put("2", serverPort);
            logger.debug("Adding " + serverName + ":" + serverPort);

            // check to make sure each server is valid
            // if not valid do not include it in the String
            Iterator it = serverNames.keySet().iterator();
            while(it.hasNext())
            {
                key = (String)it.next();
                serverName = (String)serverNames.get(key);
                serverPort = (String)serverPorts.get(key);

                port = Integer.parseInt(serverPort);

                if(ServerCheckUtil.checkServerPort(serverName, port))
                {
                    sb.append(serverName).append(":").append(port).append("|");
                }
            }
        } catch (Exception e) {
            logger.error(e);
        }

        // Remove last '|'
        if(sb.charAt(sb.length() -1) == '|')
        {
            sb.deleteCharAt(sb.length() -1);
        }
        logger.info(sb.toString());

        return sb.toString();
    }
}