package com.ftd.osp.utilities.QMS;

public class QMSAddressBrokerFactory
{
    private static QMSAddressBrokerFactory factory = new QMSAddressBrokerFactory();

    /**
     * The private constructor
     */
    private QMSAddressBrokerFactory()
    {
    }

    public static QMSAddressBrokerFactory getInstance()
    {
        return factory;
    }


    public QMSAddressBrokerWrapper makeQMSAddressBrokerWrapper(String serverString, String transport, boolean destroyAfterUse) throws Exception
    {
        QMSAddressBrokerWrapper broker = null;

        broker = new QMSAddressBrokerWrapper(serverString, transport, destroyAfterUse);

        return broker;
    }
}