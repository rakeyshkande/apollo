package com.ftd.osp.utilities.filearchive.vo;

import java.sql.Timestamp;

/**
 * Value object for FRP.FILE_ARCHIVE_PARAMETERS table
 */
public class FileArchiveParmsVO {
    String context;
    String name;
    Long daysToHold;
    Timestamp lastPurgeDate;
    
    public FileArchiveParmsVO() {
    }

    public void setContext(String context) {
        this.context = context;
    }

    public String getContext() {
        return context;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setDaysToHold(Long daysToHold) {
        this.daysToHold = daysToHold;
    }

    public Long getDaysToHold() {
        return daysToHold;
    }

    public void setLastPurgeDate(Timestamp lastPurgeDate) {
        this.lastPurgeDate = lastPurgeDate;
    }

    public Timestamp getLastPurgeDate() {
        return lastPurgeDate;
    }
}
