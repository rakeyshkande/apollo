package com.ftd.osp.utilities.filearchive.vo;

import java.sql.Timestamp;

/**
 * Value object for the FRP.FILE_ARCHIVE table
 */
public class FileArchiveVO {
    private Long fileArchiveId;
    private String context;
    private String name;
    private String fileName;
    private String data;
    private Timestamp createdOn;
    private String createdBy;
    
    public FileArchiveVO() {
    }

    public void setFileArchiveId(Long fileArchiveId) {
        this.fileArchiveId = fileArchiveId;
    }

    public Long getFileArchiveId() {
        return fileArchiveId;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public String getContext() {
        return context;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getData() {
        return data;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedBy() {
        return createdBy;
    }
}
