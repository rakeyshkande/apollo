package com.ftd.osp.utilities.filearchive.dao;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;

import com.ftd.osp.utilities.filearchive.vo.FileArchiveParmsVO;
import com.ftd.osp.utilities.filearchive.vo.FileArchiveVO;

import java.math.BigDecimal;

import java.sql.Clob;
import java.sql.Connection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Data access layer for the file archive utility.
 */
public class FileArchiveDAO {
    /**
     * Default constructor
     */
    public FileArchiveDAO() {
    }

    /**
     * Get the latest archive files with the passed in context and name.  All
     * files with the same "CREATED_ON" timestamp will be returned
     * @param conn database connection
     * @param context filter
     * @param name filter
     * @return HashMap with a key of String/fileName and value of FileArchiveVO
     * @throws Exception
     */
    public HashMap<String,FileArchiveVO> getLatestArchiveFiles(Connection conn, String context, String name) throws Exception {   
        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs = null;
        HashMap<String,FileArchiveVO> map = new HashMap<String,FileArchiveVO>();
        
        /* build DataRequest object */
        HashMap inputParams = new HashMap();
        inputParams.put("IN_CONTEXT",context);
        inputParams.put("IN_NAME",name);
        dataRequest.setInputParams(inputParams);
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("FA_GET_LATEST_ARCHIVE_FILES");
        
        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);
        
        if( rs!=null ) {
            while( rs.next() ) {
                FileArchiveVO vo = new FileArchiveVO();
                vo.setFileArchiveId(rs.getLong("FILE_ARCHIVE_ID"));
                vo.setContext(rs.getString("CONTEXT"));
                vo.setName(rs.getString("NAME"));
                String fileName = rs.getString("FILE_NAME");
                vo.setFileName(fileName);
                Clob tClob = rs.getClob("FILE_DATA_TXT");
                if( tClob!=null ) {
                    vo.setData(tClob.getSubString(1, (int) tClob.length()));
                }
                vo.setCreatedOn(rs.getTimestamp("CREATED_ON"));
                vo.setCreatedBy(rs.getString("CREATED_BY"));
                map.put(fileName, vo);
            }
        }
        
        return map;
    }

    /**
     * Get all related archive files that have the same context, name, and 
     * "CREATED_ON" timestamp of the passed in id
     * @param conn database connection
     * @param archiveFileId PK into the FRP_FILE_ARCHIVES table
     * @return HashMap with a key of String/fileName and value of FileArchiveVO
     * @throws Exception
     */
    public HashMap<String,FileArchiveVO> getRelatedArchiveFiles(Connection conn, Long archiveFileId) throws Exception {   
        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs = null;
        HashMap<String,FileArchiveVO> map = new HashMap<String,FileArchiveVO>();
        
        /* build DataRequest object */
        HashMap inputParams = new HashMap();
        inputParams.put("IN_ARCHIVE_FILE_ID",archiveFileId);
        dataRequest.setInputParams(inputParams);
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("FA_GET_RELATED_ARCHIVE_FILES");
        
        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);
        
        if( rs!=null ) {
            while( rs.next() ) {
                FileArchiveVO vo = new FileArchiveVO();
                vo.setFileArchiveId(rs.getLong("FILE_ARCHIVE_ID"));
                vo.setContext(rs.getString("CONTEXT"));
                vo.setName(rs.getString("NAME"));
                String fileName = rs.getString("FILE_NAME");
                vo.setFileName(fileName);
                Clob tClob = rs.getClob("FILE_DATA_TXT");
                if( tClob!=null ) {
                    vo.setData(tClob.getSubString(1, (int) tClob.length()));
                }
                vo.setCreatedOn(rs.getTimestamp("CREATED_ON"));
                vo.setCreatedBy(rs.getString("CREATED_BY"));
                map.put(fileName, vo);
            }
        }
        
        return map;
    }

    /**
     * Inserts a record into the FRP.FILE_ARCHIVES table
     * @param conn database conneciton
     * @param vo record to be inserted
     * @return primary key of record that was just inserted
     * @throws Exception
     */
    public long insertFileArchive(Connection conn, FileArchiveVO vo) throws Exception {
        DataRequest dataRequest = new DataRequest();
        
        /* build DataRequest object */
        HashMap inputParams = new HashMap();
        inputParams.put("IN_CONTEXT",vo.getContext());
        inputParams.put("IN_NAME",vo.getName());
        inputParams.put("IN_FILE_NAME",vo.getFileName());
        inputParams.put("IN_FILE_DATA_TXT",vo.getData());
        inputParams.put("IN_CREATED_ON",vo.getCreatedOn());
        inputParams.put("IN_CREATED_BY",vo.getCreatedBy());

        dataRequest.setInputParams(inputParams);
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("FA_INSERT_FILE_ARCHIVE");
        
        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();   
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equalsIgnoreCase("N"))
        {
            String message = (String) outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        } 
        
        BigDecimal id = (BigDecimal) outputs.get("OUT_ID");
        
        return id.longValue();
    }

    /**
     * Return all records from the FRP.FILE_ARCHIVE_PARAMETERS table
     * @param conn database connection
     * @return list of FileArchiveParmsVO value objects
     * @throws Exception
     */
    public List<FileArchiveParmsVO> getFileArchiveParameters(Connection conn) throws Exception {
        List<FileArchiveParmsVO> list = new ArrayList<FileArchiveParmsVO>();  
        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs = null;
        
        /* build DataRequest object */
        HashMap inputParams = new HashMap();
        dataRequest.setInputParams(inputParams);
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("FA_GET_FILE_ARCHIVE_PARAMETERS");
        
        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);
        
        if( rs!=null ) {
            while( rs.next() ) {
                FileArchiveParmsVO vo = new FileArchiveParmsVO();
                vo.setContext(rs.getString("CONTEXT"));
                vo.setName(rs.getString("NAME"));
                vo.setDaysToHold(rs.getLong("DAYS_TO_HOLD_QTY"));
                vo.setLastPurgeDate(rs.getTimestamp("LAST_PURGE_DATE"));
                list.add(vo);
            }
        }
        
        return list;
    }

    /**
     * Get a list of unique Category/Name pairs in the FRP.FILE_ARCHIVE table
     * @return HashMap with key of String/Category and value of list of names
     */
    public HashMap<String,List> getFileArchiveCategories(Connection conn) throws Exception {
        
        HashMap<String,List> map = new HashMap<String,List>();
        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs = null;
        
        /* build DataRequest object */
        HashMap inputParams = new HashMap();
        dataRequest.setInputParams(inputParams);
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("FA_GET_FILE_ARCHIVE_CATEGORIES");
        
        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);
        
        if( rs!=null ) {
            while( rs.next() ) {
                String context = rs.getString("CONTEXT");
                String name = rs.getString("NAME");
                
                List existing = map.get(context);
                if( existing==null ) {
                    List<String> newList = new ArrayList<String>();
                    newList.add(name);
                    map.put(context,newList);
                } else {
                    existing.add(name);
                }
            }
        }
        
        return map;
    }

    /**
     * Update the record in FRP.FILE_ARACHIVE_PARAMETERS.  If the record does not
     * exist, it will be added.
     * @param conn database connection
     * @param vo holding the input parameters
     * @throws Exception
     */
    public void updateFileArchiveParameters(Connection conn, FileArchiveParmsVO vo) throws Exception {
        DataRequest dataRequest = new DataRequest();
        
        /* build DataRequest object */
        HashMap inputParams = new HashMap();
        inputParams.put("IN_CONTEXT",vo.getContext());
        inputParams.put("IN_NAME",vo.getName());
        inputParams.put("IN_DAYS_TO_HOLD",vo.getDaysToHold());

        dataRequest.setInputParams(inputParams);
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("UPDATE_FILE_ARCHIVE_PARAMETER");
        
        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();   
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equalsIgnoreCase("N"))
        {
            String message = (String) outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        } 
        
    }
}
