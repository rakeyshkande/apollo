package com.ftd.osp.utilities;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;

import java.util.Map;

import org.apache.commons.lang.StringUtils;


public class LockUtil  {
  private static Logger logger = new Logger("com.ftd.osp.utilities.LockUtil");

  /**
   * Obtains a CLEAN.CSR_LOCKED_ENTITIES lock
   * 
   * @param conn Database connection
   * @param processId - The process id to lock
   * @exception Exception
   * @return true if lock was obtained / false if not
   */
  public boolean obtainProcessLock(Connection conn, String processId, String sessionId) throws Exception {

      boolean result = false;

      if(logger.isDebugEnabled()) {
          logger.debug("obtainProcessLock :" + processId);
      }

      DataAccessUtil dau = DataAccessUtil.getInstance();
      DataRequest dataRequest = new DataRequest();
      dataRequest.setConnection(conn);
      dataRequest.addInputParam("IN_ENTITY_TYPE", processId);
      dataRequest.addInputParam("IN_ENTITY_ID", processId);
      dataRequest.addInputParam("IN_SESSION_ID", sessionId);
      dataRequest.addInputParam("IN_CSR_ID", "SYS");
      dataRequest.addInputParam("IN_ORDER_LEVEL", "N/A");
      dataRequest.setStatementID("UPDATE_PROCESS_LOCKED_ENTITIES");

      Map outputs = (Map) dau.execute(dataRequest);
      String status = (String) outputs.get("OUT_STATUS");
      String lockObtained = (String) outputs.get("OUT_LOCK_OBTAINED");
      String lockedCsr = (String) outputs.get("OUT_LOCKED_CSR");
      logger.debug(status + " " + lockObtained + " " + lockedCsr);

      if (StringUtils.equalsIgnoreCase(lockObtained, "Y")) {
          result = true;
      }
      
      return result;
  }

  /**
   * Obtains and returns customer tier information given an order GUID
   * 
   * @param conn Database connection
   * @param processId - The process id to unlock
   * @exception Exception
   * @return true if lock was released / false if not
   */
  public boolean releaseProcessLock(Connection conn, String processId, String sessionId) throws Exception {

      boolean result = false;

      if(logger.isDebugEnabled()) {
          logger.debug("releaseProcessLock :" + processId);
      }

      DataAccessUtil dau = DataAccessUtil.getInstance();
      DataRequest dataRequest = new DataRequest();
      dataRequest.setConnection(conn);
      dataRequest.addInputParam("IN_ENTITY_TYPE", processId);
      dataRequest.addInputParam("IN_ENTITY_ID", processId);
      dataRequest.addInputParam("IN_SESSION_ID", sessionId);
      dataRequest.addInputParam("IN_CSR_ID", "SYS");
      dataRequest.setStatementID("DELETE_PROCESS_LOCKED_ENTITIES");
    
      Map outputs = (Map) dau.execute(dataRequest);
      String status = (String) outputs.get("OUT_STATUS");
      if (StringUtils.equals(status, "Y")) {
          result = true;
      }

    return result;
  }   
}