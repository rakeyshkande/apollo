package com.ftd.osp.utilities;

import com.ftd.osp.utilities.plugins.Logger;


/**
 * This class is currently being utilized by partner_order_processing.webloyalty and order_email
 * 
 * The main functionality of this class is to provide stand alone functions for the calling
 * methods to Encrypt and Decrypt strings based on the algorithm provided by Webloyalty. 
 * 
 * Note that this algorithm might not be PCI compliant and may thus be replaced in the future. 
 * 
 * @author Ali Lakhani 
 */


public class WebloyaltyUtilities
{

    private Logger logger;
    private static final String LOGGER_CATEGORY = "com.ftd.osp.utilities.WebloyaltyUtilities";

/*******************************************************************************************
 * Constructor - default constructor
 *******************************************************************************************/
    public WebloyaltyUtilities()
    {
        this.logger = new Logger(LOGGER_CATEGORY);
    }

/*******************************************************************************************
  * decrypt()
  ******************************************************************************************
  * This method will call decrypt a string by utilizing the algorithm provided by Webloyalty
  *
  * @param  toBeDecrypted - string that needs to be decrypted
  * @return String - decrypted string
  * @throws - none
  * 
  */
    public String decrypt(String toBeDecrypted)
    {
      String decryptedString = "";
      String hexValue;

      String toBeDecryptedReverse = (new StringBuffer(toBeDecrypted)).reverse().toString();
      long hexToLongValue = 0;
      long decipheredLongValue = 0;
      char decipheredCharValue;

      for (int i = 0; i < toBeDecryptedReverse.length(); i+=2)
      {
        hexValue = toBeDecryptedReverse.substring(i,i+2);
        hexToLongValue = 0;
        decipheredLongValue = 0;
        hexToLongValue = Long.parseLong(hexValue,16);
        decipheredLongValue = (hexToLongValue ^ (decryptedString.length() + 1) % 256);
        decipheredCharValue = (char) decipheredLongValue;
        decryptedString += decipheredCharValue;
      }
      return decryptedString;
    }


/*******************************************************************************************
  * encrypt()
  ******************************************************************************************
  * This method will call encrypt a string by utilizing the algorithm provided by Webloyalty
  *
  * @param  toBeEncrypted - string that needs to be encrypted
  * @return String - encrypted string
  * @throws - none
  * 
  */
    public String encrypt(String toBeEncrypted)
    {
      String encryptedString = "";
      String hexValue;
      int encodedIntValue = 0;
      char encodedCharValue;

      for (int i = 0; i < toBeEncrypted.length(); i++)
      {
        hexValue = "";
        encodedCharValue = toBeEncrypted.charAt(i);
        encodedIntValue = (int)encodedCharValue;
        hexValue = Integer.toHexString(encodedIntValue ^ ((i+1)%256)).toString();
        if (hexValue.length()<2)
          hexValue = '0' + hexValue;
        encryptedString += hexValue;
      }
      encryptedString = (new StringBuffer(encryptedString)).reverse().toString();
      return encryptedString;
    }

}