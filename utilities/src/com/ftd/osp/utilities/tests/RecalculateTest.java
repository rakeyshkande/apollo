package com.ftd.osp.utilities.tests;

import com.ftd.osp.utilities.order.RecalculateException;
import com.ftd.osp.utilities.order.RecalculateOrderBO;
import com.ftd.osp.utilities.order.vo.AddOnsVO;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.order.vo.RecipientAddressesVO;
import com.ftd.osp.utilities.order.vo.RecipientsVO;

import java.io.PrintStream;

import java.math.BigDecimal;

import java.sql.Connection;
import java.sql.DriverManager;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;


/**
 * This class tests the price calculation on an order object.
 **/
public class RecalculateTest extends TestCase  {

  /**
   * Create a constructor that take a String parameter and passes it 
   * to the super class
   **/
  public RecalculateTest(String name) {
      super(name);
      
      
      
  }

  PrintStream originalPrintStream;
  
  /**
   * Override setup() to initialize variables
   **/   
  protected void setUp(){

  }

  /**
   * Override tearDown() to release any permanent resources you allocated in setUp 
   */
   public void tearDown(){

   }


   /**
   * Prints the Results of the Order for the specific test
   * @param order
   */
    public void printOrderResult(OrderVO order)
    {
      StringBuilder sb = new StringBuilder(255).append(getName()).append(":");
      sb.append("Order Total=").append(order.getOrderTotal());
      sb.append(";Product Total=").append(order.getProductsTotal());      
      sb.append(";Tax=").append(order.getTaxTotal());
      sb.append(";Service Fee=").append(order.getServiceFeeTotal());
      sb.append(";Shipping Fee=").append(order.getShippingFeeTotal());
      sb.append(";Service Fee Savings=").append(order.getServiceFeeTotalSavings());
      sb.append(";Shipping Fee Savings=").append(order.getShippingFeeTotalSavings());      
      sb.append(";Points=").append(getTotalPoints(order));
      
      System.out.println(sb.toString());

    }
    
   /**
    * This test is just testing to make sure the object call be called without
    * blowing up.
    **/    
    public void testCalPriceGeneric() throws Exception{
            Connection conn = getConnection();
          
            conn.setAutoCommit(false);
            
            OrderVO order = new OrderVO();

            //source code:6643
            //product: 7038=floral
            //state: IL -tax
            //country: US (domestic)
            //Intl: N
            //ship method: SD (florist delivered)
            //size choice: A (Standard)
            //expected price:
            order = buildOrder("6643","7038","IL","US","N","SD","A");

            RecalculateOrderBO calc = new RecalculateOrderBO();
            calc.recalculate(conn, order);
            printOrderResult(order);
    }

/** This test checks florist delivered & non-taxed state */
    public void testCalPriceFloristNoTax() throws Exception{
            Connection conn = getConnection();
          
            conn.setAutoCommit(false);
            
            OrderVO order = new OrderVO();

            //source code:7001-discount$10
            //product: 3469   type=floral
            //state: NE - no tax
            //country: US (domestic)
            //Intl: N 
            //ship method: null - florist
            //size choice: B =deluxe
            //expected price: 68.98
            order = buildOrder("7001","3469","NE","US","N",null,"B");

            RecalculateOrderBO calc = new RecalculateOrderBO();
            calc.recalculate(conn, order);
            printOrderResult(order);

            ////assertTrue("Wrong total",order.getOrderTotal().equals("99.98"));
    }

/** This test checks to make sure tax is calculated correctly and shipping
 * is calculated correctly.*/
    public void testCalPriceShippingTax() throws Exception{
            Connection conn = getConnection();
          
            conn.setAutoCommit(false);
            
            OrderVO order = new OrderVO();

            //source code:7001-discount$10
            //product: 2586   type=SDG
            //state: IL
            //country: US (domestic)
            //Intl: N
            //ship method: SA = saturday
            //size choice: A =standard
            //expected price: 64.32
            order = buildOrder("7001","2586","IL","US","N","SA","A");

            RecalculateOrderBO calc = new RecalculateOrderBO();
            calc.recalculate(conn, order);
            printOrderResult(order);

            //assertTrue("Wrong total",order.getOrderTotal().equals("64.32"));
    }

/** This test is making sure that percentage discounts are calculated correctly.
 *  This order is not taxed.*/
    public void testCalPriceDiscountPercent() throws Exception{
            Connection conn = getConnection();
          
            conn.setAutoCommit(false);
            
            OrderVO order = new OrderVO();

            //source code:3025 -discount=10%
            //product: 3469   type=floral
            //state: NE - no tax
            //country: US (domestic)
            //Intl: N 
            //ship method: SD - florist
            //size choice: B =deluxe
            //expected price: 88.99
            order = buildOrder("3025","3469","NE","US","N","SD","B");

            RecalculateOrderBO calc = new RecalculateOrderBO();
            calc.recalculate(conn, order);
            printOrderResult(order);

            //assertTrue("Wrong total",order.getOrderTotal().equals("88.99"));
    }


/** Test no discounts.*/
    public void testCalPriceNoDiscount() throws Exception{
            Connection conn = getConnection();
          
            conn.setAutoCommit(false);
            
            OrderVO order = new OrderVO();

            //source code:7001 -discount=10%
            //product: 8212   type=floral
            //state: NE - no tax
            //country: US (domestic)
            //Intl: N 
            //ship method: SD - florist
            //size choice: B =deluxe
            //expected price: 88.99
            order = buildOrder("7001","8212","NE","US","N","SD","A");

            RecalculateOrderBO calc = new RecalculateOrderBO();
            calc.recalculate(conn, order);
            printOrderResult(order);

            //assertTrue("Wrong total",order.getOrderTotal().equals("69.98"));
    }


/** This test is making sure that prices are calculated correctly 
 *  when the order contains addons.*/
    public void testCalPriceAddons() throws Exception{
            Connection conn = getConnection();
          
            conn.setAutoCommit(false);
            
            OrderVO order = new OrderVO();

            //source code:3025 -discount=10%
            //product: 3449   type=floral
            //state: IL - tax
            //country: US (domestic)
            //Intl: N 
            //ship method: SD - florist
            //size choice: C - Premium
            //expected price: 88.99
            order = buildOrder("3025","3449","IL","US","N","SD","C");

            //Addon
            //Ballons x2 @$5
            //Card X1 @3.50
            List details = order.getOrderDetail();
            OrderDetailsVO detail = (OrderDetailsVO)details.get(0);
            List addons = new ArrayList();
            AddOnsVO addon1 = new AddOnsVO();
            addon1.setAddOnCode("RC96"); //card
            addon1.setAddOnQuantity("1");
            AddOnsVO addon2 = new AddOnsVO();
            addon2.setAddOnCode("A"); //balloon
            addon2.setAddOnQuantity("2");
            addons.add(addon1);
            addons.add(addon2);
            detail.setAddOns(addons);



            RecalculateOrderBO calc = new RecalculateOrderBO();
            calc.recalculate(conn, order);
            printOrderResult(order);

            //assertTrue("Wrong total",order.getOrderTotal().equals("105.09"));
    }

/** This test is making sure that prices are calculated correctly 
 *  when the order contains addons--using ADDONA.*/
    public void testCalPriceAddonA() throws Exception{
            Connection conn = getConnection();
          
            conn.setAutoCommit(false);
            
            OrderVO order = new OrderVO();

            //source code:3025 -discount=10%
            //product: 3449   type=floral
            //state: IL - tax
            //country: US (domestic)
            //Intl: N 
            //ship method: SD - florist
            //size choice: C - Premium
            //expected price: 88.99
            order = buildOrder("3025","3449","IL","US","N","SD","C");

            //Addon = A
            List details = order.getOrderDetail();
            OrderDetailsVO detail = (OrderDetailsVO)details.get(0);
            List addons = new ArrayList();
            AddOnsVO addon2 = new AddOnsVO();
            addon2.setAddOnCode("A"); //balloon
            addon2.setAddOnQuantity("2");
            addons.add(addon2);
            detail.setAddOns(addons);



            RecalculateOrderBO calc = new RecalculateOrderBO();
            calc.recalculate(conn, order);
            printOrderResult(order);

            //assertTrue("Wrong total",order.getOrderTotal().equals("105.09"));
    }



/** This tests an order which contains multiple lines.  One line will be taxed and the
 *  other will not be.  The order will also received a 10% discount.*/
    public void testCalPriceTwoLines() throws Exception{
            Connection conn = getConnection();
          
            conn.setAutoCommit(false);
            
            OrderVO order = new OrderVO();

            //source code:3025 -discount=10%
            //product: 3469   type=floral
            //state: NE - no tax
            //country: US (domestic)
            //Intl: N 
            //ship method: SD - florist
            //size choice: B =deluxe
            //expected price: 88.99
            order = buildOrder("3025","3469","NE","US","N","SD","B");

            //add line to the order
            //product: 2586   type=SDG
            //state: IL
            //country: US (domestic)
            //ship method: SA = saturday
            //size choice: A =standard
            //expected price: 64.32
            RecipientAddressesVO address = new RecipientAddressesVO();
            address.setStateProvince("IL");
            address.setCountry("US");
            List addresses = new ArrayList();
            addresses.add(address);
            RecipientsVO recip = new RecipientsVO();
            recip.setRecipientAddresses(addresses);
            List recipients = new ArrayList();
            recipients.add(recip);
            OrderDetailsVO line = new OrderDetailsVO();
            line.setDeliveryDate(getDeliveryDate());
            line.setSourceCode("3025");
            line.setProductId("2586");
            line.setShipMethod("SA");
            line.setRecipients(recipients);
            line.setSizeChoice("A");
            List details = order.getOrderDetail();
            details.add(line);
            line.setLineNumber(String.valueOf(details.size()));


            RecalculateOrderBO calc = new RecalculateOrderBO();
            calc.recalculate(conn, order);
            printOrderResult(order);

            //assertTrue("Wrong total",order.getOrderTotal().equals("150.11"));
    }


/** This tests an order which contains multiple lines.  The first
 *  line of the order will generate an exception (no product).  The
 *  recalculate object should still calculate the price of the second 
 *  line before throwing the exception.*/
    public void testCalPriceErrorLineOne() throws Exception{
            Connection conn = getConnection();
          
            conn.setAutoCommit(false);
            
            OrderVO order = new OrderVO();


            //source code:3025 -discount=10%
            //product: null >> generates exception
            //state: NE - no tax
            //country: US (domestic)
            //Intl: N 
            //ship method: SD - florist
            //size choice: B =deluxe
            //expected price: 88.99
            order = buildOrder("3025",null,"NE","US","N","SD","B");

            //add line to the order
            //product: 3469   type=floral
            //state: NE - no tax
            //country: US (domestic)
            //Intl: N 
            //ship method: SD - florist
            //size choice: B =deluxe
            //expected price: 88.99
            RecipientAddressesVO address = new RecipientAddressesVO();
            address.setStateProvince("NE");
            address.setCountry("US");
            List addresses = new ArrayList();
            addresses.add(address);
            RecipientsVO recip = new RecipientsVO();
            recip.setRecipientAddresses(addresses);
            List recipients = new ArrayList();
            recipients.add(recip);
            OrderDetailsVO line = new OrderDetailsVO();
            line.setProductId("3469");
            line.setShipMethod("SD");
            line.setRecipients(recipients);
            line.setSizeChoice("B");
            List details = order.getOrderDetail();
            details.add(line);


            RecalculateOrderBO calc = new RecalculateOrderBO();

            try{
              calc.recalculate(conn, order);
              ////assertTrue("Exception not thrown",false);                
            }
              catch(RecalculateException re)
              {
                printOrderResult(order);
                ////assertTrue("Wrong total",order.getOrderTotal().equals("88.99"));                
              }

    }

/** This test is making sure that a product is not discounted if it's flag
 * is set to 'N' in the product master table. */
    public void testCalPriceDiscountFlagOnProduct() throws Exception{
            Connection conn = getConnection();
          
            conn.setAutoCommit(false);
            
            OrderVO order = new OrderVO();

            //source code:3025 -discount=10%
            //product: T2   DISCOUNTS NOT ALLOWED
            //state: NE - no tax
            //country: US (domestic)
            //Intl: N 
            //ship method: SD - florist
            //size choice: B =deluxe
            //expected price: 88.99
            order = buildOrder("3025","T2","NE","US","N","SD","B");

            RecalculateOrderBO calc = new RecalculateOrderBO();
            calc.recalculate(conn, order);
            printOrderResult(order);

            ////assertTrue("Wrong total",order.getOrderTotal().equals("48.98"));
    }


/** This test is checking the calculations for internationial orders. */
    public void testCalPriceIntl() throws Exception{
            Connection conn = getConnection();
          
            conn.setAutoCommit(false);
            
            OrderVO order = new OrderVO();

            //source code:7001
            //product: 7550
            //state: 
            //country: Italy
            //Intl: N 
            //ship method: SD
            //size choice: A
            //expected price: 92.98
            order = buildOrder("7001","7550","NE","IT","N","SD","A");

            RecalculateOrderBO calc = new RecalculateOrderBO();
            calc.recalculate(conn, order);
            printOrderResult(order);

            ////assertTrue("Wrong total",order.getOrderTotal().equals("138.98"));
    }


/** This test is checking the reward calculations using the 
 *  promtion table.  Point calculations down on the item level. */
    public void testRewardPointsITMPriceHeader() throws Exception{
            Connection conn = getConnection();
          
            conn.setAutoCommit(false);
            
            OrderVO order = new OrderVO();

            //source code:7426 <base points 125
            //product: 8212
            //state: IL
            //country: US
            //Intl: N 
            //ship method: ND
            //size choice: A
            //expected price: 75.05
            //expect rewards: 125
            order = buildOrder("7426","8212","IL","US","N","SD","A");

            RecalculateOrderBO calc = new RecalculateOrderBO();
            calc.recalculate(conn, order);
            printOrderResult(order);

            ////assertTrue("Wrong total",order.getOrderTotal().equals("75.05"));

            
            
            //////assertTrue("Wrong Point Total",points.equals("125"));
    }


/** This test is checking the reward calculations using the 
 *  promtion table.  Point calculations done based on dollar. */
    public void testRewardPointsDOLPriceHeader() throws Exception{
            Connection conn = getConnection();
          
            conn.setAutoCommit(false);
            
            OrderVO order = new OrderVO();

            //source code:7619 <calculate per dollar
            //product: 8212
            //state: IL
            //country: US
            //Intl: N 
            //ship method: ND
            //size choice: A
            //expected price: 75.05
            //expect rewards: 650
            order = buildOrder("7619","8212","IL","US","N","SD","A");

            RecalculateOrderBO calc = new RecalculateOrderBO();
            calc.recalculate(conn, order);
            printOrderResult(order);

            ////assertTrue("Wrong total",order.getOrderTotal().equals("75.05"));


            
            // ////assertTrue("Wrong Point Total",points.equals("650"));
    }

/** This test is checking the reward calculations using the 
 *  promtion table.  Point calculations done by percent. */
    public void testRewardPointsPERPriceHeader() throws Exception{
            Connection conn = getConnection();
          
            conn.setAutoCommit(false);
            
            OrderVO order = new OrderVO();

            //source code:8017 <points = 10%
            //product: 8212
            //state: IL
            //country: US
            //Intl: N 
            //ship method: ND
            //size choice: A
            //expected price: 75.05
            //expect rewards: 125
            order = buildOrder("8017","8212","IL","US","N","SD","A");

            RecalculateOrderBO calc = new RecalculateOrderBO();
            calc.recalculate(conn, order);
            printOrderResult(order);

            ////assertTrue("Wrong total",order.getOrderTotal().equals("75.05"));

            String points = getTotalPoints(order) + "";
            //System.out.println("TOTAL points:" + points);
            
            //////assertTrue("Wrong Point Total",points.equals("12.00"));
    }

   /**
    * This test checks to make sure an exception thrown when bad data is 
    * passed in.
    * Bad Data=Source Code
    **/    
    public void testBadSourceCode() throws Exception{
            Connection conn = getConnection();
          
            conn.setAutoCommit(false);
            
            OrderVO order = new OrderVO();

            //source code:XXXX
            //product: 7038=floral
            //state: IL -tax
            //country: US (domestic)
            //Intl: N
            //ship method: SD (florist delivered)
            //size choice: A (Standard)
            order = buildOrder("XXXX","7038","IL","US","N","SD","A");

            RecalculateOrderBO calc = new RecalculateOrderBO();

            try{
                calc.recalculate(conn, order);
                ////assertTrue("Error was NOT thrown",false);
            }
            catch(RecalculateException re)
            {
              //error is what we expect
            }
    }


   /**
    * This test checks to make sure an exception thrown when bad data is 
    * passed in.
    * Bad Data=Product
    **/    
    public void testBadProduct() throws Exception{
            Connection conn = getConnection();
          
            conn.setAutoCommit(false);
            
            OrderVO order = new OrderVO();

            //source code:6643
            //product: @#$%
            //state: IL -tax
            //country: US (domestic)
            //Intl: N
            //ship method: SD (florist delivered)
            //size choice: A (Standard)
            order = buildOrder("6643","@#$%","IL","US","N","SD","A");

            RecalculateOrderBO calc = new RecalculateOrderBO();

            try{
                calc.recalculate(conn, order);
                ////assertTrue("Error was NOT thrown",false);
            }
            catch(RecalculateException re)
            {
              //error is what we expect
            }
    }

   /**
    * This test checks to make sure an exception thrown when bad data is 
    * passed in.
    * Bad Data=State
    **/    
    public void testBadState() throws Exception{
            Connection conn = getConnection();
          
            conn.setAutoCommit(false);
            
            OrderVO order = new OrderVO();

            //source code:6643
            //product: 7038=floral
            //state: XX
            //country: US (domestic)
            //Intl: N
            //ship method: SD (florist delivered)
            //size choice: A (Standard)
            order = buildOrder("6643","7038","XX","US","N","SD","A");

            RecalculateOrderBO calc = new RecalculateOrderBO();

            try{
                calc.recalculate(conn, order);
                ////assertTrue("Error was NOT thrown",false);
            }
            catch(RecalculateException re)
            {
              //error is what we expect
            }
    }

   /**
    * This test checks to make sure an exception thrown when bad data is 
    * passed in.
    * Bad Data=Country
    **/    
    public void testBadCountry() throws Exception{
            Connection conn = getConnection();
          
            conn.setAutoCommit(false);
            
            OrderVO order = new OrderVO();

            //source code:6643
            //product: 7038=floral
            //state: IL -tax
            //country: XX
            //Intl: N
            //ship method: SD (florist delivered)
            //size choice: A (Standard)
            order = buildOrder("6643","7038","IL","XX","N","SD","A");

            RecalculateOrderBO calc = new RecalculateOrderBO();

            try{
                calc.recalculate(conn, order);
                ////assertTrue("Error was NOT thrown",false);
            }
            catch(RecalculateException re)
            {
              //error is what we expect
            }
    }

   /**
    * This test checks to make sure an exception thrown when bad data is 
    * passed in.
    * Bad Data=Source Code
    **/    
    public void testBadShipMethod() throws Exception{
            Connection conn = getConnection();
          
            conn.setAutoCommit(false);
            
            OrderVO order = new OrderVO();

            //source code:6643
            //product: 2586 = SDG
            //state: IL -tax
            //country: US (domestic)
            //Intl: N
            //ship method: XX
            //size choice: A (Standard)
            order = buildOrder("6643","2586","IL","US","N","XX","A");

            RecalculateOrderBO calc = new RecalculateOrderBO();

            try{
                calc.recalculate(conn, order);
                ////assertTrue("Error was NOT thrown",false);
            }
            catch(RecalculateException re)
            {
              //error is what we expect
            }
    }

   /**
    * This test checks to make sure an exception thrown when bad data is 
    * passed in.
    * Bad Data=Source Code
    **/    
    public void testBadSize() throws Exception{
            Connection conn = getConnection();
          
            conn.setAutoCommit(false);
            
            OrderVO order = new OrderVO();

            //source code:6643
            //product: 7038=floral
            //state: IL -tax
            //country: US (domestic)
            //Intl: N
            //ship method: SD
            //size choice: A (Standard)
            order = buildOrder("6643","7038","IL","US","N","SD","XX");

            RecalculateOrderBO calc = new RecalculateOrderBO();

            try{
                calc.recalculate(conn, order);
                ////assertTrue("Error was NOT thrown",false);
            }
            catch(RecalculateException re)
            {
              //error is what we expect
            }
    }


   /**
    * This test checks to make sure an exception thrown when Null data is 
    * passed in.
    * Null =Source Code
    **/    
    public void testNullSourceCode() throws Exception{
            Connection conn = getConnection();
          
            conn.setAutoCommit(false);
            
            OrderVO order = new OrderVO();

            //source code:Null
            //product: 7038=floral
            //state: IL -tax
            //country: US (domestic)
            //Intl: N
            //ship method: SD (florist delivered)
            //size choice: A (Standard)
            order = buildOrder(null,"7038","IL","US","N","SD","A");

            RecalculateOrderBO calc = new RecalculateOrderBO();

            try{
                calc.recalculate(conn, order);
                ////assertTrue("Error was NOT thrown",false);
            }
            catch(RecalculateException re)
            {
              //error is what we expect
            }
    }


   /**
    * This test checks to make sure an exception thrown when Null data is 
    * passed in.
    * Null Data=Product
    **/    
    public void testNullProduct() throws Exception{
            Connection conn = getConnection();
          
            conn.setAutoCommit(false);
            
            OrderVO order = new OrderVO();

            //source code:6643
            //product: Null
            //state: IL -tax
            //country: US (domestic)
            //Intl: N
            //ship method: SD (florist delivered)
            //size choice: A (Standard)
            order = buildOrder("6643",null,"IL","US","N","SD","A");

            RecalculateOrderBO calc = new RecalculateOrderBO();

            try{
                calc.recalculate(conn, order);
                ////assertTrue("Error was NOT thrown",false);
            }
            catch(RecalculateException re)
            {
              //error is what we expect
            }
    }

   /**
    * This test checks to make sure an exception thrown when Null data is 
    * passed in.
    * Null Data=State
    **/    
    public void testNullState() throws Exception{
            Connection conn = getConnection();
          
            conn.setAutoCommit(false);
            
            OrderVO order = new OrderVO();

            //source code:6643
            //product: 7038=floral
            //state: Null
            //country: US (domestic)
            //Intl: N
            //ship method: SD (florist delivered)
            //size choice: A (Standard)
            order = buildOrder("6643","7038",null,"US","N","SD","A");

            RecalculateOrderBO calc = new RecalculateOrderBO();

            try{
                calc.recalculate(conn, order);
                ////assertTrue("Error was NOT thrown",false);
            }
            catch(RecalculateException re)
            {
              //error is what we expect
            }
    }

   /**
    * This test checks to make sure an exception thrown when Null data is 
    * passed in.
    * Null Data=Country
    **/    
    public void testNullCountry() throws Exception{
            Connection conn = getConnection();
          
            conn.setAutoCommit(false);
            
            OrderVO order = new OrderVO();

            //source code:6643
            //product: 7038=floral
            //state: IL -tax
            //country: XX
            //Intl: N
            //ship method: SD (florist delivered)
            //size choice: A (Standard)
            order = buildOrder("6643","7038","IL",null,"N","SD","A");

            RecalculateOrderBO calc = new RecalculateOrderBO();

            try{
                calc.recalculate(conn, order);
                ////assertTrue("Error was NOT thrown",false);
            }
            catch(RecalculateException re)
            {
              //error is what we expect
            }
    }

   /**
    * This test checks to make sure an exception thrown when Null data is 
    * passed in.
    * Null Data=Source Code
    **/    
    public void testNullShipMethod() throws Exception{
            Connection conn = getConnection();
          
            conn.setAutoCommit(false);
            
            OrderVO order = new OrderVO();

            //source code:6643
            //product: 2586 = SDG
            //state: IL -tax
            //country: US (domestic)
            //Intl: N
            //ship method: Null
            //size choice: A (Standard)
            order = buildOrder("6643","2586","IL","US","N",null,"A");

            RecalculateOrderBO calc = new RecalculateOrderBO();

            try{
                calc.recalculate(conn, order);
                ////assertTrue("Error was NOT thrown",false);
            }
            catch(RecalculateException re)
            {
              //error is what we expect
            }
    }

   /**
    * This test checks to make sure an exception thrown when Null data is 
    * passed in.
    * Null Data=Size
    **/    
    public void testNullSize() throws Exception{
            Connection conn = getConnection();
          
            conn.setAutoCommit(false);
            
            OrderVO order = new OrderVO();

            //source code:6643
            //product: 7038=floral
            //state: IL -tax
            //country: US (domestic)
            //Intl: N
            //ship method: SD
            //size choice: null
            order = buildOrder(null,"7038","IL","US","N","SD",null);

            RecalculateOrderBO calc = new RecalculateOrderBO();

            try{
                calc.recalculate(conn, order);
                ////assertTrue("Error was NOT thrown",false);
            }
            catch(RecalculateException re)
            {
              //error is what we expect
            }
    }
   

  private String getTotalPoints(OrderVO order)
  {
    List details = order.getOrderDetail();
    String totalPoints = "0";
    for (int i = 0; i < details.size(); i++) 
    { 
        OrderDetailsVO detail = (OrderDetailsVO)details.get(i);      
        String points = detail.getMilesPoints();
        if(points != null){
            int pos = points.indexOf("."); 
            if(pos >0)
            {
              BigDecimal totalBig = new BigDecimal(totalPoints).add(new BigDecimal(points));
              totalPoints = (totalBig.setScale(2,BigDecimal.ROUND_DOWN)).toString();
            }
            else
            {
              int totalInt = (new Integer(totalPoints).intValue()) + (new Integer(points).intValue());
              totalPoints = new Integer(totalInt).toString();
            }
        }
    }
    return totalPoints;
    
  }
  
  String getDeliveryDate()
  {
    SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
    return sdf.format(new java.util.Date(System.currentTimeMillis() + 24*60*60*1000));
  }

  private OrderVO buildOrder(String sourceCode,String productId,
            String deliveryState, String deliveryCountry,String intl, String shipMethod, String size)
            {

            OrderVO order = new OrderVO();
            order.setSourceCode(sourceCode);



            RecipientAddressesVO address = new RecipientAddressesVO();
            address.setStateProvince(deliveryState);
            address.setCountry(deliveryCountry);
            //address.setInternational(intl);
            List addresses = new ArrayList();
            addresses.add(address);

            RecipientsVO recip = new RecipientsVO();
            recip.setRecipientAddresses(addresses);
            List recipients = new ArrayList();
            recipients.add(recip);
            
            OrderDetailsVO line = new OrderDetailsVO();
            line.setSourceCode(sourceCode);
            line.setDeliveryDate(getDeliveryDate());
            line.setProductId(productId);
            line.setShipMethod(shipMethod);
            line.setRecipients(recipients);
            line.setSizeChoice(size);
            List lines = new ArrayList();
            lines.add(line);
            line.setLineNumber(String.valueOf(lines.size()));

            order.setOrderDetail(lines);
            
            order.setBuyerEmailAddress("arajoo@ftdi.com");
            order.setCompanyId("FTD");
            order.setOeOrder(true);
            order.setBuyerSignedIn(true);
            order.setOrderDate(new java.util.Date());

            return order;
              
            }

  
  private Connection getConnection() throws Exception {
            String driver_ = "oracle.jdbc.driver.OracleDriver";
            String database_ = "jdbc:oracle:thin:@adonis.ftdi.com:1521:dev48i";
            String user_ = "osp";
            String password_ = "osp";

            Class.forName(driver_);
            Connection conn = DriverManager.getConnection(database_, user_, password_);  

            return conn;
  }

//used for debugging
    public void debugtest() throws Exception{
            Connection conn = getConnection();
          
            conn.setAutoCommit(false);
            
            OrderVO order = new OrderVO();

            //source code:5360
            //product: 
            //state: IL
            //country: US (domestic)
            //Intl: N 
            //ship method: <blank>
            //size choice: A
            //expected price: 88.99
            order = buildOrder("2512","M831","IL","US","N","ND","A");

            RecalculateOrderBO calc = new RecalculateOrderBO();
            calc.recalculate(conn, order);
            printOrderResult(order);

            ////assertTrue("Wrong total",order.getOrderTotal().equals("51.45"));
    }


    public void testSubcodeDiscount() throws Exception{
            Connection conn = getConnection();
          
            conn.setAutoCommit(false);
            
            OrderVO order = new OrderVO();

            //source code:5360
            //product: 
            //state: IL
            //country: US (domestic)
            //Intl: N 
            //ship method: <blank>
            //size choice: A
            //expected price: 88.99
            order = buildOrder("5360","FF28","IL","US","N","","A");

            //add subcode
            OrderDetailsVO line = (OrderDetailsVO)order.getOrderDetail().get(0);
            line.setProductSubCodeId("F728");

            RecalculateOrderBO calc = new RecalculateOrderBO();
            calc.recalculate(conn, order);
            printOrderResult(order);

            ////assertTrue("Wrong total",order.getOrderTotal().equals("52.75"));
    }

    public void testSubcodeDiscountB() throws Exception{
            Connection conn = getConnection();
          
            conn.setAutoCommit(false);
            
            OrderVO order = new OrderVO();

            //source code:5360
            //product: F728 > subcode
            //state: IL
            //country: US (domestic)
            //Intl: N 
            //ship method: <blank>
            //size choice: A
            //expected price: 88.99
            order = buildOrder("5360","F728","IL","US","N","","A");



            RecalculateOrderBO calc = new RecalculateOrderBO();
            calc.recalculate(conn, order);
            printOrderResult(order);

            ////assertTrue("Wrong total",order.getOrderTotal().equals("52.75"));
    }

  /**
   * To run several tests at once JUnit provides an object, TestSuite which runs
   * any number of test cases together. 
   * Each test is run as a separate instance of the test class
   * You make your suite accessible to a TestRunner tool with a static method 
   * suite that returns a test suite. 
   **/
  public static Test suite(){
    TestSuite suite = new TestSuite();
    // Create an instance of of Test class that will run this test case 
    // When the test is run, the name of the test is used to look up the method 
    // to run, using reflection

      suite.addTest(new RecalculateTest("testCalPriceDiscountPercent"));
      suite.addTest(new RecalculateTest("testCalPriceGeneric"));
      suite.addTest(new RecalculateTest("testCalPriceFloristNoTax"));
      suite.addTest(new RecalculateTest("testCalPriceShippingTax"));
      suite.addTest(new RecalculateTest("testCalPriceDiscountPercent"));

        //THE DB Needs to updated to run this test. The discount flag on the product must be turned off
//      suite.addTest(new RecalculateTest("testCalPriceDiscountFlagOnProduct"));

      suite.addTest(new RecalculateTest("testCalPriceIntl"));
      suite.addTest(new RecalculateTest("testRewardPointsITMPriceHeader"));
      suite.addTest(new RecalculateTest("testRewardPointsPERPriceHeader"));
      suite.addTest(new RecalculateTest("testRewardPointsDOLPriceHeader"));
      suite.addTest(new RecalculateTest("testCalPriceTwoLines"));
      suite.addTest(new RecalculateTest("testCalPriceAddons"));      
      suite.addTest(new RecalculateTest("testBadCountry"));            
      suite.addTest(new RecalculateTest("testBadProduct"));            
      suite.addTest(new RecalculateTest("testBadShipMethod"));            
      suite.addTest(new RecalculateTest("testBadSize"));            
      suite.addTest(new RecalculateTest("testBadSourceCode"));            
      suite.addTest(new RecalculateTest("testBadState"));                 
      suite.addTest(new RecalculateTest("testNullCountry"));            
      suite.addTest(new RecalculateTest("testNullProduct"));            
      suite.addTest(new RecalculateTest("testNullShipMethod"));            
      suite.addTest(new RecalculateTest("testNullSize"));            
      suite.addTest(new RecalculateTest("testNullSourceCode"));            
      suite.addTest(new RecalculateTest("testNullState"));       
      suite.addTest(new RecalculateTest("testCalPriceErrorLineOne"));           
      suite.addTest(new RecalculateTest("testSubcodeDiscount"));           
     suite.addTest(new RecalculateTest("testSubcodeDiscountB"));    
    suite.addTest(new RecalculateTest("testCalPriceNoDiscount"));


 //    suite.addTest(new RecalculateTest("debugtest"));              

    return suite;
  }

  /**
   * You can either user the text ui or swing ui to run the test
   **/
  public static void main(String[] args){
    // pass in the class of the test that needs to be run
    //junit.swingui.TestRunner.run(FrameworkTest.class);
    junit.textui.TestRunner.run(suite());
  }
  
  
}