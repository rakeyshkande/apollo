package com.ftd.osp.utilities.tests;

//import com.ftd.osp.ordervalidator.*;
import com.ftd.osp.utilities.order.vo.*;
import com.ftd.osp.utilities.order.*;
import com.ftd.osp.utilities.dataaccess.util.OSPConnectionPool;
import com.ftd.osp.utilities.vo.*;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import java.rmi.server.UID;
import com.ftd.osp.utilities.GUID.GUIDGenerator;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import java.io.*;
import java.math.*;
import java.sql.*;
import java.sql.Connection;
import java.util.*;
import oracle.jdbc.driver.*;
import oracle.sql.*;
import com.ftd.osp.utilities.*;
import java.util.HashMap;
import java.util.Map;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.w3c.dom.Document;
import com.ftd.osp.utilities.xml.*;
import org.w3c.dom.*;


/**
 * The test class will extend junit.framework.TestCase
 **/
public class FRPDataMapperTest extends TestCase  {

  /**
   * Create a constructor that take a String parameter and passes it 
   * to the super class
   **/
  public FRPDataMapperTest(String name) {
      super(name);
  }

  /**
   * Override setup() to initialize variables
   **/   
  protected void setUp(){
  }

  /**
   * Override tearDown() to release any permanent resources you allocated in setUp 
   */
   public void tearDown(){
   }

   /**
    * Retrieve an order from the FRP schema and then issue an update against
    * that order.
    **/    
    public void testGetAndUpdateFromFRP() throws Exception{

            OrderVO order = new OrderVO();

            String guid = "";

            int maxTests = 1;
            
            List guidList = getFRPGuidList();

            Connection conn = null;
            FRPMapperDAO frpMapperDAO = null;

            //do this for n amount of guids
            for (int i = 0; i < guidList.size() && i<maxTests; i++) 
            {

                  conn = getConnection();
                  conn.setAutoCommit(false);
                  frpMapperDAO = new FRPMapperDAO(conn);
            
                  guid = (String)guidList.get(i);
guid = "FTD_GUID_3171519100-3579808805934203420-18060530910-60862889905632418620-5683985640-16188669851-1135879650-14077762980-39193200701324541648248-610726427126-516972798103-33066824757-594567377152";
                 
                  System.out.println("("+i+")Start FRP Retrieve\\Update:" + guid);            
                  try{
                    System.out.println("About to retrieve from FRP");
                    order = frpMapperDAO.mapOrderFromDB(guid);


                    //List payments = order.getPayments();
                   // PaymentsVO pay = (PaymentsVO)payments.get(0);
                   // pay.setGiftCertificateId("XXX");
                    
order = new FRPDataFixer(getConnection()).fixOrder(order);
                    
                    System.out.println("About to update FRP");
                    scrambleCustomerIds(order);

order.setCsrId("test");                    
                    frpMapperDAO.updateOrder(order);  
                    conn.commit();
                  System.out.println("("+i+")End FRP Retrieve\\Update:" + guid);            
                  }
                  catch(Exception e){
                      conn.rollback();
                      e.printStackTrace();
                      assertTrue("("+i+")Failed on order guid:"+guid,false);
                  }
                  finally{            
                    conn.close();
                  }
        
            }
            System.out.println(order.toString());        
    }
  
   /**
    * Retrieve an order from the Scrub schema and insert it into FRP
    **/    
    public void testGetInsertUpdateFromScrub() throws Exception {

            OrderVO order = new OrderVO();

            String guid = "1";
            Connection conn = getConnection();            
            ScrubMapperDAO scrub = new ScrubMapperDAO(conn);
            int maxTests = 5;
            
            List guidList = getFRPScrubGuidList();


            FRPMapperDAO frpMapperDAO = null;

            //do this for n amount of guids
            for (int i = 0; i < guidList.size() && i<=maxTests; i++) 
            {
                  guid = (String)guidList.get(0);
            
                  System.out.println("("+i+")Start Scrub Retrieve\\Update:" + guid);            

                  conn = getConnection();
                  conn.setAutoCommit(false);
                  frpMapperDAO = new FRPMapperDAO(conn);            
                  scrub = new ScrubMapperDAO(conn);
            
                  try{
                    System.out.println("About to retrieve from Scrub");
                    order = scrub.mapOrderFromDB(guid);
                    fixOrder(order);
                    System.out.println("About to insert into FRP using Scrub order");
                    frpMapperDAO.mapOrderToDB(order);  
                    conn.commit();            
                    System.out.println("About to updated FRP using Scrub order");
                    frpMapperDAO.updateOrder(order);  
                    conn.commit();
                    System.out.println("("+i+")Start Scrub Retrieve\\Update:" + guid);                                
                  }
                  catch(Exception e){
                      conn.rollback();
                      e.printStackTrace();
                      assertTrue("("+i+")Failed on order guid:"+guid,false);                      
                  }
                  finally{            
                    conn.close();
                  }
            }        

            System.out.println(order.toString());        
    }


   /** Test using the validator.*/
    public void testWithValidator() throws Exception {
        Connection conn = getConnection();
        BusinessConfig config = new BusinessConfig();
        MessageToken token = new MessageToken();

        //token.setMessage("FTD_GUID_-1619097151011862845250210007592010783688790-1734887569018850007610-111234720301017216749120405754600120669538306321985900-433396882247-569941308146114831935190-337291438553004164082");
        token.setMessage("FTD_GUID_-690680880014788010720-1848878140010674493860-23426784202860853470-2080094697056639024013983794450736059916010528517040-704783928248179070916211110651779991471379672524156-90918726291");

        config.setConnection(conn);
    //    config.setMessageToken(token);

     //   OrderValidatorBO bo = new OrderValidatorBO();
      //  bo.execute(config);   
    }

   /**
    * Retrieve an order from the Scrub schema and insert it into FRP
    * Clean the order using the FRPDataFixer utiltiy before inserting.
    **/    
    public void testGetInsertUpdateFromScrubWithFixer() throws Exception {

            OrderVO order = new OrderVO();

            String guid = "1";
            Connection conn = getConnection();            
            ScrubMapperDAO scrub = new ScrubMapperDAO(conn);
            int maxTests = 1;
            
            List guidList = getFRPScrubGuidList();


            FRPMapperDAO frpMapperDAO = null;

            //do this for n amount of guids
            for (int i = 0; i < guidList.size() && i<=maxTests; i++) 
            {
                  guid = (String)guidList.get(0);
            
                  System.out.println("("+i+")Start Scrub Retrieve\\Update:" + guid);            

                  conn = getConnection();
                  conn.setAutoCommit(false);
                  frpMapperDAO = new FRPMapperDAO(conn);            
                  scrub = new ScrubMapperDAO(conn);
            
                  try{
                    System.out.println("About to retrieve from Scrub");
                    order = scrub.mapOrderFromDB(guid);

                    order = new FRPDataFixer(conn).fixOrder(order);

                  //fix the stuff the Fixer can't do
                  fixOrderMin(order);


                    System.out.println("About to insert into FRP using Scrub order");
                    frpMapperDAO.mapOrderToDB(order);  
                    conn.commit();            
                    System.out.println("About to updated FRP using Scrub order");
                    frpMapperDAO.updateOrder(order);  
                    conn.commit();
                    System.out.println("("+i+")Start Scrub Retrieve\\Update:" + guid);                                
                  }
                  catch(Exception e){
                      conn.rollback();
                      e.printStackTrace();
                      assertTrue("("+i+")Failed on order guid:"+guid,false);                      
                  }
                  finally{            
                    conn.close();
                  }
            }        

            System.out.println(order.toString());        
    }


  private Connection getConnection() throws Exception {
            String driver_ = "oracle.jdbc.driver.OracleDriver";
            String database_ = "jdbc:oracle:thin:@stheno-dev.ftdi.com:1521:DEV2";
            String user_ = "osp";
            String password_ = "osp";

            Class.forName(driver_);
            Connection conn = DriverManager.getConnection(database_, user_, password_);  

            return conn;
  }


  private Connection getConnection(String id, String pass) throws Exception {
            String driver_ = "oracle.jdbc.driver.OracleDriver";
            String database_ = "jdbc:oracle:thin:@stheno-dev.ftdi.com:1521:DEV2";
            String user_ = id;
            String password_ = pass;

            Class.forName(driver_);
            Connection conn = DriverManager.getConnection(database_, user_, password_);  

            return conn;
  }


    //method used for testing only
    private void scrambleCustomerIds(OrderVO order)
    {
            //buyers
          List buyers = order.getBuyer();
          for (int x = 0; x < buyers.size(); x++) 
          {
            BuyerVO buyer = (BuyerVO)buyers.get(x);
            //set unique
            buyer.setCustomerId(""+getUniqueNumber());
            }          

    }

    //method used for testing only
    //only fixes what FRPFixer cannot fix
    private void fixOrderMin(OrderVO order)
    {
      String VALID_STATUS = "1008";
      int custid = 100;
      String DESTINATIN_NAME = "??";
      if(order.getStatus() == null || order.getStatus().equals(""))
      {
        order.setStatus(VALID_STATUS);
      }    
      if(order.getMasterOrderNumber() == null || order.getMasterOrderNumber().equals(""))
      {
        order.setMasterOrderNumber(""+getUniqueNumber());
      }    
    }

    //method used for testing only
    //the method fixes everything with an order
    private void fixOrder(OrderVO order)
    {
      String VALID_STATUS = "1008";
      int custid = 100;
      String DESTINATIN_NAME = "??";
      if(order.getStatus() == null || order.getStatus().equals(""))
      {
        order.setStatus(VALID_STATUS);
      }


            //buyers
          List buyers = order.getBuyer();
          for (int x = 0; x < buyers.size(); x++) 
          {
            BuyerVO buyer = (BuyerVO)buyers.get(x);

            //set unique
            buyer.setCustomerId(""+getUniqueNumber());

            if(buyer.getBestCustomer() == null || buyer.getBestCustomer().equals("0"))
            {
              buyer.setBestCustomer("Y");
            }
            if(buyer.getAutoHold() == null || buyer.getAutoHold().equals("0"))
            {
              buyer.setAutoHold("Y");
            }
            
            if(buyer.getStatus() == null || buyer.getStatus().equals(""))
            {
              buyer.setStatus(VALID_STATUS);
            }



            List addresses = buyer.getBuyerAddresses();
            for (int y = 0; y < addresses.size(); y++)             
            {
              BuyerAddressesVO addr = (BuyerAddressesVO)addresses.get(x);

              if(addr.getAddressType() == null || addr.getAddressType().equals(""))
              {
                addr.setAddressType("Home");
              }              
              else if(addr.getAddressType().equalsIgnoreCase("home"))
              {
                addr.setAddressType("Home");                
              }
              else if(addr.getAddressType().equalsIgnoreCase("other"))
              {
                addr.setAddressType("Other");                
              }
            }


            List phones = buyer.getBuyerPhones();
            for (int y = 0; y < phones.size(); y++)             
            {
              BuyerPhonesVO phone = (BuyerPhonesVO)phones.get(y);

              if(phone.getPhoneType() == null || phone.getPhoneType().equals(""))
              {
                phone.setPhoneType("Home");
              }              
              else if(phone.getPhoneType().equalsIgnoreCase("home"))
              {
                phone.setPhoneType("Home");                
              }
              else if(phone.getPhoneType().equalsIgnoreCase("work"))
              {
                phone.setPhoneType("Work");                
              }
              else if(phone.getPhoneType().equalsIgnoreCase("fax"))
              {
                phone.setPhoneType("Fax");                
              }
            }
            List emails = buyer.getBuyerEmails();
            for (int y = 0; y < emails.size(); y++)             
            {
              BuyerEmailsVO email = (BuyerEmailsVO)emails.get(y);

              if(email.getPrimary()== null || email.getPrimary().equals(""))
              {
                email.setPrimary("Y");
              }              
            }

                                 
          }








      List details = order.getOrderDetail();
      for (int i = 0; i < details.size(); i++)       
      {
          OrderDetailsVO detail = (OrderDetailsVO)details.get(i);

          if(detail.getStatus() == null || detail.getStatus().equals(""))
          {
            detail.setStatus(VALID_STATUS);
          }

          List recipients = detail.getRecipients();
          for (int x = 0; x < recipients.size(); x++) 
          {
            RecipientsVO recipient = (RecipientsVO)recipients.get(x);
            if(recipient.getCustomerId() == null || recipient.getCustomerId().equals("0"))
            {
              recipient.setCustomerId("" + ++custid);
            }

            if(recipient.getStatus() == null || recipient.getStatus().equals(""))
            {
              recipient.setStatus(VALID_STATUS);
            }

            List destinations = recipient.getDestinations();
            for (int y = 0; y < destinations.size(); y++)             
            {
              DestinationsVO dest = (DestinationsVO)destinations.get(y);
              if(dest.getName() == null || dest.getName().equals(""))
              {
                dest.setName("??");
              }
            }

            List addresses = recipient.getRecipientAddresses();
            for (int y = 0; y < addresses.size(); y++)             
            {
              RecipientAddressesVO addr = (RecipientAddressesVO)addresses.get(y);
              if(addr.getInternational() == null || addr.getInternational().equals(""))
              {
                addr.setInternational("N");
              }
              if(addr.getAddressType() == null || addr.getAddressType().equals(""))
              {
                addr.setAddressType("Home");
              }              
              else if(addr.getAddressType().equalsIgnoreCase("home"))
              {
                addr.setAddressType("Home");                
              }
              else if(addr.getAddressType().equalsIgnoreCase("other"))
              {
                addr.setAddressType("Other");                
              }
            }


            List phones = recipient.getRecipientPhones();
            for (int y = 0; y < phones.size(); y++)             
            {
              RecipientPhonesVO phone = (RecipientPhonesVO)phones.get(y);

              if(phone.getPhoneType() == null || phone.getPhoneType().equals(""))
              {
                phone.setPhoneType("Home");
              }              
              else if(phone.getPhoneType().equalsIgnoreCase("home"))
              {
                phone.setPhoneType("Home");                
              }
              else if(phone.getPhoneType().equalsIgnoreCase("work"))
              {
                phone.setPhoneType("Work");                
              }
              else if(phone.getPhoneType().equalsIgnoreCase("fax"))
              {
                phone.setPhoneType("Fax");                
              }
            }            






            
          }
          
      }
      
      
    }

/**
  * A unitity method to generate a unique 10-digit number
  * @return long a 10-digit unique number
  */
    public static long getUniqueNumber()
    {
        UID uid = new UID();
        long hashCode = Math.abs( uid.hashCode() );
        return  (hashCode < (long)1000000000) ? ( hashCode + (long)1000000000 ) : hashCode;
    }



   public List getFRPGuidList() 
      throws Exception
   {

    //need special id to get to these tables
    String id="emueller";
    String pass="ed2017";
   
    Connection con = null;
    String tranResults = null;
    DataAccessUtil dataAccess = DataAccessUtil.getInstance();
    List guidList = new ArrayList();
     try{
       con = getConnection(id,pass);
       DataRequest dataRequest = new DataRequest();
       dataRequest.setStatementID("FRP_GUID_LIST");
       dataRequest.setConnection(con);
     
       CachedResultSet rs = (CachedResultSet)dataAccess.execute(dataRequest);
       while(rs.next()){
        String guid = (String)rs.getObject(1);
        guidList.add(guid);
       }
     } 
     finally
     {
       con.close();       
     } 
     return  guidList;
   }  

   public List getFRPScrubGuidList() 
      throws Exception
   {

    //need special id to get to these tables
    String id="emueller";
    String pass="ed2017";
   
    Connection con = null;
    String tranResults = null;
    DataAccessUtil dataAccess = DataAccessUtil.getInstance();
    List guidList = new ArrayList();
     try{
       con = getConnection(id,pass);
       DataRequest dataRequest = new DataRequest();
       dataRequest.setStatementID("FRP_SCRUB_GUID_LIST");
       dataRequest.setConnection(con);
     
       CachedResultSet rs = (CachedResultSet)dataAccess.execute(dataRequest);
       while(rs.next()){
        String guid = (String)rs.getObject(1);
        guidList.add(guid);
       }
     } 
     finally
     {
       con.close();       
     } 
     return  guidList;
   }  

  /**
   * You can either user the text ui or swing ui to run the test
   **/
  public static void main(String[] args){


    // pass in the class of the test that needs to be run
    //junit.swingui.TestRunner.run(FrameworkTest.class);
    junit.textui.TestRunner.run(suite());
  }

  /**
   * To run several tests at once JUnit provides an object, TestSuite which runs
   * any number of test cases together. 
   * Each test is run as a separate instance of the test class
   * You make your suite accessible to a TestRunner tool with a static method 
   * suite that returns a test suite. 
   **/
  public static Test suite(){
    TestSuite suite = new TestSuite();
    // Create an instance of of Test class that will run this test case 
    // When the test is run, the name of the test is used to look up the method 
    // to run, using reflection
    suite.addTest(new FRPDataMapperTest("testGetAndUpdateFromFRP"));
//    suite.addTest(new FRPDataMapperTest("testGetInsertUpdateFromScrub"));
//    suite.addTest(new FRPDataMapperTest("testGetInsertUpdateFromScrubWithFixer"));
//    suite.addTest(new FRPDataMapperTest("testWithValidator"));
    


    return suite;
  }
  
  
}