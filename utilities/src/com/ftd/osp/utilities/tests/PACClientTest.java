/**
 * 
 */
package com.ftd.osp.utilities.tests;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.RecipientAddressesVO;
import com.ftd.pac.util.PACConstants;
import com.ftd.pac.util.PACUtil;
import com.ftd.pac.util.client.PACSClient;
import com.ftd.pac.util.vo.PACRequest;
import com.ftd.pac.util.vo.PACResponse;

/**
 * @author smeka
 *
 */
public class PACClientTest extends TestCase  {
	
	PACRequest getProductAvailableDatesAndChargesReq = null;
	PACRequest getAvailableProductBundleReq = null; 
	PACRequest getAvailableProductBundleErrorReq = null; 
	PACRequest getPCProdAvailableDateReq = null;

	
	public PACClientTest(String testMethod) {
		super(testMethod);
	}

	/* (non-Javadoc)
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		createGetProdAvailDatesAndChargesReq();
		createGetAvailProdBundlesReq(); 
		createGetAvailableProductBundleErrorReq();
		createGetPCProdAvailReq();
	}

	private void createGetProdAvailDatesAndChargesReq() {
		/*  http://metisdapp01v1.ftdi.com:38080/pacs/getProductAvailableDatesAndCharges
			?productId=6027
			&addonIds=1234,345
			&sourceCode=352
			&numAvailDays=120
			&channel=APOLLO
			&orderDateTime=08052015180506
			&zipCode=65001
			&isFreeShippingMember=Y
			&includeCharge=Y
		 */		
		String productId = "6027";
		List<String> addonIds = Arrays.asList("345","1234");
		String sourceCode = "352";
		int numAvailDays = 5;  
		String zipCode = "65001";
		Date deliveryDate = null; 
		Boolean includeCharge = null;
		Boolean checkMDAvailable = null;
		Boolean isFSMember = null;
		
		getProductAvailableDatesAndChargesReq =  new PACRequest(productId, addonIds, sourceCode, numAvailDays, new Date(), zipCode,
				deliveryDate, includeCharge, checkMDAvailable, isFSMember, false,"");
		
	}

	private void createGetAvailableProductBundleErrorReq() { 
		/*
		http://metisdapp01v1.ftdi.com:38080/pacs/getAvailableProductBundle
		?productId=123422233
		&addonIds=12,13,14
		&sourceCode=254
		&channel=APOLLO
		&deliveryDate=08142015
		&orderDateTime=08142015101415
		&zipCode=650012
		&isFreeShippingMember=Y
		&includeCharge=Y
		&checkMorningDeliveryAvailable=Y
	 */
		
		String productId = "123422233";
		List<String> addonIds = Arrays.asList("12","13","14");
		String sourceCode = "254";
		int numAvailDays = 5;  
		String zipCode = "650012";
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DATE, 2); 
		Date deliveryDate = c.getTime();
		Boolean includeCharge = true;
		Boolean checkMDAvailable = true;
		Boolean isFSMember = null;
		
		
		getAvailableProductBundleErrorReq = new PACRequest(productId, addonIds, sourceCode, numAvailDays, new Date(), zipCode,
				deliveryDate, includeCharge, checkMDAvailable, isFSMember, false,"");
	}
	
	private void createGetAvailProdBundlesReq() {
		/*
			http://metisdapp01v1.ftdi.com:38080/pacs/getAvailableProductBundle
			?productId=1234
			&addonIds=12,13,14
			&sourceCode=254
			&channel=APOLLO
			&deliveryDate=08142015
			&orderDateTime=08142015101415
			&zipCode=650012
			&isFreeShippingMember=Y
			&includeCharge=Y
			&checkMorningDeliveryAvailable=Y
		 */
		String productId = "6027";
		List<String> addonIds = Arrays.asList("12","13","14");
		String sourceCode = "254";
		int numAvailDays = 5;  
		String zipCode = "650012";
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DATE, 2); 
		Date deliveryDate = c.getTime();
		Boolean includeCharge = true;
		Boolean checkMDAvailable = true;
		Boolean isFSMember = null;
		
		getAvailableProductBundleReq =  new PACRequest(productId, addonIds, sourceCode, numAvailDays, new Date(), zipCode,
				deliveryDate, includeCharge, checkMDAvailable, isFSMember, false,"");
		
	}
	
	private void createGetPCProdAvailReq() {
		/* http://10.222.72.201/pacs/getPCProductAvailDate
		 * ?productId=K229
		 * &sourceCode=350
		 * &channel=APOLLO
		 * &orderDateTime=10072015235959
		 * &zipCode=90210
		 * &isFreeShippingMember=N
		 * &includeCharge=Y
		 * */
		String productId = "K229"; 
		String sourceCode = "350"; 
		String zipCode = "90210";
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DATE, 2); 
		Date deliveryDate = c.getTime();
		Boolean includeCharge = true;
		Boolean checkMDAvailable = null;
		Boolean isFSMember = null;
		
		getPCProdAvailableDateReq =  new PACRequest(productId, null, sourceCode, null, new Date(), zipCode,
				deliveryDate, includeCharge, checkMDAvailable, isFSMember, true,"");
		
	}
	
	public void testGetProductAvailableDatesAndCharges() {
		PACSClient client = new PACSClient(PACConstants.DEFAULT_PAC_URL, PACConstants.GET_PROD_AVAIL_DATES_CHARGES); 
		PACResponse response = null;
		try {
			response = client.sendRequest(getProductAvailableDatesAndChargesReq);
		} catch (Exception e) {
			System.out.println(e);
		}
		System.out.println("getProductAvailableDatesAndCharges() Response: " + response);		
	}
	
	public void testGetAvailableProductBundles() {
		PACSClient client = new PACSClient(PACConstants.DEFAULT_PAC_URL, PACConstants.GET_AVAIL_PROD_BUNDLE);
		PACResponse pacResponse = null;
		PACUtil util = null;
		try {
			pacResponse = client.sendRequest(getAvailableProductBundleReq);
			if(pacResponse.getError() != null) {
				System.out.println("getProductAvailableBundle() Response.Error: " + pacResponse.getError().get(0));
			} else {
				util = new PACUtil();
				OrderDetailsVO item = new OrderDetailsVO();
				RecipientAddressesVO recipAddress = new RecipientAddressesVO();
				recipAddress.setStateProvince("IL");
				Map<String, Object> result = util.setItemFeeSplit(item, recipAddress, null, pacResponse, false);
				
				System.out.println("Fees: " + result.get("fee"));
				System.out.println("ShipMethod: " + item.getShipMethod());
				System.out.println("isFTDWBBNAvailable: " + item.getShipMethod());
			}
			
		} catch (Exception e) {
			System.out.println(e);
		} 
	}
	
	public void testGetAvailableProductBundleErrorReq() {
		PACSClient client = new PACSClient(PACConstants.DEFAULT_PAC_URL, PACConstants.GET_AVAIL_PROD_BUNDLE); 
		PACResponse response = null;
		try {
			response = client.sendRequest(getAvailableProductBundleErrorReq);
		} catch (Exception e) {
			System.out.println(e);
		}
		System.out.println("getProductAvailableBundle() Response: " + response);	
		System.out.println("getProductAvailableBundle() Response.Error: " + response.getError().get(0));
	}
	
	public void testGetPCvailableDate() {
		PACSClient client = new PACSClient(PACConstants.DEFAULT_PAC_URL, PACConstants.GET_PC_PROD_AVAIL_DATE); 
		PACResponse response = null;
		try {
			response = client.sendRequest(getPCProdAvailableDateReq);
		} catch (Exception e) {
			System.out.println(e);
		}
		System.out.println("getPCProdAvailableDateReq() Response: " + response);	
		System.out.println("getPCProdAvailableDateReq() Response.Error: " + response.getError().get(0));
	}
	
	
	public static Test suite() {
		TestSuite suite = new TestSuite();
		//suite.addTest(new PACClientTest("testGetProductAvailableDatesAndCharges"));
		//suite.addTest(new PACClientTest("testGetAvailableProductBundles"));
		//suite.addTest(new PACClientTest("testGetAvailableProductBundleErrorReq"));
		suite.addTest(new PACClientTest("testGetPCvailableDate"));
		return suite;
	}

}
