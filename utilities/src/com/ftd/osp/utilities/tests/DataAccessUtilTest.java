package com.ftd.osp.utilities.tests;
import com.ftd.osp.utilities.GUID.GUIDGenerator;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.io.BufferedReader;
import java.io.FileReader;

import java.sql.Clob;
import java.sql.Connection;
import java.sql.Driver;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.w3c.dom.Document;

import org.w3c.dom.Document;


/**
 * The test class will extend junit.framework.TestCase
 **/
public class DataAccessUtilTest extends TestCase  {

     String xmlData = "<?xml version = '1.0' encoding = 'utf-8'?><order guid=''>"
                             + "<header>"
                             + "<master-order-number>C47448</master-order-number>"
                             + "<source-code>522</source-code>"
                             + "<origin>FOX</origin>"
                             + "<order-count>1</order-count>"
                             + "<order-amount>61.11</order-amount>"
                             + "<transaction-date>Mon Jan 21 12:30:08 CST 2004</transaction-date>"
                             + "<socket-timestamp/>"
                             + "<buyer-first-name>Anshu</buyer-first-name>"
                             + "<buyer-last-name>Gaind</buyer-last-name>"
                             + "<buyer-address1>3113 woodcreek drive</buyer-address1>"
                             + "<buyer-address2/>"
                             + "<buyer-city>downers grove</buyer-city>"
                             + "<buyer-state>IL</buyer-state>"
                             + "<buyer-postal-code>60515</buyer-postal-code>"
                             + "<buyer-country>US</buyer-country>"
                             + "<buyer-daytime-phone>5555555555</buyer-daytime-phone>"
                             + "<buyer-evening-phone/>"
                             + "<buyer-work-ext/>"
                             + "<buyer-fax/>"
                             + "<buyer-email-address/>"
                             + "<ariba-buyer-cookie/>"
                             + "<ariba-asn-buyer-number/>"
                             + "<ariba-payload/>"
                             + "<news-letter-flag>Y</news-letter-flag>"
                             + "<cc-type>Visa</cc-type>"
                             + "<cc-number>4444333322221111</cc-number>"
                             + "<cc-exp-date>01/05</cc-exp-date>"
                             + "<cc-approval-code/>"
                             + "<cc-approval-amt/>"
                             + "<cc-approval-verbage/>"
                             + "<cc-approval-action-code/>"
                             + "<cc-avs-result/>"
                             + "<cc-acq-data/>"
                             + "<co-brand-credit-card-code/>"
                             + "<aafes-ticket-number/>"
                             + "<gift-certificates/>"
                             + "<co-brands/>"
                             + "</header>"
                             
                             + "<items>"
                             + "<item>"
                             + "<order-number>FTA131245</order-number>"
                             + "<order-total>61.11</order-total>"
                             + "<tax-amount>4.13</tax-amount>"
                             + "<service-fee>9.99</service-fee>"
                             + "<extra-shipping-fee/>"
                             + "<drop-ship-charges/>"
                             + "<retail-variable-price>46.99</retail-variable-price>"
                             + "<productid>C13-3068</productid>"
                             + "<product-price>46.99</product-price>"
                             + "<color/>"
                             + "<size/>"
                             + "<add-ons/>"
                             + "<recip-first-name>robin</recip-first-name>"
                             + "<recip-last-name>bird</recip-last-name>"
                             + "<recip-address1>7205 Seminole Drive</recip-address1>"
                             + "<recip-address2/>"
                             + "<recip-city>Darien</recip-city>"
                             + "<recip-state>IL</recip-state>"
                             + "<recip-postal-code>60561</recip-postal-code>"
                             + "<recip-country>US</recip-country>"
                             + "<recip-international/>"
                             + "<recip-phone>5555555555</recip-phone>"
                             + "<recip-phone-ext/>"
                             + "<ship-to-type>R</ship-to-type>"
                             + "<ship-to-type-name/>"
                             + "<ship-to-type-info/>"
                             + "<occassion>2</occassion>"
                             + "<delivery-date>02/21/2004</delivery-date>"
                             + "<second-delivery-date/>"
                             + "<card-message>test</card-message>"
                             + "<card-signature>test</card-signature>"
                             + "<special-instructions/>"
                             + "<shipping-method/>"
                             + "<lmg-flag/>"
                             + "<lmg-email-address/>"
                             + "<lmg-email-signature/>"
                             + "<fol-indicator>FTD</fol-indicator>"
                             + "<sunday-delivery-flag/>"
                             + "<sender-release-flag/>"
                             + "<qms-result-code/>"
                             + "<qms-address1>7205 Seminole Drive</qms-address1>"
                             + "<qms-address2/>"
                             + "<qms-city>Darien</qms-city>"
                             + "<qms-state>IL</qms-state>"
                             + "<qms-postal-code>60561</qms-postal-code>"
                             + "<qms-firm-name/>"
                             + "<qms-latitude/>"
                             + "<qms-longtitude/>"
                             + "<qms-override-flag/>"
                             + "<qms-usps-range-record-type/>"
                             + "<ariba-po-number/>"
                             + "<ariba-cost-center/>"
                             + "<ariba-ams-project-code/>"
                             + "<ariba-unspsc-code>empty</ariba-unspsc-code>"
                             + "<product-substitution-acknowlegement/>"
                             + "</item>"
                             + "<item>"
                             + "<order-number>FTA131245</order-number>"
                             + "<order-total>61.11</order-total>"
                             + "<tax-amount>4.13</tax-amount>"
                             + "<service-fee>9.99</service-fee>"
                             + "<extra-shipping-fee/>"
                             + "<drop-ship-charges/>"
                             + "<retail-variable-price>46.99</retail-variable-price>"
                             + "<productid>C13-3068</productid>"
                             + "<product-price>46.99</product-price>"
                             + "<color/>"
                             + "<size/>"
                             + "<add-ons/>"
                             + "<recip-first-name>robin</recip-first-name>"
                             + "<recip-last-name>bird</recip-last-name>"
                             + "<recip-address1>7205 Seminole Drive</recip-address1>"
                             + "<recip-address2/>"
                             + "<recip-city>Darien</recip-city>"
                             + "<recip-state>IL</recip-state>"
                             + "<recip-postal-code>60561</recip-postal-code>"
                             + "<recip-country>US</recip-country>"
                             + "<recip-international/>"
                             + "<recip-phone>5555555555</recip-phone>"
                             + "<recip-phone-ext/>"
                             + "<ship-to-type>R</ship-to-type>"
                             + "<ship-to-type-name/>"
                             + "<ship-to-type-info/>"
                             + "<occassion>2</occassion>"
                             + "<delivery-date>02/21/2004</delivery-date>"
                             + "<second-delivery-date/>"
                             + "<card-message>test</card-message>"
                             + "<card-signature>test</card-signature>"
                             + "<special-instructions/>"
                             + "<shipping-method/>"
                             + "<lmg-flag/>"
                             + "<lmg-email-address/>"
                             + "<lmg-email-signature/>"
                             + "<fol-indicator>FTD</fol-indicator>"
                             + "<sunday-delivery-flag/>"
                             + "<sender-release-flag/>"
                             + "<qms-result-code/>"
                             + "<qms-address1>7205 Seminole Drive</qms-address1>"
                             + "<qms-address2/>"
                             + "<qms-city>Darien</qms-city>"
                             + "<qms-state>IL</qms-state>"
                             + "<qms-postal-code>60561</qms-postal-code>"
                             + "<qms-firm-name/>"
                             + "<qms-latitude/>"
                             + "<qms-longtitude/>"
                             + "<qms-override-flag/>"
                             + "<qms-usps-range-record-type/>"
                             + "<ariba-po-number/>"
                             + "<ariba-cost-center/>"
                             + "<ariba-ams-project-code/>"
                             + "<ariba-unspsc-code>empty</ariba-unspsc-code>"
                             + "<product-substitution-acknowlegement/>"
                             + "</item>"
                             + "<item>"
                             + "<order-number>FTA131245</order-number>"
                             + "<order-total>61.11</order-total>"
                             + "<tax-amount>4.13</tax-amount>"
                             + "<service-fee>9.99</service-fee>"
                             + "<extra-shipping-fee/>"
                             + "<drop-ship-charges/>"
                             + "<retail-variable-price>46.99</retail-variable-price>"
                             + "<productid>C13-3068</productid>"
                             + "<product-price>46.99</product-price>"
                             + "<color/>"
                             + "<size/>"
                             + "<add-ons/>"
                             + "<recip-first-name>robin</recip-first-name>"
                             + "<recip-last-name>bird</recip-last-name>"
                             + "<recip-address1>7205 Seminole Drive</recip-address1>"
                             + "<recip-address2/>"
                             + "<recip-city>Darien</recip-city>"
                             + "<recip-state>IL</recip-state>"
                             + "<recip-postal-code>60561</recip-postal-code>"
                             + "<recip-country>US</recip-country>"
                             + "<recip-international/>"
                             + "<recip-phone>5555555555</recip-phone>"
                             + "<recip-phone-ext/>"
                             + "<ship-to-type>R</ship-to-type>"
                             + "<ship-to-type-name/>"
                             + "<ship-to-type-info/>"
                             + "<occassion>2</occassion>"
                             + "<delivery-date>02/21/2004</delivery-date>"
                             + "<second-delivery-date/>"
                             + "<card-message>test</card-message>"
                             + "<card-signature>test</card-signature>"
                             + "<special-instructions/>"
                             + "<shipping-method/>"
                             + "<lmg-flag/>"
                             + "<lmg-email-address/>"
                             + "<lmg-email-signature/>"
                             + "<fol-indicator>FTD</fol-indicator>"
                             + "<sunday-delivery-flag/>"
                             + "<sender-release-flag/>"
                             + "<qms-result-code/>"
                             + "<qms-address1>7205 Seminole Drive</qms-address1>"
                             + "<qms-address2/>"
                             + "<qms-city>Darien</qms-city>"
                             + "<qms-state>IL</qms-state>"
                             + "<qms-postal-code>60561</qms-postal-code>"
                             + "<qms-firm-name/>"
                             + "<qms-latitude/>"
                             + "<qms-longtitude/>"
                             + "<qms-override-flag/>"
                             + "<qms-usps-range-record-type/>"
                             + "<ariba-po-number/>"
                             + "<ariba-cost-center/>"
                             + "<ariba-ams-project-code/>"
                             + "<ariba-unspsc-code>empty</ariba-unspsc-code>"
                             + "<product-substitution-acknowlegement/>"
                             + "</item>"
                             + "<item>"
                             + "<order-number>FTA131245</order-number>"
                             + "<order-total>61.11</order-total>"
                             + "<tax-amount>4.13</tax-amount>"
                             + "<service-fee>9.99</service-fee>"
                             + "<extra-shipping-fee/>"
                             + "<drop-ship-charges/>"
                             + "<retail-variable-price>46.99</retail-variable-price>"
                             + "<productid>C13-3068</productid>"
                             + "<product-price>46.99</product-price>"
                             + "<color/>"
                             + "<size/>"
                             + "<add-ons/>"
                             + "<recip-first-name>robin</recip-first-name>"
                             + "<recip-last-name>bird</recip-last-name>"
                             + "<recip-address1>7205 Seminole Drive</recip-address1>"
                             + "<recip-address2/>"
                             + "<recip-city>Darien</recip-city>"
                             + "<recip-state>IL</recip-state>"
                             + "<recip-postal-code>60561</recip-postal-code>"
                             + "<recip-country>US</recip-country>"
                             + "<recip-international/>"
                             + "<recip-phone>5555555555</recip-phone>"
                             + "<recip-phone-ext/>"
                             + "<ship-to-type>R</ship-to-type>"
                             + "<ship-to-type-name/>"
                             + "<ship-to-type-info/>"
                             + "<occassion>2</occassion>"
                             + "<delivery-date>02/21/2004</delivery-date>"
                             + "<second-delivery-date/>"
                             + "<card-message>test</card-message>"
                             + "<card-signature>test</card-signature>"
                             + "<special-instructions/>"
                             + "<shipping-method/>"
                             + "<lmg-flag/>"
                             + "<lmg-email-address/>"
                             + "<lmg-email-signature/>"
                             + "<fol-indicator>FTD</fol-indicator>"
                             + "<sunday-delivery-flag/>"
                             + "<sender-release-flag/>"
                             + "<qms-result-code/>"
                             + "<qms-address1>7205 Seminole Drive</qms-address1>"
                             + "<qms-address2/>"
                             + "<qms-city>Darien</qms-city>"
                             + "<qms-state>IL</qms-state>"
                             + "<qms-postal-code>60561</qms-postal-code>"
                             + "<qms-firm-name/>"
                             + "<qms-latitude/>"
                             + "<qms-longtitude/>"
                             + "<qms-override-flag/>"
                             + "<qms-usps-range-record-type/>"
                             + "<ariba-po-number/>"
                             + "<ariba-cost-center/>"
                             + "<ariba-ams-project-code/>"
                             + "<ariba-unspsc-code>empty</ariba-unspsc-code>"
                             + "<product-substitution-acknowlegement/>"
                             + "</item>"
                             + "<item>"
                             + "<order-number>FTA131245</order-number>"
                             + "<order-total>61.11</order-total>"
                             + "<tax-amount>4.13</tax-amount>"
                             + "<service-fee>9.99</service-fee>"
                             + "<extra-shipping-fee/>"
                             + "<drop-ship-charges/>"
                             + "<retail-variable-price>46.99</retail-variable-price>"
                             + "<productid>C13-3068</productid>"
                             + "<product-price>46.99</product-price>"
                             + "<color/>"
                             + "<size/>"
                             + "<add-ons/>"
                             + "<recip-first-name>robin</recip-first-name>"
                             + "<recip-last-name>bird</recip-last-name>"
                             + "<recip-address1>7205 Seminole Drive</recip-address1>"
                             + "<recip-address2/>"
                             + "<recip-city>Darien</recip-city>"
                             + "<recip-state>IL</recip-state>"
                             + "<recip-postal-code>60561</recip-postal-code>"
                             + "<recip-country>US</recip-country>"
                             + "<recip-international/>"
                             + "<recip-phone>5555555555</recip-phone>"
                             + "<recip-phone-ext/>"
                             + "<ship-to-type>R</ship-to-type>"
                             + "<ship-to-type-name/>"
                             + "<ship-to-type-info/>"
                             + "<occassion>2</occassion>"
                             + "<delivery-date>02/21/2004</delivery-date>"
                             + "<second-delivery-date/>"
                             + "<card-message>test</card-message>"
                             + "<card-signature>test</card-signature>"
                             + "<special-instructions/>"
                             + "<shipping-method/>"
                             + "<lmg-flag/>"
                             + "<lmg-email-address/>"
                             + "<lmg-email-signature/>"
                             + "<fol-indicator>FTD</fol-indicator>"
                             + "<sunday-delivery-flag/>"
                             + "<sender-release-flag/>"
                             + "<qms-result-code/>"
                             + "<qms-address1>7205 Seminole Drive</qms-address1>"
                             + "<qms-address2/>"
                             + "<qms-city>Darien</qms-city>"
                             + "<qms-state>IL</qms-state>"
                             + "<qms-postal-code>60561</qms-postal-code>"
                             + "<qms-firm-name/>"
                             + "<qms-latitude/>"
                             + "<qms-longtitude/>"
                             + "<qms-override-flag/>"
                             + "<qms-usps-range-record-type/>"
                             + "<ariba-po-number/>"
                             + "<ariba-cost-center/>"
                             + "<ariba-ams-project-code/>"
                             + "<ariba-unspsc-code>empty</ariba-unspsc-code>"
                             + "<product-substitution-acknowlegement/>"
                             + "</item>"
                             + "<item>"
                             + "<order-number>FTA131245</order-number>"
                             + "<order-total>61.11</order-total>"
                             + "<tax-amount>4.13</tax-amount>"
                             + "<service-fee>9.99</service-fee>"
                             + "<extra-shipping-fee/>"
                             + "<drop-ship-charges/>"
                             + "<retail-variable-price>46.99</retail-variable-price>"
                             + "<productid>C13-3068</productid>"
                             + "<product-price>46.99</product-price>"
                             + "<color/>"
                             + "<size/>"
                             + "<add-ons/>"
                             + "<recip-first-name>robin</recip-first-name>"
                             + "<recip-last-name>bird</recip-last-name>"
                             + "<recip-address1>7205 Seminole Drive</recip-address1>"
                             + "<recip-address2/>"
                             + "<recip-city>Darien</recip-city>"
                             + "<recip-state>IL</recip-state>"
                             + "<recip-postal-code>60561</recip-postal-code>"
                             + "<recip-country>US</recip-country>"
                             + "<recip-international/>"
                             + "<recip-phone>5555555555</recip-phone>"
                             + "<recip-phone-ext/>"
                             + "<ship-to-type>R</ship-to-type>"
                             + "<ship-to-type-name/>"
                             + "<ship-to-type-info/>"
                             + "<occassion>2</occassion>"
                             + "<delivery-date>02/21/2004</delivery-date>"
                             + "<second-delivery-date/>"
                             + "<card-message>test</card-message>"
                             + "<card-signature>test</card-signature>"
                             + "<special-instructions/>"
                             + "<shipping-method/>"
                             + "<lmg-flag/>"
                             + "<lmg-email-address/>"
                             + "<lmg-email-signature/>"
                             + "<fol-indicator>FTD</fol-indicator>"
                             + "<sunday-delivery-flag/>"
                             + "<sender-release-flag/>"
                             + "<qms-result-code/>"
                             + "<qms-address1>7205 Seminole Drive</qms-address1>"
                             + "<qms-address2/>"
                             + "<qms-city>Darien</qms-city>"
                             + "<qms-state>IL</qms-state>"
                             + "<qms-postal-code>60561</qms-postal-code>"
                             + "<qms-firm-name/>"
                             + "<qms-latitude/>"
                             + "<qms-longtitude/>"
                             + "<qms-override-flag/>"
                             + "<qms-usps-range-record-type/>"
                             + "<ariba-po-number/>"
                             + "<ariba-cost-center/>"
                             + "<ariba-ams-project-code/>"
                             + "<ariba-unspsc-code>empty</ariba-unspsc-code>"
                             + "<product-substitution-acknowlegement/>"
                             + "</item>"
                             + "<item>"
                             + "<order-number>FTA131245</order-number>"
                             + "<order-total>61.11</order-total>"
                             + "<tax-amount>4.13</tax-amount>"
                             + "<service-fee>9.99</service-fee>"
                             + "<extra-shipping-fee/>"
                             + "<drop-ship-charges/>"
                             + "<retail-variable-price>46.99</retail-variable-price>"
                             + "<productid>C13-3068</productid>"
                             + "<product-price>46.99</product-price>"
                             + "<color/>"
                             + "<size/>"
                             + "<add-ons/>"
                             + "<recip-first-name>robin</recip-first-name>"
                             + "<recip-last-name>bird</recip-last-name>"
                             + "<recip-address1>7205 Seminole Drive</recip-address1>"
                             + "<recip-address2/>"
                             + "<recip-city>Darien</recip-city>"
                             + "<recip-state>IL</recip-state>"
                             + "<recip-postal-code>60561</recip-postal-code>"
                             + "<recip-country>US</recip-country>"
                             + "<recip-international/>"
                             + "<recip-phone>5555555555</recip-phone>"
                             + "<recip-phone-ext/>"
                             + "<ship-to-type>R</ship-to-type>"
                             + "<ship-to-type-name/>"
                             + "<ship-to-type-info/>"
                             + "<occassion>2</occassion>"
                             + "<delivery-date>02/21/2004</delivery-date>"
                             + "<second-delivery-date/>"
                             + "<card-message>test</card-message>"
                             + "<card-signature>test</card-signature>"
                             + "<special-instructions/>"
                             + "<shipping-method/>"
                             + "<lmg-flag/>"
                             + "<lmg-email-address/>"
                             + "<lmg-email-signature/>"
                             + "<fol-indicator>FTD</fol-indicator>"
                             + "<sunday-delivery-flag/>"
                             + "<sender-release-flag/>"
                             + "<qms-result-code/>"
                             + "<qms-address1>7205 Seminole Drive</qms-address1>"
                             + "<qms-address2/>"
                             + "<qms-city>Darien</qms-city>"
                             + "<qms-state>IL</qms-state>"
                             + "<qms-postal-code>60561</qms-postal-code>"
                             + "<qms-firm-name/>"
                             + "<qms-latitude/>"
                             + "<qms-longtitude/>"
                             + "<qms-override-flag/>"
                             + "<qms-usps-range-record-type/>"
                             + "<ariba-po-number/>"
                             + "<ariba-cost-center/>"
                             + "<ariba-ams-project-code/>"
                             + "<ariba-unspsc-code>empty</ariba-unspsc-code>"
                             + "<product-substitution-acknowlegement/>"
                             + "</item>"
                             + "<item>"
                             + "<order-number>FTA131245</order-number>"
                             + "<order-total>61.11</order-total>"
                             + "<tax-amount>4.13</tax-amount>"
                             + "<service-fee>9.99</service-fee>"
                             + "<extra-shipping-fee/>"
                             + "<drop-ship-charges/>"
                             + "<retail-variable-price>46.99</retail-variable-price>"
                             + "<productid>C13-3068</productid>"
                             + "<product-price>46.99</product-price>"
                             + "<color/>"
                             + "<size/>"
                             + "<add-ons/>"
                             + "<recip-first-name>robin</recip-first-name>"
                             + "<recip-last-name>bird</recip-last-name>"
                             + "<recip-address1>7205 Seminole Drive</recip-address1>"
                             + "<recip-address2/>"
                             + "<recip-city>Darien</recip-city>"
                             + "<recip-state>IL</recip-state>"
                             + "<recip-postal-code>60561</recip-postal-code>"
                             + "<recip-country>US</recip-country>"
                             + "<recip-international/>"
                             + "<recip-phone>5555555555</recip-phone>"
                             + "<recip-phone-ext/>"
                             + "<ship-to-type>R</ship-to-type>"
                             + "<ship-to-type-name/>"
                             + "<ship-to-type-info/>"
                             + "<occassion>2</occassion>"
                             + "<delivery-date>02/21/2004</delivery-date>"
                             + "<second-delivery-date/>"
                             + "<card-message>test</card-message>"
                             + "<card-signature>test</card-signature>"
                             + "<special-instructions/>"
                             + "<shipping-method/>"
                             + "<lmg-flag/>"
                             + "<lmg-email-address/>"
                             + "<lmg-email-signature/>"
                             + "<fol-indicator>FTD</fol-indicator>"
                             + "<sunday-delivery-flag/>"
                             + "<sender-release-flag/>"
                             + "<qms-result-code/>"
                             + "<qms-address1>7205 Seminole Drive</qms-address1>"
                             + "<qms-address2/>"
                             + "<qms-city>Darien</qms-city>"
                             + "<qms-state>IL</qms-state>"
                             + "<qms-postal-code>60561</qms-postal-code>"
                             + "<qms-firm-name/>"
                             + "<qms-latitude/>"
                             + "<qms-longtitude/>"
                             + "<qms-override-flag/>"
                             + "<qms-usps-range-record-type/>"
                             + "<ariba-po-number/>"
                             + "<ariba-cost-center/>"
                             + "<ariba-ams-project-code/>"
                             + "<ariba-unspsc-code>empty</ariba-unspsc-code>"
                             + "<product-substitution-acknowlegement/>"
                             + "</item>"
                             + "<item>"
                             + "<order-number>FTA131245</order-number>"
                             + "<order-total>61.11</order-total>"
                             + "<tax-amount>4.13</tax-amount>"
                             + "<service-fee>9.99</service-fee>"
                             + "<extra-shipping-fee/>"
                             + "<drop-ship-charges/>"
                             + "<retail-variable-price>46.99</retail-variable-price>"
                             + "<productid>C13-3068</productid>"
                             + "<product-price>46.99</product-price>"
                             + "<color/>"
                             + "<size/>"
                             + "<add-ons/>"
                             + "<recip-first-name>robin</recip-first-name>"
                             + "<recip-last-name>bird</recip-last-name>"
                             + "<recip-address1>7205 Seminole Drive</recip-address1>"
                             + "<recip-address2/>"
                             + "<recip-city>Darien</recip-city>"
                             + "<recip-state>IL</recip-state>"
                             + "<recip-postal-code>60561</recip-postal-code>"
                             + "<recip-country>US</recip-country>"
                             + "<recip-international/>"
                             + "<recip-phone>5555555555</recip-phone>"
                             + "<recip-phone-ext/>"
                             + "<ship-to-type>R</ship-to-type>"
                             + "<ship-to-type-name/>"
                             + "<ship-to-type-info/>"
                             + "<occassion>2</occassion>"
                             + "<delivery-date>02/21/2004</delivery-date>"
                             + "<second-delivery-date/>"
                             + "<card-message>test</card-message>"
                             + "<card-signature>test</card-signature>"
                             + "<special-instructions/>"
                             + "<shipping-method/>"
                             + "<lmg-flag/>"
                             + "<lmg-email-address/>"
                             + "<lmg-email-signature/>"
                             + "<fol-indicator>FTD</fol-indicator>"
                             + "<sunday-delivery-flag/>"
                             + "<sender-release-flag/>"
                             + "<qms-result-code/>"
                             + "<qms-address1>7205 Seminole Drive</qms-address1>"
                             + "<qms-address2/>"
                             + "<qms-city>Darien</qms-city>"
                             + "<qms-state>IL</qms-state>"
                             + "<qms-postal-code>60561</qms-postal-code>"
                             + "<qms-firm-name/>"
                             + "<qms-latitude/>"
                             + "<qms-longtitude/>"
                             + "<qms-override-flag/>"
                             + "<qms-usps-range-record-type/>"
                             + "<ariba-po-number/>"
                             + "<ariba-cost-center/>"
                             + "<ariba-ams-project-code/>"
                             + "<ariba-unspsc-code>empty</ariba-unspsc-code>"
                             + "<product-substitution-acknowlegement/>"
                             + "</item>"
                             + "<item>"
                             + "<order-number>FTA131245</order-number>"
                             + "<order-total>61.11</order-total>"
                             + "<tax-amount>4.13</tax-amount>"
                             + "<service-fee>9.99</service-fee>"
                             + "<extra-shipping-fee/>"
                             + "<drop-ship-charges/>"
                             + "<retail-variable-price>46.99</retail-variable-price>"
                             + "<productid>C13-3068</productid>"
                             + "<product-price>46.99</product-price>"
                             + "<color/>"
                             + "<size/>"
                             + "<add-ons/>"
                             + "<recip-first-name>robin</recip-first-name>"
                             + "<recip-last-name>bird</recip-last-name>"
                             + "<recip-address1>7205 Seminole Drive</recip-address1>"
                             + "<recip-address2/>"
                             + "<recip-city>Darien</recip-city>"
                             + "<recip-state>IL</recip-state>"
                             + "<recip-postal-code>60561</recip-postal-code>"
                             + "<recip-country>US</recip-country>"
                             + "<recip-international/>"
                             + "<recip-phone>5555555555</recip-phone>"
                             + "<recip-phone-ext/>"
                             + "<ship-to-type>R</ship-to-type>"
                             + "<ship-to-type-name/>"
                             + "<ship-to-type-info/>"
                             + "<occassion>2</occassion>"
                             + "<delivery-date>02/21/2004</delivery-date>"
                             + "<second-delivery-date/>"
                             + "<card-message>test</card-message>"
                             + "<card-signature>test</card-signature>"
                             + "<special-instructions/>"
                             + "<shipping-method/>"
                             + "<lmg-flag/>"
                             + "<lmg-email-address/>"
                             + "<lmg-email-signature/>"
                             + "<fol-indicator>FTD</fol-indicator>"
                             + "<sunday-delivery-flag/>"
                             + "<sender-release-flag/>"
                             + "<qms-result-code/>"
                             + "<qms-address1>7205 Seminole Drive</qms-address1>"
                             + "<qms-address2/>"
                             + "<qms-city>Darien</qms-city>"
                             + "<qms-state>IL</qms-state>"
                             + "<qms-postal-code>60561</qms-postal-code>"
                             + "<qms-firm-name/>"
                             + "<qms-latitude/>"
                             + "<qms-longtitude/>"
                             + "<qms-override-flag/>"
                             + "<qms-usps-range-record-type/>"
                             + "<ariba-po-number/>"
                             + "<ariba-cost-center/>"
                             + "<ariba-ams-project-code/>"
                             + "<ariba-unspsc-code>empty</ariba-unspsc-code>"
                             + "<product-substitution-acknowlegement/>"
                             + "</item>"
                             + "</items></order>";


  

  /**
   * Create a constructor that take a String parameter and passes it 
   * to the super class
   **/
  public DataAccessUtilTest(String name) {
      super(name);
  }

  /**
   * Override setup() to initialize variables
   **/   
  protected void setUp(){
  }

  /**
   * Override tearDown() to release any permanent resources you allocated in setUp 
   */
   public void tearDown(){
   }

   /**
    * Write the test case method in the fixture class. Be sure to make it public, 
    * or it can't be invoked through reflection
    **/    
    public void testCStmtReturnCRSet(){
      try  {
          DataAccessUtil util = DataAccessUtil.getInstance();
          DataRequest request = new DataRequest();
          DataSourceUtil dsUtil = DataSourceUtil.getInstance();

          Connection con = dsUtil.getConnection("AAS", "LOCALHOST");
      
          request.setConnection(con);
          HashMap inputParams = new HashMap();
          inputParams.put("context", "TEST"); 
          inputParams.put("identity", "CREYES");
          inputParams.put("credentials", "CREYES1");
          GUIDGenerator guidGenerator = GUIDGenerator.getInstance();
          String securityToken = guidGenerator.getGUID();
          inputParams.put("securityToken", securityToken);
          request.setInputParams(inputParams);
          request.setStatementID("authenticate_identity");

          Map outputParameters = (Map) util.execute(request);

          System.out.println(outputParameters.get("status")); 
          System.out.println(outputParameters.get("message"));

      } catch (Exception ex)  {
        ex.printStackTrace();
      } finally  {
      }
            
    }
  

   /**
    * Write the test case method in the fixture class. Be sure to make it public, 
    * or it can't be invoked through reflection
    **/    
    public void testCStmtReturnXMLDOM(){
      try  {
          DataAccessUtil util = DataAccessUtil.getInstance();
          DataRequest request = new DataRequest();
          DataSourceUtil dsUtil = DataSourceUtil.getInstance();

          Connection con = dsUtil.getConnection("AAS", "LOCALHOST");
      
          request.setConnection(con);
          HashMap inputParams = new HashMap();
          inputParams.put("context", "TEST"); 
          inputParams.put("identity", "CREYES");
          inputParams.put("credentials", "CREYES1");
          GUIDGenerator guidGenerator = GUIDGenerator.getInstance();
          String securityToken = guidGenerator.getGUID();
          inputParams.put("securityToken", securityToken);
          request.setInputParams(inputParams);
          request.setStatementID("authenticate_identity2");

          Document outParametersDoc = (Document) util.execute(request);
          DOMUtil.print(outParametersDoc, System.out);

      } catch (Exception ex)  {
        ex.printStackTrace();
      } finally  {
      }
            
    }
  
   /**
    * Write the test case method in the fixture class. Be sure to make it public, 
    * or it can't be invoked through reflection
    **/    
    public void testCStmtReturnXMLString(){
      try  {
          DataAccessUtil util = DataAccessUtil.getInstance();
          DataRequest request = new DataRequest();
          DataSourceUtil dsUtil = DataSourceUtil.getInstance();

          Connection con = dsUtil.getConnection("AAS", "LOCALHOST");
      
          request.setConnection(con);
          HashMap inputParams = new HashMap();
          inputParams.put("context", "TEST"); 
          inputParams.put("identity", "CREYES");
          inputParams.put("credentials", "CREYES1");
          GUIDGenerator guidGenerator = GUIDGenerator.getInstance();
          String securityToken = guidGenerator.getGUID();
          inputParams.put("securityToken", securityToken);
          request.setInputParams(inputParams);
          request.setStatementID("authenticate_identity3");

          String outParametersXMLString = (String) util.execute(request);
          System.out.println(outParametersXMLString);
      } catch (Exception ex)  {
        ex.printStackTrace();
      } finally  {
      }
            
    }


   /**
    * Write the test case method in the fixture class. Be sure to make it public, 
    * or it can't be invoked through reflection
    **/    
    public void testStateListLookup(){
      try  {
          DataAccessUtil util = DataAccessUtil.getInstance();
          DataRequest request = new DataRequest();
          DataSourceUtil dsUtil = DataSourceUtil.getInstance();

          Connection con = dsUtil.getConnection("OSP", "LOCALHOST");
      
          request.setConnection(con);
          HashMap inputParams = new HashMap();
          request.setStatementID("STATE_LIST_LOOKUP");

          Document outParametersDoc = (Document) util.execute(request);
          DOMUtil.print(outParametersDoc, System.out);

          Document newDoc = DOMUtil.getDocument();
          DOMUtil.addSection(newDoc, outParametersDoc.getChildNodes());
          DOMUtil.print(((Document)newDoc), System.out);
          
      } catch (Exception ex)  {
        ex.printStackTrace();
      } finally  {
      }
            
    }



   /**
    * Write the test case method in the fixture class. Be sure to make it public, 
    * or it can't be invoked through reflection
    **/    
    public void testXML(){
      Connection con = null;
      try  {
          DataAccessUtil util = DataAccessUtil.getInstance();
          DataRequest request = new DataRequest();

          Properties p = new Properties();
          p.put("user", "creyes");
          p.put("password", "creyes");
          Driver driver =  (Driver)Class.forName("oracle.jdbc.driver.OracleDriver").newInstance();
          con = driver.connect("jdbc:oracle:thin:@stheno-dev.ftdi.com:1521:dev5", p);
      
          request.setConnection(con);
          HashMap inputParams = new HashMap();
          inputParams.put("IN_XML", xmlData);
          inputParams.put("IN_XPATH","/order/header/buyer-first-name/text()");
          request.setStatementID("SCRUB.TEST_XML_CLOB");
          request.setInputParams(inputParams);
          
          Map output = (Map)util.execute(request);
          Clob clob = (Clob) output.get("OUT_DOCUMENT");
          DOMUtil.print(((Document)util.toXMLDocument(clob)), System.out);
          System.out.println(util.toXMLString(clob));
          System.out.println(output.get("OUT_XPATH_VALUE"));
          
      } catch (Exception ex)  {
        ex.printStackTrace();
      } finally  {
        try 
        {
          if (con != null && (!con.isClosed()))
          {
              con.close();
          }
          
        } catch (Exception ex) 
        {
          ex.printStackTrace();
        } finally 
        {
        }
        
      }
            
    }

   /**
    * Write the test case method in the fixture class. Be sure to make it public, 
    * or it can't be invoked through reflection
    **/    
    public void testLargeXML(){
      Connection con = null;
      try  {
          DataAccessUtil util = DataAccessUtil.getInstance();
          DataRequest request = new DataRequest();
          FileReader fr     = new FileReader("c:/temp/GoodOrder51Items.xml");
            
          BufferedReader br = new BufferedReader(fr);
          StringBuffer sb = new StringBuffer();
          String record = null;
          while ((record = br.readLine()) != null) {
             sb.append(record);
          }
          Properties p = new Properties();
          p.put("user", "osp");
          p.put("password", "osp");
          Driver driver =  (Driver)Class.forName("oracle.jdbc.driver.OracleDriver").newInstance();
          con = driver.connect("jdbc:oracle:thin:@stheno-dev.ftdi.com:1521:dev5", p);
      
          request.setConnection(con);
          HashMap inputParams = new HashMap();
          System.out.println(sb.length());
          inputParams.put("IN_CONTEXT", "AMAZON");
          inputParams.put("IN_ERROR","This is an error");
          inputParams.put("IN_XML",sb.toString());
          inputParams.put("IN_EVENT_NAME","ORDER-INFUSER");
          request.setStatementID("SCRUB.INSERT_AZ_ERROR");
          request.setInputParams(inputParams);
          
          Map output = (Map)util.execute(request);
      } catch (Exception ex)  {
        ex.printStackTrace();
      } finally  {
        try 
        {
          if (con != null && (!con.isClosed()))
          {
              con.close();
          }
          
        } catch (Exception ex) 
        {
          ex.printStackTrace();
        } finally 
        {
        }
        
      }
            
    }


  /**
   * To run several tests at once JUnit provides an object, TestSuite which runs
   * any number of test cases together. 
   * Each test is run as a separate instance of the test class
   * You make your suite accessible to a TestRunner tool with a static method 
   * suite that returns a test suite. 
   **/
  public static Test suite(){
    TestSuite suite = new TestSuite();
    // Create an instance of of Test class that will run this test case 
    // When the test is run, the name of the test is used to look up the method 
    // to run, using reflection
    //suite.addTest(new DataAccessUtilTest("testCStmtReturnCRSet"));
    //suite.addTest(new DataAccessUtilTest("testCStmtReturnXMLDOM"));
    //suite.addTest(new DataAccessUtilTest("testCStmtReturnXMLString"));
    suite.addTest(new DataAccessUtilTest("testLargeXML"));
    return suite;
  }

  /**
   * You can either user the text ui or swing ui to run the test
   **/
  public static void main(String[] args){
    // pass in the class of the test that needs to be run
    //junit.swingui.TestRunner.run(FrameworkTest.class);
    junit.textui.TestRunner.run(suite());
  }
  
  
}