package com.ftd.osp.utilities.tests;

import com.ftd.osp.utilities.ping.dao.HeartBeatDAO;

import java.sql.Connection;

import java.sql.DriverManager;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class HeartBeatTest
  extends TestCase
{
  /**
   * Create a constructor that take a String parameter and passes it 
   * to the super class
   **/
  public HeartBeatTest(String name) {
      super(name);
  }

  /**
   * Override setup() to initialize variables
   **/   
  protected void setUp(){
  }

  /**
   * Override tearDown() to release any permanent resources you allocated in setUp 
   */
   public void tearDown(){
   }
   
   public void testHeartBeatDAO() throws Exception
   {
      Connection conn = null;
     try{
        
        conn = getConnection();
        HeartBeatDAO heartBeatDAO = new HeartBeatDAO(conn);
        boolean flag = heartBeatDAO.checkDBStatus();
        if(flag)
          System.out.println("Database up and running");
        else
          System.out.println("Database is down");
          
        conn.close();
     }
     catch(Exception e)
     {  
        e.printStackTrace();
        if(conn != null)
        {
          conn.close();
        }
     }
     
   }
   
   
   /**
   * To run several tests at once JUnit provides an object, TestSuite which runs
   * any number of test cases together. 
   * Each test is run as a separate instance of the test class
   * You make your suite accessible to a TestRunner tool with a static method 
   * suite that returns a test suite. 
   **/
  public static Test suite(){
    TestSuite suite = new TestSuite();
    // Create an instance of of Test class that will run this test case 
    // When the test is run, the name of the test is used to look up the method 
    // to run, using reflection
    suite.addTest(new HeartBeatTest("testHeartBeatDAO"));
    
    return suite;
  }

   /**
   * You can either user the text ui or swing ui to run the test
   **/
  public static void main(String[] args){
    // pass in the class of the test that needs to be run
    //junit.swingui.TestRunner.run(FrameworkTest.class);
    junit.textui.TestRunner.run(suite());
  }
  
  
    private Connection getConnection() throws Exception {
      String driver = "oracle.jdbc.driver.OracleDriver";
      String database = "jdbc:oracle:thin:@adonis.ftdi.com:1521:DEV47";
      String user = "osp";
      String password = "osp";
    
      Class.forName(driver);
      Connection conn = DriverManager.getConnection(database, user, password);  
    
      return conn;
  }

}
