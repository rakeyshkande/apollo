/**
 * 
 */
package com.ftd.osp.utilities.tests;

import java.io.IOException;

import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ftd.osp.utilities.IdGeneratorUtil;
import com.ftd.osp.utilities.id.vo.IdRequestVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.security.exceptions.SecurityAdminException;


/** This is the Test servlet developed to test the SVS transaction Id utility classes.
 * 	As this is the servlet, if anyone wants to use the same, they have to add this class under any webapp  which is easy to deploy
 * (like security manager) and add the convenient servlet path and mapping under web.xml. 
 * Note: Package and logger category name also should be changed accordingly. 
 * @author smeka
 *
 */
public class SVSIDGeneratorServlet extends HttpServlet {
	private Logger logger = new Logger("com.ftd.osp.utilities.tests.SVSIDGeneratorServlet");
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		Connection connection =null;
		try {
			IdRequestVO idRequestVO = new IdRequestVO("APOLLO","SVS_TRANS_ID");		
			//connection = DataRequestHelper.getDBConnection();	
			connection = getConnection();
			String transactionId = new IdGeneratorUtil().generateId(idRequestVO, connection);	    	    	
	    	response.setContentType("text/html");
	    	PrintWriter out = response.getWriter();
	    	out.write("New Trasaction Id generated is : " + transactionId);
		} catch (Exception e){
			logger.error("Error caught in test servlet : " + e.getMessage());
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (Exception e) {
					logger.error("Error caught closing connection : " + e.getMessage());
					e.printStackTrace();
				}
			}
		}
	}
	
	/** As SVS transaction ID Util classes are developed as part of FIX-5_4_0, 
	 * Database SID is defined as Grover.
	 * @return
	 * @throws Exception
	 */
	private Connection getConnection() throws Exception {
		String driver_ = "oracle.jdbc.driver.OracleDriver";
		String database_ = "jdbc:oracle:thin:@adonis.ftdi.com:1521:grover";
		String user_ = "osp";
		String password_ = "osp";

		Class.forName(driver_);
		Connection conn = DriverManager.getConnection(database_, user_,	password_);
		return conn;
	}

}
