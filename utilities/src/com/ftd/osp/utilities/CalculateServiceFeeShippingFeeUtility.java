package com.ftd.osp.utilities;

import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.constant.CacheMgrConstants;
import com.ftd.osp.utilities.cacheMgr.handlers.CountryMasterHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.FuelSurchargeHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.GlobalParmHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.ProductMasterHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.ServiceFeeHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.ServiceFeeOverrideHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.ShippingKeyCostHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.ShippingKeyDetailHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.SourceMasterHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.ZipCodeHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.enums.DeliveryMethodEnum;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.CountryMasterVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.ProductMasterVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.ServiceFeeOverrideVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.ServiceFeeVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.SourceMasterVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.OrderCalculationVO;

import java.math.BigDecimal;

import java.util.Date;


public class CalculateServiceFeeShippingFeeUtility
{
    public CalculateServiceFeeShippingFeeUtility() {
    }

    protected static Logger logger  = new Logger("com.ftd.osp.utilities.CalculateServiceFeeShippingFeeUtility");
    int SCALE = 2;
    int ROUNDING_DOWN = BigDecimal.ROUND_DOWN;
    int ROUNDING_UP = BigDecimal.ROUND_HALF_UP;

    String PRICE_CALCULATION_CONTEXT = "PRICE_CALCULATION";
    String SDG_GLOBAL_SERVICE_FEE = "SDG_GLOBAL_SERVICE_FEE";
    String FTD_APPS_CONTEXT = "FTDAPPS_PARMS";
    String ALASKA_HAWAII_CHARGE = "SPECIAL_SVC_CHARGE";

    //PRODUCT TYPES
    private static final String PRODUCT_TYPE_FRESHCUT = "FRECUT";
    private static final String PRODUCT_TYPE_SAMEDAY_FRESHCUT = "SDFC";
    private static final String PRODUCT_TYPE_FLORAL = "FLORAL";
    private static final String PRODUCT_TYPE_SAMEDAY_GIFT = "SDG";
    private static final String PRODUCT_TYPE_SPECIALTY_GIFT = "SPEGFT";

    //SHIP TYPES
    private static final String FLORIST_DELIVERED = "FL";
    private static final String SATURDAY_DELIVERED = "SA";
    private static final String SAME_DAY_DELIVERED = "SD";

    public void calculateServiceShippingFee(OrderCalculationVO orderCalcVO) throws Exception {

        // set fee amounts to zero
        orderCalcVO.setServiceFee(new BigDecimal("0"));
        orderCalcVO.setShippingFee(new BigDecimal("0"));
        orderCalcVO.setAlaskaHawaiiFee(new BigDecimal("0"));
        orderCalcVO.setFuelSurcharge(new BigDecimal("0"));

        BigDecimal itemServiceFee = new BigDecimal(0);
        BigDecimal itemShippingFee = new BigDecimal(0);

        String sourceCode = orderCalcVO.getSourceCode();
        Date deliveryDate = orderCalcVO.getDeliveryDate();
        Date orderDate = orderCalcVO.getOrderDate();
        String zipCode = orderCalcVO.getZipCode();
        String productId = orderCalcVO.getProductId();
        String countryId = orderCalcVO.getCountryId();
        String snhId = orderCalcVO.getSnhId();
        String shipMethod = orderCalcVO.getShipMethod();
        String shippingKey = orderCalcVO.getShippingKey();
        String productType = orderCalcVO.getProductType();
        String orderOrigin = orderCalcVO.getOrderOrigin();
        BigDecimal productPrice = orderCalcVO.getProductPrice();
        boolean shipMethodCarrierFlag = false;
        GlobalParmHandler globalParamHandler = (GlobalParmHandler) CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_GLOBAL_PARM);

        // If service fee id is not passed in, get it from the source code cache
        if (snhId == null) {
            if(logger.isDebugEnabled()) {
                logger.debug("Retrieving snhId from cache");
            }
            SourceMasterHandler smh = (SourceMasterHandler) CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_SOURCE_MASTER);
            SourceMasterVO smVO = smh.getSourceCodeById(sourceCode);
            if (smVO != null) {
                snhId = smVO.getSnhId();
                if(logger.isDebugEnabled()) {
                    logger.debug("snhId: " + snhId);
                }
            }
        }

        // If shipping key or product type is not passed in, get it from the product master cache
        ProductMasterHandler pmh = (ProductMasterHandler)CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_PRODUCT_MASTER);
        String webProductId =  pmh.getWebProductIdByProductId(productId);
        if(logger.isDebugEnabled()) {
            logger.debug("webProductId: " + webProductId);
        }
        ProductMasterVO pmVO = pmh.getProductByWebProductId(webProductId);
        if (shippingKey == null || shippingKey.equals("")) {
            shippingKey = pmVO.getShippingKey();
        }
        if (productType == null || productType.equals("")) {
            productType = pmVO.getProductType();
        }
        if(logger.isDebugEnabled()) {
            logger.debug("shippingKey: " + shippingKey + " productType: " + productType);
        }

        if (productType.equalsIgnoreCase("SDG") && (shipMethod == null || shipMethod.equals("") || shipMethod.equals("FL"))) {
            shipMethod = "SD";
        }
        if(logger.isDebugEnabled()) {
            logger.debug("shipMethod: " + shipMethod + " productType: " + productType);
        }

        FuelSurchargeHandler fsh = (FuelSurchargeHandler) CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_FUEL_SURCHARGE);
        BigDecimal fuelSurcharge = new BigDecimal("0");
        if (fsh != null) {
            fuelSurcharge = fsh.getFuelSurcharge();
        }

        ServiceFeeHandler sfh = (ServiceFeeHandler) CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_SERVICE_FEE);
        ServiceFeeVO sfVO = new ServiceFeeVO();
        if (sfh != null) {
            sfVO = sfh.getServiceFeeById(snhId);
        }
        ServiceFeeOverrideHandler sfoh = (ServiceFeeOverrideHandler) CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_SERVICE_FEE_OVERRIDE);
        if (sfoh != null) {
            ServiceFeeOverrideVO sfoVO = sfoh.getServiceFeeOverrideById(snhId, deliveryDate);
            if (sfoVO != null) {
                if(logger.isDebugEnabled()) {
                    logger.debug("Found service fee override");
                }
                sfVO.setDomesticCharge(sfoVO.getDomesticCharge());
                sfVO.setInternationalCharge(sfoVO.getInternationalCharge());
                sfVO.setVendorCharge(sfoVO.getVendorCharge());
                sfVO.setVendorSatUpcharge(sfoVO.getVendorSatUpcharge());
            }
        } else if(logger.isWarnEnabled()){
            logger.warn("ServiceFeeOverrideHandler is null.");
        }

        BigDecimal specialServiceCharge = null;
        if(isRecipAlaskaHawaii(zipCode)){
            specialServiceCharge = getAlaskaHawaiiSpecialServiceFee(globalParamHandler);
        }

        DeliveryMethodEnum dm = pmVO.getDeliveryMethod();
        if(DeliveryMethodEnum.CARRIER.equals(dm) || DeliveryMethodEnum.BOTH.equals(dm)) {
            shipMethodCarrierFlag = true;
        }

        // Calculate service fee.
        if (productType.equals(PRODUCT_TYPE_SAMEDAY_GIFT) &&
           (shipMethod == null || shipMethod.equals("")
           || shipMethod.equals(FLORIST_DELIVERED)
           || shipMethod.equals(SAME_DAY_DELIVERED)))
        {
            //Florist delivered same day gift
            if(logger.isDebugEnabled()) {
                logger.debug("**** SDG APPLY SERVICE FEE");
                logger.debug("** Overriding source service fee with global parm serv fee for SDG product");
            }
            itemServiceFee = new BigDecimal(globalParamHandler.getGlobalParm("PRICE_CALCULATION", "SDG_GLOBAL_SERVICE_FEE"));

            if(logger.isDebugEnabled()) {
                logger.debug("** Cache loaded for global parms SDG service fee:" + itemServiceFee);
            }

        }
        else if (productType.equals(PRODUCT_TYPE_FRESHCUT)
              || productType.equals(PRODUCT_TYPE_SAMEDAY_FRESHCUT)
              || productType.equals(PRODUCT_TYPE_FLORAL))
        {
            if (shipMethod == null || shipMethod.equals("")
                 || shipMethod.equals(FLORIST_DELIVERED)
                 || shipMethod.equals(SAME_DAY_DELIVERED))
            {
                if (isRecipDomestic(countryId)) {
                      itemServiceFee = sfVO.getDomesticCharge();
                      if(logger.isDebugEnabled()) {
                        logger.debug("domestic service fee: " + itemServiceFee.toString());
                      }
                } else {
                      itemServiceFee = sfVO.getInternationalCharge();
                      if(logger.isDebugEnabled()) {
                        logger.debug("international service fee: " + itemServiceFee.toString());
                      }
                }
            }
            else
            {
                //Order is vendor delivered
                itemServiceFee = sfVO.getVendorCharge();

                //if saturday delivery, add additionial charge
                if(shipMethod != null && shipMethod.equals(SATURDAY_DELIVERED))
                {
                     itemServiceFee = itemServiceFee.add(sfVO.getVendorSatUpcharge());
                     if(logger.isDebugEnabled()) {
                        logger.debug("saturdayCharge: " + sfVO.getVendorSatUpcharge());
                     }
                }

                if (isRecipAlaskaHawaii(zipCode)) {
                    itemShippingFee = itemShippingFee.add(specialServiceCharge);
                    if(logger.isDebugEnabled()) {
                        logger.debug("Alaska/Hawaii charge: " + specialServiceCharge);
                    }
                    orderCalcVO.setAlaskaHawaiiFee(specialServiceCharge);
                }
            }
        }

        // Calculate shipping fee.
        // Shipping fees only apply to SAMEDAY GIFT and SPECIALTY GIFT items.
        if((productType.equals(PRODUCT_TYPE_SAMEDAY_GIFT) || productType.equals(PRODUCT_TYPE_SPECIALTY_GIFT)))
        {
            String shippingDetailId = null;
            ShippingKeyDetailHandler skd = (ShippingKeyDetailHandler)CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_SHIPPING_KEY_DETAIL);
            shippingDetailId = skd.getShippingDetailId(shippingKey, productPrice);
            if (shippingDetailId != null) {
                ShippingKeyCostHandler skc = (ShippingKeyCostHandler)CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_SHIPPING_KEY_COST);
                itemShippingFee = skc.getShippingCost(shippingDetailId, shipMethod);
                if(logger.isDebugEnabled()) {
                    logger.debug(shippingDetailId + " / " + shipMethod + " / " + itemShippingFee);
                }
            }

            /* If the order is being delivered to Hawaii or Alaska and extra charge
             * is added to the shipping cost.  This charge only applies to
             * specialty gifts.  */
            if(productType.equals(PRODUCT_TYPE_SPECIALTY_GIFT) && isRecipAlaskaHawaii(zipCode) && shipMethodCarrierFlag){
                itemShippingFee = itemShippingFee.add(specialServiceCharge);
                if(logger.isDebugEnabled()) {
                    logger.debug("AK/HI: " + specialServiceCharge);
                }
            }
        }

        // Redistribute servie fee and shipping fee for florist delivered SDG.
        if (productType.equals(PRODUCT_TYPE_SAMEDAY_GIFT) && shipMethod != null && shipMethod.equalsIgnoreCase(SAME_DAY_DELIVERED)) {
            itemServiceFee = new BigDecimal(globalParamHandler.getGlobalParm("PRICE_CALCULATION", "SDG_GLOBAL_SERVICE_FEE"));
            itemShippingFee = itemShippingFee.subtract(itemServiceFee);
        }

        // Fuel surcharge
        if (fuelSurcharge.floatValue() > 0) {
            if(logger.isDebugEnabled()) {
                logger.debug("productType: " + productType);
                logger.debug("shipMethod: " + shipMethod);
                logger.debug("orderOrigin: " + orderOrigin);
            }

             // for CSP orders, subtract fuel surcharge from service fee and add to shipping fee
             if (orderOrigin != null && orderOrigin.equalsIgnoreCase("CSPI")) {
                 if (shipMethod == null || shipMethod.equals(FLORIST_DELIVERED) || shipMethod.equals(SAME_DAY_DELIVERED)) {
                     if(logger.isDebugEnabled()) {
                        logger.info("subtracting fuel surcharge from service fee");
                     }
                     itemServiceFee = itemServiceFee.subtract(fuelSurcharge);
                     if (itemServiceFee.floatValue() < 0) {
                         itemServiceFee = new BigDecimal("0");
                     }
                     itemShippingFee = itemShippingFee.add(fuelSurcharge);
                 }
             } else {
                 if( productType.equals(PRODUCT_TYPE_FRESHCUT) ||
                     (productType.equals(PRODUCT_TYPE_SAMEDAY_FRESHCUT) &&
                         (shipMethod != null
                         && !shipMethod.equals(FLORIST_DELIVERED)
                         && !shipMethod.equals(SAME_DAY_DELIVERED))) ) {

                     itemServiceFee = itemServiceFee.add(fuelSurcharge);
                     if(logger.isDebugEnabled()) {
                        logger.debug("fuel surcharge added to service fee");
                     }
                 } else {
                     itemShippingFee = itemShippingFee.add(fuelSurcharge);
                     if(logger.isDebugEnabled()) {
                        logger.debug("fuel surcharge added to shipping fee");
                     }
                 }
             }
        }

        orderCalcVO.setFuelSurcharge(fuelSurcharge.setScale(SCALE, ROUNDING_UP));
        orderCalcVO.setServiceFee(itemServiceFee.setScale(SCALE, ROUNDING_UP));
        orderCalcVO.setShippingFee(itemShippingFee.setScale(SCALE, ROUNDING_UP));
    }

    private boolean isRecipAlaskaHawaii(String zipCode) throws Exception {
        ZipCodeHandler zch = (ZipCodeHandler) CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_ZIP_CODE);
        String recipientState = null;
        if (zch != null) {
            recipientState = zch.getZipCodeState(zipCode);
            //logger.info("recipientState: " + recipientState);
        }

        if(recipientState != null && (recipientState.equalsIgnoreCase("AK") || recipientState.equalsIgnoreCase("HI"))) {
            return true;
        }
        return false;
    }

    private boolean isRecipDomestic(String countryId) throws Exception {
        String countryType = null;
        if (countryId == null || countryId.equals("")) {
            countryId = "US";
        }
        CountryMasterHandler cmh = (CountryMasterHandler) CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_COUNTRY_MASTER);
        if (cmh != null) {
              CountryMasterVO cmVO = cmh.getCountryById(countryId);
              countryType = cmVO.getCountryType();
        }
        logger.debug("countryType: " + countryType);

        if (countryType == null || countryType.equalsIgnoreCase("D")) {
            return true;
        }
        return false;
    }


    private BigDecimal getAlaskaHawaiiSpecialServiceFee(GlobalParmHandler globalParamHandler) throws Exception {
        BigDecimal specialServiceFee = null;
        String tempGlobalParm = null;
        if (globalParamHandler != null) {
            tempGlobalParm = globalParamHandler.getGlobalParm(FTD_APPS_CONTEXT, ALASKA_HAWAII_CHARGE);
        }
        if (tempGlobalParm != null && !tempGlobalParm.equals("")) {
            try {
                Double alaskaHawaiiFee = Double.parseDouble(tempGlobalParm);
                specialServiceFee = new BigDecimal(alaskaHawaiiFee).setScale(SCALE, ROUNDING_UP);
                if(logger.isDebugEnabled()) {
                    logger.debug("Alaska/Hawaii charge: " + tempGlobalParm);
                }
            } catch (Exception e) {
                logger.error("Invalid Alaska/Hawaii fee: " + tempGlobalParm);
            }
        }
        return specialServiceFee;
    }


}