package com.ftd.osp.utilities.json;

import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.JAXPUtil;

import org.json.JSONObject;
import org.json.XML;

import org.w3c.dom.Document;

/**
 * Utility class to help manipulate and create JSON objects.
 *
 * @author Mike Kruger
 */

public class JSONUtil
{
    private static XML xml = new XML();

    /**
     * Converts an XML Document to a JSON string
     *
     * @return JSON string
     */
    public static String xmlToJSON(Document doc) throws Exception
    {
    	// restore empty text nodes in the XML string from "<node/>" to "<node></node>"
        JSONObject jsonObj = xml.toJSONObject(DOMUtil.expandEmptyTextNode(JAXPUtil.toString(doc)));
        return jsonObj.toString();
    }
}