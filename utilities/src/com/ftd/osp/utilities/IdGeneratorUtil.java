/**
 * 
 */
package com.ftd.osp.utilities;

import java.sql.Connection;

import com.ftd.osp.utilities.id.IdGeneratorFactory;
import com.ftd.osp.utilities.id.vo.IdRequestVO;

/**
 * @author smeka
 * 
 */
public class IdGeneratorUtil {
	
	/** A utility method to generate an Sequence ID based on the request type of IDRequestVO
	 * @param idRequestVO
	 * @param conn
	 * @return
	 */
	public String generateId(IdRequestVO idRequestVO, Connection conn) throws Exception {
		return (new IdGeneratorFactory().getGenerator(idRequestVO)).execute(idRequestVO, conn);
	}
}
