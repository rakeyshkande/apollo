package com.ftd.osp.utilities;

import com.ftd.osp.utilities.plugins.Logger;
import java.util.Map;

public class LanguageUtility
{
  
  private static final Logger logger = new Logger("com.ftd.osp.utilities.LanguageUtility");
  
  public LanguageUtility()
  {
  }
  
  public static String specialCharacterMapping(String str, Map spcmap) throws Exception
  {                
    for (Object key: spcmap.keySet()) 
    {                          
      str = str.replaceAll((String)key,(String)spcmap.get(key));           
    }
    logger.debug("#####LanguageUtility  MAPPED STRING######"+ str);
    
    return str;
  }
  
}
