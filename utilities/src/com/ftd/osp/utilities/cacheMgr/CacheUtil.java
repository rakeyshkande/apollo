package com.ftd.osp.utilities.cacheMgr;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.xml.transform.Templates;

import com.ftd.osp.utilities.cacheMgr.constant.CacheMgrConstants;
import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.cacheMgr.handlers.AddOnHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.CSZAvailHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.CompanyMasterHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.ContentHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.CountryMasterHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.DeliveryDateRangeHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.FuelSurchargeHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.GlobalParmHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.IOTWHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.IndexFilterIndexHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.IndexSourceTemplateHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.PaymentMethodMPHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.PriceHeaderHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.ProductAttrHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.ProductColorHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.ProductCompanyHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.ProductCountryHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.ProductKeywordHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.ProductMasterHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.ProductSourceHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.ServiceClientHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.ServiceClientXSLHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.ServiceFeeHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.ServiceFeeOverrideHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.ShippingKeyCostHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.ShippingKeyDetailHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.SourceMasterHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.SourceMasterHandlerImpl;
import com.ftd.osp.utilities.cacheMgr.handlers.StateCompanyTaxHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.StateMasterHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.TransientCacheHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.UpsellHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.ZipCodeHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.AccountProgramMasterVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.AddonVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.ClientVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.ColorMasterVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.CompanyMasterVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.CountryMasterVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.IOTWVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.PaymentMethodMilesPointsVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.PriceHeaderDetailVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.ProductAttrRestrVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.ProductMasterVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.ServiceFeeOverrideVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.ServiceFeeVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.SourceMasterVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.StateMasterVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.TransientOccasionVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.TransientProductVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.UpsellDetailVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.UpsellMasterVO;
import com.ftd.osp.utilities.order.vo.TaxVO;
import com.ftd.osp.utilities.plugins.Logger;


public class CacheUtil {
    private static Logger logger  = new Logger("com.ftd.osp.utilities.cacheMgr.CacheUtil");
    private static CacheUtil cacheUtil = null;
    
    private CacheUtil() {

    }
    
    public static CacheUtil getInstance() {
        if (cacheUtil == null) {
            cacheUtil = new CacheUtil();
        }
        return cacheUtil;
    }

    public SourceMasterVO getSourceCodeById(String sourceCode) throws Exception {
        if(logger.isDebugEnabled())
          logger.debug("getSourceCodeById(" + sourceCode + ")");
        SourceMasterVO obj = null;

        SourceMasterHandler h = (SourceMasterHandler)CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_SOURCE_MASTER);
        obj =  h.getSourceCodeById(sourceCode.toUpperCase());
        return obj;
    }
    
    public List<String> getAllSources() throws Exception {
        if(logger.isDebugEnabled())
          logger.debug("getAllSources");
        List<String> obj = null;

        SourceMasterHandlerImpl h = (SourceMasterHandlerImpl)CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_SOURCE_MASTER);
        obj =  h.getAllSources();
        return obj;
    }

    public ClientVO getClientById(String clientId) throws Exception {
        if(logger.isDebugEnabled())
          logger.debug("getClientById(" + clientId + ")");
        ClientVO obj = null;

        ServiceClientHandler h = (ServiceClientHandler)CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_ORDER_SERVICE_CLIENT);
        obj =  h.getClientById(clientId);
        return obj;
    }
    
    public List getClientList() throws CacheException, Exception {
        List l = null;

        ServiceClientHandler h = (ServiceClientHandler)CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_ORDER_SERVICE_CLIENT);
        l =  h.getClientList();

        return l;

    }

    public List getCountryList() throws CacheException, Exception {
         List l = null;

         CountryMasterHandler h = (CountryMasterHandler)CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_COUNTRY_MASTER);
         l =  h.getCountryList();
         return l;
    }
    public CountryMasterVO getCountryById(String countryId) throws CacheException, Exception {
        if(logger.isDebugEnabled())
          logger.debug("getCountryById(" + countryId + ")");
         CountryMasterVO obj = null;

         CountryMasterHandler h = (CountryMasterHandler)CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_COUNTRY_MASTER);
         obj =  h.getCountryById(countryId.toUpperCase());
         return obj;
    }    

    public ProductMasterVO getProductByWebProductId(String webProductId) throws Exception {
        if(logger.isDebugEnabled())
          logger.debug("getProductByWebProductId(" + webProductId + ")");
        ProductMasterVO obj = null;

        ProductMasterHandler h = (ProductMasterHandler)CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_PRODUCT_MASTER);
        if(logger.isDebugEnabled())
        {
          logger.debug("handler null?" + (h==null? "Y" : "N"));
          logger.debug("webProductId:" + webProductId);
        }
        obj =  h.getProductByWebProductId(webProductId.toUpperCase());
        return obj;
    }
    
    public ProductMasterVO getProductById(String productId) throws Exception {
        if(logger.isDebugEnabled())
          logger.debug("getProductById(" + productId + ")");
        ProductMasterVO obj = null;

        ProductMasterHandler h = (ProductMasterHandler)CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_PRODUCT_MASTER);
        if(logger.isDebugEnabled())
        {
          logger.debug("handler null?" + (h==null? "Y" : "N"));
          logger.debug("productId:" + productId);
        }
        obj =  h.getProductById(productId.toUpperCase());
        return obj;
    }    

    public String getProductIdByWebProductId(String webProductId) throws Exception {
        if(logger.isDebugEnabled())
          logger.debug("getProductIdByWebProductId(" + webProductId + ")");
        String obj = null;

        ProductMasterHandler h = (ProductMasterHandler)CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_PRODUCT_MASTER);
        obj =  h.getProductIdByWebProductId(webProductId.toUpperCase());
        return obj;
    }

    public String getWebProductIdByProductId(String productId) throws Exception {
        if(logger.isDebugEnabled())
          logger.debug("getWebProductIdByProductId(" + productId + ")");
        String obj = null;

        ProductMasterHandler h = (ProductMasterHandler)CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_PRODUCT_MASTER);
        obj =  h.getWebProductIdByProductId(productId.toUpperCase());
        return obj;
    }

    public List<String> getAllProductIds() throws Exception {
        if(logger.isDebugEnabled())
          logger.debug("getAllProductIds");
        List<String> obj = null;

        ProductMasterHandler h = (ProductMasterHandler)CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_PRODUCT_MASTER);
        obj =  h.getAllProductIds();
        return obj;
    }    
    

    public CompanyMasterVO getCompanyById(String companyId) throws Exception {
        if(logger.isDebugEnabled())
          logger.debug("getCompanyById(" + companyId + ")");
        CompanyMasterVO obj = null;

        CompanyMasterHandler h = (CompanyMasterHandler)CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_COMPANY_MASTER);
        obj =  h.getCompanyById(companyId);
        return obj;
    }
    
    public List<CompanyMasterVO> getProductCompany(String productId) throws Exception {
        if(logger.isDebugEnabled())
          logger.debug("getProductCompany(" +  productId + ")");
        List<CompanyMasterVO> obj = null;

        ProductCompanyHandler h = (ProductCompanyHandler)CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_PRODUCT_COMPANY);
        obj =  h.getProductCompany(productId.toUpperCase());
        return obj;
    }
    
    public List<ColorMasterVO> getProductColor(String productId) throws Exception {
        if(logger.isDebugEnabled())
          logger.debug("getProductColor(" + productId + ")");
        List<ColorMasterVO> obj = null;

        ProductColorHandler h = (ProductColorHandler)CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_PRODUCT_COLOR);
        obj =  h.getProductColor(productId.toUpperCase());
        return obj;
    }
    
    public List<CountryMasterVO> getProductCountry(String productId) throws Exception {
        if(logger.isDebugEnabled())
          logger.debug("getProductCountry(" + productId + ")");
        List<CountryMasterVO> obj = null;

        ProductCountryHandler h = (ProductCountryHandler)CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_PRODUCT_COUNTRY);
        obj =  h.getProductCountry(productId.toUpperCase());
        return obj;
    }    

    public List<String> getProductKeyword(String productId) throws Exception {
        if(logger.isDebugEnabled())
          logger.debug("getProductKeyword(" + productId + ")");
        List<String> obj = null;

        ProductKeywordHandler h = (ProductKeywordHandler)CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_PRODUCT_KEYWORD);
        obj =  h.getProductKeyword(productId.toUpperCase());
        return obj;
    }    
    
    public String getUpsellMasterIdByDetailId(String upsellDetailId) throws Exception {
        if(logger.isDebugEnabled())
          logger.debug("getUpsellMasterIdByDetailId(" + upsellDetailId + ")");
        String obj = null;

        UpsellHandler h = (UpsellHandler)CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_UPSELL);
        obj =  h.getUpsellMasterIdByDetailId(upsellDetailId.toUpperCase());
        return obj;
    }    
    
    public UpsellMasterVO getUpsellMasterByMasterId(String upsellMasterId) throws Exception {
        if(logger.isDebugEnabled())
          logger.debug("getUpsellMasterByMasterId(" + upsellMasterId + ")");
        UpsellMasterVO obj = null;

        UpsellHandler h = (UpsellHandler)CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_UPSELL);
        obj =  h.getUpsellMasterById(upsellMasterId.toUpperCase());
        return obj;
    }
    
    public ProductMasterVO getBaseProductByUpsellMasterId(String upsellMasterId) throws Exception {
        if(logger.isDebugEnabled())
          logger.debug("getUpsellMasterByMasterId(" + upsellMasterId + ")");
        UpsellMasterVO obj = getUpsellMasterByMasterId(upsellMasterId);
        ProductMasterVO pmvo = null;
        String productId = null;
        
        if(obj != null) {
            List<UpsellDetailVO> upsellDetailList = obj.getUpsellDetailList();   
            for (int i = 0; i < upsellDetailList.size(); i++) {
                UpsellDetailVO udvo = upsellDetailList.get(i);
                
                if(logger.isDebugEnabled()) {
                    logger.debug("upselldetailid:" + udvo.getUpsellDetailId());
                    logger.debug("upsell detail sequence:" + udvo.getUpsellDetailSequenceNum());
                }
                if("1".equals(udvo.getUpsellDetailSequenceNum())) {
                    productId = udvo.getUpsellDetailId();
                    break;
                }
            }
            if(logger.isDebugEnabled())
                logger.debug("product id is:" + productId);
            if(productId != null) {
                pmvo = this.getProductById(productId);
                pmvo.setUpsellMasterVO(obj);
            }
        } else {
            logger.error("not found upsell master:" + upsellMasterId);
        }
        return pmvo;
    }   
    
    /**
     * Retrieves the 
     * @param webProductId
     * @return
     */
    public ProductMasterVO getProductOrBaseProductByWebProductId(String webProductId) throws Exception {
        ProductMasterVO pmvo = this.getProductByWebProductId(webProductId);
        if(pmvo == null) {
            pmvo = this.getBaseProductByUpsellMasterId(webProductId);
        }
        return pmvo;
    }
    
    public ServiceFeeVO getServiceFeeById(String snhId) throws Exception {
        if(logger.isDebugEnabled())
          logger.debug("getServiceFeeById(" + snhId + ")");
        ServiceFeeVO obj = null;

        ServiceFeeHandler h = (ServiceFeeHandler)CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_SERVICE_FEE);
        obj =  h.getServiceFeeById(snhId);
        return obj;
    }

    public BigDecimal getShippingCost(String shippingKeyId, String shipMethod, BigDecimal price) throws Exception {
        if(logger.isDebugEnabled())
          logger.debug("getShippingCost(" + shippingKeyId + "," + shipMethod + "," + price.toString() + ")");
        BigDecimal shippingCost = null;
        String shippingDetailId = null;
        ShippingKeyDetailHandler skd = (ShippingKeyDetailHandler)CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_SHIPPING_KEY_DETAIL);
        shippingDetailId = skd.getShippingDetailId(shippingKeyId, price);
        if (shippingDetailId != null) {
            ShippingKeyCostHandler skc = (ShippingKeyCostHandler)CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_SHIPPING_KEY_COST);
            shippingCost = skc.getShippingCost(shippingDetailId, shipMethod);
        }
        return shippingCost;
    }

    public PriceHeaderDetailVO getPriceHeaderDetail(String priceHeaderId, BigDecimal price) throws Exception {
        if(logger.isDebugEnabled())
          logger.debug("getPriceHeaderDetail(" + priceHeaderId + "," + price.toString() + ")");
        PriceHeaderDetailVO obj = null;

        PriceHeaderHandler h = (PriceHeaderHandler)CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_PRICE_HEADER_DETAIL);
        obj =  h.getPriceHeaderDetail(priceHeaderId, price);
        return obj;
    }     
    
    public List<String> getProductAttrRestrBySource(String sourceCode) throws Exception {
        if(logger.isDebugEnabled())
          logger.debug("getProductAttrRestrBySource(" + sourceCode + ")");
        List<String> obj = null;

        ProductAttrHandler h = (ProductAttrHandler)CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_PRODUCT_ATTR_RESTR);
        obj =  h.getProductAttrRestrBySource(sourceCode.toUpperCase());
        return obj;
    }    
    
    public ProductAttrRestrVO getProductAttrRestrById(String id) throws Exception {
        if(logger.isDebugEnabled())
          logger.debug("getProductAttrRestrById(" + id + ")");
        ProductAttrRestrVO obj = null;

        ProductAttrHandler h = (ProductAttrHandler)CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_PRODUCT_ATTR_RESTR);
        obj =  h.getProductAttrRestrById(id);
        return obj;
    }         
    
    public boolean zipCodeExists(String zipCode) throws Exception {
        if(logger.isDebugEnabled())
          logger.debug("zipCodeExists(" + zipCode + ")");
        boolean exists = false;

        ZipCodeHandler h = (ZipCodeHandler)CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_ZIP_CODE);
        exists =  h.zipCodeExists(zipCode.toUpperCase());
        if(logger.isDebugEnabled())
          logger.debug("Returning:" + exists);
        return exists;
    }      
    
    public BigDecimal getFuelSurcharge() throws Exception {
        BigDecimal fuelSurcharge = null;
        FuelSurchargeHandler h = (FuelSurchargeHandler)CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_FUEL_SURCHARGE);
        fuelSurcharge =  h.getFuelSurcharge();
        return fuelSurcharge;
    }    
    
    public String getContentWithFilter(String context, String name, String filter1, String filter2, boolean missingContentOk ) throws Exception {
        if(logger.isDebugEnabled())
          logger.debug("getContentWithFilter(" + context + "," + name + "," + filter1 + "," + filter2 + ")");
        String contentTxt = null;

        ContentHandler h = (ContentHandler)CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_CONTENT_WITH_FILTER);
        contentTxt =  h.getContentWithFilter(context, name, filter1, filter2, missingContentOk);
        return contentTxt;
    }      
    public String getGlobalParm(String context, String name) throws Exception {
        if(logger.isDebugEnabled())
          logger.debug("getGlobalParm(" + context + "," + name + ")");
        GlobalParmHandler h = (GlobalParmHandler)CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_GLOBAL_PARM);
        String value =  h.getGlobalParm(context, name);
        return value;
    }  
    
    public String getXSLByClientAction(String clientId, String actionType) throws Exception {
        if(logger.isDebugEnabled())
          logger.debug("getXSLByClientAction(" + clientId + "," + actionType + ")");
        ServiceClientXSLHandler h = (ServiceClientXSLHandler)CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_ORDER_SERVICE_CLIENT_XSL);
        String xslCode =  h.getXSLCodeByClientAction(clientId, actionType);
        String value = h.getXSLById(xslCode);
        return value;
    }
    
    public Templates getXSLTemplateByClientAction(String clientId, String actionType) throws Exception {
        if(logger.isDebugEnabled())
          logger.debug("getXSLTemplateByClientAction(" + clientId + "," + actionType + ")");
        ServiceClientXSLHandler h = (ServiceClientXSLHandler)CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_ORDER_SERVICE_CLIENT_XSL);
        String xslCode =  h.getXSLCodeByClientAction(clientId, actionType);
        Templates value = h.getXSLTemplateById(xslCode);
        return value;
    }
    
    public IOTWVO getIOTW(String sourceCode, String productId, String upsellMasterId) throws Exception {
        if(logger.isDebugEnabled())
          logger.debug("getIOTW(" + sourceCode + "," + productId + "," + upsellMasterId + ")");
        IOTWHandler h = (IOTWHandler)CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_IOTW);
        IOTWVO value = h.getIOTW(sourceCode, productId, upsellMasterId);
        return value;
    }
    
    public List<String> getProductSource(String productId) throws Exception {
        if(logger.isDebugEnabled())
          logger.debug("getProductSource(" + productId + ")");
        ProductSourceHandler h = (ProductSourceHandler)CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_PRODUCT_SOURCE);
        List<String> value = h.getProductSource(productId.toUpperCase());
        return value;
    }
    
    public String getProductTransient(String clientId, String sourceCode, String[] productIds, String loadLevel) throws Exception {
        if(logger.isDebugEnabled())
          logger.debug("getProductTransient(" + clientId + "," + sourceCode + "," + loadLevel + ")");
        TransientProductVO key = new TransientProductVO(clientId, sourceCode, productIds, loadLevel);
        TransientCacheHandler h = (TransientCacheHandler)CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_GET_PRODUCT);
        
        Object value =  h.getCacheElement(key); 
        String s = null;
        
        if(value != null) {
            s = (String) value;
        }
        return s;
    }
    public void setProductTransient(String clientId, String sourceCode, String[] productIds, String loadLevel, String value) throws Exception {
        TransientCacheHandler h = (TransientCacheHandler)CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_GET_PRODUCT);
        TransientProductVO key = new TransientProductVO(clientId, sourceCode, productIds, loadLevel);
        
        h.addCacheElement(key, value); 
    }   
    
    public String getOccasionTransient(String clientId, String sourceCode) throws Exception {
        if(logger.isDebugEnabled())
          logger.debug("getOccasionTransient(" + clientId + "," + sourceCode + ")");
        TransientOccasionVO key = new TransientOccasionVO(clientId, sourceCode);
        TransientCacheHandler h = (TransientCacheHandler)CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_GET_OCCASION);
        
        Object value =  h.getCacheElement(key); 
        String s = null;
        
        if(value != null) {
            s = (String) value;
        }
        return s;
    }    
    
    public void setOccasionTransient(String clientId, String sourceCode, String value) throws Exception {
        TransientCacheHandler h = (TransientCacheHandler)CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_GET_OCCASION);
        TransientOccasionVO key = new TransientOccasionVO(clientId, sourceCode);
        h.addCacheElement(key, value); 
    }   

    public String getCSZAvailGnaddFlag(String zipCode) throws Exception {
        if(logger.isDebugEnabled())
          logger.debug("getCSZAvailGnaddFlag(" + zipCode + ")");
        CSZAvailHandler h = (CSZAvailHandler)CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_CSZ_AVAIL);
        String value =  h.getGnaddFlag(zipCode);
        return value;
    }
    
    public String getZipCodeState(String zipCode) throws Exception {
        if(logger.isDebugEnabled())
          logger.debug("getZipCodeState(" + zipCode + ")");
        ZipCodeHandler zch = (ZipCodeHandler) CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_ZIP_CODE);
        String stateId = zch.getZipCodeState(zipCode);
        return stateId;
    }
    
    public StateMasterVO getStateById(String stateMasterId) throws Exception {
        if(logger.isDebugEnabled())
          logger.debug("getStateById(" + stateMasterId + ")");
        StateMasterHandler smh = (StateMasterHandler) CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_STATE_MASTER);
        StateMasterVO stateVO = smh.getStateById(stateMasterId);
        return stateVO;
    }
    
    public Date isDateInDeliveryDateRange(Date inDate) throws Exception {
        DeliveryDateRangeHandler ddrh = (DeliveryDateRangeHandler)CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_DELIVERY_DATE_RANGE);
        Date endDate = ddrh.isDateInDeliveryDateRange(inDate);
        return endDate;
    }

    public Map getProductAttrRestrMap() throws Exception {
        ProductAttrHandler pah = (ProductAttrHandler)CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_PRODUCT_ATTR_RESTR);
        Map productAttrRestMap = pah.getProductAttrRestrMap();
        return productAttrRestMap;
    }
    
    public String getSourceCodeDefaultDomain(String sourceCode) throws Exception {
        SourceMasterVO sourceMasterVO = this.getSourceCodeById(sourceCode);
        String companyId = "FTD";
        
        if(sourceMasterVO != null) {
            companyId = sourceMasterVO.getCompanyId();
        }
        
        CompanyMasterVO companyMasterVO = this.getCompanyById(companyId);
        return companyMasterVO.getDefaultDomain();
    }
    
    public List<String> getSourceTemplates(String sourceCode) throws Exception {
        IndexSourceTemplateHandler ith = (IndexSourceTemplateHandler)CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_INDEX_SOURCE_TEMPLATE);
        List<String> templateList  = ith.getSourceTemplates(sourceCode);
        return templateList;
    }    

    public String getFilterIndexProducts(String templateId, String indexName) throws Exception {
        IndexFilterIndexHandler ifh = (IndexFilterIndexHandler)CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_INDEX_FILTER_INDEX);
        String indexProducts = ifh.getFilterIndexProducts(templateId, indexName);
        return indexProducts;
    }    
    
    public String getTemplateIdByDomain(String domain) throws Exception {
        IndexSourceTemplateHandler ith = (IndexSourceTemplateHandler)CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_INDEX_SOURCE_TEMPLATE);
        String templateId  = ith.getTemplateIdByDomain(domain);
        return templateId;
    }
    
    public AddonVO getAddOnById(String addOnId) throws Exception {
        if(logger.isDebugEnabled())
          logger.debug("getAddOnById(" + addOnId + ")");
        AddonVO obj = null;

        AddOnHandler h = (AddOnHandler)CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_ADDON);
        obj =  h.getAddOnById(addOnId);
        return obj;
    }
    
    public PaymentMethodMilesPointsVO getPaymentMethodMPById(String paymentMethodId) throws Exception {
        if(logger.isDebugEnabled())
          logger.debug("getPaymentMethodMPById(" + paymentMethodId + ")");
        PaymentMethodMilesPointsVO obj = null;

        PaymentMethodMPHandler h = (PaymentMethodMPHandler)CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_PAYMENT_METHOD_MP);
        obj =  h.getPaymentMethodMPById(paymentMethodId);
        return obj;
    }
    
    public ServiceFeeOverrideVO getServiceFeeOverrideById(String snhId, Date deliveryDate) throws Exception {
        if(logger.isDebugEnabled())
          logger.debug("getServiceFeeOverrideById(" + snhId + ")");
        ServiceFeeOverrideVO obj = null;

        ServiceFeeOverrideHandler h = (ServiceFeeOverrideHandler)CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_SERVICE_FEE_OVERRIDE);
        obj =  h.getServiceFeeOverrideById(snhId, deliveryDate);
        return obj;
    }
    
    public AccountProgramMasterVO getFSAccountProgramDetailsFromContent() throws Exception{
    	AccountProgramMasterVO fsProgramMasterVO = new AccountProgramMasterVO();
    	ContentHandler h = (ContentHandler)CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_CONTENT_WITH_FILTER);
    	fsProgramMasterVO.setProgramName(h.getContentWithFilter("SERVICES", "PROGRAM_NAME", "FREESHIP", null, false));
    	fsProgramMasterVO.setProgramDescription(h.getContentWithFilter("SERVICES", "PROGRAM_DESCRIPTION", "FREESHIP", null, false));
    	fsProgramMasterVO.setProgramUrl(h.getContentWithFilter("SERVICES", "PROGRAM_URL", "FREESHIP", null, false));
    	fsProgramMasterVO.setDisplayName(h.getContentWithFilter("SERVICES", "PROGRAM_DISPLAY_NAME", "FREESHIP", null, false));
    	return fsProgramMasterVO;
    }
    
	public boolean isStateCompanyTaxON(String stateCode, String companyId) {

		if (logger.isDebugEnabled()) {
			logger.debug("isStateCompanyTaxON(state: " + stateCode + ", company: " + companyId + ")");
		}
	
		StateCompanyTaxHandler h = (StateCompanyTaxHandler) CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_STATE_COMPANY_TAX);
		return h.isStateCompanyTaxON(stateCode, companyId); 
	}
    
}    