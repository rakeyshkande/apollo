package com.ftd.osp.utilities.cacheMgr;

import com.ftd.osp.utilities.cacheMgr.CacheAdminTimer;
import com.ftd.osp.utilities.cacheMgr.FixedCacheRefreshTimer;
import com.ftd.osp.utilities.cacheMgr.TransientCacheExpirationTimer;
import com.ftd.osp.utilities.cacheMgr.vo.CacheVO;
import com.ftd.osp.utilities.cacheMgr.vo.CacheHandlerBase;
import com.ftd.osp.utilities.cacheMgr.vo.TransientCacheVO;
import com.ftd.osp.utilities.cacheMgr.vo.TransientCacheElementVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.util.DataAccessEntityResolver;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.j2ee.ResourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.net.URLDecoder;
import java.net.InetAddress;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.io.FileUtils;

public class CacheManager
{
    public static final String FIXED_CACHE_TYPE = "Fixed";
    public static final String TRANSIENT_CACHE_TYPE = "Transient";
    public static final String DATE_FORMAT     = "yyyy-MM-dd kk:mm:ss,SSS";
    public static final String DATE_FORMAT_LOG = "MMM dd kk:mm";
//  public static final String CACHE_STATS_LOG_HEADER    = "cstat--12345678901234567890   1  123456  123456     1234   123456789012  123456789012 123456 123456";
    public static final String CACHE_STATS_LOG_FORMAT = "cstat--%20.20s   %1.1s  %6d  %6d     %3d%s   %12.12s  %12.12s %6d %6d\n";
    public static final String CACHE_STATS_LOG_HEADER = "cstat--        CACHE       CACHE  ACCESS  REFRESH REF/EXP  LAST          FIRST         TRANS ELEM & \n" + 
                                                           "cstat--        NAME         TYPE  COUNT   COUNT   RATE     REFRESH       LOAD          ARRAY COUNTS\n";
    public static final String CACHE_STATS_LOG_PREFIX = "cstat--";

    private static final String CONFIG_FILE = "cacheMgrConfig.xml";
    private static final String CONFIG_DIR = "/u02/config/cache/";
    private static final long MIN_ADMIN_RATE   = 2;
    private static final long MIN_REFRESH_RATE = 5;
    private static final long MIN_EXPIRE_RATE  = 1;
    private static final long REFRESH_NOW = 10000;  // Delay 15 sec before spawning threads
    private static Logger logger = new Logger("com.ftd.osp.utilities.cacheMgr.CacheManager");
    private static final String MSG_TYPE_EXCEPTION = "EXCEPTION";
    private static final String MSG_SOURCE = "CACHE CONTROLLER";
    private static final String CACHE_DATASOURCE = "MySql";
    private static CacheManager CACHEMANAGER = new CacheManager();
    private static boolean SINGLETON_INITIALIZED;

    private static File         cacheConfigFile;    // Handle to cacheconfig.xml file
    private static String       dataSource;
    private static String       globalName;         // Global name (representing cacheconfig for this JVM)
    private static long        cacheAdminRate;     // Rate at which cache admin thread runs
    private static Timer        adminTimer;         // Handle to admin thread
    private static boolean     terminateThreads;   // Threads will kill themselves if true
    private static boolean     spawnRefreshThread; // Set when Fixed cache is lazy loaded so Admin thread will spawn Refresh thread
    private static Map<String,CacheVO> cacheMap;    // Actual cache 

  
  /**
   * The private constructor
   */
  private CacheManager()
  {
    super();
  }

 /**
  * The static initializer for the CacheManager. It ensures that
  * at a given time there is only a single instance
  *
  * @return the CacheManager
  **/  
  public static CacheManager getInstance()
  {          
    if (! SINGLETON_INITIALIZED )
    {
      initCache();
    }
    return CACHEMANAGER;
  }


  /**
   * Initialize Cache Manager 
   */
  private synchronized static void initCache()
  {
    Connection conn = null;
    StringWriter stringWriter = null;
    PrintWriter printWriter = null; 
    CacheHandlerBase cacheHandlerBase = null;
    Class cacheHandlerBaseClass = null;
    
    if (! SINGLETON_INITIALIZED)
    {
      try 
      {
        if(logger.isInfoEnabled())
          logger.info("Initializing CacheManager singleton");
        cacheMap = Collections.synchronizedMap(new HashMap<String,CacheVO>());
        loadCacheConfig();
        spawnAdminTimer();
        SINGLETON_INITIALIZED = true;
        
         if(logger.isInfoEnabled())
          logger.info("CacheManager singleton initialized");
          
      } catch (Exception ex) 
      {
        // In this state, we may have a partially initialized cache manager.
        logger.error(ex);
        try
        {
            conn = DataSourceUtil.getInstance().getConnection(CACHE_DATASOURCE);
            SystemMessengerVO sysVo = new SystemMessengerVO();
            sysVo.setLevel(SystemMessengerVO.LEVEL_PRODUCTION);
            stringWriter = new StringWriter();
            printWriter = new PrintWriter(stringWriter);
            ex.printStackTrace(printWriter);
            sysVo.setMessage(stringWriter.toString());
            sysVo.setSource(MSG_SOURCE);
            sysVo.setType(MSG_TYPE_EXCEPTION);
            SystemMessenger.getInstance().send(sysVo, conn, false);
        }
        catch(Exception sme)
        {
            logger.error(ex);
        }
        finally
        {
            try
            {
                if(conn != null && !conn.isClosed())
                {
                    conn.close();
                }
                if(stringWriter != null)
                {
                    stringWriter.close();
                }
                if(printWriter != null)
                {
                    printWriter.close();
                }
            }
            catch(Exception e)
            {
                logger.error(e);
            }
        }
      } 
      
    }// end if
  }


    /**
     * Loads cache config file definitions and populates cacheMap/cacheVO objects accordingly
     */
    public synchronized static void loadCacheConfig() 
        throws IOException, SAXException, ParserConfigurationException, Exception 
    {
        Document config;

        try {
            
            // Open original cache config file if this is our first time through, otherwise 
            // open our local copy of the cache config file.  See JBOSS comment below for reason why.
            //
            DocumentBuilder builder = DOMUtil.getDocumentBuilder(false, true, true, true, false, false);
            builder.setEntityResolver(new DataAccessEntityResolver());
            URL url = null;
            if (cacheConfigFile == null) {
                url = Thread.currentThread().getContextClassLoader().getResource(CONFIG_FILE);
                if (url == null) {
                    throw new IOException("The cache configuration file was not found: " + CONFIG_FILE);
                }
                if(logger.isInfoEnabled())
                  logger.info("Loading cache config: " + CONFIG_FILE + " from: " + url.getFile());
                try {    
                    File origCacheConfigFile = new File(new URLDecoder().decode(url.getFile(), "UTF-8"));
                    config = (Document)DOMUtil.getDocument(builder, origCacheConfigFile);
                } catch (FileNotFoundException e) {
                  // JP Puzon 07-29-2005
                  // Added this functionality to accomodate resources which
                  // are not physically located on the local filesystem.
                  if(logger.isInfoEnabled())
                    logger.info("Caught FileNotFoundException so trying alternate method");
                  config = DOMUtil.getDocument(builder, ResourceUtil.getInstance().getResourceAsStream(CONFIG_FILE));
                }
            } else {
                String fileName = CONFIG_DIR + globalName + CONFIG_FILE;
                if(logger.isInfoEnabled())
                  logger.info("Reloading existing cache config: " + fileName);
                url = new URL("file:" + fileName);
                File origCacheConfigFile = new File(new URLDecoder().decode(url.getFile(), "UTF-8"));
                config = (Document)DOMUtil.getDocument(builder, origCacheConfigFile);
            }
            
            // Open cach config file and retrieve data relevant to all caches
            //
            NodeList cacheNodeList = (NodeList) DOMUtil.selectNodeList(config, "/cache-config/caches/cache");    
            globalName = ((Element)DOMUtil.selectSingleNode(config,"/cache-config")).getAttribute("global-cache-name");
            dataSource = ((Element)DOMUtil.selectSingleNode(config,"/cache-config")).getAttribute("data-source");
            try {
                cacheAdminRate = Integer.parseInt(((Element)DOMUtil.selectSingleNode(config,"/cache-config")).getAttribute("cache-admin-rate"));
            } catch (NumberFormatException nfe) {
                cacheAdminRate = 0;
            }
            if (cacheAdminRate < MIN_ADMIN_RATE) {
                logger.error("Cache Admin rate is below minimum, defaulting to " + MIN_ADMIN_RATE);
                cacheAdminRate = MIN_ADMIN_RATE;
            }
            cacheAdminRate = cacheAdminRate * 60000;  // Convert minutes to milliseconds
        
            Element cacheElement;
            CacheVO cacheVo;
            
            // Loop over definitions for each cache
            //
            for (int i = 0; i < cacheNodeList.getLength(); i++) {
                boolean isTimerNeeded = false;
                cacheElement = (Element) (cacheNodeList.item(i));
                String cname = cacheElement.getAttribute("name");
                if(logger.isDebugEnabled())
                  logger.debug("Reading config data for cache: " + cname);
                
                // If CacheVO already exists for this cache, re-use it.
                // Otherwise create new and add to cacheMap.
                //
                CacheVO existingCacheVo = (CacheVO)cacheMap.get(cname);
                if (existingCacheVo != null) {
                    cacheVo = existingCacheVo;
                } else {
                    cacheVo = new CacheVO();
                    cacheMap.put(cname, cacheVo);
                }
                
                // Populate with data from config file
                //
                cacheVo.setCacheName(cname);
                String cacheType = cacheElement.getAttribute("cache-type");
                if (!FIXED_CACHE_TYPE.equals(cacheType) && !TRANSIENT_CACHE_TYPE.equals(cacheType)) {
                	logger.error("Invalid/Missing cache type for " + cname + ", type='< " + cacheType + ">'. Defaulting to '" + FIXED_CACHE_TYPE + "'.");
                	cacheType = FIXED_CACHE_TYPE;
                }
                cacheVo.setCacheType(cacheType);
                cacheVo.setCacheHandlerClassName(cacheElement.getAttribute("cache-loader"));
                String preload = cacheElement.getAttribute("preload");
                if (preload != null && preload.equalsIgnoreCase("true")) {
                    cacheVo.setPreloadFlag(true);
                    isTimerNeeded = true;
                } else {
                    cacheVo.setPreloadFlag(false);                
                }
                String refresh = cacheElement.getAttribute("refresh-rate");            // Refresh rate for Fixed caches
                if (refresh != null) {
                    try {
                        cacheVo.setRefreshRate(Integer.parseInt(refresh));
                    } catch (NumberFormatException nfe) {
                        cacheVo.setRefreshRate(0);
                    }
                    if (cacheVo.getRefreshRate() == 0) {
                        // Do nothing since we assume 0 is no refresh
                    } else if (cacheVo.getRefreshRate() < MIN_REFRESH_RATE) {
                        logger.error("Refresh rate for " + cname + " is below minimum, defaulting to " + MIN_REFRESH_RATE);
                        cacheVo.setRefreshRate(MIN_REFRESH_RATE);
                        isTimerNeeded = true;                    
                    } else {
                        isTimerNeeded = true;                    
                    }
                }
                String expire = cacheElement.getAttribute("expire-rate");              // Expiration rate for Transient cache
                if (expire != null) {
                    try {
                        cacheVo.setExpireRate(Integer.parseInt(expire));
                    } catch (NumberFormatException nfe) {
                        cacheVo.setExpireRate(0);
                    }
                    if (cacheVo.getExpireRate() == 0) {
                        // Do nothing since we assume 0 is no expiration
                    } else if (cacheVo.getExpireRate() < MIN_EXPIRE_RATE) {
                        logger.error("Expire rate for " + cname + " is below minimum, defaulting to " + MIN_EXPIRE_RATE);
                        cacheVo.setExpireRate(MIN_EXPIRE_RATE);
                        cacheVo.setExpireCheckRate(MIN_EXPIRE_RATE);
                        isTimerNeeded = true;
                    } else {
                        isTimerNeeded = true;                    
                        cacheVo.setExpireCheckRate(cacheVo.getExpireRate()); // Default check rate in case not in config
                    }
                }
                String expireCheck = cacheElement.getAttribute("expire-check-rate");   // Rate to check for Transient cache expirations
                if (expireCheck != null) {
                    try {
                        cacheVo.setExpireCheckRate(Integer.parseInt(expireCheck));
                    } catch (NumberFormatException nfe) {
                        cacheVo.setExpireCheckRate(0);
                    }
                    if (cacheVo.getExpireCheckRate() == 0) {
                        // Do nothing since we assume 0 is no expiration
                    } else if (cacheVo.getExpireCheckRate() < MIN_EXPIRE_RATE) {
                        logger.error("Expire check rate for " + cname + " is below minimum, defaulting to " + MIN_EXPIRE_RATE);
                        cacheVo.setExpireCheckRate(MIN_EXPIRE_RATE);
                        isTimerNeeded = true;
                    } else {
                        isTimerNeeded = true;                    
                    }
                }
                
                // Spawn any necessary timer threads for this cache
                //
                if (isTimerNeeded) {
                    spawnCacheTimer(cacheVo);
                }
             }
             
             // Write copy of config file to disk since under JBOSS the file is within the EAR file
             // (which makes it difficult to "touch" when we want to force cache reloads).
             //
             if (cacheConfigFile == null) {
                 String xmlString = null;
                 StringWriter sw = null;
                 PrintWriter pw = null;
                 try {
                    sw = new StringWriter();
                    pw = new PrintWriter(sw, true);
                    DOMUtil.print(config, pw);
                    xmlString = sw.toString();
                 } finally {
                    pw.close();
                 }
                 String fileName = globalName + CONFIG_FILE;
                 String cacheFileDir = CONFIG_DIR;
                 cacheConfigFile = new File(cacheFileDir, fileName);
                 if(logger.isDebugEnabled())
                   logger.debug("Copying config file to " + cacheFileDir + fileName);
                 //FileUtils.writeStringToFile(cacheConfigFile, xmlString, null);
                 FileWriter cacheConfigWriter = null; 
                 
                 try {
                    cacheConfigWriter = new FileWriter(cacheConfigFile);
                    cacheConfigWriter.write(xmlString);
                 } catch (Exception e) {
                    logger.error("Failed to copy config file to " + cacheFileDir + fileName, e);
                 } finally {
                    if (cacheConfigWriter != null) {
                      try {
                        cacheConfigWriter.close();  
                      } catch (Exception closeEx) {
                        // NOP
                      }
                    }
                 }
             }
        // ??? Catch any configuration exceptions and page out
        } catch (Exception ex) {
            throw ex;
        } 
        
    }


    /**
     * Returns last modification time of cache config file
     */
    public static long getCacheConfigLastModified() {
        long retModTime = 0;
        SimpleDateFormat df = new SimpleDateFormat(DATE_FORMAT);
        try {
            if(logger.isDebugEnabled())
              logger.debug("CacheConfig last modification time: " + df.format(cacheConfigFile.lastModified()).toString());
        } catch (Exception e) {
            // No harm done
        }
        if (cacheConfigFile != null) {
            retModTime = cacheConfigFile.lastModified();
        }
        return retModTime;
    }


    /**
     * Returns classloader for this object
     */
    public static ClassLoader getClassLoader() {
        return CACHEMANAGER.getClass().getClassLoader();
    }
  
  
    /**
    * Creates thread to do cache administration tasks
    */
    private static void spawnAdminTimer() {

        // Terminate any existing admin timer thread
        if (adminTimer != null) {    
          adminTimer.cancel();
        }
        
        if(logger.isInfoEnabled())
          logger.info("Spawning Cache Admin thread - refresh rate (millisecs): " + cacheAdminRate);
        adminTimer = new Timer();
        // Thread won't start for cacheAdminRate milliseconds, then will repeat at that rate
        adminTimer.schedule(new CacheAdminTimer(), cacheAdminRate, cacheAdminRate);
    }
  
  
    /**
    * Creates thread to handle load, reload or expiration of a particular cache object
    */
    private static void spawnCacheTimer(CacheVO cacheVo) {
        long refreshRate      = cacheVo.getRefreshRate() * 60000;       // Convert minutes to milliseconds
        long expireCheckRate  = cacheVo.getExpireCheckRate() * 60000;
        boolean cacheTimerExists = false;

        // Terminate any existing timer thread
        //
        if (cacheVo.getCacheTimer() != null) {
            cacheTimerExists = true;
            if(logger.isDebugEnabled())
              logger.debug("Cancelling an existing timer thread for " + cacheVo.getCacheName());
            cacheVo.getCacheTimer().cancel();
            cacheVo.setCacheTimer(null);
        }

        if (refreshRate > 0) {
        
            // Start refresh thread for Fixed cache.
            // Note we only spawn thread if cache is "preload" or thread already exists (which implies cache
            // was lazy-loaded at some point in past).  We don't spawn threads for non-preload, since those caches
            // may never be used (threads for non-preload will be triggered when lazy-loaded for first time).
            //
            if (cacheVo.isPreloadFlag() || cacheTimerExists) {
                if(logger.isDebugEnabled())
                  logger.debug("Spawning Fixed Cache Refresh thread - refresh rate (millisecs): " + refreshRate);
                  // Refresh now, then repeat at refresh rate.
                  // Note the only way we'd have a prior lazy-loaded cache at this point, is if cache config
                  // was changed/touched, so this refresh-now will trigger a reload.
                  scheduleTimer(cacheVo, new FixedCacheRefreshTimer(cacheVo.getCacheName()), REFRESH_NOW, refreshRate);
            }

        } else if (expireCheckRate > 0) {
        
            // Start expiration thread for Transient cache
            //
            if(logger.isDebugEnabled())
              logger.debug("Spawning Transient Cache Expire thread - execution rate (millisecs): " + expireCheckRate);
            // Thread won't start for expireCheckRate milliseconds, then will repeat at that rate
            scheduleTimer(cacheVo, new TransientCacheExpirationTimer(cacheVo.getCacheName(), cacheVo.getExpireRate()), expireCheckRate, expireCheckRate);            

        } else {
            cacheVo.setCacheTimer(null);
        }
    }


    /**
     * Does actual scheduling of a cache timer thread.  
     * Syncrhonized to prevent multiple threads being started for same cache.
     */
    private synchronized static void scheduleTimer(CacheVO cacheVo, TimerTask timerObj, long startDelay, long refreshRate) {
        Timer cacheTimer = null;

        // Terminate any existing timer thread
        //
        if (cacheVo.getCacheTimer() != null) {
            if(logger.isDebugEnabled())
              logger.debug("Cancelling existing timer thread for " + cacheVo.getCacheName());
            cacheVo.getCacheTimer().cancel();
            cacheVo.setCacheTimer(null);
        }
        cacheTimer = new Timer();
        cacheTimer.schedule(timerObj, startDelay, refreshRate);                    
        cacheVo.setCacheTimer(cacheTimer);
    }

  
    /**
    * Called only by Admin thread to look for and start any missing Fixed cache Refresh threads.
    * This is necessary since Fixed caches (not configured as "preload") are first loaded during
    * their first access (lazy-loading) from an application - hence the Refresh threads must be started too.  
    * The spawnRefreshThread flag is used to coordinate this between the first cache access and the Admin thread.
    */
    public synchronized static void checkAndSpawnMissingRefreshTimers() {
        setSpawnRefreshThread(false);
        if(logger.isDebugEnabled())
          logger.debug("Checking for Fixed caches without Refresh threads");
        Iterator iterator = cacheMap.keySet().iterator();
        CacheVO cvo;
        while (iterator.hasNext()) {
            String key = (String) iterator.next();
            cvo = (CacheVO) cacheMap.get(key);
            if (cvo.getCacheType().equalsIgnoreCase(FIXED_CACHE_TYPE) && 
                (cvo.getCacheTimer() == null) && 
                (cvo.getCacheObj() != null) &&
                (cvo.getRefreshRate() > 0)) {

                // Schedule for start after refreshRate milliseconds, then repeat at that rate
                long refreshRate = cvo.getRefreshRate() * 60000;       // Convert minutes to milliseconds
                if(logger.isDebugEnabled())
                  logger.debug("Spawning (lazy-loaded) Fixed Cache Refresh thread - refresh rate (millisecs): " + refreshRate);
                scheduleTimer(cvo, new FixedCacheRefreshTimer(cvo.getCacheName()), refreshRate, refreshRate);
            }
        }
    }

  
    /**
    * Instantiates and returns proper cache handler for caller (loading cache object if necessary).
    * All access to the actual cache object will be through this returned handler.
    * This is currently only called directly by FixedCacheRefreshTimer (since forceRefresh flag is used).
    * All other callers should use overloaded method without forceRefresh parameter.
    * 
    * @param handlerName The name of the cache
    * @param forceRefresh Set to true if cache object should be loaded/reloaded immediately.
    */
    public CacheHandlerBase getHandler(String handlerName, boolean forceRefresh) {

        Class cacheHandlerBaseClass = null;
        CacheHandlerBase handler = null;
        Connection conn = null;
        CacheVO cacheVo = null;
        
        try {
            cacheVo = (CacheVO)cacheMap.get(handlerName);
    
            // Increment handler access count (for statistics use).
            // Since we don't have any means of tracking how many times the actual cache object within
            // the handler is accessed, this is the best means of keeping track of how often a cache is accessed.
            cacheVo.incrementAccessCount();
            
            // Set the appropriate cache loader
            //
            try {
                cacheHandlerBaseClass = Class.forName(cacheVo.getCacheHandlerClassName());
            } catch (ClassNotFoundException e) {
              // Copied from old logic: "JP Puzon 08-23-05 Added the catch logic below to accomodate multiple class loaders."
              cacheHandlerBaseClass = Thread.currentThread().getContextClassLoader().loadClass(cacheVo.getCacheHandlerClassName());
            }
            handler = (CacheHandlerBase)(cacheHandlerBaseClass.newInstance());
            
            if (cacheVo.getCacheObj() != null && forceRefresh == false) {     // IF-1
    
                // Cache object already present and no forcing of refresh, 
                // so just put it in handler and return.
                //
                handler.setCachedObject(cacheVo.getCacheObj());
    
            } else {
                
                // No cache object (or flags indicating we should reload), so load it now by calling handler's load(),
                // then save it in our cacheMap and in the handler.
                //
                // Note for Fixed cache refreshes, we get here since forceRefresh flag is set.  Since called from a timer thread
                // this allows any other simultaneous access to the same cache continue unabated with the existing cache data.
                //
                conn = DataSourceUtil.getInstance().getConnection(dataSource); 
                if (cacheVo.getCacheObj() == null) {                             // IF-2

                    // There was no cache object (which should only occur before first load of object), 
                    // so synchronize on the cache VO to prevent multiple simultaneous loads
                    //
                    synchronized(cacheVo) {
                        if (logger.isInfoEnabled()) {logger.info("Synchronized CACHE load - Started");}
                        if (cacheVo.getCacheObj() == null) {
                            loadit(conn, cacheVo, handlerName, handler);
                            if (logger.isInfoEnabled()) {logger.info("Synchronized CACHE load - Completed");}
                        } else {
                            // Cache must have been loaded while we were waiting in synchronize, so just use it instead of loading again
                            handler.setCachedObject(cacheVo.getCacheObj());
                            if (logger.isInfoEnabled()) {logger.info("Synchronized CACHE load - Completed, but object was already loaded so reload was prevented");}
                        }
                    }
                    
                } else if (forceRefresh) {
                    // Cache object was already present so no need to synchronize (since this will be in timer thread)
                    loadit(conn, cacheVo, handlerName, handler);                    
                } else {
                    // This covers for reaaally small window where cache was populated between lines commented with IF-1 and IF-2 above
                    handler.setCachedObject(cacheVo.getCacheObj());
                }
            }
        } catch (IllegalStateException ise) {
            logger.error("Got IllegalStateException so assuming application was undeployed. " +
                         "Setting flags so all cache threads will terminate on waking.  Error triggering this was: " + ise);
            CacheManager.setTerminateThreads(true);
        } catch (com.sun.org.apache.xml.internal.dtm.DTMConfigurationException dtme) {
            logger.error("Got DTMConfigurationException so assuming application was undeployed. " +
                         "Setting flags so all cache threads will terminate on waking.  Error triggering this was: " + dtme);
            CacheManager.setTerminateThreads(true);
        } catch (org.apache.xml.dtm.DTMConfigurationException adtme) {
            logger.error("Got Apache DTMConfigurationException so assuming application was undeployed. " +
                         "Setting flags so all cache threads will terminate on waking.  Error triggering this was: " + adtme);
            CacheManager.setTerminateThreads(true);
        } catch (Throwable t) {
            if (cacheVo == null) {
                logger.error("Specified cache was not found: " + handlerName);
            }
            logger.error("Something bad happened when attempting to get a cache handler - stack trace follows: ");
            logger.error(t);
        } finally { 
          if( conn != null) {
            try {
              conn.close();
            } catch(Exception e) {
              //Do nothing
            }
          } 
        }
        return handler;
    }


    /**
     * Private method used only by getHandler to call actual handler load method.
     * This was only made a method to allow for conditional synchronization of this logic.
     */
    private void loadit(Connection conn, CacheVO cacheVo, String handlerName, CacheHandlerBase handler) throws Exception {
        long loadStartTime = System.currentTimeMillis();
        if(logger.isInfoEnabled()) {
          logger.info("START CACHE LOAD: " + handlerName);
        }
        
        Object newCacheObj = (Object)handler.load(conn);
        cacheVo.setCacheObj(newCacheObj);               // Save it in cacheVo (within cacheMap)
        handler.setCachedObject(newCacheObj);           // ...and in handler for caller
        
        long loadEndTime = System.currentTimeMillis(); // At this time, the cache is ready for reuse from another thread
        if(logger.isInfoEnabled()) {
          logger.info("END CACHE LOAD: " + handlerName + ", time=" + (loadEndTime - loadStartTime));
        }
        
        cacheVo.setLastRefreshTime(new Date());
        cacheVo.incrementRefreshCount();
        if (cacheVo.getFirstLoadTime() == null) {
            cacheVo.setFirstLoadTime(new Date());
        }
        setSpawnRefreshThread(true);  // Forces Admin thread to start any Fixed cache Refresh threads that are needed
    }
  
  
    /**
    * Instantiates and returns proper cache handler for caller (loading cache object if necessary).
    * All access to the actual cache object will be through this returned handler.
    * 
    * @param handlerName The name of the cache
    */
    public CacheHandlerBase getHandler(String handlerName) {
        return this.getHandler(handlerName, false);
    }
     

    /**
     * Dumps CacheVO data for a cache into a string for logging purposes
     */
    public String dumpAllCachesStr() {
        CacheVO cacheVo = null;
        TransientCacheVO tcvo = null;
        String timeStr;
        String cTypeChar = "";
        String retStr = null;
        String cacheName = null;
        String refreshTimeStr = null;
        String firstLoadTimeStr = null;
        long refExpRate = 0;
        long transCount = 0;
        long transArrayCount = 0;
        SimpleDateFormat df  = new SimpleDateFormat(DATE_FORMAT_LOG);
        SimpleDateFormat df2 = new SimpleDateFormat(DATE_FORMAT);
        
        try {
            retStr = CACHE_STATS_LOG_PREFIX + df2.format(new Date()) + ":\n" + CACHE_STATS_LOG_HEADER;
            for (String key : cacheMap.keySet()) {
                cacheVo = cacheMap.get(key);
                transCount = 0;
                transArrayCount = 0;
                if (cacheVo != null) {
                    String cType = cacheVo.getCacheType();
                    if (cType.equalsIgnoreCase(FIXED_CACHE_TYPE)) {
                    
                        // Fixed cache type
                        cTypeChar = "F";
                        refExpRate = cacheVo.getRefreshRate();
                    } else if (cType.equalsIgnoreCase(TRANSIENT_CACHE_TYPE)) {
                    
                        // Transient cache type
                        cTypeChar = "T";                    
                        refExpRate = cacheVo.getExpireRate();
                        tcvo = (TransientCacheVO) cacheVo.getCacheObj();
                        if (tcvo != null) {
                            transCount = tcvo.sizeOfElementMap();
                            transArrayCount = tcvo.sizeOfElementTimeArray();
                        }
                    }
                    try {
                        refreshTimeStr = df.format(cacheVo.getLastRefreshTime());
                    } catch (NullPointerException npe) {
                        refreshTimeStr = "";
                    }
                    try {
                        firstLoadTimeStr = df.format(cacheVo.getFirstLoadTime());
                    } catch (NullPointerException npe) {
                        firstLoadTimeStr = "";
                    }
                    retStr += String.format(CACHE_STATS_LOG_FORMAT, 
                                    cacheVo.getCacheName(), 
                                    cTypeChar, 
                                    cacheVo.getAccessCount(),
                                    cacheVo.getRefreshCount(),
                                    refExpRate, ((cacheVo.getCacheTimer() == null)?"!":" "),  // Put exclamation mark if no timer assigned 
                                    refreshTimeStr,
                                    firstLoadTimeStr,
                                    transCount,
                                    transArrayCount
                    );
                    
                }
            }
            
        } catch (Exception ex) {
            logger.error("Error dumping cacheVO for logging - " + ex);
            ex.printStackTrace();
        } 
        return retStr;                
    }


    /**
     * Updates cache statistics table in database with latest CacheVO data for all caches
     * NOT USED AT THIS TIME
     */
    public void statsSaveAllCaches() {
        Iterator iterator = cacheMap.keySet().iterator();
        CacheVO cvo;
        while (iterator.hasNext()) {
            String key = (String) iterator.next();
            statsSaveCacheVO(key);
        }
    }


    /**
     * Updates cache statistics table in database with latest CacheVO data
     * NOT USED AT THIS TIME
     */
    public void statsSaveCacheVO(String cacheName) {
        Connection conn = null;
        CacheVO cacheVo = null;
        try
        {
            InetAddress addr = InetAddress.getLocalHost();
            byte[] ipAddr = addr.getAddress();
            String hostname = addr.getHostName();
            String pathname = cacheConfigFile.getPath();
            cacheVo = cacheMap.get(cacheName);
            if (pathname.length() > 200) {
                pathname = pathname.substring(0,200);
            }

            conn = DataSourceUtil.getInstance().getConnection(CACHE_DATASOURCE);
            DataAccessUtil dau = DataAccessUtil.getInstance();
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("SAVE_CACHE_STATS");
            dataRequest.addInputParam("IN_NODE_NAME", hostname);
            dataRequest.addInputParam("IN_APP_NAME", pathname);
            dataRequest.addInputParam("IN_CACHE_NAME", cacheVo.getCacheName());
            dataRequest.addInputParam("IN_CACHE_TYPE", cacheVo.getCacheType());
            dataRequest.addInputParam("IN_PRELOAD_FLAG", cacheVo.isPreloadFlag()?"Y":"N");
            dataRequest.addInputParam("IN_ACCESS_COUNT", new Long(cacheVo.getAccessCount()));
            dataRequest.addInputParam("IN_REFRESH_COUNT", new Long(cacheVo.getRefreshCount()));
            dataRequest.addInputParam("IN_REFRESH_RATE", new Long(cacheVo.getRefreshRate()));
            dataRequest.addInputParam("IN_EXPIRE_RATE", new Long(cacheVo.getExpireRate()));
            dataRequest.addInputParam("IN_EXPIRE_CHECK_RATE", new Long(cacheVo.getExpireCheckRate()));
            dataRequest.addInputParam("IN_FIRST_LOAD_TIME", cacheVo.getFirstLoadTime() == null ? null : new java.sql.Timestamp(cacheVo.getFirstLoadTime().getTime()));
            dataRequest.addInputParam("IN_LAST_REFRESH_TIME", cacheVo.getLastRefreshTime() == null ? null : new java.sql.Timestamp(cacheVo.getLastRefreshTime().getTime()));
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();        
            Map outputs = (Map) dataAccessUtil.execute(dataRequest);
            String status = (String) outputs.get("OUT_STATUS");
            if( StringUtils.equals(status, "N") )
            {
                String message = (String) outputs.get("OUT_MESSAGE");
                logger.error("Error saving cacheVO stats to database for " + cacheName + ".  Error returned: " + message);
            }
            
        }
        catch(Exception sme)
        {
            logger.error("Error saving cacheVO stats to database for " + cacheName + ". Error: " + sme);
        }
        finally
        {
            try
            {
                if(conn != null && !conn.isClosed())
                {
                    conn.close();
                }
            }
            catch(Exception e)
            {
                logger.error(e);
            }
        }
    }
     

    /**
     * Dumps all CacheVO data into an XML document
     * NOT USED AT THIS TIME
     */
    public Document dumpAllCaches() throws Exception {
        CacheVO cacheVo = null;
        Document xmlDoc = (Document) DOMUtil.getDefaultDocument();
        Element caches = (Element) xmlDoc.createElement("caches");
        xmlDoc.appendChild(caches);
        for (String key : cacheMap.keySet()) {
            cacheVo = cacheMap.get(key);
            DOMUtil.addSection(xmlDoc, dumpCacheVO(cacheVo.getCacheName()).getChildNodes());
        }
        return xmlDoc;
    }


    /**
     * Dumps CacheVO data for a cache into an XML document
     * NOT USED AT THIS TIME
     */
    public Document dumpCacheVO(String cacheName) {
        CacheVO cacheVo = null;
        Document xmlDoc = null;
        Element node = null;
        SimpleDateFormat df = new SimpleDateFormat(DATE_FORMAT);
        String timeStr;
        
        try {
            cacheVo = cacheMap.get(cacheName);
            xmlDoc = (Document) DOMUtil.getDefaultDocument();
            Element top = xmlDoc.createElement("CacheVO");
            xmlDoc.appendChild(top);

            node = xmlDoc.createElement("cacheName");
            top.appendChild(node);
            node.appendChild(xmlDoc.createTextNode(cacheVo.getCacheName()));
            node = xmlDoc.createElement("cacheType");
            top.appendChild(node);
            node.appendChild(xmlDoc.createTextNode(cacheVo.getCacheType()));
            node = xmlDoc.createElement("accessCount");
            top.appendChild(node);
            node.appendChild(xmlDoc.createTextNode(String.valueOf(cacheVo.getAccessCount())));
            node = xmlDoc.createElement("refreshCount");
            top.appendChild(node);
            node.appendChild(xmlDoc.createTextNode(String.valueOf(cacheVo.getRefreshCount())));
            node = xmlDoc.createElement("refreshRate");
            top.appendChild(node);
            node.appendChild(xmlDoc.createTextNode(String.valueOf(cacheVo.getRefreshRate())));
            node = xmlDoc.createElement("lastRefreshTime");
            top.appendChild(node);
            try {
                timeStr = (new StringBuilder(df.format(cacheVo.getLastRefreshTime())).toString());
            } catch (NullPointerException npe) {
                timeStr = "";
            }
            node.appendChild(xmlDoc.createTextNode(timeStr));
            node = xmlDoc.createElement("expireRate");
            top.appendChild(node);
            node.appendChild(xmlDoc.createTextNode(String.valueOf(cacheVo.getExpireRate())));
            node = xmlDoc.createElement("expireCheckRate");
            top.appendChild(node);
            node.appendChild(xmlDoc.createTextNode(String.valueOf(cacheVo.getExpireCheckRate())));
            node = xmlDoc.createElement("firstLoadTime");
            top.appendChild(node);
            try {
                timeStr = (new StringBuilder(df.format(cacheVo.getFirstLoadTime())).toString());
            } catch (NullPointerException npe) {
                timeStr = "";
            }
            node.appendChild(xmlDoc.createTextNode(timeStr));
            node = xmlDoc.createElement("cacheTimer");
            top.appendChild(node);
            if (cacheVo.getCacheTimer() != null) {
                node.appendChild(xmlDoc.createTextNode("Assigned"));
            } else {
                node.appendChild(xmlDoc.createTextNode("None"));                
            }
            
        } catch (Exception ex) {
            logger.error("Error converting cacheVO for " + cacheName + " to XML: " + ex);
            ex.printStackTrace();
        } 
        return xmlDoc;                
    }
     

    /**
     * Dumps meta-data (i.e., TransientCacheElementVO) for all elements in a Transient cache into an XML document
     * NOT USED AT THIS TIME
     */
    public Document dumpTransientCache(String cacheName) {
        CacheVO cacheVo = null;
        TransientCacheVO tcvo = null;
        Map<Object, TransientCacheElementVO> elementMap = null;
        Document xmlDoc = null;
        SimpleDateFormat df = new SimpleDateFormat(DATE_FORMAT);

        try  {
            cacheVo = cacheMap.get(cacheName);
            tcvo = (TransientCacheVO) cacheVo.getCacheObj();
            xmlDoc = (Document) DOMUtil.getDefaultDocument();
            Element top = xmlDoc.createElement("TransientCacheVO");
            xmlDoc.appendChild(top);

            Element nodeName = xmlDoc.createElement("cacheName");
            top.appendChild(nodeName);
            nodeName.appendChild(xmlDoc.createTextNode(cacheVo.getCacheName()));

            if (tcvo != null && tcvo.getElementMap() != null) {
                elementMap = tcvo.getElementMap();
                for (Object key : elementMap.keySet()) {
                    Element elem = xmlDoc.createElement("TransientCacheElementVO");
                    top.appendChild(elem);
                    Element node = null;
                    TransientCacheElementVO tcevo = elementMap.get(key);
                    node = xmlDoc.createElement("elementKey");
                    elem.appendChild(node);
                    String keyStr = tcevo.getElementKey().toString();
                    if (keyStr.length() > 30) {
                        node.appendChild(xmlDoc.createTextNode(keyStr.substring(0,30) + "..."));
                    } else {
                        node.appendChild(xmlDoc.createTextNode(keyStr));
                    }
                    node = xmlDoc.createElement("elementAccessCount");
                    elem.appendChild(node);
                    node.appendChild(xmlDoc.createTextNode(String.valueOf(tcevo.getElementAccessCount())));
                    node = xmlDoc.createElement("creationTime");
                    elem.appendChild(node);
                    node.appendChild(xmlDoc.createTextNode((new StringBuilder(df.format(tcevo.getCreationTime())).toString())));
                    node = xmlDoc.createElement("updateTime");
                    elem.appendChild(node);
                    node.appendChild(xmlDoc.createTextNode((new StringBuilder(df.format(tcevo.getUpdateTime())).toString())));

                    Calendar cal = Calendar.getInstance(); 
                    Date udate = tcevo.getUpdateTime();
                    cal.setTime(udate);
                    cal.add(Calendar.MINUTE, (new Long(cacheVo.getExpireRate())).intValue());
                    Date expiration = cal.getTime(); 
                    node = xmlDoc.createElement("calculatedExpireTime");
                    elem.appendChild(node);
                    node.appendChild(xmlDoc.createTextNode((new StringBuilder(df.format(expiration)).toString())));
                }
            }
        } catch (Exception ex) {
            logger.error("Error converting transient cache meta-data " + cacheName + " to XML: " + ex);
            ex.printStackTrace();
        } 
        return xmlDoc;        
    }

    
    public static void setTerminateThreads(boolean a_terminateThreads) {
        terminateThreads = a_terminateThreads;
    }

    public static boolean isTerminateThreads() {
        return terminateThreads;
    }

    public static void setSpawnRefreshThread(boolean a_spawnRefreshThread) {
        spawnRefreshThread = a_spawnRefreshThread;
    }

    public static boolean isSpawnRefreshThread() {
        return spawnRefreshThread;
    }

    public Connection getConnection() throws Exception
    {
        Connection conn = DataSourceUtil.getInstance().getConnection(dataSource);

        return conn;
    }
}
