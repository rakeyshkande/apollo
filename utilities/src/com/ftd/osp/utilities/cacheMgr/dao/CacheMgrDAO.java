package com.ftd.osp.utilities.cacheMgr.dao;

import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;
import java.util.HashMap;


public class CacheMgrDAO
{
    protected static Logger logger  = 
        new Logger("com.ftd.osp.utilities.cacheMg.CacheMgrDAO");
        
    public CachedResultSet executeQuery(Connection conn, String statement) throws CacheException {

        CachedResultSet rs = null;
        Object output = null;
        try {
            output = this.executeQueryReturnObject(conn, statement);
            rs = (CachedResultSet)output;
        } catch (Exception e) {
            logger.error(e);
            throw new CacheException(e.getMessage());
        } finally {
            if (logger.isDebugEnabled()) {
                logger.debug("Exiting load:" + statement);
            }
        }
        return rs;

    }

    public Object executeQueryReturnObject(Connection conn, String statement) throws CacheException {

        Object output = null;
        try {
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(conn);
            dataRequest.setStatementID(statement);

            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

            if(logger.isDebugEnabled())
              logger.debug("Calling statement: " + statement);
            output = dataAccessUtil.execute(dataRequest);
            dataRequest.reset();
        } catch (Exception e) {
            logger.error(e);
            throw new CacheException(e.getMessage());
        } finally {
            if (logger.isDebugEnabled()) {
                logger.debug("Exiting load:" + statement);
            }
        }
        return output;

    }
    
    public Object executeQueryReturnObject(Connection conn, String statement, HashMap inputParams) throws CacheException {

        Object output = null;
        try {
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(conn);
            dataRequest.setStatementID(statement);
            dataRequest.setInputParams(inputParams);

            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

            if(logger.isDebugEnabled())
              logger.debug("Calling statement: " + statement);
            output = dataAccessUtil.execute(dataRequest);
            dataRequest.reset();
        } catch (Exception e) {
            logger.error(e);
            throw new CacheException(e.getMessage());
        } finally {
            if (logger.isDebugEnabled()) {
                logger.debug("Exiting load:" + statement);
            }
        }
        return output;

    }    
}
