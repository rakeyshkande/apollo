package com.ftd.osp.utilities.cacheMgr.dao;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import com.ftd.osp.utilities.cacheMgr.CacheUtil;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.CountryMasterVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.PriceHeaderDetailVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.ProductMasterVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.ServiceFeeVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.SourceMasterVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.order.RecalculateException;
import com.ftd.osp.utilities.order.dao.RecalculateOrderDAO;
import com.ftd.osp.utilities.order.vo.PaymentMethodMilesPointsVO;
import com.ftd.osp.utilities.order.vo.RecalculateOrderGlobalParmsVO;
import com.ftd.osp.utilities.order.vo.RecalculateOrderPriceHeaderDetailsVO;
import com.ftd.osp.utilities.order.vo.RecalculateOrderSourceCodeVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.ProductVO;
import com.ftd.osp.utilities.vo.ServiceChargeVO;


/**
 * This DAO provides cache data to support the Recalculate Order BO.
 *
 */
public class RecalculateOrderCacheDAO extends RecalculateOrderDAO
{

  //DATABASE STATEMENTS
  private static final String GET_ACCOUNT_ACTIVE_PROGRAMS_STATEMENT = "STMT_VIEW_ACCOUNT_ACTIVE_PROGRAMS";

  //COUNTRY TYPES
  private static final String DOMESTIC = "D";
  
  public RecalculateOrderCacheDAO recalculateOrderDAO;

  private static Logger logger = new Logger(RecalculateOrderCacheDAO.class.getName());

  public RecalculateOrderCacheDAO()
  {
  }
  
  public RecalculateOrderCacheDAO(RecalculateOrderCacheDAO recalculateOrderCacheDAO)
  {
	  	this.recalculateOrderDAO = recalculateOrderCacheDAO;
  }

  /* Return native double from String object
     * @param String
     * @return double value
     *         returns zero if String is null*/

  private double formatToNativeDouble(String value)
  {
    return value == null? 0: new Double(value).doubleValue();
  }

  /* Return Big Decimal from String object
     * @param String
     * @return double value
     *         returns zero if String is null*/

  private BigDecimal formatToBigDecimal(String value)
  {
    return value == null? new BigDecimal(0): new BigDecimal(value);
  }

  /** This method takes in an object and returns
   * it's string value.
   * @param Object
   * @return String value of object
   *         If object is null, null is returned*
   */
  protected String getValue(Object obj)
  {
    return getValue(obj, null, null);
  }

  /** This method takes in an object and returns
   * it's string value.
   * @param Object
   * @return String value of object
   *         If object is null, null is returned*
   */
  protected String getValue(Object obj, String nullValue)
  {
    return getValue(obj, nullValue, null);
  }

  /** This method takes in an object and returns
   * it's string value.
   * @param Object
   * @param Value to return if the obj is null
   * @param emptyValue If specified, value to use if the string value is empty.
   * @return String value of object
   *         If object is null, nullValue is returned*
   */
  protected String getValue(Object obj, String nullValue, String emptyValue)
  {

    String value = null;

    if (obj == null)
    {
      return nullValue;
    }

    if (obj instanceof BigDecimal)
    {
      value = obj.toString();
    }
    else if (obj instanceof String)
    {
      value = (String) obj;
    }
    else if (obj instanceof Timestamp)
    {
      value = obj.toString();
    }
    else
    {
      value = obj.toString();
    }

    if (emptyValue != null && value.length() == 0)
    {
      value = emptyValue;
    }
    return value;
  }

  /**
   * Retrieves the Source Code details from the cache
   * @param sourceCode
   * @return A populated instance of SourceVO or null if no record exists
   */
  public RecalculateOrderSourceCodeVO getSourceCodeDetails(Connection connection, String sourceCode)
  {
	try
    {
      
      SourceMasterVO smVO = CacheUtil.getInstance().getSourceCodeById(sourceCode);
      
      if (smVO == null )
      {
        return null;
      }

      RecalculateOrderSourceCodeVO sourceVO = new RecalculateOrderSourceCodeVO();
      sourceVO.setSourceCode(sourceCode);
      sourceVO.setPricingCode(smVO.getPricingCode());
      sourceVO.setPartnerId(smVO.getPartnerId());
      sourceVO.setShippingCode(smVO.getShippingCode());
      sourceVO.setJcpenneyFlag(getValue(smVO.getJcpenneyFlag(), ""));
      sourceVO.setAllowFreeShippingFlag(getValue(smVO.getAllowFreeShippingFlag(), ""));
      sourceVO.setBonusCalculationBasis(smVO.getBonusCalculationBasis());
      sourceVO.setBonusPoints(smVO.getBonusPoints());
      sourceVO.setCalculationBasis(smVO.getCalculationBasis());
      sourceVO.setPoints(smVO.getPoints());
      sourceVO.setProgramType(smVO.getProgramType());
      sourceVO.setRewardType(smVO.getRewardType());
      sourceVO.setSameDayUpcharge(smVO.getSameDayUpcharge());
      sourceVO.setDisplaySameDayUpcharge(smVO.getDisplaySameDayUpcharge());
           
      return sourceVO;
    }
    catch (Exception e)
    {
      throw new RuntimeException(e.getMessage(), e);
    }
  }


  /* Retrieve the product from the cache.
   * @param String product id
   * @returns ProductVO
   */

  public ProductVO getProduct(Connection connection, String productId)
  {
try
    {
      ProductMasterVO pmVO = CacheUtil.getInstance().getProductById(productId);
      ProductVO productVO = new ProductVO();
      
      String standardPrice = "0";
      String deluxePrice = "0";
      String premiumPrice = "0";
      
      if(pmVO.getStandardPrice() != null)
    	  standardPrice = pmVO.getStandardPrice().toString();
      if(pmVO.getDeluxePrice() != null)
    	  deluxePrice = pmVO.getDeluxePrice().toString();
      if(pmVO.getPremiumPrice() != null)
    	  premiumPrice = pmVO.getPremiumPrice().toString();
                
      productVO.setProductId(productId);
      productVO.setShippingKey(pmVO.getShippingKey());
      productVO.setStandardPrice(formatToNativeDouble(standardPrice));
      
      productVO.setDeluxePrice(formatToNativeDouble(deluxePrice));
      
      productVO.setPremiumPrice(formatToNativeDouble(premiumPrice));
      
      productVO.setProductType(pmVO.getProductType());
      productVO.setProductSubType(pmVO.getProductSubType());
      productVO.setDiscountAllowedFlag(getValue(pmVO.getDiscountAllowedFlag(), "N", "N"));
      productVO.setVariablePriceFlag(getValue(pmVO.getVariablePriceFlag(), "N", "N"));
      productVO.setTaxFlag(getValue(pmVO.getNoTaxFlag(), "N", "N"));
      productVO.setAllowFreeShippingFlag(getValue(pmVO.getAllowFreeShippingFlag(), "N", "N"));
      productVO.setShipMethodCarrier(getValue(pmVO.getShipMethodCarrier(), "N", "N"));
          
      return productVO;
    }
    catch (Exception e)
    {
      throw new RuntimeException(e.getMessage(), e);
    }
  }

  /**
   * Returns the Price Header Information for the pricing Code, price Amount
   * @param connection
   * @param pricingCode
   * @param priceAmount
   * @return DetailsVO or null if there is no price header information
   */
  public RecalculateOrderPriceHeaderDetailsVO getPriceHeaderDetails(Connection connection, String pricingCode, 
                                                                    String priceAmount)
  {
    try
    {
      PriceHeaderDetailVO phdVO= CacheUtil.getInstance().getPriceHeaderDetail(pricingCode, new BigDecimal(priceAmount));

      RecalculateOrderPriceHeaderDetailsVO priceHeaderDetailsVO = new RecalculateOrderPriceHeaderDetailsVO();
      priceHeaderDetailsVO.setDiscountType(phdVO.getDiscountType());
      priceHeaderDetailsVO.setDiscountAmount(phdVO.getDiscountAmt());

      return priceHeaderDetailsVO;
    }
    catch (Exception e)
    {
      throw new RuntimeException(e.getMessage(), e);
    }
  }


  /**
   * Retrieves the shipping costs for the given shipping method, key and price
   * Using the shipping key details table determine what shipping detail
   * id should be associated with this item.  Which shipping detail id
   * to use is dependent on the shipping key code of the product (PRODUCT_MASTER)
   * and the ship method chosen.
   *
   * Then uses the shipping detail id that was just obtained along with
   * the ship method to determine the shipping cost.  The SHIPPING_KEY_COSTS
   * table is used to find the cost.
   *
   * @param connection
   * @param shipMethod
   * @param shippingKey
   * @param price
   * @return The Shipping Cost, or null if it was unable to find the shipping detail or shipping cost.
   *
   */
  public BigDecimal getShippingCost(Connection connection, String shipMethod, String shippingKey, double price)
  {
    try
    {
      BigDecimal shippingFee = CacheUtil.getInstance().getShippingCost(shippingKey, shipMethod, new BigDecimal(price));

      return shippingFee;
    }
    catch (Exception e)
    {
      throw new RuntimeException(e.getMessage(), e);
    }
  }

  /**
   * Retrieves the AddOns Information by its ID. 
   * @param connection
   * @param addonId
   * @return com.ftd.osp.utilities.vo.AddOnVO
   */
  public com.ftd.osp.utilities.vo.AddOnVO getAddOnById(Connection connection, String addonId)
  {
    com.ftd.osp.utilities.vo.AddOnVO addOnVO = new com.ftd.osp.utilities.vo.AddOnVO();
	try
    {
      com.ftd.osp.utilities.cacheMgr.handlers.vo.AddonVO cacheAddOnVO = CacheUtil.getInstance().getAddOnById(addonId);

      
      if (cacheAddOnVO.getAddonPrice() == null)
      {
    	  addOnVO.setAddOnPrice("0");
      }
      else
      {
    	  addOnVO.setAddOnPrice(cacheAddOnVO.getAddonPrice().toString());
      }
      addOnVO.setAddOnId(addonId);
      addOnVO.setAddOnTypeId(cacheAddOnVO.getAddonTypeId());
      addOnVO.setAddOnText(cacheAddOnVO.getAddonText());
      addOnVO.setAddOnUNSPSC(cacheAddOnVO.getAddOnUNSPSC());
      addOnVO.setAddOnDescription(cacheAddOnVO.getAddonDesc());
      addOnVO.setAddOnWeight(cacheAddOnVO.getAddOnWeight());
      addOnVO.setAddOnAvailableFlag(StringUtils.equals("Y",cacheAddOnVO.getAddOnAvailableFlag()));
      addOnVO.setDefaultPerTypeFlag(StringUtils.equals("Y",cacheAddOnVO.getDefaultPerTypeFlag()));
      addOnVO.setDisplayPriceFlag(StringUtils.equals("Y",cacheAddOnVO.getDisplayPriceFlag()));
      addOnVO.setAddOnTypeDescription(cacheAddOnVO.getAddOnTypeDescription());
      addOnVO.setAddOnTypeIncludedInProductFeedFlag(StringUtils.equals("Y",cacheAddOnVO.getAddOnTypeIncludedInProductFeedFlag()));
      addOnVO.setProductId(cacheAddOnVO.getProductId());
      addOnVO.setOccasionId(cacheAddOnVO.getOccasionId());
      addOnVO.setOccasionDescription(cacheAddOnVO.getOccasionDesc());
     
      return addOnVO;
    }
    catch (Exception e)
    {
      throw new RuntimeException(e.getMessage(), e);
    }
  }

  /**
   * Retrieves parameters used for calculations from the global parameters table.
   * @param connection
   * @return
   */
  public RecalculateOrderGlobalParmsVO getGlobalRecalculationParameters(Connection connection)
  {
    try
    {
      RecalculateOrderGlobalParmsVO recalculateOrderGlobalParmsVO = new RecalculateOrderGlobalParmsVO();
      recalculateOrderGlobalParmsVO.setFreshCutServiceCharge(new BigDecimal(CacheUtil.getInstance().getGlobalParm("FTDAPPS_PARMS", "FRESHCUTS_SVC_CHARGE")));
      recalculateOrderGlobalParmsVO.setSpecialServiceCharge(new BigDecimal(CacheUtil.getInstance().getGlobalParm("FTDAPPS_PARMS", "SPECIAL_SVC_CHARGE")));
      return recalculateOrderGlobalParmsVO;
    }
    catch (Exception e)
    {
      throw new RuntimeException(e.getMessage(), e);
    }
  }


  /**
   * This method takes in a country code and returns
   * a boolean value indicating if the country is considered
   * domestic.
   * @param countryId
   * @return
   */
  public boolean isCountryDomestic(Connection connection, String country)
  {
    //If united states do not do a lookup
    if (country.equalsIgnoreCase("US") || country.equalsIgnoreCase("USA"))
    {
      return true;
    }

    try
    {
      CountryMasterVO cmVO = CacheUtil.getInstance().getCountryById(country);
      if (cmVO == null)
      {
        String msg = "Country code not found in cache.[" + country + "]";
        logger.error(msg);
        throw new RecalculateException(msg);
      }

      String domesticFlag = cmVO.getCountryType();
      return domesticFlag.equals(DOMESTIC)? true: false;
    }
    catch (Exception e)
    {
      throw new RuntimeException(e.getMessage(), e);
    }
  }


  /**
   *
   * @param connection
   * @param state
   * @return
   */
	public boolean isStateCompanyTaxON(String stateMasterId, String companyId) {
		try {
			return CacheUtil.getInstance().isStateCompanyTaxON(stateMasterId, companyId);
		} catch (Exception e) {
			logger.error(e);
			throw new RuntimeException(e.getMessage(), e);
		}
	}

  /**Returns the Fuel Surcharge for the Given Date.
   * @param connection
   * @param date
   * @return
   */
  public BigDecimal getFuelSurcharge(Connection connection, Date date)
  {
    try
    {
      String tempFuelSurcharge = CacheUtil.getInstance().getFuelSurcharge().toString();
      BigDecimal fuelSurcharge = new BigDecimal("0");
      if (tempFuelSurcharge != null)
      {
        fuelSurcharge = new BigDecimal(tempFuelSurcharge);
      }

      return fuelSurcharge;
    }
    catch (Exception e)
    {
      throw new RuntimeException(e.getMessage(), e);
    }
  }

  /**
   * Returns the Service Charge Date for the dates and source code provided.
   * @param connection
   * @param serviceFeeDate
   * @param sourceCode
   * @param deliveryDate
   * @return
   */
  public ServiceChargeVO getServiceChargeData(Connection connection, Date serviceFeeDate, String sourceCode, 
                                              Date deliveryDate)
  {
    try
    {
    	Timestamp orderDateTimestamp = new Timestamp(serviceFeeDate.getTime());
        java.sql.Date sqlDeliveryDate = new java.sql.Date(deliveryDate.getTime());

        if (logger.isDebugEnabled())
        {
          SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
          logger.debug("getServiceChargeData called\n" +
            "OrderDate timestamp: " + sdf.format(orderDateTimestamp.getTime()) + "\n" +
            "SourceCode: " + sourceCode + "\n" +
            "DeliveryDate: " + sqlDeliveryDate.toString());
        }
         
        SourceMasterVO smVO = new SourceMasterVO();
        smVO = CacheUtil.getInstance().getSourceCodeById(sourceCode);
        String snhId = smVO.getSnhId();
        
        //retrieve service fee data from the cache
        ServiceFeeVO sfVO = new ServiceFeeVO();
        sfVO = CacheUtil.getInstance().getServiceFeeById(snhId);
        
        //retrieve service fee override data from the cache
        com.ftd.osp.utilities.cacheMgr.handlers.vo.ServiceFeeOverrideVO sfoVO = new com.ftd.osp.utilities.cacheMgr.handlers.vo.ServiceFeeOverrideVO();
        sfoVO = CacheUtil.getInstance().getServiceFeeOverrideById(sfVO.getSnhId(), sqlDeliveryDate);
        
        //create a new instance of com.ftd.osp.utilities.vo.ServiceChargeVO
        ServiceChargeVO serviceChargeVO = new ServiceChargeVO();
               
        //if service fee override exists, populate ServiceChargeVO with values from cached ServiceFeeOverrideVO  
        if(sfoVO != null)
        {
        	serviceChargeVO.setSnhId(sfoVO.getSnhId());
            serviceChargeVO.setDomesticCharge(sfoVO.getDomesticCharge());
            serviceChargeVO.setInternationalCharge(sfoVO.getInternationalCharge());
            serviceChargeVO.setVendorCharge(sfoVO.getVendorCharge());
            serviceChargeVO.setVendorSatUpcharge(sfoVO.getVendorSatUpcharge());
            serviceChargeVO.setOverrideFlag("Y");
            serviceChargeVO.setSameDayUpcharge(sfoVO.getSameDayUpcharge());
        }
        else //populate ServiceChargeVO with values from cached ServiceFeeVO
        {
        	serviceChargeVO.setSnhId(sfVO.getSnhId());
            serviceChargeVO.setDomesticCharge(sfVO.getDomesticCharge());
            serviceChargeVO.setInternationalCharge(sfVO.getInternationalCharge());
            serviceChargeVO.setVendorCharge(sfVO.getVendorCharge());
            serviceChargeVO.setVendorSatUpcharge(sfVO.getVendorSatUpcharge());
            serviceChargeVO.setOverrideFlag("N");
            serviceChargeVO.setSameDayUpcharge(sfVO.getSameDayUpcharge());
        }       
        
        //Extra Validations to avoid a null pointer error elsewhere in code that uses this 
        if (serviceChargeVO.getSnhId() == null || serviceChargeVO.getDomesticCharge() == null || serviceChargeVO.getInternationalCharge() == null
          || serviceChargeVO.getVendorCharge() ==null || serviceChargeVO.getVendorSatUpcharge() == null || serviceChargeVO.getSameDayUpcharge() == null)
        {
          SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
          String inputParameters =  "OrderDate timestamp: " + sdf.format(orderDateTimestamp.getTime()) + "\n" +
            "SourceCode: " + sourceCode + "\n" +
            "DeliveryDate: " + sqlDeliveryDate.toString();
          if (serviceChargeVO.getSnhId() == null)    
          {
            logger.error("Error in getServiceChargeData: Null SnhId retrieved from the cache. \n" + inputParameters);
            throw (new Exception("Error in getServiceChargeData: Null SnhId retrieved from the cache. \n" + inputParameters));    
          }
          if (serviceChargeVO.getDomesticCharge() == null)
          {
            logger.error("Error in getServiceChargeData: Null DomesticCharge retrieved from the cache\n" + inputParameters);
            throw (new Exception("Error in getServiceChargeData: Null DomesticCharge retrieved from the cache. \n" + inputParameters));
          }
          if (serviceChargeVO.getInternationalCharge() == null)
          {
            logger.error("Error in getServiceChargeData: Null InternationalCharge retrieved from the cache\n" + inputParameters);
            throw (new Exception("Error in getServiceChargeData: Null InternationalCharge retrieved from the cache. \n" + inputParameters));
          }
          if (serviceChargeVO.getVendorCharge() ==null)
          {
            logger.error("Error in getServiceChargeData: Null VendorCharge retrieved from the cache\n" + inputParameters);
            throw (new Exception("Error in getServiceChargeData: Null VendorCharge retrieved from the cache. \n" + inputParameters));
          }
          if (serviceChargeVO.getVendorSatUpcharge() == null)
          {
            logger.error("Error in getServiceChargeData: Null VendorSatUpcharge retrieved from the cache\n" +inputParameters);
            throw (new Exception("Error in getServiceChargeData: Null VendorSatUpcharge retrieved from the cache. \n" + inputParameters));
          }
          if (serviceChargeVO.getSameDayUpcharge() == null)
          {
        	logger.error("Error in getServiceChargeData: Null SameDayUpcharge retrieved from the cache\n" +inputParameters);
            throw (new Exception("Error in getServiceChargeData: Null SameDayUpcharge retrieved from the cache. \n" + inputParameters));
          }
        }
        
        if (logger.isDebugEnabled())
        {
        	logger.debug("SnhId: " + serviceChargeVO.getSnhId().toString() + "\n" +
            "DomesticCharge: " +serviceChargeVO.getDomesticCharge().toString() + "\n" +
            "International Charge: " + serviceChargeVO.getInternationalCharge().toString() + "\n" +
            "Vendor Charge: " + serviceChargeVO.getVendorCharge().toString() + "\n" +
            "Vendor Saturday Charge: " + serviceChargeVO.getVendorSatUpcharge().toString()+ "\n" +
            "Override Flag: " + serviceChargeVO.getOverrideFlag() + "\n" +
            "Same Day Upcharge: " + serviceChargeVO.getSameDayUpcharge().toString());
        }        

        return serviceChargeVO;      
    }
    catch (Exception e)
    {
      throw new RuntimeException(e.getMessage(), e);
    }
  }


  /**
   * Retrieves the Global Service Fee for Same Day Gifts from the Global Parm Table.
   * Note: This implementation uses the Cache
   * @param connection
   * @return
   */
  public BigDecimal getSameDayGiftGlobalServiceFee(Connection connection)
  {
    try
    {     
        BigDecimal serviceFee = 
          new BigDecimal(CacheUtil.getInstance().getGlobalParm("PRICE_CALCULATION", "SDG_GLOBAL_SERVICE_FEE"));
        return serviceFee;      
    }
    catch (Exception e)
    {
      throw new RuntimeException("Unable to retrieve Global Service Fee: GLOBAL_PARM cache not available", e);
    }

  }

  /**
   * Returns the Payment Method Miles Points for the given Payment Method
   * @param connection
   * @param paymentMethod
   * @return
   */
  public PaymentMethodMilesPointsVO getPaymentMethodMilesPoints(Connection connection, String paymentMethod)
  {
    try
    {
      com.ftd.osp.utilities.cacheMgr.handlers.vo.PaymentMethodMilesPointsVO paymentMethodMilesPointsVO = CacheUtil.getInstance().getPaymentMethodMPById(paymentMethod);
      PaymentMethodMilesPointsVO mpvo = new PaymentMethodMilesPointsVO();
      mpvo.setPaymentMethodId(paymentMethodMilesPointsVO.getPaymentMethodId());
      mpvo.setDollarToMpOperator(paymentMethodMilesPointsVO.getDollarToMpOperator());
      mpvo.setRoundingMethodId(paymentMethodMilesPointsVO.getRoundingMethodId());
      mpvo.setRoundingScaleQty(paymentMethodMilesPointsVO.getRoundingScaleQty());
      mpvo.setCommissionPct(paymentMethodMilesPointsVO.getCommissionPct());
      return mpvo;
    }
    catch (Exception e)
    {
      logger.error(e);
      throw new RuntimeException(e.getMessage(), e);
    }

  }

  /**
   * Returns a Set containing the Active Program Names for the Apollo Account with the passed in Email Address
   * and order date.
   * 
   * @param connection
   * @param emailAddress
   * @param orderDate
   * @return
   */
  public Set<String> getActiveProgramsForAccount(Connection connection, String emailAddress, Date orderDate)
  { 
    Map paramMap = new HashMap();
    paramMap.put("IN_EMAIL_ADDRESS", emailAddress);
    paramMap.put("IN_ACTIVE_DATE", new java.sql.Timestamp(orderDate.getTime()));
    
    Set<String> retVal = new HashSet<String>();
       
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(connection);
    dataRequest.setStatementID(GET_ACCOUNT_ACTIVE_PROGRAMS_STATEMENT);
    dataRequest.setInputParams(paramMap);
    
    try
    {
    	DataAccessUtil dau = DataAccessUtil.getInstance();
    	CachedResultSet rs = (CachedResultSet)dau.execute(dataRequest);
        dataRequest.reset();
        while(rs.next()) {
        	String programName = rs.getString("program_name");
            
            if(!StringUtils.isEmpty(programName))
            {
              retVal.add(programName);
            }
        }
           
      return retVal;
    }
    catch (Exception e)
    {
      throw new RuntimeException(e.getMessage(), e);
    } 
  }
}

