package com.ftd.osp.utilities.cacheMgr.exception;

public class CacheException extends RuntimeException
{
    private static final long serialVersionUID = 1L;

    public CacheException() 
    {
        super();
    }

   /**
   * Constructs a new CacheException with a message string
   * @param message
   */
    public CacheException(String message)
    {
        super(message);
    }
    
  /**
   * Constructs a CacheException with a message string, and a base exception
   * 
   * @param message
   * @param ex
   */
    public CacheException(String message, Exception ex)
    {
      super(message, ex);
    }
    
}