package com.ftd.osp.utilities.cacheMgr;

import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.plugins.Logger;
import java.util.TimerTask;
import java.io.IOException;

/**
 *  Class to handle administration tasks for the CacheManager.
 *  There should only be a single instance of this class running (per JVM) since it's
 *  kicked off through the CacheManager singleton.
 */
public class CacheAdminTimer extends TimerTask
{
    private static float STATS_DUMP_RATE = 45.0F;  // How often cache stats are dumped to logs (in minutes)
    private static Logger logger = new Logger("com.ftd.osp.utilities.cacheMgr.CacheAdminTimer");  
    private static long configLastModified = 0;
    private static long lastTimeStatsDumped = System.currentTimeMillis();
  
    /**
    * The constructor
    */
    public CacheAdminTimer() {
        super();
    }
  
    /**
    * Checks if cache config file changed since last run.  If so, reload it.
    * No need to synchronize since only one instance of this class should be running.
    */
    private void checkCacheConfigAndReload() {
        try {
            if (CacheManager.isTerminateThreads()) {
                if(logger.isInfoEnabled())
                  logger.info("Cache Admin thread cancelling itself since terminate flag was set");
                this.cancel();
                
            } else if ((configLastModified != 0) && (configLastModified != CacheManager.getCacheConfigLastModified())) {
    
                // Config file changed so reload it
                if(logger.isInfoEnabled())
                  logger.info("Changes to cache config file were detected, so reloading");
                CacheManager.loadCacheConfig();
                configLastModified = CacheManager.getCacheConfigLastModified();
    
            } else if (configLastModified == 0) {
            
                // Since 0, must be our first run so set to current file modification time
                configLastModified = CacheManager.getCacheConfigLastModified();
    
            } else {
                // File has not changed, so just dump stats (occasionally)
                float elapsedStatTime = (System.currentTimeMillis() - lastTimeStatsDumped)/(60*1000F);  // Elapsed time since last stats dump (in minutes)
                if (elapsedStatTime >= STATS_DUMP_RATE) {
                    lastTimeStatsDumped = System.currentTimeMillis();
                    if(logger.isInfoEnabled())
                      logger.info("Cache Statistics: \n" + CacheManager.getInstance().dumpAllCachesStr());
                }
            }
            
            if (CacheManager.isSpawnRefreshThread()) {
                // This means a Fixed cache was lazy-loaded, so it's Refresh Timer thread needs to be started
                CacheManager.checkAndSpawnMissingRefreshTimers();    
            }
            
        } catch (IllegalStateException ise) {
            logger.error("Cache admin thread cancelling itself (most likely due to application un-deployment).  " +
                         "Setting flags so other threads will terminate on waking too.  Error triggering this was: " + ise);
            CacheManager.setTerminateThreads(true);
            this.cancel();
        } catch (com.sun.org.apache.xml.internal.dtm.DTMConfigurationException dtme) {
            logger.error("Cache admin thread cancelling itself (most likely due to application un-deployment).  " +
                         "Setting flags so other threads will terminate on waking too.  Error triggering this was: " + dtme);
            CacheManager.setTerminateThreads(true);
        } catch (Exception e) {
            logger.error("Cache admin thread cancelling itself due to error: " + e);
            this.cancel();
        }
    }
    
    /**
     * Logic executed when timer triggers an instance of this class
     */
    public void run() {
        if (logger.isDebugEnabled()) {logger.debug("Cache Administration thread is alive");}
        checkCacheConfigAndReload();
    }
}