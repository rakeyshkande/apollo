package com.ftd.osp.utilities.cacheMgr.handlers;

import java.sql.Connection;

import org.w3c.dom.Document;

import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.cacheMgr.vo.CacheHandlerBase;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;

/**
 * A custom cache handler for loading the active occasions
 * 
 * @author kdatchan
 *
 */
public class ActiveOccasionHandler extends CacheHandlerBase {
	// set by the setCachedObject method
	private Document occasionDocument;

	private static final String GET_ACTIVE_OCCASIONS = "GET_ACTIVE_OCCASIONS";

	public ActiveOccasionHandler() {
	}


	/*
	 * (non-Javadoc)
	 * @see com.ftd.osp.utilities.cacheMgr.vo.CacheHandlerBase#load(java.sql.Connection)
	 */
	public Object load(Connection con) throws CacheException {
		try {
			super.logger.debug("BEGIN LOADING ACTIVE OCCASION HANDLER...");
			DataAccessUtil dau = DataAccessUtil.getInstance();
			DataRequest dataRequest = new DataRequest();
			dataRequest.setConnection(con);
			dataRequest.setStatementID(GET_ACTIVE_OCCASIONS);
			occasionDocument = (Document) dau.execute(dataRequest);
			super.logger.debug("END LOADING ACTIVE OCCASION HANDLER...");
		} catch (Exception e) {
			e.printStackTrace();
			throw new CacheException("Could not load Active OccasionHandler cache");
		}

		return occasionDocument;
	}

	/*
	 * (non-Javadoc)
	 * @see com.ftd.osp.utilities.cacheMgr.vo.CacheHandlerBase#setCachedObject(java.lang.Object)
	 */
	public void setCachedObject(Object cachedObject) throws CacheException {
		try {
			this.occasionDocument = (Document) cachedObject;
		} catch (Exception ex) {
			super.logger.error(ex);
			throw new CacheException("Could not set the cached object.");
		}
	}

	/**
	 * 
	 * @return
	 */
	public Document getOccasionDocument() {
		return this.occasionDocument;
	}

}