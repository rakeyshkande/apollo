package com.ftd.osp.utilities.cacheMgr.handlers.vo;

import java.math.BigDecimal;

public class ServiceFeeVO
{
    private String snhId;
    private BigDecimal domesticCharge;
    private BigDecimal internationalCharge;
    private BigDecimal vendorCharge;
    private BigDecimal vendorSatUpcharge;
    private int hashcode;
    private BigDecimal sameDayUpcharge;

    public ServiceFeeVO() {}

    public ServiceFeeVO(String snhId) {
        this.snhId = snhId;
    }

    public void setSnhId(String snhId) {
        this.snhId = snhId;
    }

    public String getSnhId() {
        return snhId;
    }

    public void setDomesticCharge(BigDecimal domesticCharge) {
        this.domesticCharge = domesticCharge;
    }

    public BigDecimal getDomesticCharge() {
        return domesticCharge;
    }

    public void setInternationalCharge(BigDecimal internationalCharge) {
        this.internationalCharge = internationalCharge;
    }

    public BigDecimal getInternationalCharge() {
        return internationalCharge;
    }

    public void setVendorCharge(BigDecimal vendorCharge) {
        this.vendorCharge = vendorCharge;
    }

    public BigDecimal getVendorCharge() {
        return vendorCharge;
    }

    public void setVendorSatUpcharge(BigDecimal vendorSatUpcharge) {
        this.vendorSatUpcharge = vendorSatUpcharge;
    }

    public BigDecimal getVendorSatUpcharge() {
        return vendorSatUpcharge;
    }

    //overwriting hashCode() of Object
    public int hashCode() {
        if(this.hashcode != 0) {
            return this.hashcode;
        } else {
            hashcode = snhId.hashCode();
        }
        return hashcode;
    }
    //overwirting equals() of Ojbect
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ServiceFeeVO obj = (ServiceFeeVO)o;

        if(obj.getSnhId() == null || !obj.getSnhId().equals(this.getSnhId()))
            return false;
        return true;
    }

    public ServiceFeeVO clone() {
        ServiceFeeVO sfvo = new ServiceFeeVO();
        sfvo.setSnhId(this.getSnhId());
        sfvo.setDomesticCharge(this.getDomesticCharge());
        sfvo.setInternationalCharge(this.getInternationalCharge());
        sfvo.setVendorCharge(this.getVendorCharge());
        sfvo.setVendorSatUpcharge(this.getVendorSatUpcharge());
        sfvo.setSameDayUpcharge(this.getSameDayUpcharge());

        return sfvo;
    }

	public BigDecimal getSameDayUpcharge() {
		return sameDayUpcharge;
	}

	public void setSameDayUpcharge(BigDecimal sameDayUpcharge) {
		this.sameDayUpcharge = sameDayUpcharge;
	}

}
