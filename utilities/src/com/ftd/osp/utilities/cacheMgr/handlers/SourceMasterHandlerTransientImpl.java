package com.ftd.osp.utilities.cacheMgr.handlers;

import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.constant.CacheMgrConstants;
import com.ftd.osp.utilities.cacheMgr.dao.CacheMgrDAO;
import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.SourceMasterVO;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;

import java.sql.Connection;
import java.util.HashMap;

/**
 * Smart lazy loading of the Source Codes using the Transient Handler
 */
public class SourceMasterHandlerTransientImpl extends TransientCacheHandler implements SourceMasterHandler
{
    public SourceMasterVO getSourceCodeById(String sourceCode) throws CacheException
    {
        SourceMasterVO sourceMasterVO = null;

        try
        {
            // Get the object from cache
        	long time = System.currentTimeMillis();
            sourceMasterVO = (SourceMasterVO)getCacheElement(sourceCode);
            
            if(sourceMasterVO == null) {
            	throw new CacheException("Object not found in cache.");
            }
            if(logger.isDebugEnabled()) {
            	logger.debug("Time taken to retrieve from transient cache source code " + sourceMasterVO.getSourceCode() + ":"+ (System.currentTimeMillis() - time)+ "ms");
            }
        }
        catch (CacheException ce)
        {
            // No object in the cache, get the object from the db and load in cache
        	long time = System.currentTimeMillis();
            sourceMasterVO = loadSourceCode(sourceCode);
            if(logger.isDebugEnabled()) {
            	logger.debug("Time taken to hit DB for source code " + sourceMasterVO.getSourceCode() + ":"+ (System.currentTimeMillis() - time)+ "ms");
            }
            addCacheElement(sourceCode, sourceMasterVO);
        }

        return sourceMasterVO;
    }

    protected SourceMasterVO loadSourceCode(String sourceCode) throws CacheException
    {
        SourceMasterVO sourceMasterVO = null;
        Connection conn = null;

        try
        {
            conn = CacheManager.getInstance().getConnection();
            HashMap inputParams = new HashMap();
            inputParams.put("IN_SOURCE_CODE", sourceCode);

            CacheMgrDAO dao = new CacheMgrDAO();
            
            CachedResultSet rs = (CachedResultSet)dao.executeQueryReturnObject(conn, CacheMgrConstants.STMT_GET_SOURCE_CODE, inputParams);

            if (rs != null && rs.next())
            {
                sourceMasterVO = new SourceMasterVO(rs);
            }
            
        }
        catch (Exception e)
        {
            logger.error(e);
        }
        finally
        {
            if (conn != null)
            {
                try
                {
                    conn.close();
                }
                catch (Exception e)
                {
                    // Nothing we can do
                }
            }

        }

        return sourceMasterVO;
    }
}
