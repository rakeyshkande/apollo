package com.ftd.osp.utilities.cacheMgr.handlers;

import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.ElementConfigVO;
import com.ftd.osp.utilities.cacheMgr.vo.CacheHandlerBase;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.json.JSONUtil;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.JAXPUtil;

import java.math.BigDecimal;

import java.sql.Connection;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import org.w3c.dom.Document;
import org.w3c.dom.Element;


public class ElementConfigHandler extends CacheHandlerBase {
    private Map elements;

    private static final String GET_ELEMENTS = "GET_ELEMENTS";
    private static final String XML = "_XML";
    private static final String JSON = "_JSON";
    private static final String ROW = "_ROW";
    private static final String XML_RS = "_XML_RS";

    public ElementConfigHandler() {
    }

    /**
     * Returns the object that needs to be cached.
     * 
     * The cache cannot be configured as distributable, if the cache handler 
     * returns a custom object. In order to utilize the benefits of a distributable 
     * cache, the return objects should only be a part JDK API.
     * 
     * @param con
     * @return the object to be cached
     * @throws com.ftd.osp.utilities.cache.exception.CacheException
     */
    public Object load(Connection con) throws CacheException {
        Document doc, initialDoc;
        Element initialRs, rs, record;
        BigDecimal testValue;
        int rowCtr, cntr;

        // Contains element information
        Map elements = new HashMap();
        
        // Holds state of objects while building cache
        Map state = new HashMap();

        try {
            if(super.logger.isDebugEnabled())
                super.logger.debug("BEGIN LOADING ELEMENTS HANDLER...");

            DataAccessUtil dau = DataAccessUtil.getInstance();
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(con);
            dataRequest.setStatementID(GET_ELEMENTS);
            CachedResultSet crs = (CachedResultSet)dau.execute(dataRequest);

            ElementConfigVO elementVO = null;
            while (crs.next()) {
                cntr = 1;
                
                // Create element information
                elementVO = new ElementConfigVO();
                elementVO.setAppCode((String)crs.getObject(cntr++));
                elementVO.setElementId((String)crs.getObject(cntr++));
                elementVO.setLocationDescription((String)crs.getObject(cntr++));
                elementVO.setLabelElementId((String)crs.getObject(cntr++));
                elementVO.setCountElementId((String)crs.getObject(cntr++));
                elementVO.setRequiredFlag(StringUtils.equals((String)crs.getObject(cntr++), 
                                                             "Y"));
                elementVO.setRequiredCondition((String)crs.getObject(cntr++));
                elementVO.setRequiredContitionType((String)crs.getObject(cntr++));
                elementVO.setValidateFlag(StringUtils.equals((String)crs.getObject(cntr++), 
                                                             "Y"));
                elementVO.setValidateExpression((String)crs.getObject(cntr++));
                elementVO.setValidateExpressionType((String)crs.getObject(cntr++));
                elementVO.setErrorText((String)crs.getObject(cntr++));
                elementVO.setTitleText((String)crs.getObject(cntr++));
                elementVO.setMaxLength((BigDecimal)crs.getObject(cntr++));
                elementVO.setTabIndex((BigDecimal)crs.getObject(cntr++));
                elementVO.setTabInitialIndex((BigDecimal)crs.getObject(cntr++));
                elementVO.setTabCondition((String)crs.getObject(cntr++));
                elementVO.setTabConditionType((String)crs.getObject(cntr++));
                elementVO.setAccordionIndex((BigDecimal)crs.getObject(cntr++));
                elementVO.setTabControlIndex((BigDecimal)crs.getObject(cntr++));
                elementVO.setRecalcFlag(StringUtils.equals((String)crs.getObject(cntr++), 
                                                           "Y"));
                elementVO.setProductAvailabilityFlag(StringUtils.equals((String)crs.getObject(cntr++), 
                                                                        "Y"));
                elementVO.setAccessKey((String)crs.getObject(cntr++));
                elementVO.setXmlNodeName((String)crs.getObject(cntr++));
                elementVO.setCalcXmlFlag(StringUtils.equals((String)crs.getObject(cntr++), 
                                                            "Y"));

                // Initialize if the app code does not exist
                if( !elements.containsKey(elementVO.getAppCode()) ) {
                    elements.put(elementVO.getAppCode(), new HashMap());
  
                    initialDoc = JAXPUtil.createDocument();
                    initialDoc.appendChild(initialDoc.createElement("result"));
                    JAXPUtil.addAttribute(initialDoc.getDocumentElement(),"status","Y");
                    JAXPUtil.addAttribute(initialDoc.getDocumentElement(),"message","");
                    initialRs = initialDoc.createElement("rs");
                    JAXPUtil.addAttribute(initialRs,"name","element_config");
                    JAXPUtil.addAttribute(initialRs,"status","Y");
                    JAXPUtil.addAttribute(initialRs,"message","");
                    initialDoc.getDocumentElement().appendChild(initialRs);
  
                    elements.put(elementVO.getAppCode() + XML, initialDoc);
                    state.put(elementVO.getAppCode() + XML_RS, initialRs);
                    state.put(elementVO.getAppCode() + ROW, new Integer(0));
                }

                // Store the element data
                ((HashMap)elements.get(elementVO.getAppCode())).put(elementVO.getElementId(), elementVO);

                // Convert element data into XML
                doc = (Document)elements.get(elementVO.getAppCode() + XML);
                record = doc.createElement("record");
                rowCtr = ((Integer)state.get(elementVO.getAppCode() + ROW)).intValue();
                JAXPUtil.addAttribute(record,"row",String.valueOf(++rowCtr));
                state.put(elementVO.getAppCode() + ROW, new Integer(rowCtr));

                rs = (Element)state.get(elementVO.getAppCode() + XML_RS);
                rs.appendChild(record);
                
                record.appendChild(JAXPUtil.buildSimpleXmlNode(doc,"application",elementVO.getAppCode()));
                record.appendChild(JAXPUtil.buildSimpleXmlNode(doc,"element_id",elementVO.getElementId()));
                record.appendChild(JAXPUtil.buildSimpleXmlNode(doc,"type",elementVO.getLocationDescription()));
                record.appendChild(JAXPUtil.buildSimpleXmlNode(doc,"label_element_id",elementVO.getLabelElementId()));
                record.appendChild(JAXPUtil.buildSimpleXmlNode(doc,"count_element_id",elementVO.getCountElementId()));
                record.appendChild(JAXPUtil.buildSimpleXmlNode(doc,"required_flag",elementVO.isRequiredFlag()?"Y":"N"));
                record.appendChild(JAXPUtil.buildSimpleXmlNode(doc,"required_condition",elementVO.getRequiredCondition()));
                record.appendChild(JAXPUtil.buildSimpleXmlNode(doc,"required_condition_type",elementVO.getRequiredContitionType()));
                record.appendChild(JAXPUtil.buildSimpleXmlNode(doc,"validate_flag",elementVO.isValidateFlag()?"Y":"N"));
                record.appendChild(JAXPUtil.buildSimpleXmlNode(doc,"validate_expression",elementVO.getValidateExpression()));
                record.appendChild(JAXPUtil.buildSimpleXmlNode(doc,"validate_expression_type",elementVO.getValidateExpressionType()));
                record.appendChild(JAXPUtil.buildSimpleXmlNode(doc,"error_text",elementVO.getErrorText()));
                record.appendChild(JAXPUtil.buildSimpleXmlNode(doc,"title",elementVO.getTitleText()));
                testValue = elementVO.getMaxLength();
                record.appendChild(JAXPUtil.buildSimpleXmlNode(doc,"max_size",testValue==null?"":testValue.toString()));
                testValue = elementVO.getTabIndex();
                record.appendChild(JAXPUtil.buildSimpleXmlNode(doc,"tab_idx",testValue==null?"":testValue.toString()));
                testValue = elementVO.getTabInitialIndex();
                record.appendChild(JAXPUtil.buildSimpleXmlNode(doc,"tab_idx_initial",testValue==null?"":testValue.toString()));
                record.appendChild(JAXPUtil.buildSimpleXmlNode(doc,"tab_idx_expression",elementVO.getTabCondition()));
                record.appendChild(JAXPUtil.buildSimpleXmlNode(doc,"tab_idx_expression_type",elementVO.getTabConditionType()));
                testValue = elementVO.getAccordionIndex();
                record.appendChild(JAXPUtil.buildSimpleXmlNode(doc,"accordion_ctrl_idx",testValue==null?"":testValue.toString()));
                testValue = elementVO.getTabControlIndex();
                record.appendChild(JAXPUtil.buildSimpleXmlNode(doc,"tab_ctrl_idx",testValue==null?"":testValue.toString()));
                record.appendChild(JAXPUtil.buildSimpleXmlNode(doc,"recalc_flag",elementVO.isRecalcFlag()?"Y":"N"));
                record.appendChild(JAXPUtil.buildSimpleXmlNode(doc,"product_validate_flag",elementVO.isProductAvailabilityFlag()?"Y":"N"));
                record.appendChild(JAXPUtil.buildSimpleXmlNode(doc,"access_key",elementVO.getAccessKey()));
                record.appendChild(JAXPUtil.buildSimpleXmlNode(doc,"order_xml_node",elementVO.getXmlNodeName()));
                record.appendChild(JAXPUtil.buildSimpleXmlNode(doc,"calc_xml_flag",elementVO.isCalcXmlFlag()?"Y":"N"));
            }
            
            // Create JSON data
            String appCode;
            Object[] keys = elements.keySet().toArray();
            for(int i = 0; i < keys.length; i++) {
                appCode = (String)keys[i];
                if(appCode.indexOf(XML) == -1) {
                    doc = (Document)elements.get(appCode + XML);
                    elements.put(appCode + JSON, JSONUtil.xmlToJSON(doc));
                }
            }

            if(super.logger.isDebugEnabled())
                super.logger.debug("END LOADING ELEMENTS HANDLER...");

        } catch (Exception e) {
            super.logger.error(e);
            throw new CacheException("Could not load element cache.");
        }

        if(super.logger.isDebugEnabled()) {
            String appCode = null;
            int cnt = 0;
            for(Iterator<String> it = elements.keySet().iterator(); it.hasNext();) {
                appCode = it.next();
                if(appCode.indexOf(XML) == -1 && appCode.indexOf(JSON) == -1) {
                    cnt = ((HashMap)elements.get(appCode)).size();
                    super.logger.debug(cnt + " records loaded for appcode " + appCode);
                }
            }
        }

        return elements;
    }


    /**
     * Set the cached object in the cache handler. The cache handler is then 
     * responsible for fulfilling all application level API calls, to access the
     * data in the cached object.
     * 
     * @param cachedObject
     * @throws com.ftd.osp.utilities.cache.exception.CacheException
     */
    public void setCachedObject(Object cachedObject) throws CacheException {
        try {
            this.elements = (Map)cachedObject;
        } catch (Exception ex) {
            super.logger.error(ex);
            throw new CacheException("Could not set the cached object.");
        }
    }

  /**
   * Returns element config data for the requested appcode.
   * @param appCode
   * @return Map - Map of element config data by element id
   * @throws Exception
   */
  public Map getElements(String appCode) {
      if(elements.containsKey(appCode))
        return (Map)elements.get(appCode);
      else
          return new HashMap();
  }

  /**
   * Returns element config data as XML for the requested appcode.
   * @param appCode
   * @return Document - Document of element config data
   * @throws Exception
   */
  public Document getElementsXml(String appCode) throws Exception {
      if(elements.containsKey(appCode + XML))
          return (Document)elements.get(appCode + XML);
      else
          return JAXPUtil.createDocument();
  }

  /**
   * Returns element config data as a JSON string for the requested appcode.
   * @param appCode
   * @return String - JSON string of element config data
   * @throws Exception
   */
  public String getElementsJSON(String appCode) throws Exception {
      if(elements.containsKey(appCode + JSON))
          return (String)elements.get(appCode + JSON);
      else
          return "";
  }
}
