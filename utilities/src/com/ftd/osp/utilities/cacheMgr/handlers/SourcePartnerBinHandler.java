package com.ftd.osp.utilities.cacheMgr.handlers;

import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.cacheMgr.vo.CacheHandlerBase;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;

import java.sql.Connection;

import java.util.HashMap;
import java.util.Iterator;

public class SourcePartnerBinHandler extends CacheHandlerBase {

    private HashMap<String,HashMap<String,String>> sourceBinMap = null;
    
    public SourcePartnerBinHandler() {
    }

    /**
    * Returns the object that needs to be cached.
    * 
    * The cache cannot be configured as distributable, if the cache handler 
    * returns a custom object. In order to utilize the benefits of a distributable 
    * cache, the return objects should only be a part JDK API.
    * 
    * @param con
    * @return the object to be cached
    * @throws com.ftd.osp.utilities.cache.exception.CacheException
    */
    public Object load(Connection con) throws CacheException {

        HashMap<String,HashMap<String,String>> sourceBinMapCacheObject = null;

        try {
            super.logger.debug("BEGIN LOADING SOURCE PARTNER BIN HANDLER...");

            //Map of source codes and associated bins
            sourceBinMapCacheObject = new HashMap<String, HashMap<String,String>>();

            // Loop through all PARTNER_MASTER records that have the BIN_PROCESSING_FLAG set
            // to 'Y' and then build a hashmap of each partner's active bin numbers

            DataAccessUtil dau = DataAccessUtil.getInstance();
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(con);
            dataRequest.setStatementID("GET_BIN_PROCESSING_PARTNERS");
            CachedResultSet rs = (CachedResultSet)dau.execute(dataRequest);
            
            HashMap<String,HashMap<String,String>> partnerMap = new HashMap<String,HashMap<String,String>>();

            while (rs.next()) {
                String partnerName = rs.getString("partner_name");
            
                HashMap inputParams = new HashMap();
                inputParams.put("IN_PARTNER_NAME", partnerName);

                dataRequest.setInputParams(inputParams);
                dataRequest.setStatementID("GET_BIN_MASTER_BY_PARTNER");
                CachedResultSet rs1 = (CachedResultSet)dau.execute(dataRequest);
                
                HashMap<String,String> bins = new HashMap<String,String>();
                
                while (rs1.next()) {
                    String bin = rs1.getString("cc_bin_number");
                    bins.put(bin, partnerName);
                }
                
                partnerMap.put(partnerName, bins);
                
            }

            // Retrieve all active SOURCE_PARTNER_BIN_MAPPING records and populate the
            // sourceBinMapCacheObject hashmap with all of the bin numbers for each partner.

            dataRequest.setInputParams(null);
            dataRequest.setStatementID("GET_SOURCE_PARTNER_BIN_MAP");
            CachedResultSet rs2 = (CachedResultSet)dau.execute(dataRequest);

            while (rs2.next()) {
                String sourceCode = rs2.getString("source_code");
                String sourcePartner = rs2.getString("partner_name");
                HashMap<String,String> partnerBins = partnerMap.get(sourcePartner);

                Iterator itr = partnerBins.keySet().iterator();
                while(itr.hasNext()) {
                    String tempBin = (String) itr.next();
                    HashMap<String,String> bins = new HashMap<String,String>();
                    if (sourceBinMapCacheObject.get(sourceCode) != null) {
                        bins = sourceBinMapCacheObject.get(sourceCode);
                    }
                    bins.put(tempBin, sourcePartner);
                    sourceBinMapCacheObject.put(sourceCode, bins);
                }

            }

            super.logger.debug("END LOADING SOURCE PARTNER BIN HANDLER...");

        } catch(Exception e) {
          super.logger.error(e);
          throw new CacheException("Could not load SourcePartnerBinHandler cache.");
        }   
          
        return sourceBinMapCacheObject;      
    }

    /**
     * Set the cached object in the cache handler. The cache handler is then 
     * responsible for fulfilling all application level API calls, to access the
     * data in the cached object.
     * 
     * @param cachedObject
     * @throws com.ftd.osp.utilities.cache.exception.CacheException
     */    
    public void setCachedObject(Object cachedObject) throws CacheException {
        try {
            sourceBinMap = (HashMap<String,HashMap<String,String>>) cachedObject;  
        } catch (Exception ex) {
            super.logger.error(ex);
            throw new CacheException("Could not set the cached object.");
        } 
    }

    /**
     * 
     * @param sourceCode
     * @param ccNumber
     * @return the partner name for the source bin mapping, if one exists
     */
      public String getSourceBinPartner(String sourceCode, String ccNumber) {

          String partnerName = null;
          String bin4 = "";
          String bin5 = "";
          String bin6 = "";
          if (ccNumber != null && ccNumber.length() >= 4) {
              bin4 = ccNumber.substring(0,4);
              if (ccNumber.length() >= 5) {
                  bin5 = ccNumber.substring(0,5);
                  if (ccNumber.length() >= 6) {
                      bin6 = ccNumber.substring(0,6);
                  }
              }
          }

          if (sourceBinMap.containsKey(sourceCode)) {
              super.logger.debug("source code found");
              HashMap<String,String> bins = sourceBinMap.get(sourceCode);
              if (bins.containsKey(bin6)) {
                  partnerName = bins.get(bin6);
              } else if (bins.containsKey(bin5)) {
                  partnerName = bins.get(bin5);
              } else if (bins.containsKey(bin4)) {
                  partnerName = bins.get(bin4);
              }
          }
          super.logger.debug("partnerName: " + partnerName);
          return partnerName;

      }

}
