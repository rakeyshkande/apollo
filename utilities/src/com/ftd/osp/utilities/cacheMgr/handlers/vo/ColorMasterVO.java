package com.ftd.osp.utilities.cacheMgr.handlers.vo;


public class ColorMasterVO
{
    private String colorId;
    private String colorDesc;

    public void setColorId(String colorId) {
        this.colorId = colorId;
    }

    public String getColorId() {
        return colorId;
    }

    public void setColorDesc(String colorDesc) {
        this.colorDesc = colorDesc;
    }

    public String getColorDesc() {
        return colorDesc;
    }
}
