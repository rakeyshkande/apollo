package com.ftd.osp.utilities.cacheMgr.handlers.vo;


import com.ftd.osp.utilities.cacheMgr.constant.CacheMgrConstants;
import com.ftd.osp.utilities.cacheMgr.dao.CacheMgrDAO;
import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.cacheMgr.vo.CacheHandlerBase;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;

import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**

 */
public class TransientOccasionVO
{
    private String clientId;
    private String sourceCode;
    private int hashcode;
    
    public TransientOccasionVO (String clientId, String sourceCode) {
        this.clientId = clientId;
        this.sourceCode = sourceCode;
    }

    //overwriting hashCode() of Object
    public int hashCode() {
        if(this.hashcode != 0) {
            return this.hashcode;
        } else {
            // hashcode = clientId.hashCode() + sourceCode.hashCode();
            StringBuilder hashCodeBuilder = new StringBuilder(clientId).append("|").append(sourceCode);
            hashcode = hashCodeBuilder.toString().hashCode();
        }
        return hashcode;
    }
    
    //overwirting equals() of Ojbect
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        
        TransientOccasionVO obj = (TransientOccasionVO)o;
        
        if(this.getClientId() == null || obj.getClientId() == null)
            return false;
        if(!this.getClientId().equals(obj.getClientId()))
            return false;
        if(this.getSourceCode() == null || obj.getSourceCode() == null)
            return false;
        if(!this.getSourceCode().equals(obj.getSourceCode()))
            return false;
        return true;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientId() {
        return clientId;
    }

    public void setSourceCode(String sourceCode) {
        this.sourceCode = sourceCode;
    }

    public String getSourceCode() {
        return sourceCode;
    }

}
