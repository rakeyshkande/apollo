package com.ftd.osp.utilities.cacheMgr.handlers;

import com.ftd.osp.utilities.cacheMgr.constant.CacheMgrConstants;
import com.ftd.osp.utilities.cacheMgr.dao.CacheMgrDAO;
import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.ShippingKeyDetailVO;
import com.ftd.osp.utilities.cacheMgr.vo.CacheHandlerBase;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;

import java.math.BigDecimal;

import java.sql.Connection;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;


/**
 * This class is responsible for loading shipping key detail into cache.
 * key: snhId
 * value: ServiceFeeVO
 */
public class ShippingKeyDetailHandler extends CacheHandlerBase
{
    private Map objMap;
    public Object load(Connection conn) throws CacheException {
        logger = new Logger(this.getClass().getName());
        CacheMgrDAO osdao = new CacheMgrDAO();
        CachedResultSet rs = osdao.executeQuery(conn, CacheMgrConstants.STMT_GET_SHIPPING_KEY_DETAIL);

        Map handlerMap = new HashMap();
        Map shippingKeyMap = new HashMap();
        
        if(logger.isInfoEnabled())
          logger.info("Loading shipping key details...");
        while (rs != null && rs.next()) {   
            
            ShippingKeyDetailVO skdVO = new ShippingKeyDetailVO();
            skdVO.setShippingKeyId(rs.getString("shipping_key_id"));
            skdVO.setShippingDetailId(rs.getString("shipping_detail_id"));
            skdVO.setMinPrice(rs.getBigDecimal("min_price"));
            skdVO.setMaxPrice(rs.getBigDecimal("max_price"));
            shippingKeyMap.put(skdVO, skdVO);
        }
        
        handlerMap.put(CacheMgrConstants.CACHE_KEY_ID_GEN, shippingKeyMap);
        return handlerMap;
    }
    
    /**
     * Set the cached object in the cache handler. The cache handler is then 
     * responsible for fulfilling all application level API calls, to access the
     * data in the cached object.
     * 
     * @param cachedObject
     * @throws com.ftd.osp.utilities.cache.exception.CacheException
     */    
      public void setCachedObject(Object cachedObject) throws CacheException
      {
        try 
        {
            this.objMap = (Map)((Map) cachedObject).get(CacheMgrConstants.CACHE_KEY_ID_GEN);
        } catch (Exception ex) 
        {
          super.logger.error(ex);
          throw new CacheException("Could not set the cached object.");
        } 
      }
      
      public String getShippingDetailId(String shippingKeyId, BigDecimal price) throws CacheException
      {
          String shippingDetailId = null;
          price = price.setScale(2, BigDecimal.ROUND_HALF_UP);
          
          try {
              Set key = this.objMap.keySet();
              Iterator i = key.iterator();
              while (i.hasNext()) {
                 ShippingKeyDetailVO k = (ShippingKeyDetailVO)i.next();
                 ShippingKeyDetailVO skdvo = (ShippingKeyDetailVO)objMap.get(k); 
                  if (skdvo.getShippingKeyId().equals(shippingKeyId) &&
                         (skdvo.getMinPrice().compareTo(price) <= 0) &&
                         (skdvo.getMaxPrice().compareTo(price) >= 0)) {
                      shippingDetailId = skdvo.getShippingDetailId();
                      continue;
                  }
              }
          } catch(Exception e) {
              logger.error(e);
              throw new CacheException(e.getMessage(), e);
          }

          return shippingDetailId;
      }
      
      
      public ShippingKeyDetailVO getShippingKeyDetails(String shippingKeyId, BigDecimal price) throws CacheException {
          ShippingKeyDetailVO shippingDetail = null;
          price = price.setScale(2, BigDecimal.ROUND_HALF_UP);
          
          try {
              Set key = this.objMap.keySet();
              Iterator i = key.iterator();
              while (i.hasNext()) {
                 ShippingKeyDetailVO k = (ShippingKeyDetailVO)i.next();
                 ShippingKeyDetailVO skdvo = (ShippingKeyDetailVO)objMap.get(k); 
                  if (skdvo.getShippingKeyId().equals(shippingKeyId) &&
                         (skdvo.getMinPrice().compareTo(price) <= 0) &&
                         (skdvo.getMaxPrice().compareTo(price) >= 0)) {
                      shippingDetail = skdvo;
                      continue;
                  }
              }
          } catch(Exception e) {
              logger.error(e);
              throw new CacheException(e.getMessage(), e);
          }

          return shippingDetail;          
          
      }
      
      
      

}
