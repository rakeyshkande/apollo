package com.ftd.osp.utilities.cacheMgr.handlers;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.ftd.osp.utilities.cacheMgr.constant.CacheMgrConstants;
import com.ftd.osp.utilities.cacheMgr.dao.CacheMgrDAO;
import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.PaymentMethodMilesPointsVO;
import com.ftd.osp.utilities.cacheMgr.vo.CacheHandlerBase;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;


/**
 * This class is responsible for loading payment method miles points into cache.
 * value: PaymentMethodMilesPointsVO
 */
public class PaymentMethodMPHandler extends CacheHandlerBase
{
    private Map objMap;
    public Object load(Connection conn) throws CacheException {
        logger = new Logger(this.getClass().getName());
        CacheMgrDAO osdao = new CacheMgrDAO();
        CachedResultSet rs = osdao.executeQuery(conn, CacheMgrConstants.STMT_GET_PAYMENT_METHOD_MP);

        Map handlerMap = new HashMap();
        Map paymentMethodMPMap = new HashMap();
        
        
        if(logger.isInfoEnabled())
          logger.info("Loading payment method miles points...");
        while (rs != null && rs.next()) {   
        	PaymentMethodMilesPointsVO paymentMethodMilesPointsVO = new PaymentMethodMilesPointsVO();
        	paymentMethodMilesPointsVO.setPaymentMethodId(rs.getString("payment_method_id"));
        	paymentMethodMilesPointsVO.setDollarToMpOperator(rs.getString("dollar_to_mp_operator"));
        	paymentMethodMilesPointsVO.setRoundingMethodId(rs.getString("rounding_method_id"));
        	paymentMethodMilesPointsVO.setRoundingScaleQty(rs.getString("rounding_scale_qty"));
        	paymentMethodMilesPointsVO.setCommissionPct(rs.getString("commission_pct"));
            paymentMethodMPMap.put(paymentMethodMilesPointsVO.getPaymentMethodId(), paymentMethodMilesPointsVO);
        }
        
        handlerMap.put(CacheMgrConstants.CACHE_KEY_ID_GEN, paymentMethodMPMap);
        return handlerMap;
    }
    
    /**
     * Set the cached object in the cache handler. The cache handler is then 
     * responsible for fulfilling all application level API calls, to access the
     * data in the cached object.
     * 
     * @param cachedObject
     * @throws com.ftd.osp.utilities.cache.exception.CacheException
     */    
      public void setCachedObject(Object cachedObject) throws CacheException
      {
        try 
        {
            this.objMap = (Map)((Map) cachedObject).get(CacheMgrConstants.CACHE_KEY_ID_GEN);  
        } catch (Exception ex) 
        {
          super.logger.error(ex);
          throw new CacheException("Could not set the cached object.");
        } 
      }
      
      public PaymentMethodMilesPointsVO getPaymentMethodMPById(String paymentMethodId) throws CacheException
      {
    	  String key = paymentMethodId;
    	  PaymentMethodMilesPointsVO value = null;
          
          Object cachedObj = this.objMap;
          if (cachedObj == null) {
                throw new CacheException("Cached object is null.");
          }
          Object obj = this.objMap.get(key);

          if(obj != null) {
                value = (PaymentMethodMilesPointsVO)obj;     
          } 
          return value;
      }

}


