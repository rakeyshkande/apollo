package com.ftd.osp.utilities.cacheMgr.handlers.vo;

import com.ftd.osp.utilities.cacheMgr.handlers.enums.ImageTypeEnum;


/**

 */
public class ImageVO
{
    private String imageURL;
    private ImageTypeEnum imageType;
    private String imageFormat;
    
    public ImageVO () {
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageType(ImageTypeEnum imageType) {
        this.imageType = imageType;
    }

    public ImageTypeEnum getImageType() {
        return imageType;
    }

    public void setImageFormat(String imageFormat) {
        this.imageFormat = imageFormat;
    }

    public String getImageFormat() {
        return imageFormat;
    }
    
    public ImageVO clone() {
        ImageVO image = new ImageVO();
        image.setImageURL(this.getImageURL());
        image.setImageType(this.getImageType());
        image.setImageFormat(this.getImageFormat());
        
        return image;
    }

}
