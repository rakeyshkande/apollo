package com.ftd.osp.utilities.cacheMgr.handlers.vo;

import java.math.BigDecimal;


public class ShippingKeyDetailVO
{
    private String shippingDetailId;
    private String shippingKeyId;
    private BigDecimal minPrice;
    private BigDecimal maxPrice;


    public void setShippingDetailId(String shippingDetailId) {
        this.shippingDetailId = shippingDetailId;
    }

    public String getShippingDetailId() {
        return shippingDetailId;
    }

    public void setShippingKeyId(String shippingKeyId) {
        this.shippingKeyId = shippingKeyId;
    }

    public String getShippingKeyId() {
        return shippingKeyId;
    }

    public void setMinPrice(BigDecimal minPrice) {
        this.minPrice = minPrice;
    }

    public BigDecimal getMinPrice() {
        return minPrice;
    }

    public void setMaxPrice(BigDecimal maxPrice) {
        this.maxPrice = maxPrice;
    }

    public BigDecimal getMaxPrice() {
        return maxPrice;
    }
}
