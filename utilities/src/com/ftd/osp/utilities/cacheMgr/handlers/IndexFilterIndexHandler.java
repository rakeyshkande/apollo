package com.ftd.osp.utilities.cacheMgr.handlers;

import com.ftd.osp.utilities.cacheMgr.constant.CacheMgrConstants;
import com.ftd.osp.utilities.cacheMgr.dao.CacheMgrDAO;
import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.ColorMasterVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.CompanyMasterVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.SourceMasterVO;
import com.ftd.osp.utilities.cacheMgr.vo.CacheHandlerBase;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * This class is responsible for loading source template reference information
 * key: template id,index name
 * value: String productIds
 */
public class IndexFilterIndexHandler extends CacheHandlerBase
{
    private Map objMap;
    public Object load(Connection conn) throws CacheException {
        logger = new Logger(this.getClass().getName());
        CacheMgrDAO osdao = new CacheMgrDAO();
        CachedResultSet rs = osdao.executeQuery(conn, CacheMgrConstants.STMT_GET_FILTER_INDEX);

        Map handlerMap = new HashMap();
        Map filterIndexProductMap = new HashMap();
        String siTemplateId = null;
        String indexName = null;
        String productId = null;
        String key = null;
        String prevKey = null;
        
        String productIds = "";
        
        if(logger.isInfoEnabled())
          logger.info("Loading template filter index...");
        while (rs != null && rs.next()) {   
            siTemplateId = rs.getString("si_template_id");
            indexName = rs.getString("index_name");
            productId = rs.getString("product_id");
            key = siTemplateId + "," + indexName;

            if(!key.equals(prevKey)) {
                if(prevKey != null) {
                    filterIndexProductMap.put(prevKey, productIds);
                }
                productIds = new String();
            }
            productIds = productIds + "," + productId;

            prevKey = key;
        }
        
        if(prevKey != null) {
            filterIndexProductMap.put(prevKey, productIds);
        }
        
        handlerMap.put(CacheMgrConstants.CACHE_KEY_ID_GEN, filterIndexProductMap);
        return handlerMap;
    }

    /**
    *
    * @param cachedObject
    * @throws com.ftd.osp.utilities.cache.exception.CacheException
    */
    public void setCachedObject(Object cachedObject) throws CacheException
    {
        try
        {
            this.objMap = (Map)((Map) cachedObject).get(CacheMgrConstants.CACHE_KEY_ID_GEN);
        } catch (Exception ex)
        {
          super.logger.error(ex);
          throw new CacheException("Could not set the cached object.");
        }
    }

    public String getFilterIndexProducts(String siTemplateId, String indexName) throws CacheException
    {
        String key = siTemplateId + "," + indexName;
        String value = null;
        
        Object cachedObj = this.objMap;
        if (cachedObj == null) {
              throw new CacheException("Cached object is null.");
        }
        Object obj = this.objMap.get(key);

        if(obj != null) {
              value = (String)obj;     
        } 
        return value;
    }


}