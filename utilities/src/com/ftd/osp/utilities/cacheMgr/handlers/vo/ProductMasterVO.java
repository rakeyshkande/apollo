package com.ftd.osp.utilities.cacheMgr.handlers.vo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.ftd.osp.utilities.cacheMgr.handlers.enums.DeliveryMethodEnum;


/**

 */
public class ProductMasterVO implements Comparable<ProductMasterVO>
{
    private String productId;
    private String webProductId;
    private String categoryDesc;
    private String productType;
    private String typeDesc;
    private String productSubType;
    private String subTypeDesc;
    private String longDesc;
    private String productName;
    private String webProductName;
    private String dominantFlowers;
    private String over21Flag;
    private Date exceptionStartDate;
    private Date exceptionEndDate;
    private String personalizationId;
    private String personalizationFlag;
    private String shippingKey;
    private String status;
    private String deliveryType;
    private String discountAllowedFlag;
    private String noTaxFlag;
    private BigDecimal standardPrice; // use for sorting 
    private BigDecimal deluxePrice;
    private BigDecimal premiumPrice;
    private String preferredPricePoint;
    private String gbbPopoverFlag;
    private String gbbTitle;
    private boolean isUpsellMasterId;
    private boolean hasGBB;
    private boolean hasUpsell;
    private DeliveryMethodEnum deliveryMethod;
    private List<ImageVO> imageList; // This is the images for this product/price point, not for associated products or different price point.
                                     // For example, standard, thumbnail, zoom...
    private List<PricingVO> pricingList; // List of price type for this price point: base, sale...
    private String IOTWMessaging;
    private List<GBBVO> gbbList;
    private UpsellMasterVO upsellMasterVO;
    private List<CountryMasterVO> countryList;
    private List<String> keywordList;
    private List<ColorMasterVO> colorList;
    private List<CompanyMasterVO> companyList;
    private int hashcode;
    private int indexOrder;
    private String variablePriceFlag;
   	private String allowFreeShippingFlag;
   	private String shipMethodCarrier;
    private String shipMethodFlorist;
    

    public ProductMasterVO() {}
    
    public ProductMasterVO(String webProductId) {
        this.webProductId = webProductId;
    }
    
    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductId() {
        return productId;
    }

    public void setCategoryDesc(String categoryDesc) {
        this.categoryDesc = categoryDesc;
    }

    public String getCategoryDesc() {
        return categoryDesc;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getProductType() {
        return productType;
    }

    public void setTypeDesc(String typeDesc) {
        this.typeDesc = typeDesc;
    }

    public String getTypeDesc() {
        return typeDesc;
    }

    public void setSubTypeDesc(String subTypeDesc) {
        this.subTypeDesc = subTypeDesc;
    }

    public String getSubTypeDesc() {
        return subTypeDesc;
    }

    public void setLongDesc(String longDesc) {
        this.longDesc = longDesc;
    }

    public String getLongDesc() {
        return longDesc;
    }

    public void setDominantFlowers(String dominantFlowers) {
        this.dominantFlowers = dominantFlowers;
    }

    public String getDominantFlowers() {
        return dominantFlowers;
    }

    public void setOver21Flag(String over21Flag) {
        this.over21Flag = over21Flag;
    }

    public String getOver21Flag() {
        return over21Flag;
    }

    public void setExceptionStartDate(Date exceptionStartDate) {
        this.exceptionStartDate = exceptionStartDate;
    }

    public Date getExceptionStartDate() {
        return exceptionStartDate;
    }

    public void setExceptionEndDate(Date exceptionEndDate) {
        this.exceptionEndDate = exceptionEndDate;
    }

    public Date getExceptionEndDate() {
        return exceptionEndDate;
    }

    public void setShippingKey(String shippingKey) {
        this.shippingKey = shippingKey;
    }

    public String getShippingKey() {
        return shippingKey;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setDeliveryType(String deliveryType) {
        this.deliveryType = deliveryType;
    }

    public String getDeliveryType() {
        return deliveryType;
    }

    public void setDiscountAllowedFlag(String discountAllowedFlag) {
        this.discountAllowedFlag = discountAllowedFlag;
    }

    public String getDiscountAllowedFlag() {
        return discountAllowedFlag;
    }

    public void setNoTaxFlag(String noTaxFlag) {
        this.noTaxFlag = noTaxFlag;
    }

    public String getNoTaxFlag() {
        return noTaxFlag;
    }

    public void setPreferredPricePoint(String preferredPricePoint) {
        this.preferredPricePoint = preferredPricePoint;
    }

    public String getPreferredPricePoint() {
        return preferredPricePoint;
    }

    public void setGbbPopoverFlag(String gbbPopoverFlag) {
        this.gbbPopoverFlag = gbbPopoverFlag;
    }

    public String getGbbPopoverFlag() {
        return gbbPopoverFlag;
    }

    public void setWebProductId(String webProductId) {
        this.webProductId = webProductId;
    }

    public String getWebProductId() {
        return webProductId;
    }

    public void setIsUpsellMasterId(boolean isUpsellMasterId) {
        this.isUpsellMasterId = isUpsellMasterId;
    }

    public boolean isIsUpsellMasterId() {
        return isUpsellMasterId;
    }

    public void setDeliveryMethod(DeliveryMethodEnum deliveryMethod) {
        this.deliveryMethod = deliveryMethod;
    }

    public DeliveryMethodEnum getDeliveryMethod() {
        return deliveryMethod;
    }

    public void setImageList(List<ImageVO> imageList) {
        this.imageList = imageList;
    }

    public List<ImageVO> getImageList() {
        return imageList;
    }

    public void setPricingList(List<PricingVO> pricingList) {
        this.pricingList = pricingList;
    }

    public List<PricingVO> getPricingList() {
        return pricingList;
    }

    public void setWebProductName(String webProductName) {
        this.webProductName = webProductName;
    }

    public String getWebProductName() {
        return webProductName;
    }
    
    public void setIOTWMessaging(String iOTWMessaging) {
        this.IOTWMessaging = iOTWMessaging;
    }

    public String getIOTWMessaging() {
        return IOTWMessaging;
    }

    public void setGbbList(List<GBBVO> gbbList) {
        this.gbbList = gbbList;
    }

    public List<GBBVO> getGbbList() {
        return gbbList;
    }

    public void setCountryList(List<CountryMasterVO> countryList) {
        this.countryList = countryList;
    }

    public List<CountryMasterVO> getCountryList() {
        return countryList;
    }

    public void setKeywordList(List<String> keywordList) {
        this.keywordList = keywordList;
    }

    public List<String> getKeywordList() {
        return keywordList;
    }

    public void setColorList(List<ColorMasterVO> colorList) {
        this.colorList = colorList;
    }

    public List<ColorMasterVO> getColorList() {
        return colorList;
    }

    public void setCompanyList(List<CompanyMasterVO> companyList) {
        this.companyList = companyList;
    }

    public List<CompanyMasterVO> getCompanyList() {
        return companyList;
    }

    public void setUpsellMasterVO(UpsellMasterVO upsellMasterVO) {
        this.upsellMasterVO = upsellMasterVO;
    }

    public UpsellMasterVO getUpsellMasterVO() {
        return upsellMasterVO;
    }

    public void setHasGBB(boolean hasGBB) {
        this.hasGBB = hasGBB;
    }

    public boolean isHasGBB() {
        return hasGBB;
    }

    public void setHasUpsell(boolean hasUpsell) {
        this.hasUpsell = hasUpsell;
    }

    public boolean isHasUpsell() {
        return hasUpsell;
    }

    public void setPersonalizationId(String personalizationId) {
        this.personalizationId = personalizationId;
    }

    public String getPersonalizationId() {
        return personalizationId;
    }

    public void setGbbTitle(String gbbTitle) {
        this.gbbTitle = gbbTitle;
    }

    public String getGbbTitle() {
        return gbbTitle;
    }

    public void setPersonalizationFlag(String personalizationFlag) {
        this.personalizationFlag = personalizationFlag;
    }

    public String getPersonalizationFlag() {
        return personalizationFlag;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductName() {
        return productName;
    }
    


    public void setStandardPrice(BigDecimal standardPrice) {
        this.standardPrice = standardPrice;
    }

    public BigDecimal getStandardPrice() {
        return standardPrice;
    }

    public void setProductSubType(String productSubType) {
        this.productSubType = productSubType;
    }

    public String getProductSubType() {
        return productSubType;
    }
    
    //overwirting equals() of Ojbect
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        
        ProductMasterVO obj = (ProductMasterVO)o;

        if(this.getProductId() != null && !this.getProductId().equals(obj.getProductId()))
            return false;
        if(this.getWebProductId() != null && !this.getWebProductId().equals(obj.getWebProductId()))
            return false;
        return true;
    }

    /**
     *  Implementing comparTo so product can be compared by standard price when sorting.
     */
    public int compareTo(ProductMasterVO p) {
        int priceCmp = this.getStandardPrice().compareTo(p.getStandardPrice());
        return priceCmp;
    }

    /**
     * Implementing clone so each request will not modify the cache copy of product.
     * @return
     */
    public ProductMasterVO clone() {
        ProductMasterVO pmvo = new ProductMasterVO();
        
        pmvo.setProductId(this.getProductId());
        pmvo.setWebProductId(this.getWebProductId());
        pmvo.setCategoryDesc(this.getCategoryDesc());
        pmvo.setProductType(this.getProductType());
        pmvo.setTypeDesc(this.getTypeDesc());
        pmvo.setSubTypeDesc(this.getSubTypeDesc());
        pmvo.setLongDesc(this.getLongDesc());
        pmvo.setProductName(this.getProductName());
        pmvo.setWebProductName(this.getWebProductName());
        pmvo.setDominantFlowers(this.getDominantFlowers());
        pmvo.setOver21Flag(this.getOver21Flag());
        pmvo.setExceptionStartDate(this.getExceptionStartDate());
        pmvo.setExceptionEndDate(this.getExceptionEndDate());
        pmvo.setPersonalizationId(this.getPersonalizationId());
        pmvo.setPersonalizationFlag(this.getPersonalizationFlag());
        pmvo.setShippingKey(this.getShippingKey());
        pmvo.setStatus(this.getStatus());
        pmvo.setDeliveryType(this.getDeliveryType());
        pmvo.setDiscountAllowedFlag(this.getDiscountAllowedFlag());
        pmvo.setNoTaxFlag(this.getNoTaxFlag());
        pmvo.setPreferredPricePoint(this.getPreferredPricePoint());
        pmvo.setDeliveryMethod(this.deliveryMethod);
        pmvo.setGbbTitle(this.getGbbTitle());
        pmvo.setStandardPrice(this.getStandardPrice());
        pmvo.setIndexOrder(this.getIndexOrder());
        
        List imageList = new ArrayList<ImageVO>();
        List origImageList = this.getImageList();
        if(origImageList != null) {
            for (int i = 0; i < origImageList.size(); i++){
                ImageVO image = ((ImageVO)origImageList.get(i)).clone();
                imageList.add(image);
            }
        }
        pmvo.setImageList(imageList);
        
        List gbbList = null; 
        List origGbbList = this.getGbbList();
        if(origGbbList != null) {
            gbbList = new ArrayList<GBBVO>();
            for (int i = 0; i < origGbbList.size(); i++){
                GBBVO gbb = ((GBBVO)origGbbList.get(i)).clone();
                gbbList.add(gbb);
            }
        }
        pmvo.setGbbList(gbbList);
        
        return pmvo;
    }

    public void setDeluxePrice(BigDecimal deluxePrice) {
        this.deluxePrice = deluxePrice;
    }

    public BigDecimal getDeluxePrice() {
        return deluxePrice;
    }

    public void setPremiumPrice(BigDecimal premiumPrice) {
        this.premiumPrice = premiumPrice;
    }

    public BigDecimal getPremiumPrice() {
        return premiumPrice;
    }

    public void setIndexOrder(int indexOrder) {
        this.indexOrder = indexOrder;
    }

    public int getIndexOrder() {
        return indexOrder;
    }
    
    public String getVariablePriceFlag() {
		return variablePriceFlag;
	}

	public void setVariablePriceFlag(String variablePriceFlag) {
		this.variablePriceFlag = variablePriceFlag;
	}

	public String getAllowFreeShippingFlag() {
		return allowFreeShippingFlag;
	}

	public void setAllowFreeShippingFlag(String allowFreeShippingFlag) {
		this.allowFreeShippingFlag = allowFreeShippingFlag;
	}

	public String getShipMethodCarrier() {
		return shipMethodCarrier;
	}

	public void setShipMethodCarrier(String shipMethodCarrier) {
		this.shipMethodCarrier = shipMethodCarrier;
	}

	public String getShipMethodFlorist() {
		return shipMethodFlorist;
	}

	public void setShipMethodFlorist(String shipMethodFlorist) {
		this.shipMethodFlorist = shipMethodFlorist;
	}
}
