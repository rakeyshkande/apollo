package com.ftd.osp.utilities.cacheMgr.handlers;

import com.ftd.osp.utilities.cacheMgr.constant.CacheMgrConstants;
import com.ftd.osp.utilities.cacheMgr.dao.CacheMgrDAO;
import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.cacheMgr.vo.CacheHandlerBase;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * This class is responsible for loading source template reference information
 * key: source code
 * value: List<String> templateIds
 */
public class IndexSourceTemplateHandler extends CacheHandlerBase
{
    private Map sourceTemplateMap;
    private Map domainTemplateMap;
    private static String CACHE_KEY_SOURCE_TEMPLATE = "CACHE_KEY_SOURCE_TEMPLATE";
    private static String CACHE_KEY_DOMAIN_TEMPLATE = "CACHE_KEY_DOMAIN_TEMPLATE";
    
    public Object load(Connection conn) throws CacheException {
        logger = new Logger(this.getClass().getName());
        CacheMgrDAO osdao = new CacheMgrDAO();
        // Get source map. Source map is consisted of key=source code, and value= a list of tempalte ids.
        CachedResultSet rs = osdao.executeQuery(conn, CacheMgrConstants.STMT_GET_SOURCE_TEMPLATE_REF);

        Map handlerMap = new HashMap();
        Map sourceTemplateMap = new HashMap();
        Map domainTemplateMap = new HashMap();
        String sourceCode = null;
        String prevSourceCode = null;
        String siTemplateId = null;
 
        List<String> templateList = null;
        
        if(logger.isInfoEnabled())
          logger.info("Loading source template ref...");
        while (rs != null && rs.next()) {   
            sourceCode = rs.getString("source_code");
            siTemplateId = rs.getString("si_template_id");

            if(!sourceCode.equals(prevSourceCode)) {
                if(prevSourceCode != null) {
                    sourceTemplateMap.put(prevSourceCode, templateList);
                }
                templateList = new ArrayList();
            }
            templateList.add(siTemplateId);

            prevSourceCode = sourceCode;
        }
        
        if(prevSourceCode != null) {
            sourceTemplateMap.put(prevSourceCode, templateList);
        }
        
        // Get domain map. Domain map is consisted of key=domain, and value = template id.
        rs = osdao.executeQuery(conn, CacheMgrConstants.STMT_GET_DOMAIN_TEMPLATE);
        if(logger.isInfoEnabled())
          logger.info("Loading domain templates...");
        String domain = null;
        while (rs != null && rs.next()) {   
            domain = rs.getString("si_template_key");
            siTemplateId = rs.getString("si_template_id");

            domainTemplateMap.put(domain, siTemplateId);
        }
        
        handlerMap.put(CACHE_KEY_SOURCE_TEMPLATE, sourceTemplateMap);
        handlerMap.put(CACHE_KEY_DOMAIN_TEMPLATE, domainTemplateMap);
        return handlerMap;
    }

    /**
     *
     * @param cachedObject
     * @throws com.ftd.osp.utilities.cache.exception.CacheException
     */
      public void setCachedObject(Object cachedObject) throws CacheException
      {
        try
        {
            this.sourceTemplateMap = (Map)((Map) cachedObject).get(this.CACHE_KEY_SOURCE_TEMPLATE); 
            this.domainTemplateMap = (Map)((Map) cachedObject).get(this.CACHE_KEY_DOMAIN_TEMPLATE); 
        } catch (Exception ex)
        {
          super.logger.error(ex);
          throw new CacheException("Could not set the cached object.");
        }
      }

    public List<String> getSourceTemplates(String sourceCode) throws CacheException
    {
        String key = sourceCode;
        List<String> value = null;
        
        Object cachedObj = this.sourceTemplateMap;
        if (cachedObj == null) {
              throw new CacheException("Cached object is null.");
        }
        Object obj = this.sourceTemplateMap.get(key);

        if(obj != null) {
              value = Collections.unmodifiableList((List<String>)obj);     
        } 
        return value;
    }

    public String getTemplateIdByDomain(String domain) throws CacheException
    {
        String key = domain;
        String value = null;
        
        Object cachedObj = this.domainTemplateMap;
        if (cachedObj == null) {
              throw new CacheException("Cached object is null.");
        }
        value = (String)this.domainTemplateMap.get(key);

        return value;
    }
}
