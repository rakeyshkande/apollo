package com.ftd.osp.utilities.cacheMgr.handlers;

import com.ftd.osp.utilities.cacheMgr.constant.CacheMgrConstants;
import com.ftd.osp.utilities.cacheMgr.dao.CacheMgrDAO;
import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.ShippingKeyCostVO;
import com.ftd.osp.utilities.cacheMgr.vo.CacheHandlerBase;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;

import com.ftd.osp.utilities.plugins.Logger;

import java.math.BigDecimal;

import java.sql.Connection;

import java.util.HashMap;
import java.util.Map;

/**
 * This class is responsible for loading shipping_key_cost data
 * key: shipping_key_detail_id, shipping_method_id
 * value: shipping_cost
 */
public class ShippingKeyCostHandler extends CacheHandlerBase
{
    private Map objMap;
    public Object load(Connection conn) throws CacheException {
        logger = new Logger(this.getClass().getName());
        CacheMgrDAO osdao = new CacheMgrDAO();
        CachedResultSet rs = osdao.executeQuery(conn, CacheMgrConstants.STMT_GET_SHIPPING_KEY_COST);

        Map handlerMap = new HashMap();
        Map shippingKeyCostMap = new HashMap();
        
        if(rs != null) {
            while (rs != null && rs.next()) {
                ShippingKeyCostVO scvo = new ShippingKeyCostVO();
                scvo.setShippingDetailId(rs.getString("shipping_key_detail_id"));
                scvo.setShippingMethodId(rs.getString("shipping_method_id"));
                scvo.setShippingCost(rs.getBigDecimal("shipping_cost"));
                shippingKeyCostMap.put(scvo, scvo);
            }
        }
        
        handlerMap.put(CacheMgrConstants.CACHE_KEY_ID_GEN, shippingKeyCostMap);
        return handlerMap;
    }
    
    /**
     * Set the cached object in the cache handler. The cache handler is then 
     * responsible for fulfilling all application level API calls, to access the
     * data in the cached object.
     * 
     * @param cachedObject
     * @throws com.ftd.osp.utilities.cache.exception.CacheException
     */    
      public void setCachedObject(Object cachedObject) throws CacheException
      {
        try 
        {
          this.objMap = (Map)((Map) cachedObject).get(CacheMgrConstants.CACHE_KEY_ID_GEN);  
        } catch (Exception ex) 
        {
          super.logger.error(ex);
          throw new CacheException("Could not set the cached object.");
        } 
      }
      
      public BigDecimal getShippingCost(String shippingKeyDetailId, String shipMethod) throws CacheException
      {
          ShippingKeyCostVO key = new ShippingKeyCostVO(shippingKeyDetailId, shipMethod);
          
          Object cachedObj = this.objMap;
          if (cachedObj == null) {
                throw new CacheException("Cached object is null.");
          }
          Object obj = this.objMap.get(key);
          BigDecimal v = null;
          if(obj != null) {
                ShippingKeyCostVO value = (ShippingKeyCostVO)obj;
                v = value.getShippingCost();
          } 
          return v;
      }
      
      public ShippingKeyCostVO getShippingKeyCosts(String shippingKeyDetailId, String shipMethod) throws CacheException
      {
          ShippingKeyCostVO key = new ShippingKeyCostVO(shippingKeyDetailId, shipMethod);
          
          Object cachedObj = this.objMap;
          if (cachedObj == null) {
                throw new CacheException("Cached object is null.");
          }
          Object obj = this.objMap.get(key);
          ShippingKeyCostVO v = null;
          if(obj != null) {
                v = (ShippingKeyCostVO)obj;
          } 
          return v;
      }
      
      
}
