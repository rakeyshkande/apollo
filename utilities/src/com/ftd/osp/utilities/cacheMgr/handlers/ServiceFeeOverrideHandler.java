package com.ftd.osp.utilities.cacheMgr.handlers;

import com.ftd.osp.utilities.cacheMgr.constant.CacheMgrConstants;
import com.ftd.osp.utilities.cacheMgr.dao.CacheMgrDAO;
import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.ServiceFeeOverrideVO;
import com.ftd.osp.utilities.cacheMgr.vo.CacheHandlerBase;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;

import java.text.DateFormat;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


/**
 * This class is responsible for loading service fee into cache.
 * key: snhId
 * value: ServiceFeeVO
 */
public class ServiceFeeOverrideHandler extends CacheHandlerBase
{
    private Map objMap;
    public Object load(Connection conn) throws CacheException {
        logger = new Logger(this.getClass().getName());
        CacheMgrDAO osdao = new CacheMgrDAO();
        CachedResultSet rs = osdao.executeQuery(conn, CacheMgrConstants.STMT_GET_SERVICE_FEE_OVERRIDE);

        Map handlerMap = new HashMap();
        Map serviceFeeMap = new HashMap();

        if(logger.isInfoEnabled())
          logger.info("Loading service fee overrides...");
        while (rs != null && rs.next()) {

            ServiceFeeOverrideVO sfvo = new ServiceFeeOverrideVO();
            sfvo.setSnhId(rs.getString("snh_id"));
            sfvo.setOverrideDate(rs.getDate("override_date"));
            sfvo.setDomesticCharge(rs.getBigDecimal("domestic_charge"));
            sfvo.setInternationalCharge(rs.getBigDecimal("international_charge"));
            sfvo.setVendorCharge(rs.getBigDecimal("vendor_charge"));
            sfvo.setVendorSatUpcharge(rs.getBigDecimal("vendor_sat_upcharge"));
            sfvo.setSameDayUpcharge(rs.getBigDecimal("same_day_upcharge"));

            DateFormat df = DateFormat.getDateInstance();
            serviceFeeMap.put(sfvo.getSnhId() + df.format(sfvo.getOverrideDate()), sfvo);
        }

        handlerMap.put(CacheMgrConstants.CACHE_KEY_ID_GEN, serviceFeeMap);
        return handlerMap;
    }

    /**
     * Set the cached object in the cache handler. The cache handler is then
     * responsible for fulfilling all application level API calls, to access the
     * data in the cached object.
     *
     * @param cachedObject
     * @throws com.ftd.osp.utilities.cache.exception.CacheException
     */
      public void setCachedObject(Object cachedObject) throws CacheException
      {
        try
        {
            this.objMap = (Map)(((Map) cachedObject).get(CacheMgrConstants.CACHE_KEY_ID_GEN));
        } catch (Exception ex)
        {
          super.logger.error(ex);
          throw new CacheException("Could not set the cached object.");
        }
      }

      /**
     * Returns value object by snhId and overrideDate.
     * @param snhId
     * @param overrideDate
     * @return
     * @throws CacheException
     */
      public ServiceFeeOverrideVO getServiceFeeOverrideById(String snhId, Date overrideDate) throws CacheException
      {
    	  ServiceFeeOverrideVO value = null;
          DateFormat df = DateFormat.getDateInstance();
          String key = snhId + df.format(overrideDate);

          Object cachedObj = this.objMap;
          if (cachedObj == null) {
                throw new CacheException("Cached object is null.");
          }

          Object obj = this.objMap.get(key);

          if(obj != null) {
                value = (ServiceFeeOverrideVO)obj;
          } else if(logger.isDebugEnabled()){
                logger.debug("Cached object not found:" + key);
          }
          return value;
      }


}
