package com.ftd.osp.utilities.cacheMgr.handlers;

import com.ftd.osp.utilities.cacheMgr.constant.CacheMgrConstants;
import com.ftd.osp.utilities.cacheMgr.dao.CacheMgrDAO;
import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.cacheMgr.handlers.enums.DeliveryMethodEnum;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.ProductMasterVO;
import com.ftd.osp.utilities.cacheMgr.vo.CacheHandlerBase;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * This class is responsible for loading product company into cache.
 * key: webProductId
 * value: ProductMasterVO
 *
 * Key: productId
 * value: webProductId
 */
public class ProductMasterHandler extends CacheHandlerBase
{
    private Map objMap;
    private Map idMap;
    private Map webIdMap;
    private String DATE_FORMAT_DD = "MM/dd/yyyy";
    private String CACHE_NAME_PRODUCT_ID = "CACHE_NAME_PRODUCT_ID";
    private String CACHE_NAME_WEB_PRODUCT_ID = "CACHE_NAME_WEB_PRODUCT_ID";
    public Object load(Connection conn) throws CacheException {
        logger = new Logger(this.getClass().getName());
        CacheMgrDAO osdao = new CacheMgrDAO();
        CachedResultSet rs = osdao.executeQuery(conn, CacheMgrConstants.STMT_GET_PRODUCT_MASTER);

        Map handlerMap = new HashMap();
        Map productMasterMap = new HashMap();
        Map productIdMap = new HashMap();
        Map webProductIdMap = new HashMap();
        
        if(logger.isInfoEnabled())
          logger.info("Loaded product master...");
        while (rs != null && rs.next()) {   
            ProductMasterVO pmvo = new ProductMasterVO();
            pmvo.setProductId(rs.getString("product_id"));
            pmvo.setWebProductId(rs.getString("novator_id"));
            pmvo.setCategoryDesc(rs.getString("category_desc"));
            pmvo.setProductType(rs.getString("product_type"));
            pmvo.setTypeDesc(rs.getString("type_desc"));
            pmvo.setProductSubType(rs.getString("product_sub_type"));
            pmvo.setSubTypeDesc(rs.getString("sub_type_desc"));
            pmvo.setLongDesc(rs.getString("long_description"));
            pmvo.setProductName(rs.getString("product_name"));
            pmvo.setWebProductName(rs.getString("novator_name"));
            pmvo.setDominantFlowers(rs.getString("dominant_flowers"));
            pmvo.setOver21Flag(rs.getString("over_21"));
            pmvo.setExceptionStartDate(rs.getDate("exception_start_date"));
            pmvo.setExceptionEndDate(rs.getDate("exception_end_date"));
            pmvo.setPersonalizationId(rs.getString("personalization_template_id"));
            if(pmvo.getPersonalizationId() != null && pmvo.getPersonalizationId().length() > 0) {
                pmvo.setPersonalizationFlag("Y");
            } else {
                pmvo.setPersonalizationFlag("N");
            }
            pmvo.setShippingKey(rs.getString("shipping_key"));
            pmvo.setStatus(rs.getString("status"));
            pmvo.setDeliveryType(rs.getString("delivery_type"));
            pmvo.setDiscountAllowedFlag(rs.getString("discount_allowed_flag"));
            pmvo.setNoTaxFlag(rs.getString("no_tax_flag"));
            pmvo.setPreferredPricePoint(rs.getString("preferred_price_point"));
            
            String shipMethodFlorist = rs.getString("ship_method_florist");
            pmvo.setShipMethodFlorist(shipMethodFlorist);
                
            String shipMethodCarrier = rs.getString("ship_method_carrier");
            pmvo.setShipMethodCarrier(shipMethodCarrier);
            DeliveryMethodEnum deliveryMethod = DeliveryMethodEnum.toDeliveryMethod(shipMethodFlorist, shipMethodCarrier);
            pmvo.setDeliveryMethod(deliveryMethod);
                            
            pmvo.setStandardPrice(rs.getBigDecimal("standard_price"));
            pmvo.setDeluxePrice(rs.getBigDecimal("deluxe_price"));
            pmvo.setPremiumPrice(rs.getBigDecimal("premium_price"));
          
            // Store cache for product id lookup.
            if(pmvo.getProductId() != null && pmvo.getWebProductId() != null) {
                productIdMap.put(pmvo.getProductId(), pmvo.getWebProductId());
                webProductIdMap.put(pmvo.getWebProductId(), pmvo.getProductId());
            }
            
            pmvo.setVariablePriceFlag(rs.getString("variable_price_flag"));
            pmvo.setAllowFreeShippingFlag(rs.getString("allow_free_shipping_flag"));
            productMasterMap.put(pmvo.getWebProductId(), pmvo);
        }
        

        handlerMap.put(CacheMgrConstants.CACHE_KEY_ID_GEN, productMasterMap);
        handlerMap.put(CACHE_NAME_PRODUCT_ID, productIdMap);
        handlerMap.put(CACHE_NAME_WEB_PRODUCT_ID, webProductIdMap);
        return handlerMap;
    }
    
    /**
     * Set the cached object in the cache handler. The cache handler is then 
     * responsible for fulfilling all application level API calls, to access the
     * data in the cached object.
     * 
     * @param cachedObject
     * @throws com.ftd.osp.utilities.cache.exception.CacheException
     */    
      public void setCachedObject(Object cachedObject) throws CacheException
      {
        try 
        {
            this.objMap = (Map)((Map) cachedObject).get(CacheMgrConstants.CACHE_KEY_ID_GEN);  
            this.idMap = (Map)((Map)cachedObject).get(CACHE_NAME_PRODUCT_ID);
            this.webIdMap = (Map)((Map)cachedObject).get(CACHE_NAME_WEB_PRODUCT_ID);
        } catch (Exception ex) 
        {
          super.logger.error(ex);
          throw new CacheException("Could not set the cached object.");
        } 
      }
      
      public String getWebProductIdByProductId(String productId) throws CacheException
      {
          String key = productId;
          String value = null;
          
          Object cachedObj = this.idMap;
          if (cachedObj == null) {
                throw new CacheException("Cached object is null.");
          }
          Object obj = this.idMap.get(key);

          if(obj != null) {
                value = (String)obj;     
          } 
          return value;
      }
      
    public String getProductIdByWebProductId(String webProductId) throws CacheException
    {
        String key = webProductId;
        String value = null;
        
        Object cachedObj = this.webIdMap;
        if (cachedObj == null) {
              throw new CacheException("Cached object is null.");
        }
        Object obj = this.webIdMap.get(key);

        if(obj != null) {
              value = (String)obj;     
        } 
        return value;
    }
      
    public ProductMasterVO getProductByWebProductId(String webProductId) throws CacheException
    {
        String key = webProductId;
        ProductMasterVO value = null;
        Object cachedObj = this.objMap;
        if (cachedObj == null) {
              throw new CacheException("Cached object is null.");
        }
        Object obj = this.objMap.get(key);

        if(obj != null) {
              value = (ProductMasterVO)obj;     
        } 
        return value;
    }
      
    public ProductMasterVO getProductById(String productId) throws CacheException
    {
        String webProductId = getWebProductIdByProductId(productId);
        return getProductByWebProductId(webProductId);
    }
    
    /**
     * Returns a date relative to today.
     * @param offset
     * @return
     */
    public Date getDateTruncated(int offset) throws Exception {
      Calendar cal = Calendar.getInstance();
      cal.add(Calendar.DATE, offset);
      
      SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_DD);
      String dateString = sdf.format(cal.getTime());
      Date truncatedDate = sdf.parse(dateString);
      return truncatedDate;
    }  
    
    public List<String> getAllProductIds() throws Exception {
        List<String> productIdList = new ArrayList<String>();
        Map webIdMap = this.idMap;
        Set s = webIdMap.keySet();
        Iterator i = s.iterator();
        
        while (i.hasNext()) {
            String productId = (String)i.next();
            productIdList.add(productId);
        }
        return productIdList;
    }
}
