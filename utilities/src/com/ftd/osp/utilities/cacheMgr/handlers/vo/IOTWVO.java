package com.ftd.osp.utilities.cacheMgr.handlers.vo;

import java.math.BigDecimal;

import java.util.Date;
import java.util.List;

public class IOTWVO
{
    private String sourceCode;
    private String IOTWSourceCode;
    private String productId;
    private String upsellMasterId;
    private Date startDate;
    private Date endDate;
    private String productPageMessaging;
    int hashcode;

    public void setSourceCode(String sourceCode) {
        this.sourceCode = sourceCode;
    }

    public String getSourceCode() {
        return sourceCode;
    }


    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductId() {
        return productId;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setUpsellMasterId(String upsellMasterId) {
        this.upsellMasterId = upsellMasterId;
    }

    public String getUpsellMasterId() {
        return upsellMasterId;
    }

    public void setProductPageMessaging(String productPageMessaging) {
        this.productPageMessaging = productPageMessaging;
    }

    public String getProductPageMessaging() {
        return productPageMessaging;
    }
    
    //overwriting hashCode() of Object
    public int hashCode() {
        if(this.hashcode != 0) {
            return this.hashcode;
        } else {
            hashcode = sourceCode.hashCode();
        }
        return hashcode;
    }
    //overwirting equals() of Ojbect
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        
        IOTWVO obj = (IOTWVO)o;

        if(obj.getSourceCode() == null || !obj.getSourceCode().equals(this.getSourceCode()))
            return false;
        if(obj.getProductId() != null && !obj.getProductId().equals(this.getProductId()))
            return false;
        if(this.getProductId() != null && !this.getProductId().equals(obj.getProductId()))
            return false;            
        if(obj.getUpsellMasterId() != null && !obj.getUpsellMasterId().equals(this.getUpsellMasterId()))
            return false;  
        if(this.getUpsellMasterId() != null && !this.getUpsellMasterId().equals(obj.getUpsellMasterId()))
            return false;  
        return true;
    }

    public void setIOTWSourceCode(String iOTWSourceCode) {
        this.IOTWSourceCode = iOTWSourceCode;
    }

    public String getIOTWSourceCode() {
        return IOTWSourceCode;
    }
}
