package com.ftd.osp.utilities.cacheMgr.handlers;

import com.ftd.osp.utilities.cacheMgr.constant.CacheMgrConstants;
import com.ftd.osp.utilities.cacheMgr.dao.CacheMgrDAO;
import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.AccountProgramMasterVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.ContentVO;
import com.ftd.osp.utilities.cacheMgr.vo.CacheHandlerBase;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;

import java.util.HashMap;
import java.util.Map;


/**
 * This class is responsible for loading content with filters.
 * key: ContentVO
 * value: ContentVO
 */
public class ContentHandler extends CacheHandlerBase
{
    private Map objMap;

    public Object load(Connection conn) throws CacheException {
        logger = new Logger(this.getClass().getName());
        CacheMgrDAO osdao = new CacheMgrDAO();
        CachedResultSet rs = osdao.executeQuery(conn, CacheMgrConstants.STMT_GET_CONTENT_WITH_FILTER);

        Map handlerMap = new HashMap();
        Map contentMap = new HashMap();
        String contentContext = null;
        String contentName = null;
        String filter1Value = null;
        String filter2Value = null;
        String contentTxt = null;
        String cacheKey = null;

        if(logger.isInfoEnabled())
          logger.info("Loaded content...");
        
        while (rs != null && rs.next()) {
            contentContext = rs.getString("content_context");
            contentName = rs.getString("content_name");
            filter1Value = rs.getString("filter_1_value");
            filter2Value = rs.getString("filter_2_value");
            contentTxt = rs.getString("content_txt");
            ContentVO contentVO = new ContentVO(contentContext, contentName, filter1Value, filter2Value, contentTxt);

            if(contentMap.containsKey(contentVO.getFilter2Key())) {
              // If we see this in the logs, for some reason, the filter2 key has resulted in duplicates, which should not occur.
              // But if this is seen, that is a cause for investigation of the data in the content table.
              
              // The Dev DB does have duplicates in it. Following the logic of the FTD_APPS.GET_CONTENT_TEXT_WITH_FILTER method, keep the
              // first value returned by the query. (Maintain DB Ordering)
              if(logger.isWarnEnabled())
                logger.warn("Duplicate Content for Filter Key, will use original value for Key: " + contentVO.getFilter2Key());
              ContentVO originalVO = (ContentVO) contentMap.get(contentVO.getFilter2Key());
              if(logger.isWarnEnabled())
              {
                logger.warn("Original VO Value: " + originalVO.getContentTxt());
                logger.warn("Duplicate VO Value: " + contentVO.getContentTxt());
              }
            } else {
              // Store it with the fully qualified key for all values + filters
              // Note: For default items, the DefaultKey, Filter1Key and Filter2 keys will be the same.
              contentMap.put(contentVO.getFilter2Key(), contentVO); 
            }
        }
        
        handlerMap.put(CacheMgrConstants.CACHE_KEY_ID_GEN, contentMap);
        return handlerMap;
    }
    
    /**
     * Set the cached object in the cache handler. The cache handler is then 
     * responsible for fulfilling all application level API calls, to access the
     * data in the cached object.
     * 
     * @param cachedObject
     * @throws com.ftd.osp.utilities.cache.exception.CacheException
     */    
      public void setCachedObject(Object cachedObject) throws CacheException
      {
        try 
        {
            this.objMap = (Map)((Map) cachedObject).get(CacheMgrConstants.CACHE_KEY_ID_GEN);

        } catch (Exception ex) 
        {
          super.logger.error(ex);
          throw new CacheException("Could not set the cached object.");
        } 
      }
                 
     /**
     * Retrieves content using the context, name and filters. Retrieves from the most specific (filter2) value provided, and if
     * item is not found, attempts filter1 or no filter for default values.
     * 
     * @param contentContext
     * @param contentName
     * @param filter1
     * @param filter2
     * @param missingContentOk If <code>false</code>Logs a warning message if the content is not defined.
     * @return
     * @throws CacheException
     */
      public String getContentWithFilter(String contentContext, String contentName, String filter1, String filter2, boolean missingContentOk) throws CacheException
      {
          ContentVO key = new ContentVO(contentContext, contentName, filter1, filter2, null);
          ContentVO value = null;
          
          Object cachedObj = this.objMap;
          if (cachedObj == null) {
                throw new CacheException("Cached object is null.");
          }        
          
          String contentTxt = null;

          // Check fully qualified Key
          Object obj = this.objMap.get(key.getFilter2Key());
          
          if(obj == null) {
            // Check up to Filter 1 Level
            obj = this.objMap.get(key.getFilter1Key());
            
            if(logger.isDebugEnabled()) {
              logger.debug("Searching for Filter1 Content for Filter Key: " + key.getFilter2Key());
            }
          }
          
          if(obj == null) {
            // Check for unfiltered Default Level
            obj = this.objMap.get(key.getDefaultKey());
            
            if(logger.isDebugEnabled()) {
              logger.debug("Searching for Default Content for Filter Key: " + key.getFilter2Key());
            }            
          }

          if(obj != null) {
            value = (ContentVO)obj;
            contentTxt = value.getContentTxt();
          } else {
            if(!missingContentOk && logger.isWarnEnabled())
              logger.warn("getContentWithFilter():: Unable to locate Filtered or Default Content for Key: " + key.getFilter2Key());
          }
          
          return contentTxt;
      }
      
      
     /**
     * Delegates to {@link #getContentWithFilter(String, String, String, String, boolean)}
     * With missingContentOk set to <code>false</code>
     * @param contentContext
     * @param contentName
     * @param filter1
     * @param filter2
     * @return
     * @throws CacheException
     */
      public String getContentWithFilter(String contentContext, String contentName, String filter1, String filter2) throws CacheException
      {
        return getContentWithFilter(contentContext, contentName, filter1, filter2, false);
      }
      
      

}
