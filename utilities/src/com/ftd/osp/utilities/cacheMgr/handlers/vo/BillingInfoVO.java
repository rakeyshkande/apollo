package com.ftd.osp.utilities.cacheMgr.handlers.vo;

public class BillingInfoVO 
{
    private String infoSequence;
    private String infoPrompt;
    private String infoFormat;
    private String promptType;
    private String sourceCode;

    public BillingInfoVO()
    {
    }

    public String getInfoSequence()
    {
        return infoSequence;
    }

    public void setInfoSequence(String newInfoSequence)
    {
        infoSequence = newInfoSequence;
    }

    public String getInfoPrompt()
    {
        return infoPrompt;
    }

    public void setInfoPrompt(String newInfoPrompt)
    {
        infoPrompt = newInfoPrompt;
    }

    public String getInfoFormat()
    {
        return infoFormat;
    }

    public void setInfoFormat(String newInfoFormat)
    {
        infoFormat = newInfoFormat;
    }

    public String getPromptType()
    {
        return promptType;
    }

    public void setPromptType(String newPromptType)
    {
        promptType = newPromptType;
    }

    public String getSourceCode()
    {
        return sourceCode;
    }

    public void setSourceCode(String newSourceCode)
    {
        sourceCode = newSourceCode;
    }
}