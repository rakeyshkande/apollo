package com.ftd.osp.utilities.cacheMgr.handlers;

import com.ftd.osp.utilities.cacheMgr.handlers.vo.FTDAppsGlobalParmsVO;
import com.ftd.osp.utilities.cacheMgr.constant.CacheMgrConstants;
import com.ftd.osp.utilities.cacheMgr.dao.CacheMgrDAO;
import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.GlobalParmVO;
import com.ftd.osp.utilities.cacheMgr.vo.CacheHandlerBase;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;

import com.ftd.osp.utilities.plugins.Logger;

import java.math.BigDecimal;
import java.sql.Connection;
import java.text.SimpleDateFormat;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * This class is responsible for loading country into cache. Call frp.VIEW_GLOBAL_PARMS
 * key: context,name
 * value: String
 */
public class GlobalParmHandler extends CacheHandlerBase
{

    private FTDAppsGlobalParmsVO ftdAppsGlobalParmsVO ;
    
    /**
     * This map contains the same key value format as the new cache
     */
    private Map objMap;
    
    /**
     * This map contains the same key value format as the original cache
     * Maintained for backwards compatibility
     * <context>+<name>=<value>
     */    
    private Map frpGlobalParmMap;
    
    private static final String FTD_APPS_GLOBAL_PARMS_VO_KEY = "FTD_APPS_GLOBAL_PARMS_VO_KEY";
    private static final String FRP_GLOBAL_PARMS_MAP_KEY = "FRP_GLOBAL_PARMS_MAP_KEY";    
   
    public Object load(Connection conn) throws CacheException {
        logger = new Logger(this.getClass().getName());
        CacheMgrDAO osdao = new CacheMgrDAO();
        CachedResultSet rs = osdao.executeQuery(conn, CacheMgrConstants.STMT_GET_GLOBAL_PARM);

        String context = "";
        String name = "";
        String value = "";
        Map handlerMap = new HashMap();
        Map globalParmMap = new HashMap();
        Map frpGlobalParmMapCacheObject = new HashMap();        
        
        if(rs != null) {
            while (rs != null && rs.next()) {
                context = rs.getString("context");
                name = rs.getString("name");
                value = rs.getString("value");

                GlobalParmVO obj = new GlobalParmVO(context, name, value);
                
                globalParmMap.put(obj, obj);
                
                frpGlobalParmMapCacheObject.put((String) context + name, value);
            }
        }
        
        // Load the FTDAppsGlobalParmsVO object
        FTDAppsGlobalParmsVO ftdAppsGlobalParmsVOCacheObject = new FTDAppsGlobalParmsVO();
        rs = osdao.executeQuery(conn, CacheMgrConstants.STMT_GET_FTD_APPS_GLOBAL_PARMS);
        
        if(rs == null)
        {
            // Statement not defined, currently not implemented in mysql
            logger.warn("No Global Parms VO Object retrieved from database");
        }
        
        while(rs != null && rs.next())
        {
            ftdAppsGlobalParmsVOCacheObject.setGnaddLevel(((BigDecimal) rs.getObject(1)).longValue());
            ftdAppsGlobalParmsVOCacheObject.setGnaddDate(this.formatStringToUtilDate((String) rs.getObject(2)));
            ftdAppsGlobalParmsVOCacheObject.setIntlOverrideDays(((BigDecimal) rs.getObject(3)).longValue());
            ftdAppsGlobalParmsVOCacheObject.setMondayCutoff((String) rs.getObject(4));
            ftdAppsGlobalParmsVOCacheObject.setTuesdayCutoff((String) rs.getObject(5));
            ftdAppsGlobalParmsVOCacheObject.setWednesdayCutoff((String) rs.getObject(6));
            ftdAppsGlobalParmsVOCacheObject.setThursdayCutoff((String) rs.getObject(7));
            ftdAppsGlobalParmsVOCacheObject.setFridayCutoff((String) rs.getObject(8));
            ftdAppsGlobalParmsVOCacheObject.setSaturdayCutoff((String) rs.getObject(9));
            ftdAppsGlobalParmsVOCacheObject.setSundayCutoff((String) rs.getObject(10));
            ftdAppsGlobalParmsVOCacheObject.setExoticCutoff((String) rs.getObject(11));
            ftdAppsGlobalParmsVOCacheObject.setFreshCutsCutoff((String) rs.getObject(12));
            ftdAppsGlobalParmsVOCacheObject.setSpecialtyGiftCutoff((String) rs.getObject(13));
            ftdAppsGlobalParmsVOCacheObject.setIntlCutoff((String) rs.getObject(14));
            ftdAppsGlobalParmsVOCacheObject.setDeliveryDaysOut(((BigDecimal) rs.getObject(15)).longValue());
            ftdAppsGlobalParmsVOCacheObject.setCheckJCPenney((String) rs.getObject(16));
            ftdAppsGlobalParmsVOCacheObject.setFreshCutsSvcCharge((BigDecimal) rs.getObject(17));
            ftdAppsGlobalParmsVOCacheObject.setSpecialSvcCharge((BigDecimal) rs.getObject(18));
            ftdAppsGlobalParmsVOCacheObject.setFreshCutsSvcChargerTrigger((String) rs.getObject(19));
            ftdAppsGlobalParmsVOCacheObject.setCanadianExchangeRate((BigDecimal) rs.getObject(20));
            ftdAppsGlobalParmsVOCacheObject.setFreshCutsSatCharge((BigDecimal) rs.getObject(21));
        }        
        
        
        handlerMap.put(CacheMgrConstants.CACHE_KEY_ID_GEN, globalParmMap);
        handlerMap.put(this.FTD_APPS_GLOBAL_PARMS_VO_KEY, ftdAppsGlobalParmsVOCacheObject);
        handlerMap.put(FRP_GLOBAL_PARMS_MAP_KEY, frpGlobalParmMapCacheObject);
        
        return handlerMap;
    }
    
    /**
     * Set the cached object in the cache handler. The cache handler is then 
     * responsible for fulfilling all application level API calls, to access the
     * data in the cached object.
     * 
     * @param cachedObject
     * @throws com.ftd.osp.utilities.cache.exception.CacheException
     */    
      public void setCachedObject(Object cachedObject) throws CacheException
      {
        try 
        {
          Map cacheMap=(Map) cachedObject;
          this.objMap = (Map)cacheMap.get(CacheMgrConstants.CACHE_KEY_ID_GEN);
          this.frpGlobalParmMap = (Map)cacheMap.get(this.FRP_GLOBAL_PARMS_MAP_KEY);  
          
          if(cacheMap.get(this.FTD_APPS_GLOBAL_PARMS_VO_KEY)!=null){
        	  this.ftdAppsGlobalParmsVO=(FTDAppsGlobalParmsVO)cacheMap.get(this.FTD_APPS_GLOBAL_PARMS_VO_KEY);
          }else{
        	  super.logger.warn("No value found for "+this.FTD_APPS_GLOBAL_PARMS_VO_KEY);
          }
        } catch (Exception ex) 
        {
        	super.logger.error(ex);
          throw new CacheException("Could not set the cached object.");
        } 
      }
      
      
      /**
       * 
       * @param strDate
       * @return 
       * @throws java.lang.Exception
       */
        private Date formatStringToUtilDate(String strDate) throws CacheException 
        {
            java.util.Date utilDate = null;
            String inDateFormat = "";
            int dateLength = 0;
            int firstSep = 0;
            int lastSep = 0;

            if ((strDate != null) && (!strDate.trim().equals(""))) {
                try {

                    // set input date format
                    dateLength = strDate.length();
                    if ( dateLength > 10) {
                        inDateFormat = "yyyy-MM-dd hh:mm:ss";
                    } else {
                        firstSep = strDate.indexOf("/");
                        lastSep = strDate.lastIndexOf("/");

                        switch ( dateLength ) {
                            case 10:
                                inDateFormat = "MM/dd/yyyy";
                                break;
                            case 9:
                                if ( firstSep == 1 ) {
                                    inDateFormat = "M/dd/yyyy";
                                } else {
                                    inDateFormat = "MM/d/yyyy";
                                }
                                break;
                            case 8:
                                if ( firstSep == 1 ) {
                                    inDateFormat = "M/d/yyyy";
                                } else {
                                    inDateFormat = "MM/dd/yy";
                                }
                                break;
                            case 7:
                                if ( firstSep == 1 ) {
                                    inDateFormat = "M/dd/yy";
                                } else {
                                    inDateFormat = "MM/d/yy";
                                }
                                break;
                            case 6:
                                inDateFormat = "M/d/yy";
                                break;
                            default:
                                break;
                        }
                    }
                    SimpleDateFormat sdfInput = new SimpleDateFormat( inDateFormat );
                    SimpleDateFormat sdfOutput = new SimpleDateFormat ( "yyyy-MM-dd" );

                    java.util.Date date = sdfInput.parse(strDate);
                    String outDateString = sdfOutput.format(date);

                    // now that we have no errors, use the string to make a Util date
                    utilDate = sdfOutput.parse(outDateString);

                    } catch (Exception e) {
                       super.logger.error(e);
                       throw new CacheException(e.getMessage(), e);
                }
            }
            return utilDate;
        }      
      
      /**
       * 
       * @return 
       */
        public FTDAppsGlobalParmsVO getFTDAppsGlobalParms()
        {
            return ftdAppsGlobalParmsVO;
        }

      /**
       * Returns the Global Parameter for the specific context and name
       * @param context
       * @param name
       * @return
       * @throws CacheException
       */
      public String getGlobalParm(String context, String name) throws CacheException
      {
          GlobalParmVO key = new GlobalParmVO(context, name, null);
          
          Object cachedObj = this.objMap;
          if (cachedObj == null) {
                throw new CacheException("Cached object is null.");
          }
          Object obj = this.objMap.get(key);
          String v = null;
          if(obj != null) {
                GlobalParmVO value = (GlobalParmVO)obj;
                v = value.getValue();
          } 
          return v;
      }
      
      
      /**
       * For compatibility with previous handler. Just calls
       * {@link #getGlobalParm(String, String)}
       * @param context
       * @param name
       * @return
       * @throws CacheException
       */
      public String getFrpGlobalParm(String context, String name) throws CacheException
      {
          return getGlobalParm(context, name);          
      }
      
      /**
       * @return For compatibility with previous handler. Returns the Map<String,String> context+name=value
       */
      public Map<String, String> getFrpGlobalParmMap()
      {
          return Collections.unmodifiableMap(frpGlobalParmMap);
      }
}
