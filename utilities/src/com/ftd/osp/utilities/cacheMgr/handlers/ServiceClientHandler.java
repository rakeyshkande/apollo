package com.ftd.osp.utilities.cacheMgr.handlers;

import com.ftd.osp.utilities.cacheMgr.constant.CacheMgrConstants;
import com.ftd.osp.utilities.cacheMgr.dao.CacheMgrDAO;
import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.ClientVO;
import com.ftd.osp.utilities.cacheMgr.vo.CacheHandlerBase;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * This class is responsible for loading client into cache.
 * key: ALL
 * value: List<ClientVO>
 */
public class ServiceClientHandler extends CacheHandlerBase
{
    private Map objMap;
    public Object load(Connection conn) throws CacheException {
        logger = new Logger(this.getClass().getName());
        CacheMgrDAO osdao = new CacheMgrDAO();
        CachedResultSet rs = osdao.executeQuery(conn, CacheMgrConstants.STMT_GET_ORDER_SERVICE_CLIENT);

        Map handlerMap = new HashMap();
        Map clientMap = new HashMap();
        ClientVO cvo = null;
        
        if(logger.isDebugEnabled())
          logger.debug("Loaded service clients...");
        while (rs != null && rs.next()) {   
            
            String clientId = rs.getString("client_id");
            String displayName = rs.getString("client_name");
            cvo = new ClientVO(clientId, displayName);
            if(logger.isDebugEnabled())
            {
              logger.debug("client id:" + clientId);
              logger.debug("clientname: " + displayName);
            }
            clientMap.put(cvo, cvo);
        }
        
        handlerMap.put(CacheMgrConstants.CACHE_KEY_ID_GEN, clientMap);
        return handlerMap;
    }
    
    /**
     * Set the cached object in the cache handler. The cache handler is then 
     * responsible for fulfilling all application level API calls, to access the
     * data in the cached object.
     * 
     * @param cachedObject
     * @throws com.ftd.osp.utilities.cache.exception.CacheException
     */    
      public void setCachedObject(Object cachedObject) throws CacheException
      {
        try 
        {
            this.objMap = (Map)((Map) cachedObject).get(CacheMgrConstants.CACHE_KEY_ID_GEN);  
        } catch (Exception ex) 
        {
          super.logger.error(ex);
          throw new CacheException("Could not set the cached object.");
        } 
      }
      
      public ClientVO getClientById(String clientId) throws CacheException
      {
          ClientVO key = new ClientVO(clientId, null);
          ClientVO value = null;
          
          Object cachedObj = this.objMap;
          if (cachedObj == null) {
                throw new CacheException("Cached object is null.");
          }
          Object obj = this.objMap.get(key);

          if(obj != null) {
                value = (ClientVO)obj;     
          } 
          return value;
      }
      
      public List getClientList() throws CacheException {
          List clientList = new ArrayList();
          try {
              Set key = this.objMap.keySet();
              Iterator i = key.iterator();
              while (i.hasNext()) {
                 ClientVO k = (ClientVO)i.next();
                 ClientVO v = (ClientVO)objMap.get(k); 
                 clientList.add(v);
              }
          } catch(Exception e) {
              logger.error(e);
              throw new CacheException(e.getMessage(), e);
          }
          return clientList;
      }
}
