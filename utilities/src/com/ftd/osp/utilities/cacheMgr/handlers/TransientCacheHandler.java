package com.ftd.osp.utilities.cacheMgr.handlers;

import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.TransientCacheExpirationTimer;
import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.cacheMgr.vo.CacheHandlerBase;
import com.ftd.osp.utilities.cacheMgr.vo.TransientCacheVO;
import com.ftd.osp.utilities.cacheMgr.vo.TransientCacheElementVO;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;

import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;

/**
 */
public class TransientCacheHandler extends CacheHandlerBase
{
    private TransientCacheVO transCacheVO;
    
    public TransientCacheHandler() {
    }


    /**
    * Returns an empty TransientCacheVO object that will be cached and used to maintain
    * user-added Transient cache objects.
    * This is the implementation of the abstract method in CacheHandlerBase.
    * This should not be called by the user.
    *
    * Note, unlike Fixed caches, this method is only invoked once (when the cache is first created) for a Transient cache.  
    * This because, the load method of any handler is only invoked when getHandler is called 
    * and there is no object already cached (in CacheVO.cacheObj). 
    * Since we never clear the cacheObj for Transient caches, it's only called once. 
    */
    public Object load(Connection con) throws CacheException {
        TransientCacheVO tcvo;
        try {
            if(super.logger.isDebugEnabled())
              super.logger.debug("BEGIN LOADING TRANSIENT HANDLER CACHE..."); 
            tcvo = new TransientCacheVO();
            if(super.logger.isDebugEnabled())
              super.logger.debug("END LOADING TRANSIENT HANDLER CACHE...");         
        } catch(Exception e) {
            super.logger.error(e);
            throw new CacheException("Could not load Transient cache");
        }
        return tcvo;
   }

    
    /**
    * Set the cached object in the cache handler. 
    * This is the implementation of the abstract method in CacheHandlerBase.
    * This should not be called by the user.
    */
    public void setCachedObject(Object cachedObject) throws CacheException {
        try  {
            this.transCacheVO = (TransientCacheVO) cachedObject;  
        } catch (Exception ex) {
            super.logger.error(ex);
            throw new CacheException("Could not set the cached object.");
        } 
    }
    

    /**
     * User called method to Get the specified user-saved object from the cache.
     * The user must have already saved the object from a prior call to addCacheElement.
     */
    public Object getCacheElement(Object key) throws CacheException {
        Object retObj = null;
        try  {
            TransientCacheElementVO tcevo = (TransientCacheElementVO) transCacheVO.getTransElement(key);
            if (tcevo != null) {
                tcevo.incrementElementAccessCount();
                retObj = (Object) tcevo.getCacheElementObj();
            }
        } catch (Exception ex) {
            super.logger.error(ex);
            throw new CacheException("Could not get cache element for given key: " + key);
        } 
        return retObj;
    }
    

    /**
     * User called method to add the object to the cache.
     */
    public void addCacheElement(Object key, Object obj) throws CacheException {
        TransientCacheElementVO tcevo = null;
        try  {
        
            // If key already in cache, just save new object and update time
            //
            tcevo = (TransientCacheElementVO) transCacheVO.getTransElement(key);
            if (tcevo != null) {
                tcevo.setUpdateTime(new Date());
                tcevo.setCacheElementObj((Object) obj);

            // Otherwise key is new so create new VO, populate and shove it in the TransientCacheVO
            //
            } else {
                tcevo = new TransientCacheElementVO();
                tcevo.setElementKey(key);
                tcevo.setCreationTime(new Date());
                tcevo.setUpdateTime(tcevo.getCreationTime());
                tcevo.setElementAccessCount(0);
                tcevo.setCacheElementObj((Object) obj);
                transCacheVO.addTransElement(key, tcevo);
            }
            
            // Add entry to element array (which is used in expiration handling).
            // Note if key was already in cache, we will be adding a second entry here, but
            // we'll deal with this in checkExpireAndPurge.
            //
            transCacheVO.addTransElementTimeArray(tcevo);

        } catch (Exception ex) {
            super.logger.error(ex);
            throw new CacheException("Could not save cache element for given key: " + key);
        } 
    }

    
    /**
     * Looks for expired elements and purges them from the cache.
     * This is only called from TransientCacheExpirationTimer.
     */
    public void checkExpireAndPurge(String cacheName, long expireRateInMinutes) {    
        TransientCacheElementVO tcevo;
        int elementsToProcess = transCacheVO.sizeOfElementTimeArray();
        int purgeCache = 0;
        int purgeArray = 0;
        long accessCount = 0;
        long maxAccessCount = 0;
        long avgAccessCount = 0;

        // Determine expiration time (i.e., any elements with time less than this should be expired)
        //
        long expireTime = System.currentTimeMillis() - (expireRateInMinutes * 60000);

        // Loop over element array and compare times.  
        //
        // Note we don't use an iterator for the loop since (even though list is synchronized) 
        // we would still have to use a synchronized block.
        //
        while (elementsToProcess > 0) { 
            long elementArrayTime = transCacheVO.getElementArrayTime(0).getTime();
            if (elementArrayTime <= expireTime) {

                // Found element that expired so purge it from cache and/or time array.
                // Note we just use index of 0 since they should be ordered from oldest to newest
                // and we immediately purge expired, so next array item will be shifted to 0 index.
                //
                long elementUpdateTime = transCacheVO.getElementUpdateTime(0).getTime();
                if (elementArrayTime < elementUpdateTime) {

                    // If we get here then element was refreshed by user and should not be expired.
                    // Instead, just get rid of this entry in element time array only (since there will be one per refresh).
                    // See comments in addCacheElement (above) and in TransientCacheVO.java for more details.
                    //
                    transCacheVO.removeTransElementTimeArray(0);
                    purgeArray++;
                    
                } else {

                    // Purge cache object and remove from element time array
                    //
                    accessCount = transCacheVO.removeTransElement(0);
                    transCacheVO.removeTransElementTimeArray(0);
                    purgeCache++;
                    purgeArray++;
                    if (accessCount > maxAccessCount) {
                        maxAccessCount = accessCount;
                    }
                    avgAccessCount += accessCount;
                }
                
            } else {
            
                // This element has not expired.  Since elements are oldest to newest, we can just exit now.
                //
                break;
            }
            elementsToProcess = transCacheVO.sizeOfElementTimeArray();

        } // end while
        
        if (purgeArray > 0) {
            int totalCache = transCacheVO.sizeOfElementMap();
            StringBuilder stats = new StringBuilder(CacheManager.CACHE_STATS_LOG_PREFIX);
            stats.append(" Transient cache expiration handler: ");
            stats.append(cacheName);
            stats.append(" - Purged objs/timers: ");
            stats.append(purgeCache);
            stats.append("/");
            stats.append(purgeArray); 
            stats.append(" Remaining objs/timers: ");
            stats.append(totalCache);
            stats.append("/");
            stats.append(elementsToProcess);
            if (purgeCache > 0) {
                stats.append(" - Maximum/Average access count (of those purged): ");
                stats.append(maxAccessCount);
                stats.append("/");
                stats.append(avgAccessCount/purgeCache);
            }
            if(super.logger.isInfoEnabled())
              super.logger.info(stats.toString());
        }
    }
}