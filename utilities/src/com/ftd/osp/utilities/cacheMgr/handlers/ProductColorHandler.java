package com.ftd.osp.utilities.cacheMgr.handlers;

import com.ftd.osp.utilities.cacheMgr.constant.CacheMgrConstants;
import com.ftd.osp.utilities.cacheMgr.dao.CacheMgrDAO;
import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.ColorMasterVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.CompanyMasterVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.SourceMasterVO;
import com.ftd.osp.utilities.cacheMgr.vo.CacheHandlerBase;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * This class is responsible for loading product color into cache.
 * key: productId
 * value: ColorMasterVO
 */
public class ProductColorHandler extends CacheHandlerBase
{
    private Map objMap;
    public Object load(Connection conn) throws CacheException {
        logger = new Logger(this.getClass().getName());
        CacheMgrDAO osdao = new CacheMgrDAO();
        CachedResultSet rs = osdao.executeQuery(conn, CacheMgrConstants.STMT_GET_PRODUCT_COLOR);

        Map handlerMap = new HashMap();
        Map productColorMap = new HashMap();
        String productId = null;
        String prevProductId = null;
        List<ColorMasterVO> colorList = null;
        ColorMasterVO cmvo = null;
        
        if(logger.isInfoEnabled())
          logger.info("Loaded product color...");
        while (rs != null && rs.next()) {   
            productId = rs.getString("product_id");
            cmvo = new ColorMasterVO();
            cmvo.setColorId(rs.getString("color_master_id"));
            cmvo.setColorDesc(rs.getString("description"));

            if(!productId.equals(prevProductId)) {
                if(prevProductId != null) {
                    productColorMap.put(prevProductId, colorList);
                }
                colorList = new ArrayList();
            }
            colorList.add(cmvo);

            prevProductId = productId;
        }
        
        if(prevProductId != null) {
            productColorMap.put(prevProductId, colorList);
        }
        
        handlerMap.put(CacheMgrConstants.CACHE_KEY_ID_GEN, productColorMap);
        return handlerMap;
    }
    
    /**
     * Set the cached object in the cache handler. The cache handler is then 
     * responsible for fulfilling all application level API calls, to access the
     * data in the cached object.
     * 
     * @param cachedObject
     * @throws com.ftd.osp.utilities.cache.exception.CacheException
     */    
      public void setCachedObject(Object cachedObject) throws CacheException
      {
        try 
        {
            this.objMap = (Map)((Map) cachedObject).get(CacheMgrConstants.CACHE_KEY_ID_GEN);  
        } catch (Exception ex) 
        {
          super.logger.error(ex);
          throw new CacheException("Could not set the cached object.");
        } 
      }
      
      public List<ColorMasterVO> getProductColor(String productId) throws CacheException
      {
          String key = productId;
          List<ColorMasterVO> value = null;
          
          Object cachedObj = this.objMap;
          if (cachedObj == null) {
                throw new CacheException("Cached object is null.");
          }
          Object obj = this.objMap.get(key);

          if(obj != null) {
                value = Collections.unmodifiableList((List<ColorMasterVO>)obj);     
          } 
          return value;
      }
      

}
