package com.ftd.osp.utilities.cacheMgr.handlers.vo;


import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**

 */
public class CountryMasterVO
{
    private String countryId;
    private String countryName;
    private String countryType;
    private int hashcode;
    
    private BigDecimal displayOrder;
    private String mondayClosed;
    private String tuesdayClosed;
    private String wednesdayClosed;
    private String thursdayClosed;
    private String fridayClosed;
    private String saturdayClosed;
    private String sundayClosed;
    private long   addonDays;
    private String threeCharacterId;  //ISO Alpha 3 code
    private String status;
    private String cutoffTime;
    private String mondayTransit;
    private String tuesdayTransit;
    private String wednesdayTransit;
    private String thursdayTransit;
    private String fridayTransit;
    private String saturdayTransit;
    private String sundayTransit;
    
    public CountryMasterVO () {}
    
    public CountryMasterVO (String countryId) {
        this.countryId = countryId;
    }
    
    //overwriting hashCode() of Object
    public int hashCode() {
        if(this.hashcode != 0) {
            return this.hashcode;
        } else {
            hashcode = countryId.hashCode();
        }
        return hashcode;
    }
    //overwirting equals() of Ojbect
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        
        CountryMasterVO obj = (CountryMasterVO)o;

        if(this.getCountryId() == null || obj.getCountryId() == null) {
            return false;
        }
        if(!obj.getCountryId().equals(this.getCountryId())) {
            return false;
        }

        return true;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryType(String countryType) {
        this.countryType = countryType;
    }

    public String getCountryType() {
        return countryType;
    }

    public BigDecimal getDisplayOrder()
    {
        return displayOrder;
    }

    public void setDisplayOrder(BigDecimal displayOrder)
    {
        this.displayOrder = displayOrder;
    }

    public String getMondayClosed()
    {
        return mondayClosed;
    }

    public void setMondayClosed(String mondayClosed)
    {
        this.mondayClosed = mondayClosed;
    }

    public String getTuesdayClosed()
    {
        return tuesdayClosed;
    }

    public void setTuesdayClosed(String tuesdayClosed)
    {
        this.tuesdayClosed = tuesdayClosed;
    }

    public String getWednesdayClosed()
    {
        return wednesdayClosed;
    }

    public void setWednesdayClosed(String wednesdayClosed)
    {
        this.wednesdayClosed = wednesdayClosed;
    }

    public String getThursdayClosed()
    {
        return thursdayClosed;
    }

    public void setThursdayClosed(String thursdayClosed)
    {
        this.thursdayClosed = thursdayClosed;
    }

    public String getFridayClosed()
    {
        return fridayClosed;
    }

    public void setFridayClosed(String fridayClosed)
    {
        this.fridayClosed = fridayClosed;
    }

    public String getSaturdayClosed()
    {
        return saturdayClosed;
    }

    public void setSaturdayClosed(String saturdayClosed)
    {
        this.saturdayClosed = saturdayClosed;
    }

    public String getSundayClosed()
    {
        return sundayClosed;
    }

    public void setSundayClosed(String sundayClosed)
    {
        this.sundayClosed = sundayClosed;
    }

    public long getAddonDays()
    {
        return addonDays;
    }

    public void setAddonDays(long addonDays)
    {
        this.addonDays = addonDays;
    }

    public String getThreeCharacterId()
    {
        return threeCharacterId;
    }

    public void setThreeCharacterId(String threeCharacterId)
    {
        this.threeCharacterId = threeCharacterId;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getCutoffTime()
    {
        return cutoffTime;
    }

    public void setCutoffTime(String cutoffTime)
    {
        this.cutoffTime = cutoffTime;
    }

    public String getMondayTransit()
    {
        return mondayTransit;
    }

    public void setMondayTransit(String mondayTransit)
    {
        this.mondayTransit = mondayTransit;
    }

    public String getTuesdayTransit()
    {
        return tuesdayTransit;
    }

    public void setTuesdayTransit(String tuesdayTransit)
    {
        this.tuesdayTransit = tuesdayTransit;
    }

    public String getWednesdayTransit()
    {
        return wednesdayTransit;
    }

    public void setWednesdayTransit(String wednesdayTransit)
    {
        this.wednesdayTransit = wednesdayTransit;
    }

    public String getThursdayTransit()
    {
        return thursdayTransit;
    }

    public void setThursdayTransit(String thursdayTransit)
    {
        this.thursdayTransit = thursdayTransit;
    }

    public String getFridayTransit()
    {
        return fridayTransit;
    }

    public void setFridayTransit(String fridayTransit)
    {
        this.fridayTransit = fridayTransit;
    }

    public String getSaturdayTransit()
    {
        return saturdayTransit;
    }

    public void setSaturdayTransit(String saturdayTransit)
    {
        this.saturdayTransit = saturdayTransit;
    }

    public String getSundayTransit()
    {
        return sundayTransit;
    }

    public void setSundayTransit(String sundayTransit)
    {
        this.sundayTransit = sundayTransit;
    }
    
    public String toXML(int count) {
    	// Aliases to map to CacheVO from original CacheController
    	Map<String, String> fieldAliasMap = new HashMap<String, String>();
    	fieldAliasMap.put("countryId", "id");
    	fieldAliasMap.put("countryName", "description");
    	fieldAliasMap.put("countryType", "type");
    	
    	return toXML(count, fieldAliasMap);
    }
    
    /**
     * This method uses the Reflection API to generate an XML string that will be
     * passed back to the calling module.
     * The XML string will contain all the fields within this VO.
     *
     * @param  None
     * @return XML string
    **/
   public String toXML(int count, Map<String, String> fieldAliasMap)
   {
     StringBuffer sb = new StringBuffer();
     try
     {
       if (count == 0)
       {
         sb.append("<COUNTRY>");
       }
       else
       {
         sb.append("<COUNTRY num=" + '"' + count + '"' + ">");
       }
       
       Field[] fields = this.getClass().getDeclaredFields();

       for (int i = 0; i < fields.length; i++)
       {
         String fieldName = getAlias(fields[i].getName(), fieldAliasMap);
         if (fields[i].get(this) == null)
         {
           sb.append("<" + fieldName + "/>");
         }
         else
         {
           sb.append("<" + fieldName + ">");
           sb.append(fields[i].get(this));
           sb.append("</" + fieldName + ">");
         }
       }
       sb.append("</COUNTRY>");
     }

     catch (Exception e)
     {
       e.printStackTrace();
     }

     return sb.toString();
   }
   
   /**
    * Retrieve the aliased name for a field from the specified map.
    * @param name
    * @param aliasMap
    * @return
    */
   private String getAlias(String name, Map<String, String> aliasMap)
   {
	   String retVal = aliasMap.get(name);
	   
	   if(retVal == null)
	   {
		   return name;
	   }
	   
	   return retVal;
   }
}
