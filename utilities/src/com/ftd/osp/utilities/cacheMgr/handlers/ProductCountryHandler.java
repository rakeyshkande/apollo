package com.ftd.osp.utilities.cacheMgr.handlers;

import com.ftd.osp.utilities.cacheMgr.constant.CacheMgrConstants;
import com.ftd.osp.utilities.cacheMgr.dao.CacheMgrDAO;
import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.CountryMasterVO;
import com.ftd.osp.utilities.cacheMgr.vo.CacheHandlerBase;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * This class is responsible for loading product country into cache.
 * key: productId
 * value: CountryMasterVO
 */
public class ProductCountryHandler extends CacheHandlerBase
{
    private Map objMap;
    public Object load(Connection conn) throws CacheException {
        logger = new Logger(this.getClass().getName());
        CacheMgrDAO osdao = new CacheMgrDAO();
        CachedResultSet rs = osdao.executeQuery(conn, CacheMgrConstants.STMT_GET_PRODUCT_COUNTRY);

        Map handlerMap = new HashMap();
        Map productCountryMap = new HashMap();
        String productId = null;
        String prevProductId = null;
        List<CountryMasterVO> countryList = null;
        CountryMasterVO cmvo = null;
        
        if(logger.isInfoEnabled())
          logger.info("Loaded product country...");
        while (rs != null && rs.next()) {   
            productId = rs.getString("product_id");
            cmvo = new CountryMasterVO();
            cmvo.setCountryId(rs.getString("country_id"));
            cmvo.setCountryName(rs.getString("name"));

            if(!productId.equals(prevProductId)) {
                if(prevProductId != null) {
                    productCountryMap.put(prevProductId, countryList);
                }
                countryList = new ArrayList();
            }
            countryList.add(cmvo);

            prevProductId = productId;
        }
        
        if(prevProductId != null) {
            productCountryMap.put(prevProductId, countryList);
        }
        
        handlerMap.put(CacheMgrConstants.CACHE_KEY_ID_GEN, productCountryMap);
        return handlerMap;
    }
    
    /**
     * Set the cached object in the cache handler. The cache handler is then 
     * responsible for fulfilling all application level API calls, to access the
     * data in the cached object.
     * 
     * @param cachedObject
     * @throws com.ftd.osp.utilities.cache.exception.CacheException
     */    
      public void setCachedObject(Object cachedObject) throws CacheException
      {
        try 
        {
            this.objMap = (Map)((Map) cachedObject).get(CacheMgrConstants.CACHE_KEY_ID_GEN);  
        } catch (Exception ex) 
        {
          super.logger.error(ex);
          throw new CacheException("Could not set the cached object.");
        } 
      }
      
      public List<CountryMasterVO> getProductCountry(String productId) throws CacheException
      {
          String key = productId;
          List<CountryMasterVO> value = null;
          
          Object cachedObj = this.objMap;
          if (cachedObj == null) {
                throw new CacheException("Cached object is null.");
          }
          Object obj = this.objMap.get(key);

          if(obj != null) {
                value = Collections.unmodifiableList((List<CountryMasterVO>)obj);     
          } 
          return value;
      }
      

}
