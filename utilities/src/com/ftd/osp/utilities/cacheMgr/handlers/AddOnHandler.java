package com.ftd.osp.utilities.cacheMgr.handlers;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.ftd.osp.utilities.cacheMgr.constant.CacheMgrConstants;
import com.ftd.osp.utilities.cacheMgr.dao.CacheMgrDAO;
import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.AddonVO;
import com.ftd.osp.utilities.cacheMgr.vo.CacheHandlerBase;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;


/**
 * This class is responsible for loading add on into cache.
 * value: AddonVO
 */
public class AddOnHandler extends CacheHandlerBase
{
    private Map objMap;
    public Object load(Connection conn) throws CacheException {
        logger = new Logger(this.getClass().getName());
        CacheMgrDAO osdao = new CacheMgrDAO();
        CachedResultSet rs = osdao.executeQuery(conn, CacheMgrConstants.STMT_GET_ADDON);

        Map handlerMap = new HashMap();
        Map addOnMap = new HashMap();
        AddonVO AddonVO = null;
        
        if(logger.isInfoEnabled())
          logger.info("Loading add ons...");
        while (rs != null && rs.next()) {   
        	AddonVO = new AddonVO();
        	AddonVO.setAddonId(rs.getString("ADDON_ID"));
            AddonVO.setAddonTypeId(rs.getString("ADDON_TYPE"));
            AddonVO.setAddonText(rs.getString("ADDON_TEXT"));
            AddonVO.setAddOnUNSPSC(rs.getString("UNSPSC"));
            AddonVO.setAddonDesc(rs.getString("ADD_ON_DESCRIPTION"));
            AddonVO.setAddonPrice(new BigDecimal(rs.getString("PRICE")));
            AddonVO.setAddOnWeight(rs.getString("ADDON_WEIGHT"));
            AddonVO.setAddOnAvailableFlag(rs.getString("ADD_ON_ACTIVE_FLAG"));
            AddonVO.setDefaultPerTypeFlag(rs.getString("DEFAULT_PER_TYPE_FLAG"));
            AddonVO.setDisplayPriceFlag(rs.getString("DISPLAY_PRICE_FLAG"));
            AddonVO.setAddOnTypeDescription(rs.getString("ADD_ON_TYPE_DESCRIPTION"));
            AddonVO.setAddOnTypeIncludedInProductFeedFlag("PRODUCT_FEED_FLAG");
            AddonVO.setProductId(rs.getString("PRODUCT_ID"));
            AddonVO.setOccasionId(rs.getString("OCCASION_ID"));
            AddonVO.setOccasionDesc(rs.getString("OCCASION_DESCRIPTION"));
       	          
            addOnMap.put(AddonVO.getAddonId(), AddonVO);
        }
        
        handlerMap.put(CacheMgrConstants.CACHE_KEY_ID_GEN, addOnMap);
        return handlerMap;
    }
    
    /**
     * Set the cached object in the cache handler. The cache handler is then 
     * responsible for fulfilling all application level API calls, to access the
     * data in the cached object.
     * 
     * @param cachedObject
     * @throws com.ftd.osp.utilities.cache.exception.CacheException
     */    
      public void setCachedObject(Object cachedObject) throws CacheException
      {
        try 
        {
            this.objMap = (Map)((Map) cachedObject).get(CacheMgrConstants.CACHE_KEY_ID_GEN);  
        } catch (Exception ex) 
        {
          super.logger.error(ex);
          throw new CacheException("Could not set the cached object.");
        } 
      }
      
      public AddonVO getAddOnById(String addOnId) throws CacheException
      {
          AddonVO value = null;
          
          Object cachedObj = this.objMap;
          if (cachedObj == null) {
                throw new CacheException("Cached object is null.");
          }
          Object obj = this.objMap.get(addOnId);

          if(obj != null) {
                value = (AddonVO)obj;     
          } 
          return value;
      }
      
    
    public List<String> getAllAddOns() throws Exception {
        List<String> addOnList = new ArrayList<String>();
        Map webIdMap = this.objMap;
        Set s = webIdMap.keySet();
        Iterator i = s.iterator();
        
        while (i.hasNext()) {
            String addOnId = (String)i.next();
            addOnList.add(addOnId);
        }
        return addOnList;
    }
}

