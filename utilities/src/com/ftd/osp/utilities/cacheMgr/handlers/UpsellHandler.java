package com.ftd.osp.utilities.cacheMgr.handlers;

import com.ftd.osp.utilities.cacheMgr.constant.CacheMgrConstants;
import com.ftd.osp.utilities.cacheMgr.dao.CacheMgrDAO;
import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.SourceMasterVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.UpsellDetailVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.UpsellMasterVO;
import com.ftd.osp.utilities.cacheMgr.vo.CacheHandlerBase;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * This class is responsible for loading source code into cache.
 * key: sourceCode
 * value: SourceMasterVO
 */
public class UpsellHandler extends CacheHandlerBase
{
    private Map objMapByUpsellMasterId;
    private Map objMapByUpsellDetailId;
    private String KEY_UPSELL_DETAIL_LOOKUP = "KEY_UPSELL_DETAIL_LOOKUP";
    private String KEY_UPSELL_MASTER = "KEY_UPSELL_MASTER";
    public Object load(Connection conn) throws CacheException {
        logger = new Logger(this.getClass().getName());
        CacheMgrDAO osdao = new CacheMgrDAO();
        CachedResultSet rs = osdao.executeQuery(conn, CacheMgrConstants.STMT_GET_UPSELL_DETAIL);

        Map handlerMap = new HashMap();
        Map upsellMasterMap = new HashMap();
        Map upsellDetailMap = new HashMap();
        String prevUpsellMasterId = null;
        String upsellMasterId = null;
        List<UpsellDetailVO> upsellDetailList = new ArrayList();
        UpsellMasterVO umvo = null;
        UpsellMasterVO prevumvo = null;
        
        if(logger.isInfoEnabled())
          logger.info("Loading upsell...");

        while (rs != null && rs.next()) {   
            
            // build the detail lookup cache
            upsellMasterId = rs.getString("upsell_master_id");
            String upsellDetailId = rs.getString("upsell_detail_id");
            upsellDetailMap.put(upsellDetailId, upsellMasterId);
            
            // build the master lookup list
            UpsellDetailVO udvo = new UpsellDetailVO();
            udvo.setUpsellMasterId(upsellMasterId);
            udvo.setUpsellDetailId(upsellDetailId);
            udvo.setNovatorId(rs.getString("novator_id"));
            udvo.setPriceAmt(rs.getBigDecimal("standard_price"));
            udvo.setNovatorName(rs.getString("novator_name"));
            udvo.setUpsellDetailName(rs.getString("upsell_Detail_name"));
            udvo.setUpsellDetailSequenceNum(rs.getString("upsell_detail_sequence"));
            /*
            udvo.setGbbUpsellDetailSequenceNum(rs.getString("gbb_upsell_detail_sequence_num"));
            udvo.setGbbNameOverrideFlag(rs.getString("gbb_name_override_flag"));
            udvo.setGbbNameOverrideTxt(rs.getString("gbb_name_override_txt"));
            udvo.setGbbPriceOverrideFlag(rs.getString("gbb_price_override_flag"));
            udvo.setGbbPriceOverrideTxt(rs.getString("gbb_price_override_txt"));
            */
            udvo.setDefaultSkuFlag(rs.getString("default_sku_flag"));
            
            umvo  = new UpsellMasterVO();
            umvo.setUpsellMasterId(rs.getString("upsell_master_id"));
            umvo.setUpsellName(rs.getString("upsell_name"));
            umvo.setUpsellStatus(rs.getString("upsell_status"));
            umvo.setGbbPopoverFlag(rs.getString("gbb_popover_flag"));
            umvo.setGbbTitleTxt(rs.getString("gbb_title_txt"));
            
            if(prevUpsellMasterId == null) {
                prevUpsellMasterId = upsellMasterId;
            }
            if(upsellMasterId.equals(prevUpsellMasterId)) {
                upsellDetailList.add(udvo);
            } else {
                prevumvo.setUpsellDetailList(upsellDetailList);
                upsellMasterMap.put(prevumvo, prevumvo);
                upsellDetailList = new ArrayList();
                upsellDetailList.add(udvo);
                prevUpsellMasterId = upsellMasterId;
            }
            prevumvo = umvo;
        }
    
        // The last in list
        if(prevumvo != null) {
            prevumvo.setUpsellDetailList(upsellDetailList);
            upsellMasterMap.put(prevumvo, prevumvo);
        }
        
        handlerMap.put(KEY_UPSELL_MASTER, upsellMasterMap);
        handlerMap.put(KEY_UPSELL_DETAIL_LOOKUP, upsellDetailMap);
        
        return handlerMap;
    }
    
    /**
     * Set the cached object in the cache handler. The cache handler is then 
     * responsible for fulfilling all application level API calls, to access the
     * data in the cached object.
     * 
     * @param cachedObject
     * @throws com.ftd.osp.utilities.cache.exception.CacheException
     */    
      public void setCachedObject(Object cachedObject) throws CacheException
      {
        try 
        {
            this.objMapByUpsellMasterId = (Map)((Map) cachedObject).get(KEY_UPSELL_MASTER);  
            this.objMapByUpsellDetailId = (Map)((Map) cachedObject).get(KEY_UPSELL_DETAIL_LOOKUP);  
        } catch (Exception ex) 
        {
          super.logger.error(ex);
          throw new CacheException("Could not set the cached object.");
        } 
      }
      
      public UpsellMasterVO getUpsellMasterById(String upsellMasterId) throws CacheException
      {
          UpsellMasterVO key = new UpsellMasterVO(upsellMasterId);
          UpsellMasterVO value = null;
          
          Object cachedObj = this.objMapByUpsellMasterId;
          if (cachedObj == null) {
                throw new CacheException("Cached object is null.");
          }
          Object obj = this.objMapByUpsellMasterId.get(key);

          if(obj != null) {
                value = (UpsellMasterVO)obj;     
          } 
          return value;
      }
    public String getUpsellMasterIdByDetailId(String upsellDetailId) throws CacheException
    {
        String key = upsellDetailId;
        String value = null;
        
        Object cachedObj = this.objMapByUpsellDetailId;
        if (cachedObj == null) {
              throw new CacheException("Cached object is null.");
        }
        Object obj = this.objMapByUpsellDetailId.get(key);

        if(obj != null) {
              value = (String)obj;     
        } 
        return value;
    }
    

}
