package com.ftd.osp.utilities.cacheMgr.handlers;

import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.cacheMgr.vo.CacheHandlerBase;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;

import java.sql.Connection;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
/**
 * A cache handler is responsible for the following:
 * 
 * 1. Act as a cache loader, to load the data that needs to be cached.
 * 2. Provide application specific API, to enable access to the cached data.
 * 
 */

public class ColorHandler extends CacheHandlerBase
{
    // set by the setCachedObject method
    private Map colorsById;
    private Map colorsByDescription;

    private static final String GET_COLORS = "GET_COLORS";
    private static final String COLORS_BY_ID_KEY = "COLORS_BY_ID_KEY";
    private static final String COLORS_BY_DESCRIPTION_KEY = "COLORS_BY_DESCRIPTION_KEY";
    
    public ColorHandler()
    {        
    }
 
   /**
   * Returns the object that needs to be cached.
   * 
   * The cache cannot be configured as distributable, if the cache handler 
   * returns a custom object. In order to utilize the benefits of a distributable 
   * cache, the return objects should only be a part JDK API.
   * 
   * @param con
   * @return the object to be cached
   * @throws com.ftd.osp.utilities.cache.exception.CacheException
   */
    public Object load(Connection con) throws CacheException
    {          
        Map colorHandlerCacheObject = new HashMap();
        
        Map colorsByIdCacheObject = new HashMap();
        Map colorsByDescriptionCacheObject = new HashMap();
        try
        {
            super.logger.debug("BEGIN LOADING COLOR HANDLER...");
            DataAccessUtil dau = DataAccessUtil.getInstance();
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(con);
            dataRequest.setStatementID(GET_COLORS);
            CachedResultSet rs = (CachedResultSet)dau.execute(dataRequest);
            rs.reset();
            dataRequest.reset();
            while(rs.next())
            {
                colorsByDescriptionCacheObject.put(rs.getObject(2), rs.getObject(1));
                colorsByIdCacheObject.put(rs.getObject(1), rs.getObject(2));
            }
            super.logger.debug("END LOADING COLOR HANDLER...");
        }
        catch(Exception e)
        {
            super.logger.error(e);
            throw new CacheException("Could not load ColorHandler cache");
        }
        
        colorHandlerCacheObject.put(this.COLORS_BY_ID_KEY, colorsByIdCacheObject);
        colorHandlerCacheObject.put(this.COLORS_BY_DESCRIPTION_KEY, colorsByDescriptionCacheObject);
        logger.debug(colorsByIdCacheObject.size() + " records loaded in colors by id");
        logger.debug(colorsByDescriptionCacheObject.size() + " records loaded in colors by description");
        return colorHandlerCacheObject;
    }

  /**
   * Set the cached object in the cache handler. The cache handler is then 
   * responsible for fulfilling all application level API calls, to access the
   * data in the cached object.
   * 
   * @param cachedObject
   * @throws com.ftd.osp.utilities.cache.exception.CacheException
   */    
    public void setCachedObject(Object cachedObject) throws CacheException
    {
      try 
      {
        this.colorsByDescription = (Map)((Map) cachedObject).get(this.COLORS_BY_DESCRIPTION_KEY);  
        this.colorsById = (Map) ((Map) cachedObject).get(this.COLORS_BY_ID_KEY);  
      } catch (Exception ex) 
      {
        super.logger.error(ex);
        throw new CacheException("Could not set the cached object.");
      } 
    }

    /**
   * 
   * @param description
   * @return 
   */
    public String getColorByDescription(String description)
    {
        return (this.colorsByDescription.get(description) != null)?(String)this.colorsByDescription.get(description):null;
    }

    /**
   * 
   * @param id
   * @return 
   */
    public String getColorById(String id)
    {
        return ( this.colorsById.get(id) != null )?(String)this.colorsById.get(id):null;
    }

    /**
   * 
   * @return 
   */
    public Map getColorDescriptionMap()
    {
        return Collections.unmodifiableMap(colorsByDescription);
    }
}