package com.ftd.osp.utilities.cacheMgr.handlers.vo;
import java.math.BigDecimal;

public class SNHVO 
{
    private String id;
    private String description;
    private BigDecimal firstOrderDomestic;
    private BigDecimal secondOrderDomestic;
    private BigDecimal thirdOrderDomestic;
    private BigDecimal firstOrderInternational;
    private BigDecimal secondOrderInternational;
    private BigDecimal thirdOrderInternational;

    public SNHVO()
    {
    }

    public String getId()
    {
        return id;
    }

    public void setId(String newId)
    {
        id = newId;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String newDescription)
    {
        description = newDescription;
    }

    public BigDecimal getFirstOrderDomestic()
    {
        return firstOrderDomestic;
    }

    public void setFirstOrderDomestic(BigDecimal newFirstOrderDomestic)
    {
        firstOrderDomestic = newFirstOrderDomestic;
    }

    public BigDecimal getSecondOrderDomestic()
    {
        return secondOrderDomestic;
    }

    public void setSecondOrderDomestic(BigDecimal newSecondOrderDomestic)
    {
        secondOrderDomestic = newSecondOrderDomestic;
    }

    public BigDecimal getThirdOrderDomestic()
    {
        return thirdOrderDomestic;
    }

    public void setThirdOrderDomestic(BigDecimal newThirdOrderDomestic)
    {
        thirdOrderDomestic = newThirdOrderDomestic;
    }

    public BigDecimal getFirstOrderInternational()
    {
        return firstOrderInternational;
    }

    public void setFirstOrderInternational(BigDecimal newFirstOrderInternational)
    {
        firstOrderInternational = newFirstOrderInternational;
    }

    public BigDecimal getSecondOrderInternational()
    {
        return secondOrderInternational;
    }

    public void setSecondOrderInternational(BigDecimal newSecondOrderInternational)
    {
        secondOrderInternational = newSecondOrderInternational;
    }

    public BigDecimal getThirdOrderInternational()
    {
        return thirdOrderInternational;
    }

    public void setThirdOrderInternational(BigDecimal newThirdOrderInternational)
    {
        thirdOrderInternational = newThirdOrderInternational;
    }
}