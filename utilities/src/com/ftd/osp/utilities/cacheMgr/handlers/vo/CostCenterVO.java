package com.ftd.osp.utilities.cacheMgr.handlers.vo;

public class CostCenterVO 
{
    private String costCenterId;
    private String partnerId;
    private String sourceCode;

    public CostCenterVO()
    {
    }

    public String getCostCenterId()
    {
        return costCenterId;
    }

    public void setCostCenterId(String newCostCenterId)
    {
        costCenterId = newCostCenterId;
    }

    public String getPartnerId()
    {
        return partnerId;
    }

    public void setPartnerId(String newPartnerId)
    {
        partnerId = newPartnerId;
    }

    public String getSourceCode()
    {
        return sourceCode;
    }

    public void setSourceCode(String newSourceCode)
    {
        sourceCode = newSourceCode;
    }
}