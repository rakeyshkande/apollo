package com.ftd.osp.utilities.cacheMgr.handlers;

import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.SourceMasterVO;

public interface SourceMasterHandler
{
    SourceMasterVO getSourceCodeById(String sourceCode) throws CacheException;
}
