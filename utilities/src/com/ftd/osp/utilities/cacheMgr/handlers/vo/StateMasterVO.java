package com.ftd.osp.utilities.cacheMgr.handlers.vo;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**

 */
public class StateMasterVO
{
    private String stateMasterId;
    private String stateName;
    private String countryCode;
    private String timeZone;
    private String deliveryExclusionFlag;
    private String dropShipAvailableFlag;
    private int hashcode;

    public StateMasterVO () {}
    
    public StateMasterVO (String stateMasterId) {
        this.stateMasterId = stateMasterId;
    }
    
    //overwriting hashCode() of Object
    public int hashCode() {
        if(this.hashcode != 0) {
            return this.hashcode;
        } else {
            hashcode = stateMasterId.hashCode();
        }
        return hashcode;
    }
    //overwriting equals() of Ojbect
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        
        StateMasterVO obj = (StateMasterVO)o;

        if(this.getStateMasterId() == null || obj.getStateMasterId() == null) {
            return false;
        }
        if(!obj.getStateMasterId().equals(this.getStateMasterId())) {
            return false;
        }

        return true;
    }

    public void setStateMasterId(String stateMasterId) {
        this.stateMasterId = stateMasterId;
    }

    public String getStateMasterId() {
        return stateMasterId;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getStateName() {
        return stateName;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setDeliveryExclusionFlag(String deliveryExclusionFlag) {
        this.deliveryExclusionFlag = deliveryExclusionFlag;
    }

    public String getDeliveryExclusionFlag() {
        return deliveryExclusionFlag;
    }

    public void setDropShipAvailableFlag(String dropShipAvailableFlag) {
        this.dropShipAvailableFlag = dropShipAvailableFlag;
    }

    public String getDropShipAvailableFlag() {
        return dropShipAvailableFlag;
    }
    
    /**
     * The XML string will contain all the fields within this VO.
     * This version of the method uses a default fieldAliasMap.
     *
     * @param  None
     * @return XML string
    **/
    public String toXML(int count)
    {  	
    	// Aliases to map to CacheVO from original CacheController
    	Map<String, String> fieldAliasMap = new HashMap<String, String>();
    	fieldAliasMap.put("stateMasterId", "id");
    	fieldAliasMap.put("stateName", "name");
    	fieldAliasMap.put("dropShipAvailableFlag", "dropShipAvailable");
    	
    	return toXML(count, fieldAliasMap);
    }
    
    /**
     * This method uses the Reflection API to generate an XML string that will be
     * passed back to the calling module.
     * The XML string will contain all the fields within this VO.
     * The map can be used to map field names to a different name as expected in the XML.

     * @param count
     * @param fieldAliasMap
     * @return
     */
   public String toXML(int count, Map<String, String> fieldAliasMap)
   {
     StringBuffer sb = new StringBuffer();
     try
     {
       if (count == 0)
       {
         sb.append("<STATE>");
       }
       else
       {
         sb.append("<STATE num=" + '"' + count + '"' + ">");
       }
       Field[] fields = this.getClass().getDeclaredFields();

       for (int i = 0; i < fields.length; i++)
       {
         count++;
         String fieldName = getAlias(fields[i].getName(), fieldAliasMap);
         
         if (fields[i].get(this) == null)
         {
           sb.append("<" + fieldName + "/>");
         }
         else
         {
           sb.append("<" + fieldName + ">");
           sb.append(fields[i].get(this));
           sb.append("</" + fieldName + ">");
         }
       }
       sb.append("</STATE>");
     }

     catch (Exception e)
     {
       e.printStackTrace();
     }

     return sb.toString();
   }
   
   /**
    * Retrieve the aliased name for a field from the specified map.
    * @param name
    * @param aliasMap
    * @return
    */
   private String getAlias(String name, Map<String, String> aliasMap)
   {
	   String retVal = aliasMap.get(name);
	   
	   if(retVal == null)
	   {
		   return name;
	   }
	   
	   return retVal;
   }
}
