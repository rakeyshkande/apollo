package com.ftd.osp.utilities.cacheMgr.handlers;

import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.BillingInfoVO;
import com.ftd.osp.utilities.cacheMgr.vo.CacheHandlerBase;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;

import java.math.BigDecimal;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
/**
 * A cache handler is responsible for the following:
 * 
 * 1. Act as a cache loader, to load the data that needs to be cached.
 * 2. Provide application specific API, to enable access to the cached data.
 * 
 */

public class BillingInfoHandler extends CacheHandlerBase 
{
    // set by the setCachedObject method
    private List billingInfoList;  
    private List  billingInfoSourceList;
    
    private static final String GET_BILLING_INFO = "GET_BILLING_INFO";
    private static final String BILLING_INFO_LIST_KEY = "BILLING_INFO_LIST_KEY";
    private static final String BILLING_INFO_SOURCE_LIST_KEY = "BILLING_INFO_SOURCE_LIST_KEY";
    
    public BillingInfoHandler()
    {
    }

  
   /**
   * Returns the object that needs to be cached.
   * 
   * The cache cannot be configured as distributable, if the cache handler 
   * returns a custom object. In order to utilize the benefits of a distributable 
   * cache, the return objects should only be a part JDK API.
   * 
   * @param con
   * @return the object to be cached
   * @throws com.ftd.osp.utilities.cache.exception.CacheException
   */
    public Object load(Connection con) throws CacheException
    {  
        Map billingInfoHandlerCacheObject = new HashMap();

        List billingInfoListCacheObject = new ArrayList();  
        List  billingInfoSourceListCacheObject = new ArrayList();
        try
        {
            super.logger.debug("BEGIN LOADING BILLING INFO HANDLER...");
            
            DataAccessUtil dau = DataAccessUtil.getInstance();
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(con);
            dataRequest.setStatementID(GET_BILLING_INFO);
            CachedResultSet rs = (CachedResultSet)dau.execute(dataRequest);

            BillingInfoVO billingInfoVO = null;
            
            while(rs.next())
            {
                billingInfoVO = new BillingInfoVO();
                billingInfoVO.setSourceCode((String) rs.getObject(1));
                billingInfoVO.setInfoSequence(((BigDecimal) rs.getObject(2)).toString());
                billingInfoVO.setInfoPrompt((String) rs.getObject(3));
                billingInfoVO.setInfoFormat((String) rs.getObject(4));
                billingInfoVO.setPromptType((String) rs.getObject(5));

                billingInfoListCacheObject.add(billingInfoVO);
                billingInfoSourceListCacheObject.add(billingInfoVO.getSourceCode());
            }
            super.logger.debug("END LOADING BILLING INFO HANDLER...");            
        }
        catch(Exception e)
        {
            super.logger.error(e);
            throw new CacheException("Could not refresh BillingInfoHandler cache");
        }
        
        billingInfoHandlerCacheObject.put(this.BILLING_INFO_LIST_KEY, billingInfoListCacheObject);
        billingInfoHandlerCacheObject.put(this.BILLING_INFO_SOURCE_LIST_KEY, billingInfoSourceListCacheObject);

        logger.debug(billingInfoListCacheObject.size() + " records loaded in billing info list");
        logger.debug(billingInfoSourceListCacheObject.size() + " records loaded in billing info source list");
        return billingInfoHandlerCacheObject;
    }

  /**
   * Set the cached object in the cache handler. The cache handler is then 
   * responsible for fulfilling all application level API calls, to access the
   * data in the cached object.
   * 
   * @param cachedObject
   * @throws com.ftd.osp.utilities.cache.exception.CacheException
   */    
    public void setCachedObject(Object cachedObject) throws CacheException
    {
      try 
      {
        this.billingInfoList = (List) ((Map) cachedObject).get(this.BILLING_INFO_LIST_KEY);  
        this.billingInfoSourceList = (List) ((Map) cachedObject).get(this.BILLING_INFO_SOURCE_LIST_KEY);          
      } catch (Exception ex) 
      {
        super.logger.error(ex);
        throw new CacheException("Could not set the cached object.");
      } 
    }

    
  /**
   * 
   * @param sourceCode
   * @param prompt
   * @return 
   */
    public BillingInfoVO getBillingInfoBySourceAndPrompt(String sourceCode, String prompt)
    {
        BillingInfoVO billingInfoVO = null;

        Iterator it = billingInfoList.iterator();
        while(it.hasNext())
        {
            billingInfoVO = (BillingInfoVO)it.next();

            if(billingInfoVO.getSourceCode() != null && billingInfoVO.getSourceCode().equals(sourceCode)
               && billingInfoVO.getInfoPrompt() != null && billingInfoVO.getInfoPrompt().equalsIgnoreCase(prompt))
            {
                return billingInfoVO;
            }
        }

        return null;    
    }

  /**
   * Returns a list of billing info objects for a particular source code
   * @param sourceCode
   * @return 
   */
    public List getBillingInfoBySource(String sourceCode)
    {
        ArrayList list = new ArrayList();
        BillingInfoVO billingInfoVO = null;
        Iterator it = billingInfoList.iterator();
        while(it.hasNext())
        {
            billingInfoVO = (BillingInfoVO)it.next();

            if( ( billingInfoVO.getSourceCode() != null ) && ( billingInfoVO.getSourceCode().equals(sourceCode)) )
            {
                list.add(billingInfoVO);
            }
        }
        return list;
      
    }

  /**
   * 
   * @param sourceCode
   * @return 
   */
    public boolean hasBillingInfo(String sourceCode)
    {
        return (billingInfoSourceList.contains(sourceCode))?true:false;
    }
}