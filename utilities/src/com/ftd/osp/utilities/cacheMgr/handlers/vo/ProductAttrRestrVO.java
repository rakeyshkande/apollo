package com.ftd.osp.utilities.cacheMgr.handlers.vo;

/**
 * This class models product attribute restriction value object.
 */
public class ProductAttrRestrVO
{
    private String productAttrRestrId;
    private String productAttrRestrOper;
    private String productAttrRestrValue;
    private String javaMethodName;
    private int hashcode;

    public ProductAttrRestrVO() {}
    
    public ProductAttrRestrVO(String productAttrRestrId) {
        this.productAttrRestrId = productAttrRestrId;
    }
    
    public void setProductAttrRestrId(String productAttrRestrId) {
        this.productAttrRestrId = productAttrRestrId;
    }

    public String getProductAttrRestrId() {
        return productAttrRestrId;
    }

    public void setProductAttrRestrOper(String productAttrRestrOper) {
        this.productAttrRestrOper = productAttrRestrOper;
    }

    public String getProductAttrRestrOper() {
        return productAttrRestrOper;
    }

    public void setProductAttrRestrValue(String productAttrRestrValue) {
        this.productAttrRestrValue = productAttrRestrValue;
    }

    public String getProductAttrRestrValue() {
        return productAttrRestrValue;
    }

    public void setJavaMethodName(String javaMethodName) {
        this.javaMethodName = javaMethodName;
    }

    public String getJavaMethodName() {
        return javaMethodName;
    }
    
    //overwriting hashCode() of Object
    public int hashCode() {
        if(this.hashcode != 0) {
            return this.hashcode;
        } else {
            hashcode = productAttrRestrId.hashCode();
        }
        return hashcode;
    }
    //overwirting equals() of Ojbect
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        
        ProductAttrRestrVO obj = (ProductAttrRestrVO)o;

        if(obj.getProductAttrRestrId() == null || !obj.getProductAttrRestrId().equals(this.getProductAttrRestrId()))
            return false;
        return true;
    }
}
