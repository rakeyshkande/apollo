package com.ftd.osp.utilities.cacheMgr.handlers.vo;


import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

/**

 */
public class SourceMasterVO
{

    private String sourceCode;
    private String snhId;
    private String priceHeaderId;
    private String companyId;
    private String discountAllowedFlag;
    private String IOTWFlag;
    private String programType;
    private String calculationBasis;
    private BigDecimal points;
    private String bonusCalculationBasis;
    private BigDecimal bonusPoints;
    private BigDecimal maximumPoints;
    private String rewardType;
    private BigDecimal mpRedemptionRateAmt;
    private String paymentMethodId;
    private String dollorToMPOperator;
    private String roundingMethodId;
    private String roundingScale;
    private String displayServiceFeeFlag;
    private String displayShippingFeeFlag;
    private int hashcode;
    private String orderSource;
    private String relatedSourceCode;
    private String billingInfoLogic;
    private String billingInfoPrompt;
    private String binNumberCheckFlag;
    private String bonusType;
    private String companyName;
    private String defaultSourceCodeFlag;
    private String departmentCode;
    private String description;
    private String emergencyTextFlag;
    private String enableLpProcessing;
    private Date   endDate;
    private String externalCallCenterFlag;
    private String fraudFlag;
    private String highlightDescriptionFlag;
    private String internetOrigin;
    private String jcpenneyFlag;
    private String listCodeFlag;
    private String marketingGroup;
    private String maximumPointsMiles;
    private String mileageBonus;
    private String mileageCalculationSource;
    private String oeDefaultFlag;
    private String partnerId;
    private String pointsMilesPerDollar;
    private String pricingCode;
    private String promotionCode;
    private String requiresDeliveryConfirmation;
    private String sendToScrub;
    private String separateData;
    private String shippingCode;
    private String sourceType;
    private Date   startDate;
    private String validPayMethod;
    private String webloyaltyFlag;
    private String yellowPagesCode;
    private String primaryFlorist; 
    private List backupFloristsList;
    private String sourceCodeHasLimitIndex;
    private String applySurchargeCode;
    private double surchargeAmount;
    private String surchargeDescription;
    private String displaySurcharge;
    private String partnerName;
    private String allowFreeShippingFlag;
    private String sameDayUpcharge; 
    private String displaySameDayUpcharge; 
    
    
    public SourceMasterVO () {}

    public SourceMasterVO (CachedResultSet rs)
    {
        setSourceCode(rs.getString("source_code"));
        setSnhId(rs.getString("snh_id"));
        setPriceHeaderId(rs.getString("price_header_id"));
        setCompanyId(rs.getString("company_id"));
        setDiscountAllowedFlag(rs.getString("discount_allowed_flag"));
        setIOTWFlag(rs.getString("iotw_flag"));
        setProgramType(rs.getString("program_type"));
        setCalculationBasis(rs.getString("calculation_basis"));
        setPoints(rs.getBigDecimal("points"));
        setBonusCalculationBasis(rs.getString("bonus_calculation_basis"));
        setBonusPoints(rs.getBigDecimal("bonus_points"));
        setMaximumPoints(rs.getBigDecimal("maximum_points"));
        setRewardType(rs.getString("reward_type"));
        setOrderSource(rs.getString("order_source"));
        setRelatedSourceCode(rs.getString("related_source_code"));

        if(rs.getObject("mp_redemption_rate_amt") != null)
        {
          setMpRedemptionRateAmt(new BigDecimal(rs.getDouble("mp_redemption_rate_amt")));
        } else
        {
          setMpRedemptionRateAmt(null);
        }
        setPaymentMethodId(rs.getString("payment_method_id"));
        setDollorToMPOperator(rs.getString("dollar_to_mp_operator"));
        setRoundingMethodId(rs.getString("rounding_method_id"));
        setRoundingScale(rs.getString("rounding_scale_qty"));
        setDisplayServiceFeeFlag(rs.getString("client_disp_svc_fee_flag"));
        setDisplayShippingFeeFlag(rs.getString("client_disp_shp_fee_flag"));

        setBillingInfoLogic(rs.getString("billing_info_logic"));
        setBillingInfoPrompt(rs.getString("billing_info_prompt"));
        setBinNumberCheckFlag(rs.getString("bin_number_check_flag"));
        setBonusType(rs.getString("bonus_type"));
        setDefaultSourceCodeFlag(rs.getString("default_source_code_flag"));
        setDepartmentCode(rs.getString("department_code"));
        setDescription(rs.getString("description"));
        setEmergencyTextFlag(rs.getString("emergency_text_flag"));
        setEnableLpProcessing(rs.getString("enable_lp_processing"));
        setEndDate((Date) rs.getObject("end_date"));
        setExternalCallCenterFlag(rs.getString("external_call_center_flag"));
        setFraudFlag(rs.getString("fraud_flag"));
        setHighlightDescriptionFlag(rs.getString("highlight_description_flag"));
        setJcpenneyFlag(rs.getString("jcpenney_flag"));
        setListCodeFlag(rs.getString("list_code_flag"));
        setMarketingGroup(rs.getString("marketing_group"));
        setMaximumPointsMiles(rs.getString("maximum_points_miles"));
        setMileageBonus(rs.getString("mileage_bonus"));
        setMileageCalculationSource(rs.getString("mileage_calculation_source"));
        setOeDefaultFlag(rs.getString("oe_default_flag"));
        setPartnerId(rs.getString("partner_id"));
        setPointsMilesPerDollar(rs.getString("points_miles_per_dollar"));
        setPricingCode(rs.getString("price_header_id"));
        setPromotionCode(rs.getString("promotion_code"));
        setRequiresDeliveryConfirmation(rs.getString("requires_delivery_confirmation"));
        setSendToScrub(rs.getString("send_to_scrub"));
        setSeparateData(rs.getString("separate_data"));
        setShippingCode(rs.getString("shipping_code"));
        setSourceType(rs.getString("source_type"));
        setStartDate((Date) rs.getObject("start_date"));
        setValidPayMethod(rs.getString("valid_pay_method"));
        setWebloyaltyFlag(rs.getString("webloyalty_flag"));
        setYellowPagesCode(rs.getString("yellow_pages_code"));
        setCompanyName(rs.getString("company_name"));
        setInternetOrigin(rs.getString("internet_origin"));
        setPrimaryFlorist(rs.getString("primary_florist"));

        setApplySurchargeCode(rs.getString("apply_surcharge_code"));
        setSurchargeAmount(rs.getDouble("surcharge_amount"));
        setSurchargeDescription(rs.getString("surcharge_description"));
        setPartnerName(rs.getString("partner_name"));
        setDisplaySurcharge(rs.getString("display_surcharge_flag"));

        String backupFlorists = null;
        List backupFloristsList = null;
        if (rs.getString("backup_florists") != null &&  rs.getString("backup_florists") != "")
        {
          backupFlorists = rs.getString("backup_florists");
          backupFloristsList = new ArrayList();

          StringTokenizer st = new StringTokenizer(backupFlorists, " ");
          while (st.hasMoreTokens())
          {
            backupFloristsList.add(st.nextToken());
          }
        }
        setBackupFloristsList(backupFloristsList);
        setSourceCodeHasLimitIndex(rs.getString("source_code_has_limit_index"));
        setAllowFreeShippingFlag(rs.getString("allow_free_shipping_flag"));
        setSameDayUpcharge(rs.getString("same_day_upcharge"));

        setDisplaySameDayUpcharge(rs.getString("display_same_day_upcharge"));

    }
    
    public SourceMasterVO (String sourceCode) {
        this.sourceCode = sourceCode;
    }

    //overwriting hashCode() of Object
    public int hashCode() {
        if(this.hashcode != 0) {
            return this.hashcode;
        } else {
            hashcode = sourceCode.hashCode();
        }
        return hashcode;
    }
    
    //overwirting equals() of Ojbect
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        
        SourceMasterVO obj = (SourceMasterVO)o;

        if(obj.getSourceCode() == null || !obj.getSourceCode().equals(this.getSourceCode()))
            return false;

        return true;
    }

    public void setSourceCode(String sourceCode) {
        this.sourceCode = sourceCode;
    }

    public String getSourceCode() {
        return sourceCode;
    }

    public void setSnhId(String snhId) {
        this.snhId = snhId;
    }

    public String getSnhId() {
        return snhId;
    }

    public void setPriceHeaderId(String priceHeaderId) {
        this.priceHeaderId = priceHeaderId;
    }

    public String getPriceHeaderId() {
        return priceHeaderId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setDiscountAllowedFlag(String discountAllowedFlag) {
        this.discountAllowedFlag = discountAllowedFlag;
    }

    public String getDiscountAllowedFlag() {
        return discountAllowedFlag;
    }

    public void setIOTWFlag(String iOTWFlag) {
        this.IOTWFlag = iOTWFlag;
    }

    public String getIOTWFlag() {
        return IOTWFlag;
    }

    public void setProgramType(String programType) {
        this.programType = programType;
    }

    public String getProgramType() {
        return programType;
    }

    public void setCalculationBasis(String calculationBasis) {
        this.calculationBasis = calculationBasis;
    }

    public String getCalculationBasis() {
        return calculationBasis;
    }

    public void setPoints(BigDecimal points) {
        this.points = points;
    }

    public BigDecimal getPoints() {
        return points;
    }

    public void setBonusCalculationBasis(String bonusCalculationBasis) {
        this.bonusCalculationBasis = bonusCalculationBasis;
    }

    public String getBonusCalculationBasis() {
        return bonusCalculationBasis;
    }

    public void setBonusPoints(BigDecimal bonusPoints) {
        this.bonusPoints = bonusPoints;
    }

    public BigDecimal getBonusPoints() {
        return bonusPoints;
    }

    public void setMaximumPoints(BigDecimal maximumPoints) {
        this.maximumPoints = maximumPoints;
    }

    public BigDecimal getMaximumPoints() {
        return maximumPoints;
    }

    public void setRewardType(String rewardType) {
        this.rewardType = rewardType;
    }

    public String getRewardType() {
        return rewardType;
    }

    public void setMpRedemptionRateAmt(BigDecimal mpRedemptionRateAmt) {
        this.mpRedemptionRateAmt = mpRedemptionRateAmt;
    }

    public BigDecimal getMpRedemptionRateAmt() {
        return mpRedemptionRateAmt;
    }

    public void setPaymentMethodId(String paymentMethodId) {
        this.paymentMethodId = paymentMethodId;
    }

    public String getPaymentMethodId() {
        return paymentMethodId;
    }

    public void setDollorToMPOperator(String dollorToMPOperator) {
        this.dollorToMPOperator = dollorToMPOperator;
    }

    public String getDollorToMPOperator() {
        return dollorToMPOperator;
    }

    public void setRoundingMethodId(String roundingMethodId) {
        this.roundingMethodId = roundingMethodId;
    }

    public String getRoundingMethodId() {
        return roundingMethodId;
    }

    public void setRoundingScale(String roundingScale) {
        this.roundingScale = roundingScale;
    }

    public String getRoundingScale() {
        return roundingScale;
    }

    public void setDisplayServiceFeeFlag(String displayServiceFeeFlag) {
        this.displayServiceFeeFlag = displayServiceFeeFlag;
    }

    public String getDisplayServiceFeeFlag() {
        return displayServiceFeeFlag;
    }

    public void setDisplayShippingFeeFlag(String displayShippingFeeFlag) {
        this.displayShippingFeeFlag = displayShippingFeeFlag;
    }

    public String getDisplayShippingFeeFlag() {
        return displayShippingFeeFlag;
    }

    public void setRelatedSourceCode(String relatedSourceCode) {
        this.relatedSourceCode = relatedSourceCode;
    }

    public String getRelatedSourceCode() {
        return relatedSourceCode;
    }

    public void setOrderSource(String orderSource) {
        this.orderSource = orderSource;
    }

    public String getOrderSource() {
        return orderSource;
    }

    public int getHashcode()
    {
        return hashcode;
    }

    public void setHashcode(int hashcode)
    {
        this.hashcode = hashcode;
    }

    public String getBillingInfoLogic()
    {
        return billingInfoLogic;
    }

    public void setBillingInfoLogic(String billingInfoLogic)
    {
        this.billingInfoLogic = billingInfoLogic;
    }

    public String getBillingInfoPrompt()
    {
        return billingInfoPrompt;
    }

    public void setBillingInfoPrompt(String billingInfoPrompt)
    {
        this.billingInfoPrompt = billingInfoPrompt;
    }

    public String getBinNumberCheckFlag()
    {
        return binNumberCheckFlag;
    }

    public void setBinNumberCheckFlag(String binNumberCheckFlag)
    {
        this.binNumberCheckFlag = binNumberCheckFlag;
    }

    public String getBonusType()
    {
        return bonusType;
    }

    public void setBonusType(String bonusType)
    {
        this.bonusType = bonusType;
    }

    public String getCompanyName()
    {
        return companyName;
    }

    public void setCompanyName(String companyName)
    {
        this.companyName = companyName;
    }

    public String getDefaultSourceCodeFlag()
    {
        return defaultSourceCodeFlag;
    }

    public void setDefaultSourceCodeFlag(String defaultSourceCodeFlag)
    {
        this.defaultSourceCodeFlag = defaultSourceCodeFlag;
    }

    public String getDepartmentCode()
    {
        return departmentCode;
    }

    public void setDepartmentCode(String departmentCode)
    {
        this.departmentCode = departmentCode;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getEmergencyTextFlag()
    {
        return emergencyTextFlag;
    }

    public void setEmergencyTextFlag(String emergencyTextFlag)
    {
        this.emergencyTextFlag = emergencyTextFlag;
    }

    public String getEnableLpProcessing()
    {
        return enableLpProcessing;
    }

    public void setEnableLpProcessing(String enableLpProcessing)
    {
        this.enableLpProcessing = enableLpProcessing;
    }

    public Date getEndDate()
    {
        return endDate;
    }

    public void setEndDate(Date endDate)
    {
        this.endDate = endDate;
    }

    public String getExternalCallCenterFlag()
    {
        return externalCallCenterFlag;
    }

    public void setExternalCallCenterFlag(String externalCallCenterFlag)
    {
        this.externalCallCenterFlag = externalCallCenterFlag;
    }

    public String getFraudFlag()
    {
        return fraudFlag;
    }

    public void setFraudFlag(String fraudFlag)
    {
        this.fraudFlag = fraudFlag;
    }

    public String getHighlightDescriptionFlag()
    {
        return highlightDescriptionFlag;
    }

    public void setHighlightDescriptionFlag(String highlightDescriptionFlag)
    {
        this.highlightDescriptionFlag = highlightDescriptionFlag;
    }

    public String getInternetOrigin()
    {
        return internetOrigin;
    }

    public void setInternetOrigin(String internetOrigin)
    {
        this.internetOrigin = internetOrigin;
    }

    public String getJcpenneyFlag()
    {
        return jcpenneyFlag;
    }

    public void setJcpenneyFlag(String jcpenneyFlag)
    {
        this.jcpenneyFlag = jcpenneyFlag;
    }

    public String getListCodeFlag()
    {
        return listCodeFlag;
    }

    public void setListCodeFlag(String listCodeFlag)
    {
        this.listCodeFlag = listCodeFlag;
    }

    public String getMarketingGroup()
    {
        return marketingGroup;
    }

    public void setMarketingGroup(String marketingGroup)
    {
        this.marketingGroup = marketingGroup;
    }

    public String getMaximumPointsMiles()
    {
        return maximumPointsMiles;
    }

    public void setMaximumPointsMiles(String maximumPointsMiles)
    {
        this.maximumPointsMiles = maximumPointsMiles;
    }

    public String getMileageBonus()
    {
        return mileageBonus;
    }

    public void setMileageBonus(String mileageBonus)
    {
        this.mileageBonus = mileageBonus;
    }

    public String getMileageCalculationSource()
    {
        return mileageCalculationSource;
    }

    public void setMileageCalculationSource(String mileageCalculationSource)
    {
        this.mileageCalculationSource = mileageCalculationSource;
    }

    public String getOeDefaultFlag()
    {
        return oeDefaultFlag;
    }

    public void setOeDefaultFlag(String oeDefaultFlag)
    {
        this.oeDefaultFlag = oeDefaultFlag;
    }

    public String getPartnerId()
    {
        return partnerId;
    }

    public void setPartnerId(String partnerId)
    {
        this.partnerId = partnerId;
    }

    public String getPointsMilesPerDollar()
    {
        return pointsMilesPerDollar;
    }

    public void setPointsMilesPerDollar(String pointsMilesPerDollar)
    {
        this.pointsMilesPerDollar = pointsMilesPerDollar;
    }

    public String getPricingCode()
    {
        return pricingCode;
    }

    public void setPricingCode(String pricingCode)
    {
        this.pricingCode = pricingCode;
    }

    public String getPromotionCode()
    {
        return promotionCode;
    }

    public void setPromotionCode(String promotionCode)
    {
        this.promotionCode = promotionCode;
    }

    public String getRequiresDeliveryConfirmation()
    {
        return requiresDeliveryConfirmation;
    }

    public void setRequiresDeliveryConfirmation(String requiresDeliveryConfirmation)
    {
        this.requiresDeliveryConfirmation = requiresDeliveryConfirmation;
    }

    public String getSendToScrub()
    {
        return sendToScrub;
    }

    public void setSendToScrub(String sendToScrub)
    {
        this.sendToScrub = sendToScrub;
    }

    public String getSeparateData()
    {
        return separateData;
    }

    public void setSeparateData(String separateData)
    {
        this.separateData = separateData;
    }

    public String getShippingCode()
    {
        return shippingCode;
    }

    public void setShippingCode(String shippingCode)
    {
        this.shippingCode = shippingCode;
    }

    public String getSourceType()
    {
        return sourceType;
    }

    public void setSourceType(String sourceType)
    {
        this.sourceType = sourceType;
    }

    public Date getStartDate()
    {
        return startDate;
    }

    public void setStartDate(Date startDate)
    {
        this.startDate = startDate;
    }

    public String getValidPayMethod()
    {
        return validPayMethod;
    }

    public void setValidPayMethod(String validPayMethod)
    {
        this.validPayMethod = validPayMethod;
    }

    public String getWebloyaltyFlag()
    {
        return webloyaltyFlag;
    }

    public void setWebloyaltyFlag(String webloyaltyFlag)
    {
        this.webloyaltyFlag = webloyaltyFlag;
    }

    public String getYellowPagesCode()
    {
        return yellowPagesCode;
    }

    public void setYellowPagesCode(String yellowPagesCode)
    {
        this.yellowPagesCode = yellowPagesCode;
    }

    public String getPrimaryFlorist()
    {
        return primaryFlorist;
    }

    public void setPrimaryFlorist(String primaryFlorist)
    {
        this.primaryFlorist = primaryFlorist;
    }

    public List getBackupFloristsList()
    {
        return backupFloristsList;
    }

    public void setBackupFloristsList(List backupFloristsList)
    {
        this.backupFloristsList = backupFloristsList;
    }

    public String getSourceCodeHasLimitIndex()
    {
        return sourceCodeHasLimitIndex;
    }

    public void setSourceCodeHasLimitIndex(String sourceCodeHasLimitIndex)
    {
        this.sourceCodeHasLimitIndex = sourceCodeHasLimitIndex;
    }

    public String getApplySurchargeCode()
    {
        return applySurchargeCode;
    }

    public void setApplySurchargeCode(String applySurchargeCode)
    {
        this.applySurchargeCode = applySurchargeCode;
    }

    public double getSurchargeAmount()
    {
        return surchargeAmount;
    }

    public void setSurchargeAmount(double surchargeAmount)
    {
        this.surchargeAmount = surchargeAmount;
    }

    public String getSurchargeDescription()
    {
        return surchargeDescription;
    }

    public void setSurchargeDescription(String surchargeDescription)
    {
        this.surchargeDescription = surchargeDescription;
    }

    public String getDisplaySurcharge()
    {
        return displaySurcharge;
    }

    public void setDisplaySurcharge(String displaySurcharge)
    {
        this.displaySurcharge = displaySurcharge;
    }

    public String getPartnerName()
    {
        return partnerName;
    }

    public void setPartnerName(String partnerName)
    {
        this.partnerName = partnerName;
    }

    public String getAllowFreeShippingFlag()
    {
        return allowFreeShippingFlag;
    }

    public void setAllowFreeShippingFlag(String allowFreeShippingFlag)
    {
        this.allowFreeShippingFlag = allowFreeShippingFlag;
    }

	public void setSameDayUpcharge(String sameDayUpcharge) {
		this.sameDayUpcharge = sameDayUpcharge;
	}

	public String getSameDayUpcharge() {
		return sameDayUpcharge;
	}

	public void setDisplaySameDayUpcharge(String displaySameDayUpcharge) {
		this.displaySameDayUpcharge = displaySameDayUpcharge;
	}

	public String getDisplaySameDayUpcharge() {
		return displaySameDayUpcharge;
	}
}
