package com.ftd.osp.utilities.cacheMgr.handlers.enums;

import org.apache.commons.lang.StringUtils;


public enum ProductLoadLevelEnum {
    DISPLAY, DETAILS;

    public static ProductLoadLevelEnum toProductLoadLevel(String loadLevel) {
        if( StringUtils.equals("display", loadLevel)) {
            return DISPLAY;
        }
        else if( StringUtils.equals("details", loadLevel)) {
            return DETAILS;
        }

        return null;
    }

    public static String toProductLoadLevelString(ProductLoadLevelEnum loadLevel) {
        if( loadLevel.equals(DISPLAY)) {
            return "display";
        }
        else if( loadLevel.equals(DETAILS)) {
            return "details";
        }

        return null;
    }
    
    public static boolean isValidLoadLevel(String loadLevel) {
        if(toProductLoadLevel(loadLevel) != null) {
            return true;
        }
        return false;
    }

}