package com.ftd.osp.utilities.cacheMgr.handlers;

import com.ftd.osp.utilities.cacheMgr.constant.CacheMgrConstants;
import com.ftd.osp.utilities.cacheMgr.dao.CacheMgrDAO;
import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.ShipMethodVO;
import com.ftd.osp.utilities.cacheMgr.vo.CacheHandlerBase;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;

import java.math.BigDecimal;
import java.sql.Connection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This class is responsible for loading Ship Methods into cache. key: ALL value: List<ShipMethodVO>
 */
public class ShipMethodHandler extends CacheHandlerBase
{
    private List<ShipMethodVO> shipMethodList;
    private Map<String, ShipMethodVO> shipMethodMapById;

    /*
     * @see com.ftd.osp.utilities.cacheMgr.vo.CacheHandlerBase#load(java.sql.Connection)
     */
    public Object load(Connection conn) throws CacheException
    {
        logger = new Logger(this.getClass().getName());
        CacheMgrDAO osdao = new CacheMgrDAO();
        CachedResultSet rs = osdao.executeQuery(conn, CacheMgrConstants.STMT_GET_SHIP_METHODS);
        List<ShipMethodVO> shipMethodList = new ArrayList<ShipMethodVO>();
        Map<String, ShipMethodVO> shipMethodMapById = new HashMap<String, ShipMethodVO>();
        Map<String, Object> handlerMap = new HashMap<String, Object>();
        ShipMethodVO shipMethodVO = null;

        while (rs != null && rs.next())
        {
            shipMethodVO = new ShipMethodVO();
            shipMethodVO.setShipMethodId(((String) rs.getObject(1)).toUpperCase());
            shipMethodVO.setDescription(((String) rs.getObject(2)));
            shipMethodVO.setNovatorTag(((String) rs.getObject(3)));
            shipMethodVO.setMaxTransitDays(((BigDecimal) rs.getObject(4)).longValue());
            shipMethodVO.setSdsShipVia(((String) rs.getObject(5)));
            shipMethodVO.setSdsShipViaAir(((String) rs.getObject(6)));
            shipMethodList.add(shipMethodVO);
            shipMethodMapById.put(shipMethodVO.getShipMethodId(), shipMethodVO);
        }
        handlerMap.put(CacheMgrConstants.CACHE_KEY_ALL, shipMethodList);
        handlerMap.put(CacheMgrConstants.CACHE_KEY_ID_GEN, shipMethodMapById);
        return handlerMap;
    }

    /**
     * Set the cached object in the cache handler. The cache handler is then responsible for fulfilling all application
     * level API calls, to access the data in the cached object.
     * 
     * @param cachedObject
     * @throws com.ftd.osp.utilities.cache.exception.CacheException
     */
    public void setCachedObject(Object cachedObject) throws CacheException
    {
        try
        {
            Map<String, Object> cachedObjectMap = (Map<String, Object>) cachedObject;
            this.shipMethodList = (List<ShipMethodVO>) cachedObjectMap.get(CacheMgrConstants.CACHE_KEY_ALL);
            this.shipMethodMapById = (Map) cachedObjectMap.get(CacheMgrConstants.CACHE_KEY_ID_GEN);
        }
        catch (Exception ex)
        {
            super.logger.error(ex);
            throw new CacheException("Could not set the cached object.");
        }
    }

    /**
     * Returns an unmodifiable list of the ship methods.
     * @return
     * @throws CacheException
     */
    public List<ShipMethodVO> getShipMethodList() throws CacheException
    {
        if (shipMethodList == null)
        {
            throw new CacheException("Cached object is null.");
        }
        
        return Collections.unmodifiableList(shipMethodList);
    }

    /**
     * Returns a ship method by Id, or <code>null</code> 
     * @param countryId
     * @return
     */
    public ShipMethodVO getShipMethodById(String shipMethodId) throws CacheException
    {
        if (shipMethodMapById == null)
        {
            throw new CacheException("Cached object is null.");
        }

        ShipMethodVO retVal = this.shipMethodMapById.get(shipMethodId);
        return retVal;
    }
}
