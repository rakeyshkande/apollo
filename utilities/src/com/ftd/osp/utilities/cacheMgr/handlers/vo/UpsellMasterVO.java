package com.ftd.osp.utilities.cacheMgr.handlers.vo;

import java.util.List;


public class UpsellMasterVO
{
    private String upsellMasterId;
    private String upsellName;
    private String upsellStatus;
    private String gbbPopoverFlag;
    private String gbbTitleTxt;
    private List<UpsellDetailVO> upsellDetailList;
    private int hashcode;

    public UpsellMasterVO () {}
    
    public UpsellMasterVO (String upsellMasterId) {
        this.upsellMasterId = upsellMasterId;
    }
    public void setUpsellMasterId(String upsellMasterId) {
        this.upsellMasterId = upsellMasterId;
    }

    public String getUpsellMasterId() {
        return upsellMasterId;
    }

    public void setUpsellStatus(String upsellStatus) {
        this.upsellStatus = upsellStatus;
    }

    public String getUpsellStatus() {
        return upsellStatus;
    }

    public void setGbbPopoverFlag(String gbbPopoverFlag) {
        this.gbbPopoverFlag = gbbPopoverFlag;
    }

    public String getGbbPopoverFlag() {
        return gbbPopoverFlag;
    }

    public void setGbbTitleTxt(String gbbTitleTxt) {
        this.gbbTitleTxt = gbbTitleTxt;
    }

    public String getGbbTitleTxt() {
        return gbbTitleTxt;
    }

    public void setUpsellDetailList(List<UpsellDetailVO> upsellDetailList) {
        this.upsellDetailList = upsellDetailList;
    }

    public List<UpsellDetailVO> getUpsellDetailList() {
        return upsellDetailList;
    }

    public String getDefaultUpsellDetailId() {
        if(upsellDetailList != null) {
            for (int i = 0; i < upsellDetailList.size(); i++){
                UpsellDetailVO upsellDetail = upsellDetailList.get(i);
                if("Y".equals(upsellDetail.getDefaultSkuFlag())) {
                    return upsellDetail.getUpsellDetailId();
                }
            }
        }
        return null;
    }

    public void setUpsellName(String upsellName) {
        this.upsellName = upsellName;
    }

    public String getUpsellName() {
        return upsellName;
    }
    
    public int hashCode() {
        if(this.hashcode != 0) {
            return this.hashcode;
        } else {
            hashcode = upsellMasterId.hashCode();
        }
        return hashcode;
    }
    //overwirting equals() of Ojbect
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        
        UpsellMasterVO obj = (UpsellMasterVO)o;

        if(obj.getUpsellMasterId() == null || !obj.getUpsellMasterId().equals(this.getUpsellMasterId()))
            return false;

        return true;
    }
}
