package com.ftd.osp.utilities.cacheMgr.handlers.vo;

import java.util.Comparator;

/**
 * Compares conutries by the Display Order
 */
public class CountryVoComparator implements Comparator<CountryMasterVO>
{
    public CountryVoComparator()
    {
    }

    public int compare(CountryMasterVO obj1, CountryMasterVO obj2)
    {
        int ret = 0;
        int int1 = ((CountryMasterVO)obj1).getDisplayOrder().intValue();
        int int2 = ((CountryMasterVO)obj2).getDisplayOrder().intValue();

        if(int1 < int2)
        {
            ret = -1;
        }
        else if(int1 > int2)
        {
            ret = 1;
        }

        return ret;
    }

    public boolean equals(Object obj1)
    {
        return false;
    }
}