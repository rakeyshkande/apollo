package com.ftd.osp.utilities.cacheMgr.handlers.vo;

import java.math.BigDecimal;


public class UpsellDetailVO
{
    private String upsellMasterId;
    private String upsellDetailId;
    private String upsellDetailName;
    private String novatorId;
    private String novatorName;
    private String gbbUpsellDetailSequenceNum;
    private String defaultSkuFlag;
    private String gbbNameOverrideFlag;
    private String gbbNameOverrideTxt;
    private String gbbPriceOverrideFlag;
    private String gbbPriceOverrideTxt;
    private BigDecimal priceAmt;
    private String upsellDetailSequenceNum;

    public void setUpsellMasterId(String upsellMasterId) {
        this.upsellMasterId = upsellMasterId;
    }

    public String getUpsellMasterId() {
        return upsellMasterId;
    }

    public void setUpsellDetailId(String upsellDetailId) {
        this.upsellDetailId = upsellDetailId;
    }

    public String getUpsellDetailId() {
        return upsellDetailId;
    }

    public void setGbbUpsellDetailSequenceNum(String gbbUpsellDetailSequenceNum) {
        this.gbbUpsellDetailSequenceNum = gbbUpsellDetailSequenceNum;
    }

    public String getGbbUpsellDetailSequenceNum() {
        return gbbUpsellDetailSequenceNum;
    }

    public void setDefaultSkuFlag(String defaultSkuFlag) {
        this.defaultSkuFlag = defaultSkuFlag;
    }

    public String getDefaultSkuFlag() {
        return defaultSkuFlag;
    }

    public void setGbbNameOverrideFlag(String gbbNameOverrideFlag) {
        this.gbbNameOverrideFlag = gbbNameOverrideFlag;
    }

    public String getGbbNameOverrideFlag() {
        return gbbNameOverrideFlag;
    }

    public void setGbbNameOverrideTxt(String gbbNameOverrideTxt) {
        this.gbbNameOverrideTxt = gbbNameOverrideTxt;
    }

    public String getGbbNameOverrideTxt() {
        return gbbNameOverrideTxt;
    }

    public void setGbbPriceOverrideFlag(String gbbPriceOverrideFlag) {
        this.gbbPriceOverrideFlag = gbbPriceOverrideFlag;
    }

    public String getGbbPriceOverrideFlag() {
        return gbbPriceOverrideFlag;
    }

    public void setGbbPriceOverrideTxt(String gbbPriceOverrideTxt) {
        this.gbbPriceOverrideTxt = gbbPriceOverrideTxt;
    }

    public String getGbbPriceOverrideTxt() {
        return gbbPriceOverrideTxt;
    }

    public void setNovatorId(String novatorId) {
        this.novatorId = novatorId;
    }

    public String getNovatorId() {
        return novatorId;
    }

    public void setPriceAmt(BigDecimal priceAmt) {
        this.priceAmt = priceAmt;
    }

    public BigDecimal getPriceAmt() {
        return priceAmt;
    }

    public void setNovatorName(String novatorName) {
        this.novatorName = novatorName;
    }

    public String getNovatorName() {
        return novatorName;
    }

    public void setUpsellDetailName(String upsellDetailName) {
        this.upsellDetailName = upsellDetailName;
    }

    public String getUpsellDetailName() {
        return upsellDetailName;
    }

    public void setUpsellDetailSequenceNum(String upsellDetailSequenceNum) {
        this.upsellDetailSequenceNum = upsellDetailSequenceNum;
    }

    public String getUpsellDetailSequenceNum() {
        return upsellDetailSequenceNum;
    }
}
