package com.ftd.osp.utilities.cacheMgr.handlers.vo;


public class CompanyMasterVO
{
    private String companyId;
    private String companyName;
    private String defaultDomain;
    private int hashcode;

    public CompanyMasterVO() {}
    
    public CompanyMasterVO(String companyId) {
        this.companyId = companyId;
    }
    
    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setDefaultDomain(String defaultDomain) {
        this.defaultDomain = defaultDomain;
    }

    public String getDefaultDomain() {
        return defaultDomain;
    }
    
    //overwriting hashCode() of Object
    public int hashCode() {
        if(this.hashcode != 0) {
            return this.hashcode;
        } else {
            hashcode = companyId.hashCode();
        }
        return hashcode;
    }
    //overwirting equals() of Ojbect
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        
        CompanyMasterVO obj = (CompanyMasterVO)o;

        if(obj.getCompanyId() == null || !obj.getCompanyId().equals(this.getCompanyId()))
            return false;
        return true;
    }
}
