package com.ftd.osp.utilities.cacheMgr.handlers.vo;

import java.math.BigDecimal;

import java.util.Date;


public class AvailableDateVO
{
    private Date deliveryDate;
    private Date endDate;
    private String shipMethod;
    private String description;
    private String message;
    private BigDecimal serviceFeeAmt;
    private BigDecimal shippingFeeAmt;

    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public void setShipMethod(String shipMethod) {
        this.shipMethod = shipMethod;
    }

    public String getShipMethod() {
        return shipMethod;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setServiceFeeAmt(BigDecimal serviceFeeAmt) {
        this.serviceFeeAmt = serviceFeeAmt;
    }

    public BigDecimal getServiceFeeAmt() {
        return serviceFeeAmt;
    }

    public void setShippingFeeAmt(BigDecimal shippingFeeAmt) {
        this.shippingFeeAmt = shippingFeeAmt;
    }

    public BigDecimal getShippingFeeAmt() {
        return shippingFeeAmt;
    }
}
