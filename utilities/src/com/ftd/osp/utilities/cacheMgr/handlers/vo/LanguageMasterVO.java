package com.ftd.osp.utilities.cacheMgr.handlers.vo;

/**
 * @author skatam
 * LanguageMasterVO is to hold language information from FTD_MASTER.LANGUAGE_MASTER
 */
public class LanguageMasterVO {
	
	private String languageId;
	private String description;
	private int sortOrder;
	
	/**
	 * Default Constructor
	 */
	public LanguageMasterVO() {
	}

	/**
	 * @param languageId
	 * @param description
	 * @param sortOrder
	 */
	public LanguageMasterVO(String languageId, String description, int sortOrder) {
		this.languageId = languageId;
		this.description = description;
		this.sortOrder = sortOrder;
	}

	/**
	 * @return the languageId
	 */
	public String getLanguageId() {
		return languageId;
	}

	/**
	 * @param languageId the languageId to set
	 */
	public void setLanguageId(String languageId) {
		this.languageId = languageId;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the sortOrder
	 */
	public int getSortOrder() {
		return sortOrder;
	}

	/**
	 * @param sortOrder the sortOrder to set
	 */
	public void setSortOrder(int sortOrder) {
		this.sortOrder = sortOrder;
	}
	
}
