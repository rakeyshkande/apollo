package com.ftd.osp.utilities.cacheMgr.handlers;

import com.ftd.osp.utilities.cacheMgr.constant.CacheMgrConstants;
import com.ftd.osp.utilities.cacheMgr.dao.CacheMgrDAO;
import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.ColorMasterVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.CompanyMasterVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.ProductAttrRestrVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.SourceMasterVO;
import com.ftd.osp.utilities.cacheMgr.vo.CacheHandlerBase;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * This class is responsible for loading zip code into cache.
 * Zip code is NOT the primary key in ftd_apps.zip_code table. The caller of this cache only cares
 * if the zip code exists. Hence this cache only stores one entry per zip code.
 * 
 * key: zipCode
 * value: String
 */
public class ZipCodeHandler extends CacheHandlerBase
{
    private Map objMap;

    public Object load(Connection conn) throws CacheException {
        logger = new Logger(this.getClass().getName());
        CacheMgrDAO osdao = new CacheMgrDAO();
        CachedResultSet rs = osdao.executeQuery(conn, CacheMgrConstants.STMT_GET_ZIP_CODE);
        String zipCode = null;
        String stateId = null;
        Map handlerMap = new HashMap();
        Map zipMap = new HashMap();

        if(logger.isInfoEnabled())
          logger.info("Loaded zip code...");
        
        while (rs != null && rs.next()) {
            zipCode = rs.getString("zip_code_id");
            stateId = rs.getString("state_id");
            
            zipMap.put(zipCode, stateId);
        }

        handlerMap.put(CacheMgrConstants.CACHE_KEY_ID_GEN, zipMap);
        return handlerMap;
    }
    
    /**
     * Set the cached object in the cache handler. The cache handler is then 
     * responsible for fulfilling all application level API calls, to access the
     * data in the cached object.
     * 
     * @param cachedObject
     * @throws com.ftd.osp.utilities.cache.exception.CacheException
     */    
      public void setCachedObject(Object cachedObject) throws CacheException
      {
        try 
        {
            this.objMap = (Map)((Map) cachedObject).get(CacheMgrConstants.CACHE_KEY_ID_GEN);
        } catch (Exception ex) 
        {
          super.logger.error(ex);
          throw new CacheException("Could not set the cached object.");
        } 
      }
      
      public boolean zipCodeExists(String zipCode) throws CacheException
      {
          String key = zipCode;
          
          Object cachedObj = this.objMap;
          if (cachedObj == null) {
                throw new CacheException("Cached object is null.");
          }
          Object obj = this.objMap.get(key);

          if(obj != null) {
                return true;   
          } 
          return false;
      }
      
      public String getZipCodeState(String zipCode) throws Exception {
          String stateCode = null;
          String key = zipCode;
          
          Object cachedObj = this.objMap;
          if (cachedObj == null) {
                throw new CacheException("Cached object is null.");
          }
          Object obj = this.objMap.get(key);
          
          if (obj != null) {
              stateCode = (String) obj;
          } 
          
          return stateCode;
      }
}
