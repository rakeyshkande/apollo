package com.ftd.osp.utilities.cacheMgr.handlers;

import com.ftd.osp.utilities.cacheMgr.constant.CacheMgrConstants;
import com.ftd.osp.utilities.cacheMgr.dao.CacheMgrDAO;
import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.CompanyMasterVO;
import com.ftd.osp.utilities.cacheMgr.vo.CacheHandlerBase;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * This class is responsible for loading product company into cache.
 * key: productId
 * value: CompanyMasterVO
 */
public class ProductCompanyHandler extends CacheHandlerBase
{
    private Map objMap;
    public Object load(Connection conn) throws CacheException {
        logger = new Logger(this.getClass().getName());
        CacheMgrDAO osdao = new CacheMgrDAO();
        CachedResultSet rs = osdao.executeQuery(conn, CacheMgrConstants.STMT_GET_PRODUCT_COMPANY);

        Map handlerMap = new HashMap();
        Map productCompanyMap = new HashMap();
        String productId = null;
        String prevProductId = null;
        List<CompanyMasterVO> companyList = null;
        CompanyMasterVO cmvo = null;
        
        if(logger.isInfoEnabled())
          logger.info("Loaded product company...");
        while (rs != null && rs.next()) {   
            productId = rs.getString("product_id");
            cmvo = new CompanyMasterVO();
            cmvo.setCompanyId("company_id");
            cmvo.setCompanyName(rs.getString("company_name"));

            if(!productId.equals(prevProductId)) {
                if(prevProductId != null) {
                    productCompanyMap.put(prevProductId, companyList);
                }
                companyList = new ArrayList();
            }
            companyList.add(cmvo);

            prevProductId = productId;
        }
        
        if(prevProductId != null) {
            productCompanyMap.put(prevProductId, companyList);
        }
        
        handlerMap.put(CacheMgrConstants.CACHE_KEY_ID_GEN, productCompanyMap);
        return handlerMap;
    }
    
    /**
     * Set the cached object in the cache handler. The cache handler is then 
     * responsible for fulfilling all application level API calls, to access the
     * data in the cached object.
     * 
     * @param cachedObject
     * @throws com.ftd.osp.utilities.cache.exception.CacheException
     */    
      public void setCachedObject(Object cachedObject) throws CacheException
      {
        try 
        {
            this.objMap = (Map)((Map) cachedObject).get(CacheMgrConstants.CACHE_KEY_ID_GEN);  
        } catch (Exception ex) 
        {
          super.logger.error(ex);
          throw new CacheException("Could not set the cached object.");
        } 
      }
      
      public List<CompanyMasterVO> getProductCompany(String productId) throws CacheException
      {
          String key = productId;
          List<CompanyMasterVO> value = null;
          
          Object cachedObj = this.objMap;
          if (cachedObj == null) {
                throw new CacheException("Cached object is null.");
          }
          Object obj = this.objMap.get(key);

          if(obj != null) {
                value = Collections.unmodifiableList((List<CompanyMasterVO>)obj);     
          } 
          return value;
      }
      

}
