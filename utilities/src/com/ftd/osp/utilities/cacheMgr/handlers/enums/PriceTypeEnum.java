package com.ftd.osp.utilities.cacheMgr.handlers.enums;

import org.apache.commons.lang.StringUtils;


public enum PriceTypeEnum {
    BASE, SALE;

    public static PriceTypeEnum toPriceType(String priceType) {
        if( StringUtils.equals("base", priceType) ) {
            return BASE;
        }
        else if( StringUtils.equals("sale", priceType)) {
            return SALE;
        }


        return null;
    }

    public static String toPriceTypeString(PriceTypeEnum pt) {
        if(pt.equals(BASE)) {
            return "base";
        }
        else if(pt.equals(SALE)) {
            return "sale";
        }
        return null;
    }
}
