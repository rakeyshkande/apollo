package com.ftd.osp.utilities.cacheMgr.handlers.vo;

public class PaymentMethodMilesPointsVO {
	
	private String paymentMethodId;
	private String dollarToMpOperator;
	private String roundingMethodId;
	private String roundingScaleQty;
	private String commissionPct;
	
    public PaymentMethodMilesPointsVO () {}
    
    public PaymentMethodMilesPointsVO (String paymentMethodId) {
        this.paymentMethodId = paymentMethodId;
    }
	
	public String getPaymentMethodId() {
		return paymentMethodId;
	}
	public void setPaymentMethodId(String paymentMethodId) {
		this.paymentMethodId = paymentMethodId;
	}
	public String getDollarToMpOperator() {
		return dollarToMpOperator;
	}
	public void setDollarToMpOperator(String dollarToMpOperator) {
		this.dollarToMpOperator = dollarToMpOperator;
	}
	public String getRoundingMethodId() {
		return roundingMethodId;
	}
	public void setRoundingMethodId(String roundingMethodId) {
		this.roundingMethodId = roundingMethodId;
	}
	public String getRoundingScaleQty() {
		return roundingScaleQty;
	}
	public void setRoundingScaleQty(String roundingScaleQty) {
		this.roundingScaleQty = roundingScaleQty;
	}
	public String getCommissionPct() {
		return commissionPct;
	}
	public void setCommissionPct(String commissionPct) {
		this.commissionPct = commissionPct;
	}
	
	
	

}
