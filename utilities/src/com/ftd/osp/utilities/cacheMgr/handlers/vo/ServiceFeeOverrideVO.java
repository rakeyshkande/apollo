package com.ftd.osp.utilities.cacheMgr.handlers.vo;

import java.math.BigDecimal;

import java.util.Date;


public class ServiceFeeOverrideVO
{
    private String snhId;
    private Date overrideDate;
    private BigDecimal domesticCharge;
    private BigDecimal internationalCharge;
    private BigDecimal vendorCharge;
    private BigDecimal vendorSatUpcharge;
    private int hashcode;
    private BigDecimal sameDayUpcharge;

    public ServiceFeeOverrideVO() {}

    public ServiceFeeOverrideVO(String snhId, Date overrideDate) {
        this.snhId = snhId;
        this.overrideDate = overrideDate;
    }

    public void setSnhId(String snhId) {
        this.snhId = snhId;
    }

    public String getSnhId() {
        return snhId;
    }

    public void setOverrideDate(Date overrideDate) {
        this.overrideDate = overrideDate;
    }

    public Date getOverrideDate() {
        return overrideDate;
    }

    public void setDomesticCharge(BigDecimal domesticCharge) {
        this.domesticCharge = domesticCharge;
    }

    public BigDecimal getDomesticCharge() {
        return domesticCharge;
    }

    public void setInternationalCharge(BigDecimal internationalCharge) {
        this.internationalCharge = internationalCharge;
    }

    public BigDecimal getInternationalCharge() {
        return internationalCharge;
    }

    public void setVendorCharge(BigDecimal vendorCharge) {
        this.vendorCharge = vendorCharge;
    }

    public BigDecimal getVendorCharge() {
        return vendorCharge;
    }

    public void setVendorSatUpcharge(BigDecimal vendorSatUpcharge) {
        this.vendorSatUpcharge = vendorSatUpcharge;
    }

    public BigDecimal getVendorSatUpcharge() {
        return vendorSatUpcharge;
    }

    //overwriting hashCode() of Object
    public int hashCode() {
        if(this.hashcode != 0) {
            return this.hashcode;
        } else {
            //hashcode = snhId.hashCode() + overrideDate.hashCode();
            StringBuilder hashCodeBuilder = new StringBuilder(snhId).append("|").append(overrideDate);
            hashcode = hashCodeBuilder.toString().hashCode();

        }
        return hashcode;
    }
    //overwirting equals() of Ojbect
    public boolean equals(Object o) {

        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ServiceFeeOverrideVO obj = (ServiceFeeOverrideVO)o;

        if(obj.getSnhId() == null || obj.getOverrideDate() == null  || !(obj.getSnhId() + obj.getOverrideDate()).equals(this.getSnhId() + this.getOverrideDate()))
            return false;

        return true;
    }

	public BigDecimal getSameDayUpcharge() {
		return sameDayUpcharge;
	}

	public void setSameDayUpcharge(BigDecimal sameDayUpcharge) {
		this.sameDayUpcharge = sameDayUpcharge;
	}


}
