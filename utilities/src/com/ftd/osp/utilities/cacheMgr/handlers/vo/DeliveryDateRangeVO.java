package com.ftd.osp.utilities.cacheMgr.handlers.vo;

import java.util.Date;

/**

 */
public class DeliveryDateRangeVO
{
    private Date startDate;
    private Date endDate;
    private String instructionText;

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setInstructionText(String instructionText) {
        this.instructionText = instructionText;
    }

    public String getInstructionText() {
        return instructionText;
    }
}
