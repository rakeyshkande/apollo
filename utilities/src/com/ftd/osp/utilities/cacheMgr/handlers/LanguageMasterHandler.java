package com.ftd.osp.utilities.cacheMgr.handlers;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ftd.osp.utilities.cacheMgr.constant.CacheMgrConstants;
import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.LanguageMasterVO;
import com.ftd.osp.utilities.cacheMgr.vo.CacheHandlerBase;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;

/**
 * @author skatam 
 * LanguageMasterHandler is to hold the languages from FTD_MASTER.LANGUAGE_MASTER table.
 */
public class LanguageMasterHandler extends CacheHandlerBase 
{
	private static final String LANGUAGE_MASTER_LIST_KEY = "LANGUAGE_MASTER_LIST_KEY";
	private static final String LANGUAGE_MASTER_VO_MAP_KEY = "LANGUAGE_MASTER_VO_MAP_KEY";
	
	private List<String> languageMasterList;
	private Map<String, LanguageMasterVO> languageMasterVOMap;

	/**
	 * Default Constructor
	 */
	public LanguageMasterHandler() {
	}

	/**
	 * @return List of Language Ids
	 */
	public List<String> getLanguageMasterList() {
		return languageMasterList;
	}

	/**
	 * @return Map of Language Id and LanguageMasterVOs.
	 */
	public Map<String, LanguageMasterVO> getLanguageMasterVOMap() {
		return languageMasterVOMap;
	}

	/**
	 * @param languageId
	 * @return LanguageMasterVO for the given languageId.
	 */
	public LanguageMasterVO getLanguageMasterVOById(String languageId) {
		if (this.languageMasterVOMap.containsKey(languageId)) {
			return this.languageMasterVOMap.get(languageId);
		}
		return null;
	}

	/**
	 * @param languageId
	 * @return true if the given languageId is valid, false otherwise. 
	 */
	public boolean isValidLanguageId(String languageId) {
		if(languageId == null){
			return false;
		}
		languageId = languageId.trim();
		for (String langId : languageMasterList) {
			if(languageId.equalsIgnoreCase(langId)){
				return true;
			}
		}
		return false;
	}

	/**
	 * This method will load the Language details from FTD_MASTER.LANGUAGE_MASTER table.
	 * @param Connection
	 * @return true if the given languageId is valid, false otherwise. 
	 */
	@Override
	public Object load(Connection con) throws CacheException {
		Map<String, Object> map = new HashMap<String, Object>();

		List<String> languageMasterList = new ArrayList<String>();
		Map<String, LanguageMasterVO> languageMasterVOMap = new HashMap<String, LanguageMasterVO>();

		try {
			super.logger.debug("BEGIN LOADING LANGUAGE MASTER HANDLER CACHE...");

			DataAccessUtil dau = DataAccessUtil.getInstance();
			DataRequest dataRequest = new DataRequest();
			dataRequest.setStatementID(CacheMgrConstants.STMT_GET_LANGUAGE_MASTER);
			dataRequest.setConnection(con);
			CachedResultSet rs = (CachedResultSet) dau.execute(dataRequest);
			rs.reset();

			while (rs.next()) {
				LanguageMasterVO languageMasterVO = new LanguageMasterVO();
				languageMasterVO.setLanguageId(rs.getString("LANGUAGE_ID"));
				languageMasterVO.setDescription(rs.getString("DESCRIPTION"));
				languageMasterVO.setSortOrder(rs.getInt("DISPLAY_ORDER"));

				languageMasterList.add(languageMasterVO.getLanguageId());
				languageMasterVOMap.put(languageMasterVO.getLanguageId(),languageMasterVO);
			}
			map.put(LANGUAGE_MASTER_LIST_KEY, languageMasterList);
			map.put(LANGUAGE_MASTER_VO_MAP_KEY, languageMasterVOMap);

			super.logger.debug("END LOADING LANGUAGE MASTER HANDLER CACHE...");
		} catch (Exception e) {
			super.logger.error(e);
			throw new CacheException("Could not load LanguageMasterHandler cache");
		}

		return map;
	}

	/**
	 * This method will set the state of the Cache.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void setCachedObject(Object cachedObject) throws CacheException {
		try {
			Map<String, Object> map = (Map<String, Object>) cachedObject;
			this.languageMasterList = (List<String>) map.get(LANGUAGE_MASTER_LIST_KEY);
			this.languageMasterVOMap = 
				(Map<String, LanguageMasterVO>) map.get(LANGUAGE_MASTER_VO_MAP_KEY);
		} catch (Exception ex) {
			super.logger.error(ex);
			throw new CacheException("Could not set the LanguageMasterHandler cached object.");
		}
	}
}
