package com.ftd.osp.utilities.cacheMgr.handlers.vo;

import java.math.BigDecimal;

public class ShippingKeyCostVO
{
    private String shippingDetailId;
    private String shippingMethodId;
    private BigDecimal shippingCost;
    private int hashcode;
    
    public ShippingKeyCostVO() {
    }

    public ShippingKeyCostVO(String shippingDetailId, String shippingMethodId) {
        this.shippingDetailId = shippingDetailId;
        this.shippingMethodId = shippingMethodId;
    }

    //overwriting hashCode() of Object
    public int hashCode() {
        if(this.hashcode != 0) {
            return this.hashcode;
        } else {
            // hashcode = shippingDetailId.hashCode() + shippingMethodId.hashCode();
            StringBuilder hashCodeBuilder = new StringBuilder(shippingDetailId).append("|").append(shippingMethodId);
            hashcode = hashCodeBuilder.toString().hashCode();
        }
        return hashcode;
    }
    //overwriting equals() of Ojbect
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        
        ShippingKeyCostVO obj = (ShippingKeyCostVO)o;

        if(obj.getShippingDetailId() == null || !obj.getShippingDetailId().equals(this.getShippingDetailId()))
            return false;
        if(obj.getShippingMethodId() == null || !obj.getShippingMethodId().equals(this.getShippingMethodId()))
            return false;
        return true;
    }


    public void setShippingDetailId(String shippingDetailId) {
        this.shippingDetailId = shippingDetailId;
    }

    public String getShippingDetailId() {
        return shippingDetailId;
    }


    public void setShippingMethodId(String shippingMethodId) {
        this.shippingMethodId = shippingMethodId;
    }

    public String getShippingMethodId() {
        return shippingMethodId;
    }

    public void setShippingCost(BigDecimal shippingCost) {
        this.shippingCost = shippingCost;
    }

    public BigDecimal getShippingCost() {
        return shippingCost;
    }
}
