package com.ftd.osp.utilities.cacheMgr.handlers.vo;

/**
 * This class models an occasion returned to the client in the getOccasion call.
 */
public class OccasionVO
{
    private String indexId;
    private String indexName;


    public void setIndexId(String indexId) {
        this.indexId = indexId;
    }

    public String getIndexId() {
        return indexId;
    }

    public void setIndexName(String indexName) {
        this.indexName = indexName;
    }

    public String getIndexName() {
        return indexName;
    }
}
