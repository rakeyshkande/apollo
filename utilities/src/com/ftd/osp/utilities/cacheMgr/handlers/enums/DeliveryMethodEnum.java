package com.ftd.osp.utilities.cacheMgr.handlers.enums;

import org.apache.commons.lang.StringUtils;


public enum DeliveryMethodEnum {
    FLORIST, CARRIER, BOTH;

    public static DeliveryMethodEnum toDeliveryMethod(String shipMethodFlorist, String shipMethodCarrier) {
        if( StringUtils.equals("Y", shipMethodFlorist) &&  StringUtils.equals("Y", shipMethodCarrier)) {
            return BOTH;
        }
        else if( StringUtils.equals("Y", shipMethodFlorist)) {
            return FLORIST;
        }
        else if( StringUtils.equals("Y", shipMethodCarrier)) {
            return CARRIER;
        }

        return null;
    }

    public static String toDeliveryMethodString(DeliveryMethodEnum dm) {
        if(dm.equals(FLORIST)) {
            return "Florist";
        }
        else if(dm.equals(CARRIER)) {
            return "Carrier";
        }
        else if(dm.equals(BOTH)) {
            return "Both";
        }
        return null;
    }

}
