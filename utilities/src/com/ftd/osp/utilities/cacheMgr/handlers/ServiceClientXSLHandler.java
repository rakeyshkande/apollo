package com.ftd.osp.utilities.cacheMgr.handlers;

import com.ftd.osp.utilities.cacheMgr.constant.CacheMgrConstants;
import com.ftd.osp.utilities.cacheMgr.dao.CacheMgrDAO;
import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.ClientActionVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.ClientVO;
import com.ftd.osp.utilities.cacheMgr.vo.CacheHandlerBase;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.osp.utilities.xml.TraxUtil;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.transform.Templates;


/**
 * This class is responsible for loading client into cache.
 * key: clientId, actionType
 * value: xslCode
 * 
 * key: xslCode
 * value: xslTxt
 */
public class ServiceClientXSLHandler extends CacheHandlerBase
{
    private Map clientActionXSLMap;
    private Map XSLMap;
    private Map XSLTemplateMap;
    
    private String KEY_CLIENT_ACTION_XSL = "KEY_CLIENT_ACTION_XSL";
    private String KEY_XSL_BY_ID = "KEY_XSL_BY_ID";
    private String KEY_XSL_TEMPLATE_BY_ID = "KEY_XSL_TEMPLATE_BY_ID";
        
    public Object load(Connection conn) throws CacheException {
        logger = new Logger(this.getClass().getName());
        CacheMgrDAO osdao = new CacheMgrDAO();
        CachedResultSet rs = osdao.executeQuery(conn, CacheMgrConstants.STMT_GET_ORDER_SERVICE_CLIENT_XSL);

        Map handlerMap = new HashMap();
        Map clientMap = new HashMap();
        Map xslMap = new HashMap();
        Map xslTemplateMap = new HashMap();
        String xslCode = null;
        String clientId = null;
        String actionType = null;
        
        if(logger.isDebugEnabled())
          logger.debug("Loaded service clients action xsl mapping...");
        while (rs != null && rs.next()) {   
            
            clientId = rs.getString("client_id");
            actionType = rs.getString("action_type");
            xslCode = rs.getString("xsl_code");
            ClientActionVO cavo = new ClientActionVO(clientId, actionType, xslCode);
            clientMap.put(cavo, cavo);
        }
        
        if(logger.isDebugEnabled())
          logger.debug("Loaded service xsl...");
        rs = osdao.executeQuery(conn, CacheMgrConstants.STMT_GET_ORDER_SERVICE_XSL);
        xslCode = null;
        String xslTxt = null;
        while (rs != null && rs.next()) {   
            xslCode = rs.getString("xsl_code");
            xslTxt = rs.getString("xsl_txt");
            xslMap.put(xslCode, xslTxt);
            
            // Create and cache a Template to make transformation faster
            try {
              Templates template = TraxUtil.getInstance().compileTemplate(xslTxt);
              xslTemplateMap.put(xslCode, template);  
              
              // Invoke newTransformer to 'exercise' the templates instance
              template.newTransformer();
            } catch (Exception e) {
              logger.error("Invalid XSL Transformation for xsl_code: " + xslCode, e);
            }
            
        }
        
        handlerMap.put(KEY_CLIENT_ACTION_XSL, clientMap);
        handlerMap.put(KEY_XSL_BY_ID, xslMap);
        handlerMap.put(KEY_XSL_TEMPLATE_BY_ID, xslTemplateMap);
        return handlerMap;
    }
    
    /**
     * Set the cached object in the cache handler. The cache handler is then 
     * responsible for fulfilling all application level API calls, to access the
     * data in the cached object.
     * 
     * @param cachedObject
     * @throws com.ftd.osp.utilities.cache.exception.CacheException
     */    
      public void setCachedObject(Object cachedObject) throws CacheException
      {
        try 
        {
            this.clientActionXSLMap = (Map)((Map) cachedObject).get(KEY_CLIENT_ACTION_XSL);  
            this.XSLMap = (Map)((Map) cachedObject).get(KEY_XSL_BY_ID); 
            this.XSLTemplateMap =  (Map)((Map) cachedObject).get(KEY_XSL_TEMPLATE_BY_ID);
        } catch (Exception ex) 
        {
          super.logger.error(ex);
          throw new CacheException("Could not set the cached object.");
        } 
      }
      
    /**
     * Returns the XSL Code for the passed in client ID and actionType
     * 
     * @param clientId
     * @param actionType
     * @return The xsl code, can be utilized to retrieve the XSL String or Templates object
     * @throws com.ftd.osp.utilities.cache.exception.CacheException
     */          
      public String getXSLCodeByClientAction(String clientId, String actionType) throws CacheException
      {
          if(logger.isDebugEnabled())
            logger.debug("getXSLCodeByClientAction(" + clientId + "," + actionType + ")");
          ClientActionVO key = new ClientActionVO(clientId, actionType, null);
          ClientActionVO value = null;
          
          Object cachedObj = this.clientActionXSLMap;
          if (cachedObj == null) {
                throw new CacheException("Cached object is null.");
          }
          Object obj = this.clientActionXSLMap.get(key);
          String xslCode = null;
          if(obj != null) {
                value = (ClientActionVO)obj;     
                xslCode = value.getXslCode();
          } 
          if(logger.isDebugEnabled())
            logger.debug("xsl code:" + xslCode);
          return xslCode;
      }
      
      
    /**
     * Returns the XSL String for the passed in xslCode
     * 
     * @param xslCode
     * @return The xsl XSL String
     * @throws com.ftd.osp.utilities.cache.exception.CacheException
     */                
      public String getXSLById(String xslCode) throws CacheException {
          String key = xslCode;
          String value = null;
          Object cachedObj = this.XSLMap;
          if (cachedObj == null) {
                throw new CacheException("Cached object is null.");
          }
          Object obj = this.XSLMap.get(key);

          if(obj != null) {
                value = (String)obj;     
          } else {
                if(logger.isDebugEnabled())
                  logger.debug("xsl not found:" + xslCode);
          }
          return value;
      }
      
    /**
     * Returns the XSL Template for the passed in xslCode
     * This template can be used to create Transformer instances.
     * 
     * @param xslCode
     * @return The xsl XSL Templates object
     * @throws com.ftd.osp.utilities.cache.exception.CacheException
     */         
      public Templates getXSLTemplateById(String xslCode) throws CacheException {
          String key = xslCode;
          Templates value = null;
          Object cachedObj = this.XSLTemplateMap;
          if (cachedObj == null) {
                throw new CacheException("Cached object is null.");
          }
          Object obj = this.XSLTemplateMap.get(key);

          if(obj != null) {
                value = (Templates) obj;     
          } else {
                if(logger.isDebugEnabled())
                  logger.debug("xsl not found:" + xslCode);
          }
          return value;        
        
      }
}
