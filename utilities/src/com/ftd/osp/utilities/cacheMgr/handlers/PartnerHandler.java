package com.ftd.osp.utilities.cacheMgr.handlers;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.PartnerVO;
import com.ftd.osp.utilities.cacheMgr.vo.CacheHandlerBase;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;


/**
 * This class is responsible for loading product company into cache.
 * key: productId
 * value: CompanyMasterVO
 */
public class PartnerHandler extends CacheHandlerBase
{
	private Map preferredPartnerMap;
	private Map preferredSourceMap;
	private Map preferredResourceMap;

	private static final String GET_PREFERRED_PARTNER = "GET_PREFERRED_PARTNER";
	private static final String KEY_PREFERRED_PARTNER = "KEY_PREFERRED_PARTNER";
	private static final String KEY_PREFERRED_SOURCE = "KEY_SOURCE_PARTNER";

	public PartnerHandler()
	{        
	}

	/**
	 * Returns the object that needs to be cached.
	 * 
	 * The cache cannot be configured as distributable, if the cache handler 
	 * returns a custom object. In order to utilize the benefits of a distributable 
	 * cache, the return objects should only be a part JDK API.
	 * 
	 * @param con
	 * @return the object to be cached
	 * @throws com.ftd.osp.utilities.cacheMgr.exception.CacheException
	 */
	public Object load(Connection con) throws CacheException {
		Map partnerCacheObject = new HashMap();

		Map partnerObject = new HashMap();
		Map sourceObject = new HashMap();
		try
		{
			super.logger.debug("BEGIN LOADING PARTNER HANDLER...");
			DataAccessUtil dau = DataAccessUtil.getInstance();
			DataRequest dataRequest = new DataRequest();
			dataRequest.setConnection(con);
			dataRequest.setStatementID(GET_PREFERRED_PARTNER);
			Map outputs = (Map)dau.execute(dataRequest);
			CachedResultSet rsPartner = (CachedResultSet)outputs.get("OUT_PARTNER_CUR");
			CachedResultSet rsSource = (CachedResultSet)outputs.get("OUT_SOURCE_CUR");

			while(rsPartner != null && rsPartner.next())
			{
				PartnerVO pvo = new PartnerVO();
				pvo.setPartnerName(rsPartner.getString("PARTNER_NAME"));
				pvo.setPreferredPartnerFlag(rsPartner.getString("PREFERRED_PARTNER_FLAG"));
				pvo.setPreferredProcessingResource(rsPartner.getString("PREFERRED_PROCESSING_RESOURCE"));
				pvo.setMarketingInsertAllowed(rsPartner.getString("PREFERRED_PARTNER_FLAG"));
				pvo.setFloristResendAllowed(rsPartner.getString("FLORIST_RESEND_ALLOWED_FLAG"));
				pvo.setReplyEmailAddress(rsPartner.getString("REPLY_EMAIL_ADDRESS"));
				pvo.setDefaultPhoneSourceCode(rsPartner.getString("DEFAULT_PHONE_SOURCE_CODE"));
				pvo.setDefaultWebSourceCode(rsPartner.getString("DEFAULT_WEB_SOURCE_CODE"));
				String displayName = rsPartner.getString("DISPLAY_NAME");
				if (displayName == null || displayName.equals("")) {
					displayName = pvo.getPartnerName();
				}
				pvo.setDisplayValue(displayName);
				partnerObject.put(pvo.getPartnerName(), pvo);
			}

			while(rsSource != null && rsSource.next())
			{
				PartnerVO svo = new PartnerVO();
				svo.setSourceCode(rsSource.getString("SOURCE_CODE"));
				svo.setPartnerName(rsSource.getString("PARTNER_NAME"));
				svo.setReplyEmailAddress(rsSource.getString("REPLY_EMAIL_ADDRESS"));
				svo.setPreferredProcessingResource(rsSource.getString("PREFERRED_PROCESSING_RESOURCE"));
				svo.setPreferredPartnerFlag(rsSource.getString("PREFERRED_PARTNER_FLAG"));
				svo.setMarketingInsertAllowed(rsSource.getString("PREFERRED_PARTNER_FLAG"));
				svo.setFloristResendAllowed(rsSource.getString("FLORIST_RESEND_ALLOWED_FLAG"));
				sourceObject.put(svo.getSourceCode(), svo);
			}            

			partnerCacheObject.put(this.KEY_PREFERRED_PARTNER, partnerObject);
			partnerCacheObject.put(this.KEY_PREFERRED_SOURCE, sourceObject);        
			rsPartner.reset();
			rsSource.reset();
			dataRequest.reset();
			super.logger.debug("END LOADING PARTNER HANDLER...");
		}
		catch(Exception e)
		{
			super.logger.error(e);
			throw new CacheException("Could not load PartnerHandler cache");
		}

		logger.debug(partnerCacheObject.size() + " records loaded in preferred partner cache");

		return partnerCacheObject;
	}


	/**
	 * Set the cached object in the cache handler. The cache handler is then 
	 * responsible for fulfilling all application level API calls, to access the
	 * data in the cached object.
	 * 
	 * @param cachedObject
	 * @throws com.ftd.osp.utilities.cacheMgr.exception.CacheException
	 */    
	public void setCachedObject(Object cachedObject) throws CacheException
	{
		try 
		{
			this.preferredPartnerMap = (Map)((Map) cachedObject).get(this.KEY_PREFERRED_PARTNER);  
			this.preferredSourceMap = (Map) ((Map) cachedObject).get(this.KEY_PREFERRED_SOURCE);  
		} catch (Exception ex) 
		{
			super.logger.error(ex);
			throw new CacheException("Could not set the cached object.");
		} 
	}

	public Map getPreferredPartner()
	{
		return this.preferredPartnerMap;
	}

	public Map getPreferredSource()
	{
		return this.preferredSourceMap;
	}

	public Map getPreferredPartnerResource()
	{
		return this.preferredResourceMap;
	}


}
