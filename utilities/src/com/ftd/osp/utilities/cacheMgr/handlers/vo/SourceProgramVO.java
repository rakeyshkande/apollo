package com.ftd.osp.utilities.cacheMgr.handlers.vo;

import java.math.BigDecimal;
/**

 */
public class SourceProgramVO
{

    private String sourceCode;
	private String programType;
    private String calculationBasis;
    private BigDecimal points;
    private String bonusCalculationBasis;
    private BigDecimal bonusPoints;
    private BigDecimal maximumPoints;
    private String rewardType;
    private String partnerName;
    private String partnerId;
    
    
    public SourceProgramVO () {}
    

    public String getSourceCode() {
		return sourceCode;
	}


	public void setSourceCode(String sourceCode) {
		this.sourceCode = sourceCode;
	}


	public String getProgramType() {
		return programType;
	}

	public void setProgramType(String programType) {
		this.programType = programType;
	}

	public String getCalculationBasis() {
		return calculationBasis;
	}

	public void setCalculationBasis(String calculationBasis) {
		this.calculationBasis = calculationBasis;
	}

	public BigDecimal getPoints() {
		return points;
	}

	public void setPoints(BigDecimal points) {
		this.points = points;
	}

	public String getBonusCalculationBasis() {
		return bonusCalculationBasis;
	}

	public void setBonusCalculationBasis(String bonusCalculationBasis) {
		this.bonusCalculationBasis = bonusCalculationBasis;
	}

	public BigDecimal getBonusPoints() {
		return bonusPoints;
	}

	public void setBonusPoints(BigDecimal bonusPoints) {
		this.bonusPoints = bonusPoints;
	}

	public BigDecimal getMaximumPoints() {
		return maximumPoints;
	}

	public void setMaximumPoints(BigDecimal maximumPoints) {
		this.maximumPoints = maximumPoints;
	}

	public String getRewardType() {
		return rewardType;
	}

	public void setRewardType(String rewardType) {
		this.rewardType = rewardType;
	}

	public String getPartnerName() {
		return partnerName;
	}

	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}

	public String getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}



}
