package com.ftd.osp.utilities.cacheMgr.handlers;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

import com.ftd.osp.utilities.cacheMgr.constant.CacheMgrConstants;
import com.ftd.osp.utilities.cacheMgr.dao.CacheMgrDAO;
import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.cacheMgr.vo.CacheHandlerBase;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * This class is responsible for loading state company tax controls info into
 * cache. key: [state id]_[company id] value: is_tax_on boolean
 */

public class StateCompanyTaxHandler extends CacheHandlerBase {

	private static String UNDERSCORE = "_";

	private Map<String, Boolean> stateCompanyTaxControls;

	public Object load(Connection conn) throws CacheException {

		logger = new Logger(this.getClass().getName());
		CacheMgrDAO osdao = new CacheMgrDAO();
		CachedResultSet rs = osdao.executeQuery(conn, CacheMgrConstants.STMT_GET_STATE_COMPANY_TAX_CONTROLS);

		 
		StringBuffer stateCompany = null;		 
		Map<String, Boolean> taxControls = new HashMap<String, Boolean>();
		try {

			if (logger.isInfoEnabled()) {
				logger.info("Loaded state company tax controls...");
			}

			while (rs != null && rs.next()) {
				stateCompany = new StringBuffer().append(rs.getString("state_master_id")).append(UNDERSCORE).append(rs.getString("company_id"));
				taxControls.put(stateCompany.toString(), "Y".equals(rs.getString("is_tax_on")) ? true : false);
			} 

		} catch (Exception e) {
			logger.error("Error loading StateCompanyTaxHandler: " + e);

		}
		return taxControls;
	}

	/**
	 * Set the cached object in the cache handler. The cache handler is then
	 * responsible for fulfilling all application level API calls, to access the
	 * data in the cached object.
	 * 
	 * @param cachedObject
	 * @throws com.ftd.osp.utilities.cache.exception.CacheException
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void setCachedObject(Object cachedObject) throws CacheException {
		try {
			this.stateCompanyTaxControls = (Map) cachedObject;
		} catch (Exception ex) {
			super.logger.error(ex);
			throw new CacheException("Could not set the cached object.");
		}
	}

	/**
	 * @param state
	 * @param companyId
	 * @return
	 * @throws CacheException
	 */
	public boolean isStateCompanyTaxON(String state, String companyId) throws CacheException {
		StringBuffer key = new StringBuffer(state).append(UNDERSCORE).append(companyId);
		
		if ( this.stateCompanyTaxControls == null) {
			throw new CacheException("Cached object is null.");			
		}
		
		Boolean obj = this.stateCompanyTaxControls.get(key.toString());
		
		if (obj == null ) {
			 return false;
		}	
		return obj;
	}

}
