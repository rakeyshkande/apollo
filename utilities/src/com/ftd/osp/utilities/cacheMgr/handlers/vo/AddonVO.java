package com.ftd.osp.utilities.cacheMgr.handlers.vo;

import java.math.BigDecimal;

import java.util.List;


public class AddonVO
{
	private String addonId;
    private String addonTypeId;
    private String addonType;
    private String addonTitle;
    private String addonDesc;
    private String addonText;
    private BigDecimal addonPrice;
    private String priceType;
    private String priceUnit;
    private String displayPriceFlag;
    private String defaultPerTypeFlag;
    private int maxPurchaseQty;
    private ImageVO image;
    private String occasionId;
    private String occasionDesc;
    private String addOnTypeDescription;
    private String addOnUNSPSC;
    private String addOnWeight;
    private String productId;
    private String addOnAvailableFlag;
    private String addOnTypeIncludedInProductFeedFlag;
    private String displaySequenceNumber;
	
    public AddonVO () {}
    
    public AddonVO (String addonId) {
        this.addonId = addonId;
    }
    
    public String getAddonId() {
		return addonId;
	}
	public void setAddonId(String addonId) {
		this.addonId = addonId;
	}
	public String getAddonTypeId() {
		return addonTypeId;
	}
	public void setAddonTypeId(String addonTypeId) {
		this.addonTypeId = addonTypeId;
	}
	public String getAddonType() {
		return addonType;
	}
	public void setAddonType(String addonType) {
		this.addonType = addonType;
	}
	public String getAddonTitle() {
		return addonTitle;
	}
	public void setAddonTitle(String addonTitle) {
		this.addonTitle = addonTitle;
	}
	public String getAddonDesc() {
		return addonDesc;
	}
	public void setAddonDesc(String addonDesc) {
		this.addonDesc = addonDesc;
	}
	public String getAddonText() {
		return addonText;
	}
	public void setAddonText(String addonText) {
		this.addonText = addonText;
	}
	public BigDecimal getAddonPrice() {
		return addonPrice;
	}
	public void setAddonPrice(BigDecimal addonPrice) {
		this.addonPrice = addonPrice;
	}
	public String getPriceType() {
		return priceType;
	}
	public void setPriceType(String priceType) {
		this.priceType = priceType;
	}
	public String getPriceUnit() {
		return priceUnit;
	}
	public void setPriceUnit(String priceUnit) {
		this.priceUnit = priceUnit;
	}
	public String getDisplayPriceFlag() {
		return displayPriceFlag;
	}
	public void setDisplayPriceFlag(String displayPriceFlag) {
		this.displayPriceFlag = displayPriceFlag;
	}
	public String getDefaultPerTypeFlag() {
		return defaultPerTypeFlag;
	}
	public void setDefaultPerTypeFlag(String defaultPerTypeFlag) {
		this.defaultPerTypeFlag = defaultPerTypeFlag;
	}
	public int getMaxPurchaseQty() {
		return maxPurchaseQty;
	}
	public void setMaxPurchaseQty(int maxPurchaseQty) {
		this.maxPurchaseQty = maxPurchaseQty;
	}
	public ImageVO getImage() {
		return image;
	}
	public void setImage(ImageVO image) {
		this.image = image;
	}
	public String getOccasionId() {
		return occasionId;
	}
	public void setOccasionId(String occasionId) {
		this.occasionId = occasionId;
	}
	public String getOccasionDesc() {
		return occasionDesc;
	}
	public void setOccasionDesc(String occasionDesc) {
		this.occasionDesc = occasionDesc;
	}
	public String getAddOnTypeDescription() {
		return addOnTypeDescription;
	}
	public void setAddOnTypeDescription(String addOnTypeDescription) {
		this.addOnTypeDescription = addOnTypeDescription;
	}
	public String getAddOnUNSPSC() {
		return addOnUNSPSC;
	}
	public void setAddOnUNSPSC(String addOnUNSPSC) {
		this.addOnUNSPSC = addOnUNSPSC;
	}
	public String getAddOnWeight() {
		return addOnWeight;
	}
	public void setAddOnWeight(String addOnWeight) {
		this.addOnWeight = addOnWeight;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getAddOnAvailableFlag() {
		return addOnAvailableFlag;
	}
	public void setAddOnAvailableFlag(String addOnAvailableFlag) {
		this.addOnAvailableFlag = addOnAvailableFlag;
	}
	public String getAddOnTypeIncludedInProductFeedFlag() {
		return addOnTypeIncludedInProductFeedFlag;
	}
	public void setAddOnTypeIncludedInProductFeedFlag(
			String addOnTypeIncludedInProductFeedFlag) {
		this.addOnTypeIncludedInProductFeedFlag = addOnTypeIncludedInProductFeedFlag;
	}
	public String getDisplaySequenceNumber() {
		return displaySequenceNumber;
	}
	public void setDisplaySequenceNumber(String displaySequenceNumber) {
		this.displaySequenceNumber = displaySequenceNumber;
	}
    
}