package com.ftd.osp.utilities.cacheMgr.handlers;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.ftd.osp.utilities.cacheMgr.constant.CacheMgrConstants;
import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.cacheMgr.vo.CacheHandlerBase;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;

/**
 * This class is responsible for loading payment method and BIN number mapping into cache.
 */
public class PaymentBinMasterHandler extends CacheHandlerBase
{
    /*
     * Cache of payment method text types and associated text Set by the setCachedObject method
     */
    private Map objectMap;


    public Object load(Connection conn) throws CacheException
    {
        Map binPaymentMap = new HashMap();

        try
        {
            super.logger.debug("BEGIN LOADING PaymentBinMasterHandler...");

            // Obtain payment method text information
            DataAccessUtil dau = DataAccessUtil.getInstance();
            DataRequest dataRequest = new DataRequest();
            dataRequest.setStatementID(CacheMgrConstants.STMT_GET_PAYMENT_BIN_MASTER);
            dataRequest.setConnection(conn);
            CachedResultSet crs = (CachedResultSet) dau.execute(dataRequest);

            String binNumber = null;
            String paymentMethodId = null;
            while (crs.next())
            {
                // Obtain the BIN number
                binNumber = crs.getString("bin_number");
                paymentMethodId = crs.getString("payment_method_id");

                binPaymentMap.put(binNumber, paymentMethodId);
            }

            super.logger.debug("END LOADING PaymentBinMasterHandler...");
        }
        catch (Exception e)
        {
            super.logger.error(e);
            throw new CacheException("Could not load PaymentBinMasterHandler cache");
        }

        return binPaymentMap;
    }

    /**
     * Set the cached object in the cache handler. The cache handler is then responsible for fulfilling all application
     * level API calls, to access the data in the cached object.
     * 
     * @param cachedObject
     * @throws com.ftd.osp.utilities.cache.exception.CacheException
     */
    public void setCachedObject(Object cachedObject) throws CacheException
    {
        try
        {
            this.objectMap = (Map) cachedObject;
        }
        catch (Exception ex)
        {
            super.logger.error(ex);
            throw new CacheException("Could not set the cached object.");
        }
    }

    /**
     * Returns the payment method id mapped to the BIN number
     * Input number can be the full number. Supports BIN numbers of 6 digits.
     * @param number
     * @return
     */
    public String getPaymentMethodIdByNumber(String number)
    {

        String bin6 = "";
        String paymentMethodId = null;
        
        if (number != null && number.length() >= 6) {
            bin6 = number.substring(0,6);
        } 
        
        if (bin6 != null) {
        	paymentMethodId = (String)this.objectMap.get(bin6);
        }
        
        if(logger.isDebugEnabled()) {
        	String logMsg = "getPaymentMethodIdByNumber returning: " + paymentMethodId;
        	logger.debug(logMsg);
        }
        return paymentMethodId;
    }

}
