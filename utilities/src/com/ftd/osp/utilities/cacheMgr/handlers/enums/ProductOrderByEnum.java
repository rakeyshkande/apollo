package com.ftd.osp.utilities.cacheMgr.handlers.enums;

import org.apache.commons.lang.StringUtils;


public enum ProductOrderByEnum {
    POPULARITY, PRICE_ASC, PRICE_DESC;

    public static ProductOrderByEnum toProductOrderBy(String productOrderBy) {
        if( StringUtils.equals("popularity", productOrderBy)) {
            return POPULARITY;
        }
        else if( StringUtils.equals("priceAscending", productOrderBy)) {
            return PRICE_ASC;
        }
        else if( StringUtils.equals("priceDescending", productOrderBy)) {
            return PRICE_DESC;
        }

        return null;
    }

    public static String toProductOrderByStr(ProductOrderByEnum productOrderByEnum) {
        if( POPULARITY.equals(productOrderByEnum)) {
            return "popularity";
        }
        else if( PRICE_ASC.equals(productOrderByEnum)) {
            return "priceAscending";
        }
        else if( PRICE_DESC.equals(productOrderByEnum)) {
            return "priceDescending";
        }

        return null;
    }
    
    public static boolean isValidProductOrderBy(String productOrderBy) {
        if( "popularity".equals(productOrderBy) ||
            "priceAscending".equals(productOrderBy) ||
            "priceDescending".equals(productOrderBy) ) {
            return true;
        }

        return false;
    }

}
