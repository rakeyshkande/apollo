package com.ftd.osp.utilities.cacheMgr.handlers;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

import com.ftd.osp.utilities.cacheMgr.constant.CacheMgrConstants;
import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.SecureParmVO;
import com.ftd.osp.utilities.cacheMgr.vo.CacheHandlerBase;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * This class is responsible for loading country into cache. Call GET_SECURE_CONTEXT
 * key: context,name
 * value: String
 */
public class SecureConfigHandler extends CacheHandlerBase
{
    private Map objMap;

	private static final String GET_SECURE_CONTEXT = "GET_SECURE_CONTEXT";
	  
    public Object load(Connection conn) throws CacheException {
        logger = new Logger(this.getClass().getName());
		try {
			
        DataAccessUtil dau = DataAccessUtil.getInstance();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID(GET_SECURE_CONTEXT);
        dataRequest.addInputParam("IN_CONTEXT", null); // Retrieve all contexts
        CachedResultSet rs = (CachedResultSet) dau.execute(dataRequest);
		       

        String context = "";
        String name = "";
        String value = "";
        Map handlerMap = new HashMap();
        Map globalParmMap = new HashMap();
        
        if(rs != null) {
            while (rs != null && rs.next()) {
                context = rs.getString("context");
                name = rs.getString("name");
                value = rs.getString("value");

                SecureParmVO obj = new SecureParmVO(context, name, value);
                
                globalParmMap.put(obj, obj);
            }
        }
        
        handlerMap.put(CacheMgrConstants.CACHE_KEY_ID_GEN, globalParmMap);
        return handlerMap;
        
		} catch (Exception e) {
			throw new CacheException(e.getMessage());
		} 
    }
    
    /**
     * Set the cached object in the cache handler. The cache handler is then 
     * responsible for fulfilling all application level API calls, to access the
     * data in the cached object.
     * 
     * @param cachedObject
     * @throws com.ftd.osp.utilities.cache.exception.CacheException
     */    
      public void setCachedObject(Object cachedObject) throws CacheException
      {
          try 
          {
            this.objMap = (Map)((Map) cachedObject).get(CacheMgrConstants.CACHE_KEY_ID_GEN);  
          } catch (Exception ex) 
          {
            super.logger.error(ex);
            throw new CacheException("Could not set the cached object.");
          } 
      }
      
        
        
        /**
         * Returns the value of a property
         * @return String - property value
         */
        public String getProperty(String context, String name) throws CacheException
        {
            SecureParmVO key = new SecureParmVO(context, name, null);
            
            Object cachedObj = this.objMap;
            if (cachedObj == null) {
                  throw new CacheException("Cached object is null.");
            }
            Object obj = this.objMap.get(key);
            String v = null;
            if(obj != null) {
                  SecureParmVO value = (SecureParmVO)obj;
                  v = value.getValue();
            } 
            return v;
        } 
        
}
