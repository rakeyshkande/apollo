package com.ftd.osp.utilities.cacheMgr.handlers.vo;


/**

 */
public class TransientProductVO
{
    private String clientId;
    private String sourceCode;
    private String[] productId;
    private String loadLevel;
    private int hashcode;
    
    /**
    * Contains a cached/calculated value for the toString. Calculated by {@link TransientProductVO#updateToStringValAndHashCode()}
    */
    private String toStringVal;
    
    public TransientProductVO (String clientId, String sourceCode, String[] productId, String loadLevel) {
        this.clientId = clientId;
        this.sourceCode = sourceCode;
        this.loadLevel = loadLevel;
        this.productId = productId;
        
        updateToStringValAndHashCode();
    }

    //overwriting hashCode() of Object
    public int hashCode() {
        return hashcode; // Precalculated by updateToStringValAndHashCode
    }
    
    //overwirting equals() of Ojbect
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        
        TransientProductVO obj = (TransientProductVO)o;
        
        // The ToString is a combination of all fields exact.
        // If we want to support mixed ordering of productId, we'll need to do a different algorithm
        // But that is not the current matching process
        return this.toString().equals(obj.toString());
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
        updateToStringValAndHashCode();
    }

    public String getClientId() {
        return clientId;
    }

    public void setSourceCode(String sourceCode) {
        this.sourceCode = sourceCode;
        updateToStringValAndHashCode();
    }

    public String getSourceCode() {
        return sourceCode;
    }

    public void setProductId(String[] productId) {
        this.productId = productId;
        updateToStringValAndHashCode();
    }

    public String[] getProductId() {
        return productId;
    }

    public void setLoadLevel(String loadLevel) {
        this.loadLevel = loadLevel;
        updateToStringValAndHashCode();
    }

    public String getLoadLevel() {
        return loadLevel;
    }
    
    
    public String toString() {     
      return toStringVal;
    }
    
    
    /**
     * Calculates the toString Value by appending all fields with a | separator. Also calculates the hashcode
     * as the hashcode of the toString Value.
     * 
     * Values are "clientId | sourceCode | loadLevel | [productId, productId ...]"
     */
    private void updateToStringValAndHashCode() {
      StringBuilder toStringBuilder = new StringBuilder(255).append(clientId).append("|").append(sourceCode).append("|").append(loadLevel);
      
      if(productId != null && productId.length > 0) {
        toStringBuilder.append("|[").append(productId[0]);
        
        for(int iLoop = 1; iLoop < productId.length; iLoop ++) {
          toStringBuilder.append(",").append(productId[iLoop]);
        }
        
        toStringBuilder.append("]");
      }

      toStringVal = toStringBuilder.toString();
      hashcode = toStringVal.hashCode();
    }
    
    
   /**
   * Just prints out some samples of toString and HashCode, for testing
   */
    public static void main(String[] args) {
      testPrint(new TransientProductVO(null, null, null, null));
      testPrint(new TransientProductVO("cliendId", null, null, null));      
      testPrint(new TransientProductVO("cliendId", "sourceId", null, null));
      testPrint(new TransientProductVO("cliendId", "sourceId", null, "level0"));
      testPrint(new TransientProductVO("cliendId", "sourceId", new String[] {}, "level0"));
      testPrint(new TransientProductVO("cliendId", "sourceId", new String[] {"One"}, "level0"));
      testPrint(new TransientProductVO("cliendId", "sourceId", new String[] {"One", "Two", "Three", "Four"}, "level0"));      
      testPrint(new TransientProductVO("cliendId", "sourceId", new String[] {"One", "Two", "Three", "Four"}, null));  
    }
    
   /**
   * Prints out the toString and hashCode, for testing only.
   * @param transientProductVO
   */
   private static void testPrint(TransientProductVO transientProductVO) {
   
      String output = "VO: " + transientProductVO.toString() + "; HashCode: " + transientProductVO.hashCode();

      // Check time taken to generate toString and HashCode      
      final int TotalTestCounts = 10000;      
      long startTime = System.currentTimeMillis();

      for(int iLoop = 0; iLoop < TotalTestCounts; iLoop ++) {
        transientProductVO.updateToStringValAndHashCode();
      }
      
      long endTime = System.currentTimeMillis();
      long totalTime = endTime - startTime;
      long timeToCalculate = totalTime / TotalTestCounts;
      
      output += "; Calc Time: " + timeToCalculate;
      
      System.out.println(output);
   }
}
