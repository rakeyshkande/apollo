package com.ftd.osp.utilities.cacheMgr.handlers;

import com.ftd.osp.utilities.cacheMgr.constant.CacheMgrConstants;
import com.ftd.osp.utilities.cacheMgr.dao.CacheMgrDAO;
import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.cacheMgr.vo.CacheHandlerBase;

import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;

import java.util.HashMap;
import java.util.Map;

public class SpecialCharacterMasterHandler extends CacheHandlerBase
{

  private Map specialCharacterMap;
  public Object load(Connection conn) throws CacheException{
  
        logger = new Logger(this.getClass().getName());
        CacheMgrDAO osdao = new CacheMgrDAO();
        CachedResultSet rs = osdao.executeQuery(conn, CacheMgrConstants.STMT_GET_CHARACTER_MAPPING);
        Map map = new HashMap();        
        logger.info("####Loading SpecialCharacterMaster...");        
        while (rs != null && rs.next()) {
            map.put((String)rs.getString("SPECIAL_CHARACTER_ID"), (String)rs.getString("ENGLISH_MAPPING"));
        }
        
    return map;
  }

  public void setCachedObject(Object cachedObject) throws CacheException{  
    specialCharacterMap = (Map) cachedObject;
  }
  
  public Map getSpecialCharacterMap()
  {
    return specialCharacterMap;
  }
}
