package com.ftd.osp.utilities.cacheMgr.handlers.vo;

import java.math.BigDecimal;

public class ElementConfigVO {
    private String appCode;
    private String elementId;
    private String locationDescription;
    private String labelElementId;
    private String countElementId;
    private boolean requiredFlag;
    private String requiredCondition;
    private String requiredContitionType;
    private boolean validateFlag;
    private String validateExpression;
    private String validateExpressionType;
    private String errorText;
    private String titleText;
    private BigDecimal maxLength;
    private BigDecimal tabIndex;
    private BigDecimal tabInitialIndex;
    private String tabCondition;
    private String tabConditionType;
    private BigDecimal accordionIndex;
    private BigDecimal tabControlIndex;
    private boolean recalcFlag;
    private boolean productAvailabilityFlag;
    private String accessKey;
    private String xmlNodeName;
    private boolean calcXmlFlag;
    
    public ElementConfigVO() {
    }

    public void setAppCode(String param) {
        this.appCode = param;
    }

    public String getAppCode() {
        return appCode;
    }

    public void setElementId(String param) {
        this.elementId = param;
    }

    public String getElementId() {
        return elementId;
    }

    public void setLocationDescription(String param) {
        this.locationDescription = param;
    }

    public String getLocationDescription() {
        return locationDescription;
    }

    public void setLabelElementId(String param) {
        this.labelElementId = param;
    }

    public String getLabelElementId() {
        return labelElementId;
    }

    public void setCountElementId(String param) {
        this.countElementId = param;
    }

    public String getCountElementId() {
        return countElementId;
    }

    public void setRequiredFlag(boolean param) {
        this.requiredFlag = param;
    }

    public boolean isRequiredFlag() {
        return requiredFlag;
    }

    public void setRequiredCondition(String param) {
        this.requiredCondition = param;
    }

    public String getRequiredCondition() {
        return requiredCondition;
    }

    public void setRequiredContitionType(String param) {
        this.requiredContitionType = param;
    }

    public String getRequiredContitionType() {
        return requiredContitionType;
    }

    public void setValidateFlag(boolean param) {
        this.validateFlag = param;
    }

    public boolean isValidateFlag() {
        return validateFlag;
    }

    public void setValidateExpression(String param) {
        this.validateExpression = param;
    }

    public String getValidateExpression() {
        return validateExpression;
    }

    public void setValidateExpressionType(String param) {
        this.validateExpressionType = param;
    }

    public String getValidateExpressionType() {
        return validateExpressionType;
    }

    public void setErrorText(String param) {
        this.errorText = param;
    }

    public String getErrorText() {
        return errorText;
    }

    public void setTitleText(String param) {
        this.titleText = param;
    }

    public String getTitleText() {
        return titleText;
    }

    public void setMaxLength(BigDecimal param) {
        this.maxLength = param;
    }

    public BigDecimal getMaxLength() {
        return maxLength;
    }

    public void setTabIndex(BigDecimal param) {
        this.tabIndex = param;
    }

    public BigDecimal getTabIndex() {
        return tabIndex;
    }

    public void setTabInitialIndex(BigDecimal param) {
        this.tabInitialIndex = param;
    }

    public BigDecimal getTabInitialIndex() {
        return tabInitialIndex;
    }

    public void setTabCondition(String param) {
        this.tabCondition = param;
    }

    public String getTabCondition() {
        return tabCondition;
    }

    public void setTabConditionType(String param) {
        this.tabConditionType = param;
    }

    public String getTabConditionType() {
        return tabConditionType;
    }

    public void setAccordionIndex(BigDecimal param) {
        this.accordionIndex = param;
    }

    public BigDecimal getAccordionIndex() {
        return accordionIndex;
    }

    public void setTabControlIndex(BigDecimal param) {
        this.tabControlIndex = param;
    }

    public BigDecimal getTabControlIndex() {
        return tabControlIndex;
    }

    public void setRecalcFlag(boolean param) {
        this.recalcFlag = param;
    }

    public boolean isRecalcFlag() {
        return recalcFlag;
    }

    public void setProductAvailabilityFlag(boolean param) {
        this.productAvailabilityFlag = param;
    }

    public boolean isProductAvailabilityFlag() {
        return productAvailabilityFlag;
    }

    public void setAccessKey(String param) {
        this.accessKey = param;
    }

    public String getAccessKey() {
        return accessKey;
    }

    public void setXmlNodeName(String param) {
        this.xmlNodeName = param;
    }

    public String getXmlNodeName() {
        return xmlNodeName;
    }

    public void setCalcXmlFlag(boolean param) {
        this.calcXmlFlag = param;
    }

    public boolean isCalcXmlFlag() {
        return calcXmlFlag;
    }
}
