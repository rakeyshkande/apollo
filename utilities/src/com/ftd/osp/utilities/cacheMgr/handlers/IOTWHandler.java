package com.ftd.osp.utilities.cacheMgr.handlers;

import com.ftd.osp.utilities.cacheMgr.constant.CacheMgrConstants;
import com.ftd.osp.utilities.cacheMgr.dao.CacheMgrDAO;
import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.IOTWVO;
import com.ftd.osp.utilities.cacheMgr.vo.CacheHandlerBase;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;

import java.util.HashMap;
import java.util.Map;


/**
 * This class is responsible for loading content with filters.
 * key: ContentVO
 * value: ContentVO
 */
public class IOTWHandler extends CacheHandlerBase
{
    private Map objMap;

    public Object load(Connection conn) throws CacheException {
        logger = new Logger(this.getClass().getName());
        CacheMgrDAO osdao = new CacheMgrDAO();
        CachedResultSet rs = osdao.executeQuery(conn, CacheMgrConstants.STMT_GET_IOTW);

        Map handlerMap = new HashMap();
        Map iotwMap = new HashMap();
        String IOTWId = null;
        String productId = null;
        String upsellMasterId = null;
        String sourceCode = null;
        IOTWVO ivo = null;

        if(logger.isInfoEnabled())
          logger.info("Loaded content...");
        
        while (rs != null && rs.next()) {
            IOTWId = rs.getString("iotw_id");
            sourceCode = rs.getString("source_code");
            productId = rs.getString("product_id");
            upsellMasterId = rs.getString("upsell_master_id");

            ivo = new IOTWVO();
            ivo.setSourceCode(sourceCode);
            ivo.setProductId(productId);
            ivo.setUpsellMasterId(upsellMasterId);
            ivo.setIOTWSourceCode(rs.getString("iotw_source_code"));
            ivo.setStartDate(rs.getDate("start_date"));
            ivo.setEndDate(rs.getDate("end_date"));
            ivo.setProductPageMessaging(rs.getString("product_page_messaging_txt"));
            iotwMap.put(ivo, ivo);
        }
        
        handlerMap.put(CacheMgrConstants.CACHE_KEY_ID_GEN, iotwMap);
        return handlerMap;
    }
    
    /**
     * Set the cached object in the cache handler. The cache handler is then 
     * responsible for fulfilling all application level API calls, to access the
     * data in the cached object.
     * 
     * @param cachedObject
     * @throws com.ftd.osp.utilities.cache.exception.CacheException
     */    
      public void setCachedObject(Object cachedObject) throws CacheException
      {
        try 
        {
            this.objMap = (Map)((Map) cachedObject).get(CacheMgrConstants.CACHE_KEY_ID_GEN);

        } catch (Exception ex) 
        {
          super.logger.error(ex);
          throw new CacheException("Could not set the cached object.");
        } 
      }
      
      public IOTWVO getIOTW(String sourceCode, String productId, String upsellMasterId) throws CacheException
      {
          if(logger.isDebugEnabled())
            logger.debug("getIOTW source code is:" + sourceCode);
          IOTWVO key = new IOTWVO();
          key.setSourceCode(sourceCode);
          key.setProductId(productId);
          key.setUpsellMasterId(upsellMasterId);
          IOTWVO value = null;
          
          Object cachedObj = this.objMap;
          if (cachedObj == null) {
                throw new CacheException("Cached object is null.");
          }
          
          Object obj = this.objMap.get(key);

          if(obj != null) {
                value = (IOTWVO)obj;
          } 
          return value;
      }
      

}
