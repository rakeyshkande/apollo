package com.ftd.osp.utilities.cacheMgr.handlers;

import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.CostCenterVO;
import com.ftd.osp.utilities.cacheMgr.vo.CacheHandlerBase;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
/**
 * A cache handler is responsible for the following:
 * 
 * 1. Act as a cache loader, to load the data that needs to be cached.
 * 2. Provide application specific API, to enable access to the cached data.
 * 
 */

public class CostCenterHandler extends CacheHandlerBase 
{
    // set by the setCachedObject method
    private List costCenterList;
    private Set partnerIDSet;
    
    private static final String GET_COST_CENTERS = "GET_COST_CENTERS";
    private static final String COST_CENTER_LIST_KEY = "COST_CENTER_LIST_KEY";
    private static final String PARTNER_ID_SET_KEY = "PARTNER_ID_SET_KEY";
    
    public CostCenterHandler()
    {
    }
  
   /**
   * Returns the object that needs to be cached.
   * 
   * The cache cannot be configured as distributable, if the cache handler 
   * returns a custom object. In order to utilize the benefits of a distributable 
   * cache, the return objects should only be a part JDK API.
   * 
   * @param con
   * @return the object to be cached
   * @throws com.ftd.osp.utilities.cache.exception.CacheException
   */
    public Object load(Connection con) throws CacheException
    {  
        Map costCenterHandlerCacheObject = new HashMap();
        
        List costCenterListCacheObject = new ArrayList();
        Set partnerIDSetCacheObject = new HashSet();
        try
        {
            super.logger.debug("BEGIN LOADING COST CENTER HANDLER...");
            DataAccessUtil dau = DataAccessUtil.getInstance();
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(con);
            dataRequest.setStatementID(GET_COST_CENTERS);
            CachedResultSet rs = (CachedResultSet)dau.execute(dataRequest);

            CostCenterVO costCenterVO = null;
            
            while(rs.next())
            {
                costCenterVO = new CostCenterVO();
                costCenterVO.setCostCenterId((String) rs.getObject(1));
                costCenterVO.setPartnerId((String) rs.getObject(2));
                costCenterVO.setSourceCode((String) rs.getObject(3));

                costCenterListCacheObject.add(costCenterVO);
                partnerIDSetCacheObject.add((String) rs.getObject(2));
            }
            
            super.logger.debug("END LOADING COST CENTER HANDLER...");
        }
        catch(Exception e)
        {
            super.logger.error(e);
            throw new CacheException("Could not refresh CostCenterHandler cache");
        } 
        costCenterHandlerCacheObject.put(this.COST_CENTER_LIST_KEY, costCenterListCacheObject);
        costCenterHandlerCacheObject.put(this.PARTNER_ID_SET_KEY, partnerIDSetCacheObject);
        logger.debug(costCenterListCacheObject.size() + " records loaded in cost center list");
        logger.debug(partnerIDSetCacheObject.size() + " records loaded in partner id list");
        return costCenterHandlerCacheObject;
    }

  /**
   * Set the cached object in the cache handler. The cache handler is then 
   * responsible for fulfilling all application level API calls, to access the
   * data in the cached object.
   * 
   * @param cachedObject
   * @throws com.ftd.osp.utilities.cache.exception.CacheException
   */    
    public void setCachedObject(Object cachedObject) throws CacheException
    {
      try 
      {
        this.costCenterList = (List)((Map) cachedObject).get(this.COST_CENTER_LIST_KEY);  
        this.partnerIDSet = (Set)((Map) cachedObject).get(this.PARTNER_ID_SET_KEY);  
      } catch (Exception ex) 
      {
        super.logger.error(ex);
        throw new CacheException("Could not set the cached object.");
      } 
    }

    /**
     * Returns cost center VOs that correspond to the input parameters. If the
     * source code in the database is null then the combination of partner id and
     * cost center id is valid for any source code 
     * for this partner.
     * 
     * Returns an empty array list if no matches are found
     */
    public List getCostCenter(String costCenterId, String partnerId, String sourceCode)
    {
        ArrayList costCenterVOList = new ArrayList();
        CostCenterVO costCenterVO = null;
        
        Iterator it = costCenterList.iterator();
        while(it.hasNext())
        {
            costCenterVO = (CostCenterVO) it.next();

            if(costCenterVO.getCostCenterId() != null && costCenterVO.getCostCenterId().equals(costCenterId)
               && costCenterVO.getPartnerId() != null && costCenterVO.getPartnerId().equals(partnerId))
            {
                if (costCenterVO.getSourceCode() == null) 
                {
                    costCenterVOList.add(costCenterVO);
                }
                else if (costCenterVO.getSourceCode() != null && costCenterVO.getSourceCode().equals(sourceCode)) 
                {
                    costCenterVOList.add(costCenterVO);                    
                }
            }
        }

        return costCenterVOList;    
    }

  /**
   * 
   * @param partnerId
   * @return 
   */
    public boolean containsPartnerID(String partnerId)
    {
      if (partnerIDSet.contains(partnerId)) 
      {
          return true;
      }
      else
      {
        return false;
      }
      
    }

}