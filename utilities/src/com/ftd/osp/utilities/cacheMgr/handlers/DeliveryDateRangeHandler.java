package com.ftd.osp.utilities.cacheMgr.handlers;

import com.ftd.osp.utilities.cacheMgr.constant.CacheMgrConstants;
import com.ftd.osp.utilities.cacheMgr.dao.CacheMgrDAO;
import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.DeliveryDateRangeVO;
import com.ftd.osp.utilities.cacheMgr.vo.CacheHandlerBase;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * This class is responsible for loading delivery date ranges into cache.
 * key: ALL
 * value: List
 */
public class DeliveryDateRangeHandler extends CacheHandlerBase {
    private List dateRangeList;

    public Object load(Connection conn) throws CacheException{
        logger = new Logger(this.getClass().getName());
        CacheMgrDAO osdao = new CacheMgrDAO();
        CachedResultSet rs = osdao.executeQuery(conn, CacheMgrConstants.STMT_GET_DELIVERY_DATE_RANGE);
        List<DeliveryDateRangeVO> dateRangeList = new ArrayList();
        Map handlerMap = new HashMap();
        DeliveryDateRangeVO ddrvo = null;

        if(logger.isInfoEnabled())
          logger.info("Loading Delivery Date ranges");
        while (rs != null && rs.next()) {
            ddrvo = new DeliveryDateRangeVO();    
            ddrvo.setStartDate(rs.getDate("start_date"));
            ddrvo.setEndDate(rs.getDate("end_date"));
            ddrvo.setInstructionText(rs.getString("instruction_text"));
            dateRangeList.add(ddrvo);
        }
        handlerMap.put(CacheMgrConstants.CACHE_KEY_ALL, dateRangeList);
        return handlerMap;
    }

    /**
    * Set the cached object in the cache handler. The cache handler is then 
    * responsible for fulfilling all application level API calls, to access the
    * data in the cached object.
    * 
    * @param cachedObject
    * @throws com.ftd.osp.utilities.cache.exception.CacheException
    */    
    public void setCachedObject(Object cachedObject) throws CacheException {
        try {
            this.dateRangeList = (ArrayList)((Map) cachedObject).get(CacheMgrConstants.CACHE_KEY_ALL);  
        } catch (Exception ex) {
            super.logger.error(ex);
            throw new CacheException("Could not set the cached object.");
        } 
    }
           
    public List getDateRangeList() throws CacheException {
        return Collections.unmodifiableList(dateRangeList);
    }
    
    /*
     * Check the cache to see if the given date is part of a delivery date range
     * If so, return the end date of the range
     */
    public Date isDateInDeliveryDateRange(Date inDate) throws CacheException {
        Date endDate = null;
        for (int i=0; i<dateRangeList.size(); i++) {
            DeliveryDateRangeVO ddrvo = (DeliveryDateRangeVO) dateRangeList.get(i);
            if (ddrvo.getStartDate() != null && ddrvo.getStartDate().compareTo(inDate) <= 0 &&
                ddrvo.getEndDate() != null && ddrvo.getEndDate().compareTo(inDate) >= 0) {
                endDate = ddrvo.getEndDate();
                continue;
            }
        }
        
        return endDate;
    }
}

