package com.ftd.osp.utilities.cacheMgr.handlers.enums;

import org.apache.commons.lang.StringUtils;


public enum ImageTypeEnum {
    CATEGORY, PRODUCT, THUMBNAIL;

    public static ImageTypeEnum toImageType(String imageType) {
        if( StringUtils.equals(toImageTypeString(CATEGORY), imageType)) {
            return CATEGORY;
        }
        else if(StringUtils.equals(toImageTypeString(PRODUCT), imageType)) {
            return PRODUCT;
        }
        else if(StringUtils.equals(toImageTypeString(THUMBNAIL), imageType)) {
            return THUMBNAIL;
        }

        return null;
    }

    public static String toImageTypeString(ImageTypeEnum dm) {
        if(dm.equals(CATEGORY)) {
            return "category";
        }
        else if(dm.equals(PRODUCT)) {
            return "product";
        }
        else if(dm.equals(THUMBNAIL)) {
            return "thumbnail";
        }
        return null;
    }
}
