package com.ftd.osp.utilities.cacheMgr.handlers.vo;

public class MilesPointsVO {

	private String milesPoints;
    private String rewardType;
    
	public String getMilesPoints() {
		return milesPoints;
	}
	public void setMilesPoints(String milesPoints) {
		this.milesPoints = milesPoints;
	}
	public String getRewardType() {
		return rewardType;
	}
	public void setRewardType(String rewardType) {
		this.rewardType = rewardType;
	}
	
}
