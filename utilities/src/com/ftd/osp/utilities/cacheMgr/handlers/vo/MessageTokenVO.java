package com.ftd.osp.utilities.cacheMgr.handlers.vo;
import java.lang.reflect.Field;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * The tokens will be based off of companyId and have three different token types
 * - Text: hardcoded text value
 * - Value: data value will be the result of the Xpath query (from the tokenValue) ran against the data document
 * - Function: data value will be the result of the transformation of the XSL stored in the token value against the data document
 */
public class MessageTokenVO
{
  private String tokenId;
  private String companyId;
  private String tokenType;
  private String tokenValue;
  private String defaultValue;
  
  public MessageTokenVO()
  {
  }
  
  public MessageTokenVO(String tokenId, String companyId, String tokenType, String tokenValue, String defaultValue)
  {
    setTokenId(tokenId);
    setCompanyId(companyId);
    setTokenType(tokenType);
    setTokenValue(tokenValue);
    setDefaultValue(defaultValue);
  }


  public void setTokenId(String tokenId)
  {
    this.tokenId = tokenId;
  }


  public String getTokenId()
  {
    return tokenId;
  }


  public void setCompanyId(String companyId)
  {
    this.companyId = companyId;
  }


  public String getCompanyId()
  {
    return companyId;
  }


  public void setTokenType(String tokenType)
  {
    this.tokenType = tokenType;
  }


  public String getTokenType()
  {
    return tokenType;
  }


  public void setTokenValue(String tokenValue)
  {
    this.tokenValue = tokenValue;
  }


  public String getTokenValue()
  {
    return tokenValue;
  }


  public void setDefaultValue(String defaultValue)
  {
    this.defaultValue = defaultValue;
  }


  public String getDefaultValue()
  {
    return defaultValue;
  }
}