package com.ftd.osp.utilities.cacheMgr.handlers.vo;


import com.ftd.osp.utilities.plugins.Logger;


/**

 */
public class ContentVO
{
    private String contentContext;
    private String contentName;
    private String contentFilter1;
    private String contentFilter2;
    private String contentTxt;
    private int hashcode;
    
    /**
     * The Default Key (calculated on demand) is the Content Context + Content Name
     */
    private String defaultKey;
    
    /**
     * The Filter 1 Key (calculated on demand) is the Content Context + Content Name + Filter1 
     */
    private String filter1Key;
    
    /**
     * The Filter 2 Key (calculated on init/change) is the Content Context + Content Name + Filter1 + Filter2
     */
    private String filter2Key;
    
    public ContentVO (String contentContext, String contentName, String contentFilter1, String contentFilter2, String contentTxt) {
        this.contentContext = contentContext;
        this.contentName = contentName;
        this.contentFilter1 = contentFilter1;
        this.contentFilter2 = contentFilter2;
        this.contentTxt = contentTxt;
        
        buildKeys();
    }
    
    /**
     * Builds the Key defining this Content Object.
     * Only the filter2Key is built by this method, as that key is used frequently. The 
     * alternate keys (filter1Key and defaultKey) are only utilized as fallbacks, so, those
     * are built on demand.
     */
    private void buildKeys() 
    {
        defaultKey = null;
        filter1Key = null;    
        filter2Key = buildKey(contentContext, contentName, contentFilter1, contentFilter2);
    }
    
    /**
   * Generates a Key by appending the fields with a | delimiter between them.
   * @param contentContext
   * @param contentName
   * @param contentFilter1
   * @param contentFilter2
   * @return
   */
    private String buildKey(String contentContext, String contentName, String contentFilter1, String contentFilter2) {
        StringBuilder keyBuilder = new StringBuilder(255);
        keyBuilder.append(contentContext);
        keyBuilder.append("|").append(contentName);
        keyBuilder.append("|").append(contentFilter1);
        keyBuilder.append("|").append(contentFilter2);
        return keyBuilder.toString();
    }

    //overwriting hashCode() of Object
    public int hashCode() {
        if(this.hashcode != 0) {
            return this.hashcode;
        } else {
            // hashcode = contentContext.hashCode() + contentName.hashCode();        
            StringBuilder hashCodeBuilder = new StringBuilder(contentContext).append("|").append(contentName);
            hashcode = hashCodeBuilder.toString().hashCode();
        }
        return hashcode;
    }
    
    //overwirting equals() of Ojbect
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        
        ContentVO obj = (ContentVO)o;

        if(obj.getContentContext() == null || !obj.getContentContext().equals(this.getContentContext()))
            return false;
        if(obj.getContentName() == null || !obj.getContentName().equals(this.getContentName()))
            return false;
        if(obj.getContentFilter1() != null && !obj.getContentFilter1().equals(this.getContentFilter1()))
            return false;     
        if(this.getContentFilter1() != null && !this.getContentFilter1().equals(obj.getContentFilter1()))
            return false;    
        if(obj.getContentFilter2() != null && !obj.getContentFilter2().equals(this.getContentFilter2()))
            return false;     
        if(this.getContentFilter2() != null && !this.getContentFilter2().equals(obj.getContentFilter2()))
            return false;             
        return true;
    }

    public void setContentContext(String contentContext) {
        this.contentContext = contentContext;
        buildKeys();
    }

    public String getContentContext() {
        return contentContext;
    }

    public void setContentName(String contentName) {
        this.contentName = contentName;
        buildKeys();
    }

    public String getContentName() {
        return contentName;
    }

    public void setContentFilter1(String contentFilter1) {
        this.contentFilter1 = contentFilter1;
        buildKeys();
    }

    public String getContentFilter1() {
        return contentFilter1;
    }

    public void setContentFilter2(String contentFilter2) {
        this.contentFilter2 = contentFilter2;
        buildKeys();
    }

    public String getContentFilter2() {
        return contentFilter2;
    }


    public void setContentTxt(String contentTxt) {
        this.contentTxt = contentTxt;
    }

    public String getContentTxt() {
        return contentTxt;
    }
    
    public String getDefaultKey() {
      if(defaultKey == null) {
        defaultKey = buildKey(contentContext, contentName, null, null);      
      }
    
      return defaultKey;  
    }
    
    public String getFilter1Key() {
      if(filter1Key == null) {
        filter1Key = buildKey(contentContext, contentName, contentFilter1, null);
      }
      
      return filter1Key;
    }
    
    public String getFilter2Key() {
      return filter2Key;
    }
}
