package com.ftd.osp.utilities.cacheMgr.handlers;

import com.ftd.osp.utilities.cacheMgr.constant.CacheMgrConstants;
import com.ftd.osp.utilities.cacheMgr.dao.CacheMgrDAO;
import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.ColorMasterVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.CompanyMasterVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.ProductAttrRestrVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.SourceMasterVO;
import com.ftd.osp.utilities.cacheMgr.vo.CacheHandlerBase;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * This class is responsible for loading product attribute restrictions into cache.
 * key: product_attr_restr_id
 * value: ProductAttrRestrVO
 * 
 * key: source_code
 * List<String> of product_attr_restr_id
 */
public class ProductAttrHandler extends CacheHandlerBase
{
    private Map objMap;
    private Map sourceMap;
    private String KEY_SOURCE_PRODUCT_ATTR_RESTR_MAP = "KEY_SOURCE_PRODUCT_ATTR_RESTR_MAP";
    public Object load(Connection conn) throws CacheException {
        logger = new Logger(this.getClass().getName());
        CacheMgrDAO osdao = new CacheMgrDAO();
        CachedResultSet rs = osdao.executeQuery(conn, CacheMgrConstants.STMT_GET_PRODUCT_ATTR_RESTR);

        Map handlerMap = new HashMap();
        Map productAttrRestrMap = new HashMap();
        Map sourceProductAttrRestrMap = new HashMap();

        if(logger.isInfoEnabled())
          logger.info("Loaded product attribute restriction...");
        
        while (rs != null && rs.next()) {
            ProductAttrRestrVO paVO = new ProductAttrRestrVO();
            paVO.setProductAttrRestrId(rs.getString("product_attr_restr_id"));
            paVO.setProductAttrRestrOper(rs.getString("product_attr_restr_oper"));
            paVO.setProductAttrRestrValue(rs.getString("product_attr_restr_value"));
            paVO.setJavaMethodName(rs.getString("java_method_name"));

            productAttrRestrMap.put(paVO, paVO);
        }
        
        
        if(logger.isInfoEnabled())
          logger.info("Loaded source product attribute restriction...");
        // Get product attr restr source exclusion
        rs = osdao.executeQuery(conn, CacheMgrConstants.STMT_GET_PRODUCT_ATTR_RESTR_SOURCE);
        String sourceCode = null;
        String productAttrRestId = null;
        String key = null;
        String prevKey = null;
        List<String> attrList = null;
        while (rs != null && rs.next()) {
            sourceCode = rs.getString("source_code");
            productAttrRestId = rs.getString("product_attr_restr_id");

            key = sourceCode;

            if(!key.equals(prevKey)) {
                if(prevKey != null) {
                    sourceProductAttrRestrMap.put(prevKey, attrList);
                } 
                attrList = new ArrayList();
                
            }
           
            attrList.add(productAttrRestId);
            prevKey = key;
        }
        if(prevKey != null) {
            sourceProductAttrRestrMap.put(prevKey, attrList);
        }
        
        handlerMap.put(CacheMgrConstants.CACHE_KEY_ID_GEN, productAttrRestrMap);
        handlerMap.put(KEY_SOURCE_PRODUCT_ATTR_RESTR_MAP, sourceProductAttrRestrMap);
        return handlerMap;
    }
    
    /**
     * Set the cached object in the cache handler. The cache handler is then 
     * responsible for fulfilling all application level API calls, to access the
     * data in the cached object.
     * 
     * @param cachedObject
     * @throws com.ftd.osp.utilities.cache.exception.CacheException
     */    
      public void setCachedObject(Object cachedObject) throws CacheException
      {
        try 
        {
            this.objMap = (Map)((Map) cachedObject).get(CacheMgrConstants.CACHE_KEY_ID_GEN);
            this.sourceMap = (Map)((Map) cachedObject).get(KEY_SOURCE_PRODUCT_ATTR_RESTR_MAP);
        } catch (Exception ex) 
        {
          super.logger.error(ex);
          throw new CacheException("Could not set the cached object.");
        } 
      }
      
      public List<String> getProductAttrRestrBySource(String sourceCode) throws CacheException
      {
          String key = sourceCode;
          List<String> value = null;
          
          Object cachedObj = this.sourceMap;
          if (cachedObj == null) {
                throw new CacheException("Cached object is null.");
          }
          
          Object obj = this.sourceMap.get(key);

          if(obj != null) {
                value = Collections.unmodifiableList((List<String>)obj);     
          } 
          return value;
      }
      
      public ProductAttrRestrVO getProductAttrRestrById(String productAttrRestrId) throws CacheException
      {
          ProductAttrRestrVO key = new ProductAttrRestrVO(productAttrRestrId);
          ProductAttrRestrVO value = null;
          
          Object cachedObj = this.objMap;
          if (cachedObj == null) {
                throw new CacheException("Cached object is null.");
          }
          
          Object obj = this.objMap.get(key);

          if(obj != null) {
              value = (ProductAttrRestrVO)obj;     
          } 
          return value;
      }
      
      public Map getProductAttrRestrMap() throws CacheException
      {
          return this.objMap;
      }

}
