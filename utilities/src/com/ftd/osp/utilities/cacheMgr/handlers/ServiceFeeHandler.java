package com.ftd.osp.utilities.cacheMgr.handlers;

import com.ftd.osp.utilities.cacheMgr.constant.CacheMgrConstants;
import com.ftd.osp.utilities.cacheMgr.dao.CacheMgrDAO;
import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.ServiceFeeVO;
import com.ftd.osp.utilities.cacheMgr.vo.CacheHandlerBase;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;

import java.util.HashMap;
import java.util.Map;


/**
 * This class is responsible for loading service fee into cache.
 * key: snhId
 * value: ServiceFeeVO
 */
public class ServiceFeeHandler extends CacheHandlerBase
{
    private Map objMap;
    public Object load(Connection conn) throws CacheException {
        logger = new Logger(this.getClass().getName());
        CacheMgrDAO osdao = new CacheMgrDAO();
        CachedResultSet rs = osdao.executeQuery(conn, CacheMgrConstants.STMT_GET_SERVICE_FEE);

        Map handlerMap = new HashMap();
        Map serviceFeeMap = new HashMap();

        if(logger.isInfoEnabled())
          logger.info("Loading service fees...");
        while (rs != null && rs.next()) {

            ServiceFeeVO sfvo = new ServiceFeeVO();
            sfvo.setSnhId(rs.getString("snh_id"));
            sfvo.setDomesticCharge(rs.getBigDecimal("first_order_domestic"));
            sfvo.setInternationalCharge(rs.getBigDecimal("first_order_international"));
            sfvo.setVendorCharge(rs.getBigDecimal("vendor_charge"));
            sfvo.setVendorSatUpcharge(rs.getBigDecimal("vendor_sat_upcharge"));
            sfvo.setSameDayUpcharge(rs.getBigDecimal("same_day_upcharge"));

            serviceFeeMap.put(sfvo, sfvo);
        }

        handlerMap.put(CacheMgrConstants.CACHE_KEY_ID_GEN, serviceFeeMap);
        return handlerMap;
    }

    /**
     * Set the cached object in the cache handler. The cache handler is then
     * responsible for fulfilling all application level API calls, to access the
     * data in the cached object.
     *
     * @param cachedObject
     * @throws com.ftd.osp.utilities.cache.exception.CacheException
     */
      public void setCachedObject(Object cachedObject) throws CacheException
      {
        try
        {
            this.objMap = (Map)((Map) cachedObject).get(CacheMgrConstants.CACHE_KEY_ID_GEN);
        } catch (Exception ex)
        {
          super.logger.error(ex);
          throw new CacheException("Could not set the cached object.");
        }
      }

      public ServiceFeeVO getServiceFeeById(String snhId) throws CacheException
      {
    	  ServiceFeeVO key = new ServiceFeeVO(snhId);
          ServiceFeeVO value = null;

          Object cachedObj = this.objMap;
          if (cachedObj == null) {
                throw new CacheException("Cached object is null.");
          }
          Object obj = this.objMap.get(key);

          if(obj != null) {
                value = ((ServiceFeeVO)obj).clone();
          }
          return value;
      }


}
