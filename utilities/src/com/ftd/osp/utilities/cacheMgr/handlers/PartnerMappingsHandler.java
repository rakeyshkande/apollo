package com.ftd.osp.utilities.cacheMgr.handlers;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.cacheMgr.vo.CacheHandlerBase;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.vo.PartnerMappingVO;

/**
 * A cache handler is responsible for the following:
 * 
 * 1. Act as a cache loader, to load the data that needs to be cached. 2.
 * Provide application specific API, to enable access to the cached data.
 * 
 */

public class PartnerMappingsHandler extends CacheHandlerBase {
	// set by the setCachedObject method
	private Map<String, PartnerMappingVO> partnerMapping;
	private static final String GET_PARTNER_MAPPINGS = "GET_PARTNER_MAPPINGS";

	public PartnerMappingsHandler() {
	}

	/**
	 * Returns the object that needs to be cached.
	 * 
	 * The cache cannot be configured as distributed, if the cache handler
	 * returns a custom object. In order to utilize the benefits of a
	 * distributed cache, the return objects should only be a part JDK API.
	 * 
	 * @param con
	 * @return the object to be cached
	 * @throws com.ftd.osp.utilities.cache.exception.CacheException
	 */
	public Object load(Connection con) throws CacheException {
		Map<String, PartnerMappingVO> partnerMappingCacheObject = new HashMap<String, PartnerMappingVO>();
		try {
			super.logger.debug("BEGIN LOADING PARTNER MAPPING HANDLER...");
			Map<String, PartnerMappingVO> mappings = this.loadPartnerMappings(con);
			partnerMappingCacheObject.putAll(mappings);
			super.logger.debug("END LOADING PARTNER MAPPING HANDLER...");
		} catch (Exception e) {
			super.logger.error(e);
			throw new CacheException("Could not load Partners Mapping cache.");
		}

		logger.debug(partnerMappingCacheObject.size() + " records loaded");
		return partnerMappingCacheObject;
	}

	/**Loads the partner mapping detail. 
	 * @param conn
	 * @return
	 */
	private Map<String, PartnerMappingVO> loadPartnerMappings(Connection conn) {
		Map<String, PartnerMappingVO> map = new HashMap<String, PartnerMappingVO>();
		try {
			DataRequest request = new DataRequest();
			request.reset();
			request.setConnection(conn);
			Map<String, Object> inputParams = new HashMap<String, Object>();
			request.setInputParams(inputParams);
			request.setStatementID(GET_PARTNER_MAPPINGS);
			CachedResultSet crs = (CachedResultSet) DataAccessUtil.getInstance().execute(request);

			while (crs.next()) {
				PartnerMappingVO channelMappingVO =  populatePartnerMappingVO(crs);
				map.put(channelMappingVO.getPartnerId(), channelMappingVO);
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

		return map;

	}

	/**
	 * @param crs
	 */
	public PartnerMappingVO populatePartnerMappingVO(CachedResultSet crs) {	
		PartnerMappingVO channelMappingVO = new PartnerMappingVO();
		channelMappingVO.setPartnerId(crs.getString("PARTNER_ID"));
		channelMappingVO.setPartnerName(crs.getString("PARTNER_NAME"));
		channelMappingVO.setPartnerImage(crs.getString("PARTNER_IMAGE"));
		
		channelMappingVO.setFtdOriginMapping(crs.getString("FTD_ORIGIN_MAPPING"));
		channelMappingVO.setMasterOrderNoPrefix(crs.getString("MASTER_ORDER_NO_PREFIX"));
		channelMappingVO.setConfNumberPrefix(crs.getString("CONF_NUMBER_PREFIX"));
		
		channelMappingVO.setDefaultSourceCode(crs.getString("DEFAULT_SOURCE_CODE"));
		channelMappingVO.setDefaultRecalcSourceCode(crs.getString("DEFAULT_RECALC_SOURCE_CODE"));
		
		channelMappingVO.setBillingAddressLine1(crs.getString("BILLING_ADDRESS_LINE_1"));
		channelMappingVO.setBillingAddressLine2(crs.getString("BILLING_ADDRESS_LINE_2"));
		channelMappingVO.setBillingAddressCity(crs.getString("BILLING_CITY"));
		channelMappingVO.setBillingAddressState(crs.getString("BILLING_STATE_PROVINCE"));
		channelMappingVO.setBillingAddressZipCode(crs.getString("BILLING_POSTAL_CODE"));
		channelMappingVO.setBillingAddressCountry(crs.getString("BILLING_COUNTRY"));

		channelMappingVO.setSatDeliveryAllowedDropship(crs.getString("SAT_DELIVERY_ALLOWED_DROPSHIP"));
		channelMappingVO.setSatDeliveryAllowedFloral(crs.getString("SAT_DELIVERY_ALLOWED_FLORAL"));
		channelMappingVO.setSunDeliveryAllowedFloral(crs.getString("SUN_DELIVERY_ALLOWED_FLORAL"));

		channelMappingVO.setSendFloristPdbPrice(crs.getString("SEND_FLORIST_PDB_PRICE"));
		channelMappingVO.setSendOrdConfEmail(crs.getString("SEND_ORD_CONF_EMAIL"));
		channelMappingVO.setSendShipConfEmail(crs.getString("SEND_SHIP_CONF_EMAIL"));
		channelMappingVO.setCreateStockEmail(crs.getString("CREATE_STOCK_EMAIL"));
		channelMappingVO.setSendDelConfEmail(crs.getString("SEND_DELIVERY_CONF_EMAIL"));
		
		channelMappingVO.setPerformDropshipAVS("Y".equals(crs.getString("AVS_DROPSHIP")) ? true : false);
		channelMappingVO.setPerformFloristAVS("Y".equals(crs.getString("AVS_FLORAL")) ? true : false);
		
		channelMappingVO.setSendDconFeed("Y".equals(crs.getString("SEND_DCON_FEED")) ? true : false);
		channelMappingVO.setSendAdjustmentFeed("Y".equals(crs.getString("SEND_ORD_ADJ_FEED")) ? true : false);
		channelMappingVO.setSendFulfillmentFeed("Y".equals(crs.getString("SEND_ORD_FMT_FEED")) ? true : false);
		channelMappingVO.setSendOrdStatusUpdFeed("Y".equals(crs.getString("SEND_ORD_STATUS_UPDATE")) ? true : false);
		channelMappingVO.setSendShipStatusUpdFeed("Y".equals(crs.getString("SEND_SHIP_STATUS_UPDATE")) ? true : false);
		channelMappingVO.setSendInvoiceFeed("Y".equals(crs.getString("SEND_ORD_INVOICE")) ? true : false);
		channelMappingVO.setModifyOrderAllowed("Y".equals(crs.getString("UPDATE_ORDER_FLAG")) ? true : false);
		channelMappingVO.setDefaultBehaviour("Y".equals(crs.getString("APPLY_DEF_PTN_BEHAVIOR")) ? true : false);
		channelMappingVO.setisIncrTotalAllowed("Y".equals(crs.getString("is_incr_total_allowed")) ? true : false);
		
		
		return channelMappingVO;
	}

	/**
	 * Set the cached object in the cache handler. The cache handler is then
	 * responsible for fulfilling all application level API calls, to access the
	 * data in the cached object.
	 * 
	 * @param cachedObject
	 * @throws com.ftd.osp.utilities.cache.exception.CacheException
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void setCachedObject(Object cachedObject) throws CacheException {
		try {
			this.partnerMapping = (Map) cachedObject;
		} catch (Exception ex) {
			super.logger.error(ex);
			throw new CacheException("Could not set the cached object.");
		}
	}

	public List<String> getPartnerIds() {
		return new ArrayList<String>(this.partnerMapping.keySet());
	}

	public PartnerMappingVO getPartnerMappingVO(String partnerId) {
		return this.partnerMapping.get(partnerId);
	}

	public Map<String, PartnerMappingVO> getPartnerMappings() {
		return this.partnerMapping;
	}
}