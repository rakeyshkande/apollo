package com.ftd.osp.utilities.cacheMgr.handlers;

import com.ftd.osp.utilities.cacheMgr.constant.CacheMgrConstants;
import com.ftd.osp.utilities.cacheMgr.dao.CacheMgrDAO;
import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.ColorMasterVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.CompanyMasterVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.ProductAttrRestrVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.SourceMasterVO;
import com.ftd.osp.utilities.cacheMgr.vo.CacheHandlerBase;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;

import java.math.BigDecimal;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * This class is responsible for fuel into cache.
 */
public class FuelSurchargeHandler extends CacheHandlerBase
{
    private BigDecimal obj;

    public Object load(Connection conn) throws CacheException {
        logger = new Logger(this.getClass().getName());
        CacheMgrDAO osdao = new CacheMgrDAO();
        CachedResultSet rs = osdao.executeQuery(conn, CacheMgrConstants.STMT_GET_FUEL_SURCHARGE);

        Map handlerMap = new HashMap();
        Map fuelSurchargeMap = new HashMap();
        if(logger.isInfoEnabled())
          logger.info("Loaded fuel surchareg...");
        
        BigDecimal fuelSurcharge = null;

        if (rs.next()) {   
            fuelSurcharge = rs.getBigDecimal("fuel_surcharge_amt");
        }
        handlerMap.put(CacheMgrConstants.CACHE_KEY_ID_GEN, fuelSurcharge);
        return handlerMap;
    }
    
    /**
     * Set the cached object in the cache handler. The cache handler is then 
     * responsible for fulfilling all application level API calls, to access the
     * data in the cached object.
     * 
     * @param cachedObject
     * @throws com.ftd.osp.utilities.cache.exception.CacheException
     */    
      public void setCachedObject(Object cachedObject) throws CacheException
      {
        try 
        {
            this.obj = (BigDecimal)((Map) cachedObject).get(CacheMgrConstants.CACHE_KEY_ID_GEN);
        } catch (Exception ex) 
        {
          super.logger.error(ex);
          throw new CacheException("Could not set the cached object.");
        } 
      }
      
      public BigDecimal getFuelSurcharge() throws CacheException
      {         
          return this.obj;
      }
      

}
