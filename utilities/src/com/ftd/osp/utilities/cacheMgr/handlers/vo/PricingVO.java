package com.ftd.osp.utilities.cacheMgr.handlers.vo;


import com.ftd.osp.utilities.cacheMgr.handlers.enums.PriceTypeEnum;

import java.math.BigDecimal;


public class PricingVO
{
    private PriceTypeEnum priceType;
    private BigDecimal priceAmount;
    private BigDecimal origPriceAmount;
    private String discountType;
    private BigDecimal discountAmount;
    private BigDecimal percentOff;
    private String rewardType;
    private BigDecimal rewardAmount;
    private BigDecimal redemptionAmount;

    public void setPriceAmount(BigDecimal priceAmount) {
        this.priceAmount = priceAmount;
    }

    public BigDecimal getPriceAmount() {
        return priceAmount;
    }

    public void setDiscountAmount(BigDecimal discountAmount) {
        this.discountAmount = discountAmount;
    }

    public BigDecimal getDiscountAmount() {
        return discountAmount;
    }

    public void setRewardType(String rewardType) {
        this.rewardType = rewardType;
    }

    public String getRewardType() {
        return rewardType;
    }

    public void setRewardAmount(BigDecimal rewardAmount) {
        this.rewardAmount = rewardAmount;
    }

    public BigDecimal getRewardAmount() {
        return rewardAmount;
    }

    public void setRedemptionAmount(BigDecimal redemptionAmount) {
        this.redemptionAmount = redemptionAmount;
    }

    public BigDecimal getRedemptionAmount() {
        return redemptionAmount;
    }

    public void setDiscountType(String discountType) {
        this.discountType = discountType;
    }

    public String getDiscountType() {
        return discountType;
    }

    public void setPercentOff(BigDecimal percentOff) {
        this.percentOff = percentOff;
    }

    public BigDecimal getPercentOff() {
        return percentOff;
    }

    public void setPriceType(PriceTypeEnum priceType) {
        this.priceType = priceType;
    }

    public PriceTypeEnum getPriceType() {
        return priceType;
    }

    public void setOrigPriceAmount(BigDecimal origPriceAmount) {
        this.origPriceAmount = origPriceAmount;
    }

    public BigDecimal getOrigPriceAmount() {
        return origPriceAmount;
    }

    public PricingVO clone() {
        PricingVO price = new PricingVO();
        price.setPriceType(this.getPriceType());
        price.setPriceAmount(this.getPriceAmount());
        price.setOrigPriceAmount(this.getOrigPriceAmount());
        price.setDiscountType(this.getDiscountType());
        price.setDiscountAmount(this.getDiscountAmount());
        price.setPercentOff(this.getPercentOff());
        price.setRewardType(this.getRewardType());
        price.setRewardAmount(this.getRewardAmount());
        price.setRedemptionAmount(this.getRedemptionAmount());

        return price;
    }

}
