package com.ftd.osp.utilities.cacheMgr.handlers;

import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.PostalCodeVO;
import com.ftd.osp.utilities.cacheMgr.vo.CacheHandlerBase;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;

import java.sql.Connection;

import java.util.HashMap;
import java.util.Map;
/**
 * A cache handler is responsible for the following:
 * 
 * 1. Act as a cache loader, to load the data that needs to be cached.
 * 2. Provide application specific API, to enable access to the cached data.
 * 
 */

public class PostalCodeHandler extends CacheHandlerBase 
{
    // set by the setCachedObject method
    private Map zipcodeById;
    private static final String GET_ZIP_CODE = "GET_ZIP_CODE";

    public PostalCodeHandler()
    {
    }

   /**
   * Returns the object that needs to be cached.
   * 
   * The cache cannot be configured as distributable, if the cache handler 
   * returns a custom object. In order to utilize the benefits of a distributable 
   * cache, the return objects should only be a part JDK API.
   * 
   * @param con
   * @return the object to be cached
   * @throws com.ftd.osp.utilities.cache.exception.CacheException
   */
    public Object load(Connection con) throws CacheException
    {
        Map postalCodeHandlerCacheObject = new HashMap();
        try
        {
            super.logger.debug("BEGIN LOADING POSTAL CODE HANDLER...");
            
            DataAccessUtil dau = DataAccessUtil.getInstance();
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(con);
            dataRequest.setStatementID(GET_ZIP_CODE);
            CachedResultSet rs = (CachedResultSet)dau.execute(dataRequest);

            PostalCodeVO postalCodeVO = null;
            while(rs.next())
            {
                postalCodeVO = new PostalCodeVO();
                postalCodeVO.setZipcode((String) rs.getObject(1));
                postalCodeVO.setCity((String) rs.getObject(2));
                postalCodeVO.setState((String) rs.getObject(3));
                
                postalCodeHandlerCacheObject.put(postalCodeVO.getZipcode(), postalCodeVO);
            }
            super.logger.debug("END LOADING POSTAL CODE HANDLER...");
        }
        catch(Exception e)
        {
            super.logger.error(e);
            throw new CacheException("Could not load Postal Code cache.");
        }
        
        logger.debug(postalCodeHandlerCacheObject.size() + " records loaded");
        return postalCodeHandlerCacheObject;
    }


  /**
   * Set the cached object in the cache handler. The cache handler is then 
   * responsible for fulfilling all application level API calls, to access the
   * data in the cached object.
   * 
   * @param cachedObject
   * @throws com.ftd.osp.utilities.cache.exception.CacheException
   */    
    public void setCachedObject(Object cachedObject) throws CacheException
    {
      try 
      {
        this.zipcodeById = (Map) cachedObject;  
      } catch (Exception ex) 
      {
        super.logger.error(ex);
        throw new CacheException("Could not set the cached object.");
      } 
    }

  /**
   * 
   * @param postalCode
   * @return 
   */
    public boolean postalCodeExists(String postalCode)
    {
        return (this.zipcodeById.containsKey(postalCode))?true:false;
    }
}