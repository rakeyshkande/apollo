package com.ftd.osp.utilities.cacheMgr.handlers.vo;

import java.util.ArrayList;
import java.util.List;

/**
 * This class models a product upgrade price point, such as a floral product with multiple price point, a floral product
 * with good, better, or best display, or an upsell associated SKU.
 */
public class GBBVO
{
    private String productId;
    private String webProductId;
    private String defaultFlag;
    private String displaySequence;
    private String gbbNameOverrideTxt;
    private String gbbPriceOverrideTxt;
    //private String gbbSizeLabel; // For floral product, this is the labels displayed in PDB "Price & Personalization" section by price;
                             // for upsell, this is the HTML editable text in "Associated SKU's" section.
    private List<ImageVO> imageList;
    private List<PricingVO> pricingList;


    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductId() {
        return productId;
    }

    public void setWebProductId(String webProductId) {
        this.webProductId = webProductId;
    }

    public String getWebProductId() {
        return webProductId;
    }

    public void setDefaultFlag(String defaultFlag) {
        this.defaultFlag = defaultFlag;
    }

    public String getDefaultFlag() {
        return defaultFlag;
    }

    public void setGbbNameOverrideTxt(String gbbNameOverrideTxt) {
        this.gbbNameOverrideTxt = gbbNameOverrideTxt;
    }

    public String getGbbNameOverrideTxt() {
        return gbbNameOverrideTxt;
    }

    public void setGbbPriceOverrideTxt(String gbbPriceOverrideTxt) {
        this.gbbPriceOverrideTxt = gbbPriceOverrideTxt;
    }

    public String getGbbPriceOverrideTxt() {
        return gbbPriceOverrideTxt;
    }


    public void setDisplaySequence(String displaySequence) {
        this.displaySequence = displaySequence;
    }

    public String getDisplaySequence() {
        return displaySequence;
    }

    public void setImageList(List<ImageVO> imageList) {
        this.imageList = imageList;
    }

    public List<ImageVO> getImageList() {
        return imageList;
    }

    public void setPricingList(List<PricingVO> pricingList) {
        this.pricingList = pricingList;
    }

    public List<PricingVO> getPricingList() {
        return pricingList;
    }

    public GBBVO clone() {
        GBBVO gbb = new GBBVO();

        gbb.setProductId(this.getProductId());
        gbb.setWebProductId(this.getWebProductId());
        gbb.setDefaultFlag(this.getDefaultFlag());
        gbb.setDisplaySequence(this.getDisplaySequence());
        gbb.setGbbNameOverrideTxt(this.getGbbNameOverrideTxt());
        gbb.setGbbPriceOverrideTxt(this.getGbbPriceOverrideTxt());

        List imageList = new ArrayList<ImageVO>();
        List origImageList = this.getImageList();
        for (int i = 0; i < origImageList.size(); i++){
            ImageVO image = ((ImageVO)origImageList.get(i)).clone();
            imageList.add(image);
        }
        gbb.setImageList(imageList);

        List priceList = new ArrayList<PricingVO>();
        List origPriceList = this.getPricingList();
        for (int i = 0; i < origPriceList.size(); i++){
            PricingVO price = ((PricingVO)origPriceList.get(i)).clone();
            priceList.add(price);
        }
        gbb.setPricingList(priceList);

        return gbb;
    }
}
