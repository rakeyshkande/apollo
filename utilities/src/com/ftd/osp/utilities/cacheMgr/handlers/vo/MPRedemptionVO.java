package com.ftd.osp.utilities.cacheMgr.handlers.vo;


import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**

 */
public class MPRedemptionVO
{
    private BigDecimal mpRedemptionRateAmt;
    private String paymentMethodId;
    private String dollorToMPOperator;
    private String roundingMethodId;
    private String roundingScale;
    private String mpRedemptionRateId;
    
	public BigDecimal getMpRedemptionRateAmt() {
		return mpRedemptionRateAmt;
	}
	public void setMpRedemptionRateAmt(BigDecimal mpRedemptionRateAmt) {
		this.mpRedemptionRateAmt = mpRedemptionRateAmt;
	}
	public String getPaymentMethodId() {
		return paymentMethodId;
	}
	public void setPaymentMethodId(String paymentMethodId) {
		this.paymentMethodId = paymentMethodId;
	}
	public String getDollorToMPOperator() {
		return dollorToMPOperator;
	}
	public void setDollorToMPOperator(String dollorToMPOperator) {
		this.dollorToMPOperator = dollorToMPOperator;
	}
	public String getRoundingMethodId() {
		return roundingMethodId;
	}
	public void setRoundingMethodId(String roundingMethodId) {
		this.roundingMethodId = roundingMethodId;
	}
	public String getRoundingScale() {
		return roundingScale;
	}
	public void setRoundingScale(String roundingScale) {
		this.roundingScale = roundingScale;
	}
	public String getMpRedemptionRateId() {
		return mpRedemptionRateId;
	}
	public void setMpRedemptionRateId(String mpRedemptionRateId) {
		this.mpRedemptionRateId = mpRedemptionRateId;
	}

}
