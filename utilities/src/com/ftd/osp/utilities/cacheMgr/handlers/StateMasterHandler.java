package com.ftd.osp.utilities.cacheMgr.handlers;

import com.ftd.osp.utilities.cacheMgr.constant.CacheMgrConstants;
import com.ftd.osp.utilities.cacheMgr.dao.CacheMgrDAO;
import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.StateMasterVO;
import com.ftd.osp.utilities.cacheMgr.vo.CacheHandlerBase;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * This class is responsible for loading state_master recrods into cache.
 * key: ALL
 * value: StateMasterVO
 */
public class StateMasterHandler extends CacheHandlerBase
{
    private List stateList;
    private Map stateMap;
    private Map statesByName;
    private Map statesById;
    private static final String STATES_BY_NAME_KEY = "STATES_BY_NAME_KEY";    
    private static final String STATES_BY_ID_KEY = "STATES_BY_ID_KEY";

    public Object load(Connection conn) throws CacheException{
        logger = new Logger(this.getClass().getName());
        CacheMgrDAO osdao = new CacheMgrDAO();
        CachedResultSet rs = osdao.executeQuery(conn, CacheMgrConstants.STMT_GET_STATE_MASTER);
        List<StateMasterVO> stateList = new ArrayList();
        Map handlerMap = new HashMap();
        Map cMap = new HashMap();
        Map statesByNameCacheObject = new HashMap();
        Map statesByIdCacheObject = new HashMap();
        StateMasterVO smvo = null;

        
        while (rs != null && rs.next()) {
            smvo = new StateMasterVO();
            smvo.setStateMasterId(rs.getString("state_master_id"));
            smvo.setStateName(rs.getString("state_name"));
            smvo.setCountryCode(rs.getString("country_code"));
            smvo.setTimeZone(rs.getString("time_zone"));
            smvo.setDeliveryExclusionFlag(rs.getString("delivery_exclusion"));
            smvo.setDropShipAvailableFlag(rs.getString("drop_ship_available_flag"));
            stateList.add(smvo);
            statesByNameCacheObject.put(smvo.getStateName(), smvo);
            statesByIdCacheObject.put(smvo.getStateMasterId(), smvo);
            cMap.put(smvo, smvo);
        }
        handlerMap.put(CacheMgrConstants.CACHE_KEY_ALL, stateList);
        handlerMap.put(CacheMgrConstants.CACHE_KEY_ID_GEN, cMap);
        handlerMap.put(this.STATES_BY_NAME_KEY, statesByNameCacheObject);
        handlerMap.put(STATES_BY_ID_KEY, statesByIdCacheObject);
        return handlerMap;
    }
    
         /**
          * Set the cached object in the cache handler. The cache handler is then 
          * responsible for fulfilling all application level API calls, to access the
          * data in the cached object.
          * 
          * @param cachedObject
          * @throws com.ftd.osp.utilities.cache.exception.CacheException
          */    
           public void setCachedObject(Object cachedObject) throws CacheException
           {
             try 
             {
               this.statesById = (Map)((Map) cachedObject).get(this.STATES_BY_ID_KEY);  
               this.stateList = (ArrayList)((Map) cachedObject).get(CacheMgrConstants.CACHE_KEY_ALL);  
               this.stateMap = (Map)((Map) cachedObject).get(CacheMgrConstants.CACHE_KEY_ID_GEN);  
               this.statesByName = (Map)((Map) cachedObject).get(this.STATES_BY_NAME_KEY);  
             } catch (Exception ex) 
             {
               super.logger.error(ex);
               throw new CacheException("Could not set the cached object.");
             } 
           }
           
           public List getStateMasterList() throws CacheException
           {
               return Collections.unmodifiableList(stateList);
           }
           
           public StateMasterVO getStateById(String stateMasterId) throws CacheException {
               StateMasterVO key = new StateMasterVO(stateMasterId);
               StateMasterVO value = null;
               
               Object cachedObj = this.stateMap;
               if (cachedObj == null) {
                     throw new CacheException("Cached object is null.");
               }
               Object obj = this.stateMap.get(key);

               if(obj != null) {
                     value = (StateMasterVO)obj;     
               } 
               return value;
           }
           
           /**
            * 
            * @param id
            * @return 
         * @throws CacheException 
            */
             public String getTimeZone(String id) throws CacheException
             {
            	 StateMasterVO sVO = getStateById(id);
            	 
                 return (sVO != null)? sVO.getTimeZone():null;
             }

             /**
              * 
              * @return 
              */
             public Map getStatesByNameMap()
             {
            	 return Collections.unmodifiableMap(statesByName);
             }
             
		 /**
		  * 
		  * @return 
		  */
		   public Map getStatesByIdMap()
		   {
		       return Collections.unmodifiableMap(statesById);
		   }
             
}

