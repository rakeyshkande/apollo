package com.ftd.osp.utilities.cacheMgr.handlers.vo;

import java.math.BigDecimal;


public class PriceHeaderDetailVO
{
    private String priceHeaderId;
    private BigDecimal minDollarAmt;
    private BigDecimal maxDollarAmt;
    private String discountType;
    private BigDecimal discountAmt;

    public void setPriceHeaderId(String priceHeaderId) {
        this.priceHeaderId = priceHeaderId;
    }

    public String getPriceHeaderId() {
        return priceHeaderId;
    }

    public void setMinDollarAmt(BigDecimal minDollarAmt) {
        this.minDollarAmt = minDollarAmt;
    }

    public BigDecimal getMinDollarAmt() {
        return minDollarAmt;
    }

    public void setMaxDollarAmt(BigDecimal maxDollarAmt) {
        this.maxDollarAmt = maxDollarAmt;
    }

    public BigDecimal getMaxDollarAmt() {
        return maxDollarAmt;
    }

    public void setDiscountType(String discountType) {
        this.discountType = discountType;
    }

    public String getDiscountType() {
        return discountType;
    }

    public void setDiscountAmt(BigDecimal discountAmt) {
        this.discountAmt = discountAmt;
    }

    public BigDecimal getDiscountAmt() {
        return discountAmt;
    }
    
}
