package com.ftd.osp.utilities.cacheMgr.handlers;

import com.ftd.osp.utilities.cacheMgr.constant.CacheMgrConstants;
import com.ftd.osp.utilities.cacheMgr.dao.CacheMgrDAO;
import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.CountryMasterVO;
import com.ftd.osp.utilities.cacheMgr.vo.CacheHandlerBase;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;

import java.math.BigDecimal;
import java.sql.Connection;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * This class is responsible for loading country into cache.
 * key: ALL
 * value: List<CountryMasterVO>
 */
public class CountryMasterHandler extends CacheHandlerBase
{
    private List countryList;
    private Map countryMap;
    public Object load(Connection conn) throws CacheException{
        logger = new Logger(this.getClass().getName());
        CacheMgrDAO osdao = new CacheMgrDAO();
        CachedResultSet rs = osdao.executeQuery(conn, CacheMgrConstants.STMT_GET_COUNTRY_MASTER);
        List<CountryMasterVO> countryList = new ArrayList();
        Map handlerMap = new HashMap();
        Map cMap = new HashMap();
        CountryMasterVO countryVo = null;

        NumberFormat nf = new DecimalFormat("0000");
        String countryId = null;
        String countryDesc = null;
        Object displayOrder = null;
        String countryType = null;
        String mondayClosed = null;
        String tuesdayClosed = null;
        String wednesdayClosed = null;
        String thursdayClosed = null;
        String fridayClosed = null;
        String saturdayClosed = null;
        String sundayClosed = null;
        Object addonDays = null;
        String threeCharacterId = null;
        String status = null;
        Object cutoffTime = null;
        String mondayTransit = null;
        String tuesdayTransit = null;
        String wednesdayTransit = null;
        String thursdayTransit = null;
        String fridayTransit = null;
        String saturdayTransit = null;
        String sundayTransit = null;
        
        logger.info("Loading country master...");
        while (rs != null && rs.next()) {
            
            countryId = (String)rs.getObject("country_id");
            countryDesc = (String)rs.getObject("name");
            countryType = (String)rs.getObject("oe_country_type");
            
           
            displayOrder = rs.getObject("oe_display_order");
            if(displayOrder == null) displayOrder = new BigDecimal("999");

            addonDays = rs.getObject("add_on_days");
            if(addonDays == null)
            {
                addonDays = new BigDecimal("0");
            }
            cutoffTime = rs.getObject("cutoff_time");
            if (cutoffTime == null) {
                cutoffTime = new BigDecimal("0");
            }
            
            mondayClosed = (String)rs.getObject("monday_closed");
            tuesdayClosed = (String)rs.getObject("tuesday_closed");
            wednesdayClosed = (String)rs.getObject("wednesday_closed");
            thursdayClosed = (String)rs.getObject("thursday_closed");
            fridayClosed = (String)rs.getObject("friday_closed");
            saturdayClosed = (String)rs.getObject("saturday_closed");
            sundayClosed = (String)rs.getObject("sunday_closed");
            threeCharacterId = (String)rs.getObject("three_character_id");
            status = (String) rs.getObject("status");
            mondayTransit = (String)rs.getObject("monday_transit_flag");
            tuesdayTransit = (String)rs.getObject("tuesday_transit_flag");
            wednesdayTransit = (String)rs.getObject("wednesday_transit_flag");
            thursdayTransit = (String)rs.getObject("thursday_transit_flag");
            fridayTransit = (String)rs.getObject("friday_transit_flag");
            saturdayTransit = (String)rs.getObject("saturday_transit_flag");
            sundayTransit = (String)rs.getObject("sunday_transit_flag");
            
        
            if(countryId != null)
            {
                countryId = countryId.toUpperCase();
                countryDesc = countryDesc.toUpperCase();

                countryVo = new CountryMasterVO();
                countryVo.setCountryId(countryId);
                countryVo.setCountryType(countryType);
                countryVo.setDisplayOrder((BigDecimal)displayOrder);
                countryVo.setCountryName(countryDesc);
                countryVo.setMondayClosed(mondayClosed);
                countryVo.setTuesdayClosed(tuesdayClosed);
                countryVo.setWednesdayClosed(wednesdayClosed);
                countryVo.setThursdayClosed(thursdayClosed);
                countryVo.setFridayClosed(fridayClosed);
                countryVo.setSaturdayClosed(saturdayClosed);
                countryVo.setSundayClosed(sundayClosed);
                countryVo.setAddonDays(((BigDecimal)addonDays).longValue());
                countryVo.setThreeCharacterId(threeCharacterId);
                countryVo.setStatus(status);
                countryVo.setCutoffTime(nf.format(((BigDecimal)cutoffTime).longValue()));
                countryVo.setMondayTransit(mondayTransit);
                countryVo.setTuesdayTransit(tuesdayTransit);
                countryVo.setWednesdayTransit(wednesdayTransit);
                countryVo.setThursdayTransit(thursdayTransit);
                countryVo.setFridayTransit(fridayTransit);
                countryVo.setSaturdayTransit(saturdayTransit);
                countryVo.setSundayTransit(sundayTransit);
                
                countryList.add(countryVo);
                cMap.put(countryVo, countryVo);
            }            
        }
        handlerMap.put(CacheMgrConstants.CACHE_KEY_ALL, countryList);
        handlerMap.put(CacheMgrConstants.CACHE_KEY_ID_GEN, cMap);
        return handlerMap;
    }
         /**
          * Set the cached object in the cache handler. The cache handler is then 
          * responsible for fulfilling all application level API calls, to access the
          * data in the cached object.
          * 
          * @param cachedObject
          * @throws com.ftd.osp.utilities.cache.exception.CacheException
          */    
           public void setCachedObject(Object cachedObject) throws CacheException
           {
             try 
             {
               this.countryList = (ArrayList)((Map) cachedObject).get(CacheMgrConstants.CACHE_KEY_ALL);  
               this.countryMap = (Map)((Map) cachedObject).get(CacheMgrConstants.CACHE_KEY_ID_GEN);  
             } catch (Exception ex) 
             {
               super.logger.error(ex);
               throw new CacheException("Could not set the cached object.");
             } 
           }
           
           public List getCountryList() throws CacheException
           {
               return Collections.unmodifiableList(countryList);
           }
           
           public CountryMasterVO getCountryById(String countryId) throws CacheException {
               CountryMasterVO value = null;
               if (countryId != null) {
                   CountryMasterVO key = new CountryMasterVO(countryId);
                   Object cachedObj = this.countryMap;
                   if (cachedObj == null) {
                       throw new CacheException("Cached object is null.");
                   }               
               
                   Object obj = this.countryMap.get(key);
       
                   if(obj != null) {
                       value = (CountryMasterVO)obj;     
                   } 
               }
               return value;
           }
           
           
           /**
            * Returns true if the country is in the cache, else false.
            * @param countryId
            * @throws CacheException
            */
           public boolean countryExists(String countryId) throws CacheException {
               CountryMasterVO country = getCountryById(countryId);
               return country==null ? false:true;
           }
}

