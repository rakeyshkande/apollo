package com.ftd.osp.utilities.cacheMgr.handlers.vo;

/**
 * This class models product index value object.
 */
public class ProductIndexVO
{
    private String indexName;
    private String indexDesc;
    private String sourceCode;
    private String countryId;
    private int hashcode;

    public void setIndexName(String indexName) {
        this.indexName = indexName;
    }

    public String getIndexName() {
        return indexName;
    }

    public void setIndexDesc(String indexDesc) {
        this.indexDesc = indexDesc;
    }

    public String getIndexDesc() {
        return indexDesc;
    }

    public void setSourceCode(String sourceCode) {
        this.sourceCode = sourceCode;
    }

    public String getSourceCode() {
        return sourceCode;
    }
    
    //overwriting hashCode() of Object
    public int hashCode() {
        if(this.hashcode != 0) {
            return this.hashcode;
        } else {
            // hashcode = indexName.hashCode() + sourceCode.hashCode();
            StringBuilder hashCodeBuilder = new StringBuilder(indexName).append("|").append(sourceCode);
            hashcode = hashCodeBuilder.toString().hashCode();
        }
        return hashcode;
    }
    //overwirting equals() of Ojbect
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        
        ProductIndexVO obj = (ProductIndexVO)o;

        if(obj.getIndexName() == null || !obj.getIndexName().equals(this.getIndexName()))
            return false;
        if(obj.getSourceCode() == null || !obj.getSourceCode().equals(this.getSourceCode()))
            return false;            
        return true;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getCountryId() {
        return countryId;
    }
}
