package com.ftd.osp.utilities.cacheMgr.handlers;

import com.ftd.osp.utilities.cacheMgr.constant.CacheMgrConstants;
import com.ftd.osp.utilities.cacheMgr.dao.CacheMgrDAO;
import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.ColorMasterVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.GlobalParmVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.PriceHeaderDetailVO;
import com.ftd.osp.utilities.cacheMgr.vo.CacheHandlerBase;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;

import com.ftd.osp.utilities.plugins.Logger;

import java.math.BigDecimal;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This class is responsible for loading price header into cache.
 * key: priceHeaderId
 * value: List<PriceHeaderDetailVO>
 */
public class PriceHeaderHandler extends CacheHandlerBase
{
    private Map objMap;
    public Object load(Connection conn) throws CacheException {
        logger = new Logger(this.getClass().getName());
        CacheMgrDAO osdao = new CacheMgrDAO();
        CachedResultSet rs = osdao.executeQuery(conn, CacheMgrConstants.STMT_GET_PRICE_HEADER_DETAIL);

        PriceHeaderDetailVO phvo = null;
        String priceHeaderId = null;
        String prevPriceHeaderId = null;
        List<PriceHeaderDetailVO> detailList = null;
        Map handlerMap = new HashMap();
        Map priceHeaderMap = new HashMap();
        
        if(rs != null) {
            while (rs != null && rs.next()) {
                priceHeaderId = rs.getString("price_header_id");
                phvo = new PriceHeaderDetailVO();
                phvo.setPriceHeaderId(priceHeaderId);
                phvo.setMinDollarAmt(rs.getBigDecimal("min_dollar_amt"));
                phvo.setMaxDollarAmt(rs.getBigDecimal("max_dollar_amt"));
                phvo.setDiscountType(rs.getString("discount_type"));
                phvo.setDiscountAmt(rs.getBigDecimal("discount_amt"));
                
                if(!priceHeaderId.equals(prevPriceHeaderId)) {
                    if(prevPriceHeaderId != null) {
                        priceHeaderMap.put(prevPriceHeaderId, detailList);
                    }
                    detailList = new ArrayList();
                }
                detailList.add(phvo);

                prevPriceHeaderId = priceHeaderId;
            }
            
            if(prevPriceHeaderId != null) {
                priceHeaderMap.put(prevPriceHeaderId, detailList);
            }
        }
        
        handlerMap.put(CacheMgrConstants.CACHE_KEY_ID_GEN, priceHeaderMap);
        return handlerMap;
    }
    
    /**
     * Set the cached object in the cache handler. The cache handler is then 
     * responsible for fulfilling all application level API calls, to access the
     * data in the cached object.
     * 
     * @param cachedObject
     * @throws com.ftd.osp.utilities.cache.exception.CacheException
     */    
      public void setCachedObject(Object cachedObject) throws CacheException
      {
        try 
        {
          this.objMap = (Map)((Map) cachedObject).get(CacheMgrConstants.CACHE_KEY_ID_GEN);  
        } catch (Exception ex) 
        {
          super.logger.error(ex);
          throw new CacheException("Could not set the cached object.");
        } 
      }
      
      public PriceHeaderDetailVO getPriceHeaderDetail(String priceHeaderId, BigDecimal price) throws CacheException
      {
          String key = priceHeaderId;
          
          Object cachedObj = this.objMap;
          if (cachedObj == null) {
                throw new CacheException("Cached object is null.");
          }
          // Retrieves a list of price header detail with the same price header id but different max and min.
          Object obj = this.objMap.get(key);
          PriceHeaderDetailVO priceHeaderDetail = null;

          if(obj != null) {
                List<PriceHeaderDetailVO> detailList = (List<PriceHeaderDetailVO>)obj;
                
                for(int i = 0; i < detailList.size(); i++) {
                    priceHeaderDetail = detailList.get(i);
                    //logger.debug("price header id in for:" + priceHeaderDetail.getPriceHeaderId());
                    BigDecimal maxDollarAmt = priceHeaderDetail.getMaxDollarAmt();
                    BigDecimal minDollarAmt = priceHeaderDetail.getMinDollarAmt();
                    // price is in between min and max
                    if (minDollarAmt.compareTo(price) <= 0 && maxDollarAmt.compareTo(price) >= 0) {
                        return priceHeaderDetail;
                    }
                }
          } 
          return null;
      }
}
