package com.ftd.osp.utilities.cacheMgr.handlers.vo;


import com.ftd.osp.utilities.cacheMgr.constant.CacheMgrConstants;
import com.ftd.osp.utilities.cacheMgr.dao.CacheMgrDAO;
import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.cacheMgr.vo.CacheHandlerBase;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;

import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;

import java.util.HashMap;
import java.util.Map;

/**

 */
public class ClientActionVO
{
    private String clientId;
    private String actionType;
    private String xslCode;
    private int hashcode;
    
    public ClientActionVO (String clientId, String actionType, String xslCode) {
        this.clientId = clientId;
        this.actionType = actionType;
        this.xslCode = xslCode;
    }

    //overwriting hashCode() of Object
    public int hashCode() {
        if(this.hashcode != 0) {
            return this.hashcode;
        } else {
            hashcode = clientId.hashCode();
        }
        return hashcode;
    }
    
    //overwirting equals() of Ojbect
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        
        ClientActionVO obj = (ClientActionVO)o;

        if(obj.getClientId() == null || !obj.getClientId().equals(this.getClientId()))
            return false;
        if(obj.getActionType() == null || !obj.getActionType().equals(this.getActionType()))
            return false;

        return true;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientId() {
        return clientId;
    }


    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    public String getActionType() {
        return actionType;
    }

    public void setXslCode(String xslCode) {
        this.xslCode = xslCode;
    }

    public String getXslCode() {
        return xslCode;
    }
}
