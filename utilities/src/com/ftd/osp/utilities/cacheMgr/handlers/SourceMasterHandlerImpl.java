package com.ftd.osp.utilities.cacheMgr.handlers;

import com.ftd.osp.utilities.cacheMgr.constant.CacheMgrConstants;
import com.ftd.osp.utilities.cacheMgr.dao.CacheMgrDAO;
import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.MPRedemptionVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.SourceMasterVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.SourceProgramVO;
import com.ftd.osp.utilities.cacheMgr.vo.CacheHandlerBase;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;

import java.math.BigDecimal;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;


/**
 * This class is responsible for loading source code into cache.
 * The oracle version of the query combines source code, partner, reward, redemption
 * into one SQL with a few left outer joins. This is a performance issue for MySQL.
 * For speedy return, MySQL version will only load source code information
 * and perform left outer joins in java by invoking 2 additional queries.
 * key: sourceCode
 * value: SourceMasterVO
 */
public class SourceMasterHandlerImpl extends CacheHandlerBase implements SourceMasterHandler
{
    private Map objMap;
    public Object load(Connection conn) throws CacheException {
        logger = new Logger(this.getClass().getName());
        CacheMgrDAO osdao = new CacheMgrDAO();
        
        // Determine which DB.
        String dbName = (String)osdao.executeQueryReturnObject(conn, CacheMgrConstants.STMT_GET_DB_NAME);
        
        if(logger.isInfoEnabled()) {
        	logger.info("Loadding source for db: " + dbName);  	
        }
        
        Object returnObj = null;
        
        if(CacheMgrConstants.DB_NAME_MYSQL.equals(dbName)) {
        	returnObj = loadMySQL(conn);	
        } else {
        	returnObj = loadOracle(conn);
        }
        
        return returnObj;
    }
    
    public Object loadMySQL(Connection conn) throws CacheException {
        CacheMgrDAO osdao = new CacheMgrDAO();
       
        CachedResultSet rsSourceProgram = osdao.executeQuery(conn, CacheMgrConstants.STMT_GET_SOURCE_PROGRAM);
        CachedResultSet rsMpRedemption = osdao.executeQuery(conn, CacheMgrConstants.STMT_GET_MP_REDEMPTION);
        CachedResultSet rs = osdao.executeQuery(conn, CacheMgrConstants.STMT_GET_SOURCE_MASTER);
        Map sourceProgramMap = new HashMap();
        Map mpRedemptionMap = new HashMap();

        Map handlerMap = new HashMap();
        Map sourceCodeMap = new HashMap();
        SourceMasterVO smvo = null;
        
        if(logger.isInfoEnabled())
          logger.info("Loading source code...");
        
        while (rsSourceProgram != null && rsSourceProgram.next()) {
        	SourceProgramVO spvo = new SourceProgramVO();
        	spvo.setSourceCode(rsSourceProgram.getString("source_code"));
        	spvo.setProgramType(rsSourceProgram.getString("program_type"));
            spvo.setCalculationBasis(rsSourceProgram.getString("calculation_basis"));
            spvo.setPoints(rsSourceProgram.getBigDecimal("points"));
            spvo.setBonusCalculationBasis(rsSourceProgram.getString("bonus_calculation_basis"));
            spvo.setBonusPoints(rsSourceProgram.getBigDecimal("bonus_points"));
            spvo.setMaximumPoints(rsSourceProgram.getBigDecimal("maximum_points"));
            spvo.setRewardType(rsSourceProgram.getString("reward_type"));
            spvo.setPartnerName(rsSourceProgram.getString("partner_name"));
            spvo.setPartnerId(rsSourceProgram.getString("partner_id"));
            
            sourceProgramMap.put(spvo.getSourceCode(), spvo);
        }
        
        while (rsMpRedemption != null && rsMpRedemption.next()) {
        	MPRedemptionVO mpvo = new MPRedemptionVO();
        	mpvo.setMpRedemptionRateId(rsMpRedemption.getString("mp_redemption_rate_id"));
            mpvo.setPaymentMethodId(rsMpRedemption.getString("payment_method_id"));
            mpvo.setDollorToMPOperator(rsMpRedemption.getString("dollar_to_mp_operator"));
            mpvo.setRoundingMethodId(rsMpRedemption.getString("rounding_method_id"));
            mpvo.setRoundingScale(rsMpRedemption.getString("rounding_scale_qty"));
            
            if(rsMpRedemption.getObject("mp_redemption_rate_amt") != null)
            {
            	mpvo.setMpRedemptionRateAmt(new BigDecimal(rsMpRedemption.getDouble("mp_redemption_rate_amt")));
            } else
            {
                mpvo.setMpRedemptionRateAmt(null);
            }
            
            mpRedemptionMap.put(mpvo.getMpRedemptionRateId(), mpvo);
        }        
        
        int count=0;
        while (rs != null && rs.next()) {   
            count++;
            smvo = new SourceMasterVO();
            smvo.setSourceCode(rs.getString("source_code"));
            smvo.setSnhId(rs.getString("snh_id"));
            smvo.setPriceHeaderId(rs.getString("price_header_id"));
            smvo.setCompanyId(rs.getString("company_id"));
            smvo.setDiscountAllowedFlag(rs.getString("discount_allowed_flag"));        
            smvo.setIOTWFlag(rs.getString("iotw_flag"));
            smvo.setProgramType(rs.getString("program_type"));
            smvo.setCalculationBasis(rs.getString("calculation_basis"));
            smvo.setPoints(rs.getBigDecimal("points"));
            smvo.setBonusCalculationBasis(rs.getString("bonus_calculation_basis"));
            smvo.setBonusPoints(rs.getBigDecimal("bonus_points"));
            smvo.setMaximumPoints(rs.getBigDecimal("maximum_points"));
            smvo.setRewardType(rs.getString("reward_type"));
            smvo.setOrderSource(rs.getString("order_source"));
            smvo.setRelatedSourceCode(rs.getString("related_source_code"));

            smvo.setDisplayServiceFeeFlag(rs.getString("client_disp_svc_fee_flag"));
            smvo.setDisplayShippingFeeFlag(rs.getString("client_disp_shp_fee_flag"));

            smvo.setBillingInfoLogic(rs.getString("billing_info_logic"));
            smvo.setBillingInfoPrompt(rs.getString("billing_info_prompt"));
            smvo.setBinNumberCheckFlag(rs.getString("bin_number_check_flag"));
            smvo.setBonusType(rs.getString("bonus_type"));
            smvo.setDefaultSourceCodeFlag(rs.getString("default_source_code_flag"));
            smvo.setDepartmentCode(rs.getString("department_code"));
            smvo.setDescription(rs.getString("description"));
            smvo.setEmergencyTextFlag(rs.getString("emergency_text_flag"));
            smvo.setEnableLpProcessing(rs.getString("enable_lp_processing"));
            smvo.setEndDate((Date)rs.getObject("end_date"));
            smvo.setExternalCallCenterFlag(rs.getString("external_call_center_flag"));
            smvo.setFraudFlag(rs.getString("fraud_flag"));
            smvo.setHighlightDescriptionFlag(rs.getString("highlight_description_flag"));
            smvo.setJcpenneyFlag(rs.getString("jcpenney_flag"));
            smvo.setListCodeFlag(rs.getString("list_code_flag"));
            smvo.setMarketingGroup(rs.getString("marketing_group"));
            smvo.setMaximumPointsMiles(rs.getString("maximum_points_miles"));
            smvo.setMileageBonus(rs.getString("mileage_bonus"));
            smvo.setMileageCalculationSource(rs.getString("mileage_calculation_source"));
            smvo.setOeDefaultFlag(rs.getString("oe_default_flag"));
            smvo.setPartnerId(rs.getString("partner_id"));
            smvo.setPointsMilesPerDollar(rs.getString("points_miles_per_dollar"));
            smvo.setPricingCode(rs.getString("price_header_id"));
            smvo.setPromotionCode(rs.getString("promotion_code"));
            smvo.setRequiresDeliveryConfirmation(rs.getString("requires_delivery_confirmation"));
            smvo.setSendToScrub(rs.getString("send_to_scrub"));
            smvo.setSeparateData(rs.getString("separate_data"));
            smvo.setShippingCode(rs.getString("shipping_code"));
            smvo.setSourceType(rs.getString("source_type"));
            smvo.setStartDate((Date)rs.getObject("start_date"));
            smvo.setValidPayMethod(rs.getString("valid_pay_method"));
            smvo.setWebloyaltyFlag(rs.getString("webloyalty_flag"));
            smvo.setYellowPagesCode(rs.getString("yellow_pages_code"));
            smvo.setCompanyName(rs.getString("company_name"));
            smvo.setInternetOrigin(rs.getString("internet_origin"));
            smvo.setPrimaryFlorist(rs.getString("primary_florist"));
            
            smvo.setApplySurchargeCode(rs.getString("apply_surcharge_code"));
            smvo.setSurchargeAmount(rs.getDouble("surcharge_amount"));
            smvo.setSurchargeDescription(rs.getString("surcharge_description"));
            smvo.setPartnerName(rs.getString("partner_name"));
            smvo.setDisplaySurcharge(rs.getString("display_surcharge_flag"));
            
            String backupFlorists = null; 
            List backupFloristsList = null; 
            if (rs.getString("backup_florists") != null &&  rs.getString("backup_florists") != "")
            {
              backupFlorists = rs.getString("backup_florists");
              backupFloristsList = new ArrayList(); 
              
              StringTokenizer st = new StringTokenizer(backupFlorists, " ");
              while (st.hasMoreTokens())  
              {
                backupFloristsList.add(st.nextToken());
              }
            }
            smvo.setBackupFloristsList(backupFloristsList);
            smvo.setSourceCodeHasLimitIndex(rs.getString("source_code_has_limit_index"));
            smvo.setAllowFreeShippingFlag(rs.getString("allow_free_shipping_flag"));  
            smvo.setSameDayUpcharge(rs.getString("same_day_upcharge"));
            smvo.setDisplaySameDayUpcharge(rs.getString("display_same_day_upcharge"));
            
            SourceProgramVO spvo = (SourceProgramVO)sourceProgramMap.get(smvo.getSourceCode());
            if(spvo != null) {
            	smvo.setProgramType(spvo.getProgramType());
            	smvo.setCalculationBasis(spvo.getCalculationBasis());
            	smvo.setPoints(spvo.getPoints());
            	smvo.setBonusCalculationBasis(spvo.getBonusCalculationBasis());
            	smvo.setBonusPoints(spvo.getBonusPoints());
            	smvo.setMaximumPoints(spvo.getMaximumPoints());
            	smvo.setRewardType(spvo.getRewardType());
            	smvo.setPartnerName(spvo.getPartnerName());
            	smvo.setPartnerId(spvo.getPartnerId());
            }
            
            String mpRedemptionRateId = rs.getString("mp_redemption_rate_id");
            
            if(mpRedemptionRateId != null) {
            	MPRedemptionVO mpvo = (MPRedemptionVO)mpRedemptionMap.get(mpRedemptionRateId);
            	
            	if (mpvo != null) {
            		smvo.setPaymentMethodId(mpvo.getPaymentMethodId());
            		smvo.setDollorToMPOperator(mpvo.getDollorToMPOperator());
            		smvo.setRoundingMethodId(mpvo.getRoundingMethodId());
            		smvo.setRoundingScale(mpvo.getRoundingScale());
            		smvo.setMpRedemptionRateAmt(mpvo.getMpRedemptionRateAmt());
            	}
            }
            
            sourceCodeMap.put(smvo, smvo);
        }
        
        handlerMap.put(CacheMgrConstants.CACHE_KEY_ID_GEN, sourceCodeMap);
        if(logger.isInfoEnabled())
            logger.info("Done loading source code...");
        return handlerMap;
    }    
    
    public Object loadOracle(Connection conn) throws CacheException {
        logger = new Logger(this.getClass().getName());
        CacheMgrDAO osdao = new CacheMgrDAO();
        CachedResultSet rs = osdao.executeQuery(conn, CacheMgrConstants.STMT_GET_SOURCE_MASTER);

        Map handlerMap = new HashMap();
        Map sourceCodeMap = new HashMap();
        SourceMasterVO smvo = null;
        
        if(logger.isInfoEnabled())
          logger.info("Loading source code...");
        while (rs != null && rs.next()) {   
            
            smvo = new SourceMasterVO(rs);

            sourceCodeMap.put(smvo, smvo);
        }
        
        handlerMap.put(CacheMgrConstants.CACHE_KEY_ID_GEN, sourceCodeMap);
        return handlerMap;
    }    
    
    /**
     * Set the cached object in the cache handler. The cache handler is then 
     * responsible for fulfilling all application level API calls, to access the
     * data in the cached object.
     * 
     * @param cachedObject
     * @throws com.ftd.osp.utilities.cache.exception.CacheException
     */    
      public void setCachedObject(Object cachedObject) throws CacheException
      {
        try 
        {
            this.objMap = (Map)((Map) cachedObject).get(CacheMgrConstants.CACHE_KEY_ID_GEN);  
        } catch (Exception ex) 
        {
          super.logger.error(ex);
          throw new CacheException("Could not set the cached object.");
        } 
      }
      
      public SourceMasterVO getSourceCodeById(String sourceCode) throws CacheException
      {
          SourceMasterVO key = new SourceMasterVO(sourceCode);
          SourceMasterVO value = null;
          
          Object cachedObj = this.objMap;
          if (cachedObj == null) {
                throw new CacheException("Cached object is null.");
          }
          Object obj = this.objMap.get(key);

          if(obj != null) {
                value = (SourceMasterVO)obj;     
          } 
          return value;
      }
      
    
    public List<String> getAllSources() throws Exception {
        List<String> sourceList = new ArrayList<String>();
        Map webIdMap = this.objMap;
        Set s = webIdMap.keySet();
        Iterator i = s.iterator();
        
        while (i.hasNext()) {
            String sourceCode = (String)i.next();
            sourceList.add(sourceCode);
        }
        return sourceList;
    }
    
}
