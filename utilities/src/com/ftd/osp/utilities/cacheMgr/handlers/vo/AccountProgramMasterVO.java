package com.ftd.osp.utilities.cacheMgr.handlers.vo;

public class AccountProgramMasterVO {

    private int programMasterId;
    private String programName;
    private String programDescription;
    private String activeFlag;
    private String programUrl;
    private String displayName;

    public void setProgramMasterId(int programMasterId) {
        this.programMasterId = programMasterId;
    }

    public int getProgramMasterId() {
        return programMasterId;
    }

    public void setProgramName(String programName) {
        this.programName = programName;
    }

    public String getProgramName() {
        return programName;
    }

    public void setProgramDescription(String programDescription) {
        this.programDescription = programDescription;
    }

    public String getProgramDescription() {
        return programDescription;
    }

    public void setActiveFlag(String activeFlag) {
        this.activeFlag = activeFlag;
    }

    public String getActiveFlag() {
        return activeFlag;
    }

    public void setProgramUrl(String programUrl) {
        this.programUrl = programUrl;
    }

    public String getProgramUrl() {
        return programUrl;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }
}
