package com.ftd.osp.utilities.cacheMgr.handlers;

import com.ftd.osp.utilities.cacheMgr.constant.CacheMgrConstants;
import com.ftd.osp.utilities.cacheMgr.dao.CacheMgrDAO;
import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.CompanyMasterVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.SourceMasterVO;
import com.ftd.osp.utilities.cacheMgr.vo.CacheHandlerBase;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;

import java.util.HashMap;
import java.util.Map;


/**
 * This class is responsible for loading company master into cache.
 * key: companyId
 * value: CompanyMasterVO
 */
public class CompanyMasterHandler extends CacheHandlerBase
{
    private Map objMap;
    public Object load(Connection conn) throws CacheException {
        logger = new Logger(this.getClass().getName());
        CacheMgrDAO osdao = new CacheMgrDAO();
        CachedResultSet rs = osdao.executeQuery(conn, CacheMgrConstants.STMT_GET_COMPANY_MASTER);

        Map handlerMap = new HashMap();
        Map companyMap = new HashMap();
        String companyId = null;
        String companyName = null;
        String defaultDomain = null;
        
        if(logger.isInfoEnabled())
          logger.info("Loaded company master...");
        while (rs != null && rs.next()) {   
            
            CompanyMasterVO cmVO = new CompanyMasterVO();
            companyId = rs.getString("company_id");
            companyName = rs.getString("company_name");
            defaultDomain = rs.getString("default_domain");
            cmVO.setCompanyId(companyId);
            cmVO.setCompanyName(companyId);
            cmVO.setDefaultDomain(defaultDomain);

            companyMap.put(cmVO, cmVO);
        }
        
        handlerMap.put(CacheMgrConstants.CACHE_KEY_ID_GEN, companyMap);
        return handlerMap;
    }
    
    /**
     * Set the cached object in the cache handler. The cache handler is then 
     * responsible for fulfilling all application level API calls, to access the
     * data in the cached object.
     * 
     * @param cachedObject
     * @throws com.ftd.osp.utilities.cache.exception.CacheException
     */    
      public void setCachedObject(Object cachedObject) throws CacheException
      {
        try 
        {
            this.objMap = (Map)((Map) cachedObject).get(CacheMgrConstants.CACHE_KEY_ID_GEN);  
        } catch (Exception ex) 
        {
          super.logger.error(ex);
          throw new CacheException("Could not set the cached object.");
        } 
      }
      
      public CompanyMasterVO getCompanyById(String companyId) throws CacheException
      {
          CompanyMasterVO key = new CompanyMasterVO(companyId);
          CompanyMasterVO value = null;
          
          Object cachedObj = this.objMap;
          if (cachedObj == null) {
                throw new CacheException("Cached object is null.");
          }
          Object obj = this.objMap.get(key);

          if(obj != null) {
                value = (CompanyMasterVO)obj;     
          } 
          return value;
      }
      

}
