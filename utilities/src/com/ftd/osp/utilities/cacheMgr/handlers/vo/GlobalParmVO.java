package com.ftd.osp.utilities.cacheMgr.handlers.vo;


import com.ftd.osp.utilities.cacheMgr.constant.CacheMgrConstants;
import com.ftd.osp.utilities.cacheMgr.dao.CacheMgrDAO;
import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.cacheMgr.vo.CacheHandlerBase;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;

import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;

import java.util.HashMap;
import java.util.Map;

/**

 */
public class GlobalParmVO
{
    private String context;
    private String name;
    private String value;
    private int hashcode;
    
    public GlobalParmVO (String context, String name, String value) {
        this.context = context;
        this.name = name;
        this.value = value;
    }

    //overwriting hashCode() of Object
    public int hashCode() {
        if(this.hashcode != 0) {
            return this.hashcode;
        } else {
            // hashcode = context.hashCode() + name.hashCode();
            StringBuilder hashCodeBuilder = new StringBuilder(context).append("|").append(name);
            hashcode = hashCodeBuilder.toString().hashCode();
        }
        return hashcode;
    }
    //overwirting equals() of Ojbect
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        
        GlobalParmVO obj = (GlobalParmVO)o;

        if(obj.getContext() == null || !obj.getContext().equals(this.getContext()))
            return false;
        if(obj.getName() == null || !obj.getName().equals(this.getName()))
            return false;
        return true;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public String getContext() {
        return context;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
