package com.ftd.osp.utilities.cacheMgr.handlers.vo;


import com.ftd.osp.utilities.cacheMgr.constant.CacheMgrConstants;
import com.ftd.osp.utilities.cacheMgr.dao.CacheMgrDAO;
import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.cacheMgr.vo.CacheHandlerBase;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;

import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;

import java.util.HashMap;
import java.util.Map;

/**

 */
public class ClientVO
{
    private String clientId;
    private String displayName;
    private int hashcode;
    
    public ClientVO (String clientId, String displayName) {
        this.clientId = clientId;
        this.displayName = displayName;
    }

    //overwriting hashCode() of Object
    public int hashCode() {
        if(this.hashcode != 0) {
            return this.hashcode;
        } else {
            hashcode = clientId.hashCode();
        }
        return hashcode;
    }
    
    //overwirting equals() of Ojbect
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        
        ClientVO obj = (ClientVO)o;

        if(obj.getClientId() == null || !obj.getClientId().equals(this.getClientId()))
            return false;

        return true;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientId() {
        return clientId;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }
}
