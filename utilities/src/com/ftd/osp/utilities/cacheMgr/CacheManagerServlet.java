package com.ftd.osp.utilities.cacheMgr;

import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.plugins.Logger;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This servlet must be used by applications that need to pre-load Fixed caches.
 * Simply defining it as <load-on-startup> should be enough to trigger the pre-load
 * (i.e., the init method forces the preload).  The servlet does not need to be called,
 * but can be used to force a dump of cache statistics to the database.
 * 
 * Sample web.xml definition:
 * 
 * <servlet>
 *   <servlet-name>CacheManagerServlet</servlet-name>
 *   <servlet-class>com.ftd.osp.utilities.cacheMgr.CacheManagerServlet</servlet-class>
 *   <load-on-startup>0</load-on-startup>
 * </servlet>
 *
 * Below is needed only if you wish to call servlet to dump statistics:
 * 
 *  <servlet-mapping>
 *   <servlet-name>CacheManagerServlet</servlet-name>
 *   <url-pattern>/servlet/CacheManagerServlet</url-pattern>
 * </servlet-mapping>
 * 
 * This also requires the userThread switch to be set (under Oracle App server) in opmn.xml
 * for the appropriate container:
 * 
 * <category id="start-parameters">
 *   ...
 *   <data id="oc4j-options" value="-userThreads" />
 *   ...
 */
public class CacheManagerServlet extends HttpServlet 
{
    private static final String CONTENT_TYPE = "text/html; charset=utf-8";
    private Logger logger;
    private CacheManager cm;

    /**
     * init
     */
    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);
        logger = new Logger("com.ftd.osp.utilities.cacheMgr.CacheManagerServlet");
        cm = CacheManager.getInstance();  // This will read cache config file and force preload of Fixed caches
    }

    /**
     * doPost
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        doGet(request, response);
    }

    /**
     * doGet
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        statsSaveToDatabase();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 " +
                                            "Transitional//EN\">\n" +
                    "<HTML>\n" +
                    "<HEAD><TITLE>CacheManagerServlet Init</TITLE></HEAD>\n" +
                    "<BODY>\n" +
                    "<H2>CacheManagerServlet is alive</H2>\n<p>" +  dateFormat.format(date) +
                    "</p></BODY></HTML>");
    }

    /**
     * statsSaveToDatabase
     */
    private void statsSaveToDatabase() {
        cm = CacheManager.getInstance(); 
        cm.statsSaveAllCaches();
    }
}
