package com.ftd.osp.utilities.cacheMgr.constant;


public class CacheMgrConstants
{

     public static final String CACHE_KEY_ALL = "ALL";
     public static final String CACHE_KEY_ID_GEN = "CACHE_KEY_ID_GEN";
     
     public static final String STMT_GET_SERVICE_FEE = "STMT_GET_SERVICE_FEE";
     public static final String STMT_GET_SERVICE_FEE_OVERRIDE = "STMT_GET_SERVICE_FEE_OVERRIDE";
     public static final String STMT_GET_SHIPPING_KEY_DETAIL = "STMT_GET_SHIPPING_KEY_DETAIL";
     public static final String STMT_GET_SHIPPING_KEY_COST = "STMT_GET_SHIPPING_KEY_COST";
     public static final String STMT_GET_STATE_MASTER = "STMT_GET_STATE_MASTER";
     public static final String STMT_GET_COUNTRY_MASTER = "STMT_GET_COUNTRY_MASTER";
     public static final String STMT_GET_PRODUCT_TYPE_EXCL = "STMT_GET_PRODUCT_TYPE_EXCL";
     public static final String STMT_GET_ZIP_CODE = "STMT_GET_ZIP_CODE";
     public static final String STMT_GET_FUEL_SURCHARGE = "STMT_GET_FUEL_SURCHARGE";
     public static final String STMT_GET_OCCASION = "STMT_GET_OCCASION";
     public static final String STMT_GET_DELIVERY_DATE_RANGE = "STMT_GET_DELIVERY_DATE_RANGE";
     public static final String STMT_GET_COMPANY_MASTER = "STMT_GET_COMPANY_MASTER";
     public static final String STMT_GET_CONTENT_WITH_FILTER = "STMT_GET_CONTENT_WITH_FILTER";
     public static final String STMT_GET_GLOBAL_PARM = "STMT_GET_GLOBAL_PARM";
     public static final String STMT_GET_IOTW = "STMT_GET_IOTW";
     public static final String STMT_GET_ORDER_SERVICE_CLIENT = "STMT_GET_ORDER_SERVICE_CLIENT";
     public static final String STMT_GET_ORDER_SERVICE_CLIENT_XSL = "STMT_GET_ORDER_SERVICE_CLIENT_XSL";
     public static final String STMT_GET_ORDER_SERVICE_XSL = "STMT_GET_ORDER_SERVICE_XSL";
     public static final String STMT_GET_PRICE_HEADER_DETAIL = "STMT_GET_PRICE_HEADER_DETAIL";
     public static final String STMT_GET_PRODUCT_ATTR_RESTR = "STMT_GET_PRODUCT_ATTR_RESTR";
     public static final String STMT_GET_PRODUCT_ATTR_RESTR_SOURCE = "STMT_GET_PRODUCT_ATTR_RESTR_SOURCE";
     public static final String STMT_GET_PRODUCT_COLOR = "STMT_GET_PRODUCT_COLOR";
     public static final String STMT_GET_PRODUCT_COMPANY = "STMT_GET_PRODUCT_COMPANY";
     public static final String STMT_GET_PRODUCT_COUNTRY = "STMT_GET_PRODUCT_COUNTRY";
     public static final String STMT_GET_PRODUCT_KEYWORD = "STMT_GET_PRODUCT_KEYWORD";
     public static final String STMT_GET_PRODUCT_MASTER = "STMT_GET_PRODUCT_MASTER";
     public static final String STMT_GET_PRODUCT_SOURCE = "STMT_GET_PRODUCT_SOURCE";
     public static final String STMT_GET_SOURCE_INDEX_CHILD = "STMT_GET_SOURCE_INDEX_CHILD";
     public static final String STMT_GET_SOURCE_INDEX_PRODUCT = "STMT_GET_SOURCE_INDEX_PRODUCT";
     public static final String STMT_GET_SOURCE_MASTER = "STMT_GET_SOURCE_MASTER";
     public static final String STMT_GET_SOURCE_CODE = "STMT_GET_SOURCE_CODE";
     public static final String STMT_GET_UPSELL_DETAIL = "STMT_GET_UPSELL_DETAIL";
     public static final String STMT_GET_CSZ_AVAIL = "STMT_GET_CSZ_AVAIL";
     public static final String STMT_GET_SHIP_METHODS = "STMT_GET_SHIP_METHODS";
     public static final String STMT_GET_FTD_APPS_GLOBAL_PARMS = "GET_FTD_APPS_GLOBAL_PARMS";     
     public static final String STMT_GET_SOURCE_TEMPLATE_REF = "STMT_GET_SOURCE_TEMPLATE_REF";
     public static final String STMT_GET_FILTER_INDEX = "STMT_GET_FILTER_INDEX";
     public static final String STMT_GET_DOMAIN_TEMPLATE = "STMT_GET_DOMAIN_TEMPLATE";
     public static final String STMT_GET_LANGUAGE_MASTER = "STMT_GET_LANGUAGE_MASTER";
     public static final String STMT_GET_ADDON = "STMT_GET_ADDON";
     public static final String STMT_GET_PAYMENT_METHOD_MP = "STMT_GET_PAYMENT_METHOD_MP";
     public static final String STMT_VIEW_ACCOUNT_ACTIVE_PROGRAMS = "STMT_VIEW_ACCOUNT_ACTIVE_PROGRAMS";
     public static final String STMT_GET_SOURCE_PROGRAM = "STMT_GET_SOURCE_PROGRAM";
     public static final String STMT_GET_MP_REDEMPTION = "STMT_GET_MP_REDEMPTION";
     public static final String STMT_GET_DB_NAME = "STMT_GET_DB_NAME";
     public static final String STMT_GET_STATE_COMPANY_TAX  = "STMT_GET_STATE_COMPANY_TAX";
     public static final String STMT_GET_PAYMENT_BIN_MASTER = "STMT_GET_PAYMENT_BIN_MASTER";
     
    public static final String CACHE_NAME_SERVICE_FEE = "CACHE_NAME_SERVICE_FEE";
    public static final String CACHE_NAME_SERVICE_FEE_OVERRIDE = "CACHE_NAME_SERVICE_FEE_OVERRIDE";
    public static final String CACHE_NAME_SHIPPING_KEY_DETAIL = "CACHE_NAME_SHIPPING_KEY_DETAIL";
    public static final String CACHE_NAME_SHIPPING_KEY_COST = "CACHE_NAME_SHIPPING_KEY_COST";
    public static final String CACHE_NAME_FUEL_SURCHARGE = "CACHE_NAME_FUEL_SURCHARGE";
    public static final String CACHE_NAME_COUNTRY_MASTER = "CACHE_NAME_COUNTRY_MASTER";
    public static final String CACHE_NAME_ZIP_CODE = "CACHE_NAME_ZIP_CODE";
    public static final String CACHE_NAME_GLOBAL_PARM = "CACHE_NAME_GLOBAL_PARM"; 
    public static final String CACHE_NAME_SOURCE_MASTER = "CACHE_NAME_SOURCE_MASTER"; 
    public static final String CACHE_NAME_PRODUCT_MASTER = "CACHE_NAME_PRODUCT_MASTER";
    public static final String CACHE_NAME_PRODUCT_ATTR_RESTR = "CACHE_NAME_PRODUCT_ATTR_RESTR";
    public static final String CACHE_NAME_PRODUCT_INDEX = "CACHE_NAME_PRODUCT_INDEX";
    public static final String CACHE_NAME_COMPANY_MASTER = "CACHE_NAME_COMPANY_MASTER"; 
    public static final String CACHE_NAME_CONTENT_WITH_FILTER = "CACHE_NAME_CONTENT_WITH_FILTER";    
    public static final String CACHE_NAME_PRODUCT_SOURCE = "CACHE_NAME_PRODUCT_SOURCE";
    public static final String CACHE_NAME_UPSELL = "CACHE_NAME_UPSELL";
    public static final String CACHE_NAME_IOTW = "CACHE_NAME_IOTW";
    public static final String CACHE_NAME_PRODUCT_ADDON = "CACHE_NAME_PRODUCT_ADDON";
    public static final String CACHE_NAME_PRODUCT_COMPANY = "CACHE_NAME_PRODUCT_COMPANY";
    public static final String CACHE_NAME_PRODUCT_COLOR = "CACHE_NAME_PRODUCT_COLOR";
    public static final String CACHE_NAME_PRODUCT_COUNTRY = "CACHE_NAME_PRODUCT_COUNTRY";
    public static final String CACHE_NAME_PRODUCT_KEYWORD = "CACHE_NAME_PRODUCT_KEYWORD";
    public static final String CACHE_NAME_SOURCE_INDEX_PRODUCT = "CACHE_NAME_SOURCE_INDEX_PRODUCT";
    public static final String CACHE_NAME_PRICE_HEADER_DETAIL = "CACHE_NAME_PRICE_HEADER_DETAIL";
    public static final String CACHE_NAME_SHIPPING_METHOD_COST = "CACHE_NAME_SHIPPING_METHOD_COST";
    public static final String CACHE_NAME_STATE_MASTER = "CACHE_NAME_STATE_MASTER";
    public static final String CACHE_NAME_ORDER_SERVICE_CLIENT = "CACHE_NAME_ORDER_SERVICE_CLIENT";
    public static final String CACHE_NAME_ORDER_SERVICE_CLIENT_XSL = "CACHE_NAME_ORDER_SERVICE_CLIENT_XSL";
    public static final String CACHE_NAME_PRODUCT_TYPE_EXCL = "CACHE_NAME_PRODUCT_TYPE_EXCL";
    public static final String CACHE_NAME_SOURCE_UPSELL = "CACHE_NAME_SOURCE_UPSELL";
    public static final String CACHE_NAME_DELIVERY_DATE_RANGE = "CACHE_NAME_DELIVERY_DATE_RANGE";
    public static final String CACHE_NAME_CSZ_AVAIL = "CACHE_NAME_CSZ_AVAIL";
    public static final String CACHE_NAME_PARTNER_HANDLER = "CACHE_NAME_PARTNER_HANDLER";
    public static final String CACHE_NAME_GET_OCCASION = "CACHE_NAME_GET_OCCASION";
    public static final String CACHE_NAME_GET_PRODUCT  = "CACHE_NAME_GET_PRODUCT";
    public static final String CACHE_NAME_INDEX_SOURCE_TEMPLATE  = "CACHE_NAME_INDEX_SOURCE_TEMPLATE";
    public static final String CACHE_NAME_INDEX_FILTER_INDEX  = "CACHE_NAME_INDEX_FILTER_INDEX";
    public static final String CACHE_NAME_STATE_COMPANY_TAX = "CACHE_NAME_STATE_COMPANY_TAX";
    public static final String CACHE_NAME_OCCASION = "CACHE_NAME_OCCASION";
    
    public static final String STMT_GET_CHARACTER_MAPPING = "STMT_GET_CHARACTER_MAPPING";
    public static final String CACHE_NAME_CHARACTER_MAPPING = "CACHE_NAME_CHARACTER_MAPPING";

    public static final String CACHE_NAME_LANGUAGE_MASTER = "CACHE_NAME_LANGUAGE_MASTER";
    public static final String CACHE_NAME_ADDON = "CACHE_NAME_ADDON";
    public static final String CACHE_NAME_PAYMENT_METHOD_MP = "CACHE_NAME_PAYMENT_METHOD_MP";
    public static final String CACHE_GET_PAYMENT_BIN_MASTER = "CACHE_GET_PAYMENT_BIN_MASTER";
    public static final String STMT_GET_STATE_COMPANY_TAX_CONTROLS  = "GET_STATE_COMPANY_TAX_CONTROLS";
    
    public static final String DB_NAME_ORACLE = "ORACLE";
    public static final String DB_NAME_MYSQL = "MYSQL";
        
}