package com.ftd.osp.utilities.cacheMgr;

import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.cacheMgr.handlers.TransientCacheHandler;
import com.ftd.osp.utilities.plugins.Logger;
import java.util.TimerTask;

/**
 *  Class to handle refreshing of Fixed caches.
 *  Eache Fixed cache will have one instance of this class spawned (if a refresh rate is defined).
 */
public class FixedCacheRefreshTimer extends TimerTask
{
    private static Logger logger = new Logger("com.ftd.osp.utilities.cacheMgr.FixedCacheRefreshTimer");  
    private String cacheName;
  
    /**
    * The constructor
    */
    public FixedCacheRefreshTimer(String a_name) {
        super();
        this.cacheName = a_name;
    }
    
    /**
     * Logic executed when timer triggers an instance of this class.
     * Simply calls handler with forceRefresh flag set to true - which will cause cache object to be reloaded immediately.
     * Since this is a separate thread, it allows any other cache accesses to continue (with the current data) 
     * while the new data is being loaded here.
     */
    public void run() {
        if (logger.isDebugEnabled()) {logger.debug("Fixed cache refresh thread is alive for " + cacheName);}
        if (CacheManager.isTerminateThreads() == false) {
            CacheManager.getInstance().getHandler(this.cacheName, true);
        }
        if (CacheManager.isTerminateThreads() == true) {   // This may be set by call to getHandler - hence this second check
            if(logger.isInfoEnabled())
              logger.info("Fixed cache refresh thread '" + this.cacheName + "' cancelling itself since terminate flag was set");
            this.cancel();
        }
    }
}
