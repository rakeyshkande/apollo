package com.ftd.osp.utilities.cacheMgr;

import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.handlers.TransientCacheHandler;
import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.plugins.Logger;
import java.util.TimerTask;
import java.util.Date;

/**
 *  Class to handle Transient cache expiration checks and purging.
 *  Eache Transient cache will have one instance of this class spawned (if an expiration rate is defined).
 */
public class TransientCacheExpirationTimer extends TimerTask
{
    private static Logger logger = new Logger("com.ftd.osp.utilities.cacheMgr.TransientCacheExpirationTimer");  
    private String cacheName;
    private long elementExpireRate;
  
    /**
    * The constructor
    */
    public TransientCacheExpirationTimer(String a_name, long a_elementExpireRate) {
        super();
        this.cacheName = a_name;
        this.elementExpireRate = a_elementExpireRate;
    }
    
    /**
     * Logic executed when timer triggers an instance of this class.
     * Simply calls handler method to check if any cache elements need to be expired (and purges them if so)
     */
    public void run() {
        boolean terminateThread = false;
        if (logger.isDebugEnabled()) {logger.debug("Transient cache expiration thread is alive");}  

        if (CacheManager.isTerminateThreads() == false) {
            TransientCacheHandler tch = (TransientCacheHandler) CacheManager.getInstance().getHandler(this.cacheName);
            if (CacheManager.isTerminateThreads() == false) {   // This may be set by call to getHandler - hence this second check
                tch.checkExpireAndPurge(this.cacheName, this.elementExpireRate);
            } else {
                terminateThread = true;
            }
        } else {
            terminateThread = true;
        }
        
        if (terminateThread) {  
            if(logger.isInfoEnabled())
              logger.info("Transient cache expiration thread '" + this.cacheName + "' cancelling itself since terminate flag was set");
            this.cancel();
        }

    }
}
