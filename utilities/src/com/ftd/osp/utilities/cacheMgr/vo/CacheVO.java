package com.ftd.osp.utilities.cacheMgr.vo;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Date;
import java.util.Timer;

public class CacheVO 
{
  private String     cacheName;             // Name of this cache
  private String     cacheType;	     // Value represents either Fixed or Transient type
  private String     cacheHandlerClassName; // Class to be instantiated as handler
  private boolean  preloadFlag;            // Should be pre-loaded if true (Fixed type only)
  private long      refreshRate;           // Minutes before cache is refreshed (Fixed type only)
  private long      expireRate;            // Minutes before cache element purged (Transient only)
  private long      expireCheckRate;       // Minutes before cache timer is rerun
  private Date      lastRefreshTime;        // Most recent cache refresh time 
  private long      refreshCount;          // Number of cache refreshes (for stats use only)
  private Date      firstLoadTime;          // Time when cache first loaded (for stats use only)
  private long      accessCount;           // Number of cache accesses (for stats use only)
  private Object    cacheObj;               // Actual cache object
  private Timer     cacheTimer;             // Timer thread for expiration/refresh


    // Default constructor
    //
    public CacheVO()
    {}
 
    // Constructor to initialize from existing CacheVO 
    //
    public CacheVO(CacheVO ovo) {
      this.cacheName = ovo.getCacheName();
      this.cacheType = ovo.getCacheType();
      this.cacheHandlerClassName = ovo.getCacheHandlerClassName();
      this.preloadFlag = ovo.isPreloadFlag();
      this.refreshRate = ovo.getRefreshRate();
      this.expireRate = ovo.getExpireCheckRate();
      this.expireCheckRate = ovo.getExpireCheckRate();
      this.lastRefreshTime = ovo.getLastRefreshTime();
      this.refreshCount = ovo.getRefreshCount();
      this.firstLoadTime = ovo.getFirstLoadTime();
      this.accessCount = ovo.getAccessCount();
      this.cacheObj = ovo.getCacheObj();
      this.cacheTimer = ovo.getCacheTimer();
    }

    public void setCacheName(String cacheName) {
        this.cacheName = cacheName;
    }

    public String getCacheName() {
        return cacheName;
    }

    public void setCacheType(String cacheType) {
        this.cacheType = cacheType;
    }

    public String getCacheType() {
        return cacheType;
    }

    public void setCacheHandlerClassName(String cacheHandlerClassName) {
        this.cacheHandlerClassName = cacheHandlerClassName;
    }

    public String getCacheHandlerClassName() {
        return cacheHandlerClassName;
    }

    public void setPreloadFlag(boolean preloadFlag) {
        this.preloadFlag = preloadFlag;
    }

    public boolean isPreloadFlag() {
        return preloadFlag;
    }
    
    public void setRefreshRate(long refreshRate) {
        this.refreshRate = refreshRate;
    }

    public long getRefreshRate() {
        return refreshRate;
    }

    public void setExpireRate(long expireRate) {
        this.expireRate = expireRate;
    }

    public long getExpireRate() {
        return expireRate;
    }

    public void setExpireCheckRate(long expireCheckRate) {
        this.expireCheckRate = expireCheckRate;
    }

    public long getExpireCheckRate() {
        return expireCheckRate;
    }

    public void setLastRefreshTime(Date lastRefreshTime) {
        this.lastRefreshTime = lastRefreshTime;
    }

    public Date getLastRefreshTime() {
        return lastRefreshTime;
    }

    public void incrementRefreshCount() {
        this.refreshCount++;
    }

    public void setRefreshCount(long refreshCount) {
        this.refreshCount = refreshCount;
    }

    public long getRefreshCount() {
        return refreshCount;
    }

    public void setFirstLoadTime(Date firstLoadTime) {
        this.firstLoadTime = firstLoadTime;
    }

    public Date getFirstLoadTime() {
        return firstLoadTime;
    }

    public void incrementAccessCount() {
        this.accessCount++;
    }

    public void setAccessCount(long accessCount) {
        this.accessCount = accessCount;
    }

    public long getAccessCount() {
        return accessCount;
    }

    public void setCacheObj(Object cacheObj) {
        this.cacheObj = cacheObj;
    }

    public Object getCacheObj() {
        return cacheObj;
    }

    public void setCacheTimer(Timer cacheTimer) {
        this.cacheTimer = cacheTimer;
    }

    public Timer getCacheTimer() {
        return cacheTimer;
    }
}
