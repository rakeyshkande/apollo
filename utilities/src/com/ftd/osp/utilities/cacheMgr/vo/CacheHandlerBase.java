package com.ftd.osp.utilities.cacheMgr.vo;

import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.plugins.Logger;
import java.sql.Connection;

/**
 * Abstract superclass for all cache handlers.  The actual handlers will be invoked by the
 * CacheManager to load and save a reference to the cached object.  All user interaction
 * with a cache will be through a cache handler.
 */
public abstract class CacheHandlerBase
{
    protected Logger logger =  new Logger("com.ftd.osp.utilities.cacheMgr.vo.CacheHandlerBase");  

    /**
    * Returns the object that needs to be cached.
    */
    abstract public Object load(Connection con) throws CacheException;
    
    /**
    * Set the cached object in the cache handler. The cache handler is then 
    * responsible for fulfilling all application level API calls, to access the
    * data in the cached object.
    */
    abstract public void setCachedObject(Object cachedObject) throws CacheException;
}