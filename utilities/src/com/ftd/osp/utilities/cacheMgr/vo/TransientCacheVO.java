package com.ftd.osp.utilities.cacheMgr.vo;

import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.vo.TransientCacheElementVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.AddOnVO;

import java.util.ArrayList;
import java.util.List;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Date;

/**
 * This VO contains data needed to maintain a Transient cache.  
 * Maintenance data for each element within the cache is actually represented by a TransientCacheElementVO.
 */
public class TransientCacheVO 
{
  // This is used to maintain a list of the cache elements ordered by element creation time (to be used in the timer thread for expiration checks)
  private List<InnerElementArrayVO>            elementArray;
  
  // This is used to maintain a map of the cache elements so they can be accessed by name
  private Map<Object,TransientCacheElementVO>  elementMap;

  
    public TransientCacheVO() {
        elementArray = Collections.synchronizedList(new ArrayList<InnerElementArrayVO>());
        elementMap   = Collections.synchronizedMap(new HashMap<Object,TransientCacheElementVO>());
    }

    public Object getTransElement(Object key) {
        return elementMap.get(key);
    }


    public void addTransElement(Object key, TransientCacheElementVO element) {
        elementMap.put(key, element);
    }

    // Removes transient element VO from cache and returns how many times it was accessed 
    public long removeTransElement(int index) {
        long accessCount = 0; 
        try {
            TransientCacheElementVO tcevo = (TransientCacheElementVO) elementArray.get(index).element;
            Object key = tcevo.getElementKey();
            accessCount = elementMap.get(key).getElementAccessCount();
            elementMap.remove(key);
        } catch (NullPointerException npe) {
            Logger logger = new Logger(this.getClass().getName());
            logger.error(CacheManager.CACHE_STATS_LOG_PREFIX + " Attempted to remove transient cache element that did not exist, so ignored");
        }
        return accessCount;
    }

    public int sizeOfElementMap() {
        return elementMap.size();
    }


    public void addTransElementTimeArray(TransientCacheElementVO element) {
        InnerElementArrayVO ieavo = new InnerElementArrayVO();
        ieavo.element = element; 
        ieavo.elemArrayTime = element.getUpdateTime();
        elementArray.add(ieavo);
    }

    public void removeTransElementTimeArray(int index) {
        elementArray.remove(index);
    }

    public int sizeOfElementTimeArray() {
        return elementArray.size();
    }
    
    public Date getElementArrayTime(int index) {
        return elementArray.get(index).elemArrayTime;
    }
    
    public Date getElementUpdateTime(int index) {
        TransientCacheElementVO tcevo = (TransientCacheElementVO) elementArray.get(index).element;
        return tcevo.getUpdateTime();
    }

    public Map<Object, TransientCacheElementVO> getElementMap() {
        return elementMap;
    }


    // Inner class to keep track of when each element was added.  This is necessary
    // since there may be multiple entries for the same elementArray item (and the time fields
    // within reflect only the most recent times).
    //
    // This is all being done since we don't want to bother looking for duplicates
    // whenever element is added to ArrayList (we would have to loop over entire list
    // which could be a bit of overhead).
    //
    private class InnerElementArrayVO {
        public TransientCacheElementVO element;
        public Date elemArrayTime;
    }
}
