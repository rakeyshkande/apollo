package com.ftd.osp.utilities.cacheMgr.vo;

import com.ftd.osp.utilities.vo.VendorAddOnVO;

import java.util.Date;
import java.util.HashMap;

public class TransientCacheElementVO 
{
  private Object    elementKey;
  private Date      creationTime;       // Time this element was first added
  private Date      updateTime;         // Time this element was re-added
  private long     elementAccessCount;
  private Object    cacheElementObj;
  
  public TransientCacheElementVO()
  {}

    public void setElementKey(Object elementKey) {
        this.elementKey = elementKey;
    }

    public Object getElementKey() {
        return elementKey;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public void setElementAccessCount(long elementAccessCount) {
        this.elementAccessCount = elementAccessCount;
    }

    public long getElementAccessCount() {
        return elementAccessCount;
    }

    public void setCacheElementObj(Object cacheElementObj) {
        this.cacheElementObj = cacheElementObj;
    }

    public Object getCacheElementObj() {
        return cacheElementObj;
    }

    public void incrementElementAccessCount() {
        this.elementAccessCount++;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }
    
}
