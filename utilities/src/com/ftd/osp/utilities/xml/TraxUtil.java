package com.ftd.osp.utilities.xml;

import java.io.*;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.xml.transform.Source;
import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.URIResolver;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.w3c.dom.Node;
import org.w3c.dom.Document;

import com.ftd.osp.utilities.j2ee.ResourceUtil;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * Utility class to help facilitate XSL transformation.
 * 
 * @auther Brian Munter
 */

public class TraxUtil  
{
    private static Logger logger = new Logger(TraxUtil.class.getName());
    
    private static TraxUtil TRAXUTIL = new TraxUtil();
    private static HashMap cache = new HashMap();

    /**
     * The private Constructor
     */
    private TraxUtil() {
    }

    /**
     * Returns an instance of TraxUtil which contains a cache of all previously used templates
     * 
     * @return an instance of TraxUtil
     */
    public static TraxUtil getInstance() 
    {
        return TRAXUTIL;
    }

    /**
     * Transforms a xml source with the specified xsl file stylesheet and outputs the result
     * to the HTTPServlet response
     * 
     * @param request the http request
     * @param response the http response
     * @param source the xml file source
     * @param styleSheet the file stylesheet
     * @param parameters a map of transformer parameters
     *     
     * @exception TransformerException 
     * @exception IOException 
     * @exception Exception 
     * 
     */
     public void transform(HttpServletRequest request,
                        HttpServletResponse response,
                        File source,
                        File styleSheet,
                        Map parameters)throws TransformerException, java.io.IOException, Exception
    {
        Transformer transformer = tryCache(styleSheet);
        response.setContentType("text/html");
        this.setParameters(transformer, parameters);
        transformer.transform(new StreamSource(source), new StreamResult(response.getOutputStream()));
    }

    /**
     * Sets up the xml transform to happen on the client side.  The response is sent
     * as xml, with a stylesheet of the xsl.  The parameter map is transformted
     * into xml nodes.
     * 
     * @param request the http request
     * @param response the http response
     * @param source the xml node source
     * @param styleSheet the relative reference from a http view of the stylesheet name and path
     * @param parameters a map of transformer parameters
     *     
     * @exception TransformerException 
     * @exception IOException 
     * @exception Exception 
     * 
     */
    public void transform(HttpServletRequest request,
                        HttpServletResponse response,
                        Document source,
                        File styleSheet,
                        String xslFilePathAndName,
                        HashMap parameters)throws TransformerException, java.io.IOException, Exception
      {
          transformClientSide(request,response,source,styleSheet,xslFilePathAndName,parameters);
      }
      
      
    
    
    /**
     * Perform a transform.  The transform will be done on either the client or the
     * server side, depending on the configuration.
     * 
     * @param request the http request
     * @param response the http response
     * @param source the xml node source
     * @param styleSheet the relative reference from a http view of the stylesheet name and path
     * @param parameters a map of transformer parameters
     *     
     * @exception TransformerException 
     * @exception IOException 
     * @exception Exception 
     * 
     */
    public void transformClientSide(HttpServletRequest request,
                        HttpServletResponse response,
                        Document source,
                        File styleSheet,
                        String xslFilePathAndName,
                        HashMap parameters)throws TransformerException, java.io.IOException, Exception
    {
        //Add the parameters to the document so it will be in the xml
        if (parameters != null)
        {
            DOMUtil.addSection(source, "security", "data", 
                               parameters, true); 
        }

        // Now convert the document to a xml String
        StringBuffer responseStringBuffer = new StringBuffer(200);
            

        // We need to append a line for dealing with the xsl
        responseStringBuffer.append("<?xml version=\"1.0\" encoding=\"ISO8859-1\" ?>");
        responseStringBuffer.append("<?xml-stylesheet type=\"text/xsl\" href=\"");
        responseStringBuffer.append(xslFilePathAndName.substring(1));
        responseStringBuffer.append("\"?>\n");

        // Set the response type to xml
        response.setContentType("text/xml");
        //  Now send the xml back through the response object
        OutputStream os = response.getOutputStream();
        os.write(responseStringBuffer.toString().getBytes());
        
        DOMUtil.print(source, os);
      
    }

    /**
     * Transforms a xml node with the specified xsl file stylesheet and outputs the result
     * to the HTTPServlet response
     * 
     * @param request the http request
     * @param response the http response
     * @param source the xml node source
     * @param styleSheet the file stylesheet
     * @param parameters a map of transformer parameters
     *     
     * @exception TransformerException 
     * @exception IOException 
     * @exception Exception 
     * 
     */
    public void transform(HttpServletRequest request,
                            HttpServletResponse response,
                            Node source,
                            File styleSheet,
                            Map parameters) throws TransformerException, java.io.IOException, Exception
    {
        Transformer transformer = tryCache(styleSheet);
        response.setContentType("text/html");
        this.setParameters(transformer, parameters);
        transformer.transform(new DOMSource(source), new StreamResult(response.getOutputStream()));
    }

    
    /**
     * Transforms a xml node with the specified xsl file stylesheet and outputs the result
     * as a String
     * 
     * @param source the file source
     * @param styleSheet the resource stylesheet
     * @param parameters a map of transformer parameters
     *     
     * @return
     * @throws TransformerException
     * @throws java.io.IOException
     * @throws Exception
     */
    public String transformUsingStyleResource(Node source,
                                         String styleSheet,
                                         Map parameters) throws TransformerException, java.io.IOException, Exception 
    {
        // Internally, we wrap in File, as the cache supports both files and resources
        File resourceFile = new File(styleSheet);
        return transform(source, resourceFile, parameters);
    }
    
    /**
     * Transforms a xml node with the specified xsl file stylesheet and outputs the result
     * as a String
     * 
     * @param source the file source
     * @param styleSheet the file stylesheet
     * @param parameters a map of transformer parameters
     *     
     * @exception TransformerException 
     * @exception IOException 
     * @exception Exception 
     */
    public String transform(Node source,
                          File styleSheet,
                          Map parameters) throws TransformerException, java.io.IOException, Exception 
    {

        Transformer transformer = tryCache(styleSheet);
        this.setParameters(transformer, parameters);
    
        StringWriter stringWriter = new StringWriter();
        transformer.transform(new DOMSource(source), new StreamResult(stringWriter));

        return stringWriter.toString();
    }


    /**
     * Performs a XSL transformation using the supplied stylesheet and document.
     * 
     * @param styleSheet Input XSL
     * @param xmlSource Input Document
     * @return The transformed document.
     * @throws javax.xml.transform.TransformerException
     * @throws java.io.IOException
     * @throws java.lang.Exception
     */
    public Document transform(File styleSheet, Document xmlSource)
         throws TransformerException, java.io.IOException, Exception {

      Transformer transformer = tryCache(styleSheet);
      DOMResult result = new DOMResult();
      transformer.transform( new DOMSource(xmlSource), result );
      Document toReturn = null;
      Node node = result.getNode();
      if ( node instanceof Document ) {
        toReturn = (Document)node;
      }
      return toReturn;
    }
    
    /**
     * Performs a XSL transformation using the supplied style sheet and document.
     * 
     * @param styleSheet Resource Name for the style sheet
     * @param xmlSource Input Document
     * @return The transformed document.
     * @throws javax.xml.transform.TransformerException
     * @throws java.io.IOException
     * @throws java.lang.Exception
     */
    public Document transformUsingStyleResource(String styleSheet, Document xmlSource)
         throws TransformerException, java.io.IOException, Exception {

    	// Internally, we wrap in File, as the cache supports both files and resources
        File resourceFile = new File(styleSheet);
        return transform(resourceFile, xmlSource);
    }    


    /**
     * Transforms a xml node with the specified xsl template from database and outputs the result
     * as a String
     * 
     * @param xmlData the file source
     * @param xslTemplate the xsl template
     *     
     * @exception TransformerException 
     * @exception IOException 
     * @exception Exception 
     * 
     */
    public String transform(String xmlData,
                          String xslTemplate) throws TransformerException, java.io.IOException, Exception 
    {
        InputStream transformationDataStream = new ByteArrayInputStream( xmlData.getBytes("UTF-8"));
        InputStream transformationXMLStream = new ByteArrayInputStream( xslTemplate.getBytes("UTF-8"));

        StringWriter outputStream = new StringWriter();

        TransformerFactory tFactory = TransformerFactory.newInstance();
        
        Transformer transformer =
          tFactory.newTransformer
             (new javax.xml.transform.stream.StreamSource
                (transformationXMLStream));

        transformer.transform
          (new javax.xml.transform.stream.StreamSource
              (transformationDataStream), new javax.xml.transform.stream.StreamResult (outputStream));
        outputStream.close();
        return outputStream.toString();

    }
    
    
    /**
     * Transforms a xml node with the specified xsl template and outputs the result
     * as a String
     * 
     * @param source the file source
     * @param xslTemplate the xsl template
     * @param parameters a map of transformer parameters
     *     
     * @exception TransformerException 
     * @exception IOException 
     * @exception Exception 
     * 
     */
    public String transform(Node source,
                          String xslTemplate,
                          Map parameters) throws TransformerException, java.io.IOException, Exception 
    {
         //   XSLT
         StreamSource xlstStreamSource = new StreamSource(new StringReader(xslTemplate));
         TransformerFactory factory = TransformerFactory.newInstance();
         Transformer transformer = factory.newTransformer(xlstStreamSource);
         StringWriter stringWriter = new StringWriter();
         transformer.transform(new DOMSource(source), new StreamResult(stringWriter));

         return stringWriter.toString();
    }
    

    /**
     * Private method which loads the parameters into the transformer
     * 
     * @param transformer the xsl transformer
     * @param parameters a map of transformer parameters
     * 
     */
    private void setParameters(Transformer transformer, Map parameters) 
    {
        if (parameters != null)  
        {
            String name = null, value = null;
            for (Iterator i = parameters.keySet().iterator(); i.hasNext() ; )  
            {
                name = (String) (i.next());
                if (name != null) {
                    value = (String) parameters.get(name);
                    if(value==null)
                    {
                      value="";
                    }
                    transformer.setParameter(name, value);
                }
            }
        }
    }
   

    /**
     * Returns a Templates object from which Transformers can be created.
     * 
     * @param xslTemplate The template string to convert to a Template
     * @return Template to be utilized for generating Transformers
     * @throws Exception
     */
    public Templates compileTemplate(String xslTemplate) throws Exception
    {
        InputStream transformationXMLStream = new ByteArrayInputStream( xslTemplate.getBytes("UTF-8"));
        TransformerFactory tFactory = TransformerFactory.newInstance();
        
        return tFactory.newTemplates(new javax.xml.transform.stream.StreamSource(transformationXMLStream));
    }

    /**
     * Transforms a xml node with the specified xsl template and outputs the result
     * as a String.
     * Invokes {@link #transform(String, Templates, sizeHint)} with a size Hint of 16K
     * 
     * @param xmlData the file source
     * @param xslTemplate the xsl template
     *     
     * @exception TransformerException 
     * @exception IOException 
     * @exception Exception 
     * 
     */
    public String transform(String xmlData,
                          Templates xslTemplate) throws TransformerException, java.io.IOException, Exception 
    {
        return transform(xmlData, xslTemplate, 16384);
    }
    
    /**
     * Transforms a xml node with the specified xsl template and outputs the result
     * as a String. Allows passing in an expected size Hint to optimize memory allocation/speed.
     * Note: The Hint is used to initialize the underlying output stream and does not indicate a maximum limit.
     * 
     * @param xmlData the file source
     * @param xslTemplate the xsl template
     * @param sizeHint Hint on the expected size of the output. Optimizes the StringWriter utilized to build the output.    
     * @exception TransformerException 
     * @exception IOException 
     * @exception Exception 
     * 
     */
    public String transform(String xmlData,
                          Templates xslTemplate,
                          int sizeHint) throws TransformerException, java.io.IOException, Exception 
    {
        InputStream transformationDataStream = new ByteArrayInputStream( xmlData.getBytes("UTF-8"));

        StringWriter outputStream = new StringWriter(sizeHint);
        Transformer transformer = xslTemplate.newTransformer();

        transformer.transform
          (new javax.xml.transform.stream.StreamSource
              (transformationDataStream), new javax.xml.transform.stream.StreamResult (outputStream));
        outputStream.close();
        return outputStream.toString();
    }    
    
    
    /**
     * Private method which reads from cache to see if stylesheet is in cache or
     * needs to be loaded
     * 
     * Attempts to access the Stylesheet as a File. If not found, attempts it as a Resource.
     * For accessing as a Resource, the File path should reflect the Resource path (Not Physical 
     * File Path)
     * 
     * @param stylesheet xsl stylesheet to check cache for
     * 
     * @exception TransformerException 
     * @exception IOException 
     * @exception Exception 
     * 
     */
    private  Transformer tryCache(File styleSheet) throws TransformerException, java.io.IOException, Exception
    {
        // Search the cache for the templates entry
        TraxUtil.TemplatesCacheEntry templatesCacheEntry = (TemplatesCacheEntry) cache.get(styleSheet.getPath());

        // If entry found
        if (templatesCacheEntry != null)
        {
            // Check timestamp of modification
            if (templatesCacheEntry.isModified())
                templatesCacheEntry = null;
        }

        URIResolver uriResolver = null;
        // If no templatesEntry is found or this entry was obsolete
        if (templatesCacheEntry == null)
        {
            // If this file does not exist, try it as a resource
            InputStream styleSheetInputStream = null;
            if (!styleSheet.exists())
            {
                styleSheetInputStream = ResourceUtil.getInstance().getResourceAsStream(styleSheet.getPath());
            } else
            {
                styleSheetInputStream = new FileInputStream(styleSheet);
                uriResolver = new FilePathURIResolver(styleSheet);
                
            }
            
            if(styleSheetInputStream == null)
            {
                throw new Exception("Requested transformation [" + styleSheet.getAbsolutePath() + "] does not exist.");
            }
            

            TransformerFactory factory = TransformerFactory.newInstance();
            
            if(uriResolver != null)
            {
            	factory.setURIResolver(uriResolver);
            }
            
            // Create new cache entry
            templatesCacheEntry =
            new TraxUtil.TemplatesCacheEntry(factory.newTemplates(new StreamSource(styleSheetInputStream)), styleSheet);

            synchronized(this)
            {
                // Save this entry to the cache
                cache.put(styleSheet.getPath(), templatesCacheEntry);
            }
        }

        return templatesCacheEntry.templates.newTransformer();
    }


    /**
     * Private class to hold templates cache entry.
     */
    private class TemplatesCacheEntry
    {
        /** When was the cached entry last modified. */
        private long lastModified;

        /** Cached templates object. */
        private Templates templates;

        /** Templates file object. */
        private File templatesFile;

        /**
         * Constructs a new cache entry.
         * @param templates templates to cache.
         * @param templatesFile file, from which this transformer was loaded.
         */
        private TemplatesCacheEntry(final Templates templates, final File templatesFile)
        {
            this.templates = templates;
            this.templatesFile = templatesFile;
            
            if(templatesFile.exists())
            {
                this.lastModified = templatesFile.lastModified();
            }
        }

        /**
         * Returns true if the file is a physical file and it has been modified.
         * Else, returns false.
         * 
         * @return
         */
        public boolean isModified()
        {
            if(!templatesFile.exists())
            {
                return false;
            }
            
            if(this.lastModified != templatesFile.lastModified())
                return true;
            
            return false;
        }
    }

    
    private class FilePathURIResolver implements URIResolver
    {    	
    	File styleSheetBase; 
    	FilePathURIResolver(File stylesheet)
    	{
    		logger.debug("URI Resolver for: " + stylesheet.getAbsolutePath());
    		styleSheetBase = stylesheet.getParentFile();
    	}
    	
		public Source resolve(String href, String base)  throws TransformerException {
			logger.debug("URI Resolving: " + href + " with base + " + base);

			try {			
				File resolvedPath = new File(styleSheetBase, href);
				if(resolvedPath.exists())
				{
					return new StreamSource(new FileInputStream(resolvedPath));
				} 
			} catch (Throwable e) {
				throw new TransformerException(e);
			}			
			
			return null;
		}
    	
    	
    }
}
