package com.ftd.osp.utilities.xml;

import com.ftd.osp.framework.dispatcher.Dispatcher;
import com.ftd.osp.utilities.vo.MessageToken;

import java.io.IOException;
import java.io.StringWriter;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.jdom.CDATA;
import org.jdom.Element;
import org.jdom.Namespace;
import org.jdom.input.DOMBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

public class EventHandlerUtility 
{
      
  /**
   * Formats message and sends it to the Events Queue.
   * 
   * @param eventName String
   * @param payload String
   * @param context String
   * @throws java.io.IOException
   * @throws javax.naming.NamingException
   * @throws java.lang.Exception
   */
  public static void enqueueJmsMessage(String eventName, String payload, String context ) throws IOException, NamingException, Exception
  {
      
        try{
        org.jdom.Element root = new Element("event");
        Namespace xsi = Namespace.getNamespace("xsi","http://www.w3.org/2001/XMLSchema-instance");
        root.setAttribute("noNamespaceSchemaLocation","event.xsd",xsi);
        
        root.addContent(new Element("event-name").setText(eventName));
        root.addContent(new Element("context").setText(context));
        
        XMLOutputter xml = new XMLOutputter();
        Format format = Format.getCompactFormat();
        format.setOmitDeclaration(true);
        format.setOmitEncoding(true);
        xml.setFormat(format);
        StringWriter sw = new StringWriter();
        
        CDATA cdata = new CDATA(payload);
        Element ePayload = new Element("payload");
        ePayload.setContent(cdata);
        root.addContent(ePayload);      
        org.jdom.Document doc = new org.jdom.Document(root);
        
        xml = new XMLOutputter();
        format = Format.getCompactFormat();
        format.setOmitDeclaration(true);
        format.setOmitEncoding(true);
        xml.setFormat(format);
        sw = new StringWriter();
        xml.output(doc,sw);
        System.out.println(sw.toString());
        
        MessageToken messageToken = new MessageToken();
        messageToken.setStatus("SUCCESS");
        messageToken.setMessage(sw.toString());
        messageToken.setProperty("CONTEXT", context, "java.lang.String");
        messageToken.setJMSCorrelationID(context + " " + eventName);
        System.out.println("eventName: " + eventName);
        Dispatcher dispatcher = Dispatcher.getInstance();
        dispatcher.dispatchTextMessage(new InitialContext(), messageToken);
        } catch (IOException io)  {
        throw new IOException("Unable to convert doc to string: " + io);
        }
        catch (NamingException n)  {
          throw new Exception("error creating dispatchTextMessage: " + n);
        }
        catch (Exception e)  {
          throw new Exception("error getting instance of dispatcher: " + e);
        }
      
  }
  
   /**
     * Converts a w3c DOM Document to a JDOM Document
     * @param w3cdoc
     * @return converted document
     */
    public org.jdom.Document w3c2jdom(org.w3c.dom.Document w3cdoc) 
    {
        org.jdom.Document doc = null;
        DOMBuilder builder = new DOMBuilder();
        doc = builder.build(w3cdoc);
        
        return doc;
   }
}