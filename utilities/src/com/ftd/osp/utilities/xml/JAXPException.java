package com.ftd.osp.utilities.xml;

public class JAXPException extends Exception {
    public JAXPException() {
    }
    
    public JAXPException(Throwable t) {
        super(t);
    }
}
