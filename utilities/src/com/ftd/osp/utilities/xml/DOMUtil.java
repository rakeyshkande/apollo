package com.ftd.osp.utilities.xml;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.json.XML;
import org.w3c.dom.DOMConfiguration;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentType;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.EntityResolver;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.XMLFormat;
import com.ftd.osp.utilities.j2ee.ResourceUtil;
import com.ftd.osp.utilities.plugins.Logger;


/**
 * Utility class to help manipulate and create DOM objects.
 *
 * @author Brian Munter
 * 
 * This is an updated version of the Util, which exposes only standard org.w3c.dom.* Objects
 * in the Interface.
 */

public class DOMUtil  
{
    private static Logger logger = new Logger(DOMUtil.class.getName());
    private static Pattern pattern = Pattern.compile("<(\\w+)(\\s+\\w+\\s*=\\s*[\"\'].*[\"\'])?/>");

    /**
     * Returns an empty DOM document
     * 
     * @exception ParserConfigurationException
     * @return an empty DOM document
     */
    public static Document getDefaultDocument() throws ParserConfigurationException
    {
        Document doc = getDocumentBuilder().newDocument();
        return doc;    
    }

    /**
     * Returns an empty DOM document with root node
     * 
     * @exception ParserConfigurationException
     * @return an empty DOM document with root node
     */
    public static Document getDocument() throws ParserConfigurationException
    {
        Document doc = getDocumentBuilder().newDocument();
        Element root = (Element) doc.createElement("root");
        doc.appendChild(root);
        
        return doc;    
    }
    
    

    /**
     * Creates an XML Document with the specified Document Type Information
     * @param rootName
     * @param docTypeSysId
     * @param docTypePublicId
     * @param createRootNode If <code>true</code> the root Name node is created in the doc. Else, there is no root element
     * @return
     * @throws ParserConfigurationException
     */
    public static Document getDocument(String rootName, String docTypeSysId, String docTypePublicId, boolean createRootNode)  throws Exception
    {
        DocumentBuilder builder = getDocumentBuilder();
        DOMImplementation impl = builder.getDOMImplementation();
        
        DocumentType docType = impl.createDocumentType(rootName, docTypePublicId, docTypeSysId);
        
        Document retVal =  impl.createDocument(null, rootName, docType);
        
        if(!createRootNode)
        {
            retVal.removeChild(retVal.getDocumentElement());
        }
        
        return retVal;
    }

    /**
     * Returns an empty DOM document using the specified document builder
     * 
     * @param docBuilder the document builder
     * @exception ParserConfigurationException
     * @return an empty DOM document
     */
    public static Document getDocument(DocumentBuilder docBuilder) throws ParserConfigurationException
    {
        Document doc = docBuilder.newDocument();
        return doc;    
    }
  
    /**
     * Returns a DOM document for the file provided.
     * 
     * @param documentFile the document file
     * @exception IOException 
     * @exception SAXException 
     * @exception ParserConfigurationException 
     * @return a DOM document for the file provided.
     */
    public static Document getDocument(File documentFile) throws IOException, SAXException, ParserConfigurationException
    {
        Document doc = null;
        doc = getDocument(new FileInputStream(documentFile));
        
        return doc;            
    }

    /**
     * Returns a DOM document for the file provided using the specified document builder
     * 
     * @param docBuilder the document builder
     * @param documentFile the document file
     * @exception IOException 
     * @exception SAXException 
     * @exception ParserConfigurationException 
     * @return a DOM document for the file provided.
     */
    public static Document getDocument(DocumentBuilder docBuilder, File documentFile) 
        throws IOException, SAXException, ParserConfigurationException
    {
        Document doc = null;
        doc = getDocument(docBuilder, new FileInputStream(documentFile));
        
        return doc;            
    }

    /**
     * Returns a DOM document for the input stream.
     * 
     * @param is the input stream
     * @exception IOException 
     * @exception SAXException 
     * @exception ParserConfigurationException 
     * @return a DOM document for the input stream.
     */
    public static Document getDocument(InputStream is) throws SAXException, IOException, ParserConfigurationException  
    {
    	Document doc = getDocument(getDocumentBuilder(), is);

    	return doc;            
    }

    /**
     * Returns a DOM document for the input stream using the specified document builder.
     * 
     * @param docBuilder the document builder
     * @param is the input stream
     * @exception IOException 
     * @exception SAXException 
     * @exception ParserConfigurationException 
     * @return a DOM document for the input stream.
     */
    public static Document getDocument(DocumentBuilder docBuilder, InputStream is) throws IOException, SAXException, ParserConfigurationException
    {
    	Document doc = null;
    	BufferedInputStream bis = null;
    	InputStreamReader in = null;
        try  
        {
            bis = new BufferedInputStream(is);
        	in = new InputStreamReader(bis);         	
        	InputSource inputSource = new InputSource(in);      
        	doc = docBuilder.parse(inputSource);      
        } 
        finally  
        {
            if (bis != null)  
            {
                bis.close();
            }  
            if (in != null) 
            {
            	in.close();
            }
        }
        return doc;            
    }
  
    /**
     * Returns a DOM document for the xml string.
     * 
     * @param xmlString the xml string.
     * @exception IOException 
     * @exception SAXException 
     * @exception ParserConfigurationException 
     * @return a DOM document for the xml string.
     */
    public static Document getDocument(String xmlString) throws IOException, SAXException, ParserConfigurationException
    {
        Document doc = null;
        StringReader sr = null;
        
        try  
        {
            sr = new StringReader(xmlString);
            InputSource inputSource = new InputSource(sr);      
            doc = getDocumentBuilder().parse(inputSource);
      
        } 
        finally  
        {
            if (sr != null)  
            {
                sr.close();
            }      
        }

        return doc;            
    }

    /**
     * Returns a DOM document for the xml string.
     * 
     * @param docBuilder the document builder
     * @param xmlString the xml string.
     * @exception IOException 
     * @exception SAXException 
     * @exception ParserConfigurationException 
     * @return a DOM document for the xml string.
     */
    public static Document getDocument(DocumentBuilder docBuilder, String xmlString) 
        throws IOException, SAXException, ParserConfigurationException
    {
        Document doc = null;
        StringReader sr = null;

        try    
        {
            sr = new StringReader(xmlString);
            InputSource inputSource = new InputSource(sr);      
            doc = docBuilder.parse(inputSource);
      
        } 
        finally  
        {
            if (sr != null)  
            {
                sr.close();
            }      
        }

        return doc;            
    }

    /**
     * Appends the nodelist to the xml document
     * 
     * @param document the xml document to append to
     * @param nodeList the nodelist to be appended
     */
    public static void addSection(Document document, NodeList nodeList)
    {
        for(int i = 0; i < nodeList.getLength(); i++)
        {
            document.getDocumentElement().appendChild(document.importNode(nodeList.item(i), true));
        }
    }

    /**
     * Appends hashmap keys and values to the xml document
     * 
     * @param document the document to append to
     * @param elementName the primary node name
     * @param entry the secondary node name
     * @param values the hashmap of names and values
     */
    public static void addSection(Document document, String elementName, String entry, HashMap values, boolean elementFlag)
    {
        Element element = (Element) document.createElement(elementName);
        
        if(elementFlag)
        {
            Element entryElement = null;
            Element nameElement = null;
            Element valueElement = null;
            String key = null;

            for (Iterator keyIterator = (values.keySet()).iterator(); keyIterator.hasNext(); )
            {
                entryElement = (Element) document.createElement(entry);

                key = (String) keyIterator.next();
                nameElement = (Element) document.createElement("name");
                if(key != null && !key.equals(""))
                {
                    nameElement.appendChild(document.createTextNode(key));
                }
                entryElement.appendChild(nameElement);
                
                valueElement = (Element) document.createElement("value"); 
                if(values.get(key) != null && !values.get(key).toString().equals(""))
                {
                    valueElement.appendChild(document.createTextNode(values.get(key).toString()));
                }
                entryElement.appendChild(valueElement);
                
                element.appendChild(entryElement);
            }
        }
        else
        {
            Element tmpElement = null;
            String key = null;

            for (Iterator keyIterator = (values.keySet()).iterator(); keyIterator.hasNext(); )
            {
                tmpElement = (Element) document.createElement(entry);
                key = (String) keyIterator.next();
                tmpElement.setAttribute("name", key);
                tmpElement.setAttribute("value", (String) values.get(key));
                
                element.appendChild(tmpElement);
            }
        }
    
        document.getDocumentElement().appendChild(element);
    }

     /**
     * @deprecated use the addSection method that takes in boolean xmlType
     */
    public static void addSection(Document document, String elementName, String entry, HashMap values)
    {
        Element element = (Element) document.createElement(elementName);
        Element tmpElement = null;
        String key = null;

        for (Iterator keyIterator = (values.keySet()).iterator(); keyIterator.hasNext(); )
        {
            tmpElement = (Element) document.createElement(entry);
            key = (String)keyIterator.next();
            tmpElement.setAttribute("name", key);
            tmpElement.setAttribute("value", (String)values.get(key));
            element.appendChild(tmpElement);
        }

        document.getDocumentElement().appendChild(element);
    }

    /**
     * Encodes a string with valid xml characters
     * 
     * @param string the string to validate and encode
     */
    public static String encodeChars(String string)
	{
        if ( string == null || string.equals("") )  
        {
            return "";
        }
    
		char[] characters = string.toCharArray();
		StringBuffer encoded = new StringBuffer();
		String escape;
		
		for(int i = 0;i<string.length();i++)
		{
			escape = escapeChar(characters[i]);
			if(escape == null) 
            {
                encoded.append(characters[i]);
            }
			else 
            {
                encoded.append(escape);
            }
		}
		
		return encoded.toString();
	}

    /**
     * Private method which checks the character to see if it needs to be encoded
     * 
     * @param c the character to check
     */
    private static String escapeChar(char c)
	{
        switch(c)
		{
			case('<')  : return "&lt;";
			case('>')  : return "&gt;";  
			case('&')  : return "&amp;"; 
			case('\'') : return "&#39;"; 
			case('"') : return "&quot;";                   
		} 
        
		return null;    
	}

    /**
     * Returns a default document builder
     * 
     * @exception ParserConfigurationException 
     * @return the document builder
     */
    public static DocumentBuilder getDocumentBuilder() 
        throws ParserConfigurationException
    {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setIgnoringElementContentWhitespace(true);
        factory.setNamespaceAware(false);
        factory.setCoalescing(true);
        return factory.newDocumentBuilder();    
    }

    /**
     * Returns a document builder based on the parameters specified
     * 
     * @param coalescing Specifies that the parser produced by this code will convert CDATA nodes to Text nodes and append it to the adjacent (if any) text node.
     * @param expandEntityReferences Specifies that the parser produced by this code will expand entity reference nodes.
     * @param ignoringComments Specifies that the parser produced by this code will ignore comments.
     * @param ignoringElementContentWhitespace Specifies that the parsers created by this factory must eliminate whitespace in element content (sometimes known loosely as 'ignorable whitespace') when parsing XML documents (see XML Rec 2.10).
     * @param namespaceAware Specifies that the parser produced by this code will provide support for XML namespaces.
     * @param validating Specifies that the parser produced by this code will validate documents as they are parsed.
     * @exception ParserConfigurationException 
     * @return the document builder
     */
    public static DocumentBuilder getDocumentBuilder(boolean coalescing,
                                                     boolean expandEntityReferences,
                                                     boolean ignoringComments,
                                                     boolean ignoringElementContentWhitespace, 
                                                     boolean namespaceAware, 
                                                     boolean validating) 
        throws ParserConfigurationException
    {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setCoalescing(coalescing);
        factory.setExpandEntityReferences(expandEntityReferences);
        factory.setIgnoringComments(ignoringComments);
        factory.setIgnoringElementContentWhitespace(ignoringElementContentWhitespace);
        factory.setNamespaceAware(namespaceAware);
        factory.setValidating(validating);

        return factory.newDocumentBuilder();    
    }
       
    /******
     * Returns a string
     * 
     * @param Document
     * @param String
     * @return a String
     */
    public static String getNodeValue(Document document, String nodeName)
    {
        String nodeValue = "";
        NodeList nl = ((Element)document.getFirstChild()).getElementsByTagName(nodeName);

        if (nl.getLength() > 0)
        {
            Text firstChildText = (Text)nl.item(0).getFirstChild();
            if (firstChildText != null)  
                nodeValue = firstChildText.getNodeValue();
        }

        return nodeValue;
    }


    /**
     * Extracts the Node.TEXT value of a node. If one does not exist,
     * it returns an empty string.
     *
     * @param node  - The Node object containing the text.
     * @return String - Node's text value
     **/
    public static String getNodeValue(Node node) throws Exception
    {
      Node textNode = node.getFirstChild();
      return textNode == null ? "" : textNode.getNodeValue().trim();
    }


    /*****
     * For a single node (name sent in the input array), find and return the value.
     * 
     * Note that this method will yield correct results if there is only ONE absolute path.  
     * As an example, say we are searching for customer_name of 'smith'.  If there exist only 1 
     * records, such that only 1 path ((eg. root/customers/customer/customer_name) exist, this
     * method will work.  However, if the search resulted in multiple names, such that there are
     * more than 1 paths (eg. root/customers/customer(1)/customer_name, 
     * root/customers/customer(2)/customer_name, etc) this method may result in invalid values
     * 
     * @param Document, ArrayList
     * @param String
     * @return a String
     */
    public static String getNodeValue(Document document, ArrayList aList) throws Exception
    {
        String sValue = "";
        String sPath = "/";
        for (int i = 0; i < aList.size(); i++)
        {
          sPath += aList.get(i) + "/";
        }
        
        sPath += "text()";
        
        sValue = getNodeText(document, sPath);
        
        return sValue;
    }


    /**
     * For a resultSet, use the XMLFormat to create a Document
     * 
     * @param ResultSet, XMLFormat
     * @return Document
     */
    public static Document convertToXMLDOM(ResultSet rset, XMLFormat xmlFormat) 
      throws Exception
    {
 
        boolean upper;
        String xmlType = "attribute";
        String xmlTop = "rowset";
        String xmlBottom = "row";

        if(xmlFormat != null) {
            xmlType = xmlFormat.getAttribute("type");
            xmlTop = xmlFormat.getAttribute("top");
            xmlBottom = xmlFormat.getAttribute("bottom");
        }

        if (rset == null)  {
            return null;
        }

        Document doc = null;

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setIgnoringElementContentWhitespace(true);
        factory.setNamespaceAware(false);
        DocumentBuilder builder = factory.newDocumentBuilder();
        doc = builder.newDocument();
        Element rowset = null, row = null, column = null;

        rowset = doc.createElement(xmlTop);

        // loop thru the result set and generate xml
        int i = 0;
        ResultSetMetaData meta = rset.getMetaData();
        int columnCount = meta.getColumnCount();
        String newTextNodeData = null;
        
        while (rset.next())  
        {
            i++;
      
            // create the row tag
            row = doc.createElement(xmlBottom);
            // set up the num attrib on the row tag
            row.setAttribute("num", String.valueOf(i));

            if(xmlType.equals("attribute")) 
            {
                for (int j = 0; j < columnCount; )  
                {
                    // JP Puzon 07-05-2005
                    // Removed encoding of attribute values since all XSLT
                    // processors will do this automatically as per the spec.
                    // row.setAttribute(meta.getColumnName(++j).toLowerCase(), DOMUtil.encodeChars(rset.getString(j)));
                    row.setAttribute(meta.getColumnName(++j).toLowerCase(), rset.getString(j)!=null?rset.getString(j).trim():null);
                }
            }
            else 
            {
                // create a tag for each column name
                for (int j = 0; j < columnCount; )  
                {
                    column = doc.createElement(meta.getColumnName(++j).toLowerCase());
                    newTextNodeData = rset.getString(j)!=null?rset.getString(j).trim():null;
                    if (newTextNodeData != null)  {
                      // append the column data to the column node
                      column.appendChild(doc.createTextNode(newTextNodeData));
                        
                    }
                    // append the column to the row tag
                    row.appendChild(column);
                }
            }
      
            rowset.appendChild(row);
            
        } // end while
        
        doc.appendChild(rowset);
        
        return doc;
    }
    
    
    
    
    
    
    
    
 /**
     * For a resultSet, use the XMLFormat to create an Document
     * 
     * @param ResultSet, XMLFormat
     * @return Document
     */
    public static Document convertToXMLDOM(CachedResultSet rset, XMLFormat xmlFormat) 
      throws Exception
    {
 
        boolean upper;
        String xmlType = "attribute";
        String xmlTop = "rowset";
        String xmlBottom = "row";

        if(xmlFormat != null) {
            xmlType = xmlFormat.getAttribute("type");
            xmlTop = xmlFormat.getAttribute("top");
            xmlBottom = xmlFormat.getAttribute("bottom");
        }

        if (rset == null)  {
            return null;
        }

        Document doc = null;

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setIgnoringElementContentWhitespace(true);
        factory.setNamespaceAware(false);
        DocumentBuilder builder = factory.newDocumentBuilder();
        doc = builder.newDocument();
        Element rowset = null, row = null, column = null;

        //Define the input format to be parsed
        SimpleDateFormat sdfInput = new SimpleDateFormat("yyyy-MM-dd");

        //Define the output format to be formatted
        SimpleDateFormat sdfOutput = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.S");

        Date date;
        Calendar cInput;

        rowset = doc.createElement(xmlTop);

        // loop thru the result set and generate xml
        int i = 0;
        while (rset.next())  
        {
            i++;
            int columnCount = rset.getColumnCount();
            // create the row tag
            row = doc.createElement(xmlBottom);
            // set up the num attrib on the row tag
            row.setAttribute("num", String.valueOf(i));
                for (int j = 0; j < columnCount; )  
                {
                    Integer col=new Integer(++j);

                    String colName=((String)rset.getFieldName(j)).toLowerCase();
                    String value=rset.getString(j)!=null?rset.getString(j).trim():null;

                    if (rset.getObject(j) != null)
                    {
                      if (rset.getObject(j) instanceof java.util.Date)
                      {
                        try
                        {
                          //Create a Calendar Object
                          cInput = Calendar.getInstance();
                          
                          //Set the Calendar Object using the date retrieved
                          cInput.setTime(sdfInput.parse(value));
                          
                          //get the created on date from the calendar object
                          date = cInput.getTime();
                          
                          //Change the format
                          value = sdfOutput.format(date);
                        }
                        catch (Exception e)
                        {}
                      }
                    }
                    
                    if(xmlType.equals("attribute")){
                      row.setAttribute(colName, value);
                    }else
                    {
                       column = doc.createElement(colName);
                      if(value!=null&&value.trim().length()>0)
                      {
                        column.appendChild(doc.createTextNode(value));
                      }
                       row.appendChild(column);
                    }
                }// end for
            
            
      
            rowset.appendChild(row);
            
        } // end while
        
        doc.appendChild(rowset);
        
        return doc;
    }
    
    
    
  
    /*****
     * For a single node (name sent in the input array), update the Document with the value
     * provided. 
     * 
     * Note that this method will update correctly if there exist ONE absolute path ONLY.  
     * As an example, say we want to update the value of order_date element from 2005-03-12 to 
     * 03-12-2005.  If the document has only one path for the order date, this function will 
     * update it correctly.
     * 
     * @param1 Document - Document that contains the element
     * @param2 ArrayList - the path to traverse
     * @param3 String - the new value 
     * @return nothing
     */
    public static void updateNodeValue(Document document, ArrayList aList, String value) throws Exception
    {
        String sPath = "/";
        for (int i = 0; i < aList.size(); i++)
        {
          sPath += aList.get(i) + "/";
        }
        
        sPath += "text()";
        Text text = (Text)selectSingleNode(document, sPath);
        
        if (text != null)
          text.setData(value);
    }

    public static void updateNodeValue(Document document, String sPath, String value) throws Exception
    {
        Text text = (Text) selectSingleNode(document, sPath);
        
        if (text != null)
          text.setData(value);
    }
    
    /*****
     * Returns the text value of a node within the document.  The node is located via the supplied
     * XPath (which is intended to use the 'text()' XPath function).  If the node contains no text, 
     * empty string will be returned.
     * 
     * @param document
     * @param xPath The 
     * @returns
     * @throws IllegalArgumentException if any of the parameters are null (or whitespace only).
     */
    public static String getNodeText(Document document, String xPath) 
        throws Exception {

      // null check document
      if ( document == null ) {
        throw new IllegalArgumentException("Invalid parameter: document was null.");
      }
      
      // null/whitespace check xPath
      if ( xPath==null||xPath.trim().length()<1 ) {
        throw new IllegalArgumentException("Invalid parameter: xPath was null or whitespace only.");
      }

      String toReturn = "";
      Node element = selectSingleNode(document, xPath);
      if ( element != null && element instanceof Text ){
        Text xmlValue = (Text)element;
        toReturn = xmlValue.getNodeValue();
      } else if (element != null && element instanceof Node) {
          toReturn = getNodeValue(element);
      }

      return toReturn;
    }

    /**
     * Returns the nodelist
     * 
     * @param node the root node. Can be Document, Element, Node etc.
     * @param xPath The 
     * @returns NodeSet
     * @throws XPathExpressionException
     */
      public static NodeList selectNodeList(Node node, String xpathQuery) 
          throws XPathExpressionException 
      {
        javax.xml.xpath.XPath xpath = getXPathFactory().newXPath();
        javax.xml.xpath.XPathExpression expression = xpath.compile(xpathQuery);
        synchronized(node)
        {
            return (NodeList) expression.evaluate(node,XPathConstants.NODESET);
        }
      }
        
      /**
       * Returns the nodeset
       * 
       * @param node
       * @param xPath The 
       * @returns NodeSet
       * @throws XPathExpressionException
       */
        public static NodeList selectNodes(Node node, String xpathQuery) 
            throws XPathExpressionException 
        {
          javax.xml.xpath.XPath xpath = getXPathFactory().newXPath();
          javax.xml.xpath.XPathExpression expression = xpath.compile(xpathQuery);
          
          synchronized(node)
          {
              return (NodeList) expression.evaluate(node,XPathConstants.NODESET);
          }
        }      

    /**
     * Returns the Text node resulting from the xpath query or <code>null</code>
     * 
     * @param node
     * @param xPath The 
     * @returns NodeSet
     * @throws XPathExpressionException
     */
    public static Node selectSingleNode(Node node, String xpathQuery)
        throws XPathExpressionException
    {
        Text text = null;
        javax.xml.xpath.XPath xpath = getXPathFactory().newXPath();
        javax.xml.xpath.XPathExpression expression = xpath.compile(xpathQuery);

        NodeList nodes = null;
        
        synchronized(node)
        {
            nodes = (NodeList) expression.evaluate(node, XPathConstants.NODESET);
        }
        
        //Only return the first node value that matches the pattern
        if (nodes != null && nodes.getLength() > 0)
        {
            text = (Text) nodes.item(0);
        }

        return text;
    }    

    /**
    * Returns the first node
    * 
    * @param doc
    * @param xPathQuery  
    * @returns Text
    * @throws XPathExpressionException
    */
    public static Node selectSingleNode(Document doc, String xpathQuery) 
        throws XPathExpressionException 
    {
        Node node = null;
        NodeList nodes = selectNodeList(doc, xpathQuery);
        
        //Only return the first node value that matches the pattern
        if(nodes != null && nodes.getLength() > 0) 
        {
            node = nodes.item(0);            
        }
        
        return node;
    }
    

    /**
     * Convert document to a String
     * 
     * @param doc
     * @returns String
     */
    public static String convertToString(Document doc)
        throws Exception
    {
        StringWriter writer = new StringWriter();
        transformDocument(doc, new StreamResult(writer));
        writer.flush();
        return writer.toString();
    }
    
    /**
     * Formatted XML text
     * @param doc
     * @return
     * @throws Exception
     */
    public static String convertToFormattedString(Document doc)
    	throws Exception
	{
    	Map<String, String> defaultFormat = new HashMap<String, String>();	
    	defaultFormat.put(OutputKeys.OMIT_XML_DECLARATION, "no");
    	defaultFormat.put(OutputKeys.INDENT, "yes");
    	
    	DocumentType docType = doc.getDoctype();
    	if( docType != null)
    	{
    	    if(docType.getPublicId() != null)  	        
    	        defaultFormat.put(OutputKeys.DOCTYPE_PUBLIC, docType.getPublicId());
    	    
    	    if(docType.getSystemId() != null)
    	        defaultFormat.put(OutputKeys.DOCTYPE_SYSTEM, docType.getSystemId());
    	}
    	
    	// defaultFormat.put("{http://xml.apache.org/xslt}indent-amount", "2"); // xalan specific, does not work on oracle
    	
    	StringWriter writer = new StringWriter();
        transformDocument(doc, new StreamResult(writer), defaultFormat);
        writer.flush();
        return writer.toString();		
	}

    /**
     * Single XML Line
     * @param doc
     * @return
     * @throws Exception
     */
    public static String convertToCompactString(Document doc)
	throws Exception
	{
    	Map<String, String> defaultFormat = new HashMap<String, String>(); 	
    	defaultFormat.put(OutputKeys.OMIT_XML_DECLARATION, "yes");
    	defaultFormat.put(OutputKeys.INDENT, "no");
    	StringWriter writer = new StringWriter();
        transformDocument(doc, new StreamResult(writer), defaultFormat);
        writer.flush();
        return writer.toString();	
	}    
    /**
     * Writes the contents of this document to the given output stream
     * 
     * @param doc
     * @param writer
     * @throws Exception
     */
    public static void print(Node doc, PrintWriter out)
        throws Exception
    {
        transformDocument(doc, new StreamResult(out));
        out.flush();
    }

    /**
     * Writes the contents of this document to the given output stream
     * 
     * @param doc
     * @param writer
     * @throws Exception
     */
    public static void print(Node doc, Writer out)
        throws Exception
    {
        transformDocument(doc, new StreamResult(out));
        out.flush();
    }

    /**
     * Writes the contents of this document to the given output stream
     * 
     * @param doc
     * @param writer
     * @throws Exception
     */
    public static void print(Node doc, OutputStream out)
        throws Exception
    {
        transformDocument(doc, new StreamResult(out));
    }

    /**
     * Method to consolidate workarounds for 2 known differences/problems between xmlparser and xalan,
     * namely xalan adds in the xml declaration, which is not desirable, and on empty nodes may throw an NPE. 
     * @param doc
     * @param out
     * @throws TransformerFactoryConfigurationError
     * @throws TransformerConfigurationException
     * @throws TransformerException
     */
    private static void transformDocument(Node doc, StreamResult out)
	throws TransformerFactoryConfigurationError,
	TransformerConfigurationException, TransformerException    
    {
    	Map<String, String> defaultFormat = new HashMap<String, String>();
    	
    	// Workaround transformer differences    	
    	defaultFormat.put(OutputKeys.OMIT_XML_DECLARATION, "yes");
    	defaultFormat.put(OutputKeys.INDENT, "yes");
    	// Workaround transformer differences
    	
    	transformDocument(doc, out, defaultFormat);
    }
    
    
    /**
     * Performs actual transformation of the XML to the specified StreamResult
     * Note: This function has varying behavior to mimic the behavior of the Oracle XML Parser.
     * 
     * Issue#9519: Root cause: When XML Transformer is utilized to convert a DOM Object to text, the oracle implementation only converts the children of the DOM object, 
     * while the standard/xalan implementation includes the DOM object in the output. Note: When printing a Document object, the result is the same since the each 
     * document object has a root Element as its child. 
     * The Oracle XML Parser behavior is different if the DOM Object has been appended to its document (Object is in XML) or not (Object is not in XML).
     * 
     * This method has been unit tested to work with both Oracle XML Parser and Xalan. See DOMPUtilTest.java
     * 
     * @param doc
     * @param out
     * @param outputProperties
     * @throws TransformerFactoryConfigurationError
     * @throws TransformerConfigurationException
     * @throws TransformerException
     */
    private static void transformDocument(Node doc, StreamResult out, Map<String, String> outputProperties)
    	throws TransformerFactoryConfigurationError,
    		TransformerConfigurationException, TransformerException {
    	TransformerFactory transformerFactory = TransformerFactory.newInstance();
    	Transformer transformer = transformerFactory.newTransformer();

    	for(String key: outputProperties.keySet())
    	{
    		transformer.setOutputProperty(key, outputProperties.get(key));
    	}

    	// Workaround Xalan Issue: XALANJ-1868
    	cleanDOM(doc);    
    	// Workaround 

    	// This is to mimic Oracle XML Behavior that depends on whether the node has a parent or not
    	if(doc instanceof Document || doc.getParentNode() != null)
    	{
    	    transformer.transform(new DOMSource(doc), out);
    	} else
    	{
            NodeList children = doc.getChildNodes();
            for(int iLoop = 0; iLoop < children.getLength(); iLoop ++)
            {
                Node child = children.item(iLoop);
                transformer.transform(new DOMSource(child), out);
            }   
    	}
    }

    /**
     * Work around xalan bug that throws NPE on empty nodes. Xalan Issue: XALANJ-1868
     * @param document
     */
    public static void cleanDOM(Node document)
    {
        if(document instanceof Text)
        {
            Text textNode = (Text) document;

            if(textNode.getNodeValue() == null)
            {
                if (logger.isDebugEnabled())
                    logger.debug("TextNode with Null Value: " + textNode.getParentNode().getNodeName());
                textNode.setNodeValue("");
            }            
        }
        
        // Recurse children
        if(document.hasChildNodes())
        {
            NodeList children = document.getChildNodes();
            for(int iLoop = 0; iLoop < children.getLength(); iLoop ++)
            {
                cleanDOM(children.item(iLoop));
            }
            
        }
    }


    /**
     * Creates a Schema instance from the passed in InputSource
     * @param inputSource
     * @return
     * @throws Exception
     */
    public static Schema getSchema(Source inputSource) throws Exception
    {
        SchemaFactory factory = SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");
        Schema schema = factory.newSchema(inputSource);      
        
        return schema;
    }
    
    /**
     * Parses a document using a Validating Schema
     * @param documentSource
     * @param schema
     * @param handler
     * @return
     * @throws Exception
     */
    public static Document getDocument(InputSource documentSource, Schema schema, ErrorHandler handler)
    throws Exception
    {
        // Get a Validating Document Builder for the given Schema
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();      
        factory.setIgnoringElementContentWhitespace(true);
        factory.setNamespaceAware(true);
        factory.setCoalescing(true);
        factory.setValidating(false);
       // factory.setSchema(schema);
               
        DocumentBuilder docBuilder = factory.newDocumentBuilder();
        docBuilder.setErrorHandler(handler);
        
        return docBuilder.parse(documentSource);
    }
 
    /**
     * empty text nodes can get represented in XML as "<node/>" when other code expects it to be "<node></node>". 
     * This method expands the empty node in an XML string. 
     * BEWARE of strings containing CDATA with data that looks like an empty node, i.e. "<.../>"  as it will get translated as well.
     * @param xmlString
     * @return
     */
    public static String expandEmptyTextNode(String xmlString) {
        Matcher m = pattern.matcher(xmlString);
        String replacement = "<$1$2></$1>";
        return m.replaceAll(replacement);
	}
    
    
    /**
     * Return a new instance each time. Per JDK spec, XPathFactory is not reentrant or thread safe.
     */
    private static XPathFactory getXPathFactory()
    {
        return XPathFactory.newInstance();
    }


    /**
     * If validateDTD=true, validate the contents of a file against the internal DTD and the specified external DTD. Also, 
     * convert it to a Document. It is expected that the internal and external DTD will be the same, though not enforced. If 
     * validating, and the DTDs are different, but the document is valid against both, no errors will be returned.
     * @param contents - contents of the xml file to validated and converted to a Document
     * @param dtdFileName - external DTD file
     * @param validateDTD - should document contents be validated?
     * @param errorbuf - validation output/errors if any
     * @return - returns the file contents converted to a document even if validation failed.
     * @throws ParserConfigurationException
     * @throws TransformerException
     * @throws SAXException
     * @throws IOException
     */
    public static Document getDocument(String contents, String dtdFileName, boolean validateDTD, StringBuffer errorbuf) 
    throws ParserConfigurationException, TransformerException, SAXException, IOException {
    	DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    	factory.setValidating(validateDTD);
    	DocumentBuilder builder = factory.newDocumentBuilder();
    	builder.setEntityResolver(new DTDEntityResolver());
    	builder.setErrorHandler(new DTDErrorHandler(errorbuf));
    	// validate against internal DTD and convert to document
    	Document doc = builder.parse(new ByteArrayInputStream(contents.getBytes()));
    	
    	// transform document by replacing DTD string with external DTD
    	Writer writer = new StringWriter();
    	DOMSource source = new DOMSource(doc);
    	StreamResult result = new StreamResult(writer);
    	TransformerFactory tf = TransformerFactory.newInstance();
    	Transformer transformer = tf.newTransformer();
    	transformer.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM, dtdFileName);
    	transformer.transform(source, result);
    	
    	// Validate against the new internal DTD
    	doc = builder.parse(new ByteArrayInputStream(writer.toString().getBytes()));
    	
    	if (errorbuf.length() != 0) {
    		// log the errors with a stack trace
    		logger.error(errorbuf.toString(), new Exception("Document validation error"));
    	}
    	   	
    	return doc;
    }
    
    private static class DTDEntityResolver implements EntityResolver {
    	public InputSource resolveEntity(java.lang.String publicId, java.lang.String systemId)
    	throws SAXException, java.io.IOException
    	{
    		InputSource inSource = null;
    		if (systemId.endsWith(".dtd")) {
    			InputStream is;
    			// Strip off FQP if it exists
    			String fname = systemId.substring(systemId.lastIndexOf("/")+1);
    			is = ResourceUtil.getInstance().getResourceFileAsStream(fname);
    			if (is != null)
    				inSource = new InputSource(is);
    		}
    		 
    		return inSource;
    	}
    }
    
	private static class DTDErrorHandler implements org.xml.sax.ErrorHandler {
		StringBuffer errorbuf;
		
		public DTDErrorHandler(StringBuffer errorbuf) {
			this.errorbuf = errorbuf;
		}
		//To handle Fatal Errors
		public void fatalError(SAXParseException exception)throws SAXException {
			errorbuf.append("Line: " +exception.getLineNumber() + "\nFatal Error: "+exception.getMessage());
		}
		//To handle Errors
		public void error(SAXParseException e)throws SAXParseException {
			errorbuf.append("Line: " +e.getLineNumber() + "\nError: "+e.getMessage());
		}
		//To Handle warnings
		public void warning(SAXParseException err)throws SAXParseException{
		}
	}

	/**
     * Converts a Document to a String
     * @param doc
     * @return
     * @throws Exception
     */
    public static String domToString(Document doc) throws Exception {
    	//This will replace NULL TextNode values with "" so that xalan won't throw NPE while getting XML doc as String.
    	DOMUtil.cleanDOM(doc);
        return outputXmlFormatted(doc);
    }
    
    /**
     * Converts an XML string to Document
     * @param xmlStr
     * @return
     * @throws Exception
     */
    public static Document stringToDom (String xmlStr) throws Exception {
        return JAXPUtil.parseDocument(xmlStr);
    }
    
    public static String outputXmlFormatted(Document doc) throws Exception {
        return getXMLOutput(doc, "UTF-8", "yes", "no");
    }   
    
    public static String outputXmlRaw(Document doc) throws Exception {
        return getXMLOutput(doc, "UTF-8", "no", "yes");
    }   
    
    public static String getXMLOutput(Document doc, 
                                      String outputKeyEncoding, 
                                      String outputKeyIndent, 
                                      String outputKeyOmitDecl) throws Exception {
        StringWriter sw = new StringWriter();
        DOMSource domSource = new DOMSource(doc);
        StreamResult streamResult = new StreamResult(sw);
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer serializer = tf.newTransformer();
        serializer.setOutputProperty(OutputKeys.ENCODING,outputKeyEncoding);
        serializer.setOutputProperty(OutputKeys.INDENT,outputKeyIndent);
        serializer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION ,outputKeyOmitDecl);
        serializer.transform(domSource, streamResult); 
        return sw.toString();
    }
}