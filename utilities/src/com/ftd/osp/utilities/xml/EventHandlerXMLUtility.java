package com.ftd.osp.utilities.xml;

import com.ftd.osp.utilities.xml.DOMUtil;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;

import org.w3c.dom.Document;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * This class is used to generate the Event Handler XML which is set in the message
 * on a Message Token.    
 *
 * @author Rose Lazuk
 */
 
public class EventHandlerXMLUtility 
{
  public EventHandlerXMLUtility()
  {
  }
  
    /**
     * Build the Event Handler XML.  This xml will contain a root node
     * with 2 elements.  The elements will be made up of the event name and
     * the payload.  Since the payload can contain anything,
     * need to set a boolean flag if the payload contains XML or not.  If the
     * payload contains XML, then the xml needs to be wrapped with CDATA tags.
     * 
     * @param eventName String
     * @param payload String
     * @param isPayloadXML boolean
     * @exception ParserConfigurationException
     * @exception TransformerConfigurationException
     * @exception TransformerException
     * 
     */
    public Document generateEventHandlerXML(String eventName, String payload, boolean isPayloadXML) throws ParserConfigurationException,
    TransformerConfigurationException, TransformerException
    {
        Document eventDocument = DOMUtil.getDefaultDocument();
        Element tagElement = null;
          
        // Create order root
        Element eventElement = eventDocument.createElement("root");
        
        eventDocument.appendChild(eventElement);
        eventElement.appendChild(this.createTextNode("event-name", eventName, eventDocument, tagElement));
        
        if(isPayloadXML)
          eventElement.appendChild(this.createCDATANode("payload", payload, eventDocument, tagElement)); 
        else
          eventElement.appendChild(this.createTextNode("payload", payload, eventDocument, tagElement));
          
                           
        return eventDocument;
    }
    
    
    
    
    
    /**
     * Creates a Text node given the specified string.
     * 
     * @param elementName String
     * @param elementValue String
     * @param document Document
     * @param tagElement Element 
     * @return Element
     * 
     */
     private Element createTextNode(String elementName, String elementValue, Document document, Element tagElement)
    {
        tagElement = document.createElement(elementName);
        if(elementValue != null)
        {
            tagElement.appendChild(document.createTextNode(elementValue));
        }

        return tagElement;
    }
    
    
    
     /**
     * Creates a CDATASection node whose value is the specified string.
     * 
     * @param elementName String
     * @param elementValue String
     * @param document Document
     * @param tagElement Element 
     * @return Element
     * 
     */
     private Element createCDATANode(String elementName, String elementValue, Document document, Element tagElement)
    {
        tagElement = document.createElement(elementName);
        if(elementValue != null)
        {
            tagElement.appendChild(document.createCDATASection(elementValue));
        }

        return tagElement;
    }
   
   public static void main(String[] args)
    {
        EventHandlerXMLUtility eventHandler = new EventHandlerXMLUtility();
                
        String eventName = "ORDER INFUSOR";
        
        String payload = "<order><header></header><items><item></item></items></order>";
        boolean isPayloadXML = true;
        
        //String payload = "This is a test";
        //boolean isPayloadXML = false;
        
        try
        {
              
            Document doc = (Document) eventHandler.generateEventHandlerXML(eventName, payload, isPayloadXML);
            DOMUtil.print(doc, System.out);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }
}