package com.ftd.osp.utilities.xml;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
 
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;


/**
 * Utilities class for the JAXP parser implementation
 */
public class JAXPUtil {

    /**
     * Return a new instance each time. Per JDK spec, Factory is not guranteed thread safe.
     */
    private static DocumentBuilderFactory getDocumentValBuilderFactory()
    {
        DocumentBuilderFactory retVal = DocumentBuilderFactory.newInstance();
        retVal.setValidating(true);
        retVal.setNamespaceAware(false);
        retVal.setCoalescing(true);
        retVal.setIgnoringElementContentWhitespace(true);
        retVal.setExpandEntityReferences(false);
        return retVal;
    }
    
    /**
     * Return a new instance each time. Per JDK spec, Factory is not guranteed thread safe.
     */
    private static final DocumentBuilderFactory getDocumentBuilderFactory() { 
        DocumentBuilderFactory retVal = DocumentBuilderFactory.newInstance();
        retVal.setValidating(false);
        retVal.setNamespaceAware(false);
        retVal.setCoalescing(true);
        retVal.setIgnoringElementContentWhitespace(true);
        retVal.setExpandEntityReferences(false);
        return retVal;
    }
    
    /**
     * Return a new instance each time. Per JDK spec, Factory is not guranteed thread safe.
     */
    private static final DocumentBuilderFactory getDocumentNsValBuilderFactory() { 
        DocumentBuilderFactory retVal = DocumentBuilderFactory.newInstance();
        retVal.setValidating(true);
        retVal.setNamespaceAware(true);
        retVal.setCoalescing(true);
        retVal.setIgnoringElementContentWhitespace(true);
        retVal.setExpandEntityReferences(false);
        return retVal;
    }
    
    /**
     * Return a new instance each time. Per JDK spec, Factory is not guranteed thread safe.
     */
    private static final DocumentBuilderFactory getDocumentNsBuilderFactory() { 
        DocumentBuilderFactory retVal = DocumentBuilderFactory.newInstance();
        retVal.setValidating(false);
        retVal.setNamespaceAware(true);
        retVal.setCoalescing(true);
        retVal.setIgnoringElementContentWhitespace(true);
        retVal.setExpandEntityReferences(false);
        return retVal;
    }
    
    /**
     * Convert a DOM document to a string
     * @param document to convert
     * @return String representation of pass DOM document
     * @throws com.ftd.osp.utilities.xml.JAXPException
     */
     public static String toString(Document document) throws JAXPException {
         try {
        	 return DOMUtil.convertToFormattedString(document);
         } catch (Exception e) {
             throw new JAXPException(e);
         }
     }

    /**
     * Convert a DOM document to a string without pretty formatting
     * @param document to convert
     * @return String representation of pass DOM document
     * @throws com.ftd.osp.utilities.xml.JAXPException
     */
     public static String toStringNoFormat(Document document) throws JAXPException {
         try {        	 
             return DOMUtil.convertToCompactString(document);
         } catch (Exception e) {
             throw new JAXPException(e);
         }
     }

    /**
     * Convert a DOM element to a string.
     * 
     * Note: This function has varying behavior to mimic the behavior of the Oracle XML Parser.
     * 
     * Issue#9519: Root cause: When XML Transformer is utilized to convert a DOM Object to text, the oracle implementation only converts the children of the DOM object, 
     * while the standard/xalan implementation includes the DOM object in the output. Note: When printing a Document object, the result is the same since the each 
     * document object has a root Element as its child. 
     * The Oracle XML Parser behavior is different if the DOM Object has been appended to its document (Object is in XML) or not (Object is not in XML).
     * 
     * This method has been unit tested to work with both Oracle XML Parser and Xalan. See JAXPUtilTest.java
     * 
     * @param element to convert
     * @return String representation of pass DOM document
     * @throws com.ftd.osp.utilities.xml.JAXPException
     */
    public static String toString(Element element) throws JAXPException {        
        StringBuilder sb = new StringBuilder(1024);
        if(element.getParentNode() != null) {
            toStringImpl(element, sb);
        } else {
            NodeList children = element.getChildNodes();
            for(int iLoop = 0; iLoop < children.getLength(); iLoop ++)
            {
                Node child = children.item(iLoop);
                toStringImpl(child, sb);
            }   
        }
        
        return sb.toString();
    }
    
    /**
     * Does the actual writing of an Element out
     * @param element
     * @param sb
     * @throws JAXPException
     */
    private static void toStringImpl(Node element, StringBuilder  sb) throws JAXPException {
        String retval = null;

        try {
            Transformer idTransform = getTransformerFactory().newTransformer();
            idTransform.setOutputProperty("omit-xml-declaration","yes");
            Source input = new DOMSource(element);
            StringWriter sw = new StringWriter();
            Result output = new StreamResult(sw);
            idTransform.transform(input, output);
            retval = sw.toString();
        } catch (Exception e) {
            throw new JAXPException(e);
        }

        sb.append(retval);                
    }

    /**
     * Parse a string to a DOM document with validation turned on
     * @param strDocument to convert
     * @return Converted DOM document
     * @throws com.ftd.osp.utilities.xml.JAXPException
     */
    public static Document parseDocument(String strDocument) throws JAXPException {
        return parseDocument(strDocument,false,false);
    }
    
    /**
     * Parse a string to a DOM document
     * @param strDocument to convert
     * @param validate should validating be turned on
     * @return Converted DOM document
     * @throws com.ftd.osp.utilities.xml.JAXPException
     */
    public static Document parseDocument(String strDocument, boolean validate, boolean namespaceAware) throws JAXPException {
        Document d = null;
        
        if( StringUtils.isNotBlank(strDocument) ) {
            try {
                ByteArrayInputStream is = new ByteArrayInputStream(strDocument.getBytes());
                d = parseDocument(is,validate,namespaceAware);
            } catch (JAXPException je) {
                throw je;
            } catch (Exception e) {
                throw new JAXPException(e);
            }
        }
        return d;
    }

    /**
     * Parse an input stream to a DOM document with validation turned on
     * @param strDocument to convert
     * @return Converted DOM document
     * @throws com.ftd.osp.utilities.xml.JAXPException
     * @throws IOException 
     */
    public static Document parseDocument(InputStream strDocument) throws JAXPException, IOException {
        return parseDocument(strDocument,false,false);
    }
    
    /**
     * Parse an input stream to a DOM document
     * @param strDocument to convert
     * @param validate should validating be turned on
     * @return Converted DOM document
     * @throws com.ftd.osp.utilities.xml.JAXPException
     * @throws IOException 
     */
    public static Document parseDocument(InputStream strDocument, boolean validate, boolean namespaceAware) throws JAXPException, IOException {
        Document d = null;
        InputStreamReader in = null;
    	BufferedInputStream bis = null;

        if( strDocument!=null ) {
            try {
                DocumentBuilder parser = null;
                if( validate ) {
                    if( namespaceAware ) {
                        parser = getDocumentNsValBuilderFactory().newDocumentBuilder();
                    } else {
                        parser = getDocumentNsBuilderFactory().newDocumentBuilder();
                    }
                } else {
                    if( namespaceAware ) {
                        parser = getDocumentNsBuilderFactory().newDocumentBuilder();
                    } else {
                        parser = getDocumentBuilderFactory().newDocumentBuilder();
                    }
                }
                
                bis = new BufferedInputStream(strDocument);
            	in = new InputStreamReader(bis);         	
            	InputSource inputSource = new InputSource(in);      
                d = parser.parse(inputSource);
            } catch (Exception e) {
                throw new JAXPException(e);
            }
            finally {
                if (bis != null)  
                {
                    bis.close();
                }  
                if (in != null) 
                {
                	in.close();
                }
            }
        }
        return d;
    }
    
    /**
     * Creates an empty DOM document
     * @return empty DOM document
     * @throws com.ftd.osp.utilities.xml.JAXPException
     */
    public static Document createDocument() throws JAXPException {
        return createDocument(false,false);
    }

    /**
     * Creates an empty DOM document
     * @param validate should validating be turned on
     * @return empty DOM document
     * @throws com.ftd.osp.utilities.xml.JAXPException
     */
    public static Document createDocument(boolean validate, boolean namespaceAware) throws JAXPException {
        Document d = null;
        try {
            DocumentBuilder parser = null;
            if( validate ) {
                if( namespaceAware ) {
                    parser = getDocumentNsValBuilderFactory().newDocumentBuilder();
                } else {
                    parser = getDocumentNsBuilderFactory().newDocumentBuilder();
                }
            } else {
                if( namespaceAware ) {
                    parser = getDocumentNsBuilderFactory().newDocumentBuilder();
                } else {
                    parser = getDocumentBuilderFactory().newDocumentBuilder();
                }
            }
            d = parser.newDocument();
        } catch (Exception e) {
            throw new JAXPException(e);
        }
        return d;
    }

    /**
     * Creates an empty DOM document that is non-validating and does not know about namespaces
     * @return empty DOM document
     * @throws com.ftd.osp.utilities.xml.JAXPException
     */
    public static Document createDocumentNSAwareNV() throws JAXPException {
        Document d = null;
        try {
            DocumentBuilder parser = 
                getDocumentNsBuilderFactory().newDocumentBuilder();
            d = parser.newDocument();
        } catch (Exception e) {
            throw new JAXPException(e);
        }
        return d;
    }

    /**
     * Get the text for the passed element and return the boolean
     * representation of the text value
     * @param e element containing the text
     * @return boolean value of the text
     */
    public static boolean getBooleanValue(Element e) {
        boolean retval;

        if (e == null) {
            retval = false;
        } else {
            retval = 
                    BooleanUtils.toBoolean(StringUtils.trimToEmpty(e.getNodeValue()));
        }

        return retval;
    }


    /**
         * Description:     Takes a tag name and text and creates a simple xml node
         * @param           doc parent XML document
         * @param:          tag the xml tag name
         * @param:          text the value to be inserted into the node
         * @return:         xml element
         */
    public static Element buildSimpleXmlNode(Document doc, String tag, 
                                             String text) {
        Element retElem = doc.createElement(tag);

        retElem.appendChild(doc.createTextNode(StringUtils.trimToEmpty(text)));
        return retElem;
    }

    /**
         * Description:     Takes a tag name and text and creates a simple xml node
         *                  with CDATA.
         * @param           doc parent document
         * @param           tag the xml tag name
         * @param           text the value to be inserted into the node
         * @return          xml element
         */
    public static Element buildSimpleXmlNodeCDATA(Document doc, String tag, 
                                                  String text) {
        Element retElem = doc.createElement(tag);
        retElem.appendChild(doc.createCDATASection(StringUtils.trimToEmpty(text)));
        return retElem;
    }


    /**
         * Description:     Takes a tag name and boolean value and creates a simple xml node
         * @param           tag the xml tag name
         * @param           value the value to be inserted into the node
         * @return          xml element
         */
    public static Element buildSimpleXmlNode(Document doc, String tag, 
                                             boolean value) {
        Element retElem = doc.createElement(tag);
        retElem.appendChild(doc.createTextNode(String.valueOf(value)));
        return retElem;
    }


    /**
         * Description:     Takes a tag name and int value and creates a simple xml node
         * @param           doc parent document
         * @param           tag the xml tag name
         * @param           value the value to be inserted into the node
         * @return          xml element
         */
    public static Element buildSimpleXmlNode(Document doc, String tag, 
                                             int value) {
        Element retElem = doc.createElement(tag);
        retElem.appendChild(doc.createTextNode(String.valueOf(value)));
        return retElem;
    }


    /**
         * Description:     Takes a tag name and float value and creates a simple xml node
         * @param          tag the xml tag name
         * @param          value the value to be inserted into the node
         * @return         xml node
         */
    public static Element buildSimpleXmlNode(Document doc, String tag, 
                                             float value) {
        Element retElem = doc.createElement(tag);
        retElem.appendChild(doc.createTextNode(String.valueOf(value)));
        return retElem;
    }

    /**
     * Execute the passed xpath query and return the first element found
     * @param doc to search
     * @param xpathQuery
     * @return found element
     * @throws XPathExpressionException
     */
    public static Element selectSingleNode(Document doc, 
                                           String xpathQuery) throws XPathExpressionException {
        NodeList set = selectNodes(doc,xpathQuery);
        return (Element)set.item(0);
    }

    /**
     * Execute the passed xpath query and return the first element found
     * @param doc to search
     * @param xpathQuery
     * @param namespacePrefix
     * @param namespaceURI default namespace
     * @return found element
     * @throws XPathExpressionException
     */
    public static Element selectSingleNode(Document doc, 
                                           String xpathQuery,
                                           String namespacePrefix,
                                           String namespaceURI) throws XPathExpressionException {
        NodeList set = selectNodes(doc,xpathQuery,namespacePrefix,namespaceURI);
        return (Element)set.item(0);
    }

    /**
     * Execute the passed xpath query and return the text value of the element found
     * @param node to search
     * @param xpathQuery
     * @param namespacePrefix
     * @param namespaceURI default namespace
     * @return found element
     * @throws XPathExpressionException
     */
    public static String selectSingleNodeText(Node node, 
                                           String xpathQuery,
                                           String namespacePrefix,
                                           String namespaceURI) throws XPathExpressionException {

        javax.xml.xpath.XPath xpath = getXPathFactory().newXPath();
        if( StringUtils.isNotBlank(namespaceURI)) {
            JAXPNamespaceContextHelper helper = 
                new JAXPNamespaceContextHelper(StringUtils.isBlank(namespacePrefix)?"":namespacePrefix,namespaceURI);
            xpath.setNamespaceContext(helper);
        }
        
        synchronized(node)
        {
            return xpath.evaluate(xpathQuery,node);
        }
    }


    /**
     * Execute the passed xpath query and return the first element found
     * @param element to search
     * @param xpathQuery
     * @return found element
     * @throws XPathExpressionException
     */
    public static Element selectSingleNode(Element element, 
                                           String xpathQuery) throws XPathExpressionException {
        NodeList set = selectNodes(element,xpathQuery);
        return (Element)set.item(0);
    }

    /**
     * Execute the passed xpath query and return the first element found
     * @param element to search
     * @param xpathQuery
     * @param namespacePrefix
     * @param namespaceURI default namespace
     * @return found element
     * @throws XPathExpressionException
     */
    public static Element selectSingleNode(Element element, 
                                           String xpathQuery,
                                           String namespacePrefix,
                                           String namespaceURI) throws XPathExpressionException {
        NodeList set = selectNodes(element,xpathQuery,namespacePrefix,namespaceURI);
        return (Element)set.item(0);
    }

    /**
     * Execute the passed xpath query and return all elements found
     * @param node to search
     * @param xpathQuery
     * @return found elements
     * @throws XPathExpressionException
     */
    public static NodeList selectNodes(Node node, 
                                      String xpathQuery) throws XPathExpressionException {

        javax.xml.xpath.XPath xpath = getXPathFactory().newXPath();
        javax.xml.xpath.XPathExpression expression = xpath.compile(xpathQuery);
        
        synchronized(node)
        {
            return (NodeList)expression.evaluate(node, XPathConstants.NODESET);
        }
    }

    /**
     * Execute the passed xpath query and return all elements found
     * @param node to search
     * @param xpathQuery
     * @param namespacePrefix
     * @param namespaceURI default namespace
     * @return found elements
     * @throws XPathExpressionException
     */
    public static NodeList selectNodes(Node node, 
                                      String xpathQuery,
                                      String namespacePrefix,
                                      String namespaceURI) throws XPathExpressionException {

        javax.xml.xpath.XPath xpath = getXPathFactory().newXPath();
        if( StringUtils.isNotBlank(namespaceURI)) {
            JAXPNamespaceContextHelper helper = 
                new JAXPNamespaceContextHelper(StringUtils.isBlank(namespacePrefix)?"":namespacePrefix,namespaceURI);
            xpath.setNamespaceContext(helper);
        }
        javax.xml.xpath.XPathExpression expression = xpath.compile(xpathQuery);
        
        synchronized(node)
        {
            return (NodeList)expression.evaluate(node, XPathConstants.NODESET);
        }
    }

    /**
       * Extracts the text value of a node. If one does not exist,
       * it returns an empty string.
       *
       * @param node The Node object containing the text.
       * @return String
       **/
    public static String getTextValue(Node node) {
        String retval = null;
        
        if( node!=null ) {
            Node textNode = node.getFirstChild();
            if( textNode!=null ) {
                retval = StringUtils.trimToEmpty(textNode.getNodeValue());
            }
        }
        
        return retval == null ? "" : retval;
    }
    
    public static String getFirstChildNodeTextByTagName(Element element, String tagname) {
        String retval = null;
        
        if( element != null) {
            NodeList kids = element.getElementsByTagName(tagname);
            if( kids!=null && kids.getLength()>0 ) {
                retval = getTextValue(kids.item(0));
            }
        }
        
        return retval == null ? "" : retval;
    }

    /**
     * Adds a attribute to the specified element.  Checks for null values.
     * @param element target elements
     * @param attributeName 
     * @param value
     */
    public static void addAttribute(Element element, String attributeName, String value) {
        element.setAttribute(attributeName,StringUtils.isBlank(value)?"":value);
    }
    
    
    /**
     * Return a new instance each time. Per JDK spec, TransformerFactory is not guranteed thread safe.
     */
    private static TransformerFactory getTransformerFactory()
    {
        return TransformerFactory.newInstance();
    }
    
    /**
     * Return a new instance each time. Per JDK spec, XPathFactory is not reentrant or thread safe.
     */
    private static XPathFactory getXPathFactory()
    {
        return XPathFactory.newInstance();
    }    
}
