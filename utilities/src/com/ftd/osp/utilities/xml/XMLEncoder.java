package com.ftd.osp.utilities.xml;

import java.util.HashMap;
import java.util.Iterator;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class XMLEncoder
{

    /**
     * @deprecated use the DOMUtil class
     */
    public static Document createDocument(String type) throws Exception
    {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.newDocument();
        
        Element root = (Element) document.createElement("root");
        root.setAttribute("type", type);
        document.appendChild(root);
        
        return document;
    }

    /**
     * @deprecated use the DOMUtil class
     */
    public static void addSection(Document document, NodeList nodeList) throws Exception
    {
        for(int i = 0; i < nodeList.getLength(); i++)
        {
            document.getDocumentElement().appendChild(document.importNode(nodeList.item(i), true));
        }
    }

    /**
     * @deprecated use the DOMUtil class
     */
    public static void addSection(Document document, String elementName, String entry, HashMap values) throws Exception
    {
        Element element = (Element) document.createElement(elementName);
        Element tmpElement = null;
        String key = null;

        for (Iterator keyIterator = (values.keySet()).iterator(); keyIterator.hasNext(); )
        {
            tmpElement = (Element) document.createElement(entry);
            key = (String)keyIterator.next();
            tmpElement.setAttribute("name", key);
            tmpElement.setAttribute("value", (String)values.get(key));
            element.appendChild(tmpElement);
        }

        document.getDocumentElement().appendChild(element);
    }

	/**
     * @deprecated use the DOMUtil class
     */
	public static String encodeChars(String string)
	{
        if ( string == null || string.equals("") )  
        {
            return "";
        }
    
		char[] characters = string.toCharArray();
		StringBuffer encoded = new StringBuffer();
		String escape;
		
		for(int i = 0;i<string.length();i++)
		{
			escape = escapeChar(characters[i]);
			if(escape == null) 
            {
                encoded.append(characters[i]);
            }
			else 
            {
                encoded.append(escape);
            }
		}
		
		return encoded.toString();
	}

    private static String escapeChar(char c)
	{
        switch(c)
		{
			case('<')  : return "&lt;";
			case('>')  : return "&gt;";  
			case('&')  : return "&amp;"; 
			case('\'') : return "&#39;"; 
			case('"') : return "&quot;";                   
		} 
		return null;    
	}

}