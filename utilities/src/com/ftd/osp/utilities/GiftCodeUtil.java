package com.ftd.osp.utilities;

import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.http.ParseException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicNameValuePair;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;


import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.stats.ServiceResponseTrackingUtil;
import com.ftd.osp.utilities.vo.CreateBatchRequest;
import com.ftd.osp.utilities.vo.GiftCodeUpdateBulkReq;
import com.ftd.osp.utilities.vo.GiftCodeUpdateBulkResp;
import com.ftd.osp.utilities.vo.IssuedToRequestList;
import com.ftd.osp.utilities.vo.ReferenceCode;
import com.ftd.osp.utilities.vo.ReinstateInfoResponse;
import com.ftd.osp.utilities.vo.RetrieveGiftCodeResponse;
import com.ftd.osp.utilities.vo.ServiceResponseTrackingVO;
import com.ftd.osp.utilities.vo.StringResponse;
import com.ftd.osp.utilities.vo.UpdateBatchRequest;
import com.ftd.osp.utilities.vo.UpdateBatchResponse;
import com.ftd.osp.utilities.vo.UpdateGiftCodeRequest;
import com.ftd.osp.utilities.vo.UpdateGiftCodeResponse;
import com.ftd.osp.utilities.vo.UpdateSourceRequest;

public class GiftCodeUtil {

	private static Logger logger = new Logger(
			"com.ftd.osp.utilities.GiftCodeUtil");
	public static final String GIFT_CODE_SERVICE_URL = "GIFT_CODE_SERVICE_URL";
	public static final String GIFT_CODE_SERVICE_TIMEOUT = "GIFT_CODE_SERVICE_TIMEOUT";
	private static final String GIFT_CODE_SERVICE = "Gift Code Service";
	private static final String GIFT_CODE_SERVICE_SECRET = "GIFT_CODE_SERVICE_SECRET";

	private static GiftCodeUtil util = new GiftCodeUtil();

	public static GiftCodeUtil getInstance() {
		return util;
	}

	private static String getURL(String company, String uri) {
		String giftCodeURL = RESTUtils.getConfigParamValue(GIFT_CODE_SERVICE_URL, false);
		String URL = giftCodeURL.concat(company + uri);
		logger.info("Request URL : " + URL);
		return URL.trim();
	}

	private static String executeRequest(HttpUriRequest request) throws Exception {
		int timeout = Integer.parseInt(RESTUtils.getConfigParamValue(GIFT_CODE_SERVICE_TIMEOUT, false)) * 1000; // Convert seconds to ms.
		RESTUtils utils = new RESTUtils("Gift Code", false, false);
		
			return utils.executeRequest(request,  
				timeout, 
				RESTUtils.getConfigParamValue(GIFT_CODE_SERVICE_SECRET, true));
		
	}

	/**
	 * Calls Gift Code Service to retrieve Gift Code Id details
	 * 
	 * @param String
	 *            giftCodeId
	 * @return RetrieveGiftCodeResponse retrieveGiftCodeResponse
	 * @throws Exception 
	 */
	public static RetrieveGiftCodeResponse getGiftCodeById(String giftCodeId) throws Exception {
		// Start timer for service call
		long startTime = (new Date()).getTime();
		logger.info("getGiftCodeById : Gift Code Service request started at "
				+ System.currentTimeMillis());
		HttpGet httpGet = new HttpGet(getURL("", "retrieve?giftCodeId="
				+ giftCodeId.trim()));

		String responseString = executeRequest(httpGet);
		RetrieveGiftCodeResponse retrieveGiftCodeResponse = null;
		logger.info("Response String : "+responseString);
		if (null != responseString) {
			retrieveGiftCodeResponse = convertToRetrieveSingleObject(responseString);
		}
		logger.info("Response object : " + retrieveGiftCodeResponse);

		logServiceResponseTracking(giftCodeId, "getGiftCodeById", startTime);

		return retrieveGiftCodeResponse;
	}
	
	/**
	 * Calls Gift Code Service to retrieve latest reinstate details from the gc history table
	 * 
	 * @param String giftCodeId
	 * @param String orderDetailId
	 * @return ReinstateInfoResponse reinstateInfoResponse
	 * @throws Exception 
	 */
	public static ReinstateInfoResponse getGiftCodeReinstateHistoryById(String giftCodeId, String orderDetailId) throws Exception {
		// Start timer for service call
		long startTime = (new Date()).getTime();
		logger.info("getGiftCodeReinstateHistoryById : Gift Code Service request started at "
				+ System.currentTimeMillis());
		HttpGet httpGet = new HttpGet(getURL("", "reinstateInfo?giftCodeId="
				+ giftCodeId.trim()) +"&orderDetailId=" + orderDetailId);

		String responseString = executeRequest(httpGet);
		ReinstateInfoResponse reinstateInfoResponse = null;
		logger.info("Response String : "+responseString);
		if (null != responseString) {
			reinstateInfoResponse = convertToReinstateInfoSingleObject(responseString);
		}
		logger.info("Response object : " + reinstateInfoResponse);

		logServiceResponseTracking(giftCodeId, "getGiftCodeById", startTime);

		return reinstateInfoResponse;
	}

	/**
	 * Calls Gift Code Service to update status to Redeemed
	 * 
	 * @param updateGiftCodeRequest
	 * @param orderDetailId
	 * @return String redeemStatus
	 */
	public static String updateGiftCodeStatus(
			UpdateGiftCodeRequest updateGiftCodeRequest, String orderDetailId)
			throws Exception {
		logger.info("updateGiftCodeStatus : Gift Code Service request started at "
				+ System.currentTimeMillis());
		// Start timer for service call
		long startTime = (new Date()).getTime();
		HttpPost httpPost = null;
		UpdateGiftCodeResponse updateGiftCodeResponse = new UpdateGiftCodeResponse();
		String redeemStatus = "Invalid";

		// Retrieve the Gift Code status.
		// If the status equals Reinstate or Issued Active, then call Gift Code
		// Service to Redeem the Gift Code.
		// If the status equals Redeemed, then set status = Already Redeemed and
		// return the status.
		// If the Gift Code is expired or cancelled, or the Gift Code Id is not
		// found then set status = Invalid and return the status
		if (updateGiftCodeRequest.getStatus().equalsIgnoreCase("Reinstate")
				|| updateGiftCodeRequest.getStatus().equalsIgnoreCase(
						"Issued Active")) {
			httpPost = new HttpPost(getURL("", "update"));
			updateGiftCodeRequest.setStatus("Redeemed");
			String requestJsonString = convertToJSonString(updateGiftCodeRequest);
			logger.info("REQUEST:  " + requestJsonString);
			httpPost.setEntity(new StringEntity(requestJsonString, "utf-8"));

			String responseString = executeRequest(httpPost);

			updateGiftCodeResponse = convertToUpdateSingleObject(responseString);
			logger.info("updateResponse = " + updateGiftCodeResponse);

			redeemStatus = updateGiftCodeResponse.getStatus();

		} else if (updateGiftCodeRequest.getStatus().equalsIgnoreCase(
				"Redeemed")) {
			redeemStatus = "Already Redeemed";
		} else {
			redeemStatus = "Invalid";
		}
		logServiceResponseTracking(updateGiftCodeRequest.getGiftCodeId(),
				"updateGiftCodeStatus", startTime);
		return redeemStatus;
	}

	/**
	 * Calls Gift Code Service to update status 
	 * 
	 * @param updateGiftCodeRequest
	 * @param orderDetailId
	 * @return String status
	 */
	public static String adminUpdateStatus(UpdateGiftCodeRequest updateGiftCodeRequest) throws Exception {
		logger.info("updateRedeemed : Gift Code Service request started at " + System.currentTimeMillis());
		// Start timer for service call
		long startTime = (new Date()).getTime();
		HttpPost httpPost = null;
		UpdateGiftCodeResponse updateGiftCodeResponse = new UpdateGiftCodeResponse();
		String status = "Invalid";

		httpPost = new HttpPost(getURL("", "adminUpdate"));
		
		String requestJsonString = convertToJSonString(updateGiftCodeRequest);
		logger.info("REQUEST:  " + requestJsonString);
		httpPost.setEntity(new StringEntity(requestJsonString, "utf-8"));

		String responseString = executeRequest(httpPost);

		updateGiftCodeResponse = convertToUpdateSingleObject(responseString);
		logger.info("updateResponse = " + updateGiftCodeResponse);

		status = updateGiftCodeResponse.getStatus();

		logServiceResponseTracking(updateGiftCodeRequest.getGiftCodeId(),
				"updateRedeemed", startTime);
		return status;
	}
	
	/**
	 * Calls Gift Code Service for End of day Jobs 
	 * 
	 * @param updateGiftCodeRequest
	 * @param orderDetailId
	 * @return String status
	 */
	public static void gcMaintenance(String type) throws Exception {
		logger.info("Gift Code Maintenance : Gift Code Service request started at " + System.currentTimeMillis());
		// Start timer for service call
		long startTime = (new Date()).getTime();
		HttpGet httpGet = null;
		httpGet = new HttpGet(getURL("", "gcmaintenance?type=" + type.trim()));
		executeRequest(httpGet);
		logServiceResponseTracking(null,"Maintenance Activity : " + type, startTime);
	}

	/**
	 * Converts Response String into Response Object
	 * 
	 * @param jsonString
	 * @return RetrieveGiftCodeResponseVO
	 */
	public static RetrieveGiftCodeResponse convertToRetrieveSingleObject(
			String jsonString) {
		RetrieveGiftCodeResponse retObj = new RetrieveGiftCodeResponse();
		ObjectMapper mapperObj = new ObjectMapper();
		try {
			retObj = mapperObj.readValue(jsonString,
					RetrieveGiftCodeResponse.class);
		} catch (IOException ex) {
		}
		return retObj;
	}

	/**
	 * Converts Response String into Reinstate Info Response Object
	 * 
	 * @param jsonString
	 * @return ReinstateInfoResponseVO
	 */
	public static ReinstateInfoResponse convertToReinstateInfoSingleObject(
			String jsonString) {
		ReinstateInfoResponse retObj = new ReinstateInfoResponse();
		ObjectMapper mapperObj = new ObjectMapper();
		try {
			retObj = mapperObj.readValue(jsonString,
					ReinstateInfoResponse.class);
		} catch (IOException ex) {
		}
		return retObj;
	}
	
	
	/**
	 * Converts Response String into Response Object
	 * 
	 * @param jsonString
	 * @return UpdateGiftCodeResponseVO
	 */
	public static UpdateGiftCodeResponse convertToUpdateSingleObject(
			String jsonString) {
		UpdateGiftCodeResponse retObj = new UpdateGiftCodeResponse();
		ObjectMapper mapperObj = new ObjectMapper();
		try {
			retObj = mapperObj.readValue(jsonString,
					UpdateGiftCodeResponse.class);
		} catch (IOException ex) {
		}
		return retObj;
	}
	
	public static UpdateBatchResponse convertToUpdateBatchSingleObject(
			String jsonString) {
		UpdateBatchResponse retObj = new UpdateBatchResponse();
		ObjectMapper mapperObj = new ObjectMapper();
		try {
			retObj = mapperObj.readValue(jsonString,
					UpdateBatchResponse.class);
		} catch (IOException ex) {
		}
		return retObj;
	}
	
	public static GiftCodeUpdateBulkResp convertToUpdateBulk(
			String jsonString) {
		GiftCodeUpdateBulkResp retObj = new GiftCodeUpdateBulkResp();
		ObjectMapper mapperObj = new ObjectMapper();
		try {
			retObj = mapperObj.readValue(jsonString,
					GiftCodeUpdateBulkResp.class);
		} catch (IOException ex) {
		}
		return retObj;
	}
	
	/**
	 * Get list of Gift Certificats only for a particular batch
	 * 
	 * @param requestNumber
	 * @return List of String arrays where first element each array 
	 *         is Gift Cert number and second element is the status
	 */
   public List<String[]> getGiftCodesOnly(String requestNumber) {
      List<String[]> returnCodeList = null;
      long startTime = (new Date()).getTime();
      List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
      if (requestNumber != null) {
         params.add(new BasicNameValuePair("batchNumber", requestNumber));
      } else {
         logger.error("missing coupon request number");
      }

      String url = getURL("", "retrieveGiftCodeIds").concat("?").concat(
            URLEncodedUtils.format(params, "utf-8"));
      HttpGet get = new HttpGet(url);

      try {
         String rawResp = executeRequest(get);
         logger.info(rawResp);
         returnCodeList = convertGiftCodesToList(rawResp);
         
      } catch (Exception e) {
         logger.error("Error reading from HTTP response" + e.getMessage());
      }
      GiftCodeUtil.logServiceResponseTracking(requestNumber, "getGiftCodesOnly", startTime);
      return returnCodeList;
   }
	
	/**
	 * Parse JSON elements into list of two-element arrays (cert & status)
	 * 
	 * @param jsonString
    * @return List of String arrays where first element in each array 
    *         is Gift Cert number and second element is the status
	 * @throws Exception
	 */
   private List<String[]> convertGiftCodesToList(String jsonString) throws Exception {
      List<String[]> codeList = null; 
      ObjectMapper mapperObj = new ObjectMapper();
      JsonNode rootNode = mapperObj.readTree(jsonString);
      JsonNode detailsNode = rootNode.path("retrieveDetails");
      if (detailsNode.isArray()) {
         codeList = new ArrayList<String[]>(); 
         for (JsonNode node : detailsNode) {
            String[] codeAndStatus = {node.path("giftCodeId").asText(), node.path("status").asText()};
            codeList.add(codeAndStatus);
         }
      }
      return codeList;
   }

   
	
	public static Document getXMLDocument(String xmlString)
			throws ParserConfigurationException, SAXException, IOException {
		Document rd = null;
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		if (null != xmlString && !xmlString.isEmpty()) {
			rd = builder.parse(new InputSource(new StringReader(xmlString)));
		} else {
			rd = builder.newDocument();
		}
		return rd;
	}

	/**
	 * This method gets the value of the tagname passed for that element
	 * 
	 * @param parentElement
	 *            Document
	 * @param tagName
	 *            String
	 * @return String
	 * @throws java.lang.Exception
	 */
	public static String getStringValueFromXML(Document parentElement,
			String tagName) throws Exception {
		if (null != tagName && !tagName.isEmpty()) {
			NodeList nodeList = parentElement.getElementsByTagName(tagName);
			if (nodeList.getLength() == 0)
				return null;
			Element nodeElement = (Element) nodeList.item(0);
			NodeList textNodeList = nodeElement.getChildNodes();
			if (textNodeList.getLength() == 0)
				return null;
			return ((Node) textNodeList.item(0)).getNodeValue().trim();
		}
		return new String();
	}

	/**
	 * This method converts and object to a json string
	 * 
	 * @param Object
	 *            req
	 * @return String
	 * @throws java.lang.Exception
	 */
	public static String convertToJSonString(Object req) {
		logger.debug("Req : " + req);
		String jsonString = null;
		ObjectMapper mapperObj = new ObjectMapper();
		try {
			jsonString = mapperObj.writeValueAsString(req);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return jsonString;
	}


	public static void logServiceResponseTracking(String requestId,
			String serviceMethod, long startTime) {
		Connection connection = null;
		try {
			long responseTime = System.currentTimeMillis() - startTime;
			ServiceResponseTrackingVO srtVO = new ServiceResponseTrackingVO();
			srtVO.setServiceName(GIFT_CODE_SERVICE);
			srtVO.setServiceMethod(serviceMethod);
			srtVO.setTransactionId(requestId);
			srtVO.setResponseTime(responseTime);
			srtVO.setCreatedOn(new Date());

			connection = DataSourceUtil.getInstance().getConnection(
					"ORDER SCRUB");
			ServiceResponseTrackingUtil srtUtil = new ServiceResponseTrackingUtil();
			srtUtil.insertWithRequest(connection, srtVO);
		} catch (Exception e) {
			logger.error("Exception occured while persisting Gift Code Service response times.");
		}

	}

	/**
	 * Converts Response String into Response Object
	 * 
	 * @param s
	 * @return GiftCodeUpdateBatchResponseVO
	 */
	public static StringResponse convertToObject(String s) {
		StringResponse obj = new StringResponse();
		ObjectMapper mapperObj = new ObjectMapper();
		try {
			obj = mapperObj.readValue(s, StringResponse.class);
		} catch (IOException ex) {
		}
		return obj;
	}

	public StringResponse createGiftCertificate(
			CreateBatchRequest batchRequest, String company) throws Exception {
		long startTime = (new Date()).getTime();
		HttpPost post = null;
		String requestJsonString = GiftCodeUtil
				.convertToJSonString(batchRequest);
		if(batchRequest.getCreateDetails().getQuantity() <= 25){
			post = new HttpPost(getURL("", "create"));
		}else {
			post = new HttpPost(getURL("", "publish"));
		}
		StringResponse resp = null;
		try {
			post.setEntity(new StringEntity(requestJsonString, "utf-8"));
			resp = GiftCodeUtil.convertToObject(executeRequest(post));
		} catch (ParseException e) {
			logger.error("Error reading from HTTP response" + e.getMessage());
		} catch (IOException e) {
			logger.error("Error reading from HTTP response" + e.getMessage());
		}

		GiftCodeUtil.logServiceResponseTracking(batchRequest.getCreateDetails()
				.getBatchNumber(), "createGiftCertificate", startTime);
		return resp;
	}

	public static Map<String, String> callUpdateSourceCode(String couponNumber,
			String requestNumber, String updatedBy,
			List<ReferenceCode> codeList, String company) throws Exception {
		HttpPost post = new HttpPost(getURL("", "updateSource"));
		UpdateSourceRequest updateSourceRequest = new UpdateSourceRequest();
		updateSourceRequest.setGiftCodeId(couponNumber);
		updateSourceRequest.setBatchNumber(requestNumber);
		updateSourceRequest.setUpdatedBy(updatedBy);
		updateSourceRequest.setSource(codeList);
		String requestJsonString = GiftCodeUtil
				.convertToJSonString(updateSourceRequest);
		logger.debug("SOURCE REQUEST : "+requestJsonString);
		try {
			post.setEntity(new StringEntity(requestJsonString));
		} catch (UnsupportedEncodingException e) {
			logger.error(e.getStackTrace().toString());
		}

		executeRequest(post);
		return null;
	}

	/**
	 * Calls Gift Code Service to update status to Redeemed if it's not already
	 * and adds the redeemed reference number(s) to the gift certificate
	 * 
	 * @param updateGiftCodeRequest
	 * @param orderDetailId
	 * @return String redeemStatus
	 */
	public static String update(UpdateGiftCodeRequest updateGiftCodeRequest)
			throws Exception {
		logger.info("update : Gift Code Service request started at "
				+ System.currentTimeMillis());
		// Start timer for service call
		long startTime = (new Date()).getTime();
		HttpPost httpPost = null;
		UpdateGiftCodeResponse updateGiftCodeResponse = new UpdateGiftCodeResponse();
		String status;

		// Retrieve the Gift Code status.
		// If the status equals Reinstate or Issued Active, then call Gift Code
		// Service to Redeem the Gift Code.
		// If the status equals Redeemed, then set status = Already Redeemed and
		// return the status.
		// If the Gift Code is expired or cancelled, or the Gift Code Id is not
		// found then set status = Invalid and return the status

		httpPost = new HttpPost(getURL("", "adminUpdate"));
		String requestJsonString = convertToJSonString(updateGiftCodeRequest);
		logger.info("REQUEST:  " + requestJsonString);
		try {
			httpPost.setEntity(new StringEntity(requestJsonString));
		} catch (UnsupportedEncodingException e) {
			logger.error(e.getStackTrace().toString());
		}
		String responseString = executeRequest(httpPost);

		updateGiftCodeResponse = convertToUpdateSingleObject(responseString);
		logger.info("updateResponse = " + updateGiftCodeResponse);

		status = updateGiftCodeResponse.getStatus();

		logServiceResponseTracking(updateGiftCodeRequest.getGiftCodeId(),
				"updated", startTime);
		return status;
	}
	
	
	/**
	 * Calls Gift Code Service to update status to Redeemed if it's not already
	 * and adds the redeemed reference number(s) to the gift certificate
	 * 
	 * @param updateGiftCodeRequest
	 * @param orderDetailId
	 * @return String redeemStatus
	 * @throws Exception 
	 */
	public static String updateBatch(UpdateBatchRequest updateBatchRequest) throws Exception {
		UpdateBatchResponse response = new UpdateBatchResponse();
		logger.info("update : Gift Code Service request started at "
				+ System.currentTimeMillis());
		// Start timer for service call
		long startTime = (new Date()).getTime();
		HttpPost httpPost = null;
		String batchNumber;

		// Retrieve the Gift Code status.
		// If the status equals Reinstate or Issued Active, then call Gift Code
		// Service to Redeem the Gift Code.
		// If the status equals Redeemed, then set status = Already Redeemed and
		// return the status.
		// If the Gift Code is expired or cancelled, or the Gift Code Id is not
		// found then set status = Invalid and return the status

		httpPost = new HttpPost(getURL("", "updateBatch"));
		String requestJsonString = GiftCodeUtil
		.convertToJSonString(updateBatchRequest);
		logger.info("REQUEST:  " + requestJsonString);
		try {
			httpPost.setEntity(new StringEntity(requestJsonString));
		} catch (UnsupportedEncodingException e) {
			logger.error(e.getStackTrace().toString());
		}
		String responseString = executeRequest(httpPost);

		response = convertToUpdateBatchSingleObject(responseString);
		logger.info("updateBatchResponse = " + response);

		batchNumber = response.getBatchNumber();

		logServiceResponseTracking(batchNumber,
				"updatedBatch", startTime);
		return batchNumber;
	}
	
	public static Integer updateBulk(GiftCodeUpdateBulkReq giftCodeUpdateVO) throws Exception {
		GiftCodeUpdateBulkResp response = new GiftCodeUpdateBulkResp();
		logger.info("update : Gift Code Service request started at "
				+ System.currentTimeMillis());
		// Start timer for service call
		long startTime = (new Date()).getTime();
		HttpPost httpPost = null;
		Integer count;

		// Retrieve the Gift Code status.
		// If the status equals Reinstate or Issued Active, then call Gift Code
		// Service to Redeem the Gift Code.
		// If the status equals Redeemed, then set status = Already Redeemed and
		// return the status.
		// If the Gift Code is expired or cancelled, or the Gift Code Id is not
		// found then set status = Invalid and return the status

		httpPost = new HttpPost(getURL("", "updateBulk"));
		String requestJsonString = GiftCodeUtil
		.convertToJSonString(giftCodeUpdateVO);
		logger.info("REQUEST:  " + requestJsonString);
		try {
			httpPost.setEntity(new StringEntity(requestJsonString));
		} catch (UnsupportedEncodingException e) {
			logger.error(e.getStackTrace().toString());
		}
		String responseString = executeRequest(httpPost);

		response = convertToUpdateBulk(responseString);
		logger.info("updateBulkResponse = " + response);

		count = response.getGiftCertCount();

		logServiceResponseTracking(count.toString(),
				"updatedBulk", startTime);
		return count;
	}

	public static void updateRecipient(IssuedToRequestList request) throws Exception {
		long startTime = (new Date()).getTime();
		HttpPost post = new HttpPost(getURL("", "updateIssuedTo"));
		String requestJsonString = GiftCodeUtil.convertToJSonString(request);
		logger.debug("REQUEST:  " + requestJsonString);
		try {
			post.setEntity(new StringEntity(requestJsonString));
		} catch (UnsupportedEncodingException e) {
			logger.error(e.getStackTrace().toString());
		}

		executeRequest(post);
		
		logServiceResponseTracking(request.getBatchNumber(),
				"updatedRecipient", startTime);
	}

	public String searchGiftCertificate(String requestNumber,
			String couponNumber, String company) throws Exception {
		long startTime = (new Date()).getTime();
		List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
		if (couponNumber != null && !couponNumber.isEmpty()) {
			params.add(new BasicNameValuePair("giftCodeId", couponNumber.trim()));
		} else if (requestNumber != null) {
			params.add(new BasicNameValuePair("batchNumber", requestNumber.trim()));
		} else {
			logger.error("missing coupon number and request number");
		}

		String url = getURL("", "retrievebatch").concat("?").concat(
				URLEncodedUtils.format(params, "utf-8"));
		HttpGet get = new HttpGet(url);

		String resp = null;
		try {
			resp = executeRequest(get);
		} catch (ParseException e) {
			logger.error("Error reading from HTTP response" + e.getMessage());
		}
		GiftCodeUtil.logServiceResponseTracking(requestNumber + ":"
				+ couponNumber, "searchGiftCertificate", startTime);
		return resp;
	}

	public String getSingleStatusGiftCertificate(String couponNumber,
			String company) throws Exception {
		long startTime = (new Date()).getTime();

		List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
		params.add(new BasicNameValuePair("giftCodeId", couponNumber.trim()));

		String url = getURL("", "getStatus").concat("?").concat(
				URLEncodedUtils.format(params, "utf-8"));
		HttpGet get = new HttpGet(url);
		String resp = null;
		try {
			resp = executeRequest(get);
		} catch (ParseException e) {
			logger.error("Error reading from HTTP response" + e.getMessage());
		}
		GiftCodeUtil.logServiceResponseTracking(couponNumber.trim(),
				"searchGiftCertificate", startTime);
		return resp;
	}

	/**
	 * Update Batch Request converted to Json String for Micro service
	 * 
	 * @param jsonString
	 * @return UpdateBatchResponse
	 */
	public static UpdateBatchResponse convertToUpdateBatchObject(
			String jsonString) {
		UpdateBatchResponse retObj = new UpdateBatchResponse();
		ObjectMapper mapperObj = new ObjectMapper();
		try {
			retObj = mapperObj.readValue(jsonString, UpdateBatchResponse.class);
		} catch (IOException ex) {
		}
		return retObj;
	}

	/**
	 * Update Source request converted to Json String for Micro service.
	 * 
	 * @param sourceRespString
	 * @return StringResponse
	 */
	public static StringResponse convertToUpdateSourceObject(
			String sourceRespString) {
		StringResponse retObj = new StringResponse();
		ObjectMapper mapperObj = new ObjectMapper();
		try {
			retObj = mapperObj
					.readValue(sourceRespString, StringResponse.class);
		} catch (IOException ex) {
		}
		return retObj;
	}
}