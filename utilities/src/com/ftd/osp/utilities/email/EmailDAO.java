package com.ftd.osp.utilities.email;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;

import java.math.BigDecimal;

import java.sql.Connection;

import java.util.HashMap;
import java.util.Map;


public class EmailDAO
{
    private Connection connection;

    private static final String EMAIL_GUID_KEY = "EMAIL_GUID";
    private static final String INSERT_EMAIL_CONTENT = "FRP.INSERT_EMAIL_CONTENT";
    private static final String INSERT_EMAIL_CONFIRMATION = "FRP.INSERT_EMAIL_CONFIRMATION";
    private static final String VIEW_EMAIL_CONFIRMATION = "FRP.VIEW_EMAIL_CONFIRMATION_INFO";
    private static final String STATUS_PARAM = "RegisterOutParameterStatus";
    private static final String MESSAGE_PARAM = "RegisterOutParameterMessage";
    private static final String EMAIL_CONFIRMATION_CURSOR = "RegisterOutParameterEmailConfirmationcur";
    private static final String EMAIL_ORDER_CONTENT_CURSOR = "RegisterOutParameterEmailOrderContentcur";
    private static final String EMAIL_SECTION_CONTENT_CURSOR = "RegisterOutParameterEmailSectionContentcur";

    public EmailDAO(Connection connection)
    {
        this.connection = connection;
    }

    public EmailVO getEmail(String emailGuid) throws Exception
    {
        EmailVO email = new EmailVO();
      
        // Get the email confirmation information
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID(VIEW_EMAIL_CONFIRMATION);
        dataRequest.addInputParam(EMAIL_GUID_KEY, emailGuid);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map emailMap = (Map) dataAccessUtil.execute(dataRequest);

        CachedResultSet rs = (CachedResultSet) emailMap.get(EMAIL_CONFIRMATION_CURSOR);
        while(rs != null && rs.next())
        {
            email.setGuid((String)rs.getObject(1));
            email.setSender((String)rs.getObject(4));
            email.setRecipient((String)rs.getObject(3));
            email.setSubject((String)rs.getObject(5));
        }

        // Get email content
        String contentType = null;
        rs = (CachedResultSet) emailMap.get(EMAIL_ORDER_CONTENT_CURSOR);
        
        while(rs != null && rs.next())
        {
            contentType = rs.getObject(2).toString();
            if(contentType != null && contentType.equals("text"))
            {
                email.appendContent((String)rs.getObject(4));
            }
            else
            {
                email.appendHTMLContent((String)rs.getObject(4));
            }
        }

        return email;
    }

    public boolean saveEmail(EmailVO email) throws Exception
    {
        boolean success = true;
        
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID(INSERT_EMAIL_CONFIRMATION);
        dataRequest.addInputParam("IN_EMAIL_CONFIRMATION_GUID", email.getGuid());
        dataRequest.addInputParam("IN_EMAIL_STYLE_SHEET_ID" , new Integer(0));
        dataRequest.addInputParam("IN_TO_ADDRESS", email.getRecipient());
        dataRequest.addInputParam("IN_FROM_ADDRESS", email.getSender());
        dataRequest.addInputParam("IN_SUBJECT", email.getSubject());
        dataRequest.addInputParam("STATUS_PARAM", "");
        dataRequest.addInputParam("MESSAGE_PARAM", "");

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        String status = (String) outputs.get(STATUS_PARAM);

        if(status != null && status.equals("N"))
        {
            success = false;
            throw new Exception((String) outputs.get(MESSAGE_PARAM));
        }

        if(success)
        {
            if(email.getContent() != null && email.getContent().length() > 0)
            {
                String[] fullTextEmailSplit = splitContent(email.getContent());
                success = this.insertEmail(email.getGuid(), "text", fullTextEmailSplit);
            }

            if(email.getHTMLContent() != null && email.getHTMLContent().length() > 0)
            {
                String[] fullHTMLEmailSplit = splitContent(email.getHTMLContent());
                success = this.insertEmail(email.getGuid(), "html", fullHTMLEmailSplit);
            }
        }
        
        return success;
    }

    private boolean insertEmail(String guid, String contentType, String[] s) throws Exception
    {
        boolean success = true;

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        DataRequest dataRequest = new DataRequest();
        HashMap orderContent = null;
        String status = null;
        Map outputs = null;
        String text = null;
        
        for (int i = 0; i < s.length; i++)
        {
            orderContent = new HashMap();

            text = s[i];
            orderContent.put("IN_EMAIL_CONFIRMATION_GUID", guid);
            orderContent.put("IN_CONTENT_TYPE", contentType);
            orderContent.put("IN_SECTION", new Integer(i));
            orderContent.put("IN_CONTENT", text);
            orderContent.put("STATUS_PARAM", "");
            orderContent.put("MESSAGE_PARAM", "");

            dataRequest.setConnection(connection);
            dataRequest.setStatementID(INSERT_EMAIL_CONTENT);
            dataRequest.setInputParams(orderContent);

            outputs = (Map) dataAccessUtil.execute(dataRequest);
            status = (String) outputs.get(STATUS_PARAM);

            if(status.equals("N"))
            {
                success = false;
                throw new Exception((String) outputs.get(MESSAGE_PARAM));
            }
        }
        
        return success;
    }

    private String[] splitContent(String content) throws Exception
    {
        BigDecimal messageLength = new BigDecimal(new Integer(content.length()).toString());
        BigDecimal diff = new BigDecimal("0");
        BigDecimal chunk = new BigDecimal("2000");
        diff = messageLength.divide(chunk, BigDecimal.ROUND_UP);

        String htmlEmail[] = new String[diff.intValue()];

        char[] cbuf = new char[2000];

        int startpos = 0;
        int endpos = 2000;

        BigDecimal startValue = new BigDecimal("0");
        BigDecimal endValue = new BigDecimal("2000");
        BigDecimal incValue = new BigDecimal("2000");

        for(int i=0; i < htmlEmail.length;i++)
        {
            if(i == (htmlEmail.length - 1))
            {

                content.getChars(startpos, content.length(), cbuf, 0);
            }
            else
            {
                content.getChars(startpos, endpos, cbuf, 0);
            }

            htmlEmail[i] = new String(cbuf);

            BigDecimal d2 = startValue.add(incValue);
            startValue = d2;
            startpos = d2.intValue();

            d2 = endValue.add(incValue);
            endValue = d2;
            endpos = d2.intValue();
            cbuf = new char[2000];
        }

        return htmlEmail;
    }
}