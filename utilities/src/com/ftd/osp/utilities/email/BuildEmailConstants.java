package com.ftd.osp.utilities.email;

public class BuildEmailConstants 
{
    public static final String HTML = "HTML";
    public static final String PLAIN_TEXT = "PLAIN_TEXT";
    public static final String TEXT_FILTER = "TEXT";
    public static final String CONFIRMATION_EMAIL_TEXT = "CONFIRMATION_EMAIL_TEXT";
      
    public static final String CONFIRMATION_ACTION = "CONFIRMATION";
    public static final String REINSTATE_CONFIRMATION_ACTION = "REINSTATE_CONFIRMATION";
    public static final String PEND_REMOVE_ACTION = "PEND_REMOVE";
    public static final String PREVIEW_ACTION = "PREVIEW";
      
    public static final String JMS_STATUS = "ES1006";
    public static final String SEPARATOR = ":";
    public static boolean SEND_JMS_MSG = true;

    //database flag values
    public static final String MULTIPART_DB_FLAG = "MP";
    public static final String TEXT_DB_FLAG = "TXT";

    //procedures
    public static final String INSERT_EMAIL_CONFIRMATION = "FRP.INSERT_EMAIL_CONFIRMATION";
    public static final String INSERT_EMAIL_CONTENT = "FRP.INSERT_EMAIL_CONTENT";
    public static final String VIEW_CURRENT_EMAIL_SECTIONS = "FRP.VIEW_CURRENT_EMAIL_SECTIONS";
    public static final String VIEW_CONTENT_BY_PROGRAM = "FRP.VIEW_CONTENT_BY_PROGRAM";

    //Email ref cursors returned from database procedure
    public static final String EMAIL_CONTENT_CURSOR = "RegisterOutParameterEmailContentcur";

    //custom parameters returned from database procedure
    public static final String STATUS_PARAM = "RegisterOutParameterStatus";
    public static final String MESSAGE_PARAM = "RegisterOutParameterMessage";

    public String status;
    public String message;
    public boolean returnStatus;

    //personal greeting constants
    public static final String PERSONAL_GREETING_CONTEXT  = "PERSONAL_GREETINGS";
    public static final String PERSONAL_GREETING_IDENTIFIER = "IDENTIFIER";
    public static final String PERSONAL_GREETING_INSTRUCTIONS_HTML = "INSTRUCTIONS_HTML";
    public static final String PERSONAL_GREETING_INSTRUCTIONS_TEXT = "INSTRUCTIONS_TEXT";
    public static final String PERSONAL_GREETING_PHONE_NUMBER_FILTER = "RECORDING_PHONE";
    public static final String PERSONAL_GREETING_PHONE_NUMBER_TOKEN = "~pgphone~";
    
    //token replacement for emails
    public static final String SOURCE_CODE_TOKEN = "~sourcecode~";
    public static final String COMPANY_URL_TOKEN = "~companyurl~";
    public static final String LANGUAGE_TOKEN = "~language.message~";
    public static final String STOCK_MESSAGE_PHONE_NUMBER_TOKEN = "~phone~";
	
    //bin constants
    public static final String BIN_CONTEXT = "BIN";

    //customer tier constants
      
    public final static String PHONE_NUMBER_CONTEXT = "PHONE_NUMBER";
    public final static String CALL_CENTER_CONTACT_NUMBER = "CALL_CENTER_CONTACT_NUMBER";
    public final static String DEFAULT_TIER_FILTER = "DEFAULT";
    public final static String PHONE_NUMBER_TOKEN = "~customertierphone~";
    public final static String PREMIER_CALL_CENTER_CONTACT_NUMBER  = "PREMIER_CALL_CENTER_CONTACT_NUMBER";
    public final static String LANGUAGE_CONTEXT = "LANGUAGE";
    public final static String LANGUAGE_MESSAGE = "LANGUAGE.MESSAGE";
    public final static String COMPANY_ID = "COMPANY_ID";
    public final static String MEMBER_CALL_CENTER_CONTACT_NUMBER = "MEMBER_CALL_CENTER_CONTACT_NUMBER";
    
    //message content types
    public final static String SUBJECT = "Subject";
    public final static String HEADER = "Header";
    public final static String GUARANTEE = "Guarantee";
    public final static String MARKETING = "Marketing";
    public final static String CONTACT = "Contact";

    public final static String TEXT = "text";

    public final static String SUBJECT_CONTEXT = "subjectContent";
    public final static String HEADER_CONTENT = "headerContent";
    public final static String HEADER_CONTENT_HTML = "headerContentHtml";
    public final static String GUARANTEE_CONTENT = "guaranteeContent";
    public final static String GUARANTEE_CONTENT_HTML = "guaranteeContentHtml";
    public final static String MARKETING_CONTENT = "marketingContent";
    public final static String MARKETING_CONTENT_HTML = "marketingContentHtml";
    public final static String CONTACT_CONTENT = "contactContent";
    public final static String CONTACT_CONTENT_HTML = "contactContentHtml";
    public final static String FROM_ADDRESS = "fromAddress";
    public final static String GREETING_CONTENT = "greetingContent";
    public final static String GREETING_CONTENT_HTML = "greetingContentHtml";
    public final static String REFUND_CONTENT = "refundContent";
    public final static String REFUND_CONTENT_HTML = "refundContentHtml";
    public final static String END_CONTENT = "endContent";
    public final static String END_CONTENT_HTML = "endContentHtml";
    public final static String QUESTIONS_CONTENT = "questionsContent";
       
    public final static String SUBJECT_TEXT = "direct@hpftd1.ftdi.com~Amazon/FTD Order Shipping Issue";
    public final static String HEADER_TEXT = " \n\nWe are sorry to inform you that we could not ship your below listed FTD order placed  at Amazon.com on ";
    public final static String REFUND_TEXT = "We have initiated a refund for this order through Amazon.com. The refund should be credited to your account within 2 -3 business days. You can expect a separate notification about this from Amazon.com.";
    public final static String QUESTIONS_TEXT = "\n\nQUESTIONS? If you have questions about this order, including the status of your refund, you can visit [<A href=]http://www.amazon.com/wheres-my-stuff/[>www.amazon.com/wheres-my-stuff/<A>]. For floral delivery questions please visit [<A href=]http://www.ftd.com/amazonemail/[>www.ftd.com/amazonemail<A>] to get in touch directly with FTD.";
    public final static String END_TEXT = "\n\nThank you for shopping with FTD at Amazon.com.\n\n\nFTD";
    
    
}