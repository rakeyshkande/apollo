package com.ftd.osp.utilities.email;

import com.ftd.osp.framework.dispatcher.Dispatcher;
import com.ftd.osp.utilities.GUID.GUIDGenerator;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.j2ee.InitialContextUtil;
import com.ftd.osp.utilities.order.MercentOrderPrefixes;
import com.ftd.osp.utilities.order.ScrubMapperDAO;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.MessageToken;

import java.sql.Connection;

import java.util.HashMap;
import java.util.Map;

import javax.naming.InitialContext;


public class BuildOrderEmail
{
    private Connection connection;
    private Logger logger;

    private static final String VIEW_CONTENT_BY_PROGRAM = "FRP.VIEW_CONTENT_BY_PROGRAM";
    private final static String SUBJECT_CONTEXT = "subjectContent";
    private final static String HEADER_CONTENT = "headerContent";
    private final static String HEADER_CONTENT_HTML = "headerContentHtml";
    private final static String GUARANTEE_CONTENT = "guaranteeContent";
    private final static String GUARANTEE_CONTENT_HTML = "guaranteeContentHtml";
    private final static String MARKETING_CONTENT = "marketingContent";
    private final static String MARKETING_CONTENT_HTML = "marketingContentHtml";
    private final static String CONTACT_CONTENT = "contactContent";
    private final static String CONTACT_CONTENT_HTML = "contactContentHtml";
    private final static String FROM_ADDRESS = "fromAddress";
    private final static String SUBJECT = "Subject";
    private final static String HEADER = "Header";
    private final static String GUARANTEE = "Guarantee";
    private final static String MARKETING = "Marketing";
    private final static String CONTACT = "Contact";

    private final static String TEXT = "text";

 
    public BuildOrderEmail(Connection connection)
    {
        this.connection = connection;
        this.logger = new Logger("com.ftd.osp.utilities.email.BuildOrderEmail");
    }

    public void generatePreviewEmail(long programId,String runDate,String orderGuid,String toAddress, String comments) throws Exception
    {
        OrderVO order = null;

        BuildOrderEmailHelper helper = new BuildOrderEmailHelper(connection);

        //retrieve order from database        
        ScrubMapperDAO scrubMapperDAO = new ScrubMapperDAO(connection);
        order = scrubMapperDAO.mapOrderFromDB(orderGuid);

        //if there there are no order details then assume the order does not exist in DB
        if(order.getOrderDetail() == null || order.getOrderDetail().size() <= 0)
        {
          String msg = "Order GUID does not exist.  Update Email Admin config file with a vaild order. (" + orderGuid + ")";
          logger.error(msg);
          throw new Exception(msg);
        }

        // Get the program information
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID(VIEW_CONTENT_BY_PROGRAM);
        dataRequest.addInputParam("IN_PROGRAM_ID", new Long(programId));
        dataRequest.addInputParam("IN_DATE",helper.getSQLDate(runDate));
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        CachedResultSet rs = new CachedResultSet();
        rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
        if(rs == null || rs.getRowCount() <= 0)
        {
          throw new Exception("Program information does not exist for the entered date.");
        }

        //Place data from recordset into map
        Map sectionContentMap = getEmailSectionContent(rs);

        //Add preview comments
        String headerContent = (String)sectionContentMap.get(HEADER_CONTENT);
        String headerContentHtml = (String)sectionContentMap.get(HEADER_CONTENT_HTML);
        headerContent = comments == null? headerContent : comments + "\n" + headerContent;
        headerContentHtml = comments == null? headerContentHtml : "<p>" + comments + "</p>" + headerContentHtml;
        sectionContentMap.put(HEADER_CONTENT, headerContent);
        sectionContentMap.put(HEADER_CONTENT_HTML, headerContentHtml);

        //build email object
        EmailVO email = new EmailVO();
        email.setRecipient(toAddress);
        email.setSender((String)sectionContentMap.get(BuildEmailConstants.FROM_ADDRESS));
        email.setSubject((String)sectionContentMap.get(BuildEmailConstants.SUBJECT_CONTEXT));

       //section info
       Map infoSectionMap = helper.getOrderInfoSection(order);

        //text content
        email.appendContent((String)sectionContentMap.get(BuildEmailConstants.HEADER_CONTENT));
        //email.appendContent("\n");
        if(connection != null && new MercentOrderPrefixes(connection).isMercentOrder(order.getOrderOrigin())){
        	logger.debug("Is mercent order skip body message!!");
        } else {
        	email.appendContent((String)infoSectionMap.get(BuildEmailConstants.PLAIN_TEXT));
        }
        //email.appendContent("\n");
        email.appendContent((String)sectionContentMap.get(BuildEmailConstants.GUARANTEE_CONTENT));
        email.appendContent("\n");
        email.appendContent("\n");
        email.appendContent((String)sectionContentMap.get(BuildEmailConstants.MARKETING_CONTENT));
        email.appendContent("\n");
        email.appendContent("\n");
        email.appendContent((String)sectionContentMap.get(BuildEmailConstants.CONTACT_CONTENT));
        //html content
        email.appendHTMLContent(helper.startGlobalHTML());
        email.appendHTMLContent((String)sectionContentMap.get(BuildEmailConstants.HEADER_CONTENT_HTML));
        email.appendHTMLContent("<BR>");
        email.appendHTMLContent((String)infoSectionMap.get(BuildEmailConstants.HTML));

        email.appendHTMLContent((String)sectionContentMap.get(GUARANTEE_CONTENT_HTML));
        email.appendHTMLContent("<BR>");
        email.appendHTMLContent("<BR>");
        email.appendHTMLContent((String)sectionContentMap.get(MARKETING_CONTENT_HTML));
        email.appendHTMLContent("<BR>");
        email.appendHTMLContent("<BR>");
        email.appendHTMLContent((String)sectionContentMap.get(CONTACT_CONTENT_HTML));
        email.appendHTMLContent(helper.closeGlobalHTML());
        
        //get email guid
        String emailGuid = GUIDGenerator.getInstance().getGUID();
        email.setGuid(emailGuid);
        
        helper.updateEmailOrderTokens(email, order);
        
        //save email object to database
        EmailDAO dao = new EmailDAO(connection);
        dao.saveEmail(email);

        //send message to email service
        String message = BuildEmailConstants.PREVIEW_ACTION + "." + emailGuid;
        MessageToken token = new MessageToken();
        token.setMessage(message);
        token.setStatus(BuildEmailConstants.JMS_STATUS);
        if(BuildEmailConstants.SEND_JMS_MSG)
        {
          logger.debug("Dispatching:"+message);
          Dispatcher dispatcher = Dispatcher.getInstance();
          InitialContext initContext = InitialContextUtil.getInstance().getInitialContext("LOCALHOST");
          dispatcher.dispatch(initContext, token);
        }
        else
        {
          logger.debug(message);
          logger.info("Email messages are not being sent to JMS due to value in constants file.");
        }
      
         
    }

    public void generateConfirmationEmail(OrderVO order) throws Exception
    {
      String message = "CONFIRMATION." + order.getGUID();
      MessageToken token = new MessageToken();
      token.setMessage(message);
      token.setStatus(BuildEmailConstants.JMS_STATUS);

      if(BuildEmailConstants.SEND_JMS_MSG)
      {
        Dispatcher dispatcher = Dispatcher.getInstance();
        InitialContext initContext = InitialContextUtil.getInstance().getInitialContext("LOCALHOST");
        dispatcher.dispatch(initContext, token);
      }
    }

    public void generateReinstateConfirmationEmail(OrderVO order, String lineNumber) throws Exception
    {
        // Send JMS message to email service
        String message = BuildEmailConstants.REINSTATE_CONFIRMATION_ACTION + "." + order.getGUID() + "." + lineNumber;
        MessageToken token = new MessageToken();
        token.setMessage(message);
        token.setStatus(BuildEmailConstants.JMS_STATUS);

        if(BuildEmailConstants.SEND_JMS_MSG)
        {
            Dispatcher dispatcher = Dispatcher.getInstance();
            InitialContext initContext = InitialContextUtil.getInstance().getInitialContext("LOCALHOST");
            dispatcher.dispatch(initContext, token);
        }
    }

    public void generateScrubEmail(EmailVO email, OrderVO order) throws Exception
    {
        BuildOrderEmailHelper helper = new BuildOrderEmailHelper(connection);
        helper.updateEmailOrderTokens(email, order);
        
        // Save email to database
        EmailDAO emailDAO = new EmailDAO(connection);
        emailDAO.saveEmail(email);

        // Send JMS message to email service
        String message = BuildEmailConstants.PEND_REMOVE_ACTION + "." + email.getGuid();
        MessageToken token = new MessageToken();
        token.setMessage(message);
        token.setStatus(BuildEmailConstants.JMS_STATUS);

        if(BuildEmailConstants.SEND_JMS_MSG)
        {
            Dispatcher dispatcher = Dispatcher.getInstance();
            InitialContext initContext = InitialContextUtil.getInstance().getInitialContext("LOCALHOST");
            dispatcher.dispatch(initContext, token);
        }
    }
    
    /**
     * Set the section contents, text and html.
     * @param CachedResultSet program section content map, in a result set.
     * @return Map map of section name and content string
     * @throw Exception
     */
    private Map getEmailSectionContent(CachedResultSet rs) throws Exception {
        String seperator = "~";
        //String fromAddress = "";

        String sectionType = "";
        String contentType = "";
        int sectionId = 0;
        int holdsectionId = 0;
        boolean firstTime = true;
        StringBuffer content = new StringBuffer();
        int rowCount = rs.getRowCount();
        int count = 0;

        String subjectContent = "";
        String headerContent = "";
        String headerContentHtml = "";
        String guaranteeContent = "";
        String guaranteeContentHtml = "";
        String marketingContent = "";
        String marketingContentHtml = "";
        String contactContent = "";
        String contactContentHtml = "";
        String fromAddress = "";
        Map sectionContentMap = new HashMap();

        while(rs != null && rs.next())
        {
            count++;
            sectionId = new Integer(rs.getObject(4).toString()).intValue();

            if(firstTime)
            {
                sectionType = (String) rs.getObject(1);
                contentType = (String) rs.getObject(2);
                content.append((String)rs.getObject(3));
                holdsectionId = sectionId;
                firstTime = false;
            }
            else if (holdsectionId == sectionId)
            {
                //multiple records for a section
                content.append((String)rs.getObject(3));
            }
            else
            {
                //sectionId change figure out content type
                //process each content by section type/content type
                if(sectionType.equals(SUBJECT))
                {
                    //pull from address from subject content
                    int seperatorPosition = content.toString().indexOf(seperator);
                    fromAddress = content.toString().substring(0,seperatorPosition);
                    subjectContent = content.toString().substring(seperatorPosition + 1,content.toString().length());
                }
                else if(sectionType.equals(HEADER))
                {
                    if(contentType.equals(TEXT))
                    {
                        headerContent = content.toString();
                    }
                    else
                    {
                        headerContentHtml = content.toString();
                    }
                }
                else if(sectionType.equals(GUARANTEE))
                {
                    if(contentType.equals(TEXT))
                    {
                        guaranteeContent = content.toString();
                    }
                    else
                    {
                        guaranteeContentHtml = content.toString();
                    }
                }
                else if(sectionType.equals(MARKETING))
                {
                    if(contentType.equals(TEXT))
                    {
                        marketingContent = content.toString();
                    }
                    else
                    {
                        marketingContentHtml = content.toString();
                    }
                }
                else if(sectionType.equals(CONTACT))
                {
                    if(contentType.equals(TEXT))
                    {
                        contactContent = content.toString();
                    }
                    else
                    {
                        contactContentHtml = content.toString();
                    }
                }

                //deal with new sectionId
                content = new StringBuffer();
                sectionType = (String) rs.getObject(1);
                contentType = (String) rs.getObject(2);
                content.append((String)rs.getObject(3));
                holdsectionId = sectionId;

                if(count == rowCount)
                {
                    //last record
                    if(sectionType.equals(SUBJECT))
                    {
                        //pull from address from subject content
                        int seperatorPosition = content.toString().indexOf(seperator);
                        fromAddress = content.toString().substring(0,seperatorPosition);
                        subjectContent = content.toString().substring(seperatorPosition + 1,content.toString().length());
                    }
                    else if(sectionType.equals(HEADER))
                    {
                        if(contentType.equals(TEXT))
                        {
                            headerContent = content.toString();
                        }
                        else
                        {
                            headerContentHtml = content.toString();
                        }
                    }
                    else if(sectionType.equals(GUARANTEE))
                    {
                        if(contentType.equals(TEXT))
                        {
                            guaranteeContent = content.toString();
                        }
                        else
                        {
                            guaranteeContentHtml = content.toString();
                        }
                    }
                    else if(sectionType.equals(MARKETING))
                    {
                        if(contentType.equals(TEXT))
                        {
                            marketingContent = content.toString();
                        }
                        else
                        {
                            marketingContentHtml = content.toString();
                        }
                    }
                    else if(sectionType.equals(CONTACT))
                    {
                        if(contentType.equals(TEXT))
                        {
                            contactContent = content.toString();
                        }
                        else
                        {
                            contactContentHtml = content.toString();
                        }
                    }
                }

            }
        }

        sectionContentMap.put(SUBJECT_CONTEXT, subjectContent);
        sectionContentMap.put(HEADER_CONTENT, headerContent);
        sectionContentMap.put(HEADER_CONTENT_HTML, headerContentHtml);
        sectionContentMap.put(GUARANTEE_CONTENT, guaranteeContent);
        sectionContentMap.put(GUARANTEE_CONTENT_HTML, guaranteeContentHtml);
        sectionContentMap.put(MARKETING_CONTENT, marketingContent);
        sectionContentMap.put(MARKETING_CONTENT_HTML, marketingContentHtml);
        sectionContentMap.put(CONTACT_CONTENT, contactContent);
        sectionContentMap.put(CONTACT_CONTENT_HTML, contactContentHtml);
        sectionContentMap.put(FROM_ADDRESS, fromAddress);
        return sectionContentMap;
    }


    
}