package com.ftd.osp.utilities.email;

public class EmailVO 
{
  public static final int PLAIN_TEXT = 0;
  public static final int MULTIPART = 1;
  public static final int AOL = 2;
  
  private int contentType = 0;
  private String guid = new String();
  private String subject = new String();
  private String sender = new String();
  private String recipient = new String();
  private String content = new String();
  private String HTMLContent = new String();
  private String orderNumber =new String();
  private String recordAttributesXML = new String();

  public EmailVO()
  {}

  public void setContentType(int contentType)
  {
    this.contentType = contentType;
  }

  public int getContentType()
  {
    return this.contentType;
  }

  public void setGuid(String guid)
  {
    this.guid = guid;
  }

  public String getGuid()
  {
    return this.guid;
  }

  public void setSubject(String subject)
  {
    this.subject = subject;
  }

  public String getSubject()
  {
    return this.subject;
  }

  public void setRecipient(String recipient)
  {
    this.recipient = recipient;
  }

  public String getRecipient()
  {
    return this.recipient;
  }

  public void setSender(String sender)
  {
    this.sender = sender;
  }

  public String getSender()
  {
    return this.sender;
  }

  public String getContent()
  {
    return this.content;
  }

  public void setContent(String content)
  {
    this.content = content;
  }

  public void prependContent(String section)
  {
    this.content = (section + content);
  }

  public void appendContent(String section)
  {
    this.content = (this.content + section);
  }

  public String getHTMLContent()
  {
    return this.HTMLContent;
  }
  
  public void setHTMLContent(String HTMLContent) 
  {
    this.HTMLContent = HTMLContent;
  }

  public void prependHTMLContent(String section)
  {
    this.HTMLContent = (section + HTMLContent);
  }

  public void appendHTMLContent(String section)
  {
    this.HTMLContent = (this.HTMLContent + section);
  }

  public void setRecordAttributesXML(String recordAttributesXML) {
	this.recordAttributesXML = recordAttributesXML;
  }

  public String getRecordAttributesXML() {
	return recordAttributesXML;
  }
  
  public String getOrderNumber() {
	return orderNumber;
  }

  public void setOrderNumber(String orderNumber) {
	this.orderNumber = orderNumber;
  }
  
}