package com.ftd.osp.utilities.email;

import java.math.BigDecimal;
import java.sql.Connection;
import java.text.CharacterIterator;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.text.StringCharacterIterator;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.ftd.osp.utilities.AddOnUtility;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.PartnerUtility;
import com.ftd.osp.utilities.PreferredPartnerUtility;
import com.ftd.osp.utilities.SegmentationUtil;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.CacheUtil;
import com.ftd.osp.utilities.cacheMgr.constant.CacheMgrConstants;
import com.ftd.osp.utilities.cacheMgr.handlers.GlobalParmHandler;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.order.vo.AddOnsVO;
import com.ftd.osp.utilities.order.vo.BuyerEmailsVO;
import com.ftd.osp.utilities.order.vo.BuyerVO;
import com.ftd.osp.utilities.order.vo.CreditCardsVO;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.order.vo.PartnerVO;
import com.ftd.osp.utilities.order.vo.PaymentsVO;
import com.ftd.osp.utilities.order.vo.RecalculateOrderSourceCodeVO;
import com.ftd.osp.utilities.order.vo.RecipientAddressesVO;
import com.ftd.osp.utilities.order.vo.RecipientPhonesVO;
import com.ftd.osp.utilities.order.vo.RecipientsVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.AddOnVO;
import com.ftd.osp.utilities.xml.DOMUtil;


public class BuildOrderEmailHelper
{
  static final Logger logger = new Logger(BuildOrderEmailHelper.class.getName());
  
  private static String GLOBAL_PARAM_EMAIL_CONTEXT = "EMAIL_SERVICE";
  private static String GLOBAL_PARAM_EMAIL_TYPE_KEY = "SEND_CONTENT_TYPE";
  private static String EMAIL_SERVICE_CONFIG_FILE = "email-action-config.xml";
  
  private Connection connection = null;
  private static final int SPLIT_SIZE = 52;
  
  private static String ENGLISH = "ENUS";
  private static String SYMPATHY = "SYMPATHY";
  private static String SCI = "SCI";
  private static final String PRODUCT_TYPE_SERVICES = "SERVICES";
  
  ConfigurationUtil cu = null;

  public BuildOrderEmailHelper(Connection connection)
  {
      this.connection = connection;
		// Instantiating ConfigurationUtil as well. Note that there is no static method in this class.
		try {
			cu = ConfigurationUtil.getInstance();
		} catch (Exception e) {
			logger.error("Error caught instantiating Configuration util " + e.getMessage());
			throw new RuntimeException(e);
		}
  }

  private Map getProgramBySource(String sourceCode) throws Exception
  {
      Map emailMap = null;

      try
      {

        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID(BuildEmailConstants.VIEW_CURRENT_EMAIL_SECTIONS);
        dataRequest.addInputParam("IN_SOURCE_CODE", sourceCode);
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        emailMap = (Map)dataAccessUtil.execute(dataRequest);
        String status = (String) emailMap.get(BuildEmailConstants.STATUS_PARAM);

        if(status.equals("N"))
        {
            String message = (String) emailMap.get(BuildEmailConstants.MESSAGE_PARAM);
            throw new Exception("Unable to load program by source code. Specific: " + message);
        }
      }
      catch(Exception e)
      {
        throw new Exception("Unable to load program by source code. Specific: " + e.toString());
      }

      return emailMap;
   }

  public Map getEmailSectionContent(OrderVO order) throws Exception
  {
      Map emailMap = this.getProgramBySource(order.getSourceCode());

      String seperator = "~";
      CachedResultSet rs = new CachedResultSet();
      rs = (CachedResultSet) emailMap.get(BuildEmailConstants.EMAIL_CONTENT_CURSOR);

      String sectionType = "";
      String contentType = "";
      int sectionId = 0;
      int holdsectionId = 0;
      boolean firstTime = true;
      StringBuffer content = new StringBuffer();
      int rowCount = rs.getRowCount();
      int count = 0;

      String subjectContent = "";
      String headerContent = "";
      String headerContentHtml = "";
      String guaranteeContent = "";
      String guaranteeContentHtml = "";
      String marketingContent = "";
      String marketingContentHtml = "";
      String contactContent = "";
      String contactContentHtml = "";
      String fromAddress = "";
      Map sectionContentMap = new HashMap();

      while(rs != null && rs.next())
      {
          count++;
          sectionId = new Integer(rs.getObject(4).toString()).intValue();

          if(firstTime)
          {
              sectionType = (String) rs.getObject(1);
              contentType = (String) rs.getObject(2);
              content.append((String)rs.getObject(3));
              holdsectionId = sectionId;
              firstTime = false;
          }
          else if (holdsectionId == sectionId)
          {
              //multiple records for a section
              content.append((String)rs.getObject(3));
          }
          else
          {
              //sectionId change figure out content type
              //process each content by section type/content type
              if(sectionType.equals(BuildEmailConstants.SUBJECT))
              {
                  //pull from address from subject content
                  int seperatorPosition = content.toString().indexOf(seperator);
                  fromAddress = content.toString().substring(0,seperatorPosition);
                  subjectContent = content.toString().substring(seperatorPosition + 1,content.toString().length());
              }
              else if(sectionType.equals(BuildEmailConstants.HEADER))
              {
                  if(contentType.equals(BuildEmailConstants.TEXT))
                  {
                      headerContent = content.toString();
                  }
                  else
                  {
                      headerContentHtml = content.toString();
                  }
              }
              else if(sectionType.equals(BuildEmailConstants.GUARANTEE))
              {
                  if(contentType.equals(BuildEmailConstants.TEXT))
                  {
                      guaranteeContent = content.toString();
                  }
                  else
                  {
                      guaranteeContentHtml = content.toString();
                  }
              }
              else if(sectionType.equals(BuildEmailConstants.MARKETING))
              {
                  if(contentType.equals(BuildEmailConstants.TEXT))
                  {
                      marketingContent = content.toString();
                  }
                  else
                  {
                      marketingContentHtml = content.toString();
                  }
              }
              else if(sectionType.equals(BuildEmailConstants.CONTACT))
              {
                  if(contentType.equals(BuildEmailConstants.TEXT))
                  {
                      contactContent = content.toString();
                  }
                  else
                  {
                      contactContentHtml = content.toString();
                  }
              }

              //deal with new sectionId
              content = new StringBuffer();
              sectionType = (String) rs.getObject(1);
              contentType = (String) rs.getObject(2);
              content.append((String)rs.getObject(3));
              holdsectionId = sectionId;

              if(count == rowCount)
              {
                  //last record
                  if(sectionType.equals(BuildEmailConstants.SUBJECT))
                  {
                      //pull from address from subject content
                      int seperatorPosition = content.toString().indexOf(seperator);
                      fromAddress = content.toString().substring(0,seperatorPosition);
                      subjectContent = content.toString().substring(seperatorPosition + 1,content.toString().length());
                  }
                  else if(sectionType.equals(BuildEmailConstants.HEADER))
                  {
                      if(contentType.equals(BuildEmailConstants.TEXT))
                      {
                          headerContent = content.toString();
                      }
                      else
                      {
                          headerContentHtml = content.toString();
                      }
                  }
                  else if(sectionType.equals(BuildEmailConstants.GUARANTEE))
                  {
                      if(contentType.equals(BuildEmailConstants.TEXT))
                      {
                          guaranteeContent = content.toString();
                      }
                      else
                      {
                          guaranteeContentHtml = content.toString();
                      }
                  }
                  else if(sectionType.equals(BuildEmailConstants.MARKETING))
                  {
                      if(contentType.equals(BuildEmailConstants.TEXT))
                      {
                          marketingContent = content.toString();
                      }
                      else
                      {
                          marketingContentHtml = content.toString();
                      }
                  }
                  else if(sectionType.equals(BuildEmailConstants.CONTACT))
                  {
                      if(contentType.equals(BuildEmailConstants.TEXT))
                      {
                          contactContent = content.toString();
                      }
                      else
                      {
                          contactContentHtml = content.toString();
                      }
                  }
              }

          }
      }

      sectionContentMap.put(BuildEmailConstants.SUBJECT_CONTEXT, subjectContent);
      sectionContentMap.put(BuildEmailConstants.HEADER_CONTENT, headerContent);
      sectionContentMap.put(BuildEmailConstants.HEADER_CONTENT_HTML, headerContentHtml);
      sectionContentMap.put(BuildEmailConstants.GUARANTEE_CONTENT, guaranteeContent);
      sectionContentMap.put(BuildEmailConstants.GUARANTEE_CONTENT_HTML, guaranteeContentHtml);
      sectionContentMap.put(BuildEmailConstants.MARKETING_CONTENT, marketingContent);
      sectionContentMap.put(BuildEmailConstants.MARKETING_CONTENT_HTML, marketingContentHtml);
      sectionContentMap.put(BuildEmailConstants.CONTACT_CONTENT, contactContent);
      sectionContentMap.put(BuildEmailConstants.CONTACT_CONTENT_HTML, contactContentHtml);
      sectionContentMap.put(BuildEmailConstants.FROM_ADDRESS, fromAddress);
      return sectionContentMap;
  }

  public String extractBuyerEmail(OrderVO order) throws Exception
  {
     String toAddress =  ((BuyerEmailsVO)((BuyerVO)(order.getBuyer().get(0))).getBuyerEmails().get(0)).getEmail().toLowerCase();
     if(toAddress == null || toAddress.equals(""))
      throw new Exception("Unable to extract buyer email for order_guid: " + order.getGUID());
     return toAddress;
  }

  public HashMap getBulkOrderInfoSection(OrderVO order) throws Exception
  {
      HashMap sections = new HashMap();
      OrderDetailsVO tmpDetailVO = null;
      BuyerVO tmpBuyerVO = (BuyerVO)order.getBuyer().get(0);
      StringBuffer orderInfoSection = new StringBuffer();
      StringBuffer HTMLorderInfoSection = new StringBuffer();
      ArrayList itemGroups = new ArrayList();
      ArrayList itemGroup = null;
      boolean inGroup = false;
      double subtotal = 0;      
      //Start---Same day upcharge fields display
      boolean separateLineSameDayFee = false;
      String displaySameDayUpcharge;
      String gDisplaySameDayUpcharge;
      RecalculateOrderSourceCodeVO recalculateOrderSourceCodeVO = new RecalculateOrderSourceCodeVO();
      recalculateOrderSourceCodeVO = getSourceCodeDetails(order.getSourceCode());
      displaySameDayUpcharge = recalculateOrderSourceCodeVO.getDisplaySameDayUpcharge();
      gDisplaySameDayUpcharge = CacheUtil.getInstance().getGlobalParm("FTDAPPS_PARMS", "SAME_DAY_UPCHARGE_DISPLAY");
      if(displaySameDayUpcharge.equalsIgnoreCase("Y") || 
    		  (displaySameDayUpcharge.equalsIgnoreCase("D") && gDisplaySameDayUpcharge.equalsIgnoreCase("Y"))) {       
		separateLineSameDayFee = true;      
	  }
      //End-----Same day upcharge fields display            
      
      orderInfoSection.append(end()+ end() + end());
      HTMLorderInfoSection.append(this.startHTML());
      HTMLorderInfoSection.append(this.startHTMLTable());

      orderInfoSection.append("Customer:" + this.createSpaces(17) + tmpBuyerVO.getFirstName() + " " + tmpBuyerVO.getLastName() + end());
      HTMLorderInfoSection.append(this.addBulkStandardUpperRow("Customer:", tmpBuyerVO.getFirstName() + "&nbsp;" + tmpBuyerVO.getLastName()));
      orderInfoSection.append("Order Date:" + this.createSpaces(15) + convertDate(order.getOrderDate(),1) + end());
      HTMLorderInfoSection.append(this.addBulkStandardUpperRow("Order Date:", convertDate(order.getOrderDate(),1)));
      orderInfoSection.append("Master Order Number:" + this.createSpaces(6) + order.getMasterOrderNumber() + end());
      HTMLorderInfoSection.append(this.addBulkStandardUpperRow("Master Order Number:", order.getMasterOrderNumber()));
      HTMLorderInfoSection.append(this.endHTMLTable());
      HTMLorderInfoSection.append("<br>");
      orderInfoSection.append(end() + end());

      orderInfoSection.append("QTY\t" + this.createSpaces(28) + "\t\tDelivery\tShip" + end());
      orderInfoSection.append("   \t" + this.createSpaces(28) + "\t\tDate\t\tMethod" + end() + end());
      HTMLorderInfoSection.append(this.startBulkInfoTable());
      HTMLorderInfoSection.append(this.addBulkInfoHeader());

      for(int i = 0; i < order.getOrderDetail().size(); i++)
      {
        tmpDetailVO = (OrderDetailsVO)(order.getOrderDetail().get(i));
        subtotal += Double.parseDouble(tmpDetailVO.getProductsAmount());
        inGroup = false;

        if(itemGroups.size() == 0)
        {
          itemGroup = new ArrayList();
          itemGroup.add(tmpDetailVO);
          itemGroups.add(itemGroup);
        }
        else
        {
          for(int j = 0; j < itemGroups.size();j++)
          {
              if(
                  tmpDetailVO.getProductId().equals(((OrderDetailsVO)((ArrayList)itemGroups.get(j)).get(0)).getProductId()) &&
                  tmpDetailVO.getShipMethod().equals(((OrderDetailsVO)((ArrayList)itemGroups.get(j)).get(0)).getShipMethod()) &&
                  tmpDetailVO.getDeliveryDate().equals(((OrderDetailsVO)((ArrayList)itemGroups.get(j)).get(0)).getDeliveryDate()) &&
                  tmpDetailVO.getProductsAmount().equals(((OrderDetailsVO)((ArrayList)itemGroups.get(j)).get(0)).getProductsAmount())
                  )
              {
                  ((ArrayList)itemGroups.get(j)).add(tmpDetailVO);
                  inGroup = true;
                  break;
              }
          } //close for
          if(!inGroup)
          {
             itemGroup = new ArrayList();
             itemGroup.add(tmpDetailVO);
             itemGroups.add(itemGroup);
          }
        } //close else
      }

      ArrayList tmpArrayList = null;
      double totalLinePrice = 0;
      double itemPrice = 0;
      String shippingMethod = null;
      java.text.NumberFormat nf = java.text.NumberFormat.getCurrencyInstance(Locale.US);

      for(int i =0; i < itemGroups.size(); i++)
      {
        tmpArrayList = (ArrayList)(itemGroups.get(i));
        tmpDetailVO = (OrderDetailsVO)tmpArrayList.get(0);
        itemPrice = Double.parseDouble(tmpDetailVO.getProductsAmount());
        totalLinePrice = itemPrice * tmpArrayList.size();
        shippingMethod = this.translateShippingMethod(tmpDetailVO.getShipMethod());
        shippingMethod = this.replace(shippingMethod, " Delivery", "");
        orderInfoSection.append(tmpArrayList.size() + "\t" + truncDescription(tmpDetailVO.getItemDescription()) + " @" + formatDollars(tmpDetailVO.getProductsAmount()) + "\t" + tmpDetailVO.getDeliveryDate() + "\t" + shippingMethod + "\t" +  rightAlign(nf.format(totalLinePrice)) + end());
        orderInfoSection.append("\t" + tmpDetailVO.getProductId() + end());
        HTMLorderInfoSection.append(this.addBulkStandardInfoRow(String.valueOf(tmpArrayList.size()), truncDescription(tmpDetailVO.getItemDescription()),"&nbsp;@" + formatDollars(tmpDetailVO.getProductsAmount()), tmpDetailVO.getDeliveryDate(), shippingMethod, nf.format(totalLinePrice)));
        HTMLorderInfoSection.append(this.addBulkStandardInfoRow("&nbsp;", tmpDetailVO.getProductId(), "&nbsp;", "&nbsp;","&nbsp;", "&nbsp;"));
      }
      HTMLorderInfoSection.append(this.endHTMLTable());
      HTMLorderInfoSection.append(this.startHTMLTable()); /////////////////////////////////////////////here starts the summary table

      orderInfoSection.append(line(16) + "\t\t\t\t\t\t\t\t\t" + line(9) + end());
      HTMLorderInfoSection.append(this.addBulkStandardSummaryRow(line(16), line(9)));

      orderInfoSection.append("Subtotal:" + "\t\t\t\t\t\t\t\t\t\t" + rightAlign(nf.format(subtotal)) +  end() + end() + end());
      HTMLorderInfoSection.append(this.addBulkStandardSummaryRow("Subtotal:", nf.format(subtotal)));
//DISCOUNT
      if(order.getDiscountTotal() != null && !order.getDiscountTotal().startsWith("0"))
      {
         orderInfoSection.append("Discount:\t\t\t\t\t\t\t" + rightAlign("  (" + formatDollars(order.getDiscountTotal()) + ")") + end());
         HTMLorderInfoSection.append(this.addBulkStandardSummaryRow("Discount:", "(" + formatDollars(order.getDiscountTotal()) + ")"));
      }
      
//SHIPPING + SERVICE CHARGES
      BigDecimal totalFees = new BigDecimal(0);
            
      if(order.getShippingFeeTotal() != null && !order.getShippingFeeTotal().startsWith("0"))
      {
        BigDecimal shipFee = new BigDecimal( order.getShippingFeeTotal());
        totalFees = totalFees.add(shipFee);
      }

      if(order.getServiceFeeTotal() != null && !order.getServiceFeeTotal().startsWith("0"))
      {
        BigDecimal serviceFee = new BigDecimal(order.getServiceFeeTotal());
        //Same day upcharge code. If separateLineSameDayFee is true and has same day upcharge then subtract from service fee and then add
        //service fee to total fees. Or else service fee contains same day upcharge fees included.            
        if(separateLineSameDayFee && tmpDetailVO.getSameDayUpcharge() != null && (Float.parseFloat(tmpDetailVO.getSameDayUpcharge()) > 0)){
        	serviceFee = serviceFee.subtract(new BigDecimal(tmpDetailVO.getSameDayUpcharge()));            	
        }
        totalFees = totalFees.add(serviceFee);
      }
      
        // subtract Morning Delivery Fees from shipping/service fees if separateLineMorningDeliveryFee is Y and if morningDeliveryFee is greater than or equal to 0
      if(tmpDetailVO.getMorningDeliveryFee() != null && (Float.parseFloat(tmpDetailVO.getMorningDeliveryFee()) > 0)){
    	  if(totalFees.doubleValue() > 0)
    	  {
    		  totalFees = totalFees.subtract(new BigDecimal(tmpDetailVO.getMorningDeliveryFee()));
    	  }
      }
      
       //subtract Surcharge Fees  from shipping/service fee if displaySurcharge flag is Y and we will display separately from shipping/service fee       
      if ( StringUtils.equalsIgnoreCase(tmpDetailVO.getDisplaySurcharge(), "Y") &&  StringUtils.equalsIgnoreCase(tmpDetailVO.getApplySurchargeCode(), "ON"))
      {
        BigDecimal fuelSurchargeFee = new BigDecimal( tmpDetailVO.getFuelSurcharge());// we will show surcharge fee even if its zero 
           if(totalFees.doubleValue() > 0)
           {
              totalFees = totalFees.subtract(fuelSurchargeFee);
           }             
      }

      if(totalFees.doubleValue() > 0)
      {
        orderInfoSection.append("Shipping/Service Fees:" + "\t\t\t\t\t\t" + rightAlign("" + formatDollars(totalFees.doubleValue())) + end());
        HTMLorderInfoSection.append(this.addBulkStandardSummaryRow("Shipping/Service Fees:", "" + formatDollars(totalFees.doubleValue())));
      }
      
      //Start---Same day upcharge fields display
      logger.debug("In BuildOrderEmailHelper getOrderInfoSection displaySameDayUpcharge value is "+ displaySameDayUpcharge);
      logger.debug("In BuildOrderEmailHelper getOrderInfoSection global displaySameDayUpcharge value is "+ gDisplaySameDayUpcharge);
      logger.debug("In BuildOrderEmailHelper getOrderInfoSection sameDayUpcharge fee value is "+ tmpDetailVO.getSameDayUpcharge());
      if(separateLineSameDayFee && tmpDetailVO.getSameDayUpcharge() != null && (Float.parseFloat(tmpDetailVO.getSameDayUpcharge()) > 0)){
    	  String sameDayUpchargeLabel = CacheUtil.getInstance().getContentWithFilter("SAME_DAY_UPCHARGE", "CONFIRMATION_EMAIL_LABEL", null, null, false);
    	  logger.debug("In BuildOrderEmailHelper getOrderInfoSection sameDayUpcharge label is "+ sameDayUpchargeLabel);
    	  orderInfoSection.append(justifyToLine(sameDayUpchargeLabel+ ":", "" + formatDollars(tmpDetailVO.getSameDayUpcharge()))+ end());
    	  HTMLorderInfoSection.append(addStandardLowerRow(sameDayUpchargeLabel+ ":", "" + formatDollars(tmpDetailVO.getSameDayUpcharge())) + end());
      }
      //End-----Same day upcharge fields display
      
      //Start---Morning delivery fields display      
      logger.debug("In BuildOrderEmailHelper getBulkOrderInfoSection morningDeliveryFee value is "+ tmpDetailVO.getMorningDeliveryFee());
      
      if(tmpDetailVO.getMorningDeliveryFee() !=null && (Float.parseFloat(tmpDetailVO.getMorningDeliveryFee()) > 0)){
    	  String morningDeliveryFeeLabel = CacheUtil.getInstance().getContentWithFilter("MORNING_DELIVERY", "MORNING_DELIVERY_LABEL", null, null, false);
    	  orderInfoSection.append(justifyToLine(morningDeliveryFeeLabel + ":","" + formatDollars(tmpDetailVO.getMorningDeliveryFee())) + end());
    	  HTMLorderInfoSection.append(addStandardLowerRow(morningDeliveryFeeLabel + ":","" + formatDollars(tmpDetailVO.getMorningDeliveryFee())) + end());
    		  }
      //End-----Morning delivery fields display
      
// add the fuel surcharge fee line after shipping/service fee line          
          logger.debug("In Bulk BuildOrderEmailHelper getDisplaySurcharge value is "+ tmpDetailVO.getDisplaySurcharge());
          logger.debug("In Bulk BuildOrderEmailHelper getApplySurchargeCode value is "+ tmpDetailVO.getApplySurchargeCode());
          if ( StringUtils.equalsIgnoreCase(tmpDetailVO.getDisplaySurcharge(), "Y") &&  StringUtils.equalsIgnoreCase(tmpDetailVO.getApplySurchargeCode(), "ON"))
          {
            BigDecimal fuelSurchargeFee = new BigDecimal( tmpDetailVO.getFuelSurcharge());// we will show surcharge fee even if its zero 
            String fuelSurchargeDescription = tmpDetailVO.getFuelSurchargeDescription();
            logger.debug("fuelSurchargeDescription to display in email is"+ fuelSurchargeDescription);
            orderInfoSection.append(justifyToLine(fuelSurchargeDescription+ ":", "" + formatDollars(fuelSurchargeFee.doubleValue())) + end());
            HTMLorderInfoSection.append(addStandardLowerRow(fuelSurchargeDescription+ ":", "" + formatDollars(fuelSurchargeFee.doubleValue())) + end());
          }

//TAX
      if(order.getTaxTotal() != null && !order.getTaxTotal().startsWith("0"))
      {
        orderInfoSection.append("Tax:" + "\t\t\t\t\t\t\t\t" + rightAlign(""+formatDollars(order.getTaxTotal())) + end());
        HTMLorderInfoSection.append(this.addBulkStandardSummaryRow("Tax:", ""+formatDollars(order.getTaxTotal())));
      }
      orderInfoSection.append(end());
      orderInfoSection.append(line(16) + "\t\t\t\t\t\t" + line(9) + end());
      HTMLorderInfoSection.append(this.addBulkStandardSummaryRow(line(16), line(9)));

//TOTAL CHARGE
      orderInfoSection.append("Total Charge:" + "\t\t\t\t\t\t" + rightAlign(""+formatDollars(order.getOrderTotal())) + end());
      HTMLorderInfoSection.append(this.addBulkStandardSummaryRow("Total Charge:", "" + formatDollars(order.getOrderTotal())));
      orderInfoSection.append(end());

//CREDIT CARD TYPE
      PaymentsVO tmpPaymentsVO = (PaymentsVO)order.getPayments().get(0);
      CreditCardsVO tmpCreditCardsVO = null;
      if(!tmpPaymentsVO.getCreditCards().isEmpty())
      tmpCreditCardsVO = (CreditCardsVO)tmpPaymentsVO.getCreditCards().get(0);
      else
      tmpCreditCardsVO = new CreditCardsVO(); //just blank

      if(tmpPaymentsVO.getPaymentMethodType() != null && tmpPaymentsVO.getPaymentMethodType().equalsIgnoreCase("C"))
      {
          orderInfoSection.append("Credit Card Type:\t" + translateCCType(tmpCreditCardsVO.getCCType()));
          HTMLorderInfoSection.append(this.addBulkStandardSummaryRow("Credit Card Type:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + translateCCType(tmpCreditCardsVO.getCCType()), "&nbsp;"));
      }
      else
      if(tmpPaymentsVO.getPaymentMethodType() != null && tmpPaymentsVO.getPaymentMethodType().equalsIgnoreCase("I"))
      {
        orderInfoSection.append("Payment Type:\tInvoice");
        HTMLorderInfoSection.append(this.addBulkStandardSummaryRow("Payment Type:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Invoice", "&nbsp;"));
      }

      orderInfoSection.append(line() + end());

      HTMLorderInfoSection.append(this.endHTMLTable());
      HTMLorderInfoSection.append("<hr align=\"left\" width=\"610\" noshade size=\"1\">");

      HTMLorderInfoSection.append(this.startHTMLTable());
      orderInfoSection.append("All prices are in US dollars");
      HTMLorderInfoSection.append(addStandardLowerRow("All prices are in US dollars", "&nbsp;"));
      HTMLorderInfoSection.append(HTMLend());

      //REWARD INFORMATION
      String partnerId = recalculateOrderSourceCodeVO.getPartnerId();
      PartnerVO partner = this.getPartner(partnerId);
      if(partnerId != null && !partnerId.equals(""))
      {
        if(partner.getEmailExludeFlag() != null && !partner.getEmailExludeFlag().equalsIgnoreCase("Y"))
        {
          if(partner.getRewardName() != null && partner.getRewardName().equalsIgnoreCase("CHARITY"))
          {
            orderInfoSection.append("FTD.COM will donate 5% of the product price to " + partner.getPartnerName() + end());
            HTMLorderInfoSection.append(addStandardLowerRow("FTD.COM will donate 5% of the product price to " + partner.getPartnerName(), "&nbsp;"));
          }
          else
          if(partner.getPartnerId().equalsIgnoreCase("DISC1"))
          {
            orderInfoSection.append("Your purchase from FTD.COM has earned you " + formatDollars(order.getPartnershipBonusPoints()) + " towards your Cash Back Bonus with Discover." + end());
            HTMLorderInfoSection.append(addStandardLowerRow("Your purchase from FTD.COM has earned you " + formatDollars(order.getPartnershipBonusPoints()) + " towards your Cash Back Bonus with Discover.", "&nbsp;"));
          }
          else
          {
            long points = 0;
            if(order.getPartnershipBonusPoints() != null)
            {
              points = Math.round(Double.parseDouble(order.getPartnershipBonusPoints()));
            }
            orderInfoSection.append("You will earn " + points + " " + partner.getPartnerName() + " " + partner.getRewardName() + end());
            orderInfoSection.append("Please allow 6-8 weeks for miles/points to post to your account." + end());
            HTMLorderInfoSection.append(addStandardLowerRow("You will earn " + order.getPartnershipBonusPoints() + " " + partner.getPartnerName() + " " + partner.getRewardName(), "&nbsp;"));
            HTMLorderInfoSection.append(addStandardLowerRow("Please allow 6-8 weeks for miles/points to post to your account.", "&nbsp;"));
          }
        }
      }
      HTMLorderInfoSection.append(this.endHTMLTable());
      HTMLorderInfoSection.append(this.closeHTML());

      sections.put(BuildEmailConstants.HTML, HTMLorderInfoSection.toString());
      sections.put(BuildEmailConstants.PLAIN_TEXT, orderInfoSection.toString());

      return sections;
  }

  public HashMap getOrderInfoSection(OrderVO order) throws Exception
  {
      HashMap sections = new HashMap();
      float pa;
      float da;
      HashMap tmpHashMap = null;
      String tmpStr = "";
      StringBuffer orderInfoSection = new StringBuffer();
      StringBuffer HTMLorderInfoSection = new StringBuffer();
      OrderDetailsVO tmpDetailVO = null;
      RecipientsVO tmpRecipientsVO = null;
      RecipientAddressesVO tmpRecipientAddressesVO = null;
      RecipientPhonesVO tmpRecipientPhoneVO = null;
      PaymentsVO tmpPaymentsVO = null;
      CreditCardsVO tmpCreditCardsVO = null;
      AddOnsVO tmpAddOnsVO = null;
      boolean isMilesPointsOrder = false;
      String mpRedemptionRateAmt = order.getMpRedemptionRateAmt();
      AddOnUtility addOnUtility = new AddOnUtility();
      
      //cu = ConfigurationUtil.getInstance();

      //Start---Same day upcharge fields display
      boolean separateLineSameDayFee = false;
      String displaySameDayUpcharge;
      String gDisplaySameDayUpcharge;
      RecalculateOrderSourceCodeVO recalculateOrderSourceCodeVO = new RecalculateOrderSourceCodeVO();
      recalculateOrderSourceCodeVO = getSourceCodeDetails(order.getSourceCode());
      displaySameDayUpcharge = recalculateOrderSourceCodeVO.getDisplaySameDayUpcharge();
      gDisplaySameDayUpcharge = CacheUtil.getInstance().getGlobalParm("FTDAPPS_PARMS", "SAME_DAY_UPCHARGE_DISPLAY");
      if(displaySameDayUpcharge.equalsIgnoreCase("Y") || 
    		  (displaySameDayUpcharge.equalsIgnoreCase("D") && gDisplaySameDayUpcharge.equalsIgnoreCase("Y"))) {       
		separateLineSameDayFee = true;      
	  }
      //End-----Same day upcharge fields display
            
      
      try {
          if( mpRedemptionRateAmt!= null && !mpRedemptionRateAmt.equals("") && Double.valueOf(mpRedemptionRateAmt) > 0) {
              isMilesPointsOrder = true;
          }
      } catch (Exception e) {
          // Ignore the exception and treat the email as non-miles order.
      }

      orderInfoSection.append("\n");
      HTMLorderInfoSection.append(this.startHTML());



      orderInfoSection.append(line() + end());

      for(int i = 0; i < order.getOrderDetail().size(); i++)
      {
          tmpDetailVO = (OrderDetailsVO)(order.getOrderDetail().get(i));
          tmpRecipientsVO = (RecipientsVO)(tmpDetailVO.getRecipients().get(0));
          tmpRecipientAddressesVO = (RecipientAddressesVO)(tmpRecipientsVO.getRecipientAddresses().get(0));
          tmpRecipientPhoneVO = (RecipientPhonesVO)(tmpRecipientsVO.getRecipientPhones().get(0));


          HTMLorderInfoSection.append(startHTMLTable());

          orderInfoSection.append(addSpacesToLine("Order Number:") + getOrderNumber(tmpDetailVO, order.getOrderOrigin()) + end() + end());
          HTMLorderInfoSection.append(addStandardUpperRow("Order Number:", getOrderNumber(tmpDetailVO, order.getOrderOrigin())) + HTMLend());
          orderInfoSection.append(addSpacesToLine("Order Date:") + this.convertDate(order.getOrderDate(), 1) + end());
          HTMLorderInfoSection.append(addStandardUpperRow("Order Date:", this.convertDate(order.getOrderDate(), 1)));
          
          String productType = tmpDetailVO.getProductType();
          logger.debug("ProductType : "+tmpDetailVO.getProductType());
          
          if(!PRODUCT_TYPE_SERVICES.equals(productType))
	      {
	          if(tmpDetailVO.getShipMethod() != null && tmpDetailVO.getShipMethod().equals("GR"))
	          {
	                orderInfoSection.append(addSpacesToLine("Delivery On or Before:") + this.convertDate(tmpDetailVO.getDeliveryDate(),3) + end());
	                HTMLorderInfoSection.append(addStandardUpperRow("Delivery On or Before:", this.convertDate(tmpDetailVO.getDeliveryDate(),3)));
	          }
	          else
	          {
	            if(tmpDetailVO.getDeliveryDateRangeEnd() != null && !tmpDetailVO.getDeliveryDateRangeEnd().equals(""))
	            {
	                orderInfoSection.append(addSpacesToLine("Delivery Date:") +  this.convertDate(tmpDetailVO.getDeliveryDate(),3) + " - " + this.convertDate(tmpDetailVO.getDeliveryDateRangeEnd(), 2) + end());
	                HTMLorderInfoSection.append(addStandardUpperRow("Delivery Date:", this.convertDate(tmpDetailVO.getDeliveryDate(),3) + " - " + this.convertDate(tmpDetailVO.getDeliveryDateRangeEnd(), 2)));
	            }
	            else
	            {
	                 orderInfoSection.append(addSpacesToLine("Delivery On:") + this.convertDate(tmpDetailVO.getDeliveryDate(),3) + end());
	                 HTMLorderInfoSection.append(addStandardUpperRow("Delivery On:", this.convertDate(tmpDetailVO.getDeliveryDate(),3)));
	            }
	          }
	          orderInfoSection.append(addSpacesToLine("Delivery To:") + tmpRecipientsVO.getFirstName() + " " + tmpRecipientsVO.getLastName() + end());
	          HTMLorderInfoSection.append(addStandardUpperRow("Delivery To:", tmpRecipientsVO.getFirstName() + " " + tmpRecipientsVO.getLastName()));
	          orderInfoSection.append(addSpacesToLine("") + translateAddressType(tmpRecipientAddressesVO.getAddressType()) + end());
	          HTMLorderInfoSection.append(addStandardUpperRow("&nbsp;", translateAddressType(tmpRecipientAddressesVO.getAddressType())));
	          if(!translateAddressType(tmpRecipientAddressesVO.getAddressType()).equals("Residence"))
	          {
	
	              if(tmpRecipientAddressesVO.getName() != null && !tmpRecipientAddressesVO.getName().equals(""))
	              orderInfoSection.append(addSpacesToLine("") + tmpRecipientAddressesVO.getName() + end());
	              HTMLorderInfoSection.append(addStandardUpperRow("&nbsp;", tmpRecipientAddressesVO.getName()));
	          }
	          if(tmpRecipientAddressesVO.getAddressLine1().length() <= 30)
	          {
	             orderInfoSection.append(addSpacesToLine("") + tmpRecipientAddressesVO.getAddressLine1() + end());
	             HTMLorderInfoSection.append(addStandardUpperRow("&nbsp;", tmpRecipientAddressesVO.getAddressLine1()));
	          }
	          else
	          {
	             ArrayList addressLines = this.splitAddressLine(tmpRecipientAddressesVO.getAddressLine1());
	             for(int b = 0; b < addressLines.size(); b++)
	             {
	                orderInfoSection.append(addSpacesToLine("") + (String)(addressLines.get(b)) + end());
	                HTMLorderInfoSection.append(addStandardUpperRow("&nbsp;", (String)(addressLines.get(b))));
	             }
	          }
	
	          if(tmpRecipientAddressesVO.getAddressLine2() != null && !tmpRecipientAddressesVO.getAddressLine2().equals(""))
	          {
	              if(tmpRecipientAddressesVO.getAddressLine2().length() <=30)
	              {
	                orderInfoSection.append(addSpacesToLine("") + tmpRecipientAddressesVO.getAddressLine2() + end());
	                HTMLorderInfoSection.append(addStandardUpperRow("&nbsp;", tmpRecipientAddressesVO.getAddressLine2()));
	              }
	              else
	              {
	                  ArrayList addressLinesTwo = this.splitAddressLine(tmpRecipientAddressesVO.getAddressLine2());
	                  for(int q = 0;q < addressLinesTwo.size();q++)
	                  {
	                     orderInfoSection.append(addSpacesToLine("") + (String)(addressLinesTwo.get(q)) + end());
	                     HTMLorderInfoSection.append(addStandardUpperRow("&nbsp;", (String)(addressLinesTwo.get(q))));
	                  }
	              }
	          }
	
	          if((tmpRecipientAddressesVO.getCountry() != null && tmpRecipientAddressesVO.getCountry().equalsIgnoreCase("US")) ||
	              (tmpRecipientAddressesVO.getCountry() != null && tmpRecipientAddressesVO.getCountry().equalsIgnoreCase("CA")))
	          {
	                 orderInfoSection.append( addSpacesToLine("") +
	                                          tmpRecipientAddressesVO.getCity() + ", " +
	                                          tmpRecipientAddressesVO.getStateProvince() + " " +
	                                          fixZipCode(tmpRecipientAddressesVO.getPostalCode()) + end());
	                 HTMLorderInfoSection.append(addStandardUpperRow("&nbsp;",
	                                          tmpRecipientAddressesVO.getCity() + ",&nbsp;" +
	                                          tmpRecipientAddressesVO.getStateProvince() + "&nbsp;" +
	                                          fixZipCode(tmpRecipientAddressesVO.getPostalCode())));
	          }
	          else
	          {
	                 orderInfoSection.append( addSpacesToLine("") + tmpRecipientAddressesVO.getCity() + end());
	                 HTMLorderInfoSection.append(addStandardUpperRow("&nbsp;", tmpRecipientAddressesVO.getCity()));
	          }
	
	          orderInfoSection.append(addSpacesToLine("") + translateCoutryType(tmpRecipientAddressesVO.getCountry()) + end());
	          HTMLorderInfoSection.append(addStandardUpperRow("&nbsp;", translateCoutryType(tmpRecipientAddressesVO.getCountry())));
	
	          if(tmpRecipientPhoneVO.getPhoneNumber() != null)
	          {
	            orderInfoSection.append(addSpacesToLine("") + tmpRecipientPhoneVO.getPhoneNumber().replace(' ','-') + end() + end());
	            HTMLorderInfoSection.append(addStandardUpperRow("&nbsp;", tmpRecipientPhoneVO.getPhoneNumber().replace(' ','-')));
	          }
	
	          String giftMessageTxt = "";
	          String giftMessageHtml = "";
	          if(tmpDetailVO.getCardMessage() != null)
	          {
	            giftMessageTxt += tmpDetailVO.getCardMessage();
	            giftMessageHtml += tmpDetailVO.getCardMessage();
	          }
	          if(tmpDetailVO.getCardSignature() != null){
	            giftMessageTxt += ("  " + tmpDetailVO.getCardSignature());
	            giftMessageHtml += ("&nbsp;&nbsp;" + tmpDetailVO.getCardSignature());
	          }
	          ArrayList linesTxt = splitText(giftMessageTxt);
	          ArrayList linesHtml = splitText(giftMessageHtml);
	          orderInfoSection.append(addSpacesToLine("Gift Message:") + ((String)linesTxt.get(0)).trim() + end());
	          HTMLorderInfoSection.append(this.HTMLend());
	          HTMLorderInfoSection.append(addStandardUpperRow("Gift Message:", ((String)linesHtml.get(0)).trim()));
	
	          if(linesTxt.size() > 1)
	          {
	            for(int j = 1; j < linesTxt.size(); j++)
	            {
	              orderInfoSection.append(addSpacesToLine("") + (String)linesTxt.get(j) + end());
	            }
	          }
	          if(linesHtml.size() > 1)
	          {
	            for(int j = 1; j < linesHtml.size(); j++)
	            {
	               HTMLorderInfoSection.append(addStandardUpperRow("&nbsp;", (String)linesHtml.get(j)));
	            }
	          }
	      }
          /* Added personalization to confirmation e-mail. */
          if(tmpDetailVO.getPersonalizationData() != null && !tmpDetailVO.getPersonalizationData().equals("")) {
              String personalization = null;
              Document doc = DOMUtil.getDocument(tmpDetailVO.getPersonalizationData());

              Node itemNode =  DOMUtil.selectSingleNode(doc, "personalizations/personalization/name/text()");
              if(itemNode != null) {
                  NodeList names = DOMUtil.selectNodes(doc, "personalizations/personalization/name/text()");
                  NodeList data = DOMUtil.selectNodes(doc, "personalizations/personalization/data/text()");

                  /* Get personalized portion only.
                   *    Ex: date=January 1, 2000  -->  January 1, 2000
                   *        name=glenn wil lewis  -->  glenn wil lewis
                   */
                  HTMLorderInfoSection.append(this.HTMLend());
                  for(int j = 0; j < names.getLength(); j++) {
                      personalization = data.item(j).getNodeValue();
                      /* If it is not the first personalization, do not display the label. */
                      if(j > 0) {
                          orderInfoSection.append(addSpacesToLine("") + personalization + end());
                          HTMLorderInfoSection.append(addStandardUpperRow("&nbsp;", personalization));
                      }
                      /* Else, display "Personalization: " as the label. */
                      else {
                          orderInfoSection.append(addSpacesToLine("Personalization:") + personalization + end());
                          HTMLorderInfoSection.append(addStandardUpperRow("Personalization:", personalization));
                      }
                  }
              }
          }

          orderInfoSection.append(end());
          HTMLorderInfoSection.append(this.HTMLend());
          HTMLorderInfoSection.append(endHTMLTable());

//SPECIAL INSTRUCTIONS


          if(tmpDetailVO.getSpecialInstructions() != null && !tmpDetailVO.getSpecialInstructions().equals(""))
          {
            HTMLorderInfoSection.append(this.startHTMLTable());
            orderInfoSection.append("Additional Recipient Information:\n");
            HTMLorderInfoSection.append(assSpecialInstructionsRow("Additional Recipient Information:"));

            if(!tmpRecipientAddressesVO.getAddressType().equalsIgnoreCase("FUNERAL HOME"))
            {
               orderInfoSection.append("PLEASE NOTE: We cannot honor requests for delivery at\nspecific times of day." + end());
               HTMLorderInfoSection.append(assSpecialInstructionsRow("PLEASE NOTE: We cannot honor requests for delivery at"));
               HTMLorderInfoSection.append(assSpecialInstructionsRow("specific times of day."));
            }
            orderInfoSection.append(end());
            HTMLorderInfoSection.append("<tr><td>&nbsp;</td></tr>");
            HTMLorderInfoSection.append(endHTMLTable());
            HTMLorderInfoSection.append("<br>");
          }


//PRODUCT DESCRIPTION AND PRICE

          HTMLorderInfoSection.append(this.startHTMLTable());
          orderInfoSection.append(justifyToLine(cleanHTMLEncoding(tmpDetailVO.getItemDescription()), "" + 
                                                formatDollars(tmpDetailVO.getProductsAmount())) + 
                                                formatMilesPoints(isMilesPointsOrder, tmpDetailVO.getMilesPointsAmtProduct()) +
                                                end());
          HTMLorderInfoSection.append(addStandardLowerRow(tmpDetailVO.getItemDescription(), "" + 
                                                          formatDollars(tmpDetailVO.getProductsAmount()) , "" +
                                                          formatMilesPoints(isMilesPointsOrder, tmpDetailVO.getMilesPointsAmtProduct())
                                                          ));
          orderInfoSection.append(tmpDetailVO.getProductId());
          
          //add personal greeting id
         if (tmpDetailVO.getPersonalGreetingId() != null && tmpDetailVO.getPersonalGreetingId() != "" )
          {
            orderInfoSection.append(end() + cu.getContent(this.connection ,BuildEmailConstants.PERSONAL_GREETING_CONTEXT , BuildEmailConstants.PERSONAL_GREETING_IDENTIFIER) +  "\t" + tmpDetailVO.getPersonalGreetingId() + end());
          }

              

          tmpStr = getColorSizeFlag(tmpDetailVO.getProductId());
//COLOR
          if(tmpStr != null && tmpStr.equalsIgnoreCase("C"))
          {
             tmpHashMap = this.getColorDescription();
             orderInfoSection.append(" ");
             if(tmpHashMap != null && tmpHashMap.size() > 0)
             {
                 orderInfoSection.append((String)tmpHashMap.get(tmpDetailVO.getColorFirstChoice()) + "\n2nd Choice - " + (String)tmpHashMap.get(tmpDetailVO.getColorSecondChoice()));
                 HTMLorderInfoSection.append(addStandardLowerRow(tmpDetailVO.getProductId() + "&nbsp;" + (String)tmpHashMap.get(tmpDetailVO.getColorFirstChoice()), "&nbsp;"));
                 HTMLorderInfoSection.append(addStandardLowerRow("2nd&nbspChoice&nbsp-&nbsp" + (String)tmpHashMap.get(tmpDetailVO.getColorSecondChoice())  , "&nbsp"));
                 //adding personal greeting itentifier and id
                 if (tmpDetailVO.getPersonalGreetingId() != null && tmpDetailVO.getPersonalGreetingId() != "" )
                 {
                   HTMLorderInfoSection.append(addStandardLowerRow(cu.getContent (this.connection,BuildEmailConstants .PERSONAL_GREETING_CONTEXT , BuildEmailConstants .PERSONAL_GREETING_IDENTIFIER) +  "\t" + tmpDetailVO.getPersonalGreetingId() , "&nbsp;"));                
                 }
              }
             else
             {
                 orderInfoSection.append(tmpDetailVO.getColorFirstChoice() + " \n2nd Choice - " + tmpDetailVO.getColorSecondChoice());
                 HTMLorderInfoSection.append(addStandardLowerRow(tmpDetailVO.getProductId() + "&nbsp" + tmpDetailVO.getColorFirstChoice(), "&nbsp;"));
                 HTMLorderInfoSection.append(addStandardLowerRow("2nd&nbspChoice&nbsp-&nbsp" +  tmpDetailVO.getColorSecondChoice(), "&nbsp;"));
                 //adding personal greeting id to orders where there are two choices
                 if (tmpDetailVO.getPersonalGreetingId() != null && tmpDetailVO.getPersonalGreetingId() != "" )
                 {
                   HTMLorderInfoSection.append(addStandardLowerRow(cu.getContent (this.connection,BuildEmailConstants .PERSONAL_GREETING_CONTEXT , BuildEmailConstants.PERSONAL_GREETING_IDENTIFIER) +  "\t" + tmpDetailVO.getPersonalGreetingId() , "&nbsp;"));                
                 }
                
             }
          }
          else
          {
              HTMLorderInfoSection.append(addStandardLowerRow(tmpDetailVO.getProductId(), "&nbsp;"));
              //adding personal greeting id where there are no color selections
              if (tmpDetailVO.getPersonalGreetingId() != null && tmpDetailVO.getPersonalGreetingId() != "" )
              {
                HTMLorderInfoSection.append(addStandardLowerRow(cu.getContent (this.connection,BuildEmailConstants.PERSONAL_GREETING_CONTEXT , BuildEmailConstants.PERSONAL_GREETING_IDENTIFIER) +  "\t" + tmpDetailVO.getPersonalGreetingId() , "&nbsp;"));                
              }

          }

          orderInfoSection.append(end());
          HTMLorderInfoSection.append(HTMLend());

          for(int y=0; y < tmpDetailVO.getAddOns().size(); y++)
          {
            tmpAddOnsVO = (AddOnsVO)tmpDetailVO.getAddOns().get(y);
//ADDONS
            if(tmpAddOnsVO.getAddOnCode() != null && tmpAddOnsVO.getAddOnCode().startsWith("RC"))
            {
              orderInfoSection.append(justifyToLine("Greeting Card Qty " + 
                                                    tmpAddOnsVO.getAddOnQuantity(), "" + 
                                                    formatDollars(tmpAddOnsVO.getPrice())) + 
                                                    formatMilesPoints(isMilesPointsOrder, tmpDetailVO.getMilesPointsAmtAddon()) +
                                                    end());
              HTMLorderInfoSection.append(addStandardLowerRow("Greeting Card Qty " + 
                                                              tmpAddOnsVO.getAddOnQuantity(), "" + 
                                                              formatDollars(tmpAddOnsVO.getPrice()), "" +
                                                              formatMilesPoints(isMilesPointsOrder, tmpDetailVO.getMilesPointsAmtAddon())));
            } 
            else
            {
              AddOnVO addOnVO = null;
              if (tmpAddOnsVO.getAddOnCode() != null)
              {
                 addOnVO = addOnUtility.getAddOnById(tmpAddOnsVO.getAddOnCode(),false,false, this.connection);
              }
              BigDecimal totalPrice = new BigDecimal(tmpAddOnsVO.getPrice());
              totalPrice = totalPrice.multiply(new BigDecimal(tmpAddOnsVO.getAddOnQuantity()));
              String totalPriceString = totalPrice.toString();
                
              String quantityString = addOnVO != null ? AddOnVO.ADD_ON_TYPE_VASE_ID.toString().equalsIgnoreCase(addOnVO.getAddOnTypeId()) ? "" : " Qty " + tmpAddOnsVO.getAddOnQuantity() : " Qty " + tmpAddOnsVO.getAddOnQuantity();
              orderInfoSection.append(justifyToLine("" + tmpAddOnsVO.getDescription() + 
                                                    quantityString, "" +
                                                    addOnVO == null ? formatDollars(tmpAddOnsVO.getPrice()) : addOnVO.getDisplayPriceFlag() ? formatDollars(totalPriceString) : "") + 
                                                    formatMilesPoints(isMilesPointsOrder, tmpDetailVO.getMilesPointsAmtAddon()) +
                                                    end());
              HTMLorderInfoSection.append(addStandardLowerRow(tmpAddOnsVO.getDescription() + 
                                                              quantityString, "" +
                                                              addOnVO == null ? formatDollars(tmpAddOnsVO.getPrice()) : addOnVO.getDisplayPriceFlag() ? formatDollars(totalPriceString) : "" +
                                                              formatMilesPoints(isMilesPointsOrder, tmpDetailVO.getMilesPointsAmtAddon())
                                                              ));
            }
          }
          orderInfoSection.append(end());
          HTMLorderInfoSection.append(HTMLend());
//DISCOUNT
          if(tmpDetailVO.getDiscountAmount() != null && !tmpDetailVO.getDiscountAmount().startsWith("0"))
          {
              orderInfoSection.append(justifyToLine("Discount:", "(" + formatDollars(tmpDetailVO.getDiscountAmount())) + ")" + end());
              HTMLorderInfoSection.append(addStandardLowerRow("Discount:","(" + formatDollars(tmpDetailVO.getDiscountAmount()) + ")"));
          }
//SHIPPING + SERVICE CHARGES
          BigDecimal totalFees = new BigDecimal(0);

          if(tmpDetailVO.getShippingFeeAmount() != null && !tmpDetailVO.getShippingFeeAmount().startsWith("0"))
          {
            BigDecimal shipFee = new BigDecimal( tmpDetailVO.getShippingFeeAmount());
            totalFees = totalFees.add(shipFee);
          }
    
          if(tmpDetailVO.getServiceFeeAmount() != null && !tmpDetailVO.getServiceFeeAmount().startsWith("0"))
          {
            BigDecimal serviceFee = new BigDecimal( tmpDetailVO.getServiceFeeAmount());
          //Same day upcharge code. If separateLineSameDayFee is true and has same day upcharge then subtract from service fee and then add
          //service fee to total fees. Or else service fee contains same day upcharge fees included. 
            if(separateLineSameDayFee && tmpDetailVO.getSameDayUpcharge() != null && (Float.parseFloat(tmpDetailVO.getSameDayUpcharge()) > 0)){
            	serviceFee = serviceFee.subtract(new BigDecimal(tmpDetailVO.getSameDayUpcharge()));            	
            }
            totalFees = totalFees.add(serviceFee);                      
          }
          
          // subtract Morning Delivery Fees from shipping/service fees if separateLineMorningDeliveryFee is Y and if morningDeliveryFee is greater than or equal to 0
          if(tmpDetailVO.getMorningDeliveryFee() != null && (Float.parseFloat(tmpDetailVO.getMorningDeliveryFee()) > 0)){
        	  if(totalFees.doubleValue() > 0)
        	  {
        		  totalFees = totalFees.subtract(new BigDecimal(tmpDetailVO.getMorningDeliveryFee()));
        	  }
          }
          
           //subtract Surcharge Fees  from shipping/service fee if displaySurcharge flag is Y and we will display separately from shipping/service fee       
          if ( StringUtils.equalsIgnoreCase(tmpDetailVO.getDisplaySurcharge(), "Y")  && StringUtils.equalsIgnoreCase(tmpDetailVO.getApplySurchargeCode(), "ON"))
          {
            BigDecimal fuelSurchargeFee = new BigDecimal( tmpDetailVO.getFuelSurcharge());
               if(totalFees.doubleValue() > 0)
               {
                  totalFees = totalFees.subtract(fuelSurchargeFee);
               }             
          }

          if(totalFees.doubleValue() > 0)
          {
            orderInfoSection.append(justifyToLine("Shipping/Service Fees:", "" + formatDollars(totalFees.doubleValue())) + end());
            HTMLorderInfoSection.append(addStandardLowerRow("Shipping/Service Fees:", "" + formatDollars(totalFees.doubleValue())));
          }
          
          //Start---Same day upcharge fields display
          logger.debug("In BuildOrderEmailHelper getOrderInfoSection displaySameDayUpcharge value is "+ displaySameDayUpcharge);
          logger.debug("In BuildOrderEmailHelper getOrderInfoSection global displaySameDayUpcharge value is "+ gDisplaySameDayUpcharge);
          logger.debug("In BuildOrderEmailHelper getOrderInfoSection sameDayUpcharge fee value is "+ tmpDetailVO.getSameDayUpcharge());
          if(separateLineSameDayFee && tmpDetailVO.getSameDayUpcharge() != null && (Float.parseFloat(tmpDetailVO.getSameDayUpcharge()) > 0)){
        	  String sameDayUpchargeLabel = CacheUtil.getInstance().getContentWithFilter("SAME_DAY_UPCHARGE", "CONFIRMATION_EMAIL_LABEL", null, null, false);
        	  logger.debug("In BuildOrderEmailHelper getOrderInfoSection sameDayUpcharge label is "+ sameDayUpchargeLabel);
        	  orderInfoSection.append(justifyToLine(sameDayUpchargeLabel+ ":", "" + formatDollars(tmpDetailVO.getSameDayUpcharge())) + end());
        	  HTMLorderInfoSection.append(addStandardLowerRow(sameDayUpchargeLabel+ ":", "" + formatDollars(tmpDetailVO.getSameDayUpcharge())) + end());
          }
          //End-----Same day upcharge fields display
          
          //Start---Morning delivery fields display         
          logger.debug("In BuildOrderEmailHelper getOrderInfoSection morningDeliveryFee value is "+ tmpDetailVO.getMorningDeliveryFee());
          
          if(tmpDetailVO.getMorningDeliveryFee() !=null && (Float.parseFloat(tmpDetailVO.getMorningDeliveryFee()) >= 0)){
        	  String morningDeliveryFeeLabel = CacheUtil.getInstance().getContentWithFilter("MORNING_DELIVERY", "MORNING_DELIVERY_LABEL", null, null, false);
        	  orderInfoSection.append(justifyToLine(morningDeliveryFeeLabel + ":","" + formatDollars(tmpDetailVO.getMorningDeliveryFee())) + end());
        	  HTMLorderInfoSection.append(addStandardLowerRow(morningDeliveryFeeLabel + ":","" + formatDollars(tmpDetailVO.getMorningDeliveryFee())) + end());
        		  }
          //End-----Morning delivery fields display
          
// add the fuel surcharge fee line after shipping/service fee line          
          logger.debug("In BuildOrderEmailHelper getOrderInfoSection getDisplaySurcharge value is "+ tmpDetailVO.getDisplaySurcharge());
          logger.debug("In BuildOrderEmailHelper getOrderInfoSection getApplySurchargeCode value is "+ tmpDetailVO.getApplySurchargeCode());
          
          if ( StringUtils.equalsIgnoreCase(tmpDetailVO.getDisplaySurcharge(), "Y") && StringUtils.equalsIgnoreCase(tmpDetailVO.getApplySurchargeCode(), "ON"))
          {
            BigDecimal fuelSurchargeFee = new BigDecimal( tmpDetailVO.getFuelSurcharge());// we will show surcharge fee even if its zero 
            String fuelSurchargeDescription = tmpDetailVO.getFuelSurchargeDescription();
            logger.debug("fuelSurchargeDescription is"+ fuelSurchargeDescription);
            orderInfoSection.append(justifyToLine(fuelSurchargeDescription+ ":", "" + formatDollars(fuelSurchargeFee.doubleValue())) + end());
            HTMLorderInfoSection.append(addStandardLowerRow(fuelSurchargeDescription+ ":", "" + formatDollars(fuelSurchargeFee.doubleValue())) + end());
          }
          
//TAX
          if(tmpDetailVO.getTaxAmount() != null && !tmpDetailVO.getTaxAmount().startsWith("0"))
          {
            orderInfoSection.append(justifyToLine("Tax:", "" + formatDollars(tmpDetailVO.getTaxAmount())) + end());
            HTMLorderInfoSection.append(addStandardLowerRow("Tax:", "" + formatDollars(tmpDetailVO.getTaxAmount())));
          }

          orderInfoSection.append(justifyToLine(line(16), line(9)) + end());
          HTMLorderInfoSection.append(addStandardLowerRow(line(16), line(9)));
//SUBTOTAL
          orderInfoSection.append(justifyToLine("Subtotal", "" + formatDollars(tmpDetailVO.getExternalOrderTotal())) + formatMilesPoints(isMilesPointsOrder, tmpDetailVO.getMilesPointsAmt()) + end() + end());
          HTMLorderInfoSection.append(addStandardLowerRow("Subtotal", "" + formatDollars(tmpDetailVO.getExternalOrderTotal()), "" + formatMilesPoints(isMilesPointsOrder, tmpDetailVO.getMilesPointsAmt())));
//LAST MINUT GIFT EMAIL
          if(tmpDetailVO.getLastMinuteGiftEmail() != null && !tmpDetailVO.getLastMinuteGiftEmail().equals(""))
          {
              orderInfoSection.append("An e-mail will be sent to " + tmpDetailVO.getLastMinuteGiftEmail() + " from " + tmpDetailVO.getLastMinuteGiftSignature());
              HTMLorderInfoSection.append(addStandardLowerRow("An e-mail will be sent to " + tmpDetailVO.getLastMinuteGiftEmail() + " from " + tmpDetailVO.getLastMinuteGiftSignature(), "&nbsp;"));
          }

          HTMLorderInfoSection.append(endHTMLTable());

          orderInfoSection.append(line());
          HTMLorderInfoSection.append("<br><hr align=\"left\" width=\"580\" noshade size=\"1\"></br>");

      }

      /*tmpPaymentsVO = (PaymentsVO)order.getPayments().get(0);
      if(!tmpPaymentsVO.getCreditCards().isEmpty())
      {
        tmpCreditCardsVO = (CreditCardsVO)tmpPaymentsVO.getCreditCards().get(0);
      }
      else
      {
        tmpCreditCardsVO = new CreditCardsVO(); //just blank
      }*/
      orderInfoSection.append(end());
      HTMLorderInfoSection.append(startHTMLTable());

//GIFT CERTIFICATE
      List payments = order.getPayments();

      double giftCertTotal = 0;
      boolean hasGiftCert = false;
      if(payments != null)
      {
        for(int i=0;i<payments.size();i++)
        {
          PaymentsVO payment = (PaymentsVO)payments.get(i);
          String giftID = payment.getGiftCertificateId();


          if(giftID != null && giftID.length() > 0)
          {
            hasGiftCert = true;
            // USECASE 23038 - Unlike GC, GD retains balance.
            double giftCertAmount = 0;
            if(payment.getPaymentType().equals("GC")) { 
            	giftCertAmount = getGiftCertificateAmount(giftID);
            } else if(payment.getPaymentType().equals("GD")) { 
            	giftCertAmount =  Double.parseDouble(payment.getAmount());
            }
            giftCertTotal = giftCertTotal + giftCertAmount;
            


            orderInfoSection.append(justifyToLine("Gift Card/Certificate:", "("+ formatDollars(giftCertTotal) + ")") + end());
            HTMLorderInfoSection.append(addStandardLowerRow("Gift Card/Certificate:", "("+ formatDollars(giftCertTotal) + ")"));
          }else{
          	tmpPaymentsVO = payment;
            if(!tmpPaymentsVO.getCreditCards().isEmpty())
            {
              tmpCreditCardsVO = (CreditCardsVO)tmpPaymentsVO.getCreditCards().get(0);
            }
            else
            {
              tmpCreditCardsVO = new CreditCardsVO(); //just blank
            }
        }
        }
      }

//TOTAL CHARGE
      //Get total amount from order object
      double orderTotal = 0;
      if(order.getOrderTotal() != null && order.getOrderTotal().length() > 0)
      {
        orderTotal = Double.parseDouble(order.getOrderTotal());
      }

      //calculate total charge if get certificate used
      if(giftCertTotal > 0)
      {
        if(orderTotal < giftCertTotal)
        {
          orderTotal = 0;
        }
        else
        {
          orderTotal = orderTotal - giftCertTotal;
        }
      }

      //format total for display
      NumberFormat n = NumberFormat.getCurrencyInstance(Locale.US);
      String orderTotalFormatted = n.format(orderTotal);
      String milesTotalFormatted = "";
      if(isMilesPointsOrder) {
        milesTotalFormatted = formatMilesPoints(true, tmpPaymentsVO.getMilesPointsAmt());
      }

      if(recalculateOrderSourceCodeVO.getJcpenneyFlag() != null && recalculateOrderSourceCodeVO.getJcpenneyFlag().equals("Y"))
      {
        orderInfoSection.append(justifyToLine("Approximate Total:",  orderTotalFormatted) + milesTotalFormatted + end());
        HTMLorderInfoSection.append(addStandardLowerRow("Approximate Total:",  orderTotalFormatted, milesTotalFormatted));
      }
      else
      {
        orderInfoSection.append(justifyToLine("Total Charge:", orderTotalFormatted) + milesTotalFormatted + end());
        HTMLorderInfoSection.append(addStandardLowerRow("Total Charge:", orderTotalFormatted, milesTotalFormatted));
      }

//CREDIT CARD TYPE
      if(tmpPaymentsVO != null && tmpPaymentsVO.getPaymentMethodType() != null && tmpPaymentsVO.getPaymentMethodType().equalsIgnoreCase("C"))
      {
          //only display if the order total is > 0
          if(orderTotal > 0){
            orderInfoSection.append(addSpacesToLine("Credit Card Type:") + translateCCType(tmpCreditCardsVO.getCCType()));
            HTMLorderInfoSection.append(addStandardLowerRow("Credit Card Type:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + translateCCType(tmpCreditCardsVO.getCCType()),"&nbsp;"));
          }
      }

//ALT PAY PAYMENT
      if(tmpPaymentsVO != null && tmpPaymentsVO.getPaymentMethodType() != null && tmpPaymentsVO.getPaymentMethodType().equalsIgnoreCase("P"))
      {
        String paymentTypeDesc = tmpPaymentsVO.getPaymentTypeDesc();
        orderInfoSection.append("Payment Method:\t" + paymentTypeDesc);
        HTMLorderInfoSection.append(this.addBulkStandardSummaryRow("Payment Method:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + paymentTypeDesc, "&nbsp;"));
      }

      orderInfoSection.append(end() + end());
      HTMLorderInfoSection.append(HTMLend());

      orderInfoSection.append("All prices are in US dollars"  );
      HTMLorderInfoSection.append(addStandardLowerRow("All prices are in US dollars", "&nbsp;"));
      
//PERSONAL GREETING INSTRUCTIONS
        if (tmpDetailVO.getPersonalGreetingId() != null && tmpDetailVO.getPersonalGreetingId() != "" )
        {
          String personalGreetingPhoneToken = BuildEmailConstants.PERSONAL_GREETING_PHONE_NUMBER_TOKEN;
          String phoneNumber = cu.getContent(this.connection, BuildEmailConstants.PERSONAL_GREETING_CONTEXT, BuildEmailConstants.PERSONAL_GREETING_PHONE_NUMBER_FILTER);
          
          //get the html instructions and replace the phone number
          
          String personalGreetingInstructions = cu.getContent(this.connection ,BuildEmailConstants.PERSONAL_GREETING_CONTEXT, BuildEmailConstants.PERSONAL_GREETING_INSTRUCTIONS_HTML);
          personalGreetingInstructions = personalGreetingInstructions.replaceAll(personalGreetingPhoneToken,phoneNumber);
	  HTMLorderInfoSection.append(addStandardLowerRow(personalGreetingInstructions , "&nbsp"));
          
         //get the text instructions and replace the phone number

          personalGreetingInstructions = cu.getContent(this.connection ,BuildEmailConstants.PERSONAL_GREETING_CONTEXT, BuildEmailConstants.PERSONAL_GREETING_INSTRUCTIONS_TEXT);
          personalGreetingInstructions = personalGreetingInstructions.replaceAll(personalGreetingPhoneToken,phoneNumber);   
	  orderInfoSection.append(end() + personalGreetingInstructions + end());
	}


//REWARD INFORMATION
      String partnerId = recalculateOrderSourceCodeVO.getPartnerId();
      PartnerVO partner = this.getPartner(partnerId);
      if(partnerId != null && !partnerId.equals(""))
      {
        if(partner.getEmailExludeFlag() != null && !partner.getEmailExludeFlag().equalsIgnoreCase("Y"))
        {
          if(partner.getRewardName() != null && partner.getRewardName().equalsIgnoreCase("CHARITY"))
          {
            orderInfoSection.append("\n");
            orderInfoSection.append("FTD.COM will donate 5% of the product price to " + partner.getPartnerName() + end());
            HTMLorderInfoSection.append(addStandardLowerRow("FTD.COM will donate 5% of the product price to " + partner.getPartnerName(), "&nbsp;"));
          }
          else
          if(partner.getPartnerName() != null && partner.getPartnerName().equalsIgnoreCase("DISCOVER"))
          {
            orderInfoSection.append("\n");
            orderInfoSection.append("Your purchase from FTD.COM has earned you " + formatDollars(order.getPartnershipBonusPoints()) + " towards your Cash Back Bonus with Discover." + end());
            HTMLorderInfoSection.append(addStandardLowerRow("Your purchase from FTD.COM has earned you " + formatDollars(order.getPartnershipBonusPoints()) + " towards your Cash Back Bonus with Discover.", "&nbsp;"));
          }
          else
          if(partner.getPartnerName() != null && partner.getPartnerName().equalsIgnoreCase("UPROMISE"))
          {
            orderInfoSection.append("\n");
            orderInfoSection.append("Your purchase has earned you " + formatDollars(order.getPartnershipBonusPoints()) + " back as college savings into your Upromise account!" + end());
            HTMLorderInfoSection.append(addStandardLowerRow("Your purchase has earned you " + formatDollars(order.getPartnershipBonusPoints()) + " back as college savings into your Upromise account!", "&nbsp;"));
          }
          else
          {
            long points = 0;
            if(order.getPartnershipBonusPoints() != null)
            {
              points = Math.round(Double.parseDouble(order.getPartnershipBonusPoints()));
            }
            orderInfoSection.append("\n");
            orderInfoSection.append("You will earn " + points + " " + partner.getPartnerName() + " " + partner.getRewardName() + end());
            orderInfoSection.append("Please allow 6-8 weeks for miles/points to post to your account." + end());
            HTMLorderInfoSection.append(addStandardLowerRow("You will earn " + points + " " + partner.getPartnerName() + " " + partner.getRewardName(), "&nbsp;"));
            HTMLorderInfoSection.append(addStandardLowerRow("Please allow 6-8 weeks for miles/points to post to your account.", "&nbsp;"));
          }
        }
      }

      orderInfoSection.append(end());
      //HTMLorderInfoSection.append(this.HTMLend());
      HTMLorderInfoSection.append(this.endHTMLTable());
      HTMLorderInfoSection.append(this.closeHTML());

      sections.put(BuildEmailConstants.PLAIN_TEXT, orderInfoSection.toString());
      sections.put(BuildEmailConstants.HTML, HTMLorderInfoSection.toString());

      return sections;
  }

  private String cleanHTMLEncoding(String s)
  {
      String tmp = "";
      ArrayList htmlEncs = new ArrayList();
      ArrayList decEncs = new ArrayList();
      htmlEncs.add("&reg;");
      htmlEncs.add("&#153;");
      decEncs.add(new String(new byte[]{(byte)174}));
      decEncs.add(new String(new byte[]{(byte)153}));

      tmp = s;
      for(int i = 0; i < htmlEncs.size(); i++)
      {
         tmp = this.replace(tmp, (String)htmlEncs.get(i), (String)decEncs.get(i));
      }

      return tmp;
  }


  private String replace(String strSource, String strFrom, String strTo)
  {
    if(strFrom == null || strFrom.equals(""))
        return strSource;
    String strDest = "";
    int intFromLen = strFrom.length();
    int intPos;

    while((intPos = strSource.indexOf(strFrom)) != -1)
    {
        strDest = strDest + strSource.substring(0,intPos);
        strDest = strDest + strTo;
        strSource = strSource.substring(intPos + intFromLen);
    }
    strDest = strDest + strSource;

    return strDest;
  }

  private ArrayList splitText(String giftMessage)
  {



      if(giftMessage == null)
      {
        return new ArrayList();
      }

      String part1 = giftMessage;
      String part2 = "";

      if(giftMessage.length() > SPLIT_SIZE)
      {
            StringCharacterIterator cIt = null;
            // Go back to first space
            int spaceIndex = SPLIT_SIZE;
            cIt = new StringCharacterIterator(giftMessage);
            cIt.setIndex(spaceIndex);
            for(char c = cIt.current(); c != CharacterIterator.DONE; c = cIt.previous())
            {
                if(c == ' ')
                {
                    break;
                }
                spaceIndex--;
            }

            if(spaceIndex != -1)
            {
                part1 = giftMessage.substring(0, spaceIndex).trim();
                part2 = giftMessage.substring(spaceIndex).trim();
            }
            else
            {
                part1 = giftMessage.substring(0, SPLIT_SIZE).trim();
                part2 = giftMessage.substring(SPLIT_SIZE).trim();
            }
      }

    ArrayList tmplines = new ArrayList();
    tmplines.add(part1);
    if(part2 != null && part2.length() > 0){
      tmplines.add(part2);
    }

    return tmplines;
  }

  private String end()
  {
    return "\n";
  }

  private String line()
  {
    return "\n-------------------------------------------------------------------------------\n";
  }

  private String line(int count)
  {
      if(count < 0)
      {
        return "";
      }

      StringBuffer buf = new StringBuffer("");
      for(int i = 0; i < count; i++)
      {
        buf.append("-");
      }
      return buf.toString();
  }

  private String justifyToLine(String start, String end )
  {
      StringBuffer buf = new StringBuffer("");
      buf.append(start);
      buf.append(this.createSpaces(56-(start.length() + end.length())));
      buf.append(end);

      return buf.toString();
  }

  private String addSpacesToLine(String s)
  {
      int charsToLine = 28;
      int spacessToAdd = charsToLine - s.length();
      if(spacessToAdd < 1)
      return s;
      s = (s + createSpaces(spacessToAdd));
      return s;
  }

  private String createSpaces(int count)
  {
      if(count < 0)
      {
        return "";
      }

      StringBuffer buf = new StringBuffer("");
      for(int i = 0; i < count; i++)
      {
        buf.append(" ");
      }
      return buf.toString();
  }

  private String translateCCType(String inType)
  {

      if(inType == null || inType.equals(""))
      {
        return "";
      }
      else
      {
        CachedResultSet rs = null;
        String outType = "";
        boolean found = false;

         try
         {
            DataRequest dataRequest = new DataRequest();
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

            dataRequest.setConnection(this.connection);
            dataRequest.setStatementID("GET CC TYPES");
            rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);
            while(rs.next())
            {
              if(((String)rs.getObject(1)).equalsIgnoreCase(inType))
              {
                   outType = (String)rs.getObject(2);
                   found = true;
              }
            }
          }
          catch(Exception e)
          {}

          if(!found)
          return inType.toUpperCase();

          return outType.toUpperCase();
      }
  }

  private String translateAddressType(String inType)
  {
      if(inType == null || inType.equals(""))
      {
        return "Residence";
      }
      else
      if(inType.equalsIgnoreCase("H") || inType.equalsIgnoreCase("HOSPITAL"))
      {
        return "Hospital";
      }
      else
      if(inType.equalsIgnoreCase("F") || inType.toUpperCase().startsWith("FUNERAL"))
      {
        return "Funeral";
      }
      else
      if(inType.equalsIgnoreCase("B") || inType.equalsIgnoreCase("BUSINESS") || inType.toUpperCase().startsWith("BUS"))
      {
        return "Business";
      }
      else
      if(inType.equalsIgnoreCase("C") || inType.toUpperCase().startsWith("CEMETERY"))
      {
        return "Cemetery";
      }
      else
      if(inType.equalsIgnoreCase("N") || inType.toUpperCase().startsWith("NURSING"))
      {
        return "Nursing Home";
      }
      else
      {
        return "Residence";
      }
  }

  private String translateCoutryType(String inType)
  {
      if(inType == null || inType.equals(""))
        return " ";
      else
      if(inType.equalsIgnoreCase("US"))
        return "USA";
      else
      if(inType.equalsIgnoreCase("CA"))
        return "CANADA";
      else
      if(inType.equalsIgnoreCase("UK"))
        return "GREAT BRITAIN";
      else
      if(inType.equalsIgnoreCase("FR"))
        return "FRANCE";
      else
      if(inType.equalsIgnoreCase("DE"))
        return "GERMANY";
      else
      if(inType.equalsIgnoreCase("AU"))
        return "AUSTRALIA";
      else
        return inType;

  }

  private String translateShippingMethod(String s)
  {
    if(s == null || s.equals(""))
    {
      return "";
    }
    else
    if(s.equalsIgnoreCase("SA"))
    {
      return "Saturday Delivery";
    }
    else
    if(s.equalsIgnoreCase("SD"))
    {
      return "Saturday Delivery";
    }
    else
    if(s.equalsIgnoreCase("ND"))
    {
      return "Next Day Delivery";
    }
    else
    if(s.equalsIgnoreCase("2F"))
    {
      return "Two Day Delivery";
    }
    else
    if(s.equalsIgnoreCase("GR"))
    {
      return "Standard Delivery";
    }
    else
    {
      return s;
    }
  }

  private HashMap getColorDescription()
  {
     CachedResultSet rs = null;
     HashMap colors = new HashMap();

     try
     {
        DataRequest dataRequest = new DataRequest();
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("GET_COLORS");
        rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);
        while(rs.next())
        {
          colors.put((String)rs.getObject(1), (String)rs.getObject(2));
        }
      }
      catch(Exception e)
      {}

      return colors;
  }

  private String getColorSizeFlag(String productId)
  {
     CachedResultSet rs = null;
     String colorSizeFlag = null;

     try
     {
        DataRequest dataRequest = new DataRequest();
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("GET_PRODUCT_BY_ID");
        dataRequest.addInputParam("IN_PRODUCT_ID", productId);
        rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);
        rs.next();
        colorSizeFlag = (String)rs.getObject(10);
      }
      catch(Exception e)
      {
        colorSizeFlag = null;
      }
      return colorSizeFlag;
   }

//    private boolean isJCPSourceCode(String sourceIn)
//    {
//       CachedResultSet rs = null;
//       String tmp = null;
//       boolean jcpSourceCode = false;
//
//       try
//       {
//          DataRequest dataRequest = new DataRequest();
//          DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
//
//          dataRequest.setConnection(this.connection);
//          dataRequest.setStatementID("GLOBAL.GET_SOURCE_CODE_RECORD");
//          dataRequest.addInputParam("IN_SOURCE_CODE", sourceIn);
//          rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);
//          rs.next();
//          tmp = (String)rs.getObject(30);
//          if(tmp != null && tmp.equalsIgnoreCase("Y"))
//          jcpSourceCode = true;
//        }
//        catch(Exception e)
//        {}
//
//        return jcpSourceCode;
//    }

    /**
     *  
     */
    private RecalculateOrderSourceCodeVO  getSourceCodeDetails(String sourceIn)
    {
       CachedResultSet rs = null;       
       RecalculateOrderSourceCodeVO recalculateOrderSourceCodeVO = new RecalculateOrderSourceCodeVO();
       try
       {
          DataRequest dataRequest = new DataRequest();
          DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
          
          dataRequest.setConnection(this.connection);
          dataRequest.setStatementID("GLOBAL.GET_SOURCE_CODE_RECORD");
          dataRequest.addInputParam("IN_SOURCE_CODE", sourceIn);
          rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);
          rs.next();
          recalculateOrderSourceCodeVO.setPartnerId((String)rs.getString("PARTNER_ID"));
          recalculateOrderSourceCodeVO.setJcpenneyFlag((String)rs.getString("JCPENNEY_FLAG"));
          recalculateOrderSourceCodeVO.setDisplaySameDayUpcharge((String)rs.getString("DISPLAY_SAME_DAY_UPCHARGE"));
          recalculateOrderSourceCodeVO.setMorningDeliveryFlag((String)rs.getString("MORNING_DELIVERY_FLAG"));
        }
        catch(Exception e) {
        	e.printStackTrace();
        }
        return recalculateOrderSourceCodeVO;
    }

    private PartnerVO getPartner(String partnerId)
    {
       CachedResultSet rs = null;
       PartnerVO partner = new PartnerVO();

       try
       {
          DataRequest dataRequest = new DataRequest();
          DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

          dataRequest.setConnection(this.connection);
          dataRequest.setStatementID("GET_PARTNER");
          dataRequest.addInputParam("PARTNER_ID", partnerId);
          rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);
          rs.next();

          partner.setPartnerId(partnerId);
          partner.setPartnerName((String)rs.getObject(1));
          partner.setCustomerInfo((String)rs.getObject(2));
          partner.setRewardName((String)rs.getObject(3));
          partner.setIdLength("");
          partner.setEmailExludeFlag((String)rs.getObject(5));

        }
        catch(Exception e)
        {
          e.printStackTrace();
        }

        return partner;
    }

   private String convertDate(String inDate, int type) throws Exception
   {
      String outDate = null;
      String inDateFormat = "";
      if(type == 1)
      inDateFormat = "EEE MMM dd HH:mm:ss zzz yyyy";
      if(type == 2)
      inDateFormat = "MMM dd yyyy HH:mma";
      if(type == 3)
      inDateFormat = "MM/dd/yyyy";

      try
      {
         String outDateFormat = "EEEE MM/dd/yyyy";
         SimpleDateFormat sdfInput = new SimpleDateFormat(inDateFormat);
         SimpleDateFormat sdfOut = new SimpleDateFormat(outDateFormat);
         java.util.Date date = sdfInput.parse(inDate);
         outDate = sdfOut.format(date);
      }
      catch(Exception ex)
      {
        outDate = inDate;
      }

      return outDate;
    }

    private String fixZipCode(String inZip)
    {
      String outZip = "";

      if(inZip.length() <= 5)
      return inZip;

      if(inZip.indexOf("-") >= 0)
      return inZip;

      try
      {
        String first = inZip.substring(0, 5);
        String second = inZip.substring(5, inZip.length());
        outZip = first + "-" + second;
      }
      catch(Exception e)
      {
        outZip = inZip;
      }

      return outZip;
    }

    private ArrayList splitAddressLine(String inAddress)
    {
      ArrayList outAddress = new ArrayList();
      if(inAddress == null || inAddress.length() <= 30)
      {
         outAddress.add(inAddress);
         return outAddress;
      }
      else
      {
        try
        {
          String one = inAddress.substring(0, 30);
          String remain = inAddress.substring(30, inAddress.length());

          for(int i = one.length()-1; i >= 0; i--)
          {
            if(one.charAt(i) == ' ')
            {
              break;
            }
            else
            {
                remain = one.charAt(i) + remain;
                one = one.substring(0, one.length() - 1);
            }
          }
          outAddress.add(one);
          outAddress.add(remain);
        }
        catch(Exception e)
        {
          e.printStackTrace();
          outAddress.clear();
          outAddress.add(inAddress);
        }
      }

      return outAddress;
    }

    private String rightAlign(String s)
    {
        if(s.length() >= 9)
        return s;

        int len = s.length();
        int spaces = 9 - len;
        String ret = (this.createSpaces(spaces) + s );
        return ret;
    }

    private String truncDescription(String description)
    {
      if(description.length() > 25)
      description = description.substring(0, 25);
      else
      if(description.length() < 25)
      description = description + this.createSpaces(25 - description.length());

      return description;
    }

    private String startHTML()
    {
      return "<p>";
    }

    private String closeHTML()
    {
      return "</p>";
    }

    private String startHTMLTable()
    {
      return "<table width=\"700px\" style='font-family: Verdana, Arial, Helvetica, sans-serif; font: 10pt;'>";
        //return "<table width=\"700px\" border=\"2px\"  cellspacing=\"2px\"  cellpadding=\"2px\" style='font-family: Verdana, Arial, Helvetica, sans-serif; font: 10pt;'>";
    }

    private String endHTMLTable()
    {
      return "</table>";
    }

    private String addStandardUpperRow(String one, String two)
    {
      return 	"<tr>\n<td width=\"250px\"><font size=\"2\">" + one + "</font></td>\n<td><font size=\"2\">" + two + "</font></td>\n</tr>\n";
    }

    private String addStandardLowerRow(String one, String two)
    {
      return 	"<tr>\n<td width=\"500px\"><font size=\"2\">" + one + "</font></td>\n<td width=\"70px\" align=\"right\"><font size=\"2\">" + two + "</font></td>\n</tr>\n";
    }
    
    private String addStandardLowerRow(String one, String two, String three)
    {
      return    "<tr>\n<td width=\"500px\"><font size=\"2\">" + one + 
      "</font></td>\n<td width=\"70px\" align=\"right\"><font size=\"2\">" + two + 
      "</font></td>\n<td width=\"160px\" align=\"right\"><font size=\"2\">" + three + "</font></td>\n</tr>\n";
    }    

    private String HTMLend()
    {
      return "<tr><td><br></td></tr>\n";
    }

    private String addBulkStandardUpperRow(String one, String two)
    {
      return 	"<tr>\n<td width=\"150px\"><font size=\"2\">" + one + "</font></td>\n<td><font size=\"2\">" + two + "</font></td>\n</tr>\n";
    }

    private String addBulkInfoHeader()
    {
      return "<tr><td width=\"40px\" align=\"left\">QTY</td>" +
              "<td width=\"200px\">&nbsp;</td>" +
              "<td width=\"100px\">&nbsp;</td>" +
              "<td width=\"95px\">Delivery Date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>" +
              "<td width=\"65px\">Ship Method&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>" +
              "<td>&nbsp;</td></tr>" +
             "<tr><td width=\"40px\" align=\"left\">&nbsp;</td>" +
             "<td width=\"200px\">&nbsp;</td>" +
             "<td width=\"100px\">&nbsp;</td>" +
             "<td width=\"95px\">&nbsp;</td>" +
             "<td width=\"65px\">&nbsp;</td>" +
             "<td>&nbsp;</td></tr>";
    }

    private String addBulkStandardInfoRow(String one, String two, String three, String four, String five, String six)
    {
      return  "<tr><td width=\"40px\" align=\"left\"> <font size=\"2\">" + one + "</font>" +
              "</td><td width=\"200px\"><font size=\"2\">" + two  + "</font>" +
              "</td><td width=\"100px\"><font size=\"2\">" + three  + "</font>" +
              "</td><td width=\"95px\"><font size=\"2\">" + four  + "</font>" +
              "</td><td width=\"65px\"><font size=\"2\">" + five  + "</font>" +
              "</td><td width=\"90px\" align=\"right\"><font size=\"2\">" + six  + "</font>" + "</td></tr>";
    }

    private String addBulkStandardSummaryRow(String one, String two)
    {
      return  "<tr>" +
              "<td width=\"500px\" align=\"left\"> <font size=\"2\">" + one + "</font></td>" +
              "<td width=\"100px\" align=\"right\"><font size=\"2\">" + two + "</font></td>" +
              "</tr>";
    }

    private String startBulkInfoTable()
    {
      return "<table cellpadding=\"1\" cellspacing=\"1\">";
    }

    private String assSpecialInstructionsRow(String s)
    {
      return  "<tr>" +
              "<td><font size=\"2\">" + s + "</font></td>" +
              "</tr>";
    }

      /**
   * Description: Takes in a string Date, checks for valid formatting
   * 				in the form of mm/dd/yyyy and converts it
   * 				to a SQL date of yyyy-mm-dd.
   *
   * @param String date in string format
   * @return java.sql.Date
   *
   * Note..copied from OE project
   */
   public java.sql.Date getSQLDate(String strDate) throws Exception
   {
       java.sql.Date sqlDate = null;
        String inDateFormat = "";
        int dateLength = 0;
        int firstSep = 0;
        int lastSep = 0;

      try{
        if ((strDate != null) && (!strDate.trim().equals(""))) {

		    // set input date format
		    dateLength = strDate.length();
		    if ( dateLength > 10) {
			    firstSep = strDate.indexOf("/");
          if(firstSep == 1)
          {
            inDateFormat = "M/dd/yyyy hh:mm:ss";
          }
          else if (firstSep == 2)
          {
            inDateFormat = "MM/dd/yyyy hh:mm:ss";
          }
          else
          {
            inDateFormat = "yyyy-MM-dd hh:mm:ss";
          }

		    } else {
			    firstSep = strDate.indexOf("/");
			    lastSep = strDate.lastIndexOf("/");

			    switch ( dateLength ) {
				    case 10:
			    		inDateFormat = "MM/dd/yyyy";
				    	break;
				    case 9:
				    	if ( firstSep == 1 ) {
					    	inDateFormat = "M/dd/yyyy";
				    	} else {
					    	inDateFormat = "MM/d/yyyy";
			    		}
				    	break;
				    case 8:
				    	if ( firstSep == 1 ) {
					    	inDateFormat = "M/d/yyyy";
				    	} else {
					    	inDateFormat = "MM/dd/yy";
			    		}
				    	break;
				    case 7:
				    	if ( firstSep == 1 ) {
					    	inDateFormat = "M/dd/yy";
				    	} else {
					    	inDateFormat = "MM/d/yy";
			    		}
				    	break;
				    case 6:
				    	inDateFormat = "M/d/yy";
				    	break;
				    default:
				    	break;
			    }
		    }
            //SimpleDateFormat sdfInput = new SimpleDateFormat( "MM/dd/yyyy" );
            SimpleDateFormat sdfInput = new SimpleDateFormat(inDateFormat);
            SimpleDateFormat sdfOutput = new SimpleDateFormat ( "yyyy-MM-dd" );

            java.util.Date date = sdfInput.parse( strDate );
            String outDateString = sdfOutput.format( date );

          // now that we have no errors, use the string to make a SQL date
          sqlDate = sqlDate.valueOf(outDateString);
        }
        }
        catch(Exception ex)
        {
            throw(ex);
        }
        finally
        {
            return sqlDate;
        }
    }

    /**
     * This method retrieves the email type flag from the database.  When possible
     * this value will be retrieved from cache instead of the database.
     */
    public int getGlobalEmailFlag() throws Exception
    {
        String value = null;

        // Try cache first
        GlobalParmHandler parmHandler = (GlobalParmHandler)CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_GLOBAL_PARM);
        if(parmHandler != null)
        {
            value = parmHandler.getFrpGlobalParm(GLOBAL_PARAM_EMAIL_CONTEXT, GLOBAL_PARAM_EMAIL_TYPE_KEY);
        }
        else
        {
            DataRequest dataRequest = new DataRequest();
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            dataRequest.setConnection(connection);
            dataRequest.addInputParam("IN_CONTEXT", GLOBAL_PARAM_EMAIL_CONTEXT);
            dataRequest.addInputParam("IN_PARAM", GLOBAL_PARAM_EMAIL_TYPE_KEY);
            dataRequest.setStatementID("GET_GLOBAL_PARAM");
            String rs = (String)dataAccessUtil.execute(dataRequest);
            value = rs;
            dataRequest.reset();
        }

        int returnValue = -1;
        if(value.equals(BuildEmailConstants.MULTIPART_DB_FLAG))
        {
          returnValue = EmailVO.MULTIPART;
        }
        else if(value.equals(BuildEmailConstants.TEXT_DB_FLAG))
        {
          returnValue = EmailVO.PLAIN_TEXT;
        }

        return returnValue;
    }

    public String startGlobalHTML()
    {
      return "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">" +
      "<html>" +
      "<head>" +
      "<title>Untitled</title>" +
      "</head>" +
      "<body style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font: 10pt;\">";
    }

    public String closeGlobalHTML()
    {
      return "</body></html>";
    }

    private  boolean  isPriceZero(String price)
    {
      boolean isZero = true;

      if(price != null && price.length() > 0)
      {
          int intPrice = new Double(price).intValue();
          if(intPrice > 0)
          {
            isZero = false;
          }
      }

      return isZero;
    }

   private double getGiftCertificateAmount(String giftID) throws Exception
   {
        double returnValue = 0;


            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(connection);
            dataRequest.setStatementID("GET_GIFT_CERTIFICATE");
            dataRequest.addInputParam("GIFT_CERTIFICATE_ID", giftID);
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            CachedResultSet rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);
            if(rs.next())
            {
              BigDecimal big = (BigDecimal)rs.getObject(2);
              returnValue = big.doubleValue();
            }
      return returnValue;

   }

  /**
   * This method performs special formatting on the email content.
   *
   * @params EmailVO
   * @returns EmailVO
   */
  public EmailVO performSpecialFormatting(EmailVO emailVO) throws Exception
  {

    //ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
    int posStart = 0;
    int posEnd = 0;

    //get the from address domain
    String fromAddrDomain = "";
    if(emailVO.getRecipient() != null){
      posStart = emailVO.getRecipient().indexOf("@");
      if(posStart > 0){
        fromAddrDomain = emailVO.getRecipient().substring(posStart + 1);
      }
    }
    fromAddrDomain = fromAddrDomain.toUpperCase();

    //For certain orders the brackets within the text version of the email
    //need to be removed.  For all other orders the brackets along with the
    //text within the brackets needs to be removed.
    String removeBracketsKeepContent = cu.getProperty(EMAIL_SERVICE_CONFIG_FILE,"KEEP_BRACKET_CONTENT");

    removeBracketsKeepContent = removeBracketsKeepContent.toUpperCase();
    posStart = removeBracketsKeepContent.indexOf(fromAddrDomain);
    if(posStart >= 0)
    {
      //Remove brackets, keep content
      String content = replace(emailVO.getContent(),"[","");
      content = replace(content,"]","");
      emailVO.setContent(content);
    }
    else
    {
     //remove brackets and everything in them
     String content = emailVO.getContent();
     posStart = content.indexOf("[");
     posEnd = content.indexOf("]");
     while(posStart > 0 && posEnd > posStart)
     {
       String sub = content.substring(posStart,posEnd + 1);
       content = replace(content,sub,"");
       posStart = content.indexOf("[");
       posEnd = content.indexOf("]");
     }
     emailVO.setContent(content);
    }


    //For certain email address domains only a text version of the email should
    //be sent.
    String textOnly = cu.getProperty(EMAIL_SERVICE_CONFIG_FILE,"TEXT_ONLY_EMAIL");

    textOnly = textOnly.toUpperCase();
    posStart = textOnly.indexOf(fromAddrDomain);
    if(posStart >= 0)
    {
      emailVO.setContentType(EmailVO.AOL);
    }

    return emailVO;
  }

 private String formatDollars(double amt)
 {
   return formatDollars(Double.toString(amt));
 }

 private String formatDollars(String amt)
 {
   String ret = "";
   if(amt != null || amt.length() > 0)
   {
     try{
       NumberFormat dollarFormat = NumberFormat.getCurrencyInstance(Locale.US);
       double value = Double.parseDouble(amt);
       ret = dollarFormat.format(value);
     }
     catch(Throwable t)
     {
       //return unformatted value
       ret = amt;
     }//end catch
   }//end amt null

    return ret;
 }
 
 private String formatMilesPoints(boolean isMilesPoints, String milesPointsAmt)
 {
    String ret = "";
    
    if (isMilesPoints) {
        if(milesPointsAmt == null || milesPointsAmt.equals("")) {
            milesPointsAmt = "0";
        }
        ret = "(" + milesPointsAmt + " Miles/Points)";
    }
    return ret;
 }


  /**
   * Replaces the Source Code Token in the email link: ~sourcecode~
   * 
   * @param originalContent Email Text Content to search and replace
   * @param order Order to replace content for
   * @return The content with source code tokens replaced
   * @throws Exception On error retrieving source code information for the order
   */
  public String replaceSourceCodeToken(String originalContent, OrderVO order)
    throws Exception
  {
    String newContent = originalContent;

    if (originalContent.contains(BuildEmailConstants.SOURCE_CODE_TOKEN))
    {
      String sourceCode = order.getSourceCode();

      if (sourceCode == null || sourceCode.trim().length() == 0)
      {
        throw new Exception("The source code could not be retrieved for order " + order.getGUID());
      }
      newContent = originalContent.replaceAll(BuildEmailConstants.SOURCE_CODE_TOKEN, sourceCode);
    }

    return newContent;

  }

  /**
   * Replaces the various phone number tokens in the content.
   * 
   * Tokens replaced are: ~customertierphone~, ~<partnerlowercase>phonenumber~, ~phone~
   * 
   * @param originalContent Email Text Content to search and replace
   * @param order Order to replace content for
   * @return Content with phone number tokens replaced
   * @throws Exception On error retrieving token values information for the order
   */
  public String replacePhoneToken(String originalContent, OrderVO order)
    throws Exception
  {
    String newContent = originalContent;
    //ConfigurationUtil cu = ConfigurationUtil.getInstance();
    SegmentationUtil su = new SegmentationUtil();
    
    String tierLevel = su.getCustomerTierByScrubOrderGUID(connection, order.getGUID());
    String companyId = order.getCompanyId();
    String language = order.getLanguageId();

    logger.debug("###companyId: " + companyId);
	logger.debug("###languageId: " + language);
	logger.debug("###tierLevel: " + tierLevel);
	
	// #12961 - UC 24606 Include PC phone numbers for a PC orders when there
	// is no partner associated with order.
	HashSet<String> partners = PreferredPartnerUtility.getPreferredPartnerNames();
	
	if (replaceWidPCPhone(partners, order, originalContent)) {
		return replacePhoneTokenWidPCNo(tierLevel, order, newContent);
	}
	// UC 24606 - till here
    
    if (originalContent.contains(BuildEmailConstants.PHONE_NUMBER_TOKEN))
    {
        String phoneNumber = "";
        OrderDetailsVO tmpDetailVO = new OrderDetailsVO();
        Boolean premierFound = false;
    	
      for (int i = 0; i < order.getOrderDetail().size(); i++)
      {
        tmpDetailVO = ((OrderDetailsVO) (order.getOrderDetail().get(i)));
        if (tmpDetailVO.getPremierCollectionFlag().equalsIgnoreCase("Y") && (!premierFound))
        {
          phoneNumber = 
              cu.getContentWithFilter(connection, BuildEmailConstants.PHONE_NUMBER_CONTEXT, BuildEmailConstants.PREMIER_CALL_CENTER_CONTACT_NUMBER, 
                                      null, null);
          premierFound = true;
        }
      }
      if (!premierFound)
      {

        if(companyId != null && language != null){	
        	if(tierLevel != null){
	        	if((tierLevel.equals("A") && language.equals(this.ENGLISH)) || tierLevel.equals(this.SCI) || tierLevel.equals(this.SYMPATHY)){
			        phoneNumber = 
			            cu.getContentWithFilter(connection, BuildEmailConstants.PHONE_NUMBER_CONTEXT, BuildEmailConstants.CALL_CENTER_CONTACT_NUMBER, 
	                                tierLevel, null);
			        logger.debug("###phoneNumber A: " + phoneNumber);
	        	}
	        	else{
	            	phoneNumber = 
	            		cu.getContentWithFilter(connection, BuildEmailConstants.LANGUAGE_CONTEXT, BuildEmailConstants.PHONE_NUMBER_CONTEXT, 
	            							companyId, language);   
	            	logger.debug("###phoneNumber B: " + phoneNumber);
	        	}
        	}
        	else if (tierLevel == null){
            	phoneNumber = 
            		cu.getContentWithFilter(connection, BuildEmailConstants.LANGUAGE_CONTEXT, BuildEmailConstants.PHONE_NUMBER_CONTEXT, 
            							companyId, language);
            	logger.debug("###phoneNumber C: " + phoneNumber);
        	}
        }
        else if(companyId != null && language == null){
        	if(companyId.equals("FTD") || companyId.equals("FTDCA")){
            	phoneNumber = 
            		cu.getContentWithFilter(connection, BuildEmailConstants.LANGUAGE_CONTEXT, BuildEmailConstants.PHONE_NUMBER_CONTEXT, 
            							companyId, null);
            	logger.debug("###phoneNumber D: " + phoneNumber);
        	}
        }
      }

      //Check to make sure phone number is set to something
      //If not, set it appropriately and retest.
      if (phoneNumber == null || "".equals(phoneNumber))
      {    	  
        phoneNumber = 
            cu.getContentWithFilter(connection, BuildEmailConstants.PHONE_NUMBER_CONTEXT, BuildEmailConstants.CALL_CENTER_CONTACT_NUMBER, 
                                    tierLevel, null);

    	logger.debug("###phoneNumber E: " + phoneNumber);
        if (phoneNumber == null || "".equals(phoneNumber))
        { 
        String error = "Error retrieving the phone number for order " + 
                              order.getGUID() + ".  Company= "  + companyId + 
							  ", language= " + language + ", tier level=" + tierLevel + 
							  ". Ensure that a phone number entry exists in context.";
                               
        throw new Exception(error);
        }
      }
      newContent = originalContent.replaceAll(BuildEmailConstants.PHONE_NUMBER_TOKEN, phoneNumber);
    }


    // Replace any Partner Phone Tokens if they exist in the content
    for(String partner: PreferredPartnerUtility.getPreferredPartnerNames())
    {
      String partnerPhoneToken = "~" + partner.toLowerCase() + "phonenumber~";
      String partnerUpperCase = "" + partner.toUpperCase();
      
      if(logger.isDebugEnabled())
      {
        logger.debug("Checking for Partner Phone Token: " + partnerPhoneToken);
      }
      
      if (newContent.contains(partnerPhoneToken))
      {
        String phoneNumber = cu.getContentWithFilter(connection, BuildEmailConstants.PHONE_NUMBER_CONTEXT, BuildEmailConstants.CALL_CENTER_CONTACT_NUMBER, 
                                    partnerUpperCase, null);
  
        if (phoneNumber == null || phoneNumber.trim().length() == 0)
        {
          String error = "The Partner phone number could not be retreved for order " + order.getGUID() + 
                                " Ensure that a phone number entry exists in context " + 
                                BuildEmailConstants.PHONE_NUMBER_CONTEXT + " and filter value " + partnerUpperCase;
  
          throw new Exception(error);
        }
        
        if(logger.isDebugEnabled())
        {
          logger.debug("Replacing Partner Token: " + partnerPhoneToken + " with: " + phoneNumber);
        }
        
        newContent = newContent.replaceAll(partnerPhoneToken, phoneNumber);
      }
    }

	if (originalContent.contains(BuildEmailConstants.STOCK_MESSAGE_PHONE_NUMBER_TOKEN)) {
		String phoneNumber = cu.getContentWithFilter(connection, BuildEmailConstants.LANGUAGE_CONTEXT, BuildEmailConstants.PHONE_NUMBER_CONTEXT, 
				companyId, language);   
		logger.debug("###phoneNumber now equal: " + phoneNumber);

      if (phoneNumber == null || "".equals(phoneNumber))
      {    	  
        phoneNumber = 
            cu.getContentWithFilter(connection, BuildEmailConstants.PHONE_NUMBER_CONTEXT, BuildEmailConstants.CALL_CENTER_CONTACT_NUMBER, 
                                    tierLevel, null);

        logger.debug("###phoneNumber was empty, but now set to: " + phoneNumber);
        if (phoneNumber == null || "".equals(phoneNumber))
        { 
        String error = "Error retrieving the phone number for order " + 
                              order.getGUID() + ".  Company= "  + companyId + 
							  ", language= " + language + ", tier level=" + tierLevel + 
							  ". Ensure that a phone number entry exists in context.";
                     
        throw new Exception(error);
        }
      }
      newContent = newContent.replaceAll(BuildEmailConstants.STOCK_MESSAGE_PHONE_NUMBER_TOKEN, phoneNumber);
	}

    return newContent;

  }
  
  
  /**
   * Replaces the Partner Email Link Token. ~<partnerlowercase>emaillink~
   * 
   * @param originalContent Content to perform token replacement on
   * @param contentType Content Type for filtering the Content Tables: TEXT, HTML
   * @param order The order
   * @return Content with tokens replaced
   * @throws Exception
   */
  public String replaceEmailLinkToken(String originalContent, String contentType, OrderVO order)
    throws Exception
  {

    String newContent = originalContent;
    //ConfigurationUtil cu = ConfigurationUtil.getInstance();
    
    for(String partner: PreferredPartnerUtility.getPreferredPartnerNames())
    {
      String partnerEmailToken = "~" + partner.toLowerCase() + "emaillink~";
      String partnerUpperCase = "" + partner.toUpperCase();
      
      if(logger.isDebugEnabled())
      {
        logger.debug("Checking for Partner Email Token: " + partnerEmailToken);
      }
      
      if (newContent.contains(partnerEmailToken))
      {
        String emailLink = cu.getContentWithFilter(connection, "STOCK_EMAIL", "TOKEN", contentType, partnerUpperCase + "_EMAIL_LINK");
  
        if (emailLink == null || emailLink.trim().length() == 0)
        {
          String error = "The Partner email link could not be retrieved for order " + order.getGUID() + 
                         " Ensure that a email link entry exists in context=STOCK_EMAIL, name=TOKEN, Filter1= " + 
                         contentType + " and Filter2=" + partnerUpperCase + "_EMAIL_LINK";
  
          throw new Exception(error);
        }
        
        if(logger.isDebugEnabled())
        {
          logger.debug("Replacing Partner Token: " + partnerEmailToken + " with: " + emailLink);
        }
        
        newContent = newContent.replaceAll(partnerEmailToken, emailLink);
      }
    }    
    
    return newContent;
  }
  
  /**
   * Replaces the Company URL Token in the email link: ~companyurl~
   * 
   * @param originalContent Email Text Content to search and replace
   * @param order Order to replace content for
   * @return The content with CompanyUrl tokens replaced
   * @throws Exception On error retrieving CompanyUrl information for the order
   */
  public String replaceCompanyUrlToken(String originalContent, OrderVO order)
    throws Exception
  {
    String newContent = originalContent;

    if (originalContent.contains(BuildEmailConstants.COMPANY_URL_TOKEN))
    {
      String companyUrl = order.getCompanyUrl();

      if (companyUrl == null || companyUrl.trim().length() == 0)
      {
        throw new Exception("The company url could not be retrieved for order " + order.getGUID());
      }
      newContent = originalContent.replaceAll(BuildEmailConstants.COMPANY_URL_TOKEN, companyUrl);
    }

    return newContent;

  }
    
  /**
   * Replaces the Language Token "~language.message~".
   * 
   * @param originalContent Content to perform token replacement on
   * @param contentType Content Type for filtering the Content Tables: TEXT, HTML
   * @param order The order
   * @return Content with language tokens replaced
   * @throws Exception
   */
	public String replaceLanguageToken(String originalContent,
									   String contentType, 
									   OrderVO order) throws Exception {

		String newContent = originalContent;
		//ConfigurationUtil cu = ConfigurationUtil.getInstance();
		String languageId = StringUtils.trim(order.getLanguageId());
		
		if (originalContent.contains(BuildEmailConstants.LANGUAGE_TOKEN)) {
			String languageMessage = cu.getContentWithFilter(
				connection,
				BuildEmailConstants.LANGUAGE_CONTEXT,
				BuildEmailConstants.LANGUAGE_MESSAGE, 
				contentType, 
				languageId);
		
		if (StringUtils.isEmpty(languageMessage)){
			newContent = originalContent.replaceAll(
					BuildEmailConstants.LANGUAGE_TOKEN, "");
		} else {
			newContent = originalContent.replaceAll(
					BuildEmailConstants.LANGUAGE_TOKEN, languageMessage);
			}
		}
		return newContent;
	}

  
  /**
   * General function for replacing the various order specific tokens in the email content.
   * Replaces the Phone Token using {@link #replacePhoneToken}(). 
   * Replaces the Email Link Token using {@link #replaceEmailLinkToken}(). 
   * Replaces the Source Code Token using {@link #replaceSourceCodeToken}(). 
   * Replaces the Language Token using {@link #replaceLanguageToken}(). 
   * 
   * @param originalContent Content with tokens in it
   * @param contentType HTML or TEXT
   * @param order Order details
   * @return Content with tokens replaced
   * @throws Exception On any invalid tokens.
   */
  public String replaceOrderTokens(String originalContent, String contentType, OrderVO order) 
    throws Exception
  {
    if(logger.isDebugEnabled())
    {
      logger.debug("Replacing Original Content of type: " + contentType);
      logger.debug("sourceCode: " + order.getSourceCode());
      logger.debug("companyUrl: " + order.getCompanyUrl());
      logger.debug("Preferred Partners on Order: " + order.getPreferredProcessingPartners());
      logger.debug("Defined Preferred Partner Resources: " + PreferredPartnerUtility.getPreferredPartnerNames());
    }
    
    if(originalContent == null || originalContent.trim().length() == 0) 
    {
      return originalContent;
    }
    
    String replacedContent = originalContent;
    
    replacedContent = replacePhoneToken(replacedContent, order);
    replacedContent = replaceEmailLinkToken(replacedContent, contentType, order);
    replacedContent = replaceSourceCodeToken(replacedContent, order);
    replacedContent = replaceCompanyUrlToken(replacedContent, order);
    replacedContent = replaceLanguageToken(replacedContent, contentType, order);
    return replacedContent;
  }


/**
   * Replaces the email tokens for the content of the passed in email with the information 
   * pertaining to the order.
   * 
   * Uses {@link #replaceOrderTokens(Connection, String, String , OrderVO)}
   * 
   * @param email Email content to update
   * @param order Order information for the email
   * @throws Exception
   */
  public void updateEmailOrderTokens(EmailVO email, OrderVO order) 
    throws Exception
  {
    email.setContent(replaceOrderTokens (email.getContent(), "TEXT", order));
    email.setHTMLContent(replaceOrderTokens (email.getHTMLContent(), "HTML", order));
  }
  
	/** #12961 - Check if the email contains phone tokens and replace them with PC phone numbers.
	 * @param tierLevel
	 * @param order
	 * @param emailContent
	 * @return
	 * @throws Exception
	 */
	private String replacePhoneTokenWidPCNo(String tierLevel, OrderVO order, String emailContent) throws Exception {		
		String phoneNumber = cu.getContentWithFilter(connection, BuildEmailConstants.PHONE_NUMBER_CONTEXT,
				BuildEmailConstants.MEMBER_CALL_CENTER_CONTACT_NUMBER, "PC", null);
		if(StringUtils.isEmpty(phoneNumber)) {
			if(logger.isDebugEnabled()) {
				logger.debug("Unable to retrieve PC phone number from DB. Will be using the common call center contact number.");
			}
			phoneNumber = cu.getContentWithFilter(connection, BuildEmailConstants.PHONE_NUMBER_CONTEXT,
					BuildEmailConstants.CALL_CENTER_CONTACT_NUMBER, tierLevel, null);
		}		
		if(StringUtils.isEmpty(phoneNumber)) {
			if(logger.isDebugEnabled()) {
				logger.debug("Unable to retrieve common call center contact number. throwing exception");
			}
			String error = "Error retrieving the phone number for order " + order.getGUID()
				+ ".  Company= " + order.getCompanyId() + ", language= " + order.getLanguageId() + ", tier level="
				+ tierLevel + ". Ensure that a phone number entry exists in context.";	
			 throw new Exception(error);
		}		
		if (emailContent.contains(BuildEmailConstants.PHONE_NUMBER_TOKEN)) {				
			emailContent = emailContent.replaceAll(BuildEmailConstants.PHONE_NUMBER_TOKEN, phoneNumber);				
		}		
		if (emailContent.contains(BuildEmailConstants.STOCK_MESSAGE_PHONE_NUMBER_TOKEN)) {
			emailContent = emailContent.replaceAll(BuildEmailConstants.STOCK_MESSAGE_PHONE_NUMBER_TOKEN, phoneNumber);
		}
		return emailContent;	
	}

	/** #12961 - Checks if the order is PC member order and checks if the order is associated with any partner.
	 * @param partners
	 * @param order
	 * @param originalContent
	 * @return
	 */
	private boolean replaceWidPCPhone(HashSet<String> partners, OrderVO order, String originalContent) {
		boolean replaceWithPCPhNo = false;
		for (Object orderDetailVo : order.getOrderDetail()) {
			if (!replaceWithPCPhNo && ("Y".equals(((OrderDetailsVO) orderDetailVo).getPcFlag()))) {
				if (logger.isDebugEnabled()) {
					logger.debug("Is a premier circle member order");
				}
				replaceWithPCPhNo = true;
				break;
			}
		}		
		if (replaceWithPCPhNo) {
			for (String partner : partners) {
				String partnerPhoneToken = "~" + partner.toLowerCase() + "phonenumber~";
				if (replaceWithPCPhNo && originalContent.contains(partnerPhoneToken)) {
					if (logger.isDebugEnabled()) {
						logger.debug("Order contains partner phone token " + partnerPhoneToken);
					}
					replaceWithPCPhNo = false;
					break;
				}
			}
		}		
		return replaceWithPCPhNo;
	}
	
	/** Get order number as per the origin. If not FTD order, replace order number with the XX partner order number.
	 * @param tmpDetailVO
	 * @param orderOrigin
	 * @return
	 */
	private String getOrderNumber(OrderDetailsVO tmpDetailVO, String orderOrigin) {
		// Default value is FTD external order number.
		String orderNumber = tmpDetailVO.getExternalOrderNumber();
		
		if(StringUtils.isEmpty(orderOrigin)) {
			logger.error("Unable to determine if the order is partner order, invalid order origin");
			return orderNumber;
		}
		
		// If partner order replace it with partner order number
		CachedResultSet result = new PartnerUtility()
				.getPtnOrderDetailByConfNumber(tmpDetailVO.getExternalOrderNumber(), orderOrigin, connection);
		if (result != null && result.next()) {
			if (!StringUtils.isEmpty(result.getString("PARTNER_ORDER_ITEM_NUMBER"))) {
				orderNumber = result.getString("PARTNER_ORDER_ITEM_NUMBER");
			}
		}
		return orderNumber;
	}
}