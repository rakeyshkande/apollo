package com.ftd.osp.utilities;

import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.handlers.GlobalParmHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.SecureConfigHandler;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import java.net.URL;
import java.net.URLDecoder;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import org.xml.sax.SAXException;

/**
 * ConfigurationUtil is an updated version of ConfigurationUtil
 * that does not depend on CacheController or  Oracle XML Parser
 * or calls to any other Oracle Libraries
 */
public class ConfigurationUtil  {
  private static Logger logger = new Logger(ConfigurationUtil.class.getName());
  private static ConfigurationUtil CONFIGURATIONUTIL = new ConfigurationUtil();

  /**
   * Cache for the getPropertyNew and getPropertiesNew methods.
   */
  private static final HashMap propertyCache = new HashMap();

 /**
  * The static initializer for the ConfigurationUtil. It ensures that
  * at a given time there is only a single instance  
  * 
  * @exception SAXException
  * @exception ParserConfigurationException
  * @exception IOException
  * @return the ConfigurationUtil
  **/
  public static ConfigurationUtil getInstance() 
    throws  IOException, SAXException, ParserConfigurationException {
      return CONFIGURATIONUTIL;
  }

    private static final String SECURE_CONFIG_HANDLER = "CACHE_NAME_SECURE_CONFIG";
    private static final String GLOBAL_PARM_HANDLER   = "CACHE_NAME_GLOBAL_PARM";
    
          
    /**
     * Returns a property from a configuration file
     * This method just delegates to {@link #getPropertyNew(String, String)}
     * @param configurationFileName the configuration file name
     * @param propertyName the property name
     * @return the property value
     */
    public String getProperty(String configurationFileName, String propertyName) 
        throws TransformerException, IOException, SAXException, ParserConfigurationException {
        
        return  getPropertyNew(configurationFileName, propertyName);
    }

    /**
     * Returns a property from a configuration file - Note that this method uses the new method in the DOMUtil, and throws the Exceptions accordingly
     * 
     * @param configurationFileName the configuration file name
     * @param propertyName the property name
     * @exception TransformerException
     * @exception IOException
     * @exception SAXException
     * @exception ParserConfigurationException
     * @return the property value
     */
    public String getPropertyNew(String configurationFileName, String propertyName) 
          throws TransformerException, IOException, SAXException, ParserConfigurationException {

      Map properties = getPropertiesNew(configurationFileName);

      String value = (String)properties.get(propertyName);
      if (value == null) 
      {
          return "";
      }
      else
      {
        return  value;
      }
      
    }


    /**
     * Returns a secure system property
     * 
     * @param context - The configuration context
     * @param property - The property name
     * @exception Exception
     * @return the property value
     */

    public String getSecureProperty(String context, String name) 
          throws IOException, ParserConfigurationException, SAXException, 
          SQLException, Exception
      {
    	SecureConfigHandler sch = (SecureConfigHandler) 
            CacheManager.getInstance().getHandler(SECURE_CONFIG_HANDLER);
    	return sch.getProperty(context, name);
      }

    /**
     * Returns a normal system property from the database.
     * Ensure that CacheManager (cacheMgrConfig.xml) is configured for the 
     * {@link GlobalParmHandler} mapped to the cache name
     * {@value #GLOBAL_PARM_HANDLER}
     * 
     * @param context - The configuration context
     * @param name - The property name
     * @exception Exception
     * @return the property value
     */
    public String getFrpGlobalParm(String context, String name) 
            throws Exception
    {
        GlobalParmHandler gph = (GlobalParmHandler) 
            CacheManager.getInstance().getHandler(GLOBAL_PARM_HANDLER);
        return gph.getGlobalParm(context, name);
    }

    /**
     * Returns a normal system property from the database.  If a null value
     * is retrieved it is changed to a blank string.
     * 
     * Invokes {@link #getFrpGlobalParm}
     * 
     * @param context - The configuration context
     * @param name - The property name
     * @exception Exception
     * @return the property value
     */
    public String getFrpGlobalParmNoNull(String context, String name) 
            throws Exception
    {
        String globalParm = getFrpGlobalParm(context,name);
        if (globalParm == null)
        {
            globalParm = "";
        }
        return globalParm;
    }

  /**
   * Returns configurable content using the default filters
   * 
   * @param conn Database connection
   * @param context Content context
   * @param name Content name
   * @return Content value as a String
   * @throws Exception
   */
  public String getContent(Connection conn, String context, String name) 
             throws Exception
  {
      String value = null;
      
      value = getContentWithFilter(conn, context, name, null, null);
      
      return value;
  }

  /**
   * Returns configurable content given filter values
   * 
   * @param conn Database connection
   * @param context Content context
   * @param name Content name
   * @return Content value as a String
   * @throws Exception
   */
  public String getContentWithFilter(Connection conn, String context, String name, String filter1Value, String filter2Value) 
             throws Exception
  {
      String value = null;

      if(logger.isDebugEnabled())
      {
          logger.debug("IN_CONTENT_CONTEXT:" + context);
          logger.debug("IN_CONTENT_NAME:" + name);
          logger.debug("IN_CONTENT_FILTER_1_VALUE:" + filter1Value);
          logger.debug("IN_CONTENT_FILTER_2_VALUE:" + filter2Value);
      }

      DataAccessUtil dau = DataAccessUtil.getInstance();
      DataRequest dataRequest = new DataRequest();
      dataRequest.setConnection(conn);
      dataRequest.addInputParam("IN_CONTENT_CONTEXT", context);
      dataRequest.addInputParam("IN_CONTENT_NAME", name);
      dataRequest.addInputParam("IN_CONTENT_FILTER_1_VALUE", filter1Value);
      dataRequest.addInputParam("IN_CONTENT_FILTER_2_VALUE", filter2Value);
      dataRequest.setStatementID("GET_CONTENT_TEXT_WITH_FILTER");
      value = (String)dau.execute(dataRequest); 
        
      return value;
  }

  /**
   * Returns a Map of properties from a configuration file
   * 
   * This method just delegates the call to {@link #getPropertiesNew(String)}
   * 
   * @param configurationFileName the configuration file name
   * @exception TransformerException
   * @exception IOException
   * @exception SAXException
   * @exception ParserConfigurationException
   * @exception XSLException
   * @return the property value
   */
  public Map getProperties(String configurationFileName) 
             throws TransformerException, IOException, SAXException, 
             ParserConfigurationException,Exception
  {
      return getPropertiesNew(configurationFileName);
  }

  /**
   * Returns a Map of properties from a configuration file - Note that this method uses the new method in the DOMUtil, and throws the Exceptions accordingly
   * 
   * @param configurationFileName the configuration file name
   * @exception TransformerException
   * @exception IOException
   * @exception SAXException
   * @exception ParserConfigurationException
   * @return the property value
   */
  @SuppressWarnings({ "rawtypes", "unchecked" })
public Map getPropertiesNew(String configurationFileName) 
             throws TransformerException, IOException, SAXException, 
             ParserConfigurationException 
  {
      HashMap properties = null;
    
      // Search the cache for the config entry
      ConfigurationUtil.ConfigCacheEntry configCacheEntry = 
                          (ConfigCacheEntry) propertyCache.get(configurationFileName);

      // If entry found
      if (configCacheEntry != null)
      {
          // Check if Cache is stale
          if (configCacheEntry.isStale())
          {
              // if obsolete
              configCacheEntry = null;
              if(logger.isInfoEnabled())
              {
                logger.info("Configuration Cache is stale: " + configurationFileName);
              }
          } 
          else 
          {
              properties = (HashMap) configCacheEntry.configObject;
          }
      }
      
      if(properties == null)
      {
        properties = new HashMap();        
      }
    
      // If no configCacheEntry is found or this entry was obsolete
      if (configCacheEntry == null)
      {
          logger.info("Loading Properties File: " + configurationFileName);
      
          Class class_v = this.getClass();
          ClassLoader classLoader = class_v.getClassLoader();
          URL url = classLoader.getResource(configurationFileName);

          if (url == null) 
          {
              throw new IOException(configurationFileName + " could not be found");
          }
          
          if(logger.isDebugEnabled())
          {
            logger.debug("configutil decode is:" + new URLDecoder().decode(url.getFile(), "UTF-8"));
          }
          
          File configurationFile = new File(new URLDecoder().decode(url.getFile(), "UTF-8"));
          
          // reload the config file

          Document configDoc = null;
          try {
               configDoc = (Document)DOMUtil.getDocument(configurationFile); 
               logger.info("configutil created doc from file");
          }
          catch (FileNotFoundException e)
          {
            logger.debug("configutil filenotfoundexception");
            // JP Puzon 07-29-2005
            // Added this functionality to accomodate resources which
            // are not physically located on the local filesystem.
            configDoc = (Document)DOMUtil.getDocument(this.getClass().getClassLoader().getResourceAsStream(configurationFileName));
            logger.info("configutil created doc from resource stream");
          }

          //load the configurations into a java data structure
          NodeList property_nl = null;
          
          try
          {
              property_nl = (NodeList)DOMUtil.selectNodeList(configDoc, "//property");
          } catch (Exception e)
          {
              throw new SAXException(e);
          }
          
          Element property_node = null; 
          properties = new HashMap();
      
          for (int i = 0; i < property_nl.getLength(); i++) 
          {
            property_node = (Element) property_nl.item(i);
            if (property_node != null) 
            {
                properties.put(property_node.getAttribute("name"), property_node.getAttribute("value"));
            }
        
          }

          configCacheEntry = new ConfigCacheEntry(properties, configurationFile);
          if (configCacheEntry.isCacheable()) {
            synchronized(this)
            {
                propertyCache.put(configurationFileName, configCacheEntry);
            }      
          }
      } 

      return properties;
  }

  /**
   * Private class to hold config cache entry.
   */
  private class ConfigCacheEntry
  {
    /** When was the cached entry last modified. */
    private long lastModified;

    /** Cached config object. */
    private Object configObject;

    /** config file object. */
    private File configFile;

    /**
     * Constructs a new cache entry.
     * @param configObject object to cache.
     * @param configurationFile the configuration file.
     */
    private ConfigCacheEntry(final Object configObject, final File configFile){
      this.configObject = configObject;
      this.configFile = configFile;
      this.lastModified = configFile.lastModified();
    }
    
    
    /**
     * Checks the lastModified value as well as the file timestamp to determine if the Cache entry is stale.
     * True if the file was modified after the cache was loaded.
     * 
     * If the ConfigFile points to a resource, lastModified and configFile.lastModified are both 0.
     * 
     * @return
     */
    public boolean isStale() {
      if(lastModified < configFile.lastModified()) {
        return true;
      }
      
      return false;
    }
    
    /**
     * Checks if the data referred to by this Cache Entry is cacheable.
     * 
     * @return Currently, always returns <code>true</code>
     */
    public boolean isCacheable() {
      return true;
    }
  } 
}