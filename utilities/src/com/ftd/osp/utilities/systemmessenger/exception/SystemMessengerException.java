package com.ftd.osp.utilities.systemmessenger.exception;

public class SystemMessengerException extends Exception 
{
    public SystemMessengerException()
    {
        super();
    }

    public SystemMessengerException(String msg)
    {
        super(msg);
    }
}