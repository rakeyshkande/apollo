package com.ftd.osp.utilities.systemmessenger;


public class SystemMessengerVO 
{
    private String source;
    private String type;
    private String message;
    private boolean read;
    private int level;
    // JP Puzon 08-29-2005
    // Added the following subject parameter to support added
    // functionality of specifying the email subject line
    // of the alert.
    private String subject;

    public static final int LEVEL_DEBUG = 0;
    public static final int LEVEL_PRODUCTION = 1;
  
    public String getSource()
    {
        return source;
    }

    public void setSource(String newSource)
    {
        source = newSource;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String newType)
    {
        type = newType;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String newMessage)
    {
        message = newMessage;
    }

    public boolean isRead()
    {
        return read;
    }

    public void setRead(boolean newRead)
    {
        read = newRead;
    }

    public int getLevel()
    {
        return level;
    }

    public void setLevel(int newLevel)
    {
        level = newLevel;
    }

  public String getSubject()
  {
    return subject;
  }

  public void setSubject(String subject)
  {
    this.subject = subject;
  }
}