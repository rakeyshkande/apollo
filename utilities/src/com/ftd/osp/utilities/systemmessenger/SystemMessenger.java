package com.ftd.osp.utilities.systemmessenger;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.exception.SystemMessengerException;

/**
 * This class sends system messages to the database.
 * @author Jeff Penney
 */
public class SystemMessenger 
{
    private static final String GET_GLOBAL_PARAM = "GET_GLOBAL_PARAM";
    private static final String INSERT_MESSAGE = "INSERT_MESSAGE";
    private static final String DELETE_MESSAGE = "DELETE_MESSAGE";
    private static final String STATUS_PARAM = "RegisterOutParameterStatus";
    private static final String MESSAGE_PARAM = "RegisterOutParameterMessage";
    private static final String MESSAGE_ID = "RegisterOutParameterMessageId";
    private static final String METADATA_CONFIG_FILE_NAME = "systemmessenger.xml";
    private static long SYSTEM_STATUS_TIMEOUT = 60000;
    // Modified the max length per QE-32
    private static int MAX_MSG_LENGTH = 1900;
    private static java.util.Date lastUpdateDate = new java.util.Date(0);
    private static String systemStatus;
    
    private static SystemMessenger systemMessenger = new SystemMessenger();
    
    private Logger logger = new Logger(SystemMessenger.class.getName());
    
    private SystemMessenger()
    {
    }

    /**
     * Use this method to get an instance of this singleton object
     * @author Jeff Penney
     */
    public static SystemMessenger getInstance()
    {
        return systemMessenger;
    }

  /**
   * Use this method to send a system message
   * This method takes in a datbase connection and closes it after use.
   * 
   * @param message
   * @param conn
   * @return 
   * @throws org.xml.sax.SAXException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws java.io.IOException
   * @throws java.sql.SQLException
   * @throws com.ftd.osp.utilities.systemmessenger.exception.SystemMessengerException
   */
    public String send(SystemMessengerVO message, Connection conn) throws SAXException, 
           ParserConfigurationException, IOException, SQLException, SystemMessengerException
    {
        String ret = null;

        try
        {
            // Only check the system status every so many minutes

            synchronized(this)
            {
                SYSTEM_STATUS_TIMEOUT = Integer.parseInt(getConfig("statustimeout")) * 6000;
                if(new java.util.Date().getTime() - lastUpdateDate.getTime() > SYSTEM_STATUS_TIMEOUT)
                {
                    DataAccessUtil dau = DataAccessUtil.getInstance();
                    DataRequest dataRequest = new DataRequest();

                    // Get the max order total
                    dataRequest.setConnection(conn);
                    dataRequest.addInputParam("IN_CONTEXT", "SYSTEM_MESSAGE_UTILITY");
                    dataRequest.addInputParam("IN_PARAM", "SYSTEM_STATUS");
                    dataRequest.setStatementID(GET_GLOBAL_PARAM);
                    systemStatus = (String)dau.execute(dataRequest);
                    dataRequest.reset();

                    if(systemStatus == null)
                    {
                        throw new SystemMessengerException("Missing SYSTEM_STATUS in FRP.GLOBAL_PARMS table");
                    }

                    lastUpdateDate = new java.util.Date();
                }
            }


            // Truncate the message at MAX_MSG_LENGTH characters for the database column length
            if(message != null && message.getMessage() != null)
            {
                message.setMessage("("+getLocalHostName()+") "+message.getMessage());
                if(message.getMessage().length() > MAX_MSG_LENGTH)
                {
                    message.setMessage(message.getMessage().substring(0, MAX_MSG_LENGTH));
                }                
            }   

            if(systemStatus.equals("DEBUG"))
            {
                ret = sendMessage(message, conn);
            }
            else
            {
                if(message.getLevel() == SystemMessengerVO.LEVEL_PRODUCTION)
                {
                    ret = sendMessage(message, conn);
                }
            }
        }
        finally
        {
            if( (conn != null) && (! conn.isClosed()))
            {
                conn.close();
            }
        }
        
        return ret;
    }


  /**
   * Use this method to send a system message
   * 
   * @param message
   * @param conn
   * @param closeConnection
   * @return 
   * @throws org.xml.sax.SAXException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws java.io.IOException
   * @throws java.sql.SQLException
   * @throws com.ftd.osp.utilities.systemmessenger.exception.SystemMessengerException
   */
    public String send(SystemMessengerVO message, Connection conn, boolean closeConnection) throws SAXException, 
           ParserConfigurationException, IOException, SQLException, SystemMessengerException
    {
        String ret = null;

        try
        {
            // Only check the system status every so many minutes

            synchronized(this)
            {
                SYSTEM_STATUS_TIMEOUT = Integer.parseInt(getConfig("statustimeout")) * 6000;
                if(new java.util.Date().getTime() - lastUpdateDate.getTime() > SYSTEM_STATUS_TIMEOUT)
                {
                    DataAccessUtil dau = DataAccessUtil.getInstance();
                    DataRequest dataRequest = new DataRequest();

                    // Get the max order total
                    dataRequest.setConnection(conn);
                    dataRequest.addInputParam("IN_CONTEXT", "SYSTEM_MESSAGE_UTILITY");
                    dataRequest.addInputParam("IN_PARAM", "SYSTEM_STATUS");
                    dataRequest.setStatementID(GET_GLOBAL_PARAM);
                    systemStatus = (String)dau.execute(dataRequest);
                    dataRequest.reset();

                    if(systemStatus == null)
                    {
                        throw new SystemMessengerException("Missing SYSTEM_STATUS in FRP.GLOBAL_PARMS table");
                    }

                    lastUpdateDate = new java.util.Date();
                }
            }


            // Truncate the message at MAX_MSG_LENGTH characters for the database column length
            if(message != null && message.getMessage() != null)
            {
                message.setMessage("("+getLocalHostName()+") "+message.getMessage());
                if(message.getMessage().length() > MAX_MSG_LENGTH)
                {
                    message.setMessage(message.getMessage().substring(0, MAX_MSG_LENGTH));
                }                
            }   

            if(systemStatus.equals("DEBUG"))
            {
                ret = sendMessage(message, conn);
            }
            else
            {
                if(message.getLevel() == SystemMessengerVO.LEVEL_PRODUCTION)
                {
                    ret = sendMessage(message, conn);
                }
            }
        }
        finally
        {
            if( (closeConnection) && (conn != null) && (! conn.isClosed()))
            {
                conn.close();
            }
        }
        
        return ret;
    }

  /**
   * 
   * @param message
   * @param conn
   * @return 
   * @throws org.xml.sax.SAXException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws java.io.IOException
   * @throws java.sql.SQLException
   * @throws com.ftd.osp.utilities.systemmessenger.exception.SystemMessengerException
   */
    private String sendMessage(SystemMessengerVO message, Connection conn) throws SAXException, 
           ParserConfigurationException, IOException, SQLException, SystemMessengerException
    {
        String ret = null;

        DataAccessUtil dau = DataAccessUtil.getInstance();
        DataRequest dataRequest = new DataRequest();

        // Get localhost name
        InetAddress address = InetAddress.getLocalHost();    
		    String hostname = address.getHostName();
        if(hostname == null) hostname = "";

        // Get the max order total
        dataRequest.setConnection(conn);
        dataRequest.addInputParam("IN_SOURCE", message.getSource());
        dataRequest.addInputParam("IN_TYPE", message.getType());
        dataRequest.addInputParam("IN_MESSAGE", message.getMessage());
        // JP Puzon 08-29-2005
        // Added the following subject parameter to support added
        // functionality of specifying the email subject line
        // of the alert.
        dataRequest.addInputParam("IN_EMAIL_SUBJECT", message.getSubject());
        //dataRequest.addInputParam("IN_READ", "N");
        dataRequest.addInputParam("IN_COMPUTER", hostname);
        dataRequest.addInputParam(STATUS_PARAM, "");
        dataRequest.addInputParam(MESSAGE_PARAM, "");
        dataRequest.addInputParam(MESSAGE_ID, "");
        dataRequest.setStatementID(INSERT_MESSAGE);
        Map outputs = (Map) dau.execute(dataRequest);
        dataRequest.reset();
      
        String status = (String) outputs.get(STATUS_PARAM);
        String outMsg = null;
        if(status.equals("N"))
        {
            outMsg = (String) outputs.get(MESSAGE_PARAM);
            logger.error("Sending system message failed, " + outMsg);
            ret = null;
        }
        else
        {
            ret = (String) outputs.get(MESSAGE_ID);            
        }
        
        return ret;
    }

   /**
   * Deletes a system message.
   * 
   * @param messageId
   * @param conn
   * @return 
   * @throws org.xml.sax.SAXException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws java.io.IOException
   * @throws java.sql.SQLException
   * @throws com.ftd.osp.utilities.systemmessenger.exception.SystemMessengerException
   */
    public boolean deleteMessage(String messageId, Connection conn) throws SAXException, 
           ParserConfigurationException, IOException, SQLException, SystemMessengerException
    {
        boolean ret = false;

        DataAccessUtil dau = DataAccessUtil.getInstance();
        DataRequest dataRequest = new DataRequest();

        // Get the max order total
        dataRequest.setConnection(conn);
        dataRequest.addInputParam("IN_MESSAGE_ID", messageId);
        dataRequest.addInputParam(STATUS_PARAM, "");
        dataRequest.addInputParam(MESSAGE_PARAM, "");
        dataRequest.setStatementID(DELETE_MESSAGE);
        Map outputs = (Map) dau.execute(dataRequest);
        dataRequest.reset();
      
        String status = (String) outputs.get(STATUS_PARAM);
        String outMsg = null;
        if(status.equals("N"))
        {
            outMsg = (String) outputs.get(MESSAGE_PARAM);
            ret = false;
        }
        else
        {
            ret = true;
        }

        return ret;
    }

  /**
   * 
   * @param name
   * @return 
   */
    private String getConfig(String name)
    {
        String ret = null;

        try
        {
            ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
            ret = configUtil.getPropertyNew(METADATA_CONFIG_FILE_NAME, name);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }        

        return ret;
    }

    /**
     * Get the name of the server that the PI application is running on
     * @return String name of local host or blank if it can't be determined.
     */
    private String getLocalHostName() {
        String retVal = "";
        
        try {
            InetAddress addr = InetAddress.getLocalHost();
            retVal = addr.getHostName();
        } catch (UnknownHostException e) {
            retVal = "Unknown";
        }
        
        return retVal;
    }
}