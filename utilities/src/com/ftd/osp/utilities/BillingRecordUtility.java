package com.ftd.osp.utilities;

import java.sql.Connection;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.ftd.osp.utilities.cacheMgr.CacheUtil;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.MembershipVO;


public class BillingRecordUtility {
	
	
	private static Logger logger = new Logger("com.ftd.osp.utilities.BillingRecordUtility");

    public static final String STMT_INSERT_AP_BILLING_HEARDER = "INSERT_AP_BILLING_HEADER_NO_DUP";
    public static final String STMT_INSERT_AP_BILLING_DETAIL_PREFIX = "INSERT_AP_BILLING_DETAIL_";

    public static final String COMMON_VALUE_YES = "Y";
    public static final String COMMON_VALUE_NO = "N";


    
/**
 * Create a billing detail record in table clean.alt_pay_billing_detail_ua.
 * @param conn
 * @param billingHeaderId
 * @param membershipvo
 * @throws Throwable
 */
public void createBillingRecord(Connection conn, MembershipVO membershipvo) throws Throwable
{
    logger.info("createBillingRecord: paymentId=" +membershipvo.getPaymentId());
    // Record the transaction.
        
    String billingHeaderId = createHeader(conn, membershipvo);
        
    createBillingDetail(conn, billingHeaderId, membershipvo);
     
}


public String createHeader(Connection conn, MembershipVO membershipvo) throws Throwable
{
    Calendar today = Calendar.getInstance();
    Date billingDate = today.getTime();
    //CommonUtil commonUtil = new CommonUtil();
    //int runDateOffset = Integer.parseInt(commonUtil.getGlobalParm(MilesPointsConstants.ACCTG_CONFIG_CONTEXT, "ALT_PAY_EOD_RUNDATE_OFFSET"));
    
    //billingDate = commonUtil.calculateOffSetDate(runDateOffset);
    java.sql.Date sqlBillingDate = 
                new java.sql.Date(billingDate.getTime());
    Map inputParams = new HashMap();
    inputParams.put("IN_PAYMENT_METHOD_ID", membershipvo.getMembershipType());
    inputParams.put("IN_BATCH_DATE", sqlBillingDate);
    inputParams.put("IN_PROCESSED_FLAG", "N");
    inputParams.put("IN_DISPATCHED_COUNT", "0");
    inputParams.put("IN_PROCESSED_COUNT", "0");
    

    Map outputs = executeInsertUpdateStmt(conn, this.STMT_INSERT_AP_BILLING_HEARDER, inputParams);
    String headerId = outputs.get("OUT_BILLING_HEADER_ID").toString();
    logger.info("createHeader returning billingHeaderId:" + headerId);
    return headerId;
}



/**
 * Create a billing detail record in table clean.alt_pay_billing_detail_ua.
 * @param conn
 * @param billingHeaderId
 * @param membershipvo
 * @throws Throwable
 */
public void createBillingDetail(Connection conn, String billingHeaderId, MembershipVO membershipvo) throws Throwable
{
    logger.info("createBillingDetail. paymentId=" +membershipvo.getPaymentId());
    Map inputParams = new HashMap();
    String mileIndicator = membershipvo.getMilesPointsRequested() > 0 ? "D" : "C";
    String resSuccessFlag = membershipvo.isReturnBooleanResult() ? this.COMMON_VALUE_YES : this.COMMON_VALUE_NO;
    String resCode = null;
    
    if(membershipvo.getReturnErrorCode() != null) {
        resCode = membershipvo.getReturnErrorCode() + "-" + membershipvo.getReturnErrorMessage();
        if (resCode.length() > 50) {
            resCode.substring(0, 50);
        }
    }
    
    inputParams.put("IN_BILLING_HEADER_ID", billingHeaderId);
    //Manual entry functionality has been cut per scope reduction.
    inputParams.put("IN_MANUAL_ENTRY_FLAG", this.COMMON_VALUE_NO);
    inputParams.put("IN_PAYMENT_ID", membershipvo.getPaymentId());
    inputParams.put("IN_REQ_ACCOUNT_NUMBER", membershipvo.getMembershipNumber());
    inputParams.put("IN_REQ_MILES_AMT", String.valueOf(membershipvo.getMilesPointsRequested()));
    
    inputParams.put("IN_REQ_BONUS_CODE", CacheUtil.getInstance().getGlobalParm("ACCOUNTING_CONFIG", "UA_BONUS_CODE"));
    inputParams.put("IN_REQ_BONUS_TYPE", CacheUtil.getInstance().getGlobalParm("ACCOUNTING_CONFIG", "UA_BONUS_TYPE"));
    inputParams.put("IN_REQ_MILE_INDICATOR", mileIndicator);
    inputParams.put("IN_RES_SUCCESS_FLAG", resSuccessFlag);
    inputParams.put("IN_RES_CODE", resCode);
    
    this.executeInsertUpdateStmt(conn, 
                                  this.STMT_INSERT_AP_BILLING_DETAIL_PREFIX + membershipvo.getMembershipType(), 
                                  inputParams);

}

protected Map executeInsertUpdateStmt(Connection conn, String stmt, Map inputParams) throws Throwable
{
    logger.info("executeInsertUpdateStmt " + stmt);
    DataRequest request = new DataRequest();
    String status = "";
    Map outputs = null;
    try {
       // build DataRequest object
       request.setConnection(conn);
       request.setInputParams(inputParams);
       request.setStatementID(stmt);

       // get data
       DataAccessUtil dau = DataAccessUtil.getInstance();
       outputs = (Map) dau.execute(request);
       request.reset();
       status = (String) outputs.get("OUT_STATUS");
       if(!this.COMMON_VALUE_YES.equals(status))
       {
           String errorMessage = (String) outputs.get("OUT_MESSAGE");
           logger.error(errorMessage);
           throw new Exception(errorMessage);
       }
       logger.debug("Exiting executeInsertUpdateStmt. Status is:" + status);
    } catch (Exception e) {
        logger.error(e);
        throw e;
    }
    return outputs;
    
}

}
