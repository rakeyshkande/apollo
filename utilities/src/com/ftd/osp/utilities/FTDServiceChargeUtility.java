package com.ftd.osp.utilities;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.ServiceChargeVO;

import java.sql.Connection;
import java.sql.Timestamp;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.HashMap;

public class FTDServiceChargeUtility
{
  
  private static Logger logger = new Logger("com.ftd.osp.utilities.FTDServiceChargeUtility");
  
  //SQL query constants
  private String GET_SERVICE_CHARGE_DATA = "GET_SERVICE_CHARGE_DATA";
  private String IN_ORDER_DATE           = "IN_ORDER_DATE";
  private String IN_SOURCE_CODE          = "IN_SOURCE_CODE";
  private String IN_DELIVERY_DATE        = "IN_DELIVERY_DATE";
  
  //SQL column name constants
  private String SNH_ID                  = "SNH_ID";
  private String FIRST_ORDER_DOMESTIC    = "FIRST_ORDER_DOMESTIC";
  private String FIRST_ORDER_INTERNATIONAL = "FIRST_ORDER_INTERNATIONAL";
  private String VENDOR_CHARGE           = "VENDOR_CHARGE";
  private String VENDOR_SAT_UPCHARGE     = "VENDOR_SAT_UPCHARGE";
  private String SAME_DAY_UPCHARGE     = "SAME_DAY_UPCHARGE";
  private String OVERRIDE_FLAG     = "OVERRIDE_FLAG";
  private String SAME_DAY_UPCHARGE_FS    = "SAME_DAY_UPCHARGE_FS";

  /**
   * getServiceChageData
   * @param orderDate
   * @param sourceCode
   * @param deliveryDate
   * @param conn
   * @return ServiceChargeVO with fees
   */
  public ServiceChargeVO getServiceChargeData(Date orderDate, String sourceCode, Date deliveryDate, Connection conn) 
         throws Exception
  {
    Timestamp orderDateTimestamp = new Timestamp(orderDate.getTime());
    java.sql.Date sqlDeliveryDate = new java.sql.Date(deliveryDate.getTime());

    if (logger.isDebugEnabled())
    {
      SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
      logger.debug("FTDServiceChageUtility called\n" +
        "OrderDate timestamp: " + sdf.format(orderDateTimestamp.getTime()) + "\n" +
        "SourceCode: " + sourceCode + "\n" +
        "DeliveryDate: " + sqlDeliveryDate.toString());
    }
     
    ServiceChargeVO serviceChargeVO = new ServiceChargeVO();
    HashMap inputParams = new HashMap();
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(conn);
    dataRequest.setStatementID(GET_SERVICE_CHARGE_DATA);
    inputParams.put(IN_ORDER_DATE, orderDateTimestamp);
    inputParams.put(IN_SOURCE_CODE, sourceCode);
    inputParams.put(IN_DELIVERY_DATE,  sqlDeliveryDate);
    dataRequest.setInputParams(inputParams);

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    CachedResultSet rs = null;
    rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);
    
    if (rs == null)
    {
      logger.error("Could not obtain charge data from the database.  Query returned no data.");
      throw (new Exception("Could not obtain charge data from the database. Query returned no data."));
    }
    
    rs.next();
    serviceChargeVO.setSnhId(rs.getString(SNH_ID));
    serviceChargeVO.setDomesticCharge(rs.getBigDecimal(FIRST_ORDER_DOMESTIC));
    serviceChargeVO.setInternationalCharge(rs.getBigDecimal(FIRST_ORDER_INTERNATIONAL));
    serviceChargeVO.setVendorCharge(rs.getBigDecimal(VENDOR_CHARGE));
    serviceChargeVO.setVendorSatUpcharge(rs.getBigDecimal(VENDOR_SAT_UPCHARGE));
    serviceChargeVO.setSameDayUpcharge(rs.getBigDecimal(SAME_DAY_UPCHARGE));
    serviceChargeVO.setOverrideFlag(rs.getString(OVERRIDE_FLAG));
    serviceChargeVO.setSameDayUpchargeFS(rs.getBigDecimal(SAME_DAY_UPCHARGE_FS));
    
    //Extra Validations to avoid a null pointer error elsewhere in code that uses this utility
    if (serviceChargeVO.getSnhId() == null || serviceChargeVO.getDomesticCharge() == null || serviceChargeVO.getInternationalCharge() == null
      || serviceChargeVO.getVendorCharge() ==null || serviceChargeVO.getVendorSatUpcharge() == null )
    {
      SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
      String inputParameters =  "OrderDate timestamp: " + sdf.format(orderDateTimestamp.getTime()) + "\n" +
        "SourceCode: " + sourceCode + "\n" +
        "DeliveryDate: " + sqlDeliveryDate.toString();
      if (serviceChargeVO.getSnhId() == null)    
      {
        logger.error("Error in ServiceChargeUility: Null SnhId retrieved from the database. \n" + inputParameters);
        throw (new Exception("Error in ServiceChargeUility: Null SnhId retrieved from the database. \n" + inputParameters));    
      }
      if (serviceChargeVO.getDomesticCharge() == null)
      {
        logger.error("Error in ServiceChargeUility: Null DomesticCharge retrieved from the database\n" + inputParameters);
        throw (new Exception("Error in ServiceChargeUility: Null DomesticCharge retrieved from the database. \n" + inputParameters));
      }
      if (serviceChargeVO.getInternationalCharge() == null)
      {
        logger.error("Error in ServiceChargeUility: Null InternationalCharge retrieved from the database\n" + inputParameters);
        throw (new Exception("Error in ServiceChargeUility: Null InternationalCharge retrieved from the database. \n" + inputParameters));
      }
      if (serviceChargeVO.getVendorCharge() ==null)
      {
        logger.error("Error in ServiceChargeUility: Null VendorCharge retrieved from the database\n" + inputParameters);
        throw (new Exception("Error in ServiceChargeUility: Null VendorCharge retrieved from the database. \n" + inputParameters));
      }
      if (serviceChargeVO.getVendorSatUpcharge() == null)
      {
        logger.error("Error in ServiceChargeUility: Null VendorSatUpcharge retrieved from the database\n" +inputParameters);
        throw (new Exception("Error in ServiceChargeUility: Null VendorSatUpcharge retrieved from the database. \n" + inputParameters));
      }
    }
    
    if (logger.isDebugEnabled())
    {
      logger.debug("FTDServiceChageUtility returned\n" +
        "SnhId: " + serviceChargeVO.getSnhId().toString() + "\n" +
        "DomesticCharge: " +serviceChargeVO.getDomesticCharge().toString() + "\n" +
        "International Charge: " + serviceChargeVO.getInternationalCharge().toString() + "\n" +
        "Vendor Charge: " + serviceChargeVO.getVendorCharge().toString() + "\n" +
        "Vendor Saturday Charge: " + serviceChargeVO.getVendorSatUpcharge().toString()+ "\n" +
        "Same Day Upcharge: " + serviceChargeVO.getSameDayUpcharge().toString()+ "\n" +
        "Override Flag: " + serviceChargeVO.getOverrideFlag()+ "\n" +
        "Same Day Upcharge FS: " + serviceChargeVO.getSameDayUpchargeFS().toString());
    }
    return serviceChargeVO;
  }
}
