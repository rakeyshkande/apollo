package com.ftd.osp.utilities;

import java.io.IOException;
import java.io.StringReader;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.ftd.ftdutilities.FTDCommonUtils;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;

public class StoreFeedUtil {

	private static final String STORE_FEEDS_QUEUE = "OJMS.STORE_FEEDS";
	
	private static Logger logger = new Logger(StoreFeedUtil.class.getName());

	/** Save the store feed for a given feed type.
	 * @param storeFeedXML
	 * @param storeFeedType
	 * @return
	 */
	public String saveStoreFeed(String storeFeedXML, String storeFeedType) {
		if(logger.isDebugEnabled()) {
			logger.debug("StoreFeedUtil.saveStoreFeed(storeFeedXML, storeFeedType) : for feed type" + storeFeedType);
		}	
		try {
			if(storeFeedType == null) {
				throw new Exception("Unable to save Store feed for null feed type");
			}
			return insertStoreFeed(storeFeedXML.trim(), storeFeedType);
		} catch (Exception e) {
			logger.error("Error caught saving store feed : " + e.getMessage());
			sendSystemMessage(e.getMessage());	
		}		
		return null;
	}

	/** Derive the feed type for a given source code
	 * check if the feed is allowed to be persisted to store feeds
	 * insert the feed to store feeds.
	 * @param storeFeedXML
	 * @return
	 */
	public String saveStoreFeed(String storeFeedXML) {	
		if(logger.isDebugEnabled()) {
			logger.debug("StoreFeedUtil.saveStoreFeed(storeFeedXML) : for existing novator feeds");
		}
		try {			
			String feedType = getRootTag(storeFeedXML);			
			if(logger.isDebugEnabled()) {
				logger.debug("FeedType : " + feedType);
			}			
			if (isStoreFeed(feedType)) {				
				return insertStoreFeed(storeFeedXML, feedType);
			} 
		} catch (Exception e) {
			logger.error("Error caught saving store feed : " + e.toString());
			sendSystemMessage(e.getMessage());			
		}
		return null;
	}

	/** return root tag/ parent node representing store feed type.
	 * @param storeFeedXML
	 * @return
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 */
	private String getRootTag(String storeFeedXML) throws ParserConfigurationException, SAXException, IOException {		
		DocumentBuilder builder =  DocumentBuilderFactory.newInstance().newDocumentBuilder();
		Document doc = builder.parse(new InputSource(new StringReader(storeFeedXML)));
		Node node = doc.getDocumentElement();
		return node.getNodeName();
	}

	/**Inserts store feed to Apollo ftd_apps.Store_feeds table.
	 * @param transmitData
	 * @param feedType
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private String insertStoreFeed(String transmitData, String feedType) throws Exception {
		if(logger.isDebugEnabled()) {
			logger.debug("insertStoreFeed : " + feedType);
		}
		String csrId = "SYS";
		BigDecimal feed_id = null;
		Connection conn = null;
		try {			
			DataAccessUtil dau = DataAccessUtil.getInstance();
			DataRequest request = new DataRequest();
			conn = DataSourceUtil.getInstance().getConnection("ORDER SCRUB");
			request.setConnection(conn);
			HashMap inputParams = new HashMap();
			inputParams.put("IN_FEED_XML", transmitData);
			inputParams.put("IN_FEED_STATUS", "NEW"); 
			inputParams.put("IN_FEED_TYPE", feedType);
			inputParams.put("IN_USER_ID", csrId); 
			request.setInputParams(inputParams);
			request.setStatementID("INSERT_STORE_FEED");

			Map outputParameters = (Map) dau.execute(request);
			String status = (String) outputParameters.get("OUT_STATUS");
			feed_id = (BigDecimal) outputParameters.get("OUT_FEED_ID");
			String message = (String) outputParameters.get("OUT_MESSAGE");

			if (status != null && status.equalsIgnoreCase("N")) {
				throw new Exception("Error caught in insertStoreFeed: " + message);
			}
			logger.info("Feed inserted successfully with feed_id : " + feed_id);
		} catch (Exception e) {
			logger.error("exception caught in insertStoreFeed: " + e.toString());
			throw e;
		} 
		finally {
			closeConnection(conn);
		}
		return feed_id.toString();		
	}

	/** Check is the given feed type is valid to save to Store_feeds
	 * @param feedType
	 * @return
	 * @throws Exception 
	 */
	private boolean isStoreFeed(String feedType) throws Exception {		
		
		String allowedFeedTypes = "";
		try {
			ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
			allowedFeedTypes = configUtil.getFrpGlobalParm("PI_CONFIG", "PROCESS_FEEDS");
			if(allowedFeedTypes == null || allowedFeedTypes.equals(""))
			{
				return false;
			}
			StringTokenizer st= new StringTokenizer(allowedFeedTypes,",");
			
			if (feedType != null && feedType.length() > 0) {
			while(st.hasMoreTokens())	
				if(st.nextToken().trim().equals(feedType)) {
						return true;
				}
			}
		} catch (Exception e) {
			logger.error("exception caught in insertStoreFeed: " + e.toString());
			throw e;
		}
		return false;
	}

	/** Convenient method to send a system message 
	 * @param errorMessage
	 * @throws SQLException
	 */
	public static void sendSystemMessage(String errorMessage) {
		if(logger.isDebugEnabled()) {
			logger.debug("sendSystemMessage(..)");
		}
		Connection connection = null;
		try {
			connection = DataSourceUtil.getInstance().getConnection("ORDER SCRUB");
			SystemMessengerVO sysMessage = new SystemMessengerVO();
			sysMessage.setLevel(SystemMessengerVO.LEVEL_PRODUCTION);
			sysMessage.setSource("STORE FEED UTIL");
			sysMessage.setType("ERROR");
			sysMessage.setMessage(errorMessage);
			SystemMessenger.getInstance().send(sysMessage, connection);

		} catch (Throwable t) {
			logger.error("Sending system message failed. Message: " + errorMessage);
			logger.error(t);
		} finally {
			closeConnection(connection);
		}
	}
	
	/** insert store feeds JMS messages.
	 * @param correlationId
	 * @param payLoad
	 */
	public void postStoreFeedMessage(String correlationId, String payLoad) {
		if(logger.isDebugEnabled()) {
			logger.debug("postStoreFeedMessage(..)");
		}
		Connection connection = null;
		try {
			connection = DataSourceUtil.getInstance().getConnection("ORDER SCRUB");
			FTDCommonUtils.insertJMSMessage(connection, payLoad, STORE_FEEDS_QUEUE, correlationId, 0L);
		} catch(Exception e) {
			logger.error("Error caught in postStoreFeedMessage: " + e.getMessage());
			logger.error(e);
		} finally {
			closeConnection(connection);
		}
	}
	
	/** Close connection.
	 * @param connection
	 */
	public static void closeConnection(Connection connection) {
		try {			
			if (connection != null && !connection.isClosed()) {
				connection.close();
			}
		} catch (Exception e) {
			logger.error("Unable to close the connection." + e.getMessage());
		}
	}

}
