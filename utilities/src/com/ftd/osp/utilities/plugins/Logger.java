package com.ftd.osp.utilities.plugins;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.ObjectStreamException;
import java.net.URL;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Properties;
import java.util.Vector;

import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.LogManager;
import org.apache.log4j.Priority;
import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.varia.NullAppender;
import org.apache.log4j.xml.DOMConfigurator;


/**
 * This class acts as a facade to Log4J
 * 
 * @author Anshu Gaind
 * @version 1.0 
 **/
public class Logger  {
	protected String categoryName;
	protected transient org.apache.log4j.Logger logger;  
	private static boolean SUCCESS;
  private String propFile = "log4j.properties";

    private static final String EXTERNAL_CONFIG_LOCATION = "/u02/config/log4j";
    private static final String EXTERNAL_CONFIG_PROPERTY_NAME = "external.config.name";
    
	/**
	 * Logger constructor.
	 * Creates a system dependent file path for Log4J.properties file.
	 * This file should not be bundled in any 'jar' files in the application class path.
	 * It should be set at the root (ear) level in order to create a system dependent file path.
	 * 
	 * @param myCategoryName the category name
   * @throws java.lang.IllegalArgumentException if the passed categoryName is null.
	 */
	public Logger(String myCategoryName)  {
		super();
    init(myCategoryName);
	}
  
  
  /**
   * Initialize the settings
   **/ 
  public synchronized void init(String myCategoryName){
    // Load the properties file once for the entire class
		if (!SUCCESS){
						
			try { 
        Class class_v = this.getClass();
        ClassLoader classLoader = class_v.getClassLoader();
        URL url = classLoader.getResource(propFile);
				System.out.println( "Logger Configuration File URL: " + url.toString() );

				//check to make sure that the file is valid
				//java.io.File f = new java.io.File(url.getFile());
				//if ( (f != null) && (f.isFile()) && (f.exists()) ){
          if (propFile.endsWith("xml"))  {
            //DOMConfigurator.configureAndWatch(systemDependentPath_sb.toString());
            DOMConfigurator.configureAndWatch(url.getFile());
          } else {
            //PropertyConfigurator.configureAndWatch(systemDependentPath_sb.toString());        
            if(isFile(url.getFile())) {
                PropertyConfigurator.configureAndWatch(url.getFile());
            } else {
                configureFromEmbeddedProperties(propFile, url.toString());
            }
                
          }
          SUCCESS = true;
				//} 
        
			} catch (Throwable e){
					System.out.println("**************************");
					System.out.println("ERROR: UNABLE TO LOCATE THE CONFIGURATION FILE: " + propFile);
					System.out.println("ERROR: EXCEPTION : " + e.getMessage());
					System.out.println("**************************");
          e.printStackTrace();
      } finally{
				if (!SUCCESS){
					System.out.println("**************************");
					System.out.println("ERROR: UNABLE TO START LOG4j CONFIGURATION MANAGER");
					System.out.println("USING PROPERTY FILE: "+ propFile);
					System.out.println("**************************");
				}			
			}
		}// end loading properties file for class
		
		if (myCategoryName == null) {
			System.out.println("**************************");
			System.out.println("ERROR: PASSED CATEGORY NAME IS NULL IN LOG MANAGER");
			System.out.println("**************************");
			
			throw new IllegalArgumentException("The category name passed to LogManger cannot be null");
		} else {
			categoryName = myCategoryName;

      // JP Puzon 06-01-2005
      // Changed the line below to return the appropriate *inherited* logger
      // rather than fail out if the *specific* logger does not exist.
      // logger = org.apache.log4j.LogManager.exists(myCategoryName);

	  logger = org.apache.log4j.Logger.getLogger(myCategoryName);
      if (logger == null ){
				throw new IllegalArgumentException("The passed category name, "+myCategoryName+" is not valid");
			}
		}
  }

  /**
   * Checks if the passed in item is a valid file
   * @param file
   * @return
   */
  private boolean isFile(String file)
  {
      File configurationFile = new File(file);
      return (configurationFile.isFile() && configurationFile.exists());
  }  
  
  /**
   * Configures the Repository from the passed in properties file
   * @param propFile
   * @return
   */
  private void configureFromEmbeddedProperties(String propFile, String resourcePath) throws Exception {
      
      stdout("No File Found, Configuring from Resource Path: " + propFile);
      
      InputStream is = getClass().getClassLoader().getResourceAsStream(propFile);
      if(is == null) {
          is = Thread.currentThread().getContextClassLoader().getResourceAsStream(propFile);
      }
      
      if(is == null) {
          throw new Exception("No Resource File for Properties File: " + propFile);
      }
      
      // Load the Properties
      Properties properties = new Properties();
      properties.load(is);
      
      boolean consoleAppenderPreConfigured = false;
      
      // If there is already a console appender in the Root Heirarchy, switch the 
      // Console appender in the config to a NullAppender. The console will still be written to as long
      // as the loggers delegate to the parent appenders as well.
      // This avoids the JBOSS issue with: "ERROR: invalid console appender config detected, console stream is looping"
      // Root Cause is that the JBOSS Log4jService has CatchSystemErr and CatchSystemOut feature which route outputs to the appenders.
      // Having a Console appender causes a loop. The JBOSS Console appender in the jboss config recognize this. 
      // The log4j.properties ones don't.
      if(LogManager.getRootLogger() != null)
      {
          // Remove the root logger's configuration if there is one
          properties.remove("log4j.rootLogger");
          
          @SuppressWarnings("rawtypes")
          Enumeration appenders = LogManager.getRootLogger().getAllAppenders();
          
          // Ensure the ConsoleAppender in the properties file is removed
          while(appenders.hasMoreElements())
          {
              Object nextAppender = appenders.nextElement();
              if(ConsoleAppender.class.isAssignableFrom(nextAppender.getClass()))
              {                  
                  consoleAppenderPreConfigured = true;
                  break;
              }
          }
      }
      
      // Clean up the the Appenders.
      // There is console appender in the system. Or wrong appender file used
      for(Object key: properties.keySet())
      {
          String value = properties.get(key).toString().trim();
          if(consoleAppenderPreConfigured && value.equals(ConsoleAppender.class.getName()))
          {
              stdout("Root Console Appender Exists, Removing Child Console Appender: " + key);
              properties.put(key, NullAppender.class.getName());
          } else if(value.equals("org.apache.log4j.DailyRollingFileAppenderFix"))
          {
              // This appender should not be used.
              stdout("Switching Appender to org.apache.log4j.DailyRollingFileAppender: " + key);
              properties.put(key, "org.apache.log4j.DailyRollingFileAppender");
          }
      }      
      
      
      // Can we externalize the file?
      File log4jFile = externalizeConfiguration(properties, resourcePath);

      if(log4jFile == null)
      {
          PropertyConfigurator.configure(properties);
      } else
      {
          PropertyConfigurator.configureAndWatch(log4jFile.getAbsolutePath());
      }
  }


  /**
   * Creates an External Properties file for the log4j configuration
   * Generally when the app is running in a bundled mode, and we want to be able
   * to update the log4j properties at runtime
   * @param properties
   * @return
   */
  private File externalizeConfiguration(Properties properties, String resourcePath)
  {
      File log4jFolder = new File(EXTERNAL_CONFIG_LOCATION);
      File log4jFile = null;
      
      if(log4jFolder.exists() && log4jFolder.isDirectory())
      {
          try
          {
              String log4jconfFileName = getExternalLogFileName(properties, resourcePath); 
              if(log4jconfFileName == null || log4jconfFileName.trim().length() == 0)
              {
                  stdout("Cannot externalize log4j config file. Specify property in log4j.properties: " + EXTERNAL_CONFIG_PROPERTY_NAME);
                  return null;
              }              
              log4jFile = new File(log4jFolder, log4jconfFileName);                            
              SortedProperties sortedProps = new SortedProperties();
              sortedProps.putAll(properties);
              sortedProps.remove(EXTERNAL_CONFIG_PROPERTY_NAME); // No need to save this
              sortedProps.store(new FileOutputStream(log4jFile), "Externalized Log4j Properties. Do not modify appenders. Only categories");
              stdout("External Log4j File: " + log4jFile.getAbsolutePath());
          }
          catch (Exception e)
          {
              log4jFile = null;
              stdout("Failed to externalize log4j configuration: " + e.getMessage());
              e.printStackTrace();
          }
      } else
      {
          stdout("Cannot externalize Log4j Properties. Please create Folder: " + EXTERNAL_CONFIG_LOCATION);
      }
      
      return log4jFile;
  }
  
  /**
   * Retrieves the External Log File Name. Checks the logging properties for the file name.
   * If not found, attempts to retrieve the current ear/war name from the resource path.
   * 
   * Prepends the JBOSS container name to the target file
   * @param properties
   * @param resourcePath
   * @return 
   */
  private String getExternalLogFileName(Properties properties, String resourcePath)
  {
      // Is there a property in the log4j file?
      String log4jconfFileName = properties.getProperty(EXTERNAL_CONFIG_PROPERTY_NAME);
      
      // No, can we retrieve the ear name from the resource Path?
      if(log4jconfFileName == null || log4jconfFileName.trim().length() == 0)
      {
          log4jconfFileName = getApplicationName(resourcePath);
      }

      if(log4jconfFileName == null || log4jconfFileName.trim().length() == 0)
      {
          return null;
      }
       
      // Append properties to the file name
      if(!log4jconfFileName.endsWith("_log4j.properties"))
      {
          log4jconfFileName = log4jconfFileName + "_log4j.properties";
      }
      
      // If running in JBOSS, prepend the server/container name
      if(System.getProperty("jboss.server.name") != null)
      {
          log4jconfFileName = System.getProperty("jboss.server.name") + "_" + log4jconfFileName;
      }      
      
      return log4jconfFileName;
  }
  
  /**
   * Retrieves the application name from the resource Path
   * @param resourcePath
   * @return
   */
  private static String getApplicationName(String resourcePath)
  {
      resourcePath = resourcePath.toLowerCase();
      int iEndIndex = resourcePath.indexOf(".ear");
      
      if(iEndIndex < 0)
      {
          iEndIndex = resourcePath.indexOf(".war");
      }
      
      if(iEndIndex < 0) 
      {
          return null;
      }
      
      int iStartIndex = resourcePath.lastIndexOf('/', iEndIndex) + 1;
      if(iStartIndex < 0)
      {
          iStartIndex = 0;
      }
      
      String appName = resourcePath.substring(iStartIndex, iEndIndex + 4);
      
      
      return appName;
  }
  
  /** 
   * Used when externalizing the properties file
   */
  private class SortedProperties extends Properties {
      /**
       * Overrides, called by the store method.
       */
      @SuppressWarnings("unchecked")
      public synchronized Enumeration keys() {
         Enumeration keysEnum = super.keys();
         Vector keyList = new Vector();
         while(keysEnum.hasMoreElements()){
           keyList.add(keysEnum.nextElement());
         }
         Collections.sort(keyList);
         return keyList.elements();
      }
  }
  
   /**
   * Prints out the passed in output to STDOUT, prepending LOGGER. Used for debugging the 
   * logger configuration. 
   * @param output
   */
  private void stdout(String output)
  {
      System.out.println("LOGGER: " + output);
      
  }
/******************************************************************************/



/**
   * Log a message object with the DEBUG level
   *
   * @param message the message
   **/
	public void debug(String message) {
		logger.debug(message);
	}

  /**
   * Log a message object with the DEBUG level
   *
   * @param t the exception to log, including its stack trace.
   **/
	public void debug(Throwable t) {
		logger.debug(t.getMessage(), t);
	}

  /**
   * Log a message object with the DEBUG level
   *
   * @param message the message
   * @param t the exception to log, including its stack trace.
   **/
	public void debug(String message, Throwable t) {
		logger.debug(message, t);
	}

/******************************************************************************/

  /**
   * Log a message object with the ERROR level
   *
   * @param message the message
   **/
	public void error(String message) {
		logger.error(message);
	}	

  /**
   * Log a message object with the ERROR level
   *
   * @param t the exception to log, including its stack trace.
   **/
	public void error(Throwable t) {
		logger.error(t.getMessage(), t);
	}

  /**
   * Log a message object with the ERROR level
   *
   * @param message the message
   * @param t the exception to log, including its stack trace.
   **/
	public void error(String message, Throwable t) {
		logger.error(message, t);
	}


/******************************************************************************/

  /**
   * Log a message object with the FATAL level
   *
   * @param message the message
   **/
	public void fatal(String message) {
		logger.fatal(message);
	}

  /**
   * Log a message object with the FATAL level
   *
   * @param t the exception to log, including its stack trace.
   **/
	public void fatal(Throwable t) {
		logger.fatal(t.getMessage(), t);
	}
  /**
   * Log a message object with the FATAL level
   *
   * @param message the message
   * @param t the exception to log, including its stack trace.
   **/
	public void fatal(String message, Throwable t) {
		logger.fatal(message, t);
	}

/******************************************************************************/
  /**
   * Log a message object with the INFO level
   *
   * @param message the message
   **/
	public void info(String message) {
		logger.info(message);
	}

  /**
   * Log a message object with the INFO level
   *
   * @param t the exception to log, including its stack trace.
   **/
	public void info(Throwable t) {
		logger.warn(t.getMessage(), t);
	}
  /**
   * Log a message object with the INFO level
   *
   * @param message the message
   * @param t the exception to log, including its stack trace.
   **/
	public void info(String message, Throwable t) {
		logger.info(message, t);
	}

/******************************************************************************/

  /**
   * Log a message object with the WARN level
   *
   * @param message the message
   **/
	public void warn(String message) {
		logger.warn(message);
	}

  /**
   * Log a message object with the WARN level
   *
   * @param t the exception to log, including its stack trace.
   **/
	public void warn(Throwable t) {
		logger.warn(t.getMessage(), t);
	}

  /**
   * Log a message object with the WARN level
   *
   * @param message the message
   * @param t the exception to log, including its stack trace.
   **/
	public void warn(String message, Throwable t) {
		logger.warn(message, t);
	}
/******************************************************************************/
	
	/**
	 *	Indicates whether debug messages are turned on for the category
	 *  of this class.
	 * 
	 *  @return boolean true if debug messages are enabled
	 */
	public boolean isDebugEnabled(){
		boolean test = logger.isDebugEnabled();
		return test;
	}

	/**
	 *	Indicates whether info messages are turned on for the category
	 *  of this class.
	 * 
	 *  @return boolean true if info messages are enabled
	 */
	public boolean isInfoEnabled() {
		boolean test = logger.isInfoEnabled();
		return test;
	}
	
	/**
	 *	Indicates whether warn messages are turned on for the category
	 *  of this class.
	 * 
	 *  @return boolean true if warn messages are enabled
	 */
	public boolean isWarnEnabled() {
		return logger.isEnabledFor(Priority.WARN);
	}
	
	/**
	 *	Indicates whether error messages are turned on for the category
	 *  of this class.
	 * 
	 *  @return boolean true if error messages are enabled
	 */
	public boolean isErrorEnabled(){
		return logger.isEnabledFor(Priority.ERROR);
	}
	
	/**
	 *	Indicates whether fatal messages are turned on for the category
	 *  of this class.
	 * 
	 *  @return boolean true if fatal messages are enabled
	 */
	public boolean isFatalEnabled() {
		return logger.isEnabledFor(Priority.FATAL);
	}

	
	/**
	 * This method restores the state of the Logger after serialization.
	 * It takes the categoryName that is part of the serialized Logger
	 * and uses it to create a new Logger
	 * 
	 * @return java.lang.Object the new Log Manager
	 * @throws ObjectStreamException if there is a problem with the deserialization
	 */
	private Object readResolve() throws java.io.ObjectStreamException  {
		String myCategoryName = categoryName;
        Logger logger = null;
        logger = new Logger(myCategoryName);	
        return logger;
	}


      /**
         * Returns logger category name
         */
      public String getLoggerName() {
          return logger.getName();
      }
}
