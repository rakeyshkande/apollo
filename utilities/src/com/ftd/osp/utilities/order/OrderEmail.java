package com.ftd.osp.utilities.order;

public class OrderEmail
{
  private String subject = new String();
  private String sender = new String();
  private String recipient = new String();
  private String content = new String();

  public OrderEmail(){}

  public void setSubject(String subject)
  {
    this.subject = subject;
  }

  public String getSubject()
  {
    return this.subject;
  }

  public void setRecipient(String recipient)
  {
    this.recipient = recipient;
  }

  public String getRecipient()
  {
    return this.recipient;
  }

  public void setSender(String sender)
  {
    this.sender = sender;
  }

  public String getSender()
  {
    return this.sender;
  }

  public String getContent()
  {
    return this.content;
  }

  public void prependContent(String section)
  {
    this.content = (section + content);
  }

  public void appendContent(String section)
  {
    //System.out.println(section);
    this.content = (this.content + section);
  }

  public String getHTMLContent()
  {
    StringBuffer buf = new StringBuffer();
    buf.append("<html><head><title>Order Confirmation</title></head><body><PRE><FONT FACE=\"COURIER\">");
    buf.append(this.getContent());
    buf.append("</PRE></body></html>");    

    return buf.toString();
  }  
}