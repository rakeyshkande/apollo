package com.ftd.osp.utilities.order;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.ftd.ftdutilities.CalculateTaxUtil;
import com.ftd.ftdutilities.FTDCommonUtils;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.handlers.PaymentMethodHandler;
import com.ftd.osp.utilities.order.vo.AddOnsVO;
import com.ftd.osp.utilities.order.vo.BuyerAddressesVO;
import com.ftd.osp.utilities.order.vo.BuyerCommentsVO;
import com.ftd.osp.utilities.order.vo.BuyerEmailsVO;
import com.ftd.osp.utilities.order.vo.BuyerPhonesVO;
import com.ftd.osp.utilities.order.vo.BuyerVO;
import com.ftd.osp.utilities.order.vo.CoBrandVO;
import com.ftd.osp.utilities.order.vo.CreditCardsVO;
import com.ftd.osp.utilities.order.vo.DispositionsVO;
import com.ftd.osp.utilities.order.vo.FraudCommentsVO;
import com.ftd.osp.utilities.order.vo.MembershipsVO;
import com.ftd.osp.utilities.order.vo.OrderContactInfoVO;
import com.ftd.osp.utilities.order.vo.OrderDetailExtensionsVO;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.order.vo.PaymentsVO;
import com.ftd.osp.utilities.order.vo.RecipientAddressesVO;
import com.ftd.osp.utilities.order.vo.RecipientPhonesVO;
import com.ftd.osp.utilities.order.vo.RecipientsVO;
import com.ftd.osp.utilities.order.vo.TaxDisplayOrder;
import com.ftd.osp.utilities.order.vo.TaxVO;
import com.ftd.osp.utilities.xml.DOMUtil;


public class OrderXAO
{
	boolean canadianRecipient = false;
	
    public OrderXAO()
    {
    }

    public Document generateOrderXML(OrderVO order) throws ParserConfigurationException,  IOException, SAXException
    {
        Document orderDocument = DOMUtil.getDefaultDocument();
        Element tagElement = null;
      
        // Create order root
        Element orderElement = orderDocument.createElement("order");
        orderDocument.appendChild(orderElement);

        // Add order header to the document
        orderElement.appendChild(orderDocument.importNode(generateHeaderXML(order), true));
       

        // Add items to the document
        Element itemsElement = orderDocument.createElement("items");
        orderElement.appendChild(itemsElement);
            
        if(order.getOrderDetail() != null && order.getOrderDetail().size() > 0)
        {
            OrderDetailsVO item = null;
            Iterator itemIterator = order.getOrderDetail().iterator();
                
            while(itemIterator.hasNext())
            {
                item = (OrderDetailsVO) itemIterator.next();
                itemsElement.appendChild(orderDocument.importNode(generateItemXML(item, order.getCompanyId()), true));
            }
        }
        
    	Element surchargeElement = orderDocument.createElement("surcharge");
    	orderElement.appendChild(surchargeElement);
    	surchargeElement.appendChild(this.createTextNode("company_id", order.getCompanyId(), orderDocument, tagElement));
    	surchargeElement.appendChild(this.createTextNode("canadian_recipient", new Boolean(canadianRecipient).toString(), orderDocument, tagElement));
        if (order.getCompanyId() != null && !order.getCompanyId().equalsIgnoreCase("FTDCA") && canadianRecipient) {
        	surchargeElement.appendChild(this.createTextNode("show_explanation", "yes", orderDocument, tagElement));
        }


            
        return orderDocument;
    }

    
    private Element generateHeaderXML(OrderVO order) throws ParserConfigurationException
    {
        BuyerVO buyer = null;
        BuyerAddressesVO buyerAddress = null;
        BuyerPhonesVO buyerPhone = null;
        BuyerEmailsVO buyerEmail = null;
        BuyerCommentsVO buyerComment = null;
        PaymentsVO payment = null;
        CreditCardsVO creditCard = null;
        
        Iterator buyerIterator = null;
        Iterator buyerAddressIterator = null;
        Iterator buyerPhoneIterator = null;
        Iterator buyerEmailIterator = null;
        Iterator buyerCommentIterator = null;
        Iterator paymentIterator = null;
        Iterator creditCardIterator = null;
    
        Element tagElement = null;
        Document headerDocument = DOMUtil.getDefaultDocument();
        Element headerElement = headerDocument.createElement("header");

        headerElement.appendChild(this.createTextNode("master_order_number", order.getMasterOrderNumber(), headerDocument, tagElement));
        headerElement.appendChild(this.createTextNode("order_guid", order.getGUID(), headerDocument, tagElement));
        headerElement.appendChild(this.createTextNode("order_date", order.getOrderDate(), headerDocument, tagElement));
        headerElement.appendChild(this.createTextNode("status", order.getStatus(), headerDocument, tagElement));
        headerElement.appendChild(this.createTextNode("source_code", order.getSourceCode(), headerDocument, tagElement));
        headerElement.appendChild(this.createTextNode("source_code_description", order.getSourceDescription(), headerDocument, tagElement));
        headerElement.appendChild(this.createTextNode("order_amount", order.getOrderTotal(), headerDocument, tagElement));
        headerElement.appendChild(this.createTextNode("mp_redemption_rate_amt", order.getMpRedemptionRateAmt(), headerDocument, tagElement));
        headerElement.appendChild(this.createTextNode("partner_id", order.getPartnerId(), headerDocument, tagElement));
        headerElement.appendChild(this.createTextNode("company_id", order.getCompanyId(), headerDocument, tagElement));
        headerElement.appendChild(this.createTextNode("company_name", order.getCompanyName(), headerDocument, tagElement));
        headerElement.appendChild(this.createTextNode("origin", order.getOrderOrigin(), headerDocument, tagElement));
        headerElement.appendChild(this.createTextNode("yellow_pages_code", order.getYellowPagesCode(), headerDocument, tagElement));
        headerElement.appendChild(this.createTextNode("csr_id", order.getCsrId(), headerDocument, tagElement));
        headerElement.appendChild(this.createTextNode("cobrand_cc_code", order.getCoBrandCreditCardCode(), headerDocument, tagElement));
        headerElement.appendChild(this.createTextNode("fraud_flag", order.getFraudFlag(), headerDocument, tagElement));
        headerElement.appendChild(this.createTextNode("dnis_code", order.getDnisCode(), headerDocument, tagElement));
        headerElement.appendChild(this.createTextNode("loss_prevention_indicator", order.getLossPreventionIndicator(), headerDocument, tagElement));
        
        
        // Ariba
        headerElement.appendChild(this.createTextNode("ariba_payload", order.getAribaPayload(), headerDocument, tagElement));
        headerElement.appendChild(this.createTextNode("ariba_buyer_cookie", order.getAribaBuyerCookie(), headerDocument, tagElement));
        headerElement.appendChild(this.createTextNode("ariba_asn_buyer_number", order.getAribaBuyerAsnNumber(), headerDocument, tagElement));

        // Bulk load
        if(order.getOrderContactInfo() != null && order.getOrderContactInfo().size() > 0)
        {
            OrderContactInfoVO contactInfo = (OrderContactInfoVO) order.getOrderContactInfo().get(0);
            headerElement.appendChild(this.createTextNode("contact_first_name", contactInfo.getFirstName(), headerDocument, tagElement));
            headerElement.appendChild(this.createTextNode("contact_last_name", contactInfo.getLastName(), headerDocument, tagElement));
            headerElement.appendChild(this.createTextNode("contact_phone", contactInfo.getPhone(), headerDocument, tagElement));
            headerElement.appendChild(this.createTextNode("contact_phone_ext", contactInfo.getExt(), headerDocument, tagElement));
            headerElement.appendChild(this.createTextNode("contact_email", contactInfo.getEmail(), headerDocument, tagElement));
        }

        // Membership
        if(order.getMemberships() != null && order.getMemberships().size() > 0)
        {
            MembershipsVO membership = (MembershipsVO) order.getMemberships().get(0);
            headerElement.appendChild(this.createTextNode("membership_id", membership.getMembershipIdNumber(), headerDocument, tagElement));
            headerElement.appendChild(this.createTextNode("membership_type", membership.getMembershipType(), headerDocument, tagElement));
            headerElement.appendChild(this.createTextNode("membership_first_name", membership.getFirstName(), headerDocument, tagElement));
            headerElement.appendChild(this.createTextNode("membership_last_name", membership.getLastName(), headerDocument, tagElement));
        }

        // Fraud codes
        Element fraudCodesElement = headerDocument.createElement("fraud_codes");
        headerElement.appendChild(fraudCodesElement);
        
        if(order.getFraudCodes() != null && order.getFraudCodes().size() > 0)
        {
            String fraudCode = null;
            Element fraudCodeElement = null;
            Iterator fraudCodeIterator = order.getFraudCodes().iterator();
        
            while(fraudCodeIterator.hasNext())
            {
                fraudCode = (String) fraudCodeIterator.next();
                fraudCodeElement = headerDocument.createElement("fraud_code");
                fraudCodesElement.appendChild(fraudCodeElement);

                fraudCodeElement.appendChild(this.createTextNode("fraud_code_id", fraudCode, headerDocument, tagElement)); 
            }
        }

        // Fraud comments
        Element fraudCommentsElement = headerDocument.createElement("fraud_comments");
        headerElement.appendChild(fraudCommentsElement);
        
        if(order.getFraudComments() != null && order.getFraudComments().size() > 0)
        {
            FraudCommentsVO fraudComments = null;
            Element fraudCommentElement = null;
            Iterator fraudCommentIterator = order.getFraudComments().iterator();
        
            while(fraudCommentIterator.hasNext())
            {
                fraudComments = (FraudCommentsVO) fraudCommentIterator.next();
                fraudCommentElement = headerDocument.createElement("fraud_comment");
                fraudCommentsElement.appendChild(fraudCommentElement);

                fraudCommentElement.appendChild(this.createTextNode("fraud_id", fraudComments.getFraudID(), headerDocument, tagElement)); 
                fraudCommentElement.appendChild(this.createTextNode("fraud_comment_id", String.valueOf(fraudComments.getFraudCommentID()), headerDocument, tagElement)); 
                fraudCommentElement.appendChild(this.createTextNode("fraud_description", fraudComments.getFraudDescription(), headerDocument, tagElement)); 
                fraudCommentElement.appendChild(this.createTextNode("fraud_comments", fraudComments.getCommentText(), headerDocument, tagElement)); 
            }
        }
    
        // Buyer
        if(order.getBuyer() != null && order.getBuyer().size() > 0)
        {
            buyer = (BuyerVO) order.getBuyer().get(0);

            headerElement.appendChild(this.createTextNode("buyer_id", Long.toString(buyer.getBuyerId()), headerDocument, tagElement));

            String cleanBuyerId = "";
            if(buyer.getCleanCustomerId() != null)
            {
                cleanBuyerId = buyer.getCleanCustomerId().toString();
            }
            headerElement.appendChild(this.createTextNode("buyer_clean_id", cleanBuyerId, headerDocument, tagElement));

            headerElement.appendChild(this.createTextNode("buyer_locked_by", buyer.getLockedBy(), headerDocument, tagElement));
            headerElement.appendChild(this.createTextNode("buyer_customer_id", buyer.getCustomerId(), headerDocument, tagElement));
            headerElement.appendChild(this.createTextNode("buyer_first_name", buyer.getFirstName(), headerDocument, tagElement));
            headerElement.appendChild(this.createTextNode("buyer_last_name", buyer.getLastName(), headerDocument, tagElement));

            // Buyer address
            if(buyer.getBuyerAddresses() != null && buyer.getBuyerAddresses().size() > 0)
            {
                buyerAddress = (BuyerAddressesVO) buyer.getBuyerAddresses().get(0);

                headerElement.appendChild(this.createTextNode("buyer_address1", buyerAddress.getAddressLine1(), headerDocument, tagElement));
                headerElement.appendChild(this.createTextNode("buyer_address2", buyerAddress.getAddressLine2(), headerDocument, tagElement));
                headerElement.appendChild(this.createTextNode("buyer_city", buyerAddress.getCity(), headerDocument, tagElement));
                headerElement.appendChild(this.createTextNode("buyer_state", buyerAddress.getStateProv(), headerDocument, tagElement));

                // Format the postal code for US
                String postalCode = null;
                if(buyerAddress.getCountry() != null && buyerAddress.getCountry().equalsIgnoreCase("US"))
                {
                    postalCode = buyerAddress.getPostalCode();
                    if(postalCode == null) postalCode = "";

                    if(postalCode.length() == 9)
                    {
                          postalCode =  postalCode.substring(0, 5) + "-" + postalCode.substring(5);
                    }
                }
                else
                {
                    postalCode = buyerAddress.getPostalCode();
                }
                headerElement.appendChild(this.createTextNode("buyer_postal_code", postalCode, headerDocument, tagElement));
                headerElement.appendChild(this.createTextNode("buyer_country", buyerAddress.getCountry(), headerDocument, tagElement));
                headerElement.appendChild(this.createTextNode("buyer_address_etc", buyerAddress.getAddressEtc(), headerDocument, tagElement));
            }

            // Buyer phone numbers
            if(buyer.getBuyerPhones() != null)
            {
                buyerPhoneIterator = buyer.getBuyerPhones().iterator();
                while(buyerPhoneIterator.hasNext())
                {
                    buyerPhone = (BuyerPhonesVO) buyerPhoneIterator.next();
                
                    if(buyerPhone.getPhoneType() != null && buyerPhone.getPhoneType().equals("HOME"))
                    {
                        headerElement.appendChild(this.createTextNode("buyer_evening_phone", buyerPhone.getPhoneNumber(), headerDocument, tagElement));
                        headerElement.appendChild(this.createTextNode("buyer_evening_ext", buyerPhone.getExtension(), headerDocument, tagElement));
                    }
                    else if(buyerPhone.getPhoneType() != null && buyerPhone.getPhoneType().equals("WORK"))
                    {
                        headerElement.appendChild(this.createTextNode("buyer_daytime_phone", buyerPhone.getPhoneNumber(), headerDocument, tagElement));
                        headerElement.appendChild(this.createTextNode("buyer_daytime_ext", buyerPhone.getExtension(), headerDocument, tagElement));
                    }
                    else if(buyerPhone.getPhoneType() != null && buyerPhone.getPhoneType().equals("FAX"))
                    {
                        headerElement.appendChild(this.createTextNode("buyer_fax", buyerPhone.getPhoneNumber(), headerDocument, tagElement));
                    }
                }
            }

            // Add buyer phone numbers to xml
            if(buyer.getBuyerEmails() != null)
            {
                buyerEmailIterator = buyer.getBuyerEmails().iterator();
                while(buyerEmailIterator.hasNext())
                {
                    buyerEmail = (BuyerEmailsVO) buyerEmailIterator.next();
                    if(buyerEmail.getPrimary() != null && buyerEmail.getPrimary().equals("Y"))
                    {
                        headerElement.appendChild(this.createTextNode("buyer_email_address", buyerEmail.getEmail(), headerDocument, tagElement));
                        headerElement.appendChild(this.createTextNode("news_letter_flag", buyerEmail.getNewsletter(), headerDocument, tagElement));
                    }
                }
            }
        }

        // Payments
        if(order.getPayments() != null)
        {
            // List of payment methods.  Used to determine payment method text for a stock message.
            ArrayList paymentMethods = new ArrayList();
            Element giftCertificatesElement = headerDocument.createElement("gift_certificates");
            headerElement.appendChild(giftCertificatesElement);
            Element giftCertificateElement = null;
        
            paymentIterator = order.getPayments().iterator();
            String paymentMethodAll = "";
            while(paymentIterator.hasNext())
            {
                payment = (PaymentsVO) paymentIterator.next();
                // Null or empty payment method types default to credit card
                if(payment.getPaymentMethodType() == null || payment.getPaymentMethodType().equals(""))
                {
                    payment.setPaymentMethodType("C");
                }
                
                // Store payment method
                paymentMethods.add(payment.getPaymentType());

                if(payment.getPaymentMethodType().equals("C"))
                {
                	paymentMethodAll = paymentMethodAll + "C";
                    if(payment.getCreditCards() != null && payment.getCreditCards().size() > 0)
                    {
                        creditCard = (CreditCardsVO) payment.getCreditCards().get(0);
                            
                        headerElement.appendChild(this.createTextNode("payment_method", payment.getPaymentMethodType(), headerDocument, tagElement));
                        headerElement.appendChild(this.createTextNode("payment_method_id", creditCard.getCCType(), headerDocument, tagElement));
                        headerElement.appendChild(this.createTextNode("cc_number", creditCard.getCCNumber(), headerDocument, tagElement));
                        headerElement.appendChild(this.createTextNode("cc_exp_date", creditCard.getCCExpiration(), headerDocument, tagElement));

                        if(this.validateCreditCardDate(creditCard.getCCExpiration()))
                        {
                            headerElement.appendChild(this.createTextNode("cc_exp_month", creditCard.getCCExpiration().substring(0, 2), headerDocument, tagElement));
                            headerElement.appendChild(this.createTextNode("cc_exp_year", "20" + creditCard.getCCExpiration().substring(3, 5), headerDocument, tagElement));
                        }
                        else
                        {
                            headerElement.appendChild(this.createTextNode("cc_exp_month", creditCard.getCCExpiration(), headerDocument, tagElement));
                        }
                                
                        headerElement.appendChild(this.createTextNode("cc_approval_code", payment.getAuthNumber(), headerDocument, tagElement));
                        headerElement.appendChild(this.createTextNode("cc_approval_amt", payment.getAmount(), headerDocument, tagElement));
                        headerElement.appendChild(this.createTextNode("cc_approval_verbage", payment.getAuthResult(), headerDocument, tagElement));
                        headerElement.appendChild(this.createTextNode("cc_approval_action_code", null, headerDocument, tagElement));
                        headerElement.appendChild(this.createTextNode("cc_avs_result", payment.getAvsCode(), headerDocument, tagElement));
                        headerElement.appendChild(this.createTextNode("cc_acq_data", payment.getAcqReferenceNumber(), headerDocument, tagElement));
                        headerElement.appendChild(this.createTextNode("cc_aafes_ticket", payment.getAafesTicket(), headerDocument, tagElement));
                        headerElement.appendChild(this.createTextNode("no_charge_username", payment.getNcApprovalId(), headerDocument, tagElement));
                        
                        headerElement.appendChild(this.createTextNode("csc_response_code", payment.getCscResponseCode(), headerDocument, tagElement));
                        headerElement.appendChild(this.createTextNode("csc_validated_flag", payment.getCscValidatedFlag(), headerDocument, tagElement));
                        headerElement.appendChild(this.createTextNode("csc_failure_cnt", String.valueOf(payment.getCscFailureCount()), headerDocument, tagElement));
                    }
                }
                else if(payment.getPaymentMethodType() != null && payment.getPaymentMethodType().equals("G"))
                {
                	//If the payment 
                	paymentMethodAll = paymentMethodAll + "G";
                    giftCertificateElement = headerDocument.createElement("gift_certificate");
                    giftCertificatesElement.appendChild(giftCertificateElement);

                    giftCertificateElement.appendChild(this.createTextNode("gc_number", payment.getGiftCertificateId(), headerDocument, tagElement));
                    giftCertificateElement.appendChild(this.createTextNode("gc_amount", payment.getAmount(), headerDocument, tagElement));   
                }
                else if(payment.getPaymentMethodType() != null && payment.getPaymentMethodType().equals("R"))
                {
                	paymentMethodAll = paymentMethodAll + "R";
                    giftCertificateElement = headerDocument.createElement("gift_certificate");
                    giftCertificatesElement.appendChild(giftCertificateElement);

                    giftCertificateElement.appendChild(this.createTextNode("gc_number", payment.getGiftCertificateId(), headerDocument, tagElement));
                    giftCertificateElement.appendChild(this.createTextNode("gc_amount", payment.getAmount(), headerDocument, tagElement));   
                }
                
                else if(payment.getPaymentMethodType() != null && payment.getPaymentMethodType().equals("I"))
                {
                	paymentMethodAll = paymentMethodAll + "I";
                    headerElement.appendChild(this.createTextNode("payment_method_id", payment.getPaymentType(), headerDocument, tagElement));
                    headerElement.appendChild(this.createTextNode("payment_method", payment.getPaymentMethodType(), headerDocument, tagElement));      
                }
                else if(payment.getPaymentMethodType() != null && payment.getPaymentMethodType().equals("P"))
                {
                	paymentMethodAll = paymentMethodAll + "P";
                    headerElement.appendChild(this.createTextNode("payment_method_id", payment.getPaymentType(), headerDocument, tagElement));
                    headerElement.appendChild(this.createTextNode("payment_method", payment.getPaymentMethodType(), headerDocument, tagElement));      
                }
            }
            
            headerElement.appendChild(this.createTextNode("payment_method_all", paymentMethodAll, headerDocument, tagElement));
            
            // Create payment_method_text node based on the list of payment methods
            String paymentMethodTxt = ((PaymentMethodHandler)CacheManager.getInstance().getHandler("CACHE_NAME_PAYMENT_METHOD")).getText(paymentMethods, null);
            headerElement.appendChild(this.createTextNode("payment_method_text",
              paymentMethodTxt, headerDocument, tagElement));
        }
        
        // Co-Brands
        Element coBrandsElement = headerDocument.createElement("co_brands");
        headerElement.appendChild(coBrandsElement);
        
        if(order.getCoBrand() != null)
        {
            Iterator coBrandIterator = order.getCoBrand().iterator();
            Element coBrandElement = null;
            CoBrandVO coBrand = null;
            
            while(coBrandIterator.hasNext())
            {
                coBrand = (CoBrandVO) coBrandIterator.next();
                coBrandElement = headerDocument.createElement("co_brand");
                coBrandsElement.appendChild(coBrandElement);

                coBrandElement.appendChild(this.createTextNode("cb_name", coBrand.getInfoName(), headerDocument, tagElement));
                coBrandElement.appendChild(this.createTextNode("cb_data", coBrand.getInfoData(), headerDocument, tagElement));   
            }
        }

        // Preferred Partners (e.g., USAA)
        Element preferredPartnersElement = headerDocument.createElement("preferred_partners");
        headerElement.appendChild(preferredPartnersElement);
        HashSet<String> pp = order.getPreferredProcessingPartners();
        if (pp != null) {
            Iterator ppit = pp.iterator();
            while(ppit.hasNext()) {
              String ppName = (String)ppit.next();
              preferredPartnersElement.appendChild(this.createTextNode("preferred_partner", ppName, headerDocument, tagElement));
            }
        }

        // Preferred Partners (e.g., USAA)
        Element preferredPartnerNamesElement = headerDocument.createElement("preferred_partner_names");
        headerElement.appendChild(preferredPartnerNamesElement);
        HashSet<String> ppn = order.getPreferredProcessingPartnerNames();
        if (ppn != null) {
            Iterator ppit = ppn.iterator();
            while(ppit.hasNext()) {
              String ppName = (String)ppit.next();
              preferredPartnerNamesElement.appendChild(this.createTextNode("preferred_partner", ppName, headerDocument, tagElement));
            }
        }
        
        // Premier Circle 
        Element premierCircleElement = headerDocument.createElement("premier_circle_details");
        headerElement.appendChild(premierCircleElement);
        List<OrderDetailsVO> items = new ArrayList<OrderDetailsVO>();
        String isPcFlag = "N";
        items = order.getOrderDetail();
        for(OrderDetailsVO orderDetailsVO : items) {
        	if(orderDetailsVO.getPcFlag() != null && orderDetailsVO.getPcFlag().equals("Y")) {
        		isPcFlag = "Y";
        		break;
        	}
        }
        premierCircleElement.appendChild(this.createTextNode("pc_flag", isPcFlag, headerDocument, tagElement));
        
        headerElement.appendChild(this.createTextNode("free_shipping_use_flag", order.getFreeShippingUseFlag(), headerDocument, tagElement));
        headerElement.appendChild(this.createTextNode("free_shipping_purchase_flag", order.getFreeShippingPurchaseFlag(), headerDocument, tagElement));
        headerElement.appendChild(this.createTextNode("mrcnt_channel_icon", order.getMrcntChannelIcon(), headerDocument, tagElement));
        headerElement.appendChild(this.createTextNode("is_mercent_order", order.getIsMercentOrder(), headerDocument, tagElement));
        headerElement.appendChild(this.createTextNode("mrcnt_channel_name", order.getMrcntChannelName(), headerDocument, tagElement));
        
        // If partner order, set the partner image URL and partner name.
       	headerElement.appendChild(this.createTextNode("is_partner_order", order.isPartnerOrder() ? "Y" : "N", headerDocument, tagElement));
        headerElement.appendChild(this.createTextNode("partner_image_url", order.getPartnerImgURL(), headerDocument, tagElement));
        headerElement.appendChild(this.createTextNode("partner_name", order.getPartnerName(), headerDocument, tagElement));
        headerElement.appendChild(this.createTextNode("is_joint_cart", order.getJointCartIndicator(), headerDocument, tagElement));
       
        return headerElement;
    }

    private Element generateItemXML(OrderDetailsVO item, String companyId) throws ParserConfigurationException, IOException, SAXException
    {
        Element tagElement = null;
        Document itemDocument = DOMUtil.getDefaultDocument();
        Element itemElement = itemDocument.createElement("item");

        RecipientsVO recipient = null;
        RecipientAddressesVO recipientAddress = null;
        RecipientPhonesVO recipientPhone = null;
        
        Iterator recipientIterator = null;       
        Iterator recipientAddressIterator = null;  
        Iterator recipientPhoneIterator = null;  
        Iterator destinationIterator = null; 

        itemElement.appendChild(this.createTextNode("order_number", item.getExternalOrderNumber(), itemDocument, tagElement));
        itemElement.appendChild(this.createTextNode("order_detail_id", String.valueOf(item.getOrderDetailId()), itemDocument, tagElement));
        itemElement.appendChild(this.createTextNode("line_number", item.getLineNumber(), itemDocument, tagElement));
        itemElement.appendChild(this.createTextNode("item_status", item.getStatus(), itemDocument, tagElement));
        itemElement.appendChild(this.createTextNode("order_total", item.getExternalOrderTotal(), itemDocument, tagElement));
        itemElement.appendChild(this.createTextNode("tax_amount", item.getTaxAmount(), itemDocument, tagElement));
        itemElement.appendChild(this.createTextNode("service_fee", item.getServiceFeeAmount(), itemDocument, tagElement));
        itemElement.appendChild(this.createTextNode("extra_shipping_fee", null, itemDocument, tagElement));
        itemElement.appendChild(this.createTextNode("drop_ship_charges", item.getShippingFeeAmount(), itemDocument, tagElement));
        itemElement.appendChild(this.createTextNode("retail_variable_price", null, itemDocument, tagElement));
        itemElement.appendChild(this.createTextNode("productid", item.getProductId(), itemDocument, tagElement));
        itemElement.appendChild(this.createTextNode("subcodeid", item.getProductSubCodeId(), itemDocument, tagElement));
        itemElement.appendChild(this.createTextNode("subcode_description", item.getProductSubcodeDescription(), itemDocument, tagElement));
        itemElement.appendChild(this.createTextNode("product_description", item.getItemDescription(), itemDocument, tagElement));
        itemElement.appendChild(this.createTextNode("product_price", item.getProductsAmount(), itemDocument, tagElement));
        itemElement.appendChild(this.createTextNode("discounted_product_price", item.getDiscountedProductPrice(), itemDocument, tagElement));
        itemElement.appendChild(this.createTextNode("first_color_choice", item.getColorOneDescription(), itemDocument, tagElement));
        itemElement.appendChild(this.createTextNode("second_color_choice", item.getColorTwoDescription(), itemDocument, tagElement));
        itemElement.appendChild(this.createTextNode("size", item.getSizeChoice(), itemDocument, tagElement));
        itemElement.appendChild(this.createTextNode("florist_number", item.getFloristNumber(), itemDocument, tagElement));
        itemElement.appendChild(this.createTextNode("florist_name", item.getFloristName(), itemDocument, tagElement));
        itemElement.appendChild(this.createTextNode("discount_amount", item.getDiscountAmount(), itemDocument, tagElement));
        itemElement.appendChild(this.createTextNode("percent_off", item.getPercentOff(), itemDocument, tagElement));
        itemElement.appendChild(this.createTextNode("miles_points", item.getMilesPoints(), itemDocument, tagElement));
        itemElement.appendChild(this.createTextNode("reward_type", item.getRewardType(), itemDocument, tagElement));
        itemElement.appendChild(this.createTextNode("discount_type", item.getDiscountType(), itemDocument, tagElement));
        itemElement.appendChild(this.createTextNode("status_order", item.getStatusSortOrder(), itemDocument, tagElement));
        itemElement.appendChild(this.createTextNode("order_comments", item.getOrderComments(), itemDocument, tagElement));
        itemElement.appendChild(this.createTextNode("hp_sequence", item.getHpSequence(), itemDocument, tagElement));
        itemElement.appendChild(this.createTextNode("item_source_code", item.getSourceCode(), itemDocument, tagElement));
        itemElement.appendChild(this.createTextNode("item_source_description", item.getSourceDescription(), itemDocument, tagElement));
        itemElement.appendChild(this.createTextNode("iow_flag", item.getItemOfTheWeekFlag(), itemDocument, tagElement));
        itemElement.appendChild(this.createTextNode("quantity", item.getQuantity(), itemDocument, tagElement));
        itemElement.appendChild(this.createTextNode("commission", item.getCommission(), itemDocument, tagElement));
        itemElement.appendChild(this.createTextNode("wholesale", item.getWholesale(), itemDocument, tagElement));
        itemElement.appendChild(this.createTextNode("transaction", item.getTransaction(), itemDocument, tagElement));
        itemElement.appendChild(this.createTextNode("pdb_price", item.getPdbPrice(), itemDocument, tagElement));
        itemElement.appendChild(this.createTextNode("shipping_tax", item.getShippingTax(), itemDocument, tagElement));
        itemElement.appendChild(this.createTextNode("miles_points_amt", item.getMilesPointsAmt(), itemDocument, tagElement));
        itemElement.appendChild(this.createTextNode("premier_collection_flag", item.getPremierCollectionFlag(), itemDocument, tagElement));
        itemElement.appendChild(this.createTextNode("preferred_processing_partner", item.getPreferredProcessingPartner(), itemDocument, tagElement));
        itemElement.appendChild(this.createTextNode("bin_source_changed_flag", item.getBinSourceChangedFlag(), itemDocument, tagElement));
        itemElement.appendChild(this.createTextNode("apply_surchage_code", item.getApplySurchargeCode(), itemDocument, tagElement));        
        itemElement.appendChild(this.createTextNode("surcharge_fee", item.getFuelSurcharge(), itemDocument, tagElement));
        itemElement.appendChild(this.createTextNode("surcharge_description", item.getFuelSurchargeDescription(), itemDocument, tagElement));
        itemElement.appendChild(this.createTextNode("same_day_upcharge_fee", item.getSameDayUpcharge(), itemDocument, tagElement));
        //SGC adding international service fee
        itemElement.appendChild(this.createTextNode("international_service_fee", item.getInternationalFloristServiceCharge(),itemDocument,tagElement));
        
        
        // #11946 - BBN - Flags used on itemsdiv.xsl         
       itemElement.appendChild(this.createTextNode("morning_delivery_flag", item.getMorningDeliveryOpted() == null ? "N" : item.getMorningDeliveryOpted(), itemDocument, tagElement));
       itemElement.appendChild(this.createTextNode("morning_delivery_unavailable", !"Y".equals(item.getMorningDeliveryAvailable())  ? "Y" : "N", itemDocument, tagElement));
       itemElement.appendChild(this.createTextNode("product_type", item.getProductType(), itemDocument, tagElement));
       itemElement.appendChild(this.createTextNode("morning_delivery_fee", item.getMorningDeliveryFee(), itemDocument, tagElement));
       itemElement.appendChild(this.createTextNode("orig_order_has_mdf", item.getOriginalOrderHasMDF() == null ? "N" : item.getOriginalOrderHasMDF(), itemDocument, tagElement));
        
        // Add personalization data
        String accessoryId = null;
        String pdpPersonalizationStr = "";
        boolean isPersonalized = false;
        if(item.getPersonalizationData() != null && !item.getPersonalizationData().equals(""))
        {
            isPersonalized = true;
            HashMap<String, String> pcValueMap = new HashMap<String, String>();
            HashMap<String, String> pcSortMap = new HashMap<String, String>();
            accessoryId = FTDCommonUtils.populatePDPPersonalizationMaps(item.getPersonalizationData(), pcValueMap, pcSortMap);
            if (accessoryId != null && !accessoryId.isEmpty()) {
               
               // New PDP style personalizations (Nov 2017)
               //
               Iterator<Map.Entry<String, String>> iter = pcValueMap.entrySet().iterator();
               while (iter.hasNext()) {
                  Map.Entry<String, String> pcValuePair = (Map.Entry<String,String>)iter.next();
                  String sortOrderStr = pcSortMap.get(pcValuePair.getKey());
                  if (sortOrderStr != null && !sortOrderStr.isEmpty()) {
                     pdpPersonalizationStr += "Name: \"" + pcValuePair.getKey() + "\" Value: \"" + pcValuePair.getValue() + "\" Sort-order: " + sortOrderStr + "  ";
                  } else {
                     pdpPersonalizationStr += "Name: \"" + pcValuePair.getKey() + "\" Value: \"" + pcValuePair.getValue() + "\"  ";                     
                  }
               }               
               itemElement.appendChild(this.createTextNode("personalizations", "<notused/>", itemDocument, tagElement));  

            } else {
               
               // Old style personalizations
               //
               Document tmpDoc = DOMUtil.getDocument(item.getPersonalizationData());
               NodeList nl = null;
               try
               {
                   nl = DOMUtil.selectNodeList(tmpDoc,"personalizations");
               }
               catch (XPathExpressionException e)
               {
                   // This is a developer exception, no need to expose for handling
                   throw new RuntimeException(e);
               }
               
               // There should only be one personalizations node
               if(nl.getLength() > 0)
                 itemElement.appendChild(itemDocument.importNode(nl.item(0), true));
            }
        }
        itemElement.appendChild(this.createTextNode("pdp_personalizations", pdpPersonalizationStr, itemDocument, tagElement));
        if (!isPersonalized) {
           itemElement.appendChild(this.createTextNode("personalizations", "", itemDocument, tagElement)); 
        }

        // Add Ons
        Element addOnsElement = itemDocument.createElement("add_ons");
        itemElement.appendChild(addOnsElement);
        
        if(item.getAddOns() != null)
        {
            Element addOnElement = null;
            AddOnsVO addOn = null;
            Iterator addOnIterator = item.getAddOns().iterator();

            while(addOnIterator.hasNext())
            {
                addOn = (AddOnsVO) addOnIterator.next();
                addOnElement = itemDocument.createElement("add_on");
                addOnsElement.appendChild(addOnElement);

                addOnElement.appendChild(this.createTextNode("id", String.valueOf(addOn.getAddOnId()), itemDocument, tagElement));
                addOnElement.appendChild(this.createTextNode("quantity", addOn.getAddOnQuantity(), itemDocument, tagElement));   
                addOnElement.appendChild(this.createTextNode("code", addOn.getAddOnCode(), itemDocument, tagElement));  
                addOnElement.appendChild(this.createTextNode("description", addOn.getDescription(), itemDocument, tagElement)); 
                addOnElement.appendChild(this.createTextNode("price", addOn.getPrice(), itemDocument, tagElement));
                addOnElement.appendChild(createTextNode("discountAmount", addOn.getDiscountAmount(), itemDocument, tagElement));
                addOnElement.appendChild(this.createTextNode("type", addOn.getAddOnType(), itemDocument, tagElement));  
            }
        }

        // Dispositions
        Element dispositionsElement = itemDocument.createElement("order_dispositions");
        itemElement.appendChild(dispositionsElement);
        
        if(item.getDispositions() != null)
        {
            Element dispositionElement = null;
            DispositionsVO disposition = null;
            Iterator dispositionIterator = item.getDispositions().iterator();

            while(dispositionIterator.hasNext())
            {
                disposition = (DispositionsVO) dispositionIterator.next();
                dispositionElement = itemDocument.createElement("disposition");
                dispositionsElement.appendChild(dispositionElement);

                dispositionElement.appendChild(this.createTextNode("code", disposition.getDispositionID(), itemDocument, tagElement));
                dispositionElement.appendChild(this.createTextNode("description", disposition.getDispositionDescription(), itemDocument, tagElement));
                dispositionElement.appendChild(this.createTextNode("comments", disposition.getComments(), itemDocument, tagElement));   
                dispositionElement.appendChild(this.createTextNode("call_flag", disposition.getCalledCustomerFlag(), itemDocument, tagElement));  
                dispositionElement.appendChild(this.createTextNode("email_flag", disposition.getSentEmailFlag(), itemDocument, tagElement)); 
                dispositionElement.appendChild(this.createTextNode("stock_message_id", disposition.getStockMessageID(), itemDocument, tagElement));
                dispositionElement.appendChild(this.createTextNode("email_subject", disposition.getEmailSubject(), itemDocument, tagElement));  
                dispositionElement.appendChild(this.createTextNode("email_message", disposition.getEmailMessage(), itemDocument, tagElement));
            }
        }

        // Recipient
        if(item.getRecipients() != null)
        {
            recipientIterator = item.getRecipients().iterator();
            while(recipientIterator.hasNext())
            {
                recipient = (RecipientsVO) recipientIterator.next();

                itemElement.appendChild(this.createTextNode("recip_id", Long.toString(recipient.getRecipientId()), itemDocument, tagElement));
                itemElement.appendChild(this.createTextNode("recip_customer_id", recipient.getCustomerId(), itemDocument, tagElement));
                itemElement.appendChild(this.createTextNode("recip_first_name", recipient.getFirstName(), itemDocument, tagElement));
                itemElement.appendChild(this.createTextNode("recip_last_name", recipient.getLastName(), itemDocument, tagElement));
                itemElement.appendChild(this.createTextNode("occasion", item.getOccassionId(), itemDocument, tagElement));
                itemElement.appendChild(this.createTextNode("occasion_description", item.getOccasionDescription(), itemDocument, tagElement));
                itemElement.appendChild(this.createTextNode("delivery_date", item.getDeliveryDate(), itemDocument, tagElement));
                itemElement.appendChild(this.createTextNode("second_delivery_date", item.getDeliveryDateRangeEnd(), itemDocument, tagElement));
                itemElement.appendChild(this.createTextNode("ship_date", item.getShipDate(), itemDocument, tagElement));
                itemElement.appendChild(this.createTextNode("shipping_system", item.getShippingSystem(), itemDocument, tagElement));
                itemElement.appendChild(this.createTextNode("card_message", item.getCardMessage(), itemDocument, tagElement));
                itemElement.appendChild(this.createTextNode("card_signature", item.getCardSignature(), itemDocument, tagElement));
                itemElement.appendChild(this.createTextNode("special_instructions", item.getSpecialInstructions(), itemDocument, tagElement));
                itemElement.appendChild(this.createTextNode("shipping_method", item.getShipMethod(), itemDocument, tagElement));
                itemElement.appendChild(this.createTextNode("lmg_flag", null, itemDocument, tagElement));
                itemElement.appendChild(this.createTextNode("lmg_email_address", item.getLastMinuteGiftEmail(), itemDocument, tagElement));
                itemElement.appendChild(this.createTextNode("lmg_email_signature", item.getLastMinuteGiftSignature(), itemDocument, tagElement));
                itemElement.appendChild(this.createTextNode("fol_indicator", null, itemDocument, tagElement));
                itemElement.appendChild(this.createTextNode("sunday_delivery_flag", null, itemDocument, tagElement));
                itemElement.appendChild(this.createTextNode("sender_release_flag", item.getSenderInfoRelease(), itemDocument, tagElement));
                itemElement.appendChild(this.createTextNode("ariba_po_number", item.getAribaPoNumber(), itemDocument, tagElement));
                itemElement.appendChild(this.createTextNode("ariba_cost_center", item.getAribaCostCenter(), itemDocument, tagElement));
                itemElement.appendChild(this.createTextNode("ariba_ams_project_code", item.getAribaAmsProjectCode(), itemDocument, tagElement));
                itemElement.appendChild(this.createTextNode("ariba_unspsc_code", item.getAribaUnspscCode(), itemDocument, tagElement));
                itemElement.appendChild(this.createTextNode("military_start_ticket_number", null, itemDocument, tagElement));
                itemElement.appendChild(this.createTextNode("novator_sender_release_flag", null, itemDocument, tagElement));
                itemElement.appendChild(this.createTextNode("product_substitution_acknowledgement", item.getSubstituteAcknowledgement(), itemDocument, tagElement));
        
                // Recipient address
                if(recipient.getRecipientAddresses() != null && recipient.getRecipientAddresses().size() > 0)
                {
                    recipientAddress = (RecipientAddressesVO) recipient.getRecipientAddresses().get(0);

                    itemElement.appendChild(this.createTextNode("recip_address1", recipientAddress.getAddressLine1(), itemDocument, tagElement));
                    itemElement.appendChild(this.createTextNode("recip_address2", recipientAddress.getAddressLine2(), itemDocument, tagElement));
                    itemElement.appendChild(this.createTextNode("recip_city", recipientAddress.getCity(), itemDocument, tagElement));
                    itemElement.appendChild(this.createTextNode("recip_state", recipientAddress.getStateProvince(), itemDocument, tagElement));

                    // Format the postal code for US
                    String postalCode = null;
                    if(recipientAddress.getCountry() != null && recipientAddress.getCountry().equalsIgnoreCase("US"))
                    {
                        postalCode = recipientAddress.getPostalCode();
                        if(postalCode == null) postalCode = "";

                        if(postalCode.length() == 9)
                        {
                              postalCode =  postalCode.substring(0, 5) + "-" + postalCode.substring(5);
                        }
                    }
                    else
                    {
                        postalCode = recipientAddress.getPostalCode();
                    }

                    itemElement.appendChild(this.createTextNode("recip_postal_code", postalCode, itemDocument, tagElement));
                    itemElement.appendChild(this.createTextNode("recip_country", recipientAddress.getCountry(), itemDocument, tagElement));
                    itemElement.appendChild(this.createTextNode("recip_international", recipientAddress.getInternational(), itemDocument, tagElement));
                    itemElement.appendChild(this.createTextNode("ship_to_type", recipientAddress.getAddressType(), itemDocument, tagElement));
                    itemElement.appendChild(this.createTextNode("ship_to_type_code", recipientAddress.getAddressTypeCode(), itemDocument, tagElement));
                    itemElement.appendChild(this.createTextNode("ship_to_type_name", recipientAddress.getName(), itemDocument, tagElement));
                    itemElement.appendChild(this.createTextNode("ship_to_type_info", recipientAddress.getInfo(), itemDocument, tagElement));
                    itemElement.appendChild(this.createTextNode("time_of_service", item.getTimeOfService(), itemDocument, tagElement));
                    
                    if (recipientAddress.getCountry() != null && recipientAddress.getCountry().equalsIgnoreCase("CA")) {
                    	canadianRecipient = true;
                    }
                }
            
                // Recipient phone numbers
				if (recipient.getRecipientPhones() != null) {
					recipientPhoneIterator = recipient.getRecipientPhones().iterator(); 
					while (recipientPhoneIterator.hasNext()) {
						recipientPhone = (RecipientPhonesVO) recipientPhoneIterator.next();
						if (recipientPhone != null && !StringUtils.isEmpty(recipientPhone.getPhoneNumber()) && recipientPhone.getPhoneType().equals("WORK")) {							
							break; //Day phone							
						}
					}
					
					if(recipientPhone != null && !StringUtils.isEmpty(recipientPhone.getPhoneNumber())) {
						itemElement.appendChild(this.createTextNode("recip_phone", recipientPhone.getPhoneNumber(), 
								itemDocument, tagElement));
						itemElement.appendChild(this.createTextNode("recip_phone_ext", recipientPhone.getExtension(),
								itemDocument, tagElement));
					}
				}
            }
        }
        
        // Order Detail Extensions
        Element extensionsElement = itemDocument.createElement("item-extensions");
        if(item.getOrderDetailExtensions() != null)
        {
            itemElement.appendChild(extensionsElement);
            Element extElement = null;
            OrderDetailExtensionsVO ext = null;
            Iterator extIterator = item.getOrderDetailExtensions().iterator();

            while(extIterator.hasNext())
            {
                ext = (OrderDetailExtensionsVO) extIterator.next();
                extElement = itemDocument.createElement("extension");
                extensionsElement.appendChild(extElement);
                extElement.setAttribute("name", ext.getInfoName());
                extElement.setAttribute("data", ext.getInfoData());
            }
        }

        itemElement.appendChild(this.createTextNode("free-shipping-flag", item.getFreeShipping(), itemDocument, tagElement));
        itemElement.appendChild(this.createTextNode("product_sub_type", item.getProductSubType(), itemDocument, tagElement));

        // Taxes
        Element taxesElement = itemDocument.createElement("taxes");
        itemElement.appendChild(taxesElement);
        if (item.getItemTaxVO().getTaxSplit() != null) {
            Element taxElement = null;
            TaxVO taxVO = null;
            Iterator taxIterator = item.getItemTaxVO().getTaxSplit().iterator();
            boolean showSplit = false;
            while(taxIterator.hasNext()) {
                taxVO = (TaxVO) taxIterator.next();
                if(taxVO.getDisplayOrder() > 0) {
                	showSplit = true;
                	taxElement = itemDocument.createElement("tax");
                	taxesElement.appendChild(taxElement);
                	taxElement.appendChild(this.createTextNode("description", taxVO.getDescription(), itemDocument, tagElement));
                	taxElement.appendChild(this.createTextNode("amount", taxVO.getAmount().toString(), itemDocument, tagElement));   
                	taxElement.appendChild(this.createTextNode("display_order", String.valueOf(taxVO.getDisplayOrder()), itemDocument, tagElement));
                }
            }
            
            if(!showSplit && !StringUtils.isEmpty(item.getItemTaxVO().getTaxAmount()) &&  new BigDecimal(item.getItemTaxVO().getTaxAmount()).compareTo(BigDecimal.ZERO) > 0) {
            	taxElement = itemDocument.createElement("tax");
            	taxesElement.appendChild(taxElement);
            	String taxDescription  = new CalculateTaxUtil().getDefaultTaxLabel(recipientAddress.getCountry(), companyId);
            	
            	taxElement.appendChild(this.createTextNode("description", taxDescription, itemDocument, tagElement));
            	taxElement.appendChild(this.createTextNode("amount", item.getItemTaxVO().getTaxAmount(), itemDocument, tagElement));   
            	int displayOrder = TaxDisplayOrder.fromValue(taxDescription).value(); 
            	taxElement.appendChild(this.createTextNode("display_order", String.valueOf(displayOrder), itemDocument, tagElement));
            }
        }
        
        itemElement.appendChild(this.createTextNode("late_cutoff_fee", item.getLatecutoffCharge(), itemDocument, tagElement));
        itemElement.appendChild(this.createTextNode("vendor_sun_upcharge", item.getSundayUpcharge(), itemDocument, tagElement));
        itemElement.appendChild(this.createTextNode("vendor_mon_upcharge", item.getMondayUpcharge(), itemDocument, tagElement));

        return itemElement;
    }

    private Element createTextNode(String elementName, String elementValue, Document document, Element tagElement)
    {
        tagElement = document.createElement(elementName);
        if(elementValue != null)
        {
            tagElement.appendChild(document.createTextNode(elementValue));
        }

        return tagElement;
    }

    public static void main(String[] args)
    {
        OrderXAO orderXAO = new OrderXAO();
        Connection connection = null;
        
        try
        {
            connection = orderXAO.getConnection();

            ScrubMapperDAO scrubMapperDAO = new ScrubMapperDAO(connection);
            OrderVO order = scrubMapperDAO.mapOrderFromDB("123");
            
            Document doc = orderXAO.generateOrderXML(order);
            DOMUtil.print(doc, System.out);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            try { 
            connection.close(); }
            catch(SQLException se) {}
        }
    }

    private Connection getConnection() 
    {
        Connection connection = null;
        String driver_ = "oracle.jdbc.driver.OracleDriver";
        String database_ = "jdbc:oracle:thin:@sisyphus.ftdi.com:1521:DEV2";
        String user_ = "osp";
        String password_ = "osp";

        try
        {
            Class.forName(driver_);
            connection = DriverManager.getConnection(database_, user_, password_);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return connection;
    }

    private boolean validateCreditCardDate(String creditCardDate)
    {
        boolean validDate = true;

        String DATE_FORMAT = "MM/yy";
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
        sdf.setLenient(false);
        
        try
        {
            sdf.parse(creditCardDate);
            if(creditCardDate.length() != 5)
            {
                validDate = false;
            }
        }
        catch(Exception pe)
        {
            validDate = false;
        }

        return validDate;
    }
}