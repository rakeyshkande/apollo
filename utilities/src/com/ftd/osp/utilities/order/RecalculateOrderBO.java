package com.ftd.osp.utilities.order;


import java.io.IOException;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.commons.lang.StringUtils;
import org.xml.sax.SAXException;

import com.ftd.ftdutilities.CalculateTaxUtil;
import com.ftd.ftdutilities.FTDCAMSUtils;
import com.ftd.ftdutilities.FTDCommonUtils;
import com.ftd.osp.ordervalidator.util.ValidationConstants;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.FTDFuelSurchargeUtilities;
import com.ftd.osp.utilities.PartnerUtility;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.handlers.GlobalParmHandler;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.order.dao.RecalculateOrderDAO;
import com.ftd.osp.utilities.order.vo.AddOnsVO;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.OrderStatus;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.order.vo.PaymentMethodMilesPointsVO;
import com.ftd.osp.utilities.order.vo.PaymentsVO;
import com.ftd.osp.utilities.order.vo.RecalculateOrderGlobalParmsVO;
import com.ftd.osp.utilities.order.vo.RecalculateOrderMilesPointsVO;
import com.ftd.osp.utilities.order.vo.RecalculateOrderPriceHeaderDetailsVO;
import com.ftd.osp.utilities.order.vo.RecalculateOrderSourceCodeVO;
import com.ftd.osp.utilities.order.vo.RecipientAddressesVO;
import com.ftd.osp.utilities.order.vo.RecipientsVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.AddOnVO;
import com.ftd.osp.utilities.vo.MembershipDetailsVO;
import com.ftd.osp.utilities.vo.ProductVO;
import com.ftd.osp.utilities.vo.ServiceChargeVO;
import com.ftd.osp.utilities.vo.SurchargeVO;
import com.ftd.pac.util.PACUtil;
import com.ftd.pac.util.exception.PACException;
import com.ftd.pac.util.vo.PACResponse;


/**This class recalculates the amounts in the
 * orderVO
 *
 *
 * @author Ed Mueller
 *
 * 12/9/03 - Populate of ProductsTotal property commented out.
 * This field is supposed to be a count of the number
 * of items in the order and not a product cost total.
 */
public class RecalculateOrderBO
{

  private static final String METADATA_CONFIG_FILE_NAME = "metadata_config.xml";

  //PRICE/SIZE TYPES
  private static final String SIZE_STANDARD = "A";
  private static final String SIZE_DELUXE = "B";
  private static final String SIZE_PREMIUM = "C";

  //DISCOUNT TYPES
  private static final String DISCOUNT_TYPE_MILES = "M";
  private static final String DISCOUNT_TYPE_DOLLARS = "D";
  private static final String DISCOUNT_TYPE_PERCENT = "P";

  //PRODUCT TYPES
  private static final String PRODUCT_TYPE_FRESHCUT = "FRECUT";
  private static final String PRODUCT_TYPE_SAMEDAY_FRESHCUT = "SDFC";
  private static final String PRODUCT_TYPE_FLORAL = "FLORAL";
  private static final String PRODUCT_TYPE_SAMEDAY_GIFT = "SDG";
  private static final String PRODUCT_TYPE_SPECIALTY_GIFT = "SPEGFT";

  //SHIP TYPES
  private static final String FLORIST_DELIVERED = "FL";
  private static final String SATURDAY_DELIVERED = "SA";
  private static final String SAME_DAY_DELIVERED = "SD";

  //STATE CODES
  public static final String HAWAII = "HI";
  public static final String ALASKA = "AK";

  //PRICE CODE IGNORE VALUE  
  private static final String IGNORE_PRICE_CODE_VALUE = "ZZ";

  //FLAGS
  private static final String DISCOUNTS_NOT_ALLOWED = "N";

  //scale to which amounts are calculated
  private static final int AMOUNT_SCALE = 2;
  
  //Calculation basis
  private static final String CALC_BASIS_FIXED = "F";
  private static final String CALC_BASIS_MERCH = "M";
  private static final String CALC_BASIS_TOTAL = "T";
  
  //global params 
  RecalculateOrderGlobalParmsVO recalculateOrderGlobalParmsVO;

  private static Logger logger = new Logger(RecalculateOrderBO.class.getName());
  
  private RecalculateOrderDAO recalculateOrderDAO;
   
  /**
   * The Exception Mode to utilize. If this is true, the BO will not throw some exceptions. Instead, it will
   * log them, and continue with default behavior.
   * Front End Applications such as JOE, set this to TRUE to allow orders to flow through
   */
  private boolean frontEndExceptionMode = false;

  private boolean calculateTaxFlag = true;
  
  /**Constructor.
   * Initialize the BO with the Default DAO Implementation
   */
  public RecalculateOrderBO()
  {
	  recalculateOrderDAO = new RecalculateOrderDAO();
  }

  /**Constructor.
   * Initialize the BO with the passed in RecalculateOrderDAO Implementation
   */
  public RecalculateOrderBO(RecalculateOrderDAO inRecalculateOrderDAO)
  {
	  recalculateOrderDAO = inRecalculateOrderDAO;
  }
  
  /**Constructor.
   * Initialize the BO with the passed in RecalculateOrderCacheDAO Implementation
   */
  public RecalculateOrderBO(RecalculateOrderDAO inRecalculateOrderDAO, boolean frontEndExceptionMode)
  {
	  recalculateOrderDAO = inRecalculateOrderDAO;
	  setFrontEndExceptionMode(frontEndExceptionMode);
  }
  
  /**
   * Set's the Exception Model to utilize. If this is true, the BO will not throw some exceptions. Instead, it will
   * log them, and continue with default behavior.
   * Front End Applications such as JOE, set this to TRUE to allow orders to flow through if there are data setup 
   * issues in the background.
   * 
   * @param frontEndExceptionMode
   */
  public void setFrontEndExceptionMode(boolean frontEndExceptionMode)
  {
    this.frontEndExceptionMode = frontEndExceptionMode;
    logger.info("Front End exception Mode: " + frontEndExceptionMode);
  }

  /**
   * Set's the calculateTax Flag
   * Certain methods in JOE call RecalculateOrderBO just to get the service/shipping fees.
   * In these cases, there is no need for the overhead of calling the new Tax Service.
   * 
   * @param setCalculateTaxFlag
   */
  public void setCalculateTaxFlag(boolean calculateTaxFlag)
  {
    this.calculateTaxFlag = calculateTaxFlag;
  }

  /**Recalculate dollar amounts and tax on the orderDetails VO.
   * @param OrderDetailsVO
   * @throws Exception 
   * @returns OrderDetailVO
   */
	public OrderDetailsVO recalculateTaxAndTotal(Connection connection, OrderDetailsVO odVO, String companyId) throws Exception {
		if (logger.isDebugEnabled()) {
			logger.debug("recalculateTaxAndTotal() for externalOrderNumber: " + odVO.getExternalOrderNumber());
		}

		ProductVO productVO = recalculateOrderDAO.getProduct(connection, odVO.getProductId()); 

		// Calculate Tax
		CalculateTaxUtil taxUtil = new CalculateTaxUtil();
		taxUtil.calculateItemTax(odVO, productVO, companyId);

		if (logger.isDebugEnabled()) {
			logger.debug("recalculateTaxAndTotal() tax is recalculated to: " + odVO.getItemTaxVO());
		}

		BigDecimal total = this.calculateOrderTotal(odVO, connection);
		
		if (logger.isDebugEnabled()) {
			logger.debug("recalculateTaxAndTotal() total tax is recalculated as: " + total);
		}

		return odVO;
	}

  /**Recalculate dollar amounts on the orderDetails VO.
   * @param OrderDetailsVO
 * @throws Exception 
   * @returns BigDecimal
   */
  public BigDecimal calculateOrderTotal(OrderDetailsVO odVO)
  {
    BigDecimal orderAmount = new BigDecimal(0);
    String value = odVO.getProductsAmount();
    value = (value != null && value.trim().length() > 0)? value: "0";
    BigDecimal prodAmount = new BigDecimal(value);

    value = odVO.getAddOnAmount();
    value = (value != null && value.trim().length() > 0)? value: "0";
    BigDecimal addOnAmount = new BigDecimal(value);

    value = odVO.getServiceFeeAmount();
    value = (value != null && value.trim().length() > 0)? value: "0";
    BigDecimal serviceFeeAmount = new BigDecimal(value);

    value = odVO.getShippingFeeAmount();
    value = (value != null && value.trim().length() > 0)? value: "0";
    BigDecimal shippingFeeAmount = new BigDecimal(value);

    value = odVO.getDiscountAmount();
    value = (value != null && value.trim().length() > 0)? value: "0";
    BigDecimal discountAmount = new BigDecimal(value);

    value = odVO.getTaxAmount();
    value = (value != null && value.trim().length() > 0)? value: "0";
    BigDecimal taxAmount = new BigDecimal(value);

    value = odVO.getShippingTax();
    value = (value != null && value.trim().length() > 0)? value: "0";
    BigDecimal shipTaxAmount = new BigDecimal(value);

    value = odVO.getGccAmt();
    value = (value != null && value.trim().length() > 0)? value: "0";
    BigDecimal gccAmount = new BigDecimal(value);

    orderAmount = 
        orderAmount.add(prodAmount).add(addOnAmount).add(serviceFeeAmount).add(shippingFeeAmount).add(taxAmount).add(shipTaxAmount).subtract(discountAmount).subtract(gccAmount);
    odVO.setOrderAmount(orderAmount.toString());

    return orderAmount;
  }

  public BigDecimal calculateOrderTotal(OrderDetailsVO odVO, Connection connection) throws Exception
  {
	PartnerUtility partnerUtility = new PartnerUtility();  
	BigDecimal[] orderAmounts = null;
	
	String originId = recalculateOrderDAO.getOrderOrigin(odVO.getGuid(), connection);
	
	if(StringUtils.isEmpty(originId)) {
		logger.info("Invalid Origin Id, " + odVO.getGuid());
		return null;
	}
	
	if(new PartnerUtility().isPartnerOrder(originId,odVO.getSourceDescription(), connection)) {
		partnerUtility.getOriginalOrderAmounts(connection, odVO, "PARTNER");
	} else if(new MercentOrderPrefixes(connection).isMercentOrder(originId)){
		 orderAmounts = partnerUtility.getOriginalOrderAmounts(connection, odVO, "MERCENT");
	}
			
    BigDecimal orderAmount = new BigDecimal(0);
    
    if (orderAmounts != null) {
    String value = odVO.getProductsAmount();
    BigDecimal percentage = null;
    value = (value != null && value.trim().length() > 0)? value: "0";
    BigDecimal prodAmount = new BigDecimal(value);

    value = odVO.getAddOnAmount();
    value = (value != null && value.trim().length() > 0)? value: "0";
    BigDecimal addOnAmount = new BigDecimal(value);

    value = odVO.getServiceFeeAmount();
    value = (value != null && value.trim().length() > 0)? value: "0";
    BigDecimal serviceFeeAmount = new BigDecimal(value);

    value = odVO.getShippingFeeAmount();
    value = (value != null && value.trim().length() > 0)? value: "0";
    BigDecimal shippingFeeAmount = new BigDecimal(value);

    value = odVO.getDiscountAmount();
    value = (value != null && value.trim().length() > 0)? value: "0";
    BigDecimal discountAmount = new BigDecimal(value);
    
    value = odVO.getTaxAmount();
    value = (value != null && value.trim().length() > 0)? value: "0";
    BigDecimal shipTaxAmount, taxAmount;
    if ((orderAmounts[2].compareTo(BigDecimal.ZERO) > 0) && (orderAmounts[3].compareTo(BigDecimal.ZERO) > 0)) {
    	percentage = orderAmounts[2].divide((orderAmounts[2].add(orderAmounts[3])), 2, BigDecimal.ROUND_HALF_UP);
    	// In above line indeces are : SHIPPING_TAX_AMT_POS = 2, TAX_AMT_POS = 3;
    	shipTaxAmount = new BigDecimal(value).multiply(percentage).setScale(2, BigDecimal.ROUND_HALF_UP); 
    	odVO.setShippingTax(shipTaxAmount.toString());
    	taxAmount = new BigDecimal(value).subtract(shipTaxAmount);
    	odVO.setTaxAmount(taxAmount.toString());
    
    value = odVO.getGccAmt();
    value = (value != null && value.trim().length() > 0)? value: "0";
    BigDecimal gccAmount = new BigDecimal(value);

    orderAmount = 
        orderAmount.add(prodAmount).add(addOnAmount).add(serviceFeeAmount).add(shippingFeeAmount).add(taxAmount).add(shipTaxAmount).subtract(discountAmount).subtract(gccAmount);
    odVO.setOrderAmount(orderAmount.toString());

    }
    }
    else {
       orderAmount = this.calculateOrderTotal(odVO);
    }
    
    
    return orderAmount;
  }


  /**
   * Ali Lakhani
   *
   * Wrapper for the Recalculate method.  In the instance where the product amount is set by the
   * calling class, we do not want to clear the amounts.  It will be assumed that the calling class
   * has initialized the amount fields before calling the method.
   *
   * @param OrderVO order
   * @returns OrderVO
   */
  public void recalculate(Connection connection, OrderVO order)
    throws IOException, SAXException, ParserConfigurationException, SQLException, RecalculateException, TransformerException
  {
    boolean clearODAmounts = true;
    recalculate(connection, order, clearODAmounts);
  }


  /**Recalculate dollar amounts on the order VO.
   * @param OrderVO order
   * @returns OrderVO
   */
  public void recalculate(Connection connection, OrderVO order, boolean clearODAmounts)
    throws IOException, SAXException, ParserConfigurationException, SQLException, RecalculateException, TransformerException
  {
    logger.info("BEGIN Order recalculate");
    
	// Save the original product amounts for variable priced products
    Map originalProductAmounts = getProductAmounts(order);
    
    //Get order origin
    String origin = order.getOrderOrigin();

    // Save the original tax amount
    Map originalTaxAmounts = getTaxAmounts(order);
    
    //clear out existing amounts
    clearAmounts(order, clearODAmounts);

    //set class variables
    setGlobalParams(connection);
    
    //order totals
    BigDecimal orderTotal = new BigDecimal(0);
    BigDecimal orderShippingFee = new BigDecimal(0);
    BigDecimal orderAddonAmount = new BigDecimal(0);
    BigDecimal orderProductAmount = new BigDecimal(0);
    BigDecimal orderTaxAmount = new BigDecimal(0);
    BigDecimal orderServiceFee = new BigDecimal(0);
    BigDecimal orderSurchargeFee = new BigDecimal(0);
    BigDecimal orderDiscountTotal = new BigDecimal(0);
    BigDecimal orderPoints = new BigDecimal(0);
    BigDecimal orderServiceFeeSavings = new BigDecimal(0);
    BigDecimal orderShippingFeeSavings = new BigDecimal(0);

    //source code information
    String sourceCode = null;
    String pricingCode = null;
    String partnerIdCode = null;
    String shippingCode = null;
    String jcPennyFlag = null;

    MercentOrderPrefixes mercentOrderPrefixes = new MercentOrderPrefixes(connection);
    //Added this check to have a fix for defect no 14454.
    order.setBuyerCamsVerified(true);
    
    boolean isPartnerOrder = false;
    if(new PartnerUtility().isPartnerOrder(origin,order.getSourceCode(), connection)) {
    	isPartnerOrder = true;
    }
    
    
    
    //commented this code to save additional call to CAMS and also incorporated this logic below
    /*if(order != null && StringUtils.isNotEmpty(order.getBuyerEmailAddress()))
    {
		Boolean isCamsUp = FTDCAMSUtils.isValidFSAccount(order.getBuyerEmailAddress(), order.getOrderDateTime());
		
		if(isCamsUp == null){
			order.setBuyerCamsVerified(false);		
		}else{
			order.setBuyerCamsVerified(true);
		}
    }*/
    
    /*
     * Logic to get membership details for PC and FS and verify the memberships in just one call.
     */
    Map<String, MembershipDetailsVO> membershipMap = new HashMap<String, MembershipDetailsVO>();
    if(order != null && StringUtils.isNotEmpty(order.getBuyerEmailAddress())){
    	List<String> emailList = new ArrayList<String>();
    	emailList.add(order.getBuyerEmailAddress());
    	//SP-101 setting InvokedAsynchronusly as true when the caller invokes from MDB.when timeout happens,it  re-process the message.Else it will use existing logic. 
    	logger.debug("IsAsynchronous call : "+order.isInvokedAsynchronously());
			membershipMap = (order.isInvokedAsynchronously() && FTDCAMSUtils.CAMS_ENABLE_FLAG.equalsIgnoreCase(FTDCAMSUtils.getCAMSTimeoutEnabledFlag())) 
					? FTDCAMSUtils.getMembershipDataWithTimeoutException(emailList, order.getOrderDateTime()) 
					: (order.isOeOrder() && !this.frontEndExceptionMode
							? FTDCAMSUtils.getMembershipData(emailList, order.getOrderDateTime(), order.isOeOrder(), order.getMasterOrderNumber())
							: FTDCAMSUtils.getMembershipData(emailList, order.getOrderDateTime())
					   );

    }
    MembershipDetailsVO fsMemDetails = null;
    MembershipDetailsVO pcMemDetails = null;
    if(membershipMap == null){
		order.setBuyerCamsVerified(false);		
	}else{
		order.setBuyerCamsVerified(true);
		if(membershipMap.containsKey(FTDCAMSUtils.FREESHIPPING_MEMBER_TYPE)){
			fsMemDetails = membershipMap.get(FTDCAMSUtils.FREESHIPPING_MEMBER_TYPE);
		}
		if(membershipMap.containsKey(FTDCAMSUtils.PREMIERCIRCLE_MEMBER_TYPE)){
			pcMemDetails = membershipMap.get(FTDCAMSUtils.PREMIERCIRCLE_MEMBER_TYPE);
		}
		
	}
    // Buyer Information
	Boolean buyerHasFreeShipping = checkBuyerHasFreeShipping(connection, order.isBuyerSignedIn(), order.getBuyerEmailAddress(), order.getOrderDateTime(), fsMemDetails);
	
	if (buyerHasFreeShipping != null){
	    if (buyerHasFreeShipping) {
	        order.setBuyerHasFreeShippingFlag("Y");
	    } else {
	        if(order.getFreeShippingMembershipInCartFlag() != null && order.getFreeShippingMembershipInCartFlag().equalsIgnoreCase("Y")){
	        	order.setBuyerHasFreeShippingFlag("Y");
	        }
	        else{
	        	//QE3SP-15
	        	//For JOE orders, check if cams timeout is encountered, if it is, check if buyer on order had
	        	//free shipping when order was placed in JOE or not.
	        	if(order.getOeBuyerHasFreeShipping() != null && order.getOeBuyerHasFreeShipping().equalsIgnoreCase("Y")){
	    			buyerHasFreeShipping = true;
	    			order.setBuyerHasFreeShippingFlag("Y");
	        	}else {
	        		order.setBuyerHasFreeShippingFlag("N");
	        	}
	        }	
	    }
       } else {
    	   order.setBuyerHasFreeShippingFlag("N");
    	   buyerHasFreeShipping = false;
       }
    
    //get lines on order
    List lineItems = order.getOrderDetail();

    RecalculateException lineException = null;
    Date orderTimestamp = order.getOrderDateTime();
    SurchargeVO surchargeVO = null;
    Calendar cal = null;
    if (orderTimestamp != null)
    {
      cal = Calendar.getInstance();
      cal.setTime(orderTimestamp);
    }
    
	
	RecalculateOrderSourceCodeVO sourceVO = null;
    
    //for each item in order
    for (int i = 0; i < lineItems.size(); i++)
    {

      //catch any bulk order exception that occurs at the line level and
      //continue processing.  The exception will be rethrown after all the lines have
      //been processed.
    	  
    	    BigDecimal itemPrice = new BigDecimal("0");
    	    BigDecimal lineTotal = new  BigDecimal("0");
    	    BigDecimal lineAddonTotal = new BigDecimal("0");
    	    BigDecimal taxAmount = new BigDecimal("0");
    	    BigDecimal shippingTaxAmount = new BigDecimal("0");
    	    BigDecimal discountAmount = new BigDecimal("0");
    	    
    	    boolean itemHasFreeShipping = false;
    	    boolean addServiceFeetoLineTotal = false;
    	    boolean addShippingFeetoLineTotal = false;
    	    
    	    BigDecimal lineShippingFee = new BigDecimal(0);
    		BigDecimal lineServiceFee = new BigDecimal(0); 
    		BigDecimal freeShipSavingsExcludedServiceFee = new BigDecimal("0");
    		BigDecimal fuelSurcharge = new BigDecimal(0);
    	  
    	    boolean isPDPOrder = false;
    	    
      try
      {

        OrderDetailsVO detailVO = (OrderDetailsVO) lineItems.get(i);
        
      //retrieve product info from database
        ProductVO productVO = recalculateOrderDAO.getProduct(connection, detailVO.getProductId());
        
        logger.info("PersonalizationTemplate : "+productVO.getPersonalizationTemplate());
        if(productVO.getPersonalizationTemplate() != null && productVO.getPersonalizationTemplate().equals("PDP")){
        	isPDPOrder = true;
        }
        
        sourceCode = detailVO.getSourceCode();
        sourceVO = recalculateOrderDAO.getSourceCodeDetails(connection, sourceCode);
        if (sourceVO == null)
        {
          String msg = "Source code not found in database(code=" + sourceCode + ". (cache)";
          logger.error(msg);
          throw new RecalculateException(msg);
        }
        pricingCode = sourceVO.getPricingCode();
        partnerIdCode = sourceVO.getPartnerId();
        shippingCode = sourceVO.getShippingCode();
        jcPennyFlag = sourceVO.getJcpenneyFlag();
        
        
        if (!isPDPOrder){

        	logger.info("Doing Recalculation for non-PDP item : "+detailVO.getExternalOrderNumber());
        /*
         * Logic to tag orders when first received or email changed in the scrub. 
         * DEFECT_15186: Moving this check ahead to be done before requiredCheck() 
         */
        logger.info("order.isAllowPCCheckUpdateOrder() : "+order.isAllowPCCheckUpdateOrder());
        if(detailVO.getPcFlag() == null || StringUtils.isEmpty(detailVO.getPcFlag()) 
        		|| "N".equalsIgnoreCase(detailVO.getPcFlag())){
	        if(order.isAllowPCCheckUpdateOrder() || order.isBuyerEmailChangedInScrub() || Integer.toString(OrderStatus.RECEIVED_ITEM).equals(detailVO.getStatus())){
				if(pcMemDetails != null && "Y".equalsIgnoreCase(pcMemDetails.getStatus())){
					detailVO.setPcGroupId(pcMemDetails.getGroupId());
					detailVO.setPcMembershipId(pcMemDetails.getMembershipId());
					detailVO.setPcFlag(pcMemDetails.getStatus());
				}else{
					detailVO.setPcFlag("N");
				}
			}
        }          

        if(pcMemDetails != null && "Y".equalsIgnoreCase(pcMemDetails.getStatus())){
        	order.setBuyerHasPCMembershipFlag("Y");
		}else{
			order.setBuyerHasPCMembershipFlag("N");
		}        
        
        //check for null values
        requiredCheck(detailVO);       
        
        detailVO.setDomesticFloristServiceCharge("0");
        detailVO.setInternationalFloristServiceCharge("0");
        detailVO.setShippingCost("0");
        detailVO.setVendorCharge("0");
        detailVO.setSaturdayUpcharge("0");
        detailVO.setAlaskaHawaiiSurcharge("0");
        detailVO.setFuelSurcharge("0");
        
        

        try{
            if (detailVO.isChargeRelatedItemChanged())
            {
              logger.debug("order item changed so using current date time for FTDFuelSurchargeUtilities for update order");
              surchargeVO = FTDFuelSurchargeUtilities.getSurchargeVO(null,sourceCode,connection );   
            }
            else
            {
              surchargeVO = FTDFuelSurchargeUtilities.getSurchargeVO(cal,sourceCode,connection );   
            }

        } catch (Exception e)
        {
          logger.error("Could not get data from FTDFuelSurchargeUtilities. "+ e);
          surchargeVO = new SurchargeVO();
        }
        finally
        {
        
          if (surchargeVO == null)
          {
            logger.error("SurchargVO didn't get initialized so initializg to default values. Source code used :" + sourceCode + " and order time is:" + cal.getTime().toString() );
            surchargeVO = new SurchargeVO();
          }
        }
        
     
        

        if (productVO == null)
        {
               
            //product not found
            String msg = "Product not found in database(" + detailVO.getProductId() + ")";
            logger.error(msg);
            throw new RecalculateException(msg);
        }   
 
        //error if product does not have a product type
        if (productVO.getProductType() == null || productVO.getProductType().length() <= 0)
        {
          String msg = "Product does not have a product type.";
          logger.error(msg);
          throw new RecalculateException(msg);
        }

        //determine base price based on size choice
        String origAmount = (String) originalProductAmounts.get(detailVO.getLineNumber());

        //if origAmount is blank, null or zero then turn off the variable price flag
        //if(origAmount == null || origAmount.length()<=0 || origAmount.equals("0") || origAmount.equals("0.00"))
        //{
        //  variablePriceFlag = "N";
        //}

        double origAmountDouble;
        if ((origAmount != null && Double.parseDouble(origAmount) > 0) || !clearODAmounts)
        {
          origAmountDouble = Double.parseDouble(origAmount);
          //     if(origAmountDouble >= standardPrice && origAmountDouble <= variablePriceMax)
          //     {
          itemPrice = new BigDecimal(origAmountDouble);
          if(origin != null && ("AMZNI".equalsIgnoreCase(origin) 
        		  || mercentOrderPrefixes.isMercentOrder(origin))
        		  || isPartnerOrder)
          {
	          BigDecimal standardPrice = new BigDecimal(productVO.getStandardPrice());
	          BigDecimal deluxePrice = new BigDecimal(productVO.getDeluxePrice());
	          BigDecimal premiumPrice = new BigDecimal(productVO.getPremiumPrice());
	          logger.info("standardPrice:"+standardPrice);
	          logger.info("deluxePrice:"+deluxePrice);
	          logger.info("premiumPrice:"+premiumPrice);
	          
	          if(!itemPrice.equals(standardPrice) && !itemPrice.equals(deluxePrice) && !itemPrice.equals(premiumPrice))
	          {
	        	  String externalOrderNumber = detailVO.getExternalOrderNumber();
	        	  logger.info("externalOrderNumber :"+externalOrderNumber);
	        	  if("AMZNI".equalsIgnoreCase(origin)) {
	        		  logger.info("Amazon Price["+itemPrice+"] doesn't match with any FTD Prices.");
	        		  String amzProdId = this.getAmazonProductIdByConfirmationNo(connection, externalOrderNumber);
		        	  logger.info("amzProdId: "+amzProdId);
		        	  if(amzProdId==null){
		        		  amzProdId = "";
		        	  }
		        	  amzProdId = amzProdId.trim().toLowerCase();
		        	  if(amzProdId.endsWith("_deluxe")){        		  
		        		  itemPrice = deluxePrice;
		        	  } else if(amzProdId.endsWith("_premium")){
		        		  itemPrice = premiumPrice;
		        	  } else {
		        		  itemPrice = standardPrice;
		        	  } 
	        	  } else if (mercentOrderPrefixes.isMercentOrder(origin)) {
	        		  
	        		  logger.info("Mercent Price["+itemPrice+"] doesn't match with any FTD Prices.");
	        		  String mercentProdId = this.getMercentProductIdByConfirmationNo(connection, externalOrderNumber);
		        	  logger.info("Mercent Product Id : "+mercentProdId);
		        	  if(mercentProdId==null){
		        		  mercentProdId = "";
		        	  }
		        	  mercentProdId = mercentProdId.trim().toLowerCase();
		        	  if(mercentProdId.endsWith("_deluxe")){        		  
		        		  itemPrice = deluxePrice;
		        	  } else if(mercentProdId.endsWith("_premium")){
		        		  itemPrice = premiumPrice;
		        	  } else {
		        		  itemPrice = standardPrice;
		        	  }
	        	  } else if(isPartnerOrder) {
	        		  logger.info("Partner Price[" + itemPrice + "] doesn't match with any FTD Prices.");
	        		  String partnerProdId = this.getPartnerProductId(connection, externalOrderNumber);
		        	  logger.info("Partner order item Product Id : " + partnerProdId);
		        	  if(partnerProdId == null){
		        		  partnerProdId = "";
		        	  }
		        	  partnerProdId = partnerProdId.trim().toLowerCase();
		        	  if(partnerProdId.endsWith("_B")){        		  
		        		  itemPrice = deluxePrice;
		        	  } else if(partnerProdId.endsWith("_C")){
		        		  itemPrice = premiumPrice;
		        	  } else {
		        		  itemPrice = standardPrice;
		        	  }
	        	  }
	          }
          }
          //     }
          //     else
          //     {
          //         throw new RecalculateException("Variable price must be greater than " + standardPrice + " and less than " + variablePriceMax + "[" + origAmount + "]");
          //     }
        }
        else if (detailVO.getSizeChoice().equals(SIZE_STANDARD))
          itemPrice = new BigDecimal(productVO.getStandardPrice());
        else if (detailVO.getSizeChoice().equals(SIZE_DELUXE))
          itemPrice = new BigDecimal(productVO.getDeluxePrice());
        else if (detailVO.getSizeChoice().equals(SIZE_PREMIUM))
          itemPrice = new BigDecimal(productVO.getPremiumPrice());
        else
          throw new RecalculateException("Invalid size choice:" + detailVO.getSizeChoice());
        itemPrice = itemPrice.setScale(AMOUNT_SCALE, BigDecimal.ROUND_HALF_DOWN);

        logger.info("external_order_number: " + detailVO.getExternalOrderNumber() + " and order_detail_id: " + 
                    new Long(detailVO.getOrderDetailId()).toString());
        logger.info("origAmount: " + origAmount);
        logger.info("variable price: " + productVO.getVariablePriceFlag());
        logger.info("itemPrice: " + itemPrice);

        detailVO.setProductsAmount(itemPrice.toString());

        //Retrieve the date we will use for charge/fee calculation
        Date dateForChargeCalculation = new Date();
        //if we don't need to recalculate the order and it is a product type where the fees vary,
        //then set the charge calculation time to the order date.
        if (!detailVO.isChargeRelatedItemChanged() && 
            (productVO.getProductType().equals(PRODUCT_TYPE_FRESHCUT) || productVO.getProductType().equals(PRODUCT_TYPE_SAMEDAY_FRESHCUT) || 
             productVO.getProductType().equals(PRODUCT_TYPE_FLORAL)) && order.getOrderDateTime() != null)
        {
          logger.info("Using order date for charge calculation: " + order.getOrderDateTime());
          dateForChargeCalculation = order.getOrderDateTime();
        }

        Date dateForBBNCalculation = new Date();
        if(!detailVO.isChargeRelatedItemChanged() && order.getOrderDateTime() != null){ dateForBBNCalculation = order.getOrderDateTime(); }
        //Determine if the Item qualifies for Free Shipping
        //SN: Need to change the buyerHasFreeShipping
       itemHasFreeShipping = buyerHasFreeShipping && checkItemHasFreeShipping(connection, sourceVO, productVO);
       logger.info("itemHasFreeShipping: " + itemHasFreeShipping);
       
       			//DI-15 - if FTDW product and PQuad Product id is not null and Ship Method is not SameDay/ Floral, get the charges from PAC.
				
				detailVO.setApplyOrigFee(false);
				 
				if (PACUtil.isFTDWOrder(productVO.getShippingSystem(), productVO.getProductType(), detailVO.getShipMethod())) {
					Boolean isPCOrder = false;					
					if (productVO != null && !StringUtils.isEmpty(productVO.getPersonalizationTemplate())) {
						isPCOrder = true;
					}
					
					// DI-152 - Catch the Exception (any uncaught) and set the fee to Original fee on the order. Never show 0 fee
					BigDecimal ftdWfees = new BigDecimal("0");
					try {
						ftdWfees = calcFTDWFees(detailVO, dateForBBNCalculation, productVO.getNovatorId(), itemHasFreeShipping, connection, isPCOrder).setScale(AMOUNT_SCALE, BigDecimal.ROUND_HALF_DOWN);						
					} catch(RecalculateException e) {
						logger.error(e);
						detailVO.setApplyOrigFee(true);
					}
					
					if (!(productVO.getProductType().equals(PRODUCT_TYPE_SAMEDAY_GIFT) || productVO.getProductType().equals(PRODUCT_TYPE_SPECIALTY_GIFT))) {
						lineServiceFee = ftdWfees;
					} else {
						lineShippingFee = ftdWfees;
					}
					
					logger.debug("***** FTDW fee applied, Service Fee: " + lineServiceFee.toString() + ", Shipping Fee: " + lineShippingFee.toString());
					logger.debug("***** Item Ship method: " + detailVO.getShipMethod() + ", FTDW/PAC Allowed ship method: " + detailVO.getFtdwAllowedShipping());
				} else {
					// reset the charge - LCF etc for SDFC
					detailVO.setLatecutoffCharge(null);
					detailVO.setSundayUpcharge(null);
					detailVO.setMondayUpcharge(null);
					detailVO.setSaturdayUpcharge(null);
					detailVO.setFtdwAllowedShipping(null);
					
					lineShippingFee = calcShippingFee(connection, itemPrice.doubleValue(), detailVO, productVO, dateForBBNCalculation, itemHasFreeShipping).setScale(AMOUNT_SCALE, BigDecimal.ROUND_HALF_DOWN);
					logger.info("***** Shipping Fee (lineShippingFee):" + lineShippingFee.toString());
					lineServiceFee = calcServiceFee(connection, detailVO, productVO.getProductType(), productVO.getProductSubType(), dateForChargeCalculation, itemHasFreeShipping, sourceVO);
					logger.info("***** Service Fee (lineServiceFee):" + lineServiceFee.toString());
				}
                         
        // Correct service fee for Free Shipping, track International Fee Separately
		// No changes needed here for FTDW
        // Portion of Service Fee excluded from Free Shipping
        if(detailVO.getInternationalFloristServiceCharge() != null && itemHasFreeShipping &&
           new BigDecimal(detailVO.getInternationalFloristServiceCharge()).compareTo(new BigDecimal("0")) > 0)
        {
          freeShipSavingsExcludedServiceFee = new BigDecimal(detailVO.getInternationalFloristServiceCharge());
          lineServiceFee = lineServiceFee.subtract(freeShipSavingsExcludedServiceFee);
          if(lineServiceFee.floatValue() <= 0) 
          {
            lineServiceFee = new BigDecimal(0);            
          } 
         
          // Add the domestic charge back to the line Service fee, this is what we save 
          lineServiceFee = lineServiceFee.add(new BigDecimal(detailVO.getDomesticFloristServiceCharge()));
          detailVO.setDomesticFloristServiceCharge(null);
          
          logger.info("***** INTL FEE:" + freeShipSavingsExcludedServiceFee.toString());
        }
       
         
        logger.info("***** SERV FEE:" + lineServiceFee.toString());

        // Apollo project (phase III) defect #666
        // Not effects FTDW - this is for SD
        lineShippingFee = recalculateShippingFee(connection, detailVO, productVO.getProductType(), lineShippingFee);
        logger.info("***** SHIP 2 FEE:" + lineShippingFee.toString());
        
        // 11946 - If product type is FLORAL or ship method is invalid, make BBN fee as null.
        if((detailVO.getProductType() != null && ValidationConstants.PRODUCT_TYPE_FLORAL.equals(detailVO.getProductType())) ||
        		(StringUtils.isEmpty(detailVO.getShipMethod()) || "SD".equals(detailVO.getShipMethod()) || "FL".equals(detailVO.getShipMethod()))) {			
        	detailVO.setMorningDeliveryAvailable("N");
			detailVO.setMorningDeliveryFee(null);
		}

        if (itemHasFreeShipping && "N".equals(detailVO.getBbnChargedtoFSMembers()) 
        		&& "Y".equals(detailVO.getMorningDeliveryAvailable()) 
        		&& "Y".equals(detailVO.getMorningDeliveryOpted()) ) {
			detailVO.setMorningDeliveryFee("0.00");
		}
        
        
        if(isProductType_Service_FreeShipping(productVO.getProductType(), productVO.getProductSubType())) 
        {
          logger.info("Product is Services type Free Shipping, no fuel surcharge");

        } else
        {
            if (surchargeVO.isApplySurchargeFlag())
            {
               fuelSurcharge = new BigDecimal(String.valueOf(surchargeVO.getSurchargeAmount()));// converting to string first becuase BigDecimal doesn't scale double directly and gives a big number
            }
        }

        if (!itemHasFreeShipping)
        {
        	detailVO.setFuelSurcharge(fuelSurcharge.toString());
        }

        logger.debug("fuel surcharge in recalculateOrder BO added to OrderDetailVO  : " + detailVO.getFuelSurcharge());
        detailVO.setFuelSurchargeDescription(surchargeVO.getSurchargeDescription());
        detailVO.setApplySurchargeCode(surchargeVO.getApplySurchargeCode());
        detailVO.setDisplaySurcharge(surchargeVO.getDisplaySurcharge());
        detailVO.setSendSurchargeToFlorist(surchargeVO.getSendSurchargeToFlorist());

        if (fuelSurcharge.floatValue() > 0)
        {
          String shipMethod = detailVO.getShipMethod();
          String orderOrigin = order.getOrderOrigin();
          String productType = productVO.getProductType();
          if (productType == null) productType = "";
          String sendToFloristFlag = surchargeVO.getSendSurchargeToFlorist();
          if (sendToFloristFlag == null) sendToFloristFlag = "N";
          logger.info("productType: " + productType);
          logger.info("shipMethod: " + shipMethod);
          logger.info("orderOrigin: " + orderOrigin);
          logger.info("sendToFloristFlag: " + sendToFloristFlag);

          // for CSP orders, subtract fuel surcharge from service fee and add to shipping fee
          detailVO.setOriginalServiceFeeAmount(lineServiceFee+"");
          if (orderOrigin != null && orderOrigin.equalsIgnoreCase("CSPI"))
          {
            if (shipMethod == null || shipMethod.equals(FLORIST_DELIVERED) || shipMethod.equals(SAME_DAY_DELIVERED))
            {
              logger.info("subtracting fuel surcharge from service fee");
              lineServiceFee = lineServiceFee.subtract(fuelSurcharge);
              if (lineServiceFee.floatValue() < 0)
              {
                lineServiceFee = new BigDecimal("0");
              }
              lineShippingFee = lineShippingFee.add(fuelSurcharge);
             
            }
          }
          else
          {
              if (productType.equals(PRODUCT_TYPE_FLORAL)) {
                  if (sendToFloristFlag.equals("Y")) {
                      lineShippingFee = lineShippingFee.add(fuelSurcharge);
                      logger.info("fuel surcharge added to shipping fee");
                      
                  } else {
                      lineServiceFee = lineServiceFee.add(fuelSurcharge);
                      logger.info("fuel surcharge added to service fee");
                  }
              } else if (productType.equals(PRODUCT_TYPE_SPECIALTY_GIFT)) {
                  lineShippingFee = lineShippingFee.add(fuelSurcharge);
                  logger.info("fuel surcharge added to shipping fee");
              } else if (productType.equals(PRODUCT_TYPE_FRESHCUT)) {
                  lineServiceFee = lineServiceFee.add(fuelSurcharge);
                  logger.info("fuel surcharge added to service fee");
              } else if (productType.equals(PRODUCT_TYPE_SAMEDAY_FRESHCUT)) {
                  if (shipMethod == null || shipMethod.equals("") || shipMethod.equalsIgnoreCase("SD")) {
                      if (sendToFloristFlag.equals("Y")) {
                          lineShippingFee = lineShippingFee.add(fuelSurcharge);
                          logger.info("fuel surcharge added to shipping fee");
                      } else {
                          lineServiceFee = lineServiceFee.add(fuelSurcharge);
                          logger.info("fuel surcharge added to service fee");
                      }
                  } else {
                      lineServiceFee = lineServiceFee.add(fuelSurcharge);
                      logger.info("fuel surcharge added to service fee");
                  }
              } else if (productType.equals(PRODUCT_TYPE_SAMEDAY_GIFT)) {
                  if (shipMethod == null || shipMethod.equals("") || shipMethod.equalsIgnoreCase("SD")) {
                      if (sendToFloristFlag.equals("Y")) {
                          lineShippingFee = lineShippingFee.add(fuelSurcharge);
                          logger.info("fuel surcharge added to shipping fee");
                      } else {
                          lineServiceFee = lineServiceFee.add(fuelSurcharge);
                          logger.info("fuel surcharge added to service fee");
                      }
                  } else {
                      lineShippingFee = lineShippingFee.add(fuelSurcharge);
                      logger.info("fuel surcharge added to shipping fee");
                  }
              }
          }
        }

        if (detailVO.getOriginalServiceFeeAmount() == null) detailVO.setOriginalServiceFeeAmount("0");
        if (detailVO.getOriginalShippingFeeAmount() == null) detailVO.setOriginalShippingFeeAmount("0");
        
        logger.info("***** original service fee:" + detailVO.getOriginalServiceFeeAmount());
        logger.info("***** new service fee:" + lineServiceFee.toString());
        logger.info("***** original shipping fee:" + detailVO.getOriginalShippingFeeAmount());
        logger.info("***** new shipping fee:" + lineShippingFee.toString());
        
        if ((isPartnerOrder || mercentOrderPrefixes.isMercentOrder(origin)) &&
        		(!detailVO.getOriginalServiceFeeAmount().equals(lineServiceFee.toString()) ||
        		    !detailVO.getOriginalShippingFeeAmount().equals(lineShippingFee.toString()))) {

        	logger.info("Resetting Partner service/shipping fee");
        	String newFee = "0";
        	if (!detailVO.getOriginalServiceFeeAmount().equals("0")) {
        		newFee = detailVO.getOriginalServiceFeeAmount();
        	} else {
        		newFee = detailVO.getOriginalShippingFeeAmount();
        	}
        	
        	logger.info("new partner fee: " + newFee);        	
        	//String shipMethod = detailVO.getShipMethod();
            String productType = productVO.getProductType();
            if (productType == null) productType = "";
        	
	            if (productType.equals(PRODUCT_TYPE_FLORAL)) {
	                logger.info("Partner fee added to service fee");
	                lineServiceFee = new BigDecimal(newFee);
	                lineShippingFee = new BigDecimal("0");
	            } else if (productType.equals(PRODUCT_TYPE_SPECIALTY_GIFT)) {
	                logger.info("Partner fee added to shipping fee");
	                lineShippingFee = new BigDecimal(newFee);
	                lineServiceFee = new BigDecimal("0");
	            } else if (productType.equals(PRODUCT_TYPE_FRESHCUT)) {
	                logger.info("Partner fee added to service fee");
	                lineServiceFee = new BigDecimal(newFee);
	                lineShippingFee = new BigDecimal("0");
	            } else if (productType.equals(PRODUCT_TYPE_SAMEDAY_FRESHCUT)) {
	                logger.info("Partner fee added to service fee");
	                lineServiceFee = new BigDecimal(newFee);
	                lineShippingFee = new BigDecimal("0");
	            } else if (productType.equals(PRODUCT_TYPE_SAMEDAY_GIFT)) {
	                logger.info("Partner fee added to shipping fee");
	                lineShippingFee = new BigDecimal(newFee);
	                lineServiceFee = new BigDecimal("0");
	            }
	            
            // Fix amounts for SDG products
            lineShippingFee = recalculateShippingFee(connection, detailVO, productVO.getProductType(), lineShippingFee);
            logger.info("***** partner service fee:" + lineServiceFee.toString());
            logger.info("***** partner shipping fee:" + lineShippingFee.toString());
        }
        
        // For FTDW orders apply original Fee when PAC fails or when Ship method discrepancy found
        if(detailVO.isApplyOrigFee()) {
        	BigDecimal originalFeeTotal = new BigDecimal(detailVO.getOrigTotalFeeAmount() != null ? detailVO.getOrigTotalFeeAmount() : "0");
        	if (!(productVO.getProductType().equals(PRODUCT_TYPE_SAMEDAY_GIFT) || productVO.getProductType().equals(PRODUCT_TYPE_SPECIALTY_GIFT))) {
				lineServiceFee = originalFeeTotal;
			} else {
				lineShippingFee = originalFeeTotal;
			}
        	 logger.info("***** FTDW resetting the fees to original fees " + originalFeeTotal + " = " + lineServiceFee + " + " + lineShippingFee); 
        }
        
        
        boolean sduChargedtoFSMembers = false;
        if(itemHasFreeShipping) {
        	// Existing logic for FTDE
        	if (StringUtils.isEmpty(detailVO.getFtdwAllowedShipping())) {
    			detailVO.setShippingFeeAmountSavings(lineShippingFee.toString());
    			detailVO.setServiceFeeAmountSavings(lineServiceFee.toString());
    			detailVO.setShippingFeeAmount("0");
    			detailVO.setServiceFeeAmount(freeShipSavingsExcludedServiceFee.toString());
    			
    			// BBN fee should not be added as savings.
    			if (detailVO.getMorningDeliveryFee() != null && Double.parseDouble(detailVO.getMorningDeliveryFee()) >= 0) {
    				if (ValidationConstants.PRODUCT_TYPE_FRESHCUT.equals(detailVO.getProductType())
    						|| ValidationConstants.PRODUCT_TYPE_SAMEDAY_FRESHCUT.equals(detailVO.getProductType())) {
    					
    					detailVO.setServiceFeeAmountSavings(lineServiceFee.subtract(new BigDecimal(detailVO.getMorningDeliveryFee())).toString());
    					detailVO.setServiceFeeAmount(freeShipSavingsExcludedServiceFee.add(new BigDecimal(detailVO.getMorningDeliveryFee())).toString());
    					lineServiceFee = lineServiceFee.subtract(new BigDecimal(detailVO.getMorningDeliveryFee()));
    					addServiceFeetoLineTotal = true;
    				} else {
    					detailVO.setShippingFeeAmountSavings(lineShippingFee.subtract(new BigDecimal(detailVO.getMorningDeliveryFee())).toString());
    					detailVO.setShippingFeeAmount(detailVO.getMorningDeliveryFee());

    					lineShippingFee = lineShippingFee.subtract(new BigDecimal(detailVO.getMorningDeliveryFee()));
    					addShippingFeetoLineTotal = true;
    				}
    			}
    			
    			// SDU fee should not be added as savings for Free Shipping Members.
    			if (detailVO.getSduChargedtoFSMembers() != null && detailVO.getSduChargedtoFSMembers().equalsIgnoreCase("Y") 
    					&& detailVO.getSameDayUpcharge() != null && Double.parseDouble(detailVO.getSameDayUpcharge()) >= 0) {
    					
    					detailVO.setServiceFeeAmountSavings(lineServiceFee.subtract(new BigDecimal(detailVO.getSameDayUpcharge())).toString());
    					detailVO.setServiceFeeAmount((freeShipSavingsExcludedServiceFee.add(new BigDecimal(detailVO.getSameDayUpcharge()))).toString());
    					lineServiceFee = lineServiceFee.subtract(new BigDecimal(detailVO.getSameDayUpcharge()));
    					addServiceFeetoLineTotal = true;
    					sduChargedtoFSMembers = true;

    			}
    			
    			
    		}
        	
        	// Set all additional charges to 0 for free shipping - Sunday upcharge still exists for FTDW
            detailVO.setSaturdayUpcharge("0");
            //do not zero out same day upcharge when SDU is charged to FS members
            if (!sduChargedtoFSMembers){
            	detailVO.setSameDayUpcharge("0");
        	}
            detailVO.setMondayUpcharge("0"); 
            detailVO.setLatecutoffCharge("0");
        	
        	// Item Fee and savings for FTD-West free shipping orders  
            if(!StringUtils.isEmpty(detailVO.getFtdwAllowedShipping())) { 
            	
            	BigDecimal nonSavings = new BigDecimal("0");
            	
            	// Sunday Upcharge is not savings
            	if(!StringUtils.isEmpty(detailVO.getSundayUpcharge())) {
            		nonSavings = nonSavings.add(new BigDecimal(detailVO.getSundayUpcharge()));
            	}
            	
            	// BBN FEE is not savings
            	if(!StringUtils.isEmpty(detailVO.getMorningDeliveryFee())) {
            		nonSavings = nonSavings.add(new BigDecimal(detailVO.getMorningDeliveryFee()));
            	}
            	
            	if(lineShippingFee.compareTo(BigDecimal.ZERO) > 0) { // if SPEGFT 
            		detailVO.setShippingFeeAmountSavings(lineShippingFee.subtract(nonSavings).toString()); 
            		detailVO.setServiceFeeAmountSavings("0");
            		
            		detailVO.setShippingFeeAmount(nonSavings.toString());
            		detailVO.setServiceFeeAmount("0");   
            		addShippingFeetoLineTotal = true;
            	} else { // if FC
            		detailVO.setServiceFeeAmountSavings(lineServiceFee.subtract(nonSavings).toString()); 
            		detailVO.setShippingFeeAmountSavings("0");
            		
            		detailVO.setServiceFeeAmount(nonSavings.toString());
            		detailVO.setShippingFeeAmount("0"); 
            		addServiceFeetoLineTotal = true;
            	} 
             }              
              
        } else {        	
          detailVO.setShippingFeeAmount(lineShippingFee.toString());
          detailVO.setServiceFeeAmount(lineServiceFee.toString());
          detailVO.setShippingFeeAmountSavings("0");
          detailVO.setServiceFeeAmountSavings("0");                
        }
        detailVO.setRecalculateAppliedFreeShipping(itemHasFreeShipping);        
        
        //calculate addon amounts
        List addonList = detailVO.getAddOns();
        
        //for each addon on detail line if there are any addons
        if (addonList != null)
        {
          for (int addoni = 0; addoni < addonList.size(); addoni++)
          {
            AddOnsVO addonVO = (AddOnsVO) addonList.get(addoni);
            requiredCheck(addonVO);

            // Only recalc addon price if not already in object (or price is null or 0) 
            String priceStr = addonVO.getPrice();
            if (priceStr == null)
              priceStr = "0";
            BigDecimal addOnPrice = new BigDecimal(priceStr);
            if (addOnPrice.compareTo(new BigDecimal("0")) == 0)
            {
              addOnPrice = calcAddonPrice(connection, addonVO.getAddOnCode());
            }
            BigDecimal addonTotal = addOnPrice.multiply(new BigDecimal(addonVO.getAddOnQuantity()));
            addonVO.setPrice((addOnPrice.setScale(AMOUNT_SCALE, BigDecimal.ROUND_HALF_DOWN)).toString());
            lineAddonTotal = lineAddonTotal.add(addonTotal);
          } //end addon loop
        }
        detailVO.setAddOnAmount((lineAddonTotal.setScale(AMOUNT_SCALE, BigDecimal.ROUND_HALF_DOWN)).toString());
        //set discount and reward amounts
        calcDiscounts(connection, pricingCode, sourceVO, detailVO, productVO.getDiscountAllowedFlag());
        //Ivan Vojinovic 09/13/2004 Upromise
        //calcRewards(pricingCode,partnerIdCode,detailVO,discountAllowedFlag);
        discountAmount = new BigDecimal(detailVO.getDiscountAmount());
        
        // calculate Tax
        logger.info("Calculate Tax Flag: " + calculateTaxFlag);
        
        if (calculateTaxFlag) {
            CalculateTaxUtil taxUtil = new CalculateTaxUtil();
            taxUtil.calculateItemTax(detailVO, productVO, order.getCompanyId());
            taxAmount = new BigDecimal(detailVO.getTaxAmount());
        }
      
		if (logger.isDebugEnabled()) {
			logger.debug("recalculate() - externalOrderNumber = " + detailVO.getExternalOrderNumber() + " - taxAmount = " + detailVO.getTaxAmount().toString());
		}
				
       
        String originalTaxAmount = (String) originalTaxAmounts.get(detailVO.getLineNumber());
        String originalShippingTaxAmount = detailVO.getShippingTax();
        logger.debug("original tax: " + originalTaxAmount);
        logger.debug("original shipping tax: " + originalShippingTaxAmount);
        logger.debug("new tax: " + taxAmount.toString());
        
        if (origin != null && (origin.equalsIgnoreCase("AMZNI") || mercentOrderPrefixes.isMercentOrder(origin) || isPartnerOrder) 
        		&& detailVO.getShippingTax() != null &&	!detailVO.getShippingTax().equals("0")) {
        	
        	BigDecimal oldTax = new BigDecimal(originalTaxAmount);
        	BigDecimal oldShippingTax = new BigDecimal(originalShippingTaxAmount);
        	BigDecimal oldTotal = oldTax.add(oldShippingTax);
        	BigDecimal oldPercentage = oldShippingTax.divide(oldTotal, 2, BigDecimal.ROUND_HALF_UP);
        	logger.debug("old total: " + oldTotal.toString() + " oldShippingPercentage: " + oldPercentage.toString());
        	
        	shippingTaxAmount = oldPercentage.multiply(taxAmount);
        	shippingTaxAmount = shippingTaxAmount.setScale(AMOUNT_SCALE, BigDecimal.ROUND_DOWN);
            taxAmount = taxAmount.subtract(shippingTaxAmount);
            taxAmount = taxAmount.setScale(AMOUNT_SCALE, BigDecimal.ROUND_DOWN);
        	logger.debug("taxAmount: " + taxAmount.toString() + " shippingTaxAmount: " + shippingTaxAmount.toString());
        }
        detailVO.setTaxAmount(taxAmount.toString());
        detailVO.setShippingTax(shippingTaxAmount.toString());
        
        //calculate line total
        lineTotal = lineTotal.add(itemPrice);
        lineTotal = lineTotal.subtract(discountAmount);
        lineTotal = lineTotal.add(lineAddonTotal);
        lineTotal = lineTotal.add(taxAmount);
        lineTotal = lineTotal.add(shippingTaxAmount);
        
        if(!itemHasFreeShipping)
        {
          lineTotal = lineTotal.add(lineServiceFee);
          lineTotal = lineTotal.add(lineShippingFee);
        } else
        {
          lineTotal = lineTotal.add(freeShipSavingsExcludedServiceFee);
          //item has free shipping. add same day upcharge if there
          freeShipSavingsExcludedServiceFee = freeShipSavingsExcludedServiceFee.add(new BigDecimal(detailVO.getSameDayUpcharge()));
        }
        // No need of changes here for FTDW - the below booleans are set for FS
        if(addServiceFeetoLineTotal){
        	lineTotal = lineTotal.add(new BigDecimal(detailVO.getServiceFeeAmount()));
        }
        if(addShippingFeetoLineTotal){
        	lineTotal = lineTotal.add(new BigDecimal(detailVO.getShippingFeeAmount()));
        }
        
        }
        else{
        	
        	logger.info("Recalculation skipped for PDP item : "+detailVO.getExternalOrderNumber());
        	
        	if (detailVO.getDiscountAmount() == null) detailVO.setDiscountAmount("0");
            if (detailVO.getTaxAmount() == null) detailVO.setTaxAmount("0");
            if (detailVO.getOriginalServiceFeeAmount() == null) detailVO.setOriginalServiceFeeAmount("0");
            if (detailVO.getOriginalShippingFeeAmount() == null) detailVO.setOriginalShippingFeeAmount("0");
            if (detailVO.getShippingTax() == null) detailVO.setShippingTax("0");
            
          //calculate addon amounts
            List addonList = detailVO.getAddOns();
            BigDecimal addonTotal = new BigDecimal("0");
            
            //for each addon on detail line if there are any addons
            if (addonList != null)
            {
              for (int addoni = 0; addoni < addonList.size(); addoni++)
              {
                AddOnsVO addonVO = (AddOnsVO) addonList.get(addoni);
                requiredCheck(addonVO);

                // Only recalc addon price if not already in object (or price is null or 0) 
                String priceStr = addonVO.getPrice();
                if (priceStr == null)
                  priceStr = "0";
                BigDecimal addOnPrice = new BigDecimal(priceStr);
                addonTotal = addOnPrice.multiply(new BigDecimal(addonVO.getAddOnQuantity()));
                addonVO.setPrice((addOnPrice.setScale(AMOUNT_SCALE, BigDecimal.ROUND_HALF_DOWN)).toString());
                
              } //end addon loop
            }
            
            surchargeVO = FTDFuelSurchargeUtilities.getSurchargeVO(cal,sourceCode,connection );
        	
        	itemPrice = new BigDecimal((String) originalProductAmounts.get(detailVO.getLineNumber()));
        	//discountAmount = new BigDecimal(detailVO.getDiscountAmount());
        	lineAddonTotal = lineAddonTotal.add(addonTotal);
        	taxAmount = new BigDecimal((String) originalTaxAmounts.get(detailVO.getLineNumber()));
        	shippingTaxAmount = new BigDecimal(detailVO.getShippingTax());
        	lineServiceFee = new BigDecimal(detailVO.getOriginalServiceFeeAmount());
        	lineShippingFee = new BigDecimal(detailVO.getOriginalShippingFeeAmount());
        	
        	//setting item level values into the object
            detailVO.setAddOnAmount(lineAddonTotal.toString());
            detailVO.setDiscountAmount(discountAmount.toString());
            detailVO.setProductsAmount(itemPrice.toString());
            
            calcDiscounts(connection, pricingCode, sourceVO, detailVO, productVO.getDiscountAllowedFlag());// This only to get the Discount amount based on Source Code
            discountAmount = new BigDecimal(detailVO.getDiscountAmount());
            
            
            detailVO.setOriginalServiceFeeAmount(lineServiceFee.toString());
            detailVO.setServiceFeeAmount(lineServiceFee.toString());
            detailVO.setOriginalShippingFeeAmount(lineShippingFee.toString());
            detailVO.setShippingFeeAmount(lineShippingFee.toString());
            detailVO.setTaxAmount(taxAmount.toString());
            
            logger.info("itemPrice for PDP item : "+itemPrice);
            logger.info("discountAmount for PDP item : "+discountAmount);
            logger.info("taxAmount for PDP item : "+taxAmount);
            logger.info("shippingTaxAmount for PDP item : "+shippingTaxAmount);
            logger.info("lineServiceFee for PDP item : "+lineServiceFee);
            logger.info("lineShippingFee for PDP item : "+lineShippingFee);
            
            lineTotal = lineTotal.add(itemPrice);
            lineTotal = lineTotal.subtract(discountAmount);
            lineTotal = lineTotal.add(lineAddonTotal);
            lineTotal = lineTotal.add(taxAmount);
            lineTotal = lineTotal.add(shippingTaxAmount);
            lineTotal = lineTotal.add(lineServiceFee);
            lineTotal = lineTotal.add(lineShippingFee);
           
            
        }
        
        logger.info("The line total is : "+lineTotal);
        
        lineTotal = lineTotal.setScale(AMOUNT_SCALE, BigDecimal.ROUND_HALF_DOWN);
        detailVO.setExternalOrderTotal(lineTotal.toString());
        
        //Ivan Vojinovic 09/13/2004 Upromise - moved this down here because
        // new calcRewards code needs ability to work with order total
        calcRewards(connection, sourceVO, detailVO);
        BigDecimal pointAmount = new BigDecimal(detailVO.getMilesPoints());


        //increment order level totals
        orderTotal = orderTotal.add(lineTotal);
        orderProductAmount = orderProductAmount.add(itemPrice);
        orderTaxAmount = orderTaxAmount.add(taxAmount);
        orderTaxAmount = orderTaxAmount.add(shippingTaxAmount);
        
        if(itemHasFreeShipping)
        {
			if (StringUtils.isEmpty(detailVO.getFtdwAllowedShipping())) {
				orderServiceFeeSavings = orderServiceFeeSavings.add(lineServiceFee);
				orderShippingFeeSavings = orderShippingFeeSavings.add(lineShippingFee);
				orderServiceFee = orderServiceFee.add(freeShipSavingsExcludedServiceFee);
			} else {
				if(!StringUtils.isEmpty(detailVO.getShippingFeeAmountSavings())) {
					orderShippingFeeSavings = orderShippingFeeSavings.add(new BigDecimal(detailVO.getShippingFeeAmountSavings()));					
				}
				if(!StringUtils.isEmpty(detailVO.getShippingFeeAmount())) {
					orderShippingFee = orderShippingFee.add(new BigDecimal(detailVO.getShippingFeeAmount()));
				}
				if(!StringUtils.isEmpty(detailVO.getServiceFeeAmountSavings())) {
					orderServiceFeeSavings = orderServiceFeeSavings.add(new BigDecimal(detailVO.getServiceFeeAmountSavings()));					
				}
				if(!StringUtils.isEmpty(detailVO.getServiceFeeAmount())) {
					orderServiceFee = orderServiceFee.add(new BigDecimal(detailVO.getServiceFeeAmount()));
				}
			}
          
        } else
        {
          orderServiceFee = orderServiceFee.add(lineServiceFee);
          orderShippingFee = orderShippingFee.add(lineShippingFee);
          orderSurchargeFee = orderSurchargeFee.add(fuelSurcharge);
        }
        
        orderAddonAmount = orderAddonAmount.add(lineAddonTotal);
        orderDiscountTotal = orderDiscountTotal.add(discountAmount);
        orderPoints = orderPoints.add(pointAmount);
      } //end try
      catch (RecalculateException boe)
      {
        if (lineException == null)
        {
          lineException = boe;
        }
      }
      catch (Throwable t)
      {
        if (lineException == null)
        {
          lineException = new RecalculateException("Could not calculate product price.", t);
          logger.error(t);
        }
      }

    } //end for each item
    
    

    // Set order total
    order.setOrderTotal((orderTotal.setScale(AMOUNT_SCALE, BigDecimal.ROUND_HALF_DOWN)).toString());
    //order.setProductsTotal((orderProductAmount.setScale(AMOUNT_SCALE, BigDecimal.ROUND_HALF_DOWN)).toString());
    order.setTaxTotal((orderTaxAmount.setScale(AMOUNT_SCALE, BigDecimal.ROUND_HALF_DOWN)).toString());
    order.setServiceFeeTotal((orderServiceFee.setScale(AMOUNT_SCALE, BigDecimal.ROUND_HALF_DOWN)).toString());
    order.setShippingFeeTotal((orderShippingFee.setScale(AMOUNT_SCALE, BigDecimal.ROUND_HALF_DOWN)).toString());
    order.setAddOnAmountTotal((orderAddonAmount.setScale(AMOUNT_SCALE, BigDecimal.ROUND_HALF_DOWN)).toString());
    order.setDiscountTotal((orderDiscountTotal.setScale(AMOUNT_SCALE, BigDecimal.ROUND_HALF_DOWN)).toString());
    order.setPartnershipBonusPoints((orderPoints.setScale(AMOUNT_SCALE, BigDecimal.ROUND_HALF_DOWN)).toString());
    
    logger.info("Final order total : "+order.getOrderTotal());
    logger.info("Final Tax total : "+order.getTaxTotal());
    logger.info("Final Service Fee total : "+order.getServiceFeeTotal());
    logger.info("Final Shiping Fee total : "+order.getShippingFeeTotal());
    logger.info("Final Discount total : "+order.getDiscountTotal());
    logger.info("Final Addon Amount total : "+order.getAddOnAmountTotal());
    logger.info("Final Partnership Bonus points total : "+order.getPartnershipBonusPoints());

    if (surchargeVO == null)
    {
      logger.info("SurchargVO didn't get initialized in try catch above.  This point, we only need the ApplySurchargeCode and SurchargeDescription so initializg to default values.");
      surchargeVO = new SurchargeVO();
    }

    order.setApplySurchargeCode(surchargeVO.getApplySurchargeCode());
    order.setFuelSurchargeDescription(surchargeVO.getSurchargeDescription());    
    order.setFuelSurchargeFee(String.valueOf(orderSurchargeFee));
    order.setServiceFeeTotalSavings((orderServiceFeeSavings.setScale(AMOUNT_SCALE, BigDecimal.ROUND_HALF_DOWN)).toString());
    order.setShippingFeeTotalSavings((orderShippingFeeSavings.setScale(AMOUNT_SCALE, BigDecimal.ROUND_HALF_DOWN)).toString());
    
    // START - Per Phase 3 defects 831 and 1065
    ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
    String paymentTypeGC = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "PAYMENT_TYPE_GIFT_CERT");
    String paymentTypeGD = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "PAYMENT_TYPE_GIFT_CARD");

    PaymentsVO payment = null;
    PaymentsVO primaryPayment = null;
    double gcPaymentAmount = 0;
    if (order.getPayments() != null)
    {
      Iterator paymentIterator = order.getPayments().iterator();

      // Sum all gift card amounts
      while (paymentIterator.hasNext())
      {
        payment = (PaymentsVO) paymentIterator.next();
        logger.info("SET PAYMENT AMOUNT:" + payment.getPaymentMethodType());
        if (payment.getPaymentMethodType() != null && payment.getPaymentMethodType().equals(paymentTypeGC))
        {
          logger.info("SET PAYMENT AMOUNT - GC AMOUNT FOUND:" + payment.getAmount());
          gcPaymentAmount = 
              gcPaymentAmount += new Double(payment.getAmount() != null? payment.getAmount(): "0").doubleValue();
        }
        else if(payment.getPaymentMethodType() != null && payment.getPaymentMethodType().equals(paymentTypeGD))
        {
        	logger.info("SET PAYMENT AMOUNT - GD AMOUNT FOUND:" + payment.getAmount());
            gcPaymentAmount = 
                gcPaymentAmount += new Double(payment.getAmount() != null? payment.getAmount(): "0").doubleValue();
        }
        else
        {
          logger.info("SET PAYMENT AMOUNT - PRIMARY PAYMENT:" + payment.getAmount());
          primaryPayment = payment;
        }
      }

      // Set payment amount to order total --- subtract giftCard total from primary payment (assumes only one non gc payment can exist on an order)
      double paymentAmount = 0;
      if(order.getOrderTotal() != null && new Double(order.getOrderTotal()).doubleValue() > 0)
      {
	      paymentAmount = 
	        new Double(order.getOrderTotal() != null? order.getOrderTotal(): "0").doubleValue() - gcPaymentAmount;
	      logger.info("SET PAYMENT AMOUNT - SET AMOUNT:" + paymentAmount);
	      if (primaryPayment != null)
	      {
	        primaryPayment.setAmount(paymentAmount >= 0? 
	                                 new BigDecimal(paymentAmount).setScale(2, BigDecimal.ROUND_HALF_DOWN).toString(): "0");
	      }
      }
      // Recalc Miles/Points payment amounts (if present)
      if ((primaryPayment != null) && (primaryPayment.getMilesPointsAmt() != null) && 
          (!primaryPayment.getMilesPointsAmt().equals("")))
      {
        try
        {
          logger.info("Recalculating miles/points payment amounts");
          String primaryPayMethodType = primaryPayment.getPaymentType();
          PaymentMethodMilesPointsVO mpvo = recalculateOrderDAO.getPaymentMethodMilesPoints(connection, primaryPayMethodType);

          int milesPoints, milesPointsProduct, milesPointsAddon;
          if (mpvo != null)
          {

            // Recalc for payment record
            if (FTDCommonUtils.isPaymentMilesPoints(primaryPayment.getPaymentType()))
            {
              milesPoints = 
                  FTDCommonUtils.convertDollarsToMilesPoints(primaryPayment.getAmount(), order.getMpRedemptionRateAmt(), 
                                                             mpvo.getDollarToMpOperator(), mpvo.getRoundingScaleQty(), 
                                                             mpvo.getRoundingMethodId());
              primaryPayment.setMilesPointsAmt(Integer.toString(milesPoints));
            }
            else
            {
              // Payment type must have been switched to non-miles/points method so set miles/points to null
              primaryPayment.setMilesPointsAmt(null);
            }

            // Recalc for each order_details record
            for (int i = 0; i < lineItems.size(); i++)
            {
              OrderDetailsVO detailVO = (OrderDetailsVO) lineItems.get(i);
              if (FTDCommonUtils.isPaymentMilesPoints(primaryPayment.getPaymentType()))
              {
                milesPoints = 
                    FTDCommonUtils.convertDollarsToMilesPoints(detailVO.getExternalOrderTotal(), order.getMpRedemptionRateAmt(), 
                                                               mpvo.getDollarToMpOperator(), mpvo.getRoundingScaleQty(), 
                                                               mpvo.getRoundingMethodId());
                milesPointsProduct = 
                    FTDCommonUtils.convertDollarsToMilesPoints(detailVO.getProductsAmount(), order.getMpRedemptionRateAmt(), 
                                                               mpvo.getDollarToMpOperator(), mpvo.getRoundingScaleQty(), 
                                                               mpvo.getRoundingMethodId());
                milesPointsAddon = 
                    FTDCommonUtils.convertDollarsToMilesPoints(detailVO.getAddOnAmount(), order.getMpRedemptionRateAmt(), 
                                                               mpvo.getDollarToMpOperator(), mpvo.getRoundingScaleQty(), 
                                                               mpvo.getRoundingMethodId());

                detailVO.setMilesPointsAmt(Integer.toString(milesPoints));
                detailVO.setMilesPointsAmtProduct(Integer.toString(milesPointsProduct));
                detailVO.setMilesPointsAmtAddon(Integer.toString(milesPointsAddon));
              }
              else
              {
                // Payment type must have been switched to non-miles/points method so set miles/points to null
                detailVO.setMilesPointsAmt(null);
                detailVO.setMilesPointsAmtProduct(null);
                detailVO.setMilesPointsAmtAddon(null);
              }
            }
          }
          else
          {
            logger.error("No entry found in payment_method_miles_points for payment method: " + primaryPayMethodType);
          }
        }
        catch (Exception e)
        {
          logger.error(e);
          throw new RecalculateException(e.getMessage());
        }
      }
    }
    // END - Per Phase 3 defects 831 and 1065

    //if an exception occurred at the line level throw it
    if (lineException != null)
    {
      throw lineException;
    }

    //calling objects need zero amounts to be null
    //fixUpAmounts(order);
    //-This is not being done because price values in VO cannot be null
    logger.info("END Order recalculate");

  }

/* Add discount information to order VO.
   * @param String pricing code
   * @param double non-discounted price
   * @return double discounted price*/

  private void calcDiscounts(Connection connection, String pricingCode, RecalculateOrderSourceCodeVO sourceCodeVO, OrderDetailsVO detailVO, 
                             String discountFlag)
  {

    BigDecimal productPrice = new BigDecimal(detailVO.getProductsAmount());

    //zero out discount amount
    detailVO.setDiscountAmount("0");

    //zero out miles
    detailVO.setMilesPoints("0");
    //if no price code, then no discount
    if (pricingCode == null)
    {
      return;
    }
    //Check if the PRICE_HEADER table should be used for discount information
    if (!pricingCode.equals(IGNORE_PRICE_CODE_VALUE))
    {

      //Use pricing code value and price_header table to determine discount
      //or reward amount

      BigDecimal hundreths = new BigDecimal(0.01);

      //retrieve price header information
      RecalculateOrderPriceHeaderDetailsVO priceHeaderDetailsVO = 
        recalculateOrderDAO.getPriceHeaderDetails(connection, pricingCode, detailVO.getProductsAmount());
      if (priceHeaderDetailsVO == null)
      {
        //no discounts - setDiscountedProductPrice to normal prod amount
        detailVO.setDiscountedProductPrice(new BigDecimal(detailVO.getProductsAmount()).setScale(2, 5).toString());
        return;
      }
      String discountType = priceHeaderDetailsVO.getDiscountType();
      BigDecimal discountAmount = priceHeaderDetailsVO.getDiscountAmount();

      //store discount type if a discount exist (amount > 0)
      if (discountAmount.doubleValue() > 0)
      {
        detailVO.setDiscountType(discountType);
      }

      //what kind of discount do we have?
      if (discountType.equals(DISCOUNT_TYPE_MILES))
      {
        detailVO.setMilesPoints(discountAmount.toString());
      }
      else if (discountType.equals(DISCOUNT_TYPE_DOLLARS))
      {
        detailVO.setDiscountAmount(discountAmount.setScale(AMOUNT_SCALE, BigDecimal.ROUND_HALF_DOWN).toString());
      }
      else if (discountType.equals(DISCOUNT_TYPE_PERCENT))
      {
        //!! Note discounts are rounded down
        BigDecimal amountOff = productPrice.multiply(discountAmount).multiply(hundreths);
        amountOff = amountOff.setScale(AMOUNT_SCALE, BigDecimal.ROUND_DOWN);
        detailVO.setDiscountAmount(amountOff.toString());
        detailVO.setPercentOff(discountAmount.toString());
      }
    } //end if using price header table
    else
    {
      RecalculateOrderMilesPointsVO milesPointsVO = calcRewards(sourceCodeVO, new BigDecimal(detailVO.getProductsAmount()), new BigDecimal(detailVO.getExternalOrderTotal())); 
      //zero out miles
      detailVO.setMilesPoints(milesPointsVO.getMilesPoints());
      detailVO.setRewardType(milesPointsVO.getRewardType());

      if (logger.isDebugEnabled())
      {
        logger.debug("rewardType: " + milesPointsVO.getRewardType() + " - value: " + milesPointsVO.getMilesPoints());
      }
    }

    /* If product is not allowed to have discounts, remove discount from VO, but
     * leave the rewards.*/
    if (discountFlag.equals(DISCOUNTS_NOT_ALLOWED))
    {
      detailVO.setDiscountAmount("0");
    }

    //set discounted price
    BigDecimal discountedPrice = 
      (new BigDecimal(detailVO.getProductsAmount())).subtract(new BigDecimal(detailVO.getDiscountAmount()));
    detailVO.setDiscountedProductPrice(discountedPrice.setScale(2, 5).toString());
  }

  
  /* Calc Service Fee */
  /**
   * Wraps the Implementation for Service Fee Calculation to check for the exception model.
   * Typically, this is related to data/setup issues.
   */
  private BigDecimal calcServiceFee(Connection connection, OrderDetailsVO detailVO, String productType, String productSubType, 
                                    Date serviceFeeDate, boolean itemHasFreeShipping, RecalculateOrderSourceCodeVO sourceVO)
    throws Exception
  {
      try 
      {
        return calcServiceFeeImpl(connection, detailVO, productType, productSubType, serviceFeeDate, itemHasFreeShipping, sourceVO);
      } catch (Exception ex)
      {
        if(frontEndExceptionMode)
        {
          logger.error("Failed to calculate service fee: " + ex.getMessage(), ex);
          return new BigDecimal(0);          
        }
        
        throw ex;
      }
  }
  
  /**
   * Implementation for Service Fee Calculation
   */
  private BigDecimal calcServiceFeeImpl(Connection connection, OrderDetailsVO detailVO, String productType, String productSubType, 
                                    Date serviceFeeDate, boolean itemHasFreeShipping, RecalculateOrderSourceCodeVO sourceVO)
    throws IOException, SAXException, ParserConfigurationException, SQLException, RecalculateException, ParseException, 
           Exception
  {
    // boolean treatAsFloralOverride = false;
    BigDecimal serviceFee = new BigDecimal(0);
    String shipMethod = detailVO.getShipMethod();
    String sourceCode = detailVO.getSourceCode();
    
    // Project 8276 - Defect 10499
    String originalOrderHasSDU = detailVO.getOriginalOrderHasSDU();
    logger.info("originalOrderHasSDU: " + originalOrderHasSDU);

    logger.info("**** SDG PT:" + productType);
    logger.info("**** SDG SM:" + shipMethod);


    if (isProductType_Service_FreeShipping(productType, productSubType))
    {
      return serviceFee;
    }

    SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
    Date deliveryDate = null;

    deliveryDate = sdf.parse(detailVO.getDeliveryDate());
    ServiceChargeVO serviceChargeVO = 
      recalculateOrderDAO.getServiceChargeData(connection, serviceFeeDate, sourceCode, deliveryDate);


    /* Apollo project (phase III) defect #666
     Set service fee for SDG products...
     For same day products with same day delivery always set the serv fee to
     global parm value and override the src code value */
    if (productType.equals(PRODUCT_TYPE_SAMEDAY_GIFT) && 
        (shipMethod == null || shipMethod.equals("") || shipMethod.equals(FLORIST_DELIVERED) || 
         shipMethod.equals(SAME_DAY_DELIVERED)))
    {
      //Florist delivered same day gift
      logger.info("**** SDG APPLY SERVICE FEE");
      // treatAsFloralOverride = true;

      logger.info("** Performing calculation for SDG shipping fee");
      serviceFee = recalculateOrderDAO.getSameDayGiftGlobalServiceFee(connection);

      logger.info("** Overriding source service fee with global parm serv fee for SDG product");
      logger.info("** Cache loaded for global parms SDG service fee:" + serviceFee);
      detailVO.setVendorCharge(serviceFee.toString());
      logger.debug("vendorCharge: " + detailVO.getVendorCharge());

    }
    else if (productType.equals(PRODUCT_TYPE_FRESHCUT) || productType.equals(PRODUCT_TYPE_SAMEDAY_FRESHCUT) || 
             productType.equals(PRODUCT_TYPE_FLORAL))

    {
      if (shipMethod == null || shipMethod.equals("") || shipMethod.equals(FLORIST_DELIVERED) || 
          shipMethod.equals(SAME_DAY_DELIVERED))
      {
        //get service fee from serviceChargeVO
        BigDecimal domesticFee = null;
        BigDecimal intlFee = null;
        BigDecimal sameDayUpchargeFee = null;
        boolean isDomestic = false;
        BigDecimal sameDayUpchargeFSFee = null;

        domesticFee = serviceChargeVO.getDomesticCharge();
        intlFee = serviceChargeVO.getInternationalCharge();
        sameDayUpchargeFee = serviceChargeVO.getSameDayUpcharge();
        sameDayUpchargeFSFee = serviceChargeVO.getSameDayUpchargeFS();
        
        logger.info("****** Domestic Service Fee: " +domesticFee.toString());
        logger.info("****** International Service Fee: " +intlFee.toString());
        logger.info("****** Same Day Upcharge Fee: " +sameDayUpchargeFee.toString());
        logger.info("****** Same Day Upcharge FS Fee: " +sameDayUpchargeFSFee.toString());
        
        //get the recipient country
        RecipientAddressesVO address = getRecipientAddress(detailVO);
        requiredCheck(address);
        isDomestic = recalculateOrderDAO.isCountryDomestic(connection, address.getCountry());
        if (address == null || isDomestic)
        {
          //set fee as domestic
          serviceFee = domesticFee;
          detailVO.setDomesticFloristServiceCharge(domesticFee.toString());
          logger.debug("domestic: " + detailVO.getDomesticFloristServiceCharge());
          
          //same day upcharge
          TimeZone tz = null;
          tz = recalculateOrderDAO.getRecipientTimeZone(address.getPostalCode(), address.getStateProvince(), connection);
          Calendar cal = Calendar.getInstance(tz);
          boolean addsduFlag = false;
                   
          String recTime = cal.get(Calendar.HOUR_OF_DAY)+""+cal.get(Calendar.MINUTE)+""+cal.get(Calendar.SECOND);
          int recipientTime = Integer.parseInt(recTime); // current time in recipient's timezone
          int startTime = Integer.parseInt("000001");  //12:00:01 am
          logger.debug("###SDU to serviceFee - TimeZone ### "+tz);
          logger.debug("###SDU to serviceFee - startTime ### "+startTime);
          logger.debug("###SDU to serviceFee - recipientTime ### "+recipientTime);
          
          Date dDate = null;
          Date cDate = null;
          dDate = sdf.parse(detailVO.getDeliveryDate()); //delivery date
          cDate = sdf.parse(sdf.format(cal.getTime())); //current date based on recipeint's timezone
          //logger.debug("###SDU to serviceFee - delivery Date ### "+dDate);
          //logger.debug("###SDU to serviceFee - current Date ### "+cDate);
          logger.debug("###SDU to serviceFee - isDomestic ### "+isDomestic);
          //logger.debug("###SDU to serviceFee - recipientTime>startTime ### "+(recipientTime>startTime));
          logger.debug("###SDU to serviceFee - !itemHasFreeShipping ### "+ (!itemHasFreeShipping));
          logger.debug("###SDU to serviceFee - dDate.compareTo(cDate)==0 ### "+(dDate.compareTo(cDate)==0));
          
          if(recipientTime>=startTime && dDate.compareTo(cDate)==0 && isDomestic){
          	addsduFlag = true;
          	if (detailVO.getDeliveryDateRangeEnd() != null && detailVO.getDeliveryDateRangeEnd() != "") {
          		Date dDateRangeEnd = sdf.parse(detailVO.getDeliveryDateRangeEnd());
          		if(dDate.compareTo(dDateRangeEnd)==0){
          			addsduFlag = true;
          		}
          		else{
          			addsduFlag = false;
          		}
          	}
          }
          
          logger.info("detailVO.getOriginalOrderHasSDU(): " + detailVO.getOriginalOrderHasSDU());
          logger.info("detailVO.getSameDayUpcharge(): " + detailVO.getSameDayUpcharge());
          logger.info("detailVO.isChargeRelatedItemChanged(): " +detailVO.isChargeRelatedItemChanged());
          
          if(detailVO.getOriginalOrderHasSDU() != null && detailVO.getOriginalOrderHasSDU().equals("N"))
          {
        	  addsduFlag = false;
          }
          
          //check to see if original order has sdu = 'Y' and nothing on the order has changed that would cause the order to recalculate
          //retrieve the same day upcharge fee charged on the order and add it to the current service fee
          if (detailVO.getOriginalOrderHasSDU() != null && detailVO.getOriginalOrderHasSDU().equals("Y") && !detailVO.isChargeRelatedItemChanged()){
            if (detailVO.getSameDayUpcharge() != null && !detailVO.getSameDayUpcharge().equals("")){ 
        	  serviceFee = serviceFee.add(new BigDecimal(detailVO.getSameDayUpcharge()));
              logger.info("Adding SDU to serviceFee - "+ serviceFee.toString());
              if(detailVO.getOriginalOrderHasSDUFS() != null && detailVO.getOriginalOrderHasSDUFS().equalsIgnoreCase("Y")){
            	  detailVO.setSduChargedtoFSMembers("Y");
            	  logger.info("setting sdu charged to FS members to true");
              }
              addsduFlag = false;
            }
          }
                    
          if(addsduFlag){ 
        	  String sduFlag = null; //same day upcharge flag
        	  String sduFSFlag = null; //same day upcharge free shipping flag
              sduFlag = sourceVO.getSameDayUpcharge();
              sduFSFlag = sourceVO.getSameDayUpchargeFS();
              String gpSdu = null;
              String gpSduFS = null;
              GlobalParmHandler parmHandler = (GlobalParmHandler)CacheManager.getInstance().getHandler("CACHE_NAME_GLOBAL_PARM");
              if(parmHandler != null)
              {
            	  gpSdu = parmHandler.getGlobalParm("FTDAPPS_PARMS", "SAME_DAY_UPCHARGE");
            	  gpSduFS = parmHandler.getGlobalParm("FTDAPPS_PARMS", "SAME_DAY_UPCHARGE_FS_MEMBERS");
              }

              logger.info("sduFlag: " + sduFlag);
              logger.info("sduFSFlag: " + sduFSFlag);
              logger.info("itemHasFreeShipping: " + itemHasFreeShipping);
              logger.info("gpSdu: " + gpSdu);
              logger.info("gpSduFS: " + gpSduFS);
              
              if(itemHasFreeShipping){
            	  if(sduFSFlag.equalsIgnoreCase("Y") || ( sduFSFlag.equalsIgnoreCase("D") && gpSduFS.equalsIgnoreCase("Y"))){
            		  serviceFee = serviceFee.add(sameDayUpchargeFSFee);
            		  detailVO.setSameDayUpcharge(sameDayUpchargeFSFee.toString());
            		  detailVO.setSduChargedtoFSMembers("Y");
            		  logger.info("###Source Code Parameter adding SDUFS to serviceFee### "+ sameDayUpchargeFSFee.toString());
            		  
                      if(originalOrderHasSDU == null){
                    	  detailVO.setOriginalOrderHasSDU("Y");
                      }	
            	  }
            	  else if(sduFlag.equalsIgnoreCase("Y") || ( sduFlag.equalsIgnoreCase("D") && gpSdu.equalsIgnoreCase("Y" ))){
            		  serviceFee = serviceFee.add(sameDayUpchargeFee);
            		  detailVO.setSameDayUpcharge(sameDayUpchargeFSFee.toString());
            		  logger.info("###Source Code Parameter adding SDU to serviceFee### "+ sameDayUpchargeFee.toString());
            		  if(originalOrderHasSDU == null){
                    	  detailVO.setOriginalOrderHasSDU("Y");
                      }	
            	  }
              }
              else if(!itemHasFreeShipping){
            	  if(sduFlag.equalsIgnoreCase("Y") || ( sduFlag.equalsIgnoreCase("D") && gpSdu.equalsIgnoreCase("Y"))){
            		    serviceFee = serviceFee.add(sameDayUpchargeFee);
                    	detailVO.setSameDayUpcharge(sameDayUpchargeFee.toString());
	                    logger.info("###Source Code Parameter adding SDU to serviceFee### "+ sameDayUpchargeFee);
	                                 
	                    if(originalOrderHasSDU == null){
	                    	detailVO.setOriginalOrderHasSDU("Y");
	                    }	
	              }              
              }                
              
          } //end of same day upcharge
        }
        else
        {
          //intl fee
          if(itemHasFreeShipping)
          {
            // International Fee for FS = International Fee - Domestic Fee
            intlFee = intlFee.subtract(domesticFee);
            if(intlFee.floatValue() <= 0)
            {
              intlFee = new BigDecimal("0.00");            
            }
            detailVO.setDomesticFloristServiceCharge(domesticFee.toString());          
            logger.debug("** International Fee for Free Shipping Item: " +intlFee.toString());
          }          

          serviceFee = intlFee;
          detailVO.setInternationalFloristServiceCharge(intlFee.toString());
          logger.debug("international: " + detailVO.getInternationalFloristServiceCharge());
        } //end if address empty or is domestic
              
      }
      else
      {
        //Order is vendor delivered
        serviceFee = serviceChargeVO.getVendorCharge();
        detailVO.setVendorCharge(serviceFee.toString());
        logger.debug("vendorCharge: " + detailVO.getVendorCharge());

        //if Saturday delivery, add additional charge
        if (shipMethod != null && shipMethod.equals(SATURDAY_DELIVERED))
        {
          serviceFee = serviceFee.add(serviceChargeVO.getVendorSatUpcharge());
          detailVO.setSaturdayUpcharge(serviceChargeVO.getVendorSatUpcharge().toString());
          logger.debug("saturdayCharge: " + detailVO.getSaturdayUpcharge());
        }

        RecipientAddressesVO address = getRecipientAddress(detailVO);
        //if sending to ALASKA or HAWAII an extra charge is applied
        if (address.getStateProvince().equals(HAWAII) || address.getStateProvince().equals(ALASKA))
        {
          //add additional shipping charge
          //Only add charge if carrier delivered Defect#489
          serviceFee = serviceFee.add(recalculateOrderGlobalParmsVO.getSpecialServiceCharge());
          detailVO.setAlaskaHawaiiSurcharge(recalculateOrderGlobalParmsVO.getSpecialServiceCharge().toString());
          logger.debug("AK/HI: " + detailVO.getAlaskaHawaiiSurcharge());
        }
        
        // #11946 - BBN - Calculate BBN fee For vendor delivery products.
        calculateBBN(itemHasFreeShipping, connection, serviceFeeDate, detailVO, false);
        if(!StringUtils.isEmpty(detailVO.getMorningDeliveryFee())) {
        	serviceFee = serviceFee.add(new BigDecimal(detailVO.getMorningDeliveryFee()));
        } else {
        	logDebugMessage("calcServiceFee - Morning delivery is unavailable");
        }
        
      }
    }
    
    if (detailVO.getOriginalOrderHasSDU() == null)
    {
    	detailVO.setOriginalOrderHasSDU("N");
    }
    
    //lookup discount in SNH
    return serviceFee;
  }

  /**
   * Wraps the Implementation for Shipping Fee Calculation to check for the exception model.
   * Typically, this is related to data/setup issues.
   */
  private BigDecimal calcShippingFee(Connection connection, double price, OrderDetailsVO detailVO, ProductVO productVO, Date orderedDate, boolean itemHasFreeShipping)
  throws Exception
  {
      try
      {
        return calcShippingFeeImpl(connection, price, detailVO, productVO, orderedDate, itemHasFreeShipping);
      } catch (Exception ex){
        if(frontEndExceptionMode)
        {
          logger.error("Failed to calculate shipping fee: " + ex.getMessage(), ex);
          return new BigDecimal(0);          
        }
        
        throw ex;
      }      
  }
  
  /* Calculate the shipping fee.  Shipping fee is based on the product selected,
   * price of the product, and ship method.  Shipping fee only applies to
   * products which are carrier delivered and not freshcut.
   * An extra amounts is added to orders going to Hawii and Alaska.
   *
   * @param String shipMethod, how the time will be shipped
   * @param String shippingKey shipping key for the product
   * @param double price product price
   * @return BigDecimal shipping fee*/

  private BigDecimal calcShippingFeeImpl(Connection connection, double price, OrderDetailsVO detailVO, ProductVO productVO, 
		  Date orderedDate, boolean itemHasFreeShipping)
    throws IOException, SAXException, ParserConfigurationException, SQLException, RecalculateException
  {
    String productType = productVO.getProductType();
    String productSubType = productVO.getProductSubType(); 
    
    String shipMethod = detailVO.getShipMethod();
    String shippingKey = productVO.getShippingKey();
  
    /* Shipping fees only apply to SAMEDAY GIFT and SPECIALTY GIFT items.*/
    if (!(productType.equals(PRODUCT_TYPE_SAMEDAY_GIFT) || productType.equals(PRODUCT_TYPE_SPECIALTY_GIFT)))
    {
      return new BigDecimal("0");
    }

    if (isProductType_Service_FreeShipping(productType, productSubType))
    {
      return new BigDecimal("0");
    }

    //if shipMethod is null assume florist delivered
    if (shipMethod == null || shipMethod.length() < 1)
    {
      String msg = "A ship method does not exist for this item.  The item is probably not deliverable for the date selected.";
      logger.error(msg);
      throw new RecalculateException(msg);
    }

    /*Using the shipping key details table determine what shipping detail
      id should be associated with this item.  Which shipping detail id
      to use is dependent on the shipping key code of the product (PRODUCT_MASTER)
      and the ship method chosen.*/
    BigDecimal shippingFee = recalculateOrderDAO.getShippingCost(connection, shipMethod, shippingKey, price);

    if (shippingFee == null)
    {
      String msg = 
        "Shipping cost not found in database.[shipMethod=" + shipMethod + ", shippingKey=" + shippingKey + ", price=" + 
        price + "]";
      logger.error(msg);
      throw new RecalculateException(msg);

    }

    detailVO.setShippingCost(shippingFee.toString());
    if (logger.isDebugEnabled())
    {
      logger.debug("shippingCost: " + detailVO.getShippingCost());
    }


    /* If the order is being delivered to Hawaii or Alaska and extra charge
     * is added to the shipping cost.  This charge only applies to
     * specialty gifts.  */
    if (productType.equals(PRODUCT_TYPE_SPECIALTY_GIFT))
    {
      RecipientAddressesVO address = getRecipientAddress(detailVO);
      if (address != null)
      {
        //we have an address
        if (address.getStateProvince().equals(HAWAII) || address.getStateProvince().equals(ALASKA))
        {
          //add additional shipping charge
          //Only add charge if carrier delivered Defect#489
          
          // Original RecalculateOrderVO used ship method carrier flag from the detailVO, which ScrubMapperDAO maps from ProductMaster
          // Added second check for the flag in the ProductVO, if the value in the detailVO is null to support apps not using ScrubMapperDAO 
          // (JOE, OrderService etc.)
          if ( (detailVO.getShipMethodCarrierFlag() != null && detailVO.getShipMethodCarrierFlag().equals("Y")
               || "Y".equalsIgnoreCase(productVO.getShipMethodCarrier())))
          {
            shippingFee = shippingFee.add(recalculateOrderGlobalParmsVO.getSpecialServiceCharge());
            detailVO.setAlaskaHawaiiSurcharge(recalculateOrderGlobalParmsVO.getSpecialServiceCharge().toString());
            if (logger.isDebugEnabled())
            {
              logger.debug("AK/HI: " + detailVO.getAlaskaHawaiiSurcharge());
            }
          }
        }
      }
    }
    
    // #11946 - if the control reaches here it is that the product type is either same day gifts or specialty gifts, ship method available etc.
    // calculate BBN
    calculateBBN(itemHasFreeShipping, connection, orderedDate, detailVO, false); 
    if(!StringUtils.isEmpty(detailVO.getMorningDeliveryFee())) {
    	shippingFee = shippingFee.add(new BigDecimal(detailVO.getMorningDeliveryFee()));
    } else {
    	logDebugMessage("CalcShippingFee - Morning delivery is unavailable");
    }

    return shippingFee;
  }


  /* Retrieve the addon price from the database.
   * @param String addonCode
   * @returns BigDecimal the addon price*/

  private BigDecimal calcAddonPrice(Connection connection, String addonCode)
    throws SAXException, IOException, ParserConfigurationException, SQLException, RecalculateException
  {

    AddOnVO addOnVO = recalculateOrderDAO.getAddOnById(connection, addonCode);

    if (addOnVO == null)
    {
      String msg = "Addon not found in database. Addon:" + addonCode;
      logger.error(msg);
      throw new RecalculateException(msg);
    }
    BigDecimal price = new BigDecimal(addOnVO.getAddOnPrice());

    return price;

  }

  /* Retrieve the global parameters from the database.*/

  private void setGlobalParams(Connection connection)
    throws SAXException, IOException, ParserConfigurationException, SQLException, RecalculateException
  {
	  recalculateOrderGlobalParmsVO = recalculateOrderDAO.getGlobalRecalculationParameters(connection);

    if (recalculateOrderGlobalParmsVO == null)
    {
      String msg = "Global params not found in database.";
      logger.error(msg);
      throw new RecalculateException(msg);
    }
  }


  /* This method returns a recipient's address object.  If one does not
   * exist null is returned.
   * @param OrderDetailsVO order line
   * @RecipientAddressVO*/

  private RecipientAddressesVO getRecipientAddress(OrderDetailsVO detailVO)
  {
    RecipientAddressesVO address = null;

    List recipientList = detailVO.getRecipients();
    if (recipientList != null && recipientList.size() > 0)
    {
      RecipientsVO recipient = (RecipientsVO) recipientList.get(0);
      if (recipient != null)
      {
        List addressList = recipient.getRecipientAddresses();
        if (addressList != null && addressList.size() > 0)
        {
          address = (RecipientAddressesVO) addressList.get(0);
        }
      }
    }

    return address;
  }

  /* This method check for null values.
     * If any found an exception is thrown.*/

  private void requiredCheck(RecipientAddressesVO addressVO)
    throws RecalculateException
  {
    //country
    if (addressVO.getCountry() == null || addressVO.getCountry().length() <= 0)
    {
      throw new RecalculateException("Missing recipient country");
    }
    else
    {
      //state
      if (addressVO.getCountry().equals("US") && 
          (addressVO.getStateProvince() == null || addressVO.getStateProvince().length() <= 0))
      {
        throw new RecalculateException("Missing recipient state");
      }
    }
  }

  /* This method checks for null values.
     * If any found an exception is thrown.*/

  private void requiredCheck(AddOnsVO addonVO)
    throws RecalculateException
  {
    //country
    if (addonVO.getAddOnCode() == null || addonVO.getAddOnCode().length() <= 0)
    {
      throw new RecalculateException("Missing AddOn Code");
    }
    //state
    if (addonVO.getAddOnQuantity() == null || addonVO.getAddOnQuantity().length() <= 0)
    {
      throw new RecalculateException("Missing Addon Quantity");
    }
  }


  /* This method check for null values.     *
     * If any found an exception is thrown.*/

  private void requiredCheck(OrderDetailsVO detailVO)
    throws RecalculateException
  {

    //source code
    if (detailVO.getSourceCode() == null || detailVO.getSourceCode().length() <= 0)
    {
      throw new RecalculateException("Missing source code on detail.");
    }

    //product id
    if (detailVO.getProductId() == null || detailVO.getProductId().length() <= 0)
    {
      throw new RecalculateException("Missing product id");
    }

    //size choice
    if (detailVO.getSizeChoice() == null || detailVO.getSizeChoice().length() <= 0)
    {
      throw new RecalculateException("Missing size");
    }
  }


  /* This method will null out any amount which contains a zero.
   * The object(s) using this object need the values to be null.*/

  /*private void fixUpAmounts(OrderVO order)
  {
    //order level amounts
    order.setAddOnAmountTotal(formatZero(order.getAddOnAmountTotal()));
    order.setDiscountTotal(formatZero(order.getDiscountTotal()));
    order.setOrderTotal(formatZero(order.getOrderTotal()));
    order.setPartnershipBonusPoints(formatZero(order.getPartnershipBonusPoints()));
    //order.setProductsTotal(formatZero(order.getProductsTotal()));
    order.setServiceFeeTotal(formatZero(order.getServiceFeeTotal()));
    order.setShippingFeeTotal(formatZero(order.getShippingFeeTotal()));
    order.setTaxTotal(formatZero(order.getTaxTotal()));


    //detail level amounts
    List details = order.getOrderDetail();
    for (int i = 0; i < details.size(); i++)
    {
      OrderDetailsVO detail = (OrderDetailsVO) details.get(i);
      detail.setAddOnAmount(formatZero(detail.getAddOnAmount()));
      detail.setDiscountAmount(formatZero(detail.getDiscountAmount()));
      detail.setExternalOrderTotal(formatZero(detail.getExternalOrderTotal()));
      detail.setMilesPoints(formatZero(detail.getMilesPoints()));
      detail.setProductsAmount(formatZero(detail.getProductsAmount()));
      detail.setServiceFeeAmount(formatZero(detail.getServiceFeeAmount()));
      detail.setShippingFeeAmount(formatZero(detail.getShippingFeeAmount()));
      detail.setTaxAmount(formatZero(detail.getTaxAmount()));
    }


  }*/

  /* This method will clear out all existing amounts.*/

  private void clearAmounts(OrderVO order, boolean clearODAmounts)
  {

    //order level amounts
    order.setAddOnAmountTotal("0");
    order.setDiscountTotal("0");
    order.setOrderTotal("0");
    order.setPartnershipBonusPoints("0");
    //order.setProductsTotal("0");
    order.setServiceFeeTotal("0");
    order.setShippingFeeTotal("0");
    order.setTaxTotal("0");

    if (clearODAmounts)
    {
      //detail level amounts
      List details = order.getOrderDetail();
      for (int i = 0; i < details.size(); i++)
      {
        OrderDetailsVO detail = (OrderDetailsVO) details.get(i);
        detail.setAddOnAmount("0");
        detail.setDiscountAmount("0");
        detail.setExternalOrderTotal("0");
        detail.setMilesPoints("0");
        detail.setProductsAmount("0");
        detail.setOriginalServiceFeeAmount(detail.getServiceFeeAmount());
        detail.setServiceFeeAmount("0");
        detail.setOriginalShippingFeeAmount(detail.getShippingFeeAmount());
        detail.setShippingFeeAmount("0");
        detail.setTaxAmount("0");
      }
    }
    else
    {
      //Init to 0 only if null
      List details = order.getOrderDetail();
      for (int i = 0; i < details.size(); i++)
      {
        OrderDetailsVO detail = (OrderDetailsVO) details.get(i);
        detail.setAddOnAmount(getZeroForNull(detail.getAddOnAmount()));
        detail.setDiscountAmount(getZeroForNull(detail.getDiscountAmount()));
        detail.setExternalOrderTotal(getZeroForNull(detail.getExternalOrderTotal()));
        detail.setMilesPoints(getZeroForNull(detail.getMilesPoints()));
        detail.setProductsAmount(getZeroForNull(detail.getProductsAmount()));
        detail.setServiceFeeAmount(getZeroForNull(detail.getServiceFeeAmount()));
        detail.setShippingFeeAmount(getZeroForNull(detail.getShippingFeeAmount()));
        detail.setTaxAmount(getZeroForNull(detail.getTaxAmount()));
      }

    }

  }

  /**
   * HElper method returns the string "0" for a null or empty string
   * @param val
   * @return
   */
  private String getZeroForNull(String val)
  {
    if (val == null || val.length() == 0)
    {
      return "0";
    }

    return val;
  }


  /* If the passed in value is blank or zero return null,
    * else return the same value back*/

  private String formatZero(String value)
  {

    String out = value;

    if (value == null || value.length() < 1 || Double.parseDouble(value) == 0.0)
    {
      out = null;
    }

    return out;
  }

  private Map getProductAmounts(OrderVO order)
    throws RecalculateException
  {
    Map amounts = new HashMap();

    //detail level amounts
    List details = order.getOrderDetail();
    for (int i = 0; i < details.size(); i++)
    {
      OrderDetailsVO detail = (OrderDetailsVO) details.get(i);
      if (!amounts.containsKey(detail.getLineNumber()))
      {
        amounts.put(detail.getLineNumber(), detail.getProductsAmount());
      }
      else
      {
        throw new RecalculateException("Non Unique Line Number in Order: " + detail.getLineNumber());
      }
    }

    return amounts;
  }

  private Map getTaxAmounts(OrderVO order) throws RecalculateException {
      Map amounts = new HashMap();

      //detail level amounts
      List details = order.getOrderDetail();
      for (int i = 0; i < details.size(); i++)
      {
          OrderDetailsVO detail = (OrderDetailsVO) details.get(i);
          if (!amounts.containsKey(detail.getLineNumber()))
          {
              amounts.put(detail.getLineNumber(), detail.getTaxAmount());
          }
          else
          {
              throw new RecalculateException("Non Unique Line Number in Order: " + detail.getLineNumber());
          }
      }

      return amounts;
}


  private BigDecimal recalculateShippingFee(Connection connection, OrderDetailsVO detailVO, String productType, 
                                            BigDecimal shippingFee)
  {
    String shipMethod = detailVO.getShipMethod();
    logger.info("** recalculateShippingFee-SHIP METHOD:" + shipMethod);
    logger.info("** recalculateShippingFee-PRODUCT TYPE:" + productType);

    if (productType != null && productType.equalsIgnoreCase(PRODUCT_TYPE_SAMEDAY_GIFT) && shipMethod != null && 
        shipMethod.equalsIgnoreCase(SAME_DAY_DELIVERED))
    {
      logger.info("** Performing calculation for SDG shipping fee");
      BigDecimal serviceFee = recalculateOrderDAO.getSameDayGiftGlobalServiceFee(connection);

      detailVO.setDomesticFloristServiceCharge(serviceFee.toString());

      shippingFee = shippingFee.subtract(serviceFee);
      logger.info("** New SDG shipping fee:" + shippingFee);
      detailVO.setShippingCost(shippingFee.toString());
      detailVO.setVendorCharge("0");
      logger.debug("recalc shipping cost: " + detailVO.getShippingCost());
      logger.debug("recalc vendor charge: " + detailVO.getVendorCharge());
      
      //#11496 - add the morning delivery fee back to shipping fee here.
      if(!StringUtils.isEmpty(detailVO.getMorningDeliveryFee())) {
      	shippingFee = shippingFee.add(new BigDecimal(detailVO.getMorningDeliveryFee()));
      } else {
      	logDebugMessage("recalculateShippingFee - Morning delivery is unavailable");
      }

    }

    return shippingFee;
  }

  /**
   * Check for Free Shipping Service
   * @param productType
   * @param productSubType
   */
  private boolean isProductType_Service_FreeShipping(String productType, String productSubType)
  {
    if ("SERVICES".equalsIgnoreCase(productType) && "FREESHIP".equalsIgnoreCase(productSubType))
    {
      return true;
    }

    return false;
  }


  /**
   * Checks if the Item has Free Shipping
   * @param order
   * @param
   * @return
   */
  private boolean checkItemHasFreeShipping(Connection connection, RecalculateOrderSourceCodeVO sourceVO, ProductVO productVO)
  {
    if(sourceVO.isAllowFreeShippingFlag() && productVO.isAllowFreeShippingFlag())
    {
      logger.info("Item qualifies for free shipping");
      return true;
    }
    
    if(logger.isDebugEnabled())
    {
      logger.info("Item Source or Product does not qualify for free shipping: Source Code: " + sourceVO.getAllowFreeShippingFlag() + " Product: " + productVO.getAllowFreeShippingFlag());    
    }
    
    return false;
  }

  /**
   * Looks up the Buyer's Personal Account Information and determines if the Buyer has Free Shipping for
   * the given order date
   * @param buyerSignedIn Flag that the buyer is signed in. 
   * @param emailAddress
   * @param orderDate
   * @return
   */
  private Boolean checkBuyerHasFreeShipping(Connection connection, boolean buyerSignedIn, String emailAddress, Date orderDate, MembershipDetailsVO fsMemDetails)
  {
    if(!buyerSignedIn)
    {
      logger.info("Buyer Signed in = false");
      return false;
    }
  
    if(logger.isInfoEnabled())
    {
      logger.info("Buyer: " + emailAddress + "," + orderDate);
    }
    
    if (StringUtils.isEmpty(emailAddress) || orderDate == null)
    {
      logger.info("No email or date information. Buyer does not have active free shipping membership");
      return false;
    }
    
    //Boolean isValidFS = FTDCAMSUtils.isValidFSAccount(emailAddress, orderDate);
    /*if(isValidFS == null){
    	return false;
    }*/
    if(fsMemDetails == null){
    	logger.info("Null FS membership details returned from CAMS");
    	return false;
    }
    return fsMemDetails.isMembershipBenefitsApplicable();
    
  }

  /** 
   * @param String pricing code
   * @param double non-discounted price
   * @return double discounted price*/

	private void calcRewards(Connection connection, RecalculateOrderSourceCodeVO sourceCodeVO, OrderDetailsVO detailVO)
	{
	  RecalculateOrderMilesPointsVO milesPointsVO = calcRewards(sourceCodeVO, new BigDecimal(detailVO.getProductsAmount()), new BigDecimal(detailVO.getExternalOrderTotal())); 
	
	  detailVO.setMilesPoints(milesPointsVO.getMilesPoints());
	  detailVO.setRewardType(milesPointsVO.getRewardType());
	
	  if (logger.isDebugEnabled())
	  {
	    logger.debug("rewardType: " + milesPointsVO.getRewardType() + " - value: " + milesPointsVO.getMilesPoints());
	  }
	}
  
  private RecalculateOrderMilesPointsVO calcRewards(RecalculateOrderSourceCodeVO sourceCodeVO, BigDecimal productAmount, BigDecimal orderTotalAmount){
	  
	  String programType = sourceCodeVO.getProgramType();
      String calcBasis = sourceCodeVO.getCalculationBasis();
      BigDecimal points = sourceCodeVO.getPoints();
      String bonusCalcBasis = sourceCodeVO.getBonusCalculationBasis();
      BigDecimal bonusPoints = sourceCodeVO.getBonusPoints();
      BigDecimal rewardPoints = null;
      String rewardType = sourceCodeVO.getRewardType();

      // No order total at this point. Rewards for calculation of basis "T" will not be reflected.
      if("Default".equals(programType)) {
          rewardPoints = this.calcRewards(calcBasis, points, productAmount, new BigDecimal("0"));
          if (bonusCalcBasis != null) {
              rewardPoints = rewardPoints.add(this.calcRewards(bonusCalcBasis, bonusPoints, productAmount, new BigDecimal("0")));
          }
      }
      RecalculateOrderMilesPointsVO mpvo = new RecalculateOrderMilesPointsVO();
      if(rewardPoints != null){
    	  mpvo.setMilesPoints(rewardPoints.toString());
      }
      mpvo.setRewardType(rewardType);

      if(logger.isDebugEnabled())
        logger.debug("rewardType: " + rewardType + " - value: " + rewardPoints);
      return mpvo;
  }

  private BigDecimal calcRewards(String calcBasis, BigDecimal points, BigDecimal productAmount, BigDecimal orderTotalAmount) {
      BigDecimal baseAmount = null;
      BigDecimal rewardPoints = null;
      if(CALC_BASIS_FIXED.equals(calcBasis)) {
          baseAmount = new BigDecimal("1");
      } else if(CALC_BASIS_MERCH.equals(calcBasis)) {
          baseAmount = productAmount;
      } else if(CALC_BASIS_TOTAL.equals(calcBasis)) {
          baseAmount = orderTotalAmount;
      }
      // If the points rewarding settings allow decimal, keep 2 digits; otherwise round half up to an integer.
      BigDecimal pointsInt = points.round(new MathContext(0));

      int scale = 0;
      if(pointsInt.compareTo(points) != 0) {
          scale  = AMOUNT_SCALE;
      }

      rewardPoints = baseAmount.multiply(points);
      return rewardPoints.setScale(scale, RoundingMode.HALF_UP);
  }

	/**
	 * Method to calculate the Morning delivery fee for a given order item.
	 * 
	 * @param itemHasFreeShipping
	 * @param connection
	 * @param orderedDate
	 * @param detailVO
	 * @param skipZipCodeCheck
	 */
	private void calculateBBN(boolean itemHasFreeShipping, Connection connection, Date orderedDate,OrderDetailsVO detailVO, boolean skipZipCodeCheck) {
		try {

			// Get the sourceVO from history table.
			Calendar cal = Calendar.getInstance();	
			cal.setTime(orderedDate);
			
			RecalculateOrderSourceCodeVO histSourceVO = recalculateOrderDAO
					.getSourceCodeHistDetails(cal, detailVO.getSourceCode(),connection);
			
			if(histSourceVO == null) {
				logger.error("CalculateBBN : Unable to get the source code details form history table. BBN is not recalculated");
				return;
			}
			
			boolean isMDFAvailable = false;
			GlobalParmHandler parmHandler;	
			String morningDeliveryFlag = histSourceVO.getMorningDeliveryFlag(); 
		
			/********************* SC morning delivery flag ************************/
			if(!StringUtils.isEmpty(morningDeliveryFlag)) {
				if("G".equals(morningDeliveryFlag)) {
					parmHandler = (GlobalParmHandler)CacheManager.getInstance().getHandler("CACHE_NAME_GLOBAL_PARM");
	              if(parmHandler != null) {
	              	morningDeliveryFlag = parmHandler.getGlobalParm("FTDAPPS_PARMS", "MORNING_DELIVERY");
	              }
				}
				
				if("Y".equals(morningDeliveryFlag)) {
					isMDFAvailable = true;
					detailVO.setMorningDeliveryAvailable("Y");
				}
			}		
			logDebugMessage("Step 1 : Checked Source code morning delivery flag - " + isMDFAvailable);
		
			/********************* BBN allowed for free shipping ************************/
			if(itemHasFreeShipping) {
				logDebugMessage("Item has free shipping, Checking if BBN is allowed for FS member");
				String isBBNAllowedForFSMember = histSourceVO.getAllowBBNForFSMemeber();
				
				if("G".equals(isBBNAllowedForFSMember)) {
					parmHandler = (GlobalParmHandler)CacheManager.getInstance().getHandler("CACHE_NAME_GLOBAL_PARM");
	              if(parmHandler != null) {
	              	isBBNAllowedForFSMember = parmHandler.getGlobalParm("FTDAPPS_PARMS", "MORNING_DELIVERY_FREE_SHIPPING");
	              }
				}
				
				if("N".equals(isBBNAllowedForFSMember)) {
					isMDFAvailable = false;
				}
			}		
			logDebugMessage("Step 2 : checked if BBN allowed FS Members- " + isMDFAvailable);	
		

			/********************* Product morning delivery flag ************************/
			if(isMDFAvailable && !"Y".equals(detailVO.getProductMorningDeliveryFlag())) {
				isMDFAvailable = false;
				detailVO.setMorningDeliveryAvailable(null);
			}		
			logDebugMessage("Step 3 : Checked product morning delivery availability - " + isMDFAvailable);
		
			/********************* ZIP Code availability ************************/
			if (isMDFAvailable && !skipZipCodeCheck) {	
				isMDFAvailable = validateZipCode(detailVO, connection);
				if(!isMDFAvailable){
					detailVO.setMorningDeliveryAvailable(null);
				}
				logDebugMessage("Step 4 : checked if BBN allowed for ZipCode - " + isMDFAvailable);
			}
			
			// 3) Customer has opted for morning delivery fee in APOLLO system - morning delivery fee is null and morning delivery opted is true
			// Customer has opted for morning delivery and has changed any price related item.
			boolean includeMDF = false;
			if (isMDFAvailable) {			
				if(detailVO.isChargeRelatedItemChanged() && "Y".equals(detailVO.getMorningDeliveryOpted())) {
					logDebugMessage("Step 5 : Morning Delivery Opted and charge related item changed - " + isMDFAvailable);	
					includeMDF = true;
				} else if ("Y".equals(detailVO.getMorningDeliveryOpted())){
					logDebugMessage("Step 5 : Morning Delivery Opted and no change in price related item - " + isMDFAvailable);	
					includeMDF = true;
				} else {
					logDebugMessage("Step 5 : Morning Delivery not Opted - " + isMDFAvailable);
				}
			}
		
			// Morning delivery should be calculated irrespective of whether BBN fee included or BBN opted on order scrub page.	
			String deliveryFee = null;		
			if(isMDFAvailable) {			
				deliveryFee = recalculateOrderDAO.getDeliveryFeeData(connection, orderedDate, histSourceVO.getSourceCode());
				logDebugMessage("Step 6 : Morning delivery is recalculated as - " + deliveryFee);				
			} 
			
			if(includeMDF) {
				detailVO.setMorningDeliveryFee(deliveryFee);
				detailVO.setBbnChargedtoFSMembers("Y");
			} else{
				detailVO.setMorningDeliveryFee(null);
				detailVO.setBbnChargedtoFSMembers("N");
			}
			
			try {
				if(isMDFAvailable && (!StringUtils.isEmpty(deliveryFee) && Double.parseDouble(deliveryFee) >= 0 )) {
					detailVO.setMorningDeliveryAvailable("Y");
				}
			} catch(NumberFormatException e) {
					logDebugMessage("Error caught converting delivery fee : " + e.getMessage());
			}
		} catch (Exception e) {
			logger.error("Error caught calculating BBN Fee : " + e.getMessage());
		}
		
	}
	
	/**
	 * @param item
	 * @param connection
	 * @return
	 */
	private boolean validateZipCode(OrderDetailsVO item, Connection connection) {
		int deliveryDay = 0;				
		try {
			deliveryDay = getDeliveryDay(item.getDeliveryDate());
			logDebugMessage("Day of the week : " + deliveryDay);
			
			String weekDayDelByNoon = "N";
			String satDelByNoon = "N";
			
			if(deliveryDay == Calendar.SATURDAY) {
				satDelByNoon = "Y";
			} else if (deliveryDay != Calendar.SUNDAY) {
				weekDayDelByNoon = "Y";
			} else {
				logDebugMessage("Delivery day falls on Sunday?? - " + deliveryDay);
				return false;
			}
			
			RecipientsVO recipient = null;
	        RecipientAddressesVO recipAddress = null;
	        
	        if("Y".equals(weekDayDelByNoon) || "Y".equals(satDelByNoon)) {						
	            if(item.getRecipients() != null && item.getRecipients().size() > 0) {
	                recipient = (RecipientsVO)item.getRecipients().get(0);
	                if(recipient.getRecipientAddresses() != null && recipient.getRecipientAddresses().size() > 0) {
	                    recipAddress = (RecipientAddressesVO)recipient.getRecipientAddresses().get(0);				                   
	                }			                
	            }
	        } 
	        
	        // Recipient address is null when it is Sunday delivery.
			if(recipAddress != null && recipAddress.getPostalCode() != null) {
				String zipCode = recipAddress.getPostalCode();
				if(zipCode.length() > 5) {
					logger.info("Zip Code length is too long, using substring of 5 digits in - " + zipCode);
					zipCode = zipCode.substring(0,5);
				}
				return FTDCommonUtils.getZipCodeAvailability(connection, zipCode, weekDayDelByNoon, satDelByNoon);
            } else {
            	logger.error("Error getting Recipient address details : Unable to check BBN availability for a zipcode- Treat BBN as Unavailable");
            	return false;
            }			        
			
		} catch (ParseException pe) {
			logger.error("Error caught parsing delivery date, unable to check BBN availability for agiven zip code");	
			return false;
		}
	}
	
	/** Get Day for a given date
	 * @param deliveryDateStr
	 * @return
	 * @throws ParseException
	 */
	private int getDeliveryDay( String deliveryDateStr) throws ParseException {
        java.util.Date deliveryDate = null;                          
        
        if(StringUtils.isEmpty(deliveryDateStr)) { 
        	return 0;
        }           
       
    	DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		deliveryDate = formatter.parse(deliveryDateStr);

		Calendar cal = Calendar.getInstance();	
		cal.setTime(deliveryDate);
		return cal.get(Calendar.DAY_OF_WEEK);        	
	}
	
	private void logDebugMessage(String message) {
		if(logger.isDebugEnabled()) {
			logger.debug(message);
		}
	}
	
	private String getAmazonProductIdByConfirmationNo(Connection conn, String externalOrderNumber) 
	throws Exception
	{
		/* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_AMZ_PRODUCT_ID_BY_CNF_NO");
        Map inputParams = new HashMap();
        inputParams.put("IN_CONF_NO", externalOrderNumber);
        dataRequest.setInputParams(inputParams);

        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();   
        
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
	      
	      String status = (String) outputs.get("OUT_STATUS");
	      if(status != null && status.equalsIgnoreCase("N"))
	      {
	        String message = (String) outputs.get("OUT_MESSAGE");
	        throw new SQLException(message);
	      }
	      String productId = (String) outputs.get("OUT_PRODUCT_ID");
	      logger.debug("ProductId from DB: "+productId);
		  return productId;
	}
	
	private String getMercentProductIdByConfirmationNo(Connection conn, String externalOrderNumber) throws Exception
	{
		/* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_MRCNT_PRODUCT_ID_BY_CNF_NO");
        Map inputParams = new HashMap();
        inputParams.put("IN_CONF_NO", externalOrderNumber);
        dataRequest.setInputParams(inputParams);

        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();   
        
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
	      
	      String status = (String) outputs.get("OUT_STATUS");
	      if(status != null && status.equalsIgnoreCase("N"))
	      {
	        String message = (String) outputs.get("OUT_MESSAGE");
	        throw new SQLException(message);
	      }
	      String productId = (String) outputs.get("OUT_PRODUCT_ID");
	      logger.debug("ProductId from DB: "+productId);
		  return productId;
	}
	
	/** Method to return productId from the cached result set containing complete parther order detail.
	 * @param conn
	 * @param externalOrderNumber
	 * @return
	 * @throws Exception
	 */
	private String getPartnerProductId(Connection conn, String externalOrderNumber) throws Exception {
		
		try {
			DataRequest dataRequest = new DataRequest();
			dataRequest.setConnection(conn);
			dataRequest.setStatementID("GET_PTN_ORD_ITEM_DETAIL");
			Map inputParams = new HashMap();
			inputParams.put("IN_CONFIRMATION_NUMBER", externalOrderNumber);
			dataRequest.setInputParams(inputParams);
	
			
			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();	
			CachedResultSet result = (CachedResultSet) dataAccessUtil.execute(dataRequest);
	
			if(result !=null && result.next()) {
				logger.debug("Partner item ProductId from DB: " + result.getString("PRODUCT_ID"));
				return result.getString("PRODUCT_ID");
			}
			
		} catch (Exception e) {
			logger.error("Error caught obtaining partner order detail: " + e.getMessage());
		}
		return null;
	}
	
	/** Calculate FTDW fees
	 * @param item
	 * @param orderedDate
	 * @param productVO
	 * @param itemHasFreeShipping
	 * @param connection
	 * @return
	 * @throws RecalculateException
	 */
	private BigDecimal calcFTDWFees(OrderDetailsVO item, Date orderedDate, String novatorId, 
			boolean itemHasFreeShipping, Connection connection, boolean isPCOrder) throws RecalculateException {
		logger.info("calcFTDWFees() started at: " + System.currentTimeMillis());
		PACUtil pacUtil = new PACUtil();
		PACResponse pacResponse = new PACResponse();
		BigDecimal fee = null;
		try {
			pacResponse = pacUtil.getPACBundleResponse(item, orderedDate, novatorId, isPCOrder);
			
			Map<String, Object> result = pacUtil.setItemFeeSplit(item, getRecipientAddress(item), 
					recalculateOrderGlobalParmsVO.getSpecialServiceCharge(), pacResponse, isPCOrder);
			
			fee =  result.get("fee") != null ? (BigDecimal)result.get("fee") : new BigDecimal("0");	
			
			if(result.get("isFTDWBBNAvailable") != null && (Boolean)result.get("isFTDWBBNAvailable")) {
				calculateBBN(itemHasFreeShipping, connection, orderedDate, item, true);				
				if(!StringUtils.isEmpty(item.getMorningDeliveryFee())) {
					fee = fee.add(new BigDecimal(item.getMorningDeliveryFee()));
				}
			} 	
			
		} catch(PACException e) { 
			throw new RecalculateException(e.getMessage());
		}
		logger.info("calcFTDWFees() Ended at: " + System.currentTimeMillis());
		
		return fee;
	}
	
	/**
	 * @param emailId
	 * @param orderDate
	 * @param qConn
	 * @param productId
	 * @param sourceCode
	 * @return
	 */
	public  boolean getFreeMemberShip(String emailId, Date orderDate, Connection qConn, String productId, String sourceCode) {
		try {
			
			boolean buyerSignedIn = true;
			if (StringUtils.isEmpty(emailId)) {
				buyerSignedIn = false;
			}
			List<String> emailList = new ArrayList<String>();
			if(!StringUtils.isEmpty(emailId)){
				emailList.add(emailId);
			}
			
			Map<String, MembershipDetailsVO> membershipMap = new HashMap<String, MembershipDetailsVO>();
			if(emailList != null && emailList.size() > 0){
				membershipMap = FTDCAMSUtils.getMembershipData(emailList, orderDate);
			}else{
				membershipMap = null;
			}

			MembershipDetailsVO fsMemDetails = null;
			if (membershipMap != null && membershipMap.containsKey(FTDCAMSUtils.FREESHIPPING_MEMBER_TYPE)) {
				fsMemDetails = membershipMap.get(FTDCAMSUtils.FREESHIPPING_MEMBER_TYPE);
			}
			
			Boolean buyerHasFreeShipping = checkBuyerHasFreeShipping(qConn,buyerSignedIn, emailId, orderDate, fsMemDetails);
			RecalculateOrderSourceCodeVO sourceVO = recalculateOrderDAO.getSourceCodeDetails(qConn, sourceCode);
			com.ftd.osp.utilities.vo.ProductVO productVO = recalculateOrderDAO.getProduct(qConn, productId);
			boolean productFreeShipping = false;
			productFreeShipping = checkItemHasFreeShipping(qConn, sourceVO,productVO);
			boolean itemHasFreeShipping = false;
			itemHasFreeShipping = buyerHasFreeShipping && productFreeShipping;
			logger.info("buyerHasFreeShipping::" + buyerHasFreeShipping+ "  productFreeShipping::" + productFreeShipping+ " itemHasFreeShipping::" + itemHasFreeShipping);
			return itemHasFreeShipping;
		} catch (Exception e) {
			logger.error("Error in getFreeMemberShip......Please check logs itemHasFreeShipping :: false"
					+ e);
			return false;
		}
	}
}
