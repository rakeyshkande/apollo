package com.ftd.osp.utilities.order.dao;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang.StringUtils;
import org.xml.sax.SAXException;

import com.ftd.ftdutilities.FTDCommonUtils;
import com.ftd.osp.utilities.AddOnUtility;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.FTDFuelSurchargeUtilities;
import com.ftd.osp.utilities.FTDServiceChargeUtility;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.constant.CacheMgrConstants;
import com.ftd.osp.utilities.cacheMgr.handlers.GlobalParmHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.StateMasterHandler;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.order.RecalculateException;
import com.ftd.osp.utilities.order.vo.PaymentMethodMilesPointsVO;
import com.ftd.osp.utilities.order.vo.RecalculateOrderGlobalParmsVO;
import com.ftd.osp.utilities.order.vo.RecalculateOrderPriceHeaderDetailsVO;
import com.ftd.osp.utilities.order.vo.RecalculateOrderProductSubcodeVO;
import com.ftd.osp.utilities.order.vo.RecalculateOrderSourceCodeVO;
import com.ftd.osp.utilities.order.vo.TaxVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.AddOnVO;
import com.ftd.osp.utilities.vo.ProductVO;
import com.ftd.osp.utilities.vo.ServiceChargeVO;


/**
 * This DAO provides data accessor methods to support the Recalculate Order BO.
 *
 */
public class RecalculateOrderDAO
{

  //DATABASE STATEMENTS
  private static final String GET_PRODUCT_BY_ID_STATEMENT = "GET_PRODUCT_BY_ID";
  private static final String GET_SOURCE_CODE_BY_ID_STATEMENT = "GET_SOURCE_CODE_RECORD";
  private static final String GET_SUBCODE_BY_ID_STATEMENT = "GET_PRODUCT_SUBCODE";
  private static final String GET_MILES_POINTS_STATEMENT = "GET_MILES_POINTS";
  private static final String GET_PRICE_HEADER_INFO_STATEMENT = "GET_PRICE_HEADER_DETAILS";
  private static final String GET_SHIPPING_KEY_DETAIL_ID_STATEMENT = "GET_SHIPPING_KEY_DETAILS_BYIDAMT";
  private static final String GET_SHIPPING_KEY_COSTS_STATEMENT = "GET_SHIPPING_KEY_COSTS_BYIDS";
  private static final String OE_GET_GLOBAL_PARMS_STATEMENT = "GET_GLOBAL_PARMS";
  private static final String GET_COUNTRY_MASTER_BY_ID_STATEMENT = "GET_COUNTRY_MASTER_BY_ID";
  private static final String GET_STATE_COMPANY_TAX_BY_ID_STATEMENT = "GET_STATE_COMPANY_TAX_BY_ID";
  private static final String GET_ACCOUNT_ACTIVE_PROGRAMS_STATEMENT = "UTIL_ACCT_VIEW_ACCOUNT_ACTIVE_PROGRAMS";

  // DATABASE COLUMNS
  private static final int SOURCE_CODE_PARTNER_ID = 9;
  private static final int SOURCE_CODE_PRICING_CODE = 7;
  private static final int SOURCE_CODE_SHIPING_CODE = 8;
  private static final int SOURCE_CODE_JCPENNY_FLAG = 30;
  private static final String SOURCE_CODE_ALLOW_FREE_SHIPPING_FLAG = "ALLOW_FREE_SHIPPING_FLAG";
  private static final int SOURCE_CODE_PROGRAM_TYPE = 45;
  private static final int SOURCE_CODE_CALCULATION_BASIS = 23;
  private static final int SOURCE_CODE_POINTS = 28;
  private static final int SOURCE_CODE_BONUS_CALCULATION_BASIS = 25;
  private static final int SOURCE_CODE_BONUS_POINTS = 24;
  private static final int SOURCE_CODE_REWARD_TYPE = 46;
  private static final String SOURCE_CODE_SAME_DAY_UPCHARGE = "SAME_DAY_UPCHARGE";
  private static final String SOURCE_CODE_DISPLAY_SAME_DAY_UPCHARGE = "DISPLAY_SAME_DAY_UPCHARGE";
  private static final String SOURCE_CODE_MORNING_DELIVERY_FLAG = "MORNING_DELIVERY_FLAG";
  private static final String SOURCE_CODE_MORNING_DELIVERY_FS = "MORNING_DELIVERY_FREE_SHIPPING";
  private static final String SOURCE_CODE_SAME_DAY_UPCHARGE_FS = "SAME_DAY_UPCHARGE_FS";

  private static final int PRODUCT_MASTER_PRODUCT_TYPE = 8;
  private static final int PRODUCT_MASTER_STANDARD_PRICE = 11;
  private static final int PRODUCT_MASTER_DELUXE_PRICE = 12;
  private static final int PRODUCT_MASTER_PREMIUM_PRICE = 13;
  private static final int PRODUCT_MASTER_DISCOUNT_ALLOWED_FLAG = 36;
  private static final int PRODUCT_MASTER_SHIPPING_KEY = 58;
  private static final int PRODUCT_MASTER_VARIABLE_PRICE_FLAG = 60;
  private static final String PRODUCT_MASTER_TAX_FLAG = "taxFlag";
  private static final String PRODUCT_MASTER_ALLOW_FREE_SHIPPING_FLAG = "allowFreeShippingFlag";
  private static final String PRODUCT_MASTER_PRODUCT_SUB_TYPE = "productSubType";
  private static final String PRODUCT_MASTER_SHIP_METHOD_CARRIER_FLAG = "shipMethodCarrier";
  private static final String PRODUCT_MASTER_SHIPPING_SYSTEM = "shippingSystem";

  private static final int PRICE_HEADER_DISCOUNT_TYPE = 4;
  private static final int PRICE_HEADER_DISCOUNT_AMOUNT = 5;

  private static final int SHIPPING_DETAIL_ID_DETAIL_ID = 1;
  private static final int SHIPPING_KEY_COSTS_SHIPPING_COST = 3;

  private static final int GLOBAL_PARAMS_FRESHCUTS_SVC_CHARGE = 17;
  private static final int GLOBAL_PARAMS_SPECIAL_SVC_CHARGE = 18;

  private static final int COUNTRY_MASTER_COUNTRY_TYPE = 2;

  private static final int STATE_MASTER_TAX_RATE = 4;

  private static final int SUBCODE_PARENT_ID = 1;
  private static final int SUBCODE_PRICE = 4;

  //COUNTRY TYPES
  private static final String DOMESTIC = "D";
  
  public RecalculateOrderDAO recalculateOrderDAO;

  //default value for tax display order 
  private static final int DEFAULT_TAX_DISPLAY_ORDER = 99999;

  private static Logger logger = new Logger(RecalculateOrderDAO.class.getName());
  
  public RecalculateOrderDAO()
  {
  }
  
  public RecalculateOrderDAO(RecalculateOrderDAO recalculateOrderDAO)
  {
	  	this.recalculateOrderDAO = recalculateOrderDAO;
  }


  /* Return native double from String object
     * @param String
     * @return double value
     *         returns zero if String is null*/

  private double formatToNativeDouble(String value)
  {
    return value == null? 0: new Double(value).doubleValue();
  }

  /* Return Big Decimal from String object
     * @param String
     * @return double value
     *         returns zero if String is null*/

  private BigDecimal formatToBigDecimal(String value)
  {
    return value == null? new BigDecimal(0): new BigDecimal(value);
  }

  /** This method takes in an object and returns
   * it's string value.
   * @param Object
   * @return String value of object
   *         If object is null, null is returned*
   */
  protected String getValue(Object obj)
  {
    return getValue(obj, null, null);
  }

  /** This method takes in an object and returns
   * it's string value.
   * @param Object
   * @return String value of object
   *         If object is null, null is returned*
   */
  protected String getValue(Object obj, String nullValue)
  {
    return getValue(obj, nullValue, null);
  }

  /** This method takes in an object and returns
   * it's string value.
   * @param Object
   * @param Value to return if the obj is null
   * @param emptyValue If specified, value to use if the string value is empty.
   * @return String value of object
   *         If object is null, nullValue is returned*
   */
  protected String getValue(Object obj, String nullValue, String emptyValue)
  {

    String value = null;

    if (obj == null)
    {
      return nullValue;
    }

    if (obj instanceof BigDecimal)
    {
      value = obj.toString();
    }
    else if (obj instanceof String)
    {
      value = (String) obj;
    }
    else if (obj instanceof Timestamp)
    {
      value = obj.toString();
    }
    else
    {
      value = obj.toString();
    }

    if (emptyValue != null && value.length() == 0)
    {
      value = emptyValue;
    }

    return value;
  }

  /**
   * Retrieves the Source Code details from the database
   * @param sourceCode
   * @return A populated instance of SourceVO or null if no record exists
   */
  public RecalculateOrderSourceCodeVO getSourceCodeDetails(Connection connection, String sourceCode)
  {
    Map paramMap = new HashMap();
    paramMap.put("IN_SOURCE_CODE", sourceCode);
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(connection);
    dataRequest.setStatementID(GET_SOURCE_CODE_BY_ID_STATEMENT);
    dataRequest.setInputParams(paramMap);

    try
    {
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
      if (rs == null || !rs.next())
      {
        return null;
      }
      RecalculateOrderSourceCodeVO sourceVO = new RecalculateOrderSourceCodeVO();

      sourceVO.setSourceCode(sourceCode);
      sourceVO.setPricingCode(getValue(rs.getObject(SOURCE_CODE_PRICING_CODE)));
      sourceVO.setPartnerId(getValue(rs.getObject(SOURCE_CODE_PARTNER_ID)));
      sourceVO.setShippingCode(getValue(rs.getObject(SOURCE_CODE_SHIPING_CODE)));
      sourceVO.setJcpenneyFlag(getValue(rs.getObject(SOURCE_CODE_JCPENNY_FLAG), ""));
      sourceVO.setAllowFreeShippingFlag(getValue(rs.getObject(SOURCE_CODE_ALLOW_FREE_SHIPPING_FLAG), ""));
      sourceVO.setBonusCalculationBasis(getValue(rs.getObject(SOURCE_CODE_BONUS_CALCULATION_BASIS)));
      sourceVO.setBonusPoints(formatToBigDecimal(getValue(rs.getObject(SOURCE_CODE_BONUS_POINTS))));
      sourceVO.setCalculationBasis(getValue(rs.getObject(SOURCE_CODE_CALCULATION_BASIS)));
      sourceVO.setPoints(formatToBigDecimal(getValue(rs.getObject(SOURCE_CODE_POINTS))));
      sourceVO.setProgramType(getValue(rs.getObject(SOURCE_CODE_PROGRAM_TYPE)));
      sourceVO.setRewardType(getValue(rs.getObject(SOURCE_CODE_REWARD_TYPE)));
      sourceVO.setSameDayUpcharge(getValue(rs.getObject(SOURCE_CODE_SAME_DAY_UPCHARGE)));
      sourceVO.setDisplaySameDayUpcharge(getValue(rs.getObject(SOURCE_CODE_DISPLAY_SAME_DAY_UPCHARGE)));
      //#11946 - BBN - get Morning_delivery_flag and morning_delivery_free_shipping from source_master
      sourceVO.setMorningDeliveryFlag(getValue(rs.getObject(SOURCE_CODE_MORNING_DELIVERY_FLAG)));
      sourceVO.setAllowBBNForFSMemeber(getValue(rs.getObject(SOURCE_CODE_MORNING_DELIVERY_FS)));
      sourceVO.setSameDayUpchargeFS(getValue(rs.getObject(SOURCE_CODE_SAME_DAY_UPCHARGE_FS)));
      
            
      return sourceVO;
    }
    catch (Exception e)
    {
      logger.error(e);
      throw new RuntimeException(e.getMessage(), e);
    }
  }


  /* Retrieve the product from the database.
   * @param String product id
   * @returns ProductVO
   */

  public ProductVO getProduct(Connection connection, String productId)
  {
    Map paramMap = new HashMap();

    paramMap.put("IN_PRODUCT_ID", productId);

    // Get the order header infomration
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(connection);
    dataRequest.setStatementID(GET_PRODUCT_BY_ID_STATEMENT);
    dataRequest.setInputParams(paramMap);

    try
    {
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

      if (rs == null || !rs.next())
      {
        return null;
      }

      ProductVO productVO = new ProductVO();

      productVO.setProductId(productId);
      productVO.setNovatorId(rs.getString("novatorId"));
      productVO.setShippingKey(getValue(rs.getObject(PRODUCT_MASTER_SHIPPING_KEY)));
      productVO.setShippingSystem(getValue(rs.getObject(PRODUCT_MASTER_SHIPPING_SYSTEM)));
      productVO.setStandardPrice(formatToNativeDouble(getValue(rs.getObject(PRODUCT_MASTER_STANDARD_PRICE))));
      productVO.setDeluxePrice(formatToNativeDouble(getValue(rs.getObject(PRODUCT_MASTER_DELUXE_PRICE))));
      productVO.setPremiumPrice(formatToNativeDouble(getValue(rs.getObject(PRODUCT_MASTER_PREMIUM_PRICE))));
      productVO.setProductType(getValue(rs.getObject(PRODUCT_MASTER_PRODUCT_TYPE)));
      productVO.setProductSubType(getValue(rs.getObject(PRODUCT_MASTER_PRODUCT_SUB_TYPE)));
      productVO.setDiscountAllowedFlag(getValue(rs.getObject(PRODUCT_MASTER_DISCOUNT_ALLOWED_FLAG), "N", "N"));
      productVO.setVariablePriceFlag(getValue(rs.getObject(PRODUCT_MASTER_VARIABLE_PRICE_FLAG), "N", "N"));
      productVO.setTaxFlag(getValue(rs.getObject(PRODUCT_MASTER_TAX_FLAG), "N", "N"));
      productVO.setAllowFreeShippingFlag(getValue(rs.getObject(PRODUCT_MASTER_ALLOW_FREE_SHIPPING_FLAG), "N", "N"));
      productVO.setShipMethodCarrier(getValue(rs.getObject(PRODUCT_MASTER_SHIP_METHOD_CARRIER_FLAG), "N", "N"));
      productVO.setShippingSystem(getValue(rs.getString("shippingSystem")));
      productVO.setPquadProductId(getValue(rs.getString("pquadProductID")));
      productVO.setPersonalizationTemplate(getValue(rs.getString("personalizationTemplate")));
      
      return productVO;
    }
    catch (Exception e)
    {
      throw new RuntimeException(e.getMessage(), e);
    }
  }


  /**
   * Returns the sub code price for the passed in Sub code Id
   * @param connection
   * @param subcodeId
   * @return The Sub code price or null if the sub code was not found
   */
  public RecalculateOrderProductSubcodeVO getSubcodeById(Connection connection, String subcodeId)
  {
    Map paramMap = new HashMap();

    double price = 0;

    paramMap.put("PRODUCT_ID", subcodeId);

    // Get the order header infomration
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(connection);
    dataRequest.setStatementID(GET_SUBCODE_BY_ID_STATEMENT);
    dataRequest.setInputParams(paramMap);

    try
    {
      DataAccessUtil dau = DataAccessUtil.getInstance();
      CachedResultSet rs = (CachedResultSet) dau.execute(dataRequest);
      if (rs.next())
      {
        RecalculateOrderProductSubcodeVO productSubcodeVO = new RecalculateOrderProductSubcodeVO();
        productSubcodeVO.setProductSubcodeId(subcodeId);
        price = formatToNativeDouble(getValue(rs.getObject(SUBCODE_PRICE)));
        productSubcodeVO.setProductId(getValue(rs.getObject(SUBCODE_PARENT_ID)));
        productSubcodeVO.setSubcodePrice(price);

        return productSubcodeVO;
      }

      return null;
    }
    catch (Exception e)
    {
      throw new RuntimeException(e.getMessage(), e);
    }
  }

  /**
   * Returns the Price Header Information for the pricing Code, price Amount
   * @param connection
   * @param pricingCode
   * @param priceAmount
   * @return PriceHeaderDetailsVO or null if there is no price header information
   */
  public RecalculateOrderPriceHeaderDetailsVO getPriceHeaderDetails(Connection connection, String pricingCode, 
                                                                    String priceAmount)
  {
    //retrieve price header information
    Map paramMap = new HashMap();
    paramMap.put("IN_PRICE_HEADER_ID", pricingCode);
    paramMap.put("IN_PRICE_AMT", new Double(priceAmount));
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(connection);
    dataRequest.setStatementID(GET_PRICE_HEADER_INFO_STATEMENT);
    dataRequest.setInputParams(paramMap);

    try
    {
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
      if (rs == null || !rs.next())
      {
        return null;
      }

      String discountType = getValue(rs.getObject(PRICE_HEADER_DISCOUNT_TYPE));
      BigDecimal discountAmount = formatToBigDecimal(getValue(rs.getObject(PRICE_HEADER_DISCOUNT_AMOUNT)));

      RecalculateOrderPriceHeaderDetailsVO priceHeaderDetailsVO = new RecalculateOrderPriceHeaderDetailsVO();
      priceHeaderDetailsVO.setDiscountType(discountType);
      priceHeaderDetailsVO.setDiscountAmount(discountAmount);


      return priceHeaderDetailsVO;

    }
    catch (Exception e)
    {
      throw new RuntimeException(e.getMessage(), e);
    }
  }


  /**
   * Retrieves the shipping costs for the given shipping method, key and price
   * Using the shipping key details table determine what shipping detail
   * id should be associated with this item.  Which shipping detail id
   * to use is dependant on the shipping key code of the product (PRODUCT_MASTER)
   * and the ship method choosen.
   *
   * Then uses the shipping detail id that was just obtained along with
   * the ship method to determine the shipping cost.  The SHIPPING_KEY_COSTS
   * table is used to find the cost.
   *
   * @param connection
   * @param shipMethod
   * @param shippingKey
   * @param price
   * @return The Shipping Cost, or null if it was unable to find the shipping detail or shipping cost.
   *
   */
  public BigDecimal getShippingCost(Connection connection, String shipMethod, String shippingKey, double price)
  {
    Map paramMap = new HashMap();
    DataRequest dataRequest = new DataRequest();
    CachedResultSet rs = null;

    paramMap.put("IN_SHIPPING_KEY_ID", shippingKey);
    paramMap.put("IN_PRICE_AMT", new Double(price));
    // Get the order header infomration
    dataRequest.setConnection(connection);
    dataRequest.setStatementID(GET_SHIPPING_KEY_DETAIL_ID_STATEMENT);
    dataRequest.setInputParams(paramMap);

    try
    {
      // Find the Shipping Detail Id
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
      if (rs == null || !rs.next())
      {
        String msg = "Shipping key not found in database.[" + shippingKey + ":" + price + "]";
        logger.error(msg);
        return null;
      }

      long shippingDetailId = new Long(getValue(rs.getObject(SHIPPING_DETAIL_ID_DETAIL_ID))).longValue();

      // Retrieve the Cost
      paramMap = new HashMap();
      paramMap.put("IN_SHIPPING_KEY_DETAIL_ID", new Long(shippingDetailId).toString());
      paramMap.put("IN_SHIPPING_METHOD_ID", shipMethod);
      // Get the order header infomration
      dataRequest = new DataRequest();
      dataRequest.setConnection(connection);
      dataRequest.setStatementID(GET_SHIPPING_KEY_COSTS_STATEMENT);
      dataRequest.setInputParams(paramMap);
      rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
      if (rs == null || !rs.next())
      {
        String msg = "Shipping cost not found in database.[" + shippingDetailId + ":" + shipMethod + "]";
        logger.error(msg);
        return null;
      }

      BigDecimal shippingFee = formatToBigDecimal(getValue(rs.getObject(SHIPPING_KEY_COSTS_SHIPPING_COST)));

      return shippingFee;
    }
    catch (Exception e)
    {
      throw new RuntimeException(e.getMessage(), e);
    }
  }

  /**
   * Retrieves the AddOns Information by its ID. Delegates to the AddOnUtility
   * @param connection
   * @param addonId
   * @return
   */
  public AddOnVO getAddOnById(Connection connection, String addonId)
  {
    AddOnUtility addOnUtility = new AddOnUtility();
    try
    {
      AddOnVO addOnVO = addOnUtility.getAddOnById(addonId, Boolean.FALSE, Boolean.FALSE, connection);

      if (addOnVO.getAddOnPrice() == null || addOnVO.getAddOnPrice().length() == 0)
      {
        addOnVO.setAddOnPrice("0");
      }

      return addOnVO;
    }
    catch (Exception e)
    {
      throw new RuntimeException(e.getMessage(), e);
    }
  }

  /**
   * Retrieves parameters used for calculations from the global parameters table.
   * @param connection
   * @return
   */
  public RecalculateOrderGlobalParmsVO getGlobalRecalculationParameters(Connection connection)
  {
	DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(connection);
    dataRequest.setStatementID(OE_GET_GLOBAL_PARMS_STATEMENT);

    try
    {
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
      if (rs == null || !rs.next())
      {
        return null;
      }

      RecalculateOrderGlobalParmsVO recalculateOrderGlobalParmsVO = new RecalculateOrderGlobalParmsVO();
      recalculateOrderGlobalParmsVO.setFreshCutServiceCharge(new BigDecimal(getValue(rs.getObject(GLOBAL_PARAMS_FRESHCUTS_SVC_CHARGE))));
      recalculateOrderGlobalParmsVO.setSpecialServiceCharge(new BigDecimal(getValue(rs.getObject(GLOBAL_PARAMS_SPECIAL_SVC_CHARGE))));
      return recalculateOrderGlobalParmsVO;
    }
    catch (Exception e)
    {
      throw new RuntimeException(e.getMessage(), e);
    }
  }


  /**
   * This method takes in a country code and returns
   * a boolean value indicating if the country is considered
   * domestic.
   * @param countryId
   * @return
   */
  public boolean isCountryDomestic(Connection connection, String country)
  {
    //If united states do not do a database lookup
    if (country.equalsIgnoreCase("US") || country.equalsIgnoreCase("USA"))
    {
      return true;
    }

    Map paramMap = new HashMap();
    paramMap.put("IN_COUNTRY_ID", country);
    // Get the order header infomration
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(connection);
    dataRequest.setStatementID(GET_COUNTRY_MASTER_BY_ID_STATEMENT);
    dataRequest.setInputParams(paramMap);

    try
    {
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
      if (rs == null || !rs.next())
      {
        String msg = "Country code not found in database.[" + country + "]";
        logger.error(msg);
        throw new RecalculateException(msg);
      }

      String domesticFlag = getValue(rs.getObject(COUNTRY_MASTER_COUNTRY_TYPE));
      return domesticFlag.equals(DOMESTIC)? true: false;
    }
    catch (Exception e)
    {
      throw new RuntimeException(e.getMessage(), e);
    }
  }


  /**
   *
   * @param connection
   * @param state
   * @param company id
   * @return list
   */
  public List getStateTaxRate(Connection connection, String stateMasterId, String companyId)
  {
    Map paramMap = new HashMap();
    List taxVOs = new ArrayList();
    TaxVO tVO;
    String sDisplayOrderSeqNum;
    String sContentDescription;

    paramMap.put("IN_STATE_MASTER_ID", stateMasterId);
    paramMap.put("IN_COMPANY_ID", companyId);
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(connection);
    dataRequest.setStatementID(GET_STATE_COMPANY_TAX_BY_ID_STATEMENT);
    dataRequest.setInputParams(paramMap);

    try
    {
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
      ConfigurationUtil configUtil = ConfigurationUtil.getInstance();

      if (rs == null || !rs.next())
      {
        String msg = "Company / State code not found in database.[" + companyId + ", " + stateMasterId + "]";
        logger.error(msg);
        throw new RecalculateException(msg);
      }

      //VO for TAX1
      if (StringUtils.isNotEmpty(rs.getString("tax1_name")))
      {
        tVO = new TaxVO();
        tVO.setName(rs.getString("tax1_name"));
        tVO.setRate(formatToBigDecimal(rs.getString("tax1_rate")));
        tVO.setAmount(new BigDecimal(0));
        sDisplayOrderSeqNum = rs.getString("tax1_display_order");
        if (StringUtils.isEmpty(sDisplayOrderSeqNum))
          tVO.setDisplayOrder(DEFAULT_TAX_DISPLAY_ORDER);
        else
          tVO.setDisplayOrder(Integer.valueOf(sDisplayOrderSeqNum).intValue());
        sContentDescription = configUtil.getContentWithFilter(connection, "TAX", "TAX_DESCRIPTION", companyId, tVO.getName()); 
        tVO.setDescription(sContentDescription==null?"":sContentDescription);

        taxVOs.add(tVO);
      }

      //VO for TAX2
      if (StringUtils.isNotEmpty(rs.getString("tax2_name")))
      {
        tVO = new TaxVO();
        tVO.setName(rs.getString("tax2_name"));
        tVO.setRate(formatToBigDecimal(rs.getString("tax2_rate")));
        tVO.setAmount(new BigDecimal(0));
        sDisplayOrderSeqNum = rs.getString("tax2_display_order");
        if (StringUtils.isEmpty(sDisplayOrderSeqNum))
          tVO.setDisplayOrder(DEFAULT_TAX_DISPLAY_ORDER);
        else
          tVO.setDisplayOrder(Integer.valueOf(sDisplayOrderSeqNum).intValue());
        sContentDescription = configUtil.getContentWithFilter(connection, "TAX", "TAX_DESCRIPTION", companyId, tVO.getName()); 
        tVO.setDescription(sContentDescription==null?"":sContentDescription);
      
        taxVOs.add(tVO);
      }

      //VO for TAX3
      if (StringUtils.isNotEmpty(rs.getString("tax3_name")))
      {
        tVO = new TaxVO();
        tVO.setName(rs.getString("tax3_name"));
        tVO.setRate(formatToBigDecimal(rs.getString("tax3_rate")));
        tVO.setAmount(new BigDecimal(0));
        sDisplayOrderSeqNum = rs.getString("tax3_display_order");
        if (StringUtils.isEmpty(sDisplayOrderSeqNum))
          tVO.setDisplayOrder(DEFAULT_TAX_DISPLAY_ORDER);
        else
          tVO.setDisplayOrder(Integer.valueOf(sDisplayOrderSeqNum).intValue());
        sContentDescription = configUtil.getContentWithFilter(connection, "TAX", "TAX_DESCRIPTION", companyId, tVO.getName()); 
        tVO.setDescription(sContentDescription==null?"":sContentDescription);
      
        taxVOs.add(tVO);
      }

      //VO for TAX4
      if (StringUtils.isNotEmpty(rs.getString("tax4_name")))
      {
        tVO = new TaxVO();
        tVO.setName(rs.getString("tax4_name"));
        tVO.setRate(formatToBigDecimal(rs.getString("tax4_rate")));
        tVO.setAmount(new BigDecimal(0));
        sDisplayOrderSeqNum = rs.getString("tax4_display_order");
        if (StringUtils.isEmpty(sDisplayOrderSeqNum))
          tVO.setDisplayOrder(DEFAULT_TAX_DISPLAY_ORDER);
        else
          tVO.setDisplayOrder(Integer.valueOf(sDisplayOrderSeqNum).intValue());
        sContentDescription = configUtil.getContentWithFilter(connection, "TAX", "TAX_DESCRIPTION", companyId, tVO.getName()); 
        tVO.setDescription(sContentDescription==null?"":sContentDescription);
      
        taxVOs.add(tVO);
      }

      //VO for TAX5
      if (StringUtils.isNotEmpty(rs.getString("tax5_name")))
      {
        tVO = new TaxVO();
        tVO.setName(rs.getString("tax5_name"));
        tVO.setRate(formatToBigDecimal(rs.getString("tax5_rate")));
        tVO.setAmount(new BigDecimal(0));
        sDisplayOrderSeqNum = rs.getString("tax5_display_order");
        if (StringUtils.isEmpty(sDisplayOrderSeqNum))
          tVO.setDisplayOrder(DEFAULT_TAX_DISPLAY_ORDER);
        else
          tVO.setDisplayOrder(Integer.valueOf(sDisplayOrderSeqNum).intValue());
        sContentDescription = configUtil.getContentWithFilter(connection, "TAX", "TAX_DESCRIPTION", companyId, tVO.getName()); 
        tVO.setDescription(sContentDescription==null?"":sContentDescription);
      
        taxVOs.add(tVO);
      }


      return taxVOs;
    }
    catch (Exception e)
    {
      throw new RuntimeException(e.getMessage(), e);
    }
  }


  /**
   * Returns the Fuel Surcharge for the Given Date.
   * @param connection
   * @param date
   * @return
   */
  public BigDecimal getFuelSurcharge(Connection connection, Date date)
  {
    try
    {
      FTDFuelSurchargeUtilities fuelSurchargeUtility = new FTDFuelSurchargeUtilities();
      String tempFuelSurcharge = fuelSurchargeUtility.getFuelSurcharge(date, connection);
      BigDecimal fuelSurcharge = new BigDecimal("0");
      if (tempFuelSurcharge != null)
      {
        fuelSurcharge = new BigDecimal(tempFuelSurcharge);
      }

      return fuelSurcharge;
    }
    catch (Exception e)
    {
      throw new RuntimeException(e.getMessage(), e);
    }
  }

  /**
   * Returns the Service Charge Date for the dates and source code provided.
   * @param connection
   * @param serviceFeeDate
   * @param sourceCode
   * @param deliveryDate
   * @return
   */
  public ServiceChargeVO getServiceChargeData(Connection connection, Date serviceFeeDate, String sourceCode, 
                                              Date deliveryDate)
  {
    FTDServiceChargeUtility ftdServiceChargeUtility = new FTDServiceChargeUtility();

    try
    {
      return ftdServiceChargeUtility.getServiceChargeData(serviceFeeDate, sourceCode, deliveryDate, connection);
    }
    catch (Exception e)
    {
      throw new RuntimeException(e.getMessage(), e);
    }
  }


  /**
   * Retrieves the Global Service Fee for Same Day Gifts from the Global Parm Table.
   * Note: This implementation uses the Cache
   * @param connection
   * @return
   */
  public BigDecimal getSameDayGiftGlobalServiceFee(Connection connection)
  {
    try
    {
      GlobalParmHandler globalParamHandler = (GlobalParmHandler) CacheManager.getInstance().getHandler("CACHE_NAME_GLOBAL_PARM");
      if (globalParamHandler != null)
      {
        BigDecimal serviceFee = 
          new BigDecimal(globalParamHandler.getGlobalParm("PRICE_CALCULATION", "SDG_GLOBAL_SERVICE_FEE"));
        return serviceFee;
      }
    }
    catch (Exception e)
    {
      throw new RuntimeException("Unable to retrieve Global Service Fee: GLOBAL_PARM cache not available", e);
    }

    throw new RuntimeException("Unable to retrieve Global Service Fee: GLOBAL_PARM cache not available");
  }

  /**
   * Returns the Payment Method Miles Points for the given Payment Method
   * @param connection
   * @param paymentMethod
   * @return
   */
  public PaymentMethodMilesPointsVO getPaymentMethodMilesPoints(Connection connection, String paymentMethod)
  {
    PaymentMethodMilesPointsVO mpvo;

    try
    {
      mpvo = FTDCommonUtils.getPaymentMethodMilesPoints(paymentMethod, connection);
      return mpvo;
    }
    catch (Exception e)
    {
      throw new RuntimeException(e.getMessage(), e);
    }

  }

  /**
   * Returns a Set containing the Active Program Names for the Apollo Account with the passed in Email Address
   * and order date.
   * 
   * @param connection
   * @param emailAddress
   * @param orderDate
   * @return
   */
  public Set<String> getActiveProgramsForAccount(Connection connection, String emailAddress, Date orderDate)
  { 
    Map paramMap = new HashMap();
    paramMap.put("IN_EMAIL_ADDRESS", emailAddress);
    paramMap.put("IN_ACTIVE_DATE", new java.sql.Timestamp(orderDate.getTime()));
    
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(connection);
    dataRequest.setStatementID(GET_ACCOUNT_ACTIVE_PROGRAMS_STATEMENT);
    dataRequest.setInputParams(paramMap);
    Set<String> retVal = new HashSet<String>();

    try
    {
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      Map outputs = (Map) dataAccessUtil.execute(dataRequest);

      CachedResultSet rs = (CachedResultSet) outputs.get("OUT_CUR");
      String out_status = (String) outputs.get("OUT_STATUS");
      String out_message = (String) outputs.get("OUT_MESSAGE");
      
      if("Y".equalsIgnoreCase(out_status) == false)
      {
        throw new Exception("Failed to retrieve account program information: " + out_message);
      }
      
      while(rs.next())
      {
        String programName = rs.getString("PROGRAM_NAME");
        
        if(!StringUtils.isEmpty(programName))
        {
          retVal.add(programName);
        }
      }
      
      return retVal;
    }
    catch (Exception e)
    {
      throw new RuntimeException(e.getMessage(), e);
    } 
  }
  
  
  public TimeZone getRecipientTimeZone(String zipCode, String stateId, Connection conn) 
  throws SAXException, ParserConfigurationException, IOException, SQLException{
	  
	  if(zipCode == null) zipCode = "";
      DataAccessUtil dau = DataAccessUtil.getInstance();
      DataRequest dataRequest = new DataRequest();
      CachedResultSet rs = null;
      logger.debug("###SDU to serviceFee getRecipientTimeZone - zipCode   stateId ### "+zipCode +" "+ stateId);
      // if the country is canada then only use the first three characters of
      // the zip code
      /*if(countryId != null && countryId.equals(canadaCountryCode) && zipCode.length() > 3)
      {
          zipCode = zipCode.substring(0, 3);
      }
      else if(countryId != null && countryId.equals(usCountryCode) && zipCode.length() > 5)
      {
          zipCode = zipCode.substring(0, 5);
      }*/

      // Try cache first
      StateMasterHandler stateHandler = (StateMasterHandler)CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_STATE_MASTER);
      String tZone = null;
      if(stateHandler != null)
      {
          tZone = stateHandler.getTimeZone(stateId);
      }

      // If the zip handler is not found or the zip code is not in the cache
      // then lookup the zip code data from the database
      if(stateHandler == null || tZone == null)
      {
          dataRequest.setStatementID("GET_ZIP_CODE_DETAILS");
          dataRequest.addInputParam("ZIP_CODE", zipCode);
          dataRequest.setConnection(conn);
          rs = (CachedResultSet)dau.execute(dataRequest);
          dataRequest.reset();

          while(rs.next())
          {
              tZone = (String)rs.getObject(1);
          }
      }
      logger.debug("###SDU to serviceFee getRecipientTimeZone - tZone ### "+tZone);
      // Default to five if the zip code is not found (taken from WebOE)
      if(tZone == null) tZone = "5";
      
      Calendar calendar = Calendar.getInstance();
      TimeZone tz = calendar.getTimeZone();
      TimeZone timeZone = null;

          switch ( Integer.parseInt(tZone) )
          {
              case 0:     // international
                  timeZone = tz.getTimeZone("America/Chicago");
                  break;
              case 1:     // eastern
                  timeZone = tz.getTimeZone("America/New_York");
                  break;
              case 2:     // central
                  timeZone = tz.getTimeZone("America/Chicago");
                  break;
              case 3:     // mountain
                  timeZone = tz.getTimeZone("America/Denver");
                  break;
              case 4:     // pacific
                  timeZone = tz.getTimeZone("America/Los_Angeles");
                  break;
              case 5:     // hawaiian (alaska, hawaii)
                  timeZone = tz.getTimeZone("HST");
                  break;
              case 6:     // atlantic (puerto rico , virgin islands)
                  timeZone = tz.getTimeZone("America/Puerto_Rico");
                  break;
          }
      return timeZone;
  }
  
  
	/** #11946 - Get the Morning delivery fee for a given source code, ordered date, zip code.
	 * @param connection
	 * @param orderedDate
	 * @param detailVO
	 * @return
	 */
	public String getDeliveryFeeData(Connection connection, Date orderedDate, String sourceCode) {
		
		String bbnFee = null;
		String id = null;
		
		try {
			Timestamp orderDateTimestamp = new Timestamp(orderedDate.getTime());

			if (logger.isDebugEnabled()) {
				SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
				logger.debug("getDeliveryFeeData called\n"
						+ "OrderDate timestamp: " + sdf.format(orderDateTimestamp.getTime()) + "\n"
						+ "SourceCode: " + sourceCode);
			}

			HashMap inputParams = new HashMap();
			DataRequest dataRequest = new DataRequest();
			dataRequest.setConnection(connection);

			dataRequest.setStatementID("GET_MORNING_DELIVERY_FEE_DATA");
			inputParams.put("IN_ORDER_DATE", orderDateTimestamp);
			inputParams.put("IN_SOURCE_CODE", sourceCode);			

			dataRequest.setInputParams(inputParams);

			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
			CachedResultSet rs = null;
			rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

			if (rs == null) {
				logger.error("Could not obtain DeliveryFee data from the database.  Query returned no data.");
				throw (new Exception("Could not obtain Delivery Fee data from the database. Query returned no data."));
			}

			rs.next();
			
			if (rs.getString("MORNING_DELIVERY_FEE") != null) {	
				id = rs.getString("DELIVERY_FEE_ID");
				bbnFee = rs.getString("MORNING_DELIVERY_FEE");
			}

			if (id == null || bbnFee == null) {				
				if (logger.isDebugEnabled()) {
					logger.debug("RecalculateOrderDAO returned\n" + "DeliveryFeeId: " + id + "\n" + "Morning Delivery Fee: " + bbnFee);
				}
			}
			return bbnFee;
		} catch (Exception e) {
			logger.error("Error caught calculating BBN Fee - " + e.getMessage());
			return null;
		}
	}
	
	/** DAO method to get the source master$ details for a given source code and time stamp. 
	 * The current proc returns morning_delivery_flag, Morning_delivery_free_shipping, delivery_fee_id. 
	 * We may modify the cursor to return required data. 
	 * @param cal
	 * @param sourceCode
	 * @param conn
	 * @return
	 */
	  public RecalculateOrderSourceCodeVO getSourceCodeHistDetails(Calendar cal, String sourceCode, Connection conn) throws Exception {

	    CachedResultSet searchResults = null;
	    RecalculateOrderSourceCodeVO sourceVO = null; 

	    DataRequest dataRequest = new DataRequest();
	    dataRequest.setConnection(conn);
	    dataRequest.setStatementID("GET_SOURCE_CODE_HIST_RECORD");
	    dataRequest.addInputParam("IN_SOURCE_CODE", sourceCode);
	  
	    if (cal != null) {
	    	dataRequest.addInputParam("IN_ORDER_TIME", new Timestamp(cal.getTimeInMillis()));
	    } else {
	      dataRequest.addInputParam("IN_ORDER_TIME", new Timestamp(new java.util.Date().getTime())); 
	    }

	    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
	    searchResults = (CachedResultSet) dataAccessUtil.execute(dataRequest);

	    while(searchResults.next()) {
	    	sourceVO = new RecalculateOrderSourceCodeVO();
	    	sourceVO.setSourceCode(sourceCode);
			sourceVO.setMorningDeliveryFlag(getValue(searchResults.getObject(SOURCE_CODE_MORNING_DELIVERY_FLAG)));
			sourceVO.setAllowBBNForFSMemeber(getValue(searchResults.getObject(SOURCE_CODE_MORNING_DELIVERY_FS)));
			break;
	    }	    
	    return sourceVO;
	  }

	/** Given a order guid, retrieves the order origin Id.
	 * @param guid
	 * @param connection
	 * @return
	 */
	public String getOrderOrigin(String orderGUID, Connection connection) {
		try{
			CachedResultSet result = null;
			
		    DataRequest dataRequest = new DataRequest();
		    dataRequest.setConnection(connection);
		    
		    dataRequest.setStatementID("VIEW_ORDERS");
		    dataRequest.addInputParam("IN_ORDER_GUID", orderGUID);
		    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
		    result = (CachedResultSet) dataAccessUtil.execute(dataRequest);
	
		    if(result.next()) {
		    	return result.getString("ORDER_ORIGIN");
		    }	
		} catch (Exception e) {
			logger.error("Error caught getting the order origin for a given order guid : " + orderGUID);
		}
		return null;
	}

}
