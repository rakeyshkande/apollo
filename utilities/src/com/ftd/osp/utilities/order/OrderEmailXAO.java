package com.ftd.osp.utilities.order;

import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.order.vo.*;
import com.ftd.osp.utilities.order.*;

import java.sql.*;
import java.math.*;

import java.util.Iterator;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;

import org.w3c.dom.Element;

/**
 * Order utility for building xml string stored in database for email confirmations
 *
 * @author Doug Johnson
 */
public class OrderEmailXAO
{
    public OrderEmailXAO()
    {
    }

  /**
   * Build XML string containing order information for email confirmation.
   * Only store order information needed for email, not entire order object
   * 
   * 
   * @param OrderVO order
   * @return Document //returns XML document containing order information
   */
    public Document generateOrderEmailXML(OrderVO order) throws ParserConfigurationException, Exception
    {
        Document orderDocument = DOMUtil.getDefaultDocument();
        Element tagElement = null;
      
        // Create order root
        Element orderElement = orderDocument.createElement("ORDER");
        orderDocument.appendChild(orderElement);

        Element linesElement = orderDocument.createElement("ORDER_LINES");
        orderElement.appendChild(linesElement);

        BigDecimal totalCharge = new BigDecimal(0);
        
        // Add items to the document
        if(order.getOrderDetail() != null && order.getOrderDetail().size() > 0)
        {
            OrderDetailsVO item = null;
            Iterator itemIterator = order.getOrderDetail().iterator();          
            while(itemIterator.hasNext())
            {
                Element lineElement = orderDocument.createElement("ORDER_LINE");
                linesElement.appendChild(lineElement);
            
                item = (OrderDetailsVO) itemIterator.next();

                lineElement.appendChild(this.createTextNode("ORDER_NBR", order.getMasterOrderNumber(), orderDocument, tagElement));
                lineElement.appendChild(this.createTextNode("REFNBR", item.getExternalOrderNumber(), orderDocument, tagElement));
                lineElement.appendChild(this.createTextNode("ORDER_DATE", order.getOrderDate(), orderDocument, tagElement));
                lineElement.appendChild(this.createTextNode("DELIVERY_DATE", item.getDeliveryDate(), orderDocument, tagElement));

                Element deliveryElement = orderDocument.createElement("DELIVERY_TO");
                lineElement.appendChild(deliveryElement);                

                //get buyer object
                BuyerVO buyer = null;          
                Iterator buyerIterator = order.getBuyer().iterator();
                while(buyerIterator.hasNext())
                {
                    buyer = (BuyerVO) buyerIterator.next();
                }
                
                //get recipient object
                RecipientsVO recipient = null;
                RecipientAddressesVO address = null;                
                Iterator recipientIterator = item.getRecipients().iterator();

                while(recipientIterator.hasNext())
                {
                    recipient = (RecipientsVO) recipientIterator.next();                   
                    Iterator addressIterator = recipient.getRecipientAddresses().iterator();
                    
                    while(addressIterator.hasNext())
                    {
                        address = (RecipientAddressesVO) addressIterator.next();
                    }                    
                }
                
                deliveryElement.appendChild(this.createTextNode("CUSTOMER_NAME",buyer.getFirstName() + " " + buyer.getLastName(), orderDocument, tagElement));

                deliveryElement.appendChild(this.createTextNode("ADDRESS_1",address.getAddressLine1(), orderDocument, tagElement));
                deliveryElement.appendChild(this.createTextNode("ADDRESS_2",address.getAddressLine2(), orderDocument, tagElement));
                deliveryElement.appendChild(this.createTextNode("CITY",address.getCity(), orderDocument, tagElement));
                deliveryElement.appendChild(this.createTextNode("ST",address.getStateProvince(), orderDocument, tagElement));
                deliveryElement.appendChild(this.createTextNode("COUNTRY",address.getCountry(), orderDocument, tagElement));
                deliveryElement.appendChild(this.createTextNode("ZIP",address.getPostalCode(), orderDocument, tagElement));

                Element itemInfoElement = orderDocument.createElement("ITEM");
                lineElement.appendChild(itemInfoElement);      
                itemInfoElement.appendChild(this.createTextNode("DESCRIPTION",item.getItemDescription(), orderDocument, tagElement));
                itemInfoElement.appendChild(this.createTextNode("PRODUCT_ID",item.getProductId() , orderDocument, tagElement));
                itemInfoElement.appendChild(this.createTextNode("FIRST_CHOICE",item.getColorFirstChoice() , orderDocument, tagElement));
                itemInfoElement.appendChild(this.createTextNode("SECOND_CHOICE",item.getColorSecondChoice() , orderDocument, tagElement));
                itemInfoElement.appendChild(this.createTextNode("PRICE",item.getProductsAmount() , orderDocument, tagElement));

                lineElement.appendChild(this.createTextNode("DISCOUNT", item.getDiscountAmount(), orderDocument, tagElement));
                lineElement.appendChild(this.createTextNode("STANDARD_SHIPPING", item.getShippingFeeAmount(), orderDocument, tagElement));
                lineElement.appendChild(this.createTextNode("TAX", item.getTaxAmount(), orderDocument, tagElement));
                
                //gift message
                lineElement.appendChild(this.createTextNode("GIFT_MESSAGE", item.getCardMessage(), orderDocument, tagElement));
                
                //calculate item subtotal and add to total charge for order
                BigDecimal subTotal = new BigDecimal(0);
                BigDecimal itemPrice = new BigDecimal(item.getProductsAmount());
                BigDecimal itemDiscount = new BigDecimal(item.getDiscountAmount());
                BigDecimal itemTax = new BigDecimal(item.getTaxAmount());

                subTotal = (itemPrice.subtract(itemDiscount));
                subTotal = (subTotal.add(itemTax));
                lineElement.appendChild(this.createTextNode("SUBTOTAL", subTotal.toString(), orderDocument, tagElement));

                //recalculate Order Total
                totalCharge = totalCharge.add(subTotal);
                
                //ship method - translate in xsl 
                lineElement.appendChild(this.createTextNode("SHIP_BY", item.getShipMethod(), orderDocument, tagElement));
            }
        }
        orderElement.appendChild(this.createTextNode("TOTAL_CHARGE", totalCharge.toString(), orderDocument, tagElement));

        //get payment object
        PaymentsVO payment = null;      
        Iterator paymentIterator = order.getPayments().iterator();

        while(paymentIterator.hasNext())
        {
            payment = (PaymentsVO) paymentIterator.next();
        }

        if(payment == null ) {
            throw new Exception("Cannot retrieve order payment.");
        }
        orderElement.appendChild(this.createTextNode("PAYMENT_TYPE", payment.getPaymentType(), orderDocument, tagElement));
        orderElement.appendChild(this.createTextNode("CREDIT_CARD_TYPE", payment.getDescription(), orderDocument, tagElement));
            
        return orderDocument;
    }

    private Element createTextNode(String elementName, String elementValue, Document document, Element tagElement)
    {
        tagElement = document.createElement(elementName);
        if(elementValue != null)
        {
            tagElement.appendChild(document.createTextNode(elementValue));
        }

        return tagElement;
    }

    public static void main(String[] args)
    {
        OrderEmailXAO orderEmailXAO = new OrderEmailXAO();
        Connection connection = null;
        
        try
        {
            connection = orderEmailXAO.getConnection();

            ScrubMapperDAO scrubMapperDAO = new ScrubMapperDAO(connection);
            OrderVO order = scrubMapperDAO.mapOrderFromDB("123");
            
            Document doc = (Document) orderEmailXAO.generateOrderEmailXML(order);
            DOMUtil.print(doc, System.out);
            
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            try { 
            connection.close(); }
            catch(SQLException se) {}
        }
    }

    private Connection getConnection() 
    {
        Connection connection = null;
        String driver_ = "oracle.jdbc.driver.OracleDriver";
        String database_ = "jdbc:oracle:thin:@sisyphus.ftdi.com:1521:DEV2";
        String user_ = "osp";
        String password_ = "osp";

        try
        {
            Class.forName(driver_);
            connection = DriverManager.getConnection(database_, user_, password_);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return connection;
    }
}