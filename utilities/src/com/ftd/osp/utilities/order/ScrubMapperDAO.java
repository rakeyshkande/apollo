package com.ftd.osp.utilities.order;

import java.io.ByteArrayInputStream;

import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Clob;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;

import com.ftd.ftdutilities.CalculateTaxUtil;
import com.ftd.ftdutilities.FieldUtils;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.PartnerUtility;
import com.ftd.osp.utilities.constants.PaymentExtensionConstants;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.order.vo.AVSAddressScoreVO;
import com.ftd.osp.utilities.order.vo.AVSAddressVO;
import com.ftd.osp.utilities.order.vo.AddOnsVO;
import com.ftd.osp.utilities.order.vo.BuyerAddressesVO;
import com.ftd.osp.utilities.order.vo.BuyerCommentsVO;
import com.ftd.osp.utilities.order.vo.BuyerEmailsVO;
import com.ftd.osp.utilities.order.vo.BuyerPhonesVO;
import com.ftd.osp.utilities.order.vo.BuyerVO;
import com.ftd.osp.utilities.order.vo.CoBrandVO;
import com.ftd.osp.utilities.order.vo.CreditCardsVO;
import com.ftd.osp.utilities.order.vo.DispositionsVO;
import com.ftd.osp.utilities.order.vo.FraudCommentsVO;
import com.ftd.osp.utilities.order.vo.MembershipsVO;
import com.ftd.osp.utilities.order.vo.OrderContactInfoVO;
import com.ftd.osp.utilities.order.vo.OrderDetailExtensionsVO;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.OrderExtensionsVO;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.order.vo.PaymentsVO;
import com.ftd.osp.utilities.order.vo.RecipientAddressesVO;
import com.ftd.osp.utilities.order.vo.RecipientCommentsVO;
import com.ftd.osp.utilities.order.vo.RecipientPhonesVO;
import com.ftd.osp.utilities.order.vo.RecipientsVO;
import com.ftd.osp.utilities.order.vo.TaxSplitOrder;
import com.ftd.osp.utilities.order.vo.TaxVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;
import com.ftd.osp.utilities.vo.SurchargeVO;
import com.ftd.osp.utilities.xml.DOMUtil;

/**
 * Data access class for Scrub schema 
 *
 * @author Doug Johnson
 */
public class ScrubMapperDAO implements OrderDataMapper
{
    private Connection connection;
    private Logger logger;

    private static final String GUID_KEY = "GUID";

    //procedures
    private static final String VIEW_ORDER_INFO = "SCRUB.VIEW_ORDER_INFO";
    private static final String VIEW_ORDER_ARCHIVE = "SCRUB.VIEW_ORDER_ARCHIVE";
    private static final String UPDATE_ORDER_HEADER = "SCRUB.UPDATE_ORDER_HEADER";
    private static final String UPDATE_ORDER_DETAILS = "SCRUB.UPDATE_ORDER_DETAILS";
    private static final String UPDATE_ADD_ONS = "SCRUB.UPDATE_ADD_ONS";
    private static final String UPDATE_PAYMENTS = "SCRUB.UPDATE_PAYMENTS";
    private static final String UPDATE_PAYMENT_NOCREDITCARD = "SCRUB.UPDATE_PAYMENTS_NOCREDITCARD";
    private static final String UPDATE_CREDIT_CARDS = "SCRUB.UPDATE_CREDIT_CARDS";
    private static final String UPDATE_CO_BRAND = "SCRUB.UPDATE_CO_BRAND";
    private static final String UPDATE_MEMBERSHIPS = "SCRUB.UPDATE_MEMBERSHIPS";
    private static final String UPDATE_BUYERS = "SCRUB.UPDATE_BUYERS";
    private static final String UPDATE_BUYER_ADDRESSES = "SCRUB.UPDATE_BUYER_ADDRESSES";
    private static final String UPDATE_BUYER_COMMENTS = "SCRUB.UPDATE_BUYER_COMMENTS";
    private static final String UPDATE_BUYER_PHONES = "SCRUB.UPDATE_BUYER_PHONES";
    private static final String UPDATE_BUYER_EMAILS = "SCRUB.UPDATE_BUYER_EMAILS";
    private static final String UPDATE_RECIPIENT = "SCRUB.UPDATE_RECIPIENTS";
    private static final String UPDATE_RECIPIENT_PHONES = "SCRUB.UPDATE_RECIPIENT_PHONES";
    private static final String UPDATE_RECIPIENT_ADDRESSES = "SCRUB.UPDATE_RECIPIENT_ADDRESSES";
    private static final String UPDATE_ORDER_CONTACT_INFO = "SCRUB.UPDATE_ORDER_CONTACT_INFO";
    private static final String INSERT_BUYERS = "SCRUB.INSERT_BUYER";
    private static final String INSERT_BUYER_ADDRESSES = "SCRUB.INSERT_BUYER_ADDRESSES";
    private static final String INSERT_BUYER_COMMENTS = "SCRUB.INSERT_BUYER_COMMENTS";
    private static final String INSERT_BUYER_PHONES = "SCRUB.INSERT_BUYER_PHONES";
    private static final String INSERT_BUYER_EMAILS = "SCRUB.INSERT_BUYER_EMAILS";
    private static final String INSERT_ORDER_HEADER = "SCRUB.INSERT_ORDER_HEADER";
    private static final String INSERT_ORDER_DETAILS = "SCRUB.INSERT_ORDER_DETAILS";
    private static final String INSERT_ADD_ONS = "SCRUB.INSERT_ADD_ONS";
    private static final String INSERT_AVS_ADDRESS = "SCRUB.INSERT_AVS_ADDRESS";
    private static final String INSERT_AVS_ADDRESS_SCORE = "SCRUB.INSERT_AVS_ADDRESS_SCORE";
    private static final String INSERT_PAYMENTS = "SCRUB.INSERT_PAYMENTS";
    private static final String INSERT_CREDIT_CARDS = "SCRUB.INSERT_CREDIT_CARDS";
    private static final String INSERT_CO_BRAND = "SCRUB.INSERT_CO_BRAND";
    private static final String INSERT_RECIPIENT = "SCRUB.INSERT_RECIPIENTS";
    private static final String INSERT_RECIPIENT_PHONES = "SCRUB.INSERT_RECIPIENT_PHONES";
    private static final String INSERT_RECIPIENT_ADDRESSES = "SCRUB.INSERT_RECIPIENT_ADDRESSES";
    private static final String INSERT_RECIPIENT_COMMENTS = "SCRUB.INSERT_RECIPIENT_COMMENTS";
    private static final String INSERT_ORDER_ARCHIVE = "SCRUB.INSERT_ORDER_ARCHIVE";
    private static final String INSERT_ORDER_CONTACT_INFO = "SCRUB.INSERT_ORDER_CONTACT_INFO";
    private static final String INSERT_MEMBERSHIP = "SCRUB.INSERT_MEMBERSHIPS";
    private static final String INSERT_ORDER_DISPOSITION = "SCRUB.INSERT_ORDER_DISPOSITION";
    private static final String INSERT_FRAUD_COMMENTS = "SCRUB.INSERT_FRAUD_COMMENTS";
    private static final String INSERT_FRAUD_CODES = "SCRUB.INSERT_NOVATOR_FRAUD_CODES";    
    private static final String GET_SOURCE_CODE_RECORD = "GLOBAL.GET_SOURCE_CODE_RECORD";
    private static final String DELETE_ADD_ONS = "SCRUB.DELETE_ADD_ONS";
    private static final String INSERT_ORDER_EXT = "SCRUB.INSERT_ORDER_EXT";
    private static final String DELETE_ORDER_EXT = "SCRUB.DELETE_ORDER_EXT";
    private static final String INSERT_ORDER_DETAIL_EXT = "SCRUB.INSERT_ORDER_DETAIL_EXT";
    private static final String DELETE_ORDER_DETAIL_EXT = "SCRUB.DELETE_ORDER_DETAIL_EXT";
    
    //Order ref cursors returned from database procedure
    private static final String ORDER_HEADER_CURSOR = "RegisterOutParameterOrdercur";
    private static final String ORDER_ITEM_CURSOR = "RegisterOutParameterOrderItemcur";
    private static final String ORDER_ADD_ONS = "RegisterOutParameterAddOncur";
    private static final String ORDER_CO_BRANDS = "RegisterOutParameterCoBrandcur";
    private static final String ORDER_PAYMENTS = "RegisterOutParameterPaymentcur";
    private static final String ORDER_CREDIT_CARDS = "RegisterOutParameterCreditCardcur";
    private static final String BUYER = "RegisterOutParameterBuyercur";
    private static final String BUYER_PHONES = "RegisterOutParameterBuyerPhonecur";
    private static final String BUYER_COMMENTS = "RegisterOutParameterBuyerCommentscur";
    private static final String BUYER_ADDRESSES = "RegisterOutParameterBuyerAddrcur";
    private static final String BUYER_EMAILS = "RegisterOutParameterBuyerEmailcur";
    private static final String ORDER_MEMBERSHIPS = "RegisterOutParameterMembershipscur";
    private static final String AVS_ADDRESS = "RegisterOutParameterAvsAdresscur";
    private static final String AVS_ADDRESS_SCORES = "RegisterOutParameterAvsAdressScorescur";
    private static final String RECIPIENT = "RegisterOutParameterRecipeintscur";
    private static final String RECIPIENT_ADDRESSES = "RegisterOutParameterRecipeintsAddrcur";
    private static final String RECIPIENT_COMMENTS = "RegisterOutParameterRecipeintsCommentscur";
    private static final String RECIPIENT_PHONES = "RegisterOutParameterRecipeintsPhonecur";
    private static final String ORDER_CONTACT_INFO = "RegisterOutParameterContactInfocur";
    private static final String ORDER_DISPOSITIONS = "RegisterOutParameterOrderDisp";
    private static final String ORDER_FRAUD_COMMENTS = "RegisterOutParameterFraudCom";
    private static final String ORDER_FRAUD_CODES = "RegisterOutParameterFraudCodes";
    private static final String ORDER_DETAIL_EXTENSIONS = "RegisterOutParameterOrderDetailExtCur";

    
    //custom parameters returned from database procedure
    private static final String STATUS_PARAM = "RegisterOutParameterStatus";
    private static final String MESSAGE_PARAM = "RegisterOutParameterMessage";

    private String status;
    private String message;
    private boolean returnStatus;
    private HashSet<String> preferredPartnerList;
    private HashSet<String> preferredPartnerNameList;
    
    private String contactPhoneNumber = null;
    private String contactPhoneExt = null;
    private static final String PHONE_TYPE_HOME = "HOME";
    private static final String PHONE_TYPE_WORK = "WORK";
	private static final String ADD_ON_DISCOUNT_AMOUNT = "ADD_ON_DISCOUNT_AMOUNT";
	
	
    private static final String RESET_CARDINAL_DATA = "SCRUB.RESET_CARDINAL_DATA";
    
    private static final String PG_JCCAS = "PG-JCCAS";
    private static final String PG_PS = "PG-PS";

  /**
   * ScrubMapperDAO cunstructor
   * @param connection The Connection that will be used to access the order
   *
   */
  public ScrubMapperDAO(Connection connection)
  {
      this.connection = connection;
      this.logger = new Logger("com.ftd.osp.utilities.order.ScrubMapperDAO");
      this.returnStatus = true;
      this.preferredPartnerList = null;
      this.preferredPartnerNameList = null;
  }

  /**
   * Insert an order into the Scrub Tables
   * 
   * @param OrderVO order
   * @return true/false success of insert
   */
  public boolean mapOrderToDB(OrderVO order) throws Exception
  {
       // Insert Order 
      long time = 0L;
      long totalTime = System.currentTimeMillis();
      boolean success = true;

      time = System.currentTimeMillis();
      success = insertBuyers(order);
      logger.info("Time taken to insert buyers is " + (System.currentTimeMillis() - time)+ "ms");
      
      if(success){
        // Insert Buyer Addresses
        time = System.currentTimeMillis();
        success = insertBuyerAddresses(order);   
        logger.info("Time taken to insert buyer addresses is " + (System.currentTimeMillis() - time)+ "ms");
      }
      
      //if(success)
        // Insert Buyer Comments
      // success = insertBuyerComments(order);    

      if(success){
        // Insert Contact Information
        time = System.currentTimeMillis();
        success = insertOrderContactInfo(order);
        logger.info("Time taken to insert contact info is " + (System.currentTimeMillis() - time)+ "ms");
      }
      
      if(success){
        // Insert Buyer Phones
        time = System.currentTimeMillis();
        success = insertBuyerPhones(order); 
        logger.info("Time taken to insert buyer phones is " + (System.currentTimeMillis() - time)+ "ms");
      }

      if(success){
        // Insert Buyer Emails
        time = System.currentTimeMillis();
        success = insertBuyerEmails(order);
        logger.info("Time taken to insert buyer emails is " + (System.currentTimeMillis() - time)+ "ms");
      }

      if(success){
        // Insert Recipients
        time = System.currentTimeMillis();
        success = insertRecipients(order);
        logger.info("Time taken to insert recipients is " + (System.currentTimeMillis() - time)+ "ms");
      }

      if(success){
        // Insert Recipient addresses
        time = System.currentTimeMillis();
        success = insertRecipientAddresses(order);
        logger.info("Time taken to insert recipient addresses is " + (System.currentTimeMillis() - time)+ "ms");
      }

      if(success){
          // Insert AVS address
          time = System.currentTimeMillis();
          success = insertAVSAddress(order);
          logger.info("Time taken to insert avs address is " + (System.currentTimeMillis() - time)+ "ms");
      }
      
      if(success){
          // Insert AVS address scores
          time = System.currentTimeMillis();
          success = insertAVSAddressScores(order);
          logger.info("Time taken to insert avs address scores is " + (System.currentTimeMillis() - time)+ "ms");
      }

      if(success){
        // Insert Recipient phones
        time = System.currentTimeMillis();
        success = insertRecipientPhones(order);  
        logger.info("Time taken to insert recipient phones is " + (System.currentTimeMillis() - time)+ "ms");
      }

      //if(success)
        // Insert Recipient comments
      //  success = insertRecipientComments(order);  

      if(success){
        // Insert Order Items
        time = System.currentTimeMillis();
        success = insertOrderItems(order);
        logger.info("Time taken to insert order items is " + (System.currentTimeMillis() - time)+ "ms");
      }

      if(success){
        // Insert Add Ons
        time = System.currentTimeMillis();
        success = insertAddOns(order);
        logger.info("Time taken to insert add ons is " + (System.currentTimeMillis() - time)+ "ms");
      }

      if(success){
        //Parse through CoBrand information to see if any
        //Membership information is in there.  If so, pull it out
        //and put in into the MembershipsVO
        time = System.currentTimeMillis();
        success = separateCoBrandMembershipInfo(order);
        logger.info("Time taken to separate co-brand membership info is " + (System.currentTimeMillis() - time)+ "ms");
      }
    
     if(success){
        // Insert Memberships
        time = System.currentTimeMillis();
        success = insertMembership(order);
        logger.info("Time taken to insert memberships is " + (System.currentTimeMillis() - time)+ "ms");
     }

     if(success){
        // Insert Order Header
        time = System.currentTimeMillis();
        success = insertOrderHeader(order);
        logger.info("Time taken to insert order header is " + (System.currentTimeMillis() - time)+ "ms");
     }

     if(success){
        // Insert Co Brand
        time = System.currentTimeMillis();
        success = insertCoBrand(order);   
        logger.info("Time taken to insert co-brand is " + (System.currentTimeMillis() - time)+ "ms");
     }

      if(success){
        // Insert Credit Cards
        time = System.currentTimeMillis();
        success = insertCreditCards(order); 
        logger.info("Time taken to insert credit cards is " + (System.currentTimeMillis() - time)+ "ms");
      }
        

      if(success){
        // Insert Payments
        time = System.currentTimeMillis();
        success = insertPayments(order);
        logger.info("Time taken to insert payments is " + (System.currentTimeMillis() - time)+ "ms");
      }

      if(success){
        // Insert insertFraudCodes
        time = System.currentTimeMillis();
        success = insertFraudCodes(order);
        logger.info("Time taken to insert fraud codes is " + (System.currentTimeMillis() - time)+ "ms");
      }

      if(success){
        // Insert Fraud Comments
        time = System.currentTimeMillis();
        success = updateFraudComments(order);
        logger.info("Time taken to insert fraud comments is " + (System.currentTimeMillis() - time)+ "ms");
      }

      if(success){
        // Insert any Order Detail Extensions
        time = System.currentTimeMillis();
        success = insertOrderDetailExtensions(order);
        logger.info("Time taken to insert order detail extensions is " + (System.currentTimeMillis() - time)+ "ms");
      }
      
      if(success){
        // Insert any Order Detail Extensions
        time = System.currentTimeMillis();
        success = insertOrderExtensions(order);
        logger.info("Time taken to insert order extensions is " + (System.currentTimeMillis() - time)+ "ms");
      }
      
      logger.info("Time taken to insert the entire order is " + (System.currentTimeMillis() - totalTime)+ "ms");
      return success;

  }

 /**
   * Retrieve an order from the database
   * 
   * @param guid String order id
   * @return OrderVO
   */
  public OrderVO mapOrderFromDB(String guid) throws Exception
  {  
        long time = System.currentTimeMillis();
        OrderVO order = new OrderVO();
        Map orderMap = null;

        // Get Order
        order.setGUID(guid);

        //Retrieve entire order
        orderMap = getOrderMap(order);
        
        if(orderMap != null)
        {
          // Get Order Header
          order = getOrderHeader(orderMap,order);

          //Get fraud comments
          Collection fraudComments  = getFraudComments(orderMap);
          order.setFraudComments((List)fraudComments);

          //Get fraud codes
          Collection fraudCodes = getFraudCodes(orderMap);
          order.setFraudCodes((List)fraudCodes);

          // Get Contact Info
          Collection contactInfo = getContactInfo(orderMap);
          order.setOrderContactInfo((List)contactInfo);
          
          // Get Order Items
          Collection items  = getOrderItems(orderMap);
          order.setOrderDetail((List)items);
          
          updateMDFforPartnerOrders(order);

          // Get Co Brand
          Collection coBrand = getCoBrand(orderMap);
          order.setCoBrand((List)coBrand);

          // Get Membership
          Collection membership = getMembership(orderMap);
          order.setMemberships((List)membership);

          // Get Payments
          Collection payments  = getPayments(orderMap);
          order.setPayments((List)payments);

          // Get Buyer
          Collection allBuyerEmails = getAllBuyerEmails(orderMap);
          Collection buyers = getBuyer(orderMap, allBuyerEmails);
          order.setBuyer((List)buyers);
          
          // Get the Buyer Email Address
          order.setBuyerEmailAddress(getBuyerEmailAddress(order.getBuyerEmailId(), order.getBuyerId(), allBuyerEmails));
        }

        // Preferred Partner (e.g., USAA).
        // Note that preferredPartnerList is set in getOrderItems.
        order.setPreferredProcessingPartners(preferredPartnerList);
        order.setPreferredProcessingPartnerNames(preferredPartnerNameList);

        // Set the changed flags to false
        order.setChangeFlags(false);
        
        MercentOrderPrefixes mrcntOrderPrefixes = new MercentOrderPrefixes(this.connection);
        if(mrcntOrderPrefixes.isMercentOrder(order.getOrderOrigin())) {
        	order.setIsMercentOrder("Y");
        	order.setMrcntChannelIcon(mrcntOrderPrefixes.getMercentIconForChannel(order.getOrderOrigin()));
        	order.setMrcntChannelName(mrcntOrderPrefixes.getChannelName(order.getOrderOrigin())); 
        }
        
        //If it is partner order, set the flag
        CachedResultSet partnerCrs = getPartnerOriginsInfo(order.getOrderOrigin(), order.getSourceCode());
        if(partnerCrs!=null && partnerCrs.next() && partnerCrs.getString("APPLY_DEF_PTN_BEHAVIOR").equals("Y")){
        	order.setPartnerOrder(true);
        	order.setPartnerImgURL(partnerCrs.getString("PARTNER_IMAGE"));
        	order.setPartnerName(partnerCrs.getString("PARTNER_NAME"));
        }        
        
        logger.info("Time taken to map order from db is " + (System.currentTimeMillis() - time)+ "ms");
        return order;
  }
     
	private void updateMDFforPartnerOrders(OrderVO order) {
		if (new PartnerUtility().isPartnerOrder(order.getOrderOrigin(),order.getSourceCode(), connection)) {
			for (Object orderItem : order.getOrderDetail()) {
				OrderDetailsVO orderItemDetail  = (OrderDetailsVO)orderItem;
				// fee is greater than 0 but original order no MDF for partner orders, it means that it is opted out in Apollo.
				if(!"Y".equals(orderItemDetail.getOriginalOrderHasMDF()) && 
						(orderItemDetail.getMorningDeliveryFee() != null && 
								new BigDecimal(orderItemDetail.getMorningDeliveryFee()).compareTo(new BigDecimal(0)) > 0)) {
					logger.info("Updating morning delivery opted to N for partner orders as Original order has No MDF");
					orderItemDetail.setMorningDeliveryOpted("N");
				}
			}
		}

	}

/**
   * Update an order in the database.
   * 
   * @param order id
   * @return  true/false success of update
   */
  public boolean updateOrder(OrderVO order) throws Exception
  {
    boolean success = true;
    long time = 0L;
    long totalTime = System.currentTimeMillis();
    
    // Update Order Items
    if(success){
      time = System.currentTimeMillis();
       success = updateOrderItems(order);
      logger.info("Time taken to update order items is " + (System.currentTimeMillis() - time)+ "ms");
    }

    if(success){
      time = System.currentTimeMillis();
      // Insert Add Ons..addons are always inserted and not updated, 12/17/03-emueller
      success = insertAddOns(order);
      logger.info("Time taken to insert add-ons is " + (System.currentTimeMillis() - time)+ "ms");
    }
    
    if(success){
      time = System.currentTimeMillis();
      // Update Recipients
      success = updateRecipients(order);
      logger.info("Time taken to update recipients is " + (System.currentTimeMillis() - time)+ "ms");
    }

    if(success){
      time = System.currentTimeMillis();
      // Update Recipient phones
      success = updateRecipientPhones(order);
      logger.info("Time taken to update recipient phones is " + (System.currentTimeMillis() - time)+ "ms");
    }

    if(success){
      time = System.currentTimeMillis();
      // Update Recipient addresses
      success = updateRecipientAddresses(order);
      logger.info("Time taken to update recipient addresses is " + (System.currentTimeMillis() - time)+ "ms");
    }

    if(success){
      time = System.currentTimeMillis();
      // Update Co Brand
      success = updateCoBrand(order);
      logger.info("Time taken to update co-brand is " + (System.currentTimeMillis() - time)+ "ms");
    }

    if(success){
      time = System.currentTimeMillis();
      // Update Membership
      success = updateMembership(order);
      logger.info("Time taken to update membership info is " + (System.currentTimeMillis() - time)+ "ms");
    }
      
    if(success){
      time = System.currentTimeMillis();
      // Update Order Header
      success = updateOrderHeader(order);
      logger.info("Time taken to update order header is " + (System.currentTimeMillis() - time)+ "ms");
    }

    if(success){
      time = System.currentTimeMillis();
      // Update Credit Cards
      success = updateCreditCards(order);
      logger.info("Time taken to update credit cards is " + (System.currentTimeMillis() - time)+ "ms");
    }

    if(success){
      time = System.currentTimeMillis();
      // Update Payments
      success = updatePayments(order);
      logger.info("Time taken to update payments is " + (System.currentTimeMillis() - time)+ "ms");
    }

    //New payments may have been added to the order.
    //This will make sure any new payments are inserted into the DB.
    //This is related to defect #439  (emueller, 3/5/04)
    if(success){
      time = System.currentTimeMillis();
      // Update Payments
      success = insertPayments(order);
      logger.info("Time taken to insert payments is " + (System.currentTimeMillis() - time)+ "ms");
    }
    
    if(success){
      time = System.currentTimeMillis();
      // Update Buyer
      success = updateBuyers(order);
      logger.info("Time taken to update buyers is " + (System.currentTimeMillis() - time)+ "ms");
    }

    if(success){
      time = System.currentTimeMillis();
      // Update Buyer Addresses
      success = updateBuyerAddresses(order);    
      logger.info("Time taken to update buyer addresses is " + (System.currentTimeMillis() - time)+ "ms");
    }
    
    //Don't update the order extensions.  This is not pulled in in scrub as part of the order in VIEW_ORDER_INFO
    //If we ever modify order extensions in scrub, this will have to be uncommented and VIEW_ORDER_INFO will need to be
    //modified to return the order extensions
      //if(success){
       // time = System.currentTimeMillis();
        //success = updateOrderExtensions(order);
        //logger.info("Time taken to update order extensions is " + (System.currentTimeMillis() - time)+ "ms");
      //}

     //if(success)
      // Update Buyer Comments
      //success = updateBuyerComments(order);    

     if(success){
      time = System.currentTimeMillis();
      // Update Buyer Phones
      success = updateBuyerPhones(order); 
      logger.info("Time taken to update buyer phones is " + (System.currentTimeMillis() - time)+ "ms");
     }

     if(success){
      time = System.currentTimeMillis();
      // Update Buyer Emails
      success = updateBuyerEmails(order); 
      logger.info("Time taken to update buyer emails is " + (System.currentTimeMillis() - time)+ "ms");
     }

     if(success){
      time = System.currentTimeMillis();
      // Update Contact Info
      success = updateOrderContactInfo(order); 
      logger.info("Time taken to update order contact info is " + (System.currentTimeMillis() - time)+ "ms");
     }

     if(success){
        // Insert Dispositions
        time = System.currentTimeMillis();
        success = updateDispositions(order);
        logger.info("Time taken to update dispositions is " + (System.currentTimeMillis() - time)+ "ms");
      }

      if(success){
        // Insert Fraud Comments
        time = System.currentTimeMillis();
        success = updateFraudComments(order);
        logger.info("Time taken to update fraud comments is " + (System.currentTimeMillis() - time)+ "ms");
      }

      if(success){
        // Update any Order Detail Extensions
        time = System.currentTimeMillis();
        success = updateOrderDetailExtensions(order);
        logger.info("Time taken to update order detail extensions is " + (System.currentTimeMillis() - time)+ "ms");
      }
            
    logger.info("Time taken to update the entire order is " + (System.currentTimeMillis() - totalTime)+ "ms");
    return success;
  }

  /**
   * Retrieve the order header information
   * 
   * @param order OrderVO value object that contains the whole order
   * @return  true/false success of update
   */
  private Map getOrderMap(OrderVO order) throws Exception
  {
      // Get the order header information
      DataRequest dataRequest = new DataRequest();
      dataRequest.setConnection(connection);
      dataRequest.setStatementID(VIEW_ORDER_INFO);
      dataRequest.addInputParam(GUID_KEY, order.getGUID());

      Map orderMap = null;
      
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      orderMap = (Map)dataAccessUtil.execute(dataRequest);

      return orderMap;
  }

  /**
   * Insert the original xml into the Order_Archive Table
   * 
   * @param String orderXML contains the original xml transmission
   * @param String masterOrderNumber 
   * @return true/false success of insert
   */
  public boolean mapXMLToDB(String orderXML, String masterOrderNumber) throws Exception
  {
    DataRequest dataRequest = null;
    HashMap archiveMap = null;
    int tsid = 1;
    int min = 0;
    int max = 0;


    int totalTransmissionLength = orderXML.length();
    int sectionLength = 4000;
    int totalSections = totalTransmissionLength/sectionLength;
    int remainder = totalTransmissionLength % sectionLength;

    if(remainder > 0)
      totalSections++;
      
    try{  

      for(int i=0; i<totalSections; i++)
      {
        archiveMap = new HashMap();
        dataRequest = new DataRequest();
        archiveMap.put("IN_MASTER_ORDER_NUMBER", masterOrderNumber);
        Integer id = new Integer(tsid);
        archiveMap.put("IN_SECTION", id);
              
        if( ( (i+1) == totalSections && remainder == 0 )  || ( (i+1) < totalSections && remainder != 0 ) )
          max = max + 4000;
        else
          max = max + remainder;
      
        archiveMap.put("IN_TRANSMISSION", orderXML.substring(min,max));

        dataRequest.setConnection(connection);
        dataRequest.setStatementID(INSERT_ORDER_ARCHIVE);
        dataRequest.setInputParams(archiveMap);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
 
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
        status = (String) outputs.get(STATUS_PARAM);

        if(status.equals("N"))
        {
            message = (String) outputs.get(MESSAGE_PARAM);
            returnStatus = false;
            throw new Exception(message);
        }
        else
        {
            tsid++;
            min = max;
            returnStatus = true;
        }
     
      }
    }
    finally{}
    
    return returnStatus;
  }

  /**
   * Retrieve the original xml into the Order_Archive Table
   * 
   * @param masterOrderNumber 
   * @exception Exception
   * @return Document an xml document representing the archived order
   * 
   */
  public Document mapXMLFromDB(String masterOrderNumber) throws Exception
  {
      String transmissionString = "";
      DataRequest dataRequest = null;



      try
      {
          dataRequest = new DataRequest();
          dataRequest.setConnection(connection);
          dataRequest.setStatementID(VIEW_ORDER_ARCHIVE);
          dataRequest.addInputParam("IN_MASTER_ORDER_NUMBER", masterOrderNumber);

          DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
          CachedResultSet transmissionResults = (CachedResultSet) DataAccessUtil.getInstance().execute(dataRequest);
          
          while(transmissionResults.next())
          {
              //logger.debug("Loading sequence " + ((BigDecimal) transmissionResults.getObject(2)).toString() + " of the archived xml transmission for order " + (String) transmissionResults.getObject(1) + ".");
              logger.debug("Loading sequence " + transmissionResults.getObject(2) + " of the archived xml transmission for order " + transmissionResults.getObject(1) + ".");
              transmissionString = transmissionString + (String) transmissionResults.getObject(3);
          }
      }
      
      finally
      {
          
      }

      return DOMUtil.getDocument(transmissionString);
  }
    
  /**
   * Retrieve the order header information.
   * 
   * @param orderMap Map
   * @param order OrderVO value object that contains the whole order
   * @return  true/false success of update
   */
  public OrderVO getOrderHeader(Map orderMap,OrderVO order) throws Exception
  {
      // Get the order header information
      Object buyerId = null;
      Object buyerEmailId = null;
      Object membershipId = null;
              
      CachedResultSet rs = new CachedResultSet();
      rs = (CachedResultSet) orderMap.get(ORDER_HEADER_CURSOR);
            
      while(rs != null && rs.next())
      {
          //cleanup holding objects
          buyerId = null;
          buyerEmailId = null;
          membershipId = null;

          order.setGUID((String)rs.getObject(1));
          order.setMasterOrderNumber((String)rs.getObject(2));

          //buyerId is an integer and nullable
          buyerId = rs.getObject(3);
          if(buyerId != null){
            //order.setBuyerId(new Long(buyerId.toString()).longValue());
            order.setBuyerId(Long.parseLong(buyerId.toString()));
          }
                
          order.setOrderTotal((String)rs.getObject(4));
          order.setProductsTotal((String)rs.getObject(5));
          order.setTaxTotal((String)rs.getObject(6));
          order.setServiceFeeTotal((String)rs.getObject(7));
          order.setShippingFeeTotal((String)rs.getObject(8));

          //buyerEmailId is an integer and nullable
          buyerEmailId = rs.getObject(9);
          if(buyerEmailId != null){
            //order.setBuyerEmailId(new Long(buyerEmailId.toString()).longValue());  
            order.setBuyerEmailId(Long.parseLong(buyerEmailId.toString()));  
          }

          order.setOrderDate((String)rs.getObject(10));
          if (logger.isDebugEnabled()) {
              logger.debug("getOrderHeader: Order date default string format: " + (String)rs.getObject(10));
          }
          
          order.setOrderOrigin((String)rs.getObject(11));

          //membershipId is an integer and nullable
          membershipId = rs.getObject(12);
          if(membershipId != null){
            //order.setMembershipId(new Long(membershipId.toString()).longValue());
            order.setMembershipId(Long.parseLong(membershipId.toString()));
          }
                
          order.setCsrId((String)rs.getObject(13));
          order.setAddOnAmountTotal((String)rs.getObject(14));
          order.setPartnershipBonusPoints((String)rs.getObject(15));
          order.setSourceCode((String)rs.getObject(16));
          order.setCallTime((String)rs.getObject(17));
          order.setDnisCode((String)rs.getObject(18));
          order.setDatamartUpdateDate((String)rs.getObject(19));
          order.setDiscountTotal((String)rs.getObject(20));
          order.setYellowPagesCode((String)rs.getObject(21));
          order.setAribaBuyerAsnNumber((String)rs.getObject(22));
          order.setAribaBuyerCookie((String)rs.getObject(23));
          order.setAribaPayload((String)rs.getObject(24));
          order.setSocketTimestamp((String)rs.getObject(25));
          order.setStatus((String)rs.getObject(26));
          order.setSourceDescription((String)rs.getObject(27));
          order.setPartnerId((String)rs.getObject(28));
          order.setCompanyId((String)rs.getObject(29));//logger.debug("###SMD Company ID "+(String)rs.getObject(29));
          order.setCompanyName((String)rs.getObject(30));//logger.debug("###SMD Company Name "+(String)rs.getObject(30));
          order.setCoBrandCreditCardCode((String)rs.getObject(31));
          order.setFraudFlag((String)rs.getObject(32));
          order.setPreviousStatus((String)rs.getObject(33));
          order.setLossPreventionIndicator((String)rs.getObject(34));
          order.setMpRedemptionRateAmt((String)rs.getObject(35));
          order.setBuyerSignedIn((String) rs.getObject(36));
          order.setFreeShippingUseFlag((String)rs.getObject(37));
          order.setFreeShippingPurchaseFlag((String)rs.getObject(38));
          order.setCompanyUrl((String)rs.getObject(39));//logger.debug("###SMD Company URL "+(String)rs.getObject(39));
          order.setLanguageId((String)rs.getObject(40));
          order.setSourceType((String)rs.getObject(41));
          order.setPartnerName((String)rs.getObject(42));
          order.setJointCartIndicator(rs.getString("is_joint_cart"));
          order.setAddOnDiscountAmount(rs.getString(ADD_ON_DISCOUNT_AMOUNT));
          order.setOeBuyerHasFreeShipping(rs.getString("buyer_has_free_shipping"));
          order.setOrderCalcType(rs.getString("order_calc_type"));
      }

      return order;
    }

   /**
   * Retrieve contact information.
   * 
   * @param orderMap Map
   * @return  Collection of contact information
   * @exception Exception
   */
   private Collection getContactInfo(Map orderMap) throws Exception
    {
        // Get the contact information

        Collection contactInfos = new ArrayList();
        OrderContactInfoVO contactInfo = null;

        CachedResultSet rs = new CachedResultSet();
        rs = (CachedResultSet) orderMap.get(ORDER_CONTACT_INFO);

        while(rs != null && rs.next())
        {
            contactInfo = new OrderContactInfoVO();

            //contactInfo.setOrderContactInfoId(new Long(rs.getObject(1).toString()).longValue());
            contactInfo.setOrderContactInfoId(Long.parseLong(rs.getObject(1).toString()));
            contactInfo.setOrderGuid((String)rs.getObject(2));
            contactInfo.setFirstName((String)rs.getObject(3));
            contactInfo.setLastName((String)rs.getObject(4));
            contactInfo.setPhone((String)rs.getObject(5));
            contactInfo.setExt((String)rs.getObject(6));
            contactInfo.setEmail((String)rs.getObject(7));
            
            contactInfos.add(contactInfo);
        }

        return contactInfos;
    }

  /**
   * Retrieve the order detail items.
   * 
   * @param orderMap Map
   * @return  Collection of items
   * @exception Exception 
   */
    private Collection getOrderItems(Map orderMap) throws Exception
    {
        Collection items = new ArrayList();
        OrderDetailsVO item = null;
        
        Collection allAddOns = getAddOns(orderMap);
        Collection allRecipients = getRecipient(orderMap);
        Collection allDispositions = getDispositions(orderMap);
        Collection allExtensions = getAllOrderDetailExtensions(orderMap);
        Collection allAVSAddress = getAVSAddresses(orderMap);

        
        CachedResultSet rs = new CachedResultSet();
        rs = (CachedResultSet) orderMap.get(ORDER_ITEM_CURSOR);
            
        Object recipientId = null;
        Object recipientAddressId = null;
 
        while(rs != null && rs.next())
        {
        	List<TaxVO> taxes = new ArrayList<TaxVO>();
        	//cleanup holding objects
            recipientId = null;
            recipientAddressId = null;
                
            item = new OrderDetailsVO();

            //item.setOrderDetailId(new Long(rs.getObject(1).toString()).longValue());
            item.setOrderDetailId(Long.parseLong(rs.getObject(1).toString()));
            item.setGuid((String)rs.getObject(2));
            item.setLineNumber((String)rs.getObject(3));

            //recipientId is an integer and nullable
            recipientId = rs.getObject(4);
            if(recipientId != null) {
              //item.setRecipientId(new Long(recipientId.toString()).longValue());
              item.setRecipientId(Long.parseLong(recipientId.toString()));
            }

            //recipientAddressId is an integer and nullable
            recipientAddressId = rs.getObject(5);
            if(recipientAddressId != null){
              //item.setRecipientAddressId(new Long(recipientAddressId.toString()).longValue());
              item.setRecipientAddressId(Long.parseLong(recipientAddressId.toString()));
            }

            item.setProductId((String)rs.getObject(6));
            item.setQuantity((String)rs.getObject(7));
            item.setShipVia((String)rs.getObject(8));
            item.setShipMethod((String)rs.getObject(9));
            item.setShipDate((String)rs.getObject(10));
            item.setDeliveryDate((String)rs.getObject(11));
            item.setDeliveryDateRangeEnd((String)rs.getObject(12));
            item.setDropShipTrackingNumber((String)rs.getObject(13));
            item.setProductsAmount((String)rs.getObject(14));
            item.setTaxAmount((String)rs.getObject(15));
            item.setServiceFeeAmount((String)rs.getObject(16));
            item.setShippingFeeAmount((String)rs.getObject(17));
            item.setAddOnAmount((String)rs.getObject(18));
            item.setDiscountAmount((String)rs.getObject(19));
            item.setOccassionId((String)rs.getObject(20));
            item.setLastMinuteGiftSignature((String)rs.getObject(21));
            item.setLastMinuteNumber((String)rs.getObject(22));
            item.setLastMinuteGiftEmail((String)rs.getObject(23));
            item.setExternalOrderNumber((String)rs.getObject(24));
            item.setColorFirstChoice((String)rs.getObject(25));
            item.setColorSecondChoice((String)rs.getObject(26));
            item.setSubstituteAcknowledgement((String)rs.getObject(27));
            item.setCardMessage((String)rs.getObject(28));
            item.setCardSignature((String)rs.getObject(29));
            item.setOrderComments((String)rs.getObject(30));
            item.setOrderContactInformation((String)rs.getObject(31));
            item.setFloristNumber((String)rs.getObject(32));
            item.setSpecialInstructions((String)rs.getObject(33));
            item.setAribaUnspscCode((String)rs.getObject(34));
            item.setAribaPoNumber((String)rs.getObject(35));
            item.setAribaAmsProjectCode((String)rs.getObject(36));
            item.setAribaCostCenter((String)rs.getObject(37));
            item.setExternalOrderTotal((String)rs.getObject(38));
            item.setSizeChoice((String)rs.getObject(39));
            item.setSenderInfoRelease((String)rs.getObject(40));
            item.setStatus((String)rs.getObject(41));
            item.setItemDescription((String)rs.getObject(42));
            item.setFloristName((String)rs.getObject(43));
            item.setMilesPoints((String)rs.getObject(44));
            item.setShipMethodCarrierFlag((String)rs.getObject(45));
            item.setShipMethodFloristFlag((String)rs.getObject(46));
            item.setColorOneDescription((String)rs.getObject(48));
            item.setColorTwoDescription((String)rs.getObject(49));
            item.setProductSubCodeId((String)rs.getObject(50));
            item.setProductSubcodeDescription((String)rs.getObject(51));
            item.setProductType((String)rs.getObject(52));
           
            if(rs.getObject(53) != null){
                //item.setStatusSortOrder(((BigDecimal)rs.getObject(53)).toString());
                item.setStatusSortOrder(rs.getObject(53).toString());
            }

            item.setHpSequence((String)rs.getObject(54));

            /*
             * Item of the week
             */
            item.setSourceCode((String)rs.getObject(55));
            item.setItemOfTheWeekFlag((String)rs.getObject(56));
            item.setSourceDescription((String)rs.getObject(57));
            
            // Reinstate email flag
            item.setReinstateFlag((String)rs.getObject(58));
            
            // Occasion Description (defect #598)
            item.setOccasionDescription((String)rs.getObject(59));
            
            // Amazon
            item.setCommission((String)rs.getObject(60));
            item.setWholesale((String)rs.getObject(61));
            item.setTransaction((String)rs.getObject(62));
            item.setPdbPrice((String)rs.getObject(63));
            item.setShippingTax((String)rs.getObject(64));
            item.setPriceOverrideFlag((String)rs.getObject(65));
            item.setDiscountedProductPrice((String)rs.getObject(66));
            item.setDiscountType((String)rs.getObject(67));            
            item.setPartnerCost((String)rs.getObject(68));
            //item.setPersonalizationData((String)rs.getObject(69));
            Clob pdataTemp = rs.getClob(69);
            if(pdataTemp != null) {
                item.setPersonalizationData(pdataTemp.getSubString((long)1, (int) pdataTemp.length()));
            }
            if (rs.getObject(70) != null) {
                item.setUpdatedOnProductsAmountTime(new java.util.Date(((java.sql.Date)rs.getObject(70)).getTime()));
            }
            
            // Miles/Points Payment Amount
            item.setMilesPointsAmt((String)rs.getString(71));

            // Personal Greeting (i.e., recordable) ID
            item.setPersonalGreetingId((String)rs.getObject(72));
            
            // Product no tax flag.
            item.setNoTaxFlag((String)rs.getObject(73));
            item.setPremierCollectionFlag((String)rs.getObject(74));
            item.setOrigSourceCode((String)rs.getObject(75));
            item.setOrigProductId((String)rs.getObject(76));
            item.setOrigShipMethod((String)rs.getObject(77));
            item.setOrigDeliveryDate((String)rs.getObject(78));
            item.setOrigTotalFeeAmount((String)rs.getObject(79));

            // Preferred Partner (e.g., USAA)
            String preferredPartner = (String)rs.getObject(80);
            if (preferredPartner != null) {
              if (preferredPartnerList == null) {
                preferredPartnerList = new HashSet<String>();
              }
              preferredPartnerList.add(preferredPartner);
            }
            item.setPreferredProcessingPartner(preferredPartner);

            String preferredPartnerName = (String)rs.getObject(91);
            if (preferredPartnerName != null) {
              if (preferredPartnerNameList == null) {
                preferredPartnerNameList = new HashSet<String>();
              }
              preferredPartnerNameList.add(preferredPartnerName);
            }
            
            // Same Day flags
            item.setSameDayFreshcutFlag((String)rs.getObject(81));
            item.setSameDayGiftFlag((String)rs.getObject(82));

            // Set the bin source changed flag
            item.setBinSourceChangedFlag((String)rs.getObject(83));

            // Charge Detail fields
            item.setDomesticFloristServiceCharge(rs.getString(84));
            item.setInternationalFloristServiceCharge(rs.getString(85));
            item.setShippingCost(rs.getString(86));
            item.setVendorCharge(rs.getString(87));
            item.setSaturdayUpcharge(rs.getString(88));
            item.setAlaskaHawaiiSurcharge(rs.getString(89));
            item.setFuelSurcharge(rs.getString(90));
            item.setApplySurchargeCode(rs.getString(92));// 91 is used by partner_display_name
            item.setFuelSurchargeDescription(rs.getString(93));
            item.setSendSurchargeToFlorist(rs.getString(94));
            item.setDisplaySurcharge(rs.getString(95));
            
            item.setShippingFeeAmountSavings(bigDecimalToString(rs.getBigDecimal("SHIPPING_FEE_AMOUNT_SAVED")));
            item.setServiceFeeAmountSavings(bigDecimalToString(rs.getBigDecimal("SERVICE_FEE_AMOUNT_SAVED")));
            item.setSameDayUpcharge(bigDecimalToString(rs.getBigDecimal("SAME_DAY_UPCHARGE")));
            item.setOriginalOrderHasSDU(rs.getString("original_order_has_sdu"));
            item.setOriginalOrderHasSDUFS(rs.getString("original_order_has_sdufs"));
            item.setMorningDeliveryFee(bigDecimalToString(rs.getBigDecimal("MORNING_DELIVERY_FEE")));
            
            if(rs.getBigDecimal("MORNING_DELIVERY_FEE")!=null && rs.getBigDecimal("MORNING_DELIVERY_FEE").compareTo(new BigDecimal(0)) >= 0) {
            	item.setMorningDeliveryOpted("Y");
            } else {
            	item.setMorningDeliveryOpted("N");
            }
            
            item.setOriginalOrderHasMDF(rs.getString("original_order_has_mdf"));
            item.setProductMorningDeliveryFlag(rs.getString("PROD_MORNING_DELIVERY_FLAG"));
            item.setOrigExternalOrderTotal(rs.getString("ORIG_EXTERNAL_ORDER_TOTAL"));
            
            item.setFreeShipping(rs.getString("FREE_SHIPPING_FLAG"));
            item.setProductSubType(rs.getString("PRODUCT_SUB_TYPE"));

            item.setPcGroupId(rs.getString("PC_GROUP_ID"));
            item.setPcMembershipId(rs.getString("PC_MEMBERSHIP_ID"));
            item.setPcFlag(rs.getString("PC_FLAG"));
            item.setNovatorId(rs.getString("NOVATOR_ID"));
            item.setTimeOfService(rs.getString("TIME_OF_SERVICE"));
            item.setAddOnDiscountAmount(rs.getString(ADD_ON_DISCOUNT_AMOUNT));
            TaxVO taxVO1 = new TaxVO();
            TaxVO taxVO2 = new TaxVO();
            TaxVO taxVO3 = new TaxVO();
            TaxVO taxVO4 = new TaxVO();
            TaxVO taxVO5 = new TaxVO();
            
            //Set taxVO1 attributes info
            taxVO1.setName(rs.getString(100));
            taxVO1.setDescription(rs.getString(101));
            if(rs.getString(102) != null)
            		taxVO1.setRate(new BigDecimal(rs.getString(102)));
            if(rs.getString(103) != null)
        		taxVO1.setAmount(new BigDecimal(rs.getString(103)));
            
            //Set taxVO2 attributes info
            taxVO2.setName(rs.getString(104));
            taxVO2.setDescription(rs.getString(105));
            if(rs.getString(106) != null)
            	taxVO2.setRate(new BigDecimal(rs.getString(106)));
            if(rs.getString(107) != null)
            	taxVO2.setAmount(new BigDecimal(rs.getString(107)));
            
            //Set taxVO3 attributes info
            taxVO3.setName(rs.getString(108));
            taxVO3.setDescription(rs.getString(109));
            if(rs.getString(110) != null)
        		taxVO3.setRate(new BigDecimal(rs.getString(110)));
            if(rs.getString(111) != null)
        		taxVO3.setAmount(new BigDecimal(rs.getString(111)));
            
            //Set taxVO4 attributes info
            taxVO4.setName(rs.getString(112));
            taxVO4.setDescription(rs.getString(113));
            if(rs.getString(114) != null)
        		taxVO4.setRate(new BigDecimal(rs.getString(114)));
            if(rs.getString(115) != null)
        		taxVO4.setAmount(new BigDecimal(rs.getString(115)));
            
            //Set taxVO5 attributes info
            taxVO5.setName(rs.getString(116));
            taxVO5.setDescription(rs.getString(117));
            if(rs.getString(118) != null)
        		taxVO5.setRate(new BigDecimal(rs.getString(118)));
            if(rs.getString(119) != null)
        		taxVO5.setAmount(new BigDecimal(rs.getString(119)));
             	
        	if(taxVO1 != null && taxVO1.getName() != null && taxVO1.getName().length() > 0) 
        		taxes.add(taxVO1);
            
        	if(taxVO2 != null && taxVO2.getName() != null && taxVO2.getName().length() > 0) 
        		taxes.add(taxVO2);
        	
        	if(taxVO3 != null && taxVO3.getName() != null && taxVO3.getName().length() > 0) 
        		taxes.add(taxVO3);
        	
        	if(taxVO4 != null && taxVO4.getName() != null && taxVO4.getName().length() > 0) 
        		taxes.add(taxVO4);
        	
        	if(taxVO5 != null && taxVO5.getName() != null && taxVO5.getName().length() > 0) 
        		taxes.add(taxVO5);
            /*
        	logger.debug("adding taxes to list, tax size is: " +taxes.size());
        	
        	logger.debug("taxVO1.getName(): " + taxVO1.getName());
        	logger.debug("taxVO1.getDescription(): " + taxVO1.getDescription());
			logger.debug("taxVO1.getRate(): " + taxVO1.getRate());
			logger.debug("taxVO1.getAmount(): " + taxVO1.getAmount());
			
        	logger.debug("taxVO2.getName(): " + taxVO2.getName());
        	logger.debug("taxVO2.getDescription(): " + taxVO2.getDescription());
			logger.debug("taxVO2.getRate(): " + taxVO2.getRate());
			logger.debug("taxVO2.getAmount(): " + taxVO2.getAmount());
			
        	logger.debug("taxVO3.getName(): " + taxVO3.getName());
        	logger.debug("taxVO3.getDescription(): " + taxVO3.getDescription());
			logger.debug("taxVO3.getRate(): " + taxVO3.getRate());
			logger.debug("taxVO3.getAmount(): " + taxVO3.getAmount());
			
        	logger.debug("taxVO4.getName(): " + taxVO4.getName());
        	logger.debug("taxVO4.getDescription(): " + taxVO4.getDescription());
			logger.debug("taxVO4.getRate(): " + taxVO4.getRate());
			logger.debug("taxVO4.getAmount(): " + taxVO4.getAmount());
        	
        	logger.debug("taxVO5.getName(): " + taxVO5.getName());
        	logger.debug("taxVO5.getDescription(): " + taxVO5.getDescription());
			logger.debug("taxVO5.getRate(): " + taxVO5.getRate());
			logger.debug("taxVO5.getAmount(): " + taxVO5.getAmount());
        	*/
        	item.getItemTaxVO().setTaxSplit(taxes);
        	
            Iterator iterator = null;
    
            //for each order item build dispositions
            Collection dispositions = new ArrayList();
            iterator = allDispositions.iterator();
            DispositionsVO disposition = null;
                                
            for (int i = 0; iterator.hasNext(); i++) 
            {
               disposition = new DispositionsVO();
               disposition = (DispositionsVO)iterator.next();
                    
               if(disposition.getOrderDetailID() == item.getOrderDetailId())
               {
                  dispositions.add(disposition);
               }                    
            }

            //for each order item build order detail extensions
            Collection extensions = new ArrayList();
            iterator = allExtensions.iterator();
            OrderDetailExtensionsVO extension = null;
                                
            for (int i = 0; iterator.hasNext(); i++) 
            {
               extension = new OrderDetailExtensionsVO();
               extension = (OrderDetailExtensionsVO)iterator.next();
                    
               if(extension.getOrderDetailId() == item.getOrderDetailId())
               {
                  extensions.add(extension);
               }                    
            }
                
            //for each order item build new add ons
            Collection addOns = new ArrayList();
            iterator = allAddOns.iterator();
            AddOnsVO addOn = null;
                                
            for (int i = 0; iterator.hasNext(); i++) 
            {
               addOn = new AddOnsVO();
               addOn = (AddOnsVO)iterator.next();
                    
               if(addOn.getOrderDetailId() == item.getOrderDetailId())
               {
                  addOns.add((AddOnsVO)this.deepCopy(addOn));
               }                    
            }

            //for each order item build new recipients
            Collection recipients = new ArrayList();
            iterator = allRecipients.iterator();
            RecipientsVO recipient = null;
          
            for (int i = 0; iterator.hasNext(); i++) 
            {
               recipient = new RecipientsVO();
               recipient = (RecipientsVO)iterator.next();
                    
               if(recipient.getRecipientId() == item.getRecipientId())
               {
                  recipients.add((RecipientsVO)this.deepCopy(recipient));
               }                    
            }
                
            //for each order item set the AVS address
            iterator = allAVSAddress.iterator();
            AVSAddressVO avsAddress = null;
                                
            for (int i = 0; iterator.hasNext(); i++) 
            {
                avsAddress = new AVSAddressVO();
                avsAddress = (AVSAddressVO)iterator.next();
                    
               if(avsAddress.getRecipientId() == item.getRecipientId())
               {
                  item.setAvsAddress((AVSAddressVO)this.deepCopy(avsAddress));
               }                    
            }
                
            item.setAddOns((List)addOns);
            item.setRecipients((List)recipients);

            // Fixed 3/18
            item.setDispositions((List)dispositions);
            item.setOrderDetailExtensions((List)extensions);
            
            item.setLatecutoffCharge(rs.getString("LATE_CUTOFF_FEE"));
            item.setSundayUpcharge(rs.getString("VENDOR_SUN_UPCHARGE")); 
            item.setMondayUpcharge(rs.getString("VENDOR_MON_UPCHARGE")); 
            item.setShippingSystem(rs.getString("SHIPPING_SYSTEM"));
                
            items.add(item);
        }

        return items;
    }

    /**
     * Clone an object to reference original object.
     * @param oldObj Object
     * @exception xException 
     * @return Object
     */
    private Object deepCopy(Object oldObj) throws Exception
    {
      ObjectOutputStream out = null;
      ObjectInputStream in = null;
      Object newobj = null;

      try
      {
        ByteArrayOutputStream buf = new ByteArrayOutputStream();
        out = new ObjectOutputStream(buf);
        out.writeObject(oldObj);
        in = new ObjectInputStream(new
        ByteArrayInputStream(buf.toByteArray()));
        newobj = in.readObject();
        return newobj;
      }
      catch(Exception e)
      {
        throw(e);
      }
      finally
      {
        out.close();
        in.close();
      }
    }

  /**
   * Retrieve add on information.
   * 
   * @param orderMap Map
   * @return  Collection of add ons
   */
    private Collection getAddOns(Map orderMap) throws Exception
    {
       // Get the order detail addon information
 
        Collection addOns = new ArrayList();
        AddOnsVO addOn = null;

        CachedResultSet rs = new CachedResultSet();
        rs = (CachedResultSet) orderMap.get(ORDER_ADD_ONS);

        Object price = null;
        Object histId = null;
        
        while(rs != null && rs.next())
        {
            addOn = new AddOnsVO();

            //addOn.setAddOnId(new Long(rs.getObject(1).toString()).longValue());
            //addOn.setOrderDetailId(new Long(rs.getObject(2).toString()).longValue());
            addOn.setAddOnId(Long.parseLong(rs.getObject(1).toString()));
            addOn.setOrderDetailId(Long.parseLong(rs.getObject(2).toString()));
            addOn.setAddOnCode((String) rs.getObject(3));
            addOn.setAddOnQuantity((String) rs.getObject(4));  
            addOn.setAddOnType((String) rs.getObject(5));
            addOn.setDescription((String) rs.getObject(6));
            price = rs.getObject(7);
            if(price != null){
                addOn.setPrice(rs.getObject(7).toString());
            }
            histId = rs.getObject(8);
            if (histId != null) {
                try {
                    addOn.setAddOnHistoryId(Long.parseLong(rs.getObject(8).toString()));
                } catch (NumberFormatException nfe) {
                    // Do nothing
                }
            }
            addOn.setDiscountAmount(rs.getString(ADD_ON_DISCOUNT_AMOUNT));
            
            addOns.add(addOn);
        }

        return addOns;
    }


   /**
   * Retrieve co brand information.
   * 
   * @param orderMap Map
   * @return  Collection of co brand information
   * @exception Exception
   */
   private Collection getCoBrand(Map orderMap) throws Exception
    {
        // Get the co brand information

        Collection coBrands = new ArrayList();
        CoBrandVO coBrand = null;

        CachedResultSet rs = new CachedResultSet();
        rs = (CachedResultSet) orderMap.get(ORDER_CO_BRANDS);

        while(rs != null && rs.next())
        {
            coBrand = new CoBrandVO();

            //coBrand.setCoBrandId(new Long(rs.getObject(1).toString()).longValue());
            coBrand.setCoBrandId(Long.parseLong(rs.getObject(1).toString()));
            coBrand.setGuid((String)rs.getObject(2));
            coBrand.setInfoName((String)rs.getObject(3));
            coBrand.setInfoData((String)rs.getObject(4));
            logger.debug("getCoBrand - " + coBrand.getInfoName() + "/" + coBrand.getInfoData());
            coBrands.add(coBrand);
        }

        return coBrands;
    }

    /**
   * Retrieve membership information.
   * 
   * @param orderMap Map
   * @return  Collection of membership information
   * @exception Exception
   */
   private Collection getMembership(Map orderMap) throws Exception
    {
        // Get the membership information

        Collection memberships = new ArrayList();
        MembershipsVO membership = null;

        CachedResultSet rs = new CachedResultSet();
        rs = (CachedResultSet) orderMap.get(ORDER_MEMBERSHIPS);

        while(rs != null && rs.next())
        {
            membership = new MembershipsVO();

            //membership.setMembershipId(new Long(rs.getObject(1).toString()).longValue());
            //membership.setBuyerId(new Long(rs.getObject(2).toString()).longValue());
            membership.setMembershipId(Long.parseLong(rs.getObject(1).toString()));
            membership.setBuyerId(Long.parseLong(rs.getObject(2).toString()));
            membership.setMembershipType((String)rs.getObject(3));
            membership.setLastName((String)rs.getObject(4));
            membership.setFirstName((String)rs.getObject(5));
            membership.setMembershipIdNumber((String)rs.getObject(6));
            logger.debug("getMembership - " + membership.getMembershipType() + "/" + membership.getMembershipIdNumber());
            memberships.add(membership);
        }

        return memberships;
    }

   /**
   * Retrieve payments.
   * 
   * @param orderMap Map
   * @return  Collection of payments
   * @exception Exception
   */
    private Collection getPayments(Map orderMap) throws Exception
    {
       // Get the payments
 
        Collection payments = new ArrayList();
        PaymentsVO payment = null;
        Object creditCardId = null;
        String initAuthAmount = null;
        Collection allCreditCards = getCreditCards(orderMap);
        
        CachedResultSet rs = new CachedResultSet();
        rs = (CachedResultSet) orderMap.get(ORDER_PAYMENTS);

        while(rs != null && rs.next())
        {
            payment = new PaymentsVO();

            payment.setPaymentId(Long.parseLong(rs.getString("PAYMENT_ID")));
            payment.setGuid(rs.getString("ORDER_GUID"));
            payment.setPaymentsType(rs.getString("PAYMENT_TYPE"));
            payment.setAmount(rs.getString("AMOUNT"));

            //creditCardId is an integer and nullable
            creditCardId = rs.getObject("CREDIT_CARD_ID");
            if(creditCardId != null){
              payment.setCreditCardId(Long.parseLong(rs.getString("CREDIT_CARD_ID")));
            }

            payment.setGiftCertificateId(rs.getString("GIFT_CERTIFICATE_ID"));

            payment.setAuthResult(rs.getString("AUTH_RESULT"));                
            payment.setAuthNumber(rs.getString("AUTH_NUMBER"));                
            payment.setAvsCode(rs.getString("AVS_CODE"));
            payment.setAcqReferenceNumber(rs.getString("ACQ_REFERENCE_NUMBER"));
            payment.setAafesTicket(rs.getString("AAFES_TICKET"));                
            payment.setInvoiceNumber(rs.getString("INVOICE_NUMBER"));
            payment.setPaymentMethodType(rs.getString("PAYMENT_METHOD_TYPE")); 
            payment.setApAccount(rs.getString("AP_ACCOUNT_TXT"));
            payment.setApAuth(rs.getString("AP_AUTH_TXT"));
            payment.setApAccountId(rs.getString("AP_ACCOUNT_ID"));
            payment.setOrigAuthAmount(rs.getString("ORIG_AUTH_AMOUNT_TXT"));
            
            initAuthAmount = rs.getString("INITIAL_AUTH_AMOUNT");
            logger.info("initAuthAmount ::"+initAuthAmount);
            if(initAuthAmount!=null)
            {		
            	payment.setInitAuthAmount(new BigDecimal(initAuthAmount));
            }	
            payment.setPaymentTypeDesc(rs.getString("DESCRIPTION"));
            payment.setMilesPointsAmt(rs.getString("MILES_POINTS_AMT"));
            
            String fCount = rs.getString("CSC_FAILURE_CNT");
            payment.setCscFailureCount(fCount==null ? 0 : Integer.parseInt(fCount));
            payment.setCscResponseCode(rs.getString("CSC_RESPONSE_CODE"));
            String vFlag = rs.getString("CSC_VALIDATED_FLAG");
            payment.setCscValidatedFlag(vFlag==null ? "N" : vFlag);
            payment.setGcCouponStatus(rs.getString("GC_COUPON_STATUS"));
            payment.setGcCouponIssueAmount(rs.getBigDecimal("ISSUE_AMOUNT"));
            payment.setGcCouponExpirationDate(rs.getDate("EXPIRATION_DATE"));
            payment.setWalletIndicator(rs.getString("WALLET_INDICATOR"));
            payment.setCcAuthProvider(rs.getString("CC_AUTH_PROVIDER"));
            payment.setCardinalVerifiedFlag(rs.getString("CARDINAL_VERIFIED_FLAG"));
            payment.setRoute(rs.getString("ROUTE"));
            payment.setTokenId(rs.getString("TOKEN_ID"));
            payment.setAuthTransactionId(rs.getString("AUTHORIZATION_TRANSACTION_ID"));
            //for each payment build new credit card
            Collection creditCards = new ArrayList();
            Iterator iterator = allCreditCards.iterator();
            CreditCardsVO creditCard = null;
                                
            for (int i = 0; iterator.hasNext(); i++) 
            {
               creditCard = new CreditCardsVO();
               creditCard = (CreditCardsVO)iterator.next();
                    
               if(creditCard.getCreditCardId() == payment.getCreditCardId())
               {
                  creditCards.add((CreditCardsVO)this.deepCopy(creditCard));
       
               }                    
            }

            payment.setCreditCards((List)creditCards);
            payments.add(payment);
        }

        return payments;
    }

   /**
   * Retrieve credit card information.
   * 
   * @param orderMap Map
   * @return  Collection of credit cards
   * @exception Exception
   */
    private Collection getCreditCards(Map orderMap) throws Exception
    {
       // Get the credit cards
 
        Collection creditCards = new ArrayList();
        CreditCardsVO creditCard = null;

        CachedResultSet rs = new CachedResultSet();
        rs = (CachedResultSet) orderMap.get(ORDER_CREDIT_CARDS);

        Object buyerId = null;
            
        while(rs != null && rs.next())
        {
            //cleanup holding objects
            buyerId = null;
                
            creditCard = new CreditCardsVO();
            //creditCard.setCreditCardId(new Long(rs.getObject(1).toString()).longValue());
            creditCard.setCreditCardId(Long.parseLong(rs.getObject(1).toString()));

            //buyerId is an integer and nullable
            buyerId = rs.getObject(2);
            if(buyerId != null){
              //creditCard.setBuyerId(new Long(buyerId.toString()).longValue());
              creditCard.setBuyerId(Long.parseLong(buyerId.toString()));
            }

            creditCard.setCCType((String)rs.getObject(3));
            creditCard.setCCNumber((String)rs.getObject(4));
            creditCard.setCCExpiration((String)rs.getObject(5));
            creditCard.setPin((String)rs.getObject(6));
            creditCard.setCcEncrypted(rs.getString("cc_encrypted_flag"));
            creditCards.add(creditCard);
        }

        return creditCards;
    }
    
   /**
   * Retrieve buyer.
   * 
   * @param orderMap Map
   * @param Collection of all the buyer emails.
   * @return  Collection of buyers
   * @exception Exception
   */
    private Collection getBuyer(Map orderMap, Collection allBuyerEmails) throws Exception
    {
        Collection buyers = new ArrayList();
        BuyerVO buyer = null;
        
        Collection buyerPhones = getBuyerPhones(orderMap);
        Collection buyerComments = getBuyerComments(orderMap);
        Collection allBuyerAddresses = getAllBuyerAddresses(orderMap);

        CachedResultSet rs = new CachedResultSet();
        rs = (CachedResultSet) orderMap.get(BUYER);
            
        while(rs != null && rs.next())
        {
            buyer = new BuyerVO();

            //buyer.setBuyerId(new Long(rs.getObject(1).toString()).longValue());
            buyer.setBuyerId(Long.parseLong(rs.getObject(1).toString()));
            buyer.setCustomerId((String) rs.getObject(2));
            buyer.setLastName((String) rs.getObject(3));
            buyer.setFirstName((String) rs.getObject(4));
            buyer.setAutoHold((String) rs.getObject(5));
            buyer.setBestCustomer((String) rs.getObject(6));
            buyer.setStatus((String) rs.getObject(7));

            if(rs.getObject(8) != null){
                buyer.setCleanCustomerId(new Long(rs.getObject(8).toString()));
            }

            //set 1 to 1 relations for buyer object model
            buyer.setBuyerPhones((List)buyerPhones);
            buyer.setBuyerComments((List)buyerComments);
                
            //for each buyer build new address
            Collection addresses = new ArrayList();
            Iterator iterator = allBuyerAddresses.iterator();
            BuyerAddressesVO address = null;
                                
            for (int i = 0; iterator.hasNext(); i++) 
            {
               address = new BuyerAddressesVO();
               address = (BuyerAddressesVO)iterator.next();
                    
               if(address.getBuyerId() == buyer.getBuyerId())
               {
                  addresses.add(address);
               }                    
            }

            buyer.setBuyerAddresses((List)addresses);

            //for each buyer build new email
            Collection emails = new ArrayList();
            iterator = allBuyerEmails.iterator();
            BuyerEmailsVO email = null;
                                
            for (int i = 0; iterator.hasNext(); i++) 
            {
               email = new BuyerEmailsVO();
               email = (BuyerEmailsVO)iterator.next();
                    
               if(email.getBuyerId() == buyer.getBuyerId())
               {
                  emails.add(email);
               }                    
            }

            buyer.setBuyerEmails((List)emails);
            buyers.add(buyer);
        }

        return buyers;
    }

   /**
   * Retrieve buyer phones.
   * 
   * @param orderMap Map
   * @return  Collection of buyer phones
   * @exception Exception
   */
    private Collection getBuyerPhones(Map orderMap) throws Exception
    {
       // Get the buyer phone numbers
 
        Collection phones = new ArrayList();
        BuyerPhonesVO phone = null;

        CachedResultSet rs = new CachedResultSet();
        rs = (CachedResultSet) orderMap.get(BUYER_PHONES);
  
        Object buyerId = null;

        while(rs != null && rs.next())
        {
            //cleanup holding objects
            buyerId = null;
            
            phone = new BuyerPhonesVO();
            //phone.setBuyerPhoneId(new Long(rs.getObject(1).toString()).longValue());
            phone.setBuyerPhoneId(Long.parseLong(rs.getObject(1).toString()));

            //buyerId is an integer and nullable
            buyerId = rs.getObject(2);
            if(buyerId != null){
              //phone.setBuyerId(new Long(buyerId.toString()).longValue());
              phone.setBuyerId(Long.parseLong(buyerId.toString()));
            }

            phone.setPhoneType((String) rs.getObject(3));
            phone.setPhoneNumber((String) rs.getObject(4));
            phone.setExtension((String) rs.getObject(5));                
                             
            phones.add(phone);
        }

        return phones;
    }

   /**
   * Retrieve buyer comments.
   * 
   * @param orderMap Map
   * @return  Collection of buyer comments
   * @exception Exception
   */
    private Collection getBuyerComments(Map orderMap) throws Exception
    {
       // Get the buyer comments
 
        Collection comments = new ArrayList();
        BuyerCommentsVO comment = null;

        CachedResultSet rs = new CachedResultSet();
        rs = (CachedResultSet) orderMap.get(BUYER_COMMENTS);
  
        Object buyerId = null;

        while(rs != null && rs.next())
        {
            //cleanup holding objects
            buyerId = null;
            
            comment = new BuyerCommentsVO();
            //comment.setBuyerCommentId(new Long(rs.getObject(1).toString()).longValue());
            comment.setBuyerCommentId(Long.parseLong(rs.getObject(1).toString()));

            //buyerId is an integer and nullable
            buyerId = rs.getObject(2);
            if(buyerId != null){
              //comment.setBuyerId(new Long(buyerId.toString()).longValue());
              comment.setBuyerId(Long.parseLong(buyerId.toString()));
            }

            comment.setText((String) rs.getObject(3));
                             
            comments.add(comment);
        }

        return comments;
    }

  /**
   * Retrieve buyer addresses.
   * 
   * @param orderMap Map
   * @return  Collection of buyer addresses
   * @exception Exception
   */
    private Collection getAllBuyerAddresses(Map orderMap) throws Exception
    {
       // Get the buyer addresses
 

 
        Collection addresses = new ArrayList();
        BuyerAddressesVO address = null;

        CachedResultSet rs = new CachedResultSet();
        rs = (CachedResultSet) orderMap.get(BUYER_ADDRESSES);
  
        Object buyerId = null;

        while(rs != null && rs.next())
        {

        
            //cleanup holding objects
            buyerId = null;
            
            address = new BuyerAddressesVO();
            //address.setBuyerAddressId(new Long(rs.getObject(1).toString()).longValue());
            address.setBuyerAddressId(Long.parseLong(rs.getObject(1).toString()));

            //buyerId is an integer and nullable
            buyerId = rs.getObject(2);
            if(buyerId != null){
              //address.setBuyerId(new Long(buyerId.toString()).longValue());
              address.setBuyerId(Long.parseLong(buyerId.toString()));
            }

            address.setAddressType((String) rs.getObject(3));
            address.setAddressLine1((String) rs.getObject(4));
            address.setAddressLine2((String) rs.getObject(5));
            address.setCity((String) rs.getObject(6));
            address.setStateProv((String) rs.getObject(7));
            address.setPostalCode((String) rs.getObject(8));
            address.setCountry((String) rs.getObject(9));
            address.setCounty((String) rs.getObject(10));
            address.setAddressEtc((String) rs.getObject(11));                 
            addresses.add(address);
            

        }

        return addresses;
    }

   /**
   * Retrieve buyer emails.
   * 
   * @param orderMap Map
   * @return  Collection of buyer email addresses
   * @exception Exception
   */
    private Collection getAllBuyerEmails(Map orderMap) throws Exception
    {
       // Get the buyer emails
 
        Collection emails = new ArrayList();
        BuyerEmailsVO email = null;

        CachedResultSet rs = new CachedResultSet();
        rs = (CachedResultSet) orderMap.get(BUYER_EMAILS);
  
        Object buyerId = null;

        while(rs != null && rs.next())
        {
            //cleanup holding objects
            buyerId = null;
            
            email = new BuyerEmailsVO();
            //email.setBuyerEmailId(new Long(rs.getObject(1).toString()).longValue());
            email.setBuyerEmailId(Long.parseLong(rs.getObject(1).toString()));

            //buyerId is an integer and nullable
            buyerId = rs.getObject(2);
            if(buyerId != null){
              //email.setBuyerId(new Long(buyerId.toString()).longValue());
              email.setBuyerId(Long.parseLong(buyerId.toString()));
            }

            email.setEmail((String) rs.getObject(3));
            email.setPrimary((String) rs.getObject(4));
            email.setNewsletter((String) rs.getObject(5));
                             
            emails.add(email);
        }

        return emails;
    }

  /**
   * Retrieve Recipient information.
   * 
   * @param orderMap Map
   * @return  Collection of recipients
   * @exception Exception
   */
   private Collection getRecipient(Map orderMap) throws Exception
    {
        Collection recipients = new ArrayList();
        RecipientsVO recipient = null;
        
        Collection allRecipientAddresses = getRecipientAddresses(orderMap);
        Collection allComments = getRecipientComments(orderMap);
        Collection allPhones = getRecipientPhones(orderMap);
            
        CachedResultSet rs = new CachedResultSet();
        rs = (CachedResultSet) orderMap.get(RECIPIENT);
            
        while(rs != null && rs.next())
        {
            recipient = new RecipientsVO();

            //recipient.setRecipientId(new Long(rs.getObject(1).toString()).longValue());
            recipient.setRecipientId(Long.parseLong(rs.getObject(1).toString()));
            recipient.setLastName((String) rs.getObject(2));
            recipient.setFirstName((String) rs.getObject(3));
            recipient.setListCode((String) rs.getObject(4));
            recipient.setAutoHold((String) rs.getObject(5));
            recipient.setMailListCode((String) rs.getObject(6));
            recipient.setStatus((String) rs.getObject(7));
            recipient.setCustomerId((String) rs.getObject(8));

            //for each recipient build new address
            Collection addresses = new ArrayList();
            Iterator iterator = allRecipientAddresses.iterator();
            RecipientAddressesVO address = null;
                                
            for (int i = 0; iterator.hasNext(); i++) 
            {
               address = new RecipientAddressesVO();
               address = (RecipientAddressesVO)iterator.next();
                    
               if(address.getRecipientId() == recipient.getRecipientId())
               {
                  addresses.add(address);
               }                    
            }
            recipient.setRecipientAddresses((List)addresses);

            //for each recipient build new phone
            Collection phones = new ArrayList();
            iterator = allPhones.iterator();
            RecipientPhonesVO phone = null;
                                
            for (int i = 0; iterator.hasNext(); i++) 
            {
               phone = new RecipientPhonesVO();
               phone = (RecipientPhonesVO)iterator.next();
                    
               if(phone.getRecipientId() == recipient.getRecipientId())
               {
                  phones.add(phone);
               }                    
            }            
            
            recipient.setRecipientPhones((List)phones);
        
            recipients.add(recipient);
        }

        return recipients;
    }


  /**
   * Retrieve Recipient address information.
   * 
   * @param orderMap Map
   * @return  Collection of recipient addresses
   * @exception Exception
   */
   private Collection getRecipientAddresses(Map orderMap) throws Exception
    {
       // Get the recipient addresses
 
        Collection addresses = new ArrayList();
        RecipientAddressesVO address = null;

        CachedResultSet rs = new CachedResultSet();
        rs = (CachedResultSet) orderMap.get(RECIPIENT_ADDRESSES);
  
        Object recipientId = null;
        
        while(rs != null && rs.next())
        {
            //cleanup holding objects
            recipientId = null;
                        
            address = new RecipientAddressesVO();
            //address.setRecipientAddressId(new Long(rs.getObject(1).toString()).longValue());
            address.setRecipientAddressId(Long.parseLong(rs.getObject(1).toString()));

            //recipientId is an integer and nullable
            recipientId = rs.getObject(2);
            if(recipientId != null){
              //address.setRecipientId(new Long(recipientId.toString()).longValue());
              address.setRecipientId(Long.parseLong(recipientId.toString()));
            }

            address.setAddressType((String) rs.getObject(3));
            address.setAddressLine1((String) rs.getObject(4));
            address.setAddressLine2((String) rs.getObject(5));
            address.setCity((String) rs.getObject(6));
            address.setStateProvince((String) rs.getObject(7));
            address.setPostalCode((String) rs.getObject(8));
            address.setCountry((String) rs.getObject(9));
            address.setCounty((String) rs.getObject(10));
                
            //address.setGeofindMatchCode((String) rs.getObject(11));
            //address.setLatitude((String) rs.getObject(12));
            //address.setLongitude((String) rs.getObject(13));

            address.setInternational((String) rs.getObject(11));
            address.setName((String) rs.getObject(12));
            address.setInfo((String) rs.getObject(13));
            address.setAddressTypeCode((String) rs.getObject(14));
                   
            addresses.add(address);
        }

        return addresses;
    }
      
  /**
   * Retrieve Recipient comments.
   * 
   * @param orderMap Map
   * @return  Collection of recipient comments
   * @exception Exception
   */    
    private Collection getRecipientComments(Map orderMap) throws Exception
    {
       // Get the recipient comments
 
        Collection comments = new ArrayList();
        RecipientCommentsVO comment = null;

        CachedResultSet rs = new CachedResultSet();
        rs = (CachedResultSet) orderMap.get(RECIPIENT_COMMENTS);
  
        Object recipientId = null;

        while(rs != null && rs.next())
        {
            //cleanup holding objects
            recipientId = null;
            
            comment = new RecipientCommentsVO();
            //comment.setRecipientCommentId(new Long(rs.getObject(1).toString()).longValue());
            comment.setRecipientCommentId(Long.parseLong(rs.getObject(1).toString()));

            //recipientId is an integer and nullable
            recipientId = rs.getObject(2);
            if(recipientId != null){
              //comment.setRecipientId(new Long(recipientId.toString()).longValue());
              comment.setRecipientId(Long.parseLong(recipientId.toString()));
            }

            comment.setText((String) rs.getObject(3));
                             
            comments.add(comment);
        }

        return comments;
    }

  /**
   * Retrieve Recipient phone information.
   * 
   * @param orderMap Map
   * @return  Collection of recipient phones
   * @exception Exception
   */
    private Collection getRecipientPhones(Map orderMap) throws Exception
    {
       // Get the recipient phones
 
        Collection phones = new ArrayList();
        RecipientPhonesVO phone = null;

        CachedResultSet rs = new CachedResultSet();
        rs = (CachedResultSet) orderMap.get(RECIPIENT_PHONES);
  
        Object recipientId = null;

        while(rs != null && rs.next())
        {
            //cleanup holding objects
            recipientId = null;
            
            phone = new RecipientPhonesVO();
            //phone.setRecipientPhoneId(new Long(rs.getObject(1).toString()).longValue());
            phone.setRecipientPhoneId(Long.parseLong(rs.getObject(1).toString()));

            //recipientId is an integer and nullable
            recipientId = rs.getObject(2);
            if(recipientId != null){
              //phone.setRecipientId(new Long(recipientId.toString()).longValue());
              phone.setRecipientId(Long.parseLong(recipientId.toString()));
            }

            phone.setPhoneType((String) rs.getObject(3));
            phone.setPhoneNumber((String) rs.getObject(4));
            phone.setExtension((String) rs.getObject(5));
                             
            phones.add(phone);
        }

        return phones;
    }

    /**
     * Retrieve AVS address information.
     * 
     * @param orderMap Map
     * @return  Collection of recipients
     * @exception Exception
     */
     private Collection getAVSAddresses(Map orderMap) throws Exception
      {
          AVSAddressVO address = null;
          Collection addresses = new ArrayList();
          Collection <AVSAddressScoreVO>allAddressScores = getAVSAddressScores(orderMap);
              
          CachedResultSet rs = new CachedResultSet();
          rs = (CachedResultSet) orderMap.get(AVS_ADDRESS);
              
          while(rs != null && rs.next())
          {
              address = new AVSAddressVO();

              address.setAvsAddressId(Long.parseLong(rs.getObject(1).toString()));
              address.setRecipientId(Long.parseLong(rs.getObject(2).toString()));
              address.setAddress((String) rs.getObject(3));
              address.setCity((String) rs.getObject(4));
              address.setStateProvince((String) rs.getObject(5));
              address.setPostalCode((String) rs.getObject(6));
              address.setCountry((String) rs.getObject(7));
              address.setLatitude((String) rs.getObject(8));
              address.setLongitude((String) rs.getObject(9));
              address.setEntityType((String) rs.getObject(10));
              address.setOverrideFlag((String) rs.getObject(11));
              address.setResult((String) rs.getObject(12));
              address.setAvsPerformed((String) rs.getObject(13));
              address.setAvsPerformedOrigin((String) rs.getObject(14));

              //for each address build scores
              List<AVSAddressScoreVO> scores = new ArrayList<AVSAddressScoreVO>();
                                  
              for (AVSAddressScoreVO score : allAddressScores) 
              {                     
                 if(address.getAvsAddressId() == score.getAvsAddressId())
                 {
                    scores.add(score);
                 }                    
              }
              address.setScores(scores);  
              addresses.add(address);
          }

          return addresses;
      }

     /**
      * Retrieve address score information.
      * 
      * @param orderMap Map
      * @return  Collection of address scores
      * @exception Exception
      */
     private Collection<AVSAddressScoreVO> getAVSAddressScores(Map orderMap) throws Exception
     {
         // Get the address scores

         Collection scores = new ArrayList();
         AVSAddressScoreVO score = null;

         CachedResultSet rs = new CachedResultSet();
         rs = (CachedResultSet) orderMap.get(AVS_ADDRESS_SCORES);

         Object addressID = null;

         while(rs != null && rs.next())
         {
             addressID = null;

             score = new AVSAddressScoreVO();
             score.setAvsScoreId(Long.parseLong(rs.getObject(1).toString()));

             addressID = rs.getObject(2);
             if(addressID != null){
                 score.setAvsAddressId(Long.parseLong(addressID.toString()));
             }

             score.setReason((String) rs.getObject(3));
             score.setScore((String) rs.getObject(4));

             scores.add(score);
         }

         return scores;
     }


     private Collection getFraudComments(Map orderMap) throws Exception
     {
         // Get the fraud comments

         Collection comments = new ArrayList();
         FraudCommentsVO comment = null;

         CachedResultSet rs = new CachedResultSet();
         rs = (CachedResultSet) orderMap.get(ORDER_FRAUD_COMMENTS);

         while(rs != null && rs.next())
         {
             comment = new FraudCommentsVO();
             //comment.setFraudCommentID(new Long(((BigDecimal) rs.getObject(1)).toString()).longValue());
             comment.setFraudCommentID(Long.parseLong(rs.getObject(1).toString()));
             comment.setGUID((String)rs.getObject(2));
             comment.setFraudID((String)rs.getObject(3));
             comment.setCommentText((String)rs.getObject(4));
             comment.setFraudDescription((String)rs.getObject(5));

             comments.add(comment);
         }

         return comments;
     }


    private Collection getFraudCodes(Map orderMap) throws Exception
    {
       // Get the fraud codes
 
        Collection codes = new ArrayList();
        FraudCommentsVO comment = null;
       
        CachedResultSet rs = new CachedResultSet();
        rs = (CachedResultSet) orderMap.get(ORDER_FRAUD_CODES);

        String code = null;

        while(rs != null && rs.next())
        {
            code = (String)rs.getObject(2);
            codes.add(code);
        }

        return codes;
    }

     private Collection getDispositions(Map orderMap) throws Exception
    {
       // Get the order detail addon infomration
 
        Collection dispositions = new ArrayList();
        DispositionsVO disposition = null;

        CachedResultSet rs = new CachedResultSet();
        rs = (CachedResultSet) orderMap.get(ORDER_DISPOSITIONS);
 
        while(rs != null && rs.next())
        {
            disposition = new DispositionsVO();

            //disposition.setOrderDispositionID(new Long(((BigDecimal) rs.getObject(1)).toString()).longValue());
            //disposition.setOrderDetailID(new Long(((BigDecimal) rs.getObject(2)).toString()).longValue());
            disposition.setOrderDispositionID(Long.parseLong(rs.getObject(1).toString()));
            disposition.setOrderDetailID(Long.parseLong(rs.getObject(2).toString()));
            disposition.setDispositionID((String) rs.getObject(3));
            disposition.setComments((String) rs.getObject(4));               
            disposition.setCalledCustomerFlag((String) rs.getObject(5));               
            disposition.setSentEmailFlag((String) rs.getObject(6));               
            disposition.setStockMessageID((String) rs.getObject(7));                           
            disposition.setEmailSubject((String) rs.getObject(8));                           
            disposition.setEmailMessage((String) rs.getObject(9));
            disposition.setDispositionDescription((String) rs.getObject(12));
            dispositions.add(disposition);
        }

        return dispositions;
    }    

     private Collection getAllOrderDetailExtensions(Map orderMap) throws Exception
    {
       // Get the order detail extension infomration
 
        Collection extensions = new ArrayList();
        OrderDetailExtensionsVO extension = null;

        CachedResultSet rs = new CachedResultSet();
        rs = (CachedResultSet) orderMap.get(ORDER_DETAIL_EXTENSIONS);
 
        while(rs != null && rs.next())
        {
            extension = new OrderDetailExtensionsVO();

            extension.setExtensionId(Long.parseLong(rs.getObject(1).toString()));
            extension.setOrderDetailId(Long.parseLong(rs.getObject(2).toString()));
            extension.setInfoName((String)rs.getObject(3));
            extension.setInfoData((String)rs.getObject(4));
            
            extensions.add(extension);
        }

        return extensions;
    }    

    /**
     * Update order header information.
     * 
     * @param order OrderVO value object that contains the whole order
     * @return  boolean true/false of update
     * @exception Exception
     */
    private boolean updateOrderHeader(OrderVO order) throws Exception
    {
        // Only update the header if it has changed
        if(order.isChanged())
        {
            // update the order header information
            DataRequest dataRequest = new DataRequest();

            HashMap orderHeader = new HashMap();

            orderHeader.put("IN_GUID" , order.getGUID());
            orderHeader.put("IN_MASTER_ORDER_NUMBER" , order.getMasterOrderNumber());
            //orderHeader.put("IN_BUYER_ID" , new Integer(new Long(order.getBuyerId()).toString()));
            orderHeader.put("IN_BUYER_ID" , new Long(order.getBuyerId()));
            orderHeader.put("IN_ORDER_TOTAL" , order.getOrderTotal());
            orderHeader.put("IN_PRODUCTS_TOTAL" , order.getProductsTotal());
            orderHeader.put("IN_TAX_TOTAL" , order.getTaxTotal());
            orderHeader.put("IN_SERVICE_FEE_TOTAL" , order.getServiceFeeTotal());
            orderHeader.put("IN_SHIPPING_FEE_TOTAL" , order.getShippingFeeTotal());
            //orderHeader.put("IN_BUYER_EMAIL_ID" , new Integer(new Long(order.getBuyerEmailId()).toString()));
            orderHeader.put("IN_BUYER_EMAIL_ID" , new Long(order.getBuyerEmailId()));
            orderHeader.put("IN_ORDER_DATE" , order.getOrderDate());
            orderHeader.put("IN_ORDER_ORIGIN" , order.getOrderOrigin());
            //orderHeader.put("IN_MEMBERSHIP_ID" , new Integer(new Long(order.getMembershipId()).toString()));
            orderHeader.put("IN_MEMBERSHIP_ID" , new Long(order.getMembershipId()));
            orderHeader.put("IN_CSR_ID" , order.getCsrId());
            orderHeader.put("IN_ADD_ON_AMOUNT_TOTAL" , order.getAddOnAmountTotal());
            orderHeader.put("IN_PARTNERSHIP_BONUS_POINTS" , order.getPartnershipBonusPoints());
            orderHeader.put("IN_SOURCE_CODE" , order.getSourceCode());
            orderHeader.put("IN_CALL_TIME" , order.getCallTime());
            orderHeader.put("IN_DNIS_CODE" , order.getDnisCode());
            orderHeader.put("IN_DATAMART_UPDATE_DATE" , order.getDatamartUpdateDate());
            orderHeader.put("IN_DISCOUNT_TOTAL" , order.getDiscountTotal());
            orderHeader.put("IN_YELLOW_PAGES_CODE" , order.getYellowPagesCode());
            orderHeader.put("IN_ARIBA_BUYER_ASN_NUMBER" , order.getAribaBuyerAsnNumber());
            orderHeader.put("IN_ARIBA_BUYER_COOKIE" , order.getAribaBuyerCookie());
            orderHeader.put("IN_ARIBA_PAYLOAD" , order.getAribaPayload());
            orderHeader.put("IN_SOCKET_TIMESTAMP" , order.getSocketTimestamp());
            orderHeader.put("IN_STATUS" , order.getStatus());
            orderHeader.put("IN_FRAUD" , order.getFraudFlag());
            orderHeader.put("IN_CO_BRAND_CREDIT_CARD_CODE" , order.getCoBrandCreditCardCode());
            orderHeader.put("IN_LOSS_PREVENTION_INDICATOR" , order.getLossPreventionIndicator());
            orderHeader.put("IN_MP_REDEMPTION_RATE_AMT", order.getMpRedemptionRateAmt());
            orderHeader.put("IN_CHARACTER_MAPPING_FLAG", order.getCharacterMappingFlag());
            //orderHeader.put(STATUS_PARAM,"");
            //orderHeader.put(MESSAGE_PARAM,"");
            orderHeader.put("IN_LANGUAGE_ID", order.getLanguageId());
            orderHeader.put("IN_ADD_ON_DISCOUNT_AMOUNT", order.getAddOnDiscountAmount());
            
            dataRequest.setConnection(connection);
            dataRequest.setStatementID(UPDATE_ORDER_HEADER);
            dataRequest.setInputParams(orderHeader);
  
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

            try
            {
                Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
                status = (String) outputs.get(STATUS_PARAM);
                if(status.equals("N"))
                {
                    message = (String) outputs.get(MESSAGE_PARAM);
                    returnStatus = false;
                    throw new Exception(message);
                }
                else
                {
                    returnStatus = true;
                }
            }
            finally
            {
           
            }
        }
        else
        {
            returnStatus = true;
        }
        return returnStatus;
    }

    /**
     * Update order items information.
     * 
     * @param order OrderVO value object that contains the whole order
     * @return  boolean true/false of update
     * @exception Exception
     */
    private boolean updateOrderItems(OrderVO order) throws Exception
    {
        // update the order detail information
        try
        {
            DataRequest dataRequest = new DataRequest();

            HashMap orderDetail = null;
            Collection items = order.getOrderDetail();            
            OrderDetailsVO item = null;
            Iterator iterator = null;
            SurchargeVO surchargeVO = null ;
            Calendar cal = null;
            if (order.getOrderDate() != null)
            {
              cal = Calendar.getInstance();
              cal.setTime(order.getOrderDateTime());
            }

            iterator = items.iterator();
                                
            for (int i = 0; iterator.hasNext(); i++) 
            {
                item = (OrderDetailsVO)iterator.next();

                if(item.isChanged())
                {
                    orderDetail = new HashMap();

                    //orderDetail.put("IN_ORDER_DETAIL_ID",new Integer(new Long(item.getOrderDetailId()).toString()));
                    orderDetail.put("IN_ORDER_DETAIL_ID",new Long(item.getOrderDetailId()));
                    orderDetail.put("IN_GUID",item.getGuid());
                    orderDetail.put("IN_LINE_NUMBER",item.getLineNumber());
                    //orderDetail.put("IN_RECIPIENT_ID",new Integer(new Long(item.getRecipientId()).toString()));
                    orderDetail.put("IN_RECIPIENT_ID", new Long(item.getRecipientId()));
                    //orderDetail.put("IN_RECIPIENT_ADDRESS_ID",new Integer(new Long(item.getRecipientAddressId()).toString()));
                    orderDetail.put("IN_RECIPIENT_ADDRESS_ID",new Long(item.getRecipientAddressId()));
                    orderDetail.put("IN_PRODUCT_ID",item.getProductId());
                    orderDetail.put("IN_QUANTITY",item.getQuantity());
                    orderDetail.put("IN_SHIP_VIA",item.getShipVia());
                    orderDetail.put("IN_SHIP_METHOD",item.getShipMethod());
                    orderDetail.put("IN_SHIP_DATE",item.getShipDate());
                    orderDetail.put("IN_DELIVERY_DATE",item.getDeliveryDate());
                    orderDetail.put("IN_DELIVERY_DATE_RANGE_END",item.getDeliveryDateRangeEnd());
                    orderDetail.put("IN_DROP_SHIP_TRACKING_NUMBER",item.getDropShipTrackingNumber());
                    orderDetail.put("IN_PRODUCTS_AMOUNT",item.getProductsAmount());
                    orderDetail.put("IN_TAX_AMOUNT",item.getTaxAmount());
                    orderDetail.put("IN_SERVICE_FEE_AMOUNT",item.getServiceFeeAmount());
                    orderDetail.put("IN_SHIPPING_FEE_AMOUNT",item.getShippingFeeAmount());
                    orderDetail.put("IN_ADD_ON_AMOUNT",item.getAddOnAmount());
                    orderDetail.put("IN_DISCOUNT_AMOUNT",item.getDiscountAmount());
                    orderDetail.put("IN_OCCASION_ID",item.getOccassionId());
                    orderDetail.put("IN_LAST_MINUTE_GIFT_SIGNATURE",item.getLastMinuteGiftSignature());
                    orderDetail.put("IN_LAST_MINUTE_NUMBER",item.getLastMinuteNumber());
                    orderDetail.put("IN_LAST_MINUTE_GIFT_EMAIL",item.getLastMinuteGiftEmail());
                    orderDetail.put("IN_EXTERNAL_ORDER_NUMBER",item.getExternalOrderNumber());
                    orderDetail.put("IN_COLOR_1",item.getColorFirstChoice());
                    orderDetail.put("IN_COLOR_2",item.getColorSecondChoice());
                    orderDetail.put("IN_SUBSTITUTE_ACKNOWLEDGEMENT",item.getSubstituteAcknowledgement());
                    orderDetail.put("IN_CARD_MESSAGE",item.getCardMessage());
                    orderDetail.put("IN_CARD_SIGNATURE",item.getCardSignature());
                    orderDetail.put("IN_ORDER_COMMENTS",item.getOrderComments());
                    orderDetail.put("IN_ORDER_CONTACT_INFORMATION",item.getOrderContactInformation());
                    orderDetail.put("IN_FLORIST_NUMBER",item.getFloristNumber());
                    orderDetail.put("IN_SPECIAL_INSTRUCTIONS",item.getSpecialInstructions());
                    orderDetail.put("IN_ARIBA_UNSPSC_CODE",item.getAribaUnspscCode());
                    orderDetail.put("IN_ARIBA_PO_NUMBER",item.getAribaPoNumber());
                    orderDetail.put("IN_ARIBA_AMS_PROJECT_CODE",item.getAribaAmsProjectCode());
                    orderDetail.put("IN_ARIBA_COST_CENTER",item.getAribaCostCenter());
                    orderDetail.put("IN_EXTERNAL_ORDER_TOTAL",item.getExternalOrderTotal());
                    orderDetail.put("IN_SIZE_INDICATOR",item.getSizeChoice());
                    orderDetail.put("IN_SENDER_INFO_RELEASE",item.getSenderInfoRelease());   
                    orderDetail.put("IN_STATUS",item.getStatus());
                    orderDetail.put("IN_MILES_POINTS",item.getMilesPoints());
                    orderDetail.put("IN_SUBCODE",item.getProductSubCodeId());
                    orderDetail.put("IN_MILES_POINTS_AMT",item.getMilesPointsAmt());
                    orderDetail.put("IN_PERSONAL_GREETING_ID",item.getPersonalGreetingId());

                    /*
                    * Item of the week
                    */
                    orderDetail.put("IN_SOURCE_CODE", item.getSourceCode());
                    orderDetail.put("IN_ITEM_OF_THE_WEEK_FLAG", item.getItemOfTheWeekFlag());
                    orderDetail.put("IN_REINSTATE_FLAG", item.getReinstateFlag());
                    orderDetail.put("IN_COMMISSION",item.getCommission());
                    orderDetail.put("IN_WHOLESALE",item.getWholesale());
                    orderDetail.put("IN_TRANSACTION",item.getTransaction());
                    orderDetail.put("IN_PDB_PRICE", item.getPdbPrice());
                    orderDetail.put("IN_SHIPPING_TAX",item.getShippingTax());
                    orderDetail.put("IN_PRICE_OVERRIDE_FLAG",item.getPriceOverrideFlag());
                    orderDetail.put("IN_DISCOUNTED_PRODUCT_PRICE",item.getDiscountedProductPrice());
                    orderDetail.put("IN_DISCOUNT_TYPE",item.getDiscountType());
                    orderDetail.put("IN_PERSONALIZATION", item.getPersonalizationData());
                    orderDetail.put("IN_PARTNER_COST",item.getPartnerCost());
                    orderDetail.put("IN_FIRST_ORDER_DOMESTIC", item.getDomesticFloristServiceCharge());
                    orderDetail.put("IN_FIRST_ORDER_INTERNATIONAL", item.getInternationalFloristServiceCharge());
                    orderDetail.put("IN_SHIPPING_COST", item.getShippingCost());
                    orderDetail.put("IN_VENDOR_CHARGE", item.getVendorCharge());
                    orderDetail.put("IN_VENDOR_SAT_UPCHARGE", item.getSaturdayUpcharge());
                    orderDetail.put("IN_AK_HI_SPECIAL_SVC_CHARGE", item.getAlaskaHawaiiSurcharge());
                    
//                    String sourceCode = item.getSourceCode();
//                    if (item.isChargeRelatedItemChanged())
//                    {
//                    logger.debug("using current date time for update order");
//                      surchargeVO = FTDFuelSurchargeUtilities.getSurchargeVO(null,sourceCode,connection );   
//                    }
//                    else
//                    {
//                      surchargeVO = FTDFuelSurchargeUtilities.getSurchargeVO(cal,sourceCode,connection );   
//                    }
                    
                    orderDetail.put("IN_APPLY_SURCHARGE_CODE", item.getApplySurchargeCode());     
                    orderDetail.put("IN_FUEL_SURCHARGE_AMT", item.getFuelSurcharge());     
                    orderDetail.put("IN_SURCHARGE_DESCRIPTION", item.getFuelSurchargeDescription());     
                    orderDetail.put("IN_DISPLAY_SURCHARGE", item.getDisplaySurcharge());     
                    orderDetail.put("IN_SEND_SURCHARGE_TO_FLORIST", item.getSendSurchargeToFlorist());     

                    orderDetail.put("IN_BIN_SOURCE_CHANGED_FLAG", item.getBinSourceChangedFlag());
                    orderDetail.put("IN_SHIPPING_FEE_AMOUNT_SAVED", stringToBigDecimal(item.getShippingFeeAmountSavings()));
                    orderDetail.put("IN_SERVICE_FEE_AMOUNT_SAVED", stringToBigDecimal(item.getServiceFeeAmountSavings()));
                    orderDetail.put("IN_SAME_DAY_UPCHARGE", stringToBigDecimal(item.getSameDayUpcharge()));
                    orderDetail.put("IN_ORIGINAL_ORDER_HAS_SDU", item.getOriginalOrderHasSDU());
                    orderDetail.put("IN_ORIGINAL_ORDER_HAS_SDUFS", item.getSduChargedtoFSMembers());
                    orderDetail.put("IN_MORNING_DELIVERY_FEE", stringToBigDecimal(item.getMorningDeliveryFee()));
                    if(new PartnerUtility().isPartnerOrder(order.getOrderOrigin(),order.getSourceCode(), connection) 
                    		&& "Y".equals(item.getOriginalOrderHasMDF()) && !"Y".equals(item.getMorningDeliveryOpted())) {
                    	item.setOriginalOrderHasMDF("N");
                    }
                    orderDetail.put("IN_ORIGINAL_ORDER_HAS_MDF", item.getOriginalOrderHasMDF());

                    orderDetail.put("IN_PC_GROUP_ID", item.getPcGroupId());
                    orderDetail.put("IN_PC_MEMBERSHIP_ID", item.getPcMembershipId());
                    orderDetail.put("IN_PC_FLAG", item.getPcFlag());
                    orderDetail.put("IN_TIME_OF_SERVICE", item.getTimeOfService());
                    
                    List<TaxVO> taxList = new ArrayList<TaxVO>();
                    taxList = item.getItemTaxVO().getTaxSplit();
                    boolean isTaxPerformed = true;
                    int count = 1;
                    
                    // FLYFLW orders or any order with old XML structure for Tax, should have default description and rate inserted.
                    CalculateTaxUtil taxUtil = new CalculateTaxUtil();
                    String defaultDesc = CalculateTaxUtil.DEFAULT_TAX_LABEL;            
                    try {
                   	 defaultDesc = taxUtil.getDefaultTaxLabel(CalculateTaxUtil.US_RECIPIENT, order.getCompanyId()); // Default label for US recipients
                    } catch (Exception e) {
                    	logger.error("Error getting the default tax label, " + e.getMessage());				
       			 	}
                    
                    for (TaxVO taxVO : taxList) {	
                    	
    					if (!StringUtils.isEmpty(taxVO.getDescription()) && isTaxPerformed && TaxSplitOrder.fromValue(taxVO.getDescription()).value() > 0) {
    						int splitOrder = TaxSplitOrder.fromValue(taxVO.getDescription()).value();
    						logger.debug("Saving tax, " + taxVO.getName() + ", into bucket, " + splitOrder);
    						if (splitOrder < 5) {
    							orderDetail.put("IN_TAX" + splitOrder + "_NAME", taxVO.getName());
    							orderDetail.put("IN_TAX" + splitOrder + "_AMOUNT", taxVO.getAmount());
    							orderDetail.put("IN_TAX" + splitOrder + "_DESCRIPTION", taxVO.getDescription());
    							orderDetail.put("IN_TAX" + splitOrder + "_RATE", taxVO.getRate());
    						}
    					} else {
    						isTaxPerformed = false;
							orderDetail.put("IN_TAX" + count + "_NAME", taxVO.getName());
							orderDetail.put("IN_TAX" + count + "_AMOUNT", taxVO.getAmount());
							
							if(StringUtils.isEmpty(taxVO.getDescription())) {
								orderDetail.put("IN_TAX" + count + "_DESCRIPTION", defaultDesc);
							} else {
								orderDetail.put("IN_TAX" + count + "_DESCRIPTION", taxVO.getDescription());
							}
							
							BigDecimal taxRate = taxVO.getRate();
							if((taxVO.getRate() == null || taxVO.getRate().compareTo(BigDecimal.ZERO) == 0) 
									&& taxVO.getAmount() != null && taxVO.getAmount().compareTo(BigDecimal.ZERO) > 0) {
								try {
									taxRate = taxUtil.reverseCalcTaxRate(item, taxVO.getAmount());									
								} catch (Exception e) {
									logger.error("Error calculating the rate for item: " + item.getExternalOrderNumber() + ", Error: " + e.getMessage());
								}
							}							
							orderDetail.put("IN_TAX" + count + "_RATE", taxRate);							
							count += 1;
    					}
    					
    				}
                 
    				for (int j = 1; j <= 5; j++) { 
    					if(!orderDetail.containsKey("IN_TAX" + (j) +"_NAME")) {
    						orderDetail.put("IN_TAX" + (j) +"_NAME", null);   	            		
    	            		orderDetail.put("IN_TAX" + (j) +"_AMOUNT", null);   	            		
    	            		orderDetail.put("IN_TAX" + (j) +"_DESCRIPTION", null);   	            		
    	            		orderDetail.put("IN_TAX" + (j) +"_RATE", null);  
    					}
    				}
						
					if(taxList != null && taxList.size() > 5) {  
                     	String errorMessage = "Error occurred while processing Taxes on order:" + item.getExternalOrderNumber() +
             			  ". Received " + taxList.size() + " tax nodes in Order XML (max is 5).  Please investiate.";
                     	logger.error(errorMessage);
                     	
                     	SystemMessengerVO systemMessageVO = new SystemMessengerVO();
                     	systemMessageVO.setLevel(SystemMessengerVO.LEVEL_PRODUCTION);
                     	systemMessageVO.setSource("SCRUB MAPPER DAO");
                     	systemMessageVO.setType("ERROR");
                     	systemMessageVO.setMessage(errorMessage);
                     	systemMessageVO.setSubject("TOO MANY TAXES INCLUDED IN ORDER");
                     	
                     	SystemMessenger sysMessenger = SystemMessenger.getInstance();
                     	sysMessenger.send(systemMessageVO, connection, false);
                     } 
					
					orderDetail.put("IN_ADD_ON_DISCOUNT_AMOUNT", item.getAddOnDiscountAmount());
					orderDetail.put("IN_LATE_CUTOFF_FEE", stringToBigDecimal(item.getLatecutoffCharge()));
		            orderDetail.put("IN_VEN_SUN_UPCHARGE", stringToBigDecimal(item.getSundayUpcharge())); 
		            orderDetail.put("IN_VEN_MON_UPCHARGE", stringToBigDecimal(item.getMondayUpcharge())); 
		            
					dataRequest.setConnection(connection);
                    dataRequest.setStatementID(UPDATE_ORDER_DETAILS);
                    dataRequest.setInputParams(orderDetail);
                    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
 
                    Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
                    status = (String) outputs.get(STATUS_PARAM);
                    if(status.equals("N"))
                    {
                        message = (String) outputs.get(MESSAGE_PARAM);
                        returnStatus = false;
                        throw new Exception(message);
                    }
                    else
                    {
                        returnStatus = true;
                    }
                }
                else
                {
                    returnStatus = true;
                }
            }
        }
        finally
        {}

        return returnStatus;
    }

    /**
     * Update order add ons information.
     * 
     * @param order OrderVO value object that contains the whole order
     * @return  boolean true/false of update
     * @exception Exception
     * 
     * emueller, 12/17/03 Addons will always be inserted, never updated.  
     * See Addon Insert.
     */
/*    private boolean updateAddOns(OrderVO order) throws Exception
    {
        // update the order add ons information
        try {
        DataRequest dataRequest = new DataRequest();

        HashMap orderAddOn = null;
        Collection items = order.getOrderDetail();            
        Collection addOns = null;
        OrderDetailsVO item = null;
        AddOnsVO addOn = null;
        Iterator iterator = null;
        Iterator addOnsIterator = null;
                
        iterator = items.iterator();
                                
        for (int i = 0; iterator.hasNext(); i++) 
        {
            item = (OrderDetailsVO)iterator.next();

            addOns = item.getAddOns();
            addOnsIterator = addOns.iterator();

            for (int x = 0; addOnsIterator.hasNext(); x++) 
            {
                addOn = (AddOnsVO)addOnsIterator.next();
                    
                orderAddOn = new HashMap();

                orderAddOn.put("IN_ADD_ON_ID",new Integer(new Long(addOn.getAddOnId()).toString()));
                orderAddOn.put("IN_ORDER_DETAIL_ID",new Integer(new Long(addOn.getOrderDetailId()).toString()));
                orderAddOn.put("IN_ADD_ON_CODE",addOn.getAddOnCode());
                orderAddOn.put("IN_ADD_ON_QUANTITY",addOn.getAddOnQuantity());
                //orderAddOn.put(STATUS_PARAM,"");
                //orderAddOn.put(MESSAGE_PARAM,"");
  
                dataRequest.setConnection(connection);
                dataRequest.setStatementID(UPDATE_ADD_ONS);
                dataRequest.setInputParams(orderAddOn);
  
                DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
 
                Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
                status = (String) outputs.get(STATUS_PARAM);
                if(status.equals("N"))
                {
                    message = (String) outputs.get(MESSAGE_PARAM);
                    returnStatus = false;
                    throw new Exception(message);
                }
                else
                {
                    returnStatus = true;
                }
            }
        }
        } finally { }
        return returnStatus;
    }
*/
    /**
     * Update recipients information.
     * 
     * @param order OrderVO value object that contains the whole order
     * @return  boolean true/false of update
     * @exception Exception
     */
    private boolean updateRecipients(OrderVO order) throws Exception
    {
        // update the order recipients information
        try 
        {
            DataRequest dataRequest = new DataRequest();

            HashMap orderRecipient = null;
            Collection items = order.getOrderDetail();            
            Collection recipients = null;
            OrderDetailsVO item = null;
            RecipientsVO recipient = null;
            Iterator iterator = null;
            Iterator recipientsIterator = null;
                
            iterator = items.iterator();
                                
            for (int i = 0; iterator.hasNext(); i++) 
            {
                item = (OrderDetailsVO)iterator.next();

                recipients = item.getRecipients();
                recipientsIterator = recipients.iterator();

                for (int x = 0; recipientsIterator.hasNext(); x++) 
                {
                    recipient = (RecipientsVO)recipientsIterator.next();

                    if(recipient.isChanged())
                    {
                        orderRecipient = new HashMap();

                        //orderRecipient.put("IN_RECIPIENT_ID",new Integer(new Long(recipient.getRecipientId()).toString()));
                        orderRecipient.put("IN_RECIPIENT_ID",new Long(recipient.getRecipientId()));
                        orderRecipient.put("IN_LAST_NAME",recipient.getLastName());
                        orderRecipient.put("IN_FIRST_NAME",recipient.getFirstName());
                        orderRecipient.put("IN_LIST_CODE",recipient.getListCode());
                        orderRecipient.put("IN_AUTO_HOLD",recipient.getAutoHold());
                        orderRecipient.put("IN_MAIL_LIST_CODE",recipient.getMailListCode());
                        orderRecipient.put("IN_STATUS",recipient.getStatus());
                        orderRecipient.put("IN_CUSTOMER_ID",recipient.getCustomerId());
                        //orderRecipient.put(STATUS_PARAM,"");
                        //orderRecipient.put(MESSAGE_PARAM,"");
  
                        dataRequest.setConnection(connection);
                        dataRequest.setStatementID(UPDATE_RECIPIENT);
                        dataRequest.setInputParams(orderRecipient);
  
                        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
 
                        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
                        status = (String) outputs.get(STATUS_PARAM);
                        if(status.equals("N"))
                        {
                            message = (String) outputs.get(MESSAGE_PARAM);
                            returnStatus = false;
                            throw new Exception(message);
                        }
                        else
                        {
                            returnStatus = true;
                        }
                    }
                }
            }
        } 
        finally 
        {}
        
        return returnStatus;
    }

    /**
     * Update recipient address information.
     * 
     * @param order OrderVO value object that contains the whole order
     * @return  boolean true/false of update
     * @exception Exception
     */
    private boolean updateRecipientAddresses(OrderVO order) throws Exception
    {
        // update the recipient phones information
        try
        {
            DataRequest dataRequest = new DataRequest();

            HashMap orderRecipientAddress = null;
            Collection items = order.getOrderDetail();            
            Collection recipients = null;
            Collection recipientAddresses = null;
            OrderDetailsVO item = null;
            RecipientsVO recipient = null;
            RecipientAddressesVO recipientAddress = null;
            Iterator iterator = null;
            Iterator recipientIterator = null;
            Iterator recipientAddressesIterator = null;
                
            iterator = items.iterator();
                                
            for (int i = 0; iterator.hasNext(); i++) 
            {
                item = (OrderDetailsVO)iterator.next();
                recipients = item.getRecipients();
                recipientIterator = recipients.iterator();
        
                for (int x = 0; recipientIterator.hasNext(); x++) 
                {
                    recipient = (RecipientsVO)recipientIterator.next();
                    recipientAddresses = recipient.getRecipientAddresses();
                    recipientAddressesIterator = recipientAddresses.iterator();
                
                    for (int y = 0; recipientAddressesIterator.hasNext(); y++) 
                    {
                        recipientAddress = (RecipientAddressesVO)recipientAddressesIterator.next();
                        
                        logger.info("Insertint the recipient Addresses Line 1 : "+recipientAddress.getAddressLine1()+" Line 2 : "+recipientAddress.getAddressLine2());

                        if(recipientAddress.isChanged())
                        {
                            orderRecipientAddress = new HashMap();

                            //orderRecipientAddress.put("IN_RECIPIENT_ADDRESS_ID",new Integer(new Long(recipientAddress.getRecipientAddressId()).toString()));
                            //orderRecipientAddress.put("IN_RECIPIENT_ID",new Integer(new Long(recipientAddress.getRecipientId()).toString()));
                            orderRecipientAddress.put("IN_RECIPIENT_ADDRESS_ID",new Long(recipientAddress.getRecipientAddressId()));
                            orderRecipientAddress.put("IN_RECIPIENT_ID",new Long(recipientAddress.getRecipientId()));
                            orderRecipientAddress.put("IN_ADDRESS_TYPE",recipientAddress.getAddressType());
                            orderRecipientAddress.put("IN_ADDRESS_LINE_1",recipientAddress.getAddressLine1());
                            orderRecipientAddress.put("IN_ADDRESS_LINE_2",recipientAddress.getAddressLine2());
                            orderRecipientAddress.put("IN_CITY",recipientAddress.getCity());
                            orderRecipientAddress.put("IN_STATE_PROVINCE",recipientAddress.getStateProvince());
                            orderRecipientAddress.put("IN_POSTAL_CODE",recipientAddress.getPostalCode());
                            orderRecipientAddress.put("IN_COUNTRY",recipientAddress.getCountry());
                            orderRecipientAddress.put("IN_COUNTY",recipientAddress.getCounty());
                            orderRecipientAddress.put("IN_INTERNATIONAL",recipientAddress.getInternational());
                            orderRecipientAddress.put("IN_DESTINATION_NAME",recipientAddress.getName());
                            orderRecipientAddress.put("IN_DESTINATION_INFO",recipientAddress.getInfo());
                            //orderRecipientAddress.put(STATUS_PARAM,"");
                            //orderRecipientAddress.put(MESSAGE_PARAM,"");
                            logger.info("Scrub Mapper DAO: update recipient address line 2" + recipientAddress.getAddressLine2());
                            
                            dataRequest.setConnection(connection);
                            dataRequest.setStatementID(UPDATE_RECIPIENT_ADDRESSES);
                            dataRequest.setInputParams(orderRecipientAddress);
  
                            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
 
                            Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
                            status = (String) outputs.get(STATUS_PARAM);
                            if(status.equals("N"))
                            {
                                message = (String) outputs.get(MESSAGE_PARAM);
                                returnStatus = false;
                                throw new Exception(message);
                            }
                            else
                            {
                                returnStatus = true;
                            }
                        }
                        else
                        {
                            returnStatus = true;
                        }
                    }
                }   
            }
        }
        finally
        {}
        
        return returnStatus;
    }

    /**
     * Update recipient phone information.
     * 
     * @param order OrderVO value object that contains the whole order
     * @return  boolean true/false of update
     * @exception Exception
     */
    private boolean updateRecipientPhones(OrderVO order) throws Exception
    {
        // update the recipient phones information
        try{
        DataRequest dataRequest = new DataRequest();

        HashMap orderRecipientPhone = null;
        Collection items = order.getOrderDetail();            
        Collection recipients = null;
        Collection recipientPhones = null;
        OrderDetailsVO item = null;
        RecipientsVO recipient = null;
        RecipientPhonesVO recipientPhone = null;
        Iterator iterator = null;
        Iterator recipientIterator = null;
        Iterator recipientPhonesIterator = null;
                
        iterator = items.iterator();
                                
        for (int i = 0; iterator.hasNext(); i++) 
        {
            item = (OrderDetailsVO)iterator.next();
            recipients = item.getRecipients();
            recipientIterator = recipients.iterator();
        
            for (int x = 0; recipientIterator.hasNext(); x++) 
            {
                recipient = (RecipientsVO)recipientIterator.next();
                recipientPhones = recipient.getRecipientPhones();
                recipientPhonesIterator = recipientPhones.iterator();
                
                  for (int y = 0; recipientPhonesIterator.hasNext(); y++) 
                  {
                      recipientPhone = (RecipientPhonesVO)recipientPhonesIterator.next();
                      
                      if(recipientPhone.isChanged())
                      {
                        orderRecipientPhone = new HashMap();

                        //orderRecipientPhone.put("IN_RECIPIENT_ID",new Integer(new Long(recipientPhone.getRecipientId()).toString()));
                        orderRecipientPhone.put("IN_RECIPIENT_ID",new Long(recipientPhone.getRecipientId()));
                        orderRecipientPhone.put("IN_PHONE_TYPE",recipientPhone.getPhoneType());
                        orderRecipientPhone.put("IN_PHONE_NUMBER",recipientPhone.getPhoneNumber());
                        orderRecipientPhone.put("IN_EXTENSION",recipientPhone.getExtension());
                        //orderRecipientPhone.put("RegisterOutParameterRecipientPhoneId","");
                        //orderRecipientPhone.put(STATUS_PARAM,"");
                        //orderRecipientPhone.put(MESSAGE_PARAM,"");
  
                        dataRequest.setConnection(connection);
                        dataRequest.setStatementID(UPDATE_RECIPIENT_PHONES);
                        dataRequest.setInputParams(orderRecipientPhone);
  
                        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
 
                        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
                        status = (String) outputs.get(STATUS_PARAM);
                        if(status.equals("N"))
                        {
                            message = (String) outputs.get(MESSAGE_PARAM);
                            returnStatus = false;
                            throw new Exception(message);
                        }
                        else
                        {
                            returnStatus = true;
                        }
                      }
                      else
                      {
                          returnStatus = true;
                      }
                  }
            }   
        }
        }finally{}
        return returnStatus;
    }
    
    
    /**
     * Update co brand information.
     * 
     * @param order OrderVO value object that contains the whole order
     * @return  boolean true/false of update
     * @exception Exception
     */
    private boolean updateCoBrand(OrderVO order) throws Exception
    {
        // update the order co brand information
        try{
        DataRequest dataRequest = new DataRequest();

        HashMap orderCoBrand = null;
        Collection coBrands = order.getCoBrand();            
        CoBrandVO coBrand = null;
        Iterator iterator = null;
                
        iterator = coBrands.iterator();
                                
        for (int i = 0; iterator.hasNext(); i++) 
        {
            coBrand = (CoBrandVO)iterator.next();

            if(coBrand.isChanged())
            {
                orderCoBrand = new HashMap();

                orderCoBrand.put("IN_GUID",order.getGUID());
                orderCoBrand.put("IN_INFO_NAME",coBrand.getInfoName());
                orderCoBrand.put("IN_INFO_DATA",coBrand.getInfoData());
                //orderCoBrand.put("RegisterOutParameterCoBrandId","");
                //orderCoBrand.put(STATUS_PARAM,"");
                //orderCoBrand.put(MESSAGE_PARAM,"");
                logger.debug("Updating Co-brand data - " + coBrand.getInfoName() + "/" + coBrand.getInfoData());
  
                dataRequest.setConnection(connection);
                dataRequest.setStatementID(UPDATE_CO_BRAND);
                dataRequest.setInputParams(orderCoBrand);
  
                DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
 
                Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
                status = (String) outputs.get(STATUS_PARAM);
                if(status.equals("N"))
                {
                    message = (String) outputs.get(MESSAGE_PARAM);
                    returnStatus = false;
                    throw new Exception(message);
                }
                else
                {
                    returnStatus = true;
                }
            }
            else
            {
                returnStatus = true;
            }
        }
        }finally{}
        return returnStatus;
    }

    /**
     * Update membership information.
     * 
     * @param order OrderVO value object that contains the whole order
     * @return  boolean true/false of update
     * @exception Exception
     */
    private boolean updateMembership(OrderVO order) throws Exception
    {
        // update the order membership information
        try{
        DataRequest dataRequest = new DataRequest();

        HashMap orderMembership = null;
        Collection memberships = order.getMemberships();            
        MembershipsVO membership = null;
        Iterator iterator = null;
        long membershipId = 0;
                
        iterator = memberships.iterator();
                                
        for (int i = 0; iterator.hasNext(); i++) 
        {
            membership = (MembershipsVO)iterator.next();
            if(membership.isChanged())
            {
                orderMembership = new HashMap();

                //orderMembership.put("IN_BUYER_ID",new Integer(new Long(order.getBuyerId()).toString()));
                orderMembership.put("IN_BUYER_ID",new Long(order.getBuyerId()));
                orderMembership.put("IN_MEMBERSHIP_TYPE",membership.getMembershipType());
                orderMembership.put("IN_LAST_NAME",membership.getLastName());
                orderMembership.put("IN_FIRST_NAME",membership.getFirstName());
                orderMembership.put("IN_MEMBERSHIP_ID_NUMBER",membership.getMembershipIdNumber());
                logger.debug("Update membership - " + membership.getMembershipType() + "/" + membership.getMembershipIdNumber());
                membershipId = order.getMembershipId();
                
                if(membershipId != 0){
                  //orderMembership.put("RegisterOutParameterMembershipId",new Integer(new Long(order.getMembershipId()).toString()));
                  orderMembership.put("RegisterOutParameterMembershipId",new Long(order.getMembershipId()));
                }else {
                  orderMembership.put("RegisterOutParameterMembershipId",new Integer(0));
                }
                //orderMembership.put(STATUS_PARAM,"");
                //orderMembership.put(MESSAGE_PARAM,"");
  
                dataRequest.setConnection(connection);
                dataRequest.setStatementID(UPDATE_MEMBERSHIPS);
                dataRequest.setInputParams(orderMembership);
  
                DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
 
                Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
                status = (String) outputs.get(STATUS_PARAM);
                if(status.equals("N"))
                {
                    message = (String) outputs.get(MESSAGE_PARAM);
                    returnStatus = false;
                    throw new Exception(message);
                }
                else
                {
                    membershipId = Long.parseLong(outputs.get("RegisterOutParameterMembershipId").toString());
                    order.setMembershipId(membershipId);
                    returnStatus = true;
                }
            }
            else
            {
                returnStatus = true;
            }
        }
        }finally{}
        return returnStatus;
    }

    /**
     * Update payments information.
     * 
     * @param order OrderVO value object that contains the whole order
     * @return  boolean true/false of update
     * @exception Exception
     */
    private boolean updatePayments(OrderVO order) throws Exception
    { 
        // update the order payments information
        try{
        DataRequest dataRequest = new DataRequest();

        HashMap orderPayment = null;
        Collection payments = order.getPayments();            
        PaymentsVO payment = null;
        Iterator iterator = null;
        long creditCardId = 0;
                
        iterator = payments.iterator();
                                
        for (int i = 0; iterator.hasNext(); i++) 
        {
            payment = (PaymentsVO)iterator.next();
            
            boolean removeAuthFlag =  isPGAuthAmountIncreased(payment); 
            logger.info("removeAuthFlag ::"+removeAuthFlag);
            //logger.info("Before Payment VO Object ::"+payment.toString());
            if(removeAuthFlag) resetPaymentAuthorization(payment);
            //logger.info("After Payment VO Object ::"+payment.toString());
            
            //Add check do only perform update if payment id is greater then zero.
            //If the id is zero then this is a new payment and update should not be done.
            //This is related to defect #439  (emueller, 3/5/04)
            if(payment.isChanged() && payment.getPaymentId() > 0)
            {
                orderPayment = new HashMap();
                
                //orderPayment.put("IN_PAYMENT_ID",new Integer(new Long(payment.getPaymentId()).toString()));
                orderPayment.put("IN_PAYMENT_ID",new Long(payment.getPaymentId()));
                orderPayment.put("IN_GUID",payment.getGuid());
                orderPayment.put("IN_PAYMENT_TYPE",payment.getPaymentType());
                orderPayment.put("IN_AMOUNT",payment.getAmount());
            
                //This code is here because the DataAccess object currently does not
                //support the insertion of nulls in Long fields.  This is a workaround,
                //Sasha is deciding how this problem will be corrected.
                if(payment.getCreditCardId() == 0){
                    orderPayment.put("IN_CREDIT_CARD_ID", Integer.valueOf(String.valueOf(java.sql.Types.VARCHAR)));
                    dataRequest.setStatementID(UPDATE_PAYMENT_NOCREDITCARD);
                }
                else
                {
                    //orderPayment.put("IN_CREDIT_CARD_ID",new Integer(new Long(payment.getCreditCardId()).toString()));
                	orderPayment.put("IN_CREDIT_CARD_ID",new Long(payment.getCreditCardId()));
                    dataRequest.setStatementID(UPDATE_PAYMENTS);              
                }
                        
                orderPayment.put("IN_GIFT_CERTIFICATE_ID",payment.getGiftCertificateId());
                orderPayment.put("IN_AUTH_RESULT",payment.getAuthResult());
                orderPayment.put("IN_AUTH_NUMBER",payment.getAuthNumber());
                orderPayment.put("IN_AVS_CODE",payment.getAvsCode());
                orderPayment.put("IN_ACQ_REFERENCE_NUMBER",payment.getAcqReferenceNumber());
                orderPayment.put("IN_AAFES_TICKET",payment.getAafesTicket());
                orderPayment.put("IN_INVOICE_NUMBER",payment.getInvoiceNumber());
                orderPayment.put("IN_AP_AUTH_TXT",payment.getApAuth());
                orderPayment.put("IN_AP_ACCOUNT_ID",payment.getApAccountId());
                orderPayment.put("IN_MILES_POINTS_AMT",payment.getMilesPointsAmt());
                orderPayment.put("IN_NO_CHARGE_USERID", payment.getNcApprovalId());
                orderPayment.put("IN_CSC_RESPONSE_CODE",payment.getCscResponseCode());
                orderPayment.put("IN_CSC_VALIDATED_FLAG",payment.getCscValidatedFlag());
                orderPayment.put("IN_CSC_FAILURE_CNT",new BigDecimal(payment.getCscFailureCount()));
                orderPayment.put("IN_WALLET_INDICATOR",payment.getWalletIndicator());
                orderPayment.put("IN_CC_AUTH_PROVIDER", payment.getCcAuthProvider());
                orderPayment.put("IN_CARDINAL_VERIFIED_FLAG", payment.getCardinalVerifiedFlag());
                orderPayment.put("IN_ROUTE", payment.getRoute());
                orderPayment.put("IN_TOKEN_ID", payment.getTokenId());
                orderPayment.put("IN_AUTH_TRANSACTION_ID", payment.getAuthTransactionId());
                //orderPayment.put(STATUS_PARAM,"");
                //orderPayment.put(MESSAGE_PARAM,"");
  
                dataRequest.setConnection(connection);
                dataRequest.setInputParams(orderPayment);
  
                DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
 
                Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
                status = (String) outputs.get(STATUS_PARAM);
                if(status.equals("N"))
                {
                    message = (String) outputs.get(MESSAGE_PARAM);
                    returnStatus = false;
                    throw new Exception(message);
                }
                else
                {
                    returnStatus = true;
                }
            }
            else
            {
                returnStatus = true;
            }
            //check order amount increases by certain percentage for corresponding card type and reset the cardinal data
            if("Y".equalsIgnoreCase(payment.getCardinalVerifiedFlag()) && order.isResetCardinal()){
            	resetCardinalData(payment);
            }
        }
        }finally{}
        return returnStatus;
    }
    
    /**
     * check order amount increases by certain percentage for corresponding card type and reset the cardinal data
     * 
     * @param payment
     * @throws Exception
     */
    private void resetCardinalData(PaymentsVO payment) throws Exception{
        
    	HashMap resetCardinalPayment = null;
    	try{
    		resetCardinalPayment = new HashMap();
    		DataRequest dataRequest = new DataRequest();
    		dataRequest.setStatementID(RESET_CARDINAL_DATA);
    		resetCardinalPayment.put("IN_PAYMENT_ID",new Long(payment.getPaymentId()));
    		resetCardinalPayment.put("IN_ORDER_GUID",payment.getGuid());
    		dataRequest.setConnection(connection);
            dataRequest.setInputParams(resetCardinalPayment);

            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

            Map outputs = (Map) dataAccessUtil.execute(dataRequest);
  
            status = (String) outputs.get("OUT_STATUS");
            if(status.equals("N"))
            {
                message = (String) outputs.get("OUT_MESSAGE");
                returnStatus = false;
                throw new Exception(message);
            }
            else
            {
                returnStatus = true;
            }
            
            
    	}
    	finally{
    		
    	}
    }

    /**
     * Update credit card information.
     * 
     * @param order OrderVO value object that contains the whole order
     * @return  boolean true/false of update
     * @exception Exception
     */
    private boolean updateCreditCards(OrderVO order) throws Exception
    {
        // update the order credit cards information
        try{
        DataRequest dataRequest = new DataRequest();

        HashMap orderCreditCard = null;
        Collection payments = order.getPayments();            
        Collection creditCards = null;
        PaymentsVO payment = null;
        PaymentsVO newPayment = null;
        CreditCardsVO creditCard = null;
        Iterator iterator = null;
        Iterator paymentsIterator = null;
        Iterator creditCardsIterator = null;
        List paymentsList = null;
        long creditCardId = 0;
        String paymentRoute=null;
                
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
        String globalRoutes = configUtil.getFrpGlobalParm("SERVICE","PAYMENT_GATEWAY_ROUTES");
        String jccasCardTypes = configUtil.getFrpGlobalParm("AUTH_CONFIG","bams.cc.list");
		
        iterator = payments.iterator();
        
        for (int i = 0; iterator.hasNext(); i++) 
        {
            payment = (PaymentsVO)iterator.next();
            paymentRoute = payment.getRoute();
            
            creditCards = payment.getCreditCards();
            if(creditCards != null)
            {
              creditCardsIterator = creditCards.iterator();

              for (int x = 0; creditCardsIterator.hasNext(); x++) 
              {
                  creditCard = (CreditCardsVO)creditCardsIterator.next();

                  if(creditCard.isChanged())
                  {
                	  logger.info("Credit Card Information Changed for paymentId " + payment.getPaymentId());
                      orderCreditCard = new HashMap();

                      //orderCreditCard.put("IN_BUYER_ID", new Integer(new Long(order.getBuyerId()).toString()));
                      orderCreditCard.put("IN_BUYER_ID", new Long(order.getBuyerId()));
                      orderCreditCard.put("IN_CC_TYPE",creditCard.getCCType());
                      orderCreditCard.put("IN_CC_NUMBER",creditCard.getCCNumber());
                      orderCreditCard.put("IN_CC_EXPIRATION",creditCard.getCCExpiration());
                      orderCreditCard.put("IN_PIN",creditCard.getPin());
                      //orderCreditCard.put("RegisterOutParameterCreditCardId","");
                      //orderCreditCard.put(STATUS_PARAM,"");
                      //orderCreditCard.put(MESSAGE_PARAM,"");
  
                      dataRequest.setConnection(connection);
                      dataRequest.setStatementID(UPDATE_CREDIT_CARDS);
                      dataRequest.setInputParams(orderCreditCard);
  
                      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
 
                      Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
                      status = (String) outputs.get(STATUS_PARAM);
                      if(status.equals("N"))
                      {
                          message = (String) outputs.get(MESSAGE_PARAM);
                          returnStatus = false;
                          throw new Exception(message);
                      }
                      else
                      {
                          creditCardId = Long.parseLong(outputs.get("RegisterOutParameterCreditCardId").toString());
                    
                          paymentsIterator = payments.iterator();
                          paymentsList = new ArrayList();
                          //loop through the payments
                          //if payment type equals credit card
                          //then set the credit card id in the PaymentVO
                          //to the creditCardId returned from the database
                          for (int j = 0; paymentsIterator.hasNext(); j++) 
                          {
                              newPayment = (PaymentsVO)paymentsIterator.next();
                              if(newPayment.getPaymentType() != null && !newPayment.getPaymentType().equals("IN") && 
                                 !newPayment.getPaymentType().equals("GC") && !newPayment.getPaymentType().equals("NC")  && newPayment.getPaymentType().equalsIgnoreCase(creditCard.getCCType()))
                              {
                                creditCards = newPayment.getCreditCards();
                                logger.info("tokenId: " + newPayment.getTokenId() + " creditCardId: " + newPayment.getCreditCardId());;
                                boolean detokenize = false;
                                if (newPayment.getTokenId() != null && newPayment.getCreditCardId() <= 0) {
                                    detokenize = true;
                                    logger.info("skip reset for detokenized credit cards");
                                }
                                newPayment.setCreditCardId(creditCardId);
                                if (!detokenize) {
                                    //Reset CC-auth-provider,  CardinalVerifiedFlag and Authorization Route if the cc-type or cc-number is changed
                                    Iterator ccItr = creditCards.iterator();
                                    for(int ccInt = 0; ccItr.hasNext(); ccInt++){
                                    	CreditCardsVO cc = (CreditCardsVO)ccItr.next();
                                    	if(cc.isCcNumberChanged() || cc.isCcTypeChanged()){
                                    		newPayment.setCcAuthProvider(null);
                                    		newPayment.setCardinalVerifiedFlag("N");
                                		
							    			newPayment.setRoute(null);
								    		if (paymentRoute != null)
									    	{
										    	if(jccasCardTypes.contains(newPayment.getPaymentType()))
											    {	
												    if(globalRoutes.contains(PG_JCCAS))
    											  	 newPayment.setRoute(PG_JCCAS);
	    										}else if(globalRoutes.contains(PG_PS))
		    									{	
			    									newPayment.setRoute(PG_PS);
				    							}	
					    					}
						    				logger.info("newPayment Route in Scrub" + newPayment.getRoute());
                                    		break;
                                    	}
                                    }
                                }
                          	  	
                                paymentsList.add(newPayment);
                              }
                              else
                              {
                                paymentsList.add(newPayment);
                              }
                          }
                          order.setPayments(paymentsList);
                          returnStatus = true;
                      }//end else
                }
                else
                {
                    returnStatus = true;
                }
                }//end inner for loop
            }//end if
        }//end outer for loop
        }finally{}
        return returnStatus;
    }
    
    /**
     * Update order contact information into the order_contact_info
     * table in scrub
     * @param order OrderVO Value object that contains the
     * whole order
     * @exception Exception if any errors are encountered
     * @return boolean returns a true if contact information was 
     * successfully updated in the order_contact_info table, else it 
     * returns a false
     */
    private boolean updateOrderContactInfo(OrderVO order) throws Exception
    {
        // update the order contact information
        try {
        DataRequest dataRequest = new DataRequest();

        HashMap orderContactInfo = null;
        Collection contactInfo = order.getOrderContactInfo();
        OrderContactInfoVO orderContact = null;
        Iterator iterator = null;
        long orderContactInfoId = 0;

        if(contactInfo != null)
        {
                
        iterator = contactInfo.iterator();
                                
        for (int i = 0; iterator.hasNext(); i++) 
        {
            orderContact = (OrderContactInfoVO)iterator.next();

            if(orderContact.isChanged())
            {
                orderContactInfo = new HashMap();

                //orderContactInfo.put("IN_ORDER_CONTACT_INFO_ID", new Integer(new Long(orderContact.getOrderContactInfoId()).toString()));
                orderContactInfo.put("IN_ORDER_CONTACT_INFO_ID", new Long(orderContact.getOrderContactInfoId()));
                orderContactInfo.put("IN_ORDER_GUID", order.getGUID());
                orderContactInfo.put("IN_FIRST_NAME", orderContact.getFirstName());
                orderContactInfo.put("IN_LAST_NAME", orderContact.getLastName());
                orderContactInfo.put("IN_PHONE", orderContact.getPhone());
                orderContactInfo.put("IN_EXT", orderContact.getExt());
                orderContactInfo.put("IN_EMAIL", orderContact.getEmail());
                //orderContactInfo.put("STATUS_PARAM", "");
                //orderContactInfo.put("MESSAGE_PARAM", "");
  
                dataRequest.setConnection(connection);
                dataRequest.setStatementID(UPDATE_ORDER_CONTACT_INFO);
                dataRequest.setInputParams(orderContactInfo);
  
                DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
 
                Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
                status = (String) outputs.get(STATUS_PARAM);

                if(status.equals("N"))
                {
                    message = (String) outputs.get(MESSAGE_PARAM);
                    returnStatus = false;
                    throw new Exception(message);
                }
                else
                {
                    returnStatus = true;
                }
            }
            else
            {
                returnStatus = true;
            }            
        }//end for loop
        }//end if
        }//end try 
        finally
        {
          
        }
        return returnStatus;
    }

    /**
     * Update buyers information.
     * 
     * @param order OrderVO value object that contains the whole order
     * @return  boolean true/false of update
     * @exception Exception
     */
    private boolean updateBuyers(OrderVO order) throws Exception
    {
        // update the order co brand information
        try {
        DataRequest dataRequest = new DataRequest();

        HashMap orderBuyer = null;        
        HashMap cleanBuyer = null;
        Collection buyers = order.getBuyer();            
        BuyerVO buyer = null;
        Iterator iterator = null;
                
        iterator = buyers.iterator();
                                
        for (int i = 0; iterator.hasNext(); i++) 
        {
            buyer = (BuyerVO)iterator.next();

            if(buyer.isChanged())
            {
                orderBuyer = new HashMap();

                //orderBuyer.put("IN_BUYER_ID",new Integer(new Long(buyer.getBuyerId()).toString()));
                orderBuyer.put("IN_BUYER_ID",new Long(buyer.getBuyerId()));
                orderBuyer.put("IN_CUSTOMER_ID",buyer.getCustomerId());
                orderBuyer.put("IN_LAST_NAME",buyer.getLastName());
                orderBuyer.put("IN_FIRST_NAME",buyer.getFirstName());
                orderBuyer.put("IN_AUTO_HOLD",buyer.getAutoHold());
                orderBuyer.put("IN_BEST_CUSTOMER",buyer.getBestCustomer());
                orderBuyer.put("IN_STATUS",buyer.getStatus());
                //orderBuyer.put(STATUS_PARAM,"");
                //orderBuyer.put(MESSAGE_PARAM,"");
  
                dataRequest.setConnection(connection);
                dataRequest.setStatementID(UPDATE_BUYERS);
                dataRequest.setInputParams(orderBuyer);
  
                DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
 
                Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
                status = (String) outputs.get(STATUS_PARAM);
                if(status.equals("N"))
                {
                    message = (String) outputs.get(MESSAGE_PARAM);
                    returnStatus = false;
                    throw new Exception(message);
                }
                else
                {
                    returnStatus = true;
                }
                
                //if the order contains a clean id then udpate the clean schema
                if(buyer.getCleanCustomerId() != null)
                {
                    logger.debug("Updating clean customer " + buyer.getCleanCustomerId());                    
                    cleanBuyer = new HashMap();
                    cleanBuyer.put("IN_BUYER_ID",new Long(buyer.getBuyerId()));
                    cleanBuyer.put("IN_CLEAN_BUYER_ID",buyer.getCleanCustomerId());
                    cleanBuyer.put("IN_ORDER_GUID",order.getGUID());   
      
                    dataRequest.setConnection(connection);
                    dataRequest.setStatementID("UPDATE_CUSTOMER_FROM_BUYER");
                    dataRequest.setInputParams(cleanBuyer);      

                    Map cleanOutputs = (Map) dataAccessUtil.execute(dataRequest);
      
                    status = (String) cleanOutputs.get(STATUS_PARAM);
                    if(status.equals("N"))
                    {
                        message = (String) cleanOutputs.get(MESSAGE_PARAM);
                        returnStatus = false;
                        throw new Exception(message);
                    }

                }

                
                
                
            }
            else
            {
                returnStatus = true;
            }
        }
        }finally{}
        return returnStatus;
    }

    /**
     * Update buyer address information.
     * 
     * @param order OrderVO value object that contains the whole order
     * @return  boolean true/false of update
     * @exception Exception
     */
    private boolean updateBuyerAddresses(OrderVO order) throws Exception
    {
        // update the buyer address information
        try{
        DataRequest dataRequest = new DataRequest();

        HashMap cleanBuyer = null;
        HashMap orderBuyerAddress = null;
        Collection buyers = order.getBuyer();            
        Collection buyerAddresses = null;
        BuyerVO buyer = null;
        BuyerAddressesVO buyerAddress = null;
        Iterator iterator = null;
        Iterator buyerAddressesIterator = null;
                
        iterator = buyers.iterator();
                                
        for (int i = 0; iterator.hasNext(); i++) 
        {
            buyer = (BuyerVO)iterator.next();

            buyerAddresses = buyer.getBuyerAddresses();
            buyerAddressesIterator = buyerAddresses.iterator();

            for (int x = 0; buyerAddressesIterator.hasNext(); x++) 
            {
                buyerAddress = (BuyerAddressesVO)buyerAddressesIterator.next();
                if(buyerAddress.isChanged())
                {
                    orderBuyerAddress = new HashMap();

                    //orderBuyerAddress.put("IN_BUYER_ADDRESS_ID",new Integer(new Long(buyerAddress.getBuyerAddressId()).toString()));
                    //orderBuyerAddress.put("IN_BUYER_ID",new Integer(new Long(buyerAddress.getBuyerId()).toString()));
                    orderBuyerAddress.put("IN_BUYER_ADDRESS_ID",new Long(buyerAddress.getBuyerAddressId()));
                    orderBuyerAddress.put("IN_BUYER_ID",new Long(buyerAddress.getBuyerId()));
                    orderBuyerAddress.put("IN_ADDRESS_TYPE",buyerAddress.getAddressType());
                    orderBuyerAddress.put("IN_ADDRESS_LINE_1",buyerAddress.getAddressLine1());
                    orderBuyerAddress.put("IN_ADDRESS_LINE_2",buyerAddress.getAddressLine2());
                    orderBuyerAddress.put("IN_CITY",buyerAddress.getCity());
                    orderBuyerAddress.put("IN_STATE_PROVINCE",buyerAddress.getStateProv());
                    orderBuyerAddress.put("IN_POSTAL_CODE",buyerAddress.getPostalCode());
                    orderBuyerAddress.put("IN_COUNTRY",buyerAddress.getCountry());
                    orderBuyerAddress.put("IN_COUNTY",buyerAddress.getCounty());
                    orderBuyerAddress.put("IN_ADDRESS_ETC",buyerAddress.getAddressEtc());
                    //orderBuyerAddress.put(STATUS_PARAM,"");
                    //orderBuyerAddress.put(MESSAGE_PARAM,"");
  
  
                    dataRequest.setConnection(connection);
                    dataRequest.setStatementID(UPDATE_BUYER_ADDRESSES);
                    dataRequest.setInputParams(orderBuyerAddress);
  
                    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
 
                    Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
                    status = (String) outputs.get(STATUS_PARAM);
                    if(status.equals("N"))
                    {
                        message = (String) outputs.get(MESSAGE_PARAM);
                        returnStatus = false;
                        throw new Exception(message);
                    }
                    else
                    {
                        returnStatus = true;
                    }
                    
                    
                //if the order contains a clean id then udpate the clean schema
                if(buyer.getCleanCustomerId() != null)
                {
                    logger.debug("Updating clean customer " + buyer.getCleanCustomerId());                    
                    cleanBuyer = new HashMap();
                    cleanBuyer.put("IN_BUYER_ID",new Long(buyer.getBuyerId()));
                    cleanBuyer.put("IN_CLEAN_BUYER_ID",buyer.getCleanCustomerId());
                    cleanBuyer.put("IN_ORDER_GUID",order.getGUID());   
      
                    dataRequest.setConnection(connection);
                    dataRequest.setStatementID("UPDATE_CUSTOMER_FROM_BUYER");
                    dataRequest.setInputParams(cleanBuyer);      

                    Map cleanOutputs = (Map) dataAccessUtil.execute(dataRequest);
      
                    status = (String) cleanOutputs.get(STATUS_PARAM);
                    if(status.equals("N"))
                    {
                        message = (String) cleanOutputs.get(MESSAGE_PARAM);
                        returnStatus = false;
                        throw new Exception(message);
                    }

                }                    
                    
                    
                    
                }
                else
                {
                    returnStatus = true;
                }
            }
        }
        }finally{}
        return returnStatus;
    }

    /**
     * Update buyer comment information.
     * 
     * @param order OrderVO value object that contains the whole order
     * @return  boolean true/false of update
     * @exception Exception
     */
    private boolean updateBuyerComments(OrderVO order) throws Exception
    {
        // update the buyer comments information
        try{
        DataRequest dataRequest = new DataRequest();

        HashMap orderBuyerComment = null;
        Collection buyers = order.getBuyer();            
        Collection buyerComments = null;
        BuyerVO buyer = null;
        BuyerCommentsVO buyerComment = null;
        Iterator iterator = null;
        Iterator buyerCommentsIterator = null;
                
        iterator = buyers.iterator();
                                
        for (int i = 0; iterator.hasNext(); i++) 
        {
            buyer = (BuyerVO)iterator.next();

            buyerComments = buyer.getBuyerComments();
            buyerCommentsIterator = buyerComments.iterator();

            for (int x = 0; buyerCommentsIterator.hasNext(); x++) 
            {
                buyerComment = (BuyerCommentsVO)buyerCommentsIterator.next();
                if(buyerComment.isChanged())
                {
                    orderBuyerComment = new HashMap();

                    //orderBuyerComment.put("IN_BUYER_COMMENT_ID",new Integer(new Long(buyerComment.getBuyerCommentId()).toString()));
                    //orderBuyerComment.put("IN_BUYER_ID",new Integer(new Long(buyerComment.getBuyerId()).toString()));
                    orderBuyerComment.put("IN_BUYER_COMMENT_ID",new Long(buyerComment.getBuyerCommentId()));
                    orderBuyerComment.put("IN_BUYER_ID",new Long(buyerComment.getBuyerId()));
                    orderBuyerComment.put("IN_TEXT",buyerComment.getText());
                    //orderBuyerComment.put(STATUS_PARAM,"");
                    //orderBuyerComment.put(MESSAGE_PARAM,"");
  
                    dataRequest.setConnection(connection);
                    dataRequest.setStatementID(UPDATE_BUYER_COMMENTS);
                    dataRequest.setInputParams(orderBuyerComment);
  
                    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
 
                    Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
                    status = (String) outputs.get(STATUS_PARAM);
                    if(status.equals("N"))
                    {
                        message = (String) outputs.get(MESSAGE_PARAM);
                        returnStatus = false;
                        throw new Exception(message);
                    }
                    else
                    {
                        returnStatus = true;
                    }
                }
                else
                {
                    returnStatus = true;
                }
            }
        }
        }finally{}
        return returnStatus;
    }

    /**
     * Update buyer phone information.
     * 
     * @param order OrderVO value object that contains the whole order
     * @return  boolean true/false of update
     * @exception Exception
     */
    private boolean updateBuyerPhones(OrderVO order) throws Exception
    {
        // update the buyer phones information
        try{
        DataRequest dataRequest = new DataRequest();
        HashMap cleanBuyer = null;
        HashMap orderBuyerPhone = null;
        Collection buyers = order.getBuyer();            
        Collection buyerPhones = null;
        BuyerVO buyer = null;
        BuyerPhonesVO buyerPhone = null;
        Iterator iterator = null;
        Iterator buyerPhonesIterator = null;
                
        iterator = buyers.iterator();
                                
        for (int i = 0; iterator.hasNext(); i++) 
        {
            buyer = (BuyerVO)iterator.next();

            buyerPhones = buyer.getBuyerPhones();
            buyerPhonesIterator = buyerPhones.iterator();

            for (int x = 0; buyerPhonesIterator.hasNext(); x++) 
            {
                buyerPhone = (BuyerPhonesVO)buyerPhonesIterator.next();

                if(buyerPhone.isChanged())
                {
                    orderBuyerPhone = new HashMap();

                    //orderBuyerPhone.put("IN_BUYER_ID",new Integer(new Long(buyer.getBuyerId()).toString()));
                    orderBuyerPhone.put("IN_BUYER_ID",new Long(buyer.getBuyerId()));
                    orderBuyerPhone.put("IN_PHONE_TYPE",buyerPhone.getPhoneType());
                    orderBuyerPhone.put("IN_PHONE_NUMBER",buyerPhone.getPhoneNumber());
                    orderBuyerPhone.put("IN_EXTENSION",buyerPhone.getExtension());
                    orderBuyerPhone.put("IN_PHONE_NUMBER_TYPE",buyerPhone.getPhoneNumberType());
                    orderBuyerPhone.put("IN_SMS_OPT_IN",buyerPhone.getSmsOptIn());
                    //orderBuyerPhone.put("RegisterOutParameterBuyerPhoneId",new Integer(0));
                    //orderBuyerPhone.put(STATUS_PARAM,"");
                    //orderBuyerPhone.put(MESSAGE_PARAM,"");
  
                    dataRequest.setConnection(connection);
                    dataRequest.setStatementID(UPDATE_BUYER_PHONES);
                    dataRequest.setInputParams(orderBuyerPhone);
  
                    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
 
                    Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
                    status = (String) outputs.get(STATUS_PARAM);
                    if(status.equals("N"))
                    {
                        message = (String) outputs.get(MESSAGE_PARAM);
                        returnStatus = false;
                        throw new Exception(message);
                    }
                    else
                    {
                        returnStatus = true;
                    }
                    
                //if the order contains a clean id then udpate the clean schema
                if(buyer.getCleanCustomerId() != null)
                {
                    logger.debug("Updating clean customer " + buyer.getCleanCustomerId());                    
                    cleanBuyer = new HashMap();
                    cleanBuyer.put("IN_BUYER_ID",new Long(buyer.getBuyerId()));
                    cleanBuyer.put("IN_CLEAN_BUYER_ID",buyer.getCleanCustomerId());
                    cleanBuyer.put("IN_ORDER_GUID",order.getGUID());   
      
                    dataRequest.setConnection(connection);
                    dataRequest.setStatementID("UPDATE_CUSTOMER_FROM_BUYER");
                    dataRequest.setInputParams(cleanBuyer);      

                    Map cleanOutputs = (Map) dataAccessUtil.execute(dataRequest);
      
                    status = (String) cleanOutputs.get(STATUS_PARAM);
                    if(status.equals("N"))
                    {
                        message = (String) cleanOutputs.get(MESSAGE_PARAM);
                        returnStatus = false;
                        throw new Exception(message);
                    }

                }                    
                    
                }
                else
                {
                    returnStatus = true;
                }
            }
        }
        }finally{}
        return returnStatus;
    }

    /**
     * Update buyer email information.
     * 
     * @param order OrderVO value object that contains the whole order
     * @return  boolean true/false of update
     * @exception Exception
     */
    private boolean updateBuyerEmails(OrderVO order) throws Exception
    {
        // update the buyer emails information
        try{
        DataRequest dataRequest = new DataRequest();

        HashMap cleanBuyer = null;

        HashMap orderBuyerEmail = null;
        Collection buyers = order.getBuyer();            
        Collection buyerEmails = null;
        BuyerVO buyer = null;
        BuyerEmailsVO buyerEmail = null;
        Iterator iterator = null;
        Iterator buyerEmailsIterator = null;
                
        iterator = buyers.iterator();
                                
        for (int i = 0; iterator.hasNext(); i++) 
        {
            buyer = (BuyerVO)iterator.next();

            buyerEmails = buyer.getBuyerEmails();
            buyerEmailsIterator = buyerEmails.iterator();

            for (int x = 0; buyerEmailsIterator.hasNext(); x++) 
            {
                buyerEmail = (BuyerEmailsVO)buyerEmailsIterator.next();
                if(buyerEmail.isChanged())
                {
                    orderBuyerEmail = new HashMap();

                    //orderBuyerEmail.put("IN_BUYER_EMAIL_ID",new Integer(new Long(buyerEmail.getBuyerEmailId()).toString()));
                    //orderBuyerEmail.put("IN_BUYER_ID",new Integer(new Long(buyerEmail.getBuyerId()).toString()));
                    orderBuyerEmail.put("IN_BUYER_EMAIL_ID",new Long(buyerEmail.getBuyerEmailId()));
                    orderBuyerEmail.put("IN_BUYER_ID",new Long(buyerEmail.getBuyerId()));
                    orderBuyerEmail.put("IN_EMAIL",buyerEmail.getEmail());
                    orderBuyerEmail.put("IN_PRIMARY",buyerEmail.getPrimary());
                    orderBuyerEmail.put("IN_NEWSLETTER",buyerEmail.getNewsletter());
                    //orderBuyerEmail.put(STATUS_PARAM,"");
                    //orderBuyerEmail.put(MESSAGE_PARAM,"");
  
                    dataRequest.setConnection(connection);
                    dataRequest.setStatementID(UPDATE_BUYER_EMAILS);
                    dataRequest.setInputParams(orderBuyerEmail);
  
                    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
 
                    Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
                    status = (String) outputs.get(STATUS_PARAM);
                    if(status.equals("N"))
                    {
                        message = (String) outputs.get(MESSAGE_PARAM);
                        returnStatus = false;
                        throw new Exception(message);
                    }
                    else
                    {
                        returnStatus = true;
                    }
                    
                //if the order contains a clean id then udpate the clean schema
                if(buyer.getCleanCustomerId() != null)
                {
                    logger.debug("Updating clean customer " + buyer.getCleanCustomerId());                    
                    cleanBuyer = new HashMap();
                    cleanBuyer.put("IN_BUYER_ID",new Long(buyer.getBuyerId()));
                    cleanBuyer.put("IN_CLEAN_BUYER_ID",buyer.getCleanCustomerId());
                    cleanBuyer.put("IN_ORDER_GUID",order.getGUID());   
      
                    dataRequest.setConnection(connection);
                    dataRequest.setStatementID("UPDATE_CUSTOMER_FROM_BUYER");
                    dataRequest.setInputParams(cleanBuyer);      

                    Map cleanOutputs = (Map) dataAccessUtil.execute(dataRequest);
      
                    status = (String) cleanOutputs.get(STATUS_PARAM);
                    if(status.equals("N"))
                    {
                        message = (String) cleanOutputs.get(MESSAGE_PARAM);
                        returnStatus = false;
                        throw new Exception(message);
                    }

                }                                        
                    
                }
                else
                {
                    returnStatus = true;
                }
            }
        }
        }finally{}
        return returnStatus;
    }

    /**
     * Insert a buyer information into the buyer table in scrub
     * @param order OrderVO Value object that contains the
     * whole order
     * @exception Exception if any errors are encountered
     * @return boolean returns a true if buyer information was 
     * successfully inserted into the buyer table, else it 
     * returns a false
     */
    private boolean insertBuyers(OrderVO order) throws Exception
    {
        // insert the order buyer information
        try {
        DataRequest dataRequest = new DataRequest();

        HashMap orderBuyer = null;
        Collection buyers = order.getBuyer();            
        BuyerVO buyer = null;
        Iterator iterator = null;
        long buyerId = 0;
                
        iterator = buyers.iterator();
                                
        for (int i = 0; iterator.hasNext(); i++) 
        {
            buyer = (BuyerVO)iterator.next();

            orderBuyer = new HashMap();

            orderBuyer.put("IN_CUSTOMER_ID", buyer.getCustomerId());
            orderBuyer.put("IN_LAST_NAME" , buyer.getLastName());
            orderBuyer.put("IN_FIRST_NAME" , buyer.getFirstName());
            orderBuyer.put("IN_AUTO_HOLD" , buyer.getAutoHold());
            orderBuyer.put("IN_BEST_CUSTOMER" , buyer.getBestCustomer());
            orderBuyer.put("IN_STATUS" , buyer.getStatus());
            //orderBuyer.put("RegisterOutParameterBuyerId", new Integer(0));
            //orderBuyer.put("STATUS_PARAM", "");
            //orderBuyer.put("MESSAGE_PARAM", "");
  
            dataRequest.setConnection(connection);
            dataRequest.setStatementID(INSERT_BUYERS);
            dataRequest.setInputParams(orderBuyer);
  
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
 
            Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
            status = (String) outputs.get(STATUS_PARAM);

            if(status.equals("N"))
            {
                message = (String) outputs.get(MESSAGE_PARAM);
                returnStatus = false;
                throw new Exception(message);
            }
            else
            {
                buyerId = Long.parseLong(outputs.get("RegisterOutParameterBuyerId").toString());
                order.setBuyerId(buyerId);
                returnStatus = true;
            }
            
        }//end for loop
        }//end try 
        finally
        {
          
        }
        return returnStatus;
    }


    /**
     * Insert buyer address information
     * @param order OrderVO value object that contains the 
     * whole order
     * @exception Exception if any errors are encountered
     * @return boolean returns a true if buyer address 
     * information was successfully inserted into the buyer_addresses
     * table, else it returns a false
     */
    private boolean insertBuyerAddresses(OrderVO order) throws Exception
    {
        // insert the buyer address information
        try{
        DataRequest dataRequest = new DataRequest();

        HashMap orderBuyerAddress = null;
        Collection buyers = order.getBuyer();            
        Collection buyerAddresses = null;
        BuyerVO buyer = null;
        BuyerAddressesVO buyerAddress = null;
        Iterator iterator = null;
        Iterator buyerAddressesIterator = null;
                        
        iterator = buyers.iterator();
                                
        for (int i = 0; iterator.hasNext(); i++) 
        {
            buyer = (BuyerVO)iterator.next();

            buyerAddresses = buyer.getBuyerAddresses();
            buyerAddressesIterator = buyerAddresses.iterator();

            for (int x = 0; buyerAddressesIterator.hasNext(); x++) 
            {
                buyerAddress = (BuyerAddressesVO)buyerAddressesIterator.next();
                    
                orderBuyerAddress = new HashMap();

                //orderBuyerAddress.put("IN_BUYER_ID",new Integer(new Long(order.getBuyerId()).toString()));
                orderBuyerAddress.put("IN_BUYER_ID",new Long(order.getBuyerId()));
                orderBuyerAddress.put("IN_ADDRESS_TYPE",buyerAddress.getAddressType());
                orderBuyerAddress.put("IN_ADDRESS_LINE_1",buyerAddress.getAddressLine1());
                orderBuyerAddress.put("IN_ADDRESS_LINE_2",buyerAddress.getAddressLine2());
                orderBuyerAddress.put("IN_CITY",buyerAddress.getCity());
                orderBuyerAddress.put("IN_STATE_PROVINCE",buyerAddress.getStateProv());
                orderBuyerAddress.put("IN_POSTAL_CODE",buyerAddress.getPostalCode());
                orderBuyerAddress.put("IN_COUNTRY",buyerAddress.getCountry());
                orderBuyerAddress.put("IN_COUNTY",buyerAddress.getCounty());
                orderBuyerAddress.put("IN_ADDRESS_ETC",buyerAddress.getAddressEtc());
                //orderBuyerAddress.put("RegisterOutParameterBuyerAddressId",new Integer(0));
                //orderBuyerAddress.put(STATUS_PARAM,"");
                //orderBuyerAddress.put(MESSAGE_PARAM,"");

                dataRequest.setConnection(connection);
                dataRequest.setStatementID(INSERT_BUYER_ADDRESSES);
                dataRequest.setInputParams(orderBuyerAddress);
  
  
                DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
 
                Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
                status = (String) outputs.get(STATUS_PARAM);

                if(status.equals("N"))
                {
                    message = (String) outputs.get(MESSAGE_PARAM);
                    returnStatus = false;
                    throw new Exception(message);
                }
                else
                {
                    returnStatus = true;
                }
            
            }//end inner for loop
        }//end outer for loop    
        }//end try 
        finally
        {
          
        }
        return returnStatus;
    }

    /**
     * Insert buyer comments into the scrub buyer_comments table
     * @param order OrderVO value object that contains the 
     * whole order
     * @exception Exception if any errors are encountered
     * @return boolean returns a true if buyer comment 
     * information was successfully inserted into the buyer_comments
     * table, else it returns a false
     */
    private boolean insertBuyerComments(OrderVO order) throws Exception
    {
        // insert the buyer comments information
        try{
        DataRequest dataRequest = new DataRequest();

        HashMap orderBuyerComment = null;
        Collection buyers = order.getBuyer();            
        Collection buyerComments = null;
        BuyerVO buyer = null;
        BuyerCommentsVO buyerComment = null;
        Iterator iterator = null;
        Iterator buyerCommentsIterator = null;
                
        iterator = buyers.iterator();
                                
        for (int i = 0; iterator.hasNext(); i++) 
        {
            buyer = (BuyerVO)iterator.next();

            buyerComments = buyer.getBuyerComments();
            if(buyerComments != null)
            {
              buyerCommentsIterator = buyerComments.iterator();

              for (int x = 0; buyerCommentsIterator.hasNext(); x++) 
              {
                  buyerComment = (BuyerCommentsVO)buyerCommentsIterator.next();
                    
                  orderBuyerComment = new HashMap();

                  //orderBuyerComment.put("IN_BUYER_ID",new Integer(new Long(order.getBuyerId()).toString()));
                  orderBuyerComment.put("IN_BUYER_ID",new Long(order.getBuyerId()));
                  orderBuyerComment.put("IN_TEXT",buyerComment.getText());
                  //orderBuyerComment.put("RegisterOutParameterBuyerCommentId",new Integer(0));
                  //orderBuyerComment.put(STATUS_PARAM,"");
                  //orderBuyerComment.put(MESSAGE_PARAM,"");
  
                  dataRequest.setConnection(connection);
                  dataRequest.setStatementID(INSERT_BUYER_COMMENTS);
                  dataRequest.setInputParams(orderBuyerComment);
  
                  DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
 
                  Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
                  status = (String) outputs.get(STATUS_PARAM);
                  if(status.equals("N"))
                  {
                      message = (String) outputs.get(MESSAGE_PARAM);
                      returnStatus = false;
                      throw new Exception(message);
                  }
                  else
                  {
                      returnStatus = true;
                  }
                }//end inner for loop
            }//end if
        }//end outer for loop
        }finally{}
        return returnStatus;
    }

    /**
     * Insert buyer phones into the scrub buyer_phones scrub table
     * @param order OrderVO value object that contains the 
     * whole order
     * @exception Exception if any errors are encountered
     * @return boolean returns a true if buyer phone 
     * information was successfully inserted into the buyer_phones
     * table, else it returns a false
     */
     private boolean insertBuyerPhones(OrderVO order) throws Exception
    {
        // insert the buyer phones information
        try{
        DataRequest dataRequest = new DataRequest();

        HashMap orderBuyerPhone = null;
        Collection buyers = order.getBuyer();            
        Collection buyerPhones = null;
        BuyerVO buyer = null;
        BuyerPhonesVO buyerPhone = null;
        Iterator iterator = null;
        Iterator buyerPhonesIterator = null;
        boolean foundWorkNumber = false;
                
        iterator = buyers.iterator();
                                
        for (int i = 0; iterator.hasNext(); i++) 
        {
            buyer = (BuyerVO)iterator.next();

            buyerPhones = buyer.getBuyerPhones();
            buyerPhonesIterator = buyerPhones.iterator();

            for (int x = 0; buyerPhonesIterator.hasNext(); x++) 
            {
                buyerPhone = (BuyerPhonesVO)buyerPhonesIterator.next();
                    
                orderBuyerPhone = new HashMap();

                //orderBuyerPhone.put("IN_BUYER_ID",new Integer(new Long(order.getBuyerId()).toString()));
                orderBuyerPhone.put("IN_BUYER_ID",new Long(order.getBuyerId()));
                orderBuyerPhone.put("IN_PHONE_TYPE",buyerPhone.getPhoneType());
                orderBuyerPhone.put("IN_PHONE_NUMBER",buyerPhone.getPhoneNumber());
                orderBuyerPhone.put("IN_EXTENSION",buyerPhone.getExtension());
                orderBuyerPhone.put("IN_PHONE_NUMBER_TYPE", buyerPhone.getPhoneNumberType());
                orderBuyerPhone.put("IN_SMS_OPT_IN", buyerPhone.getSmsOptIn());
                //orderBuyerPhone.put("RegisterOutParameterBuyerPhoneId",new Integer(0));
                //orderBuyerPhone.put(STATUS_PARAM,"");
                //orderBuyerPhone.put(MESSAGE_PARAM,"");
                dataRequest.setConnection(connection);
                dataRequest.setStatementID(INSERT_BUYER_PHONES);
                dataRequest.setInputParams(orderBuyerPhone);
  
                DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
 
                Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
                status = (String) outputs.get(STATUS_PARAM);
                if(status.equals("N"))
                {
                    message = (String) outputs.get(MESSAGE_PARAM);
                    returnStatus = false;
                    throw new Exception(message);
                }
                else
                {
                    returnStatus = true;
                }
                
                if (buyerPhone.getPhoneType() == PHONE_TYPE_WORK) 
                {
                    foundWorkNumber = true;
                }
            }
        }
        
        if (!foundWorkNumber && contactPhoneNumber != null) 
        {
            logger.debug("Adding contact phone " + contactPhoneNumber + " to BUYER_PHONES");
            
            orderBuyerPhone = new HashMap();
            
            orderBuyerPhone.put("IN_BUYER_ID",new Long(order.getBuyerId()));
            orderBuyerPhone.put("IN_PHONE_TYPE", PHONE_TYPE_WORK);
            orderBuyerPhone.put("IN_PHONE_NUMBER", contactPhoneNumber);
            orderBuyerPhone.put("IN_EXTENSION", contactPhoneExt);
            orderBuyerPhone.put("IN_PHONE_NUMBER_TYPE", null);
            orderBuyerPhone.put("IN_SMS_OPT_IN", "N");
  
            dataRequest.setConnection(connection);
            dataRequest.setStatementID(INSERT_BUYER_PHONES);
            dataRequest.setInputParams(orderBuyerPhone);
  
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
 
            Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
            status = (String) outputs.get(STATUS_PARAM);
            if(status.equals("N"))
            {
                message = (String) outputs.get(MESSAGE_PARAM);
                returnStatus = false;
                throw new Exception(message);
            }
            else
            {
                returnStatus = true;
            }
        }
        }finally{}
        return returnStatus;
    }
     
    /**
     * Insert buyer emails into the scrub buyer_emails scrub table
     * @param order OrderVO value object that contains the 
     * whole order
     * @exception Exception if any errors are encountered
     * @return boolean returns a true if buyer email 
     * information was successfully inserted into the buyer_emails
     * table, else it returns a false
     */
    private boolean insertBuyerEmails(OrderVO order) throws Exception
    {
        // insert the buyer emails information
        try{
        DataRequest dataRequest = new DataRequest();

        HashMap orderBuyerEmail = null;
        Collection buyers = order.getBuyer();            
        Collection buyerEmails = null;
        BuyerVO buyer = null;
        BuyerEmailsVO buyerEmail = null;
        Iterator iterator = null;
        Iterator buyerEmailsIterator = null;
        long buyerEmailId = 0;
                
        iterator = buyers.iterator();
                                
        for (int i = 0; iterator.hasNext(); i++) 
        {
            buyer = (BuyerVO)iterator.next();

            buyerEmails = buyer.getBuyerEmails();
            buyerEmailsIterator = buyerEmails.iterator();

            for (int x = 0; buyerEmailsIterator.hasNext(); x++) 
            {
                buyerEmail = (BuyerEmailsVO)buyerEmailsIterator.next();
                    
                orderBuyerEmail = new HashMap();

                //orderBuyerEmail.put("IN_BUYER_ID",new Integer(new Long(order.getBuyerId()).toString()));
                orderBuyerEmail.put("IN_BUYER_ID",new Long(order.getBuyerId()));
                orderBuyerEmail.put("IN_EMAIL",buyerEmail.getEmail());
                orderBuyerEmail.put("IN_PRIMARY",buyerEmail.getPrimary());
                orderBuyerEmail.put("IN_NEWSLETTER",buyerEmail.getNewsletter());
                //orderBuyerEmail.put("RegisterOutParameterBuyerPhoneId",new Integer(0));
                //orderBuyerEmail.put(STATUS_PARAM,"");
                //orderBuyerEmail.put(MESSAGE_PARAM,"");
  
                dataRequest.setConnection(connection);
                dataRequest.setStatementID(INSERT_BUYER_EMAILS);
                dataRequest.setInputParams(orderBuyerEmail);
  
                DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
 
                Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
                status = (String) outputs.get(STATUS_PARAM);
                if(status.equals("N"))
                {
                    message = (String) outputs.get(MESSAGE_PARAM);
                    returnStatus = false;
                    throw new Exception(message);
                }
                else
                {
                    buyerEmailId = Long.parseLong(outputs.get("RegisterOutParameterBuyerEmailId").toString());
                    order.setBuyerEmailId(buyerEmailId);
                    returnStatus = true;
                }
            }
        }
        }finally{}
        return returnStatus;
    }

    /**
     * Insert order header information  into the scrub orders
     * table
     * @param order OrderVO value object that contains the 
     * whole order
     * @exception Exception if any errors are encountered
     * @return boolean returns a true if order header 
     * information was successfully inserted into the orders
     * table, else it returns a false
     */
    private boolean insertOrderHeader(OrderVO order) throws Exception
    {
        // insert the order header information
        DataRequest dataRequest = new DataRequest();

        HashMap orderHeader = new HashMap();

        orderHeader.put("IN_GUID" , order.getGUID());
        orderHeader.put("IN_MASTER_ORDER_NUMBER" , order.getMasterOrderNumber());
        //orderHeader.put("IN_BUYER_ID", new Integer(new Long(order.getBuyerId()).toString()));
        orderHeader.put("IN_BUYER_ID", new Long(order.getBuyerId()));
        orderHeader.put("IN_ORDER_TOTAL" , order.getOrderTotal());
        orderHeader.put("IN_PRODUCTS_TOTAL" , order.getProductsTotal());
        orderHeader.put("IN_TAX_TOTAL" , order.getTaxTotal());
        orderHeader.put("IN_SERVICE_FEE_TOTAL" , order.getServiceFeeTotal());
        orderHeader.put("IN_SHIPPING_FEE_TOTAL" , order.getShippingFeeTotal());
        //orderHeader.put("IN_BUYER_EMAIL_ID" , new Integer(new Long(order.getBuyerEmailId()).toString()));
        orderHeader.put("IN_BUYER_EMAIL_ID" , new Long(order.getBuyerEmailId()));
        orderHeader.put("IN_ORDER_DATE" , order.getOrderDate());
        orderHeader.put("IN_ORDER_ORIGIN" , order.getOrderOrigin());
        //orderHeader.put("IN_MEMBERSHIP_ID" , new Integer(new Long(order.getMembershipId()).toString()));
        orderHeader.put("IN_MEMBERSHIP_ID" , new Long(order.getMembershipId()));
        orderHeader.put("IN_CSR_ID" , order.getCsrId());
        orderHeader.put("IN_ADD_ON_AMOUNT_TOTAL" , order.getAddOnAmountTotal());
        orderHeader.put("IN_PARTNERSHIP_BONUS_POINTS" , order.getPartnershipBonusPoints());
        orderHeader.put("IN_SOURCE_CODE" , order.getSourceCode());
        orderHeader.put("IN_CALL_TIME" , order.getCallTime());
        orderHeader.put("IN_DNIS_CODE" , order.getDnisCode());
        orderHeader.put("IN_DATAMART_UPDATE_DATE" , order.getDatamartUpdateDate());
        orderHeader.put("IN_DISCOUNT_TOTAL" , order.getDiscountTotal());
        orderHeader.put("IN_YELLOW_PAGES_CODE" , order.getYellowPagesCode());
        orderHeader.put("IN_ARIBA_BUYER_ASN_NUMBER" , order.getAribaBuyerAsnNumber());
        orderHeader.put("IN_ARIBA_BUYER_COOKIE" , order.getAribaBuyerCookie());
        orderHeader.put("IN_ARIBA_PAYLOAD" , order.getAribaPayload());
        orderHeader.put("IN_FRAUD" , order.getFraudFlag());
        orderHeader.put("IN_SOCKET_TIMESTAMP" , order.getSocketTimestamp());
        orderHeader.put("IN_STATUS" , order.getStatus());
        orderHeader.put("IN_CO_BRAND_CREDIT_CARD_CODE" , order.getCoBrandCreditCardCode());
        orderHeader.put("IN_LOSS_PREVENTION_INDICATOR" , order.getLossPreventionIndicator());
        orderHeader.put("IN_MP_REDEMPTION_RATE_AMT", order.getMpRedemptionRateAmt());
        orderHeader.put("IN_BUYER_SIGNED_IN_FLAG", order.getBuyerSignedIn());
        orderHeader.put("IN_CHARACTER_MAPPING_FLAG", order.getCharacterMappingFlag());
        //orderHeader.put(STATUS_PARAM,"");
        //orderHeader.put(MESSAGE_PARAM,"");
        orderHeader.put("IN_LANGUAGE_ID" , order.getLanguageId ());
        orderHeader.put("IN_JOINT_CART" , order.getJointCartIndicator());
        orderHeader.put("IN_ADD_ON_DISCOUNT_AMOUNT" , order.getAddOnDiscountAmount());
        orderHeader.put("IN_BUYER_HAS_FREE_SHIPPING" , order.getOeBuyerHasFreeShipping());
        
        dataRequest.setConnection(connection);
        dataRequest.setStatementID(INSERT_ORDER_HEADER);
        dataRequest.setInputParams(orderHeader);
  
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

        try
        {
            Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
            status = (String) outputs.get(STATUS_PARAM);
            if(status.equals("N"))
            {
                message = (String) outputs.get(MESSAGE_PARAM);
                throw new Exception(message);
            }
            else
            {
                returnStatus = true;
            }
        }
        finally
        {
            
        }
        return returnStatus;
    }

    /**
     * Insert order items information  into the scrub order_details
     * table
     * @param order OrderVO value object that contains the 
     * whole order
     * @exception Exception if any errors are encountered
     * @return boolean returns a true if order item 
     * information was successfully inserted into the order_details
     * table, else it returns a false
     */
    private boolean insertOrderItems(OrderVO order) throws Exception
    {
        // insert the order detail information
        try{
        DataRequest dataRequest = new DataRequest();

        HashMap orderDetail = null;
        Collection items = order.getOrderDetail();            
        OrderDetailsVO item = null;
        Iterator iterator = null;
        long orderDetailId = 0;
        
        Calendar cal = null;
        if (order.getOrderDate() != null)
        {
          cal = Calendar.getInstance();
          cal.setTime(order.getOrderDateTime());
        }
       
        iterator = items.iterator();
                                
        for (int i = 0; iterator.hasNext(); i++) 
        {
            item = (OrderDetailsVO)iterator.next();

            orderDetail = new HashMap();

            orderDetail.put("IN_GUID",order.getGUID());
            orderDetail.put("IN_LINE_NUMBER",item.getLineNumber());
            String sourceCode = item.getSourceCode();

            //orderDetail.put("IN_RECIPIENT_ID",new Integer(new Long(item.getRecipientId()).toString()));
            //orderDetail.put("IN_RECIPIENT_ADDRESS_ID",new Integer(new Long(item.getRecipientAddressId()).toString()));
            orderDetail.put("IN_RECIPIENT_ID", new Long(item.getRecipientId()));
            orderDetail.put("IN_RECIPIENT_ADDRESS_ID", new Long(item.getRecipientAddressId()));
            orderDetail.put("IN_PRODUCT_ID",item.getProductId());
            orderDetail.put("IN_QUANTITY",item.getQuantity());
            orderDetail.put("IN_SHIP_VIA",item.getShipVia());
            orderDetail.put("IN_SHIP_METHOD",item.getShipMethod());
            orderDetail.put("IN_SHIP_DATE",item.getShipDate());
            orderDetail.put("IN_DELIVERY_DATE",item.getDeliveryDate());
            orderDetail.put("IN_DELIVERY_DATE_RANGE_END",item.getDeliveryDateRangeEnd());
            orderDetail.put("IN_DROP_SHIP_TRACKING_NUMBER",item.getDropShipTrackingNumber());
            orderDetail.put("IN_PRODUCTS_AMOUNT",item.getProductsAmount());
            orderDetail.put("IN_TAX_AMOUNT",item.getTaxAmount());
            orderDetail.put("IN_SERVICE_FEE_AMOUNT",item.getServiceFeeAmount());
            orderDetail.put("IN_SHIPPING_FEE_AMOUNT",item.getShippingFeeAmount());
            orderDetail.put("IN_ADD_ON_AMOUNT",item.getAddOnAmount());
            orderDetail.put("IN_DISCOUNT_AMOUNT",item.getDiscountAmount());
            orderDetail.put("IN_OCCASION_ID",item.getOccassionId());
            orderDetail.put("IN_LAST_MINUTE_GIFT_SIGNATURE",item.getLastMinuteGiftSignature());
            orderDetail.put("IN_LAST_MINUTE_NUMBER",item.getLastMinuteNumber());
            orderDetail.put("IN_LAST_MINUTE_GIFT_EMAIL",item.getLastMinuteGiftEmail());
            orderDetail.put("IN_EXTERNAL_ORDER_NUMBER",item.getExternalOrderNumber());
            orderDetail.put("IN_COLOR_1",item.getColorFirstChoice());
            orderDetail.put("IN_COLOR_2",item.getColorSecondChoice());
            orderDetail.put("IN_SUBSTITUTE_ACKNOWLEDGEMENT",item.getSubstituteAcknowledgement());
            orderDetail.put("IN_CARD_MESSAGE",item.getCardMessage());
            orderDetail.put("IN_CARD_SIGNATURE",item.getCardSignature());
            orderDetail.put("IN_ORDER_COMMENTS",item.getOrderComments());
            orderDetail.put("IN_ORDER_CONTACT_INFORMATION",item.getOrderContactInformation());
            orderDetail.put("IN_FLORIST_NUMBER",item.getFloristNumber());
            orderDetail.put("IN_SPECIAL_INSTRUCTIONS",item.getSpecialInstructions());
            orderDetail.put("IN_ARIBA_UNSPSC_CODE",item.getAribaUnspscCode());
            orderDetail.put("IN_ARIBA_PO_NUMBER",item.getAribaPoNumber());
            orderDetail.put("IN_ARIBA_AMS_PROJECT_CODE",item.getAribaAmsProjectCode());
            orderDetail.put("IN_ARIBA_COST_CENTER",item.getAribaCostCenter());
            orderDetail.put("IN_EXTERNAL_ORDER_TOTAL",item.getExternalOrderTotal());
            orderDetail.put("SIZE_INDICATOR",item.getSizeChoice());
            orderDetail.put("IN_SENDER_INFO_RELEASE",item.getSenderInfoRelease());   
            orderDetail.put("IN_STATUS",item.getStatus());
            orderDetail.put("IN_MILES_POINTS",item.getMilesPoints());
            orderDetail.put("IN_SUBCODE",item.getProductSubCodeId());
            orderDetail.put("IN_QMS_RESULT_CODE",null);
            orderDetail.put("IN_PERSONAL_GREETING_ID",item.getPersonalGreetingId());
            /*
             * Item of the week
             */
            orderDetail.put("IN_SOURCE_CODE", item.getSourceCode());
            orderDetail.put("IN_ITEM_OF_THE_WEEK_FLAG", item.getItemOfTheWeekFlag());
            orderDetail.put("IN_REINSTATE_FLAG", item.getReinstateFlag());
            orderDetail.put("IN_COMMISSION",item.getCommission());
            orderDetail.put("IN_WHOLESALE",item.getWholesale());
            orderDetail.put("IN_TRANSACTION",item.getTransaction());
            orderDetail.put("IN_PDB_PRICE" , item.getPdbPrice());
            orderDetail.put("IN_SHIPPING_TAX",item.getShippingTax());
            orderDetail.put("IN_PRICE_OVERRIDE_FLAG",item.getPriceOverrideFlag());
            orderDetail.put("IN_DISCOUNTED_PRODUCT_PRICE",item.getDiscountedProductPrice());
            orderDetail.put("IN_DISCOUNT_TYPE",item.getDiscountType());
            orderDetail.put("IN_PERSONALIZATION", item.getPersonalizationData());
            orderDetail.put("IN_PARTNER_COST",item.getPartnerCost());
            orderDetail.put("IN_FIRST_ORDER_DOMESTIC", item.getDomesticFloristServiceCharge());
            orderDetail.put("IN_FIRST_ORDER_INTERNATIONAL", item.getInternationalFloristServiceCharge());
            orderDetail.put("IN_SHIPPING_COST", item.getShippingCost());
            orderDetail.put("IN_VENDOR_CHARGE", item.getVendorCharge());
            orderDetail.put("IN_VENDOR_SAT_UPCHARGE", item.getSaturdayUpcharge());
            orderDetail.put("IN_AK_HI_SPECIAL_SVC_CHARGE", item.getAlaskaHawaiiSurcharge());
            orderDetail.put("IN_BIN_SOURCE_CHANGED_FLAG", item.getBinSourceChangedFlag());

            orderDetail.put("IN_APPLY_SURCHARGE_CODE", item.getApplySurchargeCode());     
            orderDetail.put("IN_FUEL_SURCHARGE_AMT", item.getFuelSurcharge());     
            orderDetail.put("IN_SURCHARGE_DESCRIPTION", item.getFuelSurchargeDescription());     
            orderDetail.put("IN_DISPLAY_SURCHARGE", item.getDisplaySurcharge());     
            orderDetail.put("IN_SEND_SURCHARGE_TO_FLORIST", item.getSendSurchargeToFlorist());      
                         
             orderDetail.put("IN_AUTO_RENEW_FLAG", item.getAutoRenew());
             orderDetail.put("IN_FREE_SHIPPING_FLAG", item.getFreeShipping());
             orderDetail.put("IN_SHIPPING_FEE_AMOUNT_SAVED", stringToBigDecimal(item.getShippingFeeAmountSavings()));
             orderDetail.put("IN_SERVICE_FEE_AMOUNT_SAVED", stringToBigDecimal(item.getServiceFeeAmountSavings()));
             orderDetail.put("IN_SAME_DAY_UPCHARGE", stringToBigDecimal(item.getSameDayUpcharge()));
             orderDetail.put("IN_ORIGINAL_ORDER_HAS_SDU", item.getOriginalOrderHasSDU());
             orderDetail.put("IN_ORIGINAL_ORDER_HAS_SDUFS", item.getOriginalOrderHasSDUFS());
             orderDetail.put("IN_MORNING_DELIVERY_FEE", stringToBigDecimal(item.getMorningDeliveryFee()));
             orderDetail.put("IN_ORIGINAL_ORDER_HAS_MDF", item.getOriginalOrderHasMDF());
             orderDetail.put("IN_ORIG_EXTERNAL_ORDER_TOTAL", item.getExternalOrderTotal());
             orderDetail.put("IN_PC_GROUP_ID", item.getPcGroupId());
             orderDetail.put("IN_PC_MEMBERSHIP_ID", item.getPcMembershipId());
             orderDetail.put("IN_PC_FLAG", item.getPcFlag());
             
             logger.debug("before scrub mapper dao new stuff -- from insertOrderItems() method");
            
             List<TaxVO> taxList = new ArrayList<TaxVO>();
             taxList = item.getItemTaxVO().getTaxSplit();
             
             // FLYFLW orders or any order with old XML structure for Tax, should have default description and rate inserted.
             CalculateTaxUtil taxUtil = new CalculateTaxUtil();
             String defaultDesc = CalculateTaxUtil.DEFAULT_TAX_LABEL;            
             try {
            	 defaultDesc = taxUtil.getDefaultTaxLabel(CalculateTaxUtil.US_RECIPIENT, order.getCompanyId()); // Default label for US recipients
             } catch (Exception e) {
				logger.error("Error getting the default tax label, " + e.getMessage());				
			 }
             
             boolean isTaxPerformed = true;
             int count = 1;
				for (TaxVO taxVO : taxList) {

					if (!StringUtils.isEmpty(taxVO.getDescription()) && isTaxPerformed && TaxSplitOrder.fromValue(taxVO.getDescription()).value() > 0) {
						int splitOrder = TaxSplitOrder.fromValue(taxVO.getDescription()).value();
						logger.debug("Saving tax, " + taxVO.getName() + ", into bucket, " + splitOrder);
						if (splitOrder < 5) {
							orderDetail.put("IN_TAX" + splitOrder + "_NAME", taxVO.getName());
							orderDetail.put("IN_TAX" + splitOrder + "_AMOUNT", taxVO.getAmount());
							orderDetail.put("IN_TAX" + splitOrder + "_DESCRIPTION", taxVO.getDescription());
							orderDetail.put("IN_TAX" + splitOrder + "_RATE", taxVO.getRate());
						}
					} else {
						// Might be old order. once entered this block it should never enter first block; all the tax slit with new tax service will have description.
						isTaxPerformed = false; 
						orderDetail.put("IN_TAX" + count + "_NAME", taxVO.getName());
						orderDetail.put("IN_TAX" + count + "_AMOUNT", taxVO.getAmount());
						
						if(StringUtils.isEmpty(taxVO.getDescription())) {
							orderDetail.put("IN_TAX" + count + "_DESCRIPTION", defaultDesc);
						} else {
							orderDetail.put("IN_TAX" + count + "_DESCRIPTION", taxVO.getDescription());
						}
						
						BigDecimal taxRate = taxVO.getRate();
						if((taxVO.getRate() == null || taxVO.getRate().compareTo(BigDecimal.ZERO) == 0) 
								&& taxVO.getAmount() != null && taxVO.getAmount().compareTo(BigDecimal.ZERO) > 0) {
							try {
								taxRate = taxUtil.reverseCalcTaxRate(item, taxVO.getAmount());									
							} catch (Exception e) {
								logger.error("Error calculating the rate for item: " + item.getExternalOrderNumber() + ", Error: " + e.getMessage());
							}
						}							
						orderDetail.put("IN_TAX" + count + "_RATE", taxRate);							
						count += 1;
					}
					
				}
             
				for (int j = 1; j <= 5; j++) { 
					if(!orderDetail.containsKey("IN_TAX" + (j) +"_NAME")) {
						orderDetail.put("IN_TAX" + (j) +"_NAME", null);   	            		
	            		orderDetail.put("IN_TAX" + (j) +"_AMOUNT", null);   	            		
	            		orderDetail.put("IN_TAX" + (j) +"_DESCRIPTION", null);   	            		
	            		orderDetail.put("IN_TAX" + (j) +"_RATE", null);  
					}
				}
	            orderDetail.put("IN_TIME_OF_SERVICE", item.getTimeOfService());
				if (taxList.size() > 5) {
					String errorMessage = "Error occurred while processing Taxes on order:"
							+ item.getExternalOrderNumber() + ". Received " + taxList.size() + " tax nodes in Order XML (max is 5).  Please investiate.";
					logger.error(errorMessage);
					
					SystemMessengerVO systemMessageVO = new SystemMessengerVO();
					systemMessageVO.setLevel(SystemMessengerVO.LEVEL_PRODUCTION);
					systemMessageVO.setSource("SCRUB MAPPER DAO");
					systemMessageVO.setType("ERROR");
					systemMessageVO.setMessage(errorMessage);
					systemMessageVO.setSubject("TOO MANY TAXES INCLUDED IN ORDER");

					SystemMessenger sysMessenger = SystemMessenger.getInstance();
					sysMessenger.send(systemMessageVO, connection, false);
				}
             
             
			orderDetail.put("IN_ADD_ON_DISCOUNT_AMOUNT", item.getAddOnDiscountAmount());
             logger.debug("after scrub mapper dao new stuff -- from insertOrderItems() method");
             
            //orderDetail.put("RegisterOutParameterOrderDetailId",new Integer(0));
            //orderDetail.put(STATUS_PARAM,"");
            //orderDetail.put(MESSAGE_PARAM,"");
             
             orderDetail.put("IN_LATE_CUTOFF_FEE", stringToBigDecimal(item.getLatecutoffCharge()));
             orderDetail.put("IN_VEN_SUN_UPCHARGE", stringToBigDecimal(item.getSundayUpcharge())); 
             orderDetail.put("IN_VEN_MON_UPCHARGE", stringToBigDecimal(item.getMondayUpcharge()));
             orderDetail.put("IN_ORIGINAL_ORDER_HAS_LCF", item.getOriginalOrderHasLCF());   
             orderDetail.put("IN_LEGACY_ID", item.getLegacyId());
             
            dataRequest.setConnection(connection);
            dataRequest.setStatementID(INSERT_ORDER_DETAILS);
            dataRequest.setInputParams(orderDetail);
  
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
 
            Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
            status = (String) outputs.get(STATUS_PARAM);
            if(status.equals("N"))
            {
                message = (String) outputs.get(MESSAGE_PARAM);
                returnStatus = false;
                throw new Exception(message);
            }
            else
            {
                orderDetailId = Long.parseLong(outputs.get("RegisterOutParameterOrderDetailId").toString());
                item.setOrderDetailId(orderDetailId);
                returnStatus = true;
            }
        }
        }finally{}
        return returnStatus;
    }

    /**
     * Insert recipient information into the scrub recipients
     * table
     * @param order OrderVO value object that contains the 
     * whole order
     * @exception Exception if any errors are encountered
     * @return boolean returns a true if recipient 
     * information was successfully inserted into the recipients
     * table, else it returns a false
     */
    private boolean insertRecipients(OrderVO order) throws Exception
    {
        // insert the order recipients information
        try {
        DataRequest dataRequest = new DataRequest();

        HashMap orderRecipient = null;
        Collection items = order.getOrderDetail();            
        Collection recipients = null;
        OrderDetailsVO item = null;
        RecipientsVO recipient = null;
        Iterator iterator = null;
        Iterator recipientsIterator = null;
        long recipientId = 0;
                
        iterator = items.iterator();
                                
        for (int i = 0; iterator.hasNext(); i++) 
        {
            item = (OrderDetailsVO)iterator.next();

            recipients = item.getRecipients();
            recipientsIterator = recipients.iterator();

            for (int x = 0; recipientsIterator.hasNext(); x++) 
            {
                recipient = (RecipientsVO)recipientsIterator.next();
                    
                orderRecipient = new HashMap();

                orderRecipient.put("IN_LAST_NAME",recipient.getLastName());
                orderRecipient.put("IN_FIRST_NAME",recipient.getFirstName());
                orderRecipient.put("IN_LIST_CODE",recipient.getListCode());
                orderRecipient.put("IN_AUTO_HOLD",recipient.getAutoHold());
                orderRecipient.put("IN_MAIL_LIST_CODE",recipient.getMailListCode());
                orderRecipient.put("IN_STATUS",recipient.getStatus());
                orderRecipient.put("IN_CUSTOMER_ID",recipient.getCustomerId());
                //orderRecipient.put("RegisterOutParameterRecipientId",new Integer(0));
                //orderRecipient.put(STATUS_PARAM,"");
                //orderRecipient.put(MESSAGE_PARAM,"");
  
                dataRequest.setConnection(connection);
                dataRequest.setStatementID(INSERT_RECIPIENT);
                dataRequest.setInputParams(orderRecipient);
  
                DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
 
                Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
                status = (String) outputs.get(STATUS_PARAM);
                if(status.equals("N"))
                {
                    message = (String) outputs.get(MESSAGE_PARAM);
                    returnStatus = false;
                    throw new Exception(message);
                }
                else
                {
                    recipientId = Long.parseLong(outputs.get("RegisterOutParameterRecipientId").toString());
                    //add the recipientId to the RecipientsVO
                    recipient.setRecipientId(recipientId);
                    //add the recipientId to the OrderDetailVO
                    item.setRecipientId(recipientId);
                    returnStatus = true;
                }
            }
        }
        } finally { }
        return returnStatus;
    }

    
    /**
     * Insert recipient comments into the scrub recipients_comments
     * table
     * @param order OrderVO value object that contains the 
     * whole order
     * @exception Exception if any errors are encountered
     * @return boolean returns a true if recipient comments were
     * successfully inserted into the recipients_comments
     * table, else it returns a false
     */
    private boolean insertRecipientComments(OrderVO order) throws Exception
    {
        // insert the recipient comments information
        try{
        DataRequest dataRequest = new DataRequest();

        HashMap orderRecipientComment = null;
        Collection items = order.getOrderDetail();            
        Collection recipients = null;
        Collection recipientComments = null;
        OrderDetailsVO item = null;
        RecipientsVO recipient = null;
        RecipientCommentsVO recipientComment = null;
        Iterator iterator = null;
        Iterator recipientIterator = null;
        Iterator recipientCommentsIterator = null;
                
        iterator = items.iterator();
                                
        for (int i = 0; iterator.hasNext(); i++) 
        {
            item = (OrderDetailsVO)iterator.next();
            recipients = item.getRecipients();
            recipientIterator = recipients.iterator();
        
            for (int x = 0; recipientIterator.hasNext(); x++) 
            {
                recipient = (RecipientsVO)recipientIterator.next();
                recipientComments = recipient.getRecipientComments();
                if(recipientComments != null)
                {
                  recipientCommentsIterator = recipientComments.iterator();
                
                  for (int y = 0; recipientCommentsIterator.hasNext(); y++) 
                  {
                      recipientComment = (RecipientCommentsVO)recipientCommentsIterator.next();
                    
                      orderRecipientComment = new HashMap();

                      //orderRecipientComment.put("IN_RECIPIENT_ID",new Integer(new Long(recipient.getRecipientId()).toString()));
                      orderRecipientComment.put("IN_RECIPIENT_ID",new Long(recipient.getRecipientId()));
                      orderRecipientComment.put("IN_TEXT",recipientComment.getText());
                      //orderRecipientComment.put("RegisterOutParameterRecipientCommentId","");
                      //orderRecipientComment.put(STATUS_PARAM,"");
                      //orderRecipientComment.put(MESSAGE_PARAM,"");
  
                      dataRequest.setConnection(connection);
                      dataRequest.setStatementID(INSERT_RECIPIENT_COMMENTS);
                      dataRequest.setInputParams(orderRecipientComment);
  
                      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
 
                      Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
                      status = (String) outputs.get(STATUS_PARAM);
                      if(status.equals("N"))
                      {
                          message = (String) outputs.get(MESSAGE_PARAM);
                          returnStatus = false;
                          throw new Exception(message);
                      }
                      else
                      {
                          returnStatus = true;
                      }
                  }//end inner for loop
                }//end if
            }//end outer for loop
        }
        }finally{}
        return returnStatus;
    }

    /**
     * Insert recipient address information into the scrub 
     * recipient_addresses table
     * @param order OrderVO value object that contains the 
     * whole order
     * @exception Exception if any errors are encountered
     * @return boolean returns a true if recipient address
     * information was successfully inserted into the recipient_addresses
     * table, else it returns a false
     */
    private boolean insertRecipientAddresses(OrderVO order) throws Exception
    {
        // insert the recipient address information
        try{
        DataRequest dataRequest = new DataRequest();

        HashMap orderRecipientAddress = null;
        Collection items = order.getOrderDetail();            
        Collection recipients = null;
        Collection recipientAddresses = null;
        OrderDetailsVO item = null;
        RecipientsVO recipient = null;
        RecipientAddressesVO recipientAddress = null;
        Iterator iterator = null;
        Iterator recipientIterator = null;
        Iterator recipientAddressesIterator = null;
        long recipientAddressId = 0;
                
        iterator = items.iterator();
                                
        for (int i = 0; iterator.hasNext(); i++) 
        {
            item = (OrderDetailsVO)iterator.next();
            recipients = item.getRecipients();
            recipientIterator = recipients.iterator();
        
            for (int x = 0; recipientIterator.hasNext(); x++) 
            {
                recipient = (RecipientsVO)recipientIterator.next();
                
                recipientAddresses = recipient.getRecipientAddresses();
                recipientAddressesIterator = recipientAddresses.iterator();
                
                  for (int y = 0; recipientAddressesIterator.hasNext(); y++) 
                  {
                      recipientAddress = (RecipientAddressesVO)recipientAddressesIterator.next();
                      
                      logger.info("Insertint the recipient Addresses Line 1 : "+recipientAddress.getAddressLine1()+" Line 2 : "+recipientAddress.getAddressLine2());
                    
                      orderRecipientAddress = new HashMap();

                      //orderRecipientAddress.put("IN_RECIPIENT_ID",new Integer(new Long(recipient.getRecipientId()).toString()));
                      orderRecipientAddress.put("IN_RECIPIENT_ID", new Long(recipient.getRecipientId()) );
                      orderRecipientAddress.put("IN_ADDRESS_TYPE",recipientAddress.getAddressType());
                      orderRecipientAddress.put("IN_ADDRESS_LINE_1",recipientAddress.getAddressLine1());
                      orderRecipientAddress.put("IN_ADDRESS_LINE_2",recipientAddress.getAddressLine2());
                      orderRecipientAddress.put("IN_CITY",recipientAddress.getCity());
                      orderRecipientAddress.put("IN_STATE_PROVINCE",recipientAddress.getStateProvince());
                      orderRecipientAddress.put("IN_POSTAL_CODE",recipientAddress.getPostalCode());
                      orderRecipientAddress.put("IN_COUNTRY",recipientAddress.getCountry());
                      orderRecipientAddress.put("IN_COUNTY",recipientAddress.getCounty());
                      orderRecipientAddress.put("IN_INTERNATIONAL",recipientAddress.getInternational());
                      orderRecipientAddress.put("IN_DESTINATION_NAME",recipientAddress.getName());
                      orderRecipientAddress.put("IN_DESTINATION_INFO",recipientAddress.getInfo());
                      //orderRecipientAddress.put("RegisterOutParameterRecipientAddressId","");
                      //orderRecipientAddress.put(STATUS_PARAM,"");
                      //orderRecipientAddress.put(MESSAGE_PARAM,"");
                      logger.info("Scrub Mapper DAO: recipient address line 2" + recipientAddress.getAddressLine2());
                      dataRequest.setConnection(connection);
                      dataRequest.setStatementID(INSERT_RECIPIENT_ADDRESSES);
                      dataRequest.setInputParams(orderRecipientAddress);
  
                      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
 
                      Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
                      status = (String) outputs.get(STATUS_PARAM);
                      if(status.equals("N"))
                      {
                          message = (String) outputs.get(MESSAGE_PARAM);
                          returnStatus = false;
                          throw new Exception(message);
                      }
                      else
                      {
                          recipientAddressId = Long.parseLong(outputs.get("RegisterOutParameterRecipientAddressId").toString());
                          //add recipientAddressId to OrderDetailVO
                          item.setRecipientAddressId(recipientAddressId);
                          returnStatus = true;
                      }
                  }
            }   
        }
        }finally{}
        return returnStatus;
    }

    
    /**
     * Insert recipient phone information into the scrub 
     * recipient_phones table
     * @param order OrderVO value object that contains the 
     * whole order
     * @exception Exception if any errors are encountered
     * @return boolean returns a true if recipient phone
     * information was successfully inserted into the recipient_phones
     * table, else it returns a false
     */
    private boolean insertRecipientPhones(OrderVO order) throws Exception
    {
        // insert the recipient phones information
        try{
        DataRequest dataRequest = new DataRequest();

        HashMap orderRecipientPhone = null;
        Collection items = order.getOrderDetail();            
        Collection recipients = null;
        Collection recipientPhones = null;
        OrderDetailsVO item = null;
        RecipientsVO recipient = null;
        RecipientPhonesVO recipientPhone = null;
        Iterator iterator = null;
        Iterator recipientIterator = null;
        Iterator recipientPhonesIterator = null;
                
        iterator = items.iterator();
                                
        for (int i = 0; iterator.hasNext(); i++) 
        {
            item = (OrderDetailsVO)iterator.next();
            recipients = item.getRecipients();
            recipientIterator = recipients.iterator();
        
            for (int x = 0; recipientIterator.hasNext(); x++) 
            {
                recipient = (RecipientsVO)recipientIterator.next();
                recipientPhones = recipient.getRecipientPhones();
                recipientPhonesIterator = recipientPhones.iterator();
                
                  for (int y = 0; recipientPhonesIterator.hasNext(); y++) 
                  {
                      recipientPhone = (RecipientPhonesVO)recipientPhonesIterator.next();
                    
                      orderRecipientPhone = new HashMap();

                      //orderRecipientPhone.put("IN_RECIPIENT_ID",new Integer(new Long(recipient.getRecipientId()).toString()));
                      orderRecipientPhone.put("IN_RECIPIENT_ID",new Long(recipient.getRecipientId()));
                      orderRecipientPhone.put("IN_PHONE_TYPE",recipientPhone.getPhoneType());
                      orderRecipientPhone.put("IN_PHONE_NUMBER",recipientPhone.getPhoneNumber());
                      orderRecipientPhone.put("IN_EXTENSION",recipientPhone.getExtension());
                      //orderRecipientPhone.put("RegisterOutParameterRecipientPhoneId","");
                      //orderRecipientPhone.put(STATUS_PARAM,"");
                      //orderRecipientPhone.put(MESSAGE_PARAM,"");
  
                      dataRequest.setConnection(connection);
                      dataRequest.setStatementID(INSERT_RECIPIENT_PHONES);
                      dataRequest.setInputParams(orderRecipientPhone);
  
                      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
 
                      Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
                      status = (String) outputs.get(STATUS_PARAM);
                      if(status.equals("N"))
                      {
                          message = (String) outputs.get(MESSAGE_PARAM);
                          returnStatus = false;
                          throw new Exception(message);
                      }
                      else
                      {
                          returnStatus = true;
                      }
                  }
            }   
        }
        }finally{}
        return returnStatus;
    }

    /**
     * Insert AVS (address verification service) address information into the scrub avs_address table
     * 
     * @param order OrderVO value object that contains the whole order
     * @exception Exception if any errors are encountered
     * @return boolean returns a true if avs address information was successfully inserted into 
     * the avs tables else it returns a false
     */
    public boolean insertAVSAddress(OrderVO order) throws Exception
    {
        // insert the order AVS addresses information
        try {
        DataRequest dataRequest = new DataRequest();

        HashMap orderAVSAddress = null;
        Collection items = order.getOrderDetail();            
        AVSAddressVO avsAddress = null;
        Collection recipients = null;
        OrderDetailsVO item = null;
        RecipientsVO recipient = null;
        Iterator itemIterator = null;
        Iterator recipientIterator = null;
                
        itemIterator = items.iterator();

        for (int i = 0; itemIterator.hasNext(); i++) 
        {
            item = (OrderDetailsVO)itemIterator.next();
            recipients = item.getRecipients();
            recipientIterator = recipients.iterator();
        
            for (int x = 0; recipientIterator.hasNext(); x++) 
            {
                recipient = (RecipientsVO)recipientIterator.next();
                avsAddress = item.getAvsAddress();

                //check to see if there are any AVS addresses
                if(avsAddress != null)
                {                    
                      orderAVSAddress = new HashMap();

                      orderAVSAddress.put("IN_RECIPIENT_ID",new Long(recipient.getRecipientId()));
                      orderAVSAddress.put("IN_ADDRESS",avsAddress.getAddress());
                      orderAVSAddress.put("IN_CITY",avsAddress.getCity());
                      orderAVSAddress.put("IN_STATE",avsAddress.getStateProvince());
                      orderAVSAddress.put("IN_ZIP",avsAddress.getPostalCode());
                      orderAVSAddress.put("IN_COUNTRY",avsAddress.getCountry());
                      orderAVSAddress.put("IN_LATITUDE",avsAddress.getLatitude());
                      orderAVSAddress.put("IN_LONGITUDE",avsAddress.getLongitude());
                      orderAVSAddress.put("IN_ENTITY_TYPE",avsAddress.getEntityType());
                      orderAVSAddress.put("IN_OVERRIDE_FLAG",avsAddress.getOverrideflag());
                      orderAVSAddress.put("IN_RESULT",avsAddress.getResult());
                      orderAVSAddress.put("IN_AVS_PERFORMED",avsAddress.getAvsPerformed());
                      orderAVSAddress.put("IN_AVS_PERFORMED_ORIGIN", avsAddress.getAvsPerformedOrigin());
                      
                      dataRequest.setConnection(connection);
                      dataRequest.setStatementID(INSERT_AVS_ADDRESS);
                      dataRequest.setInputParams(orderAVSAddress);
                      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
 
                      Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
                      status = (String) outputs.get(STATUS_PARAM);
                      if(status.equals("N"))
                      {
                          message = (String) outputs.get(MESSAGE_PARAM);
                          returnStatus = false;
                          throw new Exception(message);
                      }
                      else
                      {
                          Long addressId = Long.parseLong(outputs.get("RegisterOutParameterAvsAddressId").toString());
                          avsAddress.setAvsAddressId(addressId);
                          item.setAvsAddress(avsAddress);
                          returnStatus = true;                    
                      }
                  }//end if
            }//end recipientIterator for loop    
        }//end iterator for loop
                                
        
        } finally { };
        return returnStatus;
    }

    /**
     * Insert AVS (address verification service) address score information into the scrub avs_address_score table
     * 
     * @param order OrderVO value object that contains the whole order
     * @exception Exception if any errors are encountered
     * @return boolean returns a true if avs address information was successfully inserted into 
     * the avs tables else it returns a false
     */
    public boolean insertAVSAddressScores(OrderVO order) throws Exception
    {
        // insert the order AVS addresses information
        try {
        DataRequest dataRequest = new DataRequest();

        HashMap avsAddressScore = null;
        Collection items = order.getOrderDetail();            
        AVSAddressVO avsAddress = null;
        Collection recipients = null;
        OrderDetailsVO item = null;
        RecipientsVO recipient = null;
        Iterator itemIterator = null;
        Iterator recipientIterator = null;
                
        itemIterator = items.iterator();

        for (int i = 0; itemIterator.hasNext(); i++) 
        {
            item = (OrderDetailsVO)itemIterator.next();
            recipients = item.getRecipients();
            recipientIterator = recipients.iterator();

            for (int x = 0; recipientIterator.hasNext(); x++) 
            {
                recipient = (RecipientsVO)recipientIterator.next();
                avsAddress = item.getAvsAddress();
                // default returnStatus in case there are no addresses or scores
                returnStatus = true;

                if(avsAddress == null || avsAddress.getScores() == null)
                {                    
                    break;
                }

                for (AVSAddressScoreVO score : avsAddress.getScores()) 
                {
                    avsAddressScore = new HashMap();

                    avsAddressScore.put("IN_ADDRESS_ID",new Long(avsAddress.getAvsAddressId()));
                    avsAddressScore.put("IN_REASON",score.getReason());
                    avsAddressScore.put("IN_SCORE",score.getScore());

                    dataRequest.setConnection(connection);
                    dataRequest.setStatementID(INSERT_AVS_ADDRESS_SCORE);
                    dataRequest.setInputParams(avsAddressScore);
                    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

                    Map outputs = (Map) dataAccessUtil.execute(dataRequest);

                    status = (String) outputs.get(STATUS_PARAM);
                    if(status.equals("N"))
                    {
                        message = (String) outputs.get(MESSAGE_PARAM);
                        returnStatus = false;
                        throw new Exception(message);
                    }
                    else
                    {
                        returnStatus = true;                    
                    }
                } // end of score loop
            }//end recipientIterator for loop    
        }//end iterator for loop
        } finally { };
        return returnStatus;
    }


    /**
     * Insert add ons information into the scrub 
     * add_ons table
     * @param order OrderVO value object that contains the 
     * whole order
     * @exception Exception if any errors are encountered
     * @return boolean returns a true if recipient add ons
     * information was successfully inserted into the add_ons
     * table, else it returns a false
     * 
     * emueller 12/17/03
     * Prior to inserting the addon, delete all addons associated with
     * this item.  
     */
    private boolean insertAddOns(OrderVO order) throws Exception
    {
        // insert the order add ons information
        try {
        DataRequest dataRequest = new DataRequest();
        
        String partnerName = null;
        
        CachedResultSet partnerCrs = getPartnerOriginsInfo(order.getOrderOrigin(), order.getSourceCode());
        if(partnerCrs!=null && partnerCrs.next()){
        	partnerName = partnerCrs.getString("PARTNER_NAME");
        }        
        
       

        HashMap orderAddOn = null;
        Collection items = order.getOrderDetail();
        Collection addOns = null;
        OrderDetailsVO item = null;
        AddOnsVO addOn = null;
        Iterator iterator = null;
        Iterator addOnsIterator = null;
                
        iterator = items.iterator();
                                
        for (int i = 0; iterator.hasNext(); i++) 
        {
            item = (OrderDetailsVO)iterator.next();
            
            addOns = item.getAddOns();
            //check to see if there are any add ons
            if(addOns != null)
            {
                
              addOnsIterator = addOns.iterator();
  
              //emueller, 12/17/03..delete all addons on this item
              deleteAddons(item.getOrderDetailId());
  
              for (int x = 0; addOnsIterator.hasNext(); x++) 
              {
                  addOn = (AddOnsVO)addOnsIterator.next();
                  
                  String thisAddOnId = addOn.getAddOnCode();
                  
                  long thisAddOnHistoryId ;
                  
                  if(partnerName!=null){
                	  thisAddOnHistoryId = getAddOnHisoryId(connection,thisAddOnId);
                  }
                  else{
                	  thisAddOnHistoryId = addOn.getAddOnHistoryId();
                  }
                  
                  logger.info("The AddonHistoryId got : "+thisAddOnHistoryId);     
                      
                  orderAddOn = new HashMap();
  
                  //orderAddOn.put("IN_ORDER_DETAIL_ID",new Integer(new Long(item.getOrderDetailId()).toString()));
                  orderAddOn.put("IN_ORDER_DETAIL_ID",new Long(item.getOrderDetailId()));
                  orderAddOn.put("IN_ADD_ON_CODE",addOn.getAddOnCode());
                  orderAddOn.put("IN_ADD_ON_QUANTITY",addOn.getAddOnQuantity());
                  orderAddOn.put("IN_ADD_ON_PRICE", addOn.getPrice());
                  orderAddOn.put("IN_ADD_ON_DISCOUNT_AMOUNT", addOn.getDiscountAmount());
                  orderAddOn.put("IN_ADD_ON_HISTORY_ID", new Long(thisAddOnHistoryId));
                  //orderAddOn.put("RegisterOutParameterAddOnId","");
                  //orderAddOn.put(STATUS_PARAM,"");
                  //orderAddOn.put(MESSAGE_PARAM,"");
    
                  dataRequest.setConnection(connection);
                  dataRequest.setStatementID(INSERT_ADD_ONS);
                  dataRequest.setInputParams(orderAddOn);
    
                  DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
   
                  Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
                  status = (String) outputs.get(STATUS_PARAM);
                  if(status.equals("N"))
                  {
                      message = (String) outputs.get(MESSAGE_PARAM);
                      returnStatus = false;
                      throw new Exception(message);
                  }
                  else
                  {
                      returnStatus = true;
                  }
              }
            }
        }
        } finally { }
        return returnStatus;
    }
    
   

  /** Delete all Addons for the given detail ID
   * @Stirng detail ID
   * @throws Exception
   * emueller 12/17/03
   */
   private boolean deleteAddons(long detailID) throws Exception
   {

        boolean returnStatus = false;
       
        DataRequest dataRequest = new DataRequest();
        Map deleteMap = new HashMap();
        deleteMap.put("IN_ORDER_DETAIL_ID",new Long(detailID));       

        dataRequest.setConnection(connection);
        dataRequest.setStatementID(DELETE_ADD_ONS);
        dataRequest.setInputParams(deleteMap);
  
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
 
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
        String status = (String) outputs.get(STATUS_PARAM);
        if(status.equals("N"))
        {
            message = (String) outputs.get(MESSAGE_PARAM);
            returnStatus = false;
            throw new Exception(message);
        }
        else
        {
            returnStatus = true;
        }

        return returnStatus;
   }
   /**
    * Update order extensions information in the scrub 
    * order_extensions table.  Note this actually 
    * results in deleting and re-inserting data for the order.
    * 
    * @param order OrderVO value object that contains the 
    *         whole order.
    * @exception Exception if any errors are encountered.
    * @return boolean returns a true if information
    *          was successfully inserted into the order_extensions
    *          table, else it returns a false.
    */
   private boolean updateOrderExtensions(OrderVO order) throws Exception 
   {
        return insertOrderExtensions(order, true);
   }

   /**
    * Insert order extensions information in the scrub 
    * order_extensions table.
    * 
    * @param order OrderVO value object that contains the 
    *         whole order.
    * @exception Exception if any errors are encountered.
    * @return boolean returns a true if information
    *          was successfully inserted into the order_extensions
    *          table, else it returns a false.
    */
   private boolean insertOrderExtensions(OrderVO order) throws Exception 
   {
        return insertOrderExtensions(order, false);
   }
    
   /**
    * Insert or update order extensions information into the scrub 
    * order_detail_extensions table.
    * 
    * @param order OrderVO value object that contains the 
    *         whole order.
    * @param deleteFirst boolean "true" if we should remove any existing
    *         extension info first, "false" otherwise.
    * @exception Exception if any errors are encountered.
    * @return boolean returns a true if information
    *          was successfully inserted into the order_extensions
    *          table, else it returns a false.
    */
   private boolean insertOrderExtensions(OrderVO order, boolean deleteFirst) throws Exception
   {
        DataRequest dataRequest = new DataRequest();       
        if (deleteFirst == true) {
          this.deleteOrderExtensions(order.getGUID());
        }
        List <OrderExtensionsVO> orderExtensionsVO = order.getOrderExtensions();    
        if (orderExtensionsVO != null)
        {
            for(OrderExtensionsVO orderExtensionVO : orderExtensionsVO)
            {
                HashMap extData = new HashMap();
    
                extData.put("IN_ORDER_GUID", order.getGUID());
                extData.put("IN_INFO_NAME", orderExtensionVO.getInfoName());
                extData.put("IN_INFO_VALUE", orderExtensionVO.getInfoValue());
    
    
                dataRequest.setConnection(connection);
                dataRequest.setStatementID(INSERT_ORDER_EXT);
                dataRequest.setInputParams(extData);
    
                DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    
                Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
                status = (String) outputs.get(STATUS_PARAM);
                if(status.equals("N"))
                {
                    message = (String) outputs.get(MESSAGE_PARAM);
                    throw new Exception(message);
                }
            }
        }
        return true;
    }


   /** Delete all Order Extensions for the given GUID
   */
   private boolean deleteOrderExtensions(String orderGUID) throws Exception
   {

        boolean returnStatus = false;
       
        DataRequest dataRequest = new DataRequest();
        Map deleteMap = new HashMap();
        deleteMap.put("IN_ORDER_GUID", orderGUID);       

        dataRequest.setConnection(connection);
        dataRequest.setStatementID(DELETE_ORDER_EXT);
        dataRequest.setInputParams(deleteMap);
    
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
        String status = (String) outputs.get(STATUS_PARAM);
        if(status.equals("N"))
        {
            message = (String) outputs.get(MESSAGE_PARAM);
            returnStatus = false;
            throw new Exception(message);
        }
        else
        {
            returnStatus = true;
        }

        return returnStatus;
   }  
   
    /**
     * Update order detail extensions information in the scrub 
     * order_detail_extensions table.  Note this actually 
     * results in deleting and re-inserting data for the order.
     * 
     * @param order OrderVO value object that contains the 
     *         whole order.
     * @exception Exception if any errors are encountered.
     * @return boolean returns a true if information
     *          was successfully inserted into the order_detail_extensions
     *          table, else it returns a false.
     */
    private boolean updateOrderDetailExtensions(OrderVO order) throws Exception {
        return insertOrderDetailExtensions(order, true);
    }

    /**
     * Insert order detail extensions information in the scrub 
     * order_detail_extensions table.
     * 
     * @param order OrderVO value object that contains the 
     *         whole order.
     * @exception Exception if any errors are encountered.
     * @return boolean returns a true if information
     *          was successfully inserted into the order_detail_extensions
     *          table, else it returns a false.
     */
    private boolean insertOrderDetailExtensions(OrderVO order) throws Exception {
        return insertOrderDetailExtensions(order, false);
    }
    
    /**
     * Insert or update order detail extensions information into the scrub 
     * order_detail_extensions table.
     * 
     * @param order OrderVO value object that contains the 
     *         whole order.
     * @param deleteFirst boolean "true" if we should remove any existing
     *         extension info first, "false" otherwise.
     * @exception Exception if any errors are encountered.
     * @return boolean returns a true if information
     *          was successfully inserted into the order_detail_extensions
     *          table, else it returns a false.
     */
    private boolean insertOrderDetailExtensions(OrderVO order, boolean deleteFirst) throws Exception
    {
        DataRequest dataRequest = new DataRequest();

        Collection items = order.getOrderDetail();            
        Collection extensions = null;
        OrderDetailsVO item = null;
        OrderDetailExtensionsVO ext = null;
        Iterator iterator = null;
        Iterator extIterator = null;
                
        iterator = items.iterator();
                                
        for (int i = 0; iterator.hasNext(); i++) 
        {
            item = (OrderDetailsVO)iterator.next();
            extensions = item.getOrderDetailExtensions();
            if (extensions == null) {
              continue;
            }
            extIterator = extensions.iterator();

            // Delete all extensions on this item
            if (deleteFirst == true) {
              deleteOrderDetailExtensions(item.getOrderDetailId());
            }

            // Now add extensions back
            for (int x = 0; extIterator.hasNext(); x++) 
            {
                ext = (OrderDetailExtensionsVO)extIterator.next();
                HashMap extData = new HashMap();

                extData.put("IN_ORDER_DETAIL_ID", new Long(item.getOrderDetailId()));
                extData.put("IN_INFO_NAME", ext.getInfoName());
                extData.put("IN_INFO_DATA", ext.getInfoData());
                //extData.put("RegisterOutParameterExtensionId","");
                //extData.put(STATUS_PARAM,"");
                //extData.put(MESSAGE_PARAM,"");
  
                dataRequest.setConnection(connection);
                dataRequest.setStatementID(INSERT_ORDER_DETAIL_EXT);
                dataRequest.setInputParams(extData);
  
                DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
 
                Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
                status = (String) outputs.get(STATUS_PARAM);
                if(status.equals("N"))
                {
                    message = (String) outputs.get(MESSAGE_PARAM);
                    returnStatus = false;
                    throw new Exception(message);
                }
                else
                {
                    returnStatus = true;
                }
            }
        }
        return returnStatus;
    }


  /** Delete all Order Detail Extensions for the given detail ID
   */
   private boolean deleteOrderDetailExtensions(long detailID) throws Exception
   {

        boolean returnStatus = false;
       
        DataRequest dataRequest = new DataRequest();
        Map deleteMap = new HashMap();
        deleteMap.put("IN_ORDER_DETAIL_ID",new Long(detailID));       

        dataRequest.setConnection(connection);
        dataRequest.setStatementID(DELETE_ORDER_DETAIL_EXT);
        dataRequest.setInputParams(deleteMap);
  
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
 
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
        String status = (String) outputs.get(STATUS_PARAM);
        if(status.equals("N"))
        {
            message = (String) outputs.get(MESSAGE_PARAM);
            returnStatus = false;
            throw new Exception(message);
        }
        else
        {
            returnStatus = true;
        }

        return returnStatus;
   }

   /**
     * Insert co brand information into the scrub 
     * co_brands table
     * @param order OrderVO value object that contains the 
     * whole order
     * @exception Exception if any errors are encountered
     * @return boolean returns a true if co brand
     * information was successfully inserted into the co_brands
     * table, else it returns a false
     */
    private boolean insertCoBrand(OrderVO order) throws Exception
    {
        // insert the order co brand information
        try{
        DataRequest dataRequest = new DataRequest();

        HashMap orderCoBrand = null;
        HashMap orderMembership = null;
        Collection coBrands = order.getCoBrand();            
        CoBrandVO coBrand = null;
        Iterator iterator = null;
                
        iterator = coBrands.iterator();
                                
        for (int i = 0; iterator.hasNext(); i++) 
        {
            coBrand = (CoBrandVO)iterator.next();

            orderCoBrand = new HashMap();

            orderCoBrand.put("IN_GUID",order.getGUID());
            orderCoBrand.put("IN_INFO_NAME",coBrand.getInfoName());
            orderCoBrand.put("IN_INFO_DATA",coBrand.getInfoData());
            //orderCoBrand.put("RegisterOutParameterCoBrandId","");
            //orderCoBrand.put(STATUS_PARAM,"");
            //orderCoBrand.put(MESSAGE_PARAM,"");
            logger.debug("Inserting Co-brand data - " + coBrand.getInfoName() + "/" + coBrand.getInfoData());
  
            dataRequest.setConnection(connection);
            dataRequest.setStatementID(INSERT_CO_BRAND);
            dataRequest.setInputParams(orderCoBrand);
                      
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
 
            Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
            status = (String) outputs.get(STATUS_PARAM);
            if(status.equals("N"))
            {
                message = (String) outputs.get(MESSAGE_PARAM);
                returnStatus = false;
                throw new Exception(message);
            }
            else
            {
                returnStatus = true;
            }
        }
        }finally{}
        return returnStatus;
    }

    /**
     * Insert membership information into the scrub 
     * memberships table
     * @param order OrderVO value object that contains the 
     * whole order
     * @exception Exception if any errors are encountered
     * @return boolean returns a true if membership
     * information was successfully inserted into the memberships
     * table, else it returns a false
     */
    private boolean insertMembership(OrderVO order) throws Exception
    {
        // insert the membership information
        try{
        DataRequest dataRequest = new DataRequest();

        HashMap orderCoBrand = null;
        HashMap orderMembership = null;
        Collection memberships = order.getMemberships();            
        MembershipsVO membership = null;
        Iterator iterator = null;
        long membershipId = 0;

        if(memberships != null)
        {
          iterator = memberships.iterator();
                                
          for (int i = 0; iterator.hasNext(); i++) 
          {
              membership = (MembershipsVO)iterator.next();

              orderMembership = new HashMap();

              //orderMembership.put("IN_BUYER_ID",new Integer(new Long(order.getBuyerId()).toString()));
              orderMembership.put("IN_BUYER_ID",new Long(order.getBuyerId()));
              orderMembership.put("IN_MEMBERSHIP_TYPE",membership.getMembershipType());
              orderMembership.put("IN_LAST_NAME",membership.getLastName());
              orderMembership.put("IN_FIRST_NAME",membership.getFirstName());
              orderMembership.put("IN_MEMBERSHP_ID_NUMBER",membership.getMembershipIdNumber());
              //orderMembership.put("RegisterOutParameterMembershipId","");
              //orderMembership.put(STATUS_PARAM,"");
              //orderMembership.put(MESSAGE_PARAM,"");
              logger.debug("Insert membership - " + membership.getMembershipType() + "/" + membership.getMembershipIdNumber());
  
              dataRequest.setConnection(connection);
              dataRequest.setStatementID(INSERT_MEMBERSHIP);
              dataRequest.setInputParams(orderMembership);
                      
              DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
 
              Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
              status = (String) outputs.get(STATUS_PARAM);
              if(status.equals("N"))
              {
                  message = (String) outputs.get(MESSAGE_PARAM);
                  returnStatus = false;
                  throw new Exception(message);
              }
              else
              {
                  membershipId = Long.parseLong(outputs.get("RegisterOutParameterMembershipId").toString());
                  order.setMembershipId(membershipId);
                  returnStatus = true;
              }
          }//end for
        }//end outer if
        }finally{}
        return returnStatus;
    }

    /**
     * Insert payment information into the scrub 
     * payments table
     * @param order OrderVO value object that contains the 
     * whole order
     * @exception Exception if any errors are encountered
     * @return boolean returns a true if payments
     * information was successfully inserted into the payments
     * table, else it returns a false
     */
    private boolean insertPayments(OrderVO order) throws Exception
    {
        // insert the order payments information
        try{
        DataRequest dataRequest = new DataRequest();

        HashMap orderPayment = null;
        Collection payments = order.getPayments();            
        PaymentsVO payment = null;
        Iterator iterator = null;
                
        iterator = payments.iterator();
                                
        for (int i = 0; iterator.hasNext(); i++) 
        {
            payment = (PaymentsVO)iterator.next();
            //Add check do only perform insert if payment id is zero.
            //If the id is zero then this is a new payment and insert should be done.
            //This is related to defect #439  (emueller, 3/5/04)
            if(payment.getPaymentId() <= 0)
            {             
                orderPayment = new HashMap();

                orderPayment.put("IN_GUID",order.getGUID());
                orderPayment.put("IN_PAYMENT_TYPE",payment.getPaymentType());
                orderPayment.put("IN_AMOUNT",payment.getAmount());
                //orderPayment.put("IN_CREDIT_CARD_ID",new Integer(new Long(payment.getCreditCardId()).toString()));
                orderPayment.put("IN_CREDIT_CARD_ID", new Long(payment.getCreditCardId()));
                orderPayment.put("IN_GIFT_CERTIFICATE_ID",payment.getGiftCertificateId());
                orderPayment.put("IN_AUTH_RESULT",payment.getAuthResult());
                orderPayment.put("IN_AUTH_NUMBER",payment.getAuthNumber());
                orderPayment.put("IN_AVS_CODE",payment.getAvsCode());
                orderPayment.put("IN_ACQ_REFERENCE_NUMBER",payment.getAcqReferenceNumber());
                orderPayment.put("IN_AAFES_TICKET",payment.getAafesTicket());
                orderPayment.put("IN_INVOICE_NUMBER",payment.getInvoiceNumber());
                orderPayment.put("IN_AP_ACCOUNT_TXT", payment.getApAccount());
                orderPayment.put("IN_AP_AUTH_TXT", payment.getApAuth());
                orderPayment.put("IN_ORIG_AUTH_AMOUNT_TXT", payment.getOrigAuthAmount());
                orderPayment.put("IN_NC_APPROVAL_IDENTITY_ID", payment.getNcApprovalId());
                orderPayment.put("IN_NC_TYPE_CODE", payment.getNcType());
                orderPayment.put("IN_NC_REASON_CODE", payment.getNcReason());
                orderPayment.put("IN_NC_ORDER_DETAIL_ID", payment.getNcOrderReference());
                orderPayment.put("IN_ORIG_MILES_POINTS_AMT", payment.getOrigMilesPointsAmt());
                orderPayment.put("IN_CSC_RESPONSE_CODE",payment.getCscResponseCode());
                orderPayment.put("IN_CSC_VALIDATED_FLAG",payment.getCscValidatedFlag());
                orderPayment.put("IN_CSC_FAILURE_CNT",new BigDecimal(payment.getCscFailureCount()));
                orderPayment.put("IN_WALLET_INDICATOR",payment.getWalletIndicator());
                orderPayment.put("IN_CC_AUTH_PROVIDER",payment.getCcAuthProvider());
                orderPayment.put("IN_CARDINAL_VERIFIED_FLAG", payment.getCardinalVerifiedFlag());
                orderPayment.put("IN_ROUTE", payment.getRoute());
                orderPayment.put("IN_TOKEN_ID", payment.getTokenId());
                orderPayment.put("IN_AUTH_TRANSACTION_ID", payment.getAuthTransactionId());
                
                String delimitedPaymentExtFields = null;
                if(payment.getPaymentExtMap()!=null && payment.getPaymentExtMap().size()>0){
                	Map<String,Object> paymentExtInfo = new HashMap(payment.getPaymentExtMap());
                	Map<String,Object> cardSpecificDetailsMap = (Map<String, Object>) paymentExtInfo.remove(PaymentExtensionConstants.CARD_SPECIFIC_DETAIL);
                	if(cardSpecificDetailsMap != null && cardSpecificDetailsMap.size()>0){
                		paymentExtInfo.putAll(cardSpecificDetailsMap);
                	}
                	delimitedPaymentExtFields = FieldUtils.getDelimitedStringFromMap(paymentExtInfo,"###","&&&");
                    logger.info("delimitedPaymentExtFields: " + delimitedPaymentExtFields);
                }
                orderPayment.put("IN_PAYMENT_EXT_INFO",delimitedPaymentExtFields);
                orderPayment.put("IN_TOKEN_ID", payment.getTokenId());
                //orderPayment.put("RegisterOutParameterPaymentId","");
                //orderPayment.put(STATUS_PARAM,"");
                //orderPayment.put(MESSAGE_PARAM,"");
  
                dataRequest.setConnection(connection);
                dataRequest.setStatementID(INSERT_PAYMENTS);
                dataRequest.setInputParams(orderPayment);
  
                DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
 
                Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
                status = (String) outputs.get(STATUS_PARAM);
                if(status.equals("N"))
                {
                    message = (String) outputs.get(MESSAGE_PARAM);
                    returnStatus = false;
                    throw new Exception(message);
                }
                else
                {
                    returnStatus = true;
                }
            }//end payment id = 0
        }
        }finally{}
        return returnStatus;
    }

	/**
     * Insert credit card information into the scrub 
     * credit_cards table
     * @param order OrderVO value object that contains the 
     * whole order
     * @exception Exception if any errors are encountered
     * @return boolean returns a true if credit card
     * information was successfully inserted into the credit_cards
     * table, else it returns a false
     */
    private boolean insertCreditCards(OrderVO order) throws Exception
    {
        // insert the order credit cards information
        try{
        DataRequest dataRequest = new DataRequest();

        HashMap orderCreditCard = null;
        Collection payments = order.getPayments();            
        Collection creditCards = null;
        PaymentsVO payment = null;
        PaymentsVO newPayment = null;
        CreditCardsVO creditCard = null;
        Iterator iterator = null;
        Iterator paymentsIterator = null;
        Iterator creditCardsIterator = null;
        List paymentsList = null;
        long creditCardId = 0;
                
        iterator = payments.iterator();
                                
        for (int i = 0; iterator.hasNext(); i++) 
        {
            payment = (PaymentsVO)iterator.next();

            creditCards = payment.getCreditCards();
            if(creditCards != null)
            {
              creditCardsIterator = creditCards.iterator();

              for (int x = 0; creditCardsIterator.hasNext(); x++) 
              {
                  creditCard = (CreditCardsVO)creditCardsIterator.next();
                    
                  orderCreditCard = new HashMap();

                  //orderCreditCard.put("IN_BUYER_ID", new Integer(new Long(order.getBuyerId()).toString()));
                  orderCreditCard.put("IN_BUYER_ID", new Long(order.getBuyerId()) );
                  orderCreditCard.put("IN_CC_TYPE",creditCard.getCCType());
                  orderCreditCard.put("IN_CC_NUMBER",creditCard.getCCNumber());
                  orderCreditCard.put("IN_CC_EXPIRATION",creditCard.getCCExpiration());
                  orderCreditCard.put("IN_PIN", creditCard.getPin());
                  orderCreditCard.put("IN_CC_ENCRYPTED", creditCard.getCcEncrypted());
                  //orderCreditCard.put("RegisterOutParameterCreditCardId","");
                  //orderCreditCard.put(STATUS_PARAM,"");
                  //orderCreditCard.put(MESSAGE_PARAM,"");
  
                  dataRequest.setConnection(connection);
                  dataRequest.setStatementID(INSERT_CREDIT_CARDS);
                  dataRequest.setInputParams(orderCreditCard);
  
                  DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
 
                  Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
                  status = (String) outputs.get(STATUS_PARAM);
                  if(status.equals("N"))
                  {
                      message = (String) outputs.get(MESSAGE_PARAM);
                      returnStatus = false;
                      throw new Exception(message);
                  }
                  else
                  {
                      creditCardId = Long.parseLong(outputs.get("RegisterOutParameterCreditCardId").toString());
                    
                      paymentsIterator = payments.iterator();
                      paymentsList = new ArrayList();
                      //loop through the payments
                      //if payment type equals credit card
                      //then set the credit card id in the PaymentVO
                      //to the creditCardId returned from the database
                      for (int j = 0; paymentsIterator.hasNext(); j++) 
                      {
                          newPayment = (PaymentsVO)paymentsIterator.next();
                          if(newPayment.getPaymentType() != null && !newPayment.getPaymentType().equals("IN") && 
                             !newPayment.getPaymentType().equals("GC") && !newPayment.getPaymentType().equals("NC") && newPayment.getPaymentType().equalsIgnoreCase(creditCard.getCCType()))
                          {
                            creditCards = newPayment.getCreditCards();
                            newPayment.setCreditCardId(creditCardId);
                            paymentsList.add(newPayment);
                          }
                          //********************************************
                          //Jeff, code below added.  Please test this out  ... Munter changed this to an elseif on 2-19
                          else if(newPayment.getPaymentType() == null && creditCard.getCCNumber() != null && !creditCard.getCCNumber().equals(""))  
                          {
                            creditCards = newPayment.getCreditCards();
                            newPayment.setCreditCardId(creditCardId);
                            paymentsList.add(newPayment);
                          }
                          //********************************************
                          else
                          {
                            paymentsList.add(newPayment);
                          }
                      }
                      order.setPayments(paymentsList);
                      returnStatus = true;
                  }//end else
              }//end inner for loop
            }//end if
        }//end outer for loop
        }finally{}
        return returnStatus;
    }

    /**
     * Insert order contact information into the order_contact_info
     * table in scrub
     * @param order OrderVO Value object that contains the
     * whole order
     * @exception Exception if any errors are encountered
     * @return boolean returns a true if contact information was 
     * successfully inserted into the order_contact_info table, else it 
     * returns a false
     */
    private boolean insertOrderContactInfo(OrderVO order) throws Exception
    {
        // insert the order contact information
        try {
        DataRequest dataRequest = new DataRequest();

        HashMap orderContactInfo = null;
        Collection contactInfo = order.getOrderContactInfo();
        OrderContactInfoVO orderContact = null;
        Iterator iterator = null;
        long orderContactInfoId = 0;

        if(contactInfo != null)
        {
                
        iterator = contactInfo.iterator();
                                
        for (int i = 0; iterator.hasNext(); i++) 
        {
            orderContact = (OrderContactInfoVO)iterator.next();

            orderContactInfo = new HashMap();

            orderContactInfo.put("IN_ORDER_GUID", order.getGUID());
            orderContactInfo.put("IN_FIRST_NAME", orderContact.getFirstName());
            orderContactInfo.put("IN_LAST_NAME", orderContact.getLastName());
            orderContactInfo.put("IN_PHONE", orderContact.getPhone());
            orderContactInfo.put("IN_EXT", orderContact.getExt());
            orderContactInfo.put("IN_EMAIL", orderContact.getEmail());
            //orderContactInfo.put("RegisterOutParameterOrderContactInfoId", new Integer(0));
            //orderContactInfo.put("STATUS_PARAM", "");
            //orderContactInfo.put("MESSAGE_PARAM", "");
  
            dataRequest.setConnection(connection);
            dataRequest.setStatementID(INSERT_ORDER_CONTACT_INFO);
            dataRequest.setInputParams(orderContactInfo);
  
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
 
            Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
            status = (String) outputs.get(STATUS_PARAM);

            if(status.equals("N"))
            {
                message = (String) outputs.get(MESSAGE_PARAM);
                returnStatus = false;
                throw new Exception(message);
            }
            else
            {
                returnStatus = true;
            }
            
            // tschmig 01/10/06 Save number and ext for use by BUYER_PHONES
            contactPhoneNumber = orderContact.getPhone();
            contactPhoneExt = orderContact.getExt();
                     
        }//end for loop
        }//end if
        }//end try 
        finally
        {
          
        }
        return returnStatus;
    }

    private boolean updateDispositions(OrderVO order) throws Exception
    {
        // update the order disposition infomration
        DataRequest dataRequest = new DataRequest();

        HashMap orderDispositionMap = null;
        Collection items = order.getOrderDetail();            
        Collection dispositions = null;
        OrderDetailsVO item = null;
        DispositionsVO disposition = null;
        Iterator iterator = null;
        Iterator dispositionIterator = null;

        //only continue if there is something to update
        if(items == null) return true;
        
        iterator = items.iterator();
        long detailId = 0;
                                
        for (int i = 0; iterator.hasNext(); i++) 
        {
            item = (OrderDetailsVO)iterator.next();

            dispositions = item.getDispositions();

            //only continue if there is something to update
            if(dispositions == null) return true;
            
            dispositionIterator = dispositions.iterator();
            detailId = item.getOrderDetailId();

            for (int x = 0; dispositionIterator.hasNext(); x++) 
            {
                disposition = (DispositionsVO)dispositionIterator.next();
                disposition.setOrderDetailID(detailId);

                //only insert if id is <= 0 ... a new disposition
                if(disposition.getOrderDispositionID() <= 0 ){

                if(disposition.isChanged()){
                    
                  orderDispositionMap = new HashMap();
                  //orderDispositionMap.put("IN_ORDER_DETAIL_ID", new Long(detailId).toString());
                  orderDispositionMap.put("IN_ORDER_DETAIL_ID", String.valueOf(detailId));
                  orderDispositionMap.put("IN_DISPOSITION_ID",disposition.getDispositionID());
                  orderDispositionMap.put("IN_COMMENTS",disposition.getComments());
                  orderDispositionMap.put("IN_CALLED_CUSTOMER_FLAG",disposition.getCalledCustomerFlag());
                  orderDispositionMap.put("IN_SENT_EMAIL_FLAG",disposition.getSentEmailFlag());
                  orderDispositionMap.put("IN_STOCK_MESSAGE_ID",disposition.getStockMessageID());                
                  orderDispositionMap.put("IN_EMAIL_SUBJECT",disposition.getEmailSubject());                
                  orderDispositionMap.put("IN_EMAIL_MESSAGE",disposition.getEmailMessage());  
                  orderDispositionMap.put("IN_CSR_ID",order.getCsrId());
                  orderDispositionMap.put("IO_ORDER_DISPOSITION_ID",new Integer(java.sql.Types.INTEGER));                                
                  //orderDispositionMap.put(STATUS_PARAM,"");
                  //orderDispositionMap.put(MESSAGE_PARAM,"");                

                  dataRequest.setStatementID(INSERT_ORDER_DISPOSITION);  
                  dataRequest.setConnection(connection);
                  dataRequest.setInputParams(orderDispositionMap);
  
                  DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
 
                  Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
                  status = (String) outputs.get(STATUS_PARAM);
                  if(status.equals("N"))
                  {
                      message = (String) outputs.get(MESSAGE_PARAM);
                      returnStatus = false;
                      throw new Exception(message);
                  }
                  else
                  {
                        BigDecimal id = (BigDecimal) outputs.get("IO_ORDER_DISPOSITION_ID");
                        disposition.setOrderDispositionID(id.longValue());

                        returnStatus = true;
                  }
                }
              }
            }
        }
        
        return returnStatus;
    }

    private boolean updateFraudComments(OrderVO order) throws Exception
    {
        // update the fraud comments information

        DataRequest dataRequest = new DataRequest();

        HashMap orderCommentMap = null;
        Collection fraudComments = order.getFraudComments();
        FraudCommentsVO fraudComment = null;
        Iterator fraudCommentsIterator = null;

        //only continue if there is something to update
        if(fraudComments == null) return true;
                
            fraudCommentsIterator = fraudComments.iterator();

            for (int i = 0; fraudCommentsIterator.hasNext(); i++) 
            {
                fraudComment = (FraudCommentsVO)fraudCommentsIterator.next();
                    
                //only insert if comment id is <= 0 ... a new comment
                if(fraudComment.getFraudCommentID() <= 0 ){

                if(fraudComment.isChanged()){

                  orderCommentMap = new HashMap();
                  orderCommentMap.put("IN_ORDER_GUID" , order.getGUID());
                  orderCommentMap.put("IN_FRAUD_ID",fraudComment.getFraudID());
                  orderCommentMap.put("IN_COMMENT_TEXT",fraudComment.getCommentText());
                  orderCommentMap.put("IO_FRAUD_COMMENT_ID",new Integer(java.sql.Types.INTEGER));
                  //orderCommentMap.put(STATUS_PARAM,"");
                  //orderCommentMap.put(MESSAGE_PARAM,"");
  
                  dataRequest.setConnection(connection);
                  dataRequest.setStatementID(INSERT_FRAUD_COMMENTS);
                  dataRequest.setInputParams(orderCommentMap);
  
                  DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
 
                  Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
                  status = (String) outputs.get(STATUS_PARAM);
                  if(status.equals("N"))
                  {
                      message = (String) outputs.get(MESSAGE_PARAM);
                      returnStatus = false;
                      throw new Exception(message);
                  }
                  else
                  {
                      //set comment id
                      BigDecimal commentId = (BigDecimal) outputs.get("IO_FRAUD_COMMENT_ID");
                      fraudComment.setFraudCommentID(commentId.longValue());
                
                      returnStatus = true;
                  }
                } //end is changed
            }//end if id > 0
        }
        return returnStatus;
    }


    /* Insert Fraud Codes into DB */
    private boolean insertFraudCodes(OrderVO order) throws Exception
    {
        // update the fraud codes information

        DataRequest dataRequest = new DataRequest();

        List fraudCodes = order.getFraudCodes();
        String fraudCode = null;
        Map paramMap = null;
        Iterator fraudCodeIterator = null;

        //only continue if there is something to update
        if(fraudCodes == null) return true;
                
            fraudCodeIterator = fraudCodes.iterator();

            for (int i = 0; fraudCodeIterator.hasNext(); i++) 
            {
                fraudCode = (String)fraudCodeIterator.next();
                    
                //only insert if comment id is not null
                if(fraudCode != null){

                  paramMap = new HashMap();
                  paramMap.put("IN_ORDER_GUID" , order.getGUID());
                  paramMap.put("IN_NOVATOR_FRAUD_CODE",fraudCode);
                  //paramMap.put(STATUS_PARAM,"");
                  //paramMap.put(MESSAGE_PARAM,"");
  
                  dataRequest.setConnection(connection);
                  dataRequest.setStatementID(INSERT_FRAUD_CODES);
                  dataRequest.setInputParams(paramMap);
  
                  DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
 
                  Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
                  status = (String) outputs.get(STATUS_PARAM);
                  if(status.equals("N"))
                  {
                      message = (String) outputs.get(MESSAGE_PARAM);
                      returnStatus = false;
                      throw new Exception(message);
                  }
                  else
                  {
               
                      returnStatus = true;
                  }

            }//end if not null
        }
        return returnStatus;
    }

    /**
      * Get source record for the source code.  If record is not
      * null, then check to see if there is a partner_id
      * for that source_code in the source record.  If there
      * is, then the information that was passed over in the
      * coBrand object needs to be inserted into the memberships
      * table, else just store that information in the co_brand
      * table.
      * @param sourceCode String contains the source code
      * that is set in the Order value object 
      * whole order
      * @exception Exception if any errors are encountered
      * @return String partnerId 
      */
     private String getSourceCodeRecord(String sourceCode) throws Exception
     {
        // Get the source record information
        String partnerId = "";
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID(GET_SOURCE_CODE_RECORD);
        dataRequest.addInputParam("IN_SOURCE_CODE", sourceCode);

        Map orderMap = null;
      
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        CachedResultSet orderResults = (CachedResultSet) DataAccessUtil.getInstance().execute(dataRequest);
        while(orderResults != null && orderResults.next())
        {
          partnerId = ((String)orderResults.getObject(9));
        }    

        return partnerId;
     }

     /**
      * Check the source code to see if there is a partnerId
      * associated with it.  If there is, pull out the partnerId.
      * Loop through the CoBrand vo and check to see if the following
      * parameters are in there LNAME, FNAME, and whatever value
      * that comes back in the partnerId.  Populate the MembershipsVO
      * with this information and populate all the other information 
      * in a new CoBrand value object.  
      * @param order OrderVO
      * @exception Exception 
      * @return 
      */
     private boolean separateCoBrandMembershipInfo(OrderVO order) throws Exception
     {
        CoBrandVO coBrand = null;
        CoBrandVO newCoBrand = null;
        List newCoBrands = null;
        MembershipsVO membership = null;

        // insert the order co brand information
        try{
        DataRequest dataRequest = new DataRequest();
        Collection coBrands = order.getCoBrand(); 
        Iterator iterator = null;
        String partnerId = "";

        partnerId = this.getSourceCodeRecord(order.getSourceCode());
        logger.debug("partnerId = " + partnerId);
        
        if(partnerId != null)
        {
          iterator = coBrands.iterator();
          membership = new MembershipsVO();
          
          newCoBrands = new ArrayList();
                                
          for (int i = 0; iterator.hasNext(); i++) 
          {
              coBrand = (CoBrandVO)iterator.next();
              newCoBrand = new CoBrandVO();
              
              logger.debug("coBrand Name = " + coBrand.getInfoName());
              logger.debug("coBrand Data = " + coBrand.getInfoData());
              
              if( coBrand.getInfoName().equalsIgnoreCase("FNAME") ) {
                membership.setFirstName(coBrand.getInfoData());
                logger.debug("Set membership first name to " + membership.getFirstName());
              }
              if( coBrand.getInfoName().equalsIgnoreCase("LNAME") ) {
                membership.setLastName(coBrand.getInfoData());
                logger.debug("Set membership last name to " + membership.getLastName());
              }
              if( coBrand.getInfoName().equalsIgnoreCase(partnerId) )
              {
                membership.setMembershipType(partnerId);
                membership.setMembershipIdNumber(coBrand.getInfoData());
                logger.debug("Set membership type to " + membership.getMembershipType());
                logger.debug("Set membership Id to " + membership.getMembershipIdNumber());
              }
              else if( !coBrand.getInfoName().equalsIgnoreCase("FNAME") &&
                       !coBrand.getInfoName().equalsIgnoreCase("LNAME") &&
                       !coBrand.getInfoName().equalsIgnoreCase(partnerId) )         
              {
                newCoBrand.setInfoData(coBrand.getInfoData());
                newCoBrand.setInfoName(coBrand.getInfoName());
                logger.debug("Set CoBrand Name to " + newCoBrand.getInfoName());
                logger.debug("Set CoBrand Data to " + newCoBrand.getInfoData());
                newCoBrands.add(newCoBrand);
              }
           }//end for loop
           membership.setBuyerId(order.getBuyerId());
           //add Membership info to the order object
           List members = new ArrayList();
           members.add(membership);
           order.setMemberships(members);
           //add coBrand info
           order.setCoBrand(newCoBrands);
        }//end outer if
        }//end try
        finally{}
        return returnStatus;
        
     }


     /**
      * Loops through the Buyer Email Collection and finds the email that maps passed in email Id
      * @param buyerEmailId The ID of the Buyers Email Record
      * @param buyers The collection of buyers for the order
      * @return Returns the email address string, or <code>null</code> if there is no email address
      */
     private String getBuyerEmailAddress(long buyerEmailId, long buyerId, Collection<BuyerEmailsVO> buyerEmails)
     {
        if(buyerEmails == null)
        {
          if(logger.isDebugEnabled())
          {
            logger.debug("Cannot retrieve Buyer Email Address as buyer email collections is null. buyerEmailId=" + buyerEmailId);
          }
          
          return null;
        }
        
        for(BuyerEmailsVO buyerVO: buyerEmails)
        {
          if(buyerVO.getBuyerEmailId() == buyerEmailId && buyerVO.getBuyerId() == buyerId)
          {
            if(logger.isDebugEnabled())
            {
              logger.debug("Returning Buyer Email Address: " + buyerVO.getEmail());
            }
            
            return buyerVO.getEmail();
          }
        }          
  
        if(logger.isDebugEnabled())
        {
          logger.debug("No Buyer Email Address in email collection with buyerEmailId=" + buyerEmailId);
        }
          
        return null;
     }

     
/*
    public static void main(String[] args)
    {
        try
        {
            Connection conn = null;
            String driver_ = "oracle.jdbc.driver.OracleDriver";
            String database_ = "jdbc:oracle:thin:@stheno-dev.ftdi.com:1521:DEV2";
            String user_ = "osp";
            String password_ = "osp";

            Class.forName(driver_);
            conn = DriverManager.getConnection(database_, user_, password_);

            ScrubMapperDAO scrubMapperDAO = new ScrubMapperDAO(conn);

            String guid = "FTD_GUID_16722488150528204356011672138503594498380-336385990015106057400206535018601332987122139520147001730886030-36990305301384485611250-177540226922-5102120183-182374104696-810401088246";
            OrderVO order = new OrderVO();


            //scrubMapperDAO.mapOrderToDB(order);
            order = scrubMapperDAO.mapOrderFromDB(guid);
            System.out.println(order.toString());

            order.setCoBrandCreditCardCode("Y");
            scrubMapperDAO.updateOrder(order);

            System.out.println(order.toString());
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }
*/    

  /**
   * Formats a numeric string as a BigDecimal. Blank or null strings are returned as null,
   * @param val
   * @return
   */
  private BigDecimal stringToBigDecimal(String val)
  {
    if(val == null || val.length() == 0)
    {
      return null;
    }
    
    return new BigDecimal(val);  
  }

  /**
   * Returns the String value for the Big Decimal or null of the value is null
   * @param val
   * @return
   */
  private String bigDecimalToString(BigDecimal val)
  {
    if(val == null)
    {
      return null;
    }
    
    return val.toPlainString();
  }
   
	/** #17863 - get Partner Origins Info from partner_mapping table.
	 * @param partnerOriginId
	 * @param sourceCode
	 * @return
	 */
	public CachedResultSet getPartnerOriginsInfo(String partnerOriginId, String sourceCode) {
		DataRequest dataRequest = new DataRequest();
		CachedResultSet crs = null;
		try {
			HashMap<String, String> inputParams = new HashMap<String, String>();
			inputParams.put("IN_PARTNER_ORIGIN_ID", partnerOriginId);
			inputParams.put("IN_SOURCE_CODE", sourceCode);
			dataRequest.setInputParams(inputParams);
			dataRequest.setConnection(this.connection);
			dataRequest.setStatementID("GET_PARTNER_ORIGIN_INFO");
			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
			Map result = (Map) dataAccessUtil.execute(dataRequest);
			if (result != null) {
				String status = (String) result.get("OUT_STATUS");
				if (!"Y".equalsIgnoreCase(status)) {
					String message = (String) result.get("OUT_MESSAGE");
					throw new Exception(message);
				}
				crs = (CachedResultSet) result.get("OUT_CUR");
			}
		} catch (Exception e) {
			logger.error("Error caught obtaining partner origins information: " + e.getMessage());
		}
		return crs;
	}
	
	 public int getTimeInRecState(String stateId, int time) throws Exception
	 {

	  try {
	   DataRequest dataRequest = new DataRequest();
	      dataRequest.setConnection(this.connection);
	      HashMap inputParams = new HashMap();
	      inputParams.put("IN_STATE_ID", stateId);
	      inputParams.put("IN_TIME", BigDecimal.valueOf(time));
	      dataRequest.setStatementID("GET_TIME_IN_REC_STATE");
	      dataRequest.setInputParams(inputParams);
	      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
	      status = (String) dataAccessUtil.execute(dataRequest);

	  } catch (Exception e){
	   logger.error("Error caught getting the time in CST" + e.getMessage());
	   return time;
	  }
	     return Integer.valueOf(status);
	 }
	 
	 public CachedResultSet getSourceCodeById(Connection conn, String sourceCode) throws Exception {
	        logger.debug("getSourceCodeById(" + sourceCode + ")");

	        /* build DataRequest object */
	        DataRequest dataRequest = new DataRequest();
	        dataRequest.setConnection(conn);
	        dataRequest.setStatementID("GET_SOURCE_CODE_BY_ID");
	        Map inputParams = new HashMap();
	        inputParams.put("IN_SOURCE_CODE", sourceCode);
	        dataRequest.setInputParams(inputParams);

	        /* execute the stored procedure */
	        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
	        CachedResultSet cr = (CachedResultSet) dataAccessUtil.execute(dataRequest);

	        return cr;
	    }
	 
	 /**Gets the latest addon update from the addon_history table
	     * @String addon Id
	     * @throws Exception
	     **/
	    private long getAddOnHisoryId(Connection conn,String addOnId) throws Exception{
	    	
	    	 long addOnHistoryId=0;
	    	 DataRequest dataRequest = new DataRequest();
		     dataRequest.setConnection(conn);
		     dataRequest.setStatementID("GET_ADDON_HISTORY_ID");
	         Map addOnMap = new HashMap();
	         addOnMap.put("IN_ADD_ON_ID",addOnId);       
 	         dataRequest.setInputParams(addOnMap);
	  
	        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
	        CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
	        
	       if(rs!=null && rs.next()){
	    	   addOnHistoryId = rs.getLong("ADDON_HISTORY_ID");
	       }
	        
	    	return addOnHistoryId;
	    }
	    
	private boolean isPGAuthAmountIncreased(PaymentsVO payment) throws Exception {

		boolean pgSkipAuthFlag = false;
		BigDecimal initAuthAmount = payment.getInitAuthAmount();
		//BigDecimal bgOrigAuthAmount = null;
		logger.info("payment.getRoute() in isPGAuthAmountIncreased Check ::"+payment.getRoute());
		logger.debug("Modified Payment Amount ::" + payment.getAmount());
		logger.debug("Payment initAuthAmount " + initAuthAmount);
		logger.info("Payment Type " + payment.getPaymentType());

		if (payment.getRoute() != null && !payment.getRoute().isEmpty() && payment.getPaymentType() != null) {
			ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
			String globalRoutes = configUtil.getFrpGlobalParm("SERVICE", "PAYMENT_GATEWAY_ROUTES");
			String isPVSVCEnabled = configUtil.getFrpGlobalParm("SERVICE", "PAYMENT_GATEWAY_SVC_ENABLED");
			if (isPVSVCEnabled.equalsIgnoreCase("Y") && globalRoutes.contains(payment.getRoute())) {

				if (payment.getAmount() != null && initAuthAmount!=null) {
					BigDecimal newAmount = new BigDecimal(payment.getAmount());
					newAmount.setScale(2, RoundingMode.HALF_UP);
					initAuthAmount.setScale(2, RoundingMode.HALF_UP);
					/*logger.debug("bg newAmount ::"+newAmount);
					logger.debug("bg initAuthAmount ::"+initAuthAmount);*/
					
					if (payment.getPaymentId() > 0 && !payment.getPaymentType().equals("PP")) {

						if (newAmount.compareTo(initAuthAmount) > 0) {
							logger.info("Order Amount has been increaded after Original Auth. "
									+ "Hence existing Auhtorization will be removed for Payment::"
									+ payment.getPaymentId());
							pgSkipAuthFlag = true;
						}
					}
				}
			}
		}
		return pgSkipAuthFlag;
	}

	public void resetPaymentAuthorization(PaymentsVO payment) // TODO check
																// wallent and
																// Miles
	{
		/*payment.setAafesTicket("");
		payment.setAcqReferenceNumber("");*/
		//payment.setAmount("");
		payment.setAuthNumber("");
		payment.setAuthResult("");
		payment.setAvsCode("");
		//payment.setDescription("");
		payment.setAuthTransactionId("");
		/* payment.setGiftCertificateId("");
		payment.setInvoiceNumber("");
		payment.setPaymentMethodType("");
		payment.setPaymentsType("");
		payment.setApAccount("");
		payment.setApAuth("");
		payment.setApAccountId("");
		payment.setMilesPointsAmt("");
		payment.setCscResponseCode("");
		payment.setCscValidatedFlag("N");
		payment.setCscFailureCount(0);
		payment.setWalletIndicator("");*/
	}
}
