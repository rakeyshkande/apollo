package com.ftd.osp.utilities.order;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;


import com.ftd.osp.utilities.order.vo.MercentChannelMappingVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

public class MercentOrderPrefixes {

	private static List<String> mercentOrderOriginList = new ArrayList();
	private Connection connection = null;
	private static Logger logger = new Logger("com.ftd.osp.utilities.order.MercentOrderPrfixes");
	
	public MercentOrderPrefixes(Connection conn) {		
		this.connection = conn;
		mercentOrderOriginList = getMercentOrigins();
	}

	
	public boolean isMercentOrder(String orderOrigin) {
		
	  boolean isMercentOrder = false;
	  
	  if(orderOrigin != null) {		
		
		if(mercentOrderOriginList.contains(orderOrigin)) {
		  isMercentOrder = true;
		}		
	  }
	  return isMercentOrder;
	}
	
	public List getMercentOrigins() {
		
		List<String> originsList = new ArrayList<String>();
		List<MercentChannelMappingVO> mercentChannelMappingVOList = getMercentChannelDetails();
	    try {  
	    	for(MercentChannelMappingVO vo : mercentChannelMappingVOList) {
	    		originsList.add(vo.getFtdOriginMapping());
	    	}	    		    
	    } catch(Exception e) {
	    	logger.error("Error occured in MercentOrderPrefix while retrieving mercent order channel mapping details");
	    }
	    return originsList;
		
	}
	
	public List<MercentChannelMappingVO> getMercentChannelDetails()
	{
		List<MercentChannelMappingVO> mercentChannelMappingList = new ArrayList<MercentChannelMappingVO>();
		
		MercentChannelMappingVO channelMappingVO = null;
		 try {
				DataRequest request = new DataRequest();
				request.reset();
				request.setConnection(this.connection);
				HashMap inputParams = new HashMap();
				
				request.setStatementID("GET_MERCENT_CHANNEL_MAPPING_DETAILS");

				Map result = (Map)DataAccessUtil.getInstance().execute(request);
				
				if(result != null) 
				{
					String status = (String) result.get("OUT_STATUS");
					if(!"Y".equalsIgnoreCase(status))
					{
						String message = (String) result.get("OUT_MESSAGE");
						throw new Exception(message);
					}
					CachedResultSet crs = (CachedResultSet) result.get("OUT_CUR");
					while(crs.next())
					{
						channelMappingVO = new MercentChannelMappingVO();
						channelMappingVO.setChannelName(crs.getString("CHANNEL_NAME"));
						channelMappingVO.setChannelImage(crs.getString("CHANNEL_IMAGE"));
						channelMappingVO.setFtdOriginMapping(crs.getString("FTD_ORIGIN_MAPPING"));
						channelMappingVO.setMasterOrderNoPrefix(crs.getString("MASTER_ORDER_NO_PREFIX"));
						channelMappingVO.setConfNumberPrefix(crs.getString("CONF_NUMBER_PREFIX"));
						channelMappingVO.setDefaultSrcCode(crs.getString("DEF_SRC_CODE"));
						channelMappingVO.setDefaultRecalcSrcCode(crs.getString("DEF_RECALC_SRC_CODE"));
						channelMappingVO.setSendConfirmationEmail(crs.getString("SEND_CONFIRMATION_EMAIL"));
						mercentChannelMappingList.add(channelMappingVO);
					}
				}
			} catch (Exception e) {
				logger.error("Error occured in MercentOrderPrefix while retrieving mercent order channel mapping details");
			} 
		
		return mercentChannelMappingList;
	}
	
	public String getMercentIconForChannel(String origin) {
		
		String mercentChannelIcon= null;
		
		for(MercentChannelMappingVO vo : getMercentChannelDetails()) {
			if(vo.getFtdOriginMapping().equals(origin)) {
				mercentChannelIcon = vo.getChannelImage();
			}
		}
		return mercentChannelIcon;
	}
	
	public String getDefaultRecalcSourceCodeForChannel(String origin) {
		
		String defaultRecalcSourceCode= null;
		
		for(MercentChannelMappingVO vo : getMercentChannelDetails()) {
			if(vo.getFtdOriginMapping().equals(origin)) {
				defaultRecalcSourceCode = vo.getDefaultRecalcSrcCode();
			}
		}
		return defaultRecalcSourceCode;
	}
	
	public String getDefaultSourceCodeForChannel(String origin) {
		
		String defaultRecalcSourceCode= null;
		
		for(MercentChannelMappingVO vo : getMercentChannelDetails()) {
			if(vo.getFtdOriginMapping().equals(origin)) {
				defaultRecalcSourceCode = vo.getDefaultSrcCode();
			}
		}
		return defaultRecalcSourceCode;
	}
	
	public String getChannelName(String origin) {
		
		String channelName= null;
		
		for(MercentChannelMappingVO vo : getMercentChannelDetails()) {
			if(vo.getFtdOriginMapping().equals(origin)) {
				channelName = vo.getChannelName();
			}
		}
		return channelName;
	}
}
