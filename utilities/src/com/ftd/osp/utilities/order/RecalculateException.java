package com.ftd.osp.utilities.order;

/** This class defines an exception that occured
 * while attempting to recalculate the amounts
 * on the order VO.
 *  @author Ed Mueller */
public class RecalculateException extends Exception 
{

  public RecalculateException()
  {
    super();
  }
  
  public RecalculateException(String e)
  {
    super(e);
  }
  
  public RecalculateException(String e, Throwable cause)
  {
    super(e, cause);
  }
}