package com.ftd.osp.utilities.order;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.order.vo.AddOnsVO;
import com.ftd.osp.utilities.order.vo.BuyerAddressesVO;
import com.ftd.osp.utilities.order.vo.BuyerCommentsVO;
import com.ftd.osp.utilities.order.vo.BuyerEmailsVO;
import com.ftd.osp.utilities.order.vo.BuyerPhonesVO;
import com.ftd.osp.utilities.order.vo.BuyerVO;
import com.ftd.osp.utilities.order.vo.CoBrandVO;
import com.ftd.osp.utilities.order.vo.CreditCardsVO;
import com.ftd.osp.utilities.order.vo.DispositionsVO;
import com.ftd.osp.utilities.order.vo.FraudCommentsVO;
import com.ftd.osp.utilities.order.vo.MembershipsVO;
import com.ftd.osp.utilities.order.vo.OrderContactInfoVO;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.order.vo.PaymentsVO;
import com.ftd.osp.utilities.order.vo.RecipientAddressesVO;
import com.ftd.osp.utilities.order.vo.RecipientCommentsVO;
import com.ftd.osp.utilities.order.vo.RecipientPhonesVO;
import com.ftd.osp.utilities.order.vo.RecipientsVO;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;

import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


/** This class used to fix up an order object so that
 *  in can be inserted in the FRP database.
 *  @author Ed Mueller*/
public class FRPDataFixer 
{
    private Connection connection;
    private Logger logger;


    private static  final String SHIP_DATE_FORMAT="MM/dd/yyyy";
    private static  final String DELIVERY_DATE_FORMAT="MM/dd/yyyy";
    private static  final String CC_EXPIRATION_DATE_FORMAT="MM/dd/yyyy";
    private static  final String DELIVERY_DATE_RANGE_END_DATE_FORMAT="MM/dd/yyyy";
    private static  final String ORDER_DATE_DATE_FORMAT="MM/dd/yyyy hh:mm:ss";

    private static final String GET_ADDON_BY_ID_STATEMENT = "GET_ADDON_BY_ID";

    //COMMON TABLE VALUES
    private static final int STATUS_LENGTH = 20;

    //ORDER TABLE 
    private static final int ORDER_ORIGIN_LENGTH = 10;
    private static final int CSR_ID_LENGTH = 100;
    private static final int SOURCE_CODE_LENGTH = 10;
    private static final int YELLOW_PAGES_CODE_LENGTH = 10;
    private static final int ARIBA_BUYER_ASN_NUMBER_LENGTH = 100;
    private static final int ARIBA_BUYER_COOKIE_LENGTH = 100;
    private static final int ARIBA_PAYLOAD_LENGTH = 100;  
    private static final int FRAUD_FLAG_LENGTH = 1;  
    private static final int COBRAND_CREDIT_CARD_CODE_LENGTH = 1;  
    private static final String FRAUD_FLAG_DEFAULT = "N";      
    private static final String COBRAND_CREDIT_CARD_CODE_DEFAULT = "N";  
    private static final String MEMBERSHIP_DEFAULT = null;
    private static final String BUYER_EMAIL_ID_DEFAULT = null;
    private static final String ORDER_DATE_DEFAULT = null;
          
    //ORDER DETAILS TABLE
    private static final int PRODUCT_ID_LENGTH = 8;
    private static final int SHIP_VIA_LENGTH = 100;
    private static final int SHIP_METHOD_LENGTH = 10;
    private static final int DROP_SHIP_TRACKING_NUMBER_LENGTH = 100;
    private static final int LAST_MINUTE_GIFT_SIGNATURE_LENGTH = 100;
    private static final int LAST_MINUTE_NUMBER_LENGTH = 100;
    private static final int LAST_MINUTE_GIFT_EMAIL_LENGTH = 100;
    private static final int EXTERNAL_ORDER_NUMBER_LENGTH = 100;
    private static final int COLOR_1_LENGTH = 2;
    private static final int COLOR_2_LENGTH = 2;
    private static final int SUBSTITUE_ACKNOWLEDGEMENT_LENGTH = 2;
    private static final int CARD_MESSAGE_LENGTH = 250;
    private static final int CARD_SIGNATURE_LENGTH = 250;
    private static final int ORDER_COMMENTS_LENGTH = 1000;
    private static final int ORDER_CONTACT_INFORMATION_LENGTH = 1000;
    private static final int FLORIST_NUMBER_LENGTH = 100;
    private static final int SPECIAL_INSTRUCTIONS_LENGTH = 1000;
    private static final int ARIBA_UNSPSC_CODE_LENGTH = 12;
    private static final int ARIBA_PO_NUMBER_LENGTH = 40;
    private static final int ARIBA_AMS_PROJECT_CODE_LENGTH = 100;
    private static final int ARIBA_COST_CENTER_LENGTH = 100;
    private static final int SIZE_INDICATOR_LENGTH = 1;
    private static final int SENDER_INFO_RELEASE_LENGTH = 1;
    private static final String OCCASION_DEFAULT = "8";//default to other
    private static final String SHIP_DATE_DEFAULT = null;
    private static final String DELIVERY_DATE_DEFAULT = null;
    private static final String DELIVERY_DATE_RANGE_END_DEFAULT = null;
    private static final String SENDER_INFO_RELEASE_DEFAULT = "N";
    private static final String SENDER_INFO_RELEASE_NOVATOR_EMPTY = "empty";
    private static final String MILES_POINTS_DEFAULT = null;
    private static final String PRODUCT_ID_DEFAULT = " ";
    private static final int SUBCODE_LENGTH = 10;
    private static final String DELIVERY_SAME_DAY_CODE = "SD";
    
    //ADDON TABLE
    private static final int ADDON_CODE_LENGTH = 10;    

    //BUYER AND RECIPIENT ADDRESS TABLE
    private static final int ADDRESS_TYPE_LENGTH = 40;    
    private static final int ADDRESS_LINE1_LENGTH = 30;
    private static final int ADDRESS_LINE2_LENGTH = 30;
    private static final int COUNTY_LENGTH = 30;    
    private static final int CITY_LENGTH = 30;
    private static final int STATE_PROVINCE_LENGTH = 10;
    private static final int POSTAL_CODE_LENGTH = 10;
    private static final int COUNTRY_LENGTH = 2;
    private static final int GEOFIND_MATCH_CODE_LENGTH = 40;
    private static final int INTERNATIONIAL_LENGTH = 1;
    private static final int ADDRESS_ETC_LENGTH = 30;
    private static final String INTERNATIONIAL_DEFAULT = "N";
    private static final String LATITUDE_DEFAULT = null;
    private static final String LONGITUDE_DEFAULT = null;
    private static final String DESTINATION_ID_DEFAULT = null;    
    private static final String ADDRESS_TYPE_DEFAULT = "HOME";    
    private static final String ADDRESS_LINE1_DEFAULT = " ";
    private static final String CITY_DEFAULT = " ";

    //CREDIT CARD TABLE
    private static final int CC_TYPE_LENGTH = 2;    
    private static final int CC_NUMBER_LENGTH = 2048;
    private static final String CC_EXPIRATION_DEFAULT = "01/01/1901";
    private static final String CC_TYPE_DEFAULT = "VI";    
    private static final String CC_NUMBER_DEFAULT = " ";
    
    //PAYMENT TABLE
    private static final int PAYMENT_TYPE_LENGTH = 2;    
    private static final int GIFT_CERTIFICATE_ID_LENGTH = 15;    
    private static final int AUTH_RESULT_LENGTH = 100;    
    private static final int AUTH_NUMBER_LENGTH = 100;    
    private static final int AVS_CODE_LENGTH = 100;    
    private static final int ACQ_REFERENCE_NUMBER_LENGTH = 100;    
    private static final int AAFES_TICKET_LENGTH = 14;    
    private static final int INVOICE_NUMBER_LENGTH = 100;    
    private static final String PAYMENT_TYPE_DEFAULT = "VI";    
    
    //BUYER TABLE
    private static final int CUSTOMER_ID_LENGTH = 40;  
    private static final int LAST_NAME_LENGTH = 30;    
    private static final int FIRST_NAME_LENGTH = 30;    
    private static final int AUTO_HOLD_LENGTH = 1;    
    private static final int BEST_CUSTOMER_LENGTH = 1;    
    private static final String AUTO_HOLD_DEFAULT = "N";    
    private static final String BEST_CUSTOMER_DEFAULT = "N";    
    private static final String LAST_NAME_DEFAULT = " ";    
    private static final String FIRST_NAME_DEFAULT = " ";   
    
    //RECIPIENT TABLE
    private static final int RECIP_LAST_NAME_LENGTH = 30;    
    private static final int RECIP_FIRST_NAME_LENGTH = 30;
    private static final int RECIP_LIST_CODE_LENGTH = 100;
    private static final int RECIP_AUTO_HOLD_LENGTH = 1;
    private static final int RECIP_MAIL_LIST_CODE_LENGTH = 100;
    private static final int RECIP_CUSTOMER_ID_LENGTH = 40;
    private static final String RECIP_AUTO_HOLD_DEFAULT = "N";    
    private static final String RECIP_LAST_NAME_DEFAULT = " ";    
    private static final String RECIP_FIRST_NAME_DEFAULT = " ";   
    
    //CONTACT TABLE
    private static final int CONTACT_LAST_NAME_LENGTH = 30; 
    private static final int CONTACT_FIRST_NAME_LENGTH = 30; 
    private static final int CONTACT_PHONE_LENGTH = 40; 
    private static final int CONTACT_EXT_LENGTH = 40; 
    private static final int CONTACT_EMAIL_LENGTH = 40;

    //DESTINATIONS TABLE
    private static final int DESTINATION_TYPE_LENGTH = 1;   
    private static final int DESTINATION_NAME_LENGTH = 1000;   
    private static final int INFO_LENGTH = 4000;   
    private static final String DESTINATION_NAME_DEFAULT = " ";   
    private static final String DESTINATION_TYPE_DEFAULT = "H";   
    
    //RECIPIENT COMMENTS TABLE
    private static final int RECIPIENT_COMMENT_LENGTH = 4000;   

    //BUYER COMMENTS TABLE
    private static final int BUYER_COMMENT_LENGTH = 4000;   

    //RECIPIENT PHONE TABLE
    private static final int RECIPIENT_PHONE_TYPE_LENGTH = 40;    
    private static final int RECIPIENT_PHONE_NUMBER_LENGTH = 40;    
    private static final int RECIPIENT_EXTENSION_LENGTH = 40;    
    private static final String RECIPIENT_PHONE_TYPE_DEFAULT = "HOME";    
    private static final String RECIPIENT_PHONE_NUMBER_DEFAULT = " ";    
    
    //BUYER PHONE TABLE
    private static final int BUYER_PHONE_TYPE_LENGTH = 40;    
    private static final int BUYER_PHONE_NUMBER_LENGTH = 40;    
    private static final int BUYER_EXTENSION_LENGTH = 40;    
    private static final String BUYER_PHONE_TYPE_DEFAULT = "HOME";    
    private static final String BUYER_PHONE_NUMBER_DEFAULT = " ";    
    
    //MEMBERSHIP TABLE
    private static final int MEMBERSHIP_TYPE_LENGTH = 40;    
    private static final int MEMBERSHIP_ID_NUMBER_LENGTH = 100;    
    private static final int MEMBERSHIP_FIRST_NAME_LENGTH = 30;    
    private static final int MEMBERSHIP_LAST_NAME_LENGTH = 30;    
    private static final String MEMBERSHIP_TYPE_DEFAULT = " ";    
    
    //BUYER EMAIL TABLE
    private static final int EMAIL_LENGTH = 40;    
    private static final int PRIMARY_LENGTH = 1;    
    private static final int NEWSLETTER_LENGTH = 1;    
    private static final String PRIMARY_DEFAULT = "Y";    
    private static final String NEWSLETTER_DEFAULT= "Y";    

    //CO BRAND TABLE
    private static final int INFO_NAME_LENGTH = 100;    
    private static final int INFO_DATA_LENGTH = 100;    

    //DISPOSITION TABLE
    private static final int DISPOSITION_ID_LENGTH = 1;
    private static final int DISPOSITION_COMMENTS_LENGTH = 200;
    private static final int DISPOSITION_CALLED_CUSTOMER_FLAG_LENGTH=1;
    private static final int DISPOSITION_SENT_EMAIL_FLAG_LENGTH=1;
    private static final int DISPOSITION_STOCK_MESSAGE_ID_LENGTH=10;
    private static final int DISPOSITION_EMAIL_SUBJECT_LENGTH=100;
    private static final int DISPOSITION_EMAIL_MESSAGE_LENGTH=4000;
    private static final String DISPOSITION_CALLED_CUSTOMER_FLAG_DEFAULT="N";
    private static final String DISPOSITION_SENT_EMAIL_FLAG_DEFAULT="N";
    
    //FRAUD COMMENTS LENGTH
    private static final int FRAUD_COMMENT_LENGTH = 4000;

    private static final String GIFT_CERT_ID = "GIFT_CERT_ID";
    private static final String CHECK_GIFT_CERT = "CHECK_GIFT_CERT";

  /** Constructor */
  public FRPDataFixer(Connection conn)
  {
      this.connection = conn;

   
  }

  /** This method fixes an order object so that it can
   *  be inserted into the FRP tables.
   *  @param OrderVO
   *  @returns OrderVO*/
   public OrderVO fixOrder(OrderVO order) throws Exception
   {

      //fix order table values
      fix(order);

      //for each line in the order
      if(order.getOrderDetail() != null){
          Iterator itemIterator = order.getOrderDetail().iterator();
          while(itemIterator.hasNext())
          {
            //get item
            processDetails((OrderDetailsVO) itemIterator.next());        
          }
      }

      //for each co brand in the order
      if(order.getCoBrand() != null){
          Iterator coBrandIterator = order.getCoBrand().iterator();
          while(coBrandIterator.hasNext())
          {
            //get item
            fix((CoBrandVO) coBrandIterator.next());        
          }
      }

      //for each contact in the order
      if(order.getOrderContactInfo() != null){
          Iterator contactIterator = order.getOrderContactInfo().iterator();
          while(contactIterator.hasNext())
          {
            //get item
            fix((OrderContactInfoVO) contactIterator.next());        
          }
      }      

      //for each fraud comment in the order
      if(order.getFraudComments() != null){
          Iterator fraudIterator = order.getFraudComments().iterator();
          while(fraudIterator.hasNext())
          {
            //get item
            fix((FraudCommentsVO) fraudIterator.next());        
          }
      }      

      //for each membership in the order
      if(order.getMemberships() != null){
          Iterator membershipIterator = order.getMemberships().iterator();
          while(membershipIterator.hasNext())
          {
            //get item
            fix((MembershipsVO) membershipIterator.next());        
          }
      }            

      //for each payment in the order
      if(order.getPayments() != null){
          Iterator paymentIterator = order.getPayments().iterator();
          while(paymentIterator.hasNext())
          {
            //get item
            processPayments((PaymentsVO) paymentIterator.next());        
          }
      }                  

      //for each buyer in the order
      if(order.getBuyer() != null){
          Iterator buyerIterator = order.getBuyer().iterator();
          while(buyerIterator.hasNext())
          {
            //get item
            processBuyers((BuyerVO) buyerIterator.next());        
          }
      }                  

      //return order
      return order;

   }

    /* This method processes the buyer VO and all the vos contained
     * within it.*/
     private void processBuyers(BuyerVO buyer)
     {
            fix(buyer);

            //for each address in buyer
            if(buyer.getBuyerAddresses() != null)
            {
              Iterator addressIterator = buyer.getBuyerAddresses().iterator();
              while(addressIterator.hasNext())
              {
                fix((BuyerAddressesVO)addressIterator.next());
              }
            }

            //for each phone in buyer
            if(buyer.getBuyerPhones() != null)
            {
              Iterator phoneIterator = buyer.getBuyerPhones().iterator();
              while(phoneIterator.hasNext())
              {
                fix((BuyerPhonesVO)phoneIterator.next());
              }
            }

            //for each comment in buyer
            if(buyer.getBuyerComments() != null)
            {
              Iterator commentIterator = buyer.getBuyerComments().iterator();
              while(commentIterator.hasNext())
              {
                fix((BuyerCommentsVO)commentIterator.next());
              }
            }            

            //for each email in buyer
            if(buyer.getBuyerEmails() != null)
            {
              Iterator emailIterator = buyer.getBuyerEmails().iterator();
              while(emailIterator.hasNext())
              {
                fix((BuyerEmailsVO)emailIterator.next());
              }
            }                        
     }


    /* This method processes the payemnt VO and all the vos contained
     * within it.*/
     private void processPayments(PaymentsVO payment) throws Exception
     {
            fix(payment);

            //for each addon on the detail line
            if(payment.getCreditCards() != null)
            {
              Iterator cardIterator = payment.getCreditCards().iterator();
              while(cardIterator.hasNext())
              {
                fix((CreditCardsVO)cardIterator.next());
              }
            }//if order contains addons
     }

    /* This method processes the detail VO and all the vos contained
     * within it.*/
     private void processDetails(OrderDetailsVO detail) throws Exception
     {
            fix(detail);

            //for each addon on the detail line
            if(detail.getAddOns() != null)
            {
              Iterator addonIterator = detail.getAddOns().iterator();
              while(addonIterator.hasNext())
              {
                fix((AddOnsVO)addonIterator.next());
              }
            }//if order contains addons


            //for each disposition on the detail line
            if(detail.getDispositions() != null)
            {
              Iterator dispositionIterator = detail.getDispositions().iterator();
              while(dispositionIterator.hasNext())
              {
                fix((DispositionsVO)dispositionIterator.next());
                }
            }//if order contains dispositions


            //for each recipient on the detail line
            if(detail.getRecipients() != null)
            {
              Iterator recipIterator = detail.getRecipients().iterator();
              while(recipIterator.hasNext()){
                processRecipients((RecipientsVO)recipIterator.next());
              }
            }//end if recipient list not null       
     }

    /*This method process all the vo's contained in the recipient vo*/
    private void processRecipients(RecipientsVO recipient)
    {

      fix(recipient);

 

      //for each address on recipient
      if(recipient.getRecipientAddresses() != null){
          Iterator addrIterator = recipient.getRecipientAddresses().iterator();
          while(addrIterator.hasNext())
          {
            //get item
            fix((RecipientAddressesVO) addrIterator.next());        
          }
      }

      //for each phone on recipient
      if(recipient.getRecipientPhones() != null){
          Iterator phoneIterator = recipient.getRecipientPhones().iterator();
          while(phoneIterator.hasNext())
          {
            //get item
            fix((RecipientPhonesVO) phoneIterator.next());        
          }
      }

      //for each comment on recipient
      if(recipient.getRecipientComments() != null){
          Iterator commentIterator = recipient.getRecipientComments().iterator();
          while(commentIterator.hasNext())
          {
            //get item
            fix((RecipientCommentsVO) commentIterator.next());        
          }
      }      
    
     
    }

  /*This method fixes data which will be going into the 
   * order table.*/
   private void fix(OrderVO order)
   {
      order.setCoBrandCreditCardCode(checkNull(truncate(order.getCoBrandCreditCardCode(),COBRAND_CREDIT_CARD_CODE_LENGTH),COBRAND_CREDIT_CARD_CODE_DEFAULT));
      order.setFraudFlag(checkNull(truncate(order.getFraudFlag(),FRAUD_FLAG_LENGTH),FRAUD_FLAG_DEFAULT));
      order.setOrderTotal(checkDecimal(order.getOrderTotal()));
      order.setProductsTotal(checkDecimal(order.getProductsTotal()));
      order.setTaxTotal(checkDecimal(order.getTaxTotal()));
      order.setOrderDate(checkDate(order.getOrderDate(),ORDER_DATE_DEFAULT,ORDER_DATE_DATE_FORMAT));
      order.setServiceFeeTotal(checkDecimal(order.getServiceFeeTotal()));
      //order.setBuyerEmailId(checkNumber(order.getBuyerEmailId(),BUYER_EMAIL_ID_DEFAULT));
      order.setShippingFeeTotal(checkDecimal(order.getShippingFeeTotal()));
      order.setOrderOrigin(truncate(order.getOrderOrigin(),ORDER_ORIGIN_LENGTH));
      //order.setMembershipId(checkNumber(order.getMembershipId(),MEMBERSHIP_DEFAULT));
      order.setCsrId(truncate(order.getCsrId(),CSR_ID_LENGTH));
      order.setAddOnAmountTotal(checkDecimal(order.getAddOnAmountTotal()));
      order.setPartnershipBonusPoints(checkDecimal(order.getPartnershipBonusPoints()));
      order.setSourceCode(truncate(order.getSourceCode(),SOURCE_CODE_LENGTH));
      order.setCallTime(checkNumber(order.getCallTime()));
      order.setDiscountTotal(checkDecimal(order.getDiscountTotal()));
      order.setYellowPagesCode(truncate(order.getYellowPagesCode(),YELLOW_PAGES_CODE_LENGTH));
      order.setStatus(truncate(order.getStatus(),STATUS_LENGTH));
      order.setAribaBuyerAsnNumber(truncate(order.getAribaBuyerAsnNumber(),ARIBA_BUYER_ASN_NUMBER_LENGTH));
      order.setAribaBuyerCookie(truncate(order.getAribaBuyerCookie(),ARIBA_BUYER_COOKIE_LENGTH));
      order.setAribaPayload(truncate(order.getAribaPayload(),ARIBA_PAYLOAD_LENGTH));


      
     
   }

  /*This method fixes data which will be going into the 
   * order details table.*/
   private void fix(OrderDetailsVO detail)
   {
      detail.setProductId(checkNull(truncate(detail.getProductId(),PRODUCT_ID_LENGTH),PRODUCT_ID_DEFAULT));    
      detail.setQuantity(checkNumber(detail.getQuantity()));
      detail.setShipVia(truncate(detail.getShipVia(),SHIP_VIA_LENGTH));
      detail.setShipMethod(truncate(detail.getShipMethod(),SHIP_METHOD_LENGTH));
      detail.setShipDate(checkDate(detail.getShipDate(),SHIP_DATE_DEFAULT,SHIP_DATE_FORMAT));
      detail.setDeliveryDate(checkDate(detail.getDeliveryDate(),DELIVERY_DATE_DEFAULT,DELIVERY_DATE_FORMAT));
      detail.setDeliveryDateRangeEnd(checkDate(detail.getDeliveryDateRangeEnd(),DELIVERY_DATE_RANGE_END_DEFAULT,DELIVERY_DATE_RANGE_END_DATE_FORMAT));
      detail.setDropShipTrackingNumber(truncate(detail.getDropShipTrackingNumber(),DROP_SHIP_TRACKING_NUMBER_LENGTH));
      detail.setProductsAmount(checkDecimal(detail.getProductsAmount()));
      detail.setTaxAmount(checkDecimal(detail.getTaxAmount()));
      detail.setServiceFeeAmount(checkDecimal(detail.getServiceFeeAmount()));
      detail.setShippingFeeAmount(checkDecimal(detail.getShippingFeeAmount()));
      detail.setAddOnAmount(checkDecimal(detail.getAddOnAmount()));
      detail.setDiscountAmount(checkDecimal(detail.getDiscountAmount()));
      detail.setOccassionId(checkNumber(detail.getOccassionId(),OCCASION_DEFAULT));
      detail.setLastMinuteGiftSignature(truncate(detail.getLastMinuteGiftSignature(),LAST_MINUTE_GIFT_SIGNATURE_LENGTH));
      detail.setLastMinuteNumber(truncate(detail.getLastMinuteNumber(),LAST_MINUTE_NUMBER_LENGTH));
      detail.setLastMinuteGiftEmail(truncate(detail.getLastMinuteGiftEmail(),LAST_MINUTE_GIFT_EMAIL_LENGTH));
      detail.setExternalOrderNumber(truncate(detail.getExternalOrderNumber(),EXTERNAL_ORDER_NUMBER_LENGTH));
      detail.setColorFirstChoice(truncate(detail.getColorFirstChoice(),COLOR_1_LENGTH));
      detail.setColorSecondChoice(truncate(detail.getColorSecondChoice(),COLOR_2_LENGTH));
      detail.setSubstituteAcknowledgement(truncate(detail.getSubstituteAcknowledgement(),SUBSTITUE_ACKNOWLEDGEMENT_LENGTH));
      detail.setCardMessage(truncate(detail.getCardMessage(),CARD_MESSAGE_LENGTH));
      detail.setCardSignature(truncate(detail.getCardSignature(),CARD_SIGNATURE_LENGTH));
      detail.setOrderComments(truncate(detail.getOrderComments(),ORDER_COMMENTS_LENGTH));
      detail.setOrderContactInformation(truncate(detail.getOrderContactInformation(),ORDER_CONTACT_INFORMATION_LENGTH));
      detail.setFloristNumber(truncate(detail.getFloristNumber(),FLORIST_NUMBER_LENGTH));
      detail.setSpecialInstructions(truncate(detail.getSpecialInstructions(),SPECIAL_INSTRUCTIONS_LENGTH));
      detail.setStatus(truncate(detail.getStatus(),STATUS_LENGTH));
      detail.setAribaUnspscCode(truncate(detail.getAribaUnspscCode(),ARIBA_UNSPSC_CODE_LENGTH));
      detail.setAribaPoNumber(truncate(detail.getAribaPoNumber(),ARIBA_PO_NUMBER_LENGTH));
      detail.setAribaAmsProjectCode(truncate(detail.getAribaAmsProjectCode(),ARIBA_AMS_PROJECT_CODE_LENGTH));
      detail.setAribaCostCenter(truncate(detail.getAribaCostCenter(),ARIBA_COST_CENTER_LENGTH));
      detail.setExternalOrderTotal(checkDecimal(detail.getExternalOrderTotal()));
      detail.setSizeChoice(truncate(detail.getSizeChoice(),SIZE_INDICATOR_LENGTH));

      //Novator sends us some messed up values, so if this field contains 'empty' treat it as null
      if(detail.getSenderInfoRelease() != null && detail.getSenderInfoRelease().trim().equals(SENDER_INFO_RELEASE_NOVATOR_EMPTY))
      {
        detail.setSenderInfoRelease(null);
      }

      // Set same day ship method (SD) to null
      // becuase we do not store the SD record in the database for integrity 
      // reasons
      if(detail.getShipMethod() != null && detail.getShipMethod().equals(DELIVERY_SAME_DAY_CODE))
      {
        detail.setShipMethod(null);
      }      
      
      detail.setSenderInfoRelease(checkNull(truncate(detail.getSenderInfoRelease(),SENDER_INFO_RELEASE_LENGTH),SENDER_INFO_RELEASE_DEFAULT));
      detail.setMilesPoints(checkDecimal(detail.getMilesPoints(),MILES_POINTS_DEFAULT));
      detail.setProductSubCodeId(truncate(detail.getProductSubCodeId(),SUBCODE_LENGTH));
   }   

  /*This method fixes data which will be going into the 
   * co brand table.*/
   private void fix(CoBrandVO cobrand)
   {
     cobrand.setInfoName(truncate(cobrand.getInfoName(),INFO_NAME_LENGTH));
     cobrand.setInfoData(truncate(cobrand.getInfoData(),INFO_DATA_LENGTH));
   }      

  /*This method fixes data which will be going into the 
   * order contact info table.*/
   private void fix(OrderContactInfoVO contact)
   {
      contact.setFirstName(truncate(contact.getFirstName(),CONTACT_FIRST_NAME_LENGTH));
      contact.setLastName(truncate(contact.getLastName(),CONTACT_LAST_NAME_LENGTH));
      contact.setPhone(truncate(contact.getPhone(),CONTACT_PHONE_LENGTH));
      contact.setExt(truncate(contact.getExt(),CONTACT_EXT_LENGTH));
      contact.setEmail(truncate(contact.getEmail(),CONTACT_EMAIL_LENGTH));
   }      

  /*This method fixes data which will be going into the 
   * addons table.*/
   private void fix(AddOnsVO addon) throws Exception
   {
      //If an addon code was given, but it does not exist in DB..then null it out.
      //This is to prevent errors with forign key constraints
      if(addon.getAddOnCode() != null && !addonExists(addon.getAddOnCode()))
      {
        addon.setAddOnCode(null);        
      }
      else{
        addon.setAddOnCode(truncate(addon.getAddOnCode(),ADDON_CODE_LENGTH));          
      }
      
      addon.setAddOnQuantity(checkNumber(addon.getAddOnQuantity()));
   }      

  /*This method fixes data which will be going into the 
   * Disposition table.*/
   private void fix(DispositionsVO disposition)
   {
      disposition.setDispositionID(truncate(disposition.getDispositionID(),DISPOSITION_ID_LENGTH));    
      disposition.setComments(truncate(disposition.getComments(),DISPOSITION_COMMENTS_LENGTH));    
      disposition.setCalledCustomerFlag(checkNull(truncate(disposition.getCalledCustomerFlag(),DISPOSITION_CALLED_CUSTOMER_FLAG_LENGTH),DISPOSITION_CALLED_CUSTOMER_FLAG_DEFAULT));    
      disposition.setSentEmailFlag(checkNull(truncate(disposition.getSentEmailFlag(),DISPOSITION_SENT_EMAIL_FLAG_LENGTH),DISPOSITION_SENT_EMAIL_FLAG_DEFAULT));    
      disposition.setStockMessageID(truncate(disposition.getStockMessageID(),DISPOSITION_STOCK_MESSAGE_ID_LENGTH));    
      disposition.setEmailMessage(truncate(disposition.getEmailMessage(),DISPOSITION_EMAIL_MESSAGE_LENGTH));    
      disposition.setEmailSubject(truncate(disposition.getEmailSubject(),DISPOSITION_EMAIL_SUBJECT_LENGTH));    
   }      


  /*This method fixes data which will be going into the 
   * recipient address table.*/
   private void fix(RecipientAddressesVO address)
   {
      address.setAddressType(checkNull(truncate(address.getAddressType(),ADDRESS_TYPE_LENGTH),ADDRESS_TYPE_DEFAULT));    
      address.setAddressLine1(checkNull(truncate(address.getAddressLine1(),ADDRESS_LINE1_LENGTH),ADDRESS_LINE1_DEFAULT));
      address.setAddressLine2(truncate(address.getAddressLine2(),ADDRESS_LINE2_LENGTH));
      address.setCity(checkNull(truncate(address.getCity(),CITY_LENGTH),CITY_DEFAULT));
      address.setStateProvince(truncate(address.getStateProvince(),STATE_PROVINCE_LENGTH));
      address.setPostalCode(truncate(address.getPostalCode(),POSTAL_CODE_LENGTH));
      address.setCountry(truncate(address.getCountry(),COUNTRY_LENGTH));
      address.setCounty(truncate(address.getCounty(),COUNTY_LENGTH));
      address.setGeofindMatchCode(truncate(address.getGeofindMatchCode(),GEOFIND_MATCH_CODE_LENGTH));
      address.setLatitude(checkNumber(address.getLatitude(),LATITUDE_DEFAULT));
      address.setLongitude(checkNumber(address.getLongitude(),LONGITUDE_DEFAULT));
      //address.setDestinationId(checkNumber(address.getDestinationId(),DESTINATION_ID_DEFAULT));
      address.setInternational(checkNull(truncate(address.getInternational(),INTERNATIONIAL_LENGTH),INTERNATIONIAL_DEFAULT));
      address.setName(checkNull(truncate(address.getName(),DESTINATION_NAME_LENGTH),DESTINATION_NAME_DEFAULT));
      address.setInfo(truncate(address.getInfo(),INFO_LENGTH));

   }      

  /*This method fixes data which will be going into the 
   * recipient table.*/
   private void fix(RecipientsVO recip)
   {
     recip.setLastName(checkNull(truncate(recip.getLastName(),RECIP_LAST_NAME_LENGTH),RECIP_LAST_NAME_DEFAULT));
     recip.setFirstName(checkNull(truncate(recip.getFirstName(),RECIP_FIRST_NAME_LENGTH),RECIP_FIRST_NAME_DEFAULT));
     recip.setListCode(truncate(recip.getListCode(),RECIP_LIST_CODE_LENGTH));
     recip.setAutoHold(checkNull(truncate(recip.getAutoHold(),RECIP_AUTO_HOLD_LENGTH),RECIP_AUTO_HOLD_DEFAULT));
     recip.setMailListCode(truncate(recip.getMailListCode(),RECIP_MAIL_LIST_CODE_LENGTH));
     recip.setStatus(truncate(recip.getStatus(),STATUS_LENGTH));
     recip.setCustomerId(truncate(recip.getCustomerId(),RECIP_CUSTOMER_ID_LENGTH));
   }      

  /*This method fixes data which will be going into the 
   * recipient phones table.*/
   private void fix(RecipientPhonesVO phone)
   {
     phone.setPhoneType(checkNull(truncate(phone.getPhoneType(),RECIPIENT_PHONE_TYPE_LENGTH),RECIPIENT_PHONE_TYPE_DEFAULT));
     phone.setPhoneNumber(checkNull(truncate(phone.getPhoneNumber(),RECIPIENT_PHONE_NUMBER_LENGTH),RECIPIENT_PHONE_NUMBER_DEFAULT));
     phone.setExtension(truncate(phone.getExtension(),RECIPIENT_EXTENSION_LENGTH));     
   }      



  /*This method fixes data which will be going into the 
   * recipient comments table.*/
   private void fix(RecipientCommentsVO comment)
   {
     comment.setText(truncate(comment.getText(),RECIPIENT_COMMENT_LENGTH));
   }      

  /*This method fixes data which will be going into the 
   * fraud comments table.*/
   private void fix(FraudCommentsVO comment)
   {
     comment.setCommentText(truncate(comment.getCommentText(),FRAUD_COMMENT_LENGTH));
   }      

  /*This method fixes data which will be going into the 
   * buyers table.*/
   private void fix(BuyerVO buyer)
   {
      buyer.setCustomerId(truncate(buyer.getCustomerId(),CUSTOMER_ID_LENGTH));   
      buyer.setLastName(checkNull(truncate(buyer.getLastName(),LAST_NAME_LENGTH),LAST_NAME_DEFAULT));
      buyer.setFirstName(checkNull(truncate(buyer.getFirstName(),FIRST_NAME_LENGTH),FIRST_NAME_DEFAULT));
      buyer.setAutoHold(checkNull(truncate(buyer.getAutoHold(),AUTO_HOLD_LENGTH),AUTO_HOLD_DEFAULT));
      buyer.setBestCustomer(checkNull(truncate(buyer.getBestCustomer(),BEST_CUSTOMER_LENGTH),BEST_CUSTOMER_DEFAULT));
      buyer.setStatus(truncate(buyer.getStatus(),STATUS_LENGTH));
   }      

  /*This method fixes data which will be going into the 
   * buyer emails table.*/
   private void fix(BuyerEmailsVO email)
   {
      email.setEmail(truncate(email.getEmail(),EMAIL_LENGTH));       
      email.setPrimary(checkNull(truncate(email.getPrimary(),PRIMARY_LENGTH),PRIMARY_DEFAULT));
      email.setNewsletter(checkNull(truncate(email.getNewsletter(),NEWSLETTER_LENGTH),NEWSLETTER_DEFAULT));
   }      

  /*This method fixes data which will be going into the 
   * membership table.*/
   private void fix(MembershipsVO membership)
   {
     membership.setMembershipType(checkNull(truncate(membership.getMembershipType(),MEMBERSHIP_TYPE_LENGTH),MEMBERSHIP_TYPE_DEFAULT));
     membership.setMembershipIdNumber(truncate(membership.getMembershipIdNumber(),MEMBERSHIP_ID_NUMBER_LENGTH));
     membership.setFirstName(truncate(membership.getFirstName(),MEMBERSHIP_FIRST_NAME_LENGTH));
     membership.setLastName(truncate(membership.getLastName(),MEMBERSHIP_LAST_NAME_LENGTH));
   }      

  /*This method fixes data which will be going into the 
   * buyer phones table.*/
   private void fix(BuyerPhonesVO phone)
   {
     phone.setPhoneType(checkNull(truncate(phone.getPhoneType(),BUYER_PHONE_TYPE_LENGTH),BUYER_PHONE_TYPE_DEFAULT));
     phone.setPhoneNumber(checkNull(truncate(phone.getPhoneNumber(),BUYER_PHONE_NUMBER_LENGTH),BUYER_PHONE_NUMBER_DEFAULT));
     phone.setExtension(truncate(phone.getExtension(),BUYER_EXTENSION_LENGTH));
   }      

  /*This method fixes data which will be going into the 
   * payments table.*/
   private void fix(PaymentsVO payment) throws Exception
   {

      //if credit card type does not exist in DB set it to null
      if(payment.getPaymentType() != null && !creditcardExists(payment.getPaymentType()))
      {
        payment.setPaymentsType(PAYMENT_TYPE_DEFAULT);        
      }
      else{
        payment.setPaymentsType(checkNull(truncate(payment.getPaymentType(),PAYMENT_TYPE_LENGTH),PAYMENT_TYPE_DEFAULT));
      }   

      payment.setPaymentMethodType(checkNull(truncate(payment.getPaymentMethodType(),PAYMENT_TYPE_LENGTH),PAYMENT_TYPE_DEFAULT));   
      payment.setAmount(checkDecimal(payment.getAmount()));
      //payment.setCreditCardId(checkNumber(payment.getCreditCardId()));
      payment.setGiftCertificateId(truncate(payment.getGiftCertificateId(),GIFT_CERTIFICATE_ID_LENGTH));
      payment.setAuthResult(truncate(payment.getAuthResult(),AUTH_RESULT_LENGTH));
      payment.setAuthNumber(truncate(payment.getAuthNumber(),AUTH_NUMBER_LENGTH));
      payment.setAvsCode(truncate(payment.getAvsCode(),AVS_CODE_LENGTH));
      payment.setAcqReferenceNumber(truncate(payment.getAcqReferenceNumber(),ACQ_REFERENCE_NUMBER_LENGTH));
      payment.setAafesTicket(truncate(payment.getAafesTicket(),AAFES_TICKET_LENGTH));
      payment.setInvoiceNumber(truncate(payment.getInvoiceNumber(),INVOICE_NUMBER_LENGTH));

      //if gift certificate is non-null make sure it is a valid one
      if(payment.getGiftCertificateId() != null)
      {
        DataRequest dataRequest = new DataRequest();
        DataAccessUtil dau = DataAccessUtil.getInstance();        
        dataRequest.setConnection(connection);
        dataRequest.setStatementID(CHECK_GIFT_CERT);
        dataRequest.addInputParam(GIFT_CERT_ID, payment.getGiftCertificateId());
        String result = (String)dau.execute(dataRequest);
        dataRequest.reset();

        if(result != null && result.equals("Y"))
        {
            // exists
        }
        else
        {
            payment.setGiftCertificateId(null);
        }        
      }

   }      

  /*This method fixes data which will be going into the 
   * credit card table.*/
   private void fix(CreditCardsVO card) throws Exception
   {

      //if credit card type does not exist in DB set it to null
      if(card.getCCType() != null && !creditcardExists(card.getCCType()))
      {
        card.setCCType(CC_TYPE_DEFAULT);        
      }
      else{
        card.setCCType(checkNull(truncate(card.getCCType(),CC_TYPE_LENGTH),CC_TYPE_DEFAULT));    
      }   

      card.setCCNumber(checkNull(truncate(card.getCCNumber(),CC_NUMBER_LENGTH),CC_NUMBER_DEFAULT));
      card.setCCExpiration(checkDate(card.getCCExpiration(),CC_EXPIRATION_DEFAULT,CC_EXPIRATION_DATE_FORMAT));
   }      

  /*This method fixes data which will be going into the 
   * buyer comments table.*/
   private void fix(BuyerCommentsVO comment)
   {
     comment.setText(truncate(comment.getText(),BUYER_COMMENT_LENGTH));     
   }      

  /*This method fixes data which will be going into the 
   * buyer addresses table.*/
   private void fix(BuyerAddressesVO address)
   {
      address.setAddressType(checkNull(truncate(address.getAddressType(),ADDRESS_TYPE_LENGTH),ADDRESS_TYPE_DEFAULT));    
      address.setAddressLine1(checkNull(truncate(address.getAddressLine1(),ADDRESS_LINE1_LENGTH),ADDRESS_LINE1_DEFAULT));
      address.setAddressLine2(truncate(address.getAddressLine2(),ADDRESS_LINE2_LENGTH));
      address.setCity(checkNull(truncate(address.getCity(),CITY_LENGTH),CITY_DEFAULT));
      address.setStateProv(truncate(address.getStateProv(),STATE_PROVINCE_LENGTH));
      address.setPostalCode(truncate(address.getPostalCode(),POSTAL_CODE_LENGTH));
      address.setCountry(truncate(address.getCountry(),COUNTRY_LENGTH));
      address.setCounty(truncate(address.getCounty(),COUNTY_LENGTH));
      address.setAddressEtc(truncate(address.getAddressEtc(),ADDRESS_ETC_LENGTH));
   }      

   /* This method makes sure the passed in number is a number.
    * If the passed in number is null or not a number then 
    * the passed in default value will be returned.
    * @String string value to check
    * @String default to use is passed in value is not an integer*/
   private String checkNumber(String value, String defaultValue)
   {
     String numValue = null;
   
     if(value == null)
        return defaultValue;

     //verify that is is an interger
     try
     {
       numValue = new Integer(value).toString();
     }
     catch(NumberFormatException  nfe)
     {
       numValue = defaultValue;
     }

     return numValue;
     
   }

   /* This method makes sure the passed in number is numeric.
    * If the passed in number is null or not numeric then zero
    * will be returned.
    * @String string value to check
    * @String default to use is passed in value is not an integer*/
   private String checkNumber(String value)
   {
      return checkNumber(value,"0");
   }

   /* This method makes sure the passed in number is a decimal.
    * If the passed in number is null or not an decimal then the
    * default will be returned.*/
   private String checkDecimal(String value, String defaultValue)
   {
     String decValue = null;
   
     if(value == null)
        return defaultValue;

     //verify that is is an decimal
     try
     {
       decValue = new Double(value).toString();
     }
     catch(NumberFormatException  nfe)
     {
       decValue = defaultValue;
     }

     return decValue;     
   }

   /* This method makes sure the passed in number is a decimal.
    * If the passed in number is null or not an decimal then zero
    * will be returned.*/
   private String checkDecimal(String value)
   {   
      return checkDecimal(value,"0.0");
   }
   
  /* This method truncates a string value.*/
  private String truncate(String value,int max)
  {
    String newValue = value;
  
    if(value != null && value.length() > max)
    {
      newValue = value.substring(0,max);
    }

    return newValue;
  }

/*
 * Description: Takes in a string Date and checks to make sure it
 * is a valid date.
 */
private String checkDate(String strDate, String defaultValue, String dateFormat){

        String outDateString = null;
        String inDateFormat = "";
        int dateLength = 0;
        int firstSep = 0;
        int lastSep = 0;

    try{       

    if ((strDate == null) || (strDate.trim().equals(""))) 
        return defaultValue;


    //if lenght of 4 then it is the format of MMDD..reformat it
    if(strDate.length() == 5)
    {
      String month = strDate.substring(0,2);
      String year = strDate.substring(3,5);
      int yearNum =   (new Integer(year)).intValue() + 2000;
      int monthNum = new Integer(month).intValue();

      //get the last day of the month
      Calendar cal = Calendar.getInstance();
      cal.set(yearNum, monthNum -1 ,1);
      String lastDayMonth = Integer.toString(cal.getActualMaximum(Calendar.DAY_OF_MONTH));
  
      strDate = month + "/"+lastDayMonth+"/" + yearNum;
      
    }



		    // set input date format
		    dateLength = strDate.length();
		    if ( dateLength > 10) {
			    firstSep = strDate.indexOf("/");
          if(firstSep == 1)
          {
            inDateFormat = "M/dd/yyyy hh:mm:ss";            
          }
          else if (firstSep == 2)
          {
            inDateFormat = "MM/dd/yyyy hh:mm:ss";
          }
          else
          {
  			    firstSep = strDate.indexOf("/");          
            if(firstSep > 0){
              inDateFormat = "yyyy-MM-dd hh:mm:ss"; 
            }
            else
            {
              //check for spaces
              firstSep = strDate.indexOf(" ");          
              if(firstSep <= 0){
                inDateFormat = "yyyyMMddHHmmsszzz";
              }
              else
              {
                  if(dateLength > 20){
                      inDateFormat = "EEE MMM dd HH:mm:ss zzz yyyy";               
                  }
                  else{
                      inDateFormat = "MMM dd yyyy HH:mmaa";               
                  }
              }
            }
          }
			    
		    } else {
			    firstSep = strDate.indexOf("/");
			    lastSep = strDate.lastIndexOf("/");

			    switch ( dateLength ) {
				    case 10:
			    		inDateFormat = "MM/dd/yyyy";
				    	break;
				    case 9:
				    	if ( firstSep == 1 ) {
					    	inDateFormat = "M/dd/yyyy";
				    	} else {
					    	inDateFormat = "MM/d/yyyy";
			    		}
				    	break;
				    case 8:
				    	if ( firstSep == 1 ) {
					    	inDateFormat = "M/d/yyyy";
				    	} else {
					    	inDateFormat = "MM/dd/yy";
			    		}
				    	break;
				    case 7:
				    	if ( firstSep == 1 ) {
					    	inDateFormat = "M/dd/yy";
				    	} else {
					    	inDateFormat = "MM/d/yy";
			    		}
				    	break;
				    case 6:
				    	inDateFormat = "M/d/yy";
				    	break;
				    default:
				    	break;
			    }//end switch
		    }
            SimpleDateFormat sdfInput = new SimpleDateFormat(inDateFormat);
            SimpleDateFormat sdfOutput = new SimpleDateFormat ( dateFormat );

            java.util.Date date = sdfInput.parse( strDate );
            outDateString = sdfOutput.format( date );

        }
      catch(Throwable t)
      {
        outDateString = defaultValue;
      }		  

	return outDateString;
}

 /** This method checks if the passed in value is null.
  *  If it is the default value is returned. */
  private String checkNull(String value,String defaultValue)
  {
    return value == null || value.length() < 1 ? defaultValue : value;
  }


 private boolean creditcardExists(String cardId) throws Exception
 {
      Map paramMap = new HashMap();     

      boolean exists = false;

      paramMap.put("payment_method_id",cardId);

      // Get the order header infomration
      DataRequest dataRequest = new DataRequest();
      dataRequest.setConnection(connection);
      dataRequest.setStatementID("GET_PAYMENT_METHOD_BY_ID_SET");
      dataRequest.setInputParams(paramMap);

      Map addonMap = null;
      
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      CachedResultSet rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);       
      if(rs != null && rs.next())
      {
        exists = true;
      }   

      return exists;
 }

 private boolean addonExists(String addonId) throws Exception
 {
      Map paramMap = new HashMap();     

      boolean exists = false;

      paramMap.put("IN_ADDON_ID",addonId);

      // Get the order header infomration
      DataRequest dataRequest = new DataRequest();
      dataRequest.setConnection(connection);
      dataRequest.setStatementID(GET_ADDON_BY_ID_STATEMENT);
      dataRequest.setInputParams(paramMap);

      Map addonMap = null;
      
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      CachedResultSet rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);       
      if(rs != null && rs.next())
      {
        exists = true;
      }   

      return exists;
 }
   
public static void main(String[] args)
{

 
  String test = new FRPDataFixer(null).checkDate("drg54lkt09kdo;gk",CC_EXPIRATION_DEFAULT,CC_EXPIRATION_DATE_FORMAT);
  System.out.println(":::" + test);
}


}