package com.ftd.osp.utilities.order;

import com.ftd.osp.utilities.dataaccess.*;
import com.ftd.ftdutilities.GeneralConstants;

import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;

import java.sql.Connection;

/**
 * This class controls the order id as described in defect #5782.S
 */

public class OrderIdGenerator
{
    private static OrderIdGenerator ORDERIDGENERATOR = new OrderIdGenerator();
    
    public static OrderIdGenerator getInstance()
    {
        return ORDERIDGENERATOR;
    }
    /**
     * 
     * @param conn -- Database connection
     * @return -- Next available order id
     * @throws Exception
     */
    public String getOrderId (Connection conn) throws Exception
    {
        String orderId = null;
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID(GeneralConstants.GET_NEXT_ORDER_ID);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        orderId = (String)dataAccessUtil.execute(dataRequest);
        
        
        
        return orderId;
    }
}
