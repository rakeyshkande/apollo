package com.ftd.osp.utilities.order.test.order;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.order.ScrubMapperDAO;
import com.ftd.osp.utilities.order.vo.AVSAddressScoreVO;
import com.ftd.osp.utilities.order.vo.AVSAddressVO;
import com.ftd.osp.utilities.order.vo.CoBrandVO;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.OrderVO;

import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.Connection;
import java.util.*;

import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.w3c.dom.Document;

/**
 * Test class for data access class for Scrub schema 
 *
 * @author Rose Lazuk
 */
public class ScrubMapperDAOTest  extends TestCase
{
    private String orderXMLString = "";
    private Document orderXMLDocument = null;
    private OrderVO order = null;
    private ScrubMapperDAO scrubMapperDAO = null;
    private ConfigurationUtil configUtil = null;
    private final static String UTILITIES_CONFIG_FILE = "utilities_config.xml";
    private boolean mappedSuccessfully = false;
    
    
    /** 
     * Create a constructor that take a String parameter and passes it 
     * to the super class 
     * @param name String
     **/ 
    public ScrubMapperDAOTest(String name) { 
        super(name); 
        try 
        {
          configUtil = ConfigurationUtil.getInstance(); 
        } catch (Exception ex) 
        {
          ex.printStackTrace();
        } 
    }

    /**
     * Method for setting up initialization parameters
     */
    protected void setUp()
    { 
       
    }

    /**
     * Overloaded method for setting up initialization variables 
     * @param fileName String name of the file with the xml
     * to test
     */
    private void setUp(String fileName)
    { 
      try
      {
        System.out.println("getting xml");
        String record = "";
                
        FileReader fr     = new FileReader(fileName);
        
        BufferedReader br = new BufferedReader(fr);
        StringBuffer sb = new StringBuffer();
        while ((record = br.readLine()) != null) {
           sb.append(record);
        }
        System.out.println("got xml");
        this.orderXMLString = sb.toString();
        System.out.println("orderXMLString: " + orderXMLString);
                    
      }
      catch(Exception ex)
      {
        ex.printStackTrace();
      }
    
    }   
   
   /** 
    * Test inserting order into the Scrub Database
    * using the ScrubMapperDAO. A boolean value of true
    * indicates that the order was successfully inserted.
    * A boolean value of false indicates that there was
    * an error inserting the order into the database.
    **/ 
    public void testMapGoodOrderToDB() throws Exception{ 
    System.out.println("testing map good order to db");
          
    setUp(configUtil.getProperty(UTILITIES_CONFIG_FILE, "good_order"));
    ConvertXMLToOrder orderConverter = new ConvertXMLToOrder(this.orderXMLString);
    order = orderConverter.createOrder();
    //check to see if order is not null
    assertNotNull(order);

    //get a connection
    //check to see that the connection is not null
    ConnectionTest conn = new ConnectionTest();
    Connection connection = conn.getConnection();
    assertNotNull(connection);

    ScrubMapperDAO scrubMapperDAO = new ScrubMapperDAO(connection);
    mappedSuccessfully = scrubMapperDAO.mapOrderToDB(order);

    //Check if the result is the same as expected
    //A result of true is expected if the order is
    //inserted into the database correctly
    assertEquals("testMapGoodOrderToDB", true, mappedSuccessfully);
 } 

  /** 
    * Test inserting duplicate order into the Scrub Database
    * using the ScrubMapperDAO. A boolean value of false
    * indicates that there was an error inserting the order
    * into the database.
    **/ 
    public void testMapDuplicateOrderToDB() throws Exception{ 
    System.out.println("testing map duplicate order to db");
          
    setUp(configUtil.getProperty(UTILITIES_CONFIG_FILE, "duplicate_order"));
    ConvertXMLToOrder orderConverter = new ConvertXMLToOrder(this.orderXMLString);
    order = orderConverter.createOrder();
    //check to see if order is not null
    assertNotNull(order);

    //get a connection
    //check to see that the connection is not null
    ConnectionTest conn = new ConnectionTest();
    Connection connection = conn.getConnection();
    assertNotNull(connection);

    ScrubMapperDAO scrubMapperDAO = new ScrubMapperDAO(connection);
    try
    {
      mappedSuccessfully = scrubMapperDAO.mapOrderToDB(order);
    }
    catch(Exception e)
    {
      //Check if the result is the same as expected
      //A result of false is expected because the test
      //is trying to insert a duplicate record into
      //the database
      assertEquals("testMapDuplicateOrderToDB", false, mappedSuccessfully);  
    }
 } 

  /** 
    * Test getting an existing order from the Scrub Database
    * using the ScrubMapperDAO. This is expecting to process the order created by the Goodfile.xml found in OrderGatherer.
    * This includes special verification of the AVS address information.
    **/ 
    public void testMapExistingOrderFromDB() throws Exception{ 
    System.out.println("testing map existing order from db");
          
    //get a connection
    ConnectionTest conn = new ConnectionTest();
    Connection connection = conn.getConnection();
    
    //check to see that the connection is not null
    assertNotNull(connection);

    ScrubMapperDAO scrubMapperDAO = new ScrubMapperDAO(connection);
    order = scrubMapperDAO.mapOrderFromDB(configUtil.getProperty(UTILITIES_CONFIG_FILE, "existing_order_guid"));
    System.out.println(order);
    //Check if the result is the same as expected
    //A result of an order that is not null is expected
    //if the order was retrieved from the database correctly
    assertNotNull(order);
    
    // verify AVS address results
    List <OrderDetailsVO>details = order.getOrderDetail();
    assertEquals(4, details.size());
    for (OrderDetailsVO detail : details)
    {
        if (detail.getExternalOrderNumber().startsWith("CMCHIL1"))
        {
            // order with no avs address
            assertNull(detail.getAvsAddress());
        }
        else if (detail.getExternalOrderNumber().startsWith("CMCHIL2"))
        {
            // order with full avs address and scores
            AVSAddressVO address = detail.getAvsAddress();
            assertNotNull(address);
            List<AVSAddressScoreVO>scores = address.getScores();
            assertEquals(11, scores.size());
            // verify address elements are not null - don't worry about actual values
            assertNotNull(address.getAddress());
            assertNotNull(address.getAvsPerformed());
            assertNotNull(address.getCity());
            assertNotNull(address.getCountry());
            assertNotNull(address.getEntityType());
            assertNotNull(address.getLatitude());
            assertNotNull(address.getLongitude());
            assertNotNull(address.getOverrideflag());
            assertNotNull(address.getPostalCode());
            assertNotNull(address.getResult());
            assertNotNull(address.getStateProvince());
            assertNotNull(address.getAvsAddressId());
            assertNotNull(address.getRecipientId());

            for (AVSAddressScoreVO score : scores)
            {
                assertNotNull(score.getReason());
                assertNotNull(score.getScore());
                assertNotNull(score.getAvsAddressId());
                assertNotNull(score.getAvsScoreId());
            }
        } 
        else if (detail.getExternalOrderNumber().startsWith("CMCHIL3"))
        {
            // order with avs address with no address and no score
            // order with full avs address and scores
            AVSAddressVO address = detail.getAvsAddress();
            assertNotNull(address);
            assertTrue(address.getScores().size() == 0);
            // verify address elements are not null - don't worry about actual values
            assertNull(address.getAddress());
            assertNotNull(address.getAvsPerformed());
            assertNull(address.getCity());
            assertNull(address.getCountry());
            assertNull(address.getEntityType());
            assertNull(address.getLatitude());
            assertNull(address.getLongitude());
            assertNotNull(address.getOverrideflag());
            assertNull(address.getPostalCode());
            assertNotNull(address.getResult());
            assertNull(address.getStateProvince());
            assertNotNull(address.getAvsAddressId());
            assertNotNull(address.getRecipientId());
        }
        else if (detail.getExternalOrderNumber().startsWith("CMCHIL4"))
        {
            // order with partial avs address and no score
            AVSAddressVO address = detail.getAvsAddress();
            assertNotNull(address);
            assertTrue(address.getScores().size() == 0);
        }
        else
            fail("didn't find expected orders");
    }
 }

  /** 
    * Test updating an existing order from the Scrub Database
    * using the ScrubMapperDAO. 
    **/ 
    public void testUpdateExistingOrder() throws Exception{ 
    System.out.println("testing update existing order from db");
          
    //get a connection
    ConnectionTest conn = new ConnectionTest();
    Connection connection = conn.getConnection();
    
    //check to see that the connection is not null
    assertNotNull(connection);

    String guid = configUtil.getProperty(UTILITIES_CONFIG_FILE, "existing_order_guid");
    
    ScrubMapperDAO scrubMapperDAO = new ScrubMapperDAO(connection);
    //first retrieve an existing order from Scrub
    order = scrubMapperDAO.mapOrderFromDB(guid);
   
    //Check if the result is the same as expected
    //A result of an order object that is not null is expected
    //if the order was retrieved from the database correctly
    assertNotNull(order);

    //Modify some data and call the update method in the 
    //scrubMapperDAO
    Collection coBrands = order.getCoBrand();            
    CoBrandVO coBrand = null;
    Iterator iterator = null;
    Iterator it = null;
    String key = null;
    String value = null;
                
    iterator = coBrands.iterator();
    System.out.println("CoBrand information before update: ");
                            
    for (int i = 0; iterator.hasNext(); i++) 
    {
        coBrand = (CoBrandVO)iterator.next();
        System.out.println("Id: " + coBrand.getCoBrandId());
        System.out.println("Name: " + coBrand.getInfoName());
        System.out.println("Data: " + coBrand.getInfoData());
    }

    List coBrandsList = new ArrayList();
    coBrand = new CoBrandVO();
    coBrand.setGuid(guid);

    //getCoBrand info to be updated from config file
    Map map = configUtil.getProperties(UTILITIES_CONFIG_FILE);
    it = map.keySet().iterator();
    while(it.hasNext())
    {
        key = (String)it.next();
        value = (String)map.get(key);

        if(key.startsWith("co_brand_name"))
        {
            coBrand.setInfoName(value);
        }
        else if(key.startsWith("co_brand_data"))
        {
            coBrand.setInfoData(value);
        }
  
    }
  
    coBrandsList.add(coBrand);
    order.setCoBrand(coBrandsList);

    mappedSuccessfully = scrubMapperDAO.updateOrder(order);
    
    //Check if the result is the same as expected
    //A result of false is expected because the test
    //is trying to insert a duplicate record into
    //the database
    assertEquals("testUpdateExistingOrder", true, mappedSuccessfully);  
    
    coBrands = order.getCoBrand();            
    coBrand = null;
    iterator = null;
                
    iterator = coBrands.iterator();
    System.out.println("CoBrand information after update: ");
                            
    for (int i = 0; iterator.hasNext(); i++) 
    {
        coBrand = (CoBrandVO)iterator.next();
        System.out.println("Id: " + coBrand.getCoBrandId());
        System.out.println("Name: " + coBrand.getInfoName());
        System.out.println("Data: " + coBrand.getInfoData());
    }
    
 } 

    public static TestSuite suite()
    {
      TestSuite suite = new TestSuite();
      try 
      {
        suite.addTest(new ScrubMapperDAOTest("testMapGoodOrderToDB"));
        suite.addTest(new ScrubMapperDAOTest("testMapDuplicateOrderToDB"));
        suite.addTest(new ScrubMapperDAOTest("testMapExistingOrderFromDB"));
        suite.addTest(new ScrubMapperDAOTest("testUpdateExistingOrder"));
      } catch (Exception e) {
          e.printStackTrace();
      }

      return suite;
    } 

    public static void main(String args[])
{
  junit.textui.TestRunner.run( suite() );
}

}