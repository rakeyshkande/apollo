package com.ftd.osp.utilities.order.test.order;

import java.sql.*;
import java.sql.Connection;
import com.ftd.osp.utilities.ConfigurationUtil;

/**
 * Test class for getting a connection to the Scrub database 
 *
 * @author Rose Lazuk
 */

public class ConnectionTest 
{
  private final static String UTILITIES_CONFIG_FILE = "utilities_config.xml";
  private static ConfigurationUtil configUtil;

  public ConnectionTest() throws Exception
  {
    //configUtil = ConfigurationUtil.getInstance();
  }

    /**
      * 
      * @description Gets a connection to the database. 
      * @exception Throws a general Exception
      * @return returns a new connection
      */
     public static Connection getConnection() throws Exception
     {
          if(configUtil == null)
              configUtil = ConfigurationUtil.getInstance();
     
         Connection conn = null;
         String driver_ = "oracle.jdbc.driver.OracleDriver";
         String database_ = configUtil.getProperty(UTILITIES_CONFIG_FILE, "databaseConnection");
         String user_ = configUtil.getProperty(UTILITIES_CONFIG_FILE, "databaseUsername");
         String password_ = configUtil.getProperty(UTILITIES_CONFIG_FILE, "databasePassword");
 
         try
         {
             Class.forName(driver_);
             conn = DriverManager.getConnection(database_, user_, password_);
         }
         catch (Exception e)
         {
             throw e;
         }
 
         return conn;
     }

}