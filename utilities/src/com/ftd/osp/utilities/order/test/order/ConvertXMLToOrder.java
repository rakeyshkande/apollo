package com.ftd.osp.utilities.order.test.order;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.GUID.GUIDGenerator;
import com.ftd.osp.utilities.order.vo.*;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Test class that takes the fields from a test xml file
 * and populates the value objects associated with an order.
 *
 * @author Rose Lazuk
 */

public class ConvertXMLToOrder 
{
  private Document orderDocument = null;
  private OrderVO order = null;
  private String orderXMLString = null;
  private String GUID = null;
  private String origin = null;
  private long timeStamp = 0; 
  private final static String STATUS = "1001";
  private final static String ITEM_STATUS = "2001";
  
  /**
   * Constructor for ConvertXMLToOrder
   * @param orderXMLString String contains order that was sent in
   * xml transmission
   */
  public ConvertXMLToOrder(String orderXMLString)
  {
    this.orderXMLString = orderXMLString;
    this.order = new OrderVO();
    this.GUID = GUIDGenerator.getInstance().getGUID();
    this.timeStamp = System.currentTimeMillis();
  }

/**
 * Create an order
 * 
 * @exception Exception 
 * @return Order 
 */
  public OrderVO createOrder() throws Exception
  {
    try
    {
        orderDocument = DOMUtil.getDocument(orderXMLString);

    }
    catch(Exception ex)
    {
        throw new Exception(ex.toString()); 
    }
   
    
    order.setGUID(this.GUID);
    order.setTimeStamp(this.timeStamp);
    this.mapXMLtoOrder(order);
    
    return this.order;
  }

/**
 * Map the xml file that was sent in the transmission to the
 * Order Value Object
 * 
 * @param order OrderVO value object that holds the whole order
 */
  private void mapXMLtoOrder(OrderVO order) 
  {
    Node xmlNode = null;
    String nodeName = "";
    String nodeValue = "";
    
    try
    {
      Document orderXMLDoc = (Document) orderDocument;
      xmlNode = DOMUtil.selectSingleNode((Node)orderXMLDoc, "/order/header");

      nodeValue = this.getSingleNode(xmlNode, "master-order-number/text()");
      order.setMasterOrderNumber(nodeValue);
   
      nodeValue = this.getSingleNode(xmlNode, "/order/header/source-code/text()");
      order.setSourceCode(nodeValue);
       
      nodeValue = this.getSingleNode(xmlNode, "/order/header/origin/text()");
      order.setOrderOrigin(nodeValue);
      origin = nodeValue.toLowerCase();

      nodeValue = this.getSingleNode(xmlNode, "/order/header/order-count/text()");
      order.setProductsTotal(nodeValue);

      nodeValue = this.getSingleNode(xmlNode, "/order/header/order-amount/text()");
      order.setOrderTotal(nodeValue);

      nodeValue = this.getSingleNode(xmlNode, "/order/header/transaction-date/text()");
      order.setOrderDate(nodeValue);

      nodeValue = this.getSingleNode(xmlNode, "/order/header/socket-timestamp/text()");
      order.setSocketTimestamp(nodeValue);

      List buyer = this.getBuyer(xmlNode);
      order.setBuyer(buyer);
      
      nodeValue = this.getSingleNode(xmlNode, "/order/header/ariba-buyer-cookie/text()");
      order.setAribaBuyerCookie(nodeValue);
   
      nodeValue = this.getSingleNode(xmlNode, "/order/header/ariba-asn-buyer-number/text()");
      order.setAribaBuyerAsnNumber(nodeValue);

      nodeValue = this.getSingleNode(xmlNode, "/order/header/ariba-payload/text()");
      order.setAribaPayload(nodeValue);

      List payments = this.getPayments(xmlNode);
      order.setPayments(payments);

      List lCoBrands = this.getCoBrands(xmlNode);
      order.setCoBrand(lCoBrands);
    
      List lOrderDetails = this.getOrderDetails(xmlNode);
      order.setOrderDetail(lOrderDetails);

      //add bulk order specific fields to value objects
      if(origin.equals("bulk"))
      {
        List lOrderContactInfo = this.getOrderContactInfo(xmlNode);
        order.setOrderContactInfo(lOrderContactInfo);
      }

      order.setStatus(STATUS);
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
  
  }

  /**
  * Get buyer information that's associated with the order.
  * 
  * @param xmlNode Node 
  * @return List of buyer value objects
  */
 private List getBuyer(Node xmlNode)
 {
    List lBuyer = new ArrayList();
    BuyerVO buyer = new BuyerVO();
    String nodeValue = "";
   
    try{

      nodeValue = this.getSingleNode(xmlNode, "/order/header/buyer-first-name/text()");
      buyer.setFirstName(nodeValue);
      
      nodeValue = this.getSingleNode(xmlNode, "/order/header/buyer-last-name/text()");
      buyer.setLastName(nodeValue);

      List buyerAddress = this.getBuyerAddress(xmlNode);
      buyer.setBuyerAddresses(buyerAddress);

      List buyerPhones = this.getBuyerPhones(xmlNode);
      buyer.setBuyerPhones(buyerPhones);

      List buyerEmail = this.getBuyerEmail(xmlNode);
      buyer.setBuyerEmails(buyerEmail);

      buyer.setStatus(STATUS);

      lBuyer.add(buyer);
      
     }
     catch(Exception ex)
     {
      ex.printStackTrace();
     }
   
     return lBuyer;
    
 }

 /**
  * Get buyer address information associated with the order.
  * 
  * @param xmlNode Node 
  * @return List of buyer address value objects
  */
 private List getBuyerAddress(Node xmlNode)
 {
    List lBuyerAddress = new ArrayList();
    BuyerAddressesVO buyerAddress = new BuyerAddressesVO();
    String nodeValue = "";
    
    try{

      nodeValue = this.getSingleNode(xmlNode, "/order/header/buyer-address1/text()");
      buyerAddress.setAddressLine1(nodeValue);
      
      nodeValue = this.getSingleNode(xmlNode, "/order/header/buyer-address2/text()");
      buyerAddress.setAddressLine2(nodeValue);
      
      nodeValue = this.getSingleNode(xmlNode, "/order/header/buyer-city/text()");
      buyerAddress.setCity(nodeValue);
      
      nodeValue = this.getSingleNode(xmlNode, "/order/header/buyer-state/text()");
      buyerAddress.setStateProv(nodeValue);
      
      nodeValue = this.getSingleNode(xmlNode, "/order/header/buyer-postal-code/text()");
      buyerAddress.setPostalCode(nodeValue);
      
      nodeValue = this.getSingleNode(xmlNode, "/order/header/buyer-country/text()");
      buyerAddress.setCountry(nodeValue);

      //add bulk order specific fields to value objects
      if(origin.equals("bulk"))
      {
        nodeValue = this.getSingleNode(xmlNode, "/order/header/buyer-business/text()");
        if(!nodeValue.equals(""))
          {
            buyerAddress.setAddressEtc(nodeValue);
            //set address type to 'Business' if origin = bulk
            //and buyer-business is not null
            buyerAddress.setAddressType("Business");
          }
        else
        {
            //for all other cases, set address type to 'Other'
            buyerAddress.setAddressType("Other"); 
        }
      }
      else
      {
          //for all other cases, set address type to 'Other'
          buyerAddress.setAddressType("Other"); 
      }
      
      lBuyerAddress.add(buyerAddress);
      
     }
     catch(Exception ex)
     {
      ex.printStackTrace();
     }
   
     return lBuyerAddress;
    
 }

/**
  * Get buyer phone information associated with the order.
  * 
  * @param xmlNode Node 
  * @return List of buyer phone value objects
  */
 private List getBuyerPhones(Node xmlNode)
 {
    List lBuyerPhones = new ArrayList();
    BuyerPhonesVO buyerPhones = null;
    String nodeValue = "";
    
     try{

      buyerPhones = new BuyerPhonesVO();
      ConfigurationUtil configUtil = ConfigurationUtil.getInstance();

      //set work phone number
      nodeValue = this.getSingleNode(xmlNode, "/order/header/buyer-primary-phone/text()");
      if(nodeValue==null || nodeValue == ""){
    	  nodeValue = this.getSingleNode(xmlNode, "/order/header/buyer-daytime-phone/text()");
      }
      buyerPhones.setPhoneNumber(nodeValue);

      nodeValue = this.getSingleNode(xmlNode, "/order/header/buyer-primary-phone-ext/text()");
      if(nodeValue==null || nodeValue == ""){
    	  nodeValue = this.getSingleNode(xmlNode, "/order/header/buyer-work-ext/text()");
      }
      buyerPhones.setExtension(nodeValue);
      
      buyerPhones.setPhoneType("Work");
      
      lBuyerPhones.add(buyerPhones);

      //set home phone number
      buyerPhones = new BuyerPhonesVO();

      nodeValue = this.getSingleNode(xmlNode, "/order/header/buyer-secondary-phone/text()");
      if(nodeValue==null || nodeValue == ""){
    	  nodeValue = this.getSingleNode(xmlNode, "/order/header/buyer-evening-phone/text()");
      }
      buyerPhones.setPhoneNumber(nodeValue);

      buyerPhones.setPhoneType("Home");

      lBuyerPhones.add(buyerPhones);
      
      
      //set fax number
      buyerPhones = new BuyerPhonesVO();

      nodeValue = this.getSingleNode(xmlNode, "/order/header/buyer-fax/text()");
      buyerPhones.setPhoneNumber(nodeValue);

      buyerPhones.setPhoneType("Fax");

      lBuyerPhones.add(buyerPhones);
      
     }
     catch(Exception ex)
     {
      ex.printStackTrace();
     }
   
     return lBuyerPhones;
 }

 /**
  * Get buyer email information associated with the order.
  * 
  * @param xmlNode Node 
  * @return List of buyer email value objects
  */
 private List getBuyerEmail(Node xmlNode)
 {
    List lBuyerEmail = new ArrayList();
    BuyerEmailsVO buyerEmail = new BuyerEmailsVO();
    String nodeValue = "";
    
    try{
      buyerEmail.setPrimary("Y");

      nodeValue = this.getSingleNode(xmlNode, "/order/header/buyer-email-address/text()");
      buyerEmail.setEmail(nodeValue);

      nodeValue = this.getSingleNode(xmlNode, "/order/header/news-letter-flag/text()");
      buyerEmail.setNewsletter(nodeValue);
                  
      lBuyerEmail.add(buyerEmail);
      
     }
     catch(Exception ex)
     {
      ex.printStackTrace();
     }
   
     return lBuyerEmail;
 }

 /**
  * Get credit card information associated with the order.
  * 
  * @param xmlNode Node 
  * @return List of credit card value objects
  */
 private List getCreditCard(Node xmlNode)
 {
    List lCreditCard = new ArrayList();
    CreditCardsVO creditCard = null;
    String nodeValue = "";
    boolean haveCreditCardInfo = false;
    
    try{

      creditCard = new CreditCardsVO();

      nodeValue = this.getSingleNode(xmlNode, "/order/header/cc-type/text()");
      if(!nodeValue.equals(""))
      {
        creditCard.setCCType(nodeValue);
        haveCreditCardInfo = true;
      }
      nodeValue = this.getSingleNode(xmlNode, "/order/header/cc-number/text()");
      if(!nodeValue.equals(""))
      {
        creditCard.setCCNumber(nodeValue);
        haveCreditCardInfo = true;
      }
      nodeValue = this.getSingleNode(xmlNode, "/order/header/cc-exp-date/text()");
      if(!nodeValue.equals(""))
      {
        creditCard.setCCExpiration(nodeValue);
        haveCreditCardInfo = true;
      }

      if(haveCreditCardInfo)
        lCreditCard.add(creditCard);
      
     }
     catch(Exception ex)
     {
      ex.printStackTrace();
     }
   
     return lCreditCard;
 }

 /**
  * Get payment information associated with the order.
  * 
  * @param xmlNode Node 
  * @return List of payment value objects
  */
 private List getPayments(Node xmlNode)
 {
    List lPayment = new ArrayList();
    PaymentsVO payment = null;
    String nodeValue = "";
    NodeList number = null;
    NodeList amount = null;
    Node itemNode = null;
    boolean haveCreditCardInfo = false;
    boolean haveCreditCardAuthInfo = false;
    boolean haveGiftCertificateInfo = false;
    boolean haveAafesInfo = false;
    
    
    try{

      //set credit card information
      //check to see if there is any information coming for
      //credit cards, gift certificates or aafes.  If there is no
      //information for those three, then set payment type to invoice
      //"IN"
      payment = new PaymentsVO();

      ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
      
      List creditCard = this.getCreditCard(xmlNode);
      payment.setCreditCards(creditCard);

      if(creditCard.size() != 0)
      {
        haveCreditCardInfo = true;
        nodeValue = this.getSingleNode(xmlNode, "/order/header/cc-type/text()");
        payment.setPaymentsType(nodeValue);
      }
            
      nodeValue = this.getSingleNode(xmlNode, "/order/header/cc-approval-code/text()");
      if(!nodeValue.equals(""))
      {
        payment.setAuthNumber(nodeValue);
        haveCreditCardAuthInfo = true;
      }

      nodeValue = this.getSingleNode(xmlNode, "/order/header/cc-approval-amt/text()");
      if(!nodeValue.equals(""))
      {
        payment.setAmount(nodeValue);
        haveCreditCardAuthInfo = true;
      }

      nodeValue = this.getSingleNode(xmlNode, "/order/header/cc-approval-verbage/text()");
      if(!nodeValue.equals(""))
      {
        payment.setAuthResult(nodeValue);
        haveCreditCardAuthInfo = true;
      }

      nodeValue = this.getSingleNode(xmlNode, "/order/header/cc-avs-result/text()");
      if(!nodeValue.equals(""))
      {
        payment.setAvsCode(nodeValue);
        haveCreditCardAuthInfo = true;
      }
      
      nodeValue = this.getSingleNode(xmlNode, "/order/header/cc-acq-data/text()");
      if(!nodeValue.equals(""))
      {
        payment.setAcqReferenceNumber(nodeValue);
        haveCreditCardAuthInfo = true;
      }     

      if( haveCreditCardInfo || haveCreditCardAuthInfo )
        lPayment.add(payment);

      //set aafes ticket information
      payment = new PaymentsVO();

      nodeValue = this.getSingleNode(xmlNode, "/order/header/aafes-ticket-number/text()");
      if(!nodeValue.equals(""))
      {
        payment.setAafesTicket(nodeValue);
        payment.setPaymentsType("MS");
                  
        lPayment.add(payment);
        
        haveAafesInfo = true;
      }

      //set gift certificate information
      itemNode =  DOMUtil.selectSingleNode(xmlNode, "/order/header/gift-certificates/gift-certificate/number/text()");
      if(itemNode != null)
       {
          number = DOMUtil.selectNodes(xmlNode, "/order/header/gift-certificates/gift-certificate/number/text()");
          amount = DOMUtil.selectNodes(xmlNode, "/order/header/gift-certificates/gift-certificate/amount/text()");
          haveGiftCertificateInfo = true;
          
          for(int i=0; i<number.getLength(); i++)
          {
            payment = new PaymentsVO();
            payment.setPaymentsType("GC");
            payment.setGiftCertificateId(number.item(i).getNodeValue());
            payment.setAmount(amount.item(i).getNodeValue()); 
            lPayment.add(payment);
          }
       }
       if(!haveCreditCardInfo && !haveCreditCardAuthInfo
          && !haveGiftCertificateInfo &!haveAafesInfo)
       {
          payment = new PaymentsVO();
          payment.setPaymentsType("IN");
          lPayment.add(payment);
       }
          
     }
    catch(Exception ex)
     {
      ex.printStackTrace();
     }
   
     return lPayment;
 }

 /**
 * Get the value from the xmlNode 
 * 
 * @param xmlNode Node
 * @param nodeName String
 * @return String
 */
private String getSingleNode(Node xmlNode, String nodeName)
{
    Node itemNode = null;
    String nodeValue = "";
    
    try
    {
      itemNode =  DOMUtil.selectSingleNode(xmlNode, nodeName);
      if(itemNode != null)
      {
        itemNode = DOMUtil.selectSingleNode(xmlNode, nodeName);
        nodeValue = itemNode.getNodeValue();
      } 

    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
 
  return nodeValue; 
}

 /**
   * Get the cobrands associated with an order
   * 
   * @param xmlNode Node  
   * @return List of CoBrand value objects
   */
  private List getCoBrands(Node xmlNode)
  {
    CoBrandVO coBrand = null;
    List lCoBrands = new ArrayList();
    NodeList name = null;
    NodeList data = null;
    Node itemNode = null;

    try
     {
       itemNode =  DOMUtil.selectSingleNode(xmlNode, "/order/header/co-brands/co-brand/name/text()");
       if(itemNode != null)
       {
          name = DOMUtil.selectNodes(xmlNode, "/order/header/co-brands/co-brand/name/text()");
          data = DOMUtil.selectNodes(xmlNode, "/order/header/co-brands/co-brand/data/text()");
        
          for(int i=0; i<name.getLength(); i++)
          {
            coBrand = new CoBrandVO();
            coBrand.setInfoName(name.item(i).getNodeValue());
            coBrand.setInfoData(data.item(i).getNodeValue()); 
            lCoBrands.add(coBrand);
          }
       }
     }
     catch(Exception ex)
     {
      ex.printStackTrace();
     }
   
     return lCoBrands;
  }

  /**
  * Get Add Ons that are associated with the order details.
  * 
  * @param xmlNode Node 
  * @return List of add on value objects
  */
 private List getAddOns(Node xmlNode)
 {
    AddOnsVO addOn = null;
    List lAddOns = new ArrayList();
    NodeList id = null;
    NodeList quantity = null;
    Node itemNode = null;

    try
     {
       itemNode =  DOMUtil.selectSingleNode(xmlNode, "add-ons/add-on/id/text()");
       if(itemNode != null)
       {
          id = DOMUtil.selectNodes(xmlNode, "add-ons/add-on/id/text()");
          quantity = DOMUtil.selectNodes(xmlNode, "add-ons/add-on/quantity/text()");
        
          for(int i=0; i<id.getLength(); i++)
          {
            addOn = new AddOnsVO();
            addOn.setAddOnCode(id.item(i).getNodeValue());
            addOn.setAddOnQuantity(quantity.item(i).getNodeValue());
            lAddOns.add(addOn);
          }
       }
     }
     catch(Exception ex)
     {
      ex.printStackTrace();
     }
   
     return lAddOns;
    
 }

  /**
  * Get any Order Detail Extensions that are associated with the order details.
  * 
  * @param xmlNode Node 
  * @return List of order detail extensions value objects
  */
  private List getOrderDetailExtensions(Node xmlNode)
  {
    OrderDetailExtensionsVO ext = null;
    List lext = new ArrayList();
    NodeList name = null;
    NodeList data = null;
    Node itemNode = null;

    try
     {
       itemNode =  DOMUtil.selectSingleNode(xmlNode, "item-extensions/extension/name/text()");
       if(itemNode != null)
       {
          name  = DOMUtil.selectNodes(xmlNode, "item-extensions/extension/name/text()");
          data = DOMUtil.selectNodes(xmlNode, "item-extensions/extension/data/text()");
        
          for(int i=0; i<name.getLength(); i++)
          {
            ext = new OrderDetailExtensionsVO();
            ext.setInfoName(name.item(i).getNodeValue());
            ext.setInfoData(data.item(i).getNodeValue()); 
            lext.add(ext);
          }
       }
     }
     catch(Exception ex)
     {
      ex.printStackTrace();
     }
   
     return lext;
  }

 /**
  * Get Recipients that are associated with the 
  * order details.
  * 
  * @param xmlNode Node 
  * @return List of recipients value objects
  */
 private List getRecipients(Node xmlNode)
 {
    List lRecipients = new ArrayList();
    RecipientsVO recipients = null;
    String nodeValue = "";

    try
     {
        recipients = new RecipientsVO();

        nodeValue = this.getSingleNode(xmlNode, "recip-first-name/text()");
        recipients.setFirstName(nodeValue); 

        nodeValue = this.getSingleNode(xmlNode, "recip-last-name/text()");
        recipients.setLastName(nodeValue);

        List lRecipientsAddresses = this.getRecipientAddresses(xmlNode);
        recipients.setRecipientAddresses(lRecipientsAddresses);

        List lRecipientPhones = this.getRecipientPhone(xmlNode);
        recipients.setRecipientPhones(lRecipientPhones);

        recipients.setStatus(STATUS);

        lRecipients.add(recipients);
     }
     catch(Exception ex)
     {
      ex.printStackTrace();
     }
   
     return lRecipients;
 }

 /**
  * Get Recipient Addresses that are associated with the 
  * order details.
  * 
  * @param xmlNode Node 
  * @return List of recipient addresses value objects
  */
 private List getRecipientAddresses(Node xmlNode)
 {
    List lRecipientAddresses = new ArrayList();
    RecipientAddressesVO recipientAddresses = null;
    String nodeValue = "";

    try
     {
        recipientAddresses = new RecipientAddressesVO(); 
       
        nodeValue = this.getSingleNode(xmlNode, "recip-address1/text()");
        recipientAddresses.setAddressLine1(nodeValue);

        nodeValue = this.getSingleNode(xmlNode, "recip-address2/text()");
        recipientAddresses.setAddressLine2(nodeValue);

        nodeValue = this.getSingleNode(xmlNode, "recip-city/text()");
        recipientAddresses.setCity(nodeValue);

        nodeValue = this.getSingleNode(xmlNode, "recip-state/text()");
        recipientAddresses.setStateProvince(nodeValue);

        nodeValue = this.getSingleNode(xmlNode, "recip-postal-code/text()");
        recipientAddresses.setPostalCode(nodeValue);

        nodeValue = this.getSingleNode(xmlNode, "recip-country/text()");
        recipientAddresses.setCountry(nodeValue);

        nodeValue = this.getSingleNode(xmlNode, "recip-international/text()");
        recipientAddresses.setInternational(nodeValue);

        nodeValue = this.getSingleNode(xmlNode, "ship-to-type/text()");
        recipientAddresses.setAddressType(nodeValue);

        nodeValue = this.getSingleNode(xmlNode, "ship-to-type-name/text()");
        recipientAddresses.setName(nodeValue);

        nodeValue = this.getSingleNode(xmlNode, "ship-to-type-info/text()");
        recipientAddresses.setInfo(nodeValue);

        lRecipientAddresses.add(recipientAddresses);
                
     }
     catch(Exception ex)
     {
      ex.printStackTrace();
     }
   
     return lRecipientAddresses;
    
 }


 /**
  * Get Recipient Phones that are associated with the 
  * order details.
  * 
  * @param xmlNode Node 
  * @return List of recipient phone value objects
  */
 private List getRecipientPhone(Node xmlNode)
 {
    List lRecipientPhone = new ArrayList();
    RecipientPhonesVO recipientPhones = null;
    String nodeValue = "";

    try
     {
        recipientPhones = new RecipientPhonesVO(); 

        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
        recipientPhones.setPhoneType("Work");

        nodeValue = this.getSingleNode(xmlNode, "recip-phone/text()");
        recipientPhones.setPhoneNumber(nodeValue);

        nodeValue = this.getSingleNode(xmlNode, "recip-phone-ext/text()");
        recipientPhones.setExtension(nodeValue); 

        lRecipientPhone.add(recipientPhones);        
     }
     catch(Exception ex)
     {
      ex.printStackTrace();
     }
   
     return lRecipientPhone;
    
 }

 /**
 * Populate and return List of Order Detail Value Objects that 
 * are associated with the order.
 * @param xmlNode Node 
 * @return List
 */
private List getOrderDetails(Node xmlNode)
{
     OrderDetailsVO orderDetail = null;
     List lOrderDetails = new ArrayList();
     String nodeValue = "";
     String nodeName = "";
     Node itemNode = null;
     
     try{
          
          Document orderXMLDoc = (Document) orderDocument;
          NodeList nl = DOMUtil.selectNodes(orderXMLDoc, "/order/items/item");
                   
          for(int i=0; i<nl.getLength(); i++) 
          {
            orderDetail = new OrderDetailsVO();
            xmlNode = (Node) nl.item(i);

            String lineNumber = "";
            
            orderDetail.setLineNumber(lineNumber.valueOf(i+1));

            nodeValue = this.getSingleNode(xmlNode, "order-number/text()");
            orderDetail.setExternalOrderNumber(nodeValue); 

            nodeValue = this.getSingleNode(xmlNode, "order-total/text()");
            orderDetail.setExternalOrderTotal(nodeValue); 

            nodeValue = this.getSingleNode(xmlNode, "tax-amount/text()");
            orderDetail.setTaxAmount(nodeValue); 

            nodeValue = this.getSingleNode(xmlNode, "service-fee/text()");
            orderDetail.setServiceFeeAmount(nodeValue); 

            //add extra-shipping-fee and drop-ship-charges together
            //and set to shippingFeeAmount
            BigDecimal extraShippingFee = new BigDecimal("0");
            BigDecimal dropShipCharges = new BigDecimal("0");
            BigDecimal shippingFeeAmount = new BigDecimal("0");
            
            nodeValue = this.getSingleNode(xmlNode, "extra-shipping-fee/text()");
            if(!nodeValue.equals(""))
            {
              extraShippingFee = new BigDecimal(nodeValue);
            }
            nodeValue = this.getSingleNode(xmlNode, "drop-ship-charges/text()");
            if(!nodeValue.equals(""))
            {
              dropShipCharges = new BigDecimal(nodeValue);
            }
            shippingFeeAmount = extraShippingFee.add(dropShipCharges);
                        
            orderDetail.setShippingFeeAmount(shippingFeeAmount.toString());

            nodeValue = this.getSingleNode(xmlNode, "productid/text()");
            orderDetail.setProductId(nodeValue); 

            nodeValue = this.getSingleNode(xmlNode, "product-price/text()");
            orderDetail.setProductsAmount(nodeValue); 

            nodeValue = this.getSingleNode(xmlNode, "first-color-choice/text()");
            orderDetail.setColorFirstChoice(nodeValue); 

            nodeValue = this.getSingleNode(xmlNode, "second-color-choice/text()");
            orderDetail.setColorSecondChoice(nodeValue); 

            nodeValue = this.getSingleNode(xmlNode, "size/text()");
            orderDetail.setSizeChoice(nodeValue); 

            List lAddOns = this.getAddOns(xmlNode);
            orderDetail.setAddOns(lAddOns);
            
            List lRecipients = this.getRecipients(xmlNode);
            orderDetail.setRecipients(lRecipients);

            nodeValue = this.getSingleNode(xmlNode, "occassion/text()");
            orderDetail.setOccassionId(nodeValue);

            nodeValue = this.getSingleNode(xmlNode, "delivery-date/text()");
            orderDetail.setDeliveryDate(nodeValue);

            nodeValue = this.getSingleNode(xmlNode, "second-delivery-date/text()");
            orderDetail.setDeliveryDateRangeEnd(nodeValue);

            nodeValue = this.getSingleNode(xmlNode, "card-message/text()");
            orderDetail.setCardMessage(nodeValue);

            nodeValue = this.getSingleNode(xmlNode, "card-signature/text()");
            orderDetail.setCardSignature(nodeValue);

            nodeValue = this.getSingleNode(xmlNode, "special-instructions/text()");
            orderDetail.setSpecialInstructions(nodeValue);
            
            nodeValue = this.getSingleNode(xmlNode, "legacy_id/text()");
            orderDetail.setLegacyId(nodeValue);

            nodeValue = this.getSingleNode(xmlNode, "shipping-method/text()");
            orderDetail.setShipMethod(nodeValue);

            nodeValue = this.getSingleNode(xmlNode, "lmg-email-address/text()");
            orderDetail.setLastMinuteGiftEmail(nodeValue);

            nodeValue = this.getSingleNode(xmlNode, "lmg-email-signature/text()");
            orderDetail.setLastMinuteGiftSignature(nodeValue);

            nodeValue = this.getSingleNode(xmlNode, "sender-release-flag/text()");
            orderDetail.setSenderInfoRelease(nodeValue);

            nodeValue = this.getSingleNode(xmlNode, "ariba-po-number/text()");
            orderDetail.setAribaPoNumber(nodeValue);

            nodeValue = this.getSingleNode(xmlNode, "ariba-cost-center/text()");
            orderDetail.setAribaCostCenter(nodeValue);

            nodeValue = this.getSingleNode(xmlNode, "ariba-ams-project-code/text()");
            orderDetail.setAribaAmsProjectCode(nodeValue);

            nodeValue = this.getSingleNode(xmlNode, "ariba-unspsc-code/text()");
            orderDetail.setAribaUnspscCode(nodeValue);

            nodeValue = this.getSingleNode(xmlNode, "product-substitution-acknowledgement/text()");
            orderDetail.setSubstituteAcknowledgement(nodeValue);

            List lExtensions = this.getOrderDetailExtensions(xmlNode);
            orderDetail.setOrderDetailExtensions(lExtensions);

            //add bulk order specific fields to value objects
            if(origin.equals("bulk"));
            {
              nodeValue = this.getSingleNode(xmlNode, "florist/text()");
              orderDetail.setFloristNumber(nodeValue);
            }

            orderDetail.setStatus(ITEM_STATUS);
            
            lOrderDetails.add(orderDetail);
            
          }
       }
     catch(Exception ex)
     {
      ex.printStackTrace();
     }
     
return lOrderDetails;
}

 /**
  * Get contact information that's associated with the order
  * for bulk orders only.
  * 
  * @param xmlNode Node 
  * @return List of order contact info value objects
  */
 private List getOrderContactInfo(Node xmlNode)
 {
    List lOrderContactInfo = new ArrayList();
    OrderContactInfoVO orderContactInfo = new OrderContactInfoVO();
    String nodeValue = "";
   
    try{

        nodeValue = this.getSingleNode(xmlNode, "/order/header/contact-first-name/text()");
        orderContactInfo.setFirstName(nodeValue);

        nodeValue = this.getSingleNode(xmlNode, "/order/header/contact-last-name/text()");
        orderContactInfo.setLastName(nodeValue);

        nodeValue = this.getSingleNode(xmlNode, "/order/header/contact-phone/text()");
        orderContactInfo.setPhone(nodeValue);

        nodeValue = this.getSingleNode(xmlNode, "/order/header/contact-ext/text()");
        orderContactInfo.setExt(nodeValue);

        nodeValue = this.getSingleNode(xmlNode, "/order/header/contact-email-address/text()");
        orderContactInfo.setEmail(nodeValue);

        lOrderContactInfo.add(orderContactInfo);
      
     }
     catch(Exception ex)
     {
      ex.printStackTrace();
     }
   
     return lOrderContactInfo;
    
 }

  
}
