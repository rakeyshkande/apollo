package com.ftd.osp.utilities.order.test.order;

import com.ftd.osp.utilities.order.*;
import com.ftd.osp.utilities.order.vo.*;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.dataaccess.util.*;

import java.io.*;

import java.sql.*;

import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.w3c.dom.*;

/**
 * Test class for XML transformation of order for email confirmation 
 *
 * @author Doug Johnson
 */
public class OrderEmailXAOTest  extends TestCase
{
    OrderEmailXAO orderEmailXAO = null;
    Connection connection = null;
    private boolean mappedSuccessfully = false;
    private String GUID = "";
    private OrderVO order = null;
    
    
    /** 
     * Create a constructor that take a String parameter and passes it 
     * to the super class 
     * @param name String
     **/ 
    public OrderEmailXAOTest(String name,String testGUID) { 
        super(name); 
        try 
        {
	    GUID = testGUID;
        } catch (Exception ex) 
        {
          ex.printStackTrace();
        } 
    }

    /**
     * Method for setting up initialization parameters
     */
    protected void setUp()
    { 
       
    }

   /** 
    * Test converting an order to XML for email confirmation
    * A boolean value of false indicates that there was
    * an error converting the order.
    **/ 
    public void testOrderToEmail() throws Exception
    { 
        System.out.println("testing map order for email confirmation");
          
        connection = this.getConnection();

        //get a connection
        //check to see that the connection is not null
        assertNotNull(connection);

        FRPMapperDAO frpMapperDAO = new FRPMapperDAO(connection);
        order = frpMapperDAO.mapOrderFromDB(GUID);

        if(order.getOrderDetail().size() > 0)
        {

            orderEmailXAO = new OrderEmailXAO();
            Document doc = (Document) orderEmailXAO.generateOrderEmailXML(order);

            if(doc != null)
            {
                System.out.println("*********Order found!*********");
                mappedSuccessfully = true;
            }

            DOMUtil.print(doc, System.out);
        }
        else
        {
          System.out.println("*********Order not found!*********");
        }
        
        //Check if the result is the same as expected
        //A result of true is expected if the order is
        //successfully converted to XML
        assertEquals("testOrderToEmail", true, mappedSuccessfully);
    } 

    private Connection getConnection() 
    {
        Connection conn = null;
        String database_ = "jdbc:oracle:thin:@stheno-dev.ftdi.com:1521:DEV2";
        String user_ = "djohnson";
        String password_ = "djohnson";

        try
        {
            OSPConnectionPool connectionPool = OSPConnectionPool.getInstance();
            conn = connectionPool.getConnection(database_,user_,password_);
        }
        catch(Exception e)
        {
             e.printStackTrace();
        }
       
        return conn;
    }

    public static TestSuite suite()
    {
      TestSuite suite = new TestSuite();
      try 
      {
        //test for success
        suite.addTest(new OrderEmailXAOTest("testOrderToEmail","123"));

        //test for failure
        suite.addTest(new OrderEmailXAOTest("testOrderToEmail","0"));
      } catch (Exception e) {
          e.printStackTrace();
      }

      return suite;
    } 

    public static void main(String args[])
    {
      junit.textui.TestRunner.run( suite() );
    }

}
