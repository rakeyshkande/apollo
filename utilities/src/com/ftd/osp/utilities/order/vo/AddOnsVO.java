package com.ftd.osp.utilities.order.vo;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.List;

/**
 * Add Ons Value Object that maps to ADD_ONS database table
 * This is a sub collection within Order Details value object
 *
 * @author Doug Johnson
 */
public class AddOnsVO extends BaseVO implements Serializable 
{
  private long addOnId;
  private long orderDetailId;
  private String addOnCode;
  private String addOnQuantity;
  private String description;
  private String price;
  private String discountAmount;
  private String addOnType;
  private String text;
  private long addOnHistoryId;
  private Boolean displayPriceFlag;
  private String addOnDiscountAmount;


  public AddOnsVO()
  {
  }

  public long getAddOnId()
  {
    return addOnId;
  }

  public void setAddOnId(long newAddOnId)
  {
    if(valueChanged(addOnId,newAddOnId)){
      setChanged(true);
    addOnId = newAddOnId;
    }  

  }

  public long getOrderDetailId()
  {
    return orderDetailId;
  }

  public void setOrderDetailId(long newOrderDetailId)
  {
    if(valueChanged(orderDetailId,newOrderDetailId)){
      setChanged(true);
      orderDetailId = newOrderDetailId;
    }  

  }

  public String getAddOnCode()
  {
    return addOnCode;
  }

  public void setAddOnCode(String newAddOnCode)
  {
    if(valueChanged(addOnCode,newAddOnCode)){
      setChanged(true);
      addOnCode = newAddOnCode;
    }  

  }

  public String getAddOnQuantity()
  {
    return addOnQuantity;
  }

  public void setAddOnQuantity(String newAddOnQuantity)
  {
    if(valueChanged(addOnQuantity,checkNullInt(newAddOnQuantity))){
      setChanged(true);
      addOnQuantity = checkNullInt(newAddOnQuantity);
    }  
  
  }

  public String toString()
  {
    String lineSep = System.getProperty("line.separator");
    StringBuffer sb = new StringBuffer();
    StringBuffer listSb = new StringBuffer();
    // Get this Class
    Class thisClass = this.getClass();
    sb.append("Object Type = ").append(thisClass.getName()).append(lineSep);
        
    // Use reflection to loop through all the fields
    Field[] fields = thisClass.getDeclaredFields();
    for(int i = 0; i < fields.length; i++)
    {
        try
        {
            if(fields[i].getType().equals(Class.forName("java.lang.String")))
            {
                sb.append(fields[i].getName()).append(" = ").append(fields[i].get(this)).append(lineSep);
            }
            else if(fields[i].getType().equals(Class.forName("java.util.List")))
            {
                List list = (List)fields[i].get(this);
                if(list != null)
                {
                    for (int j = 0; j < list.size(); j++) 
                    {
                        listSb.append(list.get(j).toString());
                    }
                }
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    sb.append(listSb);
        
    return sb.toString();
  } 

  public String getDescription()
  {
    return description;
  }

  public void setDescription(String newDescription)
  {
    if(valueChanged(description,newDescription)){
      setChanged(true);
      description = newDescription;
    }  

  }

  public String getPrice()
  {
    return price;
  }

  public void setPrice(String newPrice)
  {
    if(valueChanged(price,newPrice)){
      setChanged(true);
      price = newPrice;
    }  

  }

  /**
 * @return the discountAmount
 */
public String getDiscountAmount() {
	return discountAmount;
}

/**
 * @param discountAmount the discountAmount to set
 */
public void setDiscountAmount(String discountAmount) {  
	if(valueChanged(this.discountAmount,discountAmount)) {
		setChanged(true);
		this.discountAmount = discountAmount;
	}
}

public String getAddOnType()
  {
    return addOnType;
  }

  public void setAddOnType(String newAddOnType)
  {
    if(valueChanged(addOnType,newAddOnType)){
      setChanged(true);
      addOnType = newAddOnType;
    }  

  }

  public String getText()
  {
    return text;
  }
    
  public void setText(String newText)
  {
    if(valueChanged(text,newText)){
      setChanged(true);
      text = newText;
    }  
  }

  public void setAddOnHistoryId(long newId)
  {
    if(valueChanged(addOnHistoryId,newId)){
      setChanged(true);
      addOnHistoryId = newId;
    }  

  }

  public long getAddOnHistoryId() {
      return addOnHistoryId;
  }

    public Boolean getDisplayPriceFlag()
    {
        return displayPriceFlag;
    }

    public void setDisplayPriceFlag(Boolean displayPriceFlag)
    {
        this.displayPriceFlag = displayPriceFlag;
    }

	
	public String getAddOnDiscountAmount() {
		return addOnDiscountAmount;
	}

	/**
	 * @param addonDiscountAmount the discountAmount to set
	 */
	public void setAddOnDiscountAmount(String addOnDiscountAmount) {  
		if(valueChanged(this.addOnDiscountAmount,addOnDiscountAmount)) {
			setChanged(true);
			this.addOnDiscountAmount = addOnDiscountAmount;
		}
	}
	
}
