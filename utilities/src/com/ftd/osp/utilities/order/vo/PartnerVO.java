package com.ftd.osp.utilities.order.vo;

public class PartnerVO 
{
  private String partnerId = null; 
  private String partnerName = null;
  private String customerInfo = null;
  private String rewardName = null;
  private String idLength = null;
  private String emailExludeFlag = null;
  
  public PartnerVO()
  {}

  public void setPartnerId(String partnerId)
  {
    this.partnerId = partnerId;
  }

  public String getPartnerId()
  {
    return this.partnerId;
  }

  public void setPartnerName(String partnerName)
  {
    this.partnerName = partnerName;
  }

  public String getPartnerName()
  {
    return this.partnerName;
  }

  public void setCustomerInfo(String customerInfo)
  {
    this.customerInfo = customerInfo;
  }

  public String getCustomerInfo()
  {
    return this.customerInfo;
  }

  public void setRewardName(String rewardName)
  {
    this.rewardName = rewardName;
  }

  public String getRewardName()
  {
    return this.rewardName;
  }

  public void setIdLength(String idLength)
  {
    this.idLength = idLength;
  }

  public String getIdLength()
  {
    return this.idLength;
  }

  public void setEmailExludeFlag(String emailExludeFlag)
  {
    this.emailExludeFlag = emailExludeFlag;
  }

  public String getEmailExludeFlag()
  {
    return this.emailExludeFlag;
  }
}