package com.ftd.osp.utilities.order.vo;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * TaxVO will contain tax info from FTD_APPS.STATE_COMPANY_TAX table. For each
 * tax column, a new VO will be created an passed around as a List.
 *
 * @author Ali Lakhani
 */
@SuppressWarnings("serial")
public class TaxVO extends BaseVO implements Serializable, XMLInterface {
	
	private String name;
	
	private String description;
	
	private BigDecimal rate;
	
	private BigDecimal amount;
	
	private int displayOrder;

	/**
	 * @param name
	 * @param description
	 * @param rate
	 * @param amount
	 * @param displayOrder
	 */
	public TaxVO(String name, String description, BigDecimal rate, BigDecimal amount, int displayOrder) {		
		this.name = name;
		this.description = description;
		this.rate = rate;
		this.amount = amount;
		this.displayOrder = displayOrder;
	}

	public TaxVO() {}

	/**
	 * This method uses the Reflection API to generate an XML string that will
	 * be passed back to the calling module. The XML string will contain all the
	 * fields within this VO, including the variables as well as (a collection
	 * of) ValueObjects.
	 *
	 * @param None
	 * @return XML string
	 **/
	@SuppressWarnings("rawtypes")
	public String toXML() {
		StringBuffer sb = new StringBuffer();
		
		try {
			sb.append("<TaxVO>");
			Field[] fields = this.getClass().getDeclaredFields();

			for (int i = 0; i < fields.length; i++) {
				
				// if the field retrieved was a list of VO
				if (fields[i].getType().equals(Class.forName("java.util.List"))) {
					List list = (List) fields[i].get(this);
					if (list != null) {
						for (int j = 0; j < list.size(); j++) {
							XMLInterface xmlInt = (XMLInterface) list.get(j);
							String sXmlVO = xmlInt.toXML();
							sb.append(sXmlVO);
						}
					}
				} else {
					
					// if the field retrieved was a VO
					if (fields[i].getType().toString().matches("(?i).*vo")) {
						XMLInterface xmlInt = (XMLInterface) fields[i].get(this);
						String sXmlVO = xmlInt.toXML();
						sb.append(sXmlVO);
					}
					
					// if the field retrieved was a Calendar object
					else if (fields[i].getType().toString().matches("(?i).*calendar")) {
						Date date;
						String fDate = null;
						if (fields[i].get(this) != null) {
							date = (((GregorianCalendar) fields[i].get(this)).getTime());
							SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a zzz");
							fDate = sdf.format(date).toString();
						}
						sb.append("<" + fields[i].getName() + ">");
						sb.append(fDate);
						sb.append("</" + fields[i].getName() + ">");

					} else {
						sb.append("<" + fields[i].getName() + ">");
						sb.append(fields[i].get(this));
						sb.append("</" + fields[i].getName() + ">");
					}
				}
			}
			sb.append("</TaxVO>");
		}

		catch (Exception e) {
			e.printStackTrace();
		}

		return sb.toString();
	}

	/**
	 * This method uses the Reflection API to generate an XML string that will
	 * be passed back to the calling module. The XML string will contain all the
	 * fields within this VO, including the variables as well as (a collection
	 * of) ValueObjects.
	 *
	 * @param None
	 * @return XML string
	 **/
	@SuppressWarnings("rawtypes")
	public String toXML(int count) {
		StringBuffer sb = new StringBuffer();
		try {
			if (count == 0) {
				sb.append("<TaxVO>");
			} else {
				sb.append("<TaxVO num=" + '"' + count + '"' + ">");
			}
			Field[] fields = this.getClass().getDeclaredFields();

			for (int i = 0; i < fields.length; i++) {
				// if the field retrieved was a list of VO
				if (fields[i].getType().equals(Class.forName("java.util.List"))) {
					List list = (List) fields[i].get(this);
					if (list != null) {
						for (int j = 0; j < list.size(); j++) {
							XMLInterface xmlInt = (XMLInterface) list.get(j);
							int k = j + 1;
							String sXmlVO = xmlInt.toXML(k);
							sb.append(sXmlVO);
						}
					}
				} else {
					// if the field retrieved was a VO
					if (fields[i].getType().toString().matches("(?i).*vo")) {
						XMLInterface xmlInt = (XMLInterface) fields[i].get(this);
						int k = i + 1;
						String sXmlVO = xmlInt.toXML(k);
						sb.append(sXmlVO);
					}
					// if the field retrieved was a Calendar object
					else if (fields[i].getType().toString().matches("(?i).*calendar")) {
						Date date;
						String fDate = null;
						if (fields[i].get(this) != null) {
							date = (((GregorianCalendar) fields[i].get(this)).getTime());
							SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a zzz");
							fDate = sdf.format(date).toString();
							sb.append("<" + fields[i].getName() + ">");
							sb.append(fDate);
							sb.append("</" + fields[i].getName() + ">");
						} else {
							sb.append("<" + fields[i].getName() + "/>");
						}

					} else {
						if (fields[i].get(this) == null) {
							sb.append("<" + fields[i].getName() + "/>");
						} else {
							sb.append("<" + fields[i].getName() + ">");
							sb.append(fields[i].get(this));
							sb.append("</" + fields[i].getName() + ">");
						}
					}
				}
			}
			sb.append("</TaxVO>");
		}

		catch (Exception e) {
			e.printStackTrace();
		}

		return sb.toString();
	}

	@SuppressWarnings("rawtypes")
	public String toString() {
		String lineSep = System.getProperty("line.separator");
		StringBuffer sb = new StringBuffer();
		StringBuffer vectorsSb = new StringBuffer();
		
		// Get this Class
		Class thisClass = this.getClass();
		sb.append("Object Type = ").append(thisClass.getName()).append(lineSep);

		// Use reflection to loop through all the fields
		Field[] fields = thisClass.getDeclaredFields();
		for (int i = 0; i < fields.length; i++) {
			try {
				if (fields[i].getType().equals(	Class.forName("java.lang.String"))) {
					sb.append(fields[i].getName()).append(" = ").append(fields[i].get(this)).append(lineSep);
				} else if (fields[i].getType().equals(Class.forName("java.util.List"))) {
					List vect = (List) fields[i].get(this);
					if (vect != null) {
						for (int j = 0; j < vect.size(); j++) {
							vectorsSb.append(vect.get(j).toString());
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		sb.append(vectorsSb);

		return sb.toString();
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}

	public BigDecimal getRate() {
		return rate;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setDisplayOrder(int displayOrder) {
		this.displayOrder = displayOrder;
	}

	public int getDisplayOrder() {
		return displayOrder;
	}

}
