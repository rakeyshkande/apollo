package com.ftd.osp.utilities.order.vo;

import java.util.List;
import java.lang.reflect.*;
import java.io.Serializable;

/**
 * Credit Card Value Object that maps to CREDIT_CARDS database table
 * This is a sub collection within Payments value object 
 *
 * @author Doug Johnson
 */
public class CreditCardsVO extends BaseVO implements Serializable  
{
  private long creditCardId;
  private long buyerId;
  private String ccType;
  private String ccNumber;
  private String ccExpiration;
  private boolean ccNumberChanged = false;
  private boolean ccTypeChanged = false;
  private String pin;
  private String ccEncrypted;

  public CreditCardsVO()
  {}

  public long getCreditCardId()
  {
    return creditCardId;
  }

  public void setCreditCardId(long newCreditCardId)
  {
    if(valueChanged(creditCardId, newCreditCardId))
    {
        setChanged(true);
    }
    creditCardId = newCreditCardId;
  }

  public long getBuyerId()
  {
    return buyerId;
  }

  public void setBuyerId(long newBuyerId)
  {
    if(valueChanged(buyerId, newBuyerId))
    {
        setChanged(true);
    }
    buyerId = newBuyerId;
  }

  public String getCCType()
  {
    return ccType;
  }

  public void setCCType(String newccType)
  {
    if(valueChanged(ccType, newccType))
    {
        setChanged(true);
        setCcTypeChanged(true);
    }
    ccType = newccType;
  }

  public String getCCNumber()
  {
    return ccNumber;
  }

  public void setCCNumber(String newCCNumber)
  {
    if(valueChanged(ccNumber, newCCNumber))
    {
        setChanged(true);
        setCcNumberChanged(true);
    }
    ccNumber = newCCNumber;
  }

  public String getCCExpiration()
  {
    return ccExpiration;
  }

  public void setCCExpiration(String newCCExperation)
  {
    if(valueChanged(ccExpiration, newCCExperation))
    {
        setChanged(true);
    }
    ccExpiration = newCCExperation;
  }
  
    public String toString()
    {
        String lineSep = System.getProperty("line.separator");
        StringBuffer sb = new StringBuffer();
        StringBuffer vectorsSb = new StringBuffer();
        // Get this Class
        Class thisClass = this.getClass();
        sb.append("Object Type = ").append(thisClass.getName()).append(lineSep);
        
        // Use reflection to loop through all the fields
        Field[] fields = thisClass.getDeclaredFields();
        for(int i = 0; i < fields.length; i++)
        {
            try
            {
                if(fields[i].getType().equals(Class.forName("java.lang.String")))
                {
                    sb.append(fields[i].getName()).append(" = ").append(fields[i].get(this)).append(lineSep);
                }
                else if(fields[i].getType().equals(Class.forName("java.util.List")))
                {
                    List vect = (List)fields[i].get(this);
                    if(vect != null)
                    {
                        for (int j = 0; j < vect.size(); j++) 
                        {
                            vectorsSb.append(vect.get(j).toString());
                        }
                    }
                }
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        }

        sb.append(vectorsSb);
        
        return sb.toString();
    }

    public boolean isCcNumberChanged()
    {
        return ccNumberChanged;
    } 

    public void setCcNumberChanged(boolean ccNumberChanged)
    {
        this.ccNumberChanged = ccNumberChanged;
    }
    
    public boolean isCcTypeChanged()
    {
        return ccTypeChanged;
    }

    public void setCcTypeChanged(boolean ccTypeChanged)
    {
        this.ccTypeChanged = ccTypeChanged;
    }

	public String getPin() {
		return pin;
	}

	public void setPin(String newPin) {
		if(valueChanged(pin, newPin))
	    {
	        setChanged(true);
	    }
	    pin = newPin;
	}

    public String getCcEncrypted() {
        return ccEncrypted;
    }

    public void setCcEncrypted(String ccEncrypted) {
        this.ccEncrypted = ccEncrypted;
    }

}
