package com.ftd.osp.utilities.order.vo;

import java.io.Serializable;

import java.math.BigDecimal;

/**
 * Provides Price Header details used for Recalculation logic.
 */
public class RecalculateOrderPriceHeaderDetailsVO
  implements Serializable
{

  String discountType;
  BigDecimal discountAmount;

  public RecalculateOrderPriceHeaderDetailsVO()
  {
  }


  public void setDiscountType(String discountType)
  {
    this.discountType = discountType;
  }

  public String getDiscountType()
  {
    return discountType;
  }

  public void setDiscountAmount(BigDecimal discountAmount)
  {
    this.discountAmount = discountAmount;
  }

  public BigDecimal getDiscountAmount()
  {
    return discountAmount;
  }
}
