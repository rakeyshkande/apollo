package com.ftd.osp.utilities.order.vo;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.List;

/**
 * Recipient Phones Value Object that maps to RECIPIENT_PHONES database table
 * This is a sub collection within Recipient value object
 *
 * @author Doug Johnson
 */
public class RecipientPhonesVO extends BaseVO implements Serializable  
{
  private long recipientPhoneId;
  private long recipientId;
  private String phoneType;
  private String phoneNumber;
  private String extension;

  public RecipientPhonesVO()
  {
  }

  public long getRecipientPhoneId()
  {
    return recipientPhoneId;
  }

  public void setRecipientPhoneId(long newRecipientPhoneId)
  {
    if(valueChanged(recipientPhoneId,newRecipientPhoneId)){
      setChanged(true);
    }    
    recipientPhoneId = newRecipientPhoneId;
  }

  public long getRecipientId()
  {
    return recipientId;
  }

  public void setRecipientId(long newRecipientId)
  {
    if(valueChanged(recipientId,newRecipientId)){
      setChanged(true);
    }      
    recipientId = newRecipientId;
  }

  public String getPhoneType()
  {
    return phoneType;
  }

  public void setPhoneType(String newPhoneType)
  {
    if(valueChanged(phoneType,newPhoneType)){
      setChanged(true);
    }      
    phoneType = newPhoneType;
  }

  public String getPhoneNumber()
  {
    return phoneNumber;
  }

  public void setPhoneNumber(String newPhoneNumber)
  {
    if(valueChanged(phoneNumber,newPhoneNumber)){
      setChanged(true);
    }      
    phoneNumber = newPhoneNumber;
  }

  public String getExtension()
  {
    return extension;
  }

  public void setExtension(String newExtension)
  {
    if(valueChanged(extension,newExtension)){
      setChanged(true);
    }      
    extension = newExtension;
  }

  public String toString()
  {
    String lineSep = System.getProperty("line.separator");
    StringBuffer sb = new StringBuffer();
    StringBuffer listSb = new StringBuffer();
    // Get this Class
    Class thisClass = this.getClass();
    sb.append("Object Type = ").append(thisClass.getName()).append(lineSep);
        
    // Use reflection to loop through all the fields
    Field[] fields = thisClass.getDeclaredFields();
    for(int i = 0; i < fields.length; i++)
    {
        try
        {
            if(fields[i].getType().equals(Class.forName("java.lang.String")))
            {
                sb.append(fields[i].getName()).append(" = ").append(fields[i].get(this)).append(lineSep);
            }
            else if(fields[i].getType().equals(Class.forName("java.util.List")))
            {
                List list = (List)fields[i].get(this);
                if(list != null)
                {
                    for (int j = 0; j < list.size(); j++) 
                    {
                        listSb.append(list.get(j).toString());
                    }
                }
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    sb.append(listSb);
        
    return sb.toString();
  }
}