package com.ftd.osp.utilities.order.vo;

import java.sql.Connection;
import java.util.Date;

public class SympathyItemsVO {
	
	private String sourceCode;
	private Date deliveryDate;
	private Integer deliveryTime;
	private String shipState;
	private String deliveryLocation;
	private String shipMethod;
	private boolean leadTimeCheckFlag;
	private String module;
	
	public String getSourceCode() {
		return sourceCode;
	}
	public void setSourceCode(String sourceCode) {
		this.sourceCode = sourceCode;
	}
	public Date getDeliveryDate() {
		return deliveryDate;
	}
	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}
	public Integer getDeliveryTime() {
		return deliveryTime;
	}
	public void setDeliveryTime(Integer deliveryTime) {
		this.deliveryTime = deliveryTime;
	}
	public String getShipState() {
		return shipState;
	}
	public void setShipState(String shipState) {
		this.shipState = shipState;
	}
	public String getDeliveryLocation() {
		return deliveryLocation;
	}
	public void setDeliveryLocation(String deliveryLocation) {
		this.deliveryLocation = deliveryLocation;
	}
	public String getShipMethod() {
		return shipMethod;
	}
	public void setShipMethod(String shipMethod) {
		this.shipMethod = shipMethod;
	}
	public boolean isLeadTimeCheckFlag() {
		return leadTimeCheckFlag;
	}
	public void setLeadTimeCheckFlag(boolean leadTimeCheckFlag) {
		this.leadTimeCheckFlag = leadTimeCheckFlag;
	}
	public String getModule() {
		return module;
	}
	public void setModule(String module) {
		this.module = module;
	}

}
