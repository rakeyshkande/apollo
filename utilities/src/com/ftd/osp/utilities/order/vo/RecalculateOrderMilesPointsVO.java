package com.ftd.osp.utilities.order.vo;

import java.io.Serializable;

/**
 * Provides Miles Point Information used for Recalculation logic.
 */
public class RecalculateOrderMilesPointsVO implements Serializable
{

  private String milesPoints ="0";
  private String rewardType = null;

  public RecalculateOrderMilesPointsVO()
  {
  }

  public void setMilesPoints(String milesPoints)
  {
    this.milesPoints = milesPoints;
  }

  public String getMilesPoints()
  {
    return milesPoints;
  }

  public void setRewardType(String rewardType)
  {
    this.rewardType = rewardType;
  }

  public String getRewardType()
  {
    return rewardType;
  }
}
