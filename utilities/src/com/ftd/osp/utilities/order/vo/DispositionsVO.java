package com.ftd.osp.utilities.order.vo;

import java.util.List;
import java.lang.reflect.*;
import java.io.Serializable;

/**
 * Buyer Value Object that maps to ORDER_DISPOSITION database table
 * This is a sub collection within Order detail value object
 *
 * @author Ed Mueller
 */
public class DispositionsVO extends BaseVO implements Serializable  {

  private long OrderDispositionID;
  private long OrderDetailID;
  private String DispositionID;
  private String Comments;
  private String CalledCustomerFlag;
  private String SentEmailFlag;
  private String StockMessageID;
  private String EmailSubject;
  private String CSRId;
  private String CommentsDate;
  private String DispositionDescription;
  String EmailMessage;

  public DispositionsVO()
  {
  }

    public String toString()
    {
        String lineSep = System.getProperty("line.separator");
        StringBuffer sb = new StringBuffer();
        StringBuffer vectorsSb = new StringBuffer();
        // Get this Class
        Class thisClass = this.getClass();
        sb.append("Object Type = ").append(thisClass.getName()).append(lineSep);
        
        // Use reflection to loop through all the fields
        Field[] fields = thisClass.getDeclaredFields();
        for(int i = 0; i < fields.length; i++)
        {
            try
            {
                if(fields[i].getType().equals(Class.forName("java.lang.String")))
                {
                    sb.append(fields[i].getName()).append(" = ").append(fields[i].get(this)).append(lineSep);
                }
                else if(fields[i].getType().equals(Class.forName("java.util.List")))
                {
                    List vect = (List)fields[i].get(this);
                    if(vect != null)
                    {
                        for (int j = 0; j < vect.size(); j++) 
                        {
                            vectorsSb.append(vect.get(j).toString());
                        }
                    }
                }
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        }

        sb.append(vectorsSb);
        
        return sb.toString();
    }  

  public long getOrderDispositionID()
  {
    return OrderDispositionID;
  }

  public void setOrderDispositionID(long newOrderDispositionID)
  {
    if(valueChanged(OrderDispositionID, newOrderDispositionID))
    {
        setChanged(true);
    }
    OrderDispositionID = newOrderDispositionID;
  }

  public long getOrderDetailID()
  {
    return OrderDetailID;
  }

  public void setOrderDetailID(long newOrderDetailID)
  {
    if(valueChanged(OrderDetailID, newOrderDetailID))
    {
        setChanged(true);
    }
    OrderDetailID = newOrderDetailID;
  }

  public String getDispositionID()
  {
    return DispositionID;
  }

  public void setDispositionID(String newDispositionID)
  {
    if(valueChanged(DispositionID, newDispositionID))
    {
        setChanged(true);
    }
    DispositionID = newDispositionID;
  }

  public String getComments()
  {
    return Comments;
  }

  public void setComments(String newComments)
  {
    if(valueChanged(Comments, newComments))
    {
        setChanged(true);
    }
    Comments = newComments;
  }

  public String getCalledCustomerFlag()
  {
    return CalledCustomerFlag;
  }

  public void setCalledCustomerFlag(String newCalledCustomerFlag)
  {
    if(valueChanged(CalledCustomerFlag, newCalledCustomerFlag))
    {
        setChanged(true);
    }
    CalledCustomerFlag = newCalledCustomerFlag;
  }

  public String getSentEmailFlag()
  {
    return SentEmailFlag;
  }

  public void setSentEmailFlag(String newSentEmailFlag)
  {
    if(valueChanged(SentEmailFlag, newSentEmailFlag))
    {
        setChanged(true);
    }
    SentEmailFlag = newSentEmailFlag;
  }

  public String getStockMessageID()
  {
    return StockMessageID;
  }

  public void setStockMessageID(String newStockMessageID)
  {
    if(valueChanged(StockMessageID, newStockMessageID))
    {
        setChanged(true);
    }
    StockMessageID = newStockMessageID;
  }

  public String getEmailSubject()
  {
    return EmailSubject;
  }

  public void setEmailSubject(String newEmailSubject)
  {
    if(valueChanged(EmailSubject, newEmailSubject))
    {
        setChanged(true);
    }
    EmailSubject = newEmailSubject;
  }

  public String getEmailMessage()
  {
    return EmailMessage;
  }

  public void setEmailMessage(String newEmailMessage)
  {
    if(valueChanged(EmailMessage, newEmailMessage))
    {
        setChanged(true);
    }
    EmailMessage = newEmailMessage;
  }

  public String getCsrId()
  {
    return CSRId;
  }

  public void setCsrId(String newCsrId)
  {
    if(valueChanged(CSRId, newCsrId))
    {
        setChanged(true);
    }
    CSRId = newCsrId;
  }

  public String getCommentsDate()
  {
    return CommentsDate;
  }

  public void setCommentsDate(String newCommentsDate)
  {
    if(valueChanged(CommentsDate, newCommentsDate))
    {
        setChanged(true);
    }
    CommentsDate = newCommentsDate;
  }

    public String getDispositionDescription()
    {
        return DispositionDescription;
    }

    public void setDispositionDescription(String newDispositionDescription)
    {
        DispositionDescription = newDispositionDescription;
    }
}