package com.ftd.osp.utilities.order.vo;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.List;

/**
 * Recipient Comments Value Object that maps to RECIPIENT_COMMENTS database table
 * This is a sub collection within Recipient value object
 *
 * @author Doug Johnson
 */
public class RecipientCommentsVO extends BaseVO implements Serializable  
{
  private long recipientCommentId;
  private long recipientId;
  private String text;

  public RecipientCommentsVO()
  {
  }

  public long getRecipientCommentId()
  {
    return recipientCommentId;
  }

  public void setRecipientCommentId(long newRecipientCommentId)
  {
    if(valueChanged(recipientCommentId,newRecipientCommentId)){
      setChanged(true);
    }      
    recipientCommentId = newRecipientCommentId;
  }

  public long getRecipientId()
  {
    return recipientId;
  }

  public void setRecipientId(long newRecipientId)
  {
    if(valueChanged(recipientId,newRecipientId)){
      setChanged(true);
    }      
    recipientId = newRecipientId;
  }

  public String getText()
  {
    return text;
  }

  public void setText(String newText)
  {
    if(valueChanged(text,newText)){
      setChanged(true);
    }      
    text = newText;
  }

  public String toString()
  {
    String lineSep = System.getProperty("line.separator");
    StringBuffer sb = new StringBuffer();
    StringBuffer listSb = new StringBuffer();
    // Get this Class
    Class thisClass = this.getClass();
    sb.append("Object Type = ").append(thisClass.getName()).append(lineSep);
        
    // Use reflection to loop through all the fields
    Field[] fields = thisClass.getDeclaredFields();
    for(int i = 0; i < fields.length; i++)
    {
        try
        {
            if(fields[i].getType().equals(Class.forName("java.lang.String")))
            {
                sb.append(fields[i].getName()).append(" = ").append(fields[i].get(this)).append(lineSep);
            }
            else if(fields[i].getType().equals(Class.forName("java.util.List")))
            {
                List list = (List)fields[i].get(this);
                if(list != null)
                {
                    for (int j = 0; j < list.size(); j++) 
                    {
                        listSb.append(list.get(j).toString());
                    }
                }
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    sb.append(listSb);
        
    return sb.toString();
  }
}