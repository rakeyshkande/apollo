package com.ftd.osp.utilities.order.vo;

import java.io.Serializable;


/**
 * Order Extensions Value Object that maps to ORDER_EXTENSIONS database table.
 * This represents optional order detail information as name value pairs.  
 * NOT validated in Scrub - it merely flows through the system as extra baggage.
 */

public class OrderExtensionsVO extends BaseVO implements Serializable {

    private long   extensionId;    // Primary key 
    private long   orderDetailId;  // Foreign key associating this with order_detail
    private String infoName;       // Name of data item
    private String infoValue;       // Data item value

    public long getExtensionId()
    {
        return extensionId;
    }

    public void setExtensionId(long extensionId)
    {
        this.extensionId = extensionId;
    }

    public long getOrderDetailId()
    {
        return orderDetailId;
    }

    public void setOrderDetailId(long orderDetailId)
    {
        this.orderDetailId = orderDetailId;
    }

    public String getInfoName()
    {
        return infoName;
    }

    public void setInfoName(String infoName)
    {
        this.infoName = infoName;
    }

    public String getInfoValue()
    {
        return infoValue;
    }

    public void setInfoValue(String infoValue)
    {
        this.infoValue = infoValue;
    }
}
