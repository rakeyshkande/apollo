package com.ftd.osp.utilities.order.vo;

import java.io.Serializable;

import java.lang.reflect.Field;

import java.util.List;


public class JcPenneyVO extends BaseVO implements Serializable  
{
  private long jcpenneyId;
  private String guid;
  private String interchangeReceiver;
  private String productDescription;
  private String unitNumber;
  private String poType;
  private String appropriation;
  private String accountNumber;
  private String unitOfMeasure;
  private String unitSize;
  private String invoiceRemitReferenceNumber;
  private String invoiceRemitAmount;
  private String poNumber;
  private String ftdWholesalePoAmount;
  private String invoiceNumber;
  private String jcpWholesalePoAmount;
  private String ediFtdPo;
  private String productSubcategory;
  private String chargebackAmount;
  private String chargebackReference;

  public JcPenneyVO()
  {
  }

  
  public long getJcpenneyId()
  {
    return jcpenneyId;
  }

  public void setJcpenneyId(long newJcpenneyId)
  {
    if(valueChanged(jcpenneyId, newJcpenneyId))
    {
        setChanged(true);
    }  
    jcpenneyId = newJcpenneyId;
  }

  public String getGuid()
  {
    return guid;
  }

  public void setGuid(String newGuid)
  {
    if(valueChanged(guid, newGuid))
    {
        setChanged(true);
    }
    guid = newGuid;
  }

  public String getInterchangeReceiver()
  {
    return interchangeReceiver;
  }

  public void setInterchangeReceiver(String newInterchangeReceiver)
  {
    if(valueChanged(interchangeReceiver, newInterchangeReceiver))
    {
        setChanged(true);
    }
    interchangeReceiver = newInterchangeReceiver;
  }

  public String getProductDescription()
  {
    return productDescription;
  }

  public void setProductDescription(String newProductDescription)
  {
    if(valueChanged(productDescription, newProductDescription))
    {
        setChanged(true);
    }
    productDescription = newProductDescription;
  }

  public String getUnitNumber()
  {
    return unitNumber;
  }

  public void setUnitNumber(String newUnitNumber)
  {
    if(valueChanged(unitNumber, newUnitNumber))
    {
        setChanged(true);
    }
    unitNumber = newUnitNumber;
  }

  public String getPoType()
  {
    return poType;
  }

  public void setPoType(String newPoType)
  {
    if(valueChanged(poType, newPoType))
    {
        setChanged(true);
    }
    poType = newPoType;
  }

  public String getAppropriation()
  {
    return appropriation;
  }

  public void setAppropriation(String newAppropriation)
  {
    if(valueChanged(appropriation, newAppropriation))
    {
        setChanged(true);
    }
    appropriation = newAppropriation;
  }

  public String getAccountNumber()
  {
    return accountNumber;
  }

  public void setAccountNumber(String newAccountNumber)
  {
    if(valueChanged(accountNumber, newAccountNumber))
    {
        setChanged(true);
    }
    accountNumber = newAccountNumber;
  }

  public String getUnitOfMeasure()
  {
    return unitOfMeasure;
  }

  public void setUnitOfMeasure(String newUnitOfMeasure)
  {
    if(valueChanged(unitOfMeasure, newUnitOfMeasure))
    {
        setChanged(true);
    }
    unitOfMeasure = newUnitOfMeasure;
  }

  public String getUnitSize()
  {
    return unitSize;
  }

  public void setUnitSize(String newUnitSize)
  {
    if(valueChanged(unitSize, newUnitSize))
    {
        setChanged(true);
    }
    unitSize = newUnitSize;
  }

  public String getInvoiceRemitReferenceNumber()
  {
    return invoiceRemitReferenceNumber;
  }

  public void setInvoiceRemitReferenceNumber(String newInvoiceRemitReferenceNumber)
  {
    if(valueChanged(invoiceRemitReferenceNumber, newInvoiceRemitReferenceNumber))
    {
        setChanged(true);
    }
    invoiceRemitReferenceNumber = newInvoiceRemitReferenceNumber;
  }

  public String getInvoiceRemitAmount()
  {
    return invoiceRemitAmount;
  }

  public void setInvoiceRemitAmount(String newInvoiceRemitAmount)
  {
    if(valueChanged(invoiceRemitAmount, newInvoiceRemitAmount))
    {
        setChanged(true);
    }
    invoiceRemitAmount = newInvoiceRemitAmount;
  }

  public String getPoNumber()
  {
    return poNumber;
  }

  public void setPoNumber(String newPoNumber)
  {
    if(valueChanged(poNumber, newPoNumber))
    {
        setChanged(true);
    }
    poNumber = newPoNumber;
  }

  public String getFtdWholesalePoAmount()
  {
    return ftdWholesalePoAmount;
  }

  public void setFtdWholesalePoAmount(String newFtdWholesalePoAmount)
  {
    if(valueChanged(ftdWholesalePoAmount, newFtdWholesalePoAmount))
    {
        setChanged(true);
    }
    ftdWholesalePoAmount = newFtdWholesalePoAmount;
  }

  public String getInvoiceNumber()
  {
    return invoiceNumber;
  }

  public void setInvoiceNumber(String newInvoiceNumber)
  {
    if(valueChanged(invoiceNumber, newInvoiceNumber))
    {
        setChanged(true);
    }
    invoiceNumber = newInvoiceNumber;
  }

  public String getJcpWholesalePoAmount()
  {
    return jcpWholesalePoAmount;
  }

  public void setJcpWholesalePoAmount(String newJcpWholesalePoAmount)
  {
    if(valueChanged(jcpWholesalePoAmount, newJcpWholesalePoAmount))
    {
        setChanged(true);
    }
    jcpWholesalePoAmount = newJcpWholesalePoAmount;
  }

  public String getEdiFtdPo()
  {
    return ediFtdPo;
  }

  public void setEdiFtdPo(String newEdiFtdPo)
  {
    if(valueChanged(ediFtdPo, newEdiFtdPo))
    {
        setChanged(true);
    }
    ediFtdPo = newEdiFtdPo;
  }

  public String getProductSubcategory()
  {
    return productSubcategory;
  }

  public void setProductSubcategory(String newProductSubcategory)
  {
    if(valueChanged(productSubcategory, newProductSubcategory))
    {
        setChanged(true);
    }
    productSubcategory = newProductSubcategory;
  }

  public String getChargebackAmount()
  {
    return chargebackAmount;
  }

  public void setChargebackAmount(String newChargebackAmount)
  {
    if(valueChanged(chargebackAmount, newChargebackAmount))
    {
        setChanged(true);
    }
    chargebackAmount = newChargebackAmount;
  }

  public String getChargebackReference()
  {
    return chargebackReference;
  }

  public void setChargebackReference(String newChargebackReference)
  {
    if(valueChanged(chargebackReference, newChargebackReference))
    {
        setChanged(true);
    }
    chargebackReference = newChargebackReference;
  }

  public String toString()
  {
    String lineSep = System.getProperty("line.separator");
    StringBuffer sb = new StringBuffer();
    StringBuffer listSb = new StringBuffer();
    // Get this Class
    Class thisClass = this.getClass();
    sb.append("Object Type = ").append(thisClass.getName()).append(lineSep);
        
    // Use reflection to loop through all the fields
    Field[] fields = thisClass.getDeclaredFields();
    for(int i = 0; i < fields.length; i++)
    {
        try
        {
            if(fields[i].getType().equals(Class.forName("java.lang.String")))
            {
                sb.append(fields[i].getName()).append(" = ").append(fields[i].get(this)).append(lineSep);
            }
            else if(fields[i].getType().equals(Class.forName("java.util.List")))
            {
                List list = (List)fields[i].get(this);
                if(list != null)
                {
                    for (int j = 0; j < list.size(); j++) 
                    {
                        listSb.append(list.get(j).toString());
                    }
                }
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    sb.append(listSb);
        
    return sb.toString();
  }
}