package com.ftd.osp.utilities.order.vo;

public interface XMLInterface 
{

  public String toXML();

  public String toXML(int count);
}