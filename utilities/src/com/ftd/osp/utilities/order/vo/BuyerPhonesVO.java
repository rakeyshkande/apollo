package com.ftd.osp.utilities.order.vo;

import java.util.List;
import java.lang.reflect.*;
import java.io.Serializable;

/**
 * Buyer Phones Value Object that maps to BUYER_PHONES database table
 * This is a sub collection within Buyer value object
 *
 * @author Doug Johnson
 */
public class BuyerPhonesVO extends BaseVO implements Serializable  
{
  private long buyerPhoneId;
  private long buyerId;
  private String phoneType;
  private String phoneNumber;
  private String extension;
  private String smsOptIn;
  private String phoneNumberType; 
 
  public BuyerPhonesVO()
  {}

  public long getBuyerPhoneId()
  {
    return buyerPhoneId;
  }

  public void setBuyerPhoneId(long newBuyerPhoneId)
  {
    if(valueChanged(buyerPhoneId, newBuyerPhoneId))
    {
        setChanged(true);
    }
    buyerPhoneId = newBuyerPhoneId;
  }

  public long getBuyerId()
  {
    return buyerId;
  }

  public void setBuyerId(long newBuyerId)
  {
    if(valueChanged(buyerId, newBuyerId))
    {
        setChanged(true);
    }
    buyerId = newBuyerId;
  }

  public String getPhoneType()
  {
    return phoneType;
  }

  public void setPhoneType(String newPhoneType)
  {
    if(valueChanged(phoneType, newPhoneType))
    {
        setChanged(true);
    }
    phoneType = newPhoneType;
  }

  public String getPhoneNumber()
  {
    return phoneNumber;
  }

  public void setPhoneNumber(String newPhoneNumber)
  {
    if(valueChanged(phoneNumber, newPhoneNumber))
    {
        setChanged(true);
    }
    phoneNumber = newPhoneNumber;
  }

  public String getExtension()
  {
    return extension;
  }

  public void setExtension(String newExtension)
  {
    if(valueChanged(extension, newExtension))
    {
        setChanged(true);
    }
    extension = newExtension;
  }
  
    public String toString()
    {
        String lineSep = System.getProperty("line.separator");
        StringBuffer sb = new StringBuffer();
        StringBuffer vectorsSb = new StringBuffer();
        // Get this Class
        Class thisClass = this.getClass();
        sb.append("Object Type = ").append(thisClass.getName()).append(lineSep);
        
        // Use reflection to loop through all the fields
        Field[] fields = thisClass.getDeclaredFields();
        for(int i = 0; i < fields.length; i++)
        {
            try
            {
                if(fields[i].getType().equals(Class.forName("java.lang.String")))
                {
                    sb.append(fields[i].getName()).append(" = ").append(fields[i].get(this)).append(lineSep);
                }
                else if(fields[i].getType().equals(Class.forName("java.util.List")))
                {
                    List vect = (List)fields[i].get(this);
                    if(vect != null)
                    {
                        for (int j = 0; j < vect.size(); j++) 
                        {
                            vectorsSb.append(vect.get(j).toString());
                        }
                    }
                }
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        }

        sb.append(vectorsSb);
        
        return sb.toString();
    }

	public String getSmsOptIn() {
		return smsOptIn;
	}

	public void setSmsOptIn(String smsOptIn) {
		this.smsOptIn = smsOptIn;
	}

	public String getPhoneNumberType() {
		return phoneNumberType;
	}

	public void setPhoneNumberType(String phoneNumberType) {
		this.phoneNumberType = phoneNumberType;
	}
}