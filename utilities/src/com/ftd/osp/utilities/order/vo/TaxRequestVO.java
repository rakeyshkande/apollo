/**
 * 
 */
package com.ftd.osp.utilities.order.vo;

import java.math.BigDecimal;

/**
 * @author smeka
 *
 */
public class TaxRequestVO {
	
	private RecipientAddressesVO recipientAddress;
	private String companyId;
	private String shipMethod;
	private String confirmationNumber;
	private long orderDetailNumber;
	private BigDecimal taxableAmount;
	
	/**
	 * @return the recipientAddress
	 */
	public RecipientAddressesVO getRecipientAddress() {
		return recipientAddress;
	}
	/**
	 * @param recipientAddress the recipientAddress to set
	 */
	public void setRecipientAddress(RecipientAddressesVO recipientAddress) {
		this.recipientAddress = recipientAddress;
	}
	/**
	 * @return the companyId
	 */
	public String getCompanyId() {
		return companyId;
	}
	/**
	 * @param companyId the companyId to set
	 */
	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}
	/**
	 * @return the shipMethod
	 */
	public String getShipMethod() {
		return shipMethod;
	}
	/**
	 * @param shipMethod the shipMethod to set
	 */
	public void setShipMethod(String shipMethod) {
		this.shipMethod = shipMethod;
	}
	/**
	 * @return the confirmationNumber
	 */
	public String getConfirmationNumber() {
		return confirmationNumber;
	}
	/**
	 * @param confirmationNumber the confirmationNumber to set
	 */
	public void setConfirmationNumber(String confirmationNumber) {
		this.confirmationNumber = confirmationNumber;
	}
	/**
	 * @return the orderDetailNumber
	 */
	public long getOrderDetailNumber() {
		return orderDetailNumber;
	}
	/**
	 * @param orderDetailNumber the orderDetailNumber to set
	 */
	public void setOrderDetailNumber(long orderDetailNumber) {
		this.orderDetailNumber = orderDetailNumber;
	}
	/**
	 * @return the taxableAmount
	 */
	public BigDecimal getTaxableAmount() {
		return taxableAmount;
	}
	/**
	 * @param taxableAmount the taxableAmount to set
	 */
	public void setTaxableAmount(BigDecimal taxableAmount) {
		this.taxableAmount = taxableAmount;
	}
	
	
}
