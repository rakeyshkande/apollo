package com.ftd.osp.utilities.order.vo;

import java.io.Serializable;
import java.lang.reflect.*;
import java.util.List;

/**
 * AVS Addresses Value Object
 * This is a sub collection within Order Details value object
 *
 */
public class AVSAddressScoreVO extends BaseVO implements Serializable 
{
    private long avsScoreId;
    private long avsAddressId;
    private String reason;
    private String score;


    public AVSAddressScoreVO()
    {
    }

    public long getAvsScoreId()
    {
        return avsScoreId;
    }

    public void setAvsScoreId(long newAvsScoreId)
    {
        if(valueChanged(avsScoreId,newAvsScoreId)){
            setChanged(true);
        }    
        avsScoreId = newAvsScoreId;
    }

    public long getAvsAddressId()
    {
        return avsAddressId;
    }

    public void setAvsAddressId(long newAvsAddressId)
    {
        if(valueChanged(avsAddressId,newAvsAddressId)){
            setChanged(true);
        }    
        avsAddressId = newAvsAddressId;
    }


    public void setReason(String newReason)
    {
        if(valueChanged(reason,newReason)){
            setChanged(true);
        }    
        this.reason = newReason;
    }

    public String getReason()
    {
        return reason;
    }

    public void setScore(String newScore)
    {
        if(valueChanged(score,newScore)){
            setChanged(true);
        }    
        this.score = newScore;
    }

    public String getScore()
    {
        return score;
    }

    public String toString()
    {
        String lineSep = System.getProperty("line.separator");
        StringBuffer sb = new StringBuffer();
        StringBuffer vectorsSb = new StringBuffer();
        // Get this Class
        Class thisClass = this.getClass();
        sb.append("Object Type = ").append(thisClass.getName()).append(lineSep);

        // Use reflection to loop through all the fields
        Field[] fields = thisClass.getDeclaredFields();
        for(int i = 0; i < fields.length; i++)
        {
            try
            {
                if(fields[i].getType().equals(Class.forName("java.lang.String")))
                {
                    sb.append(fields[i].getName()).append(" = ").append(fields[i].get(this)).append(lineSep);
                }
                else if(fields[i].getType().equals(Class.forName("java.util.List")))
                {
                    List vect = (List)fields[i].get(this);
                    if(vect != null)
                    {
                        for (int j = 0; j < vect.size(); j++) 
                        {
                            vectorsSb.append(vect.get(j).toString());
                        }
                    }
                }
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        }

        sb.append(vectorsSb);

        return sb.toString();
    }
}