package com.ftd.osp.utilities.order.vo;

import java.io.Serializable;
import java.lang.reflect.*;
import java.util.List;

/**
 * AVS Addresses Value Object
 * This is a sub collection within Order Details value object
 *
 */
public class AVSAddressVO extends BaseVO implements Serializable 
{
    private long avsAddressId;
    private long recipientId;
    private String address;
    private String city;
    private String stateProvince;
    private String postalCode;
    private String country;
    private String latitude;
    private String longitude;
    private String avsPerformed;
    private String avsPerformedOrigin;
    private String result;
    private String overrideFlag;
    private String entityType;
    private String threshold;
    private List<AVSAddressScoreVO> scores;


    public AVSAddressVO()
    {
    }

    public long getAvsAddressId()
    {
        return avsAddressId;
    }

    public void setAvsAddressId(long newAvsAddressId)
    {
        if(valueChanged(avsAddressId,newAvsAddressId)){
            setChanged(true);
        }    
        avsAddressId = newAvsAddressId;
    }

    public long getRecipientId()
    {
        return recipientId;
    }

    public void setRecipientId(long newRecipientId)
    {
        if(valueChanged(recipientId,newRecipientId)){
            setChanged(true);
        }    
        recipientId = newRecipientId;
    }

    public String getCity()
    {
        return city;
    }

    public void setCity(String newCity)
    {
        if(valueChanged(city,newCity)){
            setChanged(true);
        }    
        city = newCity;
    }

    public String getStateProvince()
    {
        return stateProvince;
    }

    public void setStateProvince(String newStateProvince)
    {
        if(valueChanged(stateProvince,newStateProvince)){
            setChanged(true);
        }    
        stateProvince = newStateProvince;
    }

    public String getPostalCode()
    {
        return postalCode;
    }

    public void setPostalCode(String newPostalCode)
    {
        if(valueChanged(postalCode,newPostalCode)){
            setChanged(true);
        }    
        postalCode = newPostalCode;
    }

    public String getCountry()
    {
        return country;
    }

    public void setCountry(String newCountry)
    {
        if(valueChanged(country,newCountry)){
            setChanged(true);
        }    
        country = newCountry;
    }

    public String getLatitude()
    {
        return latitude;
    }

    public void setLatitude(String newLatitude)
    {
        if(valueChanged(latitude,newLatitude)){
            setChanged(true);
        }    
        latitude = newLatitude;
    }

    public String getLongitude()
    {
        return longitude;
    }

    public void setLongitude(String newLongitude)
    {
        if(valueChanged(longitude,newLongitude)){
            setChanged(true);
        }    
        longitude = newLongitude;
    }

    public String getAddress()
    {
        return address;
    }

    public void setAddress(String newAddress)
    {
        if(valueChanged(address,newAddress)){
            setChanged(true);
        }    
        this.address = newAddress;
    }

    public String getAvsPerformed()
    {
        return avsPerformed;
    }

    public void setAvsPerformed(String newAvsPerformed)
    {
        if(valueChanged(avsPerformed,newAvsPerformed)){
            setChanged(true);
        }    
        this.avsPerformed = newAvsPerformed;
    }

    public String getResult()
    {
        return result;
    }

    public void setResult(String newResult)
    {
        if(valueChanged(result,newResult)){
            setChanged(true);
        }    
        this.result = newResult;
    }

    public String getOverrideflag()
    {
        return overrideFlag;
    }

    public void setOverrideFlag(String newOverrideFlag)
    {
        if(valueChanged(overrideFlag,newOverrideFlag)){
            setChanged(true);
        }    
       this.overrideFlag = newOverrideFlag;
    }

    public String getEntityType()
    {
        return entityType;
    }

    public void setEntityType(String newEntityType)
    {
        if(valueChanged(entityType,newEntityType)){
            setChanged(true);
        }    
        this.entityType = newEntityType;
    }

    public void setScores(List<AVSAddressScoreVO> scores)
    {
        this.scores = scores;
    }

    public List<AVSAddressScoreVO> getScores()
    {
        return scores;
    }
    
    

    public String getThreshold() {
		return threshold;
	}

	public void setThreshold(String threshold) {
		this.threshold = threshold;
	}

	public String toString()
    {
        String lineSep = System.getProperty("line.separator");
        StringBuffer sb = new StringBuffer();
        StringBuffer vectorsSb = new StringBuffer();
        // Get this Class
        Class thisClass = this.getClass();
        sb.append("Object Type = ").append(thisClass.getName()).append(lineSep);

        // Use reflection to loop through all the fields
        Field[] fields = thisClass.getDeclaredFields();
        for(int i = 0; i < fields.length; i++)
        {
            try
            {
                if(fields[i].getType().equals(Class.forName("java.lang.String")))
                {
                    sb.append(fields[i].getName()).append(" = ").append(fields[i].get(this)).append(lineSep);
                }
                else if(fields[i].getType().equals(Class.forName("java.util.List")))
                {
                    List vect = (List)fields[i].get(this);
                    if(vect != null)
                    {
                        for (int j = 0; j < vect.size(); j++) 
                        {
                            vectorsSb.append(vect.get(j).toString());
                        }
                    }
                }
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        }

        sb.append(vectorsSb);

        return sb.toString();
    }

	public void setAvsPerformedOrigin(String avsPerformedOrigin) {
		this.avsPerformedOrigin = avsPerformedOrigin;
	}

	public String getAvsPerformedOrigin() {
		return avsPerformedOrigin;
	}
}