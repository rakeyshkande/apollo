package com.ftd.osp.utilities.order.vo;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.List;

/**
 * Order Detail Extensions Value Object that maps to ORDER_DETAIL_EXTENSIONS database table.
 * This represents optional order detail information as name value pairs.  
 * Although originally developed to propogate Ariba info (for certain clients) 
 * through Scrub to the HP so it could be included on monthly revenue reports, 
 * it certainly could be utilized for other purposes as well.  Note this info is 
 * NOT validated in Scrub - it merely flows through the system as extra baggage.
 */
public class OrderDetailExtensionsVO extends BaseVO implements Serializable {

  private long   extensionId;    // Primary key 
  private long   orderDetailId;  // Foreign key associating this with order_detail
  private String infoName;       // Name of data item
  private String infoData;       // Data item value

  public OrderDetailExtensionsVO() {
  }
  
  public void setExtensionId(long a_val) {extensionId = a_val;}
  public long getExtensionId() {return extensionId;}
  public void setOrderDetailId(long a_val) {orderDetailId = a_val;}
  public long getOrderDetailId() {return orderDetailId;}
  public void setInfoName(String a_val) {infoName = a_val;}
  public String getInfoName() {return infoName;}
  public void setInfoData(String a_val) {infoData = a_val;}
  public String getInfoData() {return infoData;}
}