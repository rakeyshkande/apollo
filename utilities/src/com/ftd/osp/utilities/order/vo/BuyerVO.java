package com.ftd.osp.utilities.order.vo;

import java.util.List;
import java.lang.reflect.*;
import java.io.Serializable;

/**
 * Buyer Value Object that maps to BUYERS database table
 * This is a sub collection within Order value object
 *
 * @author Doug Johnson
 * 
 * emueller, 4/19/05.  Added lockedBy property for PhaseIII.  This property
 * will be populated by the application logic.
 * 
 */
public class BuyerVO extends BaseVO implements Serializable  
{
  private long buyerId;
  private String customerId;
  private String lastName;
  private String firstName;
  private String autoHold;
  private String bestCustomer;

  private List buyerEmails;
  private List buyerPhones;
  private List buyerAddresses;
  private List buyerComments;
  private List ncoa;
  private String status;
    private String lockedBy;
    private Long cleanCustomerId;

  public BuyerVO()
  {}

  public long getBuyerId()
  {
    return buyerId;
  }

  public void setBuyerId(long newBuyerId)
  {
    if(valueChanged(buyerId, newBuyerId))
    {
        setChanged(true);
    }
    buyerId = newBuyerId;
  }

  public String getCustomerId()
  {
    return customerId;
  }

  public void setCustomerId(String newCustomerId)
  {
    if(valueChanged(customerId, newCustomerId))
    {
        setChanged(true);
    }
    customerId = newCustomerId;
  }

  public String getLastName()
  {
    return lastName;
  }

  public void setLastName(String newLastName)
  {
    if(valueChanged(lastName, newLastName))
    {
        setChanged(true);
    }
    lastName = newLastName;
  }

  public String getFirstName()
  {
    return firstName;
  }

  public void setFirstName(String newFirstName)
  {
    if(valueChanged(firstName, newFirstName))
    {
        setChanged(true);
    }
    firstName = newFirstName;
  }

  public String getAutoHold()
  {
    return autoHold;
  }

  public void setAutoHold(String newAutoHold)
  {
    if(valueChanged(autoHold, newAutoHold))
    {
        setChanged(true);
    }
    autoHold = newAutoHold;
  }

  public String getBestCustomer()
  {
    return bestCustomer;
  }

  public void setBestCustomer(String newBestCustomer)
  {
    if(valueChanged(bestCustomer, newBestCustomer))
    {
        setChanged(true);
    }
    bestCustomer = newBestCustomer;
  }

  public List getBuyerEmails()
  {
    return buyerEmails;
  }

  public void setBuyerEmails(List newBuyerEmails)
  {
    buyerEmails = newBuyerEmails;
  }

  public List getBuyerPhones()
  {
    return buyerPhones;
  }

  public void setBuyerPhones(List newBuyerPhones)
  {
    buyerPhones = newBuyerPhones;
  }

  public List getBuyerAddresses()
  {
    return buyerAddresses;
  }

  public void setBuyerAddresses(List newBuyerAddresses)
  {
    buyerAddresses = newBuyerAddresses;
  }

  public List getBuyerComments()
  {
    return buyerComments;
  }

  public void setBuyerComments(List newBuyerComments)
  {
    buyerComments = newBuyerComments;
  }

  public List getNcoa()
  {
    return ncoa;
  }

  public void setNcoa(List newNcoa)
  {
    ncoa = newNcoa;
  }
  
  public String getStatus()
  {
    return status;
  }

  public void setStatus(String newStatus)
  {
    if(valueChanged(status, newStatus))
    {
        setChanged(true);
    }
    status = newStatus;
  }
  
    public String toString()
    {
        String lineSep = System.getProperty("line.separator");
        StringBuffer sb = new StringBuffer();
        StringBuffer vectorsSb = new StringBuffer();
        // Get this Class
        Class thisClass = this.getClass();
        sb.append("Object Type = ").append(thisClass.getName()).append(lineSep);
        
        // Use reflection to loop through all the fields
        Field[] fields = thisClass.getDeclaredFields();
        for(int i = 0; i < fields.length; i++)
        {
            try
            {
                if(fields[i].getType().equals(Class.forName("java.lang.String")))
                {
                    sb.append(fields[i].getName()).append(" = ").append(fields[i].get(this)).append(lineSep);
                }
                else if(fields[i].getType().equals(Class.forName("java.util.List")))
                {
                    List vect = (List)fields[i].get(this);
                    if(vect != null)
                    {
                        for (int j = 0; j < vect.size(); j++) 
                        {
                            vectorsSb.append(vect.get(j).toString());
                        }
                    }
                }
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        }

        sb.append(vectorsSb);
        
        return sb.toString();
    }

    public String getLockedBy()
    {
        return lockedBy;
    }

    public void setLockedBy(String lockedBy)
    {
        this.lockedBy = lockedBy;
    }

    public Long getCleanCustomerId()
    {
        return cleanCustomerId;
    }

    public void setCleanCustomerId(Long cleanCustomerId)
    {
        this.cleanCustomerId = cleanCustomerId;
    }
}
