package com.ftd.osp.utilities.order.vo;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.List;

/**
 * Destinations Value Object that maps to DESTINATIONS database table
 * This is a sub collection within Recipients value object
 *
 * @author Doug Johnson
 */
public class DestinationsVO extends BaseVO implements Serializable  
{
  private long destinationId;
  private long recipientId;
  private String destinationType;
  private String name;
  private String info;

  public DestinationsVO()
  {
  }

  public long getDestinationId()
  {
    return destinationId;
  }

  public void setDestinationId(long newDestinationId)
  {
    if(valueChanged(destinationId,newDestinationId)){
      setChanged(true);
    }
    destinationId = newDestinationId;
  }

  public long getRecipientId()
  {
    return recipientId;
  }

  public void setRecipientId(long newRecipientId)
  {
    if(valueChanged(recipientId,newRecipientId)){
      setChanged(true);
    }  
    recipientId = newRecipientId;
  }

  public String getDestinationType()
  {
    return destinationType;
  }

  public void setDestinationType(String newDestinationType)
  {
    if(valueChanged(destinationType,newDestinationType)){
      setChanged(true);
    }  
    destinationType = newDestinationType;
  }

  public String getName()
  {
    return name;
  }

  public void setName(String newName)
  {
    if(valueChanged(name,newName)){
      setChanged(true);
    }  
    name = newName;
  }

  public String getInfo()
  {
    return info;
  }

  public void setInfo(String newInfo)
  {
    if(valueChanged(info,newInfo)){
      setChanged(true);
    }  
    info = newInfo;
  }

  public String toString()
  {
    String lineSep = System.getProperty("line.separator");
    StringBuffer sb = new StringBuffer();
    StringBuffer listSb = new StringBuffer();
    // Get this Class
    Class thisClass = this.getClass();
    sb.append("Object Type = ").append(thisClass.getName()).append(lineSep);
        
    // Use reflection to loop through all the fields
    Field[] fields = thisClass.getDeclaredFields();
    for(int i = 0; i < fields.length; i++)
    {
        try
        {
            if(fields[i].getType().equals(Class.forName("java.lang.String")))
            {
                sb.append(fields[i].getName()).append(" = ").append(fields[i].get(this)).append(lineSep);
            }
            else if(fields[i].getType().equals(Class.forName("java.util.List")))
            {
                List list = (List)fields[i].get(this);
                if(list != null)
                {
                    for (int j = 0; j < list.size(); j++) 
                    {
                        listSb.append(list.get(j).toString());
                    }
                }
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    sb.append(listSb);
        
    return sb.toString();
  } 
}