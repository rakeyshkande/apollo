package com.ftd.osp.utilities.order.vo;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.List;

/**
 * Recipient Addresses Value Object that maps to RECIPIENT_ADDRESSES database table
 * This is a sub collection within Recipient value object
 *
 * @author Doug Johnson
 */
public class RecipientAddressesVO extends BaseVO  
{
  private long recipientAddressId;
  private long recipientId;
  private String addressType;
  private String addressLine1;
  private String addressLine2;
  private String city;
  private String stateProvince;
  private String postalCode;
  private String country;
  private String county;
  private long destinationId;
  private String international;
  private String geofindMatchCode = "0";
  private String latitude = "0";
  private String longitude = "0";
  private String Name;
  private String Info;
  private String addressTypeCode;


  public RecipientAddressesVO()
  {
  }

  public long getRecipientAddressId()
  {
    return recipientAddressId;
  }

  public void setRecipientAddressId(long newRecipientAddressId)
  {
    if(valueChanged(recipientAddressId,newRecipientAddressId)){
      setChanged(true);
    }      
    recipientAddressId = newRecipientAddressId;
  }

  public long getRecipientId()
  {
    return recipientId;
  }

  public void setRecipientId(long newRecipientId)
  {
    if(valueChanged(recipientId,newRecipientId)){
      setChanged(true);
    }      
    recipientId = newRecipientId;
  }

  public String getAddressType()
  {
    return addressType;
  }

  public void setAddressType(String newAddressType)
  {
    if(valueChanged(addressType,newAddressType)){
      setChanged(true);
    }      
    addressType = newAddressType;
  }

  public String getAddressLine1()
  {
    return addressLine1;
  }

  public void setAddressLine1(String newAddressLine1)
  {
    if(valueChanged(addressLine1,newAddressLine1)){
      setChanged(true);
    }      
    addressLine1 = newAddressLine1;
  }

  public String getAddressLine2()
  {
    return addressLine2;
  }

  public void setAddressLine2(String newAddressLine2)
  {
    if(valueChanged(addressLine2,newAddressLine2)){
      setChanged(true);
    }      
    addressLine2 = newAddressLine2;
  }

  public String getCity()
  {
    return city;
  }

  public void setCity(String newCity)
  {
    if(valueChanged(city,newCity)){
      setChanged(true);
    }      
    city = newCity;
  }

  public String getStateProvince()
  {
    return stateProvince;
  }

  public void setStateProvince(String newStateProvince)
  {
    if(valueChanged(stateProvince,newStateProvince)){
      setChanged(true);
    }      
    stateProvince = newStateProvince;
  }

  public String getPostalCode()
  {
    return postalCode;
  }

  public void setPostalCode(String newPostalCode)
  {
    if(valueChanged(postalCode,newPostalCode)){
      setChanged(true);
    }      
    postalCode = newPostalCode;
  }

  public String getCountry()
  {
    return country;
  }

  public void setCountry(String newCountry)
  {
    if(valueChanged(country,newCountry)){
      setChanged(true);
    }      
    country = newCountry;
  }

  public String getCounty()
  {

    return county;
  }

  public void setCounty(String newCounty)
  {
    if(valueChanged(county,newCounty)){
      setChanged(true);
    }      
    county = newCounty;
  }







  public long getDestinationId()
  {
    return destinationId;
  }

  public void setDestinationId(long newDestinationId)
  {
    if(valueChanged(destinationId,newDestinationId)){
      setChanged(true);
    }      
    destinationId = newDestinationId;
  }

  public String getInternational()
  {
    return international;
  }

  public void setInternational(String newInternational)
  {
    if(valueChanged(international,newInternational)){
      setChanged(true);
    }      
    international = newInternational;
  }

  public String toString()
  {
    String lineSep = System.getProperty("line.separator");
    StringBuffer sb = new StringBuffer();
    StringBuffer listSb = new StringBuffer();
    // Get this Class
    Class thisClass = this.getClass();
    sb.append("Object Type = ").append(thisClass.getName()).append(lineSep);
        
    // Use reflection to loop through all the fields
    Field[] fields = thisClass.getDeclaredFields();
    for(int i = 0; i < fields.length; i++)
    {
        try
        {
            if(fields[i].getType().equals(Class.forName("java.lang.String")))
            {
                sb.append(fields[i].getName()).append(" = ").append(fields[i].get(this)).append(lineSep);
            }
            else if(fields[i].getType().equals(Class.forName("java.util.List")))
            {
                List list = (List)fields[i].get(this);
                if(list != null)
                {
                    for (int j = 0; j < list.size(); j++) 
                    {
                        listSb.append(list.get(j).toString());
                    }
                }
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    sb.append(listSb);
        
    return sb.toString();
  }

  public String getGeofindMatchCode()
  {
    return geofindMatchCode;
  }

  public void setGeofindMatchCode(String newGeofindMatchCode)
  {
    if(valueChanged(geofindMatchCode,checkNullInt(newGeofindMatchCode))){
      setChanged(true);
    }      
    geofindMatchCode = checkNullInt(newGeofindMatchCode);
  }

  public String getLatitude()
  {
    return latitude;
  }

  public void setLatitude(String newLatitude)
  {
    if(valueChanged(latitude,checkNullInt(newLatitude))){
      setChanged(true);
    }      
    latitude = checkNullInt(newLatitude);
  }

  public String getLongitude()
  {
    return longitude;
  }

  public void setLongitude(String newLongitude)
  {
    if(valueChanged(longitude,checkNullInt(newLongitude))){
      setChanged(true);
    }      
    longitude = checkNullInt(newLongitude);
  }

  public String getName()
  {
    return Name;
  }

  public void setName(String newName)
  {
    if(valueChanged(Name,newName)){
      setChanged(true);
    }      
    Name = newName;
  }

  public String getInfo()
  {
    return Info;
  }

  public void setInfo(String newInfo)
  {
    if(valueChanged(Info,newInfo)){
      setChanged(true);
    }      
    Info = newInfo;
  }

  public String getAddressTypeCode()
  {
    return addressTypeCode;
  }

  public void setAddressTypeCode(String newAddressTypeCode)
  {
    if(valueChanged(addressTypeCode,newAddressTypeCode)){
      setChanged(true);
    }      
    addressTypeCode = newAddressTypeCode;
  }
}