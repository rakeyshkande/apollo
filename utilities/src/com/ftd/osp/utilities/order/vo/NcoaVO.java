package com.ftd.osp.utilities.order.vo;

import org.w3c.dom.Document;
import java.util.List;
import java.lang.reflect.*;
import java.io.Serializable;

public class NcoaVO implements Serializable  
{

  private long ncoaId;
  private long buyerId;
  private String deliverability;
  private String nixieGroup;
  private String dupeSetUniqueness;
  private String emsAddressScore;

  public NcoaVO()
  {}

  public long getNcoaId()
  {
    return ncoaId;
  }

  public void setNcoaId(long newNcoaId)
  {
    ncoaId = newNcoaId;
  }

  public long getBuyerId()
  {
    return buyerId;
  }

  public void setBuyerId(long newBuyerId)
  {
    buyerId = newBuyerId;
  }

  public String getDeliverability()
  {
    return deliverability;
  }

  public void setDeliverability(String newDeliverability)
  {
    deliverability = newDeliverability;
  }

  public String getNixieGroup()
  {
    return nixieGroup;
  }

  public void setNixieGroup(String newNixieGroup)
  {
    nixieGroup = newNixieGroup;
  }

  public String getDupeSetUniqueness()
  {
    return dupeSetUniqueness;
  }

  public void setDupeSetUniqueness(String newDupeSetUniqueness)
  {
    dupeSetUniqueness = newDupeSetUniqueness;
  }

  public String getEmsAddressScore()
  {
    return emsAddressScore;
  }

  public void setEmsAddressScore(String newEmsAddressScore)
  {
    emsAddressScore = newEmsAddressScore;
  }



   public String toString()
    {
        String lineSep = System.getProperty("line.separator");
        StringBuffer sb = new StringBuffer();
        StringBuffer vectorsSb = new StringBuffer();
        // Get this Class
        Class thisClass = this.getClass();
        sb.append("Object Type = ").append(thisClass.getName()).append(lineSep);
        
        // Use reflection to loop through all the fields
        Field[] fields = thisClass.getDeclaredFields();
        for(int i = 0; i < fields.length; i++)
        {
            try
            {
                if(fields[i].getType().equals(Class.forName("java.lang.String")))
                {
                    sb.append(fields[i].getName()).append(" = ").append(fields[i].get(this)).append(lineSep);
                }
                else if(fields[i].getType().equals(Class.forName("java.util.List")))
                {
                    List vect = (List)fields[i].get(this);
                    if(vect != null)
                    {
                        for (int j = 0; j < vect.size(); j++) 
                        {
                            vectorsSb.append(vect.get(j).toString());
                        }
                    }
                }
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        }

        sb.append(vectorsSb);
        
        return sb.toString();
    }

}

