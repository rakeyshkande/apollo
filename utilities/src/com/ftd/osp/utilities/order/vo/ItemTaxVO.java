/**
 * 
 */
package com.ftd.osp.utilities.order.vo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author smeka
 *
 */
@SuppressWarnings("serial")
public class ItemTaxVO extends BaseVO {
	
	private List<TaxVO> taxSplit;
	
	public static final String ZERO_VALUE = "0";
	
	private String taxAmount;
	private BigDecimal totalTaxrate;
	private String totalTaxDescription;
	
	private String shippingTax;
	private String serviceFeeTax;
	private String productTax;
	
	public List<TaxVO> getTaxSplit() {
		if(this.taxSplit == null) {
			this.taxSplit = new ArrayList<TaxVO>();
		}
		return taxSplit;
	}
	
	public void setTaxSplit(List<TaxVO> taxSplit) {	
		if(this.taxSplit == null) {
			this.taxSplit = new ArrayList<TaxVO>();
		}
		this.taxSplit = taxSplit;
	}
	
	public BigDecimal getTotalTaxrate() {
		return totalTaxrate;
	}
	public void setTotalTaxrate(BigDecimal bigDecimal) {
		this.totalTaxrate = bigDecimal;
	}
	public String getTaxAmount() {		
		return (taxAmount == null || taxAmount.length() == 0) ? ZERO_VALUE : taxAmount; 
	}

	public void setTaxAmount(String newTaxAmount) {
		if (valueChanged(taxAmount, newTaxAmount)) {
			setChanged(true);
		}
		taxAmount = newTaxAmount;
	}
	
	public String getShippingTax() {
		return shippingTax;
	}

	public void setShippingTax(String newShippingTax) {
		if (valueChanged(shippingTax, newShippingTax)) {
			setChanged(true);
		}
		shippingTax = newShippingTax;
	}
	
	public void setServiceFeeTax(String serviceFeeTax) {
		this.serviceFeeTax = serviceFeeTax;
	}

	public String getServiceFeeTax() {
		return serviceFeeTax;
	}

	public void setProductTax(String productTax) {
		this.productTax = productTax;
	}

	public String getProductTax() {
		return productTax;
	}
	
	public String getTotalTaxDescription() {
		return totalTaxDescription;
	}

	public void setTotalTaxDescription(String totalTaxDescription) {
		this.totalTaxDescription = totalTaxDescription;
	}

	@Override
	public String toString() {
		return "ItemTaxVO [taxSplit=" + taxSplit + ", totalTaxAmount=" + taxAmount
				+ ", totalTaxrate=" + totalTaxrate + ", totalTaxDescription="
				+ totalTaxDescription + "]";
	}	

}
