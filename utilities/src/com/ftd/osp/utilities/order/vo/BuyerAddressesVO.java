package com.ftd.osp.utilities.order.vo;

import java.util.List;
import java.lang.reflect.*;
import java.io.Serializable;

/**
 * Buyer Addresses Value Object that maps to BUYER_ADDRESSES database table
 * This is a sub collection within Buyer value object
 *
 * @author Doug Johnson
 */
public class BuyerAddressesVO extends BaseVO implements Serializable  
{
  private long buyerAddressId;
  private long buyerId;
  private String addressType;
  private String addressLine1;
  private String addressLine2;
  private String city;
  private String stateProv;
  private String postalCode;
  private String country;
  private String county;
  private String addressEtc;
  
 
  public BuyerAddressesVO()
  {}

  public long getBuyerAddressId()
  {
    return buyerAddressId;
  }

  public void setBuyerAddressId(long newBuyerAddressId)
  {
    if(valueChanged(buyerAddressId, newBuyerAddressId))
    {
        setChanged(true);
    }  
    buyerAddressId = newBuyerAddressId;
  }

  public long getBuyerId()
  {
    return buyerId;
  }

  public void setBuyerId(long newBuyerId)
  {
    if(valueChanged(buyerId, newBuyerId))
    {
        setChanged(true);
    }  
    buyerId = newBuyerId;
  }

  public String getAddressType()
  {
    return addressType;
  }
  
  public void setAddressType(String newAddressType)
  {
    if(valueChanged(addressType, newAddressType))
    {
        setChanged(true);
    }  
    addressType = newAddressType;
  } 

  public String getAddressLine1()
  {
    return addressLine1;
  }

  public void setAddressLine1(String newAddressLine1)
  {
    if(valueChanged(addressLine1, newAddressLine1))
    {
        setChanged(true);
    }  
    addressLine1 = newAddressLine1;
  }

  public String getAddressLine2()
  {
    return addressLine2;
  }

  public void setAddressLine2(String newAddressLine2)
  {
    if(valueChanged(addressLine2, newAddressLine2))
    {
        setChanged(true);
    }  
    addressLine2 = newAddressLine2;
  }

  public String getCity()
  {
    return city;
  }

  public void setCity(String newCity)
  {
    if(valueChanged(city, newCity))
    {
        setChanged(true);
    }  
    city = newCity;
  }

  public String getStateProv()
  {
    return stateProv;
  }

  public void setStateProv(String newStateProv)
  {
    if(valueChanged(stateProv, newStateProv))
    {
        setChanged(true);
    }  
    stateProv = newStateProv;
  }

  public String getPostalCode()
  {
    return postalCode;
  }

  public void setPostalCode(String newPostalCode)
  {
    if(valueChanged(postalCode, newPostalCode))
    {
        setChanged(true);
    }  
    postalCode = newPostalCode;
  }

  public String getCountry()
  {
    return country;
  }

  public void setCountry(String newCountry)
  {
    if(valueChanged(country, newCountry))
    {
        setChanged(true);
    }  
    country = newCountry;
  }

  public String getCounty()
  {
    return county;
  }

  public void setCounty(String newCounty)
  {
    if(valueChanged(county, newCounty))
    {
        setChanged(true);
    }  
    county = newCounty;
  }

  public String getAddressEtc()
  {
    return addressEtc;
  }

  public void setAddressEtc(String newAddressEtc)
  {
    if(valueChanged(addressEtc, newAddressEtc))
    {
        setChanged(true);
    }  
    addressEtc = newAddressEtc;
  }
  
    public String toString()
    {
        String lineSep = System.getProperty("line.separator");
        StringBuffer sb = new StringBuffer();
        StringBuffer vectorsSb = new StringBuffer();
        // Get this Class
        Class thisClass = this.getClass();
        sb.append("Object Type = ").append(thisClass.getName()).append(lineSep);
        
        // Use reflection to loop through all the fields
        Field[] fields = thisClass.getDeclaredFields();
        for(int i = 0; i < fields.length; i++)
        {
            try
            {
                if(fields[i].getType().equals(Class.forName("java.lang.String")))
                {
                    sb.append(fields[i].getName()).append(" = ").append(fields[i].get(this)).append(lineSep);
                }
                else if(fields[i].getType().equals(Class.forName("java.util.List")))
                {
                    List vect = (List)fields[i].get(this);
                    if(vect != null)
                    {
                        for (int j = 0; j < vect.size(); j++) 
                        {
                            vectorsSb.append(vect.get(j).toString());
                        }
                    }
                }
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        }

        sb.append(vectorsSb);
        
        return sb.toString();
    }
}

