package com.ftd.osp.utilities.order.vo;

import java.math.BigDecimal;

/**
 * Contains Values from FTD_Apps.Global_Parms used for Recalculation logic.
 * These are retrieved once during each recalculation from the global parms table.
 */
public class RecalculateOrderGlobalParmsVO
{
  private BigDecimal freshCutServiceCharge;
  private BigDecimal specialServiceCharge;
  
  public RecalculateOrderGlobalParmsVO()
  {
  }


  public void setFreshCutServiceCharge(BigDecimal freshCutServiceCharge)
  {
    this.freshCutServiceCharge = freshCutServiceCharge;
  }

  public BigDecimal getFreshCutServiceCharge()
  {
    return freshCutServiceCharge;
  }

  public void setSpecialServiceCharge(BigDecimal specialServiceCharge)
  {
    this.specialServiceCharge = specialServiceCharge;
  }

  public BigDecimal getSpecialServiceCharge()
  {
    return specialServiceCharge;
  }
}
