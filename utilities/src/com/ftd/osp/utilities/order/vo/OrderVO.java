package com.ftd.osp.utilities.order.vo;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import org.w3c.dom.Document;
import java.lang.reflect.*;

import java.math.BigDecimal;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.List;

/**
 * Order Value Object that maps to ORDERS database table
 * This is the primary collection for working with an Order.  It contains
 * all information about an order. 
 *
 * @author Doug Johnson
 */
public class OrderVO extends BaseVO implements OrderStatus
{

  private Document document = null;
  private long timeStamp;
  private String GUID = "";
  private String orderXMLString = null;

  
  private String masterOrderNumber;
  private long buyerId;
  private String orderTotal;
  private String productsTotal;
  private String taxTotal;
  private String serviceFeeTotal;
  private String shippingFeeTotal;
  private String serviceFeeTotalSavings;
  private String shippingFeeTotalSavings;  
  private long buyerEmailId;
  private String buyerEmailAddress;
  private String orderDate;
  private String orderOrigin;
  private long membershipId;
  private String csrId;
  private String addOnAmountTotal;
  private String partnershipBonusPoints;
  private String sourceCode;
  private String callTime;
  private String characterMappingFlag;
  
  private String dnisCode;
  private String datamartUpdateDate;
  private String discountTotal;
  private String yellowPagesCode;
  private String aribaBuyerAsnNumber;
  private String aribaBuyerCookie;
  private String aribaPayload;
  private String socketTimestamp;
  private String mpRedemptionRateAmt;

  private List <OrderExtensionsVO> orderExtensions;

  private List orderDetail;
  private List coBrand;
  private List payments;
  private List buyer;
  private List memberships;
  private String status;
  private List orderContactInfo;
  private String sourceDescription;
  private String partnerId;
  private String companyId;
  private String companyName;
  private String companyUrl;
  private List FraudComments;
  private String lastUpdateTimestamp;
  private String fraudFlag;
  private String coBrandCreditCardCode;
  private List fraudCodes;
  private String previousStatus;
  private String lossPreventionIndicator;
  private SimpleDateFormat orderDateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");
  private boolean oeOrder = false;
  private HashSet<String> preferredProcessingPartners;
  private HashSet<String> preferredProcessingPartnerNames;
  private String fuelSurchargeDescription;
  private String applySurchargeCode;
  private String fuelSurchargeFee;
  private String buyerSignedIn;
  private String freeShippingUseFlag;
  private String freeShippingPurchaseFlag;//this is backed flag used in order validator/scrub to see if free shipping was purchased 
  private String buyerHasFreeShippingFlag;
  private String freeShippingMembershipInCartFlag;//this is a flag sent in from website indicating whether or not a free shipping item exists in the customer's cart
  private String languageId;
  private boolean isBuyerEmailChangedInScrub;
  private String mrcntChannelIcon;
  private String isMercentOrder = "N";
  private String mrcntChannelName;
  private boolean allowPCCheckUpdateOrder;
  private String buyerHasPCMembershipFlag;
  private boolean buyerCamsVerified;
  private boolean isPartnerOrder;
  private String partnerImgURL;
  private String partnerName;
  private String sourceType;
  private String jointCartIndicator;
  private String addOnDiscountAmount;
  //SP-101 setting invokedAsynchronously as default to false .It will be true incase of MDB flow.
  private boolean invokedAsynchronously=false;
  private String oeBuyerHasFreeShipping;//QE3SP-15 this flag indicates whether or not the buyer for a JOE order has Free Shipping.  This flag
										//will be used in Recalculate Order if a CAMS timeout is encountered.  This is to help prevent the $0
										//orders from being dispatched to Clean.
  private boolean isResetCardinal = false;
  private String orderCalcType;
  
  public OrderVO()
  { 
  } 

  public Document getXMLDocument()
  {
    return this.document;
  }
  
  public void setXMLDocument(Document document)
  {  
    this.document = document;
  }

  public void setBuyerCamsVerified(boolean buyerCamsVerified){
  	this.buyerCamsVerified = buyerCamsVerified;
  }
  
  public boolean getBuyerCamsVerified(){
  	return buyerCamsVerified;
  }     
  
  public long getTimeStamp()
  {
    return timeStamp;
  }

  public void setTimeStamp(long newTimeStamp)
  {
    if(valueChanged(timeStamp, newTimeStamp))
    {
        setChanged(true);
    }  
    timeStamp = newTimeStamp;
  }

  public String getOrderXMLString()
  {
    return this.orderXMLString;
  }

  public void setOrderXMLString(String orderXMLString)
  {
    if(valueChanged(this.orderXMLString, orderXMLString))
    {
        setChanged(true);
    }  
    this.orderXMLString = orderXMLString;
  }
  
  public String getGUID()
  {
    return this.GUID;
  }

  public void setGUID(String GUID)
  {
    if(valueChanged(this.GUID, GUID))
    {
        setChanged(true);
    }
    this.GUID = GUID;
  }
  
  public String getMasterOrderNumber()
  {
    return masterOrderNumber;
  }

  public void setMasterOrderNumber(String newMasterOrderNumber)
  {
    if(valueChanged(masterOrderNumber, newMasterOrderNumber))
    {
        setChanged(true);
    }  
    masterOrderNumber = newMasterOrderNumber;
  }

  public long getBuyerId()
  {
    return buyerId;
  }

  public void setBuyerId(long newBuyerId)
  {
    if(valueChanged(buyerId, newBuyerId))
    {
        setChanged(true);
    }  
    buyerId = newBuyerId;
  }

  public String getOrderTotal()
  {
    return orderTotal;
  }

  public void setOrderTotal(String newOrderTotal)
  {
    if(valueChanged(orderTotal, newOrderTotal))
    {
        setChanged(true);
    }      
    orderTotal = checkNullDouble(newOrderTotal);
  }

  public String getProductsTotal()
  {
    return productsTotal;
  }

  public void setProductsTotal(String newProductsTotal)
  {
    if(valueChanged(productsTotal, newProductsTotal))
    {
        setChanged(true);
    }  
    productsTotal = checkNullDouble(newProductsTotal);
  }

  public String getTaxTotal()
  {
    return taxTotal;
  }

  public void setTaxTotal(String newTaxTotal)
  {
    if(valueChanged(taxTotal, newTaxTotal))
    {
        setChanged(true);
    }  
    taxTotal = checkNullDouble(newTaxTotal);
  }

  public String getServiceFeeTotal()
  {
    return serviceFeeTotal;
  }

  public void setServiceFeeTotal(String newServiceFeeTotal)
  {
    if(valueChanged(serviceFeeTotal, newServiceFeeTotal))
    {
        setChanged(true);
    }  
    serviceFeeTotal = checkNullDouble(newServiceFeeTotal);
  }

  public String getShippingFeeTotal()
  {
    return shippingFeeTotal;
  }

  public void setShippingFeeTotal(String newShippingFeeTotal)
  {
    if(valueChanged(shippingFeeTotal, newShippingFeeTotal))
    {
        setChanged(true);
    }  
    shippingFeeTotal = checkNullDouble(newShippingFeeTotal);
  }

  public String getMpRedemptionRateAmt()
  {
    return mpRedemptionRateAmt;
  }

  public void setMpRedemptionRateAmt(String newMpRedemptionRateAmt)
  {
    if(valueChanged(mpRedemptionRateAmt, newMpRedemptionRateAmt))
    {
        setChanged(true);
    }      
    mpRedemptionRateAmt = checkNullDouble(newMpRedemptionRateAmt);
  }

  public long getBuyerEmailId()
  {
    return buyerEmailId;
  }

  public void setBuyerEmailId(long newBuyerEmailId)
  {
    if(valueChanged(buyerEmailId, newBuyerEmailId))
    {
        setChanged(true);
    }  
    buyerEmailId = newBuyerEmailId;
  }

  public String getOrderDate()
  {
    return orderDate;
  }

  public void setOrderDate(String newOrderDate)
  {
    if(valueChanged(orderDate, newOrderDate))
    {
        setChanged(true);
    }    
    orderDate = newOrderDate;
  }
  
  public void setOrderDate(Date orderDate)
    {
      setOrderDate(orderDateFormat.format(orderDate));
    }
  
    public Date getOrderDateTime()
    {
      try {
      if (getOrderDate() != null) {
          return orderDateFormat.parse(getOrderDate());
      } else {
          return null;
      }
      } catch (ParseException e) {
          throw new RuntimeException("getOrderDateTime: Could not parse Order Date string: " + orderDate);
      }
    }

  public String getOrderOrigin()
  {
    return orderOrigin;
  }

  public void setOrderOrigin(String newOrderOrigin)
  {
    if(valueChanged(orderOrigin, newOrderOrigin))
    {
        setChanged(true);
    }  
    orderOrigin = newOrderOrigin;
  }

  public long getMembershipId()
  {
    return membershipId;
  }

  public void setMembershipId(long newMembershipId)
  {
    if(valueChanged(membershipId, newMembershipId))
    {
        setChanged(true);
    }      
    membershipId = newMembershipId;
  }

  public String getCsrId()
  {
    return csrId;
  }

  public void setCsrId(String newCsrId)
  {
    if(valueChanged(csrId, newCsrId))
    {
        setChanged(true);
    }  
    csrId = newCsrId;
  }

  public String getAddOnAmountTotal()
  {
    return addOnAmountTotal;
  }

  public void setAddOnAmountTotal(String newAddOnAmountTotal)
  {
    if(valueChanged(addOnAmountTotal, newAddOnAmountTotal))
    {
        setChanged(true);
    }  
    addOnAmountTotal = checkNullDouble(newAddOnAmountTotal);
  }

  public String getPartnershipBonusPoints()
  {
    return partnershipBonusPoints;
  }

  public void setPartnershipBonusPoints(String newPartnershipBonusPoints)
  {
    if(valueChanged(partnershipBonusPoints, newPartnershipBonusPoints))
    {
        setChanged(true);
    }  
    partnershipBonusPoints = checkNullInt(newPartnershipBonusPoints);
  }

  public String getSourceCode()
  {
    return sourceCode;
  }

  public void setSourceCode(String newSourceCode)
  {
    if(valueChanged(sourceCode, newSourceCode))
    {
        setChanged(true);
    }  
    sourceCode = newSourceCode;
  }

  public String getCallTime()
  {
    return callTime;
  }

  public void setCallTime(String newCallTime)
  {
    if(valueChanged(callTime, newCallTime))
    {
        setChanged(true);
    }  
    callTime = checkNullInt(newCallTime);
  }

  public String getDnisCode()
  {
    return dnisCode;
  }

  public void setDnisCode(String newDnisCode)
  {
    if(valueChanged(dnisCode, newDnisCode))
    {
        setChanged(true);
    }  
    dnisCode = newDnisCode;
  }

  public String getDatamartUpdateDate()
  {
    return datamartUpdateDate;
  }

  public void setDatamartUpdateDate(String newDatamartUpdateDate)
  {
    if(valueChanged(datamartUpdateDate, newDatamartUpdateDate))
    {
        setChanged(true);
    }  
    datamartUpdateDate = newDatamartUpdateDate;
  }

  public String getDiscountTotal()
  {
    return discountTotal;
  }

  public void setDiscountTotal(String newDiscountTotal)
  {
    if(valueChanged(discountTotal, newDiscountTotal))
    {
        setChanged(true);
    }  
    discountTotal = checkNullDouble(newDiscountTotal);
  }

  public String getYellowPagesCode()
  {
    return yellowPagesCode;
  }

  public void setYellowPagesCode(String newYellowPagesCode)
  {
    if(valueChanged(yellowPagesCode, newYellowPagesCode))
    {
        setChanged(true);
    }  
    yellowPagesCode = newYellowPagesCode;
  }

  public String getAribaBuyerAsnNumber()
  {
    return aribaBuyerAsnNumber;
  }

  public void setAribaBuyerAsnNumber(String newAribaBuyerAsnNumber)
  {
    if(valueChanged(aribaBuyerAsnNumber, newAribaBuyerAsnNumber))
    {
        setChanged(true);
    }  
    aribaBuyerAsnNumber = newAribaBuyerAsnNumber;
  }

  public String getAribaBuyerCookie()
  {
    return aribaBuyerCookie;
  }

  public void setAribaBuyerCookie(String newAribaBuyerCookie)
  {
    if(valueChanged(aribaBuyerCookie, newAribaBuyerCookie))
    {
        setChanged(true);
    }  
    aribaBuyerCookie = newAribaBuyerCookie;
  }
  
 public List<OrderExtensionsVO> getOrderExtensions()
   {
    return orderExtensions;
   }

 public void setOrderExtensions(List<OrderExtensionsVO> orderExtensions)
  {
    this.orderExtensions = orderExtensions;
  }

 public List getOrderDetail()
  {
    return orderDetail;
  }

  public void setOrderDetail(List newOrderDetail)
  {
    orderDetail = newOrderDetail;
  }

  public List getCoBrand()
  {
    return coBrand;
  }

  public void setCoBrand(List newCoBrand)
  {
    coBrand = newCoBrand;
  }
 
  public List getPayments()
  {
    return payments;
  }

  public void setPayments(List newPayments)
  {
    payments = newPayments;
  }

  public List getBuyer()
  {
    return buyer;
  }

  public void setBuyer(List newBuyer)
  {
    buyer = newBuyer;
  }

  public List getMemberships()
  {
    return memberships;
  }

  public void setMemberships(List newMemberships)
  {
    memberships = newMemberships;
  }

 public String toString()
    {
        String lineSep = System.getProperty("line.separator");
        StringBuffer sb = new StringBuffer();
        StringBuffer vectorsSb = new StringBuffer();
        // Get this Class
        Class thisClass = this.getClass();
        sb.append("Object Type = ").append(thisClass.getName()).append(lineSep);
        
        // Use reflection to loop through all the fields
        Field[] fields = thisClass.getDeclaredFields();
        for(int i = 0; i < fields.length; i++)
        {
            try
            {
                if(fields[i].getType().equals(Class.forName("java.lang.String")))
                {
                    sb.append(fields[i].getName()).append(" = ").append(fields[i].get(this)).append(lineSep);
                }
                else if(fields[i].getType().equals(Class.forName("java.util.List")))
                {
                    List vect = (List)fields[i].get(this);
                    if(vect != null)
                    {
                        for (int j = 0; j < vect.size(); j++) 
                        {
                            vectorsSb.append(vect.get(j).toString());
                        }
                    }
                }
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        }

        sb.append(vectorsSb);
        
        return sb.toString();
    }

  public String getAribaPayload()
  {
    return aribaPayload;
  }

  public void setAribaPayload(String newAribaPayload)
  {
    if(valueChanged(aribaPayload, newAribaPayload))
    {
        setChanged(true);
    }  
    aribaPayload = newAribaPayload;
  }

  public String getSocketTimestamp()
  {
    return socketTimestamp;
  }

  public void setSocketTimestamp(String newSocketTimestamp)
  {
    if(valueChanged(aribaPayload, newSocketTimestamp))
    {
        setChanged(true);
    }    
    socketTimestamp = newSocketTimestamp;
  }

  public String getStatus()
  {
    return status;
  }

  public void setStatus(String newStatus)
  {
    if(valueChanged(status, newStatus))
    {
        setChanged(true);
    }  
    status = newStatus;
  }

  public List getOrderContactInfo()
  {
    return orderContactInfo;
  }

  public void setOrderContactInfo(List newOrderContactInfo)
  {  
    orderContactInfo = newOrderContactInfo;
  }

  public String getSourceDescription()
  {
    return sourceDescription;
  }

  public void setSourceDescription(String newSourceDescription)
  {
    if(valueChanged(sourceDescription, newSourceDescription))
    {
        setChanged(true);
    }  
    sourceDescription = newSourceDescription;
  }

  public String getPartnerId()
  {
    return partnerId;
  }

  public void setPartnerId(String newPartnerId)
  {
    if(valueChanged(partnerId, newPartnerId))
    {
        setChanged(true);
    }  
    partnerId = newPartnerId;
  }

  public String getCompanyId()
  {
    return companyId;
  }

  public void setCompanyId(String newCompanyId)
  {
    if(valueChanged(companyId, newCompanyId))
    {
        setChanged(true);
    }  
    companyId = newCompanyId;
  }

  public String getCompanyName()
  {
    return companyName;
  }

  public void setCompanyName(String newCompanyName)
  {
    if(valueChanged(companyName, newCompanyName))
    {
        setChanged(true);
    }  
    companyName = newCompanyName;
  }

  public String getCompanyUrl()
  {
    return companyUrl;
  }

  public void setCompanyUrl(String newCompanyUrl)
  {
    if(valueChanged(companyUrl, newCompanyUrl))
    {
        setChanged(true);
    }  
    companyUrl = newCompanyUrl;
  }

  public List getFraudComments()
  {
    return FraudComments;
  }

  public void setFraudComments(List newFraudComments)
  {
    FraudComments = newFraudComments;
  }

  public String getFraudFlag()
  {
    return fraudFlag;
  }

  public void setFraudFlag(String newFraudFlag)
  {
    if(valueChanged(fraudFlag, newFraudFlag))
    {
        setChanged(true);
    }  
    fraudFlag = newFraudFlag;
  }

  public String getLastUpdateTimestamp()
  {
    return lastUpdateTimestamp;
  }

  public void setLastUpdateTimestamp(String newLastUpdateTimestamp)
  {
    if(valueChanged(lastUpdateTimestamp, newLastUpdateTimestamp))
    {
        setChanged(true);
    }  
    lastUpdateTimestamp = newLastUpdateTimestamp;
  }

  public String getCoBrandCreditCardCode()
  {
    return coBrandCreditCardCode;
  }

  public void setCoBrandCreditCardCode(String newCoBrandCreditCardCode)
  {
    if(valueChanged(coBrandCreditCardCode, newCoBrandCreditCardCode))
    {
        setChanged(true);
    }  
    coBrandCreditCardCode = newCoBrandCreditCardCode;
  }

 /** This method sets the change flag in each of
  *  the vos within the order object.
  *  
  *  @param boolean value to set the change flags too
  */
 public void setChangeFlags(boolean flag)
 {
    //set order object
    this.setChanged(flag);

    //co brands
    List coBrandList = this.getCoBrand();
    if(coBrandList != null)
    {
      Iterator coBrandIter = coBrandList.iterator();
      while(coBrandIter.hasNext())
      {
        CoBrandVO coBrand = (CoBrandVO)coBrandIter.next();
        coBrand.setChanged(flag);
      }
    }//end co brand list not null

    //payments
    List paymentList = this.getPayments();
    if(paymentList != null)
    {
      Iterator paymentIter = paymentList.iterator();
      while(paymentIter.hasNext())
      {
        PaymentsVO payVO = (PaymentsVO)paymentIter.next();
        payVO.setChanged(flag);

        //credit cards
        List creditCardList = payVO.getCreditCards();
        if(creditCardList != null)
        {
          Iterator creditIter = creditCardList.iterator();
          while(creditIter.hasNext())
          {
            CreditCardsVO card = (CreditCardsVO)creditIter.next();
            card.setChanged(flag);
          }//end credit iterator
        }//credit card list not null
        
      }//end while payment iter
    }//end payment list not null

    //buyers
    List buyerList = this.getBuyer();
    if(buyerList != null)
    {
      Iterator buyerIter = buyerList.iterator();

      //for each buyer
      while(buyerIter.hasNext()){
            BuyerVO buyer = (BuyerVO)buyerIter.next();
            buyer.setChanged(flag);

            //buyer addresses
            List buyerAddressList = buyer.getBuyerAddresses();
            if(buyerAddressList != null)
            {
              Iterator buyerAddrIter = buyerAddressList.iterator();
              while(buyerAddrIter.hasNext())
              {
                BuyerAddressesVO buyerAddr = (BuyerAddressesVO)buyerAddrIter.next();
                buyerAddr.setChanged(flag);
              }//end while more buyer addresses        
            }//end buyer address list not null

            //buyer comments
            List buyerCommentsList = buyer.getBuyerComments();
            if(buyerCommentsList != null)
            {
              Iterator buyerCommentIter = buyerCommentsList.iterator();
              while(buyerCommentIter.hasNext())
              {
                BuyerCommentsVO buyerComments = (BuyerCommentsVO)buyerCommentIter.next();
                buyerComments.setChanged(flag);
              }//end while more buyer comments        
            }//end buyer comment list not null

            //buyer emails
            List buyerEmailList = buyer.getBuyerEmails();
            if(buyerEmailList != null)
            {
              Iterator buyerEmailIter = buyerEmailList.iterator();
              while(buyerEmailIter.hasNext())
              {
                BuyerEmailsVO buyerEmails = (BuyerEmailsVO)buyerEmailIter.next();
                buyerEmails.setChanged(flag);
              }//end while more buyer emails
            }//end buyer email list

            //buyer phones
            List buyerPhoneList = buyer.getBuyerPhones();
            if(buyerPhoneList != null)
            {
              Iterator buyerPhoneIter = buyerPhoneList.iterator();
              while(buyerPhoneIter.hasNext())
              {
                BuyerPhonesVO buyerPhones = (BuyerPhonesVO)buyerPhoneIter.next();
                buyerPhones.setChanged(flag);
              }//end while more buyer phones
            }//end buyer phones list

            //buyer ncoa  -- not being used right now
            /*List buyerNCOAList = buyer.getNcoa();
            if(buyerNCOAList != null)
            {
              Iterator buyerNCOAIter = buyerNCOAList.iterator();
              while(buyerNCOAIter.hasNext())
              {
                NcoaVO ncoa = (NcoaVO)buyerNCOAIter.next();
                ncoa.setChanged(flag);
              }//end while more buyer ncoa
            }//end buyer ncoa list
            */
            
      }//while more buyers      
    }//end buyer list null

    //fraud comments
    List fraudCommentsList = this.getFraudComments();
    if(fraudCommentsList != null)
    {
      Iterator fraudIter = fraudCommentsList.iterator();
      while(fraudIter.hasNext())
      {
        FraudCommentsVO fraud = (FraudCommentsVO)fraudIter.next();
        fraud.setChanged(flag);
      }//while fraud comments iter
    }//fraud comments list  not null

    //memberships
    List membershipList = this.getMemberships();
    if(membershipList != null)
    {
      Iterator memberIter = membershipList.iterator();
      while(memberIter.hasNext())
      {
        MembershipsVO mem = (MembershipsVO)memberIter.next();
        mem.setChanged(flag);
      }//while memberships iter
    }//memberships list  not null

    //order contact
    List contactList = this.getOrderContactInfo();
    if(contactList != null)
    {
      Iterator contactIter = contactList.iterator();
      while(contactIter.hasNext())
      {
        OrderContactInfoVO contact = (OrderContactInfoVO)contactIter.next();
        contact.setChanged(flag);
      }//while order contact iter
    }//order contact list  not null


    //for each detail in the order
    List details = this.getOrderDetail();
    if(details != null){
      Iterator detailIter = details.iterator();
      while(detailIter.hasNext())
      {
        //order detail
        OrderDetailsVO orderDetail = (OrderDetailsVO)detailIter.next();
        orderDetail.setChanged(flag);

        //dispostions
        List dispList = orderDetail.getDispositions();
        if(dispList != null)
        {
          Iterator dispIter = dispList.iterator();
          while(dispIter.hasNext())
          {
            DispositionsVO disp = (DispositionsVO)dispIter.next();
            disp.setChanged(flag);
          }//whiel disp iter
        }//disp list not null

        //addons
        List addonList = orderDetail.getAddOns();
        if(addonList != null)
        {
          Iterator addonIter = addonList.iterator();
          while(addonIter.hasNext()){
            AddOnsVO addon = (AddOnsVO)addonIter.next();
            addon.setChanged(flag);
          }
        }//end addon list not null

        //Recipients
        List recipList = orderDetail.getRecipients();
        if(recipList != null)
        {
          Iterator recipIter = recipList.iterator();
          while(recipIter.hasNext())
          {
            RecipientsVO recip = (RecipientsVO)recipIter.next();
            recip.setChanged(flag);

            //destinations
            List destList = recip.getDestinations();
            if(destList != null)
            {
              Iterator destIter = destList.iterator();
              while(destIter.hasNext()){
                DestinationsVO destVO = (DestinationsVO)destIter.next();
                destVO.setChanged(flag);
              }
            }//if dest list null

            //recip comments
            List recipCommentsList = recip.getRecipientComments();
            if(recipCommentsList != null)
            {
              Iterator recipCommentsIter = recipCommentsList.iterator();
              while(recipCommentsIter.hasNext()){
                RecipientCommentsVO rcomments = (RecipientCommentsVO)recipCommentsIter.next();
                rcomments.setChanged(flag);
              }//recip comments while
            }//if recip comments list null

            //recip phones
            List recipPhoneList = recip.getRecipientPhones();
            if(recipPhoneList != null)
            {
              Iterator recipPhoneIter = recipPhoneList.iterator();
              while(recipPhoneIter.hasNext()){
                RecipientPhonesVO rphones = (RecipientPhonesVO)recipPhoneIter.next();
                rphones.setChanged(flag);
              }//recip phones while
            }//if recip phones list null


            //recip address
            List recipAddrList = recip.getRecipientAddresses();
            if(recipAddrList != null)
            {
              Iterator recipAddrIter = recipAddrList.iterator();
              while(recipAddrIter.hasNext()){
                RecipientAddressesVO raddr = (RecipientAddressesVO)recipAddrIter.next();
                raddr.setChanged(flag);

               
              }//while recip addr iter
            }//if recip address list null
            
          }//while recip iterator
        }//recip not null


        
      }//while detail iterator
    }//end if details not null
 }

  public List getFraudCodes()
  {
    return fraudCodes;
  }

  public void setFraudCodes(List newFraudCodes)
  {
    fraudCodes = newFraudCodes;
  }

  //add a fraud code
  public void addFraudCode(String code)
  {
    if(fraudCodes == null)
    {
      fraudCodes = new ArrayList();
    }

    fraudCodes.add(code);
    
  }

    public String getPreviousStatus()
    {
        return previousStatus;
    }

    public void setPreviousStatus(String newPreviousStatus)
    {
        previousStatus = newPreviousStatus;
    }
    
  public String getLossPreventionIndicator()
  {
    return lossPreventionIndicator;
  }
  
  public void setLossPreventionIndicator(String newLossPreventionIndicator)
  {
    lossPreventionIndicator = newLossPreventionIndicator;
  }

  public void setOeOrder(boolean newOeOrder)
  {
    if(oeOrder != newOeOrder)
    {
        setChanged(true);
    }  
    oeOrder = newOeOrder;
  }

  public boolean isOeOrder()
  {
    return oeOrder;
  }

    public void setPreferredProcessingPartners(HashSet<String> preferredProcessingPartners) {
        this.preferredProcessingPartners = preferredProcessingPartners;
    }

    public HashSet<String> getPreferredProcessingPartners() {
        return preferredProcessingPartners;
    }
    
    public boolean hasPreferredProcessingPartner(String partner) {
        boolean retval = false;
        if (preferredProcessingPartners != null) {
            retval = preferredProcessingPartners.contains(partner);
        }
        return retval;
    }

    public void setPreferredProcessingPartnerNames(HashSet<String> preferredProcessingPartnerNames) {
        this.preferredProcessingPartnerNames = preferredProcessingPartnerNames;
    }

    public HashSet<String> getPreferredProcessingPartnerNames() {
        return preferredProcessingPartnerNames;
    }

  public void setFuelSurchargeDescription(String fuelSurchargeDescription)
  {
    this.fuelSurchargeDescription = fuelSurchargeDescription;
  }

  public String getFuelSurchargeDescription()
  {
    return fuelSurchargeDescription;
  }

  public void setApplySurchargeCode(String applySurchargeCode)
  {
    this.applySurchargeCode = applySurchargeCode;
  }

  public String getApplySurchargeCode()
  {
    return applySurchargeCode;
  }
        
    public void setBuyerEmailAddress(String buyerEmailAddress)
    {
      this.buyerEmailAddress = buyerEmailAddress;
    }
  
    public String getBuyerEmailAddress()
    {
      return buyerEmailAddress;
    }
  
    public void setServiceFeeTotalSavings(String serviceFeeTotalSavings)
    {
      this.serviceFeeTotalSavings = serviceFeeTotalSavings;
    }
  
    public String getServiceFeeTotalSavings()
    {
      return serviceFeeTotalSavings;
    }
  
    public void setShippingFeeTotalSavings(String shippingFeeTotalSavings)
    {
      this.shippingFeeTotalSavings = shippingFeeTotalSavings;
    }
  
    public String getShippingFeeTotalSavings()
    {
      return shippingFeeTotalSavings;
    }
    public void setBuyerSignedIn(String buyerSignedIn)
    {
      this.buyerSignedIn = buyerSignedIn;
    }

    public String getBuyerSignedIn()
    {
      return buyerSignedIn;
    }
    
    /**
   * 
   * @return <code>true</code> if {@link #buyerSignedIn} == Y, else <code>false</code>
   */
    public boolean isBuyerSignedIn()
    {
      return "Y".equalsIgnoreCase(buyerSignedIn);
    }
    
    /**
   * Overloaded method to set the flag as a boolean.
   * true sets the value to 'Y'
   * false sets the value to 'N'
   * @param buyerSignedInVal
   */
    public void setBuyerSignedIn(boolean buyerSignedInVal)
    {
      buyerSignedIn = buyerSignedInVal ? "Y" : "N";
    }
    
   /**
   * Calculates and returns the sum of points of all the details
   * Fractional Points are rounded down
   * @return
   */
    public BigDecimal getTotalPoints()
    {
        int ROUND_MODE = BigDecimal.ROUND_HALF_UP;
        List details = this.getOrderDetail();
        BigDecimal totalPoints = new BigDecimal("0");

        for (int i = 0; i < details.size(); i++) 
        { 
            OrderDetailsVO detail = (OrderDetailsVO)details.get(i);      
            String points = detail.getMilesPoints();
            
            if(points != null){
                BigDecimal itemPoints = new BigDecimal(points);
                itemPoints = itemPoints.setScale(0, ROUND_MODE);
                totalPoints = totalPoints.add(itemPoints);
            }
        }

        return totalPoints;
    }
    
   /**
   * Calculates and returns the sum of the Product Amounts of all the details
   * @return
   */    
    public BigDecimal getTotalProductAmount()
    {
      List details = this.getOrderDetail();
      String totalPrice = "0";
      for (int i = 0; i < details.size(); i++) 
      { 
          OrderDetailsVO detail = (OrderDetailsVO)details.get(i);      
          String amount = detail.getProductsAmount();
          if(amount != null){
              int pos = amount.indexOf("."); 
              if(pos >0)
              {
                BigDecimal totalBig = new BigDecimal(totalPrice).add(new BigDecimal(amount));
                totalPrice = (totalBig.setScale(2,BigDecimal.ROUND_DOWN)).toString();
              }
              else
              {
                int totalInt = (new Integer(totalPrice).intValue()) + (new Integer(amount).intValue());
                totalPrice = new Integer(totalInt).toString();
              }
          }
      }
      
      return new BigDecimal(totalPrice);      
      
      
    }

  public void setFuelSurchargeFee(String fuelSurchargeFee)
  {
    this.fuelSurchargeFee = fuelSurchargeFee;
  }

  public String getFuelSurchargeFee()
  {
    return fuelSurchargeFee;
  }
    public void setFreeShippingUseFlag(String freeShippingUseFlag) {
        this.freeShippingUseFlag = freeShippingUseFlag;
    }

    public String getFreeShippingUseFlag() {
        return freeShippingUseFlag;
    }

    public void setFreeShippingPurchaseFlag(String freeShippingPurchaseFlag) {
        this.freeShippingPurchaseFlag = freeShippingPurchaseFlag;
    }

    public String getFreeShippingPurchaseFlag() {
        return freeShippingPurchaseFlag;
    }

    public void setBuyerHasFreeShippingFlag(String buyerHasFreeShippingFlag) {
        this.buyerHasFreeShippingFlag = buyerHasFreeShippingFlag;
    }

    public String getBuyerHasFreeShippingFlag() {
        return buyerHasFreeShippingFlag;
    }
        
    public void setCharacterMappingFlag(String characterMappingFlag) {
        this.characterMappingFlag = characterMappingFlag;
    }

    public String getCharacterMappingFlag() {
        return characterMappingFlag;
    }

	public String getLanguageId() {
		return languageId;
	}

	public void setLanguageId(String languageId) {
		this.languageId = languageId;
	}
    
	public String getFreeShippingMembershipInCartFlag() {
		return freeShippingMembershipInCartFlag;
	}

	public void setFreeShippingMembershipInCartFlag(
			String freeShippingMembershipInCartFlag) {
		this.freeShippingMembershipInCartFlag = freeShippingMembershipInCartFlag;
	}

	public boolean isBuyerEmailChangedInScrub() {
		return isBuyerEmailChangedInScrub;
	}

	public void setBuyerEmailChangedInScrub(boolean isBuyerEmailChangedInScrub) {
		this.isBuyerEmailChangedInScrub = isBuyerEmailChangedInScrub;
	}

	public boolean isAllowPCCheckUpdateOrder() {
		return allowPCCheckUpdateOrder;
	}

	public void setAllowPCCheckUpdateOrder(boolean allowPCCheckUpdateOrder) {
		this.allowPCCheckUpdateOrder = allowPCCheckUpdateOrder;
	}



	public void setBuyerHasPCMembershipFlag(String buyerHasPCMembershipFlag) {
		this.buyerHasPCMembershipFlag = buyerHasPCMembershipFlag;
	}

	public String getBuyerHasPCMembershipFlag() {
		return buyerHasPCMembershipFlag;
	}

	public String getMrcntChannelIcon() {
		return mrcntChannelIcon;
	}

	public void setMrcntChannelIcon(String mrcntChannelIcon) {
		this.mrcntChannelIcon = mrcntChannelIcon;
	}

	public String getIsMercentOrder() {
		return isMercentOrder;
	}

	public void setIsMercentOrder(String isMercentOrder) {
		this.isMercentOrder = isMercentOrder;
	}

	public String getMrcntChannelName() {
		return mrcntChannelName;
	}

	public void setMrcntChannelName(String mrcntChannelName) {
		this.mrcntChannelName = mrcntChannelName;
	}

	public void setPartnerOrder(boolean isPartnerOrder) {
		this.isPartnerOrder = isPartnerOrder;
	}

	public boolean isPartnerOrder() {
		return isPartnerOrder;
	}

	public void setPartnerImgURL(String partnerImgURL) {
		this.partnerImgURL = partnerImgURL;
	}

	public String getPartnerImgURL() {
		return partnerImgURL;
	}

	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}

	public String getPartnerName() {
		return partnerName;
	}

	public void setSourceType(String sourceType) {
		this.sourceType = sourceType;
	}

	public String getSourceType() {
		return sourceType;
	}
	

	public String getJointCartIndicator() {
		return jointCartIndicator;
	}

	public void setJointCartIndicator(String jointCartIndicator) {
		this.jointCartIndicator = jointCartIndicator;
	}

	/**
	 * @return the addOnDiscountAmount
	 */
	public String getAddOnDiscountAmount() {
		return addOnDiscountAmount;
	}

	/**
	 * @param addOnDiscountAmount the addOnDiscountAmount to set
	 */
	public void setAddOnDiscountAmount(String addOnDiscountAmount) {
		this.addOnDiscountAmount = addOnDiscountAmount;
	}

	public boolean isInvokedAsynchronously() {
		return invokedAsynchronously;
	}

	public void setInvokedAsynchronously(boolean invokedAsynchronously) {
		this.invokedAsynchronously = invokedAsynchronously;
	}

	public String getOeBuyerHasFreeShipping() {
		return oeBuyerHasFreeShipping;
	}

	public void setOeBuyerHasFreeShipping(String oeBuyerHasFreeShipping) {
		this.oeBuyerHasFreeShipping = oeBuyerHasFreeShipping;
	}

	public boolean isResetCardinal() {
		return isResetCardinal;
	}

	public void setResetCardinal(boolean isResetCardinal) {
		this.isResetCardinal = isResetCardinal;
	}

    public String getOrderCalcType() {
        return orderCalcType;
    }

    public void setOrderCalcType(String orderCalcType) {
        this.orderCalcType = orderCalcType;
    }
	
	

}
