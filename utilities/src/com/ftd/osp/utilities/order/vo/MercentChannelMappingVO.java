package com.ftd.osp.utilities.order.vo;

public class MercentChannelMappingVO {
	private String channelName;
	private String ftdOriginMapping;
	private String masterOrderNoPrefix;
	private String channelImage;
	private String confNumberPrefix;
	private String defaultSrcCode;
	private String defaultRecalcSrcCode;
	private String sendConfirmationEmail;
	
	public String getConfNumberPrefix() {
		return confNumberPrefix;
	}
	public void setConfNumberPrefix(String confNumberPrefix) {
		this.confNumberPrefix = confNumberPrefix;
	}
	public String getChannelName() {
		return channelName;
	}
	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}
	public String getFtdOriginMapping() {
		return ftdOriginMapping;
	}
	public void setFtdOriginMapping(String ftdOriginMapping) {
		this.ftdOriginMapping = ftdOriginMapping;
	}
	public String getMasterOrderNoPrefix() {
		return masterOrderNoPrefix;
	}
	public void setMasterOrderNoPrefix(String masterOrderNoPrefix) {
		this.masterOrderNoPrefix = masterOrderNoPrefix;
	}
	public String getChannelImage() {
		return channelImage;
	}
	public void setChannelImage(String channelImage) {
		this.channelImage = channelImage;
	}
	public String getDefaultSrcCode() {
		return defaultSrcCode;
	}
	public void setDefaultSrcCode(String defaultSrcCode) {
		this.defaultSrcCode = defaultSrcCode;
	}
	public String getDefaultRecalcSrcCode() {
		return defaultRecalcSrcCode;
	}
	public void setDefaultRecalcSrcCode(String defaultRecalcSrcCode) {
		this.defaultRecalcSrcCode = defaultRecalcSrcCode;
	}
	public String getSendConfirmationEmail() {
		return sendConfirmationEmail;
	}
	public void setSendConfirmationEmail(String sendConfirmationEmail) {
		this.sendConfirmationEmail = sendConfirmationEmail;
	}
	
	
}
