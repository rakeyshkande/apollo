/**
 * 
 */
package com.ftd.osp.utilities.order.vo;

import com.ftd.osp.utilities.plugins.Logger;

/**
 * @author smeka
 *
 */
public enum TaxSplitOrder {
	STATETAX(1), 
	COUNTYTAX(2), 
	CITYTAX(3), 
	ZIPTAX(4), 
	STATE(1), 
	COUNTY(2), 
	CITY(3), 
	ZIP(4),
	SURCHARGE(1),
	GSTHST(1), 
	GSTHSTTAX(1), 
	PST(2), 
	QST(2),
	TAXES(0);
	
	
	int splitOrder;

	TaxSplitOrder(int splitOrder) {
		this.splitOrder = splitOrder;
	}

	public int value() {
		return splitOrder;
	}
	
	private static Logger logger = new Logger(TaxDisplayOrder.class.getName());
	
	public static TaxSplitOrder fromValue(String taxType) {
		taxType = taxType.replaceAll("[^\\w\\s-]", "");
		taxType = taxType.replaceAll("\\s", "");
		
		for (TaxSplitOrder c : TaxSplitOrder.values()) {		
			
			if(c.name().equalsIgnoreCase(taxType)) {				
				return c;
			} 			
		}
		
		logger.error("TODO:Tax Type is invalid return default split order");  
		return TAXES;
	}
	
}
