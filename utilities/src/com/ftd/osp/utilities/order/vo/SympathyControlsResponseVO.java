/**
 * 
 */
package com.ftd.osp.utilities.order.vo;

/**
 * @author gboddu
 *
 */
public class SympathyControlsResponseVO {
	
	private char status;
	private String responseMessage;
	
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public String getResponseMessage() {
		return responseMessage;
	}
	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}
	

	public SympathyControlsResponseVO(char status, String message) {
		this.status = status;
		this.responseMessage = message;
	}
}
