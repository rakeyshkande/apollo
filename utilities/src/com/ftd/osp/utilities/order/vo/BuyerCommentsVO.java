package com.ftd.osp.utilities.order.vo;

import java.util.List;
import java.lang.reflect.*;
import java.io.Serializable;

/**
 * Buyer Comments Value Object that maps to BUYER_COMMENTS database table
 * This is a sub collection within Buyer value object
 *
 * @author Doug Johnson
 */
public class BuyerCommentsVO extends BaseVO implements Serializable  
{
  private long buyerCommentId;
  private long buyerId;
  private String text;

  public BuyerCommentsVO()
  {}
 
  public long getBuyerCommentId()
  {
    return buyerCommentId;
  }

  public void setBuyerCommentId(long newBuyerCommentId)
  {
    if(valueChanged(buyerCommentId, newBuyerCommentId))
    {
        setChanged(true);
    }  
    buyerCommentId = newBuyerCommentId;
  }

  public long getBuyerId()
  {
    return buyerId;
  }

  public void setBuyerId(long newBuyerId)
  {
    if(valueChanged(buyerId, newBuyerId))
    {
        setChanged(true);
    }  
    buyerId = newBuyerId;
  }

  public String getText()
  {
    return text;
  }

  public void setText(String newText)
  {
    if(valueChanged(text, newText))
    {
        setChanged(true);
    }  
    text = newText;
  }

   public String toString()
    {
        String lineSep = System.getProperty("line.separator");
        StringBuffer sb = new StringBuffer();
        StringBuffer vectorsSb = new StringBuffer();
        // Get this Class
        Class thisClass = this.getClass();
        sb.append("Object Type = ").append(thisClass.getName()).append(lineSep);
        
        // Use reflection to loop through all the fields
        Field[] fields = thisClass.getDeclaredFields();
        for(int i = 0; i < fields.length; i++)
        {
            try
            {
                if(fields[i].getType().equals(Class.forName("java.lang.String")))
                {
                    sb.append(fields[i].getName()).append(" = ").append(fields[i].get(this)).append(lineSep);
                }
                else if(fields[i].getType().equals(Class.forName("java.util.List")))
                {
                    List vect = (List)fields[i].get(this);
                    if(vect != null)
                    {
                        for (int j = 0; j < vect.size(); j++) 
                        {
                            vectorsSb.append(vect.get(j).toString());
                        }
                    }
                }
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        }

        sb.append(vectorsSb);
        
        return sb.toString();
    }
}

