package com.ftd.osp.utilities.order.vo;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Order Details Value Object that maps to ORDER_DETAILS database table
 * This is a sub collection within Order value object.
 *
 * @author Doug Johnson
 */
public class OrderDetailsVO  extends BaseVO implements Serializable  
{
  private long orderDetailId;
  private String sourceCode;
  private String sourceDescription;
  private String itemOfTheWeekFlag;
  private String guid;
  private String lineNumber;
  private long recipientId;
  private long recipientAddressId;
  private String productId;
  private String quantity;
  private String shipVia;
  private String shipMethod;
  private String shipDate;
  private String deliveryDate;
  private String deliveryDateRangeEnd;
  private String dropShipTrackingNumber;
  private String productsAmount;
  
  private String serviceFeeAmount;
  private String shippingFeeAmount;
  private String addOnAmount;
  private String discountAmount;
  private String occassionId;
  private String lastMinuteGiftSignature;
  private String lastMinuteNumber;
  private String lastMinuteGiftEmail;
  private String externalOrderNumber;
  private String colorFirstChoice;
  private String colorSecondChoice;
  private String sizeChoice;
  private String cardMessage;
  private String cardSignature;
  private String orderComments;
  private String orderContactInformation;
  private String floristNumber;
  private String specialInstructions;
  private String aribaUnspscCode;
  private String aribaPoNumber;
  private String aribaAmsProjectCode;
  private String aribaCostCenter;
  private String substituteAcknowledgement;
  private String senderInfoRelease;
  private String personalizationData;
  private List addOns;
  private List recipients;
  private AVSAddressVO avsAddress;
  private String externalOrderTotal;
  private String status;
  private String itemDescription;
  private String floristName;
  private String milesPoints;
  private String shipMethodCarrierFlag;
  private String shipMethodFloristFlag;
  private List Dispositions;
  private String DiscountedProductPrice;
  private String RewardType;
  private String DiscountType;
  private String colorOneDescription;
  private String colorTwoDescription;
  private String productSubCodeId;
  private String productSubcodeDescription;
  private String percentOff;
  private String productType;
  private String occasionDescription;
  private String statusSortOrder;
  private String hpSequence;
  private String reinstateFlag;
  private List orderDetailExtensions;
  private String commission;
  private String wholesale;
  private String transaction;
  private String pdbPrice;
 
  private String priceOverrideFlag;
  private String gccAmt;
  
  
  private Date updatedOnProductsAmountTime;
  private SimpleDateFormat updatedOnFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");
  private String orderAmount;
  private String milesPointsAmt;
  private String milesPointsAmtProduct;
  private String milesPointsAmtAddon;
  private String personalGreetingId;
  private String noTaxFlag;
  private String premierCollectionFlag;
  private String origSourceCode;
  private String origProductId;
  private String origShipMethod;
  private String origDeliveryDate;
  private String origTotalFeeAmount;
  private String preferredProcessingPartner;
  private String sameDayFreshcutFlag;
  private String sameDayGiftFlag;
  private String binSourceChangedFlag;
  private String domesticFloristServiceCharge;
  private String internationalFloristServiceCharge;
  private String shippingCost;
  private String vendorCharge;
  private String saturdayUpcharge; 
  private String sundayUpcharge;
  private String mondayUpcharge;
  private String latecutoffCharge;
  private String alaskaHawaiiSurcharge;
  private String fuelSurcharge;
  private String fuelSurchargeDescription;
  private String applySurchargeCode;
  private String displaySurcharge;
  private String sendSurchargeToFlorist;
  private String serviceFeeAmountSavings;
  private String shippingFeeAmountSavings;
  private String autoRenew;
  private String freeShipping;
  private String productSubType;
  //private List taxVOs;
  private String sameDayUpcharge;
  private String originalOrderHasSDU;
  private String morningDeliveryFee;
  private String amazonStatus;
  private String mercentStatus;
  //#12961 - Premier Circle
  private String pcGroupId;
  private String pcMembershipId;
  private String pcFlag;
  
  //#11946 - BBN
  private String morningDeliveryOpted;
  private String originalOrderHasMDF;
  private String morningDeliveryAvailable;
  //#13152 - BBN - charge Free shipping members for morning delivery fee
  private String bbnChargedtoFSMembers;

  //#12798 - BBN
  private String productMorningDeliveryFlag;
  
  private String origExternalOrderTotal;

  private String originalServiceFeeAmount;
  private String originalShippingFeeAmount;

  private static  final String DELIVERY_DATE_FORMAT="MM/dd/yyyy";

  //field added as part of walmart project.  Need for accounting. emueller 1/19/05
   private String partnerCost;

   // Added for JOE.  mkruger 9/13/07
   
   private String sduChargedtoFSMembers;
   private String originalOrderHasSDUFS;
   
    
  /**
   * This flag is set when recalculate processes the item and applies Free shipping to the item
   */
  private boolean recalculateAppliedFreeShipping = false;
  
  private String novatorId;
  
  //#199 tax fields
  private ItemTaxVO itemTaxVO; 
  private boolean taxServicePerformed;
  private String timeOfService;
  
  private String addOnDiscountAmount;
  private String originalOrderHasLCF; //Original order has Late-cutoff-fee
  private String ftdwAllowedShipping;
  private String shippingSystem;
  private boolean applyOrigFee;
  
  private String legacyId;
  
  public OrderDetailsVO()
  {}

  /*
   * 03/19/04
   * Item of the week accessors 
   */
  public void setSourceCode(String newSourceCode)
  {
    if(valueChanged(sourceCode, newSourceCode))
    {
        setChanged(true);
    }  
    sourceCode = newSourceCode;
  }

  public String getSourceCode()
  {
      return sourceCode;
  }

  public String getSourceDescription()
  {
    return sourceDescription;
  }

  public void setSourceDescription(String newSourceDescription)
  {
    if(valueChanged(sourceDescription, newSourceDescription))
    {
        setChanged(true);
    }  
    sourceDescription = newSourceDescription;
  }

  public void setItemOfTheWeekFlag(String newItemOfTheWeekFlag)
  {
      if (valueChanged(itemOfTheWeekFlag,newItemOfTheWeekFlag))
      {
          setChanged(true);
      }

      itemOfTheWeekFlag = newItemOfTheWeekFlag;
  }

  public String getItemOfTheWeekFlag()
  {
      return itemOfTheWeekFlag;
  }

  public long getOrderDetailId()
  {
    return orderDetailId;
  }

  public void setOrderDetailId(long newOrderDetailId)
  {
    if(valueChanged(orderDetailId,newOrderDetailId)){
      orderDetailId = newOrderDetailId;
      setChanged(true);
    }
  }

  public String getGuid()
  {
    return guid;
  }

  public void setGuid(String newGuid)
  {
    if(valueChanged(guid,newGuid)){
      guid = newGuid;
      setChanged(true);
    }

  }

  public String getLineNumber()
  {
    return lineNumber;
  }

  public void setLineNumber(String newLineNumber)
  {
    if(valueChanged(checkNullInt(newLineNumber),newLineNumber)){
      setChanged(true);
    }
    lineNumber = checkNullInt(newLineNumber);
  }

  public long getRecipientId()
  {
    return recipientId;
  }

  public void setRecipientId(long newRecipientId)
  {
    if(valueChanged(recipientId,newRecipientId)){
      setChanged(true);
    }
    recipientId = newRecipientId;
  }

  public long getRecipientAddressId()
  {

    return recipientAddressId;
  }

  public void setRecipientAddressId(long newRecipientAddressId)
  {
    if(valueChanged(recipientAddressId,newRecipientAddressId)){
      setChanged(true);
    }
    recipientAddressId = newRecipientAddressId;
  }

  public String getProductId()
  {
    return productId;
  }

  public void setProductId(String newProductId)
  {
    if(valueChanged(productId,newProductId)){
      setChanged(true);
    }
    productId = newProductId;
  }

  public String getQuantity()
  {
    return quantity;
  }

  public void setQuantity(String newQuantity)
  {
    if(valueChanged(checkNullInt(newQuantity),newQuantity)){
      setChanged(true);
    }
    quantity = checkNullInt(newQuantity);
  }

  public String getShipVia()
  {
    return shipVia;
  }

  public void setShipVia(String newShipVia)
  {
    if(valueChanged(shipVia,newShipVia)){
      setChanged(true);
    }
    shipVia = newShipVia;
  }

  public String getShipMethod()
  {
    return shipMethod;
  }

  public void setShipMethod(String newShipMethod)
  {
    if(valueChanged(shipMethod,newShipMethod)){
      setChanged(true);
    }
    shipMethod = newShipMethod;
  }

  public String getShipDate()
  {
    return shipDate;
  }

  public void setShipDate(String newShipDate)
  {
    if(valueChanged(shipDate,newShipDate)){
      setChanged(true);
    }
    shipDate = newShipDate;
  }

  public String getDeliveryDate()
  {
    return deliveryDate;
  }

  public void setDeliveryDate(String newDeliveryDate)
  {
    if(valueChanged(deliveryDate,newDeliveryDate)){
      setChanged(true);
    }
    deliveryDate = newDeliveryDate;
  }
  
  public void setDeliveryDate(Date newDeliveryDate)
  {
    SimpleDateFormat fmt = new SimpleDateFormat(DELIVERY_DATE_FORMAT);
    setDeliveryDate(fmt.format(newDeliveryDate));
  }

  public String getDeliveryDateRangeEnd()
  {
    return deliveryDateRangeEnd;
  }

  public void setDeliveryDateRangeEnd(String newDeliveryDateRangeEnd)
  {
    if(valueChanged(deliveryDateRangeEnd,newDeliveryDateRangeEnd)){
      setChanged(true);
    }
    deliveryDateRangeEnd = newDeliveryDateRangeEnd;
  }

  public void setDeliveryDateRangeEnd(Date newDeliveryDateRangeEnd)
  {
    SimpleDateFormat fmt = new SimpleDateFormat(DELIVERY_DATE_FORMAT);
    setDeliveryDateRangeEnd(fmt.format(newDeliveryDateRangeEnd));
  }
  
  public String getDropShipTrackingNumber()
  {
    return dropShipTrackingNumber;
  }

  public void setDropShipTrackingNumber(String newDropShipTrackingNumber)
  {
    if(valueChanged(dropShipTrackingNumber,newDropShipTrackingNumber)){
      setChanged(true);
    }
    dropShipTrackingNumber = newDropShipTrackingNumber;
  }

  public String getProductsAmount()
  {
    return productsAmount;
  }

  public void setProductsAmount(String newProductsAmount)
  {
    if(valueChanged(productsAmount,checkNullDouble(newProductsAmount))){
      setChanged(true);
    }
    productsAmount = checkNullDouble(newProductsAmount);
  }

	public String getTaxAmount() {
		if (this.itemTaxVO != null) {
			return itemTaxVO.getTaxAmount();
		}
		return ItemTaxVO.ZERO_VALUE;
	}

	public void setTaxAmount(String newTaxAmount) {
		if (this.itemTaxVO == null) {
			itemTaxVO = new ItemTaxVO();
			setChanged(true);
		} else if (valueChanged(this.itemTaxVO.getTaxAmount(), newTaxAmount)) {
			setChanged(true);
		}
		this.itemTaxVO.setTaxAmount(newTaxAmount); 
	}

  public String getServiceFeeAmount()
  {
    return serviceFeeAmount;
  }

  public void setServiceFeeAmount(String newServiceFeeAmount)
  {
    if(valueChanged(serviceFeeAmount,checkNullDouble(newServiceFeeAmount))){
      setChanged(true);
    }  
    serviceFeeAmount = checkNullDouble(newServiceFeeAmount);
  }

  public String getShippingFeeAmount()
  {
    return shippingFeeAmount;
  }

  public void setShippingFeeAmount(String newShippingFeeAmount)
  {
    if(valueChanged(shippingFeeAmount,checkNullDouble(newShippingFeeAmount))){
      setChanged(true);
    }    
    shippingFeeAmount = checkNullDouble(newShippingFeeAmount);
  }

  public String getAddOnAmount()
  {
    return addOnAmount;
  }

  public void setAddOnAmount(String newAddOnAmount)
  {
    if(valueChanged(addOnAmount,checkNullDouble(newAddOnAmount))){
      setChanged(true);
    }    
    addOnAmount = checkNullDouble(newAddOnAmount);
  }

  public String getDiscountAmount()
  {
    return discountAmount;
  }

  public void setDiscountAmount(String newDiscountAmount)
  {
    if(valueChanged(discountAmount,checkNullDouble(newDiscountAmount))){
      setChanged(true);
    }      
    discountAmount = checkNullDouble(newDiscountAmount);
  }

  public String getOccassionId()
  {
    return occassionId;
  }

  public void setOccassionId(String newOccassionId)
  {
    if(valueChanged(occassionId,checkNullInt(newOccassionId))){
      setChanged(true);
    }        
    occassionId = checkNullInt(newOccassionId);
  }

  public String getLastMinuteGiftSignature()
  {
    return lastMinuteGiftSignature;
  }

  public void setLastMinuteGiftSignature(String newLastMinuteGiftSignature)
  {
    if(valueChanged(lastMinuteGiftSignature,newLastMinuteGiftSignature)){
      setChanged(true);
    }          
    lastMinuteGiftSignature = newLastMinuteGiftSignature;
  }

  public String getLastMinuteNumber()
  {
    return lastMinuteNumber;
  }

  public void setLastMinuteNumber(String newLastMinuteNumber)
  {
    if(valueChanged(lastMinuteNumber,newLastMinuteNumber)){
      setChanged(true);
    }            
    lastMinuteNumber = newLastMinuteNumber;
  }

  public String getLastMinuteGiftEmail()
  {
    return lastMinuteGiftEmail;
  }

  public void setLastMinuteGiftEmail(String newLastMinuteGiftEmail)
  {
    if(valueChanged(lastMinuteGiftEmail,newLastMinuteGiftEmail)){
      setChanged(true);
    }              
    lastMinuteGiftEmail = newLastMinuteGiftEmail;
  }

  public String getExternalOrderNumber()
  {
    return externalOrderNumber;
  }

  public void setExternalOrderNumber(String newExternalOrderNumber)
  {
    if(valueChanged(newExternalOrderNumber,externalOrderNumber)){
      setChanged(true);
    }              
    externalOrderNumber = newExternalOrderNumber;
  }

  public String getColorFirstChoice()
  {
    return colorFirstChoice;
  }

  public void setColorFirstChoice(String newColorFirstChoice)
  {
    if(valueChanged(colorFirstChoice,newColorFirstChoice)){
      setChanged(true);
    }                
    colorFirstChoice = newColorFirstChoice;
  }

  public String getColorSecondChoice()
  {
    return colorSecondChoice;
  }

  public void setColorSecondChoice(String newColorSecondChoice)
  {
    if(valueChanged(colorSecondChoice,newColorSecondChoice)){
      setChanged(true);
    }                
    colorSecondChoice = newColorSecondChoice;
  }

  public String getSizeChoice()
  {
    return sizeChoice;
  }

  public void setSizeChoice(String newSizeChoice)
  {
    if(valueChanged(sizeChoice,newSizeChoice)){
      setChanged(true);
    }                
    sizeChoice = newSizeChoice;
  }

  public String getCardMessage()
  {
    return cardMessage;
  }

  public void setCardMessage(String newCardMessage)
  {
    if(valueChanged(cardMessage,newCardMessage)){
      setChanged(true);
    }                
    cardMessage = newCardMessage;
  }

  public String getCardSignature()
  {
    return cardSignature;
  }

  public void setCardSignature(String newCardSignature)
  {
    if(valueChanged(cardSignature,newCardSignature)){
      setChanged(true);
    }                
    cardSignature = newCardSignature;
  }

  public String getOrderComments()
  {
    return orderComments;
  }

  public void setOrderComments(String newOrderComments)
  {
    if(valueChanged(orderComments,newOrderComments)){
      setChanged(true);
    }                
    orderComments = newOrderComments;
  }

  public String getOrderContactInformation()
  {
    return orderContactInformation;
  }

  public void setOrderContactInformation(String newOrderContactInformation)
  {
    if(valueChanged(orderContactInformation,newOrderContactInformation)){
      setChanged(true);
    }                
    orderContactInformation = newOrderContactInformation;
  }

  public String getFloristNumber()
  {
    return floristNumber;
  }

  public void setFloristNumber(String newFloristNumber)
  {
    if(valueChanged(floristNumber,newFloristNumber)){
      setChanged(true);
    }                
    floristNumber = newFloristNumber;
  }

  public String getSpecialInstructions()
  {
    return specialInstructions;
  }

  public void setSpecialInstructions(String newSpecialInstructions)
  {
    if(valueChanged(specialInstructions,newSpecialInstructions)){
      setChanged(true);
    }                
    specialInstructions = newSpecialInstructions;
  }

  public String getAribaUnspscCode()
  {
    return aribaUnspscCode;
  }

  public void setAribaUnspscCode(String newAribaUnspscCode)
  {
    if(valueChanged(aribaUnspscCode,newAribaUnspscCode)){
      setChanged(true);
    }                
    aribaUnspscCode = newAribaUnspscCode;
  }

  public String getAribaPoNumber()
  {
    return aribaPoNumber;
  }

  public void setAribaPoNumber(String newAribaPoNumber)
  {
    if(valueChanged(aribaPoNumber,newAribaPoNumber)){
      setChanged(true);
    }                
    aribaPoNumber = newAribaPoNumber;
  }

  public String getAribaAmsProjectCode()
  {
    return aribaAmsProjectCode;
  }

  public void setAribaAmsProjectCode(String newAribaAmsProjectCode)
  {
    if(valueChanged(aribaAmsProjectCode,newAribaAmsProjectCode)){
      setChanged(true);
    }                
    aribaAmsProjectCode = newAribaAmsProjectCode;
  }

  public String getAribaCostCenter()
  {
    return aribaCostCenter;
  }

  public void setAribaCostCenter(String newAribaCostCenter)
  {
    if(valueChanged(aribaCostCenter,newAribaCostCenter)){
      setChanged(true);
    }                
    aribaCostCenter = newAribaCostCenter;
  }

  public String getPersonalizationData() {
      return personalizationData;
  }
  
  public void setPersonalizationData(String newPersonalizationData) {
      personalizationData = newPersonalizationData;
  }
  
  public List getAddOns()
  {
    return addOns;
  }

  public void setAddOns(List newAddOns)
  {
    addOns = newAddOns;
  }

  public List getRecipients()
  {
    return recipients;
  }

  public void setRecipients(List newRecipients)
  {

    recipients = newRecipients;
  }

  public String getSubstituteAcknowledgement()
  {
    return substituteAcknowledgement;
  }

  public void setSubstituteAcknowledgement(String newSubstituteAcknowledgement)
  {
    if(valueChanged(substituteAcknowledgement,newSubstituteAcknowledgement)){
      setChanged(true);
    }                
    substituteAcknowledgement = newSubstituteAcknowledgement;
  }

  public void setAvsAddress(AVSAddressVO avsAddress)
  {
      this.avsAddress = avsAddress;
  }

  public AVSAddressVO getAvsAddress()
  {
      return avsAddress;
  }

  public String getExternalOrderTotal()
  {
      return externalOrderTotal;
  }

  public void setExternalOrderTotal(String newExternalOrderTotal)
  {
    if(valueChanged(externalOrderTotal,checkNullDouble(newExternalOrderTotal))){
      setChanged(true);
    }                
    externalOrderTotal = checkNullDouble(newExternalOrderTotal);
  }

  public String getMilesPointsAmt()
  {
    return milesPointsAmt;
  }

  public void setMilesPointsAmt(String newMilesPointsPaymentAmt)
  {
    if(valueChanged(milesPointsAmt,checkNullDouble(newMilesPointsPaymentAmt))){
      setChanged(true);
    }                
    milesPointsAmt = checkNullDouble(newMilesPointsPaymentAmt);
  }

  public String getSenderInfoRelease()
  {
    return senderInfoRelease;
  }

  public void setSenderInfoRelease(String newSenderInfoRelease)
  {
    if(valueChanged(senderInfoRelease,newSenderInfoRelease)){
      setChanged(true);
    }                
    senderInfoRelease = newSenderInfoRelease;
  }
  
  public String toString()
  {
    String lineSep = System.getProperty("line.separator");
    StringBuffer sb = new StringBuffer();
    StringBuffer listSb = new StringBuffer();
    // Get this Class
    Class thisClass = this.getClass();
    sb.append("Object Type = ").append(thisClass.getName()).append(lineSep);
        
    // Use reflection to loop through all the fields
    Field[] fields = thisClass.getDeclaredFields();
    for(int i = 0; i < fields.length; i++)
    {
        try
        {
            if(fields[i].getType().equals(Class.forName("java.lang.String")))
            {
                sb.append(fields[i].getName()).append(" = ").append(fields[i].get(this)).append(lineSep);
            }
            else if(fields[i].getType().equals(Class.forName("java.util.List")))
            {
                List list = (List)fields[i].get(this);
                if(list != null)
                {
                    for (int j = 0; j < list.size(); j++) 
                    {
                        listSb.append(list.get(j).toString());
                    }
                }
            }
            else if (fields[i].getType().equals(Class.forName("com.ftd.osp.utilities.order.vo.AVSAddressVO")))
                if (fields[i].get(this) != null)
                    listSb.append(fields[i].get(this).toString());

        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    sb.append(listSb);
        
    return sb.toString();
  }

  public String getStatus()
  {
    return status;
  }

  public void setStatus(String newStatus)
  {
    if(valueChanged(status,newStatus)){
      setChanged(true);
    }                
    status = newStatus;
  }

  public String getItemDescription()
  {

    return itemDescription;
  }

  public void setItemDescription(String newItemDescription)
  {
    if(valueChanged(itemDescription,newItemDescription)){
      setChanged(true);
    }                
    itemDescription = newItemDescription;
  }

  public String getFloristName()
  {
    return floristName;
  }

  public void setFloristName(String newFloristName)
  {
    if(valueChanged(floristName,newFloristName)){
      setChanged(true);
    }                
    floristName = newFloristName;
  }

  public String getMilesPoints()
  {
    return milesPoints;
  }

  public void setMilesPoints(String newMilesPoints)
  {
    if(valueChanged(milesPoints,newMilesPoints)){
      setChanged(true);
    }                
    milesPoints = newMilesPoints;
  }

  public String getShipMethodCarrierFlag()
  {
    return shipMethodCarrierFlag;
  }

  public void setShipMethodCarrierFlag(String newShipMethodCarrierFlag)
  {
    if(valueChanged(shipMethodCarrierFlag,newShipMethodCarrierFlag)){
      setChanged(true);
    }                
    shipMethodCarrierFlag = newShipMethodCarrierFlag;
  }

  public String getShipMethodFloristFlag()
  {
    return shipMethodFloristFlag;
  }

  public void setShipMethodFloristFlag(String newShipMethodFloristFlag)
  {
    if(valueChanged(shipMethodFloristFlag,newShipMethodFloristFlag)){
      setChanged(true);
    }                
    shipMethodFloristFlag = newShipMethodFloristFlag;
  }

  public List getDispositions()
  {
    return Dispositions;
  }

  public void setDispositions(List newDispositions)
  {

    Dispositions = newDispositions;
  }

  public String getDiscountedProductPrice()
  {
    return DiscountedProductPrice;
  }

  public void setDiscountedProductPrice(String newDiscountedProductPrice)
  {
    if(valueChanged(DiscountedProductPrice,newDiscountedProductPrice)){
      setChanged(true);
    }                
    DiscountedProductPrice = newDiscountedProductPrice;
  }

  public String getRewardType()
  {
    return RewardType;
  }

  public void setRewardType(String newRewardType)
  {
    if(valueChanged(RewardType,newRewardType)){
      setChanged(true);
    }                
    RewardType = newRewardType;
  }

  public String getDiscountType()
  {
    return DiscountType;
  }

  public void setDiscountType(String newDiscountType)
  {
    if(valueChanged(DiscountType,newDiscountType)){
      setChanged(true);
    }                
    DiscountType = newDiscountType;
  }

  public String getColorOneDescription()
  {
    return colorOneDescription;
  }

  public void setColorOneDescription(String newColorOneDescription)
  {
    if(valueChanged(colorOneDescription,newColorOneDescription)){
      setChanged(true);
    }                
    colorOneDescription = newColorOneDescription;
  }

  public String getColorTwoDescription()
  {
    return colorTwoDescription;
  }

  public void setColorTwoDescription(String newColorTwoDescription)
  {
    if(valueChanged(colorTwoDescription,newColorTwoDescription)){
      setChanged(true);
    }                
    colorTwoDescription = newColorTwoDescription;
  }

    public String getProductSubCodeId()
    {
        return productSubCodeId;
    }

    public void setProductSubCodeId(String newProductSubCodeId)
    {
    if(valueChanged(productSubCodeId,newProductSubCodeId)){
      setChanged(true);
    }                  
        productSubCodeId = newProductSubCodeId;
    }

  public String getProductSubcodeDescription()
  {
    return productSubcodeDescription;
  }

  public void setProductSubcodeDescription(String newProductSubcodeDescription)
  {
    if(valueChanged(productSubcodeDescription,newProductSubcodeDescription)){
      setChanged(true);
    }                
    productSubcodeDescription = newProductSubcodeDescription;
  }

  public String getPercentOff()
  {
    return percentOff;
  }

  public void setPercentOff(String newPercentOff)
  {
    if(valueChanged(percentOff,newPercentOff)){
      setChanged(true);
    }                
    percentOff = newPercentOff;
  }

  public String getProductType()
  {
    return productType;
  }

  public void setProductType(String newProductType)
  {
    if(valueChanged(productType,newProductType)){
      setChanged(true);
    }                
    productType = newProductType;
  }

  public String getOccasionDescription()
  {
    return occasionDescription;
  }

  public void setOccasionDescription(String newOccasionDescription)
  {
    if(valueChanged(occasionDescription,newOccasionDescription)){
      setChanged(true);
    }                
    occasionDescription = newOccasionDescription;
  }

  public String getStatusSortOrder()
  {
        return statusSortOrder;
  }

  public void setStatusSortOrder(String newStatusSortOrder)
  {
    if(valueChanged(statusSortOrder,newStatusSortOrder)){
      setChanged(true);
    }                
        statusSortOrder = newStatusSortOrder;
  }

    public String getHpSequence()
    {
        return hpSequence;
    }

    public void setHpSequence(String newHpSequence)
    {
        hpSequence = newHpSequence;
    }

    public String getReinstateFlag()
    {
        return reinstateFlag;
    }

    public void setReinstateFlag(String newReinstateFlag)
    {
        reinstateFlag = newReinstateFlag;
    }
    
    public String getCommission()
    {
      return commission;   
    
    }
    public void setCommission(String newCommission)
    {
      if( valueChanged(commission, newCommission))
      {
        setChanged(true);
      }
      commission = newCommission;
    }
    
   
    public String getWholesale()
    {
      return wholesale;
    }
    
    public void setWholesale(String newWholesale)
    {
        if( valueChanged(wholesale, newWholesale))
        {
          setChanged(true);
        }
        wholesale = newWholesale;
    }
    
    public String getTransaction()
    {
      return transaction;
    }
    
    public void setTransaction(String newTransaction)
    {
        if(valueChanged(transaction, newTransaction))
        {
          setChanged(true);
        }
        transaction = newTransaction;
    }
    
    public String getPdbPrice()
    {
      return pdbPrice;
    }
    
    public void setPdbPrice(String newPdbPrice)
    {
      if(valueChanged(pdbPrice,newPdbPrice))
      {
        setChanged(true);
      }
      pdbPrice = newPdbPrice;
    }
    
	public String getShippingTax() {
		if (this.itemTaxVO != null) {
			return itemTaxVO.getShippingTax();
		}
		return ItemTaxVO.ZERO_VALUE;
	}

	public void setShippingTax(String newShippingTax) {
		if (this.itemTaxVO == null) {
			itemTaxVO = new ItemTaxVO();
			setChanged(true);
		} else if (valueChanged(this.itemTaxVO.getShippingTax(), newShippingTax)) {
			setChanged(true);
		}
		this.itemTaxVO.setShippingTax(newShippingTax);
	}
	
   
     public String getPriceOverrideFlag()
    {
        return priceOverrideFlag;
    }
    
    public void setPriceOverrideFlag(String newPriceOverrideFlag)
    {
        if(valueChanged(priceOverrideFlag, newPriceOverrideFlag))
        {
          setChanged(true);
        }
        priceOverrideFlag = newPriceOverrideFlag;
    }
    

    public List getOrderDetailExtensions()
    {
      return orderDetailExtensions;
    }

    public void setOrderDetailExtensions(List a_orderDetailExtensions)
    {
      orderDetailExtensions = a_orderDetailExtensions;
    }

    public String getPartnerCost()
    {
        return partnerCost;
    }

    public void setPartnerCost(String newpartnerCost)
    {
        if(valueChanged(partnerCost, newpartnerCost))
        {
          setChanged(true);
        }
        this.partnerCost = newpartnerCost;
    }


    public void setOrderAmount(String orderAmount)
    {
        this.orderAmount = orderAmount;
    }


    public String getOrderAmount()
    {
        return orderAmount;
    }


    public void setGccAmt(String gccAmt)
    {
        this.gccAmt = gccAmt;
    }


    public String getGccAmt()
    {
        return gccAmt;
    }


	public void setServiceFeeTax(String serviceFeeTax) {
		if (this.itemTaxVO == null) {
			itemTaxVO = new ItemTaxVO();
			setChanged(true);
		} else if (valueChanged(this.itemTaxVO.getServiceFeeTax(), serviceFeeTax)) {
			setChanged(true);
		}
		this.itemTaxVO.setServiceFeeTax(serviceFeeTax);
	}

	public String getServiceFeeTax() {
		if (this.itemTaxVO != null) {
			return itemTaxVO.getServiceFeeTax();
		}
		return ItemTaxVO.ZERO_VALUE;
	}
    
	public void setProductTax(String productTax) {
		if (this.itemTaxVO == null) {
			itemTaxVO = new ItemTaxVO();
			setChanged(true);
		} else if (valueChanged(this.itemTaxVO.getProductTax(), productTax)) {
			setChanged(true);
		}
		this.itemTaxVO.setProductTax(productTax);
	}


	public String getProductTax() {
		if (this.itemTaxVO != null) {
			return itemTaxVO.getProductTax();
		}
		return ItemTaxVO.ZERO_VALUE;
	}

    public Date getUpdatedOnProductsAmountTime()
    {
      return updatedOnProductsAmountTime;
    }

    public void setUpdatedOnProductsAmountTime(Date newUpdatedOnProductsAmountTime)
    {
      updatedOnProductsAmountTime = newUpdatedOnProductsAmountTime;
    }
    
    public String getUpdatedOnProductsAmount()
    {
      if (getUpdatedOnProductsAmountTime() != null) {
          return updatedOnFormat.format(getUpdatedOnProductsAmountTime());
      } else {
          return null;
      }
    }

    public void setMilesPointsAmtProduct(String milesPointsAmtProduct) {
        this.milesPointsAmtProduct = milesPointsAmtProduct;
    }

    public String getMilesPointsAmtProduct() {
        return milesPointsAmtProduct;
    }

    public void setMilesPointsAmtAddon(String milesPointsAmtAddon) {
        this.milesPointsAmtAddon = milesPointsAmtAddon;
    }

    public String getMilesPointsAmtAddon() {
        return milesPointsAmtAddon;
    }

    public void setPersonalGreetingId(String personalGreetingId) {
        this.personalGreetingId = personalGreetingId;
    }

    public String getPersonalGreetingId() {
        return personalGreetingId;
    }

    public void setNoTaxFlag(String noTaxFlag) {
        this.noTaxFlag = noTaxFlag;
    }

    public String getNoTaxFlag() {
        return noTaxFlag;
    }
    public void setPremierCollectionFlag(String premierCollectionFlag) {
        this.premierCollectionFlag = premierCollectionFlag;
    }
    public String getPremierCollectionFlag() {
        return premierCollectionFlag;
    }

    public String getOrigSourceCode()
    {
      return origSourceCode;
    }
  
    public void setOrigSourceCode(String origSourceCode)
    {
        this.origSourceCode = origSourceCode;
    }
  
    public String getOrigProductId()
    {
        return origProductId;
    }
  
    public void setOrigProductId(String origProductId)
    {
        this.origProductId = origProductId;
    }
  
    public String getOrigShipMethod()
    {
        return origShipMethod;
    }
  
    public void setOrigShipMethod(String origShipMethod)
    {
        this.origShipMethod = origShipMethod;
    }
  
    public String getOrigDeliveryDate()
    {
        return origDeliveryDate;
    }
  
    public void setOrigDeliveryDate(String origDeliveryDate)
    {
        this.origDeliveryDate = origDeliveryDate;
    }
  
    public String getOrigTotalFeeAmount()
    {
        return origTotalFeeAmount;
    }
  
    public void setOrigTotalFeeAmount(String origTotalFeeAmount)
    {
        this.origTotalFeeAmount = origTotalFeeAmount;
    }

    /**
   * This method will retun true if something has changed from the original that causes
   * a change in the charges that will be assigned to an order from when it was originally placed
   * @return
   */
    public Boolean isChargeRelatedItemChanged()
    {
      boolean isChanged = false;

      //Old orders may not have original values set
      //null calculation makes this difficult pay attention to the !s if you are trying to understand
      //what is going on.
      
      //Check for changed in source code

      if (this.getOrigSourceCode() == null || this.getSourceCode() == null)
      {
        if (!(this.getOrigSourceCode() == null && this.getSourceCode() == null))
        {
          isChanged = true;
        }        
      }
      else
      {
        if (!(this.getOrigSourceCode().equals(this.getSourceCode())) )
        {
          isChanged = true;
        }
      }

      //Check for changes in productId
      if (!isChanged)
      { 
        if (this.getOrigProductId() == null || this.getProductId() == null)
        {
          if (!(this.getOrigProductId() == null && this.getProductId() == null))
          {
            isChanged = true;
          }
            
        }
        else
        {
          if (!(this.getOrigProductId().equals(this.getProductId())) )
          {
            isChanged = true;
          }
        }
      }
      //Check for changes in ship method
      if (!isChanged)
      { 
        if (this.getOrigShipMethod() == null || this.getShipMethod() == null)
        {
          if (!(this.getOrigShipMethod() == null && this.getShipMethod() == null))
          {
            isChanged = true;
          }
            
        }
        else
        {
          if (!(this.getOrigShipMethod().equals(this.getShipMethod())) )
          {
            isChanged = true;
          }
        }
      }
      
      //Check for changes in delivery date
      if (!isChanged)
      { 
        if (this.getOrigDeliveryDate() == null || this.getDeliveryDate() == null)
        {
          if (!(this.getOrigDeliveryDate() == null && this.getDeliveryDate() == null))
          {
            isChanged = true;
          }
            
        }
        else
        {
          if (!(this.getOrigDeliveryDate().equals(this.getDeliveryDate())) )
          {
            isChanged = true;
          }
        }
      }
      return isChanged;
    }

    public void setPreferredProcessingPartner(String preferredProcessingPartner) {
        this.preferredProcessingPartner = preferredProcessingPartner;
    }

    public String getPreferredProcessingPartner() {
        return preferredProcessingPartner;
    }

    public void setSameDayFreshcutFlag(String sameDayFreshcutFlag) {
        this.sameDayFreshcutFlag = sameDayFreshcutFlag;
    }

    public String getSameDayFreshcutFlag() {
        return sameDayFreshcutFlag;
    }

    public void setSameDayGiftFlag(String sameDayGiftFlag) {
        this.sameDayGiftFlag = sameDayGiftFlag;
    }

    public String getSameDayGiftFlag() {
        return sameDayGiftFlag;
    }

    public void setBinSourceChangedFlag(String binSourceChangedFlag)
    {
        this.binSourceChangedFlag = binSourceChangedFlag;
    }

    public String getBinSourceChangedFlag()
    {
        return binSourceChangedFlag;
    }

    public void setDomesticFloristServiceCharge(String domesticFloristServiceCharge) {
        this.domesticFloristServiceCharge = domesticFloristServiceCharge;
    }

    public String getDomesticFloristServiceCharge() {
        return domesticFloristServiceCharge;
    }

    public void setInternationalFloristServiceCharge(String internationalFloristServiceCharge) {
        this.internationalFloristServiceCharge = internationalFloristServiceCharge;
    }

    public String getInternationalFloristServiceCharge() {
        return internationalFloristServiceCharge;
    }

    public void setShippingCost(String shippingCost) {
        this.shippingCost = shippingCost;
    }

    public String getShippingCost() {
        return shippingCost;
    }

    public void setVendorCharge(String vendorCharge) {
        this.vendorCharge = vendorCharge;
    }

    public String getVendorCharge() {
        return vendorCharge;
    }

    public void setSaturdayUpcharge(String saturdayUpcharge) {
        this.saturdayUpcharge = saturdayUpcharge;
    }

    public String getSaturdayUpcharge() {
        return saturdayUpcharge;
    }

    public void setAlaskaHawaiiSurcharge(String alaskaHawaiiSurcharge) {
        this.alaskaHawaiiSurcharge = alaskaHawaiiSurcharge;
    }

    public String getAlaskaHawaiiSurcharge() {
        return alaskaHawaiiSurcharge;
    }

    public void setFuelSurcharge(String fuelSurcharge) {
        this.fuelSurcharge = fuelSurcharge;
    }

    public String getFuelSurcharge() {
        return fuelSurcharge;
    }

  public void setFuelSurchargeDescription(String fuelSurchargeDescription)
  {
    this.fuelSurchargeDescription = fuelSurchargeDescription;
  }

  public String getFuelSurchargeDescription()
  {
    return fuelSurchargeDescription;
  }

  public void setApplySurchargeCode(String applySurchargeCode)
  {
    this.applySurchargeCode = applySurchargeCode;
  }

  public String getApplySurchargeCode()
  {
    return applySurchargeCode;
  }

  public void setSendSurchargeToFlorist(String sendSurchargeToFlorist)
  {
    this.sendSurchargeToFlorist = sendSurchargeToFlorist;
  }

  public String getSendSurchargeToFlorist()
  {
    return sendSurchargeToFlorist;
  }

  public void setDisplaySurcharge(String displaySurcharge)
  {
    this.displaySurcharge = displaySurcharge;
  }

  public String getDisplaySurcharge()
  {
    return displaySurcharge;
  }

  public void setServiceFeeAmountSavings(String serviceFeeAmountSavings)
  {
    if(valueChanged(this.serviceFeeAmountSavings, serviceFeeAmountSavings))
      setChanged(true);
          
    this.serviceFeeAmountSavings = serviceFeeAmountSavings;
  }

  public String getServiceFeeAmountSavings()
  {
    return serviceFeeAmountSavings;
  }

  public void setShippingFeeAmountSavings(String shippingFeeAmountSavings)
  {
    if(valueChanged(this.shippingFeeAmountSavings, shippingFeeAmountSavings))
      setChanged(true);
      
    this.shippingFeeAmountSavings = shippingFeeAmountSavings;
  }

  public String getShippingFeeAmountSavings()
  {
    return shippingFeeAmountSavings;
  }

  public void setAutoRenew(String autoRenew)
  {
    this.autoRenew = autoRenew;
  }

  public String getAutoRenew()
  {
    return autoRenew;
  }

  public void setFreeShipping(String freeShipping)
  {
    this.freeShipping = freeShipping;
  }

  public String getFreeShipping()
  {
    return freeShipping;
  }

  public void setRecalculateAppliedFreeShipping(boolean recalculateAppliedFreeShipping)
  {
    this.recalculateAppliedFreeShipping = recalculateAppliedFreeShipping;
  }

  public boolean isRecalculateAppliedFreeShipping()
  {
    return recalculateAppliedFreeShipping;
  }

    public void setProductSubType(String productSubType) {
        this.productSubType = productSubType;
    }

    public String getProductSubType() {
        return productSubType;
    }


public void setSameDayUpcharge(String sameDayUpcharge) {
	if(valueChanged(this.sameDayUpcharge, sameDayUpcharge))
	setChanged(true);
	
	this.sameDayUpcharge = sameDayUpcharge;
}

public String getSameDayUpcharge() {
	return sameDayUpcharge;
}

public void setOriginalOrderHasSDU(String originalOrderHasSDU) {
	this.originalOrderHasSDU = originalOrderHasSDU;
}

public String getOriginalOrderHasSDU() {
	return originalOrderHasSDU;
}

public void setMorningDeliveryFee(String morningDeliveryFee) {
	if(valueChanged(this.morningDeliveryFee, morningDeliveryFee))
		setChanged(true);
	this.morningDeliveryFee = morningDeliveryFee;
}

public String getMorningDeliveryFee() {
	return morningDeliveryFee;
}

	//#11946 - BBN
	/** Method returns 'Y' if customer opted for Morning delivery.
	 * @return
	 */
	public String getMorningDeliveryOpted() {
		return morningDeliveryOpted;
	}

	/** Method sets customer's Morning delivery option.
	 * @param morningDeliveryOpted
	 */
	public void setMorningDeliveryOpted(String morningDeliveryOpted) {
		this.morningDeliveryOpted = morningDeliveryOpted;
	}

	/** Method to get the original order has morning delivery fe included.
	 * returns Y/null
	 * @return
	 */
	public String getOriginalOrderHasMDF() {
		return originalOrderHasMDF;
	}

	/** Sets the original order has morning delivery flag.
	 * @param originalOrderHasMDF
	 */
	public void setOriginalOrderHasMDF(String originalOrderHasMDF) {
		this.originalOrderHasMDF = originalOrderHasMDF;
	}

	/**
	 * @return
	 */
	public String getProductMorningDeliveryFlag() {
		return productMorningDeliveryFlag;
	}

	/**
	 * @param productMorningDeliveryFlag
	 */
	public void setProductMorningDeliveryFlag(String productMorningDeliveryFlag) {
		this.productMorningDeliveryFlag = productMorningDeliveryFlag;
	}

	public String getMorningDeliveryAvailable() {
		return morningDeliveryAvailable;
	}

	public void setMorningDeliveryAvailable(String morningDeliveryAvailable) {
		this.morningDeliveryAvailable = morningDeliveryAvailable;
	}

	public String getAmazonStatus() {
		return amazonStatus;
	}

	public void setAmazonStatus(String amazonStatus) {
		this.amazonStatus = amazonStatus;
	}
	
	public String getBbnChargedtoFSMembers() {
			return bbnChargedtoFSMembers;
	}

	public void setBbnChargedtoFSMembers(String bbnChargedtoFSMembers) {
			this.bbnChargedtoFSMembers = bbnChargedtoFSMembers;
	}

	public String getOrigExternalOrderTotal() {
		return origExternalOrderTotal;
	}

	public void setOrigExternalOrderTotal(String origExternalOrderTotal) {
		this.origExternalOrderTotal = origExternalOrderTotal;
	}

	public void setPcGroupId(String pcGroupId) {
		this.pcGroupId = pcGroupId;
	}

	public String getPcGroupId() {
		return pcGroupId;
	}

	public void setPcMembershipId(String pcMembershipId) {
		this.pcMembershipId = pcMembershipId;
	}

	public String getPcMembershipId() {
		return pcMembershipId;
	}

	public void setPcFlag(String pcFlag) {
		this.pcFlag = pcFlag;
	}

	public String getPcFlag() {
		return pcFlag;
	}

	public String getMercentStatus() {
		return mercentStatus;
	}

	public void setMercentStatus(String mercentStatus) {
		this.mercentStatus = mercentStatus;
	}

	public String getOriginalServiceFeeAmount() {
		return originalServiceFeeAmount;
	}

	public void setOriginalServiceFeeAmount(String originalServiceFeeAmount) {
		this.originalServiceFeeAmount = originalServiceFeeAmount;
	}

	public String getOriginalShippingFeeAmount() {
		return originalShippingFeeAmount;
	}

	public void setOriginalShippingFeeAmount(String originalShippingFeeAmount) {
		this.originalShippingFeeAmount = originalShippingFeeAmount;
	}

	public void setNovatorId(String novatorId) {
		this.novatorId = novatorId;
	}

	public String getNovatorId() {
		return novatorId;
	}

	public ItemTaxVO getItemTaxVO() {
		if(this.itemTaxVO == null) {
			this.itemTaxVO = new ItemTaxVO();
		}
		return itemTaxVO;
	}

	public void setItemTaxVO(ItemTaxVO itemTaxVO) {
		this.itemTaxVO = itemTaxVO;
	}

	public boolean isTaxServicePerformed() {
		return taxServicePerformed;
	}

	public void setTaxServicePerformed(boolean taxServicePerformed) {
		this.taxServicePerformed = taxServicePerformed;
	}

	public String getTimeOfService() {
		return timeOfService;
	}

	public void setTimeOfService(String timeOfService) {
		this.timeOfService = timeOfService;
	}

	/**
	 * @return the addOnDiscountAmount
	 */
	public String getAddOnDiscountAmount() {
		return addOnDiscountAmount;
	}

	/**
	 * @param addOnDiscountAmount the addOnDiscountAmount to set
	 */
	public void setAddOnDiscountAmount(String addOnDiscountAmount) {
		this.addOnDiscountAmount = addOnDiscountAmount;
	}

	/**
	 * @return the sundayUpcharge
	 */
	public String getSundayUpcharge() {
		return sundayUpcharge;
	}

	/**
	 * @param sundayUpcharge the sundayUpcharge to set
	 */
	public void setSundayUpcharge(String sundayUpcharge) {
		this.sundayUpcharge = sundayUpcharge;
	}

	/**
	 * @return the mondayUpcharge
	 */
	public String getMondayUpcharge() {
		return mondayUpcharge;
	}

	/**
	 * @param mondayUpcharge the mondayUpcharge to set
	 */
	public void setMondayUpcharge(String mondayUpcharge) {
		this.mondayUpcharge = mondayUpcharge;
	}

	/**
	 * @return the latecutoffCharge
	 */
	public String getLatecutoffCharge() {
		return latecutoffCharge;
	}

	/**
	 * @param latecutoffCharge the latecutoffCharge to set
	 */
	public void setLatecutoffCharge(String latecutoffCharge) {
		this.latecutoffCharge = latecutoffCharge;
	}

	/**
	 * @return the originalOrderHasLCF
	 */
	public String getOriginalOrderHasLCF() {
		return originalOrderHasLCF;
	}

	/**
	 * @param originalOrderHasLCF the originalOrderHasLCF to set
	 */
	public void setOriginalOrderHasLCF(String originalOrderHasLCF) {
		this.originalOrderHasLCF = originalOrderHasLCF;
	}

	/**
	 * @return the ftdwAllowedShipping
	 */
	public String getFtdwAllowedShipping() {
		return ftdwAllowedShipping;
	}

	/**
	 * @param ftdwAllowedShipping the ftdwAllowedShipping to set
	 */
	public void setFtdwAllowedShipping(String ftdwAllowedShipping) {
		this.ftdwAllowedShipping = ftdwAllowedShipping;
	}

	public String getShippingSystem() {
		return shippingSystem;
	}

	public void setShippingSystem(String shippingSystem) {
		this.shippingSystem = shippingSystem;
	}

	/**
	 * @return the applyOrigFee
	 */
	public boolean isApplyOrigFee() {
		return applyOrigFee;
	}

	/**
	 * @param applyOrigFee the applyOrigFee to set
	 */
	public void setApplyOrigFee(boolean applyOrigFee) {
		this.applyOrigFee = applyOrigFee;
	}
	
	/**
	 * Check if there is any change in charge related items except for ship method
	 * 
	 * @return
	 */
	public boolean isOnlyShipmethodChanged() { 
		
		// return true is source code is changed
		if (this.getOrigSourceCode() == null || this.getSourceCode() == null) {
			if (!(this.getOrigSourceCode() == null && this.getSourceCode() == null)) {
				return false;
			}
		} else {
			if (!(this.getOrigSourceCode().equals(this.getSourceCode()))) {
				return false;
			}
		}

		// return true is product code is changed
		if (this.getOrigProductId() == null || this.getProductId() == null) {
			if (!(this.getOrigProductId() == null && this.getProductId() == null)) {
				return false;
			}

		} else {
			if (!(this.getOrigProductId().equals(this.getProductId()))) {
				return false;
			}
		}
		
		// return true if delivery date is changed
		if (this.getOrigDeliveryDate() == null || this.getDeliveryDate() == null) {
			if (!(this.getOrigDeliveryDate() == null && this
					.getDeliveryDate() == null)) {
				return false;
			}

		} else {
			if (!(this.getOrigDeliveryDate().equals(this.getDeliveryDate()))) {
				return false;
			}
		}
		
		// None of the field changed - return true if Ship method is changed
		if (this.getOrigShipMethod() == null || this.getShipMethod() == null) {
			if (!(this.getOrigShipMethod() == null && this.getShipMethod() == null)) {
				return true;
			}
		} else {
			if (!(this.getOrigShipMethod().equals(this.getShipMethod()))) {
				return true;
			}
		}			
		// return false when ship method is not changed.
		return false;
	}
	
	public void setLegacyId(String legacyId) {
		this.legacyId = legacyId;
	}

	public String getLegacyId() {
		return this.legacyId;
	}

	public String getSduChargedtoFSMembers() {
		return sduChargedtoFSMembers;
	}

	public void setSduChargedtoFSMembers(String sduChargedtoFSMembers) {
		this.sduChargedtoFSMembers = sduChargedtoFSMembers;
	}

	public String getOriginalOrderHasSDUFS() {
		return originalOrderHasSDUFS;
	}

	public void setOriginalOrderHasSDUFS(String originalOrderHasSDUFS) {
		this.originalOrderHasSDUFS = originalOrderHasSDUFS;
	}
	
}
