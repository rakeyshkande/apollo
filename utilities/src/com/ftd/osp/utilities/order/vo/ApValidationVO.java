package com.ftd.osp.utilities.order.vo;

import org.w3c.dom.Document;
import java.util.List;
import java.lang.reflect.*;
import java.io.Serializable;

import java.math.BigDecimal;

import java.util.HashMap;
import java.util.Map;

public class ApValidationVO implements Serializable  
{

  private String paymentMethodId;
  private Map validationProperties;

  public ApValidationVO()
  {
        validationProperties = new HashMap();
  }


    public void setPaymentMethodId(String paymentMethodId) {
        this.paymentMethodId = paymentMethodId;
    }

    public String getPaymentMethodId() {
        return paymentMethodId;
    }

    public void addValidationProperty(String key, String value) {
        validationProperties.put(key, value);
    }

    public Map getValidationProperties() {
        return validationProperties;
    }

}

