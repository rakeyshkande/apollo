package com.ftd.osp.utilities.order.vo;

import java.io.Serializable;

public class OrderContactInfoVO extends BaseVO implements Serializable
{
  private long orderContactInfoId;
  private String orderGuid;
  private String firstName;
  private String lastName;
  private String phone;
  private String ext;
  private String email;

  /**
   * Order Contact Info Value Object that maps to ORDER_CONTACT_INFO 
   * database table
   * This is a sub collection within Order value object
   *
   * @author Rose Lazuk
   */

  public OrderContactInfoVO()
  {
  }

  public long getOrderContactInfoId()
  {
    return orderContactInfoId;
  }

  public void setOrderContactInfoId(long newOrderContactInfoId)
  {
    if(valueChanged(orderContactInfoId, newOrderContactInfoId))
    {
        setChanged(true);
    }
    orderContactInfoId = newOrderContactInfoId;
  }

  public String getOrderGuid()
  {
    return orderGuid;
  }

  public void setOrderGuid(String newOrderGuid)
  {
    if(valueChanged(orderGuid, newOrderGuid))
    {
        setChanged(true);
    }
    orderGuid = newOrderGuid;
  }

  public String getFirstName()
  {
    return firstName;
  }

  public void setFirstName(String newFirstName)
  {
    if(valueChanged(firstName, newFirstName))
    {
        setChanged(true);
    }
    firstName = newFirstName;
  }

  public String getLastName()
  {
    return lastName;
  }

  public void setLastName(String newLastName)
  {
    if(valueChanged(lastName, newLastName))
    {
        setChanged(true);
    }
    lastName = newLastName;
  }

  public String getPhone()
  {
    return phone;
  }

  public void setPhone(String newPhone)
  {
    if(valueChanged(phone, newPhone))
    {
        setChanged(true);
    }
    phone = newPhone;
  }

  public String getExt()
  {
    return ext;
  }

  public void setExt(String newExt)
  {
    if(valueChanged(ext, newExt))
    {
        setChanged(true);
    }
    ext = newExt;
  }

  public String getEmail()
  {
    return email;
  }

  public void setEmail(String newEmail)
  {
    if(valueChanged(email, newEmail))
    {
        setChanged(true);
    }
    email = newEmail;
  }
}