package com.ftd.osp.utilities.order.vo;

import java.math.BigDecimal;

/**
 * Subset of Source Information used for Recalculation logic.
 */
public class RecalculateOrderSourceCodeVO
{
  private String sourceCode;
  private String jcpenneyFlag;
  private String partnerId;
  private String pricingCode;
  private String shippingCode;
  private String allowFreeShippingFlag;
  private String programType;
  private String calculationBasis;
  private BigDecimal points;
  private String bonusCalculationBasis;
  private BigDecimal bonusPoints;
  private String rewardType;
  private String sameDayUpcharge;
  private String displaySameDayUpcharge;
  private String sameDayUpchargeFS;
  
  // #11946 - Morning Delivery flag and Morning delivery for free shipping flags
  private String morningDeliveryFlag;
  private String allowBBNForFSMemeber;

  public void setJcpenneyFlag(String jcpenneyFlag)
  {
    this.jcpenneyFlag = jcpenneyFlag;
  }

  public String getJcpenneyFlag()
  {
    return jcpenneyFlag;
  }

  public void setPartnerId(String partnerId)
  {
    this.partnerId = partnerId;
  }

  public String getPartnerId()
  {
    return partnerId;
  }

  public void setPricingCode(String pricingCode)
  {
    this.pricingCode = pricingCode;
  }

  public String getPricingCode()
  {
    return pricingCode;
  }

  public void setShippingCode(String shippingCode)
  {
    this.shippingCode = shippingCode;
  }

  public String getShippingCode()
  {
    return shippingCode;
  }

  public void setSourceCode(String sourceCode)
  {
    this.sourceCode = sourceCode;
  }

  public String getSourceCode()
  {
    return sourceCode;
  }

  public void setAllowFreeShippingFlag(String allowFreeShippingFlag)
  {
    this.allowFreeShippingFlag = allowFreeShippingFlag;
  }

  public String getAllowFreeShippingFlag()
  {
    return allowFreeShippingFlag;
  }
  
  public boolean isAllowFreeShippingFlag()
  {
    return "Y".equalsIgnoreCase(allowFreeShippingFlag);
  }

public String getProgramType() {
	return programType;
}

public void setProgramType(String programType) {
	this.programType = programType;
}

public String getCalculationBasis() {
	return calculationBasis;
}

public void setCalculationBasis(String calculationBasis) {
	this.calculationBasis = calculationBasis;
}

public BigDecimal getPoints() {
	return points;
}

public void setPoints(BigDecimal points) {
	this.points = points;
}

public String getBonusCalculationBasis() {
	return bonusCalculationBasis;
}

public void setBonusCalculationBasis(String bonusCalculationBasis) {
	this.bonusCalculationBasis = bonusCalculationBasis;
}

public BigDecimal getBonusPoints() {
	return bonusPoints;
}

public void setBonusPoints(BigDecimal bonusPoints) {
	this.bonusPoints = bonusPoints;
}

public String getRewardType() {
	return rewardType;
}

public void setRewardType(String rewardType) {
	this.rewardType = rewardType;
}

public void setSameDayUpcharge(String sameDayUpcharge) {
	this.sameDayUpcharge = sameDayUpcharge;
}

public String getSameDayUpcharge() {
	return sameDayUpcharge;
}

public void setDisplaySameDayUpcharge(String displaySameDayUpcharge) {
	this.displaySameDayUpcharge = displaySameDayUpcharge;
}

public String getDisplaySameDayUpcharge() {
	return displaySameDayUpcharge;
}

	/**
	 * @return
	 */
	public String getMorningDeliveryFlag() {
		return morningDeliveryFlag;
	}

	/**
	 * @param morningDeliveryFlag
	 */
	public void setMorningDeliveryFlag(String morningDeliveryFlag) {
		this.morningDeliveryFlag = morningDeliveryFlag;
	}

	/**
	 * @return
	 */
	public String getAllowBBNForFSMemeber() {
		return allowBBNForFSMemeber;
	}

	/**
	 * @param allowBBNForFSMemeber
	 */
	public void setAllowBBNForFSMemeber(String allowBBNForFSMemeber) {
		this.allowBBNForFSMemeber = allowBBNForFSMemeber;
	}

	public String getSameDayUpchargeFS() {
		return sameDayUpchargeFS;
	}

	public void setSameDayUpchargeFS(String sameDayUpchargeFS) {
		this.sameDayUpchargeFS = sameDayUpchargeFS;
	}

}
