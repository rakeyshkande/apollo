package com.ftd.osp.utilities.order.vo;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.lang.reflect.*;
import java.math.BigDecimal;
import java.io.Serializable;

/**
 * Pyaments Value Object that maps to PAYMENTS database table
 * This is a sub collection within Order value object
 *
 * @author Doug Johnson
 */
public class PaymentsVO  extends BaseVO implements Serializable
{
  private String guid;
  private long paymentId;
  private String paymentType;
  private String amount;
  private long creditCardId;
  private String giftCertificateId;
  private String authResult;
  private String authNumber;
  private String avsCode;
  private String acqReferenceNumber;
  private String aafesTicket;
  private String invoiceNumber;
  private String apAccount;
  private String apAuth;
  private String apAccountId;
  private String origAuthAmount;
  
  private List creditCards;
  private String description;
  private String paymentMethodType;
  private String paymentTypeDesc;
  
  private String ncApprovalId;
  private String ncType;
  private String ncReason;
  private String ncOrderReference;
  
  private String origMilesPointsAmt;
  private String milesPointsAmt;
  
  private String cscResponseCode;
  private String cscValidatedFlag;
  private int cscFailureCount;
  


  private String gcCouponStatus;
  private BigDecimal gcCouponIssueAmount;
  private Date gcCouponExpirationDate;
  private BigDecimal gcCouponRemainingAmount;
  private Boolean gcRedeemableStatus;
  private Boolean gcHasRedemptionDetails;

  private String walletIndicator;
  
  private String ccAuthProvider;
  private Map<String,Object> paymentExtMap;
  
  private String cardinalVerifiedFlag;
  
  private String route;
  private String tokenId;
  private String authTransactionId;
  private String settlementTransactionId;
  private String refundTransactionId;
  
  
  private BigDecimal initAuthAmount;
  

  public PaymentsVO()
  {}

  public String getNcApprovalId()
  {
    return ncApprovalId;
  }

  public void setNcApprovalCode(String newNcApprovalId)
  {
    if(valueChanged(ncApprovalId, newNcApprovalId))
    {
        setChanged(true);
    }
    ncApprovalId = newNcApprovalId;
  }

  public String getNcType()
  {
    return ncType;
  }

  public void setNcType(String newNcType)
  {
    if(valueChanged(ncType, newNcType))
    {
        setChanged(true);
    }
    ncType = newNcType;
  }

  public String getNcReason()
  {
    return ncReason;
  }

  public void setNcReason(String newNcReason)
  {
    if(valueChanged(ncReason, newNcReason))
    {
        setChanged(true);
    }
    ncReason = newNcReason;
  }

  public String getNcOrderReference()
  {
    return ncOrderReference;
  }

  public void setNcOrderReference(String newNcOrderReference)
  {
    if(valueChanged(ncOrderReference, newNcOrderReference))
    {
        setChanged(true);
    }
    ncOrderReference = newNcOrderReference;
  }

  public String getGuid()
  {
    return guid;
  }

  public void setGuid(String newGuid)
  {
    if(valueChanged(guid, newGuid))
    {
        setChanged(true);
    }
    guid = newGuid;
  }

  public long getPaymentId()
  {
    return paymentId;
  }

  public void setPaymentId(long newPaymentId)
  {
    if(valueChanged(paymentId, newPaymentId))
    {
        setChanged(true);
    }
    paymentId = newPaymentId;
  }

  public String getPaymentType()
  {
    return paymentType;
  }

  public void setPaymentsType(String newPaymentType)
  {
    if(valueChanged(paymentType, newPaymentType))
    {
        setChanged(true);
    }
    paymentType = newPaymentType;
  }

  public String getAmount()
  {
    return amount;
  }

  public void setAmount(String newAmount)
  {
    if(valueChanged(amount, newAmount))
    {
        setChanged(true);
    }

    amount = checkNullDouble(newAmount);
  }

  public long getCreditCardId()
  {
    return creditCardId;
  }

  public void setCreditCardId(long newCreditCardId)
  {
    if(valueChanged(creditCardId, newCreditCardId))
    {
        setChanged(true);
    }
    creditCardId = newCreditCardId;
  }

  public String getGiftCertificateId()
  {
    return giftCertificateId;
  }

  public void setGiftCertificateId(String newGiftCertificateId)
  {
    if(valueChanged(giftCertificateId, newGiftCertificateId))
    {
        setChanged(true);
    }
    giftCertificateId = newGiftCertificateId;
  }

  public String getAuthResult()
  {
    return authResult;
  }

  public void setAuthResult(String newAuthResult)
  {
    if(valueChanged(authResult, newAuthResult))
    {
        setChanged(true);
    }
    authResult = newAuthResult;
  }

  public String getAuthNumber()
  {
    return authNumber;
  }

  public void setAuthNumber(String newAuthNumber)
  {
    if(valueChanged(authNumber, newAuthNumber))
    {
        setChanged(true);
    }
    authNumber = newAuthNumber;
  }

  public String getAvsCode()
  {
    return avsCode;
  }

  public void setAvsCode(String newAvsCode)
  {
    if(valueChanged(avsCode, newAvsCode))
    {
        setChanged(true);
    }
    avsCode = newAvsCode;
  }

  public String getAcqReferenceNumber()
  {
    return acqReferenceNumber;
  }

  public void setAcqReferenceNumber(String newAcqReferenceNumber)
  {
    if(valueChanged(acqReferenceNumber, newAcqReferenceNumber))
    {
        setChanged(true);
    }

    acqReferenceNumber = newAcqReferenceNumber;
  }

  public String getAafesTicket()
  {
    return aafesTicket;
  }

  public void setAafesTicket(String newAafesTicket)
  {
    if(valueChanged(aafesTicket, newAafesTicket))
    {
        setChanged(true);
    }
    aafesTicket = newAafesTicket;
  }

  public String getInvoiceNumber()
  {
    return invoiceNumber;
  }

  public void setInvoiceNumber(String newInvoiceNumber)
  {
    if(valueChanged(invoiceNumber, newInvoiceNumber))
    {
        setChanged(true);
    }
    invoiceNumber = newInvoiceNumber;
  }

  public List getCreditCards()
  {
    return creditCards;
  }

  public void setCreditCards(List newCreditCards)
  {
    creditCards = newCreditCards;
  }

  public String toString()
    {
        String lineSep = System.getProperty("line.separator");
        StringBuffer sb = new StringBuffer();
        StringBuffer vectorsSb = new StringBuffer();
        // Get this Class
        Class thisClass = this.getClass();
        sb.append("Object Type = ").append(thisClass.getName()).append(lineSep);

        // Use reflection to loop through all the fields
        Field[] fields = thisClass.getDeclaredFields();
        for(int i = 0; i < fields.length; i++)
        {
            try
            {
                if(fields[i].getType().equals(Class.forName("java.lang.String")))
                {
                    sb.append(fields[i].getName()).append(" = ").append(fields[i].get(this)).append(lineSep);
                }
                else if(fields[i].getType().equals(Class.forName("java.util.List")))
                {
                    List vect = (List)fields[i].get(this);
                    if(vect != null)
                    {
                        for (int j = 0; j < vect.size(); j++)
                        {
                            vectorsSb.append(vect.get(j).toString());
                        }
                    }
                }
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        }

        sb.append(vectorsSb);

        return sb.toString();
    }

  public String getDescription()
  {
    return description;
  }

  public void setDescription(String newDescription)
  {
    if(valueChanged(description, newDescription))
    {
        setChanged(true);
    }
    description = newDescription;
  }

  public String getPaymentMethodType()
  {
    return paymentMethodType;
  }

  public void setPaymentMethodType(String newPaymentMethodType)
  {
    if(valueChanged(paymentMethodType, newPaymentMethodType))
    {
        setChanged(true);
    }
    paymentMethodType = newPaymentMethodType;
  }

  public void setApAccount(String newApAccount)
  {
    if(valueChanged(apAccount, newApAccount))
    {
        setChanged(true);
    }
    apAccount = newApAccount;
  }

  public String getApAccount()
  {
    return apAccount;
  }

  public void setApAuth(String newApAuth)
  {
    if(valueChanged(apAuth, newApAuth))
    {
        setChanged(true);
    }
    apAuth = newApAuth;
  }

  public String getApAuth()
  {
    return apAuth;
  }

  public void setOrigAuthAmount(String newOrigAuthAmount)
  {
    if(valueChanged(origAuthAmount, newOrigAuthAmount))
    {
        setChanged(true);
    }
    origAuthAmount = newOrigAuthAmount;
  }

  public String getOrigAuthAmount()
  {
        return origAuthAmount;
  }

  public void setPaymentTypeDesc(String newPaymentTypeDesc)
  {
      if(valueChanged(paymentTypeDesc, newPaymentTypeDesc)) {
          setChanged(true);
      }
      this.paymentTypeDesc = newPaymentTypeDesc;
  }

  public String getPaymentTypeDesc()
  {
    return paymentTypeDesc;
  }

    public void setPaymentType(String newPaymentType) {
        if(valueChanged(paymentType, newPaymentType)) {
            setChanged(true);
        }
        this.paymentType = newPaymentType;
    }

    public void setApAccountId(String newApAccountId) {
        if(valueChanged(apAccountId, newApAccountId)) {
            setChanged(true);
        }
        this.apAccountId = newApAccountId;
    }

    public String getApAccountId() {
        return apAccountId;
    }

  public String getOrigMilesPointsAmt()
  {
    return origMilesPointsAmt;
  }

  public void setOrigMilesPointsAmt(String newOrigMilesPointsAmt)
  {
    if(valueChanged(origMilesPointsAmt, newOrigMilesPointsAmt))
    {
        setChanged(true);
    }
    origMilesPointsAmt = newOrigMilesPointsAmt;
  }

  public String getMilesPointsAmt()
  {
    return milesPointsAmt;
  }

  public void setMilesPointsAmt(String newMilesPointsAmt)
  {
    if(valueChanged(milesPointsAmt, newMilesPointsAmt))
    {
        setChanged(true);
    }
    milesPointsAmt = newMilesPointsAmt;
  }


  public void setCscResponseCode(String newCscResponseCode)
  {
    if(valueChanged(cscResponseCode, newCscResponseCode))
    {
        setChanged(true);
    }
    this.cscResponseCode = newCscResponseCode;
  }

  public String getCscResponseCode()
  {
    return cscResponseCode;
  }

  public void setCscValidatedFlag(String newCscValidatedFlag)
  {
    if(valueChanged(cscValidatedFlag, newCscValidatedFlag))
    {
        setChanged(true);
    }
    this.cscValidatedFlag = newCscValidatedFlag;
  }

  public String getCscValidatedFlag()
  {
    return cscValidatedFlag;
  }

  public void setCscFailureCount(int newCscFailureCount)
  {
    if(valueChanged(cscFailureCount, newCscFailureCount))
    {
        setChanged(true);
    }
    this.cscFailureCount = newCscFailureCount;
  }

  public int getCscFailureCount()
  {
    return cscFailureCount;
  }

public String getGcCouponStatus() {
	return gcCouponStatus;
}

public void setGcCouponStatus(String newGcCouponStatus) {
    if(valueChanged(gcCouponStatus, newGcCouponStatus))
    {
        setChanged(true);
    }
    this.gcCouponStatus = newGcCouponStatus;
}

public BigDecimal getGcCouponIssueAmount() {
	return gcCouponIssueAmount;
}

public void setGcCouponIssueAmount(BigDecimal newGcCouponIssueAmount) {
    if(valueChanged(gcCouponIssueAmount, newGcCouponIssueAmount))
    {
        setChanged(true);
    }
    this.gcCouponIssueAmount = newGcCouponIssueAmount;
}


public Date getGcCouponExpirationDate() {
	return gcCouponExpirationDate;
}

public void setGcCouponExpirationDate(Date newGcCouponExpirationDate) {
    if(valueChanged(gcCouponExpirationDate, newGcCouponExpirationDate))
    {
        setChanged(true);
    }
    this.gcCouponExpirationDate = newGcCouponExpirationDate;
}
  
public Boolean getGcRedeemableStatus() {
	return gcRedeemableStatus;
}

public void setGcRedeemableStatus(Boolean newGcRedeemableStatus) {
	this.gcRedeemableStatus = newGcRedeemableStatus;
}

public BigDecimal getGcCouponRemainingAmount() {
	return gcCouponRemainingAmount;
}

public void setGcCouponRemainingAmount(BigDecimal newGcCouponRemainingAmount) {
	if(valueChanged(gcCouponRemainingAmount, newGcCouponRemainingAmount))
    {
        setChanged(true);
    }
	this.gcCouponRemainingAmount = newGcCouponRemainingAmount;
}

public Boolean getGcHasRedemptionDetails() {
	return gcHasRedemptionDetails;
}

public void setGcHasRedemptionDetails(Boolean gcHasRedemptionDetails) {
	this.gcHasRedemptionDetails = gcHasRedemptionDetails;
}


public void setWalletIndicator(String newWalletIndicator)
{
  if(valueChanged(walletIndicator, newWalletIndicator))
  {
      setChanged(true);
  }
  this.walletIndicator = newWalletIndicator;
}

public String getWalletIndicator()
{
  return walletIndicator;
}  

public String getCcAuthProvider() {
	return ccAuthProvider;
}

public void setCcAuthProvider(String ccAuthProvider) {
	this.ccAuthProvider = ccAuthProvider;
}

public String getCardinalVerifiedFlag() {
	return cardinalVerifiedFlag;
}

public void setCardinalVerifiedFlag(String cardinalVerifiedFlag) {
	this.cardinalVerifiedFlag = cardinalVerifiedFlag;
}

public String getRoute() {
	return route;
}

public void setRoute(String route) {
	this.route = route;
}

public Map<String, Object> getPaymentExtMap() {
	if(paymentExtMap == null) {
		paymentExtMap = new HashMap<String, Object>();
	}
	return paymentExtMap;	
}

public String getTokenId() {
    return tokenId;
}

public void setTokenId(String tokenId) {
    this.tokenId = tokenId;
}

public String getAuthTransactionId() {
    return authTransactionId;
}

public void setAuthTransactionId(String authTransactionId) {
    this.authTransactionId = authTransactionId;
}

public String getSettlementTransactionId() {
	return settlementTransactionId;
}

public void setSettlementTransactionId(String settlementTransactionId) {
	this.settlementTransactionId = settlementTransactionId;
}

public String getRefundTransactionId() {
	return refundTransactionId;
}

public void setRefundTransactionId(String refundTransactionId) {
	this.refundTransactionId = refundTransactionId;
}

public BigDecimal getInitAuthAmount() {
	return initAuthAmount;
}

public void setInitAuthAmount(BigDecimal initAuthAmount) {
	this.initAuthAmount = initAuthAmount;
}



}
