package com.ftd.osp.utilities.order.vo;

import org.w3c.dom.Document;
import java.util.List;
import java.lang.reflect.*;
import java.io.Serializable;

import java.math.BigDecimal;

public class PaymentMethodMilesPointsVO implements Serializable  
{

  private String paymentMethodId;
  private String dollarToMpOperator;
  private String roundingMethodId;
  private String roundingScaleQty;
  private String commissionPct;

  public PaymentMethodMilesPointsVO()
  {}


    public void setPaymentMethodId(String _paymentMethodId) {
        this.paymentMethodId = _paymentMethodId;
    }

    public String getPaymentMethodId() {
        return paymentMethodId;
    }

    public void setDollarToMpOperator(String _dollarToMpOperator) {
        this.dollarToMpOperator = _dollarToMpOperator;
    }

    public String getDollarToMpOperator() {
        return dollarToMpOperator;
    }

    public void setRoundingMethodId(String _roundingMethodId) {
        this.roundingMethodId = _roundingMethodId;
    }

    public String getRoundingMethodId() {
        return roundingMethodId;
    }

    public void setRoundingScaleQty(String _roundingScaleQty) {
        this.roundingScaleQty = _roundingScaleQty;
    }

    public String getRoundingScaleQty() {
        return roundingScaleQty;
    }

    public void setCommissionPct(String _commissionPct) {
        this.commissionPct = _commissionPct;
    }

    public String getCommissionPct() {
        return commissionPct;
    }
}

