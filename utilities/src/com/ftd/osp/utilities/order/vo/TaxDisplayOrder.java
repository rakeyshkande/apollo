/**
 * 
 */
package com.ftd.osp.utilities.order.vo;

import com.ftd.osp.utilities.plugins.Logger;


/**
 * @author smeka
 *
 */
public enum TaxDisplayOrder {

	STATETAX(0), 
	COUNTYTAX(0), 
	CITYTAX(0), 
	ZIPTAX(0), 
	STATE(0), 
	COUNTY(0), 
	CITY(0), 
	ZIP(0),
	SURCHARGE(1),
	GSTHST(1), 
	GSTHSTTAX(1), // global param description when tax service is down
	PST(2), 
	QST(2),
	TAXES(1);


	int displayOrder;

	TaxDisplayOrder(int displayOrder) {
		this.displayOrder = displayOrder;
	}

	public int value() {
		return displayOrder;
	}
	
	private static Logger logger = new Logger(TaxDisplayOrder.class.getName());
	
	public static TaxDisplayOrder fromValue(String taxType) {
		taxType = taxType.replaceAll("[^\\w\\s-]", "");
		taxType = taxType.replaceAll("\\s", "");
		
		for (TaxDisplayOrder c : TaxDisplayOrder.values()) {		
			
			if(c.name().equalsIgnoreCase(taxType)) {				
				return c;
			} 			
		}
		
		logger.error("Tax Type is invalid return default display order"); 		
		return STATETAX;
	}
	
}
