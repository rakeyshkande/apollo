package com.ftd.osp.utilities.order.vo;

import java.io.Serializable;

/**
 * Fraud Comments Value Object that maps to FRAUD_COMMENTS database table
 *
 * @author Ed Mueller
 */
public class FraudCommentsVO extends BaseVO implements Serializable  
{
  private String GUID;
  private long FraudCommentID;
  private String CommentText;
  private String FraudID;
  private String FraudDescription;

  public FraudCommentsVO()
  {
  }

  public String getGUID()
  {
    return GUID;
  }

  public void setGUID(String newGUID)
  {
    if(valueChanged(GUID, newGUID))
    {
        setChanged(true);
    }    
    GUID = newGUID;
  }

  public long getFraudCommentID()
  {
    return FraudCommentID;
  }

  public void setFraudCommentID(long newCommentID)
  {
    if(valueChanged(FraudCommentID, newCommentID))
    {
        setChanged(true);
    }  
    FraudCommentID = newCommentID;
  }

  public String getCommentText()
  {
    return CommentText;
  }

  public void setCommentText(String newCommentText)
  {
    if(valueChanged(CommentText, newCommentText))
    {
        setChanged(true);
    }  
    CommentText = newCommentText;
  }

  public String getFraudID()
  {
    return FraudID;
  }

  public void setFraudID(String newFraudID)
  {
    if(valueChanged(FraudID, newFraudID))
    {
        setChanged(true);
    }  
    FraudID = newFraudID;
  }

  public String getFraudDescription()
  {
    return FraudDescription;
  }

  public void setFraudDescription(String newFraudDescription)
  {
    if(valueChanged(FraudDescription, newFraudDescription))
    {
        setChanged(true);
    }  
    FraudDescription = newFraudDescription;
  }
}