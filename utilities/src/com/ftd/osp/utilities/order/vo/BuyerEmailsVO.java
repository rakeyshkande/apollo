package com.ftd.osp.utilities.order.vo;

import java.io.Serializable;

/**
 * Buyer Emails Value Object that maps to BUYER_EMAILS database table
 * This is a sub collection within Buyer value object
 *
 * @author Doug Johnson
 */
public class BuyerEmailsVO extends BaseVO implements Serializable
{
  private long buyerEmailId;
  private long buyerId;
  private String email;
  private String primary;
  private String newsletter;

  public BuyerEmailsVO()
  {
  }

  public long getBuyerEmailId()
  {
    return buyerEmailId;
  }

  public void setBuyerEmailId(long newBuyerEmailId)
  {
    if(valueChanged(buyerEmailId, newBuyerEmailId))
    {
        setChanged(true);
    }
    buyerEmailId = newBuyerEmailId;
  }

  public long getBuyerId()
  {
    return buyerId;
  }

  public void setBuyerId(long newBuyerId)
  {
    if(valueChanged(buyerId, newBuyerId))
    {
        setChanged(true);
    }
    buyerId = newBuyerId;
  }

  public String getEmail()
  {
    return email;
  }

  public void setEmail(String newEmail)
  {
    if(valueChanged(email, newEmail))
    {
        setChanged(true);
    }
    email = newEmail;
  }

  public String getPrimary()
  {
    return primary;
  }

  public void setPrimary(String newPrimary)
  {
    if(valueChanged(primary, newPrimary))
    {
        setChanged(true);
    }
    primary = newPrimary;
  }

  public String getNewsletter()
  {
    return newsletter;
  }

  public void setNewsletter(String newNewsletter)
  {
    if(valueChanged(newsletter, newNewsletter))
    {
        setChanged(true);
    }
    newsletter = newNewsletter;
  }
}