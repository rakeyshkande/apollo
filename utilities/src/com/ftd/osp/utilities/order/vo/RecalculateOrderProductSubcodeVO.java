package com.ftd.osp.utilities.order.vo;

/**
 * Provides Subcode information used for Recalculation logic.
 */
public class RecalculateOrderProductSubcodeVO
{
  String productSubcodeId;
  String productId;
  double subcodePrice;
  

  public RecalculateOrderProductSubcodeVO()
  {
  }

  public void setProductSubcodeId(String productSubcodeId)
  {
    this.productSubcodeId = productSubcodeId;
  }

  public String getProductSubcodeId()
  {
    return productSubcodeId;
  }

  public void setProductId(String productId)
  {
    this.productId = productId;
  }

  public String getProductId()
  {
    return productId;
  }

  public void setSubcodePrice(double subcodePrice)
  {
    this.subcodePrice = subcodePrice;
  }

  public double getSubcodePrice()
  {
    return subcodePrice;
  }
}
