package com.ftd.osp.utilities.order.vo;

import java.util.List;
import java.lang.reflect.*;
import java.io.Serializable;

/**
 * Co Brand Value Object that maps to CO_BRANDS database table
 * This is a sub collection within Order value object
 *
 * @author Doug Johnson
 */
public class CoBrandVO extends BaseVO implements Serializable  
{
  private long coBrandId;
  private String guid;
  private String infoName;
  private String infoData;
  
  public CoBrandVO()
  {}

  public long getCoBrandId()
  {
    return coBrandId;
  }

  public void setCoBrandId(long newCoBrandId)
  {
    if(valueChanged(coBrandId, newCoBrandId))
    {
        setChanged(true);
    }
    coBrandId = newCoBrandId;
  }

  public String getGuid()
  {
    return guid;
  }

  public void setGuid(String newGuid)
  {
    if(valueChanged(guid, newGuid))
    {
        setChanged(true);
    }
    guid = newGuid;
  }
  
  public String getInfoName()
  {
    return infoName;
  }

  public void setInfoName(String newInfoName)
  {
    if(valueChanged(infoName, newInfoName))
    {
        setChanged(true);
    }
    infoName = newInfoName;
  }

  public String getInfoData()
  {
    return infoData;
  }

  public void setInfoData(String newInfoData)
  {
    if(valueChanged(infoData, newInfoData))
    {
        setChanged(true);
    }
    infoData = newInfoData;
  }

    public String toString()
    {
        String lineSep = System.getProperty("line.separator");
        StringBuffer sb = new StringBuffer();
        StringBuffer vectorsSb = new StringBuffer();
        // Get this Class
        Class thisClass = this.getClass();
        sb.append("Object Type = ").append(thisClass.getName()).append(lineSep);
        
        // Use reflection to loop through all the fields
        Field[] fields = thisClass.getDeclaredFields();
        for(int i = 0; i < fields.length; i++)
        {
            try
            {
                if(fields[i].getType().equals(Class.forName("java.lang.String")))
                {
                    sb.append(fields[i].getName()).append(" = ").append(fields[i].get(this)).append(lineSep);
                }
                else if(fields[i].getType().equals(Class.forName("java.util.List")))
                {
                    List vect = (List)fields[i].get(this);
                    if(vect != null)
                    {
                        for (int j = 0; j < vect.size(); j++) 
                        {
                            vectorsSb.append(vect.get(j).toString());
                        }
                    }
                }
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        }

        sb.append(vectorsSb);
        
        return sb.toString();
    }
}