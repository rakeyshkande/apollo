package com.ftd.osp.utilities.order.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/** Base VO
 * @author Ed Mueller */
public class BaseVO implements Serializable
{

    private static final String NULLDOUBLE = "0.0";
    private static final String NULLFLOAT = "0.0";
    private static final String NULLINT = "0";
    private static final String NULLLONG = "0";    

    private boolean changed = false;


  /* Returns 0.0 if the passed in value is null 
   * @param String value 
   * @return String */
  protected String checkNullDouble(String value)
  {
    return value == null ? NULLDOUBLE : value;
  }

  /* Returns 0 if the passed in value is null 
   * @param String value 
   * @return String */
  protected String checkNullInt(String value)
  {
    return value == null ? NULLINT : value;
  }

  /* Returns 0 if the passed in value is null 
   * @param String value 
   * @return String */
  protected String checkNullFloat(String value)
  {
    return value == null ? NULLFLOAT : value;
  }

  /* Returns 0 if the passed in value is null 
   * @param String value 
   * @return String */
  protected String checkNullLong(String value)
  {
    return value == null ? NULLLONG : value;
  }

    public boolean isChanged()
    {
        return changed;
    }

    protected void setChanged(boolean newChanged)
    {
        changed = newChanged;
    }

    protected boolean valueChanged(String origValue, String newValue)
    {
        boolean ret = false;
        if((newValue == null && origValue == null) || (newValue == null && origValue.equals("")) ||
           (origValue == null && newValue.equals("")))
        {
            // Do nothing
            ret = false;
        }
        else if(newValue != null && origValue != null && newValue.equals(origValue))
        {
            // Do nothing
            ret = false;
        }
        else
        {
            ret = true;
        }        

        return ret;
    }


    protected boolean valueChanged(long origValue, long newValue)
    {
        boolean ret = false;
        if(newValue == origValue)
        {
            // Do nothing
            ret = false;
        }
        else
        {
            //changed
            ret = true;
        }
     

        return ret;
    }
    
    protected boolean valueChanged(BigDecimal origValue, BigDecimal newValue)
    {
        boolean ret = false;
        if(newValue == origValue)
        {
            // Do nothing
            ret = false;
        }
        else
        {
            //changed
            ret = true;
        }
     

        return ret;
    }
    
    protected boolean valueChanged(Date origValue, Date newValue)
    {
        boolean ret = false;
        if(newValue == origValue)
        {
            // Do nothing
            ret = false;
        }
        else
        {
            //changed
            ret = true;
        }
     

        return ret;
    }

}