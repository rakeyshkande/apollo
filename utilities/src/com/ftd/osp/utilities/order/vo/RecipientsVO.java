package com.ftd.osp.utilities.order.vo;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.List;

/**
 * Recipient Value Object that maps to RECIPIENT database table
 * This is a sub collection within Order Details value object
 *
 * @author Doug Johnson
 */
public class RecipientsVO extends BaseVO implements Serializable  
{
  private long recipientId;
  private String lastName;
  private String firstName;
  private String listCode;
  private String autoHold;
  private String mailListCode;
  
  private List recipientPhones;
  private List recipientComments;
  private List recipientAddresses;
  private List destinations;
  private String status;
  private String customerId;

  public RecipientsVO()
  {
  }

  public long getRecipientId()
  {
    return recipientId;
  }

  public void setRecipientId(long newRecipientId)
  {
    if(valueChanged(recipientId,newRecipientId)){
      setChanged(true);
    }
    recipientId = newRecipientId;
  }

  public String getLastName()
  {
    return lastName;
  }

  public void setLastName(String newLastName)
  {
    if(valueChanged(lastName,newLastName)){
      setChanged(true);
    }  
    lastName = newLastName;
  }

  public String getFirstName()
  {
    return firstName;
  }

  public void setFirstName(String newFirstName)
  {
    if(valueChanged(firstName,newFirstName)){
      setChanged(true);
    }  
    firstName = newFirstName;
  }

  public String getListCode()
  {
    return listCode;
  }

  public void setListCode(String newListCode)
  {
    if(valueChanged(listCode,newListCode)){
      setChanged(true);
    }  
    listCode = newListCode;
  }

  public String getAutoHold()
  {
    return autoHold;
  }

  public void setAutoHold(String newAutoHold)
  {
    if(valueChanged(autoHold,newAutoHold)){
      setChanged(true);
    }  
    autoHold = newAutoHold;
  }

  public String getMailListCode()
  {
    return mailListCode;
  }

  public void setMailListCode(String newMailListCode)
  {
    if(valueChanged(mailListCode,newMailListCode)){
      setChanged(true);
    }  
    mailListCode = newMailListCode;
  }

  public List getRecipientPhones()
  {
    return recipientPhones;
  }

  public void setRecipientPhones(List newRecipientPhones)
  {

    recipientPhones = newRecipientPhones;
  }

  public List getRecipientComments()
  {
    return recipientComments;
  }

  public void setRecipientComments(List newRecipientComments)
  {

    recipientComments = newRecipientComments;
  }

  public List getRecipientAddresses()
  {
    return recipientAddresses;
  }

  public void setRecipientAddresses(List newRecipientAddresses)
  {

    recipientAddresses = newRecipientAddresses;
  }

  public List getDestinations()
  {
    return destinations;
  }

  public void setDestinations(List newDestinations)
  {

    destinations = newDestinations;
  }

  public String toString()
  {
    String lineSep = System.getProperty("line.separator");
    StringBuffer sb = new StringBuffer();
    StringBuffer listSb = new StringBuffer();
    // Get this Class
    Class thisClass = this.getClass();
    sb.append("Object Type = ").append(thisClass.getName()).append(lineSep);
        
    // Use reflection to loop through all the fields
    Field[] fields = thisClass.getDeclaredFields();
    for(int i = 0; i < fields.length; i++)
    {
        try
        {
            if(fields[i].getType().equals(Class.forName("java.lang.String")))
            {
                sb.append(fields[i].getName()).append(" = ").append(fields[i].get(this)).append(lineSep);
            }
            else if(fields[i].getType().equals(Class.forName("java.util.List")))
            {
                List list = (List)fields[i].get(this);
                if(list != null)
                {
                    for (int j = 0; j < list.size(); j++) 
                    {
                        listSb.append(list.get(j).toString());
                    }
                }
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    sb.append(listSb);
        
    return sb.toString();
  }

  public String getStatus()
  {
    return status;
  }

  public void setStatus(String newStatus)
  {
    if(valueChanged(status,newStatus)){
      setChanged(true);
    }  
    status = newStatus;
  }

  public String getCustomerId()
  {
    return customerId;
  }

  public void setCustomerId(String newCustomerId)
  {
    if(valueChanged(customerId,newCustomerId)){
      setChanged(true);
    }  
    customerId = newCustomerId;
  }
}