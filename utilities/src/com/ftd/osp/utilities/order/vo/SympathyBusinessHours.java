package com.ftd.osp.utilities.order.vo;

public class SympathyBusinessHours {
	
	private String boHrsMonFriStart;
	private String boHrsMonFriEnd;
	private String BoHrsSatStart;
	private String BoHrsSatEnd;
	private String BoHrsSunStart;
	private String BoHrsSunEnd;
	
	public String getBoHrsMonFriStart() {
		return boHrsMonFriStart;
	}
	public void setBoHrsMonFriStart(String boHrsMonFriStart) {
		this.boHrsMonFriStart = boHrsMonFriStart;
	}
	public String getBoHrsMonFriEnd() {
		return boHrsMonFriEnd;
	}
	public void setBoHrsMonFriEnd(String boHrsMonFriEnd) {
		this.boHrsMonFriEnd = boHrsMonFriEnd;
	}
	public String getBoHrsSatStart() {
		return BoHrsSatStart;
	}
	public void setBoHrsSatStart(String boHrsSatStart) {
		BoHrsSatStart = boHrsSatStart;
	}
	public String getBoHrsSatEnd() {
		return BoHrsSatEnd;
	}
	public void setBoHrsSatEnd(String boHrsSatEnd) {
		BoHrsSatEnd = boHrsSatEnd;
	}
	public String getBoHrsSunStart() {
		return BoHrsSunStart;
	}
	public void setBoHrsSunStart(String boHrsSunStart) {
		BoHrsSunStart = boHrsSunStart;
	}
	public String getBoHrsSunEnd() {
		return BoHrsSunEnd;
	}
	public void setBoHrsSunEnd(String boHrsSunEnd) {
		BoHrsSunEnd = boHrsSunEnd;
	}

}
