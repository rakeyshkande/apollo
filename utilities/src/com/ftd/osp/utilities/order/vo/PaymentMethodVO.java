package com.ftd.osp.utilities.order.vo;

import org.w3c.dom.Document;
import java.util.List;
import java.lang.reflect.*;
import java.io.Serializable;

import java.math.BigDecimal;

public class PaymentMethodVO implements Serializable  
{

  private String paymentMethodId;
  private String paymentMethodType;
  private String paymentTypeDescription;
  private BigDecimal overAuthPct;
  private BigDecimal overAuthAmt;

  public PaymentMethodVO()
  {}


    public void setPaymentMethodId(String paymentMethodId) {
        this.paymentMethodId = paymentMethodId;
    }

    public String getPaymentMethodId() {
        return paymentMethodId;
    }

    public void setPaymentMethodType(String paymentMethodType) {
        this.paymentMethodType = paymentMethodType;
    }

    public String getPaymentMethodType() {
        return paymentMethodType;
    }

    public void setOverAuthPct(BigDecimal overAuthPct) {
        this.overAuthPct = overAuthPct;
    }

    public BigDecimal getOverAuthPct() {
        return overAuthPct;
    }

    public void setOverAuthAmt(BigDecimal overAuthAmt) {
        this.overAuthAmt = overAuthAmt;
    }

    public BigDecimal getOverAuthAmt() {
        return overAuthAmt;
    }

    public void setPaymentTypeDescription(String paymentTypeDescription) {
        this.paymentTypeDescription = paymentTypeDescription;
    }

    public String getPaymentTypeDescription() {
        return paymentTypeDescription;
    }
}

