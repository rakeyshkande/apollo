package com.ftd.osp.utilities.order.vo;

/**
 * AddressType represents values from FRP.ADDRESS_TYPES
 * 
 * @author Mark Moon, Software Architects Inc.
 * @version $Revision: 1.3 $
 */
public class AddressTypeVO 
    extends BaseVO {

  private String code;
  private String description;
  private String type;
  private boolean promptForBusiness = false;
  private boolean  promptForRoom = false;
  private String roomLabelTxt = "";	
  private boolean promptForHours = false;
  private String hoursLabelTxt = "";

  
  /**
   * Default constructor.
   */
  public AddressTypeVO(){
    super();
  }


  public void setCode(String code) {
    if( valueChanged(this.code, code) )
      setChanged( true );
    this.code = code;
  }

  public String getCode() {
    return code;
  }


  public void setDescription(String description) {
    if( valueChanged(this.description, description) )
      setChanged( true );
    this.description = description;
  }

  public String getDescription() {
    return description;
  }


  public void setType(String type) {
    if( valueChanged(this.type, type) )
      setChanged( true );
    this.type = type;
  }

  public String getType() {
    return type;
  }

  public void setPromptForBusiness(boolean promptForBusiness)
  {
    this.promptForBusiness = promptForBusiness;
  }

  public boolean isPromptForBusiness()
  {
    return promptForBusiness;
  }

  public void setPromptForRoom(boolean promptForRoom)
  {
    this.promptForRoom = promptForRoom;
  }

  public boolean isPromptForRoom()
  {
    return promptForRoom;
  }

  public void setRoomLabelTxt(String roomLabelTxt)
  {
    this.roomLabelTxt = roomLabelTxt;
  }

  public String getRoomLabelTxt()
  {
    return roomLabelTxt;
  }

  public void setPromptForHours(boolean promptForHours)
  {
    this.promptForHours = promptForHours;
  }

  public boolean isPromptForHours()
  {
    return promptForHours;
  }

  public void setHoursLabelTxt(String hoursLabelTxt)
  {
    this.hoursLabelTxt = hoursLabelTxt;
  }

  public String getHoursLabelTxt()
  {
    return hoursLabelTxt;
  }
}
