package com.ftd.osp.utilities.order.vo;

/**
 * Constants for order status's 
 *
 * @author Doug Johnson
 */
public interface OrderStatus 
{
  public final static int RECEIVED = 1001;
  public final static int INVALID_HEADER = 1002;
  public final static int VALID_HEADER_INVALID_ITEM = 1003;
  public final static int VALID = 1004;
  public final static int SCRUB = 1005;
  public final static int REMOVED = 1006;
  public final static int PENDING = 1007;
  public final static int COMPLETE = 1008;
  public final static int BULK = 1009;
  public final static int ABANDONED_HEADER = 1010;
  public final static int REINSTATE = 1013;
  public final static int RECEIVED_ITEM = 2001;
  public final static int INVALID_ITEM = 2002;
  public final static int VALID_ITEM_INVALID_HEADER = 2003;
  public final static int REMOVED_ITEM = 2004;
  public final static int PENDING_ITEM = 2005;
  public final static int PROCESSED_ITEM = 2006;
  public final static int VALID_ITEM = 2007;
  public final static int PROCESSED_REMOVED = 2008;
  public final static int PROCESSED_PENDING = 2009;
  public final static int BULK_ITEM = 2010;
  public final static int ABANDONED_ITEM = 2011;
}