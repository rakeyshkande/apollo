package com.ftd.osp.utilities.order.vo;

import java.util.List;
import java.lang.reflect.*;
import java.io.Serializable;

public class MembershipsVO  extends BaseVO implements Serializable  
{
  private long membershipId;
  private long buyerId;
  private String membershipType;
  private String firstName;
  private String lastName;
  private String membershipIdNumber;
 
  public MembershipsVO()
  {}

  public long getMembershipId()
  {
    return membershipId;
  }

  public void setMembershipId(long newMembershipId)
  {
    if(valueChanged(membershipId, newMembershipId))
    {
        setChanged(true);
    }
    membershipId = newMembershipId;
  }

  public long getBuyerId()
  {
    return buyerId;
  }

  public void setBuyerId(long newBuyerId)
  {
    if(valueChanged(buyerId, newBuyerId))
    {
        setChanged(true);
    }
    buyerId = newBuyerId;
  }

  public String getMembershipType()
  {
    return membershipType;
  }

  public void setMembershipType(String newMembershipType)
  {
    if(valueChanged(membershipType, newMembershipType))
    {
        setChanged(true);
    }
    membershipType = newMembershipType;
  }
  
  public String getFirstName()
  {
    return firstName;
  }

  public void setFirstName(String newFirstName)
  {
    if(valueChanged(firstName, newFirstName))
    {
        setChanged(true);
    }
    firstName = newFirstName;
  }

  public String getLastName()
  {
    return lastName;
  }

  public void setLastName(String newLastName)
  {
    if(valueChanged(lastName, newLastName))
    {
        setChanged(true);
    }
    lastName = newLastName;
  }

  public String getMembershipIdNumber()
  {
    return membershipIdNumber;
  }

  public void setMembershipIdNumber(String newMembershipIdNumber)
  {
    if(valueChanged(membershipIdNumber, newMembershipIdNumber))
    {
        setChanged(true);
    }
    membershipIdNumber = checkNullInt(newMembershipIdNumber);
  }
  
    public String toString()
    {
        String lineSep = System.getProperty("line.separator");
        StringBuffer sb = new StringBuffer();
        StringBuffer vectorsSb = new StringBuffer();
        // Get this Class
        Class thisClass = this.getClass();
        sb.append("Object Type = ").append(thisClass.getName()).append(lineSep);
        
        // Use reflection to loop through all the fields
        Field[] fields = thisClass.getDeclaredFields();
        for(int i = 0; i < fields.length; i++)
        {
            try
            {
                if(fields[i].getType().equals(Class.forName("java.lang.String")))
                {
                    sb.append(fields[i].getName()).append(" = ").append(fields[i].get(this)).append(lineSep);
                }
                else if(fields[i].getType().equals(Class.forName("java.util.List")))
                {
                    List vect = (List)fields[i].get(this);
                    if(vect != null)
                    {
                        for (int j = 0; j < vect.size(); j++) 
                        {
                            vectorsSb.append(vect.get(j).toString());
                        }
                    }
                }
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        }

        sb.append(vectorsSb);
        
        return sb.toString();
    }}