package com.ftd.osp.utilities.order;


import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.order.vo.AddOnsVO;
import com.ftd.osp.utilities.order.vo.BuyerAddressesVO;
import com.ftd.osp.utilities.order.vo.BuyerCommentsVO;
import com.ftd.osp.utilities.order.vo.BuyerEmailsVO;
import com.ftd.osp.utilities.order.vo.BuyerPhonesVO;
import com.ftd.osp.utilities.order.vo.BuyerVO;
import com.ftd.osp.utilities.order.vo.CoBrandVO;
import com.ftd.osp.utilities.order.vo.CreditCardsVO;
import com.ftd.osp.utilities.order.vo.DispositionsVO;
import com.ftd.osp.utilities.order.vo.FraudCommentsVO;
import com.ftd.osp.utilities.order.vo.MembershipsVO;
import com.ftd.osp.utilities.order.vo.OrderContactInfoVO;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.order.vo.PaymentsVO;
import com.ftd.osp.utilities.order.vo.RecipientAddressesVO;
import com.ftd.osp.utilities.order.vo.RecipientCommentsVO;
import com.ftd.osp.utilities.order.vo.RecipientPhonesVO;
import com.ftd.osp.utilities.order.vo.RecipientsVO;
import com.ftd.osp.utilities.plugins.Logger;

import java.math.BigDecimal;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Timestamp;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Data access class for FRP schema 
 *
 * @author Doug Johnson
 * 
 * This class is used to insert, update, and retrieve orders from the database.
 */
public class FRPMapperDAO implements OrderDataMapper
{

    private Connection connection;
    private Logger logger;




    private static final String GUID_KEY = "GUID";

    //procedures
    private static final String VIEW_ORDER_INFO = "FRP.VIEW_ORDER_INFO";
    private static final String UPDATE_ORDER_HEADER = "FRP.UPDATE_ORDER_HEADER";
    private static final String UPDATE_ORDER_DETAILS = "FRP.UPDATE_ORDER_DETAILS";
    private static final String UPDATE_ADD_ONS = "FRP.UPDATE_ADD_ONS";
    private static final String INSERT_PAYMENTS = "FRP.INSERT_PAYMENTS";
    private static final String UPDATE_CREDICT_CARDS = "FRP.UPDATE_CREDICT_CARDS";
    private static final String UPDATE_CO_BRAND = "FRP.UPDATE_CO_BRAND";
    private static final String UPDATE_BUYERS = "FRP.UPDATE_BUYERS";
    private static final String UPDATE_BUYER_ADDRESSES = "FRP.UPDATE_BUYER_ADDRESSES";
    private static final String UPDATE_BUYER_PHONES = "FRP.UPDATE_BUYER_PHONES";
    private static final String UPDATE_BUYER_EMAILS = "FRP.UPDATE_BUYER_EMAILS";
    private static final String INSERT_ORDER_DISPOSITION = "FRP.INSERT_ORDER_DISPOSITION";
    private static final String INACTIVATE_PAYMENTS = "FRP.INACTIVATE_PAYMENTS";
    private static final String UPDATE_DESTINATIONS = "FRP.UPDATE_DESTINATIONS";
    private static final String UPDATE_MEMBERSHIPS = "FRP.UPDATE_MEMBERSHIPS";
    private static final String UPDATE_NCOA = "FRP.UPDATE_NCOA";

    private static final String DELETE_ADD_ONS = "FRP.DELETE_ADD_ONS";

    //insert procs
    private static final String INSERT_ORDER_HEADER = "FRP.INSERT_ORDER_HEADER";
    private static final String INSERT_ORDER_DETAILS = "FRP.INSERT_ORDER_DETAILS";

    private static final String INSERT_UNIQUE_BUYER = "FRP.INSERT_UNIQUE_BUYER";
    private static final String INSERT_UNIQUE_BUYER_PHONES = "FRP.INSERT_UNIQUE_BUYER_PHONES";

    private static final String INSERT_FRAUD_COMMENTS = "FRP.INSERT_FRAUD_COMMENTS";

    private static final String INSERT_UNIQUE_CONTACT = "FRP.INSERT_UNIQUE_CONTACT";
    private static final String UPDATE_UNIQUE_ORDER_DETAILS = "FRP.UPDATE_UNIQUE_ORDER_DETAILS";
    private static final String UPDATE_UNIQUE_CO_BRAND = "FRP.UPDATE_UNIQUE_CO_BRAND";
    private static final String INSERT_UNIQUE_ADD_ONS = "FRP.UPDATE_UNIQUE_ADD_ONS";
    private static final String UPDATE_UNIQUE_CREDIT_CARDS = "FRP.UPDATE_UNIQUE_CREDIT_CARDS";
    private static final String UPDATE_UNIQUE_PAYMENTS = "FRP.UPDATE_UNIQUE_PAYMENTS";

    private static final String INSERT_UNIQUE_RECIPIENT = "FRP.INSERT_UNIQUE_RECIPIENT";
    private static final String UPDATE_UNIQUE_RECIPIENT_PHONES = "FRP.UPDATE_UNIQUE_RECIPIENT_PHONES";
    private static final String UPDATE_UNIQUE_BUYER_PHONES = "FRP.UPDATE_UNIQUE_BUYER_PHONES";
    private static final String INSERT_RECIPIENT = "FRP.INSERT_RECIPIENTS";
    private static final String INSERT_RECIPIENT_PHONES = "FRP.INSERT_RECIPIENT_PHONES";
    private static final String INSERT_RECIPIENT_ADDRESSES = "FRP.INSERT_RECIPIENT_ADDRESSES";
    private static final String INSERT_DESTINATIONS = "FRP.INSERT_DESTINATIONS";    
    private static final String INSERT_MEMBERSHIPS = "FRP.INSERT_MEMBERSHIPS";    
    private static final String INSERT_NCOA = "FRP.INSERT_NCOA";    
    //Order ref cursors returned from database procedure
    private static final String ORDER_HEADER_CURSOR = "RegisterOutParameterOrdercur";
    private static final String ORDER_ITEM_CURSOR = "RegisterOutParameterOrderItemcur";
    private static final String ORDER_ADD_ONS = "RegisterOutParameterAddOncur";
    private static final String ORDER_CO_BRANDS = "RegisterOutParameterCoBrandcur";
    private static final String ORDER_PAYMENTS = "RegisterOutParameterPaymentcur";
    private static final String ORDER_CREDIT_CARDS = "RegisterOutParameterCreditCardcur";
    private static final String BUYER = "RegisterOutParameterBuyercur";
    private static final String BUYER_PHONES = "RegisterOutParameterBuyerPhonecur";
    private static final String BUYER_COMMENTS = "RegisterOutParameterBuyerCommentscur";
    private static final String BUYER_ADDRESSES = "RegisterOutParameterBuyerAddrcur";
    private static final String BUYER_EMAILS = "RegisterOutParameterBuyerEmailcur";
    private static final String RECIPIENT = "RegisterOutParameterRecipeintscur";
    private static final String RECIPIENT_ADDRESSES = "RegisterOutParameterRecipeintsAddrcur";
    private static final String RECIPIENT_DESTINATIONS = "RegisterOutParameterDestinationscur";
    private static final String RECIPIENT_COMMENTS = "RegisterOutParameterRecipeintsCommentscur";
    private static final String RECIPIENT_PHONES = "RegisterOutParameterRecipeintsPhonecur";
    private static final String ORDER_MEMBERSHIPS = "RegisterOutParameterMembershipscur";   
    private static final String NCOA = "RegisterOutParameterNcoacur";   
    private static final String ORDER_CONTACTS = "RegisterOutParameterOrdercontactcur";   
    private static final String ORDER_DISPOSITIONS =  "RegisterOutParameterDispositioncur";
    private static final String FRAUD_COMMENTS = "RegisterOutParameterFraudCommentscur";
    
    //custom parameters returned from database procedure
    private static final String STATUS_PARAM = "RegisterOutParameterStatus";
    private static final String MESSAGE_PARAM = "RegisterOutParameterMessage";
    private static final String BUYER_COMMENT_ID_PARM = "IO_BUYER_COMMENT_ID";
    private static final String RECIPIENT_COMMENT_ID_PARM = "IO_RECIPIENT_COMMENT_ID";
    
    private static final String INSERT_BUYER_COMMENTS = "FRP.INSERT_BUYER_COMMENTS";
    private static final String INSERT_RECIPIENT_COMMENTS = "FRP.INSERT_RECIPIENT_COMMENTS";

    private String status;
    private String message;
    private boolean returnStatus;

    private String userID;
    private final String DEFAULT_USER_ID = "Unknown";

  /** Constructor.
   *  Takes in a connection. */
  public FRPMapperDAO(Connection connection)
  {
      this.connection = connection;
      this.logger = new Logger("com.ftd.osp.utilities.order.FRPMapperDAO");
      this.returnStatus = true;


  }

  /**
   * This method inserts an order into the FRP
   * schema.
   * 
   * Notes:
   * The passed in ORDER_GUID and the passed in MASTER_ORDER_NUMBER
   * should not already exist in the database.  Exceptions are thrown
   * if any database rules are violated.
   * 
   * All ID fields in the value objects will be updated.  The database
   * generated IDs will be placed in these fields.
   * 
   * @param OrderVO order
   * @return true/false success of insert
   * @throws Exception
   */
  public boolean mapOrderToDB(OrderVO order) throws Exception
  {
    boolean success = true;
    long time = 0L;
    long totalTime = System.currentTimeMillis();

        //set change flags
        order.setChangeFlags(true);

        //set user id
        if(order.getCsrId() == null || order.getCsrId().length() <= 0)
        {
          userID = DEFAULT_USER_ID;
        }
        else
        {
          userID = order.getCsrId();
        }
    
    // insert buyer data
    if(success){
      time = System.currentTimeMillis();
       success = updateBuyerTables(order);
      logger.info("Time taken to update buyer tables is " + (System.currentTimeMillis() - time)+ "ms");
    }

    // insert Order Header
    if(success){
      time = System.currentTimeMillis();
      success = insertOrderHeader(order);
      logger.info("Time taken to insert order header is " + (System.currentTimeMillis() - time)+ "ms");
    }

    // insert payment data
    if(success){
      time = System.currentTimeMillis();
       success = updatePaymentTables(order);
      logger.info("Time taken to update payment tables is " + (System.currentTimeMillis() - time)+ "ms");
    }

    // insert recipient data
    if(success){
      time = System.currentTimeMillis();
       success = updateRecipientTables(order);
      logger.info("Time taken to update recipient tables is " + (System.currentTimeMillis() - time)+ "ms");
    }

    // insert order details
    if(success){
      time = System.currentTimeMillis();
       success = updateOrderDetails(order);
      logger.info("Time taken to update order details is " + (System.currentTimeMillis() - time)+ "ms");
    }

    // insert fraud comments
    if(success){
      time = System.currentTimeMillis();
      success = updateFraudComments(order);
      logger.info("Time taken to update fraud comments is " + (System.currentTimeMillis() - time)+ "ms");
    }
      
    logger.info("Time taken to insert the entire order is " + (System.currentTimeMillis() - totalTime)+ "ms");
    return success;
  }

  /* Updates all the tables related to payments.
   * Each called method will either update or insert
   * data.
   * 
   * @params OrderVO order to  insert-update
   * @params boolean 
   * @returns boolean success */
   private boolean updatePaymentTables(OrderVO order) throws Exception 
   {

    boolean success = true;
     
    if(success)
      //  Credit Cards
      success = updateCreditCards(order);   


    if(success)
      //  Payments
      success = updatePayments(order);

      return success;
   }

  /* Updates all the tables except for the order table.
   * Each called method will either update or insert
   * data.
   * 
   * @params OrderVO order to  insert-update
   * @params boolean 
   * @returns boolean success */
   private boolean updateBuyerTables(OrderVO order) throws Exception 
   {

    boolean success = true;
     
    if(success)
      //  Buyer
      success = updateBuyer(order);


     if(success)
      //  Buyer Phones
      success = updateBuyerPhones(order); 



      if(success)
        //Buyer Comments...comments only inserterd, not insertd
      success = updateBuyerComments(order);    


      return success;
   }


  /* Updates all the tables except for the order table.
   * Each called method will either update or insert
   * data.
   * 
   * @params OrderVO order to  insert-update
   * @params boolean 
   * @returns boolean success */
   private boolean updateRecipientTables(OrderVO order) throws Exception 
   {

    boolean success = true;

     if(success)
      //  Recipients
      success = updateRecipient(order);

    if(success)
      //  Recipient phones
      success = updateRecipientPhones(order);

    if(success)
      //  Recipient comments
      success = updateRecipientComments(order);

      return success;
   }  

  /* Updates all the tables except for the order table.
   * Each called method will either update or insert
   * data.
   * 
   * @params OrderVO order to  insert-update
   * @params boolean 
   * @returns boolean success */
   private boolean updateOrderDetails(OrderVO order) throws Exception 
   {

    boolean success = true;

    //  Order Items
    if(success)
       success = updateOrderItems(order);

    if(success)
      //  Contacts
      success = updateContacts(order);    

    if(success)
      //  Add Ons
      success = updateAddOns(order);    

    if(success)
      //Dispositions
      success = updateDispositionss(order);
     
    if(success)
      //  Co Brand
      success = updateCoBrand(order);


      return success;
   }   

 /**
   * Retrieve order from FRP database schema.
   * 
   * @param order guid
   * @return OrderVO
   * @throws Exception
   */
  public OrderVO mapOrderFromDB(String guid) throws Exception
  {
        OrderVO order = new OrderVO();
        Map orderMap = null;
        long time = System.currentTimeMillis();

        // Get Order
        order.setGUID(guid);

        //Retrieve entire order
        orderMap = getOrderMap(order);
        
        if(orderMap != null)
        {
          // Get Order Header
          order = getOrderHeader(orderMap,order);

          //Get fraud comments
          Collection fraudComments  = getFraudComments(orderMap);
          order.setFraudComments((List)fraudComments);          

          // Get Order Items
          Collection items  = getOrderItems(orderMap);
          order.setOrderDetail((List)items);

          // Get Co Brand
          Collection coBrand = getCoBrand(orderMap);
          order.setCoBrand((List)coBrand);

          //Get Membershps          
          Collection memberships  = getMemberships(orderMap);
          order.setMemberships((List)memberships);

          //Get Contacts
          Collection contacts  = getContacts(orderMap);
          order.setOrderContactInfo((List)contacts);
          
          // Get Payments
          Collection payments  = getPayments(orderMap);
          order.setPayments((List)payments);

          // Get Buyer
          Collection buyers = getBuyer(orderMap);
          order.setBuyer((List)buyers);

          //set change flags
          order.setChangeFlags(false);

        }
        
        logger.info("Time taken to map order from db is " + (System.currentTimeMillis() - time)+ "ms");
        return order;
  }
  
 
  /* Retrieve order from database. */
  private Map getOrderMap(OrderVO order) throws Exception
  {
      // Get the order header infomration
      DataRequest dataRequest = new DataRequest();
      dataRequest.setConnection(connection);
      dataRequest.setStatementID(VIEW_ORDER_INFO);
      dataRequest.addInputParam(GUID_KEY, order.getGUID());

      Map orderMap = null;
      
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      orderMap = (Map)dataAccessUtil.execute(dataRequest);

      return orderMap;
  }  


/* Get item details for the order */
    private Collection getOrderItems(Map orderMap) throws Exception
    {
        Collection items = new ArrayList();
        OrderDetailsVO item = null;
        
        Collection allAddOns = getAddOns(orderMap);
        Collection allRecipients = getRecipient(orderMap);
        Collection allDispositions = getDispositions(orderMap);
          
        CachedResultSet rs = new CachedResultSet();
        rs = (CachedResultSet) orderMap.get(ORDER_ITEM_CURSOR);
            
        String recipientId = null;
        String recipientAddressId = null;
 
        while(rs != null && rs.next())
        {
            //cleanup holding objects
            recipientId = null;
            recipientAddressId = null;
                
            item = new OrderDetailsVO();

            item.setOrderDetailId(new Long(getValue(rs.getObject(1))).longValue());
            item.setGuid(getValue(rs.getObject(2)));
            item.setLineNumber(getValue(rs.getObject(3)));

            //recipientId is an integer and nullable
            recipientId = getValue(rs.getObject(4));
            if(recipientId != null)
              item.setRecipientId(new Long(recipientId).longValue());

            //recipientAddressId is an integer and nullable
            recipientAddressId = getValue(rs.getObject(5));
            if(recipientAddressId != null)
              item.setRecipientAddressId(new Long(recipientAddressId).longValue());

            item.setProductId(getValue(rs.getObject(6)));
            item.setQuantity(getValue(rs.getObject(7)));
            item.setShipVia(getValue(rs.getObject(8)));
            item.setShipMethod(getValue(rs.getObject(9)));
            item.setShipDate(getValue(rs.getObject(10)));
            item.setDeliveryDate(getValue(rs.getObject(11)));
            item.setDeliveryDateRangeEnd(getValue(rs.getObject(12)));
            item.setDropShipTrackingNumber(getValue(rs.getObject(13)));
            item.setProductsAmount(getValue(rs.getObject(14)));
            item.setTaxAmount(getValue(rs.getObject(15)));
            item.setServiceFeeAmount(getValue(rs.getObject(16)));
            item.setShippingFeeAmount(getValue(rs.getObject(17)));
            item.setAddOnAmount(getValue(rs.getObject(18)));
            item.setDiscountAmount(getValue(rs.getObject(19)));
            item.setOccassionId(getValue(rs.getObject(20)));
            item.setLastMinuteGiftSignature(getValue(rs.getObject(21)));
            item.setLastMinuteNumber(getValue(rs.getObject(22)));
            item.setLastMinuteGiftEmail(getValue(rs.getObject(23)));
            item.setExternalOrderNumber(getValue(rs.getObject(24)));
            item.setColorFirstChoice(getValue(rs.getObject(25)));
            item.setColorSecondChoice(getValue(rs.getObject(26)));
            item.setSubstituteAcknowledgement(getValue(rs.getObject(27)));
            item.setCardMessage(getValue(rs.getObject(28)));
            item.setCardSignature(getValue(rs.getObject(29)));
            item.setOrderComments(getValue(rs.getObject(30)));
            item.setOrderContactInformation(getValue(rs.getObject(31)));
            item.setFloristNumber(getValue(rs.getObject(32)));
            item.setSpecialInstructions(getValue(rs.getObject(33)));
            item.setStatus(getValue(rs.getObject(34)));            
            item.setAribaUnspscCode(getValue(rs.getObject(35)));
            item.setAribaPoNumber(getValue(rs.getObject(36)));
            item.setAribaAmsProjectCode(getValue(rs.getObject(37)));
            item.setAribaCostCenter(getValue(rs.getObject(38)));
            item.setExternalOrderTotal(getValue(rs.getObject(39)));
            item.setSizeChoice(getValue(rs.getObject(40)));
            item.setSenderInfoRelease(getValue(rs.getObject(41)));
            item.setMilesPoints(getValue(rs.getObject(42)));
            item.setProductSubCodeId(getValue(rs.getObject(46)));
            item.setColorOneDescription(getValue(rs.getObject(47)));
            item.setColorTwoDescription(getValue(rs.getObject(48)));
            item.setOccasionDescription(getValue(rs.getObject(49)));
            
          
            Iterator iterator = null;

            //for each order item build dispositions
            Collection dispositions = new ArrayList();
            iterator = allDispositions.iterator();
                                
            for (int i = 0; iterator.hasNext(); i++) 
            {
               DispositionsVO disposition = new DispositionsVO();
               disposition = (DispositionsVO)iterator.next();
                    
               if(disposition.getOrderDetailID() == item.getOrderDetailId())
               {
                  dispositions.add(disposition);
               }                    
            }
                
            //for each order item build new add ons
            Collection addOns = new ArrayList();
            iterator = allAddOns.iterator();
                                
            for (int i = 0; iterator.hasNext(); i++) 
            {
               AddOnsVO addOn = new AddOnsVO();
               addOn = (AddOnsVO)iterator.next();
                    
               if(addOn.getOrderDetailId() == item.getOrderDetailId())
               {
                  addOns.add(addOn);
               }                    
            }

            //for each order item build new recipients
            Collection recipients = new ArrayList();
            iterator = allRecipients.iterator();
                                
            for (int i = 0; iterator.hasNext(); i++) 
            {
               RecipientsVO recipient = new RecipientsVO();
               recipient = (RecipientsVO)iterator.next();
                    
               if(recipient.getRecipientId() == item.getRecipientId())
               {
                  recipients.add(recipient);
               }                    
            }

 
                
            item.setAddOns((List)addOns);
            item.setRecipients((List)recipients);
            item.setDispositions((List)dispositions);
                
            items.add(item);
        }

        return items;
    }  

    /* Create addon objects */
    private Collection getDispositions(Map orderMap) throws Exception
    {
       // Get the order detail addon infomration
 
        Collection dispositions = new ArrayList();
        DispositionsVO disposition = null;

        CachedResultSet rs = new CachedResultSet();
        rs = (CachedResultSet) orderMap.get(ORDER_DISPOSITIONS);
 
        while(rs != null && rs.next())
        {
            disposition = new DispositionsVO();

            disposition.setOrderDispositionID(new Long(getValue(rs.getObject(1))).longValue());
            disposition.setOrderDetailID(new Long(getValue(rs.getObject(2))).longValue());
            disposition.setDispositionID(getValue( rs.getObject(3)));
            disposition.setComments(getValue( rs.getObject(4)));               
            disposition.setCalledCustomerFlag(getValue( rs.getObject(5)));               
            disposition.setSentEmailFlag(getValue( rs.getObject(6)));               
            disposition.setStockMessageID(getValue( rs.getObject(7)));                           
            disposition.setEmailSubject(getValue( rs.getObject(8)));                           
            disposition.setEmailMessage(getValue( rs.getObject(9)));                                       
            dispositions.add(disposition);
        }

        return dispositions;
    }    



    /* Create addon objects */
    private Collection getAddOns(Map orderMap) throws Exception
    {
       // Get the order detail addon infomration
 
        Collection addOns = new ArrayList();
        AddOnsVO addOn = null;

        CachedResultSet rs = new CachedResultSet();
        rs = (CachedResultSet) orderMap.get(ORDER_ADD_ONS);
 
        while(rs != null && rs.next())
        {
            addOn = new AddOnsVO();

            addOn.setAddOnId(new Long(getValue(rs.getObject(1))).longValue());
            addOn.setOrderDetailId(new Long(getValue(rs.getObject(2))).longValue());
            addOn.setAddOnCode(getValue( rs.getObject(3)));
            addOn.setAddOnQuantity(getValue( rs.getObject(4)));               
               
            addOns.add(addOn);
        }

        return addOns;
    }    

    /* Get recipient objects */
   private Collection getRecipient(Map orderMap) throws Exception
    {
        Collection recipients = new ArrayList();
        RecipientsVO recipient = null;
        
        Collection addresses = getRecipientAddresses(orderMap);
        Collection comments = getRecipientComments(orderMap);
        Collection phones = getRecipientPhones(orderMap);
            
        CachedResultSet rs = new CachedResultSet();
        rs = (CachedResultSet) orderMap.get(RECIPIENT);
            
        while(rs != null && rs.next())
        {
            recipient = new RecipientsVO();

            recipient.setRecipientId(new Long(getValue(rs.getObject(1))).longValue());
            recipient.setLastName(getValue( rs.getObject(2)));
            recipient.setFirstName(getValue( rs.getObject(3)));
            recipient.setListCode(getValue( rs.getObject(4)));
            recipient.setAutoHold(getValue( rs.getObject(5)));
            recipient.setMailListCode(getValue( rs.getObject(6)));
            recipient.setStatus(getValue( rs.getObject(7)));
            recipient.setCustomerId(getValue( rs.getObject(11)));

            Iterator iterator = null;
            Collection col = null;
                
            //Addresses
            col = new ArrayList();
            iterator = addresses.iterator();
            for (int i = 0; iterator.hasNext(); i++) 
            {
               RecipientAddressesVO vo = (RecipientAddressesVO)iterator.next();
                    
               if(vo.getRecipientId() == recipient.getRecipientId())
               {
                  col.add(vo);
               }                    
            }
            recipient.setRecipientAddresses((List)col);            
            //recipient.setRecipientAddresses((List)addresses);
            


            //Comments
            col = new ArrayList();
            iterator = comments.iterator();
            for (int i = 0; iterator.hasNext(); i++) 
            {
               RecipientCommentsVO vo = (RecipientCommentsVO)iterator.next();
                    
               if(vo.getRecipientId() == recipient.getRecipientId())
               {
                  col.add(vo);
               }                    
            }            
            recipient.setRecipientComments((List)col);
            //recipient.setRecipientComments((List)comments);

            //Phones
            col = new ArrayList();
            iterator = phones.iterator();
            for (int i = 0; iterator.hasNext(); i++) 
            {
               RecipientPhonesVO vo = (RecipientPhonesVO)iterator.next();
                    
               if(vo.getRecipientId() == recipient.getRecipientId())
               {
                  col.add(vo);
               }                    
            }                     
            recipient.setRecipientPhones((List)col);
            //recipient.setRecipientPhones((List)phones);
            
            recipients.add(recipient);
        }

        return recipients;
    }    

    /* Get addresses for recipients */
    private Collection getRecipientAddresses(Map orderMap) throws Exception
    {
       // Get the recipient addresses
 
        Collection addresses = new ArrayList();
        RecipientAddressesVO address = null;

        CachedResultSet rs = new CachedResultSet();
        rs = (CachedResultSet) orderMap.get(RECIPIENT_ADDRESSES);
  
        String recipientId = null;
        String destinationId = null;

        while(rs != null && rs.next())
        {
            //cleanup holding objects
            recipientId = null;
            destinationId = null;
            
            address = new RecipientAddressesVO();
            address.setRecipientAddressId(new Long(getValue(rs.getObject(1))).longValue());

            //recipientId is an integer and nullable
            recipientId = getValue(rs.getObject(2));
            if(recipientId != null)
              address.setRecipientId(new Long(recipientId).longValue());

            address.setAddressType(getValue( rs.getObject(3)));
            address.setAddressLine1(getValue( rs.getObject(4)));
            address.setAddressLine2(getValue( rs.getObject(5)));
            address.setCity(getValue( rs.getObject(6)));
            address.setStateProvince(getValue( rs.getObject(7)));
            address.setPostalCode(getValue( rs.getObject(8)));
            address.setCountry(getValue( rs.getObject(9)));
            address.setCounty(getValue( rs.getObject(10)));                

            address.setGeofindMatchCode(getValue( rs.getObject(11)));
            address.setLatitude(getValue( rs.getObject(12)));
            address.setLongitude(getValue( rs.getObject(13)));
            
            address.setInternational(getValue( rs.getObject(14)));
            address.setName(getValue( rs.getObject(15)));
            address.setInfo(getValue( rs.getObject(16)));
            address.setAddressTypeCode(getValue(rs.getObject(20)));

                             
            addresses.add(address);
        }

        return addresses;
    }    


//DESTINATIONS TABLE REMOVED ON 11/10/03...REMOVE THIS SECTION
//IF THE TABLE IS GONE FOR GOOD
 /*   private Collection getDestinations(Map orderMap) throws Exception
    {
       // Get the recipient destinations
 
        Collection destinations = new ArrayList();
        DestinationsVO destination = null;

        CachedResultSet rs = new CachedResultSet();
        rs = (CachedResultSet) orderMap.get(RECIPIENT_DESTINATIONS);
  
        String recipientId = null;

        while(rs != null && rs.next())
        {
            //cleanup holding objects
            recipientId = null;
            
            destination = new DestinationsVO();
            destination.setDestinationId(new Long(getValue(rs.getObject(1))).longValue());

            //recipientId is an integer and nullable
            recipientId = getValue(rs.getObject(2));
            if(recipientId != null)
              destination.setRecipientId(new Long(recipientId).longValue());

            destination.setDestinationType(getValue( rs.getObject(3)));
            destination.setName(getValue( rs.getObject(4)));
            destination.setInfo(getValue( rs.getObject(5)));
                             
            destinations.add(destination);
        }

        return destinations;
    }    
*/

    /* Retrieve recipient comments from database. */
    private Collection getRecipientComments(Map orderMap) throws Exception
    {
       // Get the recipient comments
 
        Collection comments = new ArrayList();
        RecipientCommentsVO comment = null;

        CachedResultSet rs = new CachedResultSet();
        rs = (CachedResultSet) orderMap.get(RECIPIENT_COMMENTS);
  
        Object recipientId = null;

        while(rs != null && rs.next())
        {
            //cleanup holding objects
            recipientId = null;
            
            comment = new RecipientCommentsVO();
            comment.setRecipientCommentId(new Long(getValue(rs.getObject(1))).longValue());

            //recipientId is an integer and nullable
            recipientId = rs.getObject(2);
            if(recipientId != null)
              comment.setRecipientId(new Long(recipientId.toString()).longValue());

            comment.setText(getValue( rs.getObject(3)));
                             
            comments.add(comment);
        }

        return comments;
    }    

    /* Retrieve recipient phone from database. */
    private Collection getRecipientPhones(Map orderMap) throws Exception
    {
       // Get the recipient phones
 
        Collection phones = new ArrayList();
        RecipientPhonesVO phone = null;

        CachedResultSet rs = new CachedResultSet();
        rs = (CachedResultSet) orderMap.get(RECIPIENT_PHONES);
  
        Object recipientId = null;

        while(rs != null && rs.next())
        {
            //cleanup holding objects
            recipientId = null;
            
            phone = new RecipientPhonesVO();
            phone.setRecipientPhoneId(new Long(rs.getObject(1).toString()).longValue());

            //recipientId is an integer and nullable
            recipientId = rs.getObject(2);
            if(recipientId != null)
              phone.setRecipientId(new Long(recipientId.toString()).longValue());

            phone.setPhoneType(getValue( rs.getObject(3)));
            phone.setPhoneNumber(getValue( rs.getObject(4)));
            phone.setExtension(getValue( rs.getObject(5)));
                             
            phones.add(phone);
        }

        return phones;
    }

  public OrderVO getOrderHeader(Map orderMap,OrderVO order) throws Exception
  {
      // Get the order header infomration
      String buyerId = null;
      Object buyerEmailId = null;
      Object membershipId = null;
              
      CachedResultSet rs = new CachedResultSet();
      rs = (CachedResultSet) orderMap.get(ORDER_HEADER_CURSOR);
            
      while(rs != null && rs.next())
      {
          //cleanup holding objects
          buyerId = null;
          buyerEmailId = null;
          membershipId = null;

          order.setGUID(getValue(rs.getObject(1)));
          order.setMasterOrderNumber(getValue(rs.getObject(2)));

          //buyerId is an integer and nullable
          buyerId = getValue(rs.getObject(3));
          if(buyerId != null)
            order.setBuyerId(new Long(buyerId).longValue());
               
          order.setOrderTotal(getValue(rs.getObject(4)));
          order.setProductsTotal(getValue(rs.getObject(5)));
          order.setTaxTotal(getValue(rs.getObject(6)));
          order.setServiceFeeTotal(getValue(rs.getObject(7)));
          order.setShippingFeeTotal(getValue(rs.getObject(8)));

          //buyerEmailId is an integer and nullable
          buyerEmailId = rs.getObject(9);
          if(buyerEmailId != null)
            order.setBuyerEmailId(new Long(buyerEmailId.toString()).longValue());  

          order.setOrderDate(getValue(rs.getObject(10)));
          order.setOrderOrigin(getValue(rs.getObject(11)));

          //membershipId is an integer and nullable
          membershipId = rs.getObject(12);
          if(membershipId != null)
            order.setMembershipId(new Long(membershipId.toString()).longValue());
                
          order.setCsrId(getValue(rs.getObject(13)));
          order.setAddOnAmountTotal(getValue(rs.getObject(14)));
          order.setPartnershipBonusPoints(getValue(rs.getObject(15)));
          order.setSourceCode(getValue(rs.getObject(16)));
          order.setCallTime(getValue(rs.getObject(17)));
          order.setDnisCode(getValue(rs.getObject(18)));
          order.setDatamartUpdateDate(getValue(rs.getObject(19)));
          order.setDiscountTotal(getValue(rs.getObject(20)));
          order.setYellowPagesCode(getValue(rs.getObject(21)));
          order.setStatus(getValue(rs.getObject(22)));
          order.setAribaBuyerAsnNumber(getValue(rs.getObject(23)));
          order.setAribaBuyerCookie(getValue(rs.getObject(24)));
          order.setAribaPayload(getValue(rs.getObject(25)));  
          order.setCoBrandCreditCardCode(getValue(rs.getObject(29)));
      }

      return order;
    }

    private Collection getCoBrand(Map orderMap) throws Exception
    {
        // Get the co brand infomration

        Collection coBrands = new ArrayList();
        CoBrandVO coBrand = null;

        CachedResultSet rs = new CachedResultSet();
        rs = (CachedResultSet) orderMap.get(ORDER_CO_BRANDS);

        while(rs != null && rs.next())
        {
            coBrand = new CoBrandVO();

            coBrand.setCoBrandId(new Long(rs.getObject(1).toString()).longValue());
            coBrand.setGuid(getValue(rs.getObject(2)));
            coBrand.setInfoName(getValue(rs.getObject(3)));
            coBrand.setInfoData(getValue(rs.getObject(4)));
            coBrands.add(coBrand);
        }

        return coBrands;
    }

    private Collection getPayments(Map orderMap) throws Exception
    {
       // Get the payments
 
        Collection payments = new ArrayList();
        PaymentsVO payment = null;

        Collection allCreditCards = getCreditCards(orderMap);
        
        CachedResultSet rs = new CachedResultSet();
        rs = (CachedResultSet) orderMap.get(ORDER_PAYMENTS);

        while(rs != null && rs.next())
        {
            payment = new PaymentsVO();

            payment.setPaymentId(formatToNativeLong(getValue(rs.getObject(1))));
            payment.setGuid(getValue(rs.getObject(2)));
            payment.setPaymentsType(getValue(rs.getObject(3)));
            payment.setAmount(getValue(rs.getObject(4)));
            payment.setCreditCardId(formatToNativeLong(getValue(rs.getObject(5))));
            payment.setGiftCertificateId(getValue(rs.getObject(6)));
            payment.setAuthResult(getValue(rs.getObject(7)));                
            payment.setAuthNumber(getValue(rs.getObject(8)));                
            payment.setAvsCode(getValue(rs.getObject(9)));
            payment.setAcqReferenceNumber(getValue(rs.getObject(10)));
            payment.setAafesTicket(getValue(rs.getObject(11)));                
            payment.setInvoiceNumber(getValue(rs.getObject(12)));  
            payment.setPaymentMethodType(getValue(rs.getObject(17)));
               
            //for each payment build new credit card
            Collection creditCards = new ArrayList();
            Iterator iterator = allCreditCards.iterator();
                                
            for (int i = 0; iterator.hasNext(); i++) 
            {
               CreditCardsVO creditCard = new CreditCardsVO();
               creditCard = (CreditCardsVO)iterator.next();
                    
               if(creditCard.getCreditCardId() == payment.getCreditCardId())
               {
                  creditCards.add(creditCard);
               }                    
            }

            payment.setCreditCards((List)creditCards);
            payments.add(payment);
        }

        return payments;
    }



    private Collection getFraudComments(Map orderMap) throws Exception
    {
       // Get the fraud comments
 
        Collection comments = new ArrayList();
        FraudCommentsVO comment = null;
       
        CachedResultSet rs = new CachedResultSet();
        rs = (CachedResultSet) orderMap.get(FRAUD_COMMENTS);

        while(rs != null && rs.next())
        {
            comment = new FraudCommentsVO();
            comment.setFraudCommentID(formatToNativeLong(getValue(rs.getObject(1))));
            comment.setGUID(getValue(rs.getObject(2)));
            comment.setFraudID(getValue(rs.getObject(3)));
            comment.setCommentText(getValue(rs.getObject(4)));
            comment.setFraudDescription(getValue(rs.getObject(8)));

            comments.add(comment);
        }

        return comments;
    }

    private Collection getMemberships(Map orderMap) throws Exception
    {
       // Get the memerships
 
        Collection memberships = new ArrayList();
        MembershipsVO membership = null;

       
        CachedResultSet rs = new CachedResultSet();
        rs = (CachedResultSet) orderMap.get(ORDER_MEMBERSHIPS);

        while(rs != null && rs.next())
        {
            membership = new MembershipsVO();

            membership.setMembershipId(formatToNativeLong(getValue(rs.getObject(1))));
            membership.setBuyerId(formatToNativeLong(getValue(rs.getObject(2))));
            membership.setMembershipType(getValue(rs.getObject(3)));
            membership.setLastName(getValue(rs.getObject(4)));
            membership.setFirstName(getValue(rs.getObject(5)));
            membership.setMembershipIdNumber(getValue(rs.getObject(6)));         
            memberships.add(membership);
        }

        return memberships;
    }


    private Collection getContacts(Map orderMap) throws Exception
    {
       // Get the memerships
 
        Collection contacts = new ArrayList();
        OrderContactInfoVO contact = null;
       
        CachedResultSet rs = new CachedResultSet();
        rs = (CachedResultSet) orderMap.get(ORDER_CONTACTS);

        while(rs != null && rs.next())
        {
            contact = new OrderContactInfoVO();
    
            contact.setOrderContactInfoId(formatToNativeLong(getValue(rs.getObject(1))));
            contact.setOrderGuid(getValue(rs.getObject(2)));
            contact.setFirstName(getValue(rs.getObject(3)));
            contact.setLastName(getValue(rs.getObject(4)));
            contact.setPhone(getValue(rs.getObject(4)));
            contact.setExt(getValue(rs.getObject(5)));
            contact.setEmail(getValue(rs.getObject(6)));           

            contacts.add(contact);
        }

        return contacts;
    }


    private Collection getBuyer(Map orderMap) throws Exception
    {
        Collection buyers = new ArrayList();
        BuyerVO buyer = null;
        
        Collection buyerPhones = getBuyerPhones(orderMap);
        Collection buyerComments = getBuyerComments(orderMap);
        Collection allBuyerAddresses = getAllBuyerAddresses(orderMap);
        Collection allBuyerEmails = getAllBuyerEmails(orderMap);
        //Collection allNCOAs = getNCOA(orderMap);

        CachedResultSet rs = new CachedResultSet();
        rs = (CachedResultSet) orderMap.get(BUYER);
            
        while(rs != null && rs.next())
        {
            buyer = new BuyerVO();

            buyer.setBuyerId(new Long(getValue(rs.getObject(1))).longValue());
            buyer.setCustomerId(getValue( rs.getObject(2)));
            buyer.setLastName(getValue( rs.getObject(3)));
            buyer.setFirstName(getValue( rs.getObject(4)));
            buyer.setAutoHold(getValue( rs.getObject(5)));
            buyer.setBestCustomer(getValue( rs.getObject(6)));
            buyer.setStatus(getValue( rs.getObject(7)));

            /** NOT USING NCOS RIGHT NOW
            //set 1 to 1 relations for buyer object model
            //for each buyer ncoa
            Collection buyerNCOAs = new ArrayList();
            Iterator ncoaIterator = allNCOAs.iterator();
                                
            for (int i = 0; ncoaIterator.hasNext(); i++) 
            {
               NcoaVO ncoa = new NcoaVO();
               ncoa = (NcoaVO)ncoaIterator.next();
                    
               if(ncoa.getBuyerId() == ncoa.getBuyerId())
               {
                  buyerNCOAs.add(ncoa);
               }                    
            }
            buyer.setNcoa((List)buyerNCOAs); */



            //set 1 to 1 relations for buyer object model
            //for each buyer build new email
            Collection phones = new ArrayList();
            Iterator phoneIterator = buyerPhones.iterator();
                                
            for (int i = 0; phoneIterator.hasNext(); i++) 
            {
               BuyerPhonesVO phone = new BuyerPhonesVO();
               phone = (BuyerPhonesVO)phoneIterator.next();
                    
               if(phone.getBuyerId() == buyer.getBuyerId())
               {
                  phones.add(phone);
               }                    
            }
            buyer.setBuyerPhones((List)phones);
            
            buyer.setBuyerComments((List)buyerComments);
                
            //for each buyer build new address
            Collection addresses = new ArrayList();
            Iterator iterator = allBuyerAddresses.iterator();
                                
            for (int i = 0; iterator.hasNext(); i++) 
            {
               BuyerAddressesVO address = new BuyerAddressesVO();
               address = (BuyerAddressesVO)iterator.next();
                    
               if(address.getBuyerId() == buyer.getBuyerId())
               {
                  addresses.add(address);
               }                    
            }

            buyer.setBuyerAddresses((List)addresses);

            //for each buyer build new email
            Collection emails = new ArrayList();
            iterator = allBuyerEmails.iterator();
                                
            for (int i = 0; iterator.hasNext(); i++) 
            {
               BuyerEmailsVO email = new BuyerEmailsVO();
               email = (BuyerEmailsVO)iterator.next();
                    
               if(email.getBuyerId() == buyer.getBuyerId())
               {
                  emails.add(email);
               }                    
            }

            buyer.setBuyerEmails((List)emails);
            buyers.add(buyer);
        }

        return buyers;
    }

    private Collection getBuyerPhones(Map orderMap) throws Exception
    {
       // Get the buyer phone numbers
 
        Collection phones = new ArrayList();
        BuyerPhonesVO phone = null;

        CachedResultSet rs = new CachedResultSet();
        rs = (CachedResultSet) orderMap.get(BUYER_PHONES);
  
        String buyerId = null;

        while(rs != null && rs.next())
        {
            //cleanup holding objects
            buyerId = null;
            
            phone = new BuyerPhonesVO();
            phone.setBuyerPhoneId(new Long(getValue(rs.getObject(1))).longValue());

            //buyerId is an integer and nullable
            buyerId = getValue(rs.getObject(2));
            if(buyerId != null)
              phone.setBuyerId(new Long(buyerId).longValue());

            phone.setPhoneType(getValue( rs.getObject(3)));
            phone.setPhoneNumber(getValue( rs.getObject(4)));
            phone.setExtension(getValue( rs.getObject(5)));                
                             
            phones.add(phone);
        }

        return phones;
    }

    private Collection getBuyerComments(Map orderMap) throws Exception
    {
       // Get the buyer comments
 
        Collection comments = new ArrayList();
        BuyerCommentsVO comment = null;

        CachedResultSet rs = new CachedResultSet();
        rs = (CachedResultSet) orderMap.get(BUYER_COMMENTS);
  
        String buyerId = null;

        while(rs != null && rs.next())
        {
            //cleanup holding objects
            buyerId = null;
            
            comment = new BuyerCommentsVO();
            comment.setBuyerCommentId(new Long(getValue(rs.getObject(1))).longValue());

            //buyerId is an integer and nullable
            buyerId = getValue(rs.getObject(2));
            if(buyerId != null)
              comment.setBuyerId(new Long(buyerId).longValue());

            comment.setText(getValue( rs.getObject(3)));
                             
            comments.add(comment);
        }

        return comments;
    }


    /** CURRENTLY NOT USED
     * 
    private Collection getNCOA(Map orderMap) throws Exception
    {
       // Get the NCOA
 
        Collection ncoaCol = new ArrayList();
        NcoaVO ncoa = null;

        CachedResultSet rs = new CachedResultSet();
        rs = (CachedResultSet) orderMap.get(NCOA);
  
        String buyerId = null;

        while(rs != null && rs.next())
        {
            //cleanup holding objects
            buyerId = null;
            
            ncoa = new NcoaVO();
            ncoa.setNcoaId(formatToNativeLong(getValue(rs.getObject(1))));
            ncoa.setBuyerId(formatToNativeLong(getValue(rs.getObject(2))));
            ncoa.setDeliverability(getValue(rs.getObject(3)));
            ncoa.setNixieGroup(getValue(rs.getObject(4)));
            ncoa.setDupeSetUniqueness(getValue(rs.getObject(5)));
            ncoa.setEmsAddressScore(getValue(rs.getObject(6)));
                             
            ncoaCol.add(ncoa);
        }

        return ncoaCol;
    } */

    private Collection getCreditCards(Map orderMap) throws Exception
    {
       // Get the credit cards
 
        Collection creditCards = new ArrayList();
        CreditCardsVO creditCard = null;

        CachedResultSet rs = new CachedResultSet();
        rs = (CachedResultSet) orderMap.get(ORDER_CREDIT_CARDS);

        String buyerId = null;
            
        while(rs != null && rs.next())
        {
            //cleanup holding objects
            buyerId = null;
                
            creditCard = new CreditCardsVO();
            creditCard.setCreditCardId(new Long(getValue(rs.getObject(1))).longValue());

            //buyerId is an integer and nullable
            buyerId = getValue(rs.getObject(2));
            if(buyerId != null)
              creditCard.setBuyerId(new Long(buyerId).longValue());

            creditCard.setCCType(getValue(rs.getObject(3)));
            creditCard.setCCNumber(getValue(rs.getObject(4)));
            creditCard.setCCExpiration(getValue(rs.getObject(5)));
            creditCards.add(creditCard);
        }

        return creditCards;
    }    

    private Collection getAllBuyerAddresses(Map orderMap) throws Exception
    {
       // Get the buyer addresses
 
        Collection addresses = new ArrayList();
        BuyerAddressesVO address = null;

        CachedResultSet rs = new CachedResultSet();
        rs = (CachedResultSet) orderMap.get(BUYER_ADDRESSES);
  
        String buyerId = null;

        while(rs != null && rs.next())
        {
            //cleanup holding objects
            buyerId = null;
            
            address = new BuyerAddressesVO();
            address.setBuyerAddressId(new Long(getValue(rs.getObject(1))).longValue());

            //buyerId is an integer and nullable
            buyerId = getValue(rs.getObject(2));
            if(buyerId != null)
              address.setBuyerId(new Long(buyerId).longValue());

            address.setAddressType(getValue( rs.getObject(3)));
            address.setAddressLine1(getValue( rs.getObject(4)));
            address.setAddressLine2(getValue( rs.getObject(5)));
            address.setCity(getValue( rs.getObject(6)));
            address.setStateProv(getValue( rs.getObject(7)));
            address.setPostalCode(getValue( rs.getObject(8)));
            address.setCountry(getValue( rs.getObject(9)));
            address.setCounty(getValue( rs.getObject(10)));
            address.setAddressEtc(getValue(rs.getObject(11)));
                             
            addresses.add(address);
        }

        return addresses;
    }

    private Collection getAllBuyerEmails(Map orderMap) throws Exception
    {
       // Get the buyer emails
 
        Collection emails = new ArrayList();
        BuyerEmailsVO email = null;

        CachedResultSet rs = new CachedResultSet();
        rs = (CachedResultSet) orderMap.get(BUYER_EMAILS);
  
        String buyerId = null;

        while(rs != null && rs.next())
        {
            //cleanup holding objects
            buyerId = null;
            
            email = new BuyerEmailsVO();
            email.setBuyerEmailId(new Long(getValue(rs.getObject(1))).longValue());

            //buyerId is an integer and nullable
            buyerId = getValue(rs.getObject(2));
            if(buyerId != null)
              email.setBuyerId(new Long(buyerId).longValue());

            email.setEmail(getValue( rs.getObject(3)));
            email.setPrimary(getValue( rs.getObject(4)));
            email.setNewsletter(getValue( rs.getObject(5)));
                             
            emails.add(email);
        }

        return emails;
    }


  /* This method takes in an object and returns
   * it's string value.
   * @param Object
   * @return String value of object
   *         If object is null, null is returned*/
  private String getValue(Object obj) throws Exception
  {

    String value = null;

    if(obj == null)
    {
      return null;
    }

    if(obj instanceof BigDecimal) {
      value = obj.toString();      
    }
    else if(obj instanceof String) {
      value = (String)obj;
    }
    else if(obj instanceof Timestamp) {
      value = obj.toString();
    }
    else
    {
      value = obj.toString();
    }

    return value;
  }

 /**
   * 
   * 
   * @param order id
   * @return  true/false success of update
   */
  public boolean updateOrder(OrderVO order) throws Exception
  {
    boolean success = true; 
    long time = 0L;
    long totalTime = System.currentTimeMillis();
    
        //set user id
        if(order.getCsrId() == null || order.getCsrId().length() <= 0)
        {
          userID = DEFAULT_USER_ID;
        }
        else
        {
          userID = order.getCsrId();
        }

    // insert buyer data
    if(success){
      time = System.currentTimeMillis();
       success = updateBuyerTables(order);
      logger.info("Time taken to update buyer tables is " + (System.currentTimeMillis() - time)+ "ms");
    }

    // Update Order Header
    if(success){    
      time = System.currentTimeMillis();
        success = updateOrderHeader(order);
      logger.info("Time taken to update order header is " + (System.currentTimeMillis() - time)+ "ms");
    }

    // insert payment data
    if(success){
      time = System.currentTimeMillis();
       success = updatePaymentTables(order);
      logger.info("Time taken to update payment tables is " + (System.currentTimeMillis() - time)+ "ms");
    }

    // insert recipeint data
    if(success){
      time = System.currentTimeMillis();
       success = updateRecipientTables(order);
      logger.info("Time taken to update recipient tables is " + (System.currentTimeMillis() - time)+ "ms");
    }

    // insert detail data
    if(success){
      time = System.currentTimeMillis();
       success = updateOrderDetails(order); 
      logger.info("Time taken to update order details is " + (System.currentTimeMillis() - time)+ "ms");
    }

    // insert fraud comments
    if(success){
      time = System.currentTimeMillis();
      success = updateFraudComments(order);
      logger.info("Time taken to update fraud comments is " + (System.currentTimeMillis() - time)+ "ms");
    }
      
    logger.info("Time taken to update the entire order is " + (System.currentTimeMillis() - totalTime)+ "ms");
    return success;
  }

    private boolean updateOrderHeader(OrderVO order) throws Exception
    {
        // update the order header infomration
        DataRequest dataRequest = new DataRequest();

        if(order.isChanged()){

        HashMap orderHeader = new HashMap();

        orderHeader.put("IN_UPDATED_BY" , userID);
        orderHeader.put("IN_GUID" , order.getGUID());
        orderHeader.put("IN_MASTER_ORDER_NUMBER" , order.getMasterOrderNumber());
        orderHeader.put("IN_BUYER_ID" , formatToLong(order.getBuyerId()));
        orderHeader.put("IN_ORDER_TOTAL" , formatToDouble(order.getOrderTotal()));
        orderHeader.put("IN_PRODUCTS_TOTAL" , formatToDouble(order.getProductsTotal()));
        orderHeader.put("IN_TAX_TOTAL" , formatToDouble(order.getTaxTotal()));
        orderHeader.put("IN_SERVICE_FEE_TOTAL" , formatToDouble(order.getServiceFeeTotal()));
        orderHeader.put("IN_SHIPPING_FEE_TOTAL" , formatToDouble(order.getShippingFeeTotal()));
        orderHeader.put("IN_BUYER_EMAIL_ID" , formatToLong(order.getBuyerEmailId()));
        orderHeader.put("IN_ORDER_DATE" , getSQLTimestamp(order.getOrderDate()));
        orderHeader.put("IN_ORDER_ORIGIN" , order.getOrderOrigin());
        orderHeader.put("IN_MEMBERSHIP_ID" , formatToLong(order.getMembershipId()));
        orderHeader.put("IN_CSR_ID" , order.getCsrId());
        orderHeader.put("IN_ADD_ON_AMOUNT_TOTAL" , formatToDouble(order.getAddOnAmountTotal()));
        orderHeader.put("IN_PARTNERSHIP_BONUS_POINTS" , formatToDouble(order.getPartnershipBonusPoints()));
        orderHeader.put("IN_SOURCE_CODE" , order.getSourceCode());
        orderHeader.put("IN_CALL_TIME" , formatToLong(order.getCallTime()));
        orderHeader.put("IN_DNIS_CODE" , order.getDnisCode());
//        orderHeader.put("IN_DATAMART_UPDATE_DATE" , getSQLDate(order.getDatamartUpdateDate()));
        orderHeader.put("IN_DISCOUNT_TOTAL" , formatToDouble(order.getDiscountTotal()));
        orderHeader.put("IN_YELLOW_PAGES_CODE" , order.getYellowPagesCode());
        orderHeader.put("IN_ARIBA_BUYER_ASN_NUMBER" , order.getAribaBuyerAsnNumber());
        orderHeader.put("IN_ARIBA_BUYER_COOKIE" , order.getAribaBuyerCookie());
        orderHeader.put("IN_ARIBA_PAYLOAD" , order.getAribaPayload());
        orderHeader.put("IN_STATUS" , order.getStatus());
        orderHeader.put("IN_FRAUD" , order.getFraudFlag());

        orderHeader.put("IN_CO_BRAND_CREDIT_CARD_CODE" , order.getCoBrandCreditCardCode());

        
        orderHeader.put(STATUS_PARAM,"");
        orderHeader.put(MESSAGE_PARAM,"");
  
        dataRequest.setConnection(connection);
        dataRequest.setStatementID(UPDATE_ORDER_HEADER);

        //This code is here because the DataAccess object currently does not
        //support the insertion of nulls in Long fields.  This is a workaround,
        //Sasha is deciding how this problem will be corrected.
        if((order.getBuyerEmailId() <=0) &&
              order.getMembershipId() <=0){
            orderHeader.put("IN_BUYER_EMAIL_ID", Integer.valueOf(String.valueOf(java.sql.Types.VARCHAR)));
            orderHeader.put("IN_MEMBERSHIP_ID", Integer.valueOf(String.valueOf(java.sql.Types.VARCHAR)));
            dataRequest.setStatementID("FRP.UPDATE_ORDER_HEADER_NOBUYEREMAILORMEMBERSHIP");
        }
        else if(order.getBuyerEmailId() <=0) {
            orderHeader.put("IN_BUYER_EMAIL_ID", Integer.valueOf(String.valueOf(java.sql.Types.VARCHAR)));
            dataRequest.setStatementID("FRP.UPDATE_ORDER_HEADER_NOBUYEREMAIL");
        }
        else if(order.getMembershipId() <=0){
            orderHeader.put("IN_MEMBERSHIP_ID", Integer.valueOf(String.valueOf(java.sql.Types.VARCHAR)));
            dataRequest.setStatementID("FRP.UPDATE_ORDER_HEADER_NOMEMBERSHIP");
        }        
        //End temporary code.  The entire section above should be deleted when 
        //when a fix is made for the DataAccess object.
        
        dataRequest.setInputParams(orderHeader);
  
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();


            Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
            status = (String) outputs.get(STATUS_PARAM);
            if(status.equals("N"))
            {
                message = (String) outputs.get(MESSAGE_PARAM);
                returnStatus = false;
                throw new Exception(message);
            }
            else
            {
                returnStatus = true;
            }
        }//end if changed

        return returnStatus;
    }

    private boolean updateOrderItems(OrderVO order) throws Exception
    {
        // update the order detail infomration

        DataRequest dataRequest = new DataRequest();

        HashMap orderDetail = null;
        Collection items = order.getOrderDetail();            
        OrderDetailsVO item = null;
        Iterator iterator = null;

        boolean doingUpdate = false;

        //only continue if there is something to update
        if(items == null) return true;
            
        iterator = items.iterator();
                                
        for (int i = 0; iterator.hasNext(); i++) 
        {
            item = (OrderDetailsVO)iterator.next();          

            if(item.isChanged()){

                      orderDetail = new HashMap();
                      
                      orderDetail.put("IN_UPDATED_BY" , userID);                 
                      orderDetail.put("IN_GUID",order.getGUID());
                      orderDetail.put("IN_LINE_NUMBER",formatToLong(item.getLineNumber()));
                      orderDetail.put("IN_RECIPIENT_ID",formatToLong(item.getRecipientId()));
                      orderDetail.put("IN_RECIPIENT_ADDRESS_ID",formatToLong((item.getRecipientAddressId())));
                      orderDetail.put("IN_PRODUCT_ID",item.getProductId());
                      orderDetail.put("IN_QUANTITY",formatToLong(item.getQuantity()));
                      orderDetail.put("IN_SHIP_VIA",item.getShipVia());
                      orderDetail.put("IN_SHIP_METHOD",item.getShipMethod());
                      orderDetail.put("IN_SHIP_DATE",getSQLDate(item.getShipDate()));
                      orderDetail.put("IN_DELIVERY_DATE",getSQLDate(item.getDeliveryDate()));
                      orderDetail.put("IN_DELIVERY_DATE_RANGE_END",getSQLDate(item.getDeliveryDateRangeEnd()));
                      orderDetail.put("IN_DROP_SHIP_TRACKING_NUMBER",item.getDropShipTrackingNumber());
                      orderDetail.put("IN_PRODUCTS_AMOUNT",formatToDouble(item.getProductsAmount()));
                      orderDetail.put("IN_TAX_AMOUNT",formatToDouble(item.getTaxAmount()));
                      orderDetail.put("IN_SERVICE_FEE_AMOUNT",formatToDouble(item.getServiceFeeAmount()));
                      orderDetail.put("IN_SHIPPING_FEE_AMOUNT",formatToDouble(item.getShippingFeeAmount()));
                      orderDetail.put("IN_ADD_ON_AMOUNT",formatToDouble(item.getAddOnAmount()));
                      orderDetail.put("IN_DISCOUNT_AMOUNT",formatToDouble(item.getDiscountAmount()));
                      orderDetail.put("IN_OCCASION_ID",formatToLong(item.getOccassionId()));
                      orderDetail.put("IN_LAST_MINUTE_GIFT_SIGNATURE",item.getLastMinuteGiftSignature());
                      orderDetail.put("IN_LAST_MINUTE_NUMBER",item.getLastMinuteNumber());
                      orderDetail.put("IN_LAST_MINUTE_GIFT_EMAIL",item.getLastMinuteGiftEmail());
                      orderDetail.put("IN_EXTERNAL_ORDER_NUMBER",item.getExternalOrderNumber());
                      orderDetail.put("IN_COLOR_1",item.getColorFirstChoice());
                      orderDetail.put("IN_COLOR_2",item.getColorSecondChoice());
                      orderDetail.put("IN_SUBSTITUTE_ACKNOWLEDGEMENT",item.getSubstituteAcknowledgement());
                      orderDetail.put("IN_CARD_MESSAGE",item.getCardMessage());
                      orderDetail.put("IN_CARD_SIGNATURE",item.getCardSignature());
                      orderDetail.put("IN_ORDER_COMMENTS",item.getOrderComments());
                      orderDetail.put("IN_ORDER_CONTACT_INFORMATION",item.getOrderContactInformation());
                      orderDetail.put("IN_FLORIST_NUMBER",item.getFloristNumber());
                      orderDetail.put("IN_SPECIAL_INSTRUCTIONS",item.getSpecialInstructions());
                      orderDetail.put("IN_ARIBA_UNSPSC_CODE",item.getAribaUnspscCode());
                      orderDetail.put("IN_ARIBA_PO_NUMBER",item.getAribaPoNumber());
                      orderDetail.put("IN_ARIBA_AMS_PROJECT_CODE",item.getAribaAmsProjectCode());
                      orderDetail.put("IN_ARIBA_COST_CENTER",item.getAribaCostCenter());
                      orderDetail.put("IN_EXTERNAL_ORDER_TOTAL",formatToDouble(item.getExternalOrderTotal()));
                      orderDetail.put("SIZE_INDICATOR",item.getSizeChoice());
                      orderDetail.put("IN_SENDER_INFO_RELEASE",item.getSenderInfoRelease());   
                      orderDetail.put("IN_STATUS",item.getStatus());
                      orderDetail.put("IN_MILES_POINTS",formatToDouble(item.getMilesPoints()));
                      orderDetail.put("IN_SUBCODE",item.getProductSubCodeId());
                      orderDetail.put(STATUS_PARAM,"");
                      orderDetail.put(MESSAGE_PARAM,"");

                      dataRequest.setStatementID(UPDATE_UNIQUE_ORDER_DETAILS);  

                      //This code is here because the DataAccess object currently does not
                      //support the insertion of nulls in Long fields.  This is a workaround,
                      //Sasha is deciding how this problem will be corrected.
                      if(item.getOccassionId() == null || item.getOccassionId().equals("0")){
                          orderDetail.put("IN_OCCASION_ID", Integer.valueOf(String.valueOf(java.sql.Types.VARCHAR)));
                        dataRequest.setStatementID("FRP.UPDATE_UNIQUE_ORDER_DETAILS_NOOCCASION");  
                      }
                      else
                      {
                        dataRequest.setStatementID(UPDATE_UNIQUE_ORDER_DETAILS);                          
                      }
                      
                      dataRequest.setConnection(connection);
                      dataRequest.setInputParams(orderDetail);
  
                      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
 
                      Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
                      status = (String) outputs.get(STATUS_PARAM);
                      if(status.equals("N"))
                      {
                          message = (String) outputs.get(MESSAGE_PARAM);
                          returnStatus = false;
                          throw new Exception(message);
                      }
                      else
                      {
                          BigDecimal id = (BigDecimal) outputs.get("OUT_ORDER_DETAIL_ID");
                          item.setOrderDetailId(id.longValue());
               
                          returnStatus = true;
                      }
            }//end is changed

        }
        return returnStatus;
    }

    private boolean updateAddOns(OrderVO order) throws Exception
    {

    
        // update the order add ons infomration

        DataRequest dataRequest = new DataRequest();

        HashMap orderAddOn = null;
        Collection items = order.getOrderDetail();            
        Collection addOns = null;
        OrderDetailsVO item = null;
        AddOnsVO addOn = null;
        Iterator iterator = null;
        Iterator addOnsIterator = null;
        boolean doingUpdate = false;   

        //only continue if there is something to update
        if(items == null) return true;
        
        iterator = items.iterator();
        long detailId = 0;
                                
        for (int i = 0; iterator.hasNext(); i++) 
        {
            item = (OrderDetailsVO)iterator.next();

            addOns = item.getAddOns();

            //delete all addons before inserting..emueller-12/17/03
            deleteAddons(item.getOrderDetailId());

            //Set addon list to empty for this item if it does not contain any addons
            if(addOns == null){
              addOns = new ArrayList();
            }
            
            addOnsIterator = addOns.iterator();
            detailId = item.getOrderDetailId();

            for (int x = 0; addOnsIterator.hasNext(); x++) 
            {
                addOn = (AddOnsVO)addOnsIterator.next();
                addOn.setOrderDetailId(detailId);

                //if object has not changed do not update it
                if(!addOn.isChanged()){
                    continue;
                }

                orderAddOn = new HashMap();

                orderAddOn.put("IN_UPDATED_BY" , userID);
                orderAddOn.put("IN_ORDER_DETAIL_ID",formatToLong(addOn.getOrderDetailId()));
                orderAddOn.put("IN_ADD_ON_CODE",addOn.getAddOnCode());
                orderAddOn.put("IN_ADD_ON_QUANTITY",formatToLong(addOn.getAddOnQuantity()));
                orderAddOn.put(STATUS_PARAM,"");
                orderAddOn.put(MESSAGE_PARAM,"");                

                dataRequest.setStatementID(INSERT_UNIQUE_ADD_ONS);  
                dataRequest.setConnection(connection);
                dataRequest.setInputParams(orderAddOn);
  
                DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
 
                Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
                status = (String) outputs.get(STATUS_PARAM);
                if(status.equals("N"))
                {
                    message = (String) outputs.get(MESSAGE_PARAM);
                    returnStatus = false;
                    throw new Exception(message);
                }
                else
                {

                        BigDecimal id = (BigDecimal) outputs.get("OUT_ADD_ON_ID");
                        addOn.setAddOnId(id.longValue());

                          
                    returnStatus = true;
                }
            }
        }
        return returnStatus;
    }


    private boolean updateDispositionss(OrderVO order) throws Exception
    {
        // update the order disposition infomration

        DataRequest dataRequest = new DataRequest();

        HashMap orderDispositionMap = null;
        Collection items = order.getOrderDetail();            
        Collection dispositions = null;
        OrderDetailsVO item = null;
        DispositionsVO disposition = null;
        Iterator iterator = null;
        Iterator dispositionIterator = null;

        //only continue if there is something to update
        if(items == null) return true;
        
        iterator = items.iterator();
        long detailId = 0;
                                
        for (int i = 0; iterator.hasNext(); i++) 
        {
            item = (OrderDetailsVO)iterator.next();

            dispositions = item.getDispositions();

            //only continue if there is something to update
            if(dispositions == null) return true;
            
            dispositionIterator = dispositions.iterator();
            detailId = item.getOrderDetailId();

            for (int x = 0; dispositionIterator.hasNext(); x++) 
            {
                disposition = (DispositionsVO)dispositionIterator.next();
                disposition.setOrderDetailID(detailId);

                //only insert if id is <= 0 ... a new disposition
                if(disposition.getOrderDispositionID() <= 0 ){

                if(disposition.isChanged()){
                    
                  orderDispositionMap = new HashMap();
                  orderDispositionMap.put("IN_UPDATED_BY" , userID);
                  orderDispositionMap.put("IN_ORDER_DETAIL_ID",formatToLong(disposition.getOrderDetailID()));
                  orderDispositionMap.put("IN_DISPOSITION_ID",disposition.getDispositionID());
                  orderDispositionMap.put("IN_COMMENTS",disposition.getComments());
                  orderDispositionMap.put("IN_CALLED_CUSTOMER_FLAG",disposition.getCalledCustomerFlag());
                  orderDispositionMap.put("IN_SENT_EMAIL_FLAG",disposition.getSentEmailFlag());
                  orderDispositionMap.put("IN_STOCK_MESSAGE_ID",disposition.getStockMessageID());                
                  orderDispositionMap.put("IN_EMAIL_SUBJECT",disposition.getEmailSubject());                
                  orderDispositionMap.put("IN_EMAIL_MESSAGE",disposition.getEmailMessage());                                
                  orderDispositionMap.put("IO_ORDER_DISPOSITION_ID",new Integer(java.sql.Types.INTEGER));                                
                  orderDispositionMap.put(STATUS_PARAM,"");
                  orderDispositionMap.put(MESSAGE_PARAM,"");                

                  dataRequest.setStatementID(INSERT_ORDER_DISPOSITION);  
                  dataRequest.setConnection(connection);
                  dataRequest.setInputParams(orderDispositionMap);
  
                  DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
 
                  Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
                  status = (String) outputs.get(STATUS_PARAM);
                  if(status.equals("N"))
                  {
                      message = (String) outputs.get(MESSAGE_PARAM);
                      returnStatus = false;
                      throw new Exception(message);
                  }
                  else
                  {

                          BigDecimal id = (BigDecimal) outputs.get("IO_ORDER_DISPOSITION_ID");
                          disposition.setOrderDispositionID(id.longValue());

                          
                      returnStatus = true;
                  }
                }//end changed
                }
            }
        }
        return returnStatus;
    }

    private boolean updateRecipient(OrderVO order) throws Exception
    {
        // update the order recipients infomration
        returnStatus = true;
        DataRequest dataRequest = new DataRequest();

        HashMap orderRecipient = null;
        List items = order.getOrderDetail();            
        List recipients = null;
        OrderDetailsVO item = null;
        RecipientsVO recipient = null;
        Iterator iterator = null;
        Iterator recipientsIterator = null;
        boolean doingUpdate = false;   

        //only continue if there is something to update
        if(items == null) return true;
        
        iterator = items.iterator();
                                
        for (int i = 0; iterator.hasNext(); i++) 
        {
            item = (OrderDetailsVO)iterator.next();

            recipients = item.getRecipients();
            if(recipients != null && recipients.size() > 0){

                recipient = (RecipientsVO)recipients.get(0);

                //get address
                List addressList = recipient.getRecipientAddresses();
                RecipientAddressesVO addressVO = new RecipientAddressesVO();
                String address1 = null;
                String address2 = null;
                String addressType = null;
                String city = null;
                String country = null;
                String county = null;
                String geo = null;
                String intl = null;
                String latitude = null;
                String longitude = null;
                String state=null;
                String zip=null;                
                String destName = null;
                String destInfo = null;
                if(addressList != null && addressList.size() >0)
                {
                  addressVO = (RecipientAddressesVO)addressList.get(0);
                  address1=addressVO.getAddressLine1();
                  address2=addressVO.getAddressLine2();
                  addressType=addressVO.getAddressType();
                  city=addressVO.getCity();
                  country=addressVO.getCountry();
                  county = addressVO.getCounty();
                  geo=addressVO.getGeofindMatchCode();
                  intl=addressVO.getInternational();
                  latitude=addressVO.getLatitude();
                  longitude=addressVO.getLongitude();
                  state=addressVO.getStateProvince();
                  zip=addressVO.getPostalCode();
                  destName = addressVO.getName();
                  destInfo = addressVO.getInfo();
                }


               //only if recipient changes
               if(recipient.isChanged() || addressVO.isChanged())
               {
                 
                  
                orderRecipient = new HashMap();

                orderRecipient.put("IN_UPDATED_BY" , userID);
                orderRecipient.put("IN_LAST_NAME",recipient.getLastName());
                orderRecipient.put("IN_FIRST_NAME",recipient.getFirstName());
                orderRecipient.put("IN_LIST_CODE",recipient.getListCode());
                orderRecipient.put("IN_AUTO_HOLD",recipient.getAutoHold());
                orderRecipient.put("IN_MAIL_LIST_CODE",recipient.getMailListCode());
                orderRecipient.put("IN_STATUS",recipient.getStatus());
                orderRecipient.put("IN_ADDRESS_TYPE",addressType);
                orderRecipient.put("IN_ADDRESS_LINE_1",address1);
                orderRecipient.put("IN_ADDRESS_LINE_2",address2);
                orderRecipient.put("IN_CITY",city);
                orderRecipient.put("IN_STATE_PROVINCE",state);
                orderRecipient.put("IN_POSTAL_CODE",zip);
                orderRecipient.put("IN_COUNTRY",country);
                orderRecipient.put("IN_COUNTY",county);
                orderRecipient.put("IN_GEOFIND_MATCH_CODE", geo);
                orderRecipient.put("IN_LATITUDE",formatToLong(latitude));
                orderRecipient.put("IN_LONGITUDE",formatToLong(longitude));                      
                orderRecipient.put("IN_INTERNATIONAL",intl);
                orderRecipient.put("IN_NAME",destName);
                orderRecipient.put("IN_INFO",destInfo);
                orderRecipient.put(STATUS_PARAM,"");
                orderRecipient.put(MESSAGE_PARAM,"");

                dataRequest.setStatementID(INSERT_UNIQUE_RECIPIENT);
                dataRequest.setConnection(connection);
                dataRequest.setInputParams(orderRecipient);
  
                DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
 
                Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
                status = (String) outputs.get(STATUS_PARAM);
                if(status.equals("N"))
                {
                    message = (String) outputs.get(MESSAGE_PARAM);
                    returnStatus = false;
                    throw new Exception(message);
                }
                else
                {
                    long recipID = formatToNativeLong(getValue(outputs.get("OUT_RECIPIENT_ID")));
                    long addrID = formatToNativeLong(getValue(outputs.get("OUT_RECIPIENT_ADDRESS_ID")));
                    addressVO.setRecipientAddressId(addrID);
                    addressVO.setRecipientId(recipID);
                    recipient.setRecipientId(recipID);
                    item.setRecipientAddressId(addrID);
                    item.setRecipientId(recipID);

                    returnStatus = true;
                }

               }//end if changed
            }
        }
        return returnStatus;
    }

 
    private boolean updateCoBrand(OrderVO order) throws Exception
    {
        // update the order co brand infomration

        DataRequest dataRequest = new DataRequest();
        boolean doingUpdate = false;   
        HashMap orderCoBrand = null;
        Collection coBrands = order.getCoBrand();            
        CoBrandVO coBrand = null;
        Iterator iterator = null;

        //only continue if there is something to update
        if(coBrands == null) return true;
                
        iterator = coBrands.iterator();
                                
        for (int i = 0; iterator.hasNext(); i++) 
        {
            coBrand = (CoBrandVO)iterator.next();

            if(coBrand.isChanged()){

            orderCoBrand = new HashMap();

            orderCoBrand.put("IN_UPDATED_BY" , userID);
            orderCoBrand.put("IN_GUID",order.getGUID());
            orderCoBrand.put("IN_INFO_NAME",coBrand.getInfoName());
            orderCoBrand.put("IN_INFO_DATA",coBrand.getInfoData());
            orderCoBrand.put(STATUS_PARAM,"");
            orderCoBrand.put(MESSAGE_PARAM,"");

            dataRequest.setStatementID(UPDATE_UNIQUE_CO_BRAND);  
            dataRequest.setConnection(connection);
            dataRequest.setInputParams(orderCoBrand);
  
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
 
            Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
            status = (String) outputs.get(STATUS_PARAM);
            if(status.equals("N"))
            {
                message = (String) outputs.get(MESSAGE_PARAM);
                returnStatus = false;
                throw new Exception(message);
            }
            else
            {

                    BigDecimal id = (BigDecimal) outputs.get("OUT_CO_BRAND_ID");
                    coBrand.setCoBrandId(id.longValue());

                    
                returnStatus = true;
            }
            }//end if changed
        }
        return returnStatus;
    }

    private boolean updatePayments(OrderVO order) throws Exception
    {
        // update the order payments infomration

        DataRequest dataRequest = new DataRequest();
        boolean doingUpdate = false;   
        HashMap orderPayment = null;
        Collection payments = order.getPayments();            
        PaymentsVO payment = null;
        Iterator iterator = null;

        //only continue if there is something to update
        if(payments == null) return true;

        //only continue if a payment object has changed
        //this prevents us from deactivating payments if nothing has changed
        iterator = payments.iterator();                                
        boolean changed = false;
        for (int i = 0; iterator.hasNext(); i++) 
        {
            payment = (PaymentsVO)iterator.next();
            if(payment.isChanged()){
              changed = true;
            }
        }
        if(!changed)
        {
          return true;
        }
        

        //if there are payments set them to inactive
        if(payments.size() > 0)
        {
            DataRequest dataRequestInactive = new DataRequest();          
            Map inactiveMap = new HashMap();            
            inactiveMap.put("IN_UPDATED_BY" , userID);
            inactiveMap.put("IN_GUID",order.getGUID());
            inactiveMap.put(STATUS_PARAM,"");
            inactiveMap.put(MESSAGE_PARAM,""); 
            dataRequestInactive.setStatementID(INACTIVATE_PAYMENTS);                          
            DataAccessUtil dataAccessUtilInactive = DataAccessUtil.getInstance();
            dataRequestInactive.setConnection(connection);
            dataRequestInactive.setInputParams(inactiveMap);
            Map outputsInactive = (Map) dataAccessUtilInactive.execute(dataRequestInactive);
      
            status = (String) outputsInactive.get(STATUS_PARAM);
            if(status.equals("N"))
            {
                message = (String) outputsInactive.get(MESSAGE_PARAM);
                returnStatus = false;
                throw new Exception(message);
            }
        }
        
                
        iterator = payments.iterator();
                                
        for (int i = 0; iterator.hasNext(); i++) 
        {
            payment = (PaymentsVO)iterator.next();

            if(payment.isChanged()){

            orderPayment = new HashMap();

            orderPayment.put("IN_UPDATED_BY" , userID);
            orderPayment.put("IN_GUID",order.getGUID());
            orderPayment.put("IN_PAYMENT_TYPE",payment.getPaymentType());
            orderPayment.put("IN_AMOUNT",formatToDouble(payment.getAmount()));
            orderPayment.put("IN_CREDIT_CARD_ID",formatToLong(payment.getCreditCardId()));
            orderPayment.put("IN_GIFT_CERTIFICATE_ID",payment.getGiftCertificateId());
            orderPayment.put("IN_AUTH_RESULT",payment.getAuthResult());
            orderPayment.put("IN_AUTH_NUMBER",payment.getAuthNumber());
            orderPayment.put("IN_AVS_CODE",payment.getAvsCode());
            orderPayment.put("IN_ACQ_REFERENCE_NUMBER",payment.getAcqReferenceNumber());
            orderPayment.put("IN_AAFES_TICKET",payment.getAafesTicket());
            orderPayment.put("IN_INVOICE_NUMBER",payment.getInvoiceNumber());
            orderPayment.put("OUT_PAYMENT_ID", Integer.valueOf(String.valueOf(java.sql.Types.VARCHAR)));

            orderPayment.put(STATUS_PARAM,"");
            orderPayment.put(MESSAGE_PARAM,"");


            //This code is here because the DataAccess object currently does not
            //support the insertion of nulls in Long fields.  This is a workaround,
            //Sasha is deciding how this problem will be corrected.
            if(payment.getCreditCardId() == 0){
                orderPayment.put("IN_CREDIT_CARD_ID", Integer.valueOf(String.valueOf(java.sql.Types.VARCHAR)));
                dataRequest.setStatementID("FRP.INSERT_PAYMENTS_NOCREDITCARD");
            }
            else
            {
                dataRequest.setStatementID(INSERT_PAYMENTS);              
            }

            dataRequest.setConnection(connection);
            dataRequest.setInputParams(orderPayment);
  
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
 
            Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
            status = (String) outputs.get(STATUS_PARAM);
            if(status.equals("N"))
            {
                message = (String) outputs.get(MESSAGE_PARAM);
                returnStatus = false;
                throw new Exception(message);
            }
            else
            {

                    BigDecimal id = (BigDecimal) outputs.get("OUT_PAYMENT_ID");
                    payment.setPaymentId(id.longValue());

                returnStatus = true;
            }
            } //changed
        }
        return returnStatus;
    }



    private boolean updateCreditCards(OrderVO order) throws Exception
    {
        // update the order credit cardsinfomration

        DataRequest dataRequest = new DataRequest();
        boolean doingUpdate = false;   
        HashMap orderCreditCard = null;
        Collection payments = order.getPayments();            
        Collection creditCards = null;
        PaymentsVO payment = null;
        CreditCardsVO creditCard = null;
        Iterator iterator = null;
        Iterator creditCardsIterator = null;

        //only continue if there is something to update
        if(payments == null) return true;
                
        iterator = payments.iterator();
        long buyerId = 0;
                                
        for (int i = 0; iterator.hasNext(); i++) 
        {
            payment = (PaymentsVO)iterator.next();

            creditCards = payment.getCreditCards();

            //only continue if there is something to update
            if(creditCards == null) return true;
            
            creditCardsIterator = creditCards.iterator();

            for (int x = 0; creditCardsIterator.hasNext(); x++) 
            {
                creditCard = (CreditCardsVO)creditCardsIterator.next();


                creditCard.setBuyerId(order.getBuyerId());

                if(creditCard.isChanged()){
                    
                orderCreditCard = new HashMap();

                orderCreditCard.put("IN_UPDATED_BY" , userID);
                orderCreditCard.put("IN_BUYER_ID",formatToLong(creditCard.getBuyerId()));
                orderCreditCard.put("IN_CC_TYPE",creditCard.getCCType());
                orderCreditCard.put("IN_CC_NUMBER",creditCard.getCCNumber());
                orderCreditCard.put("IN_CC_EXPIRATION",getSQLDate(creditCard.getCCExpiration()));
                orderCreditCard.put(STATUS_PARAM,"");
                orderCreditCard.put(MESSAGE_PARAM,"");

                dataRequest.setStatementID(UPDATE_UNIQUE_CREDIT_CARDS);  
                dataRequest.setConnection(connection);
                dataRequest.setInputParams(orderCreditCard);
  
                DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
 
                Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
                status = (String) outputs.get(STATUS_PARAM);
                if(status.equals("N"))
                {
                    message = (String) outputs.get(MESSAGE_PARAM);
                    returnStatus = false;
                    throw new Exception(message);
                }
                else
                {

                        BigDecimal id = (BigDecimal) outputs.get("OUT_CREDIT_CARD_ID");
                        creditCard.setCreditCardId(id.longValue());
                        payment.setCreditCardId(id.longValue());

                    returnStatus = true;
                }
                }//end is changed
            }
        }
        return returnStatus;
    }



    /* This method updates BUYER, BUYER_ADDRESSES, BUYER_EMAILS, MEMBERSHIPS. 
     * There can only be 1 buyer with 1 address, 1 email, 1 membership*/
    private boolean updateBuyer(OrderVO order) throws Exception
    {
        // update the order co brand infomration

        DataRequest dataRequest = new DataRequest();
        boolean doingUpdate = false;   
        HashMap orderBuyer = null;
        List buyers = order.getBuyer();            
        BuyerVO buyer = null;
                
        //if no buyers exist
        if(buyers == null || buyers.size() <=0)
        {
          return true;
        }

        //get buyer
        buyer = (BuyerVO)buyers.get(0);

        //emails
        List emails = buyer.getBuyerEmails();
        String emailPrimary = null;
        String emailNewsletter = null;
        String emailEmail = null;
        BuyerEmailsVO emailVO = new BuyerEmailsVO();
        if(emails !=null && emails.size() > 0)
        {
          emailVO = (BuyerEmailsVO)emails.get(0);
          emailPrimary = emailVO.getPrimary();
          emailNewsletter = emailVO.getNewsletter();
          emailEmail = emailVO.getEmail();
        }

        //addresses
        List addresses = buyer.getBuyerAddresses();
        String addressType = null;
        String addressLine1 = null;
        String addressLine2 = null;
        String city = null;
        String stateProv = null;
        String postalCode = null;
        String country = null;
        String county = null;
        String addressEtc = null;
        BuyerAddressesVO buyerAddressVO = new BuyerAddressesVO();
        if(addresses !=null && addresses.size() > 0)
        {
          buyerAddressVO = (BuyerAddressesVO)addresses.get(0);
          addressType = buyerAddressVO.getAddressType();
          addressLine1 = buyerAddressVO.getAddressLine1();
          addressLine2 = buyerAddressVO.getAddressLine2();
          city = buyerAddressVO.getCity();
          stateProv = buyerAddressVO.getStateProv();
          postalCode = buyerAddressVO.getPostalCode();
          country = buyerAddressVO.getCountry();
          county = buyerAddressVO.getCounty();
          addressEtc = buyerAddressVO.getAddressEtc();
        }

        //memberships
        List memberships = order.getMemberships();
        String membershipID = null;
        String membershipFirstName = null;
        String membershipLastName = null;
        String membershipType = null;
        MembershipsVO membershipVO = new MembershipsVO();
        if(memberships != null && memberships.size() > 0)
        {          
          membershipVO = (MembershipsVO)memberships.get(0);
          membershipID = membershipVO.getMembershipIdNumber();
          membershipFirstName = membershipVO.getFirstName();
          membershipLastName = membershipVO.getLastName();
          membershipType =membershipVO.getMembershipType();
        }

        //if changed
        if(membershipVO.isChanged() || buyerAddressVO.isChanged() || emailVO.isChanged()){

          orderBuyer = new HashMap();

          orderBuyer.put("IN_UPDATED_BY" , userID);
          orderBuyer.put("IN_CUSTOMER_ID",buyer.getCustomerId());
          orderBuyer.put("IN_LAST_NAME",buyer.getLastName());
          orderBuyer.put("IN_FIRST_NAME",buyer.getFirstName());
          orderBuyer.put("IN_AUTO_HOLD",buyer.getAutoHold());
          orderBuyer.put("IN_BEST_CUSTOMER",buyer.getBestCustomer());
          orderBuyer.put("IN_STATUS" , buyer.getStatus());
          orderBuyer.put("IN_ADDRESS_TYPE" , addressType);
          orderBuyer.put("IN_ADDRESS_LINE_1" , addressLine1);
          orderBuyer.put("IN_ADDRESS_LINE_2" , addressLine2);
          orderBuyer.put("IN_CITY" , city);
          orderBuyer.put("IN_STATE_PROVINCE" , stateProv);
          orderBuyer.put("IN_POSTAL_CODE" , postalCode);
          orderBuyer.put("IN_COUNTRY" , country);
          orderBuyer.put("IN_COUNTY" , county);
          orderBuyer.put("IN_ADDRESS_ETC" , addressEtc);          
          orderBuyer.put("IN_EMAIL" , emailEmail);
          orderBuyer.put("IN_PRIMARY" , emailPrimary);
          orderBuyer.put("IN_NEWSLETTER" , emailNewsletter);          
          orderBuyer.put("IN_MEMBERSHIP_TYPE" , membershipType);
          orderBuyer.put("IN_MEMBERSHIP_LAST_NAME" , membershipLastName);
          orderBuyer.put("IN_MEMBERSHIP_FIRST_NAME" , membershipFirstName);
          orderBuyer.put("IN_MEMBERSHIP_ID_NUMBER" , membershipID);          
          orderBuyer.put(STATUS_PARAM,"");
          orderBuyer.put(MESSAGE_PARAM,"");

          dataRequest.setStatementID(INSERT_UNIQUE_BUYER);
          dataRequest.setConnection(connection);
          dataRequest.setInputParams(orderBuyer);
  
          DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
 
          Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
          status = (String) outputs.get(STATUS_PARAM);
          if(status.equals("N"))
          {
              message = (String) outputs.get(MESSAGE_PARAM);
              returnStatus = false;
              throw new Exception(message);
          }
          else
          {
                long buyerID = formatToNativeLong(getValue(outputs.get("OUT_BUYER_ID")));
                long addressID = formatToNativeLong(getValue(outputs.get("OUT_BUYER_ADDRESS_ID")));
                long emailID = formatToNativeLong(getValue(outputs.get("OUT_BUYER_EMAIL_ID")));
                long memberID = formatToNativeLong(getValue(outputs.get("OUT_MEMBERSHIP_ID")));                

                logger.debug("Buyer inserted using id:" + buyerID);

                membershipVO.setMembershipId(memberID);
                buyerAddressVO.setBuyerAddressId(addressID);
                emailVO.setBuyerEmailId(emailID);
                buyer.setBuyerId(buyerID);

                order.setBuyerId(buyerID);
                order.setBuyerEmailId(emailID);
                order.setMembershipId(memberID);
              }

              }//end if changed     

              return true;
          }


    /* Update contacts*/
    private boolean updateContacts(OrderVO order) throws Exception
    {
        // update the order co brand infomration

        DataRequest dataRequest = new DataRequest();

        HashMap orderContact = null;
        List contacts = order.getOrderContactInfo();
        OrderContactInfoVO contact = null;
                
        //if no buyers exist
        if(contacts == null || contacts.size() <=0)
        {
          return true;
        }

        //for each contact
        for (int i = 0; i < contacts.size(); i++) 
        {
            //get contact
            contact = (OrderContactInfoVO)contacts.get(0);             

            if(contact.isChanged()){

            orderContact = new HashMap();
            orderContact.put("IN_UPDATED_BY" , userID);
            orderContact.put("IN_ORDER_GUID",contact.getOrderGuid());
            orderContact.put("IN_FIRST_NAME",contact.getFirstName());
            orderContact.put("IN_LAST_NAME",contact.getLastName());
            orderContact.put("IN_PHONE" , contact.getPhone());
            orderContact.put("IN_EXT" , contact.getExt());            
            orderContact.put("IN_EMAIL" , contact.getEmail());
            orderContact.put(STATUS_PARAM,"");
            orderContact.put(MESSAGE_PARAM,"");

          dataRequest.setStatementID(INSERT_UNIQUE_CONTACT);
          dataRequest.setConnection(connection);
          dataRequest.setInputParams(orderContact);
  
          DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
 
          Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
          status = (String) outputs.get(STATUS_PARAM);
          if(status.equals("N"))
          {
              message = (String) outputs.get(MESSAGE_PARAM);
              returnStatus = false;
              throw new Exception(message);
          }
          else
          {
                long contactID = formatToNativeLong(getValue(outputs.get("OUT_CONTACT_ID")));
                contact.setOrderContactInfoId(contactID);

              }

            }//end has changed
       
        }
              return true;
          }


    




    private boolean updateBuyerPhones(OrderVO order) throws Exception
    {
        // update the buyer phones infomration

        DataRequest dataRequest = new DataRequest();
        boolean doingUpdate = false;   
        HashMap orderBuyerPhone = null;
        Collection buyers = order.getBuyer();            
        Collection buyerPhones = null;
        BuyerVO buyer = null;
        BuyerPhonesVO buyerPhone = null;
        Iterator iterator = null;
        Iterator buyerPhonesIterator = null;

        //only continue if there is something to update
        if(buyers == null) return true;
                
        iterator = buyers.iterator();
        long buyerId = 0;
                                
        for (int i = 0; iterator.hasNext(); i++) 
        {
            buyer = (BuyerVO)iterator.next();

            buyerPhones = buyer.getBuyerPhones();

            //only continue if there is something to update
            if(buyerPhones == null) return true;
            
            buyerPhonesIterator = buyerPhones.iterator();
            buyerId = buyer.getBuyerId();

            logger.debug("Inserting into BuyerPhones using id:" + buyerId);

            for (int x = 0; buyerPhonesIterator.hasNext(); x++) 
            {
                buyerPhone = (BuyerPhonesVO)buyerPhonesIterator.next();
                buyerPhone.setBuyerId(buyerId);

                if(buyerPhone.isChanged()){
                
                orderBuyerPhone = new HashMap();

                orderBuyerPhone.put("IN_UPDATED_BY" , userID);
                orderBuyerPhone.put("IN_BUYER_ID",formatToLong(buyerPhone.getBuyerId()));
                orderBuyerPhone.put("IN_PHONE_TYPE",buyerPhone.getPhoneType());
                orderBuyerPhone.put("IN_PHONE_NUMBER",buyerPhone.getPhoneNumber());
                orderBuyerPhone.put("IN_EXTENSION",buyerPhone.getExtension());
                orderBuyerPhone.put("IN_PHONE_NUMBER_TYPE",buyerPhone.getPhoneNumberType());
                orderBuyerPhone.put("IN_SMS_OPT_IN",buyerPhone.getSmsOptIn());
                orderBuyerPhone.put(STATUS_PARAM,"");
                orderBuyerPhone.put(MESSAGE_PARAM,"");

                dataRequest.setStatementID(UPDATE_UNIQUE_BUYER_PHONES);
  
                dataRequest.setConnection(connection);
                dataRequest.setInputParams(orderBuyerPhone);
  
                DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
 
                Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
                status = (String) outputs.get(STATUS_PARAM);
                if(status.equals("N"))
                {
                    message = (String) outputs.get(MESSAGE_PARAM);
                    returnStatus = false;
                    throw new Exception(message);
                }
                else
                {

                    long id = formatToNativeLong(getValue(outputs.get("IO_BUYER_PHONE_ID")));
                    buyerPhone.setBuyerPhoneId(id);

                    returnStatus = true;
                }

                }//end has changed
            }
        }
        return returnStatus;
    }

    /* CURRENTLY NOT BEING USED

    *private boolean updateBuyerNcoas(OrderVO order, boolean forceInsert) throws Exception
    {
        // update the buyer ncoa infomration

        DataRequest dataRequest = new DataRequest();
        boolean doingUpdate = false;   
        HashMap ncoaMap = null;
        Collection buyers = order.getBuyer();            
        Collection buyerNcoas = null;
        BuyerVO buyer = null;
        NcoaVO ncoa = null;
        Iterator iterator = null;
        Iterator buyerNcoaIterator = null;
                
        iterator = buyers.iterator();
        long buyerId = 0;
                                
        for (int i = 0; iterator.hasNext(); i++) 
        {
            buyer = (BuyerVO)iterator.next();

            buyerNcoas = buyer.getNcoa();
            buyerNcoaIterator = buyerNcoas.iterator();
            buyerId = buyer.getBuyerId();

            for (int x = 0; buyerNcoaIterator.hasNext(); x++) 
            {
                ncoa = (NcoaVO)buyerNcoaIterator.next();
                ncoa.setBuyerId(buyerId);

                //check if need insert or update
                doingUpdate = (ncoa.getNcoaId() > 0 && !forceInsert);
                    
                ncoaMap = new HashMap();

                ncoaMap.put("IN_UPDATED_BY" , user);
                ncoaMap.put("IN_BUYER_ID",formatToLong(ncoa.getBuyerId()));
                ncoaMap.put("IN_DELIVERABILITY",ncoa.getDeliverability());
                ncoaMap.put("IN_NIXIE_GROUP",ncoa.getNixieGroup());
                ncoaMap.put("IN_DUPE_SET_UNIQUENESS",ncoa.getDupeSetUniqueness());
                ncoaMap.put("IN_EMS_ADDRESS_SCORE",ncoa.getEmsAddressScore());
                ncoaMap.put(STATUS_PARAM,"");
                ncoaMap.put(MESSAGE_PARAM,"");

                //set values based on what is being done
                if(doingUpdate){
                    ncoaMap.put("IN_NCOA_ID",formatToLong(ncoa.getNcoaId()));
                    dataRequest.setStatementID(UPDATE_NCOA);
                }
                else
                {
                    ncoaMap.put("IO_NCOA_ID",new Integer(java.sql.Types.INTEGER));                        
                    dataRequest.setStatementID(INSERT_NCOA);
                }
  
                dataRequest.setConnection(connection);
                dataRequest.setInputParams(ncoaMap);
  
                DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
 
                Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
                status = (String) outputs.get(STATUS_PARAM);
                if(status.equals("N"))
                {
                    message = (String) outputs.get(MESSAGE_PARAM);
                    returnStatus = false;
                    throw new Exception(message);
                }
                else
                {
                    //if doing insert
                    if(!doingUpdate){
                        BigDecimal id = (BigDecimal) outputs.get("IO_NCOA_ID");
                        ncoa.setNcoaId(id.longValue());
                    }                
                    returnStatus = true;
                }
            }
        }
        return returnStatus;
    }  */





//************ start insert commands

    private boolean insertOrderHeader(OrderVO order) throws Exception
    {


    
        // update the order header infomration
        DataRequest dataRequest = new DataRequest();

        HashMap orderHeader = new HashMap();

        orderHeader.put("IN_UPDATED_BY" , userID);
        orderHeader.put("IN_GUID" , order.getGUID());
        orderHeader.put("IN_MASTER_ORDER_NUMBER" , order.getMasterOrderNumber());
        orderHeader.put("IN_BUYER_ID" , formatToLong(order.getBuyerId()));
        orderHeader.put("IN_ORDER_TOTAL" , formatToDouble(order.getOrderTotal()));
        orderHeader.put("IN_PRODUCTS_TOTAL" , formatToDouble(order.getProductsTotal()));
        orderHeader.put("IN_TAX_TOTAL" , formatToDouble(order.getTaxTotal()));
        orderHeader.put("IN_SERVICE_FEE_TOTAL" , formatToDouble(order.getServiceFeeTotal()));
        orderHeader.put("IN_SHIPPING_FEE_TOTAL" , formatToDouble(order.getShippingFeeTotal()));
        orderHeader.put("IN_BUYER_EMAIL_ID" , formatToLong(order.getBuyerEmailId()));
        orderHeader.put("IN_ORDER_DATE" , getSQLTimestamp(order.getOrderDate()));
        orderHeader.put("IN_ORDER_ORIGIN" , order.getOrderOrigin());
        orderHeader.put("IN_MEMBERSHIP_ID" , formatToLong(order.getMembershipId()));
        orderHeader.put("IN_CSR_ID" , order.getCsrId());
        orderHeader.put("IN_ADD_ON_AMOUNT_TOTAL" , formatToDouble(order.getAddOnAmountTotal()));
        orderHeader.put("IN_PARTNERSHIP_BONUS_POINTS" , formatToDouble(order.getPartnershipBonusPoints()));
        orderHeader.put("IN_SOURCE_CODE" , order.getSourceCode());
        orderHeader.put("IN_CALL_TIME" , formatToLong(order.getCallTime()));
        orderHeader.put("IN_DNIS_CODE" , order.getDnisCode());
//        orderHeader.put("IN_DATAMART_UPDATE_DATE" , getSQLDate(order.getDatamartUpdateDate()));
        orderHeader.put("IN_DISCOUNT_TOTAL" , formatToDouble(order.getDiscountTotal()));
        orderHeader.put("IN_YELLOW_PAGES_CODE" , order.getYellowPagesCode());
        orderHeader.put("IN_ARIBA_BUYER_ASN_NUMBER" , order.getAribaBuyerAsnNumber());
        orderHeader.put("IN_ARIBA_BUYER_COOKIE" , order.getAribaBuyerCookie());
        orderHeader.put("IN_ARIBA_PAYLOAD" , order.getAribaPayload());
        orderHeader.put("IN_STATUS" , order.getStatus());
        orderHeader.put("IN_FRAUD" , order.getFraudFlag());

        orderHeader.put("IN_CO_BRAND_CREDIT_CARD_CODE" , order.getCoBrandCreditCardCode());

        orderHeader.put(STATUS_PARAM,"");
        orderHeader.put(MESSAGE_PARAM,"");
  
        dataRequest.setConnection(connection);
        dataRequest.setStatementID(INSERT_ORDER_HEADER);

        //This code is here because the DataAccess object currently does not
        //support the insertion of nulls in Long fields.  This is a workaround,
        //Sasha is deciding how this problem will be corrected.
        if((order.getBuyerEmailId() <=0) &&
              order.getMembershipId() <=0){
            orderHeader.put("IN_BUYER_EMAIL_ID", Integer.valueOf(String.valueOf(java.sql.Types.VARCHAR)));
            orderHeader.put("IN_MEMBERSHIP_ID", Integer.valueOf(String.valueOf(java.sql.Types.VARCHAR)));
            dataRequest.setStatementID("FRP.INSERT_ORDER_HEADER_NOBUYEREMAILORMEMBERSHIP");
        }
        else if(order.getBuyerEmailId() <=0) {
            orderHeader.put("IN_BUYER_EMAIL_ID", Integer.valueOf(String.valueOf(java.sql.Types.VARCHAR)));
            dataRequest.setStatementID("FRP.INSERT_ORDER_HEADER_NOBUYEREMAIL");
        }
        else if(order.getMembershipId() <=0){
            orderHeader.put("IN_MEMBERSHIP_ID", Integer.valueOf(String.valueOf(java.sql.Types.VARCHAR)));
            dataRequest.setStatementID("FRP.INSERT_ORDER_HEADER_NOMEMBERSHIP");
        }        
        //End temporary code.  The entire section above should be deleted when 
        //when a fix is made for the DataAccess object.
        
        dataRequest.setInputParams(orderHeader);
  
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();


            Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
            status = (String) outputs.get(STATUS_PARAM);
            if(status.equals("N"))
            {
                message = (String) outputs.get(MESSAGE_PARAM);
                returnStatus = false;
                throw new Exception(message);
            }
            else
            {
                returnStatus = true;
            }

        return returnStatus;
    }

    
    private boolean updateRecipientPhones(OrderVO order) throws Exception
    {
        // update the recipient phones infomration

        DataRequest dataRequest = new DataRequest();
        boolean doingUpdate = false;   
        HashMap orderRecipientPhone = null;
        Collection items = order.getOrderDetail();            
        Collection recipients = null;
        Collection recipientPhones = null;
        OrderDetailsVO item = null;
        RecipientsVO recipient = null;
        RecipientPhonesVO recipientPhone = null;
        Iterator iterator = null;
        Iterator recipientIterator = null;
        Iterator recipientPhonesIterator = null;
        long recipientId = 0;        

        //only continue if there is something to update
        if(items == null) return true;
            
        iterator = items.iterator();
                                
        for (int i = 0; iterator.hasNext(); i++) 
        {
            item = (OrderDetailsVO)iterator.next();
            recipients = item.getRecipients();

            //only continue if there is something to update
            if(recipients == null) return true;
            
            recipientIterator = recipients.iterator();
        
            for (int x = 0; recipientIterator.hasNext(); x++) 
            {
                recipient = (RecipientsVO)recipientIterator.next();
                recipientPhones = recipient.getRecipientPhones();

                //only continue if there is something to update
                if(recipientPhones == null) return true;
                
                recipientPhonesIterator = recipientPhones.iterator();
                recipientId = recipient.getRecipientId();
                
                  for (int y = 0; recipientPhonesIterator.hasNext(); y++) 
                  {
                      recipientPhone = (RecipientPhonesVO)recipientPhonesIterator.next();

                    
                      orderRecipientPhone = new HashMap();

                      recipientPhone.setRecipientId(recipientId);

                      if(recipientPhone.isChanged()){

                      orderRecipientPhone.put("IN_UPDATED_BY" , userID);
                      orderRecipientPhone.put("IN_RECIPIENT_ID",formatToLong(recipientId));
                      orderRecipientPhone.put("IN_PHONE_TYPE",recipientPhone.getPhoneType());
                      orderRecipientPhone.put("IN_PHONE_NUMBER",recipientPhone.getPhoneNumber());
                      orderRecipientPhone.put("IN_EXTENSION",recipientPhone.getExtension());
                      orderRecipientPhone.put(STATUS_PARAM,"");
                      orderRecipientPhone.put(MESSAGE_PARAM,"");

                      dataRequest.setStatementID(UPDATE_UNIQUE_RECIPIENT_PHONES);  
                      dataRequest.setConnection(connection);
                      dataRequest.setInputParams(orderRecipientPhone);
  
                      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
 
                      Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
                      status = (String) outputs.get(STATUS_PARAM);
                      if(status.equals("N"))
                      {
                          message = (String) outputs.get(MESSAGE_PARAM);
                          returnStatus = false;
                          throw new Exception(message);
                      }
                      else
                      {
                          long id = formatToNativeLong(getValue(outputs.get("IO_RECIPIENT_PHONE_ID")));
                          recipientPhone.setRecipientPhoneId(id);

                          returnStatus = true;
                      }
                      }//changed
                  }
            }   
        }
        return returnStatus;
    }
    

    private boolean updateFraudComments(OrderVO order) throws Exception
    {
        // update the fraud comments information

        DataRequest dataRequest = new DataRequest();

        HashMap orderCommentMap = null;
        Collection fraudComments = order.getFraudComments();
        FraudCommentsVO fraudComment = null;
        Iterator fraudCommentsIterator = null;

        //only continue if there is something to update
        if(fraudComments == null) return true;
                
            fraudCommentsIterator = fraudComments.iterator();

            for (int i = 0; fraudCommentsIterator.hasNext(); i++) 
            {
                fraudComment = (FraudCommentsVO)fraudCommentsIterator.next();
                    
                //only insert if comment id is <= 0 ... a new comment
                if(fraudComment.getFraudCommentID() <= 0 ){

                if(fraudComment.isChanged()){

                  orderCommentMap = new HashMap();
                  orderCommentMap.put("IN_UPDATED_BY" , userID);
                  orderCommentMap.put("IN_ORDER_GUID" , order.getGUID());
                  orderCommentMap.put("IN_FRAUD_ID",fraudComment.getFraudID());
                  orderCommentMap.put("IN_COMMENT_TEXT",fraudComment.getCommentText());
                  orderCommentMap.put("IO_FRAUD_COMMENT_ID",new Integer(java.sql.Types.INTEGER));
                  orderCommentMap.put(STATUS_PARAM,"");
                  orderCommentMap.put(MESSAGE_PARAM,"");
  
                  dataRequest.setConnection(connection);
                  dataRequest.setStatementID(INSERT_FRAUD_COMMENTS);
                  dataRequest.setInputParams(orderCommentMap);
  
                  DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
 
                  Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
                  status = (String) outputs.get(STATUS_PARAM);
                  if(status.equals("N"))
                  {
                      message = (String) outputs.get(MESSAGE_PARAM);
                      returnStatus = false;
                      throw new Exception(message);
                  }
                  else
                  {
                      //set comment id
                      BigDecimal commentId = (BigDecimal) outputs.get("IO_FRAUD_COMMENT_ID");
                      fraudComment.setFraudCommentID(commentId.longValue());
                
                      returnStatus = true;
                  }
                } //end is changed
            }//end if id > 0
        }
        return returnStatus;
    }



    


    private boolean updateBuyerComments(OrderVO order) throws Exception
    {
        // update the buyer comments infomration

        DataRequest dataRequest = new DataRequest();

        HashMap orderBuyerComment = null;
        Collection buyers = order.getBuyer();            
        Collection buyerComments = null;
        BuyerVO buyer = null;
        BuyerCommentsVO buyerComment = null;
        Iterator iterator = null;
        Iterator buyerCommentsIterator = null;

        //only continue if there is something to update
        if(buyers == null) return true;
                
        iterator = buyers.iterator();
        long buyerId = 0;
                                
        for (int i = 0; iterator.hasNext(); i++) 
        {
            buyer = (BuyerVO)iterator.next();

            buyerComments = buyer.getBuyerComments();

            //only continue if there is something to update
            if(buyerComments == null) return true;
            
            buyerCommentsIterator = buyerComments.iterator();
            buyerId = buyer.getBuyerId();

            for (int x = 0; buyerCommentsIterator.hasNext(); x++) 
            {
                buyerComment = (BuyerCommentsVO)buyerCommentsIterator.next();
                buyerComment.setBuyerId(buyerId);
                    
                //only insert if comment id is <= 0 ... a new comment
                if(buyerComment.getBuyerCommentId() <= 0 ){


                if(buyerComment.isChanged()){

                  orderBuyerComment = new HashMap();

                  orderBuyerComment.put("IN_UPDATED_BY" , userID);
                  orderBuyerComment.put("IO_BUYER_COMMENT_ID",new Integer(java.sql.Types.INTEGER));
                  orderBuyerComment.put("IN_BUYER_ID",formatToLong(buyerComment.getBuyerId()));
                  orderBuyerComment.put("IN_TEXT",buyerComment.getText());
                  orderBuyerComment.put(STATUS_PARAM,"");
                  orderBuyerComment.put(MESSAGE_PARAM,"");
  
                  dataRequest.setConnection(connection);
                  dataRequest.setStatementID(INSERT_BUYER_COMMENTS);
                  dataRequest.setInputParams(orderBuyerComment);
  
                  DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
 
                  Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
                  status = (String) outputs.get(STATUS_PARAM);
                  if(status.equals("N"))
                  {
                      message = (String) outputs.get(MESSAGE_PARAM);
                      returnStatus = false;
                      throw new Exception(message);
                  }
                  else
                  {
                      //set comment id
                      BigDecimal commentId = (BigDecimal) outputs.get(BUYER_COMMENT_ID_PARM);
                      buyerComment.setBuyerCommentId(commentId.longValue());
                
                      returnStatus = true;
                  }

                }//if has changed
              }
            }//end if id > 0
        }
        return returnStatus;
    }

    private boolean updateRecipientComments(OrderVO order) throws Exception
    {

        DataRequest dataRequest = new DataRequest();

        HashMap orderRecipientComment = null;
        Collection items = order.getOrderDetail();            
        Collection recipients = null;
        Collection recipientComments = null;
        OrderDetailsVO item = null;
        RecipientsVO recipient = null;
        RecipientCommentsVO recipientComment = null;
        Iterator iterator = null;
        Iterator recipientIterator = null;
        Iterator recipientCommentIterator = null;

        //only continue if there is something to update
        if(items == null) return true;
                
        iterator = items.iterator();
        long recipId = 0;
                                
        for (int i = 0; iterator.hasNext(); i++) 
        {
            item = (OrderDetailsVO)iterator.next();
            recipients = item.getRecipients();

            //only continue if there is something to update
            if(recipients == null) return true;

            recipientIterator = recipients.iterator();
        
            for (int x = 0; recipientIterator.hasNext(); x++) 
            {
                recipient = (RecipientsVO)recipientIterator.next();
                recipientComments = recipient.getRecipientComments();

                //only continue if there is something to update
                if(recipientComments == null) return true;
                
                recipientCommentIterator = recipientComments.iterator();
                recipId = recipient.getRecipientId();
                
                  for (int y = 0; recipientCommentIterator.hasNext(); y++) 
                  {
                      recipientComment = (RecipientCommentsVO)recipientCommentIterator.next();
                      recipientComment.setRecipientId(recipId);

                      //only insert the comment if the id has not been entered
                      if(recipientComment.getRecipientCommentId() <= 0){      

                      if(recipientComment.isChanged()){
                    
                            orderRecipientComment = new HashMap();

                            orderRecipientComment.put("IN_UPDATED_BY" , userID);
                            orderRecipientComment.put("IO_RECIPIENT_COMMENT_ID",new Integer(java.sql.Types.INTEGER));
                            orderRecipientComment.put("IN_RECIPIENT_ID",formatToLong(recipientComment.getRecipientId()));
                            orderRecipientComment.put("IN_TEXT",recipientComment.getText());
                            orderRecipientComment.put(STATUS_PARAM,"");
                            orderRecipientComment.put(MESSAGE_PARAM,"");
  
                            dataRequest.setConnection(connection);
                            dataRequest.setStatementID(INSERT_RECIPIENT_COMMENTS);
                            dataRequest.setInputParams(orderRecipientComment);
  
                            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
 
                            Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
                            status = (String) outputs.get(STATUS_PARAM);
                            if(status.equals("N"))
                            {
                                message = (String) outputs.get(MESSAGE_PARAM);
                                returnStatus = false;
                                throw new Exception(message);
                            }
                            else
                            {
                                //set comment id
                                BigDecimal commentId = (BigDecimal) outputs.get(RECIPIENT_COMMENT_ID_PARM);
                                recipientComment.setRecipientCommentId(commentId.longValue());
                      
                                returnStatus = true;
                            }
                      }//has changed
                      }//if id > 0
                  }
            }   
        }
        return returnStatus;
    }




    /* Create Dboule object from String
     * @param String value
     * @return Double object
     *         returns Long with value of zero if String is null*/    
    private Double formatToDouble(String value)
    {
      return value == null ? new Double("0.0") : new Double(value);  
    }


    /* Create long object from long native type
     * @param long
     * @return Long object */
    private Long formatToLong(long value)
    {
      return new Long(value);
    }

    /* Create long object from String
     * @param String value
     * @return Long object
     *         returns Long with value of zero if String is null*/    
    private Long formatToLong(String value)
    {
      return value == null ? new Long("0") : new Long(value);
    }

    /* Return native long from Long object
     * @param Long
     * @return long value
     *         returns zero if String is null*/    
    private long formatToNativeLong(String value)
    {
      return value == null ? 0 : new Long(value).longValue();
    } 

/*
 * Description: Takes in a string Date, checks for valid formatting
 * 				in the form of mm/dd/yyyy and converts it
 * 				to a SQL date of yyyy-mm-dd.
 *
 * @param String date in string format
 * @return java.sql.Date
 * 
 * Note..copied from OE project
 */
private static java.sql.Date getSQLDate(String strDate) throws Exception{

        java.sql.Date sqlDate = null;
        String inDateFormat = "";
        int dateLength = 0;
        int firstSep = 0;
        int lastSep = 0;

    try{




        if ((strDate != null) && (!strDate.trim().equals(""))) {

          //if lenght of 5 then it is the format of MMDD..reformat it
          if(strDate.length() == 5)
          {
            String month = strDate.substring(0,2);
            String year = strDate.substring(3,5);
            int yearNum =   (new Integer(year)).intValue() + 2000;
            strDate = month + "/31/" + yearNum;
          }


		    // set input date format
		    dateLength = strDate.length();
		    if ( dateLength > 10) {
			    firstSep = strDate.indexOf("/");
          if(firstSep == 1)
          {
            inDateFormat = "M/dd/yyyy hh:mm:ss";            
          }
          else if (firstSep == 2)
          {
            inDateFormat = "MM/dd/yyyy hh:mm:ss";
          }
          else
          {
  			    firstSep = strDate.indexOf("/");          
            if(firstSep > 0){
              inDateFormat = "yyyy-MM-dd hh:mm:ss"; 
            }
              //check for spaces
              firstSep = strDate.indexOf(" ");          
              if(firstSep <= 0){
                inDateFormat = "yyyyMMddHHmmsszzz";
              }
              else
              {
                  if(dateLength > 20){
                      inDateFormat = "EEE MMM dd HH:mm:ss zzz yyyy";               
                  }
                  else{
                      inDateFormat = "MMM dd yyyy HH:mmaa";               
                  }
              }
          }
			    
		    } else {
			    firstSep = strDate.indexOf("/");
			    lastSep = strDate.lastIndexOf("/");

			    switch ( dateLength ) {
				    case 10:
			    		inDateFormat = "MM/dd/yyyy";
				    	break;
				    case 9:
				    	if ( firstSep == 1 ) {
					    	inDateFormat = "M/dd/yyyy";
				    	} else {
					    	inDateFormat = "MM/d/yyyy";
			    		}
				    	break;
				    case 8:
				    	if ( firstSep == 1 ) {
					    	inDateFormat = "M/d/yyyy";
				    	} else {
					    	inDateFormat = "MM/dd/yy";
			    		}
				    	break;
				    case 7:
				    	if ( firstSep == 1 ) {
					    	inDateFormat = "M/dd/yy";
				    	} else {
					    	inDateFormat = "MM/d/yy";
			    		}
				    	break;
				    case 6:
				    	inDateFormat = "M/d/yy";
				    	break;
				    default:
				    	break;
			    }
		    }
            //SimpleDateFormat sdfInput = new SimpleDateFormat( "MM/dd/yyyy" );
            SimpleDateFormat sdfInput = new SimpleDateFormat(inDateFormat);

            //must be in this format to convert to date
            String outDateFormat = "yyyy-MM-dd";             


            SimpleDateFormat sdfOutput = new SimpleDateFormat ( outDateFormat );

            java.util.Date date = sdfInput.parse( strDate );
            String outDateString = sdfOutput.format( date );


          // now that we have no errors, use the string to make a SQL date
          sqlDate = sqlDate.valueOf(outDateString);

        }
        }
      catch(Exception e)
      {
        throw e;
      }		  

	return sqlDate;
}


/*
 * Description: Takes in a string Date, checks for valid formatting
 * 				in the form of mm/dd/yyyy and converts it
 * 				to a SQL date of yyyy-mm-dd.
 *
 * @param String date in string format
 * @return java.sql.Date
 * 
 * Note..copied from OE project
 */
private static java.sql.Timestamp getSQLTimestamp(String strDate) throws Exception{

        java.sql.Timestamp sqlTimeStamp = null;
        String inDateFormat = "";
        int dateLength = 0;
        int firstSep = 0;
        int lastSep = 0;

    try{




        if ((strDate != null) && (!strDate.trim().equals(""))) {

          //if lenght of 5 then it is the format of MMDD..reformat it
          if(strDate.length() == 5)
          {
            String month = strDate.substring(0,2);
            String year = strDate.substring(3,5);
            int yearNum =   (new Integer(year)).intValue() + 2000;
            strDate = month + "/31/" + yearNum;
          }


		    // set input date format
		    dateLength = strDate.length();
		    if ( dateLength > 10) {
			    firstSep = strDate.indexOf("/");
          if(firstSep == 1)
          {
            inDateFormat = "M/dd/yyyy hh:mm:ss";            
          }
          else if (firstSep == 2)
          {
            inDateFormat = "MM/dd/yyyy hh:mm:ss";
          }
          else
          {
  			    firstSep = strDate.indexOf("/");          
            if(firstSep > 0){
              inDateFormat = "yyyy-MM-dd hh:mm:ss"; 
            }
              //check for spaces
              firstSep = strDate.indexOf(" ");          
              if(firstSep <= 0){
                inDateFormat = "yyyyMMddHHmmsszzz";
              }
              else
              {
                  if(dateLength > 20){
                      inDateFormat = "EEE MMM dd HH:mm:ss zzz yyyy";               
                  }
                  else{
                      inDateFormat = "MMM dd yyyy HH:mmaa";               
                  }
              }
          }
			    
		    } else {
			    firstSep = strDate.indexOf("/");
			    lastSep = strDate.lastIndexOf("/");

			    switch ( dateLength ) {
				    case 10:
			    		inDateFormat = "MM/dd/yyyy";
				    	break;
				    case 9:
				    	if ( firstSep == 1 ) {
					    	inDateFormat = "M/dd/yyyy";
				    	} else {
					    	inDateFormat = "MM/d/yyyy";
			    		}
				    	break;
				    case 8:
				    	if ( firstSep == 1 ) {
					    	inDateFormat = "M/d/yyyy";
				    	} else {
					    	inDateFormat = "MM/dd/yy";
			    		}
				    	break;
				    case 7:
				    	if ( firstSep == 1 ) {
					    	inDateFormat = "M/dd/yy";
				    	} else {
					    	inDateFormat = "MM/d/yy";
			    		}
				    	break;
				    case 6:
				    	inDateFormat = "M/d/yy";
				    	break;
				    default:
				    	break;
			    }
		    }
            //SimpleDateFormat sdfInput = new SimpleDateFormat( "MM/dd/yyyy" );
            SimpleDateFormat sdfInput = new SimpleDateFormat(inDateFormat);

            //must be in this format to convert to date
            String outDateFormat = "yyyy-MM-dd hh:mm:ss";             


            SimpleDateFormat sdfOutput = new SimpleDateFormat ( outDateFormat );

            java.util.Date date = sdfInput.parse( strDate );
            String outDateString = sdfOutput.format( date );


          // now that we have no errors, use the string to make a SQL date
          sqlTimeStamp = new java.sql.Timestamp(date.getTime());;

        }
        }
      catch(Exception e)
      {
        throw e;
      }		  

	return sqlTimeStamp;
}

  /** Delete all Addons for the given detail ID
   * @Stirng detail ID
   * @throws Exception
   * emueller 12/17/03
   */
   private boolean deleteAddons(long detailID) throws Exception
   {

        boolean returnStatus = false;
       
        DataRequest dataRequest = new DataRequest();
        Map deleteMap = new HashMap();
        deleteMap.put("IN_ORDER_DETAIL_ID",new Long(detailID));       

        dataRequest.setConnection(connection);
        dataRequest.setStatementID(DELETE_ADD_ONS);
        dataRequest.setInputParams(deleteMap);
  
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
 
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
        String status = (String) outputs.get(STATUS_PARAM);
        if(status.equals("N"))
        {
            message = (String) outputs.get(MESSAGE_PARAM);
            returnStatus = false;
            throw new Exception(message);
        }
        else
        {
            returnStatus = true;
        }

        return returnStatus;
   }


    public static void main(String[] args)
    {
            Connection conn = null;
        try
        {

        java.sql.Date test = getSQLDate("Thu Sep 18 12:05:52 CDT 2003");


            String driver_ = "oracle.jdbc.driver.OracleDriver";
            String database_ = "jdbc:oracle:thin:@sisyphus.ftdi.com:1521:DEV2";
            String user_ = "osp";
            String password_ = "osp";

            Class.forName(driver_);
            conn = DriverManager.getConnection(database_, user_, password_);
            conn.setAutoCommit(false);
            FRPMapperDAO frpMapperDAO = new FRPMapperDAO(conn);

            String guid = "101";
            //String guid = "FTD_GUID_14605659930-21448840340-14397683130-1946420596073391458502957409070-130170920804447924501-13739207640-7506063230-9896030710-1405133379247-2081500243177109084223135-1495832330140403822269210";
            
            OrderVO order = new OrderVO();

            //ScrubMapperDAO scrub = new ScrubMapperDAO(conn);            
            //order = scrub.mapOrderFromDB(guid);
            order = frpMapperDAO.mapOrderFromDB(guid);

 /*           order.setGUID("103");
            order.setMasterOrderNumber("ED" + order.getGUID());


            //change buyer customer ids
            List buyers = order.getBuyer();
            int x = 0;
            for (int i = 0; i < buyers.size(); i++) 
            {
              BuyerVO vo = (BuyerVO)buyers.get(i);
              vo.setCustomerId(order.getMasterOrderNumber() + i);
            }
   */         
            
            


            //add new comment
           // RecipientCommentsVO commentVO = new RecipientCommentsVO();
          //  commentVO.setText("Another New Comment");
          //  commentVO.setRecipientId(1);
            
          //  List details = order.getOrderDetail();
        //    OrderDetailsVO detailVO = (OrderDetailsVO)details.get(0);
         //   List recips = detailVO.getRecipients();
         //   RecipientsVO recipVO = (RecipientsVO)recips.get(0);
         //   List comments = recipVO.getRecipientComments();
         //   comments.add(commentVO);
         //   frpMapperDAO.fixOrder(order);
            frpMapperDAO.updateOrder(order);  
//              frpMapperDAO.mapOrderToDB(order);
      
            conn.commit();
        

            System.out.println(order.toString());
        }
        catch(Exception e)
        {
            Logger log = new Logger("com.ftd.osp.utilities.order.FRPMapperDAO");
            log.error(e);
            e.printStackTrace();

          try{
            conn.rollback();
          }
          catch(Exception ee)
          {
            System.out.println("!!!!!!!!! Something bad happened !!!!!!!");
          }            
        }
        finally
        {
          try{
            conn.close();
          }
          catch(Exception ee)
          {
            System.out.println("!!!!!!!!! Something bad happened !!!!!!!");
          }   
        }
    }

    
}