package com.ftd.osp.utilities.order;

import com.ftd.osp.utilities.order.vo.OrderVO;

/**
 * 
 *
 * @author Doug Johnson
 */
 public interface OrderDataMapper  {

  /**
   * 
   * 
   * @param OrderVO order
   * @return true/false success of insert
   * @exception Exception
   */
  public boolean mapOrderToDB(OrderVO order) throws Exception;

 /**
   * 
   * 
   * @param order id
   * @return OrderVO
   * @exception Exception
   */
  public OrderVO mapOrderFromDB(String orderId) throws Exception;

 /**
   * 
   * 
   * @param order id
   * @return true/false success of update
   * @exception Exception
   */
  public boolean updateOrder(OrderVO order) throws Exception;

}