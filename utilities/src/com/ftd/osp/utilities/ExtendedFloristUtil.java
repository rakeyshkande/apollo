package com.ftd.osp.utilities;

import java.io.IOException;
import java.sql.Connection;
import java.util.Date;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.codehaus.jackson.map.ObjectMapper;

import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.stats.ServiceResponseTrackingUtil;
import com.ftd.osp.utilities.vo.ExtendedFloristAvailabilityRequest;
import com.ftd.osp.utilities.vo.ExtendedFloristAvailabilityResponse;
import com.ftd.osp.utilities.vo.ServiceResponseTrackingVO;

public class ExtendedFloristUtil {

	private static Logger logger = new Logger(
			"com.ftd.osp.utilities.ExtendedFloristUtil");
	public static final String EXTENDED_FLORIST_SERVICE_URL = "EFA_SERVICE_URL";
	public static final String EXTENDED_FLORIST_SERVICE_TIMEOUT = "EFA_SERVICE_TIMEOUT";
	private static final String EXTENDED_FLORIST_SERVICE = "Extended Florist Service";
	private static final String EXTENDED_FLORIST_SERVICE_SECRET = "EFA_SERVICE_SECRET";

	private static ExtendedFloristUtil util = new ExtendedFloristUtil();

	public static ExtendedFloristUtil getInstance() {
		return util;
	}

	private static String getURL(String company, String uri) {
		String efaCodeURL = RESTUtils.getConfigParamValue(EXTENDED_FLORIST_SERVICE_URL, false);
		String URL = efaCodeURL.concat(company + uri);
		logger.info("Request URL : " + URL);
		return URL.trim();
	}

	private static String executeRequest(HttpUriRequest request) throws Exception {
		int timeout = Integer.parseInt(RESTUtils.getConfigParamValue(EXTENDED_FLORIST_SERVICE_TIMEOUT, false)) * 1000; // Convert seconds to ms.
		RESTUtils utils = new RESTUtils("Extended Florist", false, false);
		
			return utils.executeRequest(request,  
				timeout, 
				RESTUtils.getConfigParamValue(EXTENDED_FLORIST_SERVICE_SECRET, true));
		
	}

	/**
	 * Calls Extended Florist Availability Service to see if there are any Non-Ftd Florists available to 
	 * service this order. 
	 * 
	 * @param extendedFloristAvailabilityRequest
	 * @return extendedFloristAvailabilityResponse
	 */
	public static ExtendedFloristAvailabilityResponse isAnyFloristAvailable(ExtendedFloristAvailabilityRequest extendedFloristAvailabilityRequest) throws Exception {
		logger.info("isAnyFloristAvailable : Extended Florist Availability Service request started at " + System.currentTimeMillis());
		// Start timer for service call
		long startTime = (new Date()).getTime();
		HttpPost httpPost = null;
		
		httpPost = new HttpPost(getURL("", "floristAvailable?markcode=".concat(extendedFloristAvailabilityRequest.getSourceCode())));
		
		String requestJsonString = convertToJSonString(extendedFloristAvailabilityRequest);
		logger.info("REQUEST:  " + requestJsonString);
		httpPost.setEntity(new StringEntity(requestJsonString, "utf-8"));
		String responseString = executeRequest(httpPost);
		
		ExtendedFloristAvailabilityResponse extendedFloristAvailabilityResponse = null;
		logger.info("Response String : "+responseString);
		if (null != responseString) {
			extendedFloristAvailabilityResponse = convertToRetrieveSingleObject(responseString);
		}
		logger.info("Response object : " + extendedFloristAvailabilityResponse);

		logServiceResponseTracking(extendedFloristAvailabilityRequest.getZipCode() + "|" + extendedFloristAvailabilityRequest.getProductId(),
				"isAnyFloristAvailable", startTime);
		
		return extendedFloristAvailabilityResponse;
	}
	
	
	/**
	 * Converts Response String into Response Object
	 * 
	 * @param jsonString
	 * @return ExtendedFloristAvailabilityResponse
	 */
	public static ExtendedFloristAvailabilityResponse convertToRetrieveSingleObject(
			String jsonString) {
		ExtendedFloristAvailabilityResponse retObj = new ExtendedFloristAvailabilityResponse();
		ObjectMapper mapperObj = new ObjectMapper();
		try {
			retObj = mapperObj.readValue(jsonString,
					ExtendedFloristAvailabilityResponse.class);
		} catch (IOException ex) {
		}
		return retObj;
	}

	/**
	 * This method converts and object to a json string
	 * 
	 * @param Object
	 *            req
	 * @return String
	 * @throws java.lang.Exception
	 */
	public static String convertToJSonString(Object req) {
		logger.debug("Req : " + req);
		String jsonString = null;
		ObjectMapper mapperObj = new ObjectMapper();
		try {
			jsonString = mapperObj.writeValueAsString(req);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return jsonString;
	}


	public static void logServiceResponseTracking(String requestId,
			String serviceMethod, long startTime) {
		Connection connection = null;
		try {
			long responseTime = System.currentTimeMillis() - startTime;
			ServiceResponseTrackingVO srtVO = new ServiceResponseTrackingVO();
			srtVO.setServiceName(EXTENDED_FLORIST_SERVICE);
			srtVO.setServiceMethod(serviceMethod);
			srtVO.setTransactionId(requestId);
			srtVO.setResponseTime(responseTime);
			srtVO.setCreatedOn(new Date());

			connection = DataSourceUtil.getInstance().getConnection(
					"ORDER SCRUB");
			ServiceResponseTrackingUtil srtUtil = new ServiceResponseTrackingUtil();
			srtUtil.insertWithRequest(connection, srtVO);
		} catch (Exception e) {
			logger.error("Exception occured while persisting Extended Florist Service response times.");
		}

	}
	
}