package com.ftd.osp.utilities.j2ee;


import com.ftd.osp.utilities.plugins.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

import java.net.URL;
import java.net.URLDecoder;

/**
 * Responsible for obtaining all resources external to the application.
 *
 * @author Anshu Gaind
 */

public class ResourceUtil  {
  private static Logger logger = new Logger("com.ftd.osp.utilities.j2ee.ResourceUtil");
  private static ResourceUtil RESOURCEUTIL = new ResourceUtil();

 /**
  * The static initializer for the resource util. It ensures that
  * at a given time there is only a single instance
  *
  * @return the resource util
  **/
  public static ResourceUtil getInstance(){
      return RESOURCEUTIL;
  }

  /**
   * The private Constructor
   **/
  private ResourceUtil(){
    super();
  }


  /**
   * Finds a resource with a given name. It returns the resource as an
   * InputStream. This method uses the ClassLoader
   *
   * @param name the name of the resource
   * @return the resource as a stream.
   **/
  public InputStream getResourceAsStream(String name) {
    Class class_v = this.getClass();
    ClassLoader classLoader = class_v.getClassLoader();

    InputStream is = classLoader.getResourceAsStream(name);
    if (is == null) 
    {
      // JP Puzon 08-31-2005
      // Attempt to obtain the resource using the current thread class loader.
      logger.debug("using context class loader.");
      is = Thread.currentThread().getContextClassLoader().getResourceAsStream(name);
      logger.debug("resource path:" + Thread.currentThread().getContextClassLoader().getResource(name).getPath());  
    }
    logger.debug("resource path:" + classLoader.getResource(name).getPath());
    try {
        java.util.Enumeration<URL> allResources = classLoader.getResources(name);
        while (allResources.hasMoreElements()) {
            logger.debug("resource found:" + allResources.nextElement().getPath());
        }
    } catch (Exception e) {
        logger.error(e);
    }
    logger.debug("***************************************************");
    logger.debug("* Getting Resource:: " + name + " as Stream");
    logger.debug("***************************************************");
    return is;
  }

  /**
   * Finds a resource with a given name. It returns a URL to the resource
   *
   * @param name the name of the resource
   * @return a URL to the resource
   **/
  public URL getResource(String name)  {
    Class class_v = this.getClass();
    ClassLoader classLoader = class_v.getClassLoader();

    URL url = classLoader.getResource(name);
    if (url == null) 
    {
      // JP Puzon 08-31-2005
      // Attempt to obtain the resource using the current thread class loader.
      url = Thread.currentThread().getContextClassLoader().getResource(name);
    }
    logger.debug("***************************************************");
    logger.debug("* Getting Resource:: " + name);
    logger.debug("***************************************************");
    return url;
  }

  
  /**
   * Returns a resource by its fileName as a Stream. This method will 
   * return a File if there is a physical File (Unpacked Environment). 
   * Else it will return an InputStream from the ClassLoader (Packed Environment).
   * 
   * Invokes {@link #getResource(String)} to retrieve the Resource path
   * Invokes {@link #getResourceAsStream(String)} to retrieve the Resource Stream
   * 
   * @param fileName Name of the Resource File
   * @return The InputStream for the File Resource or <code>null</code> if the File is not accessible
   */
  public InputStream getResourceFileAsStream(String fileName) {
      URL url = getResource(fileName);
      if (url == null){
          logger.error("The file was not found: " + fileName);
          return null;
      }

      File f = null;
      try {
    	  f = new File(URLDecoder.decode(url.getFile(), "UTF-8"));
      } catch (Exception e1) {
          logger.warn("Failed to retrieve InputStream for File, will attempt ClassLoader next: " + f.getAbsolutePath(), e1);
          return getResourceAsStream(fileName);
      }
  
  
      
      if(f.exists())
      {
          try
          {
              return new FileInputStream(f);
          } catch (Exception e)
          {
              logger.warn("Failed to retrieve InputStream for File, will attempt ClassLoader next: " + f.getAbsolutePath(), e);
          }
      }
      
      return getResourceAsStream(fileName);
  }
}