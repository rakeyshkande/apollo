package com.ftd.osp.utilities.j2ee;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import java.net.URL;
import java.net.URLDecoder;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.w3c.dom.*;

import org.xml.sax.SAXException;

/**
 * Provides access to an initial context object
 *
 * @author Anshu Gaind
 */

public class InitialContextUtil  {
  private static Logger logger = new Logger("com.ftd.osp.utilities.j2ee.InitialContextUtil");
  private static InitialContextUtil INITIALCONTEXTUTIL = new InitialContextUtil();
  private static final String configFile = "initialcontext.xml";
  private static Document config;
  private static boolean SINGLETON_INITIALIZED;

 /**
  * The static initializer for the intial context util. It ensures that
  * at a given time there is only a single instance
  *
  * @return the initial context util
  **/
  public static InitialContextUtil getInstance() throws Exception{
    if ( ! SINGLETON_INITIALIZED ){
        initConfig();
    }
    return INITIALCONTEXTUTIL;
  }

  /**
   * The private Constructor
   **/
  private InitialContextUtil(){
    super();
  }


  /**
   * Initialize the configuration
   */
  private static synchronized void initConfig()
        throws IOException, SAXException, ParserConfigurationException {
    if (! SINGLETON_INITIALIZED) 
    {            
        InputStream configInputStream = ResourceUtil.getInstance().getResourceFileAsStream(configFile);
        if(configInputStream != null)
        {
        	config = (Document) DOMUtil.getDocument(configInputStream);        
            SINGLETON_INITIALIZED = true;
        	
        }
        
      // do not throw an IOException if the config file is not found
      // just return a new InitialContext() later;
    }
  }


  /**
   * Returns the Initial Context object
   *
   * @param the location as specified in the config file.
   * @return the initial context object
   */
  public InitialContext getInitialContext(String location)
    throws NamingException, TransformerException, Exception{

      if (config == null) 
      {
          // Done to support the absence of the config file.
          // return an initial context anyway
          return new InitialContext();          
      }
      
      Element locationE = ( (Element) DOMUtil.selectSingleNode(config, "//location[@name='" + location + "']"));

      if (locationE == null) 
      {
          // return an initial context anyway
          return new InitialContext();
      }
      
      String lookup = locationE.getAttribute("lookup");
      if ( (lookup!= null) && (lookup.equalsIgnoreCase("remote")) ) 
      {
        String contextFactory =  locationE.getAttribute("jndi-context-factory");
        String providerURL =  locationE.getAttribute("jndi-provider-url");
        String securityPrincipal =  locationE.getAttribute("jndi-context-security-principal");
        String securityCredentials =  locationE.getAttribute("jndi-context-security-credentials");

        Hashtable env = new Hashtable();
        env.put(Context.INITIAL_CONTEXT_FACTORY, contextFactory);
        env.put(Context.PROVIDER_URL, providerURL);
        env.put(Context.SECURITY_PRINCIPAL, securityPrincipal);
        env.put(Context.SECURITY_CREDENTIALS, securityCredentials);

        // add properties
        NodeList locationE_properties = (NodeList) DOMUtil.selectNodeList(locationE, "properties/property");

        if (locationE_properties != null) 
        {
          Element propertyE = null;
          for (int i = 0; i < locationE_properties.getLength(); i++) 
          {
            propertyE = (Element)locationE_properties.item(i);
            if (propertyE != null) 
            {
                env.put(propertyE.getAttribute("name"), propertyE.getAttribute("value"));
            }
          }            
        }
        return new InitialContext(env);
      } 
      else 
      {
        return new InitialContext();
      }

  }
  
  
  /**
   * Looks up an environment item in the JNDI Binding. Searches in java:comp/env
   * first as that is where these should typically be bound. Then, searches in "" 
   * since that is what the original OC4J code did.
   * 
   * @param <T>
   * @param context The context to perform lookup on
   * @param bindingName The binding to perform the lookup for
   * @param returnType The return object class type - to avoid casting.
   * @return The bound object retrieved from the environment or root context
   * @throws Exception Expect binding not found or class cast exceptions
   */
  @SuppressWarnings("unchecked")
  public <T> T lookupEnvironmentEntry(final InitialContext context, final String bindingName, final Class<T> returnType)
  throws Exception
  {      
      try
      {
          Context myenv = (Context) context.lookup("java:comp/env");
          return (T) myenv.lookup(bindingName);
      } catch (Exception e)
      {
          logger.debug("Binding not found in java:comp/env, searching base context next: " + bindingName);
      }
      
      Context myenv = (Context) context.lookup("");
      return (T) myenv.lookup(bindingName);
  }

/*
  public static void main(String[] args) {
    try  {
    InitialContextUtil x = com.ftd.osp.utilities.j2ee.InitialContextUtil.getInstance();
    InitialContext ctx = x.getInitialContext("LOCALHOST");
    } catch (Exception ex)  {
      ex.printStackTrace();
    } finally  {
    }

  }
*/

}
