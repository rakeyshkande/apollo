package com.ftd.osp.utilities.j2ee;

import com.ftd.ftdutilities.GeneralConstants;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.NotificationVO;
import com.ftd.osp.utilities.ConfigurationUtil;

import java.io.File;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.StringTokenizer;

import java.util.regex.Pattern;
import java.util.regex.Matcher;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class NotificationUtil  {
  private static Logger logger = new Logger("com.ftd.osp.utilities.j2ee.NotificationUtil");
  private static NotificationUtil NOTIFICATIONUTIL = new NotificationUtil();
  //Timeout for the connection in milliseconds
  private static final String timeout = "60000";

 /**
  * The static initializer for the NotificationUtil. It ensures that
  * at a given time there is only a single instance
  *
  * @return the NotificationUtil
  **/
  public static NotificationUtil getInstance()  throws Exception{
      return NOTIFICATIONUTIL;
  }

  /**
   * The private Constructor
   **/
  private NotificationUtil(){
    super();
  }


  /**
   * Notifies via email to all registered addresses.
   * Relies on the SMTP server address provided in the notification value object
   *
   * @param notificationVO The notification value object
   * @exception Exception
   */
   public void notify(NotificationVO notificationVO) throws Exception{
      if (notificationVO == null)  {
        logger.error("NotificationVO is null");
          throw new Exception("NotificationVO is null");
      }

      Properties props = new Properties();
      // fill props with any information
      // Setup mail server
      logger.info("SMTP Host is " + notificationVO.getSMTPHost());      
      props.put("mail.smtp.host", notificationVO.getSMTPHost());
      props.put("mail.smtp.connectiontimeout", this.timeout);
      props.put("mail.smtp.timeout", this.timeout);

      Session session = Session.getInstance(props, null);

      this._notify(session, notificationVO);
   }



  /**
   * Notifies via email to all registered addresses.
   * The transaction is managed by the container.
   *
   * @param session the Java Mail Session
   * @param notificationVO The notification value object
   * @exception Exception
   */
   public void notify(Session session, NotificationVO notificationVO) throws Exception{

      if (session == null)  {
        logger.error("session is null");
          throw new Exception("session is null");
      }

      if (notificationVO == null)  {
        logger.error("NotificationVO is null");
          throw new Exception("NotificationVO is null");
      }
      this._notify(session, notificationVO);
   }


   /**
    *
    */
   private void _notify(Session session, NotificationVO notificationVO) throws Exception{
    try {
      Message message = new MimeMessage(session);
      message.setFrom(new InternetAddress(notificationVO.getMessageFromAddress()));

      boolean debug = false;

      // create a message
      Message msg = new MimeMessage(session);

      // set the from and to address
      InternetAddress addressFrom = new InternetAddress(notificationVO.getMessageFromAddress());
      msg.setFrom(addressFrom);

      // Allow multiple addresses
      if (hasMultipleAddresses(Message.RecipientType.TO, notificationVO))  {
            Address toAddress[] = handleMultipleAddresses(Message.RecipientType.TO, notificationVO);
            msg.addRecipients(Message.RecipientType.TO, toAddress);
      } else {
            if (   (   notificationVO.getMessageTOAddress() != null   )
                && ( ! notificationVO.getMessageTOAddress().equals("")) )  {
                msg.addRecipient(Message.RecipientType.TO,
                new InternetAddress(notificationVO.getMessageTOAddress()));
            }
      }      
      
    
//A DIRTY FIX FOR WRONG CHAR ENCODING
      StringBuffer buf = new StringBuffer();
      byte[] bytes = notificationVO.getMessageContent().getBytes("iso-8859-1");
      for(int i=0;i<bytes.length;i++)
      {
        if(bytes[i] == -96)
        buf.append((char)32);        
        else
        if(bytes[i] < 127 && bytes[i] >= 0)
        buf.append((char)bytes[i]);
      }
      
      msg.setSubject(notificationVO.getMessageSubject());
      //msg.setContent(buf.toString(), "text/plain; charset=us-ascii");
      msg.setContent(notificationVO.getMessageContent(), "text/plain; charset=us-ascii");
        
      Transport.send(msg);
        
    } catch (Exception ex)  {
      logger.error(ex);
      throw ex;
    } finally  {
    }
   }

  /**
   * Notifies via email to all registered addresses
   *
   * @param notificationVO The notification value object
   * @exception Exception
   */
   public void notifyMulipart(NotificationVO notificationVO) throws Exception{
      if (notificationVO == null)  {
        logger.error("NotificationVO is null");
          throw new Exception("NotificationVO is null");
      }

      Properties props = new Properties();
      // fill props with any information
      // Setup mail server
      logger.info("SMTP Host is " + notificationVO.getSMTPHost());      
      props.put("mail.smtp.host", notificationVO.getSMTPHost());
      props.put("mail.smtp.connectiontimeout", this.timeout);
      props.put("mail.smtp.timeout", this.timeout);
      Session session = Session.getInstance(props, null);
      this._notifyMultipart(session, notificationVO);
   }

  /**
   * Notifies via email to all registered addresses
   *
   * @param session the Java Mail Session
   * @param notificationVO The notification value object
   * @exception Exception
   */
    public void notifyMultipart(Session session, NotificationVO notificationVO) throws Exception{
      if (session == null)  {
        logger.error("session is null");
          throw new Exception("session is null");
      }
      if (notificationVO == null)  {
        logger.error("NotificationVO is null");
          throw new Exception("NotificationVO is null");
      }

      this._notifyMultipart(session, notificationVO);
   }

   /**
    *
    */
    private void _notifyMultipart(Session session, NotificationVO notificationVO) throws Exception{
      try{
        Message message = new MimeMessage(session);
        message.setFrom(new InternetAddress(notificationVO.getMessageFromAddress()));

        // TO
        if (hasMultipleAddresses(Message.RecipientType.TO, notificationVO))  {
          Address toAddress[] = handleMultipleAddresses(Message.RecipientType.TO, notificationVO);
          message.addRecipients(Message.RecipientType.TO, toAddress);
        } else {
          if (   (   notificationVO.getMessageTOAddress() != null   )
              && ( ! notificationVO.getMessageTOAddress().equals("")) )  {
              message.setRecipient(Message.RecipientType.TO,
              new InternetAddress(notificationVO.getMessageTOAddress()));
          }
        }
        // CC
        if (hasMultipleAddresses(Message.RecipientType.CC, notificationVO))  {
          Address ccAddress[] = handleMultipleAddresses(Message.RecipientType.CC, notificationVO);
          message.addRecipients(Message.RecipientType.CC, ccAddress);
        } else {
          if (   (  notificationVO.getMessageCCAddress() != null   )
              && ( ! notificationVO.getMessageCCAddress().equals("")) )  {
              message.setRecipient(Message.RecipientType.CC,
              new InternetAddress(notificationVO.getMessageCCAddress()));
          }
        }
        // BCC
        if (hasMultipleAddresses(Message.RecipientType.BCC, notificationVO))  {
          Address bccAddress[] = handleMultipleAddresses(Message.RecipientType.BCC, notificationVO);
          message.addRecipients(Message.RecipientType.BCC, bccAddress);
        } else {
          if (   (   notificationVO.getMessageBCCAddress() != null   )
              && ( ! notificationVO.getMessageBCCAddress().equals("")) )  {
              message.setRecipient(Message.RecipientType.BCC,
              new InternetAddress(notificationVO.getMessageBCCAddress()));
          }
        }
        message.setSubject(notificationVO.getMessageSubject());
        
        Multipart multipart = new MimeMultipart("alternative");
        MimeBodyPart messageBodyPart = new MimeBodyPart();

/*
        MimeMultipart mp = new MimeMultipart();            
        BodyPart tp = new MimeBodyPart();            
        tp.setText(notificationVO.getMessageContent());            
        mp.addBodyPart(tp);            
        tp = new MimeBodyPart();           
        tp.setContent(notificationVO.getMessageContentHTML(), "text/html");            
        mp.addBodyPart(tp);            
        mp.setSubType("alternative");            
  */      



        // Create the message part HTML
        //messageBodyPart.setContent(notificationVO.getMessageContentHTML(), notificationVO.getMessageMimeType());

        messageBodyPart.setContent(notificationVO.getMessageContentHTML(), "text/html");
        multipart.addBodyPart(messageBodyPart);

        // Create the message part Text
        messageBodyPart = new MimeBodyPart();
        messageBodyPart.setContent(notificationVO.getMessageContent(), "text/plain");
        multipart.addBodyPart(messageBodyPart);

        //add attachment
        if (notificationVO.hasAttachment())  {
          DataSource source = null;
          Object obj = null;
          File file = null;

          // handle file attachment
          obj = notificationVO.getFileAttachment();
          if (obj != null)  {
             messageBodyPart = new MimeBodyPart();
             file = (File)obj;
             source = new FileDataSource(file);
             messageBodyPart.setDataHandler(new DataHandler(source));
             messageBodyPart.setFileName(file.getAbsolutePath());
             multipart.addBodyPart(messageBodyPart);
          }

          // handle file attachments
          obj = notificationVO.getFileAttachments();
          if (obj != null)  {
            List list = (List)obj;
            Iterator iterator = list.iterator();

            while (iterator.hasNext())  {
             messageBodyPart = new MimeBodyPart();
             file = (File)iterator.next();
             source = new FileDataSource(file);
             messageBodyPart.setDataHandler(new DataHandler(source));
             messageBodyPart.setFileName(file.getAbsolutePath());
             multipart.addBodyPart(messageBodyPart);
            }
          }
        } // end attachment

        message.setContent(multipart);
        Transport.send(message);

    } catch (Exception ex)  {
      logger.error(ex);
      throw ex;
    } finally  {
    }
  }


  /**
   * Indicates whether or not there are multiple  Addresses
   */
   private boolean hasMultipleAddresses(Message.RecipientType  recipientType, NotificationVO notificationVO){
      String separator = notificationVO.getAddressSeparator();
      String addresses = null;

      if (recipientType == Message.RecipientType.TO)  {
        addresses = notificationVO.getMessageTOAddress();
      } else if (recipientType == Message.RecipientType.CC)  {
        addresses = notificationVO.getMessageCCAddress();
      } else if (recipientType == Message.RecipientType.BCC){
        addresses = notificationVO.getMessageBCCAddress();
      }

      if ( (addresses != null) && (!addresses.equals("")) && (addresses.indexOf(separator) != -1) )  {
          return true;
      } else {
        return false;
      }
   }

   /**
    * Returns multiple  Addresses as an Address array
    *
    * @return multiple addresses as an Address array
    */
   private Address[] handleMultipleAddresses(Message.RecipientType  recipientType, NotificationVO notificationVO) throws AddressException{
      ArrayList list = new ArrayList();
      String addresses = null;
      Address addressArray[] = null;

      if (recipientType == Message.RecipientType.TO)  {
        addresses = notificationVO.getMessageTOAddress();
      } else if (recipientType == Message.RecipientType.CC)  {
        addresses = notificationVO.getMessageCCAddress();
      } else if (recipientType == Message.RecipientType.BCC){
        addresses = notificationVO.getMessageBCCAddress();
      }

      if ( (addresses != null) && (!addresses.equals("")) )  {
        StringTokenizer st = new StringTokenizer(addresses, notificationVO.getAddressSeparator());

        while (st.hasMoreTokens())  {
          list.add(new InternetAddress(st.nextToken()));
        }

        addressArray = new Address[list.size()];
        for (int i = 0; i < addressArray.length ; i++)  {
          addressArray[i] = (Address)list.get(i);
        }
      }
      logger.debug("Sending Email Notifications to :: " + addresses);
      return addressArray;
   }

   /**
   * convertStockToHtmlEmail
   * Converts a text email to one that has html tags and replaces the bracketed tokens
   * @param message
   * @param connection
   * @return
   * @throws Exception
   */
   public StringBuffer convertStockToHtmlEmail (StringBuffer message, Connection connection) throws Exception
   { 
     logger.debug("In convertStockToHtmlEmail");
     StringBuffer htmlMessage = new StringBuffer();
     String match = null;
     //Match a URL and all of the symobls allowed in one
     Pattern htmlLink = Pattern.compile("([^\"=>])(http|https)://([#&\\n\\-=?\\+\\%\\/\\.\\w:])+");
       
     Matcher linkMatcher = htmlLink.matcher(message);
     while (linkMatcher.find()) 
     {
       match = message.substring(linkMatcher.start()+1,linkMatcher.end());
       linkMatcher.appendReplacement(htmlMessage,"$1<a href=\"" + match + "\">" + match + "</a>");
     }
       
     linkMatcher.appendTail(htmlMessage);

       
     for (int i=0; i< htmlMessage.length(); i++) 
     {
       if (htmlMessage.charAt(i) == '\n')
       {
         htmlMessage.insert(i+1,"<br/>");
       }
     }
       
       
     htmlMessage = replaceTokens(htmlMessage, "HTML", connection);

     return htmlMessage;
   }
   
  /**
  * convertStockToTextEmail
  * Converts a text email to one that has tokens replaced
  * @param message
  * @param connection
  * @return
  * @throws Exception
  */
   
   public StringBuffer convertStockToTextEmail (StringBuffer message, Connection connection) throws Exception
   {
     logger.debug("In convertStockToTextEmail");
     StringBuffer textMessage = new StringBuffer();
     textMessage = replaceTokens(message, "TEXT", connection);
     return textMessage;
   }
    
    
   /**
   * replaceTokens
   * Replaces tokens that are found in messages
   * @param message
   * @param typeFilter
   * @param connection
   * @return
   * @throws Exception
   */
   
   private StringBuffer replaceTokens (StringBuffer message, String typeFilter, Connection connection) throws Exception
   {
     logger.debug("Tokanized " + typeFilter + " message: \n" + message);

     StringBuffer unTokanizedMessage = new StringBuffer();
     ConfigurationUtil configurationUtil = ConfigurationUtil.getInstance();
     String token;
     String value;
        
     Pattern replaceToken = Pattern.compile(GeneralConstants.TOKEN_START + "(\\w|\\.)+" + GeneralConstants.TOKEN_END);
        
     Matcher linkMatcher = replaceToken.matcher(message);
     while (linkMatcher.find()) 
     {
       token = message.substring(linkMatcher.start() + GeneralConstants.TOKEN_START_LENGTH,linkMatcher.end() - GeneralConstants.TOKEN_END_LENGTH);
       if (logger.isDebugEnabled())
       {
         logger.debug("Found token " + token); 
         logger.debug("Token found at start index " + linkMatcher.start() + " and end index " + linkMatcher.end());
       }
       try 
       {
         value = configurationUtil.getContentWithFilter(connection, GeneralConstants.STOCK_EMAIL_CONTEXT, 
                                                        GeneralConstants.STOCK_EMAIL_CONTEXT_TOKEN, typeFilter, token);
         //When there is no value returned, we still want to get the email sent;
         //So we just put a blank in its place.
         if (value == null)
         {
           value = "";
         }
       }
       catch (Exception e)
       {
         value = "";
         logger.warn(e);
       }
                                                    
       linkMatcher.appendReplacement(unTokanizedMessage, value);
     }
        
     linkMatcher.appendTail(unTokanizedMessage);   
     logger.debug("Untokanized message: \n" + unTokanizedMessage);
        
     return unTokanizedMessage;
        
    }
   
   
   /**
   * fixCharacterEncoding
   * This method is called to fix character encoding by setting the message to iso-8859-1
   * It also adds linebreaks where needed.
   * @param message
   * @return
   * @throws Exception
   */

   public StringBuffer fixCharacterEncoding (String message) throws Exception
   {
     //A DIRTY FIX FOR WRONG CHAR ENCODING
     StringBuffer buf = new StringBuffer();
     byte[] bytes = message.toString().getBytes("iso-8859-1");
     for(int i=0;i<bytes.length;i++)
     {
       if(bytes[i] == -96)
       buf.append((char)32);        
       else
         if(bytes[i] < 127 && bytes[i] >= 0)
           buf.append((char)bytes[i]);
     }
   return buf;
   }
    
    /**
     * Method to send email with an attachement. Delete file as specified.
     * @param notificationVO
     * @param deleteFile
     * @throws Exception
     */
    public void notifyMultipart(NotificationVO notificationVO, boolean deleteFile) throws Exception{
        String host = notificationVO.getSMTPHost();
        String from = notificationVO.getMessageFromAddress();
        String subject = notificationVO.getMessageSubject();
        String content = notificationVO.getMessageContent();
        File fileAttachment = notificationVO.getFileAttachment();
        
        // Get system properties
        Properties props = System.getProperties();

        // Setup mail server
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.connectiontimeout", this.timeout);
        props.put("mail.smtp.timeout", this.timeout);

        // Get session
        Session session = 
          Session.getInstance(props, null);

        // Define message
        MimeMessage message = 
          new MimeMessage(session);
        message.setFrom(
          new InternetAddress(from));
        message.setSubject(
          subject);

        if (hasMultipleAddresses(Message.RecipientType.TO, notificationVO))  {
          Address toAddress[] = handleMultipleAddresses(Message.RecipientType.TO, notificationVO);
          message.addRecipients(Message.RecipientType.TO, toAddress);
        } else {
          if (   (   notificationVO.getMessageTOAddress() != null   )
              && ( ! notificationVO.getMessageTOAddress().equals("")) )  {
              message.addRecipient(Message.RecipientType.TO,
              new InternetAddress(notificationVO.getMessageTOAddress()));
          }
        }

        // create the message part 
        MimeBodyPart messageBodyPart = 
          new MimeBodyPart();

        //fill message
        messageBodyPart.setText(content);

        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(messageBodyPart);

        // Part two is attachment
        messageBodyPart = new MimeBodyPart();
        DataSource source = 
          new FileDataSource(fileAttachment);
        messageBodyPart.setDataHandler(
          new DataHandler(source));
        messageBodyPart.setFileName(fileAttachment.getName());       
        multipart.addBodyPart(messageBodyPart);

        // Put parts in message
        message.setContent(multipart);

        // Send the message
        Transport.send( message );
        
        if(deleteFile) {
            fileAttachment.delete();
        }
    }
  /*
  public static void main(String[] args) {
    try  {
      NotificationUtil util = com.ftd.osp.utilities.j2ee.NotificationUtil.getInstance();
      NotificationVO vo = new NotificationVO();
      vo.setMessageFromAddress("rlazuk@ftdi.com");
      vo.setMessageTOAddress("rlazuk@ftdi.com");
      vo.setMessageCCAddress("ssecivan@ftdi.com");
      vo.setAddressSeparator(";");
      vo.setMessageSubject("Test Message from Notification Util");
      vo.setSMTPHost("mailserver.ftdi.com");
      vo.setMessageMimeType("text/html");
      vo.setMessageContent("<b>Test Message Body</b> <a href='http://www.ftd.com'>FTD.COM</a> <img src='http://a80.g.akamai.net/f/80/71/6h/www.ftd.com/pics/promo_home_A5-A2.jpg '> ");
      util.notify(vo);

    } catch (Exception ex)  {
      ex.printStackTrace();
    } finally  {
    }

  }
  */

}