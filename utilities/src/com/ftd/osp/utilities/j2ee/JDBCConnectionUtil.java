package com.ftd.osp.utilities.j2ee;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.io.File;
import java.io.IOException;

import java.net.URL;
import java.net.URLDecoder;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import java.util.Properties;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;

import org.w3c.dom.Element;

import org.xml.sax.SAXException;

/**
 * Provides access to a JDBC connection object
 *
 * @author Anshu Gaind
 */
public class JDBCConnectionUtil {
  private static Logger logger = new Logger("com.ftd.osp.utilities.j2ee.JDBCConnectionUtil");
  private static JDBCConnectionUtil JDBCCONNECTIONUTIL = new JDBCConnectionUtil();
  private static final String configFile = "jdbcconnection.xml";
  private static Document config;
  private static boolean SINGLETON_INITIALIZED;
  
  
 /**
  * The static initializer for the JDBCConnectionPoolUtil. It ensures that
  * at a given time there is only a single instance  
  * 
  * @return the JDBCConnectionPoolUtil
  **/
  public static JDBCConnectionUtil getInstance()  throws Exception{
    if (! SINGLETON_INITIALIZED ){
        initConfig();
    }
    return JDBCCONNECTIONUTIL;
  }
  
  /**
   * The private Constructor
   **/
  private JDBCConnectionUtil() {
    super();
  }

  /**
   * Initialize the configuration
   */
  private static synchronized void initConfig() 
        throws IOException, SAXException, ParserConfigurationException {
    if (! SINGLETON_INITIALIZED) 
    {            
      URL url = ResourceUtil.getInstance().getResource(configFile);
      if (url != null) 
      {
        File f = new File(new URLDecoder().decode(url.getFile(), "UTF-8"));
        config = (Document)DOMUtil.getDocument(f); 
        SINGLETON_INITIALIZED = true;
      }
      else
      {
        throw new IOException("The configuration file was not found.");      
      }        
    }
  }
   

  /**
   * Returns a connection from the connection pool, for the specified data source,
   * and from the specified initial context location.
   *
   * @param dataSource the name of the data source
   * @return connection to the database
   * @author Anshu Gaind
   **/
  public Connection getConnection(String connectionName) throws Exception{
      Connection con = null;
      String userName = null, password = null, jdbcDriver = null, jdbcUrl = null;
      Element connectionE = null;
      
      if ( (connectionName == null) || (connectionName.equals("")) ){
        logger.error("JDBCConnectionPoolUtil:: connectionName  has no value ");
        throw new Exception("JDBCConnectionPoolUtil:: connectionName  has no value ");
      }
      connectionE = ( (Element) DOMUtil.selectSingleNode(config,  "//connection[@name='" + connectionName + "']"));
      userName = connectionE.getAttribute("username");
      if ( (userName == null) || (userName.equals("")) ){
        logger.error("JDBCConnectionPoolUtil:: UserName has no value ");
        throw new Exception("JDBCConnectionPoolUtil:: UserName has no value ");
      }
      
      password = connectionE.getAttribute("password");
      if ( (password == null) || (password.equals("")) ){
        logger.error("JDBCConnectionPoolUtil:: Password has no value ");
        throw new Exception("JDBCConnectionPoolUtil:: Password has no value ");
      }
      jdbcDriver = connectionE.getAttribute("jdbc-driver");
      if ( (password == null) || (password.equals("")) ){
        logger.error("JDBCConnectionPoolUtil:: jdbc-driver has no value ");
        throw new Exception("JDBCConnectionPoolUtil:: jdbc-driver has no value ");
      }
      jdbcUrl = connectionE.getAttribute("jdbc-url");
      if ( (password == null) || (password.equals("")) ){
        logger.error("JDBCConnectionPoolUtil:: jdbc-url has no value ");
        throw new Exception("JDBCConnectionPoolUtil:: jdbc-url has no value ");
      }
      
      Properties p = new Properties();
      p.put("user", userName);
      p.put("password", password);
      Driver driver =  (Driver)Class.forName(jdbcDriver).newInstance();
      con = driver.connect(jdbcUrl, p);
      logger.debug("Opening Connection to: " + jdbcUrl + " as " + userName );

      return con;

  }


  /**
   * Closes all open JDBC resources
   *
   * @param resultSet the result set
   * @param statement the statement
   * @param connection the connection 
   * @throws BadConnectionException
   * @returns whether or not all resources were closed
   **/
  public void close(ResultSet resultSet, 
                                 Statement statement, 
                                Connection connection) throws SQLException {
      try{
        if (resultSet != null){
          resultSet.close();
          logger.debug("Closing ResultSet");
        }
        if (statement != null){
          statement.close();
          logger.debug("Closing Statement");
        }
        if (connection != null){
          connection.close();
          logger.debug("Closing Connection");
        }
      } catch (SQLException se){
         logger.error(se);
         throw se;
      }
  }

/*  
  public static void main(String[] args) {
    try  {
    JDBCConnectionUtil dsu = com.ftd.osp.utilities.j2ee.JDBCConnectionUtil.getInstance();
      Connection con = dsu.getConnection("USER1");
      dsu.close(null, null, con);
      con = dsu.getConnection("USER1");
      dsu.close(null, null, con);
    } catch (Exception ex)  {
      ex.printStackTrace();
    } finally  {
    }
    
  }
  
*/    
 
}
