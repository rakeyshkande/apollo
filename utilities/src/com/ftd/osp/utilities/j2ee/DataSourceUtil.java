package com.ftd.osp.utilities.j2ee;

import com.ftd.osp.utilities.j2ee.InitialContextUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.net.URLDecoder;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import java.util.Hashtable;

import javax.naming.InitialContext;

import javax.sql.DataSource;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

/**
 * Provides access to a JDBC data source object
 *
 * @author Anshu Gaind
 */
public class DataSourceUtil {
  private static Logger logger  = new Logger("com.ftd.osp.utilities.j2ee.DataSourceUtil");
  private static DataSourceUtil DATASOURCEUTIL = new DataSourceUtil();
  private static final String configFile = "datasource.xml";
  private static Document config;
  private static boolean SINGLETON_INITIALIZED = false;
  
  private static boolean useInitialContextUtil;
  
  /**
   * Cache datasources
   */
  private static Hashtable cache = new Hashtable();

 /**
  * The static initializer for the data source util. It ensures that
  * at a given time there is only a single instance
  *
  * @return the data source util
  **/
  public static DataSourceUtil getInstance()  throws Exception{
    if (! SINGLETON_INITIALIZED ){
        initConfig();
    }
    return DATASOURCEUTIL;
  }

  /**
   * The private Constructor
   **/
  private DataSourceUtil(){
    super();
  }

  /**
   * Initialize the configuration
   * 
   * @throws java.io.IOException
   * @throws org.xml.sax.SAXException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws oracle.xml.parser.v2.XSLException
   */
  private synchronized static void initConfig()
        throws IOException, SAXException, ParserConfigurationException, Exception {
    if (! SINGLETON_INITIALIZED)
    {
      logger.info("initConfig. singleton not initialized");
      URL url = ResourceUtil.getInstance().getResource(configFile);
 
      if (url != null)
      {
        File f = new File(new URLDecoder().decode(url.getFile(), "UTF-8"));
        try {
            config = (Document)DOMUtil.getDocument(f);
            logger.info("Found config file:" + f.getPath());
        } catch (FileNotFoundException e)
        {
          // JP Puzon 08-15-2005
          // Added this functionality to accomodate resources which
          // are not physically located on the local filesystem.
       
          logger.info("File not found. Getting resource as stream...");
          config = (Document)DOMUtil.getDocument(ResourceUtil.getInstance().getResourceAsStream(configFile));
          StringWriter sw = new StringWriter();
          DOMUtil.print(config, (new PrintWriter(sw)));
          logger.info("got config file:" + sw.toString());
        }
        NamedNodeMap map = DOMUtil.selectSingleNode(config, "/data-sources").getAttributes();
        Node node = map.getNamedItem("use-initialcontextutil");
        String value = (node != null)?node.getNodeValue():"";
        useInitialContextUtil = (value == null || value.equals("") || value.equals("true") )?true:false;
        SINGLETON_INITIALIZED = true;
      }
      else
      {
        throw new IOException("The configuration file was not found.");
      }
    }

  }


  /**
   * Returns a connection from the connection pool.
   *
   * @param dataSource the name of the data source
   * @param initialContext
   * @return connection to the database
   * @author Anshu Gaind
   **/
  public Connection getConnection(String dataSource) throws Exception
  {
    InitialContext initialContext = (this.useInitialContextUtil)?InitialContextUtil.getInstance().getInitialContext("LOCALHOST"):new InitialContext();        
    return this.getConnection(dataSource, initialContext);
  }

 /**
  * Return the corresponding datasource object or null if not found
  *
  * @param dataSource the name of the data source
  * @param initialContext
  * @return the corresponding datasource object or null if not found
  */
  public DataSource getDataSource(String dataSource) throws Exception
  {    
    InitialContext initialContext = (this.useInitialContextUtil)?InitialContextUtil.getInstance().getInitialContext("LOCALHOST"):new InitialContext();
    return this.getDataSource(dataSource, initialContext);
  }



  /**
   * Returns a connection from the connection pool.
   *
   * @param dataSource the name of the data source
   * @param initialContext
   * @return connection to the database
   * @author Anshu Gaind
   **/
  public Connection getConnection(String dataSource, InitialContext initialContext) throws Exception{
      DataSource ds = null;
      Connection con = null;
      String userName = null, password = null, ejbLocation = null;
      Element dataSourceE = null;
      logger.debug("getConnection...");
      
      if ( (dataSource == null) || (dataSource.equals("")) ){
        logger.error("DataSourceUtil:: DataSource  has no value ");
        throw new Exception("DataSourceUtil:: DataSource  has no value ");
      }
      
      dataSourceE = ( (Element) DOMUtil.selectSingleNode(config, "//data-source[@name='" + dataSource + "']"));

      if (dataSourceE == null)
      {
          // JP Puzon 08-15-05
          // Added the explicit mention of dataSource & file contents below.
          StringWriter sw = new StringWriter();
          DOMUtil.print(config, (new PrintWriter(sw)));
          throw new Exception("The specified data source " + dataSource + " was not found in the configuration file " + sw.toString());
      }

      logger.debug("USING DATASOURCE:::" + dataSource + ":::");

      userName = dataSourceE.getAttribute("username");
      password = dataSourceE.getAttribute("password");

      ejbLocation = dataSourceE.getAttribute("ejb-location");
      if ( (ejbLocation == null) || (ejbLocation.equals("")) ){
          StringWriter sw = new StringWriter();
          DOMUtil.print(config, (new PrintWriter(sw)));
          logger.error("DataSourceUtil:: ejb-location has no value :" + sw.toString());
          
        throw new Exception("DataSourceUtil:: ejb-location has no value ");
      }


      // Try cache first
      if (cache.containsKey(dataSource))  {
          ds = (DataSource) cache.get(dataSource);
      } else {
        if (initialContext != null){
          ds = (DataSource)initialContext.lookup(ejbLocation);
          cache.put(dataSource, ds);
        } else {
          logger.error("Unable to obtain an Initial Context");
          throw new Exception("Unable to obtain an Initial Context");
        }
      }
      // get the connection
      if (     (userName == null)
            || (userName.equals(""))
            || (password == null)
            || (password.equals(""))     )
      {
        con = ds.getConnection();
      }
      else
      {
          con = ds.getConnection(userName, password);
      }

      return con;

  }


 /**
  * Return the corresponding datasource object or null if not found
  *
  * @param dataSource the name of the data source
  * @param initialContext
  * @return the corresponding datasource object or null if not found
  */
  public DataSource getDataSource(String dataSource, InitialContext initialContext) throws Exception
  {

      DataSource ds = null;
      Connection con = null;
      String userName = null, password = null, ejbLocation = null;
      Element dataSourceE = null;

      if ( (dataSource == null) || (dataSource.equals("")) ){
        logger.error("DataSourceUtil:: DataSource  has no value ");
        throw new Exception("DataSourceUtil:: DataSource  has no value ");
      }

      dataSourceE = ( (Element) DOMUtil.selectSingleNode(config, "//data-source[@name='" + dataSource + "']"));

      if (dataSourceE == null)
      {
          // JP Puzon 08-15-05
          // Added the explicit mention of dataSource & file contents below.
          StringWriter sw = new StringWriter();
          DOMUtil.print(config,(new PrintWriter(sw)));
          throw new Exception("The specified data source " + dataSource + " was not found in the configuration file " + sw.toString());
      }

      userName = dataSourceE.getAttribute("username");
      password = dataSourceE.getAttribute("password");

      ejbLocation = dataSourceE.getAttribute("ejb-location");

      if ( (ejbLocation == null) || (ejbLocation.equals("")) ){
        logger.error("DataSourceUtil:: ejb-location has no value ");
        throw new Exception("DataSourceUtil:: ejb-location has no value ");
      }

      // Try cache first
      if (cache.containsKey(dataSource))  {
          ds = (DataSource) cache.get(dataSource);
      } else {
        if (initialContext != null){
          ds = (DataSource)initialContext.lookup(ejbLocation);
          cache.put(dataSource, ds);
        } else {
          logger.error("Unable to obtain an Initial Context");
          throw new Exception("Unable to obtain an Initial Context");
        }
      }

      return ds;

  }


 /**
   * Closes all open JDBC resources
   *
   * @param resultSet the result set
   * @param statement the statement
   * @param connection the connection
   * @throws BadConnectionException
   * @returns whether or not all resources were closed
   **/
  public void close(ResultSet resultSet,
                                 Statement statement,
                                Connection connection) throws SQLException {
      try{
        if (resultSet != null){
          resultSet.close();
          logger.debug("Closing ResultSet");
        }
        if (statement != null){
          statement.close();
          logger.debug("Closing Statement");
        }
        if (connection != null){
          connection.close();
          logger.debug("Closing Connection");
        }
      } catch (SQLException se){
         logger.error(se);
         throw se;
      }
  }


  /**
   * Returns a connection from the connection pool, for the specified data source,
   * and from the specified initial context location.
   *
   * @param dataSource the name of the data source
   * @param initialContextLocation
   * @return connection to the database
   * @author Anshu Gaind
   * @deprecated
   **/
  public Connection getConnection(String dataSource, String initialContextLocation) throws Exception{
      DataSource ds = null;
      Connection con = null;
      String userName = null, password = null, ejbLocation = null;
      Element dataSourceE = null;

      if ( (dataSource == null) || (dataSource.equals("")) ){
        logger.error("DataSourceUtil:: DataSource  has no value ");
        throw new Exception("DataSourceUtil:: DataSource  has no value ");
      }
      dataSourceE = ( (Element) DOMUtil.selectSingleNode(config, "//data-source[@name='" + dataSource + "']"));
      if (dataSourceE == null)
      {
          // JP Puzon 08-15-05
          // Added the explicit mention of dataSource & file contents below.
          StringWriter sw = new StringWriter();
          DOMUtil.print(config, (new PrintWriter(sw)));
          throw new Exception("The specified data source " + dataSource + " was not found in the configuration file " + sw.toString());
      }


      userName = dataSourceE.getAttribute("username");
      password = dataSourceE.getAttribute("password");

      ejbLocation = dataSourceE.getAttribute("ejb-location");
      if ( (ejbLocation == null) || (ejbLocation.equals("")) ){
        logger.error("DataSourceUtil:: ejb-location has no value ");
        throw new Exception("DataSourceUtil:: ejb-location has no value ");
      }


      // Try cache first
      if (cache.containsKey(dataSource))  {
          ds = (DataSource) cache.get(dataSource);
      } else {
        InitialContext initialContext = InitialContextUtil.getInstance().getInitialContext(initialContextLocation);
        if (initialContext != null){
          ds = (DataSource)initialContext.lookup(ejbLocation);
          cache.put(dataSource, ds);
        } else {
          logger.error("Unable to obtain an Initial Context");
          throw new Exception("Unable to obtain an Initial Context");
        }
      }
      // get the connection
      if (     (userName == null)
            || (userName.equals(""))
            || (password == null)
            || (password.equals(""))     )
      {
        con = ds.getConnection();
      }
      else
      {
          con = ds.getConnection(userName, password);
      }


      return con;

  }

 /**
  * Return the corresponding datasource object or null if not found
  *
  * @param dataSource the name of the data source
  * @param initialContextLocation
  * @return the corresponding datasource object or null if not found
  * @deprecated
  */
  public DataSource getDataSource(String dataSource, String initialContextLocation) throws Exception
  {

      DataSource ds = null;
      Connection con = null;
      String userName = null, password = null, ejbLocation = null;
      Element dataSourceE = null;

      if ( (dataSource == null) || (dataSource.equals("")) ){
        logger.error("DataSourceUtil:: DataSource  has no value ");
        throw new Exception("DataSourceUtil:: DataSource  has no value ");
      }

      dataSourceE = ( (Element) DOMUtil.selectSingleNode(config, "//data-source[@name='" + dataSource + "']"));

      if (dataSourceE == null)
      {
          // JP Puzon 08-15-05
          // Added the explicit mention of dataSource & file contents below.
          StringWriter sw = new StringWriter();
          DOMUtil.print(config, (new PrintWriter(sw)));
          throw new Exception("The specified data source " + dataSource + " was not found in the configuration file " + sw.toString());
      }

      userName = dataSourceE.getAttribute("username");
      password = dataSourceE.getAttribute("password");

      ejbLocation = dataSourceE.getAttribute("ejb-location");

      if ( (ejbLocation == null) || (ejbLocation.equals("")) ){
        logger.error("DataSourceUtil:: ejb-location has no value ");
        throw new Exception("DataSourceUtil:: ejb-location has no value ");
      }

      // Try cache first
      if (cache.containsKey(dataSource))  {
          ds = (DataSource) cache.get(dataSource);
      } else {
        InitialContext initialContext = InitialContextUtil.getInstance().getInitialContext(initialContextLocation);
        if (initialContext != null){
          ds = (DataSource)initialContext.lookup(ejbLocation);
          cache.put(dataSource, ds);
        } else {
          logger.error("Unable to obtain an Initial Context");
          throw new Exception("Unable to obtain an Initial Context");
        }
      }

      return ds;

  }







}
