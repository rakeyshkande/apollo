package com.ftd.osp.utilities.j2ee;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import javax.ejb.EJBHome;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import javax.rmi.PortableRemoteObject;


/**
 * <p>
 * Encapsulates all JNDI usage and hides the complexities of initial context
 * creation, EJBHome object lookup, and EJBObject recreation. Multiple projects
 * can reuse this class to reduce code complexity, provide a single point of
 * control, and improve performance through use of a caching facility (reducing
 * the number of JNDI lookups required).
 * </p>
 *
 * @author  Jason Weiss
 */
public final class EJBServiceLocator {
    /** The cache of EjbHomeLocation objects. */
    private static Map ejbHomeLocationCache = new HashMap();

    private EJBServiceLocator() {
        // do nothing
    }

    private static Object createBean(EJBHome ejbHome) throws Exception {
        Method createMethod = ejbHome.getClass().getMethod("create", null);

        return createMethod.invoke(ejbHome, null);
    }

    public static Object getEJBean(String jndiHomeName, Class homeClass,
        String initialContextFactory, String ejbProviderUrl,
        boolean useDomainFix) {
        Integer ejbHomeId = new Integer(getEJBHomeId(jndiHomeName,
                    ejbProviderUrl));
        EJBHomeLocation ehl = (EJBHomeLocation) ejbHomeLocationCache.get(ejbHomeId);

        Object bean = null;

        if (ehl == null) {
            try {
                ehl = new EJBHomeLocation(jndiHomeName, homeClass,
                        initialContextFactory, ejbProviderUrl, useDomainFix);
                ejbHomeLocationCache.put(ejbHomeId, ehl);

                bean = createBean(ehl.getEJBHome());
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        } else {
            try {
                bean = createBean(ehl.getEJBHome());
            } catch (Exception e1) {
                // Synchronize on the current EJBHomeLocation object to prevent race conditions.
                synchronized (ehl) {
                    // Attempt three times to obtain a valid connection.
                    short count = 0;

                    do {
                        try {
                            // Re-initialize the connection within the cached object.
                            ehl.initEJBHome();
                            bean = createBean(ehl.getEJBHome());

                            // Break from the loop if successful.
                            break;
                        } catch (Throwable t) {
                            count++;

                            if (count == 3) {
                                throw new RuntimeException(t);
                            }
                        }
                    } while (count < 3);
                }
            }
        }

        return bean;
    }

    private static int getEJBHomeId(String jndiHomeName, String ejbProviderUrl) {
        return (jndiHomeName + ejbProviderUrl).hashCode();
    }
}


class EJBHomeLocation {
    private Hashtable icProps = new Hashtable(5);
    private EJBHome ejbHome = null;
    private String jndiHomeName = null;
    private Class homeClass = null;

    EJBHomeLocation(String jndiHomeName, Class homeClass,
        String initialContextFactory, String ejbProviderUrl,
        boolean useDomainFix) throws NamingException {
        icProps.put(Context.INITIAL_CONTEXT_FACTORY, initialContextFactory);
        icProps.put(Context.SECURITY_CREDENTIALS, "welcome");
        icProps.put(Context.SECURITY_PRINCIPAL, "admin");
        icProps.put(Context.PROVIDER_URL, ejbProviderUrl);

        // put in to fix an domain issue when using from inside a servlet
        // see: http://www.oracle.com/technology/tech/java/oc4j/904/collateral/OC4J-FAQ-EJB-904.html 
        //      #15 for details
        if (useDomainFix) {
            icProps.put("dedicated.rmicontext", "true");
        }

        this.jndiHomeName = jndiHomeName;
        this.homeClass = homeClass;
        this.ejbHome = initEJBHome(icProps, jndiHomeName, homeClass);
    }

    private EJBHome initEJBHome(Hashtable icProps, String jndiHomeName,
        Class homeClass) throws NamingException {
        InitialContext ic = new InitialContext(icProps);

        return (EJBHome) PortableRemoteObject.narrow(ic.lookup(jndiHomeName),
            homeClass);
    }

    void initEJBHome() throws NamingException {
        this.ejbHome = initEJBHome(this.icProps, this.jndiHomeName,
                this.homeClass);
    }

    Hashtable getInitialContextProps() {
        return icProps;
    }

    EJBHome getEJBHome() {
        return ejbHome;
    }
}
