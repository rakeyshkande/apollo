/**
 * 
 */
package com.ftd.osp.utilities.id.vo;

/**
 * @author smeka
 *
 */
public class IdRequestVO {
	
	private String clientName;
	
	private String requestType;
	
	public IdRequestVO(String clientName, String requestType) {
		super();
		this.clientName = clientName;
		this.requestType = requestType;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getRequestType() {
		return requestType;
	}

	public void setRequestType(String requestType) {
		this.requestType = requestType;
	} 

}
