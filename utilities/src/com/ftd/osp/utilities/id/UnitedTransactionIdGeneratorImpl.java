/**
 * 
 */
package com.ftd.osp.utilities.id;

import java.math.BigDecimal;
import java.sql.Connection;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.id.vo.IdRequestVO;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * @author smeka
 *
 */
public class UnitedTransactionIdGeneratorImpl implements IdGenerator {
	
	public static final String GET_NEXT_UNITED_TRANSACTION_SEQ = "GET_NEXT_UNITED_TRANSACTION_SEQ";
	
	private static Logger logger = new Logger(UnitedTransactionIdGeneratorImpl.class.getName());

	/** Method to get the svs transaction id in required format.
	 * It will invoke GET_NEXT_UNITED_TRANSACTION_SEQ in schema GLOBAL and package GLOBAL_PKG.
	 * This is to get the next value of the sequence CLEAN.UNITED_TRANS_ID_SEQ
	 * @param idRequestVO
	 * @param conn
	 * @return
	 */
	public String execute(IdRequestVO idRequestVO, Connection conn) throws Exception {
		
		BigDecimal txid = null;
		DataRequest dataRequest = new DataRequest();

		try {
			dataRequest = new DataRequest();
			dataRequest.setConnection(conn);
			dataRequest.setStatementID(GET_NEXT_UNITED_TRANSACTION_SEQ);
			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
			txid = ((BigDecimal) dataAccessUtil.execute(dataRequest));			
			
		} catch (Exception e) {
			logger.error("Error caught retrieving SVS transaction ID : " + e.getMessage());
			throw new Exception("Error caught retrieving SVS transaction ID : " + e.getMessage());
		} 
		
		//prepend the txid with zeros up to 9 characters
		String unitedTxId = txid.toString();
		while (unitedTxId.length() < 9) {
			unitedTxId = "0"+unitedTxId;
		}
		
		return unitedTxId;
		
	}

}
