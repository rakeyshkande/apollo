/**
 * 
 */
package com.ftd.osp.utilities.id;

import java.sql.Connection;

import com.ftd.osp.utilities.id.vo.IdRequestVO;

/**
 * @author smeka
 * 
 */

public interface IdGenerator { 
	public String execute(IdRequestVO idRequestVO, Connection conn) throws Exception;
}
