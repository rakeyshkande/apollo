/**
 * 
 */
package com.ftd.osp.utilities.id;

import com.ftd.osp.utilities.id.vo.IdRequestVO;

/**
 * @author smeka
 * 
 */
public class IdGeneratorFactory {
	
	public final static String SVS_REQUEST_TYPE = "SVS_TRANS_ID";
	public final static String UNITED_REQUEST_TYPE = "UNITED_TRANS_ID";
	

	/** generates the instance of a specific IdGenerator implementation class
	 * based on the request type of IDrequestVO 
	 * @param idRequestVO
	 * @return
	 */
	public static IdGenerator getGenerator(IdRequestVO idRequestVO) {
		IdGenerator idGenerator = null;
		if (idRequestVO != null && idRequestVO.getRequestType().equals(SVS_REQUEST_TYPE)) {
			idGenerator = new SVSTransactionIdGeneratorImpl();
		} else if (idRequestVO != null && idRequestVO.getRequestType().equals(UNITED_REQUEST_TYPE)) {
			idGenerator = new UnitedTransactionIdGeneratorImpl();
		}
		return idGenerator;
	}
}
