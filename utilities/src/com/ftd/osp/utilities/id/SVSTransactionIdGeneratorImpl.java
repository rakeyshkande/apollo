/**
 * 
 */
package com.ftd.osp.utilities.id;

import java.math.BigDecimal;
import java.sql.Connection;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.id.vo.IdRequestVO;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * @author smeka
 *
 */
public class SVSTransactionIdGeneratorImpl implements IdGenerator {
	
	public static final String SVS_TRANS_ID_PREFIX_CONTEXT = "SERVICE";
	
	public static final String SVS_TRANSACTION_ID_PREFIX = "SVS_TRANSACTION_ID_PREFIX";
	
	public static final String ID_REQUEST_SVS_CLIENT_CONTEXT = "GLOBAL_CONFIG";
	
	public static final String ID_REQUEST_SVS_CLIENT = "ID_REQUEST_SVS_CLIENT_";
	
	public static final String GET_NEXT_SVS_TRANSACTION_SEQ = "GET_NEXT_SVS_TRANSACTION_SEQ";
	
	private static Logger logger = new Logger("com.ftd.osp.utilities.id.SVSTransactionIdGeneratorImpl");

	/** Method to get the svs transaction id in required format.
	 * @param idRequestVO
	 * @param conn
	 * @return
	 */
	public String execute(IdRequestVO idRequestVO, Connection conn) throws Exception {
		
		StringBuffer svsTransactionId = new StringBuffer("");
		
		 try {
			 /* Step 1 : 
			 Retrieve from global parameters �SVS_TRANSACTION_ID_PREFIX� from context SERVICE. 
			 Use this as digit 1 through 6. 
			 Use com.ftd.osp.utilities.ConfigurationUtil.getFrpGlobalParm */
			 svsTransactionId.append(ConfigurationUtil.getInstance().getFrpGlobalParm(SVS_TRANS_ID_PREFIX_CONTEXT,SVS_TRANSACTION_ID_PREFIX));
			 logger.debug(" SVS_TRANSACTION_ID_PREFIX : " + svsTransactionId);
					
			/* Step 2:
			 * Retrieve from global parameters �ID_REQUEST_SVS_CLIENT_� + idRequestVO.getClientName() from context GLOBAL_CONFIG. 
			 * Use this as digit 7. Use com.ftd.osp.utilities.ConfigurationUtil.getFrpGlobalParm */
			 svsTransactionId.append(ConfigurationUtil.getInstance().getFrpGlobalParm(ID_REQUEST_SVS_CLIENT_CONTEXT,(ID_REQUEST_SVS_CLIENT + idRequestVO.getClientName())));
			 logger.debug(" APPENDED ID_REQUEST_SVS_CLIENT : " + svsTransactionId);
			
			/* Step 3:
			 * Invoke key.keyglobal.global_pkg.get_next_svs_transaction_seq to get the next transaction id sequence. 
			 * Pad left with 0�s to make a 9 digit number. 
			 * Use this for digit 8 through 16
			 */
			 BigDecimal transId = getSvsTransactionId(conn);
			 if(transId == null || transId.doubleValue() <= 0) {
				throw new Exception("Invalid Transaction Id returned");
			 }
			 String formattedString = String.format("%09d", transId.intValue());			 
			 svsTransactionId.append(formattedString);
			 logger.debug(" APPENDED SVS_TRANS_ID_SEQ : " + svsTransactionId);
			 
			 return svsTransactionId.toString();
			 
		 } catch(Exception e) {
			 logger.error("Error caught in execute() : " + e.getMessage());
			 throw new Exception("Error caught in execute() : " + e.getMessage());			 
		 }
		
	}
	

    /** Convenient method to make a call to the Function "GET_NEXT_SVS_TRANSACTION_SEQ" in schema GLOBAL and package GLOBAL_PKG.
     *  This is to get the next value of the sequence CLEAN.SVS_TRANS_ID_SEQ
     * @param connection
     * @return
     */
    public BigDecimal getSvsTransactionId(Connection connection) throws Exception {
    	BigDecimal svsTransactionId = null;
		DataRequest dataRequest = new DataRequest();

		try {
			dataRequest = new DataRequest();
			dataRequest.setConnection(connection);
			dataRequest.setStatementID(GET_NEXT_SVS_TRANSACTION_SEQ);
			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
			svsTransactionId = ((BigDecimal) dataAccessUtil.execute(dataRequest));			
			
		} catch (Exception e) {
			logger.error("Error caught retrieving SVS transaction ID : " + e.getMessage());
			throw new Exception("Error caught retrieving SVS transaction ID : " + e.getMessage());
		} 
		return svsTransactionId;
	}
}
