package com.ftd.osp.utilities;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;


public class SegmentationUtil  {
  private static Logger logger = new Logger("com.ftd.osp.utilities.SegmentationUtil");

  /**
   * Obtains and returns customer tier information given a customer id
   * 
   * @param connection Database connection
   * @param customerId The clean database customer id
   * @exception Exception
   * @return The customer tier value
   */
  public String getCustomerTier(Connection conn, String customerId) throws Exception
  {
    String tier = null;

    if(logger.isDebugEnabled())
    {
        logger.debug("IN_CUSTOMER_ID:" + customerId);
    }

    DataAccessUtil dau = DataAccessUtil.getInstance();
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(conn);
    dataRequest.addInputParam("IN_CUSTOMER_ID", customerId);
    dataRequest.setStatementID("GET_CUSTOMER_TIER");
    tier = (String)dau.execute(dataRequest); 
      
    return tier;
  }

  /**
   * Obtains and returns customer tier information given an order GUID
   * 
   * @param connection Database connection
   * @param customerId The clean database customer id
   * @exception Exception
   * @return The customer tier value
   */
  public String getCustomerTierByScrubOrderGUID(Connection conn, String orderGUID) throws Exception
  {
    String customerId = null;
    String tier = null;

    if(logger.isDebugEnabled())
    {
        logger.debug("IN_ORDER_GUID:" + orderGUID);
    }

    DataAccessUtil dau = DataAccessUtil.getInstance();
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(conn);
    dataRequest.addInputParam("IN_ORDER_GUID", orderGUID);
    dataRequest.setStatementID("GET_CLEAN_CUSTOMER_ID");
    customerId = (String)dau.execute(dataRequest); 
    
    tier = getCustomerTier(conn, customerId);  

    return tier;
  }   
}