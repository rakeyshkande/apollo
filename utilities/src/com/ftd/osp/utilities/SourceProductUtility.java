package com.ftd.osp.utilities;

import java.lang.reflect.Method;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.CacheUtil;
import com.ftd.osp.utilities.cacheMgr.constant.CacheMgrConstants;
import com.ftd.osp.utilities.cacheMgr.handlers.ContentHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.ProductAttrRestrVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.ProductMasterVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.SourceMasterVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.UpsellDetailVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.UpsellMasterVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.feed.FeedConstants;
import com.ftd.osp.utilities.feed.vo.ProductVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.ProductAttributeVO;


/**
 * This class has logic to determine if a product is allowed
 */
public class SourceProductUtility
{
  
  private static Logger logger = new Logger("com.ftd.osp.utilities.SourceProductUtility");
  //SQL query constants
  private String GET_PRODUCT_ATTR_SOURCE_EXCL = "GET_PRODUCT_ATTR_SOURCE_EXCL";
  private String GET_PRODUCT_BY_ID_STATEMENT = "GET_PRODUCT_BY_ID";
  private String IS_PRODUCT_ALLOWED_BY_INDEX = "IS_PRODUCT_ALLOWED_BY_INDEX";
  private String EXCLUDE_INDEX_NAME = "exclude_products";
  private String NON_SEARCH_INDEX_NAME = "nonsearchitems";
  private String LIMIT_INDEX_NAME = "limit_products";
  private String DELIVERY_TYPE_DOMESTIC = "D";
  private CacheUtil cacheUtil = null;

  public SourceProductUtility() {
    cacheUtil = CacheUtil.getInstance();
  }
  /**
   * The method returns an attribute restriction id if the product contains attribute values
   * restriced by the source code. Returns null if no restrictions apply.
   * @param sourceCode
   * @param productId
   * @param conn
   * @return ServiceChargeVO with fees
   */
  public String checkProductAttributeRestriction(com.ftd.osp.utilities.cacheMgr.handlers.vo.ProductMasterVO productVO, 
                                                  List<String> productAttrRestrBySourceList,
                                                  Map<String, com.ftd.osp.utilities.cacheMgr.handlers.vo.ProductAttrRestrVO> productAttrRestrMap) 
         throws Exception
  {
    if (logger.isDebugEnabled())
    {
      logger.debug("checkProductAttributeRestriction called.");
    }

    String errorMessage = null;
    String productAttrRestrIdMatched = null;
    
    if(productAttrRestrBySourceList != null) {
        for(int i = 0; i < productAttrRestrBySourceList.size(); i++) {
            String attrRestrId = productAttrRestrBySourceList.get(i);
            
            if(logger.isDebugEnabled()) {
              logger.debug("attrRestrId:" + attrRestrId);
            }
            
            // Retrieve attribute restriction object by id
            ProductAttrRestrVO key = new ProductAttrRestrVO();
            key.setProductAttrRestrId(attrRestrId);
            ProductAttrRestrVO attr = productAttrRestrMap.get(key);
            if(attr == null) {
                errorMessage = "Cannot find attribute restriction id:" + attrRestrId;
                throw new Exception(errorMessage);
            } else {
                String attrRestrOper = attr.getProductAttrRestrOper();
                String attrRestrValue = attr.getProductAttrRestrValue();
                String javaMethodName = attr.getJavaMethodName();

                // Dynamically get the value from object for the java method.
                // Called methods have no argument
                Class params[] = {};
                Object paramsObj[] = {};

                // Get the class
                Class cl = productVO.getClass();
                // Get the method
                Method mthd=cl.getMethod(javaMethodName, params);
                // Only supports product attribute value of type String.
                String productAttrValue = (String)mthd.invoke(productVO, paramsObj);
                
                if(logger.isDebugEnabled()) {
                  logger.debug("productAttrValue:" + productAttrValue);
                  logger.debug("attrRestrValue:" + attrRestrValue);
                }
                
                // Currently only "=" and "IS NOT NULL" operations are supported. (REF: ftd_apps.product_attr_restr.product_attr_restr_oper)
                // If a new type is added to the table, then this section need to be enhanced.
                if("=".equals(attrRestrOper)) {
                    if(attrRestrValue.equals(productAttrValue)) {
                        // ProductMasterVO passed in attribute value matched restricted value. Product should be excluded.
                        productAttrRestrIdMatched = attrRestrId;
                        if(logger.isDebugEnabled()) {
                          logger.debug("attribute mapping matched:" + javaMethodName + ":" + attrRestrValue);
                        }
                        break; // no need to evaluate other attributes.
                    }
                } else if ("IS NOT NULL".equals(attrRestrOper)) {
                    if(productAttrValue != null && productAttrValue.length() > 0) {
                        productAttrRestrIdMatched = attrRestrId;
                        if(logger.isDebugEnabled()) {
                          logger.debug("attribute mapping matched:" + javaMethodName + ":" + attrRestrValue);
                        }
                        break; // no need to evaluate other attributes.
                    }
                }

            }
        }
    }
    
    if(logger.isDebugEnabled()) {
      logger.debug("checkProductAttributeRestriction returning: " + productAttrRestrIdMatched);
    }
    
    return productAttrRestrIdMatched;
  }
  
    /**
     * The method checks if the product is restricted from the source code based on the product source relationship.
     * The method returns -1 if product is not allowed on this source code. Returns null otherwise.
     * @param sourceCode
     * @param sourceList - list of source codes that are only allowed for this product.
     * @return
     * @throws Exception
     */
    public String checkProductSourceRestriction(String sourceCode, List<String> sourceList) throws Exception {
        boolean excludeByProductSource = false;
        
        if(sourceList != null && sourceList.size() > 0) {
            
            excludeByProductSource = true;
            for(int i = 0; i < sourceList.size(); i++) {
                String pSource = sourceList.get(i);
                if(logger.isDebugEnabled()) {
                  logger.debug("product is only allowed on source code:" + pSource);
                }
                if(pSource.equals(sourceCode)) {
                    // Product is allowed on this source code
                    excludeByProductSource = false;
                    break;
                }
            }
        }

        if(logger.isDebugEnabled()) {
          logger.debug("excludeByProductSource value:" + excludeByProductSource);
        }
        
        if (excludeByProductSource) {
            return "-1";
        }
        return null;
        
    }
    
    /**
     * 
     * @param productId - this could be product id or upsell master id
     * @param sourceExcludeProductList
     * @param sourceLimtProductList
     * @return
     * @throws Exception
     */
     public String checkProductIndexRestriction (String productId, 
                                                 String sourceExcludeProductList,
                                                 String sourceLimitProductList,
                                                 boolean isUpsellMasterId
                                                 ) throws Exception 
     {
         boolean productLimited = false;
         String returnValue = null;
         boolean allChildSkuInLimitIndex = true;
         UpsellMasterVO upsellMaster = null;
         List<UpsellDetailVO> upsellDetailList = null;
         boolean isInLimitIndex = this.containsItem(sourceLimitProductList, productId);
         boolean isInExcludeIndex = this.containsItem(sourceExcludeProductList, productId);
         
         //Upsell master is considered not in the limit index unless
         //all upsell details are in the limit index.
         if(isUpsellMasterId) {
             upsellMaster = cacheUtil.getUpsellMasterByMasterId(productId);
         }
         
         if(sourceLimitProductList != null && sourceLimitProductList.length()> 0) {
            if(isInLimitIndex) {
                //If this is an upsell master id, all details must be in the limit index.
                if(upsellMaster != null) {
                         upsellDetailList = upsellMaster.getUpsellDetailList();
                         if(upsellDetailList != null) {
                             for(int j = 0; j < upsellDetailList.size(); j++) {
                                 String upsellDetailId = upsellDetailList.get(j).getUpsellDetailId();
                                 if(!containsItem(sourceLimitProductList, upsellDetailId)) {
                                     allChildSkuInLimitIndex = false;
                                     break;
                                 }
                             }
                         } 
                }
                 
                if(allChildSkuInLimitIndex) {
                    productLimited = true;
                }
                     
            }
             
             // If limit index exists and product is not in the limit index, product is not allowed on this source code.
             if (!productLimited) {
                 if(logger.isDebugEnabled()) {
                    logger.debug("Product id " + productId + " excluded because it's not in the limit index.");
                 }
                 returnValue = "-1";
             }
         }     
         // No need to check exclude index if limit index exists.
         else if(sourceExcludeProductList != null && sourceExcludeProductList.length() > 0) {
             // Only look at exclude list if limit list is empty.
                 if(isInExcludeIndex) {
                     if(logger.isDebugEnabled()) {
                        logger.debug("Product id " + productId + " excluded because it's in the exclude/nonsearch index.");
                     }
                     returnValue = "-1";
                 }
         }
            

         if(logger.isDebugEnabled()) {
           logger.debug("Product id " + productId + " checkProductIndexRestriction returning:" + returnValue);
         }
         
         return returnValue;
         
     }
    
    
    /**
     * main method to be called.
     * @param sourceCode
     * @param productId - product id could also be upsell master id
     * @param searchById - examine nonsearchitems index if searchById is false
     * @return
     * @throws Exception
     */
    public String getProductRestrictionMsg(Connection conn, String inSourceCode, String inProductId, boolean searchById, String deliveryType) throws Exception {

        CacheUtil cacheUtil = CacheUtil.getInstance();
        String sourceCode = inSourceCode;
        String productId = inProductId;
        String upsellMasterId = inProductId;
        boolean isUpsellMasterId = false;
        String sourceExcludeProductList;
        String sourceLimitProductList;
        String sourceNonSearchProductList;

        ProductMasterVO productVO = null;
        List<String> productSourceList = null;
        List<String> productAttrRestrBySourceList = null;
        Map productAttrRestrMap = null;
        CacheManager cacheMgr = CacheManager.getInstance();
        
        productSourceList = cacheUtil.getProductSource(productId);
        productAttrRestrBySourceList = cacheUtil.getProductAttrRestrBySource(sourceCode);
        SourceMasterVO sourceMasterVO = cacheUtil.getSourceCodeById(sourceCode);
        String orderSource = null;
        
        if(sourceMasterVO != null) {
            orderSource = sourceMasterVO.getOrderSource();
            
            if(orderSource != null && orderSource.equalsIgnoreCase("P")) {
                String relatedSourceCode = sourceMasterVO.getRelatedSourceCode();
                if(relatedSourceCode != null) {
                    if(logger.isDebugEnabled()) {
                        logger.debug("Switching source code from " + sourceCode + " to " + relatedSourceCode);
                    }
                    sourceCode = relatedSourceCode;
                }
            }
        } 
        
        productAttrRestrMap = cacheUtil.getProductAttrRestrMap();
        productVO = cacheUtil.getProductById(productId);
        if(productVO == null) {
            productVO = cacheUtil.getBaseProductByUpsellMasterId(upsellMasterId);
            productId = productVO.getProductId();
            isUpsellMasterId = true;
        }   

        // Check product source restriction. Product source restriction include product master
        // and upsell, both are returned in productSourceList.
        String errorCode = checkProductSourceRestriction(sourceCode, productSourceList);
        if(errorCode == null) {
            // Check product attribute restriction. If upsell, use base sku to check.
            errorCode = checkProductAttributeRestriction (productVO, productAttrRestrBySourceList, productAttrRestrMap);
            if(errorCode != null) {
            	if(logger.isDebugEnabled()) {
            		logger.debug("Product excluded by product attribute restriction: " + productId);
            	}
            }
            // International delivery type does not need special index filtering.
            // Null is considered international as not all countries in the index feed are in country master.
            if(errorCode == null && deliveryType != null && deliveryType.equalsIgnoreCase(DELIVERY_TYPE_DOMESTIC)) {
                // Retrieve cached filtering index information.
                sourceExcludeProductList = getCacheProductsByIndex(cacheUtil, sourceCode, EXCLUDE_INDEX_NAME);
                sourceLimitProductList = getCacheProductsByIndex(cacheUtil, sourceCode, LIMIT_INDEX_NAME);
                if(!searchById) {
                    sourceNonSearchProductList = getCacheProductsByIndex(cacheUtil, sourceCode, NON_SEARCH_INDEX_NAME);
                    if (sourceNonSearchProductList != null && sourceNonSearchProductList.length() > 0) {
                        if (sourceExcludeProductList != null && sourceExcludeProductList.length() > 0) {
                            sourceExcludeProductList = sourceExcludeProductList + "," + sourceNonSearchProductList;
                        } else {
                            sourceExcludeProductList = sourceNonSearchProductList;
                        }
                    }
                }
                // Exclusion by index check need to use upsell master id for upsells.
                errorCode = checkProductIndexRestriction(upsellMasterId, sourceExcludeProductList, sourceLimitProductList, isUpsellMasterId);
                if(errorCode != null) {
                	if(logger.isDebugEnabled()) {
                		logger.debug("Product excluded by product index restriction: " + upsellMasterId);
                	}
                }
            }
        } else {
        	if(logger.isDebugEnabled()) {
        		logger.debug("Product excluded by product source restriction: " + upsellMasterId);
        	}
        }

        String returnResult = null;
        if(errorCode != null && errorCode.length() > 0) {
            ContentHandler h = (ContentHandler)cacheMgr.getHandler(CacheMgrConstants.CACHE_NAME_CONTENT_WITH_FILTER);
            returnResult = h.getContentWithFilter("PRODUCT_ATTRIBUTE_EXCLUSION", "EXCLUSION_ERROR_MESSAGE", errorCode, null);
        }
        
        if(logger.isDebugEnabled()) {
          logger.debug("Product Id " + productId + " source code " + inSourceCode  + " ProductRestrictionMsg returning:" + returnResult + "(" + errorCode + ")");
        }
        
        return returnResult;
    }    
    
    /**
     * Retrieves the products (comma delimited) for given source code and index. 
     * The method gets the templates that the source code is associated wtih and looks for 
     * that index in each template. It returns products from the first template it finds
     * hence satisfying the hierarchy requirement (source,default,template,domain).
     * Source code association to domain is not saved in DB, rather implied. So if
     * not found then look in domain.
     * @param cacheUtil
     * @param sourceCode
     * @param indexName
     * @return
     * @throws Exception
     */
    private String getCacheProductsByIndex(CacheUtil cacheUtil, String sourceCode, String indexName) throws Exception {
        // retrieve list of templates the source code is associated with.
        List<String> templateIdList = cacheUtil.getSourceTemplates(sourceCode);
        logger.debug("in getCacheProductsByIndex(" + sourceCode + "," + indexName);
        String indexProducts = null;
        if(templateIdList != null) {
            for(int i = 0; i < templateIdList.size(); i++) {
                // If found in one template then quit. Template ids are ordered by hierarchy 
                String templateId = templateIdList.get(i);
                logger.debug("referenced template id:" + templateId);
                indexProducts = cacheUtil.getFilterIndexProducts(templateId, indexName);
                logger.debug("index products:" + indexProducts);
                if(indexProducts != null && indexProducts.length() > 0) {
                    break;
                }
            }
        }
        
        // Look in domain.
        if(indexProducts == null || indexProducts.length() == 0) {
            String defaultDomain = cacheUtil.getSourceCodeDefaultDomain(sourceCode);
            String domainTemplateId = cacheUtil.getTemplateIdByDomain(defaultDomain);
             indexProducts = cacheUtil.getFilterIndexProducts(domainTemplateId, indexName);
        }
        
        return indexProducts;
    }

    /**
     * The method returns an error message if the product contains attribute values
     * restriced by the source code. Returns null if no restrictions apply.
     * @param sourceCode
     * @param productId
     * @param conn
     * @return ServiceChargeVO with fees
     */
    public String getProductAttributeRestrictionMsg(String sourceCode, String productId, Connection conn) 
           throws Exception
    {
      if (logger.isDebugEnabled())
      {
        logger.debug("getProductAttributeRestrictionMsg called.");
      }
      
      ProductVO productVO = this.getProductDetail(conn, productId);
      List<ProductAttributeVO> productAttrList = this.getProductAttrSourceExcl(conn, sourceCode);
      String errorCode = null;
      String errorMessage = null;
      
      if(productAttrList != null && productVO != null) {
          for(int i = 0; i < productAttrList.size(); i++) {
              ProductAttributeVO pa = productAttrList.get(i);
              if("DELIVERY_TYPE".equals(pa.getProductAttributeName())) {
                  if((pa.getProductAttributeValue()).equals(productVO.getDeliveryType())) {
                      errorCode = pa.getProductAttributeId();
                      break;
                  }
              } else if("PRODUCT_TYPE".equals(pa.getProductAttributeName())) {
                  if((pa.getProductAttributeValue()).equals(productVO.getProductType())) {
                      errorCode = pa.getProductAttributeId();
                      break;
                  }
              } else if("OVER_21".equals(pa.getProductAttributeName())) {
                  if(productVO.isOver21Flag()) {
                      errorCode = pa.getProductAttributeId();
                      break;
                  }
              } else if("PERSONALIZATION_TEMPLATE_ID".equals(pa.getProductAttributeName())) {
                  if(productVO.getPersonalizationTemplate() != null && productVO.getPersonalizationTemplate().length() > 0) {
                      errorCode = pa.getProductAttributeId();
                      break;
                  }
              } else if("PRODUCT_SUB_TYPE".equals(pa.getProductAttributeName())) {
                  if((pa.getProductAttributeValue()).equals(productVO.getProductSubType())) {
                      errorCode = pa.getProductAttributeId();
                      break;
                  }
              } 
          }
      }
      
      if(errorCode != null) {
          // Run into an restriction of source code by product attribute. Send error message.
          ConfigurationUtil cu = ConfigurationUtil.getInstance();
          errorMessage = cu.getContentWithFilter(conn, "PRODUCT_ATTRIBUTE_EXCLUSION", "EXCLUSION_ERROR_MESSAGE", errorCode, null);
           
      }
      
      return errorMessage;
    }
    
    private ProductVO getProductDetail(Connection conn, String productId) throws Exception
    {
          ProductVO product = null;

          productId = productId.toUpperCase();
          DataRequest dataRequest = new DataRequest();
          CachedResultSet rs = null;

          try
          {
              HashMap inputParams = new HashMap();
              inputParams.put("IN_PRODUCT_ID", productId);
              
              /* build DataRequest object */
              dataRequest.setConnection(conn);
              dataRequest.setStatementID(GET_PRODUCT_BY_ID_STATEMENT);
              dataRequest.setInputParams(inputParams);
              
              /* execute the store prodcedure */
              DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance(); 
              rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
              
              if(rs!=null && rs.next())
              {
                  product = new ProductVO();
                  product.setProductId(rs.getString(FeedConstants.OE_PRODUCT_MASTER_PRODUCT_ID));
                  product.setProductType(rs.getString(FeedConstants.OE_PRODUCT_MASTER_PRODUCT_TYPE));
                  product.setDeliveryType(rs.getString(FeedConstants.OE_PRODUCT_MASTER_DELIVERY_TYPE));
                  product.setProductSubType(rs.getString(FeedConstants.OE_PRODUCT_MASTER_PRODUCT_SUB_TYPE));
                  product.setOver21Flag(StringUtils.equals("Y",rs.getString(FeedConstants.OE_PRODUCT_MASTER_OVER_21)));
                  product.setPersonalizationTemplate(rs.getString(FeedConstants.OE_PRODUCT_MASTER_PERSONALIZATION_TEMPLATE));
              }
          }
          catch(Exception e)
          {
              throw e;
          }

          logger.debug("SourceProductUtility:getProductDetail() End");
          return product;
     }
      
    /**
    * Retrieves the Product Source Attribute Exclusions from the database for the passed in Source Code.
    * (Do not cache this method as it is being used by {@link com.ftd.osp.utilities.order.dao.RecalculateOrderDAO}).
    * (If this data is going to be updated to return from the cache, then copy this code into the RecalculateOrderDAO)
    * @param conn
    * @param sourceCode
    * @return
    * @throws Exception
    */
     public List<ProductAttributeVO> getProductAttrSourceExcl(Connection conn, String sourceCode) throws Exception {
         DataRequest dataRequest = new DataRequest();
         CachedResultSet rs = null;
         List<ProductAttributeVO> productAttrList = null;
         try
         {
             HashMap inputParams = new HashMap();
             inputParams.put("IN_SOURCE_CODE", sourceCode);
             
             /* build DataRequest object */
             dataRequest.setConnection(conn);
             dataRequest.setStatementID(GET_PRODUCT_ATTR_SOURCE_EXCL);
             dataRequest.setInputParams(inputParams);
             
             /* execute the store prodcedure */
             DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance(); 
             rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
             
             if(rs != null) {
                  productAttrList = new ArrayList();
             
                  while (rs != null && rs.next()) {
                      ProductAttributeVO pa = new ProductAttributeVO();
                      pa.setProductAttributeId(rs.getString("PRODUCT_ATTR_RESTR_ID"));
                      pa.setProductAttributeName(rs.getString("PRODUCT_ATTR_RESTR_NAME"));
                      pa.setProductAttributeOperator(rs.getString("PRODUCT_ATTR_RESTR_OPER"));
                      pa.setProductAttributeValue(rs.getString("PRODUCT_ATTR_RESTR_VALUE"));
                      
                      productAttrList.add(pa);
                  }
             }
         }
         catch(Exception e)
         {
             throw e;
         }

         logger.debug("SourceProductUtility:getProductAttrSourceExcl() End");
         return productAttrList;
     }
     
    private boolean isProductAllowedByIndex(Connection conn, String sourceCode, String productId, boolean searchById) throws Exception {
        DataRequest dataRequest = new DataRequest();
        String isAllowed = null;
        boolean returnResult = false;

        try
        {
            HashMap inputParams = new HashMap();
            inputParams.put("IN_SOURCE_CODE", sourceCode);
            inputParams.put("IN_PRODUCT_ID", productId);
            inputParams.put("IN_SEARCH_BY_ID", (searchById? "Y" : "N"));
            
            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID(IS_PRODUCT_ALLOWED_BY_INDEX);
            dataRequest.setInputParams(inputParams);
            
            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance(); 
            isAllowed = (String) dataAccessUtil.execute(dataRequest);
            
            if(logger.isDebugEnabled()) {
                logger.debug("isAllowed (" + sourceCode + "," + productId + "):" + isAllowed);
            }
            returnResult = ("N".equals(isAllowed))? false : true;
            
            
        }
        catch(Exception e)
        {
            throw e;
        }

        logger.debug("SourceProductUtility:isProductAllowedByIndex() End");
        if(logger.isDebugEnabled()) {
            logger.debug("isProductAllowedByIndex(" + sourceCode + "," + productId + "," + searchById + ") returning " + returnResult + ".");
        }
        return returnResult;
    }     
    
    public Object executeQueryReturnObject(Connection conn, String statement, Map inputs) throws Exception {

        Object output = null;
        try {
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(conn);
            dataRequest.setStatementID(statement);
            dataRequest.setInputParams(inputs);

            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

            if(logger.isDebugEnabled())
              logger.debug("Calling statement: " + statement);
            output = dataAccessUtil.execute(dataRequest);
            dataRequest.reset();
        } finally {
            if (logger.isDebugEnabled()) {
                logger.debug("Exiting executeQueryReturnObject:" + statement);
            }
        }
        return output;

    }    
    
    /**
     * Call a procedure to get all products for given index.
     * @param conn
     * @param sourceCode
     * @param indexName
     * @param defaultDomain
     * @return
     * @throws Exception
     */
    public List<String> getIndexProductList(Connection conn, String sourceCode, String indexName, String defaultDomain) throws Exception
    {
        CachedResultSet output = null;
        List<String> productList = null;
        try {
            
            Map inputs = new HashMap();
            inputs.put("IN_SOURCE_CODE", sourceCode);
            inputs.put("IN_INDEX_NAME", indexName);
            inputs.put("IN_DEFAULT_DOMAIN", defaultDomain);
            
            if(logger.isDebugEnabled())
              logger.debug("Calling statement: GET_INDEX_PRODUCT_LIST");
              
            output = (CachedResultSet)executeQueryReturnObject(conn, "GET_INDEX_PRODUCT_LIST", inputs);

            if(output != null) {
                productList = new ArrayList<String>();
                while(output.next()) {
                    String productId = output.getString("product_id");
                    // List is sorted in order but may have duplicates.
                    if(!productList.contains(productId)) {
                        productList.add(productId);
                    } else if (logger.isDebugEnabled()) { 
                        logger.debug("Product duplicated: " + productId);
                    }
                }
            }
        } finally {
            if (logger.isDebugEnabled()) {
                logger.debug("Exiting getIndexProductList(" + sourceCode + "," + indexName + "," + defaultDomain);
            }
        }
        return productList;        
    
    }        

    private boolean containsItem(String itemList, String item) throws Exception {
    	boolean flag = false;
        if(itemList != null) {
        	if(logger.isDebugEnabled()){
        		logger.debug("Excluded product list : "+itemList);
        	}
        	List<String> items = new ArrayList<String> (Arrays.asList(itemList.split(",")));
    		for(String exclItem:items){
    			if(StringUtils.isNotEmpty(item) && StringUtils.isNotEmpty(exclItem) ){
    				if(item.trim().equalsIgnoreCase(exclItem.trim())){
    					flag = true;
    					continue;
    				}
    			}
    		}
            
        }
        return flag;
    }

}
