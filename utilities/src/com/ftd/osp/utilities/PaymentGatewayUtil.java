package com.ftd.osp.utilities;

import java.util.Date;
import org.apache.http.client.methods.HttpGet;
import com.ftd.osp.utilities.plugins.Logger;

public class PaymentGatewayUtil {

	private static Logger logger = new Logger("com.ftd.osp.utilities.PaymentGatewayUtil");
	private static final String DETOKENIZATION_SERVICE_URL = "PAYMENT_GATEWAY_DETOKENIZATION_URL";
	private static final String PAYMENT_GATEWAY_SVC_TIMEOUT = "PAYMENT_GATEWAY_SVC_TIMEOUT";
	private static final String PAYMENT_GATEWAY_SERVICE_SECRET = "PAYMENT_GATEWAY_SERVICE_SECRET";
	private static PaymentGatewayUtil util = new PaymentGatewayUtil();

	public static PaymentGatewayUtil getInstance() {
		return util;
	}

	/**
	 * Calls Detokenization Service to retrieve Credit Card details
	 *
	 * @param String tokenId
	 * @param String merchantReferenceId
	 * @return String responseString
	 */
	public static String getCreditCardDetailsFromToken(String tokenId, String merchantReferenceId) throws Exception {
		logger.info("getCreditCardDetailsFromToken() started with tokenId: " + tokenId + ", merchantReferenceId: " + merchantReferenceId);
		(new Date()).getTime();
		logger.info("getCreditCardDetailsFromToken: Detokenization Service request started at " + System.currentTimeMillis());

		ConfigurationUtil configurationUtil = ConfigurationUtil.getInstance();
		StringBuffer detokenizationURL = new StringBuffer(configurationUtil.getFrpGlobalParm("SERVICE", DETOKENIZATION_SERVICE_URL));
		detokenizationURL.append(tokenId).append("/");
		detokenizationURL.append(merchantReferenceId);

		HttpGet httpGet = new HttpGet(detokenizationURL.toString());

		int timeout = Integer.parseInt(RESTUtils.getConfigParamValue(PAYMENT_GATEWAY_SVC_TIMEOUT, false)) * 1000;
		RESTUtils utils = new RESTUtils("Payment Gateway", false, false);
		String responseString = utils.executeRequest(httpGet,  
				timeout, 
				RESTUtils.getConfigParamValue(PAYMENT_GATEWAY_SERVICE_SECRET, true));
		logger.info("Response : " + responseString.toString().replaceAll("\\d{12,19}", "XXXX"));

		return responseString;
	}
}