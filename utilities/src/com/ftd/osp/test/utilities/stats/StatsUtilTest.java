package com.ftd.osp.test.utilities.stats;

import junit.framework.*;
import com.ftd.osp.utilities.stats.*;
import com.ftd.osp.utilities.order.vo.*;
import java.util.*;

public class StatsUtilTest extends TestCase
{
    public StatsUtilTest(String name)
    {
        super(name);
    }

    public static Test suite()
    {
        TestSuite suite = new TestSuite("StatsUtilTest");
        suite.addTest(new StatsUtilTest("testStatsUtil"));
        suite.addTest(new StatsUtilTest("testStatsUtilNoSave"));
        
        return suite;
    }

//    public void testStatsUtilNoSave()
//    {
//        try
//        {
//            OrderVO order = getOrder(1);
//            StatsUtil statsUtil = new StatsUtil();      
//
//            statsUtil.setOrder(order);
//            statsUtil.setItem((OrderDetailsVO)order.getOrderDetail().get(0));
//            statsUtil.setState(OrderDetailStates.NEW);
//
//            statsUtil.insert();            
//        }
//        catch(Exception e)
//        {
//            e.printStackTrace();
//            this.fail();
//        }
//    }    

//    public void testStatsUtil()
//    {
//        try
//        {
//            OrderVO order = getOrder(0);
//            StatsUtil statsUtil = new StatsUtil();      
//
//            statsUtil.setOrder(order);
//            statsUtil.setItem((OrderDetailsVO)order.getOrderDetail().get(0));
//            statsUtil.setState(OrderDetailStates.NEW);
//
//            statsUtil.insert();            
//        }
//        catch(Exception e)
//        {
//            e.printStackTrace();
//            this.fail();
//        }
//    }

    public OrderVO getOrder(int i)
    {
        List items = new ArrayList();
        List payments = new ArrayList();
        List creditCards = new ArrayList();
        
        OrderVO order = new OrderVO();
        PaymentsVO payment = null;
        CreditCardsVO creditCard = null;
        OrderDetailsVO item = null;
        switch(i)
        {
            case 0:
                order.setStatus(Integer.toString(OrderStatus.RECEIVED));
                order.setMasterOrderNumber("JMP1234567890");
                order.setCsrId("TEST_CSRID");
                order.setOrderOrigin("TEST");
                item = new OrderDetailsVO();
                item.setStatus(Integer.toString(OrderStatus.RECEIVED_ITEM));
                item.setExternalOrderNumber("1234567890");
                items.add(item);
                order.setOrderDetail(items);
                break;
            case 1: // Should not be saved as stats
                order.setStatus(Integer.toString(OrderStatus.RECEIVED));
                order.setMasterOrderNumber("JMP1234567890");
                order.setCsrId("TEST_CSRID");
                order.setOrderOrigin("TEST");

                creditCard = new CreditCardsVO();
                creditCard.setCCNumber("4111111111111111");
                creditCards.add(creditCard);

                payment = new PaymentsVO();
                payment.setCreditCards(creditCards);
                payments.add(payment);

                order.setPayments(payments);
                
                item = new OrderDetailsVO();
                item.setStatus(Integer.toString(OrderStatus.RECEIVED_ITEM));
                item.setExternalOrderNumber("1234567890");
                items.add(item);
                order.setOrderDetail(items);
                break;                
            default:
                break;
        }
        
        return order;
    }
    
    public static void main(String[] args)
    {
        int iCount = args.length;
        if(iCount > 0)
        {
            StatsUtilTest statsUtilTest = null;

            for (int i = 0; i < iCount; i++) 
            {
                statsUtilTest = new StatsUtilTest(args[i]);
                statsUtilTest.run();
            }            
        }
        else
        {
            junit.textui.TestRunner.run(suite());
        }
    }   
}