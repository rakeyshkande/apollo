package com.ftd.osp.test.utilities.stats;

import junit.framework.*;
import com.ftd.osp.utilities.stats.*;
import com.ftd.osp.utilities.order.vo.*;
import java.util.*;

public class ValidationStatsUtilTest extends TestCase
{
    public ValidationStatsUtilTest(String name)
    {
        super(name);
    }

    public static Test suite()
    {
        TestSuite suite = new TestSuite("ValidationStatsUtilTest");
        suite.addTest(new ValidationStatsUtilTest("testValidationStatsUtil"));

        
        return suite;
    }

//    public void testValidationStatsUtil()
//    {
//        try
//        {
//            OrderVO order = getOrder(0);
//            ValidationStatsUtil validationStatsUtil = new ValidationStatsUtil();      
//
//            validationStatsUtil.insertReseaon(((OrderDetailsVO)order.getOrderDetail().get(0)).getExternalOrderNumber(), order.getGUID(), "Test reason", "Test value");            
//        }
//        catch(Exception e)
//        {
//            e.printStackTrace();
//            this.fail();
//        }
//    }

    public OrderVO getOrder(int i)
    {
        List items = new ArrayList();
        
        OrderVO order = new OrderVO();
        OrderDetailsVO item = null;
        switch(i)
        {
            case 0:
                order.setStatus(Integer.toString(OrderStatus.RECEIVED));
                order.setMasterOrderNumber("JMP1234567890");
                order.setCsrId("TEST_CSRID");
                order.setOrderOrigin("TEST");
                order.setGUID("FTD_GUID_1234567890");
                item = new OrderDetailsVO();
                item.setStatus(Integer.toString(OrderStatus.RECEIVED_ITEM));
                item.setExternalOrderNumber("1234567890");
                items.add(item);
                order.setOrderDetail(items);
                break;
            default:
                break;
        }
        
        return order;
    }
    
    public static void main(String[] args)
    {
        int iCount = args.length;
        if(iCount > 0)
        {
            ValidationStatsUtilTest validationStatsUtilTest = null;

            for (int i = 0; i < iCount; i++) 
            {
                validationStatsUtilTest = new ValidationStatsUtilTest(args[i]);
                validationStatsUtilTest.run();
            }            
        }
        else
        {
            junit.textui.TestRunner.run(suite());
        }
    }   
}