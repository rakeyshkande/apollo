package com.ftd.legacy.dao;

import java.sql.Connection;

/**
 * @author bsurimen
 *
 */


import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;

public class LegacySourceCodeDAO {

	private Connection connection;

	public LegacySourceCodeDAO(Connection conn) {
		this.connection = conn;
	}

	public String getSourceCode(String legacyID) {

		String sourceCode = null;
		try {

			DataRequest dataRequest = new DataRequest();
			dataRequest.setConnection(this.connection);
			dataRequest.setStatementID("SOURCE_CODE_FOR_LEGACY_ID");
			dataRequest.addInputParam("IN_LEGACY_ID", legacyID);

			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
			sourceCode = (String) dataAccessUtil.execute(dataRequest);
		} catch (Exception e) {
			new RuntimeException(e.getMessage(), e);
		}

		return sourceCode;

	}

	public String getLegacyID(String sourceCode) {
		String legacyID = null;
		try {

			DataRequest dataRequest = new DataRequest();
			dataRequest.setConnection(this.connection);
			dataRequest.setStatementID("LEGACY_ID_FOR_SOURCE_CODE");
			dataRequest.addInputParam("IN_SOURCE_CODE", sourceCode);

			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
			legacyID = (String) dataAccessUtil.execute(dataRequest);
		} catch (Exception e) {
			new RuntimeException(e.getMessage(), e);
		}

		return legacyID;
		
	}
	
	public String getOrderLegacyID(String ordeDetailId) {
		String legacyID = null;
		try {

			DataRequest dataRequest = new DataRequest();
			dataRequest.setConnection(this.connection);
			dataRequest.setStatementID("LEGACY_ID_FOR_ORDER");
			dataRequest.addInputParam("IN_ORDER_DETAIL_ID", ordeDetailId);
			
			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
			legacyID = (String) dataAccessUtil.execute(dataRequest);
		} catch (Exception e) {
			new RuntimeException(e.getMessage(), e);
		}

		return legacyID;
		
	}

}
