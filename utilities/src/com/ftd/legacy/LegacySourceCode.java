/**
 * 
 */
package com.ftd.legacy;

import java.sql.Connection;

import com.ftd.legacy.dao.LegacySourceCodeDAO;

/**
 * @author bsurimen
 *
 */
public class LegacySourceCode {

	/**
	 * 
	 */
	public LegacySourceCode(Connection conn) {
		this.legacySCDAO = new LegacySourceCodeDAO(conn);
	}

	private LegacySourceCodeDAO legacySCDAO;

	/**
	 * @param conn
	 * @param legacyID
	 * @return the Source code for given Legacy ID from Source Code Legacy ID
	 *         association
	 * @throws Exception
	 */
	public String getSourceCode(String legacyID) throws Exception {
		String sourceCode = null;
		if (legacyID != null && !"".equals(legacyID)) {
			sourceCode = legacySCDAO.getSourceCode(legacyID);
		}

		return sourceCode;
	}

	/**
	 * @param conn
	 * @param sourceCode
	 * @return the Source code for given Legacy ID from Source Code Legacy ID
	 *         association
	 * @throws Exception
	 */
	public String getLegacyID(String sourceCode) throws Exception {
		String legacyID = null;
		if (sourceCode != null && !"".equals(sourceCode)) {
			legacyID = legacySCDAO.getLegacyID(sourceCode);
		}

		return legacyID;
	}
	
	/**
	 * @param conn
	 * @param ordeDetailId
	 * @return the Legacy ID for given Order Detail ID
	 * @throws Exception
	 */
	public String getOrderLegacyID(String ordeDetailId) throws Exception {
		String legacyID = null;
		if (ordeDetailId != null && !"".equals(ordeDetailId)) {
			legacyID = legacySCDAO.getOrderLegacyID(ordeDetailId);
		}
		return legacyID;
	}
	

}
