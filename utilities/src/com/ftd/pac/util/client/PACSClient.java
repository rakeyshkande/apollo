/**
 * 
 */
package com.ftd.pac.util.client;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.List;

import javax.ws.rs.core.HttpHeaders;

import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pac.util.PACConstants;
import com.ftd.pac.util.PACUtil;
import com.ftd.pac.util.exception.PACException;
import com.ftd.pac.util.vo.PACRequest;
import com.ftd.pac.util.vo.PACResponse;

/**
 * @author smeka
 *
 */
public class PACSClient {
	private static final Logger logger = new Logger(PACSClient.class.getName());
	private static final String REQ_PARAM_SEPARATOR = "&";		
	private static final String CONTENT_TYPE = "application/json"; 
	
	private String uri;
	private String reqMethod; 	
	private int connectionTimeOut;
	private int socketTimeOut;
		
	/** Used for test
	 * @param uri
	 * @param reqMethod 
	 */
	public PACSClient(String uri, String reqMethod) {
		this.uri = uri;
		this.reqMethod = reqMethod; 
		this.connectionTimeOut = PACConstants.DEFAULT_CONNECTION_TIMEOUT;
		this.socketTimeOut = PACConstants.DEFAULT_SOCKET_TIMEOUT;
	}
	
	/**
	 * @param reqMethod
	 */
	public PACSClient(String reqMethod) { 
		this.uri = getPACURI();
		this.connectionTimeOut = getConnTimeout();
		this.socketTimeOut = getSockTimeout();

		this.reqMethod = reqMethod; 
	}

	/**
	 * @return
	 */
	private String getPACURI() {
		try {
			return ConfigurationUtil.getInstance().getFrpGlobalParm("SERVICE", "PAC_SVC_URL");
		} catch (Exception e) {
			logger.error("Error caught getting PAC URI from params: ", e);
			return PACConstants.DEFAULT_PAC_URL;
		}
	}

	/**
	 * @return
	 */
	public static int getConnTimeout() {
		try {
			return Integer.parseInt(ConfigurationUtil.getInstance().getFrpGlobalParm("SERVICE", "PAC_CONN_TIMEOUT"));
		} catch (Exception e) {
			logger.error("Error caught getting PAC Timeout from params: ", e);
			return PACConstants.DEFAULT_CONNECTION_TIMEOUT;
		}
	}
	
	/**
	 * @return
	 */
	public static int getSockTimeout() {
		try {
			return Integer.parseInt(ConfigurationUtil.getInstance().getFrpGlobalParm("SERVICE", "PAC_SOCKET_TIMEOUT"));
		} catch (Exception e) {
			logger.error("Error caught getting PAC SOCKET Timeout from params: ", e);
			return PACConstants.DEFAULT_SOCKET_TIMEOUT;
		}
	}
	
	/**
	 * @param pacRequest
	 * @return
	 * @throws Exception
	 */
	public PACResponse sendRequest(PACRequest pacRequest) throws Exception { 
		if(reqMethod.equalsIgnoreCase(PACConstants.GET_PROD_AVAIL_DATES_CHARGES) 
				|| reqMethod.equalsIgnoreCase(PACConstants.GET_AVAIL_PROD_BUNDLE)
				|| reqMethod.equalsIgnoreCase(PACConstants.GET_PC_PROD_AVAIL_DATE)) {
			return sendGetRequest(pacRequest);
		}
		throw new PACException("Unable to process PAC Request, Invalid method");
	}
	
	/**
	 * @param pacRequest
	 * @throws Exception
	 */ 
	private PACResponse sendGetRequest(PACRequest pacRequest) throws Exception {
	  logger.info("PAC request started at " + System.currentTimeMillis()); 
	  
	  String requestURL = null; 
	  HttpClient httpclient = null;
	  HttpGet httpGet = null;
	  HttpResponse response = null;
	  StringBuffer errorMessage = null;
	  try {
		  httpclient = new DefaultHttpClient();
		  requestURL = getThisRequestURL(pacRequest);
	      httpGet = new HttpGet(requestURL);		      
	      httpGet.addHeader(HttpHeaders.ACCEPT_ENCODING, CONTENT_TYPE);
	      
	      performTimeOut(httpclient);
	      
	      logger.info("PAC request for URL: " +  requestURL);
	      response = httpclient.execute(httpGet); 
	     
	      int result = response.getStatusLine().getStatusCode(); 	      
	      if (result != HttpStatus.SC_OK) {
	    	errorMessage = new StringBuffer("PAC HTTP GET request failed: " + response.getStatusLine());
	        throw new Exception("PAC HTTP GET request failed: " + response.getStatusLine());
	      }
	      
	      return processPACResponse(response); 
	  } catch (UnknownHostException e) {
			errorMessage = new StringBuffer("Exception caught calling PAC. Incorrect URI. ").append(e.getMessage()); 
			logger.error(errorMessage.toString(), e);
			throw new PACException("Exception caught calling PAC. Incorrect URI. ", e);
		} catch (SocketTimeoutException e) {
			errorMessage = new StringBuffer("Exception caught calling PAC. PAC is taking more time to respond. Closing the connection. ").append(e.getMessage()); 
			logger.error(errorMessage.toString(), e);
			throw new PACException("Exception caught calling PAC. PAC is taking more time to respond. Closing the connection. ", e);
		} catch (ConnectTimeoutException e) {
			errorMessage = new StringBuffer("Exception caught calling PAC. Unable to connect to PAC within the specified time.").append(e.getMessage()); 
			logger.error(errorMessage.toString(), e);
			throw new PACException("Exception caught calling PAC. Unable to connect to PAC within the specified time.", e);
		} catch (ConnectException e) {
			errorMessage = new StringBuffer("Exception caught calling PAC. Unable to connect to PAC within the specified time.").append(e.getMessage()); 
			logger.error(errorMessage.toString(), e);
			throw new PACException("Exception caught calling PAC. Unable to connect to PAC within the specified time.", e);
		} catch (Exception e) {
			if(errorMessage == null) {
				errorMessage = new StringBuffer();
			}
			errorMessage = errorMessage.append("Exception caught calling PAC. ").append("URL: ").append(requestURL); 
			logger.error(errorMessage.toString(), e);
			throw new PACException(errorMessage.toString() , e);
		} finally {			 
			if(httpclient != null) {
				httpclient.getConnectionManager().shutdown();
			}
			if(logger.isDebugEnabled()) {
				logger.debug("CLOSED the Http Connection and streams");
			}
			if(errorMessage != null && !StringUtils.isEmpty(errorMessage.toString())) {				
				new PACUtil().sendSystemMessage(errorMessage.toString());
			}
			logger.info("PAC request Ended at " + System.currentTimeMillis());
		} 	
	}

	/**
	 * @param httpResponse
	 * @throws PACException
	 */
	private PACResponse processPACResponse(HttpResponse httpResponse) throws PACException {
		InputStream inputStream = null;
		InputStream ios = null;
		BufferedReader br = null;
		PACResponse pacResponse = null;
		try {			
			try {
				ios = httpResponse.getEntity().getContent();
			} catch (IOException e) {
				logger.error("IOException caught calling PAC. ", e);
				throw new PACException("IOException caught calling PAC. ", e);
			}
			
			try {			
				if(ios != null) {	
					pacResponse = getRespObjectFromStream(ios);	
				}
			} catch (Exception e) {
				throw new PACException("IOException caught calling PAC. ", e);
			} 
		} finally {
			try {						
				if(inputStream != null) {							
					inputStream.close();
				}
				if(br != null) {														
					br.close();							
				}
				if(ios != null) {														
					ios.close();							
				}	
			} catch (Exception e) {
				logger.warn("Error caught closing http connections" + e);
			}
		}
		
		return pacResponse;
	}

	/**
	 * @param httpClient
	 */
	private void performTimeOut(HttpClient httpClient) {
		HttpParams httpParams = httpClient.getParams();
	    HttpConnectionParams.setConnectionTimeout(httpParams, this.connectionTimeOut);
	    HttpConnectionParams.setSoTimeout(httpParams, this.socketTimeOut);
	}
	 
	/**
	 * @param inputStream
	 * @return
	 * @throws IOException
	 * @throws Exception
	 */
	private PACResponse getRespObjectFromStream(InputStream inputStream) throws IOException, Exception {
		if(logger.isDebugEnabled()) {
			logger.debug(" Entering getJsonObjectFromJsonResponseStream() ");
		}
	
		PACResponse pacRespObj = new PACResponse();
		ObjectMapper mapper = new ObjectMapper();
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		InputStream originalStream = null;
		InputStream backupStream = null;
		byte[] buffer = new byte[1024];
		int len;
		try {
			while((len = inputStream.read(buffer)) != -1) {
				baos.write(buffer,0,len);
			}		
			originalStream = new ByteArrayInputStream(baos.toByteArray());		
			try {
				pacRespObj = mapper.readValue(originalStream, PACResponse.class);			
			}
			catch(JsonParseException e) {
				logger.error("Normal json class did not matched. Improper response, check with the service provider.");
				throw new PACException("Exception caught calling PAC. Improper JSON response from PAC Service", e);
			}
			catch(JsonMappingException e) {
				logger.error("Normal json class did not matched. Improper response, check with the service provider.");
				throw new PACException("Exception caught calling PAC. Improper JSON response from PAC Service", e);
			}
			catch(EOFException e) {
				logger.error("EOF exception occured due to: " + e.getMessage(), e);
				return null;
			} 
		} catch (Exception e) {
			throw new PACException("Exception caught calling PAC. ", e);
		} finally {
			try {
				if(baos != null) {					
					baos.close();
				}
				if(originalStream != null) {					
					originalStream.close();
				}
				if(backupStream != null) {					
					backupStream.close();
				}
			} catch (Exception e) {
				logger.warn("Error while closing the input streams" + e);
			}			
		}
		
		if(logger.isDebugEnabled()) {
			logger.debug("Exiting getRespObjectFromStream() ");
		}
		return pacRespObj;
	}

	/** Construct request XML per method type
	 * @param pacRequest
	 * @return
	 * @throws PACException 
	 */
	private String getThisRequestURL(PACRequest pacRequest) throws PACException {		
		StringBuilder requestURL = null;
		requestURL = new StringBuilder(this.uri);
		requestURL.append(this.reqMethod);
		requestURL.append("?").append("productId=" + pacRequest.getProductId());
		
		if(pacRequest.getAddonIds() != null && pacRequest.getAddonIds().size() > 0 ) {
			requestURL.append(REQ_PARAM_SEPARATOR).append("addonIds=" + getListAsString(pacRequest.getAddonIds(), ","));
		}
		
		if(!StringUtils.isEmpty(pacRequest.getSourceCode())) {
			requestURL.append(REQ_PARAM_SEPARATOR).append("sourceCode=" + pacRequest.getSourceCode());
		}
		
		if(pacRequest.getNumAvailDays() != null) {
			requestURL.append(REQ_PARAM_SEPARATOR).append("numAvailDays=" + pacRequest.getNumAvailDays());
		}
		
		if(!StringUtils.isEmpty(pacRequest.getChannel())) {
			requestURL.append(REQ_PARAM_SEPARATOR).append("channel=" + pacRequest.getChannel());
		}
		
		if(!StringUtils.isEmpty(pacRequest.getOrderDateTime())) {			
			requestURL.append(REQ_PARAM_SEPARATOR).append("orderDateTime=" + pacRequest.getOrderDateTime());
		}
		
		if(!StringUtils.isEmpty(pacRequest.getZipCode())) {
			requestURL.append(REQ_PARAM_SEPARATOR).append("zipCode=" + pacRequest.getZipCode());
		}
		
		if(!StringUtils.isEmpty(pacRequest.getDeliveryDate())) {
			requestURL.append(REQ_PARAM_SEPARATOR).append("deliveryDate=" + pacRequest.getDeliveryDate());
		}
		
		if(pacRequest.getIsFSMember() != null) {
			requestURL.append(REQ_PARAM_SEPARATOR).append("isFreeShippingMember=").append(pacRequest.getIsFSMember() ? "Y" : "N");
		}
		
		if(pacRequest.getIncludeCharge() != null) {
			requestURL.append(REQ_PARAM_SEPARATOR).append("includeCharge=").append(pacRequest.getIncludeCharge() ? "Y" : "N");
		} 
		
		if(pacRequest.getCheckMDAvailable() != null) {
			requestURL.append(REQ_PARAM_SEPARATOR).append("checkMorningDeliveryAvailable=").append(pacRequest.getCheckMDAvailable() ? "Y" : "N");
		}
		if(pacRequest.getAffiliateCode()!=null){
		    requestURL.append(REQ_PARAM_SEPARATOR).append("affiliateCode=").append(pacRequest.getAffiliateCode());
		}
		return requestURL.toString();		
	}

	private String getListAsString(List<String> list, String seperator) {
		StringBuffer string = new StringBuffer();
		if (list != null && list.size() > 0) {
			int count = 1;
			for (String element : list) { 			
				string.append(element); 

				if (count != list.size()) {
					string.append(seperator);
				}
				count++;
			}
		}
		return string.toString();
	}
	
	public static void main(String[] args) {
		PACRequest pacRequest = new PACRequest("6012", null, "70001", 10, new Date(), "60515", new Date(),
				true, true, true, false, null);
		try {
			new PACSClient("GET").sendGetRequest(pacRequest);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
