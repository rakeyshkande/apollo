/**
 * 
 */
package com.ftd.pac.util.vo;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pac.util.PACConstants;

/**
 * @author smeka
 *
 */
public class PACRequest {
	public String productId;
	public List<String> addonIds;
	public String sourceCode;
	public Integer numAvailDays;
	public String channel;
	public String orderDateTime;
	public String zipCode;	
	public String deliveryDate;	
	public Boolean includeCharge;	
	public Boolean checkMDAvailable;	
	public Boolean isFSMember;
	public String affiliateCode;
	
	private static final Logger logger = new Logger(PACRequest.class.getName());
	
		
	/**
	 * @param productId - Mandatory for all request
	 * @param addonIds - Optional
	 * @param sourceCode - Mandatory for all request
	 * @param numAvailDays - Optional
	 * @param orderDateTime - Optional - should be in format MMddyyyyhhmmss
	 * @param zipCode - Mandatory for all requests
	 * @param deliveryDate - Optional - should be in format MMddyyyy (Mandatory for bundled call) 
	 * @param includeCharge - Optional
	 * @param checkMDAvailable - Optional 
	 * @param isFSMember - Optional
	 */
	public PACRequest(String productId, List<String> addonIds,
			String sourceCode, Integer numAvailDays, Date orderDateTime, String zipCode, Date deliveryDate,
			Boolean includeCharge, Boolean checkMDAvailable, Boolean isFSMember, boolean isPCRequest,String affiliateCode) {
		super();
		
		DateFormat orderDateTimeFmt = new SimpleDateFormat("MMddyyyyHHmmss");
		orderDateTimeFmt.setTimeZone(TimeZone.getTimeZone("UTC"));
		
		this.productId = productId;
		this.addonIds = addonIds;
		this.sourceCode = sourceCode; 
		this.channel = getConfiguredChannel(); 
		this.zipCode = zipCode; 
		this.includeCharge = includeCharge;
		this.checkMDAvailable = checkMDAvailable; 
		this.isFSMember = isFSMember;	
		this.affiliateCode = affiliateCode;
		
		this.numAvailDays = numAvailDays;
		if(numAvailDays == null && !isPCRequest) {
			this.numAvailDays = getConfiguredDays();
		} 
		
		if (orderDateTime != null) {
			this.orderDateTime = orderDateTimeFmt.format(orderDateTime);
		} else {
			this.orderDateTime = orderDateTimeFmt.format(new Date());
		}
		
		if(deliveryDate != null && !isPCRequest) {
			this.deliveryDate = new SimpleDateFormat("MMddyyyy").format(deliveryDate);
		}

	}

	/**
	 * @return
	 */
	private String getConfiguredChannel() {
		try {
			return ConfigurationUtil.getInstance().getFrpGlobalParm("SERVICE", "PAC_CHANNEL_NAME");
		} catch (Exception e) {
			logger.error("Error caught getting PAC CHANNEL from params: "+ e);
			return PACConstants.DEFAULT_PAC_CHANNEL;
		}
	}

	/**
	 * @return
	 */
	private Integer getConfiguredDays() {	
		try {
			return Integer.parseInt(ConfigurationUtil.getInstance().getFrpGlobalParm("SERVICE", "PAC_DAYS_IN_RESP"));
		} catch (Exception e) {
			logger.error("Error caught getting PAC Days in response from params: "+ e);
			return PACConstants.DEFAULT_PAC_DAYS_IN_RESP;
		}
	}

	/**
	 * @return the productId
	 */
	public String getProductId() {
		return productId;
	}

	/**
	 * @return the addonIds
	 */
	public List<String> getAddonIds() {
		return addonIds;
	}

	/**
	 * @return the sourceCode
	 */
	public String getSourceCode() {
		return sourceCode;
	}

	/**
	 * @return the numAvailDays
	 */
	public Integer getNumAvailDays() {
		return numAvailDays;
	}

	/**
	 * @return the channel
	 */
	public String getChannel() {
		return channel;
	}

	/**
	 * @return the orderDateTime
	 */
	public String getOrderDateTime() {
		return orderDateTime;
	}

	/**
	 * @return the zipCode
	 */
	public String getZipCode() {
		return zipCode;
	}

	/**
	 * @return the deliveryDate
	 */
	public String getDeliveryDate() {
		return deliveryDate;
	}

	/**
	 * @return the includeCharge
	 */
	public Boolean getIncludeCharge() {
		return includeCharge;
	}

	/**
	 * @return the checkMDAvailable
	 */
	public Boolean getCheckMDAvailable() {
		return checkMDAvailable;
	}

	/**
	 * @return the isFSMember
	 */
	public Boolean getIsFSMember() {
		return isFSMember;
	}
	
	/**
	 * @return the affiliateCode
	 */
	public String getAffiliateCode() {
		return affiliateCode;
	}	
}
