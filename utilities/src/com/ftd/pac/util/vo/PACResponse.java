package com.ftd.pac.util.vo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;

@XmlRootElement(name = "PacResponse")
public class PACResponse {

    @XmlElement(name = "ProductId")
    protected String productId;
    
    @XmlElement(name = "AddonIds")
    protected String addonIds;
    
    @XmlElement(name = "StandardCharge")
    protected PACResponse.StandardCharge standardCharge;
    
    @XmlElement(name = "AvailableDates")
    protected List<PACResponse.AvailableDates> availableDates;
    
    @XmlElement(name = "Error")
    protected List<com.ftd.pac.util.vo.Error> error;

    /**
     * Gets the value of the productId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductId() {
        return productId;
    }

    /**
     * Sets the value of the productId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductId(String value) {
        this.productId = value;
    }

    /**
     * Gets the value of the addonIds property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddonIds() {
        return addonIds;
    }

    /**
     * Sets the value of the addonIds property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddonIds(String value) {
        this.addonIds = value;
    }

    /**
     * Gets the value of the standardCharge property.
     * 
     * @return
     *     possible object is
     *     {@link PACResponse.StandardCharge }
     *     
     */
    public PACResponse.StandardCharge getStandardCharge() {
        return standardCharge;
    }

    /**
     * Sets the value of the standardCharge property.
     * 
     * @param value
     *     allowed object is
     *     {@link PACResponse.StandardCharge }
     *     
     */
    public void setStandardCharge(PACResponse.StandardCharge value) {
        this.standardCharge = value;
    }

    /**
     * Gets the value of the availableDates property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the availableDates property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAvailableDates().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PACResponse.AvailableDates }
     * 
     * 
     */
    public List<PACResponse.AvailableDates> getAvailableDates() {
        if (availableDates == null) {
            availableDates = new ArrayList<PACResponse.AvailableDates>();
        }
        return this.availableDates;
    }

	/**
	 * @return the error
	 */
	public List<com.ftd.pac.util.vo.Error> getError() {
		return error;
	}

	/**
	 * @param error the error to set
	 */
	public void setError(List<com.ftd.pac.util.vo.Error> error) {
		this.error = error;
	}




	/**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="DelDate" type="{}Max8Num"/>
     *         &lt;element name="ShippingMethod" type="{}ShipMethodType"/>
     *         &lt;element name="Charges">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="Charge" maxOccurs="unbounded" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;simpleContent>
     *                         &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>double">
     *                           &lt;attribute name="Type" type="{}ChargesType" />
     *                         &lt;/extension>
     *                       &lt;/simpleContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "delDate",
        "shippingMethod",
        "charges"
    })
    public static class AvailableDates {

        @XmlElement(name = "DeliveryDate", required = true)
        protected String deliveryDate;
        
        @XmlElement(name = "ShippingMethod", required = true)
        protected ShipMethodType shippingMethod;
        
        @XmlElement(name = "isMorningDeliveryAllowed")
        protected String isMorningDeliveryAllowed;
        
        @XmlElement(name = "isOverrideCharge")
        protected String isOverrideCharge;
                
        @XmlElement(name = "Charges", required = true)
        protected PACResponse.AvailableDates.Charges charges;



        /**
		 * @return the deliveryDate
		 */
		public String getDeliveryDate() {
			return deliveryDate;
		}

		/**
		 * @param deliveryDate the deliveryDate to set
		 */
		public void setDeliveryDate(String deliveryDate) {
			this.deliveryDate = deliveryDate;
		}

		/**
         * Gets the value of the shippingMethod property.
         * 
         * @return
         *     possible object is
         *     {@link ShipMethodType }
         *     
         */
        public ShipMethodType getShippingMethod() {
            return shippingMethod;
        }

        /**
         * Sets the value of the shippingMethod property.
         * 
         * @param value
         *     allowed object is
         *     {@link ShipMethodType }
         *     
         */
        public void setShippingMethod(ShipMethodType value) {
            this.shippingMethod = value;
        }
        
        /**
		 * @return the isMorningDeliveryAllowed
		 */
		public String isMorningDeliveryAllowed() {
			return isMorningDeliveryAllowed;
		}

		/**
		 * @param isMorningDeliveryAllowed the isMorningDeliveryAllowed to set
		 */
		public void setIsMorningDeliveryAllowed(String isMorningDeliveryAllowed) {
			this.isMorningDeliveryAllowed = isMorningDeliveryAllowed;
		}
		
		/**
		 * @return the isOverrideCharge
		 */
		public String getIsOverrideCharge() {
			return isOverrideCharge;
		}

		/**
		 * @param isOverridenCharge the isOverrideCharge to set
		 */
		public void setIsOverrideCharge(String isOverrideCharge) {
			this.isOverrideCharge = isOverrideCharge;
		}

		/**
         * Gets the value of the charges property.
         * 
         * @return
         *     possible object is
         *     {@link PACResponse.AvailableDates.Charges }
         *     
         */
        public PACResponse.AvailableDates.Charges getCharges() {
            return charges;
        }

        /**
         * Sets the value of the charges property.
         * 
         * @param value
         *     allowed object is
         *     {@link PACResponse.AvailableDates.Charges }
         *     
         */
        public void setCharges(PACResponse.AvailableDates.Charges value) {
            this.charges = value;
        }


        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "charge"
        })
        public static class Charges {

            @XmlElement(name = "Charge")
            protected List<PACResponse.AvailableDates.Charges.Charge> charge;

            /**
             * Gets the value of the charge property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the charge property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getCharge().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link PACResponse.AvailableDates.Charges.Charge }
             * 
             * 
             */
            public List<PACResponse.AvailableDates.Charges.Charge> getCharge() {
                if (charge == null) {
                    charge = new ArrayList<PACResponse.AvailableDates.Charges.Charge>();
                }
                return this.charge;
            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;simpleContent>
             *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>double">
             *       &lt;attribute name="Type" type="{}ChargesType" />
             *     &lt;/extension>
             *   &lt;/simpleContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "value"
            })
            public static class Charge {

                @XmlValue
                protected BigDecimal value;
                @XmlAttribute(name = "Type")
                protected ChargesType type;

                /**
                 * Gets the value of the value property.
                 * 
                 */
                public BigDecimal getValue() {
                    return value;
                }

                /**
                 * Sets the value of the value property.
                 * 
                 */
                public void setValue(BigDecimal value) {
                    this.value = value;
                }

                /**
                 * Gets the value of the type property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link ChargesType }
                 *     
                 */
                public ChargesType getType() {
                    return type;
                }

                /**
                 * Sets the value of the type property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link ChargesType }
                 *     
                 */
                public void setType(ChargesType value) {
                    this.type = value;
                }

            }

        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;simpleContent>
     *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>double">
     *       &lt;attribute name="Type" type="{}ShipMethodType" />
     *     &lt;/extension>
     *   &lt;/simpleContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "value"
    })
    public static class StandardCharge {

        @XmlValue
        protected String value;
        
        @XmlAttribute(name = "Type")
        protected ShipMethodType type;

        /**
         * Gets the value of the value property.
         * 
         */
        public String getValue() {
            return value;
        }

        /**
         * Sets the value of the value property.
         * 
         */
        public void setValue(String value) {
            this.value = value;
        }

        /**
         * Gets the value of the type property.
         * 
         * @return
         *     possible object is
         *     {@link ShipMethodType }
         *     
         */
        public ShipMethodType getType() {
            return type;
        }

        /**
         * Sets the value of the type property.
         * 
         * @param value
         *     allowed object is
         *     {@link ShipMethodType }
         *     
         */
        public void setType(ShipMethodType value) {
            this.type = value;
        }

    }

}
