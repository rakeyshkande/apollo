package com.ftd.pac.util.vo;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Java class for ShipMethodType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ShipMethodType">
 *   &lt;restriction base="{}Max10AN">
 *     &lt;enumeration value="NextDay"/>
 *     &lt;enumeration value="TwoDay"/>
 *     &lt;enumeration value="Ground"/>
 *     &lt;enumeration value="Saturday"/>
 *     &lt;enumeration value="Sunday"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ShipMethodType")
@XmlEnum
public enum ShipMethodType {

    @XmlEnumValue("NextDay")
    NextDay("NextDay", "ND"),
    
    @XmlEnumValue("TwoDay")
    TwoDay("TwoDay", "2F"),
    
    @XmlEnumValue("Ground")
    Ground("Ground","GR"),
    
    @XmlEnumValue("Saturday")
    Saturday("Saturday", "SA"),
    
    @XmlEnumValue("Sunday")
    Sunday("Sunday", "SU");
    
    private final String key;
    private final String value;

    ShipMethodType(String key, String value) {
    	this.key = key;
        this.value = value;
    }
    
    public String key() {
        return key;
    }

    public String value() {
        return value;
    }

    public static String getValue(ShipMethodType shipMethodType) {
        for (ShipMethodType c: ShipMethodType.values()) {
            if (c.key.equals(shipMethodType.key)) {
                return shipMethodType.value;
            }
        }
        throw new IllegalArgumentException(shipMethodType.key);
    }

}
