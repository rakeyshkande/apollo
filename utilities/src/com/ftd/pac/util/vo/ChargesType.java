package com.ftd.pac.util.vo;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ChargesType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ChargesType">
 *   &lt;restriction base="{}Max50AN">
 *     &lt;enumeration value="NextDay"/>
 *     &lt;enumeration value="TwoDay"/>
 *     &lt;enumeration value="Ground"/>
 *     &lt;enumeration value="LateCutoff"/>
 *     &lt;enumeration value="SaturdayCost"/>
 *     &lt;enumeration value="SundayCost"/>
 *     &lt;enumeration value="SaturdayUpcharge"/>
 *     &lt;enumeration value="MondayUpcharge"/>
 *     &lt;enumeration value="SundayUpcharge"/>
 *     &lt;enumeration value="MorningDeliveryUpcharge"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ChargesType")
@XmlEnum
public enum ChargesType {

    @XmlEnumValue("NextDay")
    NextDay("NextDay"),
    @XmlEnumValue("TwoDay")
    TwoDay("TwoDay"),
    @XmlEnumValue("Ground")
    Ground("Ground"),
    @XmlEnumValue("LateCutoff")
    LateCutoff("LateCutoff"),
    @XmlEnumValue("SaturdayCost")
    SaturdayCost("SaturdayCost"),
    @XmlEnumValue("SundayCost")
    SundayCost("SundayCost"),
    @XmlEnumValue("SaturdayUpcharge")
    SaturdayUpcharge("SaturdayUpcharge"),
    @XmlEnumValue("MondayUpcharge")
    MondayUpcharge("MondayUpcharge"),
    @XmlEnumValue("SundayUpcharge")
    SundayUpcharge("SundayUpcharge"),
    @XmlEnumValue("MorningDeliveryUpcharge")
    MorningDeliveryUpcharge("MorningDeliveryUpcharge");
    private final String value;

    ChargesType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ChargesType fromValue(String v) {
        for (ChargesType c: ChargesType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
