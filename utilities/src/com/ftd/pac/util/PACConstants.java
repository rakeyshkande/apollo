/**
 * 
 */
package com.ftd.pac.util;



/**
 * @author smeka
 *
 */
public interface PACConstants {	
	public String GET_AVAIL_PROD_BUNDLE = "getAvailableProductBundle";
	public String GET_PROD_AVAIL_DATES_CHARGES = "getProductAvailableDatesAndCharges";
	public String GET_PC_PROD_AVAIL_DATE = "getPCProductAvailDate"; 	
	public String DEFAULT_DEV_PAC_URL = "http://metisdapp01v1.ftdi.com:38080/pacs/"; 
	public String DEFAULT_PAC_URL = "http://pacservice.ftdi.com/pacs/";  
	
	public int DEFAULT_CONNECTION_TIMEOUT = 10000;
	public int DEFAULT_SOCKET_TIMEOUT = 10000; 
	public int DEFAULT_PAC_DAYS_IN_RESP = 120;
	
	public String DEFAULT_PAC_CHANNEL = "APOLLO";	
	public String PACS_NOPAGE_SOURCE = "PACS_SERVICE_NO_PAGE";
}
