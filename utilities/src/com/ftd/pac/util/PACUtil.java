package com.ftd.pac.util;

import java.math.BigDecimal;
import java.sql.Connection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.axis.utils.StringUtils;

import com.ftd.ftdutilities.FTDCommonUtils;
import com.ftd.ftdutilities.GeneralConstants;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.order.RecalculateException;
import com.ftd.osp.utilities.order.RecalculateOrderBO;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.RecipientAddressesVO;
import com.ftd.osp.utilities.order.vo.RecipientsVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;
import com.ftd.pac.util.client.PACSClient;
import com.ftd.pac.util.exception.PACException;
import com.ftd.pac.util.vo.ChargesType;
import com.ftd.pac.util.vo.PACRequest;
import com.ftd.pac.util.vo.PACResponse;
import com.ftd.pac.util.vo.PACResponse.AvailableDates;
import com.ftd.pac.util.vo.PACResponse.AvailableDates.Charges;
import com.ftd.pac.util.vo.PACResponse.AvailableDates.Charges.Charge;

/**
 * @author smeka
 *
 */
public class PACUtil {
	
	private static Logger logger = new Logger(PACUtil.class.getName());
	
	/** If PAC returns a different ship method than of what is  on Item, set the ship method to null. 
	 * 		CSR should be allowed to change the ship method. 
	 *  no need to handle free shipping here - it is not done for FTDE - there is code in RecalculateOrderBO to handle FS/ and FS savings.
	 *  
	 *  This method returns the total fee as returned by PAC   
	 * 
	 * @param item
	 * @param recipAddress 
	 * @param itemHasFS
	 * @param specialCharge 
	 * @return
	 */
	public Map<String, Object> setItemFeeSplit(OrderDetailsVO item, RecipientAddressesVO recipAddress, BigDecimal specialCharge, PACResponse pacResponse, boolean isPCOrder) {	
		logger.info("Calculating fee split from PAC for order, " + item.getExternalOrderNumber());
		
		BigDecimal fee = new BigDecimal(0); 
		
		AvailableDates availableDate = pacResponse.getAvailableDates().get(0);  
		Charges charges = availableDate.getCharges();   
		boolean isFTDWBBNAvailable = false;
		
		String originalShipMethod = item.getShipMethod();
		String newShipMethod = null; 
		
		if (availableDate.getShippingMethod() != null) {
			newShipMethod = availableDate.getShippingMethod().value();
		}
		
		logger.info("Original Ship Method on order: " +  originalShipMethod + ", ship method sent by PAC: " + newShipMethod);
		
		// set the charges to default values
		defaultCharges(item);
		
		if(newShipMethod.equals(originalShipMethod)) {
			item.setShipMethod(newShipMethod);
		} else {
			item.setApplyOrigFee(true);
		}
			
		if (isPCOrder && isSaturdayDelivery(item.getDeliveryDate())) {
			item.setShipMethod(GeneralConstants.DELIVERY_SATURDAY_CODE);
			item.setFtdwAllowedShipping(GeneralConstants.DELIVERY_SATURDAY_CODE);
			item.setApplyOrigFee(false);
		}
		else {
			item.setFtdwAllowedShipping(newShipMethod);	
		}
		 
		for (Charge charge : charges.getCharge()) {
			logger.info("PAC returned Charge ---> " + charge.getType() + ":" + charge.getValue());	
			
			// up-charge(s)
			if(ChargesType.SaturdayUpcharge.equals(charge.getType()) || ChargesType.SaturdayCost.equals(charge.getType())) {
				item.setSaturdayUpcharge(String.valueOf(charge.getValue())); 
			}
			if(ChargesType.SundayUpcharge.equals(charge.getType()) || ChargesType.SundayCost.equals(charge.getType())) {
				item.setSundayUpcharge(String.valueOf(charge.getValue())); 
			}
			if(ChargesType.MondayUpcharge.equals(charge.getType())) {  
				item.setMondayUpcharge(String.valueOf(charge.getValue())); 
			}
			if(ChargesType.LateCutoff.equals(charge.getType())) {   
				item.setLatecutoffCharge(String.valueOf(charge.getValue())); 
			}	
			
			//Don't include MDF from PAC, we check various other conditions before applying MDF
			if(ChargesType.MorningDeliveryUpcharge.equals(charge.getType())) {
				continue;
			}
			
			fee = fee.add(charge.getValue()); 
	    }	
		
		//Morning delivery
		if(GeneralConstants.YES.equals(availableDate.isMorningDeliveryAllowed())) { 
			isFTDWBBNAvailable = true;
		}
		
		item.setMorningDeliveryAvailable(isFTDWBBNAvailable ? "Y" : null);		
		
		// IF Alaska or HAWAII apply SPL charges
		if (recipAddress.getStateProvince().equals(RecalculateOrderBO.HAWAII) || recipAddress.getStateProvince().equals(RecalculateOrderBO.ALASKA)) {
			fee = fee.add(specialCharge);
			item.setAlaskaHawaiiSurcharge(specialCharge.toString());
			logger.debug("AK/HI: " + item.getAlaskaHawaiiSurcharge());
		}
		
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("isFTDWBBNAvailable", isFTDWBBNAvailable);
		result.put("fee", fee); 
		
		return result;
	}
	
	/** Gets the PAC bundled availability response
	 * @param item
	 * @param orderedDate
	 * @param novatorId
	 * @param isPCOrder
	 * @return
	 * @throws RecalculateException
	 */
	public PACResponse getPACBundleResponse(OrderDetailsVO item, Date orderedDate, String novatorId, boolean isPCOrder) throws PACException {	
		logger.info("getPACBundleResponse() started at: " + System.currentTimeMillis()); 
		PACRequest pacRequest = null;
		Date deliveryDate = null;		
		RecipientAddressesVO recipAddress = null;
		PACSClient pacsClient = null;
		String affiliateCode = "";
		Connection connection = null;
		String sourceCode = item.getSourceCode();
		
		// Step1: Create PAC request		
		try {
			recipAddress = getRecipientAddress(item);
			try{
				connection =  DataSourceUtil.getInstance().getConnection("ORDER SCRUB");
				affiliateCode =  FTDCommonUtils.getAffiliateCode(connection, sourceCode);
			}catch(Exception exp){
				logger.error("Exception caught creating PAC request: "+ exp.getMessage()); 
			}
			logger.debug("isPCOrder: " + isPCOrder);
			logger.debug("affiliateCode"+affiliateCode);
			if(!isPCOrder) {				
				deliveryDate = new SimpleDateFormat("MM/dd/yyyy").parse(item.getDeliveryDate());		
				pacRequest = new PACRequest(novatorId, null, sourceCode, 0, orderedDate, recipAddress.getPostalCode(),
						deliveryDate, true, true, false, isPCOrder,affiliateCode);
				pacsClient = new PACSClient(PACConstants.GET_AVAIL_PROD_BUNDLE);
			} else {
				pacRequest = new PACRequest(novatorId, null, sourceCode, 0, orderedDate, recipAddress.getPostalCode(),
						null, true, null, false, isPCOrder,affiliateCode);
				pacsClient = new PACSClient(PACConstants.GET_PC_PROD_AVAIL_DATE);
			}			
			
		}  catch (ParseException e) {
			logger.error("Exception caught creating PAC request: ", e); 
			throw new PACException("Cannot recalculate Fees for FTDW orders, exception caught creating PAC request: " + e.getMessage() + ", setting original fee"); 
		}finally{
			try{
				if(!connection.isClosed()){
					connection.close();
				}
			}catch(Exception exp){
				logger.error("Unable to close the connection:"+exp.getMessage());
			}
		}
		
		//Step2: Call PAC for fees
		PACResponse pacResponse = null;
		if(pacRequest != null) {
			try {
				pacResponse = pacsClient.sendRequest(pacRequest);
			} catch (Exception e) {
				logger.error("Exception caught calling PAC: ", e); 
				throw new PACException("Cannot recalculate Fees for FTDW orders, exception caught calling PAC: " + e.getMessage() + ", setting original fee"); 
			}
		}
		
		// Step3: Validate the response
		if(pacResponse == null || pacResponse.getAvailableDates() == null || pacResponse.getAvailableDates().size() != 1) {
			logger.error("Invalid response from PAC. Product seems to be unavailable for the date: " + deliveryDate); 
			throw new PACException("Cannot recalculate Fees for FTDW orders, invalid PAC Response, setting original fee"); 
		}		
		logger.info("getPACBundleResponse() ended at: " + System.currentTimeMillis()); 
		return pacResponse;
	}
	
	/** Get the Ship method derived by PAC for FTDW orders  - this is called in edge cases when order is not recalculated.
	 * @param item
	 * @param orderedDate
	 * @param novatorId
	 * @param isPCOrder
	 * @return 
	 */
	public String getFTDWShipmethod(OrderDetailsVO item, Date orderedDate, String novatorId, boolean isPCOrder) { 
		try {
			PACResponse pacResponse = getPACBundleResponse(item, orderedDate, novatorId, isPCOrder);
			AvailableDates availableDate = pacResponse.getAvailableDates().get(0);		
			if(availableDate != null && availableDate.getShippingMethod() != null) {
				return availableDate.getShippingMethod().value();
			} 
		} catch(PACException e) {
    		logger.error("PAC thrown exception - Ship method cannot be populated.");
    	}
		return null;		
	}
	
	/** check if order is FTDW order
	 * @param shippingSystem
	 * @param productType
	 * @param shipMethod
	 * @return
	 */
	public static boolean isFTDWOrder(String shippingSystem, String productType, String shipMethod) { 
		if (GeneralConstants.FTDW_SHIPPING_KEY.equals(shippingSystem)) { 
			
			if(StringUtils.isEmpty(productType)) {
				return false;
			}
			
			if (GeneralConstants.OE_PRODUCT_TYPE_SDFC.equals(productType)) { 
				if (StringUtils.isEmpty(shipMethod) || GeneralConstants.DELIVERY_FLORIST_CODE.equals(shipMethod) 
						|| GeneralConstants.DELIVERY_SAME_DAY_CODE.equals(shipMethod)) {
					return false;
				}				
			}
			
			return true;
		}		 
		return false;    	
	}
	
	/** Get the recipient address
	 * @param detailVO
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private RecipientAddressesVO getRecipientAddress(OrderDetailsVO detailVO) {
		RecipientAddressesVO address = null;
		List<RecipientsVO> recipientList = detailVO.getRecipients();
		if (recipientList != null && recipientList.size() > 0) {
			RecipientsVO recipient = recipientList.get(0);
			if (recipient != null) {
				List<RecipientAddressesVO> addressList = recipient.getRecipientAddresses();
				if (addressList != null && addressList.size() > 0) {
					address = addressList.get(0);
				}
			}
		}
		return address;
	}
	
	/** Defaulting the charges before setting the updated values.
	 * @param item
	 */
	private void defaultCharges(OrderDetailsVO item) { 
		item.setLatecutoffCharge(null);
		item.setSundayUpcharge(null);
		item.setMondayUpcharge(null);
		item.setSaturdayUpcharge(null);		
		item.setShipMethod(null);
		item.setAlaskaHawaiiSurcharge(null);
		item.setMorningDeliveryFee(null);
		item.setFtdwAllowedShipping(null);
	}
	
	/**
	 * @param deliveryDate
	 * @return
	 */
	public static boolean isSaturdayDelivery(String deliveryDate) {
		boolean isSaturday=false;
		try{
			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
			sdf.setLenient(false);
			Calendar cal = Calendar.getInstance();
			cal.setTime(sdf.parse(deliveryDate));		
			isSaturday = Calendar.SATURDAY == cal.get(Calendar.DAY_OF_WEEK);
		}
		catch(ParseException e){
			logger.error("Unable to parse the delivery date, so can't determine as Saturday delivery !!");
		}
		return isSaturday;
	}
	
	/**
	 * @param message
	 * @return
	 * @throws Exception
	 */
	public String sendSystemMessage(String message) throws Exception {		
		String messageID = "";
		Connection connection = null;
		try {
			logger.error("Sending PAC Error System Message: " + message); 
			connection =  DataSourceUtil.getInstance().getConnection("ORDER SCRUB");
			SystemMessengerVO sysMessage = new SystemMessengerVO();
			sysMessage.setLevel(SystemMessengerVO.LEVEL_PRODUCTION); 
	        sysMessage.setSource(PACConstants.PACS_NOPAGE_SOURCE); 
			sysMessage.setType("ERROR");
			sysMessage.setMessage(message);
			sysMessage.setSubject("PAC SERVICE ERROR");

			SystemMessenger sysMessenger = SystemMessenger.getInstance(); 
			messageID = sysMessenger.send(sysMessage, connection, true); // Closes the connection

			if (messageID == null) {
				String msg = "Error occured while attempting to send out a system message. Msg not sent: " + message;
				logger.error(msg);
			}			
		} catch(Exception e) {
			logger.error("Error caught sending an alert, ", e);				
		} 
		return messageID;
	}
}
