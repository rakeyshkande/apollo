package com.ftd.pac.util.exception;

/**
 * @author smeka
 *
 */
public class PACException extends Exception { 

	/**
	 * Default
	 */
	private static final long serialVersionUID = 1L;

	public PACException() {	}

	public PACException(String message) {
		super("PAC Service Call failed, " + message);
	}

	public PACException(String message, Throwable cause) {
		super("PAC Service Call failed, " + message, cause);
	}
}