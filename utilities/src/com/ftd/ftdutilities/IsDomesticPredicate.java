package com.ftd.ftdutilities;

import org.apache.commons.collections.Predicate;

/**
 * IsDomesticPredicate determines if a country is domestic (US, CA, VI, PR).
 *
 * @author Mark Moon, Software Architects Inc.
 * @version $Revision: 1.2 $
 */
public class IsDomesticPredicate
        implements Predicate {

    // singleton instance
    public static Predicate INSTANCE = new IsDomesticPredicate();

    /**
     * Private constructor enforces singleton pattern.
     */
    private IsDomesticPredicate() {
        super();
    }

    /**
     * Returns a Predicate instance.
     * @return a IsDomesticPredicate instance
     */
    public static Predicate getInstance() {
        return INSTANCE;
    }

    /**
     * Determines if a country is domestic (US, CA, VI, PR).
     * @param input The country to determine whether or not it is domestic
     * @return true if the input is a domestic country, otherwise false.
     * @throws IllegalArgumentException if the input is not of type java.lang.String
     *         or if the String is null or whitespace only.
     */
    public boolean evaluate(Object input) {

        // type check input
        if ( !(input instanceof String) ) {
            throw new IllegalArgumentException("Excepting type of java.lang.String");
        }

        String country = (String)input;

        // country cannot be null or whitespace only
        if ( country != null && country.trim().length() > 0 ) {
          return ( "US".equals(country) || "CA".equals(country) || 
                   "VI".equals(country) || "PR".equals(country) );
        }
        
        // country was null or whitespace only, throw IllegalArgumentException
        else {
          throw new IllegalArgumentException("Input was null or whitespace only.");
        }
    }
}