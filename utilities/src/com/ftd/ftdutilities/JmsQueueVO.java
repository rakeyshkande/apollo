package com.ftd.ftdutilities;


public class JmsQueueVO {
	
	private String queueName;
	private String correlationId;
	private String payload;
	private long delaySeconds;
	
	public String getQueueName() {
		return queueName;
	}
	public void setQueueName(String queueName) {
		this.queueName = queueName;
	}
	public String getCorrelationId() {
		return correlationId;
	}
	public void setCorrelationId(String correlationId) {
		this.correlationId = correlationId;
	}
	public String getPayload() {
		return payload;
	}
	public void setPayload(String payload) {
		this.payload = payload;
	}
	public long getDelaySeconds() {
		return delaySeconds;
	}
	public void setDelaySeconds(long delaySeconds) {
		this.delaySeconds = delaySeconds;
	}
}
