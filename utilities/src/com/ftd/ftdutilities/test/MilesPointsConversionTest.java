package com.ftd.ftdutilities.test;

import com.ftd.ftdutilities.FTDCommonUtils;
import junit.framework.*;
import org.apache.commons.lang.StringUtils;

/*
 * Class to test Miles/Points conversion algorithm.
 * 
 * As a minimum to run locally:
 *   % cd FTD/utilities
 *   % build
 *   % cd FTD/lib
 *   % java -cp 'ftdutilities.jar;utilities.jar;junit-3.7.jar;commons-lang-2.2.jar;log4j-1.2.5.jar' com.ftd.ftdutilities.test.MilesPointsConversionTest
*/
public class MilesPointsConversionTest extends TestCase
{
  public MilesPointsConversionTest(String name)
  {
    super(name);
  }

  public static void main(String[] args)
  {
    junit.textui.TestRunner.run(suite());
  }   

  public static Test suite()
  {
    TestSuite suite= new TestSuite("MilesPointsConversionTest");
    suite.addTest(new MilesPointsConversionTest("testDollarConversions"));
    return suite;
  }

  private void convertIt(String dollars, String redemptionRate, String scaleOperator, 
                           String roundingScale, String roundingMode, int milesExpected) throws Exception {
      String scaleStr = " invalid scale operator ";
      String dollarStr = StringUtils.leftPad(dollars, 5);
      int mp;
      if ("/".equals(scaleOperator)) {
          scaleStr = " dollars/mile ";
      } else if ("*".equals(scaleOperator)) {
          scaleStr = " miles/dollar ";
      }
      mp = FTDCommonUtils.convertDollarsToMilesPoints(dollars, redemptionRate, scaleOperator, roundingScale, roundingMode);
      System.out.println("$" + dollarStr + " --> " + mp + " miles (@" + redemptionRate + scaleStr +
                        "rounded " + roundingMode + " to nearest " + roundingScale + " miles)");
      assertEquals(milesExpected, mp);
  }
  
  public void testDollarConversions()
  {
    try
    {
      System.out.println("");
      convertIt("40.01", "0.10", "/", "100", "UP",      500);
      convertIt("40.01", "0.10", "/", "100", "DOWN",    400);
      convertIt("5.00",  "0.10", "/", "100", "HALF_UP", 100);
      convertIt("4.94",  "0.10", "/", "100", "HALF_UP",   0);
      convertIt("4.95",  "0.10", "/", "100", "HALF_UP", 100);
      convertIt("4.94",  "0.10", "/", "100", "UP",      100);
      convertIt("4.95",  "0.10", "/", "100", "DOWN",      0);

      System.out.println("");
      convertIt("4.99",  "250", "*", "500", "HALF_UP", 1000);
      convertIt("5.00",  "250", "*", "500", "HALF_UP", 1500);
      convertIt("4.00",  "250", "*", "500", "UP",      1000);
      convertIt("4.01",  "250", "*", "500", "UP",      1500);
      convertIt("5.99",  "250", "*", "500", "DOWN",    1000);
      convertIt("6.00",  "250", "*", "500", "DOWN",    1500);

      System.out.println("");
      convertIt("-4.99",  "250", "*", "500", "HALF_UP", -1000);
      convertIt("-5.00",  "250", "*", "500", "HALF_UP", -1500);
      convertIt("-4.00",  "250", "*", "500", "UP",      -1000);
      convertIt("-4.01",  "250", "*", "500", "UP",      -1500);
      convertIt("-5.99",  "250", "*", "500", "DOWN",    -1000);
      convertIt("-6.00",  "250", "*", "500", "DOWN",    -1500);

      System.out.println("");
      convertIt("53.6",  "0.006", "/", "100", "HALF_UP", 8900);
    }
    catch(Throwable t)
    {
      t.printStackTrace();
      System.out.println("Conversion was different than expected.  Test Failed!!!");
    }
    
  }
  
}
