package com.ftd.ftdutilities.test;

import com.ftd.ftdutilities.DeliveryDateUTIL;
import com.ftd.ftdutilities.OEDeliveryDateParm;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;

import java.sql.Connection;
import java.sql.DriverManager;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.StringTokenizer;
import junit.framework.*;

public class DeliveryDateUtilTest extends TestCase
{
  public DeliveryDateUtilTest(String name)
  {
    super(name);
  }

  public static void main(String[] args)
  {
    junit.textui.TestRunner.run(suite());
  }   

  public static Test suite()
  {
    TestSuite suite= new TestSuite("DeliveryDateUtilTest");
    
    // The getShipDate test is not run.  There were too many complications 
    // automating how ship dates are calculated to come up with
    // a reliable expected result.  The getShipDate still exists if
    // anyone would like to figure out a way to easily automate the test.
    //suite.addTest(new DeliveryDateUtilTest("getShipDate"));
    
    // Changes to the code centered around isPastVendorCutoff.  getShipDate calls 
    // isPastVendorCutoff
    suite.addTest(new DeliveryDateUtilTest("isPastVendorCutoff"));
    return suite;
  }
    
  
  public void isPastVendorCutoff()
  {
    Connection conn = null;
    boolean pastCutoffExpected = false;
    boolean pastCutoff = false;
    String product = null;
    ResultSet rs = null;
    String latestCutoffVendor = null;
    String latestCutoff = null;
    int cutoffDelay = 0;
    StringTokenizer latestCutoffToken = null;
    GregorianCalendar currentTime = null;
    GregorianCalendar latestcutoffVendor = null;
    GregorianCalendar latestcutoff = null;
    
    try
    {
      conn = getConnection();
      Statement stmt = conn.createStatement();
      DeliveryDateUTIL ddu = new DeliveryDateUTIL();
      ArrayList products = new ArrayList();
      products.add("F056");
      products.add("F556");
      products.add("N029");

      for(Iterator it = products.iterator(); it.hasNext();)
      {
        product = (String)it.next();
        rs = stmt.executeQuery("select value from FRP.GLOBAL_PARMS where CONTEXT='SHIPPING_PARMS' AND NAME = 'LASTEST_CUTOFF_VENDOR'");
        rs.next();
        latestCutoffVendor = rs.getString(1);
  
        rs = stmt.executeQuery("select MAX(cutoff_time) from ftd_apps.vendor_master where vendor_id in (select vendor_id from ftd_apps.vendor_product where product_subcode_id = '" + product + "')");
        rs.next();
        latestCutoff = rs.getString(1);
        rs.close();
  
        rs = stmt.executeQuery("select value from FRP.GLOBAL_PARMS where CONTEXT='SHIPPING_PARMS' AND NAME = 'BACKEND_CUTOFF_DELAY'");
        rs.next();
        cutoffDelay = Integer.parseInt(rs.getString(1));
        rs.close();
  
        latestCutoffToken = new StringTokenizer(latestCutoff, ":");
        
        currentTime = new GregorianCalendar();
  
        latestcutoffVendor = new GregorianCalendar(currentTime.YEAR, 
          currentTime.MONTH, 
          currentTime.DATE, 
          Integer.parseInt(latestCutoffVendor.substring(0,2)), 
          Integer.parseInt(latestCutoffVendor.substring(2)));
  
        latestcutoff = new GregorianCalendar(currentTime.YEAR, 
          currentTime.MONTH, 
          currentTime.DATE, 
          Integer.parseInt((String)latestCutoffToken.nextToken()), 
          Integer.parseInt((String)latestCutoffToken.nextToken()));
  
        if(currentTime.getTime().getTime() > latestcutoffVendor.getTime().getTime())
        {
          pastCutoffExpected = true;
        }
        else if(currentTime.getTime().getTime() > (latestcutoff.getTime().getTime() + cutoffDelay * 60000))
        {
          pastCutoffExpected = true;                
        }
             
        //if past cutoff add 1 to the currentDay
        HashMap vendorCutoffInfo = (HashMap) ddu.retrieveVendorCutoffInfo(conn, product); 
        String sPastVendorCutoff = (String)vendorCutoffInfo.get("past_cutoff");
        pastCutoff = new Boolean(sPastVendorCutoff).booleanValue();

        System.out.println("product " + product);
        System.out.println("pastCutoffExpected " + pastCutoffExpected);
        System.out.println("pastCutoff " + pastCutoff);
        
        assertEquals(pastCutoffExpected, pastCutoff);
        
        System.out.println("Test successful for product " + product);
      }
    }
    catch(Throwable t)
    {
      t.printStackTrace();
      System.out.println("Cutoff time expected was different than actual cutoff.  Test Failed!!!  Please investigate for product " + product);
    }
    finally
    {
      try
      {
        if(conn != null)
          conn.close();
      }
      catch(Throwable t){}
    }
    
  }

  /*
   * The getShipDate test is not run.  There were too many complications 
   * automating how ship dates are calculated to come up with
   * a reliable expected result.  The getShipDate still exists if
   * anyone would like to figure out a way to easily automate the test.
  public void getShipDate() 
  {
    Connection conn = null;
    try 
    {
      conn = getConnection();

      String expectedShipDate = null;
      String shipDate = null;
      OEDeliveryDateParm oeParm = null;
      StringTokenizer st = null;
      ArrayList tests = new ArrayList();
      DeliveryDateUTIL ddu = new DeliveryDateUTIL();
      //Format is product type, ship method, delivery date, recipient state,
      // recipient country, product id, connection, expected result
      tests.add("FRECUT,ND,08/01/2006,IL,US,F056,07/31/2006");
      tests.add("FRECUT,SA,07/29/2006,IL,US,F056,07/29/2006");

      for(Iterator it = tests.iterator(); it.hasNext();)
      {
        st = new StringTokenizer((String)it.next(), ",");
        
        oeParm = new OEDeliveryDateParm();
        oeParm.setProductType(st.nextToken());
        
        shipDate = ddu.getShipDate(oeParm, st.nextToken(), st.nextToken(), st.nextToken(), st.nextToken(), st.nextToken(), conn);
        expectedShipDate = (String)st.nextToken();

        System.out.println("expectedShipDate:  " + expectedShipDate);
        System.out.println("shipdate:  " + shipDate);

        assertEquals(shipDate, expectedShipDate);
      }
    }
    catch (Throwable t) 
    {
      t.printStackTrace();
    }
    finally
    {
      try
      {
        if(conn != null)
          conn.close();
      }
      catch(Throwable t){}
    }
  }
   */

  private Connection getConnection() throws Exception {
    String driver = "oracle.jdbc.driver.OracleDriver";
    String database = "jdbc:oracle:thin:@adonis.ftdi.com:1522:DEV5";
    String user = "mkruger";
    String password = "mkruger";
  
    Class.forName(driver);
    Connection conn = DriverManager.getConnection(database, user, password);  
  
    return conn;
  }
  
}
