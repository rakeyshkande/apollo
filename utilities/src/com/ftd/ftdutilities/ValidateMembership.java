package com.ftd.ftdutilities;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.util.HashMap;
import java.util.regex.Pattern;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

/**
  * Membership validation class
  * @author Tom Jaskula
  */

public class ValidateMembership
{
    private static final String CHECK_AAA_MEMBERSHIP = "CHECK_AAA_MEMBERSHIP";
    private static final String GET_PARTNER = "GET_PARTNER";
	private static Logger logger = new Logger("com.ftd.ftdutilities.ValidateMembership");
	public static final int TEN_DIGITS_NUM=10;
	public static final int ELEVEN_DIGITS_NUM=11;
	//Added for getting global parameters values
	public static final String AUTH_CONFIG = "AUTH_CONFIG";
	private static final String AUTH_PROVIDER = "cc.auth.provider";
	private static final String BAMS_CC_LIST = "bams.cc.list";
	private static final String BAMS_AUTH_PROVIDER="BAMS";
    public ValidateMembership()
    {
    }

    /**
     * Create a wrapper method that decides which method to invoke based on partner name.
     * This centralizes the check digit routine logic in one place.
     * This method is called by order validator, MO, and JOE.
     * 03/08/2011. Defect 7804.
     */
     public boolean validateMembershipById(String membershipNumber, String programName, String partnerName, Connection conn) throws Exception {
         if(membershipNumber == null) {
            membershipNumber = "";      
         }
         if(partnerName == null) {
            partnerName = "";
         }
         // First check the partners table in the database
         boolean validMembershipId = true;
         // check membership number size if no routine exists.
         validMembershipId = checkPartners(programName, membershipNumber, conn);

         if ( partnerName.equalsIgnoreCase("AMERICAN AUTO ASSOC.") )
         {
             validMembershipId = validateAAAMembershipId(membershipNumber, conn);
         }
         else if ( partnerName.equalsIgnoreCase("AIR CANADA") )
         {
             validMembershipId = new ValidateMembership().checkAirCanada(membershipNumber);
         }
         else if ( partnerName.equalsIgnoreCase("ALASKA AIRLINES") )
         {
             validMembershipId = new ValidateMembership().checkAlaskaAir(membershipNumber);
         }
         else if ( partnerName.equalsIgnoreCase("AMERICAN AIRLINES") )
         {
             validMembershipId = new ValidateMembership().checkAmericanAirlinesAdvantage(membershipNumber);
         }
         else if ( partnerName.equalsIgnoreCase("AMERICA WEST") )
         {
             validMembershipId = new ValidateMembership().checkAmericaWest(membershipNumber);
         }
         else if ( partnerName.equalsIgnoreCase("ATA") )
         {
             validMembershipId = new ValidateMembership().checkATA(membershipNumber);
         }
         else if ( partnerName.equalsIgnoreCase("BAYMONT HOTELS") )
         {
             validMembershipId = new ValidateMembership().checkBaymont(membershipNumber);
         }
         else if ( partnerName.equalsIgnoreCase("BEST WESTERN") )
         {
             validMembershipId = new ValidateMembership().checkBestWestern(membershipNumber);
         }
         else if ( partnerName.equalsIgnoreCase("DELTA AIRLINES") )
         {
             validMembershipId = new ValidateMembership().checkDelta(membershipNumber);
         }
         else if ( partnerName.equalsIgnoreCase("GOLD POINTS") )
         {
             validMembershipId = new ValidateMembership().checkGoldPoints(membershipNumber);
         }
         else if ( partnerName.equalsIgnoreCase("HAWAIIAN AIRLINES") )
         {
             validMembershipId = new ValidateMembership().checkHawaiian(membershipNumber);
         }
         else if (partnerName.equalsIgnoreCase("JETBLUE") )
         {
             validMembershipId = new ValidateMembership().checkJetBlue(membershipNumber);
         }
         else if ( partnerName.equalsIgnoreCase("KOREAN AIR") )
         {
             /* Per requirements, we will be accepting any id.  Method created
              * for possible future changes.
              */
             validMembershipId = true; //new ValidateMembership().checkKoreanAir(membershipNumber);
         }
         else if (partnerName.equalsIgnoreCase("MIDWEST AIRLINES") ) 
         {
             validMembershipId = new ValidateMembership().checkMidwestExpress(membershipNumber);
         }
         else if ( partnerName.equalsIgnoreCase("NORTHWEST AIRLINES") )
         {
             validMembershipId = new ValidateMembership().checkNorthwest(membershipNumber);
         }
         else if ( partnerName.equalsIgnoreCase("SPINT") )
         {
             //SPRINT currently does not exist as a partner. Retaining code from legacy.
             validMembershipId = new ValidateMembership().checkSprint(membershipNumber);
         }
         else if ( partnerName.equalsIgnoreCase("SOUTHWEST AIRLINES") )
         {
             //SOUTHWEST AIRLINES currently does not exist as a partner. Retaining code from legacy.
             validMembershipId = new ValidateMembership().checkSouthwest(membershipNumber);
         }
         else if ( partnerName.equalsIgnoreCase("UNITED AIRLINES") || partnerName.equalsIgnoreCase("CONTINENTAL AIRLINES"))
         {
             validMembershipId = new ValidateMembership().checkUnited(membershipNumber);
         }
         else if ( partnerName.equalsIgnoreCase("US AIRWAYS") )
         {
             validMembershipId = new ValidateMembership().checkUsAir(membershipNumber);
         }
         else if ( partnerName.equalsIgnoreCase("VIRGIN AIRWAYS") )
         {
             validMembershipId = new ValidateMembership().checkVirginAir(membershipNumber);
         }     
         else if ( partnerName.equalsIgnoreCase("VIRGIN AMERICA") )
         {
             validMembershipId = new ValidateMembership().checkVirginAmerica(membershipNumber);
         }      
         
         return validMembershipId;
     }
     
     /**
     * Performs digit size checking against FTD_APPS.PROGRAM_REWARD.PARTICIPANT_ID_LENGTH_CHECCK.
     * @param origPartnerId
     * @param membershipId
     * @param con
     * @return
     * @throws Exception
     */
     
     public static boolean checkPartners(String origPartnerId, String membershipId, Connection con) throws Exception
     {
        boolean ret = false;
        BigDecimal length = null;
        membershipId = membershipId.trim();
        origPartnerId = origPartnerId.trim();

        DataRequest dataRequest = new DataRequest();

        dataRequest.setConnection(con);
        dataRequest.setStatementID(GET_PARTNER);
        dataRequest.addInputParam("PARTNER_ID", origPartnerId);
        DataAccessUtil dau = DataAccessUtil.getInstance();
        
        CachedResultSet rs = (CachedResultSet)dau.execute(dataRequest);
        rs.reset();
        while(rs.next())
        {
            length = (BigDecimal)rs.getObject(4);
            if(length == null) length = new BigDecimal("0");
        }
        
        if(length != null && (length.compareTo(BigDecimal.valueOf(0)) == 0 || membershipId.length() == ((BigDecimal)length).intValue()))
        {
            ret = true;
        }

        return ret;
     }     

    private static boolean validateAAAMembershipId(String membershipId, Connection con) throws Exception
    {
        // Validate the id is the valid length
        membershipId = membershipId.trim();
        if (membershipId.length() != 6)
        {
            return false;
        }

        // Validate the id is numeric
        try
        {
            Integer.parseInt(membershipId);
        }
        catch(NumberFormatException e)
        {
            return false;
        }

        DataRequest dataRequest = new DataRequest();

        dataRequest.setConnection(con);
        dataRequest.setStatementID(CHECK_AAA_MEMBERSHIP);
        dataRequest.addInputParam("MEMBER_ID", membershipId);
        DataAccessUtil dau = DataAccessUtil.getInstance();
        CachedResultSet rs = (CachedResultSet)dau.execute(dataRequest);
        rs.reset();
        Object memberId = null;
        String memberName = null;

        while(rs.next())
        {
            memberId = rs.getObject(1);
            memberName = (String)rs.getObject(2);
        }
        
        if(memberId != null)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
/**
* Converts Strings to longs
*
*/
    public long StringToLong(String inString)
    {
        try{
           return (long)Long.parseLong(inString);
        }catch(NumberFormatException e){
           return -1;
        }
     }

/**
* Converts Strings to ints
*
*/
    public int StringToInt(String inString)
    {
        try{
           return (int)Double.parseDouble(inString);
        }catch(NumberFormatException e){
           return -1;
        }
     }

/**
* Converts Strings to doubles
*
*/
     public double StringToDouble(String inString)
    {
        try{
           return (double)Double.parseDouble(inString);
        }catch(NumberFormatException e){
           return -1;
        }
     }

/**
* Credit Card validation
*
*/
  public boolean checkCreditCard(String ccType, String id)
    {
    int idBegin = 0;
    int ccLength = 0;
    int first4 = 0;
    int first5 = 0;
    int first6 = 0;
    int digit = 0;
    int counter = 0;
    int total = 0;
    int second = 0;
    int multTotal = 0;
    int first = 0;
    int checkDigit = 0;
    int checkDigitLength = 0;

    String lastDigit = "";
    String digitStr = "";
    int first7 =0;
  
    
    //Remove the spaces
    id = id.trim();
    ccType = ccType.trim();

    //Validate the id is numeric
    try{
        Double.parseDouble(id);
    } catch (NumberFormatException e){
        return false;
    }

    //Credit card type is required
    if (ccType.length() == 0)
        return false;

    //Carte Blanche has the same check digit routine as Diners
    if (ccType.equals("CB"))
        ccType = "DC";

    idBegin = StringToInt(id.substring(0,2));
    //Determine the credit card type for gift certificates
    if (ccType.equals("GC"))
    {

        if ((idBegin == 30) | (idBegin == 36) | (idBegin == 38))
            ccType = "DC";

        if ((idBegin == 34) | (idBegin == 37))
            ccType = "AX";

        if ((idBegin >=40) & (idBegin <=49))
            ccType = "VI";

        if ((idBegin >=50) & (idBegin <=59))
            ccType = "MC";

        if ((idBegin >=20) & (idBegin <=29))
           ccType = "MC";

        if ((idBegin >=60) & (idBegin <=69))
            ccType = "DI";
    }

    //Set the length variable based on the credit card type
    if (ccType.equals("VI"))
    {
        if(id.length() == 16)
            ccLength = 16;
        if(id.length() == 13)
            ccLength = 13;
    }

    else if((ccType.equals("MC")) | (ccType.equals("DI")))
        ccLength = 16;
    else if(ccType.equals("AX"))
        ccLength = 15;
    else if ((ccType.equals("DC")) | (ccType.equals("CB")))
        ccLength = 14;
    else if (ccType.equals("JP"))
        ccLength = 11;
    else if(ccType.equals("MS"))  // AAFES
    {
        ccLength = 16;
    }
    else
        ccLength = 16;

    //Compare the variable to the length of the id
    if (ccLength != id.length())
        return false;

    //Get the first 5 digits of the id
    first4 = StringToInt(id.substring(0,4));
    first5 = StringToInt(id.substring(0,5));
    first6 = StringToInt(id.substring(0,6));
    first7 = StringToInt(id.substring(0,7));
    boolean authProviderBAMS=false;
    //Getting BAMS global parameters.
    if(ccType.equals("DI") || ccType.equals("DC")){
    	authProviderBAMS = isAuthProviderBAMS(ccType);
    }
    
    //VISA
    if (ccType.equals("VI"))
        {
        if ((first5 < 40000) | (first6 > 499999))
            return false;
        }
    //MASTER CARD
    else if (ccType.equals("MC"))
        {
        if (((first5 < 50000) || (first5 > 59999)) && ((first5 < 20000) || (first5 > 29999)))
            return false;
        }
    //DISCOVER CARD
    else if (ccType.equals("DI") )
        {
 	    //If authProvider is BAMS and BAMS accepting Discover card.
    	if(authProviderBAMS){ 
    		if(!isValidDiscover(id,idBegin,first7))
    			return false;
    	}else{ //If authProvider is not BAMS or BAMS not accepting Discover card then by default CCAS is authProvider.
    		if ((first5 < 60000) | (first5 > 69999))
    			return false;
    	}

        }
    //AMERICAN EXPRESS
    else if (ccType.equals("AX"))
        {
        if ((first5 < 34000) | (first5 > 37999))
            return false;
        else if ((first5 > 34999) & (first5 < 37000))
            return false;
        }
    //DINERS CLUB
    else if (ccType.equals("DC"))
    {
    	//Diners Card Validations as in the jccas.
    	if(authProviderBAMS){
    		if(!isValidDinersCard(id,idBegin,first7))
    			return false;
    	}else{
    		if ((first5 < 30000) | (first5 > 38999))
    			return false;
    		if ((first5 > 30999) & (first5 < 36000))
    			return false;
    		if ((first5 > 36999) & (first5 < 38000))
    			return false;
    	}
    }
    // AAFES - Military Star
        else if(ccType.equals("MS"))
        {
            if(first4 != 6019)
            {
                return false;
            }
        }
    //ANY OTHER TYPE
    else if (! ccType.equals("JP") )
        return false;

    if(! ccType.equals("MS"))
    {
        // For JCPenney card, run check digit on only first 10 digits
        if ( ccType.equals("JP") )
        {
            checkDigitLength = 10;
        }
        // All others run check digit against the full string
        else
        {
            checkDigitLength = id.length();
        }

        //Valiate the number
            for(int i = checkDigitLength-1; i >= 1; i--)
            {
                digit = Character.getNumericValue(((id.substring(i-1, i))).charAt(0));
                counter++;

                if ((counter % 2) != 0)
                {
                    digit *= 2;

                    //Add into the total
                    if (digit > 9)
                    {
                         //get the second number of the digit variable and hold it in an int
                        digitStr = (digit + "");
                        second = Character.getNumericValue(digitStr.charAt(1));
                        total += second;
                    }
                }

                digitStr = (digit + "");
                first = Character.getNumericValue(digitStr.charAt(0));
                total += first;
            }

        //if the remainder of total divided by 10 is not 0 then ....
        if ((total % 10) != 0)
            multTotal = ((total/10) + 1)*10;
        else
            multTotal = total;

        //set the checkDigit
        checkDigit = (multTotal - total);

        //Compare the checkDigit to the last digit of the id
        lastDigit = id.substring(checkDigitLength-1,checkDigitLength);

        if (checkDigit != Character.getNumericValue(lastDigit.charAt(0)))
            return false;
    }

    return true;
  }
  /**
   * This method is to validate discover card as in jccas.
   * @param id
   * @param idBegin
   * @param first7
   * @return
   */
  private static boolean isValidDiscover(String id, int idBegin,int first7){
  	 logger.info("Payment validation for Discover Card.");
		if((idBegin >= 60 & idBegin <= 69) && (id.length() == 16)){
			if(first7 >= 6011000 && first7 <= 6011999)
				return true;						
			if(first7 >= 6221260 && first7 <= 6229259)
				return true;
			if(first7 >= 6240000 && first7 <= 6269999)
				return true;
			if(first7 >= 6282000 && first7 <= 6288999)
				return true;			
			if(first7 >= 6440000 && first7 <= 6599999)
				return true;
		}
		//Here we are validating diners card validations also.
		return isValidCard(id, idBegin, first7);
	}
  /**
   * This method is to validate Diners Card as in jccas.
   * @param id
   * @param idBegin
   * @param first7
   * @return
   */
  private static boolean isValidDinersCard(String id, int idBegin,int first7){
  	 logger.info("Payment validation for Diners Card.");
		return isValidCard(id, idBegin, first7);
	}
  /**
   * This method is to validate Card details for both Discover card and Diners Card.
   * @param id
   * @param idBegin
   * @param first7
   * @return
   */
  private static boolean isValidCard(String id, int idBegin, int first7) {
  	if(((idBegin == 30 || idBegin == 35 || idBegin == 38) && id.length() == 16 )|| (idBegin == 36 && id.length() == 14)){

  		if(first7 >= 3000000 && first7 <= 3059999)
  			return true;
  		if(first7 >= 3095000 && first7 <= 3095999)
  			return true;
  		if(first7 >= 3528000 && first7 <= 3589999)
  			return true;
  		if(first7 >= 3600000 && first7 <= 3699999)
  			return true;
  		if(first7 >= 3800000 && first7 <= 3999999)
  			return true;
  	}
  	return false;
  }
 /**
  * This method is to check card authentication provider,Whether it is BAMS Or CCAS.If it is BAMS Return True otherwise false.
  * @param ccType
  * @return
  */
	private static boolean isAuthProviderBAMS(String ccType){
		boolean authProviderBAMS=false;
		try{
			ConfigurationUtil cu = ConfigurationUtil.getInstance();
			String bamsCCList = cu.getFrpGlobalParm(AUTH_CONFIG, BAMS_CC_LIST);//It will get what are the cards accepted by BAMS.
			String authProvider = cu.getFrpGlobalParm(AUTH_CONFIG, AUTH_PROVIDER);
			if(BAMS_AUTH_PROVIDER.equals(authProvider) && checkValidBamsCC(ccType, bamsCCList) ){ 
				authProviderBAMS=true;
			}
		}catch(Exception e){
			logger.error("Error Occured while loading BAMS global parameters.");
			return false;
		}
		return authProviderBAMS;
	}
	/**
	 * Checks the given ccType present in comma separated list of credit cards. Return true if present other wise false
	 * 
	 * @param ccType
	 * @param csCcList
	 * @return
	 */
	public static boolean checkValidBamsCC(String ccType, String csCcList) {
		String[] ccList = csCcList.split(",");
		for(String cardType : ccList) {
			if(ccType.equals(cardType)) {
				return true;
			}
		}
		return false;
	}
/**
* AAA validation
*
* 11/18/02 JBozarth - AAA validation is now done directly in ValidateSVC
*
    public boolean checkAAA(String id) throws IOException
    {
    //Remove the spaces
    id = id.trim();

    //Validate the id is the valid length
    if (id.length() != 6)
        return false;

    //Validate the id is numeric
    try{
        Integer.parseInt(id);
    }catch(NumberFormatException e){
        return false;
    }

    //Read in the list of valid numbers and
    //check to see if the number is in the list
    File f = new File("c:\\Tom\\SARK\\AAA");  //CHANGE THIS TO BE ACTUAL FILE
    FileReader fr = new FileReader(f);
    BufferedReader br = new BufferedReader(fr);
    HashSet hs = new HashSet();
    String s = "";

    while ((s=br.readLine())!= null)
     {
        hs.add(s);
     }

     fr.close();
     br.close();

     if (!hs.contains(id))
        return false;

    return true;
    }
*/

/**
* Air Canada validation
*
*/
    public boolean checkAirCanada(String id)
    {
    String subTotalString = "";
    int subTotal = 0 ;
    int grandTotal = 0 ;
    int grandTotalLength = 0;

    //Remove the spaces
    id = id.trim();

    //Validate the id is the valid length
    if (id.length() != 9)
        return false;

    //Validate the id is numeric
    try{
        Integer.parseInt(id);
    }catch(NumberFormatException e){
        return false;
    }

    //Account number validation
    for(int i = 1; i <= 8; i++)
        {
            //Convert String to an int
            subTotal = Character.getNumericValue(id.charAt(i-1));

            //if the int number is even, double it
            if ((i % 2 ) == 0)
                subTotal *= 2;

            //convert the int to a String
            subTotalString = subTotal + "";

            //if the subtotal length is > 1, add the first 2 digits together
            if (subTotalString.length() > 1)
                {
                    subTotal = Character.getNumericValue(subTotalString.charAt(0)) +
                               Character.getNumericValue(subTotalString.charAt(1)) ;
                }
            //add the subTotal to the grandTotal
            grandTotal += subTotal;
         }

        //Get the length of the grandTotal
        grandTotalLength = (grandTotal + "").length();

        //If the length is > 1, then the grandTotal is now the first character of the grandTotal
        if (grandTotalLength > 1)
        {
            String temp = grandTotal + "";
            grandTotal = Character.getNumericValue(temp.charAt(grandTotalLength-1));

        }

        //If the grandTotal is now > 0, then the grandTotal is now 10 minus the grandTotal
        if (grandTotal > 0)
            grandTotal = 10 - grandTotal;

        //If the grandTotal does not equal the final number of the id then return false
        if (grandTotal != Character.getNumericValue(id.charAt(8)))
        {

            return false;
        }

    return true;
    }

/**
* Alaska Air validation
*
*/
    public boolean checkAlaskaAir(String id)
    {
    String partialId = "";
    String lastId = "";
    int partialIdInt;
    int lastIdInt;

    //Remove the spaces
    id = id.trim();

    //Validate the id is the valid length
    if (id.length() < 8 || id.length() > 12)
        return false;

    //Validate the id is numeric
    try{
        Integer.parseInt(id);
    } catch (NumberFormatException e){
        return false;
    }

    //if the remainder of the first 11 characters divided by 7 does not equal the 12th character, fail
    partialId = id.substring(0,id.length() - 1);
    lastId = id.substring(id.length() - 1,id.length());

    //Convert the Strings to ints
    partialIdInt = StringToInt(partialId);
    lastIdInt = Character.getNumericValue(lastId.charAt(0));

    if ((partialIdInt % 7) != lastIdInt)
    {
        return false;
    }

    return true;

    }

/**
* American Airlines Advantage validation
*
*/
    public boolean checkAmericanAirlinesAdvantage(String id)
    {
        HashMap AAAValueMap = new HashMap();
        HashMap AAAIdMap = new HashMap();

        String convertCounter = "";
        String pos = "";
        String single = "";
        String convertedSingle = "";
        String idPosition7 = "";
        int finalPosition = 0;
        int singleInt = 0;
        int finalIdPosition7 = 0;
        int total = 0;
        int checkDigit = 0;

        //Remove the spaces
        id = id.trim();

        //Validate the id is the valid length
        if (id.length() != 7)
            return false;

        //Convert the id to upper case
        id = id.toUpperCase();

        //Populate the Hash Map of
        AAAValueMap.put( "0", "240" );
        AAAValueMap.put( "1", "241" );
        AAAValueMap.put( "2", "242" );
        AAAValueMap.put( "3", "243" );
        AAAValueMap.put( "4", "244" );
        AAAValueMap.put( "5", "245" );
        AAAValueMap.put( "6", "246" );
        AAAValueMap.put( "7", "247" );
        AAAValueMap.put( "8", "248" );
        AAAValueMap.put( "9", "249" );
        AAAValueMap.put( "A", "193" );
        AAAValueMap.put( "B", "194" );
        AAAValueMap.put( "C", "195" );
        AAAValueMap.put( "D", "196" );
        AAAValueMap.put( "E", "197" );
        AAAValueMap.put( "F", "198" );
        AAAValueMap.put( "G", "199" );
        AAAValueMap.put( "H", "200" );
        AAAValueMap.put( "J", "209" );
        AAAValueMap.put( "K", "210" );
        AAAValueMap.put( "L", "211" );
        AAAValueMap.put( "M", "212" );
        AAAValueMap.put( "N", "213" );
        AAAValueMap.put( "P", "215" );
        AAAValueMap.put( "R", "217" );
        AAAValueMap.put( "S", "226" );
        AAAValueMap.put( "T", "227" );
        AAAValueMap.put( "U", "228" );
        AAAValueMap.put( "V", "229" );
        AAAValueMap.put( "W", "230" );
        AAAValueMap.put( "X", "231" );
        AAAValueMap.put( "Y", "232" );

        //Populate the Hash Map of id positions
        AAAIdMap.put("1","1");
        AAAIdMap.put("2","3");
        AAAIdMap.put("3","3");
        AAAIdMap.put("4","5");
        AAAIdMap.put("5","4");
        AAAIdMap.put("6","6");

        for(int i = 1; i <= 6; i++)
        {
            //convert the counter int to a String
            convertCounter = Integer.toString(i);

            //Get the String value from the id HashMap
            pos = (String)AAAIdMap.get(convertCounter);

            //Convert that String to an int, and subtract 1 because of base 0
            finalPosition = Character.getNumericValue(pos.charAt(0));
            finalPosition--;

            //Get the substring from the id and hold it in a String
            single = id.substring(finalPosition , finalPosition + 1);

            //Look up that String in the character mapping HashMap
            convertedSingle = (String)AAAValueMap.get(single);

            //Convert that String to an int
            singleInt = StringToInt(convertedSingle);

            //Total the numbers
            total += singleInt;
        }

        //Get the check digit
        checkDigit = total % 5;
        checkDigit = (checkDigit *10) / 5;

        //Get the 7th character of the id and hold it in a String
        idPosition7 = id.substring(6);

        //Convert the String to an int
        finalIdPosition7 = Character.getNumericValue(idPosition7.charAt(0));

        //Compare the check digit to the last number in the ID
        if (checkDigit != finalIdPosition7)
        {
            return false;
        }

        return true;
    }

/**
  *   America West validation
  *
  */

  public boolean checkAmericaWest(String id)
  {

     int checkDigitIdInt = 0;
     int multiplier = 0;
     int total = 0;
     int lastDigitInt = 0;
     int finTotal = 0;
     int digits = 0;
     int singleInt = 0;

     String Total = "";
     String single = "";
     String lastDigit = "";
     String checkDigitId = "";

     //Remove the spaces
     id = id.trim();

     //Validate length of the id
     if (id.length() != 11)
       return false;

     //Validate the id is numeric
     try{Long.parseLong(id);
       } catch(NumberFormatException e){
           return false;
       }

     //multiply odd positions by 1 and even digits by 2
     checkDigitId = id.substring(10);

     //Convert the strings to ints
     checkDigitIdInt = Character.getNumericValue(checkDigitId.charAt(0));

     //The Real Validation now starts....
     for(int i = 1; i <= 10; i++)
     {
       single = id.substring(i-1,i);
       singleInt = StringToInt(single);

       multiplier = i % 2;

       if (multiplier == 0) {
          digits = (singleInt * 2);
       }
       else {
          digits = (singleInt * 1);
       }

       //if the subtotal length is > 1, add the first 2 digits together
       if ((digits / 10) >= 1){
          total += 1 + (digits - 10);
       }
       else {
          total += digits;
       }

    }  // end of loop

     //get last digit from total
     Total = String.valueOf(total);
     lastDigit = Total.substring(Total.length()-1,Total.length());
     lastDigitInt = Character.getNumericValue(lastDigit.charAt(0));

     // subtract last digit from 10
     finTotal = 10 - lastDigitInt;

     if (checkDigitIdInt != finTotal) {
         return false;
     }

     return true;

  }

/**
  *   Baymont validation
  *
  */

  public boolean checkBaymont(String id)
  {
    int checkDigit = 0;
    int digitSum = 0;
    int digit1 = 0;
    int digit2 = 0;
    int digit3 = 0;
    int digit4 = 0;
    int digit5 = 0;
    int digit6 = 0;
    int digit7 = 0;
    int digit8 = 0;
    int calcDigit = 0;

     //Remove the spaces
     id = id.trim();

     //Validate length of the id
     if (id.length() != 9)
       return false;

     //Validate the id is numeric
     try{Integer.parseInt(id);
       } catch(NumberFormatException e){
           return false;
       }

    checkDigit = Character.getNumericValue(id.substring(8,9).charAt(0));
    digit1 = ((Character.getNumericValue(id.substring(0,1).charAt(0)) * 2 )) % 9;
    digit2 = Character.getNumericValue(id.substring(1,2).charAt(0));
    digit3 = ((Character.getNumericValue(id.substring(2,3).charAt(0)) * 2 )) % 9;
    digit4 = Character.getNumericValue(id.substring(3,4).charAt(0));
    digit5 = ((Character.getNumericValue(id.substring(4,5).charAt(0)) * 2 )) % 9;
    digit6 = Character.getNumericValue(id.substring(5,6).charAt(0));
    digit7 = ((Character.getNumericValue(id.substring(6,7).charAt(0)) * 2 )) % 9;
    digit8 = Character.getNumericValue(id.substring(7,8).charAt(0));

    digitSum = digit1 + digit2 + digit3 + digit4 + digit5+ digit6 + digit7 + digit8;

    calcDigit =   (10 - (digitSum % 10)) % 10;

    if (calcDigit != checkDigit)
        return false;

    return true;
  }

/**
* Best Western validation
*
*/
    public boolean checkBestWestern(String id)
    {
        String checker = "";
        String temp = "";
        String tempString = "";
        String digitsTotalStr = "";
        String lastId = "";
        int odd = 0;
        int oddTotal = 0;
        int even = 0;
        int evenTotal = 0;
        int digitsTotal = 0;
        int lastDigit = 0;
        int checkDigit = 0;

        //Remove the spaces
        id = id.trim();

        //Validate the id is the valid length
        if (id.length() != 16)
            return false;

        //Validate the id is numeric
        try{
            Double.parseDouble(id);
        } catch (NumberFormatException e){
            return false;
        }

        if (! id.substring(0,6).equals("600663"))
            return false;

        checker = id.substring(0,15);
        checker.trim();

        //sum the odds
        for(int i = 0; i <= id.length() -1; i=i+2)
        {
            tempString = checker.substring(i,i+1);
            odd = Character.getNumericValue(tempString.charAt(0)) * 2;

            if ((odd + "").length() == 2)
                odd = Character.getNumericValue((odd + "").charAt(0)) +
                      Character.getNumericValue((odd + "").charAt(1)) ;

            oddTotal += odd;
        }

        //sum the evens
        for(int i = 1; i <= id.length() -2 ; i=i+2)
        {
            tempString = checker.substring(i,i+1);
            even = Character.getNumericValue((tempString + "").charAt(0));

            evenTotal += even;
        }

        //subtract the last digit of the total from 10 if the last digit is not 0
        digitsTotal = evenTotal + oddTotal;
        digitsTotalStr = digitsTotal + "";
        lastDigit = Character.getNumericValue(digitsTotalStr.substring(digitsTotalStr.length()-1, digitsTotalStr.length()).charAt(0));

        if (lastDigit == 0)
            checkDigit = 0;
        else
            checkDigit = 10 - lastDigit;

        //Compare the checkDigit to the last digit of the id
        lastId = id.substring(id.length()-1, id.length());
        if (checkDigit != Character.getNumericValue((lastId).charAt(0)))
            return false;

        return true;
    }

/**
* Delta validation
*
*/
    public boolean checkDelta(String id)
    {
  
    //Delta and northwest have merged.  If we have not returned yet, check if this is a northwest code
    if (checkNorthwest(id)){
       return true;
    }
  
    int checkDigitValue = 0;
    String checkDigit = "";

    //Remove the spaces
    id = id.trim();

    //Validate the id is the valid length
    if (id.length() != 10)
        return false;

    //Validate the id is numeric
    try{
        Double.parseDouble(id);
    } catch (NumberFormatException e){
        return false;
    }

    //Convert the id to an int
    double idint = StringToDouble(id);

    //Check if id falls betweencertain ranges, return true and exit the check routine
    if ((idint >= 8704) & (idint <=8999))
        return true;

    if ((idint >= 900000) & (idint <=999999))
        return true;

    //retrieve the check digit
     checkDigit = id.substring(9,10);
     checkDigitValue = Character.getNumericValue(checkDigit.charAt(0));

     //if the check Delta Id function returns the same value as the checkDigit, end the program
     //but if it doesn't then if the id is in a certain range, subtract 100000 and try again
     if (checkDeltaId(id) == checkDigitValue)
        return true;
     else if ((idint >= 100000) & (idint <= 1999999))
     {
       idint -= 100000;
       String temp = idint + "";
       if(checkDeltaId(temp) == checkDigitValue)
           return true;
     }

      //Delta and northwest have merged.  If we have not returned yet, check if this is a northwest code
      if (checkNorthwest(id)){
          return true;
      }
      return false;
}
/**
* Called by checkDelta
*
*/

    //Multiply the value at every odd digit of the id (1,3,5,7,9) by 2 and add together.
    //If the result of the multiplication is greater than 10, then add the 2 digits together
    public int checkDeltaId(String id)
    {
        String single = "";
        int oddTotal = 0;
        int evenTotal = 0;
        int odd = 0;
        int singleValue = 0;
        int remainder = 0;
        int checkDigitTest = 0;

        //Remove the spaces
        id = id.trim();

        //Process the odd digits
        for(int i = 0; i <= 9; i=i+2)
        {
            //Get the value
            single = id.substring(i,i+1);

            //convert the String to an int and multiply by 2
            singleValue = (Character.getNumericValue(single.charAt(0))) * 2;

            //get the sum of the digits if the length of the string is greater than 1
            if ((singleValue + "").length() > 1)
                singleValue  = Character.getNumericValue((singleValue + "").charAt(0)) +
                            Character.getNumericValue((singleValue + "").charAt(1)) ;

             oddTotal += singleValue;
         }

        //Process the even digits
        for(int i = 1; i <= 8; i=i+2)
        {
            single = id.substring(i,i+1);

            //convert the String to an int
            singleValue = Character.getNumericValue(single.charAt(0));
            evenTotal += singleValue;
        }

        //Get the sum of the odd and even totals, divide by 10 and get the remainder
        remainder = ((oddTotal + evenTotal) % 10);

        //if the remainder is 0 then checkDigitTest = 0, else subtract remainder from 10 to get checkDigitTest
        if (remainder == 0)
            checkDigitTest = 0;
        else
            checkDigitTest = 10-remainder;

        //return the checkDigitTest value
        return checkDigitTest;
    }

/**
  *   Gold Points validation
  *
  */

  public boolean checkGoldPoints(String id)
  {
     String checkDigitId = "";
     String Total = "";
     String single = "";
     String lastDigit = "";
     String Digits = "";
     String chkDigits = "";
     int checkDigitIdInt;
     int chkDigitsInt = 0;
     int multiplier = 0;
     int total = 0;
     int digits = 0;
     int evenDigits = 0;
     int oddDigits = 0;
     int singleInt = 0;
     int lastDigitInt = 0;
     int finTotal = 0;

     //Remove the spaces
     id = id.trim();

     //Validate length of the id
     if (id.length() != 16)
       return false;

     //Validate the id is numeric
     try {
        Float.parseFloat(id);
     }
        catch(NumberFormatException e) {
           return false;
     }

     //multiply odd positions by 1 and even digits by 3
     checkDigitId = id.substring(15);

     //Convert the strings to ints
     checkDigitIdInt = Character.getNumericValue(checkDigitId.charAt(0));

     //The Real Validation now starts....
     for(int i = 1; i <= 15; i++)
     {
       single = id.substring(i-1,i);
       singleInt = StringToInt(single);

       multiplier = i % 2;

       if (multiplier == 0) {
          evenDigits += singleInt;
       }
       else {
          oddDigits += singleInt;
       }

    }  // end of loop

    //do quick check on numbers
    digits = oddDigits + evenDigits;
    Digits = String.valueOf(digits);
    chkDigits = Digits.substring(Digits.length()-1,Digits.length());
    chkDigitsInt = Character.getNumericValue(chkDigits.charAt(0));

    // if check = 0 then valid number, get out of program
    if (chkDigitsInt == 0) {
       //System.out.println("Gold Points True");
       return true;
    }
    //calculate numbers
    total = ((oddDigits + 4) * 3) + evenDigits;

     //get last digit from total
     Total = String.valueOf(total);
     lastDigit = Total.substring(Total.length()-1,Total.length());
     lastDigitInt = StringToInt(lastDigit);

     // subtract last digit from 10
     finTotal = 10 - lastDigitInt;
     if (finTotal == 10) {
        finTotal = 0;
     }

     if (checkDigitIdInt != finTotal) {
      return false;
     }

     return true;
   }

/**
  *   Hawaiian validation
  *
  */

  public boolean checkHawaiian(String id)
  {
    int checkDigit = 0;
    int sum = 0;
    int calcDigit = 0;

     //Remove the spaces
     id = id.trim();

     //Validate length of the id
     if (id.length() != 9)
       return false;

     //Validate the id is numeric
     try{Integer.parseInt(id);
       } catch(NumberFormatException e){
           return false;
       }

     //Get the checkDigit
     checkDigit = Character.getNumericValue(id.substring(8).charAt(0));

      for(int i=0; i<=7; i++)
      {
          sum += Character.getNumericValue(id.substring(i,i+1).charAt(0));
      }

      sum *= 2;

      calcDigit =  (sum % 10);

      if (calcDigit != checkDigit)
          return false;

       return true;
  }
  
  /**
   * Jet Blue validation
   * 
   */
  public boolean checkJetBlue(String id)
  {
     int first2 = 0;
     int checkDigit = 0;
     int sum = 0;
     int remainder = 0;
     
     if(id == null){
      return false;
     }
     
     //Remove the spaces
     id = id.trim();
       
     //Validate the id is the valid length
     if ((id.length() != 10)){
        return false;
     }
     
     //Validate the id is numeric
     try{Integer.parseInt(id);
      } catch(NumberFormatException e){
          return false;
      }

    //Get the first 2 digits
    first2 = StringToInt(id.substring(0,2));
    
    //Check to see if the value of the first two digits is between 10 and 99
    if((first2 < 10) || (first2 > 99)){
      return false;
    }
    
    //Get the checkDigit
    checkDigit = Character.getNumericValue(id.substring(9).charAt(0));
    
    //Add the first nine digits of membership ID together
    for(int i=0; i<=id.length() - 2; i++){
      sum += Character.getNumericValue(id.substring(i,i+1).charAt(0));
    }
    
    //Perform MOD 7
    remainder = sum % 7;
    
    //Compare checkDigit to the last digit in the id
    if (checkDigit != remainder) {
        return false;
    }
  
    return true;
  }
  
  

/**
  *   Midwest validation
  *
  */
 public boolean checkMidwestExpress(String id)
 {
    int lastDigit;
    int checkDigit;
    Long idAsLong = null;
    
	  //If ID equals null, return false
	  if(id==null) return false;
	
    //Remove the spaces
    id = id.trim();
    
    //Convert the ID to a long to strip out the zeroes as well as confirm the id is all numberics
    try{idAsLong = Long.parseLong(id);
			} catch(NumberFormatException e){
				return false;
			}

    if(idAsLong != null) {
      //Assign the long value back to the string ID.  This is necessary to find the correct length of the membership ID.
      id = Long.toString(idAsLong);
      
      if ((id.length() == 8)){       //Algorithm for 8 digit membership ID
        lastDigit = Character.getNumericValue(id.charAt(7)); //gets the last digit
        BigDecimal bd = new BigDecimal(id.substring(0,7));  //gets first 7 digits and puts in BigDecimal
        bd = bd.divide(new BigDecimal(7), 5, bd.ROUND_UP); //divide by 7 and go 5 decimals deep
        bd = bd.remainder(new BigDecimal(1)); //get remainder in proper format from previous division
        bd = bd.multiply(new BigDecimal(7)); //multiply by 7
        checkDigit = bd.intValue(); //assign this remainder (in Int Format) to checkdigit
        
        if (checkDigit == lastDigit)
          return true;	 	
      }
      else if (((id.length() == 9))){       //Algorithm for 9 digit membership ID  
        lastDigit = Character.getNumericValue(id.charAt(8)); //gets the last digit
        BigDecimal bd = new BigDecimal(id.substring(0,8));  //gets first 10 digits and puts in BigDecimal
        bd = bd.divide(new BigDecimal(7), 5, bd.ROUND_UP); //divide by 7 and go 5 decimals deep
        bd = bd.remainder(new BigDecimal(1)); //get remainder in proper format from previous division
        bd = bd.multiply(new BigDecimal(7)); //multiply by 7
        checkDigit = bd.intValue(); //assign this remainder (in Int Format) to checkdigit
        
        if (checkDigit == lastDigit)
          return true;	 	
      }
      else if ((id.length() == 11)){       //Algorithm for 11 digit membership ID
        lastDigit = Character.getNumericValue(id.charAt(10)); //gets the last digit
        BigDecimal bd = new BigDecimal(id.substring(0,10));  //gets first 10 digits and puts in BigDecimal
        bd = bd.divide(new BigDecimal(7), 5, bd.ROUND_UP); //divide by 7 and go 5 decimals deep
        bd = bd.remainder(new BigDecimal(1)); //get remainder in proper format from previous division
        bd = bd.multiply(new BigDecimal(7)); //multiply by 7
        checkDigit = bd.intValue(); //assign this remainder (in Int Format) to checkdigit
        
        if (checkDigit == lastDigit)
          return true;	 			
      }
    }
	
    //If membership ID is not 8, 9, or 11 digits long or validation doesn't match
    return false;
  }

  /**
    * Northwest validation Version 2
    *   This replaced an earlier version starting with apollo 3.7
    *   The calculation is:
    *     Dividend / 7 = Quotient    
    *     Remainder = Dividend – (Quotient * 7)    
    *     If Remainder = Check Digit, it is a valid number
    *   Example:
    *     Using account number 279261964 as an example, 27926196 is the divisor and 4 is the check digit.
    *     27926196 / 7 = 3989456.571    	After truncation, the quotient is 6.
    *     27926196 – (6 * 7) = 27926154	After truncation, the remainder is 4.
    *     The remainder (4) equals the check digit (4).
    */
  
    public boolean checkNorthwest(String id)
    {
        long dividend;
        int checkDigit;
        long quotient;
        long remainder;
        
        boolean isValid = true;
    
        //Remove the spaces
        id = id.trim();
    
        //Validate the id is numeric
        try{
            Long.parseLong(id);
        } catch (NumberFormatException e){
            isValid = false;
        }
        if (isValid){
            //Validate the id is the valid length
            if ((id.length() == 9) || (id.length() == 12)){
                 //Parse out the Id and convert the Strings to ints
                 dividend = StringToLong(id.substring(0,id.length()-1));
                
                 checkDigit = Character.getNumericValue(id.charAt(id.length()-1));
      
                 //calculate the quotient
                 quotient = dividend / 7;
                 // truncate the value to only the ones digit
                 quotient %= 10;
                 
                 //calculate the remainder
                 remainder = dividend - (quotient * 7);
                 // truncate the remainder to only one digit
                 remainder %= 10;
               
                 if (remainder != checkDigit){           
                    isValid = false;
                 }
             }
             else{
                 isValid = false;
             }
        }
        return isValid;
    }


/**
* United validation
*
*/
    public boolean checkUnited(String id)
    {
    	boolean retval = false;
    	String lid = id.trim();
    	int oddDigitVal  = 0;
    	int oddDigitSum  = 0;
    	int evenDigitSum = 0;
    	int calculatedCheckDigit = 0;
    	int embeddedCheckDigit = 0;

    	try {
	    	if (lid.length() == 8) {
	    		
	    		// Normal United Membership number
	    		//
	    		oddDigitVal   = unitedCharToInt(lid.charAt(0), true) * 2;
	    		oddDigitSum   = sumTwoDigitNumber(oddDigitVal);
	    		evenDigitSum  = unitedCharToInt(lid.charAt(1), true);
	    		oddDigitVal   = unitedCharToInt(lid.charAt(2), true) * 2;
	    		oddDigitSum  += sumTwoDigitNumber(oddDigitVal);
	    		// Only first 3 digits can have chars
	    		evenDigitSum += unitedCharToInt(lid.charAt(3), false);
	    		oddDigitVal   = unitedCharToInt(lid.charAt(4), false) * 2;
	    		oddDigitSum  += sumTwoDigitNumber(oddDigitVal);
	    		evenDigitSum += unitedCharToInt(lid.charAt(5), false);
	    		oddDigitVal   = unitedCharToInt(lid.charAt(6), false) * 2;
	    		oddDigitSum  += sumTwoDigitNumber(oddDigitVal);
	    		
	    		embeddedCheckDigit = unitedCharToInt(lid.charAt(7), false);
	    		calculatedCheckDigit = 10 - ((oddDigitSum + evenDigitSum) % 10);
	    		calculatedCheckDigit = (calculatedCheckDigit==10)?0:calculatedCheckDigit;
	    		
	    		if (embeddedCheckDigit == calculatedCheckDigit) {
	    			// Check digit match
	    			retval = true;
	    		} else {
	    			// Check digit didn't match so add one and check again
	    			calculatedCheckDigit++;
	    			calculatedCheckDigit = (calculatedCheckDigit==10)?1:calculatedCheckDigit;
	    			if (embeddedCheckDigit == calculatedCheckDigit) {
	    				retval = true;
	    			}
	    		}
	    			
	    	} else if (lid.length() == 11) {

	    		// Legacy United Membership number
	    		//
	    		retval = checkUnitedLegacy(lid);
	    	} 
    	} catch (IllegalArgumentException e) {
    		retval = false;
    	}
    	return retval;
    }

    /**
     * Sums the digits in a two digit number (or just returns passed digit if single)
     */
    private int sumTwoDigitNumber(int num) {
    	int tens = num / 10;
    	int ones = num % 10;
    	return (tens + ones);
    }
    
    /**
     * Makes sure the passed character is either a numeric digit or one of the 
     * allowed characters for a United membership number.  If not, an exception is thrown.  
     * Otherwise the appropriate integer value is returned.
     */
    private int unitedCharToInt(char cin, boolean charsAllowed) throws IllegalArgumentException {
    	int retval = -1;
    	
		// Numeric digit is always ok
    	if (Character.isDigit(cin)) {
    		retval = Character.getNumericValue(cin);
    		
    	// Allow only recognized United characters (if charsAllowed is set)
    	} else if (charsAllowed) {
    		switch(Character.toUpperCase(cin)) {
    		case 'A': 
    		case 'J':
    			retval = 1;
    			break;
    		case 'B':
    		case 'K':
    		case 'S':
    			retval = 2;
    			break;
    		case 'C':
    		case 'L':
    		case 'T':
    			retval = 3;
    			break;
    		case 'D':
    		case 'M':
    		case 'U':
    			retval = 4;
    			break;
    		case 'E':
    		case 'N':
    		case 'V':
    		   retval = 5;
    		   break;
    		case 'F':
    		case 'O':
    		case 'W':
    			retval = 6;
    			break;
    		case 'G':
    		case 'P':
    		case 'X':
    			retval = 7;
    			break;
    		case 'H':
    		case 'Q':
    		case 'Y':
    			retval = 8;
    			break;
    		case 'I':
    		case 'R':
    		case 'Z':
    			retval = 9;
    			break;
    		}
    	}
    	if (retval < 0) {
    		throw new IllegalArgumentException("Unrecognized United Membership character");
    	}
    	return retval;
    }
    
    private boolean checkUnitedLegacy(String id)
    {
        String single;
        String convertCounter;
        String hashValue;
        int singleInt = 0;
        int total = 0;
        int hashInt = 0;
        int remainder = 0;
        int checkDigit = 0;
        int compare = 0;

        //Validate the id is the valid length
        if (id.length() != 11)
            return false;

        //Validate the id is numeric
        try{
            Double.parseDouble(id);
        } catch (NumberFormatException e){
            return false;
        }

        //Create and populate the Hash Map
        HashMap UnitedMap = new HashMap();
        UnitedMap.put("1","5");
        UnitedMap.put("2","4");
        UnitedMap.put("3","3");
        UnitedMap.put("4","2");
        UnitedMap.put("5","7");
        UnitedMap.put("6","6");
        UnitedMap.put("7","5");
        UnitedMap.put("8","4");
        UnitedMap.put("9","3");
        UnitedMap.put("10","2");

        //Loop throught the ID, get the number
        //Multiply the number by the value in the HashMap that is associated with
        //the number of times the loop has run and keep a running total

        for(int i = 1; i <= 10; i++)
        {
            //convert the counter int to a String
            convertCounter = Integer.toString(i);

            //Get the String value from the id and convert it to an int
            single = id.substring(i-1, i);
            singleInt = Character.getNumericValue(single.charAt(0));

            //Get the String value from the HashMap and convert it to an int
            hashValue = (String)UnitedMap.get(convertCounter);
            hashInt = Character.getNumericValue(hashValue.charAt(0));

            //calculate the result and add the number to the total
            total += (singleInt * hashInt);
        }

        //Get the remainder of the total divided by 11
        remainder = total % 11;

        //Set the value to compare to the checkDigit
        switch (remainder)
        {
            case 0: compare = 0;
            break;
            case 1: compare = 0;
            break;
            default: compare = 11-remainder;
        }

        //the checkDigit is the last digit of the id
        checkDigit = Character.getNumericValue(id.charAt(id.length() -1));

        //compare the numbers
        if(compare != checkDigit)
            return false;

        return true;
    }

/**
* US Air validation
*
*/
   public boolean checkUsAir(String id) 
   {
      // Defect 937: America West merged into US Airways 
      String membershipId = new String(id);
      boolean isValid = false;
      try
      {
        isValid = validateUsAir(id);
        
        if (!isValid) 
        {
          try
          {
            // Attempt the America West check digit.
            isValid = checkAmericaWest(membershipId);
          }
          catch (Throwable t0)
          {
            // do nothing
          }
        }
      }
      catch (Throwable t1)
      {
        try
          {
            // Attempt the America West check digit.
            isValid = checkAmericaWest(membershipId);
          }
          catch (Throwable t2)
          {
            // do nothing
          }
      }
      
      return isValid;
  }
  
  private boolean validateUsAir(String id)
  {
     HashMap UsAirValueMap = new HashMap() ;
     HashMap UsAirIdMap = new HashMap() ;

     String singleDigitCheck = "";
     String convertCounter = "";
     String pos = "";
     String single = "";
     String convertedSingle = "";
     String idPosition7 = "";
     String checkPosition = "";
     String numerator = "";
     String checkDigitStr = "";
     String checkDigitLast = "";
     int finalPosition = 0;
     int checkIdPosition = 0;
     int singleInt = 0;
     int finalIdPosition7 = 0;
     float total = 0;
     float checkDigit = 0;
     int numeratorInt = 0;
     int finalDigitLast = 0;

     //Remove the spaces
     id = id.trim();

     //Convert Id to upper case
     id = id.toUpperCase();

     //Validate length of the id
     if ((id.length() < 7) | (id.length() > 10)) {
       return false;
     }

     //Populate the Hash Map of
     UsAirValueMap.put( "0", "240" );
     UsAirValueMap.put( "1", "241" );
     UsAirValueMap.put( "2", "242" );
     UsAirValueMap.put( "3", "243" );
     UsAirValueMap.put( "4", "244" );
     UsAirValueMap.put( "5", "245" );
     UsAirValueMap.put( "6", "246" );
     UsAirValueMap.put( "7", "247" );
     UsAirValueMap.put( "8", "248" );
     UsAirValueMap.put( "9", "249" );
     UsAirValueMap.put( "A", "193" );
     UsAirValueMap.put( "B", "194" );
     UsAirValueMap.put( "C", "195" );
     UsAirValueMap.put( "D", "196" );
     UsAirValueMap.put( "E", "197" );
     UsAirValueMap.put( "F", "198" );
     UsAirValueMap.put( "G", "199" );
     UsAirValueMap.put( "H", "200" );
     UsAirValueMap.put( "I", "201" );
     UsAirValueMap.put( "J", "209" );
     UsAirValueMap.put( "K", "210" );
     UsAirValueMap.put( "L", "211" );
     UsAirValueMap.put( "M", "212" );
     UsAirValueMap.put( "N", "213" );
     UsAirValueMap.put( "O", "214" );
     UsAirValueMap.put( "P", "215" );
     UsAirValueMap.put( "Q", "216" );
     UsAirValueMap.put( "R", "217" );
     UsAirValueMap.put( "S", "226" );
     UsAirValueMap.put( "T", "227" );
     UsAirValueMap.put( "U", "228" );
     UsAirValueMap.put( "V", "229" );
     UsAirValueMap.put( "W", "230" );
     UsAirValueMap.put( "X", "231" );
     UsAirValueMap.put( "Y", "232" );
     UsAirValueMap.put( "Z", "233" );

     //Populate the Hash Map of id positions
     UsAirIdMap.put("1","1");
     UsAirIdMap.put("2","3");
     UsAirIdMap.put("3","3");
     UsAirIdMap.put("4","5");
     UsAirIdMap.put("5","4");
     UsAirIdMap.put("6","6");

     //Validate the id is numeric
     if (id.length() != 7) {

        //id's = 8,9,10 characters must be numeric
        try{ Integer.parseInt(id);
          } catch(NumberFormatException e)
          { return false;
          }

        // check Id's that are 9 characters in length
        if (id.length() == 9) {
           singleDigitCheck = id.substring(0,1);
           numerator = id.substring(0,8);
           numeratorInt = StringToInt(numerator);
           checkDigit = (numeratorInt % 7);
           if (singleDigitCheck == "7") {
              if (checkDigit == 0) {
                 checkDigit = 7;
              }
           }

           //Get the 9th character of the id and hold it in a String
           checkPosition = id.substring(8);

           //Convert the String to an int
           checkIdPosition = Character.getNumericValue(checkPosition.charAt(0));
           if (checkDigit != checkIdPosition) {
              return false;
           }
        }

        // check ID's that are 8 characters in length
        else if (id.length() == 8) {
           numerator = id.substring(0,7);
           numeratorInt = StringToInt(numerator);
           checkDigit = (numeratorInt % 7);

           //Get the 8th character of the id and hold it in a String
           checkPosition = id.substring(7);

           //Convert the String to an int
           checkIdPosition = Character.getNumericValue(checkPosition.charAt(0));
           if (checkDigit != checkIdPosition) {
              return false;
           }
        }

        //check ID's that are 10 characters in length
        else if (id.length() == 10) {
           numerator = id.substring(0,9);
           numeratorInt = StringToInt(numerator);
           checkDigit = (numeratorInt % 7);

           //Get the 10th character of the id and hold it in a String
           checkPosition = id.substring(9);

           //Convert the String to an int
           checkIdPosition = Character.getNumericValue(checkPosition.charAt(0));
           if (checkDigit != checkIdPosition) {
              return false;
           }
        }
      }
      //id check for id's = 7 characters
      else {
         for(int i = 1; i <= 6; i++)
         {
            //convert the counter int to a String
            convertCounter = Integer.toString(i);

            //Get the String value from the id HashMap
            pos = (String)UsAirIdMap.get(convertCounter);

            //Convert that String to an int, and subtract 1 because of base 0
            finalPosition = Character.getNumericValue(pos.charAt(0));
            finalPosition--;

            //Get the substring from the id and hold it in a String
            single = id.substring(finalPosition , finalPosition + 1);

            //Look up that String in the character mapping HashMap
            convertedSingle = (String)UsAirValueMap.get(single);

            //Convert that String to an int
            singleInt = StringToInt(convertedSingle);

            //Total the numbers
            total += singleInt;
        }

        //Get the check digit
        checkDigit = (total / 5);
        checkDigitStr = Float.toString(checkDigit);
        checkDigitLast = checkDigitStr.substring(checkDigitStr.length()-1);
        finalDigitLast = Character.getNumericValue(checkDigitLast.charAt(0));

        //Get the 7th character of the id and hold it in a String
        idPosition7 = id.substring(6);

        //Convert the String to an int
        finalIdPosition7 = Character.getNumericValue(idPosition7.charAt(0));

        //Compare the check digit to the last number in the ID
        if (finalDigitLast != finalIdPosition7)
            return false;

     }

     return true;
  }

/**
* Sprint validation
*
*/
  public boolean checkSprint(String id)
  {
    //Remove the spaces
    id = id.trim();

   //Validate length of the id
     if (id.length() != 9)
       return false;

     //Validate the id is numeric
     try{
        Integer.parseInt(id);
       }catch(NumberFormatException e){
           return false;
       }

    return true;
  }

/**
* Southwest validation
*
*/
  public boolean checkSouthwest(String id)
  {
    //Remove the spaces
    id = id.trim();

    //Validate length of the id
    if (id.length() != 14)
       return false;

    try {
        long verifierId = Long.parseLong(id.substring(0,13));
        long verifier = verifierId / 7;
        verifier = verifier * 7;

        String validator = new Long(verifierId - verifier).toString();

        if(!id.substring(13).equals(validator)) {
            return false;
        }

    } catch(Exception e) {
        e.printStackTrace();
        return false;
    }

    return true;
  }

/**
* Virgin Air validation
* This method has been modified on 11/24/2015 (Enhancement QE-89) to support 10 digit number validation also.
*/
  public boolean checkVirginAir(String number) {
	  if(number != null && number.trim().length() >0){
		  logger.debug("Virgin airlines membership validation started ");
		  if(number.trim().length()== TEN_DIGITS_NUM){ //For Ten digits number validation
			  return this.checkTenDigitsVirginAirNumber(number);
		  }else if(number.trim().length()== ELEVEN_DIGITS_NUM){ //For Eleven Digits number validation.
			  return this.checkElevenDigitsVirginAirNumber(number);
		  }else{
			  logger.error("Virgin Airlines membership number is not in valid  format.");
			  return false;
		  }
	  }else{
		  logger.error("Virgin Airlines membership number is not in valid  format.");
		  return false;
	  }
  }


 /** 
   *  ATA Validation
   */
  public boolean checkATA(String id)
  {
    int idLen;
    String numerator;
    int numeratorInt, checkMod, checkId;

    if (id == null) {
      return false;
    }
    id = id.trim();
    idLen = id.length();
    
    //Validate length of the id
    if ((idLen < 7) || (idLen > 10)) {
      return false;
    }

    // Do a MOD on the last 7 digits
    numerator = id.substring(idLen - 7, idLen);
    try{
       numeratorInt = (int)Double.parseDouble(numerator);
    }catch(NumberFormatException e){
       return false;
    }
    checkMod = (numeratorInt % 7);
    
    // Get the 1st digit
    checkId = Character.getNumericValue(id.charAt(0));

    // 1st digit and MOD should match
    if (checkMod == checkId) {
      return true;
    } 
    
    return false;
  }


/**
 * ID does not need to be checked.  We will be accepting any membership id.
 * This is a place holder for if membership id requirements are added.
 */
    public boolean checkKoreanAir(String id) {
        return true;
    }

//TESTING
    public static void main(String args[])  throws IOException
    {
        ValidateMembership vm = new ValidateMembership();
        boolean x;

        // United normal
        System.out.println("Expect false for United FFFF: " + vm.checkUnited("FFFF"));
        System.out.println("Expect false for United 123: " + vm.checkUnited("123"));
        System.out.println("Expect false for United 12345678: " + vm.checkUnited("12345678"));
        System.out.println("Expect true for United FF925629: " + vm.checkUnited("FF925629"));
        System.out.println("Expect true for United FF925611: " + vm.checkUnited("FF925611"));
        System.out.println("Expect true for United FF925637: " + vm.checkUnited("FF925637"));
        System.out.println("Expect true for United FF925645: " + vm.checkUnited("FF925645"));
        System.out.println("Expect true for United FF925678: " + vm.checkUnited("FF925678"));
        System.out.println("Expect true for United FF925694: " + vm.checkUnited("FF925694"));
        System.out.println("Expect true for United FF925710: " + vm.checkUnited("FF925710"));
        // United normal but needing 1 added to checkdigit
        System.out.println("Expect false for United FF925622: " + vm.checkUnited("FF925622"));
        System.out.println("Expect true for United FF925621: " + vm.checkUnited("FF925621"));
        System.out.println("Expect true for United FF925612: " + vm.checkUnited("FF925612"));
        System.out.println("Expect true for United FF925638: " + vm.checkUnited("FF925638"));
        System.out.println("Expect true for United FF925646: " + vm.checkUnited("FF925646"));
        System.out.println("Expect true for United FF925679: " + vm.checkUnited("FF925679"));
        System.out.println("Expect true for United FF925695: " + vm.checkUnited("FF925695"));
        System.out.println("Expect true for United FF925711: " + vm.checkUnited("FF925711"));
        // United legacy
        System.out.println("Expect false for United 12345678901: " + vm.checkUnited("12345678901"));
        System.out.println("Expect true for United 00129487399: " + vm.checkUnited("00129487399"));
        System.out.println("Expect true for United 00131265247: " + vm.checkUnited("00131265247"));
        System.out.println("Expect true for United 00539942433: " + vm.checkUnited("00539942433"));
        System.out.println("Expect true for United 00414211097: " + vm.checkUnited("00414211097"));
        System.out.println("Expect true for United 00088739604: " + vm.checkUnited("00088739604"));
        System.out.println("Expect true for United 00103403421: " + vm.checkUnited("00103403421"));
        System.out.println("Expect true for United 03171606500: " + vm.checkUnited("03171606500"));
        System.out.println("Expect true for United 02075645000: " + vm.checkUnited("02075645000"));
        System.out.println("Expect true for United 00137807700: " + vm.checkUnited("00137807700"));
        System.out.println("Expect true for United 00208976052: " + vm.checkUnited("00208976052"));

/*
     x=  vm.checkAlaskaAir("123456"); // True
     x=  vm.checkAlaskaAir("1234567890123456789123456"); // True
     
     x=  vm.checkAlaskaAir("000022834206"); // True
     System.out.println("T"+x);
     x=  vm.checkAlaskaAir("50598030"); // True
     System.out.println("T"+x);
     x=  vm.checkAlaskaAir("00486430"); // True
     System.out.println("T"+x);
     x=  vm.checkAlaskaAir("40852652"); // True
     System.out.println("T"+x);
     x=   vm.checkAlaskaAir("000022834205"); // False
     System.out.println("F"+x);
     x=   vm.checkAlaskaAir("123456789123");  // False        
     System.out.println("F"+x);

     System.out.println("Expect false for ATA 15217: " + vm.checkATA("15217"));
     System.out.println("Expect true  for ATA 60015217: " + vm.checkATA("60015217"));
     System.out.println("Expect false for ATA 20015217: " + vm.checkATA("20015217"));
     System.out.println("Expect false for ATA 1007: " + vm.checkATA("1007"));
     System.out.println("Expect true  for ATA 0000007: " + vm.checkATA("0000007"));
     System.out.println("Expect false for ATA 20000008: " + vm.checkATA("20000008"));
     System.out.println("Expect true  for ATA 10000008: " + vm.checkATA("10000008"));      
        x= vm.checkNorthwest("271011647");
        System.out.println(x);
        x= vm.checkNorthwestV2("271011647");
        System.out.println(x);
        
      x= vm.checkNorthwest("926129713");
      System.out.println(x);
      x= vm.checkNorthwestV2("926129713");
      System.out.println(x);
      x= vm.checkNorthwest("932431334");
      System.out.println(x);
      x= vm.checkNorthwestV2("932431334");
      System.out.println(x);
      x= vm.checkNorthwest("942527110");
      System.out.println(x);
      x= vm.checkNorthwestV2("942527110");
      System.out.println(x);
      x= vm.checkNorthwest("943730395");
      System.out.println(x);
      x= vm.checkNorthwestV2("943730395");
      System.out.println(x);
      x= vm.checkNorthwest("994853020");
      System.out.println(x);
      x= vm.checkNorthwestV2("994853020");
      System.out.println(x);

      
      
      
      
         
        x = vm.checkAAA("438227");
        System.out.println(x);

        x = vm.checkAAA("654987");
        System.out.println(x);

        vm.checkAirCanada("739743441");  //True
        vm.checkAirCanada("530343524");  //True
        vm.checkAirCanada("111");        //False
        vm.checkAirCanada("aaaaaaaaa");  //False
        vm.checkAirCanada("111222333");  //False

        vm.checkAlaskaAir("000022834206"); // True
        vm.checkAlaskaAir("000022834205"); // False
        vm.checkAlaskaAir("123456789123");  // False

        vm.checkAmericanAirlinesAdvantage("AK65266"); //True

        x=vm.checkAmericanAirlinesAdvantage("1fc5142"); //False
        vm.checkAmericanAirlinesAdvantage("7ynm378");  //False

        x= vm.checkAmericaWest("5111111111d");
        x = vm.checkAmericaWest("0097100562"); //False
        System.out.println(x);
        x = vm.checkAmericaWest("00097100564"); //True
        System.out.println(x);

        x= vm.checkBaymont("103646468");
        x= vm.checkBaymont("106083443");

        vm.checkBestWestern("6006630222000106");     //True
        vm.checkBestWestern("6006630581061251");      //True
        vm.checkBestWestern("6006630608488297");   //True

        x= vm.checkDelta("0000008900");
        x= vm.checkDelta("2293064545");
        x= vm.checkDelta("2184889612");
        x= vm.checkDelta("2184819612");

        x = vm.checkGoldPoints("6015993011538773"); //True
        System.out.println(x);
        x = vm.checkGoldPoints("6015990148144648"); //True
        System.out.println(x);
        x = vm.checkGoldPoints("6015990148144638"); //False
        System.out.println(x);

        x= vm.checkHawaiian("108417611");

        x= vm.checkMidwestExpress("31004140"); // True

        x= vm.checkNorthwest("955994238");

        x= vm.checkUnited("00007373709");
        x= vm.checkUnited("0096749958p");
        x= vm.checkUnited("03052659075");

        x = vm.checkUsAir("1030268960");  // True
        System.out.println(x);
        x = vm.checkUsAir("57Y5X36");  //True
        System.out.println(x);
        x = vm.checkUsAir(" 8t561K0"); //True
        System.out.println(x);
        x = vm.checkUsAir("269D7R0"); //True
        System.out.println(x);
        x = vm.checkUsAir("1030268920"); //False
        System.out.println(x);
        x = vm.checkUsAir("57Y5X46"); //False
        System.out.println(x);
        x = vm.checkUsAir("8L561K1"); //False
        System.out.println(x);
        x = vm.checkUsAir("219D7R5 "); //False
        System.out.println(x);

        x = vm.checkVirginAir("00760668362"); //True
        System.out.println(x);
        x= vm.checkVirginAir("0090035269");
        x= vm.checkVirginAir("01090035269");

        x = vm.checkCreditCard("VI","4678360240690017");
        System.out.println(x);
        x = vm.checkCreditCard("VI","4217642300485570");
        System.out.println(x);
        x = vm.checkCreditCard("VI","4323711138240308");
        System.out.println(x);
        x = vm.checkCreditCard("VI","4756214260000141");
        System.out.println(x);
        x = vm.checkCreditCard("DC","38865626007217");
        System.out.println(x);
        x = vm.checkCreditCard("DC","30498462364761");
        System.out.println(x);
        x = vm.checkCreditCard("DC","38865680689017");
        System.out.println(x);
        x = vm.checkCreditCard("DC","38561244690927");
        System.out.println(x);
        x = vm.checkCreditCard("DC","38647708170900");
        System.out.println(x);
        x = vm.checkCreditCard("DC","38502355230913");
        System.out.println(x);
        x = vm.checkCreditCard("MC","5263456010042476");
        System.out.println(x);
        x = vm.checkCreditCard("MC","5511791000002431");
        System.out.println(x);
        x = vm.checkCreditCard("MC","5222772000387510");
        System.out.println(x);
        x = vm.checkCreditCard("MC","5402198851297236");
        System.out.println(x);
        x = vm.checkCreditCard("MC","5467020001917030");
        System.out.println(x);
        x = vm.checkCreditCard("MC","5178052224627067");
        System.out.println(x);
        x = vm.checkCreditCard("AX","372846532484019");
        System.out.println(x);
        x = vm.checkCreditCard("AX","372841574231014");
        System.out.println(x);
        x = vm.checkCreditCard("AX","371727406932013");
        System.out.println(x);
        x = vm.checkCreditCard("AX","372817286622023");
        System.out.println(x);
        x = vm.checkCreditCard("AX","373985733695022");
        System.out.println(x);
        x = vm.checkCreditCard("AX","372525714371038");
        System.out.println(x);
        x = vm.checkCreditCard("AX","4323711138240308");
        System.out.println(x);
        x = vm.checkCreditCard("DC","4756214260000141");
        System.out.println(x);
        x = vm.checkCreditCard("CB","38947000990026");
        System.out.println(x);
*/

        // Pass
    }


/**
* Cendant Hotels validation
*
*/
  public boolean checkCendantHotels(String id)
  {
     // initialize variables for check digit routine
     String convertedToNumeric = "";
     int checkDigitIdInt = 0;
     int oddSum = 0;
     int evenSum = 0;
     int checkResult = 0;

     //Remove the spaces
     id = id.trim();

     //Validate length of the id
     if (id.length() != 10)
       return false;

     //convert alphanumerics to numerics
     for(int i=0; i < id.length(); i++) {
       String alphaNumeric = id.substring(i,i+1).toUpperCase();;
       try{
          Integer.parseInt(alphaNumeric);
         }catch(NumberFormatException e){
            //we have an alpha, convert it
            if(alphaNumeric.equals("A")) alphaNumeric = "1";
            if(alphaNumeric.equals("B") || alphaNumeric.equals("J")) alphaNumeric = "2";
            if(alphaNumeric.equals("C") || alphaNumeric.equals("K") || alphaNumeric.equals("R")) alphaNumeric = "3";
            if(alphaNumeric.equals("D") || alphaNumeric.equals("L") || alphaNumeric.equals("S") || alphaNumeric.equals("Y")) alphaNumeric = "4";
            if(alphaNumeric.equals("E") || alphaNumeric.equals("M") || alphaNumeric.equals("T") || alphaNumeric.equals("Z")) alphaNumeric = "5";
            if(alphaNumeric.equals("F") || alphaNumeric.equals("N") || alphaNumeric.equals("U")) alphaNumeric = "6";
            if(alphaNumeric.equals("G") || alphaNumeric.equals("O") || alphaNumeric.equals("V")) alphaNumeric = "7";
            if(alphaNumeric.equals("H") || alphaNumeric.equals("P") || alphaNumeric.equals("W")) alphaNumeric = "8";
            if(alphaNumeric.equals("I") || alphaNumeric.equals("Q") || alphaNumeric.equals("X")) alphaNumeric = "9";
         }
         convertedToNumeric = convertedToNumeric + alphaNumeric;
     }

     //add all odd numbered digits
     oddSum = sumOfEveryOtherDigitMultipliedByFactor(convertedToNumeric, 0, 1);

     //add all even numbered digits multiplied by 2 (last one is check digit, so skip that one)
     evenSum = sumOfEveryOtherDigitMultipliedByFactor(convertedToNumeric.substring(0, convertedToNumeric.length() -1 ), 1, 2);

      //add the two sums, get MOD 10
     checkResult = (oddSum + evenSum)%10;

     //subtract the reminder from 10
     checkResult =  10 - checkResult;

     //check the result against the chek digit (last one)
     if(checkResult == Character.getNumericValue(convertedToNumeric.charAt(convertedToNumeric.length()-1))) {
       return true;
     } else {
       return false;
     }

}

/**
* Ivan Vojinovic 09/11/2003
*
* this function adds either odd or even digits (depending on weather we start at 0 or 1)
* within the string containing an int... and also multiply those digits by the factor specified
* before adding them. If this multiplication results in double digit number, those digits are added too.
*
* this process ssems to be common for checkdigit routines
*/

  private int sumOfEveryOtherDigitMultipliedByFactor(String id, int startPosition, int factor) {
     int digitValue = 0;
     int totalValue = 0;
     String tempString = "";
      for(int i = startPosition; i < id.length() ; i=i+2)
      {
          tempString = id.substring(i,i+1);
          digitValue = Character.getNumericValue(tempString.charAt(0)) * factor;
          //if the result of multiplication is two digits long, add those two
          if ((digitValue + "").length() == 2)
              digitValue = Character.getNumericValue((digitValue + "").charAt(0)) +
                            Character.getNumericValue((digitValue + "").charAt(1)) ;
          totalValue += digitValue;
      }

      return totalValue;
  }
  
   /**
    * Virgin America validation
    *
    */
    public boolean checkVirginAmerica(String id)
    {
        String single;
        String convertCounter;
        String hashValue;
        int singleInt = 0;
        int total = 0;
        int hashInt = 0;
        int remainder = 0;
        int checkDigit = 0;
        int compare = 0;

        //Remove the spaces
        id.trim();

        //Validate the id is the valid length
        if (id.length() != 11)
            return false;

        //Validate the id is numeric
        try{
            Double.parseDouble(id);
        } catch (NumberFormatException e){
            return false;
        }

        //Create and populate the Hash Map. The map holds the digit and its multiplier.
        HashMap UnitedMap = new HashMap();
        UnitedMap.put("1","6");
        UnitedMap.put("2","5");
        UnitedMap.put("3","4");
        UnitedMap.put("4","3");
        UnitedMap.put("5","8");
        UnitedMap.put("6","7");
        UnitedMap.put("7","6");
        UnitedMap.put("8","5");
        UnitedMap.put("9","4");
        UnitedMap.put("10","3");

        //Loop throught the ID, get the number
        //Multiply the number by the value in the HashMap that is associated with
        //the number of times the loop has run and keep a running total
        for(int i = 1; i <= 10; i++)
        {
            //convert the counter int to a String
            convertCounter = Integer.toString(i);

            //Get the String value from the id and convert it to an int
            single = id.substring(i-1, i);
            singleInt = Character.getNumericValue(single.charAt(0));

            //Get the String value from the HashMap and convert it to an int
            hashValue = (String)UnitedMap.get(convertCounter);
            hashInt = Character.getNumericValue(hashValue.charAt(0));

            //calculate the result and add the number to the total
            total += (singleInt * hashInt);
        }

        //Get the remainder of the total divided by 11
        remainder = total % 11;

        //Set the value to compare to the checkDigit
        switch (remainder)
        {
            case 0: compare = 0;
            break;
            case 1: compare = 0;
            break;
            default: compare = 11-remainder;
        }

        //the checkDigit is the last digit of the id
        checkDigit = Character.getNumericValue(id.charAt(id.length() -1));

        //compare the numbers
        if(compare != checkDigit)
            return false;

        return true;    
    }
    
    /**
     * This method is to validate eleven digits virgin air numbers.
     * @param id
     * @return
     */
    public boolean checkElevenDigitsVirginAirNumber(String id)
    {
    	logger.debug("Virgin Airlines 11 digit membership number validation started ");
       // initialize variables for check digit routine
       String checkDigitId = "";
       int checkDigitIdInt;
       int multiplier = 0;
       int total = 0;
       int finTotal = 0;
       int checkTotal = 0;
       int finalTotal = 0;
       String Total = "";
       int digits = 0;
       String single = "";
       int singleInt = 0;

       //Remove the spaces
       id = id.trim();

       //Validate length of the id
       if (id.length() != 11)
         return false;

       //Validate the id is numeric
       try{
          Integer.parseInt(id);
         }catch(NumberFormatException e){
             return false;
         }

       //multiply odd positions by 1 and even digits by 2
       checkDigitId = id.substring(10);

       //Convert the strings to ints
       checkDigitIdInt = Character.getNumericValue(checkDigitId.charAt(0));

       //The Real Validation now starts....
       for(int i = 1; i <= 10; i++)
       {
         single = id.substring(i-1,i);
         singleInt = StringToInt(single);

         multiplier = i % 2;

         if (multiplier == 0) {
            digits = (singleInt * 2);
         }
         else {
            digits = (singleInt * 1);
         }
         total += digits;

      }  // end of loop

       //Round the total to the nearest ten ie. 68 would be 70
       // so get the remainder, if 0 do nothing, otherwise subtract the
       // remainder from 10 and add that number to the checktotal.
       // if the total is already rounded then the check digit will be 0.
       finTotal = (total % 10);
       if (finTotal != 0) {
          checkTotal += (total + (10 - finTotal));
          finalTotal = (checkTotal - total);
       }

       // Check digit vs. calculated check digit
       if (checkDigitIdInt != finalTotal) {
          return false;
       }

       return true;
    }
    /**
     * This method is to validate ten digit virginAirNumber.
     * @param id
     * @return boolean
     */
    public boolean checkTenDigitsVirginAirNumber(String id) {
    	logger.debug("Virgin Airlines 10 digit membership number validation started ");
    	// Initialize variables for check digit routine
    	int checkDigitIdInt=0;
    	int multiplier = 0;
    	int total = 0;
    	int caluculatedCheckDigit = 0;
    	int digits = 0;
    	int singleInt = 0;
    	try{
    		//Remove the spaces
    		id = id.trim();

    		//Rule1.Validate length of the id
    		//Rule2.Validate the id is 10 digits numeric or not.
    		Pattern digitPattern = Pattern.compile("\\d{10}");      
    		if(!digitPattern.matcher(id).matches()){
    			return false;
    		}

    		//Rule3.Multiply odd positions by 2 and even digits by 1
    		checkDigitIdInt = Integer.parseInt(id.substring(9));
    		for(int i = 1; i <= 9; i++){
    			singleInt = Integer.parseInt(id.substring(i-1,i).trim());
    			multiplier = i % 2;
    			//Odd position numbers
    			if (multiplier != 0){
    				digits = (singleInt * 2);
    				if(digits > 9){ // If digit is not single digit.
    					char[] digitsArray=Integer.toString(digits).toCharArray();
    					if(digitsArray != null && digitsArray.length >0){
    						int sumValue=0;
    						for(char currentDigit:digitsArray){//Array of digits
    							sumValue +=Integer.parseInt(Character.valueOf(currentDigit).toString());
    							digits=sumValue;
    						}
    					}
    				}
    			}else{//Even Position numbers
    				digits = (singleInt * 1);
    			}
    			total += digits;
    		}  // end of loop

    		//Now finding check digit based on algorithm.
    		//Divide the grand total by 10.
    		//1.If the remainder is 0 then check digit is 0.
    		//2.if the remainder is not zero then remainder amount is subtracted from the number 10.  (i.e 10-value= check digit).
    		if(total >0 && (total%10) >0){
    			caluculatedCheckDigit =10-(total%10);
    		}

    	}catch(Exception e){
    		logger.error("Error Occured While validating 10 digit virgin arilines membership number.");
    		return false;
    	}
    	// Comparing check digit with calculated check digit.
		if (checkDigitIdInt != caluculatedCheckDigit) {
			return false;
		}else{
			return true;  
		}
    }
    
}