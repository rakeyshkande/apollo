package com.ftd.ftdutilities;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import javax.net.ssl.SSLSocket;
import org.apache.commons.httpclient.protocol.SSLProtocolSocketFactory;

/**
 * 
 * @author kvasant
 *
 */
public class TLSv1SocketFactory extends SSLProtocolSocketFactory
{

	public TLSv1SocketFactory() {
		super();
	}

	public Socket createSocket(String host,
			int port,
			InetAddress clientHost,
			int clientPort)
	throws IOException,UnknownHostException{
		SSLSocket socket = (SSLSocket) super.createSocket(host, port, clientHost, clientPort);
		socket.setEnabledProtocols(new String[] {"TLSv1"});
		return socket;
	}

	public Socket createSocket(String host,
			int port)
	throws IOException,
	UnknownHostException {
		SSLSocket socket = (SSLSocket) super.createSocket(host, port);
		socket.setEnabledProtocols(new String[] {"TLSv1"});
		return socket;
	}

	public Socket createSocket(Socket socket,
			String host,
			int port,
			boolean autoClose)
	throws IOException,UnknownHostException {

		SSLSocket SSLsocket = (SSLSocket) super.createSocket(socket, host,port, autoClose);
		SSLsocket.setEnabledProtocols(new String[] {"TLSv1"});
		return socket;


	}

}
