package com.ftd.ftdutilities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("serial")
public class OEDeliveryDate implements Serializable {
	private String deliveryDate;
	private String holidayText;
	private String deliverableFlag;
	private String shippingAllowed;
	private String dayOfWeek;
	private List<ShippingMethod> shippingMethods; // Array of ShippingMethod objects
	private String displayDate;
	private boolean inDateRange;

	public OEDeliveryDate() {
		deliveryDate = "";
		holidayText = "";
		deliverableFlag = "N";
		dayOfWeek = "";
		shippingMethods = new ArrayList<ShippingMethod>();
		inDateRange = false;
	}

	public String getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(String newDeliveryDate) {
		deliveryDate = newDeliveryDate;
	}

	public String getHolidayText() {
		return holidayText;
	}

	public void setHolidayText(String newHolidayText) {
		holidayText = newHolidayText;
	}

	public String getDeliverableFlag() {
		return deliverableFlag;
	}

	public void setDeliverableFlag(String newDeliverableFlag) {
		deliverableFlag = newDeliverableFlag;
	}

	public String getDayOfWeek() {
		return dayOfWeek;
	}

	public void setDayOfWeek(String newDayOfWeek) {
		dayOfWeek = newDayOfWeek;
	}

	public String getDisplayDate() {
		return displayDate;
	}

	public void setDisplayDate(String newDisplayDate) {
		displayDate = newDisplayDate;
	}

	public List<ShippingMethod> getShippingMethods() {
		return shippingMethods;
	}

	public void setShippingMethods(List<ShippingMethod> newShippingMethods) {
		shippingMethods = newShippingMethods;
	}

	public String getShippingAllowed() {
		return shippingAllowed;
	}

	public void setShippingAllowed(String newShippingAllowed) {
		shippingAllowed = newShippingAllowed;
	}

	public boolean isInDateRange() {
		return inDateRange;
	}

	public void setInDateRange(boolean newInDateRange) {
		inDateRange = newInDateRange;
	}

}