package com.ftd.ftdutilities;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeoutException;

import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpException;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONObject;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.RESTUtils;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;
import com.ftd.osp.utilities.vo.CustomerCCandFSMDetails;
import com.ftd.osp.utilities.vo.CustomerFSHistory;
import com.ftd.osp.utilities.vo.CustomerFSMDetails;
import com.ftd.osp.utilities.vo.ErrorMessageVO;
import com.ftd.osp.utilities.vo.Errors;
import com.ftd.osp.utilities.vo.MembershipData;
import com.ftd.osp.utilities.vo.MembershipDetailsVO;
import com.ftd.osp.utilities.vo.MembershipResponse;
import com.ftd.osp.utilities.vo.ProfileErrorResponse;
import com.ftd.osp.utilities.vo.UpdateFSMResponse;

public class ProfileServiceUtils {
	
	private static Logger logger = new Logger(ProfileServiceUtils.class.getName());

	public static final String FREESHIPPING_MEMBER_TYPE = "FS";
	private static final String PROFILE_WS_SOCKET_TIMEOUT = "PROFILE_WS_SOCKET_TIMEOUT";
	private static final String PROFILE_TIMEOUT_ENABLE_FLAG = "PROFILE_TIMEOUT_ENABLE_FLAG"; 
	private static final String PROFILE_SERVICE_SECRET = "PROFILE_SERVICE_SECRET";
	private static final String PROFILE_SERVICE_ISSUER = "PROFILE_SERVICE_ISSUER";
	

	private static final String ERROR_NO_EXPIRED_EMAILS = "No active FSM customers found in this date range";
	private static final String SYSMSG_SUBJ_TIMEOUT = "Profile Service Timed out";
	private static final String SYSMSG_SUBJ = "Profile Service Exception";
	private static final String CREDIT_CARD_NOT_FOUND_ERROR = "PRFL.CREDIT_CARD_INFO_NOT_FOUND";
	
	private static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
	
	private static final int DEFAULT_TIME_TO_SUSPEND = 10000; 
	private static final String DEFAULT_PROFILE_SVC_URL = "https://prod-api-gateway.gcp.ftd.com/profile/ftd/customers/";
	private static final String DEFAULT_PROFILE_SVC_URL_PCI = "https://pciprod-api-gateway.gcp.ftdi.com/profile/ftd/customers/";



	/**
	 * @param emailList
	 * @param fsCutoffDate
	 * @return
	 */
	public static Map<String, MembershipDetailsVO> getMembershipData(List<String> emailList, Date fsCutoffDate) {
		Map<String, MembershipDetailsVO> membershipMap = null;
		try {
			membershipMap = getMembershipDataWithTimeoutException(emailList, fsCutoffDate);
		} catch (CustProfileTimeoutException timeoutEx) {
			logger.error("Profile Service Returned TimeoutException: " + timeoutEx.getMessage());
			membershipMap = null;
		} catch (Exception Ex) {
			logger.error("Exception in getMembershipData() : " + Ex.getMessage());
			membershipMap = null;
		} 
		return membershipMap;
	}

	/**
	 * @param emailList
	 * @param fsCutoffDate
	 * @param sendAlertOnTimeout
	 * @param orderNumber
	 * @return
	 */
	public static Map<String, MembershipDetailsVO> getMembershipData(List<String> emailList, Date fsCutoffDate,
			boolean sendAlertOnTimeout, String orderNumber) {
		Map<String, MembershipDetailsVO> membershipMap = null;
		try {
			membershipMap = getMembershipDataWithTimeoutException(emailList, fsCutoffDate);
		} catch (CustProfileTimeoutException timeoutEx) {
			logger.error("Service Returned Timeout Exception: " + timeoutEx.getMessage());
			if (sendAlertOnTimeout && !StringUtils.isEmpty(orderNumber)) {
				sendSystemMessage(SYSMSG_SUBJ_TIMEOUT +"for getMembershipData(), OrderNumber: "+ orderNumber, SYSMSG_SUBJ);
			}
			membershipMap = null;
		} catch (Exception Ex) {
			logger.error("Exception in getMembershipData() : " + Ex.getMessage());
			membershipMap = null;
		}
		return membershipMap;
	}


	/**
	 * @param emailList
	 * @param fsCutoffDate
	 * @return
	 */
	public static Map<String, MembershipDetailsVO> getMembershipDataWithTimeoutException(List<String> emailList, Date fsCutoffDate) {
		String email = ""; 
		String responseBody = null; 

		try {
			if (emailList == null || emailList.toString() == null || StringUtils.isEmpty(emailList.toString().trim()) || fsCutoffDate == null) {
				return null;
			}
			email = emailList.get(0);
			String url = (new StringBuilder()).append(getProfileServiceURL())
					.append("memberships/ByEmail?emailIdList=").append(email)
					.toString();
			 
			if (getProfileTimeoutEnabledFlag().equalsIgnoreCase("ON")) {
				responseBody = executeRequestWithThreadExecutor(url, HttpGet.METHOD_NAME, null); 
			} else {
				responseBody = executeRequest(url, HttpGet.METHOD_NAME, null); 
			}			
			
			if (!StringUtils.isEmpty(responseBody)) {
				return parseGetMembershipDataResponse(responseBody);
			}
			
			logger.error("Unable to process getMembershipDataWithTimeoutException request.");			
			
		} catch (CustProfileTimeoutException timeoutEx) {
			logger.error("Request to getMembershipData timedout: " + timeoutEx.getMessage());
			throw timeoutEx;
		} catch (Exception e) {
			String errorMsg= "Unable to process getMembershipData profile service request";
			logger.error(errorMsg , e);
			sendSystemMessage(errorMsg);
		} 
		return null;
	}
	

	/**
	 * @param URL
	 * @param methodType
	 * @param queryString
	 * @return
	 * @throws IOException
	 */
	private static HttpUriRequest initializeProfileService(String URL, String methodType, String queryString) throws IOException {
		logger.info("Profile Service URL " + URL);
		HttpUriRequest request = null;
		if (HttpGet.METHOD_NAME.equals(methodType)) {
			request = new HttpGet(URL);
		} else if (HttpPost.METHOD_NAME.equals(methodType)) {
			request = new HttpPost(URL);
			((HttpPost) request).setEntity(new StringEntity(queryString));
		} else if (HttpPut.METHOD_NAME.equals(methodType)) {
			request = new HttpPut(URL);
			((HttpPut) request).setEntity(new StringEntity(queryString));
		}
		
		String token = RESTUtils.computeSignature(request, RESTUtils.getConfigParamValue(PROFILE_SERVICE_SECRET, true), RESTUtils.getConfigParamValue(PROFILE_SERVICE_ISSUER, true));
		logger.info("Generated token for profile service :: "+token);
		
		request.addHeader("X-API-Version", "V1");
		request.addHeader(HttpHeaders.CONTENT_TYPE, RESTUtils.APPLICATION_JSON);
		request.addHeader(RESTUtils.API_AUTH_HEADER, "Bearer " +token ); 
		
		return request;
	}
	

	/**
	 * @param URL
	 * @param methodType
	 * @param queryString
	 * @return
	 * @throws CustProfileTimeoutException
	 * @throws Exception
	 */
	private static String executeRequest(String URL, String methodType, String queryString) throws CustProfileTimeoutException, Exception {
		HttpUriRequest httpRequest = initializeProfileService(URL, methodType, queryString);
		final HttpClient httpclient = new DefaultHttpClient();
		String responseBody = null;
		HttpResponse response = null;
		int statusCode = 0;
		try {
			HttpParams httpParams = httpclient.getParams();
			HttpConnectionParams.setConnectionTimeout(httpParams, getCustProfileResponseTimeout());
			HttpConnectionParams.setSoTimeout(httpParams, getCustProfileResponseTimeout());
			response = httpclient.execute(httpRequest);	
			statusCode = (response != null && response.getStatusLine() != null) ? response.getStatusLine().getStatusCode() : 0;
			logger.info("executeRequest:: Response Status Code: " + statusCode);
			 
			if(response != null && response.getEntity() != null) {
				responseBody =  EntityUtils.toString(response.getEntity());
			}
		} catch (SocketTimeoutException timeout) {
			throw new CustProfileTimeoutException("Exception caught calling Profile Service . Profile Service is taking more time to respond. Closing the connection. , request URL :"+URL);
		} catch (ConnectTimeoutException timeout) {
			throw new CustProfileTimeoutException("Profile Service timedout, request URL :"+URL);
		} catch (Exception ex) {
			throw ex;
		} finally {
			releaseClientConn(httpclient, httpRequest, response);
		}
		return responseBody;
	}
		

	/** To fix the issue with Locks accumulating, we are terminating the thread when request thread suspend time is reached.
	 * 
	 * @param URL
	 * @param methodType
	 * @param queryString
	 * @return
	 * @throws Exception
	 */
	private static String executeRequestWithThreadExecutor(final String URL, final String methodType, final String queryString) throws CustProfileTimeoutException, Exception {		
		String responseBody = null;    		
		java.util.concurrent.ExecutorService executorService = java.util.concurrent.Executors.newSingleThreadExecutor();		
		java.util.concurrent.Future<String> future = executorService
				.submit(new java.util.concurrent.Callable<String>() {					
					public String call() throws InterruptedException, IOException, HttpException {
						return execute(URL, methodType, queryString);
					}
				});

		try {
			responseBody = (String) future.get(getCustProfileResponseTimeout(), java.util.concurrent.TimeUnit.MILLISECONDS);			
		} catch (TimeoutException timeout) {
			logger.error(SYSMSG_SUBJ_TIMEOUT, timeout);
			throw new CustProfileTimeoutException(SYSMSG_SUBJ_TIMEOUT);
		} catch (Exception ex) {
			logger.error("Exception while calling Profile service", ex);
		} finally {
			future.cancel(true); 
		}
		if (StringUtils.isEmpty(responseBody)) {
			String msg = "Null response received from Profile service ";
			logger.error(msg);
			throw new CustProfileTimeoutException(msg);
		}
		return responseBody; 		
	}
	
	
	/**
	 * @param URL
	 * @param methodType
	 * @param queryString
	 * @return
	 * @throws IOException
	 */
	protected static String execute(String URL, String methodType, String queryString) throws IOException {
		HttpClient httpclient = null;		
		HttpUriRequest httpRequest = null;
		HttpResponse response = null;
		try {
			httpclient = new DefaultHttpClient(); 		
			httpRequest = initializeProfileService(URL, methodType, queryString);
			response = httpclient.execute(httpRequest);	
			int statusCode = (response != null && response.getStatusLine() != null) ? response.getStatusLine().getStatusCode() : 0;
			logger.info("executeRequestWithThreadExecutor:: Response Status Code: " + statusCode);
			 
			if(response != null && response.getEntity() != null) {
				return EntityUtils.toString(response.getEntity());
			}
		} catch (org.apache.http.ParseException e) { 
			throw e;
		} catch (IOException e) { 
			throw e;
		} finally { 
			releaseClientConn(httpclient, httpRequest, response);
		}
		return null;
	}

	/**
	 * @param method
	 * @return
	 * @throws Exception 
	 */
	private static Map<String, MembershipDetailsVO> parseGetMembershipDataResponse(String responseBody) throws Exception {
		Map<String, MembershipDetailsVO> membershipMap = null;
		SimpleDateFormat isoFormat = new SimpleDateFormat(DATE_FORMAT);
		Date formattedDate = null;
		MembershipDetailsVO memDetails = null;
		ObjectMapper mapper = new ObjectMapper();
		String key = null;

		try {
			MembershipResponse[] respArray = mapper.readValue(responseBody, MembershipResponse[].class);
			membershipMap = new HashMap<String, MembershipDetailsVO>();

			if (respArray != null && respArray.length > 0) {
				memDetails = new MembershipDetailsVO();
				for (MembershipResponse response : respArray) {
					MembershipData data = response.getData();
					if (data != null) {
						if(!StringUtils.isEmpty(data.getStartDate())){
							formattedDate = isoFormat.parse(data.getStartDate());
							memDetails.setStartDate(formattedDate);
						}
						
						if(!StringUtils.isEmpty(data.getJoinDate())){
							formattedDate = isoFormat.parse(data.getJoinDate());
							memDetails.setJoinDate(formattedDate);
						}

						if(!StringUtils.isEmpty(data.getExpireDate())){
							formattedDate = isoFormat.parse(data.getExpireDate());
							memDetails.setExpiryDate(formattedDate);
						}
						
						memDetails.setMembershipBenefitsApplicable(false);
						memDetails.setMembershipId(data.getMembershipId());

						key = data.getMembershipName();
						memDetails.setMembershipType(key);

						if ("Active".equalsIgnoreCase(data.getStatus())) {
							memDetails.setStatus("Y");
							memDetails.setMembershipBenefitsApplicable(true); // If the active FS member then membership benefits applicable
						} else {
							memDetails.setStatus("N");
						}
					} else {
						List<Errors> respErrors = response.getErrors(); // from Response
						List<ErrorMessageVO> errors = new ArrayList<ErrorMessageVO>();

						for (Errors respError : respErrors) {
							ErrorMessageVO error = new ErrorMessageVO();

							String errorCode = respError.getCode();
							String errorMessage = respError.getMessage();

							error.setErrorCode(errorCode);
							error.setErrorMessage(errorMessage);
							logger.error("ErrorCode:: " + errorCode + ", ErrorMessage : " + errorMessage);

							errors.add(error);
						}
						memDetails.setErrorMessages(errors);
					}
				}
			}
			membershipMap.put(key, memDetails);
		} catch (Exception e) {
			logger.error("Error caught parsing getMembershipData response ", e);
			return null;
		}  
		return membershipMap;
	}

	 
	/**
	 * @param emailId
	 * @return
	 * @throws Exception
	 */
	public static CustomerFSMDetails getFSMDetails(String emailId) throws Exception { 
		String responseBody = null;
		try {
			if (emailId == null || StringUtils.isEmpty(emailId)) {
				logger.error("Email Id cannot be null ");
				return null;
			}
			
			String URL = (new StringBuilder()).append(getProfileServiceURL()).append("memberships/ByEmail?emailIdList=").append(emailId).toString();
			 
			if (getProfileTimeoutEnabledFlag().equalsIgnoreCase("ON")) {
				responseBody = executeRequestWithThreadExecutor(URL, HttpGet.METHOD_NAME, null); 
			} else {
				responseBody = executeRequest(URL, HttpGet.METHOD_NAME, null); 
			} 
			
			if (!StringUtils.isEmpty(responseBody)) {
				return parseGetFSMDetailsResponse(responseBody);
			}
			
			throw new Exception("Unable to process getFSMDetails request.");
			
		} catch (CustProfileTimeoutException timeout) {
			throw new Exception("Request to getFSMDetails timedout, " + timeout.getMessage());
		} catch (Exception e) {
			String errorMsg = "Unable to process getFSMDetails profile service request";
			logger.error(errorMsg, e);
			sendSystemMessage(errorMsg + " " + e.getMessage());
		} 
		return null;
	}

	 
	/**
	 * @param responseBody
	 * @return
	 * @throws Exception
	 */
	private static CustomerFSMDetails parseGetFSMDetailsResponse(String responseBody) throws Exception {
		CustomerFSMDetails customerfsmDetails = null;
		ObjectMapper mapper = new ObjectMapper();
		SimpleDateFormat isoFormat = new SimpleDateFormat(DATE_FORMAT);
		Date formattedDate = null;

		try {
				if(checkAPIGatewayErrors(responseBody)){
					throw new CustProfileTimeoutException("Profile Service Timed Out, responeBody : "+responseBody);
				}
				MembershipResponse[] respArray = mapper.readValue(responseBody,
						MembershipResponse[].class);
				if (respArray != null && respArray.length > 0
						&& respArray[0] != null
						&& respArray[0].getData() != null) {
					MembershipData data = respArray[0].getData();
					if (data != null) {
						customerfsmDetails = new CustomerFSMDetails();
						
						if(!StringUtils.isEmpty(data.getStartDate())){
							formattedDate = isoFormat.parse(data.getStartDate());
							customerfsmDetails.setFsStartDate(formattedDate);
						}
						
						if(!StringUtils.isEmpty(data.getJoinDate())){
							formattedDate = isoFormat.parse(data.getJoinDate());
							customerfsmDetails.setFsJoinDate(formattedDate);
						}
						
						if(!StringUtils.isEmpty(data.getExpireDate())){
							formattedDate = isoFormat.parse(data.getExpireDate());
							customerfsmDetails.setFsEndDate(formattedDate);
						}
						if (data.getIsAutoRenew()) {
							customerfsmDetails.setFsAutoRenew("1");
						} else {
							customerfsmDetails.setFsAutoRenew("0");
						}

						if ("Active".equalsIgnoreCase(data.getStatus())) {
							customerfsmDetails.setFsMember("1");
						} else {
							customerfsmDetails.setFsMember("0");
						}

						customerfsmDetails.setFsmOrderNumber(data.getOrderNumber());
						customerfsmDetails.setFsTerm(Integer.parseInt(data.getTerm()));

					} else {
						List<Errors> respErrors = respArray[0].getErrors();
						for (Errors respError : respErrors) {
							String errorCode = respError.getCode();
							String errorMessage = respError.getMessage();

							logger.error("ErrorResponse received from Profile Service for Email "
									+ respArray[0].getMembershipEmail()
									+ " ErrorCode :: "
									+ errorCode
									+ ", ErrorMessage :: " + errorMessage);
							return null;
						}
					}
				}
		} catch (ParseException e) {
			String errorMsg = "Error caught parsing getFSMDetailsResponse";
			logger.error(errorMsg);
			throw e;
		} catch (Exception e) {			
			throw e;
		} 
		return customerfsmDetails;
	}
	 
	/**
	 * @param emailIdList
	 * @param eventTypeList
	 * @return
	 * @throws Exception
	 */
	public static List<CustomerFSHistory> getCustomerFSHistory(
			List<String> emailIdList, List<String> eventTypeList) throws Exception {  
		String profileServiceURL = getProfileServiceURL(); 
		String responseBody = null;
		
		try {
			String emailIds = getCommaSeperatedString(emailIdList);
			String eventTypes = getCommaSeperatedString(eventTypeList);
			String URL = (new StringBuilder()).append(profileServiceURL)
					.append("memberships/FSHistory?emailList=")
					.append(emailIds).append("&eventTypeList=")
					.append(eventTypes).toString();
		 
			if (getProfileTimeoutEnabledFlag().equalsIgnoreCase("ON")) {
				responseBody = executeRequestWithThreadExecutor(URL, HttpGet.METHOD_NAME, null); 
			} else {
				responseBody = executeRequest(URL, HttpGet.METHOD_NAME, null); 
			}	 
			
			if (!StringUtils.isEmpty(responseBody)) {
				return parseGetCustFSHistoryResponse(responseBody);
			}
			
			throw new Exception("Unable to process getCustomerFSHistory request.");
			
		} catch (CustProfileTimeoutException timeout) {
			throw new Exception("Request to getCustomerFSHistory timedout, " + timeout.getMessage());
		} catch (Exception e) {
			String errorMsg = "Error occurred while calling ProfileServiceUtils-->getCustomerFSHistory";
			logger.error(errorMsg);
			sendSystemMessage(errorMsg + " " + e.getMessage());
		} 		
		return null;
	}

	/**
	 * @param responseBody
	 * @return
	 * @throws Exception
	 */
	private static List<CustomerFSHistory> parseGetCustFSHistoryResponse(String responseBody) throws Exception {
		List<CustomerFSHistory> custFSHistoryList = new ArrayList<CustomerFSHistory>();
		ObjectMapper mapper = new ObjectMapper();
		SimpleDateFormat isoFormat = new SimpleDateFormat(DATE_FORMAT);
		Date formattedDate = null;
		CustomerFSHistory custFSHistory = null;
		MembershipData data = null;
		try {
			MembershipResponse[] respArray = mapper.readValue(responseBody,
					MembershipResponse[].class);

			if (respArray != null && respArray.length > 0) {
				custFSHistoryList = new ArrayList<CustomerFSHistory>();
				for (MembershipResponse response : respArray) {
					if (response != null && response.getData() != null) {
						data = response.getData();

						custFSHistory = new CustomerFSHistory();
						
						if(!StringUtils.isEmpty(data.getStartDate())){
							formattedDate = isoFormat.parse(data.getStartDate());
							custFSHistory.setFsStartDate(formattedDate);
						}
						if(!StringUtils.isEmpty(data.getExpireDate())){
							formattedDate = isoFormat.parse(data.getExpireDate());
							custFSHistory.setFsEndDate(formattedDate);
						}
						if(!StringUtils.isEmpty(data.getJoinDate())){
							formattedDate = isoFormat.parse(data.getJoinDate());
							custFSHistory.setFsJoinDate(formattedDate);
						}
						if (data.getIsAutoRenew()) {
							custFSHistory.setFsAutoRenew("1");
						} else {
							custFSHistory.setFsAutoRenew("0");
						}
						if ("Active".equalsIgnoreCase(data.getStatus())) {
							custFSHistory.setFsMember("1");
						} else {
							custFSHistory.setFsMember("0");
						}
						custFSHistory.setEmail(data.getEmail());
						custFSHistory.setFsmOrderNumber(data.getOrderNumber());
						custFSHistory
								.setFsTerm(Integer.parseInt(data.getTerm()));

						custFSHistoryList.add(custFSHistory);
					} else {
						List<Errors> respErrors = response.getErrors();

						for (Errors respError : respErrors) {
							String errorCode = respError.getCode();
							String errorMessage = respError.getMessage();

							logger.error("ErrorResponse received from Profile Service for Email "
									+ response.getMembershipEmail()
									+ " ErrorCode :: "
									+ errorCode
									+ ", ErrorMessage :: " + errorMessage);
							return null;
						}
					}
				}
			}
		} catch (Exception e) {
			String errorMsg = "Error caught parsing getCustomerFSHistory response";
			logger.error(errorMsg, e);
			throw e;
		} 
		return custFSHistoryList;
	}

	/**
	 * @param emailAddress
	 * @param externalOrderNumber
	 * @param membershipStatus
	 * @param autoRenewPeriod
	 * @param redeliveryFlag
	 * @param payLoad
	 * @return
	 * @throws Exception
	 */
	public static boolean updateProgramStatusInProfile(String emailAddress,
			String externalOrderNumber, String membershipStatus,
			int autoRenewPeriod, boolean redeliveryFlag, String payLoad)
			throws Exception {
	   
		boolean status = false;
		String fsMemberStatus = null; 
		String responseBody = null;

		try {
			String URL = (new StringBuilder()).append(getProfileServiceURL())
					.append(emailAddress).append("/memberships/FSMStatus")
					.toString();
			

			if (membershipStatus != null) {
				if (membershipStatus.equalsIgnoreCase("A")) {
					fsMemberStatus = "Active";
				} else if (membershipStatus.equalsIgnoreCase("I")) {
					fsMemberStatus = "Inactive";
				}
			}
			String queryString = getJsonAsString("updateProgramStatus",
					fsMemberStatus, externalOrderNumber, autoRenewPeriod, null,
					null, null, null, null, null);
			logger.info("updateProgramStatus QueryString: " + queryString);			
			 
			if (getProfileTimeoutEnabledFlag().equalsIgnoreCase("ON")) {
				responseBody = executeRequestWithThreadExecutor(URL, HttpPut.METHOD_NAME, queryString); 
			} else {
				responseBody = executeRequest(URL, HttpPut.METHOD_NAME, queryString); 
			}			 
			
			if (!StringUtils.isEmpty(responseBody)) {
				status = parseUpdateFSStatusResponse(responseBody, emailAddress, externalOrderNumber);
				logger.info("FS status updated for email " + emailAddress + ", " + status);
				return status;
			}
			 
			throw new Exception("Unable to process request updateProgramStatusInProfile.");
			
		} catch (CustProfileTimeoutException timeout) {
			logger.error("Request to updateProgramStatusInProfile timedout. " + timeout.getMessage(), timeout);
			throw timeout;
		} catch (Exception e) {
			String errorMsg = "Unable to process request updateProgramStatusInProfile.";
			logger.error(errorMsg, e);
			if (redeliveryFlag) {
				sendSystemMessage("Please use the payload to perform manual updates : " + payLoad + "  " + e.getMessage());
			}
			throw new RuntimeException(e.getMessage(), e);
		} 
	}
	
	/**
	 * @param responseBody
	 * @return
	 * @throws IOException 
	 * @throws JsonMappingException 
	 * @throws JsonParseException 
	 * @throws Exception
	 */
	private static boolean parseUpdateFSStatusResponse(String responseBody, String emailAddress, String orderNumber) throws Exception {
		boolean status = false;
		ObjectMapper mapper = new ObjectMapper();		 
		UpdateFSMResponse response = null;
		try {
			if(checkAPIGatewayErrors(responseBody)){
				throw new CustProfileTimeoutException("Profile Service Timed Out, responeBody : "+responseBody);
			}
			response = mapper.readValue(responseBody, UpdateFSMResponse.class);			 
		} catch (IOException e) {
			logger.error("Error caught unmarshalling the response, ", e);
		}
		
		if(response != null) {
			status = response.getSuccess() == null ? false : response.getSuccess();
			logger.debug("parseUpdateFSStatusResponse: " + response.toString());
		}  
		
		if (response != null && response.getErrorMap() != null) {
			Map<String, String> errors = response.getErrorMap();
			for (Map.Entry<String, String> entry : errors.entrySet()) {
				String errorMessage = "Error occurred while Updating FS Status. Error:: " + entry.getKey() + " : " + entry.getValue() + " EmailAddress: " + emailAddress + ", orderNumber: " + orderNumber;
				logger.error(errorMessage); 
				throw new RuntimeException("Error occured while updating free shipping duration "+ emailAddress + "--" + orderNumber);
			}			 
		} 		
		return status; 
	}


	/**
	 * @param methodName
	 * @param fsMemberStatus
	 * @param externalOrderNumber
	 * @param autoRenewPeriod
	 * @param status
	 * @param paymentType
	 * @param emailAddress
	 * @param expMonth
	 * @param expYr
	 * @param lastFourDigits
	 * @return
	 * @throws Exception
	 */
	private static String getJsonAsString(String methodName,
			String fsMemberStatus, String externalOrderNumber,
			int autoRenewPeriod, String status, String paymentType,
			String emailAddress, String expMonth, String expYr, String lastFourDigits) throws Exception {
		JSONObject jobj = null;
				
		try {
			logger.debug("Entering getJsonAsString() for " + methodName + " orderNumber: " + externalOrderNumber + ", status:"
					+ fsMemberStatus + ", membershipName:" + FREESHIPPING_MEMBER_TYPE + "autoRenewPeriod: "
					+ autoRenewPeriod + "paymentType: " + paymentType + ", emailAddress:" + emailAddress + ", result:" + status);
			
			if ("updateProgramStatus".equalsIgnoreCase(methodName)) {
				if (autoRenewPeriod != 0) {
					jobj = new JSONObject();
					logger.debug("autoRenewPeriod: " + autoRenewPeriod
							+ ", externalOrderNumber:" + externalOrderNumber);
					jobj.put("autoRenewPeriod", autoRenewPeriod);
					jobj.put("orderNumber", externalOrderNumber);
				} else {
					jobj = new JSONObject();
					logger.debug("orderNumber: " + externalOrderNumber
							+ ", status:" + fsMemberStatus
							+ ", membershipName:" + FREESHIPPING_MEMBER_TYPE);
					jobj.put("status", fsMemberStatus);
					jobj.put("orderNumber", externalOrderNumber);
					jobj.put("membershipName", FREESHIPPING_MEMBER_TYPE);
				}
			} else if ("sendCreditCardStatus".equalsIgnoreCase(methodName)) {
				Calendar cal = Calendar.getInstance();
				long epochDateTime = (cal.getTime().getTime()) / 1000;
				logger.debug("paymentType: " + paymentType + ", emailAddress:"
						+ emailAddress + ", result:" + status);
				jobj = new JSONObject();
				jobj.put("ccType", paymentType);
				jobj.put("email", emailAddress);
				jobj.put("result", status);
				jobj.put("transactionTime", String.valueOf(epochDateTime));
				jobj.put("expMonth", expMonth);
				jobj.put("expYear", expYr);
				jobj.put("card", lastFourDigits);
			}
			
			if(jobj != null) {
				return jobj.toString();
			}
			
		} catch (Exception e) {
			logger.error("Error occurred while converting to json String in method " + methodName);
			throw e;
		} 
		return null;
	}

	/**
	 * @param startDate
	 * @param endDate
	 * @return
	 * @throws Exception 
	 * @throws  
	 */
	public static List<String> getExpiredEmailsByExpireDate(Date startDate, Date endDate) throws Exception {  
		String responseBody = null;
		
		//try {
			String startDtStr = formatDate(startDate, "yyyy-MM-dd");
			String endDtStr = formatDate(endDate, "yyyy-MM-dd");
			logger.info("Fetching FS Expired Emails from Profile Service between dates: "
					+ startDtStr + " and " + endDtStr);

			String URL = (new StringBuilder())
					.append(getProfileServiceURL())
					.append("memberships/ExpiredFSCustomersByDateRange?startDate=")
					.append(startDtStr).append("&endDate=").append(endDtStr)
					.toString();
			 
			if (getProfileTimeoutEnabledFlag().equalsIgnoreCase("ON")) {
				responseBody = executeRequestWithThreadExecutor(URL, HttpGet.METHOD_NAME, null); 
			} else {
				responseBody = executeRequest(URL, HttpGet.METHOD_NAME, null); 
			} 

			if (!StringUtils.isEmpty(responseBody)) {
				return parseGetExpiredEmailsResponse(responseBody);
			}
			
			//throw new Exception("unable to process request getExpiredEmailsByExpireDate.");
			
		/*} catch (Exception e) {
			String errorMsg = "Unable to process request getExpiredEmailsByExpireDate";
			logger.error(errorMsg, e);
			sendSystemMessage(errorMsg + " " + e.getMessage());
		} */
		return null;
	}

	/**
	 * @param responseBody
	 * @return
	 * @throws Exception
	 */
	private static List<String> parseGetExpiredEmailsResponse(String responseBody) throws Exception {
		List<String> fsmExpEmailList = null;
		ObjectMapper mapper = new ObjectMapper(); 
		try { 
			//When request times out at API gateway retry the request
			if(responseBody.contains("API_GATWEWAY_ERRORS") && responseBody.contains("Read timed out")){
				throw new CustProfileTimeoutException("Profile Service Time Out Error, responeBody : "+responseBody);
			}
			//Error from Profile
			else if(responseBody.contains("errorMap")){
				ProfileErrorResponse errResp = mapper.readValue(responseBody, ProfileErrorResponse.class);
				if(errResp != null){
					Map<String, String> errorMap = errResp.getErrorMap();
					for (Map.Entry<String, String> entry : errorMap.entrySet()) {
						if (ERROR_NO_EXPIRED_EMAILS.equalsIgnoreCase(entry
								.getValue())) {
							logger.info(ERROR_NO_EXPIRED_EMAILS);
						} else {
							String errorMessage = "Error Response received while getting the FS Expired Emails. Error:: "
									+ entry.getKey() + " : " + entry.getValue();
							logger.error(errorMessage);
							sendSystemMessage(errorMessage);
						}
					}
				}
			} 
			//Valid response
			else if(!responseBody.contains("error")){
			String[] strArray = mapper.readValue(responseBody, String[].class);
				if (strArray != null & strArray.length > 0) {
					logger.info("Expired emails are : ");
					for (String email : strArray) {
						logger.debug(email);
					}
					fsmExpEmailList = new ArrayList<String>(Arrays.asList(strArray));
				}else{
					return null;
				}
			}
		} catch (Exception e) {
			logger.error("Error caught parsing getExpiredEmailsByExpireDate responseBody: "+responseBody, e);
			return null;
		}
		return fsmExpEmailList;
	}
 
	/**
	 * @param status
	 * @param paymentType
	 * @param emailAddress
	 * @param expirationMonth
	 * @param expirationYear
	 * @param lastFourDigits
	 * @return
	 */
	public static boolean sendCreditCardStatusToProfile(String status,
			String paymentType, String emailAddress, String expirationMonth, String expirationYear, String lastFourDigits) { 
		boolean respStatus = false; 	
		String responseBody = null;
		
		try {
			String URL = (new StringBuilder()).append(getProfileServiceURL()).append(emailAddress)
					.append("/memberships/FSMAutoRenewCCResult").toString();			
			String queryString = getJsonAsString("sendCreditCardStatus",
					null, null, 0, status, paymentType, emailAddress, expirationMonth, expirationYear, lastFourDigits);
			 
			if (getProfileTimeoutEnabledFlag().equalsIgnoreCase("ON")) {
				responseBody = executeRequestWithThreadExecutor(URL, HttpPost.METHOD_NAME, queryString); 
			} else {
				responseBody = executeRequest(URL, HttpPost.METHOD_NAME, queryString); 
			} 
			
			if (!StringUtils.isEmpty(responseBody)) {
				respStatus = parseSendCCUpdateResponse(responseBody);
				logger.info("Credit Card status sent for email " + emailAddress + ", " + status);
				return respStatus;
			}
			
			throw new Exception("Unabe to process request sendCreditCardStatusToProfile.");
		} catch (CustProfileTimeoutException timeout) {
			logger.error("Request to sendCreditCardStatusToProfile timedout. " + timeout.getMessage(), timeout);
			throw timeout;
		} catch (Exception e) {
			String errorMsg = "Unabe to process request sendCreditCardStatusToProfile";
			logger.error(errorMsg, e);
			sendSystemMessage(errorMsg);
		} 
		return respStatus;
	}

	/**
	 * @param responseBody
	 * @return
	 * @throws Exception
	 */
	private static boolean parseSendCCUpdateResponse(String responseBody) throws Exception {
		boolean status = false;
		ObjectMapper mapper = new ObjectMapper();
		try { 
			if(checkAPIGatewayErrors(responseBody)){
				throw new CustProfileTimeoutException("Profile Service Timed Out, responeBody : "+responseBody);
			}
			
			UpdateFSMResponse response = mapper.readValue(responseBody, UpdateFSMResponse.class); 
			logger.debug("Response Received : " + response.toString());
			
			if(response != null) {
				status = response.getSuccess() == null ? false : response.getSuccess();
			}  
			
			if (response != null && response.getErrorMap() != null) {
				Map<String, String> errors = response.getErrorMap();
				for (Map.Entry<String, String> entry : errors.entrySet()) {
					String errorMessage = "Error occurred while sending Credit Card Update status. Error:: " + entry.getKey() + " : " + entry.getValue();
					logger.error(errorMessage); 
				}
				sendSystemMessage("Profile sendCreditCardStatusToProfile method returned errors, look into account management logs for more details."); 
			} 
			
		} catch (Exception e) {
			status = false;
			logger.error("Error while parsing the Update Response for method ");
			throw e;
		}
		
		return status; 
	}

	/**
	 * @param emailId
	 * @return
	 */
	public static CustomerCCandFSMDetails getFSAccount(String emailId) throws Exception {
		CustomerCCandFSMDetails custDetails = null;  		 
		String responseBody = null;
		
		//try { 
			String URL = (new StringBuilder()).append(getProfileServicePCIURL())
					.append("memberships/CustomerFSMAndCCData?email=")
					.append(emailId).toString();
			 
			if (getProfileTimeoutEnabledFlag().equalsIgnoreCase("ON")) {
				responseBody = executeRequestWithThreadExecutor(URL, HttpGet.METHOD_NAME, null); 
			} else {
				responseBody = executeRequest(URL, HttpGet.METHOD_NAME, null); 
			} 
			 
			custDetails = parseCustCCandFSMResponse(responseBody, emailId);						
			if(custDetails != null && custDetails.getErrorMap() != null || custDetails.getException() != null) {
				Map<String, String> errorMap =  custDetails.getErrorMap();
				for(Map.Entry<String, String> entry : errorMap.entrySet()){
				   String errorMessage = "Error response received for getCustomerCCandFSMdetails. Error:: "+ entry.getKey() + " : "+entry.getValue();
				   if(CREDIT_CARD_NOT_FOUND_ERROR.equals(entry.getKey())) {
					   throw new CreditCardNotFoundException("Credit card not found in profile Service response :"+responseBody +" calling CAMS to get the CCdetails ");
				   }
				   logger.error(errorMessage);
				   ProfileServiceUtils.sendSystemMessage(errorMessage);
				   return null;
			     }
			 }
		/*} catch (Exception e) {
			String errorMsg = "Unable to process profile service request for getFSAccount";
			logger.error(errorMsg, e);
			sendSystemMessage(errorMsg + e);
		}*/ 
		return custDetails;
	}

	/**
	 * @param responseBody
	 * @param emailId
	 * @return
	 * @throws Exception
	 */
	private static CustomerCCandFSMDetails parseCustCCandFSMResponse(
		String responseBody, String emailId) throws Exception {
		CustomerCCandFSMDetails custDetails = null;
		ObjectMapper mapper = new ObjectMapper();
		try { 
			if(checkAPIGatewayErrors(responseBody)){
				throw new CustProfileTimeoutException("Profile Service Timed Out, responeBody : "+responseBody);
			}
			custDetails = mapper.readValue(responseBody, CustomerCCandFSMDetails.class);
		} catch (Exception e) {
			logger.error("Error occurred while parsing the response in parseCustCCandFSMResponse() method ");
			return null;
		}
		return custDetails;
	}


	/**
	 * @return
	 */
	private static String getProfileTimeoutEnabledFlag() {
		String profileTimeoutEnabledFlag = "OFF";
		try {
			profileTimeoutEnabledFlag = ConfigurationUtil.getInstance()
					.getFrpGlobalParm(RESTUtils.SERVICE_CONTEXT, PROFILE_TIMEOUT_ENABLE_FLAG);
			if (StringUtils.isBlank(profileTimeoutEnabledFlag))
				profileTimeoutEnabledFlag = "OFF";
		} catch (Exception e) {
			profileTimeoutEnabledFlag = "OFF";
			e.printStackTrace();
		}
		logger.info("ProfileServiceTimeoutEnabledFlag value = " + profileTimeoutEnabledFlag);
		return profileTimeoutEnabledFlag;
	}

	/**
	 * @param date
	 * @param dateFromat
	 * @return
	 */
	public static String formatDate(Date date, String dateFromat) {
		SimpleDateFormat sdf = new SimpleDateFormat(dateFromat);
		return sdf.format(date);
	}

	/**
	 * @param strList
	 * @return
	 */
	public static String getCommaSeperatedString(List<String> strList) {
		String strValues = null;
		for (String value : strList) {
			if (strValues == null) {
				strValues = value;
			} else {
				strValues = strValues + "," + value;
			}
		}
		if (strValues.endsWith(",")) {
			strValues = strValues.substring(0, strValues.length() - 1);
		}
		return strValues;
	}

	/**
	 * @return
	 */
	private static String getProfileServiceURL() {
		try {
			return ConfigurationUtil.getInstance().getFrpGlobalParm("SERVICE", "PROFILE_SERVICE_URL");
		} catch (Exception e) { 
			logger.error("Error caught reading Profile Service URL from global Parms: ", e);			
		}
		return DEFAULT_PROFILE_SVC_URL;
	}
	
	/**
	 * @return
	 */
	private static String getProfileServicePCIURL() {
		try {
			return ConfigurationUtil.getInstance().getFrpGlobalParm("SERVICE", "PROFILE_SERVICE_URL_PCI");
		} catch (Exception e) { 
			logger.error("Error caught reading Profile Service PCI URL from global Parms: ", e);			
		}
		return DEFAULT_PROFILE_SVC_URL_PCI;
	}
	
	public static void sendSystemMessage(String errorMessage) {
		sendSystemMessage(errorMessage, SYSMSG_SUBJ);
	}

	//Validate the Response for API gateway errors
	public static boolean checkAPIGatewayErrors(String responseBody){
		if(responseBody.contains("API_GATWEWAY_ERRORS") && responseBody.contains("Read timed out")){
			return true;
		}
		return false;
	}

	/**
	 * @param errorMessage
	 * @param subject
	 */
	public static void sendSystemMessage(String errorMessage, String subject) {
		Connection conn = null; 
		try {
			conn = DataSourceUtil.getInstance().getConnection("ORDER SCRUB");
			SystemMessengerVO sysMessage = new SystemMessengerVO();
			sysMessage.setLevel(SystemMessengerVO.LEVEL_PRODUCTION);
			sysMessage.setSource("Utilities_Profile_Service");
			sysMessage.setSubject(subject);
			sysMessage.setType("System Exception");
			sysMessage.setMessage(errorMessage);
			SystemMessenger.getInstance().send(sysMessage, conn);

		} catch (Throwable t) {
			logger.error("Sending system message failed. Message = "
					+ errorMessage);
			logger.error(t);
		} finally {
			if(conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					logger.error("Error caught clsoing cnnection" , e);
				}
			}
		}
	}
	
	/**
	 * @param httpclient
	 * @param httpRequest
	 * @param response
	 */
	@SuppressWarnings("deprecation")
	private static void releaseClientConn(HttpClient httpclient, HttpUriRequest httpRequest, HttpResponse response) {
		try {
			if (httpclient != null) {
				httpclient.getConnectionManager().shutdown();
			}
		} catch (Exception e) {
			logger.error("Error closing http client connection, " + e);
		}
		
		try {
			if (httpRequest != null && !httpRequest.isAborted()) {
				httpRequest.abort();
			}
		} catch (Exception e) {
			logger.error("Error closing http URI Request, " + e);
		} 
		
		try {
			if(response != null && response.getEntity() != null) {			
				response.getEntity().consumeContent();
			}
		} catch (IOException e) {
				logger.error("Error closing http entity and all streams, " + e);
		}
	}
	
	
	/**
	 * @return
	 */
	public static int getCustProfileResponseTimeout() { 
		try {
			String socketTimeout = ConfigurationUtil.getInstance().getFrpGlobalParm(RESTUtils.SERVICE_CONTEXT, PROFILE_WS_SOCKET_TIMEOUT);
			logger.info("PROFILE_WS_SOCKET_TIMEOUT is " + socketTimeout);
			return Integer.parseInt(socketTimeout);			
		} catch (Exception e) {
			logger.error("Invalid response/socketTimeout, read from global params. Setting to default value", e); 
		}
		return DEFAULT_TIME_TO_SUSPEND;
	}


}
