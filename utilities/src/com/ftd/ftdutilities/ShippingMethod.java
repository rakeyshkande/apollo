package com.ftd.ftdutilities;

import java.math.BigDecimal;
import java.io.Serializable;

public class ShippingMethod implements Serializable
{
    private String code;
    private String description;
    private BigDecimal deliveryCharge;

    public ShippingMethod()
    {
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String newCode)
    {
        code = newCode;
    }

    public BigDecimal getDeliveryCharge()
    {
        return deliveryCharge;
    }

    public void setDeliveryCharge(BigDecimal newDeliveryCharge)
    {
        deliveryCharge = newDeliveryCharge;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String newDescription)
    {
        description = newDescription;
    }

    public boolean equals(Object obj)
    {
        boolean ret = false;
        ShippingMethod method = (ShippingMethod)obj;
        if(this.getCode().equals(method.getCode()))
        {
            ret = true;
        }
        return ret;
    }

}

    class ShippingMethodComparator implements java.util.Comparator
    {
    /**
     *  The compare method compares the ShippingMethod objects based on code
     *
     *  @param o1   The first ShippingMethod
     *  @param o2   The second ShippingMethod
     *  @return int negative if the first ShippingMethod orderby comes before the second's
     *              positive if the first ShippingMethod orderby comes after the second's
     *              zero if the ShippingMethodVO have the same type
     */
        public int compare(Object o1, Object o2)
        {
            int ret = 0;
            try
            {
                ShippingMethod first = (ShippingMethod) o1;
                ShippingMethod second = (ShippingMethod) o2;

                int firstOrder = 0;
                int secondOrder = 0;

                if(first.getCode().equals(GeneralConstants.DELIVERY_STANDARD_CODE))
                {
                    firstOrder = 1;
                }
                else if(first.getCode().equals(GeneralConstants.DELIVERY_TWO_DAY_CODE))
                {
                    firstOrder = 2;
                }
                else if(first.getCode().equals(GeneralConstants.DELIVERY_NEXT_DAY_CODE))
                {
                    firstOrder = 3;
                }
                else if(first.getCode().equals(GeneralConstants.DELIVERY_SATURDAY_CODE))
                {
                    firstOrder = 4;
                }
                else
                {
                    firstOrder = 5;
                }

                if(second.getCode().equals(GeneralConstants.DELIVERY_STANDARD_CODE))
                {
                    secondOrder = 1;
                }
                else if(second.getCode().equals(GeneralConstants.DELIVERY_TWO_DAY_CODE))
                {
                    secondOrder = 2;
                }
                else if(second.getCode().equals(GeneralConstants.DELIVERY_NEXT_DAY_CODE))
                {
                    secondOrder = 3;
                }
                else if(second.getCode().equals(GeneralConstants.DELIVERY_SATURDAY_CODE))
                {
                    secondOrder = 4;
                }
                else
                {
                    secondOrder = 5;
                }

                ret = new Integer(firstOrder).compareTo(new Integer(secondOrder));
            }
            catch(Exception e)
            {}

            return ret;
        }

        /**
        * Insert the method's description here.
        * @return boolean
        * @param obj java.lang.Object
        */
        public boolean equals(Object obj)
        {
            return obj instanceof ShippingMethodComparator;
        }
    }