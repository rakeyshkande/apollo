package com.ftd.ftdutilities;

import java.sql.Connection;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import com.ftd.osp.utilities.order.ScrubMapperDAO;
import com.ftd.osp.utilities.order.vo.SympathyBusinessHours;
import com.ftd.osp.utilities.order.vo.SympathyItemsVO;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * 
 * @author rsalgut
 *
 */
public class SympathyLeadTimeCalculator {
	
	static Logger logger = new Logger("com.ftd.ftdutilities.SympathyCalculateLeadTime");

	/**
	 *  
	 *  
	 * @param deliveryDate
	 * @param deliveryTimeInHours
	 * @param state
	 * @param mp
	 * @param leadTimeGlobal
	 * @param con
	 * @return
	 * @throws Exception
	 */
	public static boolean checkLeadTime(Connection con, SympathyItemsVO itemVO, SympathyBusinessHours symBH, int leadTimeGlobal) throws Exception {
		logger.debug("***********inside checkLeadTime************");
		logger.debug("The global lead time is: "+ leadTimeGlobal);
		int leadTime = 0;
		int openingHoursDay;
		int closingHoursDay;
		int deliveryTimeInHours = itemVO.getDeliveryTime();
		Calendar currentDay = Calendar.getInstance();
		Calendar deliveryDateCal = Calendar.getInstance();
		if (itemVO.getDeliveryDate() != null) {
			deliveryDateCal.setTime(itemVO.getDeliveryDate());
		} else {
			deliveryDateCal = currentDay;
		}
		//Converting the order time into recipient time zone
		int orderTimeInRecipientState = convertDateToRecipientState(con, itemVO.getShipState(),(currentDay.get(Calendar.HOUR_OF_DAY)*100+currentDay.get(Calendar.MINUTE)));
		//If the time of the order is less the business opening hours - set the order time to opening hours
		if (orderTimeInRecipientState < getOpeningHoursByDay(currentDay.get(Calendar.DAY_OF_WEEK), symBH)) {
			orderTimeInRecipientState = getOpeningHoursByDay(currentDay.get(Calendar.DAY_OF_WEEK), symBH);
		}
		//If the time of the order is greater than the business closing hours - set the order time to opening hours of the next day - also increment the day
		if (orderTimeInRecipientState > getClosingHoursByDay(currentDay.get(Calendar.DAY_OF_WEEK), symBH)) {
			orderTimeInRecipientState = getOpeningHoursByDay(currentDay.get(Calendar.DAY_OF_WEEK + 1), symBH);
			currentDay.add(Calendar.DATE, 1);
		}

		closingHoursDay = getClosingHoursByDay(currentDay.get(Calendar.DAY_OF_WEEK), symBH);
		int dayDifference = getDayDifference(deliveryDateCal,currentDay);
		logger.debug("The day difference is: " + dayDifference);
		// For same day
		if (dayDifference == 0) {
			leadTime = (deliveryTimeInHours - orderTimeInRecipientState) / 100;
			if (deliveryTimeInHours > closingHoursDay) {
				return false;
			}
		}
		// If the delivery date is next day or after that
		else {
			Calendar dayIncrementer = currentDay;
			for (int i = 0; i <= dayDifference; i++) {

				openingHoursDay = getOpeningHoursByDay(dayIncrementer.get(Calendar.DAY_OF_WEEK), symBH);
				closingHoursDay = getClosingHoursByDay(dayIncrementer.get(Calendar.DAY_OF_WEEK), symBH);

				if (i == 0) {
					leadTime = (closingHoursDay - orderTimeInRecipientState)/100;
				} else if (i == dayDifference){
					leadTime = leadTime + (deliveryTimeInHours - openingHoursDay)/100;
				} else {
					leadTime = leadTime + (closingHoursDay - openingHoursDay)/100;
				}
				dayIncrementer.add(Calendar.DATE, 1);
			}
		}
		logger.info("The lead time is: "+leadTime);
		if (leadTime >= leadTimeGlobal) {
			logger.debug("The lead time check is true");
			return true;
		} else {
			logger.debug("The lead time is false");
			return false;
		}
	}
	
	/**
	 * 
	 * @param dayOfTheWeek
	 * @param mp
	 * @return openingHours
	 */
	private static int getOpeningHoursByDay(int dayOfTheWeek, SympathyBusinessHours symBH) {
		int openingHours = 900;
		if (symBH!=null) {
			if (dayOfTheWeek == 7) {
				// get Saturday related global parms
				openingHours = (Integer) coverStringTimetoInt((String) symBH.getBoHrsSatStart());
			} else if (dayOfTheWeek == 1) {
				// get Sunday related global parms
				openingHours = (Integer) coverStringTimetoInt((String) symBH.getBoHrsSunStart());
			} else {
				// get M-F related global parm
				openingHours = (Integer) coverStringTimetoInt((String) symBH.getBoHrsMonFriStart());
			}
		}
		return openingHours;
	}
	
	/**
	 * 
	 * @param dayOfTheWeek
	 * @param mp
	 * @return closingHours
	 */
	private static int getClosingHoursByDay(int dayOfTheWeek, SympathyBusinessHours symBH) {
		int closingHours = 300;
		if (symBH!=null) {
			if (dayOfTheWeek == 7) {
				// get Saturday related global parms
				closingHours = (Integer) coverStringTimetoInt((String) symBH.getBoHrsSatEnd());
			} else if (dayOfTheWeek == 1) {
				// get Sunday related global parms
				closingHours = (Integer) coverStringTimetoInt((String) symBH.getBoHrsSunEnd());
			} else {
				// get M-F related global parm
				closingHours = (Integer) coverStringTimetoInt((String) symBH.getBoHrsMonFriEnd());
			}
		}
		return closingHours;
	}
	
	/**
	 * 
	 * @param date
	 * @return 
	 */
	
	private static int coverStringTimetoInt(String date) {
		if(date!=null) {
			String[] str = date.split(":");
			int hours = Integer.valueOf(str[0]);
			int mins = Integer.valueOf(str[1])*100/60;
			int time = (hours * 100) + mins;
			return time;
		}
		return 0;
	}
	/**
	 * 
	 * @param con
	 * @param state
	 * @param time
	 * @return
	 * @throws Exception
	 */
	private static int convertDateToRecipientState(Connection con, String state, int time) throws Exception {
	
        ScrubMapperDAO dao = new ScrubMapperDAO(con);
        int newTime = dao.getTimeInRecState(state, time);
		logger.info("The order time at recipient time zone is: "+ newTime);
        return newTime;
	}
	
	/**
	 * 
	 * @param dayTo
	 * @param dayFrom
	 * @return
	 */
	private static int getDayDifference(Calendar dayTo, Calendar dayFrom) {
		long timeDiffinMillis = dayTo.getTimeInMillis() - dayFrom.getTimeInMillis();
		long days = timeDiffinMillis / (86400 * 1000);
		long remainingSec = timeDiffinMillis % (86400 * 1000);
		if (remainingSec >= (86400 - (dayFrom.get(Calendar.HOUR_OF_DAY)*60*60 + dayFrom.get(Calendar.MINUTE)*60 + dayFrom.get(Calendar.SECOND)))) {
			days++;
		}
		return (int) days;
	}
	
}
