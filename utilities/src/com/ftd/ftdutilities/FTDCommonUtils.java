package com.ftd.ftdutilities;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.lang.StringUtils;
import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.ftd.ftdutilities.JmsQueueVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.constant.CacheMgrConstants;
import com.ftd.osp.utilities.cacheMgr.handlers.PartnerHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.PaymentBinMasterHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.PartnerVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.order.vo.ApValidationVO;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.order.vo.PaymentMethodMilesPointsVO;
import com.ftd.osp.utilities.order.vo.PaymentMethodVO;
import com.ftd.osp.utilities.order.vo.PaymentsVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;
import com.ftd.osp.utilities.xml.JAXPUtil;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.AccountProgramMasterVO;

public class FTDCommonUtils {
    private static Logger logger = new Logger("com.ftd.ftdutilities.FTDCommonUtils");
    private static final String SCALE_MULTIPLY = "*";  // Scale represents miles per dollar
    private static final String SCALE_DIVIDE   = "/";  // Scale represents dollars per mile

    private static final String PERSONALIZATION_TAG_DATA = "data";
    private static final String PERSONALIZATION_TAG_NAME = "name";
    private static final String PERSONALIZATION_TAG_ROOT = "personalization";
    
    // PDP style personalization XML tags
    //
    private static final String PDP_PERSONALIZATION_TAG_ROOT = "data";
    private static final String PDP_PERSONALIZATION_ITEMS    = "PersonalizationItems";
    private static final String PDP_PERSONALIZATION_NAME     = "DisplayName";  
    private static final String PDP_PERSONALIZATION_VALUE    = "Value";  
    private static final String PDP_PERSONALIZATION_SORT     = "SortOrder";  
    private static final String PDP_ACCESSORY_ID             = "AccessoryId";  

    
    public FTDCommonUtils(){}
    
    /*
     * Retrieves over authrized percent for the payment method id.
     * Returns over_auth_allowed_pct from ftd_apps.payment_mehtods
     */
    public static PaymentMethodVO getPaymentMethodById(String paymentMethodId, Connection connection) throws Exception
    {
        BigDecimal value = null;  
        Map paramMap = new HashMap();
        PaymentMethodVO pm = null;

        paramMap.put("payment_method_id", paymentMethodId);
        
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("GET_PAYMENT_METHOD_BY_ID_SET");
        dataRequest.setInputParams(paramMap);
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        CachedResultSet rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);
        
        if(rs != null && rs.next()) {
            pm = new PaymentMethodVO();
            pm.setPaymentMethodId(rs.getString("PAYMENT_METHOD_ID"));
            pm.setPaymentMethodType(rs.getString("PAYMENT_TYPE"));
            pm.setPaymentTypeDescription(rs.getString("DESCRIPTION"));
            pm.setOverAuthPct(rs.getBigDecimal("OVER_AUTH_ALLOWED_PCT") == null? new BigDecimal("0.00") : rs.getBigDecimal("OVER_AUTH_ALLOWED_PCT"));
            pm.setOverAuthAmt(rs.getBigDecimal("OVER_AUTH_ALLOWED_AMT") == null? new BigDecimal("0.00") : rs.getBigDecimal("OVER_AUTH_ALLOWED_AMT"));
        }
        
        return pm;  
    }//end method getPaymentMethodById  
    
     public static ApValidationVO getApValidation(String paymentMethodId, Connection connection) throws Exception
     {
         DataRequest dataRequest = new DataRequest();
         Map paramMap = new HashMap();
         paramMap.put("payment_method_id", paymentMethodId);
         
         dataRequest.setConnection(connection);
         dataRequest.setStatementID("VIEW_AP_VALIDATION_PROPERTIES");
         dataRequest.setInputParams(paramMap);
         ApValidationVO apv = new ApValidationVO();
         apv.setPaymentMethodId(paymentMethodId);
         
         DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
         CachedResultSet rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);
         String pName = null;
         String pValue = null;
         
         while(rs != null && rs.next()) {
             pName = rs.getString("PROPERTY_NAME");;
             pValue = rs.getString("PROPERTY_VALUE");
             apv.addValidationProperty(pName, pValue);
         }
         
         return apv;  
     }//end method getApValidation
     
     
    /**
     * Tests if an order has P payment method type that is over original authirized amount.
     * @param order
     * @return
     * @throws Exception
     */
    public static boolean isOrderOverAllowedAuth(OrderVO order, Connection connection) throws Exception
    {
        Collection payments = order.getPayments();
        Iterator it = payments.iterator();
        boolean isOverAuth = false;
        String paymentType = null;
        
        if(payments != null && payments.size() > 0)
        {
            it = payments.iterator();
            BigDecimal paymentAmt = null;
            BigDecimal origAuthAmt = null;
            BigDecimal allowedPctOverAuth = null;
            BigDecimal allowedAmtOverAuth = null;
            BigDecimal allowedAmt = null; // allowed amt calcumated from allowed pct
            PaymentsVO payment = null;
            String origAuthAmtTxt = null;
            String paymentAmtTxt = null;
            PaymentMethodVO pm = null;

            while(it.hasNext())
            {
                payment = (PaymentsVO)it.next();
                pm = getPaymentMethodById(payment.getPaymentType(), connection);
                //Added this condition to not to break this loop if the payment type is GC or GD
                if (pm != null && pm.getPaymentMethodId()!= null && !pm.getPaymentMethodId().equalsIgnoreCase("GC") && !pm.getPaymentMethodId().equalsIgnoreCase("GD") 
                		&& (pm.getOverAuthPct() == null || (pm.getOverAuthPct().compareTo(new BigDecimal(0.0)) == 0)) && 
                     (pm.getOverAuthAmt() == null || (pm.getOverAuthAmt().compareTo(new BigDecimal(0.0)) == 0))) {
                    break;  // Just get out if neither over-auth values are set 
                }
                
                if(payment!= null && (payment.getPaymentMethodType().equals(GeneralConstants.PAYMENT_METHOD_TYPE_AP))) {
                    paymentType = payment.getPaymentType();
                    origAuthAmtTxt = payment.getOrigAuthAmount();
                    paymentAmtTxt = payment.getAmount();
                    if (origAuthAmtTxt == null || origAuthAmtTxt.equals("")) {
                        origAuthAmtTxt = "0";
                    }
                    
                    if (paymentAmtTxt == null || paymentAmtTxt.equals("")) {
                        paymentAmtTxt = "0";
                    }
                    
                    paymentAmt = new BigDecimal(paymentAmtTxt);
                    paymentAmt = paymentAmt.setScale(2, RoundingMode.HALF_UP);
                    origAuthAmt = new BigDecimal(origAuthAmtTxt);
                    origAuthAmt = origAuthAmt.setScale(2, RoundingMode.HALF_UP);
                    allowedPctOverAuth = pm.getOverAuthPct();
                    allowedPctOverAuth = allowedPctOverAuth.setScale(2, RoundingMode.HALF_UP);
                    allowedAmt = new BigDecimal(origAuthAmt.doubleValue() + origAuthAmt.doubleValue() * allowedPctOverAuth.doubleValue() * 0.01);
                    allowedAmt = allowedAmt.setScale(2, RoundingMode.HALF_UP);
                    
                    allowedAmtOverAuth = origAuthAmt.add(pm.getOverAuthAmt());
                    
                    // Order is over auth if it's greater than allowed over auth percent 
                    // and greater than allowed over auth amount
                    if (paymentAmt.compareTo(allowedAmt) == 1 && paymentAmt.compareTo(allowedAmtOverAuth) == 1) {
                        isOverAuth = true;
                        break;
                    }
                } 
            }
        }
        
        return isOverAuth;
    }

    /*
     * Retrieves miles/points payment method information
     */
    public static PaymentMethodMilesPointsVO getPaymentMethodMilesPoints(String paymentMethodId, Connection connection) throws Exception
    {
        Map paramMap = new HashMap();
        PaymentMethodMilesPointsVO pm = null;

        paramMap.put("IN_PAYMENT_METHOD_ID", paymentMethodId);
        
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("GET_PAYMENT_METHOD_MP");
        dataRequest.setInputParams(paramMap);
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        CachedResultSet rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);
        
        if(rs != null && rs.next()) {
            pm = new PaymentMethodMilesPointsVO();
            pm.setPaymentMethodId(rs.getString("PAYMENT_METHOD_ID"));
            pm.setDollarToMpOperator(rs.getString("DOLLAR_TO_MP_OPERATOR"));
            pm.setRoundingMethodId(rs.getString("ROUNDING_METHOD_ID"));
            pm.setRoundingScaleQty(rs.getString("ROUNDING_SCALE_QTY"));
            pm.setCommissionPct(rs.getString("COMMISSION_PCT"));
        }
        
        return pm;  
    }//end method getPaymentMethodMilesPoints


    /**
    * Convert dollar amount to miles/points value based on conversion and rounding info
    * 
    * @param dollars - Dollar amount to convert
    * @param redemptionRate - Rate for converting dollars to miles/points
    * @param scaleOperator - Defines what redemptionRate represents (dollars/mile or miles/dollar)
    * @param roundingScale - Conversion will be rounded to nearest multiple of this value
    * @param roundingMode - Rounding method (half_up, half_down, etc)
    * @return Miles/points 
    * @exception Exception
    */
    public static int convertDollarsToMilesPoints(String dollars, String redemptionRate, String scaleOperator, 
                                            String roundingScale, String roundingMode) throws Exception {
        boolean scaleMultiply = false;
        boolean isNegative = false;        
        RoundingMode rMode = null;
        int returnMiles = 0;
        int rScale = Integer.parseInt(roundingScale);
        
        // Change roundingMode string to proper RoundingMode enum value
        //
        if ("HALF_UP".equals(roundingMode)) {
            rMode = RoundingMode.HALF_UP;
        } else if ("HALF_DOWN".equals(roundingMode)) {
            rMode = RoundingMode.HALF_DOWN;
        } else if ("UP".equals(roundingMode)) {
            rMode = RoundingMode.UP;
        } else if ("DOWN".equals(roundingMode)) {
            rMode = RoundingMode.DOWN;
        } else {
            throw new Exception("Defined Miles/Points rounding mode '" + roundingMode + "' not recognized");
        }

        // The redemption rate will either represent dollars per mile or miles per dollar.
        // The scaleOperator defines this (SCALE_DIVIDE for former, SCALE_MULTIPLY for latter).
        //
        if (SCALE_DIVIDE.equals(scaleOperator)) {
            scaleMultiply = false;
        } else if (SCALE_MULTIPLY.equals(scaleOperator)) {
            scaleMultiply = true;
        } else {
            throw new Exception("Defined Miles/Points scale operator '" + scaleOperator + "' not recognized");
        }
        
        // Convert dollars to miles/points
        //
        BigDecimal bigDollarAmt = new BigDecimal(dollars);
        if (bigDollarAmt.signum() < 0) {
           bigDollarAmt = bigDollarAmt.abs();   // Use absolute value for simplicity
           isNegative = true;                   // Remember value was negative
        }
        BigDecimal bigRedemptionRate = new BigDecimal(redemptionRate);
        BigDecimal bigMilesPoints;
        
        if (scaleMultiply) {
            // Scale represents miles per dollar 
            // (Note using MathContext just to prevent Non-terminating exceptions)
            bigMilesPoints = bigDollarAmt.multiply(bigRedemptionRate, new MathContext(50));
        } else {
            // Scale represents dollars per mile
            bigMilesPoints = bigDollarAmt.divide(bigRedemptionRate, new MathContext(50));
        }
        bigMilesPoints = bigMilesPoints.setScale(0, rMode);  // Round to nearest whole value
        int milesPoints = bigMilesPoints.intValueExact();

        // Round to specified number of miles/points (roundingScale)
        //
        int remainder = milesPoints % rScale;
        
        if (remainder == 0) {
            returnMiles = milesPoints;
        } else {
            switch(rMode) {
                case HALF_UP:
                    returnMiles = milesPoints - remainder;
                    if ((remainder *2) >= rScale) {
                        returnMiles += rScale;
                    }
                    break;
                case HALF_DOWN:
                    returnMiles = milesPoints - remainder;
                    if ((remainder *2) > rScale) {
                        returnMiles += rScale;
                    }
                    break;
                case UP:
                    returnMiles = (milesPoints - remainder) + rScale;
                    break;
                case DOWN:
                    returnMiles = milesPoints - remainder;
                    break;
                default:
                    returnMiles = 0;
                    break;
            }
        }
        if (isNegative && returnMiles != 0) {  // Restore negativity if necessary
            returnMiles = -returnMiles;
        }

        logger.info("Converted $" + dollars + " to " + returnMiles + " miles/points" +
                     " using: " + redemptionRate + "," + scaleOperator + "," + 
                     roundingScale + "," + roundingMode + " (rate,operator,scale,mode)");
        return returnMiles;
    }
    
    /**
    * Determine if the payment method id is miles/points.
    * 
    * @param paymentMethodId - Payment method id
    * @return boolean - True if the payment method id is miles points. 
    * @exception Exception
    */
    public static boolean isPaymentMilesPoints(String paymentMethodId) throws Exception {
        if(paymentMethodId != null && paymentMethodId.equals(GeneralConstants.PAYMENT_METHOD_TYPE_UA))
            return true;
        else
            return false;
    }

    /**
    * Enhancement for luxury item handling. Send back a list of product properties.
    * Each key is a column on ftd_apps.product_master. This is hardcoded
    * as we cannot forsee how the list will grow.
    * 
    * @exception Exception
    */
     public static HashMap getPriorityProductProperties() throws Exception {
        HashMap props = new HashMap();
        props.put("PREMIER_COLLECTION_FLAG", "Premier Collection");
        return props;
        
     }
     
     /**
     * The map returns all preferred partners in a Map. If no preferred partner exists, returns null.
     * key: partner_name
     * value: com.ftd.osp.utilities.cache.handlers.vo.PartnerVO
     * @return
     * @throws Exception
     */
     public static Map getPreferredPartner() throws Exception {
        PartnerHandler ph = (PartnerHandler)CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_PARTNER_HANDLER);
        if(ph != null) {
            return ph.getPreferredPartner();
        }
        return null;
     }
     
    /**
     * Returns the preferred partner name by source. Return null if the source code is not that of
     * a preferred partner.
     * @param sourceCode
     * @return
     */
    public static PartnerVO getPreferredPartnerBySource(String sourceCode)
    {
        PartnerHandler ph = (PartnerHandler)CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_PARTNER_HANDLER);
        if(sourceCode != null && ph != null && ph.getPreferredSource() != null) {
            return (PartnerVO)(ph.getPreferredSource().get(sourceCode));
        }
        return null;
    }

    /**
     * Returns preferred partner information by name. Return null if the name is not that of
     * a preferred partner.
     * @param partnerName
     * @return PartnerVO containing preferred partner information
     */
    public static PartnerVO getPreferredPartnerByName(String partnerName)
    {
        PartnerHandler ph = (PartnerHandler)CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_PARTNER_HANDLER);
        if(partnerName != null && ph != null && ph.getPreferredPartner() != null) {
            return (PartnerVO)(ph.getPreferredPartner()).get(partnerName);
        }
        return null;
    }

    /**
     * Returns true if the partner is a preferred partner.
     * @param partnerName
     * @return
     */
    public static boolean isPreferredPartner(String partnerName) {
        PartnerHandler ph = (PartnerHandler)CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_PARTNER_HANDLER);
        if(partnerName != null && ph != null && ph.getPreferredPartner() != null) {
            return (ph.getPreferredPartner()).containsKey(partnerName);
        }
        return false;
    }
     
     /**
     * Create XMLDocument for Mass Processing screens. Name and value are both partner names for now.
     * preferredPartners/preferredPartner/name
     * preferredPartners/preferredPartner/value
     * @return
     * @throws Exception
     */
     public static Document getPreferredPartnerForMassProcess() throws Exception {
    
        Document doc = JAXPUtil.createDocument();
        Element root = JAXPUtil.buildSimpleXmlNode(doc, "preferredPartners", "");
        doc.appendChild(root);
    
        Map ppMap = getPreferredPartner();
        if(ppMap != null) {
           Set keys = ppMap.keySet();
           Iterator iter = keys.iterator();
           while(iter.hasNext()) {
               String partnerName = (String)iter.next();
               PartnerVO pvo = (PartnerVO)ppMap.get(partnerName);
               Element parent = JAXPUtil.buildSimpleXmlNode(doc, "preferredPartner", "");
               root.appendChild(parent);
               
               Element newElement = JAXPUtil.buildSimpleXmlNode(doc,"name", partnerName);
               parent.appendChild(newElement);
               
               newElement = JAXPUtil.buildSimpleXmlNode(doc,"value", pvo.getDisplayValue());
               parent.appendChild(newElement);
           }
        }
        return doc;
     }     
     
    /**
     * Returns the replyEmailAddress if the partner is a preferred partner.
     * @param partnerName
     * @return
     */
    public static String getPreferredPartnerReplyEmail(String partnerName) 
    {
        PartnerHandler ph = (PartnerHandler)CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_PARTNER_HANDLER);
        if(partnerName != null && ph != null && ph.getPreferredPartner() != null) 
        {
            PartnerVO partnerVO = (PartnerVO) ph.getPreferredPartner().get(partnerName);
            if (partnerVO != null)
            {
                return partnerVO.getReplyEmailAddress();
            }
        }
        return null;
    }

    /**
     * Returns partner master information by name
     * @param partnerName
     * @return PartnerVO containing partner information
     */
    public static PartnerVO getPartnerByName(String partnerName, Connection connection) throws Exception
    {
        Map paramMap = new HashMap();
        PartnerVO pVO = null;

        paramMap.put("IN_PARTNER_NAME", partnerName);
        
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("GET_PARTNER_MASTER_BY_NAME");
        dataRequest.setInputParams(paramMap);
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        CachedResultSet rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);
        
        if(rs != null && rs.next()) {
            pVO = new PartnerVO();
            pVO.setPartnerName(partnerName);
            pVO.setPreferredProcessingResource(rs.getString("preferred_processing_resource"));
            pVO.setPreferredPartnerFlag(rs.getString("preferred_partner_flag"));
            pVO.setReplyEmailAddress(rs.getString("reply_email_address"));
            pVO.setDefaultPhoneSourceCode(rs.getString("default_phone_source_code"));
            pVO.setDefaultWebSourceCode(rs.getString("default_web_source_code"));
            pVO.setBinProcessingFlag(rs.getString("bin_processing_flag"));
            pVO.setDefaultPhoneSourceCodeDescription(rs.getString("default_phone_sc_description"));
            pVO.setDefaultWebSourceCodeDescription(rs.getString("default_web_sc_description"));
        }
        
        return pVO;  
    }

  /**
     * Returns the preferred partner name by order detail id. Return null if the source code is not that of
     * a preferred partner.
     * @param sourceCode
     * @return
     */
  public static PartnerVO getPreferredPartnerByOrderDetailId(String orderDetailId, Connection connection)
    throws Exception
  {
    Map paramMap = new HashMap();
    String sourceCode = null;

    paramMap.put("IN_ORDER_DETAIL_ID", orderDetailId);

    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(connection);
    dataRequest.setStatementID("CLEAN_GET_ORDER_DETAILS_II");
    dataRequest.setInputParams(paramMap);

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

    if (rs != null && rs.next())
    {
      sourceCode = (rs.getString("source_code"));
    }

    PartnerHandler ph = (PartnerHandler) CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_PARTNER_HANDLER);
    if (sourceCode != null && ph != null && ph.getPreferredSource() != null)
    {
      return (PartnerVO) (ph.getPreferredSource().get(sourceCode));
    }
    return null;
  }

  /**
   * Returns the paymentMethodId for given card number. If defaultToGC
   * is set and no match found, the returned value is defaulted to GC.
   * Caller needs to include CACHE_GET_PAYMENT_BIN_MASTER in cache config.
   * @param partnerName
   * @param defaultToGC
   * @return
   */
  public static String getPaymentMethodIdByNumber(String number, boolean defaultToGC) 
  {
      PaymentBinMasterHandler ph = (PaymentBinMasterHandler)CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_GET_PAYMENT_BIN_MASTER);
      String paymentMethodId = null;
      if(number != null && ph != null) 
      {
          paymentMethodId = ph.getPaymentMethodIdByNumber(number);
      }
      if (paymentMethodId == null || paymentMethodId.length() == 0) {
    	  if(defaultToGC) {
    		  paymentMethodId = "GC";
    	  }
      }
      return paymentMethodId;
  }
  

  /**
   * run the proc to recalculate payments for carts with removed orders. If the cart has no removed 
   * orders then nothing will change, i.e. it is OK to call this in all cases.
   * @param guid of order to recalculated
   * @return 
   */
  public static void recalculateRemovedOrders(Connection connection, String orderguid)
  {
      Map paramMap = new HashMap();
      paramMap.put("IN_GUID", orderguid);
      DataRequest dataRequest = new DataRequest();
      dataRequest.setConnection(connection);
      dataRequest.setStatementID("UPDATE_REMOVED_ORD_PAY_BY_GUID");
      dataRequest.setInputParams(paramMap);

      try
      {      
          Map outputs = null;

          /* execute the store procedure */
          DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
          outputs = (Map) dataAccessUtil.execute(dataRequest);

          /* read store procedure output parameters to determine
           * if the procedure executed successfully */
          String status = (String) outputs.get("OUT_STATUS_PARAM");
          if(status != null && status.equalsIgnoreCase("N"))
          {
              String message = (String) outputs.get("OUT_MESSAGE_PARAM");
              throw new Exception(message);
          }      
      }
      catch (Exception e)
      {
          logger.error(e);
          throw new RuntimeException(e.getMessage(), e);
      }
  }
  

  /**
   * This is method is used insert message into JMS queue
   * @param connection - Database Connection
   * @param payLoad - Message Pay Load
   * @param queueName - Queue Name
   * @param correlationId - JMS Correlation ID
   * @param delaySec - Time in Seconds
   * @throws Exception
   */
  public static void insertJMSMessage(Connection connection, String payLoad, String queueName, String correlationId, long delaySec) throws Exception
  {
      DataRequest dataRequest = new DataRequest();
      dataRequest.setConnection(connection);
      dataRequest.setStatementID("PIF_POST_A_MESSAGE");

      HashMap inputParams = new HashMap();
      inputParams.put("IN_QUEUE_NAME", queueName);
      inputParams.put("IN_CORRELATION_ID", correlationId);
      inputParams.put("IN_PAYLOAD", payLoad);
      inputParams.put("IN_DELAY_SECONDS", delaySec);
      dataRequest.setInputParams(inputParams);

      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      Map outputs = (Map)dataAccessUtil.execute(dataRequest);

      String status = (String)outputs.get("OUT_STATUS");
      if (status != null && status.equalsIgnoreCase("N"))
      {
          String message = (String)outputs.get("OUT_MESSAGE");
          throw new Exception(message);
      }

  }  
  
  
	/**
	 * #11946 - Gets the Zip code availability information for a BBN order.
	 * 
	 * @param connection
	 * @param zipCode
	 * @param weekDayDelByNoon
	 * @param satDelByNoon
	 * @return
	 */
	public static boolean getZipCodeAvailability(Connection connection,
			String zipCode, String weekDayDelByNoon, String satDelByNoon) {

		DataRequest dataRequest = new DataRequest();

		dataRequest.setStatementID("GET_BBN_AVAILABILITY");
		dataRequest.addInputParam("IN_ZIP_CODE", zipCode);
		dataRequest.addInputParam("WEEKDAY_BY_NOON", weekDayDelByNoon);
		dataRequest.addInputParam("SATURDAY_BY_NOON", satDelByNoon);

		dataRequest.setConnection(connection);
		dataRequest.setStatementID("GET_BBN_AVAILABILITY");

		try {
			Map outputs = null;

			/* execute the store procedure */
			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
			outputs = (Map) dataAccessUtil.execute(dataRequest);

			String out_status = (String) outputs.get("OUT_STATUS");
			String out_message = (String) outputs.get("OUT_MESSAGE");

			logger.debug("BBN availability : " + out_status + ", Error message: " + out_message);

			if (!"Y".equals(out_status)) {
				return false;
			}
		} catch (Exception e) {
			logger.error(e);
		}
		return true;
	} 
	
	/**
	 * This method is used to get the file name from secured config and read the file and get the stored password for given context and name.
	 * @param context
	 * @param name
	 * @return
	 * @throws Exception
	 */
	public static String getPasswordFromSSHStore(String context, String name) throws Exception
	{
		logger.info("Started getPasswordFromSSHStore");
		String passwordString = null;
		BufferedReader br = null;
		try
		{
			String pwdFile= ConfigurationUtil.getInstance().getSecureProperty(context, name);
			
			if (pwdFile != null){
				br = new BufferedReader(new FileReader(new File(pwdFile)));
				 
				passwordString = br.readLine();
				logger.info("Password retrieved successfully from ssh store");
			}
			
		}catch (Exception e)
		{
			logger.error(e.getMessage(), e);
			throw e;
		}finally {
            try {
                if (br != null) {
                    br.close();
                }
            } catch (IOException e) {
                logger.error(e.getMessage());
            }
        }
		logger.info("End getPasswordFromSSHStore");
		return passwordString;
	}
	
	// 16012- As part of 16012 adding new method insertJMS which take the input as JmsQueueVO . The JmsqueueVO will have required attributes. Going forward we should start
	//using this method to insert the jms messages
	
	/**
	   * This is method is used insert message into JMS queue
	   * @param connection - Database Connection
	   * @param payLoad - Message Pay Load
	   * @param queueName - Queue Name
	   * @param correlationId - JMS Correlation ID
	   * @param delaySec - Time in Seconds
	   * @throws Exception
	   */
	  public static void insertJMSMessage(Connection connection, JmsQueueVO jmsQueueVo) throws Exception
	  {
	      DataRequest dataRequest = new DataRequest();
	      dataRequest.setConnection(connection);
	      dataRequest.setStatementID("PIF_POST_A_MESSAGE");

	      HashMap inputParams = new HashMap();
	      inputParams.put("IN_QUEUE_NAME", jmsQueueVo.getQueueName());
	      inputParams.put("IN_CORRELATION_ID", jmsQueueVo.getCorrelationId());
	      inputParams.put("IN_PAYLOAD", jmsQueueVo.getPayload());
	      inputParams.put("IN_DELAY_SECONDS", jmsQueueVo.getDelaySeconds());
	      dataRequest.setInputParams(inputParams);

	      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
	      Map outputs = (Map)dataAccessUtil.execute(dataRequest);

	      String status = (String)outputs.get("OUT_STATUS");
	      if (status != null && status.equalsIgnoreCase("N"))
	      {
	          String message = (String)outputs.get("OUT_MESSAGE");
	          throw new Exception(message);
	      }
	  }  
	  
	  
      /**
       * This method obtains the order delivery date for an order.
       *
       * @param orderDetailId - order detail id
       * @return String - order delivery date
       * @throws Exception
       */
      public static Date getOrderDeliveryDate(Connection connection, String orderDetailId) throws Exception
      {
            Date orderDeliveryDate = null;
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(connection);
            dataRequest.setStatementID("CLEAN_GET_ORDER_DETAILS_II");
            dataRequest.addInputParam("IN_ORDER_DETAIL_ID", orderDetailId);

            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            
            CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
            if(rs.next())
            {
                if (rs.getObject("delivery_date") != null)
                {
                  String sDeliveryDate = rs.getString("delivery_date");
                  orderDeliveryDate = sdf.parse(sDeliveryDate);
                }
            }
            else
            {
              throw new Exception("No order was found for order detail id:" + orderDetailId);
            }
            return orderDeliveryDate;
     }
     public static String getSourceCodeTaxFlag(String sourceId)throws Exception{
    	 
    	    DataRequest dataRequest = new DataRequest();
    	    String sourceCodeTaxFlag = null;
    	    Connection connection=null;
    	 try{
    		 connection= DataSourceUtil.getInstance().getConnection("ORDER SCRUB");
	         dataRequest.setConnection(connection);
	         dataRequest.setStatementID("GET_SOURCE_CODE_TAX_FLAG");
	         dataRequest.addInputParam("IN_SOURCE_CODE",sourceId);
	         DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
	         sourceCodeTaxFlag = (String) dataAccessUtil.execute(dataRequest);
	         logger.debug("sourceCodeTaxFlag:"+sourceCodeTaxFlag);
    	   }catch(Exception exception){
    		   logger.error("Getting exception while getting source code flag."+exception.getMessage());
    		   logger.debug("sourceCodeTaxFlag:"+sourceCodeTaxFlag);
    	   }finally{
			try {
				if(dataRequest != null && dataRequest.getConnection() != null && !dataRequest.getConnection().isClosed()) {
					dataRequest.getConnection().close();
				}
			} catch (SQLException e) {
				logger.error("Error caught closing the connection, " + e.getMessage());
			}
		}
    	 return sourceCodeTaxFlag.trim(); 
    	 
     }
     
     /**
 	 * This method obtains the Custom Shipping Carrier for an Source Code.
 	 *
 	 * @param Source
 	 *            Code - Source Code
 	 * @return String - Affiliate Code
 	 * @throws Exception
 	 */
 	public static String getAffiliateCode(Connection connection,String sourceCode) {

 		String affiliateCode = null;
 		DataRequest dataRequest = new DataRequest();
		try {
			dataRequest.setConnection(connection);
			dataRequest.setStatementID("GET_AFFILIATE_CODE");
			dataRequest.addInputParam("IN_SOURCE_CODE", sourceCode);

			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

			affiliateCode = (String) dataAccessUtil.execute(dataRequest);
		} catch (Exception e) {
			logger.error("Error while getting affiliateCode :"+affiliateCode+" for the sourceCode :"+sourceCode+"\n"+e.getMessage());
		}
		finally{
			logger.debug(" sourceCod = "+sourceCode+" and affiliateCode:"+ affiliateCode);
		}
		return affiliateCode;
 		
 	}


   /**
    * Method to extract name/value pairs (and sort order) from the PDP (West) personalization string
    * and populate two maps so the caller has access to item names, values and their order.
    * 
    * Example PDP data (that would be passed in personalizationStr parameter):
    * 
    * <personalizations>
    *  <personalization> 
    *     <data> 
    *        <![CDATA[
    *        <Accessories>
    *           <AccessoryId>1234567</AccessoryId>
    *           <Personalization>
    *              <PersonalizationItems>
    *                 <SortOrder>1</SortOrder>
    *                 <DisplayName>First Name</DisplayName>
    *                 <Value>Dewey</Value>
    *              </PersonalizationItems>
    *              <PersonalizationItems>
    *                 <DisplayName>Last Name</DisplayName>
    *                 <Value>Hafta</Value>
    *              </PersonalizationItems>
    *           </Personalization>
    *        </Accessories>
    *        ]]>
    *     </data>
    *     <name>
    *        <![CDATA[PersonalizationPayload]]>
    *     </name>
    *  </personalization>
    * </personalizations>  
    * 
    * @param personalizationStr
    * @param pcValueMap
    * @param pcSortMap
    */
   public static String populatePDPPersonalizationMaps(String personalizationStr,  
                                                     Map<String, String> pcValueMap,  
                                                     Map<String, String> pcSortMap) {
        InputSource inSrc;
        Document doc;
        NodeList nList;
        Element elem;
        String pdpDataStr = "";
        String accessoryId = null;
        
        try {
           DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
           String _personalizationStr = StringUtils.replace(personalizationStr, "&", "&amp;");
           
           // PDP personalization XML is actually in CDATA structure within 
           // PDP_PERSONALIZATION_TAG_ROOT element, so get it out first.
           //
           inSrc = new InputSource(new StringReader(_personalizationStr));
           doc = builder.parse(inSrc);
           doc.getDocumentElement().normalize();        
           nList = doc.getElementsByTagName(PDP_PERSONALIZATION_TAG_ROOT);
           if (nList != null && nList.getLength() == 1) {
              elem = (Element) nList.item(0);
              pdpDataStr = elem.getTextContent();
           } else {
              logger.error("Invalid PDP personalization string - no data element");
              return null;
           }
           
           // Next, parse the Accessory ID
           //
           inSrc = new InputSource(new StringReader(pdpDataStr));
           doc = builder.parse(inSrc);
           doc.getDocumentElement().normalize();        
           nList = doc.getElementsByTagName(PDP_ACCESSORY_ID);
           if (nList != null) {
              elem = (Element) nList.item(0);
              if (elem != null) {
                 accessoryId = getCharacterDataFromElement(elem);
              }
           }
           logger.info("PDP Accessory ID: " + accessoryId);
           
           // Now parse the PDP XML and get the name/value pairs and their order
           //
           nList = doc.getElementsByTagName(PDP_PERSONALIZATION_ITEMS);
           logger.info("PDP personalization element count: " + nList.getLength());
           for (int eNum=0; eNum < nList.getLength(); eNum++) {
              elem = (Element) nList.item(eNum);
              // Name (or key) of the pair
              NodeList name = elem.getElementsByTagName(PDP_PERSONALIZATION_NAME);
              Element line = (Element) name.item(0);
              String key = getCharacterDataFromElement(line);
              // Value of the pair
              name = elem.getElementsByTagName(PDP_PERSONALIZATION_VALUE);
              line = (Element) name.item(0);
              String value = getCharacterDataFromElement(line);
              pcValueMap.put(key, value);
              // Sort order (optional) for the pair
              name = elem.getElementsByTagName(PDP_PERSONALIZATION_SORT);
              String order = null;
              if (name != null) {
                 line = (Element) name.item(0);
                 if (line != null) {
                    order = getCharacterDataFromElement(line);
                    pcSortMap.put(key, order);
                 }
              }
              logger.info("PDP Personalization Item Name: " + key );
              logger.info("PDP Personalization Item Value: " + value);
              logger.info("PDP Personalization Item Order: " + order);
           }
           
        } catch (Exception e) {
           logger.error("Error parsing PDP personalization info !! " + e.getMessage());
        }
        return accessoryId;
  }

   
   private static String getCharacterDataFromElement(Element f) {

      NodeList list = f.getChildNodes();
      String data;

      for(int index = 0; index < list.getLength(); index++){
          if(list.item(index) instanceof CharacterData){
              CharacterData child  = (CharacterData) list.item(index);
              data = child.getData();

              if(data != null && data.trim().length() > 0)
                 return child.getData();
          }
      }
      return "";
   }
   
}
