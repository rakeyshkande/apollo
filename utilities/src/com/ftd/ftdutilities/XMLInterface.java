package com.ftd.ftdutilities;

public interface XMLInterface 
{

  public String toXML();

  public String toXML(int count);
}