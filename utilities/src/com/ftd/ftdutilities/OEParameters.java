package com.ftd.ftdutilities;

import java.io.Serializable;

import java.lang.reflect.Field;

import java.math.BigDecimal;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.constant.CacheMgrConstants;
import com.ftd.osp.utilities.cacheMgr.handlers.GlobalParmHandler;


public class OEParameters implements Serializable
{

    private BigDecimal  canadianExchangeRate = new BigDecimal(0);
    private int         deliveryDaysOut = 120;
    private int         deliveryDaysOutMax = 120;  //added them while working on MO
    private int         deliveryDaysOutMin = 14;  //added them while working on MO
    private String      exoticCutoff = "1200";
    private String      freshCutCutoff = "1200";
    private BigDecimal  freshCutSrvcCharge = new BigDecimal(0);
    private String      freshCutSrvcChargeTrigger = "Y";
    private String      fridayCutoff = "1400";
    private Date        GNADDDate = null;
    private String      GNADDLevel = "0";
    private int         intlAddOnDays = 0;
    private String      intlCutoff = "1200";
    private String      mondayCutoff = "1400";
    private String      saturdayCutoff = "1400";
    private BigDecimal  specialSrvcCharge = new BigDecimal(0);
    private String      specialtyGiftCutoff = "1200";
    private String      sundayCutoff  = "1200";
    private String      thursdayCutoff = "1400";
    private String      tuesdayCutoff = "1400";
    private String      wednesdayCutoff = "1400";

    //Modify Order
    private String      MOChocolatesAvailable = "N";
    private BigDecimal  MOOrderMaxDays =  new BigDecimal(120);
    private GlobalParmHandler globalParamHandler = null;
    private static Logger logger  = new Logger("com.ftd.ftdutilities.OEParameters");
    public OEParameters(){
    	globalParamHandler = (GlobalParmHandler) CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_GLOBAL_PARM);
    	try{
    	  deliveryDaysOutMin = Integer.parseInt(globalParamHandler.getGlobalParm("FTDAPPS_PARMS", "DELIVERY_DAYS_OUT_MIN"));
    	}catch(Exception e){
    		logger.error("Error supressed while creating Object for OEParameters :"+deliveryDaysOutMin);
    	}
    }

    public String getGNADDLevel()
    {
        return GNADDLevel;
    }

    public void setGNADDLevel(String newGNADDLevel)
    {
        GNADDLevel = newGNADDLevel;
    }

    public Date getGNADDDate()
    {
        return GNADDDate;
    }

    public void setGNADDDate(Date newGNADDDate)
    {
        GNADDDate = newGNADDDate;
    }

    public int getIntlAddOnDays()
    {
        return intlAddOnDays;
    }

    public void setIntlAddOnDays(int newIntlAddOnDays)
    {
        intlAddOnDays = newIntlAddOnDays;
    }

    public String getMondayCutoff()
    {
        return mondayCutoff;
    }

    public void setMondayCutoff(String newMondayCutoff)
    {
        mondayCutoff = newMondayCutoff;
    }

    public String getTuesdayCutoff()
    {
        return tuesdayCutoff;
    }

    public void setTuesdayCutoff(String newTuesdayCutoff)
    {
        tuesdayCutoff = newTuesdayCutoff;
    }

    public String getWednesdayCutoff()
    {
        return wednesdayCutoff;
    }

    public void setWednesdayCutoff(String newWednesdayCutoff)
    {
        wednesdayCutoff = newWednesdayCutoff;
    }

    public String getThursdayCutoff()
    {
        return thursdayCutoff;
    }

    public void setThursdayCutoff(String newThursdayCutoff)
    {
        thursdayCutoff = newThursdayCutoff;
    }

    public String getFridayCutoff()
    {
        return fridayCutoff;
    }

    public void setFridayCutoff(String newFridayCutoff)
    {
        fridayCutoff = newFridayCutoff;
    }

    public String getSaturdayCutoff()
    {
        return saturdayCutoff;
    }

    public void setSaturdayCutoff(String newSaturdayCutoff)
    {
        saturdayCutoff = newSaturdayCutoff;
    }

    public String getSundayCutoff()
    {
        return sundayCutoff;
    }

    public void setSundayCutoff(String newSundayCutoff)
    {
        sundayCutoff = newSundayCutoff;
    }

    public int getDeliveryDaysOut()
    {
        return deliveryDaysOut;
    }

    public void setDeliveryDaysOut(int newDeliveryDaysOut)
    {
        deliveryDaysOut = newDeliveryDaysOut;
    }

    public String getIntlCutoff()
    {
        return intlCutoff;
    }

    public void setIntlCutoff(String newIntlCutoff)
    {
        intlCutoff = newIntlCutoff;
    }

    public String getSpecialtyGiftCutoff()
    {
        return specialtyGiftCutoff;
    }

    public void setSpecialtyGiftCutoff(String newSpecialtyGiftCutoff)
    {
        specialtyGiftCutoff = newSpecialtyGiftCutoff;
    }

    public String getExoticCutoff()
    {
        return exoticCutoff;
    }

    public void setExoticCutoff(String newExoticCutoff)
    {
        exoticCutoff = newExoticCutoff;
    }

    public String getFreshCutCutoff()
    {
        return freshCutCutoff;
    }

    public void setFreshCutCutoff(String newFreshCutCutoff)
    {
        freshCutCutoff = newFreshCutCutoff;
    }

    public BigDecimal getFreshCutSrvcCharge()
    {
        return freshCutSrvcCharge;
    }

    public void setFreshCutSrvcCharge(BigDecimal newFreshCutSrvcCharge)
    {
        freshCutSrvcCharge = newFreshCutSrvcCharge;
    }

    public BigDecimal getSpecialSrvcCharge()
    {
        return specialSrvcCharge;
    }

    public void setSpecialSrvcCharge(BigDecimal newSpecialSrvcCharge)
    {
        specialSrvcCharge = newSpecialSrvcCharge;
    }

    public String getFreshCutSrvcChargeTrigger()
    {
        return freshCutSrvcChargeTrigger;
    }

    public void setFreshCutSrvcChargeTrigger(String newFreshCutSrvcChargeTrigger)
    {
        freshCutSrvcChargeTrigger = newFreshCutSrvcChargeTrigger;
    }

    public void setCanadianExchangeRate(BigDecimal newCanadianExchangeRate)
    {
        canadianExchangeRate = newCanadianExchangeRate;
    }

    public BigDecimal getCanadianExchangeRate()
    {
        return canadianExchangeRate;
    }


    public BigDecimal getMOOrderMaxDays()
    {
        return this.MOOrderMaxDays;
    }

    public void setMOOrderMaxDays(BigDecimal newMOOrderMaxDays)
    {
        this.MOOrderMaxDays = newMOOrderMaxDays;
    }


    public String getMOChocolatesAvailable()
    {
        return MOChocolatesAvailable;
    }

    public void setMOChocolatesAvailable(String newMOChocolatesAvailable)
    {
        this.MOChocolatesAvailable = newMOChocolatesAvailable;
    }



    public int getDeliveryDaysOutMax()
    {
        return this.deliveryDaysOutMax;
    }

    public void setDeliveryDaysOutMax(int newDeliveryDaysOutMax)
    {
        this.deliveryDaysOutMax = newDeliveryDaysOutMax;
    }

    public int getDeliveryDaysOutMin()
    {
        return this.deliveryDaysOutMin;
    }

    public void setDeliveryDaysOutMin(int newDeliveryDaysOutMin)
    {
        this.deliveryDaysOutMin = newDeliveryDaysOutMin;
    }







  /**
    * This method uses the Reflection API to generate an XML string that will be
    * passed back to the calling module.
    * The XML string will contain all the fields within this VO, including the
    * variables as well as (a collection of) ValueObjects.
    *
    * @param  None
    * @return XML string
   **/
  public String toXML(int count)
  {
    StringBuffer sb = new StringBuffer();
    try
    {
      if (count == 0)
      {
        sb.append("<OEParameter>");
      }
      else
      {
        sb.append("<OEParameter num=" + '"' + count + '"' + ">");
      }
      Field[] fields = this.getClass().getDeclaredFields();

      for (int i = 0; i < fields.length; i++)
      {
        //if the field retrieved was a list of VO
        if(fields[i].getType().equals(Class.forName("java.util.List")))
        {
          List list = (List)fields[i].get(this);
          if(list != null)
          {
            for (int j = 0; j < list.size(); j++)
            {
              XMLInterface xmlInt = (XMLInterface)list.get(j);
              int k = j + 1;
              String sXmlVO = xmlInt.toXML(k);
              sb.append(sXmlVO);
            }
          }
        }
        else
        {
          //if the field retrieved was a VO
          if (fields[i].getType().toString().matches("(?i).*vo"))
          {
            XMLInterface xmlInt = (XMLInterface)fields[i].get(this);
            int k = i + 1;
            String sXmlVO = xmlInt.toXML(k);
            sb.append(sXmlVO);
          }
          //if the field retrieved was a Calendar object
          else if (fields[i].getType().toString().matches("(?i).*calendar"))
          {
            Date date;
            String fDate = null;
            if (fields[i].get(this) != null)
            {
              date = (((GregorianCalendar)fields[i].get(this)).getTime());
              SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss a zzz");
              fDate = sdf.format(date).toString();
  	          sb.append("<" + fields[i].getName() + ">");
              sb.append(fDate);
              sb.append("</" + fields[i].getName() + ">");
            }
            else
            {
							sb.append("<" + fields[i].getName() + "/>");
            }

          }
          else
          {
            if (fields[i].get(this) == null)
            {
              sb.append("<" + fields[i].getName() + "/>");
            }
            else
            {
              sb.append("<" + fields[i].getName() + ">");
              sb.append(fields[i].get(this));
              sb.append("</" + fields[i].getName() + ">");
            }
          }
        }
      }
      sb.append("</OEParameter>");
    }

    catch (Exception e)
    {
      e.printStackTrace();
    }

    return sb.toString();
  }




}