package com.ftd.ftdutilities;

import java.math.BigDecimal;

public interface GeneralConstants {
	public static final String YES = "Y";
	public static final String NO = "N";

	
	public static final String CUSTOMER_TYPE_ITEM_SEND_TO = "ITEM_SEND_TO";
	public static final String CUSTOMER_TYPE_ORDER_SEND_TO = "ORDER_SEND_TO";
	public static final String CUSTOMER_TYPE_BILL_TO = "BILL_TO";

	public static final String POPUP_ID_KEY = "POPUP_ID";
	public static final String LOOKUP_SOURCE_CODE_KEY = "LOOKUP_SOURCE_CODE";
	public static final String LOOKUP_ZIP_CODE_KEY = "LOOKUP_ZIP_CODE";
	public static final String LOOKUP_FLORIST_KEY = "LOOKUP_FLORIST";
	public static final String LOOKUP_GREETING_CARDS_KEY = "LOOKUP_GREETING_CARDS";
	public static final String LOOKUP_GREETING_CARD_KEY = "LOOKUP_GREETING_CARD";
	public static final String LOOKUP_CUSTOMER_KEY = "LOOKUP_CUSTOMER";
	public static final String LOOKUP_RECIPIENT_KEY = "LOOKUP_RECIPIENT";
	public static final String DELIVERY_POLICIES_KEY = "DELIVERY_POLICIES";
	public static final String LOOKUP_RECIPE_KEY = "LOOKUP_RECIPE";
	public static final String LOOKUP_CARE_INSTRUCTIONS_KEY = "LOOKUP_CARE_INSTRUCTIONS";
	public static final String DISPLAY_CALENDAR_KEY = "DISPLAY_CALENDAR";

	public static final String SOURCE_CODE_SEARCH_KEY = "sourceCodeInput";
	public static final String PHONE_NUMBER_SEARCH_KEY = "phoneInput";
	public static final String CUSTOMER_ID_SEARCH_KEY = "customerIdInput";
	public static final String INSTITUTION_SEARCH_KEY = "institutionInput";
	public static final String ADDRESS1_SEARCH_KEY = "addressInput";
	public static final String ADDRESS2_SEARCH_KEY = "address2Input";
	public static final String ZIP_CODE_SEARCH_KEY = "zipCodeInput";
	public static final String CITY_SEARCH_KEY = "cityInput";
	public static final String STATE_SEARCH_KEY = "stateInput";
	public static final String OCCASION_SEARCH_KEY = "occasionInput";
	public static final String CATEGORY_SEARCH_KEY = "categoryInput";
	public static final String GREETING_CARD_SEARCH_KEY = "cardIdInput";
	public static final String MONTH_SEARCH_KEY = "month";
	public static final String YEAR_SEARCH_KEY = "year";
	public static final String COUNTRY_TYPE_SEARCH_KEY = "countryTypeInput";
	public static final String INST_TYPE_SEARCH_KEY = "institutionTypeInput";

	public static final String MAP_TYPE_HEADER = "HEADER";
	public static final String MAP_TYPE_CRUMB = "CRUMB";
	public static final String MAP_TYPE_UPSELL = "UPSELL";
	public static final String MAP_TYPE_PROMPT = "PROMPT";

	public final static String EMAIL_ADDRESS_SEPARATOR = ";";

	public final static String STOCK_EMAIL_CONTEXT = "STOCK_EMAIL";
	public final static String STOCK_EMAIL_CONTEXT_TOKEN = "TOKEN";

	public final static String TOKEN_START = "\\[\\[\\[";
	public final static String TOKEN_END = "\\]\\]\\]";

	// Length is needed because the length of the token may not be the same as
	// the number of characters matched
	// For example: "\\[" is 2 characters with length(), but only 1 character
	// '[' is matched in a regex
	public final static Integer TOKEN_START_LENGTH = 3;
	public final static Integer TOKEN_END_LENGTH = 3;

	/* Order Blob Status */
	public static final String BLOB_STATUS_ERR_ADDON_TRANS = "ERR_ADDON_TRANS";

	/* Source Code and DNIS Constants */
	public final static String JC_PENNEY_CODE = "JP";
	public final static String PARTNER_PRICE_CODE = "ZZ";
	public final static String DISCOVER_CODE = "DISC1";
	public final static String CORP15_CODE = "CORP15";
	public final static String CORP20_CODE = "CORP20";
	public final static String ADVO_CODE = "ADVO";

	/* CCAS Constants */
	public final static String OE_CCAS_PURCHASE_REQUEST = "PR";
	public final static String OE_CCAS_JC_PURCHASE_REQUEST = "JP";
	public final static String OE_COMMON_UTILITIES_CATEGORY_NAME = "com.ftd.pdb";
	public final static String CCAS_CONFIG_REALM_KEY = "ftd.pdb.application.configuration";
	public final static String CCAS_SERVER_IP = "ccas.server.ip";
	public final static String CCAS_PORT_NUMBER = "ccas.port";
	public final static String CCAS_JC_SERVER_IP = "ccas.jc.server.ip";
	public final static String CCAS_JC_PORT_NUMBER = "ccas.jc.port";
	public final static int OE_UNKNOWN_HOST_EXCEPTION = 304;
	public final static int OE_IO_EXCEPTION = 306;

	/* Status Constants */
	public final static String DONE = "DONE";
	public final static String CANCEL = "CANCEL";
	public final static String COMPLETE = "COMPLETE";
	public final static String ABANDONED = "ABANDONED";
	public final static String IN_PROCESS = "INPROC";

	/* Delivery Constants */
	public final static String DELIVERY_NEXT_DAY = "Next Day Delivery";
	public final static String DELIVERY_TWO_DAY = "Two Day Delivery";
	public final static String DELIVERY_STANDARD = "Standard Delivery";
	public final static String DELIVERY_SATURDAY = "Saturday Delivery";
	public final static String DELIVERY_FLORIST = "Florist Delivery";
	public final static String DELIVERY_SUNDAY = "Sunday Delivery";
	public final static String DELIVERY_NEXT_DAY_CODE = "ND";
	public final static String DELIVERY_TWO_DAY_CODE = "2F";
	public final static String DELIVERY_STANDARD_CODE = "GR";
	public final static String DELIVERY_SATURDAY_CODE = "SA";
	public final static String DELIVERY_FLORIST_CODE = "FL";
	public final static String DELIVERY_SAME_DAY_CODE = "SD";
	public final static String DELIVERY_SUNDAY_CODE = "SU";
	

	public final static String DELIVERY_SERVICE_CHARGE = "Service Charge";

	/* Occasion Constants */
	public final static String OE_OCCASION_FUNERAL = "1";

	/* Product Constants */
	public final static int PRODUCTS_PER_PAGE = 9;

	/* Time Zone Constants */
	public final static String OE_TIMEZONE_INTERNATIONAL = "0";
	public final static String OE_TIMEZONE_EASTERN = "1";
	public final static String OE_TIMEZONE_CENTRAL = "2";
	public final static String OE_TIMEZONE_MOUNTAIN = "3";
	public final static String OE_TIMEZONE_PACIFIC = "4";
	public final static String OE_TIMEZONE_HAWAII = "5";
	public final static String OE_TIMEZONE_ALASKA = "5";
	public final static String OE_TIMEZONE_DEFAULT = "5";

	/* Product Type & Sub-Type constants */
	public final static String OE_PRODUCT_TYPE_FLORAL = "FLORAL";
	public final static String OE_PRODUCT_TYPE_FRECUT = "FRECUT";
	public final static String OE_PRODUCT_TYPE_SPEGFT = "SPEGFT";
	public final static String OE_PRODUCT_TYPE_SDG = "SDG";
	public final static String OE_PRODUCT_TYPE_SDFC = "SDFC";
	public final static String OE_PRODUCT_TYPE_SERVICES = "SERVICES";

	public final static String OE_PRODUCT_SUBTYPE_EXOTIC = "EXOTIC";
	public final static String OE_PRODUCT_SUBTYPE_NONE = "NONE";
	public final static String OE_PRODUCT_SUBTYPE_FREESHIP = "FREESHIP";

	/* Product delivery type constants */
	public final static String OE_DELIVERY_TYPE_DOMESTIC = "D";
	public final static String OE_DELIVERY_TYPE_INTERNATIONAL = "I";

	/* Payment and Pricing Constants */
	public final static String CREDIT_CARD_PAYMENT_TYPE = "C";
	// public final static String PAYMENT_TYPE_PAY_PAL = "PP";
	public final static String PAYMENT_METHOD_TYPE_AP = "P";
	public final static BigDecimal DISCOUNT_ROUNDER = new BigDecimal(".99");
	public final static String CASH_BACK = "Cash Back";
	public final static String POINTS = "Points";
	public final static String MILES = "Miles";
	public final static String PAYMENT_METHOD_TYPE_UA = "UA";
	public static final String GIFT_CARD_CONTEXT = "GLOBAL_CONFIG";
	public static final String GIFT_CARD_SWITCH = "GIFT_CARD_PAYMENT_ALLOWED";



	/* Country Code Constants */
	public final static String COUNTRY_CODE_US = "US";
	public final static String COUNTRY_CODE_CANADA_CA = "CA";
	public final static String COUNTRY_CODE_CANADA_CAN = "CAN";
	public final static String STATE_CODE_VIRGIN_ISLANDS = "VI";
	public final static String STATE_CODE_PUERTO_RICO = "PR";
	public final static String STATE_CODE_ALASKA = "AK";
	public final static String STATE_CODE_HAWAII = "HI";
	public final static String STATE_CODE_ARIZONA = "AZ";

	/* Function constants */
	public final static String FUNCTION_GENERALMAINT = "GENERALMAINT";

	/* Billing Info constants */
	public final static String DEFAULT_MEMBER_ID = "000000000";
	public final static String VERBAL_APPROVAL_VERBAGE = "AP";
	public final static String VERBAL_APPROVAL_ACTION_CODE = "000";

	/* Global Parm processing constants */
	public final static String XML_OCCASION_DATA = "occasionData";
	public final static String XML_SUPER_INDEX_DATA = "superIndexData";
	public final static String XML_DELIVERY_DATA = "deliveryData";
	public final static String XML_PRODUCT_LIST = "productList";
	public final static String XML_DATE_RANGE_DATA = "dateRangeList";

	/* Order Origin */
	public final static String ORDER_ORIGIN_AMAZON = "amzni";

	public static final String SCRUB_CONFIG_CONTEXT = "SCRUB_CONFIG";
	public static final String AP_INVALID_AUTH = "AP_INVALID_AUTH";

	/* Database Queries */
	public static final String GET_NEXT_ORDER_ID = "GET_NEXT_ORDER_ID";

	// Delivery Confirmation codes
	public static final String DCON_PENDING = "Pending";
	public static final String DCON_SENT = "Sent";
	public static final String DCON_QUEUED = "Queued";
	public static final String DCON_CONFIRMED = "Confirmed";
	public static final String DCON_NO_EMAIL = "No Email";

	// AVS constants
	public static final String XML_TAG_ADDRESS_VERIFICATION = "address-verification";
	public static final String XML_TAG_RECIP_ADDRESS_VERIFIED = "recip-avs-performed";
	public static final String XML_TAG_RECIP_ADDRESS_VERIFICATION_RESULT = "recip-address-verification-result";
	public static final String XML_TAG_RECIP_ADDRESS_VERIFICATION_BYPASSED = "recip-address-verification-override-flag";
	public static final String XML_TAG_AVS_STREET_ADDRESS = "avs-street-address";
	public static final String XML_TAG_AVS_CITY = "avs-city";
	public static final String XML_TAG_AVS_STATE = "avs-state";
	public static final String XML_TAG_AVS_POSTAL_CODE = "avs-postal-code";
	public static final String XML_TAG_AVS_COUNTRY = "avs-country";
	public static final String XML_TAG_AVS_ADDRESS_SCORES = "avs-address-scores";	
	public static final String XML_TAG_AVS_ENTITY_TYPE = "avs-entity-type";
	public static final String XML_TAG_AVS_LONGITUDE = "avs-longitude";
	public static final String XML_TAG_AVS_LATITUDE = "avs-latitude";
	public static final String XML_TAG_THRESHOLD_VALUE = "threshold-value";
	
    public static final String AVS_SCORE_ZIPPLUSFOUR_DIFFERENT = "avs_score_zipplusfour_different";
    public static final String AVS_SCORE_ADDRESS_LINE_DIFFERENT = "avs_score_address_line_different";
    public static final String AVS_SCORE_CITY_DIFFERENT = "avs_score_city_different";
    public static final String AVS_SCORE_STATE_DIFFERENT = "avs_score_state_different";
    public static final String AVS_SCORE_ZIP_DIFFERENT = "avs_score_zip_different";
    public static final String AVS_SCORE_LOW_CONFIDENCE = "avs_score_low_confidence";
    public static final String AVS_SCORE_MEDIUM_CONFIDENCE = "avs_score_medium_confidence";
    public static final String AVS_SCORE_HIGH_CONFIDENCE = "avs_score_high_confidence";
    public static final String AVS_SCORE_ADDRESS_TYPE_ADDRESS = "avs_score_address_type_address";
    public static final String AVS_SCORE_ADDRESS_TYPE_OTHER = "avs_score_address_type_other";
    public static final String AVS_SCORE_THRESHOLD_FLORIST = "avs_score_threshold_florist";
    public static final String AVS_SCORE_THRESHOLD_DROPSHIP = "avs_score_threshold_dropship";
    public static final String VERIFY_ADDRESS_DROPSHIP_ORDERS = "verify_address_dropship_orders";
    public static final String VERIFY_ADDRESS_FLORIST_ORDERS = "verify_address_floral_orders";
    public static final String AVS_ORDER_TYPE_DROPSHIP = "D";
    public static final String AVS_ORDER_TYPE_FLORIST = "F";
    public static final String AVS_VERIFIED_ADDRESS_FAIL = "FAIL";
    public static final String AVS_VERIFIED_ADDRESS_PASS = "PASS";
    public static final String AVS_YES = "Y";
    public static final String AVS_NO = "N";
    public static final String AVS_PERFORMED_WEBSITE = "WEBSITE";
    public static final String AVS_PERFORMED_APOLLO = "APOLLO";

    public static final String ORDER_ORIGIN_WEBSITE = "FOX";
    public static final String ORDER_ORIGIN_MOBILE = "MOBI";
    public static final String ORDER_ORIGIN_TEST = "TEST";
    public static final String ORDER_ORIGIN_BULK = "BULK";
    
    public String FTDW_SHIPPING_KEY = "FTD WEST";
    
    public static final String SERVICE_CONTEXT = "SERVICE";
	public static final String PAS_SVC_WSDL_PARAM = "PAS_SVC_URL";
	public static final String PAS_CONFIG_CONTEXT = "PAS_CONFIG"; 
	public static final String PAS_SVC_CONN_TIMEOUT_PARAM = "PAS_SVC_CONN_TIMEOUT";
	public static final String PAS_SVC_RECV_TIMEOUT_PARAM = "PAS_SVC_RECV_TIMEOUT";
	public static final String PAS_INT_SCRUB = "SCRUB";
	public static final String PAS_INT_JOE = "JOE";
	public static final String PAS_INT_OP = "OP";
	
}