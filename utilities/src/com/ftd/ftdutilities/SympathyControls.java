package com.ftd.ftdutilities;

import java.sql.Connection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.ftd.ftdutilities.SympathyLeadTimeCalculator;
import com.ftd.ftdutilities.SympathyConstants;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.order.ScrubMapperDAO;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.order.vo.RecipientAddressesVO;
import com.ftd.osp.utilities.order.vo.RecipientsVO;
import com.ftd.osp.utilities.order.vo.SympathyBusinessHours;
import com.ftd.osp.utilities.order.vo.SympathyControlsResponseVO;
import com.ftd.osp.utilities.order.vo.SympathyItemsVO;
import com.ftd.osp.utilities.plugins.Logger;



public class SympathyControls {
	
	private Connection connection;
	private String sympathyFuneralLocation = null;
	private String sympathyCemeteryLocation = null;
	private String sympathyHospitalLocation = null;
	
	public SympathyControls(Connection connection)
	{
		this.connection = connection;
		this.logger = new Logger("com.ftd.ftdutilities.SympathyControls");

	}
	ScrubMapperDAO dao = new ScrubMapperDAO(connection);
	ConfigurationUtil confUtil = new ConfigurationUtil();
	
	private static Logger logger = new Logger("com.ftd.oe.bo.SympathyControls");

	public  Map performSympathyControls(Connection qConn, SympathyItemsVO itemVO) throws Exception {
				
		CachedResultSet sourceCodeDetails = null;
		char companyFlag = 'N';
		SympathyControlsResponseVO response = new SympathyControlsResponseVO('Y', "");		
		Map map = new HashMap();		
        sourceCodeDetails = dao.getSourceCodeById(qConn, itemVO.getSourceCode());
        
 		if (sourceCodeDetails.next()) {			
 			
			String companyId = sourceCodeDetails.getString("company_id");
			String funeralCemeteryLocChk = sourceCodeDetails.getString("funeral_cemetery_loc_chk");
			String hospitalLocChck = sourceCodeDetails.getString("hospital_loc_chck");
			String funeralCemeteryLeadTimeChck = sourceCodeDetails.getString("funeral_cemetery_lead_time_chk");
			String funeralCemeteryLeadTime = sourceCodeDetails.getString("funeral_cemetery_lead_time");

			SympathyBusinessHours symBH = new SympathyBusinessHours();
			symBH.setBoHrsMonFriEnd(sourceCodeDetails.getString("bo_hrs_mon_fri_end"));
			symBH.setBoHrsMonFriStart(sourceCodeDetails.getString("bo_hrs_mon_fri_start"));
			symBH.setBoHrsSatEnd(sourceCodeDetails.getString("bo_hrs_sat_end"));
			symBH.setBoHrsSatStart(sourceCodeDetails.getString("bo_hrs_sat_start"));
			symBH.setBoHrsSunEnd(sourceCodeDetails.getString("bo_hrs_sun_end"));
			symBH.setBoHrsSunStart(sourceCodeDetails.getString("bo_hrs_sun_start"));
			
			ConfigurationUtil configUtil = new ConfigurationUtil();
			
			String globalFuneralLocChk = configUtil.getFrpGlobalParm(SympathyConstants.SYMPATHY_CONTROLS_CONFIG,"FUNERAL_CEMETERY_LOCATION_CHECK_"+companyId);
			String globalFuneralLeadTimeChck = configUtil.getFrpGlobalParm(SympathyConstants.SYMPATHY_CONTROLS_CONFIG,"FUNERAL_CEMETERY_LEAD_TIME_CHECK_"+companyId);
			String globalHospitalLocationChk = configUtil.getFrpGlobalParm(SympathyConstants.SYMPATHY_CONTROLS_CONFIG, "HOSPITAL_LOCATION_CHECK_"+companyId);
			
			companyFlag = computeCompanyFlag(companyId, configUtil);
			

			
			if(itemVO.getModule().equalsIgnoreCase("SCRUB")){
				sympathyFuneralLocation = SympathyConstants.SYMPATHY_LOCATION_FUNERAL_SCRUB;
				sympathyCemeteryLocation = SympathyConstants.SYMPATHY_LOCATION_CEMETERY_SCRUB;
				sympathyHospitalLocation = SympathyConstants.SYMPATHY_LOCATION_HOSPITAL_SCRUB;
			}
			else if(itemVO.getModule().equalsIgnoreCase("JOE") || itemVO.getModule().equalsIgnoreCase("SCRUB_UI")){
				sympathyFuneralLocation = SympathyConstants.SYMPATHY_LOCATION_FUNERAL;
				sympathyCemeteryLocation = SympathyConstants.SYMPATHY_LOCATION_CEMETERY;
				sympathyHospitalLocation = SympathyConstants.SYMPATHY_LOCATION_HOSPITAL;
				if(itemVO.getModule().equalsIgnoreCase("SCRUB_UI")){
					itemVO.setModule("SCRUB");
				}
			}
			
			if(companyFlag=='Y'){
				if (itemVO.getDeliveryLocation().contains(sympathyFuneralLocation) || itemVO.getDeliveryLocation().equalsIgnoreCase(sympathyCemeteryLocation)) {
					doFuneralCemeteryControls(qConn, itemVO, globalFuneralLocChk, funeralCemeteryLocChk, response, symBH, funeralCemeteryLeadTimeChck, globalFuneralLeadTimeChck, funeralCemeteryLeadTime);

				} else if (!itemVO.isLeadTimeCheckFlag() && (itemVO.getDeliveryLocation().equalsIgnoreCase(sympathyHospitalLocation))) {
					doHospitalControls(qConn, itemVO, response, hospitalLocChck,
							globalHospitalLocationChk);
				}
			}

		}
 		 
  		 map.put("status", response.getStatus());
 		 map.put("message", response.getResponseMessage());
 		 
		return map;
		
	}

	/**
	 * @param qConn
	 * @param shipMethod
	 * @param dao
	 * @param response
	 * @param hospitalLocChck
	 * @param globalHospitalLocationChk
	 * @throws Exception
	 */
	private void doHospitalControls(Connection qConn, SympathyItemsVO itemVO,
		 SympathyControlsResponseVO response, String hospitalLocChck, String globalHospitalLocationChk)
			throws Exception {
		if (globalHospitalLocationChk!=null && globalHospitalLocationChk.equalsIgnoreCase("Y")) {

			if (hospitalLocChck!=null && (hospitalLocChck.equalsIgnoreCase("Y") || hospitalLocChck.equalsIgnoreCase("D"))) {

				doShipMethodCheck(qConn, itemVO,"HOSPITAL_LOC_CHECK", response);

			} else {
				logger.info("Hospital Location check is OFF");
			}
		} else {
			logger.info("Global Hospital Location check is OFF");
		}
	}


	/**
	 * @param qConn
	 * @param deliveryDate
	 * @param deliveryTime
	 * @param shipState
	 * @param shipMethod
	 * @param leadTimeCheckFlag
	 * @param dao
	 * @param response
	 * @param funeralCemeteryLocChk
	 * @param funeralCemeteryLeadTimeChck
	 * @param funeralCemeteryLeadTime
	 * @param sourceControlMap
	 * @param globalFuneralLocChk
	 * @param globalFuneralLeadTimeChck
	 * @throws Exception
	 */
	private void doFuneralCemeteryControls(Connection qConn, SympathyItemsVO itemVO, String globalFuneralLocChk, String funeralCemeteryLocChk, SympathyControlsResponseVO response, SympathyBusinessHours symBH, String funeralCemeteryLeadTimeChck, String globalFuneralLeadTimeChck, String funeralCemeteryLeadTime) throws Exception {
		if (globalFuneralLocChk!=null && globalFuneralLocChk.equalsIgnoreCase("Y")) {
			
			if (funeralCemeteryLocChk != null && (funeralCemeteryLocChk.equalsIgnoreCase("Y") || funeralCemeteryLocChk.equalsIgnoreCase("D"))) {

				doShipMethodCheck(qConn, itemVO,"FUNERAL_CEMETERY_LOC_CHECK", response);

			} else {
				
				logger.info("Funeral Cemetery location check is OFF");
			}

		} else {
			
			logger.info("Global Funeral Cemetery location check is OFF");
		}
		
	
		if (globalFuneralLeadTimeChck!=null && globalFuneralLeadTimeChck.equalsIgnoreCase("Y")) {

			if (funeralCemeteryLeadTimeChck!= null && (funeralCemeteryLeadTimeChck.equalsIgnoreCase("Y") || funeralCemeteryLeadTimeChck.equalsIgnoreCase("D"))) {
				
				if(itemVO.getDeliveryTime()>0 && itemVO.getDeliveryTime()!=null){
					doLeadTimeCheck(qConn, itemVO, response, funeralCemeteryLeadTime, symBH);
				}

			} else {
				
				logger.info("Funeral Cemetery Lead time check OFF");
			}

		} else {
			
			logger.info("Global Funeral Cemetery Lead time check OFF");
		}
	}
	


	/**
	 * @param qConn
	 * @param deliveryDate
	 * @param deliveryTime
	 * @param shipState
	 * @param dao
	 * @param response
	 * @param funeralCemeteryLeadTime
	 * @param sourceControlMap
	 * @throws Exception
	 */
	private void doLeadTimeCheck(Connection qConn, SympathyItemsVO itemVO, SympathyControlsResponseVO response,
			String funeralCemeteryLeadTime, SympathyBusinessHours symBH) throws Exception
			{
		boolean checkLeadTime = false;
		try {
			checkLeadTime = SympathyLeadTimeCalculator.checkLeadTime(qConn,itemVO, symBH,Integer.valueOf(funeralCemeteryLeadTime));
		} catch (NumberFormatException e) {
			logger.error("Exception occured while getting funeral cemetry lead time" + e, e);
		} catch (Exception e) {
			logger.error("Exception occured while calculating funeral cemetry lead time" + e, e);
		}

		if (!checkLeadTime) {
			response.setStatus('N');		
			
				String message = confUtil.getContentWithFilter(qConn, "SYMPATHY_CONFIG", itemVO.getModule()+"_MESSAGES", "FUNERAL_CEMETERY_LEAD_TIME_CHECK", null);
				message = message.replace("[FUNERAL_CEMETERY_LEAD_TIME]" , funeralCemeteryLeadTime);
				String oldMessage = response.getResponseMessage();
				if(oldMessage.length()>0 && itemVO.getModule().equalsIgnoreCase("JOE")){
					message = oldMessage+"<br><br>"+message;
					response.setResponseMessage(message);
				}
				else if(oldMessage.length()>0 && itemVO.getModule().equalsIgnoreCase("SCRUB")){
					response.setResponseMessage(oldMessage);
				}else{
					response.setResponseMessage(message);
				}
			
				
			
		} else {
			
			logger.info("Lead time available for delivery");
		}
	}


	/**
	 * @param qConn
	 * @param shipMethod
	 * @param dao
	 * @param response
	 * @throws Exception
	 */
	private void doShipMethodCheck(Connection qConn, SympathyItemsVO itemVO,String  errorMessage, SympathyControlsResponseVO response)
			throws Exception {
		if (itemVO.getShipMethod().equals("dropship")) {
			response.setStatus('N');
			response.setResponseMessage(confUtil.getContentWithFilter(qConn, "SYMPATHY_CONFIG", itemVO.getModule()+"_MESSAGES", errorMessage, null));
			//[LOCATION_TYPE]
		} else {
			logger.info("Ship Method is florist.");
		}
	}


	/**
	 * @param companyId
	 * @param configUtil
	 * @return
	 * @throws Exception
	 */
	private char computeCompanyFlag(String companyId,
			ConfigurationUtil configUtil) throws Exception {
		String globalSympathyAccount = configUtil.getFrpGlobalParm(SympathyConstants.SYMPATHY_CONTROLS_CONFIG, "SYMPATHY_CONTROLS_ACCOUNTS");	
		String globalSympathyCompanyList = "";
		
		if(globalSympathyAccount!=null && globalSympathyAccount!=""){
			globalSympathyCompanyList = globalSympathyAccount.trim().toUpperCase();
		}
		else{
			return 'N';
		}
		
		
		String[] companyList = globalSympathyCompanyList.split(",");
		char companyFlag = 'N';
		
		for(String thisCompany:companyList){
			if(companyId.equals(thisCompany)){
				companyFlag = 'Y';
			}
		}
		return companyFlag;
	}
	
	public HashMap addCartLevelSympathyAlerts(OrderVO order){
		Collection items = order.getOrderDetail();
		Iterator it = items.iterator();
		HashMap map = new HashMap();
		HashMap messageMap = new HashMap();
		boolean setCartMessage = true;
		RecipientsVO tmpRecipientsVO = new RecipientsVO();
		RecipientAddressesVO tmpRecipientAddressesVO = new RecipientAddressesVO();
		int i = 1;//serves as item number. starting from 1		
		try{
			while(it.hasNext()){
					OrderDetailsVO item = (OrderDetailsVO)it.next();
								
					if (item!=null) {
						SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
			        	Date date = formatter.parse(item.getDeliveryDate());
						tmpRecipientsVO = (RecipientsVO)(item.getRecipients().get(0));
				        tmpRecipientAddressesVO = (RecipientAddressesVO)(tmpRecipientsVO.getRecipientAddresses().get(0));
		                String sympathyShipMethod = "florist";
		                String shipMethod = item.getShipMethod();
		                if(shipMethod!=null && (shipMethod.equals("ND") || shipMethod.equals("2F") || shipMethod.equals("GR") || shipMethod.equals("SA"))){
		                	sympathyShipMethod = "dropship";
		                }
		                SympathyItemsVO itemVO = new SympathyItemsVO();
		                itemVO.setSourceCode(item.getSourceCode());
		                itemVO.setDeliveryDate(date);
		                itemVO.setShipState(tmpRecipientAddressesVO.getStateProvince());
		                itemVO.setDeliveryLocation(tmpRecipientAddressesVO.getAddressType());
		                itemVO.setShipMethod(sympathyShipMethod);
		                itemVO.setLeadTimeCheckFlag(false);
		                if(tmpRecipientAddressesVO.getAddressType().length()>1){
			            	itemVO.setModule("SCRUB_UI");
			            }
			            else{
			            	itemVO.setModule("SCRUB");
			            }
		                Integer timeOfService = (item.getTimeOfService()!=null?Integer.valueOf(item.getTimeOfService()):0);
		                itemVO.setDeliveryTime(timeOfService);
		                if((!StringUtils.isEmpty(shipMethod) && sympathyShipMethod=="dropship") || (sympathyShipMethod=="florist")){
		                	map = (HashMap) performSympathyControls(connection, itemVO);
		                	messageMap.put("SympathyAlerts_"+i, map.get("message"));
		                	if(setCartMessage && messageMap.get("SympathyAlerts_"+i)!=null && messageMap.get("SympathyAlerts_"+i)!=""){
		                		messageMap.put("SympathyCheck",messageMap.get("SympathyAlerts_"+i));
		                		setCartMessage = false;
		                	}
		                	
		                }
		                else{
		                	map.put("status", 'Y');
		                	messageMap.put("SympathyAlerts_"+i, "");
		                }
					}
					++i;
				}
				
					return messageMap;
				
		}catch(Exception e){
			logger.error("Error occured while performing Sympathy Controls : "+e);
			return null;
		}
	}
	
}

