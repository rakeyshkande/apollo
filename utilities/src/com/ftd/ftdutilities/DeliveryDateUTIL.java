package com.ftd.ftdutilities;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.constant.CacheMgrConstants;
import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.cacheMgr.handlers.CountryMasterHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.GlobalParmHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.StateMasterHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.CountryMasterVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.FTDAppsGlobalParmsVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.order.MercentOrderPrefixes;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.order.vo.RecipientAddressesVO;
import com.ftd.osp.utilities.order.vo.RecipientsVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.ProductVO;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.io.IOException;

import java.math.BigDecimal;

import java.sql.Connection;
import java.sql.SQLException;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.commons.lang.StringUtils;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import org.xml.sax.SAXException;


public class DeliveryDateUTIL
{
    private static final int standardTransitTime = 3; // Days
    private static final int secondDayTransitTime = 2; // Days
    private static final int nextDayTransitTime = 1; // Day

    private static final String GET_VENDOR_NO_SHIP_DAYS = "GET_VENDOR_NO_SHIP_DAYS";
    private static final String GET_VENDOR_NO_SHIP_DATES = "GET_VENDOR_NO_SHIP_DATES";
    private static final String GET_VENDOR_DELIVERY_RESTRICT = "GET_VENDOR_DELIVERY_RESTRICT";
    private static final String GET_COUNRTY_HOLIDAYS = "GET_COUNRTY_HOLIDAYS";
    private static final String GET_PRODUCT_DETAILS = "GET PRODUCT DETAILS";
    private static final String GET_CODIFICATION_DETAILS = "GET_CODIFICATION_DETAILS";
    private static final String GET_GLOBAL_PARAMETERS = "GET_GLOBAL_PARAMETERS";
    private static final String GET_CSZ_PRODUCT_DETAILS = "GET_CSZ_PRODUCT_DETAILS";
    private static final String GET_CSZ_AVAIL_DETAILS = "GET_CSZ_AVAIL_DETAILS";
    private static final String GET_COUNTRY_DETAILS = "GET_COUNTRY_DETAILS";
    private static final String GET_ZIP_CODE_DETAILS = "GET_ZIP_CODE_DETAILS";
    private static final String GET_PRODUCT_SHIP_METHODS = "GET_PRODUCT_SHIP_METHODS";
    private static final String GET_GOTO_FLORIST_FLAG = "GET_GOTO_FLORIST_FLAG";

    private static final String COUNTRY_ID = "COUNTRY_ID";
    private static final String PRODUCT_ID = "PRODUCT_ID";
    private static final String ZIP_CODE = "ZIP_CODE";
    private static final String MODIFY_ORDER_CONTEXT = "FTDAPPS_PARMS";
    
    private static Logger logger = new Logger("com.ftd.ftdutilities.DeliveryDateUTIL");
    private static final String TEST_DATA_CONFIG_FILE_NAME = "test_data_config.xml";

    private int testYear;
    private int testMonth;
    private int testDay;
    private int testHour;
    private int testMinute;
    private boolean testMode;
    private String lastCutoffZip    = "";
    private String lastCutoffProdId = "";
    private String lastCutoffTime   = "";


    public DeliveryDateUTIL() {
      super();
      testYear = 0;
      testMonth = 0;
      testDay = 0;
      testHour = 0;
      testMinute = 0;
      testMode = false;
    }

    /**
     *
     * @param OEDeliveryDateParm object that holds all the information needed to derive delivery dates
     * @return String XML string representing delivery date information
     * @throws ParserConfigurationException 
     */

    public Document getOrderDeliveryDates(OEDeliveryDateParm parms) throws ParserConfigurationException
    {
        TimeZone timeZone = null;
        Calendar calendar = null;
        OrderVO order = parms.getOrder();
        OrderDetailsVO item = (OrderDetailsVO)order.getOrderDetail().get(parms.getItemNumber() - 1);
        RecipientsVO recipient = (RecipientsVO)item.getRecipients().get(0);
        RecipientAddressesVO recipAddress = (RecipientAddressesVO)recipient.getRecipientAddresses().get(0);

        // Domestic order, set time zone of calendar
        if (recipAddress.getInternational() == null || !recipAddress.getInternational().equals("Y"))
        {
            timeZone = getTimeZone(parms);
            calendar = Calendar.getInstance(timeZone);
        }
        else
        {
            calendar = Calendar.getInstance();
        }

        int maxDays = new OEParameters().getDeliveryDaysOut() + 1;

        ArrayList deliveryDates = getProductDeliveryDates(false, calendar, parms, maxDays,null);
        return getXMLEncodedDeliveryDates(deliveryDates, parms);
    }

    /**
     *
     * @param OEDeliveryDateParm object encompassing all information used in creating delivery dates
     * @return String representation of delivery date value object in XML
     * @throws ParserConfigurationException 
     */

    public Document getCalendarDeliveryDates(OEDeliveryDateParm parms) throws ParserConfigurationException
    {
        TimeZone timeZone = null;
        Calendar calendar = null;
        OrderVO order = parms.getOrder();
        OrderDetailsVO item = (OrderDetailsVO)order.getOrderDetail().get(parms.getItemNumber() - 1);
        RecipientsVO recipient = (RecipientsVO)item.getRecipients().get(0);
        RecipientAddressesVO recipAddress = (RecipientAddressesVO)recipient.getRecipientAddresses().get(0);

        // Domestic order, set time zone of calendar
        if (recipAddress.getInternational() == null || !recipAddress.getInternational().equals("Y"))
        {
            timeZone = getTimeZone(parms);
            calendar = Calendar.getInstance(timeZone);
        }
        else
        {
            calendar = Calendar.getInstance();
        }

        int calMonth = calendar.get(Calendar.MONTH);
        if ( (parms.getCalendarMonth() != null) &&
             (!parms.getCalendarMonth().trim().equals("")) )
        {
            calMonth = getCalendarMonth(parms.getCalendarMonth());
        }

        int calYear = calendar.get(Calendar.YEAR);
        if ( (parms.getCalendarYear() != null) &&
             (!parms.getCalendarYear().trim().equals("")) )
        {
            calYear = Integer.parseInt(parms.getCalendarYear());
        }
        calendar.set(calYear, calMonth, 1);
        int maxDays = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        ArrayList deliveryDates = getProductDeliveryDates(false, calendar, parms, maxDays,null);
        return getXMLEncodedCalendarDates(timeZone, deliveryDates, parms.getProductId());
    }


    /**
     *
     * @param OEDeliveryDateParm object that holds all the information needed to derive delivery dates
     * @return String XML string representing delivery date information
     */
    public OEDeliveryDate getFirstAvailableDate(OEDeliveryDateParm parms)
    {
        return getFirstAvailableDate(parms,null);
    }

    /**
     *
     * @param OEDeliveryDateParm object that holds all the information needed to derive delivery dates
     * @return String XML string representing delivery date information
     */
    public OEDeliveryDate getFirstAvailableDate(OEDeliveryDateParm parms, String requiredShipMethod)
    {
        TimeZone timeZone = null;
        Calendar calendar = Calendar.getInstance();
        OrderVO order = parms.getOrder();
        OrderDetailsVO item = (OrderDetailsVO)order.getOrderDetail().get(parms.getItemNumber() - 1);
        RecipientsVO recipient = (RecipientsVO)item.getRecipients().get(0);
        RecipientAddressesVO recipAddress = (RecipientAddressesVO)recipient.getRecipientAddresses().get(0);

        // Domestic order, set time zone of calendar
        if(recipAddress.getInternational() == null || !recipAddress.getInternational().equals("Y"))
        {
            timeZone = getTimeZone(parms);
            calendar.setTimeZone(timeZone);
        }

        //if passed in days out use that
        int maxDays = 0;
        if(parms !=null && parms.getGlobalParms() != null && parms.getGlobalParms().getDeliveryDaysOut() > 0)
        {
            maxDays = parms.getGlobalParms().getDeliveryDaysOut();
        }
        else
        {
          // set the number of days to process plus one
          maxDays = new OEParameters().getDeliveryDaysOut() + 1;
        }

        ArrayList deliveryDates = getProductDeliveryDates(true, calendar, parms, maxDays,requiredShipMethod);
        // Loop through delivery dates looking for the first available one.
        // If the first available date is in a range then calculate all the dates
        // in that range excluding dates that are not deliverable
        OEDeliveryDate deliveryDate = null;
        OEDeliveryDate rangeDate = null;
        StringBuffer dateRangeDisplayBuffer = new StringBuffer();
        boolean inRange = false;
        boolean firstRangeDate = true;
        for (int i = 0; i < deliveryDates.size(); i++)
        {
            deliveryDate = (OEDeliveryDate) deliveryDates.get(i);

            if(deliveryDate.getDeliverableFlag().equals("Y") && deliveryDate.isInDateRange())
            {
                if(firstRangeDate)
                {
                    rangeDate = deliveryDate;
                    firstRangeDate = false;
                }
                inRange = true;
                dateRangeDisplayBuffer.append(deliveryDate.getDayOfWeek());
                dateRangeDisplayBuffer.append(" ");
                dateRangeDisplayBuffer.append(deliveryDate.getDisplayDate());
                dateRangeDisplayBuffer.append(" or ");
            }
            else if(!deliveryDate.isInDateRange() && inRange)
            {
                inRange = false;
                deliveryDate = rangeDate;
                dateRangeDisplayBuffer.delete((dateRangeDisplayBuffer.length()-4), dateRangeDisplayBuffer.length());
                deliveryDate.setDisplayDate(dateRangeDisplayBuffer.toString());
                //issue 2479 & 2480 in MO - clear out the buffer
                dateRangeDisplayBuffer.delete(0, dateRangeDisplayBuffer.length());
            }

            if(!inRange && deliveryDate.getDeliverableFlag().equals("Y"))
            {
                break;
            }
        }

        return deliveryDate;
    }


    /**
     *
     * @param OEDeliveryDateParm object that holds all the information needed to derive delivery dates
     * @param int max days out
     * @return String XML string representing delivery date information
     */
    public OEDeliveryDate getFirstAvailableDate(OEDeliveryDateParm parms, int maxDays)
    {
        TimeZone timeZone = null;
        Calendar calendar = Calendar.getInstance();
        OrderVO order = parms.getOrder();
        OrderDetailsVO item = (OrderDetailsVO)order.getOrderDetail().get(parms.getItemNumber() - 1);
        RecipientsVO recipient = (RecipientsVO)item.getRecipients().get(0);
        RecipientAddressesVO recipAddress = (RecipientAddressesVO)recipient.getRecipientAddresses().get(0);

        // Domestic order, set time zone of calendar
        if(recipAddress.getInternational() == null || !recipAddress.getInternational().equals("Y"))
        {
            timeZone = getTimeZone(parms);
            calendar.setTimeZone(timeZone);
        }

        ArrayList deliveryDates = getProductDeliveryDates(true, calendar, parms, maxDays, null);
        // Loop through delivery dates looking for the first available one.
        // If the first available date is in a range then calculate all the dates
        // in that range excluding dates that are not deliverable
        OEDeliveryDate deliveryDate = null;
        OEDeliveryDate rangeDate = null;
        StringBuffer dateRangeDisplayBuffer = new StringBuffer();
        boolean inRange = false;
        boolean firstRangeDate = true;
        for (int i = 0; i < deliveryDates.size(); i++)
        {
            deliveryDate = (OEDeliveryDate) deliveryDates.get(i);

            if(deliveryDate.getDeliverableFlag().equals("Y") && deliveryDate.isInDateRange())
            {
                if(firstRangeDate)
                {
                    rangeDate = deliveryDate;
                    firstRangeDate = false;
                }
                inRange = true;
                dateRangeDisplayBuffer.append(deliveryDate.getDayOfWeek());
                dateRangeDisplayBuffer.append(" ");
                dateRangeDisplayBuffer.append(deliveryDate.getDisplayDate());
                dateRangeDisplayBuffer.append(" or ");
            }
            else if(!deliveryDate.isInDateRange() && inRange)
            {
                inRange = false;
                deliveryDate = rangeDate;
                dateRangeDisplayBuffer.delete((dateRangeDisplayBuffer.length()-4), dateRangeDisplayBuffer.length());
                deliveryDate.setDisplayDate(dateRangeDisplayBuffer.toString());
                //issue 2479 & 2480 in MO - clear out the buffer
                dateRangeDisplayBuffer.delete(0, dateRangeDisplayBuffer.length());
            }

            if(!inRange && deliveryDate.getDeliverableFlag().equals("Y"))
            {
                break;
            }
        }

        return deliveryDate;
    }


    /**
     *
     * @param OEDeliveryDateParm object that holds all the information needed to derive delivery dates
     * @return ArrayList list of OEDeliveryDate objects
     */
    public ArrayList getOrderProductDeliveryDates(OEDeliveryDateParm parms) throws ParserConfigurationException, SAXException, IOException, TransformerException
    {
        //setTestDateInfo();
        TimeZone timeZone = null;
        Calendar calendar = null;
        OrderVO order = parms.getOrder();
        OrderDetailsVO item = (OrderDetailsVO)order.getOrderDetail().get(parms.getItemNumber() - 1);
        RecipientsVO recipient = (RecipientsVO)item.getRecipients().get(0);
        RecipientAddressesVO recipAddress = (RecipientAddressesVO)recipient.getRecipientAddresses().get(0);

        // Domestic order, set time zone of calendar
        if ( recipAddress.getInternational() != null && recipAddress.getInternational().equals("N") ) {
            timeZone = getTimeZone(parms);
            calendar = Calendar.getInstance(timeZone);
            if(testMode)
              calendar.set(testYear, testMonth, testDay, testHour, testMinute);
        }
        else
        {
            calendar = Calendar.getInstance();
            if(testMode)
              calendar.set(testYear, testMonth, testDay, testHour, testMinute);
        }

        // set the number of days to process plus one

        int maxDays = parms.getGlobalParms().getDeliveryDaysOut() + 1;
        ArrayList deliveryDates = getProductDeliveryDates(false, calendar, parms, maxDays,null);
        return deliveryDates;
    }

    /**
     *
     * @param OEDeliveryDateParm object that holds all the information needed to derive delivery dates
     * @return ArrayList list of OEDeliveryDate objects
     */
    public ArrayList getOrderProductDeliveryDates(OEDeliveryDateParm parms, int maxDays) throws ParserConfigurationException, SAXException, IOException, TransformerException
    {
        setTestDateInfo();
        TimeZone timeZone = null;
        Calendar calendar = null;
        OrderVO order = parms.getOrder();
        OrderDetailsVO item = (OrderDetailsVO)order.getOrderDetail().get(parms.getItemNumber() - 1);
        RecipientsVO recipient = (RecipientsVO)item.getRecipients().get(0);
        RecipientAddressesVO recipAddress = (RecipientAddressesVO)recipient.getRecipientAddresses().get(0);

        // Domestic order, set time zone of calendar
        if (recipAddress.getInternational() == null || !recipAddress.getInternational().equals("Y"))
        {
            timeZone = getTimeZone(parms);
            calendar = Calendar.getInstance(timeZone);
            if(testMode)
              calendar.set(testYear, testMonth, testDay, testHour, testMinute);
        }
        else
        {
            calendar = Calendar.getInstance();
            if(testMode)
              calendar.set(testYear, testMonth, testDay, testHour, testMinute);
        }

        // set the number of days to process plus one
        // ivan vojinovic 05/02/2005: use a parameter passed in instead of the OeParameters value.
        maxDays = maxDays + 1;
        ArrayList deliveryDates = getProductDeliveryDates(false, calendar, parms, maxDays,null);
        return deliveryDates;
    }

    /**
     *
     * @param OEDeliveryDateParm object that holds all the information needed to derive delivery dates
     * @return XML string representing delivery date values
     */
    public Document getOrderProductDeliveryDatesXML(OEDeliveryDateParm parms) throws ParserConfigurationException, SAXException, IOException, TransformerException
    {
        ArrayList deliveryDates = getOrderProductDeliveryDates(parms);

        return getXMLEncodedDeliveryDates(deliveryDates, parms);
    }

    /**
     *
     * @param OEDeliveryDateParm object that holds all the information needed to derive delivery dates
     * @return ArrayList list of OEDeliveryDate objects
     * @throws ParserConfigurationException 
     */
    public Document getCalendarProductDeliveryDates(OEDeliveryDateParm parms) throws ParserConfigurationException
    {
        TimeZone timeZone = null;
        Calendar calendar = null;
        OrderVO order = parms.getOrder();
        OrderDetailsVO item = (OrderDetailsVO)order.getOrderDetail().get(parms.getItemNumber() - 1);
        RecipientsVO recipient = (RecipientsVO)item.getRecipients().get(0);
        RecipientAddressesVO recipAddress = (RecipientAddressesVO)recipient.getRecipientAddresses().get(0);

        // Domestic order, set time zone of calendar
        if (recipAddress.getInternational() == null || !recipAddress.getInternational().equals("Y"))
        {
            timeZone = getTimeZone(parms);
            calendar = Calendar.getInstance(timeZone);
        }
        else
        {
            calendar = Calendar.getInstance();
        }

        // convert passed month and year to valid calendar date
        int calMonth = calendar.get(Calendar.MONTH);
        if ( (parms.getCalendarMonth() != null) &&
             (!parms.getCalendarMonth().trim().equals("")) )
        {
            calMonth = getCalendarMonth(parms.getCalendarMonth());
        }

        int calYear = calendar.get(Calendar.YEAR);
        if ( (parms.getCalendarYear() != null)  &&
             (!parms.getCalendarYear().trim().equals("")) )
        {
            calYear = Integer.parseInt(parms.getCalendarYear());
        }

        calendar.set(calYear, calMonth, 1);
        int maxDays = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        ArrayList deliveryDates = getProductDeliveryDates(false, calendar, parms, maxDays,null);

        return getXMLEncodedCalendarDates(timeZone, deliveryDates, parms.getProductId());
     }

    /**
     *
     * @param boolean flag to stop processing once first available delivery date is identified
     * @param OEDeliveryDateParm object that holds all the information needed to derive delivery dates
     * @param Calendar calendar object that is used to derive delivery dates
     * @param int number of days to retrieve
     * @return ArrayList list of OEDeliveryDate objects
     */

    private ArrayList getProductDeliveryDates(boolean firstAvailable,
                                                Calendar calendar,
                                                    OEDeliveryDateParm delParms,
                                                            int maxDays,
                                                                String requiredShipMethod)
    {

        Date timerDate = new Date();
        ArrayList dateList = new ArrayList();
        OEDeliveryDate deliveryVO = null;
        Calendar currentDay = null;
        String currentDateStr = null;
        int currentDayOfWeek;
        int month;
        long dateDiff;
        boolean isDeliverable;
        boolean firstAvailDate = true;
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");

        String productType = delParms.getProductType();
        Calendar today = Calendar.getInstance(calendar.getTimeZone());
        if(testMode)
          today.set(testYear, testMonth, testDay, testHour, testMinute);

        OrderVO order = delParms.getOrder();
        OrderDetailsVO item = (OrderDetailsVO)order.getOrderDetail().get(delParms.getItemNumber() - 1);
        RecipientsVO recipient = (RecipientsVO)item.getRecipients().get(0);
        RecipientAddressesVO recipAddress = (RecipientAddressesVO)recipient.getRecipientAddresses().get(0);
        MercentOrderPrefixes mrcntOrderPrefixes = new MercentOrderPrefixes(delParms.getConn());
        HashMap vendorInfo = null;
        ProductVO prodVO = null;

        // Only check common carrier if not type floral or services
        if(!productType.equals(GeneralConstants.OE_PRODUCT_TYPE_FLORAL) && !productType.equals(GeneralConstants.OE_PRODUCT_TYPE_SERVICES))
        {
            /*
             *  retreive the product information,
             *  and then check for the SDS ship method
             */
            try
            {
                prodVO = retrieveProductInformation(delParms.getConn(),
                                              delParms.getProductId());
            }
            catch (Exception e)
            {
                logger.error("DeliveryDateUtil - getProductDeliveryDates() - Product Information cannot be retrieved.");
                logger.error(e);
            }

            if (prodVO != null &&
                StringUtils.isNotEmpty(prodVO.getShippingSystem()) &&
                StringUtils.equalsIgnoreCase(prodVO.getShippingSystem(), "SDS"))
            {
              try
              {
                  // Set holiday dates
                  this.getHolidayDates(delParms, this.getCountryHolidays(recipAddress.getCountry(), delParms.getConn()));

                  // Set vendor no ship dates
                  this.getVendorNoShipDates(delParms, delParms.getProductId(), delParms.getConn());

                  // Set vendor no ship days
                  this.getVendorNoShipDays(delParms, delParms.getProductId(), delParms.getConn());

                  vendorInfo = retrieveVendorCutoffInfo(delParms.getConn(),
                      StringUtils.isEmpty(delParms.getProductSubCodeId())? delParms.getProductId() : delParms.getProductSubCodeId()) ;

                  logger.info("SDS DB queries took " + (System.currentTimeMillis() - timerDate.getTime()) + " ms");

              }
              catch(Exception e)
              {
                  logger.error("DeliveryDateUtil - getProductDeliveryDates() - SDS issue - Delivery Date Util failed");
                  logger.error(e);
              }
            }
        }

        // Find delivery methods for each date
        logger.info("maxDays: " + maxDays);
        for ( int i=0; i < maxDays; i++ )
        {
            isDeliverable = true;
            currentDay = (Calendar) calendar.clone();
            currentDay.add(Calendar.DATE, i);

            month = currentDay.get(Calendar.MONTH) + 1;

            // do not use the getTime() method of the calendar object as it returns the date & time
            // converted to the timezone of the machine that it is running on. For example,
            // input E.S.T timezone zip code, getTime() will return in C.S.T if that is where server is.
            currentDateStr = zeroPad(month) + "/" + zeroPad(currentDay.get(Calendar.DAY_OF_MONTH)) + "/" + currentDay.get(Calendar.YEAR);
            currentDayOfWeek = currentDay.get(Calendar.DAY_OF_WEEK);

            // Set today's date
            deliveryVO = new OEDeliveryDate();
            deliveryVO.setDeliveryDate(currentDateStr);
            deliveryVO.setDayOfWeek(getDayOfWeek(currentDayOfWeek));

            //check if product is deliverable on this day of the week
            switch(currentDayOfWeek){
              case(Calendar.SUNDAY):
                  if(delParms.isStateSunExclusion())
                  {
                    isDeliverable = false;
                  }
                  break;
              case(Calendar.MONDAY):
                  if(delParms.isStateMonExclusion())
                  {
                    isDeliverable = false;
                  }
                  break;
              case(Calendar.TUESDAY):
                  if(delParms.isStateTueExclusion())
                  {
                    isDeliverable = false;
                  }
                  break;
              case(Calendar.WEDNESDAY):
                  if(delParms.isStateWedExclusion())
                  {
                    isDeliverable = false;
                  }
                  break;
              case(Calendar.THURSDAY):
                  if(delParms.isStateThrExclusion())
                  {
                    isDeliverable = false;
                  }
                  break;
              case(Calendar.FRIDAY):
                  if(delParms.isStateFriExclusion())
                  {
                    isDeliverable = false;
                  }
                  break;
              case(Calendar.SATURDAY):
                  if(delParms.isStateSatExclusion() )
                  {
                    isDeliverable = false;
                  }
                  break;
            }

            // check florist delivery for current date if the product is florist
            // delivered or if we have not product selected
            if(delParms.isShipMethodFlorist() || (delParms.getProductId() != null && delParms.getProductId().length() == 0))
            {
                if( order.getOrderOrigin().equalsIgnoreCase( GeneralConstants.ORDER_ORIGIN_AMAZON ))
                {
                  delParms.setSundayDelivery(Boolean.FALSE);
                }
                else if(mrcntOrderPrefixes.isMercentOrder(order.getOrderOrigin())){
                	boolean isSundayDeliveryNotAllowed = false;
                	try{
                		 isSundayDeliveryNotAllowed = ConfigurationUtil.getInstance().getFrpGlobalParm("MERCENT_CONFIG", "SUN_DELIVERY_ALLOWED_FLORAL").equalsIgnoreCase("N");
                	}catch(Exception e) {
                		logger.error(e);
                	}
                	if(isSundayDeliveryNotAllowed){
                		delParms.setSundayDelivery(Boolean.FALSE);
                	}
                }
              deliveryVO = floristDeliverableDate(deliveryVO, delParms, calendar, currentDay);
            }


            // Only check common carrier if not type floral or services
            if(!productType.equals(GeneralConstants.OE_PRODUCT_TYPE_FLORAL) && !productType.equals(GeneralConstants.OE_PRODUCT_TYPE_SERVICES))
            {
                // check common carrier delivery for current date
                deliveryVO = carrierDeliverableDate(deliveryVO, delParms, calendar, currentDay, prodVO, vendorInfo);
            }

            // If no shipping methods then the date is not deliverable
            if(productType.equals(GeneralConstants.OE_PRODUCT_TYPE_SERVICES))
            {
              // NOP, Services do not have shipping methods
            }
            else if(deliveryVO.getShippingMethods().size() < 1)
            {
                 isDeliverable = false;
            }
            else
            {
                //check if we only care about one specific ship method
                if(requiredShipMethod != null)
                {
                    boolean shipMethodFound = false;

                    //check if the ship method we care about is available
                    if(deliveryVO.getShippingMethods() != null)
                    {
                        Iterator iter = deliveryVO.getShippingMethods().iterator();
                        while(iter.hasNext())
                        {
                            ShippingMethod shippingMethod = (ShippingMethod)iter.next();
                            if(shippingMethod.getCode().equals(requiredShipMethod))
                            {
                                shipMethodFound = true;
                            }//found a match
                        }//end loop
                    }//if methods returned

                    if(!shipMethodFound)
                    {
                        isDeliverable = false;
                    }

                }
            }

            // check to see if date is a Holiday date
            if (isDeliverable && delParms.getHolidayDates() != null )
            {
                OEDeliveryDate holidayDate = (OEDeliveryDate) delParms.getHolidayDates().get(deliveryVO.getDeliveryDate());
                if ( holidayDate != null )
                {
                    deliveryVO.setHolidayText(holidayDate.getHolidayText());
                    if ( holidayDate.getDeliverableFlag().equals("N") )
                    {
                        isDeliverable = false;
                    }
                }
            }


            // Exception range is for unavailable dates and current date is in range
            if ( (isDeliverable) &&
                 ( delParms.getExceptionCode() != null && delParms.getExceptionCode().equals("U")) )
            {
                // skip date if is in exception range (includes both ends of range)
                if ( (getDateDiff(delParms.getExceptionFrom(), currentDay.getTime()) > -1)  &&
                     (getDateDiff(delParms.getExceptionTo(), currentDay.getTime()) < 1) )
                {
                    isDeliverable = false;
                }
            }

            // Exception range is for available dates and current date is in range
            if ( (isDeliverable) &&
                 ( delParms.getExceptionCode() != null && delParms.getExceptionCode().equals("A")) )
            {
                // skip date if not in exception range (includes both ends of range)
                if ( (getDateDiff(delParms.getExceptionFrom(), currentDay.getTime()) < 0)  ||
                     (getDateDiff(delParms.getExceptionTo(), currentDay.getTime()) > 0) )
                {
                    isDeliverable = false;
                }
            }

            // get the number of days between the today and process date to
            // use for comparisons.
            dateDiff = getDateDiff(today.getTime(), currentDay.getTime());

            // all dates before current date and after last allowed date out are not deliverable
            if ( (dateDiff < 0) || (dateDiff > delParms.getGlobalParms().getDeliveryDaysOut()) )
            {
                isDeliverable = false;
            }

           if ( order.getOrderOrigin().equalsIgnoreCase( GeneralConstants.ORDER_ORIGIN_AMAZON ) ||
        		   mrcntOrderPrefixes.isMercentOrder(order.getOrderOrigin()))
           {
               try
               {
                 setDateRanges(delParms);
               }
               catch(Exception e)
               {
                 logger.error("Exception: DelvieryDateUTIL---setDateRanges() " + e.toString());
               }

           }


            boolean inRange = false;
            // Check for date ranging
            if(//isDeliverable &&
               (delParms.isShipMethodFlorist() || (delParms.getProductId() != null && delParms.getProductId().length() == 0))  &&
               delParms.isDeliveryRangeFlag().booleanValue())
            {
                // Check to see if currentDay is in date range
                HashMap dateRangeFromMap = delParms.getDeliveryRangeFrom();
                HashMap dateRangeToMap = delParms.getDeliveryRangeTo();
                int y = 0;
                Date dateRangeFrom = null;
                Date dateRangeTo = null;
                Date myCurrentDate = null;
                for (y = 0; y < dateRangeFromMap.size(); y++ )
                {
                    try {
                        dateRangeFrom = sdf.parse((String)dateRangeFromMap.get(String.valueOf(y)));
                        dateRangeTo = sdf.parse((String)dateRangeToMap.get(String.valueOf(y)));
                        myCurrentDate = sdf.parse(currentDateStr);
                    }
                    catch(Exception e) {
                        logger.error(e);
                        continue;
                    }

                    if ( (getDateDiff(dateRangeFrom, myCurrentDate) > -1) &&
                         (getDateDiff(dateRangeTo, myCurrentDate) < 1) ) {
                        inRange = true;
                        break;
                    }
                }

                if(inRange) {
                    deliveryVO.setInDateRange(true);
                    delParms.setDateRange(dateRangeTo);
                    deliveryVO.setHolidayText((String)delParms.getDeliveryRangeText().get(String.valueOf(y)));
                }
                else {
                    inRange = false;
                }
            }

            deliveryVO.setDisplayDate(deliveryVO.getDeliveryDate());


            if(isDeliverable)
            {
                deliveryVO.setDeliverableFlag("Y");
                if(firstAvailDate)
                {
                    // set the first deliverable date in the order for later processing
                    delParms.setFirstAvailableDate(deliveryVO.getDeliveryDate());
                    firstAvailDate = false;
                }
            }
            else
            {
                deliveryVO.setDeliverableFlag("N");
            }

            dateList.add(deliveryVO);

            // If only need first available date, break loop
            if (firstAvailable && isDeliverable && !inRange)
            {
                break;
            }
        }

        logger.info("Delivery date calculation took " + (System.currentTimeMillis() - timerDate.getTime()) + " ms");
        return dateList;
    }


    private void setDateRanges(OEDeliveryDateParm delParms)
          throws IOException, ParserConfigurationException, SAXException, SQLException
    {
            DataAccessUtil dau = DataAccessUtil.getInstance();
            DataRequest dataRequest = new DataRequest();

            dataRequest.setStatementID("GET_DELIVERY_DATE_RANGES_CSR");
            Connection con = delParms.getConn();
            dataRequest.setConnection(con);
            CachedResultSet rs = (CachedResultSet)dau.execute(dataRequest);
            dataRequest.reset();

            Map deliveryRangeFromMap = new HashMap();
            Map deliveryRangeToMap = new HashMap();
            Map deliveryRangeTextMap = new HashMap();
            String startDate = null;
            String endDate = null;
            String deliveryRangeText = null;
            int count = 0;

            while( rs.next() )
            {
                startDate = (String)rs.getObject(1);
                endDate  = (String)rs.getObject(2);
                deliveryRangeText = (String)rs.getObject(3);

                deliveryRangeFromMap.put(String.valueOf(count), startDate);
                deliveryRangeToMap.put(String.valueOf(count), endDate);
                deliveryRangeTextMap.put(String.valueOf(count),deliveryRangeText);
                count++;
            }

            if( count > 0 )
            {
              delParms.setDeliveryRangeFlag(Boolean.TRUE);
              delParms.setDeliveryRangeFrom((HashMap)deliveryRangeFromMap);
              delParms.setDeliveryRangeTo((HashMap)deliveryRangeToMap);
              delParms.setDeliveryRangeText((HashMap)deliveryRangeTextMap);
            }
            else
            {
              delParms.setDeliveryRangeFlag(Boolean.FALSE);
            }
    }

    /**
     *
     * @param ArrayList list of OEDeliveryDate objects
     * @return String delivery date information formatted in an XML string
     * @throws ParserConfigurationException 
     */
    public Document getXMLEncodedDeliveryDates(List dates, OEDeliveryDateParm delParms) throws ParserConfigurationException
    {
        Document document = DOMUtil.getDefaultDocument();
        Element root = (Element) document.createElement("shippingData");
        document.appendChild(root);

        Element deliveryDates = (Element) document.createElement("deliveryDates");
        root.appendChild(deliveryDates);

        Element entry = null;
        Element entryRange = null;
        OEDeliveryDate currentDate = null;
        OEDeliveryDate rangeDate = null;
        HashMap shippingMethods = new HashMap();
        StringBuffer dateRangeDisplayBuffer = new StringBuffer();
        boolean inRange = false;
        boolean firstRange = true;
        ProductVO prodVO = null;
        String shippingSystem="";
		try {
			prodVO = retrieveProductInformation(delParms.getConn(),
					delParms.getProductId());
			shippingSystem = prodVO.getShippingSystem();
			logger.info("shippingSystem:::"+shippingSystem);
		} catch (Exception e) {
			logger.error("DeliveryDateUtil - getXMLEncodedDeliveryDates() - Product Information cannot be retrieved.");
			logger.error(e);
		}	

        for(int i = 0; i < dates.size(); i++)
        {
            currentDate = (OEDeliveryDate) dates.get(i);
            if ( currentDate.getDeliverableFlag().equals("Y") )
            {
                entry = (Element) (document.createElement("deliveryDate"));
                entry.setAttribute("date", currentDate.getDeliveryDate());
                entry.setAttribute("dayOfWeek", currentDate.getDayOfWeek());
                // Loop through the delivery methods and concat them
                StringBuffer sbTypes = new StringBuffer();
                ShippingMethod shippingMethod = null;
                for (int d = 0; d < currentDate.getShippingMethods().size(); d++)
                {
                    shippingMethod = (ShippingMethod)currentDate.getShippingMethods().get(d);
                    sbTypes.append(shippingMethod.getCode()).append(":");
                    shippingMethods.put(shippingMethod.getCode(), shippingMethod);
                }
                entry.setAttribute("types", sbTypes.toString());

                // Pull out date ranges for florist deliverable dates
                // This will add duplicate dates to the list with different delivery
                // methods if the product is both florist and carrier delivered
                if(currentDate.isInDateRange())
                {
                    inRange = true;
                    if(firstRange)
                    {
                        // Store the first date in the range
                        rangeDate = currentDate;
                    }
                    firstRange = false;
                    dateRangeDisplayBuffer.append(currentDate.getDayOfWeek());
                    dateRangeDisplayBuffer.append(" ");
                    dateRangeDisplayBuffer.append(currentDate.getDisplayDate());
                    dateRangeDisplayBuffer.append(" or ");

                    // remove the florist ship method from the element
                    String types = entry.getAttribute("types");
                    types = FieldUtils.replaceAll(types, "FL:", "");
                    entry.setAttribute("types", types);
                }
                else if(inRange)
                {
                    entryRange = (Element) (document.createElement("deliveryDate"));
                    dateRangeDisplayBuffer.delete((dateRangeDisplayBuffer.length()-4), dateRangeDisplayBuffer.length());
                    entryRange.setAttribute("displayDate", dateRangeDisplayBuffer.toString());
                    entryRange.setAttribute("date",rangeDate.getDeliveryDate());
                    entryRange.setAttribute("dayOfWeek", "");
                    entryRange.setAttribute("types", "FL:");
                    entryRange.setAttribute("index", ("index" + i));
                    deliveryDates.appendChild(entryRange);
                    inRange = false;
                    firstRange = true;
                    rangeDate = null;
                    //issue 2479 & 2480 in MO - clear out the buffer
                    dateRangeDisplayBuffer.delete(0, dateRangeDisplayBuffer.length());
                }
                entry.setAttribute("displayDate", currentDate.getDisplayDate());
                entry.setAttribute("index", ("index" + i));

                // Do not append the date if no delivery methods exist
                if(entry.getAttribute("types").length() != 0)
                {
                    deliveryDates.appendChild(entry);
                }
            }
        }

        if ( shippingMethods.size() > 0 )
        {
            Element methodsElement = document.createElement("shippingMethods");
            root.appendChild(methodsElement);

            Element methodElement = document.createElement("shippingMethod");
            methodsElement.appendChild(methodElement);

            // only show Two Day delivery for Hawaii & Alaska
            OrderVO order = delParms.getOrder();
            OrderDetailsVO item = (OrderDetailsVO)order.getOrderDetail().get(delParms.getItemNumber() - 1);
            RecipientsVO recipient = (RecipientsVO)item.getRecipients().get(0);
            RecipientAddressesVO recipAddress = (RecipientAddressesVO)recipient.getRecipientAddresses().get(0);
            String stateId = recipAddress.getStateProvince();

            // Two Day delivery is only available delivery option for carrier delivered products
            // going to Alaska or Hawaii
            if  ( (stateId != null) &&
                  (stateId.equals(GeneralConstants.STATE_CODE_ALASKA) ||
                   stateId.equals(GeneralConstants.STATE_CODE_HAWAII)) &&
                  (delParms.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_SPEGFT) ||
                   delParms.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_FRECUT)) )
             {
                // Product support 2-day delivery, so output shipping methods
            	if(GeneralConstants.FTDW_SHIPPING_KEY.equalsIgnoreCase(shippingSystem) && GeneralConstants.OE_PRODUCT_TYPE_SPEGFT.equals(delParms.getProductType())){
            		generateShipMethods(shippingMethods,document,methodElement,entry);
            	}
            	else if (delParms.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_SPEGFT) &&
                     shippingMethods.containsKey(GeneralConstants.DELIVERY_TWO_DAY_CODE) )
                {
                    BigDecimal cost = (BigDecimal) ((ShippingMethod)shippingMethods.get(GeneralConstants.DELIVERY_TWO_DAY_CODE)).getDeliveryCharge();

                    entry = (Element) document.createElement("method");
                    entry.setAttribute("description", GeneralConstants.DELIVERY_TWO_DAY);
                    entry.setAttribute("code", GeneralConstants.DELIVERY_TWO_DAY_CODE);
                    String costValue = (null == cost) ? "" : cost.toString();
                    logger.info("costvalue::::"+costValue);
                    entry.setAttribute("cost", costValue);

                    methodElement.appendChild(entry);
                }
                // Fresh Cut (next-day only) or product that does not support 2-day delivery
                // so cannot be delivered to Alaska or Hawaii
                else
                {
                    delParms.setDisplayProductUnavailable(GeneralConstants.YES);
                }
            }
            else
            {
                // JMP - 8/22/03
                // SDG Project rules
                // For SDG products with more than just ND delivery show both florist
                // carrier delivery methods
                if(delParms.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_SDG) &&
                   (shippingMethods.containsKey(GeneralConstants.DELIVERY_STANDARD_CODE) ||
                    shippingMethods.containsKey(GeneralConstants.DELIVERY_TWO_DAY_CODE) ||
                    shippingMethods.containsKey(GeneralConstants.DELIVERY_SATURDAY_CODE)))
                {
                    // do nothing
                }
                // For SDFC and SDG products with ND delivery only show florist
                // if possible. If not then show ND delivery
                else if((delParms.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_SDFC) ||
                   delParms.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_SDG)) &&
                   (shippingMethods.containsKey(GeneralConstants.DELIVERY_FLORIST_CODE)))
                {
                    // Remove ND delivery
                    shippingMethods.remove(GeneralConstants.DELIVERY_NEXT_DAY_CODE);
                }

                Collection methodCollection = shippingMethods.keySet();
                Iterator iter = methodCollection.iterator();

                while ( iter.hasNext() )
                {
                    String code = (String) iter.next();

                    BigDecimal cost = (BigDecimal) ((ShippingMethod)shippingMethods.get(code)).getDeliveryCharge();
                    String description = ((ShippingMethod)shippingMethods.get(code)).getDescription();

                    entry = (Element) document.createElement("method");

                    // fresh cut prducts use Floral service charge instead of delivery charge
                    if ( delParms.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_FRECUT) )
                    {
                        entry.setAttribute("cost", "");
                    }
                    else
                    {
                        if(cost == null)
                        {
                            entry.setAttribute("cost", "");
                        }
                        else
                        {
                            entry.setAttribute("cost", cost.toString());
                        }
                    }
                    entry.setAttribute("description", description);
                    entry.setAttribute("code", code);
                    methodElement.appendChild(entry);
                }
            }

        }
        return document;
    }

    /**
     *
     * @param TimeZone object representing recipients time zone
     * @param ArrayList List of OEDeliveryDate objects
     * @return String representing XML for delivery dates
     * @throws ParserConfigurationException 
     */
    public Document getXMLEncodedCalendarDates(TimeZone timeZone, ArrayList dates, String productId) throws ParserConfigurationException
    {
        if(productId != null) productId = productId.toUpperCase();
        Document document = DOMUtil.getDefaultDocument();

        Element calendarDatesElement = (Element) document.createElement("calendarDates");
        Element calendarElement = null;
        Element dateElement = null;
        Element yearElement = null;

        document.appendChild(calendarDatesElement);

        ArrayList yearList = new ArrayList();

        if ( (productId != null) &&
             (!productId.trim().equals("")) )
        {
            Element productElement = (Element) document.createElement("product");
            calendarDatesElement.appendChild(productElement);

            Element dataElement = (Element) document.createElement("data");
            dataElement.setAttribute("productId", productId);
            productElement.appendChild(dataElement);
        }

        // get Calendar for current date
        Calendar currentDay = null;
        // Domestic delivery
        if ( timeZone != null )
        {
            currentDay = Calendar.getInstance(timeZone);
        }
        // International delivery
        else
        {
            currentDay = Calendar.getInstance();
        }

        int month = currentDay.get(Calendar.MONTH);
        month++;

        String todayDate = zeroPad(month) + "/" + zeroPad(currentDay.get(Calendar.DAY_OF_MONTH)) + "/" + currentDay.get(Calendar.YEAR);

        // output Calendar and Day nodes
        for ( int i=0; i < dates.size(); i++ )
        {
            OEDeliveryDate deliveryVO = (OEDeliveryDate) dates.get(i);

            // parse delivery date into Month, Day, and Year
            String stringDate = deliveryVO.getDeliveryDate();

            String dateDay = stringDate.substring(3,5);
            String dateMonth = stringDate.substring(0,2);
            String dateYear = stringDate.substring(6,10);

            // first time through, output Calendar root element
            if ( i ==0 )
            {
                calendarElement = (Element) document.createElement("calendar");
                calendarElement.setAttribute("month", String.valueOf(Integer.parseInt(dateMonth)));
                calendarElement.setAttribute("year", dateYear);
                calendarDatesElement.appendChild(calendarElement);
            }
            dateElement = this.makeCalendarDateElement(deliveryVO, document, dateDay, todayDate, deliveryVO.isInDateRange());
            calendarElement.appendChild(dateElement);
        }

        // output Years node
        int year = currentDay.get(Calendar.YEAR);
        for (int i=0; i < 2; i++)
        {
            yearList.add( String.valueOf(year++) );
        }

        // convert array to space delimited string
        StringBuffer years = new StringBuffer();
        for ( int i=0; i < yearList.size(); i++ )
        {
            years.append( (i > 0 ? " " : "") + yearList.get(i));
        }

        yearElement = (Element) document.createElement("years");
        yearElement.setAttribute("value", years.toString());
        calendarDatesElement.appendChild(yearElement);
        return document;
    }

    private Element makeCalendarDateElement(OEDeliveryDate deliveryVO, Document document, String dateDay, String todayDate, boolean dateRange)
    {
        String stringDate = deliveryVO.getDisplayDate();
        // output delivery date information each time
        Element dateElement = (Element) document.createElement("day");
        dateElement.setAttribute("value", String.valueOf(Integer.parseInt(dateDay)));
        dateElement.setAttribute("dayOfWeek", deliveryVO.getDayOfWeek());
        dateElement.setAttribute("isDeliverable", deliveryVO.getDeliverableFlag());

        // Loop through the delivery methods and concat them
        StringBuffer sbTypes = new StringBuffer();
        ShippingMethod shippingMethod = null;
        BigDecimal lowestCharge = new BigDecimal("999.99");
        for (int d = 0; d < deliveryVO.getShippingMethods().size(); d++)
        {
            shippingMethod = (ShippingMethod)deliveryVO.getShippingMethods().get(d);
            sbTypes.append(shippingMethod.getCode()).append(":");

            if(shippingMethod.getDeliveryCharge().compareTo(lowestCharge) == -1)
            {
                lowestCharge = shippingMethod.getDeliveryCharge();
            }
        }
        dateElement.setAttribute("types", sbTypes.toString());
        if(dateRange)
        {
            dateElement.setAttribute("range", "Y");
        }
        else
        {
            dateElement.setAttribute("range", "N");
        }

        if ( lowestCharge.compareTo(new BigDecimal("999.99")) != 0 )
        {
            dateElement.setAttribute("charge", lowestCharge.toString());
        }
        else
        {
            dateElement.setAttribute("charge", "0.00");
        }

        if ( stringDate.equals(todayDate) )
        {
            dateElement.setAttribute("currentDay", "Y");
        }

        // output Holiday Text and Date identifier if exists
        if ( !deliveryVO.getHolidayText().trim().equals("") )
        {
            dateElement.setAttribute("holiday", deliveryVO.getHolidayText());
        }

        return dateElement;
    }

   /**
     *
     * @see #aaa
     * @param int numeric representation of Time Zone
     * @return TimeZone object based on intput zip time zone
     */
    private TimeZone getTimeZone(OEDeliveryDateParm parms)
    {
        Calendar calendar = Calendar.getInstance();
        TimeZone tz = calendar.getTimeZone();
        TimeZone timeZone = null;

        // cutoff time for all Specialty Gift & Fresh Cut products is Central Time
        if ( (parms.getProductType() != null) &&
             (parms.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_FRECUT) ||
              parms.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_SPEGFT)) )
        {
            timeZone = tz.getTimeZone("America/Chicago");
        }
        else
        {
            switch ( Integer.parseInt(parms.getZipTimeZone()) )
            {
                case 0:     // international
                    timeZone = tz.getTimeZone("America/Chicago");
                    break;
                case 1:     // eastern
                    timeZone = tz.getTimeZone("America/New_York");
                    break;
                case 2:     // central
                    timeZone = tz.getTimeZone("America/Chicago");
                    break;
                case 3:     // mountain
                    timeZone = tz.getTimeZone("America/Denver");
                    break;
                case 4:     // pacific
                    timeZone = tz.getTimeZone("America/Los_Angeles");
                    break;
                case 5:     // hawaiian (alaska, hawaii)
                    timeZone = tz.getTimeZone("HST");
                    break;
                case 6:     // atlantic (puerto rico , virgin islands)
                    timeZone = tz.getTimeZone("America/Puerto_Rico");
                    break;
            }
        }
        return timeZone;
    }

    /**
     *
     * @param String number representation of month
     * @return int Calendar object numeric representation of month
     */
    private int getCalendarMonth(String argMonth)
    {
        int calMonth = 0;

        switch (Integer.parseInt(argMonth))
        {
            case 1:
                calMonth = Calendar.JANUARY;
                break;
            case 2:
                calMonth = Calendar.FEBRUARY;
                break;
            case 3:
                calMonth = Calendar.MARCH;
                break;
            case 4:
                calMonth = Calendar.APRIL;
                break;
            case 5:
                calMonth = Calendar.MAY;
                break;
            case 6:
                calMonth = Calendar.JUNE;
                break;
            case 7:
                calMonth = Calendar.JULY;
                break;
            case 8:
                calMonth = Calendar.AUGUST;
                break;
            case 9:
                calMonth = Calendar.SEPTEMBER;
                break;
            case 10:
                calMonth = Calendar.OCTOBER;
                break;
            case 11:
                calMonth = Calendar.NOVEMBER;
                break;
            case 12:
                calMonth = Calendar.DECEMBER;
                break;
        }

        return calMonth;
    }

    /**
     *
     * @param Date first date to compare
     & @param Date second date to compare
     * @return Returns a long value showing the number of days between the 2 dates
     */
     public static long getDateDiff (Date inFromDate, Date inToDate)
     {
        long diff = 0;

        // convert dates to same time of day (00:00:00) for better comparison as
        // number of milliseconds between dates can be less than 24 hours
        // which causes function to return 0 as difference
        // ex. 11/06/2002 23:59 & 11/07/2002 23:58 returns 0 as less than 24
        // hours as elapsed
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
            String fromDate = sdf.format(inFromDate);
            String toDate = sdf.format(inToDate);

            Date date1 = sdf.parse(fromDate);
            Date date2 = sdf.parse(toDate);

            diff = date2.getTime() - date1.getTime();
        }
        catch (Exception e)
        {
            diff = inToDate.getTime() - inFromDate.getTime();
        }
        float tmpFlt = ((float)diff / (float)(1000 * 60 * 60 * 24));
        String tmpStr = String.valueOf(tmpFlt);
        BigDecimal bd = new BigDecimal(tmpStr);
        int ret = bd.setScale(0, BigDecimal.ROUND_HALF_DOWN).intValue();
        return ret;
     }

    /**
     * This function checks to see if the Global Next Available Delivery Date (GNADD)
     * should be used.
     * @param OEDeliveryDateParm
     * @return boolean Flag showing whether GNADD is active for the product zip code
     */
     private boolean getGnaddSetting(OEDeliveryDateParm delParms)
     {
        OEParameters oeParms = delParms.getGlobalParms();
        boolean gnaddActive = false;

        // global GNADD flag is active for all products and florists
        if ( oeParms.getGNADDLevel().equals("3") )
        {
            gnaddActive = true;
        }
        // GNADD level 2 and no GoTo Florists covering zip code, GNADD active
        else if ( oeParms.getGNADDLevel().equals("2") &&
                  delParms.getZipCodeGotoFloristFlag().equals("N") )
        {
            gnaddActive = true;
        }
        // GNADD level 1, no florists available in zip code, GNADD active
        else if ( (oeParms.getGNADDLevel().equals("1")) &&
                  (delParms.getZipCodeGNADDFlag().equals("Y")) )
        {
            gnaddActive = true;
        }

        return gnaddActive;
     }



  /**
   * This method will determine what value will be used for the cutoff time.
   * The scope is set to package so that it can be unit tested.
   * @param domesticOrder Is this a domestic order
   * @param delParms OEDeliveryDateParm object
   * @param oeParms OEParameters object
   * @param cutoffDay Cutoff day
   * @param forceSpeGft Force the item to be a specialty gift
   * @param checkZipCutoff Does the zip cutoff have to be checked?
   * @return The correct cutoff time
   * @throws Exception
   */
  int getCutoffTime(boolean domesticOrder, OEDeliveryDateParm delParms, OEParameters oeParms, Calendar cutoffDay, boolean forceSpeGft, boolean checkZipCutoff) throws Exception
  {
    String cutoffTime = null;
    if (domesticOrder)
    {
      // Specialty Gift and Exotic cutoff times always take precedence
      //
      if (delParms.getProductSubType() != null && delParms.getProductSubType().equals(GeneralConstants.OE_PRODUCT_SUBTYPE_EXOTIC))
      {
        cutoffTime = oeParms.getExoticCutoff();
      }
      else if (delParms.getProductType() != null && delParms.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_SPEGFT))
      {
        cutoffTime = oeParms.getSpecialtyGiftCutoff();
      }
      // If none of special cases above, then (if applicable) get cutoff time based on
      // delivery zipcode or codified product cutoff for florist delivered products.
      // If not applicable, then fall back to global cutoff times based on day of week (or
      // certain product type cutoffs).  Note special handling for Sat and Sun with
      // respect to zipcode cutoff.
      //
      else
      {
        String zipCutoffTime = null;
        int zipCutoffTimeInt = -1;
        int cutoffTimeInt = -1;
        OrderVO order = delParms.getOrder();
        OrderDetailsVO item = (OrderDetailsVO)order.getOrderDetail().get(delParms.getItemNumber() - 1);
        RecipientsVO recipient = (RecipientsVO)item.getRecipients().get(0);
        RecipientAddressesVO recipAddress = (RecipientAddressesVO)recipient.getRecipientAddresses().get(0);
        String curCutoffProdId = item.getProductId();
        String curCutoffZip = recipAddress.getPostalCode();
        // For florist delivered products, get zipcode cutoff time (if available)
        //
        if (checkZipCutoff)
        {
          if (curCutoffZip == null)
          {
            zipCutoffTime = null;
          }
          else if (lastCutoffZip.equals(curCutoffZip) && lastCutoffProdId.equals(curCutoffProdId))
          {
            zipCutoffTime = lastCutoffTime;
          }
          else
          {
            /*
             * This stored proc has been removed and we should no longer get here (per Jira Q1SP17-10), 
             * so log an error if we do and set 2pm (1400) cutoff.
             */
            zipCutoffTime = "1400";
            logger.error("This code path is obsolete and should no longer be traversed (per Jira Q1SP17-10). This is a dark and dangerous place.");
            /*
            try
            {
              DataAccessUtil dau = DataAccessUtil.getInstance();
              DataRequest dataRequest = new DataRequest();
              Connection con = delParms.getConn();
              dataRequest.setConnection(con);
              HashMap inputParams = new HashMap();
              inputParams.put("IN_ZIP_CODE", curCutoffZip);
              inputParams.put("IN_PRODUCT_ID", curCutoffProdId);
              dataRequest.setInputParams(inputParams);
              dataRequest.setStatementID("GET_ZIP_CUTOFF");
              CachedResultSet rs = (CachedResultSet)dau.execute(dataRequest);
              if (rs.next())
              {
                zipCutoffTime = (String)rs.getObject(1);
                if ((zipCutoffTime != null) && (zipCutoffTime.length() == 0))
                {
                  zipCutoffTime = null;
                }
              }
              */
              lastCutoffProdId = curCutoffProdId;
              lastCutoffZip = curCutoffZip;
              lastCutoffTime = zipCutoffTime;
          }
          if (zipCutoffTime != null)
          {
            try
            {
              zipCutoffTimeInt = Integer.parseInt(zipCutoffTime);
            }
            catch (NumberFormatException nfe)
            {
              logger.error("getCutoffTime: Zip cutoff is non-numeric", nfe);
              zipCutoffTimeInt = -1;
            }
          }
        }
        // End if (checkZipCutoff...
        int dayOfWeek = cutoffDay.get(Calendar.DAY_OF_WEEK);
        // Use zipcode cutoff
        //
        if (zipCutoffTimeInt > -1)
        {
          if ((dayOfWeek != Calendar.SUNDAY) && (dayOfWeek != Calendar.SATURDAY))
          {
            cutoffTime = zipCutoffTime;
          }
          else
          {
            // For weekends, zipcode cutoff is ignored unless it specifies an
            // earlier cutoff time.
            //
            switch (dayOfWeek)
            {
            case Calendar.SUNDAY:
              cutoffTime = oeParms.getSundayCutoff();
              break;
            case Calendar.SATURDAY:
              cutoffTime = oeParms.getSaturdayCutoff();
              break;
            }
            try
            {
              cutoffTimeInt = Integer.parseInt(cutoffTime);
              if (zipCutoffTimeInt < cutoffTimeInt)
              {
                cutoffTime = zipCutoffTime;
              }
            }
            catch (NumberFormatException nfe)
            {
              logger.error("getCutoffTime: Sat or Sun cutoff is non-numeric", nfe);
            }
          }
        }
        else
        {
          switch (dayOfWeek)
          {
          case Calendar//
          .SUNDAY:
            cutoffTime = oeParms.getSundayCutoff();
            break;
          case Calendar.MONDAY:
            cutoffTime = oeParms.getMondayCutoff();
            break;
          case Calendar.TUESDAY:
            cutoffTime = oeParms.getTuesdayCutoff();
            break;
          case Calendar.WEDNESDAY:
            cutoffTime = oeParms.getWednesdayCutoff();
            break;
          case Calendar.THURSDAY:
            cutoffTime = oeParms.getThursdayCutoff();
            break;
          case Calendar.FRIDAY:
            cutoffTime = oeParms.getFridayCutoff();
            break;
          case Calendar.SATURDAY:
            cutoffTime = oeParms.getSaturdayCutoff();
            break;
          }
          // Other special cases (take precedence over day-of-week cutoff)
          //
          if (delParms.getProductType() != null && delParms.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_FRECUT))
          {
            cutoffTime = oeParms.getFreshCutCutoff();
          }
          else if ((delParms.getProductType() != null && (delParms.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_SDG) || delParms.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_SDFC)) && forceSpeGft))
          {
            cutoffTime = oeParms.getSpecialtyGiftCutoff();
          }
        }
        // end else if (zipCutoffTimeInt > -1)...
        GlobalParmHandler parmHandler = (GlobalParmHandler)CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_GLOBAL_PARM);
        String latestCutOffTimeParameter;
        if (parmHandler == null)
        {
          //For unit testing
          DataRequest dataRequest = new DataRequest();
          dataRequest.setConnection(delParms.getConn());
          dataRequest.setStatementID("GET_GLOBAL_PARAM");
          Map paramMap = new HashMap();
          paramMap.put("IN_CONTEXT", "VALIDATION_SERVICE");
          paramMap.put("IN_PARAM", "LATEST_CUTOFF");
          dataRequest.setInputParams(paramMap);
          DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
          latestCutOffTimeParameter = (String)dataAccessUtil.execute(dataRequest);
          dataRequest.reset();
        }
        else
        {
          latestCutOffTimeParameter = parmHandler.getGlobalParm("VALIDATION_SERVICE", "LATEST_CUTOFF");
        }
        if (useLatestCutoffTimeParameter(latestCutOffTimeParameter, cutoffTime))
        {
          cutoffTime = latestCutOffTimeParameter;
        }
      }
    }
    // end if (domesticOrder)...
    else
    {
      cutoffTime = oeParms.getIntlCutoff();
    }
    return Integer.parseInt(cutoffTime);
  }

  /**
   * Thsi function will determine if the latest or the computed cutoff time
   * should be used
   * @param latestCutOffTimeParameter - latest cutoff time
   * @param cutOffTime - computed cutoff time
   * @return true if the latest cutoff time should be used, else, false
   */
  private boolean useLatestCutoffTimeParameter(String latestCutOffTimeParameter, String cutOffTime)
  {
    boolean useLatestCutoff = false;
    if (latestCutOffTimeParameter != null && !latestCutOffTimeParameter.equals(""))
    {
      int latestCutoff = Integer.parseInt(latestCutOffTimeParameter);
      if (cutOffTime != null && !cutOffTime.equals(""))
      {
        int cutOff = Integer.parseInt(cutOffTime);
        if (latestCutoff < cutOff)
        {
          useLatestCutoff = true;
        }
      }
      else
      {
        useLatestCutoff = true;
      }
    }
    return useLatestCutoff;
  }


    /**
     *
     * This function left pads values that are less than 10 with zeros
     * @param int integer value that is to be left padded with 0's
      * @return string result of the padded value
     */
    private String zeroPad(int i)
    {
        String val = Integer.toString(i);
        return i < 10 ? "0" + val : val;
    }

    private String getDayOfWeek(int day)
    {
        String dayOfWeek = null;

        switch ( day )
        {
            case 1:
                dayOfWeek = "Sun";
                break;
            case 2:
                dayOfWeek = "Mon";
                break;
            case 3:
                dayOfWeek = "Tue";
                break;
            case 4:
                dayOfWeek = "Wed";
                break;
            case 5:
                dayOfWeek = "Thu";
                break;
            case 6:
                dayOfWeek = "Fri";
                break;
            case 7:
                dayOfWeek = "Sat";
                break;
        }

        return dayOfWeek;
    }

    /**
     *
     * @return OEParameters object that contains the values of the global settings
     */
    public void getDeliveryRanges(String dataType, Document data, OEDeliveryDateParm parms)
    {
        HashMap deliveryRangeFrom = new HashMap();
        HashMap deliveryRangeTo = new HashMap();
        HashMap deliveryRangeText = new HashMap();

        try
        {
            Element element = null;
            NodeList nl = DOMUtil.selectNodes(data, dataType + "/deliveryRangeData/data");

            if (nl.getLength() > 0)
            {
                for ( int y = 0; y < nl.getLength(); y++ )
                {
                    element = (Element) nl.item(y);

                    deliveryRangeFrom.put(String.valueOf(y), element.getAttribute("startDate"));
                    deliveryRangeTo.put(String.valueOf(y), element.getAttribute("endDate"));
                    deliveryRangeText.put(String.valueOf(y), element.getAttribute("instructionText"));
                }
            }
        }
        catch (Exception e)
        {
            logger.error(e);
        }

        if ( deliveryRangeFrom.size() > 0 )
        {
            parms.setDeliveryRangeFlag(Boolean.TRUE);
            parms.setDeliveryRangeFrom(deliveryRangeFrom);
            parms.setDeliveryRangeTo(deliveryRangeTo);
            parms.setDeliveryRangeText(deliveryRangeText);
        }
        else
        {
            parms.setDeliveryRangeFlag(Boolean.FALSE);
        }
    }

    private OEDeliveryDate floristDeliverableDate(OEDeliveryDate dateVO, OEDeliveryDateParm delParms, Calendar calendar, Calendar currentDay)
    {
        boolean isDeliverable = true;
        boolean gnaddActive = false;
        boolean domesticOrder = false;
        OrderVO order = delParms.getOrder();
        OrderDetailsVO item = (OrderDetailsVO)order.getOrderDetail().get(delParms.getItemNumber() - 1);
        RecipientsVO recipient = (RecipientsVO)item.getRecipients().get(0);
        RecipientAddressesVO recipAddress = (RecipientAddressesVO)recipient.getRecipientAddresses().get(0);
        MercentOrderPrefixes mrcnOrderPrefixes = new MercentOrderPrefixes(delParms.getConn());
        // Domestic order, set time zone of calendar
        if ( recipAddress.getInternational() != null && recipAddress.getInternational().equals("N") )
        {
            domesticOrder = true;
        }
        Calendar today = Calendar.getInstance(calendar.getTimeZone());
        if(testMode)
              today.set(testYear, testMonth, testDay, testHour, testMinute);

        Calendar intlGNADDDay = (Calendar) today.clone();
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        Date date = null;
        try
        {
            date = sdf.parse(dateVO.getDeliveryDate());
        }
        catch(Exception e)
        {
            logger.error(e);
            return null;
        }

        int addonDays = 0;

        // Check for codified products.  Do not allow delivery if the product
        // is codified and not deliverable
        if(delParms.getCodifiedProduct() != null && delParms.getCodifiedProduct().equals("Y") &&
           delParms.getCodifiedSpecialFlag() != null && !delParms.getCodifiedSpecialFlag().equals("Y") &&
           delParms.getCodifiedAvailable() != null && delParms.getCodifiedAvailable().equals("N"))
        {
            isDeliverable = false;
        }



        // set GNADD date for country if not domestic order
        if ( !domesticOrder )
        {
            // use whichever is farther out
            if ( delParms.getGlobalParms().getIntlAddOnDays() > delParms.getCntryAddOnDays() )
            {
                addonDays = delParms.getGlobalParms().getIntlAddOnDays();
            }
            else
            {
                addonDays = delParms.getCntryAddOnDays();
            }
            intlGNADDDay.add(Calendar.DATE, addonDays);
        }

        // if the product is exotic then add one day to shipping
        if(delParms.getProductSubType().equals(GeneralConstants.OE_PRODUCT_SUBTYPE_EXOTIC))
        {
            addonDays += 1;
        }

        // check to see if domestic floral GNADD is in effect
        if ( domesticOrder )
        {
            gnaddActive = getGnaddSetting(delParms);
        }

        // check to see if GNADD is active for the passed zip code
        Date gnaddDate = delParms.getGlobalParms().getGNADDDate();
        if(gnaddDate != null)
        {
            long gnaddDiff = getDateDiff(date, gnaddDate);
            if ( gnaddActive && gnaddDiff > 0 )
            {
                    isDeliverable = false;
            }
        }

        // Set the times equal and just compare the dates
        intlGNADDDay.set(Calendar.MINUTE, currentDay.get(Calendar.MINUTE));
        intlGNADDDay.set(Calendar.SECOND, currentDay.get(Calendar.SECOND));
        intlGNADDDay.set(Calendar.MILLISECOND, currentDay.get(Calendar.MILLISECOND));

        // Check for International GNADD date
        if ( isDeliverable &&
             !domesticOrder &&
             (currentDay.before(intlGNADDDay)) )
        {
            isDeliverable = false;
        }

        // Check for days that are not deliverable
        if (isDeliverable && (currentDay.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY))
        {
            // International floral orders do not allow Sunday delivery
            if(!domesticOrder || (delParms.isSundayDelivery() == Boolean.FALSE))
            {
                isDeliverable = false;
            }
        }

        if(isDeliverable)
        {
            if(domesticOrder && !delParms.getProductSubType().equals(GeneralConstants.OE_PRODUCT_SUBTYPE_EXOTIC))
            {
                // For domestic orders
                // get the number of days between the today and process date to
                // use for comparisons.
                long dateDiff = getDateDiff(today.getTime(), currentDay.getTime());
                if ( dateDiff == 0 )
                {
                    // format the current and cutoff times for comparison
                    int currentTime = Integer.parseInt(zeroPad(currentDay.get(Calendar.HOUR_OF_DAY)) + zeroPad(currentDay.get(Calendar.MINUTE)));

                    int cutoffTime = -1;
                    try{
                      cutoffTime = getCutoffTime(domesticOrder, delParms, delParms.getGlobalParms(), currentDay, false,true);
                    }
                    catch(Exception e)
                    {
                      logger.error("Cutoff time could not be determined.");
                      logger.error(e);
                    }

                    if ( (currentTime < cutoffTime) )
                    {
                        // Make sure product supports same day delivery (floral only)
                        if ( isDeliverable && !delParms.getDeliverTodayFlag().booleanValue() )
                        {
                            isDeliverable = false;
                            delParms.setDeliverTodayFlag(Boolean.FALSE);
                        }
                    }
                    else
                    {
                        // Past cutoff so cannot deliver today
                        isDeliverable = false;
                    }

                    // Funeral Occasion - override same day delivery to available

                    if (!order.getOrderOrigin().equalsIgnoreCase(GeneralConstants.ORDER_ORIGIN_AMAZON) && 
                    		!mrcnOrderPrefixes.isMercentOrder(order.getOrderOrigin())
                            && delParms.getOccasion() != null && delParms.getOccasion().equals(GeneralConstants.OE_OCCASION_FUNERAL) )
                    {
                        isDeliverable = true;
                        delParms.setDeliverTodayFlag(Boolean.TRUE);
                    }
                }
            }
            else
            {
                // For international and exotic floral orders we check for cutoff on current day + addon days
                // Get the number of days between the today and process date to
                // use for comparisons.
                long dateDiff = getDateDiff(today.getTime(), currentDay.getTime());
                if ( dateDiff == addonDays )
                {
                    // format the current and cutoff times for comparison
                    int currentTime = Integer.parseInt(zeroPad(currentDay.get(Calendar.HOUR_OF_DAY)) + zeroPad(currentDay.get(Calendar.MINUTE)));

                    int cutoffTime = -1;
                    try{
                      cutoffTime = getCutoffTime(domesticOrder, delParms, delParms.getGlobalParms(), currentDay, false,false);
                    }
                    catch(Exception e)
                    {
                      logger.error("Cutoff time could not be determined.");
                      logger.error(e);
                    }

                    if(currentTime < cutoffTime)
                    {
                        // Can deliver this day
                    }
                    else
                    {
                        // Past cutoff so cannot deliver today
                        isDeliverable = false;
                    }
                }
                else if(dateDiff == 0)
                {
                    // Cannot delivery today because same day is not available
                    // to exotic
                    isDeliverable = false;
                }
            }
        }

        if(isDeliverable)
        {
            ShippingMethod shippingMethod = new ShippingMethod();
            shippingMethod.setDescription(GeneralConstants.DELIVERY_FLORIST);
            shippingMethod.setCode(GeneralConstants.DELIVERY_FLORIST_CODE);
            shippingMethod.setDeliveryCharge(delParms.getFloralServiceCharge());

            dateVO.getShippingMethods().add(shippingMethod);
        }

        return dateVO;
    }


    private OEDeliveryDate carrierDeliverableDate(OEDeliveryDate dateVO, OEDeliveryDateParm delParms,
        Calendar calendar, Calendar currentDay, ProductVO prodVO, HashMap vendorInfo)
    {
        boolean isDeliverable = true;

        boolean domesticOrder = false;
        OrderVO order = delParms.getOrder();
        OrderDetailsVO item = (OrderDetailsVO)order.getOrderDetail().get(delParms.getItemNumber() - 1);
        RecipientsVO recipient = (RecipientsVO)item.getRecipients().get(0);
        RecipientAddressesVO recipAddress = (RecipientAddressesVO)recipient.getRecipientAddresses().get(0);

        // Domestic order, set time zone of calendar
        if (recipAddress.getInternational() == null || !recipAddress.getInternational().equals("Y"))
        {
            domesticOrder = true;
        }

        Calendar today = Calendar.getInstance(calendar.getTimeZone());
        if(testMode)
              today.set(testYear, testMonth, testDay, testHour, testMinute);

        boolean standardAvailable = true;
        boolean secondDayAvailable = true;
        boolean nextDayAvailable = true;
        boolean saturdayAvailable = true;
        HashMap shipMethods = null;
        int saturdayTransitTime = 2; // Day

        // JMP - 8/22/03 - We need to be able to show carrier delivery according to
        // page 3 of the spec
        //Check for codified products.  Do not allow carrier delivery if the product
        // is codified, available from a florist and GNADD is not active
        //if(delParms.getCodifiedProduct() != null && delParms.getCodifiedProduct().equals("Y") &&
        //   delParms.getCodifiedSpecialFlag() != null && !delParms.getCodifiedSpecialFlag().equals("Y") &&
        //   delParms.getCodifiedAvailable() != null && !delParms.getCodifiedAvailable().equals("N") &&
        //   delParms.isShipMethodFlorist() && (getGnaddSetting(delParms) == false))
        //{
        //    isDeliverable = false;
        //}

        // Make currentDay Calendar have a timezone of CST
        currentDay.setTimeZone(currentDay.getTimeZone().getTimeZone("America/Chicago"));
      //  logger.info("delParms isVendorNoDeliveryFlag: " +  delParms.isVendorNoDeliverFlag() );
     //   logger.info("delparms getVendorNoDeliveryForm: " +  delParms.getVendorNoDeliverFrom() );

        // check if vendor has blocked product from delivery on current day
        if ( isDeliverable &&
             (delParms.isVendorNoDeliverFlag() != null) &&
             (delParms.isVendorNoDeliverFlag().booleanValue()) )
        {
            HashMap noDeliverFromMap = delParms.getVendorNoDeliverFrom();
            HashMap noDeliverToMap = delParms.getVendorNoDeliverTo();

            for ( int y = 0; y < noDeliverFromMap.size(); y++ )
            {
                Date noDeliverFrom = (Date) noDeliverFromMap.get(String.valueOf(y));
                Date noDeliverTo = (Date) noDeliverToMap.get(String.valueOf(y));

                if ( (getDateDiff(noDeliverFrom, currentDay.getTime()) > -1) &&
                     (getDateDiff(noDeliverTo, currentDay.getTime()) < 1) )
                {
                    isDeliverable = false;
                }
            }
        }

        // skip if date is Sunday
        if (isDeliverable &&
            (currentDay.get(Calendar.DAY_OF_WEEK)== Calendar.SUNDAY))
        {
            isDeliverable = false;
        }

        // Get product ship methods
        if ( delParms.getShipMethods() != null )
        {
            shipMethods = delParms.getShipMethods();
        }

        // skip if date is Saturday and Saturday delivery is not allowed
        //if (isDeliverable &&
        //    (currentDay.get(Calendar.DAY_OF_WEEK)== Calendar.SATURDAY) &&
        //    (delParms.isSaturdayDelivery() != null) &&
        //    (!delParms.isSaturdayDelivery().booleanValue()))
        //{
        //    isDeliverable = false;
        //}

        // Check for shipping restrictions
        if(isDeliverable)
        {
          Calendar standardShipDate = getShipDate(GeneralConstants.DELIVERY_STANDARD_CODE, today, currentDay, delParms,standardTransitTime);
          Calendar secondDayShipDate = getShipDate(GeneralConstants.DELIVERY_TWO_DAY_CODE, today, currentDay, delParms, secondDayTransitTime);
          Calendar nextDayShipDate = getShipDate(GeneralConstants.DELIVERY_NEXT_DAY_CODE, today, currentDay, delParms, nextDayTransitTime);

          // Check to make sure each method's ship day is greater than right now
          // format the current and cutoff times for comparison
          int currentTime = Integer.parseInt(zeroPad(currentDay.get(Calendar.HOUR_OF_DAY)) + zeroPad(currentDay.get(Calendar.MINUTE)));
          int cutoffTime = -1;
          try
          {
            if (prodVO != null &&
                StringUtils.isNotEmpty(prodVO.getShippingSystem()) &&
                StringUtils.equalsIgnoreCase(prodVO.getShippingSystem(), "SDS"))
            {
              String sCutoffTime = (String)vendorInfo.get("cutoff");
              if (StringUtils.isNotEmpty(sCutoffTime))
                cutoffTime = Integer.valueOf(sCutoffTime).intValue();
              else
              {
                logger.error("The cutoff time is empty");
              }
            }
            else
            {
              cutoffTime = getCutoffTime(domesticOrder, delParms, delParms.getGlobalParms(), currentDay, true,false);
            }
          }
          catch(Exception e)
          {
            logger.error("DeliveryDateUtil - carrierDeliverableDate() - Cutoff time could not be determined.");
            logger.error(e);
          }


            // Alaska and Hawaii must use two day shipping
            RecipientsVO recipientVO = (RecipientsVO)item.getRecipients().get(0);
            recipAddress = (RecipientAddressesVO)recipientVO.getRecipientAddresses().get(0);
            String orderState = recipAddress.getStateProvince();
            long dateDiff = getDateDiff(today.getTime(), currentDay.getTime());
            if(orderState != null && (orderState.equals(GeneralConstants.STATE_CODE_ALASKA) ||
               orderState.equals(GeneralConstants.STATE_CODE_HAWAII)))
            {
               saturdayTransitTime = secondDayTransitTime;
            }
            // If Saturday is closer than 2 days the do Next Day Delivery
            else if(dateDiff == 1)
            {
                saturdayTransitTime = nextDayTransitTime;
            }
            //Saturday is always 1 day transit for freshcuts/sameday freshcuts
            else if(dateDiff >0 && (
                delParms.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_FRECUT) ||
                delParms.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_SDFC)) )
            {
                saturdayTransitTime = nextDayTransitTime;
            }
            else if( dateDiff==2 &&
                delParms.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_SPEGFT) &&
                currentDay.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY && currentTime > cutoffTime )
            {
                saturdayTransitTime = nextDayTransitTime;
            }

            Calendar saturdayShipDate = getShipDate(GeneralConstants.DELIVERY_SATURDAY_CODE, today, currentDay, delParms, saturdayTransitTime);


            dateDiff = getDateDiff(today.getTime(), standardShipDate.getTime());
            if (dateDiff < 0 || (dateDiff == 0  && currentTime > cutoffTime))
            {
                standardAvailable = false;
            }

            dateDiff = getDateDiff(today.getTime(), secondDayShipDate.getTime());
            if (dateDiff < 0 || (dateDiff == 0  && currentTime > cutoffTime))
            {
                secondDayAvailable = false;
            }

            dateDiff = getDateDiff(today.getTime(), nextDayShipDate.getTime());
            if (dateDiff < 0 || (dateDiff == 0  && currentTime > cutoffTime))
            {
                nextDayAvailable = false;
            }

            dateDiff = getDateDiff(today.getTime(), saturdayShipDate.getTime());
            if (dateDiff < 0 || (dateDiff == 0  && currentTime > cutoffTime))
            {
                saturdayAvailable = false;
            }

            // If the difference between the ship date and the current date is
            // more than one day for Next Day delivery, do not allow the delivery
            // of this product on the current date (since we're assuming product
            // cannot be in transit more than one day).
            // Exception is friday or saturday ship for monday delivery.
            // Note this was changed (Jan 05) - it used to apply to cases where
            // Next Day was the ONLY delivery method - now it applies to cases
            // where there may be other delivery methods in addition to Next Day.
            dateDiff = getDateDiff(nextDayShipDate.getTime(), currentDay.getTime());
            if(shipMethods != null && shipMethods.containsKey(GeneralConstants.DELIVERY_NEXT_DAY) &&
               !((nextDayShipDate.get(Calendar.DAY_OF_WEEK)==Calendar.FRIDAY ||
                  nextDayShipDate.get(Calendar.DAY_OF_WEEK)==Calendar.SATURDAY) &&
                    currentDay.get(Calendar.DAY_OF_WEEK)==Calendar.MONDAY) &&
               dateDiff > 1)
            {
                nextDayAvailable = false;
            }
        }
        if(shipMethods == null || !shipMethods.containsKey(GeneralConstants.DELIVERY_STANDARD))
        {
            standardAvailable = false;
        }

        if(shipMethods == null || !shipMethods.containsKey(GeneralConstants.DELIVERY_TWO_DAY))
        {
            secondDayAvailable = false;
        }

        if(shipMethods == null || !shipMethods.containsKey(GeneralConstants.DELIVERY_NEXT_DAY))
        {
            nextDayAvailable = false;
        }

        if(shipMethods == null || !shipMethods.containsKey(GeneralConstants.DELIVERY_SATURDAY))
        {
            saturdayAvailable = false;
        }
        // if the delivery state is AK or HI then only 2nd day delivery is available
        recipient = (RecipientsVO)item.getRecipients().get(0);
        recipAddress = (RecipientAddressesVO)recipient.getRecipientAddresses().get(0);
        String recipState = recipAddress.getStateProvince();
        if(recipState != null &&
           (recipState.equals(GeneralConstants.STATE_CODE_ALASKA) ||
            recipState.equals(GeneralConstants.STATE_CODE_HAWAII)))
        {
            standardAvailable = false;
            nextDayAvailable = false;
            saturdayAvailable = false;
        }
        // if all are false then not deliverable by carrier

        if(!standardAvailable && !secondDayAvailable && !nextDayAvailable && !saturdayAvailable)
        {
            isDeliverable = false;
        }

        
        if(isDeliverable && standardAvailable && currentDay.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY)
        {
            ShippingMethod shippingMethod = new ShippingMethod();
            shippingMethod.setDescription(GeneralConstants.DELIVERY_STANDARD);
            shippingMethod.setCode(GeneralConstants.DELIVERY_STANDARD_CODE);
            shippingMethod.setDeliveryCharge(((ShippingMethod)delParms.getShipMethods().get(GeneralConstants.DELIVERY_STANDARD)).getDeliveryCharge());

            dateVO.getShippingMethods().add(shippingMethod);
        }
        if(isDeliverable && secondDayAvailable && currentDay.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY)
        {
            ShippingMethod shippingMethod = new ShippingMethod();
            shippingMethod.setDescription(GeneralConstants.DELIVERY_TWO_DAY);
            shippingMethod.setCode(GeneralConstants.DELIVERY_TWO_DAY_CODE);
            shippingMethod.setDeliveryCharge(((ShippingMethod)delParms.getShipMethods().get(GeneralConstants.DELIVERY_TWO_DAY)).getDeliveryCharge());

            dateVO.getShippingMethods().add(shippingMethod);
        }
        if(isDeliverable && nextDayAvailable && currentDay.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY)
        {
            ShippingMethod shippingMethod = new ShippingMethod();
            shippingMethod.setDescription(GeneralConstants.DELIVERY_NEXT_DAY);
            shippingMethod.setCode(GeneralConstants.DELIVERY_NEXT_DAY_CODE);
            shippingMethod.setDeliveryCharge(((ShippingMethod)delParms.getShipMethods().get(GeneralConstants.DELIVERY_NEXT_DAY)).getDeliveryCharge());

            dateVO.getShippingMethods().add(shippingMethod);
        }
        if(isDeliverable && saturdayAvailable && currentDay.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY)
        {
            ShippingMethod shippingMethod = new ShippingMethod();
            shippingMethod.setDescription(GeneralConstants.DELIVERY_SATURDAY);
            shippingMethod.setCode(GeneralConstants.DELIVERY_SATURDAY_CODE);
            shippingMethod.setDeliveryCharge(((ShippingMethod)delParms.getShipMethods().get(GeneralConstants.DELIVERY_SATURDAY)).getDeliveryCharge());

            dateVO.getShippingMethods().add(shippingMethod);
        }
        return dateVO;
    }


    /**
     * A public method for getting the ship date based on the delivery date
     * and ship method
     * @author Jeff Penney
     *
     * Overloading getShipDate method.
     *
     */
    public String getShipDate(String shipMethod, String deliveryDate,
                              String recipState, String recipCountry,
                              String productId, Connection con)
                  throws SQLException, IOException, ParseException,
                  ParserConfigurationException, SAXException
      {

        OEDeliveryDateParm parms = new OEDeliveryDateParm();

        return getShipDate(parms,shipMethod,deliveryDate,
                              recipState,recipCountry,productId,con);
      }


    /**
     * A public method for getting the ship date based on the delivery date
     * and ship method
     * @author Jeff Penney
     *
     */
    public String getShipDate(OEDeliveryDateParm parms,String shipMethod, String deliveryDate,
                              String recipState, String recipCountry,
                              String productId, Connection con)
                  throws SQLException, IOException, ParseException,
                  ParserConfigurationException, SAXException

    {

        if(productId != null) productId = productId.toUpperCase();
        String shipDate = null;
        Calendar calShipDate = null;
        Calendar today = null;
        Calendar currentDay = null;

        int transitDays = 0;
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");

        today = Calendar.getInstance();
        currentDay = Calendar.getInstance();
        currentDay.setTime(sdf.parse(deliveryDate));



        // Set holiday dates
        this.getHolidayDates(parms, this.getCountryHolidays(recipCountry, con));

        // Set vendor no ship dates
        this.getVendorNoShipDates(parms, productId, con);

        // Set vendor no ship days
        this.getVendorNoShipDays(parms, productId, con);

        if(shipMethod.equals(GeneralConstants.DELIVERY_STANDARD_CODE))
        {
            transitDays = this.standardTransitTime;
        }
        else if(shipMethod.equals(GeneralConstants.DELIVERY_TWO_DAY_CODE))
        {
            transitDays = this.secondDayTransitTime;
        }
        else if(shipMethod.equals(GeneralConstants.DELIVERY_NEXT_DAY_CODE))
        {
            transitDays = this.nextDayTransitTime;
        }
        else if(shipMethod.equals(GeneralConstants.DELIVERY_SATURDAY_CODE))
        {
            logger.info("Saturday delivery...");
            Calendar todaysDateAdjusted = today;

            //if past cutoff add 1 to the currentDay
            HashMap vendorCutoffInfo = (HashMap) retrieveVendorCutoffInfo(con, productId);
            String sPastVendorCutoff = (String)vendorCutoffInfo.get("past_cutoff");
            boolean bPastCutoff = new Boolean(sPastVendorCutoff).booleanValue();

            if(bPastCutoff)
            {
                todaysDateAdjusted.add(Calendar.DATE,1);
            }


            // Alaska and Hawaii must use two day shipping
            long dateDiff = getDateDiff(todaysDateAdjusted.getTime(), currentDay.getTime());

            if(recipState != null && (recipState.equals(GeneralConstants.STATE_CODE_ALASKA) ||
               recipState.equals(GeneralConstants.STATE_CODE_HAWAII)))
            {
               transitDays = secondDayTransitTime;
            }
            // If Saturday is closer than 2 days the do Next Day Delivery
            else if(dateDiff == 1)
            {
                transitDays = nextDayTransitTime;
            }
            //Freshcuts need to be sent out with only 1 day in transit
            else if(parms.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_FRECUT) ||
                        parms.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_SDFC) )
            {
                transitDays = nextDayTransitTime;
            }
            //Don't automatically update to two-day shipping if the product is not codified as such
            else if(parms.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_SPEGFT) &&
                        !isShipMethodAvailable(productId,GeneralConstants.DELIVERY_TWO_DAY_CODE,con) )
            {
                transitDays = 1;
            }
            else
            {
                transitDays = 2;
            }
        }


        calShipDate = this.getShipDate(shipMethod, today, currentDay, parms, transitDays);

        shipDate = zeroPad(calShipDate.get(Calendar.MONTH) + 1) + "/" + zeroPad(calShipDate.get(Calendar.DAY_OF_MONTH)) + "/" + calShipDate.get(Calendar.YEAR);


        return shipDate;
    }

    /*
     * Gets the ship day for a delivery method
     */
     private Calendar getShipDate(String shipMethod, Calendar today, Calendar currentDay, OEDeliveryDateParm delParms, int minTransitDays)
     {
    	SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
    	logger.debug("today: " + sdf.format(today.getTime()) +
    			" currentDay: " + sdf.format(currentDay.getTime()) +
    			" shipMethod: " + shipMethod +
    			" minTransitDays: " + minTransitDays);

        Calendar calTmp = (Calendar)currentDay.clone();
        String currentDateStr = zeroPad(currentDay.get(Calendar.MONTH) + 1) + "/" + zeroPad(currentDay.get(Calendar.DAY_OF_MONTH)) + "/" + currentDay.get(Calendar.YEAR);
        OEDeliveryDate holidayDate = null;

        // check to see if date is a Holiday date
        if (delParms.getHolidayDates() != null )
        {
            holidayDate = (OEDeliveryDate) delParms.getHolidayDates().get(currentDateStr);
        }

        // This section moves the ship date back the specified number of
        // transit days.
        if(minTransitDays > 0)
        {
            rollCalendarBack(calTmp);
            // decrement transit days if current date is not a weekend
            if((currentDay.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY ||
                    shipMethod.equals(GeneralConstants.DELIVERY_SATURDAY_CODE)) &&
                currentDay.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY)
            {
                minTransitDays--;
            }
            logger.debug("getShipDate(1)");
            calTmp = getShipDate(shipMethod, today, calTmp, delParms, minTransitDays);
        }

        // Once we have moved back the minimum transit days then check for
        // no ship days and vendor no ship dates.
        if ( (delParms.isVendorNoShipFlag() != null) &&
             (delParms.isVendorNoShipFlag().booleanValue()) )
        {
            HashMap noShipFromMap = delParms.getVendorNoShipFrom();
            HashMap noShipToMap = delParms.getVendorNoShipTo();

            for ( int y = 0; y < noShipFromMap.size(); y++ )
            {
                Date noShipFrom = (Date) noShipFromMap.get(String.valueOf(y));
                Date noShipTo = (Date) noShipToMap.get(String.valueOf(y));

                if ( (getDateDiff(noShipFrom, calTmp.getTime()) > -1) &&
                     (getDateDiff(noShipTo, calTmp.getTime()) < 1) )
                {
                    rollCalendarBack(calTmp);
                    logger.debug("getShipDate(2)");
                    calTmp = getShipDate(shipMethod, today, calTmp, delParms, 0);
                }
            }
        }

        // check if product cannot be shipped for delivery current day
        if ( delParms.getNoShipDays() != null )
        {
            //Ignore the Friday noship day for Saturday deliveries
            if( shipMethod.equals(GeneralConstants.DELIVERY_SATURDAY_CODE) &&
                calTmp.get(Calendar.DAY_OF_WEEK)==Calendar.FRIDAY )
            {
                if (logger.isDebugEnabled()) {
                    logger.info("Overriding Friday \"No Ship Day\" for Saturday delivery");
                }
            }
            else if ( delParms.getNoShipDays().contains(new Integer(calTmp.get(Calendar.DAY_OF_WEEK))))
            {
                rollCalendarBack(calTmp);
                logger.debug("getShipDate(3)");
                calTmp = getShipDate(shipMethod, today, calTmp, delParms, minTransitDays);
            }
        }

        // Check for shipping on holidays
        long diff = this.getDateDiff(calTmp.getTime(), currentDay.getTime());
        if((holidayDate != null && holidayDate.getShippingAllowed().equals("N")) && diff == 0)
        {
            rollCalendarBack(calTmp);
            logger.debug("getShipDate(4)");
            calTmp = getShipDate(shipMethod, today, calTmp, delParms, 0);
        }
        
        logger.debug("Ship Date returned: " + sdf.format(calTmp.getTime()));
        return calTmp;
     }

    private void rollCalendarBack(Calendar calendar)
    {
        if((calendar.get(Calendar.MONTH) - 1) < 0 && (calendar.get(Calendar.DATE) - 1) < 1)
        {
            // roll back the year too
            calendar.roll(Calendar.YEAR, false);
        }

        if((calendar.get(Calendar.DATE) - 1) < 1)
        {
            // roll back the month too
            calendar.roll(Calendar.MONTH, false);
        }

        calendar.roll(Calendar.DATE, false);
    }


//    private void printCalendar(Calendar calendar)
//    {
//        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:SS");
//        sdf.setCalendar(calendar);
//        String str = sdf.format(calendar.getTime());
//        System.out.println(str);
//    }


    private static void getVendorNoShipDates(OEDeliveryDateParm parms, String productId, Connection con)
                        throws SAXException, ParserConfigurationException, IOException,
                        SQLException, ParseException
    {
        DataAccessUtil dau = DataAccessUtil.getInstance();
        DataRequest dataRequest = new DataRequest();

        dataRequest.setStatementID(GET_VENDOR_NO_SHIP_DATES);
        dataRequest.setConnection(con);
        dataRequest.addInputParam(PRODUCT_ID, productId);
        CachedResultSet rs = (CachedResultSet)dau.execute(dataRequest);
        dataRequest.reset();

        Map shipFromMap = new HashMap();
        Map shipToMap = new HashMap();
        Date startDate = null;
        Date endDate = null;
        int count = 0;

        while(rs.next())
        {
            startDate = (Date)rs.getObject(1);
            endDate = (Date)rs.getObject(2);

            shipFromMap.put(String.valueOf(count), startDate);
            shipToMap.put(String.valueOf(count), endDate);

            count++;
        }

        if(count > 0)
        {
            parms.setVendorNoShipFlag(Boolean.TRUE);
            parms.setVendorNoShipFrom((HashMap)shipFromMap);
            parms.setVendorNoShipTo((HashMap)shipToMap);
        }
        else
        {
            parms.setVendorNoShipFlag(Boolean.FALSE);
        }
    }

    /**
   *
   * @param parms OEDeliveryDateParm object that holds all the information needed to derive delivery dates
   * @param productVendor Vendor information
   * @param con Connection to the database
   */
    //Accesses the FTD_APPS table retrieves a vendors delviery restirctions
    private static void getVendorDeliveryRestrictions(OEDeliveryDateParm parms, String productId, Connection con )
                        throws IOException, ParserConfigurationException, SAXException,
                        ParseException, SQLException
    {
        DataAccessUtil dau = DataAccessUtil.getInstance();
        DataRequest dataRequest = new DataRequest();

        dataRequest.setStatementID(GET_VENDOR_DELIVERY_RESTRICT);
        dataRequest.setConnection(con);
        dataRequest.addInputParam(PRODUCT_ID, productId);
        CachedResultSet resultSet = (CachedResultSet)dau.execute(dataRequest);
        dataRequest.reset();


        Map deliveryFromMap = new HashMap();
        Map deliveryToMap = new HashMap();

        Date startDate = null;
        Date endDate = null;
        int count = 0;

        while( resultSet.next() )
        {
            logger.info("ResultSet Size: " + resultSet.getRowCount());
            logger.info("Start Date value : " + resultSet.getObject(1));
            logger.info("End Date value : " + resultSet.getObject(2));

            startDate = (Date)resultSet.getObject(1);
            endDate  = (Date)resultSet.getObject(2);

            deliveryFromMap.put(String.valueOf(count), startDate);
            deliveryToMap.put(String.valueOf(count), endDate);
            count++;
        }

        if( count > 0 )
        {
          parms.setVendorNoDeliverFlag(Boolean.TRUE);
          parms.setVendorNoDeliverFrom((HashMap)deliveryFromMap);
          parms.setVendorNoDeliverTo((HashMap)deliveryToMap);
        }
        else
        {
          parms.setVendorNoDeliverFlag(Boolean.FALSE);
        }
    }

    private static void getVendorNoShipDays(OEDeliveryDateParm parms, String productId, Connection con)
                        throws SAXException, ParserConfigurationException, IOException,
                        SQLException, ParseException
    {
        if(productId != null) productId = productId.toUpperCase();
        DataAccessUtil dau = DataAccessUtil.getInstance();
        DataRequest dataRequest = new DataRequest();

        dataRequest.setStatementID(GET_VENDOR_NO_SHIP_DAYS);
        dataRequest.setConnection(con);
        dataRequest.addInputParam(PRODUCT_ID, productId);
        CachedResultSet rs = (CachedResultSet)dau.execute(dataRequest);
        dataRequest.reset();

        String mondayFlag = null;
        String tuesdayFlag = null;
        String wednesdayFlag = null;
        String thursdayFlag = null;
        String fridayFlag = null;
        String saturdayFlag = null;
        String sundayFlag = null;
        HashSet days = new HashSet();

        while(rs.next())
        {
            mondayFlag = (String)rs.getObject(1);
            tuesdayFlag = (String)rs.getObject(2);
            wednesdayFlag = (String)rs.getObject(3);
            thursdayFlag = (String)rs.getObject(4);
            fridayFlag = (String)rs.getObject(5);
            saturdayFlag = (String)rs.getObject(6);
            sundayFlag = (String)rs.getObject(7);
        }

        if(mondayFlag != null && mondayFlag.equals("N"))
        {
            days.add(new Integer(Calendar.MONDAY));
        }

        if(tuesdayFlag != null && tuesdayFlag.equals("N"))
        {
            days.add(new Integer(Calendar.TUESDAY));
        }

        if(wednesdayFlag != null && wednesdayFlag.equals("N"))
        {
            days.add(new Integer(Calendar.WEDNESDAY));
        }

        if(thursdayFlag != null && thursdayFlag.equals("N"))
        {
            days.add(new Integer(Calendar.THURSDAY));
        }

        if(fridayFlag != null && fridayFlag.equals("N"))
        {
            days.add(new Integer(Calendar.FRIDAY));
        }

        if(saturdayFlag != null && saturdayFlag.equals("N"))
        {
            days.add(new Integer(Calendar.SATURDAY));
        }

        if(sundayFlag != null && sundayFlag.equals("N"))
        {
            days.add(new Integer(Calendar.SUNDAY));
        }


        parms.setNoShipDays(days);
    }

    private static void getHolidayDates(OEDeliveryDateParm parms, List holidays)
    {
        Iterator it = holidays.iterator();
        HashMap holidayMap = new HashMap();
        OEDeliveryDate holiday = null;
        while(it.hasNext())
        {
            holiday = (OEDeliveryDate)it.next();
            holidayMap.put(holiday.getDeliveryDate(), holiday);
        }

        parms.setHolidayDates(holidayMap);
    }

    public static List getCountryHolidays(String countryId, Connection con) throws SAXException,
                        ParserConfigurationException, IOException, SQLException
    {
        List dates = new ArrayList();

        DataAccessUtil dau = DataAccessUtil.getInstance();
        DataRequest dataRequest = new DataRequest();

        dataRequest.setStatementID(GET_COUNRTY_HOLIDAYS);
        dataRequest.addInputParam(COUNTRY_ID, countryId);
        dataRequest.setConnection(con);
        CachedResultSet rs = (CachedResultSet)dau.execute(dataRequest);
        dataRequest.reset();

        String date = null;
        String deliverableFlag = null;
        String shippingAllowedFlag = null;
        OEDeliveryDate deliveryDate = null;

        while(rs.next())
        {
            date = (String)rs.getObject(1);
            deliverableFlag = (String)rs.getObject(3);
            shippingAllowedFlag = (String)rs.getObject(4);

            deliveryDate = new OEDeliveryDate();
            deliveryDate.setDeliveryDate(date);
            deliveryDate.setDeliverableFlag(deliverableFlag);
            deliveryDate.setShippingAllowed(shippingAllowedFlag);

            dates.add(deliveryDate);
        }

        return dates;
    }

    public static void getCodificationDetails(OEDeliveryDateParm parms, String countryId, String productId, String zipCode, String canadaCountryCode, Connection con)
                        throws SAXException, ParserConfigurationException, IOException,
                        SQLException, TransformerException
    {
        if(productId != null) productId = productId.toUpperCase();
        DataAccessUtil dau = DataAccessUtil.getInstance();
        DataRequest dataRequest = new DataRequest();
        String codifiedAvailableFlag = null;
        String codifiedDeliverableFlag = null;

        // if the country is canada then only use the first three characters of
        // the zip code
        if(countryId != null && countryId.equals(canadaCountryCode) && zipCode != null && zipCode.length() > 3)
        {
            zipCode = zipCode.substring(0, 3);
        }
        else if(countryId != null && countryId.equals("US") && zipCode != null && zipCode.length() > 5)
        {
            zipCode = zipCode.substring(0, 5);
        }

        // Get the codified flag and codified special flag
        dataRequest.setStatementID(GET_CODIFICATION_DETAILS);
        dataRequest.addInputParam(PRODUCT_ID, productId);
        dataRequest.setConnection(con);
        CachedResultSet rs = (CachedResultSet)dau.execute(dataRequest);
        dataRequest.reset();
        String codifiedProductFlag = null;
        String codifiedSpecialFlag = null;
        while(rs.next())
        {
            codifiedProductFlag = (String)rs.getObject(1);
            codifiedSpecialFlag = (String)rs.getObject(2);
        }
        if(codifiedProductFlag == null)
        {
            codifiedProductFlag = "N";
        }
        if(codifiedSpecialFlag == null)
        {
            codifiedSpecialFlag = "N";
        }

        // Get data from the CSZ_PRODUCTS table
        dataRequest.setStatementID(GET_CSZ_PRODUCT_DETAILS);
        dataRequest.addInputParam(PRODUCT_ID, productId);
        dataRequest.addInputParam(ZIP_CODE, zipCode);
        dataRequest.setConnection(con);
        rs = (CachedResultSet)dau.execute(dataRequest);
        dataRequest.reset();
        String availableFlag = null;

        while(rs.next())
        {
            availableFlag = (String)rs.getObject(1);
        }
        // if availableFlag is null then the product is not available
        if(availableFlag == null)
        {
            availableFlag = "N";
        }


        // Get data from the CSZ_PRODUCTS table
        dataRequest.setStatementID(GET_CSZ_AVAIL_DETAILS);
        dataRequest.addInputParam(ZIP_CODE, zipCode);
        dataRequest.setConnection(con);
        rs = (CachedResultSet)dau.execute(dataRequest);
        dataRequest.reset();
        String gnaddFlag = null;
        String sundayDeliveryFlag = null;
        String zipDownFlag = null;
        String returnedZipCode = null;

        while(rs.next())
        {
            returnedZipCode = (String)rs.getObject(1);
            gnaddFlag = (String)rs.getObject(2);
            sundayDeliveryFlag = (String)rs.getObject(3);
        }

        if(gnaddFlag == null)
        {
            gnaddFlag = "N";
        }
        if(sundayDeliveryFlag == null)
        {
            sundayDeliveryFlag = "N";
        }

        // Set the Zip GNADD flag
        parms.setZipCodeGNADDFlag(gnaddFlag);

        // Set the sunday delivery flag
        if(sundayDeliveryFlag.equalsIgnoreCase("N"))
        {
            parms.setSundayDelivery(Boolean.FALSE);
        }
        else
        {
            parms.setSundayDelivery(Boolean.TRUE);
        }

        // If the returnedZipCode is null then the zip code row does not exist
        if(returnedZipCode == null)
        {
            zipDownFlag = "Y";
        }
        else
        {
            zipDownFlag = "N";
        }

        // Check if the product is codified available.  Meaning that the product
        // is available in this zip code.
        if(codifiedProductFlag != null && codifiedProductFlag.equals("Y"))
        {
            if(zipCode != null)
            {
                codifiedAvailableFlag = availableFlag;
            }
            else
            {
                // International
                codifiedAvailableFlag = "S";
            }
        }
        else
        {
            // Product not codified
            codifiedAvailableFlag = "NA";
        }

        // Check if the product is codified deliverable.  Meaning that the product
        // can be delivered in this zip code.

        // Returns the following values:
        // D = Product not available and zip code is shut down
        // N = Product not available and zip code is in GNADD
        // G = Product not available and zip code is not in GNADD
        // Y = Product available
        if(availableFlag.equals("N") && zipDownFlag.equals("Y"))
        {
            codifiedDeliverableFlag = "D";
        }
        else if(availableFlag.equals("N") && !zipDownFlag.equals("Y") && !gnaddFlag.equals("Y"))
        {
            codifiedDeliverableFlag = "N";
        }
        else if(availableFlag.equals("N") && !gnaddFlag.equals("N"))
        {
            codifiedDeliverableFlag = "G";
        }
        else if(!availableFlag.equals("N"))
        {
            codifiedDeliverableFlag = availableFlag;
        }

        parms.setCodifiedProduct(codifiedProductFlag);
        parms.setCodifiedSpecialFlag(codifiedSpecialFlag);
        parms.setCodifiedAvailable(codifiedAvailableFlag);
        parms.setCodifiedDeliverable(codifiedDeliverableFlag);
    }

    private static void getProductDetails(OEDeliveryDateParm parms, String countryId, String productId,
                        String productSubCodeId, String canadaCountryCode, String usCountryCode, Connection con)
            throws SAXException, ParserConfigurationException, IOException,
                        SQLException, TransformerException
    {
        if(productId != null) productId = productId.toUpperCase();
        DataAccessUtil dau = DataAccessUtil.getInstance();
        DataRequest dataRequest = new DataRequest();

        if(countryId == null) countryId = "";
        if(productId == null) productId = "";

        // Get product master info
        dataRequest.setStatementID(GET_PRODUCT_DETAILS);
        dataRequest.addInputParam(PRODUCT_ID, productId);
        dataRequest.setConnection(con);
        CachedResultSet rs = (CachedResultSet)dau.execute(dataRequest);
        dataRequest.reset();
        String method = null;
        String productType = null;
        String productSubType = null;

        while(rs.next())
        {
            productType = (String)rs.getObject(8);
            productSubType = (String)rs.getObject(9);
            parms.setDeliveryType( (String)rs.getObject(6) );

            //get exception code info
            String exceptionCode = (String)rs.getObject(26);
            if(exceptionCode != null)
            {
              Date startDate = (Date)rs.getObject(27);
              Date endDate = (Date)rs.getObject(28);
              parms.setExceptionCode(exceptionCode);
              parms.setExceptionFrom(startDate);
              parms.setExceptionTo(endDate);
            }



            method = (String)rs.getObject(57);
            parms.setShipMethodFlorist( method!=null && method.equals("Y") );

            method = (String)rs.getObject(56);
            parms.setShipMethodCarrier( method!=null && method.equals("Y") );
        }

        if(productType == null) productType = "";
        if(productSubType == null) productSubType = "";

        parms.setProductType(productType);
        parms.setProductSubType(productSubType);


        // set delivery today flag
        if(parms.getProductSubType() != null && (parms.getProductSubType().equals(GeneralConstants.OE_PRODUCT_SUBTYPE_EXOTIC)) ||
           (parms.getProductType() != null && (parms.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_FRECUT) ||
           parms.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_SPEGFT))) ||
           (!countryId.equals(usCountryCode) && !countryId.equals(canadaCountryCode)))
        {
            parms.setDeliverTodayFlag(Boolean.FALSE);
        }
        else
        {
            parms.setDeliverTodayFlag(Boolean.TRUE);
        }

        // Get product ship methods
        parms.setShipMethods(getProductShipMethods(productId, con));

        // Set product ID
        parms.setProductId(productId);
        parms.setProductSubCodeId(productSubCodeId);
    }

    private static HashMap getProductShipMethods(String productId, Connection con)
        throws
            IOException,
            ParserConfigurationException,
            SAXException,
            SQLException {

        // Get product ship methods
        DataAccessUtil dau = DataAccessUtil.getInstance();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setStatementID(GET_PRODUCT_SHIP_METHODS);
        dataRequest.addInputParam(PRODUCT_ID, productId);
        dataRequest.setConnection(con);
        CachedResultSet rs = (CachedResultSet)dau.execute(dataRequest);
        dataRequest.reset();
        HashMap shipMethods = new HashMap();
        String shipMethodCode = null;
        String shipMethoddesc = null;
        ShippingMethod shippingMethod = null;

        while(rs.next())
        {
            shipMethodCode = (String)rs.getObject(1);

            if(shipMethodCode.equals(GeneralConstants.DELIVERY_NEXT_DAY_CODE))
            {
                shipMethoddesc = GeneralConstants.DELIVERY_NEXT_DAY;
            }
            else if(shipMethodCode.equals(GeneralConstants.DELIVERY_SATURDAY_CODE))
            {
                shipMethoddesc = GeneralConstants.DELIVERY_SATURDAY;
            }
            else if(shipMethodCode.equals(GeneralConstants.DELIVERY_STANDARD_CODE))
            {
                shipMethoddesc = GeneralConstants.DELIVERY_STANDARD;
            }
            else if(shipMethodCode.equals(GeneralConstants.DELIVERY_TWO_DAY_CODE))
            {
                shipMethoddesc = GeneralConstants.DELIVERY_TWO_DAY;
            }

            shippingMethod = new ShippingMethod();
            shippingMethod.setCode(shipMethodCode);
            shippingMethod.setDeliveryCharge(new BigDecimal(0));
            shippingMethod.setDescription(shipMethoddesc);

            shipMethods.put(shipMethoddesc, shippingMethod);
        }

        return shipMethods;
    }

    private static boolean isShipMethodAvailable(String productId, String shipMethod, Connection con)
        throws
            IOException,
            ParserConfigurationException,
            SAXException,
            SQLException {

        boolean retval = false;
        Collection shipMethods = getProductShipMethods(productId,con).values();
        Iterator it = shipMethods.iterator();
        ShippingMethod shippingMethod = null;

        while( it.hasNext() ) {
            shippingMethod = (ShippingMethod) it.next();
            if( shippingMethod.getCode().equals(shipMethod) ) {
                retval = true;
                break;
            }
        }
        return retval;
    }


    public static void getGlobalParameters(OEDeliveryDateParm parms, Connection con)
                        throws SAXException, ParserConfigurationException, IOException,
                        SQLException, ParseException
    {
        OEParameters globalParms = new OEParameters();
        GlobalParmHandler parmHandler = (GlobalParmHandler)CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_GLOBAL_PARM);
        if(parmHandler != null && parmHandler!=null )
        {
            FTDAppsGlobalParmsVO ftdAppsParms = parmHandler.getFTDAppsGlobalParms();
            globalParms.setGNADDLevel(Long.toString(ftdAppsParms.getGnaddLevel()));
            globalParms.setGNADDDate(ftdAppsParms.getGnaddDate());
            globalParms.setIntlAddOnDays((int)ftdAppsParms.getIntlOverrideDays());
            globalParms.setMondayCutoff(ftdAppsParms.getMondayCutoff());
            globalParms.setTuesdayCutoff(ftdAppsParms.getTuesdayCutoff());
            globalParms.setWednesdayCutoff(ftdAppsParms.getWednesdayCutoff());
            globalParms.setThursdayCutoff(ftdAppsParms.getThursdayCutoff());
            globalParms.setFridayCutoff(ftdAppsParms.getFridayCutoff());
            globalParms.setSaturdayCutoff(ftdAppsParms.getSaturdayCutoff());
            globalParms.setSundayCutoff(ftdAppsParms.getSundayCutoff());
            globalParms.setExoticCutoff(ftdAppsParms.getExoticCutoff());
            globalParms.setFreshCutCutoff(ftdAppsParms.getFreshCutsCutoff());
            globalParms.setSpecialtyGiftCutoff(ftdAppsParms.getSpecialtyGiftCutoff());
            globalParms.setIntlCutoff(ftdAppsParms.getIntlCutoff());
            globalParms.setDeliveryDaysOut((int)ftdAppsParms.getDeliveryDaysOut());
            globalParms.setSpecialSrvcCharge(ftdAppsParms.getSpecialSvcCharge());

        }
        else
        {
            DataAccessUtil dau = DataAccessUtil.getInstance();
            DataRequest dataRequest = new DataRequest();

            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");

            dataRequest.setStatementID(GET_GLOBAL_PARAMETERS);
            dataRequest.setConnection(con);
            CachedResultSet rs = (CachedResultSet)dau.execute(dataRequest);
            dataRequest.reset();

            while(rs.next())
            {
                globalParms.setGNADDLevel(new Integer(rs.getObject(1).toString()).toString());
                java.util.Date gnaddDate = sdf.parse((String)rs.getObject(2));
                globalParms.setGNADDDate(gnaddDate);
                globalParms.setIntlAddOnDays(new Integer(rs.getObject(3).toString()).intValue());
                globalParms.setMondayCutoff((String)rs.getObject(4));
                globalParms.setTuesdayCutoff((String)rs.getObject(5));
                globalParms.setWednesdayCutoff((String)rs.getObject(6));
                globalParms.setThursdayCutoff((String)rs.getObject(7));
                globalParms.setFridayCutoff((String)rs.getObject(8));
                globalParms.setSaturdayCutoff((String)rs.getObject(9));
                globalParms.setSundayCutoff((String)rs.getObject(10));
                globalParms.setExoticCutoff((String)rs.getObject(11));
                globalParms.setFreshCutCutoff((String)rs.getObject(12));
                globalParms.setSpecialtyGiftCutoff((String)rs.getObject(13));
                globalParms.setIntlCutoff((String)rs.getObject(14));
                globalParms.setDeliveryDaysOut(new Integer(rs.getObject(15).toString()).intValue());
                globalParms.setSpecialSrvcCharge(new BigDecimal(rs.getObject(18).toString()));
            }
        }

        parms.setGlobalParms(globalParms);
    }


    private static void getCountryDetails(OEDeliveryDateParm parms, String countryId, Connection con)
                        throws SAXException, ParserConfigurationException, IOException,
                        SQLException, CacheException
    {
        // Try cache first
        CountryMasterHandler countryHandler = (CountryMasterHandler)CacheManager.getInstance().getHandler("CACHE_NAME_COUNTRY_MASTER");

        if(countryHandler != null)
        {
            CountryMasterVO countryVo = countryHandler.getCountryById(countryId);
            if(countryVo != null)
            {
                parms.setCntryAddOnDays((int)countryVo.getAddonDays());
            }
        }
        else
        {
            DataAccessUtil dau = DataAccessUtil.getInstance();
            DataRequest dataRequest = new DataRequest();
            dataRequest.setStatementID(GET_COUNTRY_DETAILS);
            dataRequest.addInputParam(COUNTRY_ID, countryId);
            dataRequest.setConnection(con);
            CachedResultSet rs = (CachedResultSet)dau.execute(dataRequest);
            dataRequest.reset();

            while(rs.next())
            {
                parms.setCntryAddOnDays(new Integer(rs.getObject(3).toString()).intValue());
            }
        }
    }

    private static void getZipCodeDetails(OEDeliveryDateParm parms, String zipCode, String stateId, String productId, String countryId, String canadaCountryCode, String usCountryCode, Connection con)
                        throws SAXException, ParserConfigurationException, IOException,
                        SQLException, CacheException
    {
        if(productId != null) productId = productId.toUpperCase();
        if(zipCode == null) zipCode = "";
        DataAccessUtil dau = DataAccessUtil.getInstance();
        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs = null;

        // if the country is canada then only use the first three characters of
        // the zip code
        if(countryId != null && countryId.equals(canadaCountryCode) && zipCode.length() > 3)
        {
            zipCode = zipCode.substring(0, 3);
        }
        else if(countryId != null && countryId.equals(usCountryCode) && zipCode.length() > 5)
        {
            zipCode = zipCode.substring(0, 5);
        }

        // Try cache first
        StateMasterHandler stateHandler = (StateMasterHandler)CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_STATE_MASTER);
        String timeZone = null;
        if(stateHandler != null)
        {
            timeZone = stateHandler.getTimeZone(stateId);
        }

        // If the zip handler is not found or the zip code is not in the cache
        // then lookup the zip code data from the database
        if(stateHandler == null || timeZone == null)
        {
            dataRequest.setStatementID(GET_ZIP_CODE_DETAILS);
            dataRequest.addInputParam(ZIP_CODE, zipCode);
            dataRequest.setConnection(con);
            rs = (CachedResultSet)dau.execute(dataRequest);
            dataRequest.reset();

            while(rs.next())
            {
                timeZone = (String)rs.getObject(1);
            }
        }

        // Default to five if the zip code is not found (taken from WebOE)
        if(timeZone == null) timeZone = "5";

        parms.setZipTimeZone(timeZone);

        // Get the goto florist flag for this zip code
        dataRequest.setStatementID(GET_GOTO_FLORIST_FLAG);
        dataRequest.addInputParam(ZIP_CODE, zipCode);
        dataRequest.addInputParam(PRODUCT_ID, productId);
        dataRequest.addInputParam("CITY", "");
        dataRequest.addInputParam("STATE", "");
        dataRequest.addInputParam("PHONE", "");
        dataRequest.addInputParam("ADDRESS", "");
        dataRequest.addInputParam("FLORIST_NAME", "");
        dataRequest.setConnection(con);

        rs = (CachedResultSet)dau.execute(dataRequest);
        dataRequest.reset();

        String flag = null;

        while(rs.next())
        {
            flag = (String)rs.getObject(9);

            if(flag != null && flag.equals("Y"))
            {
                break;
            }
        }

        if(flag != null && flag.equals("Y"))
        {
            parms.setZipCodeGotoFloristFlag("Y");
        }
        else
        {
            parms.setZipCodeGotoFloristFlag("N");
        }

        //get zipcode cutoff
        parms.setZipCodeCutoff(getZipCodeCutoff(zipCode,productId,con));


    }

    /**
     * Overloaded method passing in
     *  - null for product sub code id.
     *  - 0 for diffInDays
     *
     * Gathers the required parameters to calculate delivery dates.  This method is sepecific to COM
     * because the COM data model varies from Web O/E and Scrub data model.  The method creates the
     * neccessary objects used throughout the DeliveryDateUTIL.
     *
     * @param comParams COMParameters Wrapper for required data for DeliveryDateUTIL.
     */
    public static OEDeliveryDateParm getCOMParameterData(String country, String productId,
                  String zip, String canadaCountryCode, String usCountryCode, String lineNumber, String occasionId,
                  Connection conn, String state, String orderOrigin, boolean isFlorist, boolean isMinDays)
        throws SAXException, ParserConfigurationException, IOException,
               SQLException, ParseException, TransformerException, Exception
    {
      return getCOMParameterData(country,productId,null,zip,canadaCountryCode,usCountryCode,
                                 lineNumber,occasionId,conn,state,orderOrigin,isFlorist,isMinDays,0);
    }



    /**
     * Overloaded method passing in
     *  - 0 for diffInDays
     *
     * Gathers the required parameters to calculate delivery dates.  This method is sepecific to COM
     * because the COM data model varies from Web O/E and Scrub data model.  The method creates the
     * neccessary objects used throughout the DeliveryDateUTIL.
     *
     * @param comParams COMParameters Wrapper for required data for DeliveryDateUTIL.
     */
    public static OEDeliveryDateParm getCOMParameterData(String country, String productId, String productSubCodeId,
                  String zip, String canadaCountryCode, String usCountryCode, String lineNumber, String occasionId,
                  Connection conn, String state, String orderOrigin, boolean isFlorist, boolean isMinDays)
        throws SAXException, ParserConfigurationException, IOException,
               SQLException, ParseException, TransformerException, Exception
    {
      return getCOMParameterData(country,productId,productSubCodeId,zip,canadaCountryCode,usCountryCode,
                                 lineNumber,occasionId,conn,state,orderOrigin,isFlorist,isMinDays,0);
    }



    /**
     * Overloaded method passing in
     *  - null for product sub code id.
     *
     * Gathers the required parameters to calculate delivery dates.  This method is sepecific to COM
     * because the COM data model varies from Web O/E and Scrub data model.  The method creates the
     * neccessary objects used throughout the DeliveryDateUTIL.
     *
     * @param comParams COMParameters Wrapper for required data for DeliveryDateUTIL.
     */
    public static OEDeliveryDateParm getCOMParameterData(String country, String productId,
                  String zip, String canadaCountryCode, String usCountryCode, String lineNumber, String occasionId,
                  Connection conn, String state, String orderOrigin, boolean isFlorist, boolean isMinDays, int daysOut)
          throws  SAXException, ParserConfigurationException, IOException,
                  SQLException, ParseException, TransformerException, Exception
    {
      return getCOMParameterData(country,productId,null,zip,canadaCountryCode,usCountryCode,
                                 lineNumber,occasionId,conn,state,orderOrigin,isFlorist,isMinDays,daysOut);
    }

    /**
     * Gathers the required parameters to calculate delivery dates.  This method is sepecific to COM
     * because the COM data model varies from Web O/E and Scrub data model.  The method creates the
     * neccessary objects used throughout the DeliveryDateUTIL.
     *
     * @param comParams COMParameters Wrapper for required data for DeliveryDateUTIL.
     */
    public static OEDeliveryDateParm getCOMParameterData(String country, String productId, String productSubCodeId,
                  String zip, String canadaCountryCode, String usCountryCode, String lineNumber, String occasionId,
                  Connection conn, String state, String orderOrigin, boolean isFlorist, boolean isMinDays, int daysOut)
          throws  SAXException, ParserConfigurationException, IOException,
                  SQLException, ParseException, TransformerException, Exception
    {
        // prepare VOs used throughout the DeliveryDateUTIL but not in COM
        // recipient address
        RecipientAddressesVO recipAddress = new RecipientAddressesVO();
        recipAddress.setStateProvince( state );
        recipAddress.setPostalCode( zip );
        recipAddress.setCountry( country );
        if ( IsDomesticPredicate.getInstance().evaluate(country) ) {
            recipAddress.setInternational("N");
        }
        else {
            recipAddress.setInternational("Y");
        }
        List recipientAddressList = new ArrayList();
        recipientAddressList.add(recipAddress);

        // recipient
        RecipientsVO recipient = new RecipientsVO();
        recipient.setRecipientAddresses( recipientAddressList );
        List recipientsList = new ArrayList();
        recipientsList.add(recipient);

        // order details
        OrderDetailsVO item = new OrderDetailsVO();
        item.setRecipients( recipientsList );
        List orderDetailsList = new ArrayList();
        orderDetailsList.add(item);

        // order
        OrderVO order = new OrderVO();
        order.setOrderDetail( orderDetailsList );
        order.setOrderOrigin( orderOrigin );
        // end VO creation

        // set the OrderVO
        OEDeliveryDateParm parms = new OEDeliveryDateParm();
        parms.setOrder( order );

        // set the Connection
        // This was added to save a Connection obj so it can be retrieved later - within
        // setDateRanges (since it's needed for Amazon).
        parms.setConn( conn );

        parms.setItemNumber( Integer.parseInt(lineNumber) );

        // get global params from FRP Global Variables
        getGlobalParameters(parms, parms.getConn());

        // Get zip code details
        getZipCodeDetails(parms, zip, state, productId, country, canadaCountryCode, usCountryCode, conn);

        // Get product details
        getProductDetails(parms, country, productId, productSubCodeId, canadaCountryCode, usCountryCode, parms.getConn());

        // Get product codification details
        getCodificationDetails(parms, country, productId, zip, canadaCountryCode, parms.getConn());

        // initialize the remaining parameters if vendor item
        if ( !isFlorist )
        {
          getBaseParameterData(parms, country, productId, zip, canadaCountryCode, usCountryCode, lineNumber, occasionId, state);

          if(daysOut > 0 )
          {
              parms.getGlobalParms().setDeliveryDaysOut(daysOut);
          }
          else
          {
              if ( isMinDays ) {
                parms.getGlobalParms().setDeliveryDaysOut( parms.getGlobalParms().getDeliveryDaysOutMin());
              }
              else {
                parms.getGlobalParms().setDeliveryDaysOut( parms.getGlobalParms().getDeliveryDaysOutMax() );
              }
          }

          //parms.setDeliveryRangeFlag(Boolean.TRUE);
          //setDateRanges(parms);

          //issue 2479 & 2480 in MO - reset the delivery range flag
          setCOMDateRanges(parms);
        }
        //added this as part of 2572.  Note that some of the methods in getBaseParameterData are still
        //missing here, and we will add them as a on-needed basis.
        else
        {
          // Get country holidays
          List countryHolidays = getCountryHolidays(country, parms.getConn());

          // Get holiday dates
          getHolidayDates(parms, countryHolidays);
        }


        return parms;
    }


    private static void setCOMDateRanges(OEDeliveryDateParm delParms) throws Exception
    {
      DataAccessUtil dau = DataAccessUtil.getInstance();
      DataRequest dataRequest = new DataRequest();

      dataRequest.setStatementID("GET_DELIVERY_DATE_RANGES_CSR");
      Connection con = delParms.getConn();
      dataRequest.setConnection(con);
      CachedResultSet rs = (CachedResultSet)dau.execute(dataRequest);
      dataRequest.reset();

      Map deliveryRangeFromMap = new HashMap();
      Map deliveryRangeToMap = new HashMap();
      Map deliveryRangeTextMap = new HashMap();
      String startDate = null;
      String endDate = null;
      String deliveryRangeText = null;
      int count = 0;

      while( rs.next() )
      {
          startDate = (String)rs.getObject(1);
          endDate  = (String)rs.getObject(2);
          deliveryRangeText = (String)rs.getObject(3);

          deliveryRangeFromMap.put(String.valueOf(count), startDate);
          deliveryRangeToMap.put(String.valueOf(count), endDate);
          deliveryRangeTextMap.put(String.valueOf(count),deliveryRangeText);
          count++;
      }

      if( count > 0 )
      {
        delParms.setDeliveryRangeFlag(Boolean.TRUE);
        delParms.setDeliveryRangeFrom((HashMap)deliveryRangeFromMap);
        delParms.setDeliveryRangeTo((HashMap)deliveryRangeToMap);
        delParms.setDeliveryRangeText((HashMap)deliveryRangeTextMap);
      }
      else
      {
        delParms.setDeliveryRangeFlag(Boolean.FALSE);
      }
    }


    //This is an Overloaded method that does not accept the product sub code id.  This way,
    //we won't have to change all the applications.
    public static OEDeliveryDateParm getParameterData(String country, String productId, String zip,
                  String canadaCountryCode, String usCountryCode,
                  String lineNumber, String occasionId, Connection conn, String state)
                  throws SAXException, ParserConfigurationException, IOException,
                         SQLException, ParseException, TransformerException, Exception
    {
      return getParameterData(country, productId, null, zip, canadaCountryCode, usCountryCode,
                              lineNumber, occasionId, conn, state);
    }

    public static OEDeliveryDateParm getParameterData(String country, String productId,
                  String productSubCodeId, String zip, String canadaCountryCode, String usCountryCode,
                  String lineNumber, String occasionId, Connection conn, String state)
                  throws SAXException, ParserConfigurationException, IOException,
                         SQLException, ParseException, TransformerException, Exception
    {
        OEDeliveryDateParm parms = new OEDeliveryDateParm();

        // This was added to save a Connection obj so it can be retrieved later - within
        // setDateRanges (since it's needed for Amazon).
        parms.setConn(conn);

        // Get global parameters
        getGlobalParameters(parms, conn);

        // Get zip code details
        getZipCodeDetails(parms, zip, state, productId, country, canadaCountryCode, usCountryCode, parms.getConn());

        // Get product details
        getProductDetails(parms, country, productId, productSubCodeId, canadaCountryCode, usCountryCode, parms.getConn());

        // Get product codification details
        getCodificationDetails(parms, country, productId, zip, canadaCountryCode, parms.getConn());

        // initialize the remaining parameters
        getBaseParameterData(parms, country, productId, zip, canadaCountryCode, usCountryCode, lineNumber, occasionId, state);

        return parms;
    }

    private static void getBaseParameterData(OEDeliveryDateParm parms, String country, String productId, String zip,
                  String canadaCountryCode, String usCountryCode,
                  String lineNumber, String occasionId, String state)
                  throws SAXException, ParserConfigurationException, IOException,
                         SQLException, ParseException, TransformerException, Exception
    {
        if(productId != null) productId = productId.toUpperCase();

        if(lineNumber == null) {
            throw new Exception("lineNumber cannot be null");
        }

        // Get state exclusion days
        getStateExclusionDetails(parms,parms.getConn(),productId,state);

        // Get vendor no ship days
        getVendorNoShipDays(parms, productId, parms.getConn());

        // Get country details
        getCountryDetails(parms, country, parms.getConn());

        // Get vendor no ship dates
        getVendorNoShipDates(parms, productId, parms.getConn());

        //Get vendor delivery restrictions
        getVendorDeliveryRestrictions(parms, productId, parms.getConn() );

        // Get country holidays
        List countryHolidays = getCountryHolidays(country, parms.getConn());

        // Get holiday dates
        getHolidayDates(parms, countryHolidays);

        parms.setItemNumber(Integer.parseInt(lineNumber));
        parms.setOccasion(occasionId);
    }

  /*
   * Retrieve zipcode cutoff time
   */
   private static String getZipCodeCutoff(String zipcode, String productId, Connection conn)
   {
      String zipCutoffTimeZip = null;
      String zipCutoffTimeProduct = null;
      String zipCutoffTime = null;
      try{
            //retrieve zipcode cutoff information (CSZ_AVAIL)
            DataAccessUtil dau = DataAccessUtil.getInstance();
            DataRequest dataRequest = new DataRequest();
            dataRequest.setStatementID(GET_CSZ_AVAIL_DETAILS);
            dataRequest.addInputParam("ZIP_CODE",    zipcode);
            dataRequest.setConnection(conn);
            CachedResultSet rs = (CachedResultSet)dau.execute(dataRequest);
            if(rs.next())
            {
                Object obj = rs.getObject(4);
                if (obj != null){
                    String value = (String)obj;
                    if(value.length() > 0) {
                      zipCutoffTimeZip = value;
                    }
                }
            } //recordset contains data


            //retrieve zipcode cutoff information by product id (CSZ_PRODUCTS)
            dataRequest.reset();
            dataRequest.setStatementID(GET_CSZ_PRODUCT_DETAILS);
            dataRequest.addInputParam("ZIP_CODE",    zipcode);
            dataRequest.addInputParam("PRODUCT_ID", productId);
            dataRequest.setConnection(conn);
            rs = (CachedResultSet)dau.execute(dataRequest);
            if(rs.next())
            {
                Object obj = rs.getObject(2);
                if (obj != null){
                    String value = (String)obj;
                    if(value.length() > 0) {
                      zipCutoffTimeProduct = value;
                    }
                }
            } //recordset contains data


      if(zipCutoffTimeZip != null && zipCutoffTimeProduct == null)
      {
        zipCutoffTime = zipCutoffTimeZip;
      }
      else if(zipCutoffTimeZip == null && zipCutoffTimeProduct != null)
      {
        zipCutoffTime = zipCutoffTimeProduct;
      }
      else if(zipCutoffTimeZip != null && zipCutoffTimeProduct != null)
      {
        if(Integer.parseInt(zipCutoffTimeZip) < Integer.parseInt(zipCutoffTimeProduct))
        {
          zipCutoffTime = zipCutoffTimeZip;
        }
        else
        {
          zipCutoffTime = zipCutoffTimeProduct;
        }
      }


      }//end try block
      catch(Exception e) {
          logger.error("Cound not obtain zipcode cutff time from database.");
          logger.error(e);
          zipCutoffTime = "-1";
      }

      return zipCutoffTime;
     }

    /*
     * This method determines if the product cannot be delivered to the specified state for the passed in
     * day of the week. (PRODUCT_EXCLUDED_STATES table in FTD_APPS)
     *
     * @param Connection database connection
     * @param ValiationDataAccessUtil
     * @param String product id
     * @param String Recipients state
     * @param int day of the week
     * @returns boolean TRUE=Deliverable
     */
    private static void getStateExclusionDetails(OEDeliveryDateParm parms,Connection conn, String productId, String state)
      throws Exception
    {

        //do no continue if the state or product is null
        if(productId == null || state == null)
        {
          return;
        }

        String sunFlag = "N";
        String monFlag = "N";
        String tueFlag = "N";
        String wedFlag = "N";
        String thrFlag = "N";
        String friFlag = "N";
        String satFlag = "N";

        DataAccessUtil dau = DataAccessUtil.getInstance();
        DataRequest dataRequest = new DataRequest();


        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_PRODUCT_EXCLUDED_STATES");
        dataRequest.addInputParam("IN_PRODUCT_ID",productId);
        dataRequest.addInputParam("IN_EXCLUDED_STATE",state);
        CachedResultSet rs = (CachedResultSet)dau.execute(dataRequest);

        if(rs.next())
        {
            if(rs.getObject(4).toString() != null ){
                sunFlag = rs.getObject(4).toString();
            }
            if(rs.getObject(5).toString() != null ){
                monFlag = rs.getObject(5).toString();
            }
            if(rs.getObject(6).toString() != null ){
                tueFlag = rs.getObject(6).toString();
            }
            if(rs.getObject(7).toString() != null ){
                wedFlag = rs.getObject(7).toString();
            }
            if(rs.getObject(8).toString() != null ){
                thrFlag = rs.getObject(8).toString();
            }
            if(rs.getObject(9).toString() != null ){
                friFlag = rs.getObject(9).toString();
            }
            if(rs.getObject(10).toString() != null ){
                satFlag = rs.getObject(10).toString();
            }
        }

        if(sunFlag.equals("Y"))
        {
          parms.setStateSunExclusion(true);
        }
        if(monFlag.equals("Y"))
        {
          parms.setStateMonExclusion(true);
        }
        if(tueFlag.equals("Y"))
        {
          parms.setStateTueExclusion(true);
        }
        if(wedFlag.equals("Y"))
        {
          parms.setStateWedExclusion(true);
        }
        if(thrFlag.equals("Y"))
        {
          parms.setStateThrExclusion(true);
        }
        if(friFlag.equals("Y"))
        {
          parms.setStateFriExclusion(true);
        }
        if(satFlag.equals("Y"))
        {
          parms.setStateSatExclusion(true);
        }

    }

    /*
     * This method checks if we are currenly past the vendor cutoff time.
     * This is not the same as the SPEGFT and FRECUT cutoff times that OE uses.
     */
    public HashMap retrieveVendorCutoffInfo(Connection conn, String productId)
    {
      HashMap resultHash = new HashMap();

      try
      {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("PAST_VENDOR_CUTOFF");
        Map paramMap = new HashMap();
        paramMap.put(PRODUCT_ID, productId);
        dataRequest.setInputParams(paramMap);
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map result = (Map)dataAccessUtil.execute(dataRequest);

        //if cutoff exists obtain the pastCutoff value
        if(result != null)
        {
          resultHash.put("past_cutoff", (String)result.get("PAST_CUTOFF"));
          resultHash.put("cutoff", (String)result.get("CUTOFF"));
        }
        //If no cutoff exists the vendor could not be found. Assume we are past
        //the cutoff to be safe.
        else
        {
          logger.error("Could not obtain a vendor cutoff time.  Please fix!  We'll assume we are past the cutoff.");
        }
      }
      catch(Exception e)
      {
        logger.error("Could not obtain a vendor cutoff time.  Please fix!  We'll assume we are past the cutoff.");
        logger.error(e);
      }

      return resultHash;
    }



    /**
     * Set the system date based on values set in the test_data_config file.
     */
    private void setTestDateInfo() throws ParserConfigurationException, SAXException, IOException, TransformerException
    {
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
        String test = configUtil.getProperty(TEST_DATA_CONFIG_FILE_NAME, "TEST_MODE");
        if(test.equalsIgnoreCase("Y"))
          testMode = true;
        testYear = Integer.parseInt(configUtil.getProperty(TEST_DATA_CONFIG_FILE_NAME, "YEAR"));
        testMonth = Integer.parseInt(configUtil.getProperty(TEST_DATA_CONFIG_FILE_NAME, "MONTH"));
        testDay = Integer.parseInt(configUtil.getProperty(TEST_DATA_CONFIG_FILE_NAME, "DAY"));
        testHour = Integer.parseInt(configUtil.getProperty(TEST_DATA_CONFIG_FILE_NAME, "HOUR"));
        testMinute = Integer.parseInt(configUtil.getProperty(TEST_DATA_CONFIG_FILE_NAME, "MINUTE"));
    }

  public boolean validDelivery(OEDeliveryDateParm oeParms,String shipMethod, Date deliveryDate)
  throws ParserConfigurationException, SAXException, IOException, TransformerException, Exception
  {
      List list = getOrderProductDeliveryDates(oeParms);

      boolean validDelivery = false;
      boolean dateFound = false;

      //loop through dates to find the delivery date
      if(list !=null)
      {
          Iterator iter = list.iterator();
          while(!dateFound && iter.hasNext())
          {
              OEDeliveryDate oeDate = (OEDeliveryDate)iter.next();
              Date listDeliveryDate = FieldUtils.formatStringToUtilDate(oeDate.getDeliveryDate());
              if(listDeliveryDate.equals(deliveryDate))
              {
                  //date was found
                  dateFound = true;

                  //is delivery date valid?
                  if(oeDate.getDeliverableFlag().equals("Y"))
                  {
                      //is the ship method valid?
                      if(oeDate.getShippingMethods() != null)
                      {
                          Iterator shipIter = oeDate.getShippingMethods().iterator();
                          while(shipIter.hasNext())
                          {
                              ShippingMethod validMethod = (ShippingMethod)shipIter.next();
                              if(shipMethod.equalsIgnoreCase(validMethod.getCode()))
                              {
                                  validDelivery = true;
                              }//if ship method valid
                          }//while loop - ship methods
                      }//if has ship methods
                  }//if deliverable
              }//if date matches
          }//whie loop - dates
      }//if delivery dates found


      return validDelivery;

  }


    public static int getStandardTransitTime()
    {
        return standardTransitTime;
    }
    public static int getSecondDayTransitTime()
    {
        return secondDayTransitTime;
    }
    public static int getNextDayTransitTime()
    {
        return nextDayTransitTime;
    }


    /*
     * This method retrieves the product information
     */
    public ProductVO retrieveProductInformation(Connection conn, String productId) throws Exception
    {
      ProductVO product = new ProductVO();
      CachedResultSet outputs = null;

      //determine if current time is past the vendor cutoff
      DataRequest dataRequest = new DataRequest();
      dataRequest.setConnection(conn);
      dataRequest.setStatementID("GET_PRODUCT_BY_ID");
      Map paramMap = new HashMap();
      paramMap.put("IN_PRODUCT_ID", productId);
      dataRequest.setInputParams(paramMap);
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

      /* execute the store prodcedure */
      outputs = (CachedResultSet)dataAccessUtil.execute(dataRequest);

      /* populate object */
      while (outputs.next())
      {
        product.setProductId(outputs.getString("productId"));
        product.setNovatorId(outputs.getString("novatorId"));
        product.setProductName(outputs.getString("productName"));
        product.setNovatorName(outputs.getString("novatorName"));
        product.setStatus(outputs.getString("status"));
        product.setDeliveryType(outputs.getString("deliveryType"));
        product.setCategory(outputs.getString("category"));
        product.setProductType(outputs.getString("productType"));
        product.setProductSubType(outputs.getString("productSubType"));
        product.setColorSizeFlag(outputs.getString("colorSizeFlag"));
        product.setStandardPrice(outputs.getDouble("standardPrice"));
        product.setDeluxePrice(outputs.getDouble("deluxePrice"));
        product.setPremiumPrice(outputs.getDouble("premiumPrice"));
        product.setPreferredPricePoint(outputs.getDouble("preferredPricePoint"));
        product.setVariablePriceMax(outputs.getDouble("variablePriceMax"));
        product.setShortDescription(outputs.getString("shortDescription"));
        product.setLongDescription(outputs.getString("longDescription"));
        product.setFloristReferenceNumber(outputs.getString("floristReferenceNumber"));
        product.setMercuryDescription(outputs.getString("mercuryDescription"));
        product.setItemComments(outputs.getString("itemsComments"));
        product.setAddOnBalloonsFlag(outputs.getString("addOnBallonsFlag"));
        product.setAddOnBearsFlag(outputs.getString("addOnBearsFlag"));
        product.setAddOnCardsFlag(outputs.getString("addOnCardsFlag"));
        product.setAddOnFuneralFlag(outputs.getString("addOnFuneralFlag"));
        product.setCodifiedFlag(outputs.getString("codifiedFlag"));
        product.setExceptionCode(outputs.getString("codifiedFlag"));
        product.setExceptionStartDate(outputs.getDate("exceptionStartDate"));
        product.setExceptionEndDate(outputs.getDate("exceptionEndDate"));
        product.setExceptionMessage(outputs.getString("exceptionMessage"));
        product.setSecondChoiceCode(outputs.getString("secondChoiceCode"));
        product.setHolidaySecondChoiceCode(outputs.getString("holidaySecondChoiceCode"));
        product.setDropshipCode(outputs.getString("dropshipCode"));
        product.setDiscountAllowedFlag(outputs.getString("discountAllowedFlag"));
        product.setDeliveryIncludedFlag(outputs.getString("discountAllowedFlag"));
        product.setTaxFlag(outputs.getString("taxFlag"));
        product.setServiceFeeFlag(outputs.getString("serviceFeeFlag"));
        product.setExoticFlag(outputs.getString("exoticFlag"));
        product.setEgiftFlag(outputs.getString("egiftFlag"));
        product.setCountryId(outputs.getString("countryId"));
        product.setArrangementSize(outputs.getString("arrangementSize"));
        product.setArrangementColors(outputs.getString("arrangementColors"));
        product.setDominantFlowers(outputs.getString("dominantFlowers"));
        product.setSearchPriority(outputs.getString("searchPriority"));
        product.setRecipe(outputs.getString("recipe"));
        product.setSubcodeFlag(outputs.getString("subcodeFlag"));
        product.setDimWeight(outputs.getString("dimWeight"));
        product.setNextDayUpgradeFlag(outputs.getString("nextDayUpgradeFlag"));
        product.setCorporateSite(outputs.getString("corporateSite"));
        product.setUnspscCode(outputs.getString("unspscCode"));
        product.setPriceRank1(outputs.getString("priceRank1"));
        product.setPriceRank2(outputs.getString("priceRank2"));
        product.setPriceRank3(outputs.getString("priceRank3"));
        product.setShipMethodCarrier(outputs.getString("shipMethodCarrier"));
        product.setShipMethodFlorist(outputs.getString("shipMethodFlorist"));
        product.setShippingKey(outputs.getString("shippingKey"));
        product.setVariablePriceFlag(outputs.getString("variablePriceFlag"));
        product.setHolidaySku(outputs.getString("holidaySku"));
        product.setHolidayPrice(outputs.getDouble("holidayPrice"));
        product.setCatalogFlag(outputs.getString("catalogFlag"));
        product.setHolidayDeluxePrice(outputs.getDouble("holidayDeluxePrice"));
        product.setHolidayPremiumPrice(outputs.getDouble("holidayPremiumPrice"));
        product.setHolidayStartDate(outputs.getDate("holidayStartDate"));
        product.setHolidayEndDate(outputs.getDate("holidayEndDate"));
        product.setHoldUntilAvailable(outputs.getString("holdUntilAvailable"));
        product.setMondayDeliveryFreshcut(outputs.getString("mondayDeliveryFreshcut"));
        product.setTwoDaySatFreshcut(outputs.getString("twoDayShipSatFreshcut"));
        product.setShippingSystem(outputs.getString("shippingSystem"));
        product.setPersonalizationTemplate(outputs.getString("personalizationTemplate"));
        product.setPersonalizationLeadDays(outputs.getString("personalizationLeadDays"));
      }

      return product;

    }


    /**
     * This method returns the ship date based on the lead days passed in.
     * @param leadDays The number of valid ship days to put between the current
     * date and the ship date.
     * @return A string MM/DD/YYYY date value representing the ship date.
     */
    public String getShipDateByLeadDays(int leadDays) throws Exception{
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
        Calendar shipDateCal = Calendar.getInstance();  
        int dayOfWeek = -1;
        int leadDaysCount = 0;

        // Determine next available shipping date
        while(true){
          dayOfWeek = shipDateCal.get(shipDateCal.DAY_OF_WEEK);
          
          // If it is a valid ship date, M-F, break out of the loop.
          if(dayOfWeek > 1 && dayOfWeek < 7) {
              break;
          }
  
          // Increment ship date
          shipDateCal.add(Calendar.DATE, 1);
        }

        /* Determine ship date based on lead days.  If lead days is zero
         * the shipping date found above will be returned. */
        while(leadDaysCount < leadDays) {
            // Increment ship date
            shipDateCal.add(Calendar.DATE, 1);

            dayOfWeek = shipDateCal.get(shipDateCal.DAY_OF_WEEK);

            // If it is a valid ship date, M-F, increment counter.
            if(dayOfWeek > 1 && dayOfWeek < 7) {
                leadDaysCount++;
            }
        }
        
        // Format ship date
        String shipDate = dateFormat.format(shipDateCal.getTime());
        
        if(logger.isDebugEnabled()){
            logger.debug("Lead days: " + leadDays);
            logger.debug("Ship date: " + shipDate);
        }
        
        return shipDate;
    }
    
    
    /**
     * This method returns the first valid delivery date based on 
     * ship date + number of days in transit.  Valid ship and delivery dates
     * should be configured for both the product and vendor.
     * @param parms Parameters containing product, recipient information, etc.
     * @param shipDate String MM/dd/yyyy date value representing the ship date.
     * @return A string MM/dd/yyyy date value representing the delivery date.
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     * @throws XSLException
     * @throws TransformerException
     */
    public String getStandardDeliveryDateFromShipDate(OEDeliveryDateParm parms, String shipDate)  throws ParserConfigurationException, SAXException, IOException, TransformerException, SQLException, Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        String deliveryDate = new String();
        int deliveryDateCount = 0; // Counter for delivery dates
        int startIndex = 0;        // Index in delivery date array list to start
        long shipDateTime = sdf.parse(shipDate).getTime();
        
        /* Get a list of possible delivery dates. */
        ArrayList deliveryDates = getOrderProductDeliveryDates(parms);
        
        /* Obtain number of days in transit */
        int daysInTransit = Integer.parseInt(
          ConfigurationUtil.getInstance().getFrpGlobalParm("SHIPPING_PARMS", "PERSONAL_CREATIONS_DAYS_IN_TRANSIT"));

        if(logger.isDebugEnabled()){
            logger.debug("Days in tranist: " + daysInTransit); 
            logger.debug("Ship date is: " + shipDate);
        }

        /* Loop through the array of delivery dates until the ship date is
         * found or the date has been passed, save the index, and break loop.
         */
        for(int i = 0; i < deliveryDates.size(); i++) {
            OEDeliveryDate oedd = (OEDeliveryDate)deliveryDates.get(i);
            long delDateTime = sdf.parse(oedd.getDeliveryDate()).getTime();
            
            if(logger.isDebugEnabled()){
                logger.debug("Delivery date: " + oedd.getDeliveryDate());
                logger.debug("Ship date time: " + shipDateTime);
                logger.debug("Delivery date time: " + delDateTime);
            }
            
            if(delDateTime >= shipDateTime){
                startIndex = i;
                break;
            }
        }
        
        /* Loop through the delivery dates, starting at the ship date, and 
         * determine a valid delivery date based off the number of days in transit.
         */
        for(int i = startIndex; i < deliveryDates.size(); i++) {
            OEDeliveryDate oeDeliveryDate = (OEDeliveryDate)deliveryDates.get(i);
            
            /* If the date is a deliverable date, increment the counter. */
            if(oeDeliveryDate.getDeliverableFlag().equalsIgnoreCase("Y")) {
                if(logger.isDebugEnabled()){
                    logger.debug("Delivery date count: " + deliveryDateCount); 
                    logger.debug("Delivery date: " + oeDeliveryDate.getDeliveryDate()); 
                }

                /* If valid delivery dates have been found, return the date. */
                if(deliveryDateCount == daysInTransit) {
                    deliveryDate = oeDeliveryDate.getDeliveryDate();
                    return deliveryDate;
                }

                deliveryDateCount++;
            }
        }
        
        return deliveryDate;
    }
    private static void  generateShipMethods(HashMap shippingMethods,Document document,Element methodElement,Element entry){
    	
    	Collection methodCollection = shippingMethods.keySet();
        Iterator iter = methodCollection.iterator();
        while ( iter.hasNext() )
        {
            String code = (String) iter.next();

            BigDecimal cost = (BigDecimal) ((ShippingMethod)shippingMethods.get(code)).getDeliveryCharge();
            String description = ((ShippingMethod)shippingMethods.get(code)).getDescription();
            logger.info("description:::"+description);
            entry = (Element) document.createElement("method");
                if(cost == null)
                {
                    entry.setAttribute("cost", "");
                }
                else
                {
                    entry.setAttribute("cost", cost.toString());
                }
            entry.setAttribute("description", description);
            entry.setAttribute("code", code);
            methodElement.appendChild(entry);
        }
    }
}