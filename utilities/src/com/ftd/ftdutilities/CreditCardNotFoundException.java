package com.ftd.ftdutilities;

public class CreditCardNotFoundException extends RuntimeException
{
    private static final long serialVersionUID = 1L;

    public CreditCardNotFoundException() 
    {
        super();
    }

   /**
   * Constructs a new CustProfileTimeoutException with a message string
   * @param message
   */
    public CreditCardNotFoundException(String message)
    {
        super(message);
    }
    
  /**
   * Constructs a CustProfileTimeoutException with a message string, and a base exception
   * 
   * @param message
   * @param ex
   */
    public CreditCardNotFoundException(String message, Exception ex)
    {
      super(message, ex);
    }
    
}