package com.ftd.ftdutilities;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.net.URLEncoder;
import java.sql.Connection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXParseException;

import com.ftd.customer.client.CCDataRestServiceClient;
import com.ftd.customer.client.CustomerRestServiceClient;
import com.ftd.customer.client.FreeShippingRestServiceClient;
import com.ftd.customer.core.domain.CustomerFSAutoRenewCCResult;
import com.ftd.customer.core.domain.CustomerFullProfile;
import com.ftd.customer.services.rest.core.request.FSMAutoRenewCCResultRequest;
import com.ftd.customer.services.rest.core.response.GenericResponse;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.handlers.GlobalParmHandler;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;
import com.ftd.osp.utilities.vo.CustomerFSHistory;
import com.ftd.osp.utilities.vo.CustomerFSMDetails;
import com.ftd.osp.utilities.vo.MembershipDetailsVO;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.util.validation.ErrorMessage;
import com.ftd.util.validation.ErrorMessages;


public class FTDCAMSUtils {
	private static Logger logger = new Logger("com.ftd.ftdutilities.FTDCAMSUtils");

	public static final String FREESHIPPING_MEMBER_TYPE = "FS";
	public static final String PREMIERCIRCLE_MEMBER_TYPE = "PC";	
	private static final String CAMS_TIMEOUT_ENABLE_FLAG="CAMS_TIMEOUT_ENABLE_FLAG";
	public static final String SERVICE = "SERVICE";
	public static final String PROFILE_SERVICE_ENABLED = "PROFILE_SERVICE_ENABLED";
	public static final String CAMS_ENABLE_FLAG = "ON";
	private static final String GET_MEMBERSHIP_CAMS_TIMEOUT = "CAMS Timeout occured, fees may need to be manually adjusted.  Check if the buyer is FS member, Order: ";
	private static String CAMS_REQUEST_TIMEOUT="TimeoutException For CAMS Service";

	/**
	 * getMembershipData handles exceptions and return null in case exception.
	 * @Params emailList,fsCutoffDate
	 * @return Map object with FS Member details
	 */
	public static Map<String, MembershipDetailsVO> getMembershipData(List<String> emailList, Date fsCutoffDate) {
		Map<String, MembershipDetailsVO> membershipMap = null;
		try {
			membershipMap = getMembershipDataWithTimeoutException(emailList,
					fsCutoffDate);
		} catch (CustProfileTimeoutException timeoutEx) {
			logger.error("Service Returned CustProfileTimeoutException: " + timeoutEx.getMessage());
			membershipMap = null; 
		} catch (Exception Ex) {
			logger.error("Exception in getMembershipData() : " + Ex.getMessage());
			membershipMap = null;
		}
		return membershipMap;
	}
	
	/**
	 * getMembershipData handles exceptions and return null in case exception.
	 * 
	 * @Params emailList,fsCutoffDate
	 * 
	 * @return Map object with FS member details
	 */
	public static Map<String, MembershipDetailsVO> getMembershipData(List<String> emailList, Date fsCutoffDate,
			boolean sendAlertOnTimeout, String orderNumber) {
		Map<String, MembershipDetailsVO> membershipMap = null;
		try {
			membershipMap = getMembershipDataWithTimeoutException(emailList, fsCutoffDate);
		} catch (CustProfileTimeoutException timeoutEx) {
			logger.error("Service Returned CustProfileTimeoutException: " + timeoutEx.getMessage());
			if (sendAlertOnTimeout && !StringUtils.isEmpty(orderNumber)) {
				sendTimeOutSystemMessage(GET_MEMBERSHIP_CAMS_TIMEOUT + " " + orderNumber);
			}
			membershipMap = null;
		} catch (Exception Ex) {
			logger.error("Exception in getMembershipData() : " + Ex.getMessage());
			membershipMap = null;
		}
		return membershipMap;
	}	
	
	/**
	 * getMembershipDataWithTimeoutException throws CustProfileTimeoutException .We are calling this method from RecalculateOrderBO.recalculate(...)
	 * for re-processing message in MDB 
	 * @Params emailList,fsCutoffDate
	 * @return Map object with fs member details
	 * 
	 */
	public static Map<String, MembershipDetailsVO> getMembershipDataWithTimeoutException(List<String> emailList, Date fsCutoffDate) {	
		
		if("Y".equalsIgnoreCase(getProfileEnabledFlag())){
			return ProfileServiceUtils.getMembershipDataWithTimeoutException(emailList, fsCutoffDate);
		} 		
		
		Map<String, MembershipDetailsVO> membershipMap = null;
		GetMethod method = null;
		
		try {			
			  String camsServiceURL = "";
			  String camsUserName = null;
			  String camsPassword = null;
			  String email = "";
			 
				if(emailList == null || emailList.toString() == null || StringUtils.isEmpty(emailList.toString().trim())  || fsCutoffDate == null)
				{
					return null;
				}
				email = emailList.get(0);
				Calendar cal = Calendar.getInstance();
				String memTypeAndDate = "FS-"+fsCutoffDate.getTime()/1000+",PC-"+cal.getTimeInMillis()/1000;
				camsServiceURL = getCAMSServiceURL();
				camsUserName = getCAMSUserName();
				camsPassword = getCAMSPassword();
				final HttpClient client = new HttpClient(); 
				
				String url = new StringBuilder(camsServiceURL).append("getMembershipData?emailId=").append(URLEncoder.encode(email, "UTF-8")).append("&membershipType=").append(memTypeAndDate).toString();
				logger.info("CAMS URL for getMembershipDataWithTimeoutException: " + url);	
				method = new GetMethod(url); 
				
				setCAMSAuthCredentials(method, camsUserName, camsPassword);
				int statusCode=0;
				
				if(getCAMSTimeoutEnabledFlag().equalsIgnoreCase("ON")){
					statusCode = getResponseTimeOut(client,method);
				} else{
					statusCode = client.executeMethod(method);
				}
				
				/**
				 * checking the response code. If the response code is 404 then Apollo considers that the email is not valid for free shipping.
				 */
				if (statusCode==404){
					logger.error("Http status 404, requested resource is not available."+ method.getResponseBodyAsString());
					return null;
				}
				InputStream responseBody = method.getResponseBodyAsStream();
				DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
				Document doc = docBuilder.parse(responseBody);
				//DOMUtil.print(doc, System.out);
				method.releaseConnection();
				XPath xpath = XPathFactory.newInstance().newXPath();
				
				NodeList errNodes = (NodeList)xpath.evaluate("/CustomerServiceResponse/errors/error", doc, XPathConstants.NODESET);
				if(errNodes != null && errNodes.getLength() > 0){
					for(int i = 0 ; i < errNodes.getLength(); i++){
						Element errElement = (Element) errNodes.item(i);
						String errorText = errElement.getAttribute("message");
			            logger.error(errorText);
					}
					membershipMap = new HashMap<String, MembershipDetailsVO>();
					membershipMap.put(PREMIERCIRCLE_MEMBER_TYPE, null);
					membershipMap.put(FREESHIPPING_MEMBER_TYPE, null);
					return membershipMap;
				}
				NodeList resNodes = (NodeList)xpath.evaluate("/CustomerServiceResponse/customerMemberships", doc, XPathConstants.NODESET);
				if (resNodes != null && resNodes.getLength() > 0){
					membershipMap = new HashMap<String, MembershipDetailsVO>();
					for(int x = 0; x<resNodes.getLength(); x++ ){
						MembershipDetailsVO memDetails = new MembershipDetailsVO();
						if(resNodes != null && resNodes.getLength() > 0 && resNodes.item(x).getChildNodes() != null && resNodes.item(x).getChildNodes().getLength() > 0){
							NodeList childNodes = resNodes.item(x).getChildNodes();
							String key = null;
							for(int i = 0 ; i < childNodes.getLength(); i++){
								if(childNodes.item(i).getNodeName().equalsIgnoreCase("atr_1")){
									memDetails.setGroupId(childNodes.item(i).getFirstChild().getNodeValue());
								}
								if(childNodes.item(i).getNodeName().equalsIgnoreCase("status")){
									memDetails.setStatus(childNodes.item(i).getFirstChild().getNodeValue());
								}
								if(childNodes.item(i).getNodeName().equalsIgnoreCase("membershipId")){
									memDetails.setMembershipId(childNodes.item(i).getFirstChild().getNodeValue());
								}
								if(childNodes.item(i).getNodeName().equalsIgnoreCase("membershipType")){
									key = childNodes.item(i).getFirstChild().getNodeValue();
									memDetails.setMembershipType(key);
								}
								if(childNodes.item(i).getNodeName().equalsIgnoreCase("membershipDescription")){
									memDetails.setMembershipType(childNodes.item(i).getFirstChild().getNodeValue());
								}
								if(childNodes.item(i).getNodeName().equalsIgnoreCase("expireDate")){
									memDetails.setExpiryDate(new Date(Long.parseLong(childNodes.item(i).getFirstChild().getNodeValue())*1000));
								}
								if(childNodes.item(i).getNodeName().equalsIgnoreCase("startDate")){
									memDetails.setStartDate(new Date(Long.parseLong(childNodes.item(i).getFirstChild().getNodeValue())*1000));
								}
								if(childNodes.item(i).getNodeName().equalsIgnoreCase("joinDate")){
									memDetails.setJoinDate(new Date(Long.parseLong(childNodes.item(i).getFirstChild().getNodeValue())*1000));
								}
								if(childNodes.item(i).getNodeName().equalsIgnoreCase("isMembershipBenefitsApplicable")){
									if("true".equalsIgnoreCase(childNodes.item(i).getFirstChild().getNodeValue())){
										memDetails.setMembershipBenefitsApplicable(true);
									}else{
										memDetails.setMembershipBenefitsApplicable(false);
									}
										
								}
								
								if(childNodes.item(i).getNodeName().equalsIgnoreCase("errorMessages")){
									System.out.println("Found Errors ...");
									NodeList errorNodes = childNodes.item(i).getChildNodes();
									List<String> errors = new ArrayList<String>();
									for(int j = 0 ; j < errorNodes.getLength(); j++){
										Element errElement = (Element) errorNodes.item(j);
										String errorText = errElement.getAttribute("message");
										String errorCode = errElement.getAttribute("code");
										System.out.println(errorCode+ " - "+errorText);
										errors.add(errorCode+ " - "+errorText);
									
									}
									memDetails.setErrorList(errors);
								}
								
							}
							membershipMap.put(key, memDetails);
						}
					}	
				}
			
		}catch(SAXParseException spe){
			logger.error("Service Returned unparsable response : "+spe.getMessage());
			return null;
		}
		catch(CustProfileTimeoutException timeout)
		{
			logger.error("TimeoutException For CAMS... in getMembershipDataWithTimeoutException() ", timeout);
			throw timeout;
		}
		catch(Exception e){
			String errorMsg= "Error calling CAMS from FTDCAMSUtils->getMembershipData";
			logger.error(errorMsg , e);
			sendSystemMessage(errorMsg);
		}finally {
			releaseClientConn(method);
	    }
		return membershipMap;
  }
	
	private static void releaseClientConn(GetMethod method) {
		try {
			if (method != null) {
				method.releaseConnection();
			}
		} catch (Exception e) {
			logger.error("Error closing http client connection, " + e);
		}
	}

	  public static String getCAMSServiceURL()
	  {
		try {
			return ConfigurationUtil.getInstance().getFrpGlobalParm("SERVICE", "CAMS_SERVICE_URL");
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage());
		}
	  }
	  
	  public static String getCAMSOrderServiceURL()
	  {
		try {
			return ConfigurationUtil.getInstance().getFrpGlobalParm("SERVICE", "CAMS_SERVICE_ORDER_URL");
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage());
		}
	  }
	  
	  public static String getCAMSUserName()
	  {
		  try {
				return ConfigurationUtil.getInstance().getSecureProperty("SERVICE", "CAMS_SERVICE_USER_NAME");
			} catch (Exception e) {
				throw new RuntimeException(e.getMessage());
			}
	  }
	  
	  public static String getCAMSPassword()
	  {
		  try {
				return FTDCommonUtils.getPasswordFromSSHStore("SERVICE", "CAMS_SERVICE_PASSWORD_FILE");
			} catch (Exception e) {
				throw new RuntimeException(e.getMessage());
			}
	  }
	  
	  public static void setCAMSAuthCredentials(GetMethod method, String userName, String password){
		  method.addRequestHeader("x-user-name", userName);
		  method.addRequestHeader("x-password", password);
	  }

	/**
	 * @param emailId
	 * @return
	 * @throws Exception
	 */
	public static CustomerFSMDetails getFSMDetails(String emailId) throws Exception {
		
		if("Y".equalsIgnoreCase(getProfileEnabledFlag())){
			return ProfileServiceUtils.getFSMDetails(emailId);
		}
		
		CustomerFSMDetails camscustomerfsmDetails = null;
		GetMethod method = null;
		try {
			String camsServiceURL = "";
			String camsUserName = null;
			String camsPassword = null;
			
				if (emailId == null || StringUtils.isEmpty(emailId)) { 
					throw new Exception("Email Id cannot be null");
				}
	
				camsServiceURL = getCAMSServiceURL();
				camsUserName = getCAMSUserName();
				camsPassword = getCAMSPassword();
				final HttpClient client = new HttpClient();
	 
				String url = (new StringBuilder()).append(camsServiceURL).append("getFSMDetails/").append(emailId).toString();
				logger.info("CAMS URL for getFSMDetails: " + url);
				
				method = new GetMethod(url);
				setCAMSAuthCredentials(method, camsUserName, camsPassword);
				
				int statusCode = 0;
	
				if (getCAMSTimeoutEnabledFlag().equalsIgnoreCase("ON")) {
					statusCode = getResponseTimeOut(client, method);
				} else {
					statusCode = client.executeMethod(method);
				}
	
				/**
				 * checking the response code. If the response code is 404 then
				 * Apollo considers that the email is not valid for free shipping.
				 */
				if (statusCode == 404) { 
					throw new Exception("Unable to process CAMS request. Http status 404, requested resource is not available." + method.getResponseBodyAsString());
				}
				
				InputStream responseBody = method.getResponseBodyAsStream();
				DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
				Document doc = docBuilder.parse(responseBody);
				method.releaseConnection();
				
				XPath xpath = XPathFactory.newInstance().newXPath();
				NodeList resNodes = (NodeList) xpath.evaluate("/FreeShippingServiceResponse/customerFSMDetails", doc, XPathConstants.NODESET);
				DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				
				if (resNodes != null && resNodes.getLength() > 0 && resNodes.item(0).getChildNodes() != null && resNodes.item(0).getChildNodes().getLength() > 0) {
					
					NodeList childNodes = resNodes.item(0).getChildNodes();
					camscustomerfsmDetails = new CustomerFSMDetails();
					for (int i = 0; i < childNodes.getLength(); i++) {
						
						if (childNodes.item(i).getNodeName().equalsIgnoreCase("fsStartDate")) {
							String fsStartDate = childNodes.item(i).getFirstChild().getNodeValue();
							camscustomerfsmDetails.setFsStartDate(sdf.parse(fsStartDate));
						}
						
						if (childNodes.item(i).getNodeName().equalsIgnoreCase("fsEndDate")) {
							String fsEndDate = childNodes.item(i).getFirstChild().getNodeValue();
							camscustomerfsmDetails.setFsEndDate(sdf.parse(fsEndDate));
						}
						
						if (childNodes.item(i).getNodeName().equalsIgnoreCase("fsEndDate")) {
							String fsEndDate = childNodes.item(i).getFirstChild().getNodeValue();
							camscustomerfsmDetails.setFsEndDate(sdf.parse(fsEndDate));
						}
						
						if (childNodes.item(i).getNodeName().equalsIgnoreCase("fsJoinDate")) {
							String fsJoinDate = childNodes.item(i).getFirstChild().getNodeValue();
							camscustomerfsmDetails.setFsJoinDate(sdf.parse(fsJoinDate));
						}
						
						if (childNodes.item(i).getNodeName().equalsIgnoreCase("fsAutoRenew")) {
							String fsAutoRenew = childNodes.item(i).getFirstChild().getNodeValue();
							camscustomerfsmDetails.setFsAutoRenew(fsAutoRenew);
						}
						
						if (childNodes.item(i).getNodeName().equalsIgnoreCase("fsMember")) {
							String fsMember = childNodes.item(i).getFirstChild().getNodeValue();
							camscustomerfsmDetails.setFsMember(fsMember);
						}
						
						if (childNodes.item(i).getNodeName().equalsIgnoreCase("fsmOrderNumber")) {
							String fsmOrderNumber = childNodes.item(i).getFirstChild().getNodeValue();
							camscustomerfsmDetails.setFsmOrderNumber(fsmOrderNumber);
						}
						
						if (childNodes.item(i).getNodeName().equalsIgnoreCase("fsTerm")) {
							String fsTerm = childNodes.item(i).getFirstChild().getNodeValue();
							camscustomerfsmDetails.setFsTerm(Integer.parseInt(fsTerm));
						}
					}
				}  
				
		} catch (SAXParseException spe) { 
			throw new Exception("Service Returned unparsable response : " + spe.getMessage());
		} catch (CustProfileTimeoutException timeout) {
			throw new Exception("TimeoutException For CAMS... in getFSMDetails() " + timeout.getMessage());
		} catch (Exception e) {
			String errorMsg= "Error calling CAMS from FTDCAMSUtils->getFSMDetails";
			logger.error(errorMsg , e);
			sendSystemMessage(errorMsg);
		} finally {
			releaseClientConn(method);
		}
		return camscustomerfsmDetails;
	}
	
	public static List<CustomerFSHistory> getCustomerFSHistory(List<String> emailIdList, List<String> eventTypeList) throws Exception {
		
		if("Y".equalsIgnoreCase(getProfileEnabledFlag())){
			return ProfileServiceUtils.getCustomerFSHistory(emailIdList, eventTypeList);
		} 
		
		List<CustomerFSHistory> customerfsDetailsList = null;
		GetMethod method = null;
		try {			
			String camsServiceURL = "";
			String camsUserName = null;
			String camsPassword = null;
			CustomerFSHistory fsHistoryObj = null;
			String emailIds = null;
			for (String emailId : emailIdList) {
				if (emailIds == null) {
					emailIds = emailId;
				} else {
					emailIds = emailIds + "," + emailId;
				}
			}
			
			if (emailIds.endsWith(",")) {
				emailIds = emailIds.substring(0, emailIds.length() - 1);
			}
			
			String eventTypes = null;
			for (String eventType : eventTypeList) {
				if (eventTypes == null) {
					eventTypes = eventType;
				} else {
					eventTypes = eventTypes + "," + eventType;
				}
			}
			
			if (eventTypes.endsWith(",")) {
				eventTypes = eventTypes.substring(0, eventTypes.length() - 1);
			}
			
			camsServiceURL = getCAMSServiceURL();
			camsUserName = getCAMSUserName();
			camsPassword = getCAMSPassword();
			final HttpClient client = new HttpClient();
	
			String url = (new StringBuilder()).append(camsServiceURL).append("getCustomerFSHistory/").append(emailIds).append("/").append(eventTypes).toString();
			logger.info("CAMS URL for getCustomerFSHistory: " + url); 
			
			method = new GetMethod(url);
			setCAMSAuthCredentials(method, camsUserName, camsPassword);			
			int statusCode = 0;
	
			if (getCAMSTimeoutEnabledFlag().equalsIgnoreCase("ON")) {
				statusCode = getResponseTimeOut(client, method);
			} else {
				statusCode = client.executeMethod(method);
			}
	
			/**
			 * checking the response code. If the response code is 404 then
			 * Apollo considers that the email is not valid for free shipping.
			 */
			if (statusCode == 404) { 
				throw new Exception("Unable to process CAMS request. Http status 404, requested resource is not available." + method.getResponseBodyAsString());
			}
			
			InputStream responseBody = method.getResponseBodyAsStream();
			DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
			Document doc = docBuilder.parse(responseBody);
						
			DOMUtil.print(doc, System.out);
			//logger.info(DOMUtil.domToString(doc)); 	 
			
			XPath xpath = XPathFactory.newInstance().newXPath();
			NodeList resNodes = (NodeList) xpath.evaluate("/FreeShippingServiceResponse/customerFSHistory", doc, XPathConstants.NODESET); 
			
			if (resNodes != null && resNodes.getLength() > 0 && resNodes.item(0).getChildNodes() != null && resNodes.item(0).getChildNodes().getLength() > 0) {	
				customerfsDetailsList = new ArrayList<CustomerFSHistory>();
				for (int i=0; i<resNodes.getLength(); i++) { 
					Document document = DOMUtil.getDefaultDocument(); 		        
					document.appendChild(document.importNode(resNodes.item(i), true));					
					com.ftd.customer.core.domain.CustomerFSHistory fsHistory = (com.ftd.customer.core.domain.CustomerFSHistory) marshallAsObject(DOMUtil.domToString(document), com.ftd.customer.core.domain.CustomerFSHistory.class);
					fsHistoryObj = new CustomerFSHistory();
					BeanUtils.copyProperties(fsHistoryObj, fsHistory);
					logger.debug("fsHistoryObj :"+fsHistoryObj.toString());
					customerfsDetailsList.add(fsHistoryObj);
				} 
			}
		
		} catch (SAXParseException spe) { 
			throw new Exception("Service Returned unparsable response : " + spe.getMessage());
		} catch (CustProfileTimeoutException timeout) {
			throw new Exception("TimeoutException For CAMS... in getCustomerFSHistory() " + timeout.getMessage());
		} catch (Exception e) {
			String errorMsg= "Error calling CAMS from FTDCAMSUtils->getCustomerFSHistory";
			logger.error(errorMsg , e);
			sendSystemMessage(errorMsg);
		} finally {
			releaseClientConn(method);
		}
		
		return customerfsDetailsList;
	}
	
	/** Send System Messages
	 * @param errorMessage
	 */
	private static void sendSystemMessage(String errorMessage) {		
		try {
			Connection conn = DataSourceUtil.getInstance().getConnection("ORDER SCRUB");
			SystemMessengerVO sysMessage = new SystemMessengerVO();
			sysMessage.setLevel(SystemMessengerVO.LEVEL_PRODUCTION);
		    sysMessage.setSource("Utilities_Profile_Service");
		    sysMessage.setType("System Exception");
		    sysMessage.setMessage(errorMessage);
			SystemMessenger.getInstance().send(sysMessage, conn);
			
		} catch (Throwable t) {
	    	logger.error("Sending system message failed. Message = " + errorMessage);
			logger.error(t);
	    }	
	}
	
	
	/**
	 * Sends an email to group to check if there is any manual intervention is required
	 * @param orderNumber 
	 */
	private static void sendTimeOutSystemMessage(String errorMessage) {		
		try {
			Connection conn = DataSourceUtil.getInstance().getConnection("ORDER SCRUB");
			SystemMessengerVO sysMessage = new SystemMessengerVO();
			sysMessage.setLevel(SystemMessengerVO.LEVEL_PRODUCTION);
		    sysMessage.setSource("CAMS Timeout - Manual Intervention required");
		    sysMessage.setSubject("CAMS Timeout - Manual Intervention required");
		    sysMessage.setType("System Exception");
		    sysMessage.setMessage(errorMessage);
		    SystemMessenger.getInstance().send(sysMessage, conn);			
		} catch (Throwable t) {
	    	logger.error("Sending system message failed. Message  " + errorMessage);
			logger.error(t); // do not propagate the error
	    }	
	}
	
	
	/**
	 * getResponseTimeOut returns statuscode of the response i.e 200 | 404 
	 * @Params final Httpclinet and final GetMethod references
	 * @return status codes i.e 200| 404 |if server is not up then it shows empty response code
	 * @throws Exception
	 */
	private static int getResponseTimeOut(final HttpClient client,final GetMethod method) throws Exception {		
		String result="";
		String camserrmsg = "CAMS Service request  timeout";
		java.util.concurrent.ExecutorService executorService = java.util.concurrent.Executors.newSingleThreadExecutor();
		java.util.concurrent.Future<String> future = executorService.submit(new java.util.concurrent.Callable<String>() 
		    {
			   public String call() throws InterruptedException, HttpException, IOException
			      {  
				     return  String.valueOf(client.executeMethod(method)); 
				  }
			});
		
		try {
			result = (String) future.get(ProfileServiceUtils.getCustProfileResponseTimeout(), java.util.concurrent.TimeUnit.MILLISECONDS);
			logger.info("CAMS service response status code : " + result);
		} catch (TimeoutException timeout) {
			logger.error(camserrmsg, timeout);
			throw new CustProfileTimeoutException(camserrmsg);
		} catch (Exception ex) {
			logger.error("Exception while calling CMAS service", ex);
		} finally {
			future.cancel(true);
		}
		if (null == result || "".equalsIgnoreCase(result)) {
			String msg = "CAMS service shut down .Please contack system Admin ";
			logger.error(msg);
			throw new CustProfileTimeoutException(msg);
		}
		return Integer.parseInt(result);
	}
	
	public static String getProfileEnabledFlag(){
		 String profileServiceEnabledFlag = "Y";
		 try{
			 GlobalParmHandler globalParamHandler = (GlobalParmHandler) CacheManager.getInstance().getHandler("CACHE_NAME_GLOBAL_PARM");
			  if(globalParamHandler != null){
				 profileServiceEnabledFlag = globalParamHandler.getFrpGlobalParm(SERVICE, PROFILE_SERVICE_ENABLED);
		      }else {
		    	  profileServiceEnabledFlag = "Y";
		      }
		 }catch(Exception e){
			 profileServiceEnabledFlag = "Y";
		 }
		 logger.info("profileServiceEnabledFlag value = "+profileServiceEnabledFlag);
		 return profileServiceEnabledFlag;
	}
	
	public static String getCAMSTimeoutEnabledFlag() {
		String camsTimeoutEnabledFlag="OFF";
		try{
			camsTimeoutEnabledFlag = ConfigurationUtil.getInstance().getFrpGlobalParm(SERVICE, CAMS_TIMEOUT_ENABLE_FLAG);
			if(StringUtils.isBlank(camsTimeoutEnabledFlag))
				camsTimeoutEnabledFlag="OFF";
		} catch(Exception e) {
			camsTimeoutEnabledFlag="OFF";
			e.printStackTrace();
		}
		logger.info("camsTimeoutEnabledFlag value = "+camsTimeoutEnabledFlag);
		return camsTimeoutEnabledFlag;
	}
	
	public static Object marshallAsObject(String xml, Class<?> clazz) throws Exception {
		Object object = null;
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(clazz);
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			StringReader sw = new StringReader(xml);
			object = unmarshaller.unmarshal(sw);
		} catch (JAXBException e) {
			throw new Exception(e);
		}
		return object;
	}
	
	
	public static List<String> getExpiredEmailsByExpireDate(Date startDate, Date endDate)throws Exception
	{
		List<String> emailIds = null;
		if("Y".equalsIgnoreCase(getProfileEnabledFlag())){
			emailIds = ProfileServiceUtils.getExpiredEmailsByExpireDate(startDate, endDate);
		}else{
			CustomerRestServiceClient fsServiceClient = getCustomerRestServiceClient();
			String startDtStr = formatDate(startDate, "yyyy-MM-dd");
			String endDtStr = formatDate(endDate, "yyyy-MM-dd");
			logger.debug("Getting FS Expired Emails from CAMS between dates: "+startDtStr+" and "+endDtStr);
			GenericResponse<List<String>> fsmListResp = null;
			if(CAMS_ENABLE_FLAG.equalsIgnoreCase(getCAMSTimeoutEnabledFlag()))
			    fsmListResp = getExpiredEmailsByExpireDateWithTimeoutException(startDtStr,endDtStr);
			else
				fsmListResp = fsServiceClient.getFSCustomersExpiredByDateRange(startDtStr, endDtStr);
			
			emailIds = fsmListResp.getData();
		}
		return emailIds;
	}
	
	private static GenericResponse<List<String>> getExpiredEmailsByExpireDateWithTimeoutException(final String startDtStr,final String endDtStr) throws InterruptedException, ExecutionException
	{
		final CustomerRestServiceClient fsServiceClient = getCustomerRestServiceClient();
		GenericResponse<List<String>> fsmListResp = null;
		
		java.util.concurrent.ExecutorService executorService = java.util.concurrent.Executors.newSingleThreadExecutor();
		java.util.concurrent.Future<GenericResponse<List<String>>> future = executorService.submit(new java.util.concurrent.Callable<GenericResponse<List<String>>>() 
			    {
				   public GenericResponse<List<String>> call() 
				      {  
					     return fsServiceClient.getFSCustomersExpiredByDateRange(startDtStr, endDtStr);
					  }
				});
		try{
			fsmListResp= (GenericResponse<List<String>>)future.get(ProfileServiceUtils.getCustProfileResponseTimeout(), java.util.concurrent.TimeUnit.MILLISECONDS);
			logger.debug("getExpiredEmailsByExpireDateWithTimeoutException");
		}catch (TimeoutException timeout) {
			logger.error(timeout);
	        throw new CustProfileTimeoutException(CAMS_REQUEST_TIMEOUT);
	    }  finally {
			 future.cancel(true);
		}
		return fsmListResp;
	}
	
	public static CustomerFSMDetails getFSM(String email){
		String emailId = email.trim();
		CustomerFSMDetails fsmDetails = null;
		GenericResponse<com.ftd.customer.core.domain.CustomerFSMDetails> fsmDetails1 = null;
		try {
			if("Y".equalsIgnoreCase(getProfileEnabledFlag())){
				logger.info("Calling Profile Service for getting FSMDetails");
				fsmDetails = ProfileServiceUtils.getFSMDetails(email);
			}
			else{
			    FreeShippingRestServiceClient fsServiceClient = getFreeShippingRestServiceClient();
				if(CAMS_ENABLE_FLAG.equalsIgnoreCase(getCAMSTimeoutEnabledFlag())){
					logger.info("Calling CAMS for getting FSMDetails");
					fsmDetails1 = getFSMWithTimeoutException(fsServiceClient, emailId);
				} else {
					fsmDetails1 = fsServiceClient.getFSM(emailId);
				}
				if(fsmDetails1 == null || fsmDetails1.getData()==null)
				{
					logger.info("No FSM Details found in CAMS for Email("+emailId+")");
					sendSystemMessage("No FSM Details found in CAMS for Email("+emailId+")");
				}
				com.ftd.customer.core.domain.CustomerFSMDetails fsmDetails2 = fsmDetails1.getData();
				fsmDetails = new CustomerFSMDetails();
				BeanUtils.copyProperties(fsmDetails, fsmDetails2);
			}
		} catch(CustProfileTimeoutException timeout) {
			throw timeout;
		} catch(Exception e){
			logger.error("Error occurred while getting FSM Details for email "+email+" , Errormsg" +e.getMessage());
		}
		return fsmDetails;
	}
	
	private static GenericResponse<com.ftd.customer.core.domain.CustomerFSMDetails> getFSMWithTimeoutException(final FreeShippingRestServiceClient fsServiceClient , String email) throws InterruptedException, ExecutionException
	{
		final String emailId = email;
		GenericResponse<com.ftd.customer.core.domain.CustomerFSMDetails> fsmDetails=null;
		 
		java.util.concurrent.ExecutorService executorService = java.util.concurrent.Executors.newSingleThreadExecutor();
		java.util.concurrent.Future<GenericResponse<com.ftd.customer.core.domain.CustomerFSMDetails>> future = executorService.submit(new java.util.concurrent.Callable<GenericResponse<com.ftd.customer.core.domain.CustomerFSMDetails>>() 
		    {
			   public GenericResponse<com.ftd.customer.core.domain.CustomerFSMDetails> call() 
			      {  
				     return fsServiceClient.getFSM(emailId);
				  }
			});
		try{
			fsmDetails= (GenericResponse<com.ftd.customer.core.domain.CustomerFSMDetails>)future.get(ProfileServiceUtils.getCustProfileResponseTimeout(), java.util.concurrent.TimeUnit.MILLISECONDS);
			logger.debug("getFSMWithTimeoutException");
		}
		catch (TimeoutException  timeout)
		   {
	        throw new CustProfileTimeoutException(CAMS_REQUEST_TIMEOUT);
	       }
		finally
		{
			 future.cancel(true);
		}
		return fsmDetails;
	}
	
	
	public static void updateProgramStatus(String emailAddress, String externalOrderNumber, String membershipStatus, boolean redeliveryFlag, String payLoad) throws Exception
	{
		boolean status = false;
		if("Y".equalsIgnoreCase(getProfileEnabledFlag())){
			logger.debug("Calling Profile for updateProgramStatus() - email:"+emailAddress+ ", externalOrderNumber:"+externalOrderNumber+",membershipStatus:"+membershipStatus+", redeliveryFlag:"+redeliveryFlag);
			status = ProfileServiceUtils.updateProgramStatusInProfile(emailAddress, externalOrderNumber, membershipStatus, 0, redeliveryFlag, payLoad);
		} else {
			try	{			
				logger.debug("Calling CAMS for updateProgramStatus() - email:"+emailAddress+ ", externalOrderNumber:"+externalOrderNumber+",membershipStatus:"+membershipStatus+", redeliveryFlag:"+redeliveryFlag);
				String fsMemberStatus = null;
				if(membershipStatus.equalsIgnoreCase("A"))
				{
					fsMemberStatus = "1";
				}else if (membershipStatus.equalsIgnoreCase("I"))
				{
					fsMemberStatus = "0";
				}
				FreeShippingRestServiceClient client = getFreeShippingRestServiceClient();
				
				com.ftd.customer.services.rest.core.request.CustomerFSMDetailsRequest req = new com.ftd.customer.services.rest.core.request.CustomerFSMDetailsRequest();
				com.ftd.customer.core.domain.CustomerFSMDetails fsmDetails = new com.ftd.customer.core.domain.CustomerFSMDetails();
				req.setEmail(emailAddress);
				fsmDetails.setFsMember(fsMemberStatus);
				fsmDetails.setFsmOrderNumber(externalOrderNumber);
				req.setCustomerFSMDetails(fsmDetails);
				
				GenericResponse<Integer> response = null;
				if(CAMS_ENABLE_FLAG.equalsIgnoreCase(getCAMSTimeoutEnabledFlag()))
					response = updateProgramStatusInCAMSWithTimeoutException(client, req);
				else
					response = client.updateCustomerFSMStatus(req);
				
				if(response != null && response.getErrors() != null) { 
					for (ErrorMessage errMsg : response.getErrors().getErrors()) {
						if("101".equals(errMsg.getCode())){
							logger.error(errMsg.getMessage());
							throw new Exception(errMsg.getMessage());
						}
					}
				}
				status = true;
							
			} catch(CustProfileTimeoutException timeout) {
				logger.error(timeout.getMessage(), timeout);
				throw timeout;
			} catch(Exception e) {
				logger.error(e.getMessage(), e);
				if(redeliveryFlag) {
					sendSystemMessage("Please use the payload to perform manual updates : "+payLoad+"  "+e.getMessage());
				}
				throw new RuntimeException(e.getMessage(), e);
			} 
		}
		if(!status){
			logger.error("Error occurred while updating the program status for email: "+emailAddress);
		}	 
	}
	
	private static GenericResponse<Integer> updateProgramStatusInCAMSWithTimeoutException(final FreeShippingRestServiceClient client,final com.ftd.customer.services.rest.core.request.CustomerFSMDetailsRequest req) throws InterruptedException, ExecutionException
	{
		GenericResponse<Integer> response = null;
		java.util.concurrent.ExecutorService executorService = java.util.concurrent.Executors.newSingleThreadExecutor();
		java.util.concurrent.Future<GenericResponse<Integer>> future = executorService.submit(new java.util.concurrent.Callable<GenericResponse<Integer>>() 
		    {
			   public GenericResponse<Integer> call() 
			      {  
				     return client.updateCustomerFSMStatus(req);
				  }
			});
		try{
			response= (GenericResponse<Integer>)future.get(ProfileServiceUtils.getCustProfileResponseTimeout(), java.util.concurrent.TimeUnit.MILLISECONDS);
			logger.debug("updateProgramStatusInCAMSWithTimeoutException");
		}
		catch (TimeoutException  timeout)
		   {
	        throw new CustProfileTimeoutException(CAMS_REQUEST_TIMEOUT);
	       }
		finally
		{
			 future.cancel(true);
		}
		return response;
	}
	
	
	public static boolean sendFSUpdateInfo(String emailAddress, int serviceDuration, String externalOrderNumber) throws Exception {
		boolean status = false;		
		if("Y".equalsIgnoreCase(getProfileEnabledFlag())){
			logger.info("Calling profile for sendFSUpdateInfo() - emailAddress: "+emailAddress+", serviceDuration: "+serviceDuration+",externalOrderNumber: "+externalOrderNumber);
			ProfileServiceUtils.updateProgramStatusInProfile(emailAddress, externalOrderNumber, null, serviceDuration, false, null);
		}
		else{
			logger.info("Calling CAMS for sendFSUpdateInfo() - emailAddress: "+emailAddress+", serviceDuration: "+serviceDuration+",externalOrderNumber: "+externalOrderNumber);
			final FreeShippingRestServiceClient fsServiceClient = getFreeShippingRestServiceClient();
			final com.ftd.customer.services.rest.core.request.CustomerFSMDetailsRequest fsmDetailsRequest = new com.ftd.customer.services.rest.core.request.CustomerFSMDetailsRequest();
			fsmDetailsRequest.setAutoRenewPeriod(serviceDuration);
			fsmDetailsRequest.setEmail(emailAddress);
			com.ftd.customer.core.domain.CustomerFSMDetails customerFSMDetails = new com.ftd.customer.core.domain.CustomerFSMDetails();
			customerFSMDetails.setFsmOrderNumber(externalOrderNumber);
			fsmDetailsRequest.setCustomerFSMDetails(customerFSMDetails);
			GenericResponse<Integer> response = null;
			if(CAMS_ENABLE_FLAG.equalsIgnoreCase(getCAMSTimeoutEnabledFlag()))
				response = sendFSUpdateInfoWithTimeoutException(fsServiceClient, fsmDetailsRequest);
			else
				response = fsServiceClient.updateCustomerFSMStatus(fsmDetailsRequest);
			ErrorMessages errors = response.getErrors();
			List<ErrorMessage> errMsgs = errors.getErrors();
			if(errMsgs != null && errMsgs.size() > 0)
			{
				logger.error("Error occured while updating free shipping duration :: "+emailAddress+"--"+ externalOrderNumber);
				throw new RuntimeException("Error occured while updating free shipping duration "+emailAddress+"--"+externalOrderNumber);
			}
		}		
		return status;
	}
	
	
	private static GenericResponse<Integer> sendFSUpdateInfoWithTimeoutException(final FreeShippingRestServiceClient fsServiceClient,final com.ftd.customer.services.rest.core.request.CustomerFSMDetailsRequest fsmDetailsRequest) throws InterruptedException, ExecutionException
	{
		GenericResponse<Integer> response=null; 
		java.util.concurrent.ExecutorService executorService = java.util.concurrent.Executors.newSingleThreadExecutor();
		java.util.concurrent.Future<GenericResponse<Integer>> future = executorService.submit(new java.util.concurrent.Callable<GenericResponse<Integer>>() 
		    {
			   public GenericResponse<Integer> call() 
			      {  
				     return fsServiceClient.updateCustomerFSMStatus(fsmDetailsRequest);
				  }
			});
		try{
			response= (GenericResponse<Integer>)future.get(ProfileServiceUtils.getCustProfileResponseTimeout(), java.util.concurrent.TimeUnit.MILLISECONDS);
			logger.debug("sendFSUpdateInfoWithTimeoutException");	
		}
		catch (TimeoutException  timeout) {
	        throw new CustProfileTimeoutException(CAMS_REQUEST_TIMEOUT);
	       }
		finally{
			 future.cancel(true);
		}
		return response;
	}
	
	public static void sendCreditCardStatus(String status, String paymentType, String emailAddress, String expirationMonth, String expirationYear, String lastFourDigits)
	{
		boolean respStatus = false;
		
		if("Y".equalsIgnoreCase(getProfileEnabledFlag())){ 
			respStatus = ProfileServiceUtils.sendCreditCardStatusToProfile(status, paymentType, emailAddress, expirationMonth, expirationYear, lastFourDigits);
		} else {
			FSMAutoRenewCCResultRequest ccRequest = new FSMAutoRenewCCResultRequest();
			CustomerFSAutoRenewCCResult ccResult = new CustomerFSAutoRenewCCResult();
			Calendar cal = Calendar.getInstance(); 
			long epochDateTime = (cal.getTime().getTime())/1000;
			try{
				FreeShippingRestServiceClient freeShippingServiceClient = getFreeShippingRestServiceClient();
				ccResult.setCcType(paymentType);
				ccResult.setEmail(emailAddress);
				ccResult.setTransactionTime(epochDateTime);
				ccResult.setResult(status);
				ccRequest.setCcResult(ccResult);
				GenericResponse<Integer> fullProfileResp = null;
				if(CAMS_ENABLE_FLAG.equalsIgnoreCase(getCAMSTimeoutEnabledFlag()))
					fullProfileResp = saveFSMAutoRenewCCResultWithTimeoutException(freeShippingServiceClient,ccRequest);
				else
					fullProfileResp = freeShippingServiceClient.saveFSMAutoRenewCCResult(ccRequest);
				
				if(fullProfileResp == null || fullProfileResp.getErrors()!=null && fullProfileResp.getErrors().hasErrors()){
					  List<ErrorMessage> errors = fullProfileResp.getErrors().getErrors();
					  for (ErrorMessage errorMessage : errors) {
						logger.error(errorMessage.getMessage());
					  }
					  sendSystemMessage("CAMS saveFSMAutoRenewCCResult method returned errors, look into account management logs for more details ");
				}
				else
					respStatus = true;
			}catch(CustProfileTimeoutException timeout){
				throw timeout;
			}
			catch(Exception e){
				logger.error("Error connecting to CAMS while calling saveFSMAutoRenewCCResult ", e);
				sendSystemMessage("Error connecting to CAMS while calling saveFSMAutoRenewCCResult "+e.getMessage());
			}
		}
		
		if(!respStatus){
			logger.error("Error occurred while updating the credit card status for email: "+ emailAddress );
		}
	}
	
	private static GenericResponse<Integer> saveFSMAutoRenewCCResultWithTimeoutException(final FreeShippingRestServiceClient freeShippingServiceClient,final FSMAutoRenewCCResultRequest ccRequest)throws InterruptedException, ExecutionException{
		GenericResponse<Integer> fullProfileResp =null;
		java.util.concurrent.ExecutorService executorService = java.util.concurrent.Executors.newSingleThreadExecutor();
		java.util.concurrent.Future<GenericResponse<Integer>> future = executorService.submit(new java.util.concurrent.Callable<GenericResponse<Integer>>() 
		    {
			   public GenericResponse<Integer> call() 
			      {  
				     return freeShippingServiceClient.saveFSMAutoRenewCCResult(ccRequest);
				  }
			});
		try{
			fullProfileResp= (GenericResponse<Integer>)future.get(ProfileServiceUtils.getCustProfileResponseTimeout(), java.util.concurrent.TimeUnit.MILLISECONDS);
			logger.debug("saveFSMAutoRenewCCResultWithTimeoutException");
		}
		catch (TimeoutException  timeout)
		   {
	        throw new CustProfileTimeoutException(CAMS_REQUEST_TIMEOUT);
	       }
		finally {
			 future.cancel(true);
		}
		return fullProfileResp;
	}
	
	
	public static GenericResponse<CustomerFullProfile> getCustomerFSMAndCCData(String emailId) throws Exception {
		GenericResponse<CustomerFullProfile> fullProfileResp = null;
		CustomerRestServiceClient custServiceClient = getCustomerRestServiceClient();

		if (CAMS_ENABLE_FLAG.equalsIgnoreCase(getCAMSTimeoutEnabledFlag())) {
			fullProfileResp = getFSAccountWithTimeoutException(custServiceClient, emailId);
		} else {
			fullProfileResp = custServiceClient.getCustomerFSMAndCCData(emailId);
		}
		if (fullProfileResp == null || fullProfileResp.getErrors() != null && fullProfileResp.getErrors().hasErrors()) {
			logger.error("Error in getting FSAccount for (" + emailId + ").");
			List<ErrorMessage> errors = fullProfileResp.getErrors().getErrors();
			for (ErrorMessage errorMessage : errors) {
				logger.error(errorMessage.getMessage());
			}
			sendSystemMessage("Error in Re-processing FSAutoRenewEmail(" + emailId + "):");
			return null;
		}
		return fullProfileResp;
	}
	
	
	private static GenericResponse<CustomerFullProfile> getFSAccountWithTimeoutException(final CustomerRestServiceClient custServiceClient,String emailId) throws InterruptedException, ExecutionException
	{
		final String email=emailId; 
		GenericResponse<CustomerFullProfile> fullProfileResp =null;
		java.util.concurrent.ExecutorService executorService = java.util.concurrent.Executors.newSingleThreadExecutor();
		java.util.concurrent.Future<GenericResponse<CustomerFullProfile>> future = executorService.submit(new java.util.concurrent.Callable<GenericResponse<CustomerFullProfile>>() 
		    {
			   public GenericResponse<CustomerFullProfile> call() 
			      {  
				     return custServiceClient.getCustomerFSMAndCCData(email);
				  }
			});
		try{
			fullProfileResp= (GenericResponse<CustomerFullProfile>)future.get(ProfileServiceUtils.getCustProfileResponseTimeout(), java.util.concurrent.TimeUnit.MILLISECONDS);
			logger.debug("getFSAccountWithTimeoutException");
		}
		catch (TimeoutException  timeout)
		   {
	        throw new CustProfileTimeoutException(CAMS_REQUEST_TIMEOUT);
	       }
		finally
		{
			 future.cancel(true);
		}
		return fullProfileResp;
		
	}
	
	public static final String formatDate(Date date, String dateFromat)
	{
		SimpleDateFormat sdf = new SimpleDateFormat(dateFromat);
		return sdf.format(date);
	}
	
	public static FreeShippingRestServiceClient getFreeShippingRestServiceClient() {
		return new FreeShippingRestServiceClient(getCAMSServiceURL(), getCAMSUserName(), getCAMSPassword());
	}
	
	public static CustomerRestServiceClient getCustomerRestServiceClient() {
		return new CustomerRestServiceClient(getCAMSServiceURL(), getCAMSUserName(), getCAMSPassword());
	}
	
	public static CCDataRestServiceClient getCCDataRestServiceClient() {
		return new CCDataRestServiceClient(getCAMSServiceURL(), getCAMSUserName(), getCAMSPassword());
	}
	
	
	
}
