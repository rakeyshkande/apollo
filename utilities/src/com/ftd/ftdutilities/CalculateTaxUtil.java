/**
 * 
 */
package com.ftd.ftdutilities;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.cacheMgr.CacheUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.order.vo.ItemTaxVO;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.RecipientAddressesVO;
import com.ftd.osp.utilities.order.vo.RecipientsVO;
import com.ftd.osp.utilities.order.vo.TaxDisplayOrder;
import com.ftd.osp.utilities.order.vo.TaxRequestVO;
import com.ftd.osp.utilities.order.vo.TaxVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;
import com.ftd.osp.utilities.vo.ProductVO;
import com.ftd.service.util.SequenceUtils;
import com.ftd.tax.service.client.TaxWebServiceClient;
import com.ftd.tax.service.client.dto.Address;
import com.ftd.tax.service.client.dto.Errors.Error;
import com.ftd.tax.service.client.dto.TaxServiceRequest;
import com.ftd.tax.service.client.dto.TaxServiceResponse;
import com.ftd.tax.service.client.dto.Taxes;
import com.ftd.tax.service.client.dto.Taxes.Tax;

/**
 * @author smeka
 *
 */
public class CalculateTaxUtil {
	
	private Logger logger = new Logger(CalculateTaxUtil.class.getName());
	
	
	public static String FLORAL = "FLORAL";
	
	public static final int AMOUNT_SCALE = 2;
	
	public static String TAX_SERVICE_CONTEXT = "TAX_CONFIG";
	
	public static String DEFAULT_TAX_RATE_PARAM = "DEFAULT_TAX_RATE";
	
	private static String DROPSHIP = "DROPSHIP";
	
	private static String FLORAL_DELIVERY = "SD";  
	
	private static String NO_TAX = "Y"; 	

	private static String TAX_REQ_ID_PREFIX_PARAM = "TAX_REQ_ID_PREFIX";
	
	private static String SERVIE_TIMEOUT_PARAM = "SERVICE_TIMEOUT";
	
	private static final String TAX_SERVICE_URL_PARAM = "TAX_SERVICE_URL"; 

	private static final String DATA_SOURCE_NAME = "ORDER SCRUB"; 

	private static final String FTDCA = "FTDCA";

	private static final String NON_FTDCA = "NON_FTDCA";

	private static final String UNDERSCORE = "_";

	private static final String DEFAULT_TAX_LABEL_PARAM = "DEFAULT_TAX_LABEL";
	
	public static final String DEFAULT_TAX_LABEL = "Taxes";
	
	private static final String DEFAULT_CONTENT_TYPE = "application/xml";

	private static final String SEQUENCE_NAME = "TAX.TAX_REQUEST_ID";

	private static final Object EQUALS = "=";

	private static final Object NEWLINE = "\n";

	private static final String GET_TAXES_PATH = "/getTaxes";
	
	private static final String TAX_NOPAGE_SOURCE = "TAX_SERVICE_NO_PAGE";

	public static final String US_RECIPIENT = "US";

	private static final String CA_RECIPIENT = "CA"; 
	
	private static final String SOURCE_CODE_TAX_FLAG = "CALCULATE_TAX_FLAG";
	
	private static String NO = "N"; 
	
	
	
	/** Calculate Tax rates (used to reverse calculate the tax amounts as per the total tax on order, Ex: used in refunds, data massager etc)
	 * @param taxRequestVO
	 * @return
	 */
	public ItemTaxVO calculateTaxRates(TaxRequestVO taxRequestVO) {
		if(logger.isDebugEnabled()) {
			logger.debug("**************calculateTaxRates()****************");
		}
		DataRequest dataRequest = null;
		String errorMessage = null;
		try {
			dataRequest = new DataRequest(); 
			dataRequest.setConnection(DataSourceUtil.getInstance().getConnection(DATA_SOURCE_NAME));
			
			// If international Order, do not call tax service.
			if(taxRequestVO.getRecipientAddress() != null && 
					!(US_RECIPIENT.equalsIgnoreCase(taxRequestVO.getRecipientAddress().getCountry()) 
							|| CA_RECIPIENT.equalsIgnoreCase(taxRequestVO.getRecipientAddress().getCountry()))) {
				logger.info("International order, do not call tax service. Set tax to Zero. " + taxRequestVO.getOrderDetailNumber());				
				return null;
			}
			
			TaxServiceRequest taxReq = createTaxServiceRequest(taxRequestVO, dataRequest);
			TaxServiceResponse taxResp = getItemTax(taxReq);
			return processTaxResponse(taxResp);
			
		} catch (Exception e) {			
			errorMessage = "Unable to calculate tax rates using tax service. " + e.getMessage();  
			logger.error(errorMessage, e); 
			try {
				sendSystemMessage(errorMessage, TAX_NOPAGE_SOURCE, dataRequest.getConnection());
			} catch (Exception e1) {
				logger.error("unable to send an alert: ", e);				
			}		
		} finally {
			try {
				if(dataRequest != null && dataRequest.getConnection() != null && !dataRequest.getConnection().isClosed()) {
					dataRequest.getConnection().close();
				}
			} catch (SQLException e) {
				logger.error("Error caught closing the connection, " + e.getMessage());
			}
		}
		return null;
	}

		
	/** 
	 * @param taxRequestVO
	 * @param dataRequest
	 * @return
	 * @throws Exception
	 */
	private TaxServiceRequest createTaxServiceRequest(TaxRequestVO taxRequestVO, DataRequest dataRequest) throws Exception { 
		String orderNumber = null;
		
		if(!StringUtils.isEmpty(taxRequestVO.getConfirmationNumber())) {
			orderNumber = taxRequestVO.getConfirmationNumber();
		} else if(taxRequestVO.getOrderDetailNumber() ==  0) {
			orderNumber = new Long(taxRequestVO.getOrderDetailNumber()).toString();
		} else {
			orderNumber = "notYetGenerated" + (long)Math.random();
		}
		
		RecipientAddressesVO recipientAddress = taxRequestVO.getRecipientAddress();
		
		if(recipientAddress == null || StringUtils.isEmpty(recipientAddress.getPostalCode())) {
			logger.info("The mandatory object recipient address cannot be null for order: " + orderNumber);				
			throw new Exception("The mandatory object recipient address cannot be null for order: " + orderNumber);
		}
		
		return createTaxServiceRequest(recipientAddress, taxRequestVO.getTaxableAmount(), taxRequestVO.getCompanyId(), taxRequestVO.getShipMethod(), orderNumber, dataRequest);
	}

	/**
	 * 1. Check is product is taxable
	 * 2. create tax request 
	 * 3. calculate tax
	 * 4. apply tax to line item
	 * 
	 * @param lineItem
	 * @param prodVO
	 * @param companyId
	 */
	public void calculateItemTax(OrderDetailsVO lineItem, ProductVO prodVO, String companyId) {
		
		if(logger.isDebugEnabled()) {
			logger.debug("**************calculateItemTax()****************");
		}
		
		String errorMessage = null;		
		DataRequest dataRequest = null;
		Connection connection = null;
		String sourceCodeTaxFlag = null;
		
		if(lineItem.getItemTaxVO() == null) {
			lineItem.setItemTaxVO(new ItemTaxVO());
		}		
		try{
		   sourceCodeTaxFlag = getSourceCodeTaxFlag(lineItem.getSourceCode());
		}catch(Exception exception){
			logger.error("Unable to get the Connection"+exception.getMessage());
		}
		
		
		//Logic for calculating tax for joe,moje,com,scrub,pending and removed screens....
		if(!(NO_TAX.equalsIgnoreCase(sourceCodeTaxFlag) && NO.equalsIgnoreCase(prodVO.getTaxFlag()))) {
			logger.info("The sourcecode or product  is not taxable. SourceCode = " +lineItem.getSourceCode()+"="+sourceCodeTaxFlag+", productId = "+prodVO.getProductId()+"="+prodVO.getTaxFlag());
			lineItem.getItemTaxVO().setTaxAmount(ItemTaxVO.ZERO_VALUE);
			lineItem.getItemTaxVO().setTaxSplit(null);
			lineItem.setTaxServicePerformed(false);
			lineItem.setNoTaxFlag(prodVO.getTaxFlag());
			return;
		}
		
		try {			
			
			dataRequest = new DataRequest(); 
			dataRequest.setConnection(DataSourceUtil.getInstance().getConnection(DATA_SOURCE_NAME));
			
			RecipientAddressesVO recipientAddress = getRecipientAddress(lineItem);
			if(recipientAddress == null || 
					StringUtils.isEmpty(recipientAddress.getStateProvince())) { // When state is null we do not even apply default rate		
				throw new Exception("The mandatory object recipient address cannot be null for order: " + lineItem.getExternalOrderNumber());
			}
			
			// If international Order, do not call tax service.
			if(!(US_RECIPIENT.equalsIgnoreCase(recipientAddress.getCountry()) || CA_RECIPIENT.equalsIgnoreCase(recipientAddress.getCountry()))) {
				logger.info("International order, do not call tax service. Set tax to Zero. " + lineItem.getExternalOrderNumber());
				lineItem.getItemTaxVO().setTaxAmount(ItemTaxVO.ZERO_VALUE);
				lineItem.getItemTaxVO().setTaxSplit(null);
				lineItem.setTaxServicePerformed(false); 
				return;
			}
								
			try {
				
				// Do not call tax service if the taxable amount is zero.
				BigDecimal taxableAmount = getTaxableAmount(lineItem);
				if(taxableAmount == null || taxableAmount.compareTo(BigDecimal.ZERO) <= 0) {
					logger.info("Taxable amount is Zero: " + lineItem.getExternalOrderNumber());
					lineItem.getItemTaxVO().setTaxAmount(ItemTaxVO.ZERO_VALUE);
					lineItem.getItemTaxVO().setTaxSplit(null);
					lineItem.setTaxServicePerformed(false);
					return;
				}
				
				TaxServiceRequest taxReq = createTaxServiceRequest(recipientAddress, taxableAmount, 
						companyId, lineItem.getShipMethod(), lineItem.getExternalOrderNumber(), dataRequest);
				
				TaxServiceResponse taxResp = getItemTax(taxReq);	
				
				applyTax(taxResp, lineItem);
				
			} catch (Exception e) {
				applyDefaultTax(lineItem, companyId, recipientAddress,sourceCodeTaxFlag);
				errorMessage = "Unable to calculate tax using tax service. Defaulted the tax to configured rate. \n" + e.getMessage();
				logger.error(errorMessage, e); 
				sendSystemMessage(errorMessage, TAX_NOPAGE_SOURCE, dataRequest.getConnection());
			}
			
		} catch (Exception e) {
			logger.error("Error caught calculating tax for order: " + lineItem.getExternalOrderNumber());
			logger.error(e);
		} finally {
			try {
				if(dataRequest != null && dataRequest.getConnection() != null && !dataRequest.getConnection().isClosed()) {
					dataRequest.getConnection().close();
				}
			} catch (SQLException e) {
				logger.error("Error caught closing the connection, " + e.getMessage());
			}
		}
	}
	
	private String getSourceCodeTaxFlag(String sourceCode)throws Exception {
		return FTDCommonUtils.getSourceCodeTaxFlag(sourceCode);
	}


	/**
	 * @param taxReq
	 * @return
	 * @throws TaxServiceException, Exception 
	 */
	private TaxServiceResponse getItemTax(TaxServiceRequest taxReq) throws Exception {
		long startedAt = System.currentTimeMillis();
		String taxServiceURI = null; 
		int timeOut = 0;
		
		TaxWebServiceClient taxClient = null; 
		TaxServiceResponse taxResp = null;
		 
		try {
			taxServiceURI = getTaxServcieURI(); 
			timeOut = getTaxServiceTimeOut();
			
			if(logger.isDebugEnabled()) {
				logger.debug("Calling Tax service, taxServiceURI" + taxServiceURI + ", taxServicePath" + GET_TAXES_PATH + ", timeOut" + timeOut);
			}
			
			taxClient = new TaxWebServiceClient(taxServiceURI, GET_TAXES_PATH, DEFAULT_CONTENT_TYPE, timeOut);			
			taxResp = taxClient.calcTaxes(taxReq);	
			boolean throwAlert = true;
			if(taxResp == null || taxResp.getErrors() != null) {
				StringBuffer erroMessage = new StringBuffer("Unable to Calculate Tax:\n");
				for (Error error : taxResp.getErrors().getErrors()) {
					erroMessage.append(error.getCode()).append(EQUALS).append(error.getMessage()).append(NEWLINE);
					
					if(error.getCode() >= 100 && error.getCode() < 200 && error.getCode() != 109) {
						throwAlert = false;
						break;
					}
				}
				logger.error("Error Response: \n" + erroMessage.toString());
				if(throwAlert) {
					throw new Exception(erroMessage.toString());
				}
			}
			
		} catch(Exception e) {
			throw new Exception(e);
		}  
		
		logger.info("Time taken for the Tax service to return response, " + (System.currentTimeMillis() - startedAt));
		return taxResp;
	}


	/**
	 * @param recipientAddress
	 * @param lineItem
	 * @param companyId
	 * @param dataRequest
	 * @return
	 */
	private TaxServiceRequest createTaxServiceRequest(
			RecipientAddressesVO recipientAddress, BigDecimal taxableAmount,
			String companyId, String ShipMethod, String orderNumber, DataRequest dataRequest) {
		TaxServiceRequest request = new TaxServiceRequest();
		
		Address address = new Address();
		address.setCity(recipientAddress.getCity());
		address.setCountry(recipientAddress.getCountry());
		address.setCounty(recipientAddress.getCounty());
		address.setState(recipientAddress.getStateProvince());
		address.setZipCode(recipientAddress.getPostalCode());
		request.setAddress(address);
		
		request.setAmount(taxableAmount);
		request.setCompanyId(companyId);
		request.setDeliveryMethod(FLORAL_DELIVERY.equalsIgnoreCase(ShipMethod) ? FLORAL : DROPSHIP);
		request.setRequestId(generateRequestId(dataRequest, orderNumber));
		
		logger.info("Tax Request for order, " + orderNumber + " is (order number can be null for joe requests), " + request);
		
		return request;
	}

	/**
	 * @param dataRequest
	 * @param externalOrderNumber
	 * @return
	 */
	private String generateRequestId(DataRequest dataRequest, String externalOrderNumber) {
		StringBuffer taxServiceReqid = new StringBuffer();
		try {
			taxServiceReqid.append(ConfigurationUtil.getInstance().getFrpGlobalParm(TAX_SERVICE_CONTEXT, TAX_REQ_ID_PREFIX_PARAM));
			dataRequest.reset();			 
            String sequenceId =  SequenceUtils.getInstance().getNextSequence(dataRequest.getConnection(), SEQUENCE_NAME);
			taxServiceReqid.append(sequenceId); 
		} catch(Exception e){
			logger.error("Unable to generate the requestId for the tax request, setting to external order number, " + externalOrderNumber);
			logger.error(e);
			if(StringUtils.isEmpty(externalOrderNumber)) {
				taxServiceReqid.append((long)Math.random());
			} else {
				taxServiceReqid.append(externalOrderNumber);
			}
		}
		if(logger.isDebugEnabled()) {
			logger.debug("Tax request ID: " + taxServiceReqid.toString() + ", for the order, " + externalOrderNumber);
		}
		return taxServiceReqid.toString();
	}

	/**
	 * @param lineItem
	 * @return
	 */
	public BigDecimal getTaxableAmount(OrderDetailsVO lineItem) {
		BigDecimal taxableAmount = new BigDecimal(0);
		
		if(!StringUtils.isEmpty(lineItem.getProductsAmount())) {
			taxableAmount = taxableAmount.add(new BigDecimal(lineItem.getProductsAmount()));			
		}
		
		if(!StringUtils.isEmpty(lineItem.getAddOnAmount())) {
			taxableAmount = taxableAmount.add(new BigDecimal(lineItem.getAddOnAmount()));			
		}
		
		if(!StringUtils.isEmpty(lineItem.getShippingFeeAmount())) {
			taxableAmount = taxableAmount.add(new BigDecimal(lineItem.getShippingFeeAmount()));			
		}
		
		if(!StringUtils.isEmpty(lineItem.getServiceFeeAmount())) {
			taxableAmount = taxableAmount.add(new BigDecimal(lineItem.getServiceFeeAmount()));			
		}
		
		if(!StringUtils.isEmpty(lineItem.getDiscountAmount())) {
			taxableAmount = taxableAmount.subtract(new BigDecimal(lineItem.getDiscountAmount()));			
		}
		return taxableAmount;
	}

	/**
	 * @param lineItem
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public RecipientAddressesVO getRecipientAddress(OrderDetailsVO lineItem) {		
		List<RecipientsVO> recipientList = lineItem.getRecipients();
		if (recipientList != null && recipientList.size() > 0) {			
			RecipientsVO recipient = (RecipientsVO) recipientList.get(0);			
			if (recipient != null && recipient.getRecipientAddresses() != null
						&& recipient.getRecipientAddresses().size() > 0) {
				return (RecipientAddressesVO) recipient.getRecipientAddresses().get(0);
			}
		}
		return null;
	}
	
	/**
	 * @param taxResp
	 * @param lineItem
	 */
	private void applyTax(TaxServiceResponse taxResp, OrderDetailsVO lineItem) {
		ItemTaxVO itemTaxVO = processTaxResponse(taxResp);		
		lineItem.setItemTaxVO(itemTaxVO);
		lineItem.setTaxServicePerformed(true);
	}
	

	/**
	 * @param taxResp 
	 * @return
	 */
	private ItemTaxVO processTaxResponse(TaxServiceResponse taxResp) {
		ItemTaxVO  itemTaxVO = new ItemTaxVO();
		if(logger.isDebugEnabled()) {
			logger.debug("Applying the tax to the requestId, " + taxResp.getRequestId());
		}		
		logger.info("Tax Response for the requestId, " + taxResp.getRequestId() + " is \n" + taxResp);
		if(taxResp != null) {
			// Set all the tax split
			if(taxResp.getTaxes() != null && taxResp.getTaxes().getTaxes() != null) {
				Taxes taxes = taxResp.getTaxes();
							
				for (Tax tax : taxes.getTaxes()) {	
					TaxVO taxSplitVO = new TaxVO(tax.getDescription(), tax.getDescription(), tax.getRate(), tax.getAmount(), TaxDisplayOrder.fromValue(tax.getDescription()).value());
					itemTaxVO.getTaxSplit().add(taxSplitVO);
				}
			}
			
			// If only total tax is present put it into state tax bucket - This is for flat rates applied for the order from FTD to CA
			if((taxResp.getTaxes() == null || taxResp.getTaxes().getTaxes() == null) && taxResp.getTotalTax() != null) {
				TaxVO taxSplitVO = new TaxVO(taxResp.getTotalTax().getDescription(), taxResp.getTotalTax().getDescription(), taxResp.getTotalTax().getTotalRate(), taxResp.getTotalTax().getTotalAmount(), TaxDisplayOrder.SURCHARGE.value());
				itemTaxVO.getTaxSplit().add(taxSplitVO);
			}
							
			// set the total tax
			if(taxResp.getTotalTax() != null && taxResp.getTotalTax().getTotalAmount() != null) { // Total Tax should never be null
				itemTaxVO.setTaxAmount(taxResp.getTotalTax().getTotalAmount().toString());
				itemTaxVO.setTotalTaxDescription(taxResp.getTotalTax().getDescription());
				itemTaxVO.setTotalTaxrate(taxResp.getTotalTax().getTotalRate());
			}
		}
		return itemTaxVO;
	}


	/** Applies the default tax rate configured and adds it to total and thus tax split is null/ tax buckets are empty.
	 * @param lineItem
	 * @param companyId
	 * @param recipientAddress
	 */
	private void applyDefaultTax(OrderDetailsVO lineItem, String companyId, RecipientAddressesVO recipientAddress,String sourceCodeTaxFlag) {
		if(logger.isDebugEnabled()) {
			logger.debug("Applying the default tax for order, " + lineItem.getExternalOrderNumber());
		}	
		
		ItemTaxVO  itemTaxVO = new ItemTaxVO();
		
		try {
			
			CacheUtil util = CacheUtil.getInstance();
			
			logger.debug("Calculate_tax_flag = "+sourceCodeTaxFlag+" for the sourceCode: " + lineItem.getSourceCode());
			//Calculating Tax based on sourceCode Tax flag.If it is N setting tax amount to 0
			if(null!=sourceCodeTaxFlag && NO.equalsIgnoreCase(sourceCodeTaxFlag)){
				lineItem.getItemTaxVO().setTaxAmount(ItemTaxVO.ZERO_VALUE);
				return;
			}
			if(!util.isStateCompanyTaxON(recipientAddress.getStateProvince(), companyId)) {	
				logger.info("The tax control is set to OFF for the state: " + recipientAddress.getStateProvince() + ", company: " + companyId);	
				lineItem.getItemTaxVO().setTaxAmount(ItemTaxVO.ZERO_VALUE);
				return;
			}
			
			String defaultTaxRate = ConfigurationUtil.getInstance().getFrpGlobalParm(TAX_SERVICE_CONTEXT, DEFAULT_TAX_RATE_PARAM);
			logger.info("Default TaxRate obtained: " + defaultTaxRate);			
			
			if (defaultTaxRate != null && new BigDecimal(defaultTaxRate) != null) {
				BigDecimal taxRatePercent = new BigDecimal(defaultTaxRate).multiply(new BigDecimal(0.01)).setScale(5, BigDecimal.ROUND_HALF_UP);				
				if (logger.isDebugEnabled()) {
					logger.debug("default taxRate: " + taxRatePercent);
				}
			    
				String taxLabel = getDefaultTaxLabel(recipientAddress.getCountry(), companyId);
				
				BigDecimal totalTaxAmount = getTaxableAmount(lineItem).multiply(taxRatePercent).setScale(AMOUNT_SCALE, BigDecimal.ROUND_HALF_UP);				
				itemTaxVO.setTaxAmount(totalTaxAmount.toString());
				itemTaxVO.setTotalTaxDescription(taxLabel);
				itemTaxVO.setTotalTaxrate(taxRatePercent);
			}
			
			lineItem.setItemTaxVO(itemTaxVO);
			lineItem.setTaxServicePerformed(false);
			
		} catch (Exception e) {
			 logger.error("Error caught retrieving default tax configuration, setting tax amount to ZERO.");
			 logger.error(e);
			 itemTaxVO.setTaxAmount(ItemTaxVO.ZERO_VALUE); 
		}
		
	}
	
	/**
	 * @param recipientCountry
	 * @param companyId
	 * @return
	 */
	public String getDefaultTaxLabel(String recipientCountry, String companyId) {
		String taxLabel = null;
		try {			
			String companyCateg = FTDCA.equals(companyId) ? FTDCA: NON_FTDCA;
			StringBuffer paramName = new StringBuffer(companyCateg).append(UNDERSCORE).append(recipientCountry).append(UNDERSCORE).append(DEFAULT_TAX_LABEL_PARAM);
			if(logger.isDebugEnabled()) {
				logger.debug("The param for tax label used is: " + paramName);
			}
			taxLabel =  ConfigurationUtil.getInstance().getFrpGlobalParm(TAX_SERVICE_CONTEXT, paramName.toString());
			
			if(StringUtils.isEmpty(taxLabel)) {
				taxLabel =  ConfigurationUtil.getInstance().getFrpGlobalParm(TAX_SERVICE_CONTEXT, DEFAULT_TAX_LABEL_PARAM);
			}
			
		} catch (Exception e) {
			logger.error("Error caught getting tax label, defaulting to Taxes, ", e);
			taxLabel =  DEFAULT_TAX_LABEL;
		}
		return taxLabel;
	}
	
	/**
	 * This method sends a message to the System Messenger.
	 * @param message 
	 * @return message id
	 */
	private String sendSystemMessage(String message, String messageSource, Connection connection) throws Exception {		
		String messageID = "";

		try {
			logger.error("Sending System Message: " + message); 

			//build system VO
			SystemMessengerVO sysMessage = new SystemMessengerVO();
			sysMessage.setLevel(SystemMessengerVO.LEVEL_PRODUCTION);
			sysMessage.setSource(messageSource);
			sysMessage.setType("ERROR");
			sysMessage.setMessage(message);
			sysMessage.setSubject("TAX CALCULATION ERROR");

			SystemMessenger sysMessenger = SystemMessenger.getInstance(); 
			messageID = sysMessenger.send(sysMessage, connection, false);

			if (messageID == null) {
				String msg = "Error occured while attempting to send out a system message. Msg not sent: " + message;
				logger.error(msg);
			}
			
		} catch(Exception e) {
			logger.error("Error caught sending an alert, ", e);			
		}

		return messageID;
	}
	
	/**
	 * @return
	 */
	private int getTaxServiceTimeOut() {
		try {
			String timeOut = ConfigurationUtil.getInstance().getFrpGlobalParm(TAX_SERVICE_CONTEXT, SERVIE_TIMEOUT_PARAM);
			return Integer.parseInt(timeOut);
		} catch (Exception e) {
			logger.error("Error caught getting tax service time out param, default to 10 seconds");
			return 10000;
		}
	}

	/**
	 * @return
	 */
	private String getTaxServcieURI() {
		try {
			return ConfigurationUtil.getInstance().getFrpGlobalParm(TAX_SERVICE_CONTEXT, TAX_SERVICE_URL_PARAM);
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage());
		}
	}
	
	/**
	 * 
	 * @param taxableAmt
	 * @param totalTax
	 * @return taxRate - returns the tax rate
	 */
	public BigDecimal reverseCalcTaxRate(BigDecimal taxableAmt, BigDecimal totalTax) {
		BigDecimal taxRate = new BigDecimal(0);
		if (taxableAmt != null && totalTax != null && taxableAmt.compareTo(BigDecimal.ZERO) > 0) {
			// Doing a direct division on BigDecimal variable is giving unexpected value
			// So converting the values to float and doing division operation
			Float taxableAmtF = new Float(String.valueOf(taxableAmt));
			Float totalTaxF = new Float(String.valueOf(totalTax));
			totalTaxF = totalTaxF/taxableAmtF;
			taxRate = new BigDecimal(String.valueOf(Math.floor(totalTaxF*100000)/100000));
		} 
		return taxRate;
	}
	
	/**
	 * @param lineItem
	 * @param tax
	 * @return
	 */
	public BigDecimal reverseCalcTaxRate(OrderDetailsVO lineItem, BigDecimal tax) {
		return reverseCalcTaxRate(getTaxableAmount(lineItem), tax);
	}	
}
