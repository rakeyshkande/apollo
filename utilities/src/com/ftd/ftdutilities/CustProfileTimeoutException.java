package com.ftd.ftdutilities;

public class CustProfileTimeoutException extends RuntimeException
{
    private static final long serialVersionUID = 1L;

    public CustProfileTimeoutException() 
    {
        super();
    }

   /**
   * Constructs a new CustProfileTimeoutException with a message string
   * @param message
   */
    public CustProfileTimeoutException(String message)
    {
        super(message);
    }
    
  /**
   * Constructs a CustProfileTimeoutException with a message string, and a base exception
   * 
   * @param message
   * @param ex
   */
    public CustProfileTimeoutException(String message, Exception ex)
    {
      super(message, ex);
    }
    
}