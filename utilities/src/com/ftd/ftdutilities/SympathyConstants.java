/**
 * 
 */
package com.ftd.ftdutilities;

/**
 * @author gboddu
 *
 */
public class SympathyConstants {

	public static final String BUS_OPERATING_MON_FRI_START_HOURS = "BoHrsMonFriStart";
	public static final String BUS_OPERATING_MON_FRI_END_HOURS = "BoHrsMonFriEnd";
	public static final String BUS_OPERATING_SAT_START_HOURS = "BoHrsSatStart";
	public static final String BUS_OPERATING_SAT_END_HOURS = "BoHrsSatEnd";
	public static final String BUS_OPERATING_SUN_START_HOURS = "BoHrsSunStart";
	public static final String BUS_OPERATING_SUN_END_HOURS = "BoHrsSunEnd";
    public static final String SYMPATHY_LOCATION_FUNERAL = "FUNERAL";
    public static final String SYMPATHY_LOCATION_CEMETERY = "CEMETERY";
    public static final String SYMPATHY_LOCATION_HOSPITAL = "HOSPITAL";
    public static final String SYMPATHY_LOCATION_HOSPITAL_SCRUB = "H";
    public static final String SYMPATHY_LOCATION_CEMETERY_SCRUB = "C";
    public static final String SYMPATHY_LOCATION_FUNERAL_SCRUB = "F";
    public static final String SYMPATHY_CONTROLS_CONFIG = "SYMPATHY_CONTROLS_CONFIG";
	
}
