/**
 * 
 */
package com.ftd.util.adapters;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 * @author smeka
 * 
 */
public class CalendarTypeAdapter extends XmlAdapter<String, Calendar> {

	private SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

	@Override
	public String marshal(Calendar v) throws Exception {
		return dateFormat.format(v.getTime());
	}

	@Override
	public Calendar unmarshal(String v) throws Exception {
		Date date = dateFormat.parse(v);
		Calendar calendar = Calendar.getInstance();
		calendar.clear();
		calendar.setTime(date);
		return calendar;
	}

}
