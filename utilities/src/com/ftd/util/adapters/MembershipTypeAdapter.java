package com.ftd.util.adapters;

import java.sql.Connection;
import java.sql.SQLException;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;

public class MembershipTypeAdapter extends XmlAdapter<String, String> {
	
	private static Logger logger  = new Logger("com.ftd.util.adapters.StringTypeAdapter");
    public final static String CUST_MEMBERSHIP_CTXT = "CUST_MEMBERSHIP";
    public final static String CUST_MEMBERSHIP_CONTENT_NAME = "MEMBERSHIP_TYPE";

	@Override
	public String marshal(String shortForm) throws Exception {
		
		Connection conn = null;
		ConfigurationUtil cu = null;
		String membershipTypeDesc = shortForm;
		try {
			conn = DataSourceUtil.getInstance().getConnection("ORDER SCRUB");
			cu = ConfigurationUtil.getInstance();
			
			// Short form sent by CAMS should be the filter id in Apollo
			membershipTypeDesc = cu.getContentWithFilter(conn, CUST_MEMBERSHIP_CTXT,
					CUST_MEMBERSHIP_CONTENT_NAME, shortForm, null);
			
		} catch (Exception e) {
			logger.error("Error getting the membership type description " + e.getMessage());
			return shortForm;
		} finally {
			if (conn != null && !conn.isClosed()) {
				try {
					conn.close();
				} catch (SQLException e) {
					logger.error("unable to close the connection " + e.getMessage());
				}
			}      		
		}
		return membershipTypeDesc;
	}

	@Override
	public String unmarshal(String shortForm) throws Exception {		
		return shortForm;
	}
}