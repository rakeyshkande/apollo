package com.ftd.util.adapters;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class DateTypeAdapter extends XmlAdapter<String, Date> {

	private SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");

	@Override
	public String marshal(Date date) throws Exception {		
		return dateFormat.format(date);
	}

	@Override
	public Date unmarshal(String v) throws Exception {
		Date date = new Date(Long.parseLong(v) *1000);
		return date;
	}
}