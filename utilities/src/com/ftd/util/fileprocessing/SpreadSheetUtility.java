package com.ftd.util.fileprocessing;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.xml.sax.SAXException;

import com.ftd.osp.utilities.plugins.Logger;

public class SpreadSheetUtility {
	private static Logger logger = new Logger("com.ftd.util.fileprocessing.SpreadSheetUtility");	
	private final int EXCEL_START_ROW = 1;
	private static SpreadSheetUtility spreadSheetUtility;
	

	/**
	 * Private constructor.
	 */
	private SpreadSheetUtility() {
	}

	public static SpreadSheetUtility getInstance() {
		if (spreadSheetUtility == null) {
			spreadSheetUtility = new SpreadSheetUtility();
		}
		return spreadSheetUtility;
	}

	public List getLineItemObjects(InputStream spreadsheetStream,
			Class lineItemVOClass) throws Exception {
		logger.debug("Entering getLineItemObjects...");
		List<FileLineItem> lineItems = new ArrayList<FileLineItem>();
		Sheet sheet = null;
		// set row counter
		int rowNumber = EXCEL_START_ROW;
		int cols = 0;

		try {
			// Get reference to worksheet.
			sheet = getWorksheet(spreadsheetStream, 0);
			cols = ((FileLineItem) lineItemVOClass.getConstructor(new Class[] {})
					.newInstance(new Object[] {})).getColumnSize();
			//
			// if(USER.equalsIgnoreCase(subject)) {
			// UserLineItemVO.init();
			// cols = UserLineItemVO.getColumnSize();
			// }

			// get first row in file
			Row row = sheet.getRow(rowNumber);

			// while not at end of file
			while (!eof(row, cols)) {
				logger.debug("Processing row " + rowNumber + "...");
				// get vo representation of line item--parse Exel doc
				lineItems.add(getLine(row, rowNumber, lineItemVOClass));

				// increment line counter
				rowNumber++;

				// get next row
				row = sheet.getRow(rowNumber);
			}// end while
			spreadsheetStream.close();
		} catch (Exception e) {
			logger.error(e);
			throw e;
		}
		return lineItems;
	}

	private Sheet getWorksheet(InputStream spreadsheetStream, int sheetNum)
			throws InvalidFormatException, IOException {

		Sheet sheet = null;
		sheet = WorkbookFactory.create(spreadsheetStream).getSheetAt(sheetNum);

		return sheet;
	}

	private FileLineItem getLine(Row row, int lineNumber, Class lineItemVOClass)
			throws ExcelProcessingException, IOException, SAXException,
			TransformerException, ParserConfigurationException, Exception {

		// Create specific LineItemVO.
		FileLineItem line = (FileLineItem) lineItemVOClass.getConstructor(
				new Class[] {}).newInstance(new Object[] {});

		int max_excel_columns = line.getColumnSize();

		line.setLineNumber(lineNumber);

		for (int i = 0; i < max_excel_columns; i++) {
			line.setFieldValue(i, getCellValue(row, (short) i).trim());
			logger.debug("Setting field " + i + " to value="
					+ getCellValue(row, (short) i));
		}

		return line;
	}

	private String getCellValue(Row row, short column)
			throws ExcelProcessingException, IOException, TransformerException,
			SAXException, ParserConfigurationException, Exception {

		Cell cell = row.getCell(column);

		// if cell is null return empty string
		if (cell == null) {
			return "";
		}

		// The format value returned for a date is 14
		short EXCEL_DATE_FORMAT = 14;
		String value = "";

		switch (cell.getCellType()) {
		// type string
		case (Cell.CELL_TYPE_STRING):
			value = cell.getStringCellValue();
			break;

		// type date
		case (Cell.CELL_TYPE_NUMERIC):
			if (cell.getCellStyle().getDataFormat() == EXCEL_DATE_FORMAT) {
				try {
					value = convertToString(cell.getDateCellValue());
					logger.debug("The date value=" + value);
				} catch (Exception e) {
					String msg = " Row:" + row.getRowNum() + " Column:"
							+ column + 1;
					logger.error(msg);
					logger.error(e);
					throw new Exception(msg);
				}
			} else {
				// Use int value if it is the same as the double value
				// This fixes the problem of loading numeric product ids with a
				// ".0" appended (i.e. 8212.0)
				int cellValueInt = (int) cell.getNumericCellValue();
				double cellValueDouble = cell.getNumericCellValue();
				if (cellValueInt == cellValueDouble) {
					value = String.valueOf(cellValueInt);
				} else {
					value = String.valueOf(cellValueDouble);
				}
			}
			break;
		// blank cell
		case (Cell.CELL_TYPE_BLANK):
			value = "";
			break;
		default:
			String msg = " Row:" + row.getRowNum() + " Column:" + column + 1;
			logger.error(msg);
			logger.error("Cell type=" + cell.getCellType());
			throw new ExcelProcessingException(msg);
		}

		// System.out.println("|" + value + "|");

		return value;
	}

	private String convertToString(Date utilDate) {
		String strDate = "";
		String inDateFormat = "";
		java.util.Date newDate = null;

		if (utilDate != null) {
			SimpleDateFormat dfOut = new SimpleDateFormat("MM/dd/yyyy");

			try {
				newDate = dfOut.parse(dfOut.format(utilDate));
				strDate = dfOut.format(newDate);
			} catch (ParseException e) {
				logger.error("Error parsing the date" + e);
				e.printStackTrace();
			}

		} else {
			strDate = "";
		}

		return strDate;
	}

	private boolean eof(Row row, int cols) throws ExcelProcessingException,
			IOException, SAXException, TransformerException,
			ParserConfigurationException, Exception {

		// check if there is any date in this row
		StringBuffer sb = new StringBuffer("");

		// if row is not null (null row means eof)

		if (row != null) {
			for (short i = 0; i < cols; i++) {
				Cell cell = row.getCell(i);
				if (cell != null) {
					sb.append(getCellValue(row, i));
				}
			}// end for
		}

		return sb.toString().equals("") ? true : false;
	}

	public static void main(String[] args) throws IOException {
		File file = new File("workbook.xlsx");
		try {
			Sheet sheet = WorkbookFactory.create(file).getSheetAt(0);
		} catch (Exception e) {
			e.getMessage();
		}
	}
}
