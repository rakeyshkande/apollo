/**
 * 
 */
package com.ftd.util.fileprocessing;

public class Field {
	 private String name;
	    private String value;
	    private String type;
	    private int position;
	    private int maxSize;
	    private boolean isRequired;

	    public static final String TYPE_STRING = "string";
	    public static final String TYPE_DATE = "date";
	    public static final String TYPE_LONG = "long";
	  
	    public Field() 
	    { 
	    }

	    public Field(String inName, String inType, int inPosition, int inMaxSize, boolean inIsRequired) {
	        name = inName;
	        type = inType;
	        position = inPosition;
	        maxSize = inMaxSize;
	        isRequired = inIsRequired;
	    }
	    
	    public String getName() 
	    {
	        return name; 
	    }

	    public void setName(String arg0) 
	    {
	        name = arg0; 
	    } 

	    public String getValue() 
	    {
	        return value; 
	    }

	    public void setValue(String arg0) 
	    {
	        value = arg0; 
	    }   

	    public String getType() 
	    {
	        return type; 
	    }

	    public void setType(String arg0) 
	    {
	        type = arg0; 
	    }   
	        
	    public int getPosition() 
	    {
	        return position; 
	    }

	    public void setPosition(int arg0) 
	    {
	        position = arg0; 
	    }  
	    
	    public int getMaxSize() 
	    {
	        return maxSize; 
	    }

	    public void setColumnSize(int arg0) 
	    {
	        maxSize = arg0; 
	    }  

	    public boolean isRequired() 
	    {
	        return isRequired; 
	    }

	    public void setIsRequired(boolean arg0) 
	    {
	        isRequired = arg0; 
	    }  
}
