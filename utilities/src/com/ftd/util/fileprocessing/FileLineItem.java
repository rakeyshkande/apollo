package com.ftd.util.fileprocessing;

import java.util.ArrayList;

public abstract class FileLineItem {

	 protected static int columnSize; // number of columns per row
	    protected ArrayList fieldList;
	    protected int lineNumber;
	  
	    public FileLineItem() { 
	        if (fieldList == null) {
	          fieldList = new ArrayList();
	        }        
	    }
	    
	    public abstract void validate() throws Exception;

	    public int getLineNumber() {  
	        return lineNumber;
	    }

	    public void setLineNumber(int arg0) {
	        lineNumber = arg0;
	    }
	    
	    public static int getColumnSize() {
	        return columnSize; 
	    }

	    public static void setColumnSize(int arg0) {
	        columnSize = arg0; 
	    } 

	    public ArrayList getFieldList() {
	        return fieldList;
	    }

	    /**
	     * Sets the value to the value attribute of the Field object
	     * which is at the specified position in the list.
	     */
	    public void setFieldValue(int position, String value) {
	        for (int i = 0 ; i < fieldList.size(); i++) {
	          if (i == position) {
	              Field field = (Field)fieldList.get(position);
	              field.setValue(value);
	              break;
	          }
	        }
	    } 

	    /**
	     *  Returns the Field in fieldList that with the specified field name.
	     *  Returns null if not found.
	     */
	    public Field getFieldByName(String fieldName) {        
	        for (int i = 0 ; i < fieldList.size(); i++) {          
	          Field field = (Field)fieldList.get(i);
	          if ((field.getName()).equals(fieldName)) {
	              return field;
	          }
	        }  
	        return null;
	    }

}
