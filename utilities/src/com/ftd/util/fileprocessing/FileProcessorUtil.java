package com.ftd.util.fileprocessing;

import java.io.InputStream;
import java.util.List;

import com.ftd.osp.utilities.plugins.Logger;

public class FileProcessorUtil {
	private static final String LOGGER_CATEGORY = "com.ftd.util.fileprocessing.FileProcessorUtil";
	private static Logger logger;
	private static FileProcessorUtil fileProcessor;

	/**
	 * Private constructor prevents object being instantiated by user.
	 */
	private FileProcessorUtil() {
		logger = new Logger(LOGGER_CATEGORY);
	}

	/**
	 * Returns the static object.
	 * 
	 * @return FileProcessor
	 */
	public static FileProcessorUtil getInstance() {
		if (fileProcessor == null) {
			fileProcessor = new FileProcessorUtil();
		}

		return fileProcessor;
	}

	
	public List processSpreadsheetFile(InputStream uploadedStream,
			Class lineItemVOClass) throws ExcelProcessingException, Exception {
		logger.debug("Entering processSpreadsheetFile...");

		// Get a list of LineItemVO's from InputStream
		List lineItemList = SpreadSheetUtility.getInstance().getLineItemObjects(
				uploadedStream, lineItemVOClass);

		FileLineItem item = null;
		// Validate data. Throw error if any object is invalid.
		for (int i = 0; i < lineItemList.size(); i++) {
			item = (FileLineItem) lineItemList.get(i);
			item.validate();
		}
		return lineItemList;
	}

	
}
