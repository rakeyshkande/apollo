package com.ftd.util.fileprocessing;

public class ExcelProcessingException extends Exception {

	 public ExcelProcessingException() {
		    super();
		  }
		  
		  public ExcelProcessingException(String message){
		    super(message);
		  }

		  public ExcelProcessingException(String message, int lineNumber, String columnName){
		    super(message + ": Line #" + lineNumber + "; Column Name: " + columnName);
		  }  

		  public ExcelProcessingException(String message, int lineNumber){
		    super(message + ": Line #" + lineNumber);
		  }   
}
