package com.ftd.util.pagination;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;

public class PaginationUtil {
	
	private static final String DEF_REC_PER_PAGE_PARAM = "REC_PAGE_SIZE";
	private static final int FIRST_PAGE = 1;
	private static final int DEFAULT_REC_PER_PAGE  = 500;
	private static Logger logger = new Logger("com.ftd.util.pagination.PaginationUtil");
	
	/** Populate the page details.
	 * @param searchVO
	 * @param inputMap
	 * @return 
	 */
	public PaginationVO getPaginationVO(int currentPage, int recordsPerPage, String screenContext) {
		
		if (recordsPerPage == 0 || currentPage == 0) {
			logger.info("No Page detail, setting default page detail");
			recordsPerPage = getDefaultRecordsPerPage(screenContext);
			currentPage = FIRST_PAGE;
		}
		
		int endRecord = (currentPage * recordsPerPage);
		int startRecord = (endRecord - recordsPerPage) + 1;
		
		PaginationVO pageDetail = new PaginationVO(recordsPerPage, currentPage);		
		pageDetail.setStartRecord(startRecord);
		pageDetail.setEndRecord(endRecord);
				
		logger.info("Showing page: " + currentPage + ", noOfRecords per page: " + recordsPerPage);
		
		return pageDetail;

	}

	/**Set default page detail. Get the no of records to be shown from configuration parameters.
	 * @param screenContext
	 * @return
	 */
	private int getDefaultRecordsPerPage(String screenContext) {			
		int recsPerPage = DEFAULT_REC_PER_PAGE;
		try {
			ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
			recsPerPage = Integer.parseInt(configUtil.getFrpGlobalParm(screenContext, DEF_REC_PER_PAGE_PARAM));
		} catch (Exception e) {
			logger.error("Error caught getting default records per page for context, " + screenContext + ", exception: " + e.getMessage());
			logger.error("Setting default records per page count as " + DEFAULT_REC_PER_PAGE);
		}
		return recsPerPage;
	}
}
