/**
 * 
 */
package com.ftd.service.util;

import java.sql.Connection;
import java.util.Map;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * This utility is used to get the next sequence number from oracle sequences 
 * 
 * @author kvasant
 *
 */
final public class SequenceUtils {
	
	private static SequenceUtils seqUtils = new SequenceUtils();
	private static final String KEYGEN_STATEMENT = "KEYGEN";
	private static final String OUT_SEQUENCE_ID = "OUT_SEQUENCE_ID";
	private static final String IN_SEQUENCE_NAME = "IN_SEQUENCE_NAME";
	private Logger logger = new Logger(SequenceUtils.class.getName());
	
	private SequenceUtils() {
		
	}
	
	/**
	 * returns singleton instance of this class
	 */
	public static SequenceUtils getInstance() {
		return seqUtils;
	}
	
	/**
	 * Returns next sequence id for input db sequence. This method used key.keygen procedure
	 * to retrieve the sequence
	 * 
	 * @param connection
	 * @param sequenceName
	 * @return
	 * @throws Exception
	 */
	public String getNextSequence(Connection connection, String sequenceName) throws Exception {
		if(connection == null) {
            throw new Exception("Connection not set");
        }
    
        DataAccessUtil dau = DataAccessUtil.getInstance();
        DataRequest dataRequest = new DataRequest();
        
        try {
            dataRequest.setConnection(connection);
            dataRequest.addInputParam(IN_SEQUENCE_NAME, sequenceName);
            
            dataRequest.setStatementID(KEYGEN_STATEMENT);
            String sequenceId = (String) dau.execute(dataRequest);
            dataRequest.reset();
            return sequenceId;
            
        } catch(Exception ex) {
        	logger.error("Error occurred while retrieving next sequence for sequence: " + sequenceName , ex);
            throw ex;
        }
	}
	

}
