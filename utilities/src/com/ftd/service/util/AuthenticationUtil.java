package com.ftd.service.util;

import org.apache.commons.lang.StringUtils;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * @author smeka
 *
 */
public class AuthenticationUtil {
	
	protected static Logger logger = new Logger("com.ftd.service.util.AuthenticationUtil");
	
	private static String SERVICE_CONTEXT = "SERVICE";
	
	/** Util method to authenticate client by user name and password.
	 * @param name
	 * @param actualPassword
	 * @return
	 */
	public static boolean authenticateClient(Client client) throws Exception {
		
		Credentials credentials = client.getCredentials();		
		if(credentials == null || StringUtils.isEmpty(credentials.getUserName()) 
				|| StringUtils.isEmpty(credentials.getPassword())) {
			return false;
		}
		
		ConfigurationUtil configUtil = null;		
		try {
			configUtil = ConfigurationUtil.getInstance();
			String expectedPassword = configUtil.getSecureProperty(SERVICE_CONTEXT, credentials.getUserName());
			if(StringUtils.isEmpty(expectedPassword) || !expectedPassword.equals(credentials.getPassword())) {
				return false;
			}
		} catch(Exception e){
			throw new Exception("Apollo Error - Unable to authenticate client");
		}		
		return true;
	}
}
