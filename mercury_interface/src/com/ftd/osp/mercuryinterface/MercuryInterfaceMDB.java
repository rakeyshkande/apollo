package com.ftd.osp.mercuryinterface;


import com.ftd.osp.mercuryinterface.dao.MercuryDAO;
import com.ftd.osp.mercuryinterface.util.CommonUtilites;
import com.ftd.osp.mercuryinterface.vo.MercuryOpsVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.MessageToken;

import java.sql.Connection;

import java.util.List;

import javax.ejb.MessageDrivenBean;
import javax.ejb.MessageDrivenContext;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import javax.naming.InitialContext;

/**
 * This a message driven bean.
 * 
 * When the container is start the setMessageDrivenContext is executed.  This 
 * method sends out JMS messages which this bean consumes.  After receing those
 * JMS messages this object will call the MercuryProcessor object..which starts
 * the batch processing of mercury messages.
 * 
 */
public class MercuryInterfaceMDB implements MessageDrivenBean, MessageListener 
{
  private MessageDrivenContext context;

  private static String LOGGER_CATEGORY = "com.ftd.osp.mercuryinterface.MercuryInterfaceMDB";

  //flag to indicate if we have sent out the initial JMS messages
  private static boolean initialMessagesSent;
  
  //Flag to indicate if we are in the process of starting the bean.
  //This is used to make sure we do not process any messages while we
  //are starting up.  This could happen if messages were left on the queue
  //when this process ended.
  //private static boolean inProcessOfStarting = false;

  /**
   * ejbCreate
   */
  public void ejbCreate()
  {
  }

  /**
   * onMesage...a JMS message was received
   * @param msg
   */
  public void onMessage(Message msg)
  {
  
    Logger logger = new Logger(LOGGER_CATEGORY);
    Connection conn = null;
    String suffix = "";
    
    //if we are in the process of stating then exit
    /*if(inProcessOfStarting)
    {
      logger.debug("We are in the process of starting the EJB.  This JMS message will be ignored.");
      return;
    }
    */
   
    try 
    {
    
      InitialContext initialContext = new InitialContext();

      //get DB connection
      conn = CommonUtilites.getConnection();  
    
      //get message and extract suffix
      TextMessage textMessage = (TextMessage)msg;
      suffix = textMessage.getText();
      
      logger.debug("Message received:" + suffix);      
      
      //call business object
      new MercuryProcessor().doMercuryProcessing(conn,suffix);     

    } catch (Throwable ex) 
    {
        //log error
        logger.error(ex);
    
        //send out sytem message
        String message = "Error processing suffix("+suffix+")  :" + ex.toString();

        try{
          CommonUtilites.sendSystemMessage(message);
        }
        catch(Exception e)
        {
          logger.error("Could not send system message:" + e.toString());
        }
    
    } finally 
    {
      try{
        conn.close();
      }
      catch(Exception e)
      {
        //do nothing
      }
    }
    
  }

  public void ejbRemove()
  {

  }

  /**
   * This message is invoked when the container is started
   * @param ctx
   */
  public void setMessageDrivenContext(MessageDrivenContext ctx)
  {
    this.context = ctx;
    
    //if the initial messages have not been then send them out..kick off the process
    if(!initialMessagesSent){
            initialMessagesSent = true;
            
            Logger logger = new Logger(LOGGER_CATEGORY);
            
            
            //inProcessOfStarting =  true;
          
            logger.debug("Starting Mercury Process");
            System.out.println("Starting Mercury Process");
          
            Connection conn = null;
          
            try{
         
                InitialContext initialContext = new InitialContext();
         
                //get DB connection
                conn = CommonUtilites.getConnection();
          
                
                //create dao
                MercuryDAO dao = new MercuryDAO(conn);
                
                //get list of available suffixs
                List suffixList = dao.getMercuryOpsByAvailable(true);
        
                //remove any existing messages
                dao.clearQueue();
        
                //turn off stating flag so that messages will begin to process
                //inProcessOfStarting = false;
        
                //send out a message to start each suffix that is available
                for(int i = 0; i < suffixList.size() ; i++){
                      MercuryOpsVO opsVO = (MercuryOpsVO)suffixList.get(i);
                
                      //Send JMS message
                      MessageToken token = new MessageToken();
                      token.setMessage(opsVO.getSuffix());
                      CommonUtilites.sendJMSMessage(initialContext,token);        
                }
                
            }
            catch(Throwable t)
            {
               String message = "Error starting mercury interface.";
               logger.error(message);
               logger.error(t);
               try{
                 CommonUtilites.sendSystemMessage(message + " : " + t.toString());
               }
                catch(Exception e)
                 {
                  logger.error(e);   
                 }
            }
            finally
            {
              try{
              conn.close();
              }
              catch(Exception e)
              {
                //do nothing
              }//end try catch
            }//end finally      
  }//end if messages sent
  }//end method
  
 
  
}//end class