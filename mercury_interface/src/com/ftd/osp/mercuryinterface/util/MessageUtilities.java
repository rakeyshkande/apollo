package com.ftd.osp.mercuryinterface.util;


import java.util.*;
import com.ftd.osp.mercuryinterface.vo.*;
import java.text.SimpleDateFormat;
import com.ftd.osp.mercuryinterface.constants.*;
import com.ftd.ftdutilities.FieldUtils;
import java.math.BigDecimal;
import com.ftd.osp.mercuryinterface.util.CommonUtilites;

/**
 * This object contains utility methods that are performed on mercury messages.
 */
public class MessageUtilities 
{

  private static final String ORDER_DATE_FORMAT = "MMM dd";
  private static final char LINE_FEED = (char)Integer.parseInt("0A",16);

  public MessageUtilities()
  {
  }
  
  /**
   * This method builds mercury message tag
   * 
   * @param msgTag The Tag name
   * @param msgText The tag text
   * @return 
   */
  static public String buildTag(String msgTag, String msgText) {
      if (msgText == null || msgText.equals("")) msgText = "None";
      String outTag = "[" + msgTag + ";" + msgText.length() + ";TEXT]" + msgText;
	return outTag;

  } // buildTag  
  
  
  /**
   * Build header reocord for transmission
   * @param int  Length of transmission
   * @return String header
   */
  static public String buildHeaderAscii( int inLength ) {
      
      //conert length to string
      String msgLength = String.valueOf(inLength);
      
      //left pad length string
      msgLength = "0000".substring(msgLength.length()) + msgLength;
      
      //build header string
      String headString = "BOXX 1.A" + msgLength;
      
      // the CRC field is not used, fill with 4 nulls
      //headString = headString + (char)0 + (char)0 + (char)0 + (char)0;
      
      //Box X and efos does not care so, put in four spaces to make for easier log debugging
      headString = headString + (char)32 + (char)32 + (char)32 + (char)32;
    
      return headString;
  } 
  

  /**
   * This method parses the data out of a mercury tag
   * 
   * @param tag
   * @param delim
   * @return 
   */
   static public TagVO parseTag ( String tag, String delim )
  {
    int countPosStart = 0;
    int countPosEnd = 0;
    
    TagVO tagVO = new TagVO();    

    countPosStart = tag.indexOf(delim);
    countPosEnd = tag.indexOf(delim,countPosStart + 1);
    tagVO.setTagText(tag.substring(1, countPosStart));
    tagVO.setTagLength(Integer.parseInt(tag.substring(countPosStart+1, countPosEnd)));

    return tagVO;

  } // parseTag
  
  /**
   * This method builds a BatchMessage for the given mercury message
   * @param MercuryMessageVO Mercury message
   * 
   */
  static public BatchMessageVO buildMessage(MercuryMessageVO mercuryVO) throws Exception
  {
    //get the message type
    String msgType = mercuryVO.getMessageType();
    if(msgType == null)
    {
      throw new Exception("Message Type is null");
    }
    
    BatchMessageVO batchMessage = null;
    
    //remove null values from MercuryVO
    mercuryVO = changeNullValues(mercuryVO);
    
    //determine batch type
    if(msgType.equals(MercuryConstants.ANS_MESSAGE))
    {
      batchMessage = getANSMessage(mercuryVO);      
    }
    else if(msgType.equals(MercuryConstants.ASK_MESSAGE))
    {
      batchMessage = getASKMessage(mercuryVO);      
    }
    else if(msgType.equals(MercuryConstants.CAN_MESSAGE))
    {
      batchMessage = getCANMessage(mercuryVO);      
    }
    else if(msgType.equals(MercuryConstants.FTD_MESSAGE))
    {
      batchMessage = getFTDMessage(mercuryVO);      
    }
    else if(msgType.equals(MercuryConstants.GEN_MESSAGE))
    {
      batchMessage = getGENMessage(mercuryVO);      
    }
    else if(msgType.equals(MercuryConstants.RET_MESSAGE))
    {
      batchMessage = getRETMessage(mercuryVO);      
    }    
    else if(msgType.equals(MercuryConstants.REJ_MESSAGE))
    {
      batchMessage = getREJMessage(mercuryVO);      
    }    
    else if(msgType.equals(MercuryConstants.ADJ_MESSAGE))
    {
      batchMessage = getADJMessage(mercuryVO);      
    }            
    else
    {
      throw new Exception("Unknown Message Type:" + msgType);
    }
    
    return batchMessage;
    
  }
  
    /*
     * Build message
     */
    static private BatchMessageVO getADJMessage(MercuryMessageVO mercuryVO)
  {
      //create message object and temporary variables
      BatchMessageVO batchMessage = new BatchMessageVO();
      StringBuffer message = new StringBuffer("");
      long messageLength = 0;

      //add header
      message.append(buildTag("MESSAGE TYPE", "ADJUSTMENT MESSAGE"));
      messageLength = messageLength + 18;//add length of 'ADJUSTMENT MESSAGE'

      //combined
      message.append(buildTag("COMBINED REPORT NUMBER", mercuryVO.getCombinedReportNumber()));
      messageLength = messageLength + mercuryVO.getCombinedReportNumber().length();
     
      //add recipient name
      message.append(buildTag("RECIPIENT NAME", mercuryVO.getRecipient()));
      messageLength = messageLength + mercuryVO.getRecipient().length();

      //rof number
      message.append(buildTag("ROF NUMBER", mercuryVO.getRofNumber()));
      messageLength = messageLength + mercuryVO.getRofNumber().length();
      
      //add delivery date
      message.append(buildTag("DELIVERY DATE", mercuryVO.getDeliveryDateText()));
      messageLength = messageLength + mercuryVO.getDeliveryDateText().length();
      
      //add filling florist
      message.append(buildTag("FILLING FLORIST CODE", mercuryVO.getFillingFlorist()));
      messageLength = messageLength + mercuryVO.getFillingFlorist().length();
      
      //add price
      String priceString = FieldUtils.formatBigDecimalAsCurrency(getBigDec(mercuryVO.getPrice()),2,false);      
      //EFOS doesn't like commas
      priceString = priceString.replace(",", "");
      message.append(buildTag("PRICE",priceString));
      messageLength = messageLength + priceString.length();
      
      //reason code
      message.append(buildTag("REASON CODE", mercuryVO.getADJReasonCode()));
      messageLength = messageLength + mercuryVO.getADJReasonCode().length();
      
      //adjustment reason text
      message.append(buildTag("ADJUSTMENT REASON TEXT", mercuryVO.getComments()));
      messageLength = messageLength + mercuryVO.getComments().length();
      
      //over under charge
      String overUnderString = FieldUtils.formatBigDecimalAsCurrency(new BigDecimal(mercuryVO.getOverUnderCharge()),2,false);   
      //EFOS doesn't like commas
      overUnderString = overUnderString.replace(",", "");
      message.append(buildTag("OVER UNDER CHARGE", overUnderString));
      messageLength = messageLength + overUnderString.length();
     
      //add operator
      message.append(buildTag("OPERATOR", mercuryVO.getOperator()));
      messageLength = messageLength + mercuryVO.getOperator().length();

      // Box X adds 17 spaces for every line feed so we need to include that in our total
      String batchString = message.toString();
      messageLength = messageLength + getCarriageReturnCount(batchString);

      //populate VO
      batchMessage.setMessage(message.toString());
      batchMessage.setLengthWithoutTags(messageLength);
     
     return batchMessage;
     
  }
  
  static private BigDecimal getBigDec(Double dbl)
  {
      BigDecimal ret = null;
      
      if(dbl != null)
      {
          ret = new BigDecimal(dbl.toString());
      }
      
      return ret;
  }
  
    /*
     * Build message
     */  
  static private BatchMessageVO getANSMessage(MercuryMessageVO mercuryVO)
  {
      //create message object and temporary variables
      BatchMessageVO batchMessage = new BatchMessageVO();
      StringBuffer message = new StringBuffer("");
      long messageLength = 0;

      //add header
      message.append(buildTag("MESSAGE TYPE", "ANSWER MESSAGE"));
      messageLength = messageLength + 14;//add length of 'ANSWER_MESSAGE'

      //add order number
      message.append(buildTag("ORDER NUMBER", mercuryVO.getMercuryOrderNumber().toUpperCase()));
      messageLength = messageLength + mercuryVO.getMercuryOrderNumber().length();
      
      //add order date
      SimpleDateFormat sdf = new SimpleDateFormat(ORDER_DATE_FORMAT);      
      message.append(buildTag("ORDER DATE", sdf.format(mercuryVO.getOrderDate())));
      messageLength = messageLength + ORDER_DATE_FORMAT.length();
      
      //add recipient name
      message.append(buildTag("RECIPIENT NAME", mercuryVO.getRecipient()));
      messageLength = messageLength + mercuryVO.getRecipient().length();
      
      //add address
      message.append(buildTag("RECIPIENT STREET ADDRESS", mercuryVO.getAddress()));
      messageLength = messageLength + mercuryVO.getAddress().length();
      
      //add city,state,zip
      message.append(buildTag("RECIPIENT CITY STATE ZIP", mercuryVO.getCityStateZip()));
      messageLength = messageLength + mercuryVO.getCityStateZip().length();
      
      //add delivery date
      message.append(buildTag("DELIVERY DATE", mercuryVO.getDeliveryDateText()));
      messageLength = messageLength + mercuryVO.getDeliveryDateText().length();

      //only include ask answer code and price if Ask Answer Code contains a value
      if(mercuryVO.getAskAnswerCode() != null && mercuryVO.getAskAnswerCode().length() > 0)
      {      
            //add ask answer code
            message.append(buildTag("ASK ANSWER CODE", mercuryVO.getAskAnswerCode()));
            messageLength = messageLength + mercuryVO.getAskAnswerCode().length();
            
            //add price
            String priceString = FieldUtils.formatBigDecimalAsCurrency(getBigDec(mercuryVO.getPrice()),2,false);      
            //EFOS doesn't like commas
            priceString = priceString.replace(",", "");
            message.append(buildTag("PRICE",priceString));
            messageLength = messageLength + priceString.length();
      }
      
      //add comments
      message.append(buildTag("REASON TEXT", mercuryVO.getComments()));
      messageLength = messageLength + mercuryVO.getComments().length();
      
      //add operator
      message.append(buildTag("OPERATOR", mercuryVO.getOperator()));
      messageLength = messageLength + mercuryVO.getOperator().length();

      // Box X adds 17 spaces for every line feed so we need to include that in our total
      String batchString = message.toString();
      messageLength = messageLength + getCarriageReturnCount(batchString);

      //populate VO
      batchMessage.setMessage(message.toString());
      batchMessage.setLengthWithoutTags(messageLength);
     
     return batchMessage;
     
  }

    /*
     * Build message
     */  
  static private BatchMessageVO getREJMessage(MercuryMessageVO mercuryVO)
  {
      //create message object and temporary variables
      BatchMessageVO batchMessage = new BatchMessageVO();
      StringBuffer message = new StringBuffer("");
      long messageLength = 0;

      //add header
      message.append(buildTag("MESSAGE TYPE", "REJECT ORDER"));
      messageLength = messageLength + 12;//add length of 'REJECT ORDER'

      //add order number
      message.append(buildTag("ORDER NUMBER", mercuryVO.getMercuryOrderNumber().toUpperCase()));
      messageLength = messageLength + mercuryVO.getMercuryOrderNumber().length();
      
      //add order date
      SimpleDateFormat sdf = new SimpleDateFormat(ORDER_DATE_FORMAT);      
      message.append(buildTag("ORDER DATE", sdf.format(mercuryVO.getOrderDate())));
      messageLength = messageLength + ORDER_DATE_FORMAT.length();
      
      //add recipient name
      message.append(buildTag("RECIPIENT NAME", mercuryVO.getRecipient()));
      messageLength = messageLength + mercuryVO.getRecipient().length();
      
      //add address
      message.append(buildTag("RECIPIENT STREET ADDRESS", mercuryVO.getAddress()));
      messageLength = messageLength + mercuryVO.getAddress().length();
      
      //add city,state,zip
      message.append(buildTag("RECIPIENT CITY STATE ZIP", mercuryVO.getCityStateZip()));
      messageLength = messageLength + mercuryVO.getCityStateZip().length();

      //add raw delivery date
      message.append(buildTag("RAW DELIVERY DATE", mercuryVO.getDeliveryDateText()));
      messageLength = messageLength + mercuryVO.getDeliveryDateText().length();
      
      //add delivery date
      message.append(buildTag("DELIVERY DATE", mercuryVO.getDeliveryDateText()));
      messageLength = messageLength + mercuryVO.getDeliveryDateText().length();

      //add comments
      message.append(buildTag("REASON TEXT", mercuryVO.getComments()));
      messageLength = messageLength + mercuryVO.getComments().length();
      
      //add operator
      message.append(buildTag("OPERATOR", mercuryVO.getOperator()));
      messageLength = messageLength + mercuryVO.getOperator().length();

      // Box X adds 17 spaces for every line feed so we need to include that in our total
      String batchString = message.toString();
      messageLength = messageLength + getCarriageReturnCount(batchString);

      //populate VO
      batchMessage.setMessage(message.toString());
      batchMessage.setLengthWithoutTags(messageLength);
     
     return batchMessage;
     
  }  
 
     /*
     * Build message
     */
   static private BatchMessageVO getASKMessage(MercuryMessageVO mercuryVO)
  {
      //create message object and temporary variables
      BatchMessageVO batchMessage = new BatchMessageVO();
      StringBuffer message = new StringBuffer("");
      long messageLength = 0;

      //add header
      message.append(buildTag("MESSAGE TYPE", "ASK MESSAGE"));
      messageLength = messageLength + 11;//add length of 'ASK_MESSAGE'

      //add order number
      message.append(buildTag("ORDER NUMBER", mercuryVO.getMercuryOrderNumber().toUpperCase()));
      messageLength = messageLength + mercuryVO.getMercuryOrderNumber().length();
      
      //add order date
      SimpleDateFormat sdf = new SimpleDateFormat(ORDER_DATE_FORMAT);
      message.append(buildTag("ORDER DATE", sdf.format(mercuryVO.getOrderDate())));
      messageLength = messageLength + ORDER_DATE_FORMAT.length();
      
      //add recipient name
      message.append(buildTag("RECIPIENT NAME", mercuryVO.getRecipient()));
      messageLength = messageLength + mercuryVO.getRecipient().length();
      
      //add address
      message.append(buildTag("RECIPIENT STREET ADDRESS", mercuryVO.getAddress()));
      messageLength = messageLength + mercuryVO.getAddress().length();
      
      //add city,state,zip
      message.append(buildTag("RECIPIENT CITY STATE ZIP", mercuryVO.getCityStateZip()));
      messageLength = messageLength + mercuryVO.getCityStateZip().length();
      
      //add delivery date
      message.append(buildTag("DELIVERY DATE", mercuryVO.getDeliveryDateText()));
      messageLength = messageLength + mercuryVO.getDeliveryDateText().length();
      
      //only include ask answer code and price if Ask Answer Code contains a value
      if(mercuryVO.getAskAnswerCode() != null && mercuryVO.getAskAnswerCode().length() > 0)
      {      
      
          //add ask answer code
          message.append(buildTag("ASK ANSWER CODE", mercuryVO.getAskAnswerCode()));
          messageLength = messageLength + mercuryVO.getAskAnswerCode().length();
          
          //add price
          String priceString = FieldUtils.formatBigDecimalAsCurrency(getBigDec(mercuryVO.getPrice()),2,false);      
          //EFOS doesn't like commas
          priceString = priceString.replace(",", "");
          message.append(buildTag("PRICE", priceString));
          messageLength = messageLength + priceString.length();

      }
      
      //add comments
      message.append(buildTag("REASON TEXT", mercuryVO.getComments()));
      messageLength = messageLength + mercuryVO.getComments().length();
      
      //add operator
      message.append(buildTag("OPERATOR", mercuryVO.getOperator()));
      messageLength = messageLength + mercuryVO.getOperator().length();

      //populate VO
      batchMessage.setMessage(message.toString());
      batchMessage.setLengthWithoutTags(messageLength);
     
     return batchMessage;
     
  }
 
    /*
     * Build message
     */              
    static private BatchMessageVO getCANMessage(MercuryMessageVO mercuryVO)
  {
      //create message object and temporary variables
      BatchMessageVO batchMessage = new BatchMessageVO();
      StringBuffer message = new StringBuffer("");
      long messageLength = 0;

      //add header
      message.append(buildTag("MESSAGE TYPE", "CANCEL ORDER"));
      messageLength = messageLength + 12;//add length of 'CANCEL ORDER'

      //add order number
      message.append(buildTag("ORDER NUMBER", mercuryVO.getMercuryOrderNumber().toUpperCase()));
      messageLength = messageLength + mercuryVO.getMercuryOrderNumber().length();
      
      //add order date
      SimpleDateFormat sdf = new SimpleDateFormat(ORDER_DATE_FORMAT);
      message.append(buildTag("ORDER DATE", sdf.format(mercuryVO.getOrderDate())));
      messageLength = messageLength + ORDER_DATE_FORMAT.length();
      
      //add recipient name
      message.append(buildTag("RECIPIENT NAME", mercuryVO.getRecipient()));
      messageLength = messageLength + mercuryVO.getRecipient().length();
      
      //add address
      message.append(buildTag("RECIPIENT STREET ADDRESS", mercuryVO.getAddress()));
      messageLength = messageLength + mercuryVO.getAddress().length();
      
      //add city,state,zip
      message.append(buildTag("RECIPIENT CITY STATE ZIP", mercuryVO.getCityStateZip()));
      messageLength = messageLength + mercuryVO.getCityStateZip().length();
      
      //add delivery date
      message.append(buildTag("DELIVERY DATE", mercuryVO.getDeliveryDateText()));
      messageLength = messageLength + mercuryVO.getDeliveryDateText().length();      
      
      //add comments
      message.append(buildTag("REASON TEXT", mercuryVO.getComments()));
      messageLength = messageLength + mercuryVO.getComments().length();    
  
      //add operator
      message.append(buildTag("OPERATOR", mercuryVO.getOperator()));
      messageLength = messageLength + mercuryVO.getOperator().length();
  
      // Box X adds 17 spaces for every line feed so we need to include that in our total
      String batchString = message.toString();
      messageLength = messageLength + getCarriageReturnCount(batchString);
      
      //populate VO
      batchMessage.setMessage(message.toString());
      batchMessage.setLengthWithoutTags(messageLength);
     
     return batchMessage;
     
  }
 
 
     /*
     * Build message
     */
    static private BatchMessageVO getFTDMessage(MercuryMessageVO mercuryVO)
  {
      //create message object and temporary variables
      BatchMessageVO batchMessage = new BatchMessageVO();
      StringBuffer message = new StringBuffer("");
      long messageLength = 0;

      //add header
      message.append(buildTag("MESSAGE TYPE", "FTD ORDER"));
      messageLength = messageLength + 9;//add length of message type

      //add filling florist
      message.append(buildTag("FILLING FLORIST CODE", mercuryVO.getFillingFlorist()));
      messageLength = messageLength + mercuryVO.getFillingFlorist().length();      
      
      //add recipient name
      message.append(buildTag("RECIPIENT NAME", mercuryVO.getRecipient()));
      messageLength = messageLength + mercuryVO.getRecipient().length();
      
      //add address
      message.append(buildTag("RECIPIENT STREET ADDRESS", mercuryVO.getAddress()));
      messageLength = messageLength + mercuryVO.getAddress().length();
      
      //add city,state,zip
      message.append(buildTag("RECIPIENT CITY STATE ZIP", mercuryVO.getCityStateZip()));
      messageLength = messageLength + mercuryVO.getCityStateZip().length();

      //add recipient phone
      message.append(buildTag("RECIPIENT PHONE", mercuryVO.getPhoneNumber()));
      messageLength = messageLength + mercuryVO.getPhoneNumber().length();
      
      //add delivery date
      message.append(buildTag("RAW DELIVERY DATE", mercuryVO.getDeliveryDateText()));
      messageLength = messageLength + mercuryVO.getDeliveryDateText().length();
      
      //first choice
      //emueller 9/29/04, per Mickey product code and description are no longer being parsed out
      message.append(buildTag("FIRST PRODUCT CODE", mercuryVO.getFirstChoice()));  
      messageLength = messageLength + mercuryVO.getFirstChoice().length();

      //add second choice
      message.append(buildTag("SECOND PRODUCT CODE", mercuryVO.getSecondChoice()));
      messageLength = messageLength + mercuryVO.getSecondChoice().length();  
      
      //add price
      String priceString = FieldUtils.formatBigDecimalAsCurrency(getBigDec(mercuryVO.getPrice()),2,false);      
      //EFOS doesn't like commas
      priceString = priceString.replace(",", "");
      message.append(buildTag("PRICE", priceString));
      messageLength = messageLength + priceString.length();
      
      //add card message
      message.append(buildTag("CARD MESSAGE", mercuryVO.getCardMessage()));
      messageLength = messageLength + mercuryVO.getCardMessage().length();
      
      //add occassion
      message.append(buildTag("OCCASION", mercuryVO.getOccasion()));
      messageLength = messageLength + mercuryVO.getOccasion().length();

      //add occassion
      message.append(buildTag("SPECIAL INSTRUCTIONS", mercuryVO.getSpecialInstructions()));
      messageLength = messageLength + mercuryVO.getSpecialInstructions().length();

      //add OPERATOR
      message.append(buildTag("OPERATOR", mercuryVO.getOperator()));
      messageLength = messageLength + mercuryVO.getOperator().length();

      //add RAW MESSAGE PRIORITY
      message.append(buildTag("RAW MESSAGE PRIORITY", mercuryVO.getPriority()));
      messageLength = messageLength + mercuryVO.getPriority().length();
      
      // Add characters if priority contains /N or /X
      if ( (mercuryVO.getPriority().indexOf("/N") != -1) || (mercuryVO.getPriority().indexOf("/X") != -1) )
            {
              messageLength = messageLength + 80;
            }
      
      // Box X adds 17 spaces for every line feed so we need to include that in our total
      String batchString = message.toString();
      messageLength = messageLength + getCarriageReturnCount(batchString);
       
       
       
      //populate VO
      batchMessage.setMessage(batchString);
      batchMessage.setLengthWithoutTags(messageLength);
     
     return batchMessage;
     
  }


    /*
     * Build message
     */ 
    static private BatchMessageVO getGENMessage(MercuryMessageVO mercuryVO)
  {
      //create message object and temporary variables
      BatchMessageVO batchMessage = new BatchMessageVO();
      StringBuffer message = new StringBuffer("");
      long messageLength = 0;

      //add header
      message.append(buildTag("MESSAGE TYPE", "GENERAL MESSAGE"));
      messageLength = messageLength + 15;//add length of GENERAL MESSAGE

      //add Filling Florist
      message.append(buildTag("FILLING FLORIST CODE", mercuryVO.getFillingFlorist()));
      messageLength = messageLength + mercuryVO.getFillingFlorist().length();
      
      //add GENERAL MESSAGE TEXT
      message.append(buildTag("GENERAL MESSAGE TEXT", mercuryVO.getComments()));
      messageLength = messageLength + mercuryVO.getComments().length();
      
      //add OPERATOR
      message.append(buildTag("OPERATOR", mercuryVO.getOperator()));
      messageLength = messageLength + mercuryVO.getOperator().length();
      
      //add MESSAGE PRIORITY
      message.append(buildTag("MESSAGE PRIORITY", mercuryVO.getPriority()));
      messageLength = messageLength + mercuryVO.getPriority().length();
      
      // Box X adds 17 spaces for every line feed so we need to include that in our total
      String batchString = message.toString();
      messageLength = messageLength + getCarriageReturnCount(batchString);      
   
      //populate VO
      batchMessage.setMessage(message.toString());
      batchMessage.setLengthWithoutTags(messageLength);
     
     return batchMessage;
     
  } 
 
     /*
     * Build message
     */
    static private BatchMessageVO getRETMessage(MercuryMessageVO mercuryVO)
  {
      //create message object and temporary variables
      BatchMessageVO batchMessage = new BatchMessageVO();
      StringBuffer message = new StringBuffer("");
      long messageLength = 0;

      //add header
      message.append(buildTag("MESSAGE TYPE", "RETRIEVAL MESSAGE"));
      messageLength = messageLength + 17;//add length of RETRIEVAL MESSAGE

      //add FROM MESSAGE NUMBER
      message.append(buildTag("FROM MESSAGE NUMBER", mercuryVO.getRetrievalFromMessage()));
      messageLength = messageLength + mercuryVO.getRetrievalFromMessage().length();
      
      //add FROM MESSAGE DATE
      message.append(buildTag("FROM MESSAGE DATE", mercuryVO.getRetrievalFromDate()));
      messageLength = messageLength + mercuryVO.getRetrievalFromDate().length();
      
      //add TO MESSAGE NUMBER
      message.append(buildTag("TO MESSAGE NUMBER", mercuryVO.getRetrievalToMessage()));
      messageLength = messageLength + mercuryVO.getRetrievalToMessage().length();
      
      //add TO MESSAGE DATE
      message.append(buildTag("TO MESSAGE DATE", mercuryVO.getRetrievalToDate()));
      messageLength = messageLength + mercuryVO.getRetrievalToDate().length();
      
      //add OPERATOR
      message.append(buildTag("OPERATOR", mercuryVO.getOperator()));
      messageLength = messageLength + mercuryVO.getOperator().length();

      // Box X adds 17 spaces for every line feed so we need to include that in our total
      String batchString = message.toString();
      messageLength = messageLength + getCarriageReturnCount(batchString);
      
      //populate VO
      batchMessage.setMessage(message.toString());
      batchMessage.setLengthWithoutTags(messageLength);
     
     return batchMessage;
     
  }  
 
  /*
   * Change all null values in the VO to empty strings.
   * This reduces the amount of checking needed while building the
   * messages.
   */
  static private MercuryMessageVO changeNullValues(MercuryMessageVO vo)
  {
    if(vo.getAddress() == null) vo.setAddress("");
    if(vo.getAskAnswerCode() == null) vo.setAskAnswerCode("");
    if(vo.getCardMessage() == null) vo.setCardMessage("");
    if(vo.getCityStateZip() == null) vo.setCityStateZip("");
    if(vo.getComments() == null) vo.setComments("");
    if(vo.getDeliveryDateText() == null) vo.setDeliveryDateText("");
    if(vo.getFillingFlorist() == null) vo.setFillingFlorist("");
    if(vo.getFirstChoice() == null) vo.setFirstChoice("");
    if(vo.getMercuryID() == null) vo.setMercuryID("");
    if(vo.getMercuryMessageNumber() == null) vo.setMercuryMessageNumber("");
    if(vo.getMercuryOrderNumber() == null) vo.setMercuryOrderNumber("");
    if(vo.getMercuryStatus() == null) vo.setMercuryStatus("");
    if(vo.getMessageType() == null) vo.setMessageType("");
    if(vo.getOccasion() == null) vo.setOccasion("");
    if(vo.getOperator() == null) vo.setOperator("");
    if(vo.getOutboundID() == null) vo.setOutboundID("");
    if(vo.getPhoneNumber() == null) vo.setPhoneNumber("");
    if(vo.getPriority() == null) vo.setPriority("");
    if(vo.getProductID() == null) vo.setProductID("");
    if(vo.getRecipient() == null) vo.setRecipient("");
    if(vo.getReferenceNumber() == null) vo.setReferenceNumber("");
    if(vo.getRofNumber() == null) vo.setRofNumber("");
    if(vo.getSakText() == null) vo.setSakText("");
    if(vo.getSecondChoice() == null) vo.setSecondChoice("");
    if(vo.getSendingFlorist() == null) vo.setSendingFlorist("");
    if(vo.getSpecialInstructions() == null) vo.setSpecialInstructions("");
    if(vo.getZipCode() == null) vo.setZipCode("");
      
    return vo;
  }
  
  /**
   * Parse out the string that was returned from Box X.
   * 
   * @param message
   * @param outgoingMessages
   * @return 
   * @throws java.lang.Exception
   */
  static public BatchResponseVO parseReturnString ( String message,List outgoingMessages ) 
    throws Exception
  {
    
    int pos = 0; //position of bracket within string
    String tempText = "";
    String tempMsg = "";

    String msgList[];
    String parseResults[] = new String[2];
    int messageCount = 0;
    int responseCount = 0;

    List warnings = new ArrayList();

    TagVO tagVO = new TagVO();

    BatchResponseVO batchResponse = new BatchResponseVO();

    //while there is still more the message to parse
    //(note as parts of the message are parsed they are removed from the string)
    while ( !message.trim().equals("") )
    {
      pos = message.indexOf("]");
      tempText = message.substring(0,pos+1);
      message = message.substring(pos+1);
      tagVO = parseTag(tempText, ";"); 
      
      if (tagVO.getTagText().equals("TOTAL MESSAGES"))
      {
        //We are in the section which contains messages that are being sent to us
        batchResponse = parseDetail(batchResponse,message, "Messages",tagVO.getTagLength(),outgoingMessages);
        message = batchResponse.getRemainingMessage();
      } 
      else if (tagVO.getTagText().equals("TOTAL RESPONSES"))
      {
        //We are in the section that contains responses to messages we sent out
        batchResponse = parseDetail(batchResponse,message, "Responses",tagVO.getTagLength(),outgoingMessages);
        message = batchResponse.getRemainingMessage();        
      }
      else if (tagVO.getTagText().equals("MORE MESSAGES WAITING"))
      {
        //Get More Messaages flag.  We don't do anything with this.
        String moreMessages = message.substring(0,tagVO.getTagLength());        
        message = message.substring(tagVO.getTagLength());
      }
      else if (tagVO.getTagText().equals("ERROR"))
      {
        tempText = message.substring(0,tagVO.getTagLength());
        message = message.substring(tagVO.getTagLength());
        batchResponse.setErrorText(tempText);
        batchResponse.setBatchError(true);
      }
      else if (tagVO.getTagText().equals("WARNING"))
      {
        //Log warnings
        tempText = message.substring(0,tagVO.getTagLength());
        warnings.add(tempText);
        message = message.substring(tagVO.getTagLength());
      }
      else if (tagVO.getTagText().equals("MESSAGE TYPE"))
      {
        //we do not do anything with message type
        tempText = message.substring(0,tagVO.getTagLength());
        message = message.substring(tagVO.getTagLength());
      }
      else if (tagVO.getTagText().equals("MESSAGE IN ERROR"))
      {
        //get the message number that was in error
        tempText = message.substring(0,tagVO.getTagLength());
        message = message.substring(tagVO.getTagLength());
        int errorNumber = Integer.parseInt( tempText );
        batchResponse.setMessageError(true);
        //now get the associated mercury message that goes with this message number
        if(outgoingMessages.size() >= errorNumber){
          MercuryMessageVO mercuryMessage = (MercuryMessageVO)outgoingMessages.get(errorNumber-1);
          batchResponse.setErrorMercuryID(mercuryMessage.getMercuryID());
        }        
      }
      else if (tagVO.getTagText().equals("MESSAGE IN ERROR TEXT"))
      {
        tempText = message.substring(0,tagVO.getTagLength());
        batchResponse.setErrorMessageText(tempText);
        message = message.substring(tagVO.getTagLength());
       
      }
      else
      {
        tempText = message.substring(0,tagVO.getTagLength());
        String errorMessage = ("Unknown Tag = " + tagVO.getTagText() + ": " + tempText);
        message = message.substring(tagVO.getTagLength());
        batchResponse.setErrorText(errorMessage);
        batchResponse.setBatchError(true);
      }
    }//end while loop
    
    //if a specfic message was flagged as being in error then the entire batch is not invalid
    if(batchResponse.getErrorMercuryID() != null && batchResponse.getErrorMercuryID().length() > 0)
    {
      batchResponse.setBatchError(false);
    }
    
    //set warnings
    batchResponse.setWarnings(warnings);

    return batchResponse;

  } // parseMessage




  /*
   * This method parses out the details of a response of incoming message.  
   * Each tag found in the response or messge must exist in the code below.
   */
  static private BatchResponseVO parseDetail (BatchResponseVO batchResponse, String message, String messageType,int messageTagLength,List outgoingMessages )
      throws Exception
  {
    int pos = 0;
    int pos2 = 0;
    int msgCount = 0;
    int msgNumber = 0;
    String tagMsg = "";
    String tempMsg = "";
    String tempText = "";
    
    int messageCount = 0;
    
    
    //get the number of messages\responses that are in the batch
    msgCount = Integer.parseInt( message.substring(0, messageTagLength) );
 
    //remove count from message string
    message = message.substring(messageTagLength);
    
   

    TagVO tagVO = new TagVO();

    //list of messages
    List messageList = new ArrayList();

    //for each message\respones
    for (int x=0; x < msgCount; x++)
    {
      //get the text that starts out this messsage\response
      pos = message.indexOf("]");
      tempText = message.substring(0,pos+1);
      message = message.substring(pos+1);
      
      //Get the tag text and length
      tagVO = parseTag(tempText, ";");
      
      //Get the message\response number
      pos = tagVO.getTagText().indexOf("(");
      pos2 = tagVO.getTagText().indexOf(")",pos + 1);
      msgNumber = Integer.parseInt(tagVO.getTagText().substring(pos+1, pos2));
     
      MercuryMessageVO mercuryMessage = new MercuryMessageVO();

      //If this is a reponse message then populate the ID and Type field
      //SAK Responses do not includes these fields, so we have to look them up based
      //on what we sent out.
      if ( messageType.equals("Responses") )
     {
        //make sure an array out of bounds error does not occur
        if(outgoingMessages.size() >= msgNumber){
          MercuryMessageVO mercuryMessageOutGoing = (MercuryMessageVO)outgoingMessages.get(msgNumber-1);
          mercuryMessage = (MercuryMessageVO)CommonUtilites.deepCopy(mercuryMessageOutGoing);
       }
     }
  
      tempMsg = message.substring(0,tagVO.getTagLength());
      message = message.substring(tagVO.getTagLength());
      
      //loop through all the values in the message\response
      while ( !tempMsg.equals("") )
      {      
        //get the field name and value
        pos = tempMsg.indexOf("]");
        tempText = tempMsg.substring(0,pos+1);
        tempMsg = tempMsg.substring(pos+1);
        tagVO = parseTag(tempText, ";");      
        tagMsg = tempMsg.substring(0,tagVO.getTagLength());
        tempMsg = tempMsg.substring(tagVO.getTagLength());
        
        //set the field values
        if ( tagVO.getTagText().equals("MERC MESSAGE TYPE") || tagVO.getTagText().equals("MESSAGE TYPE"))
        {
          //if message type length is > 3 then convert message type to 3 char type
          if(tagMsg.length() > 3)
          {
            tagMsg = convertMessageType(tagMsg);
          }
          mercuryMessage.setMessageType(tagMsg);
        }
        else if ( tagVO.getTagText().equals("CTSEQ") )
        {
          mercuryMessage.setCTSEQ(Integer.parseInt(tagMsg));
        }
        else if ( tagVO.getTagText().equals("CRSEQ") )
        {
          mercuryMessage.setCRSEQ(Integer.parseInt(tagMsg));
        }
        else if ( tagVO.getTagText().equals("SAK TEXT") )
        {
          mercuryMessage.setSakText(tagMsg);
        }
        else if ( tagVO.getTagText().equals("MERCURY MESSAGE NUMBER") )
        {
          mercuryMessage.setMercuryMessageNumber(tagMsg);
        }
        else if ( tagVO.getTagText().equals("SENDING FLORIST CODE") )
        {
          mercuryMessage.setSendingFlorist(tagMsg);
        }
        else if ( tagVO.getTagText().equals("FILLING FLORIST CODE") )
        {
          mercuryMessage.setFillingFlorist(tagMsg);
          doRetrivialCheck(batchResponse,mercuryMessage,tagMsg); 
        }
        else if ( tagVO.getTagText().equals("FILLING FLORIST CODE DESCRIPTION") )
        {
          doRetrivialCheck(batchResponse,mercuryMessage,tagMsg); 
        }
        else if ( tagVO.getTagText().equals("ORDER NUMBER") )
        {
          doRetrivialCheck(batchResponse,mercuryMessage,tagMsg); 

          //look for retrieval flags
          if(mercuryMessage.getRetrievalFlag() != null && mercuryMessage.getRetrievalFlag().length() >0)
          {            
            //parse out order number
            pos = tagVO.getTagText().indexOf(" ");
            if(pos >= 0)
            {
              mercuryMessage.setMercuryOrderNumber(tagVO.getTagText().substring(0,pos));
            }
          }
          else{
            //this is not a retrieval
            mercuryMessage.setMercuryOrderNumber(tagMsg);
          }
        }
        else if ( tagVO.getTagText().equals("RECIPIENT NAME") )
        {
          mercuryMessage.setRecipient(tagMsg);
        }
        else if ( tagVO.getTagText().equals("OPERATOR") )
        {
          mercuryMessage.setOperator(tagMsg);
        }
        else if ( tagVO.getTagText().equals("DELIVERY DATE") || tagVO.getTagText().equals("RAW DELIVERY DATE"))
        {
          mercuryMessage.setDeliveryDateText(tagMsg);
        }        
        else if ( tagVO.getTagText().equals("PRICE") )
        {
          if(tagMsg != null){
              mercuryMessage.setPrice(new Double(tagMsg));
            }
        }        
        else if ( tagVO.getTagText().equals("ORDER DATE") )
        {
          try{
            mercuryMessage.setOrderDate(FieldUtils.formatStringTimeToUtilDate(tagMsg));
          }
          catch(Exception e)
          {
            String errorMessage = "Unknown order date format:" + tagVO.getTagText();
            throw new Exception(errorMessage);
          }
        }        
        else if ( tagVO.getTagText().equals("RECIPIENT STREET ADDRESS") )
        {
          mercuryMessage.setAddress(tagMsg);
        }        
        else if ( tagVO.getTagText().equals("RECIPIENT CITY STATE ZIP") )
        {
          mercuryMessage.setCityStateZip(tagMsg);
        }        
        else if ( tagVO.getTagText().equals("ASK ANSWER CODE") )
        {
          mercuryMessage.setAskAnswerCode(tagMsg);
        }        
        else if ( tagVO.getTagText().equals("REASON TEXT") || tagVO.getTagText().equals("GENERAL MESSAGE TEXT"))
        {
          mercuryMessage.setComments(tagMsg);
        }        
        else if ( tagVO.getTagText().equals("RECIPIENT PHONE") )
        {
          mercuryMessage.setPhoneNumber(tagMsg);
        }        
        else if ( tagVO.getTagText().equals("FIRST PRODUCT CODE") )
        {
          //if message contains a product code and description then join both values
          if(mercuryMessage.getFirstChoice() != null && mercuryMessage.getFirstChoice().length() > 0){
            mercuryMessage.setFirstChoice(tagMsg + " " + mercuryMessage.getFirstChoice());
          }
          else
          {
            mercuryMessage.setFirstChoice(tagMsg);
          }
        }        
        else if ( tagVO.getTagText().equals("FIRST PRODUCT DESCRIPTION") )
        {
          //if message contains a product code and description then join both values        
          if(mercuryMessage.getFirstChoice() != null && mercuryMessage.getFirstChoice().length() > 0){
            mercuryMessage.setFirstChoice(mercuryMessage.getFirstChoice() + " " + tagMsg);
          }
          else
          {
            mercuryMessage.setFirstChoice(tagMsg);
          }
        }        
        else if ( tagVO.getTagText().equals("CARD MESSAGE") )
        {
          mercuryMessage.setCardMessage(tagMsg);
        }        
        else if ( tagVO.getTagText().equals("OCCASION") )
        {
          mercuryMessage.setOccasion(tagMsg);
        }     
        else if ( tagVO.getTagText().equals("FROM MESSAGE NUMBER") )
        {
          mercuryMessage.setRetrievalFromMessage(tagMsg);
        }        
        else if ( tagVO.getTagText().equals("FROM MESSAGE DATE") )
        {
          mercuryMessage.setRetrievalFromDate(tagMsg);
        }        
        else if ( tagVO.getTagText().equals("TO MESSAGE NUMBER") )
        {
          mercuryMessage.setRetrievalToMessage(tagMsg);
        }        
        else if ( tagVO.getTagText().equals("TO MESSAGE DATE") )
        {
          mercuryMessage.setRetrievalToDate(tagMsg);
        } 
        else if ( tagVO.getTagText().equals("SPECIAL INSTRUCTIONS") )
        {
          mercuryMessage.setSpecialInstructions(tagMsg);
        } 
        else if ( tagVO.getTagText().equals("RAW MESSAGE PRIORITY") || tagVO.getTagText().equals("MESSAGE PRIORITY") )
        {
          mercuryMessage.setPriority(tagMsg);
        }        
        else if ( tagVO.getTagText().equals("MESSAGE HEADER") )
        {
          doRetrivialCheck(batchResponse,mercuryMessage,tagMsg);          
        }//end if
 

      }//end while

        //add message to list
        messageList.add(mercuryMessage);      

    }//end message loop
    
    //put message list in either response or message list 
    if ( messageType.equals("Responses") )
    {
      batchResponse.setResponses(messageList);
    }else
    {
      batchResponse.setMessages(messageList);
    }            

    //put remaining part of string into return object
    batchResponse.setRemainingMessage(message);

    return batchResponse;
    
  } //parseDetail
   
 
 
 /*
  * Convert the long message type into a short message type.
  * 
  */
 private static String convertMessageType(String longMsgType)
 {
     String shortType = "";   
 
     //use first 3 chars from message type
     shortType = longMsgType.substring(0,3);
     
     return shortType;
   }   


 
  /*
  * Check if text contains retrival flags.  If so make the needed VO changes.
  */
 private static void doRetrivialCheck(BatchResponseVO batchResponse,MercuryMessageVO mercuryMessage,String text)
 {
      //look for autoretrieval flags
      int pos = text.indexOf("RETRANSMISSION");
      if(pos >= 0)
      {
        //manual retrieval
        mercuryMessage.setRetrievalFlag(MercuryConstants.MANUAL_RETRIEVAL);
      }
      else if(text.indexOf("AUTO-RET") >= 0)
      {
        //auto retrieval
        mercuryMessage.setRetrievalFlag(MercuryConstants.AUTO_RETRIEVAL);

        //set flag
        batchResponse.setAutoRetrieveReceived(true);
      }               
 }

    //this method checks for carriage returns.  
    //It returns the count of how many carriage returns it finds, multiplied by 17
    private static int getCarriageReturnCount(String batchString)
    {
    
      int count = 0;
      int pos = batchString.indexOf(LINE_FEED);
      while ( pos != -1 )
      {
        count++;
        pos = batchString.indexOf(LINE_FEED, pos+1);
      }            

      int returnValue = count * 17 ;
      
      return returnValue;

    }
  
}