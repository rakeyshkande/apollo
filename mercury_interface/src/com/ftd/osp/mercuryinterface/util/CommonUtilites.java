package com.ftd.osp.mercuryinterface.util;

import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.handlers.GlobalParmHandler;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.j2ee.InitialContextUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.osp.framework.dispatcher.*;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;
import java.sql.Connection;
import com.ftd.osp.mercuryinterface.constants.MercuryConstants;

/**
 * This object contains common utilties used within project
 */
public class CommonUtilites
{

  private static String LOGGER_CATEGORY = "com.ftd.osp.mercuryinterface.util.CommonUtilites";

  public CommonUtilites()
  {
  }

    /*
     * Get database connection
     */
    static public Connection getConnection() throws Exception
    {
        ConfigurationUtil config = ConfigurationUtil.getInstance();
        String dbConnection = config.getProperty(MercuryConstants.MERCURY_PROPERTY_FILE,"DATASOURCE");

        //get DB connection
        return DataSourceUtil.getInstance().getConnection(dbConnection);
    }


  /**
   * Sends out a JMS message.
   *
   * @param context Initial Context
   * @param messageToken JMS Message
   * @throws java.lang.Exception
   */
    public static void sendJMSMessage(InitialContext context,MessageToken messageToken)
        throws Exception {


       //make the correlation id the same as the message
       messageToken.setJMSCorrelationID((String)messageToken.getMessage());

       Dispatcher dispatcher = Dispatcher.getInstance();
       messageToken.setStatus("SENDSUFFIX");
       dispatcher.dispatchTextMessage(context,messageToken);
  }


/**
 * This procedure is used to convert java.util.Date to a
 * java.sql.Date.
 *
 * @param java.util.Date
 * @returns java.sql.Date
 */
public static java.sql.Date getSQLDate(java.util.Date value)
{
  java.sql.Date sqlDate = null;
  if(value != null){
    sqlDate = new java.sql.Date(value.getTime());
  }
  return sqlDate;
}

/**
 * This procedure is used to convert java.util.Date to a
 * timestamp.
 *
 * @param java.util.Date
 * @returns java.sql.Date
 */
public static java.sql.Timestamp getTimestamp(java.util.Date value)
{
  java.sql.Timestamp sqlDate = null;
  if(value != null){
    sqlDate = new java.sql.Timestamp(value.getTime());
  }
  return sqlDate;
}





/**
   * Convert String to double.  Returns 0.00 if string is null or empty.
   *
   * @param conn
   * @param message
   * @return
   * @throws java.lang.Exception
   */
   public static double getDouble(String value)
   {
     double retValue = 0.00;

     if(value != null && value.length() > 0)
     {
       retValue = Double.parseDouble(value);
     }

     return retValue;
   }


  /**
   * Checks if a field is empty (is empty string or null)
   * @param conn
   * @param message
   * @return
   * @throws java.lang.Exception
   */
   public static boolean isEmpty(String value)
   {
     return (value == null || value.length() ==0) ? true : false;
   }

  /**
   * Checks if a field is empty (is empty string or null)
   * @param conn
   * @param message
   * @return
   * @throws java.lang.Exception
   */
   public static boolean isEmpty(java.util.Date value)
   {
     return (value == null ) ? true : false;
   }


/*
 * This method sends a message to the System Messenger.
 *
 * @param String message
 * @returns String message id
 */
static public String sendSystemMessage(String message) throws Exception
{
      Logger logger =  new Logger(LOGGER_CATEGORY);

      logger.error("Sending System Message:" + message);

      ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
      String messageSource = configUtil.getProperty(MercuryConstants.MERCURY_PROPERTY_FILE,"MESSAGE_SOURCE");

      String messageID = "";

      //build system vo
      SystemMessengerVO sysMessage = new SystemMessengerVO();
      sysMessage.setLevel(SystemMessengerVO.LEVEL_PRODUCTION);
      sysMessage.setSource(messageSource);
      sysMessage.setType("ERROR");
      sysMessage.setMessage(message);

      SystemMessenger sysMessenger = SystemMessenger.getInstance();
      Connection conn = getConnection();
      try
      {
        messageID = sysMessenger.send(sysMessage,conn);
      }
      finally
      {
				//close the connection
				try
				{
					conn.close();
				}
				catch(Exception e)
				{}
      }

      if(messageID == null)
      {
        String msg = "Error occured while attempting to send out a system message. Msg not sent: " + message;
        logger.error(msg);
        System.out.println(msg);
      }

      return messageID;
}

   /**
   * Returns a transactional resource from the EJB container.
   *
   * @param jndiName
   * @return
   * @throws javax.naming.NamingException
   */
//  public static Object lookupResource(String jndiName)
//              throws Exception
//  {
//      return InitialContextUtil.getInstance().lookupEnvironmentEntry(new InitialContext(), jndiName, Object.class);
//  }

  /**
   * Truncate a string
   */
  public static String truncate(String value,int size)
  {
    if(value != null && value.length() > size)
    {
      value = value.substring(0,size);
    }

    return value;
  }

    /**
     * Clone an object to reference original object.
     * @param oldObj Object
     * @exception xException
     * @return Object
     */
    public static Object deepCopy(Object oldObj) throws Exception
    {
      ObjectOutputStream out = null;
      ObjectInputStream in = null;
      Object newobj = null;

      try
      {
        ByteArrayOutputStream buf = new ByteArrayOutputStream();
        out = new ObjectOutputStream(buf);
        out.writeObject(oldObj);
        in = new ObjectInputStream(new
        ByteArrayInputStream(buf.toByteArray()));
        newobj = in.readObject();
        return newobj;
      }
      catch(Exception e)
      {
        throw(e);
      }
      finally
      {
        out.close();
        in.close();
      }
    }

    /*
     * Checks the cache to determine if view queue flagging is turned on
     */
    public static boolean isViewQueueFlagOn() throws Exception
    {
      boolean viewQueueFlag = false;

      GlobalParmHandler globalParamHandler = (GlobalParmHandler)CacheManager.getInstance().getHandler("CACHE_NAME_GLOBAL_PARM");
      String viewQueueString = null;
      if(globalParamHandler != null)
      {
          viewQueueString = globalParamHandler.getFrpGlobalParm("LOAD_MEMBER_DATA","VIEW_QUEUE_SWITCH");
      }
      else
      {
          throw new Exception("GlobalParamHandler is null. Method=isViewQueueFlagOn");
      }

      return viewQueueString == null || viewQueueString.equals("Y") ? true : false;

    }

}