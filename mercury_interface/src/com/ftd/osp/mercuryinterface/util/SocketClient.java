

package com.ftd.osp.mercuryinterface.util;

import java.lang.Long;

import com.ftd.osp.mercuryinterface.constants.MercuryConstants;
import com.ftd.osp.mercuryinterface.vo.SocketConnectionVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.InputStream;
import java.io.DataInputStream;

import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

/**
 * Helper class for socket transmissions.
 *
 * @author Brian Munter
 */

public class SocketClient 
{
    private Socket socket;
    private OutputStreamWriter writer;
    private BufferedReader reader;
    private static Logger logger;
    private List serverList;
    private boolean sendAlerts;

    private InputStream in;
    
    private int socketTimeOut;


  /**
   *  Constructor
   *  
   * @List List of servers to connect to
   * @throws java.io.IOException
   */
    public SocketClient(List serverList) throws IOException, SAXException, TransformerException,ParserConfigurationException, Exception
    {
        this.serverList = serverList;
        
        logger = new Logger("com.ftd.osp.mercuryinterface.util.SocketClient");
        
        
        //get amount of time to wait while data
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();            
        long timeoutTemp = Long.parseLong(configUtil.getFrpGlobalParm(MercuryConstants.MERCURY_CONFIG_CONTEXT,"SOCKET_READ_TIMEOUT"));


        //convert seconds to milliseconds
        timeoutTemp = timeoutTemp * 1000;
        
        //check the timeout size before converting it to an Integer
        if(timeoutTemp > Integer.MAX_VALUE)
        {
          socketTimeOut = Integer.MAX_VALUE;
        }
        else
        {
          socketTimeOut = new Long(timeoutTemp).intValue();
        }
    }

    /**
   * Open socket connection
   * 
   * This method will attempt to connect to the first server in the serverList.
   * If it cannot open a connection to that sever then it will send out a system
   * message and attempt to connect to the next server.  It will continue to do 
   * this until a good connection is made or it runs out of servers to try.
   * 
   * @param boolean Indicates whether or not to send alert messages when a failover occurs
   * @returns boolean TRUE=made a connection
   * 
   * @throws java.io.IOException
   */
    public boolean open(boolean sendAlerts) throws Exception 
    {
    
        boolean connectionMade = false;
        SocketConnectionVO socketVO = null;
        
        //loop through list of severs until a good connection can be made
        Iterator iter = serverList.iterator();
        while(!connectionMade && iter.hasNext())
        {
          socketVO = (SocketConnectionVO)iter.next();

          try{              
              socket = new Socket(socketVO.getIPAddress(), Integer.parseInt(socketVO.getPort()));
              socket.setSoTimeout(socketTimeOut);
              writer = new OutputStreamWriter(socket.getOutputStream());
              reader = new BufferedReader(new InputStreamReader( socket.getInputStream()));
              in = new DataInputStream( socket.getInputStream());
              
              //if we made it this far then a connection was made
              connectionMade = true;
          }
          catch(Exception e)
          {
            logger.error(e);
            String errorMessage = "Socket connection to "+socketVO.getIPAddress() + ":" + socketVO.getPort() + " failed.  ";

            //If we are going to try to connect to another server send out a system message so that
            //support knows that one server is down.
            if(iter.hasNext())
            {
              //only send out system message if we need to (this is not done when efos is down)
              if(sendAlerts){
                  errorMessage = errorMessage + "Connection will be made with next server in list.";
                  CommonUtilites.sendSystemMessage(errorMessage);
              }
            }
          }//end try block
        }//end loop through each connection
        
        //if connection made log it
        if(connectionMade)
        {
          logger.info("Connection made to "+socketVO.getIPAddress() + ":" + socketVO.getPort());
        }
        
        return connectionMade;
    }

  /**
   * Send Message out a message
   * 
   * There is some screwy stuff in here related to communication with Box X
   * If the final acknowledgement being sent then the protocol for receiving
   * the response is handled differently.
   * 
   * @param message Text to be sent to box x
   * @param finalAck Sending the final acknowledgement
   * @return 
   * @throws java.io.IOException
   */
    public String send(String message, boolean finalAck) throws IOException 
    {
        char[] chars = message.toCharArray();
        String returnString = "";
        char[] buf = new char[16];

        // send/write batch message
        writer.write( chars, 0, chars.length );
        writer.flush();
        
        if( finalAck ) 
        {
          StringBuffer sb = new StringBuffer();
          int cnt;
          
          while( true ) 
          {
            cnt = reader.read(buf,0,buf.length);
            if( cnt>-1 ) {
              sb.append(String.valueOf(buf,0,cnt));
            }
            else 
            {
              break;
            }
          } 
          
          returnString = sb.toString();
        }
        else 
        {
          int backTotal = 9999;
          String work = "";
          //Read 16 byte header
          byte[] w = new byte[16];
        
          int i = in.read(w,0,16);
          //logger.debug("read result = " + i + " " + w[0] + w[1] + w[2] + w[3] + w[4] + w[5] + w[6] + w[7]);
          //Do some bit stuff to get a number out of the header.
          //The data is in binary, that is why this crazy stuff is being done.
          backTotal = (w[9]&0xff) << 8 | (w[8]&0xff);
          buf = new char[backTotal];
          //logger.debug("backTotal=" + backTotal);
        
          while ( backTotal != 0 )
          {
          
            //read in data
            i = reader.read( buf );
            
            //if -1 then there is nothing to read..exit
            if ( i == -1 ) 
            {            
              break;
            }
            
            work = new String( buf, 0, i);
            returnString = returnString + work;
            
            //logger.debug("i=" + i + " " + work.length());
            backTotal = backTotal - i;
            buf = new char[backTotal];
            //logger.debug("backTotal=" + backTotal);
            
          }//end loop
        }
        
        return returnString;
    }
    
    
    /**
   *  Close connection
   * @throws java.io.IOException
   */
    public void close() throws IOException
    {   
       
        if (writer != null)
        {
           writer.close();
        }
        if (reader != null)
        {
            reader.close();
        }  
        if (socket != null)
        {
            socket.close();
        } 
    }
    
  /**
   * Accessor to change the timeout seconds for the socket 
   * @param seconds to set.  Will be converted to milliseconds.
   * @throws SocketException 
   */
  public void setSocketTimeout(int seconds) throws SocketException//convert seconds to milliseconds

  {
    long timeoutTemp = seconds * 1000;  //check the timeout size before converting it to an Integer
    int newTimeOut;
    if (timeoutTemp > Integer.MAX_VALUE)
    {
      newTimeOut = Integer.MAX_VALUE;
    }
    else 
    {
      newTimeOut = new Long(timeoutTemp).intValue();
    }
    if (socket != null)
    {
      socket.setSoTimeout(newTimeOut);
    }
    this.socketTimeOut = newTimeOut;
  }
    
  /**
   * Accessor to retrieve the currently assigned socket timeout.
   * @return the currently assigned socket timeout value
   */
  public int getSocketTimeout()
  {
    return this.socketTimeOut;
  }
    
    
    public static void main(String[] args)
    {
      try{
        ArrayList list = new ArrayList();
        SocketConnectionVO vo = new SocketConnectionVO();
        vo.setIPAddress("125.55.15.11");
        vo.setPort("8080");
        list.add(vo);
        SocketClient x = new SocketClient(list);
        x.open(false);
      }
      catch(Exception e)
      {
        System.out.println(e);
      }
    }
    
}