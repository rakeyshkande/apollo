package com.ftd.osp.mercuryinterface.util;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.mercuryinterface.vo.*;
import com.ftd.osp.mercuryinterface.constants.*;

/**
 * This object contains the validation rules used for outgoing messages.
 */
public class MercuryValidator 
{

  private Logger logger;
  private double VALID_PRICE_MIN = .01;
  private double VALID_PRICE_MAX = 99999.99;
  private static String LOGGER_CATEGORY = "com.ftd.osp.mercuryinterface.util.MercuryValidator";

  public MercuryValidator()
  {
    logger =  new Logger(LOGGER_CATEGORY);
  }
  
  /**
   * Validate the passed in message
   * 
   * @param mercuryVO mesasge to validate
   * @return ValiationResponse
   * @throws java.lang.Exception
   */
  public ValidationResponse validate(MercuryMessageVO mercuryVO) throws Exception
  {
  
    ValidationResponse response = null;
  
    //get the message type
    String msgType = mercuryVO.getMessageType();
    if(msgType == null)
    {
      throw new Exception("Message Type is null");
    }
       
    //Determine the mesage type and what validation should be done
    if(msgType.equals(MercuryConstants.ANS_MESSAGE))
    {
      response = validateANS(mercuryVO);      
    }
    else if(msgType.equals(MercuryConstants.ASK_MESSAGE))
    {
      response = validateASK(mercuryVO);      
    }
    else if(msgType.equals(MercuryConstants.CAN_MESSAGE))
    {
      response = validateCAN(mercuryVO);      
    }
    else if(msgType.equals(MercuryConstants.FTD_MESSAGE))
    {
      response = validateFTD(mercuryVO);      
    }
    else if(msgType.equals(MercuryConstants.GEN_MESSAGE))
    {
      response = validateGEN(mercuryVO);      
    }
    else if(msgType.equals(MercuryConstants.ADJ_MESSAGE))
    {
      response = validateADJ(mercuryVO);      
    }    
    else if(msgType.equals(MercuryConstants.REJ_MESSAGE))
    {
      response = validateREJ(mercuryVO);      
    }  
    else if(msgType.equals(MercuryConstants.RET_MESSAGE))
    {
      response = validateRET(mercuryVO);      
    }  
    else
    {
      response = new ValidationResponse();
      response.setValid(false);
      response.setMessage("Unknown Message Type:" + msgType);
    }
    

    return response;
    
  }    
  
  
  /*
   * Validate ANS Message
   */
  private ValidationResponse validateANS(MercuryMessageVO vo)
  {
    ValidationResponse response = new ValidationResponse();
    response.setValid(true);

    //validate common fields
    response = validateCommonFields(response,vo);
    
    //only validate price if Ask Answer code contains a value
    if(vo.getAskAnswerCode() != null && vo.getAskAnswerCode().length() > 0)
    {          
    
        //validate price
        if(response.isValid() && !validPrice(vo.getPrice()))
        {
          response.setMessage("Invalid Price");
          response.setValid(false);
        }

    }
   
    return response;
  }

  /*
   * Validate RET message
   */
  private ValidationResponse validateRET(MercuryMessageVO vo)
  {
    ValidationResponse response = new ValidationResponse();
    response.setValid(true);



    if(response.isValid() && CommonUtilites.isEmpty((vo.getRetrievalFromDate())))
    {
      response.setMessage("Missing Retrieval From Date");
      response.setValid(false);
    }    

    if(response.isValid() && CommonUtilites.isEmpty((vo.getRetrievalToDate())))
    {
      response.setMessage("Missing Retrieval To Date");
      response.setValid(false);
    }    

    if(response.isValid() && !validOrderNumber((vo.getRetrievalFromMessage())))
    {
      response.setMessage("Missing Retrieval From Number");
      response.setValid(false);
    }    

    if(response.isValid() && !validOrderNumber((vo.getRetrievalToMessage())))
    {
      response.setMessage("Missing Retrieval To Number");
      response.setValid(false);
    }    
    
    return response;
  }   

  /*
   * Validate ASK message
   */
  private ValidationResponse validateASK(MercuryMessageVO vo)
  {
    ValidationResponse response = new ValidationResponse();
    response.setValid(true);

    //validate common fields
    response = validateCommonFields(response,vo);

    //only validate price if Ask Answer code is P
    if(vo.getAskAnswerCode() != null && vo.getAskAnswerCode().length() > 0)
    {      

        //validate price
        if(response.isValid() && !validPrice(vo.getPrice()))
        {
          response.setMessage("Invalid Price");
          response.setValid(false);
        }    
    }
    
    return response;
  } 

  
  /*
   * Validate CAN message
   */
  private ValidationResponse validateCAN(MercuryMessageVO vo)
  {
    ValidationResponse response = new ValidationResponse();
    response.setValid(true);

    //validate common fields
    response = validateCommonFields(response,vo);

    return response;

  }
  
  /*
   * Validate FTD message
   */
  private ValidationResponse validateFTD(MercuryMessageVO vo)
  {
    ValidationResponse response = new ValidationResponse();
    response.setValid(true);

        if (response.isValid() && CommonUtilites.isEmpty(vo.getOrderDate()))
        {
          response.setValid(false);
          response.setMessage("Missing Order Date");
        }  

        if (response.isValid() && CommonUtilites.isEmpty(vo.getDeliveryDateText()))
        {
          response.setValid(false);
          response.setMessage("Missing Delivery Date");
        }  
        
        if (response.isValid() && CommonUtilites.isEmpty(vo.getOperator()))
        {
          response.setValid(false);
          response.setMessage("Missing Operator");
        }          

    if (response.isValid() && CommonUtilites.isEmpty(vo.getFillingFlorist()))
    {
      response.setValid(false);
      response.setMessage("Missing filling florist code");
    }  
    else
    {
      if(!validFillingFlorist(vo.getFillingFlorist()))
      {
        response.setValid(false);
        response.setMessage("Invalid filling florist number");
      }
    }
    
    if (response.isValid() && CommonUtilites.isEmpty(vo.getRecipient()))
    {
      response.setValid(false);
      response.setMessage("Missing recipient name");
    }      

    if (response.isValid() && CommonUtilites.isEmpty(vo.getAddress()))
    {
      response.setValid(false);
      response.setMessage("Missing address");
    }      
    
    if (response.isValid() && CommonUtilites.isEmpty(vo.getCityStateZip()))
    {
      response.setValid(false);
      response.setMessage("Missing City/State/Zip");
    }      
    
    if (response.isValid() && CommonUtilites.isEmpty(vo.getPhoneNumber()))
    {
      response.setValid(false);
      response.setMessage("Missing phone number");
    }      
    
    if (response.isValid() && CommonUtilites.isEmpty(vo.getFirstChoice()))
    {
      response.setValid(false);
      response.setMessage("Missing first choice");
    }      
    
    if (response.isValid() && CommonUtilites.isEmpty(vo.getSecondChoice()))
    {
      response.setValid(false);
      response.setMessage("Missing second choice");
    }      
    
    if (response.isValid() && !validPrice(vo.getPrice()))
    {
      response.setValid(false);
      response.setMessage("Invalid price");
    }      
    
    if (response.isValid() && CommonUtilites.isEmpty(vo.getOccasion()))
    {
      response.setValid(false);
      response.setMessage("Missing occasion");
    }      
    
    if (response.isValid() && CommonUtilites.isEmpty(vo.getPriority()))
    {
      response.setValid(false);
      response.setMessage("Missing priority");
    }      
    


    return response;
  }
  
  /*
   * Validate GEN message
   */
  private ValidationResponse validateGEN(MercuryMessageVO vo)
  {
    ValidationResponse response = new ValidationResponse();
    response.setValid(true);
    
    if (response.isValid() && CommonUtilites.isEmpty(vo.getFillingFlorist()))
    {
      response.setValid(false);
      response.setMessage("Missing filling florist code");
    }  
    else
    {
      if(!validFillingFlorist(vo.getFillingFlorist()))
      {
        response.setValid(false);
        response.setMessage("Invalid filling florist value");
      }
    }

    if (response.isValid() && CommonUtilites.isEmpty(vo.getComments()))
    {
      response.setValid(false);
      response.setMessage("Missing Comments");
    }  

    if (response.isValid() && CommonUtilites.isEmpty(vo.getOperator()))
    {
      response.setValid(false);
      response.setMessage("Missing Operator");
    }     

    if (response.isValid() && CommonUtilites.isEmpty(vo.getPriority()))
    {
      response.setValid(false);
      response.setMessage("Missing Priority");
    }     


    return response;
  }
  

  /*
   * Validate REJ message
   */
  private ValidationResponse validateREJ(MercuryMessageVO vo)
  {
    ValidationResponse response = new ValidationResponse();
    response.setValid(true);

    //validate common fields
    response = validateCommonFields(response,vo);
   
    return response;
  }  
  
  private ValidationResponse validateADJ(MercuryMessageVO vo)
  {
    ValidationResponse response = new ValidationResponse();
    response.setValid(true);


    return response;
  }    
  
  
    /*
     * Validate Mercury Order Number, Order Date, Deliery Date Text, Comments Operator
     */
     private ValidationResponse validateCommonFields(ValidationResponse response,MercuryMessageVO vo)
     {
 
        if (response.isValid() && CommonUtilites.isEmpty(vo.getMercuryOrderNumber()))
        {
          response.setValid(false);
          response.setMessage("Missing Order Number");
        }
        else
        {
          if(!validOrderNumber(vo.getMercuryOrderNumber()))
          {
            response.setValid(false);
            response.setMessage("Invalid Order Number");            
          }
        }

        if (response.isValid() && CommonUtilites.isEmpty(vo.getOrderDate()))
        {
          response.setValid(false);
          response.setMessage("Missing Order Date");
        }  

        if (response.isValid() && CommonUtilites.isEmpty(vo.getDeliveryDateText()))
        {
          response.setValid(false);
          response.setMessage("Missing Delivery Date");
        }  

        if (response.isValid() && CommonUtilites.isEmpty(vo.getComments()))
        {
          response.setValid(false);
          response.setMessage("Missing Reason");
        }  
        
        if (response.isValid() && CommonUtilites.isEmpty(vo.getOperator()))
        {
          response.setValid(false);
          response.setMessage("Missing Operator");
        }          

        return response;
     }


  /*
   * This method makes use an order number is in the format of A####A-####
   */
  public boolean validOrderNumber(String value)
  {
    //start assuming value is invalid
    boolean valid = false;
    
    //check if null
    if(value != null)
    {
      //first char must be letter
      if(Character.isLetter(value.charAt(0)))
      {
          //fifth char must be letter
          if(Character.isLetter(value.charAt(5)))
          {
              //must have dash in sixth position
              if(value.substring(6,7).equals("-"))
              {
                  try
                  {                 
                    Integer.parseInt(value.substring(1,5));
                    Integer.parseInt(value.substring(7,11));
                    
                    //if we made it this far then value is valid
                    valid = true;
                  }
                  catch(Exception e)
                  {
                    //not numeric
                  }
              }//end must have dash            
          }//end fifth char letter
      }//end first char letter
    }//if correct length

    return valid;
    }
    
  /*
   * Check if the price is valid
   */
   private boolean validPrice(Double value)
   {
     boolean valid = true;
     
     //null check
     if(value == null)
     {
         return false;
     }
     
     double dblValue = value.doubleValue();
     
     
     //check min
     if(dblValue < VALID_PRICE_MIN)
     {
       valid = false;
     }

     //check max
     if(dblValue > VALID_PRICE_MAX)
     {
       valid = false;
     }
   
     return valid;
   }
   
   /* 
    * Validaate filing florist format
    */
    private boolean validFillingFlorist(String value)
    {
      //assume invalid to start
      boolean valid = false;
      
      //check length
      if(value.length() == 9)
      {
        //check for dash
        if(value.substring(2,3).equals("-"))
        {
            //check for chars at last two positins
            if(Character.isLetter(value.charAt(7)) && Character.isLetter(value.charAt(8)))
            {
              try
              {
                Integer.parseInt(value.substring(0,2));
                Integer.parseInt(value.substring(3,7));
                
                //if we made this far then it is valid
                valid = true;
              }
              catch(Exception e)
              {
                //invalid numbers
              }
            }//end char check
        }//end dash check
      }//end length check
      
      return valid;
    }

public static void main(String[] args)
{
  MercuryValidator me = new MercuryValidator();
  System.out.println(me.validOrderNumber("M1011C-2499/A"));
}
 
}
