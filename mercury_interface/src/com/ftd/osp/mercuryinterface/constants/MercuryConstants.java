package com.ftd.osp.mercuryinterface.constants;

public class MercuryConstants 
{

    //this is set to TRUE while running locally (testing\debuging\developing)
    public static final boolean DO_NOT_SEND_JMS_MESSAGES = true;

    //Mercury status's
    public static final String MERCURY_OPEN = "MO";  //Message waiting to be processed
    public static final String MERCURY_QUEUED = "MQ"; //Message being processed
    public static final String MERCURY_CLOSED = "MC"; //Message processed
    public static final String MERCURY_ERROR = "ME"; //Message in error
    public static final String MERCURY_REJECT = "MR";

    //Message types
    public static final String ANS_MESSAGE = "ANS";
    public static final String ASK_MESSAGE = "ASK";
    public static final String CAN_MESSAGE = "CAN";
    public static final String FTD_MESSAGE = "FTD";
    public static final String GEN_MESSAGE = "GEN";
    public static final String RET_MESSAGE = "RET";
    public static final String REJ_MESSAGE = "REJ";
    public static final String ADJ_MESSAGE = "ADJ";    
    public static final String FOR_MESSAGE = "FOR"; 

    public static final String CON_MESSAGE = "CON";

    
    //Config file name
    public static final String MERCURY_PROPERTY_FILE  = "mercury_config.xml";
    public static final String MERCURY_CONFIG_CONTEXT = "MERCURY_INTERFACE_CONFIG";
    


    public static final String MERCURY_DATASOURCE_PROPERTY = "DATASOURCE";
    public static final String JMS_DELAY_PROPERTY = "JMS_OracleDelay";
    
    //retrival values
    public static final String MANUAL_RETRIEVAL = "M";
    public static final String AUTO_RETRIEVAL = "A";
    
    public static final String EFOS_DOWN_ERROR = "EFOS is currently not available";
    public static final String BOXX_BUSY_ERROR = "Your Signon Has Been Rejected By EFOS";
    
    public static final String SAK_REJECT_TEXT = "reject";
    
    public static final String SAK_CANNOT_FILL_TEXT = "DESIGNATED FLORIST IS UNABLE TO FILL ORDER";
    public static final String COMMENTS_UNABLE_CONTACT_TEXT = "UNABLE TO CONTACT SELECTED MEMBER WITHIN ALLOTTED TIME";
    public static final String COMMENTS_SELECTED_FILLING = "SELECTED FILLING";
    
    public static final String SUSPEND_TEXT = "GO TO SUS";
    public static final String RESUME_TEXT = "GO TO RES";

    public static final String MERCURY_SYSTEM = "ADMIN SENDER:  MERCURY NETWORK";

  public MercuryConstants()
  {
    
  }
}