package com.ftd.osp.mercuryinterface.test;


import com.ftd.osp.utilities.GUID.GUIDGenerator;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import java.io.*;
import java.math.*;
import java.sql.*;
import java.sql.Connection;
import java.util.*;
import com.ftd.osp.utilities.*;
import com.ftd.osp.utilities.order.*;
import java.util.HashMap;
import java.util.Map;
import com.ftd.osp.mercuryinterface.*;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.w3c.dom.Document;
import com.ftd.osp.utilities.xml.*;
import org.w3c.dom.*;


/**
 * This class tests the price calculation on an order object.  
 **/
public class MercuryTest extends TestCase  {

  /**
   * Create a constructor that take a String parameter and passes it 
   * to the super class
   **/
  public MercuryTest(String name) {
      super(name);
  }

  /**
   * Override setup() to initialize variables
   **/   
  protected void setUp(){
  }

  /**
   * Override tearDown() to release any permanent resources you allocated in setUp 
   */
   public void tearDown(){
   }

/** This test is checking the calculations for internationial orders. */
    public void testRunMercury() throws Exception{
      MercuryProcessor mp = new MercuryProcessor();
//      mp.doMercuryProcessing("AA");
    }
    
    public void testGotoText() throws Exception{
        String SUSPEND_TEXT = "GO TO SUS";
        String RESUME_TEXT = "GO TO RES";
        String testString = "Go tO sUspended";
        
        if(testString != null && testString.toUpperCase().indexOf(SUSPEND_TEXT) >= 0)
        {
            System.out.println("Yes");
        }
        else
        {
            System.out.println("No");
        }
    }

  public void testUsingJMS() throws Exception
  {
 /*     enqueue_options dbms_aq.enqueue_options_t;
      message_properties dbms_aq.message_properties_t;
      message_handle raw(2000);
      message sys.aq$_jms_text_message;
      

      //Create a new JMS Message      
      message = sys.aq$_jms_text_message.construct();      
      
      //Set the properties in the message      
      message.set_string_property('CONTEXT','TEST');
      
      message.set_text('test' );
      dbms_aq.enqueue ('OJMS.EVENTS',enqueue_options,message_properties,message,message_handle);
      commit;
*/
  }
  
  private Connection getConnection() throws Exception {
            String driver_ = "oracle.jdbc.driver.OracleDriver";
            String database_ = "jdbc:oracle:thin:@stheno-dev.ftdi.com:1521:DEV2";
            String user_ = "osp";
            String password_ = "osp";

            Class.forName(driver_);
            Connection conn = DriverManager.getConnection(database_, user_, password_);  

            return conn;
  }


  /**
   * To run several tests at once JUnit provides an object, TestSuite which runs
   * any number of test cases together. 
   * Each test is run as a separate instance of the test class
   * You make your suite accessible to a TestRunner tool with a static method 
   * suite that returns a test suite. 
   **/
  public static Test suite(){
    TestSuite suite = new TestSuite();
    // Create an instance of of Test class that will run this test case 
    // When the test is run, the name of the test is used to look up the method 
    // to run, using reflection

      // suite.addTest(new MercuryTest("testRunMercury"));
      suite.addTest(new MercuryTest("testGotoText"));
     

    return suite;
  }

  /**
   * You can either user the text ui or swing ui to run the test
   **/
  public static void main(String[] args){
    // pass in the class of the test that needs to be run
    //junit.swingui.TestRunner.run(FrameworkTest.class);
    junit.textui.TestRunner.run(suite());
  }
  
  
}