package com.ftd.osp.mercuryinterface.vo;
import java.util.Date;


public class MercuryHPVO 
{
  private String MercuryHPID;
  private Date LastUpdated;

  public MercuryHPVO()
  {
  }

  public String getMercuryHPID()
  {
    return MercuryHPID;
  }

  public void setMercuryHPID(String newMercuryHPID)
  {
    MercuryHPID = newMercuryHPID;
  }

  public Date getLastUpdated()
  {
    return LastUpdated;
  }

  public void setLastUpdated(Date newLastUpdated)
  {
    LastUpdated = newLastUpdated;
  }
}