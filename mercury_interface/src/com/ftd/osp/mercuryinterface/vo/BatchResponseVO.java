package com.ftd.osp.mercuryinterface.vo;

import java.util.List;

public class BatchResponseVO 
{
  private List messages;
  private List responses;
  private List warnings;
  private String remainingMessage;
  
  private boolean messageError = false;
  private boolean batchError = false;
  private String errorText;
  private String errorMessageText;
  private String errorMercuryID;
  private boolean efosDown = false;
  private boolean autoRetrieveReceived = false;
  private boolean missingSAKs = false;
  private boolean boxXBusy = false;
  private boolean badSAKs = false;
  

  public BatchResponseVO()
  {
  }
  
  public void setBoxXBusy(boolean value)
  {
    boxXBusy = value;
  }
  public boolean isBoxXBusy()
  {
    return boxXBusy;
  } 

  public void setAutoRetrieveReceived(boolean value)
  {
    autoRetrieveReceived = value;
  }
  
  public boolean isAutoRetrieveReceived()
  {
    return autoRetrieveReceived;
  }    

  public void setMissingSAKs(boolean value)
  {
    missingSAKs = value;
  }
  
  public boolean isMissingSAKs()
  {
    return missingSAKs;
  }  
  
  public void setEfosDown(boolean value)
  {
    efosDown = value;
  }
  public boolean isEfosDown()
  {
    return efosDown;
  }    
  
  public void setErrorText(String value)
  {
    errorText = value;
  }
  
  public String getErrorText()
  {
    return errorText;
  }  
  
  public void setErrorMessageText(String value)
  {
    errorMessageText = value;
  }
  
  public String getErrorMessageText()
  {
    return errorMessageText;
  } 
  
  public void setErrorMercuryID(String value)
  {
    errorMercuryID = value;
  }
  
  public String getErrorMercuryID()
  {
    return errorMercuryID;
  }  
  
  public void setMessageError(boolean value)
  {
    messageError = value;
  }
  
  public boolean isMessageError()
  {
    return messageError;
  }    

  public void setBatchError(boolean value)
  {
    batchError = value;
  }
  public boolean isBatchError()
  {
    return batchError;
  }    
  
  public void setRemainingMessage(String value)
  {
    remainingMessage = value;
  }
  
  public String getRemainingMessage()
  {
    return remainingMessage;
  }  
  
  public void setMessages(List value)
  {
    messages = value;
  }
  public List getMessages()
  {
    return messages;
  }  
  
  public void setResponses(List value)
  {
    responses = value;
  }
  
  public List getResponses()
  {
    return responses;
  }
  
  public void setWarnings(List value)
  {
    warnings = value;
  }
  
  public List getWarnings()
  {
    return warnings;
  }    
  
  public void setBadSAKs(boolean badSAKs)
  {
    this.badSAKs = badSAKs;
  }
  
  public boolean isBadSAKs()
  {
    return badSAKs;
  }
}