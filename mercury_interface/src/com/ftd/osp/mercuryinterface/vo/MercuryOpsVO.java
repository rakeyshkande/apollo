package com.ftd.osp.mercuryinterface.vo;
import java.util.Date;

public class MercuryOpsVO 
{
  private String Suffix;
  private boolean inUse;
  private Date LastTransmission;
  private Date LastActivitiy;
  private boolean Available;
  private boolean SendNewOrders;
  private int SleepTime;
  private int maxBatchSize;
  private boolean ackAsk;
  private int ackCount=0;
  private boolean resendBatch;

  public MercuryOpsVO()
  {
  }

  public int getMaxBatchSize()
  {
    return maxBatchSize;
  }

  public void setMaxBatchSize(int value)
  {
    maxBatchSize = value;
  }

  public String getSuffix()
  {
    return Suffix;
  }

  public void setSuffix(String newSuffix)
  {
    Suffix = newSuffix;
  }

  public boolean isInUse()
  {
    return inUse;
  }

  public void setInUse(boolean newInUse)
  {
    inUse = newInUse;
  }

  public Date getLastTransmission()
  {
    return LastTransmission;
  }

  public void setLastTransmission(Date newLastTransmission)
  {
    LastTransmission = newLastTransmission;
  }

  public Date getLastActivitiy()
  {
    return LastActivitiy;
  }

  public void setLastActivitiy(Date newLastActivitiy)
  {
    LastActivitiy = newLastActivitiy;
  }

  public boolean isAvailable()
  {
    return Available;
  }

  public void setAvailable(boolean newAvailable)
  {
    Available = newAvailable;
  }

  public boolean isSendNewOrders()
  {
    return SendNewOrders;
  }

  public void setSendNewOrders(boolean newSendNewOrders)
  {
    SendNewOrders = newSendNewOrders;
  }



  public int getSleepTime()
  {
    return SleepTime;
  }

  public void setSleepTime(int newSleepTime)
  {
    SleepTime = newSleepTime;
  }


  public void setAckAsk(boolean ackAsk)
  {
    this.ackAsk = ackAsk;
  }


  public boolean isAckAsk()
  {
    return ackAsk;
  }


  public void setAckCount(int ackCount)
  {
    this.ackCount = ackCount;
  }


  public int getAckCount()
  {
    return ackCount;
  }


  public void setResendBatch(boolean resendBatch)
  {
    this.resendBatch = resendBatch;
  }


  public boolean isResendBatch()
  {
    return resendBatch;
  }
}