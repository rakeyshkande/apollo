package com.ftd.osp.mercuryinterface.vo;


public class ValidationResponse 
{

  private String message;
  private boolean valid;

  public ValidationResponse()
  {
  }
  
  public String getMessage()
  {
    return message;
  }

  public void setMessage(String value)
  {
    message = value;
  }      
  
  public boolean isValid()
  {
    return valid;
  }

  public void setValid(boolean value)
  {
    valid = value;
  }        
  
}