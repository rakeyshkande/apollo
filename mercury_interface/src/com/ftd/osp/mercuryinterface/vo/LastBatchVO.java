package com.ftd.osp.mercuryinterface.vo;

/**
 * This Class is a value object used to hold the last batch sent and the last 
 * batch received to/from Box X for a specific suffix.  This value object
 * corresponds to table MERCURY.MERCURY_BATCH.
 * 
 * @author tpeterson
 */
public class LastBatchVO 
{
  private String suffix;
  private String lastBatchSent;
  
  /**
   * Default constructor
   */
  public LastBatchVO()
  {
  }


  /**
   * Sets the suffix to the new value passed
   * @param suffix New suffix.
   */
  public void setSuffix(String suffix)
  {
    this.suffix = suffix;
  }


  /**
   * Get the suffix
   * @return the suffix
   */
  public String getSuffix()
  {
    return suffix;
  }
  
  /**
   * Sets the lastBatchSent to the new value passed
   * @param lastBatchSent New lastBatchSent.
   */
  public void setLastBatchSent(String lastBatchSent)
  {
    this.lastBatchSent = lastBatchSent;
  }

  /**
   * Gets the lastBatchSent
   * @return The lastBatchSent
   */
  public String getLastBatchSent()
  {
    return lastBatchSent;
  }

}