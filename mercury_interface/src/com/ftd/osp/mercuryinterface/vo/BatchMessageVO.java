package com.ftd.osp.mercuryinterface.vo;



public class BatchMessageVO 
{

  private String message;
  private long lengthWithoutTags = 0;

  public BatchMessageVO()
  {
  }
  
  public void setMessage(String value)
  {
    message = value;
  }
  public String getMessage()
  {
    return message;
  }

  public void setLengthWithoutTags(long value)
  {
    lengthWithoutTags = value;
  }
  public long getLengthWithoutTags()
  {
    return lengthWithoutTags;
  }

}