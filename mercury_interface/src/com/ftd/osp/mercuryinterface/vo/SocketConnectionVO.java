package com.ftd.osp.mercuryinterface.vo;

public class SocketConnectionVO 
{
  private String IPAddress;
  private String port;

  public SocketConnectionVO()
  {
  }
  
  public String getIPAddress()
  {
    return IPAddress;
  }

  public void setIPAddress(String value)
  {
    IPAddress = value;
  }    
  
  public String getPort()
  {
    return port;
  }

  public void setPort(String value)
  {
    port = value;
  }    
  
  
}