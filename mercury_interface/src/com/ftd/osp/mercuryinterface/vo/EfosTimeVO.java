package com.ftd.osp.mercuryinterface.vo;
import java.util.Calendar;

public class EfosTimeVO 
{

  Calendar efosStartTime;
  Calendar efosEndTime;
  Calendar efosAlertTime;

  public EfosTimeVO()
  {
  }

 public void setEfosAlertTime(Calendar value)
  {
    efosAlertTime = value;
  }
  public Calendar getEfosAlertTime()
  {
    return efosAlertTime;
  }    

 public void setEfosEndTime(Calendar value)
  {
    efosEndTime = value;
  }
  public Calendar getEfosEndTime()
  {
    return efosEndTime;
  }    

 public void setEfosStartTime(Calendar value)
  {
    efosStartTime = value;
  }
  public Calendar getEfosStartTime()
  {
    return efosStartTime;
  }    

}