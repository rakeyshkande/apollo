package com.ftd.osp.mercuryinterface.vo;


import java.util.*;

public class BatchSendVO 
{
  private List messageList;
  private String batchString;

  public BatchSendVO()
  {
  }
  
 public void setMessageList(List value)
  {
    messageList = value;
  }
  public List getMessageList()
  {
    return messageList;
  }    

 public void setBatchString(String value)
  {
    batchString = value;
  }
  public String getBatchString()
  {
    return batchString;
  }    
  
  
}