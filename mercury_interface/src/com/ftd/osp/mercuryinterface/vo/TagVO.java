package com.ftd.osp.mercuryinterface.vo;


public class TagVO 
{
  private int tagLength;
  private String tagText;

  public TagVO()
  {
  }

  public int getTagLength()
  {
    return tagLength;
  }

  public void setTagLength(int value)
  {
    tagLength = value;
  }  
  
  public String getTagText()
  {
    return tagText;
  }

  public void setTagText(String value)
  {
    tagText = value;
  }    


}