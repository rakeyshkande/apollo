package com.ftd.osp.mercuryinterface.vo;


public class MessageErrorVO 
{

  private MercuryMessageVO message;
  private String errorText;

  public MessageErrorVO()
  {
  }
  
  public MercuryMessageVO getMessage()
  {
    return message;
  }

  public void setMessage(MercuryMessageVO value)
  {
    message = value;
  }  

  public String getMercuryID()
  {
    return errorText;
  }

  public void setMercuryID(String value)
  {
    errorText = value;
  }  

  
}