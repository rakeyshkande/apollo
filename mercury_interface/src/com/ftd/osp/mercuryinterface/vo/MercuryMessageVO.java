package com.ftd.osp.mercuryinterface.vo;
import java.io.Serializable;
import java.util.Date;

public class MercuryMessageVO implements Cloneable,Serializable 
{
  // direction constants
  public static final String OUTBOUND = "OUTBOUND";
  public static final String INBOUND = "INBOUND";
  
  private String MercuryID;
  private String MercuryMessageNumber;
  private String MercuryOrderNumber;
  private String MercuryStatus;
  private String MessageType;
  private String OutboundID;
  private String SendingFlorist;
  private String FillingFlorist;
  private Date OrderDate;
  private String Recipient;
  private String Address;
  private String CityStateZip;
  private String PhoneNumber;
  private Date DeliveryDate;
  private String FirstChoice;
  private String SecondChoice;
  private Double Price;
  private String CardMessage;
  private String Occasion;
  private String SpecialInstructions;
  private String Priority;
  private String Operator;
  private String Comments;
  private String SakText;
  private int CTSEQ;
  private int CRSEQ;
  private Date TransmissionDate;
  private String ReferenceNumber;
  private String ProductID;
  private String ZipCode;
  private String AskAnswerCode;
  private String DeliveryDateText;
  private String SortValue;  
  private String RetrievalFlag;  
  private String fromMessageNumber;
  private String toMessageNumber;
  private Date fromMessageDate;
  private Date toMessageDate;
  private String CombinedReportNumber;
  private String RofNumber;
  private double OverUnderCharge;
  private String ADJReasonCode;
  private String retrievalFromDate;
  private String retrievalFromMessage;
  private String retrievalToDate;
  private String retrievalToMessage;  
  private String viewQueue;
  
  // new for phase III
  private String direction;
  private String suffix;
  private String orderSequence;
  private String adminSequence;

  
  public MercuryMessageVO()
  {
  }

  public String getViewQueue()
  {
    return viewQueue;
  }

  public void setViewQueue(String value)
  {
    viewQueue = value;
  }

  public String getRetrievalToMessage()
  {
    return retrievalToMessage;
  }

  public void setRetrievalToMessage(String value)
  {
    retrievalToMessage = value;
  }

  public String getRetrievalToDate()
  {
    return retrievalToDate;
  }

  public void setRetrievalToDate(String value)
  {
    retrievalToDate = value;
  }


  public String getRetrievalFromMessage()
  {
    return retrievalFromMessage;
  }

  public void setRetrievalFromMessage(String value)
  {
    retrievalFromMessage = value;
  }


  public String getRetrievalFromDate()
  {
    return retrievalFromDate;
  }

  public void setRetrievalFromDate(String value)
  {
    retrievalFromDate = value;
  }





  public double getOverUnderCharge()
  {
    return OverUnderCharge;
  }

  public void setOverUnderCharge(double value)
  {
    OverUnderCharge = value;
  }

  public String getADJReasonCode()
  {
    return ADJReasonCode;
  }

  public void setADJReasonCode(String value)
  {
    ADJReasonCode = value;
  }

  public String getRofNumber()
  {
    return RofNumber;
  }

  public void setRofNumber(String value)
  {
    RofNumber = value;
  }

  public String getCombinedReportNumber()
  {
    return CombinedReportNumber;
  }

  public void setCombinedReportNumber(String value)
  {
    CombinedReportNumber = value;
  }



  public String getMercuryID()
  {
    return MercuryID;
  }

  public void setMercuryID(String newMercuryID)
  {
    MercuryID = newMercuryID;
  }

  public String getMercuryMessageNumber()
  {
    return MercuryMessageNumber;
  }

  public void setMercuryMessageNumber(String newMercuryMessageNumber)
  {
    MercuryMessageNumber = newMercuryMessageNumber;
  }

  public String getMercuryOrderNumber()
  {
    return MercuryOrderNumber;
  }

  public void setMercuryOrderNumber(String newMercuryOrderNumber)
  {
    MercuryOrderNumber = newMercuryOrderNumber;
  }

  public String getMercuryStatus()
  {
    return MercuryStatus;
  }

  public void setMercuryStatus(String newMercuryStatus)
  {
    MercuryStatus = newMercuryStatus;
  }

  public String getMessageType()
  {
    return MessageType;
  }

  public void setMessageType(String newMessageType)
  {
    MessageType = newMessageType;
  }

  public String getOutboundID()
  {
    return OutboundID;
  }

  public void setOutboundID(String newOutboundID)
  {
    OutboundID = newOutboundID;
  }

  public String getSendingFlorist()
  {
    return SendingFlorist;
  }

  public void setSendingFlorist(String newSendingFlorist)
  {
    SendingFlorist = newSendingFlorist;
  }

  public String getFillingFlorist()
  {
    return FillingFlorist;
  }

  public void setFillingFlorist(String newFillingFlorist)
  {
    FillingFlorist = newFillingFlorist;
  }

  public Date getOrderDate()
  {
    return OrderDate;
  }

  public void setOrderDate(Date newOrderDate)
  {
    OrderDate = newOrderDate;
  }

  public String getRecipient()
  {
    return Recipient;
  }

  public void setRecipient(String newRecipient)
  {
    Recipient = newRecipient;
  }

  public String getAddress()
  {
    return Address;
  }

  public void setAddress(String newAddress)
  {
    Address = newAddress;
  }

  public String getCityStateZip()
  {
    return CityStateZip;
  }

  public void setCityStateZip(String newCityStateZip)
  {
    CityStateZip = newCityStateZip;
  }

  public String getPhoneNumber()
  {
    return PhoneNumber;
  }

  public void setPhoneNumber(String newPhoneNumber)
  {
    PhoneNumber = newPhoneNumber;
  }

  public Date getDeliveryDate()
  {
    return DeliveryDate;
  }

  public void setDeliveryDate(Date newDeliveryDate)
  {
    DeliveryDate = newDeliveryDate;
  }

  public String getFirstChoice()
  {
    return FirstChoice;
  }

  public void setFirstChoice(String newFirstChoice)
  {
    FirstChoice = newFirstChoice;
  }

  public String getSecondChoice()
  {
    return SecondChoice;
  }

  public void setSecondChoice(String newSecondChoice)
  {
    SecondChoice = newSecondChoice;
  }

  public Double getPrice()
  {
    return Price;
  }

  public void setPrice(Double newPrice)
  {
    Price = newPrice;
  }

  public String getCardMessage()
  {
    return CardMessage;
  }

  public void setCardMessage(String newCardMessage)
  {
    CardMessage = newCardMessage;
  }

  public String getOccasion()
  {
    return Occasion;
  }

  public void setOccasion(String newOccasion)
  {
    Occasion = newOccasion;
  }

  public String getSpecialInstructions()
  {
    return SpecialInstructions;
  }

  public void setSpecialInstructions(String newSpecialInstructions)
  {
    SpecialInstructions = newSpecialInstructions;
  }

  public String getPriority()
  {
    return Priority;
  }

  public void setPriority(String newPriority)
  {
    Priority = newPriority;
  }

  public String getOperator()
  {
    return Operator;
  }

  public void setOperator(String newOperator)
  {
    Operator = newOperator;
  }

  public String getComments()
  {
    return Comments;
  }

  public void setComments(String newComments)
  {
    Comments = newComments;
  }

  public String getSakText()
  {
    return SakText;
  }

  public void setSakText(String newSakText)
  {
    SakText = newSakText;
  }

  public int getCTSEQ()
  {
    return CTSEQ;
  }

  public void setCTSEQ(int newCTSEQ)
  {
    CTSEQ = newCTSEQ;
  }

  public int getCRSEQ()
  {
    return CRSEQ;
  }

  public void setCRSEQ(int newCRSEQ)
  {
    CRSEQ = newCRSEQ;
  }

  public Date getTransmissionDate()
  {
    return TransmissionDate;
  }

  public void setTransmissionDate(Date newTransmissionDate)
  {
    TransmissionDate = newTransmissionDate;
  }

  public String getReferenceNumber()
  {
    return ReferenceNumber;
  }

  public void setReferenceNumber(String newReferenceNumber)
  {
    ReferenceNumber = newReferenceNumber;
  }

  public String getProductID()
  {
    return ProductID;
  }

  public void setProductID(String newProductID)
  {
    ProductID = newProductID;
  }

  public String getZipCode()
  {
    return ZipCode;
  }

  public void setZipCode(String newZipCode)
  {
    ZipCode = newZipCode;
  }
  
  public String getAskAnswerCode()
  {
    return AskAnswerCode;
  }

  public void setAskAnswerCode(String value)
  {
    AskAnswerCode = value;
  }  

  public String getDeliveryDateText()
  {
    return DeliveryDateText;
  }

  public void setDeliveryDateText(String value)
  {
    DeliveryDateText = value;
  }  
 
  public String getSortValue()
  {
    return SortValue;
  }

  public void setSortValue(String value)
  {
    SortValue = value;
  }    
  
  public String getRetrievalFlag()
  {
    return RetrievalFlag;
  }

  public void setRetrievalFlag(String value)
  {
    RetrievalFlag = value;
  }      
  
  public Object clone(){ 
    return this;
  }


  public void setDirection(String direction)
  {
    this.direction = direction;
  }


  public String getDirection()
  {
    return direction;
  }


  public void setSuffix(String suffix)
  {
    this.suffix = suffix;
  }


  public String getSuffix()
  {
    return suffix;
  }


  public void setOrderSequence(String orderSequence)
  {
    this.orderSequence = orderSequence;
  }


  public String getOrderSequence()
  {
    return orderSequence;
  }


  public void setAdminSequence(String adminSequence)
  {
    this.adminSequence = adminSequence;
  }


  public String getAdminSequence()
  {
    return adminSequence;
  }

  
}